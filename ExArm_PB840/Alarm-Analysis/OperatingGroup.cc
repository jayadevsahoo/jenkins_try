#include "stdafx.h"
//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: OperatingGroup - Derived from pure virtual base class AlarmOperand.
//---------------------------------------------------------------------
//@ Interface-Description
// The main interface to OperatingGroup utilizes the setUpdate and setReset
// methods, and the evaluate method inherited from AlarmOperand.  When an
// OperatingCondition needs to be updated, or the operator presses ManualReset
// or there is a Patient Data alarm which has reset, all ancestor groups of the
// affected OperatingCondition or OperatingGroup are marked accordingly via the
// setUpdate or setReset methods.  After all affected groups are marked, they
// are evaluated via the evaluate method which calls evaluateUpdate_ or
// evaluateReset_ as required.
//---------------------------------------------------------------------
//@ Rationale
// The OperatingGroup collects OperatingConditions and other OperatingGroups in
// an ordered list, OperatingGroup::children_ via AlarmComponent objects, to
// model the criteria specified in the condition augmentation column of the
// Technical and Non-Technical alarm matrices.
//---------------------------------------------------------------------
//@ Implementation-Description
// If the OperatingGroup is the detection criteria for an Alarm, the pointer to
// an Alarm object, pCauses_, will point to that Alarm object; otherwise,
// pCauses_ will be null.
//
// OperatingGroup contains a (possibly empty) list of AlarmAugmentations which
// reference those Alarm objects the OperatingGroup augments, as specified in
// the alarm matrices.
//
// If there is an object derived from ViolationHistoryManager associated with 
// the OperatingGroup, pViolationManager_ will point to it.  This manager is
// responsible for tracking the number of times the state of the OperatingGroup
// evaluates to true.  (As an example, assume the OperatingGroup models the
// detection criteria for the High Exhaled Tidal Volume alarm.  The High
// Exhaled Tidal Volume alarm urgency escalates as the number of breaths, out
// of the last four which meet the detection criteria, increases.  Each time a
// breath ends and spirometry for that breath becomes available, the oldest of
// the four breaths is discarded.  If it was a violation breath, the counter
// counting violation breaths is decremented; otherwise the counter is not
// decremented.  Then, the measured Exhaled Tidal Volume for the breath which
// just ended is compared to the High Exhaled Tidal Volume alarm limit.  If the
// breath is in violation, the counter is incremented; if the breath is not in
// violation, the counter is not incremented.)  If there is an object derived
// from Constraint associated with the OperatingGroup, pConstraint_ will point
// to it.  If an OperatingGroup points to a constraint, it will have one child
// OperatingGroup only.  That child OperatingGroup will point to an object
// derived from ViolationHistoryManager.  When the parent OperatingGroup needs
// to be evaluated, the number of violations tracked by the child
// OperatingGroup object's ViolationHistoryManager will be compared against the
// bounds contained in the Constraint.  If the number of violations falls
// within the bounds contained in the constraint, the parent OperatingGroup is
// active.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//        none
//---------------------------------------------------------------------
//@ Invariants
//        none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Alarm-Analysis/vcssrc/OperatingGroup.ccv   25.0.4.0   19 Nov 2013 13:51:24   pvcs  $
//
//@ Modification-Log

//  
//  
//   Revision 001   By:   hct      Date: 01/01/95         DR Number:   n/a
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//  
//   Revision 002   By:   hct      Date: 01/18/96         DR Number:   659
//      Project:   Sigma   (R8027)
//      Description:
//         Use OsTimeStamp for interval timing instead of TimeStamp.
//  
//   Revision 003   By:   gbs      Date: 05/17/96         DR Number:   995
//      Project:   Sigma   (R8027)
//      Description:
//         Fix CALL_TRACE and add passed parameter comments.
//         These changes do not affect Unit Test.
//  
//   Revision 004   By:   sah      Date: 01/12/99         DR Number:   5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to non-inlined method.
//  
//   Revision 005   By:   hct    Date: 08/30/99         DR Number:  5513
//      Project:   NewAlarms
//      Description:
//         Removed de-escalation from Patient-Data alarms.
//  
//=====================================================================

#include "Sigma.hh"
#include "OperatingComponent.hh"
#include "OperatingGroup.hh"

//@ Usage-Classes
#include "Constraint.hh"

//@ End-Usage

// Static member initialization
static Uint 
    pActiveTopLevelGroupsMemory[(sizeof(OperatingGroupList) + sizeof(Uint) - 1) 
    / sizeof(Uint)];

OperatingGroupList&OperatingGroup::RActiveTopLevelGroups_ = 
    *((OperatingGroupList*)::pActiveTopLevelGroupsMemory);

//@ Code...

//=====================================================================
//
//      Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: OperatingGroup
//
//@ Interface-Description
//        Default constructor.
//
// >Von
//        n    Enumerated name of the AlarmOperand to be constructed.
//        pC   If this OperatingGroup models an Alarm Matrix augmentation 
//             involving a constraint (e.g. Two apnea events within 10 times
//             the apnea interval), pConstraint_ points to an object derived 
//             from Constraint.
//        pRC  If this OperatingGroup has a Constraint attached to it 
//             (pConstraint_ does not equal NULL), it is possible that the auto 
//             reset criteria is not the simple negation of the Constraint.  
//             For example, delivered O2% must be high for 30 seconds in order 
//             to declare the HIGH DELIVERED O2 alarm; however, the return of 
//             delivered O2% to normal is not sufficient to reset the alarm.  
//             The delivered O2% must be normal for 30 seconds before the alarm 
//             autoresets.  This constraint on autoreset is modeled via the 
//             Constraint pointed to by attribute pAutoResetconstraint_.
//        pVM  If this OperatingGroup has a parent with an object derived from
//             Constraint attached to it, attribute pViolationManager_ for this
//             OperatingGroup will point to an object derived from
//             ViolationHistoryManager.  The ViolationHistoryManager tracks the 
//             number of violations that occur.  For example, this could be the 
//             number of breaths in violation of an operator set limit, or the 
//             number of times the alarm has transitioned to active within a 
//             certain amount of time.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//        none
//---------------------------------------------------------------------
//@ PostCondition
//        none
//@ End-Method
//=====================================================================
OperatingGroup::OperatingGroup(OperandName                n, 
                               const Constraint*          pC,
                               const Constraint*          pRC,
                               ViolationHistoryManager*   pVM): 

                               AlarmOperand         (n),
                               pConstraint_         (pC),
                               pAutoResetConstraint_(pRC),
                               pViolationManager_   (pVM)
{
  CALL_TRACE("OperatingGroup(n, pC, pRC, pVM)" );

  topLevel_ = FALSE;
  pCauses_  = NULL;
  // $[TI1]  
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: OperatingGroup
//
//@ Interface-Description
//        Copy constructor.
//
// >Von
//        rOp  Reference to the object to be copied.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//        none
//---------------------------------------------------------------------
//@ PostCondition
//        none
//@ End-Method
//=====================================================================
OperatingGroup::OperatingGroup(const OperatingGroup& rOp): 

                               AlarmOperand         (rOp),
                               pConstraint_         (rOp.pConstraint_),
                               pAutoResetConstraint_(rOp.pAutoResetConstraint_),
                               pViolationManager_   (rOp.pViolationManager_)
{
  CALL_TRACE("OperatingGroup(rOp)");

  augmentations_ = rOp.augmentations_;
  pCauses_       = rOp.pCauses_;
  topLevel_      = rOp.topLevel_;
  children_      = rOp.children_;
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~OperatingGroup
//
//@ Interface-Description
//        Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//        none
//---------------------------------------------------------------------
//@ PostCondition
//        none
//@ End-Method
//=====================================================================
OperatingGroup::~OperatingGroup(void)
{
  CALL_TRACE("~OperatingGroup(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Initialize
//
//@ Interface-Description
//        Construct the OperatingGroupList during system initialization.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//        none
//---------------------------------------------------------------------
//@ PostCondition
//        none
//@ End-Method
//=====================================================================
void
OperatingGroup::Initialize(void) 
{
  CALL_TRACE("Initialize(void)");

  CLASS_ASSERTION(sizeof(::pActiveTopLevelGroupsMemory) >= 
                       sizeof(OperatingGroupList));

  new (::pActiveTopLevelGroupsMemory) OperatingGroupList;
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setUpdate
//
//@ Interface-Description
//        Set attribute OperatingGroup::update_ to TRUE.  If this
//        OperatingGroup is a top level group, add a reference to this
//        OperatingGroup object to the list of top level groups, otherwise,
//        message the method to set the parent OperatingGroup objects
//        update_ attribute.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//        none
//---------------------------------------------------------------------
//@ PostCondition
//        none
//@ End-Method
//=====================================================================
void
OperatingGroup::setUpdate(void)
{
  CALL_TRACE("setUpdate(void)");

  if (!update_) // $[TI1]
  {
    update_ = TRUE;
    if (topLevel_)  // $[TI1.1]
    {
      OperatingGroup::RActiveTopLevelGroups_.append(*this);
    }
    else  // $[TI1.2]
    {
      setParentUpdate_();
    }
  } // $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setReset
//
//@ Interface-Description
//        Set attribute OperatingGroup::reset_ to TRUE.  If this
//        OperatingGroup is a top level group, add a reference to this
//        OperatingGroup object to the list of top level groups, otherwise,
//        message the method to set the parent OperatingGroup objects
//        reset_ attribute.        
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//        none
//---------------------------------------------------------------------
//@ PostCondition
//        none
//@ End-Method
//=====================================================================
void
OperatingGroup::setReset(void)
{
  CALL_TRACE("setReset(void)");

  if (!reset_) // $[TI1]
  {
    reset_ = TRUE;
    if (topLevel_)  // $[TI1.1]
    {
      OperatingGroup::RActiveTopLevelGroups_.append(*this);
    }
    else  // $[TI1.2]
    {
      setParentReset_();
    }
  } // $[TI2]
}


//=====================================================================
//
//      Protected Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: evaluateUpdate_
//
//@ Interface-Description
//        Method evaluateUpdate_ evaluates this OperatingGroup in update mode.
//---------------------------------------------------------------------
//@ Implementation-Description
//        Since the state of an OperatingGroup is dependent on the states of
//        its child AlarmOperand objects, method
//        OperatingGroup::evaluateChildren_ is messaged to evaluate the states
//        of the child AlarmOperand objects and logically combine them in
//        order to determine the state of this OperatingGroup.  If this
//        OperatingGroup is modeling an Alarm Matrix row involving a
//        constraint (e.g. Two apnea events within 10 times the Apnea
//        Interval), the Constraint is messaged to evaluate itself.  If the
//        constraint evaluates to FALSE and there is an autoreset constraint,
//        the autoreset constraint is messaged to evaluate itself.  The
//        autoreset constraint must evaluate to TRUE for the state of the
//        OperatingGroup to transistion to FALSE $[05032].  The Boolean results 
//        of the evaluations is returned.
//---------------------------------------------------------------------
//@ PreCondition
//        none
//---------------------------------------------------------------------
//@ PostCondition
//        none
//@ End-Method
//=====================================================================
Boolean
OperatingGroup::evaluateUpdate_(void) 
{
  CALL_TRACE("evaluateUpdate_(void)");

  Boolean state = evaluateChildren_();

  if (pConstraint_ != NULL)  // $[TI1]
  {
    SigmaStatus status = children_.goFirst();
    CLASS_ASSERTION( status == SUCCESS );
    state = pConstraint_->evaluate((OperatingGroup&)
                children_.currentItem().getChild()); // $[TI1.1] $[TI1.2]

    if ((state == FALSE) && isActive_ && (pAutoResetConstraint_ != NULL))  // $[TI1.3]
    {
      state = !pAutoResetConstraint_->evaluateAutoReset((OperatingGroup&)
                 children_.currentItem().getChild()); // $[TI1.3.1] $[TI1.3.2]
    }  // else $[TI1.4]
  } // else $[TI2] 

  return(state);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: evaluateReset_
//
//@ Interface-Description
//        Method evaluateReset_ evaluates this OperatingGroup in reset mode.
//---------------------------------------------------------------------
//@ Implementation-Description
//        Reset mode evaluation is brought about by the operator pressing the
//        Alarm reset key or the autoreset of an Alarm which causes Loss of
//        Patient Data Monitoring.  Once all affected AlarmOperand objects are
//        marked for evaluation, they are evaluated.  Since the state of an
//        OperatingGroup is dependent on the states of its child AlarmOperand
//        objects, method OperatingGroup::evaluateChildren_ is messaged to
//        evaluate the states of the child AlarmOperand objects and logically
//        combine them in order to determine the state of this OperatingGroup.
//        In reset mode, an OperatingCondition at the bottom of the hierarchy 
//        returns FALSE, thereby causing all of its ancestors to be reset and 
//        the active non-technical alarm to become inactive. 
//        $[05016]
//---------------------------------------------------------------------
//@ PreCondition
//        none
//---------------------------------------------------------------------
//@ PostCondition
//        none
//@ End-Method
//=====================================================================
Boolean
OperatingGroup::evaluateReset_(void) 
{
  CALL_TRACE("evaluateReset_(void)");

  return(evaluateChildren_());
   // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setCauseUpdate_
//@ Interface-Description
//        While the Operating Parameter Hierarchy is being evaluted, if an
//        OperatingGroup which causes an alarm changes state, message that
//        alarm to mark itself for cause update so it will be evaluated by the
//        Alarm Analysis cluster when the Operating Parameter Hierarchy has
//        finished being evaluated.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//        none
//---------------------------------------------------------------------
//@ PostCondition
//        none
//@ End-Method
//=====================================================================
void
OperatingGroup::setCauseUpdate_(void) 
{
  CALL_TRACE("setCauseUpdate_(void)");

  if (pCauses_ != NULL) // $[TI1]
  {
    // Inform the alarm caused by this group that the group has undergone a
    // state change.
    pCauses_->setCauseUpdate(whenOccurred_, intervalTime_); 
  } // else $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setAugmentUpdate_
//
//@ Interface-Description
//        While the Operating Parameter Hierarchy is being evaluted, if an
//        OperatingGroup which augments an alarm changes state, message that
//        alarm to mark itself for augment update so it will be evaluated by
//        the Alarm Analysis cluster when the Operating Parameter Hierarchy has
//        finished being evaluated.
//---------------------------------------------------------------------
//@ Implementation-Description
//        none
//---------------------------------------------------------------------
//@ PreCondition
//        none
//---------------------------------------------------------------------
//@ PostCondition
//        none
//@ End-Method
//=====================================================================
void
OperatingGroup::setAugmentUpdate_(void) 
{
  CALL_TRACE("setAugmentUpdate_(void)");

  SigmaStatus status = augmentations_.goFirst();

  // Process each of the ConditionAugmentation objects on the list
  // augmentUpdate_.
  while (status == SUCCESS) // $[TI1]
  { 
    const AlarmAugmentation& rAugmentation = augmentations_.currentItem();
    CLASS_ASSERTION(rAugmentation.getRole() ==
              AlarmAugmentation::CONDITION_AUGMENTATION);

    // Inform the alarm augmented by this group that the group has undergone a
    // state change.
    rAugmentation.getAlarm().setAugmentUpdate(whenOccurred_);

    status = augmentations_.goNext();
  } // No while statement executed. $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateViolationHistory_
//
//@ Interface-Description
//        If there is an object derived from ViolationHistoryManager associated
//        with this OperatingGroup, message it to update the number of
//        violations it is tracking.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//        none
//---------------------------------------------------------------------
//@ PostCondition
//        none
//@ End-Method
//=====================================================================
void
OperatingGroup::updateViolationHistory_(void) 
{
  CALL_TRACE("updateViolationHistory_(void)");

  if (pViolationManager_ != NULL)  // $[TI1]
  {
    pViolationManager_->update(isActive_);
  } // else $[TI2]  
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: resetViolationHistory_
//
//@ Interface-Description
//        If there is an object derived from ViolationHistoryManager associated
//        with this OperatingGroup, message it to reset itself.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//        none
//---------------------------------------------------------------------
//@ PostCondition
//        none
//@ End-Method
//=====================================================================
void
OperatingGroup::resetViolationHistory_(void) 
{
  CALL_TRACE("resetViolationHistory_(void)");

  if (pViolationManager_ != NULL)  // $[TI1]
  {
    pViolationManager_->reset();
  } // else $[TI2]  
}


//=====================================================================
//
//      Private Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: initTopLevel_
//
//@ Interface-Description
//        This method is called during system initialization to initialize
//        attribute topLevel_.  If the OperatingGroup has no parents,
//        initTopLevel_ sets topLevel_, otherwise topLevel_ is cleared.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//        none
//---------------------------------------------------------------------
//@ PostCondition
//        none
//@ End-Method
//=====================================================================
void
OperatingGroup::initTopLevel_(void) 
{
  CALL_TRACE("initTopLevel_(void)");

  topLevel_ = getParents().isEmpty();
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: initMaxViolations_
//
//@ Interface-Description
//        If this OperatingGroup has an object derived from
//        ViolationHistoryManager attached to it (pViolationManager_ does not
//        equal NULL), message the ViolationHistoryManager to set the maximum
//        number of violations to track.  The ViolationHistoryManager gets a 
//        reference to the AlarmOperand::parents_ AlarmOperandList from this 
//        OperatingGroup and examines each of the OperatingGroup objects on the
//        parents list and queries their Constraint objects for the value of 
//        their minimum constraint and takes the largest of these as the 
//        maximum number of violations to track.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//        none
//---------------------------------------------------------------------
//@ PostCondition
//        none
//@ End-Method
//=====================================================================
void
OperatingGroup::initMaxViolations_(void)
{
  CALL_TRACE("initMaxViolations_(void)");

  // Initialize the maxNumViolations data member of the ViolationHistoryList
  if (pViolationManager_ != NULL)  // $[TI1]
  {
    CLASS_ASSERTION(!getParents().isEmpty())

    pViolationManager_->setMaxNumViolations(this);
  } // else $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: evaluateChildren_
//
//@ Interface-Description
//      Message each child AlarmOperand through the OperatingComponent objects
//      on the children_ list to evaluate itself.
//---------------------------------------------------------------------
//@ Implementation-Description
//      none
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Boolean
OperatingGroup::evaluateChildren_(void) 
{
  CALL_TRACE("evaluateChildren_(void)");

  Boolean state = FALSE;
  
  SigmaStatus status = children_.goFirst();
  while (status == SUCCESS)  // $[TI1]
  {
    OperatingComponent& opComp = children_.currentItem();
    state = opComp.evaluate(state);
    status = children_.goNext();
  } // No while statement executed. $[TI2]
  return(state);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//@ Interface-Description
//    This method is called when a software fault is detected by the
//    fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//    'lineNumber' are essential pieces of information.  The 'pFileName'
//    and 'pPredicate' strings may be defaulted in the macro to reduce
//    code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//    This method receives the call for the SoftFault, adds it sub-system
//    and class name ID and sends the message to the non-member function
//    SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition    
//    none
//@ End-Method
//=====================================================================

void
OperatingGroup::SoftFault(const SoftFaultID  softFaultID,
                          const Uint32 lineNumber,
                          const char  *pFileName,
                          const char  *pPredicate)  
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, ALARM_ANALYSIS, 
                          Alarm_Analysis::OPERATINGGROUP,
                          lineNumber, pFileName, pPredicate);
}
