

#define WINCEOEM 1
#include "Windows.h"
#include "winbase.h"
#pragma warning (push, 3)
#include <ndis.h>
#pragma warning (pop)
#include "nuiouser.h"

#include "pkfuncs.h"

#define CMDLINE_SIZE	100
char aCmdLineBuf[CMDLINE_SIZE];
#define ASPACE	' '
#define EOS           '\0'
#define HYPHEN    '-'
 
#define MAX_ARGC 10

char *file;

#define OID_NDIS_SMSC_DUMP_ALL_REGS     0xFFFFFF01U



//===============================================================================================//
DWORD NDIS_Query_OIDValue(HANDLE hHandle, NDIS_OID Oid, UINT8 *pData, UINT32 *pDataLen)
{
    PNDISUIO_QUERY_OID  pQueryOid       = NULL;
    DWORD               nBytesReturned  = 0;
    BOOL                bRetval         = TRUE;
    DWORD               nErrCode        = ERROR_SUCCESS;

    // Allocate memory for the query buffer
    pQueryOid = (PNDISUIO_QUERY_OID)malloc(*pDataLen + sizeof(NDISUIO_QUERY_OID)); 
    if(pQueryOid == NULL)
    {
        RETAILMSG(TRUE,  (TEXT("Failed to allocate memory for pQueryOid\n")));
        goto CleanUp;
    }
    
    pQueryOid->Oid = Oid;

    pQueryOid->ptcDeviceName = NULL;
    bRetval = DeviceIoControl(hHandle, 
                              IOCTL_NDISUIO_QUERY_OID_VALUE,
                              (LPVOID)pQueryOid,
                              FIELD_OFFSET(NDISUIO_QUERY_OID, Data) + *pDataLen,
                              (LPVOID)pQueryOid,
                              FIELD_OFFSET(NDISUIO_QUERY_OID, Data) + *pDataLen,
                              &nBytesReturned,
                              NULL);
    if(bRetval == FALSE)
    {
        nErrCode = GetLastError();
        RETAILMSG(TRUE, ( TEXT("IOCTL_NDISUIO_QUERY_OID_VALUE failed (BytesReturned = %ld) (Error = %d)\n"), \
            nBytesReturned, nErrCode));
        *pDataLen = nBytesReturned;
        goto CleanUp;
    }
    else
    {
        nBytesReturned -= FIELD_OFFSET(NDISUIO_QUERY_OID, Data);

        if(nBytesReturned > *pDataLen)
        {
            nBytesReturned = *pDataLen;
        }
        else
        {
            *pDataLen = nBytesReturned;
        }

        memcpy(pData, &pQueryOid->Data[0], nBytesReturned);
        
    }
    
CleanUp:

    if(pQueryOid != NULL)
    {
        free(pQueryOid);
    }
    return nErrCode;
}


DWORD NDIS_set_OIDValue(HANDLE hHandle, NDIS_OID Oid, UINT8 *pData, UINT32 DataLen)
{
   PNDISUIO_SET_OID    pSetOid         = NULL;
   DWORD               BytesReturned   = 0;
   BOOL                bRetval         = FALSE;
   DWORD               nErrCode        = 100;
    
    pSetOid = (PNDISUIO_SET_OID) malloc(DataLen + sizeof(NDISUIO_SET_OID)); 
    if(pSetOid == NULL)
    {
      RETAILMSG(TRUE, ( TEXT("Failed to allocate memory for pSetOid\n")));
    }
    else
    {
        pSetOid->Oid = Oid;
        pSetOid->ptcDeviceName = NULL;

        if(pData != NULL) 
        {
          memcpy(&pSetOid->Data[0], pData, DataLen);
        }
        bRetval = DeviceIoControl(hHandle, 
                                IOCTL_NDISUIO_SET_OID_VALUE,
                                (LPVOID)pSetOid,
                                sizeof(NDISUIO_SET_OID) + DataLen,
                                NULL,
                                0,
                                &BytesReturned,
                                NULL);
        if(bRetval == FALSE)
        {
          RETAILMSG(TRUE, ( TEXT("IOCTL_NDISUIO_SET_OID_VALUE failed (Error = %d)\n"), GetLastError()));
        }
        else
        {
            nErrCode = ERROR_SUCCESS;
        }
    }
   if(pSetOid) free(pSetOid);
   return nErrCode;
}


void NDIS_query_ndis_stats(HANDLE hHandle)
{
    ULONG64 value;
    UINT32 len = sizeof(ULONG64);
    
    NDIS_Query_OIDValue(hHandle, OID_GEN_XMIT_OK,  (UINT8 *)&value, &len);
    RETAILMSG(TRUE, (L"    OID_GEN_XMIT_OK: %d\r\n", value));
    
    NDIS_Query_OIDValue(hHandle, OID_GEN_RCV_OK,  (UINT8 *)&value, &len);
    RETAILMSG(TRUE, (L"    OID_GEN_RCV_OK: %d\r\n", value));

    NDIS_Query_OIDValue(hHandle, OID_GEN_XMIT_ERROR,  (UINT8 *)&value, &len);
    RETAILMSG(TRUE, (L"    OID_GEN_XMIT_ERROR: %d\r\n", value));
    
    NDIS_Query_OIDValue(hHandle, OID_GEN_RCV_ERROR,  (UINT8 *)&value, &len);
    RETAILMSG(TRUE, (L"    OID_GEN_RCV_ERROR: %d\r\n", value));
    
    NDIS_Query_OIDValue(hHandle, OID_GEN_RCV_NO_BUFFER,  (UINT8 *)&value, &len);
    RETAILMSG(TRUE, (L"    OID_GEN_RCV_NO_BUFFER: %d\r\n", value));
    
    NDIS_Query_OIDValue(hHandle, OID_802_3_RCV_ERROR_ALIGNMENT,  (UINT8 *)&value, &len);
    RETAILMSG(TRUE, (L"    OID_802_3_RCV_ERROR_ALIGNMENT: %d\r\n", value));
    
    NDIS_Query_OIDValue(hHandle, OID_802_3_XMIT_ONE_COLLISION,  (UINT8 *)&value, &len);
    RETAILMSG(TRUE, (L"    OID_802_3_XMIT_ONE_COLLISION: %d\r\n", value));

    NDIS_Query_OIDValue(hHandle, OID_802_3_XMIT_MORE_COLLISIONS,  (UINT8 *)&value, &len);
    RETAILMSG(TRUE, (L"    OID_802_3_XMIT_MORE_COLLISIONS: %d\r\n", value));

    NDIS_Query_OIDValue(hHandle, OID_GEN_BROADCAST_FRAMES_XMIT,  (UINT8 *)&value, &len);
    RETAILMSG(TRUE, (L"    OID_GEN_BROADCAST_FRAMES_XMIT: %d\r\n", value));

    NDIS_Query_OIDValue(hHandle, OID_GEN_BROADCAST_FRAMES_RCV,  (UINT8 *)&value, &len);
    RETAILMSG(TRUE, (L"    OID_GEN_BROADCAST_FRAMES_RCV: %d\r\n", value));

    NDIS_Query_OIDValue(hHandle, OID_GEN_MULTICAST_FRAMES_XMIT,  (UINT8 *)&value, &len);
    RETAILMSG(TRUE, (L"    OID_GEN_MULTICAST_FRAMES_XMIT: %d\r\n", value));

    NDIS_Query_OIDValue(hHandle, OID_GEN_MULTICAST_FRAMES_RCV,  (UINT8 *)&value, &len);
    RETAILMSG(TRUE, (L"    OID_GEN_MULTICAST_FRAMES_RCV: %d\r\n", value));

}

void NDIS_dump_smsc_reg(HANDLE hHandle)
{
    ULONG64 value;
    UINT32 len = sizeof(ULONG64);    
    NDIS_Query_OIDValue(hHandle, OID_NDIS_SMSC_DUMP_ALL_REGS,  (UINT8 *)&value, &len);
}

void main (DWORD	hInstance, DWORD hPrevInstance, TCHAR *pCmdLine, int nShowCmd)
{
    char       *paNext, *paCmdLine = aCmdLineBuf;
    DWORD    dwWrittenBytes;
    UINT         argc, Index=0;
    char         *argv[MAX_ARGC];
    TCHAR      dev_nm[512];
    PNDISUIO_QUERY_BINDING	pQueryBinding;
    char  buf[1024];
//    char  adapter_name[32];

    UNREFERENCED_PARAMETER(hInstance);
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(nShowCmd);

    wcstombs(paCmdLine, pCmdLine, CMDLINE_SIZE);

    argc = 1;
    while (EOS != *paCmdLine) 
    {
        while (ASPACE == *paCmdLine) paCmdLine++;
        if (EOS == *paCmdLine) break;
        for (paNext = paCmdLine; ASPACE != *paNext && EOS != *paNext; paNext++);

        if (MAX_ARGC == argc)
        {
            RETAILMSG(TRUE, (TEXT("Extra commands; not supported")));
            break;
        } else {
            argc++;
        }
        argv[argc - 1] = paCmdLine;
        if (ASPACE == *paNext) 
        {
            *paNext = '\0';
            paCmdLine = ++paNext;
        } else {
            paCmdLine = paNext;
        }
    }
//    strcpy(adapter_name,"SMSC91181");	
//    if ((argc>1) && (strcmp((char *)argv[1], "emac") == 0))
//    {
//	    strcpy(adapter_name,"EMAC1");	
//    }   

    file = CreateFile(NDISUIO_DEVICE_NAME,
                                GENERIC_READ | GENERIC_WRITE,
                                FILE_SHARE_READ | FILE_SHARE_WRITE,
                                NULL,
                                OPEN_EXISTING,
                                FILE_ATTRIBUTE_NORMAL | FILE_FLAG_OVERLAPPED,
                                INVALID_HANDLE_VALUE);

    
    if (INVALID_HANDLE_VALUE == file) {
        RETAILMSG(TRUE,(TEXT("CreateFile failed !")));
        return;
    }

    pQueryBinding=(PNDISUIO_QUERY_BINDING)buf;
    for(Index = 0;;Index++)
    {
        pQueryBinding->BindingIndex = Index;
        if (DeviceIoControl(file,
                                    IOCTL_NDISUIO_QUERY_BINDING,
                                    pQueryBinding,
                                    sizeof(NDISUIO_QUERY_BINDING),
                                    buf,
                                    sizeof(buf),
                                    &dwWrittenBytes,
                                    NULL)) 
    
        {
            /*RETAILMSG(TRUE, (TEXT( "adapter: %s\r\n" ), 
                                            (TCHAR*)&buf[ pQueryBinding->DeviceNameOffset ])) ; */
       
            if(wcscmp((TCHAR*)&buf[pQueryBinding->DeviceNameOffset ], TEXT("SMSC91181")))
                continue;
            
            else{
                //RETAILMSG(TRUE, (TEXT("find adapter!")));

                memcpy(dev_nm,  &buf[pQueryBinding->DeviceNameOffset ],
                              pQueryBinding->DeviceNameLength ); 
                break;
                }
        }else{
            RETAILMSG(TRUE, (TEXT("can not find adapter!")));
            break;
        }
    }

    if (0 == DeviceIoControl(file, 
                                        IOCTL_NDISUIO_OPEN_DEVICE,
                                        dev_nm,
                                        wcslen(dev_nm) * sizeof(TCHAR),
                                        NULL,
                                        0,
                                        &dwWrittenBytes,
                                        NULL)) 
    {
        RETAILMSG(TRUE, (TEXT("DeviceIoControl failed %d!"), GetLastError()));
        return;
    }

    
   NDIS_query_ndis_stats(file);
   NDIS_dump_smsc_reg(file);
    
    
   if(CloseHandle(file) == FALSE)
   {
      RETAILMSG(TRUE, (TEXT("CloseHandle() failed (Error: %d)\n"), GetLastError()));
   }

    return;

}


