
#ifndef Collection_HH
#define Collection_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
// Filename:  Collection -- This contains definitions of the collection globals.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Foundation/vcssrc/Collection.hhv   25.0.4.0   19 Nov 2013 14:03:44   pvcs  $
//
// Modifications
//
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//=====================================================================

//@ Usage-Class
//@ End-Usage

//@ Type:  EquivResolution
// Equivalence resolution between nodes in lists of references.
enum EquivResolution
{
  EQUIV_OBJECTS,	// test equivalence of the referenced objects...
  EQUIV_REFERENCES	// test equivalence of the references...
};

//@ Type:  SearchDirection
// Direction indicators for searching linked lists.
enum SearchDirection
{
  FORWARD,		// search in a forward direction
  BACKWARD		// search in a reverse direction
};

//@ Type:  SearchFrom
// Starting point indicators for searching linked lists.
enum SearchFrom
{
  FROM_FIRST,		// start searching from the FIRST node
  FROM_CURRENT,		// start searching from the CURRENT node
  FROM_LAST		// start searching from the LAST node
};

//@ Type:  InsertionPlace
// Where to insert a new item relative to the first item.
enum InsertionPlace
{
  AFTER_CURRENT,	// insert immediately AFTER the current node
  BEFORE_CURRENT	// insert immediately BEFORE the current node
};


#endif  // Collection_HH
