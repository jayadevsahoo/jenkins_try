// ClientSocket.cpp : implementation file
//

#include "stdafx.h"
#include "ClientSocket.h"
#include "UserMessages.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//#define	VERIFY_CHECKSUM	1

/////////////////////////////////////////////////////////////////////////////
// CClientSocket

CClientSocket::CClientSocket()
{
	DataRX = 0;
	NextRX = 0;
//Receive initializations
	memset((char *) &si_other, 0, sizeof(si_other));
	si_other.sin_family = AF_INET;
    si_other.sin_addr.s_addr = htonl(INADDR_ANY);
    si_other.sin_port = htons(TOOL_ETHERNET_PORT_UDP);
}

CClientSocket::~CClientSocket()
{
	Close();
}


// Do not edit the following lines, which are needed by ClassWizard.
#if 0
BEGIN_MESSAGE_MAP(CClientSocket, CSocket)
	//{{AFX_MSG_MAP(CClientSocket)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()
#endif	// 0

/////////////////////////////////////////////////////////////////////////////
// CClientSocket member functions
#if 0
   //Testing code 
DWORD StartTime = 0;
DWORD EndTime = 0;
#endif

void CClientSocket::OnReceive(int nErrorCode) 
{
	// TODO: Add your specialized code here and/or call the base class
#if 0
   //Testing code 
   StartTime = GetTickCount();
   DWORD Result =  StartTime - EndTime;
   TRACE("StartTime - EndTime  : %d\n",Result);
#endif

	CString strSendersIp;
	UINT uSendersPort =0;
	char *pRXBuff;
	int *pRXSize;
	int nByteRead;

	if(NextRX == 1)
	{
		pRXBuff = RXBuff2;
		pRXSize = &nRead2;
	}
	else
	{
		pRXBuff = RXBuff;
		pRXSize = &nRead;
	}

	// Could use Receive here if you don't need the senders address & port
	int slen = sizeof(sockaddr_in);
	nByteRead = recvfrom(this->m_hSocket, pRXBuff, 1000, 0, (struct sockaddr *) &si_other, &slen);
#if 0
	//Testing code 
	TRACE("text : %s - SenderPort:- %d \n",pRXBuff,uSendersPort);
#endif

	pDlg->SendMessage(WM_CHECK_APP_BUTTONS, 0, 0);

	switch (nByteRead)
	{
		case 0:       // Connection was closed.
			Close();      
			break;
		case SOCKET_ERROR:
			if (GetLastError() != WSAEWOULDBLOCK) 
			{
				AfxMessageBox (L"RX Error occurred");
				Close();
				return;
			}
			break;
		 default: // Normal case: Receive() returned the # of bytes received.
			 pRXBuff[nByteRead] = 0;
			 break;
	}
  
#if NETWORK_BYTE_ORDER

	//int MsgLongSize = (nByteRead / 4) - 1;
	int MsgLongSize = nByteRead / 4;	// assume that checksum also needs to be swapped
	UINT *pMsg = (UINT *)pRXBuff;
	for(int i=0; i<MsgLongSize; i++)
	{
		*pMsg = htonl(*pMsg);
		pMsg++;
	}

#endif

#if VERIFY_CHECKSUM	
	if(uSendersPort != DEVICE_ETHERNET_PORT_UDP)
#else
	if(1)
#endif
	{
		*pRXSize = nByteRead;
		DataRX = NextRX;
		NextRX = (NextRX + 1) % 2;
		pDlg->PostMessageW(WM_ETHERNET_DATA, (WPARAM)pRXSize, (LPARAM)pRXBuff);
	}
	else
	{
		UINT Cs = 0;
		char *pTemp = pRXBuff;

		for(UINT i=0; i < (UINT)(nByteRead - 4); i++)
		{
			Cs += *pTemp++;
		}

		UINT *pCs = (UINT *)(pRXBuff + (nByteRead - 4));

		if(Cs == *pCs)
		{
			*pRXSize = nByteRead;
			DataRX = NextRX;
			NextRX = (NextRX + 1) % 2;
			pDlg->PostMessageW(WM_ETHERNET_DATA, (WPARAM)pRXSize, (LPARAM)pRXBuff);
		}
		else
		{
			AfxMessageBox (L"Checksum mismatched");
		}
	}

#if 0
   	//Testing code
   EndTime = GetTickCount();
   Result = EndTime - StartTime;
   TRACE("EndTime - StartTime : %d\n",Result);	
#endif  
   CSocket::OnReceive(nErrorCode);
}


bool CClientSocket::GetRXData(char *pBuff, int *pRXSize)
{
	if(DataRX == 0)	// from first buffer
	{
		pBuff = RXBuff;
		*pRXSize = nRead;
	}
	else // from second buffer
	{
		pBuff = RXBuff2;
		*pRXSize = nRead2;
	}
	return true;
}

