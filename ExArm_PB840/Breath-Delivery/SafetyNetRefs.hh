#ifndef SafetyNetRefs_HH
#define SafetyNetRefs_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: SafetyNetRefs - External references for the BD run time safety
//  net tests.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/SafetyNetRefs.hhv   25.0.4.0   19 Nov 2013 14:00:06   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By: syw   Date:  21-Mar-2000    DR Number: 5611
//  Project:  NeoMode
//	Description:
//		Eliminate RSvSwitchSideRangeTest reference.
//
//  Revision: 002  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 001  By:  by   Date:  03-Aug-1995    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Initial version.
//====================================================================

// SafetyNetTestMediator
class SafetyNetTestMediator;
extern SafetyNetTestMediator& RSafetyNetTestMediator;

// InspPressureSensorTest
class InspPressureSensorTest;
extern InspPressureSensorTest& RInspPressureSensorTest;

// ExhPressureSensorTest
class ExhPressureSensorTest;
extern ExhPressureSensorTest& RExhPressureSensorTest;

// AtmPressureRangeTest
class AtmPressureRangeTest;
extern AtmPressureRangeTest& RAtmPressureRangeTest; 

// O2PsolStuckOpenTest
class O2PsolStuckOpenTest;
extern O2PsolStuckOpenTest& RO2PsolStuckOpenTest; 

// AirPsolStuckOpenTest
class AirPsolStuckOpenTest;
extern AirPsolStuckOpenTest& RAirPsolStuckOpenTest; 

// O2PsolStuckTest
class O2PsolStuckTest;
extern O2PsolStuckTest& RO2PsolStuckTest; 

// AirPsolStuckTest
class AirPsolStuckTest;
extern AirPsolStuckTest& RAirPsolStuckTest; 

// AirFlowSensorTest 
class AirFlowSensorTest;
extern AirFlowSensorTest& RAirFlowSensorTest; 

// O2FlowSensorTest 
class O2FlowSensorTest;
extern O2FlowSensorTest& RO2FlowSensorTest; 

// SvClosedLoopbackTest
class SvClosedLoopbackTest;
extern SvClosedLoopbackTest& RSvClosedLoopbackTest; 

// ExhFlowSensorFlowTest
class ExhFlowSensorFlowTest;
extern ExhFlowSensorFlowTest& RExhFlowSensorFlowTest; 

// PiStuckTest
class PiStuckTest;
extern PiStuckTest& RPiStuckTest;

// PeStuckTest
class PeStuckTest;
extern PeStuckTest& RPeStuckTest;

// SvOpenedLoopbackTest
class SvOpenedLoopbackTest;
extern SvOpenedLoopbackTest& RSvOpenedLoopbackTest;

#endif // SafetyNetRefs_HH 



