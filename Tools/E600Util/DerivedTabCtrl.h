#pragma once

#include "TabOne.h"
#include "TabTwo.h"
#include "TabThree.h"
#include "TabFour.h"
#include "TabFive.h"
#include "TabSix.h"

// CDerivedTabCtrl

#define NUM_CTABS	6

class CDerivedTabCtrl : public CTabCtrl
{
public:
	CDerivedTabCtrl();
	virtual ~CDerivedTabCtrl();
	void Init();
	void SetRectangle();
	void SelectTab(int nextTab);
	int GetSelectedTab();

	CDialog *m_tabPages[NUM_CTABS];
	int m_tabCurrent;
	int m_nNumberOfPages;
	CDialog * pDlg;
	CTabOne *pTabOne;
	CTabTwo *pTabTwo;
	CTabThree *pTabThree;
	CTabFour *pTabFour;
	CTabFive *pTabFive;
	CTabSix *pTabSix;

	CStatusBar *pStatusBar;

protected:
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg LRESULT OnEthMsgUpdateWindow(WPARAM x, LPARAM y);
	afx_msg LRESULT OnCalMsgUpdateWindow(WPARAM x, LPARAM y);
	afx_msg LRESULT OnAllGainsReceived(WPARAM x, LPARAM y);
	afx_msg LRESULT OnShutdown(WPARAM x, LPARAM y);
	afx_msg LRESULT OnSettingsReply(WPARAM x, LPARAM y);

	DECLARE_MESSAGE_MAP()
};
