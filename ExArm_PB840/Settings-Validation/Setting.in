
#ifndef Setting_IN
#define Setting_IN

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//          Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
// Class:  Setting - Setting Class.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/Setting.inv   25.0.4.0   19 Nov 2013 14:27:40   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005 By: rhj    Date: 04-Dec-2006   DR Number: 6291
//  Project:  RESPM
//	Description:
//      Fixed a small bug in setDisableNovRamUpdate().
//
//  Revision: 004 By: rhj    Date: 01-Dec-2006   DR Number: 6291
//  Project:  RESPM
//	Description:
//      Added functionality to prevent settings getting stored into NOVRAM. 
//
//  Revision: 003   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  moved 'getId()' and the forced-change-flag methods and data
//	   items up to the 'SettingSubject' base class
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//  	Initial version.
//
//=====================================================================

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//================ M E T H O D   D E S C R I P T I O N ==================
//@ Method:  getBoundStatus()  [const]
//
//@ Interface-Description
//  Returns a constant reference to this setting's bound status.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

inline const BoundStatus&
Setting::getBoundStatus(void) const
{
  CALL_TRACE("getBoundStatus()");
  return(boundStatus_);   // $[TI1]
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//================ M E T H O D   D E S C R I P T I O N ==================
//@ Method:  getBoundStatus_()
//
//@ Interface-Description
//  Returns non-constant reference to this setting's bound status.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

inline BoundStatus&
Setting::getBoundStatus_(void)
{
  CALL_TRACE("getBoundStatus_()");
  return(boundStatus_);    // $[TI1]
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  StoreActivatedSetting_(settingId, settingValue)  [static]
//
//@ Interface-Description
//  Store the ID and value -- given by 'settingId' and 'settingValue' --
//  for later retrieval upon the operator's activation of the CLEAR key.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

inline void
Setting::StoreActivatedSetting_(const SettingId::SettingIdType settingId,
			        const SettingValue&            settingValue)
{
  CALL_TRACE("StoreActivatedSetting_(settingId, settingValue)");
  Setting::RStoredSetting_.settingId    = settingId;
  Setting::RStoredSetting_.settingValue = settingValue;
}   // $[TI1]


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  GetStoredSettingId_()  [static]
//
//@ Interface-Description
//  Return the ID of the the last activated (by the operator) setting.
//  This is the ID stored via 'StoreActivatedSetting_()'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

inline SettingId::SettingIdType
Setting::GetStoredSettingId_(void)
{
  CALL_TRACE("GetStoredSettingId_()");
  return(Setting::RStoredSetting_.settingId);
}   // $[TI1]


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  GetStoredSettingValue_()  [static]
//
//@ Interface-Description
//  Return the value of the the last activated (by the operator) setting.
//  This is the value stored via 'StoreActivatedSetting_()'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

inline SettingValue
Setting::GetStoredSettingValue_(void)
{
  CALL_TRACE("GetStoredSettingValue_()");
  return(Setting::RStoredSetting_.settingValue);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  setDisableNovRamUpdate(Boolean isNovRamUpdateDisabled)
//
//@ Interface-Description
//  Set the isNovRamUpdateDisabled_ flag.  If true, it will prevent this 
//  setting id getting stored into NOVRAM.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Sets the isNovRamUpdateDisabled_ flag.
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

inline void
Setting::setDisableNovRamUpdate(Boolean isNovRamUpdateDisabled)
{
  CALL_TRACE("setDisableNovRamUpdate(Boolean isNovRamUpdateDisabled)");
  isNovRamUpdateDisabled_ = isNovRamUpdateDisabled;
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  isNovRamUpdateDisabled(void)
//
//@ Interface-Description
//  Is the setting id is currently disable from updating to NOVRAM. 
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns isNovRamUpdateDisabled_.
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

inline Boolean
Setting::isNovRamUpdateDisabled(void)
{
  CALL_TRACE("isNovRamUpdateDisabled(void)");

  return isNovRamUpdateDisabled_;
}   // $[TI1]


#if defined(SIGMA_DEVELOPMENT)

//=====================================================================
//
//  Friend Function...
//
//=====================================================================

//============ M E T H O D   D E S C R I P T I O N ====================
// Method:  operator<<(ostr, setting)  [friend]
//
// Interface-Description
//  Dumps out the contents of 'setting' to 'ostr'.
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

inline Ostream&
operator<<(Ostream& ostr, const Setting& setting)
{
  CALL_TRACE("::operator<<(ostr, setting)");
  return(setting.print_(ostr));
}

#endif  // defined(SIGMA_DEVELOPMENT)


#endif // Setting_IN 
