#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================


// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: NumericField - This is a Drawable object which displays a numeric
// value.
//---------------------------------------------------------------------
//@ Interface-Description
// The NumericField class manages the display of numbers on the screen.  The
// application code specifies the height of the numbers (font size), the
// maximum number of digits, and the justification (LEFT, RIGHT, or CENTER) at
// construction time.  Methods are provided to allow the application to set the
// value to be displayed, the precision, and italic or normal style.
//
// The NumericField class is derived from the Drawable class, and thus is
// "aware" of the screen management framework.
//
// At construction time, the initial width and height of the screen area
// (inherited from Drawable) are calculated from the maximum number of digits
// and the size of the digits being used.  Following construction, the
// application can change the area's size as needed.  As a result, it is not
// guaranteed that the numeric value will fit.
//---------------------------------------------------------------------
//@ Rationale
// The NumericField class encapsulates several display-related functions,
// including the conversion of numbers into strings, rounding, controlling font
// and style, and positioning the value within the field.
//
// This class is not derived from the TextField class, because it differs from
// the TextField class in several important ways. It maintains a fixed field
// size based on the initial font size and the MAX number of digits,
// rather than adjusting its size to its contents.  It does not need to
// deal with the internationalization factors which have to be considered when
// displaying text.
//
// The reason why the field size must be fixed is that the NumericField must be
// capable of aligning its contents to the right, left, or center of the field.
// Note that the displayed value is always centered vertically within the
// field.
//---------------------------------------------------------------------
//@ Implementation-Description
// Internally this class stores the number to be displayed as a floating-point
// value.  When it is rendered, the value is rounded to the nearest value that
// fits within the specified precision.  This rounding is performed by the
// sprintf() function, using the standard rounding method for "f" format.
//
// It is up to the application at construction time to set the numberOfDigits
// of the NumericField object large enough to take the biggest number that the
// application is going to send.  The height of the field is set to the height
// of the font, excluding descenders (because the numbers do not have
// descenders).  The width of the field is initialized to the width of a "4"
// digit (the widest digit) in italic style (the widest style) times the
// maximum number of digits + 1.  If that width is not large enough, then the
// number will extend beyond it.  Any portion extending outside the field's
// area may not be refreshed correctly by the screen management framework when
// the screen content changes.
//
// NumericField uses routines from the VGA-Graphics-Driver subsystem to draw
// itself.  It accepts calls to its draw() method from the screen management
// framework.  Changes to a NumericField will not result in changes on the
// screen until the application calls the BaseContainer::repaint() method,
// signaling the framework to update the screen.
//---------------------------------------------------------------------
//@ Fault-Handling
// This class uses the "contract programming" technique.  This technique
// utilizes the class assertion and class precondition macros provided by
// the Utilities subsystem to generate error messages in response to software
// faults.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Foundations/vcssrc/NumericField.ccv   25.0.4.0   19 Nov 2013 14:11:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 020   By: gdc    Date:  01-Jun-2007    SCR Number: 6237
//  Project: Trend
//  Description:
//	Improved font metrics.
// 
//  Revision: 019   By: gdc    Date:  21-May-2007    SCR Number: 6330
//  Project: Trend
//  Description:
//	Removed SUN prototype code.
// 
//  Revision: 018   By: gdc    Date:  09-Oct-2000    DCS Number: 5753
//  Project: Delta
//  Description:
//	Implemented Single Screen option.
//
//  Revision: 017   By: gdc    Date:  29-Jun-1999    DCS Number: 5337
//  Project: 840 Neonatal
//  Description:
//	Optimized VGA library for Neonatal project to use BitBlt and Windows
//  Bitmap structures.
//
//  Revision: 016   By: sah    Date: 11-Jan-1999   DR Number: 5321
//  Project:  Sigma (R8027)
//  Description:
//      Changed 'SoftFault()' method to non-inline method.
//
//  Revision: 015  By: gdc     Date: 16-Jun-1998     DCS Number: 5055
//  Project:  Color
//  Description:
//      Initial release. Changed in conjunction with implementation of
//		Stdio.cc to correct sprintf() rounding problems.
//
//  Revision: 014  By:  hhd	   Date:  06-Oct-97    DR Number: 2540 
//    Project:  Sigma (R8027)
//    Description:
//		Removed code that supports commas in floating point format.
//
//  Revision: 013  By: sah       Date: 06-Oct-1997    DCS Number: 2264
//  Project:  Sigma (R8027)
//  Description:
//      Fixed incorrect handling of VGA access requests/releases, which
//      may have caused the problem with the errant lines outside of the
//      waveform grid, by removing all semaphore requests/releases from
//      all of the 'draw()' methods.
//
//  Revision: 012	By:  hhd	   Date:  24-Sep-97    DR Number: 2512
//    Project:  Sigma (R8027)
//    Description:
//		Fixed further repaint problem with IBW's values.	
//
//  Revision: 011	By:  hhd	   Date:  18-Sep-97    DR Number: 2512
//    Project:  Sigma (R8027)
//    Description:
//		Set the height of NumericField based on value queried from the TextFont class.
//
//  Revision: 010  By: hhd		Date: 18-Aug-1997    DCS Number: 2231
//    Project:  Sigma (R8027)
//    Description:
//		Included height of decenders when computing height for languages that
//		use commas instead of decimal points.  Also, convert to commas before 
//		displaying such values. 
//
//  Revision: 009  By: sah       Date: 12-Jun-1997    DCS Number: 2208
//    Project:  Sigma (R8027)
//    Description:
//      Incorporated new VGA interface methods, files and types.
//
//  Revision: 008  By: yyy      Date: 18-Feb-1997  DCR Number: 761
//    Project:  Sigma (R8027)
//    Description:
//      Fixed the rounding problem when we passed a negative value to numberic field.
//
//  Revision: 007  By: mpm      Date: 29-Mar-1996  DCR Number: 473
//    Project:  Sigma (R8027)
//    Description:
//      Added calls to GuiFoundation::RequestDisplayAccess() and ReleaseDisplayAccess()
//      in order to allow shared access to the display by separate drawing tasks.
//
//  Revision: 006  By: clw      Date: 13-Mar-1996  DCR Number: 717
//    Project:  Sigma (R8027)
//    Description:
//      Fixed the 0.5 to 0.2 rounding for the target too (duh).
//
//  Revision: 005  By: yyy      Date: 28-Feb-1996  DCR Number: 717
//    Project:  Sigma (R8027)
//    Description:
//      Changed the rounding from .5 to .2 as printf seems to be rounding when
//      no decimal places are displayed but truncated otherwise.
//
//  Revision: 004  By: gdc      Date: 15-Feb-1996  DR Number: 674
//    Project:  Sigma (R8027)
//    Description:
//	Removed ifndef / endif around include directives
//
//  Revision: 003  By: gdc      Date: 15-Feb-1996  DR Number: 981
//    Project:  Sigma (R8027)
//    Description:
//	Corrected minor syntax errors detected by new compiler.
//
//  Revision: 002  By: mpm      Date: 14-Feb-1996   DCR Number: 717
//    Project:  Sigma (R8027)
//    Description:
//      Rounded numeric values before display as per DCS 717.
//
//  Revision: 001  By: yyy      Date: 01-Jan-1995  DR Number: NONE
//    Project:  Sigma (R8027)
//    Description:
//	Integration baseline.
//=====================================================================

#include "NumericField.hh"
#include "MathUtilities.hh"
#include <string.h>

//@ Usage-Classes
#include "BaseContainer.hh"
#include "Container.hh"
#include "GraphicsContext.hh"
#include "DisplayContext.hh"
#include "VgaFontData.hh"
#include "VgaRectangle.hh"
//@ End-Usage


//@ Code...


const Int NumericField::MAX_NUMERIC_STRING_LENGTH = 10;

//=====================================================================
//
// Public methods
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: NumericField
//
//@ Interface-Description
// This is the constructor for the NumericField class.  The "size" parameter
// specifies the desired font size; legal sizes are defined in the Size enum in
// the TextFont class header.  The "numberOfDigits" parameter indicates the
// MAXIMUM number of digits that will be displayed.  The "alignment" parameter
// indicates if the displayed value is to be left-justified, right-justified,
// or centered.  Note that the Allignment type is defined in the Area class
// header.
//
// The "numberOfDigits" is used to determine the initial width of the field; it
// does not take into account additional characters such as sign and decimal
// point.
//
// Note that the numeric value to be displayed is initialized to 0, the
// precision is initialized to 0, and the italic style attribute is
// initialized to FALSE.  A precision of 0 means there are no places to the
// right of the decimal; in this case, no decimal is displayed.
//---------------------------------------------------------------------
//@ Implementation-Description
// The maximum possible width of the field is calculated from the maximum
// possible number of digits. setWidth() and setHeight() are then called
// with those dimensions; this sizes the area_ inherited from Drawable.
// This method collaborates with the TextFont class to figure out the
// height and width of a digit.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

NumericField::NumericField (TextFont::Size size, Uint numberOfDigits, 
								Alignment alignment) :
	pointSize_(size),
	value_(0.0),
	numberOfDigits_(numberOfDigits),
	precision_(ONES),
	alignment_(alignment),
	isItalic_(FALSE)	
{
	CALL_TRACE("NumericField::NumericField (TextFont::Size size, "
					"Uint numberOfDigits, Alignment alignment)");

													// $[TI1]
	// Note that the style is changable by the application via a public method,
	// so even if the field is not currently in italic style, it might be
	// later.  By contrast, the size is not publically changeable.  The height
	// determined here does not include descenders.  
	Int height;
	Int width;

	TextFont::GetStringSize(L"4",size,TextFont::ITALIC,width,height);

	// Set the size of the area_ member, inheritied from parent class Drawable:
	setWidth((numberOfDigits + 1) * width);

	setHeight(height);
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~NumericField
//
//@ Interface-Description
//      Destructor - Destroys object.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This destructor does nothing.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

NumericField::~NumericField(void)
{
	CALL_TRACE("NumericField::~NumericField(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setValue
//
//@ Interface-Description
// This function sets the value which is to be displayed on the screen to the
// passed "value" parameter.  It is specified and stored internally as a
// floating-point value.
//---------------------------------------------------------------------
//@ Implementation-Description
// This function merely copies the value passed in into the private value_
// field.
//
// Since this method affects the appearance of the object, the changing()
// method inherited from Drawable must be called to ensure that the object
// is redrawn at the next repaint.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
NumericField::setValue(Real32 value)
{
	CALL_TRACE("NumericField::setValue(Real32 value)");

	changing();		// We're changing our appearance
    value_ = value;									// $[TI1]

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setItalic
//
//@ Interface-Description
// This function allows the user to change the number to italic or "normal"
// typeface.  If the passed boolean "isItalic" parameter is true, the italic
// style is selected, otherwise the normal style.  The "thin" typeface is not
// an option.  Note that "normal" is actually bold.
//---------------------------------------------------------------------
//@ Implementation-Description
// This method simply sets the private isItalic_ attribute to the passed value.
// Since this method affects the appearance of the object, the changing()
// method inherited from Drawable is called to ensure that the object is
// redrawn at the next repaint.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
NumericField::setItalic(Boolean isItalic)
{
	CALL_TRACE("NumericField::setItalic(Boolean isItalic)");

	changing();		// We're changing our appearance
    isItalic_ = isItalic;								// $[TI1]
    
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setFontSize
//
//@ Interface-Description
//  Simply sets the font size of this numeric field.
//---------------------------------------------------------------------
//@ Implementation-Description  
//  Sets the class member pointSize_ from the given parameter.
//  Since this method affects the appearance of the object, the changing()
//  method inherited from Drawable is called to ensure that the object is
//  redrawn at the next repaint.
//---------------------------------------------------------------------
//@ PreCondition
//	Must have a point size greater than TextFont::NO_SIZE and less than 
//  or equal to TextFont::MAX_SIZE.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
NumericField::setFontSize(TextFont::Size pointSize)
{
	CALL_TRACE("NumericField::::setFontSize(TextFont::Size pointSize)");

    AUX_CLASS_ASSERTION( (pointSize > TextFont::NO_SIZE && pointSize <= TextFont::MAX_SIZE), pointSize );

	changing();		// We're changing our appearance
	pointSize_  = pointSize;	
    
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setPrecision
//
//@ Interface-Description
// This function sets the number of places after the decimal to the passed
// "precision" parameter.
//---------------------------------------------------------------------
//@ Implementation-Description
// This method simply sets the private precision_ attribute to the passed
// value.  Since this method affects the appearance of the object, the
// changing() method inherited from Drawable must be called to ensure that the
// object is redrawn at the next repaint.
//
// Note that the Precision type is defined in the Utilities subsystem.
//---------------------------------------------------------------------
//@ PreCondition
// The precision value must be within valid limits.
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
NumericField::setPrecision(Precision precision)
{
	CALL_TRACE("NumericField::setPrecision(Precision precision)");
    CLASS_PRE_CONDITION((precision <= THOUSANDS)
    								 && (precision >= THOUSANDTHS));

	changing();
    precision_ = precision;								// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: render
//
//@ Interface-Description
// Draws the numeric field on the screen.  The passed clipArea parameter is
// ignored.
//---------------------------------------------------------------------
//@ Implementation-Description
// The floating point number is converted to a string. Then the realArea 
// is computed. Then the areaToDraw is
// aligned within the realArea.  The text rendering routine uses the areaToDraw
// to determine where to locate the characters.
//
// This method uses routines provided by the VGA-Graphics-Driver subsystem
// to render the string (containing the value) onto the screen.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
 
void
NumericField::render(wchar_t * pString)
{
	CALL_TRACE("render(void)");

    DisplayContext & rDc = getBaseContainer()->getDisplayContext();
    GraphicsContext & rGc = getBaseContainer()->getGraphicsContext();

	Area realArea = area_;
	realArea.setX(getRealX());
	realArea.setY(getRealY());
 
	// Now figure out width and height that the number is going to be.  We
	// locate the text at TextFont::VGA_X_START, TextFont::VGA_Y_START, so we are
	// guaranteed that the calculations stay positive.

	VgaRect r = {0,0,0,0};  
	Area areaToDraw;

	VgaFontData::TextRect(TextFont::VGA_X_START, TextFont::VGA_Y_START,  
				(Int16)getFont_(), pString, &r);

#if defined SIGMA_DEBUG
	// Check to ensure that the value fits within the area_ of the field
	// If text does not fit, it will be clipped in low-level text rendering
	// routine in the VGA-Graphics-Driver subsystem.
	if (getWidth() < (r.x1 - r.x0 + 1))
	{
		printf("NumericField::draw() value=%s, area_.getWidth()=%d is less than"
				" rectangle's range: %d, %d\n",
				pString, getWidth(), r.x0, r.x1);
	}

	if (getHeight() < (r.y1 - r.y0 + 1))
	{
		printf("NumericField::draw() value=%s, area_.getHeight()=%d y0=%d y1=%d clipY0=%d clipY1=%d\n",
				pString, getHeight(), r.y0, r.y1, rGc.getClipY0(), rGc.getClipY1());
	}
#endif	//	SIGMA_DEBUG

	// This call sets the areaToDraw (it is pass-by-reference) based on the
	realArea.alignArea(r.x1 - r.x0 + 1, r.y1 - r.y0 + 1,
												alignment_, areaToDraw);

	// Note that the Y value we pass is the bottom of the areaToDraw. This
	// is because the text is drawn on this baseline.
	rGc.setForegroundColor( getColor() );
	rDc.setTextColor(Colors::toStandardColor(getColor()));
	rDc.drawText(rGc, areaToDraw.getX1(), areaToDraw.getY2()+1, 
	                 (Int16)getFont_(), pString);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: draw	[virtual]
//
//@ Interface-Description
// Draws the numeric field on the screen.  The passed clipArea parameter is
// ignored.
//---------------------------------------------------------------------
//@ Implementation-Description
// There are three parts to this function. First the floating point number is
// converted to a string. Then the realArea is computed. Then the areaToDraw is
// aligned within the realArea.  The text rendering routine uses the areaToDraw
// to determine where to locate the characters.
//
// This method uses routines provided by the VGA-Graphics-Driver subsystem
// to render the string (containing the value) onto the screen.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
 
void
NumericField::draw(const Area &)
{
	CALL_TRACE("draw(const Area &)");
	
    wchar_t valueString[MAX_NUMERIC_STRING_LENGTH];

    Int32 count = swprintf(valueString, L"%.*f", DecimalPlaces(precision_), value_);

	CLASS_ASSERTION(count < sizeof(valueString) );

	render( valueString );
}


//=====================================================================
//
// Private methods
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getFont_
//
//@ Interface-Description
// This function figures out what font to use to render the number based on
// whether italic is set and what size the digits are.
//---------------------------------------------------------------------
//@ Implementation-Description
// The font is determined by the pointSize_ and isItalic_ attributes.
// This method uses with the TextFont class to determine the font.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

FontID
NumericField::getFont_(void) const
{
	CALL_TRACE("NumericField::getFont_(void)");

	FontID font;
	if (isItalic_)
	{	//												$[TI1]
		font = TextFont::GetFont(pointSize_, TextFont::ITALIC);
	}
	else
	{	//												$[TI2]
		font = TextFont::GetFont(pointSize_, TextFont::NORMAL);
	}

	CLASS_ASSERTION(font != TextFont::INVALID_FONT_ID);
   
	return (font);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
NumericField::SoftFault(const SoftFaultID  softFaultID,
		   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_FOUNDATIONS, NUMERIC_FIELD,
							lineNumber, pFileName, pPredicate);
}
