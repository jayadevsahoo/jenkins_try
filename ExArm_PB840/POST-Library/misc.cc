#include "stdafx.h"
#ifdef SIGMA_DEBUG
// ****************************************************************************
//
//  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
// *****************************************************************************

#include <stdarg.h>
#include <stdio.h>
#include <string.h>

#include "kernel.hh"
#include "kernex.hh"
#include "misc.hh"
#include "CpuDevice.hh"

#define  P_KERNEL_START ((EntryPtr)0xFF000400)

int   diag_port;
int   other_port;

unsigned char add_carriage_return = 1;

volatile SERIAL_PORT   port[NUMBER_OF_SERIAL_PORTS];


//-----------------------------------------------------------------------------
void  InitializeSerialPorts(void)
{
   InitializeDiagnosticInterface(SERIAL_PORT_A);
   InitializeDiagnosticInterface(SERIAL_PORT_B);

   diag_port  = SERIAL_PORT_A;
   other_port = SERIAL_PORT_B;
}


//--------------------------------------------------------------------------
void  InitializeDiagnosticInterface(int port_number)
{
   //initialize the internal data structures
   memset((void *)&port[port_number], 0 , sizeof(port[port_number]));
   ResetSerialPort(port_number);
   write_85230(port_number, 2, 80); // set the vector register
   write_85230(port_number, 1, port[port_number].interrupt_register = 0);

   write_85230(port_number, 15, 1); //access wr7 prime
   write_85230(port_number,  7, 0x0);
//   write_85230(port_number,  7, 0x24);
   write_85230(port_number, 15, 0); //exit access wr7 prime

   InitializeDiagnosticPort(port_number);
   port[port_number].options.interrupt_driven = 0;
}




//-----------------------------------------------------------------------------
int dprintf(char *fmt, ...)
{
   static char buffer[8192];
   va_list argptr;
   int cnt;

   va_start(argptr, fmt);
   cnt = vsprintf(buffer, fmt, argptr);
   va_end(argptr);
   send_string(buffer);
   return(cnt);
}

//-----------------------------------------------------------------------------
int dprintf_b(char *fmt, ...)
{
   static char buffer[8192];
   va_list argptr;
   int cnt;

   va_start(argptr, fmt);
   cnt = vsprintf(buffer, fmt, argptr);
   va_end(argptr);
   send_string_b(buffer);
   return(cnt);
}

//--------------------------------------------------------------------------
void  send_string(char *string)
{
   if(port[diag_port].options.interrupt_driven)
   {
      while(*string)
         send_char(*string++);
   }
   else
   {
      SendString(diag_port, string);
   }     
}

//--------------------------------------------------------------------------
void  send_string_b(char *string)
{
   if(port[other_port].options.interrupt_driven)
   {
      while(*string)
         send_char_b(*string++);
   }
   else
   {
      SendString(other_port, string);
   }     
}

//--------------------------------------------------------------------------
void  send_string_direct(char *string)
{
   SendString(diag_port, string);
}

//--------------------------------------------------------------------------
char  send_a_char(char ch, int port_number)
{
   volatile SERIAL_PORT *p = &port[port_number];
   if(p->options.interrupt_driven)
   {
      if(p->transmit_insert_index == p->transmit_remove_index)
      {
         //transmit buffer is empty - send it now
         DISABLE_INTERRUPTS();
         if(SendIsReady(port_number))
         {
            SendChar(port_number, ch);
            ENABLE_INTERRUPTS();
            return(ch);
         }
         ENABLE_INTERRUPTS();
      }

      int insert = (p->transmit_insert_index + 1) % SERIAL_BUFFER_SIZE;
      if(insert == p->transmit_remove_index)
      {
         //buffer is full
         while(insert == p->transmit_remove_index)
            ;  //wait for space in the buffer
      }

      //put the character into the buffer
      p->transmit_buffer[p->transmit_insert_index] = ch;
      p->transmit_insert_index = insert;
   }
   else
   {
      SendChar(port_number, ch);
   }
   return(ch);
}

char  send_char(char ch)
{
   return(send_a_char(ch, diag_port));
}

//--------------------------------------------------------------------------
char  send_char_b(char ch)
{
   return(send_a_char(ch, other_port));
}

//--------------------------------------------------------------------------
char  send_char_direct(char ch)
{
   SendChar(diag_port, ch);
   return(ch);
}

//---------------------------------------------------------------------------
void  ResetSerialPort(int port_number)
{
   if(port_number == 0)
      write_85230(port_number,  9, 0x80);
   else
      write_85230(port_number,  9, 0x40);

   int i;
   int delay;

   for (i=0; i<100; i++) {delay=i*i;}
   write_85230(port_number,  9, 0x00);
   for (i=0; i<100; i++) {delay=i*i;}

}


char *const interrupt_name[256] =
{
     NULL                                   //  0 
   , NULL                                   //  1 
   , "Access Fault"                         //  2 
   , "Address Error"                        //  3 
   , "Illegal Instruction"                  //  4 
   , "Integer Divide by Zero"               //  5 
   , "CHK, CHK2 Instructions"               //  6 
   , "FTRAPcc, TRAPcc, TRAPV Inst."         //  7 
   , "Privilege Violation"                  //  8 
   , "Trace"                                //  9 
   , "Line 1010 Emulator (A Line)"          //  10
   , "Line 1111 Emulator (F Line)"          //  11
   , NULL                                   //  12
   , NULL                                   //  13
   , "Format Error"                         //  14
   , "Uninitialized Interrupt"              //  15
   , NULL                                   //  16
   , NULL                                   //  17
   , NULL                                   //  18
   , NULL                                   //  19
   , NULL                                   //  20
   , NULL                                   //  21
   , NULL                                   //  22
   , NULL                                   //  23
   , "Spurious Interrupt"                   //  24
   , "AV 1"                                 //  25
   , "AV 2"                                 //  26
   , "AV 3 = 82C54 Timer 0"                 //  27
   , "AV 4 = 82C54 Timer 1"                 //  28
   , "AV 5 = Ethernet Controller"           //  29
   , "AV 6 = 82C54 Timer 2"                 //  30
   , "AV 7 / NMI"                           //  31
   , "Trap 0 "                              //  32
   , "Trap 1 "                              //  33
   , "Trap 2 "                              //  34
   , "Trap 3 "                              //  35
   , "Trap 4 "                              //  36
   , "Trap 5 "                              //  37
   , "Trap 6 "                              //  38
   , "Trap 7 "                              //  39
   , "Trap 8 "                              //  40
   , "Trap 9 "                              //  41
   , "Trap 10"                              //  42
   , "Trap 11"                              //  43
   , "Trap 12"                              //  44
   , "Trap 13"                              //  45
   , "Trap 14"                              //  46
   , "Trap 15"                              //  47
   , "Float Branch/Set on Unordered Cond"   //  48
   , "Float Inexact Result"                 //  49
   , "Float Divide by Zero"                 //  50
   , "Float Underflow"                      //  51
   , "Float Operand Error"                  //  52
   , "Float Overflow"                       //  53
   , "Float SNAN"                           //  54
   , "Float Unimplemented Data Type"        //  55
   , NULL                                   //  56
   , NULL                                   //  57
   , NULL                                   //  58
   , NULL                                   //  59
   , NULL                                   //  60
   , NULL                                   //  61
   , NULL                                   //  62
   , NULL                                   //  63
   , "901-0 Rotary Encoder"                 //  64
   , "901-1 Touch Screen"                   //  65
   , "901-2"                                //  66
   , "901-3"                                //  67
   , "901-4  901 Timer D"                   //  68
   , "901-5  901 Timer C"                   //  69
   , "901-6"                                //  70
   , "901-7"                                //  71
   , "901-8  901 Timer B"                   //  72
   , "901-9"                                //  73
   , "901-10"                               //  74
   , "901-11"                               //  75
   , "901-12"                               //  76
   , "901-13"                               //  77
   , "901-14"                               //  78
   , "901-15 82C54 Timer 3&4"               //  79

   , "230B      Xmit Empty"                 //  80
   , "Illegal 230B"                         //  81
   , "230B   Status Change"                 //  82
   ,"Illegal 230B"                          //  83
   , "230B Rcvr char ready"                 //  84
   ,"Illegal 230B"                          //  85
   , "230B Rcvr Spec. cond"                 //  86
   , "Illegal 230B"                         //  87
   , "230A      Xmit Empty"                 //  88
   ,"Illegal 230A"                          //  89
   , "230A   Status Change"                 //  90
   ,"Illegal 230A"                          //  91
   , "230A Rcvr char ready"                 //  92
   ,"Illegal 230A"                          //  93
   , "230A Rcvr Spec. cond"                 //  94
   ,"Illegal 230A"                          //  95

   , NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL
   , NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL
   , NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL
   , NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL
   , NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL
   , NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL
   , NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL
   , NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL
   , NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL
   , NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL
   , NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL
   , NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL
   , NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL
   , NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL
   , NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL
   , NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL

};


//--------------------------------------------------------------------------
void  DebugUnexpectedInterruptHandler(char *stack_pointer)
{
   EXCEPTION_STACK_FRAME *p = (EXCEPTION_STACK_FRAME *)stack_pointer;
   int i;
   static char buffer[512];
   int vector_number;   
   unsigned short int format_and_vector_offset;
   char *program_counter;
   static char *size[4] = { "LONG","BYTE", "WORD", "LINE"};

   asm(" trap #0");    // initialize scc

   send_string_direct(
"\n\n!!!!!!!!!!!!!!!!!!!!!! Unexpected Interrupt  !!!!!!!!!!!!!!!!!!!!!!!!!!!!");

   format_and_vector_offset = p->sr_pc_vect[3];
   program_counter  = (char *) ( (p->sr_pc_vect[1] << 16) | p->sr_pc_vect[2] );
   vector_number    = (int) (format_and_vector_offset & 0xfff)/4;


   sprintf(buffer, "\nStack Pointer = 0x%X  Format: %d  Vector #%d = "
      , stack_pointer
      , (int)(format_and_vector_offset >> 12)
      , vector_number);

   send_string_direct(buffer);
   send_string_direct(interrupt_name[vector_number]);

   if( (vector_number == NMI_VECTOR_NUMBER) || (vector_number == 2))
   {
      unsigned char nmi = NMI_SOURCE;
      sprintf(buffer, "\n NMI SOURCE = %2.2X", nmi);
      send_string_direct(buffer);
      if ( CpuDevice::IsBdCpu() )
      {
         nmi <<= 1;
         sprintf(buffer, "\n           Vent Inop B %s", ((nmi <<= 1) & 0x80) ? "YES" : " NO"), send_string_direct(buffer);
         sprintf(buffer, "\n              IP Error %s", ((nmi <<= 1) & 0x80) ? "YES" : " NO"), send_string_direct(buffer);
         sprintf(buffer, "\n           Vent Inop A %s", ((nmi <<= 1) & 0x80) ? "YES" : " NO"), send_string_direct(buffer);
         sprintf(buffer, "\n     Power Supply Fail %s", ((nmi <<= 1) & 0x80) ? "YES" : " NO"), send_string_direct(buffer);
      }
      else
      {
         nmi <<= 1;
         sprintf(buffer, "\n          SAAS Failure %s", ((nmi <<= 1) & 0x80) ? "YES" : " NO"), send_string_direct(buffer);
         sprintf(buffer, "\n       Under +12 Volts %s", ((nmi <<= 1) & 0x80) ? "YES" : " NO"), send_string_direct(buffer);
         sprintf(buffer, "\n       Over + 12 Volts %s", ((nmi <<= 1) & 0x80) ? "YES" : " NO"), send_string_direct(buffer);
         sprintf(buffer, "\n       Over + 5 Volts  %s", ((nmi <<= 1) & 0x80) ? "YES" : " NO"), send_string_direct(buffer);
      }
      sprintf(buffer,    "\n      Lan Parity Error %s", ((nmi <<= 1) & 0x80) ? "YES" : " NO"), send_string_direct(buffer);
      sprintf(buffer,    "\nProcessor Parity Error %s", ((nmi <<= 1) & 0x80) ? "YES" : " NO"), send_string_direct(buffer);
   }

   send_string_direct("\n");
   for(i = 0 ; i <= 3 ; i++)
   {
      sprintf(buffer, "d%d=%8.8X ", i, p->d[i]);
      send_string_direct(buffer);
   }
   send_string_direct("\n");
   for(i = 4 ; i <= 7 ; i++)
   {
      sprintf(buffer, "d%d=%8.8X ", i, p->d[i]);
      send_string_direct(buffer);
   }
   send_string_direct("\n");
   for(i = 0 ; i <= 3 ; i++)
   {
      sprintf(buffer, "a%d=%8.8X ", i, p->a[i]);
      send_string_direct(buffer);
   }
   send_string_direct("\n");
   for(i = 5 ; i <= 6 ; i++)
   {
      sprintf(buffer, "a%d=%8.8X ", i, p->a[i]);
      send_string_direct(buffer);
   }
   sprintf(buffer,"PC=%8.8X  SR=%4.4X", program_counter, (int)p->sr_pc_vect[0]);
   send_string_direct(buffer);
   sprintf(buffer, "  Exception Vector address=%8.8X", VBR() + 4 * vector_number);
   send_string_direct(buffer);

   switch(format_and_vector_offset >> 12)
   {
      case 0:
      case 1:
      break;

      case 2: case 3:
      {
         sprintf(buffer, "\neffective address = %8.8X", p->frame.format2_3.effective_address);
         send_string_direct(buffer);
      } break;

      case 7:
      {
         sprintf(buffer,
         "\neffective_address=%8.8X"
         "   special_status_register=%4.4X"
         "\nwrite_back status  3:%2.2X"
         "   2:%2.2X"
         "   1:%2.2X"
         "\n      >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
         "\n      >>>>>>> fault_address=%8.8X  Access = %s  Size = %s  <<<<<"
         "\n      >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
         "\nwrite_back_3_address=%8.8X"
         "   write_back_3_data=%8.8X"
         "\nwrite_back_2_address=%8.8X"
         "   write_back_2_data=%8.8X"
         "\nwrite_back_1_address=%8.8X"
         "   write_back_1_data=%8.8X"
         "\npush_data_lw1=%8.8X"
         "   push_data_lw2=%8.8X"
         "   push_data_lw3=%8.8X"
         , p->frame.format7.effective_address         
         , p->frame.format7.special_status_register 
         , p->frame.format7.write_back3_status
         , p->frame.format7.write_back2_status
         , p->frame.format7.write_back1_status
         , p->frame.format7.fault_address
         , (p->frame.format7.special_status_register & 0x100) ? "READ " : "WRITE"
         , size[(p->frame.format7.special_status_register & 0x60) >> 5]
         , p->frame.format7.write_back_3_address
         , p->frame.format7.write_back_3_data
         , p->frame.format7.write_back_2_address
         , p->frame.format7.write_back_2_data
         , p->frame.format7.write_back_1_address
         , p->frame.format7.write_back_1_data
         , p->frame.format7.push_data_lw1
         , p->frame.format7.push_data_lw2
         , p->frame.format7.push_data_lw3
         );
         send_string_direct(buffer);

      } break;
      default:
      {
         sprintf(buffer, "\n!!!!!!!!! Unknown Exception Stack Frame Format  !!!!!!!!");
         send_string_direct(buffer);
      } break;
   }                                                       
   send_string_direct("\nHit any key to continue (r - restart POST) >>>");
   if(get_char_direct() == 'r')
      (*P_KERNEL_START)();
}

//--------------------------------------------------------------------------
char  get_char_direct(void)
{
   char ch;

   switch(ch = GetChar(diag_port))
   {
      case '\n': case '\r':
      case '\b':
      break;
      default:
         send_char_direct(ch);
   }
   return(ch);
}


#endif // SIGMA_DEBUG
