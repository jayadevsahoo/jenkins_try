#include "stdafx.h"

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class:  BoundedRange - Range of 'BoundedValue' Intervals.
//---------------------------------------------------------------------
//@ Interface-Description
//  This class is the base class for all of the interval range classes.
//  This class defines much of the interval-specific operations needed
//  by all of the interval ranges.  This class has methods for testing
//  the legality of a specific bounded value in relation to the interval(s)
//  managed by instances of this class, for accessing the absolute minumum
//  and maximum constraints, for "warping" a bounded value to an interval's
//  resolution value ("click"), for testing a value against this range, and
//  for incrementing and decrementing a bounded value a specified number of
//  "clicks".  (What is meant by a "click" is the unit of resolution within
//  an interval.  For example, for an interval from '0' thru '10', with a
//  resolution of '1', the valid click locations are:  0, 1, 2, ..., 9, 10.)
//
//  This class is an abstract base class, and, as such, cannot be
//  instantiated directly; instances of this class are a result of the
//  instantiation of a derived class.
//---------------------------------------------------------------------
//@ Rationale
//  This class provides a central type for managing the operations of
//  intervals.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class declares a protected pure virtual method,
//  'getIntervalValues_()', that provides a means for this class to access
//  the specific intervals defined by its derived classes.  This method
//  allows this class to concentrate on operations for a single interval,
//  while the derived classes may define multiple intervals across their
//  entire range.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/BoundedRange.ccv   25.0.4.0   19 Nov 2013 14:27:16   pvcs  $
//
//@ Modification-Log
//
//  Revision: 007  By:  mnr    Date:  28-Dec-2009    SCR Number: 6437
//  Project:  NEO
//  Description:
//		Better precision decision for interval boundaries.
//
//  Revision: 006  By:  gdc    Date:  12-Dec-2008    SCR Number: 6068
//  Project:  840S
//  Description:
//		Removed SUN prototype code.
//
//  Revision: 005   By: sah   Date:  21-Jul-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  modified to support dynamic definition of bounded ranges, so
//         that the new, circuit-based alarm limits can be changed to the
//         'OFF' value
//      *  merged all functionality of '{Single,Double,Triple,...}IntervalRange'
//         classes into here, using a combination of a linked-list of intervals,
//         and generic algorithms to process through this list
//
//  Revision: 004   By: sah   Date:  09-Jun-1999    DR Number:  5421
//  Project:  ATC
//  Description:
//	Fixed problem discovered when '9.995998' was not being rounded to
//      '10.0' for minute volume setting.  Also, enhanced accuracy of rounding
//      by checking for epsilon differences following the rounding.  This
//      extra check eliminates inaccuracies when rounding to either the
//      interval's minimum or maximum.
//
//  Revision: 003   By: dosman Date:  16-Jun-1998    DR Number: 122
//  Project:  BILEVEL
//  Description:
//	Decreasing DEFAULT_EPSILON_FACTOR_ was necessary to make resp rate warp
//	as desired.
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 002   By: sah    Date:  18-Sep-1997    DR Number: 2360
//  Project: Sigma (R8027)
//  Description:
//	It is no longer an implementation requirement that all setting
//	values reside at a resolution value within its interval, therefore
//	a new 'checkWarping' parameter is added to 'isLegalValue()'.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//=====================================================================

#include "BoundedRange.hh"

#include <math.h>

//@ Usage-Classes
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Static Data Definitions...
//
//=====================================================================

static const Real32  DEFAULT_EPSILON_FACTOR_ = 0.005f; // 0.5%...


//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  BoundedRange(pIntervalList, maxConstraintId, minConstraintId, 
//			  useEpsilonFactoring)  [Constructor]
//
//@ Interface-Description
//  Construct a bounded range using 'pIntervalList' as a pointer to
//  the first node of a linked list of interval nodes defining the full range,
//  'maxConstraintId' and 'minConstraintId' as the bound ids for the two
//  constraints, and 'useEpsilonFactoring' as a flag to activate/deactivate
//  the use of an internal epsilon factor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

BoundedRange::BoundedRange(const BoundedInterval* pIntervalList,
						   const SettingBoundId   maxConstraintId,
						   const SettingBoundId   minConstraintId,
						   const Boolean          useEpsilonFactoring)
: pIntervalList_(pIntervalList),
	EPSILON_FACTOR_((useEpsilonFactoring)
					? ::DEFAULT_EPSILON_FACTOR_
					: 0.0f)
{
	CALL_TRACE("BoundedRange(...)");

	updateIntervalList(pIntervalList);

	maxConstraintInfo_.value  = getMaxValue();
	maxConstraintInfo_.id     = maxConstraintId;
	maxConstraintInfo_.isSoft = FALSE;

	minConstraintInfo_.value  = getMinValue();
	minConstraintInfo_.id     = minConstraintId;
	minConstraintInfo_.isSoft = FALSE;
}	// $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ~BoundedRange()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this range.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

BoundedRange::~BoundedRange(void)
{
	CALL_TRACE("~BoundedRange()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  isLegalValue(boundedValue)  [const]
//
//@ Interface-Description
//  Is 'boundedValue' a "legal" value within this resolution range.
//  A bounded value is legal when it fits within one of the ranges,
//  it contains the proper precision, and the value is at a "click"
//  boundary.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

Boolean
	BoundedRange::isLegalValue(const BoundedValue& boundedValue) const
{
	CALL_TRACE("isLegalValue(boundedValue)");

	// is 'boundedValue' within the minimum and maximum range?...
	Boolean  isLegal = (boundedValue.value >= getMinValue() &&
						boundedValue.value <= getMaxValue());

	if ( isLegal )
	{	// $[TI1]
		Real32     intervalMax, intervalRes, intervalMin;
		Precision  intervalPrec;

		// get the interval information for the value of 'boundedValue'...
		getIntervalValues_(boundedValue.value, intervalMax, intervalRes,
						   intervalPrec, intervalMin, TRUE);

		if ( boundedValue.precision != intervalPrec )
		{	// $[TI1.1]
			// the precisions are different, this could be because the value of
			// 'boundedValue' is currently on a change-over point between two
			// intervals, therefore get the interval below this change-over
			// point...
			getIntervalValues_(boundedValue.value, intervalMax, intervalRes,
							   intervalPrec, intervalMin, FALSE);
		}	// $[TI1.2] -- the precisions are equivalent...

		// is the precision of 'boundedValue' equal to the fetched interval's
		// precision, AND is the value of 'boundedValue' at a "click"
		// boundary?...$[TI1.3] (TRUE)  $[TI1.4] (FALSE)...
		isLegal = (boundedValue.precision == intervalPrec  &&
				   BoundedRange::IsLegalInterval(boundedValue.value,
												 intervalRes,
												 intervalPrec,
												 intervalMin));
	}	// $[TI2] -- 'boundedValue' is NOT within the absolute range...

	return(isLegal);
}


//=====================================================================
//@ Method:  updateIntervalList(pNewIntervalList)
//
//@ Interface-Description
//  Change this bounded range's linked list of intervals to the list
//  pointed to by 'pNewIntervalList'.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Update the 'pLastNode_' data member for the new list.
//---------------------------------------------------------------------
//@ PreConditions
//  (pNewIntervalList != NULL  &&  pNewIntervalList->pNext != NULL)
//---------------------------------------------------------------------
//@ PostConditions
//  (getIntervalList() == pNewIntervalList)
//@ End-Method
//=====================================================================

void
	BoundedRange::updateIntervalList(const BoundedInterval* pNewIntervalList)
{
	CALL_TRACE("updateIntervalList(pNewIntervalList)");
	AUX_CLASS_PRE_CONDITION((pNewIntervalList != NULL  &&
							 pNewIntervalList->pNext != NULL),
							Uint32(pNewIntervalList));

	Uint  loopCount = 0u;

	pIntervalList_ = pNewIntervalList;

	for ( pLastNode_ = pIntervalList_->pNext; pLastNode_->pNext != NULL;
		pLastNode_ = pLastNode_->pNext )
	{  // $[TI1] -- iterated at least once...
		// do nothing...
		AUX_CLASS_ASSERTION((loopCount++ < 50), Uint32(pNewIntervalList));
	}  // $[TI2] -- only a single interval (two nodes)...

#if defined(SIGMA_DEVELOPMENT)
	// during development and unit testing of the intervals, verify their
	// validity...
	const BoundedInterval*  pNode;

	for ( pNode = pIntervalList_; pNode->pNext != NULL; pNode = pNode->pNext )
	{
		// ensure that the maximum for each interval is at least as large as
		// the minimum...
		CLASS_ASSERTION((pNode->value >= pNode->pNext->value));

		// ensure that the resolution for each interval is evenly divisible into
		// this interval's full range...
		CLASS_ASSERTION((BoundedRange::IsLegalInterval(pNode->value,
													   pNode->resolution,
													   pNode->precision,
													   pNode->pNext->value)));
	}
#endif // defined(SIGMA_DEVELOPMENT)
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  testValue(rProposedValue, rBoundStatus)  [const]
//
//@ Interface-Description
//  Check 'rProposedValue' against this range.  If 'rProposedValue' is
//  outside of this range's constraint values, 'rProposedValue' is "clipped"
//  to the nearest one, and 'rBoundStatus' is set to indicate the bound
//  that is violated.  If no violation occurs, 'rBoundStatus' is set to
//  an "no-violation" state.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  (isLegalValue(rProposedValue))
//@ End-Method
//=====================================================================

void
	BoundedRange::testValue(BoundedValue& rProposedValue,
							BoundStatus&  rBoundStatus) const
{
	CALL_TRACE("testValue(rProposedValue, rBoundStatus)");

	rBoundStatus.reset();

	if ( isUpperBoundEnabled_() )
	{	// $[TI1] -- test against maximum constraint...
		if ( rProposedValue.value > maxConstraintInfo_.value )
		{	// $[TI1.1]
			Real32  dummyIntervalMax, dummyIntervalMin;
			Real32  intervalRes;

			// get the resolution and precision that goes with
			// 'maxConstraintInfo_.value', using 'maxConstraintInfo_.value' as the
			// maximum of this interval...
			getIntervalValues_(maxConstraintInfo_.value, dummyIntervalMax,
							   intervalRes, rProposedValue.precision,
							   dummyIntervalMin, FALSE);

			// calculate an "acceptable" epsilon distance from a resolution
			// location...
			const Real32  EPSILON = (intervalRes * EPSILON_FACTOR_);

			if ( (rProposedValue.value - EPSILON) > maxConstraintInfo_.value )
			{	// $[TI1.1.1] -- 'rProposedValue.value' NOT is within the epsilon...
				const BoundState  UPPER_BOUND_STATE =
					(maxConstraintInfo_.isSoft) ? UPPER_SOFT_BOUND	// $[TI1.1.1.1]
					: UPPER_HARD_BOUND;	// $[TI1.1.1.2]

				rBoundStatus.setBoundStatus(UPPER_BOUND_STATE, maxConstraintInfo_.id);
			}	// $[TI1.1.2] -- 'rProposedValue.value' is within the epsilon...

			// "clip" the proposed value to the maximum...
			rProposedValue.value = maxConstraintInfo_.value;
		}	// $[TI1.2] -- 'rProposedValue' does NOT violate max. constraint...
	}	// $[TI2] -- the upper bound is disabled...

	if ( !rBoundStatus.isInViolation()  &&  isLowerBoundEnabled_() )
	{	// $[TI3] -- test against minimum constraint...
		if ( rProposedValue.value < minConstraintInfo_.value )
		{	// $[TI3.1]
			Real32  dummyIntervalMax, dummyIntervalMin;
			Real32  intervalRes;

			// get the precision that goes with 'minConstraintInfo_.value', using
			// 'minConstraintInfo_.value' as the minimum of this interval...
			getIntervalValues_(minConstraintInfo_.value, dummyIntervalMax,
							   intervalRes, rProposedValue.precision,
							   dummyIntervalMin, TRUE);

			// calculate an "acceptable" epsilon distance from a resolution
			// location...
			const Real32  EPSILON = (intervalRes * EPSILON_FACTOR_);

			if ( (rProposedValue.value + EPSILON) < minConstraintInfo_.value )
			{	// $[TI3.1.1] -- 'rProposedValue.value' NOT is within the epsilon...
				const BoundState  LOWER_BOUND_STATE =
					(minConstraintInfo_.isSoft) ? LOWER_SOFT_BOUND	// $[TI3.1.1.1]
					: LOWER_HARD_BOUND;	// $[TI3.1.1.2]

				rBoundStatus.setBoundStatus(LOWER_BOUND_STATE, minConstraintInfo_.id);
			}	// $[TI3.1.2] -- 'rProposedValue.value' is within the epsilon...

			// "clip" the proposed value to the minimum...
			rProposedValue.value = minConstraintInfo_.value;
		}	// $[TI3.2] -- 'rProposedValue' does NOT violate min. constraint...
	}	// $[TI4] -- either upper bound violated or lower bound is disabled...
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  increment(numClicks, rCurrValue)  [const]
//
//@ Interface-Description
//  Increment 'rCurrValue', within this range, by the number of clicks
//  given by 'numClicks'.  If this increment violates this range's specified
//  maximum constraint, 'rCurrValue' is "clipped" to the maximum constraint
//  and 'rBoundStatus' is set up with the violated bound.  If this range's
//  maximum constraint is NOT violated, 'rBoundStatus' is returned in a
//  "no-violation" state.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  (isLegalValue(rCurrValue))
//---------------------------------------------------------------------
//@ PostConditions
//  (isLegalValue(rCurrValue))
//@ End-Method
//=====================================================================

void
	BoundedRange::increment(Uint          numClicks,
							BoundedValue& rCurrValue,
							BoundStatus&  rBoundStatus) const
{
	CALL_TRACE("increment(numClicks, rCurrValue, rBoundStatus)");
	SAFE_CLASS_PRE_CONDITION((isLegalValue(rCurrValue)));

	Uint    leftOverClicks = 0u;
	Real32  intervalMax, intervalRes, intervalMin;
	Int     currClick, freeClicks;

	do
	{	// $[TI1] -- this path ALWAYS executes...
		if ( leftOverClicks != 0u )
		{	// $[TI1.1]
			// update 'numClicks' with the number of left-over clicks...
			numClicks = leftOverClicks;
		}	// $[TI1.2] -- no left-over clicks; this is the first time through...

		// get the values of the interval that 'rCurrValue.value' occupies...
		getIntervalValues_(rCurrValue.value, intervalMax, intervalRes,
						   rCurrValue.precision, intervalMin, TRUE);

		// get the click number that corresponds to 'rCurrValue'...
		currClick = getClickNum_(rCurrValue.value, intervalRes, intervalMin);

		// get the number of free clicks left between 'currClick' and the
		// top of this interval...
		freeClicks = getClickNum_(intervalMax, intervalRes, intervalMin) -
					 currClick;

		if ( numClicks < freeClicks )
		{	// $[TI1.3]
			// all of the clicks can be applied within this interval with
			// room to spare...
			leftOverClicks = 0u;

			// calculate the new value based on its new "click" position...
			rCurrValue.value = intervalMin +
							   (intervalRes * (Real32)(currClick + numClicks));

			// "warp" to a click boundary...
			warpValue(rCurrValue);
		}
		else
		{	// $[TI1.4]
			// either 'numClicks' fits, exactly, into the rest of this interval,
			// OR it overflows into the next interval, if any...
			leftOverClicks = numClicks - freeClicks;

			// the new value is the maximum of this interval...
			rCurrValue.value = intervalMax;
		}

		// while there is still clicks to apply, and the value is less than the
		// maximum constraint...
	} while ( leftOverClicks > 0  &&  rCurrValue.value < maxConstraintInfo_.value );

	if ( leftOverClicks > 0 )
	{	// $[TI2] -- maximum constraint IS violated...
		const BoundState  UPPER_BOUND_STATE =
			(maxConstraintInfo_.isSoft) ? UPPER_SOFT_BOUND	// $[TI2.1]
			: UPPER_HARD_BOUND;	// $[TI2.2]

		// set the bound status up with the violated maximum constraint...
		rBoundStatus.setBoundStatus(UPPER_BOUND_STATE, maxConstraintInfo_.id);
	}
	else
	{	// $[TI3] -- maximum constraint is NOT violated...
		// set the bound status to a "no-violation" state...
		rBoundStatus.reset();
	}

	if ( rCurrValue.value == intervalMax )
	{	// $[TI4]
		Precision  upperPrec;

		// get the PRECISION of the upper interval...
		getIntervalValues_(rCurrValue.value, intervalMax, intervalRes,
						   upperPrec, intervalMin, TRUE);

		// since we're at an interval boundary, use the "coarser" of the
		// two precisions...
		rCurrValue.precision = (rCurrValue.precision > upperPrec)
							   ? rCurrValue.precision  // $[TI4.1]
							   : upperPrec;		   // $[TI4.2]
	}	// $[TI5] -- did NOT stop at a change-point...
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  decrement(numClicks, rCurrValue)  [const]
//
//@ Interface-Description
//  Decrement 'rCurrValue', within this range, by the number of clicks
//  given by 'numClicks'.  If this decrement violates this range's specified
//  minimum constraint, 'rCurrValue' is "clipped" to the minimum constraint
//  and 'rBoundStatus' is set up with the violated bound.  If this range's
//  minimum constraint is NOT violated, 'rBoundStatus' is returned in a
//  "no-violation" state.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  (isLegalValue(rCurrValue))
//---------------------------------------------------------------------
//@ PostConditions
//  (isLegalValue(rCurrValue))
//@ End-Method
//=====================================================================

void
	BoundedRange::decrement(Uint          numClicks,
							BoundedValue& rCurrValue,
							BoundStatus&  rBoundStatus) const
{
	CALL_TRACE("decrement(numClicks, rCurrValue, rBoundStatus)");
	SAFE_CLASS_PRE_CONDITION((isLegalValue(rCurrValue)));

	Uint    leftOverClicks = 0u;
	Real32  intervalMax, intervalRes, intervalMin;
	Int     currClick;

	do
	{	// $[TI1] -- this path ALWAYS executes...
		if ( leftOverClicks != 0u )
		{	// $[TI1.1]
			// update 'numClicks' to the number of clicks that are left...
			numClicks = leftOverClicks;
		}	// $[TI1.2] -- no left-over clicks; this is the first time through...

		// get the values of the interval that 'rCurrValue.value' occupies...
		getIntervalValues_(rCurrValue.value, intervalMax, intervalRes,
						   rCurrValue.precision, intervalMin, FALSE);

		// get the click number that corresponds to 'rCurrValue'...
		currClick = getClickNum_(rCurrValue.value, intervalRes, intervalMin);

		if ( numClicks < currClick )
		{	// $[TI1.3]
			// all of the clicks can be applied within this interval with
			// room to spare...
			leftOverClicks = 0u;

			// calculate the new value based on its new "click" position...
			rCurrValue.value = intervalMin +
							   (intervalRes * (Real32)(currClick - numClicks));

			// "warp" to a click boundary...
			warpValue(rCurrValue);
		}
		else
		{	// $[TI1.4]
			// either 'numClicks' fits, exactly, into the this interval, OR it
			// underflows into the lower interval, if any...
			leftOverClicks = numClicks - currClick;

			// the new value is the minimum of this interval...
			rCurrValue.value = intervalMin;
		}

		// while there is still clicks to apply, and the value is more than the
		// absolute minimum...
	} while ( leftOverClicks > 0  &&  rCurrValue.value > minConstraintInfo_.value );

	if ( leftOverClicks > 0 )
	{	// $[TI2] -- minimum constraint IS violated...
		const BoundState  LOWER_BOUND_STATE =
			(minConstraintInfo_.isSoft) ? LOWER_SOFT_BOUND	// $[TI2.1]
			: LOWER_HARD_BOUND;	// $[TI2.2]

		// set the bound status up with the violated minimum constraint...
		rBoundStatus.setBoundStatus(LOWER_BOUND_STATE, minConstraintInfo_.id);
	}
	else
	{	// $[TI3] -- minimum constraint is NOT violated...
		// set the bound status to a "no-violation" state...
		rBoundStatus.reset();
	}

	if ( rCurrValue.value == intervalMin )
	{	// $[TI4]
		Precision  lowerPrec;

		// get the PRECISION of the lower interval...
		getIntervalValues_(rCurrValue.value, intervalMax, intervalRes,
						   lowerPrec, intervalMin, FALSE);

		// since we're at an interval boundary, use the "coarser" of the
		// two precisions...
		rCurrValue.precision = (rCurrValue.precision > lowerPrec)
							   ? rCurrValue.precision  // $[TI4.1]
							   : lowerPrec;		   // $[TI4.2]
	}	// $[TI5] -- did NOT stop at a change-point...
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  warpValue(rCurrValue, warpDirection, clipToRange)  [const]
//
//@ Interface-Description
//  This "warps" (or shifts) the value contained by 'rCurrValue' to a
//  click boundary within it's current range.  The value returned in
//  'rCurrValue' is the "precise" representation of the click boundary
//  value.  The direction of the warping is controlled by 'warpDirection'
//  which can indicate to warp to the click "above", to the  "nearest"
//  click, or to the click "below" the value given by 'rCurrValue'.  If
//  'rCurrValue' is already equivalent to a click, no warping is done.  If
//  'clipToRange' is 'TRUE' the value is also "clipped" to this interval's
//  minimum and maximum constraints.  If 'rCurrValue' is outside of this
//  instance's absolute range, no warping is done.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  (warpDirection == BoundedRange::WARP_UP       ||
//   warpDirection == BoundedRange::WARP_NEAREST  ||
//   warpDirection == BoundedRange::WARP_DOWN)
//---------------------------------------------------------------------
//@ PostConditions
//  (clipToRange  &&  isLegalValue(rCurrValue))
//@ End-Method
//=====================================================================

void
	BoundedRange::warpValue(BoundedValue&               rCurrValue,
							const BoundedRange::WarpDir warpDirection,
							const Boolean               clipToRange) const
{
	CALL_TRACE("warpValue(rCurrValue, warpDirection)");

	Real32     intervalMax, intervalRes, intervalMin;
	Precision  intervalPrec;

	// this flag is used when 'rCurrValue' is at, or near, a change-point;
	// this flag indicates whether to start by using the upper interval around
	// that change point, or by first using the lower interval (see end of method
	// for second try)...
	const Boolean  USE_UPPER_INTERVAL =
		(warpDirection != BoundedRange::WARP_DOWN);

	// get the interval parameters for the interval that 'rCurrValue' resides
	// in; the various intervals are searched in a highest-to-lowest order,
	// therefore, the returned interval is the highest interval that
	// 'rCurrValue.value' matches...
	getIntervalValues_(rCurrValue.value, intervalMax, intervalRes,
					   intervalPrec, intervalMin, USE_UPPER_INTERVAL);

	// calculate a small percentage of a resolution to be used to
	// determine whether to ignore the "down" or "up" request,
	// because of how close 'rCurrValue.value' is to a resolution
	// location (DCS #1534)...
	const Real32  EPSILON = (intervalRes * EPSILON_FACTOR_);

	if ( rCurrValue.value < intervalMin  &&
		 (rCurrValue.value + EPSILON) >= intervalMin )
	{
		// $[TI7]
		rCurrValue.value = intervalMin;
	}
	else if ( rCurrValue.value > intervalMax  &&
			  (rCurrValue.value - EPSILON) <= intervalMax )
	{
		// $[TI8]
		rCurrValue.value = intervalMax;
	}
	// $[TI9] -- implied else

	if ( rCurrValue.value > intervalMin )
	{	// $[TI1] -- 'rCurrValue.value' is within the absolute range...
		// get the difference between the value of 'rCurrValue' and an exact
		// "click" boundary...
		const Real32  REMAINDER = ::fmod((rCurrValue.value - intervalMin),
										 intervalRes);

		if ( REMAINDER != 0.0f )
		{	// $[TI1.1]
			// currently NO bounded settings have a negative resolution, therefore
			// this method is NOT implementated for a negative resolution (or
			// remainder)...
			AUX_CLASS_ASSERTION((intervalRes > 0.0f  &&  REMAINDER > 0.0f),
								(REMAINDER > 0.0f));

			// there is a non-zero difference between the value of 'rCurrValue' and
			// its nearest "click" boundary, therefore get half of a "click"...
			const Real32  HALF_CLICK = (intervalRes * 0.5f) - EPSILON;

			BoundedRange::WarpDir  actualWarpDirection = warpDirection;

			switch ( warpDirection )
			{
				case WARP_DOWN :
				case WARP_UP :		// $[TI1.1.1]
					{
						// if 'rCurrValue.value' is within the epsilon threshold of the
						// resolution, then ignore the requested warp direction and
						// "round"...
						if ( REMAINDER <= EPSILON )
						{	// $[TI1.1.1.1]
							// the remainder indicates that 'rCurrValue.value' is just
							// above the "click" location, therefore warp DOWN...
							actualWarpDirection = WARP_DOWN;
						}
						else if ( (intervalRes - REMAINDER) <= EPSILON )
						{	// $[TI1.1.1.2]
							// the remainder indicates that 'rCurrValue.value' is just
							// below the "click" location, therefore warp UP...
							actualWarpDirection = WARP_UP;
						}	// $[TI1.1.1.3] -- use requested warping direction...
					}
					break;

				case WARP_NEAREST :	// $[TI1.1.2]
					if ( REMAINDER > HALF_CLICK  &&  (REMAINDER - HALF_CLICK) < 0.0001f )
					{  // $[TI1.1.2.1]
						// SPECIAL CASE:  basically, this is to ensure that negative values,
						//                whose remainder is essentially equal to
						//                'HALF_CLICK', are warped "down" (Neo-DCS #18)...
						actualWarpDirection = (rCurrValue.value >= 0.0f)
											  ? WARP_UP	   // $[TI1.1.2.1.1]
											  : WARP_DOWN; // $[TI1.1.2.1.2]
					}  // $[TI1.1.2.2]
					break;

				default :
					// invalid 'warpDirection'...
					AUX_CLASS_ASSERTION_FAILURE(warpDirection);
					break;
			};

			if ( actualWarpDirection == WARP_DOWN  ||
				 (actualWarpDirection == WARP_NEAREST  &&  REMAINDER < HALF_CLICK) )
			{	// $[TI1.1.3]
				// either 'actualWarpDirection' is 'WARP_DOWN', OR its 'WARP_NEAREST'
				// with a remainder that's smaller than half a click, therefore round
				// DOWN to the resolution "click" just below the value of
				// 'rCurrValue'...
				rCurrValue.value -= REMAINDER;
			}
			else if ( actualWarpDirection == WARP_UP  ||
					  (actualWarpDirection == WARP_NEAREST  &&  REMAINDER > HALF_CLICK) )
			{	// $[TI1.1.4]
				// either 'actualWarpDirection' is 'WARP_UP', OR its 'WARP_NEAREST'
				// with a remainder that's more than a half of a click, therefore
				// round UP to the resolution "click" just above the value of
				// 'rCurrValue'...
				rCurrValue.value += (intervalRes - REMAINDER);
			}
			else
			{	// $[TI1.1.5]
				// 'actualWarpDirection' is 'WARP_NEAREST' with a remainder that's
				// equal to a half of a click, therefore round the positive values UP
				// and the negative values DOWN...
				rCurrValue.value += (rCurrValue.value >= 0.0f)
									? HALF_CLICK	   // $[TI1.1.5.1]
									: -HALF_CLICK;	   // $[TI1.1.5.2]
			}

			if ( EPSILON >= ABS_VALUE(intervalMin - rCurrValue.value) )
			{  // $[TI1.1.6] -- 'rCurrValue' very close to minimum interval value...
				// due to inaccuracies of calculating remainders, etc., the rounded
				// value is extremely close to, but not exactly at, the interval
				// minimum value, therefore set it equal to it...
				rCurrValue.value = intervalMin;
			}
			else if ( EPSILON >= ABS_VALUE(intervalMax - rCurrValue.value) )
			{  // $[TI1.1.7] -- 'rCurrValue' very close to maximum interval value...
				// due to inaccuracies of calculating remainders, etc., the rounded
				// value is extremely close to, but not exactly at, the interval
				// maximum value, therefore set it equal to it...
				rCurrValue.value = intervalMax;
			}  // $[TI1.1.8] -- not "close to" minimum or maximum values...
		}	// $[TI1.2] -- 'REMAINDER' is equal to zero...
	}
	else if ( rCurrValue.value < intervalMin  &&
			  intervalMin > getMinConstraintValue() )
	{	// $[TI10] -- 'rCurrValue.value' is less-than this interval's minimum
		//            value AND this interval's minimum value is NOT the full
		//            range's minimum value...
		// this case happens when 'rCurrValue.value' is equivalent to (at the lower
		// interval's precision), but less-than, a change-point's value, therefore
		// it must be treated as equal to the change-point (DCS #5421)...
		rCurrValue.value = intervalMin;
	}	// $[TI2] -- apply no "warping" to 'rCurrValue.value'...

	if ( clipToRange )
	{	// $[TI3]
		// "boundary" clipping is requested, therefore see if any is needed...
		if ( rCurrValue.value > intervalMax )
		{	// $[TI3.1]
			// "clip" to upper boundary...
			rCurrValue.value = intervalMax;
		}
		else if ( rCurrValue.value < intervalMin )
		{	// $[TI3.2]
			// "clip" to lower boundary...
			rCurrValue.value = intervalMin;
		}	// $[TI3.3]
	}	// $[TI4] -- no "boundary" clipping is to be done...

	if ( rCurrValue.value == intervalMin )
	{	// $[TI5]
		// stopped at a change-point, make sure that the "coarser" precision is
		// returned...
		Precision  lowerPrec;

		getIntervalValues_(rCurrValue.value, intervalMax, intervalRes,
						   lowerPrec, intervalMin, !USE_UPPER_INTERVAL);

		rCurrValue.precision = (intervalPrec > lowerPrec)
							   ? intervalPrec  // $[TI5.1]
							   : lowerPrec;	   // $[TI5.2]
	}
	else
	{	// $[TI6] -- did NOT stop at a change-point...
		// store the precision of this interval...
		rCurrValue.precision = intervalPrec;
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pBoolTest)
//                 [static]
//
//@ Interface-Description
//  Report the software fault that occured within the source code
//  of this class.  The fault, indicated by 'softFaultID', occured
//  at 'lineNumber' in 'pFileName'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

void
	BoundedRange::SoftFault(const SoftFaultID softFaultID,
							const Uint32      lineNumber,
							const char*       pFileName,
							const char*       pBoolTest)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pBoolTest)");

	FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION, BOUNDED_RANGE, lineNumber,
							pFileName, pBoolTest);
}


//=====================================================================
//
//  Private Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  isAnIntervalMatch_(currValue, isIncreasing, nextIntervalMax,
//				nextIntervalPrec)  [const]
//
//@ Interface-Description
//  This returns 'TRUE' if the value given by 'currValue' is "matched"
//  with the interval represented interval.  This is used for determining
//  which of a range's intervals is the one that is to be used for
//  'currValue' (see 'getIntervalValues_()' of this class's derived classes).
//---------------------------------------------------------------------
//@ Implementation-Description
//  The criteria of whether an interval is a "match" for 'currValue' is
//  based on one of two things (in order) being true:
//	1.  'currValue' is greater-than, AND not equivalent (at the given
//	    precision) to, the "next" interval maximum.
//	2.  'currValue' is being increased (incremented) AND it IS
//	    equivalent (at the given precision) to the "next" interval's
//	    maximum.
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

Boolean
	BoundedRange::isAnIntervalMatch_(const Real32    currValue,
									 const Boolean   isIncreasing,
									 const Real32    nextIntervalMax,
									 const Precision nextIntervalPrec) const
{
	CALL_TRACE("isAnIntervalMatch_(...)");

	const Boolean  IS_GREATER_THAN = (currValue > nextIntervalMax);
	const Boolean  IS_EQUIV_TO     = ::IsEquivalent(currValue,
													nextIntervalMax,
													nextIntervalPrec);
	Boolean  isAMatch;

	isAMatch = (
			   // is 'currValue' greater than the next interval's maximum,
			   // AND NOT equivalent to it, at the given precision?...
			   (IS_GREATER_THAN  &&  !IS_EQUIV_TO)  ||

			   // OR is 'currValue' increasing AND greater than the next
			   // interval's maximum, OR is it increasing AND equivalent to
			   // the next interval's maximum, at the given precision?...
			   (isIncreasing  &&  (IS_GREATER_THAN  ||  IS_EQUIV_TO))
			   );	// $[TI1] (TRUE)  $[TI2] (FALSE)

	return(isAMatch);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getIntervalValues_(currValue, rIntervalMax, rIntervalRes,
//				rIntervalPrec, rIntervalMin, isIncreasing)
//
//@ Interface-Description
//  Return the range values of the range that corresponds to 'currValue'.
//  If 'currValue' is equal to a change-over value, then the range will
//  be determined according to whether the values are advancing
//  ('isIncreasing') or declining ('!isIncreasing').
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

void
	BoundedRange::getIntervalValues_(const Real32  currValue,
									 Real32&       rIntervalMax,
									 Real32&       rIntervalRes,
									 Precision&    rIntervalPrec,
									 Real32&       rIntervalMin,
									 const Boolean isIncreasing) const
{
	CALL_TRACE("getIntervalValues_(currValue, rIntervalMax, rIntervalRes, rIntervalPrec, rIntervalMin, isIncreasing)");

	const Real32  MAX_CONSTRAINT_VALUE = getMaxConstraintValue();
	const Real32  MIN_CONSTRAINT_VALUE = getMinConstraintValue();

	Precision  precision;

	Boolean  isThisIntervalOn, isNextIntervalOn;

	const BoundedInterval*  pThisNode = pIntervalList_;
	const BoundedInterval*  pNextNode = pIntervalList_->pNext;

	for ( ; pNextNode != NULL; pThisNode = pNextNode, pNextNode = pNextNode->pNext )
	{  // $[TI1] -- this path is ALWAYS taken...
		//-----------------------------------------------------------------
		// test this interval against 'currValue' to determine if there
		// is a "match"...
		//-----------------------------------------------------------------

		// store off this interval's low value, which is found in the next
		// node...
		const Real32  THIS_LOW_VALUE = pNextNode->value;

		isThisIntervalOn = (MAX_CONSTRAINT_VALUE >= THIS_LOW_VALUE);

		if ( pNextNode->pNext != NULL )
		{  // $[TI1.1] -- there is a next interval...
			// store off the next interval's low value...
			const Real32  NEXT_LOW_VALUE = pNextNode->pNext->value;

			// This is to use the right precision on the boundary values
			// between intervals. e.g. Vt was going from 5 to 5.0 to 4.9
			isNextIntervalOn = (MAX_CONSTRAINT_VALUE >= NEXT_LOW_VALUE  &&
								MIN_CONSTRAINT_VALUE < THIS_LOW_VALUE);

			// the lower precision is needed below to determine an interval "match"...
			precision = MIN_VALUE(pNextNode->precision,	  // $[TI1.1.1]
								  pThisNode->precision);  // $[TI1.1.2]
		}
		else
		{  // $[TI1.2] -- there is no next interval...
			isNextIntervalOn = FALSE;
			precision        = pThisNode->precision;
		}

		if ( isThisIntervalOn  &&
			 (!isNextIntervalOn  ||
			  isAnIntervalMatch_(currValue, isIncreasing, THIS_LOW_VALUE,
								 precision)) )
		{	// $[TI1.3]
			// THIS INTERVAL IS VALID while EITHER 'currValue' is greater-than
			// this interval's low value or its right at this interval's  low value
			// while being increased, OR while the "next" lower interval is NOT valid,
			// therefore pass back this interval's information...
			rIntervalMax  = MIN_VALUE(pThisNode->value,	  // $[TI1.3.1]
									  MAX_CONSTRAINT_VALUE);  // $[TI1.3.2]
			rIntervalRes  = pThisNode->resolution;
			rIntervalPrec = pThisNode->precision;
			rIntervalMin  = MAX_VALUE(pNextNode->value,	  // $[TI1.3.3]
									  MIN_CONSTRAINT_VALUE);  // $[TI1.3.4]

			break;	// break out of for loop...
		}	// $[TI1.4] -- check the next interval...
	}  // end of for loop...
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getClickNum_(currValue, intervalRes, intervalMin)  [const]
//
//@ Interface-Description
//  Return the "click" number that corresponds to 'currValue'.  For example,
//  if 'currValue' is equal to '((2 * intervalRes) + intervalMin)', a value
//  of '2' is returned.
//---------------------------------------------------------------------
//@ Implementation-Description
//  By dividing the interval between 'currValue' and 'intervalMin', by the
//  the interval's resolution ('intervalRes'), the "exact" number of
//  "clicks" can be determined.  Using '::WholePart()', the exact number
//  of "clicks" is translated (truncated) to a whole number.
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

Uint32
	BoundedRange::getClickNum_(const Real32 currValue,
							   const Real32 intervalRes,
							   const Real32 intervalMin) const
{
	CALL_TRACE("getClickNum_(currValue, intervalRes, intervalMin)");

	Uint32  currClickNum;

	if ( currValue > intervalMin )
	{	// $[TI1]
		const Real32  EXACT_NUM_CLICKS = (currValue - intervalMin) / intervalRes;

		// "round" to the nearest whole number; '::DEFAULT_EPSILON_FACTOR_' is
		// added to ensure that "exact" numbers, such as '3.999993', are properly
		// represented (as '4' clicks)...
		currClickNum = (Uint32)::WholePart(EXACT_NUM_CLICKS + ::DEFAULT_EPSILON_FACTOR_);
	}
	else
	{	// $[TI2]
		// 'currValue' is equivalent to or below this interval's minimum,
		// therefore the current "click" number is zero...
		currClickNum = 0u;
	}

	return(currClickNum);
}


//=====================================================================
//
//  Friend Functions...
//
//=====================================================================

#if defined(SIGMA_DEVELOPMENT)

//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
// Free-Function:  operator<<(ostr, boundedRange)  [friend]
//
// Interface-Description
//  Dump the contents of 'boundedRange' into 'ostr'.
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreConditions
//  none
//---------------------------------------------------------------------
// PostConditions
//  none
// End-Free-Function
//=====================================================================

Ostream&
	operator<<(Ostream& ostr, const BoundedRange& boundedRange)
{
	CALL_TRACE("::operator<<(ostr, boundedRange)");

	ostr << "BoundedRange {" << endl;

	const BoundedInterval*  pNode;

	for ( pNode = boundedRange.pIntervalList_; pNode != NULL; pNode = pNode->pNext )
	{
		ostr << "  value          = " << pNode->value << endl;

		if ( pNode->pNext != NULL )
		{
			ostr << "  resolution     = " << pNode->resolution << endl;
			ostr << "  precision      = " << pNode->precision  << endl;
		}
	}

	ostr << "  -------------------" << endl;

	ostr << "  MAX_CONSTRAINT = <" << boundedRange.maxConstraintInfo_.value
		<< ", "                   << boundedRange.maxConstraintInfo_.id
		<< ", "                   << boundedRange.maxConstraintInfo_.isSoft
		<< ">"                    << endl;
	ostr << "  MIN_CONSTRAINT = <" << boundedRange.minConstraintInfo_.value
		<< ", "                   << boundedRange.minConstraintInfo_.id
		<< ", "                   << boundedRange.minConstraintInfo_.isSoft
		<< ">"                    << endl;
	ostr << '}' << endl;

	return(ostr);
}

#endif // defined(SIGMA_DEVELOPMENT)
