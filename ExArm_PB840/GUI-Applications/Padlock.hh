#ifndef Padlock_HH
#define Padlock_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: Padlock -  A "padlock" is used for adjusting the Constant During
// Rate Change setting on the Vent Setup subscreen.  Switches between a
// "locked padlock" bitmap and an "unlocked padlock" button.  This is
// displayed only while Respiratory Rate is selected within the Vent Setup
// subscreen.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/Padlock.hhv   25.0.4.0   19 Nov 2013 14:08:12   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By:  sah	   Date:  23-Feb-1999    DCS Number:  5314
//  Project:  ATC
//  Description:
//      Added integrated with new dynamic, setting applicability functionality.
//      This allows for looser coupling between settings and GUI, and the
//      need for significantly less "setting knowledge" in the GUI subsystem.
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "Container.hh"
#include "ButtonTarget.hh"

//@ Usage-Classes
#include "ConstantParmValue.hh"
#include "Bitmap.hh"
#include "Button.hh"
#include "SettingObserver.hh"
//@ End-Usage

class Padlock : public Container, public ButtonTarget, public SettingObserver
{
public:
	Padlock(Int16 centerX, Int16 yOrg,
						ConstantParmValue::ConstantParmValueId timingValue);
    ~Padlock(void);

	// ButtonTarget virtual method
	virtual void buttonDownHappened(Button *pButton,
												Boolean byOperatorAction);

	// Drawable virtual methods
	virtual void activate  (void);
	virtual void deactivate(void);

	// SettingObserver virtual method
    virtual void  valueUpdate(const Notification::ChangeQualifier qualifierId,
                              const SettingSubject*               pSubject);

    static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL, 
			  const char*       pPredicate = NULL);
  
protected:

private:
    // these methods are purposely declared, but not implemented...
    Padlock(void);							// not implemented...
    Padlock(const Padlock&);				// not implemented...
    void   operator=(const Padlock&);		// not implemented...

	void updateDisplay_(const SettingSubject* pSubject);

    //@ Data-Member: centerX_
	// The X coordinate around which the padlock is centered.
	Int16 centerX_;

    //@ Data-Member: timingValue_
    // The discrete value that specifies which timing parameter will be
    // set as the Constant During Rate Change setting if this padlock is
    // locked by the user.
    ConstantParmValue::ConstantParmValueId timingValue_;

    //@ Data-Member: unlockedButton_
    // The button that is displayed when the padlock is in the unlocked
    // state.  The user can press this button to lock the padlock.
    Button unlockedButton_;

    //@ Data-Member: unlockedBitmap_
    // The bitmap that depicts the unlocked state of the padlock.
    Bitmap unlockedBitmap_;

    //@ Data-Member: lockedBitmap_
    // The bitmap that depicts the locked state of the padlock.
    Bitmap lockedBitmap_;

};

#endif // Padlock_HH
