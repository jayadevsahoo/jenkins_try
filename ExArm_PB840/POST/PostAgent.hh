#ifndef PostAgent_HH
#define PostAgent_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//====================================================================
 
//====================================================================
// Class:  PostAgent - Network Object containing POST data
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/POST/vcssrc/PostAgent.hhv   25.0.4.0   19 Nov 2013 14:17:04   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004    By: sah   Date: 14-Jan-1999  DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 003    By: Gary Cederquist  Date: 26-FEB-1998  DR Number: 5029
//    Project: Sigma (R8027)
//    Description:
//       Corrected subsystem and class ID passed to SoftFault.
//
//  Revision: 002    By: Gary Cederquist  Date: 21-APR-1997  DR Number: 1982
//    Project: Sigma (R8027)
//    Description:
//       Changed to accomodate new part number format (length).
//
//  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//====================================================================

//@ Usage-Classes
#include "Post.hh"
#include "TimeStamp.hh"
//@ End-Usage

class PostAgent
{
 
  public:
    enum
    {
        PART_NO_SIZE = 13
    };

    PostAgent(void);
    PostAgent(const PostAgent& rPostAgent);
    PostAgent& operator=(const PostAgent& rhs); 
	~PostAgent(void);


    static void          SoftFault(const SoftFaultID  softFaultID,
				   const Uint32       lineNumber,
				   const char*        pFileName = NULL,
				   const char*        pPredicate = NULL);
 

    inline const TimeStamp&     getPowerDownTime(void) const;
    inline const TimeStamp&     getPowerUpTime(void) const;
    inline DowntimeStatus       getDowntimeStatus(void) const;
    inline ShutdownState        getStartupState(void) const;
    inline Boolean              isPostFailed(void) const;
    inline Criticality          getBackgroundFailed(void) const;
    inline Boolean              isServiceModeRequested(void) const;
    inline const char *         getKernelPartNumber(void) const;


  private:
    // data members placed largest first to pack bits at end and
    // conserve space for the network object

    //@ Data-Member:     powerDownTime_
    const TimeStamp      powerDownTime_;

    //@ Data-Member:     powerUpTime_
    const TimeStamp      powerUpTime_;

    //@ Data-Member:     downtimeStatus_
    const DowntimeStatus downtimeStatus_;

    //@ Data-Member:     startupState_
    const ShutdownState  startupState_;

    //@ Data-Member:     postFailed_
    const Boolean        postFailed_;

    //@ Data-Member:     backgroundFailed_
    const Criticality    backgroundFailed_;

    //@ Data-Member:     serviceModeRequested_
    const Boolean        serviceModeRequested_;

    //@ Data-Member:     kernelPartNumber_
    char                 kernelPartNumber_[PostAgent::PART_NO_SIZE+1];

};
 
#include "PostAgent.in"

#endif // PostAgent_HH
