#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: Solenoid - A class to drive and manage the solenoids.
//---------------------------------------------------------------------
//@ Interface-Description
//      This class allows the opening and closing of a solenoid.  Methods are
//		implemented to obtain the current state of the solenoid (open or
//		closed); command open a solenoid; and to command close a solenoid.
//---------------------------------------------------------------------
//@ Rationale
//      This is an abstraction of a solenoid.
//---------------------------------------------------------------------
//@ Implementation-Description
//      When constructed, the deenergized state is associated with
//      open or closed.  The bit mask for controlling the solenoid
//      is also defined along with the corresponding register.  The state
//		of the solenoid is also maintained.
//---------------------------------------------------------------------
//@ Fault-Handling
//		n/a
//---------------------------------------------------------------------
//@ Restrictions
//		none
//---------------------------------------------------------------------
//@ Invariants
//        None
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/Solenoid.ccv   25.0.4.0   19 Nov 2013 13:54:12   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005 By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//	Description:
//		Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 004  By: syw    Date:  05-Feb-1997    DR Number: DCS 1697
//  	Project:  Sigma (R8027)
//		Description:
//			Initialize currentState_ to deenergized state in constructor.
//
//  Revision: 003  By: syw    Date:  19-Dec-1996    DR Number: DCS 1603
//  	Project:  Sigma (R8027)
//		Description:
//			Changed Register to BitAccessGpio.  Call requestBitUpdate()
//			instead of write().  Change masks to bit location.
//
//  Revision: 002  By: syw    Date: 05-Dec-1995   DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Moved open() and close() from Solenoid.in to this file.
//
//	Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: INITIAL RELEASE
//      Project:  Sigma (R8027)
//      Description:
//      	Initial version
//
//=====================================================================

#include "Solenoid.hh"
#include "BitAccessGpio.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Solenoid()
//
//@ Interface-Description
//		Constructor.  This method takes three arguments: the deenergized
//		state of the solenoid, the mask, and the Register that the solenoid
//		is associated with. 
//---------------------------------------------------------------------
//@ Implementation-Description
//   	The arguments passed in initilaizes the corresponding private data
//		members.  The bit number is determined from the bit mask.
//---------------------------------------------------------------------
//@ PreCondition
//      None
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================

Solenoid::Solenoid( const Solenoid::SolenoidState deenergizedState,
					const ApplicationDigitalOutputs_t bit,
					BitAccessGpio& rSolenoidPort) 
    : deenergizedState_( deenergizedState), 	// $[TI1.1]
	  currentState_( deenergizedState),  		// $[TI1.2]
      rSolenoidPort_( rSolenoidPort)			// $[TI1.3]
{
    CALL_TRACE("Solenoid::Solenoid( \
    		const Solenoid::SolenoidState deenergizedState, \
    		const Uint16 solMask, BitAccessGpio& rSolenoidPort) ") ;

	bit_ = bit ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~Solenoid()
//
//@ Interface-Description
//		Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//      None
//---------------------------------------------------------------------
//@ PreCondition
//      None
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================

Solenoid::~Solenoid( void)
{
	CALL_TRACE("Solenoid::~Solenoid( void)") ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: open()
//
//@ Interface-Description
//    	This method takes no arguments and opens the solenoid.  It returns
//		nothing.  This method CAN NOT be INTERUPTED.
//---------------------------------------------------------------------
//@ Implementation-Description
//    	Based upon the definition of open, energize or deenergize the
//    	solenoid to open the solenoid.
//---------------------------------------------------------------------
//@ PreCondition
//    	None
//---------------------------------------------------------------------
//@ PostCondition
//    	None
//@ End-Method
//=====================================================================

void
Solenoid::open( void)
{
	CALL_TRACE("Solenoid::open( void)") ;

    if (deenergizedState_ == Solenoid::OPEN)
    {
        // $[TI1.1]
	    rSolenoidPort_.requestBitUpdate( bit_, BitAccessGpio::OFF) ;
    }
    else
    {
    	// $[TI1.2]
	    rSolenoidPort_.requestBitUpdate( bit_, BitAccessGpio::ON) ;
    }

    currentState_ = Solenoid::OPEN ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: close()
//
//@ Interface-Description
//    	This method takes no arguments and closes the solenoid.  It returns
//		nothing.  This method CAN NOT be INTERUPTED.
//---------------------------------------------------------------------
//@ Implementation-Description
//    	Based upon the definition of open, energize or deenergize the
//    	solenoid to close the solenoid.
//---------------------------------------------------------------------
//@ PreCondition
//    	None
//---------------------------------------------------------------------
//@ PostCondition
//    	None
//@ End-Method
//=====================================================================

void
Solenoid::close( void)
{
	CALL_TRACE("Solenoid::close( void)") ;

    if (deenergizedState_ == Solenoid::OPEN)
    {
		// $[TI1.1]
	    rSolenoidPort_.requestBitUpdate( bit_, BitAccessGpio::ON) ;
    }
    else
    {
		// $[TI1.2]
	    rSolenoidPort_.requestBitUpdate( bit_, BitAccessGpio::OFF) ;
    }

    currentState_ = Solenoid::CLOSED ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getCurrentState()
//
//@ Interface-Description
//    	This method takes no arguments and returns the current state of the
//		solenoid.
//---------------------------------------------------------------------
//@ Implementation-Description
//    	Returns the data member currentState_.
//---------------------------------------------------------------------
//@ PreCondition
//    	None
//---------------------------------------------------------------------
//@ PostCondition
//    	None
//@ End-Method
//=====================================================================

Solenoid::SolenoidState
Solenoid::getCurrentState( void)
{
	CALL_TRACE("Solenoid::getCurrentState( void)") ;

	// $[TI1]
	return( currentState_) ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault()
//
//@ Interface-Description
//      This method takes four arguments: softFaultID, lineNumber, pFileName,
//      and pPredicate and is called when a software fault is detected by the
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
Solenoid::SoftFault( const SoftFaultID  softFaultID,
					 const Uint32       lineNumber,
			 		 const char*        pFileName,
					 const char*        pPredicate)
{
	CALL_TRACE("SoftFault( softFaultID, lineNumber, pFileName, pPredicate)") ;
	
  	FaultHandler::SoftFault( softFaultID, BD_IO_DEVICES, SOLENOID,
                          lineNumber, pFileName, pPredicate) ;
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================



//=====================================================================
//
//  Private Methods...
//
//=====================================================================







