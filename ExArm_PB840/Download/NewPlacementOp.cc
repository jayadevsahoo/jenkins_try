#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//    Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Filename:  NewPlacementOp - Global Placement New Operator.
// 	Provides global new/delete override operators.
//---------------------------------------------------------------------
//@ Interface-Description
//  This defines a global, placement new operator.  This new operator
//  is used to run a constructor on a given block of memory.
//  Overrides the new and delete operators. There is no dynamic allocation
//  of memory (heap free store) allowed in Sigma. If an attempt is made to
//  allocate or deallocate memory an assertion will cause termination of
//  the calling task.
//---------------------------------------------------------------------
//@ Rationale
//  This function will be used extensively to initialize static class
//  instances, therefore this is provided for the extra checking
//  of the pointer against 'NULL'.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Standard placement-new implementation used.
//  If anyone wants to call new and delete operator, 
//  will utilize assertions to prevent dynamic allocation and deallocation
//  of memory.
//---------------------------------------------------------------------
//@ Fault-Handling
//  A standard pre-condition test is used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Download/vcssrc/NewPlacementOp.ccv   25.0.4.0   19 Nov 2013 14:02:50   pvcs  $ 
//
//@ Modification-Log
//  
//   Revision: 001   By: jhv   Date: 9-Feb-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//=====================================================================
#if	defined(SIGMA_TARGET)

#include "DownloadFault.hh"
#include "NewPlacementOp.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//=====================================================================
//
//  External Functions...
//
//=====================================================================

//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function:  operator new(size_t, void* pMemory)  [Placement New Operator]
//
//@ Interface-Description
//  Run a given constructor on 'pMemory'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (pMemory != NULL)
//---------------------------------------------------------------------
//@ PostCondition
//  ((new (pMemory) Type(...)) != NULL)
//@ End-Free-Function
//=====================================================================

void*
operator new(size_t, void* pMemory)
{
	// $[TI1]
	FREE_ASSERTION((pMemory != NULL), DOWNLOAD, NEW_PLACEMENT);
	return(pMemory);
}


//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function: operator new(unsigned int)
//
//@ Interface-Description
//  This function overrides the library version global new operator.  
//  Use of this function is not allowed.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Dynamic memory allocation (heap free store) is prohibited in 
//  Sigma, this function immediately asserts and terminates the 
//  calling task.
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//@ End-Free-Function
//=====================================================================
void *
operator new(unsigned int)
{
	// no [TI] needed cause FREE_ASSERTION_FAILURE
    FREE_ASSERTION_FAILURE( DOWNLOAD, NEW_OPERATOR);
	return(NULL); // get rid of the compiler warning, no return.
}

//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function: operator delete(void *)
//
//@ Interface-Description
//  Function overrides the library version global delete operator.  
//  Use of this function is not allowed.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Since dynamic memory allocation (heap space) is prohibited in 
//  Sigma, this function immediately asserts and terminates the 
//  calling task.
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//@ End-Free-Function
//=====================================================================
void
operator delete(void *)
{
	// no [TI] needed cause FREE_ASSERTION_FAILURE
    FREE_ASSERTION_FAILURE( DOWNLOAD, DELETE_OPERATOR);
}


#ifdef SIGMA_DEVELOPMENT
//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function: __pure_virtual_called(void)
//
//@ Interface-Description
//  This function overrides the library version __pure_virtual_called 
//  function.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns with no processing.
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//@ End-Free-Function
//=====================================================================
extern "C" void  __pure_virtual_called(void) 
{
}
#endif // SIGMA_DEVELOPMENT

#endif //defined(SIGMA_TARGET)
