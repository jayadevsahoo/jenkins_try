#ifndef OcclusionTrigger_HH
#define OcclusionTrigger_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: OcclusionTrigger - Monitors the system for the specific 
//  conditions that cause severe occlusion or exhaust port occlusion
//  to be detected.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/OcclusionTrigger.hhv   25.0.4.0   19 Nov 2013 13:59:56   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 004  By:  sp    Date:  19-Sept-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Remove unit test methods.
//
//  Revision: 003  By:  sp    Date:  3-Sept-1996    DR Number: DCS 1293
//       Project:  Sigma (R8027)
//       Description:
//             update per controller spec rev 5.0.
//
//  Revision: 002  By:  sp    Date:  4-Mar-1996    DR Number:DCS 674
//       Project:  Sigma (R8027)
//       Description:
//             Remove #ifndef.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#  include "Sigma.hh"
 
#  include "Breath_Delivery.hh"

#  include "ModeTrigger.hh"

//@ Usage-Classes
//@ End-Usage


class OcclusionTrigger : public ModeTrigger
{
  public:
    OcclusionTrigger(void);
    virtual ~OcclusionTrigger(void);
    virtual void enable(void);

    static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL,
			  const char*       pPredicate = NULL);


  protected:
    virtual Boolean triggerCondition_(void); 

  private:
    OcclusionTrigger(const OcclusionTrigger&);    // Declared but not implemented
    void operator=(const OcclusionTrigger&);   // Declared but not implemented

    Boolean severeOcclusion_(void);
    Boolean exhaustPortOccluded_(void);

    //@ Data-Member:   factor1_
    // Tubing circuit factor 1 to use for either ped. or adult circuits.
    Real32  factor1_;

    //@ Data-Member:  factor2_
    // Tubing circuit factor 2 to use for either ped. or adult circuits.
    Real32  factor2_;

    //@ Data-Member:  factor3_
    // Tubing circuit factor 3 to use for either ped. or adult circuits.
    Real32  factor3_;

    //@ Data-Member:  count1_
    //  Used to time the severe occlusion conditions
    Int32  count1_;

    //@ Data-Member:  count2_
    //  Used to time the severe occlusion conditions
    Int32  count2_;

    //@ Data-Member:  count3_
    //  Used to time the severe occlusion conditions
    Int32  count3_;

    //@ Data-Member:  exhaustPortCount_
    //  Used to time the exhaust port occlusion condition
    Int32  exhaustPortCount_;

};


#endif // OcclusionTrigger_HH 
