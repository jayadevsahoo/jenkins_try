#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//============== C L A S S   D E S C R I P T I O N ====================
//@ Class:  ShadowTraceEnableSetting - Used to enable/disable shadow traces
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a non-batch, discrete setting that is used for enabling/disabling
//  waveform shadow traces (e.g., Pcarinal-time).  This class inherits from
//  'NonBatchDiscreteSetting' and provides the specific information needed
//  for the representation of type of Waveform Plot #1.  This information
//  includes the set of values that this setting can have (see
//  'ShadowTraceEnableValue.hh'), and this non-batch setting's default value.
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class has no data members, and no dependent settings.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/ShadowTraceEnableSetting.ccv   25.0.4.0   19 Nov 2013 14:27:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: rhj    Date: 05-Jun-2006    SCR Number: 6236
//  Project:  RESPM
//  Description:
//      Modified to support Flow Volume Loop
//   
//  Revision: 001   By: emccoy  Date:  14-Apr-2002    DR Number:  5848
//  Project:  VCP
//  Description:
//	Added for supporting the enabling/disabling of shadow traces.
//
//=====================================================================

#include "ShadowTraceEnableSetting.hh"
#include "ShadowTraceEnableValue.hh"
#include "Plot1TypeValue.hh"
#include "Plot2TypeValue.hh"
#include "SupportTypeValue.hh"
#include "ModeValue.hh"

//@ Usage-Classes
#include "SettingsMgr.hh"
#include "ContextMgr.hh"
#include "AcceptedContext.hh"
#include "SafetyPcvSettingValues.hh"
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  ShadowTraceEnableSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[VC01001] -- enable/disable of Pcari's shadow trace
//  $[PA01000] -- enable/disable of Plung's shadow trace
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ShadowTraceEnableSetting::ShadowTraceEnableSetting(void)
  : NonBatchDiscreteSetting(SettingId::SHADOW_TRACE_ENABLE,
			    Setting::NULL_DEPENDENT_ARRAY_,
			    ShadowTraceEnableValue::TOTAL_VALUES)
{
  CALL_TRACE("ShadowTraceEnableSetting()");
}   // $[TI1]


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method:  ~ShadowTraceEnableSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ShadowTraceEnableSetting::~ShadowTraceEnableSetting(void)
{
  CALL_TRACE("~ShadowTraceEnableSetting(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This setting is only changeable for pressure-time curves, while
//  spont type is changeable, and set to either 'TC' (Pcarinal) or 'PA'
//  (Plung).  Otherwise, this setting is inapplicable.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
ShadowTraceEnableSetting::getApplicability(
				      const Notification::ChangeQualifier
					  ) const
{
  CALL_TRACE("getApplicability(qualifierId)");

  Applicability::Id  applicability;

  const Setting*  pPlot1Type =
			SettingsMgr::GetSettingPtr(SettingId::PLOT1_TYPE);
  const Setting*  pSpontType =
			SettingsMgr::GetSettingPtr(SettingId::SUPPORT_TYPE);
  const Setting*  pModeType =
               SettingsMgr::GetSettingPtr(SettingId::MODE);


  // get this non-batch setting's accepted value...
  const DiscreteValue  PLOT1_TYPE_VALUE = pPlot1Type->getAcceptedValue();

  DiscreteValue  spontTypeValue;
  


  if (ContextMgr::GetAcceptedContext().isBdInSafetyPcvState())
  {  // $[TI1] -- must use Safety-PCV value...
    spontTypeValue = SafetyPcvSettingValues::GetValue(SettingId::SUPPORT_TYPE);
  }
  else
  {  // $[TI2] -- accepted values can be used...
    spontTypeValue = pSpontType->getAcceptedValue();
  }

  const Applicability::Id  SPONT_TYPE_APPLIC =
			pSpontType->getApplicability(Notification::ACCEPTED);

  switch (PLOT1_TYPE_VALUE)
  {
  case Plot1TypeValue::PRESSURE_VS_TIME :			// $[TI3]
    if (SPONT_TYPE_APPLIC == Applicability::CHANGEABLE)
    {  // $[TI3.1]
      if (spontTypeValue == SupportTypeValue::ATC_SUPPORT_TYPE  ||
	  spontTypeValue == SupportTypeValue::PAV_SUPPORT_TYPE)
      {  // $[TI3.1.1]
	applicability = Applicability::CHANGEABLE;
      }
      else
      {  // $[TI3.1.2]
	applicability = Applicability::INAPPLICABLE;
      }
    }
    else
    {  // $[TI3.2]
      applicability = Applicability::INAPPLICABLE;
    }
    break;

  case Plot1TypeValue::VOLUME_VS_TIME :
  case Plot1TypeValue::FLOW_VS_TIME :				// $[TI4]
    // don't know yet; need to check plot #2's value...
    {
      const Setting*  pPlot2Type =
			    SettingsMgr::GetSettingPtr(SettingId::PLOT2_TYPE);

      const DiscreteValue  PLOT2_TYPE_VALUE = pPlot2Type->getAcceptedValue();

      switch (PLOT2_TYPE_VALUE)
      {
      case Plot2TypeValue::PRESSURE_VS_TIME :			// $[TI4.1]
	if (SPONT_TYPE_APPLIC == Applicability::CHANGEABLE)
	{  // $[TI4.1.1]
	  if (spontTypeValue == SupportTypeValue::ATC_SUPPORT_TYPE  ||
	      spontTypeValue == SupportTypeValue::PAV_SUPPORT_TYPE)
	  {  // $[TI4.1.1.1]
	    applicability = Applicability::CHANGEABLE;
	  }
	  else
	  {  // $[TI4.1.1.2]
	    applicability = Applicability::INAPPLICABLE;
	  }
	}
	else
	{  // $[TI4.1.2]
	  applicability = Applicability::INAPPLICABLE;
	}
	break;

      case Plot2TypeValue::VOLUME_VS_TIME :
      case Plot2TypeValue::FLOW_VS_TIME :
      case Plot2TypeValue::WOB_GRAPHIC :
      case Plot2TypeValue::NO_PLOT2 :				// $[TI4.2]
	applicability = Applicability::INAPPLICABLE;
	break;

      case Plot2TypeValue::TOTAL_PLOT2_TYPES :
      default :
	// unexpected plot2 type value...
	AUX_CLASS_ASSERTION_FAILURE(PLOT2_TYPE_VALUE);
	break;
      }
    }
    break;
  case Plot1TypeValue::FLOW_VS_VOLUME :	
  case Plot1TypeValue::PRESSURE_VS_VOLUME :			// $[TI5]
    // not applicable to pressure-volume loops...
    applicability = Applicability::INAPPLICABLE;
    break;

  case Plot1TypeValue::TOTAL_PLOT1_TYPES :
  default :
    // unexpected plot1 type value...
    AUX_CLASS_ASSERTION_FAILURE(PLOT1_TYPE_VALUE);
    break;
  }

  /* Not allowed during A/C */
  const DiscreteValue  MODE_TYPE_VALUE = pModeType->getAcceptedValue();
  if( MODE_TYPE_VALUE == ModeValue::AC_MODE_VALUE)
     applicability = Applicability::INAPPLICABLE;
      
  return(applicability);
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getDefaultValue()  [const, virtual]
//
//@ Interface-Description
//  Get the setting's default value.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
ShadowTraceEnableSetting::getDefaultValue(void) const
{
  CALL_TRACE("getDefaultValue()");

  return(DiscreteValue(ShadowTraceEnableValue::ENABLE_SHADOW));  // $[TI1]
}


//==================== M E T H O D   D E S C R I P T I O N =============
//@ Method:  applicabilityUpdate(qualifierId, pSubject)  [virtual]
//
//@ Interface-Description
//  Respond to spont type's applicability changes.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
ShadowTraceEnableSetting::applicabilityUpdate(
			      const Notification::ChangeQualifier qualifierId,
			      const SettingSubject*
					     )
{
  CALL_TRACE("valueUpdate(qualifierId, pSubject)");

  if (qualifierId == Notification::ACCEPTED)
  {  // $[TI1] -- only interested in ACCEPTED changes...
    // spont type's applicability has changed, therefore update this
    // setting's applicability, accordingly...
    updateApplicability(qualifierId);
  }  // $[TI2]
}


//==================== M E T H O D   D E S C R I P T I O N =============
//@ Method:  valueUpdate(qualifierId, pSubject)  [virtual]
//
//@ Interface-Description
//  Respond to subject's value changes, by, possibly, updating this setting's
//  applicability.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
ShadowTraceEnableSetting::valueUpdate(
			      const Notification::ChangeQualifier qualifierId,
			      const SettingSubject*
				     )
{
  CALL_TRACE("valueUpdate(qualifierId, pSubject)");

  if (qualifierId == Notification::ACCEPTED)
  {  // $[TI1] -- only interested in ACCEPTED changes...
    // update this instance's applicability...
    updateApplicability(qualifierId);
  }  // $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: doRetainAttachment(pSubject)  [virtual, const]
//
//@ Interface-Description
//  When task-level, state changes occur, all subjects are notified
//  to detach from all observers.  This is done because the GUI-Apps
//  observers may be overwritten by new class instances, without any
//  explicit destructor/detachment calls.
//
//  This method is defined here to indicate to any of this instance's
//  subjects, not to detach this observer.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean
ShadowTraceEnableSetting::doRetainAttachment(const SettingSubject*) const
{
  CALL_TRACE("doRetainAttachment(pSubject)");

  return(TRUE);
}  // $[TI1]


//==================== M E T H O D   D E S C R I P T I O N ============
//@ Method:  settingObserverInit()  [virtual]
//
//@ Interface-Description
//  This setting needs to monitor the value of Plot1 Type setting,
//  therefore this virtual method is overridden to provide a point during
//  initialization to attach to that (subject) setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
ShadowTraceEnableSetting::settingObserverInit(void)
{
  CALL_TRACE("settingObserverInit()");

  // monitor changes in the plot types' values...
  attachToSubject_(SettingId::PLOT1_TYPE, Notification::VALUE_CHANGED);
  attachToSubject_(SettingId::PLOT2_TYPE, Notification::VALUE_CHANGED);

  // monitor changes in the value and applicability of spont type...
  attachToSubject_(SettingId::SUPPORT_TYPE,
		   Notification::APPLICABILITY_CHANGED);
  attachToSubject_(SettingId::SUPPORT_TYPE,
		   Notification::VALUE_CHANGED);
 
// monitor changes in mode value...
 attachToSubject_(SettingId::MODE, Notification::VALUE_CHANGED);

}  // $[TI1]


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//			[static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
ShadowTraceEnableSetting::SoftFault(const SoftFaultID  softFaultID,
				    const Uint32       lineNumber,
				    const char*        pFileName,
				    const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, ::SETTINGS_VALIDATION,
                          SHADOW_TRACE_ENABLE_SETTING, lineNumber, pFileName,
                          pPredicate);
}
