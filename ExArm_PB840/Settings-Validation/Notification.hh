
#ifndef Notification_HH
#define Notification_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  Notification - Setting Notification.
//---------------------------------------------------------------------
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/Notification.hhv   25.0.4.0   19 Nov 2013 14:27:32   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001   By: sah   Date:  26-Jan-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//     Created to work with new applicability functionality.
//
//====================================================================

//@ Usage-Classes
//@ End-Usage


struct Notification
{
  //@ Type:  SettingChangeId
  // Enumerator representing the types of changes a setting subject
  // will undergo.
  enum SettingChangeId
  {
    VALUE_CHANGED,
    APPLICABILITY_CHANGED,

    NUM_SETTING_CHANGE_TYPES
  };

  //@ Type:  ContextChangeId
  // Enumerator representing the types of changes a context subject
  // will undergo.
  enum ContextChangeId
  {
    BATCH_CHANGED,
    NON_BATCH_CHANGED,

    NUM_CONTEXT_CHANGE_TYPES
  };

  //@ Type:  ChangeQualifier
  // Enumerator representing the source of the above changes.
  enum ChangeQualifier
  {
    ACCEPTED,
    ADJUSTED,

    NUM_CHANGE_QUALIFIERS
  };
};


#endif // Notification_HH 
