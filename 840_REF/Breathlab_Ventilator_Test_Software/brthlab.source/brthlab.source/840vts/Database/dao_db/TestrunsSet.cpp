// TestrunsSet.cpp : implementation file
//

#include "stdafx.h"
#include "daocpvtdb.h"
#include "TestrunsSet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTestrunsSet

IMPLEMENT_DYNAMIC(CTestrunsSet, CDaoRecordset)

CTestrunsSet::CTestrunsSet(CDaoDatabase* pdb)
	: CDaoRecordset(pdb)
{
	//{{AFX_FIELD_INIT(CTestrunsSet)
	m_ID = 0;
	m_pf_flag = 0;
	m_Fact = _T("");
	m_MinRdg = 0.0;
	m_Rdg = 0.0;
	m_MaxRdg = 0.0;
	m_TEST_ID = 0;
	m_Type = 0;
	m_nFields = 8;
	//}}AFX_FIELD_INIT
	m_nDefaultType = dbOpenDynaset;
}


CString CTestrunsSet::GetDefaultDBName()
{
	return _T(DB_PATH_NAME);
}

CString CTestrunsSet::GetDefaultSQL()
{
	return _T("[TestRuns]");
}

void CTestrunsSet::DoFieldExchange(CDaoFieldExchange* pFX)
{
	//{{AFX_FIELD_MAP(CTestrunsSet)
	pFX->SetFieldType(CDaoFieldExchange::outputColumn);
	DFX_Long(pFX, _T("[ID]"), m_ID);
	DFX_Long(pFX, _T("[pf_flag]"), m_pf_flag);
	DFX_Text(pFX, _T("[Fact]"), m_Fact);
	DFX_Double(pFX, _T("[MinRdg]"), m_MinRdg);
	DFX_Double(pFX, _T("[Rdg]"), m_Rdg);
	DFX_Double(pFX, _T("[MaxRdg]"), m_MaxRdg);
	DFX_Short(pFX, _T("[TEST_ID]"), m_TEST_ID);
	DFX_Long(pFX, _T("[Type]"), m_Type);
	//}}AFX_FIELD_MAP
}

/////////////////////////////////////////////////////////////////////////////
// CTestrunsSet diagnostics

#ifdef _DEBUG
void CTestrunsSet::AssertValid() const
{
	CDaoRecordset::AssertValid();
}

void CTestrunsSet::Dump(CDumpContext& dc) const
{
	CDaoRecordset::Dump(dc);
}
#endif //_DEBUG
