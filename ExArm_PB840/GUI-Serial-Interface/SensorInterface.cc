#include "stdafx.h"
//====================================================================
// This is a proprietary work to which Nellcor Puritan Bennett
// corporation of California claims exclusive right.  No part of this
// work may be used, disclosed, reproduced, sorted in an information
// retrieval system, or transmitted by any means, electronic,
// mechanical, photocopying, recording, or otherwise without the prior
// written permission of Nellcor Puritan Bennett Corporation.
//
//     Copyright (c) 2008, Nellcor Puritan Bennett Corporation
//=====================================================================

// =========================== M O D U L E   D E S C R I P T I O N ====
//@ Filename: SensorInterface - Protocol Driver for the Realtime 
//  waveform. 
//---------------------------------------------------------------------
//@ Interface-Description
// provide one-way communications of stored data from Sensor Data 
// buffers. Data is stream out at the selected serial port rate
//---------------------------------------------------------------------
//@ Rationale
//  Provides Real Time Waveform output processing.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The SerialInterface::Task instatiates this class when the 
//  ventilator transitions to a normal (ie. non-SERVICE) state. 
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness of 
//  parameterized data. Outgoing  data is check that it does not 
//  exceed a certain length.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Serial-Interface/vcssrc/SensorInterface.ccv   25.0.4.0   19 Nov 2013 14:12:46   pvcs  $
//
//  Revision: 002   By: gdc    Date: 16-Aug-2010     SCR Number: 6663
//  Project: API/MAT
//  Description:
//  Modified to support for VentSet API. Changed sleep interval to 100ms
//  (from 10ms) since we generate waveforms every breath with a minimum 
//  breath time of 300ms.
// 
//  Revision: 001 	By: erm 	Date: 01/24/2008	SCR Number: 6441
//	Project:  840S
//	Description:
//		Initial version 
//=====================================================================

#include "Sigma.hh"
#include "SensorInterface.hh"
#include <stdio.h>
#include "IpcIds.hh"
#include "SerialPort.hh"
#include "Task.hh"
#include "TaskInfo.hh"
#include "SoftwareOptions.hh"
#include "OsTimeStamp.hh"
#include "WaveformsSubScreen.hh"
#include <string.h>
//@ End-Usage

//@ Code...


static const Uint32 TRANSMITAL_WAIT_TIME = 15000;	 //in millseconds


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SensorInterface [constructor]
//
//@ Interface-Description
//  Constructor for the SensorInterface class. Instantiates the Real 
//  Time waveform protocol interface on the specified SerialPort.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SensorInterface::SensorInterface( SerialPort & rSerialPort,
								  SerialInterface::PortNum portNum ) :
SerialInterface( rSerialPort, portNum )
{
	//$[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SensorInterface [destructor]
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SensorInterface::~SensorInterface()	
{
	// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: startPrint
//
//@ Interface-Description
//  Someone's trying to print on the Sensor interface. Cancel the 
//  operation and continue.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Boolean
SensorInterface::startPrint()
{
// $[TI1]
	return TRUE;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setCurrentPrinter_
//
//@ Interface-Description
//  Someone's trying to assign the printer to a Sensor interface. Do
//  nothing so the assignment has no real effect.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
SensorInterface::setCurrentPrinter_()
{
	// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processRequests_
//
//@ Interface-Description
//  entry Task, triggers on addition to the Sensor Buffer.  
//---------------------------------------------------------------------
//@ Implementation-Description
//  if there is anything in Sensor Buffer
//  create and transmit report 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SensorInterface::processRequests_(void)
{

	SensorDataBuffer::SensorEntry sensorData;
	SensorDataBuffer & rSensorDataBuffer  = WaveformsSubScreen::GetSensorDataBuffer();

	seqNum_ = 0;
	outputBuffer_[0] = '\0';

	while (!areSettingsChanged_)
	{

		if (rSensorDataBuffer.read(&sensorData, sizeof(SensorDataBuffer::SensorEntry )))
		{
			createReport(sensorData);
			write(outputBuffer_,strlen(outputBuffer_), TRANSMITAL_WAIT_TIME);

		}
		Task::Delay(0,100);
	}
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: createReport
//
//@ Interface-Description
//  Incoming Sensor Entry to provide presuure and flow data
//---------------------------------------------------------------------
//@ Implementation-Description
// create report with Breath Start(BS) and Breath End (BE) markers
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void 
SensorInterface::createReport(SensorDataBuffer::SensorEntry &sensorData)
{

	char tempBuffer[50];
	const char BreathEnd[] = "\nBE\n" ;

	int footLen = strlen(BreathEnd);

	//seed with breath start and current Sequence Number
	sprintf(outputBuffer_,"BS, S:%d,",seqNum_++);


	for (int x = 0; x < sensorData.count && (x <  SensorDataBuffer::MAX_SENSOR_DATA_POINTS ); x++)
	{

		int currentTempStrSize = sprintf(tempBuffer,"\n%3.2f, %3.2f", sensorData.flowValues[x], sensorData.pressureValues[x]);

		//Leave room to include the footer
		if ((strlen(outputBuffer_) + (currentTempStrSize + 1 )) < MAX_OUTPUT_BUFFER_SIZE - footLen)
		{
			strcat(outputBuffer_,tempBuffer);
		}
		else
		{

			break;	  //truncate
		}

	}


	strcat(outputBuffer_,BreathEnd);

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//@ Interface-Description
// Implicitly gain execution control when a software assertion macro
// detects and error.
//---------------------------------------------------------------------
//@ Implementation-Description
// Automatically invoked when CLASS_ASSERTION and CLASS_PRE_CONDITON
// macros (among others) detect a fault.  We just call FaultHandler::SoftFault
// to display pertinent information to the user.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

void    
SensorInterface::SoftFault(  const SoftFaultID softFaultID
								 , const Uint32      lineNumber
								 , const char *      pFileName
								 , const char *      pPredicate )
{
	FaultHandler::SoftFault(softFaultID, GUI_SERIAL_INTERFACE,
							GUI_Serial_Interface::SENSOR_INTERFACE_CLASS, 
							lineNumber, pFileName, pPredicate);
}
