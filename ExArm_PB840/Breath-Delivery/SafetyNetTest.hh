#ifndef SafetyNetTest_HH
#define SafetyNetTest_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
//@ Class: SafetyNetTest - Base class for the BD safety net tests.
//---------------------------------------------------------------------
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/SafetyNetTest.hhv   25.0.4.0   19 Nov 2013 14:00:08   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005  By: iv     Date:  08-Jun-1999    DR Number: 5425
//  Project:  ATC
//  Description:
//      Added PSOL_COMMAND_OFFSET.
//
//  Revision: 004  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 003  By:  iv    Date: 23-Nov-1998     DR Number:DCS 5266
//       Project:  Sigma (R8027)
//       Description:
//           Added MAX_EXH_FLOW_TEST_COUNT_LIMIT.
//
//  Revision: 002 By: iv     Date: 10-Mar-1998   DR Number: DCS 5041
//  	Project:  Sigma (R8027)
//		Description:
//			Added errorCode_ data member and access method for it.
//
//  Revision: 001  By:  by    Date:  05-Sep-1995    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Initial version.
//
//=====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"

//@ Usage-Classes
#include "BD_IO_Devices.hh"
#include "Background.hh"
//@ End-Usage

//@ Constant: MAX_PRES_SENSOR_STUCK_LIMIT
//  used as maximum absolute expiratory pressure reading
//  as a test criteria for pressure sensor stuck criterion
extern const Real32 MAX_PRES_SENSOR_STUCK_LIMIT;
 
//@ Constant: MAX_PSOL_STUCK_TEST_FLOW_RATE
//  used as maximum flow rate tolerance for PSOL stuck test
extern const Real32 MAX_PSOL_STUCK_TEST_FLOW_RATE;
 
//@ Constant: SLOWEST_PSOL_COMMAND_GRADIENT
//  used in the calculation of low PSOL command limits
extern const Real32 SLOWEST_PSOL_COMMAND_GRADIENT;
 
//@ Constant: MIN_LOW_PSOL_COMMAND
//  used in the calculation of low PSOL command limits
extern const Real32 MIN_LOW_PSOL_COMMAND;
 
//@ Constant: QUICKEST_PSOL_COMMAND_GRADIENT
//  used in the calculation of high PSOL command limits
extern const Real32 QUICKEST_PSOL_COMMAND_GRADIENT;
 
//@ Constant: MAX_HIGH_PSOL_COMMAND
//  used in the calculation of high PSOL command limits
extern const Real32 MAX_HIGH_PSOL_COMMAND;

//@ Constant: PSOL_COMMAND_AT_10_LPM
//  DAC value for PSOL at 10 LPM
extern const AdcCounts PSOL_COMMAND_AT_10_LPM;

//@ Constant: MIN_FLOW_TEST_COUNT_LIMIT
//  minimum ADC count for flow sensor test
extern const AdcCounts MIN_FLOW_TEST_COUNT_LIMIT;

//@ Constant: MAX_EXH_FLOW_TEST_COUNT_LIMIT
//  maximum ADC count for exhalation flow sensor test
extern const AdcCounts MAX_EXH_FLOW_TEST_COUNT_LIMIT;

//@ Constant: MAX_FLOW_TEST_LIMIT 
//  maximum ADC count for flow sensor test
extern const Int16 MAX_FLOW_TEST_LIMIT;

//@ Constant: MIN_PSOL_STUCK_OPEN_FLOW_LPM 
// minimum PSOL stuck open test flow rate
extern const Real32 MIN_PSOL_STUCK_OPEN_FLOW_LPM;

//@ Constant: MAX_PSOL_LEAK_LPM 
// maximum PSOL leak rate
extern const Real32 MAX_PSOL_LEAK_LPM;

//@ Constant: MAX_PSOL_CLOSED_FLOW_LPM 
// maximum PSOL closed flow rate tolerance
extern const Real32 MAX_PSOL_CLOSED_FLOW_LPM;
   
//@ Constant: MAX_CYCLES_PSOL_STUCK_OPEN 
// maximum time tolerance for PSOL stuck open
extern const Int16 MAX_CYCLES_PSOL_STUCK_OPEN;

//@ Constant: MAX_CYCLES_PSOL_STUCK
// maximum time tolerance for PSOL stuck 
extern const Int16 MAX_CYCLES_PSOL_STUCK;

//@ Constant: MAX_PRESSURE_OOR_CYCLES 
// maximum time tolerance for pressure sensor OOR
extern const Int16 MAX_PRESSURE_OOR_CYCLES;

//@ Constant: MAX_FLOW_HIGH_OOR_CYCLES
// maximum flow rate for OOR high flow
extern const Int16 MAX_FLOW_HIGH_OOR_CYCLES;
 
//@ Constant: MAX_FLOW_LOW_OOR_CYCLES
// maximum flow rate for OOR low flow
extern const Int16 MAX_FLOW_LOW_OOR_CYCLES;
  
//@ Constant: MAX_PRES_TEST_LIMIT
//  Maximum test limit for OOR pressure value
extern const Real32 MAX_PRES_TEST_LIMIT;

//@ Constant: DELAY_AFTER_SUCTIONING_CYCLES
// number of cycles to after suction manuever before background check commences.
extern const Int16 DELAY_AFTER_SUCTIONING_CYCLES;         // 10 seconds

//@ Constant: SUCTIONING_CYCLES
// number of cycles to declare suction manuever is occurring.
extern const Int16 SUCTIONING_CYCLES;              // 40ms

//@ Constant: PRESSURE_SENSOR_SUCTION_CRITERIA
//  criteria for pressure sensor suctioning manuever detection
extern const Real32 PRESSURE_SENSOR_SUCTION_CRITERIA;
//@ Constant: PRESSURE_SENSOR_STUCK_CRITERIA
//  Concurrent criteria for pressure sensor stuck low detection
extern const Real32 PRESSURE_SENSOR_STUCK_CRITERIA;

//@ Constant: MIN_PRES_TEST_LIMIT
//  Minimum test limit for OOR pressure value
extern const Real32 MIN_PRES_TEST_LIMIT;
   
//@ Constant: PRESSURE_STUCK_TEST_PRES_CRITERIA 
// threshold for testing pressure sensor stuck
extern const Real32 PRESSURE_STUCK_TEST_PRES_CRITERIA;
  
//@ Constant: PRESSURE_STUCK_TEST_FLOW_CRITERIA
// threshold for testing flow sensor stuck
extern const Real32 PRESSURE_STUCK_TEST_FLOW_CRITERIA;
   
//@ Constant: MAX_PRESSURE_STUCK_TEST_CYCLES 
//  Maximum number of cycles the pressure sensor stuck
//  criteria is met before an event declared
extern const Int16 MAX_PRESSURE_STUCK_TEST_CYCLES;

//@ Constant: PSOL_CURRENT_TO_DAC_SCALE 
// PSOL current to DAC factor
extern const Real32 PSOL_CURRENT_TO_DAC_SCALE;

//@ Constant: PSOL_COMMAND_OFFSET
// offset to add to the high limit of the PSOL command
extern const DacCounts PSOL_COMMAND_OFFSET;

class SafetyNetTest
{
    public:

		enum SuctioningStateMachine
		{
		NO_SUCTIONING,
		SUCTIONING_INPROGRESS,
		SUCTIONING_DELAY
		};
        SafetyNetTest( const BkEventName eventId, const Int16 maxCycles );
        virtual ~SafetyNetTest( void );

        static void SoftFault( const SoftFaultID softFaultID,
			       const Uint32      lineNumber,
			       const char*       pFileName  = NULL,
			       const char*       pPredicate = NULL );
                                  
        inline void setBackgndCheckFailed( Boolean checkFailed );
        inline Boolean getBackgndCheckFailed( void ) const;
        inline Uint16 getErrorCode( void ) const;
        inline Boolean getIsSuctioning( void ) const;
        inline SafetyNetTest::SuctioningStateMachine getSuctioningState( void ) const;
                                  
        virtual BkEventName  checkCriteria( void ) = 0;    // pure virtual function

    protected:

        Real32 getCurrentDesiredO2Flow_( void ) const;
        Real32 getCurrentDesiredAirFlow_( void ) const;
		//returns true if were suctioning
		Boolean IsSuctioning( const Real32 exhPressure,  
							  const Real32 inspPressure);

        //@ Data-Member: suctioningState
        // The suction state machine variable
		SuctioningStateMachine suctioningState_;

        //@ Data-Member: numCyclesAfterSuctionCriteriaMet_
        // The number of consecutive cycles (BD task cycles or breath cycles
        // depending upon the safety net test) that the delay crteriacheck has 
		// been met
        Uint32 numCyclesAfterSuctionCriteriaMet_;

        //@ Data-Member: numCyclesSuctionCriteriaMet_
        // The number of consecutive cycles (BD task cycles or breath cycles
        // depending upon the safety net test) that the delay crteriacheck has 
		// been met
        Uint32 numCyclesSuctionCriteriaMet_;

        //@ Data-Member: backgndEventId_
        // Background Event Identifier used to notify the Background
        // Subsystem which Background Test failed
        BkEventName backgndEventId_;

        //@ Data-Member: backgndCheckFailed_
        // Background Check Failed identifier, which indicates whether or
        // not the test has already failed.  Once a test has failed, it
        // is no longer executed since background detected failures do
        // not auto-reset
        Boolean backgndCheckFailed_;

        //@ Data-Member: maxCyclesCriteriaMet_
        // The number of consecutive cycles (BD task cycles or breath cycles 
        // depending upon the safety net test) that the check must indicate 
        // that a problem exists before the Background subsystem is notified
        Int16 maxCyclesCriteriaMet_;

        //@ Data-Member: numCyclesCriteriaMet_
        // The number of consecutive cycles (BD task cycles or breath cycles
        // depending upon the safety net test) that the check has indicated that
        // a problem may exist; once this count reaches maxCyclesCriteriaMet_,
        // the Background subsystem is notified
        Int16 numCyclesCriteriaMet_;

        //@ Data-Member: errorCode_
        // error code to report when a fault occurs
        Uint16 errorCode_;

        //@ Data-Member: suctioning_
        // TRUE if suctioning is occuring
		Boolean isSuctioning_;

    private:

        // these methods are purposely declared, but not implemented...
        SafetyNetTest( void );                      // not implemented... 
        SafetyNetTest( const SafetyNetTest& );      // not implemented...
        void operator=( const SafetyNetTest& );     // not implemented...

};

// Inlined methods
#include "SafetyNetTest.in"

#endif // SafetyNetTest_HH 


