#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1994, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: SafetyVentilationSubScreen - Automatically displays the current
// "Safety PCV" ventilation settings.
//---------------------------------------------------------------------
//@ Interface-Description
// This class is a container, derived from the GUI-Foundation Container
// class.  This class contains textual fields, lines, boxes, and numeric
// setting buttons.
//
// The layout of the display mimics the MainSettingsArea on the lower
// screen.  Thus, there is a "mode indicator" bar across the top of the
// display for showing the mode, mandatory type, and trigger type
// setting values (IBW is not shown here and support type is not
// applicable).  Also, there is a set of inactive "main setting" buttons for
// each of the following Safety PCV settings:
// >Von
//	1.	Inspiratory pressure.
//	2.	Inspiratory time
//	3.	Oxygen percentage.
//	4.	PEEP.
//	5.	Pressure sensitivity.
//	6.	Respiratory rate.
//	7.	Expiratory sensitivity.
// >Voff
// The activate() method takes care of the details of initializing the
// numeric values displayed in these inactive main setting buttons.
//---------------------------------------------------------------------
//@ Rationale
// This class is dedicated to managing the display of the
// Safety PCV subscreen on the UpperSubScreenArea.
//---------------------------------------------------------------------
//@ Implementation-Description
// To support its display, this area displays textual fields, line
// objects, box objects, and flattened inactive setting buttons.  The
// display is "static" and does not change -- i.e., the setting buttons
// are not registered with the setting registrar.  The setting button
// objects are used in their "flat mode" for display purposes only.
//---------------------------------------------------------------------
//@ Fault-Handling
//  none
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/SafetyVentilationSubScreen.ccv   25.0.4.0   19 Nov 2013 14:08:20   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006   By: srp   Date:  24-Apr-2002    DR Number: 5327 & 5908
//  Project:  NeoMode
//  Description:
//      Added Modification log. Updated for NeoMode  03/01/00
//
//  Revision: 005   By: sah    Date: 24-Oct-2000     DCS Number:  5755
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      *  swapped placement of Ti and Pi setting buttons
//
//  Revision: 004  By: sah  Date: 08-Feb-1999  DR Number: 5314
//  Project:  ATC (R8027)
//  Description:
//      The interface of 'SafetyPcvSettingValues' to a more generic
//	interface.  (Needed for internal settings functionlity involved
//	with new dynamic applicability mechanism.)
//
//  Revision: 003  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 002  By:  yyy    Date:  24-JUL-97    DR Number: 2265
//    Project:  Sigma (R8027)
//    Description:
//      24-JUL-97  Changed SUBTITLE_X_ from 90 to 0 due to changes in
//		MiscStrs::SAFETY_SUBSCREEN_SUBTITLE per translation.
//
//  Revision: 001  By:  wks    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "SafetyVentilationSubScreen.hh"

//@ Usage-Classes
#include "SafetyPcvSettingValues.hh"
#include "MiscStrs.hh"
#include "SettingId.hh"
#include "UpperSubScreenArea.hh"
#include "GuiApp.hh"
//@ End-Usage

//@ Code...

static const Int32 BUTTON_WIDTH_ = 98;
static const Int32 BUTTON_HEIGHT_ = 58;
static const Int32 NUMERIC_VALUE_CENTER_X_ = 36;
static const Int32 NUMERIC_VALUE_CENTER_Y_ = 39;
static const Int32 MODE_INDICATOR_X1_ = 21;
static const Int32 MODE_INDICATOR_Y1_ = 82;
static const Int32 MODE_INDICATOR_WIDTH_ = (BUTTON_WIDTH_ - 1) * 6 + 1;
static const Int32 MODE_INDICATOR_HEIGHT_ = 25;
static const Int32 BUTTON_AREA_X1_ = MODE_INDICATOR_X1_;
static const Int32 BUTTON_AREA_Y1_ = 
				MODE_INDICATOR_Y1_ + MODE_INDICATOR_HEIGHT_ - 1;
static const Int32 LEFT_WING_X1_ = 140;
static const Int32 LEFT_WING_X2_ = 187;
static const Int32 RIGHT_WING_X1_ = 248;
static const Int32 RIGHT_WING_X2_ = 295;
static const Int32 WING_Y_ = 93;
static const Int32 WING_PEN_ = 2;
static const Int32 WINGTIP_WIDTH_ = 8;
static const Int32 WINGTIP_HEIGHT_ = 7;
static const Int32 TITLE_Y_ = 10;
static const Int32 SUBTITLE_Y_ = 52;
static const Int32 FOOTNOTE_X_ = 20;
static const Int32 FOOTNOTE_Y_ = 242;

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SafetyVentilationSubScreen()  [Constructor]
//
//@ Interface-Description
// Constructor.  The 'pSubScreenArea' parameter is the pointer to the
// parent SubScreenArea which contains this subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Initializes all members and initializes the colors, sizes, and
// positions of all graphical objects and setting buttons.  Adds all
// graphical objects and setting buttons to the parent container for
// display.  The setting buttons are flattened so that they cannot be
// selected by the user.  Note the setting buttons are constructed
// without the normal setting id's, message id's, or subscreen
// associations.
//---------------------------------------------------------------------
//@ PreCondition
// The given `pSubScreenArea' pointer must not be NULL.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SafetyVentilationSubScreen::SafetyVentilationSubScreen(SubScreenArea*
													pSubScreenArea) :
	SubScreen(pSubScreenArea),
	titleText_(MiscStrs::SAFETY_SUBSCREEN_TITLE),
	subTitleText_(MiscStrs::SAFETY_SUBSCREEN_SUBTITLE),
	footnoteText_(MiscStrs::SAFETY_SUBSCREEN_FOOTNOTE),
	modeText_(MiscStrs::MODE_SAFETY_PCV_TITLE),
	mandatoryTypeText_(MiscStrs::MAND_TYPE_APNEA_VENT_PCV_TITLE),
	leftWingtip_(LEFT_WING_X1_ - WINGTIP_WIDTH_, WING_Y_ + WINGTIP_HEIGHT_,
				LEFT_WING_X1_, WING_Y_, WING_PEN_),
	leftWing_(LEFT_WING_X1_, WING_Y_, LEFT_WING_X2_, WING_Y_, WING_PEN_),
	rightWing_(RIGHT_WING_X1_, WING_Y_, RIGHT_WING_X2_, WING_Y_, WING_PEN_),
	rightWingtip_(RIGHT_WING_X2_, WING_Y_,
				RIGHT_WING_X2_ + WINGTIP_WIDTH_, WING_Y_ + WINGTIP_HEIGHT_, 
				WING_PEN_),
	triggerTypeText_(MiscStrs::TRIGGER_TYPE_SAFETY_PCV_TITLE),
	modeBackground_(MODE_INDICATOR_X1_, MODE_INDICATOR_Y1_,
				MODE_INDICATOR_WIDTH_, MODE_INDICATOR_HEIGHT_),
	buttonBackground_(BUTTON_AREA_X1_, BUTTON_AREA_Y1_, MODE_INDICATOR_WIDTH_, 
				BUTTON_HEIGHT_*2 - 1),
	flowAccelerationButton_(BUTTON_AREA_X1_, BUTTON_AREA_Y1_ +
				BUTTON_HEIGHT_ - 1, BUTTON_WIDTH_, BUTTON_HEIGHT_,
				Button::DARK, 1, Button::SQUARE, Button::NO_BORDER,
				TextFont::TWENTY_FOUR,
				NUMERIC_VALUE_CENTER_X_, NUMERIC_VALUE_CENTER_Y_,
				MiscStrs::FLOW_ACCELERATION_BUTTON_TITLE_LARGE,
				MiscStrs::PERCENT_UNITS_LARGE,
				SettingId::NULL_SETTING_ID, NULL, NULL, TRUE),
	inspiratoryPressureButton_(BUTTON_AREA_X1_ + (BUTTON_WIDTH_-1), 
				BUTTON_AREA_Y1_, 
				BUTTON_WIDTH_, BUTTON_HEIGHT_, Button::DARK, 1, Button::SQUARE,
				Button::NO_BORDER, TextFont::TWENTY_FOUR,
				NUMERIC_VALUE_CENTER_X_ - 4, NUMERIC_VALUE_CENTER_Y_,
				MiscStrs::INSPIRATORYPRESSURE_BUTTON_TITLE_LARGE,
				GuiApp::GetPressureUnitsLarge(),
				SettingId::NULL_SETTING_ID, NULL, NULL, TRUE),
	inspiratoryTimeButton_(BUTTON_AREA_X1_ + (BUTTON_WIDTH_-1)*2, BUTTON_AREA_Y1_, 
				BUTTON_WIDTH_, BUTTON_HEIGHT_, Button::DARK, 1, Button::SQUARE,
				Button::NO_BORDER, TextFont::TWENTY_FOUR,
				NUMERIC_VALUE_CENTER_X_, NUMERIC_VALUE_CENTER_Y_,
				MiscStrs::INSPIRATORY_TIME_BUTTON_TITLE_LARGE,
				MiscStrs::SEC_UNITS_LARGE,
				SettingId::NULL_SETTING_ID, NULL, NULL, TRUE),
	oxygenPercentageButton_(BUTTON_AREA_X1_ + (BUTTON_WIDTH_-1)*5, BUTTON_AREA_Y1_, 
				BUTTON_WIDTH_, BUTTON_HEIGHT_, Button::DARK, 1, Button::SQUARE,
				Button::NO_BORDER, TextFont::TWENTY_FOUR,
				NUMERIC_VALUE_CENTER_X_, NUMERIC_VALUE_CENTER_Y_,
				MiscStrs::OXYGEN_PERCENTAGE_BUTTON_TITLE_LARGE,
				MiscStrs::PERCENT_UNITS_LARGE,
				SettingId::NULL_SETTING_ID, NULL, NULL, TRUE),
	peepButton_(BUTTON_AREA_X1_ + (BUTTON_WIDTH_-1)*5, 
				BUTTON_AREA_Y1_ + (BUTTON_HEIGHT_-1),
				BUTTON_WIDTH_, BUTTON_HEIGHT_, Button::DARK, 1, Button::SQUARE,
				Button::NO_BORDER, TextFont::TWENTY_FOUR,
				NUMERIC_VALUE_CENTER_X_, NUMERIC_VALUE_CENTER_Y_,
				MiscStrs::PEEP_BUTTON_TITLE_LARGE,
				GuiApp::GetPressureUnitsLarge(),
				SettingId::NULL_SETTING_ID, NULL, NULL, TRUE),
	pressureSensitivityButton_(BUTTON_AREA_X1_ + (BUTTON_WIDTH_-1)*4, 
				BUTTON_AREA_Y1_, 
				BUTTON_WIDTH_, BUTTON_HEIGHT_, Button::DARK, 1, Button::SQUARE,
				Button::NO_BORDER, TextFont::TWENTY_FOUR,
				NUMERIC_VALUE_CENTER_X_, NUMERIC_VALUE_CENTER_Y_,
				MiscStrs::PRESSURE_SENSITIVITY_BUTTON_TITLE_LARGE,
				GuiApp::GetPressureUnitsLarge(),
				SettingId::PRESS_SENS, NULL, NULL, TRUE),
	respiratoryRateButton_(BUTTON_AREA_X1_, BUTTON_AREA_Y1_, 
				BUTTON_WIDTH_, BUTTON_HEIGHT_, Button::DARK, 1, Button::SQUARE,
				Button::NO_BORDER, TextFont::TWENTY_FOUR,
				NUMERIC_VALUE_CENTER_X_, NUMERIC_VALUE_CENTER_Y_,
				MiscStrs::RESPIRATORY_RATE_BUTTON_TITLE_LARGE,
				MiscStrs::ONE_PER_MIN_UNITS_LARGE,
				SettingId::NULL_SETTING_ID, NULL, NULL, TRUE)
{
	CALL_TRACE("SafetyVentilationSubScreen::SafetyVentilationSubScreen"
													"(pSubScreenArea)");
	CLASS_PRE_CONDITION(pSubScreenArea != NULL);


	// Size and position the area
	setX(0);
	setY(0);
	setWidth(UpperSubScreenArea::CONTAINER_WIDTH);
	setHeight(UpperSubScreenArea::CONTAINER_HEIGHT);

	// Set subscreen background color
	setColor(Colors::LIGHT_BLUE);
	setFillColor(Colors::LIGHT_BLUE);

	//
	// Set colors, then add fixed backgrounds and fixed mode indicator bar text.
	//
	buttonBackground_.setColor(Colors::DARK_BLUE);
	buttonBackground_.setFillColor(Colors::DARK_BLUE);
	addDrawable(&buttonBackground_);
	modeBackground_.setColor(Colors::BLACK);
	modeBackground_.setFillColor(Colors::BLACK);
	addDrawable(&modeBackground_);
	titleText_.setY(TITLE_Y_);
	titleText_.setColor(Colors::BLACK);
	addDrawable(&titleText_);
	titleText_.positionInContainer(GRAVITY_CENTER);
	subTitleText_.setY(SUBTITLE_Y_);
	subTitleText_.setColor(Colors::BLACK);
	addDrawable(&subTitleText_);
	subTitleText_.positionInContainer(GRAVITY_CENTER);
	footnoteText_.setX(FOOTNOTE_X_);
	footnoteText_.setY(FOOTNOTE_Y_);
	footnoteText_.setColor(Colors::BLACK);
	addDrawable(&footnoteText_);
	modeText_.setX(MODE_INDICATOR_X1_);
	modeText_.setY(MODE_INDICATOR_Y1_);
	modeText_.setColor(Colors::WHITE);
	addDrawable(&modeText_);
	leftWingtip_.setColor(Colors::WHITE);
	addDrawable(&leftWingtip_);
	leftWing_.setColor(Colors::WHITE);
	addDrawable(&leftWing_);
	rightWing_.setColor(Colors::WHITE);
	addDrawable(&rightWing_);
	rightWingtip_.setColor(Colors::WHITE);
	addDrawable(&rightWingtip_);
	mandatoryTypeText_.setX(MODE_INDICATOR_X1_);
	mandatoryTypeText_.setY(MODE_INDICATOR_Y1_);
	mandatoryTypeText_.setColor(Colors::WHITE);
	addDrawable(&mandatoryTypeText_);
	triggerTypeText_.setX(MODE_INDICATOR_X1_);
	triggerTypeText_.setY(MODE_INDICATOR_Y1_);
	triggerTypeText_.setColor(Colors::WHITE);
	addDrawable(&triggerTypeText_);

	//
	// Add all of the buttons and set them to the flat state.
	//
	addDrawable(&flowAccelerationButton_);
	flowAccelerationButton_.setToFlat();
	addDrawable(&inspiratoryPressureButton_);
	inspiratoryPressureButton_.setToFlat();
	addDrawable(&inspiratoryTimeButton_);
	inspiratoryTimeButton_.setToFlat();
	addDrawable(&oxygenPercentageButton_);
	oxygenPercentageButton_.setToFlat();
	addDrawable(&peepButton_);
	peepButton_.setToFlat();
	addDrawable(&pressureSensitivityButton_);
	pressureSensitivityButton_.setToFlat();
	addDrawable(&respiratoryRateButton_);
	respiratoryRateButton_.setToFlat();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~SafetyVentilationSubScreen  [Destructor]
//
//@ Interface-Description
// Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  n/a
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SafetyVentilationSubScreen::~SafetyVentilationSubScreen(void)
{
	CALL_TRACE("SafetyVentilationSubScreen::~SafetyVentilationSubScreen(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activate()
//
//@ Interface-Description
// Prepares this subscreen for display.
//---------------------------------------------------------------------
//@ Implementation-Description
// Initializes the displayed values for all of the inactive setting buttons.
// $[01209] The safety PCV in progress subscreen shall display the ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SafetyVentilationSubScreen::activate(void)
{
	CALL_TRACE("SafetyVentilationSubScreen::activate(void)");

	BoundedValue settingValue;


	// Respiratory Rate
	settingValue = SafetyPcvSettingValues::GetValue(SettingId::RESP_RATE);
	respiratoryRateButton_.setValue(settingValue.value, settingValue.precision);

	// Oxygen Percentage 	
	settingValue = SafetyPcvSettingValues::GetValue(SettingId::OXYGEN_PERCENT);
	oxygenPercentageButton_.setValue(settingValue.value,
														settingValue.precision);

	// Peep
	settingValue = SafetyPcvSettingValues::GetValue(SettingId::PEEP);
	peepButton_.setValue(settingValue.value, settingValue.precision);

	// Inspiratory Time
	// This value is displayed in seconds but stored in milliseconds.  We must
	// decide which precision to display the inspiratory time value in.  Simply
	// scale the stored precision down by 1000.
	Precision inspTimePrecision;
	settingValue = SafetyPcvSettingValues::GetValue(SettingId::INSP_TIME);
	switch (settingValue.precision)
	{
	case THOUSANDS:										// $[TI1]
		inspTimePrecision = ONES;
		break;

	case HUNDREDS:										// $[TI2]
		inspTimePrecision = TENTHS;
		break;

	case TENS:											// $[TI3]
		inspTimePrecision = HUNDREDTHS;
		break;

	default:						// All the rest default to THOUSANDTHS
		inspTimePrecision = THOUSANDTHS;				// $[TI4]
		break;
	}

	inspiratoryTimeButton_.setValue(settingValue.value / 1000,
															inspTimePrecision);

	// Inspiratory Pressure
	settingValue = SafetyPcvSettingValues::GetValue(SettingId::INSP_PRESS);
	inspiratoryPressureButton_.setValue(settingValue.value,
														settingValue.precision);

	// Pressure Sensitivity
	settingValue = SafetyPcvSettingValues::GetValue(SettingId::PRESS_SENS);
	pressureSensitivityButton_.setValue(settingValue.value,
														settingValue.precision);

	// Expiratory Sensitivity
	settingValue =
			  SafetyPcvSettingValues::GetValue(SettingId::FLOW_ACCEL_PERCENT);
	flowAccelerationButton_.setValue(settingValue.value,
														settingValue.precision);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: deactivate()
//
//@ Interface-Description
// Prepares this subscreen for removal from the display.  Does nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
// This an empty implementation of a pure virtual function.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SafetyVentilationSubScreen::deactivate(void)
{
	CALL_TRACE("SafetyVentilationSubScreen::deactivate(void)");

	// do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
SafetyVentilationSubScreen::SoftFault(const SoftFaultID  softFaultID,
									 const Uint32       lineNumber,
									 const char*        pFileName,
									 const char*        pPredicate)  
{
	CALL_TRACE("SafetyVentilationSubScreen::SoftFault(softFaultID, "
			   "lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, SAFETYVENTILATIONSUBSCREEN,
							lineNumber, pFileName, pPredicate);
}
