#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: Arts - provides special hardware outputs required for ARTS testing.
//---------------------------------------------------------------------
//@ Interface-Description
//		This is a static class.  A single method is provided to update to
//		the metabolics DAC port with the desired flow and write to a bit
//		that represents the current phase of the breath.
//---------------------------------------------------------------------
//@ Rationale
//		This class implements the requirements for ARTS testing.
//---------------------------------------------------------------------
//@ Implementation-Description
//		This class is provided to support ARTS testing ONLY.  The metabolics
//		DAC port has dual usage.  It is used to output data to metabolics and
//		for ARTS testing.  Having both functions simultaneously is not
//		recommended since it will overwrite the data written from the other.
//---------------------------------------------------------------------
//@ Fault-Handling
//		N/A
//---------------------------------------------------------------------
//@ Restrictions
//		none
//---------------------------------------------------------------------
//@ Invariants
//		none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/Arts.ccv   25.0.4.0   19 Nov 2013 13:59:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 007   By: rhj    Date: 19-Jan-2011   SCR Number: 6619 
//  Project:  PROX
//  Description:
//     Added a condition where if a prox startup failure exists 
//     disable the I:E signals to prevent autozeros and purges.
//
//  Revision: 006   By: syw   Date:  15-Jun-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//		PAV project-related changes:
//		Changes to accommodate PAV_INSPIRATORY_PAUSE enum.
//
//  Revision: 005  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 004  By: iv    Date:  22-Jul-1998    DR Number: None
//  	Project:  Sigma (R8027)
//		Description:
//          Bilevel initial version.
//
//  Revision: 003  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 002  By: syw    Date:  19-Dec-1996    DR Number: DCS 1603
//  	Project:  Sigma (R8027)
//		Description:
//			Changed Register to BitAccessRegister.  Call requestBitUpdate()
//			instead of write().  Change mask to bit location.
//
//  Revision: 001  By:  syw    Date:  6-Nov-1996    DR Number: INITIAL RELEASE
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//=====================================================================

#include "Arts.hh"

#include "ControllersRefs.hh"
#include "RegisterRefs.hh"
#include "PhaseRefs.hh"

//@ Usage-Classes

#include "VolumeController.hh"
#include "BitAccessGpio.hh"
#include "BreathPhase.hh"
#include "TaskControlAgent.hh"
#include "SigmaState.hh"
#include "BreathMiscRefs.hh"
#include "ProxManager.hh"
#include "Psol.hh"
#include "ValveRefs.hh"
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: UpdateOutputs
//
//@ Interface - Description
//		This static method has no arguments and returns nothing.  This
//		method is called to output to the metabolics DAC port the desired
//		flow and to toggle a bit to indicate the breath phase (inspiration or
//		exhalation).  This method is intended to be called every BD cycle.
//-----------------------------------------------------------------------
//@ Implementation - Description
//		The desired flow is obtained from the volume controller if the
//		breath phase is volume control, otherwise it is obtained from the
//		flow controller.  A bit is set high during inspiration, otherwise
//		it is set low.
//-----------------------------------------------------------------------
//@ PreCondition
//		none
//-----------------------------------------------------------------------
//@ PostCondition
//		none
//@ End - Method
//=======================================================================
void
Arts::UpdateOutputs( void)
{
	CALL_TRACE("Arts::UpdateDesiredFlow( void)") ;

	BreathPhase* pBreathPhase = BreathPhase::GetCurrentBreathPhase() ;
	
	// E600 - Last set DAC values of Air and O2 are added
	Int16 desiredFlowCommand = RAirPsol.getPsolCommand() + RO2Psol.getPsolCommand();

	if (desiredFlowCommand < 0)
	{
    	// $[TI2.1]
		desiredFlowCommand = 0 ;
	}
	else if (desiredFlowCommand > MAX_COUNT_VALUE)
	{
    	// $[TI2.2]
		desiredFlowCommand = MAX_COUNT_VALUE ;
	}  	// implied else $[TI2.3]
	
	// E600 Set Desired flow DAC value
	RDesiredFlowPsol.updatePsol(desiredFlowCommand);

	// If a prox startup failure exists hijack the I:E bit
	// this in turn keeps the prox board from performing purges and autozeros
	// when we can't communicate
	if (!RProxManager.getstartupFailure())
	{
    	if ( TaskControlAgent::GetGuiState() != STATE_SST )
    	{
    		if (pBreathPhase->getPhaseType() == BreathPhaseType::INSPIRATION ||
    			pBreathPhase->getPhaseType() == BreathPhaseType::PAV_INSPIRATORY_PAUSE ||
    			pBreathPhase->getPhaseType() == BreathPhaseType::INSPIRATORY_PAUSE)
    		{
    			// $[TI3.1]
    			// E600 - set bit high
				RBitAccessGpio.requestBitUpdate( DOUT_IE_PHASE_OUT,  BitAccessGpio::ON);
				
				// TODO E600_LL: for timing debug, removed later
				RBitAccessGpio.requestBitUpdate( DOUT_LED5,  BitAccessGpio::ON);
    		}
    		else
    		{
    			// $[TI3.2]
    			// E600 - set bit low
    			RBitAccessGpio.requestBitUpdate( DOUT_IE_PHASE_OUT,  BitAccessGpio::OFF);
				
				// TODO E600_LL: for timing debug, removed later
				RBitAccessGpio.requestBitUpdate( DOUT_LED5,  BitAccessGpio::OFF);
    		}
    	}
	}
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
Arts::SoftFault( const SoftFaultID  softFaultID,
                   		const Uint32       lineNumber,
		   				const char*        pFileName,
		   				const char*        pPredicate)
{
	CALL_TRACE("Arts::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  	FaultHandler::SoftFault( softFaultID, BREATH_DELIVERY, ARTS,
  							 lineNumber, pFileName, pPredicate) ;
}

#endif // defined(SIGMA_DEVELOPMENT)


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//=====================================================================
//
//  Private Methods...
//
//=====================================================================

