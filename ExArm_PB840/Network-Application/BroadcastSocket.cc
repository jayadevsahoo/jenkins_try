#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@  Class: BroadcastSocket - Class definition that provides 
//  interface to a UDP socket that can broadcast data.
//---------------------------------------------------------------------
//@ Interface-Description
//    The broadcast Socket class provides interfaces to a UDP socket
//    that can broadcast data and receive UDP data packets.
//    The class figures out the Sigma Network broadcast address 
//    based on the network IP address of the host machine.
//---------------------------------------------------------------------
//@ Rationale
//   This class enables network data broadcasting on the sigma
//   subnet. Broadcasting capability relies on a UDP communication
//   protocol and all other network socket classes only support
//   TCP/IP communication method.
//---------------------------------------------------------------------
//@ Implementation-Description
//   The BroadcastSocket is a UDP socket and its communication mode
//   is set to non-blocking mode. This is to avoid an Application
//   task which broadcasts a network message has to be put into
//   a sleep state while lower level Stack-Ware code completes
//   transmitting data to the network successfully. In a non-blocking
//   IO mode, a transmit socket library call returns immediately
//   after all the user data is copied into the Stack-Ware system
//   buffer. Unlike other TCP/IP socket types, a network message
//   header and its data are sent out within one UDP packet just by
//   calling the 'sendto' library call once.
//---------------------------------------------------------------------
//@ Fault-Handling
//  Some of the repeated socket library errors are recorded to
//  the non-volatile diagnostic memory. All the failed requests
//  return an error status to calling methods. Each method follows
//  the contract programming practice to generate a soft fault in
//  the case of software logic error. 
//---------------------------------------------------------------------
//@ Restrictions
//    The BroadcastSocket is designed mainly to support broadcasting
//    capability. If an application wants to use it as a generic
//    UDP socket, it can be done by overriding reInit method and
//    sendMsg method.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Network-Application/vcssrc/BroadcastSocket.ccv   25.0.4.0   19 Nov 2013 14:14:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 009  By: gdc     Date: 12-Jan-2011  SCR Number: 6734
//  Project:  XENA2
//  Description:
//      Removed assert on NULL data pointer to allow header-only 
//      broadcast messages.
//
//  Revision: 008  By: gdc     Date: 04-Jan-2011  SCR Number: 6729
//  Project:  XENA2
//  Description:
//      Broadcast messages enabled for ENGINEERING data keys only. 
//
//  Revision: 007  By: gdc     Date: 30-Dec-2010  SCR Number: 6697
//  Project:  XENA2
//  Description:
//      Removed code to broadcast BD IP address as this logic is no 
//      longer used and consumes all mbufs causing a BD reset
//      during communication loss with the GUI.
//
//  Revision: 006  By: gdc     Date: 30-Dec-2010  SCR Number: 6717
//  Project:  XENA2
//  Description:
//      Implemented openUdpBroadcast to properly set up UDP broadcast 
//      socket.
//
//  Revision: 005  By: gdc     Date: 30-Dec-2010  SCR Number: 6721
//  Project:  XENA2
//  Description:
//      Use stack variable for network destination address so broadcastMsg
//      can be called from multiple threads.
//
//  Revision: 004  By: gdc     Date: 30-Dec-2010  SCR Number: 6716
//  Project:  XENA2
//  Description:
//      Use thread-safe socket library.
//
//  Revision: 003  By: gdc     Date: 30-Dec-2010  SCR Number: 6712
//  Project:  XENA2
//  Description:
//      Use socket sendmsg() instead of send() to eliminate copy 
//      operation.
//
//  Revision: 002  By: sah     Date: 13-Jan-99    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 001  By:  <jhv>    Date:  dd-mmm-yy    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//=====================================================================

#include "BroadcastSocket.hh"
#include "NetworkApp.hh"
#include "CommFaultId.hh"
#include "SoftwareOptions.hh"
#include <string.h>   // for memset


//=====================================================================
//
//  Static Data Initialization...
//
//=====================================================================

//@ Data-Member: InitAddr_
// used to initialize destination address using built-in assignment operator
//if ported to WinCE or Windows32
#if defined(_WIN32_WCE) || defined(WIN32)
	static struct sockaddr_in  InitAddr_;
#endif

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BroadcastSocket()  [Default Constructor]
//
//@ Interface-Description
//    Initilizes all the instance variables.
//---------------------------------------------------------------------
//@ Implementation-Description
//   Let the NetworkSocket class constructor do all the 
//   instance variable initialization.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

BroadcastSocket::BroadcastSocket()
{
    CALL_TRACE("Broadcast()");
//if ported to WinCE or Windows32
#if defined(_WIN32_WCE) || defined(WIN32)
    // Initialize the static InitAddr_ used during broadcastMsg for faster 
    // initialization of the destination address. 
    memset((char*)&InitAddr_, 0, sizeof(InitAddr_));
    InitAddr_.sin_family = AF_INET;
    InitAddr_.sin_addr.s_addr = INADDR_BROADCAST;
#endif
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~BroadcastSocket()  [Destructor]
//
//@ Interface-Description
//   Default Destructor. Close the socket if it is opened.
//---------------------------------------------------------------------
//@ Implementation-Description
//	 none.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

BroadcastSocket::~BroadcastSocket(void)
{
    CALL_TRACE("~Broadcast()");
    if ( opened() )
    {
        // $[TI1]
        down(); 
    }  // $[TI2]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: openUdpBroadcast
//
//@ Interface-Description
// Creates a UDP socket used to broadcast data.
// Returns -1 for any error. Otherwise, returns the socket ID of the 
// bew socket.
//---------------------------------------------------------------------
//@ Implementation-Description
// Use the multi-threaded socket library to create a non-blocking UDP
// broadcast socket.
//---------------------------------------------------------------------
//@ PreCondition
//   Shouldn't reopen an already open state socket.
//	 (! opened() )
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Int
BroadcastSocket::openUdpBroadcast(void)
{
    CLASS_PRE_CONDITION( !opened() );
    int error;

    PRINTF(("NetworkSocket::openUdpBroadcast "));
//if ported to WinCE or Windows32
#if defined(_WIN32_WCE) || defined(WIN32)

    if ( (socketId_ = socket(AF_INET, SOCK_DGRAM, 0)) == INVALID_SOCKET )
    {
		//TODO E600 VM: another cause of circular dependency. Sockets should not be reporting
		//     communication errors.. this should be on the app level. remove all calls to
		//     ReportCommError from all sockets.
		error = WSAGetLastError();
        NetworkApp::ReportCommError("NetworkSocket::openUdpBroadcast socket()", error);
        return -1;
    }
    PRINTF(("socketId = %d\n", socketId_));

    unsigned long nonblockingon = 1;
    if ( ioctlsocket(socketId_, FIONBIO, &nonblockingon) != 0 )
    {
		error = WSAGetLastError();
        closesocket(socketId_);
        NetworkApp::ReportCommError( "NetworkSocket::openUdpBroadcast IOCTL(FIONBIO)", error);
        return -1; 
    }

    int  broadcaston = 1;
    if ( setsockopt(socketId_, SOL_SOCKET, SO_BROADCAST,
                   (char *) &broadcaston, sizeof(broadcaston) ) != 0 )
    {
		error = WSAGetLastError();
        NetworkApp::ReportCommError("setsockopt(SO_BROADCAST)", error);
        return -1; 
    }

    reInit(socketId_);

    return socketId_;
#else
	return -1;
#endif
}

#if 0
// this method is not used, but remains here for future reference
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:: recvMsg( XmitDataMsgId msgId, char *buf, Uint maxBytes)
//
//@ Interface-Description
//    The method receives a given network message to a user provided
//    data area. The message header information is also put into the
//    user data area. If the method doesn't receive a specified
//    network message, it generates an assertion. If a received
//    network message size is larger than a specified maximum
//    bytes, the method returns -1. All other type of communication
//    errors are detected by the 'readData' method and an error message
//    will be logged to the non-volatile diagnostic memory. The method
//    returns -1 when there is an communication error or the socket
//    is not open.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//	  Since a sigle UDP packet should be read in as an entire packet,
//    do not read a network message header separately, otherwise
//    out of sync data is received.
//----------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Int 
BroadcastSocket::recvMsg( XmitDataMsgId msgId, char *pData, 
                          Uint32 maxBytes ) 
{
    NetworkApp::XmitBuf recvBuf;

    SAFE_CLASS_PRE_CONDITION(NetworkApp::IsValidId(msgId) &&
                             (maxBytes <= MAX_NETWORK_BUF_SIZE));

    if (readData((char *) &recvBuf, maxBytes + sizeof(NetworkMsgHeader)) 
        == (maxBytes + sizeof( NetworkMsgHeader)))
    {
        // $[TI1]
        if ( recvBuf.header.pktSize !=  maxBytes)
        {
            // $[TI2]	
            NetworkApp::ReportCommError("BroadcastSocket::RecvMsg: got a wrong packt size",
                                        NETWORKAPP_WRONG_INPUT_COUNT);
            return(-1);
        }
        // $[TI3]
        if (recvBuf.header.msgId != msgId)
        {
            // $[TI4]
            NetworkApp::ReportCommError("BroadcastSocket::RecvMsg: got a wrong message",
                                        NETWORKAPP_INVALID_COMM_MSGID );
            return(-1);
        }
        ::bcopy((char *) recvBuf.data, pData, (size_t) maxBytes);
        // $[TI5]
        return(maxBytes);
    }
    NetworkApp::ReportCommError("BroadcastSocket::RecvMsg: COMM error",
                                error);
    // $[TI6]
    return(-1);   
}
#endif 

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:: broadcastMsg
//
//@ Interface-Description
//    This method is called to broadcast a network message with
//    a 'nbytes' of data to a given port id. Any node on the network
//    can open up a UDP socket at the given port id and receive
//    the sent data.  The method returns number of data bytes that 
//    are successfully transmitted. The method returns -1 if any
//    communication error.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//    Uses a stack variable for the network destination address
//    since this method is called from multiple threads. The static
//    variable InitAddr_ is used to quickly initialize the destination 
//    address.
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 
Int
BroadcastSocket::broadcastMsg( XmitDataMsgId msgId, void* buf,
                               Uint nbytes, PortId portId)
{
    if (SoftwareOptions::GetSoftwareOptions().getDataKeyType() !=
        SoftwareOptions::ENGINEERING)
    {
        return -1;
    }

//if ported to WinCE or Windows32
#if defined(_WIN32_WCE) || defined(WIN32)
	CLASS_ASSERTION( nbytes <= MAX_BROADCAST_MSG_SIZE );

	Int error;
	Int bytesToSend = 0;

    if ( !opened() )
    {
        return -1;
    }

	//construct the message
	XmitBuf sendBuf;
	//this sequence number is not used (dead code).. kept as-is from 840 for compatibility
    Uint32 seqNumber = getNextSequenceNum();
	sendBuf.setHeader((Uint16)msgId, (Uint16)nbytes, seqNumber);
	//copy data portion. The endianness of data was already converted hton 
	//by the caller app
    if ( nbytes && buf)
    {
		memcpy((void*)sendBuf.data, (const void *)buf, (size_t) nbytes);
    }
	//total send bytes: msg header + msg data
	bytesToSend = nbytes + sizeof(sendBuf.header);
	
	//change the header's endianness to network. Change only the header because
	//data was done by the caller.
	sendBuf.headerHtoN();

	//broadcast destination address
	sockaddr_in  dest = InitAddr_;
	dest.sin_family = AF_INET;
    dest.sin_port = htons(portId);
	dest.sin_addr.s_addr = htonl(INADDR_BROADCAST); //broadcast address

    int count;
    count = sendto(socketId_, (const char*)&sendBuf, bytesToSend, 0, (sockaddr*)&dest, sizeof(dest));   // no flags used
    if ( count == SOCKET_ERROR )
    {
		error = WSAGetLastError();
        setLostMsgFlag_(TRUE);
        NetworkApp::ReportCommError("BroadcastSocket::broadcastMsg, send()", error);
        return -1;
    }
    else if ( count != bytesToSend )
    {
        setLostMsgFlag_(TRUE);
        NetworkApp::ReportCommError("BroadcastSocket::broadcastMsg, send() bytes", count);
        return -1;
    }
	//else send was successful, return number of sent data bytes
	//(only data bytes send requested by the caller, header size does not count)
    return nbytes;
#else
	return -1;
#endif
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
BroadcastSocket::SoftFault(const SoftFaultID  softFaultID,
                           const Uint32       lineNumber,
                           const char*        pFileName,
                           const char*        pPredicate)
{
    CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
    FaultHandler::SoftFault(softFaultID, NETWORK_APPLICATION, BROADCAST_SOCKET_CLASS,
                            lineNumber, pFileName, pPredicate);
}
