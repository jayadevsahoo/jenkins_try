#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: VentSetupSubScreen - The subscreen in the Lower screen
// selected by pressing the Vent Setup button.  Displays two screens,
// one for setting the Mode, Mandatory Type, Support Type and Trigger
// Type and the other for adjusting settings appropriate to the
// Vent mode selected on the first screen.  Also used in New Patient Setup.
//---------------------------------------------------------------------
//@ Interface-Description
// Only one instance of VentSetupSubScreen should be created (in
// LowerSubScreenArea).  No methods of this class need to be called directly by
// LowerSubScreenArea.  Only methods that are part of the GUI-Application
// framework are defined. The class reacts to events such as activation,
// deactivation and button presses.
//
// The activateHappened()/deactivateHappened() methods are called automatically on
// display/removal of this subscreen by our subscreen area owner.
// buttonDownHappened() and buttonUpHappened() are called when any button in
// the subscreen is pressed.  If the Accept key is pressed then 
// acceptHappened() is called.  Changes to settings
// are indicated via valueChangeHappened().  Timer events (specifically the
// subscreen setting change timeout event) are passed Into timerEventHappened()
// and Adjust Panel events are communicated via the various adjustPanel
// methods.
//
// This subscreen is divided Into two parts, screen 1 (main controls screen)
// and screen 2 (breath settings screen).  Screen 1 is displayed when first
// entering this screen.  It displays the main breath control settings (Mode,
// Mandatory Type, Support Type and Trigger Type) along with a continue button.
// Pressing the Continue button brings the operator to screen 2 which displays
// the various settings applicable to the chosen breath control settings as
// well as a breath timing diagram.
//
// Screen 1 may also contain a "previous setup" button which allows the
// operator to restore the settings that were active before the last Vent
// Setup batch modification.  This setup must be restored within 45 minutes
// of the last batch change.
//
// The Vent Setup subscreen is also used in the New Patient Setup sequence.
// Its functionality is virtually unchanged from its use during normal
// ventilation apart from having to display different prompts, screen titles.
//---------------------------------------------------------------------
//@ Rationale
// Implements all the functionality for the Vent Setup subscreen and the last
// two steps of New Patient Setup in a single class.
//---------------------------------------------------------------------
//@ Implementation-Description
// The class creates many buttons (mostly setting buttons) and most of its work
// is in determining which ones to display and where on screen they should go.
// The most complicated setup is on screen 2 and there is a private method,
// layoutScreen2_(), which is devoted to the display of this screen.  It
// determines where to place the setting buttons applicable to the proposed
// main control settings from screen 1.
//
// All subscreens which display settings must use the subscreen setting change
// timer to timeout the use of the subscreen.  If no user activity on the GUI
// occurs within 3 minutes then the Vent Setup timeouts, it abandons all
// settings adjustments and places the operator on screen 1.
//
// Confirmation of settings changes is achieved by use of the ACCEPT key.
// The ACCEPT key is activated as soon as any settings are modified
// (unless in New Patient Setup mode where it will appear immediately).  To
// confirm settings changes and exit the screen, the ACCEPT key is pressed.
//
// There is also a "disaster recovery" feature in this subscreen.  If there
// exists a previous set of "recoverable" vent settings for the current patient
// then a button entitled "Previous Setup" is displayed on screen 1.  If the
// operator presses this then the subscreen will switch to screen 2 which will
// display the previous set of settings (these
// are the settings that were in effect immediately prior to the last batch of
// changes accepted from this subscreen, including alarm and apnea settings).
// The operator is not allowed to modify these settings so the setting buttons
// are displayed "flat".  Confirmation of these settings via the ACCEPT key 
// will return the ventilator to its previous operating setup.
//
// The operation of this subscreen is modified by whether the subscreen
// is activated during normal setup or by way of New Patient Setup.
// The method LowerScreen::RLowerScreen.isInPatientSetupMode() tells us if
// New Patient Setup mode is active and, if so, Vent Setup will operate
// slightly differently from normal.  The subscreen title will be "New
// Patient Setup/Settings" rather than "Vent Setup/Settings". Also, some
// prompts will be different and the IBW value will be displayed, flashing,
// on both screens.
//
// Setting buttons on screen 2 are arranged as they will appear in the Main
// Settings Area after acceptance, including the "status line".  One or two
// additional setting buttons may be shown at the right (High Circuit
// Pressure Limit and Flow Acceleration).  The breath timing diagram is
// displayed below the buttons.
// 
// $[BL00400]
// $[01057]
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and pre-conditions
// to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
// Only one instance of this class should be created (in LowerSubScreenArea).
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/VentSetupSubScreen.ccv   25.0.4.0   19 Nov 2013 14:08:42   pvcs  $
//
//@ Modification-Log
//
//  Revision: 042   By: mnr    Date: 23-Oct-2009    SCR Number: 6528
//  Project:  S840BUILD3
//  Description:
//      Circuit Type container color and width changed to accomodate 
//		foreing language strings adjacent to it.
//
//  Revision: 041   By: gdc    Date: 27-Apr-2009    SCR Number: 6489
//  Project:  840S
//  Description:
//      Modifications to provide for transitioning of tube 
//		I.D. to new patient value when spontaneous type is changed to 
//		PAV with an incompatible tube I.D.. This includes changes to 
//		the user interface to verify transitioned value with flashing
//		verify arrow icon. Change better supports verification icon
//		used for tube type and I.D. as well as humidification type and
//		volume.
//
//  Revision: 040   By: gdc    Date: 09-Feb-2009    SCR Number: 6439
//  Project:  840S
//  Description:
//      Removed workaround for touch "drill-down" problem.
//
//  Revision: 039   By: gdc    Date: 22-Jan-2009    SCR Number: 6458
//  Project:  840S
//  Description:
//  	Changed to display the same sub-screen title for current settings.
//
//  Revision: 038   By: gdc    Date: 11-Aug-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal non-invasive CPAP.
//      Modified to support changing IBW while ventilating.
//
//  Revision: 037  By:  gdc	  Date:  28-May-2007    SCR Number: 6237 & 6330
//  Project:  Trend
//  Description:
//       Refined text format strings. Removed SUN prototype code.
//
//  Revision: 036   By: rhj   Date:  05-May-2006    SCR Number: 6181 & 6153
//  Project:  PAV4
//  Description:
//       Enable PAV+ for English US and change Expiratory Sensitivity 
//       label from % to L/min when PAV is set.  
//
//  Revision: 035   By: gdc   Date:  27-May-2005    SCR Number: 6170
//  Project:  NIV2
//  Description:
//       Removed Insp Too Long alarm for NIV. Added High Ti spont alert. 
//      
//  Revision: 034   By: gdc   Date:  15-Feb-2005    SCR Number: 6144
//  Project:  NIV1
//  Description:
//       Added disconnect sensitivity to second screen in NIV.
//       Added NIV indicator in status bar of second setup sub-screen. 
//   
//  Revision: 033   By: rhj   Date:  24-Jan-2005    SCR Number: 6153
//  Project:  PAV
//  Description:
//       Added a check to disable the PAV+ option for the ENGLISH US Version. 
//   
//  Revision: 032   By: heatherw   Date:  04-March-2002    DR Number: 5790
//  Project:  VTPC
//  Description:
//      VTCP project-related changes:
//      *  adjusted text for Hi Vti button for appropriate combinations
//         of Mode, MandType and SupportType.
//      *  MandType = VC+ Vti Hi Vti buton should be Vti Mand except when 
//         SupportType is ATC then the button should be just Vti, because
//         it is applicable for both mand and spont breaths.
//      *  Every other condition is either Spont or not applicable.
//
//  Revision: 031   By: gdc    Date:  04-Jan-2001    DCS Number: 5493
//  Project:  GuiComms
//  Description:
//      Modified for new Touch Driver interface.
//
//  Revision: 030   By: sah   Date:  23-May-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//      PAV project-related changes:
//      *  added support for 'PA' spontaneous type
//
//  Revision: 029  By: sah     Date:  05-May-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      *  swapped placement of timing and pressure setting buttons, and
//         re-organized initialization for improved readability
//      *  re-designed interface to discrete setting value strings, whereby
//         the point-size and style are inserted at run-time
//      *  added support for 'VC+' mandatory type and 'VS' spontaneous type
//      *  removed display of 'TC_NOT_ALLOWED_*' prompts because of use of
//         new drop-down menus
//      *  removed option-specific discrete setting help messages, because
//         of use of new drop-down menus
//
//  Revision: 028   By: sah    Date:  20-Apr-2000    DCS Number: 5705
//  Project:  NeoMode
//  Description:
//      Modified initialization of the "Pi" and "Psupp" buttons to include
//      the use of the "above PEEP" phrase, as an auxillary title.
//
//  Revision: 027   By: gdc    Date:  29-Jun-1999    DCS Number: 5327
//  Project: 840 Neonatal
//  Description:
//      Optimized graphics library. Implemented Direct Draw mode for drawing
//      outside of BaseContainer refresh cycle.
//
//  Revision: 026  By: hhd    Date: 13-Aug-1999  DCS Number: 5411 
//  Project:  ATC
//  Description:
//      Implemented a mechanism to freeze the blinking of the attention icons
//      when the buttons which they are on are touched.
//
//  Revision: 025  By: hhd    Date: 06-Jul-1999  DCS Number: 5460
//  Project:  ATC
//  Description:
//     Modified to correct the spontMandTypeMsg_'s Show flag value.

//  Revision: 024  By: sah    Date: 14-Jun-1999  DCS Number: 5383
//  Project:  ATC
//  Description:
//      Improved notification of unavailability of 'TC' value, by
//      displaying message in prompt area whenever IBW < 7 kg, and
//      Spont Type button is pressed.
//
//  Revision: 023  By: hhd    Date: 07-June-1999  DCS Number: 5423 
//  Project:  ATC
//  Description:
//		Used ARROW_ICON instead for setting verification icon and eliminated
//	the icon background box.
//
//  Revision: 022  By: hhd    Date: 27-May-1999  DCS Number:  5385
//  Project:  ATC
//  Description:
//		Modified to use different button titles for Tube Id and Tube Type buttons according to 
//	whether they are in verify state.
//
//  Revision: 021  By: hhd    Date: 19-May-1999  DCS Number: 5396
//  Project:  ATC
//  Description:
//		In valueUpdateHappened_() method, removed errorneous call to 
//		isSettingChanged() and revamped code.
//
//  Revision: 020  By: hhd    Date: 10-May-1999  DCS Number: 5382
//  Project:  ATC
//  Description:
//		Added ibwTitle_ object back to the subscreen for every activation of screen1.
//
//  Revision: 019  By: hhd    Date: 07-May-1999  DCS Number: 5365
//  Project:  ATC
//  Description:
//		Minor change to positioning of buttons and text.
//
//  Revision: 018  By: hhd    Date: 05-May-1999  DCS Number: 5365
//  Project:  ATC
//  Description:
//		Added background for flashing icons.
// 
//  Revision: 017  By: sah    Date: 29-Apr-1999  DCS Number: 5365
//  Project:  ATC
//  Description:
//      Supporting new verification-needed mechanism, whereby certain
//      setting buttons that are deemed "ultra"-important will flash an
//      icon, and this subscreen will flash the same icon along with a
//      message.
//
//  Revision: 016  By:  sah	   Date:  23-Feb-1999    DCS Number:  5314
//  Project:  ATC
//  Description:
//      Added integrated with new dynamic, setting applicability functionality.
//      This allows for looser coupling between settings and GUI, and the
//      need for significantly less "setting knowledge" in the GUI subsystem.
//
//  Revision: 015  By: yyy  Date: 08-Feb-1999  DR Number: 5307
//  Project:  ATC (R8027)
//  Description:
//      Added "isPreviousSetup_" checking in acceptHappened() method.
//
//  Revision: 014  By: sah  Date: 08-Feb-1999  DR Number: 5314
//  Project:  ATC (R8027)
//  Description:
//      Changed 'PEEP_HI' to 'PEEP_HIGH'.
//
//  Revision: 013  By:  hhd Date:  27-Jan-1999    DR Number: 5322
//       Project: ATC 
//       Description:
//			Added new setting buttons as required for ATC.
//
//  Revision: 012  By:  syw   Date:  16-Nov-1998    DR Number: 5252
//       Project:  BiLevel
//       Description:
//			Added requirement tracing.
//
//  Revision: 011  By:  yyy    Date:  01-APR-98    DR Number: 5252
//       Project:  Sigma (R8027)
//       Description:
//             Mapping SRS to code.
//
//  Revision: 010  By:  gdc    Date:  02-Sep-1998    DCS Number: 5153
//  Project:  Color
//  Description:
//      Added traceable ID's for Color Project requirements.
//
//  Revision: 009  By: yyy      Date: 05-Jan-1998  DR Number: 
//    Project:  BiLevel (R8027)
//    Description:
//     Initial release for BiLevel adding software option.
//     Merged /840/Baseline/GUI-Applications/vcssrc/VentSetupSubScreen.ccv   1.36.1.0   07/30/98 10:22:36   gdc
//
//  Revision: 008  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 007  By:  hhd	   Date:  06-Oct-97    DR Number: 2540 
//    Project:  Sigma (R8027)
//    Description:
//		Removed code that supports commas in floating poInt format.
//
//  Revision: 006  By:  yyy    Date:  23-Sep-97    DR Number: 2336
//    Project:  Sigma (R8027)
//    Description:
//      Set the isPreviousSetup_, onScreen1_, and areCurrentSettings_ at the
//		beginning of the activate() before reset the adjustPanelTarget to
//		display the correct prompt messages.
//
//  Revision: 005  By:  yyy    Date:  10-Sep-97    DR Number: 1796
//    Project:  Sigma (R8027)
//    Description:
//      Removed all references to PREVIOUS SETUP RECOVERY.
//
//  Revision: 004  By:  yyy    Date:  10-Sep-97    DR Number: 2169
//    Project:  Sigma (R8027)
//    Description:
//      Removed obsolete SRS references.
//
//  Revision: 003  By:  hhd    Date:  22-AUG-97    DR Number: 2321
//       Project:  Sigma (R8027)
//       Description:
//				Cleaned up code modification made for DCS 2321.
//
//  Revision: 002  By:  hhd    Date:  18-AUG-97    DR Number: 2321
//       Project:  Sigma (R8027)
//       Description:
//          Converted decimal poInt to comma for all European but German language.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "VentSetupSubScreen.hh"

//@ Usage-Classes
#include "AdjustPanel.hh"
#include "ApneaIntervalSetting.hh"
#include "Colors.hh"
#include "ConstantParmValue.hh"
#include "ContextSubject.hh"
#include "FlowPatternValue.hh"
#include "GuiTimerId.hh"
#include "GuiTimerRegistrar.hh"
#include "Image.hh"
#include "LeakCompEnabledValue.hh"
#include "LowerScreen.hh"
#include "MandTypeValue.hh"
#include "MiscStrs.hh"
#include "ModeValue.hh"
#include "PatientCctTypeValue.hh"
#include "PatientCctTypeValue.hh"
#include "PromptArea.hh"
#include "SettingConstants.hh"
#include "SettingContextHandle.hh"
#include "SIterR_Drawable.hh"
#include "SoftwareOptions.hh"
#include "Sound.hh"
#include "SupportTypeValue.hh"
#include "TextUtil.hh"
#include "TriggerTypeValue.hh"
#include "TubeTypeValue.hh"
#include "VentTypeValue.hh"
//@ End-Usage

//@ Code...

// Initialize static constants.
//static const Int32 IBW_TITLE_Y_ = 20;
static const Int32 SCRN1_BUTTON_WIDTH_ = 140;
static const Int32 SCRN1_BUTTON_HEIGHT_ = 46;
static const Int32 SETTING_BUTTON_BORDER_ = 3;
static const Int32 SCRN1_BUTTON_X_OFFSET_ = 8;
static const Int32 SCRN1_BUTTON_X_GAP_ = 10;
static const Int32 SCRN1_BUTTON_Y_GAP_ = 18;  // Japanese needs minimum 16
static const Int32 SCRN1_BUTTON_X_ =
( ::LOWER_SUB_SCREEN_AREA_WIDTH - SCRN1_BUTTON_WIDTH_ * 4 
  - SCRN1_BUTTON_X_GAP_ * 3 ) / 2;
static const Int32 SCRN1_BUTTON_Y_ = 35;
static const Int32 CONTINUE_BUTTON_X_ = 542;
static const Int32 CONTINUE_BUTTON_Y_ = 200;
static const Int32 CONTINUE_BUTTON_WIDTH_ = 84;
static const Int32 CONTINUE_BUTTON_HEIGHT_ = 34;
static const Int32 CONTINUE_BUTTON_BORDER_ = 3;
static const Int32 PREVIOUS_SETUP_BUTTON_Y_ = 205;
static const Int32 PREVIOUS_SETUP_BUTTON_WIDTH_ = 82;
static const Int32 PREVIOUS_SETUP_BUTTON_HEIGHT_ = 35;
static const Int32 PREVIOUS_SETUP_BUTTON_BORDER_ = 3;
static const Int32 SCRN2_BUTTON_WIDTH_ = 106;
static const Int32 SCRN2_BUTTON_HEIGHT_ = 46;
static const Int32 SCRN2_BUTTON_BORDER_ = 3;
static const Int32 SCRN2_BUTTON_WIDTH_SHAVE_ = 2;
static const Int32 STATUS_BAR_X_ = 1;
static const Int32 STATUS_BAR_Y_ = 1;
static const Int32 STATUS_BAR_WIDTH_ = ::LOWER_SUB_SCREEN_AREA_WIDTH - 2;
static const Int32 STATUS_BAR_HEIGHT_ = 19;
static const Int32 MAIN_SETTINGS_AREA_X_ = 0;
static const Int32 MAIN_SETTINGS_AREA_Y_ = STATUS_BAR_Y_ + STATUS_BAR_HEIGHT_;
static const Int32 MAIN_SETTINGS_AREA_WIDTH_ = ::LOWER_SUB_SCREEN_AREA_WIDTH;
static const Int32 MAIN_SETTINGS_AREA_HEIGHT_ = 2 * SCRN2_BUTTON_HEIGHT_ + 2;
static const Int32 ROW0_BUTTON_Y_ = 2;
static const Int32 ROW1_BUTTON_Y_ = ROW0_BUTTON_Y_ + SCRN2_BUTTON_HEIGHT_;
static const Int32 ROW2_BUTTON_Y_ = MAIN_SETTINGS_AREA_Y_
									+ MAIN_SETTINGS_AREA_HEIGHT_ + 8;
static const Int32 ROW3_BUTTON_Y_ = ROW2_BUTTON_Y_ + SCRN2_BUTTON_HEIGHT_;
//static const Int32 ROW4_BUTTON_Y_ = ROW3_BUTTON_Y_ + SCRN2_BUTTON_HEIGHT_;
static const Int32 NUMERIC_VALUE_CENTER_X_ = 35;
static const Int32 NUMERIC_VALUE_CENTER_Y_ = 26;
static const Int32 IE_RATIO_VALUE_CENTER_X_ = 46;
static const Int32 IE_RATIO_VALUE_CENTER_Y_ = NUMERIC_VALUE_CENTER_Y_;
static const Int32 LEFT_WING_X1_ = 113;
static const Int32 LEFT_WING_X2_ = 188;
static const Int32 RIGHT_WING_X1_ = 235;
static const Int32 RIGHT_WING_X2_ = 310;
static const Int32 WING_Y_ = 10;
static const Int32 WING_PEN_ = 2;
static const Int32 WINGTIP_WIDTH_ = 6;
static const Int32 WINGTIP_HEIGHT_ = 6;
static const Int32 TIMING_DIAGRAM_X_ = 10;
static const Int32 TIMING_DIAGRAM_Y_ = 124;
static const Real32 MIN_TC_BODY_WEIGHT_ = 7.0;

static const Int32 CCT_TYPE_BOX_X_ = 0;
static const Int32 CCT_TYPE_BOX_Y_ = 0;
static const Int32 CCT_TYPE_BOX_WIDTH_ = SCRN1_BUTTON_WIDTH_ - 30; //previous: SCRN1_BUTTON_WIDTH_
static const Int32 CCT_TYPE_BOX_HEIGHT_ = 31; // sub-screen title area height
static const Int32 CCT_TYPE_BOX_LINE_WIDTH_ = 1;

StringId LITERS_PER_MINUTE_SMALL =
L"{p=8,T,x=76,y=23:L{x=68,Y=1:__}{x=70,y=35:min}}";  
StringId ONE_PER_MINUTE_SMALL =
L"{p=8,T,x=76,y=23:1{x=68,Y=1:__}{x=70,y=35:min}}";  

static Uint  VentTypeInfoMemory_[(sizeof(DiscreteSettingButton::ValueInfo) +
								  (sizeof(StringId) * (VentTypeValue::TOTAL_VENT_TYPE_VALUES - 1)))];
static Uint  ModeInfoMemory_[(sizeof(DiscreteSettingButton::ValueInfo) +
							  (sizeof(StringId) * (ModeValue::TOTAL_MODE_VALUES - 1)))];
static Uint  MandTypeInfoMemory_[(sizeof(DiscreteSettingButton::ValueInfo) +
								  (sizeof(StringId) * (MandTypeValue::TOTAL_MAND_TYPES - 1)))];
static Uint  SpontTypeInfoMemory_[(sizeof(DiscreteSettingButton::ValueInfo) +
								   (sizeof(StringId) * (SupportTypeValue::TOTAL_SUPPORT_TYPES - 1)))];
static Uint  TriggerTypeInfoMemory_[(sizeof(DiscreteSettingButton::ValueInfo) +
									 (sizeof(StringId) * (TriggerTypeValue::TOTAL_TRIGGER_TYPES - 1)))];
static Uint  FlowPatternInfoMemory_[(sizeof(DiscreteSettingButton::ValueInfo) +
									 (sizeof(StringId) * (FlowPatternValue::TOTAL_FLOW_PATTERNS - 1)))];
static Uint  TubeTypeInfoMemory_[(sizeof(DiscreteSettingButton::ValueInfo) +
								  (sizeof(StringId) * (TubeTypeValue::TOTAL_TUBE_TYPE_VALUES - 1)))];

static DiscreteSettingButton::ValueInfo*  PVentTypeInfo_ =
(DiscreteSettingButton::ValueInfo*)::VentTypeInfoMemory_;
static DiscreteSettingButton::ValueInfo*  PModeInfo_ =
(DiscreteSettingButton::ValueInfo*)::ModeInfoMemory_;
static DiscreteSettingButton::ValueInfo*  PMandTypeInfo_ =
(DiscreteSettingButton::ValueInfo*)::MandTypeInfoMemory_;
static DiscreteSettingButton::ValueInfo*  PSpontTypeInfo_ =
(DiscreteSettingButton::ValueInfo*)::SpontTypeInfoMemory_;
static DiscreteSettingButton::ValueInfo*  PTriggerTypeInfo_ =
(DiscreteSettingButton::ValueInfo*)::TriggerTypeInfoMemory_;
static DiscreteSettingButton::ValueInfo*  PFlowPatternInfo_ =
(DiscreteSettingButton::ValueInfo*)::FlowPatternInfoMemory_;
static DiscreteSettingButton::ValueInfo*  PTubeTypeInfo_ =
(DiscreteSettingButton::ValueInfo*)::TubeTypeInfoMemory_;

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: VentSetupSubScreen()  [Constructor]
//
//@ Interface-Description
// Constructs the Vent Setup subscreen.  Only one instance of this
// class should be created (by LowerSubScreenArea).  It is passed a
// pointer to LowerSubScreenArea.
//---------------------------------------------------------------------
//@ Implementation-Description
// Many buttons are initialized here.  The subscreen is sized and
// colored.  We register as a target for all settings in this subscreen.
// We register for callbacks from numerous buttons.  We setup the valid
// discrete values for all DiscreteSettingButton's and size, position and
// color any graphics that need it.
//
// $[01034] All of these buttons must be of the select type except
//			Constant During Rate Change.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

VentSetupSubScreen::VentSetupSubScreen(SubScreenArea *pSubScreenArea) :
BatchSettingsSubScreen( pSubScreenArea),
onScreen1_(             TRUE),
isPreviousSetup_(       FALSE),
previousSetupButton_(   SCRN1_BUTTON_X_OFFSET_,
						PREVIOUS_SETUP_BUTTON_Y_, PREVIOUS_SETUP_BUTTON_WIDTH_,
						PREVIOUS_SETUP_BUTTON_HEIGHT_,
						Button::LIGHT, PREVIOUS_SETUP_BUTTON_BORDER_, 
						Button::SQUARE,
						Button::NO_BORDER, MiscStrs::PREVIOUS_SETUP_BUTTON_TITLE),
continueButton_(        CONTINUE_BUTTON_X_, CONTINUE_BUTTON_Y_,
						CONTINUE_BUTTON_WIDTH_, CONTINUE_BUTTON_HEIGHT_,
						Button::LIGHT, CONTINUE_BUTTON_BORDER_, Button::SQUARE,
						Button::NO_BORDER, MiscStrs::CONTINUE_BUTTON_TITLE),

//=================================================================
// Main Control Settings (1st screen)...
//=================================================================
newIbwButton_(          SCRN1_BUTTON_X_, 
						SCRN1_BUTTON_Y_),
ventTypeButton_(        SCRN1_BUTTON_X_ + 1 * (SCRN1_BUTTON_WIDTH_ + SCRN1_BUTTON_X_GAP_), 
						SCRN1_BUTTON_Y_ + 0 * (SCRN1_BUTTON_HEIGHT_ + SCRN1_BUTTON_Y_GAP_),
						SCRN1_BUTTON_WIDTH_, 
						SCRN1_BUTTON_HEIGHT_, 
						Button::LIGHT,
						SETTING_BUTTON_BORDER_, 
						Button::ROUND,
						Button::NO_BORDER, 
						MiscStrs::VENT_TYPE_BUTTON_TITLE_SMALL,
						SettingId::VENT_TYPE, 
						this,
						MiscStrs::VENT_TYPE_HELP_MESSAGE, 
						18),
modeButton_(            SCRN1_BUTTON_X_ + 0 * (SCRN1_BUTTON_WIDTH_ + SCRN1_BUTTON_X_GAP_), 
						SCRN1_BUTTON_Y_ + 1 * (SCRN1_BUTTON_HEIGHT_ + SCRN1_BUTTON_Y_GAP_),
						SCRN1_BUTTON_WIDTH_, 
						SCRN1_BUTTON_HEIGHT_, 
						Button::LIGHT,
						SETTING_BUTTON_BORDER_, 
						Button::ROUND,
						Button::NO_BORDER, 
						MiscStrs::MODE_BUTTON_TITLE_SMALL,
						SettingId::MODE, 
						this,
						MiscStrs::MODE_HELP_MESSAGE, 
						18),
mandatoryTypeButton_(   SCRN1_BUTTON_X_ + 1 * (SCRN1_BUTTON_WIDTH_ + SCRN1_BUTTON_X_GAP_), 
						SCRN1_BUTTON_Y_ + 1 * (SCRN1_BUTTON_HEIGHT_ + SCRN1_BUTTON_Y_GAP_),
						SCRN1_BUTTON_WIDTH_, 
						SCRN1_BUTTON_HEIGHT_, 
						Button::LIGHT, SETTING_BUTTON_BORDER_, 
						Button::ROUND,
						Button::NO_BORDER, 
						MiscStrs::MAND_TYPE_BUTTON_TITLE_SMALL,
						SettingId::MAND_TYPE, 
						this,
						MiscStrs::MAND_TYPE_HELP_MESSAGE, 
						18),
supportTypeButton_(     SCRN1_BUTTON_X_ + 2 * (SCRN1_BUTTON_WIDTH_ + SCRN1_BUTTON_X_GAP_), 
						SCRN1_BUTTON_Y_ + 1 * (SCRN1_BUTTON_HEIGHT_ + SCRN1_BUTTON_Y_GAP_),
						SCRN1_BUTTON_WIDTH_, 
						SCRN1_BUTTON_HEIGHT_, 
						Button::LIGHT, 
						SETTING_BUTTON_BORDER_, 
						Button::ROUND,
						Button::NO_BORDER, 
						MiscStrs::SPONT_TYPE_BUTTON_TITLE_SMALL,
						SettingId::SUPPORT_TYPE, 
						this,
						MiscStrs::SPONT_TYPE_HELP_MESSAGE, 
						18),
triggerTypeButton_(     SCRN1_BUTTON_X_ + 3 * (SCRN1_BUTTON_WIDTH_ + SCRN1_BUTTON_X_GAP_), 
						SCRN1_BUTTON_Y_ + 1 * (SCRN1_BUTTON_HEIGHT_ + SCRN1_BUTTON_Y_GAP_),
						SCRN1_BUTTON_WIDTH_, 
						SCRN1_BUTTON_HEIGHT_, 
						Button::LIGHT, 
						SETTING_BUTTON_BORDER_, 
						Button::ROUND,
						Button::NO_BORDER, 
						MiscStrs::TRIGGER_TYPE_BUTTON_TITLE_SMALL,
						SettingId::TRIGGER_TYPE, 
						this,
						MiscStrs::TRIGGER_TYPE_HELP_MESSAGE, 
						18),
cctTypeContainer_(      ),
cctTypeBox_(            CCT_TYPE_BOX_X_, 
						CCT_TYPE_BOX_Y_, 
						CCT_TYPE_BOX_WIDTH_, 
						CCT_TYPE_BOX_HEIGHT_, 
						CCT_TYPE_BOX_LINE_WIDTH_),
cctTypeLabel_(          MiscStrs::CCT_TYPE_LABEL),
cctTypeText_(           ),

//=================================================================
// Breath Settings (2nd Screen)...
//=================================================================

//-----------------------------------------------------------------
// Column #0, Row #0...
//-----------------------------------------------------------------
respiratoryRateButton_( 0, ROW0_BUTTON_Y_,
						SCRN2_BUTTON_WIDTH_, SCRN2_BUTTON_HEIGHT_,
						Button::LIGHT, SCRN2_BUTTON_BORDER_,
						Button::ROUND, Button::NO_BORDER, TextFont::EIGHTEEN,
						NUMERIC_VALUE_CENTER_X_, NUMERIC_VALUE_CENTER_Y_,
						MiscStrs::RESPIRATORY_RATE_BUTTON_TITLE_SMALL,
						MiscStrs::ONE_PER_MIN_UNITS_SMALL,
						SettingId::RESP_RATE, this,
						MiscStrs::RESPIRATORY_RATE_HELP_MESSAGE),

//-----------------------------------------------------------------
// Column #0, Row #1...
//-----------------------------------------------------------------
flowAccelerationButton_(0,  ROW1_BUTTON_Y_,
						SCRN2_BUTTON_WIDTH_, SCRN2_BUTTON_HEIGHT_,
						Button::LIGHT, SCRN2_BUTTON_BORDER_,
						Button::ROUND, Button::NO_BORDER, TextFont::EIGHTEEN,
						NUMERIC_VALUE_CENTER_X_, NUMERIC_VALUE_CENTER_Y_,
						MiscStrs::FLOW_ACCELERATION_BUTTON_TITLE_SMALL,
						MiscStrs::PERCENT_UNITS_SMALL,
						SettingId::FLOW_ACCEL_PERCENT, this,
						MiscStrs::FLOW_ACCELERATION_HELP_MESSAGE),

//-----------------------------------------------------------------
// Column #1, Row #0...
//-----------------------------------------------------------------
inspiratoryPressureButton_(SCRN2_BUTTON_WIDTH_, ROW0_BUTTON_Y_,
						   SCRN2_BUTTON_WIDTH_, SCRN2_BUTTON_HEIGHT_,
						   Button::LIGHT, SCRN2_BUTTON_BORDER_,
						   Button::ROUND, Button::NO_BORDER, TextFont::EIGHTEEN,
						   NUMERIC_VALUE_CENTER_X_, NUMERIC_VALUE_CENTER_Y_,
						   MiscStrs::INSPIRATORY_PRESSURE_BUTTON_TITLE_SMALL,
						   GuiApp::GetPressureUnitsSmall(),
						   SettingId::INSP_PRESS, this,
						   MiscStrs::INSPIRATORY_PRESSURE_HELP_MESSAGE, FALSE,
						   MiscStrs::ABOVE_PEEP_AUX_TEXT_SMALL, ::GRAVITY_RIGHT),
peepHighButton_(        SCRN2_BUTTON_WIDTH_, ROW0_BUTTON_Y_,
						SCRN2_BUTTON_WIDTH_, SCRN2_BUTTON_HEIGHT_, Button::LIGHT,
						SCRN2_BUTTON_BORDER_, Button::ROUND,
						Button::NO_BORDER, TextFont::EIGHTEEN,
						NUMERIC_VALUE_CENTER_X_, NUMERIC_VALUE_CENTER_Y_,
						MiscStrs::PEEP_HI_BUTTON_TITLE_SMALL,
						GuiApp::GetPressureUnitsSmall(),
						SettingId::PEEP_HIGH, this,
						MiscStrs::PEEP_HI_HELP_MESSAGE),
tidalVolumeButton_(     SCRN2_BUTTON_WIDTH_, ROW0_BUTTON_Y_,
						SCRN2_BUTTON_WIDTH_, SCRN2_BUTTON_HEIGHT_, Button::LIGHT,
						SCRN2_BUTTON_BORDER_,
						Button::ROUND, Button::NO_BORDER, TextFont::EIGHTEEN,
						NUMERIC_VALUE_CENTER_X_, NUMERIC_VALUE_CENTER_Y_,
						MiscStrs::TIDAL_VOLUME_BUTTON_TITLE_SMALL,
						MiscStrs::ML_UNITS_SMALL,
						SettingId::TIDAL_VOLUME, this,
						MiscStrs::TIDAL_VOLUME_VCV_HELP_MSG),

//-----------------------------------------------------------------
// Column #1, Row #1...
//-----------------------------------------------------------------
peepLowButton_(         SCRN2_BUTTON_WIDTH_, ROW1_BUTTON_Y_,
						SCRN2_BUTTON_WIDTH_, SCRN2_BUTTON_HEIGHT_, Button::LIGHT,
						SCRN2_BUTTON_BORDER_, Button::ROUND,
						Button::NO_BORDER, TextFont::EIGHTEEN,
						NUMERIC_VALUE_CENTER_X_, NUMERIC_VALUE_CENTER_Y_,
						MiscStrs::PEEP_LOW_BUTTON_TITLE_SMALL,
						GuiApp::GetPressureUnitsSmall(),
						SettingId::PEEP_LOW, this,
						MiscStrs::PEEP_LOW_HELP_MESSAGE),
plateauTimeButton_(     SCRN2_BUTTON_WIDTH_, ROW1_BUTTON_Y_,
						SCRN2_BUTTON_WIDTH_, SCRN2_BUTTON_HEIGHT_, Button::LIGHT,
						SCRN2_BUTTON_BORDER_,
						Button::ROUND, Button::NO_BORDER, TextFont::EIGHTEEN,
						NUMERIC_VALUE_CENTER_X_, NUMERIC_VALUE_CENTER_Y_,
						MiscStrs::PLATEAU_TIME_BUTTON_TITLE_SMALL,
						MiscStrs::SEC_UNITS_SMALL,
						SettingId::PLATEAU_TIME, this,
						MiscStrs::PLATEAU_TIME_HELP_MESSAGE),

//-----------------------------------------------------------------
// Column #2, Row #0...
//-----------------------------------------------------------------
expiratoryTimeButton_(  2 * SCRN2_BUTTON_WIDTH_, ROW0_BUTTON_Y_,
						SCRN2_BUTTON_WIDTH_, SCRN2_BUTTON_HEIGHT_,
						Button::LIGHT, SCRN2_BUTTON_BORDER_,
						Button::ROUND, Button::NO_BORDER, TextFont::EIGHTEEN,
						NUMERIC_VALUE_CENTER_X_, NUMERIC_VALUE_CENTER_Y_,
						MiscStrs::EXPIRATORY_TIME_BUTTON_TITLE_SMALL,
						MiscStrs::SEC_UNITS_SMALL,
						SettingId::EXP_TIME, this,
						MiscStrs::EXPIRATORY_TIME_HELP_MESSAGE),
peepLowTimeButton_(     2 * SCRN2_BUTTON_WIDTH_, ROW0_BUTTON_Y_,
						SCRN2_BUTTON_WIDTH_, SCRN2_BUTTON_HEIGHT_,
						Button::LIGHT, SCRN2_BUTTON_BORDER_,
						Button::ROUND, Button::NO_BORDER, TextFont::EIGHTEEN,
						NUMERIC_VALUE_CENTER_X_, NUMERIC_VALUE_CENTER_Y_,
						MiscStrs::PEEP_LOW_TIME_BUTTON_TITLE_SMALL,
						MiscStrs::SEC_UNITS_SMALL,
						SettingId::PEEP_LOW_TIME, this,
						MiscStrs::PEEP_LOW_TIME_HELP_MESSAGE),
ieRatioButton_(         2 * SCRN2_BUTTON_WIDTH_, ROW0_BUTTON_Y_,
						SCRN2_BUTTON_WIDTH_, SCRN2_BUTTON_HEIGHT_, Button::LIGHT,
						SCRN2_BUTTON_BORDER_, Button::ROUND, Button::NO_BORDER,
						TextFont::EIGHTEEN,
						IE_RATIO_VALUE_CENTER_X_, IE_RATIO_VALUE_CENTER_Y_,
						MiscStrs::IE_RATIO_BUTTON_TITLE_SMALL,
						SettingId::IE_RATIO,
						this, MiscStrs::IE_RATIO_HELP_MESSAGE),
hlRatioButton_(         2 * SCRN2_BUTTON_WIDTH_, ROW0_BUTTON_Y_,
						SCRN2_BUTTON_WIDTH_, SCRN2_BUTTON_HEIGHT_, Button::LIGHT,
						SCRN2_BUTTON_BORDER_, Button::ROUND, Button::NO_BORDER,
						TextFont::EIGHTEEN,
						IE_RATIO_VALUE_CENTER_X_, IE_RATIO_VALUE_CENTER_Y_,
						MiscStrs::PEEP_HIGH_PEEP_LOW_RATIO_BUTTON_TITLE_SMALL, 
						SettingId::HL_RATIO, this,
						MiscStrs::PEEP_HIGH_PEEP_LOW_RATIO_HELP_MESSAGE),
inspiratoryTimeButton_( 2 * SCRN2_BUTTON_WIDTH_, ROW0_BUTTON_Y_,
						SCRN2_BUTTON_WIDTH_, SCRN2_BUTTON_HEIGHT_,
						Button::LIGHT, SCRN2_BUTTON_BORDER_, Button::ROUND,
						Button::NO_BORDER, TextFont::EIGHTEEN,
						NUMERIC_VALUE_CENTER_X_, NUMERIC_VALUE_CENTER_Y_,
						MiscStrs::INSPIRATORY_TIME_BUTTON_TITLE_SMALL,
						MiscStrs::SEC_UNITS_SMALL,
						SettingId::INSP_TIME, this,
						MiscStrs::INSPIRATORY_TIME_HELP_MESSAGE),
peepHighTimeButton_(    2 * SCRN2_BUTTON_WIDTH_, ROW0_BUTTON_Y_,
						SCRN2_BUTTON_WIDTH_, SCRN2_BUTTON_HEIGHT_,
						Button::LIGHT, SCRN2_BUTTON_BORDER_, Button::ROUND,
						Button::NO_BORDER, TextFont::EIGHTEEN,
						NUMERIC_VALUE_CENTER_X_, NUMERIC_VALUE_CENTER_Y_,
						MiscStrs::PEEP_HIGH_TIME_BUTTON_TITLE_SMALL,
						MiscStrs::SEC_UNITS_SMALL,
						SettingId::PEEP_HIGH_TIME, this,
						MiscStrs::PEEP_HIGH_TIME_HELP_MESSAGE),
peakFlowButton_(        2 * SCRN2_BUTTON_WIDTH_, ROW0_BUTTON_Y_,
						SCRN2_BUTTON_WIDTH_, SCRN2_BUTTON_HEIGHT_, Button::LIGHT,
						SCRN2_BUTTON_BORDER_, Button::ROUND, Button::NO_BORDER,
						TextFont::EIGHTEEN,
						NUMERIC_VALUE_CENTER_X_, NUMERIC_VALUE_CENTER_Y_,
						MiscStrs::PEAK_FLOW_BUTTON_TITLE_SMALL,
						MiscStrs::L_PER_MIN_UNITS_SMALL,
						SettingId::PEAK_INSP_FLOW, this,
						MiscStrs::PEAK_FLOW_HELP_MESSAGE),

//-----------------------------------------------------------------
// Column #2, Row #1...
//-----------------------------------------------------------------
flowPatternButton_(     2 * SCRN2_BUTTON_WIDTH_, ROW1_BUTTON_Y_,
						SCRN2_BUTTON_WIDTH_, SCRN2_BUTTON_HEIGHT_,
						Button::LIGHT, SCRN2_BUTTON_BORDER_,
						Button::ROUND, Button::NO_BORDER, NULL_STRING_ID,
						SettingId::FLOW_PATTERN, this,
						MiscStrs::FLOW_PATTERN_HELP_MESSAGE, 12),

//-----------------------------------------------------------------
// Column #3, Row #0...
//-----------------------------------------------------------------
pressureSupportButton_( 3 * SCRN2_BUTTON_WIDTH_, ROW0_BUTTON_Y_,
						SCRN2_BUTTON_WIDTH_, SCRN2_BUTTON_HEIGHT_,
						Button::LIGHT, SCRN2_BUTTON_BORDER_,
						Button::ROUND, Button::NO_BORDER, TextFont::EIGHTEEN,
						NUMERIC_VALUE_CENTER_X_, NUMERIC_VALUE_CENTER_Y_,
						MiscStrs::PRESSURE_SUPPORT_BUTTON_TITLE_SMALL,
						GuiApp::GetPressureUnitsSmall(),
						SettingId::PRESS_SUPP_LEVEL, this,
						MiscStrs::PRESSURE_SUPPORT_HELP_MESSAGE, FALSE,
						MiscStrs::ABOVE_PEEP_AUX_TEXT_SMALL, ::GRAVITY_RIGHT),
percentSupportButton_(  3 * SCRN2_BUTTON_WIDTH_, ROW0_BUTTON_Y_,
						SCRN2_BUTTON_WIDTH_, SCRN2_BUTTON_HEIGHT_,
						Button::LIGHT, SCRN2_BUTTON_BORDER_,
						Button::ROUND, Button::NO_BORDER, TextFont::EIGHTEEN,
						NUMERIC_VALUE_CENTER_X_, NUMERIC_VALUE_CENTER_Y_,
						MiscStrs::PERCENT_SUPPORT_BUTTON_TITLE_SMALL,
						MiscStrs::PERCENT_UNITS_SMALL,
						SettingId::PERCENT_SUPPORT, this,
						MiscStrs::PERCENT_SUPPORT_HELP_MESSAGE),
volumeSupportButton_(   3 * SCRN2_BUTTON_WIDTH_, ROW0_BUTTON_Y_,
						SCRN2_BUTTON_WIDTH_, SCRN2_BUTTON_HEIGHT_,
						Button::LIGHT, SCRN2_BUTTON_BORDER_,
						Button::ROUND, Button::NO_BORDER, TextFont::EIGHTEEN,
						NUMERIC_VALUE_CENTER_X_, NUMERIC_VALUE_CENTER_Y_,
						MiscStrs::VOLUME_SUPPORT_BUTTON_TITLE_SMALL,
						MiscStrs::ML_UNITS_SMALL,
						SettingId::VOLUME_SUPPORT, this,
						MiscStrs::VOLUME_SUPPORT_HELP_MESSAGE),

//-----------------------------------------------------------------
// Column #3, Row #1... [NIV Active]
//-----------------------------------------------------------------
highSpontInspTimeButton_(3 * SCRN2_BUTTON_WIDTH_, ROW1_BUTTON_Y_,
						 SCRN2_BUTTON_WIDTH_, SCRN2_BUTTON_HEIGHT_,
						 Button::LIGHT, SCRN2_BUTTON_BORDER_, Button::ROUND,
						 Button::NO_BORDER, TextFont::EIGHTEEN,
						 NUMERIC_VALUE_CENTER_X_, NUMERIC_VALUE_CENTER_Y_,
						 MiscStrs::HIGH_SPONT_INSP_TIME_BUTTON_TITLE_SMALL,
						 MiscStrs::SEC_UNITS_SMALL,
						 SettingId::HIGH_SPONT_INSP_TIME, this,
						 MiscStrs::HIGH_SPONT_INSP_TIME_HELP_MESSAGE),

//-----------------------------------------------------------------
// Column #4, Row #0...
//-----------------------------------------------------------------
flowSensitivityButton_(     4 * SCRN2_BUTTON_WIDTH_, ROW0_BUTTON_Y_,
							SCRN2_BUTTON_WIDTH_, SCRN2_BUTTON_HEIGHT_,
							Button::LIGHT, SCRN2_BUTTON_BORDER_,
							Button::ROUND, Button::NO_BORDER, TextFont::EIGHTEEN,
							NUMERIC_VALUE_CENTER_X_, NUMERIC_VALUE_CENTER_Y_,
							MiscStrs::FLOW_SENSITIVITY_BUTTON_TITLE_SMALL,
							MiscStrs::L_PER_MIN_UNITS_SMALL,
							SettingId::FLOW_SENS, this,
							MiscStrs::FLOW_SENSITIVITY_HELP_MESSAGE),
pressureSensitivityButton_( 4 * SCRN2_BUTTON_WIDTH_, ROW0_BUTTON_Y_,
							SCRN2_BUTTON_WIDTH_, SCRN2_BUTTON_HEIGHT_,
							Button::LIGHT, SCRN2_BUTTON_BORDER_,
							Button::ROUND, Button::NO_BORDER, TextFont::EIGHTEEN,
							NUMERIC_VALUE_CENTER_X_, NUMERIC_VALUE_CENTER_Y_,
							MiscStrs::PRESSURE_SENSITIVITY_BUTTON_TITLE_SMALL,
							GuiApp::GetPressureUnitsSmall(),
							SettingId::PRESS_SENS, this,
							MiscStrs::PRESSURE_SENSITIVITY_HELP_MESSAGE),

//-----------------------------------------------------------------
// Column #4, Row #1...
//-----------------------------------------------------------------
expiratorySensitivityButton_(4 * SCRN2_BUTTON_WIDTH_, ROW1_BUTTON_Y_,
							 SCRN2_BUTTON_WIDTH_, SCRN2_BUTTON_HEIGHT_,
							 Button::LIGHT, SCRN2_BUTTON_BORDER_,
							 Button::ROUND, Button::NO_BORDER, TextFont::EIGHTEEN,
							 NUMERIC_VALUE_CENTER_X_, NUMERIC_VALUE_CENTER_Y_,
							 MiscStrs::EXPIRATORY_SENS_BUTTON_TITLE_SMALL,
							 MiscStrs::PERCENT_UNITS_SMALL,
							 SettingId::EXP_SENS, this,
							 MiscStrs::EXPIRATORY_SENSITIVITY_HELP_MESSAGE),

//-----------------------------------------------------------------
// Column #4, Row #2...  [NIV active]
//-----------------------------------------------------------------
discSensButton_(            4 * SCRN2_BUTTON_WIDTH_, ROW2_BUTTON_Y_,
							SCRN2_BUTTON_WIDTH_, SCRN2_BUTTON_HEIGHT_,
							Button::LIGHT, SCRN2_BUTTON_BORDER_,
							Button::ROUND, Button::NO_BORDER, TextFont::EIGHTEEN,
							NUMERIC_VALUE_CENTER_X_, NUMERIC_VALUE_CENTER_Y_,
							MiscStrs::DISCONNECTION_SENS_BUTTON_TITLE_SMALL,
							MiscStrs::PERCENT_UNITS_SMALL_MORE_SETTINGS_SUB_SCREEN,
							SettingId::DISCO_SENS, this,
							MiscStrs::DISCONNECTION_SENSITIVITY_HELP_MESSAGE),

//-----------------------------------------------------------------
// Column #4, Row #2...  [PA/TC active]
//-----------------------------------------------------------------
tubeIdButton_(          4 * SCRN2_BUTTON_WIDTH_, ROW2_BUTTON_Y_,
						SCRN2_BUTTON_WIDTH_, SCRN2_BUTTON_HEIGHT_,
						Button::LIGHT, SETTING_BUTTON_BORDER_,
						Button::ROUND, Button::NO_BORDER, TextFont::EIGHTEEN,
						NUMERIC_VALUE_CENTER_X_, NUMERIC_VALUE_CENTER_Y_,
						MiscStrs::TUBE_ID_BUTTON_TITLE_SMALL,
						MiscStrs::TC_MM_UNITS_SMALL,
						SettingId::TUBE_ID, this,
						MiscStrs::TUBE_ID_HELP_MESSAGE),

//-----------------------------------------------------------------
// Column #4, Row #3...  [PA/TC active]
//-----------------------------------------------------------------
tubeTypeButton_(        4 * SCRN2_BUTTON_WIDTH_, ROW3_BUTTON_Y_,
						SCRN2_BUTTON_WIDTH_, SCRN2_BUTTON_HEIGHT_,
						Button::LIGHT, SETTING_BUTTON_BORDER_, Button::ROUND,
						Button::NO_BORDER,
						MiscStrs::TUBE_TYPE_BUTTON_TITLE_SMALL,
						SettingId::TUBE_TYPE, this,
						MiscStrs::TUBE_TYPE_HELP_MESSAGE, 18),

//-----------------------------------------------------------------
// Column #5, Row #0...
//-----------------------------------------------------------------
oxygenPercentageButton_(5 * SCRN2_BUTTON_WIDTH_, ROW0_BUTTON_Y_,
						SCRN2_BUTTON_WIDTH_ - SCRN2_BUTTON_WIDTH_SHAVE_, 
						SCRN2_BUTTON_HEIGHT_,
						Button::LIGHT, SCRN2_BUTTON_BORDER_,
						Button::ROUND, Button::NO_BORDER, TextFont::EIGHTEEN,
						NUMERIC_VALUE_CENTER_X_, NUMERIC_VALUE_CENTER_Y_,
						MiscStrs::OXYGEN_PERCENTAGE_BUTTON_TITLE_SMALL,
						MiscStrs::PERCENT_UNITS_SMALL,
						SettingId::OXYGEN_PERCENT, this,
						MiscStrs::OXYGEN_PERCENTAGE_HELP_MESSAGE),

//-----------------------------------------------------------------
// Column #5, Row #1...
//-----------------------------------------------------------------
peepButton_(            5 * SCRN2_BUTTON_WIDTH_, ROW1_BUTTON_Y_,
						SCRN2_BUTTON_WIDTH_ - SCRN2_BUTTON_WIDTH_SHAVE_,
						SCRN2_BUTTON_HEIGHT_, Button::LIGHT,
						SCRN2_BUTTON_BORDER_, Button::ROUND,
						Button::NO_BORDER, TextFont::EIGHTEEN,
						NUMERIC_VALUE_CENTER_X_, NUMERIC_VALUE_CENTER_Y_,
						MiscStrs::PEEP_BUTTON_TITLE_SMALL,
						GuiApp::GetPressureUnitsSmall(),
						SettingId::PEEP, this,
						MiscStrs::PEEP_HELP_MESSAGE),

//-----------------------------------------------------------------
// Column #5, Row #2...
//-----------------------------------------------------------------
highCircuitPressureButton_(5 * SCRN2_BUTTON_WIDTH_, ROW2_BUTTON_Y_,
						   SCRN2_BUTTON_WIDTH_ - SCRN2_BUTTON_WIDTH_SHAVE_, 
						   SCRN2_BUTTON_HEIGHT_,
						   Button::LIGHT, SCRN2_BUTTON_BORDER_,
						   Button::ROUND, Button::NO_BORDER, TextFont::EIGHTEEN,
						   NUMERIC_VALUE_CENTER_X_, NUMERIC_VALUE_CENTER_Y_,
						   MiscStrs::HIGH_CIRCUIT_PRESSURE_BUTTON_TITLE_SMALL,
						   GuiApp::GetPressureUnitsSmall(),
						   SettingId::HIGH_CCT_PRESS, this,
						   MiscStrs::HIGH_CIRCUIT_PRESSURE_HELP_MESSAGE),

//-----------------------------------------------------------------
// Column #5, Row #3...
//-----------------------------------------------------------------
inhaledSpontTidalVolumeButton_(5 * SCRN2_BUTTON_WIDTH_, ROW3_BUTTON_Y_,
							   SCRN2_BUTTON_WIDTH_ - SCRN2_BUTTON_WIDTH_SHAVE_,
							   SCRN2_BUTTON_HEIGHT_,
							   Button::LIGHT, SCRN2_BUTTON_BORDER_,
							   Button::ROUND, Button::NO_BORDER, TextFont::EIGHTEEN,
							   NUMERIC_VALUE_CENTER_X_, NUMERIC_VALUE_CENTER_Y_,
							   MiscStrs::HIGH_INH_SPONT_TIDAL_VOL_BUTTON_TITLE_SMALL,
							   MiscStrs::ML_UNITS_SMALL,
							   SettingId::HIGH_INSP_TIDAL_VOL, this,
							   MiscStrs::HIGH_INH_SPONT_VOL_HELP_MESSAGE),
//-----------------------------------------------------------------
// Column #5, Row #3...  [CPAP active]
//-----------------------------------------------------------------
apneaIntervalButton_(       5 * SCRN2_BUTTON_WIDTH_, ROW3_BUTTON_Y_,
							SCRN2_BUTTON_WIDTH_, SCRN2_BUTTON_HEIGHT_,
							Button::LIGHT, SCRN2_BUTTON_BORDER_,
							Button::ROUND, Button::NO_BORDER, TextFont::EIGHTEEN,
							NUMERIC_VALUE_CENTER_X_, NUMERIC_VALUE_CENTER_Y_,
							MiscStrs::APNEA_INTERVAL_BUTTON_TITLE,
							MiscStrs::SEC_UNITS_SMALL,
							SettingId::APNEA_INTERVAL, this,
							MiscStrs::APNEA_INTERVAL_HELP_MESSAGE),
apneaDisabledMsg_(          MiscStrs::APNEA_DISABLED_WARNING_MSG),

verifySettingsMsg_(         MiscStrs::VERIFY_SETTINGS_MESSAGE),
verifySettingsIcon_(        Image::RCheckArrowIcon),
spontMandTypeMsg_(          MiscStrs::SPONT_MAND_TYPE_MESSAGE),
leftWingtip_(               LEFT_WING_X1_ - WINGTIP_WIDTH_,
							WING_Y_ + WINGTIP_HEIGHT_,
							LEFT_WING_X1_, WING_Y_, WING_PEN_),
leftWing_(                  LEFT_WING_X1_, WING_Y_, LEFT_WING_X2_,
							WING_Y_, WING_PEN_),
rightWing_(                 RIGHT_WING_X1_, WING_Y_, RIGHT_WING_X2_,
							WING_Y_, WING_PEN_),
rightWingtip_(              RIGHT_WING_X2_, WING_Y_, 
							RIGHT_WING_X2_ + WINGTIP_WIDTH_,
							WING_Y_ + WINGTIP_HEIGHT_, WING_PEN_),
titleArea_(                 NULL, SubScreenTitleArea::SSTA_CENTER),
breathTimingDiagram_(       FALSE, TRUE)
{
	CALL_TRACE("VentSetupSubScreen(pSubScreenArea)");


	// Size and position the subscreen
	setX(0);
	setY(0);
	setWidth(LOWER_SUB_SCREEN_AREA_WIDTH);
	setHeight(LOWER_SUB_SCREEN_AREA_HEIGHT);
	setFillColor(Colors::MEDIUM_BLUE);

	// Register for callbacks for all buttons in which we are interested.
	previousSetupButton_.setButtonCallback(this);
	continueButton_.setButtonCallback(this);
	respiratoryRateButton_.setButtonCallback(this);
	inspiratoryTimeButton_.setButtonCallback(this);
	peepHighTimeButton_.setButtonCallback(this);
	expiratoryTimeButton_.setButtonCallback(this);
	peepLowTimeButton_.setButtonCallback(this);
	ieRatioButton_.setButtonCallback(this);
	hlRatioButton_.setButtonCallback(this);
	tubeIdButton_.setButtonCallback(this);
	tubeTypeButton_.setButtonCallback(this);
	newIbwButton_.setButtonCallback(this);


	// set up values for vent type....
	::PVentTypeInfo_->numValues = VentTypeValue::TOTAL_VENT_TYPE_VALUES;
	::PVentTypeInfo_->arrValues[VentTypeValue::INVASIVE_VENT_TYPE] =
	MiscStrs::VENT_TYPE_INVASIVE_VALUE;
	::PVentTypeInfo_->arrValues[VentTypeValue::NIV_VENT_TYPE] =
	MiscStrs::VENT_TYPE_NIV_VALUE;
	ventTypeButton_.setValueInfo(::PVentTypeInfo_);

	// set up values for mode....
	::PModeInfo_->numValues = ModeValue::TOTAL_MODE_VALUES;
	::PModeInfo_->arrValues[ModeValue::AC_MODE_VALUE] =
	MiscStrs::MODE_AC_VALUE;
	::PModeInfo_->arrValues[ModeValue::SIMV_MODE_VALUE] =
	MiscStrs::MODE_SIMV_VALUE;
	::PModeInfo_->arrValues[ModeValue::SPONT_MODE_VALUE] =
	MiscStrs::MODE_SPONT_VALUE;
	::PModeInfo_->arrValues[ModeValue::BILEVEL_MODE_VALUE] =
	MiscStrs::MODE_BILEVEL_VALUE;
	::PModeInfo_->arrValues[ModeValue::CPAP_MODE_VALUE] =
	MiscStrs::MODE_CPAP_VALUE;
	modeButton_.setValueInfo(::PModeInfo_);


	// set up values for mandatory type....
	::PMandTypeInfo_->numValues = MandTypeValue::TOTAL_MAND_TYPES;
	::PMandTypeInfo_->arrValues[MandTypeValue::PCV_MAND_TYPE] =
	MiscStrs::MAND_TYPE_PCV_VALUE;
	::PMandTypeInfo_->arrValues[MandTypeValue::VCV_MAND_TYPE] =
	MiscStrs::MAND_TYPE_VCV_VALUE;
	::PMandTypeInfo_->arrValues[MandTypeValue::VCP_MAND_TYPE] =
	MiscStrs::MAND_TYPE_VCP_VALUE;
	mandatoryTypeButton_.setValueInfo(::PMandTypeInfo_);


	// set up values for spontaneous type....
	::PSpontTypeInfo_->numValues = SupportTypeValue::TOTAL_SUPPORT_TYPES;
	::PSpontTypeInfo_->arrValues[SupportTypeValue::OFF_SUPPORT_TYPE] =
	MiscStrs::SPONT_TYPE_OFF_VALUE;
	::PSpontTypeInfo_->arrValues[SupportTypeValue::PSV_SUPPORT_TYPE] =
	MiscStrs::SPONT_TYPE_PSV_VALUE;
	::PSpontTypeInfo_->arrValues[SupportTypeValue::ATC_SUPPORT_TYPE] =
	MiscStrs::SPONT_TYPE_ATC_VALUE;
	::PSpontTypeInfo_->arrValues[SupportTypeValue::VSV_SUPPORT_TYPE] =
	MiscStrs::SPONT_TYPE_VSV_VALUE;
	::PSpontTypeInfo_->arrValues[SupportTypeValue::PAV_SUPPORT_TYPE] =
	MiscStrs::SPONT_TYPE_PAV_VALUE;
	supportTypeButton_.setValueInfo(::PSpontTypeInfo_);


	// set up values for trigger type....
	::PTriggerTypeInfo_->numValues = TriggerTypeValue::TOTAL_TRIGGER_TYPES;
	::PTriggerTypeInfo_->arrValues[TriggerTypeValue::FLOW_TRIGGER] =
	MiscStrs::TRIGGER_TYPE_FLOW_VALUE;
	::PTriggerTypeInfo_->arrValues[TriggerTypeValue::PRESSURE_TRIGGER] =
	MiscStrs::TRIGGER_TYPE_PRESSURE_VALUE;
	triggerTypeButton_.setValueInfo(::PTriggerTypeInfo_);


	// $[01313] Setting buttons intended for allowing adjustment of the flow ...
	// set up values for flow pattern....
	::PFlowPatternInfo_->numValues = FlowPatternValue::TOTAL_FLOW_PATTERNS;
	::PFlowPatternInfo_->arrValues[FlowPatternValue::SQUARE_FLOW_PATTERN] =
	MiscStrs::FLOW_PATTERN_SQUARE_VALUE_SMALL;
	::PFlowPatternInfo_->arrValues[FlowPatternValue::RAMP_FLOW_PATTERN] =
	MiscStrs::FLOW_PATTERN_RAMP_VALUE_SMALL;
	flowPatternButton_.setValueInfo(::PFlowPatternInfo_);
	flowPatternButton_.setValueTextPosition(::GRAVITY_TOP);


	// set up values for tube type....
	::PTubeTypeInfo_->numValues = TubeTypeValue::TOTAL_TUBE_TYPE_VALUES;
	::PTubeTypeInfo_->arrValues[TubeTypeValue::ET_TUBE_TYPE] =
	MiscStrs::TUBE_TYPE_ET_VALUE;
	::PTubeTypeInfo_->arrValues[TubeTypeValue::TRACH_TUBE_TYPE] =
	MiscStrs::TUBE_TYPE_TRACH_VALUE;
	tubeTypeButton_.setValueInfo(::PTubeTypeInfo_);


	// Position the title graphics on screen 2's breath control status bar.
	mandatoryTypeTitle_.setX(SCRN2_BUTTON_WIDTH_);
	supportTypeTitle_.setX(3 * SCRN2_BUTTON_WIDTH_);
	triggerTypeTitle_.setX(4 * SCRN2_BUTTON_WIDTH_);

	// Position and size the status bar, main settings area and
	// breath timing diagram on screen 2
	statusBar_.setX(STATUS_BAR_X_);
	statusBar_.setY(STATUS_BAR_Y_);
	statusBar_.setWidth(STATUS_BAR_WIDTH_);
	statusBar_.setHeight(STATUS_BAR_HEIGHT_);
	statusBar_.setFillColor(Colors::LIGHT_BLUE);
	statusBar_.setContentsColor(Colors::BLACK);
	statusBar_.addDrawable(&ventTypeTitle_);
	statusBar_.addDrawable(&modeTitle_);
	statusBar_.addDrawable(&mandatoryTypeTitle_);
	statusBar_.addDrawable(&supportTypeTitle_);
	statusBar_.addDrawable(&triggerTypeTitle_);
	statusBar_.addDrawable(&leftWingtip_);
	statusBar_.addDrawable(&leftWing_);
	statusBar_.addDrawable(&rightWing_);
	statusBar_.addDrawable(&rightWingtip_);
	addDrawable(&statusBar_);

	apneaIntervalButton_.setTimingResolutionEnabled(TRUE);
	apneaDisabledMsg_.setY(ROW3_BUTTON_Y_);
	apneaDisabledMsg_.setX(4 * SCRN2_BUTTON_WIDTH_);
	apneaDisabledMsg_.setColor(Colors::BLACK_ON_YELLOW_BLINK_SLOW);
	addDrawable(&apneaDisabledMsg_);

	// these settings provide for the verification-needed mechanism...
	tubeIdButton_.setVerifyNeeded(TRUE);
	tubeTypeButton_.setVerifyNeeded(TRUE);

	verifySettingsIcon_.setX(4 * SCRN2_BUTTON_WIDTH_ + 3);
	verifySettingsIcon_.setY(ROW3_BUTTON_Y_ + SCRN2_BUTTON_HEIGHT_ + 5);
	verifySettingsIcon_.setColor(Colors::BLACK_ON_YELLOW_BLINK_SLOW);
	addDrawable(&verifySettingsIcon_);

	verifySettingsMsg_.setX(verifySettingsIcon_.getX() + 22);
	verifySettingsMsg_.setY(verifySettingsIcon_.getY());
	verifySettingsMsg_.setColor(Colors::WHITE);
	addDrawable(&verifySettingsMsg_);

	spontMandTypeMsg_.setColor(Colors::WHITE);
	spontMandTypeMsg_.setX(SCRN1_BUTTON_X_ + SCRN1_BUTTON_WIDTH_ + SCRN1_BUTTON_X_GAP_ + 2);
	spontMandTypeMsg_.setY(SCRN1_BUTTON_Y_ + SCRN1_BUTTON_HEIGHT_);
	addDrawable(&spontMandTypeMsg_);

	mainSettingsAreaContainer_.setX(MAIN_SETTINGS_AREA_X_);
	mainSettingsAreaContainer_.setY(MAIN_SETTINGS_AREA_Y_);
	mainSettingsAreaContainer_.setWidth(MAIN_SETTINGS_AREA_WIDTH_);
	mainSettingsAreaContainer_.setHeight(MAIN_SETTINGS_AREA_HEIGHT_);
	mainSettingsAreaContainer_.setFillColor(Colors::EXTRA_DARK_BLUE);
	addDrawable(&mainSettingsAreaContainer_);

	breathTimingDiagram_.setX(TIMING_DIAGRAM_X_);
	breathTimingDiagram_.setY(TIMING_DIAGRAM_Y_);
	addDrawable(&breathTimingDiagram_);

	addDrawable(&previousSetupButton_);
	addDrawable(&continueButton_);
	addDrawable(&ibwTitle_);
	addDrawable(&titleArea_);

	// add the drawables, size and position the cctTypeContainer_
	// the box is hidden but we use it to size the container 
	cctTypeBox_.setColor(Colors::BLACK);
	cctTypeLabel_.setColor(Colors::WHITE);
	cctTypeText_.setColor(Colors::WHITE);
	cctTypeContainer_.addDrawable(&cctTypeBox_);
	cctTypeContainer_.setFillColor(Colors::EXTRA_DARK_BLUE);

	cctTypeContainer_.setX( SCRN1_BUTTON_X_ );
	cctTypeContainer_.setY( 0 );
	cctTypeContainer_.sizeToFit();

	cctTypeContainer_.addDrawable(&cctTypeLabel_);
	cctTypeLabel_.setY(CCT_TYPE_BOX_LINE_WIDTH_ + 1);
	cctTypeLabel_.setX(5);

	cctTypeContainer_.addDrawable(&cctTypeText_);
	cctTypeText_.setY(CCT_TYPE_BOX_LINE_WIDTH_ + cctTypeLabel_.getHeight() + 2);
	cctTypeText_.setX(7);

	addDrawable(&cctTypeContainer_);

	const DiscreteValue  PATIENT_CCT_TYPE_VALUE =
	SettingContextHandle::GetSettingValue(ContextId::ACCEPTED_CONTEXT_ID,
										  SettingId::PATIENT_CCT_TYPE);

	switch ( PATIENT_CCT_TYPE_VALUE )
	{
		case PatientCctTypeValue::NEONATAL_CIRCUIT :
			cctTypeText_.setText(MiscStrs::NEO_CIRCUIT_TYPE_MSG);
			break;
		case PatientCctTypeValue::PEDIATRIC_CIRCUIT :
			cctTypeText_.setText(MiscStrs::PED_CIRCUIT_TYPE_MSG);
			break;
		case PatientCctTypeValue::ADULT_CIRCUIT :
			cctTypeText_.setText(MiscStrs::ADULT_CIRCUIT_TYPE_MSG);
			break;
		default :
			AUX_CLASS_ASSERTION_FAILURE(PATIENT_CCT_TYPE_VALUE);
			break;
	}

	//-----------------------------------------------------------------
	// initialize array of Screen #1's setting button pointers...
	//-----------------------------------------------------------------

	// add Screen #1 buttons to array...
	Uint idx = 0u;
	arrScreen1BtnPtrs_[idx++] = &ventTypeButton_;
	arrScreen1BtnPtrs_[idx++] = &modeButton_;
	arrScreen1BtnPtrs_[idx++] = &mandatoryTypeButton_;
	arrScreen1BtnPtrs_[idx++] = &supportTypeButton_;
	arrScreen1BtnPtrs_[idx++] = &triggerTypeButton_;
	arrScreen1BtnPtrs_[idx] = NULL;
	AUX_CLASS_ASSERTION((idx <= VENT_SETUP_MAX_BUTTONS_1), idx);

	for ( idx = 0u; arrScreen1BtnPtrs_[idx] != NULL; idx++ )
	{
		(arrScreen1BtnPtrs_[idx])->setShowViewable(TRUE);
	}

	//-----------------------------------------------------------------
	// add Screen #1's setting buttons to container...
	//-----------------------------------------------------------------

	for ( idx = 0u; arrScreen1BtnPtrs_[idx] != NULL; idx++ )
	{
		// all of Screen #1's buttons attach to this subscreen directly...
		addDrawable(arrScreen1BtnPtrs_[idx]);
	}

	// IBW button is a special case screen 1 button -- add it separately
	addDrawable(&newIbwButton_);

	//-----------------------------------------------------------------
	// initialize array of Screen #2's setting button pointers...
	//-----------------------------------------------------------------

	// add Screen #2 buttons to array...
	idx = 0u;
	arrScreen2BtnPtrs_[idx++] = &highCircuitPressureButton_; // must be 1st...
	arrScreen2BtnPtrs_[idx++] = &discSensButton_;			 // must be 2nd...
	arrScreen2BtnPtrs_[idx++] = &tubeTypeButton_;			 // must be 3nd...
	arrScreen2BtnPtrs_[idx++] = &tubeIdButton_;				 // must be 4th...
	arrScreen2BtnPtrs_[idx++] = &inhaledSpontTidalVolumeButton_;// must be 5th..
	arrScreen2BtnPtrs_[idx++] = &apneaIntervalButton_;			// must be 6th..
	arrScreen2BtnPtrs_[idx++] = &expiratorySensitivityButton_;
	arrScreen2BtnPtrs_[idx++] = &expiratoryTimeButton_;
	arrScreen2BtnPtrs_[idx++] = &peepLowTimeButton_;
	arrScreen2BtnPtrs_[idx++] = &flowAccelerationButton_;
	arrScreen2BtnPtrs_[idx++] = &flowPatternButton_;
	arrScreen2BtnPtrs_[idx++] = &flowSensitivityButton_;
	arrScreen2BtnPtrs_[idx++] = &ieRatioButton_;
	arrScreen2BtnPtrs_[idx++] = &hlRatioButton_;
	arrScreen2BtnPtrs_[idx++] = &inspiratoryPressureButton_;
	arrScreen2BtnPtrs_[idx++] = &inspiratoryTimeButton_;
	arrScreen2BtnPtrs_[idx++] = &peepHighTimeButton_;
	arrScreen2BtnPtrs_[idx++] = &oxygenPercentageButton_;
	arrScreen2BtnPtrs_[idx++] = &peakFlowButton_;
	arrScreen2BtnPtrs_[idx++] = &peepButton_;
	arrScreen2BtnPtrs_[idx++] = &peepLowButton_;
	arrScreen2BtnPtrs_[idx++] = &peepHighButton_;
	arrScreen2BtnPtrs_[idx++] = &plateauTimeButton_;
	arrScreen2BtnPtrs_[idx++] = &pressureSensitivityButton_;
	arrScreen2BtnPtrs_[idx++] = &pressureSupportButton_;
	arrScreen2BtnPtrs_[idx++] = &respiratoryRateButton_;
	arrScreen2BtnPtrs_[idx++] = &tidalVolumeButton_;
	arrScreen2BtnPtrs_[idx++] = &percentSupportButton_;
	arrScreen2BtnPtrs_[idx++] = &volumeSupportButton_;
	arrScreen2BtnPtrs_[idx++] = &highSpontInspTimeButton_;
	arrScreen2BtnPtrs_[idx]   = NULL;
	AUX_CLASS_ASSERTION((idx <= VENT_SETUP_MAX_BUTTONS_2), idx);

	flowAccelerationButton_.setShowViewable(TRUE);

	//-----------------------------------------------------------------
	// add Screen #1's setting buttons to container...
	//-----------------------------------------------------------------

	idx = 0u;
	addDrawable(arrScreen2BtnPtrs_[idx++]);	 // add Pcirc to subscreen...
	addDrawable(arrScreen2BtnPtrs_[idx++]);	 // add Disconnect Sens to subscreen...
	addDrawable(arrScreen2BtnPtrs_[idx++]);	 // add Tube Type to subscreen...
	addDrawable(arrScreen2BtnPtrs_[idx++]);	 // add Tube I. D. to subscreen...
	addDrawable(arrScreen2BtnPtrs_[idx++]);	 // add Vti-spont to subscreen...
	addDrawable(arrScreen2BtnPtrs_[idx++]);	 // add apnea interval Ta to subscreen...

	// the rest of Screen #2's buttons attach to an internal container...
	for ( ; arrScreen2BtnPtrs_[idx] != NULL; idx++ )
	{
		mainSettingsAreaContainer_.addDrawable(arrScreen2BtnPtrs_[idx]);
	}
	// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~VentSetupSubScreen  [Destructor]
//
//@ Interface-Description
// Destroys the Vent Setup subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Does nothing.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

VentSetupSubScreen::~VentSetupSubScreen(void)
{
	CALL_TRACE("~VentSetupSubScreen()");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: acceptHappened
//
//@ Interface-Description
// Called by the BatchSettingsSubScreen when the Accept key is pressed.
// Allows us to accept the vent settings and exit this subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// The operator has accepted the settings so we inform the Settings-Validation
// subsystem of such.  Before we do, however, we first backup the current
// accepted settings though we do not back them up if the operator is doing
// a disaster recovery, i.e. going back to a previous setup.
//
// We then deactivate this subscreen.  We inform the Lower Screen of the
// event as it may need to display the Apnea Setup subscreen.
//
// $[01056] When batch settings are being setup ventilator settings
//			will not be affected
// $[01220] Previous Setup timeout should be restarted when settings accepted.
// $[01257] The Accept key shall be the ultimate confirmation step for all ...
// $[01258] The GUI shall accept all adjusted settings.
// $[01215] The Accept sound shall be produced whenever the accept ...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
VentSetupSubScreen::acceptHappened(void)
{
	CALL_TRACE("acceptHappened()");

	if ( onScreen1_  ||
		 (areSettingsCurrent_()  &&  !isPreviousSetup_   &&
		  !LowerScreen::RLowerScreen.isInPatientSetupMode()) )
	{														// $[TI3.1]
		Sound::Start(Sound::INVALID_ENTRY);
	}
	else
	{														// $[TI3.2]
		// Make the Accept sound
		Sound::Start(Sound::ACCEPT);

		// Deactivate ourselves
		// $[01361] If the vent setup subscreen is displayed, it shall be dismissed.
		getSubScreenArea()->deactivateSubScreen();

		// Now store the current settings (either a new patient or normal
		// ventilation batch).
		// If the Lower Screen is in Patient Setup mode then simply inform
		// it that we have been accepted and let it handle stuff from there.
		if ( LowerScreen::RLowerScreen.isInPatientSetupMode() )
		{													// $[TI1.1]
			// Update Vent startup state.
			GuiApp::SetVentStartupSequenceComplete(TRUE);

			SettingContextHandle::AcceptNewPatientBatch();
			LowerScreen::RLowerScreen.newPatientSetupScreen3Accepted();
		}
		else
		{													// $[TI1.2]
			// Accept Vent Setup batch settings.
			ApneaCorrectionStatus apneaCorrectionStatus =
			SettingContextHandle::AcceptVentSetupBatch();

			// The settings subsystem may have auto-adjusted some apnea
			// parameters upon acceptance of the vent settings above.
			// Pass the correction status to the Lower Screen so that it
			// can decide whether or not to auto-launch the Apnea Setup
			// subscreen.
			LowerScreen::RLowerScreen.reviewApneaSetup(apneaCorrectionStatus);
		}

		// If we're accepting previous setup settings then we must invalidate
		// those previous settings otherwise we should start a timer to timeout
		// the current settings from being recovered.
		if ( isPreviousSetup_ )
		{													// $[TI2.1]
			SettingContextHandle::InvalidateRecoverableBatch();
		}													// $[TI2.2]
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: buttonDownHappened
//
//@ Interface-Description
// Called when certain buttons in Vent Setup are pressed down (those buttons
// for which we register a callback).  We must act upon the Continue,
// Previous Setup and breath timing setting buttons being pressed.
// Passed a poInter to the button as well as 'isByOperatorAction', a flag which
// indicates whether the operator pressed the button down or whether the
// setToDown() method was called.
//---------------------------------------------------------------------
//@ Implementation-Description
// If the continue button or previous setup button were pressed on
// screen 1 then we have to head in to screen 2.  We remove the
// screen 1 graphics and call the layoutScreen2_() method.  If the
// previous setup button was pressed then we tell the Settings-Validation
// subsystem to recover the last setup.
//
// If the Respiratory Rate, Inspiratory Time, Expiratory Time or I:E Ratio
// buttons were pressed down then we need to inform the Breath Timing diagram
// of the fact because it needs to highlight the corresponding setting value
// and possibly display padlocks.
//
// $[01014] Step 1 -- Adjust Ideal Body Weight. When the user initiates the new
//          patient setup...
// $[01015] Step 2 -- Main Control Settings.  After selecting an appropriate 
//          ideal body weight the GUI shall...
// $[01016] In New Patient Setup display breath settings after Main Control
//			settings
// $[01082] Not allowed to change main control and breath settings at same time
// $[01094] 3 padlocks displayed when respiratory rate is selected.
// $[01229] During patient circuit disconnect or severe occlusion the gui ...
// $[01349] The contact sound shall be produced when any select button ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
VentSetupSubScreen::buttonDownHappened(Button *pButton,
									   Boolean isByOperatorAction)
{
	CALL_TRACE("buttonDownHappened(pButton, isByOperatorAction)");

	if ( pButton == &newIbwButton_ )
	{
		// during new patient setup, only the IBW is activated until the user touches it
		if ( LowerScreen::RLowerScreen.isInPatientSetupMode() )
		{
			operateOnButtons_(arrScreen1BtnPtrs_, &SettingButton::activate);
			continueButton_.setShow(TRUE);
			cctTypeContainer_.setShow(TRUE);
			GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY,
										   PromptArea::PA_LOW,
										   PromptStrs::CHOOSE_SETUP_P);
		}
	}
	// Check for the Continue or Previous Setup button being pressed.  If one
	// of them has been then the operator is proceeding to Screen 2.
	// Clear Screen 1 and layout Screen 2 appropriately.
	else if ( (pButton == &continueButton_) || (pButton == &previousSetupButton_) )
	{													// $[TI1]
		CLASS_ASSERTION(isByOperatorAction);

		// deactivate all Screen #1's setting buttons...
		newIbwButton_.deactivate();
		cctTypeContainer_.setShow(FALSE);
		operateOnButtons_(arrScreen1BtnPtrs_, &SettingButton::deactivate);

		// Remove Screen 1 stuff  			
		continueButton_.setShow(FALSE);
		spontMandTypeMsg_.setShow(FALSE);
		titleArea_.setShow(FALSE);

		if ( previousSetupButton_.isVisible() )
		{												// $[TI1.1]
			previousSetupButton_.setShow(FALSE);
		}												// $[TI1.2]

		// Clear the Adjust Panel focus
		AdjustPanel::TakeNonPersistentFocus(NULL, TRUE);

		// Check which of the two buttons were pressed
		if ( pButton == &previousSetupButton_ )
		{												// $[TI1.3]
			// Previous Setup button pressed! Bounce the Previous Setup button,
			// restore the previous setup and set a flag indicating that
			// we are displaying a Previous Setup.
			previousSetupButton_.setToUp();
			SettingContextHandle::RecoverStoredBatch();
			isPreviousSetup_ = TRUE;
		}
		else
		{												// $[TI1.4]
			if ( LowerScreen::RLowerScreen.isInPatientSetupMode() )
			{											// $[TI1.4.1]
				// Notify Settings subsystem that we're proceeding to adjusting
				// the breath setting in the new patient setup sequence.
				SettingContextHandle::
				AdjustNewPatientBatch(BREATH_SETTINGS_ADJUST_PHASE);
			}
			else
			{											// $[TI1.4.2]
				// Notify Settings subsystem that we're proceeding to adjusting
				// the breath setting in the normal vent setup sequence.
				SettingContextHandle::
				AdjustVentSetupBatch(BREATH_SETTINGS_ADJUST_PHASE);
			}

			// Bounce the Continue button
			continueButton_.setToUp();
		}

		onScreen1_ = FALSE;			// We're heading to Screen 2 now
		layoutScreen2_();			// Do Screen 2 layout
	}
	// Check for the Respiratory Rate button being pressed
	else if ( pButton == &respiratoryRateButton_ )
	{													// $[TI3]
		// Inform the breath timing diagram of same, $[01094]
		breathTimingDiagram_.setActiveSetting(SettingId::RESP_RATE);
	}
	// Check for the Inspiratory Time button being pressed
	else if ( pButton == &inspiratoryTimeButton_ )
	{													// $[TI4]
		// Inform the breath timing diagram of same
		breathTimingDiagram_.setActiveSetting(SettingId::INSP_TIME);
	}
	// Check for the Expiratory Time button being pressed
	else if ( pButton == &expiratoryTimeButton_ )
	{													// $[TI5]
		// Inform the breath timing diagram of same
		breathTimingDiagram_.setActiveSetting(SettingId::EXP_TIME);
	}
	// Check for the I:E Ratio button being pressed
	else if ( pButton == &ieRatioButton_ )
	{													// $[TI6]
		// Inform the breath timing diagram of same
		breathTimingDiagram_.setActiveSetting(SettingId::IE_RATIO);
	}
	// Check for the PEEP High Time button being pressed
	else if ( pButton == &peepHighTimeButton_ )
	{													// $[TI7]
		// Inform the breath timing diagram of same
		breathTimingDiagram_.setActiveSetting(SettingId::PEEP_HIGH_TIME);
	}
	// Check for the PEEP Low Time button being pressed
	else if ( pButton == &peepLowTimeButton_ )
	{													// $[TI8]
		// Inform the breath timing diagram of same
		breathTimingDiagram_.setActiveSetting(SettingId::PEEP_LOW_TIME);
	}
	// Check for the H:L Ratio button being pressed
	else if ( pButton == &hlRatioButton_ )
	{													// $[TI9]
		// Inform the breath timing diagram of same
		breathTimingDiagram_.setActiveSetting(SettingId::HL_RATIO);
	}
	// Check for the TubeId and TubeType buttons being pressed
	else if ( pButton == &tubeIdButton_ || pButton == &tubeTypeButton_ )
	{
		if ( !tubeIdButton_.isInVerifyState() && !tubeTypeButton_.isInVerifyState() )
		{
			verifySettingsIcon_.setColor(Colors::BLACK);            
		}
	}
	else
	{
		// Unknown button
		CLASS_ASSERTION_FAILURE();						// $[TI7]
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: buttonUpHappened
//
//@ Interface-Description
// Called when any button in Vent Setup is pressed up.  Passed a poInter to the
// button as well as 'isByOperatorAction', a flag which indicates whether the
// operator pressed the button up or whether the setToUp() method was called.
//---------------------------------------------------------------------
//@ Implementation-Description
// If the Respiratory Rate, Inspiratory Time, Expiratory Time or I:E Ratio
// buttons were pressed up then we need to inform the breath timing diagram
// that there is no active setting so that it can unhighlight the setting
// in question.
//
// There is a possibility that this event occurs after this subscreen is
// removed from the screen (when this subscreen is deactivated and the
// Adjust Panel focus is lost).  In this case we do nothing, simply return.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
VentSetupSubScreen::buttonUpHappened(Button *pButton, Boolean isByOperatorAction)
{
	CALL_TRACE("buttonUpHappened(pButton, )");

	// Don't do anything if this subscreen isn't displayed
	if ( !isVisible() )
	{													// $[TI1]
		return;
	}													// $[TI2]

	if ( pButton == &newIbwButton_ && !isByOperatorAction )
	{
		// $[LC01010] The user shall not be allowed to adjust IBW if any other settings 
		//            button is touched...
		// $[LC02007] during new patient setup, flatten the IBW button once another button is pressed
		if ( LowerScreen::RLowerScreen.isInPatientSetupMode() )
		{
			newIbwButton_.setToFlat();
		}
	}
	// Check for one of the timing buttons being pressed up
	else if ( (pButton == &respiratoryRateButton_)
			  || (pButton == &inspiratoryTimeButton_)
			  || (pButton == &expiratoryTimeButton_)
			  || (pButton == &ieRatioButton_)
			  || (pButton == &peepHighTimeButton_)
			  || (pButton == &peepLowTimeButton_)
			  || (pButton == &hlRatioButton_) )
	{													// $[TI4]
		// Inform the breath timing diagram that there is no active setting
		breathTimingDiagram_.setActiveSetting(SettingId::NULL_SETTING_ID);
	}
	// Check for the Spont Type button being pressed up
	else if ( pButton == &supportTypeButton_ )
	{													// $[TI6]
		const BoundedValue  IBW =
		SettingContextHandle::GetSettingValue(ContextId::ADJUSTED_CONTEXT_ID,
											  SettingId::IBW); 

		if ( SoftwareOptions::IsOptionEnabled(SoftwareOptions::ATC)  &&
			 IBW.value < ::MIN_TC_BODY_WEIGHT_ )
		{											 // $[TI6.1]	
			// Remove prompt advising users that TC is not applicable to
			// patients of this IBW...
			GuiApp::PPromptArea->setPrompt(PromptArea::PA_ADVISORY,
										   PromptArea::PA_LOW,
										   ::NULL_STRING_ID);
		}											 // $[TI6.2]	
	}													// $[TI5]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: layoutScreen2_
//
//@ Interface-Description
// Lays out screen 2.  No parameters needed.  Called before screen 2 is
// displayed.  Displays the breath settings.
//---------------------------------------------------------------------
//@ Implementation-Description
// Decides, based on the main control settings, what setting buttons are
// displayed on the screen.  If we are displaying the previous setup then all
// buttons are set to flat.  The contents of the title area are also setup.
// Besides the screen title, the title area displays the current Mode,
// Mandatory Type, Support Type, Trigger Type and, in New Patient Setup mode,
// the IBW value.
//
// $[01002] Inactive settings and data must be removed from view ...
// $[01051] If a setting is inapplicable then remove its setting button ...
// $[01055] Multiple setting changes are allowed before accepting ...
// $[01071] When Mand. type is PC or mode is SPONT or mode is SIMV...
// $[01081] The breath settings consist of ...
// $[01083] Breath setting button is displayed only when applicable ...
// $[01084] Main control settings displayed in read-only manner when ...
// $[01086] No way to get back to main control settings ...
// $[01091] Prompt user that other settings restored in previous setup ...
// $[01093] Breath timing graph displayed with breath settings.
// $[01093] Breath timing graph displayed with breath settings.
// $[LC02002]\c\ The Apnea Interval of OFF must be approved...
// $[LC02003] when CPAP is selected, operator must approve Apnea Interval OFF...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
VentSetupSubScreen::layoutScreen2_(void)
{
	CALL_TRACE("layoutScreen2_()");

	// these objects are to show in screen #2...
	statusBar_.setShow(TRUE);
	mainSettingsAreaContainer_.setShow(TRUE);
	breathTimingDiagram_.setShow(TRUE);
	spontMandTypeMsg_.setShow(FALSE);

	const ContextSubject *const  P_ADJUSTED_SUBJECT =
	getSubjectPtr_(ContextId::ADJUSTED_CONTEXT_ID);

	const DiscreteValue VENT_TYPE =
	SettingContextHandle::GetSettingValue(ContextId::ADJUSTED_CONTEXT_ID,
										  SettingId::VENT_TYPE);

	const DiscreteValue MODE =
	SettingContextHandle::GetSettingValue(ContextId::ADJUSTED_CONTEXT_ID,
										  SettingId::MODE);

	const DiscreteValue MANDATORY_TYPE =
	SettingContextHandle::GetSettingValue(ContextId::ADJUSTED_CONTEXT_ID,
										  SettingId::MAND_TYPE);

	const DiscreteValue SPONT_TYPE =
	SettingContextHandle::GetSettingValue(ContextId::ADJUSTED_CONTEXT_ID,
										  SettingId::SUPPORT_TYPE);

	const DiscreteValue TRIGGER_TYPE =
	SettingContextHandle::GetSettingValue(ContextId::ADJUSTED_CONTEXT_ID,
										  SettingId::TRIGGER_TYPE);

	// Make sure status bar or title area does not display IBW value
	ibwTitle_.setShow(FALSE);

	// always show IBW in status bar now that it can be changed OTF
	{
		// Format the Ideal Body Weight value for display in the title area.
		// Note: we assume that the IBW setting is stored with either a precision of TENTHS or ONES!
		BoundedValue  boundedValue =
		SettingContextHandle:: GetSettingValue(ContextId::ADJUSTED_CONTEXT_ID,
											   SettingId::IBW); 

		Boolean isIbwChanged = P_ADJUSTED_SUBJECT->isSettingChanged(SettingId::IBW);

		wchar_t ibwText[128];                                        
		Int count = swprintf(ibwText, MiscStrs::IBW_FORMAT_STRING_2,
							isIbwChanged ? 'I' : 'N',
							boundedValue.precision == TENTHS ? 1 : 0,
							boundedValue.value,
							MiscStrs::KILOGRAM_STRING);

		AUX_CLASS_ASSERTION((count < sizeof(ibwText)), count );

		ibwTitle_.setText(ibwText);
		ibwTitle_.setColor(Colors::BLACK);
		ibwTitle_.setHighlighted( isIbwChanged );
		statusBar_.addDrawable(&ibwTitle_);

		ibwTitle_.setShow(TRUE);
	}

	// Decide which subscreen title to display
	if ( isPreviousSetup_ )
	{													// $[TI1.2]
		setVentSetupTabTitleText_(MiscStrs::VENT_PREVIOUS_SETUP_TAB_BUTTON_LABEL);
	}
	else if ( !areSettingsCurrent_() )
	{													// $[TI1.3]
		setVentSetupTabTitleText_(MiscStrs::VENT_PROPOSED_SETUP_TAB_BUTTON_LABEL);

		// Set Primary prompt to "Press ACCEPT if OK."
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY,
									   PromptArea::PA_HIGH, PromptStrs::PRESS_ACCEPT_IF_OK_P);

		// Set secondary prompt to 
		// "To apply: Press ACCEPT/To cancel: Press SETUP"
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
									   PromptArea::PA_LOW, PromptStrs::VENT_SETUP_PROCEED_CANCEL_S);
	}
	else
	{													// $[TI1.4]
		setVentSetupTabTitleText_(MiscStrs::VENT_CURRENT_SETUP_TAB_BUTTON_LABEL);
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
									   PromptArea::PA_LOW, PromptStrs::VENT_SETUP_CANCEL_S);
	}

	if ( isPreviousSetup_ )
	{// $[TI27.1]
		breathTimingDiagram_.setForcedPadlockState(
												  BreathTimingDiagram::FORCED_OFF
												  );
	}
	else
	{// $[TI27.2]
		breathTimingDiagram_.setForcedPadlockState(
												  BreathTimingDiagram::NONE
												  );
	}

	// Display the Breath Timing Diagram.
	breathTimingDiagram_.activate();

	//
	// Display the setting buttons and breath control titles
	//

	// Both the mode and the vent type are used in this expression
	// since we want the mode to be italicized when exiting NIV since the
	// vent type field for (normal) invasive vent type is blank.
	const Boolean  IS_MODE_CHANGED = 
	P_ADJUSTED_SUBJECT->isSettingChanged(SettingId::MODE) ||
	P_ADJUSTED_SUBJECT->isSettingChanged(SettingId::VENT_TYPE);
	// $[TI31.1] TRUE, $[TI31.2] FALSE

	// Display the correct Vent Type title in the status bar 
	StringId  titleFormatStr;

	switch ( VENT_TYPE )
	{
		case VentTypeValue::INVASIVE_VENT_TYPE:	   // $[TI32.1]
			titleFormatStr = MiscStrs::VENT_TYPE_INVASIVE_TITLE;
			break;
		case VentTypeValue::NIV_VENT_TYPE:	  // $[TI32.2]
			titleFormatStr = MiscStrs::VENT_TYPE_NIV_TITLE;
			break;
		default:	// $[TI32.3]
			// unexpected mode value...
			titleFormatStr = L"{p=12,x=4,y=16,%c:*}";  
			break;
	}

	// set the state of vent type's title field...
	setTitleChangeState_(titleFormatStr, IS_MODE_CHANGED, ventTypeTitle_);

	switch ( MODE )
	{
		case ModeValue::AC_MODE_VALUE:						// $[TI5.1]
			titleFormatStr = MiscStrs::MODE_AC_TITLE;
			break;

		case ModeValue::SIMV_MODE_VALUE:					// $[TI5.2]
			titleFormatStr = MiscStrs::MODE_SIMV_TITLE;
			break;

		case ModeValue::SPONT_MODE_VALUE:					// $[TI5.3]
			titleFormatStr = MiscStrs::MODE_SPONT_TITLE;
			break;

		case ModeValue::BILEVEL_MODE_VALUE:					// $[TI5.5]
			titleFormatStr = MiscStrs::MODE_BILEVEL_TITLE;
			break;

		case ModeValue::CPAP_MODE_VALUE:
			titleFormatStr = MiscStrs::MODE_CPAP_TITLE;
			break;

		default:											// $[TI5.4]
			// unexpected mode value...
			titleFormatStr = L"{p=12,x=29,y=16,%c:***}";  
			break;
	}

	// set the state of Mode's title field...
	setTitleChangeState_(titleFormatStr, IS_MODE_CHANGED, modeTitle_);


	// Display the correct Mandatory Type title in the title area and any
	// appropriate setting buttons.
	const Boolean  IS_MAND_TYPE_CHANGED = 
	P_ADJUSTED_SUBJECT->isSettingChanged(SettingId::MAND_TYPE);

	// Display the correct Mandatory Type title in the title area and any
	// appropriate setting buttons.
	switch ( MANDATORY_TYPE )
	{
		case MandTypeValue::PCV_MAND_TYPE:					// $[TI9.1]
			titleFormatStr = MiscStrs::MAND_TYPE_PCV_TITLE;
			break;

		case MandTypeValue::VCV_MAND_TYPE:					// $[TI9.2]
			titleFormatStr = MiscStrs::MAND_TYPE_VCV_TITLE;
			tidalVolumeButton_.setMessageId(MiscStrs::TIDAL_VOLUME_VCV_HELP_MSG);
			break;

		case MandTypeValue::VCP_MAND_TYPE:					// $[TI9.3]
			titleFormatStr = MiscStrs::MAND_TYPE_VCP_TITLE;
			tidalVolumeButton_.setMessageId(MiscStrs::TIDAL_VOLUME_VCP_HELP_MSG);
			break;

		default:
			// unexpected mandatory type value...
			titleFormatStr = L"{p=12,x=91,y=16,%c:***}";  
			break;
	}

	// set the state of Mandatory Type's title field...
	setTitleChangeState_(titleFormatStr, IS_MAND_TYPE_CHANGED,
						 mandatoryTypeTitle_);


	// Display the correct Support Type title in the status bar and any
	// appropriate setting buttons.
	const Applicability::Id  SPONT_TYPE_APPLIC =
	SettingContextHandle::GetSettingApplicability(ContextId::ADJUSTED_CONTEXT_ID,
												  SettingId::SUPPORT_TYPE);

	// Clear it if the Mode is AC
	if ( SPONT_TYPE_APPLIC == Applicability::INAPPLICABLE )
	{													// $[TI15.1]
		supportTypeTitle_.setShow(FALSE);
	}
	else
	{													// $[TI15.2]
		supportTypeTitle_.setShow(TRUE);

		const Boolean  IS_SPONT_TYPE_CHANGED = 
		P_ADJUSTED_SUBJECT->isSettingChanged(SettingId::SUPPORT_TYPE);

		switch ( SPONT_TYPE )
		{
			case SupportTypeValue::PSV_SUPPORT_TYPE:		// $[TI16.1]
				titleFormatStr = MiscStrs::SPONT_TYPE_PSV_TITLE;
				break;

			case SupportTypeValue::OFF_SUPPORT_TYPE:		// $[TI16.2]
				// only show "NONE", if its changed to "NONE"...
				titleFormatStr = (IS_SPONT_TYPE_CHANGED)
								 ? MiscStrs::SPONT_TYPE_OFF_TITLE  // $[TI16.2.1]
								 : ::NULL_STRING_ID;			   // $[TI16.2.2]
				break;

			case SupportTypeValue::ATC_SUPPORT_TYPE:		// $[TI16.3]
				titleFormatStr = MiscStrs::SPONT_TYPE_ATC_TITLE;
				break;

			case SupportTypeValue::VSV_SUPPORT_TYPE:		// $[TI16.4]
				titleFormatStr = MiscStrs::SPONT_TYPE_VSV_TITLE;
				break;

			case SupportTypeValue::PAV_SUPPORT_TYPE:		// $[TI16.5]
				titleFormatStr = MiscStrs::SPONT_TYPE_PAV_TITLE;
				break;

			default:
				// unexpected spontaneous type value...
				titleFormatStr = L"{p=12,x=31,y=16,%c:***}";  
				break;
		}

		if ( titleFormatStr != ::NULL_STRING_ID )
		{												// $[TI16.6]
			// set the state of Spontaneous Type's title field...
			setTitleChangeState_(titleFormatStr, IS_SPONT_TYPE_CHANGED,
								 supportTypeTitle_);
		}
		else
		{												// $[TI16.7]
			supportTypeTitle_.setShow(FALSE);
		}

		// Change Expiratory Sensitivity label from percent to (L/min) if 
		// support type is set to PAV, otherwise it is label as percent
		if ( SPONT_TYPE == SupportTypeValue::PAV_SUPPORT_TYPE )
		{
			expiratorySensitivityButton_.setUnitsText(MiscStrs::L_PER_MIN_UNITS_SMALL);
			expiratorySensitivityButton_.setMessageId(MiscStrs::EXPIRATORY_SENSITIVITY_PAV_HELP_MESSAGE);

		}
		else
		{
			expiratorySensitivityButton_.setUnitsText(MiscStrs::PERCENT_UNITS_SMALL);
			expiratorySensitivityButton_.setMessageId(MiscStrs::EXPIRATORY_SENSITIVITY_HELP_MESSAGE);

		}

	}

	// Display the appropriate Trigger Type title in the title area
	const Boolean  IS_TRIGGER_TYPE_CHANGED = 
	P_ADJUSTED_SUBJECT->isSettingChanged(SettingId::TRIGGER_TYPE);

	switch ( TRIGGER_TYPE )
	{
		case TriggerTypeValue::FLOW_TRIGGER:				// $[TI19.1]
			titleFormatStr = MiscStrs::TRIGGER_TYPE_FLOW_TITLE;
			break;

		case TriggerTypeValue::PRESSURE_TRIGGER:			// $[TI19.2]
			titleFormatStr = MiscStrs::TRIGGER_TYPE_PRESSURE_TITLE;
			break;

		default:
			// unexpected trigger type value...
			titleFormatStr = L"{p=12,x=30,y=16,%c:***}";  
			break;
	}

	// set the state of Trigger Type's title field...
	setTitleChangeState_(titleFormatStr, IS_TRIGGER_TYPE_CHANGED,
						 triggerTypeTitle_);

	// Adjust Patient Data with the appropriate label, value and unit depending
	// on the Mode, MandType and SpontType.
	// The following logic depends on if SupportType is applicable
	if ( SPONT_TYPE_APPLIC != Applicability::INAPPLICABLE )
	{															  // $[TI20.0.0]       
		if ( MANDATORY_TYPE == MandTypeValue::VCP_MAND_TYPE )
		{														   // $[TI20.0.1.0]
			if ( SPONT_TYPE == SupportTypeValue::ATC_SUPPORT_TYPE )
			{														// $[TI20.0.1.1]          
				inhaledSpontTidalVolumeButton_.setTitleText(MiscStrs::HIGH_INH_TIDAL_VOL_BUTTON_TITLE_SMALL);
				inhaledSpontTidalVolumeButton_.setMessageId(MiscStrs::HIGH_INH_TIDAL_VOL_HELP_MESSAGE);
				addDrawable(&inhaledSpontTidalVolumeButton_);
			}
			else
			{														// $[TI20.0.1.2]
				inhaledSpontTidalVolumeButton_.setTitleText(MiscStrs::HIGH_INH_MAND_TIDAL_VOL_BUTTON_TITLE_SMALL);
				inhaledSpontTidalVolumeButton_.setMessageId(MiscStrs::HIGH_INH_MAND_VOL_HELP_MESSAGE);
				addDrawable(&inhaledSpontTidalVolumeButton_);
			}       
		}
		else
			if ( MODE == ModeValue::SIMV_MODE_VALUE )
		{														   // $[TI20.0.2.0]
			if ( SPONT_TYPE == SupportTypeValue::ATC_SUPPORT_TYPE )
			{													   // $[TI20.0.2.1]
				inhaledSpontTidalVolumeButton_.setTitleText(MiscStrs::HIGH_INH_SPONT_TIDAL_VOL_BUTTON_TITLE_SMALL);
				inhaledSpontTidalVolumeButton_.setMessageId(MiscStrs::HIGH_INH_SPONT_VOL_HELP_MESSAGE);
				addDrawable(&inhaledSpontTidalVolumeButton_);
			}
		}
		else
		{														   // $[TI20.0.3.0]
			if ( SPONT_TYPE == SupportTypeValue::ATC_SUPPORT_TYPE ||
				 SPONT_TYPE == SupportTypeValue::PAV_SUPPORT_TYPE ||
				 SPONT_TYPE == SupportTypeValue::VSV_SUPPORT_TYPE )
			{														// $[TI20.0.3.1]
				inhaledSpontTidalVolumeButton_.setTitleText(MiscStrs::HIGH_INH_SPONT_TIDAL_VOL_BUTTON_TITLE_SMALL);
				inhaledSpontTidalVolumeButton_.setMessageId(MiscStrs::HIGH_INH_SPONT_VOL_HELP_MESSAGE);
				addDrawable(&inhaledSpontTidalVolumeButton_);
			}
		}
	}
	else
	{															  // $[TI20.1.0]
		if ( MANDATORY_TYPE == MandTypeValue::VCP_MAND_TYPE )
		{														   // $[TI20.1.1]
			inhaledSpontTidalVolumeButton_.setTitleText(MiscStrs::HIGH_INH_MAND_TIDAL_VOL_BUTTON_TITLE_SMALL);
			inhaledSpontTidalVolumeButton_.setMessageId(MiscStrs::HIGH_INH_MAND_VOL_HELP_MESSAGE);
			addDrawable(&inhaledSpontTidalVolumeButton_);
		}
	}



	// Activate all setting buttons in screen 2.
	operateOnButtons_(arrScreen2BtnPtrs_, &SettingButton::activate);

	// $[TI33.1] TRUE, $[TI33.2] FALSE
	discSensButton_.setShow(VENT_TYPE == VentTypeValue::NIV_VENT_TYPE);

	const DiscreteValue LEAK_COMP_ENABLED_VALUE =
	SettingContextHandle::GetSettingValue(ContextId::ADJUSTED_CONTEXT_ID,
										  SettingId::LEAK_COMP_ENABLED);

	if ( LEAK_COMP_ENABLED_VALUE == LeakCompEnabledValue::LEAK_COMP_ENABLED )
	{
		discSensButton_.setUnitsText(MiscStrs::L_PER_MIN_UNITS_SMALL);
		discSensButton_.setMessageId(MiscStrs::DISCONNECTION_SENSITIVITY_L_PER_MIN_HELP_MESSAGE);
	}
	else
	{
		discSensButton_.setUnitsText(MiscStrs::PERCENT_UNITS_SMALL);
		discSensButton_.setMessageId(MiscStrs::DISCONNECTION_SENSITIVITY_HELP_MESSAGE);
	}

	apneaIntervalButton_.setShow(MODE == ModeValue::CPAP_MODE_VALUE);

	SettingPtr pSetting = SettingsMgr::GetSettingPtr(SettingId::APNEA_INTERVAL);
	const BoundedValue  APNEA_INTERVAL = pSetting->getAdjustedValue();
	apneaDisabledMsg_.setShow(APNEA_INTERVAL == DEFINED_UPPER_ALARM_LIMIT_OFF);

	// Display of the notification message led by an attention icon.
	if ( tubeIdButton_.isInVerifyState() || tubeTypeButton_.isInVerifyState() )
	{
		verifySettingsIcon_.setColor(Colors::BLACK_ON_YELLOW_BLINK_SLOW);
		verifySettingsIcon_.setShow(TRUE);
		verifySettingsMsg_.setShow(TRUE);
	}
	else
	{
		verifySettingsIcon_.setColor(Colors::BLACK);            
		verifySettingsIcon_.setShow(FALSE);
		verifySettingsMsg_.setShow(FALSE);
	}

	tubeIdButton_.setTitleText(MiscStrs::TUBE_ID_BUTTON_TITLE_SMALL);
	tubeTypeButton_.setTitleText(MiscStrs::TUBE_TYPE_BUTTON_TITLE_SMALL);

	// Setup Primary, Advisory and Secondary prompts.  Which ones we set depends
	// on what mode we're in
	if ( LowerScreen::RLowerScreen.isInPatientSetupMode() )
	{												// $[TI22.1]
		// New Patient Setup mode, set Primary prompt to "Adjust settings
		// as needed."
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY,
									   PromptArea::PA_HIGH,
									   PromptStrs::ADJUST_SETTINGS_P);
		// Set Advisory prompt to "All values above auto-set!"
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_ADVISORY,
									   PromptArea::PA_LOW,
									   PromptStrs::SETTINGS_AUTO_SET_A);
	}
	else
	{												// $[TI22.2]
		// Normal ventilation mode, set Primary prompt to "Adjust
		// settings as needed."
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY,
									   PromptArea::PA_LOW,
									   PromptStrs::ADJUST_SETTINGS_P);
	}

	// Check if the settings have been adjusted or if they are previous
	// settings or if we're in New Patient Setup mode.  If so, display the
	// appropriate Secondary prompt
	if ( LowerScreen::RLowerScreen.isInPatientSetupMode() )
	{												// $[TI23.2]
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
									   PromptArea::PA_LOW,
									   PromptStrs::NEW_PATIENT_PROCEED_CANCEL_S);
	}
	else if ( !areSettingsCurrent_()  ||  isPreviousSetup_ )
	{												// $[TI23.1]
		// $[01059] When no setting button is selected, in a state where ...
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
									   PromptArea::PA_LOW,
									   PromptStrs::VENT_SETUP_PROCEED_CANCEL_S);
	}

	// Check if we are displaying a previous setup.
	if ( isPreviousSetup_ )
	{													// $[TI26.1]
		// Set all buttons to flat mode and display correct Primary and
		// Advisory prompts
		operateOnButtons_(arrScreen2BtnPtrs_, &SettingButton::setToFlat);

		// Set Primary prompt to "Press ACCEPT if OK."
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY,
									   PromptArea::PA_HIGH, PromptStrs::PRESS_ACCEPT_IF_OK_P);

		// Set Advisory prompt to inform the operator that all settings,
		// including Alarm and Apnea settings, will be restored also. $[01091]
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_ADVISORY,
									   PromptArea::PA_LOW,
									   PromptStrs::VENT_SETUP_ALARMS_AND_APNEA_RESTORED_A);
	}													// $[TI26.2]

	// Make sure this subscreen has the default adjust panel focus
	AdjustPanel::TakeNonPersistentFocus(this, TRUE, FALSE);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: timerEventHappened
//
//@ Interface-Description
// Called when the Subscreen Setting Change timer or the Recoverable Setup
// timer that we started runs out.  Passed the id of the timer.
//---------------------------------------------------------------------
//@ Implementation-Description
// If it's the Recoverable Setup timeout that occurred then we invalidate
// the recoverable setup in the Settings-Validation subsystem and we
// remove the Previous Setup button if it's visible.
//
// Otherwise the Subscreen Setting Change timer occurred and we do the
// following:
//
// If the New Patient Setup sequence is active then we inform the Lower
// Screen of the event and it will send the user back to the Vent Startup
// subscreen.
//
// If not, then we reset the subscreen back to screen 1 by deactivating and
// then activating this subscreen.
//
// There is a possibility that the Subscreen Setting Change timeout occurs
// after this subscreen has been removed from the display.  In this case we
// ignore the event.
//
// $[01087] After 3 minutes of user inactivity ...
// $[01089] If the 45 minute recoverable setup timer goes off then ...
//---------------------------------------------------------------------
//@ PreCondition
// The timer id must be the Subscreen Setting Change or Recoverable Setup
// Timer id.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
VentSetupSubScreen::timerEventHappened(GuiTimerId::GuiTimerIdType timerId)
{
	CALL_TRACE("timerEventHappened(timerId)");

	if ( isVisible() )
	{													// $[TI1.1]
		switch ( timerId )
		{
			case GuiTimerId::SUBSCREEN_SETTING_CHANGE_TIMEOUT:	// $[TI2.1]
				if ( LowerScreen::RLowerScreen.isInPatientSetupMode() )
				{											// $[TI3.1]
					// In New Patient Setup mode simply inform the Lower Screen
					// of the timeout
					LowerScreen::RLowerScreen.activityTimeout();
				}
				else
				{											// $[TI3.2]
					// Go back to Screen 1 by reactivating this screen
					deactivate();
					activate();
				}
				break;

			default:
				AUX_CLASS_ASSERTION_FAILURE(timerId);
				break;
		}
	}													// $[TI1.2]

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelRestoreFocusHappened()
//
//@ Interface-Description
//  This virtual function of BatchSettingsSubScreen is overriden here
//  to always clear the primary prompt when panel focus is lost.
//---------------------------------------------------------------------
//@ Implementation-Description
//  If no batch setting are changed, then clears the high priority
//  prompt. If batch settings have been changed, changes the prompt
//  according to which screen (screen 1 or screen 2) is active.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
VentSetupSubScreen::adjustPanelRestoreFocusHappened(void)
{
	CALL_TRACE("adjustPanelRestoreFocusHappened()");

	if ( !areSettingsCurrent_() )
	{														// $[TI1.1]
		if ( onScreen1_ )
		{													// $[TI2.1]
			// Set Primary "Press CONTINUE if OK." prompt.
			GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY,
										   PromptArea::PA_HIGH, PromptStrs::PRESS_CONTINUE_IF_OK_P);
		}
		else
		{													// $[TI2.2]
			// Set Primary "Press ACCEPT if OK." prompt.
			GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY,
										   PromptArea::PA_HIGH, PromptStrs::PRESS_ACCEPT_IF_OK_P);
		}
	}
	else
	{														// $[TI1.2]
		// Clear the high priority prompts
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY,
									   PromptArea::PA_HIGH, NULL_STRING_ID);
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setVentSetupTabTitleText_
//
//@ Interface-Description
//  Sets the title on the vent setup subscreen select tab button.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls the LowerScreenSelectArea method to set the button's title.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
VentSetupSubScreen::setVentSetupTabTitleText_( StringId stringId )
{
	(LowerScreen::RLowerScreen.getLowerScreenSelectArea())->
	setVentSetupTabTitleText(stringId);
	// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
VentSetupSubScreen::SoftFault(const SoftFaultID  softFaultID,
							  const Uint32       lineNumber,
							  const char*        pFileName,
							  const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, VENTSETUPSUBSCREEN,
							lineNumber, pFileName, pPredicate);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activateHappened_
//
//@ Interface-Description
// Called by LowerSubScreenArea just before this subscreen is displayed
// allowing us to do any necessary display setup.  Takes no arguments.
//---------------------------------------------------------------------
//@ Implementation-Description
// This is where the screen 1 contents are setup.  First of all we start the
// subscreen setting change timer.  We then begin adjustment of the accepted
// settings.  The subscreen container is cleared of all drawables.  The screen
// 1 contents are then added to the container.  The previous setup button is
// added only if there is a set of previous settings available in the
// Settings-Validation subsystem.  Various prompts are displayed to assist the
// user.  The screen title is displayed (the exact title depends on whether we
// are in New Patient Setup mode or not.  The IBW value is displayed here if we
// are in this mode).
//
// $[01015] In New Patient Setup, after IBW is adjusted, display Main
//			Control settings along with Continue buttons and review
//			prompts
// $[01080] Main control settings consist of ...
// $[01082] Not allowed to change main control and breath settings at same time
// $[01086] Continue button must be provided ...
// $[01088] Must provide a Previous Setup button when ...
// $[01092] Previous setup unavailable after a setup is recovered.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
VentSetupSubScreen::activateHappened_(void)
{
	CALL_TRACE("activateHappened_()");

	BoundedValue boundedValue =
	SettingContextHandle::GetSettingValue(ContextId::ADJUSTED_CONTEXT_ID,
										  SettingId::IBW); 

	onScreen1_ = TRUE;				// We always start on Screen 1
	isPreviousSetup_ = FALSE;		// No previous setup settings displayed

	// Check if we want to adjust the New Patient settings.
	if ( LowerScreen::RLowerScreen.isInPatientSetupMode() )
	{													// $[TI1.1]
		// this call initializes the main settings of IBW, vent-type, mode,
		// mand, spontaneous type and trigger type to their new-patient values
		SettingContextHandle::AdjustNewPatientBatch(IBW_ADJUST_PHASE);
		SettingContextHandle::AdjustNewPatientBatch(MAIN_CONTROL_ADJUST_PHASE);

		// Set Primary prompt to "Adjust IBW setting."
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY, PromptArea::PA_LOW,
									   PromptStrs::ADJUST_IBW_P);

		// ALL settings are "changed" in NPS...
		setCurrentSettingsFlag_(FALSE);

		// Set Secondary prompt to tell user how to continue or cancel.
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
									   PromptArea::PA_LOW, PromptStrs::NEW_PATIENT_CONTINUE_RESTART_S);

		// Set the title to New Patient Setup
		titleArea_.setTitle(MiscStrs::NEW_PATIENT_VENT_SETUP_TITLE);

		// deactivate and hide all the setting buttons except for IBW
		operateOnButtons_(arrScreen1BtnPtrs_, &SettingButton::deactivate);
		continueButton_.setShow(FALSE);
		newIbwButton_.activate();
		cctTypeContainer_.setShow(TRUE);
	}
	else
	{													// $[TI1.2]
		// Setup normal adjustment of main control ventilator settings
		SettingContextHandle::AdjustVentSetupBatch(MAIN_CONTROL_ADJUST_PHASE);

		// Normal ventilation mode, set Primary prompt to "Adjust settings as needed."
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY,
									   PromptArea::PA_LOW, PromptStrs::ADJUST_SETTINGS_P);

		// Set Secondary prompt to tell user how to continue or cancel.
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
									   PromptArea::PA_LOW, PromptStrs::VENT_SETUP_CONTINUE_S);

		titleArea_.setTitle(MiscStrs::CURRENT_VENT_SETTINGS_TITLE);
		setVentSetupTabTitleText_(MiscStrs::VENT_CURRENT_SETUP_TAB_BUTTON_LABEL);

		ibwTitle_.setShow(FALSE);

		operateOnButtons_(arrScreen1BtnPtrs_, &SettingButton::activate);
		newIbwButton_.activate();
		cctTypeContainer_.setShow(TRUE);
		continueButton_.setShow(TRUE);
	}

	// must not query for this value before the 'SettingContextHandle' calls
	// above can initialize this value appropriately...
	const DiscreteValue MODE =
	SettingContextHandle::GetSettingValue(ContextId::ADJUSTED_CONTEXT_ID,
										  SettingId::MODE);

	// these objects are not to show in screen #1...
	statusBar_.setShow(FALSE);
	mainSettingsAreaContainer_.setShow(FALSE);
	breathTimingDiagram_.setShow(FALSE);
	verifySettingsIcon_.setShow(FALSE);
	verifySettingsMsg_.setShow(FALSE);
	apneaDisabledMsg_.setShow(FALSE);

	spontMandTypeMsg_.setShow((MODE == ModeValue::CPAP_MODE_VALUE) ||
							  (MODE == ModeValue::SPONT_MODE_VALUE));
	titleArea_.setShow(TRUE);

	// Show previous setup button only if the settings are recoverable and
	// we're not in settings lockout mode.
	if ( SettingContextHandle::IsRecoverableBatchValid() )
	{													// $[TI4.1]
		previousSetupButton_.setShow(TRUE);
	}
	else
	{
		previousSetupButton_.setShow(FALSE);            
	}													// $[TI4.2]

	// deactivate all Screen 2 buttons
	operateOnButtons_(arrScreen2BtnPtrs_, &SettingButton::deactivate);

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: deactivateHappened_
//
//@ Interface-Description
// Called by LowerSubScreenArea just after this subscreen is removed from the
// display allowing us to do any necessary cleanup.  Takes no arguments.
//---------------------------------------------------------------------
//@ Implementation-Description
// We release the Adjust Panel focus, clear all prompts that we may have
// displayed, tell the breath timing diagram that there's no active settings
// and make sure all screen 2 buttons are in the up position (they may have
// been flat due to displaying a previous setup).  Don't put them up, though,
// if Settings is locked out.  If Settings is not locked out, make sure all
// Screen 2 buttons are put in the up position if they were flat.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
VentSetupSubScreen::deactivateHappened_(void)
{
	CALL_TRACE("deactivateHappened_()");

	// deactivate all setting buttons...
	newIbwButton_.deactivate();
	operateOnButtons_(arrScreen1BtnPtrs_, &SettingButton::deactivate);
	operateOnButtons_(arrScreen2BtnPtrs_, &SettingButton::deactivate);

	breathTimingDiagram_.deactivate();

	// restore the setup tab button to its default title
	setVentSetupTabTitleText_(MiscStrs::VENT_SETUP_TAB_BUTTON_LABEL);
	// $[TI1]
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: valueUpdateHappened_
//
//@ Interface-Description
// Called when a setting has changed. Passed the following parameters:
// >Von
//	settingId	The enum id of the setting which changed.
//	contextId	The context in which the change happened, either
//				ACCEPTED_CONTEXT_ID or ADJUSTABLE_CONTEXT_ID.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
// If this subscreen is not displayed we ignore the setting change.  Otherwise,
// we do the following: if we're on screen 1 then we have to check whether we
// should show the Support Type button or not (hide it if mode is A/C) and show
// the Spont Mandatory Type message (shown in Spont mode) or not.  We also 
// remove the Previous Setup button if it was being displayed.
//
// A screen 2 change to the Constant During Rate change setting
// will cause a new timing button to be displayed.
//
// If it is the first change on either screen then we record
// the fact that the settings have been adjusted ( we need this to determine
// when to activate the ACCEPT key).
// We also change the screen title to "Proposed" instead of "Current".
//
// $[01058] The accept key shall not be enabled... until at least one setting..
// $[01090] Remove Previous Setup button if a change is made.
// $[CL01001] When a non-selected padlock is selected, the corresponding
//            timing parameter's setting button is displayed, and selected,
//            in place of the "old" constant setting button.
//---------------------------------------------------------------------
//@ PreCondition
// The qualifierId must be either ACCEPTED or ADJUSTED.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
VentSetupSubScreen::valueUpdateHappened_(
										const BatchSettingsSubScreen::TransitionId_ transitionId,
										const Notification::ChangeQualifier         qualifierId,
										const ContextSubject*                       pSubject
										)
{
	CALL_TRACE("valueUpdateHappened_(transitionId, qualifierId, pSubject)");

	AUX_CLASS_PRE_CONDITION((qualifierId == Notification::ACCEPTED  || 
							 qualifierId == Notification::ADJUSTED),
							qualifierId)

	const DiscreteValue  MODE_VALUE =
	SettingContextHandle::GetSettingValue(ContextId::ADJUSTED_CONTEXT_ID,
										  SettingId::MODE);

	// Show or hide the Spont Mandatory Type
	// warning message based on the current Mode value...
	spontMandTypeMsg_.setShow((MODE_VALUE == ModeValue::SPONT_MODE_VALUE || 
							   MODE_VALUE == ModeValue::CPAP_MODE_VALUE) && onScreen1_);


	if ( onScreen1_ )
	{						// $[TI1]	
		// Some setting has been adjusted.  We may now be in the state that
		// at least one setting is changed (we must display a proposed screen
		// title) or no settings have been changed (if the current setting
		// was adjusted back to its original value in which case we display
		// a "current" screen title).  Don't bother changing screen titles
		// if we're in New Patient Setup mode.
		if ( !LowerScreen::RLowerScreen.isInPatientSetupMode() )
		{												// $[TI3.3]
			if ( transitionId == BatchSettingsSubScreen::CURRENT_TO_PROPOSED )
			{											// $[TI3.3.1]
				// Title changes to proposed
				titleArea_.setTitle(MiscStrs::PROPOSED_VENT_SETTINGS_TITLE);
				setVentSetupTabTitleText_(
										 MiscStrs::VENT_PROPOSED_SETUP_TAB_BUTTON_LABEL
										 );

				// Make sure the Previous Setup button is not shown. $[01090]
				previousSetupButton_.setShow(FALSE);
			}
			else if ( transitionId == BatchSettingsSubScreen::PROPOSED_TO_CURRENT )
			{											// $[TI3.3.2]
				// Title changes to current
				titleArea_.setTitle(MiscStrs::CURRENT_VENT_SETTINGS_TITLE);
				setVentSetupTabTitleText_(
										 MiscStrs::VENT_CURRENT_SETUP_TAB_BUTTON_LABEL
										 );

				// Show the Previous Setting button if that's valid	
				if ( SettingContextHandle::IsRecoverableBatchValid() )
				{										// $[TI3.3.2.1]
					previousSetupButton_.setShow(TRUE);
				}										// $[TI3.3.2.2]
			}											// $[TI3.3.3]
		}												// $[TI3.4]
	}													// $[TI4]
	else
	{
		// Screen 2 change!

		SettingPtr pSetting = SettingsMgr::GetSettingPtr(SettingId::APNEA_INTERVAL);
		const BoundedValue  APNEA_INTERVAL = pSetting->getAdjustedValue();

		apneaDisabledMsg_.setShow(APNEA_INTERVAL == DEFINED_UPPER_ALARM_LIMIT_OFF);

		// Some setting has been adjusted.  We may now be in the state that
		// at least one setting is changed (we must display a proposed screen
		// title) or no settings have been changed (if the current setting
		// was adjusted back to its original value in which case we display
		// a "current" screen title).  Don't bother changing screen titles
		// if we're in New Patient Setup mode.
		if ( !LowerScreen::RLowerScreen.isInPatientSetupMode() )
		{													// $[TI5]
			if ( transitionId == BatchSettingsSubScreen::CURRENT_TO_PROPOSED )
			{												// $[TI5.1]
				// Change tab button title and corresponding prompt.
				setVentSetupTabTitleText_( MiscStrs::VENT_PROPOSED_SETUP_TAB_BUTTON_LABEL );
				GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
											   PromptArea::PA_LOW, PromptStrs::VENT_SETUP_PROCEED_CANCEL_S);
			}
			else if ( transitionId == BatchSettingsSubScreen::PROPOSED_TO_CURRENT )
			{												// $[TI5.2]
				// tab button title changes to current, remove associated prompt
				setVentSetupTabTitleText_( MiscStrs::VENT_CURRENT_SETUP_TAB_BUTTON_LABEL );
				GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
											   PromptArea::PA_LOW, PromptStrs::VENT_SETUP_CANCEL_S);
			}												// $[TI5.3]
		}
		else
		{													// $[TI6]
			// Get rid of the NPS "all values above auto-set" prompt.
			GuiApp::PPromptArea->setPrompt(PromptArea::PA_ADVISORY,
										   PromptArea::PA_LOW, NULL_STRING_ID);
		}
	}
}
