#include "stdafx.h"

#if defined(SIGMA_BD_CPU)  ||  defined(SIGMA_COMMON)

//======================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//======================================================================

//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: PhasedInContext - Context of Phased-In Settings.
//---------------------------------------------------------------------
//@ Interface-Description
//  A context is a storage pool for different kinds of setting values.  The
//  settings, themselves, do not contain their values, the contexts do.  This
//  context is used for managing the Breath-Delivery's phased-in settings.
//  Only Breath-Delivery setting values are stored in this context, and the
//  methods provided by this context are for the phasing-in of those settings.
//  This context contains setting values that are currently applied to the
//  patient.
// 
//  This context provides methods for phasing-in the settings that are to
//  be applied to the patient.  This "phase-in" is initiated by the
//  Breath-Delivery Control Task via one of three methods:  one to phase-in
//  all of the Safety-PCV settings at once, and a second method for settings
//  phase-in during normal breathing modes.
//
//  This context, also, provides query methods to get access to all of the
//  currently phased-in setting values.
//
//  This class is available on the Breath-Delivery CPU, only.
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for the Phased-In Context.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Since the phasing-in of the setting values is done via a thread of
//  the Breath-Delivery Control Task, it is very important that the
//  phase-in process is done in optimal time.  It also means, that there
//  are no re-entrancy issues involved with this class -- the Breath-Delivery
//  Control Task is the highest application-oriented task on the BD CPU.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/PhasedInContext.ccv   25.0.4.0   19 Nov 2013 14:27:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 014  By: rhj    Date: 12-Feb-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//       PROX project-related changes.
// 
//  Revision: 013  By: rhj    Date: 10-Nov-2008    SCR Number: 6435
//  Project:  LEAK
//  Description:
//       Leak Compensation project-related changes.
//       
//  Revision: 012  By: gdc    Date: 18-Feb-2005    SCR Number: 6144
//  Project:  NIV1
//  Description:
//      Changes to support NIV.
//
//  Revision: 011 By: srp    Date: 28-May-2002   DR Number: 5898
//  Project:  VCP
//	Description:
//		Incorporated Remediation code review issue(s) -- commentary changes only.
//
//  Revision: 010  By: sah     Date:  05-May-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      *  added phase-in support for new volume support level setting
//
//  Revision: 009   By: sah    Date: 20-Jul-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//      NeoMode project-related changes:
//      *  obsoleted patient type setting
//
//  Revision: 008   By: sah    Date: 12-Mar-1999    DR Number: 5106
//  Project:  ATC
//  Description:
//	Changed trigger type to have a batch-independent phase-in rule.
//
//  Revision: 007   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new, BiLevel-specific settings to phase-in rules
//
//  Revision: 006   By: sah   Date:  04-Feb-1999    DR Number: 5322
//  Project:  ATC
//  Description:
//	Project-related changes:
//	*  added phase-in rules for the new ATC-specific settings.
//
//  Revision: 005   By: sah    Date: 29-Sep-1998    DR Number: 5106
//  Project:  BILEVEL
//  Description:
//	Changed flow sensitivity to be non-batch-dependent.
//
//  Revision: 004   By: dosman    Date: 08-Jan-1998    DR Number: 
//  Project:  BILEVEL
//  Description:
//	initial version for BiLevel.
//  
//  Revision: 003   By: sah    Date: 04 Mar 1997    DR Number: 1808
//  Project: Sigma (R8027)
//  Description:
//	Added missing requirements mapping to code.
//  
//  Revision: 002   By: sah    Date: 14 Jan 1997    DR Number: 1636
//  Project: Sigma (R8027)
//  Description:
//	Changed apnea interval's phase-in to immediate.  This fixes
//	problem where apnea is not started in time for SPONT.
//  
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//=====================================================================

#include "PhasedInContext.hh"

#if defined(SIGMA_DEVELOPMENT)
#include "Ostream.hh"
#endif // defined(SIGMA_DEVELOPMENT)

//@ Usage-Classes
#include "SafetyPcvSettingValues.hh"
#include "BitArray_NUM_BD_SETTING_IDS.hh"
#include "ContextMgr.hh"
#include "PendingContext.hh"
#include "NovRamManager.hh"
#include "Post.hh"
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  PhasedInContext()  [Default Constructor]
//
//@ Interface-Description
//  Construct a default phased-in context.  If the "BD-Settings-Initialized"
//  Flag within NOVRAM is set, this context will be initialized to the
//  phased-in values in NOVRAM.  Otherwise, this context will be initialized
//  the default values (see 'SafetyPcvSettingValues').
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

PhasedInContext::PhasedInContext(void)
{
  CALL_TRACE("PhasedInContext()");

  if (Post::GetStartupState() == ACSWITCH  ||
      (Post::GetStartupState()   == POWERFAIL  &&
       Post::GetDowntimeStatus() == DOWN_LONG))
  {   // $[TI1]
    // either this reset is occuring due to an A/C power switch cycle, or a
    // long power failure, therefore "clear" the Patient-Settings-Available
    // Flag...
    NovRamManager::UpdatePatientSettingsFlag(FALSE);
  }   // $[TI2] -- NOT an A/C power switch or long power failure....

  if (areBatchSettingsInitialized())
  {   // $[TI3] -- there are accepted setting values in NOVRAM...
    // update this context's local copy of the currently phased-in values
    // from the values in NOVRAM...
    NovRamManager::GetAcceptedBatchSettings(phasedInBdValues_);
  }
  else
  {   // $[TI4] -- there are NO accepted setting values in NOVRAM; phase-in
      //	   Safety-PCV values for the non-breathing values...
      //
      //	   NOTE:  these initial values are NOT intended for use at
      //	   this stage, this initialization is just a precautionary
      //           measure.
    phaseInSafetyPcvSettings();
  }

#if defined(SIGMA_DEVELOPMENT)
  // to allow print outs of the phase-ins during development...
  phasedInBdValues_.registerCallback(
				  &PhasedInContext::RegisterSettingPhaseIn_
				    );
#endif // defined(SIGMA_DEVELOPMENT)
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~PhasedInContext(void)  [Destructor]
//
//@ Interface-Description
//  Destroy this pending phase-in context.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

PhasedInContext::~PhasedInContext(void)
{
  CALL_TRACE("~PhasedInContext()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  phaseInSafetyPcvSettings()
//
//@ Interface-Description
//  Phase-in the Safety-PCV setting values into this context.  This
//  is called upon entrance to Safety-PCV mode, and will immediately
//  "phase-in" all of the Safety-PCV setting values.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Copy each of the bounded and discrete Safety-PCV values into this
//  context WITHOUT updating the values in NOVRAM.  Since Safety-PCV
//  is a transient state, it is not appropriate to copy the Safety-PCV
//  values into persistent storage.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
PhasedInContext::phaseInSafetyPcvSettings(void)
{
  CALL_TRACE("phaseInSafetyPcvSettings()");

  Uint  idx;

  // phase-in every single bounded Safety-PCV value...
  for (idx  = SettingId::LOW_BATCH_BD_BOUNDED_ID_VALUE;
       idx <= SettingId::HIGH_BATCH_BD_BOUNDED_ID_VALUE; idx++)
  {
    const SettingId::SettingIdType  SETTING_ID =
					(SettingId::SettingIdType)idx;

    // "phase-in" this bounded Safety-PCV value...
    phasedInBdValues_.setBoundedValue(SETTING_ID,
		 BoundedValue(SafetyPcvSettingValues::GetValue(SETTING_ID)));
  }

  // phase-in every single discrete Safety-PCV value...
  for (idx  = SettingId::LOW_BATCH_BD_DISCRETE_ID_VALUE;
       idx <= SettingId::HIGH_BATCH_BD_DISCRETE_ID_VALUE; idx++)
  {
    const SettingId::SettingIdType  SETTING_ID =
					(SettingId::SettingIdType)idx;

    // "phase-in" this discrete Safety-PCV value...
    phasedInBdValues_.setDiscreteValue(SETTING_ID,
	        DiscreteValue(SafetyPcvSettingValues::GetValue(SETTING_ID)));
  }
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  phaseInPendingSettings(phaseInState)
//
//@ Interface-Description
//  Phase-in any of the pending BD settings that are to be phased in
//  during the current breath phase given by 'phaseInState'.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method is executed as part of the BD Exec Task, which CANNOT be
//  blocked on any semaphore, so only "non-blocking" NOVRAM data items can
//  be accessed in this method.  Also, speed is of the essence.
//
//  $[02033] Phase-in rules for batch settings...
//  $[02281] Apnea flow acceleration percent setting phase-in rules...
//  $[02051] Apnea flow pattern setting phase-in rules...
//  $[02061] Apnea inspiratory pressure setting phase-in rules...
//  $[02065] Apnea inspiratory time setting phase-in rules...
//  $[02070] Apnea interval setting phase-in rules...
//  $[02073] Apnea mandatory type setting phase-in rules...
//  $[02263] Apnea minimum inspiratory flow setting phase-in rules...
//  $[02080] Apnea oxygen percentage setting phase-in rules...
//  $[02285] Apnea plateau time setting phase-in rules...
//  $[02086] Apnea peak inspiratory flow setting phase-in rules...
//  $[02093] Apnea respiratory rate setting phase-in rules...
//  $[02098] Apnea tidal volume setting phase-in rules...
//  $[02279] Disconnection sensitivity setting phase-in rules...
//  $[02114] Expiratory sensitivity setting phase-in rules...
//  $[02284] FiO2 enabled setting phase-in rules...
//  $[02124] Flow acceleration percent setting phase-in rules...
//  $[02127] Flow pattern setting phase-in rules...
//  $[02132] Flow sensitivity setting phase-in rules...
//  $[02137] High circuit pressure setting phase-in rules...
//  $[TC02041] High inspired tidal volume setting phase-in rules...
//  $[02156] Humidification type setting phase-in rules...
//  $[TC02005] Humidification volume setting phase-in rules...
//  $[02161] Ideal body weight setting phase-in rules...
//  $[02171] Inspiratory pressure setting phase-in rules...
//  $[02176] Inspiratory time setting phase-in rules...
//  $[02193] Mandatory type setting phase-in rules...
//  $[02264] Minimum inspiratory flow setting phase-in rules...
//  $[02203] Mode setting phase-in rules...
//  $[02212] Oxygen percentage setting phase-in rules...
//  $[02283] Nominal line voltage setting phase-in rules...
//  $[02282] Patient circuit type setting phase-in rules...
//  $[02221] Peak inspiratory flow setting phase-in rules...
//  $[TC02029] Percent support setting phase-in rules...
//  $[02226] Plateau time setting phase-in rules...
//  $[02232] Peep setting phase-in rules...
//  $[BL02011] Peep high setting phase-in rules...
//  $[TC02010] Peep high time setting phase-in rules...
//  $[TC02021] Peep low setting phase-in rules...
//  $[02237] Pressure sensitivity setting phase-in rules...
//  $[02241] Pressure support level setting phase-in rules...
//  $[02246] Respiratory rate setting phase-in rules...
//  $[02250] Support type setting phase-in rules...
//  $[02254] Tidal volume setting phase-in rules...
//  $[02259] Trigger type setting phase-in rules...
//  $[TC02034] Tube id setting phase-in rules...
//  $[TC02037] Tube type setting phase-in rules...
//  $[VC02004] Volume support level setting phase-in rules...
//  $[NI02009] Vent Type phase-in rules...
//  $[LC02012] Leak Comp enabled setting phase-in rules... 
//  $[PX02003] Prox enabled setting phase-in rules... 
//---------------------------------------------------------------------
//@ PreCondition
//  (phaseInState == PhaseInEvent::NON_BREATHING	 ||
//   phaseInState == PhaseInEvent::START_OF_INSPIRATION  ||
//   phaseInState == PhaseInEvent::DURING_INSPIRATION    ||
//   phaseInState == PhaseInEvent::START_OF_EXHALATION   ||
//   phaseInState == PhaseInEvent::DURING_EXHALATION)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
PhasedInContext::phaseInPendingSettings(
			  const PhaseInEvent::PhaseInState phaseInState
				       )
{
  CALL_TRACE("phaseInPendingSettings(phaseInState)");

  // get a non-constant reference to the Pending Context...
  PendingContext&  rPendingContext = ContextMgr::GetPendingContext();

  // get a constant reference to the bit array that represents the pending
  // flags of each of the settings...
  const FixedBitArray(NUM_BD_SETTING_IDS)&  rPendingFlags = 
					  rPendingContext.getPendingFlags();

  if (rPendingFlags.areAnySet())
  {   // $[TI1] -- there are settings that are currently pending...
    // this array contains all of the ids of the settings whose phase-in
    // may be restricted due to the pending of any other of these
    // settings ($[02033])...
    static const SettingId::SettingIdType  ARR_RESTRICTED_IDS_[] =
    {
      SettingId::FLOW_ACCEL_PERCENT,
      SettingId::FLOW_PATTERN,
      SettingId::HIGH_CCT_PRESS,
      SettingId::HIGH_INSP_TIDAL_VOL,
      SettingId::HUMID_TYPE,
      SettingId::HUMID_VOLUME,
      SettingId::IBW,
      SettingId::INSP_PRESS,
      SettingId::INSP_TIME,
      SettingId::MAND_TYPE,
      SettingId::MIN_INSP_FLOW,
      SettingId::MODE,
      SettingId::OXYGEN_PERCENT,
      SettingId::PEAK_INSP_FLOW,
      SettingId::PLATEAU_TIME,
      SettingId::PEEP,
      SettingId::PEEP_HIGH,
      SettingId::PEEP_HIGH_TIME,
      SettingId::PEEP_LOW,
      SettingId::PERCENT_SUPPORT,
      SettingId::PRESS_SUPP_LEVEL,
      SettingId::RESP_RATE,
      SettingId::SUPPORT_TYPE,
      SettingId::TIDAL_VOLUME,
      SettingId::TUBE_ID,
      SettingId::TUBE_TYPE,
      SettingId::VENT_TYPE,
      SettingId::VOLUME_SUPPORT,
      SettingId::APNEA_FLOW_PATTERN,
      SettingId::APNEA_INSP_PRESS,
      SettingId::APNEA_INSP_TIME,
      SettingId::APNEA_MAND_TYPE,
      SettingId::APNEA_MIN_INSP_FLOW,
      SettingId::APNEA_O2_PERCENT,
      SettingId::APNEA_PEAK_INSP_FLOW,
      SettingId::APNEA_RESP_RATE,
      SettingId::APNEA_TIDAL_VOLUME,
      SettingId::APNEA_FLOW_ACCEL_PERCENT,
      SettingId::APNEA_PLATEAU_TIME,
      SettingId::LEAK_COMP_ENABLED,
	  SettingId::PROX_ENABLED,
      SettingId::NULL_SETTING_ID
    };

    // this is used to point to an array of setting ids that are RESTRICTIVE
    // at this point in the breath ('phaseInState')...
    const SettingId::SettingIdType*  pRestrictiveByPhaseIds = NULL;

    switch (phaseInState)
    {
    case PhaseInEvent::NON_BREATHING :
    case PhaseInEvent::START_OF_INSPIRATION :		// $[TI1.1]
      {
	// NONE of the settings are restrictive at the start of
	// inspiration, or in "non-breathing" mode...
	static const SettingId::SettingIdType
					ARR_RESTRICTIVE_START_OF_INSP_[] =
	{
	  SettingId::NULL_SETTING_ID
	};

	pRestrictiveByPhaseIds = ARR_RESTRICTIVE_START_OF_INSP_;
      }
      break;

    case PhaseInEvent::DURING_INSPIRATION :		// $[TI1.2]
      {
	// this array contains all of the ids of the settings that are
	// NOT to be phased-in at this stage, and, therefore, will
	// restrict ALL of the RESTRICTED settings from phasing-in...
	static const SettingId::SettingIdType
					  ARR_RESTRICTIVE_DURING_INSP_[] =
	{
	  SettingId::FLOW_ACCEL_PERCENT,
	  SettingId::FLOW_PATTERN,
	  SettingId::HUMID_TYPE,
	  SettingId::HUMID_VOLUME,
	  SettingId::INSP_PRESS,
	  SettingId::INSP_TIME,
	  SettingId::MAND_TYPE,
	  SettingId::MIN_INSP_FLOW,
	  SettingId::MODE,
	  SettingId::OXYGEN_PERCENT,
	  SettingId::PEAK_INSP_FLOW,
	  SettingId::PLATEAU_TIME,
	  SettingId::PEEP,
	  SettingId::PEEP_HIGH,
	  SettingId::PEEP_HIGH_TIME,
	  SettingId::PEEP_LOW,
	  SettingId::PERCENT_SUPPORT,
	  SettingId::PRESS_SUPP_LEVEL,
	  SettingId::RESP_RATE,
	  SettingId::SUPPORT_TYPE,
	  SettingId::TIDAL_VOLUME,
	  SettingId::TUBE_ID,
	  SettingId::TUBE_TYPE,
	  SettingId::VENT_TYPE,
	  SettingId::VOLUME_SUPPORT,
	  SettingId::APNEA_FLOW_PATTERN,
	  SettingId::APNEA_INSP_PRESS,
	  SettingId::APNEA_INSP_TIME,
	  SettingId::APNEA_MAND_TYPE,
	  SettingId::APNEA_MIN_INSP_FLOW,
	  SettingId::APNEA_O2_PERCENT,
	  SettingId::APNEA_PEAK_INSP_FLOW,
	  SettingId::APNEA_RESP_RATE,
	  SettingId::APNEA_TIDAL_VOLUME,
	  SettingId::APNEA_FLOW_ACCEL_PERCENT,
	  SettingId::APNEA_PLATEAU_TIME,
	  SettingId::IBW,
	  SettingId::LEAK_COMP_ENABLED,
	  SettingId::PROX_ENABLED,
	  SettingId::NULL_SETTING_ID
	};

	pRestrictiveByPhaseIds = ARR_RESTRICTIVE_DURING_INSP_;
      }
      break;

    case PhaseInEvent::START_OF_EXHALATION :		// $[TI1.3]
      {
	// this array contains all of the ids of the settings that are
	// NOT to be phased-in at this stage, and, therefore, will
	// restrict ALL of the RESTRICTED settings from phasing-in...
	static const SettingId::SettingIdType
					  ARR_RESTRICTIVE_START_OF_EXP_[] =
	{
	  SettingId::HUMID_TYPE,
	  SettingId::HUMID_VOLUME,
	  SettingId::IBW,
	  SettingId::INSP_TIME,
	  SettingId::MODE,
	  SettingId::PEEP_HIGH_TIME,
	  SettingId::RESP_RATE,
	  SettingId::TUBE_ID,
	  SettingId::TUBE_TYPE,
	  SettingId::VENT_TYPE,
	  SettingId::APNEA_INSP_TIME,
	  SettingId::APNEA_RESP_RATE,
	  SettingId::LEAK_COMP_ENABLED,
  	  SettingId::PROX_ENABLED,		
	  SettingId::NULL_SETTING_ID
	};

	pRestrictiveByPhaseIds = ARR_RESTRICTIVE_START_OF_EXP_;
      }
      break;

    case PhaseInEvent::DURING_EXHALATION :		// $[TI1.4]
      {
	// this array contains all of the ids of the settings that are
	// NOT to be phased-in at this stage, and, therefore, will
	// restrict ALL of the RESTRICTED settings from phasing-in...
	static const SettingId::SettingIdType
					  ARR_RESTRICTIVE_DURING_EXP_[] =
	{
	  SettingId::FLOW_SENS,
	  SettingId::HUMID_TYPE,
	  SettingId::HUMID_VOLUME,
	  SettingId::IBW,
	  SettingId::INSP_TIME,
	  SettingId::MODE,
	  SettingId::OXYGEN_PERCENT,
	  SettingId::PEEP,
	  SettingId::PEEP_HIGH,
	  SettingId::PEEP_HIGH_TIME,
	  SettingId::PEEP_LOW,
	  SettingId::PERCENT_SUPPORT,
	  SettingId::RESP_RATE,
	  SettingId::TRIGGER_TYPE,
	  SettingId::TUBE_ID,
	  SettingId::TUBE_TYPE,
	  SettingId::VENT_TYPE,
	  SettingId::APNEA_INSP_TIME,
	  SettingId::APNEA_O2_PERCENT,
	  SettingId::APNEA_RESP_RATE,
	  SettingId::LEAK_COMP_ENABLED,
	  SettingId::PROX_ENABLED,
	  SettingId::NULL_SETTING_ID
	};

	pRestrictiveByPhaseIds = ARR_RESTRICTIVE_DURING_EXP_;
      }
      break;

    default :
      // illegal breath phase...
      AUX_CLASS_ASSERTION_FAILURE(phaseInState);
      break;
    };

    // initialize a bit array of the "phase-in" flags to have the flags of
    // all of the currently pending settings turned on -- each "phase-in"
    // flag indicates whether the corresponding setting is to be phased-in
    // at this time...
    FixedBitArray(NUM_BD_SETTING_IDS)  phaseInFlags = rPendingFlags;

    for (Uint idx = 0u;
	 pRestrictiveByPhaseIds[idx] != SettingId::NULL_SETTING_ID; idx++)
    {   // $[TI1.5] -- at least one iteration...
      if (rPendingFlags.isBitSet(pRestrictiveByPhaseIds[idx]))
      {   // $[TI1.5.1]
	// a RESTRICTIVE setting is currently pending, therefore NONE of
	// the RESTRICTED settings can be phased-in at this time...
	//
	// (NOTE:  re-using 'idx'...)
	for (idx = 0u;
	     ARR_RESTRICTED_IDS_[idx] != SettingId::NULL_SETTING_ID; idx++)
	{
	  // clear the "phase-in" bit of all of the RESTRICTED settings...
	  phaseInFlags.clearBit(ARR_RESTRICTED_IDS_[idx]);
	}

	// ensure that ALL of the settings restricted by the current phase are
	// also not allowed to phase-in; this allows for phase-restricted
	// settings (found in 'pRestrictiveByPhaseIds') that are NOT
	// batch-dependent (not found in 'ARR_RESTRICTED_IDS_', e.g., flow
	// sensitivity), be easily handled with the phase-in mechanism (DCS
	// #5106)...
	// (NOTE:  re-using 'idx'...)
	for (idx = 0u;
	     pRestrictiveByPhaseIds[idx] != SettingId::NULL_SETTING_ID; idx++)
	{
	  // clear the "phase-in" bit of all of the RESTRICTIVE settings...
	  phaseInFlags.clearBit(pRestrictiveByPhaseIds[idx]);
	}

	break;   // break out of this loop...
      }   // $[TI1.5.2]
    }   // $[TI1.6] -- no iterations ('START_OF_INSPIRATION')...

    if (phaseInFlags.areAnySet())
    {   // $[TI1.7]
      // there ARE settings that need to be phased-in, at this time...
      performPhaseIn_(phaseInFlags);
    }   // $[TI1.8] -- there are NO settings to phase-in at this time...

    if (!rPendingFlags.areAnySet())
    {   // $[TI1.9] -- all pending settings have been phased-in...
      // make sure the Vent-Startup-Pending-Flag is NOT set, anymore...
      rPendingContext.ventStartupPhaseInComplete();
    }   // $[TI1.10] -- still more pending settings...
  }   // $[TI2] -- there are NO pending settings...
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  arePatientSettingsAvailable()  [const]
//
//@ Interface-Description
//  Are there phased-in patient setting values available?
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean
PhasedInContext::arePatientSettingsAvailable(void) const
{
  CALL_TRACE("arePatientSettingsAvailable()");

  return(NovRamManager::ArePatientSettingsAvailable());
}   // $[TI1] (TRUE)  $[TI2] (FALSE)


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  areBatchSettingsInitialized()  [const]
//
//@ Interface-Description
//  Are there ANY batch setting values available?
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean
PhasedInContext::areBatchSettingsInitialized(void) const
{
  CALL_TRACE("areBatchSettingsInitialized()");

  return(NovRamManager::AreBatchSettingsInitialized());
}   // $[TI1] (TRUE)  $[TI2] (FALSE)


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  setBoundedValue(boundedId, boundedValue)  [const]
//
// Interface-Description
//  Set the phased-in setting identified by 'boundedId' to the bounded value
//  given by 'boundedValue'.
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//  (SettingId::IsBdBoundedId(boundedId))
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

void
PhasedInContext::setBoundedValue(const SettingId::SettingIdType boundedId,
				 const BoundedValue&            boundedValue)
{
  CALL_TRACE("setBoundedValue(boundedId, boundedValue)");
  SAFE_CLASS_PRE_CONDITION((SettingId::IsBdBoundedId(boundedId)));

  phasedInBdValues_.setBoundedValue(boundedId, boundedValue);
}


//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  setDiscreteValue(discreteId, discreteValue)  [const]
//
// Interface-Description
//  Set the phased-in setting identified by 'discreteId' to the discrete
//  value given by 'discreteValue'.
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//  (SettingId::IsBdDiscreteId(discreteId))
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

void
PhasedInContext::setDiscreteValue(const SettingId::SettingIdType discreteId,
				  DiscreteValue                  discreteValue)
{
  CALL_TRACE("setDiscreteValue(discreteId, discreteValue)");
  SAFE_CLASS_PRE_CONDITION((SettingId::IsBdDiscreteId(discreteId)));

  phasedInBdValues_.setDiscreteValue(discreteId, discreteValue);
}

#endif // defined(SIGMA_DEVELOPMENT)


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
PhasedInContext::SoftFault(const SoftFaultID  softFaultID,
			   const Uint32       lineNumber,
			   const char*        pFileName,
			   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
                          PHASED_IN_CONTEXT, lineNumber, pFileName,
                          pPredicate);
}


//=====================================================================
//
//  Private Methods...
//
//=====================================================================

#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  RegisterSettingPhaseIn_()  [static]
//
// Interface-Description
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

void
PhasedInContext::RegisterSettingPhaseIn_(const SettingId::SettingIdType settingId)
{
  CALL_TRACE("RegisterSettingPhaseIn_(settingId)");

  if (Settings_Validation::IsDebugOn(::PHASED_IN_CONTEXT))
  {
    const PhasedInContext&  rPhasedInContext =
				      ContextMgr::GetPhasedInContext();
 
    cout << "Changed PHASED-IN Setting:" << endl;
    cout << "  Name:   " << SettingId::GetSettingName(settingId)
	 << endl;
    cout << "  Value:  ";

    if (SettingId::IsBoundedId(settingId))
    {
      const BoundedValue&  rBoundedValue =
				rPhasedInContext.getBoundedValue(settingId);

      cout << rBoundedValue;
    }
    else if (SettingId::IsDiscreteId(settingId))
    {
      cout << rPhasedInContext.getDiscreteValue(settingId);
    }

    cout << endl;
  }
}

#endif // defined(SIGMA_DEVELOPMENT)


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  performPhaseIn_(rPhaseInFlags)
//
//@ Interface-Description
//  Phase-in all of the pending settings that correspond to "set" bits
//  in 'rPhaseInFlags'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (rPhaseInFlags.areAnySet())
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
PhasedInContext::performPhaseIn_(
			  FixedBitArray(NUM_BD_SETTING_IDS)& rPhaseInFlags
				)
{
  CALL_TRACE("performPhaseIn_(rPhaseInFlags)");
  SAFE_CLASS_PRE_CONDITION((rPhaseInFlags.areAnySet()));

  PendingContext&        rPendingContext = ContextMgr::GetPendingContext();
  const BdSettingValues& rPendingValues  = rPendingContext.getPendingValues();

  Uint  idx;

  if (rPhaseInFlags.areAnyClear())
  {   // $[TI1]
    // a phase-in of the entire set of settings is NOT requested, therefore
    // phase-in only those BOUNDED settings whose bit is set in
    // 'rPhaseInFlags'...
    for (idx  = SettingId::LOW_BATCH_BD_BOUNDED_ID_VALUE;
	 idx <= SettingId::HIGH_BATCH_BD_BOUNDED_ID_VALUE; idx++)
    {   // $[TI1.1] -- this branch ALWAYS executes...
      const SettingId::SettingIdType  SETTING_ID =
					    (SettingId::SettingIdType)idx;

      if (rPhaseInFlags.isBitSet(SETTING_ID))
      {   // $[TI1.1.1]
	// copy the value indicated by 'SETTING_ID', from the pending context
	// into the "new" BD setting value buffer...
	phasedInBdValues_.setBoundedValue(SETTING_ID,
				 rPendingValues.getBoundedValue(SETTING_ID));

	// clear the bit indicated by 'SETTING_ID'...
	rPhaseInFlags.clearBit(SETTING_ID);

	// clear the Pending Context's bit that indicates that the setting
	// identified by 'SETTING_ID' is pending a phase-in...
	rPendingContext.clearPendingFlag(SETTING_ID);

	if (!rPhaseInFlags.areAnySet())
	{   // $[TI1.1.1.1]
	  // there are no more bits that are "set", therefore break out of
	  // this loop...
	  break;
	}   // $[TI1.1.1.2] -- there are more settings to phase in...
      }   // $[TI1.1.2] -- the setting identified by 'SETTING_ID' is NOT to
	  //		   be phased-in...
    }  // end of for...

    if (rPhaseInFlags.areAnySet())
    {   // $[TI1.2]
      // there are more bits that are "set", therefore phase-in only those
      // DISCRETE settings whose "bit" is set in 'rPhaseInFlags'...
      for (idx = SettingId::LOW_BATCH_BD_DISCRETE_ID_VALUE;
	   idx <= SettingId::HIGH_BATCH_BD_DISCRETE_ID_VALUE; idx++)
      {   // $[TI1.2.1] -- this path ALWAYS executes...
	const SettingId::SettingIdType  SETTING_ID =
					  (SettingId::SettingIdType)idx;

	if (rPhaseInFlags.isBitSet(SETTING_ID))
	{   // $[TI1.2.1.1]
	  // "phase-in" this discrete setting...
	  phasedInBdValues_.setDiscreteValue(SETTING_ID,
			       rPendingValues.getDiscreteValue(SETTING_ID));

	  // clear the bit indicated by 'SETTING_ID'...
	  rPhaseInFlags.clearBit(SETTING_ID);

	  // clear the Pending Context's bit that indicates that the setting
	  // identified by 'SETTING_ID' is pending a phase-in...
	  rPendingContext.clearPendingFlag(SETTING_ID);

	  if (!rPhaseInFlags.areAnySet())
	  {   // $[TI1.2.1.1.1]
	    // there are no more bits that are "set", therefore break out of
	    // this loop...
	    break;
	  }   // $[TI1.2.1.1.2] -- there are more settings to phase-in...
	}   // $[TI1.2.1.2] -- the setting indicated by 'SETTING_ID' is
	    //		       NOT to be phased-in...
      }  // end of for...
    }   // $[TI1.2.2] -- there are NO discrete settings to be phased-in...
  }
  else
  {   // $[TI2]
    // ALL of the bits in 'rPhaseInFlags' are set, therefore phase-in every
    // single bounded setting value...
    for (idx = SettingId::LOW_BATCH_BD_BOUNDED_ID_VALUE;
	 idx <= SettingId::HIGH_BATCH_BD_BOUNDED_ID_VALUE; idx++)
    {
      const SettingId::SettingIdType  SETTING_ID =
					(SettingId::SettingIdType)idx;

      // "phase-in" this bounded setting...
      phasedInBdValues_.setBoundedValue(SETTING_ID,
				 rPendingValues.getBoundedValue(SETTING_ID));

      // clear the Pending Context's bit that indicates that the setting
      // identified by 'SETTING_ID' is pending a phase-in...
      rPendingContext.clearPendingFlag(SETTING_ID);
    }

    // ...and phase-in every single discrete setting value...
    for (idx = SettingId::LOW_BATCH_BD_DISCRETE_ID_VALUE;
	 idx <= SettingId::HIGH_BATCH_BD_DISCRETE_ID_VALUE; idx++)
    {
      const SettingId::SettingIdType  SETTING_ID =
					    (SettingId::SettingIdType)idx;

      // "phase-in" this discrete setting...
      phasedInBdValues_.setDiscreteValue(SETTING_ID,
			        rPendingValues.getDiscreteValue(SETTING_ID));

      // clear the Pending Context's bit that indicates that the setting
      // identified by 'SETTING_ID' is pending a phase-in...
      rPendingContext.clearPendingFlag(SETTING_ID);
    }
  }   // end of else...
}


#endif // defined(SIGMA_BD_CPU)  ||  defined(SIGMA_COMMON)
