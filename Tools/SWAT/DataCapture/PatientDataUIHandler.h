//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file PatientDataUIHandler.h
//----------------------------------------------------------------------------

#ifndef PATIENTDATAUIHANDLER_H
#define PATIENTDATAUIHANDLER_H


//-------------------------------------
// I N C L U D E S
//-------------------------------------
#include "IPatientDataSet.h"
#include "IEventLogger.h"

//-------------------------------------
// C L A S S  D E C L
//-------------------------------------



class PatientDataUIHandler{
    public:

    static bool register_types(IEventLogger *pIEventLogger, void *pData );

    private:

    
    /// @brief not used
    PatientDataUIHandler();


    /// @brief not used
    ~PatientDataUIHandler();
};

#endif
