#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//================= C L A S S   D E S C R I P T I O N =================
//@ Class:  PeepSetting - Positive End-Expiratory Pressure Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, bounded setting that contains the positive pressure
//  applied to the patient's circuit during exhalation.  This class
//  inherits from 'BoundedSetting' and provides the specific information
//  needed for representation of PEEP.  This information includes the
//  interval and range of this setting's values, and this batch setting's
//  new-patient value (see 'getNewPatientValue()').
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class has NO dependent settings, or dynamic bounds.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/PeepSetting.ccv   25.0.4.0   19 Nov 2013 14:27:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 014   By: gdc    Date: 24-Sep-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal non-invasive CPAP.
//
//  Revision: 013  By: gdc    Date: 22-Feb-2005    SCR Number: 6148
//  Project:  NIV1
//  Description:
//      Added the dependent setting requirement number for low circ pressure.  
//
//  Revision: 012  By: gdc    Date: 18-Feb-2005    SCR Number: 6144
//  Project:  NIV1
//  Description:
//      Changes to support NIV.
//   
//  Revision: 013  By: sah    Date:  22-Aug-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//	VTPC project-related changes:
//      *  eliminated unneeded include
//
//  Revision: 012   By: sah    Date: 05-May-2000    DR Number: 5718
//  Project:  NeoMode
//  Description:
//      Modified 'findSoftMinMaxValue_()' method to only return soft bound
//      values, if NOT in either New Patient or Vent Setup modes.
//
//  Revision: 011   By: sah   Date:  27-Apr-2000    DR Number: 5713
//  Project:  NeoMode
//  Description:
//      Added NEONATAL-only, soft bounds, based on the currently-accepted
//      value.
//
//  Revision: 010   By: sah    Date: 29-Jun-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  modified to use newly-modified 'BoundedRange' class to handle
//         range, via linked list of interval nodes
//
//  Revision: 009   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'getApplicability()' method
//	*  added new 'acceptTransition()' method
//	*  removed unnecessary 'isAcceptedValid()' method
//
//  Revision: 008   By: dosman    Date: 07-May-1998    DR Number: 
//  Project:  BILEVEL
//  Description:
//	add & fix TIs
//
//  Revision: 007   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 006   By: dosman    Date: 24-Dec-1997    DR Number: 
//  Project:  BILEVEL
//  Description:
//	Initial BiLevel version.
//	added symbolic constants
//	added BILEVEL_MODE_VALUE
//	made sure that PI does not affect constraints on PEEP during BILEVEL
//	changed updateConstraints_() to include constraint
//	on PEEP from PEEP_HI during BiLevel
//
//  Revision: 005   By: sah    Date:  28-May-1998    DR Number: 5058
//  Project:  COLOR
//  Description:
//	--- Merged Rev "Color" (1.22.1.0) into Rev "BiLevel" (1.22.2.0)
//	Modified range/resolution for improving the user's ability to
//	accurately change this setting's value.
//
//  Revision: 004   By: sah    Date: 10-Feb-1997    DR Number: 1747
//  Project: Sigma (R8027)
//  Description:
//	Added new maximum limit of (90 - Psupp).
//
//  Revision: 003   By: sah    Date: 11-Dec-1996    DR Number: 1623
//  Project: Sigma (R8027)
//  Description:
//	Changed maximum limit from "(Pcirc - 6)" to "(Pcirc - 7)".
//
//  Revision: 002   By: sah    Date:  08-Nov-1996    DR Number: 1562
//  Project: Sigma (R8027)
//  Description:
//	Fixed "warping" in 'updateConstraints_()' to be UP/DOWN, instead
//	of NEAREST; NEAREST causes problems due to incompatible resolutions.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//=====================================================================

#include "PeepSetting.hh"
#include "MandTypeValue.hh"
#include "ModeValue.hh"
#include "SettingConstants.hh"
#include "PatientCctTypeValue.hh"

//@ Usage-Classes
#include "SettingsMgr.hh"
#include "ContextMgr.hh"
#include "AdjustedContext.hh"
//@ End-Usage


//@ Code...

//====================================================================
//
//  Static Data Definitions...
//
//====================================================================

// $[NI02012] -- this setting's dependent settings...
static const SettingId::SettingIdType  ARR_DEPENDENT_SETTING_IDS_[] =
  {
    SettingId::LOW_CCT_PRESS,
    SettingId::NULL_SETTING_ID
  };

//  $[02228] -- The setting's range ...
//  $[02231] -- The setting's resolution ...
static const BoundedInterval  LAST_NODE_ =
  {
    0.0f,		// absolute minimum...
    0.0f,		// unused...
    TENTHS,		// unused...
    NULL
  };
static const BoundedInterval  NODE1_ =
  {
    20.0f,		// change-point...
    0.5f,		// resolution...
    TENTHS,		// precision...
    &::LAST_NODE_
  };
static const BoundedInterval  INTERVAL_LIST_ =
  {
    45.0f,		// absolute maximum...
    1.0f,		// resolution...
    ONES,		// precision...
    &::NODE1_
  };


//====================================================================
//
//  Public Methods...
//
//====================================================================

//============= M E T H O D   D E S C R I P T I O N ==================
//@ Method: PeepSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This setting inherits from BoundedSetting.  The method passes to
//  BoundedSetting it's default information. Also, this method initializes
//  the value interval.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

PeepSetting::PeepSetting(void)
	: BatchBoundedSetting(SettingId::PEEP,
			      ::ARR_DEPENDENT_SETTING_IDS_,
				  &::INTERVAL_LIST_,
				  PEEP_MAX_ID,		// maxConstraintId...
				  PEEP_MIN_ID)	// minConstraintId...
{
  CALL_TRACE("PeepSetting()");

  // register this setting's soft bounds...
  registerSoftBound_(::PEEP_SOFT_MIN_BASED_ACCEPTED_ID, Setting::LOWER);
  registerSoftBound_(::PEEP_SOFT_MAX_BASED_ACCEPTED_ID, Setting::UPPER);
}   // $[TI1]


//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  ~PeepSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

PeepSetting::~PeepSetting(void)
{
  CALL_TRACE("~PeepSetting()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability(qualifierId)  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[01071] -- main settings applicability
//  $[01081] -- breath settings applicability
//---------------------------------------------------------------------
//@ PreCondition
//  (qualifierId == Notification::ACCEPTED  ||
//   qualifierId == Notification::ADJUSTED)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
PeepSetting::getApplicability(const Notification::ChangeQualifier qualifierId) const
{
  CALL_TRACE("getApplicability(qualifierId)");

  const Setting*  pMode = SettingsMgr::GetSettingPtr(SettingId::MODE);

  DiscreteValue  modeValue;
  
  switch (qualifierId)
  {
  case Notification::ACCEPTED :		// $[TI1]
    modeValue = pMode->getAcceptedValue();
    break;
  case Notification::ADJUSTED :		// $[TI2]
    modeValue = pMode->getAdjustedValue();
    break;
  default :
    AUX_CLASS_ASSERTION_FAILURE(qualifierId);
    break;
  }

  return((modeValue == ModeValue::AC_MODE_VALUE  ||
	  modeValue == ModeValue::SIMV_MODE_VALUE  ||
	  modeValue == ModeValue::CPAP_MODE_VALUE  ||
	  modeValue == ModeValue::SPONT_MODE_VALUE)
	   ? Applicability::CHANGEABLE		// $[TI3]
	   : Applicability::INAPPLICABLE);	// $[TI4]
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [const, virtual]
//
//@ Interface-Description
//  Get the setting's new-patient value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns a constant for the new patient value.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
PeepSetting::getNewPatientValue(void) const
{
  CALL_TRACE("getNewPatientValue()");

  BoundedValue  newPatient;
  
  // $[02229] The setting's new-patient value ...
  newPatient.value     = SettingConstants::NP_PEEP_VALUE;
  newPatient.precision = TENTHS;

  return(newPatient);  // $[TI1]
}


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method: acceptTransition(settingId, newValue, currValue) [virtual]
//
//@ Interface-Description
//  This method is responsible for updating this setting based on the
//  the Main Control Setting identified by 'settingId' and the transition
//  of its value from 'currValue' to 'newValue'.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[BL02000]\b\ -- transition from BILEVEL to SPONT...
//  $[BL02000]\e\ -- transition from BILEVEL to non-SPONT...
//---------------------------------------------------------------------
//@ PreCondition
//  ((settingId == SettingId::MODE)           &&
//   ((newValue == ModeValue::SIMV_MODE_VALUE)  ||
//    (newValue == ModeValue::AC_MODE_VALUE)  ||
//    (newValue == ModeValue::CPAP_MODE_VALUE)  ||
//    (newValue == ModeValue::SPONT_MODE_VALUE))  &&
//   (currValue == ModeValue::BILEVEL_MODE_VALUE))
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================

void
PeepSetting::acceptTransition(const SettingId::SettingIdType settingId,
			      const DiscreteValue            newValue,
			      const DiscreteValue	     currValue)
{
  CALL_TRACE("acceptTransition(settingId, newValue, currValue)");

  AUX_CLASS_PRE_CONDITION((settingId == SettingId::MODE), settingId);
  AUX_CLASS_PRE_CONDITION(((newValue == ModeValue::SIMV_MODE_VALUE)  ||
			   (newValue == ModeValue::SPONT_MODE_VALUE) ||
			   (newValue == ModeValue::CPAP_MODE_VALUE) ||
			   (newValue == ModeValue::AC_MODE_VALUE) ),
			  newValue);
  AUX_CLASS_PRE_CONDITION((currValue == ModeValue::BILEVEL_MODE_VALUE),
			  currValue);

  const Setting*  pPeepLow = SettingsMgr::GetSettingPtr(SettingId::PEEP_LOW);

  BoundedValue  newPeep;

  newPeep = pPeepLow->getAdjustedValue();

  // store the value...
  setAdjustedValue(newPeep);

  // activation of Transition Rule MUST be italicized...
  setForcedChangeFlag();
}   // $[TI1]


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//	      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
PeepSetting::SoftFault(const SoftFaultID  softFaultID,
		       const Uint32       lineNumber,
		       const char*        pFileName,
		       const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
			  PEEP_SETTING, lineNumber, pFileName,
			  pPredicate);
}


//====================================================================
//
//  Protected Methods...
//
//====================================================================

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  updateConstraints_()  [virtual]
//
//@ Interface-Description
//  Update the dynamic bounds of this setting.  This method sets this
//  setting's maximum constraint to the most restrictive of this setting's
//  upper bounds, and, correspondingly, set this setting's minimum
//  constraint to its most restrictive lower bound.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Determine the minimum upper bound, and set the upper limit to it.
//
//  $[02228] -- PEEP's range
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
PeepSetting::updateConstraints_(void)
{
  CALL_TRACE("updateConstraints_()");

  Real32  softMinValue, softMaxValue;

  // get the soft-bound limits...
  findSoftMinMaxValues_(softMinValue, softMaxValue);

  //-------------------------------------------------------------------
  // determine PEEP's maximum constraint value...
  //-------------------------------------------------------------------
  {
    BoundedRange::ConstraintInfo  maxConstraintInfo;

    BoundedValue  maxLimit;

    // reset this range's maximum constraint to allow for "warping" of a
    // new maximum value...
    getBoundedRange_().resetMaxConstraint();

    //-----------------------------------------------------------------
    // initialize to PEEP's absolute maximum...
    //-----------------------------------------------------------------

    // start off with the absolute maximum bound value and ID...
    maxConstraintInfo.id    = PEEP_MAX_ID;
    maxConstraintInfo.value = getAbsoluteMaxValue_();


    //===================================================================
    // calculate PEEP's maximum bound that is based on high circuit
    // pressure...
    //===================================================================

    const Setting*  pHighCctPress =
			  SettingsMgr::GetSettingPtr(SettingId::HIGH_CCT_PRESS);

    const Real32  HIGH_CCT_PRESS_VALUE =
			  BoundedValue(pHighCctPress->getAdjustedValue()).value;

    maxLimit.value = (HIGH_CCT_PRESS_VALUE -
		      SettingConstants::MIN_NON_MAND_PRESS_BUFF);

    if (maxLimit.value < maxConstraintInfo.value)
    {   // $[TI1]
      // warp the calculated maximum value to a lower "click" boundary...
      getBoundedRange_().warpValue(maxLimit, BoundedRange::WARP_DOWN);

      // the high circuit pressure-based maximum is more restrictive than
      // 'maxConstraintInfo.value', therefore save its value and id as the maximum
      // bound value and id...
      maxConstraintInfo.id    = PEEP_MAX_BASED_PCIRC_ID;
      maxConstraintInfo.value = maxLimit.value;
    }  // $[TI2] -- high cct. pressure's absolute max is more restrictive...


    const Setting*  pInspPress =
			      SettingsMgr::GetSettingPtr(SettingId::INSP_PRESS);

    if (pInspPress->getApplicability(Notification::ADJUSTED) !=
						  Applicability::INAPPLICABLE)
    {   // $[TI3]
      const Real32  INSP_PRESS_VALUE =
			  BoundedValue(pInspPress->getAdjustedValue()).value;

      //===================================================================
      // calculate PEEP's maximum bound that is based on high circuit
      // pressure and inspiratory pressure...
      //===================================================================

      // calculate the first inspiratory pressure-based limit...
      maxLimit.value = (HIGH_CCT_PRESS_VALUE - INSP_PRESS_VALUE -
			SettingConstants::MIN_MAND_PRESS_BUFF);

      if (maxLimit.value < maxConstraintInfo.value)
      {   // $[TI3.1]
	// warp the calculated maximum value to a lower "click" boundary...
	getBoundedRange_().warpValue(maxLimit, BoundedRange::WARP_DOWN);

	// the first high circuit pressure-based maximum is more restrictive
	// than 'maxConstraintInfo.value', therefore save its value and ID as the
	// maximum bound value and ID...
	maxConstraintInfo.id    = PEEP_MAX_BASED_PCIRC_PI_ID;
	maxConstraintInfo.value = maxLimit.value;
      }   // $[TI3.2] -- 'maxConstraintInfo.value' is more restrictive...

      //===================================================================
      // calculate PEEP's maximum bound that is based on inspiratory pressure...
      //===================================================================

      // calculate the second inspiratory pressure-based limit...
      maxLimit.value = (SettingConstants::MAX_ALLOWED_PRESSURE_SETTING_VALUE -
			  INSP_PRESS_VALUE);

      if (maxLimit.value < maxConstraintInfo.value)
      {   // $[TI3.3]
	// warp the calculated maximum value to a lower "click" boundary...
	getBoundedRange_().warpValue(maxLimit, BoundedRange::WARP_DOWN);

	// the second inspiratory pressure-based maximum is more restrictive
	// than 'maxConstraintInfo.value', therefore save its value and ID as the
	// maximum bound value and ID...
	maxConstraintInfo.id    = PEEP_MAX_BASED_PI_ID;
	maxConstraintInfo.value = maxLimit.value;
      }   // $[TI3.4] -- 'maxConstraintInfo.value' is more restrictive...
    }   // $[TI4] -- inspiratory pressure is not applicable...


    const Setting*  pPressSupp =
		     SettingsMgr::GetSettingPtr(SettingId::PRESS_SUPP_LEVEL);

    if (pPressSupp->getApplicability(Notification::ADJUSTED) !=
						  Applicability::INAPPLICABLE)
    {   // $[TI5]
      const Real32  PRESS_SUPP_VALUE =
			  BoundedValue(pPressSupp->getAdjustedValue()).value;

      //===================================================================
      // calculate PEEP's maximum bound that is based on high circuit pressure
      // and pressure support...
      //===================================================================

      maxLimit.value = (HIGH_CCT_PRESS_VALUE - PRESS_SUPP_VALUE -
			SettingConstants::MIN_MAND_PRESS_BUFF);

      if (maxLimit.value < maxConstraintInfo.value)
      {   // $[TI5.1]
	// warp the calculated maximum values to a lower "click" boundary...
	getBoundedRange_().warpValue(maxLimit, BoundedRange::WARP_DOWN);

	// the high circuit pressure-based maximum is more restrictive than
	// 'maxConstraintInfo.value', therefore save its value and id as the maximum
	// bound value and id...
	maxConstraintInfo.id    = PEEP_MAX_BASED_PCIRC_PSUPP_ID;
	maxConstraintInfo.value = maxLimit.value;
      }   // $[TI5.2] -- 'maxConstraintInfo.value' is more restrictive...


      //===================================================================
      // calculate PEEP's maximum bound that is based on pressure support...
      //===================================================================

      maxLimit.value = (SettingConstants::MAX_ALLOWED_PRESSURE_SETTING_VALUE -
			  PRESS_SUPP_VALUE);

      if (maxLimit.value < maxConstraintInfo.value)
      {   // $[TI5.3]
	// warp the calculated maximum values to a lower "click" boundary...
	getBoundedRange_().warpValue(maxLimit, BoundedRange::WARP_DOWN);

	// the pressure support level-based maximum is more restrictive than
	// 'maxConstraintInfo.value', therefore save its value and id as the
	// maximum bound value and id...
	maxConstraintInfo.id    = PEEP_MAX_BASED_PSUPP_ID;
	maxConstraintInfo.value = maxLimit.value;
      }   // $[TI5.4] -- 'maxConstraintInfo.value' is more restrictive...
    }   // $[TI6] -- pressure support level is not applicable...

    //-------------------------------------------------------------------
    // now that the most restrictive hard bound has been determined, compare
    // to soft-bound limit...
    //-------------------------------------------------------------------

    if (isSoftBoundActive_(PEEP_SOFT_MAX_BASED_ACCEPTED_ID)  &&
        softMaxValue < maxConstraintInfo.value)
    {  // $[TI7] -- soft max is more restrictive...
      maxConstraintInfo.id    = PEEP_SOFT_MAX_BASED_ACCEPTED_ID;
      maxConstraintInfo.value = softMaxValue;
    }  // $[TI8] -- hard max is more restrictive...

   
    // check for soft bound id  ($[TI9] -- TRUE  $[TI10] -- FALSE)...
    maxConstraintInfo.isSoft = (maxConstraintInfo.id ==
					      PEEP_SOFT_MAX_BASED_ACCEPTED_ID);

    // update the maximum bound to the most restrictive bound...
    getBoundedRange_().updateMaxConstraint(maxConstraintInfo);
  }


  //-------------------------------------------------------------------
  // determine PEEP's minimum constraint value...
  //-------------------------------------------------------------------
  {
    BoundedRange::ConstraintInfo  minConstraintInfo;

    //-----------------------------------------------------------------
    // initialize to PEEP's absolute minimum...
    //-----------------------------------------------------------------

    minConstraintInfo.id    = PEEP_MIN_ID;
    minConstraintInfo.value = getAbsoluteMinValue_();

    //-------------------------------------------------------------------
    // now that the most restrictive hard bound has been determined, compare
    // to soft-bound limit...
    //-------------------------------------------------------------------

    if (isSoftBoundActive_(PEEP_SOFT_MIN_BASED_ACCEPTED_ID)  &&
        softMinValue > minConstraintInfo.value)
    {  // $[TI11] -- soft min is more restrictive...
      minConstraintInfo.id    = PEEP_SOFT_MIN_BASED_ACCEPTED_ID;
      minConstraintInfo.value = softMinValue;
    }  // $[TI12] -- hard min is more restrictive...

    // check for soft bound id  ($[TI13] -- TRUE  $[TI14] -- FALSE)...
    minConstraintInfo.isSoft = (minConstraintInfo.id ==
					      PEEP_SOFT_MIN_BASED_ACCEPTED_ID);

    // update the minimum bound to the most restrictive bound...
    getBoundedRange_().updateMinConstraint(minConstraintInfo);
  }
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  findSoftMinMaxValues_(rSoftMinValue, rSoftMaxValue)  [const]
//
//@ Interface-Description
//  Return, via 'rSoftMinValue' and 'rSoftMaxValue', the soft bound lower
//  and upper limit values, respectively.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[NE02015] -- PEEP's soft range...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
PeepSetting::findSoftMinMaxValues_(Real32& rSoftMinValue,
				   Real32& rSoftMaxValue) const
{
  CALL_TRACE("findSoftMinMaxValues_(rSoftMinValue, rSoftMaxValue)");

  const Real32  ABSOLUTE_MIN = getAbsoluteMinValue_();
  const Real32  ABSOLUTE_MAX = getAbsoluteMaxValue_();

  const Setting*  pCircuitType =
		      SettingsMgr::GetSettingPtr(SettingId::PATIENT_CCT_TYPE);

  const DiscreteValue  CIRCUIT_TYPE = pCircuitType->getAdjustedValue();

  switch (CIRCUIT_TYPE)
  {
  case PatientCctTypeValue::ADULT_CIRCUIT :
  case PatientCctTypeValue::PEDIATRIC_CIRCUIT :		// $[TI1]
    // no soft-bounds for these circuit types...
    rSoftMinValue = ABSOLUTE_MIN;
    rSoftMaxValue = ABSOLUTE_MAX;
    break;
  case PatientCctTypeValue::NEONATAL_CIRCUIT :		// $[TI2]
    {
      const AdjustedContext::BatchState  BATCH_STATE =
			    ContextMgr::GetAdjustedContext().getBatchState();

      if (BATCH_STATE == AdjustedContext::NEW_PATIENT_BATCH  ||
	  BATCH_STATE == AdjustedContext::VENT_SETUP_BATCH)
      {  // $[TI2.1]
	// no soft-bounds when in New-Patient or Vent Setups...
	rSoftMinValue = ABSOLUTE_MIN;
	rSoftMaxValue = ABSOLUTE_MAX;
      }
      else
      {  // $[TI2.2]
	const Real32  ACCEPTED_PEEP_VALUE =
					BoundedValue(getAcceptedValue()).value;

	rSoftMinValue = (ACCEPTED_PEEP_VALUE - 2.0f);
	rSoftMaxValue = (ACCEPTED_PEEP_VALUE + 2.0f);
      }
    }
    break;
  default :
    // unexpected circuit type...
    AUX_CLASS_ASSERTION_FAILURE(CIRCUIT_TYPE);
    break;
  };
}
