 
#ifndef SListC_AlarmUpdate_HH
#define SListC_AlarmUpdate_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: SListC_AlarmUpdate - Singly-Linked List of 'AlarmUpdate' Items.
//--------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Foundation/vcssrc/AbstractSListC.H_v   25.0.4.0   19 Nov 2013 14:03:42   pvcs  $
//
//@ Modification-Log
//
//   Revision: 003   By: sah   Date: 07-Jan-1999   DR Number: 5321
//   Project:  ATC
//   Description:
//	Changed 'SoftFault()' method to an non-inlined method.
//  
//   Revision: 002   By: sah   Date: 06-Feb-1996   DCS Number: 674
//   Project:  Sigma (R8027)
//   Description:
//	Removed 'ifndef' and 'endif' from around includes.
//  
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//====================================================================

#if defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)
class  Ostream;
#endif  // defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)

//@ Usage-Classes
#include "SNodeC_AlarmUpdate.hh"
#include "Heap.hh"
//@ End-Usage


class SListC_AlarmUpdate
{
#if defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)
    // Friend:  operator<<
    // This facilitates debug-only printing of this class's instances.
    friend inline Ostream&  operator<<(Ostream&                  ostr,
				       const SListC_AlarmUpdate& sList);
#endif  // defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)

    //@ Friend:  SIterC_AlarmUpdate
    // This list's corresponding iterator needs access to the protected
    // members of this list.
    friend class  SIterC_AlarmUpdate;

    //@ Friend:  SListR_AlarmUpdate
    // The singly-linked reference list uses a singly-linked list of
    // void*'s, and needs access to it's protected members.
    friend class  SListR_AlarmUpdate;

  public:
    virtual ~SListC_AlarmUpdate(void);

    inline Boolean  isEmpty(void) const;
    inline Boolean  isFull (void) const;

    inline Boolean  isAtFirst(void) const;
    inline Boolean  isAtLast (void) const;

    inline Uint32  getNumItems(void) const;
    inline Uint32  getMaxItems(void) const;

    inline SigmaStatus  goFirst(void);
    inline SigmaStatus  goNext (void);

    SigmaStatus  search(const AlarmUpdate&      item,
			const SearchFrom startFrom = FROM_FIRST);

    void  insert (const AlarmUpdate&          newItem,
		  const InsertionPlace insertPlace = AFTER_CURRENT);
    void  append (const AlarmUpdate& newItem);
    void  prepend(const AlarmUpdate& newItem);

    SigmaStatus  removeCurrent(void);
    SigmaStatus  removeItem(const AlarmUpdate& item);
    void         clearList(void);

    inline const AlarmUpdate&  currentItem(void) const;
    inline AlarmUpdate&        currentItem(void);

    void  operator=(const SListC_AlarmUpdate& sList);

    inline Boolean  operator==(const SListC_AlarmUpdate& sList) const;
    inline Boolean  operator!=(const SListC_AlarmUpdate& sList) const;

    static void  SoftFault(const SoftFaultID softFaultID,
                           const Uint32      lineNumber,
                           const char*       pFileName = NULL,
                           const char*       pBoolTest = NULL);

  protected:
    inline SListC_AlarmUpdate(Heap* pHeap);

    // return a pointer to the link that is equivalent to 'item', starting
    // the search from 'pStart'...
    const SNodeC_AlarmUpdate* findNode_(const AlarmUpdate&        item,
				 const SNodeC_AlarmUpdate* pStartNode) const;

    inline Boolean  isLegalNode_(const SNodeC_AlarmUpdate* pNode) const;

    inline const SNodeC_AlarmUpdate*  getFirst_  (void) const;
    inline const SNodeC_AlarmUpdate*  getCurrent_(void) const;

    inline SNodeC_AlarmUpdate*  getFirst_  (void);
    inline SNodeC_AlarmUpdate*  getCurrent_(void);

    inline void  setCurrent_(SNodeC_AlarmUpdate* pNewCurr);

    Boolean  isEquivTo_(const SListC_AlarmUpdate& sList) const;

#if defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)
    virtual Ostream&  print_(Ostream& ostr) const = 0;
#endif  // defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)

  private:
    SListC_AlarmUpdate(const SListC_AlarmUpdate&);	// not implemented...
    SListC_AlarmUpdate(void);				// not implemented...

    inline void  deallocNode_(SNodeC_AlarmUpdate*& pOldNode);

    SNodeC_AlarmUpdate* getPrevNode_(const SNodeC_AlarmUpdate* pStart) const;

    //@ Data-Member:  rHeapOfNodes_
    // A reference to this list's heap of nodes.
    Heap&  rHeapOfNodes_;

    //@ Data-Member:  pFirst_
    // A pointer to the first node in this list.
    SNodeC_AlarmUpdate*  pFirst_;

    //@ Data-Member:  pCurrent_
    // A pointer to the current node of this list.
    SNodeC_AlarmUpdate*  pCurrent_;
};


// Inlined methods...
#include "SListC_AlarmUpdate.in"


#endif  // SListC_AlarmUpdate_HH
