#ifndef FsCrossCheckTest_HH
#define FsCrossCheckTest_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: FsCrossCheckTest - Test the flow sensors and PSOLs
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/FsCrossCheckTest.hhv   25.0.4.0   19 Nov 2013 14:23:40   pvcs  $
//
//@ Modification-Log
//
//  Revision: 008  By: quf    Date: 26-Dec-2000     DR Number: 5832
//  Project:  Baseline
//  Description:
//	- Extracted code from findLiftoff_() to create method
//	  buildPsolTables_() which performs a single iteration of the
//	  PSOL liftoff calibration for both gases.  Removed bug which
//	  allowed a partial (i.e. corrupt) calibration table build.
//	- findLiftoff_() now performs 3 PSOL liftoff calibrations per gas
//	  and averages the results.
//
//  Revision: 007  By: sah    Date: 30-Jun-1999     DR Number: 5327
//  Project:  NeoMode
//  Description:
//      Incorporation of NeoMode Project requirements.
//
//  Revision: 006  By: quf    Date: 13-Oct-1999     DR Number: 5540
//  Project:  ATC
//  Description:
//		PSOL current check modified to account for gas type and Patm.
//
//  Revision: 005  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//	Revision: 004  By:  quf    Date: 17-Nov-1997    DR Number: DCS 2513
//      Project:  Sigma (840)
//      Description:
//			Removed lowFlowTest() and highFlowTest().
//
//	Revision: 003  By:  quf    Date: 10-Oct-1997    DR Number: DCS 2453
//      Project:  Sigma (840)
//      Description:
//			Code and comment cleanup from code peer review.
//
//  Revision: 002  By:  quf    Date: 22-May-1997    DR Number: DCS 2131
//       Project:  Sigma (840)
//       Description:
//            Modification-Log fix only.
//
//  Revision: 001  By:  mad    Date:  2-Nov-1994    DR Number: 
//	Project:  Sigma (R8027)
//	Description:
//		Initial version (Integration baseline).
//
//====================================================================

#include "ServiceMode.hh"

//@ Usage-Classes

#include "SmStatusId.hh"
#include "SigmaState.hh"
#include "PsolLiftoff.hh"

#ifndef SIGMA_UNIT_TEST
# include "Psol.hh"
# include "FlowSensor.hh"
#else
# include "FakePsol.hh"
# include "FakeSensor.hh"
#endif  // SIGMA_UNIT_TEST

//@ End-Usage

class FsCrossCheckTest
{
  public:

	enum TestFlowId
	{
		TEST_120_LPM = 0 ,
		TEST_60_LPM  = 1 ,
		TEST_20_LPM	 = 2 ,
		TEST_5_LPM   = 3 ,
		TEST_1_LPM   = 4 ,
		TEST_0_LPM   = 5 ,

		MAX_FS_TEST_POINTS = 6
	};

    FsCrossCheckTest(void);
    ~FsCrossCheckTest(void);

    static void SoftFault(const SoftFaultID softFaultID,
		  const Uint32      lineNumber,
		  const char*       pFileName  = NULL, 
		  const char*       pPredicate = NULL);

    void estTest (void);
    void sstTest (void);

  protected:

  private:
    FsCrossCheckTest( const FsCrossCheckTest&);	// not implemented...
    void operator=( const FsCrossCheckTest&);	// not implemented...

	Boolean testFlowAndPsol_( GasType testGas, const TestFlowId flowIdStart,
				const TestFlowId flowIdEnd, SigmaState state = STATE_SERVICE);
					
	void findLiftoff_( const Boolean airPassed, const Boolean o2Passed);
	void buildPsolTables_( Boolean airPassed, Boolean o2Passed,
								PsolLiftoff& psolLiftoff );

	Real32 expectedPsolCurrent_( const GasType testGas,
									const Real32 nominalPsolCurrent );

	//@ Data-Member: flowTestPoint_[]
	// array of test flow levels
	Real32 flowTestPoint_[MAX_FS_TEST_POINTS];

	//@ Data-Member: commandTestPoint_[]
	// array of PSOL command nominal values corresponding to
	// the test flow levels
	Real32 commandTestPoint_[MAX_FS_TEST_POINTS];

	//@ Data-Member: commandTestLimit_[]
	// array of PSOL command max deviations corresponding to
	// the test flow levels
	Real32 commandTestLimit_[MAX_FS_TEST_POINTS];
};


#endif // FsCrossCheckTest_HH 
