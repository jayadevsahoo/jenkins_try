
#ifndef BatchValue_HH
#define BatchValue_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: BatchValue - Union of the possible values for a batch setting.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/BatchValue.hhv   25.0.4.0   19 Nov 2013 14:27:16   pvcs  $
//
//@ Modification-Log
//  
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//		Integration baseline.
//
//====================================================================

#include "SettingClassId.hh"
#include "SettingId.hh"

//@ Usage-Classes
#include "BoundedValue.hh"
#include "DiscreteValue.hh"
#include "SequentialValue.hh"
//@ End-Usage


class BatchValue
{
  public:
    inline BatchValue(const BatchValue&      batchValue);
    inline BatchValue(const BoundedValue&    initBoundedValue);
    inline BatchValue(const DiscreteValue    initDiscreteValue);
    inline BatchValue(const SequentialValue  initSequentialValue);
    BatchValue(void);
    ~BatchValue(void);

    inline const BoundedValue&  getBoundedValue(void) const;
    inline void  setBoundedValue(const BoundedValue& newValue);

    inline DiscreteValue  getDiscreteValue(void) const;
    inline void           setDiscreteValue(const DiscreteValue newValue);

    inline SequentialValue  getSequentialValue(void) const;
    inline void             setSequentialValue(const SequentialValue newValue);

  private:
    void  operator=(const BatchValue&);		// not implemented...

    union
    {
      //@ Data-Member:  boundedValue_
      // A bounded batch setting value.
      BoundedValue  boundedValue_;

      //@ Data-Member:  discreteValue_
      // A discrete batch setting value.
      DiscreteValue  discreteValue_;

      //@ Data-Member:  sequentialValue_
      // A sequential batch setting value.
      SequentialValue  sequentialValue_;
    };
};


// Inlined methods
#include "BatchValue.in"


#endif // BatchValue_HH 
