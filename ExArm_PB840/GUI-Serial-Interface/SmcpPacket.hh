#ifndef	SmcpPacket_HH
# define SmcpPacket_HH

//====================================================================
// This is a proprietary work to which Nellcor Puritan Bennett
// corporation of California claims exclusive right.  No part of this
// work may be used, disclosed, reproduced, sorted in an information
// retrieval system, or transmitted by any means, electronic,
// mechanical, photocopying, recording, or otherwise without the prior
// written permission of Nellcor Puritan Bennett Corporation.
//
//     Copyright (c) 1997, Nellcor Puritan Bennett Corporation
//=====================================================================

// =========================== M O D U L E   D E S C R I P T I O N ====
//@ Filename:  SmcpPacket - SMCP Communications Packet Declaration
//---------------------------------------------------------------------
//@ Version-Information
// @ $Header:   /840/Baseline/GUI-Serial-Interface/vcssrc/SmcpPacket.hhv   25.0.4.0   19 Nov 2013 14:12:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003    By: rhj       Date: 05-Sept-2006  DR Number: 6169
//  Project: RESPM
//  Description:
//    Increase the buffer size to support the Alarm log downloads.
//
//  Revision: 002    By: quf       Date: 16-Aug-2001  DR Number: 5493
//  Project: GuiComms
//  Description:
//  Introduced encapsulated version of class IDs.
//
//  Revision: 001    By: Gary Cederquist  Date: 24-Nov-1997  DR Number: 2605
//    Project: Sigma (R8027)
//    Description:
//       Complete restructuring of GUI-Serial-Interface to fix
//       several problems.
//
//=====================================================================

#include "GUI_Serial_Interface.hh"

enum SmcpResponseParameter
{
	MAX_SMCP_RESPONSE_SIZE = 200
};

//@ Type: SmcpMessage
// structure of SMCP data passed between the application and SmcpInterface
struct SmcpMessage
{
	Uint8  number;
	Uint8  function;
	Uint8  data[MAX_SMCP_RESPONSE_SIZE]; // includes 16-bit CRC
};

//@ Type: SmcpPacket
// structure of SMCP data passed between the SerialInterface class and the host
struct	SmcpPacket
{
	Uint16		length;	      // of rest of packet
	SmcpMessage smcpMessage;
};

#endif	// ! SmcpPacket_HH
