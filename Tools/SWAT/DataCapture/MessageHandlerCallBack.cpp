//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file MessageHandlerCallBack.cpp
//----------------------------------------------------------------------------

//-------------------------------------
// I N C L U D E S
//-------------------------------------
#include"MessageHandlerCallBack.h"
#include"CLogger.h"

//-------------------------------------
// C O D E
//-------------------------------------

//---------------------------
// ~MessageHandlerCallBack
//---------------------------
MessageHandlerCallBack::~MessageHandlerCallBack()
{
}

//---------------------------
// MessageHandlerCallBack
//---------------------------
MessageHandlerCallBack::MessageHandlerCallBack(
        bool (*function)(boost::shared_ptr<Message> msg, void *pUser), 
        void *pUser):
    m_function(function),
    m_pUser(pUser), 
    m_pActive(true)
{
}

//---------------------------
// execute
//---------------------------
bool MessageHandlerCallBack::execute(boost::shared_ptr<Message> msg)
{
    bool bRetval = true;

    if(m_pActive)
    {
        bRetval = m_function(msg, m_pUser);
    }

    return bRetval;
}

//---------------------------
// activate
//---------------------------
void MessageHandlerCallBack::activate()
{
    m_pActive = true;
}

//---------------------------
// deactivate
//---------------------------
void MessageHandlerCallBack::deactivate()
{
    m_pActive = false;
}



