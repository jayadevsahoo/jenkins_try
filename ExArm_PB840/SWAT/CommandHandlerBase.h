////////////////Copyright (c) 2013 Covidien/////////////////////////////////////
/*******************************************************************************
Name:		CommandHandlerBase.h

Purpose:
	Header file for CommandHandlerBase class.

REVISION HISTORY:

REVISION: 001	BY: SF		DATE: 20-AUG-2013
PROJECT: e600 vent
DR NUMBER: N / A
DESCRIPTION:
	Initialize version.

END OF REVISIONS

*******************************************************************************/

#pragma once

#include "tinyxml.hh"
#include "SWATTypes.h"

class ISWATService;
class CommandHandlerBase
{
private:
	// if there is error in parse-execute-BuildResponse process
	SWATStatus  	err_;

protected:
	// child class need to generate response in to this buffer
	char			resp_[4096];
	
	ISWATService*	PSWATService_;

	// a handler class must implement this function to parse a xml command
	virtual void ParseCommand(TiXmlElement* ele) = 0;
	
	// a handler class must implement this function to execute a command
	virtual void EexecuteCommand() = 0;

	// build a ACK_NAK response by default.
	virtual void BuildResponse();

	// set a error
	inline void Error(SWATStatus erroCode);
	
public:
	CommandHandlerBase();
	virtual ~CommandHandlerBase();

	void ProcessCommandBody(TiXmlElement* ele);

	inline const char*  GetResponse()   const;

	inline bool         HasError()      const;
};

////////////////////////////////////////////////////////////////////////////////
// inline functions implementation
////////////////////////////////////////////////////////////////////////////////
bool CommandHandlerBase::HasError() const
{
	return (err_ != SWAT_SUCCESS);
}

const char* CommandHandlerBase::GetResponse() const
{
	return resp_;
}

void CommandHandlerBase::Error( SWATStatus erroCode ) 
{
	this->err_ = erroCode;
}