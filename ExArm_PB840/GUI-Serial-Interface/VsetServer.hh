#ifndef	VsetServer_HH
#define VsetServer_HH
//====================================================================
// This is a proprietary work to which the Covidien corporation claims 
// exclusive right.  No part of this work may be used, disclosed, 
// reproduced, sorted in an information retrieval system, or 
// transmitted by any means, electronic, mechanical, photocopying, 
// recording, or otherwise without the prior written permission of 
// Covidien Corporation.
//
//              Copyright (c) 2010, Covidien Corporation
//=====================================================================

//=====================================================================
//@ Class: VsetServer - VSET command server
//---------------------------------------------------------------------
//@ Version-Information
//  @(#) $Header:   /840/Baseline/GUI-Serial-Interface/vcssrc/VsetServer.hhv   25.0.4.0   19 Nov 2013 14:12:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By:  gdc    Date:  11-Aug-2010    SCR Number: 6663
//  Project:  API/MAT
//  Description:
//	Initial version.
//=====================================================================

#include "Sigma.hh"
#include "GUI_Serial_Interface.hh"

//@ Usage-Classes
#include "BoundedValue.hh"
#include "SerialInterface.hh"
#include "Sound.hh"

//@ End-Usage

class VsetServer
{
	public:
		VsetServer(void);
		~VsetServer(void);

		static  VsetServer& GetVsetServer(Uint32 portNum);
		static void ProcessBatch(void);
		static void Report(const char* statusMsg);
		static void Report(BoundedValue boundedValue, const char* boundMsg);
		static void Report(const char* value, Boolean isBoundViolated, const char* boundMsg);

		static  void    SoftFault(const SoftFaultID softFaultID,
								  const   Uint32  lineNumber,
								  const   char *  pFileName = NULL,
								  const   char *  pPredicate = NULL);

		int processCmd(const char * cmdLine);
		void processBatch(void);

	private:

		VsetServer(const VsetServer &);	// not implemented...
		void    operator=(const VsetServer &);	// not implemented...

		void output_(void);
		void soundAlert_(Sound::SoundId id) const;
		void report_(const char* status);

		SigmaStatus changeSetting_(const SettingId::SettingIdType settingId, 
								   const char* settingName, 
								   const char* valueArg);

		// The serial port associated with this instantiation of the VsetServer
		Uint32 portNum_;

		// serial output buffer returned to external controller 
		// sized to allow processing of at least 10 settings - TBD
		char buf_[512];

		// buffer used to tokenize command line
		char command_[256];

		// number of bytes in output buffer
		Int32 bytesOut_;

		// ACCEPT/REJECT sounds enabled or not
		Boolean alertsEnabled_;

		// 10 settings max per batch based on 2 parameters per setting
		char* argv_[20];

		int argc_;

		// the processBatch return code
		int batchStatus_; 
};

#endif	// VsetServer_HH
