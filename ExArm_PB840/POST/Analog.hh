#ifndef Analog_HH
#define Analog_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//=====================================================================
// Class: Analog - Analog Interface Tests and Support Functions
//---------------------------------------------------------------------
//
//@ Modification-Log
//
//  Revision: 002    By: Gary Cederquist  Date: 10-AUG-1999  DR Number: 5502
//    Project: 806 Compressor
//    Description:
//       Removed AC Voltage Test which was redundant to background test.
//
//  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//
//=====================================================================

#ifdef SIGMA_BD_CPU

#include "Post.hh"
#include "PostAdcChannels.hh"

// Test table for analog devices
typedef struct
{
   Uint16   		lowLimit;
   Uint16   		highLimit;
   Criticality 	criticality;
}  TestTableStruct;

typedef struct
{
   Uint16 testDacPoint; //with hamming code encoded
   TestTableStruct checkPoint;
} TestDacStruct;

extern const TestTableStruct PostAnalogDeviceTestTable[];

class Analog {
  public:
    static Criticality TestAnalogInterfacePCB    (void);
    static Criticality TestADC                   (void);
    static Criticality TestDAC                   (void);
    static Criticality TestAnalogDevices         (void);
    static Criticality TestSerialDevice          (void);
    static Boolean     OutOfRange( Uint16 val, TestTableStruct range);
    static Uint16      ReadAdc (PostAdcChannels::AdcChannelId adcId);
    static Boolean     WaitAdcNewSample          (void);

private:
    Analog();        // not implemented
    ~Analog();       // not implemented

    static const TestDacStruct TestDac3ATable_[];
    static const TestDacStruct TestDac3BTable_[];
    static const TestDacStruct TestDac4ATable_[];
    static const TestDacStruct TestDac4BTable_[];

    static Boolean	ReadAnalogChannels_(Uint16 *pChannelBuffer);
    static Boolean	ReadSubmux_(  Uint16 selectSubmux
                                , Uint16 & rSubmuxValue );

    static Criticality  TestDacPoints_(  const TestDacStruct * pTestTable
                                       , const Int32 maxIndex);

    static Uint32 TestRAM_(  Uint16 * const startAddress  
                           , const Uint16 length);
};

#endif  // SIGMA_BD_CPU
#endif   // Analog_HH
