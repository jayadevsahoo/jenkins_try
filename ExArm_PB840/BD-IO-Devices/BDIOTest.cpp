#include "stdafx.h"
//*****************************************************************************
//
//  BDIOTest.cpp - Implementation of the BDIOTest_t class.
//
//*****************************************************************************
#include "BDIOTest.h"
#include "BDIORefs.hh"
#include "ExhFlowSensor.h"
#include "ServosGlobal.h"
#include "gpio_ioctls.h"

#include "BitAccessRefs.hh"
#include "BinaryCommand.hh"
#include "BinaryIndicator.hh"
#include "BitAccessGpio.hh"
#include "BitAccessGpioMediator.hh"
#include "Psol.hh"
#include "FlowSensor.hh"
#include "PressureSensor.hh"
#include "AdcChannels.hh"
#include "SensorMediator.hh"
#include "MainSensorRefs.hh"
#include "BD_IO_Devices.hh"
#include "Solenoid.hh"
#include "ValveRefs.hh"
#include "VcvPhase.hh"
#include "PhaseRefs.hh"
#include "FlowController.hh"
#include "PressureController.hh"
#include "ControllersRefs.hh"
#include "ExhalationValve.hh"
#include "BreathPhase.hh"

#include "SettingId.hh"
#include "SettingXmitData.hh"
#include "ContextMgr.hh"
#include "PendingContext.hh"
#include "Post.hh"

#include "UtilityFunctions.h"
#include "EthernetBroadcastManager.h"

#include "SmRefs.hh"
#include "SmManager.hh"

#define FORCE_ALL_CALS	1
#define SKIP_PSOL_CAL	1

static Psol *pDesiredFlowPsol;

static UINT SettingIdMap[NUM_TEST_SETTING_IDS] =
{
	SettingId::MAND_TYPE,
	SettingId::MODE,
	SettingId::VENT_TYPE,
	SettingId::TRIGGER_TYPE,
	SettingId::FLOW_PATTERN,
	SettingId::SUPPORT_TYPE,
	SettingId::RESP_RATE,
	SettingId::TIDAL_VOLUME,
	SettingId::PEAK_INSP_FLOW,
	SettingId::PLATEAU_TIME,
	SettingId::FLOW_SENS,
	SettingId::PRESS_SENS,
	SettingId::OXYGEN_PERCENT,
	SettingId::PEEP,
	SettingId::INSP_PRESS,
	SettingId::INSP_TIME,
	SettingId::FLOW_ACCEL_PERCENT,
	SettingId::PRESS_SUPP_LEVEL,
	SettingId::EXP_SENS,
	SettingId::IBW,
	//Alarm settings start here 
	SettingId::HIGH_CCT_PRESS,
	//Calculated settings start here
	SettingId::MIN_INSP_FLOW
};

//*****************************************************************************
//
//  BDIOTest_t::BDIOTest_t - BDIOTest constructor
//
//*****************************************************************************

BDIOTest_t::BDIOTest_t()
{
}

void BDIOTest_t::Initialize()
{
	pDesiredFlowPsol = new Psol(Psol::DESIRED_FLOW, 0);

	// using legacy E600 code to send flow
	pP1Transducer = new TransducerCalibrationTable_t(CAL_ID_P1, 0, 60);
	pP2Transducer = new TransducerCalibrationTable_t(CAL_ID_P2, 0, 60);
	pPdrvTransducer = new TransducerCalibrationTable_t(CAL_ID_PDRIVE, 0, 60);
	pAirValve = new InspValveCalibrationTable_t(CAL_ID_AIR_VALVE, DAC_AIR_VALVE);
	pO2Valve = new InspValveCalibrationTable_t(CAL_ID_O2_VALVE, DAC_O2_VALVE);

	// TODO E600 MS. Remove the entire e600 exhalation valve calibration when no longer needed.
	//pExhValve = new ExhValveCalibrationTable_t(CAL_ID_EXH_VALVE);
	pExhFlowSensorCal = new ExhFlowSensorCalibrationTable_t(CAL_ID_EXH_FLOW_SENSOR, DAC_AIR_VALVE, DAC_O2_VALVE);	
	pAirInletPressureTransducer = new TransducerCalibrationTable_t(CAL_ID_PAIR, 0, 15);
	pO2InletPressureTransducer = new TransducerCalibrationTable_t(CAL_ID_PO2, 0, 15);

	CalibrationObj[CAL_ID_P1] = pP1Transducer;
	CalibrationObj[CAL_ID_P2] = pP2Transducer;
	CalibrationObj[CAL_ID_PDRIVE] = pPdrvTransducer;
	CalibrationObj[CAL_ID_AIR_VALVE] = pAirValve;
	CalibrationObj[CAL_ID_O2_VALVE] = pO2Valve;
	// TODO E600 MS. Remove the entire e600 exhalation valve calibration when no longer needed.
	//CalibrationObj[CAL_ID_EXH_VALVE] = pExhValve;
	CalibrationObj[CAL_ID_PAIR] = pAirInletPressureTransducer;
	CalibrationObj[CAL_ID_PO2] = pO2InletPressureTransducer;

	TestState = BDIO_TEST_STATE1;

	BdioTestCase = BDIO_TEST_NONE;
	// to have an autozero of the exh. flow sensor at initialization
	BdioTestCase = BDIO_TEST_EXH_AZ;
	//BdioTestCase = BDIO_TEST_EXH_FLOW_SENSOR;
	//BdioTestCase = BDIO_CALIBRATION;
	//BdioTestCase = BDIO_TEST_AIR_FLOW_SENSOR;
	//BdioTestCase = BDIO_TEST_O2_FLOW_SENSOR;

	BdioCalType = CAL_NONE;
	//BdioCalType = CAL_EXH_FLOW_SENSOR;
	//BdioCalType = CAL_FLOW_SENSOR_OFFSET;
	//BdioCalType = CAL_PSOL_LIFTOFF;
	//BdioCalType = BDIO_CAL_EXH_VALVE;
	//BdioCalType = CAL_ALL;

	ProgressCount = 0;
}


BDIOTest_t::~BDIOTest_t(void)
{
    if (pDesiredFlowPsol)
    {
        delete pDesiredFlowPsol;
    }

    if (pAirValve)
    {
        delete pAirValve;
    }

    if (pO2Valve)
    {
        delete pO2Valve;
    }

    if (pExhFlowSensorCal)
    {
        delete pExhFlowSensorCal;
    }
}


void BDIOTest_t::ResetAllValves()
{
	AioDioDriver.Write(DAC_AIR_VALVE, 0);
	AioDioDriver.Write(DAC_O2_VALVE, 0);
	AioDioDriver.Write(DAC_EXHALATION_VALVE, 0);
}

void BDIOTest_t::CancelCal(BdioCalType_t CalType)
{
	if(BdioCalType == CalType)
	{
		BdioCalType = CAL_NONE;
		ProgressCount = 0;
		ResetAllValves();
		RETAILMSG(1, (L"BDIOTest: Calibration canceled\r\n"));
	}
}

void BDIOTest_t::SendDebugPacket()
{
	float DebugBuf[18];

	DebugBuf[0] = ConvertedData.O2Flow;
	DebugBuf[1] = ConvertedData.AirFlow;
	DebugBuf[2] = ConvertedData.ExhalationFlow;
	DebugBuf[3] = ConvertedData.InspPressure;
	DebugBuf[4] = ConvertedData.ExpPressure;
	DebugBuf[5] = ConvertedData.ExhDrivePressure;
	DebugBuf[6] = ConvertedData.AirPressure;
	DebugBuf[7] = ConvertedData.O2Pressure;
	DebugBuf[8] = ConvertedData.AirwayPressure;
	DebugBuf[9] = RawA2dReadings.O2Flow.Value;
	DebugBuf[10] = 0;	// Bar Pressure in E600 Eng.
	DebugBuf[11] = CommandValues.AirDac;
	DebugBuf[12] = CommandValues.O2Dac;
	DebugBuf[13] = CommandValues.ExhalationDAC;
	DebugBuf[14] = RawA2dReadings.AirTemp.Value;
	DebugBuf[15] = RawA2dReadings.ExhaleFlow.Value;
	DebugBuf[16] = RawA2dReadings.AirFlow.Value;
	DebugBuf[17] = RawA2dReadings.O2Temp.Value;

	EthernetBroadcastManager.SendDebug(DebugBuf);
}


void BDIOTest_t::ConvertToEngineeringUnits(ConvertedA2dReadings_t &Converted)
{
	Converted.ExpPressure = RExhPressureSensor.getValue();
	Converted.FilteredExpPressure = (Pressure_t)RExhPressureSensor.getFilteredValue();

	Converted.InspPressure = RInspPressureSensor.getValue();
	Converted.FilteredInspPressure = (Pressure_t)RInspPressureSensor.getFilteredValue();

	Converted.ExhDrivePressure = RExhDrvPressureSensor.getValue();
    if(Converted.ExhDrivePressure < 0)
		Converted.ExhDrivePressure = 0;
	Converted.FilteredExhDrivePressure = (Pressure_t)RExhDrvPressureSensor.getFilteredValue();

	Converted.AirFlow = RAirFlowSensor.getValue();
	Converted.FilteredAirFlow = RAirFlowSensor.getFilteredValue();

	Converted.O2Flow = RO2FlowSensor.getValue();
	Converted.FilteredO2Flow = RO2FlowSensor.getFilteredValue();

	RExhFlowSensor.GetFlow(Converted.ExhalationFlow);
	Converted.FilteredExhFlow = RExhFlowSensor.getFilteredValue();
}

void BDIOTest_t::SetCommandValues()
{
	CommandValues.AirDac = RAirPsol.getPsolCommand();
	CommandValues.O2Dac = RO2Psol.getPsolCommand();
	CommandValues.ExhalationDAC = RExhalationValve.getPsolCommand();
	CommandValues.DesiredAirFlow = RAirFlowController.getDesiredFlow();
	CommandValues.DesiredO2Flow = RO2FlowController.getDesiredFlow();
	CommandValues.DesiredPressure = RPressureController.getDesiredPressure();
	CommandValues.Phase = (float)BreathPhase::GetCurrentBreathPhase()->getPhaseType();
}

//*****************************************************************************
//
//  BDIOTest_t::ProcessA2ds -  Process A/Ds, servo, write DACs
//
//*****************************************************************************
bool BDIOTest_t::ProcessBdioTest()
{
	static DWORD PrevTickCount = GetTickCount();
	DWORD ElapsedTickCount;
	static DWORD ElapsedTickSum = 0;
	static DWORD DelayCount = 0;
	static uint16_t DacValue = 0;
	static bool bFlowSent = false;
	static float FlowValue = 10.0;
	static float FlowValuePrev;
	static bool bAdcChannelsInit = false;
	static bool bToggled = false;
	static bool bCalAll = false;
	static bool bCalStarted = false;
	static uint16_t TestStep = 0;
	static bool	bDelayed = false;
	static InspFlowType_t FlowType = FLOW_TYPE_AIR;

	static CalibrationStatus_t Status = CAL_PASSED;
	WCHAR P1Str[10], P2Str[10], PdrvStr[10];
	WCHAR FlowO2Str[10], FlowAirStr[10], FlowExhStr[10];

	ElapsedTickCount = GetTickCount() - PrevTickCount;
	ElapsedTickSum += ElapsedTickCount;

	if(!bAdcChannelsInit)
	{
		bAdcChannelsInit = true;
	}
	else
	{
		wsprintf(P1Str, L"%03.2f", ConvertedData.InspPressure);
		wsprintf(P2Str, L"%03.2f", ConvertedData.ExpPressure);
		wsprintf(PdrvStr, L"%03.2f", ConvertedData.ExhDrivePressure);
		wsprintf(FlowAirStr, L"%03.2f", ConvertedData.AirFlow);
		wsprintf(FlowO2Str, L"%03.2f", ConvertedData.O2Flow);
		wsprintf(FlowExhStr, L"%03.2f", ConvertedData.ExhalationFlow);
	}


	switch(TestState)
	{
	case BDIO_TEST_STATE1:
	{
		ResetAllValves();

		if(BdioTestCase == BDIO_TEST_EXH_AZ)
			TestState = BDIO_TEST_STATE2;
		else
			TestState = BDIO_TEST_STATE4;
		PrevTickCount = GetTickCount();
		break;
	}
	case BDIO_TEST_STATE2:
	{
		// wait 2 seconds before requesting autozero of exh. flow sensor
		// this autozero might not be nec. in the future
		if(ElapsedTickCount > 2000)
		{
			TestState = BDIO_TEST_STATE3;
			RExhFlowSensor.Autozero();
		}
		break;
	}
	case BDIO_TEST_STATE3:
	{
		if(!RExhFlowSensor.IsAutozeroInProgress())
		{
			RETAILMSG(1, (L"BDIOTest: Exh AZ Completed\r\n"));
			TestState = BDIO_TEST_STATE4;
			BdioTestCase = BDIO_TEST_NONE;
		}
		break;
	}
	case BDIO_TEST_STATE4:
	{
		static uint16_t TickCount = 0;
		if(BdioTestCase != BDIO_CALIBRATION)
		{
			// TODO E600 MS. Remove the calibration part of this file
			// when it is determined that they are no longer needed.
			if( FALSE  )
			//if( AioDioDriver.IsServiceRequest()  )
			{
				BdioTestCase = BDIO_CALIBRATION;
#if FORCE_ALL_CALS
				BdioCalType = CAL_ALL;
#endif
				TestState = BDIO_TEST_STATE5;
				RETAILMSG(1, (L"BDIOTest: In Calibration Mode\r\n"));
			}
			else if(++TickCount > 200)
			{
				TestState = BDIO_TEST_STATE5;
			}
		}
	}
	case BDIO_TEST_STATE5:
	{
		uint16_t ExeIntervalCount = 20;
		if(BdioTestCase == BDIO_TEST_NONE)
			ExeIntervalCount = 200;

		if(ElapsedTickCount >= ExeIntervalCount)
		{
			PrevTickCount = GetTickCount();
		}
		else
		{
			break;
		}

		switch(BdioTestCase)
		{
		case BDIO_TEST_DOUT:
			{
				// Testing BinaryCommand
				BinaryCommand biCmd((ApplicationDigitalOutputs_t)DOUT_LED6, RBitAccessGpio);
				static BitAccessGpio::BitState GpioOutState = BitAccessGpio::OFF;
				GpioOutState = (BitAccessGpio::BitState)(~(uint16_t)GpioOutState & 0x01);
				biCmd.setBit(GpioOutState);
			}
			break;
		case BDIO_TEST_DIN:
			{
				// Testing BinaryIndicator
				BinaryIndicator biInd(DIN_PB_OUT, RBitAccessGpio);
				static BinaryIndicator::IndicatorState GpioInState = BinaryIndicator::OFF;
				BinaryIndicator::IndicatorState NewGpioInState = biInd.getState();
				if((uint8_t)GpioInState != (uint8_t)NewGpioInState)
				{
					GpioInState = NewGpioInState;
					RETAILMSG(1, (L"BDIOTest: New Indicator State = %d\r\n", NewGpioInState));
				}
			}
			break;

		case BDIO_TEST_INSP_SOLENOID: case BDIO_TEST_EXH_SOLENOID:
		case BDIO_TEST_CROSSOVER_SOLENOID: case BDIO_TEST_SAFETY_SOLENOID:
			{
				Solenoid *pSolenoid;

				if(BdioTestCase == BDIO_TEST_INSP_SOLENOID)
					pSolenoid = &RInspAutozeroSolenoid;
				else if(BdioTestCase == BDIO_TEST_EXH_SOLENOID)
					pSolenoid = &RExhAutozeroSolenoid;
				else if(BdioTestCase == BDIO_TEST_CROSSOVER_SOLENOID)
					pSolenoid = &RCrossoverSolenoid;
				else
					pSolenoid = &RSafetyValve;

				if(ElapsedTickSum > 1000)
				{
					if(bToggled)
					{
						pSolenoid->open();
						bToggled = false;
					}
					else
					{
						pSolenoid->close();
						bToggled = true;
					}
					ElapsedTickSum = 0;
				}
			}
			break;
		case BDIO_TEST_DAC_AIR:
			RAirPsol.updatePsol(DacValue);
			DacValue += 5;
			if(DacValue > 4095)
			{
				DacValue = 0;
				RAirPsol.updatePsol(DacValue);
			}
			break;
		case BDIO_TEST_DAC_O2:
			RO2Psol.updatePsol(DacValue);
			DacValue += 5;
			if(DacValue > 4095)
			{
				DacValue = 0;
				RO2Psol.updatePsol(DacValue);
			}
			break;
		case BDIO_TEST_DAC_EXH:
			RExhPsol.updatePsol(DacValue);
			DacValue += 5;
			if(DacValue > 4095)
			{
				DacValue = 0;
				RExhPsol.updatePsol(DacValue);
			}
			break;
		case BDIO_TEST_DAC_DESIRED_FLOW:
			pDesiredFlowPsol->updatePsol(DacValue);
			DacValue += 5;
			if(DacValue > 4095)
			{
				DacValue = 0;
				pDesiredFlowPsol->updatePsol(DacValue);
			}
			break;
		case BDIO_TEST_AIR_FLOW_SENSOR: case BDIO_TEST_O2_FLOW_SENSOR:

			if(!bFlowSent)
			{
				if(BdioTestCase == BDIO_TEST_AIR_FLOW_SENSOR)
					pAirValve->SendFlow(FlowValue);
				else
					pO2Valve->SendFlow(FlowValue);
				bFlowSent = true;
				FlowValuePrev = FlowValue;
				FlowValue += 10.0;
				if(FlowValue > 200.0)
					FlowValue = 10.0;
				ElapsedTickSum = 0;
			}
			else
			{
				if(ElapsedTickSum > 5000)
				{
					WCHAR DelFlowStr[10], AirFlowStr[10], O2FlowStr[10], ExhFlowStr[10];
					wsprintf(DelFlowStr, L"%3.2f", FlowValuePrev);
					wsprintf(AirFlowStr, L"%3.2f", RAirFlowSensor.getValue());
					wsprintf(O2FlowStr, L"%3.2f", RO2FlowSensor.getValue());
					wsprintf(ExhFlowStr, L"%3.2f", RExhFlowSensor.getValue());

					DEBUGMSG(1, (L"DelFlow=%s, AirFlow=%s, O2Flow=%s, ExhFlow=%s\r\n",
						DelFlowStr, AirFlowStr, O2FlowStr, ExhFlowStr));
					bFlowSent = false;
				}
			}
			break;
		case BDIO_TEST_EXH_FLOW_SENSOR:
			if(TestStep == 0)
			{
				// wait 2 seconds before requesting autozero of exh. flow sensor
				// this autozero is to be removed in the future
				if(++DelayCount > 100)		// 100 * 20
				{
					RExhFlowSensor.Autozero();
					TestStep = 1;
				}
			}
			else if(TestStep == 1)
			{
				if(!RExhFlowSensor.IsAutozeroInProgress())
				{
					TestStep = 0;
					BdioTestCase = BDIO_TEST_NONE;
					RETAILMSG(1, (L"BDIOTest: Exh AZ Completed\r\n"));
				}
			}
			break;
		case BDIO_TEST_PRESSURE_SENSOR:
			if(!bFlowSent)
			{
				pAirValve->SendFlow(10);
				bFlowSent = true;
				ElapsedTickSum = 0;
			}

			if(ElapsedTickSum > 2000)
			{
				static uint16_t DacValue = 0;
				if(DacValue <= 1300)
					DacValue += 100;
				else
					DacValue += 10;
				if(DacValue > 1800)
					DacValue = 0;
				RExhPsol.updatePsol(DacValue);

				DEBUGMSG(1, (L"P1 = %s, P2 = %s, Pdrv = %s, ExhDac = %d\r\n", P1Str, P2Str, PdrvStr, DacValue));
				ElapsedTickSum = 0;
			}
			break;
		case BDIO_CALIBRATION:
			switch(BdioCalType)
			{
			case CAL_AIR_PSOL:
#if SKIP_PSOL_CAL
				RETAILMSG(1, (L"Air Psol Calibration skipped...\r\n"));
				BdioCalType = CAL_O2_PSOL;
#else
				if(!bCalStarted)
				{
					RETAILMSG(1, (L"Air Psol Calibration Started...\r\n"));
					bCalStarted = true;
				}
				Status = pAirValve->Calibrate(ConvertedData.FilteredAirFlow, true);
				if(Status != CAL_PENDING)
				{
					if(bCalAll)
					{
						BdioCalType = CAL_O2_PSOL;
					}
					else
					{
						ProgressCount = 0;
						BdioCalType = CAL_NONE;
						SendCalStatus(Status);
					}
					RETAILMSG(1, (L"Air Psol Calibration Completed\r\n"));
					bCalStarted = false;
				}
#endif
				break;
			case CAL_O2_PSOL:
#if SKIP_PSOL_CAL
				RETAILMSG(1, (L"O2 Psol Calibration skipped...\r\n"));
				BdioCalType = CAL_EXH_VALVE;
#else
				if(!bCalStarted)
				{
					RETAILMSG(1, (L"O2 Psol Calibration Started...\r\n"));
					bCalStarted = true;
				}
				Status = pO2Valve->Calibrate(ConvertedData.FilteredO2Flow, true);
				if(Status != CAL_PENDING)
				{
					if(bCalAll)
					{
						BdioCalType = CAL_EXH_VALVE;
					}
					else
					{
						ProgressCount = 0;
						BdioCalType = CAL_NONE;
						SendCalStatus(Status);
					}
					RETAILMSG(1, (L"O2 Flow Sensor Calibration Completed\r\n"));
					bCalStarted = false;
				}
#endif
				break;
			case CAL_EXH_VALVE:
				if(!bCalStarted)
				{
					if(ElapsedTickSum > 1000)
					{
						RETAILMSG(1, (L"Exh Valve Calibration Started...\r\n"));
						ElapsedTickSum = 0;
						bCalStarted = true;
					}
					else
						break;
				}
				Status = pExhValve->Calibrate(&ConvertedData);
				if(Status != CAL_PENDING)
				{
					// Save the cal data
					// TODO E600 MS. Remove the entire e600 exhalation valve calibration when no longer needed.
					//RExhalationValve.UpdateCalData(pExhValve);
					if(bCalAll)
					{
						BdioCalType = CAL_EXH_FLOW_SENSOR;
					}
					else
					{
						ProgressCount = 0;
						BdioCalType = CAL_NONE;
						SendCalStatus(Status);
					}
					RETAILMSG(1, (L"Exh Psol Calibration Completed\r\n"));
					bCalStarted = false;
				}
				break;
			case CAL_EXH_FLOW_SENSOR:
				// TODO E600_LL: uncomment the line beflow if wanting to do flow sensor offset cal for new sensor board only
				//if(ExhCommunicationThread_t::SensorBoard == EXH_SENSOR_BOARD_NEW)
				{
					BdioCalType = CAL_FLOW_SENSOR_OFFSET;
					break;
				}

				if(!bCalStarted)
				{
					if(ElapsedTickSum > 1000)
					{
						RETAILMSG(1, (L"Exh Flow Sensor Calibration Started...\r\n"));
						bCalStarted = true;
						ElapsedTickSum = 0;
					}
					else
						break;
				}
				Status = pExhFlowSensorCal->Calibrate(ConvertedData.FilteredAirFlow, FLOW_TYPE_AIR);

				if(Status != CAL_PENDING)
				{
					if(Status == CAL_PASSED)
					{
						RExhFlowSensor.UpdateCalData(pExhFlowSensorCal);
						
						if(bCalAll)
						{
							BdioCalType = CAL_PSOL_LIFTOFF;
						}
						else
						{
							ProgressCount = 0;
							BdioCalType = CAL_NONE;
							SendCalStatus(Status);
						}						
						RETAILMSG(1, (L"Exh Flow Sensor Calibration Completed\r\n"));
						bCalStarted = false;
						FlowType = FLOW_TYPE_AIR;
					}
					else
					{
						RETAILMSG(1, (L"Exh Flow Sensor Calibration Failed\r\n"));
					}
				}
				break;
			case CAL_FLOW_SENSOR_OFFSET:
				if(!bCalStarted)
				{
					if(ElapsedTickSum > 1000)
					{
						RETAILMSG(1, (L"Flow Sensor Offset Calibration Started\r\n"));
						bCalStarted = true;
						DelayCount = 0;
						Status = CAL_PENDING;
						ElapsedTickSum = 0;
					}
					else
						break;
				}
				else if(!bDelayed)
				{
					if(++DelayCount < 1000)
						break;
					bDelayed = true;
				}
				else
				{
					if(!RSmManager.isCalibrationInProgress())
					{
						Status = CAL_PASSED;
						if(bCalAll)
						{
							BdioCalType = CAL_PSOL_LIFTOFF;
						}
						else
						{
							ProgressCount = 0;
							BdioCalType = CAL_NONE;
							SendCalStatus(Status);
						}
						RETAILMSG(1, (L"Exh Flow Sensor Offset Calibration Completed\r\n"));
						bCalStarted = false;
						bDelayed = false;
					}
				}

				break;
			case CAL_PSOL_LIFTOFF:
				if(!bCalStarted)
				{
					if(ElapsedTickSum > 1000)
					{
						RETAILMSG(1, (L"Psol Liftoff Calibration Started\r\n"));
						bCalStarted = true;
						DelayCount = 0;
						Status = CAL_PENDING;
						ElapsedTickSum = 0;
					}
					else
						break;
				}
				else if(!bDelayed)
				{
					if(++DelayCount < 1000)
						break;
					bDelayed = true;
				}
				else
				{
					if(!RSmManager.isCalibrationInProgress())
					{
						RETAILMSG(1, (L"Psol Liftoff Calibration Completed\r\n"));
						if(bCalAll)
						{
							bCalAll = false;
							RETAILMSG(1, (L"All Calibrations Completed\r\n"));
						}
						ProgressCount = 0;
						BdioCalType = CAL_NONE;
						Status = CAL_PASSED;
						SendCalStatus(Status);
						bCalStarted = false;
						bDelayed = false;
					}
				}
				break;
			case CAL_ALL:
				bCalAll = true;
				ProgressCount = 0;
				BdioCalType = CAL_AIR_PSOL;
				break;

			default:
				break;
			}//switch(BdioCalType)

			if(BdioCalType != CAL_NONE)
			{
				if(++ProgressCount >= 50)
				{
					SendCalStatus(Status);
					ProgressCount = 0;
				}
			}
			break;
		default:
			/*DEBUGMSG(1, (L"Phase=%d,P1=%s,P2=%s,Pd=%s, Fexh=%s\r\n",
				BreathPhase::GetCurrentBreathPhase()->getPhaseType(), P1Str, P2Str, PdrvStr, FlowExhStr));*/
			//DEBUGMSG(1, (L"AirFlow=%s, O2Flow=%s, ExhFlow=%s\r\n", FlowAirStr, FlowO2Str, FlowExhStr));
			/*DEBUGMSG(1, (L"Ph=%d,P1=%s,P2=%s,Pd=%s,Fa=%s,Fo=%s,Fe=%s\r\n",
				BreathPhase::GetCurrentBreathPhase()->getPhaseType(),P1Str,P2Str,PdrvStr,FlowAirStr,FlowO2Str,FlowExhStr));*/
			break;
		} // switch((BdioTestCase)
	} // case BDIO_TEST_STATE5
	} // switch(TestState)

	RawA2dReadings.ExhaleFlow.Value = RExhFlowSensor.GetRawData();
	ConvertToEngineeringUnits(ConvertedData);
	SetCommandValues();

	if(BdioTestCase == BDIO_TEST_NONE)
	{
		EthernetBroadcastManager.collect(ConvertedData, RawA2dReadings, CommandValues);
		return false;
	}
	else
	{
		SendDebugPacket();
		return true;
	}
}


void BDIOTest_t::SendCalStatus(uint32_t Status)
{
	CalStatusMsg_t Msg;

	Msg.msgId = BD_CAL_STATUS_MSG_ID;
	Msg.pktSize = sizeof(Msg.calStatus);
	Msg.seqNumber = 0;
	Msg.calStatus = Status;
	Msg.CheckSum = 0;

	EthernetBroadcastManager.SendPacket((char *)&Msg, sizeof(CalStatusMsg_t));
}


void BDIOTest_t::ProcessRxSettingsPacket(char *pSettingsPacket)
{
	SettingsPacket_t *pPacket = (SettingsPacket_t *)pSettingsPacket;
	static bool bFirstPacket = true;

	PendingContext&  rPendingContext = ContextMgr::GetPendingContext();
	BdSettingValues  xmittedBdValues;
	Uint             idx;
	Uint			 numXmitElems = pPacket->NumOfSettings;

	xmittedBdValues = rPendingContext.getPendingValues();

	for ( idx = 0; idx < numXmitElems; idx++ )
	{	// always enters this branch...
		SettingId::SettingIdType  SettingId = (SettingId::SettingIdType)pPacket->Data[idx].settingId;

		if ( SettingId::IsBdBoundedId(SettingId) )
		{	// element is a bounded, BD setting...
			BoundedValue  newBoundedValue;

			// get the precision and value of the transmitted bounded setting
			// value identified by 'SETTING_ID' (and 'idx')...
			newBoundedValue.precision = (Precision)pPacket->Data[idx].boundedPrec;
			newBoundedValue.value = pPacket->Data[idx].boundedValue;

			xmittedBdValues.setBoundedValue(SettingId, newBoundedValue);
		}
		else if ( SettingId::IsBdDiscreteId(SettingId) )
		{	// element is a discrete, BD setting...
			xmittedBdValues.setDiscreteValue(SettingId,(DiscreteValue)( pPacket->Data[idx].discreteValue));
		}
		else
		{
			DEBUGMSG(1, (L"Setting ID Invalid!!!\r\n"));
		}  // end of if...
	}  // end of for...

	// forward the transmitted setting values to the Pending Context...
	if(bFirstPacket)
		rPendingContext.processTransmission(VENT_STARTUP_UPDATE_XMITTED, xmittedBdValues);
	else
		rPendingContext.processTransmission(EXTERNAL_UPDATE_XMITTED, xmittedBdValues);

	// a transaction has completed, therefore set the Sys-Init Complete
	// Flag...
	Post::SetSysInitComplete(TRUE);
}


#include "PhasedInContextHandle.hh"
extern void SendMsg(char *pMsg, int Size);

void BDIOTest_t::ProcessTxSettingsPacket(char *pSettingsPacket)
{
	SettingsRequestPacket_t *pRxPacket = (SettingsRequestPacket_t *)pSettingsPacket;
	SettingsReplyPacket_t Packet;
	Uint idx;

	Packet.MsgId = pRxPacket->MsgId;
	Packet.NumOfSettings = NUM_TEST_SETTING_IDS;

	for ( idx = 0; idx < NUM_TEST_SETTING_IDS; idx++ )
	{
		SettingId::SettingIdType Id = (SettingId::SettingIdType)SettingIdMap[idx];

		if ( SettingId::IsBdBoundedId(Id) )
		{
			Packet.Data[idx].boundedValue = PhasedInContextHandle::GetBoundedValue(Id);
		}
		else
		{
			Packet.Data[idx].discreteValue = PhasedInContextHandle::GetDiscreteValue(Id);
		}
	}

	EthernetBroadcastManager.SendPacket((char *)&Packet, sizeof(SettingsReplyPacket_t));
}


void BDIOTest_t::ProcessRxSettingsDebugPacket(char *pSettingsPacket)
{
	SettingsDebugPacket_t *pPacket = (SettingsDebugPacket_t *)pSettingsPacket;
	RVcvPhase.setCktCompFactor(pPacket->Data[COMPLIANCE_ID].boundedValue);
}

//*****************************************************************************
//
//  BDIOTest_t::newCycle()
//
//*****************************************************************************

bool BDIOTest_t::newCycle()
{
    return ProcessBdioTest();
}

