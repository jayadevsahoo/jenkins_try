#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Nellcor Puritan Bennett Inc of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Nellcor Puritan Bennett Inc of California.
//
//            Copyright (c) 1996, Nellcor Puritan Bennett Inc
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: TimeMonitor - Contains BD 5 ms clock check code.
//---------------------------------------------------------------------
//@ Interface-Description
//---------------------------------------------------------------------
//@ Rationale
//  This class contains the data and collection/access methods of TimeMonitor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  TimeMonitor::newCycle is called once per Breath-Delivery cycle by the
//  Breath-Delivery subsystem. 
//---------------------------------------------------------------------
//@ Fault-Handling
//  Standard fault handling macros are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Safety-Net/vcssrc/TimeMonitor.ccv   27.0.1.0   20 Nov 2013 17:31:38   rhjimen  $
//
//@ Modification-Log
//
//  Revision: 001  By: rpr    Date:  6-July-2011    SCR Number: 6765
//  Project:  BI
//  Description:
//      Initial revision.   Got rid of BdMonitorClass and kept the 5 ms time check
//      test testFiveMsTimer_.
//
//=====================================================================

#include "Background.hh"
#include "TimeMonitor.hh"
#include "CpuDevice.hh"

//@ Usage-Classes
//@ End-Usage
//@ Code...

Uint PTimeMonitor [(sizeof(TimeMonitor) + sizeof(Uint) - 1) / 
					  sizeof(Uint)];
TimeMonitor& RTimeMonitor =
*((TimeMonitor*)PTimeMonitor);


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TimeMonitor [Default constructor]
//
//@ Interface-Description
//    Default constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//    Initializes private data.
//---------------------------------------------------------------------
//@ PreCondition
//    None
//---------------------------------------------------------------------
//@ PostCondition
//    None
//@ End-Method
//=====================================================================

TimeMonitor::TimeMonitor(void)

{
    CALL_TRACE("TimeMonitor::TimeMonitor(void)");

    prevCounterValue_ = 0;
    isXena2Config_ = CpuDevice::IsXena2Config();
	isFirstTime_ = TRUE;

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~TimeMonitor [Destructor]
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//    None
//---------------------------------------------------------------------
//@ PostCondition
//    None
//@ End-Method
//=====================================================================

TimeMonitor::~TimeMonitor(void)
{
    CALL_TRACE("TimeMonitor::~TimeMonitor(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newCycle
//
//@ Interface-Description
//    This method takes no parameters and returns void.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The test the 5 ms cycle timer:
//  
//---------------------------------------------------------------------
//@ PreCondition
//    None
//---------------------------------------------------------------------
//@ PostCondition
//    None
//@ End-Method
//=====================================================================
void
TimeMonitor::newCycle(void)
{
    if ( isXena2Config_ )
    {
        testFiveMsTimer_();
    }

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: testFiveMsTimer_ [private]
//
//@ Interface-Description
//  This method accepts no parameters and returns none. It tests the
//  5ms interrupt timer against the 32-bit counter provided in the FPGA. 
//  The FPGA counter updates every 5ms by 1. 
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method performs a cycle-to-cycle test every interrupt cycle 
//  (5ms) that verifies the 5ms timer had not changed more 
//  than CLK_VARIANCE_THRESHOLD_MS in ms.
//
//  This method uses the AUX_CLASS_ASSERTION_FAILURE macro to log the 
//  elapsed time in question and reset the CPU to rerun POST. If POST
//  detects a clock variance that cannot be corrected with a reset and
//  reinitialization of the timers, it will assert a MAJOR POST failure
//  and not allow ventilation.
//---------------------------------------------------------------------
//@ PreCondition
//    None
//---------------------------------------------------------------------
//@ PostCondition
//    None
//@ End-Method
//=====================================================================
void
TimeMonitor::testFiveMsTimer_(void)
{
    static const Uint32 CLK_VARIANCE_THRESHOLD_MS = 40;
    static const Uint32 CLK_VARIANCE_UPPER_LIMIT_MS = 250;
    static const Uint32 FPGA_CTR_CYCLE_TIME_MS = 5;
    static const Uint32 CLK_VARIANCE_THRESHOLD_CYCLES = (CLK_VARIANCE_THRESHOLD_MS / FPGA_CTR_CYCLE_TIME_MS);
    static const Uint32 CLK_VARIANCE_LOWER_LIMIT_CYCLES = 0;
    static const Uint32 CLK_VARIANCE_UPPER_LIMIT_CYCLES = (CLK_VARIANCE_UPPER_LIMIT_MS / FPGA_CTR_CYCLE_TIME_MS);
    static const Uint32 FPGA_CTR_MAX_VALUE = Uint32(0xFFFFFFFF);

#ifdef E600_840_TEMP_REMOVED
    Uint32 currCounterValue = *FPGA_CTR;
#endif
Uint32 currCounterValue = 0;

	if (isFirstTime_)
	{
		isFirstTime_ = FALSE;
		prevCounterValue_ = currCounterValue;
	}

	Uint32 elapsedCycles;

	if (currCounterValue < prevCounterValue_)
	{
		// rollover occurs on 249th day
		elapsedCycles = (FPGA_CTR_MAX_VALUE - prevCounterValue_ + 1) + currCounterValue;
	}
	else
	{
		elapsedCycles = currCounterValue - prevCounterValue_;
	}

	// perform cycle-to-cycle variance test
	if (elapsedCycles < CLK_VARIANCE_LOWER_LIMIT_CYCLES || 
		elapsedCycles > CLK_VARIANCE_UPPER_LIMIT_CYCLES)
	{
		// $[XB06000] cycle variance less than 0 or greater than 250ms
		Background::ReportBkEvent(BK_BD_TOD_FAIL, Uint16(elapsedCycles * FPGA_CTR_CYCLE_TIME_MS) );
	}
	else if (elapsedCycles >= CLK_VARIANCE_THRESHOLD_CYCLES)
	{
		// $[XB06001] cycle time exceeds 40ms - reset in attempt to clear condition
		AUX_CLASS_ASSERTION_FAILURE(elapsedCycles * FPGA_CTR_CYCLE_TIME_MS);
	}

	prevCounterValue_ = currCounterValue;

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
TimeMonitor::SoftFault(const SoftFaultID  softFaultID,
                           const Uint32       lineNumber,
                           const char*        pFileName,
                           const char*        pPredicate)
{
    CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
    FaultHandler::SoftFault(softFaultID, SAFETY_NET, TIMEMONITOR,
                            lineNumber, pFileName, pPredicate);
}
