#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//=====================================================================
//@ Class: Delay - Delay function provided to delay a specified time
//                 based on an external clock source.
//---------------------------------------------------------------------
//@ Interface-Description
//  The public interface for this class is the WaitForTicks method that
//  waits for the specified number of 10ms transitions of the clock or
//  counter before returning to the caller.  
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description.
//---------------------------------------------------------------------
//@ Rationale
//  This class encapsulates functions related to delaying a specified 
//  number of ticks from an external clock source.
//---------------------------------------------------------------------
//@ Fault-Handling
//  Standard fault handling mechanisms apply.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//    None
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/POST-Library/vcssrc/Delay.ccv   25.0.4.0   19 Nov 2013 14:16:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004   By: Gary Cederquist  Date: 27-Jan-2011  SCR Number: 6671
//  Project: XENA2
//  Description:
//      Corrected implementation of WaitForTicks for the Xena2 counter to 
//      account for rollover and correct the loop count for a non-responsive
//      clock. Separated the pre-Xena2 and Xena2 code into separate 
//      functions.
//
//  Revision: 003   By: Mitesh Raval   Date: 14-Oct-2010  SCR Number: 6671
//  Project: XENA2
//  Description:
//       Class DS1286 renamed to Delay. WaitForTicks() updated for Xena II
//       config to use FPGA counter.
//       WaitForTicks() updated for Xena2 version RTC (BQ4822Y) with 
//       backwards compatibility maintained.
//       New struct BQ4822YRegMap added for Xena2 version RTC.
//
//  Revision: 002   By: Gary Cederquist  Date: 15-AUG-1997  DR Number: 1875
//    Project:   Sigma   (R8027)
//    Description:
//       Added testable item annotations.
//
//  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//=====================================================================


#include "Delay.hh"
#include "MemoryMap.hh"
#include "CpuDevice.hh"
#include "DS1286.hh"
#include "STM48T58.hh"

//@ Usage-Classes
//@ End-Usage


//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method: WaitForTicks
//
//@ Interface-Description
//  Waits the specified number of 10ms ticks of the external system
//  clock or a minimum of 10ms per tick specified if the clock is not
//  functioning. Returns TRUE if the external clock provided the 
//  tick transitions, FALSE if the clock failed to transition during 
//  the wait period. This method assumes interrupts are blocked to 
//  accurately measure or approximate the specified delay.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses CpuDevice::IsXena2Config to determine the type of clock used
//  for measuring delays on this board. Passes the number of ticks
//  to the appropriate private method to wait for the specified number
//  of transitions of the clock. 
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
Boolean
Delay::WaitForTicks(Uint32 numberTicks)
{
    if ( CpuDevice::IsXena2Config() )
    {
        // Xena2 uses a 5ms counter so double the number of ticks
        return Delay::Xena2WaitForTicks_(2*numberTicks);
    }
    else
    {
        return Delay::DS1286WaitForTicks_(numberTicks);
    }
}

//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method: DS1286WaitForTicks - wait for Time of Day Ticks on DS1286
//
//@ Interface-Description
//
// This method delays for the specified number of clock ticks of the 
// DS1286.  A tick is defined as a change in the 1/100th second register
// of the Dallas Semiconductor DS1286 Real Time Clock.
//
// Warning: The DS1286 does not provide a true 10 millisec count in
//          the 1/100ths sec register.  The count is an "average" 100 Hz.
//          The time between updates of the 1/100th sec register are
//          usually 10 millisec, but occasionally one is short by
//          .24 milliseconds.
//
//
//    Inputs:  Uint32 numberTicks - the number of ticks to wait.
//
//             Note: A value of 0 will return immediately. A value
//                   of 1 will return on the next transistion and
//                   could therefore cause a delay from 0 to 10 mSec.
//
//    Outputs: NONE
//
//    Returns: FALSE - time out waiting for Timer of Day Tick
//             TRUE  - saw the desired number of ticks
//
//---------------------------------------------------------------------
//@ Implementation-Description
//
// A pointer to the 1/100ths second register in the time of day clock
// is created.  If the requested tick count is 0, the function returns
// immediately. Otherwise the pointer is used to take a snapshot of the
// current value of the 1/100ths second register.  That snapshot is compared
// to the 1/100ths second register using the pointer until a change is
// seen. There is a loop counter that, if it counts down to zero, indicates
// that the Timer of Day clock is not counting properly and the function
// returns FALSE to indicate that the delay requested failed. If a
// change is seen before the loop count reaches zero, the requested number
// of ticks is decremented and if it becomes zero the function returns 
// TRUE to indicate that the requested delay was performed.
//
//  The number of times through the loop before declaring a clock
//  failure is based on a number derived from measurements made with
//  the AMC emulator.  The number measured for cached code is 8896 
//  per 10ms on a 24MHz processor.  A larger number guarantees at least 
//  10ms per tick have elapsed when TOD clock has failed.  This number 
//  is processor dependent and not portable.  
//---------------------------------------------------------------------
//@ PreCondition
//    None
//---------------------------------------------------------------------
//@ PostCondition
//    None
//
//@ End-Method
//=====================================================================

Boolean
Delay::DS1286WaitForTicks_(Uint32 numberTicks)
{
#ifdef E600_840_TEMP_REMOVED
    static const Uint32 TIME_OF_DAY_TICK_LOOP_COUNT = 10000;
    Boolean todSuccess = TRUE;

    volatile const DS1286RegMap * P_DS1286 = (DS1286RegMap*)DS1286_BASE;

    // ****************************************************************
    // **** note:      any change in the byte that contains the    ****
    // ****            1/100th sec bit field will be a tick        ****
    // ****************************************************************
    while (numberTicks--)
    {
        // $[TI1.1]
        register Uint8 test_value = P_DS1286->second_hundreths;

		//TODO E600 took out declaration of i out of the for loop
		// to get the code to compile
		Uint32 i;
        for (  i=TIME_OF_DAY_TICK_LOOP_COUNT
               ; i && (test_value == P_DS1286->second_hundreths)
            ; i--)
        {
            // do nothing
        }

        if ( !i )
        {
            // $[TI2.1]
            todSuccess = FALSE;
        }
        // $[TI2.2]
    }
    // $[TI1.2]

    return todSuccess;
#endif
	return false;
}


//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method: Xena2WaitForTicks - wait for ticks of Xena2 counter
//
//@ Interface-Description
//
// This method delays for the specified number of 5ms ticks of the 
// Xena2 counter based on the board's Ethernet crystal oscillator.
//
//    Inputs:  Uint32 numberTicks - the number of ticks to wait.
//
//             Note: A value of 0 will return immediately. A value
//                   of 1 will return on the next transistion and
//                   could therefore cause a delay from 0 to 5 mSec.
//
//    Outputs: NONE
//
//    Returns: FALSE - time out waiting for counter transition
//             TRUE  - saw the desired number of ticks
//
//---------------------------------------------------------------------
//@ Implementation-Description
//
// If the requested tick count is 0, the function returns immediately. 
// Otherwise the pointer is used to take a snapshot of the current 
// value of the counter.  That snapshot is compared to the counter 
// until a change is seen. There is a loop counter that, if it counts i
// down to zero, indicates that the counter is not counting properly 
// and the function returns FALSE to indicate that the delay i
// requested failed. If a change is seen before the loop count reaches 
// zero, the requested number of ticks is decremented and if it becomes
// zero the function returns TRUE to indicate that the requested 
// delay was performed.
//
//  The number of times through the loop before declaring a clock
//  failure is based on a number derived from measurements made with
//  the AMC emulator.  The number measured for cached code is 8896 
//  per 10ms on a 24MHz processor.  A larger number guarantees at least 
//  10ms per tick have elapsed when TOD clock has failed.  This number 
//  is processor dependent and not portable.
//---------------------------------------------------------------------
//@ PreCondition
//    None
//---------------------------------------------------------------------
//@ PostCondition
//    None
//
//@ End-Method
//=====================================================================
Boolean
Delay::Xena2WaitForTicks_(Uint32 numberTicks)
{
#ifdef E600_840_TEMP_REMOVED
    // 5ms minimum loop delay
    static const Uint32 TIME_OF_DAY_TICK_LOOP_COUNT = 5000;
    Boolean todSuccess = TRUE;

    while (numberTicks--)
    {
        // $[TI1.1]
        register Uint32 test_value = *FPGA_CTR;

		//TODO E600 took out declaration of i out of the for loop
		// to get the code to compile
		Uint32 i;
        // look for transition in clock value which is 5ms
        for ( i=TIME_OF_DAY_TICK_LOOP_COUNT
              ; i && ( (*FPGA_CTR == test_value) )
            ; i--)
        {
            // do nothing
        }

        if ( !i )
        {
            // $[TI2.1]
            todSuccess = FALSE;
        }
        // $[TI2.2]
    }
    // $[TI1.2]

    return todSuccess;
#endif
	return false;
}
