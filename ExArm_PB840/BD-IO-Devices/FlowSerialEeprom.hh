#if defined( SIGMA_BD_CPU)

#ifndef FlowSerialEeprom_HH
#define FlowSerialEeprom_HH

//=======================================================================
// This is a proprietary work to which Puritan-Bennett Corporation
// claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or,
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or, otherwise with the prior written permission of
// Puritan-Bennett Corporation.
//
//		Copyright (c) 1995, Puritan-Bennett Corporation
//
//=======================================================================


//=======================================================================
// Class:  FlowSerialEeprom - Interface to access the flow sensor calibration
//		data through the serial eeprom.
//-----------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/FlowSerialEeprom.hhv   26.0.1.0   04 Feb 2013 10:36:40   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005   By: gdc    Date: 26-Jan-2009    SCR Number: 6407
//  Project:  840S
//  Description:
//      Modified to bring source into strict ARM compliance.
//
//  Revision: 004  By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 003 By: syw    Date: 06-Apr-1998   DR Number: DCS 5065
//  	Project:  Sigma (R8027)
//		Description:
//			Read whole serial eeprom in computation of checksum.  Added a
//			check to determine if a new revised flow sensor was read.  Read
//			serial number as a 10 digit serial number.
//
//  Revision: 002  By: syw    Date:  19-Dec-1996    DR Number: DCS 1641
//  	Project:  Sigma (R8027)
//		Description:
//			Change constructor since we no longer have a rSerialReadWritePort but
//			we now have rSerialReadPort and rSerialWritePort.
//
//  Revision: 001  By:  syw    Date: 08-Nov-1996    DR Number: INITIAL RELEASE
//		Project:  Sigma (R8027)
//		Description:
//			Initial version (Integration baseline).
//
//=======================================================================
#include "Sigma.hh"
#include "FlowSensorCalInfo.hh"

//@ Usage-Classes

class FlowSensorCoeffEntry ;

//@ End-Usage

//@ Constant: CHECKSUM_ADDRESS
// address of where the checksum data is stored in the eeprom
static const Uint8 CHECKSUM_ADDRESS                = 0;

//@ Constant: SERIAL_NUMBER_ADDRESS
// address of where the serial number data is stored in the eeprom
static const Uint8 SERIAL_NUMBER_ADDRESS			= 2;

static const Uint8 MODEL_ADDRESS					= 6;
static const Uint8 REVISION_ADDRESS					= 10;
static const Uint8 CAL_DATE_ADDRESS					= 12;
static const Uint8 K0_FACTOR_ADDRESS                = 16;
static const Uint8 K1_FACTOR_ADDRESS                = 20;
static const Uint8 K2_FACTOR_ADDRESS                = 24;
static const Uint8 K3_FACTOR_ADDRESS                = 28;
static const Uint8 K4_FACTOR_ADDRESS                = 32;
static const Uint8 T_CAL_ADDRESS       			    = 36;
static const Uint8 SPAN_ADDRESS       			    = 40;
static const Uint8 ZERO_ADDRESS       			    = 44;
static const Uint8 TEMPERATURE_CORRECTION_ADDRESS	= 48;
static const Uint8 NUM_COEFFS_ADDRESS				= 62;
static const Uint8 BEGINNING_COEFF_ADDRESS          = 64;

//@ Constant: CHECKSUM_RANGE
// error code for checksum out of range
static const Uint32 CHECKSUM_RANGE = 0x0001 ;

//@ Constant: AIRCOEFF_RANGE
// error code for the number of air coefficients out of range
static const Uint32 AIRCOEFF_RANGE = 0x0002 ;

//@ Constant: O2COEFF_RANGE
// error code for the number of o2 coefficients out of range
static const Uint32 O2COEFF_RANGE  = 0x0004 ;

//@ Constant: CHECKSUM_EQUAL
// error code for checksums not agreeing
static const Uint32 CHECKSUM_EQUAL = 0x0008 ;

//@ Constant: NEW_REVISED_SENSOR
// error code for detecting new revised flow sensor
static const Uint32 NEW_REVISED_SENSOR = 0x0010 ;

typedef struct coeff
{
	Real32	vf;						/* Vf coefficient */
	Real32	a;						/* A coefficient */
	Real32	b;						/* B coefficient */
	Real32	c;						/* C coefficient */ 
}COEFF_STRUCT;

typedef struct TsiTableStruct	/* 260 bytes */
{								/* address offset */
	Uint16	 	crc;		/* 0 */
	Uint16		padWord1;	/* 2 */
	Uint32 		serial;		/* 4 */
	Uint32 		model;		/* 8 */
	Uint8 		rev;		/* 12 */
	Uint8 		padByte1;	/* 13 */
	Uint16 		calYear;	/* 14 */
	Uint8 		calMon;		/* 16 */
	Uint8 		calDate;	/* 17 */
	Uint16		padWord2;	/* 18 */
	Real32		kFactor[NUM_K_FACTORS];	/* 20 */
	Real32  	tcal;		/* 40 */
	Real32 		span; 		/* 44 */
	Real32 		zero; 		/* 48 */
	Real32		tempCorr;	/* 52 */
	Uint16    	pad[5];		/* 56, size = 10 bytes */
	Uint8		noCoeff; 	/* 66 */
	Uint8 		padByte2;	/* 67 */
	COEFF_STRUCT	coeff[MAX_FLOW_SENSOR_COEFF_SETS];	/* 68, size = 16 x 12 = 192 bytes */
}TSI_TABLE_STRUCT;

class FlowSerialEeprom
{
  public:

  	enum SetType { AIR_SET, O2_SET, INVALID_SET} ;

   	FlowSerialEeprom( SetType setType) ;
	FlowSerialEeprom() ;

   	~FlowSerialEeprom( void ) ;

   	static void	SoftFault( const SoftFaultID softFaultID,
							    const Uint32      lineNumber,
							    const char*       pFileName = NULL,
							    const char*       pPredicate = NULL ) ;

	Uint16 readChecksum( void);
    Uint32 readSerialNumber( void);
	Boolean IsReadError() { return bReadError_; }
    
	Uint32 readFlowSensorEeprom( FlowSensorCalInfo *calInfo, const SetType setType) ;

  protected:

  private:

	void readFlowSensorTable_( );
	Uint16 calcCRC_(Uint8 *pData, Uint32 size);
	Boolean validateChecksum_(Uint8 *pTable, Uint32 size);
	void printTsiTable_(TSI_TABLE_STRUCT *pTsiTab, LPCTSTR lpRamFileName, LPCTSTR lpSdFileName);

	TSI_TABLE_STRUCT		tsiTable_ ;
	SetType					tableType_ ;
	Boolean					bReadError_ ;
	Boolean					bTableRead_ ;
	Uint32					errorStatus_ ;      // If this is 0, everything is ok
};

#endif // FlowSerialEeprom_HH


#endif // defined( SIGMA_BD_CPU)
