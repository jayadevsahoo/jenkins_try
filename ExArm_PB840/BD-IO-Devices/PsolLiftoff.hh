#ifndef PsolLiftoff_HH
#define PsolLiftoff_HH
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: PsolLiftoff - Proportional solenoid Liftoff class.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/PsolLiftoff.hhv   25.0.4.0   19 Nov 2013 13:54:10   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By: quf    Date: 26-Dec-2000   DR Number: 5832
//  Project:  Baseline
//  Description:
//		Added methods operator+() and operator/() to allow PSOL
//		liftoff calibration averaging, checkPsolTables() to check
//		for valid air and O2 PSOL tables.
//
//  Revision: 003  By: syw    Date: 02-Mar-2000   DR Number: 5684
//  Project:  NeoMode
//  Description:
//      Added code to store psol lookup table.
//
//  Revision: 002  By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 001  By:  syw    Date: 08-Nov-1996    DR Number: INITIAL RELEASE
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "Sigma.hh"
#include "BD_IO_Devices.hh"

//@ Usage-Classes
//@ End-Usage

const Real32 FLOW_INCREMENT = 0.05 ;			// lpm
const Real32 INVERSE_FLOW_INCREMENT = 20.0 ;	// 1 / FLOW_INCREMENT
const Real32 MAX_PSOL_LOOKUP_FLOW = 0.3 ; 		// lpm
const Uint32 PSOL_TABLE_SIZE = 6 ;				// 6 = MAX_PSOL_LOOKUP_FLOW / FLOW_INCREMENT


class PsolLiftoff 
{
  public:
    PsolLiftoff( void) ;
    ~PsolLiftoff( void) ;

    enum PsolSide {AIR_SIDE, O2_SIDE} ;
    
    PsolLiftoff( const PsolLiftoff&) ;
    void operator=( const PsolLiftoff&) ;
    PsolLiftoff operator+( const PsolLiftoff&) const;
	PsolLiftoff operator/( const Uint32) const;

    static void SoftFault( const SoftFaultID softFaultID,
						   const Uint32      lineNumber,
						   const char*       pFileName  = NULL, 
						   const char*       pPredicate = NULL) ;

	inline Uint16 getAirPsolLiftoff (void);
	inline Uint16 getO2PsolLiftoff (void);

	inline void setAirPsolLiftoff (Uint16 airLiftoff);
	inline void setO2PsolLiftoff (Uint16 o2Liftoff);

	inline void setAirPsolTable( const Uint16 index, const Uint32 value) ;
	inline void setO2PsolTable( const Uint16 index, const Uint32 value) ;

	Uint32 lookupPsolCommand( const Real32 flow, const PsolSide side) ;

	void checkPsolTables( void);

#if defined(GRAPHICS)
	friend class FsCrossCheckTest;
#endif  // defined(GRAPHICS)

  protected:

  private:

	//@ Data-Member: airLiftoff_
	// air liftoff value
	Uint16 airLiftoff_;

	//@ Data-Member: o2Liftoff_
	// o2 liftoff value
	Uint16 o2Liftoff_;

	//@ Data-Member: airPsolTable_[PSOL_TABLE_SIZE]
	// storage for air psol characterization
	Uint32 airPsolTable_[PSOL_TABLE_SIZE] ;

	//@ Data-Member: o2PsolTable_[PSOL_TABLE_SIZE]
	// storage for o2 psol characterization
	Uint32 o2PsolTable_[PSOL_TABLE_SIZE] ;

} ;

// Inlined methods
#include "PsolLiftoff.in"

#endif // PsolLiftoff_HH 
