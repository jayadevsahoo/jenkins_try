#include "stdafx.h"
//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1995, Puritan-Bennett Corporation
//====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: Container - A rectangular object capable of holding Drawables.
//---------------------------------------------------------------------
//@ Interface-Description
// This class serves as a container for Drawables.
//
// The Container class encapsulates a screen area, and a list of
// contained Drawable objects.  The Container's area is filled with its
// fill color, serving as a background for its contained drawables.  The
// upper left corner of this area serves as the origin for specifying the
// position of its Drawables on the screen.  Changing the position of
// the Container shifts this origin, and thus shifts the screen location
// of all contained Drawables.
//
// This class inherits from Drawable.  All of the parts of the interface
// relating to size and position of the Container are defined in
// Drawable.  Since a Container is a Drawable, a Container can contain
// Containers.
//
// Drawable objects are appended to a Container by calling drawable's
// appendTo() methods.  Drawable objects are removed from a Container by
// calling their removeFromParent() methods.  Drawable objects can also
// be added to a Container by calling the Container's addDrawable()
// method, passing a pointer to the Drawable as a parameter.  Similarly,
// Drawables can be removed from a Container by calling the Container's
// removeDrawable() method.
//
// A Container also has the ability to determine which of its contained
// touch-sensitive Drawables has been touched.  This is invoked by calling the
// Container's findTouchedDrawable() method.  This search is done only if the
// Container object itself is currently non-touch-sensitive; otherwise, the
// findTouchedDrawable() method reports whether the touch is anywhere within
// the Container's area.
//
// A Container object's fill color is set by calling its setFillColor()
// method.  This sets the color of the Container's background.  The
// current fill color can be queried by calling the Container's
// getFillColor() method.  The setContentsColor() method sets the color
// of all contained Drawables.  The virtual Container::setColor() method
// is redefined to do nothing, thus redefining the default behavior
// inherited from Drawable::setColor().  This is done because a Container
// has no "foreground" color.
//---------------------------------------------------------------------
//@ Rationale
// The Container class provides the primary framework for screen
// management.  It allows construction of an hierarchical organizational
// structure for Drawable screen objects that is needed both for
// determining which areas of the screen need updating during screen
// repaints, and for determining which screen object has been touched.
// The Container class provides the ability to display, move, or erase
// an entire area of the screen at once.
//---------------------------------------------------------------------
//@ Implementation-Description
// Container's draw() method first draws the portion of its background which
// overlaps with the given clip area, using the fill color, unless the fill
// color is NO_COLOR or this area is completely obscured by opaque contained
// Drawables.  It then calls the draw() method of each contained Drawable that
// overlaps with the clip area which it was given.
//
// The real clip area used by the DisplayContext::setClip() is the intersection
// between the container area and the clip area passed into the Container's
// draw() method.
//
// Container's findTouchedDrawable() method determines which of its contained
// touch-sensitive Drawables has been touched, provided that the Container is
// not itself touch-sensitive.  It checks each of the Container's
// touch-sensitive Drawables to see if it overlaps with the "touch area", which
// is a small region surrounding the touched point.  In addition, it calls the
// findTouchedDrawable() methods of all contained non-touch-sensitive
// Containers, thus implementing a recursive search.
//
// If the Container IS touch-sensitive, the findTouchedDrawable() method
// simply determines if the area of the Container has been touched.
//
// Drawable objects are appended to a Container using the appendTo()
// method of Drawable.  This method in turn calls the private
// setChild_() method of Container, which updates the Container's list
// of children.  This is possible because Container grants friendship to
// the appendTo() method of Drawable.  A Drawable object can be removed
// from a Container by calling its removeFromParent() method, which
// calls the private removeChild_() method of Container.
//
// Drawable objects can also be added to a Container by calling the
// Container's addDrawable() method, passing a pointer to the Drawable
// as a parameter.  This method simply calls the Drawable's appendTo()
// method.  Similarly, Drawables can be removed from a Container by
// calling the Container's removeDrawable() method.
//
// Note that contained objects are not required to fit within the Container's
// defined area.  Despite this, if any of the contained Drawables extend
// outside the Container, problems may arise during repainting.  For example,
// suppose that a repaint is in progress, and a given Drawable overlaps the
// clip area during one of the calls to draw() (see BaseContainer::repaint()).
// Normally the portion of the Drawable overlapping the clip area would be
// drawn; however, if the Drawable is a child of a Container and extends
// outside that Container, and the Container does NOT overlap the clip area,
// then the Drawable will not be redrawn.  This happens because the checking
// for overlap is done via the containment tree; the overlapping Drawable's
// draw() method is only called from its parent Container's draw().  But in
// this case, the parent Container does not overlap the clip area, so its
// draw() is not called.
//---------------------------------------------------------------------
//@ Fault-Handling
// This class uses the "contract programming" technique.  This technique
// utilizes the class assertion and class precondition macros provided by
// the Foundation  subsystem to generate error messages in response to software
// faults.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Foundations/vcssrc/Container.ccv   25.0.4.0   19 Nov 2013 14:11:40   pvcs  $
//
//@ Modification-Log
//
//  Revision: 011   By: gdc    Date:  21-May-2007   SCR Number: 6330
//  Project: Trend
//  Description:
//	Removed SUN prototype code.
//
//  Revision: 010   By: gdc    Date:  25-Jan-2007    SCR Number: 6237
//  Project: Trend
//  Description:
//	Changed to accomodate derived classes with different sized 
//  Drawable lists.
//  
//  Revision: 009   By: gdc    Date:  9-Oct-2000    DCS Number: 5493
//  Project: GuiComms
//  Description:
//	Added Single Screen Compatibility.
//
//  Revision: 008   By: gdc    Date:  29-Jun-1999    DCS Number: 5337
//  Project: 840 Neonatal
//  Description:
//	Optimized VGA library for Neonatal project to use BitBlt and Windows
//  Bitmap structures.
//
//  Revision: 007   By: sah    Date: 11-Jan-1999   DR Number: 5321
//  Project:  Sigma (R8027)
//  Description:
//      Changed 'SoftFault()' method to non-inline method.
//
//  Revision: 006  By: gdc     Date: 16-Jun-1998     DCS Number:
//  Project:  Color
//  Description:
//      Initial release.
//
//  Revision: 005  By: sah       Date: 06-Oct-1997    DCS Number: 2264
//  Project:  Sigma (R8027)
//  Description:
//      Fixed incorrect handling of VGA access requests/releases, which
//      may have caused the problem with the errant lines outside of the
//      waveform grid, by removing all semaphore requests/releases from
//      all of the 'draw()' methods.
//
//  Revision: 004  By: sah       Date: 12-Jun-1997    DCS Number: 2208
//    Project:  Sigma (R8027)
//    Description:
//      Incorporated new VGA interface methods, files and types.
//
//  Revision: 003  By: mpm      Date: 29-Mar-1996  DCR Number: 473
//    Project:  Sigma (R8027)
//    Description:
//      Added calls to GuiFoundation::RequestDisplayAccess() and ReleaseDisplayAccess()
//      in order to allow shared access to the display by separate drawing tasks.
//
//  Revision: 002  By: gdc      Date: 15-Feb-1996  DCR Number: 981
//    Project:  Sigma (R8027)
//    Description:
//      Corrected minor syntax errors detected by new compiler.
//
//  Revision: 001  By: yyy      Date: 01-Jan-1995  DR Number: NONE
//    Project:  Sigma (R8027)
//    Description:
//	Integration baseline.
//=====================================================================

#include "Container.hh"
#include "BaseContainer.hh"

#include "GraphicsContext.hh"
#include "DisplayContext.hh"

//@ Usage-Classes
#include "SIterR_Drawable.hh"
//@ End-Usage

//@ Code...


//=====================================================================
//
// Public methods
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Container
//
//@ Interface-Description
// Default constructor which initializes a Container object.
//---------------------------------------------------------------------
//@ Implementation-Description
// Initializes all local data members. The color_ data member inherited
// from Drawable is changed from default BLACK to NO_COLOR.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Container::Container(void) :
	rChildren_(children_),
	fillColor_(Colors::NO_COLOR),
	contentsColor_(Colors::NO_COLOR)
{
	CALL_TRACE("Container::Container(void)");

	//														$[TI1]
	//
	// Initialize inherited protected data members.
	// NOTE: Container object never uses the color_ data member.  
	//
	color_ = Colors::NO_COLOR;

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~Container [virtual]
//
//@ Interface-Description
//  Destroy this Container.
//---------------------------------------------------------------------
//@ Implementation-Description
//  N/A.  This will cause class assertion.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Container::~Container(void)
{
	CALL_TRACE("Container::~Container(void)");


}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: addDrawable
//
//@ Interface-Description
// Appends the Drawable pointed to by the passed "pDrawable" to this Container.
//
// This method calls the appendTo() method of the Drawable pointed to by the
// passed pDrawable parameter.
//---------------------------------------------------------------------
//@ Implementation-Description
// This method calls the appendTo() method of the Drawable pointed to by the
// passed pDrawable parameter.
//
// Note: this is the preferred method for adding a Drawable to a container; the
// private setChild_() method should NOT be used.  It is only to be called from
// the Drawable::appendTo() method; this is possible because the Container
// class grants friendship to Drawable::appendTo().
//---------------------------------------------------------------------
//@ PreCondition
// The given "pDrawable" pointer must not be NULL.
//---------------------------------------------------------------------
//@ PostCondition
//@ End-Method
//=====================================================================
void
Container::addDrawable(Drawable* pDrawable)
{
	CALL_TRACE("Container::addDrawable(Drawable *)");
	AUX_CLASS_PRE_CONDITION(pDrawable != NULL, 0);

	//															$[TI1]
	pDrawable->appendTo(this);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: removeDrawable
//
//@ Interface-Description
// Removes the Drawable pointed to by the passed "pDrawable" from this
// Container.
//---------------------------------------------------------------------
//@ Implementation-Description
// Just calls Drawable::removeFromParent().  If pDrawable points to a
// Drawable that has no parent, then the call does nothing.
//---------------------------------------------------------------------
//@ PreCondition
// The given "pDrawable" pointer must not be NULL.
//---------------------------------------------------------------------
//@ PostCondition
//@ End-Method
//=====================================================================
void
Container::removeDrawable(Drawable* pDrawable)
{
	CALL_TRACE("Container::removeDrawable(Drawable *)");
	CLASS_PRE_CONDITION(pDrawable != NULL);

	if (pDrawable->getParentContainer() == this)
	{	// $[TI1]
		pDrawable->removeFromParent();
	}	// $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: removeAllDrawables
//
//@ Interface-Description
// Removes the ALL the children drawables from this container.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Loop through the list of contained Drawables (children),
// and remove each one from the Container.
//---------------------------------------------------------------------
//@ PreCondition
// None.
//---------------------------------------------------------------------
//@ PostCondition
//@ End-Method
//=====================================================================
void
Container::removeAllDrawables(void)
{
	CALL_TRACE("Container::removeAllDrawables(void)");

	// Loop through the list of contained Drawables (children),
	// and remove each one from the Container.
	//
	SigmaStatus status;
	
	status = rChildren_.goFirst();
	
	while (SUCCESS == status)
	{	//														$[TI1]
		Drawable& childDrawable = rChildren_.currentItem();
		removeDrawable(&childDrawable);
		status = rChildren_.goFirst();
	}
	//															$[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setColor [virtual]
//
//@ Interface-Description
// Redefinition of virtual Drawable::setColor() method.  The passed parameter
// is ignored; this method does nothing.  This method is redefined to do
// nothing because the color attribute inherited from parent class Drawable is
// not used by the Container class.
//---------------------------------------------------------------------
//@ Implementation-Description
// Do nothing, except a sanity check that the inherited Drawable::color_
// member is still set to NO_COLOR.  This implementation intentionally
// does nothing in order to redefine the default behavior provided by
// the inherited (virtual) Drawable::setColor() method.  Changing the
// value of color_ would not cause a problem in itself, since color_
// is simply not used by Container; however, redefining the setColor()
// method avoids an unneeded call to changing(), resulting in a faster
// repaint.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
Container::setColor(Colors::ColorS)
{
	CALL_TRACE("Container::setColor(Colors::ColorS)");
	SAFE_CLASS_ASSERTION(Colors::NO_COLOR == color_);

	//													$[TI1]
	// This method intentionally left blank.
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setFillColor    [virtual]
//
//@ Interface-Description
// Sets the fill ("interior" or "background") color for this Container
// to the passed "fillColor". NOTE: To make the Container "transparent" so
// Drawable objects behind it will show through, set the fill color to
// NO_COLOR.
//---------------------------------------------------------------------
//@ Implementation-Description
// Sets the internal fillColor_ member to the given "fillColor".  Sets
// the inherited Drawable::isOpaque_ flag based on whether the given
// "fillColor" is a real color or NO_COLOR.
//---------------------------------------------------------------------
//@ PreCondition
// The passed fill color must be a positive legal color, or it must be NO_COLOR.
//---------------------------------------------------------------------
//@ PostCondition
//@ End-Method
//=====================================================================
void
Container::setFillColor(Colors::ColorS fillColor)
{
	CALL_TRACE("Container::setFillColor(Colors::ColorS)");

	CLASS_PRE_CONDITION((fillColor >= 0 &&
						 fillColor < Colors::NUM_COLORS
						)
						|| fillColor == Colors::NO_COLOR
					   );

	changing();
	
	fillColor_ = fillColor;
	
	if (fillColor_ == Colors::NO_COLOR)
	{	//													$[TI1]
		isOpaque_ = FALSE;
	}
	else
	{	//													$[TI2]
		isOpaque_ = TRUE;
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setContentsColor [virtual]
//
//@ Interface-Description
// Sets the color of all directly contained Drawables to the given
// "contentsColor".  Contained Containers and their contents are not affected.
// The specified color is saved internally; this is needed because Drawables
// added to this container later are also set to the specified color.
//---------------------------------------------------------------------
//@ Implementation-Description
// Sets the internal contentsColor_ member, then calls the (virtual) setColor()
// method for all contained Drawables.  Note that if a contained Drawable is a
// generic Container, then the Container does not recursively pick up the
// contents color; this is because the Container class has redefined the
// inherited setColor() method to be a no-op.  For a child Container to pick up
// the contents Color, it must provide a redefined setColor() method that
// continues the recursion to its child Drawables.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
Container::setContentsColor(Colors::ColorS contentsColor)
{
	CALL_TRACE("Container::setContentsColor(Colors::ColorS)");

	contentsColor_ = contentsColor;

	for (SigmaStatus status = rChildren_.goFirst();
		 SUCCESS == status;
		 status = rChildren_.goNext())
	{	//												$[TI1]
		rChildren_.currentItem().setColor(contentsColor_);
	}
	//													$[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: sizeToFit
//
//@ Interface-Description
// This method sets the Container's right and bottom edges to just encompass
// the contained Drawables.  This does not adjust the top and left edges, so if
// any of the contained Drawables are outside these bounds, sizeToFit() will
// not change that situation.
//---------------------------------------------------------------------
//@ Implementation-Description
// Walks the list of child Drawables, searching the areas of each child
// Drawable, to identify the rightmost and bottommost edges.  Adjusts the
// Container's private area_ member (inherited from Drawable) so that its right
// and bottom edges equal the rightmost and bottommost edges of the contained
// Drawables.  This may shrink or expand the Container's area_.  The
// Container's origin (upper left corner) is not affected.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
Container::sizeToFit(void)
{
	CALL_TRACE("Container::sizeToFit(void)");

	//
	// Initialize to zero-sized bounding box.
	//
	Int maxX2 = 0;
	Int maxY2 = 0;

	//
	// Loop thru all child objects and determine their areas.
	//
	for (SigmaStatus status = rChildren_.goFirst();
		 SUCCESS == status;
		 status = rChildren_.goNext())
	{	//													$[TI1]
		//
		// Fetch area of current child.
		//
		const Area& childArea = rChildren_.currentItem().getArea();

		//
		// If right edge of current child is greater than any yet found, then
		// save its location:
		//
		if (childArea.getX2() > maxX2)	//					$[TI1.1]
		{
			maxX2 = childArea.getX2();
		}
		//													$[TI1.2]
		//
		// If bottom edge of current child is greater than any yet found, then
		// save its location:
		//
		if (childArea.getY2() > maxY2)	//					$[TI1.3]
		{
			maxY2 = childArea.getY2();
		}
		//													$[TI1.4]	
	}
	//														$[TI2]
	//
	// "Stretch" right edge of Container's area_ to enclose the rightmost
	// child. Add 1 because the coordinate system starts from 0; for example, a
	// Container of width 10 has an X2 value of 11.
	//
	setWidth(maxX2 + 1); 

	//
	// "Stretch" bottom edge of bounding box to enclose the bottommost child.
	//
	setHeight(maxY2 + 1); 
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: findTouchedDrawable [virtual]
//
//@ Interface-Description
// This method determines if this Container object or its contents has been
// touched, and returns a pointer to the touched item.
//
// If this container is touch-sensitive, and the passed "touchArea" overlaps
// with it, this method will return a pointer to this container.  The touchArea
// is a small area centered around the point on the screen where a user touch
// was detected.
//
// If this container is not touch sensitive: this method searches through all
// of the contained touch-sensitive contained Drawables.  If one and only one
// of these overlaps with the passed touchArea, a pointer to it is returned.
// In addition, all contained Containers that are not touch sensitive will
// search through their contents, thus implementing a search of the entire
// containment tree.
//
// The "numHits" parameter is a reference to an Int that accumulates the number
// of touch-sensitive objects that overlap with the touch area.  This is needed
// to ensure that the touch area only overlaps ONE Drawable.  This Int should
// be defined at the point where the top-level call of the recursive set of
// calls to findTouchedDrawable() is made.  As implemented in the GUI
// framework, this is in the TouchAt_() method of the Touch class. It is the
// responsibility of the code making the top-level call to initialize the Int
// referred to by numHits to 0.
//
// If no Drawable overlaps with the touchArea, NULL is returned.
//
// This method uses an internal static variable to monitor the depth of
// recursion; for this reason, it should not be called reentrantly.
//---------------------------------------------------------------------
//@ Implementation-Description
// This method redefines the virtual Drawable::findTouchedDrawable() method for
// Container, to add the abililty to search the contents of the Container for
// touch hits if the Container is not touch-sensitive.
//
// If this container is touch-sensitive (as determined by the isSensitive_
// attribute): this method will return a pointer to this object if the passed
// "touchArea" overlaps with its area_.
//
// If this container is not touch sensitive: this method searches through all
// of the contained touch-sensitive child Drawables.  If one and only one of
// these overlaps with the passed "touchArea", a pointer to it is returned.  In
// addition, all contained Containers that are not touch sensitive will search
// through their contents, thus implementing a search of the entire containment
// tree.
//
// When checking the children, do the hit test using only the intersection
// between this Container's area and the given "touchArea".  This guarantees
// that the coordinates of the area passed down the call tree remain positive
// after the position is adjusted to allow for the Container origin.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
const Drawable *
Container::findTouchedDrawable(const Area& touchArea,
								Int& numHits) const
{
	CALL_TRACE("Container::findTouchedDrawable(const Area& touchArea,"
				" Int& numHits) const");

	const Drawable*	pTargetTemp = NULL;
	const Drawable*	pTarget = NULL;
	const Area&	myArea = getArea();
	Area 		overlapArea;

	//
	// First, determine whether or not the "touchArea" overlaps with myself.
	// If more than 1 sensitive Drawable already hit, simply return NULL.
	//
	if (isVisible() &&
		numHits <= 1 &&
		myArea.makeOverlap(touchArea, overlapArea)
	   )
	{	//												$[TI1]
		if (isSensitive_)
		{	//											$[TI1.1]
			//
			// This object is on-screen, sensitive, and it overlaps the
			// touchArea.
			// Note: if an object is completely covered by another object
			// that is not touch-sensitive - and both have isShown_==TRUE -
			// the covered object can still detect touch...
			//
			pTarget =  this;
			numHits++;	// This is a reference to an Int in the Touch
						// class that accumulates the number of sensitive
						// objects that overlap with the touch area.
			if (numHits > 1)	// 2nd hit found
			{	//										$[TI1.1.1]
				pTarget = NULL;
			}
			//											$[TI1.1.2]
		}
		else
		{	//											$[TI1.2]
			//
			// Convert the overlapping area to relative coordinates
			// based on the origin of this Container.
			//
			overlapArea.moveBy(-getX(), -getY());

			//
			// Loop through all of the children using an iterator.  The numHits
			// param will be incremented by each hit.  If numHits is > 1, this
			// means that more than one sensitive Drawable has been
			// encountered, so no further searching is needed.
			//
			SIteratorR(Drawable) iterator(rChildren_);
			for (SigmaStatus status = iterator.goFirst();
				 SUCCESS == status;
				 status = iterator.goNext())
			{	//										$[TI1.2.1]
				pTargetTemp = iterator.currentItem().
									findTouchedDrawable(overlapArea, numHits);

				if (pTargetTemp != NULL)	// a hit was found
				{	//									$[TI1.2.1.1]
					pTarget = pTargetTemp;
				}
				else if (numHits > 1)	// 2nd hit found
				{	//									$[TI1.2.1.2]
					pTarget = NULL;
					break;
				}
				//										$[TI1.2.1.3]

			}	// end of for loop
			//											$[TI1.2.2]
		}
	}
	//													$[TI2]
	return pTarget;	// can be NULL
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: evaluateVisible [virtual]
//
//@ Interface-Description
// Determines the visibility status of this object.  This is not the same as
// the value returned by the getShow() method; in order to be visible, a
// Drawable must be shown, and its parent Container must be visible.  In short,
// the user can set the shown status using setShow(), but cannot set the "is
// visible" status; it will be set true by this method only if all levels of
// the containment tree above the Drawable are shown and visible.  An object is
// only visible if it is appended to a BaseContainer.
//
// Note that this method does not return the visibility status; to determine
// the outcome, the public isVisible() method can be called.
//
// Note also that object overlap is NOT taken into account when determining
// visibility.  If two objects are both shown, and their Container
// is visible, then they will also be considered visible - even if one is
// not really visible on-screen because the other is "on top" of it.
//
// This method uses an internal static variable to monitor the depth of
// recursion; for this reason, it should not be called reentrantly.
//---------------------------------------------------------------------
//@ Implementation-Description
// This method is a redefined version of the one inherited from the Drawable
// class.  First, it calls the evaluateVisible() method in the base class to
// determine the visibility state of this object.  The visibility is determined
// by the existence and visibility of the parent Container as well as the
// isShown_ status of the drawable.  This is based on the assumption that the
// parent's isVisible_ member is up-to-date.  There is no series of recursive
// calls moving UP the containment tree.
//
// It then uses recursive calls to update the visibility status of its
// children.  Note that it does not re-evaluate the visibility of its parents;
// in general, evaluateVisible() assumes that all levels of the containment
// tree above it have valid isVisible_ settings, but it updates all levels
// below it.
//
// This method is redefined in the BaseContainer class (which has no parent)
// to always return TRUE.
//
// Future programmers: make sure that the visibility status gets updated
// whenever anything happens that could affect the visibility of areas of the
// screen!  For example: when adding a Drawable to a Container, the
// evauluateVisible() method of that Drawable is called because the Container
// might be hidden.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void 
Container::evaluateVisible(void)
{
	CALL_TRACE("Container::evaluateVisible(void)");

	// Save our current visibility state, then call the base class
	// evaluateVisible() method to reevaluate our visibility state.
	//
	Boolean saveVisible = isVisible();
	Drawable::evaluateVisible();

	//
	// Now check to see whether or not our visibility state has changed.
	//
	if (isVisible() != saveVisible)
	{	//											$[TI1]
		//
		// Our visibility state has changed, so reevaluate all of the
		// children.
		//
		for (SigmaStatus status = rChildren_.goFirst();
			 SUCCESS == status;
			 status = rChildren_.goNext())
		{	//										$[TI1.1]
			rChildren_.currentItem().evaluateVisible();
		}
		//											$[TI1.2]
	}
	//												$[TI2]
	
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: evaluateBase [virtual]
//
//@ Interface-Description
// This method updates the private BaseContainer pointer for this Container.
// If this Container's BaseContainer has changed, it also updates the
// BaseContainer pointers for all contained Drawables.
//
// This method uses an internal static variable to monitor the depth of
// recursion; for this reason, it should not be called reentrantly.
//---------------------------------------------------------------------
//@ Implementation-Description
// This method redefines the evaluateBase() method inherited from Drawable.  It
// calls Drawable's evaluateBase() method to update the BaseContainer pointer
// for this Container.  It extends the functionality in the base class by
// adding the updating of the BaseContainer pointers of the contained
// Drawables.  This is done only if the BaseContainer pointer of the Container
// has changed.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
Container::evaluateBase(void)
{
	CALL_TRACE("Container::evaluateBase(void)");

	// Save pointer to our current Base Container, then call parent
	// evaluateBase() method to reevaluate our Base Container.
	//
	BaseContainer* pOldBase = getBaseContainer();
	Drawable::evaluateBase();

	//
	// Now check to see whether or not our Base Container has changed.
	//
	if (getBaseContainer() != pOldBase)
	{	//											$[TI1]
		//
		// Our Base Container has changed, so reevaluate all of the
		// children.
		//
		for (SigmaStatus status = rChildren_.goFirst();
			 SUCCESS == status;
			 status = rChildren_.goNext())
		{	//										$[TI1.1]
			rChildren_.currentItem().evaluateBase();
		}
		//											$[TI1.2]
	}
	//												$[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: draw [virtual]
//
//@ Interface-Description
// Draws this Container and its contents.  Draws the portion of the Container's
// background that intersects the "clipArea", and any child Drawables that
// intersect the "clipArea".
//
// This method uses an internal static variable to monitor the depth of
// recursion; for this reason, it should not be called reentrantly.
//---------------------------------------------------------------------
//@ Implementation-Description
// The first phase is to draw the background of the Container.  This is done
// only if the fillColor_ is not NO_COLOR.  This invlolves drawing the portion
// of the background of the Container which intersects the "clipArea", using
// the fill color.  Only the minimum rectangular area that covers all portions
// of the intersection area that are not covered by opaque contained Drawables
// is drawn.
//
// The second phase then redraws all "shown" child Drawables which intersect
// the "clipArea".  The clipArea is passed to the draw() methods of the child
// Drawables.
//---------------------------------------------------------------------
//@ PreCondition
//  The object must be visible.
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
Container::draw(const Area& clipArea)
{
	CALL_TRACE("Container::draw(const Area&)");
	CLASS_PRE_CONDITION(isVisible());

    DisplayContext & rDc = getBaseContainer()->getDisplayContext();
    GraphicsContext & rGc = getBaseContainer()->getGraphicsContext();

	Area realContainerArea = getArea();

	// First, determine this Container's area, in real (absolute)
	// coordinates.  We must use real coordinates since the given
	// "clipArea" is in real coordinates.
	//
	if (getParentContainer() != NULL)
	{	//											$[TI1]
		realContainerArea.moveBy(getParentContainer()->getRealX(), 
								 getParentContainer()->getRealY()); 
	}
	//												$[TI2]


	// Second, determine the real clipping area (intersectionArea).
	// The clipping is done by using the intersection between the real container
	// area and the clipArea.
	// This is because the passed in clipArea is the merged area of all the drawables
	// that had been changed up to this time.  When merging the areas in the repaint
	// routine, there is no difference between whether this area is from a container
	// or a drawabale.  This ends with a clipArea which may be bigger than the container
	// itself.
	//
	Area intersectionArea;
	Boolean isOverlapped = clipArea.makeOverlap(realContainerArea,
												intersectionArea);

	// Remember the coordinates of the intersection Area.
	Int x1 = intersectionArea.getX1();
	Int y1 = intersectionArea.getY1();
	Int x2 = intersectionArea.getX2();
	Int y2 = intersectionArea.getY2();

	// set the clip area in the graphics context
	rDc.setClip(rGc, x1, y1, x2, y2);

	//
	// If the Container has a fill color, then we may have to redraw the
	// portion of the Container's "background" area that intersects with
	// the given "clipArea" using the Container's fill color.  This must
	// be done before drawing the contained Drawables.
	//
	if (fillColor_ != Colors::NO_COLOR)
	{   //												$[TI3] 
		//
		//
		// The intersection area (in real coordinates) determines how much of
		// the Container background must be redrawn using the fill color.
		//
		// If there are any opaque contained Drawables that cover the
		// intersection (background) area, then we can reduce the size of the
		// intersection area by the portion of the opaque area that covers the
		// intersection area.  So, loop through all child Drawables looking for
		// opaque items that overlap with the intersection area.  To keep
		// things simple, the intersection area is only reduced if the reduced
		// area remains rectangular.
		//
		Boolean drawBackground = TRUE;
		for (SigmaStatus status = rChildren_.goFirst();
			 (SUCCESS == status) && (drawBackground == TRUE);
			 status = rChildren_.goNext())
		{	//											$[TI3.1]
			Drawable& childDrawable = rChildren_.currentItem();

			if (childDrawable.isOpaque() && childDrawable.isVisible())
			{	//										$[TI3.1.1]
				//
				// Found opaque child Drawable, so compute it's area in
				// real (absolute) coordinates.
				//
				Area childRealArea = childDrawable.getArea();
				childRealArea.moveBy(getRealX(), getRealY());

				if (childRealArea.containsArea(intersectionArea))
				{	//									$[TI3.1.1.1]
					//
					// This opaque Drawable completely covers the
					// intersection area, so there's no need to draw
					// this Container's background at all.
					//
					drawBackground = FALSE;
				}
				else
				{	//									$[TI3.1.1.2]
					//
					// Reduce the intersection area by the portion of the
					// opaque child's area that overlaps the intersection area.
					// This only occurs if the child area completely covers
					// exactly one edge of the intersection area; in other
					// words, the result must be rectangular.  If the child
					// does not overlap the intersectionArea, there is no
					// effect.
					//
					intersectionArea.subtractArea(childRealArea);
				}
			}
			//											$[TI3.1.2]
		}
		//												$[TI3.2]
		//
		// If the intersection is not completely covered, then draw it.
		//
		if (drawBackground)
		{	//											$[TI3.3]
		  rGc.setForegroundColor( fillColor_ );
		  rDc.fillRectangle(rGc,
								intersectionArea.getX1(),
								intersectionArea.getY1(), 
								intersectionArea.getX2(),
								intersectionArea.getY2() );
		}
		//												$[TI3.4]
	}
	//													$[TI4]
	//

	//	Draw the container's children ONLY when the container overlaps the
	//  clipArea.
	//
	if (isOverlapped)
	{	//													$[TI5]
		// Create the new overlapped area as the clipping area.
		//
		Area containerBoundedRealArea(x1, y1, x2, y2);

		// We have to convert the containerBoundedRealArea to local (relative) coordinates
		// to allow us to compare it to the area of the children.  The
		// containerBoundedRealArea is passed to the draw() methods of the children.
		//
		Area localClipArea = containerBoundedRealArea;
		localClipArea.moveBy(-getRealX(), -getRealY());
		//
		// Loop through all of the contained Drawables and draw them if they
		// overlap with the given "clipArea".
		// The clipArea is passed to the draw() methods of the children.
		//
		for (SigmaStatus status = rChildren_.goFirst();
			 SUCCESS == status;
			 status = rChildren_.goNext())
		{	//												$[TI5.1]
			if (rChildren_.currentItem().getShow())
			{	//											$[TI5.1.1]
				if (localClipArea.overlapsArea(rChildren_.currentItem().getArea()))
				{	//										$[TI5.1.1.1]
					// Always reset the clip area.
					// In case there is a nested container within this
					// container, reset of the clip area is necessary because
					// the clipped region set at the beginning of this method
					// may have been changed during the nested container draw()
					// call.
					rDc.setClip(rGc, x1, y1, x2, y2);
					rChildren_.currentItem().draw(containerBoundedRealArea);
				}
				//											$[TI5.1.1.2]
			}
			//												$[TI5.1.2]
		}
		//													$[TI5.2]
	}
	//														$[TI16]
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: convertToRealX [virtual]
//
//@ Interface-Description
// Transforms the passed local "x" coordinate into a real (absolute) X
// coordinate, and returns it.  By "local" we mean relative to the upper left
// corner of this Container; by "real" we mean relative to the upper left
// corner of its BaseContainer, which is assumed to occupy an entire VGA
// screen.
//---------------------------------------------------------------------
//@ Implementation-Description
// The given "x" coordinate is assumed to be a relative (local) coordinate
// with respect to this Container's origin.  Thus, all we have to
// do is add the value of this Container's real (absolute) X origin.
//---------------------------------------------------------------------
//@ PreCondition
//	This Container must have a BaseContainer.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Int
Container::convertToRealX(Int x)
{
	CALL_TRACE("Container::convertToRealX(Int x)");
	CLASS_PRE_CONDITION(pBaseContainer_ != NULL);

	//											$[TI1]
	return getRealX() + x;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: convertToRealY [virtual]
//
//@ Interface-Description
// Transforms the passed local "y" coordinate into a real (absolute) Y
// coordinate, and returns it.  By "local" we mean relative to the upper left
// corner of this Container; by "real" we mean relative to the upper left
// corner of its BaseContainer, which is assumed to occupy an entire VGA
// screen.
//---------------------------------------------------------------------
//@ Implementation-Description
// The given "y" coordinate is assumed to be a relative (local) coordinate
// with respect to this Container's origin.  Thus, all we have to
// do is add the value of this Container's real (absolute) Y origin.
//---------------------------------------------------------------------
//@ PreCondition
//	This Container must have a BaseContainer.
//---------------------------------------------------------------------
//@ PostCondition
//  none    
//@ End-Method
//=====================================================================
Int 
Container::convertToRealY(Int y) 
{ 
	CALL_TRACE("Container::convertToRealY(Unit)"); 
	CLASS_ASSERTION(pBaseContainer_ != NULL); 

	//											$[TI1]
	return getRealY() + y;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: scroll
//
//@ Interface-Description
//  Called to move the specified area by the specified defined X and Y delta distances.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Get the display context then directly move the scroll area by the
//	specified delta X and delta Y.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//  none    
//@ End-Method
//=====================================================================
void
Container::scroll(const Area & scrollArea, Int deltaX, Int deltaY)
{
  CLASS_ASSERTION( pBaseContainer_ );
  CLASS_ASSERTION( pBaseContainer_->isDirectDrawEnabled() );

  DisplayContext & rDc = pBaseContainer_->getDisplayContext();
	
  rDc.moveBlock( 
	  convertToRealX(scrollArea.getX1()),         // xSrc
	  convertToRealY(scrollArea.getY1()),         // ySrc
	  convertToRealX(scrollArea.getX1()+deltaX),  // xDest
	  convertToRealY(scrollArea.getY1()+deltaY),  // yDest
	  scrollArea.getX2()-scrollArea.getX1()+1,    // xWidth
	  scrollArea.getY2()-scrollArea.getY1()+1 );  // yHeight

  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setDirectDraw [virtual]
//
//@ Interface-Description
//  Enables or disables the Direct Draw mode as specified.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Call the setDirectDraw method of the its BaseContainer parent.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//  none    
//@ End-Method
//=====================================================================
void
Container::setDirectDraw( Boolean enableDirectDraw )
{
  CLASS_ASSERTION( pBaseContainer_ );
  pBaseContainer_->setDirectDraw( enableDirectDraw );
  // 											$[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isDisplayLockPending
//
//@ Interface-Description
//  Queries the display lock pending status of this container.
//---------------------------------------------------------------------
//@ Implementation-Description
// 	Returns the display lock pending status of the BaseContainer parent. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//  none    
//@ End-Method
//=====================================================================
Boolean
Container::isDisplayLockPending(void) const
{
  CLASS_ASSERTION( pBaseContainer_ );
  return pBaseContainer_->getDisplayContext().isLockPending();
  // 											$[TI1]
}


//=====================================================================
//
// Protected methods
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Container [constructor] [protected]
//
//@ Interface-Description
// Protected constructor used by derived classes to construct this
// base class to reference a different list of children Drawables.
//---------------------------------------------------------------------
//@ Implementation-Description
// Initializes all local data members. The color_ data member inherited
// from Drawable is changed from default BLACK to NO_COLOR. Initializes
// the private rChildren_ to the specified parameter.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Container::Container(SListR_Drawable &rChildren) :
	rChildren_(rChildren),
	fillColor_(Colors::NO_COLOR),
	contentsColor_(Colors::NO_COLOR)
{
	CALL_TRACE("Container(SListR_Drawable)");

	//														$[TI1]
	//
	// Initialize inherited protected data members.
	// NOTE: Container object never uses the color_ data member.  
	//
	color_ = Colors::NO_COLOR;

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getChildren_ [protected]
//
//@ Interface-Description
// This protected method returns a const pointer to this Container's private
// list of contained "child" Drawables.  This allows the descendents of
// Container to access the private rChildren_ list.  Since the pointer is const,
// they can iterate through the list, but they cannot change it.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply returns the address of the list of children.  Note that this list
// is based on an instantiated template class from the Foundation subsystem.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

const SListR_Drawable* Container::getChildren_(void) const
{
	CALL_TRACE("Container::getChildren_(void) const");

	//											$[TI1]
	return &rChildren_;
}


//=====================================================================
//
// Private methods
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setChild_
//
//@ Interface-Description
// Adds the given "pDrawable" to the list of child Drawables.  This method
// should be called only from the Drawable::appendTo() method.  If you need to
// add a Drawable to this Container, use the addDrawable() method instead.
// This method shouldn't be used except from Drawable::appendTo() because
// it does not update some of the added Drawable's member variables, such
// as its pParentContainer_ and pBaseContainer_.
//
// If a contents color has been set for this Container using the
// setContentsColor() method, then the color of the new child Drawable will be
// set to that color.
//---------------------------------------------------------------------
//@ Implementation-Description
// This method sets the color of the new child Drawable to this Container's
// private contentsColor_, unless it is NO_COLOR.  This is done using the
// setColor() method.  The new child is then added to this Container's
// private list of contained Drawables (rChildren_).
//
// It is assumed that the caller sets the parent pointer of the given
// "pDrawable" to point to this Container.  Thus, the caller is typically
// "pDrawable" itself.  The caller is also responsible for updating the
// pDrawable's BaseContainer pointer.
//
// Note that this class bestows friendship upon the Drawable::appendTo()
// method in order to allow it to call this private method.
//---------------------------------------------------------------------
//@ PreCondition
// The given "pDrawable" pointer must not be NULL.
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
Container::setChild_(Drawable* pDrawable)
{
	CALL_TRACE("Container::setChild_(Drawable *)");
	CLASS_PRE_CONDITION(pDrawable != NULL);

	//
	// A new child object takes on the contents color of this parent
	// Container, if any.
	//
   	if (contentsColor_ != Colors::NO_COLOR)
   	{	//												$[TI1]
   		pDrawable->setColor(contentsColor_);
   	}
	//													$[TI2]
	//
	// Add the Drawable to our collection.
	//
	AUX_CLASS_ASSERTION(!rChildren_.isFull(), Uint(this));
   	rChildren_.append(*pDrawable);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: removeChild_
//
//@ Interface-Description
// Removes the given "pDrawable" from the list of child Drawables.  This method
// should be called only from the Drawable::removeFromParent() method.  If you
// need to remove a Drawable from this Container, use the removeDrawable()
// method instead.  This method shouldn't be used except from
// Drawable::removeFromParent() because it does not update some of the removed
// Drawable's member variables, such as its pParentContainer_ and
// pBaseContainer_.
//---------------------------------------------------------------------
//@ Implementation-Description
// This method removes the Drawable pointed to by pDrawable from the
// Container's private list of contained Drawables (rChildren_).  It does this
// by using the removeItem() method of the list object.  Note that this list is
// based on an instantiated template class from the Foundation  subsystem.
//
// It is assumed that the caller sets the parent pointer of the given
//"pDrawable" to NULL.  Thus, the caller is typically "pDrawable" itself.
//---------------------------------------------------------------------
//@ PreCondition
// The given "pDrawable" pointer must not be NULL.
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
Container::removeChild_(Drawable* pDrawable)
{
   	CALL_TRACE("Container::removeChild_(Drawable *)");
   	CLASS_PRE_CONDITION(pDrawable != NULL);

	//
	// Remove the Drawable from our collection.
	//
   	if (rChildren_.removeItem(*pDrawable, EQUIV_REFERENCES) == FAILURE)
   	{   //												$[TI2]

		CLASS_ASSERTION_FAILURE();
   	}
   	//												$[TI1]
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void 
Container::SoftFault(const SoftFaultID  softFaultID,
					 const Uint32       lineNumber,
					 const char*        pFileName,
					 const char*        pPredicate)  
{
	CALL_TRACE("Container::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_FOUNDATIONS, CONTAINER,
							lineNumber, pFileName, pPredicate);
}
