#ifndef Trace_HH
#define Trace_HH
#ifndef SIGMA_OFFICIAL
#ifdef TRACE_ON
#ifndef TRACE_OFF

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sotred in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: Trace - Call tracing - NON OFFICIAL ONLY
//---------------------------------------------------------------------
// Version
// @(#) $Header:   /840/Baseline/Sys-Init/vcssrc/Trace.hhv   25.0.4.0   19 Nov 2013 14:33:58   pvcs  $
//
// Modification-Log
//  
//  Revision: 001  By:  gdc    Date:  05-16-95    DR Number: 
//	Project:  Sigma (R8027) 
//	Description:
//		Initial version (Integration baseline). 
//
//====================================================================

// Usage-Classes
// End-Usage

#include "SigmaTypes.hh"

class Trace {
  public:
    Trace ( const char*  funcName,
		const char*  fileName,
		const int    lineNumber);
    ~Trace(void);

    static void     SetTraceOn(void);
    static void     SetTraceOn(int thread);
    static void     SetTraceOff(void);
    static void     SetTraceOff(int thread);
    static Boolean  IsTraceOn(void);
    static Boolean  IsTraceOn(int thread);

  private:
    const char*     pFuncName_;
    const char*     pFileName_;
    const int       lineNumber_;
};


// Inlined Methods...
#include "Trace.in"


#endif  // !TRACE_OFF
#endif  // TRACE_ON
#endif  // !SIGMA_OFFICIAL
#endif  // Trace_HH
