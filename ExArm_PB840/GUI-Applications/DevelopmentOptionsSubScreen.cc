#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: DevelopmentOptionsSubScreen - Subscreen displaying development
//         options.
//---------------------------------------------------------------------
//@ Interface-Description
//  This class is derived from subscreen class and contains buttons for
//  overriding SST for neonatal, pediatric, and adult circuit, overriding
//  EST, gui and bd download, and software options.  
//---------------------------------------------------------------------
//@ Rationale
//  This class is dedicated to managing development activities.
//---------------------------------------------------------------------
//@ Implementation-Description
//  For bypassing SST and/or EST, the service mode manager is notified.  
//---------------------------------------------------------------------
//@ Fault-Handling
//  None.
//---------------------------------------------------------------------
//@ Restrictions
//  None.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/DevelopmentOptionsSubScreen.ccv   25.0.4.0   19 Nov 2013 14:07:40   pvcs  $
//
//@ Modification-Log
//
//  Revision: 023  By:  mnr   Date:  28-Dec-2009    SCR Number: 6437
//  Project:  NEO
//  Description:
//		Disable Adult/PED SST override if NeoModeLockout is enabled.
//
//  Revision: 022  By:  gdc    Date:  18-Feb-2009    SCR Number: 6476
//  Project:  840S
//  Description:
//		Implemented NeoMode Update option.
//
//  Revision: 021  By:  gdc    Date:  10-Dec-2008    SCR Number: 6377
//  Project:  840S
//  Description:
//	Removed DELTA project single screen support.
//
//  Revision: 020  By: rhj    Date: 07-Nov-2008   SCR Number:  6435
//  Project:  840S
//  Description:
//      Moved Software options functionality to the Software options subscreen.
//
//  Revision: 019  By: gdc    Date: 03-Jun-2007   SCR Number: 6237 
//  Project:  Trend
//	Description:
//		Changed text baseline for title as part of automated text positioning.
//
//  Revision: 018  By: gdc    Date: 26-May-2007   SCR Number: 6330 
//  Project:  Trend
//	Description:
//		Removed SUN prototype code.
//
//  Revision: 017   By:   rhj    Date: 05-March-2007      DR Number: 6359
//  Project:  RESPM
//  Description:
//        Resolve merge conflict for RM to Baseline 
//
//  Revision: 016  By: erm    Date: 16-DEC-2002   DR Number: 6028 
//  Project:  EMI
//	Description:
//		Added the abilty to change Manufacturing Datakeys
//              to production Datakeys.
//
//  Revision: 015  By: gfu		Date: 12-July-2002   DCS Number:  6015
//  Project:  VCP
//  Description:
//      Added ifdef around setting up of single screen option button information with a Manufacturing Datakey
//   installed to remove bus access fault on the manfacutirng floor when the Development Option Subscreen
//   was activated.     
//
//  Revision: 014  By: srp		Date: 28-May-2002   DCS Number:  5908
//  Project:  VCP
//  Description:
//      Removed ifdef with FORNOW not per coding standards   
//
//  Revision: 013  By: jja		Date: 23-May-2002   DCS Number:  6010 
//  Project:  VCP
//  Description:
//      Fixed option display following GuiComm/VC+ merge.
//      Restore option buttons to consistent type (i.e.BUTTON::ROUND)
//
//  Revision: 012  By: gdc    Date: 28-Aug-2000   DCS Number:  5753
//  Project:  Delta
//  Description:
//      Implemented Single Screen option.
//
//  Revision: 011  By: sah    Date: 17-Jul-2000   DCS Number:  5755
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      *  standardized static data types
//      *  added support for VTPC option
//
//  Revision: 010   By: sah   Date:  23-May-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//      PAV project-related changes:
//      *  added support for PAV option
//
//  Revision: 009  By: sah    Date: 30-Mar-2000   DCS Number:  5671
//  Project:  NeoMode
//  Description:
//      Set option-set buttons to flat if not an Engineering data key.
//
//  Revision: 008  By: sah    Date: 28-Feb-2000   DCS Number:  5664
//  Project:  NeoMode
//  Description:
//      Fixed problem where calls to 'setTo{Up,Down}()' in 'activate()'
//      were being treated as if the user had pressed the button.
//
//  Revision: 007  By: sah    Date: 15-Feb-2000   DCS Number:  5327
//  Project:  NeoMode
//  Description:
//      Added capabilities for displaying software options.
//
//  Revision: 006  By: sph    Date: 05-Jan-2000   DCS Number:  5293
//  Project:  NeoMode
//  Description:
//      Added 800ms delay and darkened button when by pass flash, overide est 
//      and/or sst are pressed
//
//  Revision: 005  By: sah    Date: 06-Jul-1999   DCS Number:  5424
//  Project:  NeoMode
//  Description:
//      Removed obsoleted buttons; added new circuit type-specific,
//      SST override buttons; added ability to activated/deactivate
//      software options.
//
//  Revision: 004  By:  sah	   Date:  11-Jan-1999    DCS Number: 5321
//  Project:  ATC
//  Description:
//		Added 'SIGMA_DEVELOPMENT' conditionals around this class (unused)
//		'SoftFault()' method.
//
//  Revision: 003  By: yyy      Date: 05-Jan-1998  DR Number: 
//    Project:  BiLevel (R8027)
//    Description:
//     Initial release for BiLevel adding software option.
//     Merged /840/Baseline/GUI-Applications/vcssrc/DevelopmentOptionsSubScreen.ccv   1.22.1.0   07/30/98 10:12:42   gdc
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  wks    Date:  01-APR-94    DR Number: 
//  Project:  Sigma (R8027)
//  Description:
//      Integration baseline.
//
//=====================================================================

#include "DevelopmentOptionsSubScreen.hh"
#include "SmTestId.hh"

//@ Usage-Classes
#include "UpperSubScreenArea.hh"
#include "Image.hh"
#include "Task.hh"
//@ End-Usage


#include "UpperScreen.hh"
#include "ServiceUpperScreen.hh"
#include "ServiceUpperScreenSelectArea.hh"
#include "AdjustPanel.hh"
#include "SoftwareOptions.hh"
#include "SettingContextHandle.hh"
#include "PatientCctTypeValue.hh"
#include "NovRamManager.hh"
#include "VgaGraphicsDriver.hh"
#include "SmManager.hh"

//@ Code...

static const Int  BUTTON_X_=10;
static const Int  BUTTON_Y_=40;

static const Int  BUTTON_WIDTH_=124;
static const Int  BUTTON_HEIGHT_=36;
static const Int  BUTTON_BORDER_ = 3;
static const Int  BUTTON_X_GAP_=20;
static const Int  BUTTON_Y_GAP_=20;

Uint32 DevelopmentOptionsSubScreen::lastDisplay_ = DEVELOPMENTOPTIONSSUBSCREEN;


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: DevelopmentOptionsSubScreen()  [Constructor]
//
//@ Interface-Description
// Constructor.  The 'pSubScreenArea' parameter is the pointer to the
// parent SubScreenArea which contains this subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Initializes all members and initializes the colors, sizes, and
// positions of all graphical objects.  Adds all graphical objects to
// the parent container for display.  Registers with the
// ContextObserver for notification of changes to the 'mode'
// setting.  Also, registers for updates with the Patient Data
// subsystem.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
 
DevelopmentOptionsSubScreen::DevelopmentOptionsSubScreen(SubScreenArea* pSubScreenArea) :
	SubScreen(pSubScreenArea),
	titleArea_(L"{p=14,y=18:Development Options}", SubScreenTitleArea::SSTA_CENTER),

	//-----------------------------------------------------------------
	// ROW 0:
	//-----------------------------------------------------------------
	byPassFlashButton_(BUTTON_X_+ (0 * (BUTTON_WIDTH_ + BUTTON_X_GAP_)),
					BUTTON_Y_+ (0 * (BUTTON_HEIGHT_ + BUTTON_Y_GAP_)),
					BUTTON_WIDTH_, BUTTON_HEIGHT_,
					Button::LIGHT, BUTTON_BORDER_, Button::SQUARE, Button::NO_BORDER,
					L"{p=10,x=0,y=17:Default FLASH}"),
	setEstPassButton_(BUTTON_X_+ (1 * (BUTTON_WIDTH_ + BUTTON_X_GAP_)),
					BUTTON_Y_+ (0 * (BUTTON_HEIGHT_ + BUTTON_Y_GAP_)),
					BUTTON_WIDTH_, BUTTON_HEIGHT_,
					Button::LIGHT, BUTTON_BORDER_, Button::SQUARE, Button::NO_BORDER,
					L"{p=10,x=0,y=17:Default EST}"),

	//-----------------------------------------------------------------
	// ROW 1:
	//-----------------------------------------------------------------
	pedSstOverrideButton_(BUTTON_X_+ (0 * (BUTTON_WIDTH_ + BUTTON_X_GAP_)),
						BUTTON_Y_+ (1 * (BUTTON_HEIGHT_ + BUTTON_Y_GAP_)),
						BUTTON_WIDTH_, BUTTON_HEIGHT_,
						Button::LIGHT, BUTTON_BORDER_, Button::SQUARE, Button::NO_BORDER,
						L"{p=10,x=0,y=17:Pediatric SST}"),
	adultSstOverrideButton_(BUTTON_X_+ (1 * (BUTTON_WIDTH_ + BUTTON_X_GAP_)),
						BUTTON_Y_+ (1 * (BUTTON_HEIGHT_ + BUTTON_Y_GAP_)),
						BUTTON_WIDTH_, BUTTON_HEIGHT_,
						Button::LIGHT, BUTTON_BORDER_, Button::SQUARE, Button::NO_BORDER,
						L"{p=10,x=0,y=17:Adult SST}"),
	neoSstOverrideButton_(BUTTON_X_+ (2 * (BUTTON_WIDTH_ + BUTTON_X_GAP_)),
						BUTTON_Y_+ (1 * (BUTTON_HEIGHT_ + BUTTON_Y_GAP_)),
						BUTTON_WIDTH_, BUTTON_HEIGHT_,
						Button::LIGHT, BUTTON_BORDER_, Button::SQUARE, Button::NO_BORDER,
						L"{p=10,x=0,y=17:Neonatal SST}"),

	//-----------------------------------------------------------------


	//-----------------------------------------------------------------
	// ROW 3:
	//-----------------------------------------------------------------
	guiDownloadButton_(BUTTON_X_+ (0 * (BUTTON_WIDTH_ + BUTTON_X_GAP_)),
					BUTTON_Y_+ (3 * (BUTTON_HEIGHT_ + BUTTON_Y_GAP_)),
					BUTTON_WIDTH_, BUTTON_HEIGHT_,
					Button::LIGHT, BUTTON_BORDER_, Button::SQUARE, Button::NO_BORDER,
					L"{p=10,x=0,y=17:GUI DownLoad}"),
	bdDownloadButton_(BUTTON_X_+ (1 * (BUTTON_WIDTH_ + BUTTON_X_GAP_)),
					BUTTON_Y_+ (3 * (BUTTON_HEIGHT_ + BUTTON_Y_GAP_)),
					BUTTON_WIDTH_, BUTTON_HEIGHT_,
					Button::LIGHT, BUTTON_BORDER_, Button::SQUARE, Button::NO_BORDER,
					L"{p=10,x=0,y=17:BD DownLoad}"),
	mfgToProdDatakeyButton_(BUTTON_X_+ (2 * (BUTTON_WIDTH_ + BUTTON_X_GAP_)),
					BUTTON_Y_+ (3 * (BUTTON_HEIGHT_ + BUTTON_Y_GAP_)),
					BUTTON_WIDTH_, BUTTON_HEIGHT_,
					Button::LIGHT, BUTTON_BORDER_, Button::SQUARE, Button::NO_BORDER,
					L"{p=10,x=0,y=17:Mfg->Prod DKey}"),

	softwareOptionsButton_(BUTTON_X_+ (2 * (BUTTON_WIDTH_ + BUTTON_X_GAP_)),
					BUTTON_Y_+ (0 * (BUTTON_HEIGHT_ + BUTTON_Y_GAP_)),
					BUTTON_WIDTH_, BUTTON_HEIGHT_,
					Button::LIGHT, BUTTON_BORDER_, Button::SQUARE, Button::NO_BORDER,
					MiscStrs::SOFTWARE_OPTIONS_BUTTON_LABEL),

	pCurrentButton_(NULL)
{
	CALL_TRACE("DevelopmentOptionsSubScreen::DevelopmentOptionsSubScreen(pSubScreenArea)");
	// Size and position the area
	setX(0);
	setY(0);
	setWidth(UpperSubScreenArea::CONTAINER_WIDTH);
	setHeight(UpperSubScreenArea::CONTAINER_HEIGHT);

	// Set subscreen background color
	setFillColor(Colors::MEDIUM_BLUE);

	// Display the screen title
	addDrawable(&titleArea_);

	addDrawable(&byPassFlashButton_);
	addDrawable(&setEstPassButton_);
	addDrawable(&neoSstOverrideButton_);
	addDrawable(&pedSstOverrideButton_);
	addDrawable(&adultSstOverrideButton_);
	addDrawable(&guiDownloadButton_);
	addDrawable(&bdDownloadButton_);
	addDrawable(&mfgToProdDatakeyButton_);


	byPassFlashButton_.setButtonCallback(this);
	setEstPassButton_.setButtonCallback(this);
	neoSstOverrideButton_.setButtonCallback(this);
	pedSstOverrideButton_.setButtonCallback(this);
	adultSstOverrideButton_.setButtonCallback(this);
	guiDownloadButton_.setButtonCallback(this);
	bdDownloadButton_.setButtonCallback(this);
	mfgToProdDatakeyButton_.setButtonCallback(this);

	softwareOptionsButton_.setButtonCallback(this); 
    addDrawable(&softwareOptionsButton_);

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~DevelopmentOptionsSubScreen  [Destructor]
//
//@ Interface-Description
//  Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  n/a
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

DevelopmentOptionsSubScreen::~DevelopmentOptionsSubScreen(void)
{
	CALL_TRACE("DevelopmentOptionsSubScreen::~DevelopmentOptionsSubScreen(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activate()
//
//@ Interface-Description
//  Prepares this subscreen for display.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Checks available option, sets option buttons and sets override
//  buttons accordingly.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
DevelopmentOptionsSubScreen::activate(void)
{
	CALL_TRACE("DevelopmentOptionsSubScreen::activate(void)");

    // Force this subscreen to the Software Options subscreen if it was the last 
    // subscreen displayed before it was deactivated.
    if(lastDisplay_ ==  SOFTWARE_OPTIONS_SUBSCREEN)
    {
        displaySoftwareOptionsSubScreen_();
		return;
    }



// only allow MFG Datakey to activate mfg-> production conversion
        if (SoftwareOptions::GetSoftwareOptions().getDataKeyType() !=
			SoftwareOptions::MANUFACTURING)
	{
	    mfgToProdDatakeyButton_.setToFlat();	
	}

	// only allow the "Neonatal SST" button to be pressed, if NeoMode is
	// enabled...
	if (SoftwareOptions::IsOptionEnabled(SoftwareOptions::NEO_MODE))
	{
		neoSstOverrideButton_.setToUp();
	}
	else
	{
		neoSstOverrideButton_.setToFlat();
	}

	// only allow the "Neonatal SST" button to be UP and not other buttons, 
	// if NeoModeLockout is enabled...[NV04000]
	if (SoftwareOptions::IsOptionEnabled(SoftwareOptions::NEO_MODE_LOCKOUT))
	{
		neoSstOverrideButton_.setToUp();
		pedSstOverrideButton_.setToFlat();
		adultSstOverrideButton_.setToFlat();
	}
	else
	{
        pedSstOverrideButton_.setToUp();
		adultSstOverrideButton_.setToUp();
	}

	if (!GuiApp::IsSstFailure())
	{
		const DiscreteValue  CIRCUIT_TYPE = 
				SettingContextHandle::GetSettingValue(
										ContextId::ACCEPTED_CONTEXT_ID,
										SettingId::PATIENT_CCT_TYPE
													 );

		switch (CIRCUIT_TYPE)
		{
		case PatientCctTypeValue::PEDIATRIC_CIRCUIT :
			pedSstOverrideButton_.setButtonType(Button::DARK);

			adultSstOverrideButton_.setContentsColor(Colors::BLACK);
			adultSstOverrideButton_.setButtonType(Button::LIGHT_NON_LIT);
			neoSstOverrideButton_.setContentsColor(Colors::BLACK);
			neoSstOverrideButton_.setButtonType(Button::LIGHT_NON_LIT);
			break;
		case PatientCctTypeValue::ADULT_CIRCUIT :
			adultSstOverrideButton_.setButtonType(Button::DARK);

			pedSstOverrideButton_.setContentsColor(Colors::BLACK);
			pedSstOverrideButton_.setButtonType(Button::LIGHT_NON_LIT);
			neoSstOverrideButton_.setContentsColor(Colors::BLACK);
			neoSstOverrideButton_.setButtonType(Button::LIGHT_NON_LIT);
			break;
		case PatientCctTypeValue::NEONATAL_CIRCUIT :
			neoSstOverrideButton_.setButtonType(Button::DARK);

			pedSstOverrideButton_.setContentsColor(Colors::BLACK);
			pedSstOverrideButton_.setButtonType(Button::LIGHT_NON_LIT);
			adultSstOverrideButton_.setContentsColor(Colors::BLACK);
			adultSstOverrideButton_.setButtonType(Button::LIGHT_NON_LIT);
			break;
		}
	}
}															//	$[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: deactivate()
//
//@ Interface-Description
//   Prepares this subscreen for removal from the display
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
DevelopmentOptionsSubScreen::deactivate(void)
{
	CALL_TRACE("DevelopmentOptionsSubScreen::deactivate(void)");

	if (pCurrentButton_ == &guiDownloadButton_ ||
		pCurrentButton_ == &bdDownloadButton_ ||
		pCurrentButton_ == &mfgToProdDatakeyButton_)
	{
		// Pop up the button.
		pCurrentButton_->setToUp();
	}
        
        
	pCurrentButton_ = NULL;
}															//	$[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: buttonDownHappened
//
//@ Interface-Description
// Called when any button in DevelopmentOptionsSubScreen is pressed down.  
//---------------------------------------------------------------------
//@ Implementation-Description
// Check the button pressed.  For options button, enable option bit by
// notifying service mode manager.  For by pass buttons, let service
// mode manager know of type of service. For Software options button,
// call displaySoftwareOptionsSubScreen_()  function to display 
// software options subscreen.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
DevelopmentOptionsSubScreen::buttonDownHappened(Button *pButton,
											    Boolean byOperatorAction)
{
	if (!byOperatorAction &&   pButton!= NULL)
	{
		// only process this callback if action is initiated by user...
		return;
	}

    if ( pButton == &softwareOptionsButton_)
	{

		displaySoftwareOptionsSubScreen_();
		pButton->setToUp();

	}


	if (pButton == &byPassFlashButton_)
	{
		// Let the service mode  manager knows the type of Service mode.
		SmManager::DoFunction(SmTestId::DEFAULT_FLASH_ID);

		Task::Delay(0,600);
		byPassFlashButton_.setButtonType(Button::DARK);
	}
	else if (pButton == &setEstPassButton_)
	{
		// Let the service mode  manager knows the type of Service mode.
		SmManager::DoFunction(SmTestId::FORCE_EST_OVERIDE_ID);

		Task::Delay(0,600);
		setEstPassButton_.setButtonType(Button::DARK);
	}
	else if (pButton == &pedSstOverrideButton_)
	{
		SmManager::DoFunction(SmTestId::FORCE_SST_PED_OVERIDE_ID);

		Task::Delay(0,600);
		pedSstOverrideButton_.setButtonType(Button::DARK);

		adultSstOverrideButton_.setContentsColor(Colors::BLACK);
		adultSstOverrideButton_.setButtonType(Button::LIGHT_NON_LIT);
		neoSstOverrideButton_.setContentsColor(Colors::BLACK);
		neoSstOverrideButton_.setButtonType(Button::LIGHT_NON_LIT);
	}
	else if (pButton == &adultSstOverrideButton_)
	{
		SmManager::DoFunction(SmTestId::FORCE_SST_ADULT_OVERIDE_ID);

		Task::Delay(0,600);
		adultSstOverrideButton_.setButtonType(Button::DARK);

		pedSstOverrideButton_.setContentsColor(Colors::BLACK);
		pedSstOverrideButton_.setButtonType(Button::LIGHT_NON_LIT);
		neoSstOverrideButton_.setContentsColor(Colors::BLACK);
		neoSstOverrideButton_.setButtonType(Button::LIGHT_NON_LIT);
	}
	else if (pButton == &neoSstOverrideButton_)
	{
		SmManager::DoFunction(SmTestId::FORCE_SST_NEO_OVERIDE_ID);

		Task::Delay(0,600);
		neoSstOverrideButton_.setButtonType(Button::DARK);

		pedSstOverrideButton_.setContentsColor(Colors::BLACK);
		pedSstOverrideButton_.setButtonType(Button::LIGHT_NON_LIT);
		adultSstOverrideButton_.setContentsColor(Colors::BLACK);
		adultSstOverrideButton_.setButtonType(Button::LIGHT_NON_LIT);
	}

	if (pButton != &guiDownloadButton_ &&
		pButton != &bdDownloadButton_ &&
		pButton != &mfgToProdDatakeyButton_)
	{
		if (pCurrentButton_ != NULL)
		{
			// Pop up the button.
			pCurrentButton_->setToUp();
		}

		// Bounce button
		if (pButton != &neoSstOverrideButton_  ||
			SoftwareOptions::IsOptionEnabled(SoftwareOptions::NEO_MODE))
		{
			pButton->setToUp();
		}
	}
	else 
	{	// This is only temporarily provide a access method to do download.

		if (pCurrentButton_ != pButton && pCurrentButton_ != NULL)
		{
			// Pop up the button.
			pCurrentButton_->setToUp();
		}

		// Grab Adjust Panel focus
		AdjustPanel::TakeNonPersistentFocus(this, TRUE);
		//
		// Set all prompts 
		//
		setTestPrompt(PromptArea::PA_PRIMARY, 
								PromptArea::PA_HIGH,
								PromptStrs::SM_KEY_ACCEPT_P);
		setTestPrompt(PromptArea::PA_ADVISORY, 
								PromptArea::PA_HIGH,
								NULL_STRING_ID);
		setTestPrompt(PromptArea::PA_ADVISORY, 
								PromptArea::PA_LOW,
								NULL_STRING_ID);
		setTestPrompt(PromptArea::PA_SECONDARY,
								PromptArea::PA_HIGH,
								PromptStrs::DEVELOP_OPTIONS_SCREENS_CANCEL_S);
	}

	pCurrentButton_ = pButton;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: buttonUpHappened
//
//@ Interface-Description
// Called when any button in DevelopmentOptionsSubScreen is pressed up.
//---------------------------------------------------------------------
//@ Implementation-Description
//  For options, set option bit to FALSE.  For downloads, clear requests.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
DevelopmentOptionsSubScreen::buttonUpHappened(Button *pButton,
											  Boolean byOperatorAction)
{
	if (!byOperatorAction)
	{
		// only process this callback if action is initiated by user...
		return;
	}


	if (pButton == &guiDownloadButton_ ||
		pButton == &bdDownloadButton_ ||
		pButton == &mfgToProdDatakeyButton_)
	{
		// Pop up the button.
		clearDownLoadButtonRequest_(pButton);
	}
}	


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelRestoreFocusHappened
//
//@ Interface-Description
// This is a virtual method inherited from being an AdjustPanelTarget.  It is
// normally called when the default adjust panel focus is restored.  Tell the
// subscreen to restore the appropriated prompts.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls restoreTestPrompt to restore test prompts.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
DevelopmentOptionsSubScreen::adjustPanelRestoreFocusHappened(void)
{
	CALL_TRACE("DevelopmentOptionsSubScreen::adjustPanelRestoreFocusHappened(void)");


	restoreTestPrompt();

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelAcceptPressHappened
//
//@ Interface-Description
// This is a virtual method inherited from being an AdjustPanelTarget.  It is
// normally called when the operator presses down the Accept key to signal the
// accepting of start test process.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Set all buttons to flat when gui or bd download is accepted.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
DevelopmentOptionsSubScreen::adjustPanelAcceptPressHappened(void)
{
	CALL_TRACE("DevelopmentOptionsSubScreen::adjustPanelAcceptPressHappened(void)");


	if (pCurrentButton_ == &guiDownloadButton_ ||
		pCurrentButton_ == &bdDownloadButton_ ||
		pCurrentButton_ == &mfgToProdDatakeyButton_)
	{
		byPassFlashButton_.setToFlat();
		setEstPassButton_.setToFlat();
		neoSstOverrideButton_.setToFlat();
		pedSstOverrideButton_.setToFlat();
		adultSstOverrideButton_.setToFlat();
		softwareOptionsButton_.setToFlat();
 

		clearDownLoadButtonRequest_(pCurrentButton_);

		// Hide the drawables within the upper screen select area.
		ServiceUpperScreen::RServiceUpperScreen.getServiceUpperScreenSelectArea()->setBlank(TRUE);
		
        if( pCurrentButton_ == &mfgToProdDatakeyButton_ )
        {
        	mfgToProdDatakeyButton_.setToFlat(); 
            // Let the service mode  manager knows the type of Service mode.
        	SmManager::DoFunction(SmTestId::MFG_PROD_KEY_CONVERT_ID);
        }
		else if (pCurrentButton_ == &guiDownloadButton_)
		{
			guiDownloadButton_.setToFlat();

			// Let the service mode  manager knows the type of Service mode.
			SmManager::DoFunction(SmTestId::GUI_DOWNLOAD_ID);
		}
		else if (pCurrentButton_ == &bdDownloadButton_)
		{
			bdDownloadButton_.setToFlat();

			// Let the service mode  manager knows the type of Service mode.
			SmManager::DoFunction(SmTestId::BD_DOWNLOAD_ID);
		}

		pCurrentButton_ = NULL;
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: clearDownLoadButtonRequest_
//
//@ Interface-Description
// This is a virtual method inherited from being an AdjustPanelTarget.  It is
// normally called when the operator presses down the Accept key to signal the
// accepting of start test process. 
//---------------------------------------------------------------------
//@ Implementation-Description
//  Make sure nothing on screen keeps focus and clear all prompts.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
DevelopmentOptionsSubScreen::clearDownLoadButtonRequest_(Button *pButton)
{
	CALL_TRACE("DevelopmentOptionsSubScreen::clearDownLoadButtonRequest_(Button *pButton)");


	// Service mode had request either an ACCEPT  prompt action.
	// Make sure that nothing in this subscreen keeps focus.
	// Clear the Adjust Panel focus, make the Accept sound
	AdjustPanel::TakeNonPersistentFocus(NULL, TRUE);

	// Clear the Primary prompts
	setTestPrompt(PromptArea::PA_PRIMARY,
					 PromptArea::PA_HIGH, NULL_STRING_ID);

	// Clear the Advisory prompts
	setTestPrompt(PromptArea::PA_ADVISORY,
					 PromptArea::PA_HIGH, NULL_STRING_ID);

	// Clear the Secondary prompt
	setTestPrompt(PromptArea::PA_SECONDARY,
					PromptArea::PA_HIGH, NULL_STRING_ID);

	// Clear the Primary prompts
	setTestPrompt(PromptArea::PA_PRIMARY,
					 PromptArea::PA_LOW, NULL_STRING_ID);

	// Clear the Advisory prompts
	setTestPrompt(PromptArea::PA_ADVISORY,
					 PromptArea::PA_LOW, NULL_STRING_ID);

	// Clear the Secondary prompt
	setTestPrompt(PromptArea::PA_SECONDARY,
					PromptArea::PA_LOW, NULL_STRING_ID);
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: displaySoftwareOptionsSubScreen_
//
//@ Interface-Description
//  Activates the Software Options Subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method activates Software Options Subscreen by getting 
//  two required pointers, Software Options and the Development Options tab 
//  button.  Also set the lastDisplay_ variable to the software options
//  subscreen.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void DevelopmentOptionsSubScreen::displaySoftwareOptionsSubScreen_()
{
    CALL_TRACE("DevelopmentOptionsSubScreen::displaySoftwareOptionsSubScreen_()");

    // Set Software Options Subscreen as the last display
    lastDisplay_ = SOFTWARE_OPTIONS_SUBSCREEN;

    // Retrieve the Software Options Subscreen.
    SubScreen*  pSubscreen = UpperSubScreenArea::GetSoftwareOptionsSubScreen();
    CLASS_ASSERTION(pSubscreen != NULL);

    // Retrieve the Development Option tab button.
	TabButton * pTabButton = ServiceUpperScreen::RServiceUpperScreen.getServiceUpperScreenSelectArea()->
														  getDevelopmentOptionTabButton();
    CLASS_ASSERTION(pTabButton != NULL);


    // Activate the Software Options subscreen with the Development Options tab button.
    // Activating the subscreen automatically forces the associated
    // tab button down. 
    getSubScreenArea()->activateSubScreen(pSubscreen, pTabButton);

}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
DevelopmentOptionsSubScreen::SoftFault(const SoftFaultID  softFaultID,
		   const Uint32       lineNumber,
		   const char*	pFileName,
		   const char*	pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS,
							DEVELOPMENTOPTIONSSUBSCREEN,
							lineNumber, pFileName, pPredicate);
}


