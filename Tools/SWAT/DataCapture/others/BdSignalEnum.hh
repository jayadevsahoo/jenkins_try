//----------------------------------------------------------------------------
//            Copyright (c) 2010 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file BdSignalEnum.hh
/// @brief Enumeration identifying all parameters that BD signal can output
///
/// This file is also used by SWAT.
//----------------------------------------------------------------------------

#ifndef BDSIGNALENUM_HH_
#define BDSIGNALENUM_HH_

//
// IMPORTANT!!!
//
// The name after the BDSIG_ must match the name of the helper function
// responsible for this signal.
//
// For example,
//          BDSIG_InspFlowSensor
//          Get_InspFlowSensor
//
//    Because the macro MAKE_BD_SIGS uses the InspFlowSensor to generate code.
//

typedef enum BdSignalEnum
{
	BDSIG_InspFlowSensor = 0,
	BDSIG_ExhFlowSensorOffset,
	BDSIG_ExhFlowSensorNoOffset,
	BDSIG_LungFlow,
	BDSIG_LungFlow_btps,
	BDSIG_InspPressureSensor,
	BDSIG_ExhPressureSensor,
	BDSIG_SVPressureSensor,
	BDSIG_PressWyeInspEst,
	BDSIG_InspFlowControllerDesiredFlow,
	BDSIG_PavPhaseElasticPlusResistivePressure,
	BDSIG_PavPhaseElsticPressure,
	BDSIG_PavPhaseResistivePressure,
	BDSIG_PavMgrCraw,
	BDSIG_C_Patient,
	BDSIG_R_raw,
	BDSIG_RrawSlope,
	BDSIG_R_ET,
	BDSIG_R_Patient,
	BDSIG_R_Total,
	BDSIG_PatientResistiveWob,
	BDSIG_PatientElastanceWob,
	BDSIG_PatientWob,
	BDSIG_TotalWob,
	BDSIG_PavMgrState,
	BDSIG_PavPercentSupport,
	BDSIG_PavEndExhWyePressure,
	BDSIG_PavTriggerPressureDrop,
	BDSIG_PavIntrinsicPeep,
	BDSIG_PavWyePressure,
	BDSIG_PavLungPressure,
	BDSIG_cRawPressureDelta,
	BDSIG_PavBreathNum,
	BDSIG_PavPhaseDesiredPressure,
	BDSIG_PavElasticTarget,
	BDSIG_PavPreventPauseCond,
	BDSIG_PavSchedulePauseCond,

	BDSIG_PressureControllerDesiredPressure,
	BDSIG_ExhalationValvePressureCmd,

	BDSIG_PeepCntrlrFeedbackPressure,

	BDSIG_InspLungBtpsVolume,
	BDSIG_ExhLungBtpsVolume,
	BDSIG_ExhValvDacPortRegisterImage,
	BDSIG_ExhValvNegativeDacPortRegisterImage,
	BDSIG_VolumeTargetedControllerCPatient,
	BDSIG_VolumeTargetedControllerCPatientFiltered,

	BDSIG_Fio2MonitorCount,
	BDSIG_Fio2MonitorPercentO2,
	BDSIG_AtmosphericPressureSensorValue,
	BDSIG_AtmosphericPressureSensorCount,

	BDSIG_SmFlowControllerFlowTarget,
	BDSIG_SmPressureControllerPressureTarget,

	BDSIG_InspVolumeControllerDesiredFlow,

	BDSIG_InspPsolCmd,
	BDSIG_REVDampingGainDacPortRegisterImg,

	BDSIG_InspGasTmpSensorPredictedTemp,
	BDSIG_ExhGasTmpSensorPredictedTemp,
	BDSIG_PhaseType,

	BDSIG_ExhValveCurrent,
	BDSIG_PoppetPosition,
	BDSIG_PoppetVelocity,
	BDSIG_PoppetAccelerationCounts,

	BDSIG_SmFlowController,
	BDSIG_SmPressureController,

	BDSIG_ExhFlowSensorCount,
	BDSIG_inspFlowSensorCounts,
	BDSIG_inspTempSensorCounts,
	BDSIG_inspPsolCurrentCounts,

	BDSIG_VolumeControllerType,
	BDSIG_VcvPhaseDesiredVolume,
	BDSIG_VolumeControllerDesiredVolume,
	BDSIG_VolumeControllerDeliveredVolume,
	BDSIG_VcvPhaseTubingCompliance,
	BDSIG_VcvPhaseComplianceDeltaP,
	BDSIG_PressWyeUnfiltered,
	BDSIG_EEP,
	BDSIG_EIP,
	BDSIG_VcvPhaseAdiabaticFactor,
	BDSIG_CircuitCompInspTime,

	BDSIG_patientPressure,
	BDSIG_lungPressure,
	BDSIG_netFlow,
	BDSIG_netVolume,
	BDSIG_lungVolume,

	BDSIG_compensationDesiredPressure,
	BDSIG_pidEstimationTerm,
	BDSIG_pidDerivativeTerm,
	BDSIG_pidProportionalTerm,
	BDSIG_pidComplianceTerm,
	BDSIG_pidFlowCommand,

	BDSIG_peepCtrlPressureTerm,
	BDSIG_InspLungResistance,
	BDSIG_InspLungCompliance,
	BDSIG_InspLungDynamicResistance,
	BDSIG_InspLungDynamicCompliance,
	BDSIG_InspGasTmpSensor,
	BDSIG_ExhGasTmpSensor,
	BDSIG_SpirometryConditions,
	BDSIG_ExhFlowSensorLatchedTemp,
	BDSIG_ExhFlowSensorSkewAdjustTemp,
	BDSIG_NetInspVolume,
	BDSIG_NetExhVolume,
	BDSIG_ComplianceVolume,
	BDSIG_VteEIP,
	BDSIG_VTE,
	BDSIG_VTE_MAND,
	BDSIG_VTE_SPONT,
	BDSIG_ExhFlowSensorRaw,
	BDSIG_ExhFlowSensorSkewAdjusted,
	BDSIG_ExhFlowSensorSlope,
	BDSIG_ExhFlowActiveOffset,
	BDSIG_ExhVolumeOffset,
	BDSIG_VteAdiabaticFactor,
	BDSIG_IsExhFlowFinished,
	BDSIG_InspFlowSensorVf,
	BDSIG_ExhFlowSensorVf,
	BDSIG_InspGasTmpSensorVt,
	BDSIG_ExhGasTmpSensorVt,
	BDSIG_BiLevelFlag,
	BDSIG_BiLevelMandVolume,
	BDSIG_BiLevelNullInsp,
	BDSIG_NetFlowSkew,
	BDSIG_VtePeakHold,
	BDSIG_InspPressSensorCounts,
	BDSIG_ExhPressSensorCounts,

	BDSIG_SmTurbulenceStrength,
	BDSIG_SmTurbulenceStrengthLimit,
	BDSIG_EvLvdtTemperature,

	// Mixer Parameters
	BDSIG_mixSetPoint,
	BDSIG_mixO2AirPressure,

	BDSIG_mixDACWrapADC1,
	BDSIG_mixPressureAirInletBuffer,
	BDSIG_mixFlowQairMixSignalBuffer,
	BDSIG_mixTempQairMixSignalBuffer,
	BDSIG_mixPressureO2InletBuffer,
	BDSIG_mixFlowQO2MixSignalBuffer,
	BDSIG_mixTempQO2MixSignalBuffer,
	BDSIG_mixDACWrapADC2,
	BDSIG_mixPressureHelioxInletBuffer,
	BDSIG_mixFlowQHelioxMixSignalBuffer,
	BDSIG_mixTempQHelioxMixSignalBuffer,
	BDSIG_mixISolAccDump,
	BDSIG_mixTestAdc,
	BDSIG_mixPlus12VDisplayedScaled,
	BDSIG_mixDACWrapADC3,
	BDSIG_mixMinus12VScaled,
	BDSIG_mixMinus5VScaled,
	BDSIG_mixPlus5VScaled,
	BDSIG_mixPlus12VAnaScaled,
	BDSIG_mixPlus12VScaled,
	BDSIG_mixPlus24VScaled,
	BDSIG_mixAccPressBuffer,
	BDSIG_mixDACWrapADC4,
	BDSIG_mixAirMixPsolISig,
	BDSIG_mixO2MixPsolISig,
	BDSIG_mixHelioxMixPsolISig,

	BDSIG_mixCalcAirFlow,
	BDSIG_mixCalcO2Flow,
	BDSIG_mixCalcHelioxFlow,
	BDSIG_mixCalcAirQCmd,
	BDSIG_mixCalcO2QCmd,
	BDSIG_mixCalcHEO2QCmd,
	BDSIG_mixCalcAirPwmCmd,
	BDSIG_mixCalcO2PwmCmd,
	BDSIG_mixCalcHEO2PwmCmd,
	BDSIG_mixCalcAccPressure,
	BDSIG_mixCalcPressureControllerOutput,
	BDSIG_mixCalcEstimatedMixPercentage,
	BDSIG_mixCalcMixControllerOutput,

	BDSIG_mixAirFlow,
	BDSIG_mixO2Flow,
	BDSIG_mixAirPressure,
	BDSIG_mixO2Pressure,

	// IESYNC Parameters
	BDSIG_ieSyncTheta0,
	BDSIG_ieSyncTheta1,
	BDSIG_ieSyncTheta2,
	BDSIG_ieSyncPhiD,
	BDSIG_ieSyncTubingCompliance,
	BDSIG_ieSyncLungCompliance,
	BDSIG_ieSyncPatientResistance,
	BDSIG_ieSyncDeltaInterpleuralP,
	BDSIG_ieSyncIESignal,
	BDSIG_ieSyncCovarianceP0,
	BDSIG_ieSyncCovarianceP1,
	BDSIG_ieSyncCovarianceP2,
	BDSIG_ieSyncPhi0,
	BDSIG_ieSyncPhi1,
	BDSIG_ieSyncPressureY,
	BDSIG_ieSyncEpsilon,

	BDSIG_complianceSlopeLogPress,
	BDSIG_complianceInterceptLogPress,
	BDSIG_inspCompVolumeMl,
	BDSIG_totalVolumeMl,

    // Option Board Interfaces (PROX & Capnostat)
	BDSIG_ProxFlow,
	BDSIG_ProxPressure,
	BDSIG_CO2Value,
	BDSIG_VolumeCO2ExpiredPerMinute,
	BDSIG_EndTidalCO2,
	BDSIG_InspiredCO2,
	BDSIG_MixedExpiredCO2,
	BDSIG_ProxBarometricPressure,
	BDSIG_ProxStartOfInspirationMark,
	BDSIG_ProxStartOfExpirationMark,
	BDSIG_ProxInspVolume,
	BDSIG_ProxExhVolume,
	BDSIG_ProxInspiredTime,
	BDSIG_ProxExpiredTime,
	BDSIG_ProxCapnostatStatus,
    BDSIG_ProxDataValid,
    BDSIG_ProxHardwareStatus,
    BDSIG_ProxAccumPressure,
    BDSIG_ProxSensorType,
    BDSIG_ProxFlowWithATM,
    BDSIG_IsProxAvailable,
    BDSIG_ProxEnableSettingValue,

    BDSIG_IsProxPresent,
    BDSIG_IsProxCapBusy,
    BDSIG_IsCapnostatPresent,
    BDSIG_IsOptionSvShutoffRequested,
    BDSIG_IsProxOverPressurePresent,
    BDSIG_IsProxPurgeChargeTimeout,

    BDSIG_ieSyncPreviousBreathOffset,
    BDSIG_ieSyncRunningOffset,
    BDSIG_ieSyncIsInitialized,
    BDSIG_DeltaInterpleuralPwithOffset,
    BDSIG_InspBtpsCf,
    BDSIG_ExpBtpsCf,

    BDSIG_DesiredFlowOutput,

    BDSIG_P100State,
    BDSIG_VitalCapacityState,
    BDSIG_NifState,
    BDSIG_NetFlowInspTrigger,
    BDSIG_PressureBackupInspTrigger,
    BDSIG_PressureInspTrigger,
    BDSIG_ArmManeuverTrigger,
    BDSIG_DisarmManeuverTrigger,
    BDSIG_P100CompleteTrigger,

    BDSIG_moduleIO1,
    BDSIG_moduleIO2,
    BDSIG_moduleIO3,
    BDSIG_moduleIO4,
    BDSIG_moduleIO5,
    BDSIG_moduleIO6,
    BDSIG_moduleIO7,
    BDSIG_moduleIO8,
    BDSIG_moduleIO9,
    BDSIG_moduleIO10,
    BDSIG_moduleIO11,
    BDSIG_moduleIO12,
    BDSIG_moduleIO13,
    BDSIG_moduleIO14,
    BDSIG_moduleIO15,
    BDSIG_moduleIO16,
    BDSIG_moduleIO17,
    BDSIG_moduleIO18,

	NUM_BDSIGS,
	BDSIG_NONE
} BdSignalEnum;

#endif // BDSIGNALENUM_HH_
