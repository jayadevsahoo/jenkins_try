#ifndef PressureController_IN
#define PressureController_IN
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
// Class: PressureController - Implements a pressure controller using a
//		PSOL for flow delivery.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/PressureController.inv   25.0.4.0   19 Nov 2013 14:00:04   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006   By: syw   Date:  14-Sep-2000    DR Number: 5695
//  Project:  VTPC
//  Description:
//		VTPC project-related changes:
//		Added getDesiredPressure().
//
//  Revision: 005  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 004 By: syw    Date: 03-Jul-1996   DR Number: 1073
//  	Project:  Sigma (R8027)
//		Description:
//			Added setInitIntegratorValue() method.
//
//  Revision: 003 By: syw    Date: 06-Feb-1996   DR Number: 600
//  	Project:  Sigma (R8027)
//		Description:
//			Added getFlowCommadLimit() method.
//
//  Revision: 002 By: syw    Date: 26-Feb-1996   DR Number: DCS 600
//  	Project:  Sigma (R8027)
//		Description:
//			Controls Rev 04 changes.
//
//  Revision: 001  By:  syw    Date:  26-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version.
//
//=====================================================================

//=====================================================================
//
//  Public Methods...
//
//=====================================================================
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:	setKp()
//
//@ Interface-Description
//		This method has the proportional gain as an argument and has no return
//		value.  This method is called to store the value of the proportional gain
//		that is passed in.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The integrator_ is adjusted to avoid discontinuities.  The 
//		data member kp_ is set to the value passed in. 
//		$[04308]
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

inline void
PressureController::setKp( const Real32 kp)
{
	CALL_TRACE("PressureController::setKp( const Real32 kp)") ;
	
	// $[TI1]
	integrator_ += (kp_ * error_) / ki_ ;
	
#if CONTROLS_TUNING
	//Save kp to rollback integrator
	Real32 oldKp = kp_;
	
	kp_ = kp ;
	
	if (!IsEquivalent(kp, 0.0, TENTHS))
	{
		integrator_ -= (oldKp * error_) / ki_ ;
		overrideKp_ = TRUE;
	}
#endif	
}

//=====================================================================
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:	setKi()
//
//@ Interface-Description
//		This method has the proportional gain as an argument and has no return
//		value.  This method is called to store the value of the proportional gain
//		that is passed in.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The integrator_ is adjusted to avoid discontinuities.  The 
//		data member ki_ is set to the value passed in. 
//		$[04308]
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

inline void
PressureController::setKi( const Real32 ki)
{
	CALL_TRACE("PressureController::setKi( const Real32 ki)") ;
	
	ki_ = ki ;
	
#if CONTROLS_TUNING
	overrideKi_ = TRUE;
#endif	
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:	getWf()
//
//@ Interface-Description
//		This method has no arguments and returns the weighting factor.
//---------------------------------------------------------------------
//@ Implementation-Description
//		wf_ is returned.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
inline Real32
PressureController::getWf( void) const
{
	CALL_TRACE("PressureController::getWf( void)") ;

	// $[TI1]
	return( wf_) ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:	getErrorGain()
//
//@ Interface-Description
//		This method has no arguments and returns the error gain.
//---------------------------------------------------------------------
//@ Implementation-Description
//		errorGain_ is returned.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
inline Real32
PressureController::getErrorGain( void) const
{
	CALL_TRACE("PressureController::getErrorGain( void)") ;

	// $[TI1]
	return( errorGain_) ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:	getFlowCommandLimit()
//
//@ Interface-Description
//		This method has no arguments and returns the flow command limit.
//---------------------------------------------------------------------
//@ Implementation-Description
//		flowCommandLimit_ is returned.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
inline Real32
PressureController::getFlowCommandLimit( void) const
{
	CALL_TRACE("PressureController::getFlowCommandLimit( void)") ;

	// $[TI1]
	return( flowCommandLimit_) ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setInitIntegratorValue
//
//@ Interface-Description
//		This method has the last desired flow from the flow controller as an
//		argument and has no return value.  This method is called to set the
//		initial integrator value.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The initIntegratorValue_ data member is set to the value passed in
//		divided by ki.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
inline void
PressureController::setInitIntegratorValue( const Real32 lastDesiredFlow)
{
	CALL_TRACE("PressureController::setInitIntegratorValue( const Real32 lastDesiredFlow)") ;
	
	// $[TI1]
	initIntegratorValue_ = lastDesiredFlow / ki_ ;
}
 
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getDesiredPressure
//
//@ Interface-Description
//		This method has no arguments and returns the desired pressure.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Return desiredPressure_ data member
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
inline Real32
PressureController::getDesiredPressure( void) const
{
	CALL_TRACE("PressureController::getDesiredPressure( void)") ;

	// $[TI1]
	return( desiredPressure_) ;
}

//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//=====================================================================
//
//  Private Methods...
//
//=====================================================================
									  
#endif // PressureController_IN





