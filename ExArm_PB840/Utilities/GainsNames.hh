//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file  GainsNames.hh
/// @brief Definition of the GainsNames enum
//----------------------------------------------------------------------------
#ifndef GAINSNAMES_HH_
#define GAINSNAMES_HH_

//-------------------------------------
// I N C L U D E S
//-------------------------------------

//----------------------------------------------------------------------------
// GAINS MANAGER TOUCH POINT for adding gains, constants, variables
// for manipulation.
//----------------------------------------------------------------------------

//-------------------------------------
// N A M E   S P A C E
//-------------------------------------
namespace GainsManagerNS
{
    //-------------------------------------
    // S T R U C T   D E F I N I T I O N
    //-------------------------------------
    typedef enum GainsNames
    {
        GAINS_NAMES_NO_NAME,

        // PEEP CONTROLLER
		PEEP_KI,
        GAINS_NAMES_LOW = PEEP_KI,        

        // FLOW CONTROLLER
        FLOW_A_FACTOR,
        FLOW_B_FACTOR,
        FLOW_C_FACTOR,
        FLOW_D_FACTOR,

        // PRESSURE CONTROLLER
        PRESSURE_KI,
		PRESSURE_KP,

		// PID PRESSURE CONTROLLER
		PID_KI,
		PID_KP,
        PID_KD,

        // PCV Pressure Based Phase
        PCV_PED_KP,
        PCV_ADU_KP,
        PCV_PED_KI,
        PCV_ADU_KI,

        GAINS_NAMES_HIGH = PCV_ADU_KI

    } GainsNames;

    typedef enum PressureBasedPhases
    {
        NO_PHASE,
        OSCBILEVEL,
        OSCPCV,
        PCV,
        PSV
    };

    typedef enum PressureBasedPhaseGainsNames
    {
        NO_PBP_GAIN,
        PEDIATRIC_CIRCUIT_KP,
        ADULT_CIRCUIT_KP,
        PEDIATRIC_CIRCUIT_KI,
        ADULT_CIRCUIT_KI,
    };
}
#endif // GAINSNAMES_HH_
