#ifndef AuxResultLog_HH
#define AuxResultLog_HH
//====================================================================
// This is a proprietary work to which Nellcor Puritan Bennett
// corporation of California claims exclusive right.  No part of this
// work may be used, disclosed, reproduced, sorted in an information
// retrieval system, or transmitted by any means, electronic,
// mechanical, photocopying, recording, or otherwise without the prior
// written permission of Nellcor Puritan Bennett Corporation.
//
//     Copyright (c) 1995, Nellcor Puritan Bennett Corporation
//=====================================================================


//============================== C L A S S   D E S C R I P T I O N ====
// Class: AuxResultLog - POST Auxiliary Results Log
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/POST-Library/vcssrc/AuxResultLog.hhv   25.0.4.0   19 Nov 2013 14:16:36   pvcs  $
//
//@ Modification-Log
//
//   Revision 001   By: Gary Cederquist  Date: 12-MAY-1997  DR Number: 2085
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version
//
//=====================================================================

#include "Sigma.hh"
#include "AuxResult.hh"

class  AuxResultLog
{
  public:
    AuxResultLog();
    ~AuxResultLog();

    void       add( const AuxResult & auxResult );
    Boolean    verify(void);
    Boolean    isInitialized(void);
    const AuxResult * getLastResult( const Uint8 testNumber );

    static AuxResultLog & GetPostAuxResultLog(void);
 
  private:
    enum
    {
        NUM_AUX_RESULTS = 16
    };
 
    Boolean       initialized_;
    Uint8         nextEntry_;
    Uint8         pad0;
    Uint8         pad1;
    AuxResult     auxResult_[AuxResultLog::NUM_AUX_RESULTS];
};

#endif  // AuxResultLog_HH
