#ifndef CompactFlashTestSubScreen_HH
#define CompactFlashTestSubScreen_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: CompactFlashTestSubScreen - Compact Flash test service mode
//                                    sub-screen.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/CompactFlashTestSubScreen.hhv   25.0.4.0   19 Nov 2013 14:07:40   pvcs  $
//
//
//@ Modification-Log
//
//  Revision: 001  By:  gdc	   Date:  23-Mar-2007    SCR Number: 6237
//  Project:  Trend
//  Description:
//		Initial version.
//
//====================================================================

#include "SubScreen.hh"
#include "GuiTimerTarget.hh"
#include "AdjustPanelTarget.hh"
#include "GuiTestManagerTarget.hh"

//@ Usage-Classes
#include "GuiTestManager.hh"
#include "ServiceStatusArea.hh"
#include "SmPromptId.hh"
#include "SubScreenTitleArea.hh"
#include "TextButton.hh"
#include "GuiAppClassIds.hh"
class TestResult;
//@ End-Usage

class CompactFlashTestSubScreen 
:   public SubScreen, 
	public GuiTimerTarget,
	public AdjustPanelTarget,
	public GuiTestManagerTarget 
{
public:
	CompactFlashTestSubScreen(SubScreenArea *pSubScreenArea);
	~CompactFlashTestSubScreen(void);

	// SubScreen virtual 
	virtual void activate(void);
	virtual void deactivate(void);

	// ButtonTarget virtual
	virtual void buttonDownHappened(Button *pButton,
									Boolean byOperatorAction);
	virtual void buttonUpHappened(Button *pButton,
								  Boolean byOperatorAction);

	// AdjustPanelTarget virtual
	virtual void adjustPanelAcceptPressHappened(void);
	virtual void adjustPanelRestoreFocusHappened(void);

	// GuiTestManagerTarget virtual
	virtual void processTestPrompt(Int command);
	virtual void processTestKeyAllowed(Int keyAllowed);
	virtual void processTestResultStatus(Int resultStatus, Int testResultId=-1);
	virtual void processTestResultCondition(Int resultCondition);
	virtual void processTestData(Int dataIndex, TestResult *pResult);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:

	private:
	enum TestConditionId
	{
		MAX_CONDITION_ID = 2
	};

	enum TestScreenId
	{
		TEST_PROMPT_ACCEPTED_CLEARED,
		TEST_SETUP,
		TEST_START_ACCEPTED,
		MAX_TEST_SCREENS
	};

	enum TestStateId
	{
		PROMPT_ACCEPT_BUTTON_PRESSED,
		START_BUTTON_PRESSED,
		UNDEFINED_TEST_STATE,
		MAX_TEST_STATES
	};

	// these methods are purposely declared, but not implemented...
	CompactFlashTestSubScreen(void);			 // not implemented...
	CompactFlashTestSubScreen(const CompactFlashTestSubScreen&);	// not implemented...
	void operator=(const CompactFlashTestSubScreen&);	 // not implemented...

	void layoutScreen_(TestScreenId testScreenId);
	void setErrorTable_(void);
	void startTest_(SmTestId::ServiceModeTestId currentTestId);

	//@ Data-Member: testStatus_
	// A container for displaying the current Compact Flash Test status
	ServiceStatusArea testStatus_;

	//@ Data-Member: errDisplayArea_
	// An array of TextField objects used to display test conditions in the
	// lower screen
	TextField errDisplayArea_[MAX_CONDITION_ID];

	//@ Data-Member: conditionText_
	// Text for error condition prompts
	StringId conditionText_[MAX_CONDITION_ID];

	//@ Data-Member: errorHappened_
	// Flag that indicates status of test
	Int errorHappened_;

	//@ Data-Member: errorIndex_
	// Index into conditionText_ in the error display area
	Int errorIndex_;

	//@ Data-Member: keyAllowedId_
	// Contains the expected user key response ID.
	Int keyAllowedId_;

	//@ Data-Member: userKeyPressedId_
	// Holds the operator pressed key Id.
	SmPromptId::ActionId userKeyPressedId_;

	//@ Data-Member: startTestButton_
	// The Start Test button used to start the Compact Flash Test
	TextButton startTestButton_;

	//@ Data-Member: testStateId_
	// The test status identifier.
	TestStateId testStateId_;

	//@ Data-Member: titleArea_
	// The sub-screen's title at the top left of the screen.
	SubScreenTitleArea titleArea_;

	//@ Data-Member: guiTestMgr_ 
	// Object instance of GuiTestManager
	GuiTestManager guiTestMgr_;

	//@ Data-Member: currentTestId_
	// Object instance of GuiTestManager
	SmTestId::ServiceModeTestId currentTestId_;

};

#endif // CompactFlashTestSubScreen_HH 
