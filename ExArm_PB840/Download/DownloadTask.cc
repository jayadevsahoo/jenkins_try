#include "stdafx.h"
#if	defined(SIGMA_TARGET)
//=====================================================================
//   This is a proprietary work to which Puritan-Bennett corporation
//   of California claims exclusive right.  No part of this work may
//  be used, disclosed, reproduced, sorted in an information retrieval
//     system, or transmitted by any means, electronic, mechanical,
//   photocopying, recording, or otherwise without the prior written
//      permission of Puritan-Bennett Corporation of California.
//
//          Copyright (c) 1996, Nellcor/Puritan-Bennett Corporation
//=====================================================================


//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: DownloadTask - Handles all the download tasks.
//---------------------------------------------------------------------
//@ Interface-Description
//	  Based on the TaskEntry table, the class creates VRTX tasks
//	  for the download system.
//---------------------------------------------------------------------
//@ Rationale
//	 Encapsulates all the activities of creating and maintaining VRTX
//	 tasks.
//---------------------------------------------------------------------
//@ Implementation-Description
//   VRTX OS has to know which application routines to call whenever
//   it creates a new VRTX task, delete a VRTX task or do a context
//   switch.
//   Stackware and Flash Burner tasks have to run as a supervisor
//   mode to access some of the system IO registers.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions  and pre-conditions are used to
//  ensure correctness of software conditons.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Download/vcssrc/DownloadTask.ccv   25.0.4.0   19 Nov 2013 14:02:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date: 15-Jan-99    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//   Revision 001   By:   jhv      Date: 23/Feb/96    DR Number:   n/a  
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//=====================================================================

//=====================================================================
//
//      Includes...
//
//=====================================================================

#include "DownloadTask.hh"
#include <string.h>
#ifdef SIGMA_DEVELOPMENT
#  include <stdio.h>
#endif // SIGMA_DEVELOPMENT

#ifdef SIGMA_UNIT_TEST
extern void TestDownloadTask(void);
extern void TestFlashMemTask(void);
#define DOWNLOAD_TASK_NAME TestDownloadTask
#define FLASHMEM_TASK_NAME TestFlashMemTask
#else
#define DOWNLOAD_TASK_NAME Download::Task
#define FLASHMEM_TASK_NAME FlashMem::Task
#endif // !SIGMA_UNIT_TEST

class FlashMem   {public: static void Task(void); };
class Download   {public: static void Task(void); };
extern void StackWareTask(void);


//=====================================================================
//
//      Static Data...
//
//=====================================================================

//  local constants 

const Int MAX_OS_TASKS = 8;
const Int OS_TASK_STACK_SIZE = 10*1024;
const Int SYSTEM_STACK_SIZE = 2*1024;
DownloadTask *DownloadTask::PTasks_[NUM_TASKS+1];

//  local typedefs

typedef struct 
{
    unsigned char    data[28];
} StackFrame;

typedef struct
{
    TCB *            pTcb;
    unsigned char    id;
    unsigned char *  pSsp;
    unsigned char *  pUsp;
    unsigned char *  pStk;
} TcbPointers;

unsigned char Stack_[NUM_TASKS][TASK_STACK_SIZE];
static Uint32 TaskMem_[NUM_TASKS] [ ( sizeof(DownloadTask) + 
			sizeof(Uint32) - 1)/sizeof(Uint32)];

//@ Code...

//=====================================================================
//
//      Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: DownloadTask(TaskId taskId, TaskMode exMode,
//            unsigned char *pStack, TaskPri priority, EntryPtr pEnter)
//
//@ Interface-Description
//		Contructor for a DownloadTask object.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
DownloadTask::DownloadTask(TaskId taskId, TaskMode exMode,
	unsigned char *pStack, TaskPri priority, EntryPtr pEnter)
{
 	// $[TI1]
 	taskId_ = taskId;
	priority_ = priority;
	entryPoint_ = pEnter;
	pStack_ = (unsigned char *)pStack;
	exMode_  = exMode;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: createTask_(void)
//
//@ Interface-Description
//		Create a VRTX task. 
//---------------------------------------------------------------------
//@ Implementation-Description
//   The ::sc_tcreate VRTX system call creates a Supervisor mode task.
//   The ::sys_sc_tcreate VRTX system call creates a User mode task.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
DownloadTask::createTask_(void)
{
    int err;

    // Create a Supervisor or User task.  
    if ( exMode_ == SUPERVISOR_MODE ) // $[TI1]
    {
        ::sc_tcreate(DownloadTask::Enter_, taskId_, priority_, &err);
    }
    else // $[TI2]
    {
        ::sys_sc_tcreateu(DownloadTask::Enter_, taskId_, priority_, &err);
    }
    CLASS_ASSERTION(err == RET_OK);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TaskCreateHook( TCB * pCreateeTcb, TCB * )
//
//@ Interface-Description
//  The TCreateHook_ method is called by VRTX as a part of VRTX 
//  task creation ( ::sc_tcreate and ::sys_sc_tcreateu VRTX calls) and provide
//  a user opportunity to assign to a new stack area for the new task
//  and initialize its contents from the temporary stack provided by VRTX.
//  The stack contents are initialized to its initial memory pattern to
//  debug a stack overflow condition.
//
//  For non-download tasks such as debuggers, this method reserves
//  one of the stacks in the combinedStack memory.  Each task is either
//  a user mode or supervisory mode task as indicated by the supervisor
//  bit in the task's status register.  For supervisor mode tasks,
//  the entire stack area is reserved for the supervisor stack.
//  For user mode tasks, the stack area is split into a user stack 
//  and a small supervisor stack ( 2K bytes on top ).
//  Upon completing this allocation, this method copies the contents
//  of the temporary stack provided by VRTX to the new stack area.  It
//  then adjusts the task's TCB pointers to reference the new stack 
//  area(s) and returns.
//---------------------------------------------------------------------
//@ Implementation-Description
//  References the supervisor bit in the task's status register (SR) 
//  located on the temporary stack to determine if a task is a 
//  supervisor or user mode task.  If the TCB stack base pointer is
//  not NULL, VRTX has already allocated a stack and this method exits.
//  If the task id indicates this task was started from an entry in 
//  the task table, the method uses the stack area and size contained
//  in the TaskInfo object for this task id.  For unmanaged tasks, the
//  next stack in the combinedStack area is allocated and assigned to
//  the task.  Splits the stack area into separate user and supervisor 
//  stacks for user mode tasks.  Maintains a single supervisor stack
//  for supervisor tasks.
//  Copies the temporary stack frame created by VRTX to the new 
//  supervisor stack and updates the stack pointers in the TCB.
//  VRTX services may not be called from any VRTX task hook function.
//---------------------------------------------------------------------
//@ PreCondition
//  pCreateeTcb != NULL
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
DownloadTask::TaskCreateHook( TCB * pCreateeTcb, TCB * )
{
static int IStack = 0;
static unsigned char  OsStack_[MAX_OS_TASKS][OS_TASK_STACK_SIZE];
unsigned char * pStackBottom;
size_t          stackSize;

CLASS_PRE_CONDITION(pCreateeTcb != NULL);

StackFrame *  pOsStackFrame = (StackFrame *)(pCreateeTcb->tbssp);
Boolean superMode = pOsStackFrame->data[20] & 0x20;

    //  Allocate stack only if the OS has not allocated one already
    if (pCreateeTcb->tbstack == NULL) // $[TI1]
    {
        TaskId taskId  = (TaskId) pCreateeTcb->tbid;

        if ( IsDownloadTaskId( taskId) ) // $[TI1.1]
        {
            pStackBottom = PTasks_[taskId]->getStack();
            stackSize = TASK_STACK_SIZE;
        }
        else // $[TI1.2]
        {
            //  Spectra tasks
			CLASS_ASSERTION(IStack < MAX_OS_TASKS);
            pStackBottom =  OsStack_[++IStack];
            stackSize = OS_TASK_STACK_SIZE; 
        }
        pCreateeTcb->tbssp = pStackBottom + stackSize;

        //  If the stack must be initialized, the following
        //  memset should be broken up to strobe the watchdog timer 
        //  at a minimum of every 60ms.  Otherwise the watchdog fires
        //  and resets the processor.  Task hooks execute with interrupts
        //  disabled.
#if defined(SIGMA_DEVELOPMENT)
        memset(pStackBottom, STACK_MAGIC_COOKIE, stackSize);
#endif // SIGMA_DEVELOPMENT

        if (!superMode) // $[TI1.3]
        {
			// Allocate the top "SYSTEM_STACK_SIZE" bytes as the supervisor 
			// mode stack space to handle interrupts.
            pCreateeTcb->tbusp = pCreateeTcb->tbssp - SYSTEM_STACK_SIZE;
        } // else $[TI1.4]


        //  copy stack frame contents from the temporary stack to the new stack
        StackFrame *  pNewFrame = (StackFrame *)(pCreateeTcb->tbssp);
        memcpy( --pNewFrame, pOsStackFrame, sizeof(StackFrame) );
        pCreateeTcb->tbssp = (unsigned char *)pNewFrame;
    } // else $[TI2]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TaskDeleteHook( TCB *, TCB * )
//
//@ Interface-Description
//  The TDeleteHook_ method is called by VRTX as a part of VRTX 
//  task deletion process. The method does nothing since the system
//  never deletes a task until it resets.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
DownloadTask::TaskDeleteHook( TCB *, TCB * )
{
	// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:TaskSwitchHook( TCB *pTCB, TCB * )
//
//@ Interface-Description
//  The TaskSwitchHook_ method is called by VRTX as a part of VRTX 
//  task switch process, but we don't have to anything.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
DownloadTask::TaskSwitchHook( TCB *, TCB * )
{
	// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: CreateTasks(void)
//
//@ Interface-Description
//  Registers Task management routines to the VRTX OS.
//  Initialize the static PTasks_ table with all the DownloadTask
//  objects and creates all the download tasks.  
//---------------------------------------------------------------------
//@ Implementation-Description
//   VRTX OS has to know which application routines to call whenever
//   it creates a new VRTX task, delete a VRTX task and do a context
//   switch.
//   Stackware and Flash Burner tasks have to run as a supervisor
//   mode to access some of the system IO registers.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
DownloadTask::CreateTasks(void)
{
	// $[TI1]
	sys_insert_hooks( (void *) DownloadTask::TaskCreateHook,
		(void *) DownloadTask::TaskDeleteHook, 
			(void *) DownloadTask::TaskSwitchHook);
	PTasks_[STACKWARE_TASK_ID] = 
		new ( TaskMem_[STACKWARE_TASK_ID -1 ])
			DownloadTask(STACKWARE_TASK_ID, SUPERVISOR_MODE, Stack_[STACKWARE_TASK_ID -1],
				STACKWARE_PRIORITY, ( EntryPtr )StackWareTask);
	PTasks_[STACKWARE_TASK_ID]->createTask_();

	PTasks_[TFTP_CLIENT_ID] = 
		new ( TaskMem_[TFTP_CLIENT_ID -1 ])
			DownloadTask(TFTP_CLIENT_ID, SUPERVISOR_MODE, Stack_[TFTP_CLIENT_ID -1],
				TFTP_CLIENT_PRIORITY,  (EntryPtr) DOWNLOAD_TASK_NAME);
	PTasks_[TFTP_CLIENT_ID]->createTask_(); 

#if defined(SIGMA_UNIT_TEST)
extern void TestMain(void);
extern void TestUserTask(void);
	PTasks_[TESTMAIN_ID] = 
		new ( TaskMem_[TESTMAIN_ID -1 ])
			DownloadTask(TESTMAIN_ID, SUPERVISOR_MODE, Stack_[TESTMAIN_ID -1],
				TESTMAIN_PRIORITY,  (EntryPtr) TestMain);
	PTasks_[TESTMAIN_ID]->createTask_(); 

	PTasks_[TESTUSER_ID] = 
		new ( TaskMem_[TESTUSER_ID -1 ])
			DownloadTask(TESTUSER_ID, USER_MODE, Stack_[TESTUSER_ID -1],
				TESTUSER_PRIORITY,  (EntryPtr) TestUserTask);
	PTasks_[TESTUSER_ID]->createTask_(); 
#endif // SIGMA_UNIT_TEST

	PTasks_[FLASH_BURNER_ID] = 
		new ( TaskMem_[FLASH_BURNER_ID -1 ])
			DownloadTask(FLASH_BURNER_ID, SUPERVISOR_MODE, Stack_[FLASH_BURNER_ID -1],
				FLASH_BURNER_PRIORITY,  (EntryPtr)FLASHMEM_TASK_NAME);
	PTasks_[FLASH_BURNER_ID]->createTask_();

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Enter_(void)
//
//@ Interface-Description
//		Vrtx task entry point for every task.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
DownloadTask::Enter_(void)
{
    TaskInfo vrtxinfo;
    Int32        err;

	// $[TI1]
    ::sc_tinquiry((int*)&vrtxinfo, 0, &err);
    CLASS_ASSERTION(err == RET_OK);

    CLASS_ASSERTION(IsDownloadTaskId(vrtxinfo.id));
    CLASS_ASSERTION(*PTasks_[vrtxinfo.id]->entryPoint_);

    // call the task's entry point
    (*PTasks_[vrtxinfo.id]->entryPoint_)();
 
    // A task should not return. Assert if the task returns.
    // VRTX does not know how to handle a return from a task.
    CLASS_ASSERTION_FAILURE();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
DownloadTask::SoftFault(const FaultType  softFaultID,
                   const Uint32       lineNumber,
           const char*        pFileName,
           const char*        pPredicate)
{
  DownloadFault::SoftFault(softFaultID, DOWNLOAD,
        DOWNLOAD_TASK_CLASS, lineNumber, pFileName, pPredicate); 
}

//=====================================================================
//
//      Protected Methods...
//
//=====================================================================

//=====================================================================
//
//      Private Methods...
//
//=====================================================================

#endif //defined(SIGMA_TARGET)
