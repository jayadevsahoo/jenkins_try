#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================ C L A S S     D E S C R I P T I O N ====
//@ Filename: wdt.cc - Watchdog timer test
//
//---------------------------------------------------------------------
//@ Interface-Description
//  >Von
//    The watchdog timer is verified in this module by a test that
//    generates an intentional watchdog CPU reset.  Two timers measure
//    the minimum and maximum limits of the watchdog device.
//
//  Free-Functions:
//
//    WatchdogTimerTest             - Sets up WDT test
//    WatchdogTestPass1             - Pass one
//    WatchdogTestPass2             - Pass two
//    WatchdogMinInterruptHandler   - interrupt handler connected to the
//                                    interrupt from the min timer
//    WatchdogMaxInterruptHandler   - interrupt handler connected to the
//                                    interrupt from the max timer
//    DisableWatchdog               - Installs timer interrupt to disable
//                                    the watchdog.
//
//---------------------------------------------------------------------
//@ Rationale
//    This implements the watchdog timer verification of POST.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//    The watchdog timer is a separate circuit designed to reset the CPU if
//    the applications software does not strobe it  within an acceptable
//    time period. This test generates an intentional timeout and measures
//    whether the circuit resets too late, too early, or not at all.
//    A two pass cycle is designed for this test, a pass to initialize
//    variables and timers, followed by a second pass to verify the reset
//    occurred in its specified time.  A latched hardware bit specific to
//    the watchdog reset function is also verified.
//
//    Once the watchdog is verified a timer interrupt is installed to
//    periodically strobe the watchdog for the remainder of POST.  This test
//    is performed early in the kernel for several reasons; prior to the
//    start of this test the POST application must be peppered with macros
//    to strobe the watchdog.    Verifying this circuit early allows the
//    insertion of an automated mechanism, minimizing the amount of
//    individual strobing macros.   Secondly, the tests prior to the watchdog
//    test are performed twice in a two pass cycle, the minimum amount of
//    repeated tests are performed to save time.   It is also desirable to
//    test this circuit early in POST  to determine if the circuit is
//    prematurely resetting the system.  If POST tests this circuit late
//    in the sequence, an early reset failure will not be discernible.
//
//    The watchdog timer circuit is verified to reset the system after 62ms
//    but no later than 250ms when it is not strobed. The latch indicating
//    that a watchdog has occurred is tested to be set and cleared properly.
//    $[00308]
//
//    At some point later in the Kernel, somewhere during the EEPROM checksum
//    test, the watchdog will bark.  The EEPROM checksum test is designed to
//    save its last positional location by writing its routine pointer to
//    the NVRAM location LastTestInProgress.  At the end of the second pass
//    of this test this pointer is used to jump around the previously
//    checksummed blocks of code.  This saves unnecessary retest time.
//
//    If this test passes it then installs an interrupt based on an 82C54
//    timer counter to continue strobing the watchdog through the rest of
//    POST.
//
//    At the end of pass 2, only the counter used to strobe the watchdog is
//    active.  During pass 1, 2 of 3 counters are used, but after pass 2
//    only one counter is active.
//
//  >Voff
//
//---------------------------------------------------------------------
//@ Fault-Handling
//    During pass 1: if the watchdog does not occur, the maximum timer
//    interrupt will fail this test.
//
//    During pass 2: if the minimum timer did not occur during pass 1
//    then the test will fail.  If the hardware register does not
//    indicate a watchdog reset occured, this indicates that either
//    the power failed during the watchdog reset or the hardware
//    register has failed.  In this case, the watchdog test is retried
//    twice more to assure that the hardware latch has failed and that
//    the operator did not simply power off the vent during the watchdog
//    reset.  At the end of the two retries, the hardware latch is
//    declared as failed and the test fails.  If the watchdog bite reset
//    fails to clear the bite, the test will fail.
//
//    KernelError is called if this test fails.
//---------------------------------------------------------------------
//@ Restrictions
//    This test must be called after the system timers have been validated.
//---------------------------------------------------------------------
//@ Invariants
//    None
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Kernel/vcssrc/wdt.ccv   25.0.4.0   19 Nov 2013 14:13:58   pvcs  $
//
//@ Modification-Log
//
//  Revision: 007    By: Gary Cederquist  Date: 26-Apr-1999  DCS Number: 5370
//    Project: 840 Cost Reduction
//    Description:
//       Corrected revision code conflict.
//
//  Revision: 006    By: Gary Cederquist  Date: 14-Dec-1998  DR Number: 5311
//    Project: 840 Cost Reduction
//    Description:
//       Initial version.
//
//  Revision: 005    By: Gary Cederquist  Date: 14-Oct-1997  DR Number: 2553
//    Project: Sigma (R8027)
//    Description:
//		Use watchdog "bite" latch as state of power up during
//	 	phase 2.  Restart the test if the latch is not set to assume
//	 	that power was lost during the watchdog reset phase.
//	 	This resolves the problem encountered when operating on
//	 	degraded oscillating (battery) power that causes a watchdog
//	 	timer test failure.
//
//  Revision: 004    By: Gary Cederquist  Date: 09-SEP-1997  DR Number: 2471
//    Project: Sigma (R8027)
//    Description:
//       Conditionally included misc.hh for DEBUG only.
//
//  Revision: 003    By: Gary Cederquist  Date: 15-AUG-1997  DR Number: 1875
//    Project: Sigma (R8027)
//    Description:
//       Added testable item annotations.
//
//  Revision: 002    By: Gary Cederquist  Date: 25-APR-1997  DR Number: 1996
//    Project: Sigma (R8027)
//    Description:
//       Added missing requirement number.
//
//  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//=====================================================================

#include "wdt.hh"
#include "kernel.hh"
#include "kernex.hh"
#include "postnv.hh"
#include "timertst.hh"
#include "CpuDevice.hh"
#include "Post_Library.hh"
#include "I82C54.hh"
#include "MemoryMap.hh"
//TODO E600 #include "NmiSource.hh"
#include "PbTimer.hh"

#if defined(SIGMA_DEBUG)
# include "misc.hh"
#endif

static Criticality WatchdogTestPass1 (void);
static Criticality WatchdogTestPass2 (void);
static void        WatchdogMinInterruptHandler(void);
static void        WatchdogMaxInterruptHandler(void);


// the following vars do not use Uint so emulator can properly type them
unsigned int  UT_WatchdogTestStart;
unsigned int  UT_WatchdogMinExpired;
unsigned int  UT_WatchdogMaxExpired;
unsigned int  CostReductionBoard_;

static volatile unsigned int  MaxCounter_;
static volatile unsigned int  MinCounter_;

#if defined(SIGMA_PRODUCTION)
const Criticality WDT_SEVERITY = MAJOR;
#else
const Criticality WDT_SEVERITY = PASSED;
#endif


//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Free-Function: WatchdogTimerTest  (void)
//
//@ Interface-Description
//    Tests the watchdog timer by generating, then measuring an expected
//    CPU watchdog reset.  $[11033]
//
//    INPUTS:  NONE
//
//    OUTPUTS: PASSED, MAJOR
//---------------------------------------------------------------------
//@ Implementation-Description
//    This routine selects the appropriate pass of the Watchdog test to
//    implement the following sequence:
//
//    Pass 1 - the NOVRAM variable testingWatchdog = FALSE
//       a. Initializes two timers with two interrupt handlers to be called
//          when the timers expire.
//          1. Timer 1 - set to max specified time for Watchdog timer.
//              If this timer expires that means the Watchdog timer
//              is out of spec or not working. Thats a MAJOR failure.
//          2. Timer 2 - set to the min specified time for Watchdog timer.
//              If this timer expires; watchdogMinTimeExpired is set to TRUE.
//              watchdogMinTimeExpired (in non-volatile memory) will be used
//              in the second pass of the Watchdog test.
//       b. testingWatchdog is set to TRUE
//       c. Control is returned to the caller (POST executive).
//
//    Pass 2 - the NOVRAM variable testingWatchdog = TRUE
//       a. If watchdogMinTimeExpired is not TRUE, thats a MAJOR failure.
//       b. If the H/W Watchdog bite is not asserted then either the watchdog
//          bite latch has failed or the power failed during the watchdog
//          reset.  In this case, the watchdog test is retried two times
//          before assuming the watchdog bite latch failed and declaring
//          a MAJOR failure.  If it is asserted, clear it.  If the bit
//          remains asserted thats a MAJOR failure.
//       c. If watchdogMinTimeExpired does is TRUE and the H/W Watchdog bit
//          was active and we were able to clear it. Then the test
//          passes.
//             1. set testingWatchdog   to FALSE
//             2. set watchdogMinTimeExpired to FALSE
//             3. setup a timer with an interrupt handler that strobes
//                the Watchdog to prevent further resets without having
//                to strobe the Watchdog in the main POST thread.
//---------------------------------------------------------------------
//@ PreCondition
//
//  Timers:
//   This test needs 2 timers to be operational
//
//  Kernel NOVRAM Test:
//   Two flags stored in NVRAM are required for this test.  NVRAM must
//   be tested that it can be accessed, but the state variables for the
//   watchdog test cannot be changed.  If these state variables are
//   corrupt the uninitialized path (see flowchart) is run.  If these
//   variables continue to be uninitialized or corrupt the rolling
//   thunder test will fail.
//
//  Rolling Thunder Test:
//	 Since this test performs an intentional reset, a mechanism to
//	 catch the improbable event of more than 2 cycles occurring is
//	 caught by placing rolling thunder before the WD test.  It must
//	 be placed before the watchdog timer test.
//
//  Unintentional Reset Umpire Test:
//   This test must be placed after the watchdog timer test.  This is
//   done so as not to confuse the expected watchdog reset with an
//   unexpected reset.  The watchdog timer test saves the state of
//   an unintentional reset to be checked later by the unintentional
//   reset umpire test.  This requires the umpire test to be placed
//   after the watchdog timer test.
//
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Free-Function
//=====================================================================

Criticality
WatchdogTimerTest  (void)
{
	//TODO E600 port this or remove


#if defined(SIGMA_UNIT_TEST)
   volatile
#endif
   Criticality status = PASSED;
/*
#if defined(SIGMA_UNIT_TEST)
   if (!PKernelNovram->unitTestWatchdog)
       return PASSED;
#endif

   CostReductionBoard_ = CpuDevice::GetBoardRevision() == NMI_COST_REDUCTION;
   // $[TI3.1] $[TI3.2]

   Boolean isPbmonInFlash
       = (*PBMON_MAGIC_COOKIE_MEM == PBMON_MAGIC_COOKIE); // $[TI4.1] $[TI4.2]

UNIT_TEST_POINT(POSTUT_21_6)

   //  skip watchdog timer test when PBMON is loaded in flash -
   //  necessary since PBMON does not strobe watchdog timer
   //  this is a hook for manufacturing checkout after which PBMON
   //  is erased from flash memory and "normal" processing of the
   //  download or application ensues
   if ( isPbmonInFlash )
   {
       // $[TI1.1]
       PKernelNovram->testingWatchdog = FALSE;
       status = PASSED;
   }
   else
   {
       // $[TI1.2]

      if ( !PKernelNovram->testingWatchdog )
      {
         // $[TI2.1]
         status = WatchdogTestPass1();
UNIT_TEST_POINT(Watchdog1_return);
      }
      else
      {
         // $[TI2.2]
         status = WatchdogTestPass2();
UNIT_TEST_POINT(Watchdog2_return);
      }
   }

*/
   return status;
}

//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Free-Function: WatchdogTestPass1  (void)
//
//@ Interface-Description
//    Initializes two timers during the first pass of the watchdog
//    timer test.  Installs interrupt handlers for each timer.
//    Sets the shutdown state to "watchdog occured" if the hardware
//    watchdog bite is asserted and the current shutdown state is
//    unknown.
//
//    INPUTS:  NONE
//
//    OUTPUTS: watchdogMinTimeExpired set to FALSE
//             testingWatchdog   set to TRUE
//
//    RETURNS: PASSED
//---------------------------------------------------------------------
//@ Implementation-Description
//    Save the state of the H/W watchdog bit to the NVRAM variable
//    WatchdogOccurred.  This is done because this test will overwrite the
//    bit with an expected watchdog reset.  Set up the timers in the
//    following manner.
//
//       82C54 timer1 = maximum time of the watchdog sentry,
//       82C54 timer2 = minimum time
//
//
//    Installs interrupt hooks for both timers, refer to the handlers
//    for specific actions.  Strobe the watchdog once to synchronize the
//    time, then return to the caller.
//
//    There is no direct failure detected in this routine.  Pass 1 fails
//    only if timer1 expires, the interrupt handler declares the error.
//
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Free-Function
//=====================================================================

//Note:  We cannot time .250 seconds directly with a 16 bit timer.
//       Therefore the max interrupt handler will be called 32 times
//       and will provide the final divide down of the clock.
//       Therefore; this count divides the main frequency down to provide
//       1/32th the desired timed interval.
#define MAX_COUNT           ((Uint16)62500)  // 8e6/62500 = .0078125 Sec
                                             // * 32 = .250 Sec = the desired
                                             //                   interval

#define MAX_TIMER_INTERRUPT_VECTOR     (27)
#define MIN_TIMER_INTERRUPT_VECTOR     (28)

//Note:  We cannot time .062 seconds directly with a 16 bit timer.
//       Therefore the min interrupt handler will be called 8 times
//       and will provide the final divide down of the clock.
//       Therefore; this count divides the main frequency down to provide
//       1/8th the desired timed interval.
#define MIN_COUNT           ((Uint16)62000)  // 8e6/62000 = .00775 Sec
                                             // * 8 = .062 Sec = the desired
                                             //                  interval


#define MAX_DIVIDE_COUNT   (32)
#define MIN_DIVIDE_COUNT   (8)


// The HW watchdog circuit is designed to reset the CPU within 62.5-250ms
// if it has not been strobed.  To disable it we strobe it with an interrupt
// that is set to go off within 8ms.  This is the minimum frequency that
// the 8254 can interrupt.
#define WATCHDOG_DISABLE_COUNT   ((Uint16)65535)  // 8e6/65535 = .00819 sec



// The cost reduction board contains three timers implemented in an
// FPGA.  The base frequency is 8Mhz. Each countdown timer can be
// programmed with up to a 22 bit value so it is unnecessary to count
// interrupts as is done with the I8254 since the allowable range for
// the PB timers is greater than the timing requirements of 62.5ms to
// 250ms.

#define PBTIMER_INT_COUNT ( 1 )
#define PBTIMER_NS_PER_TICK ( 125 )
#define PBTIMER_MIN_COUNT ( 62000000/PBTIMER_NS_PER_TICK )
#define PBTIMER_MAX_COUNT ( 250000000/PBTIMER_NS_PER_TICK )
#define PBTIMER_WD_DISABLE_COUNT ( 8000000/PBTIMER_NS_PER_TICK )

static Criticality
WatchdogTestPass1(void)
{
   CpuDevice::EnableAutovector( ENABLE_AUTOVEC_NONE );

#if defined(SIGMA_DEBUG)
   dprintf("Watchdog Test - Pass 1\n");
#endif


   //  The unexpected reset umpire needs to know if a watchdog has occurred
   //  before the watchdog test starts.  If watchdog has fired then set
   //  the watchdogOccurred TRUE.  When TRUE and the shutdown state
   //  is UNKNOWN, the ResetUmpire logs a strike against the CPU.
   //  This test does not set the shutdown state to WATCHDOG here since
   //  a power failure during the window between this test and the
   //  ResetUmpire would result in losing a "strike".
   //
   //  The "Rolling Thunder" Test halts POST processing prior to the
   //  Watchdog Timer Test if the watchdog fires during POST so no
   //  processing for this situation is required here.

   Boolean watchdogBit = CpuDevice::IsWatchdogBiteOn();

UNIT_TEST_POINT(POSTUT_21)

   PKernelNovram->watchdogOccurred = watchdogBit;

   // ***************************************************************
   // **** connect the interrupt handlers  **************************
   // ***************************************************************
#ifdef E600_840_TEMP_REMOVED
   InstallHandler(MAX_TIMER_INTERRUPT_VECTOR, WatchdogMaxInterruptHandler);
   InstallHandler(MIN_TIMER_INTERRUPT_VECTOR, WatchdogMinInterruptHandler);
#endif

   I82C54RegMap * timer = (I82C54RegMap*)I82C541_BASE;

   if ( !CostReductionBoard_ )
   {														// $[TI1.1]
	  // **** set up the interrupt counters
      MaxCounter_ = MAX_DIVIDE_COUNT;
      MinCounter_ = MIN_DIVIDE_COUNT;

	  // **** set up the timers
      timer->control = (Uint8)(COUNTER_0 | (2<<1) | (3<<4)); //mode=2 rw=3 bcd=0
      timer->control = (Uint8)(COUNTER_1 | (2<<1) | (3<<4)); //mode=2 rw=3 bcd=0

	  // **** stop the counters
      timer->counter_0 = ((Uint8)((MAX_COUNT   ) & 0xff));
      timer->counter_1 = ((Uint8)((MIN_COUNT   ) & 0xff));
   }
   else
   {														// $[TI1.2]
	  // **** set up the interrupt counters
      MaxCounter_ = PBTIMER_INT_COUNT;
      MinCounter_ = PBTIMER_INT_COUNT;

      // **** stop the counters
      *P_PBTIMER_1_CONTROL = 0;
      *P_PBTIMER_2_CONTROL = 0;
      *P_PBTIMER_3_CONTROL = 0;

	  // **** set up the counters
      *P_PBTIMER_1_PRELOAD = PBTIMER_MAX_COUNT;  // for 250 ms
      *P_PBTIMER_2_PRELOAD = PBTIMER_MIN_COUNT;  // for 62.5 ms
   }


   // **** clear any pending interrupts
   CpuDevice::ResetAutovector3();
   CpuDevice::ResetAutovector4();
   CpuDevice::ResetAutovector6();

   // ****************************************
   // **** set variables to the next pass ****
   // ****************************************
   PKernelNovram->watchdogMinTimeExpired = FALSE;
   PKernelNovram->testingWatchdog   = TRUE;

UNIT_TEST_POINT(POSTUT_21_5)

   UT_WatchdogTestStart = 1;

   // *****************************************************************
   // **** start the timers and strobe the Watchdog to synchronize ****
   // *****************************************************************
   if ( !CostReductionBoard_ )
   {														// $[TI2.1]
      timer->counter_0 = ((Uint8)((MAX_COUNT>>8) & 0xff));
      timer->counter_1 = ((Uint8)((MIN_COUNT>>8) & 0xff));
   }
   else
   {														// $[TI2.2]
      *P_PBTIMER_1_CONTROL = PBTIMER_EN;
      *P_PBTIMER_2_CONTROL = PBTIMER_EN;
   }

   CpuDevice::StrobeWatchdog();

   CpuDevice::EnableAutovector( ENABLE_AUTOVEC_3 | ENABLE_AUTOVEC_4 );

   // **** Unmask Interrupts ****
   ENABLE_INTERRUPTS();

   // $[TI1]
   return(PASSED);
}


//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Free-Function: WatchdogTestPass2
//
//@ Interface-Description
//    This determines if the watchdog occurred after the minimum
//    time had elapsed.  The determination of minimum time is the
//    state of watchdogMinTimeExpired.  If it is not TRUE this routine
//    fails and returns a MAJOR error to the caller.
//
//    The hardware watchdog "bite" latch must be asserted as an expected
//    watchdog reset is the only proper way into pass 2.  If it is not
//    asserted then we assume that either the latch is failed or power
//    was lost during the watchdog reset.  In this case, the test is
//    retried twice before the watchdog bite latch is declared as failed
//    and the test returns a MAJOR failure.  The test returns a MAJOR
//    failure if the watchdog bite cannot be reset.
//
//    INPUTS:  NONE
//
//    OUTPUTS: NONE
//
//    RETURNS: MAJOR, PASSED
//---------------------------------------------------------------------
//@ Implementation-Description
//    If the NOVRAM variable watchdogMinTimeExpired is not TRUE then
//    return a MAJOR failure.
//
//    If watchdog bite latch is not asserted then restart the watchdog
//    test up to two times before declaring the failure of the latch
//    and returning a MAJOR failure.
//
//    Reset the "bite" latch and read it back. If it is not negated
//    then return a MAJOR failure.
//
//    Disable the watchdog by installing the watchdog interrupt handler.
//
//    The state of the watchdog bit, watchdogMinTimeExpired and
//    testingWatchdog are reset, even if the test fails to prevent
//    a lockout condition.
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Free-Function
//=====================================================================
static Criticality
WatchdogTestPass2(void)
{
#if defined(SIGMA_UNIT_TEST)
   volatile
#endif
   Criticality status     = PASSED;
   Boolean timedOut       = PKernelNovram->watchdogMinTimeExpired;
   Boolean watchdogBit    = CpuDevice::IsWatchdogBiteOn();

UNIT_TEST_POINT(POSTUT_21_4)

#if defined(SIGMA_DEBUG)
   dprintf("Watchdog Test - Pass 2\n");
#endif

   // ***********************************************
   // **** if the min timer in pass 1 did not    ****
   // **** set watchdogMinTimeExpired, that is a ****
   // **** failure                               ****
   // ***********************************************
   if ( !timedOut )
   {
      // $[TI1.1]
      status = WDT_SEVERITY;
   }
   // $[TI1.2]

   //
   //  If the hardware bit that indicates a Watchdog timeout
   //  occurred is not set, assume a power interrupt occured
   //  during the watchdog reset.  In this case, we restart the
   //  watchdog test. If the latch has actually failed, this
   //  will cause an infinite POST reset/retry sequence, but it
   //  is safer to assume that the latch works and retry instead
   //  of signal a false positive for the test and preventing
   //  ventilation. If the latch has failed, the POST timers will
   //  take the vent to Vent-INOP within 10 seconds in any case.
   //
   //  The number of times POST can be reentered is
   //  limited by the "Rolling Thunder" count.  Decrement this count
   //  before retrying the Watchdog Timer test.  The maximum watchdog
   //  retry count prevents an infinite reset condition caused
   //  by a failed Watchdog Bite latch.
   //
   if ( !watchdogBit )
   {
      // $[TI2.1]
      --PKernelNovram->rollingThunderCount;
      return WatchdogTestPass1();
   }
   // $[TI2.2]

   // ***********************************************
   // **** clear the hardware bit that indicates ****
   // **** that a Watchdog timeout occurred, if  ****
   // **** it does not clear, that is a failure. ****
   // ***********************************************
   CpuDevice::ClearWatchdogBite();
   Boolean watchdogBiteCleared = !CpuDevice::IsWatchdogBiteOn();

UNIT_TEST_POINT(POSTUT_21_2)

   if ( !watchdogBiteCleared )
   {
      // $[TI4.1]
      status = MAJOR;
   }
   // $[TI4.2]

   PKernelNovram->testingWatchdog   = FALSE;
   PKernelNovram->watchdogMinTimeExpired = FALSE;
   PKernelNovram->watchdogRetries = 0;

   DisableWatchdog();

   return status;
}

//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Free-Function: WatchdogMinInterruptHandler
//
//@ Interface-Description
//    This interrupt handler is used to verify that the Watchdog timer is
//    not firing too quickly.
//
//    INPUTS:  NONE
//
//    OUTPUTS: watchdogMinTimeExpired set to TRUE
//
//    RETURNS: NONE
//---------------------------------------------------------------------
//@ Implementation-Description
//    This interrupt handler is connected to one of the timers used to
//    measure the Watchdog timer circuit.  If this handler is invoked,
//    the minimum time for the Watchdog has elapsed.  That means the
//    Watchdog timer is not firing to quickly.
//
//    This routine sets the variable watchdogMinTimeExpired to TRUE.
//
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Free-Function
//=====================================================================
#ifdef E600_840_TEMP_REMOVED
static interrupt void
#endif
void WatchdogMinInterruptHandler(void)
{
   // **** clear the interrupt
   CpuDevice::ResetAutovector4();

   // **** count down the interrupts ****
   if (!--MinCounter_)
   {
       // $[TI1.1]
       UT_WatchdogMinExpired = 1;

#if defined(SIGMA_DEBUG)
       dprintf("\nWatchdog Min timer expired!!\n");
#endif
       PKernelNovram->watchdogMinTimeExpired = TRUE;

       if ( !CostReductionBoard_ )
       {													// $[TI2.1]
          // Now disable this timer and remove its interrupt vector.
          I82C54RegMap * timer = (I82C54RegMap*)I82C541_BASE;
          timer->control = COUNTER_1 | (2<<1) | (3<<4);
          timer->counter_1 = 0;
       }
       else
       {													// $[TI2.2]
          *P_PBTIMER_2_CONTROL = 0;
       }

UNIT_TEST_POINT(POSTUT_21_1)
#ifdef E600_840_TEMP_REMOVED
       InstallHandler(MIN_TIMER_INTERRUPT_VECTOR, IllegalExceptionHandler);
#endif
   }
   // $[TI1.2]
}


//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Free-Function: WatchdogMaxInterruptHandler
//
//@ Interface-Description
//    This interrupt handler is used to verify the Watchdog timer is not
//    firing too slowly.  It will also determine if the Watchdog timer
//    is working at all.
//
//    INPUTS:  NONE
//
//    OUTPUTS: NONE
//
//    RETURNS: NONE
//---------------------------------------------------------------------
//@ Implementation-Description
//    This interrupt handler is connected to one of the timers used to
//    measure the Watchdog timer circuit.  If this handler is invoked
//    the maximum time for the Watchdog has elapsed without an reset.
//    That means the Watchdog timer is firing to slowly or is not
//    working at all.
//
//
// Warning:  This test will directly call KernelError() if there is an
//           an error.  Since this is an interrupt routine, the stack
//           pointer has data saved on it.  KernelError() must not
//           (and according to the design, will not) return to this
//           module or continue running POST without reinitiallizing
//           the stack.
//
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Free-Function
//=====================================================================
#ifdef E600_840_TEMP_REMOVED
static interrupt void
#endif
void WatchdogMaxInterruptHandler(void)
{
   // **** clear the interrupt
   CpuDevice::ResetAutovector3();

   // **** count down the interrupts ****
   if (!--MaxCounter_)
   {
       // $[TI1.1]
       UT_WatchdogMaxExpired = 1;

#if defined(SIGMA_DEBUG)
       dprintf("\nWatchdog Max timer expired!!\n");
#endif
       //  reset testingWatchdog to FALSE since next entry to POST should
       //  enter phase 1 of the watchdog test
       PKernelNovram->testingWatchdog = FALSE;

       //  set up the test in progress and LED codes since we are
       //  probably not in the watchdog test if this interrupt occurs
       SetPostStep(POST_ERROR_WATCHDOG);

       SetTestResult(WDT_SEVERITY);

       if (WDT_SEVERITY == MAJOR)
       {
           // $[TI2.1]
           KernelError();
       }
       // $[TI2.2]
   }
   // $[TI1.2]
}



//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Free-Function: DisableWatchdog
//
//@ Interface-Description
//    This function installs a timer interrupt that strobes the watchdog
//    the rate set by the timer.
//
//    INPUTS:  NONE
//
//    OUTPUTS: NONE
//
//    RETURNS: NONE
//---------------------------------------------------------------------
//@ Implementation-Description
//    Installs a timer interrupt (8254 #0) that will service the watchdog
//    thereby disabling it from resetting the CPU.  The vector used
//    is the 16 bit timer.
//
//    This function disables an important safety feature of the
//    system.  It is important that a backup mechanism to remove this
//    interrupt or mitigate its effect be in place.  The POST 10 second
//    timer is intended to be armed and counting down while the watchdog
//    is disabled, this is the backup system.  It is also assumed that the
//    application software removes the interrupt installed by this routine
//    once POST is complete.
//
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Free-Function
//=====================================================================
void DisableWatchdog(void)
{
   // disable interrupts during setup
   DISABLE_INTERRUPTS();

#ifdef E600_840_TEMP_REMOVED
   InstallHandler(MAX_TIMER_INTERRUPT_VECTOR, WatchdogDisableInterruptHandler);
#endif

   // clear any pending interrupts
   CpuDevice::ResetAutovector3();

   if ( !CostReductionBoard_ )
   {														// $[TI1.1]
      // Set up the timer
      I82C54RegMap * timer = (I82C54RegMap*)I82C541_BASE;
      timer->control = (Uint8)(COUNTER_0 | (2<<1) | (3<<4)); //mode=2 rw=3 bcd=0
      timer->counter_0 = WATCHDOG_DISABLE_COUNT & 0xff;
      timer->counter_0 = WATCHDOG_DISABLE_COUNT >> 8 & 0xff;
   }
   else
   {														// $[TI1.2]
      *P_PBTIMER_1_PRELOAD = PBTIMER_WD_DISABLE_COUNT;
      *P_PBTIMER_1_CONTROL = PBTIMER_EN;
   }

   // unmask the autovector
   CpuDevice::EnableAutovector( ENABLE_AUTOVEC_3 );

   ENABLE_INTERRUPTS();
}

