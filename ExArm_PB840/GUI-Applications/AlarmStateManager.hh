
#ifndef AlarmStateManager_HH
#define AlarmStateManager_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1994, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: AlarmStateManager -- Manager of the alarm states
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/AlarmStateManager.hhv   25.0.4.0   19 Nov 2013 14:07:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  sah    Date:  28-Jul-97    DCS Number: 2320
//  Project:  Sigma (R8027)
//  Description:
//       Integration baseline.
//
//====================================================================

#include "GuiAppClassIds.hh"

//@ Usage-Classes
#include "OsTimeStamp.hh"
#include "Alarm.hh"
#include "AlarmUpdateSortedList.hh"
//@ End-Usage


class AlarmStateManager
{
  public:
	static void  Initialize(void);

	static void         ProcessEventBasedAlarmUpdates(void);
	inline static void  ProcessPeriodicAlarmUpdates  (void);

	inline static const AlarmUpdateSortedList&  GetCurrAlarmList (void);
	inline static Alarm::Urgency                GetHighestUrgency(void);

	inline static void  ManualAlarmsResetActivated(void);

	static void SoftFault(const SoftFaultID softFaultID,
							 const Uint32      lineNumber,
							 const char*       pFileName  = NULL, 
							 const char*       pPredicate = NULL);

	//@ Type:  AlarmUrgencyInfo
	// Information regarding the state of a particular alarm urgency.
	struct AlarmUrgencyInfo
	{
	  OsTimeStamp  urgencyTimer;
	  Uint         minUrgencyTime;
	};

  private:
	// these methods are purposely declared, but not implemented...
	AlarmStateManager(void);						// not implemented...
	~AlarmStateManager(void);						// not implemented...
	AlarmStateManager(const AlarmStateManager&);	// not implemented...
	void operator=(const AlarmStateManager&);		// not implemented...

	static Boolean  IsNewLowAlarmActive_(void);

	static Alarm::Urgency  CalcHighestUrgency_(
									const AlarmUpdateSortedList& sortedList
											  );

	//@ Data-Member:  ArrUrgencyInfo_
	// Static array containing information about each of the urgency
	// levels.
	static AlarmUrgencyInfo  ArrUrgencyInfo_[];

	//@ Data-Member:  PrevAlarmsList_
	// Static, sorted list used for containing the previous set of active
	// alarms.
    static AlarmUpdateSortedList&  RPrevAlarmsList_;

	//@ Data-Member:  CurrAlarmsList_
	// Static, sorted list used for containing the current set of active
	// alarms, combined with any pending alarms.
    static AlarmUpdateSortedList&  RCurrAlarmsList_;

	//@ Data-Member:  PrevAlarmsSilencedFlag_
	// Static boolean storing whether the previous processing cycle had the
	// alarms-silence state activated.
    static Boolean  PrevAlarmsSilencedFlag_;

	//@ Data-Member:  AlarmsResetActivaedFlag_
	// Static boolean storing whether the an alarms-reset state was activated.
    static Boolean  AlarmsResetActivatedFlag_;

	//@ Data-Member:  DoPeriodicProcessing_
	// Static boolean storing whether periodic processing is enabled, or not.
    static Boolean  DoPeriodicProcessing_;

	//@ Constant:  MIN_LOW_URGENCY_TIME_
	// Constant containing the minimum time a low-urgency alarm must remain
	// up before allowing an autoreset to remove the alarm (based on the
	// time length of the corresponding alarm burst).
    static const Uint  MIN_LOW_URGENCY_TIME_;

	//@ Constant:  MIN_MED_URGENCY_TIME_
	// Constant containing the minimum time a medium-urgency alarm must remain
	// up before allowing an autoreset to remove the alarm (based on the
	// time length of the corresponding alarm burst).
    static const Uint  MIN_MED_URGENCY_TIME_;

	//@ Constant:  MIN_HIGH_URGENCY_TIME_
	// Constant containing the minimum time a high-urgency alarm must remain
	// up before allowing an autoreset to remove the alarm (based on the
	// time length of the corresponding alarm burst).
    static const Uint  MIN_HIGH_URGENCY_TIME_;
};

// Inlined methods
#include "AlarmStateManager.in"

#endif // AlarmStateManager_HH 
