/*****************************************************************************/
/*   Module Name:    registry.c                                             
/*   Purpose:      Generl functions for manipulating the registry                                 
/*                                                                          
/*                                                                           
/*   Requirements:                                       
/*
/*   Modification History:
/*   
/*   Description of Change			        By			      Date 
/*   ---------------------					-----   		  ------
/*   Added Alarm Log functionality.         RHJ             27 Nov,2007    
/*   Modified LoadUserReportDirectory()
/*   and SaveUserReportDirectory().
/*
/*	 Critical Observation during Phase1 
/* 	 is incorportated.						Kathirvel(CGS)    27 Sep,2000
/*
/*   $Revision::                                                
/*                                                                          
/*****************************************************************************/

/*****************************************************************************
THIS IS A PROPRIETARY WORK TO WHICH NELLCOR PURITAN BENNETT CORPORATION CLAIMS
EXCLUSIVE RIGHT.  NO PART OF THIS WORK MAY BE USED, DISCLOSED, REPRODUCED,
STORED IN AN INFORMATION RETIEVAL SYSTEM, OR TRANSMITTED, IN ANY FORM OR BY ANY
MEANS ELECTRONIC, MECHANICAL, PHOTOCOPYING, RECORDING OR OTHERWISE WITHOUT THE
PRIOR WRITTEN PERMISSION OF NELLCOR PURITAN BENNETT CORPORATION.
 
Copyright (c) 1999, Mallinckrodt Nellcor Puritan Bennett
All rights reserved.
*******************************************************************************/
#include <windows.h>
#include <ansi_c.h>
#include <cviauto.h>
#include <formatio.h>
#include <utility.h>

#include "ldb.h"
#include "registry.h"
#include "usradmin.h"

/*****************************************************************************/
/* Name:     GetLangList                                                     
/*                                                                           
/* Purpose:  Retrieves the list of supported languages from the registry.
/*                                          
/*                                                                           
/* Example Call:   char* GetLangList(void);                                    
/*                                                                           
/* Input:    <None>          
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                       
/*                                                                           
/* Return:   List of supported languages. Comma delimited                                                         
/*                                                                           
/*****************************************************************************/
char* GetLangList(void)
{
	HKEY hCPVTkey;
	char* buffer;
	DWORD retlen;
	long error;

	retlen = (MAX_LANG_LENGTH + 1) * nNumLangTables;
	buffer = (char *)malloc(retlen); // Modified the statement due to Critical Observation.
	error = RegOpenKeyEx(   CPVT_HKEY, CPVT_SUBKEY, RESERVED_ZERO, KEY_QUERY_VALUE, &hCPVTkey);
	error = RegQueryValueEx(hCPVTkey,  LANGS_KEY,   RESERVED_NULL, NULL,            (BYTE *)buffer, &retlen);
	if(error != ERROR_SUCCESS || retlen <= 0){
		free(buffer);
		return NULL;
	}
	buffer[retlen-1] = '\0';	//just to make sure it's terminated.
	RegCloseKey(hCPVTkey);

	if(retlen <= 0){
		free(buffer);
		return NULL;
	}
	else{
		return buffer;
	}
}


/*****************************************************************************/
/* Name:     LoadUserOptions                                                     
/*                                                                           
/* Purpose:  Gets the user options from the registry
/*                                          
/*                                                                           
/* Example Call:   int LoadUserOptions(char* UserName);                                    
/*                                                                           
/* Input:    char* UserName   User to get options for          
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
int LoadUserOptions(char* UserName)
{
	HKEY hCPVTkey;
	HKEY hUSERKey;
	char buffer[MAX_LANG_LENGTH * 2];
	DWORD retlen;
	long error;

	retlen = MAX_LANG_LENGTH + 1;

	if(UserName != NULL){
		error = RegOpenKeyEx(PWD_HKEY, PWD_SUBKEY, RESERVED_ZERO, KEY_QUERY_VALUE, &hCPVTkey);
		error = RegOpenKeyEx(hCPVTkey, UserName,   RESERVED_ZERO, KEY_QUERY_VALUE, &hUSERKey);
	}
	else{
		error = RegOpenKeyEx(CPVT_HKEY, CPVT_SUBKEY,         RESERVED_ZERO, KEY_QUERY_VALUE, &hCPVTkey);
		error = RegOpenKeyEx(hCPVTkey,  REG_DEF_OPTIONS_KEY, RESERVED_ZERO, KEY_QUERY_VALUE, &hUSERKey);
	}
	error = RegQueryValueEx(hUSERKey, LANG_KEY, RESERVED_NULL, NULL, (BYTE *)buffer, &retlen);

	RegCloseKey(hUSERKey);
	RegCloseKey(hCPVTkey);

	if(retlen >= (MAX_LANG_LENGTH * 2)){
		retlen = (MAX_LANG_LENGTH *2 )-1;
	}

	buffer[retlen] = '\0';	//just to make sure it's terminated.
	crntOpts.Language = GetLangfromString(buffer);
	
	ProcessOptions();

	return NO_ERROR;
}

/*****************************************************************************/
/* Name:     SaveUserLanguage                                                     
/*                                                                           
/* Purpose:  Saves the user's selected language to the registry
/*                                          
/*                                                                           
/* Example Call:   int SaveUserLanguage(char* UserName, char* Lang);                                    
/*                                                                           
/* Input:    char* UserName   User to set
/*		char* Lang   	 Selected language          
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
int SaveUserLanguage(char* UserName, char* Lang)
{
	HKEY hCPVTkey;
	HKEY hUserKey;
	long error;

	if(UserName == NULL || Lang == NULL){
		return ERROR;
	}

	error = RegOpenKeyEx( PWD_HKEY, PWD_SUBKEY, RESERVED_ZERO, KEY_QUERY_VALUE, &hCPVTkey);
	error = RegOpenKeyEx( hCPVTkey, UserName,   RESERVED_ZERO, KEY_ALL_ACCESS,  &hUserKey);
	error = RegSetValueEx(hUserKey, LANG_KEY,   RESERVED_ZERO, REG_SZ,          (BYTE *)Lang, strlen(Lang)+1);

	RegCloseKey(hUserKey);
	RegCloseKey(hCPVTkey);

	return NO_ERROR;
}

/*****************************************************************************/
/* Name:     GetCSEEnabled                                                   
/*                                                                           
/* Purpose:  Checks if the CSE flag is set in the registry
/*                                          
/*                                                                           
/* Example Call:   int GetCSEEnabled(void);                                    
/*                                                                           
/* Input:    input1 -	description          
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   1 if CSE enabled; 0 otherwise                                                     
/*                                                                           
/*****************************************************************************/
int GetCSEEnabled(void)
{
#ifdef CSE_FLAG_REGISTRY
	HKEY hCPVTkey, hUserKey;
	char buffer[MAX_LANG_LENGTH*2];
	DWORD retlen;
	long result;

	retlen = 5;
	result = RegOpenKeyEx(CSE_HKEY, CSE_SUBKEY, RESERVED_ZERO, KEY_QUERY_VALUE, &hCPVTkey);
	if(result != ERROR_SUCCESS){
		return 0;
	}
	result = RegQueryValueEx(hCPVTkey, "", RESERVED_NULL, NULL, (BYTE *)buffer, &retlen);

	RegCloseKey(hCPVTkey);

	if(result != ERROR_SUCCESS){
		return 0;
	}
	if(retlen){
		return atoi(buffer);
	}

	return 0;
#else
	int result;
	long fileSize;

	result = GetFileInfo(LDBf(Ldb_CSE_File), &fileSize);
	
	if(result == 0){
		//file not found
		return 0;
	}
	else{
		return 1;
	}
#endif
	
}

#ifdef RICH_REPORT
/*****************************************************************************/
/* Name:     LoadUserReportDirectory                                                     
/*                                                                           
/* Purpose:  Gets the last report directory selected by the user based
/*           on report type
/*                               
/*                                                                           
/* Example Call:   int LoadUserReportDirectory(LOGTYPES type, char* path_name);                                    
/*                                                                           
/* Input:    LOGTYPES type		- report type selected
/*			 char*    path_name - path where the last selected file was found         
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   0 - valid pathname retrieved from registry
/*			-1 - error in retrieving pathname                                                         
/*                                                                           
/*****************************************************************************/
int LoadUserReportDirectory(LOGTYPES type, char* path_name)
{
	HKEY    hCPVTkey;
	long    result;
	DWORD   retlen;
	LPCTSTR DirectoryKey;

	switch(type){
		case INFO_LOG:
			DirectoryKey = RPT_DIAG_DIR_KEY;
			break;

		case DIAG_LOG:
			DirectoryKey = RPT_INFO_DIR_KEY;
			break;

		case TEST_LOG:
			DirectoryKey = RPT_TEST_DIR_KEY;
			break;
        case ALARM_LOG:
            DirectoryKey = RPT_ALARM_DIR_KEY;
            break;
		default:
			strcpy(path_name,".");
			return -1;
			break;
	}

	result = RegOpenKeyEx(RPT_HKEY, RPT_SUBKEY, RESERVED_ZERO, KEY_QUERY_VALUE, &hCPVTkey);
	if(result == ERROR_SUCCESS){
		retlen = MAX_PATHNAME_LEN;
		result = RegQueryValueEx(hCPVTkey, DirectoryKey, RESERVED_NULL, NULL, (BYTE *)path_name, &retlen);
	}

	RegCloseKey(hCPVTkey);

	if(result != ERROR_SUCCESS){
		strcpy(path_name,".");
		return -1;
	}
	else{
		return 0;
	}
}

/*****************************************************************************/
/* Name:     SaveUserReportDirectory                                                     
/*                                                                           
/* Purpose:  Saves the last report directory selected by the user based
/*           on report type
/*                               
/*                                                                           
/* Example Call:   int SaveUserReportDirectory(LOGTYPES type, char* path_name);                                    
/*                                                                           
/* Input:    LOGTYPES type		- report type selected
/*			 char*    path_name - path where the last selected file was found         
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   0 - pathname sucessfully stored in registry
/*			-1 - error in storing pathname                                                         
/*                                                                           
/*****************************************************************************/
int SaveUserReportDirectory(LOGTYPES type, char* file_name)
{
	HKEY    hCPVTkey;
	int     result;
	int     length;
	char    path_name[MAX_PATHNAME_LEN];
	LPCTSTR DirectoryKey;

	switch(type){
		case INFO_LOG:
			DirectoryKey = RPT_DIAG_DIR_KEY;
			break;

		case DIAG_LOG:
			DirectoryKey = RPT_INFO_DIR_KEY;
			break;

		case TEST_LOG:
			DirectoryKey = RPT_TEST_DIR_KEY;
			break;
        case ALARM_LOG:
            DirectoryKey = RPT_ALARM_DIR_KEY;
            break;

		default:
			return -1;
			break;
	}

	result = RegOpenKeyEx(RPT_HKEY, RPT_SUBKEY, RESERVED_ZERO, KEY_ALL_ACCESS, &hCPVTkey);
	if(result == ERROR_SUCCESS){
		strcpy(path_name,file_name);
		length = strlen(path_name);
		while(path_name[length] != '\\'){
			length--;
		}

		path_name[length] = '\0';
		
		RegSetValueEx(hCPVTkey, DirectoryKey, RESERVED_ZERO, REG_SZ, (BYTE *)path_name, length);
		RegCloseKey(hCPVTkey);
		return 0;
	}
	else{
		return -1;
	}
}
#else
/*****************************************************************************/
/* Name:     SetLogInfo                                                     
/*                                                                           
/* Purpose:  Saves log information to the registry
/*                               
/*                                                                           
/* Example Call:   int SetLogInfo(char* path, char* LogKey);                                    
/*                                                                           
/* Input:    char* path	  - path of log file to save
/*			 char* LogKey - key of log to save info for      
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   result of RegSetValueEx                                                         
/*                                                                           
/*****************************************************************************/
int SetLogInfo(char* path, char* LogKey)
{
	HKEY hCPVTkey;
	DWORD retlen;
	long retval;

	retlen = LOG_MAX_PATH;

	RegOpenKeyEx(LOG_HKEY, LOG_SUBKEY, RESERVED_ZERO, KEY_ALL_ACCESS, &hCPVTkey);

	retval = RegSetValueEx(hCPVTkey, LogKey, RESERVED_ZERO, REG_SZ, (BYTE *)path, strlen(path)+1);
	RegCloseKey(hCPVTkey);

	return (int)retval;
}

/*****************************************************************************/
/* Name:     GetLogInfo                                                     
/*                                                                           
/* Purpose:  Retrieves log information from the registry
/*                               
/*                                                                           
/* Example Call:   int GetLogInfo(char* path, char* LogKey);                                    
/*                                                                           
/* Input:    char* path	  - buffer to store information to
/*			 char* LogKey - key of log to get info for         
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   Result of RegQueryValueEx                                                        
/*                                                                           
/*****************************************************************************/
int GetLogInfo(char* path, char* LogKey)
{
	HKEY  hCPVTkey;
	DWORD retlen;
	long  result;

	retlen = LOG_MAX_PATH;

	RegOpenKeyEx(LOG_HKEY, LOG_SUBKEY, RESERVED_ZERO, KEY_QUERY_VALUE, &hCPVTkey);
	result = RegQueryValueEx(hCPVTkey, LogKey, RESERVED_NULL, NULL, (BYTE *)path, &retlen);
	RegCloseKey(hCPVTkey);
	path[retlen] = '\0';	//just to make sure it's terminated.

	return (int)result;
}
#endif

/*****************************************************************************/
/* Name:     RegGetInfoFields                                                     
/*                                                                           
/* Purpose:  Gets company information from the registry
/*                               
/*                                                                           
/* Example Call:   int RegGetInfoFields(Info1, Info2, Info3, Info4, Info5)
/*                                                                           
/* Input:    char* Info1	  - buffer to store first line of information to
/* 		     char* Info2	  - buffer to store second line of information to
/* 		     char* Info3	  - buffer to store third line of information to
/* 		     char* Info4	  - buffer to store fourth line of information to
/* 		     char* Info5	  - buffer to store fifth line of information to
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   0 on success, -1 otherwise                                                     
/*                                                                           
/*****************************************************************************/
int RegGetInfoFields(char* Info1, char* Info2, char* Info3, char* Info4, char* Info5)
{
	HKEY  hCPVTkey;
	DWORD retlen;
	long  result;

	result = RegOpenKeyEx(INFO_HKEY, INFO_SUBKEY, RESERVED_ZERO, KEY_QUERY_VALUE, &hCPVTkey);
	if(result != ERROR_SUCCESS){
		return -1;
	}

	retlen = MAX_INFO_LEN;
	result = RegQueryValueEx(hCPVTkey, INFO_FIELD1, RESERVED_NULL, NULL, (BYTE *)Info1, &retlen);
	if(result != ERROR_SUCCESS){
		return -1;
	}

	retlen = MAX_INFO_LEN;
	result = RegQueryValueEx(hCPVTkey, INFO_FIELD2, RESERVED_NULL, NULL, (BYTE *)Info2, &retlen);
	if(result != ERROR_SUCCESS){
		return -1;
	}

	retlen = MAX_INFO_LEN;
	result = RegQueryValueEx(hCPVTkey, INFO_FIELD3, RESERVED_NULL, NULL, (BYTE *)Info3, &retlen);
	if(result != ERROR_SUCCESS){
		return -1;
	}

	retlen = MAX_INFO_LEN;
	result = RegQueryValueEx(hCPVTkey, INFO_FIELD4, RESERVED_NULL, NULL, (BYTE *)Info4, &retlen);
	if(result != ERROR_SUCCESS){
		return -1;
	}

	retlen = MAX_INFO_LEN;
	result = RegQueryValueEx(hCPVTkey, INFO_FIELD5, RESERVED_NULL, NULL, (BYTE *)Info5, &retlen);
	if(result != ERROR_SUCCESS){
		return -1;
	}

	RegCloseKey(hCPVTkey);

	return 0;
}
