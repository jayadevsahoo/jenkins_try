 
#ifndef SupportTypeSetting_HH
#define SupportTypeSetting_HH

//====================================================================
//
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
//@ Class:  SupportTypeSetting - Support Type Setting.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/SupportTypeSetting.hhv   25.0.4.0   19 Nov 2013 14:27:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 013   By: gdc    Date: 31-Dec-2008    SCR Number: 6177
//  Project:  840S
//  Description:
//      Restructured class to correctly revert to the user-selected value 
// 		when an "observed" setting has forced a change to the value of
// 		this setting. Changed getNewPatient method to provide default value 
// 		for	this setting based on the settings it depends upon. Implemented
// 		derived method setAdjustedValue to save the user-selected value for
// 		so it can be used by valueUpdate in handling changes to higher level
// 		settings such as vent-type.
//
//  Revision: 012   By: gdc    Date: 24-Sep-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal non-invasive CPAP.
//
//  Revision: 011  By: gdc    Date: 18-Feb-2005    SCR Number: 6144
//  Project:  NIV1
//  Description:
//      Changes to support NIV.
//   
//  Revision: 010   By: sah   Date:  23-May-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//      PAV project-related changes:
//      *  added new member, 'isLastSpontValuePav_', for supporting
//         the automatic changing of this setting, based on changes to
//         mode (see 'valueUpdate()')
//      *  added override of 'resetState()' to clear new
//         'isLastSpontValuePav_' flag
//      *  added override of 'calcNewValue()' to clear new
//         'isLastSpontValuePav_' flag, when the user changes the setting
//         directly
//
//  Revision: 009  By: sah    Date:  05-May-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//	VTPC project-related changes:
//      *  changed 'isEnabledValue()' to a public method, to provide
//         support for the new drop-down menus
//      *  added new member, 'isLastSpontValueVsv_', for supporting the
//         automatic changing of this setting, based on changes to mode
//         (see 'valueUpdate()')
//      *  added override of 'resetState()' to clear new
//         'isLastSpontValueVsv_' flag
//      *  added override of 'calcNewValue()' to clear new
//         'isLastSpontValueVsv_' flag, when the user changes the setting
//         directly
//
//  Revision: 006  By: sah     Date:  02-Aug-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//	VTPC project-related changes:
//
//  Revision: 005   By: sah   Date:  23-May-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//      PAV project-related changes:
//
//  Revision: 004   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'getApplicability()' method
//	*  now an observer of mode setting, therefore 'valueUpdate()',
//	   'doRetainAttachment()' and 'settingObserverInit()' overridden from
//	   observer base class
//
//  Revision: 003   By: sah   Date:  04-Feb-1999    DR Number: 5322
//  Project:  ATC
//  Description:
//	Project-related changes:
//	*  added new 'isEnabledValue_()' method to determine whether new
//	   'ATC' value is allowed, or not
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Initial version.
//  
//====================================================================

//@ Usage-Classes
#include "BatchDiscreteSetting.hh"
#include "SettingObserver.hh"
#include "ModeValue.hh"
#include "VentTypeValue.hh"
//@ End-Usage


class SupportTypeSetting : public BatchDiscreteSetting, public SettingObserver
{
  public:
    SupportTypeSetting(void);
    virtual ~SupportTypeSetting(void);

	// Setting virtual
	virtual void  setAdjustedValue(const SettingValue& newValue);

    virtual Applicability::Id  getApplicability(
			    const Notification::ChangeQualifier qualifierId
					       ) const;

    virtual Boolean isEnabledValue(const DiscreteValue value) const;

    virtual const BoundStatus&  calcNewValue(const Int16 knobDelta);

    virtual void  calcTransition(const DiscreteValue newValue,
                                 const DiscreteValue currValue);

    virtual SettingValue  getNewPatientValue(void) const;

    virtual void  resetState(void);

    // SettingObserver methods...
    virtual void  valueUpdate(const Notification::ChangeQualifier qualifierId,
			      const SettingSubject*               pSubject);
    virtual Boolean  doRetainAttachment(const SettingSubject* pSubject) const;

    virtual void  settingObserverInit(void);

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL, 
			   const char*       pPredicate = NULL);

  private:
    SupportTypeSetting(const SupportTypeSetting&); // not implemented...
    void  operator=(const SupportTypeSetting&);	   // not implemented...

    //@ Data-Member:  operatorSelectedValue_
    // Stores the spontaneous type that was selected by the operator.
	DiscreteValue operatorSelectedValue_;

    //@ Data-Member:  isChangeForced_
	// TRUE when a vent-type or mode change forces a change to the spontaneous type
	Boolean isChangeForced_;
};


#endif // SupportTypeSetting_HH 
