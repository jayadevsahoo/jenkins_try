#include "StdAfx.h"
#include "TimeSnapshot.h"

TimeSnapshot* TimeSnapshot::instance = NULL;
TimeSnapshot::TimeSnapshot(void)
{
	m_dwBitbltTotal = m_dwBmpTotal = m_dwFirstTime = m_dwEndTime = 0;
	m_dwBmpRecord = m_dwBitbltRecord = 0;
}

TimeSnapshot::~TimeSnapshot(void)
{
}

void TimeSnapshot::start()
{
	m_dwFirstTime = ::GetTickCount();
}

void TimeSnapshot::stop()
{
	m_dwEndTime = ::GetTickCount();
	DWORD totalTime = m_dwEndTime - m_dwFirstTime;
}

void TimeSnapshot::bmpStart()
{
	m_dwBmpRecord = ::GetTickCount();
}

void TimeSnapshot::bmpEnd()
{
	m_dwBmpTotal += GetTickCount() - m_dwBmpRecord;
}

void TimeSnapshot::bitbltStart()
{
	m_dwBitbltRecord = GetTickCount();
}

void TimeSnapshot::bitbltEnd()
{
	m_dwBitbltTotal = GetTickCount() - m_dwBitbltRecord;
}

TimeSnapshot* TimeSnapshot::getInstance()
{
	if (instance == NULL)
		instance = new TimeSnapshot;

	return instance;
}
