// PowerResetDlg.cpp : implementation file
//

#include "stdafx.h"
#include "PowerResetDlg.h"


// CPowerResetDlg dialog

IMPLEMENT_DYNAMIC(CPowerResetDlg, CDialog)

CPowerResetDlg::CPowerResetDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPowerResetDlg::IDD, pParent)
{

}

CPowerResetDlg::~CPowerResetDlg()
{
}

void CPowerResetDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EXPLORER_RESET, m_webCtrl);
}


BEGIN_MESSAGE_MAP(CPowerResetDlg, CDialog)
END_MESSAGE_MAP()


// CPowerResetDlg message handlers

BOOL CPowerResetDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

//	bSwitchedOn = false;
//	bSwitchedOff = false;
	bSwitched = false;



	/////////////
	static bool b = true;
	CString IPCmd0 = "http://admin:12345678@192.168.0.50/Set.cmd?CMD=SetPower+P60=0+P61=0+P62=0+P63=0";
	CString IPCmd1 = "http://admin:12345678@192.168.0.50/Set.cmd?CMD=SetPower+P60=1+P61=0+P62=0+P63=0";
	if(b) m_webCtrl.Navigate( IPCmd1 ,NULL,NULL,NULL,NULL);
	else m_webCtrl.Navigate( IPCmd0 ,NULL,NULL,NULL,NULL);
	b  = !(b);

	////////////

//	m_webCtrl.Navigate(((CSwatApp*)AfxGetApp())->m_strPowerSwitch_IPAddr,NULL,NULL,NULL,NULL);



	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}
BEGIN_EVENTSINK_MAP(CPowerResetDlg, CDialog)
	ON_EVENT(CPowerResetDlg, IDC_EXPLORER_RESET, 259, CPowerResetDlg::DocumentCompleteExplorerReset, VTS_DISPATCH VTS_PVARIANT)
END_EVENTSINK_MAP()

void CPowerResetDlg::DocumentCompleteExplorerReset(LPDISPATCH pDisp, VARIANT* URL)
{
	/* TODO: Add your message handler code here

	m_SwitchState = GetStatus();//don't click anything in getstatus which create endless loop
	

	if(m_SwitchState == ERROR_GETTING_STATE)
	{
		LOG_MESSAGE(LM_INFO,"GetStatus() returned error_getting_state %s...Refreshing now", __FUNCTION__);
		m_webCtrl.Refresh();
		return;
	}

	if(m_SwitchState == LOGGIN_ERROR)
	{
		LOG_MESSAGE(LM_INFO,"GetStatus() returned log_in_error %s...calling ClickLogInError()", __FUNCTION__);
		ClickLogInError();
		return;
	}
	if(m_SwitchState == NOT_LOGGED_IN)
	{
		LOG_MESSAGE(LM_INFO,"GetStatus() returned not_logged_in in %s, calling LogIn() ", __FUNCTION__);
		LogIn();
		return;
	}

	if(!bSwitched) //if not switched yet, switch now
	{

		if(!SwitchOnOff(bToSwitchOnOROff))
		{//could not switch, navigate back to home page and log in again
			m_webCtrl.Navigate(((CSwatApp*)AfxGetApp())->m_strPowerSwitch_IPAddr,NULL,NULL,NULL,NULL);
		}
		return;
	}
	//reaches here only when logged in and switched
	LOG_MESSAGE(LM_INFO,"Calling OnOK() now  %s", __FUNCTION__); */
	OnOK();
	LOG_MESSAGE(LM_INFO,"exit  %s", __FUNCTION__);

}
bool CPowerResetDlg::CheckLogin()
{
	LOG_MESSAGE(LM_INFO,"Enter %s", __FUNCTION__);
	HRESULT hr;
	IHTMLDocument2* pDoc= (IHTMLDocument2*) m_webCtrl.get_Document();
	if(pDoc == NULL )
	{
		LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
		return false;
	}
	CComBSTR bstr;
	pDoc->get_title(&bstr);


	if(CComBSTR(bstr)==CComBSTR(L"TF-33x WebControl"))
	{
		//if there was any error in logging, click ok and set logged in to false so it tries to log in again.
		//ok button on error html page has following:
		/*<input TYPE="button" NAME="back" VALUE="OK" ONCLICK="window.location='/'"> */
		void* pElem= ElemFromID(L"back", IID_IHTMLElement);
		IHTMLElement* pSubmit= (IHTMLElement*)pElem;
	
		((CSwatApp*)AfxGetApp())->LogStatus("Error logging in, retrying now...");
		hr= pSubmit->click(); 


		return false;
	};

	return true;

}
bool CPowerResetDlg::LogIn()
{
	LOG_MESSAGE(LM_INFO,"Enter %s", __FUNCTION__);

	void* pElem= ElemFromID(L"Username", IID_IHTMLInputElement) ;
	IHTMLInputElement* pTextBoxUser= (IHTMLInputElement*)pElem;

	HRESULT hr = pTextBoxUser->put_value( L"some" ); // populate a text box
	hr = pTextBoxUser->put_value( L"admin" ); // populate a text box

	pElem= ElemFromID(L"Password", IID_IHTMLInputElement) ;
	IHTMLInputElement* pTextBoxPswd= (IHTMLInputElement*)pElem;

	hr= pTextBoxPswd->put_value( L"some2overwrite" ); 
	hr= pTextBoxPswd->put_value( L"12345678" ); 

	pElem= ElemFromID(L"Submitbtn", IID_IHTMLElement);
	IHTMLElement* pSubmit= (IHTMLElement*)pElem;

	hr= pSubmit->click(); 
	

	LOG_MESSAGE(LM_INFO,"Exit %s", __FUNCTION__);
	return true;

}

//-------------------------------------------------------
// Returns a desired type of IHTMLElement pointer
// to provide access to the object with a specific ID
//
void* CPowerResetDlg::ElemFromID( LPWSTR szID, IID nTypeIID )
{
	LOG_MESSAGE(LM_INFO,"Enter %s", __FUNCTION__);
	HRESULT hr;
	IHTMLDocument2* pDoc= (IHTMLDocument2*) m_webCtrl.get_Document();
	IHTMLElementCollection* pAll=  NULL;
 
	hr= pDoc->get_all( &pAll ); 

	CComVariant vElement( szID ); // the id or name of the control
	CComVariant vIndex(0,VT_I4);  // 0 (presume it's not a collection)
 
	IDispatch* pDisp;
 	hr= pAll->item(vElement,vIndex,&pDisp);
	if(pDisp == NULL) return NULL;
 
	void* pElement;   // will coerce to desired type later
 	hr= pDisp->QueryInterface( nTypeIID,(void**)&pElement);

	LOG_MESSAGE(LM_INFO,"Exit %s", __FUNCTION__);
	return( pElement );
}

bool CPowerResetDlg::ClickLogInError()
{
	LOG_MESSAGE(LM_INFO,"Enter %s", __FUNCTION__);
	HRESULT hr;
	IHTMLDocument2* pDoc= (IHTMLDocument2*) m_webCtrl.get_Document();
	if(pDoc == NULL )
	{
		LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
		return false;
	}
	CComBSTR bstr;
	pDoc->get_title(&bstr);
	if(CComBSTR(bstr)==CComBSTR(L"TF-33x WebControl"))
	{
		//if there was any error in logging, click ok and set logged in to false so it tries to log in again.
		//ok button on error html page has following:
		/*<input TYPE="button" NAME="back" VALUE="OK" ONCLICK="window.location='/'"> */
		void* pElem= ElemFromID(L"back", IID_IHTMLElement);
		IHTMLElement* pSubmit= (IHTMLElement*)pElem;

		((CSwatApp*)AfxGetApp())->LogStatus("Error logging in, retrying now...");
//		m_SwitchState = NOT_LOGGED_IN;
		hr= pSubmit->click(); 
		return false;
	};
	return true;

}


bool CPowerResetDlg::SwitchOnOff(bool bOnOff)
{
	LOG_MESSAGE(LM_INFO,"Enter %s", __FUNCTION__);
	HRESULT hr;
	IHTMLDocument2* pDoc= (IHTMLDocument2*) m_webCtrl.get_Document();
	if(pDoc == NULL )
	{
		LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
		return false;
	}

	IHTMLFramesCollection2 *pAllFrames = NULL;
	hr = pDoc->get_frames(&pAllFrames);
	if(hr != S_OK )
	{
		LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
		return false;
	}
	if(pAllFrames == NULL )
	{
		LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
		return false;
	}

	CComVariant vIndex(1,VT_I4);  // Get frame at location 1
	CComVariant vFrame(VT_DISPATCH);  

	hr = pAllFrames->item(&vIndex, &vFrame);
	if(hr != S_OK )
	{
		LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
		return false;
	}
	void* pVoid;
	hr= vFrame.pdispVal->QueryInterface( IID_IHTMLWindow2,(void**)&pVoid);
	if(vFrame.pdispVal == NULL )
	{
		LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
		return false;
	}
	if(hr != S_OK )
	{
		LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
		return false;
	}
	IHTMLWindow2* pWin= (IHTMLWindow2*)pVoid;
	IHTMLDocument2* pDocF0;
	pWin->get_document(&pDocF0);
	if(pWin == NULL )
	{
		LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
		return false;
	}
	if( pDocF0 == NULL)
	{
		LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
		return false;
	}
	IHTMLElementCollection* pAll=  NULL;
	hr= pDocF0->get_all( &pAll ); 
	if(hr != S_OK )
	{
		LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
		return false;
	}
	if( pAll == NULL)
	{
		LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
		return false;
	}

	//click ON
	CComVariant vElementF0( L"P60" ); // the id or name of the control
	CComVariant vIndex0(0,VT_I4);  // 0 
	CComVariant vIndex1(1,VT_I4);
	IDispatch* pDisp;

	if(bOnOff)
	{
 		hr= pAll->item(vElementF0, vIndex0, &pDisp);
	}
	else
	{
		hr= pAll->item(vElementF0, vIndex1, &pDisp);
	}

	if(hr != S_OK )
	{
		LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
		return false;
	}
	if( pDisp == NULL)
	{
		LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
		return false;
	}

	void* pElement;   
 	hr= pDisp->QueryInterface( IID_IHTMLElement,(void**)&pElement);
	if(hr != S_OK )
	{
		LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
		return false;
	}
	IHTMLElement* pSubmit= (IHTMLElement*)pElement;
	if( pSubmit == NULL)
	{
		LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
		return false;
	}

	hr= pSubmit->click(); 
	if(hr != S_OK )
	{
		LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
		return false;
	}

	//click APPLY
	CComVariant vElementApply( L"Apply" ); 
	if( pAll == NULL)
	{
		LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
		return false;
	}
 	hr= pAll->item(vElementApply, vIndex0, &pDisp);
	if(hr != S_OK )
	{
		LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
		return false;
	}
	if(pDisp == NULL)
	{
		LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
		return false;
	}
 	hr= pDisp->QueryInterface( IID_IHTMLElement,(void**)&pElement);
	if(hr != S_OK )
	{
		LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
		return false;
	}
	pSubmit= (IHTMLElement*)pElement;
	if(pSubmit==NULL)
	{
		LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
		return false;
	}
	hr= pSubmit->click(); 
	if(hr != S_OK )
	{
		LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
		return false;
	}

	bSwitched = true;

	LOG_MESSAGE(LM_INFO,"Exit %s", __FUNCTION__);

	return true;
	
}

power_page_status_t CPowerResetDlg::GetStatus()
{
	LOG_MESSAGE(LM_INFO,"Enter %s", __FUNCTION__);
	//check if login error
	{
		IHTMLDocument2* pDoc= (IHTMLDocument2*) m_webCtrl.get_Document();
		if(pDoc == NULL )
		{
			LOG_MESSAGE(LM_ERROR,"error %s", __FUNCTION__);
			m_SwitchState =  ERROR_GETTING_STATE;
		}
		CComBSTR bstr;
		pDoc->get_title(&bstr);

		if(CComBSTR(bstr)==CComBSTR(L"TF-33x WebControl"))
		{	m_SwitchState = LOGGIN_ERROR;
			return LOGGIN_ERROR;
		}
	}
	//check if on login page (not_logged_in)
	{
		void* pElem= ElemFromID(L"Username", IID_IHTMLInputElement) ;
		if(pElem != NULL) 
		{
			LOG_MESSAGE(LM_INFO,"pTextBoxUser is not nill %s", __FUNCTION__);
			return NOT_LOGGED_IN;
		}
	}
	//check if 'Apply' available
	{
		LOG_MESSAGE(LM_INFO,"checking for Apply in %s", __FUNCTION__);

					if(((CSwatApp*)AfxGetApp())->m_strPowerSwitch_IPAddr.IsEmpty()) 
					{
						LOG_MESSAGE(LM_INFO,"Pwr IP empty in %s returning", __FUNCTION__);
						return ERROR_GETTING_STATE;
					}


					LOG_MESSAGE(LM_INFO,"Enter %s", __FUNCTION__);
					HRESULT hr;
					IHTMLDocument2* pDoc= (IHTMLDocument2*) m_webCtrl.get_Document();
					if(pDoc == NULL )
					{
						LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
						return ERROR_GETTING_STATE;
					}

					IHTMLFramesCollection2 *pAllFrames = NULL;
					hr = pDoc->get_frames(&pAllFrames);
					if(hr != S_OK )
					{
						LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
						return ERROR_GETTING_STATE;
					}
					if(pAllFrames == NULL )
					{
						LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
						return ERROR_GETTING_STATE;
					}

					CComVariant vIndex(1,VT_I4);  // Get frame at location 1
					CComVariant vFrame(VT_DISPATCH);  

					hr = pAllFrames->item(&vIndex, &vFrame);
					if(hr != S_OK )
					{
						LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
						return ERROR_GETTING_STATE;
					}
					void* pVoid;
					hr= vFrame.pdispVal->QueryInterface( IID_IHTMLWindow2,(void**)&pVoid);
					if(vFrame.pdispVal == NULL )
					{
						LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
						return ERROR_GETTING_STATE;
					}
					if(hr != S_OK )
					{
						LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
						return ERROR_GETTING_STATE;
					}
					IHTMLWindow2* pWin= (IHTMLWindow2*)pVoid;
					IHTMLDocument2* pDocF0;
					pWin->get_document(&pDocF0);
					if(pWin == NULL )
					{
						LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
						return ERROR_GETTING_STATE;
					}
					if( pDocF0 == NULL)
					{
						LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
						return ERROR_GETTING_STATE;
					}
					IHTMLElementCollection* pAll=  NULL;
					hr= pDocF0->get_all( &pAll ); 
					if(hr != S_OK )
					{
						LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
						return ERROR_GETTING_STATE;
					}
					if( pAll == NULL)
					{
						LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
						return ERROR_GETTING_STATE;
					}

					
				//	CComVariant vElementF0( L"P60" ); // the id or name of the control
					CComVariant vIndex0(0,VT_I4);  // 0 
					CComVariant vIndex1(1,VT_I4);
					IDispatch* pDisp;
					void* pElement;

		
					//Check APPLY
					CComVariant vElementApply( L"Apply" ); 
					if( pAll == NULL)
					{
						LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
						return ERROR_GETTING_STATE;
					}
 					hr= pAll->item(vElementApply, vIndex0, &pDisp);
					if(hr != S_OK )
					{
						LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
						return ERROR_GETTING_STATE;
					}
					if(pDisp == NULL)
					{
						LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
						return ERROR_GETTING_STATE;
					}
 					hr= pDisp->QueryInterface( IID_IHTMLElement,(void**)&pElement);
					if(hr != S_OK )
					{
						LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
						return ERROR_GETTING_STATE;
					}
					IHTMLElement* pSubmit= (IHTMLElement*)pElement;
					if(pSubmit==NULL)
					{
						LOG_MESSAGE(LM_INFO,"error %s", __FUNCTION__);
						return ERROR_GETTING_STATE;
					}
					else
					{
						return LOGGED_IN;
					}
						

					LOG_MESSAGE(LM_INFO,"Exit %s", __FUNCTION__);

					return ERROR_GETTING_STATE;





	}
	return ERROR_GETTING_STATE;

}