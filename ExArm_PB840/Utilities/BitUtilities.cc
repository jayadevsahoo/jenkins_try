#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sotred in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//    Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Filename:  BitUtilities - Bit Operation Utilities.
//---------------------------------------------------------------------
//@ Interface-Description
//  This provides a set of utilities to use when dealing with bits
//  within an array of integers.  The first two functions, 'BitMask()'
//  and 'BitOffset()', are used by the other functions to determine
//  a mask and offset for a particular bit that is within a particular
//  integer of the array.  These two functions may, or may not, be useful
//  anywhere else in the system, but they are declared publicly so as
//  to be inlined and used within the other functions.  The other
//  functions, 'IsBitSet()', 'SetBit()', and 'ClearBit()', provide means
//  of testing and changing a bit within the array of integers.
//---------------------------------------------------------------------
//@ Rationale
//  These are utilities are available for use throughout the Sigma
//  System for standard bit operations.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Each function uses very simple, and standard, bit-wise operations.
//---------------------------------------------------------------------
//@ Fault-Handling
//  None.
//---------------------------------------------------------------------
//@ Restrictions
//  These utilities don't have a way to ensure that the array being
//  used is big enough to contain each bit being tested/changed,
//  therefore the following restriction is to be met by the client(s)
//  of these utilities:
//
//  sizeof(arrUints[]) >= ((bitNum + ::BITS_PER_LWORD-1) / ::BITS_PER_LWORD)
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Utilities/vcssrc/BitUtilities.ccv   25.0.4.0   19 Nov 2013 14:35:12   pvcs  $
//
//@ Modification-Log
//  
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//=====================================================================

#include "BitUtilities.hh"

Uint32 CreateBitMask(Uint8 numBits, Uint8 startIndex)
{
	Uint32 ret = 0;
	for(Uint32 i=0; i<numBits; i++)
		ret = (ret << 1) | 0x1;
	if(startIndex > numBits)
		ret <<= (startIndex-numBits);

	return ret;
}

//@ Usage-Classes
//@ End-Usage

//@ Code...
