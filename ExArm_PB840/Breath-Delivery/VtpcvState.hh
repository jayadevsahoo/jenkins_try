#ifndef VtpcvState_HH
#define VtpcvState_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: VtpcvState - State of VCP delivery.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/VtpcvState.hhv   25.0.4.0   19 Nov 2013 14:00:18   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001   By: jja Date:  14-Mar-2002    DR Number: 5860
//  Project:  VCP
//  Description:
//      Created for the possible states of a vloume targeted breath,
//      i.e. STARTUP or NORMAL.  Applies to VC+ or VS.
//
//====================================================================


//@ Usage-Classes
//@ End-Usage


struct VtpcvState
{
  //@ Type:  State
  // All of the possible state values of Vtpcv.
  enum Id
  {
	STARTUP_VTPCV,
	NORMAL_VTPCV
  };
};


#endif // VtpcvState_HH 
