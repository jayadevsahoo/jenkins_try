/*
================================================================================
*             Texas Instruments OMAP(TM) Platform Software
* (c) Copyright Texas Instruments, Incorporated. All Rights Reserved.
*
* Use of this software is controlled by the terms and conditions found
* in the license agreement under which this software has been supplied.
*
================================================================================
*/
#pragma warning(push)
#pragma warning(disable : 6385)


#include "bsp.h"
#include "bsp_cfg.h"
#include "ceddkex.h"
#include "image_cfg.h"
#include "am38xx_display_cfg.h"

//
//  Defines
//

//------------------------------------------------------------------------------
// Get frame buffer physical memory address and size
BOOL
Display_GetFrameBufMemory(
    DWORD   *pFrameBufMemLen,
    DWORD   *pFrameBufMemAddr
    )
{
    //  Return frame buffer memory parameters
    if( pFrameBufMemLen ) {
        *pFrameBufMemLen = IMAGE_WINCE_DISPLAY_FRAMEBUF_SIZE;
    }

    if( pFrameBufMemAddr ) {
        *pFrameBufMemAddr = IMAGE_WINCE_DISPLAY_FRAMEBUF_PA;
    }

    DEBUGMSG(ZONE_FUNCTION, (L"Display_GetFrameBufMemory. Physical Addr=0x%x, size=0x%x\r\n", 
                               *pFrameBufMemAddr, *pFrameBufMemLen));

    return TRUE;
}

// Get vpss shared memory (shared memory to communicate with VPSS M3 display processor) address and size
BOOL
Display_GetVPSSSharedMemory(
    DWORD   *pVPSSSharedMemLen,
    DWORD   *pVPSSSharedMemAddr
    )
{
    //  Return vpss shared memory parameters
    if( pVPSSSharedMemLen ) {
        *pVPSSSharedMemLen = IMAGE_WINCE_DISPLAY_VPSS_SHAREDMEM_SIZE;
    }

    if( pVPSSSharedMemAddr ) {
        *pVPSSSharedMemAddr = IMAGE_WINCE_DISPLAY_VPSS_SHAREDMEM_PA;
    }

    DEBUGMSG(ZONE_FUNCTION, (L"Display_GetVPSSSharedMemory. Physical Addr=0x%x, size=0x%x\r\n", 
                               *pVPSSSharedMemAddr, *pVPSSSharedMemLen));

    return TRUE;
}


#pragma warning(pop)

