#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// =========================== M O D U L E   D E S C R I P T I O N ====
//@ Filename: SimvTimeInspTrigger - Inspiratory time trigger for SIMV.
//---------------------------------------------------------------------
//@ Interface-Description
//  This class triggers the start of inspiration when SIMV is the active
//  mode of breathing.  The trigger is contained on a list in the 
//  BreathTriggerMediator.  The object enabling this trigger must contain
//  the algorithm for determining this trigger's applicability.
//---------------------------------------------------------------------
//@ Rationale
//  This class implements the algorithm for determining when it is time
//  to start a mandatory inspiration during SIMV.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This trigger may be enabled or disabled.  The trigger condition is
//  checked every BD cycle when the trigger is enabled, and on the active
//  trigger list in the BreathTriggerMediator.  When this trigger fires,
//  it notifies the BreathPhaseScheduler that is it time to transition
//  to a new breath phase.
//---------------------------------------------------------------------
//@ Fault-Handling
//  n/a.
//---------------------------------------------------------------------
//@ Restrictions
//  Only one instance of this class may be created.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/SimvTimeInspTrigger.ccv   25.0.4.0   19 Nov 2013 14:00:08   pvcs  $
//
//@ Modification-Log
//
//  Revision: 008  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 007  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 006  By:  iv    Date:  10-Apr-1997    DR Number: DCS 1895
//       Project:  Sigma (R8027)
//       Description:
//             In triggerCondition_() - eliminated the timeUp_ from the
//             apnea elapsed time condition.
//
//  Revision: 005  By:  sp    Date:  31-Oct-1996    DR Number:
//       Project:  Sigma (R8027)
//       Description:
//             Inspection rework.
//
//  Revision: 004  By:  iv    Date:  11-July-1996    DR Number: DCS 1072
//       Project:  Sigma (R8027)
//       Description:
//             Added a condition to the trigger such that when apnea is not
//             possible, and apnea elapsed time has been exceeded, 
//             a mandatory breath will be delivered. This condition
//             is common to spont and non spont breaths.
//
//  Revision: 003  By:  sp    Date:  4-Mar-1996    DR Number:674
//       Project:  Sigma (R8027)
//       Description:
//             Remove #ifndef.
//
//  Revision: 002  By:  sp    Date:  15-Feb-1994    DR Number:600
//       Project:  Sigma (R8027)
//       Description:
//             added #include "BD_IO_Devices.hh" since CYCLE_TIME_MS was moved there.
//             Removed scope operator (::) from TRUE/FALSE useage.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#  include "SimvTimeInspTrigger.hh"
#  include "BreathMiscRefs.hh"
#  include "TimersRefs.hh"
#  include "BD_IO_Devices.hh"

//@ Usage-Classes
#  include "IntervalTimer.hh"
#  include "SimvScheduler.hh"
#  include "BreathRecord.hh"
#  include "BreathSet.hh"
#  include "Trigger.hh"
#  include "ApneaInterval.hh"
//@ End-Usage


//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SimvTimeInspTrigger()  
//
//@ Interface-Description
//      This constructor accepts a reference to an interval timer for
//      timing the mandatory part of the SIMV cycle.
//---------------------------------------------------------------------
//@ Implementation-Description
//      Initialize the timer using TimerBreathTrigger's constructor.
//	Initialize TriggerId_ using BreathTrigger's constructor.
//      This constructor does not need to initialize anything.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SimvTimeInspTrigger::SimvTimeInspTrigger( IntervalTimer& iTimer)
 : TimerBreathTrigger (iTimer, Trigger::SIMV_TIME_INSP)
{
  CALL_TRACE("SimvTimeInspTrigger()");
  // $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~SimvTimeInspTrigger() 
//
//@ Interface-Description
//	Destructor
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SimvTimeInspTrigger::~SimvTimeInspTrigger(void)
{
  CALL_TRACE("~SimvTimeInspTrigger()");

}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
SimvTimeInspTrigger::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, SIMVTIMEINSPTRIGGER,
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)



//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: triggerCondition_()
//
//@ Interface-Description
//    This method takes no parameters.  If the trigger condition has 
//    occured, the method returns true, otherwise, the method returns 
//    false.
//---------------------------------------------------------------------
//@ Implementation-Description
//    $[04234]
//    One of the following conditions must be met if an inspiration is to be
//    triggered:
//    1.  breathType is SPONT and the timer expires and phase is not restricted 
//    2.  a) breathType is not SPONT and 
//        b) the timer expires and 
//        c) phase is not restricted or there is only one BD cycle remaining 
//           in the SIMV cycle.
//    3.  a) the timer expires and 
//        b) apnea is not possible and
//        c) apnea time has elapsed. 
//    The third condition makes sure that apnea won't start if it is not possible,
//    instead, an SIMV mandatory breath will be delivered. This condition does not
//    distinguish between SPONT and Mandatory breath types since a spont breath
//    should not be restricted after 10 seconds (minimum apnea interval) and a mandatory
//    breath should have been started more than 60/f seconds ago (since apnea is not
//    possible).
//        
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

Boolean
SimvTimeInspTrigger::triggerCondition_(void)
{
  CALL_TRACE("triggerCondition_()");
  Boolean returnValue = FALSE;
  Int32 currentTime = RSimvCycleTimer.getCurrentTime();
  Int32 SimvIntervalTime = RSimvCycleTimer.getTargetTime();
  const ::BreathType breathType = 
	(RBreathSet.getCurrentBreathRecord())->getBreathType();
  BreathRecord* pCurrentRecord = RBreathSet.getCurrentBreathRecord();
  Real32 breathDuration =  pCurrentRecord->getBreathDuration();
  Real32 apneaIntervalForSimvInspTrigger = (Real32)RApneaInterval.getApneaInterval() + 10.0F;

  if ( ( (breathType == ::SPONT) && timeUp_ && 
         !BreathRecord::IsPhaseRestricted() ) ||
       ( (breathType != ::SPONT) && timeUp_ &&
         (!BreathRecord::IsPhaseRestricted() ||
                                (SimvIntervalTime - currentTime <= CYCLE_TIME_MS)) ) ||
       ( breathDuration >= apneaIntervalForSimvInspTrigger &&
                         !BreathPhaseScheduler::CheckIfApneaPossible() ) )
  {
    // $[TI1]
    returnValue = TRUE;
  }
  // $[TI2]

  //TRUE: $[TI3]
  //FALSE: $[TI4]
  return (returnValue);
}

//=====================================================================
//
//  Private Methods...
//
//=====================================================================
