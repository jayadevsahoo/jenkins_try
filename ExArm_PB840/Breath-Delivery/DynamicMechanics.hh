#ifndef DynamicMechanics_HH
#define DynamicMechanics_HH
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2006, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: DynamicMechanics - The class for dynamic mechanics data and 
//                           related calculations.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/DynamicMechanics.hhv   25.0.4.0   19 Nov 2013 13:59:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002   By: gdc   Date:  20-Feb-2007    SCR Number: 6345
//  Project:  RESPM
//  Description:
//		Removed C20/C related code.
//
//  Revision: 001   By: gdc   Date:  13-Oct-2006    SCR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM initial version.
//
//=====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"

//@ Usage-Classes

#include "Resistance.hh"
#include "SettingId.hh"

//@ End-Usage


class DynamicMechanics
{
  public:

    DynamicMechanics(void) ;
    virtual ~DynamicMechanics( void) ;

	struct RmDataType
	{
		Real32 flow;
		Real32 volume;
		Real32 pressure;
	};

    void newCycle(void);
    void newBreath(void);
	void update(void);

	inline Real32 getCompliance(void) const;
    inline Real32 getResistance(void) const;
    inline Real32 getPeakSpontInspFlow(void) const;
    
	static void SoftFault(const SoftFaultID softFaultID,
							const Uint32      lineNumber,
							const char*       pFileName  = NULL, 
							const char*       pPredicate = NULL);

  protected:

  private:
    DynamicMechanics( const DynamicMechanics&) ;    	// not implemented
    void operator=( const DynamicMechanics&) ;	// not implemented

	void updateCompliance_(void);
	void updateResistance_(void);
	void updatePeakSpontInspFlow_(void);

    //@ Data-Member:  peepSetting_
    // The PEEP setting for the current breath that is excluded from measured pressure 
	Real32   peepSetting_;

    //@ Data-Member:  complianceBreathCount_
    // The number of breaths included in the current dynamic compliance average
	Int32    complianceBreathCount_;

    //@ Data-Member:  psfBreathCount_
    // The number of breaths included in the current Peak Spontaneous Flow average
	Int32    psfBreathCount_;

    //@ Data-Member:  resistanceBreathCount_
    // The number of breaths included in the current dynamic resistance average
	Int32    resistanceBreathCount_;

    //@ Data-Member:  timeSinceComplianceUpdate_
    // Time elapsed since a valid compliance breath was included in the average
	Int32    timeSinceComplianceUpdate_;

    //@ Data-Member:  timeSinceResistanceUpdate_
    // Time elapsed since a valid resistance breath was included in the average
	Int32    timeSinceResistanceUpdate_;

    //@ Data-Member:  timeSincePsfUpdate_
    // Time elapsed since a valid PSF breath was included in the average
	Int32    timeSincePsfUpdate_;

    //@ Data-Member:  complianceAlpha_
    // The coefficient for the alpha filter used to average dynamic compliance data
	Real32   complianceAlpha_;

    //@ Data-Member:  resistanceAlpha_
    // The coefficient for the alpha filter used to average dynamic resistance data
	Real32   resistanceAlpha_;

    //@ Data-Member:  psfAlpha_
    // The coefficient for the alpha filter used to average peak spontaneous flow
	Real32   psfAlpha_;

    //@ Data-Member:  singleBreathCompliance_
    // dynamic compliance computed for the last breath
	Real32   singleBreathCompliance_;

    //@ Data-Member:  singleBreathCompliance20_
    // dynamic compliance computed for the final 20% of the last breath
	Real32   singleBreathCompliance20_;

    //@ Data-Member:  singleBreathResistance_
    // dynamic resistance computed for the last breath
	Real32   singleBreathResistance_;

    //@ Data-Member:  singleBreathPsf_
    // peak spontaneous flow for the last breath
	Real32   singleBreathPsf_;

    //@ Data-Member:  compliance_
    // computed averaged dynamic compliance
	Real32   compliance_;

    //@ Data-Member:  resistance_;
    // computed averaged dynamic resistance
	Real32   resistance_;

    //@ Data-Member:  peakSpontInspFlow_;
    // computed averaged peak spontaneous flow
	Real32   peakSpontInspFlow_;

    //@ Data-Member:  sumVolumeDotFlow_
    // dot product of Vi * Qi
	Real32 sumVolumeDotFlow_;

    //@ Data-Member:  sumVolumeDotVolume_
    // dot product of Vi * Vi
	Real32 sumVolumeDotVolume_;

    //@ Data-Member:  sumFlowDotFlow_
    // dot product of Qi * Qi
	Real32 sumFlowDotFlow_;

    //@ Data-Member:  sumPressureDotVolume_
    // dot product of Pi * Vi
	Real32 sumPressureDotVolume_;

    //@ Data-Member:  sumPressureDotFlow_
    // dot product of Pi * Qi
	Real32 sumPressureDotFlow_;

	//@ Data-Member:  RmData
	// array of respiratory mechanics data
	static DynamicMechanics::RmDataType RmData[1600];

	//@ Data-Member:  RmData
	// current index into RmData array
	static Int32      RmDataIndex;
} ;

// Inlined methods
#include "DynamicMechanics.in"

#endif // DynamicMechanics_HH 
