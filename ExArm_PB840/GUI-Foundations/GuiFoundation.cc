#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1995, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
//@ Class: GuiFoundation - This is the main subsystem class, which
// contains the globals used by the subsystem.
//---------------------------------------------------------------------
//@ Interface-Description
// This is the "subsystem" class for the GUI-Foundations subsystem. It provides
// the main subsystem initialization function, GuiFoundation::Initialize().
// There are also a number of debug methods.
//
// The two "global" upper and lower base containers are public data members of
// this class.  Actually these members are references to BaseContainers created
// using the "placement new" feature of C++.  Note that
// GuiFoundation::Initialize() is the only method that can instantiate
// BaseContainers, because it has the friendship of the BaseContainer class,
// and the BaseContainer constructor is private.
//---------------------------------------------------------------------
//@ Rationale
// This is the "main" class for the GUI-Foundations subsystem, which
// encapsulates the "global" subsystem objects (upper base container and
// lower base container) as well as the highest level subsystem
// initialization function, GuiFoundation::Initialize().
//---------------------------------------------------------------------
//@ Implementation-Description
// The Initialize() method constructs the "global" upper and lower base
// containers into fixed-sized memory pools using the placement new operator.
// These pools, ::LowerBaseMemory_ and ::UpperBaseMemory_, are defined as
// static Word arrays within this file. The Initialize() method is also
// responsible for calling the Initialize() methods of this subsystem's classes
// in the proper order.
//---------------------------------------------------------------------
//@ Fault-Handling
// This class uses the "contract programming" technique.  This technique
// utilizes the class assertion and class precondition macros provided by
// the Utilities subsystem to generate error messages in response to software
// faults.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Foundations/vcssrc/GuiFoundation.ccv   25.0.4.0   19 Nov 2013 14:11:42   pvcs  $
//
//@ Modification-Log
//
//  Revision: 011  By:  gdc    Date:  10-Dec-2008    SCR Number: 6377
//  Project:  840S
//  Description:
//	Removed DELTA project single screen support.
//
//  Revision: 010   By: gdc    Date:  21-May-2007    SCR Number: 6330
//  Project: Trend
//  Description:
//	Removed SUN prototype code.
// 
//  Revision: 009   By: gdc    Date:  28-Aug-2000    DCS Number: 5493
//  Project: GuiComms
//  Description:
//	Added Single Screen Compatibility. Eliminated repaint after 
//  BaseContainer initialization to remove white flash during init.
//  GuiApp will repaint when graphics are ready to be displayed.
//
//  Revision: 008   By: gdc    Date:  29-Jun-1999    DCS Number: 5337
//  Project: 840 Neonatal
//  Description:
//	Optimized VGA library for Neonatal project to use BitBlt and Windows
//  Bitmap structures.
//
//  Revision: 007   By: sah    Date: 11-Jan-1999   DR Number: 5321
//  Project:  Sigma (R8027)
//  Description:
//      Changed 'SoftFault()' method to non-inline method.
//
//  Revision: 006  By: sah       Date: 03-Oct-1997    DCS Number: 2264
//  Project:  Sigma (R8027)
//  Description:
//      Fixed incorrect handling of VGA access requests/releases, which
//      may have caused the problem with the errant lines outside of the
//      waveform grid.
//
//  Revision: 005  By: sah       Date: 12-Jun-1997    DCS Number: 2208
//    Project:  Sigma (R8027)
//    Description:
//      Incorporated new VGA interface methods, files and types.  Also,
//      removed VGA initialization from this method (it's been moved
//      into VGA's own initialization method.
//
//  Revision: 004  By: mpm      Date: 29-Mar-1996   DCR Number: 473
//    Project:  Sigma (R8027)
//    Description:
//      Added RequestDisplayAccess(), ReleaseDisplayAccess(), SaveDisplayContext(),
//      RestoreDisplayContext() and SetDrawingArea() to allow multiple tasks to
//      share access to the VGA display for drawing purposes.
//
//  Revision: 003  By: yyy      Date: 18-Oct-1995   DCR Number: 594
//    Project:  Sigma (R8027)
//    Description:
//      Removed the #ifdef SIGMA_DEBUG statement for arrayDebugFlags[].
//
//  Revision: 002  By: yyy      Date: 02-Jul-1995   DCR Number: 473 
//    Project:  Sigma (R8027)
//    Description:
//      Added two methods RequestDisplayAccess() and ReleaseDisplayAccess().
//      This two methods will synchronize with the Blink task in accessing the VGA
//      display memory.
//
//  Revision: 001  By: yyy      Date: 01-Jan-1995  DR Number: NONE
//    Project:  Sigma (R8027)
//    Description:
//	Integration baseline.
//=====================================================================

#include "GuiFoundation.hh"

//
// GUI-Foundation includes.
//
#include "BaseContainer.hh"
#include "DisplayContext.hh"
#include "NullDisplayContext.hh"
#include "VgaDisplayContext.hh"
#include "Colors.hh"
#include "Image.hh"
#include "Touch.hh"
#include "KeyPanel.hh"
#include "TextFont.hh"
#include "VgaGraphicsDriver.hh"

Uint GuiFoundation::DisplayAccessNesting_ = 0;
BaseContainer* GuiFoundation::PCurrentDisplayContainer_ = NULL;

#if defined( SIGMA_GUIPC_CPU )
Boolean	GuiFoundation::GuiFoundationInitialized_ = FALSE;
#endif // SIGMA_GUIPC_CPU
//
// Debug members.
//
Uint  GuiFoundation::arrDebugFlags_[];
 
//@ Code...


//=====================================================================
//
// Public methods
//
//=====================================================================
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetUpperBaseContainer
//
//@ Interface-Description
//  Returns a reference to the Upper Screen base container.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Constructs the BaseContainer statically on first call and returns
//  its reference.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
BaseContainer &
GuiFoundation::GetUpperBaseContainer(void)
{
  // $[TI1]
  static DisplayContext & rDisplayContext =
		  VgaGraphicsDriver::GetUpperDisplayContext();
  static BaseContainer baseContainer(rDisplayContext);
  return baseContainer;
}
 
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetLowerBaseContainer
//
//@ Interface-Description
//  Returns a reference to the lower screen base container appropriate
//  to the ventilator GUI configuration (dual screen or single screen).
//---------------------------------------------------------------------
//@ Implementation-Description
//  Constructs the BaseContainer statically on first call and returns
//  its reference.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
BaseContainer &
GuiFoundation::GetLowerBaseContainer(void)
{
  // $[TI1]
  static DisplayContext & rDisplayContext =
		  VgaGraphicsDriver::GetLowerDisplayContext();
  static BaseContainer baseContainer(rDisplayContext);
  return baseContainer;
}
 
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetCurrentDisplayContainerPtr
//
//@ Interface-Description
//  Returns a pointer to the current displayed base container.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
BaseContainer *
GuiFoundation::GetCurrentDisplayContainerPtr(void)
{
  // $[TI1]
  return PCurrentDisplayContainer_;
}

#if defined( SIGMA_GUIPC_CPU )
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: IsInitialized
//
//@ Interface-Description
//  Returns the initialization status of GuiFoundation
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
Boolean
GuiFoundation::IsInitialized(void)
{
	return GuiFoundationInitialized_;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: InvalidateBaseContainers
//
//@ Interface-Description
//  Invalidate upper and lower base containers to redraw them.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
GuiFoundation::InvalidateBaseContainers(void)
{
	if ( GuiFoundation::IsInitialized() )
	{
		GetUpperBaseContainer().invalidate();
		GetLowerBaseContainer().invalidate();
	}
}
#endif // SIGMA_GUIPC_CPU

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Initialize
//
//@ Interface-Description
// Initialize the GUI-Foundations subsystem.
//---------------------------------------------------------------------
//@ Implementation-Description
// Runs placement new to construct the "global" lower and upper base containers
// into a pre-allocated, fixed-size memory pool.  Also, calls the Initialize()
// methods for GUI-Foundations classes as necessary.  Note that this is the
// only method that can instantiate BaseContainers, because it has the
// friendship of the BaseContainer class, and the BaseContainer constructor is
// private.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
GuiFoundation::Initialize(void)
{
    CALL_TRACE("GuiFoundation::Initialize(void)");

 	//										$[TI1]
    // Construct the Upper and Lower Base containers.
    //
	BaseContainer& rUpperBaseContainer = GetUpperBaseContainer();
	BaseContainer& rLowerBaseContainer = GetLowerBaseContainer();

	PCurrentDisplayContainer_ = &rLowerBaseContainer;
	PCurrentDisplayContainer_->getDisplayContext().showFrame();

	//
	// Initialize the Upper and Lower Base containers to completely
	// fill the upper and lower screens.
	//
	rLowerBaseContainer.setWidth(SCREEN_WIDTH_PIXELS);
	rLowerBaseContainer.setHeight(SCREEN_HEIGHT_PIXELS);
	
	rUpperBaseContainer.setWidth(SCREEN_WIDTH_PIXELS);
	rUpperBaseContainer.setHeight(SCREEN_HEIGHT_PIXELS);

	DisplayAccessNesting_ = 0;		// Initialize display access counter

	//
	// Initialize other classes in this subsystem as necessary.
	//
	TextFont::Initialize();
	Colors::Initialize();
	Image::Initialize();
	Touch::Initialize();
	KeyPanel::Initialize();

#if defined( SIGMA_GUIPC_CPU )
	GuiFoundationInitialized_ = TRUE;
#endif // SIGMA_GUIPC_CPU
}

#if defined(SIGMA_DEBUG)
//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  TurnAllDebugOn  [static]
//
// Interface-Description
//  Turn all of the the debug flags for this subsystem on.
//---------------------------------------------------------------------
// Implementation-Description                      
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  (GuiFoundation::IsDebugOn(...any class id...))
// End-Method
//=====================================================================
void
GuiFoundation::TurnAllDebugOn(void)
{
    CALL_TRACE("GuiFoundation::TurnAllDebugOn(void)");
 
    for (Uint32 idx = 0; idx < GuiFoundation::NUM_WORDS; idx++)
        GuiFoundation::arrDebugFlags_[idx] = ~(0u);
}

 
//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  TurnAllDebugOff  [static]
//
// Interface-Description
//  Turn all of the the debug flags for this subsystem off.
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  (!GuiFoundation::IsDebugOn(...any class id...))
// End-Method
//=====================================================================
void
GuiFoundation::TurnAllDebugOff(void)
{
    CALL_TRACE("GuiFoundation::TurnAllDebugOff(void)");
 
    for (Uint32 idx = 0; idx < GuiFoundation::NUM_WORDS; idx++)
        GuiFoundation::arrDebugFlags_[idx] = 0u;
}
 
#endif  // defined(SIGMA_DEBUG)


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//        [static, inline]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds its sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void          
GuiFoundation::SoftFault(const SoftFaultID  softFaultID,
					  	 const Uint32       lineNumber,
						 const char*        pFileName,
						 const char*        pPredicate)
{
	CALL_TRACE("GuiFoundation::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
			   FaultHandler::SoftFault(softFaultID, GUI_FOUNDATIONS, GUI_FOUNDATION_CLASS,
									   lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)
