#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: SafeStatePhase - Implements safe state phase.
//---------------------------------------------------------------------
//@ Interface-Description
//		This class is derived from the base class BreathPhase.  Virtual
//		methods are implemented to initialize the SafeStatePhase, to perform
//		SafeState activities each BD cycle during safe state, and to allow for
//		internal clean up and graceful termination of the breath phase.
//---------------------------------------------------------------------
//@ Rationale
//		This class implements the algorithms for safe state.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The Psols, exhalation valve, and the damping gain are applied
//		zero command and the safety valve is commanded open.
//---------------------------------------------------------------------
//@ Fault-Handling
//		N/A
//---------------------------------------------------------------------
//@ Restrictions
//		none
//---------------------------------------------------------------------
//@ Invariants
//		none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/SafeStatePhase.ccv   25.0.4.0   19 Nov 2013 14:00:06   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 004  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 003 By: syw   Date: 16-Aug-1996   DR Number: DCS 1076
//  	Project:  Sigma (R8027)
//		Description:
//			Added method updateFlowControllerFlags.  Use FlowController::updatePsol( 0)
//			to close psols instead of Psol::updatePsol( 0).  Call
//			FlowController::newBreath().
//
//  Revision: 002 By: syw    Date: 21-Feb-1996   DR Number: DCS 674
//  	Project:  Sigma (R8027)
//		Description:
//			Removed #ifndef around #include.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number:
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "SafeStatePhase.hh"

#include "ValveRefs.hh"
#include "ControllersRefs.hh"

//@ Usage-Classes

#include "Solenoid.hh"
#include "ExhalationValve.hh"
#include "Psol.hh"
#include "BD_IO_Devices.hh"
#include "FlowController.hh"

//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SafeStatePhase()
//
//@ Interface-Description
//		Constructor.  This method has no arguments and returns nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The base class constructor is called with the phase type argument.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

SafeStatePhase::SafeStatePhase(void)
 : BreathPhase(BreathPhaseType::NON_BREATHING)	// $[TI1]
{
	CALL_TRACE("SafeStatePhase::SafeStatePhase(void)") ;

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~SafeStatePhase( )
//
//@ Interface-Description
//		Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//		none.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

SafeStatePhase::~SafeStatePhase(void)
{
	CALL_TRACE("SafeStatePhase::~SafeStatePhase(void)") ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newCycle()
//
//@ Interface-Description
//		This method has no arguments and has no return value.  This method is
//		called every BD cycle during the safe state phase.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The flow controller flags are updated.  The exhalation valve and damping
//		gain are set to zero in	newBreath().  This method must open the safety
//		valve and command the psols to zero every cycle as a safety feature.
// 		$[04222] $[04223]
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
SafeStatePhase::newCycle( void)
{
	CALL_TRACE("SafeStatePhase::newCycle( void)") ;

	// $[TI1]
	updateFlowControllerFlags_() ;

    RAirFlowController.updatePsol( 0.0F) ;
   	RO2FlowController.updatePsol( 0.0F) ;

	RSafetyValve.open() ;

	// no breath activity
    elapsedTimeMs_ += CYCLE_TIME_MS ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newBreath()
//
//@ Interface-Description
//		This method has no arguments and has no return value.  This method is
//		called at the beginning of safe state phase.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The exhalation valve and damping gain are commanded to zero.  The
//		safety valve is commanded open and the flow controllers are
//		initialized by their newBreath() method.
// 		$[04204] $[04222] $[04272]
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
SafeStatePhase::newBreath( void)
{
	CALL_TRACE("SafeStatePhase::newBreath( void)") ;
	
	// $[TI1]
	RExhalationValve.deenergize() ;
  	RSafetyValve.open() ;
    RAirFlowController.newBreath() ;
   	RO2FlowController.newBreath() ;

    elapsedTimeMs_ = 0 ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
SafeStatePhase::SoftFault( const SoftFaultID  softFaultID,
                   		   const Uint32       lineNumber,
						   const char*        pFileName,
						   const char*        pPredicate)
{
  	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)") ;
  	FaultHandler::SoftFault( softFaultID, BREATH_DELIVERY, SAFESTATEPHASE,
                          	 lineNumber, pFileName, pPredicate) ;
}

//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateFlowControllerFlags_
//
//@ Interface-Description
//		This method has no arguments and has no return value.  This method is
//		called to set the controller shutdown flag of the flow controller to
//		TRUE.  This method overloads the base class method.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Set flags to true.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
SafeStatePhase::updateFlowControllerFlags_( void)
{
	CALL_TRACE("SafeStatePhase::updateFlowControllerFlags_( void)") ;
	
	// $[TI1]
	RO2FlowController.setControllerShutdown( TRUE) ;
	RAirFlowController.setControllerShutdown( TRUE) ;
}


//=====================================================================
//
//  Private Methods...
//
//=====================================================================
