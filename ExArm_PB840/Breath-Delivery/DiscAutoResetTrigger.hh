#ifndef DiscAutoResetTrigger_HH
#define DiscAutoResetTrigger_HH
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: DiscAutoResetTrigger - Monitors the system for the specific 
//	conditions that cause auto reset of idle mode to be detected.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/DiscAutoResetTrigger.hhv   25.0.4.0   19 Nov 2013 13:59:44   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 005  By:  iv    Date:  18-Jul-1997   DR Number: DCS 2298
//  	Project:  Sigma (R8027)
//		Description:
//             Implemented a running average of MAX_FLOW_SAMPLES
//             samples. 
//
//  Revision: 004  By:  sp    Date:  23-Sept-1996    DR Number:600
//       Project:  Sigma (R8027)
//       Description:
//             Remove unit test method.
//
//  Revision: 003  By:  sp    Date:  6-May-1996    DR Number:927
//       Project:  Sigma (R8027)
//       Description:
//             update to the latest disconnect algorithm.
//
//  Revision: 002  By:  sp    Date:  4-Mar-1996    DR Number:674
//       Project:  Sigma (R8027)
//       Description:
//             Remove #ifndef.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================
#  include "Sigma.hh"
#  include "Breath_Delivery.hh"
#  include "ModeTrigger.hh"

//@ Usage-Classes
//@ End-Usage

//@ Constant: MAX_FLOW_SAMPLES
// number of flow samples to average
static const Int8 MAX_FLOW_SAMPLES = 10 ;

class DiscAutoResetTrigger : public ModeTrigger
{
  public:
    DiscAutoResetTrigger(void);
    virtual ~DiscAutoResetTrigger(void);

    virtual void enable(void);

    static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL,
			  const char*       pPredicate = NULL);

  protected:
    virtual Boolean triggerCondition_(void);

  private:
    DiscAutoResetTrigger(const DiscAutoResetTrigger&);    // Declared but not implemented
    void operator=(const DiscAutoResetTrigger&);   // Declared but not implemented

    //@ Data-Member:  flowCount_
    // Used to count the duration for which the flow condition is true.
    Int32  flowCount_;

    //@ Data-Member:  minPressureCount_
    //  Used to count number of cycles for which 
    //  the MIN_PRESSURE_THRESHOLD condition is true.
    Int32  minPressureCount_;

    //@ Data-Member:  maxPressureCount_
    //  Used to count number of cycles for which the 
    //  MAX_PRESSURE_THRESHOLD condition is true.
    Int32  maxPressureCount_;

    //@ Data-Member:  occlusionCount_
    //  Used to count the duration for which the occlusion condition is true.
    Int32 occlusionCount_;

    //@ Data-Member:  flowIndex_
    //  Used as an index to the flow measurements buffer
    Int8 flowIndex_;

    //@ Data-Member:  idleFlowSum_
    //  The sum of all idle flow measurements in the flow buffer.
    Real32 idleFlowSum_;

    //@ Data-Member:  exhFlowSum_
    //  The sum of all exhalation flow measurements in the flow buffer.
    Real32 exhFlowSum_;

    //@ Data-Member:  idleFlowSample_[]
    //  An array to buffer all idle flow measurements
    Real32 idleFlowSample_[MAX_FLOW_SAMPLES];

    //@ Data-Member:  exhFlowSample_[]
    //  An array to buffer all exhalation flow measurements
    Real32 exhFlowSample_[MAX_FLOW_SAMPLES];

};


#endif // DiscAutoResetTrigger_HH 
