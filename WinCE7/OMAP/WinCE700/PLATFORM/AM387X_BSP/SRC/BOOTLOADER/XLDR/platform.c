// All rights reserved ADENEO EMBEDDED 2010
// Copyright (c) 2007, 2008 BSQUARE Corporation. All rights reserved.

/*
================================================================================
*             Texas Instruments OMAP(TM) Platform Software
* (c) Copyright Texas Instruments, Incorporated. All Rights Reserved.
*
* Use of this software is controlled by the terms and conditions found
* in the license agreement under which this software has been supplied.
*
================================================================================
*/
//
//  File:  platform.c
//
//  This file contains X-Loader configuration code for AM387X
//
#include "bsp.h"
#include "bsp_cfg.h"
#include "bsp_def.h"

#include "sdk_i2c.h"
#include "sdk_padcfg.h"
#include "oal_padcfg.h"
#include "oal_i2c.h"

#include "bsp_padcfg.h"
#include "omap_cpuver.h"

#include "am387x_config.h"
#include "am387x_base_regs.h"
#include "omap_gpmc_regs.h"
#include "am387x_clocks.h"
#include "am387x_prcm.h"
#include "am387x_dmm.h"

#if defined(FMD_ONENAND) && defined(FMD_NAND)
    #error FMD_ONENAND and FMD_NAND cannot both be defined.
#endif

#define __raw_writel(v, a)  OUTREG32((a), (v))
#define __raw_readl(a)      INREG32((a))

#define IN_DDR                 0

/* Clocks are derived from ADPLLJ */
#define ADPLLJ_CLKCTRL			0x4
#define ADPLLJ_TENABLE			0x8
#define ADPLLJ_TENABLEDIV		0xC
#define ADPLLJ_M2NDIV			0x10
#define ADPLLJ_MN2DIV			0x14
#define ADPLLJ_STATUS			0x24


#define MODENA_PLL_BASE			(AM387X_PLLSS_REGS_PA + 0x048)
#define DSP_PLL_BASE			(AM387X_PLLSS_REGS_PA + 0x080)
#define SGX_PLL_BASE			(AM387X_PLLSS_REGS_PA + 0x0B0)
#define IVA_PLL_BASE			(AM387X_PLLSS_REGS_PA + 0x0E0)
#define L3_PLL_BASE				(AM387X_PLLSS_REGS_PA + 0x110)
#define ISS_PLL_BASE			(AM387X_PLLSS_REGS_PA + 0x140)
#define DSS_PLL_BASE			(AM387X_PLLSS_REGS_PA + 0x170)
#define VIDEO_0_PLL_BASE		(AM387X_PLLSS_REGS_PA + 0x1A0)
#define VIDEO_1_PLL_BASE		(AM387X_PLLSS_REGS_PA + 0x1D0)
#define HDMI_PLL_BASE			(AM387X_PLLSS_REGS_PA + 0x200)
#define AUDIO_PLL_BASE			(AM387X_PLLSS_REGS_PA + 0x230)
#define USB_PLL_BASE			(AM387X_PLLSS_REGS_PA + 0x260)
#define DDR_PLL_BASE			(AM387X_PLLSS_REGS_PA + 0x290)


#define SATA_PLLCFG0			(AM387X_L4_CNTRL_MODULE_PA + 0x720)
#define SATA_PLLCFG1			(AM387X_L4_CNTRL_MODULE_PA + 0x724)
#define SATA_PLLCFG2			(AM387X_L4_CNTRL_MODULE_PA + 0x728)
#define SATA_PLLCFG3			(AM387X_L4_CNTRL_MODULE_PA + 0x72C)
#define SATA_PLLCFG4			(AM387X_L4_CNTRL_MODULE_PA + 0x730)
#define SATA_PLLSTATUS			(AM387X_L4_CNTRL_MODULE_PA + 0x734)
#define SATA_RXSTATUS			(AM387X_L4_CNTRL_MODULE_PA + 0x738)
#define SATA_TXSTATUS			(AM387X_L4_CNTRL_MODULE_PA + 0x73C)



/* Put the pll config values over here */
#define THIS_AUDIO_N		19
#define AUDIO_M		500
#define AUDIO_M2	2
#define AUDIO_CLKCTRL	0x801

#define MODENA_N	0x1
#define MODENA_M	0x3C
#define MODENA_M2	1
#define MODENA_CLKCTRL	0x1

UINT32 pg_val_ti814x(UINT32 pg1_val, UINT32 pg2_val);
#define L3_N		19
#define L3_M		(pg_val_ti814x(880, 800))
#define L3_M2		4
#define L3_CLKCTRL	0x801

#define THIS_DDR_N	19
#define DDR_M		(pg_val_ti814x(666, 800))
#define DDR_M2		2
#define DDR_CLKCTRL	0x801

#define DSP_N		19
#define DSP_M		500
#define DSP_M2		1
#define DSP_CLKCTRL	0x801

#define DSS_N		19
//GEL: DSS_M==800
//Uboot: DSS_M=666
#define DSS_M		(pg_val_ti814x(800, 800))
#define DSS_M2		4
#define DSS_CLKCTRL	0x801

// From Gel +++FIXME: may need PG2.1 update
#define SGX_N		19
#define SGX_M		800
#define SGX_M2		4
#define SGX_CLKCTRL	0x801

#define IVA_N		19
//GEL: IVA_M==532
#define IVA_M		(pg_val_ti814x(850 /*612*/, 612))
#define IVA_M2		2
#define IVA_CLKCTRL	0x801

#define ISS_N		19
#define ISS_M		800
#define ISS_M2		(pg_val_ti814x(2, 2))
#define ISS_CLKCTRL	0x801

#define USB_N		19
#define USB_M		960
#define USB_M2		(pg_val_ti814x(1, 5))
#define USB_CLKCTRL	0x200a0801

// From Gel. Video 0 PLL (54MHz)  +++FIXME: may need PG2.1 update
#define VIDEO_0_N		19
#define VIDEO_0_M		540
#define VIDEO_0_M2		10
#define VIDEO_0_CLKCTRL	0x801

// From Gel. Video 1 PLL (148.5 MHz)  +++FIXME: may need PG2.1 update
#define VIDEO_1_N		19
#define VIDEO_1_M		594
#define VIDEO_1_M2		4
#define VIDEO_1_CLKCTRL	0x801

#define AM387X_GPTIMER_CLKSRC			(AM387X_PLLSS_REGS_PA + 0x2E0)

#define DDR0_IO_CTRL			0x48140E04
#define DDR1_IO_CTRL			0x48140E08

#define VTP0_CTRL_REG	   		0x48140E0C
#define VTP1_CTRL_REG			0x48140E10

#define N_PINS				    (271) /* PIN1=800, PIN 271=800+270*4=C38) */

/* WDT related */
/* TODO: Move to a new file */
#define WDT_BASE    AM387X_WDT0_REGS_PA
#define WDT_WDSC	(WDT_BASE + 0x010)
#define WDT_WDST	(WDT_BASE + 0x014)
#define WDT_WISR	(WDT_BASE + 0x018)
#define WDT_WIER	(WDT_BASE + 0x01C)
#define WDT_WWER	(WDT_BASE + 0x020)
#define WDT_WCLR	(WDT_BASE + 0x024)
#define WDT_WCRR	(WDT_BASE + 0x028)
#define WDT_WLDR	(WDT_BASE + 0x02C)
#define WDT_WTGR	(WDT_BASE + 0x030)
#define WDT_WWPS	(WDT_BASE + 0x034)
#define WDT_WDLY	(WDT_BASE + 0x044)
#define WDT_WSPR	(WDT_BASE + 0x048)
#define WDT_WIRQEOI	(WDT_BASE + 0x050)
#define WDT_WIRQSTATRAW	(WDT_BASE + 0x054)
#define WDT_WIRQSTAT	(WDT_BASE + 0x058)
#define WDT_WIRQENSET	(WDT_BASE + 0x05C)
#define WDT_WIRQENCLR	(WDT_BASE + 0x060)

#define WDT_UNFREEZE	(AM387X_L4_CNTRL_MODULE_PA + 0x100)

static const UINT32 gpmc_m_nand[GPMC_MAX_REG] = {
	M_NAND_GPMC_CONFIG1,
	M_NAND_GPMC_CONFIG2,
	M_NAND_GPMC_CONFIG3,
	M_NAND_GPMC_CONFIG4,
	M_NAND_GPMC_CONFIG5,
	M_NAND_GPMC_CONFIG6, 0
};


//------------------------------------------------------------------------------
//  Function Prototypes
//

static VOID Unlock_pll_control_mmr();
static VOID audio_pll_config();
static VOID Sata_pll_config();
static VOID Modena_pll_config();
static VOID L3_pll_config();
static VOID Ddr_pll_config();
static VOID Dsp_pll_config();
static VOID Dss_pll_config();
static VOID Iva_pll_config();
static VOID Iss_pll_config();
static VOID Usb_pll_config();
static VOID Per_clocks_enable();

static VOID PrcmInit(UINT32 in_ddr);
static VOID Enable_gpmc_cs_config(const UINT32 *gpmc_config, OMAP_GPMC_REGS* pGpmc, UINT32 cs, UINT32 base, UINT32 size);

static VOID Gpmc_init();

//-----------------------------------------
//
// Function: OALGetTickCount
//
// Stub routine
//
UINT32 OALGetTickCount()
{
    return 1;
}

static VOID xdelay(UINT32 loops)
{
    OALStall(loops);
    return;
}

/* CPU Revision for TI814X PG2.1 is 0x3 and PG1.0 is0 */
enum cpu_rev {
	PG1_0 = 0,
	PG2_1,
	PG_END
};

#define DEVICE_ID  (AM387X_L4_CNTRL_MODULE_PA + 0x0600)

#define	   DDR0_PHY_BASE_ADDR	0x47C0C400
#define	   DDR1_PHY_BASE_ADDR	0x47C0C800

/* DDR Phy MMRs OFFSETs */
#define CMD0_REG_PHY_CTRL_SLAVE_RATIO_0		0x01C
#define CMD0_REG_PHY_DLL_LOCK_DIFF_0		0x028
#define CMD0_REG_PHY_INVERT_CLKOUT_0		0x02C
#define CMD1_REG_PHY_CTRL_SLAVE_RATIO_0		0x050
#define CMD1_REG_PHY_DLL_LOCK_DIFF_0		0x05C
#define CMD1_REG_PHY_INVERT_CLKOUT_0		0x060
#define CMD2_REG_PHY_CTRL_SLAVE_RATIO_0		0x084
#define CMD2_REG_PHY_DLL_LOCK_DIFF_0		0x090
#define CMD2_REG_PHY_INVERT_CLKOUT_0		0x094

/* DDR0 Phy MMRs */
#define CMD0_REG_PHY0_CTRL_SLAVE_RATIO_0	(0x01C + DDR0_PHY_BASE_ADDR)
#define CMD0_REG_PHY0_DLL_LOCK_DIFF_0		(0x028 + DDR0_PHY_BASE_ADDR)
#define CMD0_REG_PHY0_INVERT_CLKOUT_0		(0x02C + DDR0_PHY_BASE_ADDR)
#define CMD1_REG_PHY0_CTRL_SLAVE_RATIO_0	(0x050 + DDR0_PHY_BASE_ADDR)
#define CMD1_REG_PHY0_DLL_LOCK_DIFF_0		(0x05C + DDR0_PHY_BASE_ADDR)
#define CMD1_REG_PHY0_INVERT_CLKOUT_0		(0x060 + DDR0_PHY_BASE_ADDR)
#define CMD2_REG_PHY0_CTRL_SLAVE_RATIO_0	(0x084 + DDR0_PHY_BASE_ADDR)
#define CMD2_REG_PHY0_DLL_LOCK_DIFF_0		(0x090 + DDR0_PHY_BASE_ADDR)
#define CMD2_REG_PHY0_INVERT_CLKOUT_0		(0x094 + DDR0_PHY_BASE_ADDR)

#define DATA0_REG_PHY0_RD_DQS_SLAVE_RATIO_0	(0x0C8 + DDR0_PHY_BASE_ADDR)
#define DATA0_REG_PHY0_WR_DQS_SLAVE_RATIO_0	(0x0DC + DDR0_PHY_BASE_ADDR)
#define DATA0_REG_PHY0_WRLVL_INIT_RATIO_0	(0x0F0 + DDR0_PHY_BASE_ADDR)
#define DATA0_REG_PHY0_WRLVL_INIT_MODE_0	(0x0F8 + DDR0_PHY_BASE_ADDR)
#define DATA0_REG_PHY0_GATELVL_INIT_RATIO_0	(0x0FC + DDR0_PHY_BASE_ADDR)
#define DATA0_REG_PHY0_GATELVL_INIT_MODE_0	(0x104 + DDR0_PHY_BASE_ADDR)
#define DATA0_REG_PHY0_FIFO_WE_SLAVE_RATIO_0	(0x108 + DDR0_PHY_BASE_ADDR)
#define DATA0_REG_PHY0_WR_DATA_SLAVE_RATIO_0	(0x120 + DDR0_PHY_BASE_ADDR)
#define DATA0_REG_PHY0_USE_RANK0_DELAYS		(0x134 + DDR0_PHY_BASE_ADDR)
#define DATA0_REG_PHY0_DLL_LOCK_DIFF_0		(0x138 + DDR0_PHY_BASE_ADDR)

#define DATA1_REG_PHY0_RD_DQS_SLAVE_RATIO_0	(0x16C + DDR0_PHY_BASE_ADDR)
#define DATA1_REG_PHY0_WR_DQS_SLAVE_RATIO_0	(0x180 + DDR0_PHY_BASE_ADDR)
#define DATA1_REG_PHY0_WRLVL_INIT_RATIO_0	(0x194 + DDR0_PHY_BASE_ADDR)
#define DATA1_REG_PHY0_WRLVL_INIT_MODE_0	(0x19C + DDR0_PHY_BASE_ADDR)
#define DATA1_REG_PHY0_GATELVL_INIT_RATIO_0	(0x1A0 + DDR0_PHY_BASE_ADDR)
#define DATA1_REG_PHY0_GATELVL_INIT_MODE_0	(0x1A8 + DDR0_PHY_BASE_ADDR)
#define DATA1_REG_PHY0_FIFO_WE_SLAVE_RATIO_0	(0x1AC + DDR0_PHY_BASE_ADDR)
#define DATA1_REG_PHY0_WR_DATA_SLAVE_RATIO_0	(0x1C4 + DDR0_PHY_BASE_ADDR)
#define DATA1_REG_PHY0_USE_RANK0_DELAYS		(0x1D8 + DDR0_PHY_BASE_ADDR)
#define DATA1_REG_PHY0_DLL_LOCK_DIFF_0		(0x1DC + DDR0_PHY_BASE_ADDR)

#define DATA2_REG_PHY0_RD_DQS_SLAVE_RATIO_0	(0x210 + DDR0_PHY_BASE_ADDR)
#define DATA2_REG_PHY0_WR_DQS_SLAVE_RATIO_0	(0x224 + DDR0_PHY_BASE_ADDR)
#define DATA2_REG_PHY0_WRLVL_INIT_RATIO_0	(0x238 + DDR0_PHY_BASE_ADDR)
#define DATA2_REG_PHY0_WRLVL_INIT_MODE_0	(0x240 + DDR0_PHY_BASE_ADDR)
#define DATA2_REG_PHY0_GATELVL_INIT_RATIO_0	(0x244 + DDR0_PHY_BASE_ADDR)
#define DATA2_REG_PHY0_GATELVL_INIT_MODE_0	(0x24C + DDR0_PHY_BASE_ADDR)
#define DATA2_REG_PHY0_FIFO_WE_SLAVE_RATIO_0	(0x250 + DDR0_PHY_BASE_ADDR)
#define DATA2_REG_PHY0_WR_DATA_SLAVE_RATIO_0	(0x268 + DDR0_PHY_BASE_ADDR)
#define DATA2_REG_PHY0_USE_RANK0_DELAYS		(0x27C + DDR0_PHY_BASE_ADDR)
#define DATA2_REG_PHY0_DLL_LOCK_DIFF_0		(0x280 + DDR0_PHY_BASE_ADDR)

#define DATA3_REG_PHY0_RD_DQS_SLAVE_RATIO_0	(0x2B4 + DDR0_PHY_BASE_ADDR)
#define DATA3_REG_PHY0_WR_DQS_SLAVE_RATIO_0	(0x2C8 + DDR0_PHY_BASE_ADDR)
#define DATA3_REG_PHY0_WRLVL_INIT_RATIO_0	(0x2DC + DDR0_PHY_BASE_ADDR)
#define DATA3_REG_PHY0_WRLVL_INIT_MODE_0	(0x2E4 + DDR0_PHY_BASE_ADDR)
#define DATA3_REG_PHY0_GATELVL_INIT_RATIO_0	(0x2E8 + DDR0_PHY_BASE_ADDR)
#define DATA3_REG_PHY0_GATELVL_INIT_MODE_0	(0x2F0 + DDR0_PHY_BASE_ADDR)
#define DATA3_REG_PHY0_FIFO_WE_SLAVE_RATIO_0	(0x2F4 + DDR0_PHY_BASE_ADDR)
#define DATA3_REG_PHY0_WR_DATA_SLAVE_RATIO_0	(0x30C + DDR0_PHY_BASE_ADDR)
#define DATA3_REG_PHY0_USE_RANK0_DELAYS		(0x320 + DDR0_PHY_BASE_ADDR)
#define DATA3_REG_PHY0_DLL_LOCK_DIFF_0		(0x324 + DDR0_PHY_BASE_ADDR)

/* DDR1 Phy MMRs */
#define	CMD0_REG_PHY1_CTRL_SLAVE_RATIO_0	(0x01C + DDR1_PHY_BASE_ADDR)
#define	CMD0_REG_PHY1_DLL_LOCK_DIFF_0		(0x028 + DDR1_PHY_BASE_ADDR)
#define	CMD0_REG_PHY1_INVERT_CLKOUT_0		(0x02C + DDR1_PHY_BASE_ADDR)
#define	CMD1_REG_PHY1_CTRL_SLAVE_RATIO_0	(0x050 + DDR1_PHY_BASE_ADDR)
#define	CMD1_REG_PHY1_DLL_LOCK_DIFF_0		(0x05C + DDR1_PHY_BASE_ADDR)
#define	CMD1_REG_PHY1_INVERT_CLKOUT_0		(0x060 + DDR1_PHY_BASE_ADDR)
#define	CMD2_REG_PHY1_CTRL_SLAVE_RATIO_0	(0x084 + DDR1_PHY_BASE_ADDR)
#define	CMD2_REG_PHY1_DLL_LOCK_DIFF_0		(0x090 + DDR1_PHY_BASE_ADDR)
#define	CMD2_REG_PHY1_INVERT_CLKOUT_0		(0x094 + DDR1_PHY_BASE_ADDR)

#define	DATA0_REG_PHY1_RD_DQS_SLAVE_RATIO_0	(0x0C8 + DDR1_PHY_BASE_ADDR)
#define	DATA0_REG_PHY1_WR_DQS_SLAVE_RATIO_0	(0x0DC + DDR1_PHY_BASE_ADDR)
#define	DATA0_REG_PHY1_WRLVL_INIT_RATIO_0	(0x0F0 + DDR1_PHY_BASE_ADDR)
#define	DATA0_REG_PHY1_WRLVL_INIT_MODE_0	(0x0F8 + DDR1_PHY_BASE_ADDR)
#define	DATA0_REG_PHY1_GATELVL_INIT_RATIO_0	(0x0FC + DDR1_PHY_BASE_ADDR)
#define	DATA0_REG_PHY1_GATELVL_INIT_MODE_0	(0x104 + DDR1_PHY_BASE_ADDR)
#define	DATA0_REG_PHY1_FIFO_WE_SLAVE_RATIO_0	(0x108 + DDR1_PHY_BASE_ADDR)
#define	DATA0_REG_PHY1_WR_DATA_SLAVE_RATIO_0	(0x120 + DDR1_PHY_BASE_ADDR)
#define	DATA0_REG_PHY1_USE_RANK0_DELAYS		(0x134 + DDR1_PHY_BASE_ADDR)
#define	DATA0_REG_PHY1_DLL_LOCK_DIFF_0		(0x138 + DDR1_PHY_BASE_ADDR)

#define	DATA1_REG_PHY1_RD_DQS_SLAVE_RATIO_0	(0x16C + DDR1_PHY_BASE_ADDR)
#define	DATA1_REG_PHY1_WR_DQS_SLAVE_RATIO_0	(0x180 + DDR1_PHY_BASE_ADDR)
#define	DATA1_REG_PHY1_WRLVL_INIT_RATIO_0	(0x194 + DDR1_PHY_BASE_ADDR)
#define	DATA1_REG_PHY1_WRLVL_INIT_MODE_0	(0x19C + DDR1_PHY_BASE_ADDR)
#define	DATA1_REG_PHY1_GATELVL_INIT_RATIO_0	(0x1A0 + DDR1_PHY_BASE_ADDR)
#define	DATA1_REG_PHY1_GATELVL_INIT_MODE_0	(0x1A8 + DDR1_PHY_BASE_ADDR)
#define	DATA1_REG_PHY1_FIFO_WE_SLAVE_RATIO_0	(0x1AC + DDR1_PHY_BASE_ADDR)
#define	DATA1_REG_PHY1_WR_DATA_SLAVE_RATIO_0	(0x1C4 + DDR1_PHY_BASE_ADDR)
#define	DATA1_REG_PHY1_USE_RANK0_DELAYS		(0x1D8 + DDR1_PHY_BASE_ADDR)
#define	DATA1_REG_PHY1_DLL_LOCK_DIFF_0		(0x1DC + DDR1_PHY_BASE_ADDR)

#define	DATA2_REG_PHY1_RD_DQS_SLAVE_RATIO_0	(0x210 + DDR1_PHY_BASE_ADDR)
#define	DATA2_REG_PHY1_WR_DQS_SLAVE_RATIO_0	(0x224 + DDR1_PHY_BASE_ADDR)
#define	DATA2_REG_PHY1_WRLVL_INIT_RATIO_0	(0x238 + DDR1_PHY_BASE_ADDR)
#define	DATA2_REG_PHY1_WRLVL_INIT_MODE_0	(0x240 + DDR1_PHY_BASE_ADDR)
#define	DATA2_REG_PHY1_GATELVL_INIT_RATIO_0	(0x244 + DDR1_PHY_BASE_ADDR)
#define	DATA2_REG_PHY1_GATELVL_INIT_MODE_0	(0x24C + DDR1_PHY_BASE_ADDR)
#define	DATA2_REG_PHY1_FIFO_WE_SLAVE_RATIO_0	(0x250 + DDR1_PHY_BASE_ADDR)
#define	DATA2_REG_PHY1_WR_DATA_SLAVE_RATIO_0	(0x268 + DDR1_PHY_BASE_ADDR)
#define	DATA2_REG_PHY1_USE_RANK0_DELAYS		(0x27C + DDR1_PHY_BASE_ADDR)
#define	DATA2_REG_PHY1_DLL_LOCK_DIFF_0		(0x280 + DDR1_PHY_BASE_ADDR)

#define	DATA3_REG_PHY1_RD_DQS_SLAVE_RATIO_0	(0x2B4 + DDR1_PHY_BASE_ADDR)
#define	DATA3_REG_PHY1_WR_DQS_SLAVE_RATIO_0	(0x2C8 + DDR1_PHY_BASE_ADDR)
#define	DATA3_REG_PHY1_WRLVL_INIT_RATIO_0	(0x2DC + DDR1_PHY_BASE_ADDR)
#define	DATA3_REG_PHY1_WRLVL_INIT_MODE_0	(0x2E4 + DDR1_PHY_BASE_ADDR)
#define	DATA3_REG_PHY1_GATELVL_INIT_RATIO_0	(0x2E8 + DDR1_PHY_BASE_ADDR)
#define	DATA3_REG_PHY1_GATELVL_INIT_MODE_0	(0x2F0 + DDR1_PHY_BASE_ADDR)
#define	DATA3_REG_PHY1_FIFO_WE_SLAVE_RATIO_0	(0x2F4 + DDR1_PHY_BASE_ADDR)
#define	DATA3_REG_PHY1_WR_DATA_SLAVE_RATIO_0	(0x30C + DDR1_PHY_BASE_ADDR)
#define	DATA3_REG_PHY1_USE_RANK0_DELAYS		(0x320 + DDR1_PHY_BASE_ADDR)
#define	DATA3_REG_PHY1_DLL_LOCK_DIFF_0		(0x324 + DDR1_PHY_BASE_ADDR)

#define DATA_MACRO_0			0
#define DATA_MACRO_1			1
#define DATA_MACRO_2			2
#define DATA_MACRO_3			3
#define DDR_PHY0			0
#define DDR_PHY1			1

/* Common DDR PHY parameters */
#define	PHY_INVERT_CLKOUT_DEFINE		0

#define	DDR3_PHY_INVERT_CLKOUT_OFF		0
#define DDR_EMIF_REF_TRIGGER			0x10000000

#define	PHY_REG_USE_RANK0_DELAY_DEFINE		0
#define	mDDR_PHY_REG_USE_RANK0_DELAY_DEFINE	1
#define	PHY_DLL_LOCK_DIFF_DEFINE		0x4
#define	PHY_CMD0_DLL_LOCK_DIFF_DEFINE		0x4

#define	PHY_GATELVL_INIT_CS0_DEFINE		0x0
#define	PHY_WRLVL_INIT_CS0_DEFINE		0x0

#define	PHY_GATELVL_INIT_CS1_DEFINE		0x0
#define	PHY_WRLVL_INIT_CS1_DEFINE		0x0
#define	PHY_CTRL_SLAVE_RATIO_CS1_DEFINE		0x80

/* TI814X DDR2 PHY CFG parameters */
#define	DDR2_PHY_RD_DQS_CS0_DEFINE		0x35
#define	DDR2_PHY_WR_DQS_CS0_DEFINE		0x20

#define	DDR2_PHY_RD_DQS_GATE_CS0_DEFINE		0x90

#define	DDR2_PHY_WR_DATA_CS0_DEFINE		0x50
#define	DDR2_PHY_CTRL_SLAVE_RATIO_CS0_DEFINE	0x80

/* TI814X DDR3 PHY CFG parameters */
#define DDR3_PHY_RD_DQS_CS0_DEFINE		0x30
#define DDR3_PHY_WR_DQS_CS0_DEFINE		0x21

#define DDR3_PHY_RD_DQS_GATE_CS0_DEFINE		0xC0

#define DDR3_PHY_WR_DATA_CS0_DEFINE		0x44
#define DDR3_PHY_CTRL_SLAVE_RATIO_CS0_DEFINE	0x80

/* DDR0/1 IO CTRL parameters */
#define DDR0_IO_CTRL_DEFINE		0x00030303
#define DDR1_IO_CTRL_DEFINE		0x00030303

/* Initially set a large DDR refresh period */
#define DDR_EMIF_REF_CTRL		0x00004000

/* TI814X DDR2 EMIF CFG Registers values 333MHz*/
#define DDR2_EMIF_READ_LATENCY		0x07
#define DDR2_EMIF_TIM1			0x0AAAF552
#define DDR2_EMIF_TIM2			0x043631D2
#define DDR2_EMIF_TIM3			0x00000327
#define DDR2_EMIF_REF_CTRL		0x10000C30
#define DDR2_EMIF_SDRAM_CONFIG		0x40801AB2
#define DDR2_EMIF_SDRAM_ZQCR		0x50074BE1

/* TI814X DDR3 EMIF CFG Registers values 400MHz */
#define DDR3_EMIF_READ_LATENCY		0x00173209
#define DDR3_EMIF_TIM1			0x0AAAD4DB
#define DDR3_EMIF_TIM2			0x682F7FDA
#define DDR3_EMIF_TIM3			0x501F82BF
#define DDR3_EMIF_REF_CTRL		0x00000C30
#define DDR3_EMIF_SDRAM_CONFIG		0x61C011B2
#define DDR3_EMIF_SDRAM_ZQCR		0x50074BE1

/*
 * TI814X PG1.0 DMM LISA MAPPING
 * Two 256MB sections with 128-byte interleaved(hole in b/w)
 */
#define PG1_0_DMM_LISA_MAP__0		0x0
#define PG1_0_DMM_LISA_MAP__1		0x0
#define PG1_0_DMM_LISA_MAP__2		0x80440300
#define PG1_0_DMM_LISA_MAP__3		0xC0440300

/*
 * TI814X PG2.1 DMM LISA MAPPING
 * 1G contiguous section with 128-byte interleaving
 */
#define PG2_1_DMM_LISA_MAP__0		0x0
#define PG2_1_DMM_LISA_MAP__1		0x0
#define PG2_1_DMM_LISA_MAP__2		0x0
#define PG2_1_DMM_LISA_MAP__3		0x80640300


static void cmd_macro_config(
    UINT32 ddr_phy,
    UINT32 inv_clk_out, 
    UINT32 ctrl_slave_ratio_cs0, 
    UINT32 cmd_dll_lock_diff
)
{
	UINT32 ddr_phy_base = (DDR_PHY0 == ddr_phy) ?  
                              DDR0_PHY_BASE_ADDR : DDR1_PHY_BASE_ADDR;

	__raw_writel(inv_clk_out, ddr_phy_base + CMD1_REG_PHY_INVERT_CLKOUT_0);
	__raw_writel(inv_clk_out, ddr_phy_base + CMD0_REG_PHY_INVERT_CLKOUT_0);
	__raw_writel(inv_clk_out, ddr_phy_base + CMD2_REG_PHY_INVERT_CLKOUT_0);

	__raw_writel(((ctrl_slave_ratio_cs0 << 10) | ctrl_slave_ratio_cs0),
		ddr_phy_base + CMD0_REG_PHY_CTRL_SLAVE_RATIO_0);

	__raw_writel(((ctrl_slave_ratio_cs0 << 10) | ctrl_slave_ratio_cs0),
		ddr_phy_base + CMD1_REG_PHY_CTRL_SLAVE_RATIO_0);

	__raw_writel(((ctrl_slave_ratio_cs0 << 10) | ctrl_slave_ratio_cs0),
		 ddr_phy_base + CMD2_REG_PHY_CTRL_SLAVE_RATIO_0);

	__raw_writel(cmd_dll_lock_diff,
		 ddr_phy_base + CMD0_REG_PHY_DLL_LOCK_DIFF_0);

	__raw_writel(cmd_dll_lock_diff,
		 ddr_phy_base + CMD1_REG_PHY_DLL_LOCK_DIFF_0);

	__raw_writel(cmd_dll_lock_diff,
		 ddr_phy_base + CMD2_REG_PHY_DLL_LOCK_DIFF_0);
}

static void data_macro_config(
    UINT32 macro_num, 
    UINT32 phy_num, 
    UINT32 rd_dqs_cs0,
    UINT32 wr_dqs_cs0, 
    UINT32 fifo_we_cs0, 
    UINT32 wr_data_cs0
)
{
	/* 0xA4 is size of each data macro mmr region.
	 * phy1 is at offset 0x400 from phy0
	 */
	UINT32 base = (macro_num * 0xA4) + (phy_num * 0x400);

	OUTREG32((DATA0_REG_PHY0_RD_DQS_SLAVE_RATIO_0 + base), ((rd_dqs_cs0 << 10) | rd_dqs_cs0));
	OUTREG32((DATA0_REG_PHY0_WR_DQS_SLAVE_RATIO_0 + base), ((wr_dqs_cs0 << 10) | wr_dqs_cs0));

	OUTREG32((DATA0_REG_PHY0_WRLVL_INIT_RATIO_0 + base), 
                    ((PHY_WRLVL_INIT_CS1_DEFINE << 10) | PHY_WRLVL_INIT_CS0_DEFINE));

	OUTREG32((DATA0_REG_PHY0_GATELVL_INIT_RATIO_0 + base), 
                    ((PHY_GATELVL_INIT_CS1_DEFINE << 10) | PHY_GATELVL_INIT_CS0_DEFINE));

	OUTREG32((DATA0_REG_PHY0_FIFO_WE_SLAVE_RATIO_0 + base), ((fifo_we_cs0 << 10) | fifo_we_cs0));
	OUTREG32((DATA0_REG_PHY0_WR_DATA_SLAVE_RATIO_0 + base), ((wr_data_cs0 << 10) | wr_data_cs0));
	OUTREG32((DATA0_REG_PHY0_DLL_LOCK_DIFF_0 + base),       PHY_DLL_LOCK_DIFF_DEFINE);
}

/******************************************
 * get_cpu_rev(void) - extract rev info
 ******************************************/
UINT32 get_cpu_rev(UINT32 *pID)
{
	UINT32 id;
	UINT32 rev;

	id = INREG32(DEVICE_ID);

    if (pID)
        *pID = id;

	rev = (id >> 28) & 0xF;

	/* PG2.1 devices should read 0x3 as chip rev
	 * Some PG2.1 devices have 0xc as chip rev
	 */
	if (0x3 == rev || 0xc == rev)
		return PG2_1;
	else
		return PG1_0;
}

UINT32 pg_val_ti814x(UINT32 pg1_val, UINT32 pg2_val)
{
	/* PG2.1 devices should read 0x3 as chip rev */
	if (PG2_1 == get_cpu_rev(NULL))
		return pg2_val;
	else
		return pg1_val;
}

int is_ddr3(void)
{
	/*
	 * PG1.0 by default uses DDR2 &  PG2.1 uses DDR3
	 * To use PG2.1 and DDR2 enable #define CONFIG_TI814X_EVM_DDR2
	 * in "include/configs/ti8148_evm.h"
	 */
	if (PG2_1 == get_cpu_rev(NULL))
		#ifdef CONFIG_TI814X_EVM_DDR2
			return 0;
		#else
			return 1;
		#endif
	else
		return 0;
}


// Ref - GEL: EMIF4ClkEnable_Micron_DDR2()
static void config_ti814x_ddr(void)
{
	int macro, phy_num;

    AM387X_PRCM_REGS* pPrcmRegs = OALPAtoUA(AM387X_PRCM_REGS_PA);
    AM387X_DMM_REGS*  pDmmRegs  = OALPAtoUA(AM387X_DMM_REGS_PA);
    AM387X_DDR_EMIF_REGS *pDdrEmif0Regs = OALPAtoUA(AM387X_EMIF4_0_CFG_REGS_PA);
    AM387X_DDR_EMIF_REGS *pDdrEmif1Regs = OALPAtoUA(AM387X_EMIF4_1_CFG_REGS_PA);

    // Emif_PRCM_Clk_Enable() begin:
    /*Enable the Power Domain Transition of L3 Fast Domain Peripheral*/
	OUTREG32(&pPrcmRegs->CM_ALWON2_FW_CLKCTRL,        0x2); 

    /*Enable the Power Domain Transition of L3 Fast Domain Peripheral*/
	OUTREG32(&pPrcmRegs->CM_ALWON2_L3_FAST_CLKSTCTRL, 0x2);

	OUTREG32(&pPrcmRegs->CM_ALWON2_EMIF_0_CLKCTRL,    0x2);	/*Enable EMIF0 Clock*/
	OUTREG32(&pPrcmRegs->CM_ALWON2_EMIF_1_CLKCTRL,    0x2);	/*Enable EMIF1 Clock*/
	OUTREG32(&pPrcmRegs->CM_ALWON2_DMM_CLKCTRL,       0x2);

    /*Poll for L3_FAST_GCLK  & DDR_GCLK  are active*/
	while((INREG32(&pPrcmRegs->CM_ALWON2_L3_FAST_CLKSTCTRL) & 0x300) != 0x300);	
   /*Poll for Module is functional*/
	while((INREG32(&pPrcmRegs->CM_ALWON2_EMIF_0_CLKCTRL)) != 0x2);
	while((INREG32(&pPrcmRegs->CM_ALWON2_EMIF_1_CLKCTRL)) != 0x2);
	while((INREG32(&pPrcmRegs->CM_ALWON2_DMM_CLKCTRL))    != 0x2);
    // Emif_PRCM_Clk_Enable() end.

	if (is_ddr3()) {

		cmd_macro_config(DDR_PHY0, DDR3_PHY_INVERT_CLKOUT_OFF,
				DDR3_PHY_CTRL_SLAVE_RATIO_CS0_DEFINE,
				PHY_CMD0_DLL_LOCK_DIFF_DEFINE);

		cmd_macro_config(DDR_PHY1, DDR3_PHY_INVERT_CLKOUT_OFF,
				DDR3_PHY_CTRL_SLAVE_RATIO_CS0_DEFINE,
				PHY_CMD0_DLL_LOCK_DIFF_DEFINE);

		for (phy_num = 0; phy_num <= DDR_PHY1; phy_num++) {
			for (macro = 0; macro <= DATA_MACRO_3; macro++) {
				data_macro_config(macro, phy_num,
					DDR3_PHY_RD_DQS_CS0_DEFINE,
					DDR3_PHY_WR_DQS_CS0_DEFINE,
					DDR3_PHY_RD_DQS_GATE_CS0_DEFINE,
					DDR3_PHY_WR_DATA_CS0_DEFINE);
			}
		}

	} else {
		cmd_macro_config(DDR_PHY0, PHY_INVERT_CLKOUT_DEFINE,
				DDR2_PHY_CTRL_SLAVE_RATIO_CS0_DEFINE,
				PHY_CMD0_DLL_LOCK_DIFF_DEFINE);
		cmd_macro_config(DDR_PHY1, PHY_INVERT_CLKOUT_DEFINE,
				DDR2_PHY_CTRL_SLAVE_RATIO_CS0_DEFINE,
				PHY_CMD0_DLL_LOCK_DIFF_DEFINE);

		for (phy_num = 0; phy_num <= DDR_PHY1; phy_num++) {
			for (macro = 0; macro <= DATA_MACRO_3; macro++) {
				data_macro_config(macro, phy_num,
					DDR2_PHY_RD_DQS_CS0_DEFINE,
					DDR2_PHY_WR_DQS_CS0_DEFINE,
					DDR2_PHY_RD_DQS_GATE_CS0_DEFINE,
					DDR2_PHY_WR_DATA_CS0_DEFINE);
			}
		}
	}

	/* DDR IO CTRL config */
	__raw_writel(DDR0_IO_CTRL_DEFINE, DDR0_IO_CTRL);
	__raw_writel(DDR1_IO_CTRL_DEFINE, DDR1_IO_CTRL);

    // Vtp_Enable() begin:
    // Write 1 to ENABLE bit
	OUTREG32(VTP0_CTRL_REG, INREG32(VTP0_CTRL_REG) | 0x00000040);
	OUTREG32(VTP1_CTRL_REG, INREG32(VTP1_CTRL_REG) | 0x00000040);

	// Write 0 to CLRZ bit
	OUTREG32(VTP0_CTRL_REG, INREG32(VTP0_CTRL_REG) & 0xfffffffe);
	OUTREG32(VTP1_CTRL_REG, INREG32(VTP1_CTRL_REG) & 0xfffffffe);

	// Write 1 to CLRZ bit
	OUTREG32(VTP0_CTRL_REG, INREG32(VTP0_CTRL_REG) | 0x00000001);
	OUTREG32(VTP1_CTRL_REG, INREG32(VTP1_CTRL_REG) | 0x00000001);

	// Read VTP control registers & check READY bits
	while( (INREG32(VTP0_CTRL_REG) & 0x00000020) != 0x20);
	while( (INREG32(VTP1_CTRL_REG) & 0x00000020) != 0x20);
    // Vtp_Enable() end.

	if (PG1_0 == get_cpu_rev(NULL)) 
    {
		/*
		 * Program the PG1.0 DMM to Access EMIF0 and EMIF1
		 * Two 256MB sections with 128-byte interleaved (hole in b/w)
		 */
    	OUTREG32(&pDmmRegs->DMM_LISA_MAP__0, 0x0);
    	OUTREG32(&pDmmRegs->DMM_LISA_MAP__1, 0x0);
    	OUTREG32(&pDmmRegs->DMM_LISA_MAP__2, 0x80440300);
    	OUTREG32(&pDmmRegs->DMM_LISA_MAP__3, 0xC0440300);
    
    	while(INREG32(&pDmmRegs->DMM_LISA_MAP__0)!=0x0);
    	while(INREG32(&pDmmRegs->DMM_LISA_MAP__1)!=0x0);
    	while(INREG32(&pDmmRegs->DMM_LISA_MAP__2)!=0x80440300);
    	while(INREG32(&pDmmRegs->DMM_LISA_MAP__3)!=0xC0440300);
	} 
    else 
    {
		/*
		 * Program the PG2.1 DMM to Access EMIF0 and EMIF1
		 * 1G in 2 sections with no interleaving
		 */
    	OUTREG32(&pDmmRegs->DMM_LISA_MAP__0, 0x0);
    	OUTREG32(&pDmmRegs->DMM_LISA_MAP__1, 0x0);
    	OUTREG32(&pDmmRegs->DMM_LISA_MAP__2, 0x0);
    	OUTREG32(&pDmmRegs->DMM_LISA_MAP__3, 0x80640300);
    
    	while(INREG32(&pDmmRegs->DMM_LISA_MAP__0)!=0x0);
    	while(INREG32(&pDmmRegs->DMM_LISA_MAP__1)!=0x0);
    	while(INREG32(&pDmmRegs->DMM_LISA_MAP__2)!=0x0);
    	while(INREG32(&pDmmRegs->DMM_LISA_MAP__3)!=0x80640300);
	}

	OUTREG32(&pDmmRegs->DMM_PAT_VIEW_MAP_BASE, 0x80000000);

	if (!is_ddr3()) 
    {
		/*Program EMIF0 CFG Registers*/
		__raw_writel(DDR2_EMIF_READ_LATENCY, &pDdrEmif0Regs->DDR_PHY_CTRL_1);
		__raw_writel(DDR2_EMIF_READ_LATENCY, &pDdrEmif0Regs->DDR_PHY_CTRL_1_SHADOW);
		__raw_writel(DDR2_EMIF_TIM1, &pDdrEmif0Regs->SDRAM_TIM_1);
		__raw_writel(DDR2_EMIF_TIM1, &pDdrEmif0Regs->SDRAM_TIM_1_SHADOW);
		__raw_writel(DDR2_EMIF_TIM2, &pDdrEmif0Regs->SDRAM_TIM_2);
		__raw_writel(DDR2_EMIF_TIM2, &pDdrEmif0Regs->SDRAM_TIM_2_SHADOW);
		__raw_writel(DDR2_EMIF_TIM3, &pDdrEmif0Regs->SDRAM_TIM_3);
		__raw_writel(DDR2_EMIF_TIM3, &pDdrEmif0Regs->SDRAM_TIM_3_SHADOW);
		__raw_writel(DDR2_EMIF_SDRAM_CONFIG, &pDdrEmif0Regs->SDRAM_CONFIG);

		__raw_writel(DDR_EMIF_REF_CTRL | DDR_EMIF_REF_TRIGGER, &pDdrEmif0Regs->SDRAM_REF_CTRL);
		__raw_writel(DDR_EMIF_REF_CTRL, &pDdrEmif0Regs->SDRAM_REF_CTRL_SHADOW);
		__raw_writel(DDR2_EMIF_SDRAM_ZQCR, &pDdrEmif0Regs->SDRAM_ZQCR);
		__raw_writel(DDR_EMIF_REF_CTRL, &pDdrEmif0Regs->SDRAM_REF_CTRL);
		__raw_writel(DDR_EMIF_REF_CTRL, &pDdrEmif0Regs->SDRAM_REF_CTRL_SHADOW);

		__raw_writel(DDR2_EMIF_REF_CTRL, &pDdrEmif0Regs->SDRAM_REF_CTRL);
		__raw_writel(DDR2_EMIF_REF_CTRL, &pDdrEmif0Regs->SDRAM_REF_CTRL_SHADOW);

		/*Program EMIF1 CFG Registers*/
		__raw_writel(DDR2_EMIF_READ_LATENCY, &pDdrEmif1Regs->DDR_PHY_CTRL_1);
		__raw_writel(DDR2_EMIF_READ_LATENCY, &pDdrEmif1Regs->DDR_PHY_CTRL_1_SHADOW);
		__raw_writel(DDR2_EMIF_TIM1, &pDdrEmif1Regs->SDRAM_TIM_1);
		__raw_writel(DDR2_EMIF_TIM1, &pDdrEmif1Regs->SDRAM_TIM_1_SHADOW);
		__raw_writel(DDR2_EMIF_TIM2, &pDdrEmif1Regs->SDRAM_TIM_2);
		__raw_writel(DDR2_EMIF_TIM2, &pDdrEmif1Regs->SDRAM_TIM_2_SHADOW);
		__raw_writel(DDR2_EMIF_TIM3, &pDdrEmif1Regs->SDRAM_TIM_3);
		__raw_writel(DDR2_EMIF_TIM3, &pDdrEmif1Regs->SDRAM_TIM_3_SHADOW);
		__raw_writel(DDR2_EMIF_SDRAM_CONFIG, &pDdrEmif1Regs->SDRAM_CONFIG);

		__raw_writel(DDR_EMIF_REF_CTRL | DDR_EMIF_REF_TRIGGER, &pDdrEmif1Regs->SDRAM_REF_CTRL);
		__raw_writel(DDR_EMIF_REF_CTRL, &pDdrEmif1Regs->SDRAM_REF_CTRL_SHADOW);
		__raw_writel(DDR2_EMIF_SDRAM_ZQCR, &pDdrEmif1Regs->SDRAM_ZQCR);
		__raw_writel(DDR_EMIF_REF_CTRL, &pDdrEmif1Regs->SDRAM_REF_CTRL);
		__raw_writel(DDR_EMIF_REF_CTRL, &pDdrEmif1Regs->SDRAM_REF_CTRL_SHADOW);

		__raw_writel(DDR2_EMIF_REF_CTRL, &pDdrEmif1Regs->SDRAM_REF_CTRL);
		__raw_writel(DDR2_EMIF_REF_CTRL, &pDdrEmif1Regs->SDRAM_REF_CTRL_SHADOW);
	} 
    else 
    {
		/*Program EMIF0 CFG Registers*/
		__raw_writel(DDR3_EMIF_READ_LATENCY, &pDdrEmif0Regs->DDR_PHY_CTRL_1);
		__raw_writel(DDR3_EMIF_READ_LATENCY, &pDdrEmif0Regs->DDR_PHY_CTRL_1_SHADOW);
		__raw_writel(DDR3_EMIF_TIM1, &pDdrEmif0Regs->SDRAM_TIM_1);
		__raw_writel(DDR3_EMIF_TIM1, &pDdrEmif0Regs->SDRAM_TIM_1_SHADOW);
		__raw_writel(DDR3_EMIF_TIM2, &pDdrEmif0Regs->SDRAM_TIM_2);
		__raw_writel(DDR3_EMIF_TIM2, &pDdrEmif0Regs->SDRAM_TIM_2_SHADOW);
		__raw_writel(DDR3_EMIF_TIM3, &pDdrEmif0Regs->SDRAM_TIM_3);
		__raw_writel(DDR3_EMIF_TIM3, &pDdrEmif0Regs->SDRAM_TIM_3_SHADOW);
		__raw_writel(DDR3_EMIF_SDRAM_CONFIG, &pDdrEmif0Regs->SDRAM_CONFIG);

		__raw_writel(DDR_EMIF_REF_CTRL | DDR_EMIF_REF_TRIGGER, &pDdrEmif0Regs->SDRAM_REF_CTRL);
		__raw_writel(DDR_EMIF_REF_CTRL, &pDdrEmif0Regs->SDRAM_REF_CTRL_SHADOW);
		__raw_writel(DDR3_EMIF_SDRAM_ZQCR, &pDdrEmif0Regs->SDRAM_ZQCR);
		__raw_writel(DDR_EMIF_REF_CTRL, &pDdrEmif0Regs->SDRAM_REF_CTRL);
		__raw_writel(DDR_EMIF_REF_CTRL, &pDdrEmif0Regs->SDRAM_REF_CTRL_SHADOW);

		__raw_writel(DDR3_EMIF_REF_CTRL, &pDdrEmif0Regs->SDRAM_REF_CTRL);
		__raw_writel(DDR3_EMIF_REF_CTRL, &pDdrEmif0Regs->SDRAM_REF_CTRL_SHADOW);

		/*Program EMIF1 CFG Registers*/
		__raw_writel(DDR3_EMIF_READ_LATENCY, &pDdrEmif1Regs->DDR_PHY_CTRL_1);
		__raw_writel(DDR3_EMIF_READ_LATENCY, &pDdrEmif1Regs->DDR_PHY_CTRL_1_SHADOW);
		__raw_writel(DDR3_EMIF_TIM1, &pDdrEmif1Regs->SDRAM_TIM_1);
		__raw_writel(DDR3_EMIF_TIM1, &pDdrEmif1Regs->SDRAM_TIM_1_SHADOW);
		__raw_writel(DDR3_EMIF_TIM2, &pDdrEmif1Regs->SDRAM_TIM_2);
		__raw_writel(DDR3_EMIF_TIM2, &pDdrEmif1Regs->SDRAM_TIM_2_SHADOW);
		__raw_writel(DDR3_EMIF_TIM3, &pDdrEmif1Regs->SDRAM_TIM_3);
		__raw_writel(DDR3_EMIF_TIM3, &pDdrEmif1Regs->SDRAM_TIM_3_SHADOW);
		__raw_writel(DDR3_EMIF_SDRAM_CONFIG, &pDdrEmif1Regs->SDRAM_CONFIG);

		__raw_writel(DDR_EMIF_REF_CTRL | DDR_EMIF_REF_TRIGGER, &pDdrEmif1Regs->SDRAM_REF_CTRL);
		__raw_writel(DDR_EMIF_REF_CTRL, &pDdrEmif1Regs->SDRAM_REF_CTRL_SHADOW);
		__raw_writel(DDR3_EMIF_SDRAM_ZQCR, &pDdrEmif1Regs->SDRAM_ZQCR);
		__raw_writel(DDR_EMIF_REF_CTRL, &pDdrEmif1Regs->SDRAM_REF_CTRL);
		__raw_writel(DDR_EMIF_REF_CTRL, &pDdrEmif1Regs->SDRAM_REF_CTRL_SHADOW);

		__raw_writel(DDR3_EMIF_REF_CTRL, &pDdrEmif1Regs->SDRAM_REF_CTRL);
		__raw_writel(DDR3_EMIF_REF_CTRL, &pDdrEmif1Regs->SDRAM_REF_CTRL_SHADOW);
	}
}


static VOID Unlock_pll_control_mmr()
{
    OUTREG32(OALPAtoUA(0x481C5040), 0x1EDA4C3D);
    OUTREG32(OALPAtoUA(0x48140060), 0x2FF1AC2B);
    OUTREG32(OALPAtoUA(0x48140064), 0xF757FDC0);
    OUTREG32(OALPAtoUA(0x48140068), 0xE2BC3A6D);
    OUTREG32(OALPAtoUA(0x4814006c), 0x1EBF131D);
    OUTREG32(OALPAtoUA(0x48140070), 0x6F361E05);
}


/*
 * configure individual ADPLLJ
 */
static void Pll_config(UINT32 base, UINT32 n, UINT32 m, UINT32 m2, UINT32 clkctrl_val)
{
	UINT32 m2nval, mn2val, read_clkctrl = 0;

	m2nval = (m2 << 16) | n;
	mn2val = m;

	/* by-pass pll */
	read_clkctrl = INREG32(OALPAtoUA(base + ADPLLJ_CLKCTRL));
	OUTREG32(OALPAtoUA(base + ADPLLJ_CLKCTRL), (read_clkctrl | 0x00800000));
	while ((INREG32(OALPAtoUA(base + ADPLLJ_STATUS)) & 0x101) != 0x101);
	read_clkctrl = INREG32(OALPAtoUA(base + ADPLLJ_CLKCTRL));
	OUTREG32(OALPAtoUA(base + ADPLLJ_CLKCTRL), (read_clkctrl & 0xfffffffe));


	/*
	 * ref_clk = 20/(n + 1);
	 * clkout_dco = ref_clk * m;
	 * clk_out = clkout_dco/m2;
	*/

    OUTREG32(OALPAtoUA(base + ADPLLJ_M2NDIV), m2nval);
    OUTREG32(OALPAtoUA(base + ADPLLJ_MN2DIV), mn2val);

	/* Load M2, N2 dividers of ADPLL */
    OUTREG32(OALPAtoUA(base + ADPLLJ_TENABLEDIV), 0x1);
    OUTREG32(OALPAtoUA(base + ADPLLJ_TENABLEDIV), 0x0);

	/* Loda M, N dividers of ADPLL */
    OUTREG32(OALPAtoUA(base + ADPLLJ_TENABLE), 0x1);
    OUTREG32(OALPAtoUA(base + ADPLLJ_TENABLE), 0x0);

    read_clkctrl = INREG32(OALPAtoUA(base + ADPLLJ_CLKCTRL));

	if (MODENA_PLL_BASE == base)
        OUTREG32(OALPAtoUA(base + ADPLLJ_CLKCTRL), 
                 (read_clkctrl & 0xff7fffff) | clkctrl_val);
	else
        OUTREG32(OALPAtoUA(base + ADPLLJ_CLKCTRL), 
                 (read_clkctrl & 0xff7fe3ff) | clkctrl_val);

	/* Wait for phase and freq lock */
	while((INREG32(OALPAtoUA(base + ADPLLJ_STATUS)) & 0x00000600) != 0x00000600);
}


static VOID audio_pll_config()
{
	Pll_config(AUDIO_PLL_BASE,
			THIS_AUDIO_N, AUDIO_M,
			AUDIO_M2, AUDIO_CLKCTRL);
}

static VOID Sata_pll_config()
{
    AM387X_PRCM_REGS* pPrcmRegs = OALPAtoUA(AM387X_PRCM_REGS_PA);

    OUTREG32(OALPAtoUA(SATA_PLLCFG1), 0xC12C003C);
	xdelay(100);
    OUTREG32(OALPAtoUA(SATA_PLLCFG3), 0x004008E0);
	xdelay(100);

    OUTREG32(OALPAtoUA(SATA_PLLCFG0), 0x80000004);
	xdelay(100);

	/* Enable PLL LDO */
    OUTREG32(OALPAtoUA(SATA_PLLCFG0), 0x80000014);
	xdelay(100);

	/* Enable DIG LDO, ENBGSC_REF, PLL LDO */
    OUTREG32(OALPAtoUA(SATA_PLLCFG0), 0x80000016);
	xdelay(100);

    OUTREG32(OALPAtoUA(SATA_PLLCFG0), 0xC0000017);
	xdelay(100);

	/* wait for ADPLL lock */
	while(((INREG32(OALPAtoUA(SATA_PLLSTATUS)) & 0x01) != 0x01));

	OUTREG32(&pPrcmRegs->CM_ALWON2_SATA_CLKCTRL, 0x2);                   /*Enable SATA Clock*/
	while((INREG32(&pPrcmRegs->CM_ALWON2_SATA_CLKCTRL) & 0x0F) != 0x02); /*Poll for Module is functional*/

	OUTREG32(&pPrcmRegs->CM_ALWON2_L3_MED_CLKSTCTRL, 0x2);                   /*Enable SATA Clock*/
	while((INREG32(&pPrcmRegs->CM_ALWON2_L3_MED_CLKSTCTRL) & 0x0F) != 0x02); /*Poll for Module is functional*/

}

static VOID Modena_pll_config()
{
	Pll_config(MODENA_PLL_BASE,
			MODENA_N, MODENA_M,
			MODENA_M2, MODENA_CLKCTRL);
}




static VOID L3_pll_config()
{
	Pll_config(L3_PLL_BASE,
			L3_N, L3_M,
			L3_M2, L3_CLKCTRL);
}

static VOID Ddr_pll_config()
{
	Pll_config(DDR_PLL_BASE,
			THIS_DDR_N, DDR_M,
			DDR_M2, DDR_CLKCTRL);
}

static VOID Dsp_pll_config()
{
	Pll_config(DSP_PLL_BASE,
			DSP_N, DSP_M,
			DSP_M2, DSP_CLKCTRL);
}

static VOID Dss_pll_config()
{
	Pll_config(DSS_PLL_BASE,
			DSS_N, DSS_M,
			DSS_M2, DSS_CLKCTRL);
}

static VOID Sgx_pll_config()
{
    AM387X_PRCM_REGS* pPrcmRegs = OALPAtoUA(AM387X_PRCM_REGS_PA);

//    PLL_Clocks_Config(SGX_PLL_BASE,OSC_0,19,800,4,ADPLLJ_CLKCRTL_HS2);
	Pll_config(SGX_PLL_BASE,
			SGX_N, SGX_M,
			SGX_M2, SGX_CLKCTRL);

    /* The above setting makes clk 200MHz. Set PRCM to not divide */
    OUTREG32(&pPrcmRegs->CM_SYSCLK23_CLKSEL, 0);
}

static VOID Iva_pll_config()
{
	Pll_config(IVA_PLL_BASE,
			IVA_N, IVA_M,
			IVA_M2, IVA_CLKCTRL);
}

static VOID Iss_pll_config()
{
	Pll_config(ISS_PLL_BASE,
			ISS_N, ISS_M,
			ISS_M2, ISS_CLKCTRL);
}

static VOID Usb_pll_config()
{
	Pll_config(USB_PLL_BASE,
			USB_N, USB_M,
			USB_M2, USB_CLKCTRL);
}

static VOID Video_0_pll_config()
{
	Pll_config(VIDEO_0_PLL_BASE,
			VIDEO_0_N, VIDEO_0_M,
			VIDEO_0_M2, VIDEO_0_CLKCTRL);
}

static VOID Video_1_pll_config()
{
	Pll_config(VIDEO_1_PLL_BASE,
			VIDEO_1_N, VIDEO_1_M,
			VIDEO_1_M2, VIDEO_1_CLKCTRL);
}

/*
 * Enable the clks & power for perifs (TIMER1, UART0,...)
 */
void Per_clocks_enable(void)
{
    AM387X_PRCM_REGS* pPrcmRegs = OALPAtoUA(AM387X_PRCM_REGS_PA);
	UINT32 temp;

    OUTREG32(&pPrcmRegs->CM_ALWON_L3_SLOW_CLKSTCTRL, 0x2);

	/* TODO: No module level enable as in ti8148 ??? */
	/* Selects OSC0 (20MHz) for DMTIMER1 */
	temp = INREG32(OALPAtoUA(AM387X_GPTIMER_CLKSRC));
	temp &= ~(0x7 << 3);
	temp |= (0x4 << 3);
    OUTREG32(OALPAtoUA(AM387X_GPTIMER_CLKSRC), temp);

    OUTREG32(OALPAtoUA(AM387X_GPTIMER1_REGS_PA + 0x54), 0x2);
	while(INREG32(OALPAtoUA(AM387X_GPTIMER1_REGS_PA + 0x10)) & 1);

    OUTREG32(OALPAtoUA(AM387X_GPTIMER1_REGS_PA + 0x38), 0x1);

	/* UARTs */
    OUTREG32(&pPrcmRegs->CM_ALWON_UART_0_CLKCTRL, 0x2);
	while(INREG32(&pPrcmRegs->CM_ALWON_UART_0_CLKCTRL) != 0x2);

    OUTREG32(&pPrcmRegs->CM_ALWON_UART_1_CLKCTRL, 0x2);
	while(INREG32(&pPrcmRegs->CM_ALWON_UART_1_CLKCTRL) != 0x2);

    OUTREG32(&pPrcmRegs->CM_ALWON_UART_2_CLKCTRL, 0x2);
	while(INREG32(&pPrcmRegs->CM_ALWON_UART_2_CLKCTRL) != 0x2);

	while((INREG32(&pPrcmRegs->CM_ALWON_L3_SLOW_CLKSTCTRL) & 0x2100) != 0x2100);

	/* SPI */
    OUTREG32(&pPrcmRegs->CM_ALWON_SPI_CLKCTRL, 0x2);
	while(INREG32(&pPrcmRegs->CM_ALWON_SPI_CLKCTRL) != 0x2);

	/* I2C0 and I2C2 */
    OUTREG32(&pPrcmRegs->CM_ALWON_I2C_02_CLKCTRL, 0x2);
	while(INREG32(&pPrcmRegs->CM_ALWON_I2C_02_CLKCTRL) != 0x2);

    /* Ethernet */
    OUTREG32(&pPrcmRegs->CM_ETHERNET_CLKSTCTRL, 0x2);
	while((INREG32(&pPrcmRegs->CM_ETHERNET_CLKSTCTRL) & 0x0F) != 0x2);
	
    OUTREG32(&pPrcmRegs->CM_ALWON_ETHERNET_0_CLKCTRL, 0x2);
	while((INREG32(&pPrcmRegs->CM_ALWON_ETHERNET_0_CLKCTRL) & 0x0F) != 0x2);
	
    OUTREG32(&pPrcmRegs->CM_ALWON_ETHERNET_1_CLKCTRL, 0x2);
	while((INREG32(&pPrcmRegs->CM_ALWON_ETHERNET_1_CLKCTRL) & 0x0F) != 0x2);
	
	while(INREG32(&pPrcmRegs->CM_ETHERNET_CLKSTCTRL) != 0x302);

	/* HSMMC */
    OUTREG32(&pPrcmRegs->CM_ALWON_MMCHS_1_CLKCTRL, 0x2);
	while(INREG32(&pPrcmRegs->CM_ALWON_MMCHS_1_CLKCTRL) != 0x2);

    //+++FIXME
	/* WDT */
	/* For WDT to be functional, it needs to be first stopped by writing
	 * the pattern 0xAAAA followed by 0x5555 in the WDT start/stop register.
	 * After that a write-once register in Control module needs to be
	 * configured to unfreeze the timer.
	 * Note: It is important to stop the watchdog before unfreezing it
	*/
	OUTREG32(OALPAtoUA(WDT_WSPR), 0xAAAA);
	while (INREG32(OALPAtoUA(WDT_WWPS)) != 0x0);
	OUTREG32(OALPAtoUA(WDT_WSPR), 0x5555);
	while (INREG32(OALPAtoUA(WDT_WWPS)) != 0x0);

	/* Unfreeze WDT */
	OUTREG32(OALPAtoUA(WDT_UNFREEZE), 0x13);
}


// Ref.  GEL PLL_CLOCKS_Config()
static VOID PrcmInit(UINT32 in_ddr)
{
    AM387X_PRCM_REGS* pPrcmRegs = OALPAtoUA(AM387X_PRCM_REGS_PA);

	/* Enable the control module */
    OUTREG32(&pPrcmRegs->CM_ALWON_CONTROL_CLKCTRL, 0x2);
//	while(INREG32(&pPrcmRegs->CM_ALWON_CONTROL_CLKCTRL) != 0x2);

    /* Setup the various plls */
    audio_pll_config();

    Modena_pll_config();
    L3_pll_config();
    Ddr_pll_config();
    Dsp_pll_config();
    Dss_pll_config();
    Sgx_pll_config();
    Iva_pll_config();
    Iss_pll_config();
    Usb_pll_config();
    Video_0_pll_config();
    Video_1_pll_config();

    Sata_pll_config();

    /*  With clk freqs setup to desired values, enable the required peripherals */
    Per_clocks_enable();
}


/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

static VOID Enable_gpmc_cs_config(const UINT32 *gpmc_config, OMAP_GPMC_REGS* pGpmc, UINT32 cs, UINT32 base, UINT32 size)
{
    REG32 *pCS = &pGpmc->GPMC_CONFIG1_0;
    REG32 *pcs_confign;

    pCS = pCS + cs * 12; // Move to the proper CS
    pcs_confign = pCS;   // GPMC_CONFIG1_n

    OUTREG32((pcs_confign+6), 0);             // GPMC_CONFIG7_n
	xdelay(1000);
	/* Delay for settling */
    OUTREG32(pcs_confign++, gpmc_config[0]);  // GPMC_CONFIG1_n
    OUTREG32(pcs_confign++, gpmc_config[1]);  // GPMC_CONFIG2_n
    OUTREG32(pcs_confign++, gpmc_config[2]);  // GPMC_CONFIG3_n
    OUTREG32(pcs_confign++, gpmc_config[3]);  // GPMC_CONFIG4_n
    OUTREG32(pcs_confign++, gpmc_config[4]);  // GPMC_CONFIG5_n
    OUTREG32(pcs_confign++, gpmc_config[5]);  // GPMC_CONFIG6_n
	/* Enable the config */
    OUTREG32(pcs_confign, (((size & 0xF) << 8) | ((base >> 24) & 0x3F) | (1 << 6)));  // GPMC_CONFIG7_n
	xdelay(2000);
}


static VOID Gpmc_init()
{
    UINT32 base, size;
    const UINT32 *gpmc_config = NULL;
    OMAP_GPMC_REGS* pGpmc = OALPAtoUA(AM387X_GPMC_REGS_PA);

    // global settings
    OUTREG32(&pGpmc->GPMC_SYSCONFIG, 0x00000019);
    OUTREG32(&pGpmc->GPMC_IRQSTATUS, 0x00000100);
    OUTREG32(&pGpmc->GPMC_IRQENABLE, 0x00000200);
    OUTREG32(&pGpmc->GPMC_CONFIG,    0x00000012);

	/*
	 * Disable the GPMC0 config set by ROM code
	 */
    OUTREG32(&pGpmc->GPMC_CONFIG7_0, 0x00000000);
    xdelay(1000);  // +++TODO

    gpmc_config = gpmc_m_nand;
    base = GPMC_NAND_BASE;
    size = GPMC_NAND_SIZE;
    Enable_gpmc_cs_config(gpmc_config, pGpmc, 0, GPMC_NAND_BASE, GPMC_NAND_SIZE);
}


#define PADCTRL_BASE  AM387X_L4_CNTRL_MODULE_PA

#define PAD204_CNTRL  (*(volatile unsigned int *)(PADCTRL_BASE + 0x0B2c))
#define PAD205_CNTRL  (*(volatile unsigned int *)(PADCTRL_BASE + 0x0B30))
#define PAD206_CNTRL  (*(volatile unsigned int *)(PADCTRL_BASE + 0x0B34))
#define PAD207_CNTRL  (*(volatile unsigned int *)(PADCTRL_BASE + 0x0B38))
#define PAD208_CNTRL  (*(volatile unsigned int *)(PADCTRL_BASE + 0x0B3c))
#define PAD209_CNTRL  (*(volatile unsigned int *)(PADCTRL_BASE + 0x0B40))
#define PAD210_CNTRL  (*(volatile unsigned int *)(PADCTRL_BASE + 0x0B44))
#define PAD211_CNTRL  (*(volatile unsigned int *)(PADCTRL_BASE + 0x0B48))
#define PAD212_CNTRL  (*(volatile unsigned int *)(PADCTRL_BASE + 0x0B4c))
#define PAD213_CNTRL  (*(volatile unsigned int *)(PADCTRL_BASE + 0x0B50))
#define PAD214_CNTRL  (*(volatile unsigned int *)(PADCTRL_BASE + 0x0B54))
#define PAD215_CNTRL  (*(volatile unsigned int *)(PADCTRL_BASE + 0x0B58))
#define PAD216_CNTRL  (*(volatile unsigned int *)(PADCTRL_BASE + 0x0B5c))
#define PAD217_CNTRL  (*(volatile unsigned int *)(PADCTRL_BASE + 0x0B60))
#define PAD218_CNTRL  (*(volatile unsigned int *)(PADCTRL_BASE + 0x0B64))
#define PAD219_CNTRL  (*(volatile unsigned int *)(PADCTRL_BASE + 0x0B68))
#define PAD220_CNTRL  (*(volatile unsigned int *)(PADCTRL_BASE + 0x0B6c))
#define PAD221_CNTRL  (*(volatile unsigned int *)(PADCTRL_BASE + 0x0B70))
#define PAD222_CNTRL  (*(volatile unsigned int *)(PADCTRL_BASE + 0x0B74))
#define PAD223_CNTRL  (*(volatile unsigned int *)(PADCTRL_BASE + 0x0B78))
#define PAD224_CNTRL  (*(volatile unsigned int *)(PADCTRL_BASE + 0x0B7c))
#define PAD225_CNTRL  (*(volatile unsigned int *)(PADCTRL_BASE + 0x0B80))
#define PAD226_CNTRL  (*(volatile unsigned int *)(PADCTRL_BASE + 0x0B84))
#define PAD227_CNTRL  (*(volatile unsigned int *)(PADCTRL_BASE + 0x0B88))

#define PAD232_CNTRL  (*(volatile unsigned int *)(PADCTRL_BASE + 0x0B9C))
#define PAD233_CNTRL  (*(volatile unsigned int *)(PADCTRL_BASE + 0x0BA0))
#define PAD234_CNTRL  (*(volatile unsigned int *)(PADCTRL_BASE + 0x0BA4))
#define PAD235_CNTRL  (*(volatile unsigned int *)(PADCTRL_BASE + 0x0BA8))
#define PAD236_CNTRL  (*(volatile unsigned int *)(PADCTRL_BASE + 0x0BAC))
#define PAD237_CNTRL  (*(volatile unsigned int *)(PADCTRL_BASE + 0x0BB0))
#define PAD238_CNTRL  (*(volatile unsigned int *)(PADCTRL_BASE + 0x0BB4))
#define PAD239_CNTRL  (*(volatile unsigned int *)(PADCTRL_BASE + 0x0BB8))
#define PAD240_CNTRL  (*(volatile unsigned int *)(PADCTRL_BASE + 0x0BBC))
#define PAD241_CNTRL  (*(volatile unsigned int *)(PADCTRL_BASE + 0x0BC0))
#define PAD242_CNTRL  (*(volatile unsigned int *)(PADCTRL_BASE + 0x0BC4))
#define PAD243_CNTRL  (*(volatile unsigned int *)(PADCTRL_BASE + 0x0BC8))
#define PAD244_CNTRL  (*(volatile unsigned int *)(PADCTRL_BASE + 0x0BCC))
#define PAD245_CNTRL  (*(volatile unsigned int *)(PADCTRL_BASE + 0x0BD0))
#define PAD246_CNTRL  (*(volatile unsigned int *)(PADCTRL_BASE + 0x0BD4))
#define PAD247_CNTRL  (*(volatile unsigned int *)(PADCTRL_BASE + 0x0BD8))
#define PAD248_CNTRL  (*(volatile unsigned int *)(PADCTRL_BASE + 0x0BDC))
#define PAD249_CNTRL  (*(volatile unsigned int *)(PADCTRL_BASE + 0x0BE0))
#define PAD250_CNTRL  (*(volatile unsigned int *)(PADCTRL_BASE + 0x0BE4))
#define PAD251_CNTRL  (*(volatile unsigned int *)(PADCTRL_BASE + 0x0BE8))
#define PAD252_CNTRL  (*(volatile unsigned int *)(PADCTRL_BASE + 0x0BEC))
#define PAD253_CNTRL  (*(volatile unsigned int *)(PADCTRL_BASE + 0x0BF0))
#define PAD254_CNTRL  (*(volatile unsigned int *)(PADCTRL_BASE + 0x0BF4))
#define PAD255_CNTRL  (*(volatile unsigned int *)(PADCTRL_BASE + 0x0BF8))
#define PAD256_CNTRL  (*(volatile unsigned int *)(PADCTRL_BASE + 0x0BFC))
#define PAD257_CNTRL  (*(volatile unsigned int *)(PADCTRL_BASE + 0x0C00))
#define PAD258_CNTRL  (*(volatile unsigned int *)(PADCTRL_BASE + 0x0C04))

#define BIT(x) (1<<(x))

static void Cpsw_pad_config(void)
{
	volatile UINT32 val = 0;

    /*configure pin mux for rmii_refclk,mdio_clk,mdio_d */
	val = PAD232_CNTRL;
	PAD232_CNTRL = (volatile unsigned int) (BIT(18) | BIT(0));
	val = PAD233_CNTRL;
	PAD233_CNTRL = (volatile unsigned int) (BIT(19) | BIT(17) | BIT(0));
	val = PAD234_CNTRL;
	PAD234_CNTRL = (volatile unsigned int) (BIT(19) | BIT(18) | BIT(17) | BIT(0));

    /*For PG1.0 we only support GMII Mode, setup gmii0/gmii1 pins here*/
	if (PG1_0 == get_cpu_rev(NULL)) {
        /* setup gmii0 pins, pins235-258 in function mode 1 */
		val = PAD235_CNTRL;
		PAD235_CNTRL = (volatile unsigned int) (BIT(19) | BIT(18) | BIT(0));
		val = PAD236_CNTRL;
		PAD236_CNTRL = (volatile unsigned int) (BIT(19) | BIT(18) | BIT(0));
		val = PAD237_CNTRL;
		PAD237_CNTRL = (volatile unsigned int) (BIT(19) | BIT(18) | BIT(0));
		val = PAD238_CNTRL;
		PAD238_CNTRL = (volatile unsigned int) (BIT(19) | BIT(18) | BIT(0));
		val = PAD239_CNTRL;
		PAD239_CNTRL = (volatile unsigned int) (BIT(19) | BIT(18) | BIT(0));
		val = PAD240_CNTRL;
		PAD240_CNTRL = (volatile unsigned int) (BIT(18) | BIT(0));
		val = PAD241_CNTRL;
		PAD241_CNTRL = (volatile unsigned int) (BIT(18) | BIT(0));
		val = PAD242_CNTRL;
		PAD242_CNTRL = (volatile unsigned int) (BIT(18) | BIT(0));
		val = PAD243_CNTRL;
		PAD243_CNTRL = (volatile unsigned int) (BIT(18) | BIT(0));
		val = PAD244_CNTRL;
		PAD244_CNTRL = (volatile unsigned int) (BIT(18) | BIT(0));
		val = PAD245_CNTRL;
		PAD245_CNTRL = (volatile unsigned int) (BIT(18) | BIT(0));
		val = PAD246_CNTRL;
		PAD246_CNTRL = (volatile unsigned int) (BIT(18) | BIT(0));
		val = PAD247_CNTRL;
		PAD247_CNTRL = (volatile unsigned int) (BIT(18) | BIT(0));
		val = PAD248_CNTRL;
		PAD248_CNTRL = (volatile unsigned int) (BIT(18) | BIT(0));
		val = PAD249_CNTRL;
		PAD249_CNTRL = (volatile unsigned int) (BIT(0));
		val = PAD250_CNTRL;
		PAD250_CNTRL = (volatile unsigned int) (BIT(0));
		val = PAD251_CNTRL;
		PAD251_CNTRL = (volatile unsigned int) (BIT(0));
		val = PAD252_CNTRL;
		PAD252_CNTRL = (volatile unsigned int) (BIT(0));
		val = PAD253_CNTRL;
		PAD253_CNTRL = (volatile unsigned int) (BIT(0));
		val = PAD254_CNTRL;
		PAD254_CNTRL = (volatile unsigned int) (BIT(0));
		val = PAD255_CNTRL;
		PAD255_CNTRL = (volatile unsigned int) (BIT(0));
		val = PAD256_CNTRL;
		PAD256_CNTRL = (volatile unsigned int) (BIT(0));
		val = PAD257_CNTRL;
		PAD257_CNTRL = (volatile unsigned int) (BIT(0));
		val = PAD258_CNTRL;
		PAD258_CNTRL = (volatile unsigned int) (BIT(0));

		/* setup gmii1 pins, pins204-227 in function mode 2 */
		val = PAD204_CNTRL;
		PAD204_CNTRL = (volatile unsigned int) (BIT(19) | BIT(18) | BIT(1));
		val = PAD205_CNTRL;
		PAD205_CNTRL = (volatile unsigned int) (BIT(19) | BIT(18) | BIT(1));
		val = PAD206_CNTRL;
		PAD206_CNTRL = (volatile unsigned int) (BIT(19) | BIT(18) | BIT(1));
		val = PAD207_CNTRL;
		PAD207_CNTRL = (volatile unsigned int) (BIT(19) | BIT(18) | BIT(1));
		val = PAD208_CNTRL;
		PAD208_CNTRL = (volatile unsigned int) (BIT(19) | BIT(18) | BIT(1));
		val = PAD209_CNTRL;
		PAD209_CNTRL = (volatile unsigned int) (BIT(18) | BIT(1));
		val = PAD210_CNTRL;
		PAD210_CNTRL = (volatile unsigned int) (BIT(18) | BIT(1));
		val = PAD211_CNTRL;
		PAD211_CNTRL = (volatile unsigned int) (BIT(18) | BIT(1));
		val = PAD212_CNTRL;
		PAD212_CNTRL = (volatile unsigned int) (BIT(18) | BIT(1));
		val = PAD213_CNTRL;
		PAD213_CNTRL = (volatile unsigned int) (BIT(18) | BIT(1));
		val = PAD214_CNTRL;
		PAD214_CNTRL = (volatile unsigned int) (BIT(18) | BIT(1));
		val = PAD215_CNTRL;
		PAD215_CNTRL = (volatile unsigned int) (BIT(18) | BIT(1));
		val = PAD216_CNTRL;
		PAD216_CNTRL = (volatile unsigned int) (BIT(18) | BIT(1));
		val = PAD217_CNTRL;
		PAD217_CNTRL = (volatile unsigned int) (BIT(18) | BIT(1));
		val = PAD218_CNTRL;
		PAD218_CNTRL = (volatile unsigned int) (BIT(1));
		val = PAD219_CNTRL;
		PAD219_CNTRL = (volatile unsigned int) (BIT(1));
		val = PAD220_CNTRL;
		PAD220_CNTRL = (volatile unsigned int) (BIT(1));
		val = PAD221_CNTRL;
		PAD221_CNTRL = (volatile unsigned int) (BIT(1));
		val = PAD222_CNTRL;
		PAD222_CNTRL = (volatile unsigned int) (BIT(1));
		val = PAD223_CNTRL;
		PAD223_CNTRL = (volatile unsigned int) (BIT(1));
		val = PAD224_CNTRL;
		PAD224_CNTRL = (volatile unsigned int) (BIT(1));
		val = PAD225_CNTRL;
		PAD225_CNTRL = (volatile unsigned int) (BIT(1));
		val = PAD226_CNTRL;
		PAD226_CNTRL = (volatile unsigned int) (BIT(1));
		val = PAD227_CNTRL;
		PAD227_CNTRL = (volatile unsigned int) (BIT(1));

	} else {/*setup rgmii0/rgmii1 pins here*/
		/* In this case we enable rgmii_en bit in GMII_SEL register and
		 * still program the pins in gmii mode: gmii0 pins in mode 1*/
		val = PAD235_CNTRL; /*rgmii0_rxc*/
		PAD235_CNTRL = (volatile unsigned int) (BIT(18) | BIT(0));
		val = PAD236_CNTRL; /*rgmii0_rxctl*/
		PAD236_CNTRL = (volatile unsigned int) (BIT(18) | BIT(0));
		val = PAD237_CNTRL; /*rgmii0_rxd[2]*/
		PAD237_CNTRL = (volatile unsigned int) (BIT(18) | BIT(0));
		val = PAD238_CNTRL; /*rgmii0_txctl*/
		PAD238_CNTRL = (volatile unsigned int) BIT(0);
		val = PAD239_CNTRL; /*rgmii0_txc*/
		PAD239_CNTRL = (volatile unsigned int) BIT(0);
		val = PAD240_CNTRL; /*rgmii0_txd[0]*/
		PAD240_CNTRL = (volatile unsigned int) BIT(0);
		val = PAD241_CNTRL; /*rgmii0_rxd[0]*/
		PAD241_CNTRL = (volatile unsigned int) (BIT(18) | BIT(0));
		val = PAD242_CNTRL; /*rgmii0_rxd[1]*/
		PAD242_CNTRL = (volatile unsigned int) (BIT(18) | BIT(0));
		val = PAD243_CNTRL; /*rgmii1_rxctl*/
		PAD243_CNTRL = (volatile unsigned int) (BIT(18) | BIT(0));
		val = PAD244_CNTRL; /*rgmii0_rxd[3]*/
		PAD244_CNTRL = (volatile unsigned int) (BIT(18) | BIT(0));
		val = PAD245_CNTRL; /*rgmii0_txd[3]*/
		PAD245_CNTRL = (volatile unsigned int) BIT(0);
		val = PAD246_CNTRL; /*rgmii0_txd[2]*/
		PAD246_CNTRL = (volatile unsigned int) BIT(0);
		val = PAD247_CNTRL; /*rgmii0_txd[1]*/
		PAD247_CNTRL = (volatile unsigned int) BIT(0);
		val = PAD248_CNTRL; /*rgmii1_rxd[1]*/
		PAD248_CNTRL = (volatile unsigned int) (BIT(18) | BIT(0));
		val = PAD249_CNTRL; /*rgmii1_rxc*/
		PAD249_CNTRL = (volatile unsigned int) (BIT(18) | BIT(0));
		val = PAD250_CNTRL; /*rgmii1_rxd[3]*/
		PAD250_CNTRL = (volatile unsigned int) (BIT(18) | BIT(0));
		val = PAD251_CNTRL; /*rgmii1_txd[1]*/
		PAD251_CNTRL = (volatile unsigned int) (BIT(0));
		val = PAD252_CNTRL; /*rgmii1_txctl*/
		PAD252_CNTRL = (volatile unsigned int) (BIT(0));
		val = PAD253_CNTRL; /*rgmii1_txd[0]*/
		PAD253_CNTRL = (volatile unsigned int) (BIT(0));
		val = PAD254_CNTRL; /*rgmii1_txd[2]*/
		PAD254_CNTRL = (volatile unsigned int) (BIT(0));
		val = PAD255_CNTRL; /*rgmii1_txc*/
		PAD255_CNTRL = (volatile unsigned int) (BIT(0));
		val = PAD256_CNTRL; /*rgmii1_rxd[0]*/
		PAD256_CNTRL = (volatile unsigned int) (BIT(18) | BIT(0));
		val = PAD257_CNTRL; /*rgmii1_txd[3]*/
		PAD257_CNTRL = (volatile unsigned int) (BIT(0));
		val = PAD258_CNTRL; /*rgmii1_rxd[2]*/
		PAD258_CNTRL = (volatile unsigned int) (BIT(18) | BIT(0));
	}
}

#define PAD263_CNTRL  (*(volatile unsigned int *)(PADCTRL_BASE + 0x0C18))
#define PAD264_CNTRL  (*(volatile unsigned int *)(PADCTRL_BASE + 0x0C1C))

static void I2c0_pad_config()
{
#if 0
	WR_MEM_32(PINCNTL263, 0x00060001);  
	WR_MEM_32(PINCNTL264, 0x00060001);  
#endif

	volatile UINT32 val = 0;

	val = PAD263_CNTRL;
	PAD263_CNTRL = (volatile unsigned int) (BIT(18) | BIT(17) | BIT(0));

	val = PAD264_CNTRL;
	PAD264_CNTRL = (volatile unsigned int) (BIT(18) | BIT(17) | BIT(0));
}


#define PAD16_CNTRL  (*(volatile unsigned int *)(PADCTRL_BASE + 0x083C))
#define PAD39_CNTRL  (*(volatile unsigned int *)(PADCTRL_BASE + 0x0898))
#define PAD40_CNTRL  (*(volatile unsigned int *)(PADCTRL_BASE + 0x089C))
#define PAD41_CNTRL  (*(volatile unsigned int *)(PADCTRL_BASE + 0x08A0))
#define PAD42_CNTRL  (*(volatile unsigned int *)(PADCTRL_BASE + 0x08A4))

static void Audio_pad_config(void)
{
#if 0
	WR_MEM_32(PINCNTL16, 0x00060004);  /* MCA2- AHCLKX */
	WR_MEM_32(PINCNTL39, 0x00060001);  /* MCA2- ACLKX */
	WR_MEM_32(PINCNTL40, 0x00060001);  /* MCA2- AFSX */
	WR_MEM_32(PINCNTL41, 0x00060001);  /* MCA2- AXR0 */
	WR_MEM_32(PINCNTL42, 0x00060001);  /* MCA2- AXR1 */
#endif

	volatile UINT32 val = 0;

	val = PAD16_CNTRL;
	PAD16_CNTRL = (volatile unsigned int) (BIT(18) | BIT(17) | BIT(2));

	val = PAD39_CNTRL;
	PAD39_CNTRL = (volatile unsigned int) (BIT(18) | BIT(17) | BIT(0));

	val = PAD40_CNTRL;
	PAD40_CNTRL = (volatile unsigned int) (BIT(18) | BIT(17) | BIT(0));

	val = PAD41_CNTRL;
	PAD41_CNTRL = (volatile unsigned int) (BIT(18) | BIT(17) | BIT(0));

	val = PAD42_CNTRL;
	PAD42_CNTRL = (volatile unsigned int) (BIT(18) | BIT(17) | BIT(0));
}


/*
 * baord specific muxing of pins
 */
void Set_muxconf_regs(void)
{
	UINT32 i, add, val;
	UINT32 pad_conf[] = {
#include "mux.h"
	};

	for (i = 0; i<N_PINS; i++)
	{
		add = AM387X_PINCTRL_REGS_PA + (i*4);
		val = INREG32(add);
		val |= pad_conf[i];
		OUTREG32(add, val);
	}

	/* MMC/SD pull-down enable */
    OUTREG32(0x48140928, 0x000C0040);
}


#define RMII_REFCLK_SRC			(AM387X_PLLSS_REGS_PA + 0x2E8)
#define GMII_SEL				(AM387X_L4_CNTRL_MODULE_PA + 0x650)

/*
 * Basic board specific setup
 */
int Board_init(void)
{
//	u32 regVal;

	/* Do the required pin-muxing before modules are setup */
	Set_muxconf_regs();

	/* setup RMII_REFCLK to be sourced from audio_pll */
	OUTREG32(RMII_REFCLK_SRC, 0x4);

	if (PG2_1 == get_cpu_rev(NULL)) 
    {
		/*program GMII_SEL register for RGMII mode */
		OUTREG32(GMII_SEL, 0x30A);
	}

	Cpsw_pad_config();

    I2c0_pad_config();

    Audio_pad_config();

#if 0
	/* Get Timer and UART out of reset */

	/* UART softreset */
	regVal = __raw_readl(UART_SYSCFG);
	regVal |= 0x2;
	__raw_writel(regVal, UART_SYSCFG);
	while( (__raw_readl(UART_SYSSTS) & 0x1) != 0x1);

	/* Disable smart idle */
	regVal = __raw_readl(UART_SYSCFG);
	regVal |= (1<<3);
	__raw_writel(regVal, UART_SYSCFG);

	/* mach type passed to kernel */
	gd->bd->bi_arch_number = MACH_TYPE_TI8148EVM;

	/* address of boot parameters */
	gd->bd->bi_boot_params = PHYS_DRAM_1 + 0x100;
#endif

	Gpmc_init();

	return 0;
}


//------------------------------------------------------------------------------
//
//  Function:  PlatformSetup
//
//  Initializes platform settings.  Stack based initialization only - no 
//  global variables allowed.
//
VOID PlatformSetup()
{
    Unlock_pll_control_mmr();

    /* Setup the PLLs and the clocks for the peripherals */
    PrcmInit(IN_DDR);

	if (!IN_DDR)
		config_ti814x_ddr();	/* Do DDR settings */

    Board_init();
}




