#ifndef ScarletDisplayContext_HH
#define ScarletDisplayContext_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2010, Covidien Plc
//====================================================================

//====================================================================
// Class: ScarletDisplayContext -- Provides Fujitsu Scarlet specific
//        implementation for methods inherited from DisplayContext.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/VGA-Graphics-Driver/vcssrc/ScarletDisplayContext.hhv   25.0.4.0   19 Nov 2013 14:37:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003   By: rpr     Date: 11-Mar-2011     SCR Number: 6752
//  Project: XENA2
//      ExpandFont and ExpandIcon passes the original witdth of the font/bitmap
//		so clipping can be accounted for properly.
// 
//  Revision: 002   By: rpr     Date: 11-Mar-2011     SCR Number: 6751
//  Project: XENA2
//      Made offScreenImage larger to account for larger expectation with
//		btblting routines.
// 
//  Revision: 001   By: mnr    Date:  24-Sep-2010    SCR Number: 6672
//  Project: XENA2
//  Description:
//     Initial checkin
//
//====================================================================

#include "VgaGraphicsDriver.hh"
#include "DisplayContext.hh"
#include "VgaDisplayId.hh"
#include "VgaScarlet.hh"

//@ Usage-Classes
#include "RgbQuad.hh"
//@ End-Usage


class ScarletDisplayContext : public DisplayContext
{
  public:
    ScarletDisplayContext( VgaDisplayId displayId, Int frame = 0);
    
    ~ScarletDisplayContext();

    virtual void lock(void);
    virtual void unlock(void);
    virtual void accessDisplay(void) const;
    
    virtual void fillSolid(
                    Int xDest, Int yDest, Int xWidth, Int yHeight, Int color);

    virtual void expandFont(Int xDest, Int yDest, Int xWidth, Int yHeight, 
                    const Byte * pFontData, Uint fontOffset, Int color, Int orignalWidth);

    virtual void expandIcon(Int xDest, Int yDest, Int xWidth, Int yHeight, 
                    const Byte * pFontData, Uint fontOffset, Int color, Int orignalWidth);

    virtual void moveBlock(Int xSrc, Int ySrc, Int xDest, 
                    Int yDest, Int xWidth, Int yHeight);

    virtual void *allocateImage(Uint size);

    virtual void freeImage(void *pBlock);

    virtual void storeImage(void *pDest, Int xSrc, Int ySrc, Int xWidth, Int yHeight);

    virtual void recoverImage(Int xDest, Int yDest, const void *pSrc, Int xWidth, Int yHeight);

    virtual void allocateColor(Byte & index, const RgbQuad & rgb );

    virtual void storeColor(Byte index, const RgbQuad & rgb, 
                    Boolean readOnly=TRUE);

    virtual void getColor(Byte index, RgbQuad & rgb);

    virtual void showFrame(void);

    virtual void  drawPoint(
                    GraphicsContext & rGc, Int x, Int y);

    virtual void  drawLine(
                    GraphicsContext & rGc,
                    Int x0, Int y0, Int x1, Int y1);

    virtual void  fillRectangle(
                    GraphicsContext & rGc,
                    Int x0, Int y0, Int x1, Int y1 );

    virtual void  drawTriangle(
                    GraphicsContext & rGc,
                    Int x0, Int y0,
                    Int x1, Int y1,
                    Int x2, Int y2 );

    virtual void  fillTriangle(
                    GraphicsContext & rGc,
                    Int x0, Int y0,
                    Int x1, Int y1,
                    Int x2, Int y2 );

    virtual void  drawBitmap(
                    GraphicsContext & rGc,
                    DibInfo & rDibInfo,
                    Int xPos, Int yPos );

    virtual void  drawText(
                    GraphicsContext & rGc,
                    Int xPos, Int yPos, Uint fontId, const char* pString);

    virtual void  drawJapaneseText(
                    GraphicsContext & rGc,
                    Int xPos, Int yPos, Uint fontId, const char* pString);

    virtual Uint32 loadFont(const VgaFontData::CharHeader & rFontHeader);

    static void  SoftFault(const SoftFaultID softFaultID,
               const Uint32      lineNumber,
               const char*       pFileName  = NULL, 
               const char*       pPredicate = NULL);

  private:
    ScarletDisplayContext(); //declared only
    ScarletDisplayContext(const ScarletDisplayContext&); // declared only
    void operator=(const ScarletDisplayContext&);    // declared only

    Boolean isInDisplayMemory_(const void* pMemory) const;

	// off screen images are moved via bltCopyAlt routines which assume 
	// horizotal sizes of 640 width, allocate enough memory for 
	// offScreenImage to store maximum size 640*480; keeps images from 
	// overflowing into scratch pad and fonts 
	// with new scarlet chip there is plenty of memory to contain the
	// larger offscreen image and keep all the fonts in video memory
    struct OffScreenMemory
    {
        Byte offScreenImage[640*480];  // for off-screen image storage
        Byte scratchPad[36*36];        // for 36 * 36 icons
        Byte fontData[1];              // start of off-screen font storage
    };

    enum
    {
      SCRATCH_PAD_SIZE_ = 288   // for 36 * 36 icons
    };

    //@ Data-Member:  rVgaScarlet_
    // reference to the VGA controller object for this display
    VgaScarlet & rVgaScarlet_;

    //@ Data-Member:  pOsmStackTop_
    // pointer to the top of the off-screen memory stack
    Byte * pOsmStackTop_;

    //@ Data-Member:  isOffScreenImageAllocated_
    // TRUE if the off-screen image is allocated, FALSE when free
    Boolean isOffScreenImageAllocated_;

    //@ Data-Member:  pOffScreenMemory_
    // pointer to VGA memory located above the display frame memory area(s)
    OffScreenMemory *pOffScreenMemory_;

};

#endif // ScarletDisplayContext_HH 
