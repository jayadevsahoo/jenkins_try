#ifndef TaskFlags_HH
#define TaskFlags_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//====================================================================
 
//====================================================================
// Filename:  TaskFlags - TaskTableEntry flags.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Sys-Init/vcssrc/TaskFlags.hhv   25.0.4.0   19 Nov 2013 14:33:58   pvcs  $
//
//@ Modification-Log
//
//   Revision 001   By:   gdc      Date: 05/16/95    DR Number:   n/a
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//====================================================================
 
#if 0  
// for bug in genDD
#endif

#include "Sys_Init.hh"

//@ Type:    TaskFlags
//  Defines TaskTableEntry flags mask.
enum TaskFlags 
{
    F_SUPER     = 0x00000080,
    F_DEBUG1    = 0x00000100,
    F_DEBUG2    = 0x00000200,
    F_DEBUG3    = 0x00000400,
    F_DEBUG4    = 0x00000800,
    F_TRACE     = 0x00001000,
    F_TCMSG     = 0x00002000,

    F_REMOTE    = 0x00010000,   // NOT USED
    F_BROADCAST = 0x00020000,
    F_LOCALCOM  = 0x00040000
};

#endif // TaskFlags_HH
