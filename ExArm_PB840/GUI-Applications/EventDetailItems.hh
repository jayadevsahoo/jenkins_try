#ifndef EventDetailItems_HH
#define EventDetailItems_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: EventDetailItems -   This class contains the event's 
//                              description of each menu item.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/EventDetailItems.hhv   25.0.4.0   19 Nov 2013 14:07:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By: rhj    Date: 06-Jun-2007    SCR Number: 6237
//  Project:  Trend
//  Description:
//      Initial version.
//=====================================================================

//@ Usage-Classes
#include "GuiAppClassIds.hh"
#include "LogTarget.hh"
#include "TrendEvents.hh"
#include "TrendFormatData.hh"
//@ End-Usage

class EventDetailItems : public LogTarget
{
public:
	EventDetailItems(void);
	virtual ~EventDetailItems(void);

	// LogTarget virtual
	virtual void getLogEntryColumn(Uint16 entryNumber, 
								   Uint16 columnNumber,
								   Boolean &rIsCheapText, 
								   StringId &rString1,
								   StringId &rString2, 
								   StringId &rString3,
								   StringId &rString1Message);

	static StringId GetEventDetailStr( TrendEvents::EventId eventId);

	void updateEventDetailTable( TrendEvents::EventId eventId[], 
								 Uint32 totalNumberOfEvents);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);


	//@ Data-Member: EventDetailTable
	// Provides a mapping of the event id vs the event detail strings.
	struct EventDetailTable
	{
		Int32 eventId[ TrendEvents::TOTAL_EVENT_IDS ];
		StringId eventDetailStr[TrendEvents::TOTAL_EVENT_IDS ];
	};

private:
	// these methods are purposely declared, but not implemented...
	EventDetailItems(const EventDetailItems&);				// not implemented...
	void   operator=(const EventDetailItems&);			// not implemented...

	//@ Data-Member: eventDetailStrs_
	// Stores all event detail strings.
	StringId eventDetailStrs_[TrendEvents::TOTAL_EVENT_IDS];

	//@ Data-Member: eventDetailTable_
	// The event detail table.
	EventDetailTable eventDetailTable_;

	// Constant: MAX_EVENT_ID_STR_LENGTH_ 
	// Maximum length of an event id string
	enum 
	{
		MAX_EVENT_ID_STR_LENGTH_ = 100
	};

	//@ Data-Member: eventIdStr_
	// Stores an event id string.
	wchar_t eventIdStr_[MAX_EVENT_ID_STR_LENGTH_];  
};


#endif // EventDetailItems_HH
