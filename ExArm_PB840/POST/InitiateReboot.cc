#include "stdafx.h"
//====================================================================
// This is a proprietary work to which Nellcor Puritan Bennett
// corporation of California claims exclusive right.  No part of this
// work may be used, disclosed, reproduced, sorted in an information
// retrieval system, or transmitted by any means, electronic,
// mechanical, photocopying, recording, or otherwise without the prior
// written permission of Nellcor Puritan Bennett Corporation.
//
//     Copyright (c) 1995, Nellcor Puritan Bennett Corporation
//=====================================================================

//============================ M O D U L E   D E S C R I P T I O N ====
//@ Filename:  InitiateReboot - Functions to reboot the system.
//---------------------------------------------------------------------
//@ Interface-Description
//  This module contains the functions necessary to reboot the 
//  processor and the system.  The external defined function 
//  InitiateReboot provides the interface to the application to save
//  the shutdown state and trap to the InitiateRebootHandler which
//  disables interrupts (a privileged operation) and stops strobing
//  the watchdog timer allowing it to reset the processor and reenter
//  POST. $[05123]
//---------------------------------------------------------------------
//@ Rationale
//  Provides the central mechanism for resetting the processor and
//  rebooting the application.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ Fault-Handling
//	none
//---------------------------------------------------------------------
//@ Restrictions
//	none
//---------------------------------------------------------------------
//@ Invariants
//	none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/POST/vcssrc/InitiateReboot.ccv   25.0.4.0   19 Nov 2013 14:17:04   pvcs  $
//
//@ Modification-Log
//  
//  Revision: 008    By: Gary Cederquist  Date: 27-Jan-2011  SCR Number: 6671
//    Project: XENA2
//    Description:
//		Modified to use Delay::WaitForTicks.
//
//  Revision: 007    By: Gary Cederquist  Date: 18-Jan-2001  DR Number: 5493
//    Project: GuiComms
//    Description:
//		Modified for dual backlight control.
//
//  Revision: 006    By: Gary Cederquist  Date: 05-Oct-1999  DR Number: 5507
//    Project: ATC
//    Description:
//		Added manual store operation for Simtek NOVRAM inside the BD
//		InitiateRebootHandler().
//
//  Revision: 005    By: Gary Cederquist  Date: 15-AUG-1997  DR Number: 1875
//    Project: Sigma (R8027)
//    Description:
//       Added testable item annotations.
//
//  Revision: 004    By: Gary Cederquist  Date: 17-JUN-1997  DR Number: 2208
//    Project: Sigma (R8027)
//    Description:
//       Modified for new interface to VGA-Graphics-Driver subsystem.
//
//  Revision: 003    By: hct              Date: 15-MAY-1997  DR Number: 1996
//    Project: Sigma (R8027)
//    Description:
//       Added missing requirment number 05123.
//
//  Revision: 002    By: Gary Cederquist  Date: 25-APR-1997  DR Number: 1996
//    Project: Sigma (R8027)
//    Description:
//       Added missing requirment number.
//
//  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//=====================================================================

//=====================================================================
//
//      Includes...
//
//=====================================================================
 
#include "InitiateReboot.hh"
#include <stdlib.h>

//@ Usage-Classes
#include "BdIoUtil.hh"
#include "Delay.hh"
#include "DiagnosticCode.hh"
#include "MemoryMap.hh"
//TODO E600 #include "NmiSource.hh"
#include "NovRamManager.hh"
#include "Post.hh"
#include "Post_Library.hh"
//@ End-Usage

//TODO E600 removed 
//#include <cpu.h>
#include <stdlib.h>

//TODO E600 removed 
//#include <vrtxil.h>

#ifdef SIGMA_GUI_CPU
#include "VgaDevice.hh"
#endif


//@ Code...

//============= F R E E   F U N C T I O N    D E S C R I P T I O N ====
//@ Free-Function: LogWatchdogFailure
//
//@ Interface-Description
//  In the event the watchdog timer fails to reset the processor within
//  250-260ms, this function logs the watchdog timer failure to the system
//  log.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Free-Function
//=====================================================================
 
void
LogWatchdogFailure()
{
    DiagnosticCode  diagnosticCode;
    diagnosticCode.setSystemEventCode( DiagnosticCode::
                                       WATCHDOG_TIMER_FAILED);
    NovRamManager::LogSystemDiagnostic(diagnosticCode);
    // $[TI1]
}

//============= F R E E   F U N C T I O N    D E S C R I P T I O N ====
//@ Free-Function: InitiateRebootHandler
//
//@ Interface-Description
//  This handler running in the supervisor state disables interrupts
//  and stops strobing the watchdog timer allowing it to reset the 
//  processor.  It turns on the alarms and LEDs prior to waiting for
//  the watchdog reset.  On the BD, the ventilator is placed in the 
//  safe-state prior to waiting for the reset.  If the reset doesn't
//  occur within 250-260ms, this function places the ventilator in 
//  VENT-INOP preventing any software control of the ventilator until
//  power is cycled.  This handler runs in supervisor mode (as do
//  all trap handlers) so interrupts can be disabled (a privileged
//  operation).  $[00308]
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Free-Function
//=====================================================================
 
#ifdef SIGMA_DEVELOPMENT
void
InitiateRebootHandler()
{
    ::abort();
}
#endif

#ifdef SIGMA_PRODUCTION
#ifdef SIGMA_BD_CPU
void 
InitiateRebootHandler()
{
    cpu_interrupt_disable();

    // turn on the vent alarm and front panel LEDs
    SwitchAlarms( TRUE );
	SwitchLeds( TRUE );
    ActivateSafeState();

	if ( CpuDevice::GetBoardRevision() == NMI_COST_REDUCTION )
    {
	  // $[TI1.1]
      //
      // lowcost board
      //
      // The following sequence of reads is interpreted
      //  by the SIMTEK NOVRAM as a command to memorize
      //  the content to the surface SRAM into storage.
      //  NOTE: NOVRAM is comprised of two interleaved
      //  8-bit parts, so these WORD-wide reads access
      //  both parts simultaneously; the address sequence
      //  is right out of the SIMTEK book, << 1 in order
      //  to allow for the interleaving.

      *((volatile Uint16 *)(NOVRAM_BASE+(0x0E38 << 1)) );
      *((volatile Uint16 *)(NOVRAM_BASE+(0x31C7 << 1)) );
      *((volatile Uint16 *)(NOVRAM_BASE+(0x03E0 << 1)) );
      *((volatile Uint16 *)(NOVRAM_BASE+(0x3C1F << 1)) );
      *((volatile Uint16 *)(NOVRAM_BASE+(0x303F << 1)) );
      *((volatile Uint16 *)(NOVRAM_BASE+(0x0FC0 << 1)) );
    }
	// $[TI1.2]

    Delay::WaitForTicks(26);  // wait 250-260 ms

    // Watchdog timer has failed to reset the CPU at this point

    LogWatchdogFailure();

    ActivateVentInop();

    CpuDevice::LightDiagnosticLED(POST_ERROR_POWER_FAIL_WATCHDOG);
    for (;;) 
    {
       // loop forever
    }
}
#endif // SIGMA_BD_CPU


#ifdef SIGMA_GUI_CPU
void 
InitiateRebootHandler()
{
    cpu_interrupt_disable();

    // turn on the remote alarm and front panel LEDs
    SwitchAlarms( TRUE );
    SwitchLeds( TRUE );

    Delay::WaitForTicks(26);  // wait 250-260 ms

    // Watchdog timer has failed to reset the CPU at this point

    LogWatchdogFailure();

    //  Darken the screen
    VgaDevice::SetBrightness(0,0);
    VgaDevice::SetContrast(0,0);

    CpuDevice::LightDiagnosticLED(POST_ERROR_POWER_FAIL_WATCHDOG);
    for (;;) 
    {
        // loop forever
    }
    // $[TI2]
}
#endif  // SIGMA_GUI_CPU


#endif // SIGMA_PRODUCTION
//=====================================================================
//
//  External Functions...
//
//=====================================================================


//============= F R E E   F U N C T I O N    D E S C R I P T I O N ====
//@ Free-Function: InitiateReboot
//
//@ Interface-Description
//  This function sets the shutdown state of the processor and traps 
//  to InitiateRebootHandler that executes in supervisor mode.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Free-Function
//=====================================================================
 
void
InitiateReboot(ShutdownState state)
{
    Post::SetShutdownState_(state);
#ifdef SIGMA_DEVELOPMENT
    ::abort();
#else

	//TODO E600 removed asm
    //asm(" move.l  #1,d0 ");
    //asm(" trap    #1 ");
#endif
    // $[TI1]
}

