#include "StdAfx.h"

#include <string>

#include "ScriptInterpreter.h"

#include <lua.hpp>
#include <lauxlib.h>
#include <lualib.h>

HostLogFunction::HostLogFunction(CPB880SWATUnitTestDlg * pDlg, PMFN_HOSTLOG pLogFunc):pHostDlg_(pDlg), pLogFunc_(pLogFunc)
{
}

void HostLogFunction::operator ()( const TCHAR* pLogMsg )
{
	(pHostDlg_->*pLogFunc_)( pLogMsg );
}


HostLogFunction ScriptInterpreter::logFunc_ = HostLogFunction(NULL, NULL);

ScriptInterpreter::ScriptInterpreter(const HostLogFunction& logFunc) : L_(NULL)
{
	logFunc_ = logFunc;

	L_ = lua_open();

	// load various Lua libraries
	luaL_openlibs( L_ );

	// reference standard library
	SetLuaPath( L_, "C:\\Program Files (x86)\\Lua\\5.1\\lua\\?.lua" );

	lua_register( L_, "host_log", host_log );
}

ScriptInterpreter::~ScriptInterpreter(void)
{
	lua_close( L_ );
}

bool ScriptInterpreter::RunLuaFile(const TCHAR * fileName, CString& strMsg)
{
	logFunc_( fileName );

	int error = luaL_dofile( L_, fileName );

	if ( error )
	{
		strMsg.Format("%s", lua_tostring( L_, -1 ));

		return false;
	}
	
	return true;
}

int ScriptInterpreter::SetLuaPath(lua_State * L, const char * path)
{
	lua_getglobal( L, "package" );
	lua_getfield( L, -1, "path" );					// get field "path" from table at top of stack (-1)
	std::string cur_path = lua_tostring( L, -1 );	// grab path string from top of stack
	cur_path.append( ";" );							// do your path magic here
	cur_path.append( path );
	lua_pop( L, 1 );								// get rid of the string on the stack we just pushed on line 5
	lua_pushstring( L, cur_path.c_str() );			// push the new one
	lua_setfield( L, -2, "path" );					// set the field "path" in table at -2 with value at top of stack
	lua_pop( L, 1 );								// get rid of package table from top of stack

	return 0;										// all done!
}

int ScriptInterpreter::host_log(lua_State * L)
{
	TCHAR buff[512];
	strcpy_s(buff, lua_tostring(L, -1));

	logFunc_( buff );

	return 0;
}