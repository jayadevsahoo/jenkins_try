#ifndef AlarmLimitSettingButton_HH
#define AlarmLimitSettingButton_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: AlarmLimitSettingButton - A setting button which displays
// an alarm limit containing an alarm bitmap and either a numeric value
// or the text "OFF".
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/AlarmLimitSettingButton.hhv   25.0.4.0   19 Nov 2013 14:07:32   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005  By:  sah	   Date:  23-Feb-1999    DCS Number:  5314
//  Project:  ATC
//  Description:
//      Added integrated with new dynamic, setting applicability functionality.
//      This allows for looser coupling between settings and GUI, and the
//      need for significantly less "setting knowledge" in the GUI subsystem.
//
//  Revision: 004  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "SettingButton.hh"

//@ Usage-Classes
#include "NumericField.hh"
//@ End-Usage

class AlarmLimitSettingButton : public SettingButton
{
public:
	AlarmLimitSettingButton(Uint16 xOrg, Uint16 yOrg,
				SettingId::SettingIdType settingId, StringId messageId,
				SubScreen * pFocusSubScreen);
	~AlarmLimitSettingButton(void);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

	//@ Constant: BUTTON_WIDTH
	// The width of an alarm limit setting button.
	static const Int32 BUTTON_WIDTH;
	
	//@ Constant: BUTTON_HEIGHT
	// The height of an alarm limit setting button.
	static const Int32 BUTTON_HEIGHT;

protected:
	// SettingButton virtual method
	virtual void updateDisplay_(Notification::ChangeQualifier qualifierId,
                                const SettingSubject*         pSubject);

private:
	// these methods are purposely declared, but not implemented...
	AlarmLimitSettingButton(const AlarmLimitSettingButton&);// not implemented...
	void   operator=(const AlarmLimitSettingButton&);	// not implemented...

	//@ Data-Member: numericValue_
	// The numeric field which displays this button's numeric setting value.
	NumericField numericValue_;

	//@ Data-Member: offValue_
	// The text field which displays OFF if the alarm limit's value indicates
	// that the alarm limit has been switch off.
	TextField offValue_;
};


#endif // AlarmLimitSettingButton_HH 
