
//----------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------
//
//  "softdload.c"
//
//  Implementation file for UI build functions
//
//  Date: 03-28-2002
//
//----------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------
//  Include header files
//----------------------------------------------------------------------------------------------------

#include <Utility.h> // definition of NULL

#include "globals.h"
//#include "bmpfile.h" // used when converting embedded bitmaps to code
#include "ldb.h"
#include "rt_bmps.h"
#include "softdload.h"
#include "toolbox.h" // definition of errChk macro

int DloadType = 0;

extern int CVIFUNC_C GetUserBitmapFromText (char *imageText, int version, int *bitmapId); // needed if there are bitmaps
extern char IP[20];            /* Address of Ethernet Card */
extern char szIPaddr[4][256];
extern int nNumAdapters;
extern int MultiHomed;         /* 2 or more Ethernet Cards */

//----------------------------------------------------------------------------------------------------
//  Prototype UI build functions.
//----------------------------------------------------------------------------------------------------

int BuildBOOT_PANEL (int hParentPanel);
int BuildIMAG_PANEL (int hParentPanel);
int BuildINS_PANEL  (int hParentPanel);
int BuildSTATEPANEL (int hParentPanel);
int BuildTFTP_PANEL (int hParentPanel);

//-------------------------------------------------------------------------------------------------
// Prototype callbacks for Panel: BOOT_PANEL
// ------------------------------------------------------------------------------------------------

// Control: BOOT_PANEL_BOOTP_TIMEOUT
int CVICALLBACK calcTimeout (int panelHandle, int control, int event, 
		void *callbackData, int eventData1, int eventData2);
// Control: BOOT_PANEL_BOOTP_CANCEL
int CVICALLBACK BOOTPCancelCallback(int panel, int control, int event, 
		void *callbackData, int eventData1, int eventData2);

//-------------------------------------------------------------------------------------------------
// Prototype callbacks for Panel: IMAG_PANEL
// ------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
// Prototype callbacks for Panel: INS_PANEL
// ------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
// Prototype callbacks for Panel: STATEPANEL
// ------------------------------------------------------------------------------------------------
int CVICALLBACK DnloadStateCB(int panel, int control, int event, 
		void *callbackData, int eventData1, int eventData2);

//-------------------------------------------------------------------------------------------------
// Prototype callbacks for Panel: TFTP_PANEL
// ------------------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------------------
// Declare identifiers for Panel:  BOOT_PANEL
// ------------------------------------------------------------------------------------------------

int BOOT_PANEL_BOOTP_CANCEL                  = 0;  // control identifier
int BOOT_PANEL_BOOTP_TEXT                    = 0;  // control identifier
int BOOT_PANEL_DECORATION                    = 0;  // control identifier
int BOOT_PANEL_BOOTP_TIMEOUT                 = 0;  // control identifier

//-------------------------------------------------------------------------------------------------
// Declare identifiers for Panel:  IMAG_PANEL
// ------------------------------------------------------------------------------------------------

int IMAG_PANEL_VENTSFTWR_BOX                 = 0;  // control identifier
int IMAG_PANEL_VENTSFTWR_BOX_LBL             = 0;  // control identifier
int IMAG_PANEL_840_GUISOFTWARE_LBL           = 0;  // control identifier
int IMAG_PANEL_840_GUISOFTWARE               = 0;  // control identifier
int IMAG_PANEL_840_BDSOFTWARE_LBL            = 0;  // control identifier
int IMAG_PANEL_840_BDSOFTWARE                = 0;  // control identifier
int IMAG_PANEL_840_AUTODET_STATUS            = 0;  // control identifier
int IMAG_PANEL_VENT_CAL_BUTTON               = 0;  // control identifier
int IMAG_PANEL_ADAPTER_LIST                  = 0;  // control identifier
int IMAG_PANEL_IMAG_LIST                     = 0;  // control identifier
int IMAG_PANEL_LANG_DONE                     = 0;  // control identifier
int IMAG_PANEL_LANG_CANCEL                   = 0;  // control identifier
int IMAG_PANEL_TEXTMSG                       = 0;  // control identifier

//-------------------------------------------------------------------------------------------------
// Declare identifiers for Panel:  INS_PANEL
// ------------------------------------------------------------------------------------------------

int INS_PANEL_INS_DONE                       = 0;  // control identifier
int INS_PANEL_INS_CANCEL                     = 0;  // control identifier
int INS_PANEL_INSTRUCTIONS                   = 0;  // control identifier
int INS_PANEL_DECORATION                     = 0;  // control identifier
int INS_PANEL_TEXTMSG                        = 0;  // control identifier

//-------------------------------------------------------------------------------------------------
// Declare identifiers for Panel:  STATEPANEL
// ------------------------------------------------------------------------------------------------

int STATEPANEL_GROUP_BOX                     = 0;  // control identifier
int STATEPANEL_INITIAL_DLOAD                 = 0;  // control identifier
int STATEPANEL_REPLACE_BD_DLOAD              = 0;  // control identifier
int STATEPANEL_REPLACE_GUI_DLOAD             = 0;  // control identifier
int STATEPANEL_REPLACE_BOTH_DLOAD            = 0;  // control identifier
int STATEPANEL_STATE_DONE                    = 0;  // control identifier
int STATEPANEL_STATE_CANCEL                  = 0;  // control identifier

//-------------------------------------------------------------------------------------------------
// Declare identifiers for Panel:  TFTP_PANEL
// ------------------------------------------------------------------------------------------------

int TFTP_PANEL_BD_GROUP_BOX                  = 0;  // control identifier
int TFTP_PANEL_GUI_GROUP_BOX                 = 0;  // control identifier
int TFTP_PANEL_BD_LED                        = 0;  // control identifier
int TFTP_PANEL_GUI_LED                       = 0;  // control identifier
int TFTP_PANEL_BD_FILE                       = 0;  // control identifier
int TFTP_PANEL_GUI_FILE                      = 0;  // control identifier
int TFTP_PANEL_BD_PERCENT                    = 0;  // control identifier
int TFTP_PANEL_GUI_PERCENT                   = 0;  // control identifier
int TFTP_PANEL_BD_BLOCK_SENT                 = 0;  // control identifier
int TFTP_PANEL_BD_TOTAL                      = 0;  // control identifier
int TFTP_PANEL_GUI_BLOCK_SENT                = 0;  // control identifier
int TFTP_PANEL_GUI_TOTAL                     = 0;  // control identifier
int TFTP_PANEL_BD_PROGRAM                    = 0;  // control identifier
int TFTP_PANEL_BD_MSG                        = 0;  // control identifier
int TFTP_PANEL_BD_COMPLETE                   = 0;  // control identifier
int TFTP_PANEL_GUI_PROGRAM                   = 0;  // control identifier
int TFTP_PANEL_GUI_MSG                       = 0;  // control identifier
int TFTP_PANEL_GUI_COMPLETE                  = 0;  // control identifier

//----------------------------------------------------------------------------------------------------
// BuildBOOT_PANEL ():  Build UI object BOOT_PANEL; return handle or standard UI error code.
//----------------------------------------------------------------------------------------------------
int BuildBOOT_PANEL (int hParentPanel)
{
    int hPanel;
    int error = 0;
	int nZplaneOrder = 0;
	int nTabOrder    = 0;
	int panelWidth;
	int buttonWidth;

    // Create the panel
    errChk(hPanel = NewPanel (hParentPanel, LDBf(Ldb_BootPanelTitle), 90, 390, 150, 364));

    // Set the panel's attributes
    errChk(SetPanelAttribute (hPanel, ATTR_CONSTANT_NAME, "BOOT_PANEL"));
    errChk(SetPanelAttribute (hPanel, ATTR_CALLBACK_FUNCTION_POINTER, 0));
    errChk(SetPanelAttribute (hPanel, ATTR_TITLEBAR_VISIBLE, 1));
    errChk(SetPanelAttribute (hPanel, ATTR_BACKCOLOR, VAL_LT_GRAY));
    errChk(SetPanelAttribute (hPanel, ATTR_CAN_MINIMIZE, FALSE));
    errChk(SetPanelAttribute (hPanel, ATTR_CAN_MAXIMIZE, FALSE));
    errChk(SetPanelAttribute (hPanel, ATTR_SIZABLE,      FALSE));
    errChk(SetPanelAttribute (hPanel, ATTR_MOVABLE,      FALSE));
    if (hParentPanel){
        errChk(SetPanelAttribute (hPanel, ATTR_FRAME_STYLE,     VAL_RAISED_OUTLINED_FRAME));
        errChk(SetPanelAttribute (hPanel, ATTR_FRAME_THICKNESS, 10));
        errChk(SetPanelAttribute (hPanel, ATTR_FRAME_COLOR,     VAL_LT_GRAY));
        errChk(SetPanelAttribute (hPanel, ATTR_TITLE_BACKCOLOR, VAL_LT_GRAY));
        errChk(SetPanelAttribute (hPanel, ATTR_PARENT_SHARES_SHORTCUT_KEYS, 1));
    }
    // when ATTR_SYSTEM_MENU_VISIBLE == 0, 
    //   then ATTR_CLOSE_ITEM_VISIBLE = 0 regardless of its setting
    errChk(SetPanelAttribute (hPanel, ATTR_SYSTEM_MENU_VISIBLE, FALSE));
    errChk(SetPanelAttribute (hPanel, ATTR_CLOSE_ITEM_VISIBLE,  FALSE));

    // Build control: BOOT_PANEL_BOOTP_TEXT
    errChk(BOOT_PANEL_BOOTP_TEXT = NewCtrl (hPanel, CTRL_TEXT_BOX, "", 34, 43));
    errChk(SetCtrlAttribute (hPanel, BOOT_PANEL_BOOTP_TEXT, ATTR_CONSTANT_NAME, "BOOTP_TEXT"));
    errChk(SetCtrlAttribute (hPanel, BOOT_PANEL_BOOTP_TEXT, ATTR_HEIGHT,          22));
    errChk(SetCtrlAttribute (hPanel, BOOT_PANEL_BOOTP_TEXT, ATTR_WIDTH,           278));
    errChk(SetCtrlAttribute (hPanel, BOOT_PANEL_BOOTP_TEXT, ATTR_CTRL_MODE,       VAL_INDICATOR));
    errChk(SetCtrlAttribute (hPanel, BOOT_PANEL_BOOTP_TEXT, ATTR_FRAME_COLOR,     VAL_LT_GRAY));
    errChk(SetCtrlAttribute (hPanel, BOOT_PANEL_BOOTP_TEXT, ATTR_TEXT_FONT,       VAL_EDITOR_FONT));
    errChk(SetCtrlAttribute (hPanel, BOOT_PANEL_BOOTP_TEXT, ATTR_TEXT_POINT_SIZE, 18));
    errChk(SetCtrlAttribute (hPanel, BOOT_PANEL_BOOTP_TEXT, ATTR_TEXT_BGCOLOR,    VAL_WHITE));
    errChk(SetCtrlAttribute (hPanel, BOOT_PANEL_BOOTP_TEXT, ATTR_WRAP_MODE,       VAL_WORD_WRAP));
    errChk(SetCtrlAttribute (hPanel, BOOT_PANEL_BOOTP_TEXT, ATTR_TEXT_JUSTIFY,    VAL_CENTER_JUSTIFIED));
    errChk(SetCtrlAttribute (hPanel, BOOT_PANEL_BOOTP_TEXT, ATTR_DFLT_VALUE, ""));
    errChk(DefaultCtrl (hPanel, BOOT_PANEL_BOOTP_TEXT));

    // Build control: BOOT_PANEL_DECORATION
    errChk(BOOT_PANEL_DECORATION = NewCtrl (hPanel, CTRL_RAISED_FRAME, "", 20, 23));
    errChk(SetCtrlAttribute (hPanel, BOOT_PANEL_DECORATION, ATTR_CONSTANT_NAME, "DECORATION"));
    errChk(SetCtrlAttribute (hPanel, BOOT_PANEL_DECORATION, ATTR_FRAME_COLOR, VAL_LT_GRAY));
    errChk(SetCtrlAttribute (hPanel, BOOT_PANEL_DECORATION, ATTR_CTRL_MODE,   VAL_INDICATOR));
    errChk(SetCtrlAttribute (hPanel, BOOT_PANEL_DECORATION, ATTR_HEIGHT,      50));
    errChk(SetCtrlAttribute (hPanel, BOOT_PANEL_DECORATION, ATTR_WIDTH,       318));

	// Build Cancel bitmap button
	errChk(BOOT_PANEL_BOOTP_CANCEL = NewCtrl(hPanel, CTRL_PICTURE_COMMAND_BUTTON, 0, 85, 100));
	errChk(setButtonBMP(hPanel, BOOT_PANEL_BOOTP_CANCEL, BMP_Cancel192));
    errChk(SetCtrlAttribute(hPanel, BOOT_PANEL_BOOTP_CANCEL, ATTR_CALLBACK_FUNCTION_POINTER, BOOTPCancelCallback));
	errChk(SetCtrlAttribute(hPanel, BOOT_PANEL_BOOTP_CANCEL, ATTR_CMD_BUTTON_COLOR,          VAL_TRANSPARENT));
	errChk(SetCtrlAttribute(hPanel, BOOT_PANEL_BOOTP_CANCEL, ATTR_FIT_MODE,                  VAL_SIZE_TO_IMAGE));
	// Place the button in the middle:
	errChk(GetPanelAttribute(hPanel, ATTR_WIDTH, &panelWidth));
	errChk(GetCtrlAttribute(hPanel, BOOT_PANEL_BOOTP_CANCEL,   ATTR_WIDTH, &buttonWidth));
	errChk(SetCtrlAttribute(hPanel, BOOT_PANEL_BOOTP_CANCEL,   ATTR_LEFT, (panelWidth/2)-(buttonWidth/2)));

    // Build control: BOOT_PANEL_BOOTP_TIMEOUT
    errChk(BOOT_PANEL_BOOTP_TIMEOUT = NewCtrl (hPanel, CTRL_TIMER, "", 0, 0));
    errChk(SetCtrlAttribute (hPanel, BOOT_PANEL_BOOTP_TIMEOUT, ATTR_CONSTANT_NAME, "BOOTP_TIMEOUT"));
    errChk(SetCtrlAttribute (hPanel, BOOT_PANEL_BOOTP_TIMEOUT, ATTR_CALLBACK_FUNCTION_POINTER, calcTimeout));
    errChk(SetCtrlAttribute (hPanel, BOOT_PANEL_BOOTP_TIMEOUT, ATTR_INTERVAL,                  10.0));
    errChk(SetCtrlAttribute (hPanel, BOOT_PANEL_BOOTP_TIMEOUT, ATTR_ENABLED,                   FALSE));

    // Set up ZPLANE order
    errChk(SetCtrlAttribute (hPanel, BOOT_PANEL_BOOTP_TIMEOUT, ATTR_ZPLANE_POSITION, nZplaneOrder++));
    errChk(SetCtrlAttribute (hPanel, BOOT_PANEL_DECORATION,    ATTR_ZPLANE_POSITION, nZplaneOrder++));
    errChk(SetCtrlAttribute (hPanel, BOOT_PANEL_BOOTP_TEXT,    ATTR_ZPLANE_POSITION, nZplaneOrder++));
    errChk(SetCtrlAttribute (hPanel, BOOT_PANEL_BOOTP_CANCEL,  ATTR_ZPLANE_POSITION, nZplaneOrder++));

    // Set up TAB order
    errChk(SetCtrlAttribute (hPanel, BOOT_PANEL_BOOTP_CANCEL, ATTR_CTRL_TAB_POSITION, nTabOrder++));
    errChk(SetCtrlAttribute (hPanel, BOOT_PANEL_BOOTP_TEXT,   ATTR_CTRL_TAB_POSITION, nTabOrder++));

    // Finalize panel colors, positioning, and sizing
    errChk(SetPanelAttribute (hPanel, ATTR_CONFORM_TO_SYSTEM,        FALSE));
    errChk(SetPanelAttribute (hPanel, ATTR_SCALE_CONTENTS_ON_RESIZE, FALSE));

    //We're done!
    return hPanel;
Error:
//    if (bitmapId)
//        DiscardBitmap (bitmapId);
    return error;
}

//----------------------------------------------------------------------------------------------------
// BuildIMAG_PANEL ():  Build UI object IMAG_PANEL; return handle or standard UI error code.
//----------------------------------------------------------------------------------------------------
int BuildIMAG_PANEL (int hParentPanel)
{
    int hPanel;
    int error = 0;
    int bitmapId = 0; // needed if there are bitmaps 
    char *dataPtr = NULL; 
   	int panelHeight, panelWidth;
   	int buttonWidth;
	int nCtrlTop, nCtrlLeft, nCtrlHeight, nCtrlWidth;
	int nNewCtrlTop, nNewCtrlLeft, nNewCtrlHeight, nNewCtrlWidth;
	int nAdaptersAdj = 0;
	int nZplaneOrder = 0;
	int nTabOrder    = 0;

	if(MultiHomed){
		nAdaptersAdj = 115;
	}
    // Create the panel
    errChk(hPanel = NewPanel (hParentPanel, LDBf(Ldb_ImagPanelTitle), 131, 98, 320 + nAdaptersAdj, 450));

    // Set the panel's attributes
    errChk(SetPanelAttribute (hPanel, ATTR_CONSTANT_NAME, "IMAG_PANEL"));
    errChk(SetPanelAttribute (hPanel, ATTR_CALLBACK_FUNCTION_POINTER, 0));
    errChk(SetPanelAttribute (hPanel, ATTR_BACKCOLOR, VAL_LT_GRAY));
    errChk(SetPanelAttribute (hPanel, ATTR_CAN_MINIMIZE, FALSE));
    errChk(SetPanelAttribute (hPanel, ATTR_CAN_MAXIMIZE, FALSE));
    errChk(SetPanelAttribute (hPanel, ATTR_SIZABLE,      FALSE));
    errChk(SetPanelAttribute (hPanel, ATTR_MOVABLE,      TRUE));
    if (hParentPanel){
        errChk(SetPanelAttribute (hPanel, ATTR_FRAME_STYLE,        VAL_RAISED_OUTLINED_FRAME));
        errChk(SetPanelAttribute (hPanel, ATTR_FRAME_THICKNESS,    10));
        errChk(SetPanelAttribute (hPanel, ATTR_FRAME_COLOR,        VAL_LT_GRAY));
        errChk(SetPanelAttribute (hPanel, ATTR_TITLE_FONT,         VAL_DIALOG_META_FONT));
        errChk(SetPanelAttribute (hPanel, ATTR_TITLE_POINT_SIZE,   15));
        errChk(SetPanelAttribute (hPanel, ATTR_TITLEBAR_THICKNESS, 19));
        errChk(SetPanelAttribute (hPanel, ATTR_TITLE_BACKCOLOR,    VAL_LT_GRAY));
    }
    errChk(SetPanelAttribute (hPanel, ATTR_MIN_HEIGHT_FOR_SCALING, 0));
    errChk(SetPanelAttribute (hPanel, ATTR_MIN_WIDTH_FOR_SCALING,  0));
    // when ATTR_SYSTEM_MENU_VISIBLE == 0, 
    //   then ATTR_CLOSE_ITEM_VISIBLE = 0 regardless of its setting
    errChk(SetPanelAttribute (hPanel, ATTR_SYSTEM_MENU_VISIBLE,    TRUE));
    errChk(SetPanelAttribute (hPanel, ATTR_CLOSE_ITEM_VISIBLE,     TRUE));

    // Build control: IMAG_PANEL_VENTSFTWR_BOX
    errChk(IMAG_PANEL_VENTSFTWR_BOX = NewCtrl (hPanel, CTRL_RECESSED_FRAME, "", 15, 9));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_VENTSFTWR_BOX, ATTR_CONSTANT_NAME, "VENTSFWR_BOX"));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_VENTSFTWR_BOX, ATTR_HEIGHT,      75));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_VENTSFTWR_BOX, ATTR_WIDTH,       435));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_VENTSFTWR_BOX, ATTR_CTRL_MODE,   VAL_INDICATOR));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_VENTSFTWR_BOX, ATTR_FRAME_COLOR, VAL_LT_GRAY));

    // Build control: IMAG_PANEL_VENTSFTWR_BOX_LBL
    errChk(IMAG_PANEL_VENTSFTWR_BOX_LBL = NewCtrl (hPanel, CTRL_TEXT_MSG, "", 7, 18));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_VENTSFTWR_BOX_LBL, ATTR_CONSTANT_NAME, "VENTSFTWR_BOX_LBL"));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_VENTSFTWR_BOX_LBL, ATTR_CTRL_MODE,       VAL_INDICATOR));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_VENTSFTWR_BOX_LBL, ATTR_TEXT_COLOR,      VAL_BLACK));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_VENTSFTWR_BOX_LBL, ATTR_TEXT_FONT,       "Arial"));
	errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_VENTSFTWR_BOX_LBL, ATTR_TEXT_POINT_SIZE, 11));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_VENTSFTWR_BOX_LBL, ATTR_TEXT_BGCOLOR,    VAL_LT_GRAY));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_VENTSFTWR_BOX_LBL, ATTR_HEIGHT,          16));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_VENTSFTWR_BOX_LBL, ATTR_WIDTH,           70));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_VENTSFTWR_BOX_LBL, ATTR_SIZE_TO_TEXT,    TRUE));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_VENTSFTWR_BOX_LBL, ATTR_TEXT_JUSTIFY,    VAL_LEFT_JUSTIFIED));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_VENTSFTWR_BOX_LBL, ATTR_DFLT_VALUE,      LDBf(Ldb_840Vent_Name)));
    errChk(DefaultCtrl (hPanel, IMAG_PANEL_VENTSFTWR_BOX_LBL));

    // Build control: IMAG_PANEL_840_GUISOFTWARE_LBL
    errChk(IMAG_PANEL_840_GUISOFTWARE_LBL = NewCtrl (hPanel, CTRL_TEXT_MSG, "", 30, 30));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_840_GUISOFTWARE_LBL, ATTR_CONSTANT_NAME, "840_GUISOFTWARE_LBL"));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_840_GUISOFTWARE_LBL, ATTR_CTRL_MODE,       VAL_INDICATOR));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_840_GUISOFTWARE_LBL, ATTR_TEXT_COLOR,      VAL_BLACK));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_840_GUISOFTWARE_LBL, ATTR_TEXT_FONT,       "Arial"));
	errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_840_GUISOFTWARE_LBL, ATTR_TEXT_POINT_SIZE, 11));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_840_GUISOFTWARE_LBL, ATTR_TEXT_BGCOLOR,    VAL_LT_GRAY));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_840_GUISOFTWARE_LBL, ATTR_HEIGHT,          16));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_840_GUISOFTWARE_LBL, ATTR_WIDTH,           91));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_840_GUISOFTWARE_LBL, ATTR_SIZE_TO_TEXT,    TRUE));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_840_GUISOFTWARE_LBL, ATTR_TEXT_JUSTIFY,    VAL_LEFT_JUSTIFIED));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_840_GUISOFTWARE_LBL, ATTR_DFLT_VALUE,      LDBf(Ldb_840_GUIsoftware_Lbl)));
    errChk(DefaultCtrl (hPanel, IMAG_PANEL_840_GUISOFTWARE_LBL));
    errChk(GetCtrlAttribute (hPanel, IMAG_PANEL_840_GUISOFTWARE_LBL, ATTR_WIDTH, &nCtrlWidth));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_840_GUISOFTWARE_LBL, ATTR_LEFT,  211 - 15 - nCtrlWidth));

    // Build control: IMAG_PANEL_840_GUISOFTWARE
    errChk(IMAG_PANEL_840_GUISOFTWARE = NewCtrl (hPanel, CTRL_TEXT_MSG, "", 30, 211));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_840_GUISOFTWARE, ATTR_CONSTANT_NAME, "840_GUISOFTWARE"));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_840_GUISOFTWARE, ATTR_CTRL_MODE,       VAL_INDICATOR));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_840_GUISOFTWARE, ATTR_TEXT_COLOR,      VAL_BLACK));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_840_GUISOFTWARE, ATTR_TEXT_FONT,       "Arial"));
	errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_840_GUISOFTWARE, ATTR_TEXT_POINT_SIZE, 11));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_840_GUISOFTWARE, ATTR_TEXT_BGCOLOR,    VAL_LT_GRAY));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_840_GUISOFTWARE, ATTR_HEIGHT,          16));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_840_GUISOFTWARE, ATTR_WIDTH,           24));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_840_GUISOFTWARE, ATTR_SIZE_TO_TEXT,    TRUE));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_840_GUISOFTWARE, ATTR_TEXT_JUSTIFY,    VAL_LEFT_JUSTIFIED));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_840_GUISOFTWARE, ATTR_DFLT_VALUE,      "- - -"));
    errChk(DefaultCtrl (hPanel, IMAG_PANEL_840_GUISOFTWARE));

    // Build control: IMAG_PANEL_840_BDSOFTWARE_LBL
    errChk(IMAG_PANEL_840_BDSOFTWARE_LBL = NewCtrl (hPanel, CTRL_TEXT_MSG, "", 60, 32));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_840_BDSOFTWARE_LBL, ATTR_CONSTANT_NAME, "840_BDSOFTWARE_LBL"));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_840_BDSOFTWARE_LBL, ATTR_CTRL_MODE,       VAL_INDICATOR));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_840_BDSOFTWARE_LBL, ATTR_TEXT_COLOR,      VAL_BLACK));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_840_BDSOFTWARE_LBL, ATTR_TEXT_FONT,       "Arial"));
	errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_840_BDSOFTWARE_LBL, ATTR_TEXT_POINT_SIZE, 11));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_840_BDSOFTWARE_LBL, ATTR_TEXT_BGCOLOR,    VAL_LT_GRAY));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_840_BDSOFTWARE_LBL, ATTR_HEIGHT,          16));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_840_BDSOFTWARE_LBL, ATTR_WIDTH,           88));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_840_BDSOFTWARE_LBL, ATTR_SIZE_TO_TEXT,    TRUE));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_840_BDSOFTWARE_LBL, ATTR_TEXT_JUSTIFY,    VAL_LEFT_JUSTIFIED));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_840_BDSOFTWARE_LBL, ATTR_DFLT_VALUE,      LDBf(Ldb_840_BDUsoftware_Lbl)));
    errChk(DefaultCtrl (hPanel, IMAG_PANEL_840_BDSOFTWARE_LBL));
    errChk(GetCtrlAttribute (hPanel, IMAG_PANEL_840_BDSOFTWARE_LBL, ATTR_WIDTH, &nCtrlWidth));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_840_BDSOFTWARE_LBL, ATTR_LEFT,  211 - 15 - nCtrlWidth));

    // Build control: IMAG_PANEL_840_BDSOFTWARE
    errChk(IMAG_PANEL_840_BDSOFTWARE = NewCtrl (hPanel, CTRL_TEXT_MSG, "", 60, 211));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_840_BDSOFTWARE, ATTR_CONSTANT_NAME, "840_BDSOFTWARE"));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_840_BDSOFTWARE, ATTR_CTRL_MODE,       VAL_INDICATOR));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_840_BDSOFTWARE, ATTR_TEXT_COLOR,      VAL_BLACK));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_840_BDSOFTWARE, ATTR_TEXT_FONT,       "Arial"));
	errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_840_BDSOFTWARE, ATTR_TEXT_POINT_SIZE, 11));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_840_BDSOFTWARE, ATTR_TEXT_BGCOLOR,    VAL_LT_GRAY));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_840_BDSOFTWARE, ATTR_HEIGHT,          16));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_840_BDSOFTWARE, ATTR_WIDTH,           21));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_840_BDSOFTWARE, ATTR_SIZE_TO_TEXT,    TRUE));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_840_BDSOFTWARE, ATTR_TEXT_JUSTIFY,    VAL_LEFT_JUSTIFIED));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_840_BDSOFTWARE, ATTR_DFLT_VALUE,      "- - -"));
    errChk(DefaultCtrl (hPanel, IMAG_PANEL_840_BDSOFTWARE));

    // Build control: IMAG_PANEL_840_AUTODET_STATUS
    errChk(IMAG_PANEL_840_AUTODET_STATUS = NewCtrl (hPanel, CTRL_TEXT_MSG, "", 45, 100));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_840_AUTODET_STATUS, ATTR_CONSTANT_NAME, "840_AUTODET_STATUS"));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_840_AUTODET_STATUS, ATTR_CTRL_MODE,       VAL_INDICATOR));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_840_AUTODET_STATUS, ATTR_TEXT_COLOR,      VAL_BLACK));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_840_AUTODET_STATUS, ATTR_TEXT_FONT,       "Arial"));
	errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_840_AUTODET_STATUS, ATTR_TEXT_POINT_SIZE, 11));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_840_AUTODET_STATUS, ATTR_TEXT_BGCOLOR,    VAL_LT_GRAY));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_840_AUTODET_STATUS, ATTR_HEIGHT,          16));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_840_AUTODET_STATUS, ATTR_WIDTH,           133));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_840_AUTODET_STATUS, ATTR_SIZE_TO_TEXT,    TRUE));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_840_AUTODET_STATUS, ATTR_TEXT_JUSTIFY,    VAL_LEFT_JUSTIFIED));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_840_AUTODET_STATUS, ATTR_DFLT_VALUE,      LDBf(Ldb_840_NoDetect)));
    errChk(DefaultCtrl (hPanel, IMAG_PANEL_840_AUTODET_STATUS));

    // Build control: IMAG_PANEL_VENT_CAL_BUTTON
    errChk(IMAG_PANEL_VENT_CAL_BUTTON = NewCtrl (hPanel, CTRL_PICTURE_COMMAND_BUTTON, "", 25, 380));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_VENT_CAL_BUTTON, ATTR_CONSTANT_NAME, "VENT_CAL_BUTTON"));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_VENT_CAL_BUTTON, ATTR_CALLBACK_FUNCTION_POINTER, ImagePanelCallback));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_VENT_CAL_BUTTON, ATTR_SHORTCUT_KEY, 0));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_VENT_CAL_BUTTON, ATTR_CTRL_MODE,  VAL_HOT));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_VENT_CAL_BUTTON, ATTR_DFLT_VALUE, 0));
    errChk(DefaultCtrl (hPanel, IMAG_PANEL_VENT_CAL_BUTTON));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_VENT_CAL_BUTTON, ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_VENT_CAL_BUTTON, ATTR_FIT_MODE, VAL_SIZE_TO_IMAGE));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_VENT_CAL_BUTTON, ATTR_HEIGHT, 51));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_VENT_CAL_BUTTON, ATTR_WIDTH,  51));
    dataPtr = 
        "!!!!!$!!-3\?C!<!!!!>!!!<9!!!<!!!!!!!!!Q!Q!!9!!!!!0!``Q`!!8/!!_!!_O`!YO_!QO\\\"FNZ"
        "#/+(;X[%FZ[#FZ[#Q\\[\"Y_4/@!`!!!LY_!LFZ#L;X%L+S&<`P'2/+ZNP)`OP'+OS&;OX%QO\\\"_E`"
        "/;`!!![!Y_[!FZ[#+SW&ZPU)/1<YN(<`P'L;X%LQ\\\"J_`/07!!_!!_O`!FOZ#+NS&/'%XMW'P\?W#C*"
        "FZ7XFR-HFJ%<BE_3B@\\.B>_3F@%<FD-HFJ7XGRC*GZQ\?U#/$<XM'<WK'<`P'L3O\"LQ\\\"4/5!_!!Q"
        "O\\\"3NO\"XNM'WNK'VNK'UNF'BJ)Y09ML`93A]90\?]2/\"/B\?]0B\?\\/'#/\?']0\?%]/\"9/>]2/\"/B"
        "\?]0B\?]/'\"/>%]/\"9/>`:4A0:MMCJ)YPNA\"VNK'WNJ'VNK'`OP';OX%_E`/2`!!![!FZW#^HW#TIW&"
        "TIW&QDW#@&FX+FBI^1B\?^1B@^1B\?^1B\?^0B\?^1B@]1B@^1E\?/$'^1@'^1\?%]/%91\?^:0\?+:FJ@J&XPNA"
        "\"TNI&UNG%VOI&3OO\"YE_/0`!!![!FZW#^HW#TGW$TGW%H4F^2PBM_3BA_3BA_3B@_23/&@'_3@'_2@"
        "'_3@'_2A'_3A'_3@'_3@'_2@'_3@'_3@'_2@'_3@'_3@72PN;H4^<TG%<TG%<TG$L3O\"JY_/0.!!_!!"
        "FNZ#^NH#QND#QND#AJ&X):AG!:5A!95B`95A`95B`:5A!:5A363;3633363;+63D+63D+63\\+63\\#9"
        "3%`:5A!:4A!:4A!95A`2/$5FA!5BA`5FA)@GGA'EY/$<QD#L3O\"JY_/0,!!_!!MJU^^NH#PNA\"PNA\""
        "<:`U#::C\":7C!2/%7FB\"7FB+36\\3313))!)11!199!9BB!BJJ!JRR&R##6###6#33633363336;+3"
        "6D+36D+36\\3LF1\"7FB\"7FC$:FC<`EU/$<PA\"L3O\"J_`/0*!!_!!YN_!^NH#MJ=`ON\?!<:`U$2/$"
        "9FC#:FC$:FC$9FC#9FC#9FD331;RR%R/+!!!!!999!999!RRR6;;;;\\%55$/$:9C#::C<:`U/'$O\?[!"
        "FZU#/*`!!!L3O\";M=`;M=`;\?%X7'\?E7%<D7%;E7%<E7%<D7%<E7%<D7%<E6+3\\6333!)))1/-!\"!!"
        "#6##T6TT$:<D$:<D%:<D%:;E%:<E':\?F\?J%XMJ=`MJ=`YKA_QN\\\"/0(!![!MUG^YAG_L:G_E-F\\-G"
        "FI'>FF'\?FF'>FF&\?FF'>FF'>FF'\?FE&>FF331;RR%R/-!!!!!9996;;;;\\%57'>F7'>F7&>F7&>E7'>"
        "F7'>F7-HI;D-[;L:_;L:_L3O\"4/'!_!!_O`!3NO\"JJ7]GJ3[5:TP(:AG(:AG(:AF):AG):AG(:AG):"
        "AF(:AF+6;\\3533)\"))/!-!!&!##6#TT6T=ZP)PP0)47&\?)BFG(AFG)AFG)AFG(AFG5UGPG3G[J7G]R"
        "9K^MUE^/&`!!!KMU^;R9^;I4\\;>#V7*DH7*DH6+C/*$H*:DI*:DH*:DH+:DH353;R\"RR/!-!!!!99&"
        "9;;7;\\%F5;RP)PP0)`X0!\?C%5/%7*DH7+CH;>\"V;H4];H4]L(F'J_`/0$!!_!!_O`!3NO\"FJ1[CJ,"
        "X3:RN,:GI,:FJ,:GJ,:FJ,:GJ/&$,GFI+;6\\3313))%)/-!!!!6###6TTT7;LF7,GI73L=]47\?]`X!]"
        "47\?7,FI7-GI7,GI7,FI73RN;C-Y;F1[;R9^LQ\\\"4/$!_!![J\\`ZN<\"EJ/Z>J#W.5J/$FK.IEJ/$7"
        ".JK7.IK7-JK633;!RRR1/-!!!!1\"11;6;;\\J%5.:JK.:JK.:IK+<D-PTP)`RX!T6TT.:JK.:IK.:JK"
        ".:JK9:^RDJ/YFK1ZBJIZ/0$!![!MUG^F/GXB+FX7WFQ0MFM0LEL/\"7/LM70ML60M/*$L0:LM=:Z)363"
        "33633#5##R!RRJ!JJ9!999\"99/!(!!!!RR&RTT6TD\\FF0MFL0MFM0MFL#3K-BO@%`X0!++&9#;F5#D"
        "F=0MFL0LFL7WGQC,GXB+KX!=K[_``!!!`!!![!7DGZA)GWA(FX2QFO1OFM2OFN2PFM2PFM2PFN2OFM;R"
        "F)##6###6#;;6;TT6TLL6LTT;T55F5TT6TLL6L33633363##1#RR!RJJ!J99!999&93373_'E-/$73PN"
        "72PN7#;-6#3L]`X!]`X!63DP7#;572PM71PN73PO;A(Y;@)W;S2WLQ\\\"`!!!`!!!K!=[;>&U;=\"V7"
        "4RO74SO74SO7D\\NK%5N6#++!9991/%!!!!1!11!!!!1!11!!!!R!RRR\"RR;6;;L6LL_J'-;6;;_J'-"
        "T6TTD6DDL6LLL:\\F5:SO5:SO4:RO4:RO#9D5Z83\\+T+9`SX!BEO%Z:7)3:RN4:SO3:RO<J!V>J&UGK"
        "*WBLIZ!`!!!^!!\\J2\\=J#T::^T5;UP%K5N^`'/!W)TVKVV=J==T6TT;5;;R\"RR/!%!!!!11!1!!!!"
        "99!911%1/%!!!!!)))63336DDD67CP2/$6:VP6:VQ6:UP5:UP#;D5BHO%`SX!^A^9Z6+\\+:H>5:UQ7:"
        "XQ;:]T<J$TCK'V7LDZ!`!!!^!!SJ2W;J!S9:\\S8;XR5L55!X)T`TX!!W)TNKNNNJNN_I'-J\"JJ/!$!"
        "!!!99&9DD6DDD1DRR!R11!111!1))!)JJ&J##6#336333633;6P3LF17XFQ8XFQ7XFR7XFQ7XFQ7XFR+"
        "LG=V+@A`X0!77\"1R+6L+HF;3RFI8YFR9\\GS;!GS=\"KT7DPZ!!`!!!W!P*FS9^FR:\\FS:[PS',0Z`"
        "X0!`X0!PH%1/&K555;_'-6TTTKNNNKNNNK^^^K^^^K===6TTT6333633363AA73D%2/+9:[S=<Z)`TX!"
        "`QX!R5#D_:8#0:OB9:[S::\\T9:^R:;_R7LDZ!`!!!^!!PJ*S8:\\Q;<]U'R,Z/0%`X'!^16X3TEF/$7"
        ";^TK-5=K-5=3/$5J55_J'-363336;;/&\"/H6`:^FS;^ES/%7;]T2/%;:^T;:]T;:]T;:]U3<TF`TX!`"
        "QX!R5#D^67\\1:QA::]R::^T8:\\Q8;\\Q7LDZ!`!!!^!!PJ*S6:YO<K!V5GNLBFO%/0$`X0!47&\?+LF"
        "=3TGF=!GV=!FV<`FU=`EV/$7=`U7<`U6=`/*%U=E!/&FU=`EV/$7<`U7=`V7=`U7=`T73TF]`X!]`X!&"
        "R#D&Z3L7.N;7=`U7<`U76YO78[PK/:U`!!!`!!!;P*S76XO;>#V;\?#W73\\N]`X!]`X!&Z3\\6#;L7#;"
        "%7+L=;@%X;\?$W:\?$/*&W\?E#/$GW\?$GW\?#GW\?#GW>#GW>#GV>#GW\?#GW>\"FV;^EP/$;>#W;\?\"U;\?\"V"
        "7;\\N]`X!]`X!&R+D&R+L7'G17<`R;>#W74VN76XOK/:U`!!!`!!!;P*S74VN;\?#W;@%X73TF]`X!]`X"
        "!&Z+3\"JZ3&R+D7#D-;@%X;@&X;@&X7;\\N3/*=K==5K55=K===J==_I'-9\"990:O;>J#U@J&XAJ'Y@"
        "J&X@J%WEL+;`TX!PQP)R5+DR6+L$:E(=J!R=J\"V2:SM4;VN/L:U!`!!!^!!PJ*S1:RL>J$WBJ)Y3<TF"
        "4T7\?`RX!L1TRB%R+R6#;#:;%AJ(YAJ(YBJ(Y;;\\N^L^^`^``/0(PH`X````PH`XPH`X``Q`99&9+NG/"
        "=\"FPA/+$(Y;B(YJ5NL]`X!]47\?&R+D&R+L7#D%;>#R;\?$W71RL77UMK7DZ`!!!`!!!K$1R2/\"/:PJ="
        "J!VDJ+[3<\\F'T,Z`SX!EAPNJ%Z+R6#D#:D%DJ+ZDJ+[DJ+[3;\\FNLNN`^``>:(5+1/$BE%3BE%+BF%"
        ">(P551U;#9E%#9K%-!AK11\"9`=6\\;`GKD*GWF,GU;%GVD+PZ',0Z`X+!BO2%R+2LZ36L&FG(\?%FT<`"
        "EU/\"7/PJ77UMKBIZ`!!!`!!!K/:U7.MI76XO;E.\\;;%NJ5FD]`X!]PP)&Z+3&Z3T7+N/;F.\\;E.\\"
        ";E.\\73\\FKNNN````3+B%\"R)\\#AT,#AT,#BN-\"R)\\3;R1`51;\"J!L\"J!TK-!K!111&Z3T7;^H"
        ";H,Q;Z8<;Z7;;F.\\]PP)]`X!6TTT&Z3L&Z3L7+D-;@&T7:]S7.MI7AWPKMU^`!!!`!!!KBIZ2/\"/:M"
        "J0:PKFJ/\\GJ1^;<\\F\?TC5`SX!EBPN#6;\\3:T=HJ1]GJ0]GJ0]3;\\=NLNN`^``3)B%R)9%!9LV!9L"
        "V)9;NR*9%;,R15]1;J%!TJ'!T-I!K1!11Z63L/&\"/LG2J+KD$;P\"`X0!470\?`X0!47&\?#DB%Z32L_;"
        "6V+NF/<`FO4UFO,JGHP*KS_``!!!`!!![!TVF]KZFJ*FGGA'GXI3G_;%KN5F@D`X0!PP&);Q6T3\\G=J"
        "4G^J3G^J4F_3\\K=NNPN``U`3BA%R9B%!LBV!LBV);ANR9E%;JP-PH[X-!KK9)KRNFA^11\"1R+6D,GG"
        ".K,KG*>5]/$]`X!J5FD6#D\\&Z3L6%@Z;;!H;\?%V2/\"/:LL,;JH/J:U/0$!![!_`[!$1FR'BFE9\\GS"
        "K6G_D-FN3TP=++09`X0!77&1;\\G3G*GKK7G`I4F\\;`K=NNPN``U`3JA-Z)5\\/$#BN-\"Z)\\33J-`"
        "````XXX`HHHKVVV!111&R+D7,H.;M/IK;B'4/%`SX!BFO%*6CO#6D\\9:]G::[S):EF4;PHMJU^/0%!!"
        "[!BIEZ/\"7/FF2/\"/:LKEJ.\\NN9!;J%F;8QTPTP)`TX!'R,ZFJ,\?JJ5[KJ6^HK)DNLNN`^``3)J-Z&"
        ")\\/!$NFA)Z)5\\3JP-XX[X^^F^LL6L##1#))\")Z36D-PG0N2KM8@P,`X0!`X0!47+\?BO6%3A6A#=7R"
        "<!GF>$FS*FFG'BFDQ^KK_`4/%!`!!!K[\\`7Q^K7&\?C76XO<O<\";D-N2/\"/:T5L8LDPSP)EBPN3:\\"
        "=HJ3WMJ9^HK-JNLNN`^``3)B%R)9%!9LV!9DN)9;NR*9%3,J-P^PPT5TT)!))1!9B9!BJZ6;L0:V6LJ4"
        "S^K<;5FNL=:Z)#5;TR5+DR5#;Z63L::^E::\\S&:\?C,;EEBJIZ/0'!![!BIEZ/\"7/FF7&\?C;D+Z<P\?$"
        ";;%F6+F_&Z3L64OI73L%;G1R;L8[;H-JKNNN````33B%#R9%'!T^'!LV'!DN#R9%33J-`PPP6TTT!199"
        "\"JZ+&R+D6$E]73\\=;L8Y<Q\?#;\?-J7+N/&Z3D&J#+&J#36#;L7<`N7&\?C7$<B7Q^KJ_`/0'!!_!![J\\"
        "`[J&M\"::A.:LELN8!DJ-O;:\\3*9K#`6=T4:X0KJ7WNJ:];K%FNLNN`^``3)B%N%5`)93F)93F19+FN"
        "&5`3,B%P^PP_I'-1!9BR6#3#6;T.:Q0MJ:\\PJ\?`NJ=^@J,J19P)Z53DJ5#3R6#3#6;T2:QN\"::A-;C"
        "CBJIZ/0)!![!MUF^>MBE`7F\?8YGRRAW%M;E\\/\"7/T52/\"/:S*=J&@QN@!QJ\?_IK.KNLNN`^``;*R1"
        "#%9/$E%#BE%#9E%;RP1TP[X55A59B\"JZ36D+F6_;`G=O<G\\TCW$P>G_F0FO9]F6#D2TZ;6L#;2TZ32"
        "\\`7B\?`7K\?$1ER/+`!!!K7DZ7,>@'^5>7<_U;N=`;\?-J;K9W;N<[<SC#;T@^;S5NKNNN`XXXKF=N3/&9"
        "K)RFL=NH_HHNINN1!19Z6+3#6;L::^:PJ>]VNG(TNE%NJ;ZAJ.K;:^76:W2#9D%^94>^:4>Q;^K[J\\`"
        "/0,!![!/:FU$8B<\\2F<<_GTVGW'XIW(WIW(WFW&TBG`R5KMVVPV51V;LL5L/&6LLD6DDD6TTTKF=N;_"
        "'-;_'-!11177Y3;Q\?]<XI)<XI(<TD$<SC#;M:]7<_T']2='\\2=7DPEKTV]4/-!_!!_O`!$J1R$98<[:"
        ".<7:YQQN\?$ZNL*WNH(SJC`GJ-J;6;;T6TTT6TT/+(==L=^'`/XX`XHHQH99&95TG*Q\?G\\ZLW)YLW*YL"
        "W*M9V!7YBQ[0B<[0F<>MKEMUE^//`!!!L_`!K/:U7,>@'Y-:2/\"/:LKFJ1\\[NO+QJ@`EI2K`55EN\""
        "X`D6DD/+*55G5_'A-11&1-G7]O=GZXKW)[NW+G0F].LBKY-B:Y-F:KZKJTVE]/2`!!!K7DZ7>ME'X+:'"
        "[0<77YR;I4^<RB\";D,H&R#3!\?EH1/,)\"))+6F_IJ4VMN:!79XR[90<V:*9\":3<[K&M[J\\`/04!!["
        "!TVG][&FM$8B<V*B9^4F>7WGPC*GWF0FP#D2TZ32DR+2/(3&R#3&R+;6#;L6#;T72QJ'^4>'V*9'[,97"
        ";HCK/:UJ_`/06!!_!!_O`!BJIZQ:^K$98<X9)8U:'7)9BCZ53\\Z53TZ53LZ2/$;6L#;2LZ32LZ32DR+"
        "2DJ#6D(@BFU'B7U'B7[,F95DKA$1KRTVE]/;`!!!KBIZ;[&M7;HC7\"3<'[,91/*U9'7X9)8_:0:,:>@"
        "Q;^K/K:UTJV]/0\?!![![\\K`BIKZ$1FRQ^EK/(7DPE7KZJ;[&MK7DZKTV]8/\"!]!!";
    errChk(GetUserBitmapFromText(dataPtr, 102, &bitmapId));
    if (bitmapId){
        errChk(SetCtrlBitmap (hPanel, IMAG_PANEL_VENT_CAL_BUTTON, 0, bitmapId));
//        SaveBitmapAsCode("c:\\brthlab\\840vts\\bmps\\bitmaps\\pc840_icon192.bmp", bitmapId, "PC_840_Icon_192");
        DiscardBitmap(bitmapId);
        bitmapId = 0;
    }

	if(MultiHomed){
     // Build control: IMAG_PANEL_ADAPTER_LIST
    errChk(IMAG_PANEL_ADAPTER_LIST = NewCtrl (hPanel, CTRL_LIST, "", 142, 9));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_ADAPTER_LIST, ATTR_CONSTANT_NAME, "IMAG_LIST"));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_ADAPTER_LIST, ATTR_CALLBACK_FUNCTION_POINTER, 0));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_ADAPTER_LIST, ATTR_SHORTCUT_KEY,             0));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_ADAPTER_LIST, ATTR_HEIGHT,                   86));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_ADAPTER_LIST, ATTR_WIDTH,                    410));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_ADAPTER_LIST, ATTR_CTRL_MODE,                VAL_HOT));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_ADAPTER_LIST, ATTR_FRAME_COLOR,              VAL_LT_GRAY));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_ADAPTER_LIST, ATTR_SCROLL_BARS,              VAL_VERT_SCROLL_BAR));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_ADAPTER_LIST, ATTR_SCROLL_BAR_COLOR,         VAL_LT_GRAY));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_ADAPTER_LIST, ATTR_DATA_TYPE,                VAL_INTEGER));
//  errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_ADAPTER_LIST, ATTR_ALLOW_ROOM_FOR_IMAGES,    0));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_ADAPTER_LIST, ATTR_TEXT_FONT,                VAL_DIALOG_FONT));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_ADAPTER_LIST, ATTR_TEXT_POINT_SIZE,          13));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_ADAPTER_LIST, ATTR_TEXT_BOLD,                TRUE));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_ADAPTER_LIST, ATTR_TEXT_BGCOLOR,             VAL_WHITE));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_ADAPTER_LIST, ATTR_VISIBLE_LINES,            4));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_ADAPTER_LIST, ATTR_LABEL_TOP,                110));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_ADAPTER_LIST, ATTR_LABEL_LEFT,               96));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_ADAPTER_LIST, ATTR_LABEL_HEIGHT,             22));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_ADAPTER_LIST, ATTR_LABEL_WIDTH,              246));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_ADAPTER_LIST, ATTR_LABEL_SIZE_TO_TEXT,       TRUE));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_ADAPTER_LIST, ATTR_LABEL_FONT,               VAL_DIALOG_FONT));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_ADAPTER_LIST, ATTR_LABEL_POINT_SIZE,         17));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_ADAPTER_LIST, ATTR_LABEL_BOLD,               FALSE));
//  errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_ADAPTER_LIST, ATTR_LABEL_VISIBLE,            TRUE));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_ADAPTER_LIST, ATTR_LABEL_BGCOLOR,            VAL_LT_GRAY));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_ADAPTER_LIST, ATTR_LABEL_TEXT,               LDBf(Ldb_ImagPnlIPlist_lbl)));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_ADAPTER_LIST, ATTR_CHECK_MODE,               0));
//  errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_ADAPTER_LIST, ATTR_CHECK_STYLE,              VAL_CHECK_MARK));
//  errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_ADAPTER_LIST, ATTR_TEXT_CLICK_TOGGLES_CHECK, TRUE));
	}

     // Build control: IMAG_PANEL_IMAG_LIST
    errChk(IMAG_PANEL_IMAG_LIST = NewCtrl (hPanel, CTRL_LIST, "", 142 + nAdaptersAdj, 9));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_IMAG_LIST, ATTR_CONSTANT_NAME, "IMAG_LIST"));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_IMAG_LIST, ATTR_CALLBACK_FUNCTION_POINTER, 0));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_IMAG_LIST, ATTR_SHORTCUT_KEY,             0));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_IMAG_LIST, ATTR_HEIGHT,                   86));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_IMAG_LIST, ATTR_WIDTH,                    410));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_IMAG_LIST, ATTR_CTRL_MODE,                VAL_HOT));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_IMAG_LIST, ATTR_FRAME_COLOR,              VAL_LT_GRAY));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_IMAG_LIST, ATTR_SCROLL_BARS,              VAL_VERT_SCROLL_BAR));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_IMAG_LIST, ATTR_SCROLL_BAR_COLOR,         VAL_LT_GRAY));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_IMAG_LIST, ATTR_DATA_TYPE,                VAL_INTEGER));
//  errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_IMAG_LIST, ATTR_ALLOW_ROOM_FOR_IMAGES,    0));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_IMAG_LIST, ATTR_TEXT_FONT,                VAL_DIALOG_FONT));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_IMAG_LIST, ATTR_TEXT_POINT_SIZE,          13));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_IMAG_LIST, ATTR_TEXT_BOLD,                TRUE));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_IMAG_LIST, ATTR_TEXT_BGCOLOR,             VAL_WHITE));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_IMAG_LIST, ATTR_VISIBLE_LINES,            6));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_IMAG_LIST, ATTR_LABEL_TOP,                110 + nAdaptersAdj));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_IMAG_LIST, ATTR_LABEL_LEFT,               96));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_IMAG_LIST, ATTR_LABEL_HEIGHT,             22));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_IMAG_LIST, ATTR_LABEL_WIDTH,              246));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_IMAG_LIST, ATTR_LABEL_SIZE_TO_TEXT,       TRUE));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_IMAG_LIST, ATTR_LABEL_FONT,               VAL_DIALOG_FONT));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_IMAG_LIST, ATTR_LABEL_POINT_SIZE,         17));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_IMAG_LIST, ATTR_LABEL_BOLD,               FALSE));
//  errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_IMAG_LIST, ATTR_LABEL_VISIBLE,            TRUE));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_IMAG_LIST, ATTR_LABEL_BGCOLOR,            VAL_LT_GRAY));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_IMAG_LIST, ATTR_LABEL_TEXT,               LDBf(Ldb_ImagPnlImaglist_lbl)));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_IMAG_LIST, ATTR_CHECK_MODE,               0));
//  errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_IMAG_LIST, ATTR_CHECK_STYLE,              VAL_CHECK_MARK));
//  errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_IMAG_LIST, ATTR_TEXT_CLICK_TOGGLES_CHECK, TRUE));

	// Build OK bitmap button
	errChk(IMAG_PANEL_LANG_DONE = NewCtrl(hPanel, CTRL_PICTURE_COMMAND_BUTTON, 0, 256 + nAdaptersAdj, 129));
	setButtonBMP(hPanel, IMAG_PANEL_LANG_DONE, BMP_Okay192);	
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_LANG_DONE, ATTR_CALLBACK_FUNCTION_POINTER, ImagePanelCallback));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_LANG_DONE, ATTR_SHORTCUT_KEY,              0));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_LANG_DONE, ATTR_CTRL_MODE,                 VAL_HOT));
	errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_LANG_DONE, ATTR_CMD_BUTTON_COLOR,          VAL_TRANSPARENT));
	errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_LANG_DONE, ATTR_FIT_MODE,                  VAL_SIZE_TO_IMAGE));
	
	// Build Cancel bitmap button
	errChk(IMAG_PANEL_LANG_CANCEL = NewCtrl(hPanel, CTRL_PICTURE_COMMAND_BUTTON, 0, 256 + nAdaptersAdj, 234));
	setButtonBMP(hPanel, IMAG_PANEL_LANG_CANCEL, BMP_Cancel192);	
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_LANG_CANCEL, ATTR_CALLBACK_FUNCTION_POINTER, ImagePanelCallback));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_LANG_CANCEL, ATTR_SHORTCUT_KEY,              0));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_LANG_CANCEL, ATTR_CTRL_MODE,                 VAL_HOT));
	errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_LANG_CANCEL, ATTR_CMD_BUTTON_COLOR,          VAL_TRANSPARENT));
	errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_LANG_CANCEL, ATTR_FIT_MODE,                  VAL_SIZE_TO_IMAGE));

    // Build control: IMAG_PANEL_TEXTMSG
    errChk(IMAG_PANEL_TEXTMSG = NewCtrl (hPanel, CTRL_TEXT_MSG, "", 110 + nAdaptersAdj, 96));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_TEXTMSG, ATTR_CONSTANT_NAME, "TEXTMSG"));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_TEXTMSG, ATTR_CTRL_MODE,       VAL_INDICATOR));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_TEXTMSG, ATTR_HEIGHT,          22));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_TEXTMSG, ATTR_WIDTH,           246));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_TEXTMSG, ATTR_SIZE_TO_TEXT,    TRUE));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_TEXTMSG, ATTR_TEXT_JUSTIFY,    VAL_LEFT_JUSTIFIED));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_TEXTMSG, ATTR_TEXT_FONT,       VAL_DIALOG_FONT));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_TEXTMSG, ATTR_TEXT_POINT_SIZE, 17));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_TEXTMSG, ATTR_TEXT_BOLD,       FALSE/*TRUE*/));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_TEXTMSG, ATTR_TEXT_BGCOLOR,    VAL_LT_GRAY));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_TEXTMSG, ATTR_TEXT_RAISED,     FALSE/*TRUE*/));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_TEXTMSG, ATTR_VISIBLE,         FALSE/*TRUE*/));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_TEXTMSG, ATTR_DFLT_VALUE,      LDBf(Ldb_ImagPnlImaglist_lbl)));
    errChk(DefaultCtrl (hPanel, IMAG_PANEL_TEXTMSG));

	// Place IMAG_PANEL_TEXTMSG in the middle:
	errChk(GetPanelAttribute(hPanel, ATTR_WIDTH, &panelWidth));
	errChk(GetCtrlAttribute(hPanel, IMAG_PANEL_TEXTMSG, ATTR_WIDTH, &nCtrlWidth));
	errChk(SetCtrlAttribute(hPanel, IMAG_PANEL_TEXTMSG, ATTR_LEFT, ( panelWidth/2)-(nCtrlWidth/2)));

	if(MultiHomed){
	// Place IMAG_PANEL_ADAPTER_LIST in the middle:
	errChk(GetPanelAttribute(hPanel, ATTR_WIDTH, &panelWidth));
	errChk(GetCtrlAttribute(hPanel, IMAG_PANEL_ADAPTER_LIST, ATTR_WIDTH,       &nCtrlWidth));
	errChk(SetCtrlAttribute(hPanel, IMAG_PANEL_ADAPTER_LIST, ATTR_LEFT,        (panelWidth/2)-(nCtrlWidth/2)));
	errChk(GetCtrlAttribute(hPanel, IMAG_PANEL_ADAPTER_LIST, ATTR_LABEL_WIDTH, &nCtrlWidth));
	errChk(SetCtrlAttribute(hPanel, IMAG_PANEL_ADAPTER_LIST, ATTR_LABEL_LEFT,  (panelWidth/2)-(nCtrlWidth/2)));
	}

	// Place IMAG_PANEL_IMAG_LIST in the middle:
	errChk(GetPanelAttribute(hPanel, ATTR_WIDTH, &panelWidth));
	errChk(GetCtrlAttribute(hPanel, IMAG_PANEL_IMAG_LIST, ATTR_WIDTH,       &nCtrlWidth));
	errChk(SetCtrlAttribute(hPanel, IMAG_PANEL_IMAG_LIST, ATTR_LEFT,        (panelWidth/2)-(nCtrlWidth/2)));
	errChk(GetCtrlAttribute(hPanel, IMAG_PANEL_IMAG_LIST, ATTR_LABEL_WIDTH, &nCtrlWidth));
	errChk(SetCtrlAttribute(hPanel, IMAG_PANEL_IMAG_LIST, ATTR_LABEL_LEFT,  (panelWidth/2)-(nCtrlWidth/2)));

	// Place the buttons in the middle:
	errChk(GetPanelAttribute(hPanel, ATTR_WIDTH, &panelWidth));
	errChk(GetCtrlAttribute(hPanel, IMAG_PANEL_LANG_DONE,   ATTR_WIDTH, &buttonWidth));
	errChk(SetCtrlAttribute(hPanel, IMAG_PANEL_LANG_DONE,   ATTR_LEFT,  (  panelWidth/3)-(buttonWidth/2)));
	errChk(SetCtrlAttribute(hPanel, IMAG_PANEL_LANG_CANCEL, ATTR_LEFT,  (2*panelWidth/3)-(buttonWidth/2)));

    // Set up ZPLANE order
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_LANG_CANCEL,         ATTR_ZPLANE_POSITION, nZplaneOrder++));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_LANG_DONE,           ATTR_ZPLANE_POSITION, nZplaneOrder++));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_TEXTMSG,             ATTR_ZPLANE_POSITION, nZplaneOrder++));
	if(MultiHomed){
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_ADAPTER_LIST,        ATTR_ZPLANE_POSITION, nZplaneOrder++));
    }
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_IMAG_LIST,           ATTR_ZPLANE_POSITION, nZplaneOrder++));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_VENT_CAL_BUTTON,     ATTR_ZPLANE_POSITION, nZplaneOrder++));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_840_AUTODET_STATUS,  ATTR_ZPLANE_POSITION, nZplaneOrder++));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_840_BDSOFTWARE,      ATTR_ZPLANE_POSITION, nZplaneOrder++));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_840_BDSOFTWARE_LBL,  ATTR_ZPLANE_POSITION, nZplaneOrder++));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_840_GUISOFTWARE,     ATTR_ZPLANE_POSITION, nZplaneOrder++));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_840_GUISOFTWARE_LBL, ATTR_ZPLANE_POSITION, nZplaneOrder++));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_VENTSFTWR_BOX_LBL,   ATTR_ZPLANE_POSITION, nZplaneOrder++));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_VENTSFTWR_BOX,       ATTR_ZPLANE_POSITION, nZplaneOrder++));

    // Set up TAB order
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_VENT_CAL_BUTTON, ATTR_CTRL_TAB_POSITION, nTabOrder++));
	if(MultiHomed){
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_ADAPTER_LIST,    ATTR_CTRL_TAB_POSITION, nTabOrder++));
	}
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_IMAG_LIST,       ATTR_CTRL_TAB_POSITION, nTabOrder++));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_LANG_DONE,       ATTR_CTRL_TAB_POSITION, nTabOrder++));
    errChk(SetCtrlAttribute (hPanel, IMAG_PANEL_LANG_CANCEL,     ATTR_CTRL_TAB_POSITION, nTabOrder++));

    // Finalize panel colors, positioning, and sizing
    errChk(SetPanelAttribute (hPanel, ATTR_CONFORM_TO_SYSTEM,        FALSE));
    errChk(SetPanelAttribute (hPanel, ATTR_TOP,                      VAL_AUTO_CENTER));
    errChk(SetPanelAttribute (hPanel, ATTR_LEFT,                     VAL_AUTO_CENTER));
    errChk(SetPanelAttribute (hPanel, ATTR_SCALE_CONTENTS_ON_RESIZE, FALSE));

   	errChk(SetPanelAttribute(hPanel, ATTR_CLOSE_CTRL, IMAG_PANEL_LANG_CANCEL));

    //We're done!
    return hPanel;
Error:
    if (bitmapId)
        DiscardBitmap (bitmapId);
    return error;
}

//----------------------------------------------------------------------------------------------------
// BuildINS_PANEL ():  Build UI object INS_PANEL; return handle or standard UI error code.
//----------------------------------------------------------------------------------------------------
int BuildINS_PANEL (int hParentPanel)
{
    int hPanel;
    int error = 0;
    char dataPtr[1024] = {'\0'}; 
   	int panelWidth;
   	int buttonWidth;
   	int CtrlWidth;
	int nZplaneOrder = 0;
	int nTabOrder    = 0;

    // Create the panel
    errChk(hPanel = NewPanel (hParentPanel, LDBf(Ldb_InsPanelTitle), 20, 2, 250, 512));

    // Set the panel's attributes
    errChk(SetPanelAttribute (hPanel, ATTR_CONSTANT_NAME, "INS_PANEL"));
    errChk(SetPanelAttribute (hPanel, ATTR_CALLBACK_FUNCTION_POINTER, 0));
    errChk(SetPanelAttribute (hPanel, ATTR_BACKCOLOR, VAL_LT_GRAY));
    errChk(SetPanelAttribute (hPanel, ATTR_CAN_MINIMIZE, FALSE));
    errChk(SetPanelAttribute (hPanel, ATTR_CAN_MAXIMIZE, FALSE));
    errChk(SetPanelAttribute (hPanel, ATTR_SIZABLE,      FALSE));
    errChk(SetPanelAttribute (hPanel, ATTR_MOVABLE,      TRUE));
    if (hParentPanel){
        errChk(SetPanelAttribute (hPanel, ATTR_FRAME_STYLE, VAL_RAISED_OUTLINED_FRAME));
        errChk(SetPanelAttribute (hPanel, ATTR_FRAME_THICKNESS,    10));
        errChk(SetPanelAttribute (hPanel, ATTR_FRAME_COLOR,        VAL_LT_GRAY));
        errChk(SetPanelAttribute (hPanel, ATTR_TITLE_FONT,         VAL_DIALOG_META_FONT));
        errChk(SetPanelAttribute (hPanel, ATTR_TITLE_POINT_SIZE,   15));
        errChk(SetPanelAttribute (hPanel, ATTR_TITLEBAR_THICKNESS, 19));
        errChk(SetPanelAttribute (hPanel, ATTR_TITLE_BACKCOLOR,    VAL_LT_GRAY));
    }
    errChk(SetPanelAttribute (hPanel, ATTR_MIN_HEIGHT_FOR_SCALING, 0));
    errChk(SetPanelAttribute (hPanel, ATTR_MIN_WIDTH_FOR_SCALING,  0));
    // when ATTR_SYSTEM_MENU_VISIBLE == 0, 
    //   then ATTR_CLOSE_ITEM_VISIBLE = 0 regardless of its setting
    errChk(SetPanelAttribute (hPanel, ATTR_SYSTEM_MENU_VISIBLE, TRUE));
    errChk(SetPanelAttribute (hPanel, ATTR_CLOSE_ITEM_VISIBLE,  TRUE));

	// Build OK bitmap button
	errChk(INS_PANEL_INS_DONE = NewCtrl(hPanel, CTRL_PICTURE_COMMAND_BUTTON, 0, 180, 170));
	setButtonBMP(hPanel, INS_PANEL_INS_DONE, BMP_Okay192);	
	errChk(SetCtrlAttribute(hPanel, INS_PANEL_INS_DONE, ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT));
	errChk(SetCtrlAttribute(hPanel, INS_PANEL_INS_DONE, ATTR_FIT_MODE,         VAL_SIZE_TO_IMAGE));
	
	// Build Cancel bitmap button
	errChk(INS_PANEL_INS_CANCEL = NewCtrl(hPanel, CTRL_PICTURE_COMMAND_BUTTON, 0, 180, 270));
	errChk(setButtonBMP(hPanel, INS_PANEL_INS_CANCEL, BMP_Cancel192));
	errChk(SetCtrlAttribute(hPanel, INS_PANEL_INS_CANCEL, ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT));
	errChk(SetCtrlAttribute(hPanel, INS_PANEL_INS_CANCEL, ATTR_FIT_MODE,         VAL_SIZE_TO_IMAGE));

    // Build control: INS_PANEL_INSTRUCTIONS
    errChk(INS_PANEL_INSTRUCTIONS = NewCtrl (hPanel, CTRL_TEXT_BOX, "", 61, 43));
    errChk(SetCtrlAttribute (hPanel, INS_PANEL_INSTRUCTIONS, ATTR_CONSTANT_NAME, "INSTRUCTIONS"));
    errChk(SetCtrlAttribute (hPanel, INS_PANEL_INSTRUCTIONS, ATTR_CTRL_MODE,        VAL_INDICATOR));
    errChk(SetCtrlAttribute (hPanel, INS_PANEL_INSTRUCTIONS, ATTR_FRAME_COLOR,      VAL_LT_GRAY));
    errChk(SetCtrlAttribute (hPanel, INS_PANEL_INSTRUCTIONS, ATTR_SCROLL_BARS,      VAL_NO_SCROLL_BARS));
    errChk(SetCtrlAttribute (hPanel, INS_PANEL_INSTRUCTIONS, ATTR_SCROLL_BAR_COLOR, VAL_LT_GRAY));
    errChk(SetCtrlAttribute (hPanel, INS_PANEL_INSTRUCTIONS, ATTR_LABEL_HEIGHT,     15));
    errChk(SetCtrlAttribute (hPanel, INS_PANEL_INSTRUCTIONS, ATTR_LABEL_WIDTH,      9));
    errChk(SetCtrlAttribute (hPanel, INS_PANEL_INSTRUCTIONS, ATTR_LABEL_BGCOLOR,    VAL_LT_GRAY));
    errChk(SetCtrlAttribute (hPanel, INS_PANEL_INSTRUCTIONS, ATTR_TEXT_FONT,        VAL_DIALOG_FONT));
    errChk(SetCtrlAttribute (hPanel, INS_PANEL_INSTRUCTIONS, ATTR_TEXT_POINT_SIZE,  13));
    errChk(SetCtrlAttribute (hPanel, INS_PANEL_INSTRUCTIONS, ATTR_TEXT_BGCOLOR,     VAL_WHITE));
//    dataPtr = 
//        " Install serial cable between computer and ventilator GUI assembly.\n"
//        " Install ethernet breakout box between ventilator GUI and BDU.\n"
//        " Install ethernet cable between breakout box and computer.\n"
//        " Power-up ventilator in Service Mode for External Test Control.";
    strcpy(dataPtr, LDBf(Ldb_InsPnlInstr_Line01));
    strcat(dataPtr, LDBf(Ldb_InsPnlInstr_Line02));
    strcat(dataPtr, LDBf(Ldb_InsPnlInstr_Line03));
    strcat(dataPtr, LDBf(Ldb_InsPnlInstr_Line04));
    errChk(SetCtrlAttribute (hPanel, INS_PANEL_INSTRUCTIONS, ATTR_DFLT_VALUE, dataPtr));
    errChk(DefaultCtrl (hPanel, INS_PANEL_INSTRUCTIONS));
    errChk(SetCtrlAttribute (hPanel, INS_PANEL_INSTRUCTIONS, ATTR_HEIGHT,             70));
    errChk(SetCtrlAttribute (hPanel, INS_PANEL_INSTRUCTIONS, ATTR_WIDTH,              425));
    errChk(SetCtrlAttribute (hPanel, INS_PANEL_INSTRUCTIONS, ATTR_LABEL_TOP,          40));
    errChk(SetCtrlAttribute (hPanel, INS_PANEL_INSTRUCTIONS, ATTR_LABEL_LEFT,         43));
    errChk(SetCtrlAttribute (hPanel, INS_PANEL_INSTRUCTIONS, ATTR_LABEL_SIZE_TO_TEXT, TRUE));
    errChk(SetCtrlAttribute (hPanel, INS_PANEL_INSTRUCTIONS, ATTR_TEXT_JUSTIFY,       VAL_LEFT_JUSTIFIED));

    // Build control: INS_PANEL_DECORATION
    errChk(INS_PANEL_DECORATION = NewCtrl (hPanel, CTRL_RECESSED_FRAME, "", 13, 27));
    errChk(SetCtrlAttribute (hPanel, INS_PANEL_DECORATION, ATTR_CONSTANT_NAME, "DECORATION"));
    errChk(SetCtrlAttribute (hPanel, INS_PANEL_DECORATION, ATTR_FRAME_COLOR, VAL_LT_GRAY));
    errChk(SetCtrlAttribute (hPanel, INS_PANEL_DECORATION, ATTR_CTRL_MODE,   VAL_INDICATOR));
    errChk(SetCtrlAttribute (hPanel, INS_PANEL_DECORATION, ATTR_HEIGHT,      155));
    errChk(SetCtrlAttribute (hPanel, INS_PANEL_DECORATION, ATTR_WIDTH,       457));

    // Build control: INS_PANEL_TEXTMSG
    errChk(INS_PANEL_TEXTMSG = NewCtrl (hPanel, CTRL_TEXT_MSG, "", 21, 91));
    errChk(SetCtrlAttribute (hPanel, INS_PANEL_TEXTMSG, ATTR_CONSTANT_NAME, "TEXTMSG"));
    errChk(SetCtrlAttribute (hPanel, INS_PANEL_TEXTMSG, ATTR_CTRL_MODE,       VAL_INDICATOR));
    errChk(SetCtrlAttribute (hPanel, INS_PANEL_TEXTMSG, ATTR_HEIGHT,          18));
    errChk(SetCtrlAttribute (hPanel, INS_PANEL_TEXTMSG, ATTR_WIDTH,           293));
    errChk(SetCtrlAttribute (hPanel, INS_PANEL_TEXTMSG, ATTR_TEXT_FONT,       VAL_DIALOG_FONT));
    errChk(SetCtrlAttribute (hPanel, INS_PANEL_TEXTMSG, ATTR_TEXT_POINT_SIZE, 17));
    errChk(SetCtrlAttribute (hPanel, INS_PANEL_TEXTMSG, ATTR_TEXT_BOLD,       FALSE));
    errChk(SetCtrlAttribute (hPanel, INS_PANEL_TEXTMSG, ATTR_TEXT_BGCOLOR,    VAL_LT_GRAY));
    errChk(SetCtrlAttribute (hPanel, INS_PANEL_TEXTMSG, ATTR_TEXT_RAISED,     FALSE));
    errChk(SetCtrlAttribute (hPanel, INS_PANEL_TEXTMSG, ATTR_DFLT_VALUE,      LDBf(Ldb_InsPnlTextMsg)));
    errChk(DefaultCtrl (hPanel, INS_PANEL_TEXTMSG));
    errChk(SetCtrlAttribute (hPanel, INS_PANEL_TEXTMSG, ATTR_SIZE_TO_TEXT, TRUE));
    errChk(SetCtrlAttribute (hPanel, INS_PANEL_TEXTMSG, ATTR_TEXT_JUSTIFY, VAL_LEFT_JUSTIFIED));

	// Place the buttons in the middle:
	errChk(GetPanelAttribute(hPanel, ATTR_WIDTH, &panelWidth));
	errChk(GetCtrlAttribute(hPanel, INS_PANEL_INS_DONE,   ATTR_WIDTH, &buttonWidth));
	errChk(SetCtrlAttribute(hPanel, INS_PANEL_INS_DONE,   ATTR_LEFT,  (  panelWidth/3)-(buttonWidth/2)));
	errChk(SetCtrlAttribute(hPanel, INS_PANEL_INS_CANCEL, ATTR_LEFT,  (2*panelWidth/3)-(buttonWidth/2)));

	// Place TEXTMSG in the middle
	errChk(GetCtrlAttribute(hPanel, INS_PANEL_TEXTMSG, ATTR_WIDTH, &CtrlWidth));
	errChk(SetCtrlAttribute(hPanel, INS_PANEL_TEXTMSG, ATTR_LEFT, ((panelWidth-CtrlWidth)/2) ));

    // Set up ZPLANE order
    errChk(SetCtrlAttribute (hPanel, INS_PANEL_INS_CANCEL,   ATTR_ZPLANE_POSITION, nZplaneOrder++));
    errChk(SetCtrlAttribute (hPanel, INS_PANEL_INSTRUCTIONS, ATTR_ZPLANE_POSITION, nZplaneOrder++));
    errChk(SetCtrlAttribute (hPanel, INS_PANEL_TEXTMSG,      ATTR_ZPLANE_POSITION, nZplaneOrder++));
    errChk(SetCtrlAttribute (hPanel, INS_PANEL_INS_DONE,     ATTR_ZPLANE_POSITION, nZplaneOrder++));
    errChk(SetCtrlAttribute (hPanel, INS_PANEL_DECORATION,   ATTR_ZPLANE_POSITION, nZplaneOrder++));

    // Set up TAB order
    errChk(SetCtrlAttribute (hPanel, INS_PANEL_INS_DONE,     ATTR_CTRL_TAB_POSITION, nTabOrder++));
    errChk(SetCtrlAttribute (hPanel, INS_PANEL_INS_CANCEL,   ATTR_CTRL_TAB_POSITION, nTabOrder++));
    errChk(SetCtrlAttribute (hPanel, INS_PANEL_INSTRUCTIONS, ATTR_CTRL_TAB_POSITION, nTabOrder++));

    // Finalize panel colors, positioning, and sizing
    errChk(SetPanelAttribute (hPanel, ATTR_CONFORM_TO_SYSTEM,        FALSE));
    errChk(SetPanelAttribute (hPanel, ATTR_TOP,                      VAL_AUTO_CENTER));
    errChk(SetPanelAttribute (hPanel, ATTR_LEFT,                     VAL_AUTO_CENTER));
    errChk(SetPanelAttribute (hPanel, ATTR_SCALE_CONTENTS_ON_RESIZE, FALSE));

   	errChk(SetPanelAttribute(hPanel, ATTR_CLOSE_CTRL, INS_PANEL_INS_CANCEL));

    //We're done!
    return hPanel;
Error:
//    if (bitmapId)
//        DiscardBitmap (bitmapId);
    return error;
}

//----------------------------------------------------------------------------------------------------
// BuildSTATEPANEL ():  Build UI object STATEPANEL; return handle or standard UI error code.
//----------------------------------------------------------------------------------------------------
int BuildSTATEPANEL (int hParentPanel)
{
    int hPanel;
    int error = 0;
   	int panelWidth;
   	int buttonWidth;
	int nZplaneOrder = 0;
	int nTabOrder    = 0;

    // Create the panel
    /*"Install or replace ventilator software"*/
    errChk(hPanel = NewPanel (hParentPanel, LDBf(Ldb_StatePnlTitle), 81, 172, 160, 348));

    // Set the panel's attributes
    errChk(SetPanelAttribute (hPanel, ATTR_CONSTANT_NAME, "STATEPANEL"));
    errChk(SetPanelAttribute (hPanel, ATTR_CALLBACK_FUNCTION_POINTER, 0));
    errChk(SetPanelAttribute (hPanel, ATTR_BACKCOLOR, VAL_LT_GRAY));
    errChk(SetPanelAttribute (hPanel, ATTR_CAN_MINIMIZE, FALSE));
    errChk(SetPanelAttribute (hPanel, ATTR_CAN_MAXIMIZE, FALSE));
    errChk(SetPanelAttribute (hPanel, ATTR_SIZABLE,      FALSE));
    errChk(SetPanelAttribute (hPanel, ATTR_MOVABLE,      TRUE));
    if (hParentPanel){
        errChk(SetPanelAttribute (hPanel, ATTR_FRAME_STYLE,        VAL_RAISED_OUTLINED_FRAME));
        errChk(SetPanelAttribute (hPanel, ATTR_FRAME_THICKNESS,    10));
        errChk(SetPanelAttribute (hPanel, ATTR_FRAME_COLOR,        VAL_LT_GRAY));
        errChk(SetPanelAttribute (hPanel, ATTR_TITLE_FONT,         VAL_DIALOG_META_FONT));
        errChk(SetPanelAttribute (hPanel, ATTR_TITLE_POINT_SIZE,   14));
        errChk(SetPanelAttribute (hPanel, ATTR_TITLEBAR_THICKNESS, 19));
        errChk(SetPanelAttribute (hPanel, ATTR_TITLE_BACKCOLOR,    VAL_LT_GRAY));
    }
    errChk(SetPanelAttribute (hPanel, ATTR_MIN_HEIGHT_FOR_SCALING, 0));
    errChk(SetPanelAttribute (hPanel, ATTR_MIN_WIDTH_FOR_SCALING,  0));
    // when ATTR_SYSTEM_MENU_VISIBLE == 0, 
    //   then ATTR_CLOSE_ITEM_VISIBLE = 0 regardless of its setting
    errChk(SetPanelAttribute (hPanel, ATTR_SYSTEM_MENU_VISIBLE,    TRUE));
    errChk(SetPanelAttribute (hPanel, ATTR_CLOSE_ITEM_VISIBLE,     TRUE));

    // Build control: STATEPANEL_GROUP_BOX
    errChk(STATEPANEL_GROUP_BOX = NewCtrl (hPanel, CTRL_RECESSED_FRAME, "", 10, 15));
    errChk(SetCtrlAttribute (hPanel, STATEPANEL_GROUP_BOX, ATTR_CONSTANT_NAME, "GROUP_BOX"));
    errChk(SetCtrlAttribute (hPanel, STATEPANEL_GROUP_BOX, ATTR_HEIGHT,      70));
    errChk(SetCtrlAttribute (hPanel, STATEPANEL_GROUP_BOX, ATTR_WIDTH,       320));
    errChk(SetCtrlAttribute (hPanel, STATEPANEL_GROUP_BOX, ATTR_CTRL_MODE,   VAL_INDICATOR));
    errChk(SetCtrlAttribute (hPanel, STATEPANEL_GROUP_BOX, ATTR_FRAME_COLOR, VAL_LT_GRAY));

    // Build control: STATEPANEL_INITIAL_DLOAD
    /* "Initial Download - BDU and/or GUI Software" */
    errChk(STATEPANEL_INITIAL_DLOAD = NewCtrl (hPanel, CTRL_ROUND_RADIO_BUTTON, LDBf(Ldb_StatePnlInit), 25, 25));
    errChk(SetCtrlAttribute (hPanel, STATEPANEL_INITIAL_DLOAD, ATTR_CONSTANT_NAME, "INITIAL_DLOAD"));
    errChk(SetCtrlAttribute (hPanel, STATEPANEL_INITIAL_DLOAD, ATTR_CALLBACK_FUNCTION_POINTER, DnloadStateCB));
    errChk(SetCtrlAttribute (hPanel, STATEPANEL_INITIAL_DLOAD, ATTR_SHORTCUT_KEY,              0));
    errChk(SetCtrlAttribute (hPanel, STATEPANEL_INITIAL_DLOAD, ATTR_CTRL_MODE,                 VAL_HOT));
    errChk(SetCtrlAttribute (hPanel, STATEPANEL_INITIAL_DLOAD, ATTR_HEIGHT,                    13));
    errChk(SetCtrlAttribute (hPanel, STATEPANEL_INITIAL_DLOAD, ATTR_WIDTH,                     13));
    errChk(SetCtrlAttribute (hPanel, STATEPANEL_INITIAL_DLOAD, ATTR_LABEL_TOP,                 22));
    errChk(SetCtrlAttribute (hPanel, STATEPANEL_INITIAL_DLOAD, ATTR_LABEL_LEFT,                45));
    errChk(SetCtrlAttribute (hPanel, STATEPANEL_INITIAL_DLOAD, ATTR_LABEL_FONT,                "Arial"));
    errChk(SetCtrlAttribute (hPanel, STATEPANEL_INITIAL_DLOAD, ATTR_LABEL_POINT_SIZE,          14));
    errChk(SetCtrlAttribute (hPanel, STATEPANEL_INITIAL_DLOAD, ATTR_LABEL_BOLD,                FALSE));
    errChk(SetCtrlAttribute (hPanel, STATEPANEL_INITIAL_DLOAD, ATTR_LABEL_SIZE_TO_TEXT,        TRUE));
    errChk(SetCtrlAttribute (hPanel, STATEPANEL_INITIAL_DLOAD, ATTR_DFLT_VALUE,                ON));
    errChk(DefaultCtrl (hPanel, STATEPANEL_INITIAL_DLOAD));

    // Build control: STATEPANEL_REPLACE_BOTH_DLOAD
    /* "Replace BDU and GUI Software" */
    errChk(STATEPANEL_REPLACE_BOTH_DLOAD = NewCtrl (hPanel, CTRL_ROUND_RADIO_BUTTON, LDBf(Ldb_StatePnlReplace), 50, 25));
    errChk(SetCtrlAttribute (hPanel, STATEPANEL_REPLACE_BOTH_DLOAD, ATTR_CONSTANT_NAME, "BOTH_DLOAD"));
    errChk(SetCtrlAttribute (hPanel, STATEPANEL_REPLACE_BOTH_DLOAD, ATTR_CALLBACK_FUNCTION_POINTER, DnloadStateCB));
    errChk(SetCtrlAttribute (hPanel, STATEPANEL_REPLACE_BOTH_DLOAD, ATTR_SHORTCUT_KEY,              0));
    errChk(SetCtrlAttribute (hPanel, STATEPANEL_REPLACE_BOTH_DLOAD, ATTR_CTRL_MODE,                 VAL_HOT));
    errChk(SetCtrlAttribute (hPanel, STATEPANEL_REPLACE_BOTH_DLOAD, ATTR_HEIGHT,                    13));
    errChk(SetCtrlAttribute (hPanel, STATEPANEL_REPLACE_BOTH_DLOAD, ATTR_WIDTH,                     13));
    errChk(SetCtrlAttribute (hPanel, STATEPANEL_REPLACE_BOTH_DLOAD, ATTR_LABEL_TOP,                 47));
    errChk(SetCtrlAttribute (hPanel, STATEPANEL_REPLACE_BOTH_DLOAD, ATTR_LABEL_LEFT,                45));
    errChk(SetCtrlAttribute (hPanel, STATEPANEL_REPLACE_BOTH_DLOAD, ATTR_LABEL_FONT,                "Arial"));
    errChk(SetCtrlAttribute (hPanel, STATEPANEL_REPLACE_BOTH_DLOAD, ATTR_LABEL_POINT_SIZE,          14));
    errChk(SetCtrlAttribute (hPanel, STATEPANEL_REPLACE_BOTH_DLOAD, ATTR_LABEL_BOLD,                FALSE));
    errChk(SetCtrlAttribute (hPanel, STATEPANEL_REPLACE_BOTH_DLOAD, ATTR_LABEL_SIZE_TO_TEXT,        TRUE));
    errChk(SetCtrlAttribute (hPanel, STATEPANEL_REPLACE_BOTH_DLOAD, ATTR_DFLT_VALUE,                OFF));
    errChk(DefaultCtrl (hPanel, STATEPANEL_REPLACE_BOTH_DLOAD));

	// Build OK bitmap button
	errChk(STATEPANEL_STATE_DONE = NewCtrl(hPanel, CTRL_PICTURE_COMMAND_BUTTON, 0, 95, 78));
	errChk(setButtonBMP(hPanel, STATEPANEL_STATE_DONE, BMP_Okay192));
	errChk(SetCtrlAttribute(hPanel, STATEPANEL_STATE_DONE, ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT));
	errChk(SetCtrlAttribute(hPanel, STATEPANEL_STATE_DONE, ATTR_FIT_MODE,         VAL_SIZE_TO_IMAGE));
	
	// Build Cancel bitmap button
	errChk(STATEPANEL_STATE_CANCEL = NewCtrl(hPanel, CTRL_PICTURE_COMMAND_BUTTON, 0, 95, 178));
	errChk(setButtonBMP(hPanel, STATEPANEL_STATE_CANCEL, BMP_Cancel192));
	errChk(SetCtrlAttribute(hPanel, STATEPANEL_STATE_CANCEL, ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT));
	errChk(SetCtrlAttribute(hPanel, STATEPANEL_STATE_CANCEL, ATTR_FIT_MODE,         VAL_SIZE_TO_IMAGE));

	// Place the buttons in the middle:
	errChk(GetPanelAttribute(hPanel, ATTR_WIDTH, &panelWidth));
	errChk(GetCtrlAttribute(hPanel, STATEPANEL_STATE_DONE, ATTR_WIDTH, &buttonWidth));
	errChk(SetCtrlAttribute(hPanel, STATEPANEL_STATE_DONE,   ATTR_LEFT, (  panelWidth/3)-(buttonWidth/2)));
	errChk(SetCtrlAttribute(hPanel, STATEPANEL_STATE_CANCEL, ATTR_LEFT, (2*panelWidth/3)-(buttonWidth/2)));

    // Set up ZPLANE order
    errChk(SetCtrlAttribute (hPanel, STATEPANEL_STATE_CANCEL,       ATTR_ZPLANE_POSITION, nZplaneOrder++));
    errChk(SetCtrlAttribute (hPanel, STATEPANEL_STATE_DONE,         ATTR_ZPLANE_POSITION, nZplaneOrder++));
    errChk(SetCtrlAttribute (hPanel, STATEPANEL_INITIAL_DLOAD,      ATTR_ZPLANE_POSITION, nZplaneOrder++));
    errChk(SetCtrlAttribute (hPanel, STATEPANEL_REPLACE_BOTH_DLOAD, ATTR_ZPLANE_POSITION, nZplaneOrder++));
    errChk(SetCtrlAttribute (hPanel, STATEPANEL_GROUP_BOX,          ATTR_ZPLANE_POSITION, nZplaneOrder++));

    // Set up TAB order
    errChk(SetCtrlAttribute (hPanel, STATEPANEL_INITIAL_DLOAD,      ATTR_CTRL_TAB_POSITION, nTabOrder++));
    errChk(SetCtrlAttribute (hPanel, STATEPANEL_REPLACE_BOTH_DLOAD, ATTR_CTRL_TAB_POSITION, nTabOrder++));
    errChk(SetCtrlAttribute (hPanel, STATEPANEL_STATE_DONE,         ATTR_CTRL_TAB_POSITION, nTabOrder++));
    errChk(SetCtrlAttribute (hPanel, STATEPANEL_STATE_CANCEL,       ATTR_CTRL_TAB_POSITION, nTabOrder++));

    // Finalize panel colors, positioning, and sizing
    errChk(SetPanelAttribute (hPanel, ATTR_CONFORM_TO_SYSTEM,        FALSE));
    errChk(SetPanelAttribute (hPanel, ATTR_TOP,                      VAL_AUTO_CENTER));
    errChk(SetPanelAttribute (hPanel, ATTR_LEFT,                     VAL_AUTO_CENTER));
    errChk(SetPanelAttribute (hPanel, ATTR_SCALE_CONTENTS_ON_RESIZE, FALSE));

   	errChk(SetPanelAttribute(hPanel, ATTR_CLOSE_CTRL, STATEPANEL_STATE_CANCEL));

    //We're done!
    return hPanel;
Error:
//    if (bitmapId)
//        DiscardBitmap (bitmapId);
    return error;
}

#define TFTP_PANEL_COLOR 0X9CA4B4
//----------------------------------------------------------------------------------------------------
// BuildTFTP_PANEL ():  Build UI object TFTP_PANEL; return handle or standard UI error code.
//----------------------------------------------------------------------------------------------------
int BuildTFTP_PANEL (int hParentPanel)
{
    int hPanel;
    int error = 0;
	int nZplaneOrder = 0;
	int nTabOrder    = 0;

    // Create the panel
    // "File Transfer Progress"
    errChk(hPanel = NewPanel (hParentPanel, LDBf(Ldb_TFTP_PnlTitle), 92, 13, 350, 358));

    // Set the panel's attributes
    errChk(SetPanelAttribute (hPanel, ATTR_CONSTANT_NAME, "TFTP_PANEL"));
    errChk(SetPanelAttribute (hPanel, ATTR_BACKCOLOR, TFTP_PANEL_COLOR));
    errChk(SetPanelAttribute (hPanel, ATTR_CAN_MINIMIZE, FALSE));
    errChk(SetPanelAttribute (hPanel, ATTR_CAN_MAXIMIZE, FALSE));
    errChk(SetPanelAttribute (hPanel, ATTR_SIZABLE,      FALSE));
    errChk(SetPanelAttribute (hPanel, ATTR_MOVABLE,      FALSE));
    if (hParentPanel){
        errChk(SetPanelAttribute (hPanel, ATTR_FRAME_STYLE,        VAL_RAISED_OUTLINED_FRAME));
        errChk(SetPanelAttribute (hPanel, ATTR_FRAME_THICKNESS,    10));
        errChk(SetPanelAttribute (hPanel, ATTR_FRAME_COLOR,        VAL_LT_GRAY));
        errChk(SetPanelAttribute (hPanel, ATTR_TITLE_FONT,         VAL_DIALOG_META_FONT));
        errChk(SetPanelAttribute (hPanel, ATTR_TITLE_POINT_SIZE,   11));
        errChk(SetPanelAttribute (hPanel, ATTR_TITLE_SIZE_TO_FONT, TRUE));
        errChk(SetPanelAttribute (hPanel, ATTR_TITLE_BACKCOLOR,    VAL_LT_GRAY));
    }
    errChk(SetPanelAttribute (hPanel, ATTR_MIN_HEIGHT_FOR_SCALING, 0));
    errChk(SetPanelAttribute (hPanel, ATTR_MIN_WIDTH_FOR_SCALING,  0));
    // when ATTR_SYSTEM_MENU_VISIBLE == 0, 
    //   then ATTR_CLOSE_ITEM_VISIBLE = 0 regardless of its setting
    errChk(SetPanelAttribute (hPanel, ATTR_SYSTEM_MENU_VISIBLE, TRUE));
    errChk(SetPanelAttribute (hPanel, ATTR_CLOSE_ITEM_VISIBLE,  TRUE));

    // Build control: TFTP_PANEL_BD_GROUP_BOX
    errChk(TFTP_PANEL_BD_GROUP_BOX = NewCtrl (hPanel, CTRL_RECESSED_FRAME, "", 13, 30));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_GROUP_BOX, ATTR_CONSTANT_NAME, "BD_GROUP_BOX"));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_GROUP_BOX, ATTR_HEIGHT,      150));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_GROUP_BOX, ATTR_WIDTH,       305));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_GROUP_BOX, ATTR_CTRL_MODE,   VAL_INDICATOR));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_GROUP_BOX, ATTR_FRAME_COLOR, VAL_LT_GRAY));

    // Build control: TFTP_PANEL_GUI_GROUP_BOX
    errChk(TFTP_PANEL_GUI_GROUP_BOX = NewCtrl (hPanel, CTRL_RECESSED_FRAME, "", 180, 30));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_GROUP_BOX, ATTR_CONSTANT_NAME, "GUI_GROUP_BOX"));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_GROUP_BOX, ATTR_HEIGHT,      150));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_GROUP_BOX, ATTR_WIDTH,       305));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_GROUP_BOX, ATTR_CTRL_MODE,   VAL_INDICATOR));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_GROUP_BOX, ATTR_FRAME_COLOR, VAL_LT_GRAY));

    // Build control: TFTP_PANEL_BD_LED
    errChk(TFTP_PANEL_BD_LED = NewCtrl (hPanel, CTRL_SQUARE_LIGHT, "", 20, 41));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_LED, ATTR_CONSTANT_NAME, "BD_LED"));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_LED, ATTR_HEIGHT,     8));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_LED, ATTR_WIDTH,      13));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_LED, ATTR_CTRL_MODE,  VAL_INDICATOR));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_LED, ATTR_ON_COLOR,   0XCC0066));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_LED, ATTR_OFF_COLOR,  VAL_LT_GRAY));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_LED, ATTR_DFLT_VALUE, OFF));
    errChk(DefaultCtrl (hPanel, TFTP_PANEL_BD_LED));

    // Build control: TFTP_PANEL_GUI_LED
    errChk(TFTP_PANEL_GUI_LED = NewCtrl (hPanel, CTRL_SQUARE_LIGHT, "", 187, 41));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_LED, ATTR_CONSTANT_NAME, "GUI_LED"));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_LED, ATTR_HEIGHT,     8));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_LED, ATTR_WIDTH,      13));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_LED, ATTR_CTRL_MODE,  VAL_INDICATOR));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_LED, ATTR_ON_COLOR,   0XCC0066));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_LED, ATTR_OFF_COLOR,  VAL_LT_GRAY));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_LED, ATTR_DFLT_VALUE, OFF));
    errChk(DefaultCtrl (hPanel, TFTP_PANEL_GUI_LED));

    // Build control: TFTP_PANEL_BD_FILE
    // "BDU File:"
    errChk(TFTP_PANEL_BD_FILE = NewCtrl (hPanel, CTRL_STRING, LDBf(Ldb_TFTP_BDUfile_lbl), 33, 140));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_FILE, ATTR_CONSTANT_NAME, "BD_FILE"));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_FILE, ATTR_WIDTH,              164));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_FILE, ATTR_CTRL_MODE,          VAL_INDICATOR));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_FILE, ATTR_FRAME_COLOR,        VAL_LT_GRAY));
	errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_FILE, ATTR_TEXT_FONT,          VAL_DIALOG_META_FONT));
	errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_FILE, ATTR_TEXT_POINT_SIZE,    11));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_FILE, ATTR_TEXT_BGCOLOR,       VAL_WHITE));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_FILE, ATTR_TEXT_JUSTIFY,       VAL_LEFT_JUSTIFIED));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_FILE, ATTR_LABEL_TOP,          34));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_FILE, ATTR_LABEL_LEFT,         50));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_FILE, ATTR_LABEL_HEIGHT,       15));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_FILE, ATTR_LABEL_WIDTH,        40));
	errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_FILE, ATTR_LABEL_FONT,         VAL_DIALOG_META_FONT));
	errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_FILE, ATTR_LABEL_POINT_SIZE,   11));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_FILE, ATTR_LABEL_BGCOLOR,      /*VAL_TRANSPARENT*/TFTP_PANEL_COLOR));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_FILE, ATTR_LABEL_RAISED,       TRUE));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_FILE, ATTR_LABEL_SIZE_TO_TEXT, TRUE));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_FILE, ATTR_DFLT_VALUE, ""));
    errChk(DefaultCtrl (hPanel, TFTP_PANEL_BD_FILE));

    // Build control: TFTP_PANEL_GUI_FILE
    // "GUI File:"
    errChk(TFTP_PANEL_GUI_FILE = NewCtrl (hPanel, CTRL_STRING, LDBf(Ldb_TFTP_GUIfile_lbl), 199, 140));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_FILE, ATTR_CONSTANT_NAME, "GUI_FILE"));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_FILE, ATTR_WIDTH,              164));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_FILE, ATTR_CTRL_MODE,          VAL_INDICATOR));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_FILE, ATTR_FRAME_COLOR,        VAL_LT_GRAY));
	errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_FILE, ATTR_TEXT_FONT,          VAL_DIALOG_META_FONT));
	errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_FILE, ATTR_TEXT_POINT_SIZE,    11));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_FILE, ATTR_TEXT_BGCOLOR,       VAL_WHITE));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_FILE, ATTR_TEXT_JUSTIFY,       VAL_LEFT_JUSTIFIED));
//  errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_FILE, ATTR_NO_EDIT_TEXT,       FALSE));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_FILE, ATTR_LABEL_TOP,          201));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_FILE, ATTR_LABEL_LEFT,         50));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_FILE, ATTR_LABEL_HEIGHT,       15));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_FILE, ATTR_LABEL_WIDTH,        41));
	errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_FILE, ATTR_LABEL_FONT,         VAL_DIALOG_META_FONT));
	errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_FILE, ATTR_LABEL_POINT_SIZE,   11));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_FILE, ATTR_LABEL_BGCOLOR,      TFTP_PANEL_COLOR));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_FILE, ATTR_LABEL_RAISED,       TRUE));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_FILE, ATTR_LABEL_SIZE_TO_TEXT, TRUE));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_FILE, ATTR_DFLT_VALUE, ""));
    errChk(DefaultCtrl (hPanel, TFTP_PANEL_GUI_FILE));

    // Build control: TFTP_PANEL_BD_PERCENT
    // "% Completed"
    errChk(TFTP_PANEL_BD_PERCENT = NewCtrl (hPanel, CTRL_NUMERIC_FLAT_HSLIDE, LDBf(Ldb_TFTP_BDUpercent_lbl), 61, 142));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_PERCENT, ATTR_CONSTANT_NAME, "BD_PERCENT"));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_PERCENT, ATTR_HEIGHT,             16));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_PERCENT, ATTR_WIDTH,              160));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_PERCENT, ATTR_CTRL_MODE,          VAL_INDICATOR));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_PERCENT, ATTR_DATA_TYPE,          VAL_DOUBLE));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_PERCENT, ATTR_FRAME_COLOR,        VAL_LT_GRAY));
	errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_PERCENT, ATTR_TEXT_FONT,          VAL_DIALOG_META_FONT));
	errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_PERCENT, ATTR_TEXT_POINT_SIZE,    11));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_PERCENT, ATTR_TEXT_BGCOLOR,       VAL_WHITE));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_PERCENT, ATTR_TEXT_JUSTIFY,       VAL_LEFT_JUSTIFIED));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_PERCENT, ATTR_FORMAT,             VAL_FLOATING_PT_FORMAT));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_PERCENT, ATTR_MAX_VALUE,          100.0));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_PERCENT, ATTR_MIN_VALUE,          0.0));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_PERCENT, ATTR_INCR_VALUE,         1.0));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_PERCENT, ATTR_PRECISION,          0));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_PERCENT, ATTR_SHOW_INCDEC_ARROWS, 0));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_PERCENT, ATTR_DIG_DISP_TOP,       73));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_PERCENT, ATTR_DIG_DISP_LEFT,      57));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_PERCENT, ATTR_DIG_DISP_HEIGHT,    19));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_PERCENT, ATTR_DIG_DISP_WIDTH,     75));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_PERCENT, ATTR_SHOW_DIG_DISP,      0));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_PERCENT, ATTR_NEEDLE_COLOR,       VAL_BLACK));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_PERCENT, ATTR_FILL_HOUSING_COLOR, VAL_WHITE));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_PERCENT, ATTR_MARKER_STYLE,       VAL_NO_INNER_MARKERS));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_PERCENT, ATTR_TICK_STYLE,         VAL_FULL_TICKS));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_PERCENT, ATTR_FILL_COLOR,         VAL_BLUE));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_PERCENT, ATTR_FILL_OPTION,        VAL_LEFT_FILL));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_PERCENT, ATTR_LABEL_TOP,          63));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_PERCENT, ATTR_LABEL_LEFT,         50));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_PERCENT, ATTR_LABEL_HEIGHT,       15));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_PERCENT, ATTR_LABEL_WIDTH,        64));
	errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_PERCENT, ATTR_LABEL_FONT,         VAL_DIALOG_META_FONT));
	errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_PERCENT, ATTR_LABEL_POINT_SIZE,   11));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_PERCENT, ATTR_LABEL_BGCOLOR,      TFTP_PANEL_COLOR));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_PERCENT, ATTR_LABEL_RAISED,       TRUE));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_PERCENT, ATTR_LABEL_SIZE_TO_TEXT, TRUE));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_PERCENT, ATTR_DFLT_VALUE, 0.0));
    errChk(DefaultCtrl (hPanel, TFTP_PANEL_BD_PERCENT));

    // Build control: TFTP_PANEL_GUI_PERCENT
    // "% Completed"
    errChk(TFTP_PANEL_GUI_PERCENT = NewCtrl (hPanel, CTRL_NUMERIC_FLAT_HSLIDE, LDBf(Ldb_TFTP_GUIpercent_lbl), 228, 142));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_PERCENT, ATTR_CONSTANT_NAME, "GUI_PERCENT"));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_PERCENT, ATTR_HEIGHT,             16));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_PERCENT, ATTR_WIDTH,              160));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_PERCENT, ATTR_CTRL_MODE,          VAL_INDICATOR));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_PERCENT, ATTR_DATA_TYPE,          VAL_DOUBLE));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_PERCENT, ATTR_FRAME_COLOR,        VAL_LT_GRAY));
	errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_PERCENT, ATTR_TEXT_FONT,          VAL_DIALOG_META_FONT));
	errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_PERCENT, ATTR_TEXT_POINT_SIZE,    11));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_PERCENT, ATTR_TEXT_BGCOLOR,       VAL_WHITE));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_PERCENT, ATTR_TEXT_JUSTIFY,       VAL_LEFT_JUSTIFIED));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_PERCENT, ATTR_FORMAT,             VAL_FLOATING_PT_FORMAT));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_PERCENT, ATTR_MAX_VALUE,          100.0));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_PERCENT, ATTR_MIN_VALUE,          0.0));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_PERCENT, ATTR_INCR_VALUE,         1.0));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_PERCENT, ATTR_PRECISION,          0));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_PERCENT, ATTR_SHOW_INCDEC_ARROWS, 0));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_PERCENT, ATTR_DIG_DISP_TOP,       268));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_PERCENT, ATTR_DIG_DISP_LEFT,      162));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_PERCENT, ATTR_DIG_DISP_HEIGHT,    19));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_PERCENT, ATTR_DIG_DISP_WIDTH,     75));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_PERCENT, ATTR_SHOW_DIG_DISP,      0));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_PERCENT, ATTR_NEEDLE_COLOR,       VAL_BLACK));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_PERCENT, ATTR_FILL_HOUSING_COLOR, VAL_WHITE));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_PERCENT, ATTR_MARKER_STYLE,       VAL_NO_INNER_MARKERS));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_PERCENT, ATTR_TICK_STYLE,         VAL_FULL_TICKS));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_PERCENT, ATTR_FILL_COLOR,         VAL_BLUE));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_PERCENT, ATTR_FILL_OPTION,        VAL_LEFT_FILL));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_PERCENT, ATTR_LABEL_TOP,          230));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_PERCENT, ATTR_LABEL_LEFT,         50));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_PERCENT, ATTR_LABEL_HEIGHT,       15));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_PERCENT, ATTR_LABEL_WIDTH,        67));
	errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_PERCENT, ATTR_LABEL_FONT,         VAL_DIALOG_META_FONT));
	errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_PERCENT, ATTR_LABEL_POINT_SIZE,   11));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_PERCENT, ATTR_LABEL_BGCOLOR,      TFTP_PANEL_COLOR));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_PERCENT, ATTR_LABEL_RAISED,       TRUE));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_PERCENT, ATTR_LABEL_SIZE_TO_TEXT, TRUE));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_PERCENT, ATTR_DFLT_VALUE, 0.0));
    errChk(DefaultCtrl (hPanel, TFTP_PANEL_GUI_PERCENT));

    // Build control: TFTP_PANEL_BD_BLOCK_SENT
    // "Blocks Sent:"
    errChk(TFTP_PANEL_BD_BLOCK_SENT = NewCtrl (hPanel, CTRL_NUMERIC, LDBf(Ldb_TFTP_BDUblocks_lbl), 105, 190));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_BLOCK_SENT, ATTR_CONSTANT_NAME, "BD_BLOCK_SENT"));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_BLOCK_SENT, ATTR_WIDTH,              75));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_BLOCK_SENT, ATTR_CTRL_MODE,          VAL_INDICATOR));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_BLOCK_SENT, ATTR_DATA_TYPE,          VAL_INTEGER));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_BLOCK_SENT, ATTR_FRAME_COLOR,        VAL_LT_GRAY));
	errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_BLOCK_SENT, ATTR_TEXT_FONT,          VAL_DIALOG_META_FONT));
	errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_BLOCK_SENT, ATTR_TEXT_POINT_SIZE,    11));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_BLOCK_SENT, ATTR_TEXT_BGCOLOR,       VAL_WHITE));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_BLOCK_SENT, ATTR_TEXT_JUSTIFY,       VAL_CENTER_JUSTIFIED));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_BLOCK_SENT, ATTR_MAX_VALUE,          2147483647));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_BLOCK_SENT, ATTR_MIN_VALUE,          0));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_BLOCK_SENT, ATTR_INCR_VALUE,         1));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_BLOCK_SENT, ATTR_PRECISION,          0));
//  errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_BLOCK_SENT, ATTR_SHOW_INCDEC_ARROWS, 1));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_BLOCK_SENT, ATTR_LABEL_TOP,          108));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_BLOCK_SENT, ATTR_LABEL_LEFT,         70));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_BLOCK_SENT, ATTR_LABEL_HEIGHT,       15));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_BLOCK_SENT, ATTR_LABEL_WIDTH,        63));
	errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_BLOCK_SENT, ATTR_LABEL_FONT,         VAL_DIALOG_META_FONT));
	errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_BLOCK_SENT, ATTR_LABEL_POINT_SIZE,   11));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_BLOCK_SENT, ATTR_LABEL_BGCOLOR,      TFTP_PANEL_COLOR));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_BLOCK_SENT, ATTR_LABEL_SIZE_TO_TEXT, TRUE));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_BLOCK_SENT, ATTR_DFLT_VALUE, 0));
    errChk(DefaultCtrl (hPanel, TFTP_PANEL_BD_BLOCK_SENT));

    // Build control: TFTP_PANEL_BD_TOTAL
    // "Total Blocks:"
    errChk(TFTP_PANEL_BD_TOTAL = NewCtrl (hPanel, CTRL_NUMERIC, LDBf(Ldb_TFTP_BDUtotal_lbl), 127, 190));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_TOTAL, ATTR_CONSTANT_NAME, "BD_TOTAL"));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_TOTAL, ATTR_WIDTH,              75));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_TOTAL, ATTR_CTRL_MODE,          VAL_INDICATOR));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_TOTAL, ATTR_DATA_TYPE,          VAL_INTEGER));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_TOTAL, ATTR_FRAME_COLOR,        VAL_LT_GRAY));
	errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_TOTAL, ATTR_TEXT_FONT,          VAL_DIALOG_META_FONT));
	errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_TOTAL, ATTR_TEXT_POINT_SIZE,    11));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_TOTAL, ATTR_TEXT_BGCOLOR,       VAL_WHITE));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_TOTAL, ATTR_TEXT_JUSTIFY,       VAL_CENTER_JUSTIFIED));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_TOTAL, ATTR_MAX_VALUE,          2147483647));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_TOTAL, ATTR_MIN_VALUE,          -2147483648));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_TOTAL, ATTR_INCR_VALUE,         1));
//  errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_TOTAL, ATTR_FORMAT,             VAL_DECIMAL_FORMAT));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_TOTAL, ATTR_PRECISION,          0));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_TOTAL, ATTR_SHOW_INCDEC_ARROWS, 0));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_TOTAL, ATTR_LABEL_TOP,          130));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_TOTAL, ATTR_LABEL_LEFT,         70));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_TOTAL, ATTR_LABEL_HEIGHT,       15));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_TOTAL, ATTR_LABEL_WIDTH,        65));
	errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_TOTAL, ATTR_LABEL_FONT,         VAL_DIALOG_META_FONT));
	errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_TOTAL, ATTR_LABEL_POINT_SIZE,   11));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_TOTAL, ATTR_LABEL_BGCOLOR,      TFTP_PANEL_COLOR));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_TOTAL, ATTR_LABEL_SIZE_TO_TEXT, TRUE));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_TOTAL, ATTR_DFLT_VALUE, 0));
    errChk(DefaultCtrl (hPanel, TFTP_PANEL_BD_TOTAL));

    // Build control: TFTP_PANEL_GUI_BLOCK_SENT
    // "Blocks Sent:"
    errChk(TFTP_PANEL_GUI_BLOCK_SENT = NewCtrl (hPanel, CTRL_NUMERIC, LDBf(Ldb_TFTP_GUIblocks_lbl), 272, 190));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_BLOCK_SENT, ATTR_CONSTANT_NAME, "GUI_BLOCK_SENT"));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_BLOCK_SENT, ATTR_WIDTH,              75));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_BLOCK_SENT, ATTR_CTRL_MODE,          VAL_INDICATOR));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_BLOCK_SENT, ATTR_DATA_TYPE,          VAL_INTEGER));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_BLOCK_SENT, ATTR_FRAME_COLOR,        VAL_LT_GRAY));
	errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_BLOCK_SENT, ATTR_TEXT_FONT,          VAL_DIALOG_META_FONT));
	errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_BLOCK_SENT, ATTR_TEXT_POINT_SIZE,    11));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_BLOCK_SENT, ATTR_TEXT_BGCOLOR,       VAL_WHITE));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_BLOCK_SENT, ATTR_TEXT_JUSTIFY,       VAL_CENTER_JUSTIFIED));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_BLOCK_SENT, ATTR_MAX_VALUE,          2147483647));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_BLOCK_SENT, ATTR_MIN_VALUE,          0));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_BLOCK_SENT, ATTR_INCR_VALUE,         1));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_BLOCK_SENT, ATTR_PRECISION,          0));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_BLOCK_SENT, ATTR_SHOW_INCDEC_ARROWS, 0));
//  errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_BLOCK_SENT, ATTR_NO_EDIT_TEXT,       0));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_BLOCK_SENT, ATTR_LABEL_TOP,          275));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_BLOCK_SENT, ATTR_LABEL_LEFT,         70));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_BLOCK_SENT, ATTR_LABEL_HEIGHT,       15));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_BLOCK_SENT, ATTR_LABEL_WIDTH,        63));
	errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_BLOCK_SENT, ATTR_LABEL_FONT,         VAL_DIALOG_META_FONT));
	errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_BLOCK_SENT, ATTR_LABEL_POINT_SIZE,   11));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_BLOCK_SENT, ATTR_LABEL_BGCOLOR,      TFTP_PANEL_COLOR));
//  errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_BLOCK_SENT, ATTR_LABEL_RAISED,       FALSE));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_BLOCK_SENT, ATTR_LABEL_SIZE_TO_TEXT, TRUE));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_BLOCK_SENT, ATTR_DFLT_VALUE, 0));
    errChk(DefaultCtrl (hPanel, TFTP_PANEL_GUI_BLOCK_SENT));

    // Build control: TFTP_PANEL_GUI_TOTAL
    // "Total Blocks:"
    errChk(TFTP_PANEL_GUI_TOTAL = NewCtrl (hPanel, CTRL_NUMERIC, LDBf(Ldb_TFTP_GUItotal_lbl), 294, 190));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_TOTAL, ATTR_CONSTANT_NAME, "GUI_TOTAL"));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_TOTAL, ATTR_WIDTH,              75));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_TOTAL, ATTR_CTRL_MODE,          VAL_INDICATOR));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_TOTAL, ATTR_DATA_TYPE,          VAL_INTEGER));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_TOTAL, ATTR_FRAME_COLOR,        VAL_LT_GRAY));
	errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_TOTAL, ATTR_TEXT_FONT,          VAL_DIALOG_META_FONT));
	errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_TOTAL, ATTR_TEXT_POINT_SIZE,    11));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_TOTAL, ATTR_TEXT_BGCOLOR,       VAL_WHITE));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_TOTAL, ATTR_TEXT_JUSTIFY,       VAL_CENTER_JUSTIFIED));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_TOTAL, ATTR_MAX_VALUE,          2147483647));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_TOTAL, ATTR_MIN_VALUE,          -2147483648));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_TOTAL, ATTR_INCR_VALUE,         1));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_TOTAL, ATTR_PRECISION,          0));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_TOTAL, ATTR_SHOW_INCDEC_ARROWS, 0));
//  errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_TOTAL, ATTR_NO_EDIT_TEXT,       0));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_TOTAL, ATTR_LABEL_TOP,          297));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_TOTAL, ATTR_LABEL_LEFT,         70));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_TOTAL, ATTR_LABEL_HEIGHT,       15));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_TOTAL, ATTR_LABEL_WIDTH,        65));
	errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_TOTAL, ATTR_LABEL_FONT,         VAL_DIALOG_META_FONT));
	errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_TOTAL, ATTR_LABEL_POINT_SIZE,   11));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_TOTAL, ATTR_LABEL_BGCOLOR,      TFTP_PANEL_COLOR));
//  errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_TOTAL, ATTR_LABEL_RAISED,       FALSE));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_TOTAL, ATTR_LABEL_SIZE_TO_TEXT, TRUE));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_TOTAL, ATTR_DFLT_VALUE, 0));
    errChk(DefaultCtrl (hPanel, TFTP_PANEL_GUI_TOTAL));

    // Build control: TFTP_PANEL_BD_PROGRAM
    errChk(TFTP_PANEL_BD_PROGRAM = NewCtrl (hPanel, CTRL_RAISED_BOX, "", 28, 41));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_PROGRAM, ATTR_CONSTANT_NAME, "BD_PROGRAM"));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_PROGRAM, ATTR_HEIGHT,      120));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_PROGRAM, ATTR_WIDTH,       285));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_PROGRAM, ATTR_CTRL_MODE,   VAL_INDICATOR));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_PROGRAM, ATTR_FRAME_COLOR, VAL_LT_GRAY));

    // Build control: TFTP_PANEL_BD_MSG
    // "Updating BDU Flash Rom"
    errChk(TFTP_PANEL_BD_MSG = NewCtrl (hPanel, CTRL_TEXT_MSG, "", 75, 132));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_MSG, ATTR_CONSTANT_NAME, "BD_MSG"));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_MSG, ATTR_HEIGHT,          15));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_MSG, ATTR_WIDTH,           117));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_MSG, ATTR_CTRL_MODE,       VAL_INDICATOR));
	errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_MSG, ATTR_TEXT_FONT,       VAL_DIALOG_META_FONT));
	errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_MSG, ATTR_TEXT_POINT_SIZE, 11));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_MSG, ATTR_TEXT_BGCOLOR,    TFTP_PANEL_COLOR));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_MSG, ATTR_TEXT_RAISED,     TRUE));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_MSG, ATTR_SIZE_TO_TEXT,    TRUE));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_MSG, ATTR_TEXT_JUSTIFY,    VAL_LEFT_JUSTIFIED));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_MSG, ATTR_DFLT_VALUE,      LDBf(Ldb_TFTP_BDUmsg)));
    errChk(DefaultCtrl (hPanel, TFTP_PANEL_BD_MSG));
	errChk(CenterCtrl(hPanel, TFTP_PANEL_BD_MSG, 75, 41, 15, 285));

    // Build control: TFTP_PANEL_BD_COMPLETE
    // "BDU PROCESS COMPLETE"
    errChk(TFTP_PANEL_BD_COMPLETE = NewCtrl (hPanel, CTRL_TEXT_MSG, "", 75, 116));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_COMPLETE, ATTR_CONSTANT_NAME, "BD_COMPLETE"));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_COMPLETE, ATTR_HEIGHT,          15));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_COMPLETE, ATTR_WIDTH,           153));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_COMPLETE, ATTR_CTRL_MODE,       VAL_INDICATOR));
	errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_COMPLETE, ATTR_TEXT_FONT,       VAL_DIALOG_META_FONT));
	errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_COMPLETE, ATTR_TEXT_POINT_SIZE, 11));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_COMPLETE, ATTR_TEXT_BOLD,       TRUE));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_COMPLETE, ATTR_TEXT_BGCOLOR,    TFTP_PANEL_COLOR));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_COMPLETE, ATTR_TEXT_RAISED,     TRUE));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_COMPLETE, ATTR_SIZE_TO_TEXT,    TRUE));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_COMPLETE, ATTR_TEXT_JUSTIFY,    VAL_LEFT_JUSTIFIED));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_COMPLETE, ATTR_DFLT_VALUE,      LDBf(Ldb_TFTP_BDUcomplete)));
    errChk(DefaultCtrl (hPanel, TFTP_PANEL_BD_COMPLETE));
	CenterCtrl(hPanel, TFTP_PANEL_BD_COMPLETE, 75, 41, 15, 285);

    // Build control: TFTP_PANEL_GUI_PROGRAM
    errChk(TFTP_PANEL_GUI_PROGRAM = NewCtrl (hPanel, CTRL_RAISED_BOX, "", 195, 41));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_PROGRAM, ATTR_CONSTANT_NAME, "GUI_PROGRAM"));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_PROGRAM, ATTR_HEIGHT,      120));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_PROGRAM, ATTR_WIDTH,       285));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_PROGRAM, ATTR_CTRL_MODE,   VAL_INDICATOR));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_PROGRAM, ATTR_FRAME_COLOR, VAL_LT_GRAY));

    // Build control: TFTP_PANEL_GUI_MSG
    // "Updating GUI Flash Rom"
    errChk(TFTP_PANEL_GUI_MSG = NewCtrl (hPanel, CTRL_TEXT_MSG, "", 244, 128));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_MSG, ATTR_CONSTANT_NAME, "GUI_MSG"));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_MSG, ATTR_HEIGHT,          15));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_MSG, ATTR_WIDTH,           124));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_MSG, ATTR_CTRL_MODE,       VAL_INDICATOR));
	errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_MSG, ATTR_TEXT_FONT,       VAL_DIALOG_META_FONT));
	errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_MSG, ATTR_TEXT_POINT_SIZE, 11));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_MSG, ATTR_TEXT_BGCOLOR,    TFTP_PANEL_COLOR));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_MSG, ATTR_TEXT_RAISED,     TRUE));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_MSG, ATTR_SIZE_TO_TEXT,    TRUE));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_MSG, ATTR_TEXT_JUSTIFY,    VAL_LEFT_JUSTIFIED));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_MSG, ATTR_DFLT_VALUE,      LDBf(Ldb_TFTP_GUImsg)));
    errChk(DefaultCtrl (hPanel, TFTP_PANEL_GUI_MSG));
	CenterCtrl(hPanel, TFTP_PANEL_GUI_MSG, 244, 41, 15, 285);

    // Build control: TFTP_PANEL_GUI_COMPLETE
    // "GUI PROCESS COMPLETE"
    errChk(TFTP_PANEL_GUI_COMPLETE = NewCtrl (hPanel, CTRL_TEXT_MSG, "", 244, 106));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_COMPLETE, ATTR_CONSTANT_NAME, "GUI_COMPLETE"));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_COMPLETE, ATTR_HEIGHT,          15));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_COMPLETE, ATTR_WIDTH,           158));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_COMPLETE, ATTR_CTRL_MODE,       VAL_INDICATOR));
	errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_COMPLETE, ATTR_TEXT_FONT,       VAL_DIALOG_META_FONT));
	errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_COMPLETE, ATTR_TEXT_POINT_SIZE, 11));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_COMPLETE, ATTR_TEXT_BOLD,       TRUE));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_COMPLETE, ATTR_TEXT_BGCOLOR,    TFTP_PANEL_COLOR));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_COMPLETE, ATTR_TEXT_RAISED,     TRUE));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_COMPLETE, ATTR_SIZE_TO_TEXT,    TRUE));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_COMPLETE, ATTR_TEXT_JUSTIFY,    VAL_LEFT_JUSTIFIED));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_COMPLETE, ATTR_DFLT_VALUE,      LDBf(Ldb_TFTP_GUIcomplete)));
    errChk(DefaultCtrl (hPanel, TFTP_PANEL_GUI_COMPLETE));
	CenterCtrl(hPanel, TFTP_PANEL_GUI_COMPLETE, 244, 41, 15, 285);

    // Set up ZPLANE order
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_COMPLETE,   ATTR_ZPLANE_POSITION, nZplaneOrder++));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_COMPLETE,    ATTR_ZPLANE_POSITION, nZplaneOrder++));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_MSG,         ATTR_ZPLANE_POSITION, nZplaneOrder++));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_PROGRAM,     ATTR_ZPLANE_POSITION, nZplaneOrder++));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_MSG,        ATTR_ZPLANE_POSITION, nZplaneOrder++));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_PROGRAM,    ATTR_ZPLANE_POSITION, nZplaneOrder++));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_TOTAL,      ATTR_ZPLANE_POSITION, nZplaneOrder++));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_BLOCK_SENT, ATTR_ZPLANE_POSITION, nZplaneOrder++));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_PERCENT,    ATTR_ZPLANE_POSITION, nZplaneOrder++));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_FILE,       ATTR_ZPLANE_POSITION, nZplaneOrder++));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_TOTAL,       ATTR_ZPLANE_POSITION, nZplaneOrder++));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_PERCENT,     ATTR_ZPLANE_POSITION, nZplaneOrder++));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_FILE,        ATTR_ZPLANE_POSITION, nZplaneOrder++));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_BLOCK_SENT,  ATTR_ZPLANE_POSITION, nZplaneOrder++));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_LED,        ATTR_ZPLANE_POSITION, nZplaneOrder++));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_LED,         ATTR_ZPLANE_POSITION, nZplaneOrder++));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_GROUP_BOX,  ATTR_ZPLANE_POSITION, nZplaneOrder++));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_GROUP_BOX,   ATTR_ZPLANE_POSITION, nZplaneOrder++));

    // Set up TAB order
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_LED,        ATTR_CTRL_TAB_POSITION, nTabOrder++));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_LED,         ATTR_CTRL_TAB_POSITION, nTabOrder++));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_BLOCK_SENT,  ATTR_CTRL_TAB_POSITION, nTabOrder++));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_FILE,        ATTR_CTRL_TAB_POSITION, nTabOrder++));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_PERCENT,     ATTR_CTRL_TAB_POSITION, nTabOrder++));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_BD_TOTAL,       ATTR_CTRL_TAB_POSITION, nTabOrder++));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_FILE,       ATTR_CTRL_TAB_POSITION, nTabOrder++));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_PERCENT,    ATTR_CTRL_TAB_POSITION, nTabOrder++));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_BLOCK_SENT, ATTR_CTRL_TAB_POSITION, nTabOrder++));
    errChk(SetCtrlAttribute (hPanel, TFTP_PANEL_GUI_TOTAL,      ATTR_CTRL_TAB_POSITION, nTabOrder++));

    // Finalize panel colors, positioning, and sizing
    errChk(SetPanelAttribute (hPanel, ATTR_CONFORM_TO_SYSTEM,        FALSE));
    errChk(SetPanelAttribute (hPanel, ATTR_SCALE_CONTENTS_ON_RESIZE, FALSE));

    //We're done!
    return hPanel;
Error:
//    if (bitmapId)
//        DiscardBitmap (bitmapId);
    return error;
}

/*****************************************************************************/
/* Name:      DnloadStateCB                                                  
/*                                                                           
/* Purpose:  Callback for options on the firmware download setup panel.
/*                                          
/*                                                                           
/* Example Call:   int CVICALLBACK DnloadStateCB(int panel, int control, int event,
/*      	void *callbackData, int eventData1, int eventData2);                                    
/*                                                                           
/* Input:    standard callbacdk arguments
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   Always returns 0                                                         
/*                                                                           
/*****************************************************************************/
int CVICALLBACK DnloadStateCB(int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int checked;

	switch(event){
		case EVENT_COMMIT:
			GetCtrlVal(panel, control, &checked);
			if(!checked){
				SetCtrlVal(panel, control, TRUE);	
				if(control == STATEPANEL_INITIAL_DLOAD){
					DloadType = 0;
				}
				else if(control == STATEPANEL_REPLACE_BOTH_DLOAD){
					DloadType = 1;
				}
				return 0;
			}
			else{
				if(control == STATEPANEL_INITIAL_DLOAD){
					SetCtrlVal(panel, STATEPANEL_INITIAL_DLOAD,      TRUE);
					SetCtrlVal(panel, STATEPANEL_REPLACE_BOTH_DLOAD, FALSE);
					DloadType = 0;
				}
				else if(control == STATEPANEL_REPLACE_BOTH_DLOAD){
					SetCtrlVal(panel, STATEPANEL_INITIAL_DLOAD,      FALSE);
					SetCtrlVal(panel, STATEPANEL_REPLACE_BOTH_DLOAD, TRUE);
					DloadType = 1;
				}
			}
			break;
	} // end switch(event)

	return 0;
}

