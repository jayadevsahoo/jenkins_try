// TabLogDlg.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "TabLogDlg.h"
#include "swat.h"



// CTabLogDlg dialog

IMPLEMENT_DYNAMIC(CTabLogDlg, CDialog)

CTabLogDlg::CTabLogDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CTabLogDlg::IDD, pParent)
	, m_strLogStatus(_T(""))
{
	m_bHideError = false;
	m_bFreeze = false;
}

CTabLogDlg::~CTabLogDlg()
{
}

void CTabLogDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_STATUS, m_strLogStatus);
	DDX_Control(pDX, IDC_CHECK_CRITICAL, m_Chk_Critical);
}


BEGIN_MESSAGE_MAP(CTabLogDlg, CDialog)

	ON_BN_CLICKED(IDC_CHECK_FREEZE, &CTabLogDlg::OnBnClickedCheckFreeze)
	ON_BN_CLICKED(IDC_CHECK_POP_ERROR_MSG, &CTabLogDlg::OnBnClickedCheckPopErrorMsg)
	ON_BN_CLICKED(IDC_BUTTON_EXPAND, &CTabLogDlg::OnBnClickedButtonExpand)
END_MESSAGE_MAP()


// CTabLogDlg message handlers


// these are internal SWAT logs, not test status
void CTabLogDlg::UpdateSWATLog(CString &str)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	pApp->m_SWAT_log_file.WriteString(str);
	pApp->m_SWAT_log_file.Flush();

	if((str.Find("LM_ERROR") >= 0) || (str.Find("LM_CRITICAL") >=0)) //Post critical errors to the main App
	{
			CString* p = new CString(str);
			BOOL b = ::PostMessage(pApp->m_pMainWnd->m_hWnd,WM_CRITICAL_ERROR_RCVD, 0, reinterpret_cast<LPARAM>(p));
			str = "-->" + str;
	
			pApp->GUI_IODisconnect();


	}
	
	if(IsWindow(m_hWnd))
	{
		str.TrimRight();
		m_strLogStatus = m_strLogStatus + "\r\n" +str;


		int nLength = m_strLogStatus.GetLength();
		if( nLength > 100*1024)
		{
			m_strLogStatus = "Truncated...\r\n" + m_strLogStatus.Right(1024);
		}

		if(!m_bFreeze)//don't update if frozen
		{
			CWnd *wndEdit = GetDlgItem(IDC_EDIT_STATUS);
			if(wndEdit)
			{
				wndEdit->SetRedraw(FALSE); 
				wndEdit->SetWindowTextA(m_strLogStatus);
				int nLines = ((CEdit*)wndEdit)->GetLineCount();
				((CEdit*)wndEdit)->LineScroll(nLines,0); //make sure edit has WS_HSCROLL style added.

				wndEdit->SetRedraw(TRUE); 

				//If SWAT was opened thru cmd line AND session is over, kill SWAT
				if((str.Find("Completed session") > 0))
				{
					//exit SWAT if it was from cmd prompt
					if(pApp->m_lpCmdLine[0] != _T('\0'))
					{
						AfxGetMainWnd()->SendMessage(WM_CLOSE);
					}
				}

			
			}
			
		}	

	}



	



}

BOOL CTabLogDlg::OnInitDialog()
{

	CheckDlgButton(IDC_CHECK_FREEZE, BST_UNCHECKED);
	
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	if(pApp->m_bPopErrorMsg)
		CheckDlgButton(IDC_CHECK_POP_ERROR_MSG, BST_CHECKED);
	
	return true;
}

CSwatApp* pApp = (CSwatApp*)AfxGetApp();
void CTabLogDlg::OnBnClickedCheckFreeze()
{
	m_bFreeze = IsDlgButtonChecked(IDC_CHECK_FREEZE) ? true : false;
}

void CTabLogDlg::OnBnClickedCheckPopErrorMsg()
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	pApp->m_bPopErrorMsg = IsDlgButtonChecked(IDC_CHECK_POP_ERROR_MSG);
}

void CTabLogDlg::OnBnClickedButtonExpand()
{
	CString str(m_strLogStatus);
	CWaitCursor wait;
	str.Replace("\r\n", "\n");

	CString strTemp;
	CStdioFile file( TEMP_EXPAND_LOG_FILE, CFile::modeCreate | CFile::modeWrite | CFile::typeText );
	file.WriteString(str);
	file.Close();

	ShellExecute(0, "open", "notepad.exe", TEMP_EXPAND_LOG_FILE, 0, SW_SHOW);
}
