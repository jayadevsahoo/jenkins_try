#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
// Class: FakeServiceModeManager - Fakes out the interface between the
// Service Data subsystem and GUI.  Generates randomly changing
// pieces of Service Data.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Data/vcssrc/FakeServiceModeManager.ccv   25.0.4.0   19 Nov 2013 14:21:02   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002   By: mnr   Date: 24-Mar-2010   SCR Number: 6436
//  Project:  PROX4
//  Description:
//      PROX SST related updates.
// 
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================


#if defined FAKE_SM
#include "Sigma.hh"
# include <stdlib.h>
#include "FakeServiceModeManager.hh"

#ifdef SIGMA_TARGET		// FORNOW - for random() on the target
	extern "C" { int rand(); }
#	define random rand
#endif // SIGMA_TARGET

//@ Usage-Classes
#include "ServiceDataMgr.hh"
#include "TestDatum.hh"
#include "NetworkApp.hh"
#include "SDTimerRegistrar.hh"
#include "SDTimerId.hh"
#include "SmPackets.hh"
#include "NovRamManager.hh"

#ifdef SIGMA_TARGET
#	include "Task.hh"
#	include "MsgQueue.hh"
#	include "MailBox.hh"
#	include "UserAnnunciationMsg.hh"

#	define private public		    	// Allow access to all data
#	define protected public			// Allow access to all data
#	include "SmManager.hh"
#	include "SmRefs.hh"
#	include "IpcIds.hh"
#endif // SIGMA_TARGET

//@ End-Usage

//@ Code...

Int FakeServiceModeManager::MaxPromptSequence_[] =  { -1 };

SmTestId::ServiceModeTestId FakeServiceModeManager::CurrentTestId_ = SmTestId::TEST_NOT_START_ID;
Boolean FakeServiceModeManager::RepeatTestRequested_ = FALSE;
Int FakeServiceModeManager::CurrentPromptSequence_ = -1;
SmPromptId::ActionId FakeServiceModeManager::UserRequestToStopTest_ = SmPromptId::NULL_ACTION_ID;
void (*FakeServiceModeManager::PTestFunction_[SmTestId::NUM_SERVICE_TEST_ID])(void);
Boolean FakeServiceModeManager::isGuiTest_[SmTestId::NUM_SERVICE_TEST_ID];
Int FakeServiceModeManager::setTestSteps = -1;

//
// Uint boundary-aligned memory pools for static members.
//
static Uint FakeSMControlTimerMemory[(sizeof(FakeSMControlTimer) + sizeof(Uint) - 1)
															/ sizeof(Uint)];
FakeSMControlTimer& FakeServiceModeManager::testTimer_ =
			*((FakeSMControlTimer *) FakeSMControlTimerMemory);

int FakeServiceModeManager::counter = 0;
Boolean FakeServiceModeManager::setExactData_ = FALSE;

#if defined SIGMA_TARGET
MsgQueue serviceModeTestQ(SERVICE_MODE_TEST_Q);
#endif	// SIGMA_TARGET

#if defined SIGMA_UNIT_TEST
SmStatusId::TestStatusId FakeServiceModeManager::TestStatusId_=SmStatusId::TEST_ALERT;
SmStatusId::TestConditionId FakeServiceModeManager::TestConditionId_=SmStatusId::CONDITION_2;
#endif	// SIGMA_UNIT_TEST


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: FakeServiceModeManager()  [Default Constructor]
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

FakeServiceModeManager::FakeServiceModeManager(void)
{
	CALL_TRACE("FakeServiceModeManager::FakeServiceModeManager(void)");

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~FakeServiceModeManager  [Destructor]
//
//@ Interface-Description
//  none
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

FakeServiceModeManager::~FakeServiceModeManager(void)
{
	CALL_TRACE("FakeServiceModeManager::~FakeServiceModeManager(void)");
}


#if defined (SIGMA_BD_CPU)
//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method:  RecvTestMode				[static]
//
//@ Interface-Description
// This method receives a packet of GUI Service data from the network.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
// 		(pData && ( sizeof(TestDataPacket) == size ))
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
FakeServiceModeManager::RecvTestMode(XmitDataMsgId, void* pData, Uint size)
{
	CALL_TRACE("FakeServiceModeManager::RecvTestMode(XmitDataMsgId, void *pData, Uint size)");

	CLASS_ASSERTION((pData) && (sizeof(SmMsgPacket) == size ))

	SmMsgPacket *smMsgPacket;
	smMsgPacket = (SmMsgPacket *) pData;

	switch (smMsgPacket->packetType)
	{
	case TEST_REQUEST:
#if defined FORNOW
printf("FakeServiceModeManager::RecvTestMode(), testId=%d, repeatTest=%d\n",
							smMsgPacket->packetData.testRequested.testId,
							smMsgPacket->packetData.testRequested.testRepeat,
							smMsgPacket->packetData.packetParameters));
#endif 	// FORNOW

		DoTest(smMsgPacket->packetData.testRequested.testId,
				smMsgPacket->packetData.testRequested.testRepeat,
				smMsgPacket->packetData.packetParameters);
		break;
	case FUNCTION_REQUEST:
#if defined FORNOW
printf("FakeServiceModeManager::RecvTestMode(), functionId=%d\n",
							smMsgPacket->packetData.functionId);
#endif 	// FORNOW
	
		DoFunction(smMsgPacket->packetData.functionId);
		break;
	}
	
}

//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method:  RecvActionData				[static]
//
//@ Interface-Description
// This method receives a packet of GUI Service data from the network.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
// 		(pData && ( sizeof(ActionDataPacket) == size ))
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
FakeServiceModeManager::RecvActionData(XmitDataMsgId, void* pData, Uint size)
{
	SmPromptId::ActionId *responseId;

	CALL_TRACE("FakeServiceModeManager::RecvActionData(XmitDataMsgId, void *pData, Uint size)");
	CLASS_PRE_CONDITION((pData) &&
				(sizeof(SmPromptId::ActionId) == size ))

	responseId = (SmPromptId::ActionId *) pData;

	OperatorAction(CurrentTestId_, *responseId);
}

#endif 	// SIGMA_BD_CPU


void
setTestToNotApplicable(void)
{
#if defined SIGMA_BD_CPU
	NovRamManager::SetEstTestToNotApplicable(SmTestId::EST_TEST_GAS_SUPPLY_ID);
	NovRamManager::SetEstTestToNotApplicable(SmTestId::EST_TEST_BD_LAMP_ID);
	NovRamManager::SetEstTestToNotApplicable(SmTestId::EST_TEST_BD_AUDIO_ID);
	NovRamManager::SetEstTestToNotApplicable(SmTestId::EST_TEST_FS_CROSS_CHECK_ID);
	NovRamManager::SetEstTestToNotApplicable(SmTestId::EST_TEST_CIRCUIT_PRESSURE_ID);
	NovRamManager::SetEstTestToNotApplicable(SmTestId::EST_TEST_SAFETY_SYSTEM_ID);
	NovRamManager::SetEstTestToNotApplicable(SmTestId::EST_TEST_EXH_VALVE_SEAL_ID);
	NovRamManager::SetEstTestToNotApplicable(SmTestId::EST_TEST_EXH_VALVE_ID);
	NovRamManager::SetEstTestToNotApplicable(SmTestId::EST_TEST_SM_LEAK_ID);
	NovRamManager::SetEstTestToNotApplicable(SmTestId::EST_TEST_EXH_HEATER_ID);
	NovRamManager::SetEstTestToNotApplicable(SmTestId::EST_TEST_GENERAL_ELECTRONICS_ID);
	NovRamManager::SetEstTestToNotApplicable(SmTestId::EST_TEST_BATTERY_ID);
	NovRamManager::SetEstTestToNotApplicable(SmTestId::EST_TEST_GUI_KEYBOARD_ID);
	NovRamManager::SetEstTestToNotApplicable(SmTestId::EST_TEST_GUI_KNOB_ID);
	NovRamManager::SetEstTestToNotApplicable(SmTestId::EST_TEST_GUI_LAMP_ID);
	NovRamManager::SetEstTestToNotApplicable(SmTestId::EST_TEST_GUI_AUDIO_ID);
	NovRamManager::SetEstTestToNotApplicable(SmTestId::EST_TEST_GUI_TOUCH_ID);
	NovRamManager::SetEstTestToNotApplicable(SmTestId::EST_TEST_GUI_SERIAL_PORT_ID);
	NovRamManager::SetEstTestToNotApplicable(SmTestId::EST_TEST_GUI_NURSE_CALL_ID);

	NovRamManager::SetSstTestToNotApplicable(SmTestId::SST_TEST_CIRCUIT_RESISTANCE_ID);
	NovRamManager::SetSstTestToNotApplicable(SmTestId::SST_TEST_EXPIRATORY_FILTER_ID);
	NovRamManager::SetSstTestToNotApplicable(SmTestId::SST_TEST_CIRCUIT_LEAK_ID);
	NovRamManager::SetSstTestToNotApplicable(SmTestId::SST_TEST_CIRCUIT_PRESSURE_ID);
	NovRamManager::SetSstTestToNotApplicable(SmTestId::SST_TEST_FLOW_SENSORS_ID);
	NovRamManager::SetSstTestToNotApplicable(SmTestId::SST_TEST_COMPLIANCE_CALIBRATION_ID);
	NovRamManager::SetSstTestToNotApplicable(SmTestId::SST_TEST_PROX_ID);
#endif	// 	SIGMA_BD_CPU
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Initialize [static]
//
//@ Interface-Description
// This method initializes the KeyPanel class.  This consists of setting
// the callback target for each key to NULL and setting the keyEnabled_
// Boolean array to TRUE.
//---------------------------------------------------------------------
//@ Implementation-Description
// Just sets all the key callback targets to NULL and sets the keyEnabled_
// Boolean array to TRUE.
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//@ End-Method
//=====================================================================
void
FakeServiceModeManager::Initialize(void)
{
	CALL_TRACE("FakeServiceModeManager::Initialize(void)");

	SmTestId::Initialize();
	
#if defined (SIGMA_BD_CPU)
	NetworkApp::RegisterRecvMsg(SERVICE_MODE_MSG,
								FakeServiceModeManager::RecvTestMode);
	NetworkApp::RegisterRecvMsg(SERVICE_MODE_ACTION,
								FakeServiceModeManager::RecvActionData);
#endif 	// SIGMA_BD_CPU

	new (&testTimer_) FakeSMControlTimer;

#if defined (SIGMA_SUN)
	// Initialize the test to GUI ONLY for SUN environment.
	isGuiTest_[SmTestId::INITIALIZE_FLOW_SENSORS_ID] = TRUE;
	isGuiTest_[SmTestId::GAS_SUPPLY_TEST_ID] = TRUE;
	isGuiTest_[SmTestId::CIRCUIT_RESISTANCE_TEST_ID] = TRUE;
	isGuiTest_[SmTestId::SST_FILTER_TEST_ID] = TRUE;
	isGuiTest_[SmTestId::GUI_KEYBOARD_TEST_ID] = TRUE;
	isGuiTest_[SmTestId::GUI_KNOB_TEST_ID] = TRUE;
	isGuiTest_[SmTestId::GUI_LAMP_TEST_ID] = TRUE;
	isGuiTest_[SmTestId::BD_LAMP_TEST_ID] = TRUE;
	isGuiTest_[SmTestId::GUI_AUDIO_TEST_ID] = TRUE;
	isGuiTest_[SmTestId::BD_AUDIO_TEST_ID] = TRUE;
	isGuiTest_[SmTestId::FS_CROSS_CHECK_TEST_ID] = TRUE;
	isGuiTest_[SmTestId::SST_FS_CC_TEST_ID] = TRUE;
	isGuiTest_[SmTestId::LOW_FLOW_FS_CC_TEST_ID] = TRUE;
	isGuiTest_[SmTestId::HIGH_FLOW_FS_CC_TEST_ID] = TRUE;
	isGuiTest_[SmTestId::CIRCUIT_PRESSURE_TEST_ID] = TRUE;
	isGuiTest_[SmTestId::SAFETY_SYSTEM_TEST_ID] = TRUE;
	isGuiTest_[SmTestId::EXH_VALVE_SEAL_TEST_ID] = TRUE;
	isGuiTest_[SmTestId::EXH_VALVE_TEST_ID] = TRUE;
	isGuiTest_[SmTestId::EXH_VALVE_CALIB_TEST_ID] = TRUE;
	isGuiTest_[SmTestId::SM_LEAK_TEST_ID] = TRUE;
	isGuiTest_[SmTestId::SST_LEAK_TEST_ID] = TRUE;
	isGuiTest_[SmTestId::COMPLIANCE_CALIB_TEST_ID] = TRUE;
	isGuiTest_[SmTestId::EXH_HEATER_TEST_ID] = TRUE;
	isGuiTest_[SmTestId::GENERAL_ELECTRONICS_TEST_ID] = TRUE;
	isGuiTest_[SmTestId::GUI_TOUCH_TEST_ID] = TRUE;
	isGuiTest_[SmTestId::GUI_SERIAL_PORT_TEST_ID] = TRUE;
	isGuiTest_[SmTestId::BATTERY_TEST_ID] = TRUE;
	isGuiTest_[SmTestId::RESTART_BD_ID] = TRUE;
	isGuiTest_[SmTestId::RESTART_GUI_ID] = TRUE;
	isGuiTest_[SmTestId::BD_INIT_SERIAL_NUMBER_ID] = TRUE;
	isGuiTest_[SmTestId::GUI_INIT_SERIAL_NUMBER_ID] = TRUE;
	isGuiTest_[SmTestId::CAL_INFO_DUPLICATION_ID] = TRUE;
	isGuiTest_[SmTestId::BD_VENT_INOP_ID] = TRUE;
	isGuiTest_[SmTestId::GUI_VENT_INOP_ID] = TRUE;
	isGuiTest_[SmTestId::BD_INIT_DEFAULT_SERIAL_NUMBER_ID] = TRUE;
	isGuiTest_[SmTestId::GUI_INIT_DEFAULT_SERIAL_NUMBER_ID] = TRUE;
	isGuiTest_[SmTestId::FS_CALIBRATION_ID] = TRUE;
	isGuiTest_[SmTestId::GUI_NURSE_CALL_TEST_ID] = TRUE;
	isGuiTest_[SmTestId::ATMOS_PRESSURE_CAL_ID] = TRUE;
	isGuiTest_[SmTestId::ATMOS_PRESSURE_READ_ID] = TRUE;
#else
	setTestToNotApplicable();
	
	// Initialize the test to either GUI or BD test for TARGET environemnt.
	isGuiTest_[SmTestId::INITIALIZE_FLOW_SENSORS_ID] = TRUE;
	isGuiTest_[SmTestId::GAS_SUPPLY_TEST_ID] = TRUE;
	isGuiTest_[SmTestId::CIRCUIT_RESISTANCE_TEST_ID] = TRUE;
	isGuiTest_[SmTestId::SST_FILTER_TEST_ID] = TRUE;
	isGuiTest_[SmTestId::GUI_KEYBOARD_TEST_ID] = TRUE;
	isGuiTest_[SmTestId::GUI_KNOB_TEST_ID] = TRUE;
	isGuiTest_[SmTestId::GUI_LAMP_TEST_ID] = TRUE;
	isGuiTest_[SmTestId::BD_LAMP_TEST_ID] = TRUE;
	isGuiTest_[SmTestId::GUI_AUDIO_TEST_ID] = TRUE;
	isGuiTest_[SmTestId::BD_AUDIO_TEST_ID] = TRUE;
	isGuiTest_[SmTestId::FS_CROSS_CHECK_TEST_ID] = TRUE;
	isGuiTest_[SmTestId::SST_FS_CC_TEST_ID] = TRUE;
	isGuiTest_[SmTestId::LOW_FLOW_FS_CC_TEST_ID] = TRUE;
	isGuiTest_[SmTestId::HIGH_FLOW_FS_CC_TEST_ID] = TRUE;
	isGuiTest_[SmTestId::CIRCUIT_PRESSURE_TEST_ID] = TRUE;
	isGuiTest_[SmTestId::SAFETY_SYSTEM_TEST_ID] = TRUE;
	isGuiTest_[SmTestId::EXH_VALVE_SEAL_TEST_ID] = TRUE;
	isGuiTest_[SmTestId::EXH_VALVE_TEST_ID] = TRUE;
	isGuiTest_[SmTestId::EXH_VALVE_CALIB_TEST_ID] = TRUE;
	isGuiTest_[SmTestId::SM_LEAK_TEST_ID] = TRUE;
	isGuiTest_[SmTestId::SST_LEAK_TEST_ID] = TRUE;
	isGuiTest_[SmTestId::COMPLIANCE_CALIB_TEST_ID] = TRUE;
	isGuiTest_[SmTestId::EXH_HEATER_TEST_ID] = TRUE;
	isGuiTest_[SmTestId::GENERAL_ELECTRONICS_TEST_ID] = TRUE;
	isGuiTest_[SmTestId::GUI_TOUCH_TEST_ID] = TRUE;
	isGuiTest_[SmTestId::GUI_SERIAL_PORT_TEST_ID] = TRUE;
	isGuiTest_[SmTestId::BATTERY_TEST_ID] = TRUE;
	isGuiTest_[SmTestId::RESTART_BD_ID] = TRUE;
	isGuiTest_[SmTestId::RESTART_GUI_ID] = TRUE;
	isGuiTest_[SmTestId::BD_INIT_SERIAL_NUMBER_ID] = TRUE;
	isGuiTest_[SmTestId::GUI_INIT_SERIAL_NUMBER_ID] = TRUE;
	isGuiTest_[SmTestId::CAL_INFO_DUPLICATION_ID] = TRUE;
	isGuiTest_[SmTestId::BD_VENT_INOP_ID] = TRUE;
	isGuiTest_[SmTestId::GUI_VENT_INOP_ID] = TRUE;
	isGuiTest_[SmTestId::BD_INIT_DEFAULT_SERIAL_NUMBER_ID] = TRUE;
	isGuiTest_[SmTestId::GUI_INIT_DEFAULT_SERIAL_NUMBER_ID] = TRUE;
	isGuiTest_[SmTestId::FS_CALIBRATION_ID] = TRUE;
	isGuiTest_[SmTestId::GUI_NURSE_CALL_TEST_ID] = TRUE;
	isGuiTest_[SmTestId::ATMOS_PRESSURE_CAL_ID] = TRUE;
	isGuiTest_[SmTestId::ATMOS_PRESSURE_READ_ID] = TRUE;
#endif	// SIGMA_SUN

	// Initialize the test function pointer into the PTestFunction array
	PTestFunction_[SmTestId::INITIALIZE_FLOW_SENSORS_ID] = initializeFlowSensor;
	PTestFunction_[SmTestId::GAS_SUPPLY_TEST_ID] = gasSupply;
	PTestFunction_[SmTestId::CIRCUIT_RESISTANCE_TEST_ID] = circuitResistance;
	PTestFunction_[SmTestId::SST_FILTER_TEST_ID] = sstFilter;
	PTestFunction_[SmTestId::GUI_KEYBOARD_TEST_ID] = guiKeyboard;
	PTestFunction_[SmTestId::GUI_KNOB_TEST_ID] = guiKnob;
	PTestFunction_[SmTestId::GUI_LAMP_TEST_ID] = guiLamp;
	PTestFunction_[SmTestId::BD_LAMP_TEST_ID] = bdLamp;
	PTestFunction_[SmTestId::GUI_AUDIO_TEST_ID] = guiAudio;
	PTestFunction_[SmTestId::BD_AUDIO_TEST_ID] = bdAudio;
	PTestFunction_[SmTestId::FS_CROSS_CHECK_TEST_ID] = fsCrossCheck;
	PTestFunction_[SmTestId::SST_FS_CC_TEST_ID] = sstFsCcTest;
	PTestFunction_[SmTestId::LOW_FLOW_FS_CC_TEST_ID] = lowFlowFsCc;
	PTestFunction_[SmTestId::HIGH_FLOW_FS_CC_TEST_ID] = highFlowFsCc;
	PTestFunction_[SmTestId::CIRCUIT_PRESSURE_TEST_ID] = circuitPressure;
	PTestFunction_[SmTestId::SAFETY_SYSTEM_TEST_ID] = safetySystem;
	PTestFunction_[SmTestId::EXH_VALVE_SEAL_TEST_ID] = exhValveSeal;
	PTestFunction_[SmTestId::EXH_VALVE_TEST_ID] = exhValve;
	PTestFunction_[SmTestId::EXH_VALVE_CALIB_TEST_ID] = exhValveCalib;
	PTestFunction_[SmTestId::SM_LEAK_TEST_ID] = smLeak;
	PTestFunction_[SmTestId::SST_LEAK_TEST_ID] = sstLeak;
	PTestFunction_[SmTestId::COMPLIANCE_CALIB_TEST_ID] = complianceCalib;
	PTestFunction_[SmTestId::EXH_HEATER_TEST_ID] = exhHeater;
	PTestFunction_[SmTestId::GENERAL_ELECTRONICS_TEST_ID] = generalElectronics;
	PTestFunction_[SmTestId::GUI_TOUCH_TEST_ID] = guiTouch;
	PTestFunction_[SmTestId::GUI_SERIAL_PORT_TEST_ID] = guiSerialPort;
	PTestFunction_[SmTestId::BATTERY_TEST_ID] = battery;
	PTestFunction_[SmTestId::RESTART_BD_ID] = restartBd;
	PTestFunction_[SmTestId::RESTART_GUI_ID] = restartGui;
	PTestFunction_[SmTestId::BD_INIT_SERIAL_NUMBER_ID] = bdSerialNumber;
	PTestFunction_[SmTestId::GUI_INIT_SERIAL_NUMBER_ID] = guiSerialNumber;
	PTestFunction_[SmTestId::CAL_INFO_DUPLICATION_ID] = calInfoDuplication;
	PTestFunction_[SmTestId::BD_VENT_INOP_ID] = BdventInop;
	PTestFunction_[SmTestId::GUI_VENT_INOP_ID] = GuiventInop;
	PTestFunction_[SmTestId::BD_INIT_DEFAULT_SERIAL_NUMBER_ID] = bdInitDefaultSerialNumber;
	PTestFunction_[SmTestId::GUI_INIT_DEFAULT_SERIAL_NUMBER_ID] = guiInitDefaultSerialNumber;
	PTestFunction_[SmTestId::FS_CALIBRATION_ID] = fsCalibration;
	PTestFunction_[SmTestId::GUI_NURSE_CALL_TEST_ID] = guiNurseCall;
	PTestFunction_[SmTestId::ATMOS_PRESSURE_CAL_ID] = atmPressureTransducerCal;
	PTestFunction_[SmTestId::ATMOS_PRESSURE_READ_ID] = atmPressureTransducerRead;

	MaxPromptSequence_[SmTestId::INITIALIZE_FLOW_SENSORS_ID] = 0;
	MaxPromptSequence_[SmTestId::GAS_SUPPLY_TEST_ID] = 8;
	MaxPromptSequence_[SmTestId::CIRCUIT_RESISTANCE_TEST_ID] = 3;
	MaxPromptSequence_[SmTestId::SST_FILTER_TEST_ID] = 3;
	MaxPromptSequence_[SmTestId::GUI_KEYBOARD_TEST_ID] = 15;
	MaxPromptSequence_[SmTestId::GUI_KNOB_TEST_ID] = 2;
	MaxPromptSequence_[SmTestId::GUI_LAMP_TEST_ID] = 4;
	MaxPromptSequence_[SmTestId::BD_LAMP_TEST_ID] = 4;
	MaxPromptSequence_[SmTestId::GUI_AUDIO_TEST_ID] = 1;
	MaxPromptSequence_[SmTestId::BD_AUDIO_TEST_ID] = 1;
	MaxPromptSequence_[SmTestId::FS_CROSS_CHECK_TEST_ID] = 2;
	MaxPromptSequence_[SmTestId::SST_FS_CC_TEST_ID] = 2;
	MaxPromptSequence_[SmTestId::LOW_FLOW_FS_CC_TEST_ID] = 2;
	MaxPromptSequence_[SmTestId::HIGH_FLOW_FS_CC_TEST_ID] = 2;
	MaxPromptSequence_[SmTestId::CIRCUIT_PRESSURE_TEST_ID] = 1;
	MaxPromptSequence_[SmTestId::SAFETY_SYSTEM_TEST_ID] = 1;
	MaxPromptSequence_[SmTestId::EXH_VALVE_SEAL_TEST_ID] = 0;
	MaxPromptSequence_[SmTestId::EXH_VALVE_TEST_ID] = 1;
	MaxPromptSequence_[SmTestId::EXH_VALVE_CALIB_TEST_ID] = 3;
	MaxPromptSequence_[SmTestId::SM_LEAK_TEST_ID] = 1;
	MaxPromptSequence_[SmTestId::SST_LEAK_TEST_ID] = 1;
	MaxPromptSequence_[SmTestId::COMPLIANCE_CALIB_TEST_ID] = 1;
	MaxPromptSequence_[SmTestId::EXH_HEATER_TEST_ID] = 0;
#if defined SIGMA_UNIT_TEST
	MaxPromptSequence_[SmTestId::GENERAL_ELECTRONICS_TEST_ID] = 5;
#else
	MaxPromptSequence_[SmTestId::GENERAL_ELECTRONICS_TEST_ID] = 0;
#endif	// 	SIGMA_UNIT_TEST
	MaxPromptSequence_[SmTestId::GUI_TOUCH_TEST_ID] = 1;
	MaxPromptSequence_[SmTestId::GUI_SERIAL_PORT_TEST_ID] = 0;
	MaxPromptSequence_[SmTestId::BATTERY_TEST_ID] = 4;
	MaxPromptSequence_[SmTestId::RESTART_BD_ID] = 1;
	MaxPromptSequence_[SmTestId::RESTART_GUI_ID] = 1;
	MaxPromptSequence_[SmTestId::BD_INIT_SERIAL_NUMBER_ID] = 2;
	MaxPromptSequence_[SmTestId::GUI_INIT_SERIAL_NUMBER_ID] = 2;
	MaxPromptSequence_[SmTestId::CAL_INFO_DUPLICATION_ID] = 0;
	MaxPromptSequence_[SmTestId::BD_VENT_INOP_ID] = 11;
	MaxPromptSequence_[SmTestId::GUI_VENT_INOP_ID] = 0;
	MaxPromptSequence_[SmTestId::BD_INIT_DEFAULT_SERIAL_NUMBER_ID] = 0;
	MaxPromptSequence_[SmTestId::GUI_INIT_DEFAULT_SERIAL_NUMBER_ID] = 0;
	MaxPromptSequence_[SmTestId::FS_CALIBRATION_ID] = 3;
	MaxPromptSequence_[SmTestId::GUI_NURSE_CALL_TEST_ID] = 3;
	MaxPromptSequence_[SmTestId::ATMOS_PRESSURE_CAL_ID] = 3;
	MaxPromptSequence_[SmTestId::ATMOS_PRESSURE_READ_ID] = 3;

	CurrentTestId_ = SmTestId::TEST_NOT_START_ID;
	RepeatTestRequested_ = FALSE;
	CurrentPromptSequence_ = 0;
	UserRequestToStopTest_ = SmPromptId::NULL_ACTION_ID;
	setTestSteps = -1;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Task [public, static]
//
//@ Interface-Description
// Entry point for the GUI-Applications task on the target system.  This
// is an infinite loop which pends for messages on the User Annunciation
// queue and forwards the various types of messages received to the
// appropriate object/handler.  The message format is specified in a
// UserAnnunciationMsg structure.  After handling a message (or group of
// messages) the Upper and Lower screens are repainted so that any graphical
// changes which occurred as a result of the message(s) are displayed.
//---------------------------------------------------------------------
//@ Implementation-Description
// In an infinite loop, we first repaint the Upper and Lower screens,
// hence updating the display with the latest changes.  We then wait
// on the User Annunciation Queue.  If a message is received on the
// queue we decode it and execute the appropriate action.
//
// We only wait 100msec (?) for a new message on the queue.  If no message
// is received in that time then we can retrieve any new waveform data,
// update the screen and then return to waiting for a message on the queue.
// Waveforms are updated every iteration through the infinite loop, whether
// messages are available or not.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
FakeServiceModeManager::Task(void)
{
#ifdef SIGMA_TARGET
	MsgQueue userAnnunciationQ(SERVICE_MODE_TASK_Q);
												// Handle to User Annunc. queue
	Int32 qWord;								// 32 bits from the queue
	UserAnnunciationMsg eventMsg;				// Structure of 32 bits qWord
	Int32 rval;									// Return value

	while(1)
	{
		eventMsg.qWord = 0;	// Clear previous event data

		rval = userAnnunciationQ.pendForMsg(qWord, 100);

		// Check to see if we read something
		if (rval == Ipc::OK)
		{
			// Plug event data into our UserAnnunciationMsg union so that
			// we can interpret the 32 bits in the message.
			eventMsg.qWord = qWord;
#if defined FORNOW
printf("FakeServiceModeManager::Task, eventMsg.touchParts.eventType=%d\n", eventMsg.touchParts.eventType);
#endif 	// FORNOW

			switch (eventMsg.touchParts.eventType)
			{
			// Timer event
			case UserAnnunciationMsg::TIMER_EVENT:
				// Inform the timer registrar which will inform the
				// appropriate object that it's timer has gone off.
				SDTimerRegistrar::TimerEventHappened(eventMsg);
				break;
			}
		}
	} // the end of while
#endif // SIGMA_TARGET
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: DoTest [static]
//
//@ Interface-Description
//  none
//---------------------------------------------------------------------
//@ Implementation-Description
//  Callback for timer event, a concrete implementation of a pure
//  virtual function.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void 
FakeServiceModeManager::DoTest(SmTestId::ServiceModeTestId testId, Boolean testRepeat, const char * const pParameters)
{
	CALL_TRACE("FakeServiceModeManager::DoTest(SmTestId::ServiceModeTestId testId, Boolean testRepeat)");

	CurrentTestId_ = testId;
	RepeatTestRequested_ = testRepeat;
	
	CLASS_PRE_CONDITION(CurrentTestId_ < SmTestId::NUM_SERVICE_TEST_ID &&
						CurrentTestId_ > SmTestId::TEST_NOT_START_ID);

	SDTimerRegistrar::RegisterTarget(SDTimerId::SERVICE_DATA_TIMER, &testTimer_);
	CurrentPromptSequence_ = 0;
	UserRequestToStopTest_ = SmPromptId::NULL_ACTION_ID;
	setTestSteps = -1;
	
#if defined (FORNOW)
printf("FakeServiceModeManager::DoTest(%d, %d)\n", (Int) testId, testRepeat);
#endif	// (FORNOW)


#if defined (FORNOW)
printf("PTestFunction_[CurrentTestId_] = %d\n",PTestFunction_[testId]);
#endif	// (FORNOW)


#if defined (SIGMA_GUI_CPU) || defined (SIGMA_COMMON)    

	if (isGuiTest_[testId])
	{
		testTimer_.startTimer();
	}
	else
	{
#if defined FORNOW
printf("   before calling NetworkApp::XmitMsg(SERVICE_MODE_MSG)\n");
#endif 	// FORNOW

#	if defined (SIGMA_TARGET)    
		SmMsgPacket smMsgPacket;

		smMsgPacket.packetType = TEST_REQUEST;
		smMsgPacket.packetData.testRequested.testId = testId;
		smMsgPacket.packetData.testRequested.testRepeat = testRepeat;
		
		NetworkApp::XmitMsg(SERVICE_MODE_MSG, (void *) &smMsgPacket,
							sizeof(smMsgPacket));
#	elif defined (SIGMA_SUN)
		testTimer_.startTimer();
#	endif // SIGMA_TARGET
	}
	
#elif defined(SIGMA_BD_CPU)

	testTimer_.startTimer();

#endif // SIGMA_GUI_CPU || SIGMA_COMMON

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: DoFunction [static]
//
//@ Interface-Description
//  none
//---------------------------------------------------------------------
//@ Implementation-Description
//  Callback for timer event, a concrete implementation of a pure
//  virtual function.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void 
FakeServiceModeManager::DoFunction(SmTestId::FunctionId functionId)
{
	CALL_TRACE("FakeServiceModeManager::DoFunction(SmTestId::FunctionId functionId)");

	CLASS_PRE_CONDITION(functionId < SmTestId::FUNCTION_ID_MAX &&
						functionId > SmTestId::FUNCTION_ID_START);

#if defined (FORNOW)
printf("FakeServiceModeManager::DoFunction() functionId = %d\n", functionId);
#endif	// (FORNOW)
			

#if defined (SIGMA_GUI_CPU) || defined (SIGMA_COMMON)    

#if defined FORNOW
printf("   before calling NetworkApp::XmitMsg(SERVICE_MODE_MSG)\n");
#endif 	// FORNOW

#	if defined (SIGMA_TARGET)    
		SmMsgPacket smMsgPacket;

		smMsgPacket.packetType = FUNCTION_REQUEST;
		smMsgPacket.packetData.functionId = functionId;
		
		NetworkApp::XmitMsg(SERVICE_MODE_MSG, (void *) &smMsgPacket,
							sizeof(smMsgPacket));

#	endif // SIGMA_TARGET
							
#endif // SIGMA_GUI_CPU || SIGMA_COMMON

#if defined (SIGMA_TARGET)
	Int32 rMsg;
	
	RSmManager.functionRequestEnd_ = RSmManager.functionRequestStart_ = 0;

#	if defined (SIGMA_BD_CPU)
	if (CurrentTestId_ != SmTestId::TEST_NOT_START_ID)
	{
		if (serviceModeTestQ.isMsgAvail())
		{
			serviceModeTestQ.acceptMsg(rMsg);
		}
	}
#	endif // SIGMA_BD_CPU	
#endif // SIGMA_TARGET

	// Processing functionId information
	switch (functionId)
	{
	case SmTestId::EST_PASSED_ID:
		break;
	case SmTestId::SET_TEST_TYPE_EST_ID:
		ServiceDataMgr::SetTestType(SmTestId::SM_EST_TYPE);
		break;
	case SmTestId::SET_TEST_TYPE_SST_ID:
		ServiceDataMgr::SetTestType(SmTestId::SM_SST_TYPE);
		break;
	case SmTestId::SET_TEST_TYPE_REMOTE_ID:
		ServiceDataMgr::SetTestType(SmTestId::SM_EXTERNAL_TYPE);
		break;
	case SmTestId::SET_TEST_TYPE_MISC_ID:
		ServiceDataMgr::SetTestType(SmTestId::SM_MISC_TYPE);
		break;
	case SmTestId::EST_OVERRIDE_ID:
		// Update to NovRam the override status result.
		NovRamManager::EstOverridden();
		break;
	case SmTestId::SST_OVERRIDE_ID:
		// Update to NovRam the override status result.
		NovRamManager::SstOverridden();
		break;
	case SmTestId::EST_COMPLETE_ID:
		// Update to NovRam the complete status result.
		NovRamManager::CompletedEst();
		break;
	case SmTestId::SST_COMPLETE_ID:
		// Update to NovRam the complete status result.
		NovRamManager::CompletedSst();
		break;
	case SmTestId::BD_DOWNLOAD_ID:
	case SmTestId::GUI_DOWNLOAD_ID:
		printf("DownLoading in processing...\n");
		break;
	case SmTestId::FORCE_EST_OVERIDE_ID:
		// Update to NovRam the override status result.
		NovRamManager::OverrideAllEstTests();
		break;
	case SmTestId::FORCE_SST_OVERIDE_ID:
		// Update to NovRam the override status result.
		NovRamManager::OverrideAllSstTests();
		break;
	case SmTestId::DEFAULT_FLASH_ID:
		// Update to NovRam the enable vent inop status result.
//		NovRamManager::UpdateVentInopEnabledFlag(TRUE);
		break;
	case SmTestId::DISABLE_VENT_INOP_ID:
		// Update to NovRam the disable vent inop status result.
//		NovRamManager::UpdateVentInopEnabledFlag(FALSE);
		break;
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: OperatorAction [static]
//
//@ Interface-Description
//  none
//---------------------------------------------------------------------
//@ Implementation-Description
//  Callback for timer event, a concrete implementation of a pure
//  virtual function.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void 
FakeServiceModeManager::OperatorAction(SmTestId::ServiceModeTestId smTestId, SmPromptId::ActionId responseId)
{
	CALL_TRACE("FakeServiceModeManager::OperatorAction(SmTestId::ServiceModeTestId smTestId, SmPromptId::ActionId responseId)");
			
	CLASS_PRE_CONDITION(CurrentTestId_ == smTestId);

#if defined (FORNOW)
printf("FakeServiceModeManager::OperatorAction()\n"
			"   CurrentTestId_=%d, responseId=%d, CurrentPromptSequence_ = %d, MaxPromptSequence_[%d] = %d\n",
			CurrentTestId_, responseId, CurrentPromptSequence_, MaxPromptSequence_[CurrentTestId_]);
#endif	// (FORNOW)
			

#if defined (SIGMA_GUI_CPU) || defined (SIGMA_COMMON)    

		if (!isGuiTest_[CurrentTestId_])
		{
#if defined FORNOW
printf("   XmitMsg(SERVICE_MODE_ACTION) from GUI to BD board\n");
#endif 	// FORNOW

#	if defined (SIGMA_TARGET)    
			NetworkApp::XmitMsg(SERVICE_MODE_ACTION, (void *) &responseId, sizeof(responseId));
			return;
#	endif // SIGMA_TARGET
		}
#endif // SIGMA_GUI_CPU || SIGMA_COMMON

			
	switch (responseId)
	{
	case SmPromptId::NULL_ACTION_ID:
		UserRequestToStopTest_ = SmPromptId::NULL_ACTION_ID;
		testTimer_.startTimer();
		break;
		
	case SmPromptId::USER_EXIT:
		// User just decided to stop the current test.
		// All test should be stopped.
		UserRequestToStopTest_ = SmPromptId::USER_EXIT;
		testTimer_.startTimer();
		break;

	case SmPromptId::KEY_ACCEPT:
		// User just confirmed to continue with the current test.
		SAFE_CLASS_ASSERTION(CurrentPromptSequence_ <=
							MaxPromptSequence_[CurrentTestId_]);

		UserRequestToStopTest_ = SmPromptId::NULL_ACTION_ID;
		testTimer_.startTimer();
		break;
	case SmPromptId::KEY_CLEAR:
		// User just confirmed to continue with the current test.
		SAFE_CLASS_ASSERTION(CurrentPromptSequence_ <=
							MaxPromptSequence_[CurrentTestId_]);

		if (smTestId == SmTestId::GUI_LAMP_TEST_ID || smTestId == SmTestId::BD_LAMP_TEST_ID)
		{
			UserRequestToStopTest_ = SmPromptId::KEY_CLEAR;
		}
		else
		{
			UserRequestToStopTest_ = SmPromptId::NULL_ACTION_ID;
		}			
		testTimer_.startTimer();
		break;
	default:
		SAFE_CLASS_ASSERTION(FALSE);
		break;
	}
}


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method:  IsGuiTest()  [static]
//
//@ Interface-Description
//     Determine the passed test is a GUI test or not.
//---------------------------------------------------------------------
//@ Implementation-Description
//     Set the return value to TRUE is the test is a GUI test, else
//     return FALSE.
//---------------------------------------------------------------------
//@ PreCondition
//  smId > TEST_NOT_START_ID && smId < NUM_SERVICE_TEST_ID
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean
FakeServiceModeManager::IsGuiTest(SmTestId::ServiceModeTestId smId)
{
	CALL_TRACE("FakeServiceModeManager:IsGuiTest(SmTestId::ServiceModeTestId smId)");

	CLASS_PRE_CONDITION(smId > SmTestId::TEST_NOT_START_ID &&
						smId < SmTestId::NUM_SERVICE_TEST_ID);

	return(isGuiTest_[(Int) smId]);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: sMTimerEventHappened
//
//@ Interface-Description
//  none
//---------------------------------------------------------------------
//@ Implementation-Description
//  Callback for timer event, a concrete implementation of a pure
//  virtual function.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void 
FakeServiceModeManager::sMTimerEventHappened(void)
{
	testTimer_.stopTimer();

	setTestSteps++;

	CLASS_PRE_CONDITION(setTestSteps >= 0);

#if defined (FORNOW)
printf("FakeServiceModeManager::sMTimerEventHappened()\n");
#endif	// (FORNOW)

	PTestFunction_[CurrentTestId_]();

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
FakeServiceModeManager::SoftFault(const SoftFaultID  softFaultID,
		   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)  
{
	CALL_TRACE("FakeServiceModeManager::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, SERVICE_DATA,
					SERVICE_MODE_MANAGER, lineNumber, pFileName, pPredicate);
}


Boolean
FakeServiceModeManager::randomResult(Int count)
{
	Int accum;

	accum = 0;

#if defined SD_FAILURE	|| defined SD_ALERT
	return(TRUE);
#elif defined SD_PASS
	return(FALSE);
#else	// mixed cases
	for (Int idx = 0; idx < count; idx++)
	{
		if (random() % 2)
		{
			accum++;
		}
	}

	if (accum == count)
		return (TRUE);
	else
		return(FALSE);
#endif	// SD_FAILURE || SD_ALERT
}


void
FakeServiceModeManager::initializeFlowSensor(void)
{
	printf("initializeFlowSensor(void), setTestSteps=%d, at %d\n",
				setTestSteps, __LINE__);

#if defined SIGMA_UNIT_TEST
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
	case 1:
		generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 2:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}

#elif defined SM_UNIT_TEST
	if (counter == 0)
	{
		switch (setTestSteps)
		{
		case START_TEST:
			generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
			break;
		
		case 1:	
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// Unable to read the air flow sensor
				generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;

		case 2:
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// Unable to read the O2 flow sensor
				generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_2);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;

		case 3:
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// Unable to read the O2 flow sensor
				generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_3);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;

		case 4:
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// Unable to read the O2 flow sensor
				generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_4);
			}
			else
			{
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;

		case 5:
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// Unable to read the O2 flow sensor
				generateTestStatusPacket(SmStatusId::TEST_FAILURE, SmStatusId::CONDITION_5);
			}
			else
			{
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;

		case 6:
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
		case 7:
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
		}
	}
	else if (counter == 1)
	{
		switch (setTestSteps)
		{
		case START_TEST:
			generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
			break;
		
		case 1:
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{
				generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
			}
			else
			{	
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
		}
	}

#else
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
		
	case 1:	
		if (randomResult(2))
		{	// Unable to read the air flow sensor
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_1);
		}
		else
		{
			testTimer_.startTimer();
		}
		break;
	case 2:
		if (randomResult(2))
		{	// Unable to read the O2 flow sensor
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_2);
		}
		else
		{
			testTimer_.startTimer();
		}
		break;
	case 3:
		if (randomResult(2))
		{	// Unable to read the O2 flow sensor
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_3);
		}
		else
		{
			testTimer_.startTimer();
		}
		break;
	case 4:
		if (randomResult(2))
		{	// Unable to read the O2 flow sensor
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_4);
		}
		else
		{
			testTimer_.startTimer();
		}
		break;
	case 5:
		if (randomResult(2))
		{	// Unable to read the O2 flow sensor
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_5);
		}
		else
		{
			testTimer_.startTimer();
		}
		break;
	case 6:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// User continues
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		}
		else
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	case 7:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}
#endif	// SIGMA_UNIT_TEST
}


void
FakeServiceModeManager::gasSupply(void)
{

	printf("gasSupply(void), setTestSteps=%d, at %d\n",
				setTestSteps, __LINE__);

#if defined SIGMA_UNIT_TEST
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
	case 1:	
		generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_1);
		break;
	case 2:	
		generateTestStatusPacket(SmStatusId::TEST_FAILURE, SmStatusId::CONDITION_2);
		break;
	case 3:
		generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 4:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}
#elif defined SM_UNIT_TEST
	if (counter != 1 )		// OVERALL STATUS = PASS
	{
		switch (setTestSteps)
		{
		case START_TEST:
			generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
			break;

		case 1:		// TEST_END
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
			break;
	
		case 2:		// TEST_OPERATOR_EXIT
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			break;
	
		default:
			break;
		}
	}
	else if (counter==1)		// OVERALL STATUS = MINOR FAILURE
	{
		switch (setTestSteps)
		{
		case START_TEST:
			generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
			break;

		case 1:		// CONNECT_AIR_PROMPT, ACCEPT_KEY_ONLY
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{
				generateTestPromptPacket(SmPromptId::CONNECT_AIR_PROMPT, SmPromptId::ACCEPT_KEY_ONLY); }
			else if (UserRequestToStopTest_ == SmPromptId::USER_EXIT)
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
	
		case 2:			// CONNECT_O2_PROMPT, ACCEPT_AND_CANCEL_ONLY
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{
				generateTestPromptPacket(SmPromptId::CONNECT_O2_PROMPT, SmPromptId::ACCEPT_AND_CANCEL_ONLY);
			}
			else if (UserRequestToStopTest_ == SmPromptId::USER_EXIT)
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
	
		case 3:		// DISCONNECT_AIR_PROMPT, ACCEPT_PASS_AND_CANCEL_FAIL_ONLY
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{
				generateTestPromptPacket(SmPromptId::DISCONNECT_AIR_PROMPT, SmPromptId::ACCEPT_PASS_AND_CANCEL_FAIL_ONLY);
			}
			else if (UserRequestToStopTest_ == SmPromptId::USER_EXIT)
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
	
		case 4:		// DISCONNECT_O2_PROMPT, ACCEPT_AND_CANCEL_ONLY
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{
				generateTestPromptPacket(SmPromptId::DISCONNECT_O2_PROMPT, SmPromptId::ACCEPT_AND_CANCEL_ONLY);
			}
			else if (UserRequestToStopTest_ == SmPromptId::USER_EXIT)
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
	
		case 5:		// ALERT, CONDITION_4
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_4);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
	
		case 6:		// ALERT, CONDITION_5
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_5);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
	
		case 7:		// TEST_END
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
			break;
	
		case 8:		// TEST_OPERATOR_EXIT
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			break;
	
		default:
			break;
		}
	}
	else if (counter == 6 )		// For Laptop test only
	{
		switch (setTestSteps)
		{
		case START_TEST:
			generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
			break;

		case 1:	
			generateTestPromptPacket(SmPromptId::CONNECT_AIR_PROMPT, SmPromptId::ACCEPT_KEY_ONLY);
			break;
			
		case 2:		// ALERT, CONDITION_5
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_5);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
	
		case 3:
			// air present switch indicates pressure
			generateTestDataPacket(1, 1);
			break;		
		case 4:		// TEST_END
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
			break;
	
		case 5:		// TEST_OPERATOR_EXIT
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			break;
	
		default:
			break;
		}
	}
	else
	{				
		printf ("Illegal FakeServiceModeManager's counter!!!\n");
	}

#else
	
				
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
		
	case 1:	
		generateTestPromptPacket(SmPromptId::CONNECT_AIR_PROMPT, SmPromptId::ACCEPT_KEY_ONLY);
		break;
	case 2:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// User continues
			generateTestPromptPacket(SmPromptId::CONNECT_O2_PROMPT, SmPromptId::ACCEPT_KEY_ONLY);
		}
		else
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	case 3:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// User continues
			generateTestPromptPacket(SmPromptId::DISCNT_TUBE_BFR_EXH_FLTR_PROMPT, SmPromptId::ACCEPT_KEY_ONLY);
		}
		else
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	case 4:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// User continues
			generateTestPromptPacket(SmPromptId::DISCONNECT_AIR_PROMPT, SmPromptId::ACCEPT_KEY_ONLY);
		}
		else
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	case 5:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// User continues
			generateTestDataPacket(1, 1);
		}
		else
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	case 6:
		if (randomResult(2))
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_1);
		}
		else
		{	// air present switch indicates pressure
			generateTestDataPacket(1, 2);
		}
		break;		
	case 7:
		if (randomResult(2))
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_2);
		}
		else
		{	// inspiratory pressure is not 10 cmH2O
			generateTestPromptPacket(SmPromptId::DISCONNECT_O2_PROMPT, SmPromptId::ACCEPT_KEY_ONLY);
		}
		break;		
	case 8:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// User continues
			generateTestDataPacket(1, 3);
		}
		else
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	case 9:
		if (randomResult(2))
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_3);
		}
		else
		{	// O2 present switch indicates pressure
			generateTestPromptPacket(SmPromptId::CONNECT_AIR_PROMPT, SmPromptId::ACCEPT_KEY_ONLY);
		}
		break;		
	case 10:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// User continues
			generateTestDataPacket(1, 4);
		}
		else
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	case 11:
		if (randomResult(2))
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_4);
		}
		else
		{	// air present switch indicates no pressure
			generateTestDataPacket(1, 5);
		}
		break;		
	case 12:
		if (randomResult(2))
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_5);
		}
		else
		{	// inspiratory pressure is not 10 cmH2O
			generateTestPromptPacket(SmPromptId::CONNECT_O2_PROMPT, SmPromptId::ACCEPT_KEY_ONLY);
		}
		break;		
	case 13:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// User continues
			generateTestDataPacket(1, 6);
		}
		else
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	case 14:
		if (randomResult(2))
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_6);
		}
		else
		{	// O2 present switch indicates no pressure
			generateTestPromptPacket(SmPromptId::CONCT_TUBE_BFR_EXH_FLTR_PROMPT, SmPromptId::ACCEPT_KEY_ONLY);
		}
		break;		
	case 15:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// User continues
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		}
		else
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	case 16:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}
#endif	// SIGMA_UNIT_TEST
}


void
FakeServiceModeManager::circuitResistance(void)
{

	printf("circuitResistance(void), setTestSteps=%d, at %d\n",
				setTestSteps, __LINE__);

#if defined SIGMA_UNIT_TEST
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
	case 1:
		generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_1);
		break;		
	case 2:
		generateTestStatusPacket(SmStatusId::TEST_FAILURE, SmStatusId::CONDITION_2);
		break;		
	case 3:
		generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 4:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}

#elif defined SM_UNIT_TEST
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;

	case 1:	
		generateTestPromptPacket(SmPromptId::CONNECT_AIR_PROMPT, SmPromptId::ACCEPT_KEY_ONLY);
		break;

	case 2:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// User continues
			generateTestDataPacket(1, SmDataId::CIRCUIT_RESISTANCE_EXP_PRESSURE);
		}
		else
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;

	case 3:
		generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;

	case 4:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}


#else
	
				
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
		
	case 1:	
		generateTestPromptPacket(SmPromptId::CONNECT_AIR_PROMPT, SmPromptId::ACCEPT_KEY_ONLY);
		break;
	case 2:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// User continues
			generateTestPromptPacket(SmPromptId::UNBLOCK_WYE_PROMPT, SmPromptId::ACCEPT_KEY_ONLY);
		}
		else
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	case 3:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// User continues
			generateTestDataPacket(1, 0);
		}
		else
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;		
	case 4:
		if (randomResult(2))
		{	// inspiratory pressure > 20 cmH2O or < 1.5 cmH2O
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_1);
		}
		else
		{
			testTimer_.startTimer();
		}
		break;
	case 5:
		generateTestPromptPacket(SmPromptId::ATTACH_CIRCUIT_PROMPT, SmPromptId::ACCEPT_KEY_ONLY);
		break;
	case 6:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// User continues
			generateTestDataPacket(2, 1);
		}
		else
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	case 7:
		if (randomResult(2))
		{	// Test Failed
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_2);
		}
		else
		{	// compliance is equal (+- 10%) to the gold standard tubing 
			testTimer_.startTimer();
		}
		break;
	case 8:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		}
		break;
	case 9:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}
#endif	// SIGMA_UNIT_TEST
}



void
FakeServiceModeManager::sstFilter(void)
{
	printf("sstFilter(void), setTestSteps=%d, at %d\n",
				setTestSteps, __LINE__);

#if defined SIGMA_UNIT_TEST
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
	case 1:
		generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 2:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}

#elif defined SM_UNIT_TEST
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;

	case 1:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// User continues
			generateTestDataPacket(1, SmDataId::SST_FILTER_DELTA_PRESSURE);
		}
		else
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;

	case 2:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// User continues
			generateTestDataPacket(1, SmDataId::SST_FILTER_DELTA_PRESSURE+1);
		}
		else
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;

	case 3:
		generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;

	case 4:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}


#else
	
				
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
		
	case 1:	
		generateTestPromptPacket(SmPromptId::CONNECT_AIR_PROMPT, SmPromptId::ACCEPT_KEY_ONLY);
		break;
	case 2:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// User continues
			generateTestPromptPacket(SmPromptId::CONNECT_O2_PROMPT, SmPromptId::ACCEPT_KEY_ONLY);
		}
		else
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	case 3:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// User continues
			generateTestDataPacket(2, 1);
		}
		else
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	case 4:
		generateTestPromptPacket(SmPromptId::CONCT_TUBE_BFR_EXH_FLTR_PROMPT, SmPromptId::ACCEPT_KEY_ONLY);
		break;
	case 5:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// User continues
			generateTestDataPacket(3, 3);
		}
		else
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	case 6:
		if (randomResult(2))
		{	// exhalation pressure > 1 ??? cmH2O
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_1);
		}
		else
		{	
			testTimer_.startTimer();
		}
		break;
	case 7:
		if (randomResult(2))
		{	// inspiratory pressure rise > 3 cmH2O
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_2);
		}
		else
		{	
			testTimer_.startTimer();
		}
		break;
	case 8:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		}
		break;
	case 9:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}
#endif	// SIGMA_UNIT_TEST
}


void
FakeServiceModeManager::guiKeyboard(void)
{
	printf("guiKeyboard(void), setTestSteps=%d, at %d\n",
				setTestSteps, __LINE__);
				
#if defined SIGMA_UNIT_TEST
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
	case 1:
		generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 2:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}

#elif defined SM_UNIT_TEST
	if (counter != 1 )		// OVERALL STATUS = PASS
	{
		switch (setTestSteps)
		{
		case START_TEST:
			generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
			break;

		case 1:	
			generateTestPromptPacket(SmPromptId::KEY_ACCEPT_PROMPT, SmPromptId::NO_KEYS);
			break;
		case 2:		// TEST_END
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
			break;
	
		case 3:		// TEST_OPERATOR_EXIT
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			break;
	
		default:
			break;
		}
	}
	else if (counter==1)		// OVERALL STATUS = MINOR FAILURE
	{
		switch (setTestSteps)
		{
		case START_TEST:
			generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
			break;

		case 1:		// ALERT, CONDITION_1
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
	
		case 2:		// ALERT, CONDITION_2
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_2);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
	
		case 3:		// ALERT, CONDITION_3
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_3);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
	
		case 4:		// TEST_END
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
			break;
	
		case 5:		// TEST_OPERATOR_EXIT
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			break;
	
		default:
			break;
		}
	}
	else
	{				
		printf ("Illegal FakeServiceModeManager's counter!!!\n");
	}


#else
	
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
		
	case 1:	
		generateTestPromptPacket(SmPromptId::KEY_ACCEPT_PROMPT, SmPromptId::NO_KEYS);
		break;
	case 2:
		if (randomResult(2))
		{	// keyboard error?
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_1);
		}
		else
		{	
			testTimer_.startTimer();
		}
		break;
	case 3:
		generateTestPromptPacket(SmPromptId::KEY_CLEAR_PROMPT, SmPromptId::NO_KEYS);
		break;
	case 4:
		if (randomResult(2))
		{	// keyboard error?
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_2);
		}
		else
		{	
			testTimer_.startTimer();
		}
		break;
	case 5:
		generateTestPromptPacket(SmPromptId::KEY_INSP_PAUSE_PROMPT, SmPromptId::NO_KEYS);
		break;
	case 6:
		if (randomResult(2))
		{	// keyboard error?
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_2);
		}
		else
		{	
			testTimer_.startTimer();
		}
		break;
	case 7:
		generateTestPromptPacket(SmPromptId::KEY_EXP_PAUSE_PROMPT, SmPromptId::NO_KEYS);
		break;
	case 8:
		if (randomResult(2))
		{	// keyboard error?
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_2);
		}
		else
		{	
			testTimer_.startTimer();
		}
		break;
	case 9:
		generateTestPromptPacket(SmPromptId::KEY_MAN_INSP_PROMPT, SmPromptId::NO_KEYS);
		break;
	case 10:
		if (randomResult(2))
		{	// keyboard error?
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_2);
		}
		else
		{	
			testTimer_.startTimer();
		}
		break;
	case 11:
		generateTestPromptPacket(SmPromptId::KEY_HUNDRED_PERCENT_O2_PROMPT, SmPromptId::NO_KEYS);
		break;
	case 12:
		if (randomResult(2))
		{	// keyboard error?
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_2);
		}
		else
		{	
			testTimer_.startTimer();
		}
		break;
	case 13:
		generateTestPromptPacket(SmPromptId::KEY_INFO_PROMPT, SmPromptId::NO_KEYS);
		break;
	case 14:
		if (randomResult(2))
		{	// keyboard error?
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_2);
		}
		else
		{	
			testTimer_.startTimer();
		}
		break;
	case 15:
		generateTestPromptPacket(SmPromptId::KEY_ALARM_RESET_PROMPT, SmPromptId::NO_KEYS);
		break;
	case 16:
		if (randomResult(2))
		{	// keyboard error?
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_2);
		}
		else
		{	
			testTimer_.startTimer();
		}
		break;
	case 17:
		generateTestPromptPacket(SmPromptId::KEY_ALARM_SILENCE_PROMPT, SmPromptId::NO_KEYS);
		break;
	case 18:
		if (randomResult(2))
		{	// keyboard error?
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_2);
		}
		else
		{	
			testTimer_.startTimer();
		}
		break;
	case 19:
		generateTestPromptPacket(SmPromptId::KEY_ALARM_VOLUME_PROMPT, SmPromptId::NO_KEYS);
		break;
	case 20:
		if (randomResult(2))
		{	// keyboard error?
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_2);
		}
		else
		{	
			testTimer_.startTimer();
		}
		break;
	case 21:
		generateTestPromptPacket(SmPromptId::KEY_SCREEN_BRIGHTNESS_PROMPT, SmPromptId::NO_KEYS);
		break;
	case 22:
		if (randomResult(2))
		{	// keyboard error?
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_2);
		}
		else
		{	
			testTimer_.startTimer();
		}
		break;
	case 23:
		generateTestPromptPacket(SmPromptId::KEY_SCREEN_CONTRAST_PROMPT, SmPromptId::NO_KEYS);
		break;
	case 24:
		if (randomResult(2))
		{	// keyboard error?
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_2);
		}
		else
		{	
			testTimer_.startTimer();
		}
		break;
	case 25:
		generateTestPromptPacket(SmPromptId::KEY_SCREEN_LOCK_PROMPT, SmPromptId::NO_KEYS);
		break;
	case 26:
		if (randomResult(2))
		{	// keyboard error?
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_2);
		}
		else
		{	
			testTimer_.startTimer();
		}
		break;
	case 27:
		generateTestPromptPacket(SmPromptId::KEY_LEFT_SPARE_PROMPT, SmPromptId::NO_KEYS);
		break;
	case 28:
		if (randomResult(2))
		{	// keyboard error?
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_2);
		}
		else
		{	
			testTimer_.startTimer();
		}
		break;
	case 29:
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 30:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}
#endif	// SIGMA_UNIT_TEST
}


void
FakeServiceModeManager::guiKnob(void)
{
	printf("guiKnob(void), setTestSteps=%d, counter = %d at %d\n",
				setTestSteps, counter, __LINE__);
				
#if defined SIGMA_UNIT_TEST
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
	case 1:	
		generateTestPromptPacket(SmPromptId::TURN_KNOB_LEFT_PROMPT, SmPromptId::USER_EXIT_ONLY);
		break;
	case 2:
		generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 3:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}

#elif defined SM_UNIT_TEST

	if (counter == 0)		// OVERALL STATUS = PASS 
	{
		switch (setTestSteps)
		{
		case START_TEST:
			generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
			break;
		case 1:
			printf("<FakeServiceModeManager> Press EXIT then turn knob left now.\n");
			generateTestPromptPacket(SmPromptId::TURN_KNOB_LEFT_PROMPT, SmPromptId::USER_EXIT_ONLY);
			break;
		case 2:	
			printf("<FakeServiceModeManager> Turn knob right now.\n");
			generateTestPromptPacket(SmPromptId::TURN_KNOB_RIGHT_PROMPT, SmPromptId::USER_EXIT_ONLY);
			break;
		case 3:
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
			break;
		case 4:
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
		}
	}
	else if (counter == 6)	// Laptop unit test
	{
		switch (setTestSteps)
		{
		case START_TEST:
			generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
			break;
		case 1:
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
			break;
		case 2:
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_2);
			break;
		case 3:
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
		}
	}
	else if (counter == 7)	// Laptop unit test
	{
		switch (setTestSteps)
		{
		case START_TEST:
			generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
			break;
		case 1:	
			generateTestPromptPacket(SmPromptId::TURN_KNOB_LEFT_PROMPT, SmPromptId::ACCEPT_PASS_AND_CANCEL_FAIL_ONLY);
			break;
		}
	}
	else if (counter == 8)	// Laptop unit test
	{
		switch (setTestSteps)
		{
		case 2:
			// air present switch indicates pressure
			generateTestDataPacket(1, 1, 123, TRUE);
			break;		
		case 3:
			break;		
		}
	}
#else
	
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
		
	case 1:	
		generateTestPromptPacket(SmPromptId::TURN_KNOB_LEFT_PROMPT, SmPromptId::USER_EXIT_ONLY);
		break;
	case 2:	
		generateTestPromptPacket(SmPromptId::TURN_KNOB_RIGHT_PROMPT, SmPromptId::USER_EXIT_ONLY);
		break;
	case 3:
		if (randomResult(2))
		{	// keyboard error?
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_2);
		}
		else
		{	
			testTimer_.startTimer();
		}
		break;
	case 4:
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 5:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}
#endif	// SIGMA_UNIT_TEST
}


void
FakeServiceModeManager::guiLamp(void)
{
	printf("guiLamp(void), setTestSteps=%d, at %d\n",
				setTestSteps, __LINE__);
				
#if defined SIGMA_UNIT_TEST

	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
		
	case 1:
		generateTestPromptPacket(SmPromptId::HIGH_ALARM_PROMPT, SmPromptId::ACCEPT_PASS_AND_CANCEL_FAIL_ONLY);
		break;
	case 2:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// User continues
			generateTestDataPacket(2, 0);
		}
		else if (UserRequestToStopTest_ == SmPromptId::KEY_CLEAR)
		{	// Test failed
			generateFailedTestStatusPacket(TestStatusId_, SmStatusId::NULL_CONDITION_ITEM);
			TestStatusId_ = SmStatusId::TEST_ALERT;
		}
		else
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	case 3:
		if (UserRequestToStopTest_ != SmPromptId::KEY_CLEAR)
		{	// User continues
			generateTestPromptPacket(SmPromptId::MEDIUM_ALARM_PROMPT, SmPromptId::ACCEPT_PASS_AND_CANCEL_FAIL_ONLY);
		}
		else
		{
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		}
		break;
	case 4:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// User continues
			generateTestDataPacket(2, 2);
		}
		else if (UserRequestToStopTest_ == SmPromptId::KEY_CLEAR)
		{	// Test failed
			generateFailedTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_2);
		}
		else
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	case 5:
		if (UserRequestToStopTest_ != SmPromptId::KEY_CLEAR)
		{	// User continues
			generateTestPromptPacket(SmPromptId::LOW_ALARM_PROMPT, SmPromptId::ACCEPT_PASS_AND_CANCEL_FAIL_ONLY);
		}
		else
		{
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		}
		break;
	case 6:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// User continues
			generateTestDataPacket(2, 4);
		}
		else if (UserRequestToStopTest_ == SmPromptId::KEY_CLEAR)
		{	// Test failed
			generateFailedTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_3);
		}
		else
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	case 7:
		if (UserRequestToStopTest_ != SmPromptId::KEY_CLEAR)
		{	// User continues
			generateTestPromptPacket(SmPromptId::ON_BATTERY_POWER_PROMPT, SmPromptId::ACCEPT_PASS_AND_CANCEL_FAIL_ONLY);
		}
		else
		{
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		}
		break;
	case 8:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// User continues
			generateTestDataPacket(2, 6);
		}
		else if (UserRequestToStopTest_ == SmPromptId::KEY_CLEAR)
		{	// Test failed
			generateFailedTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_4);
		}
		else
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	case 9:
		generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 10:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}

#elif defined SM_UNIT_TEST
	if (counter == 1 )		// OVERALL STATUS = ? 
	{
		switch (setTestSteps)
		{
		case START_TEST:
			generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
			break;
		
		case 1:
			generateTestPromptPacket(SmPromptId::HIGH_ALARM_PROMPT, SmPromptId::ACCEPT_PASS_AND_CANCEL_FAIL_ONLY);
			break;
		case 2:
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				generateTestDataPacket(2, 2);
			}
			else if (UserRequestToStopTest_ == SmPromptId::KEY_CLEAR)
			{	// Test failed
				generateFailedTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
		case 3:
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
			break;
		case 4:
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
		}
	}
	else 		// OVERALL STATUS = PASS 
	{
		switch (setTestSteps)
		{
		case START_TEST:
			generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
			break;
		case 1:
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
			break;
		case 2:
			{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			break;
			}
		}
	}

#else
	
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
		
	case 1:
		generateTestPromptPacket(SmPromptId::HIGH_ALARM_PROMPT, SmPromptId::ACCEPT_PASS_AND_CANCEL_FAIL_ONLY);
		break;
	case 2:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// User continues
			generateTestDataPacket(1, 1);
		}
		else if (UserRequestToStopTest_ == SmPromptId::KEY_CLEAR)
		{	// Test failed
			generateFailedTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_1);
		}
		else
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	case 3:
		if (UserRequestToStopTest_ != SmPromptId::KEY_CLEAR)
		{	// User continues
			generateTestPromptPacket(SmPromptId::MEDIUM_ALARM_PROMPT, SmPromptId::ACCEPT_PASS_AND_CANCEL_FAIL_ONLY);
		}
		else
		{
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		}
		break;
	case 4:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// User continues
			generateTestDataPacket(1, 2);
		}
		else if (UserRequestToStopTest_ == SmPromptId::KEY_CLEAR)
		{	// Test failed
			generateFailedTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_2);
		}
		else
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	case 5:
		if (UserRequestToStopTest_ != SmPromptId::KEY_CLEAR)
		{	// User continues
			generateTestPromptPacket(SmPromptId::LOW_ALARM_PROMPT, SmPromptId::ACCEPT_PASS_AND_CANCEL_FAIL_ONLY);
		}
		else
		{
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		}
		break;
	case 6:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// User continues
			generateTestDataPacket(1, 3);
		}
		else if (UserRequestToStopTest_ == SmPromptId::KEY_CLEAR)
		{	// Test failed
			generateFailedTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_3);
		}
		else
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	case 7:
		if (UserRequestToStopTest_ != SmPromptId::KEY_CLEAR)
		{	// User continues
			generateTestPromptPacket(SmPromptId::ON_BATTERY_POWER_PROMPT, SmPromptId::ACCEPT_PASS_AND_CANCEL_FAIL_ONLY);
		}
		else
		{
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		}
		break;
	case 8:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// User continues
			generateTestDataPacket(1, 4);
		}
		else if (UserRequestToStopTest_ == SmPromptId::KEY_CLEAR)
		{	// Test failed
			generateFailedTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_4);
		}
		else
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	case 9:
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 10:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}
#endif	// SIGMA_UNIT_TEST
}


void
FakeServiceModeManager::bdLamp(void)
{
	printf("bdLamp(void), setTestSteps=%d, at %d\n",
				setTestSteps, __LINE__);
				
#if defined SIGMA_UNIT_TEST
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
	case 1:
		generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 2:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}

#elif defined SM_UNIT_TEST
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
	case 1:
		generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 2:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}


#else
	
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
		
	case 1:
		generateTestPromptPacket(SmPromptId::VENTILATOR_INOPERATIVE_PROMPT, SmPromptId::ACCEPT_PASS_AND_CANCEL_FAIL_ONLY);
		break;
	case 2:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// User continues
			generateTestDataPacket(2, 2);
		}
		else if (UserRequestToStopTest_ == SmPromptId::KEY_CLEAR)
		{	// Test failed
			generateFailedTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_1);
		}
		else
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	case 3:
		if (UserRequestToStopTest_ != SmPromptId::KEY_CLEAR)
		{	// User continues
			generateTestPromptPacket(SmPromptId::SAFETY_VALVE_OPEN_PROMPT, SmPromptId::ACCEPT_PASS_AND_CANCEL_FAIL_ONLY);
		}
		else
		{
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		}
		break;
	case 4:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// User continues
			generateTestDataPacket(2, 2);
		}
		else if (UserRequestToStopTest_ == SmPromptId::KEY_CLEAR)
		{	// Test failed
			generateFailedTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_2);
		}
		else
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	case 5:
		if (UserRequestToStopTest_ != SmPromptId::KEY_CLEAR)
		{	// User continues
			generateTestPromptPacket(SmPromptId::LOSS_OF_GUI_PROMPT, SmPromptId::ACCEPT_PASS_AND_CANCEL_FAIL_ONLY);
		}
		else
		{
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		}
		break;
	case 6:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// User continues
			generateTestDataPacket(2, 2);
		}
		else if (UserRequestToStopTest_ == SmPromptId::KEY_CLEAR)
		{	// Test failed
			generateFailedTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_3);
		}
		else
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	case 7:
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 8:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}
#endif	// SIGMA_UNIT_TEST
}


void
FakeServiceModeManager::guiAudio(void)
{
	printf("guiAudio(void), setTestSteps=%d, at %d\n",
				setTestSteps, __LINE__);
				
#if defined SIGMA_UNIT_TEST
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
	case 1:
		generateTestStatusPacket(SmStatusId::NOT_INSTALLED, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 2:
		generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 3:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}
				
#elif defined SM_UNIT_TEST
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
	case 1:
		generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 2:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}


#else
	
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
		
	case 1:	
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 2:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}
#endif	// SIGMA_UNIT_TEST
}


void
FakeServiceModeManager::bdAudio(void)
{
	printf("bdAudio(void), setTestSteps=%d, at %d\n",
				setTestSteps, __LINE__);
				
#if defined SIGMA_UNIT_TEST
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
	case 1:
		generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 2:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}
				
#elif defined SM_UNIT_TEST
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
	case 1:
		generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 2:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}


#else
	
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
		
	case 1:	
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 2:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}
#endif	// SIGMA_UNIT_TEST
}

void
FakeServiceModeManager::fsCrossCheck(void)
{
	printf("fsCrossCheck(void), setTestSteps=%d, at %d\n",
				setTestSteps, __LINE__);

				
#if defined SIGMA_UNIT_TEST
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
	case 1:
		generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 2:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}
				
#elif defined SM_UNIT_TEST
	if (counter == 0)		// OVERALL STATUS = PASS 
	{
		switch (setTestSteps)
		{
		case START_TEST:
			generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
			break;
		case 1:
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
			break;
		case 2:
			// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			break;
		default:
			break;
		}
	}
	else if (counter==1)		// OVERALL STATUS = MINOR FAILURE
	{
		switch (setTestSteps)
		{
		case START_TEST:
			generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
			break;

		case 1:		// CONNECT_AIR_PROMPT, ACCEPT_KEY_ONLY
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{
				generateTestPromptPacket(SmPromptId::CONNECT_AIR_PROMPT, SmPromptId::ACCEPT_KEY_ONLY);
			}
			else if (UserRequestToStopTest_ == SmPromptId::USER_EXIT)
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
	
		case 2:		// ALERT, CONDITION_1
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
	
		case 3:		// TEST_END
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
			break;
	
		case 4:		// TEST_OPERATOR_EXIT
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			break;
	
		default:
			break;
		}
	}
	else if (counter==2)		// OVERALL STATUS = MINOR FAILURE
	{
		switch (setTestSteps)
		{
		case START_TEST:
			generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
			break;

		case 1:		// ALERT, CONDITION_1
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
	
		case 2:		// TEST_END
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
			break;
	
		case 3:		// TEST_OPERATOR_EXIT
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			break;
	
		default:
			break;
		}
	}
	else if (counter==3)		// OVERALL STATUS = MAJOR FAILURE
	{
		switch (setTestSteps)
		{
		case START_TEST:
			generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
			break;

		case 1:		// FAILURE , CONDITION_2
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				generateTestStatusPacket(SmStatusId::TEST_FAILURE, SmStatusId::CONDITION_2);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
	
		case 2:		// TEST_END
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
			break;
	
		case 3:		// TEST_OPERATOR_EXIT
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			break;
	
		default:
			break;
		}
	}
	else if (counter==4)		// OVERALL STATUS = MAJOR FAILURE
	{
		switch (setTestSteps)
		{
		case START_TEST:
			generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
			break;

		case 1:		// ALERT, CONDITION_2
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_2);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
	
		case 2:		//	DATA 
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				printf("Sending data.\n");	
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
	
		case 3:		//	DATA 
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				printf("Sending data.\n");	
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
	
		case 4:		//	DATA 
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				printf("Sending data.\n");	
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
	
		case 5:		//	DATA 
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				printf("Sending data.\n");	
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
	
		case 6:		//	DATA 
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				printf("Sending data.\n");	
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
	
		case 7:		//	DATA 
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				printf("Sending data.\n");	
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
	
		case 8:		//	DATA 
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				printf("Sending data.\n");	
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
	
		case 9:		//	DATA 
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				printf("Sending data.\n");	
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
	
		case 10:		//	DATA 
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				printf("Sending data.\n");	
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
	
		case 11:		//	DATA 
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				printf("Sending data.\n");	
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
	
		case 12:		// TEST_END
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
			break;
	
		default:
			break;
		}
	}
	else
	{				
		printf ("Illegal FakeServiceModeManager's counter!!!\n");
	}


#else
	
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
		
	case 1:	
		generateTestPromptPacket(SmPromptId::CONNECT_AIR_PROMPT, SmPromptId::ACCEPT_KEY_ONLY);
		break;
	case 2:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// User continues
			generateTestPromptPacket(SmPromptId::CONNECT_O2_PROMPT, SmPromptId::ACCEPT_KEY_ONLY);
		}
		else
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	case 3:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// User continues
			generateTestDataPacket(2, 1);
		}
		else
		{	
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	case 4:
		if (randomResult(2))
		{	// inspiration/exhalation flow delta > 10%
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_1);
		}
		else
		{	
			testTimer_.startTimer();
		}
		break;
	case 5:
		if (randomResult(2))
		{	// PSOL current is out of range (+/- 18% from nominal)
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_2);
		}
		else
		{	
			testTimer_.startTimer();
		}
		break;
	case 6:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		}
		break;
	case 7:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}
#endif	// SIGMA_UNIT_TEST
}


void
FakeServiceModeManager::sstFsCcTest(void)
{
	printf("sstFsCcTest(void), setTestSteps=%d, at %d\n",
				setTestSteps, __LINE__);
				
#if defined SIGMA_UNIT_TEST
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
	case 1:
		generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 2:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}

#elif defined SM_UNIT_TEST
	if (counter == 0)		// OVERALL STATUS = PASS 
	{
		switch (setTestSteps)
		{
		case START_TEST:
			generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
			break;

		case 1:
			printf("<FakeServiceModeManager> Press EXIT now.\n");
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;

		case 2:
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;

		case 3:
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;

		case 4:
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;

		case 5:
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;

		case 6:
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
			break;

		default:
			break;
		}
	}
	else if (counter==1)		// OVERALL STATUS = MINOR FAILURE / OVERRIDE
	{
		switch (setTestSteps)
		{
		case START_TEST:
			generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
			break;
		
		case 1:		//	DATA 
			UserRequestToStopTest_ = SmPromptId::NULL_ACTION_ID;
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				printf("Sending data.\n");	
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
	
		case 2:		//	DATA 
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				printf("Sending data.\n");	
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
	
		case 3:		//	DATA 
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				printf("Sending data.\n");	
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
	
		case 4:		//	DATA 
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				printf("Sending data.\n");	
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
	
		case 5:		//	DATA 
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				printf("Sending data.\n");	
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
	
		case 6:		//	DATA 
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				printf("Sending data.\n");	
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
	
		case 7:		//	DATA 
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				printf("Sending data.\n");	
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
	
		case 8:		//	DATA 
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				printf("Sending data.\n");	
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
	
		case 9:		//	DATA 
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				printf("Sending data.\n");	
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
	
		case 10:		//	DATA 
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				printf("Sending data.\n");	
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
	

		case 11:		// Connect circuit and block wye, ACCEPT_KEY_ONLY
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{
				generateTestPromptPacket(SmPromptId::ATTACH_CIRCUIT_PROMPT, SmPromptId::ACCEPT_KEY_ONLY);
			}
			else if (UserRequestToStopTest_ == SmPromptId::USER_EXIT)
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
	
		case 12:		// ALERT, CONDITION_1
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;

		case 13:		// TEST_END
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
			break;
	
		default:
			break;
		}
	}
	else if (counter==2)		// OVERALL STATUS = MINOR FAILURE / NON-OVERRIDE
	{
		switch (setTestSteps)
		{
		case START_TEST:
			generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
			break;

		case 1:		// ALERT, CONDITION_1
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
	
		case 2:		// TEST_END
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
			break;
	
		case 3:		// TEST_OPERATOR_EXIT
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			break;
	
		default:
			break;
		}
	}
	else if (counter==3)		// OVERALL STATUS = MAJOR FAILURE
	{
		switch (setTestSteps)
		{
		case START_TEST:
			generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
			break;

		case 1:		// FAILURE , CONDITION_2
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				generateTestStatusPacket(SmStatusId::TEST_FAILURE, SmStatusId::CONDITION_2);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
	
		case 2:		// TEST_END
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
			break;
	
		case 3:		// TEST_OPERATOR_EXIT
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			break;
	
		default:
			break;
		}
	}
	else if (counter==4)		// OVERALL STATUS = MAJOR FAILURE
	{
		switch (setTestSteps)
		{
		case START_TEST:
			generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
			break;

		case 1:		// ALERT, CONDITION_2
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_2);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
	
		case 2:		//	DATA 
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				printf("Sending data.\n");	
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
	
		case 3:		//	DATA 
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				printf("Sending data.\n");	
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
	
		case 4:		//	DATA 
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				printf("Sending data.\n");	
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
	
		case 5:		//	DATA 
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				printf("Sending data.\n");	
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
	
		case 6:		//	DATA 
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				printf("Sending data.\n");	
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
	
		case 7:		//	DATA 
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				printf("Sending data.\n");	
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
	
		case 8:		//	DATA 
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				printf("Sending data.\n");	
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
	
		case 9:		//	DATA 
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				printf("Sending data.\n");	
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
	
		case 10:		//	DATA 
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				printf("Sending data.\n");	
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
	
		case 11:		//	DATA 
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				printf("Sending data.\n");	
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
	
		case 12:		// TEST_END
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
			break;
	
		default:
			break;
		}
	}
	else
	{				
		printf ("Illegal FakeServiceModeManager's counter!!!\n");
	}

#else
	
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;

#if defined TEST_NOT_INSTALL
	case 1:	
		generateTestStatusPacket(SmStatusId::NOT_INSTALLED, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 2:	
		generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 3:
		// User exit
		generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		break;
#else		
	case 1:	
		generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 2:
		// User exit
		generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		break;
#endif	// TEST_NOT_INSTALL
	}
#endif	// SIGMA_UNIT_TEST
}


void
FakeServiceModeManager::lowFlowFsCc(void)
{
	printf("lowFlowFsCc(void), setTestSteps=%d, at %d\n",
				setTestSteps, __LINE__);

				
#if defined SIGMA_UNIT_TEST
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
	case 1:
		generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 2:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}

				
#elif defined SM_UNIT_TEST
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
	case 1:
		generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 2:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}


#else
	
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
		
	case 1:	
		generateTestPromptPacket(SmPromptId::CONNECT_AIR_PROMPT, SmPromptId::ACCEPT_KEY_ONLY);
		break;
	case 2:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// User continues
			generateTestPromptPacket(SmPromptId::CONNECT_O2_PROMPT, SmPromptId::ACCEPT_KEY_ONLY);
		}
		else
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	case 3:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// User continues
			generateTestDataPacket(4, 1);
		}
		else
		{	
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	case 4:
		if (randomResult(2))
		{	// unable to establish flow
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_1);
		}
		else
		{	
			generateTestDataPacket(2, 5);
		}
		break;
	case 5:
		if (randomResult(2))
		{	// inspiration/exhalation flow delta > 10%
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_2);
		}
		else
		{	
			testTimer_.startTimer();
		}
		break;
	case 6:
		if (randomResult(2))
		{	// PSOL current is out of range (+/- 18% from nominal)
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_3);
		}
		else
		{	
			testTimer_.startTimer();
		}
		break;
	case 7:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		}
		break;
	case 8:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}
#endif	// SIGMA_UNIT_TEST
}


void
FakeServiceModeManager::highFlowFsCc(void)
{
	printf("highFlowFsCc(void), setTestSteps=%d, at %d\n",
				setTestSteps, __LINE__);

				
#if defined SIGMA_UNIT_TEST
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
	case 1:
		generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 2:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}

				
#elif defined SM_UNIT_TEST
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
	case 1:
		generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 2:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}


#else
	
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
		
	case 1:	
		generateTestPromptPacket(SmPromptId::CONNECT_AIR_PROMPT, SmPromptId::ACCEPT_KEY_ONLY);
		break;
	case 2:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// User continues
			generateTestPromptPacket(SmPromptId::CONNECT_O2_PROMPT, SmPromptId::ACCEPT_KEY_ONLY);
		}
		else
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	case 3:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// User continues
			generateTestDataPacket(4, 1);
		}
		else
		{	
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	case 4:
		if (randomResult(2))
		{	// test point != 150 LPM and if unable to establish flow
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_1);
		}
		else
		{	
			generateTestDataPacket(2, 5);
		}
		break;
	case 5:
		if (randomResult(2))
		{	// inspiration/exhalation flow delta > 10%
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_2);
		}
		else
		{	
			testTimer_.startTimer();
		}
		break;
	case 6:
		if (randomResult(2))
		{	// PSOL current is out of range (+/- 18% from nominal)
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_3);
		}
		else
		{	
			testTimer_.startTimer();
		}
		break;
	case 7:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		}
		break;
	case 8:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}
#endif	// SIGMA_UNIT_TEST
}


void
FakeServiceModeManager::circuitPressure(void)
{
	printf("circuitPressure(void), setTestSteps=%d, at %d\n",
				setTestSteps, __LINE__);

				
#if defined SIGMA_UNIT_TEST
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
	case 1:	
		generateTestPromptPacket(SmPromptId::CONNECT_AIR_PROMPT, SmPromptId::ACCEPT_KEY_ONLY);
		break;
	case 2:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// User continues
//			TestStatusId_ = SmStatusId::TEST_ALERT;
//			TestConditionId_ = SmStatusId::CONDITION_2;
			generateTestStatusPacket(TestStatusId_, TestConditionId_);
		}
		else
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	case 3:
//		TestStatusId_ = SmStatusId::TEST_ALERT;
//		TestConditionId_ = SmStatusId::CONDITION_2;
		generateTestStatusPacket(TestStatusId_, TestConditionId_);
		break;
	case 4:
		generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 5:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}

#elif defined SM_UNIT_TEST
	if (counter == 0) // OVERALL STATUS = PASS
	{
		switch (setTestSteps)
		{
		case START_TEST:
			generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
			break;

		case 1:
			printf("<FakeServiceModeManager> Press EXIT now.\n");
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;

		case 2:
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;

		case 3:
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;

		case 4:
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;

		case 5:
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;

		case 6:
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;

		case 7:
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;

		case 8:
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;

		case 9:
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;

		case 10:
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;

		case 11:		// TEST_END
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
			break;
	
		case 12:		// TEST_OPERATOR_EXIT
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			break;
	
		default:
			break;
		}
	}
	else if (counter == 1 || counter==2) // OVERALL STATUS = PASS
	{
		switch (setTestSteps)
		{
		case START_TEST:
			generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
			break;

		case 1:
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;

		case 2:		// TEST_END
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
			break;
	
		case 3:		// TEST_OPERATOR_EXIT
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			break;
	
		default:
			break;
		}
	}
	else if (counter==3) 		// OVERALL STATUS = MAJOR FAILURE
	{
		switch (setTestSteps)
		{
		case START_TEST:
			generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
			break;

		case 1:		// FAILURE , CONDITION_2
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				generateTestStatusPacket(SmStatusId::TEST_FAILURE, SmStatusId::CONDITION_2);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
	
		case 2:		// TEST_END
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
			break;
	
		case 3:		// TEST_OPERATOR_EXIT
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			break;
	
		default:
			break;
		}
	}
	else if (counter==4) 		// OVERALL STATUS = MAJOR FAILURE
	{
		switch (setTestSteps)
		{
		case START_TEST:
			generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
			break;

		case 1:		// FAILURE , CONDITION_2
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				generateTestStatusPacket(SmStatusId::TEST_FAILURE, SmStatusId::CONDITION_2);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
	
		case 2:		//	DATA 
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				printf("Sending data.\n");	
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
	
		case 3:		//	DATA 
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				printf("Sending data.\n");	
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
	
		case 4:		//	DATA 
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				printf("Sending data.\n");	
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
	
		case 5:		//	DATA 
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				printf("Sending data.\n");	
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
	
		case 6:		//	DATA 
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				printf("Sending data.\n");	
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
	
		case 7:		//	DATA 
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				printf("Sending data.\n");	
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
	
		case 8:		//	DATA 
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				printf("Sending data.\n");	
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
	
		case 9:		//	DATA 
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				printf("Sending data.\n");	
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
	
		case 10:		//	DATA 
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				printf("Sending data.\n");	
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
	
		case 11:		//	DATA 
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				printf("Sending data.\n");	
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
	
		case 12:		// TEST_END
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
			break;
	
		default:
			break;
		}
	}
	else
	{				
		printf ("counter=%d Illegal FakeServiceModeManager's counter!!!\n", counter);
	}

#else
	
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
		
	case 1:	
		generateTestPromptPacket(SmPromptId::CONNECT_AIR_PROMPT, SmPromptId::ACCEPT_KEY_ONLY);
		break;
	case 2:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// User continues
			generateTestDataPacket(1, 1);
		}
		else
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	case 3:
		if (randomResult(2))
		{	// inspiratory pressure sensor is not 0 cmH2O
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_1);
		}
		else
		{	
			generateTestDataPacket(1, 2);
		}
		break;
	case 4:
		if (randomResult(2))
		{	// exhalation pressure sensor is not 0 cmH2O
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_2);
		}
		else
		{
			testTimer_.startTimer();
		}
		break;
	case 5:
		if (randomResult(2))
		{	// unable to pressurize
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_3);
		}
		else
		{
			generateTestDataPacket(2, 3);
		}
		break;
	case 6:
		if (randomResult(2))
		{	// inspiratory pressure sensor is not 10 cmH2O
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_4);
		}
		else
		{
			testTimer_.startTimer();
		}
		break;
	case 7:
		if (randomResult(2))
		{	// exhalation pressure is not 10 cmH2O
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_5);
		}
		else
		{
			generateTestDataPacket(1, 5);
		}
		break;
	case 8:
		if (randomResult(2))
		{	// inspiratory pressure is not zero
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_6);
		}
		else
		{
			generateTestDataPacket(1, 6);
		}
		break;
	case 9:
		if (randomResult(2))
		{	// exhalation pressure is not zero
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_7);
		}
		else
		{
			generateTestDataPacket(6, 7);
		}
		break;
	case 10:
		if (randomResult(2))
		{	// average inspiratory pressure sensor is out of tolerance
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_8);
		}
		else
		{
			testTimer_.startTimer();
		}
		break;
	case 11:
		if (randomResult(2))
		{	// average exhalation pressure sensor is out of tolerance
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_9);
		}
		else
		{
			testTimer_.startTimer();
		}
		break;
	case 12:
		if (randomResult(2))
		{	// autozeroed inspiratory pressure differs from the autozeroed exhalation pressure
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_9);
		}
		else
		{	
			testTimer_.startTimer();
		}
		break;
	case 13:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		}
		break;
	case 14:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}
#endif	// SIGMA_UNIT_TEST
}


void
FakeServiceModeManager::safetySystem(void)
{
	printf("safetySystem(void), setTestSteps=%d, at %d\n",
				setTestSteps, __LINE__);

				
#if defined SIGMA_UNIT_TEST
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
	case 1:
		generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 2:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}

				
#elif defined SM_UNIT_TEST
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
	case 1:
		generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 2:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}


#else
	
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
		
	case 1:	
		generateTestPromptPacket(SmPromptId::CONNECT_AIR_PROMPT, SmPromptId::ACCEPT_KEY_ONLY);
		break;
	case 2:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// User continues
			generateTestDataPacket(2, 1);
		}
		else
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	case 3:
		if (randomResult(2))
		{	// inspiratory pressure > 1.4 cmH2O
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_1);
		}
		else
		{	
			testTimer_.startTimer();
		}
		break;
	case 4:
		if (randomResult(2))
		{	// Safety Valve current > 0.022 V
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_2);
		}
		else
		{	
			generateTestDataPacket(1, 3);
		}
		break;
	case 5:
		if (randomResult(2))
		{	// Safety Valve current < 6.000V
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_3);
		}
		else
		{	
			generateTestDataPacket(1, 4);
		}
		break;
	case 6:
		if (randomResult(2))
		{	// time is not 0.3 to 1.0 seconds
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_4);
		}
		else
		{	
			generateTestDataPacket(1, 5);
		}
		break;
	case 7:
		if (randomResult(2))
		{	// Safety Valve current > 3.000 V
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_5);
		}
		else
		{	
			testTimer_.startTimer();
		}
		break;
	case 8:
		if (randomResult(2))
		{	// Safety Valve current < 2.000V
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_6);
		}
		else
		{	
			generateTestDataPacket(2, 6);
		}
		break;
	case 9:
		if (randomResult(2))
		{	// inspiratory pressure < 110 cmH2O
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_7);
		}
		else
		{	
			testTimer_.startTimer();
		}
		break;
	case 10:
		if (randomResult(2))
		{	// exhalation flow > 1 LPM
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_8);
		}
		else
		{	
			testTimer_.startTimer();
		}
		break;
	case 11:
		if (randomResult(2))
		{	// max inspiratory pressure > 127.5 cmH2O
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_9);
		}
		else
		{	
			testTimer_.startTimer();
		}
		break;
	case 12:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		}
		break;
	case 13:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}
#endif	// SIGMA_UNIT_TEST
}

void
FakeServiceModeManager::exhValveSeal(void)
{
	printf("exhValveSeal(void), setTestSteps=%d, at %d\n",
				setTestSteps, __LINE__);

				
#if defined SIGMA_UNIT_TEST
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
	case 1:
		generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 2:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}
				
#elif defined SM_UNIT_TEST
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
	case 1:
		generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 2:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}


#else
	
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
		
	case 1:	
		generateTestDataPacket(1, 1);
		break;
	case 2:
		if (randomResult(2))
		{	// exhalation pressure is not 8 to 10 cmH2O
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_1);
		}
		else
		{	
			testTimer_.startTimer();
		}
		break;
	case 3:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		}
		break;
	case 4:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}
#endif	// SIGMA_UNIT_TEST
}


void
FakeServiceModeManager::exhValve(void)
{
	printf("exhValve(void), setTestSteps=%d, at %d\n",
				setTestSteps, __LINE__);

				
#if defined SIGMA_UNIT_TEST
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
	case 1:
		generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 2:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}

				
#elif defined SM_UNIT_TEST
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
	case 1:
		generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 2:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}


#else
	
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
		
	case 1:	
		generateTestPromptPacket(SmPromptId::CONNECT_AIR_PROMPT, SmPromptId::ACCEPT_KEY_ONLY);
		break;
	case 2:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// User continues
			generateTestDataPacket(20, 1);
		}
		else
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	case 3:
		if (randomResult(2))
		{	// the exhalation pressure / test point delta >= 2 cmH2O
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_1);
		}
		else
		{	
			testTimer_.startTimer();
		}
		break;
	case 4:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		}
		break;
	case 5:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}
#endif	// SIGMA_UNIT_TEST
}


void
FakeServiceModeManager::exhValveCalib(void)
{
	printf("exhValveCalib(void), setTestSteps=%d, at %d\n",
				setTestSteps, __LINE__);

				
#if defined SIGMA_UNIT_TEST
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
	case 1:
		generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_1);
		break;
	case 2:
		generateTestStatusPacket(SmStatusId::TEST_FAILURE, SmStatusId::CONDITION_2);
		break;
	case 3:
		generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 4:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}
				
#elif defined SM_UNIT_TEST
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
	case 1:	
		generateTestPromptPacket(SmPromptId::REMOVE_INSP_FILTER_PROMPT, SmPromptId::ACCEPT_KEY_ONLY);
		break;
	case 2:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// User continues
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_1);
		}
		else
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	case 3:	
		generateTestStatusPacket(SmStatusId::TEST_FAILURE, SmStatusId::CONDITION_2);
		break;
	case 4:	
		generateTestPromptPacket(SmPromptId::CONNECT_AIR_PROMPT, SmPromptId::ACCEPT_AND_CANCEL_ONLY);
		break;
	case 5:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// User continues
			if (randomResult(2))
			{	// inspiratory/exhalation pressure sensors differ > 3.54%
				generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_2);
			}
			else
			{	
				testTimer_.startTimer();
			}
		}
		else
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	case 6:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		}
		break;
	}


#else
	
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
		
	case 1:	
		generateTestPromptPacket(SmPromptId::REMOVE_INSP_FILTER_PROMPT, SmPromptId::ACCEPT_KEY_ONLY);
		break;
	case 2:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// User continues
			generateTestPromptPacket(SmPromptId::CONNECT_AIR_PROMPT, SmPromptId::ACCEPT_AND_CANCEL_ONLY);
		}
		else
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	case 3:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// User continues
			if (randomResult(2))
			{	// inspiratory/exhalation pressure sensors differ > 3.54%
				generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_1);
			}
			else
			{	
				testTimer_.startTimer();
			}
		}
		else
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	case 4:
		if (randomResult(2))
		{	// inspiratory/exhalation pressure sensors differ > 3.54%
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_2);
		}
		else
		{	
			testTimer_.startTimer();
		}
		break;
	case 5:
		if (randomResult(2))
		{	// exhalation pressure is not 8-10 cm H2O
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_3);
		}
		else
		{	
			testTimer_.startTimer();
		}
		break;
	case 6:
		generateTestDataPacket(23, 1);
		break;
	case 7:
		if (randomResult(2))
		{	// exhalation valve current is not 12.15 +1 1.05 mA/cm H2O
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_4);
		}
		else
		{	
			testTimer_.startTimer();
		}
		break;
	case 8:
		if (randomResult(2))
		{	// exhalation valve offset is not 6-26 mA @ 0.5 cm H2O
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_5);
		}
		else
		{	
			testTimer_.startTimer();
		}
		break;
	case 9:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		}
		break;
	case 10:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}
#endif	// SIGMA_UNIT_TEST
}


void
FakeServiceModeManager::smLeak(void)
{
	printf("smLeak(void), setTestSteps=%d, at %d\n",
				setTestSteps, __LINE__);

				
#if defined SIGMA_UNIT_TEST
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
	case 1:
		generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 2:
		generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		break;
	}
#elif defined SM_UNIT_TEST
	if (counter == 0) // EXIT_BEFORE_PROMPT_STATE
	{
		switch (setTestSteps)
		{
		case START_TEST:
			generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
			printf("<FakeServiceModeManager> Press EXIT now (Do not ACCEPT this command.\n");
			break;

		case 1:	
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;

		case 2:
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;

		case 3:
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;

		case 4:
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;

		case 5:
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;

		case 6:
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;

		case 7:
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;

		case 8:
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;

		case 9:
			generateTestPromptPacket(SmPromptId::CONNECT_AIR_PROMPT, SmPromptId::ACCEPT_KEY_ONLY);
			break;

		case 10:
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
			break;

		case 11:
			// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			break;

		default:
			break;
		}
	}
	else if (counter==1)		// OVERALL STATUS = MINOR FAILURE
	{
		switch (setTestSteps)
		{
		case START_TEST:
			generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
			break;
		case 1:
			generateSmLeakDataPacket(1, 2);
			break;

		case 2:		// ALERT, CONDITION_1
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
	
		case 3:		// TEST_END
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
			break;
	
		case 4:		// TEST_OPERATOR_EXIT
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			break;
	
		default:
			break;
		}
	}
	else 
	{
		switch (setTestSteps)
		{
		case START_TEST:
			generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
			break;
		case 1:
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
			break;
		case 2:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			break;
		}
		default:
			break;
		}
	}


#else
	
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
		
	case 1:	
		generateTestPromptPacket(SmPromptId::CONNECT_AIR_PROMPT, SmPromptId::ACCEPT_KEY_ONLY);
		break;
	case 2:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// User continues
			generateTestDataPacket(2, 1);
		}
		else
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	case 3:
		if (randomResult(2))
		{	// inspiratory pressure delta > ??? cmH2O
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_1);
		}
		else
		{	
			generateTestDataPacket(1, 3);
		}
		break;
	case 4:
		if (randomResult(2))
		{	// inspiratory pressure delta > ??? LPM
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_2);
		}
		else
		{	
			testTimer_.startTimer();
		}
		break;
	case 5:
		if (randomResult(2))
		{	// inspiratory pressure delta < ??? LPM
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_3);
		}
		else
		{	
			testTimer_.startTimer();
		}
		break;
	case 6:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		}
		break;
	case 7:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}
#endif	// SIGMA_UNIT_TEST
}


void
FakeServiceModeManager::sstLeak(void)
{
	printf("sstLeak(void), setTestSteps=%d, at %d\n",
				setTestSteps, __LINE__);

				
#if defined SIGMA_UNIT_TEST
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
	case 1:
		generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_1);
		break;
	case 2:
		generateTestStatusPacket(SmStatusId::TEST_FAILURE, SmStatusId::CONDITION_2);
		break;
	case 3:
		generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 4:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}
				
#elif defined SM_UNIT_TEST
	if (counter == 0)
	{
		switch (setTestSteps)
		{
		case START_TEST:
			generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
			printf("<FakeServiceModeManager> Press EXIT now (Do not ACCEPT this command.\n");
			break;

		case 1:	
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				generateTestDataPacket(1, SmDataId::SST_LEAK_INSPIRATORY_PRESSURE1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;

		case 2:
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				generateTestDataPacket(1, SmDataId::SST_LEAK_MONITOR_PRESSURE);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;

		case 3:
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				generateTestDataPacket(1, SmDataId::SST_LEAK_INSPIRATORY_PRESSURE2);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;

		case 4:
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				generateTestDataPacket(1, SmDataId::SST_LEAK_DELTA_PRESSURE);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;

		case 5:
			generateTestPromptPacket(SmPromptId::CONNECT_AIR_PROMPT, SmPromptId::ACCEPT_KEY_ONLY);
			break;

		case 6:
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
			break;

		}
	}
	else if (counter==1)
	{
		switch (setTestSteps)
		{
		case START_TEST:
			generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
			break;

		case 1:
			generateSmLeakDataPacket(1, 2);
			break;

		case 2:
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;

		case 3:
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_2);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;

		case 4:
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
			break;

		}
	}
	else if (counter==4)
	{
		switch (setTestSteps)
		{
		case START_TEST:
			generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
			break;

		case 1:
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				generateTestStatusPacket(SmStatusId::TEST_FAILURE, SmStatusId::CONDITION_1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;

		case 2:		//	DATA 
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				printf("Sending data.\n");	
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
	
		case 3:		//	DATA 
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				printf("Sending data.\n");	
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
	
		case 4:		//	DATA 
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				printf("Sending data.\n");	
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
	
		case 5:		//	DATA 
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				printf("Sending data.\n");	
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
	
		case 6:		//	DATA 
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				printf("Sending data.\n");	
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
	
		case 7:		//	DATA 
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				printf("Sending data.\n");	
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
	
		case 8:		//	DATA 
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				printf("Sending data.\n");	
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
	
		case 9:		//	DATA 
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				printf("Sending data.\n");	
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
	
		case 10:		//	DATA 
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				printf("Sending data.\n");	
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
	
		case 11:		//	DATA 
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{	// User continues
				printf("Sending data.\n");	
				generateTestDataPacket(1, 1);
			}
			else
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
	
		case 12:
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
			break;

		}
	}
	else
	{
		switch (setTestSteps)
		{
		case START_TEST:
			generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
			break;
		case 1:
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
			break;
		case 2:
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
		}
	}

#else
	
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
		
	case 1:	
		generateTestPromptPacket(SmPromptId::CONNECT_AIR_PROMPT, SmPromptId::ACCEPT_KEY_ONLY);
		break;
	case 2:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// User continues
			generateTestDataPacket(4, 1);
		}
		else
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	case 3:
		if (randomResult(2))
		{	// inspiratory pressure delta > ??? cmH2O
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_1);
		}
		else
		{	
			testTimer_.startTimer();
		}
		break;
	case 4:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// User continues
			generateTestDataPacket(4, 1);
		}
		else
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	case 5:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// User continues
			generateTestDataPacket(4, 1);
		}
		else
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	case 6:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// User continues
			generateTestDataPacket(1, 2);
		}
		else
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	case 7:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		}
		break;
	case 8:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}
#endif	// SIGMA_UNIT_TEST
}


void
FakeServiceModeManager::complianceCalib(void)
{
	printf("complianceCalib(void), setTestSteps=%d, at %d\n",
				setTestSteps, __LINE__);

				
#if defined SIGMA_UNIT_TEST
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
	case 1:
		generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 2:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}
				
#elif defined SM_UNIT_TEST
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;

	case 1:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// User continues
			generateTestDataPacket(1, 1);
		}
		else
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;

	case 2:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// User continues
			generateTestDataPacket(1, SmDataId::COMPLIANCE_CALIB_NOMINAL_COMPLIANCE);
		}
		else
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;

	case 3:
		generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;

	case 4:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}


#else
	
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
		
	case 1:	
		if (randomResult(2))
		{	// FiO2 sensor voltage delta < <TBD> volts
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_1);
		}
		else
		{	
			testTimer_.startTimer();
		}
	case 2:
		generateTestDataPacket(43, 1);
		break;
	case 3:
		generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 4:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}
#endif	// SIGMA_UNIT_TEST
}


void
FakeServiceModeManager::exhHeater(void)
{
	printf("exhHeater(void), setTestSteps=%d, at %d\n",
				setTestSteps, __LINE__);
				
#if defined SIGMA_UNIT_TEST
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
	case 1:	
		generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_1);
		break;
	case 2:	
		generateTestStatusPacket(SmStatusId::TEST_FAILURE, SmStatusId::CONDITION_2);
		break;
	case 3:
		generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 4:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}
				
#elif defined SM_UNIT_TEST
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
	case 1:
		generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 2:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}


#else
	
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
		
	case 1:	
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 2:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}
#endif	// SIGMA_UNIT_TEST
}

void
FakeServiceModeManager::generalElectronics(void)
{
	printf("generalElectronics(void), setTestSteps=%d, at %d\n",
				setTestSteps, __LINE__);
				
#if defined SIGMA_UNIT_TEST
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
		
	case 1:
		generateTestPromptPacket(SmPromptId::HIGH_ALARM_PROMPT, SmPromptId::ACCEPT_PASS_AND_CANCEL_FAIL_ONLY);
		break;
	case 2:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// User continues
			generateTestDataPacket(2, 0);
		}
		else if (UserRequestToStopTest_ == SmPromptId::KEY_CLEAR)
		{	// Test failed
			generateFailedTestStatusPacket(TestStatusId_, SmStatusId::NULL_CONDITION_ITEM);
			TestStatusId_ = SmStatusId::TEST_ALERT;
		}
		else
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	case 3:
		if (UserRequestToStopTest_ != SmPromptId::KEY_CLEAR)
		{	// User continues
			generateTestPromptPacket(SmPromptId::MEDIUM_ALARM_PROMPT, SmPromptId::ACCEPT_PASS_AND_CANCEL_FAIL_ONLY);
		}
		else
		{
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		}
		break;
	case 4:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// User continues
			generateTestDataPacket(2, 2);
		}
		else if (UserRequestToStopTest_ == SmPromptId::KEY_CLEAR)
		{	// Test failed
			generateFailedTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_2);
		}
		else
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	case 5:
		if (UserRequestToStopTest_ != SmPromptId::KEY_CLEAR)
		{	// User continues
			generateTestPromptPacket(SmPromptId::LOW_ALARM_PROMPT, SmPromptId::ACCEPT_PASS_AND_CANCEL_FAIL_ONLY);
		}
		else
		{
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		}
		break;
	case 6:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// User continues
			generateTestDataPacket(2, 4);
		}
		else if (UserRequestToStopTest_ == SmPromptId::KEY_CLEAR)
		{	// Test failed
			generateFailedTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_3);
		}
		else
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	case 7:
		if (UserRequestToStopTest_ != SmPromptId::KEY_CLEAR)
		{	// User continues
			generateTestPromptPacket(SmPromptId::ON_BATTERY_POWER_PROMPT, SmPromptId::ACCEPT_PASS_AND_CANCEL_FAIL_ONLY);
		}
		else
		{
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		}
		break;
	case 8:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// User continues
			generateTestDataPacket(2, 6);
		}
		else if (UserRequestToStopTest_ == SmPromptId::KEY_CLEAR)
		{	// Test failed
			generateFailedTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_4);
		}
		else
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	case 9:
		if (UserRequestToStopTest_ != SmPromptId::KEY_CLEAR)
		{	// User continues
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_1);
		}
		else
		{
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		}
		break;
	case 10:
		generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_2);
		break;
	case 11:
		generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_3);
		break;
	case 12:
		generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_4);
		break;
	case 13:
		generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_5);
		break;
	case 14:
		generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_6);
		break;
	case 15:
		generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_7);
		break;
	case 16:
		generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_8);
		break;
	case 17:
		generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_9);
		break;
	case 18:
		generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_10);
		break;
	case 19:
		generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_11);
		break;
	case 20:
		generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_12);
		break;
	case 21:
		generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_13);
		break;
	case 22:
		generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_14);
		break;
	case 23:
		generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_15);
		break;
	case 24:
		generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_16);
		break;
	case 25:
		generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_17);
		break;
	case 26:
		generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_18);
		break;
	case 27:
		generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_19);
		break;
	case 28:
		generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_20);
		break;
	case 29:
		generateTestDataPacket(12, 8);
		break;
	case 30:
		generateTestDataPacket(10, 20);
		break;
	case 31:
		generateTestDataPacket(10, 30);
		break;
	case 32:
		generateTestDataPacket(6, 40);
		break;
	case 33:
		generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 34:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}
				
#elif defined SM_UNIT_TEST
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
	case 1:
		generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 2:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}


#else
	
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
		
	case 1:	
		generateTestDataPacket(31, 1);
		break;
	case 2:
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 3:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}
#endif	// SIGMA_UNIT_TEST
}


void
FakeServiceModeManager::guiTouch(void)
{
	printf("guiTouch(void), setTestSteps=%d, at %d\n",
				setTestSteps, __LINE__);
				
#if defined SIGMA_UNIT_TEST
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
	case 1:
		generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 2:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}
				
#elif defined SM_UNIT_TEST
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
	case 1:
		generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 2:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}


#else
	
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
		
	case 1:	
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 2:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}
#endif	// SIGMA_UNIT_TEST
}


void
FakeServiceModeManager::guiSerialPort(void)
{
	printf("guiSerialPort(void), setTestSteps=%d, at %d\n",
				setTestSteps, __LINE__);
				
#if defined SIGMA_UNIT_TEST
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
	case 1:
		generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 2:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}
				
#elif defined SM_UNIT_TEST
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
	case 1:
		generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 2:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}


#else
	
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
		
	case 1:	
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 2:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}
#endif	// SIGMA_UNIT_TEST
}


void
FakeServiceModeManager::battery(void)
{
	printf("battery(void), setTestSteps=%d, at %d\n",
				setTestSteps, __LINE__);
				
#if defined SIGMA_UNIT_TEST
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
	case 1:
		generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 2:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}
				
#elif defined SM_UNIT_TEST
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
	case 1:
		generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 2:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}


#else
	
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
		
	case 1:	
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 2:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}
#endif	// SIGMA_UNIT_TEST
}


void
FakeServiceModeManager::restartBd(void)
{
	printf("restartBd(void), setTestSteps=%d, at %d\n",
				setTestSteps, __LINE__);
				
#if defined SIGMA_UNIT_TEST
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
	case 1:
		generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 2:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}
				
#elif defined SM_UNIT_TEST
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
	case 1:
		generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 2:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}


#else
	
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
		
	case 1:	
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 2:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}
#endif	// SIGMA_UNIT_TEST
}


void
FakeServiceModeManager::restartGui(void)
{
	printf("restartGui(void), setTestSteps=%d, at %d\n",
				setTestSteps, __LINE__);
				
#if defined SIGMA_UNIT_TEST
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
	case 1:
		generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 2:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}
				
#elif defined SM_UNIT_TEST
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
	case 1:
		generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 2:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}


#else
	
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
		
	case 1:	
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 2:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}
#endif	// SIGMA_UNIT_TEST
}


void
FakeServiceModeManager::bdSerialNumber(void)
{
	printf("bdSerialNumber(void), setTestSteps=%d, at %d\n",
				setTestSteps, __LINE__);
				
#if defined SIGMA_UNIT_TEST
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
	case 1:
		generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 2:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}

#elif defined SM_UNIT_TEST				
	if (counter == 0)
	{
		switch (setTestSteps)
		{
		case START_TEST:
			generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
			break;
		case 1:	
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_1);
			break;
		case 2:
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_2);
			break;
		case 3:
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
			break;
		}
	}
	else if (counter == 1)
	{
		switch (setTestSteps)
		{
		case START_TEST:
			generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
			break;
		case 1:
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
			break;
		}
	}

#else
	
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
		
	case 1:	
		if (randomResult(2))
		{	// compressors elapsed timer has not changed
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_1);
		}
		else
		{	
			testTimer_.startTimer();
		}
		break;
	case 2:
		if (randomResult(2))
		{	// compressors elapsed timer has not changed
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_2);
		}
		else
		{	
			testTimer_.startTimer();
		}
		break;
	case 3:
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 4:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}
#endif	// SIGMA_UNIT_TEST
}


void
FakeServiceModeManager::guiSerialNumber(void)
{
	printf("guiSerialNumber(void), setTestSteps=%d, at %d\n",
				setTestSteps, __LINE__);
				
#if defined SIGMA_UNIT_TEST
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
	case 1:
		generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 2:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}

#elif defined SM_UNIT_TEST
	if (counter == 0)
	{
		switch (setTestSteps)
		{
		case START_TEST:
			generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
			break;
		
		case 1:
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_1);
			break;

		case 2:
			generateTestStatusPacket(SmStatusId::TEST_FAILURE, SmStatusId::CONDITION_2);
			break;

		case 3:
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
			break;
		}
	}
	else if (counter == 1)
	{
		switch (setTestSteps)
		{
		case START_TEST:
			generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
			break;
		case 1:
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
			break;
		}
	}


#else
	
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
		
	case 1:	
		if (randomResult(2))
		{	// compressors elapsed timer has not changed
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_1);
		}
		else
		{	
			testTimer_.startTimer();
		}
		break;
	case 2:
		if (randomResult(2))
		{	// compressors elapsed timer has not changed
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_2);
		}
		else
		{	
			testTimer_.startTimer();
		}
		break;
	case 3:
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 4:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}
#endif	// SIGMA_UNIT_TEST
}


void
FakeServiceModeManager::calInfoDuplication(void)
{
	printf("calInfoDuplication(void), setTestSteps=%d, at %d\n",
				setTestSteps, __LINE__);
				
#if defined SIGMA_UNIT_TEST
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
	case 1:
		generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 2:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}
#elif defined SM_UNIT_TEST

	if (counter == 0)		// OVERALL STATUS = PASS
	{	
		switch (setTestSteps)
		{
		case START_TEST:
			generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
			break;
		
		case 1:	
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{
				generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
			}
			else
			{	
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;

		case 2:
			{	// User exit
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
		}
	}
	else if (counter == 1)
	{
		switch (setTestSteps)
		{
		case START_TEST:
			generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
			break;
		
		case 1:
			if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
			{
				generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
			}
			else
			{	
				generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
		}
	}

#else
	
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
		
	case 1:	
		if (randomResult(2))
		{	// error burning flash
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_1);
		}
		else
		{	
			testTimer_.startTimer();
		}
		break;
	case 2:
		if (randomResult(2))
		{	// did not receive flash data
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_2);
		}
		else
		{	
			testTimer_.startTimer();
		}
		break;
	case 3:
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 4:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}
#endif	// SIGMA_UNIT_TEST
}


void
FakeServiceModeManager::BdventInop(void)
{
	printf("BdventInop(void), setTestSteps=%d, at %d\n",
				setTestSteps, __LINE__);
				
#if defined SIGMA_UNIT_TEST
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
	case 1:
		generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 2:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}
#elif defined SM_UNIT_TEST
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
		
	case 1:	
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// User continues
			generateTestPromptPacket(SmPromptId::POWER_DOWN_PROMPT, SmPromptId::NO_KEYS);
		}
		else
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;

	case 2:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// User continues
			generateTestPromptPacket(SmPromptId::SOUND_PROMPT, SmPromptId::ACCEPT_AND_CANCEL_ONLY);
		}
		else
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;

	case 3:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// User continues
			generateTestPromptPacket(SmPromptId::VENTILATOR_INOPERATIVE_PROMPT, SmPromptId::ACCEPT_AND_CANCEL_ONLY);
		}
		else
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;

	case 4:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// User continues
			generateTestPromptPacket(SmPromptId::SAFETY_VALVE_OPEN_PROMPT, SmPromptId::ACCEPT_AND_CANCEL_ONLY);
		}
		else
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;

	case 5:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// User continues
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_1);
		}
		else
		{
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;

	case 6:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// did not receive flash data
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_2);
		}
		else
		{	
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;

	case 7:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// did not receive flash data
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		}
		else
		{	
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;

	case 8:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}

#else
	
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
		
	case 1:	
		generateTestPromptPacket(SmPromptId::POWER_DOWN_PROMPT, SmPromptId::NO_KEYS);
		break;
	case 2:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// User continues
			generateTestPromptPacket(SmPromptId::GUI_SIDE_TEST_PROMPT, SmPromptId::NO_KEYS);
		}
		else
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	case 3:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// User continues
			generateTestPromptPacket(SmPromptId::VENT_INOP_A_TEST_PROMPT, SmPromptId::NO_KEYS);
		}
		else
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	case 4:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// User continues
			generateTestPromptPacket(SmPromptId::VENT_INOP_B_TEST_PROMPT, SmPromptId::NO_KEYS);
		}
		else
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;

	case 5:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// User continues
			generateTestPromptPacket(SmPromptId::TEN_SEC_A_TEST_PROMPT, SmPromptId::NO_KEYS);
		}
		else
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;

	case 6:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// User continues
			generateTestPromptPacket(SmPromptId::TEN_SEC_B_TEST_PROMPT, SmPromptId::NO_KEYS);
		}
		else
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;

	case 7:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// User continues
			generateTestPromptPacket(SmPromptId::SOUND_PROMPT, SmPromptId::ACCEPT_AND_CANCEL_ONLY);
		}
		else
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;

	case 8:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// User continues
			generateTestPromptPacket(SmPromptId::VENTILATOR_INOPERATIVE_PROMPT, SmPromptId::ACCEPT_AND_CANCEL_ONLY);
		}
		else
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;

	case 9:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// User continues
			generateTestPromptPacket(SmPromptId::SAFETY_VALVE_OPEN_PROMPT, SmPromptId::ACCEPT_AND_CANCEL_ONLY);
		}
		else
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;

	case 10:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// User continues
			generateTestPromptPacket(SmPromptId::VENTILATOR_INOPERATIVE_PROMPT, SmPromptId::ACCEPT_AND_CANCEL_ONLY);
		}
		else
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	case 11:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// User continues
			generateTestPromptPacket(SmPromptId::SAFETY_VALVE_OPEN_PROMPT, SmPromptId::ACCEPT_AND_CANCEL_ONLY);
		}
		else
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	case 12:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// User continues
			if (randomResult(2))
			{	// error burning flash
				generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_1);
			}
			else
			{
				testTimer_.startTimer();
			}
		}
		else
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	case 13:
		if (randomResult(2))
		{	// did not receive flash data
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_2);
		}
		else
		{	
			testTimer_.startTimer();
		}
		break;
	case 14:
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 15:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}
#endif	// SIGMA_UNIT_TEST
}


void
FakeServiceModeManager::GuiventInop(void)
{
	printf("GuiventInop(void), setTestSteps=%d, at %d\n",
				setTestSteps, __LINE__);
				
#if defined SIGMA_UNIT_TEST
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
	case 1:
		generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 2:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}
#elif SM_UNIT_TEST
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
		
	case 1:	
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// did not receive flash data
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		}
		else
		{	
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
		
	case 2:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}

#else
	
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
		
	case 1:	
		generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
		
	case 2:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}
#endif	// SIGMA_UNIT_TEST
}


void
FakeServiceModeManager::bdInitDefaultSerialNumber(void)
{
	printf("bdInitDefaultSerialNumber(void), setTestSteps=%d, at %d\n",
				setTestSteps, __LINE__);
				
#if defined SIGMA_UNIT_TEST
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
	case 1:
		generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 2:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}
				
#elif defined SM_UNIT_TEST
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
	case 1:
		generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 2:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}


#else
	
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
		
	case 1:	
		if (randomResult(2))
		{	// compressors elapsed timer has not changed
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_1);
		}
		else
		{	
			testTimer_.startTimer();
		}
		break;
	case 2:
		if (randomResult(2))
		{	// compressors elapsed timer has not changed
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_2);
		}
		else
		{	
			testTimer_.startTimer();
		}
		break;
	case 3:
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 4:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}
#endif	// SIGMA_UNIT_TEST
}


void
FakeServiceModeManager::guiInitDefaultSerialNumber(void)
{
	printf("guiInitDefaultSerialNumber(void), setTestSteps=%d, at %d\n",
				setTestSteps, __LINE__);
				
#if defined SIGMA_UNIT_TEST
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
	case 1:
		generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 2:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}
				
#elif defined SM_UNIT_TEST
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
	case 1:
		generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 2:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}


#else
	
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
		
	case 1:	
		if (randomResult(2))
		{	// compressors elapsed timer has not changed
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_1);
		}
		else
		{	
			testTimer_.startTimer();
		}
		break;
	case 2:
		if (randomResult(2))
		{	// compressors elapsed timer has not changed
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_2);
		}
		else
		{	
			testTimer_.startTimer();
		}
		break;
	case 3:
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 4:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}
#endif	// SIGMA_UNIT_TEST
}


void
FakeServiceModeManager::fsCalibration(void)
{
	printf("fsCalibration(void), setTestSteps=%d, at %d\n",
				setTestSteps, __LINE__);
				
#if defined SIGMA_UNIT_TEST
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
	case 1:
		generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 2:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}
				
#elif defined SM_UNIT_TEST
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
	case 1:	
		generateTestPromptPacket(SmPromptId::REMOVE_INSP_FILTER_PROMPT, SmPromptId::ACCEPT_KEY_ONLY);
		break;
	case 2:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// User continues
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_1);
		}
		else
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	case 3:	
		generateTestStatusPacket(SmStatusId::TEST_FAILURE, SmStatusId::CONDITION_2);
		break;
	case 4:	
		generateTestPromptPacket(SmPromptId::CONNECT_AIR_PROMPT, SmPromptId::ACCEPT_KEY_ONLY);
		break;
	case 5:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{	// User continues
			if (randomResult(2))
			{	// inspiratory/exhalation pressure sensors differ > 3.54%
				generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_2);
			}
			else
			{	
				testTimer_.startTimer();
			}
		}
		else
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	case 6:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		}
		break;
	}

#else
	
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
		
	case 1:
		generateTestPromptPacket(SmPromptId::ATTACH_CIRCUIT_PROMPT, SmPromptId::ACCEPT_KEY_ONLY);
		break;
	case 2:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{
			generateTestPromptPacket(SmPromptId::CONNECT_O2_PROMPT, SmPromptId::ACCEPT_AND_CANCEL_ONLY);
		}
		else if (UserRequestToStopTest_ == SmPromptId::USER_EXIT)
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	case 3:
		if (UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
		{
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_1);
		}
		else if (UserRequestToStopTest_ == SmPromptId::USER_EXIT)
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	case 4:
		if (randomResult(2))
		{	// compressors elapsed timer has not changed
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_2);
		}
		else
		{	
			testTimer_.startTimer();
		}
		break;
	case 5:
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 6:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}
#endif	// SIGMA_UNIT_TEST
}


void
FakeServiceModeManager::guiNurseCall(void)
{
	printf("guiNurseCall(void), setTestSteps=%d, at %d\n",
				setTestSteps, __LINE__);
				
#if defined SIGMA_UNIT_TEST
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
	case 1:
		generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 2:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}
				
#elif defined SM_UNIT_TEST
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
	case 1:
		generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 2:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}


#else
	
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
		
	case 1:	
		if (randomResult(2))
		{	// compressors elapsed timer has not changed
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_1);
		}
		else
		{	
			testTimer_.startTimer();
		}
		break;
	case 2:
		if (randomResult(2))
		{	// compressors elapsed timer has not changed
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_2);
		}
		else
		{	
			testTimer_.startTimer();
		}
		break;
	case 3:
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 4:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}
#endif	// SIGMA_UNIT_TEST
}


void
FakeServiceModeManager::atmPressureTransducerCal(void)
{
	printf("atmPressureTransducerCal(void), setTestSteps=%d, at %d\n",
				setTestSteps, __LINE__);
				
#if defined SIGMA_UNIT_TEST
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
	case 1:
		generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 2:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}
				
#elif defined SM_UNIT_TEST
	if (counter == 1 )		// OVERALL STATUS = PASS
	{
		switch (setTestSteps)
		{
		case START_TEST:
			generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
			break;
			
		case 1:
			generateTestDataPacket(1, 1, 550, TRUE);
			break;
		
		case 2:
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
			break;
		case 3:
			{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
		}
	}
	else
	{
		switch (setTestSteps)
		{
		case START_TEST:
			generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
			break;
			
		case 1:
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_1);
			break;
		
		case 2:
			generateTestDataPacket(1, 1, 550, TRUE);
			break;
		
		case 3:
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
			break;
		case 4:
			{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
			}
			break;
		}
	}
#else
	
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
		
	case 1:	
		if (randomResult(2))
		{	// compressors elapsed timer has not changed
			generateTestStatusPacket(SmStatusId::TEST_ALERT, SmStatusId::CONDITION_1);
		}
		else
		{	
			testTimer_.startTimer();
		}
		break;
	case 2:
			generateTestDataPacket(1, 1);
		break;
		
	case 3:
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 4:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}
#endif	// SIGMA_UNIT_TEST
}


void
FakeServiceModeManager::atmPressureTransducerRead(void)
{
	printf("atmPressureTransducerRead(void), setTestSteps=%d, at %d\n",
				setTestSteps, __LINE__);
				
#if defined SIGMA_UNIT_TEST
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
	case 1:
		generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 2:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}
				
#elif defined SM_UNIT_TEST
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
		
	case 1:
			generateTestDataPacket(1, 1, 550, TRUE);
		break;
		
	case 2:
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 3:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}
#else
	
	switch (setTestSteps)
	{
	case START_TEST:
		generateTestStatusPacket(SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
		break;
		
	case 1:
			generateTestDataPacket(1, 1);
		break;
		
	case 2:
			generateTestStatusPacket(SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);
		break;
	case 3:
		{	// User exit
			generateTestStatusPacket(SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);
		}
		break;
	}
#endif	// SIGMA_UNIT_TEST
}


void
FakeServiceModeManager::generateTestDataPacket(Int dataCount, Int dataIdx, Real32 dataValue, Boolean setExectData)
{
	TestDataPacket pData;
	Int idx;

	UserRequestToStopTest_ = SmPromptId::NULL_ACTION_ID;
	idx = 0;
	pData.testId = CurrentTestId_;
	pData.dataCount = dataCount;
	pData.startDataIndex = (SmDataId::ItemizedTestDataId)
						(dataIdx + SmDataId::TEST_DATA_START);

	for (idx = 0; idx < dataCount; idx++)
	{
		if (setExectData)
		{
			pData.pointData[idx] = dataValue;
		}
		else
		{
			pData.pointData[idx] = idx + dataIdx + 0.5;
		}

#if defined (FORNOW)
printf("generateTestDataPacket(), testId=%d, dataCount=%d, dataId=%d, data=%f\n",
		pData.testId, pData.dataCount,
		pData.startDataIndex, pData.pointData[idx]);
#endif	// (FORNOW)
	}

	ServiceDataMgr::NewTestData(&pData, sizeof(pData));
	testTimer_.startTimer();
}
	

void
FakeServiceModeManager::generateTestPromptPacket(SmPromptId::PromptId promptId, SmPromptId::KeysAllowedId keyAllowed)
{
	TestPromptPacket pPrompt;

	UserRequestToStopTest_ = SmPromptId::NULL_ACTION_ID;

	if (CurrentPromptSequence_ < MaxPromptSequence_[CurrentTestId_] &&
		UserRequestToStopTest_ == SmPromptId::NULL_ACTION_ID)
	{	// Proceed with the next prompt.
		CurrentPromptSequence_++;
		pPrompt.testId = CurrentTestId_;
		pPrompt.prompt = promptId;
		pPrompt.keysAllowed = keyAllowed;
		
#if defined (FORNOW)
printf("   Prompt command, testId=%d, prompt=%d, keyAllowed=%d\n",
		pPrompt.testId, pPrompt.prompt, pPrompt.keysAllowed);
#endif	// (FORNOW)
		
		ServiceDataMgr::NewPromptData(&pPrompt, sizeof(pPrompt));
	}
	else
	{
		CLASS_PRE_CONDITION(FALSE);
	}
}
	


void
FakeServiceModeManager::generateTestStatusPacket(SmStatusId::TestStatusId testStatus,
							SmStatusId::TestConditionId condition)
{
	TestStatusPacket pStatus;
	static Int count=0;

	UserRequestToStopTest_ = SmPromptId::NULL_ACTION_ID;

#if defined SIGMA_UNIT_TEST
	if (testStatus == SmStatusId::TEST_START)
	{
		pStatus.repeatTestRequested = RepeatTestRequested_;
	}
#elif defined SM_UNIT_TEST
	// Do nothing
#else
	if (testStatus == SmStatusId::TEST_ALERT)
	{
#	if defined SD_FAILURE	
		testStatus = SmStatusId::TEST_FAILURE;
#	elif defined SD_ALERT
		testStatus = SmStatusId::TEST_ALERT;
#	else
		if (randomResult(2))
		{
			if (!((count++) %3))
			{
				testStatus = SmStatusId::TEST_FAILURE;
			}
		}
#	endif	// SD_FAILURE	
	}
	else if (testStatus == SmStatusId::TEST_START)
	{
		pStatus.repeatTestRequested = RepeatTestRequested_;
	}
#endif	// SIGMA_UNIT_TEST

	pStatus.testId = CurrentTestId_;
	pStatus.status = testStatus;
	pStatus.condition = condition;

#if defined (FORNOW)
printf("   Status command, testId=%d, repeatTestRequested = %d, status=%d, condition=%d\n",
		pStatus.testId, pStatus.repeatTestRequested, pStatus.status, pStatus.condition);
#endif	// (FORNOW)
		
		
	ServiceDataMgr::NewStatusData(&pStatus, sizeof(pStatus));

	if (testStatus == SmStatusId::TEST_START ||
		testStatus == SmStatusId::TEST_ALERT ||
		testStatus == SmStatusId::NOT_INSTALLED ||
		testStatus == SmStatusId::TEST_FAILURE)
	{	// Continue to next test with alert or failure errors
		testTimer_.startTimer();
	}
	else if (testStatus == SmStatusId::TEST_END)
	{
		CurrentTestId_ = SmTestId::TEST_NOT_START_ID;
	}
}


void
FakeServiceModeManager::generateFailedTestStatusPacket(SmStatusId::TestStatusId testStatus,
							SmStatusId::TestConditionId condition)
{
	TestStatusPacket pStatus;
	static Int count=0;

	if (testStatus == SmStatusId::TEST_ALERT)
	{
#if defined SD_FAILURE	
		testStatus = SmStatusId::TEST_FAILURE;
#elif defined SD_ALERT
		testStatus = SmStatusId::TEST_ALERT;
#else
		if (randomResult(2))
		{
			if (!((count++) %3))
			{
				testStatus = SmStatusId::TEST_FAILURE;
			}
		}
#endif	// SD_FAILURE	
	}
	else if (testStatus == SmStatusId::TEST_START)
	{
		pStatus.repeatTestRequested = RepeatTestRequested_;
	}

//	UserRequestToStopTest_ = SmPromptId::NULL_ACTION_ID;
	
	pStatus.testId = CurrentTestId_;
	pStatus.status = testStatus;
	pStatus.condition = condition;

#if defined (FORNOW)
printf("   Status command, testId=%d, repeatTestRequested = %d, status=%d, condition=%d\n",
		pStatus.testId, pStatus.repeatTestRequested, pStatus.status, pStatus.condition);
#endif	// (FORNOW)
		
		
	ServiceDataMgr::NewStatusData(&pStatus, sizeof(pStatus));

	// Continue to next test
	testTimer_.startTimer();

}

	
void
FakeServiceModeManager::generateSmLeakDataPacket(Int dataCount, Int dataIdx) {
	TestDataPacket pData;
	Int idx;
	static int num=0;

	idx = 0;
	pData.testId = CurrentTestId_;
	pData.dataCount = dataCount;
	pData.startDataIndex = (SmDataId::ItemizedTestDataId)
						(dataIdx + SmDataId::TEST_DATA_START - 1);

	if (num == 0)
	{
		pData.pointData[0] = 74.0;
		printf("SmLeakData = 74.0\n");
	}
	else
	{
		pData.pointData[0] = 86.0;
		printf("SmLeakData = 86.0\n");
	}


#if defined (SIGMA_UNIT_TEST) || defined (FORNOW)
printf("generateTestDataPacket(), testId=%d, dataCount=%d, dataId=%d, data=%f\n",
		pData.testId, pData.dataCount,
		pData.startDataIndex, pData.pointData[idx]);
#endif	// (SIGMA_UNIT_TEST) || (FORNOW)
		
		

	ServiceDataMgr::NewTestData(&pData, sizeof(pData));
	testTimer_.startTimer();
	num++;
}
	

#endif	// FAKE_SM
