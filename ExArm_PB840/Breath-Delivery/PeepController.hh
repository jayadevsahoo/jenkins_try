
#ifndef PeepController_HH
#define PeepController_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: PeepController - Controller for maintaining peep during exhalation
//			phase.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/PeepController.hhv   25.0.4.0   19 Nov 2013 14:00:02   pvcs  $
//
//@ Modification-Log
//
//  Revision: 009   By: rhj    Date: 07-July-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//       Leak Compensation project-related changes.
//       
//  Revision: 008  By: syw     Date:  09-Oct-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//		VTPC project-related changes:
//      Added getFeedbackPressure().
//
//  Revision: 007  By: syw     Date:  31-Oct-2000    DR Number: 5793
//  Project:  Metabolics
//  Description:
//		New puffless controller implementation.
//
//  Revision: 006  By:  syw    Date:   02-Nov-1999    DR Number: DCS 5573
//       Project:  ATC
//       Description:
//			Added filteredLungFlow_, secondMinPress_, filteredLungVolume_,
//			and isPeepControllerFrozen_
//
//  Revision: 005  By:  syw    Date:  07-Jul-1999    DR Number: DCS 5464
//       Project:  ATC
//       Description:
//		 	Added isCrossing() access method.
//
//  Revision: 004  By:  syw    Date:  24-Jun-1999    DR Number: DCS 5449
//       Project:  ATC
//       Description:
//			Added noise band to determine fuzzy gain.  Removed openLoopTraj_
//			argument from calculateFuzzyGain_().
//
//  Revision: 003  By:  syw    Date:  23-Jun-1999    DR Number: DCS 5447
//       Project:  ATC
//       Description:
//			Add DCS 5322 to header
//
//  Revision: 002  By:  syw    Date:  23-Jun-1999    DR Number: DCS 5446
//       Project:  ATC
//       Description:
//			Added flowOffset_
//
//  Revision: 001  By:  syw    Date:  20-Jan-1999    DR Number: DCS 5322
//       Project:  ATC
//       Description:
//             ATC initial version.
//
//=====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"

//@ Usage-Classes

#include "Resistance.hh"

//@ End-Usage

class PeepController {
  public:
    PeepController( void) ;
    ~PeepController( void) ;
	
    static void SoftFault( const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL, 
			  const char*       pPredicate = NULL) ;

    void newBreath( const Real32 peep, const Real32 flowTarget) ;
    void updatePsolAndExhalationValves( const Uint32 elapsedTimeMs) ;
	inline Real32 getFeedbackPressure( void) const ;
	inline Boolean isControllerFrozen( void) const ;
	inline Real32 getPeepBaseline( void) const ;
	inline Boolean isPeepChanged( void) const ;
		
    inline Real32 getLeakAdd( void) const;
    void   inline setBiasFlowDelta( Real32 biasFlowData);
    void   inline setFilteredBiasFlowDelta( Real32 filBiasFlowData);

	void setKi( Real32 gain );
	Real32 getKi(void) const;

    inline void  resetLeakAdd();

  protected:

  private:
    PeepController( const PeepController&) ;	// not implemented...
    void   operator=( const PeepController&) ;		// not implemented...

	void determineFeedbackPressure_( void) ;
	Real32 calculateErrorGain_( const Real32 error) ;

	//@ Data-Member: feedbackPressure_
	// pressure signal used for control
	Real32 feedbackPressure_ ;

	//@ Data-Member: pressureTerm_
	// integrated pressure sum
	Real32 pressureTerm_ ;

	//@ Data-Member: peep_
	// peep setting
	Real32 peep_ ;

	//@ Data-Member: flowTarget_
	// base flow level
	Real32 flowTarget_ ;

	//@ Data-Member: inspResistance_
	// inspiratory side resistance
	Resistance inspResistance_ ;

	//@ Data-Member: exhResistance_ ;
	// expiratory side resistance
	Resistance exhResistance_ ;

	//@ Data-Member: desiredFlow_
	// desired flow to command to psols
	Real32 desiredFlow_ ;

	//@ Data-Member: noiseBand_
	// flow threshold to determine error gain
	Real32 noiseBand_ ;

	//@ Data-Member: isControllerFrozen_
	// set true once steady state is achieved for a period of time
	Boolean isControllerFrozen_ ;

	//@ Data-Member: steadyStateCounter_
	// number of times steady state condition is met
	Uint32 steadyStateCounter_ ;

	//@ Data-Member: isClosedLoopControl_
	// indicates if the controller is open or closed loop
	Boolean isClosedLoopControl_ ;
	
	//@ Data-Member: isPeepChanged_
	// flag to indicate if new peep value changed 
	Boolean isPeepChanged_ ;
	
	//@ Data-Member: prePeep_
	// to save theprevious peep value 
	Real32 prePeep_;

	//@ Data-Member: prevGeometricTraj_
	// previous geometric trajectory value
	Real32 prevGeometricTraj_ ;

	//@ Data-Member: baseTraj_
	// base trajectory value
	Real32 baseTraj_ ;
	
	//@ Data-Member: leakAdd_
	// The amount of leak added to the flow target.
	Real32 leakAdd_;

	//@ Data-Member: filteredBiasFlowDelta_
	// The amount of additional filtered bias flow 
	// added to flow target
	Real32 filteredBiasFlowDelta_;

	//@ Data-Member: biasFlowDelta_
	// The amount of additional bias flow 
	// added to flow target
	Real32 biasFlowDelta_;

	//@ Data-Member: ki_
	// integral gain
	Real32 ki_;

	//NOTE: "prox" term refers to the proximal flow in the airway
	// but it has nothing to do and not coming from a prox sensor..
	//@ Data-Members for Peep Controller
	Real32 proxPressRef_;     
	Real32 targetPressAdjusted_;  
	Real32 proxPressCorr_;  
	Real32 proxPressCorrPrev_;  
	Real32 proxPressRefPrev_;
	Uint32 peepCtrlFfPrev_;
	Real32 pressDriveRefFiltPrev_;
	Real32  prePeepBaseErr_;   
	Real32 peepBaseErr_;
	Int32 elapsedTimeMs_; 
	Real32 boundPprox_;
	Real32 proxPAvg_;
	Int32 nStable_;
	Int32 nPeepDrop_;
	Int32 nTranBreath_;
	Boolean peepVcvTran_;
	Real32 filterCoeff_;
	Int32 nStableBreath_; //number of breath which reach stable PEEP range
} ;

#include "PeepController.in"

#endif // PeepController_HH 




