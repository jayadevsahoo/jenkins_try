#ifndef TimePlot_IN
#define TimePlot_IN

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
// Class: TimePlot - A drawable class that can plot a specified 
// stream of waveform data against time for use in the Waveforms subscreen.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/TimePlot.inv   25.0.4.0   19 Nov 2013 14:08:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setYUnits
//
//@ Interface-Description
// Sets the Y units, i.e. specifies the patient data id which will be
// used for retrieving the sample data that will be plotted against time.
//---------------------------------------------------------------------
//@ Implementation-Description
// Set yUnits_ to the input parameter.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

inline void
TimePlot::setYUnits(PatientDataId::PatientItemId yUnits)
{
	CLASS_ASSERTION(PatientDataId::NULL_PATIENT_DATA_ITEM != yUnits);

	yUnits_ = yUnits;									// $[TI1]
}

#endif // TimePlot_IN 
