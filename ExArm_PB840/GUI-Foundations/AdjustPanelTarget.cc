#include "stdafx.h"

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1995, Puritan-Bennett Corporation
//====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: AdjustPanelTarget - This abstract class provides a mechanism for
// callbacks from the AdjustPanel.
//---------------------------------------------------------------------
//@ Interface-Description
// Any object that receives AdjustPanel events must have this class as
// one of its parent classes.  These events include knob rotation and
// pressing the Accept and Clear keys.
//
// Although multiple inheritance is not normally allowed, adding this
// class as an additional parent class is acceptable because its only
// use is to implement a callback interface to another class.
//
// Classes derived from this base class will be capable of
// receiving callbacks from the AdjustPanel.  Registering with the 
// AdjustPanel for callback requires a pointer to an object derived from
// this class.
//
// Classes derived from this class will be abstract unless they provide
// definitions for the pure virtual callback methods defined in header 
// file AdjustPanelTarget.hh.  These implement the handling of all
// possible user actions handled by the AdjustPanel.
//---------------------------------------------------------------------
//@ Rationale
// This class was created to provide a framework for the callback
// mechanism for the AdjustPanel class.  
//---------------------------------------------------------------------
//@ Implementation-Description
// The member functions are all virtual functions but do nothing.
// The constructor is protected function so it is not possible to create
// an instance of this class.
//---------------------------------------------------------------------
//@ Fault-Handling
// None.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Foundations/vcssrc/AdjustPanelTarget.ccv   25.0.4.0   19 Nov 2013 14:11:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By: gdc     Date: 21-May-2007    SCR Number: 6237
//  Project:  Trend
//  Description:
//	Implemented TargetType to identify feedback sensitive focus
//  objects.
//
//  Revision: 003   By: gdc    Date:  21-May-2007   SCR Number: 6330
//  Project: Trend
//  Description:
//	Removed SUN prototype code.
//
//  Revision: 002  By: yyy      Date: 25-Jan-1996   DCR Number: 655
//    Project:  Sigma (R8027)
//    Description:
//      Added adjustPanelRestoreFocusHappened method.
//
//  Revision: 001  By: yyy      Date: 01-Jan-1995  DR Number: NONE
//    Project:  Sigma (R8027)
//    Description:
//	Integration baseline.
//=====================================================================

#include "AdjustPanelTarget.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...


//=====================================================================
//
// Public methods
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelKnobDeltaHappened(Int32 delta) [virtual]
//
//@ Interface-Description
// This is a virtual method that does nothing.  Class derived from this base
// class will be capable of receiving callbacks from the AdjustPanel.
// AdjustPanel makes the callback when it receives notification that the knob
// has been turned.  The "delta" parameter specifies how far the knob was
// turned, in units of "knob clicks".
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
AdjustPanelTarget::adjustPanelKnobDeltaHappened(Int32 delta)
{
	CALL_TRACE("AdjustPanelTarget::adjustPanelKnobDeltaHappened(Int32 delta)");
	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelLoseFocusHappened [pure virtual]
//
//@ Interface-Description
// This is a virtual method that does nothing.  Class derived from this base
// class will be capable of receiving callbacks from the AdjustPanel.
// AdjustPanel makes the callback when another AdjustPanelTarget takes the
// focus; this notifies the previous target that it is losing the focus.  If
// the current AdjustPanel focus is cancelled by setting it to NULL, this also
// causes this callback to be executed.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
AdjustPanelTarget::adjustPanelLoseFocusHappened(void)
{
	CALL_TRACE("AdjustPanelTarget::adjustPanelLoseFocusHappened(void)");
	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelRestoreFocusHappened [pure virtual]
//
//@ Interface-Description
// This is a virtual method that does nothing.  Class derived from this base
// class will be capable of receiving callbacks from the AdjustPanel.
// AdjustPanel makes the callback when a default AdjustPanelTarget regains the
// AdjustPanel focus after another target relinquishes the focus.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
AdjustPanelTarget::adjustPanelRestoreFocusHappened(void)
{
	CALL_TRACE("AdjustPanelTarget::adjustPanelRestoreFocusHappened(void)");
	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelAcceptPressHappened [virtual]
//
//@ Interface-Description
// This is a virtual method that does nothing.  It must be defined by a derived
// class if that class needs to perform processing in response to any Accept
// key press.  AdjustPanel makes the callback when it receives notification
// that the Accept key has transitioned to the down state.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//
// Note that the destruction of the AdjustPanelTarget may leave a danggling
// pointer which resides in the AdjustPanel class.  It is the application
// program's responsibility to de-associate the AdjustPanelTarge pointer in
// the  AdjustPanel class before destorys the AdjustPanelTarget object.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
AdjustPanelTarget::adjustPanelAcceptPressHappened(void)
{
	CALL_TRACE("AdjustPanelTarget::adjustPanelAcceptPressHappened(void)");
	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelAcceptReleaseHappened [virtual]
//
//@ Interface-Description
// This is a virtual method that does nothing.  It must be defined by a derived
// class if that class needs to perform processing in response to any Accept
// key release.  AdjustPanel makes the callback when it receives notification
// that the Accept key has transitioned to the up state.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
AdjustPanelTarget::adjustPanelAcceptReleaseHappened(void)
{
	CALL_TRACE("AdjustPanelTarget::adjustPanelAcceptReleaseHappened(void)");
	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelClearPressHappened [virtual]
//
//@ Interface-Description
// This is a virtual method that does nothing.  It must be defined by a derived
// class if that class needs to perform processing in response to any Clear key
// press.  AdjustPanel makes the callback when it receives notification that
// the Clear key has transitioned to the down state.  Note that there is no
// notification when the Clear key is released.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
AdjustPanelTarget::adjustPanelClearPressHappened(void)
{
	CALL_TRACE("AdjustPanelTarget::adjustPanelClearPressHappened(void)");
	// Do nothing
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getTargetType [virtual]
//
//@ Interface-Description
//  This virtual method returns the AdjustPanelTarget::Type for this object.
//  This base class implementation always returns the TYPE_NULL. It
//  is the responsibility of the derived classes to override this method
//  and return a type specific to the derived class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This base class returns TARGET_TYPE_NULL. If a derived class wants 
//  to identify itself as feedback sensitive, it returns a type of 
//  TARGET_TYPE_SETTING.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

AdjustPanelTarget::TargetType AdjustPanelTarget::getTargetType(void) const
{
	CALL_TRACE("getTargetType(void)");

	return AdjustPanelTarget::TARGET_TYPE_NULL;
}


//=====================================================================
//
// Protected methods
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: AdjustPanelTarget
//
//@ Interface-Description
// Construct the object. Since this is an abstract base class, this
// constructor does nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
// This is an abstract base class, this constructor does nothing.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

AdjustPanelTarget::AdjustPanelTarget(void)
{
	CALL_TRACE("AdjustPanelTarget::AdjustPanelTarget(void)");
	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~AdjustPanelTarget
//
//@ Interface-Description
// Destroy object.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This destructor does nothing.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

AdjustPanelTarget::~AdjustPanelTarget(void)
{
	CALL_TRACE("AdjustPanelTarget::~AdjustPanelTarget(void)");
	// Do nothing
}




