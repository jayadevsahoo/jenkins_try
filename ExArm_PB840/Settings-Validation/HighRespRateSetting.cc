#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//================= C L A S S   D E S C R I P T I O N =================
//@ Class: HighRespRateSetting - High Respiratory Rate Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, bounded setting that contains the upper limit of
//  total respiratory rate (in breaths-per-minute), above which the "high
//  respiratory rate alarm" shall be activated.  This class inherits from
//  'BoundedSetting' and provides the specific information needed for
//  representation of high respiratory rate.  This information includes the
//  interval and range of this setting's values, and this batch setting's
//  new-patient value (see 'getNewPatientValue()').
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class has NO dependent settings, and no dynamic bounds.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/HighRespRateSetting.ccv   25.0.4.0   19 Nov 2013 14:27:26   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005   By: sah    Date: 29-Jun-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  modified to use newly-modified 'BoundedRange' class to handle
//         range, via linked list of interval nodes
//      *  eliminated 'get{Max,Min}Limit()' method, now defined by base class
//      *  incorporated circuit-specific ranges
//
//  Revision: 004   By: sah  Date:  11-Jan-1999    DR Number:  5321
//  Project:  ATC
//  Description:
//	As part of making all 'SoftFault()' methods non-inlined, I'm
//	also making those 'SoftFault()' methods that are NOT called
//	in PRODUCTION mode, DEVELOPMENT-only.
//
//  Revision: 003   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'getApplicability()' method
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//=====================================================================

#include "HighRespRateSetting.hh"
#include "SettingConstants.hh"
#include "PatientCctTypeValue.hh"

//@ Usage-Classes
#include "SettingsMgr.hh"
//@ End-Usage


//@ Code...

//====================================================================
//
//  Static Data Definitions...
//
//====================================================================

#define DEFAULT_MAX_VALUE  Real32(170.0f)

//  $[02146] -- The setting's range ...
//  $[02149] -- The setting's resolution ...
static const BoundedInterval  LAST_NODE_ =
  {
    10.0f,		// absolute minimum...
    0.0f,		// unused...
    ONES,		// unused...
    NULL
  };
static BoundedInterval  Node1_ =
  {
    DEFAULT_MAX_VALUE,	// change-point...
    1.0f,		// resolution...
    ONES,		// precision...
    &::LAST_NODE_
  };
static BoundedInterval  IntervalList_ =
  {
    DEFINED_UPPER_ALARM_LIMIT_OFF,// absolute maximum...
    (DEFINED_UPPER_ALARM_LIMIT_OFF - DEFAULT_MAX_VALUE),// resolution...
    ONES,		// precision...
    &::Node1_
  };


//====================================================================
//
//  Public Methods...
//
//====================================================================

//============= M E T H O D   D E S C R I P T I O N ==================
//@ Method: HighRespRateSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This setting inherits from BoundedSetting.  The method passes to
//  BoundedSetting it's default information.  Also, this method
//  initializes the value interval range.
//
//  The resolution for the "max" range below, is calculated by taking
//  the difference between the absolute maximum value and the next lower
//  change-point.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

HighRespRateSetting::HighRespRateSetting(void)
 : BatchBoundedSetting(SettingId::HIGH_RESP_RATE,
		       Setting::NULL_DEPENDENT_ARRAY_,
		       &::IntervalList_,
		       HIGH_RESP_RATE_MAX_ID,	// maxConstraintId...
		       HIGH_RESP_RATE_MIN_ID)	// minConstraintId...
{
  CALL_TRACE("HighRespRateSetting()");
}   // $[TI1]


//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  ~HighRespRateSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

HighRespRateSetting::~HighRespRateSetting(void)
{
  CALL_TRACE("~HighRespRateSetting()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This setting is ALWAYS changeable.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
HighRespRateSetting::getApplicability(const Notification::ChangeQualifier) const
{
  CALL_TRACE("getApplicability(qualifierId)");

  return(Applicability::CHANGEABLE);
}   // $[TI1]


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [const, virtual]
//
//@ Interface-Description
//  Get the setting's new-patient value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method returns a constant representing the new patient value
//  for this setting.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
HighRespRateSetting::getNewPatientValue(void) const
{
  CALL_TRACE("getNewPatientValue()");

  BoundedValue newPatient;

  // $[02147] The setting's new-patient value ...

  // the high respiratory rate's new-patient value is 'OFF'...
  newPatient.value     = SettingConstants::UPPER_ALARM_LIMIT_OFF;
  newPatient.precision = ONES;

  return(newPatient); // $[TI1]
}


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//	      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
HighRespRateSetting::SoftFault(const SoftFaultID  softFaultID,
			       const Uint32       lineNumber,
			       const char*        pFileName,
			       const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
			  HIGH_RESP_RATE_SETTING, lineNumber, pFileName,
			  pPredicate);
}


//====================================================================
//
//  Protected Methods...
//
//====================================================================

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  updateConstraints_()  [virtual]
//
//@ Interface-Description
//  Update the dynamic bounds of this setting.  This method sets this
//  setting's maximum constraint to the most restrictive of this setting's
//  upper bounds, and, correspondingly, set this setting's minimum
//  constraint to its most restrictive lower bound.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02146] -- The setting's range ...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
HighRespRateSetting::updateConstraints_(void)
{
  CALL_TRACE("updateConstraints_()");

  //-------------------------------------------------------------------
  // update upper limit value...
  //-------------------------------------------------------------------
  const Setting*  pPatientCctType =
		    SettingsMgr::GetSettingPtr(SettingId::PATIENT_CCT_TYPE);

  const DiscreteValue  PATIENT_CCT_TYPE_VALUE =
					pPatientCctType->getAdjustedValue();

  // update the upper-most change point, based on the circuit type...
  switch (PATIENT_CCT_TYPE_VALUE)
  {
  case PatientCctTypeValue::NEONATAL_CIRCUIT :		// $[TI1]
    ::Node1_.value = 170.0f; // 1/min...
    break;
  case PatientCctTypeValue::PEDIATRIC_CIRCUIT :
  case PatientCctTypeValue::ADULT_CIRCUIT :		// $[TI2]
    ::Node1_.value = 110.0f; // 1/min...
    break;
  default :
    // unexpected patient circuit type value...
    AUX_CLASS_ASSERTION_FAILURE(PATIENT_CCT_TYPE_VALUE);
    break;
  }

  // update the resolution between the upper-most change point, and the
  // absolute maximum, "OFF" value...
  ::IntervalList_.resolution = (::IntervalList_.value - ::Node1_.value);
}
