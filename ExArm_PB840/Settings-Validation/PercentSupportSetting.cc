#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//================= C L A S S   D E S C R I P T I O N =================
//@ Class: PercentSupportSetting - Percent Support Setting
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, bounded setting that is used to adjust the percent
//  of patient work that is supported during a tube-compensated breath.
//  This class inherits from 'BatchBoundedSetting' and provides the specific
//  information needed for representation of percent support.  This information
//  includes the interval and range of this setting's values, this batch
//  setting's new-patient value (see 'getNewPatientValue()'), and the
//  contraints of this setting.
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class has no dependent settings, and never needs to update its
//  bound values, therefore 'calcDependentValues_()' and 'updateConstraints_()'
//  are NOT overridden.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/PercentSupportSetting.ccv   25.0.4.0   19 Nov 2013 14:27:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 008  By: gfu     Date:  06-Aug-2004    DR Number: 6132
//  Project:  PAV
//  Description:
//	   Modified code per SDCR #6132.  Added soft bound to the 
//     Percent Support setting when PAV support type is active.  
//     Also changed hard coded values to constants defined in 
//     SettingConstants.
//
//  Revision: 007  By: gfu     Date:  27-Jul-2004    DR Number: 6132
//  Project:  PAV
//  Description:
//     Modified code per SDCR #6132:  Based upon input from the Field 
//     Evaluations in Canada:
//     The default %Support value for PAV should be decreased to 50%.
//
//  Revision: 006  By: sah     Date:  02-Aug-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//	VTPC project-related changes:
//      *  added support of new VS spontaneous type
//
//  Revision: 005   By: sah   Date:  23-May-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//	PAV project-related changes:
//      *  added support for new PA spontaneous type value, along with a
//         dynamic range, PA-specific new-patient value, and new
//         transition rules
//
//  Revision: 004   By: sah    Date: 22-Sep-1999    DR Number: 5424
//  Project:  NeoMode
//  Description:
//	Obsoleted 'SettingOptions' class, to use new global
//      'SoftwareOptions' class.
//
//  Revision: 003   By: sah   Date:  22-Jul-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  modified to use newly-modified 'BoundedRange' class to handle
//         range, via linked list of interval nodes
//
//  Revision: 002   By: sah   Date:  06-Jan-1999    DR Number:  5314
//  Project:  ATC
//  Description:
//     Added new 'getApplicability()' method.
//
//  Revision: 001   By: sah   Date:  06-Jan-1999    DR Number:  5322
//  Project:  ATC
//  Description:
//	New ATC-specific setting.
//
//=====================================================================

#include "PercentSupportSetting.hh"
#include "SupportTypeValue.hh"
#include "ModeValue.hh"
#include "SettingConstants.hh"

//@ Usage-Classes
#include "SettingsMgr.hh"
#include "SoftwareOptions.hh"
//@ End-Usage


//@ Code...

//====================================================================
//
//  Static Data Definitions...
//
//====================================================================

//  $[TC02026] -- The setting's range...
//  $[TC02028] -- The setting's resolution ...
static const BoundedInterval  LAST_NODE_ =
  {
    5.0f,		// absolute minimum...
    0.0f,		// unused...
    ONES,		// unused...
    NULL
  };
static const BoundedInterval  INTERVAL_LIST_ =
  {
    100.0f,		// absolute maximum...
    5.0f,		// resolution...
    ONES,		// precision...
    &::LAST_NODE_
  };


//====================================================================
//
//  Public Methods...
//
//====================================================================

//============= M E T H O D   D E S C R I P T I O N ==================
//@ Method: PercentSupportSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This setting inherits from BoundedSetting.  The method passes to
//  BoundedSetting it's default information. Also, this method initializes
//  the value interval range for the setting.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

PercentSupportSetting::PercentSupportSetting(void)
   : BatchBoundedSetting(SettingId::PERCENT_SUPPORT,
			 Setting::NULL_DEPENDENT_ARRAY_,
			 &::INTERVAL_LIST_,
			 PERCENT_SUPPORT_MAX_ID,// maxConstraintId...
			 PERCENT_SUPPORT_MIN_ID)// minConstraintId...
{
  CALL_TRACE("PercentSupportSetting()");

  
  // register this setting's soft bound.  This setting will
  // be used to add a soft bound limit to the % support in
  // PAV - the soft bound shall not be active in TC.  
  
  registerSoftBound_(::PERCENT_SUPPORT_SOFT_MAX_ID, Setting::UPPER);
  
}  // $[TI1]


//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  ~PercentSupportSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

PercentSupportSetting::~PercentSupportSetting(void)
{
  CALL_TRACE("~PercentSupportSetting()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[01071] -- main settings applicability
//  $[01081] -- breath settings applicability
//---------------------------------------------------------------------
//@ PreCondition
//  (qualifierId == Notification::ACCEPTED  ||
//   qualifierId == Notification::ADJUSTED)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
PercentSupportSetting::getApplicability(const Notification::ChangeQualifier qualifierId) const
{
  CALL_TRACE("getApplicability(qualifierId)");

  Applicability::Id  applicability;

  if (SoftwareOptions::IsOptionEnabled(SoftwareOptions::ATC)  ||
      SoftwareOptions::IsOptionEnabled(SoftwareOptions::PAV))
  {  // $[TI1] -- TC and/or PA is an active option...
    const Setting*  pSupportType =
			SettingsMgr::GetSettingPtr(SettingId::SUPPORT_TYPE);

    if (pSupportType->getApplicability(qualifierId) !=
						Applicability::INAPPLICABLE)
    {  // $[TI1.1] -- support type is applicable...
      DiscreteValue  supportTypeValue;
      
      switch (qualifierId)
      {
      case Notification::ACCEPTED :		// $[TI1.1.1]
	supportTypeValue = pSupportType->getAcceptedValue();
	break;
      case Notification::ADJUSTED :		// $[TI1.1.2]
	supportTypeValue = pSupportType->getAdjustedValue();
	break;
      default :
	AUX_CLASS_ASSERTION_FAILURE(qualifierId);
	break;
      }

      switch (supportTypeValue)
      {
      case SupportTypeValue::PSV_SUPPORT_TYPE :
      case SupportTypeValue::OFF_SUPPORT_TYPE :
      case SupportTypeValue::VSV_SUPPORT_TYPE :	// $[TI1.1.3]
	applicability = Applicability::INAPPLICABLE;
	break;
      case SupportTypeValue::ATC_SUPPORT_TYPE :
      case SupportTypeValue::PAV_SUPPORT_TYPE :	// $[TI1.1.4]
	applicability = Applicability::CHANGEABLE;
	break;
      default :
	// unexpected 'supportTypeValue'...
	AUX_CLASS_ASSERTION_FAILURE(supportTypeValue);
	break;
      };
    }
    else
    {  // $[TI1.2] -- support type is NOT applicable...
      // percent support can't be applicable if support type isn't...
      applicability = Applicability::INAPPLICABLE;
    }
  }
  else
  {  // $[TI2] -- TC and PA are not an active option...
    applicability = Applicability::INAPPLICABLE;
  }

  return(applicability);
}


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method: acceptTransition(settingId, newValue, currValue) [virtual]
//
//@ Interface-Description
//  This method is responsible for updating this setting based on the
//  the Main Control Setting identified by 'settingId' and the transition
//  of its value from 'currValue' to 'newValue'.
//
//  The setting is interested in the transition of mode to "SIMV" or "SPONT,
//  and of support type to "PSV".
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02025]\c\ -- transitioning from A/C to TC-applicable...
//  $[PA02000]  -- transitioning to TC or PA...
//---------------------------------------------------------------------
//@ PreCondition
//  ((settingId == SettingId::MODE           &&
//    currValue == ModeValue::AC_MODE_VALUE)  ||
//   (settingId == SettingId::SUPPORT_TYPE           &&
//    (newValue  == SupportTypeValue::ATC_SUPPORT_TYPE  ||
//     newValue  == SupportTypeValue::PAV_SUPPORT_TYPE)))
//---------------------------------------------------------------------
//@ PostCondition
//   None
//@ End-Method
//=====================================================================

void
PercentSupportSetting::acceptTransition(
				    const SettingId::SettingIdType settingId,
				    const DiscreteValue            newValue,
				    const DiscreteValue            currValue
				       )
{
  CALL_TRACE("acceptTransition(settingId, newValue, currValue)");

  if (settingId == SettingId::MODE)
  {
    // $[TI1] -- "from A/C"...
    AUX_CLASS_PRE_CONDITION((currValue == ModeValue::AC_MODE_VALUE), currValue);
  }
  else if (settingId == SettingId::SUPPORT_TYPE)
  {
    // $[TI2] -- "to TC" or "to PA"...
    AUX_CLASS_PRE_CONDITION(
    			(newValue == SupportTypeValue::ATC_SUPPORT_TYPE  ||
    			 newValue == SupportTypeValue::PAV_SUPPORT_TYPE),
			newValue
			   );
  }
  else
  {
    AUX_CLASS_ASSERTION_FAILURE(settingId);
  }

  if (getApplicability(Notification::ADJUSTED) !=
					      Applicability::INAPPLICABLE)
  {   // $[TI3] -- percent support is applicable...
    // use new-patient value...
    BoundedValue  percentSupp = getNewPatientValue();

    BoundStatus  dummyStatus(getId());

    // During transition, we need to activate / deactivate
    // the soft bound based on the support type.  Soft bound
    // shall only be active in PAV.

    if (newValue == SupportTypeValue::PAV_SUPPORT_TYPE)
    {
       activateSoftBound_(::PERCENT_SUPPORT_SOFT_MAX_ID);
    }
    else
    {
       deactivateSoftBound_(::PERCENT_SUPPORT_SOFT_MAX_ID);
    }
    

    // calculate bound constraints...
    updateConstraints_();

    // use the calculation of the min/max bound in 'updateConstraints_()'
    // to limit the current adjusted value to a valid range...
    getBoundedRange_().testValue(percentSupp, dummyStatus);

    // store the transition value into the adjusted context...
    setAdjustedValue(percentSupp);

    // activation of transition rule must be italicized...
    setForcedChangeFlag();
  } // $[TI4] -- percent support is NOT applicable...
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [const, virtual]
//
//@ Interface-Description
//  Return the setting's new patient value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The method returns a static constant.
//
//  $[TC02027] -- setting's new-patient value...
//   [TC02027] [P10.1.10.e][P10.1.1.e] [PA-MODIFIED] 
//        New-Patient: 	100%; when spontaneous type = TC  
//                       50%; when spontaneous type = PA.  
//
//   SDCR #6132  Based upon input from the Field Evaluations in Canada:  
//               the default % Support value for PAV should be decreased
//               to 50%.

//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
PercentSupportSetting::getNewPatientValue(void) const
{
  CALL_TRACE("getNewPatientValue()");

  BoundedValue  newPatient;

  const Setting*  pSpontType =
		    SettingsMgr::GetSettingPtr(SettingId::SUPPORT_TYPE);

  const DiscreteValue  SPONT_TYPE_VALUE = pSpontType->getAdjustedValue();

  switch (SPONT_TYPE_VALUE)
  {
  case SupportTypeValue::OFF_SUPPORT_TYPE :
  case SupportTypeValue::PSV_SUPPORT_TYPE :
  case SupportTypeValue::ATC_SUPPORT_TYPE :
  case SupportTypeValue::VSV_SUPPORT_TYPE :		// $[TI1]
    newPatient.value = SettingConstants::NEW_PATIENT_PERCENT_SUPP_NON_PAV;
    break;
  case SupportTypeValue::PAV_SUPPORT_TYPE :		// $[TI2]
    newPatient.value = SettingConstants::NEW_PATIENT_PERCENT_SUPP_PAV;
    break;
  default :
    // unexpected patient spont type value...
    AUX_CLASS_ASSERTION_FAILURE(SPONT_TYPE_VALUE);
    break;
  }

  newPatient.precision = ONES;

  return(newPatient);
}


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//	      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
PercentSupportSetting::SoftFault(const SoftFaultID  softFaultID,
				 const Uint32       lineNumber,
				 const char*        pFileName,
				 const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
			  PERCENT_SUPPORT_SETTING, lineNumber, pFileName,
			  pPredicate);
}


//====================================================================
//
//  Protected Methods...
//
//====================================================================

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  updateConstraints_()  [virtual]
//
//@ Interface-Description
//  Update the dynamic bounds of this setting.  This method sets this
//  setting's maximum constraint to the most restrictive of this setting's
//  upper bounds, and, correspondingly, sets this setting's minimum
//  constraint to its most restrictive lower bound.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[TC02026] -- setting's range...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
PercentSupportSetting::updateConstraints_(void)
{
  CALL_TRACE("updateConstraints_()");

  BoundedRange::ConstraintInfo  constraintInfo;

  //-------------------------------------------------------------------
  //  update maximum constraint...
  //  
  //  The maximum  constraint varies based on the Support Type 
  //  selected.  In PAV, the control has a soft bound at 80%.
  //  No soft bounds are required for other support types.
  //-------------------------------------------------------------------

  const Setting*  pSpontType = SettingsMgr::GetSettingPtr(SettingId::SUPPORT_TYPE);

  const DiscreteValue  SPONT_TYPE_VALUE = pSpontType->getAdjustedValue();
  Real32  dummyMinValue;
  
  switch (SPONT_TYPE_VALUE)
  {
  case SupportTypeValue::OFF_SUPPORT_TYPE :
  case SupportTypeValue::PSV_SUPPORT_TYPE :
  case SupportTypeValue::ATC_SUPPORT_TYPE :
  case SupportTypeValue::VSV_SUPPORT_TYPE :     
     
     constraintInfo.value  = getAbsoluteMaxValue_();
     constraintInfo.id     = PERCENT_SUPPORT_MAX_ID;
     constraintInfo.isSoft = FALSE;
     break;

  case SupportTypeValue::PAV_SUPPORT_TYPE :

     // PAV support is selected. We need to check if the soft bound
     // is active to set the appropriate constraint.  When active, 
     // the soft bound max value will be acquired with a call to
     // the virtual function isSoftBoundActive_()
     // 
     if (isSoftBoundActive_(::PERCENT_SUPPORT_SOFT_MAX_ID))
     {
        findSoftMinMaxValues_(dummyMinValue,constraintInfo.value);
        constraintInfo.id     = PERCENT_SUPPORT_SOFT_MAX_ID;
        constraintInfo.isSoft = TRUE;
     }
     else
     {
     
        constraintInfo.value  = getAbsoluteMaxValue_();
        constraintInfo.id     = PERCENT_SUPPORT_MAX_ID;
        constraintInfo.isSoft = FALSE;
     }
     break;
  default:
     // unexpected patient spont type value...
     AUX_CLASS_ASSERTION_FAILURE(SPONT_TYPE_VALUE);
     break;
  }
  

  getBoundedRange_().updateMaxConstraint(constraintInfo);

  //-------------------------------------------------------------------
  //  update minimum constraint...
  //-------------------------------------------------------------------

  constraintInfo.value  = getAbsoluteMinValue_();
  constraintInfo.id     = PERCENT_SUPPORT_MIN_ID;
  constraintInfo.isSoft = FALSE;

  getBoundedRange_().updateMinConstraint(constraintInfo);
}  // $[TI1]


//================ M E T H O D   D E S C R I P T I O N ================
//@ Method:  getAbsoluteMaxValue_()  [const, virtual]
//
//@ Interface-Description
//  Use this to return the absolute maximum value that is currently available
//  from this setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Real32
PercentSupportSetting::getAbsoluteMaxValue_(void) const
{
  CALL_TRACE("getAbsoluteMaxValue_()");

  Real32  absoluteMaxValue;

  const Setting*  pSpontType =
		    SettingsMgr::GetSettingPtr(SettingId::SUPPORT_TYPE);

  const DiscreteValue  SPONT_TYPE_VALUE = pSpontType->getAdjustedValue();

  switch (SPONT_TYPE_VALUE)
  {
  case SupportTypeValue::OFF_SUPPORT_TYPE :
  case SupportTypeValue::PSV_SUPPORT_TYPE :
  case SupportTypeValue::ATC_SUPPORT_TYPE :
  case SupportTypeValue::VSV_SUPPORT_TYPE :		// $[TI1]
    absoluteMaxValue = ::INTERVAL_LIST_.value;  // absolute maximum...
    break;
  case SupportTypeValue::PAV_SUPPORT_TYPE :		// $[TI2]
    absoluteMaxValue = SettingConstants::ABS_MAX_PERCENT_SUPP_PAV;
    break;
  default :
    // unexpected patient spont type value...
    AUX_CLASS_ASSERTION_FAILURE(SPONT_TYPE_VALUE);
    break;
  }

  return(absoluteMaxValue);
}


//================ M E T H O D   D E S C R I P T I O N ================
//@ Method:  getAbsoluteMinValue_()  [const, virtual]
//
//@ Interface-Description
//  Use this to return the absolute minimum value that is currently available
//  from this setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Real32
PercentSupportSetting::getAbsoluteMinValue_(void) const
{
  CALL_TRACE("getAbsoluteMinValue_()");

  Real32  absoluteMinValue;

  const Setting*  pSpontType =
		    SettingsMgr::GetSettingPtr(SettingId::SUPPORT_TYPE);

  const DiscreteValue  SPONT_TYPE_VALUE = pSpontType->getAdjustedValue();

  switch (SPONT_TYPE_VALUE)
  {
  case SupportTypeValue::OFF_SUPPORT_TYPE :
  case SupportTypeValue::PSV_SUPPORT_TYPE :
  case SupportTypeValue::PAV_SUPPORT_TYPE :
  case SupportTypeValue::VSV_SUPPORT_TYPE :		// $[TI1]
    absoluteMinValue = ::LAST_NODE_.value;  // absolute minimum...
    break;
  case SupportTypeValue::ATC_SUPPORT_TYPE :		// $[TI2]
    absoluteMinValue = SettingConstants::ABS_MIN_PERCENT_SUPP_ATC;
    break;
  default :
    // unexpected patient spont type value...
    AUX_CLASS_ASSERTION_FAILURE(SPONT_TYPE_VALUE);
    break;
  }

  return(absoluteMinValue);
}

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  findSoftMinMaxValues_(rSoftMinValue, rSoftMaxValue)  [const]
//
//@ Interface-Description
//  Return, via 'rSoftMinValue' and 'rSoftMaxValue', the soft bound lower
//  and upper limit values, respectively.
//---------------------------------------------------------------------
//@ Implementation-Description
//  setting's soft range...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
PercentSupportSetting::findSoftMinMaxValues_(Real32& rSoftMinValue,
					                              Real32& rSoftMaxValue) const
{
  CALL_TRACE("findSoftMinMaxValues_(rSoftMinValue, rSoftMaxValue)");

  const Setting*  pSpontType = SettingsMgr::GetSettingPtr(SettingId::SUPPORT_TYPE);

  const DiscreteValue  SPONT_TYPE_VALUE = pSpontType->getAdjustedValue();
  
  switch (SPONT_TYPE_VALUE)
  {
     
  case SupportTypeValue::PAV_SUPPORT_TYPE :
     rSoftMaxValue = SettingConstants::MAX_SOFT_PERCENT_SUPP_PAV;   // 80.0f
     break;
  case SupportTypeValue::OFF_SUPPORT_TYPE :
  case SupportTypeValue::PSV_SUPPORT_TYPE :
  case SupportTypeValue::ATC_SUPPORT_TYPE :
  case SupportTypeValue::VSV_SUPPORT_TYPE :		
     rSoftMaxValue = getAbsoluteMaxValue_();
     break;
  default:
     // unexpected patient spont type value...
     AUX_CLASS_ASSERTION_FAILURE(SPONT_TYPE_VALUE);
     break;
 
  }

  // no lower soft-bound for this setting...
  rSoftMinValue = getAbsoluteMinValue_();
}

