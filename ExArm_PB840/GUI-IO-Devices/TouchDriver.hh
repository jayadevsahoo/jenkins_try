#ifndef TouchDriver_HH
#define TouchDriver_HH
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1994-2001, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
//@ Class: TouchDriver.hh - Touch Frame application interface and base class.
//---------------------------------------------------------------------
//  Version-Information
// @(#) $Header:   /840/Baseline/GUI-IO-Devices/vcssrc/TouchDriver.hhv   25.0.4.0   19 Nov 2013 14:12:20   pvcs  $
//
//  Modification-Log
//
//  Revision: 005   By: gdc    Date: 16-Aug-2010     SCR Number: 6663
//  Project: API/MAT
//  Description:
//  Added support for VentSet API. Improved touch screen responsiveness 
//  by reducing inter-character delay to 10ms from 20ms.
// 
//  Revision: 004   By: gdc    Date: 26-Jan-2009    SCR Number: 6407
//  Project:  840S
//  Description:
//      Modified to bring source into strict ARM compliance.
//
//  Revision: 003  By:  gdc    Date:  07-Aug-2008    SCR Number: 6439
//  Project:  840S
//  Description:
//  Fixed "drill-down" problem. See .cc for full description of fix.
//
//  Revision: 002  By:  quf    Date:  18-Dec-2001    DCS Number: 5977
//  Project:  GuiComms
//  Description:
//  Added handleErrorReport_() and measuredReportLength_.
//
//  Revision: 001  By:  gdc    Date:  03-Jan-2001    DCS Number: 5493
//  Project:  GuiComms
//  Description:
//    Initial version to coding standards (Integration baseline).
//=====================================================================
#include "Sigma.hh"
#include "UserAnnunciationMsg.hh"
#include "MsgQueue.hh"
#include "OsTimeStamp.hh"
#include "IpcIds.hh"


//@ Code...

class ChangeStateMessage;
class OsTimeStamp;

class TouchDriver
{
  public:

	enum CoordinateEvent
	{
	  TOUCH_RELEASED = 0,
	  TOUCH_PRESSED = 1
	};

	TouchDriver(void);
	~TouchDriver(void);

	static void	Initialize(void);
	static Boolean IsTouchScreenReleased(void);
	static void RouteTouchEvents(IpcId queueId);
	static void AckTouchInput(void);
	static void	ReadTouch(Int16 &x,Int16 &y);

    void handleCoordinateReport_(
			Int32 col, Int32 row, CoordinateEvent event);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL,
						  const char*       pPredicate = NULL);

  protected:
	void postTouch_(UserAnnunciationMsg::TouchAction action);
	static void routeTouchEvents_(IpcId queueId);

  private:

	//@ Data-Member: Col_
	// Contains the column number (x-coordinate) that was blocked.
	// Used to detect location changes and touch releases.
	static Int16 Col_;

	//@ Data-Member: Row_
	// Contains the row number (y-coordinate) that was blocked.
	// Used to detect location changes and touch releases.
	static Int16 Row_;

	//@ Data-Member: prevCol_
	// Contains the column number (x-coordinate) that was blocked 
	// previously. Used to detect location changes and touch releases.
	Int32 prevCol_;

	//@ Data-Member: prevRow_
	// Contains the row number (y-coordinate) that was blocked 
	// previously. Used to detect location changes and touch releases.
	Int32 prevRow_;

	//@ Data-Member: TouchInputAllowed_
	// touch screen throttle avoids filling up GUI-Apps
	// queue causing an assertion - queue flow control
	static Boolean	TouchInputAllowed_;

	//@ Data-Member: touchQueueId_
	// Contains the queue identifier of the task that has requested to
	// receive coordinate reports. This normally contains the queue
	// identifier for the GuiApp task.
	static IpcId touchQueueId_;

	//@ Data-Member: touchDelay_
	// Touch delay timer
	OsTimeStamp touchDelay_;

	//@ Data-Member: isTouchScreenReleased
	//Touch Screen Released
	static Boolean isTouchScreenReleased_;
};

#endif // TouchDriver_HH
