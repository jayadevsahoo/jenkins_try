#include "StdAfx.h"
#include "BDSendSettings.h"

CBDSendSettings::CBDSendSettings(void)
{
        IConfiguration_F   *f1;  
        ISettingsCapture_F *f2;
        bool bRetval;
    

        // Get a handle to the DLL module.
        hinstLib = LoadLibrary(TEXT("PCAPP.dll"));

        if(hinstLib == NULL)
        {
			AfxMessageBox("Unable to load PCApp.dll");
            throw -1;
        }
     
        f1 =  (IConfiguration_F   *)GetProcAddress(hinstLib, "getConfigurationInstance");
        f2 =  (ISettingsCapture_F *)GetProcAddress(hinstLib, "getSettingsCaptureInstance");

        if(f1==NULL || f2==NULL)
        {
			AfxMessageBox("Invalid PCApp.dll");
            FreeLibrary(hinstLib);
            throw -1;
        }

        m_pIConfiguration   =  f1();


        m_pIConfiguration     = getConfigurationInstance();

        if(m_pIConfiguration == NULL)
		{
			LOG_MESSAGE(LM_ERROR,"Error in creating instance of getConfigurationInstance");
            return;
        }

        bRetval = m_pIConfiguration->loadXMLDocument ("setting_id.xml");

        if(bRetval == false)
        {
            m_pIConfiguration->destroy();
			LOG_MESSAGE(LM_ERROR, "Unable to load setting_id.xml");
            return;
        }




}

CBDSendSettings::~CBDSendSettings(void)
{
	m_pISettingsCapture->destroy();
	m_pIConfiguration  ->destroy();
     
}



int CBDSendSettings::BDSendSettings(char **pData, char *pHost, char *pPort)
{
	int nReturn = m_pIConfiguration->sendSettings(pData, pHost, pPort);
	
	if(nReturn == -1) return -1;


	return 0;
}