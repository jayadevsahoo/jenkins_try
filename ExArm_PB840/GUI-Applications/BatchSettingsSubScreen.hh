
#ifndef BatchSettingsSubScreen_HH
#define BatchSettingsSubScreen_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: BatchSettingsSubScreen
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/BatchSettingsSubScreen.hhv   25.0.4.0   19 Nov 2013 14:07:36   pvcs  $
//
//@ Modification-Log
// 
//  Revision: 005  By:  ksg	   Date:  20-Jun-2007    SCR Number: 6237
//  Project:  TREND
//  Description:
//		Added support to handle settings change timeouts while 
//  	no BatchSettingsSubscreen derivative is active.
//
//  Revision: 004  By: sah     Date:  23-May-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      *  added new 'setTitleChangeState_()' method for standardized
//         handling of run-time formatting of discrete setting values
//
//  Revision: 003  By:  sah	   Date:  23-Feb-1999    DCS Number:  5314
//  Project:  ATC
//  Description:
//      Added integrated with new dynamic, setting applicability functionality.
//      This allows for looser coupling between settings and GUI, and the
//      need for significantly less "setting knowledge" in the GUI subsystem.
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  sah    Date:  23-Feb-98    DR Number: 
//  Project:  UserPrefs
//  Description:
//      Integration baseline.
//
//====================================================================

//@ Usage-Classes
#include "AdjustPanelTarget.hh"
#include "GuiTimerTarget.hh"
#include "SubScreen.hh"
#include "ContextObserver.hh"
#include "TextField.hh"
//@ End-Usage

// forward declare
class TimeoutSubScreen;

class BatchSettingsSubScreen : public SubScreen, public AdjustPanelTarget,
	public GuiTimerTarget, public ContextObserver
{
public:
	BatchSettingsSubScreen(SubScreenArea *pSubScreenArea);
	~BatchSettingsSubScreen(void);

	// SubScreen virtual methods...
	virtual void activate(void);
	virtual void deactivate(void);

	// AdjustPanelTarget virtual methods
	virtual void  adjustPanelAcceptPressHappened(void);
	virtual void  adjustPanelClearPressHappened(void);
	virtual void  adjustPanelLoseFocusHappened(void);
	virtual void  adjustPanelRestoreFocusHappened(void);

	// ContextObserver virtual method...
	virtual void  batchSettingUpdate(const Notification::ChangeQualifier qualifierId,
									 const ContextSubject*               pSubject,
									 const SettingId::SettingIdType      settingId);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);


protected:
	enum  TransitionId_
	{
		CURRENT_TO_PROPOSED,
		PROPOSED_TO_CURRENT,
		NO_TRANSITION
	};

	virtual void  activateHappened_  (void) = 0;
	virtual void  deactivateHappened_(void) = 0;

	virtual void  valueUpdateHappened_(
									  const TransitionId_                 transitionId,
									  const Notification::ChangeQualifier qualifierId,
									  const ContextSubject*               pSubject
									  ) = 0;

	inline Boolean  areSettingsCurrent_(void) const;

	inline void  setCurrentSettingsFlag_(const Boolean flagValue);

	void  setTitleChangeState_(const StringId titleText,
							   const Boolean  isChanged,
							   TextField&     titleField);

private:
	// these methods are purposely declared, but not implemented...
	BatchSettingsSubScreen(void);						// not implemented...
	BatchSettingsSubScreen(const BatchSettingsSubScreen&);// not implemented...
	void   operator=(const BatchSettingsSubScreen&);	// not implemented...

	//@ Data-Member:  currentSettingsFlag_
	// Boolean flag indicating whether this subscreen's setting values
	// are current (equal to "accepted" values), or not.
	Boolean  currentSettingsFlag_;

};


#include "BatchSettingsSubScreen.in"


#endif // BatchSettingsSubScreen_HH 
