#include "stdafx.h"
////////////////Copyright (c) 2014 Covidien/////////////////////////////////////
/*******************************************************************************
bitbucket.org/covidien/e600.git
Name:		ScreenCaptureMgr.cpp

Purpose:
	Manage the screen capture files. 

REVISION HISTORY:

REVISION: 001	BY: SF		DATE: 10-MAR-2014
PROJECT: 880 vent
DR NUMBER: N / A
DESCRIPTION:
	Initialize version.

END OF REVISIONS

*******************************************************************************/

#include <algorithm>

#include "ScreenCaptureMgr.h"

#define DS_BITMAP_FILEMARKER  ((WORD) ('M' << 8) | 'B')    // is always "BM" = 0x4D42

#if defined(WIN32)
    const TCHAR*    DEFAULT_SCREENSHOT_DIR         = _T("\\VentScreenshots\\");
#elif defined(_WIN32_WCE)
    const TCHAR*    DEFAULT_SCREENSHOT_DIR         = _T("\\FlashFX Disk\\VentScreenshots\\");
#endif

ScreenCaptureMgr::ScreenCaptureMgr(void)
    : screenShotsDir_( DEFAULT_SCREENSHOT_DIR )
    , initializedError_( 0 )
{
}

ScreenCaptureMgr::~ScreenCaptureMgr(void)
{
}


int ScreenCaptureMgr::initialize()
{
    WIN32_FIND_DATA FindFileData;
    HANDLE hFind = INVALID_HANDLE_VALUE;
    DWORD dwError;
    initializedError_ = 0;

    TCHAR DirSpec[MAX_PATH];

    if( DirSpec == NULL )
    {
        printf( "Insufficient memory available\n" );
        initializedError_ = -1;
    }
    else
    {
        DWORD dwAttrib = GetFileAttributes( screenShotsDir_ );

        bool dirExists = (dwAttrib != INVALID_FILE_ATTRIBUTES &&
                            (dwAttrib & FILE_ATTRIBUTE_DIRECTORY) );

        if ( !dirExists )
        {
            BOOL created = CreateDirectory( screenShotsDir_, NULL );
            if ( !created )
            {
                initializedError_ = -1;
            }
        }

        // Prepare string for use with FindFile functions.  First, 
        // copy the string to a buffer, then append '\*' to the 
        // directory name.
        _tcscpy(DirSpec, screenShotsDir_);
        _tcscat(DirSpec, _T("\\*.bmp"));

        // Find the first file in the directory.
        hFind = FindFirstFile(DirSpec, &FindFileData);

        if (hFind != INVALID_HANDLE_VALUE) 
        {
            // List all the other files in the directory.
            do
            {
                bitmapList_.push_back(FindFileData.cFileName);
            }
            while (FindNextFile(hFind, &FindFileData) != 0);

            FindClose(hFind);

            dwError = GetLastError();
            if (dwError != ERROR_NO_MORE_FILES) 
            {
                printf ("FindNextFile error. Error is %u.\n", dwError);
                initializedError_ = (-1);
            }
        }
    }

    return initializedError_;
}

int ScreenCaptureMgr::appendToBuffer_(const tstring & sFileName)
{
    if ( !isScreenshotExists_( sFileName ) )
    {
        bitmapList_.push_back(sFileName);
    }

    return 0;
}

bool ScreenCaptureMgr::clear()
{
    if ( initializedError_ )
    {
        return false;
    }

    bool clearFailed = false;

    for(vector<tstring>::iterator it = bitmapList_.begin();
        it != bitmapList_.end(); it++)
    {
        tstring szName = *it;

        tstring sFullPath = screenShotsDir_;
        sFullPath += szName.c_str();
        if ( !DeleteFile(sFullPath.c_str()) )
        {
            clearFailed = true;
        }
    }

    bitmapList_.clear();

    return !clearFailed;
}

int ScreenCaptureMgr::queryScreenshotFileName(int index, TCHAR * rScreenAddr)
{
    if(initializedError_ || index < 0 || index >= bitmapList_.size())
    {
        return -1;
    }

    _tcscpy(rScreenAddr, bitmapList_[index].c_str());

    return 0;
}

const TCHAR * ScreenCaptureMgr::getScreenshotsDirectory() 
{
    return ScreenCaptureMgr::screenShotsDir_;
}

SWATStatus ScreenCaptureMgr::getBufferSize( int& bufferSize )
{
    if ( isInitialized_() )
    {
        bufferSize = bitmapList_.size();
        return SWAT_SUCCESS;
    }
    
    return SWAT_SS_INIT_ERROR;
}

bool ScreenCaptureMgr::isInitialized_()
{
    return !initializedError_;
}

SWATStatus ScreenCaptureMgr::saveScreenToFile(int x, int y, int w, int h,
                                              const TCHAR* pBmpFileName)
{
    if ( isScreenshotExists_( pBmpFileName ) )
    {
        return SWAT_SS_ALREADY_EXISTS;
    }

    tstring sFullPath = getScreenshotsDirectory();
    sFullPath += pBmpFileName;

    bool    isSaved           = false;
	int     nFullScreenWidth  = 0;
	int	    nFullScreenHeight = 0;
	HDC     hScrDC            = NULL;
	HDC     hMemDC            = NULL;
	HGDIOBJ oldbmp            = NULL;
	HBITMAP hBitmap           = NULL;
	BYTE*   lpBitmapBits      = NULL; 

	try
	{
		hScrDC = ::GetDC(NULL);
		if(!hScrDC)
			throw _T("GetDC failed");

		//get screen info
		int nBitsPix = GetDeviceCaps(hScrDC, BITSPIXEL), nBytesPix = nBitsPix / 8;
		nFullScreenWidth = GetDeviceCaps(hScrDC, HORZRES);
		nFullScreenHeight = GetDeviceCaps(hScrDC, VERTRES);

        // bmp file name, default is CapScreen plus timestamp 
        // (CapScreenYYYYMMDD_HHmmSS.bmp)
		TCHAR szBmpFileName[MAX_PATH] = {0};
		validateParams_(nFullScreenWidth, nFullScreenHeight, x, y, w, h,
			           sFullPath.c_str(), szBmpFileName);
		
		//init bitmap info header
		BITMAPINFO bi; 
		ZeroMemory(&bi, sizeof(BITMAPINFO));
		bi.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
		bi.bmiHeader.biWidth = w;
		bi.bmiHeader.biHeight = h;
		bi.bmiHeader.biPlanes = 1;
		bi.bmiHeader.biBitCount = nBitsPix;

		//copy the screen into a bitmap in memory
		hMemDC = ::CreateCompatibleDC(hScrDC); 
		if(!hMemDC)
			throw _T("CreateCompatibleDC failed");

		hBitmap = ::CreateDIBSection(hMemDC, &bi, DIB_RGB_COLORS, 
                                     (LPVOID*)&lpBitmapBits, NULL, 0);

		if(!hBitmap)
			throw _T("CreateDIBSection failed");

		oldbmp = ::SelectObject(hMemDC, hBitmap); 
		if(!oldbmp)
			throw _T("SelectObject failed");

		if(0 == ::BitBlt(hMemDC, 0, 0, w, h, hScrDC, x, y, SRCCOPY))
			throw _T("BitBlt failed");

		//init bmp file header
		BITMAPFILEHEADER bh;
		ZeroMemory(&bh, sizeof(BITMAPFILEHEADER));
		bh.bfType = DS_BITMAP_FILEMARKER;
		bh.bfOffBits = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER);
		bh.bfSize = bh.bfOffBits + (w*h)* nBytesPix;

		//write bmp file
		HANDLE file = CreateFile(szBmpFileName, GENERIC_WRITE, 0, NULL,
                                    CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);

		if(INVALID_HANDLE_VALUE == file)
			throw _T("CreateFile failed");

		DWORD dwBytesWriten;
		WriteFile(file, &bh, sizeof(BITMAPFILEHEADER), &dwBytesWriten, NULL);
		WriteFile(file, &(bi.bmiHeader), sizeof(BITMAPINFOHEADER), &dwBytesWriten, NULL);
		WriteFile(file, lpBitmapBits, nBytesPix * w * h, &dwBytesWriten, NULL);
		CloseHandle(file);

        isSaved = true;
	}
	catch(TCHAR *szErr)
	{
		wprintf(szErr);
	}

	if(oldbmp)
		::SelectObject(hMemDC, oldbmp);
	if(hBitmap)
		::DeleteObject(hBitmap);
	if(hMemDC)
		::DeleteObject(hMemDC);
	if(hScrDC)
		::ReleaseDC(NULL, hScrDC);

    if ( isSaved )
    {
        appendToBuffer_( pBmpFileName );
    }

	return  SWAT_SUCCESS;
}

void ScreenCaptureMgr::validateParams_(const int nFullScreenWidth, 
                                      const int nFullScreenHeight,
                                      int & x, int & y, int & w, int & h,
                                      const TCHAR * pBmpFileNameIn, 
                                      TCHAR * szBmpFileNameOut)
{
    	if(x < 0 || x >= nFullScreenWidth)
		x = 0;

	if(w < 1 || (x + w > nFullScreenWidth))
		w = nFullScreenWidth - x;

	if(y < 0 || y >= nFullScreenHeight)
		y = 0;

	if(h < 1 || (y + h > nFullScreenHeight))
		h = nFullScreenHeight - y;

	if(pBmpFileNameIn)
		_tcscpy_s(szBmpFileNameOut, MAX_PATH, pBmpFileNameIn);
	else
	{
		SYSTEMTIME  SystemTime;
		GetLocalTime(&SystemTime);

		_stprintf_s(szBmpFileNameOut, MAX_PATH, 
            _T("CapScreen%04d%02d%02d_%02d%02d%02d.bmp"),
			SystemTime.wYear,
			SystemTime.wMonth,
			SystemTime.wDay,
			SystemTime.wHour,
			SystemTime.wMinute,
			SystemTime.wSecond);
	}
}

bool ScreenCaptureMgr::isScreenshotExists_(const tstring& fileName)
{
     if( bitmapList_.end() 
        == find(bitmapList_.begin(), bitmapList_.end(), fileName))
    {
        return false;
    }

    return true;
}
