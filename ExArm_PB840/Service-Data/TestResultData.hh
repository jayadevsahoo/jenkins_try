#ifndef TestResultData_HH
#define TestResultData_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1994, Puritan-Bennett Corporation
//====================================================================

//=====================================================================
//@ Class:  TestResultData -  Manage the service test data to be saved into
//                            or loaded from NovRam.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Data/vcssrc/TestResultData.hhv   25.0.4.0   19 Nov 2013 14:21:06   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah     Date:  15-Jan-99   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' to a non-inlined method.
//
//  Revision: 001  By:  yyy    Date:  16-May-95   DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//=====================================================================

#include "Sigma.hh"
#include "ServiceDataClassIds.hh"
#include "SmDataId.hh"

class TestResultData 
{
public:
	TestResultData(const TestResultData&);		
	virtual ~TestResultData(void);

	struct TestDataStruct
	{
		Real32	*pData;
		int		count;
	};

	// Following are two pure virtual methods which are defined in
	// some derived classes of this class
 	virtual void loadDataFromNovRam(void)=0;
	virtual void testCompleted(void)=0;

	// Initialize a test's related data stored in pTestInfo_. 
	void testStart(Uint testId);

	// Returns the Service data for a given test id and a specified index
	Real32 getTestResultData( 
				Uint testId,
				SmDataId::ItemizedTestDataId dataIndex);

	// Stores the given test datum into its private data structure.
	void setTestResultData(Real32 testData,
				Uint testId,
				SmDataId::ItemizedTestDataId dataIndex);

	// Return the maximum possible number of test data items for the
	// specific test id.
	inline int getTestDataCount(int testId);

	static void  SoftFault(const SoftFaultID softFaultID,
					const Uint32      lineNumber,
					const char*       pFileName  = NULL,
					const char*       pPredicate = NULL);

#if defined(SIGMA_DEVELOPMENT)
	friend class Ostream&  operator<<(Ostream& ostr, TestResultData&);
#endif // defined(SIGMA_DEVELOPMENT)
    
	TestResultData& operator=(const TestResultData&); 

protected:
	TestResultData(TestDataStruct *pTestInfo, Real32 *pArrEntries, Int32 maxEntries);

private:				  
	//@ Data-Member:  pTestInfo_
	// A pointer to the array of items which keep information about the test
	// data array.
	TestDataStruct* pTestInfo_;

	//@ Data-Member:  pTestDataArray_
	// A pointer to the derived class's array of test data entries.
	Real32* pTestDataArray_;

	//@ Constant:  MAX_ENTRIES_
	// The maximum number of entries allowed in this data log.
	Int32  MAX_ENTRIES_;
 
};


#include "TestResultData.in"


#endif // TestResultData_HH 
