#include "stdafx.h"
//=====================================================================
//   This is a proprietary work to which Puritan-Bennett corporation
//   of California claims exclusive right.  No part of this work may
//  be used, disclosed, reproduced, sorted in an information retrieval
//     system, or transmitted by any means, electronic, mechanical,
//   photocopying, recording, or otherwise without the prior written
//      permission of Puritan-Bennett Corporation of California.
//
//          Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: GuiOnline - GUI State: Online.
//---------------------------------------------------------------------
//@ Interface-Description
//  The GuiOnline class is a FsmState used by the GuiFsm finite state
//  machine.  The class represents the state that GUI Task Control
//  enters upon completing a GUI transition to the Online state.  It
//  contains methods for processing events while in this state. 
//---------------------------------------------------------------------
//@ Rationale
//  This class contains the methods for processing events while
//  in the GUI Online state.
//---------------------------------------------------------------------
//@ Implementation
//  GuiOnline is derived from the FsmState class.  It overrides the
//  virtual method receiveBdReady to provide state specific behavior
//  when it receives a BdReadyMessage.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Sys-Init/vcssrc/GuiOnline.ccv   25.0.4.0   19 Nov 2013 14:33:52   pvcs  $
//
//@ Modification-Log
//
//   Revision 002   By:   sah      Date: 01/13/99    DR Number: 5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to a non-inlined method.
//
//   Revision 001   By:   gdc      Date: 07/06/95    DR Number:   n/a  
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//=====================================================================

//=====================================================================
//
//      Includes...
//
//=====================================================================

#include "GuiOnline.hh"
#include "SigmaState.hh"
#include "TaskFlags.hh"

//@ Usage-Classes
#include "BdReadyMessage.hh"
#include "GuiFsm.hh"
#include "GuiOnlineInit.hh"
#include "GuiSst.hh"
#include "GuiSstInit.hh"
#include "Reset.hh"
#include "SettingContextHandle.hh"
#include "TaskAgentManager.hh"
#include "TaskControlAgent.hh"
//@ End-Usage

//=====================================================================
//
//      Static Data...
//
//=====================================================================

//@ Code...

//=====================================================================
//
//      Public Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GuiOnline [default constructor]
//
//@ Interface-Description
//  GuiOnline default constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

GuiOnline::GuiOnline()
{
    CALL_TRACE("GuiOnline()");
    // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~GuiOnline [destructor]
//
//@ Interface-Description
//  GuiOnline class destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

GuiOnline::~GuiOnline()
{
    CALL_TRACE("~GuiOnline()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
// Method: nameOf
//
// Interface-Description
//  Returns the name of the class.
//---------------------------------------------------------------------
// Implementation-Description
//  Returns a pointer to the const string containing the class name.
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

const char*
GuiOnline::nameOf(void) const
{
    CALL_TRACE("nameOf(void)");
    return "GuiOnline";
    // $[TI1]
}
 
 
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getSigmaState
//
//@ Interface-Description
//  The getSigmaState method returns the SigmaState associated with
//  this GuiOnline state.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns a SigmaState enumeration for the ONLINE state.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SigmaState
GuiOnline::getSigmaState(void) const
{
    CALL_TRACE("getSigmaState(void)");
    return STATE_ONLINE;
    // $[TI1]
}
 

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: receiveBdReady
//
//@ Interface-Description
//  The receiveBdReady method processes a BdReadyMessage received from
//  BD task control by GUI task control in the GuiOnline state.  It
//  first updates the local state of the BD from the message.  If the
//  BD reports ready in the INOP or ONLINE states with no settings
//  available but settings were previously set (eg. the BD experienced
//  an ACSWITCH type power failure) then this method resets the GUI to
//  resynchronize with the BD.  If the settings are OK then it forwards
//  the BdReadyMessage to the application tasks and starts a transition
//  to the ONLINE-INIT state.  If the BD reports ready in the SST
//  state, it starts a state transition to the SST-INIT state.  For any
//  other BD state, this method resets the GUI to resynchronize with the 
//  BD.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method invokes FsmState::SetBdState to set the local state of
//  the BD to the state contained in the BdReadyMessage.  
//  TaskAgentManager::Send forwards the BdReadyMessage to each 
//  managed task.  
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
void
GuiOnline::receiveBdReady(const BdReadyMessage& message) const
{
    CALL_TRACE("receiveBdReady(const BdReadyMessage&)");

    FsmState::SetBdState(message);
 
    switch (message.getState())
    {
        case STATE_INOP:
        case STATE_ONLINE: // $[TI1]
            //  if the BD is online without settings but the GUI has
            //  completed a settings transaction then we reboot the GUI
            //  to get resynchronized with the BD.  This can happen when
            //  the BD goes through an AC startup but the GUI didn't
            //  power down.
            if (   !message.areBdSettingsInitialized() 
                && SettingContextHandle::HasVentStartupCompleted() )
            {
                // $[TI1.1]
				Reset::Initiate(INTENTIONAL, Reset::GUIONLINE);
            }
            // $[TI1.2]
            TaskAgentManager::Send(message);
            GuiFsm::GetGuiOnlineInitState().startTransition();
            break;
        case STATE_SST:    // $[TI2] 
            TaskAgentManager::Send(message);
            GuiFsm::GetGuiSstInitState().startTransition();
            break;
        case STATE_SERVICE:  // $[TI3]
        case STATE_START:
        case STATE_TIMEOUT:
        default:
			Reset::Initiate(INTENTIONAL, Reset::GUIONLINE);
            break;
    }
}

 

#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//
//@ Interface-Description
//   This method is called when a software fault is detected by the
//   fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//   'lineNumber' are essential pieces of information.  The 'pFileName'
//   and 'pPredicate' strings may be defaulted in the macro to reduce
//   code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This method receives the call for the SoftFault, adds it sub-system
//   and class name ID and sends the message to the non-member function
//   SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
void
GuiOnline::SoftFault(const SoftFaultID  softFaultID,
                     const Uint32       lineNumber,
                     const char*        pFileName,
                     const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
 
  FaultHandler::SoftFault(softFaultID, SYS_INIT,
                          Sys_Init::ID_GUIONLINE,
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)

//=====================================================================
//
//      Protected Methods...
//
//=====================================================================

//=====================================================================
//
//      Private Methods...
//
//=====================================================================




