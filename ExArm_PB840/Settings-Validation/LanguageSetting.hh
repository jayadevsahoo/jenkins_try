#ifndef LanguageSetting_HH
#define LanguageSetting_HH

//====================================================================
//
// This is a proprietary work to which Covidien corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Covidien Corporation of California.
//
//            Copyright (c) 2012, Covidien Corporation
//====================================================================

//====================================================================
//@ Class:  LanguageSetting - Pressure Units Setting.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /data/840/Baseline/Settings-Validation/vcssrc/LanguageSetting.hhv   27.0   27 Aug 2013 10:42:18   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001   By: gdc    Date:  06-Aug-2012    SCR Number: 6778
//  Project: ZHO1
//  Description:
//	Initial version.
//  
//====================================================================

//@ Usage-Classes
#include "BatchDiscreteSetting.hh"
//@ End-Usage


class LanguageSetting : public BatchDiscreteSetting
{
  public:
    LanguageSetting(void);
    virtual ~LanguageSetting(void);

    virtual Applicability::Id  getApplicability(
			    const Notification::ChangeQualifier qualifierId
					       ) const;

    virtual Boolean isEnabledValue(const DiscreteValue value) const;

    virtual SettingValue  getNewPatientValue(void) const;

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL, 
			   const char*       pPredicate = NULL);

  private:
    LanguageSetting(const LanguageSetting&);	// not implemented...
    void  operator=(const LanguageSetting&);		// not implemented...
};


#endif // LanguageSetting_HH 
