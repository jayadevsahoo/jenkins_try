#ifndef TrendDbTable_HH
#define TrendDbTable_HH


//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
// Class: TrendDbTable
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Trend-Database/vcssrc/TrendDbTable.hhv   25.0.4.0   19 Nov 2013 14:34:40   pvcs  $
//
//@ Modification-Log
// 
//  Revision: 001  By:  mjf    Date:  18-Jun-2007    SCR Number: 6237
//       Project:  Trend
//       Description:
//             Initial version
// 
//=====================================================================
//@ Usage-Classes
//@ End-Usage

#include <string.h>
#include "TrendDataMgr.hh"
#include "TrendDbTableBuffer.hh"
#include "TrendDbTableVars.hh"
#include "TrendDbRecord.hh"
#include "CompactFlash.hh"


// forward declare
class TimeStamp;
class TrendValue;


class TrendDbTable
{
public:

	TrendDbTable( TrendDataMgr::TableId tableId, 
				  TrendDbRecord & tableRecord,
				  CompactFlash & tableInterface );
	~TrendDbTable();

	void config( Uint32 tableStartBlock, Uint32 tableCapacity );

	void clear( void );

	inline Uint32 getStartLba( void ) const;
	inline Uint32 getCapacityLba( void ) const;
	inline Uint32 getTableCapacity( void ) const;
	inline Int16  getRecordSize( void ) const;
	inline Uint32 getRecordCount( void ) const;
	inline Uint32 getRecordPosition( void ) const;


	// Methods used to append data to the table
	SigmaStatus setNextRecord( Uint32 seqNumber, const TimeStamp & timeStamp );
	void appendData( Int8 slotNumber, const TrendValue & newValuse );
	void closeRecord( void );



	SigmaStatus loadRecordSet( TrendDbTableBuffer & recordSet,
						   Uint32 position, Int16 dataSize );


	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

private:

	// no copy or assignment constructor
	TrendDbTable( const TrendDbTable& );		// not implemented
	void operator=( const     TrendDbTable& );	// not implemented


	SigmaStatus flushWriteBuffer_( void );

	//@ Data-Member: tableId_
	//  Unique identify for this table
	TrendDataMgr::TableId   tableId_;

	//@ Data-Member: tableRecord_
	//  Used to parse record data
	TrendDbRecord & tableRecord_;

	//@ Data-Member: tableInterface_
	//  The interface to the compact flash used for read/write
	CompactFlash & tableInterface_;

	//@ Data-Member: writeMemory_;
	//  A compact flash block used to hold data being written
	//  Is the RAM memory for the writeBuffer_
	CompactFlash::Block writeMemory_[ TrendDataMgr::TREND_WRITE_BUFFER_SIZE ];

	//@ Data-Member: writeBuffer_;
	//  A buffer used to hold a block of data to queue the records until a 
	//  block is full and then can be flushed to the compact flash
	TrendDbTableBuffer writeBuffer_;

	//@ Data-Member: tableVars_
	//  Holds the table management variables
	TrendDbTableVars tableVars_;

	//@ Data-Member: tableRecordCapacity_
	//  The capacity of the table in rows
	Uint32  tableRecordCapacity_;
};

// Inlined methods
#include "TrendDbTable.in"

#endif // TrendDbTableBuffer_HH
