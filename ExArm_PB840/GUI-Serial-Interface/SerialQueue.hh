#ifndef SerialQueue_HH
#define SerialQueue_HH

//====================================================================
// This is a proprietary work to which the Covidien corporation claims 
// exclusive right.  No part of this work may be used, disclosed, 
// reproduced, sorted in an information retrieval system, or 
// transmitted by any means, electronic, mechanical, photocopying, 
// recording, or otherwise without the prior written permission of 
// Covidien Corporation.
//
//              Copyright (c) 2008, Covidien Corporation
//=====================================================================

//=====================================================================
// Class:  SerialQueue
//---------------------------------------------------------------------
//@ Version-Information
//
//@ Modification-Log
//
//  Revision: 001   By: gdc    Date: 16-Aug-2010     SCR Number: 6663
//  Project: API/MAT
//  Description:
//  Initial version to support for VentSet API.
//=====================================================================


class SerialQueue
{
public :
	SerialQueue( void );
	~SerialQueue();
	int insert( unsigned char c );
	int remove( void );
	int peek( unsigned char *buffer_, int num ) const;
	int inUseCount( void ) const;
	int freeCount( void ) const;
	void clear( void );

	enum QueueSizes
	{
		QUEUE_SIZE = 1024
		//HIGH_WATER_MARK = ( QUEUE_SIZE * 3 ) / 4;
		//LOW_WATER_MARK = QUEUE_SIZE / 4;
	};

private :
	volatile unsigned int headIndex_;
	volatile unsigned int tailIndex_;
	volatile unsigned char buffer_[ QUEUE_SIZE ];
};

#include "SerialQueue.in"

#endif  // ifndef SerialQueue_HH
