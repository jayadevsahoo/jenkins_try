#ifndef BtatEventState_HH
#define BtatEventState_HH
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================
  
//============================ H E A D E R   D E S C R I P T I O N ====
//  Class: BtatEventState - storage and maintenance utilities for
//                          active BTAT alarms.
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header::   /840/Baseline/Safety-Net/vcssrc/BtatEventState.hhv   10.7   08/17/07 10:33:28   pvcs  
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date:  15-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001    By: by   Date: 12-Dec-1995  DR Number: None
//
//    Project: Sigma (840)
//    Description:
//       Initial version.
//
//=====================================================================

#include "Sigma.hh"
#include "Background.hh"
#include "OperatingParamEventName.hh"

// Number of active alarms maintained in NOVRAM
static const Uint8 MAX_BTAT_EVENTS = 17;

class BtatEventState
{
    public:

        BtatEventState( void );  // Default constructor
        BtatEventState( const BtatEventState& rBtatEvents ); // Copy Constructor
        ~BtatEventState( void ); // Destructor

        void operator= ( const BtatEventState& rBtatEvents );

        Int16 findEntry( const OperatingParameterEventName entry ) const;

        inline OperatingParameterEventName
        getBtatState( const Uint8 tableNdx ) const;

        inline void
        setBtatState( const Uint8 tableNdx,
                      const OperatingParameterEventName newStateValue );

        inline void resetActiveBtatEvents( void );

        static void SoftFault( const SoftFaultID softFaultID,
			       const Uint32      lineNumber,
			       const char*       pFileName  = NULL, 
			       const char*       pPredicate = NULL );

    protected:
  
    private:

        //@ Data-Member:  arrBtatEvents_
        //  Current state of each of the BTAT alarms.
        OperatingParameterEventName arrBtatEvents_[ MAX_BTAT_EVENTS ];    

#if defined( SIGMA_DEBUG )
   public:
       void printBtat( void );
#endif // defined( SIGMA_DEBUG )

};

#include "BtatEventState.in"

#endif // BtatEventState_HH

