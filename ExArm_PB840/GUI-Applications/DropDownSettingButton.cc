#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-2007, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: DropDownSettingButton - A special type of DiscreteSettingButton
// that displays a drop-down menu, containing each of the allowable discrete
// values.
//---------------------------------------------------------------------
//@ Interface-Description
// This class is a specialization of the DiscreteSettingButton, whereby
// instances of this class display a drop-down menu, with each of the
// allowable discrete values, when pressed.  The drop-down menu also
// distinguishes the setting's current value, from the other allowable
// values.
//---------------------------------------------------------------------
//@ Rationale
// Contains all the functionality needed to allow the operator to
// view and adjust a discrete setting in a single class.
//---------------------------------------------------------------------
//@ Implementation-Description
// Most of the interaction with the Settings-Validation subsystem is handled in
// the parent SettingButton class.  All this class must do is build the
// contents of the drop-down menu, based on the setting's 'isEnabledValue()'
// method.
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and pre-conditions
// to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/DropDownSettingButton.ccv   25.0.4.0   19 Nov 2013 14:07:42   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By:  gdc    Date:  07-APR-2008    SCR Number:  6407
//  Project:  TREND2
//  Description:
//	Removed redefinition of countof macro to resolve compiler warning.
//
//  Revision: 002  By:  gdc	   Date:  09-Apr-2007    SCR Number:  6237
//  Project:  Trend
//  Description:
//      Added configurable drop down text size. Added automated vertical
//      text positioning of drop-down menu text.
//
//  Revision: 001  By: sah     Date:  23-May-200    DR Number: 5755
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      *  added dropdown buttons
//
//=====================================================================

#include "DropDownSettingButton.hh"

//@ Usage-Classes
#include "SettingSubject.hh"
//@ End-Usage

//@ Code...

static const Int32  DEFAULT_TEXT_SIZE_ = 10;


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: DropDownSettingButton()  [Constructor]
//
//@ Interface-Description
// Constructs an instance of this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

DropDownSettingButton::DropDownSettingButton(Uint16 xOrg, Uint16 yOrg,
											 Uint16 width, Uint16 height, 
											 Button::ButtonType buttonType,
											 Uint8 bevelSize, Button::CornerType cornerType,
											 Button::BorderType borderType,
											 StringId titleText, SettingId::SettingIdType settingId,
											 SubScreen *pFocusSubScreen, StringId messageId,
											 Uint valuePointSize, Boolean isMainSetting) 
:   DiscreteSettingButton(  xOrg, yOrg, width, height, buttonType,
							bevelSize, cornerType, borderType, titleText,
							settingId, pFocusSubScreen, messageId, valuePointSize,
							isMainSetting),
	dropDownTextSize_(      DEFAULT_TEXT_SIZE_),
	highlightedValue_(      0),
	isFirstUpdate_(         TRUE)
{
	CALL_TRACE("DropDownSettingButton()");

	dropDownBox_.setX(xOrg + bevelSize);
	dropDownBox_.setY((yOrg + height));
	dropDownBox_.setWidth(width - (2 * bevelSize));
	dropDownBox_.setFillColor(Colors::BLACK);
	dropDownBox_.setShow(TRUE);

	for (Uint idx = 0u; idx < MAX_DROPDOWN_VALUES; idx++)
	{											 // $[TI1]
												 // $[VC01000] -- display drop-down menu just below setting button...
		arrValueArea_[idx].setX(1);
		arrValueArea_[idx].setWidth(dropDownBox_.getWidth()-2);
		arrValueArea_[idx].setShow(TRUE);

		dropDownBox_.addDrawable(arrValueArea_ + idx);

		arrValueFields_[idx].setY(0);
		arrValueFields_[idx].setShow(TRUE);

		arrValueArea_[idx].addDrawable(arrValueFields_ + idx);
	}

	setDropDownTextSize(dropDownTextSize_);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~DropDownSettingButton  [Destructor]
//
//@ Interface-Description
// Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

DropDownSettingButton::~DropDownSettingButton(void)
{
	CALL_TRACE("DropDownSettingButton::~DropDownSettingButton(void)");

	// Do nothing
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setDropDownTextSize
//
//@ Interface-Description
// Sets the point size of the drop down text to the specified size and
// resets the size of the drop down area.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void DropDownSettingButton::setDropDownTextSize(const Int32 dropDownTextSize)
{
	CALL_TRACE("setDropDownTextSize");

	static const Int CONTAINER_MARGIN_ = 2;

	dropDownTextSize_ = dropDownTextSize;

	fieldHeight_ = TextFont::GetMaxAscent(TextFont::Size(dropDownTextSize_),TextFont::NORMAL) +
				   TextFont::GetDescent(TextFont::Size(dropDownTextSize_),TextFont::NORMAL) + 
				   CONTAINER_MARGIN_;

	for (Uint idx = 0u; idx < MAX_DROPDOWN_VALUES; idx++)
	{
		arrValueArea_[idx].setY(idx * fieldHeight_);
		arrValueArea_[idx].setHeight(fieldHeight_);
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: applicabilityUpdate
//
//@ Interface-Description
//  Called when applicability of the setting ids have changed.
// >Von
//  qualifierId	The context in which the change happened, either
//					ACCEPTED or ADJUSTED.
//  pSubject	The pointer to the Setting's Context subject.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void DropDownSettingButton::applicabilityUpdate( const Notification::ChangeQualifier qualifierId,
												 const SettingSubject*               pSubject )
{
	CALL_TRACE("applicabilityUpdate(qualifierId, pSubject)");

	const Applicability::Id  NEW_APPLICABILITY =
		pSubject->getApplicability(qualifierId);

	switch (NEW_APPLICABILITY)
	{
	case Applicability::CHANGEABLE :
	case Applicability::INAPPLICABLE :			 // $[TI1]
		// no change from what base class does...
		SettingButton::applicabilityUpdate(qualifierId, pSubject);
		break;

	case Applicability::VIEWABLE :				 // $[TI2]
		// unlike other setting buttons, we want to see this one when
		// "viewable"...
		setToFlat();
		setShow(TRUE);
		isFirstUpdate_ = TRUE;
		updateDisplay_(qualifierId, pSubject);
		break;

	default :
		// unexpected applicability id...
		AUX_CLASS_ASSERTION_FAILURE(NEW_APPLICABILITY);
		break;
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateDisplay_ [Protected, virtual]
//
//@ Interface-Description
// Called when the display of this setting button needs to be updated with
// the latest setting value.  Passed a setting context id informing us the
// setting context from which we should retrieve the new setting value.
//---------------------------------------------------------------------
//@ Implementation-Description
// $[VC01000] -- the current value shall be distinguishable from all other
//               values...
//---------------------------------------------------------------------
//@ PreCondition
//	The number of menu items can't exceed our array sizes
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void DropDownSettingButton::updateDisplay_(Notification::ChangeQualifier qualifierId,
										   const SettingSubject*         pSubject)
{
	CALL_TRACE("updateDisplay_(qualifierId, pSubject)");

	// verify the first time we build this menu that all the entries will fit
	AUX_CLASS_PRE_CONDITION(pValueInfo_->numValues <= countof(arrValueArea_), pValueInfo_->numValues);
	AUX_CLASS_PRE_CONDITION(pValueInfo_->numValues <= countof(arrValueFields_), pValueInfo_->numValues);
	AUX_CLASS_PRE_CONDITION(pValueInfo_->numValues <= countof(arrValueState_), pValueInfo_->numValues);

	Boolean isMenuChanged = isFirstUpdate_;

	for (Uint ix = 0; ix < pValueInfo_->numValues; ix++)
	{
		Boolean newState = pSubject->isEnabledValue(ix);
		isMenuChanged = isMenuChanged || (arrValueState_[ix] != newState);
		arrValueState_[ix] = newState;
	}

	const DiscreteValue  CURR_VALUE = pSubject->getAdjustedValue();

	if (isMenuChanged)
	{
		isFirstUpdate_ = FALSE;

		// Hide all the drop down box areas until they're made visible during
		// the update below. We do this because the size of the drop-down menu
		// changes with the number of menu entries
		for (Uint ix = 0; ix < countof(arrValueArea_); ix++)
		{
			arrValueArea_[ix].setShow(FALSE); 
		}

		Uint  dropDownIdx = 0u;

		// update drop-down box...
		for (Uint valueIdx = 0; valueIdx < pValueInfo_->numValues; valueIdx++)
		{										 // $[TI1.1]
			if (pSubject->isEnabledValue(valueIdx))
			{									 // $[TI1.1.1]
				arrValueArea_[dropDownIdx].setShow(TRUE);

				const Boolean  IS_CURR_VALUE = (valueIdx == CURR_VALUE);

				if (IS_CURR_VALUE)
				{								 // $[TI1.1.1.1]
					arrValueArea_[dropDownIdx].setColor(Colors::BLACK);
					arrValueArea_[dropDownIdx].setFillColor(Colors::BLACK);
					arrValueFields_[dropDownIdx].setColor(Colors::WHITE);
				}
				else
				{								 // $[TI1.1.1.2]
					arrValueArea_[dropDownIdx].setColor(Colors::GOLDENROD);
					arrValueArea_[dropDownIdx].setFillColor(Colors::GOLDENROD);
					arrValueFields_[dropDownIdx].setColor(Colors::BLACK);
				}

				// set the text field up with this value's string...
				setValueString_(pValueInfo_->arrValues[valueIdx],
								arrValueFields_[dropDownIdx]);

				dropDownIdx++;
			}									 // $[TI1.1.2] -- 'valueIdx' value is NOT enabled...
		}										 // end of for loop...
		highlightedValue_ = CURR_VALUE;

		// now that the total number of enabled values have been determined,
		// use that number to calculate the overall drop-down area's height...
		dropDownBox_.setHeight((fieldHeight_ * dropDownIdx) + 1);
	}
	else if (CURR_VALUE != highlightedValue_)
	{
		Uint  dropDownIdx = 0;

		// only update the newly selected row and the old selected row
		for (Uint valueIdx = 0; valueIdx < pValueInfo_->numValues; valueIdx++)
		{
			if (pSubject->isEnabledValue(valueIdx))
			{
				if (valueIdx == CURR_VALUE)
				{
					// highlight the new highlighted row 
					arrValueArea_[dropDownIdx].setColor(Colors::BLACK);
					arrValueArea_[dropDownIdx].setFillColor(Colors::BLACK);
					arrValueFields_[dropDownIdx].setColor(Colors::WHITE);
				}
				else if (valueIdx == highlightedValue_)
				{
					// unhighlight the old highlighted row
					arrValueArea_[dropDownIdx].setColor(Colors::GOLDENROD);
					arrValueArea_[dropDownIdx].setFillColor(Colors::GOLDENROD);
					arrValueFields_[dropDownIdx].setColor(Colors::BLACK);
				}
				dropDownIdx++;
			}
		}
		highlightedValue_ = CURR_VALUE;
	}

	// forward to base class to update the "value" text in the button itself
	DiscreteSettingButton::updateDisplay_(qualifierId, pSubject);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: downHappened_
//
//@ Interface-Description
// This is a virtual method inherited from being a Button.  It is called when
// this button is sent into the Down state, either by the operator pressing us
// down (byOperatorAction = TRUE) or by the application calling the method
// setToDown() (byOperatorAction = FALSE).
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void DropDownSettingButton::downHappened_(Boolean byOperatorAction)
{
	CALL_TRACE("SettingButton::downHappened_(Boolean byOperatorAction)");

	// add drop-down box to subscreen...
	getParentContainer()->addDrawable(&dropDownBox_);

	if (SettingId::IsNonBatchId(SUBJECT_ID_))
	{											 // $[TI1]
		// force an update of the drop-down box, to ensure box contains
		// up-to-date information...

		isFirstUpdate_ = TRUE;
		updateDisplay_(Notification::ACCEPTED, getSubjectPtr_(SUBJECT_ID_));
	}											 // $[TI2]

	SettingButton::downHappened_(byOperatorAction);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: upHappened_
//
//@ Interface-Description
// This is a virtual method inherited from being a Button.  It is called when
// this button is sent into the Up state, either by the operator pressing us
// down (byOperatorAction = TRUE) or by the application calling the method
// setToUp() (byOperatorAction = FALSE).
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void DropDownSettingButton::upHappened_(Boolean byOperatorAction)
{
	CALL_TRACE("SettingButton::upHappened_(Boolean byOperatorAction)");

	// remove drop-down box from subscreen...
	getParentContainer()->removeDrawable(&dropDownBox_);

	isFirstUpdate_ = TRUE;
	SettingButton::upHappened_(byOperatorAction);
}												 // $[TI1]



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void DropDownSettingButton::SoftFault(const SoftFaultID  softFaultID,
									  const Uint32       lineNumber,
									  const char*        pFileName,
									  const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, DROPDOWNSETTINGBUTTON,
							lineNumber, pFileName, pPredicate);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setValueString_
//
//@ Interface-Description
//  Update 'rTextField' with a formatted messaged created with 'formatString',
//  which contains the current value, and 'isCurrValue', which indicates
//  whether this value is the currently-selected value, or not.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
 
void DropDownSettingButton::setValueString_(StringId       formatString,
											TextField&     rTextField) const
{
	CALL_TRACE("setValueString_(formatString, isCurrValue, rTextField)");

	wchar_t  tmpString[TextField::MAX_STRING_LENGTH+1];

	tmpString[0] = L'\0';

	if (formatString != ::NULL_STRING_ID)
	{
		const wchar_t  FONT_STYLE = L'N';

		rTextField.setY(0);

		// size the text field based on the maximum ascent of the "normal" style
		Int yOffset = TextFont::GetMaxAscent(TextFont::Size(dropDownTextSize_), TextFont::NORMAL);

		swprintf(tmpString, formatString,
				dropDownTextSize_,				 // p=%d
				yOffset,						 // y=%d
				FONT_STYLE);					 // %c

	}											 // $[TI2]

	rTextField.setText(tmpString);
	rTextField.positionInContainer(::GRAVITY_CENTER);
	rTextField.positionInContainer(::GRAVITY_VERTICAL_CENTER);
}
