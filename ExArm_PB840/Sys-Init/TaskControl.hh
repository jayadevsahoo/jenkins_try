#ifndef TaskControl_HH
#define TaskControl_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//====================================================================
 
//====================================================================
// Class:  TaskControl - Sys-Init subsystem class
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Sys-Init/vcssrc/TaskControl.hhv   25.0.4.0   19 Nov 2013 14:33:56   pvcs  $
//
//@ Modification-Log
//
//   Revision 003   By:   sah      Date: 01/13/99    DR Number: 5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to a non-inlined method.
//
//   Revision 002   By:   gdc      Date: 11-JUN-1997 DR Number: 1902
//      Project:   Sigma   (R8027)
//      Description:
//         Modified for processing TaskReadyMessages and TransitionTimers
//         in a different context than other message so one transition
//         completes before starting another.
//
//   Revision 001   By:   gdc      Date: 05/16/95    DR Number:   n/a
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//====================================================================
 
//@ Usage-Classes
#include "Sys_Init.hh"
#include "NetworkApp.hh"
//@ End-Usage

class AppContext;
class Fsm;
class TaskControlMessage;
class CommDownMessage;
class CommUpMessage;
class ChangeStateMessage;
class BdReadyMessage;
class GuiReadyMessage;
class SyncClockMessage;
class StartupTimer;
class SyncClockTimer;
class TransitionTimer;
class BdFailedMessage;

class TaskControl
{
  public:

    static void  Initialize(void);
    static void  MainLoop(void);
    static void  SendEvent(Int32 queueId, TaskControlMessage* pMessage);
    static AppContext& GetAppContext(void);
    static AppContext& GetNormalContext(void);
    static AppContext& GetTransitionContext(void);

    static void  CommDownCallback(const CommDownMessage& rMessage);
    static void  CommUpCallback(const CommUpMessage& rMessage);
    static void  ChangeStateCallback(const ChangeStateMessage& rMessage);
    static void  BdReadyCallback(const BdReadyMessage& rMessage);
    static void  GuiReadyCallback(const GuiReadyMessage& rMessage);
    static void  SyncClockCallback(const SyncClockMessage& rMessage);
    static void  StartupTimerCallback(const StartupTimer& rMessage);
    static void  SyncClockTimerCallback(const SyncClockTimer& rMessage);
    static void  TransitionTimerCallback(const TransitionTimer& rMessage);
    static void  BdFailedCallback(const BdFailedMessage& rMessage);

    static void  NetworkMessageCallback( XmitDataMsgId msgId,
                                         void * pData, 
                                         size_t nbytes);

    static void     SoftFault(const SoftFaultID softFaultID,
			      const Uint32      lineNumber,
			      const char*       pFileName  = NULL,
			      const char*       pPredicate = NULL);

  private:
    TaskControl();                             // declared only
    ~TaskControl();                            // declared only

    TaskControl(const TaskControl&);           // declared only
    void operator=(const TaskControl&);        // declared only
};


#endif // TaskControl_HH
