#include "stdafx.h"
//=====================================================================
//   This is a proprietary work to which Puritan-Bennett corporation
//   of California claims exclusive right.  No part of this work may
//  be used, disclosed, reproduced, sorted in an information retrieval
//     system, or transmitted by any means, electronic, mechanical,
//   photocopying, recording, or otherwise without the prior written
//      permission of Puritan-Bennett Corporation of California.
//
//          Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: GuiServiceInit - GUI State: ServiceInit.
//---------------------------------------------------------------------
//@ Interface-Description
//  The GuiServiceInit class is a FsmState used by the GuiFsm finite state
//  machine.  The class represents the state that GUI Task Control
//  enters upon completing a GUI transition to the ServiceInit state.  It
//  contains methods for processing events while in this state. 
//---------------------------------------------------------------------
//@ Rationale
//  This class contains the methods for processing events while
//  in the GUI ServiceInit state.
//---------------------------------------------------------------------
//@ Implementation-Description
//  GuiServiceInit is derived from the FsmState class.  It overrides the
//  virtual method activate() to provide state specific behavior
//  when activated.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Sys-Init/vcssrc/GuiServiceInit.ccv   25.0.4.0   19 Nov 2013 14:33:54   pvcs  $
//
//@ Modification-Log
//
//   Revision 003   By:   sah      Date: 01/13/99    DR Number: 5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to a non-inlined method.
//
//   Revision 002   By:   gdc      Date: 07/06/95    DR Number:  1902
//      Project:   Sigma   (R8027)
//      Description:
//         Removed receiveBdReady() as "dead" code since the 
//         GuiServiceInit() state is a transitory state only.
//
//   Revision 001   By:   gdc      Date: 07/06/95    DR Number:   n/a  
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//=====================================================================

//=====================================================================
//
//      Includes...
//
//=====================================================================

#include "GuiServiceInit.hh"
#include "SigmaState.hh"
#include "TaskFlags.hh"

//@ Usage-Classes
#include "GuiFsm.hh"
#include "GuiService.hh"
//TODO E600 port #include "Reset.hh"
#include "TaskControlAgent.hh"
#include "TaskAgentManager.hh"
//@ End-Usage

//=====================================================================
//
//      Static Data...
//
//=====================================================================

//@ Code...

//=====================================================================
//
//      Public Methods...
//
//=====================================================================
 

#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//
//@ Interface-Description
//   This method is called when a software fault is detected by the
//   fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//   'lineNumber' are essential pieces of information.  The 'pFileName'
//   and 'pPredicate' strings may be defaulted in the macro to reduce
//   code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This method receives the call for the SoftFault, adds it sub-system
//   and class name ID and sends the message to the non-member function
//   SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
void
GuiServiceInit::SoftFault(const SoftFaultID  softFaultID,
                     const Uint32       lineNumber,
                     const char*        pFileName,
                     const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
 
  FaultHandler::SoftFault(softFaultID, SYS_INIT,
                          Sys_Init::ID_GUISERVICEINIT,
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)

//=====================================================================
//
//      Protected Methods...
//
//=====================================================================

//=====================================================================
//
//      Private Methods...
//
//=====================================================================





//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GuiServiceInit [default constructor]
//
//@ Interface-Description
//  GuiServiceInit class default constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

GuiServiceInit::GuiServiceInit()
{
    CALL_TRACE("GuiServiceInit()");
    // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~GuiServiceInit [destructor]
//
//@ Interface-Description
//  GuiServiceInit class destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

GuiServiceInit::~GuiServiceInit()
{
    CALL_TRACE("~GuiServiceInit()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
// Method: nameOf
//
// Interface-Description
//  Returns the name of the class.
//---------------------------------------------------------------------
// Implementation-Description
//  Returns a pointer to the const string containing the class name.
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

const char*
GuiServiceInit::nameOf(void) const
{
    CALL_TRACE("nameOf(void)");
    return "GuiServiceInit";
    //  $[TI1]
}
 

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getSigmaState
//
//@ Interface-Description
//  The getSigmaState method returns the SigmaState associated with
//  this GuiServiceInit state.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns a SigmaState enumeration for the SERVICE state.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SigmaState
GuiServiceInit::getSigmaState(void) const
{
    CALL_TRACE("getSigmaState(void)");
    return STATE_INIT;
    // $[TI1]
}
 

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activate
//
//@ Interface-Description
//  This virtual function is called upon entry into the SERVICE-INIT
//  state.  It immediately begins a transition to the SERVICE state.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses the virtual method FsmState::startTransition() to initiate the
//  transition to the SERVICE state.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
GuiServiceInit::activate(void) const
{
    CALL_TRACE("activate(void)");
    GuiFsm::GetGuiServiceState().startTransition();
    // $[TI1]
}
