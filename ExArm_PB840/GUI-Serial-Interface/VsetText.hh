#ifndef VsetText_HH
#define VsetText_HH

//====================================================================
// This is a proprietary work to which Covidien corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2010, Covidien, Inc.
//====================================================================
//====================================================================
// Class: VsetText - Manages text strings for VSET API
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Serial-Interface/vcssrc/VsetText.hhv   25.0.4.0   19 Nov 2013 14:12:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001   By: gdc    Date: 16-Aug-2010     SCR Number: 6663
//  Project: API/MAT
//  Description:
//  Initial implementation to support VentSet API.
//====================================================================

#include "Sigma.hh"
#include "GUI_Serial_Interface.hh"

class VsetText
{
  public:

	// Constant: MAX_STRING_LENGTH
	// This is the longest string that you can pass in to the VsetText
	enum {MAX_STRING_LENGTH = 200 };

	VsetText(const char *pString);
	VsetText(void);

	virtual ~VsetText(void);

	void setSimpleText(const char *pString);
	const char* getSerialText(void) const {return serialText_;}
    operator const char*(void) const {return serialText_;}

	static void SoftFault(const SoftFaultID softFaultID,
						 const Uint        lineNumber,
						 const char*       pFileName  = NULL, 
						 const char*       pPredicate = NULL);

  protected:

  private:

	// these methods are purposely declared, but not implemented...
	VsetText(const VsetText&);                   // not implemented...
	void operator=(const VsetText&);              // not implemented...

	void convert_(void);
	void renderSerialText_(Boolean isTextSubscripted);
	Int getValue_(void);
	void dumpTextString_(void) const;

	//@ Data-Member: simpleText_
	// Points to the "simple text" string to be converted to serial text
	const char* simpleText_;

	//@ Data-Member: serialText_
	// Contains the "serial text" string converted from simple text
	// this text string is what VsetServer outputs to the serial port
	char serialText_[MAX_STRING_LENGTH+1]; 

	//@ Data-Member: serialTextIndex_
	// Index of current character in simpleText_ used in conversion process
	Int simpleTextIndex_;

	//@ Data-Member: serialTextIndex_
	// Index of current character in serialText_ used in conversion process
	Int serialTextIndex_;
};

#endif // VsetText_HH 
