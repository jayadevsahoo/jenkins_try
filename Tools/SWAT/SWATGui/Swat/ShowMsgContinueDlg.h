#pragma once


// CShowMsgContinueDlg dialog

class CShowMsgContinueDlg : public CDialog
{
	DECLARE_DYNAMIC(CShowMsgContinueDlg)

public:
	CShowMsgContinueDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CShowMsgContinueDlg();

// Dialog Data
	enum { IDD = IDD_SHOW_CONT_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	void ShowString(CString strMessage);
protected:
	virtual void PostNcDestroy();
};
