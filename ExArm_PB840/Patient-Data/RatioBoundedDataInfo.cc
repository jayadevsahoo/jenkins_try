#include "stdafx.h"
#if		defined(SIGMA_GUI_CPU) || defined(SIGMA_COMMON)
//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, NellCor/Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: RatioBoundedDataInfo - Patient data processing agent class
// 	    which handles patient data that is a ratio value. 
//---------------------------------------------------------------------
//@ Interface-Description:
//>Von
//  The RatioBoundedDataInfo class provides exactly same interface
//  as its parent class, the BoundedDataInfo class except .
//  A RatioBoundedDataInfo class object is instantiated to 
//  overrides modifyData_ method to provide three precision ranges and
//  support a ratio data in a negative inverse ratio representation.
//
//  Class Key Methods:
//  ---------------------------------
//  modifyData_  - processes a raw ratio value received from the Breath
//                 Delivery system to a negative inverse ratio format.
//---------------------------------------------------------------------
//@ Rationale
//   The purpose of this class is to encapsulate all the data and 
//   behaviors that are necessary for providing three precison ranges
//   and a negative reverse ratio format Bounded patient data.
//---------------------------------------------------------------------
//@ Implementation-Description
//  It inherits most of the BoundedDataInfo class properties.
//>Voff
//---------------------------------------------------------------------
//@ Fault-Handling
//    SoftFault is generated if  certain pre-conditions or assertions
//    are not met.
//---------------------------------------------------------------------
//@ Restrictions
//    None.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Patient-Data/vcssrc/RatioBoundedDataInfo.ccv   25.0.4.0   19 Nov 2013 14:18:00   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By: sah   Date:  13-Jan-99    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 003  By:  hhd	Date:  09-JUL-1998  DR Number: XXXX
//  Project:  Color
//  Description:
//		Added Testable Items.
//
//  Revision: 002  By:  gdc    Date:  26-MAR-1996  DR Number: 5059
//  Project:  Color
//  Description:
//  	Fixed modifyData_() to properly set display precision of data
//		when either N:1 or 1:N data is received. Added testable items.
//
//  Revision: 001  By:  jhv    Date:  20-May-96    DR Number: 
//  Project:  Sigma (R8027)
//  Description:
//		Initial version (Integration baseline).
//
//=====================================================================

#include "RatioBoundedDataInfo.hh"
#include "PatientDataMgr.hh"
#include "MathUtilities.hh"

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: RatioBoundedDataInfo(PatientDataId::PatientItemId id,
//	const Real32 min,const Real32 max, const Real32 lowerPoint,
//  const Real32 upperPoint, const Precision lowerP, const Precision midP, 
//  const Precision higherP ) [constructor]
//---------------------------------------------------------------------
//@ Interface-Description
//     Initializes member variables to initial values.
//---------------------------------------------------------------------
//@ Implementation-Description
//   Most of the inherited members are initialized by the parent
//   class, BoundedDataInfo, constructor.
//   A precision for a data value that is less than the 
//   UPPER_CHANGE_POINT_ and more than  the MID_CHANGE_POINT_
//   is added as MID_PRECISION_.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

RatioBoundedDataInfo::RatioBoundedDataInfo( const PatientDataId::PatientItemId id,
    const Real32 min, const Real32 max, const Real32 lowerPoint,
    const Real32 upperPoint, const Precision lowerP, const Precision midP, 
    const Precision higherP ):
    BoundedDataInfo(id, min, max, lowerPoint, lowerP, higherP),
		UPPER_CHANGE_POINT_( upperPoint), MID_PRECISION_(midP)
{
			// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~RatioBoundedDataInfo() [ destructor ]
//@ Interface-Description
//   Does nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

RatioBoundedDataInfo:: ~RatioBoundedDataInfo()
{
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: modifyData_(void *pValue) [ virtual ]
//
//@ Interface-Description
//    Processes a given ratio value to an acceptable ratio format.
//    The method determines an appripriate precision for the data.
//    A data value is clipped to upper and lower boundry values if
//    necessary.
//    A processed data value and a chosen precision is saved in
//    this object's data buffer. A ratio is always expressed as 1
//    to a number larger than one. When a raw value is less than 1,
//    the inverse value of a given ratio value is represented as a
//    negative number.
//---------------------------------------------------------------------
//@ Implementation-Description
//    There are three different precision ranges for a ratio data.
//    The BreathItemDataInfo::modifyData_ updates the double
//    buffer index and calls callback routines for this patient
//    data item change notice.
//    $[03001], $[03043], $[03044]
//---------------------------------------------------------------------
//@ PreCondition
//    ( rawValue >= 0 )
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
RatioBoundedDataInfo::modifyData_(void *pValue)
{
	Real32 rawValue= *(Real32 *)pValue;
	Real32 correctedValue = rawValue;
	Int32  sign = 1;

  	CLASS_PRE_CONDITION(rawValue >= 0.0 );
	
	data_[index_.network].bValue.clippingState = BoundedValue::UNCLIPPED;
	data_[index_.network].bValue.timedOut = FALSE ;

  	if ( rawValue < MIN_VALUE_  )
	{												// $[TI2]
  		rawValue  = MIN_VALUE_;   
		data_[index_.network].bValue.clippingState = BoundedValue::ABSOLUTE_CLIP;
	} 												// $[TI3]

	if ( rawValue > MAX_VALUE_ )
	{ 												// $[TI5]
		data_[index_.network].bValue.clippingState = BoundedValue::ABSOLUTE_CLIP;
    	rawValue = MAX_VALUE_;   
	} 												// $[TI6]

	if ( rawValue < 1.0 )
	{												// $[TI7]

		correctedValue = 1.0 / rawValue;
		sign = -1;
	}
	else
	{												// $[TI8]
		correctedValue = rawValue;
		sign = 1;
	}

	Precision precision = LOWER_PRECISION_;
	correctedValue = ::PreciseValue(correctedValue, precision);
	
	if ( correctedValue >= MID_CHANGE_POINT_
		|| ::IsEquivalent( correctedValue, MID_CHANGE_POINT_, precision))
	{												// $[TI9]
		precision = MID_PRECISION_;
		correctedValue = ::PreciseValue(correctedValue, precision);

		if ( correctedValue >= UPPER_CHANGE_POINT_
			|| ::IsEquivalent( correctedValue, UPPER_CHANGE_POINT_, precision))
		{											// $[TI10]
			precision = UPPER_PRECISION_;
			correctedValue = ::PreciseValue(correctedValue, precision);
		}											// $[TI11]

	}												// $[TI12]

   	data_[index_.network].bValue.data.precision = precision;
   	data_[index_.network].bValue.data.value = sign * correctedValue;   

	BreathItemDataInfo::modifyData_(pValue);

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
RatioBoundedDataInfo::SoftFault(const SoftFaultID  softFaultID,
							    const Uint32       lineNumber,
							    const char*        pFileName,
							    const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, PATIENT_DATA,
  		RATIO_BOUNDED_DATA_INFO, lineNumber, pFileName, pPredicate);
}

#endif		//defined(SIGMA_GUI_CPU) || defined(SIGMA_COMMON)
