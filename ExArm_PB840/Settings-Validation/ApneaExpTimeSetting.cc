#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//================= C L A S S   D E S C R I P T I O N =================
//@ Class:  ApneaExpTimeSetting - Apnea Expiratory Time Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, bounded setting that contains the time (in milliseconds)
//  an expiration lasts during apnea ventilation.  This class inherits from
//  'BatchBoundedSetting' and provides the specific information needed for
//  representation of apnea expiratory time.  This information includes
//  the interval and range of this setting's values, this setting's response
//  to a change of one of its primary setting (see 'acceptPrimaryChange()'),
//  this batch setting's new-patient value (see 'getNewPatientValue()'), and
//  the contraints and dependent settings of this setting.
//
//  This class provides a static method for calculating apnea expiratory time
//  based on the value of apnea inspiratory time and apnea respiratory rate.
//  This calculation method provides a standard way for all settings (as
//  needed) to calculate an apnea expiratory time value.
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class defines an 'updateConstraints_()' method for updating
//  the constraints of this setting.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/ApneaExpTimeSetting.ccv   25.0.4.0   19 Nov 2013 14:27:10   pvcs  $
//
//@ Modification-Log
//
//  Revision: 007   By: gdc    Date: 16-Aug-2010     SCR Number: 6663
//  Project: API/MAT
//  Description:
//  Added support for VentSet API.
// 
//  Revision: 006   By: sah   Date:  22-Jul-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  modified to use newly-modified 'BoundedRange' class to handle
//         range, via linked list of interval nodes
//      *  eliminated unneeded DEVELOPMENT only test code
//      *  incorporated use of new 'warpToRange()' method
//
//  Revision: 005   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'getApplicability()' method
//	*  added 'acceptTransition()' method due to new Transition Rule
//	   implementation
//	*  cleaned up DEVELOPMENT-only 'is{Adjusted,Accepted}Valid()' methods
//
//  Revision: 004   By: dosman Date:  25-Jun-1998    DR Number: BILEVEL 66
//  Project:  BILEVEL
//  Description:
//	The intermediate value of inspiratory time computed in 
//   	calcBasedOnSetting_ was changed to be warped in the same 
//	direction as the direction the "setting being calculated".
//	Also, special processing was added in BoundedSetting::calcNewValue()
//
//  Revision: 003   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 002   By: sah    Date:  18-Sep-1997    DR Number: 2360
//  Project: Sigma (R8027)
//  Description:
//	Intermediate calculations of NON-CONSTRAINT values of this setting
//	no longer need to be "warped" to a resolution value, therefore replace
//	'warpValue()' with the new 'getValuePrecision()' method, and
//	remove the 'warpDirection' parameter from the calculation
//	method(s).  Also, update the two DEVELOPMENT-only methods for
//	testing the validity of the adjusted/accepted values to use
//	'IsEquivalent()'.  NOTE:  the warping directions for the apnea
//	inspiratory time min/max bounds was changed to 'WARP_NEAREST',
//	because the calculation is just a summation of floating points
//	which may have a very small delta from the actual resolution value.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//=====================================================================

#include "ApneaExpTimeSetting.hh"
#include "SettingConstants.hh"
#include "FlowPatternValue.hh"
#include "MandTypeValue.hh"

//@ Usage-Classes
#include "ApneaRespRateSetting.hh"
#include "SettingsMgr.hh"
//@ End-Usage


//@ Code...

//====================================================================
//
//  Static Data Definitions...
//
//====================================================================

// $[02046] -- this setting's dependent settings...
static const SettingId::SettingIdType  ARR_DEPENDENT_SETTING_IDS_[] =
  {
    SettingId::APNEA_IE_RATIO,
    SettingId::APNEA_INSP_TIME,

    SettingId::NULL_SETTING_ID
  };


//  $[02044] -- The setting's resolution...
static const BoundedInterval  LAST_NODE_ =
  {
    200.0f,		// absolute minimum...
    0.0f,		// unused...
    TENS,		// unused...
    NULL
  };
static const BoundedInterval  INTERVAL_LIST_ =
  {
    100000.0f,		// absolute maximum...
    10.0f,		// resolution...
    TENS,		// precision...
    &::LAST_NODE_
  };


//====================================================================
//
//  Public Methods...
//
//====================================================================

//============= M E T H O D   D E S C R I P T I O N ==================
//@ Method: ApneaExpTimeSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This setting's maximum and minimum constraint ID are set to
//  'NULL_SETTING_BOUND_ID', because they can NEVER be reached, therefore no
//  bound ID is defined for them -- this setting's values are constrained by
//  the constraints of apnea inspiratory time, apnea I:E ratio and apnea
//  respiratory rate.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ApneaExpTimeSetting::ApneaExpTimeSetting(void)
	 : BatchBoundedSetting(SettingId::APNEA_EXP_TIME,
			       ::ARR_DEPENDENT_SETTING_IDS_,
			       &::INTERVAL_LIST_,
			       NULL_SETTING_BOUND_ID,	// maxConstraintId...
			       NULL_SETTING_BOUND_ID)	// minConstraintId...
{
  CALL_TRACE("ApneaExpTimeSetting()");
	isATimingSetting_ = TRUE;
}   // $[TI1]


//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  ~ApneaExpTimeSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ApneaExpTimeSetting::~ApneaExpTimeSetting(void)
{
  CALL_TRACE("~ApneaExpTimeSetting()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[01071] -- main settings applicability
//---------------------------------------------------------------------
//@ PreCondition
//  (qualifierId == Notification::ACCEPTED  ||
//   qualifierId == Notification::ADJUSTED)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
ApneaExpTimeSetting::getApplicability(
			      const Notification::ChangeQualifier qualifierId
				     ) const
{
  CALL_TRACE("getApplicability(qualifierId)");

  // only viewable, because Apnea Constant Parm is fixed at
  // INSP_TIME_CONSTANT...
  return(Applicability::VIEWABLE);
}  // $[TI1]


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [const, virtual]
//
//@ Interface-Description
//  Return the setting's new patient value.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
ApneaExpTimeSetting::getNewPatientValue(void) const
{
  CALL_TRACE("getNewPatientValue()");

  BoundedValue  newPatient;

  newPatient = calcBasedOnSetting_(SettingId::APNEA_INSP_TIME,
				   BoundedRange::WARP_NEAREST,
  				   BASED_ON_NEW_PATIENT_VALUES);

  return(newPatient);    // $[TI1]
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method: acceptPrimaryChange(primaryId)
//
//@ Interface-Description
//  One of this setting's primary settings have changed, therefore update
//  this setting's value based on the new value of the primary setting
//  -- indicated by 'primaryId'.  If this setting's bound is violated by
//  the primary setting's newly "adjusted" value, a pointer to this
//  setting's bound status is returned.  Otherwise, a value of 'NULL' is
//  returned to indicate no dependent bound was violated.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02002] -- new dependent settings based on proposed primary settings...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

const BoundStatus*
ApneaExpTimeSetting::acceptPrimaryChange(
				  const SettingId::SettingIdType primaryId
				    	)
{
  CALL_TRACE("acceptPrimaryChange(primaryId)");

  // update dynamic constraints...
  updateConstraints_();

  BoundStatus&  rBoundStatus = getBoundStatus_();

  // initialize to holding this setting's id...
  rBoundStatus.setViolationId(getId());

  BoundedValue  newApneaExpTime;

  newApneaExpTime = calcBasedOnSetting_(primaryId, BoundedRange::WARP_NEAREST,
				        BASED_ON_ADJUSTED_VALUES);

  // test 'newApneaExpTime' against this setting's bounded range; the bound
  // violation state, if any, is returned in 'rBoundStatus', while
  // 'newApneaExpTime' is "clipped" if a bound is violated...
  getBoundedRange_().testValue(newApneaExpTime, rBoundStatus);

  // store the value...
  setAdjustedValue(newApneaExpTime);

  const BoundStatus*  pBoundStatus;

  // if there is a bound violation return a pointer to this setting's
  // bound status, otherwise return 'NULL'...
  pBoundStatus = (rBoundStatus.isInViolation()) ? &rBoundStatus	// $[TI1]
						: NULL;		// $[TI2]

  return(pBoundStatus);
}


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method: acceptTransition(settingId, newValue, currValue) [virtual]
//
//@ Interface-Description
//  This method is responsible for updating this setting based on the
//  the Main Control Setting identified by 'settingId' and the transition
//  of its value from 'currValue' to 'newValue'.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02024]\f\ -- transitioning from apnea PC...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
ApneaExpTimeSetting::acceptTransition(const SettingId::SettingIdType,
				      const DiscreteValue,
				      const DiscreteValue)
{
  CALL_TRACE("acceptTransition(settingId, newValue, currValue)");

  BoundedValue  newApneaExpTime;

  newApneaExpTime = calcBasedOnSetting_(SettingId::APNEA_INSP_TIME,
				        BoundedRange::WARP_NEAREST,
				        BASED_ON_ADJUSTED_VALUES);

  // store the transition value into the adjusted context...
  setAdjustedValue(newApneaExpTime);

  setForcedChangeFlag();
}


#if defined(SIGMA_DEVELOPMENT)

//============== M E T H O D   D E S C R I P T I O N =================
// Method:  isAcceptedValid()  [const, virtual]
//
// Interface-Description
//  Is the accepted value of this setting valid as determined by its
//  dependencies?
//---------------------------------------------------------------------
// Implementation-Description
//  The setting determines, and returns its validity.
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

Boolean
ApneaExpTimeSetting::isAcceptedValid(void) const
{
  CALL_TRACE("isAcceptedValid()");

  Boolean  isValid = BoundedSetting::isAcceptedValid();

  if (isValid)
  {
    if (getApplicability(Notification::ACCEPTED) != Applicability::INAPPLICABLE)
    {
      // get the accepted value...
      const BoundedValue  ACCEPTED_VALUE = getAcceptedValue();

      // is the the accepted apnea expiratory time value consistent with the
      // accepted apnea inspiratory time and apnea breath period?...
      const Setting*  pApneaInspTime =
		SettingsMgr::GetSettingPtr(SettingId::APNEA_INSP_TIME);

      const Real32  APNEA_BREATH_PERIOD_VALUE =
			    ApneaRespRateSetting::GetApneaBreathPeriod(
						      BASED_ON_ACCEPTED_VALUES
								      );
      const Real32  APNEA_INSP_TIME_VALUE =
		      BoundedValue(pApneaInspTime->getAcceptedValue()).value;

      const Real32        APNEA_EXP_TIME_VALUE =
					BoundedValue(getAcceptedValue()).value;
      const BoundedValue  CALC_APNEA_EXP_TIME  =
	   calcBasedOnSetting_(SettingId::APNEA_INSP_TIME,
			       BoundedRange::WARP_NEAREST,
			       BASED_ON_ACCEPTED_VALUES);

      isValid = ::IsEquivalent(APNEA_EXP_TIME_VALUE,
			       CALC_APNEA_EXP_TIME.value,
			       CALC_APNEA_EXP_TIME.precision);
    }
  }

  return(isValid);
}


//============== M E T H O D   D E S C R I P T I O N =================
// Method:  isAdjustedValid()  [const, virtual]
//
// Interface-Description
//  Is the adjusted value of this setting valid as determined by its
//  dependencies?
//---------------------------------------------------------------------
// Implementation-Description
//  The setting determines, and returns its validity.
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

Boolean
ApneaExpTimeSetting::isAdjustedValid(void)
{
  CALL_TRACE("IsAdjustedValid()");

  Boolean  isValid = BoundedSetting::isAdjustedValid();

  if (isValid)
  {
    if (getApplicability(Notification::ADJUSTED) != Applicability::INAPPLICABLE)
    {
      // get the adjusted value...
      const BoundedValue  ADJUSTED_VALUE = getAdjustedValue();

      // is the the adjusted apnea expiratory time value consistent with the
      // adjusted apnea inspiratory time and apnea breath period?...
      const Setting*  pApneaInspTime =
		SettingsMgr::GetSettingPtr(SettingId::APNEA_INSP_TIME);

      const Real32  APNEA_INSP_TIME_VALUE =
		      BoundedValue(pApneaInspTime->getAdjustedValue()).value;
      const Real32  APNEA_BREATH_PERIOD_VALUE =
			  ApneaRespRateSetting::GetApneaBreathPeriod(
						      BASED_ON_ADJUSTED_VALUES
								    );

      const Real32        APNEA_EXP_TIME_VALUE =
					BoundedValue(getAdjustedValue()).value;
      const BoundedValue  CALC_APNEA_EXP_TIME  =
	   calcBasedOnSetting_(SettingId::APNEA_INSP_TIME,
			       BoundedRange::WARP_NEAREST,
			       BASED_ON_ADJUSTED_VALUES);

      isValid = ::IsEquivalent(APNEA_EXP_TIME_VALUE,
			       CALC_APNEA_EXP_TIME.value,
			       CALC_APNEA_EXP_TIME.precision);

      if (!isValid)
      {
	const Setting*  pApneaRespRate = SettingsMgr::GetSettingPtr(
						    SettingId::APNEA_RESP_RATE
								   );

	cout << "INVALID Apnea Expiratory Time:\n";
	cout << *this << *pApneaInspTime << *pApneaRespRate << endl;
      }
    }
  }

  return(isValid);
}

#endif // defined (SIGMA_DEVELOPMENT)


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//	      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
ApneaExpTimeSetting::SoftFault(const SoftFaultID  softFaultID,
			       const Uint32       lineNumber,
			       const char*        pFileName,
			       const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
			  APNEA_EXP_TIME_SETTING, lineNumber, pFileName,
			  pPredicate);
}


//====================================================================
//
//  Protected Methods...
//
//====================================================================

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  updateConstraints_()  [virtual]
//
//@ Interface-Description
//  Update the dynamic bounds of this setting.  This method sets this
//  setting's maximum constraint to the most restrictive of this setting's
//  upper bounds, and, correspondingly, set this setting's minimum
//  constraint to its most restrictive lower bound.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
ApneaExpTimeSetting::updateConstraints_(void)
{
  CALL_TRACE("updateConstraints_()");

  //===================================================================
  // determine apnea expiratory time's maximum bound...
  //===================================================================

  const Real32  BREATH_PERIOD_BASED_MAX =
    (ApneaRespRateSetting::GetApneaBreathPeriod(BASED_ON_ADJUSTED_VALUES) -
				    100.0f);

  BoundedRange::ConstraintInfo  maxConstraintInfo;

  // NOTE:   though this constraint will never be hit in a quiescent mode,
  //         it is possible with a LARGE knob delta to jump to an apnea
  //         exp time value larger than this constraint, thereby messing
  //         up apnea I:E ratio bound calculations; this prevents the
  //         screwed up I:E calculation scenario, and allows for apnea I:E
  //         ratio to detect the bound violation, itself...
  maxConstraintInfo.id    = NULL_SETTING_BOUND_ID;
  maxConstraintInfo.value = BREATH_PERIOD_BASED_MAX;

  // this setting has no soft bounds...
  maxConstraintInfo.isSoft = FALSE;

  // update apnea expiratory time's maximum constraint info...
  getBoundedRange_().updateMaxConstraint(maxConstraintInfo);
}   // $[TI1]


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method: calcBasedOnSetting_(basedOnSettingId, warpDirection, basedOnCategory)  [const]
//
//@ Interface-Description
//  This method is responsible for calculating, and returning, a new
//  "adjusted" value for this setting, based on the "adjusted" value
//  of the dependent setting given by 'basedOnSettingId'.  This method will
//  calculate values for each of this setting's dependent settings,
//  based on values in the Adjusted Context.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  ((basedOnSettingId == SettingId::APNEA_RESP_RATE  ||
//    basedOnSettingId == SettingId::APNEA_INSP_TIME)  &&
//   (basedOnCategory == BASED_ON_ADJUSTED_VALUES  ||
//    basedOnCategory == BASED_ON_NEW_PATIENT_VALUES))
//			||
//  ((basedOnSettingId == SettingId::APNEA_IE_RATIO  ||
//    basedOnSettingId == SettingId::APNEA_TIDAL_VOLUME  ||
//    basedOnSettingId == SettingId::APNEA_PEAK_INSP_FLOW  ||
//    basedOnSettingId == SettingId::APNEA_PLATEAU_TIME  ||
//    basedOnSettingId == SettingId::APNEA_FLOW_PATTERN)  &&
//    basedOnCategory == BASED_ON_ADJUSTED_VALUES)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

BoundedValue
ApneaExpTimeSetting::calcBasedOnSetting_(
			  const SettingId::SettingIdType basedOnSettingId,
			  const BoundedRange::WarpDir    warpDirection,
			  const BasedOnCategory          basedOnCategory
					) const
{
  CALL_TRACE("calcBasedOnSetting_(basedOnSettingId, warpDirection, basedOnCategory)");

  // get the apnea breath period from the apnea respiratory rate...
  const Real32  APNEA_BREATH_PERIOD_VALUE =
		  ApneaRespRateSetting::GetApneaBreathPeriod(basedOnCategory);

  BoundedValue  apneaExpTime;

  switch (basedOnSettingId)
  {
  //-------------------------------------------------------------------
  // $[02014](2) -- new apnea exp. time while changing apnea insp. time...
  // $[02018](2) -- new apnea exp. time while keeping apnea insp. time
  //		    constant...
  // $[02043]    -- new-patient equation...
  //-------------------------------------------------------------------
  case SettingId::APNEA_RESP_RATE :
  case SettingId::APNEA_INSP_TIME :
    {   // $[TI1] -- base apnea exp time's calculation on apnea insp time...
      const Setting*  pApneaInspTime =
		       SettingsMgr::GetSettingPtr(SettingId::APNEA_INSP_TIME);

      Real32  apneaInspTimeValue;

      switch (basedOnCategory)
      {
      case BASED_ON_NEW_PATIENT_VALUES :	// $[TI1.1]
	// get the new-patient apnea insp time value...
	apneaInspTimeValue =
		    BoundedValue(pApneaInspTime->getNewPatientValue()).value;
	break;
      case BASED_ON_ADJUSTED_VALUES :		// $[TI1.2]
	// get the "adjusted" apnea insp time value...
	apneaInspTimeValue =
		       BoundedValue(pApneaInspTime->getAdjustedValue()).value;
	break;
      case BASED_ON_ACCEPTED_VALUES :
	// fall through to 'default:' when not in DEVELOPMENT mode...
#if defined(SIGMA_DEVELOPMENT)
	// get the "accepted" apnea insp time value...
	apneaInspTimeValue =
		       BoundedValue(pApneaInspTime->getAcceptedValue()).value;
	break;
#endif // defined(SIGMA_DEVELOPMENT)
      default :
	// unexpected 'basedOnCategory' value..
	AUX_CLASS_ASSERTION_FAILURE(basedOnCategory);
	break;
      };

      // calculate apnea expiratory time...
      apneaExpTime.value = APNEA_BREATH_PERIOD_VALUE - apneaInspTimeValue;
    }
    break;

  //-------------------------------------------------------------------
  // $[02013](2) -- new apnea exp. time while changing apnea I:E ratio...
  //-------------------------------------------------------------------
  case SettingId::APNEA_IE_RATIO :
    {   // $[TI2] -- base apnea insp time's calculation on apnea I:E ratio...
      // apnea inspiratory time is to base its value on the "adjusted"
      // apnea I:E ratio, only...
      AUX_CLASS_ASSERTION((basedOnCategory == BASED_ON_ADJUSTED_VALUES),
      			  basedOnCategory);

      const Setting*  pApneaIeRatio =
		       SettingsMgr::GetSettingPtr(SettingId::APNEA_IE_RATIO);

      // get the apnea I:E ratio value from the Adjusted Context...
      const Real32  APNEA_IE_RATIO_VALUE =
			BoundedValue(pApneaIeRatio->getAdjustedValue()).value;

      Real32  eRatioValue;
      Real32  iRatioValue;

      if (APNEA_IE_RATIO_VALUE < 0.0f)
      {   // $[TI2.1]
	// the apnea I:E ratio value is negative, therefore we have 1:xx with
	// "xx" the apnea expiratory portion of the ratio...
	iRatioValue = 1.0f;
	eRatioValue = -(APNEA_IE_RATIO_VALUE);
      }
      else
      {   // $[TI2.2]
	// the apnea I:E ratio value is positive, therefore we have xx:1 with
	// "xx" the apnea inspiratory portion of the ratio...
	iRatioValue = APNEA_IE_RATIO_VALUE;
	eRatioValue = 1.0f;
      }

      // calculate the apnea exp time from the apnea I:E ratio...
      apneaExpTime.value = (eRatioValue * APNEA_BREATH_PERIOD_VALUE) /
				   (iRatioValue + eRatioValue);
    }
    break;

  //-------------------------------------------------------------------
  // $[02019](4) -- new apnea exp. time with new VCV parameter...
  //-------------------------------------------------------------------
  case SettingId::APNEA_TIDAL_VOLUME :
  case SettingId::APNEA_PEAK_INSP_FLOW :
  case SettingId::APNEA_PLATEAU_TIME :
  case SettingId::APNEA_FLOW_PATTERN :
    {   // $[TI3] -- base apnea exp time's calculation on the apnea VCV
	//           settings...
      // apnea expiratory time is to base its value on the "adjusted" VCV
      // settings, only...
      AUX_CLASS_ASSERTION((basedOnCategory == BASED_ON_ADJUSTED_VALUES),
			  basedOnCategory);

      // get pointers to each of the apnea VCV parameters...
      const Setting*  pApneaFlowPattern =
		 SettingsMgr::GetSettingPtr(SettingId::APNEA_FLOW_PATTERN);
      const Setting*  pApneaPeakInspFlow =
		SettingsMgr::GetSettingPtr(SettingId::APNEA_PEAK_INSP_FLOW);
      const Setting*  pApneaPlateauTime =
		  SettingsMgr::GetSettingPtr(SettingId::APNEA_PLATEAU_TIME);
      const Setting*  pApneaTidalVolume =
		  SettingsMgr::GetSettingPtr(SettingId::APNEA_TIDAL_VOLUME);

      const DiscreteValue  APNEA_FLOW_PATTERN_VALUE =
				       pApneaFlowPattern->getAdjustedValue();

      const Real32  APNEA_PEAK_INSP_FLOW_VALUE =
		   BoundedValue(pApneaPeakInspFlow->getAdjustedValue()).value;
      const Real32  APNEA_PLATEAU_TIME_VALUE =
		   BoundedValue(pApneaPlateauTime->getAdjustedValue()).value;
      const Real32  APNEA_TIDAL_VOLUME_VALUE =
		   BoundedValue(pApneaTidalVolume->getAdjustedValue()).value;

      BoundedValue  apneaInspTime;

      switch (APNEA_FLOW_PATTERN_VALUE)
      {
      case FlowPatternValue::SQUARE_FLOW_PATTERN : 
	{   // $[TI3.1] 
	  static const Real32  SQUARE_FACTOR_ = (60000.0f * 0.001f);

	  // $[02019]b -- formula for calculating apnea inspiratory time
	  //              from the apnea VCV parameters...
	  apneaInspTime.value = ((SQUARE_FACTOR_ * APNEA_TIDAL_VOLUME_VALUE) /
					  APNEA_PEAK_INSP_FLOW_VALUE);
	}
	break;

      case FlowPatternValue::RAMP_FLOW_PATTERN :  
	{   // $[TI3.2] 
	  static const Real32  RAMP_FACTOR_ = (60000.0f * 0.001f * 2.0f);

	  const Setting*  pApneaMinInspFlow =
		  SettingsMgr::GetSettingPtr(SettingId::APNEA_MIN_INSP_FLOW);

	  const Real32  APNEA_MIN_INSP_FLOW_VALUE =
		    BoundedValue(pApneaMinInspFlow->getAdjustedValue()).value;

	  // $[02019]c -- formula for calculating apnea inspiratory time from
	  //              the apnea VCV parameters...
	  apneaInspTime.value = ((RAMP_FACTOR_ * APNEA_TIDAL_VOLUME_VALUE) /
		    (APNEA_PEAK_INSP_FLOW_VALUE + APNEA_MIN_INSP_FLOW_VALUE));
	}
	break;

      default :
	// unexpected 'APNEA_FLOW_PATTERN_VALUE' value...
	AUX_CLASS_ASSERTION_FAILURE(APNEA_FLOW_PATTERN_VALUE);
	break;
      };

      // add the apnea plateau time to the calculated apnea inspiratory
      // time -- irregardless of the apnea flow pattern...$[02019]b...
      apneaInspTime.value += APNEA_PLATEAU_TIME_VALUE;

      BoundedSetting*  pApneaInspTime =
	      SettingsMgr::GetBoundedSettingPtr(SettingId::APNEA_INSP_TIME);

      // because apnea inspiratory time has a "coarse" resolution in VCV,
      // the "raw" apnea insp time value must be rounded to a resolution
      // boundary, before calculating apnea expiratory time...
      pApneaInspTime->warpToRange(apneaInspTime, warpDirection, FALSE);

      // calculate apnea expiratory time...
      apneaExpTime.value = APNEA_BREATH_PERIOD_VALUE - apneaInspTime.value;
    }
    break;

  default :
    // unexpected dependent setting id...
    AUX_CLASS_ASSERTION_FAILURE(basedOnSettingId);
    break;
  };

  // place apnea expiratory time on a "click" boundary...
  getBoundedRange().warpValue(apneaExpTime, warpDirection, FALSE);

  return(apneaExpTime);
}
