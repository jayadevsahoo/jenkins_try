#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//================= C L A S S   D E S C R I P T I O N =================
//@ Class:  O2PercentSetting - Oxygen Percentage Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, bounded setting that contains the percentage of oxygen
//  in the gas delivered to the patient during ventilation.  This class
//  inherits from 'BoundedSetting' and provides the specific information
//  needed for representation of oxygen percentage.  This information
//  includes the interval and range of this setting's values, and this batch
//  setting's new-patient value (see 'getNewPatientValue()').
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class has no (explicit) dependent settings or dynamic constraints.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/O2PercentSetting.ccv   25.0.4.0   19 Nov 2013 14:27:32   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005   By: sah    Date: 29-Jun-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//      *  modified to use newly-modified 'BoundedRange' class to handle
//         range, via linked list of interval nodes
//      *  incorporated circuit-specific new-patient values
//
//  Revision: 004   By: sah  Date:  11-Jan-1999    DR Number:  5321
//  Project:  ATC
//  Description:
//	As part of making all 'SoftFault()' methods non-inlined, I'm
//	also making those 'SoftFault()' methods that are NOT called
//	in PRODUCTION mode, DEVELOPMENT-only.
//
//  Revision: 003   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'getApplicability()' method
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//=====================================================================

#include "O2PercentSetting.hh"
#include "PatientCctTypeValue.hh"

//@ Usage-Classes
#include "SettingsMgr.hh"
//@ End-Usage


//@ Code...

//====================================================================
//
//  Static Data Definitions...
//
//====================================================================

//  $[02208] -- The setting's range ...
//  $[02211] -- The setting's resolution ...
static const BoundedInterval  LAST_NODE_ =
  {
    21.0f,		// absolute minimum...
    0.0f,		// unused...
    ONES,		// unused...
    NULL
  };
static const BoundedInterval  INTERVAL_LIST_ =
  {
    100.0f,		// absolute maximum...
    1.0f,		// resolution...
    ONES,		// precision...
    &::LAST_NODE_
  };


//====================================================================
//
//  Public Methods...
//
//====================================================================

//============= M E T H O D   D E S C R I P T I O N ==================
//@ Method: O2PercentSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This setting inherits from BoundedSetting.  The method passes to
//  BoundedSetting it's default information.  Also, the method initializes
//  the value interval.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

O2PercentSetting::O2PercentSetting(void)
       : BatchBoundedSetting(SettingId::OXYGEN_PERCENT,
			     Setting::NULL_DEPENDENT_ARRAY_,
			     &::INTERVAL_LIST_,
			     O2_PERCENT_MAX_ID,	// maxConstraintId...
			     O2_PERCENT_MIN_ID)	// minConstraintId...
{
  CALL_TRACE("O2PercentSetting()");
}   // $[TI1]


//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  ~O2PercentSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Default destructor.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

O2PercentSetting::~O2PercentSetting(void)
{
  CALL_TRACE("~O2PercentSetting()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Oxygen percentage is ALWAYS changeable.
//
//  $[01071] -- main settings applicability
//  $[01081] -- breath settings applicability
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
O2PercentSetting::getApplicability(const Notification::ChangeQualifier) const
{
  CALL_TRACE("getApplicability(qualifierId)");

  return(Applicability::CHANGEABLE);
}   // $[TI1]


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [const, virtual]
//
//@ Interface-Description
//  Get the setting's new-patient value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02209] -- new-patient value...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
O2PercentSetting::getNewPatientValue(void) const
{
  CALL_TRACE("getNewPatientValue()");

  BoundedValue  newPatient;

  const Setting*  pPatientCctType =
		    SettingsMgr::GetSettingPtr(SettingId::PATIENT_CCT_TYPE);

  const DiscreteValue  PATIENT_CCT_TYPE_VALUE =
					pPatientCctType->getAdjustedValue();

  switch (PATIENT_CCT_TYPE_VALUE)
  {
  case PatientCctTypeValue::NEONATAL_CIRCUIT :		// $[TI1]
    newPatient.value     = 40.0f;
    newPatient.precision = ONES;
    break;
  case PatientCctTypeValue::PEDIATRIC_CIRCUIT :
  case PatientCctTypeValue::ADULT_CIRCUIT :		// $[TI2]
    newPatient.value     = 100.0f;
    newPatient.precision = ONES;
    break;
  default :
    // unexpected patient circuit type value...
    AUX_CLASS_ASSERTION_FAILURE(PATIENT_CCT_TYPE_VALUE);
    break;
  }

  return(newPatient);
}


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//	      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
O2PercentSetting::SoftFault(const SoftFaultID  softFaultID,
			    const Uint32       lineNumber,
			    const char*        pFileName,
			    const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
			  OXYGEN_PERCENT_SETTING, lineNumber, pFileName,
			  pPredicate);
}
