#include "stdafx.h"

//=====================================================================
//   This is a proprietary work to which Puritan-Bennett corporation
//   of California claims exclusive right.  No part of this work may
//  be used, disclosed, reproduced, sorted in an information retrieval
//     system, or transmitted by any means, electronic, mechanical,
//   photocopying, recording, or otherwise without the prior written
//      permission of Puritan-Bennett Corporation of California.
//
//          Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//============================== C L A S S   D E S C R I P T I O N ====
//@ Filename: TaskTableDef.cc - TaskTable::Table_ definition.
//---------------------------------------------------------------------
//@ Interface-Description
//  This file contains the definition of the TaskTable::Table_ private
//  data member.  It is maintained separately from other TaskTable class
//  members to allow development configurations the ability to
//  override the table's definition at link time.
//---------------------------------------------------------------------
//@ Rationale
//---------------------------------------------------------------------
//@ Implementation-Description
//  Defines the TaskTable::Table_.  Each entry defines an task thread
//  that the OS-Foundation Task and TaskInfo classes use to initialize
//  from.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Sys-Init/vcssrc/TaskTableDef.ccv   25.0.4.1   20 Nov 2013 17:37:48   rhjimen  $
//
//@ Modification-Log
//
//  Revision: 023  By: rpr    Date:  6-Nov-2012    SCR Number: 6777
//  Project:  BI
//  Description:
//      Removed all monitor software.
//
//  Revision: 022   By:   erm   Date: 4-May-2010   SCR Number: 6436
//  Project:  PROX
//  Description:
//      Modified BD serial task priorty and increased some report times.
//
//  Revision: 021   By:   rhj   Date: 23-Feb-2010   SCR Number: 6436
//  Project:  PROX
//  Description:
//      Added Breath-Delivery Serial task.
//
//  Revision: 020   By: erm    Date: 18-Aug-2010     SCR Number: 6663
//  Project: API/MAT
//  Description:
//		Added more time to the BdStatus report timeout.
//
//  Revision: 019  By:  gdc    Date:  22-Jul-2009    SCR Number: 6147
//  Project:  840S
//  Description:
//		Changed priorities so Task-Control and Network-App are higher
//		priority than Timer Task allowing Communication State and
//		Node State message to be queued ahead of Timer messages.
//		This was one cause of the XB0071 (BD Monitor No Data) fault.
//		Increased touch task timeout to 2 seconds to help minimize
//		task monitor resets associated with this task. Added
//		BdMonitorTask to the BD to handle transmitting BdMonitoredData
//		to the GUI.
//
//  Revision: 018  By:  gdc    Date:  12-Dec-2008    SCR Number: 6068
//  Project:  840S
//  Description:
//		Removed SUN prototype code.
//
//  Revision: 017	By: gdc	 Date: 03-FEB-2008	  SCR Number: 6407
//	Project:  NA
//  Description:
//  Increased stack size for serial interface tasks. The preferred
//  inline optimization -Oi increased stack usage.
//
//  Revision: 016	By: gdc	 Date: 18-APR-2007	  SCR Number: 6237
//	Project:  TREND
//  Description:
//  Added Trend Data Manager task.
//
//  Revision: 015	By: erm	 Date:  5-SEP-2001	   DR Number: 5934
//	Project:  GUIComms (840)
//  Description:
//  Change TaskTable entry for TouchDriver task-monitor period
//  back to 1000. Period of 60000 was used for serial touch development.
//
//  Revision: 014   By: syw   Date:  14-Sep-2000    DR Number: 5695
//  Project:  VTPC
//  Description:
//		Eliminate GRAPHICS conditional compile
//
//  Revision: 013  By: syw   Date:  02-Apr-1998    DR Number: 5048
//  Project:  Sigma (840)
//  Description:
//	Added #ifdef TIMING_TEST code.  Need the BdExec task to be
//	F_SUPER also.
//
//  Revision: 012  By:  gdc    Date:  24-Nov-1997    DCS Number: 2605
//  Project:  Sigma (R8027)
//  Description:
//      Removed DCI task as part of GUI-Serial-Interface restructuring.
//      All DCI responses are now handled directly in the Serial-Interface
//      task.
//
//  Revision: 011  By:  sah    Date:  13-Nov-1997    DCS Number: 2629
//  Project:  Sigma (R8027)
//  Description:
//      Changed the task-monitor periods for BK Maint Task and BK Memory
//	Task, to reduce task monitor failures due to unreasonably high
//	load levels.
//
//  Revision: 010  By:  sah    Date:  27-Oct-1997    DCS Number: 2581
//  Project:  Sigma (R8027)
//  Description:
//      Increased the stack sizes of the GUI Apps Task ('Stack9') and
//	the GUI Bar Task ('Stack24').  Due to changes for this DCS,
//	these two tasks will use more stack.  Also, decreased the size
//	of the Settings Xaction Task stack ('Stack8'), because it uses
//	less than 1K, therefore 8K was way too much.
//
//  Revision: 009  By:  gdc    Date:  01-Oct-1997    DCS Number: 2513
//  Project:  Sigma (R8027)
//  Description:
//      Add SmTasks::TaskControlMsgTask to BD.
//
//  Revision: 008  By:  iv     Date:  14-Aug-1997    DCS Number: 2305
//  Project:  Sigma (R8027)
//  Description:
//      Add BackgroundMaintApp::Task on the GUI CPU.
//
//  Revision: 007  By:  sah    Date:  02-Jul-1997    DCS Number: 1848
//  Project:  Sigma (R8027)
//  Description:
//      Add monitoring of the following GUI tasks:  BD-Monitor Task,
//      Keys Task, Knob Task, and Touch Task.
//
//  Revision: 006  By:  gdc    Date:  01-Jul-1997    DCS Number: 2232/1848
//  Project:  Sigma (R8027)
//  Description:
//      Changed BackgroundMemoryApp and SerialInterface tasks to
//      same priority which must cooperatively multitask (DCS 2232).
//      Monitor BackgroundMemoryApp (DCS 1848).
//
//  Revision: 005  By:  hct    Date:  26-Jun-1997    DCS Number: 1779
//  Project:  Sigma (R8027)
//  Description:
//      Reversed revision 2 to decrease task-monitor period of
//      Network-Apps task back to 1300ms.
//
//  Revision: 004  By:  sah    Date:  25-Jun-1997    DCS Number: 2021
//  Project:  Sigma (R8027)
//  Description:
//      Added the touch task into the task-control's state-change mechanism.
//
//   Revision 003   By:   sah      Date: 12-Jun-1997  DCS Number: 2208
//      Project:   Sigma   (R8027)
//      Description:
//         Modified Blink Task's driver method name, as per new interface.
//
//   Revision 002   By:   sah      Date: 12-Jun-1997    DCS Number:  1779
//      Project:   Sigma   (R8027)
//      Description:
//         Increased task-monitor period of Network-Apps task (temporarily),
//         to eliminate problem with rapid connects/disconnects.
//
//   Revision 001   By:   gdc      Date: 07/06/95    DR Number:   n/a
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//=====================================================================

//=====================================================================
//
//      Includes...
//
//=====================================================================

#include "TaskTable.hh"
#include "NV_MemoryStorageThread.hh"
#include "TaskMonitor.hh"

//@ Usage-Classes
//@ End-Usage

//=====================================================================
//
//      Static Data...
//
//=====================================================================


//  NOTE: this file is best viewed using a .W...I...D...E. window

//  valid init flags - valid for SIGMA_DEVELOPMENT ONLY
//  defined in TaskFlags.hh

// F_DEBUG1        // set debug level 1
// F_DEBUG2        // set debug level 2
// F_DEBUG3        // set debug level 3
// F_DEBUG4        // set debug level 4
// F_TRACE         // set trace on
// F_REMOTEF       // enable remote communications

//  Init task flag...
#ifdef LOCAL_COM
  const  Uint INITFLAG = F_SUPER | F_LOCALCOM;
#else
  const  Uint INITFLAG = F_SUPER;
#endif

#define STACK(A)  (A),sizeof(A)

//  StackWare task flag...

const  Uint FSTACKW = F_SUPER;


#ifdef SIGMA_GUI_CPU

//  GUI Task Stacks...

unsigned char Stack0[12*1024];
unsigned char Stack1[4*1024];
unsigned char Stack2[3*1024];
unsigned char Stack3[10*1024];
unsigned char Stack4[3*1024];
unsigned char Stack5[3*1024];
unsigned char Stack6[4*1024];
unsigned char Stack7[3*1024];
unsigned char Stack8[8*1024];
unsigned char Stack9[28*1024];
unsigned char Stack10[8*1024];
unsigned char Stack11[16*1024];
unsigned char Stack12[3*1024];
unsigned char Stack13[6*1024];
unsigned char Stack14[6*1024];
unsigned char Stack15[25*1024];
unsigned char Stack16[25*1024];
unsigned char Stack17[6*1024];
unsigned char Stack19[25*1024];
unsigned char Stack20[8*1024];
unsigned char Stack21[6*1024];
unsigned char Stack22[3*1024];
unsigned char Stack23[3*1024];
unsigned char Stack24[7*1024];
unsigned char Stack25[3*1024];
unsigned char Stack26[3*1024];

//  GUI Entry Point declarations...
class TaskControl		    {public: static void  MainLoop(void); };
class TimerTbl              {public: static void MsgTimerTask(void); };

#if !defined(USE_PCH)
class AlarmManager          {public: static void ProcessTaskQueue(void); };
class NetworkApp            {public: static void Task(void); };
class GuiApp                {public: static void TaskScheduler(void);
							 		 static void TaskExecutor(void);
							 		 static void TaskBreathBar(void);};
#endif
class SettingsXaction       {public: static void TaskDriver(void); };
class SmTasks               {public: static void ServiceModeManagerTask(void);
                                     static void TaskControlMsgTask(void); };
class Checker               {public: static void Main(void); };
class BackgroundMaintApp    {public: static void Task(void); };
class BackgroundCycleApp    {public: static void Task(void); };
class BackgroundMemoryApp   {public: static void Task(void); };
class SerialInterface       {public: static void Task1(void);
                                     static void Task2(void);
                                     static void Task3(void); };
class Dci                   {public: static void DciTask(void); };
class NovRamXaction         {public: static void TaskDriver(void); };
class BackgroundFaultHandler{public: static void ProcessQueueTask( void ); };
class ServiceDataMgr		{public: static void SDTask( void ); };
class VgaBlink		    {public: static void TaskDriver(void); };
class TouchDriver		    {public: static void Task(void); };

#if !defined(USE_PCH)
class TrendDataMgr 			{public: static void Task(void); };
#endif

#ifdef INTEGRATION_TEST_ENABLE
class SWATCommandServer		{public: static void Task(void); };
#if !defined(USE_PCH)
class PDSimulationServer    {public: static void PDSimulationServerTask( void ); };
#endif
#endif // INTEGRATION_TEST_ENABLE

extern void KeysDrvTask(void);
extern void KnobDrvTask(void);

//============================================================================
//    NOTE: BackgroundMemoryApp::Task MUST be the absolute lowest priority.
//          Any task priority lower than BackgroundMemoryApp::Task will
//          not get a chance to run.
//============================================================================

//  GUI Task Table itself...

const TaskTableEntry TaskTable::Table_[] =
{
// TaskControl MUST be the first entry
// CONVENTION - after common tasks place tasks in descending priority

// Name         Entry Point                      Pri    Debug   Queue                         Stack
// -----------  -------------------------------  ---- --------  ---------------------------   -----
  {"TaskControl", TaskControl::MainLoop,          135, INITFLAG, TASK_CONTROL_Q,              STACK(Stack0 ),10000, TASK_CONTROL_Q }
 ,{"Monitor",    TaskMonitor::MonitorTask,        125, 0,        NULL_ID,                     STACK(Stack12),    0, NULL_ID }
 ,{"Timer",      TimerTbl::MsgTimerTask,          140, 0,        NULL_ID,                     STACK(Stack2 ),  200, NULL_ID }
 ,{"NetworkApp", NetworkApp::Task,                135, 0,        NULL_ID,                     STACK(Stack3 ),  NETWORK_APP_MONITOR_PERIOD, NULL_ID }
 ,{"BkFaultHandler",  BackgroundFaultHandler::ProcessQueueTask,
                                                  165, F_TCMSG,  BACKGROUND_FAULT_HANDLER_Q,  STACK(Stack21),   500, BACKGROUND_FAULT_HANDLER_Q }
 ,{"Blink",      VgaBlink::TaskDriver,            170, 0,        NULL_ID,                     STACK(Stack7 ),  333, NULL_ID }
 ,{"Alarms",     AlarmManager::ProcessTaskQueue,  180, 0,        OPERATING_PARAMETER_EVENT_Q, STACK(Stack10), 5000, OPERATING_PARAMETER_EVENT_Q }
 ,{"NovRam",     NovRamXaction::TaskDriver,       180, F_TCMSG,	 NOVRAM_XACTION_Q,            STACK(Stack20), 30000, NOVRAM_XACTION_Q }
 ,{"SMManager",  SmTasks::ServiceModeManagerTask, 190, F_SUPER,  NULL_ID,                     STACK(Stack11), 0, NULL_ID }
 ,{"SMQueue",    SmTasks::TaskControlMsgTask,     190, F_TCMSG,  SM_GUI_TASK_CTRL_MSG_Q,      STACK(Stack17), 0, SM_GUI_TASK_CTRL_MSG_Q }
 ,{"Settings",   SettingsXaction::TaskDriver,     190, F_TCMSG,  SETTINGS_XACTION_Q,          STACK(Stack8 ), 10000, SETTINGS_XACTION_Q }
 ,{"Keys",       KeysDrvTask,                     200, 0,        NULL_ID,                     STACK(Stack4 ),  1000, NULL_ID }
 ,{"Knob",       KnobDrvTask,                     200, 0,        NULL_ID,                     STACK(Stack5 ),  1000, NULL_ID }
 ,{"BK Cycle",   BackgroundCycleApp::Task,        200, F_SUPER,        BACKGROUND_CYCLE_Q,          STACK(Stack13), 10000, BACKGROUND_CYCLE_Q }
 ,{"SDManager",  ServiceDataMgr::SDTask,          185, 0,        NULL_ID,                     STACK(Stack22),  5000, SERVICE_DATA_TASK_Q }
 ,{"GUIBar",     GuiApp::TaskBreathBar,           170, 0,        GUIAPP_BREATH_BAR_Q,         STACK(Stack24),  5000, GUIAPP_BREATH_BAR_Q }
 ,{"TrendMgr",   TrendDataMgr::Task,              210, 0,        NULL_ID,                     STACK(Stack26),      0, NULL_ID }
 ,{"GUISchd",    GuiApp::TaskScheduler,           215, 0,        GUIAPP_SCHEDULER_Q,          STACK(Stack23),  5000, GUIAPP_SCHEDULER_Q }
 ,{"GUIApp",     GuiApp::TaskExecutor,            220, F_TCMSG,  USER_ANNUNCIATION_Q,         STACK(Stack9),   5000, USER_ANNUNCIATION_Q }
 ,{"BK Maint",   BackgroundMaintApp::Task,        240, 0,        NULL_ID,                     STACK(Stack25), 120000, NULL_ID }
 ,{"SerialIF1",  SerialInterface::Task1,          250, F_SUPER,  NULL_ID,                     STACK(Stack15),      0, NULL_ID }
 ,{"SerialIF2",  SerialInterface::Task2,          250, F_SUPER,  NULL_ID,                     STACK(Stack16),      0, NULL_ID }
 ,{"SerialIF3",  SerialInterface::Task3,          250, F_SUPER,  NULL_ID,                     STACK(Stack19),      0, NULL_ID }
 ,{"BK Memory",  BackgroundMemoryApp::Task,       250, 0,        NULL_ID,                     STACK(Stack14), 300000, NULL_ID }
 ,{"NV_Store",   NV_MemoryStorageThread::Thread,  255, 0,        NULL_ID,                      0,            0x10000, NULL_ID }
#ifdef INTEGRATION_TEST_ENABLE
 ,{"SWATGui",	 SWATCommandServer::Task,		  255, 0,		 NULL_ID,					  STACK(Stack1),	   0, NULL_ID }
 ,{"PDSimulationServer",
	 PDSimulationServer::PDSimulationServerTask,  255, 0,    	 NULL_ID,                     0,				   0, NULL_ID }
#endif
};

#endif  // SIGMA_GUI_CPU


#ifdef SIGMA_BD_CPU

//  BD Task Stacks...

unsigned char Stack0[32*1024];
unsigned char Stack1[96*1024];
unsigned char Stack2[32*1024];
unsigned char Stack3[16*1024];
unsigned char Stack4[32*1024];
unsigned char Stack5[16*1024];
unsigned char Stack6[16*1024];
unsigned char Stack7[16*1024];
unsigned char Stack8[16*1024];
unsigned char Stack9[24*1024];
unsigned char Stack10[32*1024];
unsigned char Stack11[16*1024];
unsigned char Stack12[16*1024];
unsigned char Stack13[6*1024];
unsigned char Stack14[16*1024];
unsigned char Stack15[6*1024];
unsigned char Stack16[16*1024];
unsigned char Stack17[12*1024];
unsigned char Stack18[16*1024];
unsigned char Stack19[6*1024];
unsigned char Stack20[6*1024];
unsigned char Stack22[35*1024];

//  BD Entry Point declarations...
class TaskControl		   {public: static void  MainLoop(void); };
class TimerTbl             {public: static void MsgTimerTask(void); };
class AlarmManager         {public: static void ProcessTaskQueue(void); };
#if !defined(USE_PCH)
class NetworkApp           {public: static void Task(void); };
#endif
class BdTasks              {public: static void BdStatusTask(void);
                                    static void BdExecutiveTask(void);
                                    static void BdSecondaryTask(void);
                                    static void XmitFlashTableTask(void);
                           };
#if !defined(USE_PCH)
class SpiroWfTask          {public: static void WaveformTask(void);
                                    static void SpirometryTask(void);
                                    static void BreathDataTask(void);
                           };
#endif
class SmTasks              {public: static void ServiceModeManagerTask(void);
                                    static void TaskControlMsgTask(void); };
class SettingsXaction      {public: static void TaskDriver(void); };
class NovRamXaction        {public: static void TaskDriver(void); };
class BackgroundCycleApp   {public: static void Task(void); };
class BackgroundMaintApp   {public: static void Task(void); };
class BackgroundMemoryApp  {public: static void Task(void); };
class BackgroundFaultHandler {public: static void ProcessQueueTask( void ); };

#ifdef INTEGRATION_TEST_ENABLE
namespace swat
{
    class UDPProxyServer   {public: static void UDPProxyServerTask( void ); };
}
#endif
#if !defined(USE_PCH)
class BdSerialInterface    {public: static void Task1(void); 
									static void Task2(void); };
#endif

#ifdef EBM_TEST
class BatteryInfoBroadcaster {public: static void BroadcastTask( void ) ; };
#endif

#include "AnalogInputThread.h"
#include "EthernetBroadcastManager.h"


//  BD Task Table itself...

const TaskTableEntry TaskTable::Table_[] =
{
  // init MUST be the first entry
  // CONVENTION - after init task place tasks in descending priority

// Name         Entry Point                      Pri    Debug   Queue                         Stack
// -----------  -------------------------------  ---- --------  ---------------------------   -----
  {"TaskControl", TaskControl::MainLoop,         135, INITFLAG, TASK_CONTROL_Q,               STACK(Stack0 ), 10000, TASK_CONTROL_Q }
 ,{"Monitor",   TaskMonitor::MonitorTask,        125, 0,        NULL_ID,                      STACK(Stack13),     0, NULL_ID }
 ,{"Timer",     TimerTbl::MsgTimerTask,          140, 0,        NULL_ID,                      STACK(Stack3 ),   400, NULL_ID }
 ,{"NetworkApp",NetworkApp::Task,                135, 0,        NULL_ID,                      STACK(Stack4 ),   NETWORK_APP_MONITOR_PERIOD, NULL_ID }
 ,{"BdExec",    BdTasks::BdExecutiveTask,        122, F_TCMSG | F_SUPER,  TIMER_5MS_Q,        STACK(Stack1 ),    600, NULL_ID }
 ,{"BdSecond",  BdTasks::BdSecondaryTask,        123, 0,        NULL_ID,                      STACK(Stack18),   240, TIMER_20MS_Q }
 ,{"BkFaultHandler",  BackgroundFaultHandler::ProcessQueueTask,
                                                 155, F_TCMSG,  BACKGROUND_FAULT_HANDLER_Q,   STACK(Stack19),   750, BACKGROUND_FAULT_HANDLER_Q }
 ,{"WfTask",    SpiroWfTask::WaveformTask,       160, 0,        WAVEFORM_TASK_Q,              STACK(Stack5 ),   1200, WAVEFORM_TASK_Q }
 ,{"SpiroTask", SpiroWfTask::SpirometryTask,     170, 0,        SPIROMETRY_TASK_Q,            STACK(Stack6 ),   1200, SPIROMETRY_TASK_Q }
 ,{"BDataTask", SpiroWfTask::BreathDataTask,     170, 0,        BREATH_DATA_Q,                STACK(Stack7 ),   1200, BREATH_DATA_Q }
 ,{"Alarms",    AlarmManager::ProcessTaskQueue,  180, 0,        OPERATING_PARAMETER_EVENT_Q,  STACK(Stack12),  5000, OPERATING_PARAMETER_EVENT_Q }
 ,{"NovRam",    NovRamXaction::TaskDriver,       180, F_TCMSG,	NOVRAM_XACTION_Q,             STACK(Stack11), 30000, NOVRAM_XACTION_Q }
 ,{"BD Status", BdTasks::BdStatusTask,           190, F_TCMSG,  BD_STATUS_TASK_Q,             STACK(Stack8 ),   1200, BD_STATUS_TASK_Q }
 ,{"Settings",  SettingsXaction::TaskDriver,     190, F_TCMSG,  SETTINGS_XACTION_Q,           STACK(Stack9 ), 30000, SETTINGS_XACTION_Q }
 ,{"SmManager", SmTasks::ServiceModeManagerTask, 200, F_SUPER,  NULL_ID,                      STACK(Stack10),     0, NULL_ID }
 ,{"SMQueue",   SmTasks::TaskControlMsgTask,     200, F_TCMSG,  SM_GUI_TASK_CTRL_MSG_Q,       STACK(Stack20),     0, SM_GUI_TASK_CTRL_MSG_Q }
 ,{"BK Cycle",  BackgroundCycleApp::Task,        210, 0,        BACKGROUND_CYCLE_Q,           STACK(Stack15), 10000, BACKGROUND_CYCLE_Q }
 ,{"ExhVTask",  BdTasks::XmitFlashTableTask,     220, 0,        NULL_ID,                      STACK(Stack14),     0, NULL_ID }
 ,{"BK Maint",  BackgroundMaintApp::Task,        220, 0,        NULL_ID,                      STACK(Stack16), 60000, NULL_ID }
 ,{"BK Memory", BackgroundMemoryApp::Task,       225, 0,        NULL_ID,                      STACK(Stack17), 60000, NULL_ID }
 ,{"SerialIF1", BdSerialInterface::Task1,        175, F_SUPER,  NULL_ID,                      STACK(Stack22),     0, NULL_ID }
 ,{"NV_Store",  NV_MemoryStorageThread::Thread,  255, 0,        NULL_ID,                      0,            0x10000, NULL_ID }
 ,{"ADC Input", AnalogInputThread_t::Thread,     0,   0,        NULL_ID,                      0,            0x10000, NULL_ID }
 ,{"SerialIF2", BdSerialInterface::Task2		,200, 0,        NULL_ID,                      0,            0x10000, NULL_ID }
 ,{"EthBcast Rx", EthernetBroadcastRXThread_t::Thread,201, 0,	NULL_ID,					  0,            0x10000, NULL_ID }

#if defined( INTEGRATION_TEST_ENABLE )
 ,{"UDPProxyServer", swat::UDPProxyServer::UDPProxyServerTask,
                                                  250, 0,       NULL_ID,                      0,				  0, NULL_ID }
#endif // INTEGRATION_TEST_ENABLE

#if defined( EBM_TEST )
 ,{"BatteryInfoBroadcast", BatteryInfoBroadcaster::BroadcastTask, 250, 0,       NULL_ID,                      0,				  0, NULL_ID }
#endif
};

#endif  // SIGMA_BD_CPU

const Uint TaskTable::NumEntries_
           = sizeof(TaskTable::Table_) / sizeof(TaskTableEntry);
