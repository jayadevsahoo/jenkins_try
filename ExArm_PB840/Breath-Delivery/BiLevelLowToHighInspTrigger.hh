
#ifndef BiLevelLowToHighInspTrigger_HH
#define BiLevelLowToHighInspTrigger_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: BiLevelLowToHighInspTrigger - Inspiratory time trigger for BiLevel.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/BiLevelLowToHighInspTrigger.hhv   25.0.4.0   19 Nov 2013 13:59:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 002  By:  sp    Date:  4-Mar-1996    DR Number:674
//       Project:  Sigma (R8027)
//       Description:
//             Remove #ifndef.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial Version
//====================================================================

#  include "Sigma.hh"

#  include "Breath_Delivery.hh"

//@ Usage-Classes
#  include "BreathTrigger.hh"
#  include "BreathType.hh"

//@ End-Usage

class BiLevelLowToHighInspTrigger : public BreathTrigger
{
  public:
    BiLevelLowToHighInspTrigger(void);
    virtual ~BiLevelLowToHighInspTrigger(void);

    static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL, 
			  const char*       pPredicate = NULL);

	void timeUpHappened(Boolean timeUp,
						::BreathType lastBlBreathType,
						Int32 phasedInPeepLowTimeMs,
						Boolean rateChange);

	inline void timeUpHappened(Boolean timeUp);

  protected:
    virtual Boolean triggerCondition_(void);

  private:
    BiLevelLowToHighInspTrigger(const BiLevelLowToHighInspTrigger&);	// not implemented...
    void   operator=(const BiLevelLowToHighInspTrigger&);	// not implemented...

    //@ Data-Member: timeUp_
    // Set to true when the interval timer expires
    Boolean timeUp_;

    //@ Data-Member: timeUpStartTime_
    // current timer count when timeup occured
    Int32 timeUpStartTime_;
    
    //@ Data-Member: lastBlBreathType_
	// The breath type of the last breath at peep high level.
    ::BreathType lastBlBreathType_;

	//@ Data-Member: phasedInPeepLowTimeMs_
	// length of current peep cycle interval
	Int32 phasedInPeepLowTimeMs_;

	//@ Data-Member: Boolean rateChange_
	// state variable to indicate rate change is in process;
	Boolean rateChange_;

};


// Inlined methods
#include "BiLevelLowToHighInspTrigger.in"


#endif // BiLevelLowToHighInspTrigger_HH 
