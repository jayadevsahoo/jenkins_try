#ifndef AlarmLogTabButton_HH
#define AlarmLogTabButton_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1994, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: AlarmLogTabButton - A TabButton whose icon visually indicates whether or
// not there are unviewed alarm log entries.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/AlarmLogTabButton.hhv   25.0.4.0   19 Nov 2013 14:07:32   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By:  gdc    Date:  10-Dec-2008    SCR Number: 6377
//  Project:  840S
//  Description:
//	Removed DELTA project single screen support.
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  wks    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "TabButton.hh"

//@ Usage-Classes
#include "Bitmap.hh"
#include "TimeStamp.hh"
#include "GuiAppClassIds.hh"
//@ End-Usage

class AlarmLogTabButton : public TabButton
{
public:
	AlarmLogTabButton(Uint16 xOrg, Uint16 yOrg, Uint16 width, Uint16 height,
			ButtonType buttonType, Uint8 bevelSize, CornerType cornerType,
			StringId title, SubScreen *pSubScreen);
	~AlarmLogTabButton(void);

	virtual void updateDisplay(void);

	static void SoftFault(const SoftFaultID softFaultID,
								 const Uint32      lineNumber,
								 const char*       pFileName  = NULL, 
								 const char*       pPredicate = NULL);

protected:

	virtual void downHappened_(Boolean isByOperatorAction);
	virtual void upHappened_(Boolean isByOperatorAction);

private:
	// these methods are purposely declared, but not implemented...
	AlarmLogTabButton(void);						// not implemented...
	AlarmLogTabButton(const AlarmLogTabButton&);	// not implemented...
	void operator=(const AlarmLogTabButton&);		// not implemented...

	//@ Data-Member: clipboardBitmap_
	// Clipboard bitmap for alarm log button, which is appended to the
	// button container.  If there are no unviewed log entries, then
	// this bitmap is displayed by itself on the button.  Otherwise, if
	// there are unviewed log entries, then this bitmap is displayed
	// along with the alertBitmap_.
	Bitmap clipboardBitmap_;

	//@ Data-Member: alertBitmap_
	// Alert bitmap for alarm log button, which is appended to the
	// button container.  If there are no unviewed log entries, then
	// this bitmap is hidden.  Otherwise, if there are unviewed log entries,
	// then this bitmap is displayed along with the clipboardBitmap_.
	Bitmap alertBitmap_;

	//@ Data-Member: numLogEntries_
	// Keeps track of the number of log entries in the alarm log.  This
	// is used to do a quick test to see if the alarm log has changed
	// while the alarm log is not displayed (i.e., the alarm log button
	// is in the up state).  When the alarm log reaches its capacity,
	// this counter is no longer useful since the alarm log contents
	// could change while the number of entries stays constant.
	Int32 numLogEntries_;

	//@ Data-Member: lastTimeStamp_
	// A copy of the time stamp for the "newest" alarm log entry.  This
	// is used for a finer-grained test to see if the alarm log has
	// changed while the alarm log is not displayed (i.e., the alarm log
	// button is in the up state).  
	TimeStamp lastTimeStamp_;

};

#endif // AlarmLogTabButton_HH 
