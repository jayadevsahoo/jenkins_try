#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: AlarmSetupSubScreen - The sub-screen in the Lower screen
// selected by pressing the Alarm Setup button.  It displays the
// five alarm sliders allowing the operator to adjust alarm limits.
//---------------------------------------------------------------------
//@ Interface-Description
// This subscreen displays alarm sliders, allowing the setting of various alarm
// limits which monitor the following measured patient data: Circuit Pressure,
// Respiratory Rate, Exhaled Minute Volume, Mandatory Exhaled Tidal Volume and
// Spontaneous Exhaled Tidal Volume.
//
// Using the sliders the following alarm limits can be set: High Circuit
// Pressure, High Respiratory Rate, High and Low Exhaled Minute
// Volume, Low Mandatory Exhaled Tidal Volume, Low Spontaneous Exhaled
// Tidal Volume and High Exhaled Tidal Volume.
//
// If any of the alarm limits are modified then the ACCEPT key is
// activated, allowing the operator to confirm the changes.  The subscreen
// tab button will also change from "CURRENT ALARM" to "PROPOSED ALARM"
//
// The activateHappened()/deactivateHappened() methods are called automatically on
// display/removal of this subscreen by our subscreen area owner.
// If the Accept key is pressed down then acceptHappened() is called.  
// Changes to settings are indicated via valueUpdateHappened().
// Timer events (specifically the subscreen timeout event) are passed
// into timerEventHappened() and Adjust Panel events are communicated
// via the various adjustPanel methods.  Alarm events are communicated
// via the alarmEventHappened() method.
//
//---------------------------------------------------------------------
//@ Rationale
// Groups the components of the Alarm Setup subscreen in a single class.
//---------------------------------------------------------------------
//@ Implementation-Description
// We create 5 AlarmSlider's and add these to the subscreen contents along with
// a SubScreenTitleArea and a ProceedButton.  The ProceedButton is initially
// made invisible.
//
// To detect modification of the alarm limits we use ContextObserver to
// register for update notices when any of the alarm limits change.  A callback
// during display of this subscreen will indicate that a change has occurred
// and we can then activate the ACCEPT key and change the tab button title.
//
// $[01057]
//---------------------------------------------------------------------
//@ Fault-Handling
// Some use of asserts to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
// There should only be one instance of this class.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/AlarmSetupSubScreen.ccv   25.0.4.0   19 Nov 2013 14:07:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 015   By: rhj    Date: 15-Mar-2011   SCR Number: 6755
//  Project:  PROX
//  Description:
//      Display 840's volume data during waiting for patient 
//      connect even if prox is enabled.
//  
//  Revision: 014   By: rhj    Date: 22-Dec-2010   SCR Number: 6631
//  Project:  PROX
//  Description:
//      Display 840's volume data during PROX startup.
//  
//  Revision: 013   By:   rhj   Date: 23-Feb-2010   SCR Number: 6436
//  Project:  PROX
//  Description:
//      Modified to display PROX symbols when PROX is enabled.
//
//  Revision: 012   By:   rhj    Date: 05-March-2009    SCR Number: 6454
//  Project:  840S
//  Description:
//       Change the inhaledSpontTidalVolumeSlider_'s patient data
//       id to INSP_TIDAL_VOL_ALARM_ITEM.
//  
//  Revision: 011   By: gdc Date:  15-Feb-2005    DR Number: 6144
//  Project:  NIV1  
//  Description:
//      DCS 6144 - NIV1 added low circuit pressure alarm slider 
//
//  Revision: 010   By: heatherw Date:  25-Mar-2002    DR Number: 5790
//  Project:  VCP  
//  Description:
//      Added updateInspTidalVolumeSlider_ method to eliminate some logic.
//      Also added function calls for alarm names associated with 
//      inhaledSpontTidalVolumeSlider_.
//
//  Revision: 009   By: ERM      Date:  22-Mar-2002    DR Number: 5790
//  Project:  VCP
//  Description:
//      Replace  alarm A340 with A380
//
//  Revision: 008   By: jja      Date:  22-Mar-2002    DR Number: 5790
//  Project:  VCP
//  Description:
//      Revise names for consistency now that Vti applies to mand breaths also.
//
//  Revision: 007  By:  heatherw   Date:  04-Mar-2002    DCS Number:  5790
//  Project:  VTPC
//  Description:
//      Added condition before the display of Insp. High Volume Limit alarm. 
//	    This was done to change the text which is associated with the min label
//      for the Vti alarm slider.  It now alternates text between Vti Spont,
//      Vti Mand and Vti depending on the Mode, MandType and SupportType.
//
//  Revision: 006  By:  sph	   Date:  08-Feb-2000    DCS Number:  5504
//  Project:  NeoMode
//  Description:
//      Added recent-activity range to alarm bars whereby the standard deviation 
//      the last N consecutive breaths is displayed.
//
//  Revision: 005  By:  sah	   Date:  23-Feb-1999    DCS Number:  5314
//  Project:  ATC
//  Description:
//      Added integrated with new dynamic, setting applicability functionality.
//      This allows for looser coupling between settings and GUI, and the
//      need for significantly less "setting knowledge" in the GUI subsystem.
//
//  Revision: 004  By:  hhd	   Date:  02-Feb-1999    DCS Number:  5322 
//  Project:  ATC
//  Description:
//		Added condition before the display of Insp. High Volume Limit alarm. 
//		Removed method invocation of setMin/MaxSliderValue().
//
//  Revision: 003  By:  hhd	   Date:  01-Feb-1999    DCS Number:  5322 
//  Project:  ATC
//  Description:
//		Initial version.
//		
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "AlarmSetupSubScreen.hh"

#include "Sigma.hh"
#include "ModeValue.hh"

//@ Usage-Classes
#include "AdjustPanel.hh"
#include "GuiTimerRegistrar.hh"
#include "LowerScreen.hh"
#include "MiscStrs.hh"
#include "PromptArea.hh"
#include "SettingContextHandle.hh"
#include "Sound.hh"
#include "PatientDataRegistrar.hh"
#include "ContextSubject.hh"
#include "StaticStdDeviationBuffer.hh"
#include "MandTypeValue.hh"
#include "SupportTypeValue.hh" 
#include "ProxEnabledValue.hh"
#include "BdGuiEvent.hh"
#include "UpperScreen.hh"
#include "VitalPatientDataArea.hh"

//@ End-Usage

//@ Code...
static const Int32 SLIDER_OFFSET_1_ = 2;  
static const Int32 SLIDER_GAP_1_ = 1; 
static const Int32 SLIDER_OFFSET_2_ = 19; 
static const Int32 SLIDER_GAP_2_ = 19;


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: AlarmSetupSubScreen()  [Constructor]
//
//@ Interface-Description
// Constructs an AlarmSetupSubScreen.  Only one instance of this class
// should be created.  One parameter is needed and that is a pointer
// to the subscreen area in which this subscreen will be displayed,
// i.e. a pointer to the Lower Subscreen Area.
//---------------------------------------------------------------------
//@ Implementation-Description
// The subscreen is sized and all drawable components (5 alarm sliders 
// and subscreen title area) are added to the displayable
// container.  We then register as a target for changes to all the alarm limit
// settings.  Note that construction of an AlarmSlider will register this
// object as a target of the AlarmSlider's buttons.  We need to trap these
// button events for resetting the subscreen setting change timeout.
//
// $[01055] Multiple setting changes are allowed before accepting ...
// $[01107] Alarm Setup subscreen adjusts the following settings ...
// $[01108] Alarm Setup subscreen displays the following alarm sliders ...
// $[01109] High exhaled tidal volume setting moves simultaneously on
//			two sliders.
//---------------------------------------------------------------------
//@ PreCondition
// The subscreen area pointer must be non-null.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

AlarmSetupSubScreen::AlarmSetupSubScreen(SubScreenArea *pSubScreenArea) :
			BatchSettingsSubScreen(pSubScreenArea),
			peakCircuitPressureSlider_(
				SettingId::LOW_CCT_PRESS, 
				MiscStrs::LOW_CCT_PRES_HELP_MESSAGE,
				SettingId::HIGH_CCT_PRESS,
				MiscStrs::HIGH_CCT_PRES_HELP_MESSAGE,
				A400,
				A225,
				PatientDataId::PEAK_CCT_PRESS_ITEM,
				MiscStrs::PEAK_CCT_PRESSURE_SLIDER_MIN_LABEL,
				GuiApp::PressUnits() == PressUnitsValue::CMH2O_UNIT_VALUE ?
					MiscStrs::PEAK_CCT_PRESSURE_SLIDER_MAX_LABEL :
					MiscStrs::PEAK_CCT_PRESSURE_SLIDER_MAX_HPA_LABEL,
														// $[TI1], $[TI2]
				this,
				StaticStdDeviationBuffer::GetSummationBuffer(AlarmSlider::PEAK_CIRCUIT_PRESSURE),
				StaticStdDeviationBuffer::GetDeviationBuffer(AlarmSlider::PEAK_CIRCUIT_PRESSURE),
				StaticStdDeviationBuffer::GetBufferSize(),
				FALSE),
			respiratoryRateSlider_(
				SettingId::NULL_SETTING_ID, NULL_STRING_ID,
				SettingId::HIGH_RESP_RATE,
				MiscStrs::HIGH_RESP_RATE_HELP_MESSAGE,
				ALARM_NAME_NULL,
				A235,
				PatientDataId::TOTAL_RESP_RATE_ITEM,
				MiscStrs::RESPIRATORY_RATE_SLIDER_MIN_LABEL,
				MiscStrs::RESPIRATORY_RATE_SLIDER_MAX_LABEL, 
				this,
				StaticStdDeviationBuffer::GetSummationBuffer(AlarmSlider::RESPIRATORY_RATE),
				StaticStdDeviationBuffer::GetDeviationBuffer(AlarmSlider::RESPIRATORY_RATE),
				StaticStdDeviationBuffer::GetBufferSize(),
				TRUE),
			exhaledMinuteVolumeSlider_(
				SettingId::LOW_EXH_MINUTE_VOL,
				MiscStrs::LOW_EXH_MIN_VOL_HELP_MESSAGE,
				SettingId::HIGH_EXH_MINUTE_VOL,
				MiscStrs::HIGH_EXH_MIN_VOL_HELP_MESSAGE,
				A260,
				A210,
				PatientDataId::EXH_MINUTE_VOL_ITEM,
				MiscStrs::EXH_MINUTE_VOL_SLIDER_MIN_LABEL,
				MiscStrs::EXH_MINUTE_VOL_SLIDER_MAX_LABEL, 
				this,
				StaticStdDeviationBuffer::GetSummationBuffer(AlarmSlider::EXHALED_MINUTE_VOLUME),
				StaticStdDeviationBuffer::GetDeviationBuffer(AlarmSlider::EXHALED_MINUTE_VOLUME),
				StaticStdDeviationBuffer::GetBufferSize(),
				TRUE),
			exhaledMandTidalVolumeSlider_(
				SettingId::LOW_EXH_MAND_TIDAL_VOL,
				MiscStrs::LOW_EXH_MAND_TIDAL_VOL_HELP_MESSAGE,
				SettingId::HIGH_EXH_TIDAL_VOL,
				MiscStrs::HIGH_EXH_TIDAL_VOL_HELP_MESSAGE,
				A255,
				A215,
				PatientDataId::EXH_MAND_TIDAL_VOL_ITEM,
				MiscStrs::EXH_MAND_TIDAL_VOL_SLIDER_MIN_LABEL,
				MiscStrs::EXH_MAND_TIDAL_VOL_SLIDER_MAX_LABEL, 
				this,
				StaticStdDeviationBuffer::GetSummationBuffer(AlarmSlider::EXHALED_MAND_TIDAL_VOLUME),
				StaticStdDeviationBuffer::GetDeviationBuffer(AlarmSlider::EXHALED_MAND_TIDAL_VOLUME),
				StaticStdDeviationBuffer::GetBufferSize(),
				TRUE),
			exhaledSpontTidalVolumeSlider_(
				SettingId::LOW_EXH_SPONT_TIDAL_VOL,
				MiscStrs::LOW_EXH_SPON_TIDAL_VOL_HELP_MESSAGE,
				SettingId::HIGH_EXH_TIDAL_VOL,
				MiscStrs::HIGH_EXH_TIDAL_VOL_HELP_MESSAGE,
				A265,
				A215,
				PatientDataId::EXH_SPONT_TIDAL_VOL_ITEM,
				MiscStrs::EXH_SPON_TIDAL_VOL_SLIDER_MIN_LABEL,
				MiscStrs::EXH_SPON_TIDAL_VOL_SLIDER_MAX_LABEL, 
				this,
				StaticStdDeviationBuffer::GetSummationBuffer(AlarmSlider::EXHALED_SPONT_TIDAL_VOLUME),
				StaticStdDeviationBuffer::GetDeviationBuffer(AlarmSlider::EXHALED_SPONT_TIDAL_VOLUME),
				StaticStdDeviationBuffer::GetBufferSize(),
				TRUE),
			inhaledSpontTidalVolumeSlider_(
				SettingId::NULL_SETTING_ID, NULL_STRING_ID,
				SettingId::HIGH_INSP_TIDAL_VOL,
				MiscStrs::HIGH_INH_SPONT_VOL_HELP_MESSAGE,
				ALARM_NAME_NULL,
				A380,
				PatientDataId::INSP_TIDAL_VOL_ALARM_ITEM,
				MiscStrs::INH_SPON_TIDAL_VOL_SLIDER_MIN_LABEL,
				MiscStrs::INH_SPON_TIDAL_VOL_SLIDER_MAX_LABEL, 
				this,
				StaticStdDeviationBuffer::GetSummationBuffer(AlarmSlider::INHALED_SPONT_TIDAL_VOLUME),
				StaticStdDeviationBuffer::GetDeviationBuffer(AlarmSlider::INHALED_SPONT_TIDAL_VOLUME),
				StaticStdDeviationBuffer::GetBufferSize(),
				TRUE)
{
	CALL_TRACE("AlarmSetupSubScreen(pSubScreenArea)");
	CLASS_PRE_CONDITION(pSubScreenArea != NULL);

	// $[TI1]

	// Size and position the sub-screen
	setX(0);
	setY(0);
	setWidth(LOWER_SUB_SCREEN_AREA_WIDTH);
	setHeight(LOWER_SUB_SCREEN_AREA_HEIGHT);

	Uint  idx;

	idx = 0u;
	arrAlarmSliderPtrs_[idx++] = &peakCircuitPressureSlider_;
	arrAlarmSliderPtrs_[idx++] = &respiratoryRateSlider_;
	arrAlarmSliderPtrs_[idx++] = &exhaledMinuteVolumeSlider_;
	arrAlarmSliderPtrs_[idx++] = &exhaledMandTidalVolumeSlider_;
	arrAlarmSliderPtrs_[idx++] = &exhaledSpontTidalVolumeSlider_;
	arrAlarmSliderPtrs_[idx++] = &inhaledSpontTidalVolumeSlider_;
	arrAlarmSliderPtrs_[idx]   = NULL;

	AUX_CLASS_ASSERTION((idx <= MAX_ALARM_SLIDERS_), idx);

	// Append alarm sliders
	for (idx = 0u; arrAlarmSliderPtrs_[idx] != NULL; idx++)
	{
		addDrawable(arrAlarmSliderPtrs_[idx]);
	}

	isProxInop_ = FALSE;
    isProxEnabled_ = FALSE;
	BdEventRegistrar::RegisterTarget(EventData::PROX_FAULT, this);
	BdEventRegistrar::RegisterTarget(EventData::SAFETY_VENT, this);
	BdEventRegistrar::RegisterTarget(EventData::PROX_READY, this);
	isProxInStartupMode_ = FALSE;

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~AlarmSetupSubScreen  [Destructor]
//
//@ Interface-Description
// Destroys the alarm setup subscreen.  Does nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

AlarmSetupSubScreen::~AlarmSetupSubScreen(void)
{
	CALL_TRACE("AlarmSetupSubScreen::~AlarmSetupSubScreen(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: acceptHappened
//
//@ Interface-Description
// Called by the Proceed button when the Accept key is pressed while the
// Proceed button is pressed down.  Allows us to accept the Alarm settings and
// redisplay this subscreen with the new settings now being current settings.
//---------------------------------------------------------------------
//@ Implementation-Description
// Make the "Accept" sound then clear the Adjust Panel focus.  If any of the
// alarm settings is changed, hide the measured value pointer for that slider
// then accept the adjusted settings and deactivate this subscreen.
//
// $[01056] When batch settings are being setup ventilator settings
//			will not be affected
// $[01257] The Accept key shall be the ultimate confirmation step for all ...
// $[01258] The GUI shall accept all adjusted settings.
// $[01114] When a change to an alarm limit is accepted, the measured value...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
AlarmSetupSubScreen::acceptHappened(void)
{
	CALL_TRACE("AlarmSetupSubScreen::acceptHappened(void)");

	if (areSettingsCurrent_())
	{													// $[TI11.1]
		Sound::Start(Sound::INVALID_ENTRY);
	}
	else
	{													// $[TI11.2]
		Sound::Start(Sound::ACCEPT);				// Make the Accept sound

		AdjustPanel::TakeNonPersistentFocus(NULL);	// Clear Adjust Panel focus
		PatientDataRegistrar::ResetPatientValue();

		// hide all of the data pointers of those settings that have changed...
		hideChangedDataPointers_();

		// Accept adjusted settings
		ApneaCorrectionStatus apneaCorrectionStatus;
		apneaCorrectionStatus = SettingContextHandle::AcceptBatch();

		activate();		// Reactivate this subscreen

		//
		// The settings subsystem may have auto-adjusted some apnea
		// parameters upon acceptance of the alarm settings above.  Pass the
		// correction status to the Lower Screen so that it can decide
		// whether or not to auto-launch the apnea setup screen.
		//
		LowerScreen::RLowerScreen.reviewApneaSetup(apneaCorrectionStatus);
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: timerEventHappened
//
//@ Interface-Description
// Called when a timer that we started runs out.  We are passed the id of the
// timer.  This will be the subscreen setting change timer.  We must reset the
// subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// We must reset this subscreen.  We do this by deactivating and then
// reactivating this subscreen.
//
// There is a possibility that the event occurs after this subscreen
// has been removed from the display.  In this case we ignore the
// event. 
//---------------------------------------------------------------------
//@ PreCondition
// The timerId must be the Screen Activity timer id.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
AlarmSetupSubScreen::timerEventHappened(GuiTimerId::GuiTimerIdType timerId)
{
	CALL_TRACE("AlarmSetupSubScreen::timerEventHappened("
									"GuiTimerId::GuiTimerIdType timerId)");

	CLASS_PRE_CONDITION(timerId ==
							GuiTimerId::SUBSCREEN_SETTING_CHANGE_TIMEOUT);

	// Check if the subscreen is visible
	if (isVisible())
	{													// $[TI1]
		// Clear the Adjust Panel focus
		AdjustPanel::TakeNonPersistentFocus(NULL);

		// Reactivate this subscreen.
		deactivate();
		activate();
	}													// $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: hideAllDataPointers [public]
//
//@ Interface-Description
// Called when all measured value pointers need to be turned off. 
//---------------------------------------------------------------------
//@ Implementation-Description
//	The method hidePatientDataPointer() is called on all alarm sliders.
//	This method calls hidePatientDataPointer() which sets the show flag 
//	for any alarm slider object 's value pointer to FALSE.
//---------------------------------------------------------------------
//@ PreCondition
//	none 
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
AlarmSetupSubScreen::hideAllDataPointers()
{
	CALL_TRACE("AlarmSetupSubScreen::hideAllDataPointers()");
	// $[TI1]

	// Hide value pointers of all alarm sliders 
	for (Uint idx = 0u; arrAlarmSliderPtrs_[idx] != NULL; idx++)
	{
		(arrAlarmSliderPtrs_[idx])->hidePatientDataPointer();
	}
}	

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: hideAllStdDeviationBoxes [public]
//
//@ Interface-Description
// Called when all standard deviation bars need to be turned off. 
//---------------------------------------------------------------------
//@ Implementation-Description
//	The method hideStdDeviationBox() is called on all alarm sliders.
//	This method calls hideStdDeviationBox() which sets the show flag for 
//	any alarm slider object 's standard deviation box to FALSE.
// $[NE01009] The alarm bars shall support an activity range display...
//---------------------------------------------------------------------
//@ PreCondition
//	none 
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
AlarmSetupSubScreen::hideAllStdDeviationBoxes()
{
	CALL_TRACE("AlarmSetupSubScreen::hideAllStdDeviationBoxes()");
	// $[TI1]

	// Hide standard deviation box for all alarm sliders 
	for (Uint idx = 0u; arrAlarmSliderPtrs_[idx] != NULL; idx++)
	{
		(arrAlarmSliderPtrs_[idx])->hideStdDeviationBox();
	}
}	


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: alarmEventHappened
//
//@ Interface-Description
// Called to update the alarm state information contained in the 
// alarm setup subscreen sliders. This is called anytime a change in
// the alarm state of the vent occurs.
//---------------------------------------------------------------------
//@ Implementation-Description
// Calls each member slider button alarmEventHappened() method.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
AlarmSetupSubScreen::alarmEventHappened(void)
{
	CALL_TRACE("AlarmSetupSubScreen::alarmEventHappened(void)");
	// $[TI1]

	for (Uint idx = 0u; arrAlarmSliderPtrs_[idx] != NULL; idx++)
	{
		(arrAlarmSliderPtrs_[idx])->alarmEventHappened();
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setAlarmSetupTabTitleText_
//
//@ Interface-Description
//  Sets the title on the alarm setup subscreen select tab button.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls the LowerScreenSelectArea method to set the button's title.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
void
AlarmSetupSubScreen::setAlarmSetupTabTitleText_( StringId stringId )
{
    static LowerScreenSelectArea * PScreenSelectArea_
         = LowerScreen::RLowerScreen.getLowerScreenSelectArea();
 
	// $[TI1]
    PScreenSelectArea_->setAlarmSetupTabTitleText( stringId );
}
 

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
AlarmSetupSubScreen::SoftFault(const SoftFaultID  softFaultID,
							   const Uint32       lineNumber,
							   const char*        pFileName,
							   const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, ALARMSETUPSUBSCREEN,
									lineNumber, pFileName, pPredicate);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activateHappened_
//
//@ Interface-Description
// Called by LowerSubScreenArea just before this subscreen is displayed.
// Needs no arguments.  Allows setting up of the subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// We first inform the Settings subsystem to begin adjusting the Accepted
// setting context.  Then we start the timer to timeout subscreen setting
// changes and proceed to activate the alarm sliders (tells them to display
// the current setting and patient data values).  If current mode is A/C,
// we hide the measured value indicator.  Then initialize the subscreen
// title to be "Current Alarm Setup".  Display some prompts to guide the operator.
//
// $[01111] When the mode is A/C, the current measured value indicator on ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
AlarmSetupSubScreen::activateHappened_(void)
{
	CALL_TRACE("activateHappened_()");

	// Tells Settings-Validation that we are about to adjust batch settings.
	SettingContextHandle::AdjustBatch();

	// Activate sliders
	Uint  numShownSliders = 0u;
	Uint idx = 0u;
	for (idx = 0u; arrAlarmSliderPtrs_[idx] != NULL; idx++)
	{
		AlarmSlider*  pAlarmSlider = arrAlarmSliderPtrs_[idx];

		pAlarmSlider->activate();

		if (pAlarmSlider->getShow())
		{
			numShownSliders++;
		}
	}

	if (numShownSliders < MAX_ALARM_SLIDERS_)
	{		// $[TI3]
		sliderOffset_ = SLIDER_OFFSET_2_;
		sliderGap_    = SLIDER_GAP_2_;
	}
	else
	{	   // $[TI4]
		sliderOffset_ = SLIDER_OFFSET_1_;
		sliderGap_    = SLIDER_GAP_1_;
	}

	// Position alarm sliders.
	for (idx = 0u; arrAlarmSliderPtrs_[idx] != NULL; idx++)
	{
		(arrAlarmSliderPtrs_[idx])->setX(
			sliderOffset_ + (idx * (sliderGap_ + AlarmSlider::CONTAINER_WIDTH))
									    );
	}

	const ContextSubject*  pAcceptedSubject =
								getSubjectPtr_(ContextId::ACCEPTED_CONTEXT_ID);

	// If mode is A/C, the value pointer of the exhaledSpontTidalVolumeSlider 
	// must be hidden.
    if (DiscreteValue(pAcceptedSubject->getSettingValue(SettingId::MODE)) ==
											ModeValue::AC_MODE_VALUE)
    {						   							// $[TI1]
		exhaledSpontTidalVolumeSlider_.hidePatientDataPointer();
    }                                                               
														
    // Update the inspired volume slider labels. 
    updateInspTidalVolumeSlider_();

	// $[PX01004] Add the prox symbol when PROX is enabled.
	if (isProxActive_())
	{
		exhaledMinuteVolumeSlider_.setLabelText(MiscStrs::PROXIMAL_EXH_MINUTE_VOL_SLIDER_MIN_LABEL);
        exhaledMandTidalVolumeSlider_.setLabelText(MiscStrs::PROXIMAL_EXH_MAND_TIDAL_VOL_SLIDER_MIN_LABEL);
		exhaledSpontTidalVolumeSlider_.setLabelText(MiscStrs::PROXIMAL_EXH_SPON_TIDAL_VOL_SLIDER_MIN_LABEL);

	}
	else
	{
		exhaledMinuteVolumeSlider_.setLabelText(MiscStrs::EXH_MINUTE_VOL_SLIDER_MIN_LABEL);
        exhaledMandTidalVolumeSlider_.setLabelText(MiscStrs::EXH_MAND_TIDAL_VOL_SLIDER_MIN_LABEL);
		exhaledSpontTidalVolumeSlider_.setLabelText(MiscStrs::EXH_SPON_TIDAL_VOL_SLIDER_MIN_LABEL);
	}


	// Initialize title text
	setAlarmSetupTabTitleText_(MiscStrs::ALARM_CURRENT_SETUP_TAB_BUTTON_LABEL);

	// Set Primary prompt to "Adjust settings."
	GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY,
					PromptArea::PA_LOW, PromptStrs::ADJUST_SETTINGS_P);

	// Set Secondary prompt to tell user to press ALARM button to cancel
	GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
					PromptArea::PA_LOW, PromptStrs::ALARM_SETUP_CANCEL_S);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateInspTidalVolumeSlider_
//
//@ Interface-Description
//  Called to update the labels for the Vti Alarm Slider.
// 
//---------------------------------------------------------------------
//@ Implementation-Description
//  As the accepted context changes the labels for the Vti slider 
//  also change.  This method takes care of this logic.  Also,
//  there are seperate alarms depending on the context.  This alarm
//  name needs to be associated with the correct alarm, which is
//  determined at run-time after the settings have been accepted.  
//  These names must be appended here so Alarm Analysis can 
//  associate the correct level of urgency with the appropriate alarm.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void 
AlarmSetupSubScreen::updateInspTidalVolumeSlider_(void)
{
     
 
   const ContextSubject *const  P_ACCEPTED_SUBJECT =
							    getSubjectPtr_(ContextId::ACCEPTED_CONTEXT_ID);
   
   DiscreteValue  modeValue =
               P_ACCEPTED_SUBJECT->getSettingValue(SettingId::MODE);

   DiscreteValue  spontTypeValue =
               P_ACCEPTED_SUBJECT->getSettingValue(SettingId::SUPPORT_TYPE);
   DiscreteValue  mandTypeValue =
               P_ACCEPTED_SUBJECT->getSettingValue(SettingId::MAND_TYPE);
   DiscreteValue  proxEnabledValue =
               P_ACCEPTED_SUBJECT->getSettingValue(SettingId::PROX_ENABLED);

   Boolean  Is_Spont_Type_Applic = (SettingContextHandle::GetSettingApplicability(	ContextId::ACCEPTED_CONTEXT_ID,
                                                                             SettingId::SUPPORT_TYPE ) 
                                                                          != Applicability::INAPPLICABLE);
   Boolean Is_TC_Active  = (Is_Spont_Type_Applic && spontTypeValue  == SupportTypeValue::ATC_SUPPORT_TYPE);
   Boolean Is_PA_Active  = (Is_Spont_Type_Applic && spontTypeValue  == SupportTypeValue::PAV_SUPPORT_TYPE);
   Boolean Is_VS_Active  = (Is_Spont_Type_Applic && spontTypeValue  == SupportTypeValue::VSV_SUPPORT_TYPE);			   
   Boolean Is_VCP_Active = (mandTypeValue == MandTypeValue::VCP_MAND_TYPE);
   isProxEnabled_ = (proxEnabledValue == ProxEnabledValue::PROX_ENABLED);


  if ( Is_VCP_Active )
  {                                                           // $[TI4.3.0.0.0]              		
     if ( Is_TC_Active && Is_Spont_Type_Applic )
	 {                                                        // $[TI4.3.0.1.0]
		// Label as Vti (no subscript) 

		// $[PX01004] Add the prox symbol when PROX is enabled.
		if (isProxActive_())
		{
			inhaledSpontTidalVolumeSlider_.setLabelText(MiscStrs::PROXIMAL_INH_TIDAL_VOL_SLIDER_MIN_LABEL);  
		}
		else
		{	
			inhaledSpontTidalVolumeSlider_.setLabelText(MiscStrs::INH_TIDAL_VOL_SLIDER_MIN_LABEL);  
		}
        inhaledSpontTidalVolumeSlider_.setUpperButtonMessageId(MiscStrs::HIGH_INH_TIDAL_VOL_HELP_MESSAGE);   
     }
		
     else 	
     {                                                        // $[TI4.3.0.1.2]
        // Label as Vti mand               

		 // $[PX01004] Add the prox symbol when PROX is enabled.
		if (isProxActive_())
		{
			inhaledSpontTidalVolumeSlider_.setLabelText(MiscStrs::PROXIMAL_INH_MAND_TIDAL_VOL_SLIDER_MIN_LABEL);  
		}
		else
		{	
			inhaledSpontTidalVolumeSlider_.setLabelText(MiscStrs::INH_MAND_TIDAL_VOL_SLIDER_MIN_LABEL);       
		}

        inhaledSpontTidalVolumeSlider_.setUpperButtonMessageId(MiscStrs::HIGH_INH_MAND_VOL_HELP_MESSAGE); 		
     }
     
     inhaledSpontTidalVolumeSlider_.setUpperAlarm(A390);
  }
  else 
  if( Is_Spont_Type_Applic && (Is_PA_Active || 
      Is_TC_Active || Is_VS_Active ))
  {                                                           // $[TI4.3.0.2.0]     
  	  // Label as Vti spont

	  // $[PX01004] Add the prox symbol when PROX is enabled.
	 if (isProxActive_())
	 {
		 inhaledSpontTidalVolumeSlider_.setLabelText(MiscStrs::PROXIMAL_INH_SPON_TIDAL_VOL_SLIDER_MIN_LABEL);  
	 }
	 else
	 {	
		 inhaledSpontTidalVolumeSlider_.setLabelText(MiscStrs::INH_SPON_TIDAL_VOL_SLIDER_MIN_LABEL);  
	 }


     inhaledSpontTidalVolumeSlider_.setUpperButtonMessageId(MiscStrs::HIGH_INH_SPONT_VOL_HELP_MESSAGE);
     
     if ( Is_VS_Active)
     {
        inhaledSpontTidalVolumeSlider_.setUpperAlarm(A385);
     }
     else
     {
        inhaledSpontTidalVolumeSlider_.setUpperAlarm(A380);
     }
    
  }
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: deactivateHappened_
//
//@ Interface-Description
// Called when this subscreen is being deactivated, i.e. removed from
// the display, allowing us to do any necessary cleanup.
//---------------------------------------------------------------------
//@ Implementation-Description
// Clears any prompts which may have been displayed and clears the Adjust Panel
// focus in case we had it.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
AlarmSetupSubScreen::deactivateHappened_(void)
{
	CALL_TRACE("AlarmSetupSubScreen::deactivateHappened_(void)");

	for (Uint idx = 0u; arrAlarmSliderPtrs_[idx] != NULL; idx++)
	{
		(arrAlarmSliderPtrs_[idx])->deactivate();
	}

	setAlarmSetupTabTitleText_(MiscStrs::ALARM_SETUP_TAB_BUTTON_LABEL);
}		// $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  valueUpdateHappened_
//
//@ Interface-Description
// Called when a setting adjusted by this subscreen has changed.
// Passed the following parameters:
// >Von
//	settingId	The enum id of the setting which changed.
//	contextId	The context in which the change happened, either
//				ContextId::ACCEPTED_CONTEXT_ID or
//				ContextId::ADJUSTED_CONTEXT_ID.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
// If the current seting value is changed, we change the subscreen's
// title to "Proposed Alarm Setup" and display a new prompt.  If the
// proposed value is changed back to the original value, we change the
// subscreen title to "Current Alarm Setup" and reset the prompt.
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
AlarmSetupSubScreen::valueUpdateHappened_(
					  const BatchSettingsSubScreen::TransitionId_ transitionId,
					  const Notification::ChangeQualifier,
					  const ContextSubject*
										 )
{
	CALL_TRACE("valueUpdateHappened_(transitionId, qualifierId, pSubject)");

	if (transitionId == BatchSettingsSubScreen::CURRENT_TO_PROPOSED)
	{													// $[TI1]
		// Change Current Alarm Setup screen title to Proposed Alarm Setup
		// and show the Proceed button
		setAlarmSetupTabTitleText_( 
							MiscStrs::ALARM_PROPOSED_SETUP_TAB_BUTTON_LABEL
								  );

		// Change the Secondary prompt to
		//   "To apply: press ACCEPT. To cancel: touch ALARM."
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
				PromptArea::PA_LOW, PromptStrs::ALARM_SETUP_PROCEED_CANCEL_S);
	}
	else if (transitionId == BatchSettingsSubScreen::PROPOSED_TO_CURRENT)
	{													// $[TI2]
		// Change Proposed Alarm Setup screen title back to Current Alarm Setup
		setAlarmSetupTabTitleText_( 
						MiscStrs::ALARM_CURRENT_SETUP_TAB_BUTTON_LABEL
								  );

		// Reset Secondary prompt
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
						PromptArea::PA_LOW, PromptStrs::ALARM_SETUP_CANCEL_S);
	}													// $[TI3]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: hideChangedDataPointers_ [public]
//
//@ Interface-Description
//  Hide the data pointers of those sliders whose setting is changing.
//---------------------------------------------------------------------
//@ Implementation-Description
// $[01113] If Alarm Setup subscreen is displayed and the user presses the...
//---------------------------------------------------------------------
//@ PreCondition
//	none 
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
AlarmSetupSubScreen::hideChangedDataPointers_(void)
{
	CALL_TRACE("hideChangedDataPointers_()");

	// Hide value pointers of all alarm sliders whose setting is changing...
	for (Uint idx = 0u; arrAlarmSliderPtrs_[idx] != NULL; idx++)
	{  // $[TI1]
		AlarmSlider*  pAlarmSlider = arrAlarmSliderPtrs_[idx];

		if (pAlarmSlider->isVisible())
		{  // $[TI1.1]
			const SettingId::SettingIdType  UPPER_SETTING_ID =
											 pAlarmSlider->getUpperSettingId();
			const SettingId::SettingIdType  LOWER_SETTING_ID =
											 pAlarmSlider->getLowerSettingId();

			const ContextSubject *const  P_ADJUSTED_SUBJECT =
							    getSubjectPtr_(ContextId::ADJUSTED_CONTEXT_ID);

			Boolean  doHideDataPointer;

			if (UPPER_SETTING_ID != SettingId::NULL_SETTING_ID  &&
				LOWER_SETTING_ID != SettingId::NULL_SETTING_ID)
			{  // $[TI1.1.1] -- two settings on this slider...
				doHideDataPointer =
					(P_ADJUSTED_SUBJECT->isSettingChanged(UPPER_SETTING_ID)  ||
					 P_ADJUSTED_SUBJECT->isSettingChanged(LOWER_SETTING_ID));
			}
			else if (UPPER_SETTING_ID != SettingId::NULL_SETTING_ID)
			{  // $[TI1.1.2] -- only an upper setting on this slider...
				doHideDataPointer =
					(P_ADJUSTED_SUBJECT->isSettingChanged(UPPER_SETTING_ID));
			}
			else if (LOWER_SETTING_ID != SettingId::NULL_SETTING_ID)
			{  // $[TI1.1.3] -- only a lower setting on this slider...
				doHideDataPointer =
					(P_ADJUSTED_SUBJECT->isSettingChanged(LOWER_SETTING_ID));
			}
			else
			{  // no settings on this slider...
				// NOTE:  this path should NEVER be reached...
				doHideDataPointer = TRUE;
			}

			if (doHideDataPointer)
			{  // $[TI1.1.4] -- hide the data pointer...
			  pAlarmSlider->hidePatientDataPointer();
			}  // $[TI1.1.5] -- don't hide the data pointer...
		}  // $[TI1.2]
	}
}	

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: bdEventHappened
//
//@ Interface-Description
// Called normally via the BdEventRegistrar when a BD event in which we are
// interested occurs.
//---------------------------------------------------------------------
//@ Implementation-Description
// When Breath-Delivery sends a PROX_FAULT message with an active 
// event id, remove all PROX symbols by setting the isProxInop_ 
// to TRUE, and reactivate this subscreen. 
// $[PX06003]
//---------------------------------------------------------------------
//@ PreCondition                                              
//  none                                                      
//---------------------------------------------------------------------
//@ PostCondition                                             
//  none                                                      
//@ End-Method                                                
//=====================================================================
                                                              
void                                                          
AlarmSetupSubScreen::bdEventHappened(EventData::EventId eventId,
									EventData::EventStatus eventStatus,
									EventData::EventPrompt)
{

	if (EventData::PROX_FAULT == eventId ||
		EventData::SAFETY_VENT == eventId)
	{

		// When PROX is inoperative, set the isProxInop_ to remove the PROX symbols 
		// and refresh the screen.
		if(eventStatus == EventData::ACTIVE)		  
		{
            isProxInop_ = TRUE;
		}
		else
		{
			isProxInop_ = FALSE;
		}

		if (isVisible())
		{
			deactivate();
			activate();

		}
	}
	else if (EventData::PROX_READY == eventId)
	{

		if(eventStatus == EventData::ACTIVE)		  
		{
            isProxInStartupMode_ = FALSE;
		}
		else
		{
			isProxInStartupMode_ = TRUE;
		}

        if (isVisible())
		{
			deactivate();
			activate();
		}

	}

}                                                             
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isProxActive_
//
//@ Interface-Description
//   This method takes no parameters and returns whether prox is 
//   active or not active.
//---------------------------------------------------------------------
//@ Implementation-Description
//   The value of the data member isProxInStartupMode_ && isProxEnabled_ && 
//   !isProxInop_ && isVentNotInStartupOrWaitForPT is returned.
//---------------------------------------------------------------------
//@ PreCondition
//   none.
//---------------------------------------------------------------------
//@ PostCondition
//   none.
//@ End-Method
//=====================================================================
Boolean
AlarmSetupSubScreen::isProxActive_(void)
{
       

    return (!isProxInStartupMode_ && isProxEnabled_ && !isProxInop_ && 
            UpperScreen::RUpperScreen.getVitalPatientDataArea()->
	              			       isVentNotInStartupOrWaitForPT());
}




