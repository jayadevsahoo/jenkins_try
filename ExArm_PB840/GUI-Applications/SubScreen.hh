#ifndef SubScreen_HH
#define SubScreen_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: SubScreen - A subscreen (or display screen).  Subscreens are
// displayed in a subscreen area (see SubScreenArea) and are normally activated
// by pressing subscreen buttons (see SubScreenButton).  This class is part
// of the Loadable Screens cluster.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/SubScreen.hhv   25.0.4.0   19 Nov 2013 14:08:32   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006  By:  gdc	   Date:  25-Jan-2007    SCR Number: 6237
//  Project:  Trend
//  Description:
//		Trend project related changes.
//
//  Revision: 005  By:  quf	   Date:  15-Aug-2001    DCS Number: 5493
//  Project:  GuiComms
//  Description:
//	Fixed existing comment.
//
//  Revision: 004  By:  yyy	   Date:  29-Jun-1998    DCS Number: 5327
//  Project:  NeoMode
//  Description:
//		Initial version.
//
//  Revision: 003  By:  hhd	   Date:  27-Apr-1999    DCS Number: 5322
//  Project:  ATC
//  Description:
//		Initial version.
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "ButtonTarget.hh"
#include "LargeContainer.hh"

//@ Usage-Classes
class SettingButton;  // forward declaration...
//@ End-Usage

class Button;
class SubScreenArea;

class SubScreen : public LargeContainer, public ButtonTarget
{
public:
	SubScreen(SubScreenArea *pSubScreenArea, Boolean isMainSetting=FALSE);
	~SubScreen(void);

	virtual void activate(void) = 0;
	virtual void deactivate(void) = 0;
	virtual void acceptHappened(void);
	virtual void initializeMainSetting(void);

	Boolean isMainSetting(void);
	
	inline SubScreenArea *getSubScreenArea(void) const;

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:
	//@ Type:  OperationMethoPtr_
	//
	typedef void  (SettingButton::* OperationMethodPtr_)(void);

	void  operateOnButtons_(SettingButton*      arrButtonPtrs[],
							OperationMethodPtr_ pOperationMethod);


private:
	// these methods are purposely declared, but not implemented...
	SubScreen(const SubScreen&);		// not implemented...
	void   operator=(const SubScreen&);	// not implemented...

	//@ Data-Member: pSubScreenArea_
	// This is a pointer to the subscreen area which will display
	// this subscreen
	SubScreenArea *pSubScreenArea_;

	//@ Data-Member: isMainSetting_
	// Boolean indicating if this subscreen is a main setting subscreen
	Boolean isMainSetting_;
};

// Inlined methods
#include "SubScreen.in"

#endif // SubScreen_HH 
