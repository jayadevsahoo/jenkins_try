#ifndef DownloadClassIds_HH
#define DownloadClassIds_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
//@ Filename:  DownloadClassIds - Ids of all of the Down load 
//            subsytem Classes.
//---------------------------------------------------------------------
//@ Version
//@(#) $Header:   /840/Baseline/Download/vcssrc/DownloadClassIds.hhv   25.0.4.0   19 Nov 2013 14:02:48   pvcs  $ 
//@ Modification-Log
//
//  Revision: 000  By:  jhv    Date:  Jan-25-96    DR Number:
//	Project:  Sigma (R8027)
//	Description:
//		Initial version 
//  
//====================================================================

//@ Type:  DownloadClassId
enum DownloadClassId
{
	DOWNLOAD_CLASS,
	NETSOCKET_CLASS,
	UDPSOCKET_CLASS,
	TFTP_SOCKET_CLASS,
	BUF_QUEUE_CLASS,
	SEMAPHORE_CLASS,
	QUEUE_CLASS,
	FLASH_MEM_CLASS,
	FLASH_HARDWARE_CLASS,
	TIMETICK_CLASS,
	DOWNLOAD_FAULT_CLASS,
	DOWNLOAD_TASK_CLASS,
	BUILD_FLASH_CLASS,
	BOOT_FILE_CLASS,
	LOCKFILE_CLASS,
	SRECORD_CLASS,
	NEW_PLACEMENT,
	NEW_OPERATOR,
	DELETE_OPERATOR,
	SYSTEM_SUPPORT
};

#endif // DownloadClassIds_HH 
