#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: ExhValveIController - Controller for the exhalation valve during
//      inspiration phase.
//---------------------------------------------------------------------
//@ Interface-Description
//      Methods are implemented to initialize the controller and to control
//      the exhalation valve to the target pressure each BD cycle when the
//      controller is active.
//---------------------------------------------------------------------
//@ Rationale
//      This class implements the algorithms to control the exhalation valve
//      during the inspiration phase of a breath. 
//---------------------------------------------------------------------
//@ Implementation-Description
//      This class controls the exhalation valve to a position such that
//      the desired relief flow through the exhalation valve during
//      inspiration is achieved.  The desired pressure is used to aid the
//      exhalation controller in achieving optimal flow control.
//      
//      The controller is a feedforward (open loop) controller.
//      Once the exhalation flow drops below the desired relief flow or the
//      circuit pressure drops below the desired pressure, the controller then
//      becomes a feedforward plus a PI (proportional plus integral) type of
//      controller.  The PI controller is used to achieve the amount of relief
//      flow through the exhalation valve.		
//---------------------------------------------------------------------
//@ Fault-Handling
//		N/A
//---------------------------------------------------------------------
//@ Restrictions
//      Only one instance of this class may be created.
//---------------------------------------------------------------------
//@ Invariants
//		none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/ExhValveIController.ccv   25.0.4.0   19 Nov 2013 13:59:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 012  By: syw     Date:  09-Feb-2000    DR Number: 5632
//  Project:  NeoMode
//  Description:
//		For neonatal circuit types:
//		* new ki gain
//		* if open loop errorGain = 0.0 else errorGain = 1.0
//
//  Revision: 011  By: syw     Date:  18-Nov-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//		Added code to set errorGain_ = 0.0 if isOpenLoopControl_.
//
//  Revision: 010  By: syw     Date:  26-Apr-1999    DR Number: 5322
//  Project:  ATC
//  Description:
//		Change interface to newBreath() and updateExhalationValve()
//		for TC.
//
//  Revision: 009  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 008  By: syw   Date:  04-Nov-1997    DR Number: 2169
//  	Project:  Sigma (840)
//		Description:
//			Delete 04276 requirement tracing
//
//  Revision: 007  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 006  By:  syw    Date: 13-Mar-1997    DR Number: DCS 1780, 1827
//       Project:  Sigma (R8027)
//       Description:
//			Changed rExhDryFlowSensor.getValue() to rExhFlowSensor.getDryValue().
//
//  Revision: 005 By: syw    Date: 01-Oct-1996   DR Number: DCS 600
//  	Project:  Sigma (R8027)
//		Description:
//			Changed MAX_FLOW_CONTROL_OUTPUT from 5.0 to 2.0.
//
//  Revision: 004 By: syw    Date: 12-Jul-1996   DR Number: DCS 600
//  	Project:  Sigma (R8027)
//		Description:
//			Changed MAX_FLOW_CONTROL_OUTPUT from 2.0 to 5.0.
//
//  Revision: 003 By: syw    Date: 26-Feb-1996   DR Number: DCS 600
//  	Project:  Sigma (R8027)
//		Description:
//			Controls Rev 04 changes:
//			- changed argument types passed in to newBreath() and
//			  updateExhalationValve() methods.
//			- added determineErrorGain_() method.
//			- initialize isFlowControllerActive_ in newBreath().
//			- changed ki_ gain in newBreath().
//			- store desiredReliefFlow_ from reliefFlowTarget in
//			  updateExhalationValve().
//			- call determineErrorGain_() based on isFlowControllerActive_
//			  value in updateExhalationValve() method.
//
//  Revision: 002 By: syw    Date: 21-Feb-1996   DR Number: DCS 674
//  	Project:  Sigma (R8027)
//		Description:
//			Removed #ifndef around #include.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "ExhValveIController.hh"

#include "ValveRefs.hh"
#include "MainSensorRefs.hh"

//@ Usage-Classes

#include "ExhFlowSensor.hh"
#include "PressureSensor.hh"
#include "ExhalationValve.hh"
#include "ExhValveTable.hh"
#include "BD_IO_Devices.hh"
#include "PatientCctTypeValue.hh"
#include "PhasedInContextHandle.hh"

//@ End-Usage

// $04106]
//@ Constant: MAX_FLOW_CONTROL_OUTPUT
// maximum output of flow controller = 5 cmh20
static const Real32 MAX_FLOW_CONTROL_OUTPUT = 5.0F ;

static const Real32 DEFAULT_EXV_KP = 0.1F ;		// cmh20/lpm
static const Real32 DEFAULT_EXV_KI = 0.006F ;	// cmh20/lpm-msec

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ExhValveIController()
//
//@ Interface-Description
//      Constructor.  This method has no arguments and returns nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//		none
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

ExhValveIController::ExhValveIController(void)
{
	CALL_TRACE("ExhValveIController::ExhValveIController(void)") ;

#if CONTROLS_TUNING
	kpTuning_ = DEFAULT_EXV_KP;
	kiTuning_ = DEFAULT_EXV_KI;
#endif
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~ExhValveIController( void)
//
//@ Interface-Description
//      Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//      none
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

ExhValveIController::~ExhValveIController(void)
{
	CALL_TRACE("ExhValveIController::~ExhValveIController(void)") ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newBreath()
//
//@ Interface-Description
//      This method has totalTrajLimit as an argument and has no return value.  This
//      method is called at the beginning of every inspiration to
//      initialize the controller.
//---------------------------------------------------------------------
//@ Implementation-Description
//      The data members are initialized.
// 		$[04104]          
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
ExhValveIController::newBreath( const Real32 totalTrajLimit)
{
	CALL_TRACE("ExhValveIController::newBreath( const Real32 totalTrajLimit)") ;
			 
   	// $[TI1]
    integrator_ = 0.0F ;
    errorGain_ = 0.0F ;
	isFlowControllerActive_ = FALSE ;
	
#if CONTROLS_TUNING
	kp_ = kpTuning_;
#else
	kp_ = DEFAULT_EXV_KP ;		// cmh20/lpm
#endif

	const DiscreteValue  PATIENT_CCT_TYPE =
		PhasedInContextHandle::GetDiscreteValue(SettingId::PATIENT_CCT_TYPE);

	switch (PATIENT_CCT_TYPE)
	{
		case PatientCctTypeValue::PEDIATRIC_CIRCUIT :
		case PatientCctTypeValue::ADULT_CIRCUIT :
#if CONTROLS_TUNING
			ki_ = kiTuning_;
#else
		   	// $[TI2]
		    ki_ = DEFAULT_EXV_KI ;	// cmh20/lpm-msec
#endif
			break;

		case PatientCctTypeValue::NEONATAL_CIRCUIT :
		   	// $[TI3]
		    ki_ = 0.0008F ;	// cmh20/lpm-msec
			break;

		default :
			// unexpected circuit type value...
			AUX_CLASS_ASSERTION_FAILURE(PATIENT_CCT_TYPE);
			break;
	}
	
    qexhLessThanDesiredFlag_ = FALSE ;
    pexhLessThanTargetFlag_ = FALSE ;

	//	$[TC04030] h
	totalTrajLimitReached_ = FALSE ;
	factorConditionMet_ = FALSE ;
   	totalTrajLimit_ = totalTrajLimit ;
   	isOpenLoopControl_ = FALSE ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateExhalationValve()
//
//@ Interface-Description
//      This method has the target pressure used by the feedforward part
//      of the controller, the desired relief flow, whether the new
//		controller algorithm is active or not and factor as arguments and has no
//		return value.  This method is called during every inspiration BD
//		cycle to control the exhalation valve.
//---------------------------------------------------------------------
//@ Implementation-Description
//      The trajectory = targetPressure + flowControllerOutput.  The
//      flowControllerOutput is the output of a PI controller with
//      desiredReliefFlow_ as the setpoint.  The PI controller is activated
//      if certain conditions are met.  Once activated, it will remain
//      activated throughout the remainder of the inspiration.  If the
//		newController flag is set, the trajectory is re-computed.
// 		$[04103] $[04104] $[04105] $[04106] $[04303]
//---------------------------------------------------------------------
//@ PreCondition
//		ki_ > 0.0
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
ExhValveIController::updateExhalationValve( const Real32 targetPressure,
											const Real32 reliefFlowTarget,
			    							const Boolean newController,
    										const Real32 factor)
{
	CALL_TRACE("ExhValveIController::updateExhalationValve( const Real32 targetPressure, \
											const Real32 reliefFlowTarget, \
			    							const Boolean newController, \
    										const Real32 factor)");
					 	
    CLASS_PRE_CONDITION( ki_ > 0.0F) ;

    targetPressure_ = targetPressure ;
    desiredReliefFlow_ = reliefFlowTarget ;
	
    Real32	exhalationFlow = RExhFlowSensor.getDryValue() ;
    Real32	patExhPressure = RExhPressureSensor.getValue() ;

    // trap flag when event occurs
    if (exhalationFlow < desiredReliefFlow_)
    {
	   	// $[TI1.1]
    	qexhLessThanDesiredFlag_ = TRUE ;
    }	// implied else $[TI1.2]

    // trap flag when event occurs
    if (patExhPressure < targetPressure)
    {
	   	// $[TI2.1]
    	pexhLessThanTargetFlag_ = TRUE ;
    }   // implied else $[TI2.2]

    // once flow controller is active (errorGain_ non-zero), it will remain active for
    // the remainder of the inspiration
	
    if (!isFlowControllerActive_)
    {
	   	// $[TI3.1]
        if ( (exhalationFlow >= desiredReliefFlow_ && qexhLessThanDesiredFlag_)
	    		|| (patExhPressure >= targetPressure && pexhLessThanTargetFlag_)
	   	   )
        {
		   	// $[TI4.1]
            isFlowControllerActive_ = TRUE ;
            determineErrorGain_( exhalationFlow, desiredReliefFlow_) ;
		}   // implied else $[TI4.2]
    }
    else
    {
	   	// $[TI3.2]
        determineErrorGain_( exhalationFlow, desiredReliefFlow_) ;
    }

    Real32 flowError = errorGain_ * (exhalationFlow - desiredReliefFlow_) ;
    
    integrator_ += flowError * CYCLE_TIME_MS ;

    Real32 	flowControlOutput = kp_ * flowError + ki_ * integrator_ ;

    // limit flowControlOutput
    if (flowControlOutput > MAX_FLOW_CONTROL_OUTPUT)
    {
	   	// $[TI5.1]
        integrator_ = (MAX_FLOW_CONTROL_OUTPUT - flowError * kp_) / ki_ ;
        flowControlOutput = MAX_FLOW_CONTROL_OUTPUT ;
    }  	// implied else $[TI5.2]


    Real32 trajectory = targetPressure + flowControlOutput ;
	
	if (newController)
	{
	   	// $[TI7]
		//	$[TC04300] f 
		if (totalTrajLimitReached_ == TRUE || factorConditionMet_ == TRUE)
		{
		   	// $[TI9]
			//	$[TC04030] e
			if (totalTrajLimitReached_ == FALSE)
			{
			   	// $[TI11]
				flowControlOutput = 0.0 ;
			}	// implied else $[TI12]
		
			//	$[TC04030] f
			trajectory = MIN_VALUE( targetPressure + 2.0F * factor + flowControlOutput,
								totalTrajLimit_ + 2.0F) ;
		}
		else
		{
		   	// $[TI10]
			trajectory = MIN_VALUE( targetPressure + PRESSURE_TO_SEAL,
								totalTrajLimit_ + 2.0F) ;
		}
	}  // implied else $[TI8]

    // limit trajectory range
    if (trajectory > PRESSURE_RANGE)
    {
	   	// $[TI6.1]
    	trajectory = PRESSURE_RANGE ;
    }
    else if (trajectory < 0.0F)
    {
	   	// $[TI6.2]
        integrator_ = -(targetPressure + flowError * kp_) / ki_ ;
    	trajectory = 0.0F ;
    }	// implied else $[TI6.3]

	RExhalationValve.updateValveL2h( trajectory, ExhalationValve::NORMAL) ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
ExhValveIController::SoftFault( const SoftFaultID  softFaultID,
				                const Uint32       lineNumber, 
 							    const char*        pFileName,
							    const char*        pPredicate)
{
	CALL_TRACE( "SoftFault( softFaultID, lineNumber, pFileName, pPredicate)") ;
	FaultHandler::SoftFault( softFaultID, BREATH_DELIVERY, EXHVALVEICONTROLLER,
	                         lineNumber, pFileName, pPredicate) ;
}


#if CONTROLS_TUNING

void ExhValveIController::setKp( const Real32 gain)
{
	kpTuning_ = gain;
}

//Sets the Ki gain.
void ExhValveIController::setKi( const Real32 gain )
{ 
	kiTuning_ = gain;
}

#endif

//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//=====================================================================
//
//  Private Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: determineErrorGain_()
//
//@ Interface-Description
//      This method has exhalation flow and desired relief flow as arguments
//		and has no return value.  This method is called to determine the
//		error gain of the controller.
//---------------------------------------------------------------------
//@ Implementation-Description
//      The errorGain_ is set to a value that is dependent on whether the
//		condition exhFlow < desiredReliefFlow_ is met or not.
//		$[04303]
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
void
ExhValveIController::determineErrorGain_( const Real32 exhFlow,
										  const Real32 desiredReliefFlow)
{
	CALL_TRACE("ExhValveIController::determineErrorGain_( const Real32 exhFlow, \
			   const Real32 desiredReliefFlow)") ;

	if (exhFlow < desiredReliefFlow)
	{
	   	// $[TI1.1]
		errorGain_ = 0.2F * (desiredReliefFlow - exhFlow) ;
	}
	else
	{
	   	// $[TI1.2]
		errorGain_ = 0.01F * (exhFlow - desiredReliefFlow) ;
	}

	if (errorGain_ > 1.0)
	{
	   	// $[TI2.1]
		errorGain_ = 1.0F ;
	} 	// implied else $[TI2.2]

	const DiscreteValue  PATIENT_CCT_TYPE =
		PhasedInContextHandle::GetDiscreteValue(SettingId::PATIENT_CCT_TYPE);

	if (PATIENT_CCT_TYPE == PatientCctTypeValue::NEONATAL_CIRCUIT)
	{
		// $[TI5]
		if (isOpenLoopControl_)
		{
	   		// $[TI3]
			errorGain_ = 0.0 ;
		}  	
		else
		{
			// $[TI4]
			errorGain_ = 1.0 ;
		}
	}
	// $[TI6]
}














