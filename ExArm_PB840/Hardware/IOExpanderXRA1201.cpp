#include "stdafx.h"

//*****************************************************************************
//
//  IOExpanderXRA1201.cpp - Implementation file for IOExpanderXRA1201_t class
//
//
//*****************************************************************************
#include "IOExpanderXRA1201.h"

// TODO E600 remove unnecessary and dead code later

//*****************************************************************************
//
//  IOExpanderXRA1201_t::IOExpanderXRA1201_t() default constructor
//
//*****************************************************************************
IOExpanderXRA1201_t::IOExpanderXRA1201_t(HANDLE FileHandle, uint8_t BusAddress, I2cSubAddressMode_t SubAddrMode, I2cBaudIndex_t BaudIndex, DWORD Timeout = 30) : 
					I2C(FileHandle, BusAddress, SubAddrMode, BaudIndex)
{

    OutputRegister1 = 0x08;  
    OutputRegister0 = 0xFF;  // initially, all the GPIO pins are configured as Inputs


    InitialSelfTestResult = false;
	UNUSED_SYMBOL(Timeout);	
}

					//*****************************************************************************
//
//  IOExpanderXRA1201_t::IOExpanderXRA1201_t() destructor
//
//*****************************************************************************
IOExpanderXRA1201_t::~IOExpanderXRA1201_t()
{
	SetAmberLed(ALARM_LED_OFF);
	SetRedLed(ALARM_LED_OFF);  
}

#if 0
//********************************************************************************
//
//  IOExpanderXRA1201_t:: StartInitialSelfTest()
//  This function will test the basic functionality of this IOExpanderXRA1201
//  driver to determine if the IOExpander chip is working correctly or not before
//  we can use it 
//
//********************************************************************************
void IOExpanderXRA1201_t::StartInitialSelfTest()
{
    GpioPinStatus_t PinStatus = GPIO_OFF;

    bool Result; 
    
    RETAILMSG(TRUE, (_T("ExStartup: IOExpander self test is starting.......\r\n")));

    // try to set GP0.7 to input
    Result = SetPinDirection(DBI_GP0_PIN_7,GPIO_INPUT_DIRECTION);

    if(Result)
    {
        RETAILMSG(TRUE, (_T("ExStartup: Successfully set GP0.7 pin to input\r\n")));
    }
    else
    {
        RETAILMSG(TRUE, (_T("ExStartup: Failed to set GP0.7 pin to input\r\n")));
    }

    int RetryCounter = 10;

    // Read GP0.7 test
    while(--RetryCounter >= 0 && Result) // continue the rest of the test only if we pass the previous test
    {
        Result = ReadPinStatus(DBI_GP0_PIN_7,&PinStatus);
        
        if(Result)
        {
             RETAILMSG(TRUE, (_T("ExStartup: Read GP0.7 pin status ok. Pin status = %d\r\n"),PinStatus));
        }
        else
        {
             RETAILMSG(TRUE, (_T("ExStartup: Failed to Read GP0.7 pin status! Pin status = %d\r\n"),PinStatus));
             break;
        }
    }

    if(Result)   // continue the rest of the test only if we pass the previous test
    {
        // try to set GP0.0 to output
        Result = SetPinDirection(DBI_GP0_PIN_0,GPIO_OUTPUT_DIRECTION);

        if(Result)
        {
            RETAILMSG(TRUE, (_T("ExStartup: Successfully set GP0.0 pin to output\r\n")));
        }
        else
        {
            RETAILMSG(TRUE, (_T("ExStartup: Failed to set GP0.0 pin to output\r\n")));
        }

        RetryCounter = 10;

        // write GP0.0 test
        while(--RetryCounter >= 0 && Result)  // continue the rest of the test only if we pass the previous test           
        {
            // toggling the pin GP0.0 to on or off
            if(PinStatus == GPIO_OFF)
            {
                PinStatus = GPIO_ON;

            }else
            {
                PinStatus = GPIO_OFF;
            }

            Result = SetPinStatus(DBI_GP0_PIN_0,PinStatus);

            if(!Result)  // if any error is detected, we need to get out the loop
            {
                RETAILMSG(TRUE, (_T("ExStartup: Failed to toggle GP0.0 pin!\r\n")));

                break;
            }
        }
    }

    // Based on the test result, set the InitialSelfTestResult member variable to true or false conditionally
    if(Result)
    {
        RETAILMSG(TRUE, (_T("ExStartup: IOExpander self test is completed and successful!\r\n")));

        InitialSelfTestResult = true;

    }else
    {
        InitialSelfTestResult = false;
    }
   
}
#endif
//************************************************************************************************
//
//  IOExpanderXRA1201_t::::EnableIRQ - 
//  Enables IRQ from port 0
//
//************************************************************************************************
bool IOExpanderXRA1201_t::EnableIRQ(void)
{
//   	    BYTE TxData[2];
    	BYTE Register = ACCESS_TO_INTCAP0;
    if(!I2C::WriteDevice(Register, (BYTE)0xFF)) // Interrupts for all keys
    {
        return true;
    }
    else
    {
        return false;
    }
}
//************************************************************************************************
//
//  IOExpanderXRA1201_t::::SetPinDirection - 
//  Set a GPIO pin's direction to either Input or Output. This function returns false if it fails
//  to set the GPIO pin direction. Otherwise it shall return true.
//
//************************************************************************************************
bool IOExpanderXRA1201_t::SetPinDirection(BYTE Port)
{
    bool Rtn;
	uint8_t Register, Mask;
    static const BYTE PIN_CONFIGURATION_MASK0 = 0xFF;
    static const BYTE PIN_CONFIGURATION_MASK1 = 0x70;

    // conditionally fill up the TxData array based on the DigitalInputOutput
    switch(Port)
    {
        default:
        case GPIO_PORT_0:    
             Mask = PIN_CONFIGURATION_MASK0;
             Register = ACCESS_TO_IODIR0;        // set the command based on the register to access
             break;

        case GPIO_PORT_1:       
            Mask = PIN_CONFIGURATION_MASK1;
            Register = ACCESS_TO_IODIR1;       // set the command based on the register to access
            break;
    }

	if(I2C::WriteDevice(Register, Mask) == I2C_SUCCESSFUL)
    {
        Rtn = true;
    }
    else
    {
        Rtn = false;
    }
// initialise backlight
    Register = ACCESS_TO_OLAT1;
    Mask = 0x08;
    I2C::WriteDevice(Register, Mask);
    return Rtn;
}

//***************************************************************************************
//
//  IOExpanderXRA1201_t::::SetPinStatus - 
//  Set a GPIO pin's status to either On or Off. This function returns false if it fails
//  to set the GPIO status. Otherwise it shall return true.
//
//***************************************************************************************
bool IOExpanderXRA1201_t::SetPinStatus(BYTE DigitalPortAndPinNumber, GpioPinStatus_t PinStatus)
{
    uint8_t Port = (DigitalPortAndPinNumber & 0x08) >> 3;
    uint8_t BitMask = GpioPortControlsTable[(DigitalPortAndPinNumber & 0x07)]; 
    BYTE TxData[2];
	uint8_t Register;
	uint8_t Data;
    
    // conditionally fill up the TxData array based on the DigitalInputOutput
    switch(Port)
    {
        default:
        case GPIO_PORT_0:               
  
             Register = ACCESS_TO_OLAT0;        // set the command based on the register to access

             if(PinStatus == GPIO_ON)
             {
                TxData[0] = OutputRegister0 | BitMask;
             }
             else
             {
                TxData[0] = OutputRegister0 & ~BitMask;
             }      
             OutputRegister0 = TxData[0];
             Data = OutputRegister0;
            break;

        case GPIO_PORT_1:
        
            Register = ACCESS_TO_OLAT1;       // set the command based on the register to access

            if(PinStatus == GPIO_ON)
            {
                TxData[1] = OutputRegister1 | BitMask;
            }
            else
            {
                TxData[1] = OutputRegister1 & ~BitMask;
            }
            OutputRegister1 = TxData[1];
            Data = OutputRegister1;
            break;
    }

	if(I2C::WriteDevice(Register, Data) == I2C_SUCCESSFUL)
    {
        return true;
    }
    else
    {
        return false;
    }
}

//***************************************************************************************
//
//  IOExpanderXRA1201_t::ReadPinStatus - 
//  Read a GPIO pin's status to determine it is either On or Off. Once it has determined
//  the status, the value of the call by reference PinStatus parameter will be updated.
//  This function shall return false if it fails to read the GPIO status. Otherwise it
//  shall return true.
//
//***************************************************************************************
bool IOExpanderXRA1201_t::ReadPinStatus(BYTE DigitalPortAndPinNumber, GpioPinStatus_t *PinStatus)
{
    bool RetVal;

    BYTE BitMask = GpioPortControlsTable[(DigitalPortAndPinNumber & 0x07)]; 
    BYTE Port = (DigitalPortAndPinNumber & 0x08) >> 3;
	BYTE Register;
    BYTE RxData; 
	
	if(Port == GPIO_PORT_0)
    {
		Register = ACCESS_TO_GP0;        // set the command based on the register to access   
        RxData = PinStateRegister0;
    }
    else
    {
	    Register = ACCESS_TO_GP1;        // set the command based on the register to access
        RxData = PinStateRegister1;
    }

	if(I2C::ReadDevice(Register, &RxData) == I2C_SUCCESSFUL) // Now read the two data bytes
    {
        RetVal = true;
    }
    else
    {
        RetVal = false;
    }
    // we are only interested in the data from the 0th element of the RxData array,
    // because 0th element of the RxData array shall hold the register data specified by the command byte, 
    // 1th element of the RxData array shall hold the another register data in the same pair
    if((BitMask & RxData) == BitMask)  
    {
       *PinStatus = GPIO_ON;
    }
    else
    {
       *PinStatus = GPIO_OFF;
    }

    return RetVal;
}
//***************************************************************************************
//
//  IOExpanderXRA1201_t::ReadPort0Status - 
//  Read all port 0 GPIO pin's status 
//  This function shall return false if it fails to read the GPIO status. Otherwise it
//  shall return true.
//
//***************************************************************************************
bool IOExpanderXRA1201_t::ReadPort0Status(BYTE *PortStatus)
{
    bool RetVal;

	BYTE Register;
    BYTE RxData; 
	
	Register = ACCESS_TO_GP0;        // set the command based on the register to access   
    RxData = PinStateRegister0;


	if(I2C::ReadDevice(Register, &RxData) == I2C_SUCCESSFUL) // Now read the two data bytes
    {
        RetVal = true;
        PortStatus[0] = RxData;
    }
    else
    {
        RetVal = false;
    }

    return RetVal;
}
//#############################################################################
//
//  IOExpanderXRA1201_t::SetRedLed(DigitalOutputStates_t State)
//
//#############################################################################

void IOExpanderXRA1201_t::SetRedLed(LedStatus_t State)
{
	static const BYTE RED_LED   = 0x08 | 0x0; //Port1 Pin 0
	SetPinStatus(RED_LED, (GpioPinStatus_t)State);
}
//#############################################################################
//
//  IOExpanderXRA1201_t::SetAmberLed(DigitalOutputStates_t State)
//
//#############################################################################

void IOExpanderXRA1201_t::SetAmberLed(LedStatus_t State)
{
 static const BYTE AMBER_LED = 0x08 | 0x1; //Port1 Pin 1
 SetPinStatus(AMBER_LED, (GpioPinStatus_t)State);
}
