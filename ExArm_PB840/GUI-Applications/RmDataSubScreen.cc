#include "stdafx.h"
#ifdef SIGMA_GUI_CPU

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1994, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: RmDataSubScreen - Subscreen for displaying supplemental 
//                           Respiratory Mechanics Patient Data.
//---------------------------------------------------------------------
//@ Interface-Description
// This class is a container, derived from the GUI-Foundation Container
// class.  This class contains textual fields, numeric fields, and a GUI
// Setting handle object.  This class inherits Patient Data callbacks
// from the abstract PatientDataTarget class.
//
// This area relies on the data provided by the Patient Data subsystem
// for the measured patient data values.  Also, this area watches the
// 'mode' setting maintained by the Settings subsystem.
//---------------------------------------------------------------------
//@ Rationale
// This class is dedicated to managing the display of the
// RmDataSubScreen on the UpperSubScreenArea.
//---------------------------------------------------------------------
//@ Implementation-Description
// The Patient Data subsystem notifies this area when updated values are
// available.  The numeric measured values are updated once per breath.
// Each data field has the capability of hiding the numeric value when
// there is no data available or flashing the numeric value when the
// data reaches a built-in limit.
//
// The various display objects for all three modes are added to the
// Container when it is constructed. 
//---------------------------------------------------------------------
//@ Fault-Handling
// None.
//---------------------------------------------------------------------
//@ Restrictions
// None.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/RmDataSubScreen.ccv   25.0.4.0   19 Nov 2013 14:08:18   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005   By: rhj    Date: 05-Apr-2011   SCR Number: 6759
//  Project:  PROX
//  Description:
//      Added the missing wye symbol for patient data items f/Vt 
//      and Vdot e spont  
//  
//  Revision: 004   By: gdc    Date: 24-Sep-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal non-invasive CPAP.
//
//  Revision: 003   By: gdc    Date: 11-Aug-2008    SCR Number: 6439
//  Project:  840S
//  Description:
//      Removed touch screen "drill-down" work-around.
//
//  Revision: 002   By: rhj   Date:  20-Feb-2007    SCR Number: 6345
//  Project:  RESPM
//  Description:
//        Removed C20/C.
//
//  Revision: 001   By: rhj   Date:  10-May-2006    SCR Number: 6236
//  Project:  RESPM
//  Description:
//       RESPM Project Initial Version
//
//=====================================================================

#include "RmDataSubScreen.hh"

//@ Usage-Classes
#include "PatientDataRegistrar.hh"
#include "BreathDatumHandle.hh"
#include "Fio2EnabledValue.hh"
#include "MiscStrs.hh"
#include "ModeValue.hh"
#include "UpperScreen.hh"
#include "UpperSubScreenArea.hh"
#include "BdEventRegistrar.hh"
#include "BdGuiEvent.hh"
#include "EventData.hh"
#include "GuiEventRegistrar.hh"
#include "MathUtilities.hh"
#include "SupportTypeValue.hh"
#include "MandTypeValue.hh"
#include "SettingContextHandle.hh"
#include "SafetyPcvSettingValues.hh"
#include "PatientDataMgr.hh"
#include "VtpcvState.hh"
#include "PavState.hh"
#include "SoftwareOptions.hh"
#include "Setting.hh"
#include "Precision.hh"
#include "PatientCctTypeValue.hh"
#include "ProxEnabledValue.hh"

//@ End-Usage

//@ Code...

static const Int32 ROW1_Y_ =   6;
static const Int32 ROW2_Y_ =  43;       // + 37 pixel spread...
static const Int32 ROW3_Y_ =  80;
static const Int32 ROW4_Y_ = 117;
// static const Int32 ROW5_Y_ = 154;    
static const Int32 ROW6_Y_ = 191;
static const Int32 ROW7_Y_ = 228;

static const Int32 COL1_LABEL_X_ =  10;
static const Int32 COL1_VALUE_X_ =  60;     // LABEL + 50
static const Int32 COL1_UNIT_X_  = 160;     // VALUE + 100

static const Int32 COL2_LABEL_X_ = 215;
static const Int32 COL2_VALUE_X_ = 265;
static const Int32 COL2_MARKER_X_= 290;
static const Int32 COL2_UNIT_X_  = 365;
static const Int32 COL2_ERROR_X_ = 305;

static const Int32 COL3_LABEL_X_ = 440;
static const Int32 COL3_VALUE_X_ = 490;
static const Int32 COL3_UNIT_X_  = 590;

static const Int32 VALUE_WIDTH_  = 90;
static const Int32 VALUE_HEIGHT_ = 35;

static const Int32 BACK_BUTTON_X_ = 535;    
static const Int32 BACK_BUTTON_Y_ = 230;    
static const Int32 BACK_BUTTON_WIDTH_ = 90; 
static const Int32 BACK_BUTTON_HEIGHT_ = 40;   
static const Int32 BACK_BUTTON_BORDER_ = 3;

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: RmDataSubScreen()  [Constructor]
//
//@ Interface-Description
// Constructor.  The 'pSubScreenArea' parameter is the pointer to the
// parent SubScreenArea which contains this subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Initializes all members and initializes the colors, sizes, and
// positions of all graphical objects.  Adds all graphical objects to
// the parent container for display.  Registers with the
// ContextObserver for notification of changes to the 'mode'
// setting.  Also, registers for updates with the Patient Data
// subsystem.
//
// $[01006] All shorthand symbols in the RM Data subscreen must display
//          their meaning in the Message Area ...
// $[RM12042]
// $[RM12043]
// $[RM12044]
//---------------------------------------------------------------------
//@ PreCondition
// The given 'pSubScreenArea' pointer must not be NULL.
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

RmDataSubScreen::RmDataSubScreen(SubScreenArea* pSubScreenArea) :
    SubScreen(pSubScreenArea),
    
    dynamicComplianceLabel_(MiscStrs::TOUCHABLE_DYNAMIC_COMPLIANCE_LABEL,
                            MiscStrs::TOUCHABLE_DYNAMIC_COMPLIANCE_MSG),
    dynamicComplianceValue_(TextFont::TWENTY_FOUR, 5, RIGHT),
    dynamicComplianceUnit_( GuiApp::PressUnits() == PressUnitsValue::CMH2O_UNIT_VALUE ?
                            MiscStrs::TOUCHABLE_DYNAMIC_COMPLIANCE_UNIT :
                            MiscStrs::TOUCHABLE_DYNAMIC_COMPLIANCE_HPA_UNIT),     
    
    dynamicResistanceLabel_(MiscStrs::TOUCHABLE_DYNAMIC_RESISTANCE_LABEL,
                            MiscStrs::TOUCHABLE_DYNAMIC_RESISTANCE_MSG),
    dynamicResistanceValue_(TextFont::TWENTY_FOUR, 5, RIGHT),
    dynamicResistanceUnit_(  GuiApp::PressUnits() == PressUnitsValue::CMH2O_UNIT_VALUE ?
                             MiscStrs::TOUCHABLE_DYNAMIC_RESISTANCE_UNIT :
                             MiscStrs::TOUCHABLE_DYNAMIC_RESISTANCE_HPA_UNIT),   
    
    peakSpontFlowLabel_(MiscStrs::TOUCHABLE_PEAK_SPONT_FLOW_LABEL,
                        MiscStrs::TOUCHABLE_PEAK_SPONT_FLOW_MSG),
    peakSpontFlowValue_(TextFont::TWENTY_FOUR, 5, RIGHT),
    peakSpontFlowUnit_( MiscStrs::TOUCHABLE_PEAK_SPONT_FLOW_UNIT),     
    
    peakExpFlowLabel_(MiscStrs::TOUCHABLE_PEAK_EXP_FLOW_LABEL,
                      MiscStrs::TOUCHABLE_PEAK_EXP_FLOW_MSG),
    peakExpFlowValue_(TextFont::TWENTY_FOUR, 5, RIGHT),
    peakExpFlowUnit_( MiscStrs::TOUCHABLE_PEAK_EXP_FLOW_UNIT),     
    
    endExpFlowLabel_(MiscStrs::TOUCHABLE_END_EXP_FLOW_LABEL,
                     MiscStrs::TOUCHABLE_END_EXP_FLOW_MSG),
    endExpFlowValue_(TextFont::TWENTY_FOUR, 5, RIGHT),
    endExpFlowUnit_( MiscStrs::TOUCHABLE_END_EXP_FLOW_UNIT),    
        
    spontFvRatioLabel_(MiscStrs::TOUCHABLE_SPONT_FV_RATIO_LABEL,
                       MiscStrs::TOUCHABLE_SPONT_FV_RATIO_MSG),
    spontFvRatioValue_(TextFont::TWENTY_FOUR, 3, RIGHT),
    
    intrinsicPeepLabel_(MiscStrs::TOUCHABLE_LARGE_INTRINSIC_PEEP_LABEL,
                           MiscStrs::TOUCHABLE_INTRINSIC_PEEP_MSG),
    intrinsicPeepValue_(TextFont::TWENTY_FOUR, 3, RIGHT),
    intrinsicPeepUnit_(GuiApp::PressUnits() == PressUnitsValue::CMH2O_UNIT_VALUE  ?
                         MiscStrs::TOUCHABLE_PRESSURE_UNIT :
                         MiscStrs::TOUCHABLE_PRESSURE_HPA_UNIT),
    intrinsicPeepMarkerText_(MiscStrs::RM_MARKER_LABEL),
    notAvailableLabel_(MiscStrs::TOUCHABLE_RM_NOT_AVAILABLE_TITLE,
                       MiscStrs::TOUCHABLE_RM_NOT_AVAILABLE_MSG),
    backButton_(BACK_BUTTON_X_, BACK_BUTTON_Y_,
                BACK_BUTTON_WIDTH_, BACK_BUTTON_HEIGHT_,
                Button::LIGHT, BACK_BUTTON_BORDER_, Button::SQUARE,
                Button::NO_BORDER, MiscStrs::BACK_BUTTON_TITLE),
    cStaticLabel_(MiscStrs::TOUCHABLE_LARGE_STATIC_COMPLIANCE_LABEL,
                           MiscStrs::TOUCHABLE_STATIC_COMPLIANCE_MSG),
    cStaticValue_(TextFont::TWENTY_FOUR, 3, RIGHT),
    cStaticUnit_(GuiApp::PressUnits() == PressUnitsValue::CMH2O_UNIT_VALUE  ?
                         MiscStrs::TOUCHABLE_STATIC_COMPLIANCE_UNIT :
                         MiscStrs::TOUCHABLE_STATIC_COMPLIANCE_HPA_UNIT),
    cStaticNotAvail_(MiscStrs::RM_DASH_LABEL),
    cStaticMarkerText_(MiscStrs::RM_MARKER_LABEL),
    cStaticInadequateText_(MiscStrs::RM_STAR_LABEL),

    rStaticLabel_(MiscStrs::TOUCHABLE_LARGE_STATIC_RESISTANCE_LABEL,
                           MiscStrs::TOUCHABLE_STATIC_RESISTANCE_MSG),
    rStaticValue_(TextFont::TWENTY_FOUR, 3, RIGHT),
    rStaticUnit_(GuiApp::PressUnits() == PressUnitsValue::CMH2O_UNIT_VALUE  ?
                         MiscStrs::TOUCHABLE_STATIC_RESISTANCE_UNIT :
                         MiscStrs::TOUCHABLE_STATIC_RESISTANCE_HPA_UNIT),
    rStaticNotAvail_(MiscStrs::RM_DASH_LABEL),
    rStaticMarkerText_(MiscStrs::RM_MARKER_LABEL),
    rStaticInadequateText_(MiscStrs::RM_STAR_LABEL),

    //-----------------------------------------------------------------
    // $[PA01006] -- display PAV Startup messages
    //-----------------------------------------------------------------
    pavStatusMsg_(MiscStrs::PAV_STARTUP_STATUS_LARGE_MSG),

    //-----------------------------------------------------------------
    // $[VC01008] -- display VCP/VS Startup messages
    // $[VC01009]
    //-----------------------------------------------------------------

    vcpStatusMsg_(MiscStrs::VCP_STARTUP_STATUS_LARGE_MSG),
    vsStatusMsg_(MiscStrs::VS_STARTUP_STATUS_LARGE_MSG),

    
    previousPData_(-1)
{
    CALL_TRACE("RmDataSubScreen::RmDataSubScreen(pSubScreenArea)");
    CLASS_PRE_CONDITION(NULL != pSubScreenArea);


    // Size and position the area
    setX(0);
    setY(0);
    setWidth(UpperSubScreenArea::CONTAINER_WIDTH);
    setHeight(UpperSubScreenArea::CONTAINER_HEIGHT);

    // Set subscreen background color
    setFillColor(Colors::BLACK);

    // Determine which pressure units to use.
	 
    StringId  PRESS_UNITS =
        (GuiApp::PressUnits() == PressUnitsValue::CMH2O_UNIT_VALUE)
        ? L"cmH{S:2}O"  // $[TI6.1]
        : L"hPa";       // $[TI6.2]

    // Set the Button's callback routine.
    backButton_.setButtonCallback(this);
    addDrawable(&backButton_);


    //
    // Set colors and add label fields.
    //


    //=================================================================
    // Column #1, Row #1...
    //=================================================================

    dynamicComplianceLabel_.setColor(Colors::WHITE);
    dynamicComplianceLabel_.setX(::COL1_LABEL_X_);
    dynamicComplianceLabel_.setY(::ROW1_Y_);
    addDrawable(&dynamicComplianceLabel_);

    dynamicComplianceUnit_.setColor(Colors::LIGHT_BLUE);
    dynamicComplianceUnit_.setX(::COL1_UNIT_X_);
    dynamicComplianceUnit_.setY(::ROW1_Y_);
    addDrawable(&dynamicComplianceUnit_);

    dynamicComplianceValue_.setValue(0);
    dynamicComplianceValue_.setColor(Colors::WHITE);
    dynamicComplianceValue_.setX(::COL1_VALUE_X_);
    dynamicComplianceValue_.setY(::ROW1_Y_);
    dynamicComplianceValue_.setWidth(::VALUE_WIDTH_);
    dynamicComplianceValue_.setHeight(::VALUE_HEIGHT_);
    addDrawable(&dynamicComplianceValue_);


    //-----------------------------------------------------------------
    // Column #1, Row #2...
    //-----------------------------------------------------------------


    dynamicResistanceLabel_.setColor(Colors::WHITE);
    dynamicResistanceLabel_.setX(::COL1_LABEL_X_);
    dynamicResistanceLabel_.setY(::ROW2_Y_);
    addDrawable(&dynamicResistanceLabel_);

    dynamicResistanceUnit_.setColor(Colors::LIGHT_BLUE);
    dynamicResistanceUnit_.setX(::COL1_UNIT_X_);
    dynamicResistanceUnit_.setY(::ROW2_Y_);
    addDrawable(&dynamicResistanceUnit_);

    dynamicResistanceValue_.setValue(0);
    dynamicResistanceValue_.setColor(Colors::WHITE);
    dynamicResistanceValue_.setX(::COL1_VALUE_X_);
    dynamicResistanceValue_.setY(::ROW2_Y_);
    dynamicResistanceValue_.setWidth(::VALUE_WIDTH_);
    dynamicResistanceValue_.setHeight(::VALUE_HEIGHT_);
    addDrawable(&dynamicResistanceValue_);

    //-----------------------------------------------------------------
    // Column #1, Row #3...
    //-----------------------------------------------------------------

    peakExpFlowLabel_.setColor(Colors::WHITE);
    peakExpFlowLabel_.setX(::COL1_LABEL_X_);
    peakExpFlowLabel_.setY(::ROW3_Y_);
    addDrawable(&peakExpFlowLabel_);

    peakExpFlowUnit_.setColor(Colors::LIGHT_BLUE);
    peakExpFlowUnit_.setX(::COL1_UNIT_X_);
    peakExpFlowUnit_.setY(::ROW3_Y_);
    addDrawable(&peakExpFlowUnit_);

    peakExpFlowValue_.setValue(0);
    peakExpFlowValue_.setColor(Colors::WHITE);
    peakExpFlowValue_.setX(::COL1_VALUE_X_);
    peakExpFlowValue_.setY(::ROW3_Y_);
    peakExpFlowValue_.setWidth(::VALUE_WIDTH_);
    peakExpFlowValue_.setHeight(::VALUE_HEIGHT_);
    addDrawable(&peakExpFlowValue_);

    //-----------------------------------------------------------------
    // Column #1, Row #4...
    //-----------------------------------------------------------------

    endExpFlowLabel_.setColor(Colors::WHITE);
    endExpFlowLabel_.setX(::COL1_LABEL_X_);
    endExpFlowLabel_.setY(::ROW4_Y_);
    addDrawable(&endExpFlowLabel_);

    endExpFlowUnit_.setColor(Colors::LIGHT_BLUE);
    endExpFlowUnit_.setX(::COL1_UNIT_X_);
    endExpFlowUnit_.setY(::ROW4_Y_);
    addDrawable(&endExpFlowUnit_);

    endExpFlowValue_.setValue(0);
    endExpFlowValue_.setColor(Colors::WHITE);
    endExpFlowValue_.setX(::COL1_VALUE_X_);
    endExpFlowValue_.setY(::ROW4_Y_);
    endExpFlowValue_.setWidth(::VALUE_WIDTH_);
    endExpFlowValue_.setHeight(::VALUE_HEIGHT_);
    addDrawable(&endExpFlowValue_);

    //-----------------------------------------------------------------
    // Column #1, Row #5...
    //-----------------------------------------------------------------



    //-----------------------------------------------------------------
    // Column #1, Row #6...
    //-----------------------------------------------------------------

    //-----------------------------------------------------------------
    // Column #1, Row #7...
    //-----------------------------------------------------------------


    //=================================================================
    // Column #2, Row #1...
    //=================================================================

    cStaticLabel_.setColor(Colors::WHITE);
    cStaticLabel_.setX(::COL2_LABEL_X_);
    cStaticLabel_.setY(::ROW1_Y_);
    addDrawable(&cStaticLabel_);

    cStaticUnit_.setColor(Colors::LIGHT_BLUE);
    cStaticUnit_.setX(::COL2_UNIT_X_);
    cStaticUnit_.setY(::ROW1_Y_);
    addDrawable(&cStaticUnit_);

    cStaticNotAvail_.setColor(Colors::WHITE);
    cStaticNotAvail_.setX(::COL2_ERROR_X_);
    cStaticNotAvail_.setY(::ROW1_Y_ + 5);
    addDrawable(&cStaticNotAvail_);

    cStaticMarkerText_.setColor(Colors::WHITE);
    cStaticMarkerText_.setX(::COL2_MARKER_X_);
    cStaticMarkerText_.setY(::ROW1_Y_);
    addDrawable(&cStaticMarkerText_);

    cStaticInadequateText_.setColor(Colors::WHITE);
    cStaticInadequateText_.setX(::COL2_ERROR_X_);
    cStaticInadequateText_.setY(::ROW1_Y_ + 5);
    addDrawable(&cStaticInadequateText_);

    cStaticValue_.setPrecision(ONES);
    cStaticValue_.setValue(0);
    cStaticValue_.setColor(Colors::WHITE);
    cStaticValue_.setX(::COL2_VALUE_X_);
    cStaticValue_.setY(::ROW1_Y_);
    cStaticValue_.setWidth(::VALUE_WIDTH_);
    cStaticValue_.setHeight(::VALUE_HEIGHT_);
    addDrawable(&cStaticValue_);

    //=================================================================
    // Column #2, Row #2... 
    //=================================================================

    rStaticLabel_.setColor(Colors::WHITE);
    rStaticLabel_.setX(::COL2_LABEL_X_);
    rStaticLabel_.setY(::ROW2_Y_);
    addDrawable(&rStaticLabel_);

    rStaticUnit_.setColor(Colors::LIGHT_BLUE);
    rStaticUnit_.setX(::COL2_UNIT_X_);
    rStaticUnit_.setY(::ROW2_Y_);
    addDrawable(&rStaticUnit_);

    rStaticNotAvail_.setColor(Colors::WHITE);
    rStaticNotAvail_.setX(::COL2_ERROR_X_);
    rStaticNotAvail_.setY(::ROW2_Y_ + 5);
    addDrawable(&rStaticNotAvail_);

    rStaticMarkerText_.setColor(Colors::WHITE);
    rStaticMarkerText_.setX(::COL2_MARKER_X_);
    rStaticMarkerText_.setY(::ROW2_Y_);
    addDrawable(&rStaticMarkerText_);

    rStaticInadequateText_.setColor(Colors::WHITE);
    rStaticInadequateText_.setX(::COL2_ERROR_X_);
    rStaticInadequateText_.setY(::ROW2_Y_ + 5);
    addDrawable(&rStaticInadequateText_);

    rStaticValue_.setPrecision(ONES);
    rStaticValue_.setValue(0);
    rStaticValue_.setColor(Colors::WHITE);
    rStaticValue_.setX(::COL2_VALUE_X_);
    rStaticValue_.setY(::ROW2_Y_);
    rStaticValue_.setWidth(::VALUE_WIDTH_);
    rStaticValue_.setHeight(::VALUE_HEIGHT_);
    addDrawable(&rStaticValue_);

    notAvailableLabel_.setColor(Colors::WHITE);
    notAvailableLabel_.setX( (LOWER_SUB_SCREEN_AREA_WIDTH / 2) - (notAvailableLabel_.getWidth()/2) );
    notAvailableLabel_.setY(::ROW2_Y_);
    addDrawable(&notAvailableLabel_);

    //-----------------------------------------------------------------
    // Column #2, Row #3...
    //-----------------------------------------------------------------
    intrinsicPeepLabel_.setColor(Colors::WHITE);
    intrinsicPeepLabel_.setX(::COL2_LABEL_X_);
    intrinsicPeepLabel_.setY(::ROW3_Y_);
    addDrawable(&intrinsicPeepLabel_);

    intrinsicPeepUnit_.setColor(Colors::LIGHT_BLUE);
    intrinsicPeepUnit_.setX(::COL2_UNIT_X_);
    intrinsicPeepUnit_.setY(::ROW3_Y_);
    addDrawable(&intrinsicPeepUnit_);

    intrinsicPeepValue_.setPrecision(ONES);
    intrinsicPeepValue_.setValue(0);
    intrinsicPeepValue_.setColor(Colors::WHITE);
    intrinsicPeepValue_.setX(::COL2_VALUE_X_);
    intrinsicPeepValue_.setY(::ROW3_Y_);
    intrinsicPeepValue_.setWidth(::VALUE_WIDTH_);
    intrinsicPeepValue_.setHeight(::VALUE_HEIGHT_);
    addDrawable(&intrinsicPeepValue_);

    intrinsicPeepMarkerText_.setColor(Colors::WHITE);
    intrinsicPeepMarkerText_.setX(COL2_MARKER_X_);
    intrinsicPeepMarkerText_.setY(ROW3_Y_);
    addDrawable(&intrinsicPeepMarkerText_);

    //-----------------------------------------------------------------
    // Column #2, Row #4...
    //-----------------------------------------------------------------


    //-----------------------------------------------------------------
    // Column #2, Row #5...
    //-----------------------------------------------------------------


    //-----------------------------------------------------------------
    // Column #2, Row #6...
    //-----------------------------------------------------------------
    pavStatusMsg_.setColor(Colors::WHITE);
    pavStatusMsg_.setX(::COL2_LABEL_X_);
    pavStatusMsg_.setY(::ROW6_Y_);
    addDrawable(&pavStatusMsg_);


    //-----------------------------------------------------------------
    // Column #2, Row #7...
    //-----------------------------------------------------------------
    vcpStatusMsg_.setColor(Colors::WHITE);
    vcpStatusMsg_.setX(::COL2_LABEL_X_);
    vcpStatusMsg_.setY(::ROW7_Y_);
    addDrawable(&vcpStatusMsg_);
    vcpStatusMsg_.setShow(FALSE);

    vsStatusMsg_.setColor(Colors::WHITE);
    vsStatusMsg_.setX(::COL2_LABEL_X_);
    vsStatusMsg_.setY(::ROW7_Y_);
    addDrawable(&vsStatusMsg_);
    vsStatusMsg_.setShow(FALSE);


    //=================================================================
    // Column #3, Row #1...
    //=================================================================
    peakSpontFlowLabel_.setColor(Colors::WHITE);
    peakSpontFlowLabel_.setX(::COL3_LABEL_X_);
    peakSpontFlowLabel_.setY(::ROW1_Y_);
    addDrawable(&peakSpontFlowLabel_);

    peakSpontFlowUnit_.setColor(Colors::LIGHT_BLUE);
    peakSpontFlowUnit_.setX(::COL3_UNIT_X_);
    peakSpontFlowUnit_.setY(::ROW1_Y_);
    addDrawable(&peakSpontFlowUnit_);

    peakSpontFlowValue_.setValue(0);
    peakSpontFlowValue_.setColor(Colors::WHITE);
    peakSpontFlowValue_.setX(::COL3_VALUE_X_);
    peakSpontFlowValue_.setY(::ROW1_Y_);
    peakSpontFlowValue_.setWidth(::VALUE_WIDTH_);
    peakSpontFlowValue_.setHeight(::VALUE_HEIGHT_);
    addDrawable(&peakSpontFlowValue_);

    //-----------------------------------------------------------------
    // Column #3, Row #2...
    //-----------------------------------------------------------------
    spontFvRatioLabel_.setColor(Colors::WHITE);
    spontFvRatioLabel_.setX(::COL3_LABEL_X_);
    spontFvRatioLabel_.setY(::ROW2_Y_);
    addDrawable(&spontFvRatioLabel_);

    spontFvRatioValue_.setValue(0);
    spontFvRatioValue_.setColor(Colors::WHITE);
    spontFvRatioValue_.setX(::COL3_VALUE_X_);
    spontFvRatioValue_.setY(::ROW2_Y_);
    spontFvRatioValue_.setWidth(::VALUE_WIDTH_);
    spontFvRatioValue_.setHeight(::VALUE_HEIGHT_);
    addDrawable(&spontFvRatioValue_);

    //-----------------------------------------------------------------
    // Column #3, Row #3...
    //-----------------------------------------------------------------


    //-----------------------------------------------------------------
    // Column #3, Row #4...
    //-----------------------------------------------------------------


    //-----------------------------------------------------------------
    // Column #3, Row #5...
    //-----------------------------------------------------------------


    //-----------------------------------------------------------------
    // Column #3, Row #6...
    //-----------------------------------------------------------------


    //-----------------------------------------------------------------
    // Column #3, Row #7...
    //-----------------------------------------------------------------



    // this subscreen needs to ALWAYS be attached to the accepted context
    // subject...
    attachToSubject_(ContextId::ACCEPTED_CONTEXT_ID,
                     Notification::BATCH_CHANGED);

    //
    // Register for changes to patient data values.
    //

    // Register for PEEP Recovery
    PatientDataRegistrar::RegisterTarget(PatientDataId::IS_PEEP_RECOVERY_ITEM, this);

    // Register for CDYN
    PatientDataRegistrar::RegisterTarget(PatientDataId::DYNAMIC_COMPLIANCE_ITEM,
                                         this);

    // Register for RDYN
    PatientDataRegistrar::RegisterTarget(PatientDataId::DYNAMIC_RESISTANCE_ITEM,
                                         this);
    // Register for PSF
    PatientDataRegistrar::RegisterTarget(PatientDataId::PEAK_SPONT_INSP_FLOW_ITEM, 
                                         this);

    // Register for PEF
    PatientDataRegistrar::RegisterTarget(PatientDataId::PEAK_EXPIRATORY_FLOW_ITEM, 
                                         this);

    // Register for EEF
    PatientDataRegistrar::RegisterTarget(PatientDataId::END_EXPIRATORY_FLOW_ITEM,
                                         this);

    // Register for C20/C
    PatientDataRegistrar::RegisterTarget(PatientDataId::C20_TO_C_RATIO_ITEM, this);

    // Register for F/vt
    PatientDataRegistrar::RegisterTarget(PatientDataId::SPONT_RATE_TO_VOLUME_RATIO_ITEM,
                                         this);
    // Register for PEEPi
    PatientDataRegistrar::RegisterTarget(PatientDataId::PEEP_INTRINSIC_ITEM,
                                         this);
    PatientDataRegistrar::RegisterTarget(PatientDataId::END_EXP_PAUSE_PRESSURE_STATE_ITEM, 
                                         this);

    // Register for PAV
    PatientDataRegistrar::RegisterTarget(PatientDataId::PAV_STATE_ITEM, this);

    // Register for VC+ and VS
    PatientDataRegistrar::RegisterTarget(PatientDataId::VTPCV_STATE_ITEM, this);


    // Register for C Static
    PatientDataRegistrar::RegisterTarget(PatientDataId::STATIC_LUNG_COMPLIANCE_ITEM, this);
    PatientDataRegistrar::RegisterTarget(PatientDataId::STATIC_LUNG_COMPLIANCE_VALID_ITEM, this);

    // Register for R Static
    PatientDataRegistrar::RegisterTarget(PatientDataId::STATIC_AIRWAY_RESISTANCE_ITEM, this);
    PatientDataRegistrar::RegisterTarget(PatientDataId::STATIC_AIRWAY_RESISTANCE_VALID_ITEM, this);


    // Register for GUI state change callbacks.
    GuiEventRegistrar::RegisterTarget(this);

    // Register for Breath-Delivery status events
    BdEventRegistrar::RegisterTarget(EventData::VENT_INOP, this);   
    BdEventRegistrar::RegisterTarget(EventData::PATIENT_CONNECT, this); 
    BdEventRegistrar::RegisterTarget(EventData::SVO, this); 
    BdEventRegistrar::RegisterTarget(EventData::OCCLUSION, this);
    BdEventRegistrar::RegisterTarget(EventData::APNEA_VENT, this);
    BdEventRegistrar::RegisterTarget(EventData::SAFETY_VENT, this);
	BdEventRegistrar::RegisterTarget(EventData::PROX_FAULT, this);
	BdEventRegistrar::RegisterTarget(EventData::PROX_READY, this);



    // Set default values
    isInSpontMode_      = FALSE;
    isSpontTypeApplic_  = FALSE;
    isNivActive_        = FALSE;         
    isBilevelActive_    = FALSE;
    isPaActive_         = FALSE;
    isVcpActive_        = FALSE;
    isVsActive_         = FALSE;
    isRmAvailable_      = FALSE;
    peepiInitiated_     = FALSE;
    cStatRstatInitiated_= FALSE;
    resistanceFlag_     = PauseTypes::NOT_REQUIRED;
    complianceFlag_     = PauseTypes::NOT_REQUIRED;
    pressureStability_  = PauseTypes::NOT_REQUIRED;

    // Hide PEEPi, Cstat, and Rstat $[RM12052]
    intrinsicPeepLabel_.setShow(FALSE);
    intrinsicPeepUnit_.setShow(FALSE);
    intrinsicPeepValue_.setShow(FALSE);
    intrinsicPeepMarkerText_.setShow(FALSE);

    cStaticLabel_.setShow(FALSE);
    cStaticUnit_.setShow(FALSE);
    cStaticValue_.setShow(FALSE);
    cStaticNotAvail_.setShow(FALSE);
    cStaticMarkerText_.setShow(FALSE);
    cStaticInadequateText_.setShow(FALSE);

    rStaticLabel_.setShow(FALSE);
    rStaticUnit_.setShow(FALSE);
    rStaticValue_.setShow(FALSE);
    rStaticNotAvail_.setShow(FALSE);
    rStaticMarkerText_.setShow(FALSE);
    rStaticInadequateText_.setShow(FALSE);

    isProxInop_ = FALSE;
    isProxInStartup_ = FALSE;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~RmDataSubScreen  [Destructor]
//
//@ Interface-Description
// Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  n/a
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

RmDataSubScreen::~RmDataSubScreen(void)
{
    CALL_TRACE("RmDataSubScreen::RmDataSubScreen(void)");

    // Do nothing.
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activate()
//
//@ Interface-Description
//  Prepares this subscreen for display.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
RmDataSubScreen::activate(void)
{
    CALL_TRACE("RmDataSubScreen::activate(void)");

	// do nothing
}   


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: deactivate()
//
//@ Interface-Description
// Prepares this subscreen for removal from the display.
//---------------------------------------------------------------------
//@ Implementation-Description
// Empty implementation of a pure virtual function.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
RmDataSubScreen::deactivate(void)
{
    CALL_TRACE("RmDataSubScreen::deactivate(void)");

    // Do nothing.
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: batchSettingUpdate
//
//@ Interface-Description
//  Called when batch settings in this subscreen changed.  Passed the
//  following parameters:
// >Von
//  qualifierId The context in which the change happened, either
//                  ACCEPTED or ADJUSTED.
//  pSubject    The pointer to the Setting's Context subject.

//  settingId   The enum id of the setting which changed.
// >Voff

//---------------------------------------------------------------------
//@ Implementation-Description
// We are only interested in changes to the Accepted Context so we
// ignore all others.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
RmDataSubScreen::batchSettingUpdate(
                                   const Notification::ChangeQualifier qualifierId,
                                   const ContextSubject*               pSubject,
                                   const SettingId::SettingIdType      settingId
                                   )
{
    CALL_TRACE("RmDataSubScreen::batchSettingUpdate(qualifierId, pSubject, settingId)");

    updateSettingApplicability_();

    if (qualifierId == Notification::ACCEPTED)
    {                                                   // $[TI1]
        static Boolean  HasFirstUpdateOccurred_ = FALSE;

        if (HasFirstUpdateOccurred_)
        {   // $[TI1.1] -- update already occurred, update conditionally...
            switch (settingId)
            {
                case SettingId::VENT_TYPE :
                case SettingId::PEEP_HIGH_TIME:
                case SettingId::PEEP_LOW_TIME:
                case SettingId::MODE :
                case SettingId::FIO2_ENABLED :
                case SettingId::SUPPORT_TYPE :
                case SettingId::MAND_TYPE :     // $[TI1.1.1]
                case SettingId::PROX_ENABLED:
                case SettingId::NUM_BATCH_IDS :                 // $[TI1.1.1]
                    // update the display of this screen...
                    updateShowStates_();
                    break;
                default :                                       // $[TI1.1.2]
                    // do nothing for all other setting changes...
                    break;
            }
        }
        else
        {   // $[TI1.2] -- this is first update, force an update...
            updateShowStates_();
            HasFirstUpdateOccurred_ = TRUE;
        }

        if(!isVcpActive_ && vcpStatusMsg_.getShow())
        {
            vcpStatusMsg_.setShow(FALSE);
        }
        if(!isVsActive_ && vsStatusMsg_.getShow())
        {
            vsStatusMsg_.setShow(FALSE);
        }

    }
    // $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: patientDataChangeHappened
//
//@ Interface-Description
// Handles update event notification for patient data fields.  The
// 'patientDataId' identifies which patient data value changed so that
// we know which data value to update.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
// The given 'patientDataId' value must be one of the id's associated
// with the patient data fields in this area.
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

void 
RmDataSubScreen::patientDataChangeHappened(
                                          PatientDataId::PatientItemId patientDataId)
{
    CALL_TRACE("RmDataSubScreen::patientDataChangeHappened(patientDataId)");

    updateDataValue_(patientDataId);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: guiEventHappened
//
//@ Interface-Description
// Called normally via GuiEventRegistrar when the GUI event has changed.
// Passed the id of the state which changed.
//---------------------------------------------------------------------
//@ Implementation-Description
// Just call updateShowStates_() to decide if a change in the display
// mode is needed.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
RmDataSubScreen::guiEventHappened(GuiApp::GuiAppEvent)
{
    CALL_TRACE("RmDataSubScreen::guiEventHappened(GuiApp::GuiAppEvent)");

    //
    // Call the normal updateShowStates_() routine to set the visibility of
    // the mode-sensitive patient data values.
    //
    updateShowStates_();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: bdEventHappened
//
//@ Interface-Description
// Called normally via the BdEventRegistrar when a BD event in which we are
// interested occurs, e.g. Safety Ventilation or Ventilator Inoperative.
// Passed the following parameters:
// >Von
//  eventId     The type of the BD event.
//  eventStatus The status of the BD event.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
// Ignore passed parameters and just call updateShowStates_() which will decide
// what display mode change is needed, if any.
//---------------------------------------------------------------------
//@ PreCondition                                              
//  none                                                      
//---------------------------------------------------------------------
//@ PostCondition                                             
//  none                                                      
//@ End-Method                                                
//=====================================================================

void                                                          
RmDataSubScreen::bdEventHappened(EventData::EventId eventId,
                                 EventData::EventStatus eventStatus,
                                 EventData::EventPrompt eventPrompt)
{
    CALL_TRACE("RmDataSubScreen::bdEventHappened(EventData::EventId,"
                                                "EventData::EventStatus,)"
                                                "EventData::EventPrompt eventPrompt)");
	if (EventData::PROX_FAULT == eventId ||
        EventData::SAFETY_VENT == eventId)
	{

		// When PROX is inoperative, set the isProxInop_ to remove the PROX symbols 
		// and refresh the screen.
		if(eventStatus == EventData::ACTIVE)
		{
            isProxInop_ = TRUE;
		}
		else
		{
			isProxInop_ = FALSE;
		}

	}
	else if (EventData::PROX_READY == eventId)
	{

		if(eventStatus == EventData::ACTIVE)		  
		{
            isProxInStartup_ = FALSE;
		}
		else
		{
			isProxInStartup_ = TRUE;
		}
	}

    //
    // Call the normal updateShowStates_() routine to set the visibility of
    // the mode-sensitive patient data values.
    //
    updateShowStates_();
}                                                             


#endif // SIGMA_GUI_CPU


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//        [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition   
//  none
//@ End-Method
//=====================================================================

void
RmDataSubScreen::SoftFault(const SoftFaultID  softFaultID,
                           const Uint32       lineNumber,
                           const char*        pFileName,
                           const char*        pPredicate)  
{
    CALL_TRACE("RmDataSubScreen::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
    FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, RMDATASUBSCREEN,
                            lineNumber, pFileName, pPredicate);
}


//=====================================================================
//
// Private methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateShowStates_()
//
//@ Interface-Description
// Prepares this subscreen for display.
//---------------------------------------------------------------------
//@ Implementation-Description
// Fetches the current value of 'mode' setting and show values that are
// applicable with the current settings.  The ones that are not applicable
// shall not be displayed.  Force the update of each field so that the
// correct up-to-the-second patient data values are displayed immediately when
// the subscreen is displayed.
// $[RM12401]
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
RmDataSubScreen::updateShowStates_(void)
{
    CALL_TRACE("RmDataSubScreen::updateShowStates_()");

    const ContextSubject *const  P_ACCEPTED_SUBJECT =
    getSubjectPtr_(ContextId::ACCEPTED_CONTEXT_ID);

    // Get the current settings applicability.
    updateSettingApplicability_();

    // Update the volume labels
    updateVolumeLabel_();

    if ((GuiApp::IsSettingsLockedOut())
#ifndef SIGMA_DEVELOPMENT // FORNOW ignore the flash table
        || (!GuiApp::IsBdAndGuiFlashTheSame())
#endif // SIGMA_DEVELOPMENT
        || (GuiApp::SERIAL_NUMBER_OK != GuiApp::GetSerialNumberInfoState())
        || (BdGuiEvent::GetEventStatus(EventData::VENT_INOP) ==
            EventData::ACTIVE)
        || (BdGuiEvent::GetEventStatus(EventData::PATIENT_CONNECT) !=
            EventData::ACTIVE)
        || (BdGuiEvent::GetEventStatus(EventData::SVO) ==
            EventData::ACTIVE)
        || (BdGuiEvent::GetEventStatus(EventData::OCCLUSION) ==
            EventData::ACTIVE))
    {   // Hide the patient data                    // $[TI3]

        pavStatusMsg_.setShow(FALSE);
        vcpStatusMsg_.setShow(FALSE);
        vsStatusMsg_.setShow(FALSE);

        // if NIV & Bilevel is active display the "Not available"
        // message instead of the RM data.  $[RM12031]
        if (!isRmAvailable_)
        {

            notAvailableLabel_.setShow(TRUE);
            notAvailableLabel_.setText(MiscStrs::TOUCHABLE_RM_NOT_AVAILABLE_TITLE);

            dynamicComplianceLabel_.setShow(FALSE);
            dynamicComplianceUnit_.setShow(FALSE);
            dynamicComplianceValue_.setShow(FALSE);

            dynamicResistanceLabel_.setShow(FALSE);
            dynamicResistanceUnit_.setShow(FALSE);
            dynamicResistanceValue_.setShow(FALSE);

            peakSpontFlowLabel_.setShow(FALSE);
            peakSpontFlowUnit_.setShow(FALSE);
            peakSpontFlowValue_.setShow(FALSE);

            peakExpFlowLabel_.setShow(FALSE);
            peakExpFlowUnit_.setShow(FALSE);
            peakExpFlowValue_.setShow(FALSE);

            endExpFlowLabel_.setShow(FALSE);
            endExpFlowUnit_.setShow(FALSE);
            endExpFlowValue_.setShow(FALSE);

            spontFvRatioLabel_.setShow(FALSE);
            spontFvRatioValue_.setShow(FALSE);

            intrinsicPeepLabel_.setShow(FALSE);
            intrinsicPeepUnit_.setShow(FALSE);
            intrinsicPeepValue_.setShow(FALSE);
            intrinsicPeepMarkerText_.setShow(FALSE);

            cStaticLabel_.setShow(FALSE);
            cStaticUnit_.setShow(FALSE);
            cStaticValue_.setShow(FALSE);
            cStaticNotAvail_.setShow(FALSE);
            cStaticMarkerText_.setShow(FALSE);
            cStaticInadequateText_.setShow(FALSE);

            rStaticLabel_.setShow(FALSE);
            rStaticUnit_.setShow(FALSE);
            rStaticValue_.setShow(FALSE);
            rStaticNotAvail_.setShow(FALSE);
            rStaticMarkerText_.setShow(FALSE);
            rStaticInadequateText_.setShow(FALSE);


        }
        else
        {
            // Display the RM data values and do not show the 
            // "Not Available" message.
            notAvailableLabel_.setShow(FALSE);
            notAvailableLabel_.setText(MiscStrs::TOUCHABLE_RM_NOT_AVAILABLE_TITLE);

            dynamicComplianceLabel_.setShow(TRUE);
            dynamicComplianceUnit_.setShow(FALSE);
            dynamicComplianceValue_.setShow(FALSE);

            dynamicResistanceLabel_.setShow(TRUE);
            dynamicResistanceUnit_.setShow(FALSE);
            dynamicResistanceValue_.setShow(FALSE);

            peakSpontFlowLabel_.setShow(isSpontTypeApplic_);
            peakSpontFlowUnit_.setShow(FALSE);
            peakSpontFlowValue_.setShow(FALSE);

            peakExpFlowLabel_.setShow(TRUE);
            peakExpFlowUnit_.setShow(FALSE);
            peakExpFlowValue_.setShow(FALSE);

            endExpFlowLabel_.setShow(TRUE);
            endExpFlowUnit_.setShow(FALSE);
            endExpFlowValue_.setShow(FALSE);

            spontFvRatioLabel_.setShow(isInSpontMode_);
            spontFvRatioValue_.setShow(FALSE);


            intrinsicPeepLabel_.setShow(peepiInitiated_);
            intrinsicPeepUnit_.setShow(FALSE);
            intrinsicPeepValue_.setShow(FALSE);
            intrinsicPeepMarkerText_.setShow(FALSE);

            cStaticLabel_.setShow(cStatRstatInitiated_);
            cStaticUnit_.setShow(FALSE);
            cStaticValue_.setShow(FALSE);
            cStaticNotAvail_.setShow(FALSE);
            cStaticMarkerText_.setShow(FALSE);
            cStaticInadequateText_.setShow(FALSE);

            rStaticLabel_.setShow(cStatRstatInitiated_);
            rStaticUnit_.setShow(FALSE);
            rStaticValue_.setShow(FALSE);
            rStaticNotAvail_.setShow(FALSE);
            rStaticMarkerText_.setShow(FALSE);
            rStaticInadequateText_.setShow(FALSE);


        }


    }
    else
    {

        // In safety or apnea mode, there should be no spontaneous data
        // being sent over...
        if ((BdGuiEvent::GetEventStatus(EventData::APNEA_VENT) == EventData::ACTIVE) ||
            (BdGuiEvent::GetEventStatus(EventData::SAFETY_VENT) == EventData::ACTIVE))
        {                                 
            notAvailableLabel_.setShow(FALSE);
            notAvailableLabel_.setText(MiscStrs::TOUCHABLE_RM_NOT_AVAILABLE_TITLE);

            dynamicComplianceLabel_.setShow(FALSE);
            dynamicComplianceUnit_.setShow(FALSE);
            dynamicComplianceValue_.setShow(FALSE);

            dynamicResistanceLabel_.setShow(FALSE);
            dynamicResistanceUnit_.setShow(FALSE);
            dynamicResistanceValue_.setShow(FALSE);

            peakSpontFlowLabel_.setShow(FALSE);
            peakSpontFlowUnit_.setShow(FALSE);
            peakSpontFlowValue_.setShow(FALSE);

            peakExpFlowLabel_.setShow(FALSE);
            peakExpFlowUnit_.setShow(FALSE);
            peakExpFlowValue_.setShow(FALSE);

            endExpFlowLabel_.setShow(FALSE);
            endExpFlowUnit_.setShow(FALSE);
            endExpFlowValue_.setShow(FALSE);

            spontFvRatioLabel_.setShow(FALSE);
            spontFvRatioValue_.setShow(FALSE);

            pavStatusMsg_.setShow(FALSE);
            vcpStatusMsg_.setShow(FALSE);
            vsStatusMsg_.setShow(FALSE);

            intrinsicPeepLabel_.setShow(FALSE);
            intrinsicPeepUnit_.setShow(FALSE);
            intrinsicPeepValue_.setShow(FALSE);
            intrinsicPeepMarkerText_.setShow(FALSE);
            
            cStaticLabel_.setShow(FALSE);
            cStaticUnit_.setShow(FALSE);
            cStaticValue_.setShow(FALSE);
            cStaticNotAvail_.setShow(FALSE);
            cStaticMarkerText_.setShow(FALSE);
            cStaticInadequateText_.setShow(FALSE);
            
            rStaticLabel_.setShow(FALSE);
            rStaticUnit_.setShow(FALSE);
            rStaticValue_.setShow(FALSE);
            rStaticNotAvail_.setShow(FALSE);
            rStaticMarkerText_.setShow(FALSE);
            rStaticInadequateText_.setShow(FALSE);

        }
        else  // not safety PCV or Apnea     
        {

            // if NIV, Bilevel is active display the "Not available"
            // message instead of the RM data.  $[RM12031]
            if (!isRmAvailable_)
            {

                notAvailableLabel_.setShow(TRUE);
                notAvailableLabel_.setText(MiscStrs::TOUCHABLE_RM_NOT_AVAILABLE_TITLE);

                dynamicComplianceLabel_.setShow(FALSE);
                dynamicComplianceUnit_.setShow(FALSE);
                dynamicComplianceValue_.setShow(FALSE);

                dynamicResistanceLabel_.setShow(FALSE);
                dynamicResistanceUnit_.setShow(FALSE);
                dynamicResistanceValue_.setShow(FALSE);

                peakSpontFlowLabel_.setShow(FALSE);
                peakSpontFlowUnit_.setShow(FALSE);
                peakSpontFlowValue_.setShow(FALSE);

                peakExpFlowLabel_.setShow(FALSE);
                peakExpFlowUnit_.setShow(FALSE);
                peakExpFlowValue_.setShow(FALSE);

                endExpFlowLabel_.setShow(FALSE);
                endExpFlowUnit_.setShow(FALSE);
                endExpFlowValue_.setShow(FALSE);

                spontFvRatioLabel_.setShow(FALSE);
                spontFvRatioValue_.setShow(FALSE);

                intrinsicPeepLabel_.setShow(FALSE);
                intrinsicPeepUnit_.setShow(FALSE);
                intrinsicPeepValue_.setShow(FALSE);
                intrinsicPeepMarkerText_.setShow(FALSE);

                cStaticLabel_.setShow(FALSE);
                cStaticUnit_.setShow(FALSE);
                cStaticValue_.setShow(FALSE);
                cStaticNotAvail_.setShow(FALSE);
                cStaticMarkerText_.setShow(FALSE);
                cStaticInadequateText_.setShow(FALSE);

                rStaticLabel_.setShow(FALSE);
                rStaticUnit_.setShow(FALSE);
                rStaticValue_.setShow(FALSE);
                rStaticNotAvail_.setShow(FALSE);
                rStaticMarkerText_.setShow(FALSE);
                rStaticInadequateText_.setShow(FALSE);

            }
            else // Not NIV, BILEVEL
            {
                // Display the RM data values and do not show the 
                // "Not Available" message.

                dynamicComplianceLabel_.setShow(TRUE);
                dynamicResistanceLabel_.setShow(TRUE);
                peakExpFlowLabel_.setShow(TRUE);
                endExpFlowLabel_.setShow(TRUE);

                if (isSpontTypeApplic_)
                {                                           
                    peakSpontFlowLabel_.setShow(TRUE);

                    if (isInSpontMode_)
                    {
                        spontFvRatioLabel_.setShow(TRUE);
                    }
                    else
                    {

                        spontFvRatioLabel_.setShow(FALSE);
                        spontFvRatioValue_.setShow(FALSE);
                    }  // end if(isInSpontMode_)

                    if(!isPaActive_)
                    {                    
                        pavStatusMsg_.setShow(FALSE);
                    }


                } // end if(isSpontTypeApplic_)
                else
                {
                    peakSpontFlowLabel_.setShow(FALSE);
                    peakSpontFlowUnit_.setShow(FALSE);
                    peakSpontFlowValue_.setShow(FALSE);
                    vsStatusMsg_.setShow(FALSE);
                    spontFvRatioLabel_.setShow(FALSE);
                    spontFvRatioValue_.setShow(FALSE);
                    pavStatusMsg_.setShow(FALSE);

                }

                notAvailableLabel_.setShow(FALSE);
                notAvailableLabel_.setText(MiscStrs::TOUCHABLE_RM_NOT_AVAILABLE_TITLE);

                intrinsicPeepLabel_.setShow(peepiInitiated_);
                intrinsicPeepUnit_.setShow(peepiInitiated_);
                intrinsicPeepValue_.setShow(peepiInitiated_);
                intrinsicPeepMarkerText_.setShow(pressureStability_ == PauseTypes::UNSTABLE);

                if( cStatRstatInitiated_)
                {
                      displayRstat_(resistanceFlag_);
                      displayCstat_(complianceFlag_);
                }

            }  


        }  //end if(Safety PCV or APNEA)
    }

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateSettingApplicability_
//
//@ Interface-Description
// Handles updating the flags which indicate if a 
// setting is applicable or not.
// Because of design constraints the functionality of this
// method could not be placed in the batchSettingUpdate.
//---------------------------------------------------------------------
//@ Implementation-Description
// none
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

void
RmDataSubScreen::updateSettingApplicability_(void)
{
    CALL_TRACE("RmDataSubScreen::updateSettingApplicability_(void)");

    const ContextSubject *const  P_ACCEPTED_SUBJECT = getSubjectPtr_(
                                                      ContextId::ACCEPTED_CONTEXT_ID);

    const DiscreteValue  VENT_TYPE_VALUE =
    P_ACCEPTED_SUBJECT->getSettingValue(SettingId::VENT_TYPE);
    const DiscreteValue  MODE_VALUE =
    P_ACCEPTED_SUBJECT->getSettingValue(SettingId::MODE);
    const DiscreteValue  SPONT_TYPE_VALUE =
    P_ACCEPTED_SUBJECT->getSettingValue(SettingId::SUPPORT_TYPE);
    const DiscreteValue  MAND_TYPE_VALUE =
    P_ACCEPTED_SUBJECT->getSettingValue(SettingId::MAND_TYPE);

    isInSpontMode_ = (MODE_VALUE == ModeValue::SPONT_MODE_VALUE) || (MODE_VALUE == ModeValue::CPAP_MODE_VALUE);
    isSpontTypeApplic_ = (GuiApp::IsVentStartupSequenceComplete())
                            ? (SettingContextHandle::GetSettingApplicability(
                                                    ContextId::ACCEPTED_CONTEXT_ID,
                                                    SettingId::SUPPORT_TYPE
                                                    ) 
                               != Applicability::INAPPLICABLE): FALSE;


    isNivActive_      = (VENT_TYPE_VALUE == VentTypeValue::NIV_VENT_TYPE);
    isBilevelActive_  = (MODE_VALUE == ModeValue::BILEVEL_MODE_VALUE);

    const BoundedValue  PEEP_HIGH_TIME = P_ACCEPTED_SUBJECT->getSettingValue(
                                                                SettingId::PEEP_HIGH_TIME);

    const BoundedValue  PEEP_LOW_TIME = P_ACCEPTED_SUBJECT->getSettingValue(
                                                                SettingId::PEEP_LOW_TIME);

    isPaActive_ = (isSpontTypeApplic_  &&
              SPONT_TYPE_VALUE  == SupportTypeValue::PAV_SUPPORT_TYPE);
    isVsActive_ =  (isSpontTypeApplic_  &&
              SPONT_TYPE_VALUE  == SupportTypeValue::VSV_SUPPORT_TYPE);            
    isVcpActive_ = (MAND_TYPE_VALUE == MandTypeValue::VCP_MAND_TYPE);

    isRmAvailable_ = !(isNivActive_ || isBilevelActive_);

} // end method

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateDataValue_
//
//@ Interface-Description
// Handles update event notification for patient data fields.  The
// 'patientDataId' identifies which patient data value changed so that
// we know which data value to update.
//---------------------------------------------------------------------
//@ Implementation-Description
// Updates the display of a patient data field to the current measured
// value of the patient data item.  If the value for a patient data
// field is outside the allowable range (maintained by the Patient Data
// subsystem), then flash the (clipped) numeric value.  This processing
// only occurs if this subscreen is currently displayed.
//
// $[RM12046] 
// $[RM12045]
// $[RM12047] 
// $[RM12048] 
// $[RM12049]
// $[RM12050] 
//---------------------------------------------------------------------
//@ PreCondition
// The given 'patientDataId' value must be one of the id's associated
// with the patient data fields in this area.
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

void 
RmDataSubScreen::updateDataValue_(const PatientDataId::PatientItemId patientDataId)
{
    CALL_TRACE("RmDataSubScreen::updateDataValue_(const PatientDataId::PatientItemId"
                                                        "patientDataId)");

    // Get the new value of the patient datum
    BreathDatumHandle breathDatumHandle(patientDataId);
    BoundedBreathDatum boundedDatum;
    DiscreteBreathDatum discreteDatum;

    if (PatientDataMgr::IsBoundedId(patientDataId))
    {
        boundedDatum = breathDatumHandle.getBoundedValue();
    }
    else
    {
        discreteDatum = breathDatumHandle.getDiscreteValue();

        // To eliminate multiple update for breath type item
        if (patientDataId == PatientDataId::BREATH_TYPE_ITEM)
        {
            if (previousPData_ != discreteDatum.data)
            {
                previousPData_ = discreteDatum.data;
            }
            else
            {
                return;
            }
        }
    }

    updateSettingApplicability_();

    // the timeouts of 'EXH_SPONT_MINUTE_VOL_ITEM', 'SPONT_PERCENT_TI_ITEM',
    // 'SPONT_RATE_TO_VOLUME_RATIO_ITEM' are all based on the timer of
    // 'EXH_SPONT_TIDAL_VOL', therefore get the state of that timer...
    BreathDatumHandle  exhSpontTidalVolHandle(PatientDataId::EXH_SPONT_TIDAL_VOL_ITEM);

    const BoundedBreathDatum&  EXH_SPONT_TIDAL_VOL_DATUM =
           exhSpontTidalVolHandle.getBoundedValue();

    switch (patientDataId)
    {
        //
        // C Dyn - Dynamic Compliance
        //
        case PatientDataId::DYNAMIC_COMPLIANCE_ITEM :      
            dynamicComplianceValue_.setValue(boundedDatum.data.value);
            dynamicComplianceValue_.setPrecision(boundedDatum.data.precision);

            if (!boundedDatum.timedOut  &&  dynamicComplianceLabel_.getShow())
            {
                dynamicComplianceValue_.setShow(TRUE);
                dynamicComplianceUnit_.setShow(TRUE);
            }
            else
            {
                dynamicComplianceValue_.setShow(FALSE);
                dynamicComplianceUnit_.setShow(FALSE);
            }

            // Flash the value if it's out of range
            if (boundedDatum.clippingState == BoundedValue::ABSOLUTE_CLIP)
            {
                dynamicComplianceValue_.setColor(Colors::WHITE_ON_EXTRA_DARK_GREY_BLINK);
            }
            else
            {
                dynamicComplianceValue_.setColor(Colors::WHITE);
            }
            break;

        //
        // R Dyn - Dynamic Resistance 
        //
        case PatientDataId::DYNAMIC_RESISTANCE_ITEM :  
            dynamicResistanceValue_.setValue(boundedDatum.data.value);
            dynamicResistanceValue_.setPrecision(boundedDatum.data.precision);

            if (!boundedDatum.timedOut  &&  dynamicResistanceLabel_.getShow())
            {                                           
                dynamicResistanceValue_.setShow(TRUE);
                dynamicResistanceUnit_.setShow(TRUE);
            }
            else
            {                                          
                dynamicResistanceValue_.setShow(FALSE);
                dynamicResistanceUnit_.setShow(FALSE);
            }

            // Flash the value if it's out of range
            if (boundedDatum.clippingState == BoundedValue::ABSOLUTE_CLIP)
            {                                          
                dynamicResistanceValue_.setColor(Colors::WHITE_ON_EXTRA_DARK_GREY_BLINK);
            }
            else
            {                                          
                dynamicResistanceValue_.setColor(Colors::WHITE);
            }
            break;
        //
        // PSF - Peak Spontaneous Flow
        //
        case PatientDataId::PEAK_SPONT_INSP_FLOW_ITEM: 
            peakSpontFlowValue_.setValue(boundedDatum.data.value);
            peakSpontFlowValue_.setPrecision(boundedDatum.data.precision);

            if (peakSpontFlowLabel_.getShow()  &&
                !boundedDatum.timedOut)
            {
                peakSpontFlowValue_.setShow(TRUE);
                peakSpontFlowUnit_.setShow(TRUE);
            }
            else
            {
                peakSpontFlowValue_.setShow(FALSE);
                peakSpontFlowUnit_.setShow(FALSE);

            }


            // Flash the value if it's out of range
            if (boundedDatum.clippingState == BoundedValue::ABSOLUTE_CLIP)
            {
                peakSpontFlowValue_.setColor(Colors::WHITE_ON_EXTRA_DARK_GREY_BLINK);
            }
            else
            {
                peakSpontFlowValue_.setColor(Colors::WHITE);
            }

            break;
        //
        // PEF - Peak Expiratory Flow
        //
        case PatientDataId::PEAK_EXPIRATORY_FLOW_ITEM:  
            peakExpFlowValue_.setValue(boundedDatum.data.value);
            peakExpFlowValue_.setPrecision(boundedDatum.data.precision);

            if (!boundedDatum.timedOut && peakExpFlowLabel_.getShow())
            {
                peakExpFlowValue_.setShow(TRUE);
                peakExpFlowUnit_.setShow(TRUE);
            }
            else
            {
                peakExpFlowValue_.setShow(FALSE);
                peakExpFlowUnit_.setShow(FALSE);
            }

            // Flash the value if it's out of range
            if (boundedDatum.clippingState == BoundedValue::ABSOLUTE_CLIP)
            {
                peakExpFlowValue_.setColor(Colors::WHITE_ON_EXTRA_DARK_GREY_BLINK);
            }
            else
            {
                peakExpFlowValue_.setColor(Colors::WHITE);
            }
            break;
        //
        // EEF - End of Expiratory Flow
        //
        case PatientDataId::END_EXPIRATORY_FLOW_ITEM:
            endExpFlowValue_.setValue(boundedDatum.data.value);
            endExpFlowValue_.setPrecision(boundedDatum.data.precision);

            if (!boundedDatum.timedOut && endExpFlowLabel_.getShow())
            {
                endExpFlowValue_.setShow(TRUE);
                endExpFlowUnit_.setShow(TRUE);
            }
            else
            {
                endExpFlowValue_.setShow(FALSE);
                endExpFlowUnit_.setShow(FALSE);
            }

            // Flash the value if it's out of range
            if (boundedDatum.clippingState == BoundedValue::ABSOLUTE_CLIP)
            {
                endExpFlowValue_.setColor(Colors::WHITE_ON_EXTRA_DARK_GREY_BLINK);
            }
            else
            {
                endExpFlowValue_.setColor(Colors::WHITE);
            }
            break;

        //
        // PEEPi - PEEP Intrinsic 
        //
        case PatientDataId::END_EXP_PAUSE_PRESSURE_STATE_ITEM:
            if (isRmAvailable_)
            {
                peepiInitiated_ = TRUE;

                pressureStability_ = (PauseTypes::ComplianceState) 
                                              BreathDatumHandle(PatientDataId::
                                              END_EXP_PAUSE_PRESSURE_STATE_ITEM).
                                              getDiscreteValue().data;
    
                if (pressureStability_ == PauseTypes::UNSTABLE)
                {   
                    // data is questionable -  Number and "()" is displayed
                    intrinsicPeepMarkerText_.setShow(TRUE);
                }   
                else
                {
                    intrinsicPeepMarkerText_.setShow(FALSE);
                }
            }

            boundedDatum = 
                BreathDatumHandle(PatientDataId::PEEP_INTRINSIC_ITEM).
                                                              getBoundedValue();

        case PatientDataId::PEEP_INTRINSIC_ITEM :       // PEEPi
            if (isRmAvailable_)
            {

                intrinsicPeepValue_.setValue(boundedDatum.data.value);
                intrinsicPeepValue_.setPrecision(boundedDatum.data.precision);
        
                intrinsicPeepLabel_.setShow(TRUE);
                intrinsicPeepValue_.setShow(TRUE);
                intrinsicPeepUnit_.setShow(TRUE);

    
            }
            else
            {
                intrinsicPeepLabel_.setShow(FALSE);
                intrinsicPeepValue_.setShow(FALSE);
                intrinsicPeepUnit_.setShow(FALSE);
                intrinsicPeepMarkerText_.setShow(FALSE);

            }

            break;


        //
        //  F/Vt - Spontaneous rate to volume ratio
        //
        case PatientDataId::SPONT_RATE_TO_VOLUME_RATIO_ITEM :     // F/Vt 
            spontFvRatioValue_.setValue(boundedDatum.data.value);
            spontFvRatioValue_.setPrecision(boundedDatum.data.precision);

            if (spontFvRatioLabel_.getShow()  &&
                !EXH_SPONT_TIDAL_VOL_DATUM.timedOut)
            {
                spontFvRatioValue_.setShow(TRUE);
            }
            else
            {
                spontFvRatioValue_.setShow(FALSE);
            }

            // Flash the value if it's out of range
            if (boundedDatum.clippingState == BoundedValue::ABSOLUTE_CLIP)
            {
                spontFvRatioValue_.setColor(Colors::WHITE_ON_EXTRA_DARK_GREY_BLINK);

            }
            else
            {
                spontFvRatioValue_.setColor(Colors::WHITE);
            }
            break;
        //
        //  PAV Startup Message
        //
        case PatientDataId::PAV_STATE_ITEM :
            switch (breathDatumHandle.getDiscreteValue().data)
            {
            case PavState::STARTUP :                        // $[TI6.1]
                // $[PA01006] -- display PAV Startup messages
                pavStatusMsg_.setShow(isPaActive_);
                break;
            case PavState::CLOSED_LOOP :                    // $[TI6.2]
                pavStatusMsg_.setShow(FALSE);
                break;
            }
            break;

        //
        //  VC plus or VS start up messages.
        // 
        case PatientDataId::VTPCV_STATE_ITEM :
            switch (discreteDatum.data)
            {
            case VtpcvState::STARTUP_VTPCV :
                vcpStatusMsg_.setShow(isVcpActive_);                                     
                vsStatusMsg_.setShow(isVsActive_);
                break;                                                           
            case VtpcvState::NORMAL_VTPCV :                                      
                if(vcpStatusMsg_.getShow())
                {
                    vcpStatusMsg_.setShow(FALSE);
                }
                if(vsStatusMsg_.getShow())
                {
                    vsStatusMsg_.setShow(FALSE);
                }
                break;
            default:
                // do nothing...
                break;
            }
            break;

        //
        //  Cstat - Static Lung Compliance
        //
        case PatientDataId::STATIC_LUNG_COMPLIANCE_VALID_ITEM:
            if (isRmAvailable_)
            {
                cStatRstatInitiated_ = TRUE;
    
                complianceFlag_ = (PauseTypes::ComplianceState) 
                                               BreathDatumHandle(PatientDataId::
                                               STATIC_LUNG_COMPLIANCE_VALID_ITEM).
                                               getDiscreteValue().data;    
                displayCstat_(complianceFlag_);    
    
                boundedDatum = 
                    BreathDatumHandle(PatientDataId::STATIC_LUNG_COMPLIANCE_ITEM).
                                                                  getBoundedValue();
            }
        case PatientDataId::STATIC_LUNG_COMPLIANCE_ITEM :       // Cstat
            if (isRmAvailable_)
            {
                cStaticValue_.setValue(boundedDatum.data.value);
                cStaticValue_.setPrecision(boundedDatum.data.precision);
                
            }
            else
            {
                cStaticLabel_.setShow(FALSE);
                cStaticValue_.setShow(FALSE);
                cStaticUnit_.setShow(FALSE);
                cStaticMarkerText_.setShow(FALSE);       
                cStaticNotAvail_.setShow(FALSE); 
                cStaticInadequateText_.setShow(FALSE);   

            }

            break;

        //
        //  Rstat - Static Airway Resistance
        //
        case PatientDataId::STATIC_AIRWAY_RESISTANCE_VALID_ITEM:
            if (isRmAvailable_)
            {
                cStatRstatInitiated_  = TRUE;
    
                resistanceFlag_ = (PauseTypes::ComplianceState) 
                                               BreathDatumHandle(PatientDataId::
                                               STATIC_AIRWAY_RESISTANCE_VALID_ITEM).
                                               getDiscreteValue().data;
                displayRstat_(resistanceFlag_);    
    
                boundedDatum = 
                    BreathDatumHandle(PatientDataId::STATIC_AIRWAY_RESISTANCE_ITEM).
                                                                  getBoundedValue();
            }
        case PatientDataId::STATIC_AIRWAY_RESISTANCE_ITEM :  // R Stat
            if (isRmAvailable_)
            {
                rStaticValue_.setValue(boundedDatum.data.value);
                rStaticValue_.setPrecision(boundedDatum.data.precision);
            }
            else
            {
                rStaticLabel_.setShow(FALSE);
                rStaticValue_.setShow(FALSE);
                rStaticUnit_.setShow(FALSE);
                rStaticMarkerText_.setShow(FALSE);       
                rStaticNotAvail_.setShow(FALSE); 
                rStaticInadequateText_.setShow(FALSE);   

            }

            break;

        default:
            // do nothing?..
            break;
    }  // end switch (patientDataId)

}  //end method

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: buttonDownHappened
//
//@ Interface-Description
// Handles button down events for all of the "instant action" screen
// select buttons on this subscreen.  At the moment, it is used for 
// responding to button down events on the backButton_.
//  The `pButton' parameter is the button that was pressed down.
// The `isByOperatorAction' flag is TRUE if the given button was pressed
// by operator.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Reset the backButton_ to the "up" state so that it will be in the 
//  correct state the next time this subscreen is displayed.  Deactivate the
//  this subscreen. Retrieve the more data tab button and the more data 
//  subscreen.  Then activate the More Data SubScreen.
//  The tab button is immediately set to down to resolve the problem of
//  activating a new screen automatically bouncing back up the tab button.
//---------------------------------------------------------------------
//@ PreCondition
//  isByOperatorAction must be set to TRUE, pButton cannot be NULL and
//  pButton must be a backButton_ variable.
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
RmDataSubScreen::buttonDownHappened(Button* pButton, Boolean isByOperatorAction)
{
    CALL_TRACE("RmDataSubScreen::buttonDownHappened(Button* ," 
                                           "Boolean isByOperatorAction)");

    CLASS_PRE_CONDITION(pButton != NULL);
    CLASS_PRE_CONDITION(isByOperatorAction);
    CLASS_PRE_CONDITION(pButton == &backButton_);

    MoreDataSubScreen::setLastDisplay(MOREDATASUBSCREEN);

    // Bounce the back button.
    backButton_.setToUp();

    // Deactivate/dismiss this current subscreen
    getSubScreenArea()->deactivateSubScreen();

    // Fetch the previous screen, the more data subscreen.
    SubScreen* pSubscreen = UpperSubScreenArea::GetMoreDataSubScreen();
    CLASS_ASSERTION(pSubscreen != NULL);

    //
    // Fetch the more data tab button.
    //
    TabButton * pTabButton = UpperScreen::RUpperScreen.getUpperScreenSelectArea()->
                             getMoreDataTabButton();
    CLASS_ASSERTION(pTabButton != NULL);


    SAFE_CLASS_ASSERTION(pSubscreen && pTabButton);

    // Activate the subscreen and force down the associated tab 
    // button. 
    getSubScreenArea()->activateSubScreen(pSubscreen, pTabButton);


}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: displayRstat_
//
//@ Interface-Description
// Displays Rstat.
//---------------------------------------------------------------------
//@ Implementation-Description
// It takes a resistance flag which determines the value of Rstat.  
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

void RmDataSubScreen::displayRstat_(PauseTypes::ComplianceState resistanceFlag)
{

    CALL_TRACE("RmDataSubScreen::displayRstat_(PauseTypes::ComplianceState resistanceFlag)");

    rStaticLabel_.setShow(TRUE);

    // $[BL04064] :p,q
    switch (resistanceFlag)
    {
    case PauseTypes::VALID:                         // $[TI1.1]
        // data is reliable
        rStaticValue_.setShow(TRUE);
        rStaticUnit_.setShow(TRUE);
        rStaticMarkerText_.setShow(FALSE);       
        rStaticNotAvail_.setShow(FALSE); 
        rStaticInadequateText_.setShow(FALSE);   
        break;
    case PauseTypes::OUT_OF_RANGE:
    case PauseTypes::SUB_THRESHOLD_INPUT:
    case PauseTypes::CORRUPTED_MEASUREMENT:
    case PauseTypes::EXH_TOO_SHORT:
    case PauseTypes::NOT_STABLE:                    // $[TI1.2]
        // data is questionable -  Number and "()" is displayed
        rStaticValue_.setShow(TRUE);
        rStaticUnit_.setShow(TRUE);
        rStaticMarkerText_.setShow(TRUE);       
        rStaticNotAvail_.setShow(FALSE); 
        rStaticInadequateText_.setShow(FALSE);   
        break;
    case PauseTypes::UNAVAILABLE:                   // $[TI1.3]
        // data can not be calculated - "***" and "()" is displayed    
        rStaticValue_.setShow(FALSE);
        rStaticUnit_.setShow(TRUE);
        rStaticMarkerText_.setShow(TRUE);       
        rStaticInadequateText_.setShow(TRUE);  
        rStaticNotAvail_.setShow(FALSE); 

        break;
    case PauseTypes::NOT_REQUIRED:                  // $[TI1.4]
        // data is not required for this case - display dash            

        rStaticValue_.setShow(FALSE);
        rStaticUnit_.setShow(FALSE);
        rStaticInadequateText_.setShow(FALSE);  
        rStaticMarkerText_.setShow(FALSE);       
        rStaticNotAvail_.setShow(TRUE); 
        break;
    default:
        //do nothing
        break;

    }
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: displayCstat_
//
//@ Interface-Description
// Displays Cstat.
//---------------------------------------------------------------------
//@ Implementation-Description
// It takes a compliance flag which determines the value of Cstat.  
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

void RmDataSubScreen::displayCstat_(PauseTypes::ComplianceState complianceFlag)
{

    CALL_TRACE("RmDataSubScreen::displayCstat_(PauseTypes::ComplianceState complianceFlag)");

    cStaticLabel_.setShow(TRUE);

    // $[BL04064] :p,q
    switch (complianceFlag)
    {
    case PauseTypes::VALID:                         // $[TI1.1]
        // data is reliable
        cStaticValue_.setShow(TRUE);
        cStaticUnit_.setShow(TRUE);
        cStaticMarkerText_.setShow(FALSE);       
        cStaticNotAvail_.setShow(FALSE); 
        cStaticInadequateText_.setShow(FALSE);   
        break;
    case PauseTypes::OUT_OF_RANGE:
    case PauseTypes::SUB_THRESHOLD_INPUT:
    case PauseTypes::CORRUPTED_MEASUREMENT:
    case PauseTypes::EXH_TOO_SHORT:
    case PauseTypes::NOT_STABLE:                    // $[TI1.2]
        // data is questionable -  Number and "()" is displayed
        cStaticValue_.setShow(TRUE);
        cStaticUnit_.setShow(TRUE);
        cStaticMarkerText_.setShow(TRUE);       
        cStaticNotAvail_.setShow(FALSE); 
        cStaticInadequateText_.setShow(FALSE);   
        break;
    case PauseTypes::UNAVAILABLE:                   // $[TI1.3]
        // data can not be calculated - "***" and "()" is displayed    
        cStaticValue_.setShow(FALSE);
        cStaticUnit_.setShow(TRUE);
        cStaticMarkerText_.setShow(TRUE);       
        cStaticInadequateText_.setShow(TRUE);  
        cStaticNotAvail_.setShow(FALSE); 
                    
        break;
    case PauseTypes::NOT_REQUIRED:                  // $[TI1.4]
        // data is not required for this case - display dash            

        cStaticValue_.setShow(FALSE);
        cStaticUnit_.setShow(FALSE);
        cStaticInadequateText_.setShow(FALSE);  
        cStaticMarkerText_.setShow(FALSE);       
        cStaticNotAvail_.setShow(TRUE); 

        break;

    default:
        //do nothing
        break;
    }

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateVolumeLabel_
//
//@ Interface-Description
// Handles updating the Vdot e spont label.  
// --------------------------------------------------------------------
//@ Implementation-Description
// If PROX is active add the subscript y to Vdot e spont label. 
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

void
RmDataSubScreen::updateVolumeLabel_(void)
{
    const ContextSubject *const  P_ACCEPTED_SUBJECT =
                         getSubjectPtr_(ContextId::ACCEPTED_CONTEXT_ID);
    const DiscreteValue PROX_ENABLED_VALUE =
                P_ACCEPTED_SUBJECT->getSettingValue(SettingId::PROX_ENABLED);


     Boolean isProxAvailable = (!isProxInStartup_ && (PROX_ENABLED_VALUE == ProxEnabledValue::PROX_ENABLED)
                                 && !isProxInop_ && UpperScreen::RUpperScreen.getVitalPatientDataArea()->
									           isVentNotInStartupOrWaitForPT());

    if (isProxAvailable)
    {
        spontFvRatioLabel_.setText(MiscStrs::PROX_TOUCHABLE_SPONT_FV_RATIO_LABEL);
        spontFvRatioLabel_.setMessage(MiscStrs::PROX_TOUCHABLE_SPONT_FV_RATIO_MSG);
    }
    else
    {
        spontFvRatioLabel_.setText(MiscStrs::TOUCHABLE_SPONT_FV_RATIO_LABEL);
        spontFvRatioLabel_.setMessage(MiscStrs::TOUCHABLE_SPONT_FV_RATIO_MSG);
    }

    addDrawable(&spontFvRatioLabel_);


} // end method



