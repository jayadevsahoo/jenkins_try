#pragma once

class RgnHelper
{
public:
	static void appendRgn( CRgn* pTargetRgn, const LPRECT lpRect );
	static void appendRgn( CRgn* pTargetRgn, CRgn* pRgn );
	static void substractRgn( CRgn* pTargetRgn, const LPRECT lpRect );
	static void substractRgn( CRgn* pTargetRgn, CRgn* pRgn );
	static void setEmpty( CRgn* pRgn );
	RgnHelper(void);
	~RgnHelper(void);
};
