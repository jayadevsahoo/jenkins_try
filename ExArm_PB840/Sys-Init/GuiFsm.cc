#include "stdafx.h"
//=====================================================================
//   This is a proprietary work to which Puritan-Bennett corporation
//   of California claims exclusive right.  No part of this work may
//  be used, disclosed, reproduced, sorted in an information retrieval
//     system, or transmitted by any means, electronic, mechanical,
//   photocopying, recording, or otherwise without the prior written
//      permission of Puritan-Bennett Corporation of California.
//
//          Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: GuiFsm - GUI Finite State Machine.
//---------------------------------------------------------------------
//@ Interface-Description
//  The GuiFsm contains the static instantiations of the valid 
//  FsmStates for the GUI Task-Control.  This class instantiates a 
//  single instance for each valid FsmState and provides static 
//  accessor functions to these states. 
//---------------------------------------------------------------------
//@ Rationale
//  The GuiFsm contains the FsmState instantiations for GUI task control.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Each FsmState instantiation is defined in a static function which
//  defines a concrete FsmState as its static data.  The first time the
//  static function is called, the FsmState is constructed and its
//  reference returned.  On subsequent calls to the static function,
//  only the reference is returned since the object is already
//  constructed.  The Initialize entry point provides controlled 
//  static initialization by calling each static member function
//  containing function static.  There is only one instantiation for
//  each FsmState which are accessed using the GuiFsm accessor methods. 
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Sys-Init/vcssrc/GuiFsm.ccv   25.0.4.0   19 Nov 2013 14:33:52   pvcs  $
//
//@ Modification-Log
//
//   Revision 002   By:   sah      Date: 01/13/99    DR Number: 5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to a non-inlined method.
//
//   Revision 001   By:   gdc      Date: 07/06/95    DR Number:   n/a  
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//=====================================================================

//=====================================================================
//
//      Includes...
//
//=====================================================================

#include "GuiFsm.hh"
#ifdef SIGMA_DEVELOPMENT
#  include "Ostream.hh"
#endif

//@ Usage-Classes
#include "FsmState.hh"
#include "GuiStart.hh"
#include "GuiInop.hh"
#include "GuiOnline.hh"
#include "GuiOnlineInit.hh"
#include "GuiService.hh"
#include "GuiServiceInit.hh"
#include "GuiSst.hh"
#include "GuiSstInit.hh"
#include "GuiTimeout.hh"
#include "TaskControlAgent.hh"
//@ End-Usage

//=====================================================================
//
//      Static Data...
//
//=====================================================================

//@ Code...

//=====================================================================
//
//      Public Methods...
//
//=====================================================================



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Initialize
//
//@ Interface-Description
//  GuiFsm class initialization.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The Initialize method calls each GuiFsm method containing function  
//  static to initialize their static object.  Subsequent calls to these
//  methods return a reference to the constructed static object.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
GuiFsm::Initialize(void)
{
    CALL_TRACE("Initialize(void)");
    
    // controlled initialization of statics
    GuiFsm::GetGuiStartState();
    GuiFsm::GetGuiOnlineState();
    GuiFsm::GetGuiOnlineInitState();
    GuiFsm::GetGuiServiceInitState();
    GuiFsm::GetGuiSstInitState();
    GuiFsm::GetGuiInopState();
    GuiFsm::GetGuiServiceState();
    GuiFsm::GetGuiSstState();
    GuiFsm::GetGuiTimeoutState();
    // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetGuiOnlineState
//
//@ Interface-Description
//  The GetGuiOnlineState returns a reference to the GuiOnline state.
//---------------------------------------------------------------------
//@ Implementation-Description
//  When this method is called for the first time during initialization,
//  the local static GuiOnline state is constructed and a reference to 
//  it returned.  On subsequent calls the method returns a reference to 
//  the local static only.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

const GuiOnline&
GuiFsm::GetGuiOnlineState(void)
{
    CALL_TRACE("OnlineState(void)");
    static const GuiOnline state;
    return state;
    // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetGuiInopState
//
//@ Interface-Description
//  The GetGuiInopState returns a reference to the GuiInop state.
//---------------------------------------------------------------------
//@ Implementation-Description
//  When this method is called for the first time during initialization,
//  the local static GuiInop state is constructed and a reference to it
//  returned.  On subsequent calls the method returns a reference to 
//  the local static only.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

const GuiInop&
GuiFsm::GetGuiInopState(void)
{
    CALL_TRACE("GetGuiInopState(void)");
    static const GuiInop state;
    return state;
    // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetGuiServiceState
//
//@ Interface-Description
//  The GetGuiServiceState returns a reference to the GuiService state.
//---------------------------------------------------------------------
//@ Implementation-Description
//  When this method is called for the first time during initialization,
//  the local static GuiService state is constructed and a reference to 
//  it returned.  On subsequent calls the method returns a reference to 
//  the local static only.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================


const GuiService&
GuiFsm::GetGuiServiceState(void)
{
    CALL_TRACE("GetGuiServiceState(void)");
    static const GuiService state;
    return state;
    // $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetGuiStartState
//
//@ Interface-Description
//  The GetGuiStartState returns a reference to the GuiStart state.
//---------------------------------------------------------------------
//@ Implementation-Description
//  When this method is called for the first time during initialization,
//  the local static GuiStart state is constructed and a reference to 
//  it returned.  On subsequent calls the method returns a reference to 
//  the local static only.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================


const GuiStart&
GuiFsm::GetGuiStartState(void)
{
    CALL_TRACE("GetGuiStartState(void)");
    static const GuiStart state;
    return state;
    // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetGuiSstState
//
//@ Interface-Description
//  The GetGuiSstState returns a reference to the GuiSst state.
//---------------------------------------------------------------------
//@ Implementation-Description
//  When this method is called for the first time during initialization,
//  the local static GuiSst state is constructed and a reference to 
//  it returned.  On subsequent calls the method returns a reference to 
//  the local static only.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

const GuiSst&
GuiFsm::GetGuiSstState(void)
{
    CALL_TRACE("GetGuiSstState(void)");
    static const GuiSst state;
    return state;
    // $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetGuiTimeoutState
//
//@ Interface-Description
//  The GetGuiTimeoutState returns a reference to the GuiTimeout state.
//---------------------------------------------------------------------
//@ Implementation-Description
//  When this method is called for the first time during initialization,
//  the local static GuiTimeout state is constructed and a reference to 
//  it returned.  On subsequent calls the method returns a reference to 
//  the local static only.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

const GuiTimeout&
GuiFsm::GetGuiTimeoutState(void)
{
    CALL_TRACE("GetGuiTimeoutState(void)");
    static const GuiTimeout state;
    return state;
    // $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetGuiOnlineInitState
//
//@ Interface-Description
//  The GetGuiOnlineInitState returns a reference to the GuiOnlineInit
//   state.
//---------------------------------------------------------------------
//@ Implementation-Description
//  When this method is called for the first time during initialization,
//  the local static GuiOnlineInit state is constructed and a reference to 
//  it returned.  On subsequent calls the method returns a reference to 
//  the local static only.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

const GuiOnlineInit&
GuiFsm::GetGuiOnlineInitState(void)
{
    CALL_TRACE("GetGuiOnlineInitState(void)");
    static const GuiOnlineInit state;
    return state;
    // $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetGuiServiceInitState
//
//@ Interface-Description
//  The GetGuiServiceInitState returns a reference to the GuiServiceInit
//   state.
//---------------------------------------------------------------------
//@ Implementation-Description
//  When this method is called for the first time during initialization,
//  the local static GuiServiceInit state is constructed and a reference to 
//  it returned.  On subsequent calls the method returns a reference to 
//  the local static only.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

const GuiServiceInit&
GuiFsm::GetGuiServiceInitState(void)
{
    CALL_TRACE("GetGuiServiceInitState(void)");
    static const GuiServiceInit state;
    return state;
    // $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetGuiSstInitState
//
//@ Interface-Description
//  The GetGuiSstInitState returns a reference to the GuiSstInit
//   state.
//---------------------------------------------------------------------
//@ Implementation-Description
//  When this method is called for the first time during initialization,
//  the local static GuiSstInit state is constructed and a reference to 
//  it returned.  On subsequent calls the method returns a reference to 
//  the local static only.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

const GuiSstInit&
GuiFsm::GetGuiSstInitState(void)
{
    CALL_TRACE("GetGuiSstInitState(void)");
    static const GuiSstInit state;
    return state;
    // $[TI1]
}
 

#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//
//@ Interface-Description
//   This method is called when a software fault is detected by the
//   fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//   'lineNumber' are essential pieces of information.  The 'pFileName'
//   and 'pPredicate' strings may be defaulted in the macro to reduce
//   code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This method receives the call for the SoftFault, adds it sub-system
//   and class name ID and sends the message to the non-member function
//   SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
void
GuiFsm::SoftFault(const SoftFaultID  softFaultID,
                  const Uint32       lineNumber,
                  const char*        pFileName,
                  const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
 
  FaultHandler::SoftFault(softFaultID, SYS_INIT,
                          Sys_Init::ID_GUIFSM,
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)

//=====================================================================
//
//      Protected Methods...
//
//=====================================================================

//=====================================================================
//
//      Private Methods...
//
//=====================================================================




