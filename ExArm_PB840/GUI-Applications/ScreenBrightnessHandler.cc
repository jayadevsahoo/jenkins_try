#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: ScreenBrightnessHandler - Handles setting of Screen Brightness 
// (initiated by the Screen Brightness off-screen key).
//---------------------------------------------------------------------
//@ Interface-Description
// This is a class dedicated to the setting of the screen brightness.  The
// Screen Brightness is set by pressing down the Screen Brightness offscreen key
// and, while keeping it pressed, using the rotary knob to adjust.
//
// The keyPanelPressHappened() and keyPanelReleaseHappened() methods
// inform us of Screen Brightness key events.  We use the adjustPanel's methods
// to trap knob events and valueUpdated() tells us when the
// Screen Brightness setting changes so that we can communicate this event
// to the GUI-Foundation sound system.
//---------------------------------------------------------------------
//@ Rationale
// Something needs to handle the Screen Brightness key and this is it.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply register for Screen Brightness Key event callbacks.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
// Only one instance of this class should be created.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/ScreenBrightnessHandler.ccv   25.0.4.0   19 Nov 2013 14:08:20   pvcs  $
//
//@ Modification-Log
//
//  Revision: 014   By: gdc    Date:  26-May-2007    SCR Number: 6330
//  Project: Trend
//  Description:
// 		Removed SUN prototype code.
//
//  Revision: 013   By: hlg    Date:  20-Sep-2001    DCS Number: 5947
//  Project: GUIComms
//  Description:
// 		Removed callback to display messages when using cost
// 		reduced GUI because this key is not functional.
//
//  Revision: 012   By: gdc    Date:  17-Jan-2001    DCS Number: 5493
//  Project: GuiComms
//  Description:
// 		Modified for new backlight drivers.
//
//  Revision: 011   By: hhd    Date:  16-Nov-1999    DCS Number: 5327
//  Project: Neo-Mode
//  Description:
// 		Modified as part of a general cleanup effort for Neo-Mode.
//
//  Revision: 010  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 008  By:  sah	   Date:  15-Sep-1998    DCS Number: 5133
//  Project:  Color
//  Description:
//		Added mapping for new [CL01004] and [CL02001] requirements.
//
//  Revision: 007  By:  gdc    Date:  02-Sep-1998    DCS Number: 5153
//  Project:  Color
//  Description:
//      Added traceable ID's for Color Project requirements.
//
//  Revision: 006  By: yyy      Date: 05-Jan-1998  DR Number: 
//    Project:  BiLevel (R8027)
//    Description:
//     Initial release for BiLevel adding software option.
//     Merged /840/Baseline/GUI-Applications/vcssrc/ScreenBrightnessHandler.ccv   1.24.1.0   07/30/98 10:19:32   gdc
//
//  Revision: 005  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 004  By:  gdc   Date:  18-May-1998    DCS Number:  XXXX
//  Project:  Sigma (R8027)
//  Description:
//      For color, made the screen brightness control invalid.
//
//  Revision: 003  By:  sah   Date:  13-May-1998    DCS Number:  5089
//  Project:  Sigma (R8027)
//  Description:
//      Now masking in lower 4 bits of brightness value, because of change
//      to GUI PCB for supporting color upgrade.
//
//  Revision: 002  By:  sah   Date:  12-Jun-1997    DCS Number:  2208
//  Project:  Sigma (R8027)
//  Description:
//      Modified code to incorporate new VGA interface.
//
//  Revision: 001  By:  hhd Date:  13-MAY-95    DR Number:
//  Project:  Sigma (R8027)
//  Description:
//      Integration baseline.
//
//=====================================================================
#include "ScreenBrightnessHandler.hh"

//@ Usage-Classes
#include "VgaDevice.hh"
#include "AdjustPanel.hh"
#include "BoundStrs.hh"
#include "KeyPanel.hh"
// TODO E600
//#include "OsUtil.hh"			// for ::IsUpperDisplayColor() and ::IsGuiCommsConfig()
#include "PromptArea.hh"
#include "SettingConstants.hh"
#include "Sound.hh"
#include "MiscStrs.hh"
#include "MessageArea.hh"
#include "SettingSubject.hh"
#include "BoundStatus.hh"
#include "GuiApp.hh"
//@ End-Usage

//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ScreenBrightnessHandler()  [Default Constructor]
//
//@ Interface-Description
// Creates the Screen Brightness object.
//---------------------------------------------------------------------
//@ Implementation-Description
// Register for Screen Brightness Key event callbacks and Screen Brightness 
// setting changes.  Call valueUpdated() in order to initialize the
// display with the current value of Screen Brightness.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

ScreenBrightnessHandler::ScreenBrightnessHandler(void)
{
	CALL_TRACE("ScreenBrightnessHandler::ScreenBrightnessHandler(void)");

	// Register for Screen Brightness key presses
	if (!::IsGuiCommsConfig())
	{  // $[TI3] -- only functional if button is labeled (old gui)...
		KeyPanel::SetCallback(KeyPanel::SCREEN_BRIGHTNESS_KEY, this);
	}  // $[TI4]

	if (!::IsUpperDisplayColor())
	{  // $[TI1] -- only monitor if non-color screens...
		attachToSubject_(SettingId::DISPLAY_BRIGHTNESS,
						 Notification::VALUE_CHANGED);
	}  // $[TI2] -- don't monitor if color screens...

	// Fake a setting change event so as to initialize the display with the
	// current screen brightness value.
	valueUpdate(Notification::ACCEPTED,
				getSubjectPtr_(SettingId::DISPLAY_BRIGHTNESS));
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~ScreenBrightnessHandler  [Destructor]
//
//@ Interface-Description
//	n/a	
//---------------------------------------------------------------------
//@ Implementation-Description
//	This is an empty implementation of a pure virtual function.	
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

ScreenBrightnessHandler::~ScreenBrightnessHandler(void)
{
	CALL_TRACE("ScreenBrightnessHandler::~ScreenBrightnessHandler(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: keyPanelPressHappened
//
//@ Interface-Description
// Called when the Screen Brightness key is pressed.  We begin the Screen 
// Brightness adjustment process.
//---------------------------------------------------------------------
//@ Implementation-Description
// We grab the Adjust Panel focus (for knob events), and post appropriate 
// prompts.  Display the offscreen key help message.
// $[01239] While the display brightness key is held down, the gui ...
// $[CL02001] Display brightness control available for grey-scale only.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
ScreenBrightnessHandler::keyPanelPressHappened(KeyPanel::KeyId)
{
	CALL_TRACE("keyPanelPressHappened(KeyPanel::KeyId)");

	// This is a key press.  Grab the Adjust Panel focus
	AdjustPanel::TakePersistentFocus(this);

	// $[CL02001] option available for grey-scale only
	if ( !IsUpperDisplayColor() )
	{													// $[TI1.1]
		// Display a Primary and Secondary prompt, and clear Advisory prompt
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY,
					PromptArea::PA_HIGH, PromptStrs::USE_KNOB_TO_ADJUST_P);
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
					PromptArea::PA_HIGH, PromptStrs::SCREEN_BRIGHTNESS_CANCEL_S);
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_ADVISORY,
					PromptArea::PA_HIGH, PromptStrs::EMPTY_A);
	}
	else
	{													// $[TI1.2]
        // Display Primary and Secondary prompts, and clear Advisory prompt
        // not available on color screens
        GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY,
                    PromptArea::PA_HIGH, PromptStrs::EMPTY_A);
        GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
                    PromptArea::PA_HIGH, PromptStrs::SCREEN_BRIGHTNESS_NOT_AVAILABLE_A);
        GuiApp::PPromptArea->setPrompt(PromptArea::PA_ADVISORY,
                    PromptArea::PA_HIGH, PromptStrs::EMPTY_A);
 
        // Make invalid sound
        Sound::Start(Sound::INVALID_ENTRY);
	}

	// Display the offscreen key help message 
	GuiApp::PMessageArea->setMessage(
							MiscStrs::SCREEN_BRIGHTNESS_HELP_MESSAGE,
									MessageArea::MA_HIGH);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: keyPanelReleaseHappened
//
//@ Interface-Description
// Called when the Screen Brightness key is released.  Stop the Screen 
// Brightness adjustment.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Call the AdjustPanel function TakePersistentFocus to clear the focus. 		
//	Simply turn off the offscreen help message.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ScreenBrightnessHandler::keyPanelReleaseHappened(KeyPanel::KeyId)
{
	CALL_TRACE("keyPanelReleaseHappened(KeyPanel::KeyId)");

	// Clear the Adjust Panel focus
	AdjustPanel::TakePersistentFocus(NULL);

	// Clear the off screen key message
	GuiApp::PMessageArea->clearMessage(MessageArea::MA_HIGH);
													//$[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelKnobDeltaHappened
//
//@ Interface-Description
// This is a virtual method inherited from being an AdjustPanelTarget.  If this
// object has the Adjust Panel focus (as we do during the Screen Brightness
// adjustment) then it will be called when the operator turns the knob.  It is
// passed an integer corresponding to the amount the knob was turned.  We
// inform the Settings-Validation subsystem of the delta value and any
// resultant hard bound violation is displayed in the Prompt Area.
//---------------------------------------------------------------------
//@ Implementation-Description
// Any deltas received by this button are sent immediately to the setting via
// the calcNewValue() method of ScreenBrightnessHandle_.  If the delta causes this
// setting to change then an update will be passed by the Settings-Validation
// subsystem back to us via the SettingObserver before the calcNewValue()
// method returns.  The return value of calcNewValue() indicates the current
// hard bound status of the Screen Brightness setting.  If there is a "hard bound
// violation" we will display an Advisory message in the Prompt Area.
//
// $[01238] The gui shall put the new display brightness setting into ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ScreenBrightnessHandler::adjustPanelKnobDeltaHappened(Int32 delta)
{
	CALL_TRACE("ScreenBrightnessHandler::adjustPanelKnobDeltaHappened(Int32 delta)");

	if ( !::IsUpperDisplayColor() )
	{														// $[TI3.1]
		SettingSubject*  pSubject =
								getSubjectPtr_(SettingId::DISPLAY_BRIGHTNESS);

		// Pass the knob delta on to the Screen Brightness setting
		const BoundStatus&  boundStatus = pSubject->calcNewValue(delta);

		// Get the string id appropriate to the hard bound id (if any)
		StringId hardBoundStr =
					BoundStrs::GetSettingBoundString(boundStatus.getBoundId());

		// Display the Advisory message corresponding to the hard bound id
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_ADVISORY, 
						PromptArea::PA_HIGH,
						hardBoundStr == NULL_STRING_ID ?
						PromptStrs::EMPTY_A : hardBoundStr);
	
		// If a hard bound was struck then make the Invalid Entry sound
		if (hardBoundStr != NULL_STRING_ID)
		{													// $[TI1]
			Sound::Start(Sound::INVALID_ENTRY);
		}													// $[TI2]
	}
	else
	{														// $[TI3.2]
		// control not applicable to color
		Sound::Start(Sound::INVALID_ENTRY);
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelAcceptPressHappened
//
//@ Interface-Description
// This is a virtual method inherited from being an AdjustPanelTarget.  It is
// called when the operator presses down the Accept key when we have the
// Adjust Panel focus.  This is an invalid operation so sound the Invalid
// Entry sound.
//---------------------------------------------------------------------
//@ Implementation-Description
// Tell GUI-Foundation's Sound class to make the Invalid Entry sound.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ScreenBrightnessHandler::adjustPanelAcceptPressHappened(void)
{
	CALL_TRACE("ScreenBrightnessHandler::adjustPanelAcceptPressHappened(void)");

	// Make the Invalid Entry sound
	Sound::Start(Sound::INVALID_ENTRY);					// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelClearPressHappened
//
//@ Interface-Description
// This is a virtual method inherited from being an AdjustPanelTarget.  It is
// called when the operator presses the Clear key.  This is an invalid
// operation so sound the Invalid Entry sound.
//---------------------------------------------------------------------
//@ Implementation-Description
// Tell GUI-Foundation's Sound class to make the Invalid Entry sound.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ScreenBrightnessHandler::adjustPanelClearPressHappened(void)
{
	CALL_TRACE("ScreenBrightnessHandler::adjustPanelClearPressHappened(void)");

	// Make the Contact sound
	Sound::Start(Sound::INVALID_ENTRY);					// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelLoseFocusHappened
//
//@ Interface-Description
// This is a virtual method inherited from being an AdjustPanelTarget.
// It is called when the Screen Brightness is about to lose the focus of
// the Adjust Panel.  This normally happens when some other button is
// pressed down or when the Screen Brightness key is pressed up.  We must
// terminate the Screen Brightness adjustment.
//---------------------------------------------------------------------
//@ Implementation-Description
// We tell GUI-Foundation's Sound class to stop sounding the Screen Brightness
// tone and remove any prompts we may have displayed.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ScreenBrightnessHandler::adjustPanelLoseFocusHappened(void)
{
	CALL_TRACE("ScreenBrightnessHandler::adjustPanelLoseFocusHappened(void)");

	// Cancel the Screen Brightness (call the GUI-IO-Devices subsystem's function)

	// Clear the Primary, Secondary and Advisory prompts
	GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY,
										PromptArea::PA_HIGH, NULL_STRING_ID);
	GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
										PromptArea::PA_HIGH, NULL_STRING_ID);
	GuiApp::PPromptArea->setPrompt(PromptArea::PA_ADVISORY,
										PromptArea::PA_HIGH, NULL_STRING_ID);
														// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: valueUpdate
//
//@ Interface-Description
// Called to update the display with the current screen brightness value. 
// Passed the following parameters:
// >Von
//	qualifierId	The context in which the change happened, either
//  pSubject	Pointer to the Setting's Context Subject.
// >Voff

//---------------------------------------------------------------------
//@ Implementation-Description
//  Bring the current brightness value to scale before setting it in
//  VgaDevice subsystem.
//  
//---------------------------------------------------------------------
//@ PreCondition
//  Setting id is valid.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ScreenBrightnessHandler::valueUpdate(
								const Notification::ChangeQualifier qualifierId,
							    const SettingSubject*               pSubject
									)
{
	CALL_TRACE("valueUpdate(qualifierId, pSubject)");

	AUX_CLASS_PRE_CONDITION((SettingId::DISPLAY_BRIGHTNESS == pSubject->getId()),
						    pSubject->getId());

	// Get the current value of the Display Brightness setting
	const SequentialValue  BRIGHTNESS_VALUE = pSubject->getAcceptedValue();
 
	VgaDevice::SetBrightness(BRIGHTNESS_VALUE, BRIGHTNESS_VALUE);
}  // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
ScreenBrightnessHandler::SoftFault(const SoftFaultID  softFaultID,
								    const Uint32       lineNumber,
								    const char*        pFileName,
								    const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, 
							SCREENBRIGHTNESSHANDLER,
							lineNumber, pFileName, pPredicate);
}
