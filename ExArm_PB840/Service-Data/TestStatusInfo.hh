
#ifndef TestStatusInfo_HH
#define TestStatusInfo_HH

#if		defined(SIGMA_GUI_CPU) || defined(SIGMA_COMMON)
//====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//=====================================================================
// Class:  TestStatusInfo - Service mode test result information processing
//                          information for status test result type.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Data/vcssrc/TestStatusInfo.hhv   25.0.4.0   19 Nov 2013 14:21:06   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah     Date:  15-Jan-99   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' to a non-inlined method.
//
//  Revision: 001  By:  yyy    Date:  16-May-95   DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//====================================================================

#include "Sigma.hh"
#include "ServiceDataClassIds.hh"
#include "TestDataId.hh"
#include "TestInfo.hh"
#include "TestDatum.hh"


class TestStatusInfo : public TestInfo
{
	//@ Friend: ServiceDataMgr.
	// Allows ServiceDataMgr class to call the 'modifyData_' private method
	// when it receives new data from the network.
	friend class ServiceDataMgr;

public:
    TestStatusInfo(TestDataId::TestDataItemId id);
    ~TestStatusInfo(void);
    
    static void  SoftFault(const SoftFaultID softFaultID, 
				  const Uint32      lineNumber, 
				  const char*       pFileName = NULL, 
				  const char*       pBoolTest = NULL);
				  
protected:
	// Processes a raw breath data value and maintains the modified data
	// in its buffer. 
    virtual void modifyData_(void *pValue);

private:
    TestStatusInfo(void); 						   	// not implemented ...
    TestStatusInfo(const TestStatusInfo& range);		// not implemented ...
    void  operator=(const TestStatusInfo&);			// not implemented...

};


#endif		//defined(SIGMA_GUI_CPU) || defined(SIGMA_COMMON)

#endif  // TestStatusInfo_HH
