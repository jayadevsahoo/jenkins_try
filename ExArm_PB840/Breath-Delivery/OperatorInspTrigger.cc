#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: OperatorInspTrigger - Triggers inspiration based on operator request.
//---------------------------------------------------------------------
//@ Interface-Description
//    This class is enabled when the operator has requested a manual
//    inspiration.  This trigger is kept on every list contained 
//    in the BreathTriggerMediator. This class is derived from 
//    PeepRecoveryInspTrigger.
//---------------------------------------------------------------------
//@ Rationale
//    This class implements operator's request to trigger an inspiration 
//---------------------------------------------------------------------
//@ Implementation-Description
//    This class contains a boolean state variable to keep track of whether 
//    this trigger is enabled or not.  If the trigger is not enabled, it  
//    will always return a state of false.  If it is enabled, the 
//    breath phase restricted condition is checked by the trigger.
//    The method triggerAction is responsible for setting the status of the
//    user event to ACTIVE if the trigger condition is true, or REJECTED if
//    the trigger condition is false.  This method must also disable the
//    trigger regardless of the trigger condition.
//---------------------------------------------------------------------
//@ Fault-Handling
//    n/a.
//---------------------------------------------------------------------
//@ Restrictions
//    Only one instance of this trigger may be created.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/OperatorInspTrigger.ccv   25.0.4.0   19 Nov 2013 13:59:56   pvcs  $
//
//@ Modification-Log
//
//  Revision: 008  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 007  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 006  By:  iv    Date:  01-Feb-1997    DR Number: NONE
//       Project:  Sigma (840)
//       Description:
//             Rework per Breath Delivery formal code review.
//
//  Revision: 005  By:  sp    Date:  31-Oct-1996    DR Number: NONE
//       Project:  Sigma (R8027)
//       Description:
//             Inspection rework.
//
//  Revision: 004  By:  sp    Date:  26-Sept-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Removed method triggerCondition_ and derived from PeepRecoveryInspTrigger.
//
//  Revision: 003  By:  sp    Date:  4-Mar-1996    DR Number: DCS 674
//       Project:  Sigma (R8027)
//       Description:
//             Remove #ifndef.
//
//  Revision: 002  By:  kam   Date:  29-Sep-1995    DR Number: DCS 600 
//       Project:  Sigma (R8027)
//       Description:
//             Updated for BD-GUI user event request and event status interface.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: NONE 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "OperatorInspTrigger.hh"

//@ Usage-Classes
#  include "BreathRecord.hh"

#  include "UiEvent.hh"

#  include "BreathMiscRefs.hh"

#  include "BreathPhaseScheduler.hh"
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: OperatorInspTrigger()  
//
//@ Interface-Description
//	Default Contstructor
//---------------------------------------------------------------------
//@ Implementation-Description
//      Initialize triggerId_ using PeepRecoveryInspTrigger's constructor.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

OperatorInspTrigger::OperatorInspTrigger(void) 
: PeepRecoveryInspTrigger(Trigger::OPERATOR_INSP)
{
  CALL_TRACE("OperatorInspTrigger()");

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~OperatorInspTrigger()  
//
//@ Interface-Description
//	Destructor
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

OperatorInspTrigger::~OperatorInspTrigger(void)
{
  CALL_TRACE("~OperatorInspTrigger()");

}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
OperatorInspTrigger::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, OPERATORINSPTRIGGER,
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)



//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: triggerAction_
//
//@ Interface-Description
//      This method takes the state of the trigger condition as an 
//      argument and has no return value.
//      Called by determineState to manage breath transition.
// $[01247] $[01248] 
//---------------------------------------------------------------------
//@ Implementation-Description
//	First the trigger is disabled.
//      If the trigger condition passed in is true, determineBreathPhase
//      method is called to manage the breath or phase transition 
//      (eg exhalation to inspiration). Set manual insp event according 
//	to the trigger condition.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
OperatorInspTrigger::triggerAction_(const Boolean trigCond)
{
  CALL_TRACE("triggerAction_(const Boolean trigCond)");
 
  disable();
  if (trigCond)
  {
    // $[TI1]
    RManualInspEvent.setEventStatus(EventData::ACTIVE);
    (BreathPhaseScheduler::GetCurrentScheduler()).determineBreathPhase(*this);
  }
  else
  {
    // $[TI2]
    RManualInspEvent.setEventStatus(EventData::REJECTED);
  }
}


//=====================================================================
//
//  Private Methods...
//
//=====================================================================
