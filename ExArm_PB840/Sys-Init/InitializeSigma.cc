#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1994-1995, Puritan-Bennett Corporation
//=====================================================================

// =========================== M O D U L E   D E S C R I P T I O N ====
//@ Filename: InitializeSigma.cc - Hardware and system initialization
//---------------------------------------------------------------------
//@ Interface-Description
//  This function is sys.entry_point2 in Os/*/Os.def.  VRTX supplies
//  this hook (sys.entry_point2) to allow user initialization
//  prior to the beginning of VRTX execution (ie: vrtx_go).
//---------------------------------------------------------------------
//@ Rationale
//---------------------------------------------------------------------
//@ Implementation-Description
//  Called by VRTX prior to vrtx_go to initialize the Sigma hardware
//  and create the init task which initializes the software subsystems
//  and starts all other Sigma tasks.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Sys-Init/vcssrc/InitializeSigma.ccv   25.0.4.0   19 Nov 2013 14:33:54   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By:  gdc    Date:  12-Dec-2008    SCR Number: 6068
//  Project:  840S
//  Description:
//		Removed SUN prototype code.
//
//  Revision: 1.6	By: jdm	Date: 3/14/95	DR Number:
//	Project:  Sigma (R8027)
//	Description:
//		Initial version to coding standards (Integration baseline).
//=====================================================================

#include <stdio.h>

//TODO E600 #include "OsUtil.hh"
//TODO E600 #include "Exception.hh"
#include "Post.hh"

//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function:  InitializeSigma - Initialize target hardware/application
//
//@ Interface-Description
//  This function is called at the end of OS bootup and initialization.
//  It initializes the CPU boards using the BoardInit() function in
//  the Operating-System subsystem.  It then calls Sys_Init::Create()
//  to create the init task.  Upon returning from this function,
//  vrtx_go() is called to commence tasking.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Free-Function
//=====================================================================

//TODO E600 delete this qhole file..
//extern "C" void
//InitializeSigma()
//{	
//    // run POST Phase 3 Tests
//	//TODO E600 do we need this? Or rewrite POST for what is needed
//    //Post::Phase3Exec();
//
//    // install Sigma exception handlers
////TODO E600 port    Exception::InstallHandlers();
//
//    // generic board initialization
//#ifdef E600_840_TEMP_REMOVED
//    BoardInit();
//#endif
//
//    // initialize/create init task
//    //TODO E600 relocated Sys_Init::Create to main
//}

