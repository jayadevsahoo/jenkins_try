#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: UiEventBuffer - circular buffer for user interface events.
//---------------------------------------------------------------------
//@ Interface-Description
//  The class manages a buffer to store and process user events. It provides
//  services such as add message, read message, check if buffer is empty, and
//  reset buffer. It holds user events in a user event buffer. It
//  tracks the write and read operations to determine buffer status.
//---------------------------------------------------------------------
//@ Rationale
//  The class provides the UiEvent class with the storage and methods 
//  required to	manage user events as they are sent by the GUI. 
//---------------------------------------------------------------------
//@ Implementation-Description
//  A buffer UE_BUF_SIZE size of EventData is implemented together with a
//  read and a write index. The state of these indices are effected by operations
//  such as reset, add, and read. When the values of the two indices are equal,
//  the buffer is considered empty.
//---------------------------------------------------------------------
//@ Fault-Handling
//	n/a.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header::   /840/Baseline/Breath-Delivery/vcssrc/UiEventBuffer.ccv   10.7   08/17/07 09:44:58   pvcs  
//
//@ Modification-Log
//
//  Revision: 002  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001  By:  iv    Date:  01-Feb-1997    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Initial version - per Breath Delivery formal code review 
//=====================================================================

# include "UiEventBuffer.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//@ Constant: MAX_NUM_MSGS
// The maximum number of messages allowed to accumulate in the buffer
// before an error is declared.
const Int32 MAX_NUM_MSGS = UE_BUF_SIZE - 1;

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: UiEventBuffer()  
//
//@ Interface-Description
//	Default Constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

UiEventBuffer::UiEventBuffer(void)
{
// $[TI1]
  CALL_TRACE("UiEventBuffer()");

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~UiEventBuffer()  
//
//@ Interface-Description
//	Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

UiEventBuffer::~UiEventBuffer(void)
{
  CALL_TRACE("~UiEventBuffer()");

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: addMsg
//
//@ Interface-Description
//  The method accepts an EventData type and returns nothing.
//  The method is called by only one lower priority task and may be interrupted by
//  the main BD task. The implementation makes sure that data integrity is not
//  affected when the call is interrupted.   
//---------------------------------------------------------------------
//@ Implementation-Description
//  The writeIndex_ is used to index the userEventMsg_ array to store the
//  eventData passed in. A tempIndex is used to allow an atomic, uninterruptable
//  assignment operation to writeIndex_.
//---------------------------------------------------------------------
//@ PreCondition
//  numMsgs_() <= MAX_NUM_MSGS
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
UiEventBuffer::addMsg(const EventData eventData)
{
// $[TI1]
	CALL_TRACE("UiEventBuffer::addMsg(const EventData eventData)");

    Int32 tempIndex = writeIndex_;
    userEventMsg_[writeIndex_] = eventData;
    incrementIndex_(tempIndex);
    // update writeIndex_ in one indivisible instruction
    writeIndex_ = tempIndex;
    CLASS_ASSERTION(numMsgs_() <= MAX_NUM_MSGS);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: readCurrentMsg
//
//@ Interface-Description
//  The method accepts an EventData& type and returns a Boolean.
//  The Boolean variable returns, indicates whether the buffer is empty
//  or not. The method is called as part of the highest priority task
//  thread to read the current user event.    
//---------------------------------------------------------------------
//@ Implementation-Description
//  If the buffer is not empty, the eventData is read using the readIndex_,
//  and a FALSE status is returned indicating that the buffer was not empty.
//  If the buffer is empty, a TRUE status is returned.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean 
UiEventBuffer::readCurrentMsg(EventData& eventData)
{
// $[TI1]
	CALL_TRACE("UiEventBuffer::readCurrentMsg(EventData& eventData)");

    Boolean isEmptyStatus;

    if ( !isEmpty_() )
    {// $[TI1.1]
        eventData = userEventMsg_[readIndex_];
        incrementIndex_(readIndex_);
        isEmptyStatus = FALSE;
    }
    else
    {// $[TI1.2]
        isEmptyStatus = TRUE;
    }
    return (isEmptyStatus);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
UiEventBuffer::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, UIEVENTBUFFER,
                          lineNumber, pFileName, pPredicate);
}



//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//=====================================================================
//
//  Private Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: numMsgs_
//
//@ Interface-Description
//  The method takes no argumnets and returns an Int32, which indicates
//  the number of messages in the buffer. 
//---------------------------------------------------------------------
//@ Implementation-Description
//  The value of the number of messages between writeIndex_ and readIndex_ 
//  are returned.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Int32
UiEventBuffer::numMsgs_(void) const
{
// $[TI1]
    CALL_TRACE("UiEventBuffer::numMsgs_(void)");

    Int32 diff = writeIndex_ - readIndex_;
    if (diff < 0)
    {// $[TI1.1]
       diff = diff + UE_BUF_SIZE;  
    }// $[TI1.2]

    return (diff);
}









