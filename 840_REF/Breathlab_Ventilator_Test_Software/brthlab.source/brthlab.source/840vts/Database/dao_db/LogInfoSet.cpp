// LogInfoSet.cpp : implementation file
//

#include "stdafx.h"
#include "daocpvtdb.h"
#include "LogInfoSet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLogInfoSet

IMPLEMENT_DYNAMIC(CLogInfoSet, CDaoRecordset)

CLogInfoSet::CLogInfoSet(CDaoDatabase* pdb)
	: CDaoRecordset(pdb)
{
	//{{AFX_FIELD_INIT(CLogInfoSet)
	m_ID = 0;
	m_Line = _T("");
	m_Header = 0;
	m_nFields = 3;
	//}}AFX_FIELD_INIT
	m_nDefaultType = dbOpenDynaset;
}


CString CLogInfoSet::GetDefaultDBName()
{
	return _T(DB_PATH_NAME);
}

CString CLogInfoSet::GetDefaultSQL()
{
	return _T("[LogInfo]");
}

void CLogInfoSet::DoFieldExchange(CDaoFieldExchange* pFX)
{
	//{{AFX_FIELD_MAP(CLogInfoSet)
	pFX->SetFieldType(CDaoFieldExchange::outputColumn);
	DFX_Long(pFX, _T("[ID]"), m_ID);
	DFX_Text(pFX, _T("[Line]"), m_Line);
	DFX_Long(pFX, _T("[Header]"), m_Header);
	//}}AFX_FIELD_MAP
}

/////////////////////////////////////////////////////////////////////////////
// CLogInfoSet diagnostics

#ifdef _DEBUG
void CLogInfoSet::AssertValid() const
{
	CDaoRecordset::AssertValid();
}

void CLogInfoSet::Dump(CDumpContext& dc) const
{
	CDaoRecordset::Dump(dc);
}
#endif //_DEBUG
