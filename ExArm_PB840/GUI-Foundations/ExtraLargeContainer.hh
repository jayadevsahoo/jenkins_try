#ifndef ExtraLargeContainer_HH
#define ExtraLargeContainer_HH
//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//====================================================================
//====================================================================
// Class: ExtraLargeContainer - A Container containing an extra large
//                              of Drawables.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Foundations/vcssrc/ExtraLargeContainer.hhv   25.0.4.0   19 Nov 2013 14:11:42   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001   By: gdc    Date:  23-Jan-2007    SCR Number: 6237
//  Project: TREND
//  Description:
//	Initial version.
//
//====================================================================

#include "Container.hh"
#include "TemplateMacros.hh"
#include "SListR_Drawable_XL_DRAWABLE_LIST_NUM.hh"

class ExtraLargeContainer : public Container
{
public:
	ExtraLargeContainer(void);
	virtual ~ExtraLargeContainer(void);

private:
	ExtraLargeContainer(const ExtraLargeContainer&);		// Declared, not implemented.
	void operator=(const ExtraLargeContainer&);	// Declared, not implemented.

	//@ Data-Member: children_
	// A linked list which holds references to all of the contained
	// Drawables.
	FixedSListR(Drawable, XL_DRAWABLE_LIST_NUM) children_;
};

#endif // ExtraLargeContainer_HH 
