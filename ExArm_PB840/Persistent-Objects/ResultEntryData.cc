#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sotred in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//============================ M O D U L E   D E S C R I P T I O N ====
//@ Class: ResultEntryData - Data for a result entry.
//---------------------------------------------------------------------
//@ Interface-Description
//  This class provides a storage for the data that is needed for each
//  result entry within a result table.  Instances of this class are
//  double-buffered within 'ResultTableEntry' for storage within the
//  various result tables.
//---------------------------------------------------------------------
//@ Rationale
//  A concrete class containing the data needed for EST/SST test results,
//  is required.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Persistent-Objects/vcssrc/ResultEntryData.ccv   25.0.4.0   19 Nov 2013 14:18:54   pvcs  $
//
//@ Modification-Table
//
//  Revision: 002  By:  sah    Date:  01-Jan-1999    DCS Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' to a non-inlined method.
//	Also, as part of making all 'SoftFault()' methods non-inlined,
//	I'm also making those 'SoftFault()' methods that are NOT called
//	in PRODUCTION mode, DEVELOPMENT-only.
//
//  Revision: 001  By:  sah    Date:  15-Mar-1997    DCS Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//          Initial version (Integration baseline).
//
//=====================================================================

#include "ResultEntryData.hh"

//@ Usage-Classes...
#include "TimeStamp.hh"
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ResultEntryData()  [Copy Constructor]
//
//@ Interface-Description
//  Construct an instance of this class, using 'entryData' for
//  initialization.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ResultEntryData::ResultEntryData(const ResultEntryData& entryData)
{
  CALL_TRACE("ResultEntryData(entryData)");
  
  // copy 'entryData'...
  operator=(entryData);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ResultEntryData()  [Default Constructor]
//
//@ Interface-Description
//  Construct a default log entry.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ResultEntryData::ResultEntryData(void)
			     : isCompleted_(FALSE),
			       testStartSequenceNumber_(0u),
			       resultOfPrevRun_(::UNDEFINED_TEST_RESULT_ID),
			       resultOfCurrRun_(::UNDEFINED_TEST_RESULT_ID)
{
  CALL_TRACE("ResultEntryData()");
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ~ResultEntryData()  [Destructor]
//
//@ Interface-Description
//  Destroy this log entry.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ResultEntryData::~ResultEntryData(void)
{
  CALL_TRACE("~ResultEntryData()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  saveCurrValues()  [Copy Constructor]
//
//@ Interface-Description
//  Save the "current" values into the "previous" slots.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  (getResultOfCurrRun() == getResultOfPrevRun()  &&
//   getTimeOfCurrRun()   == getTimeOfPrevRun())
//@ End-Method
//=====================================================================

void
ResultEntryData::saveCurrValues(void)
{
  CALL_TRACE("saveCurrValues()");
  
  resultOfPrevRun_  = resultOfCurrRun_;
  timeOfPrevResult_ = timeOfCurrResult_;
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  operator=(entryData)  [Copy Constructor]
//
//@ Interface-Description
//  Copy the values of 'entryData' into this instance.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
ResultEntryData::operator=(const ResultEntryData& entryData)
{
  CALL_TRACE("operator=(entryData)");
  
  isCompleted_             = entryData.isCompleted_;
  testStartSequenceNumber_ = entryData.testStartSequenceNumber_;
  resultOfPrevRun_         = entryData.resultOfPrevRun_;
  timeOfPrevResult_        = entryData.timeOfPrevResult_;
  resultOfCurrRun_         = entryData.resultOfCurrRun_;
  timeOfCurrResult_        = entryData.timeOfCurrResult_;
}   // $[TI1]


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                              [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//  and 'lineNumber' are essential pieces of information.  The
//  'pFileName' and 'pPredicate' strings may be defaulted in the macro
//  to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
ResultEntryData::SoftFault(const SoftFaultID  softFaultID,
			    const Uint32       lineNumber,
			    const char*        pFileName,
			    const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, PERSISTENT_OBJS, RESULT_TABLE_ENTRY,
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)


//=====================================================================
//
//  Friend Functions...
//
//=====================================================================

#if defined(SIGMA_DEVELOPMENT)

//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
// Free-Function: operator<<(ostr, entryData)  [friend]
//
// Interface-Description
//  Dump the contents of 'entryData' out to 'ostr'.
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Free-Function
//=====================================================================

Ostream&
operator<<(Ostream& ostr, const ResultEntryData& entryData)
{
  CALL_TRACE("operator<<(ostr, tableEntry)");

  const char*  pResultString;

  TimeOfDay  timeOfDay;

  timeOfDay = entryData.timeOfCurrResult_.getTimeOfDay();

  ostr << "CURRENT RUN:\n"
       << "<"   << timeOfDay.month
       << "-"   << timeOfDay.date
       << "-"   << timeOfDay.year
       << " @ " << timeOfDay.hour
       << ":"   << timeOfDay.minute
       << ":"   << timeOfDay.second
       << ", ";

  switch (entryData.resultOfCurrRun_)
  {
  case NOT_APPLICABLE_TEST_RESULT_ID :
    pResultString = "NOT APPLICABLE";
    break;
  case UNDEFINED_TEST_RESULT_ID :
    pResultString = "UNDEFINED";
    break;
  case INCOMPLETE_TEST_RESULT_ID :
    pResultString = "INCOMPLETE";
    break;
  case NOT_INSTALLED_TEST_ID :
    pResultString = "NOT INSTALLED";
    break;
  case PASSED_TEST_ID :
    pResultString = "PASSED";
    break;
  case OVERRIDDEN_TEST_ID :
    pResultString = "OVERRIDDEN";
    break;
  case MINOR_TEST_FAILURE_ID :
    pResultString = "MINOR";
    break;
  case MAJOR_TEST_FAILURE_ID :
    pResultString = "MAJOR";
    break;
  case NUM_TEST_RESULT_IDS :
  default :
    pResultString = "**ERROR**";
    break;
  };

  cout << pResultString << '>' << endl;


  timeOfDay = entryData.timeOfPrevResult_.getTimeOfDay();

  ostr << "PREVIOUS RUN:\n"
       << "<"   << timeOfDay.month
       << "-"   << timeOfDay.date
       << "-"   << timeOfDay.year
       << " @ " << timeOfDay.hour
       << ":"   << timeOfDay.minute
       << ":"   << timeOfDay.second
       << ", ";

  switch (entryData.resultOfPrevRun_)
  {
  case NOT_APPLICABLE_TEST_RESULT_ID :
    pResultString = "NOT APPLICABLE";
    break;
  case UNDEFINED_TEST_RESULT_ID :
    pResultString = "UNDEFINED";
    break;
  case INCOMPLETE_TEST_RESULT_ID :
    pResultString = "INCOMPLETE";
    break;
  case NOT_INSTALLED_TEST_ID :
    pResultString = "NOT INSTALLED";
    break;
  case PASSED_TEST_ID :
    pResultString = "PASSED";
    break;
  case OVERRIDDEN_TEST_ID :
    pResultString = "OVERRIDDEN";
    break;
  case MINOR_TEST_FAILURE_ID :
    pResultString = "MINOR";
    break;
  case MAJOR_TEST_FAILURE_ID :
    pResultString = "MAJOR";
    break;
  case NUM_TEST_RESULT_IDS :
  default :
    pResultString = "**ERROR**";
    break;
  };

  cout << pResultString << '>' << endl;

  return(ostr);
}

#endif // defined(SIGMA_DEVELOPMENT)
