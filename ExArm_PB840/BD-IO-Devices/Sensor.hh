#ifndef Sensor_HH
#define Sensor_HH
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: Sensor - Base class for all sensors
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/Sensor.hhv   25.0.4.0   19 Nov 2013 13:54:12   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006  By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 005 By: syw    Date: 13-Mar-1997   DR Number: DCS 1827
//  	Project:  Sigma (R8027)
//		Description:
//			Eliminate FlowSensorId.
//
//  Revision: 005 By: syw    Date: 02-May-1996   DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Added BD_CPU macros.  Added constructor for GUI CPU.
//
//  Revision: 004 By: syw    Date: 13-Mar-1996   DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Added EXH_O2_FLOW_SENSOR.
//
//  Revision: 003 By: syw    Date: 20-Feb-1996   DR Number: DCS 674
//  	Project:  Sigma (R8027)
//		Description:
//			Removed #ifndef around #include.
//
//  Revision: 002  By: syw    Date:  05-Dec-1995    DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Added SafetyNetSensorData * to constructor and as a data member.
//			Added getSafetyNetSensorDataAddr() method.
//
//	Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: INITIAL RELEASE
//      Project:  Sigma (R8027)
//      Description:
//      	Initial version
//
//=====================================================================

#include "BD_IO_Devices.hh"

//@ Usage-Classes

#include "AdcChannels.hh"
#include "SafetyNetSensorData.hh"

#ifdef INTEGRATION_TEST_ENABLE
#include "XReal.h"
#endif // INTEGRATION_TEST_ENABLE

//@ End-Usage

class Sensor 
{
  public:

#if defined (SIGMA_BD_CPU)

    Sensor( const AdcChannels::AdcChannelId id, SafetyNetSensorData *pData) ;
	Sensor( const AdcChannels::AdcChannelId id) ;

#else

	Sensor( void) ;
	
#endif // defined (SIGMA_BD_CPU)

    virtual ~Sensor( void) ;
    static void SoftFault( const SoftFaultID softFaultID,
						   const Uint32      lineNumber,
						   const char*       pFileName  = NULL, 
						   const char*       pPredicate = NULL) ;

#if defined (SIGMA_BD_CPU)

    inline Real32 getValue( void) const ;
    inline AdcCounts getCount( void) const ;
    inline AdcChannels::AdcChannelId getAdcId( void) const ;
    virtual void updateValue( void) ;
	inline SafetyNetSensorData * getSafetyNetSensorDataAddr( void) ;
#ifdef INTEGRATION_TEST_ENABLE
    void updateVirtualValue();
#endif // INTEGRATION_TEST_ENABLE

#endif // defined (SIGMA_BD_CPU)

  protected:

    virtual Real32 rawToEngValue_( const AdcCounts count) const ;

    //@ Data-Member: engValue_
    //value in engineering units, based on last sample
    Real32 engValue_ ;

#if defined (SIGMA_BD_CPU)

    //@ Data-Member: rawCount_
    //the ADC raw count as recorded for the last ADC sample
    AdcCounts rawCount_ ;

    //@ Data-Member: adcId_
    // the ADC channel id for the sensor
    AdcChannels::AdcChannelId adcId_ ;

	//@ Data-Member: *pSafetyNetSensorData
	// pointer to SafetyNetSensorData
	SafetyNetSensorData * pSafetyNetSensorData_ ;

#ifdef INTEGRATION_TEST_ENABLE
    swat::YReal m_rValue;
#endif // INTEGRATION_TEST_ENABLE

#endif // defined (SIGMA_BD_CPU)
	
  private:

#if defined (SIGMA_BD_CPU)

    Sensor( void) ;                    // not implemented
    
#endif // defined (SIGMA_BD_CPU)

    Sensor( const Sensor&) ;           // not implemented
    void operator=( const Sensor&) ;   // not implemented
} ;

// Inlined methods
#include "Sensor.in"

#endif // Sensor_HH
