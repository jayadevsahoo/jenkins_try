
#ifndef SoftFaultId_HH
#define SoftFaultId_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sotred in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Filename:  SoftFaultId - Enumerators of the Software Exceptions.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Utilities/vcssrc/SoftFaultId.hhv   25.0.4.0   19 Nov 2013 14:35:14   pvcs  $
//
//@ Modification-Log
//
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//====================================================================

//@ Usage-Classes
//@ End-Usage


//@ Type:  SoftFaultID
// Software exception identifier.
enum SoftFaultID
{
  UNDEFINED_SOFT_FAULT,  // this MUST be zero...

  INVARIANT_ID,
  PRE_CONDITION_ID,
  POST_CONDITION_ID,
  ASSERTION_ID,

  NUM_SOFT_FAULT_IDS
};


#endif // SoftFaultId_HH 
