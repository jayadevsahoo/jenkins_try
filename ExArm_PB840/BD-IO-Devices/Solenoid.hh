
#ifndef Solenoid_HH
#define Solenoid_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: Solenoid - A class to drive and manage the solenoids.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/Solenoid.hhv   25.0.4.0   19 Nov 2013 13:54:12   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005  By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 004  By: syw    Date:  19-Dec-1996    DR Number: DCS 1603
//  	Project:  Sigma (R8027)
//		Description:
//			Changed Register to BitAccessGpio.  Change masks to bit location.
//
//  Revision: 003  By: syw    Date: 20-Feb-1996   DR Number: DCS 674
//  	Project:  Sigma (R8027)
//		Description:
//			Removed #ifndef around #include.
//
//  Revision: 002  By: syw    Date: 05-Dec-1995   DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Made open() and close() not inlined.
//
//	Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: INITIAL RELEASE
//      Project:  Sigma (R8027)
//      Description:
//      	Initial version
//
//=====================================================================

#include "Sigma.hh"

//@ Usage-Classes

#include "BitAccessGpio.hh"
#include "BD_IO_Devices.hh"
//@ End-Usage

class Solenoid {
  public:

    enum SolenoidState { CLOSED, OPEN } ;

    Solenoid( const Solenoid::SolenoidState deenergizedState,
    		  const ApplicationDigitalOutputs_t bit, 
			  BitAccessGpio& rSolenoidPort) ;
    virtual ~Solenoid( void) ;

    static void SoftFault( const SoftFaultID softFaultID,
						   const Uint32      lineNumber,
						   const char*       pFileName  = NULL, 
						   const char*       pPredicate = NULL) ;
    virtual void open( void) ;
    virtual void close( void) ;
    
	virtual SolenoidState getCurrentState( void) ;   
    
	// TODO E600_LL:  to be eliminated in the future
	Real32 getCurrent( void) const { return 0;}
  
  protected:

  private:
    Solenoid( void);    					//default constructor
    Solenoid( const Solenoid&) ;            // not implemented...
    void   operator=( const Solenoid&) ;    // not implemented...

    //@ Data-Member:  deenergizedState_
    // solenoid state when no command is applied
    const SolenoidState  deenergizedState_ ;

    //@ Data_Member: bit_
    // bit position of solenoid within IO port
    ApplicationDigitalOutputs_t bit_ ;

    //@ Data-Member: rSolenoidPort_;
    // address of solenoid IO port
    BitAccessGpio& rSolenoidPort_ ;

	//@ Data-Member: currentState_ ;
	// current state of solenoid
	SolenoidState currentState_ ;    
} ;


#endif // Solenoid_HH 
