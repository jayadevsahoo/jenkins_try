#ifndef TrendDBTable_IN
#define TrendDbTable_IN

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
//@ Class: TrendDbTable
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Trend-Database/vcssrc/TrendDbTable.inv   25.0.4.0   19 Nov 2013 14:34:40   pvcs  $
//
//@ Modification-Log
// 
// 
//  Revision: 001  By:  mjf    Date:  18-Jun-2007    SCR Number: 6237
//       Project:  Trend
//       Description:
//             Initial version
// 
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getStartLbaBlock 	
//
//@ Interface-Description
//  return the start block on the compact 
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
Uint32 TrendDbTable::getStartLba( void ) const
{
	return( tableVars_.getStartLba() );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getCapacityLba
//
//@ Interface-Description
//  Return the table capacity in blocks
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
Uint32 TrendDbTable::getCapacityLba( void ) const
{
	return( tableVars_.getCapacityLba() );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getTableCapacity
//
//@ Interface-Description
//  Return the table capacity in sample records "rows"
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
Uint32 TrendDbTable::getTableCapacity( void ) const
{
	return( tableRecordCapacity_ );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getRecordSize
//
//@ Interface-Description
//  return the number of bytes in each sample record "row"
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
Int16 TrendDbTable::getRecordSize( void ) const
{
	return( tableRecord_.size() );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getRecordCount
//
//@ Interface-Description
//  return the number of sample records in the database
//---------------------------------------------------------------------
//@ Implementation-Description
//  Include the records that are on the compact flash in complete
//  block increments and the number of records in the write buffer
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
Uint32 TrendDbTable::getRecordCount( void ) const
{
	Int32 recordsInRam = writeBuffer_.getRecordPosition() + 1;
	Int32 recordsInCf = tableVars_.getSizeLba() * writeBuffer_.getRecordsPerBlock();
	return( recordsInRam + recordsInCf );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: recordPosition
//
//@ Interface-Description
//  return the position in sample records "rows" of the current database head
//---------------------------------------------------------------------
//@ Implementation-Description
//  includes the records in the write buffer
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
Uint32 TrendDbTable::getRecordPosition( void ) const
{
	return( (writeBuffer_.getRecordPosition() + 1) +
			(writeBuffer_.getRecordsPerBlock() * 
			 (tableVars_.getHeadLba() - tableVars_.getStartLba()) ));
}


#endif // TrendDbTable_IN 
