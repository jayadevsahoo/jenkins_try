#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: TrendDataValues - An array of trend values
//---------------------------------------------------------------------
//@ Interface-Description
//  Encapsulation of an array of trend values for one column of the trend
//  data set.  The array repesents values for one parmaeter which a max
//  size of 360.
//  The client can get the Trend Id for the column getId(), check to see
//  if the column has any data isValid(), or get any olf the values from
//  the array getValue().
//---------------------------------------------------------------------
//@ Rationale
//  Provides a protected access to the column of trend data
//---------------------------------------------------------------------
//@ Implementation-Description
//  The client has limited read access to the column of data.
//  The Trend Data Manager and Repositories have unresticted access to 
//  the column of data, which is used to fill in the data.
//---------------------------------------------------------------------
//@ Fault-Handling
//  SoftFault is used
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Trend-Database/vcssrc/TrendDataValues.ccv   25.0.4.0   19 Nov 2013 14:34:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By:  mjf    Date:  22-Aug-2006    SCR Number: 6237
//       Project:  Trend
//       Description:
//             Initial version.
// 
//=====================================================================

//@ Usage-Classes
//@ End-Usage
// 
#include "Trend_Database.hh"
#include "TrendDataValues.hh"
#include "FaultHandlerMacros.hh"

//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TrendDataValues [constructor]
//
//@ Interface-Description
//  No action
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
TrendDataValues::TrendDataValues( void )
{
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TrendDataManager [destructor]
//
//@ Interface-Description
//  Does no action  
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
TrendDataValues::~TrendDataValues()	
{
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setId_
//
//@ Interface-Description
//  Set the Trend Select enum Id
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDataValues::setId_( TrendSelectValue::TrendSelectValueId trendId )
{
	TREND_MGR_DBG2( 32, "Data Values: set column Id ", trendId );
	trendId_ = trendId;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setValue_
//
//@ Interface-Description
//  Set the Trend Select value for this row
//---------------------------------------------------------------------
//@ Implementation-Description
//  Deep copy on the Trend Value
//---------------------------------------------------------------------
//@ PreCondition
//	Validates the row
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDataValues::setValue_( Uint16 row, const TrendValue & trendValue )
{
	AUX_CLASS_PRE_CONDITION( (row < TrendDataMgr::MAX_ROWS), row );
	valueArray_[ row ] = trendValue;
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================
void TrendDataValues::SoftFault(const SoftFaultID  softFaultID,
							   const Uint32       lineNumber,
							   const char*        pFileName,
							   const char*        pPredicate)  
{
	FaultHandler::SoftFault(softFaultID, TREND_DATABASE, TREND_DATA_VALUES,
							lineNumber, pFileName, pPredicate);
}



