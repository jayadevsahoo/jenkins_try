
#ifndef TestInfo_HH
#define TestInfo_HH

#if		defined(SIGMA_GUI_CPU) || defined(SIGMA_COMMON)
//====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993,1995 Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  TestInfo - Service Data Item processing agent class.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Data/vcssrc/TestInfo.hhv   25.0.4.0   19 Nov 2013 14:21:06   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah     Date:  15-Jan-99   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' to a non-inlined method.
//
//  Revision: 001  By:  yyy    Date:  16-May-95   DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//====================================================================

#include "Sigma.hh"
#include "ServiceDataClassIds.hh"
#include "DataValue.hh"
#include "BufferIndex.hh"
#include "TestDataId.hh"
#include "TestDatum.hh"

class TestInfo
{
	//@ Friend: ServiceDataMgr.
	// Allows ServiceDataMgr class to call the 'modifyData_' private method
	// when it receives new data from the network.
	friend class ServiceDataMgr;

public:
	TestInfo(TestDataId::TestDataItemId dataId);
	~TestInfo(void);

	// Methods to retrieve the data value.
    inline const Real32 &getDataValue(void);

	// Methods to retrieve the command value.
	inline const Int &getCommandValue(void);

	// Methods to retrieve the status value.
	inline const Int &getStatusValue(void);

	// Methods to query for valid data type
	inline Boolean isData(void) const;

	// Methods to query for valid command type
	inline Boolean isCommand(void) const;

	// Methods to query for valid status type
	inline Boolean isStatus(void) const;

    static void  SoftFault(const SoftFaultID softFaultID, 
				  const Uint32      lineNumber, 
				  const char*       pFileName = NULL, 
				  const char*       pBoolTest = NULL); 

protected:
    virtual	void modifyData_( void *);

    //@ data-member: data_[2]
    // Two-dimensional service Data buffer to hold service data.
    TestDatum data_[2];

	//@ Data-Member:   index_
	//  index_.network indexes into one of the dual data buffer that
	//  new data from the network is put in.
	//  index_.active indexes into the dual data buffer that holds
	//  the most current data.
    BufferIndex	index_;

    //@ Data-Member: dataId_
    // A constant, unique service item id that identifies a test data item type
	// that this object is managing.
    TestDataId::TestDataItemId dataId_;

private:
    TestInfo(void);						// not implemented ...
    TestInfo(const TestInfo& range); 	// not implemented ...
    void  operator=(const TestInfo&); 	// not implemented...

};

// Inlined methods...
#include "TestInfo.in"

#endif		//defined(SIGMA_GUI_CPU) || defined(SIGMA_COMMON)
#endif  // TestInfo_HH

