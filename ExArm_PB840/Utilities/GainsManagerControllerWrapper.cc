#include "stdafx.h"
//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file  GainsManagerControllerWrapper.cc
/// @brief Implementation of GainsManagerControllerWrapper class
//----------------------------------------------------------------------------

//-------------------------------------
// I N C L U D E S
//-------------------------------------
#include "SigmaTypes.hh"
#include "GainsManagerControllerWrapper.hh"
#include "ControllersRefs.hh"
#include "PeepController.hh"
#include "FlowController.hh"
#include "PressureController.hh"
#include "PidPressureController.hh"
#include "PhasedInContextHandle.hh"
#include "SupportTypeValue.hh"
#include "BreathMiscRefs.hh"
#include "Tube.hh"

using namespace GainsManagerNS;

//----------------------------------------------------------------------------
// GAINS MANAGER TOUCH POINT for adding gains, constants, variables
// for manipulation.
//----------------------------------------------------------------------------

//---------------------------
// setPeepKi
//---------------------------
void   GainsManagerControllerWrapper::setPeepKi( Real32 r )
{
    RPeepController.setKi( r );
} // setPeepKi

//---------------------------
// getPeepKi
//---------------------------
Real32 GainsManagerControllerWrapper::getPeepKi()
{
    Real32 r = RPeepController.getKi();

    return r;
} // getPeepKi

//---------------------------
// setAFactor
//---------------------------
void   GainsManagerControllerWrapper::setAFactor( Real32 r )
{
	FlowController::setAFactor( r );
} // setAFactor

//---------------------------
// getAFactor
//---------------------------
Real32 GainsManagerControllerWrapper::getAFactor()
{
	Real32 r = FlowController::getAFactor();

	return r;
} // getAFactor

//---------------------------
// setBFactor
//---------------------------
void   GainsManagerControllerWrapper::setBFactor( Real32 r )
{
	FlowController::setBFactor( r );
} // setBFactor

//---------------------------
// getBFactor
//---------------------------
Real32 GainsManagerControllerWrapper::getBFactor()
{
	Real32 r = FlowController::getBFactor();

	return r;
} // getBFactor

//---------------------------
// setCFactor
//---------------------------
void   GainsManagerControllerWrapper::setCFactor( Real32 r )
{
	FlowController::setCFactor( r );
} // setCFactor

//---------------------------
// getCFactor
//---------------------------
Real32 GainsManagerControllerWrapper::getCFactor()
{
	Real32 r = FlowController::getCFactor();

	return r;
} // getCFactor

//---------------------------
// setDFactor
//---------------------------
void   GainsManagerControllerWrapper::setDFactor( Real32 r )
{
	FlowController::setDFactor( r );
} // setDFactor

//---------------------------
// getDFactor
//---------------------------
Real32 GainsManagerControllerWrapper::getDFactor()
{
	Real32 r = FlowController::getDFactor();

	return r;
} // getDFactor

//---------------------------
// setPressureKp
//---------------------------
void GainsManagerControllerWrapper::setPressureKp( Real32 r )
{
    RPressureController.setKp( r );
} // setPressureKp

//---------------------------
// getPressureKp
//---------------------------
Real32 GainsManagerControllerWrapper::getPressureKp()
{
	Real32 r = RPressureController.getKp();
    return r;
} // getPressureKp

//---------------------------
// setPressureKi
//---------------------------
void   GainsManagerControllerWrapper::setPressureKi( Real32 r )
{
    RPressureController.setKi( r );
} // setPressureKi

//---------------------------
// getPressureKi
//---------------------------
Real32 GainsManagerControllerWrapper::getPressureKi()
{
	Real32 r = RPressureController.getKi();
    return r;
} // getPressureKi

//---------------------------
// getPidKi
//---------------------------
Real32 GainsManagerControllerWrapper::getPidKi()
{
	Real32 r = RPidPressureController.getKi();
    return r;
} // getPidKi

//---------------------------
// setPidKi
//---------------------------
void GainsManagerControllerWrapper::setPidKi( Real32 r )
{
	RPidPressureController.setKi( r );
} // setPidKi

//---------------------------
// getPidKp
//---------------------------
Real32 GainsManagerControllerWrapper::getPidKp()
{
	Real32 r = RPidPressureController.getKp();
    return r;
} // getPidKp

//---------------------------
// setPidKp
//---------------------------
void GainsManagerControllerWrapper::setPidKp( Real32 r )
{
	RPidPressureController.setKp( r );
} // setPidKp

//---------------------------
// setPidKd
//---------------------------
void   GainsManagerControllerWrapper::setPidKd( Real32 r )
{
    RPidPressureController.setKd( r );
} // setPidKd

//---------------------------
// getPidKd
//---------------------------
Real32 GainsManagerControllerWrapper::getPidKd()
{
	Real32 r = RPidPressureController.getKd();
    return r;
} // getPidKd


