
#ifndef VolatileMemory_HH
#define VolatileMemory_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class:  VolatileMemory - Volatile Memory Block.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Persistent-Objects/vcssrc/VolatileMemory.hhv   25.0.4.0   19 Nov 2013 14:18:56   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By:  sah    Date:  01-Jan-1999    DCS Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' to a non-inlined method.
//
//  Revision: 003  By:  sah    Date:  29-Sep-1997    DCS Number: 2525
//  Project:  Sigma (R8027)
//  Description:
//	Removed obsoleted data references.
//
//  Revision: 002  By:  sah    Date:  04-Sep-1997    DCS Number: 1923
//  Project:  Sigma (R8027)
//  Description:
//	Replaced the Communication Diagnostic Log with the new System
//	Information Log.  Also, removed unneeded Unit Test access
//	directives.
//
//  Revision: 001  By:  sah    Date:  15-Mar-1997    DCS Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//          Initial version (Integration baseline).
//
//====================================================================

#include "Sigma.hh"
#include "NovRamStatus.hh"

//@ Usage-Classes
#include "SystemInfoLog.hh"
#include "SystemDiagLog.hh"
#include "EstDiagLog.hh"
#include "EstResultTable.hh"
#include "SstDiagLog.hh"
#include "SstResultTable.hh"
#include "Resistance.hh"
//@ End-Usage


class VolatileMemory
{
    //@ Friend:  NovRamManager
    // 'NovRamManager' needs access to ALL of this class's members.
    friend class NovRamManager;

    //@ Friend:  NovRamXaction
    // 'NovRamXaction' needs access to ALL of this class's members.
    friend class NovRamXaction;

  public:
    static inline class VolatileMemory*  GetAddress(void);

    static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL, 
			  const char*       pPredicate = NULL);

  private:
    // Everything is private, because only this class's friends (i.e.,
    // 'NovRamManager' and 'NovRamXaction') should be using this class
    // directly.
    VolatileMemory(const VolatileMemory&);	// not implemented...
    void  operator=(const VolatileMemory&);	// not implemented...

    VolatileMemory(void);
    ~VolatileMemory(void);

    void  initialize_(void);

    //@ Data-Member:  commDiagLog_
    // System Information Log.
    SystemInfoLog  systemInfoLog_;

    //@ Data-Member:  systemDiagLog_
    // System Diagnostic Code Log.
    SystemDiagLog  systemDiagLog_;

    //@ Data-Member:  estDiagLog_
    // EST Diagnostic Code Log.
    EstDiagLog  estDiagLog_;

    //@ Data-Member:  estResultTable_
    // EST Result Table.
    EstResultTable  estResultTable_;

    //@ Data-Member:  sstDiagLog_
    // SST Diagnostic Code Log.
    SstDiagLog  sstDiagLog_;

    //@ Data-Member:  sstResultTable_
    // SST Result Table.
    SstResultTable  sstResultTable_;

    //@ Data-Member:  systemOperationalHours_
    // The system's operational hours.
    Uint  systemOperationalHours_;

    //@ Data-Member:  compressorOperationalHours_
    // Compressor's operational hours.
    Uint  compressorOperationalHours_;

    //@ Data-Member:  inspCctResistance_
    // Resistance of the inspiratory circuit.
    Resistance  inspCctResistance_;

    //@ Data-Member:  exhCctResistance_
    // Resistance of the exhalation circuit.
    Resistance  exhCctResistance_;

    //@ Data-Member:  IsInitialized_
    // This is a static boolean flag to inidicate whether this class's
    // data members are ready for "interaction" (i.e., it indicates whether
    // 'initialize_()' has been called).
    static Boolean  IsInitialized_;

    //@ Data-Member:  P_INSTANCE_
    // Constant pointer to the one (non-constant) instance of this class.
    static VolatileMemory *const  P_INSTANCE_;
};


// Inlined methods...
#include "VolatileMemory.in"


#endif // VolatileMemory_HH 
