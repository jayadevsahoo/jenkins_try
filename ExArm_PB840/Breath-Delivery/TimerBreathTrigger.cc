#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: TimerBreathTrigger - Triggers when corresponding Interval Timer expired.
//---------------------------------------------------------------------
//@ Interface-Description
//	This class triggers inspiration or expiration depending on usage
//	of the trigger. It is enabled or disabled, depending upon the 
//	applicability of the trigger. When the interval timer expired,
//	the trigger is considered to have "fired".  This trigger is kept 
//	on a list contained in the BreathTriggerMediator.
//---------------------------------------------------------------------
//@ Rationale
//	This class implements time dependent breath triggers.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This class contains a boolean state variable to keep track of 
//	whether this trigger is enabled or not.  If the trigger is not 
//	enabled, it will always return a state of false.  If it is enabled, 
//	the trigger "fires" when the referenced interval timer expires. 
//	When a timer expires, timeUpHappened is called to update the 
//	data member timeUp_ of this trigger. 
//	$[04010]
//---------------------------------------------------------------------
//@ Fault-Handling
//	n/a.
//---------------------------------------------------------------------
//@ Restrictions
//	none
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/TimerBreathTrigger.ccv   25.0.4.0   19 Nov 2013 14:00:12   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006   By: gdc   Date:  22-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Added base class qualifier to isEnabled_ data member for clarity.
//
//  Revision: 005  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 004  By:  sp    Date:  31-Oct-1996    DR Number:
//       Project:  Sigma (R8027)
//       Description:
//             Inspection rework.
//
//  Revision: 003  By:  sp    Date:  10-Oct-1996    DR Number:600
//       Project:  Sigma (R8027)
//       Description:
//             Updated traceability
//
//  Revision: 002  By:  sp    Date:  30-Sept-1996    DR Number:600
//       Project:  Sigma (R8027)
//       Description:
//             Removed unit test methods.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "TimerBreathTrigger.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TimerBreathTrigger
//
//@ Interface-Description
//	This constructor takes arguments of interval timer and trigger id.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Initialize triggerId_ by calling BreathTrigger's constructor.
//	Initialize the reference rIntervalTimer_ to the argument iTimer.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

TimerBreathTrigger::TimerBreathTrigger(
	IntervalTimer& iTimer, 
	const Trigger::TriggerId id
)
: TimerTarget(),
  rIntervalTimer_(iTimer),
  BreathTrigger(id)
{
  CALL_TRACE("TimerBreathTrigger(IntervalTimer& iTimer, Trigger::TriggerId id)");
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~TimerBreathTrigger() 
//
//@ Interface-Description 
//	Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

TimerBreathTrigger::~TimerBreathTrigger(void)
{
  CALL_TRACE("~TimerBreathTrigger()");

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: disable
//
//@ Interface-Description
//     This method takes no parameters and has no return value.
//     This trigger becomes disabled, and is no longer able to detect the trigger
//     condition it is set up to monitor.
//---------------------------------------------------------------------
//@ Implementation-Description
//     The state data member isEnabled_ and timeUp_ is set to false.
//     The iTimer_ is stopped.
//---------------------------------------------------------------------
//@ PreCondition
//     none
//---------------------------------------------------------------------
//@ PostCondition
//     none
//@ End-Method
//=====================================================================
void
TimerBreathTrigger::disable(void)
{
  CALL_TRACE("disable(void)");
 
  // $[TI1]
  timeUp_ = FALSE;
  rIntervalTimer_.stop();
  Trigger::isEnabled_ = FALSE;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: timeUpHappened
//
//@ Interface-Description
//	This method takes a const reference to the interval timer that has 
//      expired as a parameter, and has no return value.  This method is
//	used by interval timer to update state of this trigger.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Update timeUp_. The expired timer is stopped. 
//---------------------------------------------------------------------
//@ PreCondition
//      (&rIntervalTimer_ == &rTimer)
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
TimerBreathTrigger::timeUpHappened(const IntervalTimer& rTimer)
{
  CALL_TRACE("timeUpHappened(const IntervalTimer& rTimer)");

  // $[TI1]
  CLASS_PRE_CONDITION(&rIntervalTimer_ == &rTimer);	
  timeUp_ = TRUE;
  rIntervalTimer_.stop();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
TimerBreathTrigger::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, TIMERBREATHTRIGGER,
                          lineNumber, pFileName, pPredicate);
}



//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: triggerCondition_
//
//@ Interface-Description
//	This method takes no parameter.
//	This method returns true if interval timer expired else returns false.
//---------------------------------------------------------------------
//@ Implementation-Description
//	return data member timeUp_
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Boolean
TimerBreathTrigger::triggerCondition_(void)
{
  CALL_TRACE("triggerCondition_(void)");
 
  // TRUE: $[TI1]
  // FALSE: $[TI2]
  return (timeUp_);
}

//=====================================================================
//
//  Private Methods...
//
//=====================================================================

