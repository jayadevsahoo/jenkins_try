#ifndef SafetyNet_HH
#define SafetyNet_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//============================ H E A D E R   D E S C R I P T I O N ====
//  Class: SafetyNet - Safety-Net Subsystem module ID class
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Safety-Net/vcssrc/SafetyNet.hhv   25.0.4.1   20 Nov 2013 16:09:24   rhjimen  $
//
//@ Modification-Log
//
//  Revision: 004  By: rpr    Date:  6-Nov-2012    SCR Number: 6777
//  Project:  BI
//  Description:
//      Removed all monitor software.  The time monitor performs the 5 ms
//		clock check. 
// 
//  Revision: 003  By: mnr    Date:  03-Dec-2010    SCR Number: 6671
//  Project:  XENA2
//  Description:
//      Removed BREATHTIMEMONITOR classId as it is being obsoleted.
//
//  Revision: 002  By: sah    Date:  15-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001    By: by  Date: 12-Dec-1995 DR Number: None
//
//    Project: Sigma (840)
//    Description:
//       Initial version.
//
//=====================================================================

#include "Sigma.hh"


///////////////////////////////////////////////////////
// NOTE: The order of the class IDs are permenant !!!
//       Only added to the end of the list.
///////////////////////////////////////////////////////
enum BackgroundClassId
{
    SAFETYNET              = 0,
    BACKGROUND             = 1,
    BACKGROUNDCYCLETIMER   = 2,
    BACKGROUNDCYCLEAPP     = 3,
    BACKGROUNDBDTESTS      = 4,
    BACKGROUNDGUITESTS     = 5,
    BACKGROUNDMAINTAPP     = 6,
    BACKGROUNDMEMORYAPP    = 7,
    BTATEVENTSTATE         = 8,
    TASKMONITOR            = 9,
    TASKMONITORQUEUEMSG    = 10,
    ALARMSMONITOR          = 11,
    APNEAALARMMONITOR      = 12,
    APNEAINTERVALMONITOR   = 13,
    BDMONITOR              = 14,
    BDMONITORCHECK         = 15,
    TIMEMONITOR		       = 16,
    BDMONITORMEDIATOR      = 17,
    DATACORRUPTIONINFO     = 18,
    HIPMONITOR             = 19,
    INSPTIMEMONITOR        = 20,
    O2MONITOR              = 21,
    BACKGROUNDFAULTHANDLER = 22 
};



class SafetyNet
{

    public:

        static void Initialize( void );

        static void SoftFault( const SoftFaultID softFaultID,
                   const Uint32      lineNumber,
                   const char*       pFileName  = NULL,
                   const char*       pPredicate = NULL );
 
    private:

        SafetyNet( void );   // Constructor, not implemented
        ~SafetyNet( void );  // Destructor, not implemented

        SafetyNet( const SafetyNet& );        // Not implemented
        void operator= ( const SafetyNet& );  // Not implemented
};


#endif    // SafetyNet_HH
