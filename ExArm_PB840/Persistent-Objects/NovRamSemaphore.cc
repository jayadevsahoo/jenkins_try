#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: NovRamSemaphore - NOVRAM Semaphore.
//---------------------------------------------------------------------
//@ Interface-Description
//  This class provides a single-writer, multiple-reader semaphore for
//  protecting the various NOVRAM data items from multiple update threads.
//---------------------------------------------------------------------
//@ Rationale
//  There are many more "read" threads, than "write" threads, through
//  NOVRAM therefore this type of semaphore reduces thread-locking.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class has its own mutual-exclusion semaphore for protecting
//  its internal counter information.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Persistent-Objects/vcssrc/NovRamSemaphore.ccv   25.0.4.0   19 Nov 2013 14:18:50   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By:  sah    Date:  01-Jan-1999    DCS Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' to a non-inlined method.
//
//  Revision: 003  By:  sah    Date:  30-Sep-1997    DCS Number: 2530
//  Project:  Sigma (R8027)
//  Description:
//     Provide additional semaphore for use by the internal double buffer
//     instance of 'NovRamSequenceNum'.
//
//  Revision: 002  By:  sah    Date:  25-Jun-1997    DCS Number: 1908
//  Project:  Sigma (R8027)
//  Description:
//     Provide additional semaphores for those double-buffered items that
//     are accessed by time-critical tasks.
//
//  Revision: 001  By:  sah    Date:  15-Mar-1997    DCS Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//          Initial version (Integration baseline).
//
//=====================================================================

#include "NovRamSemaphore.hh"

//@ Usage-Classes
#include "MutEx.hh"
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Static Data Definitions...
//
//=====================================================================

static Uint  CounterMutExMemory_[(sizeof(MutEx) + sizeof(Uint) - 1) /
					sizeof(Uint)];

//=====================================================================
//
//  Static Member Definitions...
//
//=====================================================================

MutEx&  NovRamSemaphore::RCounterMutEx_ = *((MutEx*)::CounterMutExMemory_);

NovRamSemaphore::SemaphoreInfo  NovRamSemaphore::ArrSemaphoreInfo_[
					 NovRamSemaphore::NUM_ACCESS_TYPES
								  ];

Boolean  NovRamSemaphore::BlockingEnabledFlag_ = FALSE;


//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  Initialize()   [static]
//
//@ Interface-Description
//  Initialize this class's static data members.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

void
NovRamSemaphore::Initialize(void)
{
  CALL_TRACE("Initialize()");

  SAFE_CLASS_ASSERTION((sizeof(::CounterMutExMemory_) >= sizeof(MutEx)));
  new (&NovRamSemaphore::RCounterMutEx_) MutEx(::NOVRAM_READ_COUNTER_MT);

  ArrSemaphoreInfo_[DIAG_LOG_ACCESS].mutexId     = ::NOVRAM_DIAG_LOG_MT;
  ArrSemaphoreInfo_[ALARM_LOG_ACCESS].mutexId    = ::NOVRAM_ALARM_LOG_MT;
  ArrSemaphoreInfo_[RESULT_TABLE_ACCESS].mutexId = ::NOVRAM_RESULT_TABLE_MT;
  ArrSemaphoreInfo_[SEQUENCE_NUM_ACCESS].mutexId = ::NOVRAM_SEQUENCE_NUM_MT;

  ArrSemaphoreInfo_[DOUBLE_BUFF_ACCESS].mutexId  = ::NOVRAM_DOUBLE_BUFF_MT;
#if defined(SIGMA_GUI_CPU)
  ArrSemaphoreInfo_[DATA_CORRUPTION_INFO_ACCESS].mutexId =
  						   ::NOVRAM_DATA_CORR_INFO_MT;
#elif defined(SIGMA_BD_CPU)
  ArrSemaphoreInfo_[BD_SYSTEM_STATE_ACCESS].mutexId =
  						   ::NOVRAM_BD_SYSTEM_STATE_MT;
  ArrSemaphoreInfo_[FIO2_CAL_INFO_ACCESS].mutexId  =
  						   ::NOVRAM_FIO2_CAL_INFO_MT;
#endif // defined(SIGMA_GUI_CPU)
  ArrSemaphoreInfo_[SEQ_NUM_BUFFER_ACCESS].mutexId = ::NOVRAM_SEQ_NUM_BUFFER_MT;

  for (Uint idx = 0u; idx < NovRamSemaphore::NUM_ACCESS_TYPES; idx++)
  {
    ArrSemaphoreInfo_[idx].readAccessCount = 0u;
    SAFE_AUX_CLASS_ASSERTION((ArrSemaphoreInfo_[idx].mutexId != 0u), idx);
  }

  // disable during initialization stage...
  NovRamSemaphore::DisableBlocking();
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  RequestReadAccess(accessId)   [static]
//
//@ Interface-Description
//  Request "read" access to NOVRAM.  If there are no "write" threads with
//  access granted, then this thread will be granted access immediately.  If
//  access is already granted to a "write" thread, this thread will block
//  until access is freed up again.  (NOTE: a "read" thread will NOT block
//  another "read" thread from access.)
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

void
NovRamSemaphore::RequestReadAccess(
			    const NovRamSemaphore::AccessIdType accessId
			          )
{
  CALL_TRACE("RequestReadAccess(accessId)");
  AUX_CLASS_PRE_CONDITION((accessId >= 0  &&  accessId < NUM_ACCESS_TYPES),
  			  accessId);

  if (NovRamSemaphore::IsBlockingEnabled())
  {   // $[TI1]
    RCounterMutEx_.request();

    if (ArrSemaphoreInfo_[accessId].readAccessCount == 0u)
    {   // $[TI1.1]
      // there are no "read" access granted, yet...
      MutEx  accessMutex(ArrSemaphoreInfo_[accessId].mutexId);

      accessMutex.request();
    }   // $[TI1.2] -- a "read" thread already has access...

    ArrSemaphoreInfo_[accessId].readAccessCount++;

    RCounterMutEx_.release();
  }   // $[TI2] -- blocking is disabled...
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ReleaseReadAccess(accessId)  [static]
//
//@ Interface-Description
//  Release "read" access to NOVRAM.  If there are no other "read" threads
//  with access granted, then the access will be freed up for access by any
//  type of thread.  If other "read" threads also have access granted, access
//  will only be available to other "read" threads.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

void
NovRamSemaphore::ReleaseReadAccess(
			    const NovRamSemaphore::AccessIdType accessId
			          )
{
  CALL_TRACE("ReleaseReadAccess(accessId)");
  AUX_CLASS_PRE_CONDITION((accessId >= 0  &&  accessId < NUM_ACCESS_TYPES),
  			  accessId);

  if (NovRamSemaphore::IsBlockingEnabled())
  {   // $[TI1]
    CLASS_PRE_CONDITION((ArrSemaphoreInfo_[accessId].readAccessCount > 0u));

    RCounterMutEx_.request();

    ArrSemaphoreInfo_[accessId].readAccessCount--;

    if (ArrSemaphoreInfo_[accessId].readAccessCount == 0u)
    {   // $[TI1.1]
      // there are no more "read" access threads...
      MutEx  accessMutex(ArrSemaphoreInfo_[accessId].mutexId);

      accessMutex.release();
    }   // $[TI1.2] -- a "read" thread still has access...

    RCounterMutEx_.release();
  }   // $[TI2] -- blocking is disabled...
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  RequestWriteAccess(accessId)  [static]
//
//@ Interface-Description
//  Request "write" access to NOVRAM.  If there are no other "read" or
//  "write" threads with access granted, then this thread will be granted
//  access immediately.  If access is already granted, this thread will
//  block until access is freed up again.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

void
NovRamSemaphore::RequestWriteAccess(
			    const NovRamSemaphore::AccessIdType accessId
			           )
{
  CALL_TRACE("RequestWriteAccess(accessId)");
  AUX_CLASS_PRE_CONDITION((accessId >= 0  &&  accessId < NUM_ACCESS_TYPES),
  			  accessId);

  if (NovRamSemaphore::IsBlockingEnabled())
  {   // $[TI1]
    MutEx  accessMutex(ArrSemaphoreInfo_[accessId].mutexId);

    accessMutex.request();
  }   // $[TI2] -- blocking is disabled...
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ReleaseWriteAccess(accessId)
//
//@ Interface-Description
//  Release "write" access to NOVRAM.  Access is freed up for access by any
//  type of thread.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

void
NovRamSemaphore::ReleaseWriteAccess(
			    const NovRamSemaphore::AccessIdType accessId
			           )
{
  CALL_TRACE("ReleaseWriteAccess(accessId)");
  AUX_CLASS_PRE_CONDITION((accessId >= 0  &&  accessId < NUM_ACCESS_TYPES),
  			  accessId);

  if (NovRamSemaphore::IsBlockingEnabled())
  {   // $[TI1]
    MutEx  accessMutex(ArrSemaphoreInfo_[accessId].mutexId);

    accessMutex.release();
  }   // $[TI2] -- blocking is disabled...
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//            [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//  and 'lineNumber' are essential pieces of information.  The
//  'pFileName' and 'pPredicate' strings may be defaulted in the macro
//  to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//  none 
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

void
NovRamSemaphore::SoftFault(const SoftFaultID  softFaultID,
			   const Uint32       lineNumber,
			   const char*        pFileName,
			   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, PERSISTENT_OBJS, NOV_RAM_SEMAPHORE,
                          lineNumber, pFileName, pPredicate);
}
