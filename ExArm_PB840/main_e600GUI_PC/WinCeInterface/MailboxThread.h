
//*****************************************************************************
//
//  MailboxThread.h - Header file for the MailboxThread_t class.
//
//*****************************************************************************

#ifndef MAILBOX_THREAD_H

#define MAILBOX_THREAD_H

#include "Thread.h"
#include "ApplicationMessages.h"
#include "MsgQue.h"


class MailboxThread_t : public Thread_t, public MsgQue_t
{


    public:
        MailboxThread_t(uint16_t MaxMailEntrySize, uint16_t Entries, LPCWSTR Name=NULL, bool DoubleQueue=false);
        ~MailboxThread_t(void);

        bool SendMail(uint8_t *MessagePtr, uint16_t MessageSize, int32_t Timeout=0, short queue=0);

		void GetQueueInformation(LPMSGQUEUEINFO lpInfo, short queue=0);
		uint32_t NumberofMailsInTheQueue(short queue=0);
    protected:


		bool GetMail(uint8_t *MessagePtr, uint32_t Timeout=INFINITE, short queue=0);
        bool GetMail(uint8_t *MessagePtr, uint32_t *BytesRead, uint32_t Timeout=INFINITE, short queue=0);

        HANDLE ReadHandle[2];
        HANDLE WriteHandle[2];

        int32_t MaxEntrySize;
        uint32_t ErrVal;
		bool	UseDoubleQueue;
};


#endif











