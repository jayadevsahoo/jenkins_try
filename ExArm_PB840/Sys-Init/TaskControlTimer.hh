#ifndef TaskControlTimer_HH
#define TaskControlTimer_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//====================================================================
 
//====================================================================
// Class:  TaskControlTimer - Sys-Init subsystem class
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Sys-Init/vcssrc/TaskControlTimer.hhv   25.0.4.0   19 Nov 2013 14:33:58   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By:  gdc    Date:  12-Dec-2008    SCR Number: 6068
//  Project:  840S
//  Description:
//		Removed SUN prototype code.
//
//   Revision 003   By:   sah      Date: 01/13/99    DR Number: 5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to a non-inlined method.
//
//   Revision 002   By:   gdc      Date: 11-JUN-1997 DR Number: 1902
//      Project:   Sigma   (R8027)
//      Description:
//         Modified to queue derived TaskControlTimers to a specified
//         queue so so TaskControl can process them using different 
//         contexts.
//
//   Revision 001   By:   gdc      Date: 07/06/95    DR Number:   n/a  
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//====================================================================
 
//@ Usage-Classes
#include "Sys_Init.hh"
#include "MsgTimer.hh"
#include "OsTimeStamp.hh"
#include "TaskControlQueueMsg.hh"
//@ End-Usage
 
class AppContext;

class TaskControlTimer {
  public:

    virtual void         dispatch(const AppContext& context) const = 0;
    virtual const char * nameOf(void) const = 0;

    void    set(void);
    void    cancel(void);
    Boolean isExpired(void) const;


    static void          SoftFault(const SoftFaultID  softFaultID,
				   const Uint32       lineNumber,
				   const char*        pFileName = NULL,
				   const char*        pPredicate = NULL);
 
#ifdef SIGMA_UNIT_TEST
    Sys_Init::InitClassId  isA(void) const;
    TaskControlQueueMsg    getMsg(void) const;
    const MsgTimer&        getMsgTimer(void) const;
    friend class           TransitionTimer_UT;
#endif

  protected:
    TaskControlTimer(  Sys_Init::InitClassId classId
                     , const Int32 timeout
                     , const Int32 queueNumber );
    virtual ~TaskControlTimer(); 

  private:
    TaskControlTimer();                            // declared only
    TaskControlTimer(const TaskControlTimer&);     // declared only
    void operator=(const TaskControlTimer&);       // declared only


    //@ Data-Member:    classId_
    //  Contains the class identifier for the object.
    Sys_Init::InitClassId  classId_;

    //@ Data-Member:    msg_
    //  The object which MsgTimer queues to task control when the timer
    //  expires.
    TaskControlQueueMsg msg_;

    //@ Data-Member:    duration_
    //  The timer duration (in milliseconds).
    Uint32              duration_;

    //@ Data-Member:    timeSet_
    //  The time when the timer was started.
    OsTimeStamp         timeSet_;

    //@ Data-Member:    active_
    //  TRUE if timer is set.  FALSE if timer is not set or is cancelled.
    Boolean             active_;

    //@ Data-Member:    timer_
    //  MsgTimer object which is set for the timeout period.
    MsgTimer            timer_;

};
 

#endif // TaskControlTimer_HH
