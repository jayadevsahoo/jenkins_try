
#ifndef NifManeuver_HH
#define NifManeuver_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2006, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: NifManeuver - Negative Inspiratory Force (NIF) Maneuver Handler
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/NifManeuver.hhv   25.0.4.0   19 Nov 2013 13:59:54   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001   By: gdc   Date:  22-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Initial version.
//
//====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"
#include "EventData.hh"
#include "Maneuver.hh"
#include "TimerTarget.hh"
#include "PauseTypes.hh"

//@ Usage-Classes

//@ End-Usage



class NifManeuver :  public Maneuver, public TimerTarget
{
  public:

	NifManeuver(ManeuverId maneuverId, IntervalTimer& rPauseTimer);
    ~NifManeuver(void);

    static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL, 
			  const char*       pPredicate = NULL);

	// virtual from Maneuver
	virtual void activate(void);
	virtual Boolean complete(void);
	virtual void clear(void);
	virtual void terminateManeuver(void);
	virtual void newCycle(void);
	virtual Boolean processUserEvent(UiEvent& rUiEvent);

	EventData::EventStatus handleManeuver(const Boolean isStartRequest);

	inline Real32 getNifPressure(void) const;
	inline Int32  getElapsedTime(void) const;
  
	virtual void timeUpHappened(const IntervalTimer& timer);

  protected:

  private:
    NifManeuver(const NifManeuver&);		// not implemented...
    void   operator=(const NifManeuver&);	// not implemented...
	NifManeuver(void);

	//@ Data-Member: &rPauseTimer_
	// a reference to the server IntervalTimer.
	IntervalTimer& rPauseTimer_;

    //@ Data-Member: elapsedTime_             
    //  time elapsed since "starting" this maneuver [ms]
    Int32 elapsedTime_;
    
	//@ Data-Member: isTimedOut_
	// TRUE when the interval timer has expired
	Boolean isTimedOut_;
	
	//@ Data-Member: isButtonPressed_
	// TRUE when user is touching (pressing) the START button, otherwise FALSE
	Boolean isButtonPressed_;
	
	//@ Data-Member: isActiveAckSent_
	// TRUE when audio ack has been sent to acknowledge activation
	Boolean isActiveAckSent_;
	
	//@ Data-Member: baselinePressure_
	// Pressure measured at the start of the maneuver activation when
	// valves are first closed
	Real32 baselinePressure_;
	
	//@ Data-Member: nifPressure_
	// Maximum negative inspiratory force achieved during maneuver
	Real32 nifPressure_;
	
};



// Inlined methods...
#include "NifManeuver.in"


#endif // NifManeuver_HH 
