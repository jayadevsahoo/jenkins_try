#ifndef LungFlowExpTrigger_HH
#define LungFlowExpTrigger_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: LungFlowExpTrigger - Triggers exhalation based on  
//      lung flow. 
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/LungFlowExpTrigger.hhv   25.0.4.0   19 Nov 2013 13:59:52   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004   By:   rhj    Date: 05-March-2007      DR Number: 6359
//  Project:  RESPM
//  Description:
//        Resolve merge conflict for RM to Baseline 
//
//  Revision: 003   By: syw   Date:  20-Jul-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//		PAV project-related changes:
//		Added isUsePavEsens_ flag and set method.
//
//  Revision: 002  By:  syw    Date:  23-Jun-1999    DR Number: DCS 5447
//       Project:  ATC
//       Description:
//			Add DCS 5322 to header
//
//  Revision: 001  By: sah     Date:  12-Jan-1999    DR Number: 5321, 5322
//  Project:  ATC
//  Description:
//		ATC initial release.
//      Changed 'SoftFault()' method to a non-inlined method.
//
//=====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"

//@ Usage-Classes

#include "BreathTrigger.hh"

//@ End-Usage

class LungFlowExpTrigger : public BreathTrigger
{
  public:
    LungFlowExpTrigger( void) ;
    virtual ~LungFlowExpTrigger( void) ;

    static void SoftFault( const SoftFaultID softFaultID,
						   const Uint32      lineNumber,
						   const char*       pFileName  = NULL, 
						   const char*       pPredicate = NULL) ;
  
	inline void setDesiredFlow( const Real32 desiredFlow) ;
	inline void setIsUsePavEsens( void) ;
	inline void setIsUseLpmEsens( void) ;
	virtual void enable( void) ;

  protected:
    virtual Boolean triggerCondition_( void) ;

  private:
    LungFlowExpTrigger( const LungFlowExpTrigger& ) ;	// not implemented...
    void   operator=( const LungFlowExpTrigger& ) ;		// not implemented...

	//@ Data-Member: peakDesiredFlow_
	// peak desired flow
	Real32 peakDesiredFlow_ ;

	//@ Data-Member: desiredFlow_
	// desired flow
	Real32 desiredFlow_ ;

	//@ Data-Member: isUsePavEsens_
	// set if PA esens is to be used
	Boolean isUsePavEsens_ ;

	//@ Data-Member: isUseLpmEsens_
	// set if esens is to be used as absolute Lpm
	Boolean isUseLpmEsens_ ;

} ;

// Inlined methods...
#include "LungFlowExpTrigger.in"


#endif // LungFlowExpTrigger_HH
 
