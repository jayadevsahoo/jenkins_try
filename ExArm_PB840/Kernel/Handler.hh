#ifndef Handler_HH
#define Handler_HH

//====================================================================
// This is a proprietary work to which Nellcor Puritan Bennett
// corporation of California claims exclusive right.  No part of this
// work may be used, disclosed, reproduced, sorted in an information
// retrieval system, or transmitted by any means, electronic,
// mechanical, photocopying, recording, or otherwise without the prior
// written permission of Nellcor Puritan Bennett Corporation.
//
//     Copyright (c) 1995, Nellcor Puritan Bennett Corporation
//=====================================================================
 

//============================ H E A D E R   D E S C R I P T I O N ====
// Filename: Handler.hh - POST Unexpected Exception Handler.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Kernel/vcssrc/Handler.hhv   25.0.4.0   19 Nov 2013 14:13:54   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002    By: Gary Cederquist  Date: 15-MAY-1997  DR Number: 2085
//    Project: Sigma (R8027)
//    Description:
//       Changed exception stack frame declaration so Handler.cc can
//       decode the stack frame for logging of auxiliary information.
//
//  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//=====================================================================

#include "SigmaTypes.hh"

#define  VECTOR_ACCESS_FAULT    (2)
#define  VECTOR_NMI             (31)
 
struct FORMAT2_3
{
   char   * effective_address;
};
 
struct FORMAT7
{
   char   * effective_address;
   Uint16   special_status_register;
   Uint16   write_back3_status;
   Uint16   write_back2_status;
   Uint16   write_back1_status;
   Uint8  * fault_address;
   Uint8  * write_back_3_address;
   Uint32   write_back_3_data;
   Uint8  * write_back_2_address;
   Uint32   write_back_2_data;
   Uint8  * write_back_1_address;
   Uint32   write_back_1_data;
   Uint32   push_data_lw1;
   Uint32   push_data_lw2;
   Uint32   push_data_lw3;
};
 
struct EXCEPTION_STACK_FRAME
{
   Uint16   sr;
   Uint16   pc_high;
   Uint16   pc_low;
   Uint16   format_and_vector_offset;
 
   union
   {
      FORMAT2_3  format2_3;
      FORMAT7    format7;
 
   } frame;
 
};
 
extern void  UnexpectedInterruptHandler(char *stack_pointer);
extern void  DebugUnexpectedInterruptHandler(char *stack_pointer);


#endif // Handler_HH
