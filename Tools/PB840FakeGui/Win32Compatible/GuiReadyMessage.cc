#include "StdAfx.h"
#include "GuiReadyMessage.hh"


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~TaskControlMessage [destructor]
//
//@ Interface-Description
//  TaskControlMessage destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

TaskControlMessage::~TaskControlMessage()
{
    CALL_TRACE("~TaskControlMessage()");
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TaskControlMessage [constructor]
//
//@ Interface-Description
//  The TaskControlMessage constructor accepts an enumerated class
//  identifier.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Initializes private data classId_ from the classId argument.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

TaskControlMessage::TaskControlMessage(enum Sys_Init::InitClassId classId)
: classId_(classId)
{
    CALL_TRACE("TaskControlMessage()");
    // $[TI1]
}

void TimeStamp::now()
{
    CTime t = CTime::GetCurrentTime();

    year_ = t.GetYear();
    month_ = t.GetMonth();
    dayOfMon_ = t.GetDay();
    hour_ = t.GetHour();
    min_ = t.GetMinute();
    sec_ = t.GetSecond();
    hsec_ = 0;

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: PostAgent [constructor]
//
//@ Interface-Description
//  Default constructor.  Conditionally compiled for either the BD or
//  the GUI since some POST data on the BD is not available on the GUI.
//  It initializes its private data members by accessing static POST 
//  data.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
PostAgent::PostAgent(void)
     :powerDownTime_        ()
     ,powerUpTime_          ()
     ,downtimeStatus_       (DOWN_LONG)
     ,startupState_         (ACSWITCH)
     ,postFailed_           (FALSE)
     ,backgroundFailed_     (PASSED)
     ,serviceModeRequested_ (FALSE)
{
    
    CALL_TRACE("PostAgent(void)");

    strcpy_s(  kernelPartNumber_
            , "KERNAL_PARTNO" );

    // $[TI2]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~PostAgent [destructor]
//
//@ Interface-Description
//  Default destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

PostAgent::~PostAgent(void)
{
    CALL_TRACE("~PostAgent(void)");
    // $[TI1]
}

SerialNumber::SerialNumber()
{
    strcpy_s( data_, "XXXXXXXXXX") ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GuiReadyMessage [constructor]
//
//@ Interface-Description
//  The GuiReadyMessage constructor uses the state and service-required
//  arguments to intialize the contents of the message.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls the base class TaskControlMessage constructor with the
//  classId for this message.  Initializes its private data members.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

GuiReadyMessage::GuiReadyMessage(
    enum SigmaState state,
    Boolean service_required)

    :  TaskControlMessage(Sys_Init::ID_GUIREADYMESSAGE)
      ,state_(state)
      ,serviceRequired_(service_required)
      ,postAgent_()
      ,serialNumber_()
{
    CALL_TRACE("GuiReadyMessage(Int32, enum SigmaState, Boolean)");
    // $[TI1]
}

GuiReadyMessage::~GuiReadyMessage()
{
    CALL_TRACE("~GuiReadyMessage()");
}




const char * GuiReadyMessage::nameOf() const
{
    return NULL;
}

void GuiReadyMessage::dispatch(const AppContext & context) const
{
    
}

TimeStamp::TimeStamp()
{
    now();
}