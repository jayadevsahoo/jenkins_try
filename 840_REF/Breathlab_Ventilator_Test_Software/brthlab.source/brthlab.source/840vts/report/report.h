/*****************************************************************************/
/*   Module Name:    report.h                                            
/*   Purpose:      Purpose                                 
/*                                                                          
/*                                                                           
/*   Requirements:   This module implements requirements detailed in         
/*          <75359-93>                                                       
/*
/*   Modification History:
/*   
/*   Description of Change			        By			      Date 
/*   ---------------------					-----   		  ------
/*   Added an Alarm Log enum to LOGTYPES.   RHJ               27 Nov,2007                                       
/*
/*   Reports to preview or delete			Lakshmi(CGS)	  01 Dec,2000
/*   function prototypes are added.
/*
/*	 Critical Observation during Phase1 
/* 	 is incorportated.						Kathirvel(CGS)    27 Sep,2000
/*
/*   $Revision::   1.1      $                                                
/*                                                                          
/*****************************************************************************/

/*****************************************************************************
THIS IS A PROPRIETARY WORK TO WHICH NELLCOR PURITAN BENNETT CORPORATION CLAIMS
EXCLUSIVE RIGHT.  NO PART OF THIS WORK MAY BE USED, DISCLOSED, REPRODUCED,
STORED IN AN INFORMATION RETIEVAL SYSTEM, OR TRANSMITTED, IN ANY FORM OR BY ANY
MEANS ELECTRONIC, MECHANICAL, PHOTOCOPYING, RECORDING OR OTHERWISE WITHOUT THE
PRIOR WRITTEN PERMISSION OF NELLCOR PURITAN BENNETT CORPORATION.
 
Copyright (c) 1999, Mallinckrodt Nellcor Puritan Bennett
All rights reserved.
*******************************************************************************/

#ifndef _REPORTS
#define _REPORTS

typedef enum {
	INFO_LOG = 0,
	DIAG_LOG,
	TEST_LOG,
    ALARM_LOG
} LOGTYPES;

void DisplayReport(void);
void CheckForNoNameLog(char** path_name, char** file_name, LOGTYPES log);
int HandleLogPrint( LOGTYPES log, char* filename ); // Modified the statement due to Critical Observation.
void RptCheckItem(int panel, int control); // Modified the statement due to Critical Observation.
void DisplaySelectReports(void);
void PreviewReports(void);
int DeleteReports(void);
void InsertReportsList(void);  

#endif // #ifndef _REPORTS
