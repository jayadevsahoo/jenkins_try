#ifndef AlarmRepository_HH
#define AlarmRepository_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Type: AlarmRepository - Interface between Alarm-Analysis and DCI subsystems.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Alarm-Analysis/vcssrc/AlarmRepository.hhv   25.0.4.0   19 Nov 2013 13:51:16   pvcs  $
//
//@ Modification-Log
//  
//   Revision 001   By:   gdc      Date: 27-FEB-1998      DR Number:  5017
//      Project:   Sigma   (R8027)
//      Description:
//   Initial version to supply alarm state information to Alarm Setup
//   Subscreen Slider Buttons.
//   
//====================================================================

#include "Sigma.hh"

//@ Usage-Classes
#include "Alarm.hh"
#include "AlarmName.hh"
//@ End-Usage

class AlarmRepository
{
  public:

    static void Initialize(void);

#if defined(SIGMA_GUI_CPU) || defined(SIGMA_DEVELOPMENT)
    static Alarm::Urgency GetStatus(AlarmName alarmName);
    static void SetStatus(AlarmName alarmName, Alarm::Urgency urgency);
#endif

    static void          SoftFault(const SoftFaultID  softFaultID,
                                   const Uint32       lineNumber,
                                   const char*        pFileName = NULL,
                                   const char*        pPredicate = NULL);

  private:

    AlarmRepository(void);             // Declared only
    AlarmRepository(const AlarmRepository&);  // Declared only
    ~AlarmRepository(void);            // Declared only
    //operator=(const AlarmRepository&); // Declared only

};

#endif // AlarmRepository_HH 
