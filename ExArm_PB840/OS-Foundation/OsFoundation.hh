#ifndef OsFoundation_HH
#define OsFoundation_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  OsFoundation - The OsFoundation Subsystem.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/OS-Foundation/vcssrc/OsFoundation.hhv   25.0.4.0   19 Nov 2013 14:15:18   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005    By: gdc              Date: 09-Feb-2009  SCR Number: 6435
//    Project: 840S
//    Description:
//       Moved SerialPort class to OS-Foundation to support serial
//       port functionality on BD for proximal flow sensor.
//
//  Revision: 004    By: rhj              Date: 07-Aug-2007  DCS Number: 6397
//    Project: Trend
//    Description:
//       Added Ostream class.
//
//  Revision: 003    By: Gary Cederquist  Date: 08-Oct-1997  DR Number: 2549
//    Project: Sigma (R8027)
//    Description:
//       Added HiResTimer support.
//
//  Revision: 002    By: Gary Cederquist  Date: 15-APR-1997  DR Number: 1943
//    Project: Sigma (R8027)
//    Description:
//       Added revision headers.
//
//  Revision: 001    By: Greg Hamm        Date: 01-OCT-1996  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//====================================================================

#include "Sigma.hh"
#include "Ipc.hh"

//@ Usage-Classes
//@ End-Usage

class OsFoundation
{
  public:
    //@ Type:  OsClassId
    // Ids of all of the classes/files of this subsystem.
    // Used for Fault Handling.
    enum OsClassId 
    {
        MEMORY              = 0,
        MAILBOX             = 1,
        MSG_QUEUE           = 2,
        MSG_TIMER           = 3,
        MUTEX               = 4,
        TASK                = 5,
        TASK_INFO           = 6,
        TIMER_TBL           = 7,
        OSFOUNDATION_CLASS  = 8,
        TIMESTAMP           = 9,
        OSTIMESTAMP         = 10,
        IPC_QUEUE           = 11,
        BUFFERPOOL          = 12,
        HIRESTIMER          = 13,
		OSTREAM             = 14,
		SERIAL_PORT         = 15,
    
        NUM_OSFOUNDATION_CLASSES
    };

};

#endif // OsFoundation_HH 
