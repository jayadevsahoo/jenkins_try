
#ifndef AveragedBreathData_HH
#define AveragedBreathData_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: AveragedBreathData - Averaged spirometric data.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/AveragedBreathData.hhv   25.0.4.0   19 Nov 2013 13:59:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 008  By: rhj     Date:  24-Feb-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//		Added prevRespiratoryRate_ and getPrevRespiratoryRate();
//
//  Revision: 007  By: jja     Date:  30-June-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//		VTPC project-related changes:
//		Added spontFtotVtRatio_ calculation.
//
//  Revision: 006  By:  jja    Date:  20-Apr-00    DR Number: 5708 
//	Project:  NeoMode
//	Description:
//		Revise Pmean to be an averaged value instead of  
//              single breath.
//
//  Revision: 005  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 004  By:  sp    Date:  9-Oct-1996    DR Number:600
//       Project:  Sigma (R8027)
//       Description:
//             Removed unit test methods.
//
//  Revision: 003  By:  iv    Date:  4-Mar-1996    DR Number:674
//       Project:  Sigma (R8027)
//       Description:
//             Remove #ifndef.
//
//  Revision: 002  By:  sp    Date:  01-Feb-1994    DR Number:600 
//       Project:  Sigma (R8027)
//       Description:
//             Change BreathPhaseScheduler to SchedulerId.
//             Change the lastUpdateTS_ type to OsTimeStamp.
//             Add unit test methods.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#  include "Sigma.hh"

#include "Breath_Delivery.hh"

//@ Usage-Classes
#include "TimerTarget.hh"

#include "OsTimeStamp.hh"

class IntervalTimer;
//@ End-Usage


class AveragedBreathData : public TimerTarget {
  public:
    AveragedBreathData(IntervalTimer& rTimer);
    ~AveragedBreathData(void);

    void updateAveragedBreathData(const Int32 msg);
    virtual void timeUpHappened(const IntervalTimer& timer);

    static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL, 
			  const char*       pPredicate = NULL);
    Real32 getPrevRespiratoryRate(void);
  protected:

  private:
    AveragedBreathData(const AveragedBreathData&);	// not implemented...
    AveragedBreathData(void);                       // not implemented...
    void   operator=(const AveragedBreathData&);	// not implemented...

    void updateDryFactor_(void);

    //@ Data-Member:  exhMinuteVol_
    // calculated exhaled volume for mandatory and spont breaths.
    Real32  exhMinuteVol_;	

    //@ Data-Member:  exhSpontMinuteVol_
    // calculated exhaled volume for spont breaths only.
    Real32  exhSpontMinuteVol_;	

    //@ Data-Member:  inspMinuteVolume_
    // calculated delivered volume for spont and mandatory breaths only.
    Real32  inspMinuteVolume_;

    //@ Data-Member:  respiratoryRate_
    // calculated respiratory rate for mandatory and spont breaths.
    Real32  respiratoryRate_;	

    //@ Data-Member: meanCircuitPressure_
    // calculated mean circuit pressure for mandatory and spont breaths
    Real32 meanCircuitPressure_;

    //@ Data-Member:  rAvgBreathDataTimer_
    // a reference to the timer server
    IntervalTimer& rAvgBreathDataTimer_;

    //@ Data-Member:  lastUpdateTS_
    // a time stamp for last update
    OsTimeStamp lastUpdateTS_;

    //@ Data-Member:  spontFtotVtRatio_
    // calculated rate to tidal volume ratio for spont breaths only.
    Real32  spontFtotVtRatio_;	

    //@ Data-Member:  prevRespiratoryRate_
    // Stores the previous respiratory rate for mandatory and spont breaths.
    Real32  prevRespiratoryRate_;	

#ifdef SIGMA_UNIT_TEST
  public:
    void write(void);
#endif //SIGMA_UNIT_TEST


};


#endif // AveragedBreathData_HH 
