// TestSessionSet.h : Implementation of the CTestSessionSet class



// CTestSessionSet implementation

// code generated on Thursday, September 17, 2009, 3:51 PM

#include "stdafx.h"
#include "TestSessionSet.h"
IMPLEMENT_DYNAMIC(CTestSessionSet, CRecordset)

CTestSessionSet::CTestSessionSet(CDatabase* pdb)
	: CRecordset(pdb)
{
	m_start_event_sequence_id = 0;
	m_stop_event_sequence_id = 0;
	m_session_start_time = "";
	m_session_stop_time = "";
	m_user_name = "";
	m_hw_version = "";
	m_test_session_id = "";
	m_nFields = 7;
	m_nDefaultType = dynaset;
}
CString CTestSessionSet::GetDefaultConnect()
{
	return _T("DSN=PostgreSQL30;DATABASE=viking_integration_test;SERVER=127.0.0.1;PORT=5432;UID=postgres;PWD=its4me@1234;SSLmode=disable;ReadOnly=0;Protocol=7.4;FakeOidIndex=0;ShowOidColumn=0;RowVersioning=0;ShowSystemTables=0;ConnSettings=;Fetch=100;Socket=4096;UnknownSizes=0;MaxVarcharSize=255;MaxLongVarcharSize=8190;Debug=0;CommLog=0;Optimizer=0;Ksqo=1;UseDeclareFetch=0;TextAsLongVarchar=1;UnknownsAsLongVarchar=0;BoolsAsChar=1;Parse=0;CancelAsFreeStmt=0;ExtraSysTablePrefixes=dd_;;LFConversion=1;UpdatableCursors=1;DisallowPremature=0;TrueIsMinus1=0;BI=0;ByteaAsLongVarBinary=0;UseServerSidePrepare=0;LowerCaseIdentifier=0;XaOpt=1");
//	return _T("DSN=PostgreSQL30;DATABASE=viking_integration_test;SERVER=127.0.0.1;PORT=5432;UID=michael.ekaireb;PWD=its4me;SSLmode=disable;ReadOnly=0;Protocol=7.4;FakeOidIndex=0;ShowOidColumn=0;RowVersioning=0;ShowSystemTables=0;ConnSettings=;Fetch=100;Socket=4096;UnknownSizes=0;MaxVarcharSize=255;MaxLongVarcharSize=8190;Debug=0;CommLog=0;Optimizer=0;Ksqo=1;UseDeclareFetch=0;TextAsLongVarchar=1;UnknownsAsLongVarchar=0;BoolsAsChar=1;Parse=0;CancelAsFreeStmt=0;ExtraSysTablePrefixes=dd_;;LFConversion=1;UpdatableCursors=1;DisallowPremature=0;TrueIsMinus1=0;BI=0;ByteaAsLongVarBinary=0;UseServerSidePrepare=0;LowerCaseIdentifier=0;XaOpt=1");
}

CString CTestSessionSet::GetDefaultSQL()
{
	return _T("[TestResults].[test_session]");
}

void CTestSessionSet::DoFieldExchange(CFieldExchange* pFX)
{
	pFX->SetFieldType(CFieldExchange::outputColumn);
// Macros such as RFX_Text() and RFX_Int() are dependent on the
// type of the member variable, not the type of the field in the database.
// ODBC will try to automatically convert the column value to the requested type
	RFX_BigInt(pFX, _T("[start_event_sequence_id]"), m_start_event_sequence_id);
	RFX_BigInt(pFX, _T("[stop_event_sequence_id]"), m_stop_event_sequence_id);
	RFX_Text(pFX, _T("[session_start_time]"), m_session_start_time);
	RFX_Text(pFX, _T("[session_stop_time]"), m_session_stop_time);
	RFX_Text(pFX, _T("[user_name]"), m_user_name);
	RFX_Text(pFX, _T("[hw_version]"), m_hw_version);
	RFX_Text(pFX, _T("[test_session_id]"), m_test_session_id);

}
/////////////////////////////////////////////////////////////////////////////
// CTestSessionSet diagnostics

#ifdef _DEBUG
void CTestSessionSet::AssertValid() const
{
	CRecordset::AssertValid();
}

void CTestSessionSet::Dump(CDumpContext& dc) const
{
	CRecordset::Dump(dc);
}
#endif //_DEBUG


