/*****************************************************************************/
/*   Module Name:    perpanel.c                                            
/*   Purpose:      840 Ventilator Final Test                                              
/*
/*   Modification History:
/*
/*   Description of Change			        By			      Date 
/*   ---------------------					-----   		  ------
/*	 Test Executive related functions 
/* 	 are removed and replaced with			Hamsavally(CGS)   20 Dec,2000
/*   respective functions.	
/*							 
/*   Modify panel attributes at run-time    Dave Richardson   20 Nov, 2002
/*   to swap the Ti and Pi pressure 
/*   parameter controls displayed to mimic
/*   the vent screen during pressure testing.
/*
/*  Requirements:   This module implements requirements detailed in        
/*          <72361-93>  
/*                                                                           
/*   $Revision::   1.2      $                                                
/*                                                                           
/*   $Log:   //pleasa1apo2/swqa/data/arc/840_CPVT_VTS/Program/testutil/perpanel.c-arc  $
 * 
 *    Rev 1.2   04 Dec 2002 15:36:42   Buchman
 * 840 VTS build done
 * 
 *    Rev 1.1   20 Nov 2002 15:09:16   Richardson
 * Updated per SCR # 48

/*****************************************************************************/

/*****************************************************************************
THIS IS A PROPRIETARY WORK TO WHICH PURITAN-BENNETT CORPORATION CLAIMS
EXCLUSIVE RIGHT.  NO PART OF THIS WORK MAY BE USED, DISCLOSED, REPRODUCED,
STORED IN AN INFORMATION RETIEVAL SYSTEM, OR TRANSMITTED, IN ANY FORM OR BY ANY
MEANS ELECTRONIC, MECHANICAL, PHOTOCOPYING, RECORDING OR OTHERWISE WITHOUT THE
PRIOR WRITTEN PERMISSION OF PURITAN-BENNETT CORPORATION.

Copyright (c) 1999-2002, Puritan-Bennett
All rights reserved.
*******************************************************************************/

/***********************************************/
/****************** INCLUDES *******************/
/***********************************************/

#include <ansi_c.h>
#include <toolbox.h>
#include <userint.h>
#include <utility.h>
#include "globals.h"
#include "72500.h"
#ifdef DEMOMODE
#include "72361.h" // DEMO_TIMEOUT
#endif
#include "cpvtmain.h"
#include "ldb.h"
#include "mainpnls.h"
#include "panapi.h"
#include "prmptpnl.h"
#include "version.h"
#include "testing.h"

/***********************************************/
/************** GLOBAL VARIABLES ***************/
/***********************************************/

int  PerfPanel;
int  FlashId = 0;
int  FlashIndex[MAX_FLASH];
int  FlashBin[MAX_FLASH];
int  CurrentFlash;

/***********************************************/
/************ FUNCTION DECLARATION  ************/
/***********************************************/

/*****************************************************************************/
/* Name:     DisplaySST                                                    
/*                                                                           
/* Purpose:  Displays the run short self test panel
/*                                          
/*                                                                           
/* Example Call: int DisplaySST();                                    
/*                                                                           
/* Input:    path -  uir file to load panel from (no longer used).         
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   always 0                                                         
/*                                                                           
/*****************************************************************************/
int DisplaySST(void)
{
	int err = 0,
	sstPanel,
	handle,
	control;
	BOOL doneFlag = FALSE;

	#ifdef DEMOMODE
	#ifdef USE_DEMO_TIMEOUT
	double demoTimeOut;
	#endif
	#endif

	sstPanel = TestPrompt(SSTPanel);
	doneFlag = FALSE;
	#ifdef DEMOMODE
	#ifdef USE_DEMO_TIMEOUT
	demoTimeOut = Timer() + (double)DEMO_TIMEOUT;
	#endif
	#endif
	while(!doneFlag){
		DisplayPanel(sstPanel);
		GetUserEvent(0, &handle, &control); /* Check for user event */
		#ifdef DEMOMODE
		#ifdef USE_DEMO_TIMEOUT
		if(Timer() >= demoTimeOut){
			handle = sstPanel;
			control = ALARMPANEL_DONE;
			Beep();
		}
		#endif
		#endif
		if(handle != sstPanel){
			continue;
		}

		if(control == ALARMPANEL_DONE){
			doneFlag = TRUE;
		}
	}

	TestPrompt(Clear);

	return err;   
}


/*****************************************************************************/
/* Name:     InitPerfPanel                                                    
/*                                                                           
/* Purpose:  Sets up the performance panel.
/*                                          
/*                                                                           
/* Example Call: int InitPerfPanel(int *panelId, int *done);                                    
/*                                                                           
/* Input:    panelId - panel handle of the new perf panel- passed by reference
/*			 done -  control constant of the done button
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   0 if panel loaded, 1 if error occured                                                         
/*                                                                           
/*****************************************************************************/
int InitPerfPanel(int *panelId, int *done)
{
	int x,
	err = 0;
	char* testInfo[8];
	int CtrlWidth;

	FlashId = 0;
	CurrentFlash = 0;

	/* Setup the panel */
	if( (PerfPanel = LoadPanel(0, LDBf(Ldb_MainpanelFile), PERF_PANEL)) < 0 ){
		err = 1;
	}
	SetCtrlAttribute(PerfPanel, PERF_PANEL_O2TIMER,   ATTR_ENABLED, FALSE);
	SetCtrlAttribute(PerfPanel, PERF_PANEL_O2PERCENT, ATTR_VISIBLE, FALSE);
	SetCtrlAttribute(PerfPanel, PERF_PANEL_O2READING, ATTR_VISIBLE, FALSE);

	// Run-time adjustments until panel redesign
	SetPanelAttribute(PerfPanel, ATTR_FLOATING,   VAL_FLOAT_ALWAYS);
	SetPanelAttribute(PerfPanel, ATTR_CLOSE_CTRL, PERF_PANEL_ABORT);
	SetCtrlAttribute(PerfPanel, PERF_PANEL_INST_BOX, ATTR_SCROLL_BAR_COLOR,   0X505050);
	SetCtrlAttribute(PerfPanel, PERF_PANEL_INST_BOX, ATTR_MAX_ENTRY_LENGTH,   -1);
	SetCtrlAttribute(PerfPanel, PERF_PANEL_INST_BOX, ATTR_FIRST_VISIBLE_LINE, 0);
	SetCtrlAttribute(PerfPanel, PERF_PANEL_INST_BOX, ATTR_WRAP_MODE,          VAL_WORD_WRAP);
	SetCtrlAttribute(PerfPanel, PERF_PANEL_INST_BOX, ATTR_EXTRA_LINES,        -1);
	SetCtrlAttribute(PerfPanel, PERF_PANEL_INST_BOX, ATTR_ENTER_IS_NEWLINE,   FALSE);
	SetCtrlAttribute(PerfPanel, PERF_PANEL_INST_BOX, ATTR_SCROLL_BAR_SIZE,    VAL_LARGE_SCROLL_BARS);
	MoveBehind(PerfPanel, PERF_PANEL_INST_BOX, PERF_PANEL_DECORATION_2);
	MoveBehind(PerfPanel, PERF_PANEL_DECORATION_2, PERF_PANEL_TESTTITLE);
	SetCtrlVal(PerfPanel, PERF_PANEL_PI_TITLE, "Pi");
	GetCtrlAttribute(PerfPanel, PERF_PANEL_VMAX,     ATTR_WIDTH,            &CtrlWidth);
	SetCtrlAttribute(PerfPanel, PERF_PANEL_WAVEFORM, ATTR_LABEL_FONT,       "Arial");
	SetCtrlAttribute(PerfPanel, PERF_PANEL_WAVEFORM, ATTR_LABEL_POINT_SIZE, 12);
	SetCtrlAttribute(PerfPanel, PERF_PANEL_WAVEFORM, ATTR_WIDTH,            CtrlWidth);

	SetCtrlAttribute(PerfPanel, PERF_PANEL_DONE,  ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT);
	SetCtrlAttribute(PerfPanel, PERF_PANEL_ABORT, ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT);

	SetActiveCtrl(PerfPanel, PERF_PANEL_DONE);

	// Swap the Pi & Ti controls
	SetCtrlAttribute(PerfPanel, PERF_PANEL_PI,       ATTR_LEFT, 206);
	SetCtrlAttribute(PerfPanel, PERF_PANEL_PI_TITLE, ATTR_LEFT, 249);
	SetCtrlAttribute(PerfPanel, PERF_PANEL_PI_CM,    ATTR_LEFT, 276);
	SetCtrlAttribute(PerfPanel, PERF_PANEL_PI_H20,   ATTR_LEFT, 269);
	SetCtrlAttribute(PerfPanel, PERF_PANEL_TI_TITLE, ATTR_LEFT, 346);
	SetCtrlAttribute(PerfPanel, PERF_PANEL_TI,       ATTR_LEFT, 306);
	SetCtrlAttribute(PerfPanel, PERF_PANEL_TI_S,     ATTR_LEFT, 385);

	// Title the instructions box	
	ResetTextBox(PerfPanel, PERF_PANEL_TESTTITLE, "");
	SetCtrlVal(PerfPanel, PERF_PANEL_TESTTITLE, TestName[CurrLanguage][iCurrentTest]);

	(*panelId) = PerfPanel;
	(*done) = PERF_PANEL_DONE;

	for(x = 0; x < MAX_FLASH; x++){
		FlashBin[x] = 0;
	}

	SetManTypeDisplay(        D_OFF, 0); 
	SetTriggerTypeDisplay(    D_OFF, 0);  
	SetIdealBodyWeightDisplay(D_OFF, 0);  
	SetBreathPerMinuteDisplay(D_OFF, 0.0); 
	SetVolumeDisplay(         D_OFF, 0); 
	SetMaxFlowDisplay(        D_OFF, 0.0); 
	SetTplDisplay(            D_OFF, 0.0); 
	SetWaveFormDisplay(       D_OFF, 0);  
	SetPsensDisplay(          D_OFF, 0); 
	SetO2Display(             D_OFF, 0); 
	SetPEEPDisplay(           D_OFF, 0.0);
	SetTiDisplay(             D_OFF, 0.0); 
	SetPiDisplay(             D_OFF, 0.0);
	SetPDisplay(              D_OFF, 0);   
	SetInstructionBox(D_OFF, "", "");

	return err;
}


/*****************************************************************************/
/* Name:      FlashOn                                                     
/*                                                                           
/* Purpose:  Marks a control to flash on the perfpanel
/*                                          
/*                                                                           
/* Example Call:   void FlashOn(int control);                                    
/*                                                                           
/* Input:    control - constant of the control to flash          
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
void FlashOn(int control)
{
	switch(control){
		case F_VOLUME:  FlashId = PERF_PANEL_VT;      break; 
		case F_BREATH:  FlashId = PERF_PANEL_FSET;    break;
		case F_FLOW:    FlashId = PERF_PANEL_VMAX;    break;
		case F_TPL:     FlashId = PERF_PANEL_TPL;     break;
		case F_PSENS:   FlashId = PERF_PANEL_PSENS;   break;
		case F_PEEP:    FlashId = PERF_PANEL_PEEP;    break;
		case F_O2:      FlashId = PERF_PANEL_O2;      break;
		case F_PI:      FlashId = PERF_PANEL_PI;      break;
		case F_P:       FlashId = PERF_PANEL_P;       break; 
		case F_TI:      FlashId = PERF_PANEL_TI;      break;
		case F_IBW:     FlashId = PERF_PANEL_BODY_WEIGHT; break;
		case F_MANTYPE: FlashId = PERF_PANEL_MANTYPE; break;
		case F_TRIGGER: FlashId = PERF_PANEL_TRIGGER; break;
	}

	FlashBin[CurrentFlash++] = FlashId;

	return;
}


/*****************************************************************************/
/* Name:   ResetFlashOn                                                    
/*                                                                           
/* Purpose:  Resets all controls to a none flash state
/*                                          
/*                                                                           
/* Example Call:   void ResetFlashOn(int control);                                    
/*                                                                           
/* Input:    control - control constant to reset         
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
void ResetFlashOn(int control)
{
	int x;

	CurrentFlash = 0;

	for(x = 0; x < MAX_FLASH; x++){
		if(FlashBin[x]){
			SetCtrlAttribute(PerfPanel, FlashBin[x], ATTR_LABEL_COLOR, VAL_WHITE); 
			FlashBin[x] = 0;
		}
	}
	FlashOn(control);

	return;
}


/*****************************************************************************/
/* Name:     SetInstructionBox                                                     
/*                                                                           
/* Purpose:  sets the text of the perf panel instruction box...
/*                                          
/*                                                                           
/* Example Call:  void SetInstructionBox(int display, char *title, char *inst);                                    
/*                                                                           
/* Input:    display - D_OFF = hide the box, else show it.
/*			 title - title of the box
/*			 inst - instruction text
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
void SetInstructionBox(int display, char *title, char *inst)
{
	char cBuf[1024]; // note: must be large enough for longest message
	int lines;
	int vislines;

	if(display){
		strcpy(cBuf, inst);
		ResetTextBox(PerfPanel, PERF_PANEL_INST_BOX , " ");
		ReplaceTextBoxLine(PerfPanel, PERF_PANEL_INST_BOX, 0, cBuf);
		SetCtrlAttribute(PerfPanel, PERF_PANEL_INST_BOX, ATTR_VISIBLE, TRUE);
		GetCtrlAttribute(PerfPanel, PERF_PANEL_INST_BOX, ATTR_TOTAL_LINES,   &lines);
		GetCtrlAttribute(PerfPanel, PERF_PANEL_INST_BOX, ATTR_VISIBLE_LINES, &vislines);
		if(lines > vislines){
			SetCtrlAttribute(PerfPanel, PERF_PANEL_INST_BOX, ATTR_SCROLL_BARS,      VAL_VERT_SCROLL_BAR);
		}
		else{
			SetCtrlAttribute(PerfPanel, PERF_PANEL_INST_BOX, ATTR_SCROLL_BARS,      VAL_NO_SCROLL_BARS);
		}
	}
	else{
		SetCtrlAttribute(PerfPanel,PERF_PANEL_INST_BOX, ATTR_VISIBLE, FALSE);
	}

	return;
}


/*****************************************************************************/
/* Name:      SetManTypeDisplay                                                      
/*                                                                           
/* Purpose:  Sets the man type display on the perf panel
/*                                          
/*                                                                           
/* Example Call:  void SetManTypeDisplay(int display,int mode);                                    
/*                                                                           
/* Input:    display - show or hide the man type
/*			 mode - man type, PC or VC
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
void SetManTypeDisplay(int display, int mode)
{
	char cBuf[10];

	if(display){
		switch(mode){
			case PC:
				sprintf(cBuf, LDBf(Ldb_PC));
				break;
			case VC:
				sprintf(cBuf, LDBf(Ldb_VC));
				break;
		} //end switch

		SetCtrlAttribute(PerfPanel,PERF_PANEL_MANTYPE, ATTR_LABEL_TEXT, cBuf);       
		SetCtrlAttribute(PerfPanel,PERF_PANEL_MANTYPE, ATTR_VISIBLE,    TRUE);
	}
	else{
		SetCtrlAttribute(PerfPanel,PERF_PANEL_MANTYPE, ATTR_VISIBLE, FALSE);
	}

	return;
}


/*****************************************************************************/
/* Name:     SetTriggerTypeDisplay                                                 
/*                                                                           
/* Purpose:  sets the trigger display on the perf panel
/*                                          
/*                                                                           
/* Example Call:   void SetTriggerTypeDisplay(int display,int mode);                                    
/*                                                                           
/* Input:    display - show or hide trigger type
/*			 mode - PTRIG or VTRIG
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
void SetTriggerTypeDisplay(int display, int mode)
{
	char cBuf[10];

	if(display){
		switch(mode){
			case PTRIG:
				sprintf(cBuf, LDBf(Ldb_PTRIG));
				SetCtrlAttribute(PerfPanel,PERF_PANEL_VTRIG_L,   ATTR_VISIBLE, FALSE);
				SetCtrlAttribute(PerfPanel,PERF_PANEL_VTRIG_MIN, ATTR_VISIBLE, FALSE);
				SetCtrlAttribute(PerfPanel,PERF_PANEL_PSENS_CM,  ATTR_VISIBLE, TRUE);
				SetCtrlAttribute(PerfPanel,PERF_PANEL_PSENS_H20, ATTR_VISIBLE, TRUE);
				SetCtrlVal(PerfPanel, PERF_PANEL_PSENS_TITLE, LDBf(Ldb_Psens)); 
				break;
			case VTRIG:
				sprintf(cBuf, LDBf(Ldb_VTRIG));
				SetCtrlAttribute(PerfPanel,PERF_PANEL_PSENS_CM,  ATTR_VISIBLE, FALSE);
				SetCtrlAttribute(PerfPanel,PERF_PANEL_PSENS_H20, ATTR_VISIBLE, FALSE);
				SetCtrlAttribute(PerfPanel,PERF_PANEL_VTRIG_L,   ATTR_VISIBLE, TRUE);
				SetCtrlAttribute(PerfPanel,PERF_PANEL_VTRIG_MIN, ATTR_VISIBLE, TRUE);
				SetCtrlVal(PerfPanel, PERF_PANEL_PSENS_TITLE, LDBf(Ldb_Vsens)); 
				break;
		} //end switch

		SetCtrlAttribute(PerfPanel,PERF_PANEL_TRIGGER,     ATTR_LABEL_TEXT, cBuf);
		SetCtrlAttribute(PerfPanel,PERF_PANEL_TRIGGER,     ATTR_VISIBLE,    TRUE);
		SetCtrlAttribute(PerfPanel,PERF_PANEL_PSENS_TITLE, ATTR_VISIBLE,    TRUE);
	}
	else{
		SetCtrlAttribute(PerfPanel,PERF_PANEL_TRIGGER,     ATTR_VISIBLE, FALSE);
		SetCtrlAttribute(PerfPanel,PERF_PANEL_PSENS_CM,    ATTR_VISIBLE, FALSE);
		SetCtrlAttribute(PerfPanel,PERF_PANEL_PSENS_H20,   ATTR_VISIBLE, FALSE);
		SetCtrlAttribute(PerfPanel,PERF_PANEL_PSENS_TITLE, ATTR_VISIBLE, FALSE);
	}

	return;
}


/*****************************************************************************/
/* Name:     SetIdealBodyWeightDisplay                                                 
/*                                                                           
/* Purpose:  Sets Ideal Body Weight Display on perf panel 
/*                                          
/*                                                                           
/* Example Call:   void SetIdealBodyWeightDisplay(int display,int reading);                                    
/*                                                                           
/* Input:    display - show or hide display
/*			 reading - Ideal body wieght to display
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
void SetIdealBodyWeightDisplay(int display, int reading)
{
	char cBuf[10];

	/* reading is in KG */
	if(display){
		sprintf(cBuf, "%d kg", reading);
		SetCtrlAttribute(PerfPanel, PERF_PANEL_BODY_WEIGHT, ATTR_LABEL_TEXT, cBuf);
		SetCtrlAttribute(PerfPanel, PERF_PANEL_BODY_WEIGHT, ATTR_VISIBLE,    TRUE); 
	}
	else{
		SetCtrlAttribute(PerfPanel, PERF_PANEL_BODY_WEIGHT, ATTR_VISIBLE,    FALSE); 
	}

	return;
}


/*****************************************************************************/
/* Name:     SetBreathPerMinuteDisplay                                       
/*                                                                           
/* Purpose:  Sets Breath Per Minute Display
/*                                          
/*                                                                           
/* Example Call:  void SetBreathPerMinuteDisplay(int display, int reading);  
/*                                                                           
/* Input:    display - show or hide display
/*			 reading - breath per minute to display
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
void SetBreathPerMinuteDisplay(int display, double reading)
{
	char cBuf[10];

	if(display){
		if(reading >= 10){
			sprintf(cBuf, "%3d", (int)reading);
		}
		else{
			sprintf(cBuf, "%3.1lf", reading);
		}
		SetCtrlAttribute(PerfPanel, PERF_PANEL_FSET,     ATTR_LABEL_TEXT, cBuf);   
		SetCtrlAttribute(PerfPanel, PERF_PANEL_FSET,     ATTR_VISIBLE, TRUE); 
		SetCtrlAttribute(PerfPanel, PERF_PANEL_FREQ_F,   ATTR_VISIBLE, TRUE);
		SetCtrlAttribute(PerfPanel, PERF_PANEL_AC_MIN,   ATTR_VISIBLE, TRUE);
		SetCtrlAttribute(PerfPanel, PERF_PANEL_AC_TITLE, ATTR_VISIBLE, TRUE);
	}
	else{
		SetCtrlAttribute(PerfPanel, PERF_PANEL_FSET,     ATTR_VISIBLE, FALSE); 
		SetCtrlAttribute(PerfPanel, PERF_PANEL_FREQ_F,   ATTR_VISIBLE, FALSE);
		SetCtrlAttribute(PerfPanel, PERF_PANEL_AC_MIN,   ATTR_VISIBLE, FALSE);
		SetCtrlAttribute(PerfPanel, PERF_PANEL_AC_TITLE, ATTR_VISIBLE, FALSE);
	}

	return;
}


/*****************************************************************************/
/* Name:     SetVolumeDisplay                                                   
/*                                                                           
/* Purpose:  Sets Volume Display  
/*                                          
/*                                                                           
/* Example Call:   void SetVolumeDisplay(int display, int reading);                                    
/*                                                                           
/* Input:    display - show or hide display
/*			 reading - volume to display
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
void SetVolumeDisplay(int display, int reading)
{
	char cBuf[10];

	if(display){
		sprintf(cBuf, "%d", reading);
		SetCtrlAttribute(PerfPanel, PERF_PANEL_VT,       ATTR_LABEL_TEXT, cBuf);   
		SetCtrlAttribute(PerfPanel, PERF_PANEL_VT,       ATTR_VISIBLE, TRUE);
		SetCtrlAttribute(PerfPanel, PERF_PANEL_VT_TITLE, ATTR_VISIBLE, TRUE);
		SetCtrlAttribute(PerfPanel, PERF_PANEL_VT_ML,    ATTR_VISIBLE, TRUE);
	}
	else{ 
		SetCtrlAttribute(PerfPanel, PERF_PANEL_VT,       ATTR_VISIBLE, FALSE);    
		SetCtrlAttribute(PerfPanel, PERF_PANEL_VT_TITLE, ATTR_VISIBLE, FALSE);
		SetCtrlAttribute(PerfPanel, PERF_PANEL_VT_ML,    ATTR_VISIBLE, FALSE);
	}

	return;
}


/*****************************************************************************/
/* Name:     SetMaxFlowDisplay                                                     
/*                                                                           
/* Purpose:  Sets Max Flow Display  
/*                                          
/*                                                                           
/* Example Call:   void SetMaxFlowDisplay(int display, double reading);                                    
/*                                                                           
/* Input:    display - show or hide display
/*			 reading - max flow to display
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
void SetMaxFlowDisplay(int display, double reading)
{
	char cBuf[10];

	if(display){
		if(reading >= 20){
			sprintf(cBuf, "%3d", (int)reading);
		}
		else{
			sprintf(cBuf, "%3.1lf", reading);
		}
		SetCtrlAttribute(PerfPanel, PERF_PANEL_VMAX,       ATTR_LABEL_TEXT, cBuf);   
		SetCtrlAttribute(PerfPanel, PERF_PANEL_VMAX_TITLE, ATTR_VISIBLE, TRUE);
		SetCtrlAttribute(PerfPanel, PERF_PANEL_VMAX,       ATTR_VISIBLE, TRUE);
		SetCtrlAttribute(PerfPanel, PERF_PANEL_VMAX_L,     ATTR_VISIBLE, TRUE); 
		SetCtrlAttribute(PerfPanel, PERF_PANEL_VMAX_MIN,   ATTR_VISIBLE, TRUE);   
	}
	else{ 
		SetCtrlAttribute(PerfPanel, PERF_PANEL_VMAX,       ATTR_VISIBLE, FALSE);    
		SetCtrlAttribute(PerfPanel, PERF_PANEL_VMAX_TITLE, ATTR_VISIBLE, FALSE);
		SetCtrlAttribute(PerfPanel, PERF_PANEL_VMAX_L,     ATTR_VISIBLE, FALSE); 
		SetCtrlAttribute(PerfPanel, PERF_PANEL_VMAX_MIN,   ATTR_VISIBLE, FALSE);  
	}

	return;
}


/*****************************************************************************/
/* Name:     SetTplDisplay                                                      
/*                                                                           
/* Purpose:  Sets the Tpl display
/*                                          
/*                                                                           
/* Example Call:   void SetTplDisplay(int display, double reading);                                    
/*                                                                           
/* Input:    display - show or hide display
/*			 reading - reading to display
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
void SetTplDisplay(int display, double reading)
{
	char cBuf[10];

	if(display){
		sprintf(cBuf, "%3.1f", reading);
		SetCtrlAttribute(PerfPanel, PERF_PANEL_TPL,       ATTR_LABEL_TEXT, cBuf);   
		SetCtrlAttribute(PerfPanel, PERF_PANEL_TPL,       ATTR_VISIBLE, TRUE);
		SetCtrlAttribute(PerfPanel, PERF_PANEL_TPL_S,     ATTR_VISIBLE, TRUE); 
		SetCtrlAttribute(PerfPanel, PERF_PANEL_TPL_TITLE, ATTR_VISIBLE, TRUE);
	}
	else{ 
		SetCtrlAttribute(PerfPanel, PERF_PANEL_TPL,       ATTR_VISIBLE, FALSE);    
		SetCtrlAttribute(PerfPanel, PERF_PANEL_TPL_TITLE, ATTR_VISIBLE, FALSE); 
		SetCtrlAttribute(PerfPanel, PERF_PANEL_TPL_S,     ATTR_VISIBLE, FALSE);
	}

	return;
}


/*****************************************************************************/
/* Name:     SetWaveFormDisplay                                                      
/*                                                                           
/* Purpose:  Set the waveform to display					
/*                                          
/*                                                                           
/* Example Call:   void SetWaveFormDisplay(int display, int type);                                    
/*                                                                           
/* Input:    display - show or hide display
/*			 type - type of wave to display (SQUARE or TRIANGLE)
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
void SetWaveFormDisplay(int display, int type)
{
	char cBuf[10];

	if(display){
		if(type == SQUARE){
			sprintf(cBuf, "%s", LDBf(Ldb_SQUARE));
			SetCtrlAttribute(PerfPanel, PERF_PANEL_WAVEFORM, ATTR_LABEL_TEXT, cBuf);  
			SetCtrlAttribute(PerfPanel, PERF_PANEL_WAVEFORM, ATTR_VISIBLE, TRUE);
		}
		else{
			sprintf(cBuf, "%s", LDBf(Ldb_TRIANGLE));
			SetCtrlAttribute(PerfPanel, PERF_PANEL_WAVEFORM, ATTR_LABEL_TEXT, cBuf);
			SetCtrlAttribute(PerfPanel, PERF_PANEL_WAVEFORM, ATTR_VISIBLE, TRUE);
		}
	}
	else{
		SetCtrlAttribute(PerfPanel, PERF_PANEL_WAVEFORM, ATTR_VISIBLE, FALSE);
	}

  	return;
}  


/*****************************************************************************/
/* Name:    SetPsensDisplay                                                   
/*                                                                           
/* Purpose:  Sets the Psens display
/*                                          
/*                                                                           
/* Example Call:   void SetPsensDisplay(int display, double reading);                                    
/*                                                                           
/* Input:    Display - show or hide display
/*			 reading - psens reading to display
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
void SetPsensDisplay(int display, double reading)
{
	char cBuf[10];

	if(display){
		sprintf(cBuf, "%3.1lf", reading);
		SetCtrlAttribute(PerfPanel, PERF_PANEL_PSENS,       ATTR_LABEL_TEXT,cBuf);   
		SetCtrlAttribute(PerfPanel, PERF_PANEL_PSENS,       ATTR_VISIBLE, TRUE);
		SetCtrlAttribute(PerfPanel, PERF_PANEL_PSENS_TITLE, ATTR_VISIBLE, TRUE);
		SetCtrlAttribute(PerfPanel, PERF_PANEL_PSENS_CM,    ATTR_VISIBLE, TRUE);
		SetCtrlAttribute(PerfPanel, PERF_PANEL_PSENS_H20,   ATTR_VISIBLE, TRUE);
	}
	else{
		SetCtrlAttribute(PerfPanel, PERF_PANEL_PSENS,       ATTR_VISIBLE, FALSE);
		SetCtrlAttribute(PerfPanel, PERF_PANEL_PSENS_TITLE, ATTR_VISIBLE, FALSE);
		SetCtrlAttribute(PerfPanel, PERF_PANEL_PSENS_CM,    ATTR_VISIBLE, FALSE);
		SetCtrlAttribute(PerfPanel, PERF_PANEL_PSENS_H20,   ATTR_VISIBLE, FALSE);
	}

	return;
}


/*****************************************************************************/
/* Name:     SetTiDisplay                                                     
/*                                                                           
/* Purpose:  Sets the Ti Display
/*                                          
/*                                                                           
/* Example Call:   void SetTiDisplay(int display, double reading);                                    
/*                                                                           
/* Input:    display - show or hide display
/*			 reading - reading to display
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
void SetTiDisplay(int display, double reading)
{
	char cBuf[10];

	if(display){
		sprintf(cBuf, "%3.2lf", reading);  
		SetCtrlAttribute(PerfPanel, PERF_PANEL_TI,       ATTR_LABEL_TEXT, cBuf);
		SetCtrlAttribute(PerfPanel, PERF_PANEL_TI,       ATTR_VISIBLE, TRUE);
		SetCtrlAttribute(PerfPanel, PERF_PANEL_TI_TITLE, ATTR_VISIBLE, TRUE);
		SetCtrlAttribute(PerfPanel, PERF_PANEL_TI_S,     ATTR_VISIBLE, TRUE);
	}
	else{
		SetCtrlAttribute(PerfPanel, PERF_PANEL_TI,       ATTR_VISIBLE, FALSE);
		SetCtrlAttribute(PerfPanel, PERF_PANEL_TI_TITLE, ATTR_VISIBLE, FALSE);
		SetCtrlAttribute(PerfPanel, PERF_PANEL_TI_S,     ATTR_VISIBLE, FALSE);
	}

	return;
}


/*****************************************************************************/
/* Name:     SetPiDisplay                                                 
/*                                                                           
/* Purpose:  Sets the Pi display
/*                                          
/*                                                                           
/* Example Call:   void SetPiDisplay(int display, double reading);                                    
/*                                                                           
/* Input:    display - show or hide display
/*			 reading - mreading to display
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
void SetPiDisplay(int display, double reading)
{
	char cBuf[10];

	if(display){
		sprintf(cBuf, "%3d", (int)reading);  
		SetCtrlAttribute(PerfPanel, PERF_PANEL_PI,       ATTR_LABEL_TEXT, cBuf);
		SetCtrlAttribute(PerfPanel, PERF_PANEL_PI,       ATTR_VISIBLE, TRUE);
		SetCtrlAttribute(PerfPanel, PERF_PANEL_PI_TITLE, ATTR_VISIBLE, TRUE);
		SetCtrlAttribute(PerfPanel, PERF_PANEL_PI_CM,    ATTR_VISIBLE, TRUE);
		SetCtrlAttribute(PerfPanel, PERF_PANEL_PI_H20,   ATTR_VISIBLE, TRUE);
	}
	else{
		SetCtrlAttribute(PerfPanel, PERF_PANEL_PI,       ATTR_VISIBLE, FALSE);
		SetCtrlAttribute(PerfPanel, PERF_PANEL_PI_TITLE, ATTR_VISIBLE, FALSE);
		SetCtrlAttribute(PerfPanel, PERF_PANEL_PI_CM,    ATTR_VISIBLE, FALSE);
		SetCtrlAttribute(PerfPanel, PERF_PANEL_PI_H20,   ATTR_VISIBLE, FALSE);
	}

	return;
}


/*****************************************************************************/
/* Name:     SetPDisplay                                                  
/*                                                                           
/* Purpose: Sets the P Display
/*                                          
/*                                                                           
/* Example Call: void SetPDisplay(int display, int reading);                                    
/*                                                                           
/* Input:    display - show or hide display
/*			 reading - mreading to display
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
void SetPDisplay(int display, int reading)   
{
	char cBuf[10];

	if(display){
		sprintf(cBuf, "%d", reading); 
		SetCtrlAttribute(PerfPanel, PERF_PANEL_P,       ATTR_LABEL_TEXT, cBuf); 
		SetCtrlAttribute(PerfPanel, PERF_PANEL_P_TITLE, ATTR_VISIBLE, TRUE);
		SetCtrlAttribute(PerfPanel, PERF_PANEL_P,       ATTR_VISIBLE, TRUE);
		SetCtrlAttribute(PerfPanel, PERF_PANEL_P_PER,   ATTR_VISIBLE, TRUE);
	}
	else{
		SetCtrlAttribute(PerfPanel, PERF_PANEL_P_TITLE, ATTR_VISIBLE, FALSE);
		SetCtrlAttribute(PerfPanel, PERF_PANEL_P,       ATTR_VISIBLE, FALSE);
		SetCtrlAttribute(PerfPanel, PERF_PANEL_P_PER,   ATTR_VISIBLE, FALSE);
	}

	return;
}


/*****************************************************************************/
/* Name:     SetO2Display                                                      
/*                                                                           
/* Purpose:  Sets the O2 display
/*                                          
/*                                                                           
/* Example Call:   void SetO2Display(int display, int reading);                                    
/*                                                                           
/* Input:    display - show or hide display
/*			 reading - reading to display
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/  
void SetO2Display(int display, int reading)
{
	char cBuf[10];

	if(display){
		sprintf(cBuf, "%d", reading);
		SetCtrlAttribute(PerfPanel, PERF_PANEL_O2,       ATTR_LABEL_TEXT, cBuf);   
		SetCtrlAttribute(PerfPanel, PERF_PANEL_O2,       ATTR_VISIBLE, TRUE);
		SetCtrlAttribute(PerfPanel, PERF_PANEL_O2_TITLE, ATTR_VISIBLE, TRUE);
		SetCtrlAttribute(PerfPanel, PERF_PANEL_O2_PER,   ATTR_VISIBLE, TRUE);
	}
	else{
		SetCtrlAttribute(PerfPanel, PERF_PANEL_O2,       ATTR_VISIBLE, FALSE);
		SetCtrlAttribute(PerfPanel, PERF_PANEL_O2_TITLE, ATTR_VISIBLE, FALSE);
		SetCtrlAttribute(PerfPanel, PERF_PANEL_O2_PER,   ATTR_VISIBLE, FALSE);
	}

	return;
}


/*****************************************************************************/
/* Name:     SetPEEPDisplay                                                   
/*                                                                           
/* Purpose:  Sets the PEEP display.
/*                                          
/*                                                                           
/* Example Call:   void SetPEEPDisplay(int display, double reading);                                    
/*                                                                           
/* Input:    display - show or hide display
/*			 reading - reading to display
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
void SetPEEPDisplay(int display, double reading)
{
	char cBuf[10];

	if(display){
		if(reading >= 20){
			sprintf(cBuf, "%3d", (int)reading);
		}
		else{
			sprintf(cBuf, "%3.1lf", reading);
		}
		SetCtrlAttribute(PerfPanel, PERF_PANEL_PEEP,       ATTR_LABEL_TEXT, cBuf);   
		SetCtrlAttribute(PerfPanel, PERF_PANEL_PEEP,       ATTR_VISIBLE, TRUE);
		SetCtrlAttribute(PerfPanel, PERF_PANEL_PEEP_TITLE, ATTR_VISIBLE, TRUE);
		SetCtrlAttribute(PerfPanel, PERF_PANEL_PEEP_CM,    ATTR_VISIBLE, TRUE);
		SetCtrlAttribute(PerfPanel, PERF_PANEL_PEEP_H20,   ATTR_VISIBLE, TRUE);  
	}
	else{
		SetCtrlAttribute(PerfPanel, PERF_PANEL_PEEP,       ATTR_VISIBLE, FALSE);
		SetCtrlAttribute(PerfPanel, PERF_PANEL_PEEP_TITLE, ATTR_VISIBLE, FALSE);
		SetCtrlAttribute(PerfPanel, PERF_PANEL_PEEP_CM,    ATTR_VISIBLE, FALSE);
		SetCtrlAttribute(PerfPanel, PERF_PANEL_PEEP_H20,   ATTR_VISIBLE, FALSE);  
	}

	return;
}


/*****************************************************************************/
/* Name:    Flasher                                                   
/*                                                                           
/* Purpose:  Callback for the timer which controls the flashing of changed values on the perf panel
/*                                          
/*                                                                           
/* Example Call:  int  CVICALLBACK Flasher(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);                                    
/*                                                                           
/* Input:    standard callback arguments        
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   always 0                                               
/*                                                                           
/*****************************************************************************/
int CVICALLBACK Flasher(int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int x;

	if( (panel == PerfPanel) && (control == PERF_PANEL_PANEL_TIMER) && (FlashId != 0) ){
		for(x = 0; x < MAX_FLASH; x++){
			if(FlashBin[x]){
				if(FlashIndex[x]){
					SetCtrlAttribute(PerfPanel, FlashBin[x], ATTR_LABEL_COLOR, VAL_WHITE);
					FlashIndex[x] = 0;
				}
				else{
					SetCtrlAttribute(PerfPanel, FlashBin[x], ATTR_LABEL_COLOR, VAL_BLACK);
					FlashIndex[x] = 1;
				}
			}
		} 
	}

	return 0;
}


/*****************************************************************************/
/* Name:      O2TimerCB                                                  
/*                                                                           
/* Purpose:  Cllback for the timer which controls the display of the O2 reading on the perf panel
/*                                          
/*                                                                           
/* Example Call:   int CVICALLBACK O2TimerCB(int panel, int control, int event,
/*		void *callbackData, int eventData1, int eventData2);                                    
/*                                                                           
/* Input:    standard callback arguments      
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   always 0                                                         
/*                                                                           
/*****************************************************************************/
int CVICALLBACK O2TimerCB(int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	double O2Percent;
	char buffer[50];

	switch(event){
		case EVENT_TIMER_TICK:
			#ifndef DEMOMODE
			MeasureOxygen(&O2Percent);
			sprintf(buffer, "%3.2f", O2Percent);
			SetCtrlVal(panel, PERF_PANEL_O2READING, buffer);
			#endif
			break;
	}

	return 0;
}

