#include "stdafx.h"
//====================================================================
// This is a proprietary work to which Nellcor Puritan Bennett
// corporation of California claims exclusive right.  No part of this
// work may be used, disclosed, reproduced, sorted in an information
// retrieval system, or transmitted by any means, electronic,
// mechanical, photocopying, recording, or otherwise without the prior
// written permission of Nellcor Puritan Bennett Corporation.
//
//     Copyright (c) 1995, Nellcor Puritan Bennett Corporation
//=====================================================================


//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: AuxResult - POST Auxiliary Result Data
//---------------------------------------------------------------------
//@ Interface-Description
//  This class contains the data for each auxiliary result in the 
//  POST AuxResultLog.  Each object contains information pertaining to
//  the state of the processor when a test failure or unexpected 
//  interrupt occurs during POST.
//---------------------------------------------------------------------
//@ Rationale
//  This class contains POST auxiliary result data.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/POST-Library/vcssrc/AuxResult.ccv   25.0.4.0   19 Nov 2013 14:16:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002   By: Gary Cederquist  Date: 15-AUG-1997  DR Number: 1875
//    Project:   Sigma   (R8027)
//    Description:
//       Added testable item annotations.
//
//  Revision: 001   By: Gary Cederquist  Date: 12-MAY-1997  DR Number: 2085
//    Project:   Sigma   (R8027)
//    Description:
//       Initial version
//
//=====================================================================

//=====================================================================
//
//      Includes...
//
//=====================================================================

#include "AuxResult.hh"

//=====================================================================
//
//      Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: AuxResult [constructor]
//
//@ Interface-Description
//  Default constructor.  
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
AuxResult::AuxResult() : testNumber_     (0)
                        ,vectorNumber_   (0)
                        ,nmiSource_      (0)
                        ,auxCode_        (0)
                        ,programCounter_ (0)
{
    // $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: AuxResult [constructor]
//
//@ Interface-Description
//  Constructor.  
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
AuxResult::AuxResult(  const Uint8 testNumber
                     , const Uint8 vectorNumber 
                     , const Uint8 nmiSource
                     , const Uint8 auxCode 
                     , Byte* const pc )
    : testNumber_     (testNumber)
    , vectorNumber_   (vectorNumber)
    , nmiSource_      (nmiSource)
    , auxCode_        (auxCode) 
    , programCounter_ (pc) 
{
    // $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: AuxResult [copy constructor]
//
//@ Interface-Description
//  Copy constructor.  
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
AuxResult::AuxResult( const AuxResult & rAuxResult )
{
    operator=(rAuxResult);
    // $[TI3]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: operator=
//
//@ Interface-Description
//  Assignment operator.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
AuxResult::operator=( const AuxResult & rAuxResult )
{
    testNumber_     = (rAuxResult.testNumber_);
    vectorNumber_   = (rAuxResult.vectorNumber_);
    nmiSource_      = (rAuxResult.nmiSource_);
    auxCode_        = (rAuxResult.auxCode_);
    programCounter_ = (rAuxResult.programCounter_);
    // $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~AuxResult [destructor]
//
//@ Interface-Description
//  Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
AuxResult::~AuxResult()
{
}
