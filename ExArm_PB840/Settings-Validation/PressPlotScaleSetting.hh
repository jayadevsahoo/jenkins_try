
#ifndef PressPlotScaleSetting_HH
#define PressPlotScaleSetting_HH

//====================================================================
//
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
//@ Class:  PressPlotScaleSetting - Y Axis Scale for the Pressure Waveform.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/PressPlotScaleSetting.hhv   25.0.4.0   19 Nov 2013 14:27:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'getApplicability()' method
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Initial version.
//  
//====================================================================

//@ Usage-Classes
#include "NonBatchDiscreteSetting.hh"
//@ End-Usage


class PressPlotScaleSetting : public NonBatchDiscreteSetting
{
  public:
    PressPlotScaleSetting(void);
    virtual ~PressPlotScaleSetting(void);

    virtual Applicability::Id  getApplicability(
			    const Notification::ChangeQualifier qualifierId
					       ) const;

    virtual SettingValue  getDefaultValue(void) const;

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32 lineNumber,
			   const char* pFileName  = NULL, 
			   const char* pPredicate = NULL);

  private:
    PressPlotScaleSetting(const PressPlotScaleSetting&);// not implemented...
    void  operator=(const PressPlotScaleSetting&);    // not implemented...
};


#endif // PressPlotScaleSetting_HH 
