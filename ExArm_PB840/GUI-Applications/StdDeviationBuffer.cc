#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================


// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: StdDeviationBuffer - class to calculate standard deriviation
//---------------------------------------------------------------------
//@ Interface-Description
// This class is responsible for calculating the standard deriviation.
//---------------------------------------------------------------------
//@ Rationale
// This class encapsulates the calculation of the standard deriviation.
//---------------------------------------------------------------------
//@ Implementation-Description
// The class implements methods for updating the data buffer, and calculating
// the standard deriviation.
// updateBuffer() puts the data in the buffers.  getStdDeviation() returns
// the standard deriviation.  getUpper() returns the high end of norm value (
// mean + 1 standard deriviation). getLower() returns the low end of norm
// value (mean - 1 standard deriviation).
//---------------------------------------------------------------------
//@ Fault-Handling
// n/a
//---------------------------------------------------------------------
//@ Restrictions
// n/a
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/StdDeviationBuffer.ccv   25.0.4.0   19 Nov 2013 14:08:32   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By:  sph	   Date:  08-Feb-2000    DCS Number:  5504
//  Project:  NeoMode
//  Description:
//      Added recent-activity range to alarm bars whereby the standard deviation 
//      the last N consecutive breaths is displayed.
//
//  Revision: 001  By:  yyy	   Date:  29-Jun-1998    DCS Number: 5327
//  Project:  NeoMode
//  Description:
//		Initial version.
//
//=====================================================================

#include "StdDeviationBuffer.hh"

//@ Usage-Classes
#include <math.h>

//@ End-Usage

#ifdef SIGMA_DEVELOPMENT
#include "Ostream.hh"
#endif

#ifdef FORNOW
#include "Task.hh"
#include "PrintQueue.hh"
#include "Ostream.hh"
#endif

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: StdDeviationBuffer()  [Default Contstructor]
//
//@ Interface-Description
//        Constructor.
// The constructor takes a pointer to a pre-allocated data buffers, and
// the size of this buffer.  The SummationBuffer and varianceBuffer_ are
// constructed by these arguments. 
//---------------------------------------------------------------------
//@ Implementation-Description
//        none
//---------------------------------------------------------------------
//@ PreCondition
//        none
//---------------------------------------------------------------------
//@ PostCondition
//        none
//@ End-Method
//=====================================================================
StdDeviationBuffer::StdDeviationBuffer(Real32 *pSummationBuffer, Real32 *pDeviationBuffer, Uint32 bufferSize) :
	SummationBuffer(pSummationBuffer, bufferSize),
	varianceBuffer_(pDeviationBuffer, bufferSize)
{
    CALL_TRACE("StdDeviationBuffer::StdDeviationBuffer(void)");

    // $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~StdDeviationBuffer()  [Destructor]
//
//@ Interface-Description
//        Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//        none
//---------------------------------------------------------------------
//@ PostCondition
//        none
//@ End-Method
//=====================================================================
StdDeviationBuffer::~StdDeviationBuffer(void)
{
	CALL_TRACE("StdDeviationBuffer::~StdDeviationBuffer(void)");
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateBuffer()
//
//@ Interface-Description
//		This method has the data to be put in the buffers as an argument and
//		returns nothing. 
//---------------------------------------------------------------------
//@ Implementation-Description
// The data is added to the cumulative sum in the SummationBuffer.  It
// also added the power of the data into the varianceBuffer_.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
StdDeviationBuffer::updateBuffer( const Real32 data)
{
	CALL_TRACE("SummationBuffer::updateBuffer( const Real32 data)") ;

	// Update the data by using the parent class's updateBuffer()
	SummationBuffer::updateBuffer(data);
	varianceBuffer_.updateBuffer(pow(data, 2));
    // $[TI1]
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getStdDeviation
//
//@ Interface-Description
//		This method has no arguments and returns the standard deriviation.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Return the computed the standard deriviation.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

Real32
StdDeviationBuffer::getStdDeviation( void) const
{
	CALL_TRACE("SummationBuffer::getStdDeviation( void)") ;
	
	Real32 stdDeviation;

	Real32 summation = getSum();
	Real32 squareSum = varianceBuffer_.getSum();

	Int32 sampleCount = varianceBuffer_.getNumSamples();
	
	if (sampleCount < 2)
	{											// $[TI1.1]
		stdDeviation = 0.0;
	}
	else
	{											// $[TI1.2]
		stdDeviation = (sampleCount * squareSum - pow(summation , 2)) / (sampleCount * (sampleCount - 1));
		if (stdDeviation < 0)
		{										// $[TI1.2.1]
			stdDeviation = 0.0;
		}
		else
		{										// $[TI1.2.2]
			stdDeviation = sqrt(stdDeviation);
		}
	}
	
	return( stdDeviation) ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getUpper
//
//@ Interface-Description
//		This method has no arguments and returns the upper bound of the
//		standard deriviation value.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Return the upper bound of the standard deriviation value.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

Real32
StdDeviationBuffer::getUpper( void) const
{
	CALL_TRACE("SummationBuffer::getUpper( void)") ;
	
	Real32 upper;

	upper = getStdDeviation() + getAverage();
	
	return( upper) ;
	// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getLower
//
//@ Interface-Description
//		This method has no arguments and returns the lower bound of the
//		standard deriviation value.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Return the lower bound of the standard deriviation value.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

Real32
StdDeviationBuffer::getLower( void) const
{
	CALL_TRACE("SummationBuffer::getLower( void)") ;
	
	Real32 lower;

	lower = getAverage() - getStdDeviation();
	
	return( lower) ;
	// $[TI1]
}



#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
 
void
StdDeviationBuffer::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
                   const char*        pFileName,
                   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, STDDEVIATIONBUFFER,
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)
