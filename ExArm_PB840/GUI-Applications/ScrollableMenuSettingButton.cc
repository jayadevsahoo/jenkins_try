#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: ScrollableMenuSettingButton - A specialized
//	DiscreteSettingButton that displays a pop-up ScrollableMenu
//---------------------------------------------------------------------
//@ Interface-Description
//	This class is a specialization of the DiscreteSettingButton, whereby
//	instances of this class display a pop-up menu, with each of the
//	allowable discrete setting values, when pressed.
//---------------------------------------------------------------------
//@ Rationale
//  Contains all the functionality needed to allow the operator to view
//  and adjust a discrete setting controlling a pop-up menu in single
//  class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Most of the interaction with the Settings-Validation subsystem is
//  handled in the parent SettingButton class. The referenced
//  ScrollableMenu class handles formatting and display of the pop-up
//  menu window. The parent DiscreteSettingButton class handles the
//  appearance of the setting button itself. This class pops up the
//  ScrollableMenu when the button is pressed down and pops it down when
//  the button is pressed up. The updateDisplay_ method receives the
//  setting changes associated with this button and passes these changes
//  along to its ScrollableMenu to update the pop-up menu and along to
//  the DiscreteSettingButton base class to update the button's value
//  display.
//---------------------------------------------------------------------
//@ Fault-Handling
//  No special fault handling apart from the usual assertions and
//	pre-conditions to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions 
//	none 
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/ScrollableMenuSettingButton.ccv   25.0.4.0   19 Nov 2013 14:08:22   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By: gdc     Date:  06-Dec-2006   SCR Number: 6237
//  Project:  TREND
//  Description:
//		Initial version.
//=====================================================================

#include "ScrollableMenuSettingButton.hh"

//@ Usage-Classes
#include "SettingSubject.hh"
#include "ScrollableMenu.hh"
#include "SettingMenuItems.hh"
//@ End-Usage

//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ScrollableMenuSettingButton()  [Constructor]
//
//@ Interface-Description
//	Constructs an instance of this class.
//---------------------------------------------------------------------
//@ Implementation-Description 
//  Initializes private data. Passes along the required parameters to
//  the DiscreteSettingButton base class.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

ScrollableMenuSettingButton::ScrollableMenuSettingButton(
														Uint16 xOrg, 
														Uint16 yOrg,
														Uint16 width, 
														Uint16 height, 
														Button::ButtonType buttonType,
														Uint8 bevelSize, 
														Button::CornerType cornerType,
														Button::BorderType borderType,
														StringId titleText, 
														SettingId::SettingIdType settingId,
														ScrollableMenu& rScrollableMenu,
														SettingMenuItems& rSettingMenuItems,
														Uint16 menuX,
														Uint16 menuY,
														Boolean isMainSetting) 
:   DiscreteSettingButton(  xOrg, yOrg, width, height, buttonType,
							bevelSize, cornerType, borderType, titleText,
							settingId, NULL, NULL, 12, isMainSetting),
	rScrollableMenu_(       rScrollableMenu),
	rSettingMenuItems_(     rSettingMenuItems),
	SCROLLABLE_MENU_X(      menuX),
	SCROLLABLE_MENU_Y(      menuY)
{
	CALL_TRACE("ScrollableMenuSettingButton()");

	// DiscreteSettingButton needs the ValueInfo data to display the "value" text
	// We get this data from the specified SettingMenuItems object
	setValueInfo(&rSettingMenuItems_.getValueInfo());
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~ScrollableMenuSettingButton  [Destructor]
//
//@ Interface-Description
//	Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//	none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

ScrollableMenuSettingButton::~ScrollableMenuSettingButton(void)
{
	CALL_TRACE("ScrollableMenuSettingButton::~ScrollableMenuSettingButton(void)");

	// Do nothing
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateDisplay_ [Protected, virtual]
//
//@ Interface-Description
//  Called when the display of this setting button needs to be updated
//  with the latest setting value.  Passed a setting context id
//	informing us the setting context from which we should retrieve the
//  new setting value. Since the ScrollableMenuSettingButton always
//  displays the current adjusted value, the context qualifier is
//  ignored.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Get the adjusted value of the setting and passes it on to
//  ScrollableMenu to update the pop-up menu and on to the
//  DiscreteSettingButton base class to update the button's value text.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void ScrollableMenuSettingButton::updateDisplay_(Notification::ChangeQualifier qualifierId,
												 const SettingSubject*         pSubject)
{
	CALL_TRACE("updateDisplay_(qualifierId, pSubject)");
	const SettingId::SettingIdType  SETTING_ID = pSubject->getId();

	DiscreteValue currentValue = pSubject->getAdjustedValue();

	rScrollableMenu_.setSelectedEntry(currentValue);

	// forward on to base class...
	DiscreteSettingButton::updateDisplay_(qualifierId, pSubject);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: downHappened_ [virtual]
//
//@ Interface-Description
//	This is a virtual method inherited from being a Button. It is called
//  when this button is sent into the Down state, either by the operator
//  pressing us down (byOperatorAction = TRUE) or by the application
//  calling the method setToDown() (byOperatorAction = FALSE).
//---------------------------------------------------------------------
//@ Implementation-Description 
//  When this setting button gets pressed down, we pop-up the associated
//  ScrollableMenu by adding its Drawable to our parent container and
//  calling ScrollableMenu::setEntryInfo to initialize the object with
//  our SettingMenuItems.
//---------------------------------------------------------------------
//@ PreCondition 
//	none 
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void ScrollableMenuSettingButton::downHappened_(Boolean byOperatorAction)
{
	CALL_TRACE("SettingButton::downHappened_(Boolean byOperatorAction)");
	SettingSubject* pSetting = getSubjectPtr_(getSettingId());
	DiscreteValue currentValue = pSetting->getAdjustedValue();

	SettingButton::downHappened_(byOperatorAction);

	// add drop-down menu to subscreen...
	rScrollableMenu_.setX(SCROLLABLE_MENU_X);
	rScrollableMenu_.setY(SCROLLABLE_MENU_Y);

	getParentContainer()->addDrawable(&rScrollableMenu_);

	// show the menu with the selected entry highlighted
	rScrollableMenu_.setEntryInfo(currentValue, rSettingMenuItems_.getValueInfo().numValues);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: upHappened_ [virtual]
//
//@ Interface-Description
//  This is a virtual method inherited from being a Button.  It is
//	called when this button is sent into the Up state, either by the
//	operator pressing us down (byOperatorAction = TRUE) or by the
//	application calling the method setToUp() (byOperatorAction = FALSE).
//---------------------------------------------------------------------
//@ Implementation-Description 
//  We "pop down" the associated ScrollablMenu by deactivating it and
//  removing its Drawable our parent container.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void ScrollableMenuSettingButton::upHappened_(Boolean byOperatorAction)
{
	CALL_TRACE("SettingButton::upHappened_(Boolean byOperatorAction)");

	rScrollableMenu_.deactivate();
	getParentContainer()->removeDrawable(&rScrollableMenu_);
	SettingButton::upHappened_(byOperatorAction);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void ScrollableMenuSettingButton::SoftFault(const SoftFaultID  softFaultID,
											const Uint32       lineNumber,
											const char*        pFileName,
											const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, SCROLLABLEMENUSETTINGBUTTON,
							lineNumber, pFileName, pPredicate);
}

