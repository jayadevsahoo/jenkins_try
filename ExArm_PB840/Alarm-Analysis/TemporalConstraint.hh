#ifndef TemporalConstraint_HH
#define TemporalConstraint_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: TemporalConstraint - 
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Alarm-Analysis/vcssrc/TemporalConstraint.hhv   25.0.4.0   19 Nov 2013 13:51:26   pvcs  $
//
//@ Modification-Log

//  
//   Revision 001   By:   hct      Date: 01/01/95         DR Number:   n/a
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//  
//   Revision 002   By:   sah      Date: 01/12/99         DR Number:   5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to non-inlined method.
//  
//   Revision 003   By:   hct      Date: 04/22/99         DR Number:   5359
//      Project:   ATC
//      Description:
//         Added functionality to store values of pMinCalcFunction_ and
//         pMaxCalcFunction_ when associated OperatingGroup becomes active.
//  
//====================================================================

#include "Sigma.hh"
#include "Alarm_Analysis.hh"

//@ Usage-Classes
#include "Constraint.hh"

//@ End-Usage


class TemporalConstraint : public Constraint 
{
  public:

    TemporalConstraint(Constraint::BooleanOperator minBooleanOp, 
                     Constraint::BooleanOperator maxBooleanOp,
                     Constraint::CalcFunctionPtrType pMinCalcFunction = NULL,
                     Constraint::CalcFunctionPtrType pMaxCalcFunction = NULL);
    virtual ~TemporalConstraint(void);

    virtual Boolean evaluate         (OperatingGroup& rChildGroup) const;
    virtual Boolean evaluateAutoReset(OperatingGroup& rChildGroup) const;
    virtual Int32  getMinNum(void) const;

    static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);
  
  protected:

  private:
    //@ Data-Member:    minValue_
    // Storage to hold value of pMinCalcFunction_ at time
    // associated OperatingGroup becomes active.
    Int32 minValue_;

    //@ Data-Member:    maxValue_
    // Storage to hold value of pMaxCalcFunction_ at time
    // associated OperatingGroup becomes active.
    Int32 maxValue_;


};


#endif // TemporalConstraint_HH 
