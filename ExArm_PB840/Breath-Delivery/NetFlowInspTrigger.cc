#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: NetFlowInspTrigger - Detects patient effort during a flow 
//	   triggered exhalation.
//---------------------------------------------------------------------
//@ Interface-Description
//    This class triggers inspiration based on the expiratory flow 
//    measurement and the operator set value for flow sensitivity, 
//    as well as other qualifying factors. This trigger is used as the method of
//    detecting patient inspiratory effort when flow triggering has
//    been selected.  It is enabled or disabled, depending upon the
//    applicability of the trigger.  When the flow condition calculated 
//    by this trigger is true, the trigger is considered to have fired.  
//    This trigger is kept on a list contained in the BreathTriggerMediator.
//---------------------------------------------------------------------
//@ Rationale
//    This class performs the calculations necessary to determine if the patient is 
//    making an inspiratory effort when flow triggering is active.
//---------------------------------------------------------------------
//@ Implementation-Description
//    This class is derived from PatientTrigger. The flow triggering
//    conditions are implemented in the method triggerCondition.
//---------------------------------------------------------------------
//@ Fault-Handling
//    n/a.
//---------------------------------------------------------------------
//@ Restrictions
//    Only one instance of this class may be created.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/NetFlowInspTrigger.ccv   25.0.4.0   19 Nov 2013 13:59:54   pvcs  $
//
//@ Modification-Log
//
//  Revision: 019   By: rhj   Date:  05-May-2010    SCR Number: 6436 
//  Project:  PROX
//  Description:
//		Optimize leakcomp algorithms by minimizing the use of 
//      math api calls.
//
//  Revision: 018   By: rhj   Date:  12-Mar-2009    SCR Number: 6492 
//  Project:  840S
//  Description:
//		Added an extra condition to reduce autocycling when 
//      exhalation leak rate is underestimated.
//
//  Revision: 017   By: rhj   Date:  19-Jan-2009    SCR Number: 6455 
//  Project:  840S
//  Description:
//		Enhanced Leak Compensation algorithms to handle no leaks.
// 
//  Revision: 016   By:   rhj    Date: 07-July-2008   SCR Number: 6435
//  Project:  LEAK
//  Description:
//       Leak Compensation project-related changes.
//       
//  Revision: 015  By: srp     Date:  29-May-2002    DR Number: 5827
//  Project:  VCP
//  Description:
//		Traced NP24019 - NP240xx from the control requirement.
//
//  Revision: 014  By: syw     Date:  12-Dec-2000    DR Number: 5821
//  Project:  Metabolics
//  Description:
//		Obtain peak exh flow and time of occurance to be used to determine the
//			estimated time that the exh flow will reach base flow.
//		Hold off exhDryFlow < baseFlow + 1 condition by exhDryFlow >=
//			baseFlow + 1 at least once.
//		Set transientTime_ = 0.0 if the exh flow dips belows base flow and
//			recovers above base flow or the elapsed time is greater than
//			the estimated time that the exh flow will reach base flow.
//
//  Revision: 013  By: syw     Date:  31-Oct-2000    DR Number: 5793
//  Project:  Metabolics
//  Description:
//		Changes due to new puffless controller implementaion.
//
//  Revision: 012  By: syw     Date:  26-Jan-2000    DR Number: 5327
//  Project:  NeoMode
//  Description:
//		Use count of 2 if neomode and if Bilevel low to high transition breath
//		to trigger based on flow slope, otherwise count is 1.
//      Updated for NeoMode
//
//  Revision: 011  By: syw     Date:  02-Nov-1999    DR Number: 5573
//  Project:  ATC
//  Description:
//		Use exhFlow for trigger condition and added exhAutozeroOccurred_
//		and lungFlowConditionMet_ to condition.
//
//  Revision: 010  By: syw     Date:  14-Jun-1999    DR Number: 5322
//  Project:  ATC
//  Description:
//		Added enable method and additional condition in trigggerCondition_().
//
//  Revision: 009  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 008  By: iv    Date:  18-Jul-1997    DR Number: 2302
//  	Project:  Sigma (840)
//		Description:
//			Removed Pi slope condition.
//
//  Revision: 007  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 006  By:  sp    Date: 1-Apr-1997    DR Number: DCS NONE
//       Project:  Sigma (R8027)
//       Description:
//             Inspection rework.
//
//  Revision: 005  By:  syw    Date: 13-Mar-1997    DR Number: DCS 1780
//       Project:  Sigma (R8027)
//       Description:
//             Changed rExhDryFlowSensor.getValue() to rExhFlowSensor.getDryValue().
//
//  Revision: 004  By:  sp    Date:  31-Oct-1996    DR Number:
//       Project:  Sigma (R8027)
//       Description:
//             Inspection rework.
//
//  Revision: 003  By:  sp    Date:  26-Jun-1996    DR Number:600
//       Project:  Sigma (R8027)
//       Description:
//             Implement new patient trigger's algorithm.
//
//  Revision: 002  By:  sp    Date:  4-Mar-1996    DR Number:674
//       Project:  Sigma (R8027)
//       Description:
//             Remove #ifndef.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "NetFlowInspTrigger.hh"

//@ Usage-Classes
#include "MainSensorRefs.hh"

#include "ExhFlowSensor.hh"
#include "PressureSensor.hh"
#include "BreathRecord.hh"
#include "PatientCctTypeValue.hh"
#include "PhasedInContextHandle.hh"
#include "Peep.hh"
#include "PeepController.hh"
#include "ControllersRefs.hh"
#include "BreathMiscRefs.hh"
#include "LeakCompEnabledValue.hh"
#include "SettingId.hh"
#include "SettingConstants.hh"
#include "LungData.hh"
#include "LeakCompMgr.hh"
#include "Breath_Delivery.hh"
#include <math.h>

#include "CalInfoRefs.hh"
#include "FlowSensorOffset.hh"


//@ End-Usage

//@ Code...

//====================================================================
//
//  Static Data...
//
//====================================================================


//@ Constant: PURGE_FLOW
// flow during exhalation with pressure triggering
extern const Real32 PURGE_FLOW ;


//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: NetFlowInspTrigger()  
//
//@ Interface-Description
//	Default Constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//      Initialize triggerId_ using PatientTrigger's constructor.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

NetFlowInspTrigger::NetFlowInspTrigger(const Trigger::TriggerId triggerId):
PatientTrigger(triggerId),
cycleState_( NetFlowInspTrigger::STEADY)
{
  CALL_TRACE("NetFlowInspTrigger()");
  // $[TI1]

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~NetFlowInspTrigger()
//
//@ Interface-Description  
//	Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

NetFlowInspTrigger::~NetFlowInspTrigger(void)
{
  CALL_TRACE("~NetFlowInspTrigger()");

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: enable()
//
//@ Interface-Description
//	This method has no arguments and returns nothing.  This method is called to
//	enable the trigger.
//---------------------------------------------------------------------
//@ Implementation-Description
//	The base class enable() method is called and data members are initialized.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
NetFlowInspTrigger::enable(void)
{
	CALL_TRACE("NetFlowInspTrigger::enable(void)") ;
	 
   	// $[TI1]
	PatientTrigger::enable() ;
	flowSlopeNegCount_ = 0;

	pressSlopeNegCount_ = 0.0 ;
	trigFlowCount_ = 0 ;
	transientTime_ = 0.0 ;

	flowCondition1Met_ = FALSE ;
	flowCondition2Met_ = FALSE ;
	exhFlowBelowBaseFlow_ = FALSE ;
	timeToBaseFlowObtained_ = FALSE ;
	peakFlow_ = 0.0 ;
	peakFlowTime_ = 0.0 ;
	timeToBaseFlow_ = 0.0 ;
	elapsedTime_ = 0 ;
	dOffset_ = 0.0;
	
}

#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
NetFlowInspTrigger::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, NETFLOWINSPTRIGGER,
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)



//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: triggerCondition_
//
//@ Interface-Description
//    This method takes no parameters.  If the trigger condition has occured,
//    the method returns true, otherwise, the method returns false.
//---------------------------------------------------------------------
//@ Implementation-Description
//    All of the following conditions must be met:
//        1.    isTriggerActive_ is TRUE
//        2.    If Leak compensation is enabled,                
//              ( filteredCompLungFlow > flowSens ) || (exhDryFlow <= ESTIMATED_FLOW_LIMIT)
//              else 
//              estimated flow <= ESTIMATED_FLOW_LIMIT 
//        3.    The sign of the dry exhalation flow slope signal is 
//              less than or equal to 0 for the specified counts.
//        4.    The sign of the expiratory pressure slope 
//              signals are less than or equal to 0 for the specified
//				count value.
//    If the above conditions are met, then this method returns true.
// $[04007]
// $[NP24019], $[NP24020], $[NP24021], $[NP24022], $[NP24024],
// $[NP24025], $[NP24026], $[NP24027], $[NP24028]
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Boolean
NetFlowInspTrigger::triggerCondition_(void)
{
  CALL_TRACE("triggerCondition_()");

    const Real32 ESTIMATED_FLOW_LIMIT = 1.5F; //LPM
    const Real32 BACKUP_FLOW_SENS = 2.0F; //LPM

	Boolean rtnValue = FALSE;
  	Real32 exhDryFlow = RExhFlowSensor.getDryValue();
	Real32 airFlow = RAirFlowSensor.getValue();
	Real32 o2Flow = RO2FlowSensor.getValue();
	Real32 inspFlow = airFlow + o2Flow ;
	Real32 exhRawFlow = RExhFlowSensor.getUnoffsettedValue();
	
	Real32 airOffset = RAirSideFlowSensorOffset.getOffset((Uint32)airFlow);
	Real32 o2Offset = RO2SideFlowSensorOffset.getOffset((Uint32)o2Flow);
	Real32 exhDryFlowCorr = inspFlow + airOffset + o2Offset;

	if (RExhFlowSensor.getDrySlope() <= 0.0F && RExhFlowSensor.getDrySlope() >= -0.05f)
		dOffset_ = exhRawFlow - exhDryFlowCorr;
	else 
		dOffset_ = dOffset_;

	if (dOffset_ >= 1.5f)
		dOffset_ = 1.5f;
	if (dOffset_ <= - 1.5f)
		dOffset_ = -1.5f;
	
  	prevDryExhFlow_ = exhDryFlow;

	Real32 flowSens = 0.0f;
    Real32 baseFlow = 0.0f;

	if (triggerId_ == Trigger::NET_FLOW_BACKUP_INSP)
	{
		flowSens = BACKUP_FLOW_SENS + (RLeakCompMgr.getExhLeakRate()/ 15.0f);
		baseFlow = PURGE_FLOW;
	}
	else
	{	
		flowSens = PhasedInContextHandle::GetBoundedValue(SettingId::FLOW_SENS).value;
		baseFlow = flowSens + ESTIMATED_FLOW_LIMIT;
	}

	// store peak flow occurance and corresponding time stamp
	if (exhDryFlow >= peakFlow_)
	{
		// $[TI27]
		peakFlow_ = exhDryFlow ;
		peakFlowTime_ = (Real32)elapsedTime_ ;
	}
	// $[TI28]

	if (RExhFlowSensor.getDrySlope() <= 0.0F)
	{
		// $[TI1]
		flowSlopeNegCount_++ ;
	}
	else
	{
		// $[TI2]
		flowSlopeNegCount_ = 0 ;
	}

	Uint32 countTarget = 1 ;

	const DiscreteValue  PATIENT_CCT_TYPE =
		PhasedInContextHandle::GetDiscreteValue(SettingId::PATIENT_CCT_TYPE) ;

	if (PATIENT_CCT_TYPE == PatientCctTypeValue::NEONATAL_CIRCUIT &&
			BreathRecord::GetIsMandBreath())
	{
		// $[TI3]
		countTarget = 2 ;
	}
	// $[TI4]

    // $[NP24028]
	if (exhDryFlow >= baseFlow)  
	{
		// $[TI29]
		flowCondition1Met_ = TRUE ;
	}
	// $[TI30]

    // $[NP24021]
    // $[TI30]
	if (exhDryFlow >= baseFlow + 1.0)
	{
		// $[TI31]
		flowCondition2Met_ = TRUE ;
	}
	// $[TI32]
    // $[NP24027]
	if (exhDryFlow < baseFlow + 1.0 && flowCondition2Met_ && !timeToBaseFlowObtained_)
	{
		// $[TI33]
		timeToBaseFlowObtained_ = TRUE ;
		
		Real32 slope = (peakFlow_ - exhDryFlow) / (peakFlowTime_ - elapsedTime_) ;
		Real32 intercept = peakFlow_ - slope * peakFlowTime_ ;
		timeToBaseFlow_ = (baseFlow - intercept) / slope ;
	}
	// $[TI34]

	if (exhDryFlow < baseFlow + 1.0
			&& trigFlowCount_ < MAX_FLOW_PTS
			&& flowCondition2Met_)
	{
		// $[TI5]
		triggerFlow_[trigFlowCount_] = exhDryFlow ;
		trigFlowCount_++ ;
	}
	else if (trigFlowCount_ >= MAX_FLOW_PTS)
	{
		// $[TI26]
		trigFlowCount_++ ;
	}
	// $[TI6]
	
	if (trigFlowCount_ == MAX_FLOW_PTS)
	{
        // $[NP24020]
		// $[TI7]
		Real32 avgSlope = -(11.0F * triggerFlow_[3] + 3.0F * triggerFlow_[2]
								- 3.0F * triggerFlow_[1] - 11.0F * triggerFlow_[0])
								 / 36.0F ;
		
		if (avgSlope < .01)
		{
			// $[TI8]
			avgSlope = 0.01 ;
		}
		// $[TI9]
		//$[NP24022]
		Real32 avgAmplitude = (triggerFlow_[3] + triggerFlow_[2] + triggerFlow_[1] + triggerFlow_[0]) / MAX_FLOW_PTS ;
		
        // $[NP24019]
		transientTime_ = 3.1416F * avgAmplitude / avgSlope * 1.25F ;
		
		if (transientTime_ > 100.0)
		{
			// $[TI10]
			transientTime_ = 100.0 ;
		}
		else if (transientTime_ < 4.0)
		{
			// $[TI11]
			transientTime_ = 4.0 ;
		}
		// $[TI12]
	}
	// $[TI13]

    // $[NP24028]
	if (flowCondition1Met_ && exhDryFlow < baseFlow)
	{
		// $[TI35]
		exhFlowBelowBaseFlow_ = TRUE ;
	}
	// $[TI36]                      

	static Real32 const TIME_BUFFER = 50.0 ;
	
    // $[NP24026]
	// reset transientTime_ to 0.0 when the following event occurs
	if ( exhDryFlow > baseFlow &&
			(exhFlowBelowBaseFlow_ || timeToBaseFlowObtained_ &&
					elapsedTime_ > timeToBaseFlow_ + TIME_BUFFER))
	{
		// $[TI37]
		transientTime_ = 0.0 ;
	}
	// $[TI38]
	
	Boolean flowTrigCondition2 = FALSE;

	// If Leak Compensation is enabled, use leak compensated flow trigger condition 
	// otherwise use the orginal flow trigger condition. $[LC24030] $[LC24029]
	if (RLeakCompMgr.isEnabled())
	{
		
		Real32 filteredCompLungFlow = RLungData.getFilteredLungFlow();
		if (triggerId_ == Trigger::NET_FLOW_BACKUP_INSP)
		{
            flowTrigCondition2 =  ( filteredCompLungFlow > flowSens );
		}
		else
		{

			static Real32 flowLimit = 0.992f;  

			if (elapsedTime_ == 0)
			{
			    flowLimit = 0.992f;  
			}
			else
			{
				flowLimit *= 0.97044f;  
			} 

		    // The conditions on dry exhalation flow ANDed with flow sensitivity condition
			// is intended to reduce autocycling when exhalation leak rate is overestimated 
			// or underestimated.
			flowTrigCondition2 =  ((( filteredCompLungFlow > (flowSens + flowLimit) ) && 
									(exhDryFlow <= (ESTIMATED_FLOW_LIMIT + MAX_VALUE(0.0f, RPeepController.getLeakAdd() - RLeakCompMgr.getLeakFlow() )))) ||
									((exhDryFlow <= (MAX_VALUE( (ESTIMATED_FLOW_LIMIT - 0.5f) + MIN_VALUE(RLeakCompMgr.getExhFlowOffset(), 0.0f), 0.3f))) &&
									 (BreathRecord::GetTrueExhTime() > 450)) 
								   );



		}
	}
	else
	{
		if (dOffset_ >= 0.0f)
			flowTrigCondition2 = (exhDryFlow <= (ESTIMATED_FLOW_LIMIT + 0.1f* dOffset_));
		else
			flowTrigCondition2 = (exhDryFlow <= (ESTIMATED_FLOW_LIMIT + 1.0f* dOffset_));

	}

	if (trigFlowCount_ >= MAX_FLOW_PTS && trigFlowCount_ < transientTime_ &&
			RPeepController.getFeedbackPressure() > (RPeep.getActualPeep() - 0.5) )
	{
        // $[NP24024]
		// $[TI14]
		if (RExhPressureSensor.getSlope() <= 0.0F &&
			    flowTrigCondition2 &&
				flowSlopeNegCount_ > 0)
		{
			// $[TI15]
			pressSlopeNegCount_ += 0.1 ;
			
			if (RExhPressureSensor.getSlope() < 0.0F)
			{
				// $[TI16]
				pressSlopeNegCount_ += 0.4 ;	
			}
			// $[TI17]
		}
		else
		{
			// $[TI18]
			pressSlopeNegCount_ = 0.0 ;
		}

		// Transient
		cycleState_ =  TRANSIENT;
	}
	else
	{
		// Steady State
		cycleState_ =  STEADY;
		
        // $[[NP24025]
		// $[TI19]
		if (RExhPressureSensor.getSlope() <= 0.0F && flowSlopeNegCount_ > 0)
		{
			// $[TI20]
			pressSlopeNegCount_ += 1.0 ;
		}
		else
		{
			// $[TI21]
			pressSlopeNegCount_ = 0.0 ;
		}
	}
	
  	if (!isTriggerActive_)
  	{
		// $[TI22]
    	activateTrigger_() ;
  	}
	// $[TI23]
	
  	if  ( isTriggerActive_ &&
		    flowTrigCondition2 &&
        	flowSlopeNegCount_ >= countTarget &&
        	pressSlopeNegCount_ >= 1.0)
  	{
		// $[TI24]
    	rtnValue = TRUE ;
  	}
	// $[TI25]

	elapsedTime_ += CYCLE_TIME_MS ;
  	return (rtnValue) ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getCycleState()
//
//@ Interface-Description
//      This method has no arguments.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Returns the current cycle state of the flow during exhalation.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
Uint32
NetFlowInspTrigger::getCycleState( void)
{
	CALL_TRACE("NetFlowInspTrigger::getCycleState( void)") ;

	return( cycleState_) ;
}



//=====================================================================
//
//  Private Methods...
//
//=====================================================================
