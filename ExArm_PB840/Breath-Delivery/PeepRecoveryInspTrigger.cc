#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: PeepRecoveryInspTrigger - Triggers Peep recovery breath.
//---------------------------------------------------------------------
//@ Interface-Description
//    This class is enabled on transitions that require establishment of
//    Peep such that no breath stacking would occur.
//---------------------------------------------------------------------
//@ Rationale
//    Sometimes peep can not be established right away since the breath
//    phase and the exhaled flow are restrictive.
//---------------------------------------------------------------------
//@ Implementation-Description
//    This class contains a boolean state variable to keep track of whether 
//    this trigger is enabled or not.  If the trigger is not enabled, it  
//    will always return a state of false.  If it is enabled, the 
//    breath phase restricted condition is checked by the trigger.
//---------------------------------------------------------------------
//@ Fault-Handling
//    n/a.
//---------------------------------------------------------------------
//@ Restrictions
//    Only one instance of this trigger may be created.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/PeepRecoveryInspTrigger.ccv   25.0.4.0   19 Nov 2013 14:00:02   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006   By: syw   Date:  01-Jun-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//		PAV project-related changes:
//		Changes to accommodate PAV_INSPIRATORY_PAUSE enum.
//
//  Revision: 005  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 004  By:  iv    Date:  25-Jul-1997   DR Number: DCS 2298
//  	Project:  Sigma (R8027)
//		Description:
//          Removed the unit test driver form tip.
//
//  Revision: 003  By:  iv    Date:  24-Jul-1997   DR Number: DCS 2298
//  	Project:  Sigma (R8027)
//		Description:
//          Added a unit test driver.
//
//  Revision: 002  By:  sp    Date:  31-Oct-1996    DR Number:
//       Project:  Sigma (R8027)
//       Description:
//             Inspection rework.
//
//  Revision: 001  By:  sp    Date:  26-Sep-1996    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "PeepRecoveryInspTrigger.hh"

//@ Usage-Classes
#  include "BreathRecord.hh"
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: PeepRecoveryInspTrigger()  
//
//@ Interface-Description
//	This Contstructor takes TriggerId as an argument.
//---------------------------------------------------------------------
//@ Implementation-Description
//      Initialize triggerId_ using BreathTrigger's constructor.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

PeepRecoveryInspTrigger::PeepRecoveryInspTrigger(Trigger::TriggerId id) 
: BreathTrigger(id)
{
  CALL_TRACE("PeepRecoveryInspTrigger()");
  // $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~PeepRecoveryInspTrigger()  
//
//@ Interface-Description
//	Destructor
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

PeepRecoveryInspTrigger::~PeepRecoveryInspTrigger(void)
{
  CALL_TRACE("~PeepRecoveryInspTrigger()");

}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
PeepRecoveryInspTrigger::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, PEEPRECOVERYINSPTRIGGER,
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)



//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: triggerCondition_
//
//@ Interface-Description
//    This method takes no parameters.  If the phase is not restricted, the method
//    returns true, otherwise, the method returns false.
//---------------------------------------------------------------------
//@ Implementation-Description
//    The following conditions must be met if an inspiration is to be triggered:
//        1.    BreathRecord::IsPhasedRestricted() is FALSE.
//        2.    PhaseType_ is not BreathPhaseType::INSPIRATORY_PAUSE
//        3.    PhaseType_ is not BreathPhaseType::EXPIRATORY_PAUSE
//        4.    PhaseType_ is not BreathPhaseType::BILEVEL_PAUSE_PHASE
//        5.    PhaseType_ is not BreathPhaseType::PAV_INSPIRATORY_PAUSE
//    If the above condition is met, then this method returns true.
// $[04012] $[01247]
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Boolean
PeepRecoveryInspTrigger::triggerCondition_(void)
{
  CALL_TRACE("triggerCondition_()");

  Boolean rtnValue = FALSE;
  if (!BreathRecord::IsPhaseRestricted() &&
  	   BreathRecord::GetPhaseType() != BreathPhaseType::INSPIRATORY_PAUSE &&
  	   BreathRecord::GetPhaseType() != BreathPhaseType::PAV_INSPIRATORY_PAUSE &&
  	   BreathRecord::GetPhaseType() != BreathPhaseType::EXPIRATORY_PAUSE &&
  	   BreathRecord::GetPhaseType() != BreathPhaseType::BILEVEL_PAUSE_PHASE)
  {
    //$[TI1]
    rtnValue = TRUE;
  }
  //$[TI2]

  //TRUE: $[TI3]
  //FALSE: $[TI4]
  return (rtnValue);
}

//=====================================================================
//
//  Private Methods...
//
//=====================================================================
