#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sotred in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// =========================== M O D U L E   D E S C R I P T I O N ====
//@ Class: DoubleBuffer - Double-Buffered Memory Framework.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a generic mechanism to manage a double-buffered data item.
//  This class is a base class that is used by a derived class to
//  manage these data items.  This class regards the data item as
//  two buffers of bytes, each of the same size.  It is these two
//  buffers that all interactions with the data items is performed
//  by this class.
//---------------------------------------------------------------------
//@ Rationale
//  This class provides a common framework for managing a data item
//  whose integrity is to be ensured at all times.
//---------------------------------------------------------------------
//@ Implementation-Description
//  A record of buffer information is kept for each of the two
//  buffers ('InfoRecord_').  This record contains a pointer to the
//  buffer, the size of the buffer, a CRC value for the buffer, and
//  a CRC value for the fields of this information record.  A
//  single-worded value is used to indicate the current state of the two
//  buffers.  While the state, stored in 'currState_', indicates which,
//  if any, of the two buffers are valid.
//
//  This class does NOT use a semaphore to protect from multiple threads,
//  it is left as the responsibility of the derived class for protection.
//---------------------------------------------------------------------
//@ Fault-Handling
//  none
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Persistent-Objects/vcssrc/DoubleBuffer.ccv   25.0.4.0   19 Nov 2013 14:18:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By:  sah    Date:  01-Jan-1999    DCS Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' to a non-inlined method.
//
//  Revision: 002  By:  sah    Date:  21-Oct-1997    DCS Number: 2575
//  Project:  Sigma (R8027)
//  Description:
//	Fixed problem where an "actual" state indicating that one of the
//	buffers was valid, combined with an "expected" state of the other
//	buffer being valid, returned a status of 'ITEM_VERIFIED'.
//
//  Revision: 001  By:  sah    Date:  15-Mar-1997    DCS Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//          Initial version (Integration baseline).
//  
//=====================================================================

#include "DoubleBuffer.hh"
#include <string.h>  // for 'memcpy()' and 'memcmp()'...

//@ Usage-Classes...
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ~DoubleBuffer()  [Destructor]
//
//@ Interface-Description
//  Destroy this double-buffered data instance.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

DoubleBuffer::~DoubleBuffer(void)
{
  CALL_TRACE("~DoubleBuffer()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//            [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//  and 'lineNumber' are essential pieces of information.  The
//  'pFileName' and 'pPredicate' strings may be defaulted in the macro
//  to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//  none 
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

void
DoubleBuffer::SoftFault(const SoftFaultID  softFaultID,
		        const Uint32       lineNumber,
		        const char*        pFileName,
		        const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, PERSISTENT_OBJS, DOUBLE_BUFFER,
                          lineNumber, pFileName, pPredicate);
}

 
//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  DoubleBuffer(pBuffer0, pBuffer1, bufferSize)  [Constructor]
//
//@ Interface-Description
//  Construct a double-buffered memory base class, using 'pBuffer0' and
//  'pBuffer1' as pointers to the two buffers that this double buffer
//  utility consists of.  The size of each buffer is given by
//  'bufferSize'.
//
//  This constructor will complete BEFORE the constructors of the
//  two data items to be buffered are started, therefore this double
//  buffer is left in an "unknown" state.  The derived class is
//  required to update this double buffer after the data items'
//  constructors complete.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  (pBuffer0 == ((Byte*)this + sizeof(DoubleBuffer)))
//  (pBuffer0 < pBuffer1  &&  (pBuffer0 + bufferSize) == pBuffer1)
//---------------------------------------------------------------------
//@ PostConditions
//  (getState() == DoubleBuffer::ILLEGAL_STATE)
//@ End-Method
//=====================================================================

DoubleBuffer::DoubleBuffer(Byte*        pBuffer0,
			   Byte*        pBuffer1,
			   const size_t bufferSize)
			   : currState_(DoubleBuffer::ILLEGAL_STATE)
{
  //===================================================================
  //  at this point the current state is 'ILLEGAL_STATE'...
  //===================================================================

  CALL_TRACE("DoubleBuffer(pBuffer0, pBuffer1, bufferSize)");
  CLASS_PRE_CONDITION((pBuffer0 == ((Byte*)this + sizeof(DoubleBuffer))));
  CLASS_PRE_CONDITION((pBuffer1 == (pBuffer0 + bufferSize)));

#if defined(SIGMA_DEVELOPMENT)
  // test the assertion that 'InfoRecord_' is NOT "padded" by the compiler...
  CLASS_ASSERTION((sizeof(InfoRecord_) ==
		     (sizeof(Byte*) + sizeof(Uint) + 2*sizeof(CrcValue))));

  // test the assertion that 'infoCrc' is the last field of 'InfoRecord_'...
  CLASS_ASSERTION(((sizeof(InfoRecord_) - sizeof(CrcValue)) ==
							infoCrcOffset_()));
#endif  // defined(SIGMA_DEVELOPMENT)

  // initialize the buffer data for the READ buffer...
  arrInfoRecord_[DoubleBuffer::READ_BUFFER_ID].pBuffer    = pBuffer0;
  arrInfoRecord_[DoubleBuffer::READ_BUFFER_ID].bufferSize = bufferSize;
  arrInfoRecord_[DoubleBuffer::READ_BUFFER_ID].bufferCrc  = (CrcValue)0;
  arrInfoRecord_[DoubleBuffer::READ_BUFFER_ID].infoCrc    = (CrcValue)0;

  // initialize the buffer data for the WRITE buffer...
  arrInfoRecord_[DoubleBuffer::WRITE_BUFFER_ID].pBuffer    = pBuffer1;
  arrInfoRecord_[DoubleBuffer::WRITE_BUFFER_ID].bufferSize = bufferSize;
  arrInfoRecord_[DoubleBuffer::WRITE_BUFFER_ID].bufferCrc  = (CrcValue)0;
  arrInfoRecord_[DoubleBuffer::WRITE_BUFFER_ID].infoCrc    = (CrcValue)0;
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  initBuffer_(sizeofDataItem)
//
//@ Interface-Description
//  This is called when a system reset occurred, therefore this instance
//  needs to be verified and initialized.  The actual size of the buffered
//  data items (i.e., 'sizeofDataItem') is used to ensure that the
//  information contained within this double buffer is correct.  There are
//  three types of "initialization" that can occur:  verification only,
//  "correction" of a partially invalid buffer, or a full initialization.
//
//  If both buffers are valid, then only a verification of validity is done.
//  The resulting state is left to indicate the "actual" state of the
//  instance, and 'NovRamStatus::ITEM_VERIFIED' is returned.
//
//  If one of the buffers is invalid, while the other is valid, the invalid
//  buffer is "corrected" using the valid buffer.  This "correction" leaves
//  the new state of this buffer as 'DoubleBuffer::BOTH_BUFFERS_OK', and
//  'NovRamStatus::ITEM_RECOVERED' is returned.
//
//  Finally, if neither buffer is in a valid (or "correctable") state, this
//  buffer's state is set to 'DoubleBuffer::ILLEGAL_STATE', and
//  'NovRamStatus::ITEM_INITIALIZED' is returned.  This indicates to this
//  class's derived class that a full initialization needs to be initiated.
//---------------------------------------------------------------------
//@ Implementation-Description
//  When verifying a buffer, the buffer's information ('InfoRecord_') must
//  be verified BEFORE that buffer's memory is verified.  The reason
//  for this restriction is that if the buffer's information is
//  invalid (especially its pointer to the buffer or its buffer size),
//  the actual verification of the buffer can not be run with confidence.
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  (getState() == DoubleBuffer::BOTH_BUFFERS_OK  ||
//   getState() == DoubleBuffer::ILLEGAL_STATE)
//@ End-Method
//=====================================================================

NovRamStatus::InitializationId
DoubleBuffer::initBuffer_(const size_t sizeofDataItem)
{
  CALL_TRACE("initBuffer_(sizeofDataItem)");

  DoubleBufferState  actualState = DoubleBuffer::ILLEGAL_STATE;

  //===================================================================
  //  'actualState' is set to 'ILLEGAL_STATE'...
  //===================================================================

  // verify the correctness of the information record for the READ
  // buffer...$[TI1] (TRUE)  $[TI2] (FALSE)
  const Boolean  IS_READ_INFO_VALID =
	      isInfoValid_(DoubleBuffer::READ_BUFFER_ID, sizeofDataItem);

  // is the READ buffer valid?...
  if (IS_READ_INFO_VALID  &&  isBufferValid_(DoubleBuffer::READ_BUFFER_ID))
  {   // $[TI3]
    // it is true that the READ buffer is valid, therefore set
    // 'actualState' to 'READ_BUFFER_OK'...
    actualState = DoubleBuffer::READ_BUFFER_OK;
  }   // $[TI4] -- the READ buffer is NOT valid...

  //===================================================================
  //  'actualState' is either 'READ_BUFFER_OK' or 'ILLEGAL_STATE'...
  //===================================================================

  // verify the correctness of the information record for the WRITE
  // buffer...$[TI5] (TRUE)  $[TI6] (FALSE)
  const Boolean  IS_WRITE_INFO_VALID =
	      isInfoValid_(DoubleBuffer::WRITE_BUFFER_ID, sizeofDataItem);

  // is the WRITE buffer valid?...
  if (IS_WRITE_INFO_VALID  &&  isBufferValid_(DoubleBuffer::WRITE_BUFFER_ID))
  {   // $[TI7]
    // it is true that the WRITE buffer is valid...
    if (actualState == DoubleBuffer::READ_BUFFER_OK)
    {   // $[TI7.1]
      // it is also true that the READ buffer is valid, therefore make
      // sure that both buffers contain equivalent data...
      if (::memcmp(arrInfoRecord_[DoubleBuffer::READ_BUFFER_ID].pBuffer,
		   arrInfoRecord_[DoubleBuffer::WRITE_BUFFER_ID].pBuffer,
		   arrInfoRecord_[DoubleBuffer::WRITE_BUFFER_ID].bufferSize) == 0)
      {   // $[TI7.1.1]
	// both buffers are valid AND they both have the same data,
	// therefore both buffers are OK...
	actualState = DoubleBuffer::BOTH_BUFFERS_OK;
      }
      else
      {   // $[TI7.1.2]
	// both buffers are valid BUT they have different data, therefore
	// since during normal operation this can only happen in the middle
	// of an update (using the WRITE buffer), the READ buffer is no
	// longer considered valid...
	actualState = DoubleBuffer::WRITE_BUFFER_OK;
      }
    }
    else
    {   // $[TI7.2]
      // only the WRITE buffer is valid...
      actualState = DoubleBuffer::WRITE_BUFFER_OK;
    }
  }   // $[TI8] -- the WRITE buffer is NOT valid...

  //===================================================================
  // 'actualState' is either 'BOTH_BUFFERS_OK', 'READ_BUFFER_OK',
  // 'WRITE_BUFFER_OK', or 'ILLEGAL_STATE'...
  //===================================================================

  if (actualState != DoubleBuffer::ILLEGAL_STATE  &&  !doBuffersMatch_())
  {   // $[TI9]
    // the pointers and sizes of the READ and WRITE buffer don't "match",
    // therefore use the valid buffer to "correct" the invalid one, and
    // make sure that this corruption is noted (i.e., return 'ITEM_RECOVERED')
    // by setting 'currState_' to "ILLEGAL"...
    currState_ = DoubleBuffer::ILLEGAL_STATE;

    //=================================================================
    // 'actualState' will ALWAYS be either 'READ_BUFFER_OK' or
    // 'WRITE_BUFFER_OK', in this path...
    //=================================================================

    if (actualState == DoubleBuffer::READ_BUFFER_OK)
    {   // $[TI9.1]
      // the READ buffer is valid, but the WRITE buffer's information
      // fields are not valid, therefore "correct" the pointer and size
      // fields so that an update can be done...

      // "correct" the WRITE buffer's pointer by using the READ buffer's
      // pointer and size...
      arrInfoRecord_[DoubleBuffer::WRITE_BUFFER_ID].pBuffer =
		(arrInfoRecord_[DoubleBuffer::READ_BUFFER_ID].pBuffer +
		 arrInfoRecord_[DoubleBuffer::READ_BUFFER_ID].bufferSize);

      // "correct" the WRITE buffer's size by copying the READ buffer's
      // size...
      arrInfoRecord_[DoubleBuffer::WRITE_BUFFER_ID].bufferSize =
		    arrInfoRecord_[DoubleBuffer::READ_BUFFER_ID].bufferSize;
    }
    else if (actualState == DoubleBuffer::WRITE_BUFFER_OK)
    {   // $[TI9.2]
      // the WRITE buffer is valid, but the READ buffer's information
      // fields are not valid, therefore "correct" the pointer and size
      // fields so that an update can be done...

      // "correct" the READ buffer's pointer by using the WRITE buffer's
      // pointer and size...
      arrInfoRecord_[DoubleBuffer::READ_BUFFER_ID].pBuffer =
		(arrInfoRecord_[DoubleBuffer::WRITE_BUFFER_ID].pBuffer -
		 arrInfoRecord_[DoubleBuffer::WRITE_BUFFER_ID].bufferSize);

      // "correct" the READ buffer's size by copying the WRITE buffer's
      // size...
      arrInfoRecord_[DoubleBuffer::READ_BUFFER_ID].bufferSize =
		    arrInfoRecord_[DoubleBuffer::WRITE_BUFFER_ID].bufferSize;
    }
    else
    {
      // unexpected 'actualState'...
      AUX_CLASS_ASSERTION_FAILURE(actualState);
    }

    // at this point, they should ALWAYS "match"...
    SAFE_AUX_CLASS_ASSERTION((doBuffersMatch_()), actualState);
  }   // $[TI10] -- either 'ILLEGAL_STATE' or buffers DO match...

  NovRamStatus::InitializationId  initStatusId;

  switch (currState_)
  {
  case DoubleBuffer::BOTH_BUFFERS_OK :	// $[TI11]
    switch (actualState)
    {
    case DoubleBuffer::BOTH_BUFFERS_OK :	// $[TI11.1]
      // both buffers were previously OK, and they both still are...
      initStatusId = NovRamStatus::ITEM_VERIFIED;
      break;
    case DoubleBuffer::READ_BUFFER_OK :
    case DoubleBuffer::WRITE_BUFFER_OK :	// $[TI11.2]
      // both buffers were previously OK, but only one of them is OK now,
      // therefore a "recovery" is needed...
      initStatusId = NovRamStatus::ITEM_RECOVERED;
      break;
    case DoubleBuffer::ILLEGAL_STATE :		// $[TI11.3]
      // both buffers were previously OK, but neither one is OK now, therefore
      // a full initialization is needed...
      initStatusId = NovRamStatus::ITEM_INITIALIZED;
      break;
    default :
      // something happened to 'actualState'...
      AUX_CLASS_ASSERTION_FAILURE(actualState);
      initStatusId = NovRamStatus::ITEM_INITIALIZED;  // never gets here...
      break;
    };
    break;

  case DoubleBuffer::READ_BUFFER_OK :	// $[TI12]
    switch (actualState)
    {
    case DoubleBuffer::BOTH_BUFFERS_OK :
    case DoubleBuffer::READ_BUFFER_OK :		// $[TI12.1]
      // a reset may have occurred during an update of this instance,
      // therefore consider this instance fully valid...
      initStatusId = NovRamStatus::ITEM_VERIFIED;
      break;
    case DoubleBuffer::WRITE_BUFFER_OK :	// $[TI12.3]
      // the READ buffer was previously OK, but now only the WRITE buffer
      // is OK, therefore a "recovery" is needed...
      initStatusId = NovRamStatus::ITEM_RECOVERED;
      break;
    case DoubleBuffer::ILLEGAL_STATE :		// $[TI12.2]
      // one buffer was previously OK, but now that buffer is invalid,
      // therefore a full initialization is needed...
      initStatusId = NovRamStatus::ITEM_INITIALIZED;
      break;
    default :
      // something happened to 'actualState'...
      AUX_CLASS_ASSERTION_FAILURE(actualState);
      initStatusId = NovRamStatus::ITEM_INITIALIZED;  // never gets here...
      break;
    };
    break;

  case DoubleBuffer::WRITE_BUFFER_OK :	// $[TI17]
    switch (actualState)
    {
    case DoubleBuffer::BOTH_BUFFERS_OK :
    case DoubleBuffer::WRITE_BUFFER_OK :	// $[TI17.1]
      // a reset may have during an update of this instance,
      // therefore consider this instance fully valid...
      initStatusId = NovRamStatus::ITEM_VERIFIED;
      break;
    case DoubleBuffer::READ_BUFFER_OK :		// $[TI17.2]
      // the WRITE buffer was previously OK, but now only the READ buffer
      // is OK, therefore a "recovery" is needed...
      initStatusId = NovRamStatus::ITEM_RECOVERED;
      break;
    case DoubleBuffer::ILLEGAL_STATE :		// $[TI17.3]
      // one buffer was previously OK, but now that buffer is invalid,
      // therefore a full initialization is needed...
      initStatusId = NovRamStatus::ITEM_INITIALIZED;
      break;
    default :
      // something happened to 'actualState'...
      AUX_CLASS_ASSERTION_FAILURE(actualState);
      initStatusId = NovRamStatus::ITEM_INITIALIZED;  // never gets here...
      break;
    };
    break;

  case DoubleBuffer::ILLEGAL_STATE :
  default :				// $[TI13]
    switch (actualState)
    {
    case DoubleBuffer::BOTH_BUFFERS_OK :
    case DoubleBuffer::READ_BUFFER_OK :
    case DoubleBuffer::WRITE_BUFFER_OK :	// $[TI13.1]
      // 'currState_' is/was corrupted, therefore "recover" to the actual
      // state...
      initStatusId = NovRamStatus::ITEM_RECOVERED;
      break;
    case DoubleBuffer::ILLEGAL_STATE :		// $[TI13.2]
      // neither buffer is OK, therefore a full initialization is needed...
      initStatusId = NovRamStatus::ITEM_INITIALIZED;
      break;
    default :
      // something happened to 'actualState'...
      AUX_CLASS_ASSERTION_FAILURE(actualState);
      initStatusId = NovRamStatus::ITEM_INITIALIZED;  // never gets here...
      break;
    };
    break;
  };

  // make the current state the actual state...
  currState_ = actualState;

  if (currState_ == DoubleBuffer::READ_BUFFER_OK)
  {   // $[TI14]
    // only the READ buffer is OK, therefore update the WRITE buffer
    // using the READ buffer...
    updateUsingReadBuff_();
  }
  else if (currState_ == DoubleBuffer::WRITE_BUFFER_OK)
  {   // $[TI15]
    // only the WRITE buffer is OK, therefore update the READ buffer
    // using the WRITE buffer...
    updateUsingWriteBuff_();
  }   // $[TI16] -- 'currState_' is either "BOTH" or "ILLEGAL"...

  //===================================================================
  // 'currState' is either 'BOTH_BUFFER_OK' or 'ILLEGAL_STATE'...
  //===================================================================

  // verify that this instance is left in one, of two, states, and that
  // the data within the buffer matches that state...
  SAFE_AUX_CLASS_ASSERTION((
	(currState_ == DoubleBuffer::BOTH_BUFFERS_OK  &&
	 verifyBuffer_(sizeofDataItem) == NovRamStatus::ITEM_VALID)  ||
	currState_ == DoubleBuffer::ILLEGAL_STATE), currState_);

  return(initStatusId);
}

 
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  verifyBuffer_(sizeofDataItem)
//
//@ Interface-Description
//  This verifies that this instance is in a valid state.  A valid
//  state is defined as containing a valid buffer state value
//  (i.e. 'BOTH_BUFFERS_OK', 'READ_BUFFER_OK', or 'WRITE_BUFFER_OK'),
//  that matches the actual state of the buffer and buffer information.
//  For example, if the current state is 'READ_BUFFER_OK', the READ
//  buffer must match its CRC AND its buffer information block must
//  match its buffer info CRC.  There are no requirements of the state
//  of a buffer that is not explicitly identified in the state variable
//  (i.e., if the current state is 'READ_BUFFER_OK' there is NO
//  requirement on the state of the WRITE buffer).  If either the current
//  state value is not a valid buffer state value (as listed above), or
//  the buffers do not pass the CRC verifications, the buffer manager
//  is considered in an invalid state.
//
//  If this instance is determined to be in a VALID state,
//  'NovRamStatus::ITEM_VALID' is returned and the current state is retained,
//  otherwise 'NovRamStatus::ITEM_INVALID' is returned and this instance is
//  left with its state set to 'ILLEGAL_STATE'.
//---------------------------------------------------------------------
//@ Implementation-Description
//  When verifying a buffer, the buffer's information ('InfoRecord_') must
//  be verified BEFORE that buffer's memory is verified.  The reason
//  for this restriction is that if the buffer's information is
//  invalid (especially its pointer to the buffer or its buffer size),
//  the actual verification of the buffer can not be run with confidence.
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  ((verifyBuffer_(sizeofDataItem) == NovRamStatus::ITEM_VALID  &&
//    (getState() == DoubleBuffer::BOTH_BUFFERS_OK  ||
//     getState() == DoubleBuffer::READ_BUFFER_OK   ||
//     getState() == DoubleBuffer::WRITE_BUFFER_OK))  ||
//   (verifyBuffer_(sizeofDataItem) == NovRamStatus::ITEM_INVALID  &&
//    getState() == DoubleBuffer::ILLEGAL_STATE))
//@ End-Method
//=====================================================================

NovRamStatus::VerificationId
DoubleBuffer::verifyBuffer_(const size_t sizeofDataItem)
{
  CALL_TRACE("verifyBuffer_(sizeofDataItem)");

  NovRamStatus::VerificationId  verifStatusId = NovRamStatus::ITEM_INVALID;

  if (doBuffersMatch_())
  {   // $[TI1]
    // the buffers do "match" each other -- as far as the buffer
    // information -- now see if the buffers are valid...

    //=================================================================
    // NOTE:  The info fields MUST be verified before the buffers,
    //        themselves, thereby ensuring that the information containing
    //        the pointer to the buffer and its size is valid.
    //=================================================================

    // verify that the READ buffer's information record AND buffer is
    // currently valid...$[TI1.1] (TRUE)  $[TI1.2] (FALSE)...
    const Boolean  IS_READ_BUFFER_VALID =
	    (isInfoValid_(DoubleBuffer::READ_BUFFER_ID, sizeofDataItem)  &&
	     isBufferValid_(DoubleBuffer::READ_BUFFER_ID));

    // verify that the WRITE buffer's information record AND buffer is
    // currently valid...$[TI1.3] (TRUE)  $[TI1.4] (FALSE)...
    const Boolean  IS_WRITE_BUFFER_VALID =
	    (isInfoValid_(DoubleBuffer::WRITE_BUFFER_ID, sizeofDataItem)  &&
	     isBufferValid_(DoubleBuffer::WRITE_BUFFER_ID));

    switch (currState_)
    {
    case DoubleBuffer::BOTH_BUFFERS_OK :	// $[TI1.5]
      // 'currState' contains a legal state value, therefore check to make
      // sure that both the READ and WRITE buffer are valid; if the two
      // buffers are valid, test (using '::memcmp()') to make sure they are
      // equal to each other...
      if (IS_READ_BUFFER_VALID  &&  IS_WRITE_BUFFER_VALID  &&
	  ::memcmp(arrInfoRecord_[DoubleBuffer::READ_BUFFER_ID].pBuffer,
		   arrInfoRecord_[DoubleBuffer::WRITE_BUFFER_ID].pBuffer,
		   arrInfoRecord_[DoubleBuffer::WRITE_BUFFER_ID].bufferSize) == 0)
      {   // $[TI1.5.1]
	// it is true that both buffers are valid AND equivalent, therefore
	// return 'NovRamStatus::ITEM_VALID'...
	verifStatusId = NovRamStatus::ITEM_VALID;
      }   // $[TI1.5.2] -- BOTH buffers are NOT valid...

      break;
    case DoubleBuffer::WRITE_BUFFER_OK :	// $[TI1.6]
      // 'currState' contains a legal state value, therefore check to make
      // sure that the WRITE buffer and its buffer information table match
      // their CRCs...
      if (IS_WRITE_BUFFER_VALID)
      {   // $[TI1.6.1]
	// it is true that the WRITE buffer is valid, therefore return
	// 'NovRamStatus::ITEM_VALID'...
	verifStatusId = NovRamStatus::ITEM_VALID;
      }   // $[TI1.6.2] -- the WRITE buffer is NOT valid...

      break;
    case DoubleBuffer::READ_BUFFER_OK :		// $[TI1.7]
      // 'currState' contains a legal state value, therefore check to make
      // sure that the READ buffer and its buffer information table match
      // their CRCs...
      if (IS_READ_BUFFER_VALID)
      {   // $[TI1.7.1]
	// it is true that the READ buffer is valid, therefore return
	// 'NovRamStatus::ITEM_VALID'...
	verifStatusId = NovRamStatus::ITEM_VALID;
      }   // $[TI1.7.2] -- the READ buffer is NOT valid...

      break;
    case DoubleBuffer::ILLEGAL_STATE :
    default :					// $[TI1.8]
      // do nothing, just fall through...
      break;
    };  // switch (currState_)...
  }   // $[TI2] -- the buffers do NOT "match"...

  if (verifStatusId == NovRamStatus::ITEM_INVALID)
  {   // $[TI3]
    // either the actual state of the buffers did not match the state in
    // 'currState_', or 'currState_' contained an invalid state value,
    // therefore put this buffer manager into its "illegal" state...
    currState_ = DoubleBuffer::ILLEGAL_STATE;
  }   // $[TI4] -- this instance is valid...

  return(verifStatusId);
}

 
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  accessReadBuffer_()  [const]
//
//@ Interface-Description
//  Return the index of the current read-only buffer item.  The state
//  of this double-buffered memory manager will not change.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  (getState() == DoubleBuffer::BOTH_BUFFERS_OK  ||
//   getState() == DoubleBuffer::READ_BUFFER_OK   ||
//   getState() == DoubleBuffer::WRITE_BUFFER_OK)
//---------------------------------------------------------------------
//@ PostConditions
//  (getState() == OLD{getState()})
//@ End-Method
//=====================================================================

Uint
DoubleBuffer::accessReadBuffer_(void) const
{
  CALL_TRACE("accessReadBuffer_()");

  //===================================================================
  //  at this point the current state is either 'BOTH_BUFFERS_OK',
  //  'READ_BUFFER_OK', or 'WRITE_BUFFER_OK'...
  //===================================================================

  DoubleBuffer::BufferId  bufferId;

  switch (currState_)
  {
  case DoubleBuffer::BOTH_BUFFERS_OK :		// $[TI1]
  case DoubleBuffer::READ_BUFFER_OK :		// $[TI2]
    // if both buffers are OK or only the READ buffer is OK, it is the
    // READ buffer that is used as the read-only buffer...
    bufferId = DoubleBuffer::READ_BUFFER_ID;
    break;
  case DoubleBuffer::WRITE_BUFFER_OK :		// $[TI3]
    // if only the WRITE buffer is OK, it is the WRITE buffer that is
    // used as the read-only buffer...
    bufferId = DoubleBuffer::WRITE_BUFFER_ID;
    break;
  case DoubleBuffer::ILLEGAL_STATE :
  default :
    AUX_CLASS_ASSERTION_FAILURE(currState_);
    bufferId = DoubleBuffer::READ_BUFFER_ID;  // never run; avoids warning...
    break;
  };

  return((Uint)bufferId);
}

 
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  accessWriteBuffer_()
//
//@ Interface-Description
//  Return the index of the current writable buffer item.  Since access
//  to the write buffer is being granted and the client (the derived
//  class) can change the WRITE buffer, the WRITE buffer will NOT
//  be considered OK when this method returns.  In other words, the
//  state will be left as 'READ_BUFFER_OK'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  (getState() == DoubleBuffer::BOTH_BUFFERS_OK)
//---------------------------------------------------------------------
//@ PostConditions
//  (getState() == DoubleBuffer::READ_BUFFER_OK)
//@ End-Method
//=====================================================================

Uint
DoubleBuffer::accessWriteBuffer_(void)
{
  CALL_TRACE("accessWriteBuffer_()");
  AUX_CLASS_PRE_CONDITION((currState_ == DoubleBuffer::BOTH_BUFFERS_OK),
  			  currState_);

  //===================================================================
  //  at this point the current state is 'BOTH_BUFFERS_OK'...
  //===================================================================

  BufferId  bufferId;

  // since the WRITE buffer is being accessed and may be changed,
  // the current state must reflect the fact that the WRITE buffer
  // may not be valid...
  currState_ = DoubleBuffer::READ_BUFFER_OK;
  bufferId   = DoubleBuffer::WRITE_BUFFER_ID;

  //===================================================================
  //  at this point the current state is 'READ_BUFFER_OK'...
  //===================================================================

  SAFE_AUX_CLASS_POST_CONDITION((currState_ == DoubleBuffer::READ_BUFFER_OK),
				currState_, DoubleBuffer);

  return((Uint)bufferId);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  updateUsingReadBuff_()
//
//@ Interface-Description
//  Update this double-buffered memory manager using the current READ
//  buffer.  This uses the current READ buffer to update the WRITE
//  buffer.  This method is only to be used when the READ buffer is the
//  only valid buffer (see 'initBuffer_()').
//---------------------------------------------------------------------
//@ Implementation-Description
//  The bytes of the READ buffer are copied into the WRITE buffer,
//  then the CRC for the READ buffer is copied into the WRITE buffer's
//  information item.  Next, the CRC for the WRITE buffer's information
//  is calculated.  Finally, the state is changed from a read-buffer-OK
//  state to both-buffers-OK.
//---------------------------------------------------------------------
//@ PreConditions
//  (getState() == DoubleBuffer::READ_BUFFER_OK)
//---------------------------------------------------------------------
//@ PostConditions
//  (getState() == DoubleBuffer::BOTH_BUFFERS_OK)
//@ End-Method
//=====================================================================

void
DoubleBuffer::updateUsingReadBuff_(void)
{
  CALL_TRACE("upateUsingReadBuff_()");
  AUX_CLASS_PRE_CONDITION((currState_ == DoubleBuffer::READ_BUFFER_OK),
  			  currState_);

#if defined(SIGMA_DEVELOPMENT)
  SAFE_CLASS_ASSERTION((doBuffersMatch_()));
  const size_t  BUFFER_SIZE =
		    arrInfoRecord_[DoubleBuffer::READ_BUFFER_ID].bufferSize;
  SAFE_CLASS_ASSERTION(
		  (verifyBuffer_(BUFFER_SIZE) == NovRamStatus::ITEM_VALID)
		      );
#endif // defined(SIGMA_DEVELOPMENT)

  //===================================================================
  //  at this point the current state is 'READ_BUFFER_OK'...
  //===================================================================

  // copy the READ buffer into the WRITE buffer...
  ::memcpy(arrInfoRecord_[DoubleBuffer::WRITE_BUFFER_ID].pBuffer,
	   arrInfoRecord_[DoubleBuffer::READ_BUFFER_ID].pBuffer,
	   arrInfoRecord_[DoubleBuffer::READ_BUFFER_ID].bufferSize);

  // copy the calculated CRC value for the current READ buffer into
  // the WRITE buffer's CRC value...
  arrInfoRecord_[DoubleBuffer::WRITE_BUFFER_ID].bufferCrc =
		    arrInfoRecord_[DoubleBuffer::READ_BUFFER_ID].bufferCrc;

  // calculate the CRC value for the current WRITE buffer's info
  // values...
  arrInfoRecord_[DoubleBuffer::WRITE_BUFFER_ID].infoCrc =
			       calcInfoCrc_(DoubleBuffer::WRITE_BUFFER_ID);

  // in a single, uninterruptible instruction change the buffer state to
  // "both O.K."...
  currState_ = DoubleBuffer::BOTH_BUFFERS_OK;

  //===================================================================
  //  at this point the current state is 'BOTH_BUFFERS_OK'...
  //===================================================================

  // verify that the Crc's "jive" with the current state...
  SAFE_CLASS_ASSERTION(
		  (verifyBuffer_(BUFFER_SIZE) == NovRamStatus::ITEM_VALID)
		      );
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  updateUsingWriteBuff_()
//
//@ Interface-Description
//  Update this double-buffered memory manager using the current WRITE
//  buffer.  This uses the current WRITE buffer to update the READ
//  buffer.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This update is done in two stages.  If the current state is a read-
//  buffer-OK or an unknown state, stage #1 is done.  Otherwise, if in a
//  write-buffer-OK state is the current state, stage #1 is skipped.
//
//  In stage #1, the CRC for the WRITE buffer is calculated and stored,
//  then the CRC for the WRITE buffer's information is calculated and
//  stored.  Finally, the current state is changed to write-buffer-OK.
//  
//  In stage #2, the bytes of the WRITE buffer are copied into the
//  READ buffer.  Next, the CRC for the READ buffer is copied from
//  the WRITE buffer, and the CRC for the READ buffer's information
//  is calculated and stored.  Finally, the current state is changed to
//  both-buffers-OK.
//---------------------------------------------------------------------
//@ PreConditions
//  (getState() == DoubleBuffer::READ_BUFFER_OK   ||
//   getState() == DoubleBuffer::WRITE_BUFFER_OK  ||
//   getState() == DoubleBuffer::ILLEGAL_STATE)
//---------------------------------------------------------------------
//@ PostConditions
//  (getState() == DoubleBuffer::BOTH_BUFFERS_OK)
//@ End-Method
//=====================================================================

void
DoubleBuffer::updateUsingWriteBuff_(void)
{
  CALL_TRACE("upateUsingWriteBuff_()");
  AUX_CLASS_PRE_CONDITION((currState_ == DoubleBuffer::READ_BUFFER_OK   ||
			   currState_ == DoubleBuffer::WRITE_BUFFER_OK  ||
			   currState_ == DoubleBuffer::ILLEGAL_STATE),
		          currState_);

  //===================================================================
  //  at this point the current state is either 'READ_BUFFER_OK',
  //  'WRITE_BUFFER_OK', or 'ILLEGAL_STATE'...
  //===================================================================

#if defined(SIGMA_DEVELOPMENT)
  SAFE_CLASS_ASSERTION((doBuffersMatch_()));
  const size_t  BUFFER_SIZE =
		    arrInfoRecord_[DoubleBuffer::READ_BUFFER_ID].bufferSize;
  // verify that the Crc's "jive" with the current state...
  if (currState_ != DoubleBuffer::ILLEGAL_STATE)
  {
    CLASS_ASSERTION((verifyBuffer_(BUFFER_SIZE) == NovRamStatus::ITEM_VALID));
  }
#endif  // defined(SIGMA_DEVELOPMENT)

  if (currState_ != DoubleBuffer::WRITE_BUFFER_OK)
  {   // $[TI1]
    // calculate the CRC value for the current WRITE buffer, and
    // store its CRC value in that buffer...
    arrInfoRecord_[DoubleBuffer::WRITE_BUFFER_ID].bufferCrc =
			      calcBufferCrc_(DoubleBuffer::WRITE_BUFFER_ID);

    // calculate the CRC value for the current WRITE buffer's info
    // values...
    arrInfoRecord_[DoubleBuffer::WRITE_BUFFER_ID].infoCrc =
    				calcInfoCrc_(DoubleBuffer::WRITE_BUFFER_ID);

    // in a single, uninterruptible instruction change the buffer data to the
    // new buffer data...
    currState_ = DoubleBuffer::WRITE_BUFFER_OK;

    // verify that the Crc's "jive" with the current state...
    SAFE_CLASS_ASSERTION(
		  (verifyBuffer_(BUFFER_SIZE) == NovRamStatus::ITEM_VALID)
		        );
  }   // $[TI2] -- 'currState_' is already "WRITE"...

  //===================================================================
  //  at this point the current state is 'WRITE_BUFFER_OK'...
  //===================================================================

  // copy the WRITE buffer into the READ buffer...
  ::memcpy(arrInfoRecord_[DoubleBuffer::READ_BUFFER_ID].pBuffer,
  	   arrInfoRecord_[DoubleBuffer::WRITE_BUFFER_ID].pBuffer,
  	   arrInfoRecord_[DoubleBuffer::WRITE_BUFFER_ID].bufferSize);

  // copy the calculated CRC value for the current WRITE buffer into
  // the READ buffers CRC value...
  arrInfoRecord_[DoubleBuffer::READ_BUFFER_ID].bufferCrc =
		   arrInfoRecord_[DoubleBuffer::WRITE_BUFFER_ID].bufferCrc;

  // calculate the CRC value for the current READ buffer's info
  // values...
  arrInfoRecord_[DoubleBuffer::READ_BUFFER_ID].infoCrc =
				calcInfoCrc_(DoubleBuffer::READ_BUFFER_ID);

  // in a single, uninterruptible instruction change the buffer state to
  // "both O.K."...
  currState_ = DoubleBuffer::BOTH_BUFFERS_OK;

  //===================================================================
  //  at this point the current state is 'BOTH_BUFFERS_OK'...
  //===================================================================

  // verify that the Crc's "jive" with the current state...
  SAFE_CLASS_POST_CONDITION(
		  (verifyBuffer_(BUFFER_SIZE) == NovRamStatus::ITEM_VALID),
			  DoubleBuffer);
}
