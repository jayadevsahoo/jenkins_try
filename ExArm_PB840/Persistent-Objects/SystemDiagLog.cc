#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sotred in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//============================ M O D U L E   D E S C R I P T I O N ====
//@ Class: SystemDiagLog - System Diagnostic Code Log.
//---------------------------------------------------------------------
//@ Interface-Description
//  This persistent information log contains the system's diagnostic
//  codes.  When a software fault, software trap, or hardware fault
//  is detected, a diagnostic code log entry ('CodeLogEntry') is added
//  to this log.  When an entry is added at a time that the log is full,
//  the oldest entry is removed to make room for the new entry.
//
//  This log is derived from 'DiagCodeLog', which manages the internal
//  array of log entries.  There are no virtual methods -- THEY ARE NOT
//  ALLOWED IN NOVRAM!
//---------------------------------------------------------------------
//@ Rationale
//---------------------------------------------------------------------
//@ Implementation-Description
//  This log, working with 'DiagCodeLog', manages an internal fixed
//  array of log entries ('CodeLogEntry').  The fixed array is defined
//  in this class, with a pointer to the abstract (size-independent)
//  base class of the fixed array stored in this class's base class.
//  This allows common functionality between the the other diagnostic
//  logs (i.e., 'SystemInfoLog' and 'PostDiagLog') to be shared in the
//  common base class.
//
//  The base class is also responsible for protecting against multiple
//  threads, therefore this class should NOT be using any semaphores
//  to protect itself -- the base class already has protection.
//---------------------------------------------------------------------
//@ Fault-Handling
//  none
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Persistent-Objects/vcssrc/SystemDiagLog.ccv   25.0.4.0   19 Nov 2013 14:18:54   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By:  sah    Date:  07-May-1998    DCS Number: 5041
//  Project:  Sigma (R8027)
//  Description:
//      Added calling of 'NovRamManager::ReportAuxillaryInfo()' to the
//	initialization method.  Also, changed 'SoftFault()' to a
//	non-inlined method.
//
//  Revision: 002  By:  sah    Date:  04-Sep-1997    DCS Number: 1923
//  Project:  Sigma (R8027)
//  Description:
//	Replaced obsolete reference to 'CommDiagLog', with a reference to
//	'SystemInfoLog'.
//
//  Revision: 001  By:  sah    Date:  15-Mar-1997    DCS Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//          Initial version (Integration baseline).
//  
//=====================================================================

#include "SystemDiagLog.hh"

//@ Usage-Classes...
#include "Array_CodeLogEntry.hh"
#include "NovRamManager.hh"
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SystemDiagLog()  [Default Constructor]
//
//@ Interface-Description
//  Construct a default diagnostic code log.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//  (isEmpty())
//@ End-Method
//=====================================================================

SystemDiagLog::SystemDiagLog(void)
		     : DiagCodeLog(arrEntries_, ::MAX_SYS_LOG_ENTRIES,
				   NovRamUpdateManager::SYSTEM_LOG_UPDATE)
{
  CALL_TRACE("SystemDiagLog()");
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ~SystemDiagLog()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this fault log.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

SystemDiagLog::~SystemDiagLog(void)
{
  CALL_TRACE("~SystemDiagLog()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  logDiagnostic(diagnosticCode)
//
//@ Interface-Description
//  Log the fault indicated by 'diagnosticCode' into this fault log.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  (!isEmpty())
//@ End-Method
//=====================================================================

void
SystemDiagLog::logDiagnostic(const DiagnosticCode diagnosticCode)
{
  CALL_TRACE("logDiagnostic(diagnosticCode)");

  if (verifySelf() != NovRamStatus::ITEM_VALID)
  {   // $[TI3]
    // logging diagnostic before instance is initialized...
    initialize();
  }   // $[TI4]

  const DiagnosticCode::DiagCodeTypeId  DIAG_CODE_TYPE =
					diagnosticCode.getDiagCodeTypeId();
  AUX_CLASS_PRE_CONDITION(
	        (DIAG_CODE_TYPE == DiagnosticCode::SYSTEM_EVENT_DIAG     ||
		 DIAG_CODE_TYPE == DiagnosticCode::SOFTWARE_TEST_DIAG    ||
		 DIAG_CODE_TYPE == DiagnosticCode::EXCEPTION_DIAG        ||
		 DIAG_CODE_TYPE == DiagnosticCode::NMI_DIAG              ||
		 DIAG_CODE_TYPE == DiagnosticCode::BACKGROUND_TEST_DIAG  ||
		 DIAG_CODE_TYPE == DiagnosticCode::STARTUP_SELF_TEST_DIAG),
	        DIAG_CODE_TYPE
			 );

  // store into this log using the base class's method...
  logDiagCode_(diagnosticCode);

  if (DIAG_CODE_TYPE != DiagnosticCode::SOFTWARE_TEST_DIAG  &&
      DIAG_CODE_TYPE != DiagnosticCode::EXCEPTION_DIAG      &&
      DIAG_CODE_TYPE != DiagnosticCode::NMI_DIAG)
  {   // $[TI1]
    NovRamUpdateManager::UpdateHappened(
				  NovRamUpdateManager::SYSTEM_LOG_UPDATE
				       );
  }   // $[TI2] -- no "update" needed, because we're re-booting...

  //---------------------------------------------------------------------
  // NOTE:  testable item #2 cannot be easily tested via unit testing,
  //        therefore extra diligence is needed during reviews.
  //---------------------------------------------------------------------
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  initialize()  [const]
//
//@ Interface-Description
//  A reset sequence occurred, therefore verify the integrity of
//  this instance.  If this log is determined to be completely valid,
//  'NovRamStatus::ITEM_VERIFIED' is returned.  If any of this log's
//  entries are not valid, but the overall infrastructure of the instance
//  is valid, then those entries are "corrected" and
//  'NovRamStatus::ITEM_RECOVERED' is returned.  Otherwise, this instance
//  is re-constructed from scratch, and 'NovRamStatus::ITEM_INITIALIZED'
//  is returned.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Use the base class's initialization method.
//
//  The reason that the 'ReportAuxillaryINfo()' method is not called for
//  a "recovery", is that the log entries, themselves, report that type
//  of failure.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  (verifySelf() == NovRamStatus::ITEM_VALID)
//@ End-Method
//=====================================================================

NovRamStatus::InitializationId
SystemDiagLog::initialize(void)
{
  CALL_TRACE("initialize()");

  NovRamStatus::InitializationId  initStatusId;

  // forward to base class's method...
  initStatusId = initLog_(::MAX_SYS_LOG_ENTRIES);

  switch (initStatusId)
  {
  case NovRamStatus::ITEM_INITIALIZED :		// $[TI1]
    // a full initialization of this diagnostic log is needed...
    new (this) SystemDiagLog();

    // a corruption was detected somewhere in the "base class" portion of this
    // instance's memory, therefore report to this effect...
    NovRamManager::ReportAuxillaryInfo(this, sizeof(DiagCodeLog),
				       initStatusId);
    break;
  case NovRamStatus::ITEM_RECOVERED :		// $[TI3]
    // a corruption was detected somewhere in the "code entries" portion of
    // this instance's memory, therefore report to this effect...
    NovRamManager::ReportAuxillaryInfo(arrEntries_, sizeof(arrEntries_),
				       initStatusId);
    break;
  case NovRamStatus::ITEM_VERIFIED :		// $[TI2]
    // do nothing...
    break;
  default :
    AUX_CLASS_ASSERTION_FAILURE(initStatusId);
    break;
  };

  // at this point, the entire log should be valid...
  SAFE_CLASS_POST_CONDITION((verifySelf() == NovRamStatus::ITEM_VALID),
  			    SystemDiagLog);

  return(initStatusId);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getCurrEntries(rArrEntries)  [const]
//
//@ Interface-Description
//  Initialize the array 'rArrEntries' to contain all of the current
//  entries, in an order from most recent -- at index '0' -- to least
//  recent.  If this log is not full, the entries at the end of the
//  returned array will be cleared to indicate that they are not part
//  of the current entries.  For example, if there are 14 current 
//  entries in this diagnostic log, the entries at, and beyond, index
//  '14' will be cleared, and the entries from '0' thru '13' will not.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses base class to get a copy (in the proper order) of all of the
//  current entries of this log.
//---------------------------------------------------------------------
//@ PreCondition
//  (verifySelf() == NovRamStatus::ITEM_VALID)
//  (rArrEntries.getNumElems() >= ::MAX_SYS_LOG_ENTRIES)
//---------------------------------------------------------------------
//@ PostCondition
//  (verifySelf() == NovRamStatus::ITEM_VALID)
//@ End-Method
//=====================================================================

void
SystemDiagLog::getCurrEntries(AbstractArray(CodeLogEntry)& rArrEntries) const
{
  CALL_TRACE("getCurrEntries(rArrEntries)");
  CLASS_PRE_CONDITION((verifySelf() == NovRamStatus::ITEM_VALID));
  SAFE_CLASS_PRE_CONDITION((rArrEntries.getNumElems() >=
						    ::MAX_SYS_LOG_ENTRIES));

  // get a copy of all of the current entries from the base class...
  copyCurrEntries_(rArrEntries);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//				[static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//  and 'lineNumber' are essential pieces of information.  The
//  'pFileName' and 'pPredicate' strings may be defaulted in the macro
//  to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//  none 
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

void
SystemDiagLog::SoftFault(const SoftFaultID  softFaultID,
			 const Uint32       lineNumber,
			 const char*        pFileName,
			 const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, PERSISTENT_OBJS, SYSTEM_DIAG_LOG,
                          lineNumber, pFileName, pPredicate);
}
