#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
//@ Class: PowerSource - implements the functionality required for 
//                       determining the AC power source.
//---------------------------------------------------------------------
//@ Interface-Description
//  This class implements the functionality to determine the current 
//  AC power quality.
//  This class has an initialization method which must be invoked
//  during the Breath-Delivery subsystem initialization.
//---------------------------------------------------------------------
//@ Rationale
//  This class implements the algorithm for determining the ventialtor 
//  power source.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This object interrogates the SystemBattery object to determine
//  which power source is currently used.  If a changes in power source
//  is detected, Alarm-Analysis Subsystem is notified immediately.
//  If current power source is AC, the AC voltage is monitored
//  every newCycle() to determine its quality.  Any change in the
//  AC power quality is relayed to the Alarms Subsystem.
//---------------------------------------------------------------------
//@ Fault-Handling
//  N/A
//---------------------------------------------------------------------
//@ Restrictions
//  Since this class uses the SystemBattery object, it must be initialized
//  after the SystemBattery object has been initialized.  
//---------------------------------------------------------------------
//@ Invariants
//  None.
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @ (#) $Header::   /840/Baseline/BD-IO-Devices/vcssrc/PowerSource.ccv   10.7   08/17/07 09:30:40   pvcs  
//
//@ Modification-Log
//
//  Revision: 013 By: sah    Date: 08-Sep-1999   DR Number: 5518
//  Project:  ATC
//	Description:
//		Changed compVoltageThresh_ and recoveryOffset values.
//
//  Revision: 012 By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//	Description:
//		Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 011  By:  hct    Date:  24-FEB-1998    DR Number: DCS 2746
//       Project:  Sigma (R8027)
//       Description:
//         Changed RECOVERY_OFFSET constant to variable recoveryOffset in 
//         method initialize() to be dependant on the system line voltage.  
//         Changed lowAcThresh variable in DetermineLowAcThresh() to 90.0 for 
//         120V systems.
//
//  Revision: 010  By:  syw    Date:  28-Oct-1996    DR Number: DCS 2592
//       Project:  Sigma (R8027)
//       Description:
//			Changed RECOVERY_OFFSET to 10.0 VAC.
//
//  Revision: 009  By:  syw    Date:  22-Oct-1996    DR Number: DCS 2576
//       Project:  Sigma (R8027)
//       Description:
//			Added check for powerSource_ == AC_POWER to prevent multiple
//			loggings of alarms.
//
//  Revision: 008  By:  syw    Date:  22-Oct-1996    DR Number: DCS 2472
//       Project:  Sigma (R8027)
//       Description:
//			Modified compRecoveryVoltageThresh_ = compVoltageThresh_ + RECOVERY_OFFSET
//			Modified lowAcThresh values in DetermineLowAcThresh().
//
//  Revision: 007  By:  syw    Date:  08-Sep-1996    DR Number: DCS 2280, 2118
//       Project:  Sigma (R8027)
//       Description:
//       	Initialize compVoltageThresh_ and compRecoveryVoltageThresh_
//			data member.  Defined DetermineLowAcThresh().
//
//  Revision: 006  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
// Revision: 005 By: iv   Date: 05-May-1997   DR Number: DCS 2031
//      Project:  Sigma (840)
//      Description:
//          Added a method initialStatusNotification(), and changed
//          initial conditions for the ac voltage buffer.. 
//
// Revision: 004 By: iv   Date: 23-Apr-1997   DR Number: DCS 1970
//      Project:  Sigma (840)
//      Description:
//          Made low ac voltage persistent below low ac threshold.
//          In checkAcPowerCondition_ added hysteresis for the low ac
//          threshold.
//
// Revision: 003 By: by   Date: 26-Feb-1997   DR Number: DCS 1801
//      Project:  Sigma (840)
//      Description:
//          Modified status update to Alarms Subsystem when on BPS
//          from BDALARM_LOW_AC_POWER to BDALARM_NOT_LOW_AC_POWER.
//
// Revision: 002 By: by   Date: 22-Jan-1997   DR Number: DCS 1573
//      Project:  Sigma (840)
//      Description:
//          Added logic to cancel a insufficient AC power alarm when
//          on battery power.
//
//  Revision: 001  By:  by    Date:  11-Nov-1996    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Initial version.
//
//=====================================================================

#include "SystemBattery.hh"
#include "PowerSource.hh"
#include "PhasedInContextHandle.hh"
#include "NominalLineVoltValue.hh"
#include "MiscSensorRefs.hh"
#include "VentObjectRefs.hh"
#include "MathUtilities.hh"
#include "ExternalBattery.hh"

//@ Usage-Classes

//@ End-Usage

//@Code...

// Power source voltage sensor threshold values definition

// 0.00000F         2.01717F        2.14592F        4.16309F
// No,              EBM only,       AC/DC only,     Both AC/DC and EBM

//@ Constant: THRESHOLD_EBM_VOLTAGE
//  AC/DC not exist, EBM exist
static const Real32 THRESHOLD_EBM_VOLTAGE      = 1.00858F;

//@ Constant: THRESHOLD_ACDC_VOLTAGE
//  AC/DC exists, EBM not exist
static const Real32 THRESHOLD_ACDC_VOLTAGE     = 2.08155F;

//@ Constant: THRESHOLD_ACDC_EBM_VOLTAGE
//  Both AC/DC and EBM exist at the same time
static const Real32 THRESHOLD_ACDC_EBM_VOLTAGE = 3.15451F;

const Real32 PowerSource::VOLTSENSOR_VOLT_MIN  = 0.5F;
const Real32 PowerSource::VOLTSENSOR_VOLT_MAX  = 4.5F;

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: PowerSource()  [Default Constructor]
//
//@ Interface-Description
//  This constructor takes no argument.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=====================================================================

PowerSource::PowerSource( void )
{
    CALL_TRACE("PowerSource::PowerSource(void)");
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: PowerSource()  [Destructor]
//
//@ Interface-Description
//  Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=====================================================================

PowerSource::~PowerSource(void)
{
     CALL_TRACE("PowerSource::~PowerSource(void)");
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newCycle()
//
//@ Interface-Description
//  This method accepts no argument and returns no value.
// 
//  This method read voltage from a ADC sensor, then the voltage is 
//  used to determine what type of power source is using. 
//---------------------------------------------------------------------
//@ Implementation-Description
//  Update the status of the power source (AC or battery). 
//  If external battery is connected, the voltage red from Power Source
//  Voltage sensor should locate in two areas.
//      1) THRESHOLD_EBM_VOLTAGE < voltage < THRESHOLD_ACDC_VOLTAGE
//      2) THRESHOLD_ACDC_EBM_VOLTAGE < voltage
//  If AC/DC is connected, the voltage read from Power Source Voltage
//  sensor should great than THRESHOLD_ACDC_VOLTAGE
//  If a changes in power source is detected, Alarm-Analysis Subsystem
//  is notified immediately. 
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=====================================================================

void
PowerSource::newCycle( void )
{
    CALL_TRACE("PowerSource::newCycle( void )");

    RPowerSourceVoltage.updateValue();

    Real32 sensorVoltage = RPowerSourceVoltage.getValue();

    Source oldPowerSource = powerSource_;

    // if the value read from sensor is stable enough
    if ( fabs(sensorVoltage - lastPowerSourceSensorVolt_) < 0.02 )
    {
        
        externalBatteryConnected_ = ( ( sensorVoltage > THRESHOLD_EBM_VOLTAGE
            && sensorVoltage < THRESHOLD_ACDC_VOLTAGE ) 
            || sensorVoltage > THRESHOLD_ACDC_EBM_VOLTAGE ) ? TRUE : FALSE;

        powerSource_ = 
            (sensorVoltage > THRESHOLD_ACDC_VOLTAGE) ? AC_POWER : BATTERY_POWER;
    }

    lastPowerSourceSensorVolt_ = sensorVoltage;

    // If power source is changed
    if ( oldPowerSource != powerSource_ )
    {
        // TODO E600 revisit for alarm logic when SRS is ready
        if ( powerSource_ == AC_POWER )
        {
            pAlarmCallBack_( BdAlarmId::BDALARM_NOT_LOW_AC_POWER, FALSE );
        }
        else if ( powerSource_ == BATTERY_POWER )
        {
            pAlarmCallBack_( BdAlarmId::BDALARM_LOW_AC_POWER, FALSE );
        }
    }
}
    
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: initialize()
//
//@ Interface-Description
//  This method takes no arguments and has no return value.
//  It sets up the PowerSource object to an initial known state.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Power source is initialized to AC_POWER, and external battery is
//  considered not installed by default.
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=====================================================================

void
PowerSource::initialize( void )
{
    CALL_TRACE("PowerSource::initialize( void )");

    powerSource_ = AC_POWER;

    lastPowerSourceSensorVolt_ = 0.0F;
    externalBatteryConnected_  = FALSE;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: initialStatusNotification
//
//@ Interface-Description
//
//  This method takes no arguments and has no return value. It is
//  invoked after system initialization to notify Alarms of initial
//  Power source status.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//  Data member powerSource_ is used to notify Alarms-Analysis of
//  PowerSource status.
//---------------------------------------------------------------------
//@ PreCondition
//  currentAcPowerQuality_ must be valid.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=====================================================================

void
PowerSource::initialStatusNotification( void )
{
	CALL_TRACE("PowerSource::initialStatusNotification( void )");

    // TODO E600 revisit for alarm logic when SRS is ready
    if ( powerSource_ == AC_POWER )
    {
        pAlarmCallBack_( BdAlarmId::BDALARM_NOT_LOW_AC_POWER, TRUE );
    }
    else if ( powerSource_ == BATTERY_POWER )
    {
        pAlarmCallBack_( BdAlarmId::BDALARM_LOW_AC_POWER, TRUE );
    }
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//  [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//  and 'lineNumber' are essential pieces of information.  The
//  'pFileName' and 'pPredicate' strings may be defaulted in the macro
//  to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=====================================================================

void
PowerSource::SoftFault( const SoftFaultID  softFaultID,
                        const Uint32       lineNumber,
                        const char*        pFileName,
                        const char*        pPredicate )
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, BD_IO_DEVICES, POWERSOURCE,
                          lineNumber, pFileName, pPredicate);
}

//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//=====================================================================
//
//  Private Methods...
//
//=====================================================================
