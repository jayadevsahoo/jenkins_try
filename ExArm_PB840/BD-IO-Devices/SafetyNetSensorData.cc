#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: SafetyNetSensorData - Sensor Safety Net Data
//---------------------------------------------------------------------
//@ Interface-Description
//  Each object of the Sensor class, whose corresponding sensor has
//  a min/max value,  has a pointer to a corresponding
//  SafetyNetSensorData object, which contains the min/max expected
//  reading and other information used during Safety Net checks of
//  the validity of the sensor reading.
//
//  This class contains methods to verify that a reading is within
//  the predefined range which are invoked from the Sensor class,
//  the SafetyNetTestMediator class and the Background subsystem.
//---------------------------------------------------------------------
//@ Rationale
//  The SafetyNetSensorData class contains the min/max values for
//  the sensor, the number of cycles (cycles can be BD cycles, breath
//  cycles or a fixed time cycle depending upon how often the method
//  to check the range is called) that a sensor reading is
//  allowed to remain outside those limits, the Background Event Id
//  and other data used in the BD runtime safety net checks.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Two methods for checking the range are defined: checkRange() and
//  sensorReadingRangeCheck().   checkRange() is invoked from the
//  updateValue method of the sensor class and is used to range check
//  sensors before the sensor class updates the engineering
//  and count values used by the Breath Delivery control system; this
//  method may also update the sensor reading to be used with a default
//	or the last valid sample.
//
//  sensorReadingRangeCheck() is invoked by the Background subsystem
//  to check sensor ranges for sensors that are only checked every 500ms
//  1 sec, 10 sec, etc.  sensorReadingRangeCheck() is also invoked by
//  some of the SafetyNetRangeTest instances through the SafetyNetTestMediator.
//  These checks are only performed once a breath or depend upon some
//  other piece of information and therefore are not checked as part
//  of the Sensor::newCycle() path by the checkRange() method.
//---------------------------------------------------------------------
//@ Fault-Handling
//  N/A
//---------------------------------------------------------------------
//@ Restrictions
//  None
//---------------------------------------------------------------------
//@ Invariants
//  None
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/SafetyNetSensorData.ccv   25.0.4.0   19 Nov 2013 13:54:10   pvcs  $
//
//@ Modification-Log
//
//  Revision: 011 By: erm    Date: 15-May-2003   DR Number: 6055
//  Project:  AVER
//	Description:
//		Modified 5 and 12 Volt Gui Sentry
//
//  Revision: 010 By: srp    Date: 28-May-2002   DR Number: 5901
//  Project:  VCP
//	Description:
//		Incorporated Remediation code review issue(s) -- commentary changes only.
//
//  Revision: 009 By: syw    Date: 16-Mar-2000   DR Number: 5611
//  Project:  NeoMode
//	Description:
//		SV_SWITCHED_MIN_COUNTS and SV_SWITCHED_MAX_COUNTS 
//		were deleted.
//
//  Revision: 008 By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//	Description:
//		Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 007 By: iv     Date: 10-Mar-1998   DR Number: DCS 5041
//  	Project:  Sigma (R8027)
//		Description:
//			Store rawCount in lastRawCountReading_ data member.
//
//  Revision: 006 By: iv     Date: 17-Oct-1997   DR Number: DCS 1883
//  	Project:  Sigma (R8027)
//		Description:
//			Modified default values for air and o2 temperature sensors to 35
//          from 25 deg. C
//
//  Revision: 005 By: syw    Date: 07-Oct-1997   DR Number: DCS 2537
//  	Project:  Sigma (R8027)
//		Description:
//			Modified checkRange() to have no return value.  checkRange() will
//			modify rawCount (if a reading is out of range) instead of returning
//			a flag to indicate whether or not to update the engineering value.
//			Update lastRawCountBeforeFailure_ data member.
//
//  Revision: 004 By: iv     Date: 27-May-1997   DR Number: DCS 2167
//  	Project:  Sigma (R8027)
//		Description:
//          Changed O2_SENSOR_MIN_COUNTS to 1010 and O2_SENSOR_MAX_COUNTS
//          to 4085.
//
//  Revision: 003 By: syw    Date: 19-May-1997   DR Number: DCS 1620
//  	Project:  Sigma (R8027)
//		Description:
//			Check for !STATE_SERVICE && !STATE_SST instead of STATE_ONLINE
//			so that the safetyNetSensorData is active during initialization.
//
//  Revision: 002  By:  iv    Date:  09-May-1997    DR Number: DCS 2040
//       Project:  Sigma (840)
//       Description:
//             Changed ALARM_CABLE_MAX_COUNTS value to 2068 counts.
//
//  Revision: 001  By:  by    Date:  25-Jul-1995    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Initial version.
//
//=====================================================================

#include "SafetyNetSensorData.hh"

//@ Usage-Classes
#include "AdcChannels.hh"
#include "TaskControlAgent.hh"
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Min/Max ADC Count Values for Sensors -- These values represent
//    ADC signals after an ADC offset correction has been applied.
//
//====================================================================

const AdcCounts NO_DEFAULT_VALUE_USED = 0;

const AdcCounts Q_TEMP_MIN_COUNTS = 445;        // $[06116], $[06106], $[06118]
const AdcCounts Q_TEMP_MAX_COUNTS = 3349;       // $[06115], $[06105], $[06117]
const AdcCounts Q_AIR_O2_TEMP_DEFAULT = 1595;   // 35 deg. C
const AdcCounts Q_EXH_TEMP_DEFAULT = 1386;

const AdcCounts SENTRY_12V_MIN_COUNTS = 2861;   // $[06004], $[06162]
const AdcCounts SENTRY_12V_MAX_COUNTS = 3274;   // $[06003], $[06161]
const AdcCounts ALARM_CABLE_MIN_COUNTS = 1433;  // $[06160]
const AdcCounts ALARM_CABLE_MAX_COUNTS = 2068;  // $[06159]
const AdcCounts PSOL_CURR_MIN_COUNTS = 0;       // no min value
const AdcCounts PSOL_CURR_MAX_COUNTS = 3180;    // $[06148], $[06150]
const AdcCounts EXH_MOTOR_CURR_MIN_COUNTS = 0;  // no min value
const AdcCounts EXH_MOTOR_CURR_MAX_COUNTS = 906;// $[06152]
const AdcCounts SENTRY_10V_MIN_COUNTS = 3643;   // $[06156]
const AdcCounts SENTRY_10V_MAX_COUNTS = 3805;   // $[06154]
const AdcCounts EXH_COIL_TEMP_MIN_COUNTS = 53;  // $[06059]
const AdcCounts EXH_COIL_TEMP_MAX_COUNTS = 3317;// $[06058]
const AdcCounts O2_SENSOR_MIN_COUNTS = 1010;    // $[06093]
const AdcCounts O2_SENSOR_MAX_COUNTS = 4085;    // $[06092]
const AdcCounts SENTRY_5V_MIN_COUNTS = 1911;    // $[06164], $[06166]
const AdcCounts SENTRY_5V_MAX_COUNTS = 2188;    // $[06163], $[06165]
const AdcCounts SV_LOOPBACK_MIN_COUNTS = 478;   // $[06015]
const AdcCounts SV_LOOPBACK_MAX_COUNTS = 3833;  // $[06155]
const AdcCounts SENTRY_15V_MIN_COUNTS = 2967;   // $[06006]
const AdcCounts SENTRY_15V_MAX_COUNTS = 3172;   // $[06005]
const AdcCounts PF_CAP_VOLT_MIN_COUNTS = 1822;  // $[06055]
const AdcCounts PF_CAP_VOLT_MAX_COUNTS = 2112;  // $[06054]
const AdcCounts EXH_MAN_TEMP_MIN_COUNTS = 315;  // $[06119]
const AdcCounts EXH_MAN_TEMP_MAX_COUNTS = 3317; // $[06120]
const AdcCounts BPS_BATT_VOLT_MIN_COUNTS = 0;   // no min value
const AdcCounts BPS_BATT_VOLT_MAX_COUNTS = 3133;// $[06136]
const AdcCounts BPS_BATT_CURR_MIN_COUNTS = 195; // $[06139]
const AdcCounts BPS_BATT_CURR_MAX_COUNTS = 3922;// $[06138]
const AdcCounts LV_REF_MIN_COUNTS = 11;         // $[06019]
const AdcCounts LV_REF_MAX_COUNTS = 29;         // $[06019]
const AdcCounts BPS_MODEL_MIN_COUNTS = 195;     // $[06143]
const AdcCounts ATM_PRESS_MIN_COUNTS = 957;     // $[06075] : 6 PSI
const AdcCounts ATM_PRESS_MAX_COUNTS = 2119;    // $[06074] : 18 PSI

const AdcCounts SENTRY_GUI_12V_MIN_COUNTS = 0;   
const AdcCounts SENTRY_GUI_12V_MAX_COUNTS = 4095;
const AdcCounts SENTRY_GUI_5V_MIN_COUNTS = 0;    
const AdcCounts SENTRY_GUI_5V_MAX_COUNTS = 4095;  


//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SafetyNetSensorData()
//
//@ Interface-Description
//  Constructor.  This method takes as input all of the safety net 
//  data (minAdcCounts, maxAdcCounts, defaultAdcCounts, maxCyclesOor,
//  eventId, checkedInSensorNewCycle and useDefaultValue)
//  associated with a sensor and sets the corresponding object
//  data members.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The SafetyNetSensorData attributes are assigned to the corresponding
//  argument.  Also, the numCyclesOorHigh_ and numCyclesOorLow_
//  attributes are initialized to 0 and the firstReadingStored_,
//  backgndCheckFailed_, and backgndEventReported_ attributes are
//  initialized to FALSE.
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

SafetyNetSensorData::SafetyNetSensorData (
  const AdcCounts minAdcCounts,
  const AdcCounts maxAdcCounts,
  const AdcCounts defaultAdcCounts,
  const Int16 maxCyclesOor,
  const BkEventName eventId,
  const Boolean checkedInSensorNewCycle,
  const Boolean useDefaultValue):
  
    minAdcCounts_ (minAdcCounts),               
    maxAdcCounts_ (maxAdcCounts),               
    defaultAdcCounts_ (defaultAdcCounts),       
    maxCyclesOor_ (maxCyclesOor),               
    backgndEventId_ (eventId),                  
    checkedInSensorNewCycle_ (checkedInSensorNewCycle),
    useDefaultValue_ (useDefaultValue) 
{
    // $[TI1]

    CALL_TRACE ("SafetyNetSensorData::SafetyNetSensorData (const AdcCounts minAdcCounts, \
                 const AdcCounts maxAdcCounts, const AdcCounts defaultAdcCounts, \
                 const Int16 maxCyclesOor, const BkEventName eventId)");

    numCyclesOorHigh_  = 0;
    numCyclesOorLow_  = 0;
    firstReadingStored_ = FALSE;
    backgndCheckFailed_ = FALSE;
    backgndEventReported_ = FALSE;
    lastRawCountBeforeFailure_ = minAdcCounts ;
    lastRawCountReading_ = minAdcCounts ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~SafetyNetSensorData ()
//
//@ Interface-Description
//  Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  None
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================
SafetyNetSensorData::~SafetyNetSensorData (void)
{
    CALL_TRACE("SafetyNetSensorData::~SafetyNetSensorData (void)") ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: checkRange()
//
//@ Interface-Description
//  This method accepts a reference to a AdcCounts and returns nothing.
//  This method is invoked to verify that the sampled value for 
//  this Sensor is within the allowable range.
//	If the value is out of range, this function updates the passed by
//	reference value to the default or the last valid measurement.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method first checks if the background check has already
//  failed (background events are not auto-resettable so once a
//  sensor has failed, it is no longer checked and the last
//  reading or a default reading is used until EST is run and
//  the background events are reset).
//
//  If the background check has not already failed and this
//  sensor is expected to be checked by the SensorMediator::newCycle
//  path (other sensors are checked via the Background subsystem
//  or through the SafetyNetTestMediator::newCycle path), the
//  passed rawCount reading is compared to its min/max values.
//  If out of range, the out of range attribute is incremented
//  and compared to the maxCyclesOor_ attribute.  If the value
//  has been out of range for maxCyclesOor_, the backgndCheckFailed_
//  attribute is set TRUE so that the event is reported via
//  the SafetyNetTestMediator::newCycle().
//
//  If the value is out of range, a default value is used if
//  the attribute useDefaultValue_ is TRUE; in this case,
//  the rawCount attribute is updated with the default value.
//
// $[06003] $[06004] $[06005] $[06006] $[06007] $[06008] $[06013]
// $[06014] $[06058] $[06059] $[06105] $[06106] $[06115]
// $[06116] $[06117] $[06118] $[06119] $[06120] $[06148] $[06150] 
// $[06152] $[06154] $[06156] $[06161] $[06162] $[06163]
// $[06164] $[06165] $[06166] 
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

void
SafetyNetSensorData::checkRange (AdcCounts &rawCount) 
{
    CALL_TRACE("SafetyNetSensorData::checkRange (AdcCounts &rawCount)") ;

    if (!backgndCheckFailed_)
    {
    // $[TI1]

        // Only check the sensor reading against its ranges if
        // checkedInSensorNewCycle_ is TRUE; if it is FALSE the
        // sensor is checked somewhere else (eg. by the Background task) 
		// Only check if the system state is online !
        if ( ( checkedInSensorNewCycle_ ) &&
    		TaskControlAgent::GetBdState() != (SigmaState) STATE_SERVICE &&
    		TaskControlAgent::GetBdState() != (SigmaState) STATE_SST &&
    		TaskControlAgent::GetBdState() != (SigmaState) STATE_START)
        {
        // $[TI1.1]

            lastRawCountReading_ = rawCount;
            if (rawCount < minAdcCounts_)
            {
            // $[TI1.1.1]
                numCyclesOorHigh_ = 0;
                ++numCyclesOorLow_;

                // If the useDefaultValue_ flag is TRUE, overwrite the rawCount
                // with the default value and set the return value to TRUE so that
                // the sensor attributes are updated with the default.
                if (useDefaultValue_)
                {
                // $[TI1.1.1.1]
                    rawCount = defaultAdcCounts_;
                }
                else
                {
                // $[TI1.1.1.2]
					rawCount = lastRawCountBeforeFailure_ ;
               	}
            }
            else if (rawCount > maxAdcCounts_)
            {
            // $[TI1.1.2]
                numCyclesOorLow_ = 0;
                ++numCyclesOorHigh_;

                // If the useDefaultValue_ flag is TRUE, overwrite the rawCount
                // with the default value and set the return value to TRUE so that
                // the sensor attributes are updated with the default.
                if (useDefaultValue_)
                {
                // $[TI1.1.2.1]
                    rawCount = defaultAdcCounts_;
                }
                else
                {
                // $[TI1.1.2.2]
					rawCount = lastRawCountBeforeFailure_ ;
				}
            }
            else
            {
            // $[TI1.1.3]
                resetNumCyclesOor();
				lastRawCountBeforeFailure_ = rawCount ;
            }
  
            if ( numCyclesOorLow_ >= maxCyclesOor_  ||
                 numCyclesOorHigh_ >= maxCyclesOor_ )
            {
            // $[TI1.1.4]
                backgndCheckFailed_ = TRUE;
            }
            // $[TI1.1.5]
        }	// implied else $[TI1.2]
    }
    else
    {
	    // $[TI2]
		if (useDefaultValue_)
        {
		    // $[TI2.1]
			rawCount = defaultAdcCounts_;
        }
        else
        {
		    // $[TI2.2]
			rawCount = lastRawCountBeforeFailure_ ;
        }
   	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: sensorReadingRangeCheck()
//
//@ Interface-Description
//  This method takes an AdcCounts sensor reading as an argument
//  and returns nothing.  It is invoked by parts of the system other 
//  than the newCycle() path of the SensorMediator to range check
//  a sensor reading.  Some sensors are only range checked once a
//  breath or once every x number of seconds.
//
//  This method provides access to the SafetyNetSensorData info
//  without tying the check to the BD cycle.
//
//  Assumes this call will be followed by a call to
//  getBackgndEventId() to report any background event detected
//  by this method.
//
//---------------------------------------------------------------------
//@ Implementation-Description
//  If the background check has not previously failed, and the system's state
//  is ONLINE, the passed rawCount is compared to the minAdcCounts_ and
//  maxAdcCounts_ values.  The numCyclesOorHigh_ and numCyclesOorLow_ values
//  are updated based on where the rawCount value falls inside/outside the
//  range.  If either numCyclesOorHigh_ or numCyclesOorLow_ reaches the
//  maxCyclesOor_ limit, the backgndCheckFailed_ attribute is set to TRUE.
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

void
SafetyNetSensorData::sensorReadingRangeCheck (const AdcCounts rawCount) 
{
    CALL_TRACE("SafetyNetSensorData::sensorReadingRangeCheck(const AdcCounts rawCount)");

    if ( ( FALSE == backgndCheckFailed_ ) &&
		 ( STATE_ONLINE == TaskControlAgent::GetBdState() ) )
    {
    // $[TI1]
        lastRawCountReading_ = rawCount;
        if (rawCount < minAdcCounts_)
        {
        // $[TI1.1]
            numCyclesOorHigh_ = 0;
            ++numCyclesOorLow_;
        }
        else if (rawCount > maxAdcCounts_)
        {
        // $[TI1.2]
            numCyclesOorLow_ = 0;
            ++numCyclesOorHigh_;
        }
        else
        {
        // $[TI1.3]
            resetNumCyclesOor();
        }
  
        if (numCyclesOorLow_ >= maxCyclesOor_  || numCyclesOorHigh_ >= maxCyclesOor_)
        {
        // $[TI1.4]
            backgndCheckFailed_ = TRUE;

        }
        // $[TI1.5]
    
    }    // !backgndCheckFailed_
    // $[TI2]
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault()
//
//@ Interface-Description
//  This method takes four arguments: softFaultID, lineNumber, pFileName,
//  and pPredicate and is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds its sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================
 
void
SafetyNetSensorData::SoftFault( const SoftFaultID  softFaultID,
                                const Uint32       lineNumber,
                                const char*        pFileName,
                                const char*        pPredicate )
{
  CALL_TRACE("SoftFault( softFaultID, lineNumber, pFileName, pPredicate)") ;
    
  FaultHandler::SoftFault( softFaultID, BD_IO_DEVICES, SAFETYNETSENSORDATA, lineNumber,
                           pFileName, pPredicate) ;
}

#endif // defined(SIGMA_DEVELOPMENT)

//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//=====================================================================
//
//  Private Methods...
//
//=====================================================================



