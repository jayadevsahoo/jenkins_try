#include "stdafx.h"
// compiled only for GUI CPU
#ifdef SIGMA_GUI_CPU

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================ M O D U L E   D E S C R I P T I O N ====
//@ Filename: GuiPost - GUI CPU Power On Self Test
//---------------------------------------------------------------------
//@ Interface-Description
//  This module contains the Phase 2 POST test definitions and accessor
//  method for the GUI CPU.  The GetTestItems method returns the list
//  of tests appropriate for the GUI.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ Fault Handling
//  Faults during POST cause the test in progress to fail.
//---------------------------------------------------------------------
//@ Restrictions
//  None
//---------------------------------------------------------------------
//@ Invariants
//  None
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/POST/vcssrc/GuiPost.ccv   25.0.4.0   19 Nov 2013 14:17:04   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006    By: Gary Cederquist  Date: 26-JUL-1999  DR Number: 5488
//    Project: 840 ATC
//    Description:
//       Changed requirement id 11097 to TC11000.
//
//  Revision: 005    By: Gary Cederquist  Date: 05-MAY-1999  DR Number: 5373
//    Project: 840 ATC
//    Description:
//       Deconflicted LONG enumerator by defining a "classed" enumerator
//		 for PostType.
//
//  Revision: 004    By: Gary Cederquist  Date: 05-MAY-1999  DR Number: 5203
//    Project: 840 ATC
//    Description:
//       Implemented the PrevPostFailureTests to allow entry into 
//		 service mode after a previous POST failure. The PrevPostFailureTests
//		 skips all POST tests except for the service switch test that
//		 provides entry into service mode. POST bypasses the normal test
//		 sequence if a previous POST failure exists since running the 
//		 tests again will result in another POST failure which would not
//		 allow entry into service mode where a better diagnosis of the 
//		 problem can occur.
//
//  Revision: 003    By: Gary Cederquist  Date: 15-AUG-1997  DR Number: 1875
//    Project: Sigma (R8027)
//    Description:
//       Added testable item annotations.
//
//  Revision: 002    By: Gary Cederquist  Date: 05-AUG-1997  DR Number: 2199
//    Project: Sigma (R8027)
//    Description:
//       Replaced SAAS resonance test with audible feedback test.
//
//  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//=====================================================================

#include "GuiPost.hh"

#include <stdio.h>
#include "Cpu.hh"
#include "PostErrors.hh"
#include "Bto.hh"
#include "NmiRegister.hh"
#include "Ether.hh"
#include "Dram.hh"


// -------------------------------------------------------------------------
// This is the POST executive test structure for the BD CPU.  Each test
// Each test is executed in the order they appear in this structure.
//
//
// Structure Members:
//
// testName:        Text of test in progress, used to send status information
//                  to a debug port
//
// testNumber:      Value displayed on diagnostic LED indicator.
//
// ptestProcedure:  Pointer to test procedure to execute
// -----------------------------------------------------------------------------

// Structure definition for unit testing

const PostTestItem GuiPost::TestItems_[] =
{

// Test                       Test                                Test
// Name                       Number (error code)                 procedure

   {"Bus Timer Test",          POST_ERROR_BTO,                     Bto::Test,              }
  ,{"DRAM Parity Test",        POST_ERROR_DRAM_PARITY,             Dram::ParityTest        }
  ,{"NMI Register Test",       POST_ERROR_NMI_REGISTER,            NmiRegister::Test,      }
  ,{"Ethernet Start",          POST_ERROR_LAN_SELFTEST_START,      Ether::TestStart,       }
  // TODO E600 port
  //,{"SAAS Self Test Start",    POST_ERROR_SAASTEST_START,          Saas::StartSelfTest,    }
  ,{"DRAM Memory Test",        POST_ERROR_DRAM_MEMORY,             Dram::Test    ,         }
  ,{"Ethernet End",            POST_ERROR_LAN_SELFTEST_END,        Ether::TestEnd,         }
  // TODO E600 port
  //,{"SAAS Self Test End",      POST_ERROR_SAASTEST_END,            Saas::EndSelfTest,      }
  //,{"SAAS Audible Feedback",   POST_ERROR_RESONANCE_START,         Saas::ProvideFeedback,   }
  ,{"END OF TESTS",            POST_ERROR_UNUSED,                  NULL,                   }

};

const PostTestItem  GuiPost::PrevPostFailureTests_[] =
{                                                 
   {"DRAM Memory Clear",       POST_ERROR_DRAM_MEMORY,             Dram::Clear                    }
  ,{"END OF TESTS",            POST_ERROR_UNUSED,                  NULL                           }
};

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetTestItems
//
//@ Interface-Description
//  Returns the GUI POST test item list.  Sets the POST type to POST_LONG
//  since the GUI does not have a "short" version of POST.
//  $[TC11000] ...bypass phase 2 POST if in "POST failed" state
//---------------------------------------------------------------------
//@ Implementation-Description
//    See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

const PostTestItem *
GuiPost::GetTestItems(void)
{
    Post::SetPostType_( Post::POST_LONG );
    // $[TI1]
	if ( Post::IsPostFailed() )  // $[TC11000]
	{
	  // $[TI1.1]
	  return GuiPost::PrevPostFailureTests_;
	}
	else
	{
	  // $[TI1.2]
	  return GuiPost::TestItems_;
	}
}



#endif  // SIGMA_GUI_CPU

