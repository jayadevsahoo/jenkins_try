#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//=====================================================================
//@ Filename: CpuDevice.cc - POST accessor methods to CPU registers.
//
//---------------------------------------------------------------------
//@ Interface-Description
//  The CpuDevice class provides accessor methods for I/O registers
//  controlled by the CPU.  These registers include:
//
//  1. I/O Register 1.
//  2. Diagnostic LED Register.
//  3. Watchdog Timer Registers.
//  4. Autovector Mask/Enable Register.
//  5. Autovector Reset Registers.
//  6. NMI Source Register.
//
//  The interface to each of these registers is described in the 
//  method descriptions.  Most methods are defined as inline to 
//  maximize performance.
//---------------------------------------------------------------------
//@ Rationale
//  CpuDevice is a collection of CPU register accessor methods.
//---------------------------------------------------------------------
//@ Implementation-Description
//  POST calls the CpuDevice accessor methods which access memory 
//  mapped I/O registers directly.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard POST fault handling mechanism is used in this class.
//---------------------------------------------------------------------
//@ Restrictions
//    None
//---------------------------------------------------------------------
//@ Invariants
//    None
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/POST-Library/vcssrc/CpuDevice.ccv   25.0.4.0   19 Nov 2013 14:16:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005  By:  gdc    Date:  27-Jan-2011    SCR Number: 6706
//  Project:  XENA2
//  Description:
//		Added IsXena2Config function.
//
//  Revision: 004  By:  gdc    Date:  12-Dec-2008    SCR Number: 6068
//  Project:  840S
//  Description:
//		Removed SUN prototype code.
//
//  Revision: 003   By: Gary Cederquist  Date: 26-Apr-1999  DCS Number: 5370
//    Project: 840 Cost Reduction
//    Description:
//       Corrected revision code conflict.
//
//  Revision: 002   By: Gary Cederquist  Date: 15-AUG-1997  DR Number: 1875
//    Project:   Sigma   (R8027)
//    Description:
//       Added testable item annotations.
//
//  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//=====================================================================

#include "CpuDevice.hh"
//TODO E600 #include "NmiSource.hh"
#include "MemoryMap.hh"
#include "postnv.hh"

//@ Usage-Classes
//@ End-Usage


//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method: GetBoardRevision
//
//@ Interface-Description
//  >Von
//  Returns the board revision level based on information contained 
//  in the NMI source register.  
//
//  INPUT:  none
//  OUTPUT: Uint8  revision level
//
//  >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns the revision bits masked from the NMI source register. The 
//  client should compare this value against the revision enumerator 
//  contained in NmiSource.hh.
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Free-Function
//=====================================================================

Uint8
CpuDevice::GetBoardRevision(void)
{
    // $[TI1]
#ifdef E600_840_TEMP_REMOVED
    return *NMI_REGISTER & NMI_REV_MASK;
#endif
	return 0;
}
//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Method:  IsXena2Config 
//
//@ Interface-Description
//  Returns TRUE if the CPU board is a XENA2 configuration as
//  determined by specific I/O registers.
//  This method can be called from Kernel POST since it uses no
//  initialized variables. Other sub-systems should call the 
//  Operating-System function IsXena2Config() that returns a cached
//  value for the value returned from this method.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns TRUE if the board registers indicate it is a Xena2 board.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Free-Function
//=====================================================================

Boolean
CpuDevice::IsXena2Config(void)
{
#ifdef E600_840_TEMP_REMOVED
    Uint8 revControlBits = *NMI_REGISTER & NMI_REV_MASK;

    if (revControlBits == NMI_REV_MASK)
    {
        // "Pre-Cost Reduction detected!
        return FALSE;
    }
    else
    {
        // "Cost Reduction / Xena / Xena 2 detected! (0x00)
        return ( *REV_CTRL_REG == XENA_II );
    }
#endif
	return false;
}

//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method: ClearBitsIoReg1
//
//@ Interface-Description
//   Clears I/O register 1 bits defined in the input mask.
//---------------------------------------------------------------------
//@ Implementation-Description
//   The contents of I/O register 1 are cached in NOVRAM since the
//   register is write-only.  The bits specified in the mask are first
//   cleared in the cached variable.  This method then writes the contents
//   of the cached variabl to the hardware register.
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

void
CpuDevice::ClearBitsIoReg1(Uint8 mask)
{
#ifdef E600_840_TEMP_REMOVED
    PKernelNovram->ioReg1Cache &= ~mask;
    *IO_REGISTER_1 = PKernelNovram->ioReg1Cache;
    // $[TI1]
#endif
}


//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method: SetBitsIoReg1
//
//@ Interface-Description
//   Sets I/O register 1 bits defined in the input mask.
//---------------------------------------------------------------------
//@ Implementation-Description
//   The contents of I/O register 1 are cached in NOVRAM since the
//   register is write-only.  The bits specified in the mask are first
//   set in the cached variable.  This method then writes the contents
//   of the cached variabl to the hardware register.
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

void
CpuDevice::SetBitsIoReg1(Uint8 mask)
{
#ifdef E600_840_TEMP_REMOVED
    PKernelNovram->ioReg1Cache |= mask;
    *IO_REGISTER_1 = PKernelNovram->ioReg1Cache;
    // $[TI1]
#endif
}

