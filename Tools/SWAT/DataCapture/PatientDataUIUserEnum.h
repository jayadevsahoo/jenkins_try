//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file PatientDataUIUserEnum.h
//----------------------------------------------------------------------------

#ifndef USERENUM_H
#define USERENUM_H

//-------------------------------------
// I N C L U D E S
//-------------------------------------

#include "PatientDataUIEnumDef.h"
#include "PatientDataUIUserInt.h"

//-------------------------------------
// C L A S S  D E C L
//-------------------------------------

class UserEnum:public UserInt
{
    public:
    UserEnum(char *name, Enum::EnumDef *pEnumDef=NULL);

    char *getEnumValue();
    bool isEnum();
    bool setEnum (unsigned int uiValue);

    char* getEnum(int cnt);

    private:
    Enum::EnumDef *m_pEnumDef;
};

#endif
