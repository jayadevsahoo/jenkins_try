#ifndef StackCheck_HH
#define StackCheck_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//====================================================================
 
//====================================================================
//@ Class:  StackCheck - Stack Checker class
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Sys-Init/vcssrc/StackCheck.hhv   25.0.4.0   19 Nov 2013 14:33:56   pvcs  $
//
//@ Modification-Log
//
//   Revision 002   By:   sah      Date: 01/13/99    DR Number: 5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to a non-inlined method.
//
//   Revision 001   By:   gdc      Date: 05/16/95    DR Number:   n/a
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//====================================================================
 

//@ Usage-Classes
#include "Sigma.hh"
#include "Task.hh"
//@ End-Usage
 
class StackCheck
{
  public:
    static void Print(void);
    static void Register( unsigned char   taskId,
                          unsigned char * pUStackTop,
                          Int32           userStackSize,
                          unsigned char * pSStackTop,
                          Int32           supvStackSize);
  
    static Int32 Margin(void);
    static void  Task(void);

    static void     SoftFault(const SoftFaultID softFaultID,
			      const Uint32      lineNumber,
			      const char*       pFileName  = NULL,
			      const char*       pPredicate = NULL);
 

    // struct contains task and stack information for each call 
    // to Initialize
    struct TaskRecord
    {
        unsigned char   taskId;       // VRTX task id
        unsigned char * pUStackTop;
        Uint32          userStackSize;
        unsigned char * pSStackTop;
        Uint32          supvStackSize;
    };

#ifdef SIGMA_UNIT_TEST
    static Int32 GetTaskCount(void);
    static const StackCheck::TaskRecord&  GetTaskData(const Int32 index);
#endif

  private:
    StackCheck();    // declared only
    ~StackCheck();   // declared only

    StackCheck(const StackCheck&);      // declared only
    void operator=(const StackCheck&);  // declared only

    //@ Data-Member:    TaskData_
    //  This array contains a TaskRecord for each task identified 
    //  through the Initialize method.
    static StackCheck::TaskRecord  TaskData_[];

    //@ Data-Member:    TaskCount_
    //  Current number of initialized TaskRecord's.
    static Int32                   TaskCount_;
};


#endif  // StackCheck_HH
