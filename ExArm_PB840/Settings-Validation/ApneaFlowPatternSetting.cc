#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//============== C L A S S   D E S C R I P T I O N ====================
//@ Class:  ApneaFlowPatternSetting - Apnea Flow Pattern Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, discrete setting that indicates the gas-flow pattern
//  for mandatory volume-based apnea breaths.  This class inherits from
//  'BatchDiscreteSetting' and provides the specific information needed for
//  the representation of apnea flow pattern.  This information includes the
//  set of values that this setting can have (see 'FlowPatternValue.hh'), and
//  this batch setting's new-patient value.
//
//  This discrete setting has a dependent setting (apnea inspiratory time),
//  and therefore must process updates to, and violations of, its dependent
//  setting.
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/ApneaFlowPatternSetting.ccv   25.0.4.0   19 Nov 2013 14:27:10   pvcs  $
//
//@ Modification-Log
//
//  Revision: 008   By: gdc    Date: 16-Aug-2010     SCR Number: 6663
//  Project: API/MAT
//  Description:
//  Added support for VentSet API.
// 
//  Revision: 007   By: gdc    Date: 05-Jan-2009    SCR Number: 5773
//  Project:  840S
//  Description:
//      Modified to always return RAMP for new patient value.
//
//  Revision: 006   By: sah    Date: 29-Jun-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	Incorporated initial specifications for NeoMode Project. Also,
//      eliminated use of unneeded 'INVALID_DISCRETE_VALUE' bound state.
//
//  Revision: 005   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'getApplicability()' method
//	*  cleaned up DEVELOPMENT-only 'is{Adjusted,Accepted}Valid()' methods
//
//  Revision: 004   By: dosman    Date:  07-Apr-1998    DCS Number: 
//  Project:  BILEVEL
//  Description:
//	Fixed incorrect default value caused by getNewPatientValue() 
//	returning SettingValue: casted it to DiscreteValue
//
//  Revision: 003   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 002   By: sah    Date:  04-Jun-1997    DCS Number: 2188
//  Project: Sigma (R8027)
//  Description:
//	As part of the fixes for this DCS, I noticed the need for an
//	'AUX_CLASS_ASSERTION_FAILURE()'.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//=====================================================================

#include "ApneaFlowPatternSetting.hh"
#include "FlowPatternValue.hh"
#include "MandTypeValue.hh"
#include "PatientCctTypeValue.hh"

//@ Usage-Classes
#include "SettingsMgr.hh"
//@ End-Usage

//@ Code...

//====================================================================
//
//  Static Data Definitions...
//
//====================================================================

// $[02052] -- this setting's dependent settings...
static const SettingId::SettingIdType  ARR_DEPENDENT_SETTING_IDS_[] =
{
	SettingId::APNEA_EXP_TIME,
	SettingId::APNEA_IE_RATIO,
	SettingId::APNEA_INSP_TIME,

	SettingId::NULL_SETTING_ID
};

static const DiscreteParameterItem ParameterLookup_[] = 
{
	{"SQ", FlowPatternValue::SQUARE_FLOW_PATTERN},
	{"RA", FlowPatternValue::RAMP_FLOW_PATTERN},
	{NULL , NULL}
};


//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  ApneaFlowPatternSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02049] -- setting range
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ApneaFlowPatternSetting::ApneaFlowPatternSetting(void)
: BatchDiscreteSetting(SettingId::APNEA_FLOW_PATTERN,
					   ::ARR_DEPENDENT_SETTING_IDS_,
					   FlowPatternValue::TOTAL_FLOW_PATTERNS)
{
	CALL_TRACE("ApneaFlowPatternSetting()");
	setLookupTable(ParameterLookup_);
}	// $[TI1]


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method:  ~ApneaFlowPatternSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ApneaFlowPatternSetting::~ApneaFlowPatternSetting(void)
{
	CALL_TRACE("~ApneaFlowPatternSetting(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[01096] -- apnea breath settings applicability
//---------------------------------------------------------------------
//@ PreCondition
//  (qualifierId == Notification::ACCEPTED  ||
//   qualifierId == Notification::ADJUSTED)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
	ApneaFlowPatternSetting::getApplicability(
											 const Notification::ChangeQualifier qualifierId
											 ) const
{
	CALL_TRACE("getApplicability(qualifierId)");

	const Setting*  pApneaMandType =
		SettingsMgr::GetSettingPtr(SettingId::APNEA_MAND_TYPE);

	DiscreteValue  apneaMandTypeValue;

	switch ( qualifierId )
	{
		case Notification::ACCEPTED :	  // $[TI1]
			apneaMandTypeValue = pApneaMandType->getAcceptedValue();
			break;
		case Notification::ADJUSTED :	  // $[TI2]
			apneaMandTypeValue = pApneaMandType->getAdjustedValue();
			break;
		default :
			AUX_CLASS_ASSERTION_FAILURE(qualifierId);
			break;
	}

	return((apneaMandTypeValue == MandTypeValue::VCV_MAND_TYPE)
		   ? Applicability::CHANGEABLE		// $[TI3]
		   : Applicability::INAPPLICABLE);	// $[TI4]
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [const, virtual]
//
//@ Interface-Description
//  Return this setting's new-patient value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02050] -- new-patient value...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
	ApneaFlowPatternSetting::getNewPatientValue(void) const
{
	return(DiscreteValue(FlowPatternValue::RAMP_FLOW_PATTERN));
}


#if defined(SIGMA_DEVELOPMENT)

//============== M E T H O D   D E S C R I P T I O N =================
// Method:  isAcceptedValid()  [const, virtual]
//
// Interface-Description
//  Is the accepted value of this setting valid as determined by its
//  dependencies?  Return "True" if the setting is valid, returns "False"
//  if the setting is not valid.
//---------------------------------------------------------------------
// Implementation-Description
//  The setting determines, and returns its validity.
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

Boolean
	ApneaFlowPatternSetting::isAcceptedValid(void) const
{
	CALL_TRACE("isAcceptedValid()");

	Boolean isValid;

	// is the accepted apnea flow pattern value within its range?...
	isValid = DiscreteSetting::isAcceptedValid(); // forward to base class...

	if ( isValid )
	{
		if ( getApplicability(Notification::ACCEPTED) !=
			 Applicability::INAPPLICABLE )
		{
			// is the the accepted apnea flow pattern value consistent with the
			// accepted apnea inspiratory time?...
			const Setting*  pApneaInspTime =
				SettingsMgr::GetSettingPtr(SettingId::APNEA_INSP_TIME);

			isValid = pApneaInspTime->isAcceptedValid();
		}
	}

	return(isValid);
}


//============== M E T H O D   D E S C R I P T I O N =================
// Method:  isAdjustedValid()  [const, virtual]
//
// Interface-Description
//  Is the adjusted value of this setting valid as determined by its
//  dependencies?
//---------------------------------------------------------------------
// Implementation-Description
//  The setting determines, and returns its validity.
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

Boolean
	ApneaFlowPatternSetting::isAdjustedValid(void)
{
	CALL_TRACE("isAdjustedValid()");

	Boolean isValid;

	// is the adjusted apnea flow pattern value within its range?...
	isValid = DiscreteSetting::isAdjustedValid(); // forward to base class...

	if ( isValid )
	{
		if ( getApplicability(Notification::ADJUSTED) !=
			 Applicability::INAPPLICABLE )
		{
			// is the the adjusted apnea flow pattern value consistent with the
			// adjusted apnea inspiratory time?...
			Setting*  pApneaInspTime =
				SettingsMgr::GetSettingPtr(SettingId::APNEA_INSP_TIME);

			isValid = pApneaInspTime->isAdjustedValid();
		}
	}

	return(isValid);
}

#endif  // defined(SIGMA_DEVELOPMENT)


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//			[static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	ApneaFlowPatternSetting::SoftFault(const SoftFaultID  softFaultID,
									   const Uint32       lineNumber,
									   const char*        pFileName,
									   const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

	FaultHandler::SoftFault(softFaultID, ::SETTINGS_VALIDATION,
							APNEA_FLOW_PATTERN_SETTING, lineNumber, pFileName,
							pPredicate);
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  processBoundViolation_()  [virtual]
//
//@ Interface-Description
//  A dependent setting's bound has been violated due to a proposed
//  change to this setting.  This method is responsible for re-mapping
//  the dependent setting bound information, to bound information
//  specific to this setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	ApneaFlowPatternSetting::processBoundViolation_(void)
{
	CALL_TRACE("processBoundViolation_()");

	// get a reference to this setting's bound status...
	BoundStatus&  rBoundStatus = getBoundStatus_();

	SettingBoundId  flowPatternBoundId = ::NULL_SETTING_BOUND_ID;

	switch ( rBoundStatus.getBoundId() )
	{
		case ::APNEA_IE_RATIO_MAX_ID :		  // $[TI1]
			flowPatternBoundId = ::APNEA_FLOW_PATT_BASED_IE_MAX_ID;
			rBoundStatus.setViolationId(SettingId::APNEA_IE_RATIO);
			break;
		case ::APNEA_INSP_TIME_MAX_ID :		  // $[TI2]
			flowPatternBoundId = ::APNEA_FLOW_PATT_BASED_TI_MAX_ID;
			rBoundStatus.setViolationId(SettingId::APNEA_INSP_TIME);
			break;
		case ::APNEA_INSP_TIME_MIN_ID :		  // $[TI3]
			flowPatternBoundId = ::APNEA_FLOW_PATT_BASED_TI_MIN_ID;
			rBoundStatus.setViolationId(SettingId::APNEA_INSP_TIME);
			break;
		default :
			// unexpected bound occurred...
			AUX_CLASS_ASSERTION_FAILURE(rBoundStatus.getBoundId());
			break;
	};

	// set apnea flow pattern's bound status up to indicate that flow
	// pattern has violated its bound...
	rBoundStatus.setBoundStatus(rBoundStatus.getBoundState(), flowPatternBoundId);

	// NOTE:  by returning a violated bound status, the base class's
	//        'calcNewValue()' method will take care of attempting the
	//        "next" apnea flow pattern value...
}
