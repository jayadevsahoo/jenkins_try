// All rights reserved ADENEO EMBEDDED 2010
/*
================================================================================
*             Texas Instruments OMAP(TM) Platform Software
* (c) Copyright Texas Instruments, Incorporated. All Rights Reserved.
*
* Use of this software is controlled by the terms and conditions found
* in the license agreement under which this software has been supplied.
*
================================================================================
*/
//
//  File:  gpttimer.h
//
#ifndef __GPT_TIMER_H_
#define __GPT_TIMER_H_

#ifdef __cplusplus
extern "C" {
#endif

//Timer Mode
#define TIMER_MODE_FREERUN				0
#define TIMER_MODE_PERIODIC				1


// IOCTL to create GPT timer event handle
#define GPT_IOCTL_TIMER_CREATE_EVENT    1
// IOCTL to close GPT timer event handle
#define GPT_IOCTL_TIMER_RELEASE_EVENT   2
// IOCTL to start the GPT timer
#define GPT_IOCTL_TIMER_START           3
// IOCTL to stop the GPT timer
#define GPT_IOCTL_TIMER_STOP            4
// IOCTL to set the delay between
// interrupts for the GPT timer in ms
#define GPT_IOCTL_TIMER_UPDATE_PERIOD   5
	
#define GPT_IOCTL_TIMER_SET_MODE        6
// To obtain GPT main counter value
#define GPT_IOCTL_TIMER_GET_COUNT       7
// To set the event rate in ms
#define GPT_IOCTL_TIMER_SET_EVENT_RATE  8

#ifdef __cplusplus
}
#endif

#endif
