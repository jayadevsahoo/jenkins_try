
#ifndef FaultHandler_HH
#define FaultHandler_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sotred in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class:  FaultHandler - Fault-Handler for the System.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Utilities/vcssrc/FaultHandler.hhv   25.0.4.0   19 Nov 2013 14:35:14   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By:  gdc    Date:  12-Dec-2008    SCR Number: 6068
//  Project:  840S
//  Description:
//		Removed SUN prototype code.
//
//   Revision: 003   By: sah   Date: 04-Mar-1997   DCS Number: 1811
//   Project:  Sigma (R8027)
//   Description:
//	Added calls to log exception and NMI information.  Also, need
//	mechanism for logging faults during POST.
//
//   Revision: 002   By: sah   Date: 06-Feb-1996   DCS Number: 674
//   Project:  Sigma (R8027)
//   Description:
//	Removed 'ifndef' and 'endif' from include line.
//  
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//====================================================================

#include "SigmaTypes.hh"
#include "SoftFaultId.hh"

//@ Usage-Classes
class DiagnosticCode;
//@ End-Usage


class FaultHandler
{
  public:
    static void  Initialize(void);

    static void  StoreAuxErrorCode(const Uint32 auxErrorCode);

    static void  SoftFault(const SoftFaultID softFaultId,
			   const SubSystemID subSystemId,
			   const Uint32      moduleId,
			   const Uint32      lineNumber,
			   const char*       pFileName = NULL,
			   const char*       pBoolTest = NULL);

    static void  SystemSoftFault(const SoftFaultID softFaultId,
				 const SubSystemID subSystemId,
				 const Uint32      moduleId,
				 const Uint32      lineNumber);

    static void  LogExceptionCode(const Uint16 vectorNumber,
				  const void*  instructionAddr,
				  const void*  faultAddr,
				  const Uint16 taskId,
				  const char*  pExceptionName = NULL);

    static void  LogNmiCode(const Uint16 nmiSource,
			    const Uint32 errorCode,
			    const char*  pNmiName = NULL);

    static void  LogDiagnosticCode(const DiagnosticCode& diagCode);

  private:
    FaultHandler(const FaultHandler&);		// not implemented...
    FaultHandler(void);				// not implemented...
    ~FaultHandler(void);			// not implemented...
    void  operator=(const FaultHandler&);	// not implemented...

    //@ Data-Member:  AuxErrorCode_
    // This is a statically-allocated integer that is used for temporary
    // storage of auxillary code values.
    static Uint32  AuxErrorCode_;
};


#endif // FaultHandler_HH 
