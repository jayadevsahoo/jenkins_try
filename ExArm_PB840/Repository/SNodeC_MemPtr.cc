#include "stdafx.h"

//====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: SNodeC_MemPtr - Node for Singly-Linked Lists of Generic Data
//---------------------------------------------------------------------
//@ Interface-Description
//  This is to be used exclusively within the collection classes.
//---------------------------------------------------------------------
//@ Rationale
//  This is a simple container to be used, privately, within the
//  various singly-linked collection classes.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class contains two members:  an object of the type specified
//  by 'MemPtr', and a pointer to the "next" node.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  This can only be constructed by 'SListC_MemPtr'.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Foundation/vcssrc/SLinkedNodeC.C_v   25.0.4.0   19 Nov 2013 14:03:50   pvcs  $
//
//@ Modification-Log
//
//   Revision: 003   By: sah   Date: 08-Feb-1999   DR Number: 5339
//   Project:  ATC
//   Description:
//	Removed 'SIGMA_DEVELOPMENT' flags from around 'SoftFault()'
//	method, to allow for mixed-mode builds.
//
//   Revision: 002   By: sah   Date: 07-Jan-1999   DR Number: 5321
//   Project:  ATC
//   Description:
//	Changed 'SoftFault()' method to an non-inlined method.
//	Also, as part of making all 'SoftFault()' methods non-inlined,
//	I'm also making those 'SoftFault()' methods that are NOT called
//	in PRODUCTION mode, DEVELOPMENT-only.
//
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//=====================================================================

#include "SNodeC_MemPtr.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...


//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pBoolTest)
//	                [static]
//
//@ Interface-Description
//	Report the software fault that occured within the source code
//	of this class.  The fault, indicated by 'softFaultID', occured
//	at 'lineNumber' in 'pFileName'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//	none
//---------------------------------------------------------------------
//@ PostConditions
//	none
//@ End-Method
//=====================================================================

void
SNodeC_MemPtr::SoftFault(const SoftFaultID softFaultID,
			      const Uint32      lineNumber,
			      const char*       pFileName,
			      const char*       pBoolTest)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pBoolTest)");
  FaultHandler::SoftFault(softFaultID, FOUNDATION, S_LINKED_NODE_C,
                          lineNumber, pFileName, pBoolTest);
}
