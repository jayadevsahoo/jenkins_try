#include <windows.h>
#include <nkintr.h>
#include <pkfuncs.h>
#include <oal.h>
#include <oalex.h>
#include <am387x.h>
//#include <bus.h>

float PrcmClockGetSystemClockFrequency();
BOOL PrcmDeviceEnableIClock( UINT devId, BOOL bEnable);
BOOL PrcmDeviceEnableFClock( UINT devId, BOOL bEnable);
BOOL PrcmDeviceSetSourceClocks( UINT devId, UINT count, UINT rgClocks[] );
BOOL PrcmDeviceEnableAutoIdle( UINT clkId, BOOL bEnable);
BOOL PrcmDeviceGetContextState( UINT devId, BOOL bSet);

BOOL PrcmDeviceEnableClocks( UINT devId,  BOOL bEnable);
#if 0
{
	OALMSG(1,(L"PrcmDeviceEnableClocks %d; %d;\r\n", devId, bEnable));
	return TRUE;
}

BOOL OALIoCtlPrcmDeviceGetDeviceManagementTable(
    UINT32 code, VOID  *pInBuffer,     UINT32 inSize, 
    VOID  *pOutBuffer, UINT32 outSize, UINT32 *pOutSize
    )
//	returns a table referencing the device clock management functions
{
    BOOL rc = FALSE;

    UNREFERENCED_PARAMETER(inSize);
    UNREFERENCED_PARAMETER(pInBuffer);
    UNREFERENCED_PARAMETER(code);
    UNREFERENCED_PARAMETER(pOutBuffer);
    UNREFERENCED_PARAMETER(outSize);
    UNREFERENCED_PARAMETER(pOutSize);

	return rc;
}

void PrcmInit(void){;}
BOOL PrcmClockSetParent(UINT clockId, UINT newParentClockId )
{
	OALMSG(1,(L"PrcmClockSetParent %d; %d;\r\n", clockId, newParentClockId));
	return TRUE;
}

void PrcmPostInit(){;}
#endif
VOID OEMPowerOff() {;}

//-----------------------------------------------------------------------------
// originaly implemented in the oal_pm.c 
UINT32 OALPrcmIntrHandler()
{

#if 0
    const UINT clear_mask = PRM_IRQENABLE_VP1_NOSMPSACK_EN |
                            PRM_IRQENABLE_VP2_NOSMPSACK_EN |
                            PRM_IRQENABLE_VC_SAERR_EN |
                            PRM_IRQENABLE_VC_RAERR_EN |
                            PRM_IRQENABLE_VC_TIMEOUTERR_EN |
                            PRM_IRQENABLE_WKUP_EN |
                            PRM_IRQENABLE_TRANSITION_EN |
                            PRM_IRQENABLE_MPU_DPLL_RECAL_EN |
                            PRM_IRQENABLE_CORE_DPLL_RECAL_EN |
                            PRM_IRQENABLE_VP1_OPPCHANGEDONE_EN |
                            PRM_IRQENABLE_VP2_OPPCHANGEDONE_EN |
                            PRM_IRQENABLE_IO_EN ;
#endif 
	UINT sysIntr = SYSINTR_NOP;

    OALMSG(1/*OAL_FUNC*/, (L"+OALPrcmIntrHandler\r\n"));

    // get cause of interrupt
//    sysIntr = PrcmInterruptProcess(clear_mask);

    OALMSG(OAL_FUNC, (L"-OALPrcmIntrHandler\r\n"));
    return sysIntr;
}

DWORD OALMux_UpdateOnDeviceStateChange( UINT devId, UINT oldState, UINT newState, BOOL bPreStateChange )
{
	UNREFERENCED_PARAMETER(devId);
    UNREFERENCED_PARAMETER(oldState);
    UNREFERENCED_PARAMETER(newState);
    UNREFERENCED_PARAMETER(bPreStateChange);
    return (DWORD) -1;
}

//=============================================================================

//-----------------------------------------------------------------------------
// originaly implemented in the oal_pm.c 
VOID OALPowerPostInit() {
    OALMSG(OAL_FUNC, (L"+OALPowerPostInit: MOVE TO POWER MANAGEMENT\r\n"));
	PrcmPostInit();
}

