#pragma once
#include "explorer.h"


// CTabPowerDlg dialog

enum power_page_status_t {NOT_LOGGED_IN, LOGGED_IN, LOGGIN_ERROR, ERROR_GETTING_STATE};

class CTabPowerDlg : public CDialog
{
	DECLARE_DYNAMIC(CTabPowerDlg)

public:
	CTabPowerDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CTabPowerDlg();

// Dialog Data
	enum { IDD = IDD_TAB_POWER_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	CExplorer m_webCtrl;
	virtual BOOL OnInitDialog();
	DECLARE_EVENTSINK_MAP()
	void DocumentCompleteExplorerReset(LPDISPATCH pDisp, VARIANT* URL);
public:
	power_page_status_t m_SwitchState;
	power_page_status_t GetStatus();
	bool LogIn();
	bool ClickLogInError();
	void* ElemFromID( LPWSTR szID, IID nTypeIID );


public:
	void Refresh();
	bool SwitchOnOff(bool bOnOff);
	bool ClickLinkFrame0( );
	bool ClickIfTimeOut( );
	afx_msg void OnBnClickedButtonRefresh();
	afx_msg void OnBnClickedButtonApply();
	int m_nPower;
	CString m_strOption;
	CString m_strOnOff;
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnBnClickedButtonDefault();
	afx_msg void OnBnClickedButtonHitS();


};
