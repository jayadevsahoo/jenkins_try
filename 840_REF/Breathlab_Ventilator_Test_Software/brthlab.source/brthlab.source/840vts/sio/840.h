/*****************************************************************************/
/*   Module Name:    840.h                                           
/*   Purpose:      Header for 840 Ventilator Serial Communications Module                               
/*  
/*   Modification History:
/*
/*   Description of Change			        By			      Date 
/*   ---------------------					-----   		  ------
/*   The Error codes in ErrorMessage		Dwarkanath(CGS)   12 Dec,2000
/*   functions are changed.
/*
/*	 TestExecutive related code is 
/* 	 removed and replaced corresponding		Hamsavally(CGS)   8 Oct,2000
/*   functionality. 
/*     
/*   Added symbol definitions for           Dave Richardson   15 Nov, 2002
/*   AUTO_DETECT and NO_AUTO_DETECT
/*                                                                           
/*                                                                           
/*   $Revision::   1.2      $                                                
/*                                                                          
/*****************************************************************************/

/*****************************************************************************
THIS IS A PROPRIETARY WORK TO WHICH PURITAN-BENNETT CORPORATION CLAIMS
EXCLUSIVE RIGHT.  NO PART OF THIS WORK MAY BE USED, DISCLOSED, REPRODUCED,
STORED IN AN INFORMATION RETIEVAL SYSTEM, OR TRANSMITTED, IN ANY FORM OR BY ANY
MEANS ELECTRONIC, MECHANICAL, PHOTOCOPYING, RECORDING OR OTHERWISE WITHOUT THE
PRIOR WRITTEN PERMISSION OF PURITAN-BENNETT CORPORATION.
 
Copyright (c) 1999-2002, Puritan-Bennett
All rights reserved.
*******************************************************************************/

#ifndef _840_H
#define _840_H

#include "840Msgs.h"
#include "com.h"

// Defines
#ifndef AUTO_DETECT
#define AUTO_DETECT 0
#endif
#ifndef NO_AUTO_DETECT
#define NO_AUTO_DETECT 1
#endif

// types of connection to 840. Either, did not connect to 840, connected with
// and invalid command (meaning the vent was already in remote control mode), or
// connected with a communication granted (meaning vent was waiting to enter
// remote control mode when we made the connection)
enum { CONNECTED_FAILED, CONNECTED_INVALID_MSG, CONNECTED_COMMUNICATION_GRANTED };

// public functions
int Is840Connected(void);
int Establish840(int noAutoDetect);
int Send840(char*);
int Recv840(char*, int, int*, double);
int Terminate840(void);
int CharsIn840Q(void);
int ComBreak840(int breakTime);
int FlushInQ840(void);
int FlushOutQ840(void);
void RegisterFor840DetectCB(ComDetectStatusFuncPtr func);
void Init840ComStruct(void);
int GetLastConnectType840(void);

// private functions
static int TestStrResponseValidFunc(char* str, int len);
static int Parse840Frame(char* str, int len);

// 840 receive macro used to automate redundant code
// only for use within a test state, not allowed in test state
// support functions
#define RECV_840() \
{\
    int comStatus;\
    if (CharsIn840Q() && (CommActive == COM_840_INACTIVE)) {\
        comStatus = ProcessIncomingPacket();\
        if (comStatus != SUCCESS) {\
            gErrorFlag = TRUE;\
			ErrorMessage(COM_840_ERROR);\
            return;\
        }\
    }\
}

// 840 send macro used to automate redundant code
// only for use within a test state, not allowed in test state
// support functions
#define SEND_840(func) \
{\
    if (func != SUCCESS) {\
        gErrorFlag = TRUE;\
		ErrorMessage(COM_840_ERROR);\
        return;\
    }\
}    

#endif //_840_H
