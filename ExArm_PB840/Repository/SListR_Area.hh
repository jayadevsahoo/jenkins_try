
#ifndef SListR_Area_HH
#define SListR_Area_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  SListR_Area - Abstract Singly-Linked List of 'Area' References.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Foundation/vcssrc/AbstractSListR.H_v   25.0.4.0   19 Nov 2013 14:03:44   pvcs  $
//
//@ Modification-Log
//
//   Revision: 003   By: sah   Date: 07-Jan-1999   DR Number: 5321
//   Project:  ATC
//   Description:
//	Changed 'SoftFault()' method to an non-inlined method.
//  
//   Revision: 002   By: sah   Date: 06-Feb-1996   DCS Number: 674
//   Project:  Sigma (R8027)
//   Description:
//	Removed 'ifndef' and 'endif' from around includes.
//  
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//====================================================================

class Area;  // forward declartion...

//@ Usage-Classes
#include "SNodeC_MemPtr.hh"
#include "SListC_MemPtr.hh"
//@ End-Usage


class SListR_Area
{
#if defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)
    // Friend:  operator<<
    // This faciliates debug-only printing of this class's instances.
    friend inline Ostream&  operator<<(Ostream&                  ostr,
				       const SListR_Area& sList);
#endif  // defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)

    //@ Friend:  SIterR_Area
    // 'SIterR_Area' needs access to the protected methods of this
    // class.
    friend class SIterR_Area;

  public:
    virtual ~SListR_Area(void);

    inline Boolean  isEmpty(void) const;
    inline Boolean  isFull (void) const;

    inline Boolean  isAtFirst(void) const;
    inline Boolean  isAtLast (void) const;

    inline Uint32  getNumItems(void) const;
    inline Uint32  getMaxItems(void) const;

    inline SigmaStatus  goFirst(void);
    inline SigmaStatus  goNext (void);

    SigmaStatus  search(Area&                 item,
			const SearchFrom      startFrom = FROM_FIRST,
			const EquivResolution foundWhen = EQUIV_OBJECTS);

    inline void  insert (Area&                newItem,
		         const InsertionPlace insertPlace = AFTER_CURRENT);
    inline void  append (Area& newItem);
    inline void  prepend(Area& newItem);

    inline SigmaStatus  removeCurrent(void);
    SigmaStatus  removeItem(Area&                 item,
			    const EquivResolution foundWhen = EQUIV_OBJECTS);
    inline void  clearList(void);

    inline const Area&  currentItem(void) const;
    inline Area&        currentItem(void);

    inline void  operator=(const SListR_Area& sList);

    // test for equivalence, based on 'equivWhen'...
    Boolean  isEquivTo(const SListR_Area& sList,
                       const EquivResolution     equivWhen) const;

    inline Boolean  operator==(const SListR_Area& sList) const;
    inline Boolean  operator!=(const SListR_Area& sList) const;

    static void  SoftFault(const SoftFaultID softFaultID, 
			   const Uint32      lineNumber, 
			   const char*       pFileName = NULL, 
			   const char*       pBoolTest = NULL); 

  protected:
    inline SListR_Area(SListC_MemPtr* pList);

    const SNodeC_MemPtr*  findNode_(
				      Area&                       item,
				      const SNodeC_MemPtr* pStartNode,
				      const EquivResolution       foundWhen
					  ) const;

    inline Boolean  isLegalNode_(const SNodeC_MemPtr* pNode) const;

    inline const SNodeC_MemPtr*  getFirst_  (void) const;
    inline const SNodeC_MemPtr*  getCurrent_(void) const;

    inline SNodeC_MemPtr*  getFirst_  (void);
    inline SNodeC_MemPtr*  getCurrent_(void);

    inline void  setCurrent_(SNodeC_MemPtr* pNewCurr);

#if defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)
    virtual Ostream&  print_(Ostream& ostr) const = 0;
#endif  // defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)

  private:
    SListR_Area(const SListR_Area&);	// not implemented...
    SListR_Area(void); 			// not implemented...

    //@ Data-Member:  rPtrList_
    // A reference to the abstract list of pointers.
    SListC_MemPtr&  rPtrList_;
};


// Inlined methods...
#include "SListR_Area.in"


#endif  // SListR_Area_HH
