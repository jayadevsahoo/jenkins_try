 
#ifndef MandTypeSetting_HH
#define MandTypeSetting_HH

//====================================================================
//
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
//@ Class:  MandTypeSetting - Mandatory Type Setting.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/MandTypeSetting.hhv   25.0.4.0   19 Nov 2013 14:27:30   pvcs  $
//
//@ Modification-Log
//
//  Revision: 007   By: gdc    Date: 31-Dec-2008    SCR Number: 6177
//  Project:  840S
//  Description:
//      Restructured class to correctly revert to the user-selected value 
// 		when an "observed" setting has forced a change to the value of
// 		this setting. Changed getNewPatient method to provide default value 
// 		for	this setting based on the settings it depends upon. Implemented
// 		derived method setAdjustedValue to save the user-selected value for
// 		so it can be used by valueUpdate in handling changes to higher level
// 		settings such as vent-type.
//
//  Revision: 006  By: gdc    Date: 20-Feb-2005    SCR Number: 6144
//  Project:  NIV1
//  Description:
//      Changes to support NIV.
//
//  Revision: 005  By: sah     Date:  17-Jul-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//	VTPC project-related changes:
//      *  added 'isEnabledValue()' method to control access to 'VC+' value
//      *  added new members, 'isNonBilevelStored_' and 'isLastMandValueVtpc_',
//         for supporting the automatic changing of this setting, based on
//         changes to mode (see 'valueUpdate()')
//      *  added override of 'calcNewValue()' to clear new
//         'isLastMandValueVtpc_' flag, when the user changes the setting
//         directly
//
//  Revision: 004   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added special processing for BiLevel (hard-coded to 'PC'),
//	   thereby removing this special processing from non-related
//	   classes
//	*  added new 'getApplicability()' method
//	*  removed unneeded 'acceptTransition()' method (now handled via
//	   the observer mechanism -- see 'valueUpdate()')
//	*  now an observer of mode setting, therefore 'valueUpdate()',
//	   'doRetainAttachment()' and 'settingObserverInit()' overridden
//	   from observer base class
//
//  Revision: 003   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 002   By: dosman Date:  18-Feb-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial BiLevel version.
//	added acceptTransition() (see Method Description for details)
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Initial version.
//  
//====================================================================

//@ Usage-Classes
#include "BatchDiscreteSetting.hh"
#include "SettingObserver.hh"
//@ End-Usage


class MandTypeSetting : public BatchDiscreteSetting, public SettingObserver
{
  public:
    MandTypeSetting(void);
    virtual ~MandTypeSetting(void);

	// Setting virtual
	virtual void  setAdjustedValue(const SettingValue& newAdjustedValue);

    virtual Applicability::Id  getApplicability(
			    const Notification::ChangeQualifier qualifierId
					       ) const;

    virtual Boolean  isEnabledValue(const DiscreteValue value) const;

    virtual const BoundStatus&  calcNewValue(const Int16 knobDelta);

    virtual void  calcTransition(const DiscreteValue newValue,
				 const DiscreteValue currValue);

    virtual SettingValue  getNewPatientValue(void) const;

    virtual void  resetState(void);

    // SettingObserver methods...
    virtual void  valueUpdate(const Notification::ChangeQualifier qualifierId,
			      const SettingSubject*               pSubject);
    virtual Boolean  doRetainAttachment(const SettingSubject* pSubject) const;

    virtual void  settingObserverInit(void);

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL, 
			   const char*       pPredicate = NULL);

  private:
    MandTypeSetting(const MandTypeSetting&);	// not implemented...
    void  operator=(const MandTypeSetting&);	// not implemented...

	//@ Data-Member:  operatorSelectedValue_
	// Stores the mandatory type that was selected by the operator.
	DiscreteValue operatorSelectedValue_;

	//@ Data-Member:  isChangeForced_
	// TRUE when a mode or vent-type change forces a change to the mandatory type
	Boolean isChangeForced_;
};


#endif // MandTypeSetting_HH 
