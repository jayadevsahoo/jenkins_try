#ifndef LowerScreen_HH
#define LowerScreen_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: LowerScreen - Contains and displays the
// five areas that constitute the Lower screen during normal ventilation (not
// Service mode): Main Settings Area, Lower Subscreen Area, Lower Screen Select
// Area, Message Area and Prompt Area.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/LowerScreen.hhv   25.0.4.0   19 Nov 2013 14:08:04   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004   By: gdc    Date:  06-Nov-2008    SCR Number: 6435
//  Project: 840S
//  Description:
// 	Modified to support changing IBW during ventilation. 
// 	
//  Revision: 003   By: hhd    Date:  16-Nov-1999    DCS Number: 5327
//  Project: Neo-Mode
//  Description:
// 	Modified as part of a general cleanup effort for Neo-Mode.
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "ButtonTarget.hh"
#include "Container.hh"
#include "GuiEventTarget.hh"

//@ Usage-Classes
#include "ApneaCorrectionStatus.hh"
#include "LowerScreenSelectArea.hh"
#include "LowerSubScreenArea.hh"
#include "MainSettingsArea.hh"
#include "MessageArea.hh"
#include "PromptArea.hh"
#include "TextButton.hh"
#include "GuiAppClassIds.hh"
//@ End-Usage

class LowerScreen : public Container, public ButtonTarget, public GuiEventTarget
{
public:
	LowerScreen();
	~LowerScreen(void);

	//@ Type: ApneaPatientSetupStatus
	// Apnea Setup is launched automatically at the end of both Same and
	// New Patient setup.  This status (valid values are APNEA_NO_SETUP,
	// APNEA_SAME_PATIENT and APNEA_NEW_PATIENT) tells the Apnea Setup
	// subscreen after which setup sequence, if any, it was auto-launched.
	// See apneaPatientSetupStatus() for more information.
	enum ApneaPatientSetupStatus
	{
		APNEA_NO_SETUP,
		APNEA_SAME_PATIENT,
		APNEA_NEW_PATIENT
	};

	// Event handlers for the Same/New Patient Setup sequence
	void startNewPatientSetup(void);
	void samePatientSetupAccepted(void);
	void newPatientSetupScreen3Accepted(void);
	void apneaSetupDeactivated(void);
	void reviewApneaSetup(ApneaCorrectionStatus apneaCorrectionStatus);
	void apneaSetupReviewed(void);
	void activityTimeout(void);

	inline void setPatientSetupMode(Boolean);
	inline Boolean isInPatientSetupMode(void) const;
	inline ApneaPatientSetupStatus apneaPatientSetupStatus(void) const;
	inline ApneaCorrectionStatus apneaReviewStatus(void) const;

	// Inline methods for returning pointers to various areas of the Lower
	// Screen
	inline MainSettingsArea      *getMainSettingsArea(void);
	inline LowerSubScreenArea    *getLowerSubScreenArea(void);
	inline LowerScreenSelectArea *getLowerScreenSelectArea(void);
	inline PromptArea            *getPromptArea(void);
	inline MessageArea           *getMessageArea(void);

	inline void  showSubScreenBorders(void);
	inline void  hideSubScreenBorders(void);

	// ButtonTarget virtual method
	virtual void buttonDownHappened(Button *pButton,
												Boolean byOperatorAction);

	// GuiEventTarget virtual method
	virtual void guiEventHappened(GuiApp::GuiAppEvent eventId);
	static inline void Initialize();
	static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL, 
			  const char*       pPredicate = NULL);

	// Reference to LowerScreen, used by many to access public methods of this class
  	static LowerScreen&  RLowerScreen;

protected:

private:
	// these methods are purposely declared, but not implemented...
	LowerScreen(const LowerScreen&);		// not implemented...
	void   operator=(const LowerScreen&);

	//@ Data-Member: promptArea_
	// The Prompt Area
	PromptArea promptArea_;

	//@ Data-Member: messageArea_
	// The Message Area
	MessageArea messageArea_;

	//@ Data-Member: mainSettingsArea_
	// The MainSettings Area
	MainSettingsArea mainSettingsArea_;

	Box  subScreenUpperBorder_;
	Box  subScreenLeftBorder_;
	Box  subScreenRightBorder_;

	//@ Data-Member: lowerSubScreenArea_
	// The Lower Sub-screen Area
	LowerSubScreenArea lowerSubScreenArea_;

	//@ Data-Member: lowerScreenSelectArea_
	// The Lower Screen Select Area
	LowerScreenSelectArea lowerScreenSelectArea_;

	//@ Data-Member: restartButton_
	// Used for going back to the Ventilator Start screen while the
	// operator is setting up a new patient.
	TextButton restartButton_;

	//@ Data-Member: isInPatientSetupMode_
	// Tells us if we are currently involved in setting up the same or
	// a new patient.  This flag is used by Vent Setup and Apnea Setup
	// to tell if they must do things slightly differently because
	// a new/same patient is being setup.
	Boolean isInPatientSetupMode_;

	//@ Data-Member: apneaPatientSetupStatus_
	// At the end of Same/New Patient Setup we must display the Apnea Setup
	// screen.  The screen must display special prompts at this point.
	// We use this flag to tell Apnea Setup which prompts to display.  After
	// Apnea Setup is deactivated this value is set to APNEA_NO_SETUP to
	// indicate no special Patient Setup review prompts from now on.
	ApneaPatientSetupStatus apneaPatientSetupStatus_;

	//@ Data-Member: apneaReviewStatus_
	// Tells us if we are currently involved in auto-launching the
	// Apnea Setup subscreen due to auto-adjustment of apnea
	// settings by Settings and tells us which settings were adjusted.
	// This flag is used by Apnea Setup to decide which prompts to display.
	// A value of NONE_CORRECTED indicates that no review is needed.
	ApneaCorrectionStatus apneaReviewStatus_;

};

// Inlined methods
#include "LowerScreen.in"

#endif // LowerScreen_HH 
