#ifndef Box_HH
#define Box_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: Box - This class is used to define and display a box.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Foundations/vcssrc/Box.hhv   25.0.4.0   19 Nov 2013 14:11:40   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006   By: gdc    Date:  21-May-2007    SCR Number: 6330
//  Project: Trend
//  Description:
//	Removed SUN prototype code.
// 
//  Revision: 005   By: sah    Date: 11-Jan-1999   DR Number: 5321
//  Project:  Sigma (R8027)
//  Description:
//      Changed 'SoftFault()' method to non-inline method.
//
//  Revision: 004  By: gdc      Date: 15-Feb-1996  DR Number: 674
//    Project:  Sigma (R8027)
//    Description:
//	Removed ifndef / endif around include directives
//
//  Revision: 003  By: mpm      Date: 02-Aug-1995  DCR Number: 486
//    Project:  Sigma (R8027)
//    Description:
//      Made destructor virtual as per coding standard requirements.
//
//  Revision: 002  By: yyy      Date: 16-May-1995  DCR Number: 1949
//    Project:  Sigma (R8027)
//    Description:
//      Added default constructor for Box class which takes no argument.
//
//  Revision: 001  By: yyy      Date: 01-Jan-1995  DR Number: NONE
//    Project:  Sigma (R8027)
//    Description:
//	Integration baseline.
//====================================================================

#include "Sigma.hh"
#include "GuiFoundation.hh"
#include "Drawable.hh"

//@ Usage-Classes
#include "Colors.hh"
class Area;
//@ End-Usage

class Box : public Drawable
{
public:
	Box(void);
	Box(Int16 xOrg, Int16 yOrg, Int16 width, Int16 height, Int16 penWidth = 1);
	virtual ~Box(void);

	void setPenWidth(Int16 penWidth);
	void setFillColor(Colors::ColorS color);

	virtual void draw(const Area& clipArea);

	static void SoftFault(const SoftFaultID  softFaultID,
						  const Uint32       lineNumber,
						  const char*        pFileName  = NULL,
						  const char*        pPredicate = NULL);

protected:

private:
	Box(const Box&);				// declared, not implemented.
	void operator=(const Box&);		// declared, not implemented.

	//@ Data-Member: penWidth_
	// Stores the border width of the box.
	Int16 penWidth_;

	//@ Data-Member: fillColor_
	// Stores the interior color of the box.
	Colors::ColorS fillColor_;
};


#endif  // Box_HH
