
#ifndef Precision_HH
#define Precision_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
// Filename:  Precision - The Precision Enumerator.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Utilities/vcssrc/Precision.hhv   25.0.4.0   19 Nov 2013 14:35:14   pvcs  $
//
//@ Modifications
//  
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//=====================================================================

//@ Usage-Class
//@ End-Usage

//@ Type:  Precision
// The level of accuracy for a floating-point number.
enum Precision
{
  THOUSANDTHS = -3,	// accurate to 10^(-3)...
  HUNDREDTHS  = -2,	// accurate to 10^(-2)...
  TENTHS      = -1,	// accurate to 10^(-1)...
  ONES        =  0,	// accurate to 10^0...
  TENS        =  1,	// accurate to 10^1...
  HUNDREDS    =  2,	// accurate to 10^2...
  THOUSANDS   =  3	// accurate to 10^3...
};


#endif  // Precision_HH
