#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1994, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: AlarmRender - Low-level word wrapping routines for alarm displays.
//---------------------------------------------------------------------
//@ Interface-Description
// A class wrapper for static methods and string buffers that generically
// render alarm messages on multiple lines, with word wrapping, if necessary.
// Auto-positioning of the messages is also provided so that the messages are
// justified correctly. These methods fetch the alarm message strings from the
// Alarms subsystem and format the text using up to three string buffers.  Each
// buffer represents one line of the message.  Each line of the message is
// subject to a maximum width limit, which cannot be exceeded.  Word wrapping
// conversion is chosen by calling WithWordWrap().  If word wrapping is not
// desired then WithoutWordWrap() (faster) should be used.
//
// This class also provides a method (GetSymbolHelpMessage()) which returns
// a symbol translation help message for an alarm root cause symbol.
// This is used to permit the user to touch a displayed root cause symbol and
// see the translated meaning on the lower screen Message Area.
//
// The Initialize() method must be called before any method is used.
//---------------------------------------------------------------------
//@ Rationale
// This code is used to format both normal alarm analysis messages (AlarmMsg
// objects) and alarm log analysis messages (in the AlarmLogSubScreen object).
// It makes sense to put the rendering code in a single, common place rather
// than having clones in two different places.  Also, this code can be cleanly
// lifted out and used in say, a PC-based alarm rendering validation tool.
//---------------------------------------------------------------------
//@ Implementation-Description
// The rendering routines are given the message id's which make up the various
// "phrases" of an alarm analysis message.  Using these id's, the routines
// fetch the raw phrase strings from the Alarms subsystem (these are C strings,
// not Cheap Text).  The rendering process involves splitting the raw phrase
// strings into individual words, then packing the words onto a line until
// there are no more words or until the addition of a word would cause the line
// to exceed its maximum width.
//
// The caller has two rendering options.  First, the caller would typically use
// the simpler WithoutWordWrap() routine to render up to three phrases, given
// the restriction that each phrase must fit on its own line.  If any of these
// phrases are too long, then this routine simply returns a failure status.
// Second, if the WithoutWordWrap() routine finds that a single phrase is too
// long, or there are more than three phrases, then the caller must use the
// more complicated WithWordWrap() routine to attempt to fit the phrases on up
// to three lines.
//
// If word wrapping is allowed, then the leftover word, if any, at the end of a
// line is placed on the next available line (in this case, failure occurs only
// if you run out of available lines).  Checking to see whether or not a series
// of words fits on a line is a trial and error procedure which repeatedly
// converts the words in the phrase to Cheap Text and checks the rendered size
// of the text to see if it still fits within the given line.
//
// A complication to the overall process involves rendering the dependent
// augmentation symbols of an alarm analysis message in bold font.  The other
// portions of the analysis message are rendered in thin (non-bold) font.
//
// The routine that returns symbol translation messages for the root cause
// symbols performs a look-up to see whether or not the root cause message id
// refers to a symbol or a normal (non-symbol) phrase.
//---------------------------------------------------------------------
//@ Fault-Handling
// Usual use of asserts to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
// It is assumed that the WithWordWrap() routine will always succeed in packing
// all of the words onto the given number of lines without exceeding the
// maximum width limit.  If the WithWordWrap() routine is unsuccessful, it
// detects this at runtime with an assertion.  Therefore, it is important to
// thoroughly test all possible alarm messages for a given language to
// guarantee that there will be no runtime failures.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/AlarmRender.ccv   25.0.4.0   19 Nov 2013 14:07:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 017  By:  gdc 	Date:  15-Feb-2005   DCS Number: 6144
//  Project:  NIV1
//  Description:
//      DCS 6144 - NIV1 added low circuit pressure alarm 
//
//  Revision: 016  By:  Boris Kuznetsov	Date:  05-May-2003   DCS Number: 6036
//  Project:  Russian
//  Description:
//  	Added seeting size font 8 instead of 6 for Russian font 
//
//  Revision: 015  By:  heatherw	Date:  07-Mar-2002   DCS Number: 5790
//  Project:  VTPC
//  Description:
//	Added three new cases for Vti Alarm.
//
//  Revision: 014  By:  btray	   Date:  03-Aug-1999    DCS Number: 5492
//  Project:  ATC
//  Description:
//	Increased Y-position by one pixel whenever 3 lines are being rendered
//	for the alarm analysis message. 
//
//  Revision: 013  By:  hct	   Date:  24-Feb-1999    DCS Number: 5322
//  Project:  ATC
//  Description:
//		Added new switch option for getting COMPENSATION_LIMIT_MSG .
//
//  Revision: 012  By:  hhd	   Date:  03-Feb-1999    DCS Number: 5322
//  Project:  ATC
//  Description:
//		Added new switch option for getting INSPIRED_VOLUME_LIMIT_MSG .
//
//  Revision: 011  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision:  010    By:  sah	 Date:  15-Jan-1998      DR Number:  5004
//  Project:    Sigma (R8027)
//  Description:
//		Incorporated new 'AlarmStrs' class into interface with the alarm
//      strings.  Also, modified 'GetTok_()' to not tokenize a subscript
//      field.  For example, "V{S:TE MAND}" was being tokenized into two
//      separate tokens because of the space character.
//
//  Revision:  009    By:  hhd	 Date:  15-10-1997      DR Number: 2555, 2560 
//  Project:    Sigma (R8027)
//  Description:
//		10/15/97  Fixed alarm message format problems, per above DCS's. 
//		10-29-97  Looping thru to skip all spaces instead of just one, per #2560.
//
//  Revision:  008    By:  hhd	 Date:  07-10-1997      DR Number: 2538 
//  Project:    Sigma (R8027)
//  Description:
//		Fixed alarm messages being shifted problem. 
//
//  Revision:  007    By:  hhd	 Date:  03-10-1997      DR Number: 2521 
//  Project:    Sigma (R8027)
//  Description:
//		Fixed problem with English text being displayed in smaller font unnecessarily, 
//     	causing vent to reset (direct result of last mod).
//
//  Revision:  006    By:  hhd	 Date:  30-Sep-1997      DR Number: 2521 
//  Project:    Sigma (R8027)
//  Description:
//		Enhanced code to add vertical formatting for Alarm messages in either the Alarm log
//		or the Alarm area.
//
//  Revision:  005    By:  hhd	 Date:  10-Sep-1997      DR Number: 2379
//  Project:    Sigma (R8027)
//  Description:
//		Modified to fix problems with prior code change and to do some code clean up. 
//
//  Revision:  004    By:  sah   Date:  20-Aug-1997      DR Number: 2379
//  Project:    Sigma (R8027)
//  Description:
//		Performance improvements to the alarm rendering.
//
//  Revision:  003  By: hhd   Date:  30-July-997      DR Number: DCS 2265
//    Project:    Sigma (R8027)
//    Description:
//		   Modified With/WithoutWordWrap and add several new auxiliary methods to
//			accomodate foreign language version of the Alarm messages.
// 
//  Revision: 002  By: yyy      Date: 15-May-1997  DR Number: 2110
//    Project:  Sigma (R8027)
//    Description:
//      Added SRS 01140 mapping.
//
//  Revision: 001  By:  wks    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "AlarmRender.hh"

//
// Sigma includes.
//
#include "Sigma.hh"
#include <string.h>

//@ Usage-Classes
#include "AlarmStrs.hh"
#include "MiscStrs.hh"
#include "TextField.hh"
#include "LanguageValue.hh"
//@ End-Usage

//@ Code...

//
// Static member initializations.
//
const wchar_t  AlarmRender::DELIMITER_CHAR_ = L'$';
TextField &AlarmRender::RTextField_ = *((TextField *)TextFieldMemory_);

wchar_t AlarmRender::CheapText_[] = { L'\0' };  
wchar_t AlarmRender::CurrentLine_[] = { L'\0' };  
wchar_t AlarmRender::Buf_[] = { L'\0' };  
wchar_t * AlarmRender::PBuf_ = NULL;	  	
Int	 AlarmRender::TextFieldMemory_[] = { 0 };

static const Int MAX_BASE_MSGS_ = 2;
static const Int MAX_ANALYSIS_MSGS_ = 3;

static const wchar_t CJK_GLUE_CHAR = L'+';  
static const wchar_t HYPHEN = L'-';  
static Int32 isHyphenated = FALSE;

// The Y baseline for positioning cheap text strings when finding the dimensions
// of an alarm message.
static const Int32 TEXT_BASELINE = 500;
static Boolean IsCJK_ = FALSE;
static wchar_t FontStyleChar_;   

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Initialize	[public, static]
//
//@ Interface-Description
// Initializes the alarm rendering code.  Must be the first method called
// in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
// Construct the TextField object we use for calculating the dimensions of
// cheap text strings.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
AlarmRender::Initialize(void)
{
	CALL_TRACE("AlarmRender::Initialize(void)");

	new (&RTextField_) TextField;						// $[TI1]

	Uint16 language = GuiApp::GetLanguage();
	if ( (language == LanguageValue::CHINESE)
	    || (language == LanguageValue::JAPANESE) )
	{
		IsCJK_ = TRUE;
		FontStyleChar_ = L'J';
	}
	else
	{
		IsCJK_ = FALSE;
		FontStyleChar_ = L'N';
	}
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: WithoutWordWrap	[public, static]
//
//@ Interface-Description
// Converts up to three raw alarm analysis phrases to Cheap Text format
// and stores the cheap text in up to three character buffers.  The cheap
// text is automatically positioned to fit within a specified area with
// the text being centered vertically and formatted according to specifications
// horizontally. Parameters are as follows:
// >Von
//	pString1		Pointer to the first buffer that will contain unwrapped
//					cheap text.
//	pString2		Pointer to the second buffer that will contain unwrapped
//					cheap text.
//	pString3		Pointer to the third buffer that will contain unwrapped
//					cheap text.
//	pPhraseIds		A null-terminated array of alarm phrase id's.  Must hold
//					between 1 and 3 phrases.
//	fontSize		The point size of the rendered text.
//	areaWidth		The width of the area in which the text should be rendered.
//	areaHeight		The height of the area in which the text should be rendered.
//	aligment		How the text should be aligned horizontally within the area,
//					LEFT, RIGHT or CENTER.
//	margin			When the alignment is LEFT or RIGHT, this specifies the
//					margin in pixels to leave at the edge of the area when
//					left or right justifying text.  When aligment is CENTER
//					it should be set to twice the margin needed at each side
// 					of the text, e.g. when centering text you may demand that
//					at least 2 pixels be unused at either side of the text so
//					set margin to be 4.
// >Voff
// The return value is TRUE if the phrases were rendered successfully, or
// FALSE if the phrases did not fit within the required width.
//---------------------------------------------------------------------
//@ Implementation-Description
// For each phrase id, this routine fetches the corresponding alarm phrase
// string (raw C string) from the alarm string database, converts the phrase
// string into a Cheap Text string of the proper point size, hands the Cheap
// Text to a TextField object, then gets the dimensions of the text.  If the
// results exceed the area width, return immediately with a FALSE status.
//
// During the conversion to Cheap Text, note that the routine checks to
// see whether the phrase is to rendered in normal font (the default) or
// thin font.
//
// If all phrases fit within the area width we go ahead and format the
// cheap text strings according to the desired alignment and center them
// vertically within the area.  TRUE is returned.
//---------------------------------------------------------------------
//@ PreCondition
// The 'pString1' and 'pPhraseIds' arguments must not be NULL.  The number of
// non-null phrase id's in the 'pPhraseIds' array must be either 1, 2, or 3 and
// the number of non-NULL char * arguments must match the number of phrase
// id's.  The length of a raw phrase string or the converted Cheap Text string
// must not overflow any string buffers (validated with assertions).
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
 
Boolean
AlarmRender::WithoutWordWrap(
					wchar_t* pString1, wchar_t* pString2, wchar_t* pString3,
					const int* pPhraseIds, TextFont::Size fontSize,
					Int32 areaWidth, Int32 areaHeight, Alignment alignment,
					Int32 margin)
{
    CALL_TRACE("AlarmRender::WithoutWordWrap("
					"char* pString1, char* pString2, char* pString3, "
					"const int* pPhraseIds, TextFont::Size fontSize, "
					"Int32 areaWidth, Int32 areaHeight, Alignment alignment, "
					"Int32 margin)");

	CLASS_PRE_CONDITION(pPhraseIds != NULL);
	CLASS_PRE_CONDITION(pString1 != NULL);
 
	wchar_t  alarmMsg0[TextField::MAX_STRING_LENGTH];
	wchar_t  alarmMsg1[TextField::MAX_STRING_LENGTH];
	wchar_t  alarmMsg2[TextField::MAX_STRING_LENGTH];

	AlarmTextInfo_   alarmTextInfoArray[3];

	alarmTextInfoArray[0].pText = alarmMsg0;
	alarmTextInfoArray[1].pText = alarmMsg1;
	alarmTextInfoArray[2].pText = alarmMsg2;

	// using a pointer to an individual array item, as opposed to indexing into
	// the array, improves execution performance...
	AlarmTextInfo_*  pCurrAlarmTextInfo;

	Int   stringSize;
	Uint  phraseIdx;

	// Convert each plain "phrase" string to preliminary Cheap Text format
	// (there may already be some cheap text embedded in the alarm phrase,
	// e.g. [O2]) just in order to determine the dimensions of the string.
	for (phraseIdx = 0u, pCurrAlarmTextInfo = alarmTextInfoArray;
	  	 pPhraseIds[phraseIdx] != MESSAGE_NAME_NULL;
	  	 phraseIdx++, pCurrAlarmTextInfo++)
	{													// $[TI17.1]
		const wchar_t*  pRawText;   
		wchar_t*        pAlarmText = pCurrAlarmTextInfo->pText;  

		// Store a pointer to the current alarm phrase
		pRawText = AlarmStrs::GetMessage(MessageName(pPhraseIds[phraseIdx]));

		// Remove all hidden hyphens in current alarm phrase and store it in alarmMsgs buffer.
		StripDelimiterChar_(pRawText, pAlarmText);

		// Now get the dimensions of the phrase (rendered in the chosen font
		// size).

		// Format the cheap text slightly differently depending on whether
		// the alarm message has the "thin font" tag at the start or not.
		if (L'T' == pAlarmText[0] && L':' == pAlarmText[1])
		{												// $[TI1]
			stringSize = swprintf(CheapText_, L"{p=%d,y=%d,%s}",
									fontSize, TEXT_BASELINE, pAlarmText);
		}
        else
        {
            // If no leading style char found, assume Normal/CJK
			stringSize = swprintf(CheapText_, L"{p=%d,y=%d,%c:%s}",
                                 fontSize, TEXT_BASELINE, FontStyleChar_, pAlarmText);
        }

		// Verify that phrase is not too long
		AUX_CLASS_ASSERTION((stringSize < sizeof(CheapText_)), stringSize);

		// Program the cheap text into a TextField object
		RTextField_.setText(CheapText_);

		// Get the width of the cheap text
		pCurrAlarmTextInfo->width = RTextField_.getWidth();

		// The moment of truth ... check to see if current string fits within
		// the specified width of the rendering area.
		if ((pCurrAlarmTextInfo->width + margin) > areaWidth)
		{												// $[TI3]
			return(FALSE);
		}												// $[TI4]

		// It fits width-wise.  Let's find the absolute height of the text
		// (from uppermost pixel to lowermost pixel) and the descent (the
		// number of pixels below the baseline).  We'll need these when we
		// have to center the strings vertically within the height of the area.
		pCurrAlarmTextInfo->height = RTextField_.getHeight()
									- RTextField_.getUnusedVerticalHeight();
		pCurrAlarmTextInfo->descent = RTextField_.getHeight() - TEXT_BASELINE;
	}													// $[TI17.2]

	// store the number of phrases...
	const Uint  NUM_PHRASES = phraseIdx;

	// Let's find the Y positions of the top left of each string so that
	// the strings are centered vertically.  Different algorithm depending
	// on the number of phrases.
	switch (NUM_PHRASES)
	{
	case 1:												// $[TI5]
		{
			const Int  HEIGHT0 = alarmTextInfoArray[0].height;

			alarmTextInfoArray[0].yPos = (areaHeight - HEIGHT0) / 2;
		}
		break;

	case 2:												// $[TI6]
		{
			const Int  HEIGHT0    = alarmTextInfoArray[0].height;
			const Int  HEIGHT1    = alarmTextInfoArray[1].height;
			const Int  PHRASE_GAP = (areaHeight - HEIGHT0 - HEIGHT1) / 3;

			alarmTextInfoArray[0].yPos = PHRASE_GAP;
			alarmTextInfoArray[1].yPos = HEIGHT0 + (2 * PHRASE_GAP);
		}
		break;

	case 3:												// $[TI7]
		{
			const Int  HEIGHT0    = alarmTextInfoArray[0].height;
			const Int  HEIGHT1    = alarmTextInfoArray[1].height;
			const Int  HEIGHT2    = alarmTextInfoArray[2].height;
			const Int  PHRASE_GAP = (areaHeight - HEIGHT0 - HEIGHT1 - HEIGHT2) / 4;

			alarmTextInfoArray[0].yPos = PHRASE_GAP;
			alarmTextInfoArray[1].yPos = HEIGHT0 + (2 * PHRASE_GAP);
			alarmTextInfoArray[2].yPos = HEIGHT0 + HEIGHT1 + (3 * PHRASE_GAP);
		}
		break;

	default:
		AUX_CLASS_ASSERTION_FAILURE(NUM_PHRASES);
		break;
	}

	// Generate the final cheap text for each phrase, containing the correct
	// positioning information.  Plug results into pString1, pString2 and
	// pString3.
	for (phraseIdx = 0u, pCurrAlarmTextInfo = alarmTextInfoArray;
	  	 phraseIdx < NUM_PHRASES; phraseIdx++, pCurrAlarmTextInfo++)
	{													// $[TI18.1]
		Int32 msgX;		// X position at which each string should be positioned
						// so as to align the string according to the
						// 'alignment' and 'margin' parameters.

		// Select the proper destination string.
		wchar_t* pCurrString; 

		switch (phraseIdx)
		{
		case 0:											// $[TI8]
			pCurrString = pString1;
			break;
		case 1:											// $[TI9]
			pCurrString = pString2;
			break;
		case 2:											// $[TI10]
			pCurrString = pString3;
			break;
		default:
			CLASS_ASSERTION_FAILURE();
			break;
		}

		// Calculate the X position at which the cheap text should be positioned
		// according to what alignment is desired.
		switch (alignment)
		{
		case LEFT:										// $[TI11]
			msgX = margin;
			break;
		case RIGHT:										// $[TI12]
			msgX = areaWidth - margin - pCurrAlarmTextInfo->width;
			break;
		case CENTER:									// $[TI13]
			msgX = (areaWidth - pCurrAlarmTextInfo->width) / 2;
			break;
		default:
			AUX_CLASS_ASSERTION_FAILURE(alignment);
			break;
		}

		const Int  Y_POS = (pCurrAlarmTextInfo->yPos + pCurrAlarmTextInfo->height -
							pCurrAlarmTextInfo->descent);

		wchar_t*  pAlarmText = pCurrAlarmTextInfo->pText;
		
		// Generate the cheap text (keeping a lookout for the "thin font" tag)
		if (L'T' == pAlarmText[0]  && L':' == pAlarmText[1])
		{												// $[TI14]
			stringSize = swprintf(pCurrString, L"{p=%d,x=%d,y=%d%s}", fontSize,
								 msgX, Y_POS, pAlarmText);
		}
		else
		{												// $[TI15]
			// If no leading style char found, assume NORMAL
            stringSize = swprintf(pCurrString, L"{p=%d,x=%d,y=%d,%c:%s}", fontSize,
                                 msgX, Y_POS, FontStyleChar_, pAlarmText);
		}

		AUX_CLASS_ASSERTION((stringSize < TextField::MAX_STRING_LENGTH),
							stringSize);
	}													// $[TI18.2]

	// If we get this far, then all of the phrases have been rendered 
	// successfully into the TextField objects.
	return (TRUE);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: WithWordWrap	[public, static]
//
//@ Interface-Description
// Converts an arbitrary number of raw alarm analysis phrases to Cheap Text
// format and stores the cheap text in up to three character buffers.  The
// cheap text is word-wrapped to fit within a specified area with each line of
// text being centered vertically and formatted according to specifications
// horizontally. Parameters are as follows:
// >Von
//	pString1		Pointer to the first buffer that will contain unwrapped
//					cheap text.
//	pString2		Pointer to the second buffer that will contain unwrapped
//					cheap text.
//	pString3		Pointer to the third buffer that will contain unwrapped
//					cheap text.
//	pPhraseIds		A null-terminated array of alarm phrase id's.  Must have
//					at least one phrase.
//	fontSize		The point size of the rendered text.
//	areaWidth		The width of the area in which the text should be rendered.
//	areaHeight		The height of the area in which the text should be rendered.
//	aligment		How the text should be aligned horizontally within the area,
//					LEFT, RIGHT or CENTER.
//	margin			When the alignment is LEFT or RIGHT, this specifies the
//					margin in pixels to leave at the edge of the area when
//					left or right justifying text.  When aligment is CENTER
//					it should be set to twice the margin needed at each side
// 					of the text, e.g. when centering text you may demand that
//					at least 2 pixels be unused at either side of the text so
//					set margin to be 4.
// isBaseAlarmMsg	Flag to indicate whether we are doing word wrapping on a BASE
//					alarm message (=TRUE) or not (=FALSE).  The intent is for this 
//					method to detect the maximum lines to do the wrapping.  BASE alarm
//					in the Alarm area only take 2 lines as opposed to 3 lines in other
//					Alarm areas.
// >Voff
// The return value is the number of lines used to render the phrases.
//---------------------------------------------------------------------
//@ Implementation-Description
// Processing occurs in two major phases:
// >Von
//	1. Combine all tokens (words) into a single large buffer.
//	2. Test render each individual token in Cheap Text format, saving
//     the width of the token.
//  3. Perform word wrapping to pack each output line with the maximum
//     number of tokens.
// >Voff
// Phase 1:  Combine tokens into single buffer.
// For each phrase id, this routine fetches the corresponding alarm phrase
// string (raw C string) from the alarm string database and appends the raw
// phase string to a large buffer.  The phrases are separated by special
// markers (a backslash character) so that the following test rendering phase
// knows where to insert an extra space to separate the phrases.
//
// Phase 2: Perform word wrapping of tokens.
//	Invoke WordWrapping_() method to perform the word wrapping.  Depending on the
//	return value which signals if overflow happened, WordWrapping_() is performed
// once again with the next available font size to achieve desired result.
//---------------------------------------------------------------------
//@ PreCondition
// The 'pString1', 'pString2', 'pString3' and 'pPhraseIds' arguments must not
// be NULL.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Int32
AlarmRender::WithWordWrap(
					wchar_t* pString1, wchar_t* pString2, wchar_t* pString3,
					const int* pPhraseIds, TextFont::Size fontSize,
					Int32 areaWidth, Int32 areaHeight, Alignment alignment,
					Int32 margin, Int32 isBaseAlarmMsg)
{
	CALL_TRACE("AlarmRender::WithWordWrap("
					"char* pString1, char* pString2, char* pString3, "
					"const int* pPhraseIds, TextFont::Size fontSize, "
					"Int32 areaWidth, Int32 areaHeight, Alignment alignment, "
					"Int32 margin, Int32 isBaseAlarmMsg)");
	
	// Check for illegal null parameters
	CLASS_PRE_CONDITION(pString1 != NULL);
	CLASS_PRE_CONDITION(pString2 != NULL);
	CLASS_PRE_CONDITION(pString3 != NULL);
	CLASS_PRE_CONDITION(pPhraseIds != NULL);

	Uint  phraseIdx;

	// The first step is to build a single string buffer (Buf_) which
	// contains all of the tokens to be displayed.  If a phrase is prefixed
	// by "T:", then all tokens in that phrase are to be rendered in thin
	// font.  Otherwise, all tokens in that phrase are to be rendered in
	// normal (bold) font.  To mark the boundaries between phrases, insert
	// a special backslash (\) token into the token stream.  These
	// separators are taken out when the tokens are processed below.
	Buf_[0] = L'\0';		// start with a clean buffer
	for (phraseIdx = 0u; pPhraseIds[phraseIdx] != MESSAGE_NAME_NULL; phraseIdx++)
	{													// $[TI2]
		// Get the phrase string from Alarm-Analysis and add to the buffer.
		const wchar_t*  pPhrase =
				  AlarmStrs::GetMessage(MessageName(pPhraseIds[phraseIdx]));
		AUX_CLASS_ASSERTION(NULL != pPhrase,phraseIdx);

		if (phraseIdx != 0u)
		{												// $[TI2.1]
			wcscat(Buf_, L" \\ ");	// insert trick token to separate "phrases"
		}												// $[TI2.2]

		wcscat(Buf_, pPhrase);
	}													// $[TI3]

	CLASS_PRE_CONDITION((phraseIdx >= 1));
	CLASS_ASSERTION((wcslen(Buf_) + MAX_FORMAT_SIZE_) < sizeof(Buf_));

	Int32  numLines = WordWrapping_(pString1, pString2, pString3, fontSize,
								    areaWidth, areaHeight, alignment, margin,
								    isBaseAlarmMsg);
	
	if ((isBaseAlarmMsg ? MAX_BASE_MSGS_ : MAX_ANALYSIS_MSGS_) < numLines)
	{												   // $[TI4.1] 
		// If text drawn in current font size is too long to fit into the
		// available space, try wrapping it again in the next smaller font size. 
		pString1[0] = L'\0';
		pString2[0] = L'\0';
		pString3[0] = L'\0';

		numLines = WordWrapping_(pString1, pString2, pString3, 
								 GetNextSmallerFontSize_(fontSize), 
								 areaWidth, areaHeight, alignment, margin,
								 isBaseAlarmMsg);
	}												  // $[TI4.2]
	AUX_CLASS_ASSERTION((numLines > 0), numLines);
	return(numLines);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetSymbolHelpMessage	[public, static]
//
//@ Interface-Description
// Given an alarm message id ('pPhraseIds'), determine whether or not the root
// cause message is a symbol.  If it is a symbol, then return the help message
// that should be displayed when that symbol is touched by the user.
//---------------------------------------------------------------------
//@ Implementation-Description
// Decode the given alarm message id to see whether or not it is a
// touchable root cause symbol.  If so, then set the translation message
// for the root cause symbol.  This allows the user to touch the
// displayed alarm root cause symbol and see the translation message in
// the lower screen Message Area.  If the alarm root cause message is
// not a symbol, then a null message is returned (textual root cause
// messages are not touchable).
//---------------------------------------------------------------------
//@ PreCondition
// The 'pPhraseIds' parameter must not be NULL.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

StringId
AlarmRender::GetSymbolHelpMessage(const int* pPhraseIds)
{
	CALL_TRACE("AlarmRender::GetSymbolHelpMessage(const int* pPhraseIds)");

	StringId helpMessage;

	CLASS_PRE_CONDITION(NULL != pPhraseIds);

	switch (pPhraseIds[0])
	{
	case HIGH_PRES_CIRC_MSG:							// $[TI1]
		helpMessage = MiscStrs::TOUCHABLE_HIGH_CIRC_PRES_MSG;
		break;

	case HIGH_TIDAL_VOL_MSG:							// $[TI2]
		helpMessage = MiscStrs::TOUCHABLE_HIGH_TIDAL_VOLUME_MSG;
		break;

	case HIGH_MINUTE_VOL_MSG:							// $[TI3]
		helpMessage = MiscStrs::TOUCHABLE_HIGH_MINUTE_VOLUME_MSG;
		break;

	case HIGH_RESP_RATE_MSG:							// $[TI4]
		helpMessage = MiscStrs::TOUCHABLE_HIGH_RESP_RATE_MSG;
		break;

	case LOW_MAND_TIDAL_VOL_MSG:						// $[TI5]
		helpMessage = MiscStrs::TOUCHABLE_LOW_MAND_TIDAL_VOL_MSG;
		break;

	case LOW_SPONT_TIDAL_VOL_MSG:						// $[TI6]
		helpMessage = MiscStrs::TOUCHABLE_LOW_SPONT_TIDAL_VOL_MSG;
		break;

	case LOW_MINUTE_VOL_MSG:							// $[TI7]
		helpMessage = MiscStrs::TOUCHABLE_LOW_MINUTE_VOLUME_MSG;
		break;

	case LOW_O2_MSG:									// $[TI8]
		helpMessage = MiscStrs::TOUCHABLE_LOW_O2_MSG;
		break;

	case HIGH_O2_MSG:									// $[TI9]
		helpMessage = MiscStrs::TOUCHABLE_HIGH_O2_MSG;
		break;

	case HIGH_PRES_VENT_MSG:							// $[TI10]
		helpMessage = MiscStrs::TOUCHABLE_HIGH_PRES_VENT_MSG;
		break;
	
    case INSPIRED_SPONT_VOLUME_LIMIT_MSG:			   // $[TI12]
    case INSPIRED_SPONT_PRESSURE_LIMIT_MSG:
		helpMessage = MiscStrs::TOUCHABLE_HIGH_INSP_SPONT_TIDAL_VOL_MSG;
		break;

    case INSPIRED_MAND_VOLUME_LIMIT_MSG:			   // $[TI20]
        helpMessage = MiscStrs::TOUCHABLE_HIGH_INSP_MAND_TIDAL_VOL_MSG;
        break;
    
    case COMPENSATION_LIMIT_MSG:					// $[TI13]
		helpMessage = MiscStrs::TOUCHABLE_HIGH_COMP_PRES_MSG;
		break;

    case LOW_PRES_CIRC_MSG:							// $[TI21]
		helpMessage = MiscStrs::TOUCHABLE_LOW_CIRC_PRES_MSG;
		break;

	default:											// $[TI11]
		helpMessage = NULL_STRING_ID;
		break;
	}

	return (helpMessage);
}


//=====================================================================
//
// Private Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: WordWrapping_	[private, static]
//
//@ Interface-Description
// Converts an arbitrary number of raw alarm analysis phrases to Cheap Text
// format and stores the cheap text in up to three character buffers.  The
// cheap text is word-wrapped to fit within a specified area with each line of
// text being centered vertically and formatted according to specifications
// horizontally. Parameters are as follows:
// >Von
//	pString1		Pointer to the first buffer that will contain unwrapped
//					cheap text.
//	pString2		Pointer to the second buffer that will contain unwrapped
//					cheap text.
//	pString3		Pointer to the third buffer that will contain unwrapped
//					cheap text.
//	fontSize		The point size of the rendered text.
//	areaWidth	The width of the area in which the text should be rendered.
//	areaHeight	The height of the area in which the text should be rendered.
//	aligment		How the text should be aligned horizontally within the area,
//					LEFT, RIGHT or CENTER.
//	margin		When the alignment is LEFT or RIGHT, this specifies the
//					margin in pixels to leave at the edge of the area when
//					left or right justifying text.  When aligment is CENTER
//					it should be set to twice the margin needed at each side
// 				of the text, e.g. when centering text you may demand that
//					at least 2 pixels be unused at either side of the text so
//					set margin to be 4.
// isBaseAlarmMsg	Flag to indicate whether we are doing word wrapping on a BASE
//					alarm message (=TRUE) or not (=FALSE).  The intent is for this 
//					method to detect the maximum lines to do the wrapping.  BASE alarm
//					in the Alarm area only take 2 lines as opposed to 3 lines in other
//					Alarm areas.
// >Voff
// The return value is the number of lines used to render the phrases plus the overFlow
//	flag value (0 if wordwrapping completes successfully, 1 if line overflow occurs.)
//---------------------------------------------------------------------
//@ Implementation-Description
// Processing occurs in three major phases:
// >Von
//	1. Test render each individual token in Cheap Text format, saving
//     the width of the token.
// 2. Perform word wrapping to pack each output line with the maximum
//     number of tokens.
// >Voff
// Phase 1: Perform test rendering of tokens.
// For each token (a token being a string of characters unbroken by white
// space) in the large buffer, convert it to Cheap Text format, render the
// Cheap Text to determine it's width, then save the Cheap Text token in
// another large buffer (along with the terminating NULL character at the end
// of each token) and save the width of the token in a parallel buffer.  These
// buffers contain "space" tokens which act as placeholders for the spaces
// between tokens, "hyphen" tokens which act as placeholders for the hidden
// hyphens in alarm strings.  The backslash tokens from the previous phase are 
// converted into space tokens to provide two consecutive spaces between phrases.
// Also, the first token (word) of each phrase determines the Cheap Text font style
// (normal vs.  bold) for the rest of the words in the phrase.
//
// Phase 3:  Perform word wrapping.
// Now that we have pre-computed the sizes of each individual token, spaces and
// hyphens, we execute a straightforward loop to combine the individual Cheap
// Text tokens into one large Cheap Text string which represents the contents
// of each line, up to a maximum of three lines.  There is special care to
// remove unnecessary space tokens from the front of a line.  When a line is 
// filled, action is taken to determine if, where and when a hyphen is added to the
// current line.  Note that the final Cheap Text string consists of a list of
// Cheap Text strings, taking advantage of the nesting feature supported by Cheap Text.
//
// If all phrases are rendered successfully, return the number of lines
// used to render all phrases plus the value of the overflow flag
// (0 if wordwrapping completes successfully, 1 if line overflow occurs.)
//---------------------------------------------------------------------
//@ PreCondition
// The 'pString1', 'pString2', 'pString3' and 'pPhraseIds' arguments must not
// be NULL.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
 
Int32
AlarmRender::WordWrapping_(
					wchar_t* pString1, wchar_t* pString2, wchar_t* pString3,
					TextFont::Size fontSize, Int32 areaWidth, Int32 areaHeight,
					Alignment alignment, Int32 margin, Int32 isBaseAlarmMsg)
{
	wchar_t *pValuePlace;				// Used for finding the place in a cheap
									// text string where an X or Y value
									// needs to be put (see below for info).
	wchar_t tmpValueBuf[10];			// Temporary buffer.
	const wchar_t X_PLACE_MARKER = L' ';// This character marks the place in a
									// cheap text string where the X value
									// is placed.
	const wchar_t Y_PLACE_MARKER = L'*';// This character marks the place in a
									// cheap text string where the Y value
									// is placed.

	// Current font style of alarm message (NORMAL or THIN).
	enum LocalFontStyle
	{
		NULL_STYLE,
		NORMAL_STYLE,
		THIN_STYLE,
		CJK_STYLE
	};
	LocalFontStyle fontStyle;	// state variable for current font style

	// Initialize the strtok() tokenizer for the pre-processed token buffer
	// Buf_[].  The first token is a special case for strtok().
	fontStyle = NULL_STYLE;
	CurrentLine_[0] = L'\0';


	Boolean		overFlowValue = 0;	// Mark whether rendering space is overflowed

	Int32 		spaceWidth = 0;		// pre-computed width of rendered space
	wchar_t	  	spaceToken[2];		// used by renderedTokenPtrs[]

	spaceToken[0] = L' ';
	spaceToken[1] = L'\0';

	// Test render a space character to figure out how wide a space is,
	// saving the "token" in spaceToken[].
	swprintf(CheapText_, L"{p=%d,y=%d,%c:%s}", fontSize, fontSize + 2, FontStyleChar_, spaceToken);
	RTextField_.setText(CheapText_);
	spaceWidth = RTextField_.getWidth();

	const Int32 NUM_SPACES_IN_WIDE_SPACE = 3;
	wchar_t	wideSpaceToken[NUM_SPACES_IN_WIDE_SPACE+1];
	Int32 wideSpaceWidth = 0;		// will hold pixel width of "wide" rendered space
	if (IsCJK_)
	{
		// Added wide space token for CJK
		for (Int32 ii=0; ii<NUM_SPACES_IN_WIDE_SPACE ; ii++)
		{
			wideSpaceToken[ii] = L' ';
		}
		wideSpaceToken[NUM_SPACES_IN_WIDE_SPACE] = L'\0';
		wideSpaceWidth = spaceWidth * NUM_SPACES_IN_WIDE_SPACE; 	// width of wide space token
	}

	wchar_t* renderedTokenPtrs[MAX_TOKENS_];	// table of ptrs into  
											// CurrentLine_[]
	Int32 renderedTokenWidths[MAX_TOKENS_];		// table of rendered widths
	Int32 hyphenMarkFlag[MAX_TOKENS_];
	Int32 numRenderedTokens = 0;				// number of table entries
	wchar_t* pCurToken = NULL;						// token being processed

	Int    stringSize;

	// Now, do the first step -- test render one token at a time.  Okay,
	// each iteration of this loop copies one of the pre-processed tokens
	// from the master token list (Buf_[]) and adds it to the token list
	// (renderedTokenPtrs[]).  The CurrentLine_[] buffer is used to store the
	// tokens, where CurrentLine_ contains a stream of *NULL-terminated*
	// tokens.
	PBuf_ = Buf_;
	static wchar_t  TokenArray_[TextField::MAX_STRING_LENGTH];	   // Buffer to store found token.   
	
	wchar_t* pTok = NULL;  

	Boolean firstPhrase = TRUE;

	while ((pTok = GetTok_(TokenArray_)) != NULL)
	{ 												 	// $[TI3]
		// Add the next token, if any, to the token array with the appropriate
		// number of spaces between tokens.  That is, one space between two
		// tokens in the same phrase and two spaces between two tokens of
		// different phrases.
		if (0 == wcscmp(pTok, L"\\"))
		{												// $[TI3.1]
			// Convert trick separator token into an extra space.
			fontStyle = NULL_STYLE;
			hyphenMarkFlag[numRenderedTokens] = FALSE;

			if (IsCJK_)
			{
				if (firstPhrase)
				{		// $[TI3.1.1]
					// For Japanese, use a wide space after the analysis message:
					renderedTokenPtrs[numRenderedTokens] = wideSpaceToken;
					renderedTokenWidths[numRenderedTokens++] = wideSpaceWidth;
				}
				else
				{	  // $[TI3.1.2]
					renderedTokenPtrs[numRenderedTokens] = spaceToken;
					renderedTokenWidths[numRenderedTokens++] = spaceWidth;
				}
				firstPhrase = FALSE;
			}
			else
			{
				renderedTokenPtrs[numRenderedTokens] = spaceToken;
				renderedTokenWidths[numRenderedTokens++] = spaceWidth;
			}
		}
		else
		{
														// $[TI3.2]
			if (NULL_STYLE == fontStyle)
			{											// $[TI3.2.1]
				// This is the first token of a phrase, so we must check
				// to see what font style for this entire phrase.
				if (L'T' == pTok[0] && L':' == pTok[1])
				{										// $[TI3.2.1.1]
					pTok += 2;
					fontStyle = THIN_STYLE;
				}
				else if (IsCJK_)
				{
					// $[TI3.2.1.2]
					// For CJK, assume that style is CJK_STYLE 
					fontStyle = CJK_STYLE;
				}
				else
				{										// $[TI3.2.1.2]
					fontStyle = NORMAL_STYLE;
				}
			}										  // $[TI3.3]

			switch (fontStyle)
			{
			case NORMAL_STYLE:						// $[TI3.3.1]
				swprintf(CheapText_, L"{N:%s}", pTok);
				break;

			case THIN_STYLE:							// $[TI3.3.2]
				swprintf(CheapText_, L"{T:%s}", pTok);
				break;

			case CJK_STYLE: 						   // $[TI3.3.3] 
				swprintf(CheapText_, L"%s", pTok);  
				break;	// need to exclude J/Z style in format to strip off
						// one additional level of braces which otherwise
						// would cause an assertion -- string too long.

			default:
				AUX_CLASS_ASSERTION_FAILURE(fontStyle);	// state machine broken
				break;
			}

			// Check to see if this is not the first token we've processed
			if (NULL != pCurToken)				 
			{
				//	$[TI3.4.1]
				if (!IsCJK_)
				{
					// It's not.  If it's a new word, add a separating space "token" to the token
					if (hyphenMarkFlag[numRenderedTokens-1] == FALSE)
					{										 //	$[TI3.4.1.1]
						hyphenMarkFlag[numRenderedTokens] = FALSE;
						renderedTokenPtrs[numRenderedTokens] = spaceToken;
						renderedTokenWidths[numRenderedTokens++] = spaceWidth;
					}										 //	$[TI3.4.1.2]
				}
				// skip past terminating NULL
				pCurToken += wcslen(pCurToken) + 1;
			}
			else
			{								  			//	$[TI3.4.2]
				// First token, point current token to the start of the
				// current line
				pCurToken = CurrentLine_;
			}

			wcscpy(pCurToken, CheapText_);

			stringSize = swprintf(CheapText_, L"{p=%d,y=%d,%c:%s}", fontSize,
								 (fontSize + TEXT_BASELINE), FontStyleChar_, pCurToken);
			AUX_CLASS_ASSERTION((stringSize < sizeof(CheapText_)), stringSize);


			// Test render the token in order to determine its width.
			RTextField_.setText(CheapText_);

			// Save the token and its width in the rendered token list.
			hyphenMarkFlag[numRenderedTokens] =  isHyphenated;
			renderedTokenPtrs[numRenderedTokens] = pCurToken;
			renderedTokenWidths[numRenderedTokens++] = RTextField_.getWidth();
		}
	} 	// $[TI3.5]

	AUX_CLASS_ASSERTION((numRenderedTokens < MAX_TOKENS_), numRenderedTokens);

	// set the ending element with an ending marker...
	renderedTokenPtrs[numRenderedTokens]   = NULL;
	renderedTokenWidths[numRenderedTokens] = -1;
	hyphenMarkFlag[numRenderedTokens]      = FALSE;
	
	// Begin the second step -- word wrapping.  At this point,
	// renderedTokenPtrs[] contains a list of tokens (including spaces)
	// and renderedTokenWidths[] contains the rendered width of each
	// token.  Now, we must pack each output line with the maximum number
	// of tokens that will fit on that line.

	Int32 	hyphenWidth = 0;			// pre-computed width of rendered hyphen
	Int32 	hyphenLength = 0;			// pre-computed length of rendered hyphen
	wchar_t	hyphenToken[7];				// used by renderedTokenPtrs[]

	// Test render a hyphen character to figure out how wide a hyphen is,
	// saving the "token" in hyphenToken[].
	hyphenToken[0] = L'\0';
	swprintf(hyphenToken, L"{T:%c}", HYPHEN);

	swprintf(CheapText_, L"{p=%d,y=%d,T:%c}", fontSize, (fontSize + 2), HYPHEN);
	RTextField_.setText(CheapText_);
	hyphenWidth = RTextField_.getWidth();
	hyphenLength = wcslen(hyphenToken);

	Int32 currentToken = 0;

	AlarmTextInfo_   alarmLineInfoArray[3];

	alarmLineInfoArray[0].pText = pString1;
	alarmLineInfoArray[1].pText = pString2;
	alarmLineInfoArray[2].pText = pString3;

	// using a pointer to an individual array item, as opposed to indexing into
	// the array, improves execution performance...
	AlarmTextInfo_*  pCurrLineInfo = alarmLineInfoArray;

	Int32 currentLineNum = 0;
	for (currentLineNum = 0; currentLineNum < (isBaseAlarmMsg ? MAX_BASE_MSGS_: MAX_ANALYSIS_MSGS_);
                    		pCurrLineInfo++, currentLineNum++)
	{									 // $[TI3.6]							 		

		while (wcscmp(spaceToken, renderedTokenPtrs[currentToken]) == 0)
		{										// $[TI3.6.1]
			currentToken++;
		}										// $[TI3.6.2]	

		wchar_t*  pCurrLineText = pCurrLineInfo->pText;

		CLASS_ASSERTION(NULL != pCurrLineText);
		pCurrLineInfo->width = 0;

		// Copy the initial cheap text into the current line.  Store place
		// markers for the X and Y values.  We will later overwrite values
		// at these places.  Force J mode at start of each line

		swprintf(pCurrLineText, L"{p=%d,%c     ,y=%c   ,%c:",
				fontSize, X_PLACE_MARKER, Y_PLACE_MARKER, FontStyleChar_);

		pCurrLineInfo->length = wcslen(pCurrLineText);

		// Copy as many tokens as will fit the current line.

		while (currentToken < numRenderedTokens)
		{												// $[TI3.7]
			// Check to see if the current token can fit on the current line.
			// We must meet two criterias.  The rendered display width must be large
			// enough to hold the new token.  The actual string length must within
			// the defined TextField::MAX_STRING_LENGTH.
			// $[01140] The root cause portion of the alarm analysis ...
			Int tokLen = wcslen(renderedTokenPtrs[currentToken]);
		
			if ((pCurrLineInfo->width +
				 	renderedTokenWidths[currentToken] + margin) <= areaWidth &&
				(pCurrLineInfo->length + tokLen) < TextField::MAX_STRING_LENGTH)
			{											// $[TI3.7.1]
				// It can fit, so add it to the current line and accumulate its width
				// in the line's width.
				wcscat(pCurrLineText, renderedTokenPtrs[currentToken]);
				pCurrLineInfo->width += renderedTokenWidths[currentToken];
				pCurrLineInfo->length += tokLen;
				currentToken++;
			}
			else
			{											// $[TI3.7.3]
				// Current token would cause us to exceed max line length, so
				// if in the middle of a word, we add a hyphen before carrying text to
				// the next line.
				if (hyphenMarkFlag[currentToken-1] == TRUE)
				{										// $[TI3.7.3.1]
					if ((pCurrLineInfo->width + hyphenWidth + margin)
				 	 				 <= areaWidth &&
						(pCurrLineInfo->length + hyphenLength) < TextField::MAX_STRING_LENGTH)
					{									// $[TI3.7.3.1.1]
						// There is room for the hyphen, so add it to the list
						wcscat(pCurrLineText, hyphenToken);
						pCurrLineInfo->width += hyphenWidth;
						pCurrLineInfo->length += hyphenLength;
			  		}
			  		else
			  		{								  // $[TI3.7.3.1.2]
						// Not enough room for the hyphen, so we take the previously added token
						// out of the current line then add a hyphen (No action is needed for the current token)
						
						// Save the forementioned token in the renderedTokenPtrs buffer.
						// currentToken now indexes to the PREVIOUS TOKEN 
						currentToken--;

						// Update AlarmMsgs' width.
						pCurrLineInfo->width -= renderedTokenWidths[currentToken];

						pCurrLineInfo->length -= wcslen(renderedTokenPtrs[currentToken]);

						*(pCurrLineText+(pCurrLineInfo->length)) = L'\0';

						// Check the current status of previous token then add a hyphen only 
						if (hyphenMarkFlag[currentToken-1] == TRUE)
						{								// $[TI3.7.3.1.2.1]
							if ((pCurrLineInfo->width + hyphenWidth + margin)
				 	 				 <= areaWidth &&
								(pCurrLineInfo->length + hyphenLength) < TextField::MAX_STRING_LENGTH)
							{						// $[TI3.7.3.1.2.1.1]
								wcscat(pCurrLineText, hyphenToken);
								pCurrLineInfo->width += hyphenWidth;
								pCurrLineInfo->length += hyphenLength;
							}							// $[TI3.7.3.1.2.1.2]
						}								// $[TI3.7.3.1.2.2]
			  		}
				}
				// We're done for the current line.
				break;
			}
		}												// $[TI3.8]

		// Finish the Cheap Text string for current line.
		wcscat(pCurrLineText, L"}");

		// Let's go find the height and descent of the current line.  Insert
		// a large Y baseline value into the Y position of the cheap text
		// string (guaranteeing that the string can be rendered) then get
		// the height.

		// Find where to place the Y value in the cheap text.
		pValuePlace = wcschr(pCurrLineText, Y_PLACE_MARKER);
		SAFE_CLASS_ASSERTION(pValuePlace);

		// Put large Y baseline value there.
		stringSize = swprintf(tmpValueBuf, L"%04d", TEXT_BASELINE);
		
		wcsncpy(pValuePlace, tmpValueBuf, stringSize);

		// Put cheap text into a text field and get the height and descent
		// of the string.
		RTextField_.setText(pCurrLineText);

		pCurrLineInfo->height = RTextField_.getHeight()
									- RTextField_.getUnusedVerticalHeight();
		pCurrLineInfo->descent = RTextField_.getHeight() - TEXT_BASELINE;

        // To get around vertical spacing problem caused by lower VGA-Graphics-Subsystems.
        // TextRect() computes and returns the smallest possible area occupied by the text.
        // This data is used by GUI-Applications to compute the height of the text.  The
        // defect of this scheme is most visible in multiple lines of text, some with ascenders
        // and descenders; others without. 
		// For Japanese, the style is normal, so the descent info is same as normal font.
		if (!(isBaseAlarmMsg) && pCurrLineInfo->descent == 0)
		{       // $[TI3.8.1]
			pCurrLineInfo->descent = TextFont::GetDescent(fontSize,
							(fontStyle==NORMAL_STYLE) ? TextFont::NORMAL : TextFont::THIN);
			pCurrLineInfo->height += pCurrLineInfo->descent;
		}       // $[TI3.8.2]
 
		// Replace original Y place marker in string so that we can find the
		// place later to put in the final Y position of the text's baseline.
		stringSize = swprintf(tmpValueBuf, L"%c   ", Y_PLACE_MARKER);

		wcsncpy(pValuePlace, tmpValueBuf, stringSize);

		// If we're out of tokens, we're done.  Otherwise skip any space
		// "tokens" that might appear at the beginning of the next line and
		// advance to the next output line.
		if (currentToken >= numRenderedTokens)
		{												// $[TI3.9]
			break;
		}
	}													// $[TI3.11]

	if (currentLineNum >= (isBaseAlarmMsg ? MAX_BASE_MSGS_ : MAX_ANALYSIS_MSGS_))
	{											   // $[TI3.11.1]
		currentLineNum--;
		overFlowValue = 1;
	}												// $[TI3.11.2]

	const Uint  NUM_LINES = (currentLineNum + 1);
	Real32 phraseGap;
	// Let's calculate the Y positions of the top left of each line so that
	// the lines are centered vertically within the prescribed area.  Algorithm
	// differs based on how many lines we are rendering.
	switch (NUM_LINES)
	{
	case 1:												// $[TI4]
		{
			const Int  HEIGHT0 = alarmLineInfoArray[0].height;
			phraseGap = HEIGHT0;
			alarmLineInfoArray[0].yPos = (areaHeight - HEIGHT0) / 2;
		}
		break;

	case 2:												// $[TI5]
		{
			const Int  HEIGHT0    = alarmLineInfoArray[0].height;
			const Int  HEIGHT1    = alarmLineInfoArray[1].height;
			phraseGap = (areaHeight - HEIGHT0 - HEIGHT1) / 3.0;

			alarmLineInfoArray[0].yPos = Int32(phraseGap);
			alarmLineInfoArray[1].yPos = Int32(HEIGHT0 + 2 * phraseGap);
		}
		break;

	case 3:												// $[TI6]
		{
			const Int  HEIGHT0    = alarmLineInfoArray[0].height;
			const Int  HEIGHT1    = alarmLineInfoArray[1].height;
			const Int  HEIGHT2    = alarmLineInfoArray[2].height;
			phraseGap = (areaHeight - HEIGHT0 - HEIGHT1 - HEIGHT2) / 4.0;

			alarmLineInfoArray[0].yPos = Int32(phraseGap) >0 ? (Int32(phraseGap)+1) : 2;											// $[TI6.1] $[TI6.2]	
			alarmLineInfoArray[1].yPos = (Int32(HEIGHT0 + 2 * phraseGap) + 1);
			alarmLineInfoArray[2].yPos = (Int32(HEIGHT0 + HEIGHT1 + 3 * phraseGap) + 1);
		}
		break;

	default:
		AUX_CLASS_ASSERTION_FAILURE(NUM_LINES);
		break;
	}

	// Okay, time to finalize the X and Y positions of each string and stuff
	// the values into the cheap text.
	for (currentLineNum = 0, pCurrLineInfo = alarmLineInfoArray;
	  	 currentLineNum < NUM_LINES; currentLineNum++, pCurrLineInfo++)
	{													// $[TI7]
		Int32 msgX;		// X position at which each string should be positioned
						// so as to align the string according to the
						// 'alignment' parameter.

						// so as to align the string according to the
						// 'alignment' parameter.

		// Calculate the X position at which the cheap text should be positioned
		// according to what alignment is desired.
		switch (alignment)
		{
		case LEFT:									 // $[TI8]
			msgX = margin;
			break;

		case RIGHT:									 // $[TI9]
			msgX = areaWidth - margin - pCurrLineInfo->width;
			break;

		case CENTER:								 // $[TI10]
			msgX = (areaWidth - pCurrLineInfo->width) / 2;
			break;

		default:
			AUX_CLASS_ASSERTION_FAILURE(alignment);
			break;
		}

		// Okay, let's stuff the correct X and Y position values into the
		// current line.  Remember alarmMsgsY[] stores the Y position of
		// the top left of each string whereas the Y position that needs to
		// be inserted into the cheap text is Y position of the *baseline*
		// of the text so we need to do a funky calculation to find
		// the correct Y baseline so as to achieve the Y position stored in
		// alarmMsgsY[].

		wchar_t*  pCurrLineText = pCurrLineInfo->pText;

		// Do the X first.  Look for X place marker.
		pValuePlace = wcschr(pCurrLineText, X_PLACE_MARKER);
		SAFE_CLASS_ASSERTION(pValuePlace);
		stringSize = swprintf(tmpValueBuf, L"x=%04d", msgX);
		wcsncpy(pValuePlace, tmpValueBuf, stringSize);

		const Int  Y_POS = (pCurrLineInfo->yPos + pCurrLineInfo->height -
							pCurrLineInfo->descent);

		// Now do the Y baseline value.  Look for Y place marker.
		pValuePlace = wcschr(pCurrLineText, Y_PLACE_MARKER);
		SAFE_CLASS_ASSERTION(pValuePlace);
		stringSize = swprintf(tmpValueBuf, L"%04d", Y_POS);
		wcsncpy(pValuePlace, tmpValueBuf, stringSize);
	}
														// $[TI11]
 	// Return number of lines rendered
	if  (phraseGap <= 0 && NUM_LINES == 3)
	{					// $[TI11.1]
		return(isBaseAlarmMsg ? (MAX_BASE_MSGS_+1) : (MAX_ANALYSIS_MSGS_+1)); 
									 	// To guarantee WordWrapping is redone.
	}
	else
	{					// $[TI11.2]
		return(NUM_LINES + overFlowValue);
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetNextSmallerFontSize_  [private]
//
//@ Interface-Description
//	Get the next available smaller font size than the given one and return.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method knows all available font sizes.  It returns
// the next smaller font size than the given size.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

TextFont::Size 
AlarmRender::GetNextSmallerFontSize_(TextFont::Size fontSize)
{
    Int32 sFont = fontSize;
		
	switch (fontSize)
	{										// $[TI1]
	case TextFont::SIX:						// $[TI1.1]
		sFont = fontSize;
		if (GuiApp::GetLanguage() == LanguageValue::RUSSIAN)
		{
			sFont = fontSize + 2;
		}
		break;

	case TextFont::EIGHT:					// $[TI1.2]
	case TextFont::TEN:
	case TextFont::TWELVE:
	case TextFont::FOURTEEN:
		sFont = fontSize - 2;
		break;

	case TextFont::EIGHTEEN:				// $[TI1.3]
		sFont = TextFont::FOURTEEN;
		break;

	case TextFont::TWENTY_FOUR:				// $[TI1.4]
		sFont = TextFont::EIGHTEEN;
		break;

	default:								// $[TI1.5]
		AUX_CLASS_ASSERTION_FAILURE(fontSize);
		break;
	}

    return((TextFont::Size)sFont);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetTok_ [private]
//
//@ Interface-Description
//	Returns a string from the Buf_ pool of Alarm message strings, separated
// by blank, tab, newline or hyphen character.
//---------------------------------------------------------------------
//@ Implementation-Description
// Look for the next token in the buffer Buf_ which ends with blank, tab,
// newline or hyphen character.  If found, set/reset the isHyphenated flag to
// indicate whether the found token finishes the current word, in which case
// a space character will be added next in another routine.  The found token is 
// then returned.  If no token is found, return empty string.
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
 
wchar_t*
AlarmRender::GetTok_(wchar_t *pTokenString)
{
	// skip leading white space...
	while (*PBuf_ != L'\0'  &&  IsWhiteSpaceChar_(*PBuf_))
	{										// $[TI1]
		PBuf_++ ;
	}										// $[TI2]

	wchar_t*  pToken = pTokenString;

	// Search for next token.  The token will not include any blank, tab, newline or
	// hyphen characters.

	if (IsCJK_)
	{
		// Only gets one char at a time - unless it hits a brace, in which
		// case it proceeds to the close brace.  If a shift-character is found, it and the following character
		// are grabbed.  And, characters joined by CJK_GLUE_CHAR characters are kept in same token.
		if (*PBuf_ != L'\0')
		{   // $[TI3] -- another token is available...

			if (*PBuf_ == L'{')
			{   // $[TI4]
				// found the beginning of an embedded field, make sure
				// that the whole field remains a part of this
				// token (i.e., don't break up "V{S:TE MAND}" into two
				// different tokens)...
				
				// This must be able to handle multi levels of braces.
				int level=-1;
				int count = 0;
				while (*PBuf_ != L'}' || level>0)
				{	 // $[TI5]
					if (*PBuf_ == L'{')
					{  // $[TI6]
						level ++;
					}  // $[TI7]

					if (*PBuf_ == L'}')
					{  // $[TI8]
						level --;
					}  // $[TI9]

					// copy over until while not a '}' character...
					*pToken++ = *PBuf_++;
					count++;
					CLASS_ASSERTION(count < TextField::MAX_STRING_LENGTH); // should be SAFE assertion
				}	 // $[TI10]

				// copy '}' character...
				*pToken++ = *PBuf_++;
			}
			else
			{  // $[TI12]
				int count = 0;
				Boolean firstPass = TRUE;
				do // loop thru all characters joined by glue characters
				{	 // $[TI13]
					// if this is a glue character, skip and grab next:
					if (*PBuf_ == CJK_GLUE_CHAR)
					{	 // $[TI14]
						CLASS_ASSERTION(!firstPass);  // Glue character can't be 1st char of token
						*PBuf_++;		// skip over the glue character
						
						// if glue char found, next char must be present
						CLASS_ASSERTION(*PBuf_ != L'\0');
					}	 // $[TI15]

					// if the next character is a shift character, must grab it as well as next:
					if (*PBuf_ == L'^' || *PBuf_ == L'~' || *PBuf_ == L'`')
					{  // $[TI16]
						*pToken++ = *PBuf_++;
						// if shift char found, next char must be present
						CLASS_ASSERTION(*PBuf_ != L'\0');
					}  // $[TI17]

					// Grab character
					*pToken++ = *PBuf_++;

					count++;
					CLASS_ASSERTION(count < TextField::MAX_STRING_LENGTH); // should be SAFE assertion
					firstPass = FALSE;
					
				} while (*PBuf_ == CJK_GLUE_CHAR);	// if next char is glue char, loop back
				// $[TI18]
			}	 // $[TI19]
		}   // $[TI20] -- no more tokens in 'PBuf_'...
	}
	else
	{ 	// non-CJK tokenizer
		while (*PBuf_ != L'\0'  &&  !IsWhiteSpaceChar_(*PBuf_))
		{   // $[TI3] -- another token is available...
			if (PBuf_[0] == L'{'  &&  PBuf_[1] == L'S'  &&  PBuf_[2] == L':')
			{	 // $[TI4]
				// found the beginning of a subscript field, make sure
				// that the whole subscript field remains a part of this
				// token (i.e., don't break up "V{S:TE MAND}" into two
				// different tokens)...
				while (*PBuf_ != L'}')
				{	 // $[TI5]
					// copy over until while not a '}' character...
					*pToken++ = *PBuf_++;
				}	// $[TI6]

				// copy '}' character...
				*pToken++ = *PBuf_++;
			}
			else
			{	 // $[TI7]
				*pToken++ = *PBuf_++;
			}
		}   // $[TI8] -- no more tokens in 'PBuf_'...
	}

	*pToken = L'\0';

	if (*pTokenString == L'\0')
	{   // $[TI21] -- no more tokens...
		isHyphenated = FALSE;
		pToken       = NULL;
	}
	else
	{   // $[TI22] -- found another token...
		isHyphenated = (*PBuf_ == DELIMITER_CHAR_);
		pToken       = pTokenString;
	}


  	return(pToken);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: StripDelimiterChar_	[public, static]
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
AlarmRender::StripDelimiterChar_(const wchar_t *pInString, wchar_t *pOutString)
{
	for (; *pInString != L'\0'; pInString++)
	{
        if (IsCJK_)
		{
			if ((*pInString != DELIMITER_CHAR_)
				&& (*pInString != CJK_GLUE_CHAR))
			{
				*pOutString++ = *pInString; 
			}
		}
        else if (*pInString != DELIMITER_CHAR_)
		{								// $[TI1.1]
			*pOutString++ = *pInString;	
		}								// $[TI1.2]
	}									// $[TI2]
	*pOutString = L'\0';
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
AlarmRender::SoftFault(const SoftFaultID  softFaultID,
					   const Uint32       lineNumber,
					   const char*        pFileName,
					   const char*        pPredicate)  
{
	CALL_TRACE("AlarmMsg::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, ALARMRENDER,
							lineNumber, pFileName, pPredicate);
}

// =================================================================

