#ifndef UserOptions_HH
#define UserOptions_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: UserOptions - Storage class for User-Configured Software Options
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/UserOptions.hhv   25.0.4.0   19 Nov 2013 13:54:14   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002   By:   rhj    Date: 05-March-2007      DR Number: 6359
//  Project:  RESPM
//  Description:
//        Resolve merge conflict for RM to Baseline 
//
//  Revision: 001  By:  sah    Date: 09-Sep-1999    DR Number: 5424
//  Project:  NeoMode
//  Description:
//      Created to support changing of software option bits, via the
//      Development Options Subscreen.
//
//====================================================================

#include "Sigma.hh"

//@ Usage-Classes
//@ End-Usage



class UserOptions
{
  public:
    UserOptions(void);
    ~UserOptions(void);

    void operator=(const UserOptions&);
    
	inline Boolean  isInitialized(void) const;

	inline Uint32  getOptionBits(void) const;
	inline void    setOptionBits(const Uint32 newOptionBits);
	inline void    resetOptionBits(void);

    static void SoftFault(const SoftFaultID	softFaultID,
						  const Uint32		lineNumber,
						  const char		*pFileName  = NULL, 
						  const char		*pPredicate = NULL);
	
  private:
    UserOptions(const UserOptions&);  // not implemented...

	//@ Data-Member:  initializationFlag_
	// Flag indicating whether user-initialized, or not.
	Boolean  initializationFlag_;

	//@ Data-Member:  optionBits_
	// State of the software option bits.
	Uint32  optionBits_;
};


#include "UserOptions.in"


#endif // UserOptions_HH
