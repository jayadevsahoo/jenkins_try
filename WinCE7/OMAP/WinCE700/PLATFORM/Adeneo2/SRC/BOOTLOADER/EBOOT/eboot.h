// All rights reserved ADENEO EMBEDDED 2010
// Copyright (c) 2007, 2008 BSQUARE Corporation. All rights reserved.

/*
================================================================================
*             Texas Instruments OMAP(TM) Platform Software
* (c) Copyright Texas Instruments, Incorporated. All Rights Reserved.
*
* Use of this software is controlled by the terms and conditions found
* in the license agreement under which this software has been supplied.
*
================================================================================
*/
//
//  Header:  eboot.h
//
//  This header file is comprised of component header files that defines
//  the standard include hierarchy for the bootloader. It also defines few
//  trivial constant.
//
#ifndef __EBOOT_H
#define __EBOOT_H

//------------------------------------------------------------------------------
#include "bsp.h"

#pragma warning(push)
#pragma warning(disable: 4201 4115)

#include <blcommon.h>
#include <nkintr.h>
#include <halether.h>
#include <fmd.h>
#include <bootpart.h>
#include <oal.h>
#include <oal_blmenu.h>

#pragma warning(pop)

#include "boot_args.h"
#include "args.h"


//------------------------------------------------------------------------------

#define EBOOT_VERSION_MAJOR         0
#define EBOOT_VERSION_MINOR         0

//------------------------------------------------------------------------------

typedef struct {
    UINT32 start;
    UINT32 length;
    UINT32 base;
} REGION_INFO_EX;

//------------------------------------------------------------------------------

#define DOWNLOAD_TYPE_UNKNOWN       0
#define DOWNLOAD_TYPE_RAM           1
#define DOWNLOAD_TYPE_BINDIO        2
#define DOWNLOAD_TYPE_XLDR          3
#define DOWNLOAD_TYPE_EBOOT         4
#define DOWNLOAD_TYPE_IPL           5
#define DOWNLOAD_TYPE_FLASHNAND     6
#define DOWNLOAD_TYPE_FLASHNOR      7
#define DOWNLOAD_TYPE_LOGO			8
#define DOWNLOAD_TYPE_EXT			9
#define DOWNLOAD_TYPE_XEL			10

//------------------------------------------------------------------------------
#define LOGO1						1
#define LOGO2						2
#define LOGO3						3
#define LOGO4						4

#define LOGO1_NB0_FILE				L"screen1.bmp"
#define LOGO2_NB0_FILE				L"screen2.bmp"
#define LOGO3_NB0_FILE				L"screen3.bmp"
#define LOGO4_NB0_FILE				L"screen4.bmp"
#define LOGO_NB0_FILE_LEN           11
#define LOGO_NB0_FILE_SIZE			((640 * 480 * 3) + 56)
#define MAX_LOGO_FILE_SIZE			0x100000

#define XEL_NB0_FILE				L"bloader.nb0"
#define XEL_NB0_FILE_LEN			11
#define XEL_NB0_FILE_SIZE			(IMAGE_XLDR_BOOTSEC_NAND_SIZE + IMAGE_EBOOT_BOOTSEC_NAND_SIZE + LOGO_NB0_FILE_SIZE)

#define NK_BIN_FILE					L"nk.bin"
#define NK_BIN_FILE_LEN				6

#define MAX_NB0_FILE_SIZE			(IMAGE_XLDR_BOOTSEC_NAND_SIZE + IMAGE_EBOOT_BOOTSEC_NAND_SIZE + (2*MAX_LOGO_FILE_SIZE))

#define INITIAL_CRC32				0x78807773	//"NPMI"
#define XLOADER_ROM_SIG_OFFSET		0x48

//------------------------------------------------------------------------------

typedef struct {
    OAL_KITL_TYPE bootDeviceType;
    UINT32 type;
    UINT32 numRegions;
    UINT32 launchAddress;
    REGION_INFO_EX region[BL_MAX_BIN_REGIONS];

    UINT32 recordOffset;
    UINT8  *pReadBuffer;
    UINT32 readSize;
} EBOOT_CONTEXT;

//------------------------------------------------------------------------------

extern BOOT_CFG g_bootCfg;
extern EBOOT_CONTEXT g_eboot;
extern OAL_KITL_DEVICE g_bootDevices[];
extern OAL_KITL_DEVICE g_kitlDevices[];

extern UINT32   g_ulFlashBase;

//------------------------------------------------------------------------------

VOID OEMMultiBinNotify(MultiBINInfo *pInfo);

//------------------------------------------------------------------------------

VOID   BLMenu(BOOL bForced, DWORD delay);    
BOOL   BLReadBootCfg(BOOT_CFG *pBootCfg);
BOOL   BLWriteBootCfg(BOOT_CFG *pBootCfg);
BOOL   BLReserveBootBlocks();
BOOL   BLConfigureFlashPartitions(BOOL bForceEnable);
BOOL   OSPartitionExists();
BOOL   XloaderExists();
BOOL   NKExists();
BOOL   BLShowLogo(DWORD dwLogo);
VOID   BLHideLogo();
UINT32 BLEthDownload(BOOT_CFG *pBootCfg, OAL_KITL_DEVICE *pBootDevices);
BOOL   BLEthReadData(ULONG size, UCHAR *pData);
VOID   BLEthConfig(BSP_ARGS *pArgs);
UINT32 BLSDCardDownload(WCHAR *filename);
BOOL   BLSDCardReadData(ULONG size, UCHAR *pData);
UINT32 BLFlashDownload(BOOT_CFG *pConfig, OAL_KITL_DEVICE *pBootDevices);
BOOL   BLSDCardReadLogo(WCHAR *filename, UCHAR *pData, DWORD size);
BOOL   BLSDCardFileExists(WCHAR *filename);
//UINT32 BLVAtoPA(UINT32 address);

UINT32 OALStringToUINT32(LPCWSTR psz);

DWORD CalcCrc32(DWORD Crc32, LPBYTE pData, DWORD count);
BOOL VerifyBloaderCRC();
DWORD GetFlashSectorSize();
  
//------------------------------------------------------------------------------

#endif
