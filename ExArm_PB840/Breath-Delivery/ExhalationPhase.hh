
#ifndef ExhalationPhase_HH
#define ExhalationPhase_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: ExhalationPhase - Implements the exhalation phase.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/ExhalationPhase.hhv   25.0.4.0   19 Nov 2013 13:59:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 011   By:   rhj    Date: 25-Oct-2010   SCR Number:   6694   
//  Project:  PROX
//  Description:
//       Removed the isEquivalent logic.
//
//  Revision: 010   By: rhj    Date: 10-May-2010    SCR Number: 6436   
//  Project:  PROX
//  Description:
//       Optimize leak comp related changes by minimizing the use of 
//       pow function calls.
//
//  Revision: 009   By: rhj    Date: 07-July-2008   SCR Number: 6435
//  Project:  LEAK
//  Description:
//       Leak Compensation project-related changes.
// 
//  Revision: 008  By: syw     Date:  26-Apr-1999    DR Number: 5322
//  Project:  ATC
//  Description:
//		Implement new exhalation phase algorithm. 
//
//  Revision: 007  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 006 By: sp    Date: 19-Feb-1997   DR Number: DCS 1780
//  	Project:  Sigma (R8027)
//		Description:
//			Add methods getFlowTarget and IsPulseFlow
//
//  Revision: 005 By: syw    Date: 12-Jul-1996   DR Number: DCS 1077
//  	Project:  Sigma (R8027)
//		Description:
//			Eliminate shutoff of base flow during pressure triggering.
//
//  Revision: 004 By: syw    Date: 12-Jul-1996   DR Number: DCS 1073
//  	Project:  Sigma (R8027)
//		Description:
//			Added relinquish control method.
//
//  Revision: 003 By: syw    Date: 26-Feb-1996   DR Number: DCS 600
//  	Project:  Sigma (R8027)
//		Description:
//			Controls Rev 04 changes:
//			- renamed methods and data members to be consistent with
//			  Controls Spec nomenclature.
//			- added new methods and data members.  
//
//  Revision: 002 By: syw    Date: 21-Feb-1996   DR Number: DCS 674
//  	Project:  Sigma (R8027)
//		Description:
//			Removed #ifndef around #include.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"

//@ Usage-Classes

#include "BreathPhase.hh"
#include <math.h>

//@ End-Usage

extern const Real32 PURGE_FLOW ;

class ExhalationPhase : public BreathPhase {
  public:
    ExhalationPhase( void) ;
    virtual ~ExhalationPhase( void) ;
    virtual void newBreath(void) ;
    virtual void newCycle(void) ;
    virtual void relinquishControl( const BreathTrigger& trigger) ;


    inline Real32 getFlowTarget(void) const;
    inline Real32 getLeakAdd(void) const;
    inline void setFlowTarget(Real32 flowTarget);
    inline void  resetLeakAdd();


	static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL, 
			  const char*       pPredicate = NULL) ;
  protected:
	virtual void updateFlowControllerFlags_( void) ;
  
  private:
    ExhalationPhase( const ExhalationPhase&) ;	// not implemented...
    void   operator=( const ExhalationPhase&) ;	// not implemented...

	//@ Data-Member: flowTarget_
	// base flow level
	Real32 flowTarget_ ;


	//@ Data-Member: Real32 leakAdded_
	// Used for Leak Compensation of bias flow.
    Real32 leakAdd_;

	//@ Data-Member: Real32 deltaLeakAdd_
	// Used for Leak Compensation of bias flow when autocycling occurs.
	Real32 deltaLeakAdd_;
} ;


// Inlined methods...
#include "ExhalationPhase.in"


#endif // ExhalationPhase_HH 


