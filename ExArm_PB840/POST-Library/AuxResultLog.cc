#include "stdafx.h"
//====================================================================
// This is a proprietary work to which Nellcor Puritan Bennett
// corporation of California claims exclusive right.  No part of this
// work may be used, disclosed, reproduced, sorted in an information
// retrieval system, or transmitted by any means, electronic,
// mechanical, photocopying, recording, or otherwise without the prior
// written permission of Nellcor Puritan Bennett Corporation.
//
//     Copyright (c) 1995, Nellcor Puritan Bennett Corporation
//=====================================================================


//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: AuxResultLog - POST Auxiliary Result Log
//---------------------------------------------------------------------
//@ Interface-Description
//  This class contains the methods for maintaining the auxiliary POST
//  results log contained in NOVRAM.  A static accessor returns the
//  reference to the object located in Kernel NOVRAM.
//---------------------------------------------------------------------
//@ Rationale
//  Contains methods for creating and maintaining POST auxiliary
//  results log.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/POST-Library/vcssrc/AuxResultLog.ccv   25.0.4.0   19 Nov 2013 14:16:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002   By: Gary Cederquist  Date: 15-AUG-1997  DR Number: 1875
//    Project:   Sigma   (R8027)
//    Description:
//       Added testable item annotations.
//
//  Revision: 001   By: Gary Cederquist  Date: 12-MAY-1997  DR Number: 2085
//    Project:   Sigma   (R8027)
//    Description:
//       Initial version
//
//=====================================================================

//=====================================================================
//
//      Includes...
//
//=====================================================================

#include "AuxResultLog.hh"
#include "postnv.hh"

//=====================================================================
//
//      Public Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: AuxResultLog [constructor]
//
//@ Interface-Description
//  Default constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
AuxResultLog::AuxResultLog()
{
    initialized_ = TRUE;
    // $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: AuxResultLog [destructor]
//
//@ Interface-Description
//  Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
AuxResultLog::~AuxResultLog()
{
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetPostAuxResultLog [static]
//
//@ Interface-Description
//  Returns a reference to the POST auxiliary results log stored in
//  NOVRAM.  Using this accessor, the object is self-initializing.
//  This static method checks if the object is initialized and if it
//  is not, constructs it in NOVRAM automatically.  This function is
//  normally called during the Kernel NOVRAM test so that should be
//  the only time it is constructed.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
AuxResultLog & 
AuxResultLog::GetPostAuxResultLog(void)
{
    AuxResultLog & auxResultLog = PKernelNovram->auxResultLog;

    if ( !auxResultLog.isInitialized() )
    {                                                       // $[TI1.1]
        new (&(PKernelNovram->auxResultLog)) AuxResultLog;
    }
                                                            // $[TI1.2]
    return auxResultLog;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: add
//
//@ Interface-Description
//  Adds the specified AuxResult to the AuxResultLog.  The log is a
//  circular log with NUM_AUX_RESULTS entries.  The next entry index
//  is adjusted after adding the specified entry.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
AuxResultLog::add( const AuxResult &  auxResult )
{
    verify();
    auxResult_[nextEntry_] = auxResult;
    nextEntry_ = ++nextEntry_ % AuxResultLog::NUM_AUX_RESULTS;
    // $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isInitialized
//
//@ Interface-Description
//  Returns TRUE if the AuxResultLog is initialized, otherwise FALSE.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
Boolean
AuxResultLog::isInitialized(void)
{
    // $[TI1]
    return  initialized_;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: verify
//
//@ Interface-Description
//  Verifies and corrects the integrity of the log.  Returns TRUE if
//  the log was in error and corrected in any way.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Checks the range of the nextEntry_ index.  Returns TRUE if the
//  index was out of range and corrected.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
Boolean
AuxResultLog::verify(void)
{
    if (   nextEntry_ >= 0 
        && nextEntry_ < AuxResultLog::NUM_AUX_RESULTS )
    {                                                       // $[TI1.1]
        return FALSE;
    }
    else
    {                                                       // $[TI1.2]
        nextEntry_ = 0;
        return TRUE;
    }
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getLastResult
//
//@ Interface-Description
//  Returns a pointer to the latest AuxResult contained in the log
//  corresponding to the specified test number.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Searches the log from the oldest entry to the newest, assigning the
//  return pointer if a matching test number is found in the log.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
const AuxResult * 
AuxResultLog::getLastResult( const Uint8 testNumber )
{
    AuxResult * pAuxResult = NULL;
    AuxResult * pTestAuxResult = NULL;

    for (Uint8 i=0; i<AuxResultLog::NUM_AUX_RESULTS; i++)
    {
        pTestAuxResult
             = &auxResult_[(nextEntry_ + i) % AuxResultLog::NUM_AUX_RESULTS];

        if ( pTestAuxResult->getTestNumber() == testNumber )
        {                                                       // $[TI1.1]
            pAuxResult = pTestAuxResult;
        }                                                       // $[TI1.2]
    }

    return pAuxResult;
}
