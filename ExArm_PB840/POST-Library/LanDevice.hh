#ifndef LanDevice_HH
#define LanDevice_HH
 
//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//====================================================================
 
//====================================================================
//@ Filename:  LanDevice - Ethernet Port Definitions
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/POST-Library/vcssrc/LanDevice.hhv   25.0.4.0   19 Nov 2013 14:16:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//====================================================================
 
// **** LAN
#define  P_LAN_PORT        ((volatile Uint32 *)0xFFBE8028)
#define  LAN_SELF_TEST_CMD (1)

#endif // LanDevice_HH

