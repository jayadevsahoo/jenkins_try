#ifndef Container_HH
#define Container_HH
//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1995, Puritan-Bennett Corporation
//====================================================================
//====================================================================
// Class: Container - A rectangular object capable of containing Drawables.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Foundations/vcssrc/Container.hhv   25.0.4.0   19 Nov 2013 14:11:42   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006   By: gdc    Date:  21-May-2007    SCR Number: 6330
//  Project: Trend
//  Description:
//	Removed SUN prototype code.
//
//  Revision: 005   By: gdc    Date:  25-Jan-2007    SCR Number: 6237
//  Project: Trend
//  Description:
//	Changed to accomodate derived classes with different sized 
//  Drawable lists.
//  
//  Revision: 004   By: gdc    Date:  29-Jun-1999    DCS Number: 5337
//  Project: 840 Neonatal
//  Description:
//	Optimized VGA library for Neonatal project to use BitBlt and Windows
//  Bitmap structures.
//
//  Revision: 003   By: sah    Date: 11-Jan-1999   DR Number: 5321
//  Project:  Sigma (R8027)
//  Description:
//      Changed 'SoftFault()' method to non-inline method.
//
//  Revision: 002  By: gdc      Date: 15-Feb-1996  DR Number: 674
//    Project:  Sigma (R8027)
//    Description:
//        Removed ifndef / endif around include directives
//
//  Revision: 001  By: yyy      Date: 01-Jan-1995  DR Number: NONE
//    Project:  Sigma (R8027)
//    Description:
//        Integration baseline.
//
//====================================================================

#include "Sigma.hh"
#include "GuiFoundation.hh"

//@ Usage-Classes
#include "Drawable.hh"
#include "Colors.hh"
#include "TemplateMacros.hh"
#include "SListR_Drawable_SM_DRAWABLE_LIST_NUM.hh"
//@ End-Usage

class Container : public Drawable
{
	//@ Friend: Drawable::appendTo()
	// Allows external access to the private setChild_() method of the
	// function of Container.  We do not wish to allow anyone else to
	// see setChild_(), because the Drawable does the other work of
	// setting the parent of the Drawable.
	friend void Drawable::appendTo(Container* pParentContainer);

	//@ Friend: Drawable::removeFromParent()
	// Allows external access to the private removeChild_() method.  We
	// do not wish to allow anyone else to see removeChild_(), because
	// the Drawable does the other work of setting the parent to NULL.
	friend void Drawable::removeFromParent(void);

public:
	Container(void);
	virtual ~Container(void);

	void addDrawable(Drawable* pDrawable);
	void removeDrawable(Drawable* pDrawable);
	void removeAllDrawables(void);
	

	virtual void setColor(Colors::ColorS);		// redefines Drawable::setColor()
	virtual void setFillColor(Colors::ColorS fillColor);
	virtual void setContentsColor(Colors::ColorS contentsColor);

	void sizeToFit(void);
	virtual const Drawable* findTouchedDrawable(const Area& touchArea,
												Int& numHits) const;
	virtual void evaluateVisible(void);
	virtual void evaluateBase(void);

	virtual void draw(const Area& clipArea );

	virtual Int convertToRealX(Int x);
	virtual Int convertToRealY(Int y);

	virtual void setDirectDraw(Boolean enableDirectDraw);
	Boolean isDisplayLockPending(void) const;

	void scroll(const Area & scrollArea, Int deltaX, Int deltaY);

	inline Colors::ColorS getFillColor(void) const;
	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32 lineNumber,
						  const char *pFileName  = NULL,
						  const char *pPredicate = NULL);

protected:
	Container(SListR_Drawable &rChildren);
	const SListR_Drawable *getChildren_(void) const;


private:
	Container(const Container&);		// Declared, not implemented.
	void operator=(const Container&);	// Declared, not implemented.

	void setChild_(Drawable* pDrawable);
	void removeChild_(Drawable* pDrawable);

	//@ Data-Member: fillColor_
	// Stores the fill ("background") color of this Container.  We could
	// use the Drawable::color_ to store the fill color for Containers,
	// however, a separate fillColor_ is used to make the implementation
	// of Containers consistent with the implementation of Box and
	// Triangle classes.  Thus, Container does not use Drawable::color_
	// at all.
	Colors::ColorS fillColor_;

	//@ Data-Member: contentsColor_
	// Holds the current/default color of the Drawables *directly* owned
	// by this Container.  This color does not apply to items owned by
	// child Containers.  When set, this acts as the default color for
	// the Container since all added Drawables are set to this color.
	Colors::ColorS contentsColor_;

	//@ Data-Member: children_
	// A linked list which holds references to all of the contained
	// Drawables.
	FixedSListR(Drawable, SM_DRAWABLE_LIST_NUM) children_;

	//@ Data-Member: rChildren_
	// A reference to the Drawable list used by this container
	// for normal containers, this references children_. For 
	// derived Containers, this may reference a Drawable list 
	// in the derived container.
	SListR_Drawable &rChildren_;
};

// Inlined methods
#include "Container.in"

#endif // Container_HH 
