//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file Semaphore.cpp
//----------------------------------------------------------------------------

//-------------------------------------
// I N C L U D E S
//-------------------------------------
#include "Semaphore.hh"
#include "CLogger.h"


//-------------------------------------
// C O D E
//-------------------------------------

//---------------------------
// Semaphore::Semaphore()
//---------------------------
Semaphore::Semaphore()
{
    m_Semaphore = CreateSemaphore( 
        NULL,           // default security attributes
        0,              // initial count
        8192,  // maximum count
        NULL);          // unnamed semaphore

    m_bActive = true;
}

//---------------------------
// Semaphore::~Semaphore()
//---------------------------
Semaphore::~Semaphore()
{
    m_bActive = false;

    CloseHandle(m_Semaphore);
}

//---------------------------
// Semaphore::post()
//---------------------------
bool Semaphore::post()
{
    if(m_bActive == true)
    {
        return (ReleaseSemaphore(m_Semaphore,1,NULL)!=0) && m_bActive;
    }

    return false;
}
   
//---------------------------
// Semaphore::wait()
//---------------------------
bool Semaphore::wait()
{

    if(m_bActive == true)
    {
        return (WaitForSingleObject(m_Semaphore,INFINITE)==WAIT_OBJECT_0)&&m_bActive;
    }
    return false;
}

//---------------------------
// Semaphore::trywait()
//---------------------------
bool Semaphore::trywait()
{
    if(m_bActive == true)
    {
        return (WaitForSingleObject(m_Semaphore,0L)==WAIT_OBJECT_0)&&m_bActive;
    }

    return false;
}

//---------------------------
// Semaphore::timedwait()
//---------------------------
bool Semaphore::timedwait(unsigned int time)
{
    if(m_bActive == true)
    {
        return (WaitForSingleObject(m_Semaphore,time * 1000)==WAIT_OBJECT_0)&&m_bActive;
    }
    return false;
}

