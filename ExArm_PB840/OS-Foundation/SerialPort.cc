#include "stdafx.h"
//====================================================================
// This is a proprietary work to which Nellcor Puritan Bennett
// corporation of California claims exclusive right.  No part of this
// work may be used, disclosed, reproduced, sorted in an information
// retrieval system, or transmitted by any means, electronic,
// mechanical, photocopying, recording, or otherwise without the prior
// written permission of Nellcor Puritan Bennett Corporation.
//
//     Copyright (c) 1997, Nellcor Puritan Bennett Corporation
//=====================================================================

// =========================== M O D U L E   D E S C R I P T I O N ====
//@ Class: SerialPort - Serial-Interface Serial Port Driver
//---------------------------------------------------------------------
//@ Interface-Description
//  This class encapsulates the methods that drive the serial port for
//  the Serial-Interface subsystem. The serial port is actually
//  controlled by the serial device driver and the logio interface in
//  the VRTX operating system, but this class provides a better
//  abstraction of the serial port and its functions than accessing
//  logio and the serial chip directly.  All logio and chip access is
//  isolated to this class so if another serial control chip were used,
//  only the private methods in this class would require change. This 
//  class provides methods to set serial communication parameters, read
//  from the serial port, write to the serial port and enable or disable
//  the loopback mode of the serial controller chip.
//---------------------------------------------------------------------
//@ Rationale
//  Provide an abstraction layer for serial port access.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  Callers of logio functions MUST have supervisor privileges, otherwise
//  a privilege violation exception is taken.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/OS-Foundation/vcssrc/SerialPort.ccv   25.0.4.0   19 Nov 2013 14:15:18   pvcs  $
//
//@ Modification-Log
// 
//  Revision: 009    By: rpr         Date: 31-March-2010 SCR Number: 6436
//    Project: PROX
//    Description:
//    Add GetCTS function,  returns the Clear to Send Signal
//
//  Revision: 008    By: gdc         Date: 11-Aug-2010 SCR Number: 6663
//    Project: API/MAT
//    Description:
//    Previous attempts to fix nonresponding port were only partially
//    successful. This modification uses the serial port device driver
//    in zi8250.c to reset the serial channel associated with the port
//    and reinitializes the logio charbufs. This modification also 
//    blocks all interrupts during modifications to the serial port since
//    blocking only serial interrupts resulted in buffer overrun errors
//    when another higher priority interrupt came in while serial 
//    interrupts were blocked. This modification was tested extensively
//    using the API VentSet interface.
// 
//  Revision: 007    By: gdc         Date: 11-Aug-2008 SCR Number: 6427
//    Project: 840S
//    Description:
//    Fixed non-communicating DCI port problem. IUS was getting set
//    without a corresponding interrupt and ISR processing that would
//    clear the IUS. Since the IUS was never cleared, the port stopped 
//    responding to external interrupts. This condition occured when the
//    serial port was being initialized while an external device 
//    continued to send data and create an interrupt condition on the 
//    serial port device. The fix resets the IUS in the reset() method
//    that gets called during initialization and whenever the serial
//    interface detects a "silent" port.
//    Moved this file to OS-Foundation from GUI-Serial-Interface to 
//    provide support for serial communications on the BD.
//  
//
//  Revision: 006    By: ljs              Date: 8-Dec-2003  DR Number: 6127
//  Project: Baseline
//  Description:
//      Modified to make Serial Communications more robust by:
//      - Ignore UART parity error when serial port configuration has parity==NONE;
//        But, log that error if using an Engineering/Production DataKey.
//
//  Revision: 005    By: gdc              Date: 11-Sep-2001  DR Number: 5946
//    Project: GuiComms
//    Description:
//       Masked all interrupts using cpu_interrupt_disable() when changing 
//       serial port parameters and resetting the serial port. Both of these
//       functions call logio calls that have proven problematic with any 
//       interrupt level enabled.
//
//  Revision: 004    By: quf              Date: 26-Jun-2001  DR Number: 5493
//    Project: GuiComms
//    Description:
//       Modified SoftFault to use encapsulated version of SERIAL_PORT_CLASS.
//
//  Revision: 003    By: quf              Date: 04-Dec-00    DR Number: 5784
//    Project: Baseline
//    Description:
//       Modified for Spacelabs monitor compatibility:
//       - return value of isReceiveError() now distinguishes between
//         parity and overflow errors.
//
//  Revision: 002    By: Gary Cederquist  Date: 24-Nov-1997  DR Number: 2605
//    Project: Sigma (R8027)
//    Description:
//       Complete restructuring of GUI-Serial-Interface to fix
//       several problems.
//
//  Revision: 001   By: Gary Cederquist  Date: 06-Nov-1997  Number: 2605
//  Project:  Sigma (R8027)
//  Description:
//      Created SerialPort class to resolve non-responsive interface.
//
//=====================================================================

#include <stdio.h>

#if defined(_WIN32_WCE)
// The define below will enable the new serial driver. 
// Please make sure a compatible BSP is loaded.
#define NEW_SERIAL_DRIVER	1
#include "sdk_ser.h"
#endif

#include "SerialPort.hh"
#include "OsTimeStamp.hh"
#include "Task.hh"
#include "SoftwareOptions.hh"

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SerialPort [constructor]
//
//@ Interface-Description
//  Constructs the SerialPort object using the specified device name.
//  For the VRTX OS, the device name for the serial port is either
//  "DEV_SERIAL_1" or "DEV_SERIAL_2". Opens the serial device for
//  access through the logio interface and initializes private pointers
//  to the serial device descriptors.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

SerialPort::SerialPort( const Uint8 deviceNumber )
	: interruptsDisabled_( FALSE )
	, baudRate_(9600)
	, dataBits_(8)
	, parity_(SerialPort::NONE)
	, stopBits_(1)
{

	InitComplete_ = FALSE;

	TCHAR comPortNumberString[15] = {0};
	
#if NEW_SERIAL_DRIVER
	{
		wsprintf(comPortNumberString,L"SER%d:",deviceNumber); // colon character is important and has to be followed by the COM #
	}
#else
	{
		wsprintf(comPortNumberString,L"COM%d:",deviceNumber);
	}
#endif

    serialPortHandle_ = CreateFile(comPortNumberString,             // open the serial port here
                                  GENERIC_READ|GENERIC_WRITE,
                                  NULL,
                                  NULL,
                                  OPEN_EXISTING,
                                  FILE_ATTRIBUTE_NORMAL,
                                  NULL);


	driverDebugMask_ = 0;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SerialPort [destructor]
//
//@ Interface-Description
//  Default destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SerialPort::~SerialPort( void )
{
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  reset
//
//@ Interface-Description
//  Reset the serial port hardware and device driver.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls the setParameters method that calls the logio_device_control
//  function to reset the serial channel and set the serial port
//  communications parameters. Uses the current serial port parameters.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
SerialPort::reset(void)
{
	setParameters(baudRate_,dataBits_,parity_,stopBits_);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  flushReceiveBuffer 
//
//@ Interface-Description
//  Reads data from the serial port device driver until no more data is 
//  available thereby flushing the device driver character receive buffer.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
SerialPort::flushReceiveBuffer(void)
{
	PurgeComm(serialPortHandle_,
		  PURGE_TXABORT | PURGE_RXABORT | PURGE_TXCLEAR |
		  PURGE_RXCLEAR);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  read
//
//@ Interface-Description
//  Reads the specified number of bytes from the serial port. Returns
//  when the specified number of bytes is read or when the specified
//  timeout (in milliseconds) occurs. Returns the actual number of
//  bytes read.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Uint
SerialPort::read( void * pBuffer, Uint count, Uint milliseconds )
{
	Uint32	bytesToRead = 0;
	char *		pBytes = (char *)pBuffer;
	uint32_t	bytesRead = 0;
	uint32_t    byteCount = 0;
	Uint32	accumulatedDelayTime = 0;


	if(InitComplete_ && serialPortHandle_ != INVALID_HANDLE_VALUE)
    {
#if NEW_SERIAL_DRIVER
		{
			if (!DeviceIoControl(serialPortHandle_, IOCTL_SER_READ, pBuffer, count, NULL, NULL, &bytesRead, NULL))
			{
				bytesRead = 0;
			}
		}		
#else
		{
			for ( ; ; )
			{
				// $[TI1.1]
				Int bytesToRead = count;

				//  return byte count updated only when status OK!
				if ( ReadFile(serialPortHandle_,pBytes, bytesToRead ,&byteCount,NULL) )
				{
					// $[TI2.1]
    				bytesRead += byteCount;
					count -= byteCount;
					pBytes += byteCount;
				}
				// $[TI2.1]

				if ( (count == 0) || (milliseconds <= 0))
				{
					// $[TI3.1]
					break;
				}
				// $[TI3.2]

				if ( accumulatedDelayTime > milliseconds )
				{
					// $[TI4.1]
					break;
				}
				// $[TI4.2]
				Task::Delay(0,DATA_READ_DELAY_TIME);
				
				// Keep track of how much time we've delayed
				accumulatedDelayTime += DATA_READ_DELAY_TIME;   			
			}

		}

#endif
	}

	if (!count && (GetLastError() == ERROR_IO_PENDING)) 
	{
		COMSTAT com_stat;
		uint32_t	err = 0;

		ClearCommError(serialPortHandle_, &err, &com_stat);
		
		PurgeComm(serialPortHandle_,
			  PURGE_TXABORT | PURGE_RXABORT | PURGE_TXCLEAR |
			  PURGE_RXCLEAR);
	}

	return bytesRead;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  write
//
//@ Interface-Description
//  Writes the specified number of bytes to the serial port. Returns
//  when the specified number of bytes is written or when the specified
//  timeout (in milliseconds) occurs. Returns the actual number of
//  bytes written to the device driver buffer.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Uint
SerialPort::write( const void * pBuffer, Uint count, Uint milliseconds )
{

	Uint32	bytesToWrite = 0;
	char *		pBytes = (char *)pBuffer;
	uint32_t	bytesWritten = 0;
	uint32_t    byteCount = 0;
	Uint32	accumulatedDelayTime = 0;
    
	if(InitComplete_ && serialPortHandle_ != INVALID_HANDLE_VALUE)
    {

#if NEW_SERIAL_DRIVER
		{
			if (!DeviceIoControl(serialPortHandle_, IOCTL_SER_WRITE, pBytes, count, NULL, NULL, &bytesWritten, NULL))
			{
				bytesWritten = 0;
			}
		}		
#else
		{
			for ( ; ; )
			{
				// $[TI1.1]
				Int bytesToWrite = count;

				//  return byte count updated only when status OK!
				if ( WriteFile(serialPortHandle_,pBytes, bytesToWrite ,&byteCount,NULL) )
				{
					// $[TI2.1]
    				bytesWritten += byteCount;
					count -= byteCount;
					pBytes += byteCount;
				}
				// $[TI2.1]

				if ( (count == 0) || (milliseconds <= 0))
				{
					// $[TI3.1]
					break;
				}
				// $[TI3.2]

				if ( accumulatedDelayTime > milliseconds )
				{
					// $[TI4.1]
					break;
				}
				// $[TI4.2]
				Task::Delay(0,DATA_READ_DELAY_TIME);
				
				// Keep track of how much time we've delayed
				accumulatedDelayTime += DATA_READ_DELAY_TIME;   			
			}

		}
#endif
	}

	if (!count && (GetLastError() == ERROR_IO_PENDING)) 
	{
		COMSTAT com_stat;
		uint32_t	err = 0;

		ClearCommError(serialPortHandle_, &err, &com_stat);
		
		PurgeComm(serialPortHandle_,
			  PURGE_TXABORT | PURGE_RXABORT | PURGE_TXCLEAR |
			  PURGE_RXCLEAR);
	}

	return bytesWritten;

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  wait
//
//@ Interface-Description
//  Waits for completion of any pending write operation up to the 
//  timeout (in milliseconds) specified. Returns TRUE if the operation
//  did not complete successfully, otherwise returns FALSE. 
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean
SerialPort::wait( Uint milliseconds )
{
	//Task::Delay(0,milliseconds) cannot be used since there are other objects 
	//which uses Serialport even before Tasking is switched On.
	Sleep(milliseconds);
	return FALSE;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  setParameters
//
//@ Interface-Description
//  Sets the serial port communication parameters to the specified values.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
SerialPort::setParameters(  Int32 baudRate
                          , Int32 dataBits
                          , SerialPort::Parity parity
                          , Int32 stopBits )
{
	baudRate_   = baudRate;
	parity_     = parity;
	dataBits_   = dataBits;
	stopBits_   = stopBits;

	if(serialPortHandle_ == INVALID_HANDLE_VALUE)                  
    {
		return;
    }
    else // open Serial Port successfully!
    {
	
		DCB serialPortParameterSetting = {0};
		serialPortParameterSetting.DCBlength = sizeof(DCB); 
		
		// try to retrieve the default serial port parameter settings
		if(!GetCommState(serialPortHandle_,&serialPortParameterSetting)) 
        {
			//TO DO
        }
        else // retrieve Serial Port parameter settings successfully!
        {
            // we need to specify the serial port parameter settings in which we are interested
            serialPortParameterSetting.BaudRate = baudRate_; 
            serialPortParameterSetting.ByteSize = dataBits_;
            serialPortParameterSetting.StopBits = stopBits_;
            serialPortParameterSetting.Parity   = parity_;

            // try to set the serial port parameter settings
			if(!SetCommState(serialPortHandle_,&serialPortParameterSetting)) 
            {
				//TO DO
			}
            else // set serial port parameter settings successfully!
            {
				COMMTIMEOUTS communicaitonPortTimeOut = {0};               
				
				// try to retrieve Serial Communication Port timeout setting
				if(!GetCommTimeouts(serialPortHandle_,&communicaitonPortTimeOut)) 
                {
					//TO DO
                }
                else // retrieve Serial Communication Port timeout settings successfully!
                {
                    // we need to specify the serial port timeout settings in which we are interested
                    communicaitonPortTimeOut.ReadIntervalTimeout         = MAXDWORD;  
                    communicaitonPortTimeOut.ReadTotalTimeoutMultiplier  = READ_TOTAL_TIMEOUT_MULTIPLIER;  
                    communicaitonPortTimeOut.ReadTotalTimeoutConstant    = READ_TOTAL_TIMEOUT_CONSTANT;    
                    communicaitonPortTimeOut.WriteTotalTimeoutMultiplier = WRITE_TOTAL_TIMEOUT_MULTIPLIER;  
                    communicaitonPortTimeOut.WriteTotalTimeoutConstant   = WRITE_TOTAL_TIMEOUT_CONSTANT;   
                    
                    if(!SetCommTimeouts(serialPortHandle_,&communicaitonPortTimeOut)) // try to set the serial timeout parameter settings
                    {
						//TO DO
                    }
                    else // set Serial Communication Port timeout settings successfully!
                    {
                        // We have to make sure that any data remaining in the serial port's input and output buffer from the previous
                        // communication get cleaned up (purged) completely before starting the new communication
                        if(!PurgeComm(serialPortHandle_, PURGE_RXCLEAR|PURGE_TXCLEAR))      
                        {
							// To DO
							// if we fail to discard any data remaining in the serial port's input and output buffer, return ??
                        }
                        else // purge communication data successfully!
                        {

#if NEW_SERIAL_DRIVER
                            {
                                uint32_t UseIoctlDoTxData = TRUE;
                                DeviceIoControl(serialPortHandle_, IOCTL_CHANGE_DEBUG, NULL, driverDebugMask_, NULL, UseIoctlDoTxData, NULL, NULL);
#ifndef __SPLIT__GUI__PC__

								// E600 TODO Check
								#define SERIAL_DRIVER_IST_THREAD_PRIORITY        (100)
								#define SERIAL_DRIVER_RX_DMA_THREAD_PRIORITY     (103)

								DeviceIoControl(serialPortHandle_, IOCTL_DISPATCH_THREAD_PRIORITY, NULL, SERIAL_DRIVER_IST_THREAD_PRIORITY, NULL, UseIoctlDoTxData, NULL, NULL);
                                DeviceIoControl(serialPortHandle_, IOCTL_ISTRXDMA_THREAD_PRIORITY, NULL, SERIAL_DRIVER_RX_DMA_THREAD_PRIORITY, NULL, UseIoctlDoTxData, NULL, NULL);
#endif
							}
#endif
							InitComplete_ = TRUE;
                        } 
                    }            
                }        
            }    
        }
    }

	AUX_CLASS_ASSERTION(InitComplete_ != FALSE, InitComplete_);

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetCTS
//
//@ Interface-Description
//  Gets the CTS line status of serial port
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean SerialPort::GetCTS(void)
{
	//TODO E600 port
	//it needs to be ported for Prox 
	return 0;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  disableInterrupts_
//
//@ Interface-Description
//  Disables interrupts.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses the operating system call to disable all interrupts. All 
//  interrupts are disabled instead of just serial port interrupts since
//  the serial port interrupt can be blocked for too long if another
//  higher priority interrupt, such as the ethernet controller, begins
//  its processing. This delay caused buffer overruns on the serial 
//  device that don't happen if all interrupts are blocked only briefly.
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
SerialPort::disableInterrupts_( void )
{
	//TODO E600 port
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  restoreInterrupts_
//
//@ Interface-Description
//  Restores interrupts.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Resets the interrupt processing level to what it was prior to 
//  disabling the interrupts.
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
SerialPort::restoreInterrupts_( void )
{
	//TODO E600 port
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  enableLoopback
//
//@ Interface-Description
//  Enables the serial controller loopback mode in which data written 
//  to the transmitter immediately shows up in the receiver.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
SerialPort::enableLoopback(void)
{
	//TODO E600 port
	disableInterrupts_();
/*

	// disable DPLL, BR source = PCLK
    writeReg_( WR0_R14, (WR14_DPDIS | WR14_PCLK) );

	// local loop back enable, BR gen enable, BR source = PCLK
    writeReg_( WR0_R14, (WR14_LLOOP | WR14_BRGENA | WR14_PCLK) );
*/

	restoreInterrupts_();

	// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  disableLoopback
//
//@ Interface-Description
//  Disables the serial controller loopback mode, restoring normal
//  operation.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
SerialPort::disableLoopback(void)
{
	//TODO E600 port
	disableInterrupts_();
/*

	// disable DPLL, BR source = PCLK
    writeReg_( WR0_R14, (WR14_LLOOP | WR14_BRGENA | WR14_PCLK) );

	// local loop back disable, BR gen enable, BR source = PCLK
    writeReg_( WR0_R14, (WR14_BRGENA | WR14_PCLK) );
*/

	restoreInterrupts_();
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  idleFunction_
//
//@ Interface-Description
//  This function provides the idle activity for the task when waiting
//  for serial port activity. Since the serial interface task is the
//  lowest priority task in the system, this function need only yield 
//  to another lowest priority task that might be ready to run.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
SerialPort::idleFunction_(void)
{
	Task::Yield();   

	// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//@ Interface-Description
// Implicitly gain execution control when a software assertion macro
// detects and error.
//---------------------------------------------------------------------
//@ Implementation-Description
// Automatically invoked when CLASS_ASSERTION and CLASS_PRE_CONDITON
// macros (among others) detect a fault.  We just call FaultHandler::SoftFault
// to display pertinent information to the user.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

void
SerialPort::SoftFault(const SoftFaultID softFaultID,
                      const Uint32	lineNumber,
                      const char *	pFileName,
                      const char *	pPredicate)
{
	FaultHandler::SoftFault(softFaultID, OS_FOUNDATION,
		OsFoundation::SERIAL_PORT, 
		lineNumber, pFileName, pPredicate);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  bytesInReceiveBuffer
//
//@ Interface-Description
// The number of bytes received by the serial provider but not yet read 
// by a ReadFile operation.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

Uint32 
SerialPort::bytesInReceiveBuffer(void)
{

	Uint32 ret = 0;

	COMSTAT com_stat;
	uint32_t errors;

	if(ClearCommError(serialPortHandle_, &errors, &com_stat))
	{
		ret = com_stat.cbInQue;
	}
		
	return ret;
	
}


//#############################################################################
//
//  SerialCommunication_t::SetCommunicationBaudRate -  
//
//#############################################################################
Boolean SerialPort::SetCommunicationBaudRate(Uint32 BaudRate)
{
    DCB SerialPortParameterSetting = {0};
    
    SerialPortParameterSetting.DCBlength = sizeof(DCB);     

    if(!InitComplete_ || serialPortHandle_ == INVALID_HANDLE_VALUE)
    {
        return false;
    }

	if(!GetCommState(serialPortHandle_,&SerialPortParameterSetting)) // try to retrieve the default serial port parameter settings
    {
        DEBUGMSG(true,(_T("Failed to retrieve Serial Port parameter settings!\r\n")));
        return false;
    }
    else // retrieve Serial Port parameter settings successfully!
    {
        // we need to specify the serial port parameter settings in which we are interested
        SerialPortParameterSetting.BaudRate = BaudRate; 

        if(!SetCommState(serialPortHandle_,&SerialPortParameterSetting)) // try to set the serial port parameter settings
        {
			DWORD err = GetLastError();
            DEBUGMSG(true,(_T("Failed to set Serial Port parameter settings!\r\n")));
            return false;
		}
	}

	return true;
}
