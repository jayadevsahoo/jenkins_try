#include "stdafx.h"
#ifdef SIGMA_GUI_CPU

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1994, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: MoreDataSubScreen - Subscreen for displaying supplemental Patient
//								Data.
//---------------------------------------------------------------------
//@ Interface-Description
// This class is a container, derived from the GUI-Foundation Container
// class.  This class contains textual fields, numeric fields, and a GUI
// Setting handle object.  This class inherits Patient Data callbacks
// from the abstract PatientDataTarget class.
// 
// The following measured data parameters are displayed:
// >Von
//	1.	Eend Exhalation Pressure
//	2.	Exhaled spontaneous minute volume.
//  3.  Delivered O2
//  4.  Insp spont tidal volume
//  5.  End inspiratory pressure
// >Voff
//
// This area relies on the data provided by the Patient Data subsystem
// for the measured patient data values.  Also, this area watches the
// 'mode' setting maintained by the Settings subsystem (the Exhaled
// Spontaneous Minute Volume and Inspiratory Spontaneous Tidal Volume 
// are not displayed when the mode is A/C and when in apnea or safety ventilation.
//---------------------------------------------------------------------
//@ Rationale
// This class is dedicated to managing the display of the
// MoreDataSubScreen on the UpperSubScreenArea.
//---------------------------------------------------------------------
//@ Implementation-Description
// The Patient Data subsystem notifies this area when updated values are
// available.  The numeric measured values are updated once per breath.
// Each data field has the capability of hiding the numeric value when
// there is no data available or flashing the numeric value when the
// data reaches a built-in limit.
//
// The various display objects for all three modes are added to the
// Container when it is constructed.  When the value of the 'mode'
// setting maintained by the Settings subsystem is A/C, the object
// associated with 'exhaled spontaneous minute volume' and inspiratory 
// Spontaneous Tidal Volume are  hidden since they are not applicable.
//---------------------------------------------------------------------
//@ Fault-Handling
// None.
//---------------------------------------------------------------------
//@ Restrictions
// None.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/MoreDataSubScreen.ccv   25.0.4.0   19 Nov 2013 14:08:08   pvcs  $
//
//@ Modification-Log
//
//  Revision: 039   By: rhj    Date: 05-Apr-2011   SCR Number: 6759
//  Project:  PROX
//  Description:
//      Added the missing wye symbol for patient data items f/Vt 
//      and Vdot e spont  
//  
//  Revision: 038   By: rhj    Date: 15-Mar-2011   SCR Number: 6755 
//  Project:  PROX
//  Description:
//      Display 840's volume data during waiting for patient 
//      connect even if prox is enabled.
//  
//  Revision: 037   By: rhj    Date: 22-Dec-2010   SCR Number: 6631
//  Project:  PROX
//  Description:
//      Display 840's volume data during PROX startup.
//  
//  Revision: 036   By: rhj    Date: 10-Sept-2010    SCR Number: 6631
//  Project: PROX
//  Description:
//      Modified to display Vti-y after 840 is retrieving PROX data.
//
//  Revision: 035   By: rhj    Date: 18-Feb-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//      MOdified to display Vti-y symbol when PROX is enabled.
//
//  Revision: 034   By: rhj    Date: 15-Apr-2009    SCR Number: 6448
//  Project:  840S2
//  Description:
//      Moved Vleak to row three and LEAK to row five.
//
//  Revision: 033   By: rhj    Date: 03-Dec-2008    SCR Number: 6443
//  Project:  840S
//  Description:
//      Fixed SCR 6443 -- Vti/Vtl field in More Patient Data is not 
//      completely erased.
//
//  Revision: 032   By: gdc    Date: 24-Sep-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal non-invasive CPAP.
//
//  Revision: 031   By: gdc   Date:  16-Sep-2008    SCR Number: xxxx
//  Project:  840N
//  Description:
//	    Hide PEEP and Vte,Spont in CPAP mode. 
//	    Removed touch screen "drill-down" work-around.
// 
//  Revision: 030   By: rhj    Date: 07-July-2008   SCR Number: 6435
//  Project:  LEAK
//  Description:
//       Leak Compensation project-related changes:
//       Added iLEAK, LEAK, %LEAK, and modified Vti to display it 
//       when there is a pressure-based breath.
//       
//  Revision: 029   By: rhj    Date:  10-May-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//      RESPM related changes.
//   
//  Revision: 028   By: gdc    Date:  15-Feb-2005    DR Number: 6144
//  Project:  NIV1
//  Description:
//      DCS 6144 - NIV1
//      added PEEP display for NIV
//
//  Revision: 027   By: emccoy Date:  27-July-2004    DR Number: 6138
//  Project:  PAV3
//  Description:
//      Project PAV3 DCS 6138
//      added support for ft/vte/kg
//
//  Revision: 026   By: ccyang Date:  11-Sept-2003 DR Number: 6114
//  Project:  PAV
//  Description:
//     SDCR6114 update PA requirement tracing
//
//  Revision: 025   By: ccyang Date:  27-Aug-2003 DR Number: 6089
//  Project:  PAV
//  Description:
//     fixed SCR_6089, bogus Vti Spont value on start up.
//
//  Revision: 024   By: jja Date:  23-Apr-2002 DR Number: 5806
//  Project:  VCP
//  Description:
//      Corrected display of SPONT_INSP_TIME_ITEM values following occlusion, 
//      apnea, etc.
//      Revised to proper display following power fail.
//
//  Revision: 023   By: jja Date:  16-Apr-2002 DR Number: 5860
//  Project:  VCP
//  Description:
//      Corrected VC+/VS Startup display persistence on settings change
//
//  Revision: 022   By: heatherw Date:  08-Apr-2002 DR Number: 5608
//  Project:  VCP
//  Description:
//      Added a method to handle setting applicabiltiy.  This method
//      was needed to handle settings after a reset condition.
//
//  Revision: 021   By: jja   Date:  15-Mar-2002    DR Number: 5790
//  Project:  VCP
//  Description:
//      Create method for handling lable of inspired volume parameter (Vti)
// 	and general clean up/fix up of code to handle the numerous development
//	bugs reported related to incorrect data displays, incorrect updates. etc.
//      Revise names for consistency now that Vti applies to mand breaths also.
//
//  Revision: 020   By: jja   Date:  15-Mar-2002    DR Number: 5860
//  Project:  VCP
//  Description:
//      Handle VC+/VS Startup messages.
//
//  Revision: 019   By: heatherw   Date:  04-Mar-2002    DR Number: 5790
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      *  added new labels for MAND and for the combination of MAND and
//         SPONT which is just Vti. These labels apply when the MandType
//         is VC+ and the when the SupportType and Mode are either
//         ATC or SIMV.  These changes occured in 'updateShowStates()'.
//      *  added a case for MAND_TYPE batch changes in 'batchSettingUpdate()'.
//
//  Revision: 018   By: sah   Date:  16-Aug-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      *  added new SPONT-based data items:  inspiratory time, inspiratory
//         time ratio, and rapid/shallow breathing index
//      *  reorganized initialization to better indicate placement of
//         data items on screen
//      *  added a high-level, state-management method, 'updateShowStates_()',
//         and a method for updating the various data values,
//         'updateDataValue_()'
//
//  Revision: 017   By: sah   Date:  23-May-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//      PAV project-related changes:
//      *  added new PAV-based data items:  compliance, resistance,
//         elastance and work-of-breathing
//
//  Revision: 016  By:  sah	   Date:  10-Apr-2000    DCS Number: 5691
//  Project:  NeoMode
//  Description:
//      Removed 'PEEP' from this subscreen; PEEP is now back in VPDA.
//
//  Revision: 015  By:  yyy	   Date:  29-Jun-1998    DCS Number: 5327
//  Project:  NeoMode
//  Description:
//      Initial version.
//
//  Revision: 014  By: yyy      Date: 22-Mar-1998  DR Number: 5274
//    Project:  ATC (R8027)
//    Description:
//     Changed numeric number fileds' space holder to meet the patient
//	   data menager's definition.  Changed the VALUE_WIDTH to increase
//     the width of the numeric field.
//
//  Revision: 013  By: hct    Date: 20-Jan-1999   DR Number: 5322
//  Project:  ATC
//  Description:
//      Added ATC functionality.
//
//  Revision: 012  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 010  By: yyy      Date: 05-Jan-1998  DR Number: 5286
//    Project:  BiLevel (R8027)
//    Description:
//     Added a previousPData_ to eliminate multiple update for breath type.
//	   Deleted spont minute volume unit setShow() call due to breath type
//	   data change occured.
//
//  Revision: 009  By: yyy      Date: 05-Jan-1998  DR Number: 
//    Project:  BiLevel (R8027)
//    Description:
//     Initial release for BiLevel adding software option.
//     Merged /840/Baseline/GUI-Applications/vcssrc/MoreDataSubScreen.ccv   1.48.1.0   07/30/98 10:16:50   gdc
//
//  Revision: 008  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 007  By:  yyy    Date:  10-Oct-97    DR Number: 2523
//    Project:  Sigma (R8027)
//    Description:
//      Query the status of the Mean Circuit Pressure Data Id in deciding
//		whether to display it.
//
//  Revision: 006  By:  yyy    Date:  10-Sep-97    DR Number: 2169
//    Project:  Sigma (R8027)
//    Description:
//      Removed obsolete SRS references.
//
//  Revision: 005  By: yyy      Date: 08-Sep-1997  DR Number: 2412
//    Project:  Sigma (R8027)
//    Description:
//      Registered to the BdEventRegistrar to allow to process the event.
//
//  Revision: 004  By: yyy      Date: 04-Jun-1997  DR Number: 2184
//    Project:  Sigma (R8027)
//    Description:
//      Check the Flow sensor is enabled or not whenever we first activate
//		the screen to avoid of displaying FiO2 value when shouldn't.
//
//  Revision: 003  By: yyy      Date: 27-May-1997  DR Number: 2115
//    Project:  Sigma (R8027)
//    Description:
//      Forced to reset the patient data reserver whenever the displayed
//		patient data are forced to be hidden.
//
//  Revision: 002  By: yyy      Date: 15-May-1997  DR Number: 1878
//    Project:  Sigma (R8027)
//    Description:
//      Added check for ALARM_RESET BD event so we can refresh the 
//		subscreen to display the appropriate labels.
//
//  Revision: 001  By:  wks    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "MoreDataSubScreen.hh"

//@ Usage-Classes
#include "PatientDataRegistrar.hh"
#include "BreathDatumHandle.hh"
#include "Fio2EnabledValue.hh"
#include "MiscStrs.hh"
#include "ModeValue.hh"
#include "UpperScreen.hh"
#include "UpperSubScreenArea.hh"
#include "BdEventRegistrar.hh"
#include "BdGuiEvent.hh"
#include "EventData.hh"
#include "GuiEventRegistrar.hh"
#include "MathUtilities.hh"
#include "SupportTypeValue.hh"
#include "MandTypeValue.hh"
#include "SettingContextHandle.hh"
#include "SafetyPcvSettingValues.hh"
#include "PatientDataMgr.hh"
#include "VtpcvState.hh"
#include "PavState.hh"
#include "SoftwareOptions.hh"
#include "Setting.hh"
#include "Precision.hh"
#include "LeakCompEnabledValue.hh"
#include "ProxEnabledValue.hh"
//@ End-Usage

//@ Code...

static const Int32 ROW1_Y_ =   6;
static const Int32 ROW2_Y_ =  43;       // + 37 pixel spread...
static const Int32 ROW3_Y_ =  80;
static const Int32 ROW4_Y_ = 117;
static const Int32 ROW5_Y_ = 154;
static const Int32 ROW6_Y_ = 191;
static const Int32 ROW7_Y_ = 228;

static const Int32 COL1_LABEL_X_ =  10;
static const Int32 COL1_VALUE_X_ =  60;		// LABEL + 50
static const Int32 COL1_UNIT_X_  = 160;		// VALUE + 100

static const Int32 COL2_LABEL_X_ = 215;
static const Int32 COL2_VALUE_X_ = 265;
static const Int32 COL2_UNIT_X_  = 365;

static const Int32 COL3_LABEL_X_ = 440;
static const Int32 COL3_VALUE_X_ = 490;
static const Int32 COL3_UNIT_X_  = 590;

static const Int32 VALUE_WIDTH_  = 90;
static const Int32 VALUE_HEIGHT_ = 35;


static const Int32 RM_BUTTON_X_ = 535;    
static const Int32 RM_BUTTON_Y_ = 230;    
static const Int32 RM_BUTTON_WIDTH_ = 90; 
static const Int32 RM_BUTTON_HEIGHT_ = 40;   
static const Int32 RM_BUTTON_BORDER_ = 3;

static  Boolean  IS_VCP_ACTIVE        = FALSE;
static  Boolean  IS_VCV_ACTIVE        = FALSE;
static  Boolean  IS_VS_ACTIVE         = FALSE;
static  Boolean  IS_IN_SPONT_MODE     = FALSE;
static  Boolean  IS_SPONT_TYPE_APPLIC = FALSE;
static  Boolean  IS_PA_ACTIVE         = FALSE;
static  Boolean  IS_NIV_ACTIVE        = FALSE;
static  Boolean  IS_LEAK_COMP_ACTIVE        = FALSE;
static  Boolean  IS_APNEA_VCV_ACTIVE  = FALSE;
static  Boolean  IS_AC_ACTIVE         = FALSE;
static  Boolean  IS_PROX_ACTIVE = FALSE;
static  Boolean  IS_PROX_STARTUP = FALSE;

Uint32 MoreDataSubScreen::lastDisplay_ = MOREDATASUBSCREEN;

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: MoreDataSubScreen()  [Constructor]
//
//@ Interface-Description
// Constructor.  The 'pSubScreenArea' parameter is the pointer to the
// parent SubScreenArea which contains this subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Initializes all members and initializes the colors, sizes, and
// positions of all graphical objects.  Adds all graphical objects to
// the parent container for display.  Registers with the
// ContextObserver for notification of changes to the 'mode'
// setting.  Also, registers for updates with the Patient Data
// subsystem.
//
// $[01006] All shorthand symbols in the More Data subscreen must display
//			their meaning in the Message Area ...
// $[01177] Display requirements for data same as for VPDA.
//---------------------------------------------------------------------
//@ PreCondition
// The given 'pSubScreenArea' pointer must not be NULL.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

MoreDataSubScreen::MoreDataSubScreen(SubScreenArea* pSubScreenArea) :
	SubScreen(pSubScreenArea),

	endInspiratoryPressureLabel_(MiscStrs::TOUCHABLE_END_INSP_PRES_LABEL,
								 MiscStrs::TOUCHABLE_END_INSP_PRES_MSG),
	endInspiratoryPressureValue_(TextFont::TWENTY_FOUR, 5, RIGHT),
	endInspiratoryPressureUnit_(
								GuiApp::PressUnits() == PressUnitsValue::CMH2O_UNIT_VALUE ?
								MiscStrs::TOUCHABLE_PRESSURE_UNIT :
								MiscStrs::TOUCHABLE_PRESSURE_HPA_UNIT), 	// $[TI3], $[TI4]

	endExhalationPressureLabel_(MiscStrs::TOUCHABLE_END_EXH_PRES_LABEL,
							    MiscStrs::TOUCHABLE_END_EXH_PRES_MSG),
	endExhalationPressureValue_(TextFont::TWENTY_FOUR, 5, RIGHT),
	endExhalationPressureUnit_(
								GuiApp::PressUnits() == PressUnitsValue::CMH2O_UNIT_VALUE ?
								MiscStrs::TOUCHABLE_PRESSURE_UNIT :
								MiscStrs::TOUCHABLE_PRESSURE_HPA_UNIT), 	// $[TI5.1], $[TI5.2]

	spontMinuteVolumeLabel_(MiscStrs::TOUCHABLE_SPONT_MINUTE_VOL_LABEL,
							MiscStrs::TOUCHABLE_SPONT_MINUTE_VOL_MSG),
	spontMinuteVolumeValue_(TextFont::TWENTY_FOUR, 4, RIGHT),
	spontMinuteVolumeUnit_(MiscStrs::TOUCHABLE_SPONT_MINUTE_VOL_UNIT),

	deliveredOxygenPercentLabel_(MiscStrs::TOUCHABLE_DEL_O2_PERCENT_LABEL,
								 MiscStrs::TOUCHABLE_DEL_O2_PERCENT_MSG),
	deliveredOxygenPercentValue_(TextFont::TWENTY_FOUR, 3, RIGHT),
	deliveredOxygenPercentUnit_(MiscStrs::TOUCHABLE_DEL_O2_PERCENT_UNIT),

	inspTidalVolLabel_(MiscStrs::TOUCHABLE_INSP_SPONT_TIDAL_VOL_LABEL,
							MiscStrs::TOUCHABLE_INSP_SPONT_TIDAL_VOL_MSG),
	inspTidalVolValue_(TextFont::TWENTY_FOUR, 4, RIGHT),
	inspTidalVolUnit_(MiscStrs::TOUCHABLE_INSP_SPONT_TIDAL_VOL_UNIT),

	//-----------------------------------------------------------------
	// $[PA01001] -- display PAV-based compliance, resistance, elastance
	//               and work-of-breathing...
	//-----------------------------------------------------------------

	pavComplianceLabel_(MiscStrs::TOUCHABLE_PAV_COMPLIANCE_LABEL,
							MiscStrs::TOUCHABLE_PAV_COMPLIANCE_MSG),
	pavComplianceValue_(TextFont::TWENTY_FOUR, 3, RIGHT),
	pavComplianceUnit_(
             GuiApp::PressUnits() == PressUnitsValue::CMH2O_UNIT_VALUE ?
                          MiscStrs::TOUCHABLE_PAV_COMPLIANCE_UNIT :
                          MiscStrs::TOUCHABLE_PAV_COMPLIANCE_HPA_UNIT),

	pavComplianceMarker_(MiscStrs::LARGE_MARKER_LABEL),

	pavElastanceLabel_(MiscStrs::TOUCHABLE_PAV_ELASTANCE_LABEL,
							MiscStrs::TOUCHABLE_PAV_ELASTANCE_MSG),
	pavElastanceValue_(TextFont::TWENTY_FOUR, 3, RIGHT),
	pavElastanceUnit_(
             GuiApp::PressUnits() == PressUnitsValue::CMH2O_UNIT_VALUE ?
                MiscStrs::TOUCHABLE_PAV_ELASTANCE_UNIT:
                MiscStrs::TOUCHABLE_PAV_ELASTANCE_HPA_UNIT),

    pavElastanceMarker_(MiscStrs::LARGE_MARKER_LABEL),
	
    pavResistanceLabel_(MiscStrs::TOUCHABLE_PAV_RESISTANCE_LABEL,
							MiscStrs::TOUCHABLE_PAV_RESISTANCE_MSG),
	pavResistanceValue_(TextFont::TWENTY_FOUR, 3, RIGHT),
	
	pavResistanceUnit_(
             GuiApp::PressUnits() == PressUnitsValue::CMH2O_UNIT_VALUE ?
		MiscStrs::TOUCHABLE_PAV_RESISTANCE_UNIT :
		MiscStrs::TOUCHABLE_PAV_RESISTANCE_HPA_UNIT),
             
	pavResistanceMarker_(MiscStrs::LARGE_MARKER_LABEL),

	totalResistanceLabel_(MiscStrs::TOUCHABLE_TOTAL_RESISTANCE_LABEL,
	MiscStrs::TOUCHABLE_TOTAL_RESISTANCE_MSG),
	totalResistanceValue_(TextFont::TWENTY_FOUR, 3, RIGHT),
	totalResistanceUnit_(
             GuiApp::PressUnits() == PressUnitsValue::CMH2O_UNIT_VALUE ?
		MiscStrs::TOUCHABLE_PAV_RESISTANCE_UNIT:
		MiscStrs::TOUCHABLE_PAV_RESISTANCE_HPA_UNIT),

	totalResistanceMarker_(MiscStrs::LARGE_MARKER_LABEL),

	totalWorkOfBreathingLabel_(MiscStrs::TOUCHABLE_TOTAL_WORK_OF_BREATHING_LABEL_LARGE,
			MiscStrs::TOUCHABLE_TOTAL_WORK_OF_BREATHING_MSG),
	totalWorkOfBreathingValue_(TextFont::TWENTY_FOUR, 3, RIGHT),
	totalWorkOfBreathingUnit_(MiscStrs::TOUCHABLE_WORK_OF_BREATHING_UNIT),

	pavIntrinsicPeepLabel_(MiscStrs::TOUCHABLE_LARGE_INTRINSIC_PEEP_LABEL,
							MiscStrs::TOUCHABLE_PAV_INTRINSIC_PEEP_MSG),
	pavIntrinsicPeepValue_(TextFont::TWENTY_FOUR, 3, RIGHT),
	pavIntrinsicPeepUnit_(
			GuiApp::PressUnits() == PressUnitsValue::CMH2O_UNIT_VALUE  ?
			MiscStrs::TOUCHABLE_PRESSURE_UNIT :
			MiscStrs::TOUCHABLE_PRESSURE_HPA_UNIT),

	//-----------------------------------------------------------------
	// $[PA01006] -- display PAV Startup messages
	//-----------------------------------------------------------------
	pavStatusMsg_(MiscStrs::PAV_STARTUP_STATUS_LARGE_MSG),

	//-----------------------------------------------------------------
	// $[VC01008] -- display VCP/VS Startup messages
	// $[VC01009]
	//-----------------------------------------------------------------

	vcpStatusMsg_(MiscStrs::VCP_STARTUP_STATUS_LARGE_MSG),
	vsStatusMsg_(MiscStrs::VS_STARTUP_STATUS_LARGE_MSG),

	//-----------------------------------------------------------------
	//  -- Display Prox Startup message
	//-----------------------------------------------------------------
	proxStatusMsg_(MiscStrs::PROX_STARTUP_LARGE_MSG),


	//-----------------------------------------------------------------
	// $[VC01004] -- display spontaneous rapid/shallow breathing index,
	//               inspiratory time ratio and inspiratory time...
	//-----------------------------------------------------------------

	spontPercentTiLabel_(MiscStrs::TOUCHABLE_SPONT_PERCENT_TI_LABEL,
							MiscStrs::TOUCHABLE_SPONT_PERCENT_TI_MSG),
	spontPercentTiValue_(TextFont::TWENTY_FOUR, 3, RIGHT),

	spontFvRatioLabel_(MiscStrs::TOUCHABLE_SPONT_FV_RATIO_LABEL,
							MiscStrs::TOUCHABLE_SPONT_FV_RATIO_MSG),
	spontFvRatioValue_(TextFont::TWENTY_FOUR, 3, RIGHT),

	spontInspTimeLabel_(MiscStrs::TOUCHABLE_SPONT_INSP_TIME_LABEL,
							MiscStrs::TOUCHABLE_SPONT_INSP_TIME_MSG),
	spontInspTimeValue_(TextFont::TWENTY_FOUR, 3, RIGHT),
	spontInspTimeUnit_(MiscStrs::TOUCHABLE_SPONT_INSP_TIME_UNIT),

//------------------------------------------------------------------
// PAV --display f/vte/kg
   normalizeSpontFvRatioLabel_(MiscStrs::TOUCHABLE_NORMALIZE_SPONT_FV_RATIO_LABEL,
                        MiscStrs::TOUCHABLE_NORMALIZE_SPONT_FV_RATIO_MSG),
                        
   normalizeSpontFvRatioValue_(TextFont::TWENTY_FOUR, 3, RIGHT),
   
   //--------------------------------------------------------------------


	//-----------------------------------------------------------------
	// $[LC01003] -- display iLeak, LEAK, Percent Leak, 
	//-----------------------------------------------------------------

	percentLeakLabel_(MiscStrs::TOUCHABLE_PERCENT_LEAK_LABEL,
					  MiscStrs::TOUCHABLE_PERCENT_LEAK_MSG),
	percentLeakValue_(TextFont::TWENTY_FOUR, 5, RIGHT),
	percentLeakUnit_( MiscStrs::TOUCHABLE_PERCENT_LEAK_UNIT), 	

	exhLeakRateLabel_(MiscStrs::TOUCHABLE_EXH_LEAK_RATE_LABEL,
					  MiscStrs::TOUCHABLE_EXH_LEAK_RATE_MSG),
	exhLeakRateValue_(TextFont::TWENTY_FOUR, 5, RIGHT),
	exhLeakRateUnit_( MiscStrs::TOUCHABLE_EXH_LEAK_RATE_UNIT), 	
	inspLeakVolLabel_(MiscStrs::TOUCHABLE_INSP_LEAK_VOL_LABEL,
					  MiscStrs::TOUCHABLE_INSP_LEAK_VOL_MSG),
	inspLeakVolValue_(TextFont::TWENTY_FOUR, 5, RIGHT),
	inspLeakVolUnit_( MiscStrs::TOUCHABLE_INSP_LEAK_VOL_UNIT), 	


    RmButton_(RM_BUTTON_X_, RM_BUTTON_Y_,
			RM_BUTTON_WIDTH_, RM_BUTTON_HEIGHT_,
			Button::LIGHT, RM_BUTTON_BORDER_, Button::SQUARE,
			Button::NO_BORDER, MiscStrs::RM_BUTTON_TITLE),
    isProxInop_(FALSE),
	previousPData_(-1)
{
	CALL_TRACE("MoreDataSubScreen::MoreDataSubScreen(pSubScreenArea)");
	CLASS_PRE_CONDITION(NULL != pSubScreenArea);


	// Size and position the area
	setX(0);
	setY(0);
	setWidth(UpperSubScreenArea::CONTAINER_WIDTH);
	setHeight(UpperSubScreenArea::CONTAINER_HEIGHT);

	// Set subscreen background color
	setFillColor(Colors::BLACK);

 
    StringId  PRESS_UNITS =
				(GuiApp::PressUnits() == PressUnitsValue::CMH2O_UNIT_VALUE)
				 ? L"cmH{S:2}O"  // $[TI6.1] 
				 : L"hPa";       // $[TI6.2]
	//
	// Set color and add label fields.
	//

	//=================================================================
	// Column #1, Row #1...
	//=================================================================
	deliveredOxygenPercentLabel_.setColor(Colors::WHITE);
	deliveredOxygenPercentLabel_.setX(::COL1_LABEL_X_);
	deliveredOxygenPercentLabel_.setY(::ROW1_Y_);
	addDrawable(&deliveredOxygenPercentLabel_);
 
	deliveredOxygenPercentUnit_.setColor(Colors::LIGHT_BLUE);
	deliveredOxygenPercentUnit_.setX(::COL1_UNIT_X_);
	deliveredOxygenPercentUnit_.setY(::ROW1_Y_);
	addDrawable(&deliveredOxygenPercentUnit_);

	deliveredOxygenPercentValue_.setValue(100);
	deliveredOxygenPercentValue_.setColor(Colors::WHITE);
	deliveredOxygenPercentValue_.setX(::COL1_VALUE_X_);
	deliveredOxygenPercentValue_.setY(::ROW1_Y_);
	deliveredOxygenPercentValue_.setWidth(::VALUE_WIDTH_);
	deliveredOxygenPercentValue_.setHeight(::VALUE_HEIGHT_);
	addDrawable(&deliveredOxygenPercentValue_);


	//-----------------------------------------------------------------
	// Column #1, Row #2...
	//-----------------------------------------------------------------
	spontInspTimeLabel_.setColor(Colors::WHITE);
	spontInspTimeLabel_.setX(::COL1_LABEL_X_);
	spontInspTimeLabel_.setY(::ROW2_Y_);
	addDrawable(&spontInspTimeLabel_);

	spontInspTimeUnit_.setColor(Colors::LIGHT_BLUE);
	spontInspTimeUnit_.setX(::COL1_UNIT_X_);
	spontInspTimeUnit_.setY(::ROW2_Y_);
	addDrawable(&spontInspTimeUnit_);

	spontInspTimeValue_.setPrecision(ONES);
	spontInspTimeValue_.setValue(50);
	spontInspTimeValue_.setColor(Colors::WHITE);
	spontInspTimeValue_.setX(::COL1_VALUE_X_);
	spontInspTimeValue_.setY(::ROW2_Y_);
	spontInspTimeValue_.setWidth(::VALUE_WIDTH_);
	spontInspTimeValue_.setHeight(::VALUE_HEIGHT_);
	addDrawable(&spontInspTimeValue_);


	//-----------------------------------------------------------------
	// Column #1, Row #3...
	//-----------------------------------------------------------------
	spontPercentTiLabel_.setColor(Colors::WHITE);
	spontPercentTiLabel_.setX(::COL1_LABEL_X_);
	spontPercentTiLabel_.setY(::ROW3_Y_);
	addDrawable(&spontPercentTiLabel_);

	spontPercentTiValue_.setValue(38.0);
	spontPercentTiValue_.setColor(Colors::WHITE);
	spontPercentTiValue_.setX(::COL1_VALUE_X_);
	spontPercentTiValue_.setY(::ROW3_Y_);
	spontPercentTiValue_.setWidth(::VALUE_WIDTH_);
	spontPercentTiValue_.setHeight(::VALUE_HEIGHT_);
	addDrawable(&spontPercentTiValue_);


	//-----------------------------------------------------------------
	// Column #1, Row #4...
	//-----------------------------------------------------------------
	spontFvRatioLabel_.setColor(Colors::WHITE);
	spontFvRatioLabel_.setX(::COL1_LABEL_X_);
	spontFvRatioLabel_.setY(::ROW4_Y_);
	addDrawable(&spontFvRatioLabel_);

	spontFvRatioValue_.setValue(38.0);
	spontFvRatioValue_.setColor(Colors::WHITE);
	spontFvRatioValue_.setX(::COL1_VALUE_X_);
	spontFvRatioValue_.setY(::ROW4_Y_);
	spontFvRatioValue_.setWidth(::VALUE_WIDTH_);
	spontFvRatioValue_.setHeight(::VALUE_HEIGHT_);
	addDrawable(&spontFvRatioValue_);


	//-----------------------------------------------------------------
	// Column #1, Row #5...
	//-----------------------------------------------------------------
    normalizeSpontFvRatioLabel_.setColor(Colors::WHITE);
    normalizeSpontFvRatioLabel_.setX(::COL1_LABEL_X_);
    normalizeSpontFvRatioLabel_.setY(::ROW5_Y_);
    addDrawable(&normalizeSpontFvRatioLabel_);
     
    normalizeSpontFvRatioValue_.setValue(0.0);
	normalizeSpontFvRatioValue_.setColor(Colors::WHITE);
	normalizeSpontFvRatioValue_.setX(::COL1_VALUE_X_);
	normalizeSpontFvRatioValue_.setY(::ROW5_Y_);
	normalizeSpontFvRatioValue_.setWidth(::VALUE_WIDTH_);
	normalizeSpontFvRatioValue_.setHeight(::VALUE_HEIGHT_);
	addDrawable(&normalizeSpontFvRatioValue_);


	//-----------------------------------------------------------------
	// Column #1, Row #6...
	//-----------------------------------------------------------------
	proxStatusMsg_.setColor(Colors::WHITE);
	proxStatusMsg_.setX(::COL1_LABEL_X_);
	proxStatusMsg_.setY(::ROW6_Y_);
	addDrawable(&proxStatusMsg_);


	//-----------------------------------------------------------------
	// Column #1, Row #7...
	//-----------------------------------------------------------------


	//=================================================================
	// Column #2, Row #1...
	//=================================================================
	endInspiratoryPressureLabel_.setColor(Colors::WHITE);
	endInspiratoryPressureLabel_.setX(::COL2_LABEL_X_);
	endInspiratoryPressureLabel_.setY(::ROW1_Y_);
	addDrawable(&endInspiratoryPressureLabel_);

	endInspiratoryPressureUnit_.setColor(Colors::LIGHT_BLUE);
	endInspiratoryPressureUnit_.setX(::COL2_UNIT_X_);
	endInspiratoryPressureUnit_.setY(::ROW1_Y_);
	addDrawable(&endInspiratoryPressureUnit_);

	endInspiratoryPressureValue_.setPrecision(TENTHS);
	endInspiratoryPressureValue_.setValue(25.0);
	endInspiratoryPressureValue_.setColor(Colors::WHITE);
	endInspiratoryPressureValue_.setX(::COL2_VALUE_X_);
	endInspiratoryPressureValue_.setY(::ROW1_Y_);
	endInspiratoryPressureValue_.setWidth(::VALUE_WIDTH_);
	endInspiratoryPressureValue_.setHeight(::VALUE_HEIGHT_);
	addDrawable(&endInspiratoryPressureValue_);


	//=================================================================
	// Column #2, Row #2... [NIV]
	//=================================================================
	static wchar_t EndExhalationPressureMsg_[120];  
	endExhalationPressureLabel_.setColor(Colors::WHITE);
	endExhalationPressureLabel_.setX(::COL2_LABEL_X_);
	endExhalationPressureLabel_.setY(::ROW2_Y_+7);
	swprintf(EndExhalationPressureMsg_, MiscStrs::TOUCHABLE_END_EXH_PRES_MSG,
					PRESS_UNITS);
	endExhalationPressureLabel_.setMessage(EndExhalationPressureMsg_);
	addDrawable(&endExhalationPressureLabel_);

	endExhalationPressureUnit_.setColor(Colors::LIGHT_BLUE);
	endExhalationPressureUnit_.setX(::COL2_UNIT_X_);
	endExhalationPressureUnit_.setY(::ROW2_Y_);
	addDrawable(&endExhalationPressureUnit_);

	endExhalationPressureValue_.setPrecision(TENTHS);
	endExhalationPressureValue_.setValue(25.0);
	endExhalationPressureValue_.setColor(Colors::WHITE);
	endExhalationPressureValue_.setX(::COL2_VALUE_X_);
	endExhalationPressureValue_.setY(::ROW2_Y_);
	endExhalationPressureValue_.setWidth(::VALUE_WIDTH_);
	endExhalationPressureValue_.setHeight(::VALUE_HEIGHT_);
	addDrawable(&endExhalationPressureValue_);


	//-----------------------------------------------------------------
	// Column #2, Row #2... [PA/TC]
	//-----------------------------------------------------------------
	pavComplianceLabel_.setColor(Colors::WHITE);
	pavComplianceLabel_.setX(::COL2_LABEL_X_);
	pavComplianceLabel_.setY(::ROW2_Y_);
	addDrawable(&pavComplianceLabel_);

	pavComplianceUnit_.setColor(Colors::LIGHT_BLUE);
	pavComplianceUnit_.setX(::COL2_UNIT_X_);
	pavComplianceUnit_.setY(::ROW2_Y_);
	addDrawable(&pavComplianceUnit_);

	pavComplianceValue_.setPrecision(ONES);
	pavComplianceValue_.setValue(25);
	pavComplianceValue_.setColor(Colors::WHITE);
	pavComplianceValue_.setX(::COL2_VALUE_X_);
	pavComplianceValue_.setY(::ROW2_Y_);
	pavComplianceValue_.setWidth(::VALUE_WIDTH_);
	pavComplianceValue_.setHeight(::VALUE_HEIGHT_);
	addDrawable(&pavComplianceValue_);

	pavComplianceMarker_.setColor(Colors::WHITE);
	pavComplianceMarker_.setX(::COL2_VALUE_X_);
	pavComplianceMarker_.setY(::ROW2_Y_);
	addDrawable(&pavComplianceMarker_);


	//-----------------------------------------------------------------
	// Column #2, Row #3...
	//-----------------------------------------------------------------
	pavElastanceLabel_.setColor(Colors::WHITE);
	pavElastanceLabel_.setX(::COL2_LABEL_X_);
	pavElastanceLabel_.setY(::ROW3_Y_);
	addDrawable(&pavElastanceLabel_);

	pavElastanceUnit_.setColor(Colors::LIGHT_BLUE);
	pavElastanceUnit_.setX(::COL2_UNIT_X_);
	pavElastanceUnit_.setY(::ROW3_Y_);
	addDrawable(&pavElastanceUnit_);

	pavElastanceValue_.setPrecision(ONES);
	pavElastanceValue_.setValue(25);
	pavElastanceValue_.setColor(Colors::WHITE);
	pavElastanceValue_.setX(::COL2_VALUE_X_);
	pavElastanceValue_.setY(::ROW3_Y_);
	pavElastanceValue_.setWidth(::VALUE_WIDTH_);
	pavElastanceValue_.setHeight(::VALUE_HEIGHT_);
	addDrawable(&pavElastanceValue_);

	pavElastanceMarker_.setColor(Colors::WHITE);
	pavElastanceMarker_.setX(::COL2_VALUE_X_);
	pavElastanceMarker_.setY(::ROW3_Y_);
	addDrawable(&pavElastanceMarker_);


	//-----------------------------------------------------------------
	// Column #2, Row #4...
	//-----------------------------------------------------------------
	pavResistanceLabel_.setColor(Colors::WHITE);
	pavResistanceLabel_.setX(::COL2_LABEL_X_);
	pavResistanceLabel_.setY(::ROW4_Y_);
	addDrawable(&pavResistanceLabel_);

	pavResistanceUnit_.setColor(Colors::LIGHT_BLUE);
	pavResistanceUnit_.setX(::COL2_UNIT_X_);
	pavResistanceUnit_.setY(::ROW4_Y_);
	addDrawable(&pavResistanceUnit_);

	pavResistanceValue_.setPrecision(ONES);
	pavResistanceValue_.setValue(25);
	pavResistanceValue_.setColor(Colors::WHITE);
	pavResistanceValue_.setX(::COL2_VALUE_X_);
	pavResistanceValue_.setY(::ROW4_Y_);
	pavResistanceValue_.setWidth(::VALUE_WIDTH_);
	pavResistanceValue_.setHeight(::VALUE_HEIGHT_);
	addDrawable(&pavResistanceValue_);

	pavResistanceMarker_.setColor(Colors::WHITE);
	pavResistanceMarker_.setX(::COL2_VALUE_X_);
	pavResistanceMarker_.setY(::ROW4_Y_);
	addDrawable(&pavResistanceMarker_);


	//-----------------------------------------------------------------
	// Column #2, Row #5...
	//-----------------------------------------------------------------
	totalResistanceLabel_.setColor(Colors::WHITE);
	totalResistanceLabel_.setX(::COL2_LABEL_X_);
	totalResistanceLabel_.setY(::ROW5_Y_);
	addDrawable(&totalResistanceLabel_);

	totalResistanceUnit_.setColor(Colors::LIGHT_BLUE);
	totalResistanceUnit_.setX(::COL2_UNIT_X_);
	totalResistanceUnit_.setY(::ROW5_Y_);
	addDrawable(&totalResistanceUnit_);

	totalResistanceValue_.setPrecision(ONES);
	totalResistanceValue_.setValue(25);
	totalResistanceValue_.setColor(Colors::WHITE);
	totalResistanceValue_.setX(::COL2_VALUE_X_);
	totalResistanceValue_.setY(::ROW5_Y_);
	totalResistanceValue_.setWidth(::VALUE_WIDTH_);
	totalResistanceValue_.setHeight(::VALUE_HEIGHT_);
	addDrawable(&totalResistanceValue_);

	totalResistanceMarker_.setColor(Colors::WHITE);
	totalResistanceMarker_.setX(::COL2_VALUE_X_);
	totalResistanceMarker_.setY(::ROW5_Y_);
	addDrawable(&totalResistanceMarker_);


	//-----------------------------------------------------------------
	// Column #2, Row #6...
	//-----------------------------------------------------------------
	pavIntrinsicPeepLabel_.setColor(Colors::WHITE);
	pavIntrinsicPeepLabel_.setX(::COL2_LABEL_X_);
	pavIntrinsicPeepLabel_.setY(::ROW6_Y_);
	addDrawable(&pavIntrinsicPeepLabel_);

	pavIntrinsicPeepUnit_.setColor(Colors::LIGHT_BLUE);
	pavIntrinsicPeepUnit_.setX(::COL2_UNIT_X_);
	pavIntrinsicPeepUnit_.setY(::ROW6_Y_);
	addDrawable(&pavIntrinsicPeepUnit_);

	pavIntrinsicPeepValue_.setPrecision(ONES);
	pavIntrinsicPeepValue_.setValue(25);
	pavIntrinsicPeepValue_.setColor(Colors::WHITE);
	pavIntrinsicPeepValue_.setX(::COL2_VALUE_X_);
	pavIntrinsicPeepValue_.setY(::ROW6_Y_);
	pavIntrinsicPeepValue_.setWidth(::VALUE_WIDTH_);
	pavIntrinsicPeepValue_.setHeight(::VALUE_HEIGHT_);
	addDrawable(&pavIntrinsicPeepValue_);

	pavStatusMsg_.setColor(Colors::WHITE);
	pavStatusMsg_.setX(::COL2_LABEL_X_);
	pavStatusMsg_.setY(::ROW6_Y_);
	addDrawable(&pavStatusMsg_);


	//-----------------------------------------------------------------
	// Column #2, Row #7...
	//-----------------------------------------------------------------
	// This data item is only to be displayed for testing purposes, and not
	// appropriate for production release...
    if (SoftwareOptions::ENGINEERING 
        == SoftwareOptions::GetSoftwareOptions().getDataKeyType())
    {
    	totalWorkOfBreathingLabel_.setColor(Colors::WHITE);
    	totalWorkOfBreathingLabel_.setX(::COL2_LABEL_X_);
    	totalWorkOfBreathingLabel_.setY(::ROW7_Y_);
    	addDrawable(&totalWorkOfBreathingLabel_);

    	totalWorkOfBreathingUnit_.setColor(Colors::LIGHT_BLUE);
    	totalWorkOfBreathingUnit_.setX(::COL2_UNIT_X_);
    	totalWorkOfBreathingUnit_.setY(::ROW7_Y_);
	    addDrawable(&totalWorkOfBreathingUnit_);

    	totalWorkOfBreathingValue_.setPrecision(ONES);
    	totalWorkOfBreathingValue_.setValue(25);
	    totalWorkOfBreathingValue_.setColor(Colors::WHITE);
    	totalWorkOfBreathingValue_.setX(::COL2_VALUE_X_);
	    totalWorkOfBreathingValue_.setY(::ROW7_Y_);
    	totalWorkOfBreathingValue_.setWidth(::VALUE_WIDTH_);
	    totalWorkOfBreathingValue_.setHeight(::VALUE_HEIGHT_);
    	addDrawable(&totalWorkOfBreathingValue_);
    }


	//-----------------------------------------------------------------
	// Column #2, Row #7...
	//-----------------------------------------------------------------
	vcpStatusMsg_.setColor(Colors::WHITE);
	vcpStatusMsg_.setX(::COL2_LABEL_X_);
	vcpStatusMsg_.setY(::ROW7_Y_);
	addDrawable(&vcpStatusMsg_);
	vcpStatusMsg_.setShow(FALSE);

	vsStatusMsg_.setColor(Colors::WHITE);
	vsStatusMsg_.setX(::COL2_LABEL_X_);
	vsStatusMsg_.setY(::ROW7_Y_);
	addDrawable(&vsStatusMsg_);
	vsStatusMsg_.setShow(FALSE);


	//=================================================================
	// Column #3, Row #1...
	//=================================================================
	spontMinuteVolumeLabel_.setColor(Colors::WHITE);
	spontMinuteVolumeLabel_.setX(::COL3_LABEL_X_);
	spontMinuteVolumeLabel_.setY(::ROW1_Y_);
	addDrawable(&spontMinuteVolumeLabel_);

	spontMinuteVolumeUnit_.setColor(Colors::LIGHT_BLUE);
	spontMinuteVolumeUnit_.setX(::COL3_UNIT_X_);
	spontMinuteVolumeUnit_.setY(::ROW1_Y_);
	addDrawable(&spontMinuteVolumeUnit_);

	spontMinuteVolumeValue_.setPrecision(HUNDREDTHS);
	spontMinuteVolumeValue_.setValue(2.5);
	spontMinuteVolumeValue_.setColor(Colors::WHITE);
	spontMinuteVolumeValue_.setX(::COL3_VALUE_X_);
	spontMinuteVolumeValue_.setY(::ROW1_Y_);
	spontMinuteVolumeValue_.setWidth(::VALUE_WIDTH_);
	spontMinuteVolumeValue_.setHeight(::VALUE_HEIGHT_);
	addDrawable(&spontMinuteVolumeValue_);


	//-----------------------------------------------------------------
	// Column #3, Row #2...
	//-----------------------------------------------------------------
	inspTidalVolLabel_.setColor(Colors::WHITE);
	inspTidalVolLabel_.setX(::COL3_LABEL_X_);
	inspTidalVolLabel_.setY(::ROW2_Y_);
	addDrawable(&inspTidalVolLabel_);

	inspTidalVolUnit_.setColor(Colors::LIGHT_BLUE);
	inspTidalVolUnit_.setX(::COL3_UNIT_X_);
	inspTidalVolUnit_.setY(::ROW2_Y_);
	addDrawable(&inspTidalVolUnit_);

	inspTidalVolValue_.setPrecision(ONES);
	inspTidalVolValue_.setValue(25);
	inspTidalVolValue_.setColor(Colors::WHITE);
	inspTidalVolValue_.setX(::COL3_VALUE_X_);
	inspTidalVolValue_.setY(::ROW2_Y_);
	inspTidalVolValue_.setWidth(::VALUE_WIDTH_);
	inspTidalVolValue_.setHeight(::VALUE_HEIGHT_);
	addDrawable(&inspTidalVolValue_);

	//-----------------------------------------------------------------
	// Column #3, Row #3...
	//-----------------------------------------------------------------


	inspLeakVolLabel_.setColor(Colors::WHITE);
	inspLeakVolLabel_.setX(::COL3_LABEL_X_);
	inspLeakVolLabel_.setY(::ROW3_Y_);
	addDrawable(&inspLeakVolLabel_);

	inspLeakVolUnit_.setColor(Colors::LIGHT_BLUE);
	inspLeakVolUnit_.setX(::COL3_UNIT_X_);
	inspLeakVolUnit_.setY(::ROW3_Y_);
	addDrawable(&inspLeakVolUnit_);

	inspLeakVolValue_.setPrecision(ONES);
    inspLeakVolValue_.setValue(0);
	inspLeakVolValue_.setColor(Colors::WHITE);
	inspLeakVolValue_.setX(::COL3_VALUE_X_);
	inspLeakVolValue_.setY(::ROW3_Y_);
	inspLeakVolValue_.setWidth(::VALUE_WIDTH_);
	inspLeakVolValue_.setHeight(::VALUE_HEIGHT_);
	addDrawable(&inspLeakVolValue_);


	//-----------------------------------------------------------------
	// Column #3, Row #4...
	//-----------------------------------------------------------------

	percentLeakLabel_.setColor(Colors::WHITE);
	percentLeakLabel_.setX(::COL3_LABEL_X_);
	percentLeakLabel_.setY(::ROW4_Y_);
	addDrawable(&percentLeakLabel_);

	percentLeakUnit_.setColor(Colors::LIGHT_BLUE);
	percentLeakUnit_.setX(::COL3_UNIT_X_);
	percentLeakUnit_.setY(::ROW4_Y_);
	addDrawable(&percentLeakUnit_);

	percentLeakValue_.setPrecision(ONES);
	percentLeakValue_.setValue(0);
	percentLeakValue_.setColor(Colors::WHITE);
	percentLeakValue_.setX(::COL3_VALUE_X_);
	percentLeakValue_.setY(::ROW4_Y_);
	percentLeakValue_.setWidth(::VALUE_WIDTH_);
	percentLeakValue_.setHeight(::VALUE_HEIGHT_);
	addDrawable(&percentLeakValue_);

	//-----------------------------------------------------------------
	// Column #3, Row #5...
	//-----------------------------------------------------------------


	exhLeakRateLabel_.setColor(Colors::WHITE);
	exhLeakRateLabel_.setX(::COL3_LABEL_X_);
	exhLeakRateLabel_.setY(::ROW5_Y_);
	addDrawable(&exhLeakRateLabel_);

	exhLeakRateUnit_.setColor(Colors::LIGHT_BLUE);
	exhLeakRateUnit_.setX(::COL3_UNIT_X_);
	exhLeakRateUnit_.setY(::ROW5_Y_);
	addDrawable(&exhLeakRateUnit_);

	exhLeakRateValue_.setPrecision(ONES);
    exhLeakRateValue_.setValue(0);
	exhLeakRateValue_.setColor(Colors::WHITE);
	exhLeakRateValue_.setX(::COL3_VALUE_X_);
	exhLeakRateValue_.setY(::ROW5_Y_);
	exhLeakRateValue_.setWidth(::VALUE_WIDTH_);
	exhLeakRateValue_.setHeight(::VALUE_HEIGHT_);
	addDrawable(&exhLeakRateValue_);


	//-----------------------------------------------------------------
	// Column #3, Row #6...
	//-----------------------------------------------------------------


	//-----------------------------------------------------------------
	// Column #3, Row #7...
	//-----------------------------------------------------------------



	// this subscreen needs to ALWAYS be attached to the accepted context
	// subject...
	attachToSubject_(ContextId::ACCEPTED_CONTEXT_ID,
					 Notification::BATCH_CHANGED);

	//
	// Register for changes to patient data values.
	//
	PatientDataRegistrar::
		RegisterTarget(PatientDataId::EXH_SPONT_MINUTE_VOL_ITEM, this);
	PatientDataRegistrar::RegisterTarget(PatientDataId::DELIVERED_O2_PERCENTAGE_ITEM, this);
	PatientDataRegistrar::RegisterTarget(PatientDataId::INSP_TIDAL_VOL_ITEM, this);
	PatientDataRegistrar::RegisterTarget(PatientDataId::IS_PEEP_RECOVERY_ITEM, this);
	PatientDataRegistrar::RegisterTarget(PatientDataId::BREATH_TYPE_ITEM, this);
	PatientDataRegistrar::RegisterTarget(PatientDataId::END_INSP_PRESS_ITEM,
										 this);
	PatientDataRegistrar::RegisterTarget(PatientDataId::END_EXP_PRESS_ITEM,
										 this);
	
	PatientDataRegistrar::RegisterTarget(PatientDataId::TOTAL_AIRWAY_RESISTANCE_ITEM,
										 this);
	PatientDataRegistrar::RegisterTarget(PatientDataId::PAV_LUNG_ELASTANCE_ITEM,
										 this);
                                         
	// This data item is only to be displayed for testing purposes, and not
	// appropriate for production release...
    if (SoftwareOptions::ENGINEERING 
        == SoftwareOptions::GetSoftwareOptions().getDataKeyType())
    {    
	    PatientDataRegistrar::RegisterTarget(PatientDataId::TOTAL_WORK_OF_BREATHING_ITEM, 
                                             this);
    }

	PatientDataRegistrar::RegisterTarget(PatientDataId::PAV_STATE_ITEM, this);
	PatientDataRegistrar::RegisterTarget(PatientDataId::PAV_PEEP_INTRINSIC_ITEM,
										 this);

	PatientDataRegistrar::RegisterTarget(PatientDataId::SPONT_RATE_TO_VOLUME_RATIO_ITEM,
										 this);
	PatientDataRegistrar::RegisterTarget(PatientDataId::SPONT_PERCENT_TI_ITEM,
										 this);
	PatientDataRegistrar::RegisterTarget(PatientDataId::SPONT_INSP_TIME_ITEM,
										 this);
	PatientDataRegistrar::RegisterTarget(PatientDataId::VTPCV_STATE_ITEM, this);
	PatientDataRegistrar::RegisterTarget(PatientDataId::PERCENT_LEAK_ITEM, this);
	PatientDataRegistrar::RegisterTarget(PatientDataId::EXH_LEAK_RATE_ITEM, this);
	PatientDataRegistrar::RegisterTarget(PatientDataId::INSP_LEAK_VOL_ITEM, this);

	// Register for GUI state change callbacks.
	GuiEventRegistrar::RegisterTarget(this);

	// Register for Breath-Delivery status events
	BdEventRegistrar::RegisterTarget(EventData::VENT_INOP, this);	
	BdEventRegistrar::RegisterTarget(EventData::PATIENT_CONNECT, this);	
	BdEventRegistrar::RegisterTarget(EventData::SVO, this);	
	BdEventRegistrar::RegisterTarget(EventData::OCCLUSION, this);
	BdEventRegistrar::RegisterTarget(EventData::APNEA_VENT, this);
	BdEventRegistrar::RegisterTarget(EventData::SAFETY_VENT, this);
	BdEventRegistrar::RegisterTarget(EventData::PROX_FAULT, this);
	BdEventRegistrar::RegisterTarget(EventData::PROX_READY, this);

	RmButton_.setButtonCallback(this); 
    addDrawable(&RmButton_);

    // If the RM Option is not enabled, do not display the RM button
    // to allow access to RM Data Subscreen
    // $[RM01000] 
    if (!SoftwareOptions::IsOptionEnabled(SoftwareOptions::RESP_MECH))	
    {
        RmButton_.setShow(FALSE);      
    }

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~MoreDataSubScreen  [Destructor]
//
//@ Interface-Description
// Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  n/a
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

MoreDataSubScreen::~MoreDataSubScreen(void)
{
	CALL_TRACE("MoreDataSubScreen::MoreDataSubScreen(void)");

	// Do nothing.
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activate()
//
//@ Interface-Description
// Prepares this subscreen for display.
//---------------------------------------------------------------------
//@ Implementation-Description
//  During the activation of this screen, the RM button is set to flat
//  and the touch screen release timer is activated.  Once the  
//  touch screen is released, the RmButton shall be set back to the  
//  up state position.  If the lastDisplay_ is set to RMDATASUBSCREEN,
//  the displayRmDataSubScreen_ function is called to handle activation
//  of the RM Data SubScreen.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
MoreDataSubScreen::activate(void)
{
	CALL_TRACE("MoreDataSubScreen::activate(void)");

    // Force this subscreen to the RM data subscreen if it was the last 
    // subscreen displayed before it was deactivated.
    if(lastDisplay_ ==  RMDATASUBSCREEN)
    {
        displayRmDataSubScreen_();
    }

}	


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: deactivate()
//
//@ Interface-Description
// Prepares this subscreen for removal from the display.
//---------------------------------------------------------------------
//@ Implementation-Description
// Empty implementation of a pure virtual function.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
MoreDataSubScreen::deactivate(void)
{
	CALL_TRACE("MoreDataSubScreen::deactivate(void)");

	// Do nothing.
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: batchSettingUpdate
//
//@ Interface-Description
//  Called when batch settings in this subscreen changed.  Passed the
//  following parameters:
// >Von
//  qualifierId	The context in which the change happened, either
//					ACCEPTED or ADJUSTED.
//  pSubject	The pointer to the Setting's Context subject.

//	settingId	The enum id of the setting which changed.
// >Voff
  
//---------------------------------------------------------------------
//@ Implementation-Description
// We are only interested in changes to the Accepted Context so we
// ignore all others.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
MoreDataSubScreen::batchSettingUpdate(
							 const Notification::ChangeQualifier qualifierId,
							 const ContextSubject*               pSubject,
							 const SettingId::SettingIdType      settingId
									 )
{
	CALL_TRACE("batchSettingUpdate(qualifierId, pSubject, settingId)");
      	    
    updateSettingApplicability_();

    if (qualifierId == Notification::ACCEPTED)
	{													// $[TI1]
		static Boolean  HasFirstUpdateOccurred_ = FALSE;

		if (HasFirstUpdateOccurred_)
		{	// $[TI1.1] -- update already occurred, update conditionally...
			switch (settingId)
			{
			case SettingId::VENT_TYPE :
			case SettingId::MODE :
			case SettingId::FIO2_ENABLED :
			case SettingId::SUPPORT_TYPE :
       		case SettingId::MAND_TYPE :     // $[TI1.1.1]
			case SettingId::LEAK_COMP_ENABLED:
			case SettingId::APNEA_MAND_TYPE:
			case SettingId::PROX_ENABLED:
			case SettingId::NUM_BATCH_IDS :					// $[TI1.1.1]
				// update the display of this screen...
				updateShowStates_();
				break;
			default :										// $[TI1.1.2]
				// do nothing for all other setting changes...
				break;
			}
		}
		else
		{	// $[TI1.2] -- this is first update, force an update...
			updateShowStates_();

			HasFirstUpdateOccurred_ = TRUE;
		}

		if(!IS_VCP_ACTIVE && vcpStatusMsg_.getShow())
		{
			vcpStatusMsg_.setShow(FALSE);
		}
		if(!IS_VS_ACTIVE && vsStatusMsg_.getShow())
		{
			vsStatusMsg_.setShow(FALSE);
		}
	}													// $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: patientDataChangeHappened
//
//@ Interface-Description
// Handles update event notification for patient data fields.  The
// 'patientDataId' identifies which patient data value changed so that
// we know which data value to update.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
// The given 'patientDataId' value must be one of the id's associated
// with the patient data fields in this area.
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

void 
MoreDataSubScreen::patientDataChangeHappened(
								PatientDataId::PatientItemId patientDataId)
{
	CALL_TRACE("patientDataChangeHappened(patientDataId)");

	updateDataValue_(patientDataId);
}	// $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: guiEventHappened
//
//@ Interface-Description
// Called normally via GuiEventRegistrar when the GUI event has changed.
// Passed the id of the state which changed.
//---------------------------------------------------------------------
//@ Implementation-Description
// Just call updateShowStates_() to decide if a change in the display
// mode is needed.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
MoreDataSubScreen::guiEventHappened(GuiApp::GuiAppEvent)
{
	CALL_TRACE("MoreDataSubScreen::guiEventHappened(GuiApp::GuiAppEvent)");

	//
	// Call the normal updateShowStates_() routine to set the visibility of
	// the mode-sensitive patient data values.
	//
	updateShowStates_();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: bdEventHappened
//
//@ Interface-Description
// Called normally via the BdEventRegistrar when a BD event in which we are
// interested occurs, e.g. Safety Ventilation or Ventilator Inoperative.
// Passed the following parameters:
// >Von
//	eventId		The type of the BD event.
//	eventStatus	The status of the BD event.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
// Ignore passed parameters and just call setMode_() which will decide
// what display mode change is needed, if any.
// 
// Also when Breath-Delivery sends a PROX_FAULT message with an active 
// event id, remove all PROX symbols by setting the isProxInop_ 
// to TRUE, and reactivate this subscreen. 
// $[PX06003] 
//---------------------------------------------------------------------
//@ PreCondition                                              
//  none                                                      
//---------------------------------------------------------------------
//@ PostCondition                                             
//  none                                                      
//@ End-Method                                                
//=====================================================================
                                                              
void                                                          
MoreDataSubScreen::bdEventHappened(EventData::EventId eventId,
						           EventData::EventStatus eventStatus,
								   EventData::EventPrompt eventPrompt)
{


	if (EventData::PROX_FAULT == eventId ||
        EventData::SAFETY_VENT == eventId)
	{

		// When PROX is inoperative, set the isProxInop_ to remove the PROX symbols 
		// and refresh the screen.
		if(eventStatus == EventData::ACTIVE)
		{
            isProxInop_ = TRUE;
		}
		else
		{
			isProxInop_ = FALSE;
		}

	}
	else if (EventData::PROX_READY == eventId)
	{

		if(eventStatus == EventData::ACTIVE)		  
		{
            IS_PROX_STARTUP = FALSE;
		}
		else
		{
			IS_PROX_STARTUP = TRUE;
		}
	}

	

	//
	// Call the normal updateShowStates_() routine to set the visibility of
	// the mode-sensitive patient data values.
	//
	updateShowStates_();


}                                                             


#endif // SIGMA_GUI_CPU


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
MoreDataSubScreen::SoftFault(const SoftFaultID  softFaultID,
						     const Uint32       lineNumber,
						     const char*        pFileName,
						     const char*        pPredicate)  
{
	CALL_TRACE("MoreDataSubScreen::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, MOREDATASUBSCREEN,
							lineNumber, pFileName, pPredicate);
}


//=====================================================================
//
// Private methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateShowStates_()
//
//@ Interface-Description
// Prepares this subscreen for display.
//---------------------------------------------------------------------
//@ Implementation-Description
// Fetches the current value of 'mode' setting.  If the value is A/C, then hide
// the 'exhaled spontaneous minute volume' and 'inspiratory spontaneous tidal 
// volume' objects.  Otherwise, show these
// objects.  The value and units for the oxygen percentage must be removed if
// the oxygen sensor is disabled.  Force the update of each field so that the
// correct up-to-the-second patient data values are displayed immediately when
// the subscreen is displayed.
// Spont minute volume not displayed when in apnea or safety ventilation.
// $[TC01001] The More Data shall display the current measured value for the ...
// $[LC02003]\e\ in CPAP, measured PEEP shall be removed...
// $[LC02001]\e\ exiting CPAP, measured PEEP shall be viewable...
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
MoreDataSubScreen::updateShowStates_(void)
{
	CALL_TRACE("updateShowStates_()");

	//-----------------------------------------------------------------
	//  NOTE:  In future software releases, Patient Data display is a 
	//         potential candidate for redesign using an applicability
	//         feature similar to that used in Settings-Validation.  This
	//         type of mechanism would facilitate expansion of displayed
	//         data items, and criteria of their display.
	//-----------------------------------------------------------------

	const ContextSubject *const  P_ACCEPTED_SUBJECT =
							getSubjectPtr_(ContextId::ACCEPTED_CONTEXT_ID);

	const DiscreteValue  FIO2_ENABLED_VALUE =
				(GuiApp::IsVentStartupSequenceComplete())
				 ? P_ACCEPTED_SUBJECT->getSettingValue(SettingId::FIO2_ENABLED)
				 : SafetyPcvSettingValues::GetValue(SettingId::FIO2_ENABLED);

	const DiscreteValue  MODE_VALUE =
		P_ACCEPTED_SUBJECT->getSettingValue(SettingId::MODE);

	// Show or hide oxygen percentage depending on whether the oxygen
	// sensor has been enabled or disabled.
	if (FIO2_ENABLED_VALUE == Fio2EnabledValue::YES_FIO2_ENABLED)
	{											// $[TI1]
		addDrawable(&deliveredOxygenPercentValue_);
		addDrawable(&deliveredOxygenPercentUnit_);
	}
	else	
	{											// $[TI2]
		removeDrawable(&deliveredOxygenPercentValue_);
		removeDrawable(&deliveredOxygenPercentUnit_);
	}
	
    // Get the current settings applicability.
    updateSettingApplicability_();

	// Update inspired tidal volume labels.
	updateVolumeLabel_();
    


	if ((GuiApp::IsSettingsLockedOut())
#ifndef SIGMA_DEVELOPMENT // FORNOW ignore the flash table
		|| (!GuiApp::IsBdAndGuiFlashTheSame())
#endif // SIGMA_DEVELOPMENT
		|| (GuiApp::SERIAL_NUMBER_OK != GuiApp::GetSerialNumberInfoState())
		|| (BdGuiEvent::GetEventStatus(EventData::VENT_INOP) ==
											EventData::ACTIVE)
		|| (BdGuiEvent::GetEventStatus(EventData::PATIENT_CONNECT) !=
									EventData::ACTIVE)
		|| (BdGuiEvent::GetEventStatus(EventData::SVO) ==
									EventData::ACTIVE)
		|| (BdGuiEvent::GetEventStatus(EventData::OCCLUSION) ==
									EventData::ACTIVE))
	{	// Hide the patient data					// $[TI3]

		endInspiratoryPressureUnit_.setShow(FALSE);
		endInspiratoryPressureValue_.setShow(FALSE);

		endExhalationPressureLabel_.setShow(IS_NIV_ACTIVE);
		endExhalationPressureUnit_.setShow(FALSE);
		endExhalationPressureValue_.setShow(FALSE);

		spontMinuteVolumeLabel_.setShow(IS_SPONT_TYPE_APPLIC);
		spontMinuteVolumeUnit_.setShow(FALSE);
		spontMinuteVolumeValue_.setShow(FALSE);
		
		deliveredOxygenPercentUnit_.setShow(FALSE);
		deliveredOxygenPercentValue_.setShow(FALSE);

		inspTidalVolLabel_.setShow( !(IS_VCV_ACTIVE && IS_AC_ACTIVE) );
		inspTidalVolUnit_.setShow(FALSE);
		inspTidalVolValue_.setShow(FALSE);

		pavComplianceLabel_.setShow(IS_PA_ACTIVE);
		pavComplianceUnit_.setShow(FALSE);
		pavComplianceValue_.setShow(FALSE);
		pavComplianceMarker_.setShow(FALSE);

		pavElastanceLabel_.setShow(IS_PA_ACTIVE);
		pavElastanceUnit_.setShow(FALSE);
		pavElastanceValue_.setShow(FALSE);
		pavElastanceMarker_.setShow(FALSE);

		pavResistanceLabel_.setShow(IS_PA_ACTIVE);
		pavResistanceUnit_.setShow(FALSE);
		pavResistanceValue_.setShow(FALSE);
		pavResistanceMarker_.setShow(FALSE);

		totalResistanceLabel_.setShow(IS_PA_ACTIVE);
		totalResistanceUnit_.setShow(FALSE);
		totalResistanceValue_.setShow(FALSE);
		totalResistanceMarker_.setShow(FALSE);

		totalWorkOfBreathingLabel_.setShow(IS_PA_ACTIVE);
		totalWorkOfBreathingUnit_.setShow(FALSE);
		totalWorkOfBreathingValue_.setShow(FALSE);

		pavIntrinsicPeepLabel_.setShow(FALSE);
		pavIntrinsicPeepUnit_.setShow(FALSE);
		pavIntrinsicPeepValue_.setShow(FALSE);

		pavStatusMsg_.setShow(FALSE);
		vcpStatusMsg_.setShow(FALSE);
		vsStatusMsg_.setShow(FALSE);
		proxStatusMsg_.setShow(FALSE);

		spontPercentTiLabel_.setShow(IS_IN_SPONT_MODE);
		spontPercentTiValue_.setShow(FALSE);

		spontFvRatioLabel_.setShow(IS_IN_SPONT_MODE);
		spontFvRatioValue_.setShow(FALSE);
		
		normalizeSpontFvRatioLabel_.setShow(IS_PA_ACTIVE);
		normalizeSpontFvRatioValue_.setShow(FALSE);

		spontInspTimeLabel_.setShow(IS_SPONT_TYPE_APPLIC);
		spontInspTimeUnit_.setShow(FALSE);
		spontInspTimeValue_.setShow(FALSE);

		percentLeakLabel_.setShow(IS_LEAK_COMP_ACTIVE);
		percentLeakValue_.setShow(FALSE);
		percentLeakUnit_.setShow(FALSE);

		exhLeakRateLabel_.setShow(IS_LEAK_COMP_ACTIVE);
		exhLeakRateValue_.setShow(FALSE);
		exhLeakRateUnit_.setShow(FALSE);

		inspLeakVolLabel_.setShow(IS_LEAK_COMP_ACTIVE);
		inspLeakVolValue_.setShow(FALSE);
		inspLeakVolUnit_.setShow(FALSE);

		// Force an patient data invalid so when new data
		// occured, we will update them immediately.
		PatientDataRegistrar::ResetPatientValue();
	}
	else
	{	// Don't hide the data										// $[TI4]
		updateDataValue_(PatientDataId::END_INSP_PRESS_ITEM);
		updateDataValue_(PatientDataId::END_EXP_PRESS_ITEM);

		// In safety or apnea mode, there should be no spontaneous data
		// being sent over...
		if ((BdGuiEvent::GetEventStatus(EventData::APNEA_VENT) == EventData::ACTIVE) ||
			(BdGuiEvent::GetEventStatus(EventData::SAFETY_VENT) == EventData::ACTIVE))
		{										// $[TI4.1]
			spontMinuteVolumeLabel_.setShow(FALSE);
			spontMinuteVolumeUnit_.setShow(FALSE);
			spontMinuteVolumeValue_.setShow(FALSE);

			inspTidalVolLabel_.setShow(!IS_APNEA_VCV_ACTIVE);
			inspTidalVolUnit_.setShow(FALSE);
			inspTidalVolValue_.setShow(FALSE);

			pavComplianceLabel_.setShow(FALSE);
			pavComplianceUnit_.setShow(FALSE);
			pavComplianceValue_.setShow(FALSE);
			pavComplianceMarker_.setShow(FALSE);

			pavElastanceLabel_.setShow(FALSE);
			pavElastanceUnit_.setShow(FALSE);
			pavElastanceValue_.setShow(FALSE);
			pavElastanceMarker_.setShow(FALSE);

			pavResistanceLabel_.setShow(FALSE);
			pavResistanceUnit_.setShow(FALSE);
			pavResistanceValue_.setShow(FALSE);
			pavResistanceMarker_.setShow(FALSE);

			totalResistanceLabel_.setShow(FALSE);
			totalResistanceUnit_.setShow(FALSE);
			totalResistanceValue_.setShow(FALSE);
			totalResistanceMarker_.setShow(FALSE);

			totalWorkOfBreathingLabel_.setShow(FALSE);
			totalWorkOfBreathingUnit_.setShow(FALSE);
			totalWorkOfBreathingValue_.setShow(FALSE);

			pavIntrinsicPeepLabel_.setShow(FALSE);
			pavIntrinsicPeepUnit_.setShow(FALSE);
			pavIntrinsicPeepValue_.setShow(FALSE);

			pavStatusMsg_.setShow(FALSE);
			proxStatusMsg_.setShow(FALSE);

			spontPercentTiLabel_.setShow(FALSE);
			spontPercentTiValue_.setShow(FALSE);

			spontFvRatioLabel_.setShow(FALSE);
			spontFvRatioValue_.setShow(FALSE);
			
			normalizeSpontFvRatioLabel_.setShow(FALSE);
			normalizeSpontFvRatioValue_.setShow(FALSE);

			vcpStatusMsg_.setShow(FALSE);
			vsStatusMsg_.setShow(FALSE);

			spontInspTimeLabel_.setShow(FALSE);
			spontInspTimeUnit_.setShow(FALSE);
			spontInspTimeValue_.setShow(FALSE);

			percentLeakLabel_.setShow(FALSE);
			percentLeakValue_.setShow(FALSE);
			percentLeakUnit_.setShow(FALSE);

			exhLeakRateLabel_.setShow(FALSE);
			exhLeakRateValue_.setShow(FALSE);
			exhLeakRateUnit_.setShow(FALSE);

			inspLeakVolLabel_.setShow(FALSE);
			inspLeakVolValue_.setShow(FALSE);
			inspLeakVolUnit_.setShow(FALSE);

		}
		else 
		{	// not safety PCV or Apnea									// $[TI4.2]
			if (IS_SPONT_TYPE_APPLIC)
			{											// $[TI4.2.1]
				spontMinuteVolumeLabel_.setShow(TRUE);
				spontInspTimeLabel_.setShow(TRUE);

				if (IS_IN_SPONT_MODE)
				{										// $[TI4.2.1.1]
					spontPercentTiLabel_.setShow(TRUE);
					spontFvRatioLabel_.setShow(TRUE);
					
				}
				else
				{										// $[TI4.2.1.2]
					spontPercentTiLabel_.setShow(FALSE);
					spontPercentTiValue_.setShow(FALSE);

					spontFvRatioLabel_.setShow(FALSE);
					spontFvRatioValue_.setShow(FALSE);
					
					normalizeSpontFvRatioLabel_.setShow(FALSE);
					normalizeSpontFvRatioValue_.setShow(FALSE);
				}  // end if(IS_IN_SPONT_MODE)

				if (IS_PA_ACTIVE)
				{										// $[TI4.2.1.5]
					pavComplianceLabel_.setShow(TRUE);
					pavResistanceLabel_.setShow(TRUE);
					totalResistanceLabel_.setShow(TRUE);
					pavElastanceLabel_.setShow(TRUE);
					totalWorkOfBreathingLabel_.setShow(TRUE);
				    normalizeSpontFvRatioLabel_.setShow(TRUE);

					// PEEPi's value and unit are not explicitly set here,
					// they're set when the PAV state is transmitted...
				}
				else
				{										// $[TI4.2.1.6]
					pavComplianceLabel_.setShow(FALSE);
					pavComplianceValue_.setShow(FALSE);
					pavComplianceUnit_.setShow(FALSE);
					pavComplianceMarker_.setShow(FALSE);

					pavElastanceLabel_.setShow(FALSE);
					pavElastanceValue_.setShow(FALSE);
					pavElastanceUnit_.setShow(FALSE);
					pavElastanceMarker_.setShow(FALSE);

					pavResistanceLabel_.setShow(FALSE);
					pavResistanceValue_.setShow(FALSE);
					pavResistanceUnit_.setShow(FALSE);
					pavResistanceMarker_.setShow(FALSE);

					totalResistanceLabel_.setShow(FALSE);
					totalResistanceValue_.setShow(FALSE);
					totalResistanceUnit_.setShow(FALSE);
					totalResistanceMarker_.setShow(FALSE);

					totalWorkOfBreathingLabel_.setShow(FALSE);
					totalWorkOfBreathingValue_.setShow(FALSE);
					totalWorkOfBreathingUnit_.setShow(FALSE);

					pavIntrinsicPeepLabel_.setShow(FALSE);
					pavIntrinsicPeepValue_.setShow(FALSE);
					pavIntrinsicPeepUnit_.setShow(FALSE);
					
					normalizeSpontFvRatioLabel_.setShow(FALSE);
					normalizeSpontFvRatioValue_.setShow(FALSE);

					pavStatusMsg_.setShow(FALSE);
				}
			}
			else
			{	// not IS_SPONT_TYPE_APPLIC										// $[TI4.2.2]
				spontMinuteVolumeLabel_.setShow(FALSE);
				spontMinuteVolumeValue_.setShow(FALSE);
				spontMinuteVolumeUnit_.setShow(FALSE);

				spontPercentTiLabel_.setShow(FALSE);
				spontPercentTiValue_.setShow(FALSE);

				spontFvRatioLabel_.setShow(FALSE);
				spontFvRatioValue_.setShow(FALSE);
                
               
				spontInspTimeLabel_.setShow(FALSE);
				spontInspTimeValue_.setShow(FALSE);
				spontInspTimeUnit_.setShow(FALSE);
				pavComplianceLabel_.setShow(FALSE);
				pavComplianceValue_.setShow(FALSE);
				pavComplianceUnit_.setShow(FALSE);
				pavComplianceMarker_.setShow(FALSE);

				pavElastanceLabel_.setShow(FALSE);
				pavElastanceValue_.setShow(FALSE);
				pavElastanceUnit_.setShow(FALSE);
				pavElastanceMarker_.setShow(FALSE);

				pavResistanceLabel_.setShow(FALSE);
				pavResistanceValue_.setShow(FALSE);
				pavResistanceUnit_.setShow(FALSE);
				pavResistanceMarker_.setShow(FALSE);

				totalResistanceLabel_.setShow(FALSE);
				totalResistanceValue_.setShow(FALSE);
				totalResistanceUnit_.setShow(FALSE);
				totalResistanceMarker_.setShow(FALSE);

				totalWorkOfBreathingLabel_.setShow(FALSE);
				totalWorkOfBreathingValue_.setShow(FALSE);
				totalWorkOfBreathingUnit_.setShow(FALSE);

				pavIntrinsicPeepLabel_.setShow(FALSE);
				pavIntrinsicPeepValue_.setShow(FALSE);
				pavIntrinsicPeepUnit_.setShow(FALSE);
				
				normalizeSpontFvRatioLabel_.setShow(FALSE);
				normalizeSpontFvRatioValue_.setShow(FALSE);

				pavStatusMsg_.setShow(FALSE);

		   		vsStatusMsg_.setShow(FALSE);
			}  //end if(IS_SPONT_TYPE_APPLIC)
			
			endExhalationPressureLabel_.setShow(IS_NIV_ACTIVE);
			endExhalationPressureUnit_.setShow(IS_NIV_ACTIVE);
			endExhalationPressureValue_.setShow(IS_NIV_ACTIVE);

			percentLeakLabel_.setShow(IS_LEAK_COMP_ACTIVE);
			percentLeakUnit_.setShow(IS_LEAK_COMP_ACTIVE);
			percentLeakValue_.setShow(IS_LEAK_COMP_ACTIVE);

			exhLeakRateLabel_.setShow(IS_LEAK_COMP_ACTIVE);
			exhLeakRateValue_.setShow(IS_LEAK_COMP_ACTIVE);
			exhLeakRateUnit_.setShow(IS_LEAK_COMP_ACTIVE);

			inspLeakVolLabel_.setShow(IS_LEAK_COMP_ACTIVE);
			inspLeakVolValue_.setShow(IS_LEAK_COMP_ACTIVE);
			inspLeakVolUnit_.setShow(IS_LEAK_COMP_ACTIVE);


			inspTidalVolLabel_.setShow(!(IS_VCV_ACTIVE && IS_AC_ACTIVE));
			inspTidalVolUnit_.setShow(  !(IS_VCV_ACTIVE && IS_AC_ACTIVE));
			inspTidalVolValue_.setShow( !(IS_VCV_ACTIVE && IS_AC_ACTIVE));

			proxStatusMsg_.setShow(IS_PROX_STARTUP && IS_PROX_ACTIVE && !isProxInop_);

		}  //end if(Safety PCV or APNEA)
	}
    // Let the default setting above happened but overwrite with this special mode
 
    if (ModeValue::CPAP_MODE_VALUE == MODE_VALUE)
    {
        endExhalationPressureLabel_.setShow(FALSE);
        endExhalationPressureUnit_.setShow(FALSE);
        endExhalationPressureValue_.setShow(FALSE);

        spontMinuteVolumeLabel_.setShow(FALSE);
        spontMinuteVolumeUnit_.setShow(FALSE);
        spontMinuteVolumeValue_.setShow(FALSE);					

    }

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateVolumeLabel_
//
//@ Interface-Description
// Handles updating the label on the inspired tidal volume parameter based
// on Leak Comp setting. Also if Prox is active, add a subscript y to all
// volume labels.
//---------------------------------------------------------------------
//@ Implementation-Description
// Updates the inspired tidal volume parameter symbol.  Display Vtl when
// Leak Comp is enabled.  Otherwise if Leak Comp is disabled,
// display Vti.  If PROX is active add the subscript y to all volume 
// labels.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

void
MoreDataSubScreen::updateVolumeLabel_(void)
{

     Boolean isProxAvailable = (!IS_PROX_STARTUP && IS_PROX_ACTIVE && !isProxInop_ && UpperScreen::RUpperScreen.getVitalPatientDataArea()->
									           isVentNotInStartupOrWaitForPT());

    // $[LC01003] -- if Leak Compensation is Enabled, 
	// the More Data Subscreen shall relabel VTI as VTL...
	if (IS_LEAK_COMP_ACTIVE)
	{

		// $[PX01002] -- if PROX is Enabled, 
		// the More Data Subscreen shall relabel VTL as VTL-y.
		if (isProxAvailable)
		{
			inspTidalVolLabel_.setText(MiscStrs::PROX_TOUCHABLE_INSP_TIDAL_VOL_LEAK_LABEL);
			inspTidalVolLabel_.setMessage(MiscStrs::PROX_TOUCHABLE_INSP_TIDAL_VOL_LEAK_MSG);
		}
		else
		{
			/* Label as Vtl (no subscript) */
			inspTidalVolLabel_.setText(MiscStrs::TOUCHABLE_INSP_TIDAL_VOL_LEAK_LABEL);
			inspTidalVolLabel_.setMessage(MiscStrs::TOUCHABLE_INSP_TIDAL_VOL_LEAK_MSG);
		}
	}	
	else
	{
		// $[PX01002] -- if PROX is Enabled, 
		// the More Data Subscreen shall relabel VTI as VTI-y.
		if (isProxAvailable)
		{
			inspTidalVolLabel_.setText(MiscStrs::PROX_TOUCHABLE_INSP_TIDAL_VOL_LABEL);
			inspTidalVolLabel_.setMessage(MiscStrs::PROX_TOUCHABLE_INSP_TIDAL_VOL_MSG);
		}
		else
		{
			/* Label as Vti (no subscript) */
			inspTidalVolLabel_.setText(MiscStrs::TOUCHABLE_INSP_TIDAL_VOL_LABEL);
			inspTidalVolLabel_.setMessage(MiscStrs::TOUCHABLE_INSP_TIDAL_VOL_MSG);
		}

	}


    if (isProxAvailable)
    {
        spontFvRatioLabel_.setText(MiscStrs::PROX_TOUCHABLE_SPONT_FV_RATIO_LABEL);
        spontFvRatioLabel_.setMessage(MiscStrs::PROX_TOUCHABLE_SPONT_FV_RATIO_MSG);
        spontMinuteVolumeLabel_.setText(MiscStrs::PROX_TOUCHABLE_SPONT_MINUTE_VOL_LABEL);
        spontMinuteVolumeLabel_.setMessage(MiscStrs::PROX_TOUCHABLE_SPONT_MINUTE_VOL_MSG);
    }
    else
    {
        spontFvRatioLabel_.setText(MiscStrs::TOUCHABLE_SPONT_FV_RATIO_LABEL);
        spontFvRatioLabel_.setMessage(MiscStrs::TOUCHABLE_SPONT_FV_RATIO_MSG);
        spontMinuteVolumeLabel_.setText(MiscStrs::TOUCHABLE_SPONT_MINUTE_VOL_LABEL);
        spontMinuteVolumeLabel_.setMessage(MiscStrs::TOUCHABLE_SPONT_MINUTE_VOL_MSG);
    }

    addDrawable(&inspTidalVolLabel_);
    addDrawable(&spontFvRatioLabel_);
    addDrawable(&spontMinuteVolumeLabel_);


} // end method


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateSettingApplicability_
//
//@ Interface-Description
// Handles updating the flags which indicate if a 
// setting is applicable or not.
// Because of design constraints the functionality of this
// method could not be placed in the batchSettingUpdate.
//---------------------------------------------------------------------
//@ Implementation-Description
// none
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

void
MoreDataSubScreen::updateSettingApplicability_(void)
{
   
   const ContextSubject *const  P_ACCEPTED_SUBJECT =
   						getSubjectPtr_(ContextId::ACCEPTED_CONTEXT_ID);

   const DiscreteValue  VENT_TYPE_VALUE =
   			P_ACCEPTED_SUBJECT->getSettingValue(SettingId::VENT_TYPE);
   const DiscreteValue  MODE_VALUE =
   			P_ACCEPTED_SUBJECT->getSettingValue(SettingId::MODE);
   const DiscreteValue  SPONT_TYPE_VALUE =
   			P_ACCEPTED_SUBJECT->getSettingValue(SettingId::SUPPORT_TYPE);
   const DiscreteValue  MAND_TYPE_VALUE =
   			P_ACCEPTED_SUBJECT->getSettingValue(SettingId::MAND_TYPE);
   const DiscreteValue  APNEA_MAND_TYPE_VALUE =
   			P_ACCEPTED_SUBJECT->getSettingValue(SettingId::APNEA_MAND_TYPE);
   const DiscreteValue  LEAK_COMP_ENABLED_VALUE =
	   P_ACCEPTED_SUBJECT->getSettingValue(SettingId::LEAK_COMP_ENABLED);
   const DiscreteValue PROX_ENABLED_VALUE =
			   P_ACCEPTED_SUBJECT->getSettingValue(SettingId::PROX_ENABLED);

   IS_AC_ACTIVE = (MODE_VALUE == ModeValue::AC_MODE_VALUE);

   IS_IN_SPONT_MODE = (MODE_VALUE == ModeValue::SPONT_MODE_VALUE) || (MODE_VALUE == ModeValue::CPAP_MODE_VALUE);
   IS_SPONT_TYPE_APPLIC =
       (GuiApp::IsVentStartupSequenceComplete())
        ? (SettingContextHandle::GetSettingApplicability(
                                           ContextId::ACCEPTED_CONTEXT_ID,
                                           SettingId::SUPPORT_TYPE
                                                     ) !=
                                               Applicability::INAPPLICABLE)
        : FALSE;
   IS_PA_ACTIVE = (IS_SPONT_TYPE_APPLIC  &&
   			 SPONT_TYPE_VALUE  == SupportTypeValue::PAV_SUPPORT_TYPE);
   IS_VS_ACTIVE =  (IS_SPONT_TYPE_APPLIC  &&
   	  		 SPONT_TYPE_VALUE  == SupportTypeValue::VSV_SUPPORT_TYPE);			   
   IS_VCP_ACTIVE = (MAND_TYPE_VALUE == MandTypeValue::VCP_MAND_TYPE);
   IS_VCV_ACTIVE = (MAND_TYPE_VALUE == MandTypeValue::VCV_MAND_TYPE);
   IS_APNEA_VCV_ACTIVE = (APNEA_MAND_TYPE_VALUE == MandTypeValue::VCV_MAND_TYPE);
   IS_NIV_ACTIVE = (VENT_TYPE_VALUE == VentTypeValue::NIV_VENT_TYPE);
   IS_LEAK_COMP_ACTIVE = (LEAK_COMP_ENABLED_VALUE == LeakCompEnabledValue::LEAK_COMP_ENABLED);
   IS_PROX_ACTIVE = (PROX_ENABLED_VALUE == ProxEnabledValue::PROX_ENABLED);
} // end method

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateDataValue_
//
//@ Interface-Description
// Handles update event notification for patient data fields.  The
// 'patientDataId' identifies which patient data value changed so that
// we know which data value to update.
//---------------------------------------------------------------------
//@ Implementation-Description
// Updates the display of a patient data field to the current measured
// value of the patient data item.  If the value for a patient data
// field is outside the allowable range (maintained by the Patient Data
// subsystem), then flash the (clipped) numeric value.  This processing
// only occurs if this subscreen is currently displayed.
//
// $[01179] -- spontaneous data blank after 2 minutes of no updates...
// $[TC01001] The More Data shall display the current measured value for the ...
//---------------------------------------------------------------------
//@ PreCondition
// The given 'patientDataId' value must be one of the id's associated
// with the patient data fields in this area.
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

void 
MoreDataSubScreen::updateDataValue_(
							const PatientDataId::PatientItemId patientDataId
								   )
{
	CALL_TRACE("updateDataValue_(patientDataId)");

	// Get the new value of the patient datum
	BreathDatumHandle breathDatumHandle(patientDataId);
	BoundedBreathDatum boundedDatum;
	DiscreteBreathDatum discreteDatum;

	if (PatientDataMgr::IsBoundedId(patientDataId))
	{												// $[TI1]
		boundedDatum = breathDatumHandle.getBoundedValue();
	}
	else
	{												// $[TI2]
		discreteDatum = breathDatumHandle.getDiscreteValue();

		// To eliminate multiple update for breath type item
		if (patientDataId == PatientDataId::BREATH_TYPE_ITEM)
		{											// $[TI2.1]
			if (previousPData_ != discreteDatum.data)
			{										// $[TI2.1.1]
				previousPData_ = discreteDatum.data;
			}
			else
			{										// $[TI2.1.2]
				return;
			}
		}											// $[TI2.2]
	}


    updateSettingApplicability_();

    { // dummy extra braces to maintain indentation...

		// the timeouts of 'EXH_SPONT_MINUTE_VOL_ITEM', 'SPONT_PERCENT_TI_ITEM',
		// 'SPONT_RATE_TO_VOLUME_RATIO_ITEM' are all based on the timer of
		// 'EXH_SPONT_TIDAL_VOL', therefore get the state of that timer...
		BreathDatumHandle  exhSpontTidalVolHandle(
									PatientDataId::EXH_SPONT_TIDAL_VOL_ITEM);

		const BoundedBreathDatum&  EXH_SPONT_TIDAL_VOL_DATUM =
									exhSpontTidalVolHandle.getBoundedValue();

		switch (patientDataId)
		{
		case PatientDataId::EXH_SPONT_MINUTE_VOL_ITEM:	// $[TI1.3]
			// $[01335] The more data shall display the current measured value ...
			if (spontMinuteVolumeLabel_.getShow())
			{										  	// $[TI1.3.1]
				spontMinuteVolumeValue_.setValue(boundedDatum.data.value);
				spontMinuteVolumeValue_.setPrecision(boundedDatum.data.precision);

				// If EXH_SPONT_TIDAL_VOL is timeout
				// do not display the spontMinuteVolume.
				if (EXH_SPONT_TIDAL_VOL_DATUM.timedOut)
				{										// $[TI1.3.1.1]
					spontMinuteVolumeValue_.setShow(FALSE);
					spontMinuteVolumeUnit_.setShow(FALSE);
				}
				else
				{										// $[TI1.3.1.2]
					spontMinuteVolumeValue_.setShow(TRUE);
					spontMinuteVolumeUnit_.setShow(TRUE);
	
					// Flash the value if it's out of range
					if (boundedDatum.clippingState == BoundedValue::ABSOLUTE_CLIP)
					{									// $[TI1.3.1.2.1]
						spontMinuteVolumeValue_.setColor(
											Colors::WHITE_ON_EXTRA_DARK_GREY_BLINK);
					}
					else
					{									// $[TI1.3.1.2.2]
						spontMinuteVolumeValue_.setColor(Colors::WHITE);
					}
				}
			}											// $[TI1.3.2]
			break;

		case PatientDataId::DELIVERED_O2_PERCENTAGE_ITEM:// $[TI1.4]
			// $[01336] The more data shall display the current measured value ...
			deliveredOxygenPercentValue_.setValue(boundedDatum.data.value);
			deliveredOxygenPercentValue_.setPrecision(boundedDatum.data.precision);

			if (boundedDatum.timedOut ||
				IsEquivalent(boundedDatum.data.value, -1.0F, ONES))
			{											// $[TI1.4.1]
				deliveredOxygenPercentValue_.setShow(FALSE);
				deliveredOxygenPercentUnit_.setShow(FALSE);
			}
			else
			{											// $[TI1.4.2]
				deliveredOxygenPercentValue_.setShow(TRUE);
				deliveredOxygenPercentUnit_.setShow(TRUE);

				// Flash the value if it's out of range
				if (boundedDatum.clippingState == BoundedValue::ABSOLUTE_CLIP)
				{										// $[TI1.4.2.1]
					deliveredOxygenPercentValue_.setColor(
										Colors::WHITE_ON_EXTRA_DARK_GREY_BLINK);
				}
				else
				{										// $[TI1.4.2.2]
					deliveredOxygenPercentValue_.setColor(Colors::WHITE);
				}
			}
			break;

		case PatientDataId::IS_PEEP_RECOVERY_ITEM:		// $[TI1.5]
			if (discreteDatum.data)
			{											// $[TI1.5.1]
				// Make sure the patient data field labels and values are hidden
				// only when we are in peep recovery mode (TRUE state).
				spontMinuteVolumeUnit_.setShow(FALSE);
				spontMinuteVolumeValue_.setShow(FALSE);

				deliveredOxygenPercentUnit_.setShow(FALSE);
				deliveredOxygenPercentValue_.setShow(FALSE);

				inspTidalVolUnit_.setShow(FALSE);
				inspTidalVolValue_.setShow(FALSE);

				endInspiratoryPressureUnit_.setShow(FALSE);
				endInspiratoryPressureValue_.setShow(FALSE);

				endExhalationPressureUnit_.setShow(FALSE);
				endExhalationPressureValue_.setShow(FALSE);

                pavComplianceUnit_.setShow(FALSE);
                pavComplianceValue_.setShow(FALSE);
                pavComplianceMarker_.setShow(FALSE);

                pavElastanceUnit_.setShow(FALSE);
                pavElastanceValue_.setShow(FALSE);
                pavElastanceMarker_.setShow(FALSE);

                pavResistanceUnit_.setShow(FALSE);
                pavResistanceValue_.setShow(FALSE);
                pavResistanceMarker_.setShow(FALSE);

                totalResistanceUnit_.setShow(FALSE);
                totalResistanceValue_.setShow(FALSE);
                totalResistanceMarker_.setShow(FALSE);

                totalWorkOfBreathingUnit_.setShow(FALSE);
                totalWorkOfBreathingValue_.setShow(FALSE);

                pavIntrinsicPeepValue_.setShow(FALSE);
                pavIntrinsicPeepUnit_.setShow(FALSE);

				spontPercentTiValue_.setShow(FALSE);
				spontFvRatioValue_.setShow(FALSE);
				normalizeSpontFvRatioValue_.setShow(FALSE);
				spontInspTimeUnit_.setShow(FALSE);
				spontInspTimeValue_.setShow(FALSE);

				percentLeakValue_.setShow(FALSE);
				percentLeakUnit_.setShow(FALSE);

				exhLeakRateValue_.setShow(FALSE);
				exhLeakRateUnit_.setShow(FALSE);

				inspLeakVolValue_.setShow(FALSE);
				inspLeakVolUnit_.setShow(FALSE);
			}											// $[TI1.5.2]
			break;

		case PatientDataId::INSP_TIDAL_VOL_ITEM: // $[TI1.6]
			// $[TC03000] When applicable (TC, VS, PA, VC+), the compliance and
			// BTPS compensated value of inspired tidal volume shall be displayed on
			// the More Data subscreen.
			inspTidalVolValue_.setValue(boundedDatum.data.value);
			inspTidalVolValue_.setPrecision(boundedDatum.data.precision);

			if (inspTidalVolLabel_.getShow()  && !boundedDatum.timedOut)
			{											// $[TI1.6.1]
				inspTidalVolValue_.setShow(TRUE);
				inspTidalVolUnit_.setShow(TRUE);
			}
			else
			{											// $[TI1.6.2]
				inspTidalVolValue_.setShow(FALSE);
				inspTidalVolUnit_.setShow(FALSE);
			}

			// Flash the value if it's out of range
			if (boundedDatum.clippingState == BoundedValue::ABSOLUTE_CLIP)
			{											// $[TI1.6.3]
				inspTidalVolValue_.setColor(
								  Colors::WHITE_ON_EXTRA_DARK_GREY_BLINK);
			}
			else
			{											// $[TI1.6.4]
				inspTidalVolValue_.setColor(Colors::WHITE);
			}
			break;

		case PatientDataId::END_INSP_PRESS_ITEM:		// $[TI1.7]
			endInspiratoryPressureValue_.setValue(boundedDatum.data.value);
			endInspiratoryPressureValue_.setPrecision(boundedDatum.data.precision);
			
			if (!boundedDatum.timedOut && endInspiratoryPressureLabel_.getShow())
			{											// $[TI1.7.1]
				endInspiratoryPressureValue_.setShow(TRUE);
				endInspiratoryPressureUnit_.setShow(TRUE);
			}
			else
			{											// $[TI1.7.2]
				endInspiratoryPressureValue_.setShow(FALSE);
				endInspiratoryPressureUnit_.setShow(FALSE);
			}

			// Flash the value if it's out of range
			if (boundedDatum.clippingState == BoundedValue::ABSOLUTE_CLIP)
		   	{											// $[TI1.7.3]
				endInspiratoryPressureValue_.setColor(
													  Colors::WHITE_ON_EXTRA_DARK_GREY_BLINK);
			}
			else
			{											// $[TI1.7.4]
				endInspiratoryPressureValue_.setColor(Colors::WHITE);
			}
			break;

		case PatientDataId::END_EXP_PRESS_ITEM:		// $[TI16]
			endExhalationPressureValue_.setValue(boundedDatum.data.value);
			endExhalationPressureValue_.setPrecision(boundedDatum.data.precision);
			
			if (!boundedDatum.timedOut && endExhalationPressureLabel_.getShow())
			{											// $[TI16.1.1]
				endExhalationPressureValue_.setShow(TRUE);
				endExhalationPressureUnit_.setShow(TRUE);
			}
			else
			{											// $[TI16.1.2]
				endExhalationPressureValue_.setShow(FALSE);
				endExhalationPressureUnit_.setShow(FALSE);
			}

			// Flash the value if it's out of range
			if (boundedDatum.clippingState == BoundedValue::ABSOLUTE_CLIP)
		   	{											// $[TI16.2.1]
				endExhalationPressureValue_.setColor(
								  Colors::WHITE_ON_EXTRA_DARK_GREY_BLINK);
			}
			else
			{											// $[TI16.2.2]
				endExhalationPressureValue_.setColor(Colors::WHITE);
			}
			break;

        case PatientDataId::PAV_LUNG_ELASTANCE_ITEM :		// $[TI10]
            //-------------------------------------------------------------
            // SPECIAL:  unlike most of the other data items, the PAV-based
            //           elastance and compliance, Cpav and Epav, are keyed
            //           off of Epav's callback only; Cpav's new value is
            //           ready when Epav's callback is received...
            //-------------------------------------------------------------
            {
                BreathDatumHandle  cpavDatumHandle(
                                      PatientDataId::PAV_LUNG_COMPLIANCE_ITEM
                                                  );

                BoundedBreathDatum  epavDatum = boundedDatum;
                BoundedBreathDatum  cpavDatum =
                                            cpavDatumHandle.getBoundedValue();

                pavElastanceValue_.setValue(epavDatum.data.value);
                pavElastanceValue_.setPrecision(epavDatum.data.precision);

                if (!epavDatum.timedOut  &&  pavElastanceLabel_.getShow())
                {											// $[TI10.1]
				    // $[PA03008] -- Rate...at the completion of each.... 
                    pavElastanceValue_.setShow(TRUE);
                    pavElastanceUnit_.setShow(TRUE);
                }
                else
                {											// $[TI10.2]
                    pavElastanceValue_.setShow(FALSE);
                    pavElastanceUnit_.setShow(FALSE);
                }

                pavComplianceValue_.setValue(cpavDatum.data.value);
                pavComplianceValue_.setPrecision(cpavDatum.data.precision);

                if (!cpavDatum.timedOut  &&  pavComplianceLabel_.getShow())
                {											// $[TI10.3]
				    // $[PA03002] -- Rate...at the completion of each.... 
                    pavComplianceValue_.setShow(TRUE);
                    pavComplianceUnit_.setShow(TRUE);
                }
                else
                {											// $[TI10.4]
                    pavComplianceValue_.setShow(FALSE);
                    pavComplianceUnit_.setShow(FALSE);
                }

                const Boolean  IS_VALUE_SHOWN = pavComplianceValue_.getShow();

                // $[PA01002] -- if either Cpav or Epav violate a limit, both
                //               shall indicate the violation...
                if (epavDatum.clippingState == BoundedValue::ABSOLUTE_CLIP ||
                    cpavDatum.clippingState == BoundedValue::ABSOLUTE_CLIP)
                {	// $[TI10.5] -- both should be blinking and bracketed...
                    pavElastanceValue_.setColor(
                                          Colors::WHITE_ON_EXTRA_DARK_GREY_BLINK
                                               );
                    pavElastanceMarker_.setShow(IS_VALUE_SHOWN);

                    pavComplianceValue_.setColor(
                                          Colors::WHITE_ON_EXTRA_DARK_GREY_BLINK
                                                );
                    pavComplianceMarker_.setShow(IS_VALUE_SHOWN);
                }
                else if (epavDatum.clippingState == BoundedValue::VARIABLE_CLIP ||
                         cpavDatum.clippingState == BoundedValue::VARIABLE_CLIP)
                {	// $[TI10.6] -- both bracketed, but not blinking...
                    pavElastanceValue_.setColor(Colors::WHITE);
                    pavElastanceMarker_.setShow(IS_VALUE_SHOWN);

                    pavComplianceValue_.setColor(Colors::WHITE);
                    pavComplianceMarker_.setShow(IS_VALUE_SHOWN);
                }
                else
                {	// $[TI10.7] -- no blinking or bracketing...
                    pavElastanceValue_.setColor(Colors::WHITE);
                    pavElastanceMarker_.setShow(FALSE);

                    pavComplianceValue_.setColor(Colors::WHITE);
                    pavComplianceMarker_.setShow(FALSE);
                }
            }
            break;

        case PatientDataId::TOTAL_AIRWAY_RESISTANCE_ITEM :		// $[TI15]
            //-------------------------------------------------------------
            // SPECIAL:  unlike most of the other data items, the two PAV-based
            //           resistances, Rtot and Rpav, are keyed off of Rtot's
            //           callback only; Rpav's new value is ready when Rtot's
            //           callback is received...
            //-------------------------------------------------------------
            {
                BreathDatumHandle  rpavDatumHandle(
                                      PatientDataId::PAV_PATIENT_RESISTANCE_ITEM
                                                  );

                BoundedBreathDatum  rtotDatum = boundedDatum;
                BoundedBreathDatum  rpavDatum =
                                            rpavDatumHandle.getBoundedValue();

                totalResistanceValue_.setValue(rtotDatum.data.value);
                totalResistanceValue_.setPrecision(rtotDatum.data.precision);

                if (!rtotDatum.timedOut  &&  totalResistanceLabel_.getShow())
                {											// $[TI15.1]
				    // $[PA03020] -- Rate...at the completion of each.... 
                    totalResistanceValue_.setShow(TRUE);
                    totalResistanceUnit_.setShow(TRUE);
                }
                else
                {											// $[TI15.2]
                    totalResistanceValue_.setShow(FALSE);
                    totalResistanceUnit_.setShow(FALSE);
                }

                const Boolean  IS_RTOT_VALUE_SHOWN
                                            = totalResistanceValue_.getShow();

                // $[PA01003] -- if Rpav violates a limit, its value, along with
                //               Rtot's value, shall be bracketed...
                // $[PA01004] -- if Rtot violates a limit, its value shall be
                //               bracketed...
                switch (rtotDatum.clippingState)
                {
                case BoundedValue::UNCLIPPED :							// $[TI15.3]
                    totalResistanceValue_.setColor(Colors::WHITE);
                    // if Rpav is being bracketed, then Rtot needs to be
                    // bracketed...
                    totalResistanceMarker_.setShow(
                                        (rpavDatum.clippingState != BoundedValue::UNCLIPPED
                                         &&  IS_RTOT_VALUE_SHOWN)
                                                  );
                    break;
                case BoundedValue::ABSOLUTE_CLIP :						// $[TI15.4]
                    totalResistanceValue_.setColor(
                                          Colors::WHITE_ON_EXTRA_DARK_GREY_BLINK
                                                  );
                    totalResistanceMarker_.setShow(IS_RTOT_VALUE_SHOWN);
                    break;
                case BoundedValue::VARIABLE_CLIP :						// $[TI15.5]
                    totalResistanceValue_.setColor(Colors::WHITE);
                    totalResistanceMarker_.setShow(IS_RTOT_VALUE_SHOWN);
                    break;
                default :
                    // unexpected clipping state...
                    AUX_CLASS_ASSERTION_FAILURE(rtotDatum.clippingState);
                    break;
                }

                pavResistanceValue_.setValue(rpavDatum.data.value);
                pavResistanceValue_.setPrecision(rpavDatum.data.precision);

                if (!rpavDatum.timedOut  &&  pavResistanceLabel_.getShow())
                {											// $[TI15.6]
				    // $[PA03005] -- Rate...at the completion of each.... 
                    pavResistanceValue_.setShow(TRUE);
                    pavResistanceUnit_.setShow(TRUE);
                }
                else
                {											// $[TI15.7]
                    pavResistanceValue_.setShow(FALSE);
                    pavResistanceUnit_.setShow(FALSE);
                }

                const Boolean  IS_RPAV_VALUE_SHOWN = pavResistanceValue_.getShow();

                // $[PA01003] -- if Rpav violates a limit, its value shall be
                //               bracketed...
                switch (rpavDatum.clippingState)
                {
                case BoundedValue::UNCLIPPED :						// $[TI15.8]
                    pavResistanceValue_.setColor(Colors::WHITE);
                    // if Rtot is being bracketed, then Rpav needs to be
                    // bracketed...
                    pavResistanceMarker_.setShow(
                                        (rtotDatum.clippingState != BoundedValue::UNCLIPPED
                                         &&  IS_RPAV_VALUE_SHOWN)
                                                  );
                    break;
                case BoundedValue::ABSOLUTE_CLIP :					// $[TI15.9]
                    pavResistanceValue_.setColor(
                                          Colors::WHITE_ON_EXTRA_DARK_GREY_BLINK
                                                );
                    pavResistanceMarker_.setShow(IS_RPAV_VALUE_SHOWN);
                    break;
                case BoundedValue::VARIABLE_CLIP :					// $[TI15.10]
                    pavResistanceValue_.setColor(Colors::WHITE);
                    pavResistanceMarker_.setShow(IS_RPAV_VALUE_SHOWN);
                    break;
                default :
                    // unexpected clipping state...
                    AUX_CLASS_ASSERTION_FAILURE(rpavDatum.clippingState);
                    break;
                }
            }
            break;

        case PatientDataId::PAV_STATE_ITEM :
            switch (breathDatumHandle.getDiscreteValue().data)
            {
            case PavState::STARTUP :						// $[TI6.1]
	            // $[PA01006] -- display PAV Startup messages
                pavStatusMsg_.setShow(IS_PA_ACTIVE);
                pavIntrinsicPeepLabel_.setShow(FALSE);
                pavIntrinsicPeepValue_.setShow(FALSE);
                pavIntrinsicPeepUnit_.setShow(FALSE);
                break;
            case PavState::CLOSED_LOOP :					// $[TI6.2]
                pavStatusMsg_.setShow(FALSE);
                pavIntrinsicPeepLabel_.setShow(IS_PA_ACTIVE);
                break;
            }
            break;

        case PatientDataId::PAV_PEEP_INTRINSIC_ITEM :		// $[TI9]
            pavIntrinsicPeepValue_.setValue(boundedDatum.data.value);
            pavIntrinsicPeepValue_.setPrecision(boundedDatum.data.precision);

            // Make sure symbol is displayed only when the PAV status message
            // is NOT shown...
            pavIntrinsicPeepLabel_.setShow( pavIntrinsicPeepLabel_.getShow()
                                           &&  !pavStatusMsg_.getShow());

            if (!boundedDatum.timedOut  &&  pavIntrinsicPeepLabel_.getShow())
            {											// $[TI9.1]
				// $[PA03014] -- Rate...at the completion of each.... 
                pavIntrinsicPeepValue_.setShow(TRUE);
                pavIntrinsicPeepUnit_.setShow(TRUE);
            }
            else
            {											// $[TI9.2]
                pavIntrinsicPeepValue_.setShow(FALSE);
                pavIntrinsicPeepUnit_.setShow(FALSE);
            }

            // Flash the value if it's out of range
            if (boundedDatum.clippingState == BoundedValue::ABSOLUTE_CLIP)
            {											// $[TI9.3]
                pavIntrinsicPeepValue_.setColor(
                                      Colors::WHITE_ON_EXTRA_DARK_GREY_BLINK
                                                );
            }
            else
            {											// $[TI9.4]
                pavIntrinsicPeepValue_.setColor(Colors::WHITE);
            }
            break;

        case PatientDataId::TOTAL_WORK_OF_BREATHING_ITEM :		// $[TI11]
            totalWorkOfBreathingValue_.setValue(boundedDatum.data.value);
            totalWorkOfBreathingValue_.setPrecision(boundedDatum.data.precision);

            if (!boundedDatum.timedOut  &&  totalWorkOfBreathingLabel_.getShow())
            {											// $[TI11.1]
                totalWorkOfBreathingValue_.setShow(TRUE);
                totalWorkOfBreathingUnit_.setShow(TRUE);
            }
            else
            {											// $[TI11.2]
                totalWorkOfBreathingValue_.setShow(FALSE);
                totalWorkOfBreathingUnit_.setShow(FALSE);
            }

            // Flash the value if it's out of range
            if (boundedDatum.clippingState == BoundedValue::ABSOLUTE_CLIP)
            {											// $[TI11.1]
                totalWorkOfBreathingValue_.setColor(
                                      Colors::WHITE_ON_EXTRA_DARK_GREY_BLINK
                                               );
            }
            else
            {											// $[TI11.2]
                totalWorkOfBreathingValue_.setColor(Colors::WHITE);
            }
            break;

         case PatientDataId::SPONT_PERCENT_TI_ITEM :		// $[TI1.12]
			spontPercentTiValue_.setValue(boundedDatum.data.value);
			spontPercentTiValue_.setPrecision(boundedDatum.data.precision);

			if (spontPercentTiLabel_.getShow() &&
			    !EXH_SPONT_TIDAL_VOL_DATUM.timedOut)  
			{											// $[TI1.12.1]
				spontPercentTiValue_.setShow(TRUE);
			}
			else
			{											// $[TI1.12.2]
				spontPercentTiValue_.setShow(FALSE);
			}
			
			// Flash the value if it's out of range
			if (boundedDatum.clippingState == BoundedValue::ABSOLUTE_CLIP)
		   	{											// $[TI1.12.3]
				spontPercentTiValue_.setColor(
									  Colors::WHITE_ON_EXTRA_DARK_GREY_BLINK
											 );
			}
			else
			{											// $[TI1.12.4]
				spontPercentTiValue_.setColor(Colors::WHITE);
			}
			break;

		case PatientDataId::SPONT_INSP_TIME_ITEM :		// $[TI1.13]
			spontInspTimeValue_.setValue(boundedDatum.data.value);
			spontInspTimeValue_.setPrecision(boundedDatum.data.precision);

			if (spontInspTimeLabel_.getShow() && !boundedDatum.timedOut)
			{											// $[TI1.13.1]
				spontInspTimeValue_.setShow(TRUE);
				spontInspTimeUnit_.setShow(TRUE);
			}
			else
			{											// $[TI1.13.2]
				spontInspTimeValue_.setShow(FALSE);
				spontInspTimeUnit_.setShow(FALSE);
			}
			
			// Flash the value if it's out of range
			if (boundedDatum.clippingState == BoundedValue::ABSOLUTE_CLIP)
		   	{											// $[TI1.13.3]
				spontInspTimeValue_.setColor(
									  Colors::WHITE_ON_EXTRA_DARK_GREY_BLINK
										    );
			}
			else
			{											// $[TI1.13.4]
				spontInspTimeValue_.setColor(Colors::WHITE);
			}
			break;

		case PatientDataId::SPONT_RATE_TO_VOLUME_RATIO_ITEM :		// $[TI1.14]
			spontFvRatioValue_.setValue(boundedDatum.data.value);
			spontFvRatioValue_.setPrecision(boundedDatum.data.precision);
			
			if(IS_PA_ACTIVE)
			  {
			    // Accepted Context contains the  value of IBW in kg)...
                const Setting*  pIbw = SettingsMgr::GetSettingPtr(SettingId::IBW);
                const Real32  IBW_VALUE = BoundedValue(pIbw->getAcceptedValue()).value;
                const Real32  N_VALUE =  boundedDatum.data.value / IBW_VALUE;
                normalizeSpontFvRatioValue_.setValue(N_VALUE);
                if( N_VALUE >= 10.0F)   /* do not crowd display */
			      {
			         normalizeSpontFvRatioValue_.setPrecision( ONES );
			      }
			     else
			      {
			        normalizeSpontFvRatioValue_.setPrecision( TENTHS );
			      }
              }
			if (spontFvRatioLabel_.getShow()  &&
			    !EXH_SPONT_TIDAL_VOL_DATUM.timedOut)
			{											// $[TI1.14.1]
				spontFvRatioValue_.setShow(TRUE);
				if(IS_PA_ACTIVE)
				 {
				     normalizeSpontFvRatioValue_.setShow(TRUE);
				 }
			}
			else
			{											// $[TI1.14.2]
				spontFvRatioValue_.setShow(FALSE);
				if(IS_PA_ACTIVE)
				 {
				    normalizeSpontFvRatioValue_.setShow(FALSE);
				 }
			}
			
			// Flash the value if it's out of range
			if (boundedDatum.clippingState == BoundedValue::ABSOLUTE_CLIP)
		   	{											// $[TI1.14.3]
				spontFvRatioValue_.setColor(
									  Colors::WHITE_ON_EXTRA_DARK_GREY_BLINK
										   );
										   
			    if(IS_PA_ACTIVE)
			     {
			        normalizeSpontFvRatioValue_.setColor(
									  Colors::WHITE_ON_EXTRA_DARK_GREY_BLINK
										   );
			     
			     }
			}
			else
			{											// $[TI1.14.4]
				spontFvRatioValue_.setColor(Colors::WHITE);
				if(IS_PA_ACTIVE)
				 {
				    normalizeSpontFvRatioValue_.setColor(Colors::WHITE);
				 }
			}
			break;

		case PatientDataId::VTPCV_STATE_ITEM :
			switch (discreteDatum.data)
			{
			case VtpcvState::STARTUP_VTPCV :
				vcpStatusMsg_.setShow(IS_VCP_ACTIVE);									 
				vsStatusMsg_.setShow(IS_VS_ACTIVE);
				break;															 
			case VtpcvState::NORMAL_VTPCV :										 
				if(vcpStatusMsg_.getShow())
				{
					vcpStatusMsg_.setShow(FALSE);
				}
				if(vsStatusMsg_.getShow())
				{
					vsStatusMsg_.setShow(FALSE);
				}
				break;
			default:
				// do nothing...
				break;
			}
			break;
		case PatientDataId::PERCENT_LEAK_ITEM:
			//  Percent leak shall be displayed on the More Data Subscreen.
			percentLeakValue_.setValue(boundedDatum.data.value);
			percentLeakValue_.setPrecision(boundedDatum.data.precision);

			if (!boundedDatum.timedOut && percentLeakLabel_.getShow())
			{									
				percentLeakValue_.setShow(TRUE);
				percentLeakUnit_.setShow(TRUE);
			}
			else
			{										
				percentLeakValue_.setShow(FALSE);
				percentLeakUnit_.setShow(FALSE);
			}

			// Flash the value if it's out of range
			if (boundedDatum.clippingState == BoundedValue::ABSOLUTE_CLIP)
		   	{									
				percentLeakValue_.setColor(
								  Colors::WHITE_ON_EXTRA_DARK_GREY_BLINK);
			}
			else
			{							
				percentLeakValue_.setColor(Colors::WHITE);
			}
			break;
		case PatientDataId::EXH_LEAK_RATE_ITEM:
			//  Exhalation leak rate shall be displayed on the More Data Subscreen.
			exhLeakRateValue_.setValue(boundedDatum.data.value);
			exhLeakRateValue_.setPrecision(boundedDatum.data.precision);

			if (!boundedDatum.timedOut && exhLeakRateLabel_.getShow())
			{											
				exhLeakRateValue_.setShow(TRUE);
				exhLeakRateUnit_.setShow(TRUE);
			}
			else
			{										
				exhLeakRateValue_.setShow(FALSE);
				exhLeakRateUnit_.setShow(FALSE);
			}

			// Flash the value if it's out of range
			if (boundedDatum.clippingState == BoundedValue::ABSOLUTE_CLIP)
		   	{										
				exhLeakRateValue_.setColor(
								  Colors::WHITE_ON_EXTRA_DARK_GREY_BLINK);
			}
			else
			{											
				exhLeakRateValue_.setColor(Colors::WHITE);
			}
			break;
		case PatientDataId::INSP_LEAK_VOL_ITEM:
			//  Inspiratory leak volume shall be displayed on the More Data Subscreen.
			inspLeakVolValue_.setValue(boundedDatum.data.value);
			inspLeakVolValue_.setPrecision(boundedDatum.data.precision);

			if (!boundedDatum.timedOut && inspLeakVolLabel_.getShow())
			{										
				inspLeakVolValue_.setShow(TRUE);
				inspLeakVolUnit_.setShow(TRUE);
			}
			else
			{									
				inspLeakVolValue_.setShow(FALSE);
				inspLeakVolUnit_.setShow(FALSE);
			}

			// Flash the value if it's out of range
			if (boundedDatum.clippingState == BoundedValue::ABSOLUTE_CLIP)
		   	{										
				inspLeakVolValue_.setColor(
								  Colors::WHITE_ON_EXTRA_DARK_GREY_BLINK);
			}
			else
			{										
				inspLeakVolValue_.setColor(Colors::WHITE);
			}
			break;

		default:
			// do nothing?..
			break;
		}  // end switch (patientDataId)
    } // dummy extra braces to maintain indentation...
}  //end method




//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: buttonDownHappened
//
//@ Interface-Description
// Handles button down events for all of the "instant action" screen
// select buttons on this subscreen.  At the moment, it is used for 
// responding to button down events on the RmButton_.
//  The `pButton' parameter is the button that was pressed down.
// The `isByOperatorAction' flag is TRUE if the given button was pressed
// by operator.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Reset the RmButton_ to the "up" state so that it will be in the 
//  correct state the next time this subscreen is displayed.  
//  Call the displayRMDataSubScreen funtion to deactivate this subscreen
//  and activate the RM Data Subscreen.
//---------------------------------------------------------------------
//@ PreCondition
//  isByOperatorAction must be set to TRUE, pButton cannot be NULL and
//  pButton must be a RmButton_ variable.
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
MoreDataSubScreen::buttonDownHappened(Button *pButton,
                                  Boolean isByOperatorAction)
{
    CALL_TRACE("MoreDataSubScreen::buttonDownHappened(pButton, isByOperatorAction)");

    SAFE_CLASS_PRE_CONDITION(pButton != NULL);
    SAFE_CLASS_PRE_CONDITION(TRUE == isByOperatorAction);
    SAFE_CLASS_ASSERTION(pButton == &RmButton_);

    displayRmDataSubScreen_();

    // Reset whichever button was pressed.
    pButton->setToUp();

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: displayRmDataSubScreen_
//
//@ Interface-Description
//  Activates the Respiratory Mechanics Data Subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method activates Respiratory Mechanics Data Subscreen by getting 
//  two required pointers, RM Data Subscreen and the More Data tab 
//  button.  Also set the lastDisplay_ variable to the RM data Subscreen.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void MoreDataSubScreen::displayRmDataSubScreen_()
{
    CALL_TRACE("MoreDataSubScreen::displayRmDataSubScreen_()");

    // Set RM data Subscreen as the last display
    lastDisplay_ = RMDATASUBSCREEN;

    // Retrieve the RM Data Subscreen.
    SubScreen*  pSubscreen = UpperSubScreenArea::GetRmDataSubScreen();
    CLASS_ASSERTION(pSubscreen != NULL);

    // Retrieve the More Data tab button.
    TabButton*  pTabButton = UpperScreen::RUpperScreen.getUpperScreenSelectArea()->
                                          getMoreDataTabButton();
    CLASS_ASSERTION(pTabButton != NULL);


    // Activate the RM subscreen with the More Data tab button.
    // Activating the subscreen automatically forces the associated
    // tab button down. 
    getSubScreenArea()->activateSubScreen(pSubscreen, pTabButton);

}

