#ifndef	SensorInterface_HH
#define SensorInterface_HH

//====================================================================
// This is a proprietary work to which Nellcor Puritan Bennett
// corporation of California claims exclusive right.  No part of this
// work may be used, disclosed, reproduced, sorted in an information
// retrieval system, or transmitted by any means, electronic,
// mechanical, photocopying, recording, or otherwise without the prior
// written permission of Nellcor Puritan Bennett Corporation.
//
//     Copyright (c) 2008, Nellcor Puritan Bennett Corporation
//=====================================================================

//=====================================================================
//@ Class:  SensorInterface - GUI Realtime Waveform Protocol
//                         Handler
//---------------------------------------------------------------------
//@ Version-Information
// @ $Header:   /840/Baseline/GUI-Serial-Interface/vcssrc/SensorInterface.hhv   25.0.4.0   19 Nov 2013 14:12:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002   By: gdc    Date: 26-Jan-2009    SCR Number: 6407
//  Project:  840S
//  Description:
//      Modified to bring source into strict ARM compliance.
//
//  Revision: 001    By: erm   Date: 26-Jan-2009 	SCR Number: 6441
//  Project: S840
//  Description:
//      Initial version.
//
//=====================================================================

#include "GUI_Serial_Interface.hh"
#include "SerialInterface.hh"
#include "SettingId.hh"
#include "SensorDataBuffer.hh"

//@ Usage-Classes
//@ End-Usage


class SerialPort;

class SensorInterface : public SerialInterface
{
  public:
   	enum {MAX_OUTPUT_BUFFER_SIZE = 6000 };
	SensorInterface( SerialPort & rSerialPort,
            SerialInterface::PortNum portNum );
	~SensorInterface();

	// virtual from SerialInterface
	virtual Boolean startPrint(void);
    void createReport(SensorDataBuffer::SensorEntry &sensorData);


	static void  SoftFault(const SoftFaultID softFaultID,
					       const Uint32	lineNumber,
					       const char *	pFileName = NULL,
					       const char *	pPredicate = NULL);

  protected:
	// virtual from SerialInterface
	virtual void processRequests_(void);
	virtual void setCurrentPrinter_(void);

  private:
    //@ Data-Member: outputBuffer_
	// Data to be transnitted
    char   outputBuffer_[MAX_OUTPUT_BUFFER_SIZE]; 

	//@ Data-Member: SeqNum_ 
	// Data to be transnitted
	Uint16 seqNum_;
};

#endif	// ! SensorInterface_HH
