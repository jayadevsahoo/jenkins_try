//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file  GainsManager.hh
/// @brief Definition of the GainsManager class
//----------------------------------------------------------------------------
#ifndef GAINSMANAGER_HH_
#define GAINSMANAGER_HH_

//-------------------------------------
// I N C L U D E S
//-------------------------------------
#include "GainsNames.hh"
#include "GainsConfig.hh"
#include "SigmaTypes.hh"
#include "SoftFaultId.hh"
#include "MutEx.hh"
#include <map>

using namespace std;

class PressureBasedPhase;

//-------------------------------------
// N A M E   S P A C E
//-------------------------------------
/// @namespace GainsManagerNS
/// @brief Namespace containing GainsManager and related classes/enums.
namespace GainsManagerNS
{
    //----------------------------------------------------------------------------
    // C L A S S   D E C L A R A T I O N
    // Declare this class as part of the GainsManagerNS namespace
    //----------------------------------------------------------------------------
    /// @class GainsManager
    /// @brief Singleton class to manage controller gains
    ///
    /// Manages the modification of controller gains. Provides an interface for
    /// setting and getting the gains of the controllers that are made accessible
    /// to this manager through GainsConfig objects. Breath-Delivery is responsible
    /// for loading the default gains that the controllers are using into the
    /// Gains Manager then messaging the Gains Manager to distribute gains to the
    /// controllers. If gains have been modified and the Gains Manager is active
    /// then Gains Manager will distribute modified gains to the controllers.
    //-------------------------------------------------------------------------
    class GainsManager;
};

//-------------------------------------
// C L A S S   D E F I N I T I O N
//-------------------------------------
class GainsManagerNS::GainsManager
{
public:
	//-------------------------------------
	// E N U M E R A T I O N S
	//-------------------------------------
	//-------------------------------------
	/// @enum GainsManagerActive_t
	/// @brief Indicates whether Gains Mgr is managing gains or not
	//-------------------------------------
	typedef enum GainsManagerActive_t
	{
	    GAINS_MGR_ACTIVE = 0,
	    GAINS_MGR_INACTIVE
	} GainsManagerActive_t;

    //-------------------------------------
    /// @enum GainsManagerInitStatus_t
    /// @brief Indicates initiation status
    //-------------------------------------
    typedef enum GainsManagerInitStatus_t
    {
        GAINS_MGR_NOINIT = 0,
        GAINS_MGR_INPROGRESS,
        GAINS_MGR_REGCOMPLETE,
		GAINS_MGR_REGFAILED,
        GAINS_MGR_INITSUCCESS
    } GainsManagerInitStatus_t;

	//-------------------------------------
	/// @enum GainsManagerDistributionStatus_t
	/// @brief Distribution status of gains
	//-------------------------------------
	typedef enum GainsManagerDistributionStatus_t
	{
	    GAINS_MGR_NONE = 0,
	    GAINS_MGR_GAINS_TO_DISTRIBUTE = 1,
	    GAINS_MGR_GAINS_DISTRIBUTED = 2,
	} GainsManagerDistributionStatus_t;

public:
	//--------------------------------------------------------------------
    /// This function creates and provides the singleton instance to
    /// its clients.
    ///
    /// @return The singleton instance of the GainsManager.
    ///
    /// @post An instance of GainsManager is created and is available
    ///       for the clients.
    //--------------------------------------------------------------------
    static GainsManager& getInstance(void);

    //--------------------------------------------------------------------
    /// Indicates whether the GainsManager is active or not.
    ///
    /// @returns Whether the GainsManager is active or not
    //--------------------------------------------------------------------
    GainsManagerActive_t getActive(void) const;

    //--------------------------------------------------------------------
    /// Turns the GainsManager on and off. If the GainsManager is not
    /// active then a call to distribute the gains has no effect.
    ///
    /// @param[in] active - indicates whether GainsManager is on or off
    ///
    /// @pre GainsManger is instantiated
    //--------------------------------------------------------------------
    void setActive( GainsManagerActive_t active );

	//--------------------------------------------------------------------
	/// Internal static method for handling assertions generated by this class
	///
	/// @param[in] softFaultID
	/// @param[in] lineNumber
	/// @param[in] pFileName
	/// @param[in] pPredicate
	//--------------------------------------------------------------------
	static void   SoftFault(const SoftFaultID  softFaultID,
				  const Uint32       lineNumber,
				  const Char*        pFileName = NULL,
				  const Char*        pPredicate = NULL);

	//--------------------------------------------------------------------
	/// Load the gains from the controllers into the GainsManager. This is
	/// only done during GainsManager initialization. This method is used
	/// by Breath-Delivery so if we can't get the mutex return immediately.
	/// Loading gains can wait until the next breath.
	///
	/// @return TRUE if successful, FALSE if initialization is already complete,
	///         if registration is already complete, or there are no gains
	///         to load
	///
	/// @pre GainsManager initialization is in progress
	/// @post GainsManager initialization is complete
	//--------------------------------------------------------------------
	Boolean loadGains(void);

    //--------------------------------------------------------------------
    /// Restores the original gain values loaded from the controllers.
    ///
    /// @return TRUE if successful, FALSE is initialization is not complete
    ///         or if there are no gains registered to restore.
    ///
    /// @pre GainsManager initialization is complete and gains are registered
    //--------------------------------------------------------------------
	Boolean restoreOriginalGains(void);

    //--------------------------------------------------------------------
    /// Distributes the gains that have changed to the controllers
	/// overriding the previous value. This method is used
    /// by Breath-Delivery so if we can't get the mutex return immediately.
    /// Distributing gains can wait until the next breath.
    ///
    /// @return TRUE if successful, FALSE is initialization is not complete,
    ///         if the manager is not active, or if there are no gains
	///         registered to distribute.
    ///
    /// @pre GainsManager initialization is complete, active and there
	///      are gains regisetered.
    //--------------------------------------------------------------------
	Boolean distributeGains(void);

    //--------------------------------------------------------------------
    /// Gets all the gains managed by the gains manager. Returns the gains
	/// using a std::map.
	///
	/// @param[out] std::map - a map of gain names to gain values
	///
    /// @return TRUE if successful, FALSE is initialization is not complete,
    ///         or if there are no gains registered.
    ///
    /// @pre GainsManager initialization is complete
    //--------------------------------------------------------------------
	Boolean getGains( map<GainsManagerNS::GainsNames, Real32>& gmap );

    //--------------------------------------------------------------------
    /// Gets all the original gain value of the gains managed by the gains
	/// manager. Returns the gains using a std::map.
    ///
    /// @param[out] std::map - a map of gain names to gain original values
    ///
    /// @return TRUE if successful, FALSE is initialization is not complete,
    ///         or if there are no gains registered.
    ///
    /// @pre GainsManager initialization is complete
    //--------------------------------------------------------------------
    Boolean getOriginalGains( map<GainsManagerNS::GainsNames, Real32>& gmap );

	//--------------------------------------------------------------------
    /// Sets a new gain and schedules the gain for distribution.
	///
	/// @param[in] name - the name of the gain to set
	/// @param[in] gain - the gain value to set
	///
    /// @return TRUE if successful, FALSE is initialization is not complete,
    ///         or if there are no gains registered.
    ///
    /// @pre GainsManager initialization is complete
    //--------------------------------------------------------------------
	Boolean setGain( GainsManagerNS::GainsNames name, Real32 gain);

    //--------------------------------------------------------------------
    /// Gets a gain.
	///
	/// @param[in]  name - the name of the gain to get
	/// @param[out] gain - the gain value
	///
    /// @return TRUE if successful, FALSE if initialization is not complete,
	///         if there are no gains registered, or if the named gain
	///         is not found in the collection of managed gains.
    ///
    /// @pre GainsManager initialization is complete
    //--------------------------------------------------------------------
	Boolean getGain( GainsManagerNS::GainsNames name, Real32& gain );

    //--------------------------------------------------------------------
    /// Registers a gain into the manager.
	///
    /// @param[in] gainId - the name of the pressure based phase gain to register
	///
    /// @return TRUE if successful, FALSE if initialization is not in
    ///         progress, or if the named gain is already registered,
	///
    /// @pre GainsManager initialization is in progress
    //--------------------------------------------------------------------
	Boolean registerGain( GainsManagerNS::GainsNames gainId );

    //--------------------------------------------------------------------
    /// Indicates to the Gains Manager that all gains have been registered.
	/// Puts the Gains Manager in a state to be ready for loading.
	///
    /// @return TRUE is operation is success, FALSE initialization is not
	///         in progress.
    ///
    /// @pre GainsManager initialization is in progress
	/// @post GainsManager registration is marked as complete.
    //--------------------------------------------------------------------
	Boolean registrationComplete(bool);

    //--------------------------------------------------------------------
    /// Resets the gains manager to prepare for re-initialization. If the
	/// manager was in use (initialization complete, gains registered)
	/// then restore the gains used by the controllers to their original
	/// values.
    ///
	/// @post GainsManager is Inactive, initialization is in progress,
	///       no gains are registered.
    //--------------------------------------------------------------------
    void reset(void);

private:
	//--------------------------------------------------------------------
	/// Default Constructor
    //--------------------------------------------------------------------
    GainsManager(void);

	//--------------------------------------------------------------------
	/// Copy Constructor not implemented
	//--------------------------------------------------------------------
    GainsManager( const GainsManager& ); // not implemented

	//--------------------------------------------------------------------
	/// Destructor not implemented
	//--------------------------------------------------------------------
	~GainsManager(void);

	//--------------------------------------------------------------------
	/// Assignment operator not implmented
	//--------------------------------------------------------------------
	GainsManager& operator=(const GainsManager& ); // not implemented

private:
	static GainsManager* m_instance_ptr;  /// Singleton instance pointer

	GainsManagerActive_t m_active; /// Indicates whether it is active or not
	GainsManagerInitStatus_t m_initStatus; /// Init status of the Gains Manager
	GainsManagerDistributionStatus_t m_distStatus; /// distribution status of gains
	map<GainsManagerNS::GainsNames, GainsManagerNS::GainsConfig*> m_gainsMap;  ///map containing all the gains to manage
};

#endif //GAINSMANAGER_HH_
