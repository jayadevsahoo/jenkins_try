#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Nellcor Puritan Bennett of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Nellcor Puritan Bennett of California.
//
//            Copyright (c) 1995, Nellcor Puritan Bennett
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: SmSwitchConfirmation - monitors the Service Mode Switch and
//   determines if the operator has requested that SST be started
//---------------------------------------------------------------------
//@ Interface-Description
//   The method SmSwitchConfirmation::RequestSmSwitchConfirmation()
//   is defined as the UserEventCallback method for the
//   SST_CONFIRMATION user event.
//   When one of those user event requests is received, the UiEvent
//   NewCycle() method invokes the callback method.
//
//   The RequestSmSwitchConfirmation() and timeUpHappened() methods
//   interface with the BinaryIndicator object for the Service
//   Mode switch to determine if the operator is pressing
//   the switch or not.
//
//   These methods also interface with the UiEvent object
//   for SST confirmation via the
//   setEventStatus() method.
//---------------------------------------------------------------------
//@ Rationale
//   Used to monitor and detect operator press of the Service Mode
//   switch after (s)he has requested that SST be started.
//
//   The method SetIsOkayToEnterSm() is invoked each time a new
//   scheduler takes control to set the IsOkayToEnterSm_ data member to
//   indicate whether or not the operator is allowed to enter SST at
//   this time.  The vent can only be put in service
//   state on power up if no patient is attached.
//---------------------------------------------------------------------
//@ Implementation-Description
//   The method SmSwitchConfirmation::RequestSmSwitchConfirmation()
//   is defined as the UserEventCallback method for the
//   SST_CONFIRMATION user event.  It is
//   invoked to start the SmSwitchConfirmationTimer when the operator
//   requests that one of those events be started and it stops the
//   SmSwitchConfirmationTimer when the operator requests that one
//   of the events be stopped.
//
//   The method timeUpHappened() is invoked every time the
//   SmSwitchConfirmationTimer expires.  Each time this timer expires,
//   the Service mode switch state is determined.  If the operator
//   has pressed the button after requested that SST 
//   has been requested, timeUpHappened() initiates the
//   transition to SST.  This method also
//   cancels the event if the operator fails to take action before
//   the operator timeout period expires.
//
//   The method SetIsOkayToEnterSm() cancels any outstanding requests
//   for SST if the passed parameter is FALSE.
//   In any case, it updates the IsOkayToEnterSm_ data member.
// $[01018]
//---------------------------------------------------------------------
//@ Fault-Handling
// n/a
//---------------------------------------------------------------------
//@ Restrictions
//    Only one instance of this class may be created 
//---------------------------------------------------------------------
//@ Invariants
// n/a
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/SmSwitchConfirmation.ccv   25.0.4.0   19 Nov 2013 14:00:10   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 003  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 002  By:  sp   Date:  25-Feb-1997    DR Number: DCS NONE
//       Project:  Sigma (840)
//       Description:
//             Inspection rework.
//
//  Revision: 001  By:  kam   Date:  06-Nov-1995    DR Number: DCS NONE
//       Project:  Sigma (840)
//       Description:
//             Initial version
//
//=====================================================================

#include "SmSwitchConfirmation.hh"
#include "TimersRefs.hh"
#include "BreathMiscRefs.hh"
#include "VentObjectRefs.hh"
#include "TaskControlAgent.hh"
#include "VentStatus.hh"
#include "BdTasks.hh"

//@ Usage-Classes

#include "IntervalTimer.hh"
#include "BinaryIndicator.hh"

//@ End-Usage

//@ Code...

// constants for use by class methods
//@ Constant: SM_SWITCH_INTERVAL_MS
// check the Service Mode switch every 100ms to see if it is being pressed
const Int32 SM_SWITCH_INTERVAL_MS = 100;

//@ Constant: SM_SWITCH_COUNT_THRESHOLD
// Every SM_SWITCH_INTERVAL_MS, the switch is checked; the operator
// has 5 seconds in which to press the switch; this counter is
// incremented every SM_SWITCH_INTERVAL_MS while waiting for the
// operator to press the switch
const Int32 SM_SWITCH_COUNT_THRESHOLD = 5000 / SM_SWITCH_INTERVAL_MS;

// Initialize static data
Boolean SmSwitchConfirmation::IsOkayToEnterSm_ = FALSE;
Int32 SmSwitchConfirmation::OperatorTimeoutCount_ = 0;
EventData::EventId SmSwitchConfirmation::CurrentUserEvent_ = EventData::SST_CONFIRMATION;


//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SmSwitchConfirmation()  [Constructor]
//
//@ Interface-Description
//    The constructor takes an IntervalTimer reference as an argument.
//---------------------------------------------------------------------
//@ Implementation-Description
//    The private data member rSmSwitchConfirmationTimer_ is initialized with the 
//    argument passed to the base class constructor. 
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

SmSwitchConfirmation::SmSwitchConfirmation(IntervalTimer& rTimer) 
    : TimerTarget(), rSmSwitchConfirmationTimer_(rTimer)
{
    // $[TI1]
    CALL_TRACE("SmSwitchConfirmation::SmSwitchConfirmation(IntervalTimer& rTimer)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~SmSwitchConfirmation()  [Destructor]
//
//@ Interface-Description
// Destructor
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

SmSwitchConfirmation::~SmSwitchConfirmation(void)
{
  CALL_TRACE("~SmSwitchConfirmation(void)");
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: RequestSmSwitchConfirmation
//
//@ Interface-Description
//    This method takes a user event ID and a boolean indicating whether
//    the user has requested to start or stop the event as inputs and
//    returns an eventStatus.
//
//    This method is invoked via the callback mechanism of the UiEvent
//    for SST_CONFIRMATION received from the GUI
//    by the UiEvent class when a user request to start or stop SST
//    Confirmation is detected.
//
//    This method invokes the getState() method of the BinaryIndicator object
//    for the Service Mode Switch.  It also invokes methods to stop and
//    start the interval timer SmSwitchConfirmationTimer_.
//---------------------------------------------------------------------
//@ Implementation-Description
//    Based on the event id, this method sets up a pointer to the
//    user event object (ServiceRequest).
//    It then retrieves the event status of the user event to verify that
//    it is consistent with the requested status.  For example, this method
//    should never be called to start SST Confirmation if the SST Confirmation
//    user event is not currently idle.
//
//    If the requested event status is TRUE (start), the method verifies
//    that the current status is idle.  It then checks the 
//    SmSwitchConfirmation::IsOkayToEnterSm_ flag to make sure that the
//    ventilator is in a state that allows the operator to enter SST. 
//    If IsOkayToEnterSm_ is FALSE, the request is rejected
//    by setting the returned status to REJECTED.  If IsOkayToEnterSm_ 
//    is TRUE, the current status of the Service Mode switch is determined.  
//    If the current state is ON (the operator is already pressing the button), 
//    the returned status is set to PENDING since the operator must release 
//    the button before the user event becomes active.  If the current state 
//    is not ON, the returned status is set to ACTIVE and the SmSwitchConfirmationTimer
//    is started  and the SmSwitchConfirmation::OperatorTimeoutCount_ is
//    reset.
//
//    If the requested event status is FALSE (stop), the method stops
//    the SmSwitchConfirmationTimer and sets the returned status to
//    CANCEL.
//---------------------------------------------------------------------
//@ PreCondition
//    id == EventData::SST_CONFIRMATION
//    current event status and requested event status are consistent
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

EventData::EventStatus
SmSwitchConfirmation::RequestSmSwitchConfirmation (const EventData::EventId id,
                                                    const Boolean eventStatus)
{
    CALL_TRACE("SmSwitchConfirmation::RequestSmSwitchConfirmation(EventData::EventId id,\
                                Boolean eventStatus)");

    UiEvent *pUserEvent;
    
    CLASS_PRE_CONDITION (id == EventData::SST_CONFIRMATION)

    pUserEvent = &RServiceRequest;
    SmSwitchConfirmation::CurrentUserEvent_ = EventData::SST_CONFIRMATION;
    
    EventData::EventStatus currentStatus = pUserEvent->getEventStatus();
    EventData::EventStatus newStatus;

    // If eventStatus is TRUE, the user has requested that SST 
    // begin.  The BD needs to start looking for a
    // press of the button. 
    if (eventStatus)
    {
        // $[TI4]
        // make sure current status is okay.
        CLASS_PRE_CONDITION (currentStatus == EventData::IDLE);

        // Make sure that the user is allowed to enter SST at this time.
        if (SmSwitchConfirmation::IsOkayToEnterSm_)
        {
			newStatus = EventData::ACTIVE;
	
			pUserEvent->setEventStatus(EventData::COMPLETE);

            TaskControlAgent::ChangeState(STATE_SST);

            // turn off alarm if sst is required, otherwise alarm is already off.
            
            if (BdTasks::IsSstRequired())
            { // $[TI4.1.1]
				VentStatus::DirectVentStatusService(VentStatus::BD_AUDIO_ALARM,
					VentStatus::DEACTIVATE);
			}

        }
        else
        {
            // $[TI4.2]
            // Cannot enter Service Mode at this time
            newStatus = EventData::REJECTED;
        }

    }

    // The operator has canceled the request
    else
    {
        // $[TI5]
        // Stop the timer
        RSmSwitchConfirmation.rSmSwitchConfirmationTimer_.stop();

        // The operator has canceled the request.  BD confirms this by sending
        // the cancel status back to the GUI
        newStatus = EventData::CANCEL;
    }

    return (newStatus);
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: timeUpHappened
//
//@ Interface-Description
//    This method overides the pure virtual method of the base class
//    TimerTarget. It takes an interval timer reference as an argument and has no
//    return value.   This method accesses the BinaryIndicator method
//    getState() for the service mode switch and the UiEvent method
//    setEventStatus() to update the event status for SST_CONFIRMATION.
//    If the service switch state is on, then TaskControlAgent is notified.
//---------------------------------------------------------------------
//@ Implementation-Description
//    This method first verifies which User Event (SST Confirmation)
//    is associated with the Service Mode Switch timer.
//    It then gets the current status of the event.
//
//    If the current event status is ACTIVE, the ServiceSwitch is read.  If
//    it is on, the SmSwitchConfirmationTimer is stopped, the event status
//    is set to COMPLETE and SST is entered.
//
//    If the current event status is ACTIVE but the service switch is off,
//    the SmSwitchConfirmation::OperatorTimeoutCount_ is incremented.  If
//    the value exceeds the SM_SWITCH_COUNT_THRESHOLD (indicating that the
//    operator failed to press the button before the timeout expired),
//    the SmSwitchConfirmationTimer is stopped and the event status is
//    set to CANCEL.  If the total timeout period has not elapsed, the timer
//    is restarted.
//
//    If the current event status is PENDING, the state of the service
//    switch is read.  If it is ON, the operator still has not released the
//    switch (he was pressing the switch on entry and must release then
//    repress; he hasn't released it yet).  In this case the
//    SmSwitchConfirmation::OperatorTimeoutCount_ is incremented.    If
//    the value exceeds the SM_SWITCH_COUNT_THRESHOLD,
//    the SmSwitchConfirmationTimer is stopped and the event status is
//    set to CANCEL.  If the total timeout period has not elapsed, the timer
//    is restarted.
//
//    If the current event status is PENDING but the service switch is off,
//    indicating that the operator has released it and the vent should start
//    looking for another press, the event status is updated to ACTIVE.
//---------------------------------------------------------------------
//@ PreCondition
//    The argument passed is checked to be a reference to the SST Confirmation
//    timer.
//    SmSwitchConfirmation::CurrentUserEvent_ == SST_CONFIRMATION 
//      
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
void
SmSwitchConfirmation::timeUpHappened(const IntervalTimer& timer)
{
    CALL_TRACE("SmSwitchConfirmation::timeUpHappened(const IntervalTimer& timer)");
    UiEvent *pUserEvent;

    CLASS_PRE_CONDITION (&timer == &rSmSwitchConfirmationTimer_);
    CLASS_PRE_CONDITION (SmSwitchConfirmation::CurrentUserEvent_ == EventData::SST_CONFIRMATION);

    pUserEvent = &RServiceRequest;

    EventData::EventStatus currentStatus = pUserEvent->getEventStatus();

    if (currentStatus == EventData::ACTIVE)
    {
            
        // Stop the timer and let the update the user event status
        rSmSwitchConfirmationTimer_.stop();
        pUserEvent->setEventStatus(EventData::COMPLETE);

        TaskControlAgent::ChangeState(STATE_SST);

        // turn off alarm if sst is required, otherwise alarm is already off.
        
        if (BdTasks::IsSstRequired())
        { // $[TI4.1.1]
			VentStatus::DirectVentStatusService(VentStatus::BD_AUDIO_ALARM,
				VentStatus::DEACTIVATE);
		}
        

    }  // end if ACTIVE

    else if (currentStatus == EventData::PENDING)
    {
        // $[TI5]
        //Check if the operator is still pressing the service mode switch

        OperatorTimeoutCount_++;
        
        if (RServiceSwitch.getState() == BinaryIndicator::ON)
        {
            // $[TI5.1]
            // Total time that the operator has to press the SST button after
            // entering the SST screen has elapsed.  Cancel the user event
            if (OperatorTimeoutCount_ >= SM_SWITCH_COUNT_THRESHOLD)
            {
                // $[TI5.1.1]
                rSmSwitchConfirmationTimer_.stop();
                pUserEvent->setEventStatus(EventData::CANCEL);
            }

            else
            {
                // $[TI5.1.2]
                // Restart the timer
                rSmSwitchConfirmationTimer_.restart();
            }
        }
        else
        {
            // $[TI5.2]
            // The operator let go of the switch, now the SST confirmation
            // user event is active -- restarts the timer.
            pUserEvent->setEventStatus(EventData::ACTIVE);
            rSmSwitchConfirmationTimer_.restart();
        }
    }
    else
    {
        // $[TI6]
        CLASS_PRE_CONDITION ((currentStatus == EventData::ACTIVE) ||
                             (currentStatus == EventData::PENDING));
    }
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SetIsOkayToEnterSm 
//
//@ Interface-Description
//      This method takes a Boolean as input and returns nothing.  
//	This method sets the flag that determines whether it is possible to 
//	enter service mode and updates the user event status for service request. 
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method sets the data member isOkayToEnterSm_ to the argument.
//      If the argument flag is true, then check the current status.
//	if current status is active or pending, then stop the
//	rSmSwitchConfirmationTimer_ and set service request event to cancel.
//--------------------------------------------------------------------- 
//@ PreCondition
//      CurrentUserEvent_ == EventData::SST_CONFIRMATION 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 
void
SmSwitchConfirmation::SetIsOkayToEnterSm (const Boolean flag)
{
    CALL_TRACE("SmSwitchConfirmation::setIsOkayToEnterSm (Boolean flag)");
    SmSwitchConfirmation::IsOkayToEnterSm_ = flag;

    // If the ventilator has entered a mode that does not allow the operator
    // to enter SST, any outstanding request must be canceled
    if (!flag)
    {

        // $[TI1]
        
        CLASS_PRE_CONDITION (SmSwitchConfirmation::CurrentUserEvent_ == EventData::SST_CONFIRMATION);

        EventData::EventStatus currentStatus = RServiceRequest.getEventStatus();

        // Cancel any outstanding events
        if ((currentStatus == EventData::ACTIVE) ||
            (currentStatus == EventData::PENDING))
        {
            // $[TI1.4]
            RSmSwitchConfirmation.rSmSwitchConfirmationTimer_.stop();
            RServiceRequest.setEventStatus(EventData::CANCEL);
        }
        // $[TI1.5]

    }
    // $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
SmSwitchConfirmation::SoftFault(const SoftFaultID  softFaultID,
                                const Uint32       lineNumber,
                                const char*        pFileName,
                                const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, SMSWITCHCONFIRMATION, lineNumber,
                          pFileName, pPredicate);
}




//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//=====================================================================
//
//  Private Methods...
//
//=====================================================================




