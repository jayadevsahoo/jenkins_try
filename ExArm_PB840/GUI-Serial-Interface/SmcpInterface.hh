#ifndef	SmcpInterface_HH
#define SmcpInterface_HH

//====================================================================
// This is a proprietary work to which Nellcor Puritan Bennett
// corporation of California claims exclusive right.  No part of this
// work may be used, disclosed, reproduced, sorted in an information
// retrieval system, or transmitted by any means, electronic,
// mechanical, photocopying, recording, or otherwise without the prior
// written permission of Nellcor Puritan Bennett Corporation.
//
//     Copyright (c) 1997, Nellcor Puritan Bennett Corporation
//=====================================================================

//=====================================================================
//@ Class:  SmcpInterface - Service-Mode Communications Protocol Interface
//---------------------------------------------------------------------
//@ Version-Information
// @ $Header:   /840/Baseline/GUI-Serial-Interface/vcssrc/SmcpInterface.hhv   25.0.4.0   19 Nov 2013 14:12:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006   By: gdc    Date: 26-Jan-2009    SCR Number: 6407
//  Project:  840S
//  Description:
//      Modified to bring source into strict ARM compliance.
//
//  Revision: 005    By: gdc        Date: 13-Jan-2009  SCR Number: 5944
//  Project: 840S
//  Description:
//  	Modified to allow changes to service mode baud rate without
//  	requiring power cycle.
//
//  Revision: 004    By: erm           Date: 23-Jan-2002  DR Number: 5984
//  Project: GUIComms
//  Description
//   Added External loopback capability.
//
//  Revision: 003    By: hct           Date: 22-OCT-1999  DR Number: 5493
//  Project: GuiComms
//  Description:
//      Add GuiComms functionality.
//
//  Revision: 002    By: Gary Cederquist  Date: 24-Nov-1997  DR Number: 2605
//    Project: Sigma (R8027)
//    Description:
//       Complete restructuring of GUI-Serial-Interface to fix
//       several problems.
//
//  Revision  1.X	By: jdm	Date: X/XX/96	DR Number:
//	Project:    Sigma (R8027)
//	Description:
//		Initial version to coding standards (Integration baseline).
//=====================================================================

#include "GUI_Serial_Interface.hh"
#include "SerialInterface.hh"

//@ Usage-Classes
#include "MsgQueue.hh"
#include "MailBox.hh"
#include "SmcpRequestBuffer.hh"
#include "SmcpResponseBuffer.hh"
#include "UserAnnunciationMsg.hh"
//@ End-Usage


class SerialPort;

class SmcpInterface : public SerialInterface
{
	public:

		SmcpInterface( SerialPort & rSerialPort,
					   SerialInterface::PortNum portNum,
					   IpcId smcpQueueId );
		~SmcpInterface();

		virtual void driver_(void);
		virtual Uint write( const void * pBuffer, Uint count, Uint milliseconds = 0 );
		virtual Uint read( void * pBuffer, Uint count, Uint milliseconds = 0 );

		virtual void enableLoopback(void);
		virtual void disableLoopback(void);
		virtual void enableExternalLoopback(void);
		virtual void disableExternalLoopback(void);

		static  void      SoftFault(const SoftFaultID softFauldID,
									const Uint32 lineNumber,
									const char * pFileName = NULL,
									const char * pPredicate = NULL);

	protected:

		// SerialInterface virtual
		virtual void setSerialParameters_(void);
		virtual void attachToSubjects_(void);
		virtual void detachFromSubjects_(void);

	private:

		void       processQueue_(void);
		void       processRequests_(void);
		void       outputSerialData_(void);

		void       ackPacket_(void);
		void       nakPacket_(void);
		void       sendNak_(void);

		void       loopBack_(void);
		void       postConsumer(  UserAnnunciationMsg::UAMEventType event
								  , Uint8 failure = 0 ) const;


		//@ Data-Member: rApplicationQueue_
		//  the intertask queue to post for messages (queued in the
		//  SmcpRequestBuffer) during normal (non-loopback) mode
		MsgQueue   rApplicationQueue_;

		//@ Data-Member: rLoopbackQueue_
		//  the intertask queue to post for messages (queued in the
		//  SmcpRequestBuffer) during loopback mode
		MsgQueue   rLoopbackQueue_;

		//@ Data-Member: rSerialInterfaceQueue_
		//  mailbox queue for the Serial-Interface task (ie. self)
		MailBox    rSerialInterfaceQueue_;

		//@ Data-Member: pConsumerQueue_
		//  pointer to the current consumer (application) queue
		MsgQueue * pConsumerQueue_;

		//@ Data-Member: loopbackEnabled_
		//  TRUE when looback mode is enabled
		Boolean    loopbackEnabled_;

		//@ Data-Member: rSmcpRequestBuffer_
		//  SerialBuffer in which SMCP Request Messages are queued from the
		//  Serial-Interface task to the application task (GUI-Application or 
		//  Service-Mode)
		SmcpRequestBuffer rSmcpRequestBuffer_;

		//@ Data-Member: rSmcpResponseBuffer_
		//  SerialBuffer in which SMCP Response Messages are queued from the
		//  application to the Serial-Interface task
		SmcpResponseBuffer rSmcpResponseBuffer_;
};

#endif	// ! SmcpInterface_HH
