
#=====================================================================
# This is a proprietary work to which Puritan-Bennett corporation of
# California claims exclusive right.  No part of this work may be used,
# disclosed, reproduced, stored in an information retrieval system, or
# transmitted by any means, electronic, mechanical, photocopying,
# recording, or otherwise without the prior written permission of
# Puritan-Bennett Corporation of California.
#
#            Copyright (c) 1993, Puritan-Bennett Corporation
#=====================================================================

#=====================================================================
#@ Filename:  SymbolProcessFile.sed -- 'sed' filter file for converting
#                                      all symbol names to symbol values.
#---------------------------------------------------------------------
# This is used by 'processStrings.x', to replace all of the symbol names
# (e.g., '[Vt]') in the '.IN' files, to the corresponding symbol values
# (e.g., 'V{S:T}') in the '.in' files.  This file must be modified when
# new symbols are added or modified.
#---------------------------------------------------------------------
#
#@ Version-Information
# @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/SymbolProcessFile1.sev   25.0.4.0   19 Nov 2013 14:08:34   pvcs  $
#
#@ Modification-Log
#
#  Revision: 023  By: sah	Date: 02-Oct-2000   DCS Number:  5755
#  Project:  VTPC
#  Description:
#      VTPC Project-related changes:
#      *  deleted 'MEAN P' which was obsoleted a long time ago
#
#  Revision: 022  By: sah	Date: 10-Apr-2000   DCS Number:  5702
#  Project:  NeoMode
#  Description:
#      As part of the translation effort, I noticed that the '{X,Y}_AXIS_ADJUST'
#      symbols were still being supported, even though they are now obsoleted.
#      Therefore, these fields have now been removed from this file, and each of
#      the Help files.
#
#  Revision 021  By:  gdc    Date: 22-Jul-99     DR Number: 5327
#  Project:   840 Neonatal
#  Description:
#      Changed 'gp' to 'p' and split into SymbolProcessFile1.sed and 
#	   SymbolProcessFile2.sed for Solaris compatibility.
#
#  Revision 020  By:  hct    Date: 18-FEB-99     DR Number: 5322
#  Project:   Sigma   (R8O27)
#  Description:
#      Added new symbol for HI Pcomp LIMIT.
#
#  Revision 019   By:  hhd    Date: 03-FEB-99     DR Number: 
#  Project:   Sigma   (R8O27)
#  Description:
#      Added new symbol for HI Vti spont LIMIT.
#
#  Revision 018   By:  hct    Date: 14-APR-99     DR Number: 5322
#  Project:   Sigma   (R8O27)
#  Description:
#      Add ATC functionality.
#
#  Revision: 004  By:  yyy    Date:  01-APR-98    DR Number: 5252
#  Project:  Sigma (R8027)
#  Description:
#      Mapping SRS to code.
#
#  Revision: 003  By: btray  Date:  03-Nov-1998     DR Number: 
#  Project:  Sigma (R8027)
#  Description:
#	Fix for DCS# 5250; Removed language specific symbols [Th] and [Tl]
#
#  Revision: 002  By: dosman Date:  29-Apr-1998    DR Number: 34
#  Project:  Sigma (R8027)
#  Description:
#	added symbols for inspiratory time bilevel
#	and expiratory time bilevel 
#   merged /840/Baseline/GUI-Applications/vcssrc/SymbolProcessFile.sev   1.0.1.0  (Color)
#
#  Revision: 001  By: sah    Date:  11-Feb-1998    DR Number: 5004
#  Project:  Sigma (R8027)
#  Description:
#       Integration baseline.
#
#=====================================================================

# $[01004] To minimize the use of screen "real-estate" ...
# $[01005] The following tables lists the settings, ...
# from symbol name directly into its octal character code...
s/\[Vdot\]/\\001/g
#*** \002 is available for use  ***
s/\[DN_ARROW\]/\\003/g
s/\[UP_ARROW\]/\\004/g
s/\[FLOW_ACCEL\]/\\005/g
s/\[RAMP\]/\\006/g
s/\[SQUARE\]/\\007/g
s/\[SINE\]/\\010/g
s/\[OTHER_SCREENS\]/\\011/g
s/\[DN_ARROW_BAR\]/\\012/g
s/\[UP_ARROW_BAR\]/\\013/g
s/\[ALARM_SYM\]/\\014/g
s/\[>=\]/\\015/g
s/\[<=\]/\\016/g
s/\[PADLOCK\]/\\017/g
s/\[CONTRAST\]/\\020/g
s/\[BRIGHTNESS\]/\\021/g
s/\[ALARM_VOLUME\]/\\022/g
s/\[ALARM_SILENCE\]/\\023/g
s/\[MEAN_COMPLIANCE\]/\\024/g
#*** \025 is available for use  ***
s/\[MORE_PATIENT_DATA\]/\\026/g
s/\[ALARM_LOG\]/\\027/g
#*** \030 is available for use  ***
#*** \031 is available for use  ***
s/\[NORMAL_GUI_OP_SYM\]/\\032/g
s/\[DELTA_SYM\]/\\177/g
s/\[BATTERY_SYM\]/\\200/g
s/\[BAR_SYM\]/\\201/g
s/\[SVO_SYM\]/\\215/g
s/\[VENT_INOP_SYM\]/\\216/g
s/\[GUI_INOP_SYM\]/\\217/g
s/\[COMPRESSOR_SYM\]/\\220/g
s/\[BULLET_SYM\]/\\225/g
s/\[NORMAL_VENT_OP_SYM\]/\\235/g
s/\[STOP\]/\\236/g
s/\[GRAPHICS_SYM\]/\\240/g
s/\[1\/2\]/\\275/g
s/\[`A\]/\\300/g
s/\['A\]/\\301/g
s/\[^A\]/\\302/g
s/\[~A\]/\\303/g
s/\[HA\]/\\304/g
s/\[AA\]/\\305/g
s/\[`E\]/\\310/g
s/\['E\]/\\311/g
s/\[^E\]/\\312/g
s/\[HE\]/\\313/g
s/\[`I\]/\\314/g
s/\['I\]/\\315/g
s/\[^I\]/\\316/g
s/\[HI\]/\\317/g
s/\[`O\]/\\322/g
s/\['O\]/\\323/g
s/\[^O\]/\\324/g
s/\[~O\]/\\325/g
s/\[HO\]/\\326/g
s/\[`U\]/\\331/g
s/\['U\]/\\332/g
s/\[^U\]/\\333/g
s/\[HU\]/\\334/g
s/\[`a\]/\\340/g
s/\['a\]/\\341/g
s/\[^a\]/\\342/g
s/\[~a\]/\\343/g
s/\[Ha\]/\\344/g
s/\[Aa\]/\\345/g
s/\[`e\]/\\350/g
s/\['e\]/\\351/g
s/\[^e\]/\\352/g
s/\[He\]/\\353/g
s/\[`i\]/\\354/g
s/\['i\]/\\355/g
s/\[^i\]/\\356/g
s/\[Hi\]/\\357/g
s/\[`o\]/\\362/g
s/\['o\]/\\363/g
s/\[^o\]/\\364/g
s/\[~o\]/\\365/g
s/\[Ho\]/\\366/g
s/\[`u\]/\\371/g
s/\['u\]/\\372/g
s/\[^u\]/\\373/g
s/\[Hu\]/\\374/g

