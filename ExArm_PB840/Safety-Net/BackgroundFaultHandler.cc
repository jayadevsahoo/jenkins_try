#include "stdafx.h"
//===========================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.             
//                                            
//            Copyright (c) 1995, Puritan-Bennett Corporation
//============================================================================

//=========================== C L A S S   D E S C R I P T I O N ==============
//@ Class: BackgroundFaultHandler - independent task for reporting and 
//                                  processing of system failures.
//----------------------------------------------------------------------------
//@ Interface-Description
//  This class is the entry point for reporting all Background errors.
//  When a Background failure event is detected by the Safety-Net systems
//  a message is generated and sent to this task's queue.
//  The method ProcessQueueTask() will process messages put on this task's
//  queue.
//---------------------------------------------------------------------------
//@ Rationale
//  This class handles all Background event fuctionalities. 
//----------------------------------------------------------------------------
//@ Implementation-Description
//  When the Safety-Net Subsystem, which also resides in Breath-Delivery Subsystem
//  and BD-IO-Devices Subsystems, detect a failure, a Background event is
//  declared by passing a message of type BK_EVENT_MSG with the background event
//  ID to BACKGROUND_FAULT_HANDLER_Q queue.  ProcessQueueTask() processes all
//  messages on the BACKGROUND_FAULT_HANDLER_Q queue.
//
//  BkEventsNames are keyed to alarm identifers.  A list of active alarms are
//  checked to determine if an existing alarm type is present, and a new alarm
//  is declared if an alarm does not exist.  Alarm type of BTAT1 will
//  shut down the ventilator by declaring the VENT-INOP condition.
//
//  This task handles messages of type BK_EVENT_MSG, TASK_MONITOR_MSG, and
//  LOG_DIAGNOSTIC_MSG.
//
//  The methods in this class are static and do not require any instantiation.
//----------------------------------------------------------------------------
//@ Fault-Handling
//  N/A
//----------------------------------------------------------------------------
//@ Restrictions
//  None
//----------------------------------------------------------------------------
//@ Invariants
//  None
//----------------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Safety-Net/vcssrc/BackgroundFaultHandler.ccv   25.0.4.0   19 Nov 2013 14:20:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 029  By: rhj    Date: 19-Feb-2010   SCR Number: 6436
//  Project:  PROX
//  Description:
//      Added PROX_ERROR_INFO background fault information.
// 
//  Revision: 028  By: gdc    Date:  18-Aug-2009    SCR Number: 6147
//  Project:  XB
//  Description:
//      Changed Breath Time Monitor (74) test type from a "major" test failure 
// 		to a "minor" test failure.
//
//  Revision: 027  By: gdc    Date:  20-Mar-2007    SCR Number: 6237
//  Project:  Trend
//  Description:
//      Added background event for Compact Flash errors.
//
//  Revision: 026 By: srp    Date: 28-May-2002   DR Number: 5901
//  Project:  VCP
//	Description:
//		Incorporated Remediation code review issue(s) -- commentary changes only.
//
//  Revision: 025  By: hct    Date:  10-JAN-2000    DR Number: DCS 5204
//    Project:  NeoMode (840)
//    Description:
//	On the GUI CPU only, modified ProcessQueueTask to wait until
//      the decision is made to transition to service mode or online
//      to process background fault events.  During initialization 
//      when the decision to go to service mode has not yet been made,
//      background events will be put back on the background fault handler
//      queue.  If the vent is in service mode, all background faults
//      will be logged in the diagnostic log only.  If the vent is not
//      in service mode, background events will be handled normally.
// 
//  Revision: 024  By: hhd    Date:  18-Aug-1999    DR Number: DCS 5513
//    Project:  ATC (840)
//    Description:
//      Modified the Event Definition table so that BK_COMPR_ELAPSED_TIMER,
//      BK_DATAKEY_UPDATE_FAIL, BK_COMPR_BAD_DATA AND BK_COMPR_UPDATE_PM_HRS
//	events will not trigger an alarm.
// 
//  Revision: 023  By: ivc    Date:  19-May-1999    DR Number: DCS 5397
//    Project:  ATC (840)
//    Description:
//      Changed ADC LOOPBACK message from BTAT1 to EVENT_NAME_NULL.
//
//  Revision: 022  By: iv     Date:  03-May-1999    DR Number: 5376
//  Project:  ATC
//  Description:
//      Added background code for BPS events.
//
//  Revision: 021  By: sah    Date:  15-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 020  By: gdc    Date:  15-Oct-1998    DR Number: DCS 5212
//    Project:  BiLevel (840)
//    Description:
//		Changed to process BTAT 4 and BTAT 7 events even when in 
//		service mode in order to turn off/on the GUI/BD loss of 
//		interface LED during communication transitions.
//
//  Revision: 019  By: gdc    Date:  12-Oct-1998    DR Number: DCS 5164
//    Project:  BiLevel (840)
//    Description:
//		Background::ReportBkEvent() no longer latches vent-inop if the
//		operator has requested service mode. This prevents a
//		vent-inop condition when the vent is being brought into
//		service. The background error is still logged in the system
//		diagnostic log however. Since the state of the service mode
//		switch is not known until communication begins with the BD, any
//		background GUI event that occurs before communication begins
//		may cause the GUI to enter the INOP state. For GUI background
//		events that occur after communication begins and the target
//		state (service or online) is known, ProcessBkEvent() has been
//		changed to log them but not bring the GUI to GUI-INOP.
//
//  Revision: 018  By: iv      Date: 30-Mar-1998    DR Number: DCS 5056
//    Project:  Sigma (R8027)
//    Description:
//		 Changed COMPR_UPDATE_SN from btat 3 to NULL event with no error id.
//
//  Revision: 017  By: iv      Date: 07-Jan-1998    DR Number: DCS 5000
//    Project:  Sigma (840)
//    Description:
//       Added a Uint16 default argument to methods ProcessBkEvent() and
//       LogDiagnosticCodeUtil_().
//
//  Revision: 016  By: iv      Date: 11-Nov-1997    DR Number: DCS 2596
//    Project:  Sigma (R8027)
//  Description:
//      Changed background event failure ids for extended BD cycle and extended secondary cycle
//      from MINOR_TEST_FAILURE_ID to NOT_APPLICABLE_TEST_RESULT_ID. Changed the destination log
//      for these events as well as for the waveform drop events from diagnostic code log to the
//      system information log.
//
//  Revision: 015  By: iv      Date: 06-Oct-1997    DR Number: DCS 2543
//    Project:  Sigma (R8027)
//  Description:
//      Added a new event id: BK_TOUCH_SCREEN_RESUME_INFO and changed the routine
//      LogDiagnosticCodeUtil_() to store this event in the information log.
//
//  Revision: 014  By: iv      Date: 26-Sep-1997    DR Number: DCS 1920
//    Project:  Sigma (R8027)
//  Description:
//      Added a new event id: BK_TOUCH_SCREEN_BLOCKED_INFO and changed the routine
//      LogDiagnosticCodeUtil_() to store this event in the information log.
//
//  Revision: 013  By: iv      Date: 12-Sep-1997    DR Number: DCS 1923
//    Project:  Sigma (R8027)
//  Description:
//      Modified logging of 'Init Resume Comm' diagnostics, such that they go
//      directly to the System Information Log, rather than the Diagnostic Code
//      Log.
//
//  Revision: 012   By: dosman  Date: 25-Jun-1997   DR Number: DCS 2233
//    Project:  Sigma (R8027)
//    Description:
//	Added a new background error "DATAKEY_SIZE_ERROR"
//
//  Revision: 011   By: syw    Date: 20-Jun-1997   DR Number: DCS 1902 
//    Project:  Sigma (R8027)
//    Description:
//      Prevent placing the ventilator in INOP state if going to service or if
//		the current state is service.
//
//  Revision: 010   By: syw    Date: 07-May-1997   DR Number: DCS 2004 
//    Project:  Sigma (R8027)
//    Description:
//      Changed COMPR_CHECKSUM to COMPR_BAD_DATA.  Added COMPR_UPDATE_SN,
//		and COMPR_UPDATE_PM_HRS.
//
//  Revision: 009   By: iv    Date: 05-May-1997   DR Number: DCS 2039 
//    Project:  Sigma (R8027)
//    Description:
//      Fixed DeclareVentInop_() to set inop state on the GUI for
//      BTAT 1.
//
//  Revision: 008   By: iv    Date: 29-Apr-1997   DR Number: None 
//    Project:  Sigma (R8027)
//    Description:
//      Rework per unit test peer review.
//
//  Revision: 007   By: iv   Date: 28-Apr-1997   DR Number: 1996
//    Project:  Sigma (840)
//    Description:
//       Added tracing to SRS req. 06001
//
//  Revision: 006   By: iv   Date: 24-Apr-1997   DR Number: 1999
//    Project:  Sigma (840)
//    Description:
//       Fixed code for unit testing.
//
//  Revision: 005   By: iv   Date: 18-Mar-1997   DR Number: 1833
//    Project:  Sigma (840)
//    Description:
//       Removed BTAT8 from the EST required list.
//
//  Revision: 004   By: iv   Date: 17-Mar-1997   DR Number: 1739
//    Project:  Sigma (840)
//    Description:
//       When a previous major POST fault exists, the GUI CPU is responsible
//       to declare BTAT 16 whereas the BD CPU declares BTAT 1.
//
//  Revision: 003   By: by   Date: 28-Feb-1997   DR Number: None
//    Project:  Sigma (840)
//    Description:
//       When a BTAT9 and BTAT11 both exist, the alarm type is
//       upgraded to a BTAT1.
//
//  Revision: 002   By: by   Date: 18-Feb-1997   DR Number: 1771
//    Project:  Sigma (840)
//    Description:
//       Modified the arming of EST Required device alert on power-up
//       instead of when an EST Required to reset alert occurs.
//       As a result, ArmEstRequiredAlert_() is obsolete.
//
//  Revision: 001   By: by   Date: 05-DEC-1996   DR Number: None
//    Project:  Sigma (840)
//    Description:
//      Initial version.
//
//===========================================================================

#include "BackgroundFaultHandler.hh"                             

//@ Usage-Classes
#include "AppContext.hh"
#include "AlarmQueueMsg.hh"
#include "Background.hh"                             
#include "BackgroundMsg.hh"
#include "BdGuiEvent.hh"
#include "DiagnosticCode.hh"
#include "IpcIds.hh"
#include "MsgQueue.hh"
#include "MsgTimer.hh"
#include "NovRamManager.hh"
#include "NovRamSemaphore.hh"
//#include "OsUtil.hh"
#include "Post.hh"
#include "Task.hh"
#include "TaskControlQueueMsg.hh"
#include "TaskMonitor.hh"
//TODO E600 #include "Watchdog.hh"
#include "TaskControlAgent.hh"

#if defined( SIGMA_BD_CPU )
#include "VentStatus.hh"
#endif // defined( SIGMA_BD_CPU )

#if defined( SIGMA_SAFETY_NET )
#include <stdio.h>
#endif // defined( SIGMA_SAFETY_NET )


#ifdef SIGMA_GUI_CPU
//  250ms delay
static const Int32 REPOST_TO_QUEUE_DELAY = 250 ;
#endif // SIGMA_GUI_CPU

///////////////////////////////////////////////////////////////////////////
//                DECLARE STATIC PRIVATE DATA MEMBERS
///////////////////////////////////////////////////////////////////////////
Boolean
BackgroundFaultHandler::BkEventStatusTable_[ BK_MAX_EVENT ];

//////////////////////////////////////////////////////////////////////////
// This is the list of Background event definitions.  It contains the
// alarm type, the serverity, and a text string of the alarm ID.
// The BkEventName enumeration defined in Background.hh are keys to
// this table.  Hence, BkEventName and this table must be kept in synch.
//////////////////////////////////////////////////////////////////////////

const BackgroundFaultHandler::BkEventType
BackgroundFaultHandler::BK_EVENT_DEFINITON_TABLE_[ BK_MAX_EVENT ] =
{
    EVENT_NAME_NULL, ::NOT_APPLICABLE_TEST_RESULT_ID, FALSE,  "NO_EVENT", 
    BTAT_1_EN,       ::MAJOR_TEST_FAILURE_ID,         FALSE,  "SV_SWITCHED_SIDE_OOR",  
    BTAT_1_EN,       ::MAJOR_TEST_FAILURE_ID,         FALSE,  "EXH_FLOW_OOR",          
    BTAT_1_EN,       ::MAJOR_TEST_FAILURE_ID,         FALSE,  "O2_PSOL_CURRENT_OOR",   
    BTAT_1_EN,       ::MAJOR_TEST_FAILURE_ID,         FALSE,  "AIR_PSOL_CURRENT_OOR",  
    BTAT_1_EN,       ::MAJOR_TEST_FAILURE_ID,         FALSE,  "EXH_MOTOR_CURR_OOR",    
    BTAT_2_EN,       ::MINOR_TEST_FAILURE_ID,         FALSE,  "EXH_VLV_COIL_TEMP_OOR", 
    BTAT_1_EN,       ::MAJOR_TEST_FAILURE_ID,         FALSE,  "EXH_PRESS_OOR",         
    BTAT_1_EN,       ::MAJOR_TEST_FAILURE_ID,         FALSE,  "INSP_PRESS_OOR",        
    BTAT_1_EN,       ::MAJOR_TEST_FAILURE_ID,         FALSE,  "AIR_FLOW_OOR_HIGH",
    // 10
    BTAT_9_EN,       ::MINOR_TEST_FAILURE_ID,         FALSE,  "AIR_FLOW_OOR_LOW",
    BTAT_10_EN,      ::MINOR_TEST_FAILURE_ID,         FALSE,  "AIR_FLOW_TEMP_OOR",     
    BTAT_1_EN,       ::MAJOR_TEST_FAILURE_ID,         FALSE,  "O2_FLOW_OOR_HIGH",
    BTAT_11_EN,      ::MINOR_TEST_FAILURE_ID,         FALSE,  "O2_FLOW_OOR_LOW",
    BTAT_12_EN,      ::MINOR_TEST_FAILURE_ID,         FALSE,  "O2_FLOW_TEMP_OOR",      
    BTAT_13_EN,      ::MINOR_TEST_FAILURE_ID,         FALSE,  "EXH_FLOW_TEMP_OOR",     
    BTAT_1_EN,       ::MAJOR_TEST_FAILURE_ID,         FALSE,  "BD_10V_SUPPLY_MON_OOR", 
    BTAT_1_EN,       ::MAJOR_TEST_FAILURE_ID,         FALSE,  "BD_12V_FAIL_OOR",       
    BTAT_1_EN,       ::MAJOR_TEST_FAILURE_ID,         FALSE,  "BD_15V_FAIL_OOR",       
    BTAT_1_EN,       ::MAJOR_TEST_FAILURE_ID,         FALSE,  "NEG_15V_FAIL_OOR",
    // 20 
    BTAT_2_EN,       ::MINOR_TEST_FAILURE_ID,         FALSE,  "GUI_12V_FAIL_OOR",      
    BTAT_2_EN,       ::MINOR_TEST_FAILURE_ID,         FALSE,  "GUI_5V_FAIL_OOR", 
    BTAT_1_EN,       ::MAJOR_TEST_FAILURE_ID,         FALSE,  "BD_5V_FAIL_OOR",        
    BTAT_1_EN,       ::MAJOR_TEST_FAILURE_ID,         FALSE,  "O2_PSOL_STUCK",  
    BTAT_1_EN,       ::MAJOR_TEST_FAILURE_ID,         FALSE,  "AIR_PSOL_STUCK", 
    BTAT_1_EN,       ::MAJOR_TEST_FAILURE_ID,         FALSE,  "AIR_PSOL_STUCK_OPEN",   
    BTAT_1_EN,       ::MAJOR_TEST_FAILURE_ID,         FALSE,  "O2_PSOL_STUCK_OPEN",
    BTAT_2_EN,       ::MINOR_TEST_FAILURE_ID,         FALSE,  "ATM_PRESS_OOR",         
    BTAT_8_EN,       ::MINOR_TEST_FAILURE_ID,         TRUE,   "O2_SENSOR_OOR",         
    NOT_BTAT_8_EN,   ::NOT_APPLICABLE_TEST_RESULT_ID, TRUE,   "O2_SENSOR_OOR_RESET",
    // 30 
    BTAT_1_EN,       ::MAJOR_TEST_FAILURE_ID,         FALSE,  "SVO_CURRENT_OOR",       
    BTAT_1_EN,       ::MAJOR_TEST_FAILURE_ID,         FALSE,  "PI_STUCK",    
    BTAT_1_EN,       ::MAJOR_TEST_FAILURE_ID,         FALSE,  "PE_STUCK",    
    BTAT_1_EN,       ::MAJOR_TEST_FAILURE_ID,         FALSE,  "INSP_AUTOZERO_FAIL",    
    BTAT_1_EN,       ::MAJOR_TEST_FAILURE_ID,         FALSE,  "EXH_AUTOZERO_FAIL",
    BTAT_3_EN,       ::MINOR_TEST_FAILURE_ID,         FALSE,  "POWER_FAIL_CAP",
    BTAT_3_EN,       ::MINOR_TEST_FAILURE_ID,         FALSE,  "ALARM_CABLE",           
    BTAT_1_EN,       ::MAJOR_TEST_FAILURE_ID,         FALSE,  "ADC_FAIL_HIGH",
    BTAT_1_EN,       ::MAJOR_TEST_FAILURE_ID,         FALSE,  "ADC_FAIL_LOW",          
    EVENT_NAME_NULL, ::NOT_APPLICABLE_TEST_RESULT_ID, FALSE,  "ADC_LOOPBACK_FAIL", 
    // 40 
    BTAT_6_EN,       ::MINOR_TEST_FAILURE_ID,         TRUE,   "TOUCH_SCREEN_FAIL",
    BTAT_6_EN,       ::MINOR_TEST_FAILURE_ID,         TRUE,   "TOUCH_SCREEN_BLOCKED",
    NOT_BTAT_6_EN,   ::NOT_APPLICABLE_TEST_RESULT_ID, TRUE,   "TOUCH_SCREEN_RESUME",   
    BTAT_2_EN,       ::MINOR_TEST_FAILURE_ID,         FALSE,  "AC_SWITCH_STUCK",       
    BTAT_1_EN,       ::MAJOR_TEST_FAILURE_ID,         FALSE,  "BD_NOVRAM_CHECKSUM",
    BTAT_15_EN,      ::MINOR_TEST_FAILURE_ID,         FALSE,  "BD_TOD_FAIL",
    BTAT_15_EN,      ::MINOR_TEST_FAILURE_ID,         FALSE,  "GUI_TOD_FAIL",          
    BTAT_16_EN,      ::MINOR_TEST_FAILURE_ID,         FALSE,  "GUI_NOVRAM_CHECKSUM",
    BTAT_3_EN,       ::MINOR_TEST_FAILURE_ID,         FALSE,  "BPS_VOLTAGE_OOR",       
    BTAT_3_EN,       ::MINOR_TEST_FAILURE_ID,         FALSE,  "BPS_CURRENT_OOR", 
    // 50
    BTAT_3_EN,       ::MINOR_TEST_FAILURE_ID,         FALSE,  "BPS_MODEL_OOR", 
    BTAT_14_EN,      ::MINOR_TEST_FAILURE_ID,         FALSE,  "EXH_HEATER_OOR",
    BTAT_17_EN,      ::MINOR_TEST_FAILURE_ID,         FALSE,  "GUI_STUCK_KEY",         
    BTAT_1_EN,       ::MAJOR_TEST_FAILURE_ID,         FALSE,  "BD_EEPROM_CHECKSUM",
    BTAT_16_EN,      ::MINOR_TEST_FAILURE_ID,         FALSE,  "GUI_EEPROM_CHECKSUM",   
    BTAT_17_EN,      ::MINOR_TEST_FAILURE_ID,         FALSE,  "GUI_SAAS_COMM_FAIL",    
    EVENT_NAME_NULL, ::NOT_APPLICABLE_TEST_RESULT_ID, FALSE,  "COMPR_ELAPSED_TIMER",
    EVENT_NAME_NULL, ::NOT_APPLICABLE_TEST_RESULT_ID, FALSE,  "COMPR_BAD_DATA",        
    BTAT_4_EN,       ::MINOR_TEST_FAILURE_ID,         TRUE,   "LOSS_OF_GUI_COMM",
    BTAT_7_EN,       ::MINOR_TEST_FAILURE_ID,         TRUE,   "LOSS_OF_BD_COMM",       
    // 60 
    NOT_BTAT_4_EN,   ::NOT_APPLICABLE_TEST_RESULT_ID, TRUE,   "RESUME_GUI_COMM",       
    NOT_BTAT_7_EN,   ::NOT_APPLICABLE_TEST_RESULT_ID, TRUE,   "RESUME_BD_COMM",
    BTAT_5_EN,       ::MINOR_TEST_FAILURE_ID,         FALSE,  "EST_REQUIRED",
    BTAT_17_EN,      ::MINOR_TEST_FAILURE_ID,         FALSE,  "GUI_SAAS_AUDIO_FAIL",   
    BTAT_1_EN,       ::MAJOR_TEST_FAILURE_ID,         FALSE,  "LV_REF_OOR",            
    BTAT_1_EN,       ::MAJOR_TEST_FAILURE_ID,         FALSE,  "SVC_CURRENT_OOR",        
    BTAT_1_EN,       ::MAJOR_TEST_FAILURE_ID,         FALSE,  "BD_MON_ALARMS_FAIL",
    BTAT_1_EN,       ::MAJOR_TEST_FAILURE_ID,         FALSE,  "BD_MON_APNEA_ALARM_FAIL",
    BTAT_1_EN,       ::MAJOR_TEST_FAILURE_ID,         FALSE,  "BD_MON_APNEA_INT_FAIL",
    BTAT_1_EN,       ::MAJOR_TEST_FAILURE_ID,         FALSE,  "BD_MON_HIP_FAIL",
    // 70 
    BTAT_1_EN,       ::MAJOR_TEST_FAILURE_ID,         FALSE,  "BD_MON_INSP_TIME_FAIL",
    BTAT_1_EN,       ::MAJOR_TEST_FAILURE_ID,         FALSE,  "BD_MON_NO_DATA",
    BTAT_1_EN,       ::MAJOR_TEST_FAILURE_ID,         FALSE,  "BD_MON_DATA_CORRUPTED",
    BTAT_1_EN,       ::MAJOR_TEST_FAILURE_ID,         FALSE,  "BD_MON_O2_MIXTURE_FAIL",
    BTAT_1_EN,       ::MINOR_TEST_FAILURE_ID,         FALSE,  "BD_MON_BREATH_TIME_FAIL",
	EVENT_NAME_NULL, ::NOT_APPLICABLE_TEST_RESULT_ID, FALSE,  "DATAKEY_UPDATE_FAIL",
    EVENT_NAME_NULL, ::MAJOR_TEST_FAILURE_ID,         TRUE,   "TASK_MONITOR_FAIL", 
    EVENT_NAME_NULL, ::NOT_APPLICABLE_TEST_RESULT_ID, TRUE,   "GUI_WAVEFORM_DROP",
    EVENT_NAME_NULL, ::NOT_APPLICABLE_TEST_RESULT_ID, TRUE,   "BD_WAVEFORM_DROP",
    BTAT_1_EN,       ::MAJOR_TEST_FAILURE_ID,         FALSE,  "FORCED_VENTINOP",
    //80
    EVENT_NAME_NULL, ::NOT_APPLICABLE_TEST_RESULT_ID, TRUE,   "MULT_MAIN_CYCLE_MSGS",
    EVENT_NAME_NULL, ::NOT_APPLICABLE_TEST_RESULT_ID, TRUE,   "MULT_SECOND_CYCLE_MSGS",
    EVENT_NAME_NULL, ::MAJOR_TEST_FAILURE_ID,         TRUE,   "WATCHDOG_FAILURE",
    NOT_BTAT_4_EN,   ::NOT_APPLICABLE_TEST_RESULT_ID, FALSE,  "INIT_RESUME_GUI_COMM",
    NOT_BTAT_7_EN,   ::NOT_APPLICABLE_TEST_RESULT_ID, FALSE,  "INIT_RESUME_BD_COMM",
    BTAT_4_EN,       ::MINOR_TEST_FAILURE_ID,         FALSE,  "INIT_LOSS_GUI_COMM",
    BTAT_7_EN,       ::MINOR_TEST_FAILURE_ID,         FALSE,  "INIT_LOSS_BD_COMM",
    EVENT_NAME_NULL, ::NOT_APPLICABLE_TEST_RESULT_ID, FALSE,  "COMPR_UPDATE_SN",        
    EVENT_NAME_NULL, ::NOT_APPLICABLE_TEST_RESULT_ID, FALSE,  "COMPR_UPDATE_PM_HRS",
    BTAT_3_EN,       ::MINOR_TEST_FAILURE_ID,         FALSE,  "DATAKEY_SIZE_ERROR",
    //90
    EVENT_NAME_NULL, ::NOT_APPLICABLE_TEST_RESULT_ID, TRUE,   "TOUCH_SCREEN_BLOCKED_INFO",
    EVENT_NAME_NULL, ::NOT_APPLICABLE_TEST_RESULT_ID, TRUE,   "TOUCH_SCREEN_RESUME_INFO",
    EVENT_NAME_NULL, ::NOT_APPLICABLE_TEST_RESULT_ID, TRUE,   "BPS_EVENT_INFO",
    EVENT_NAME_NULL, ::NOT_APPLICABLE_TEST_RESULT_ID, TRUE,   "COMPACT_FLASH_ERROR_INFO",
    EVENT_NAME_NULL, ::NOT_APPLICABLE_TEST_RESULT_ID, TRUE,   "PROX_ERROR_INFO"
};

const OperatingParameterEventName
BackgroundFaultHandler::MINOR_FAULT_BTAT_LIST_[ MAX_MINOR_BTAT_LIST_ITEMS ] = 
{
    BTAT_2_EN,     BTAT_3_EN,  BTAT_4_EN,     NOT_BTAT_4_EN, BTAT_6_EN,
    NOT_BTAT_6_EN, BTAT_7_EN,  NOT_BTAT_7_EN, BTAT_8_EN,     NOT_BTAT_8_EN,
    BTAT_9_EN,     BTAT_10_EN, BTAT_11_EN,    BTAT_12_EN,    BTAT_13_EN,
    BTAT_14_EN,    BTAT_15_EN, BTAT_17_EN
};

const OperatingParameterEventName
BackgroundFaultHandler::EST_REQUIRED_BTAT_LIST_[ MAX_EST_REQUIRED_BTAT_LIST_ITEMS ] = 
{
    BTAT_2_EN,     BTAT_3_EN,  BTAT_9_EN,     BTAT_10_EN,
    BTAT_11_EN,    BTAT_12_EN, BTAT_13_EN,    BTAT_14_EN,    BTAT_15_EN,
    BTAT_17_EN
};


//============================ M E T H O D   D E S C R I P T I O N ============
//@ Method: Initialize 
//
//@ Interface-Description
//  This method accepts no parameter and returns no value.
//
//  This method cleans all minor BTATs from NOVRAM.
//-----------------------------------------------------------------------------
//@ Implementation-Description
//  This method retrieves the current active BTAT state from NOVRAM.  It
//  searches through the array for any minor BTATs and clears it.  Lastly,
//  the new BTAT state is restored to the NOVRAM.
//-----------------------------------------------------------------------------
//@ PreCondition
//  None
//-----------------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=============================================================================
 
void
BackgroundFaultHandler::Initialize( void )
{
    BtatEventState btatEvents;
    Int16          tmpNdx;
    Uint8          btatNdx;
    Uint8          bkEventNdx;
    Boolean        estRequiredAlertArmed = FALSE;

    // Initialize all Background events to FALSE 
    for ( bkEventNdx = 0; bkEventNdx < BK_MAX_EVENT; ++bkEventNdx )
    {
        BkEventStatusTable_[ bkEventNdx ] = FALSE;
    }

    // Get the active alarm events from NOVRAM
    NovRamManager::GetActiveBtatEvents( btatEvents );
 
    // Check for EST Required Device Alerts from previous session 
    for ( btatNdx = 0; btatNdx < MAX_EST_REQUIRED_BTAT_LIST_ITEMS; ++btatNdx )
    {
        tmpNdx = btatEvents.findEntry( EST_REQUIRED_BTAT_LIST_[ btatNdx ] );
 
        if ( tmpNdx != NOT_FOUND )
        {
        // $[TI1]

            if ( ( FALSE == estRequiredAlertArmed ) &&
                 ( NOT_FOUND == btatEvents.findEntry( BTAT_5_EN ) ) )
            {
            // $[TI1.1]

                /////////////////////////////////////////////
                // replace previously existing minor BTAT
                // with EST Required device alert
                /////////////////////////////////////////////
                btatEvents.setBtatState( (Uint8)tmpNdx, BTAT_5_EN );
                estRequiredAlertArmed = TRUE;
            }
            // $[TI1.2]
        }
        // $[TI2]
    }

    // Reset all minor alarms in NOVRAM
    for ( btatNdx = 0; btatNdx < MAX_MINOR_BTAT_LIST_ITEMS; ++btatNdx )
    {
        tmpNdx = btatEvents.findEntry( MINOR_FAULT_BTAT_LIST_[ btatNdx ] );
 
        if ( tmpNdx != NOT_FOUND )
        {
        // $[TI3]
            //////////////////////////////////////////////////
            // clear previously existing minor BTAT
            //////////////////////////////////////////////////
            btatEvents.setBtatState((Uint8) tmpNdx, EVENT_NAME_NULL );
        }
        // $[TI4]
    }

    // Restore new image of active BTAT to NOVRAM 
    NovRamManager::UpdateActiveBtatEvents( btatEvents );
}

//============================ M E T H O D   D E S C R I P T I O N ============
//@ Method: ProcessQueueTask 
//
//@ Interface-Description
//  This method accepts no parameter and returns no value.
//
//  This task process all messages put on the BACKGROUND_FAULT_HANDLER_Q
//  queue.  The messages processed by this method includes TASK_MONITOR_MSG,
//  BK_EVENT_MSG, and  LOG_DIAGNOSTIC_MSG.  All other types of messages 
//  recieved by this task are considered invalid and will cause this task
//  to generate a soft fault.
//-----------------------------------------------------------------------------
//@ Implementation-Description
//  This task process Background Event and Diagnostic Log messages.  It pends 
//  on its queue until a message is received.  Once a message has arrived,
//  it determines its message ID and dispatch the message data to the 
//  appropriate method.
//
//   On the GUI CPU only, modified ProcessQueueTask to wait until
//   the decision is made to transition to service mode or online
//   to process background fault events.  During initialization 
//   when the decision to go to service mode has not yet been made,
//   background events will be put back on the background fault handler
//   queue.  If the vent is in service mode, all background faults
//   will be logged in the diagnostic log only.  If the vent is not
//   in service mode, background events will be handled normally.
//-----------------------------------------------------------------------------
//@ PreCondition
//  None
//-----------------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=============================================================================

void 
BackgroundFaultHandler::ProcessQueueTask( void )
{
    MsgQueue backgroundFaultHandlerQ( ::BACKGROUND_FAULT_HANDLER_Q );
    BackgroundMsg backgroundMsg;
    Int32 queueStatus, rawMessage;
    Uint16 newBkErrorCode;
    BkEventName newBkEventName;

    // Set up callback from Sys-Init when all BD registered tasks
    // are ready.
    AppContext appContext;
 
    appContext.setCallback( BackgroundFaultHandler::ChangeStateCallback );
    appContext.setCallback( BackgroundFaultHandler::GuiReadyCallback );

#ifndef SIGMA_UNIT_TEST
	for(;;)
    {
#endif // SIGMA_UNIT_TEST

        queueStatus = backgroundFaultHandlerQ.pendForMsg( rawMessage );
        CLASS_ASSERTION( Ipc::OK == queueStatus );
    
        backgroundMsg.queueData = rawMessage;
        newBkEventName = (BkEventName) backgroundMsg.queueInfo.eventName;
        newBkErrorCode = backgroundMsg.queueInfo.errorCode;

        switch ( backgroundMsg.tskMsg.msgId )
        {
            case TaskControlQueueMsg::TASK_CONTROL_MSG:
            // $[TI1]
                appContext.dispatchEvent(rawMessage);
                break;

            case TaskMonitorQueueMsg::TASK_MONITOR_MSG:
            // $[TI2]
                TaskMonitor::Report();
                break;

            case BackgroundMsg::BK_EVENT_MSG:
            // $[TI3]
#if defined (SIGMA_GUI_CPU)
                switch (TaskControlAgent::GetMyState())
                {
                  case STATE_ONLINE:  // $[TI3.1]
                  case STATE_INOP:    // $[TI3.2]
                  case STATE_TIMEOUT: // $[TI3.3]
                  case STATE_SST:     // $[TI3.4]
                  case STATE_FAILED:  // $[TI3.5]
                    ProcessBkEvent(newBkEventName, newBkErrorCode);
                    break;
                  case STATE_SERVICE: // $[TI3.6]
                    LogDiagnosticCodeUtil_(newBkEventName, newBkErrorCode);
                    break;
                  case STATE_INIT:    // $[TI3.7]
                  case STATE_START:   // $[TI3.8]
                  case STATE_UNKNOWN: // $[TI3.9]
                    // Put the message back on the queue until the GUI
                    // knows if it going to service mode or not.
                    MsgQueue::PutMsg( ::BACKGROUND_FAULT_HANDLER_Q,
                          backgroundMsg.queueData );
                    Task::Delay(0, REPOST_TO_QUEUE_DELAY);
                    break;
                  default:
                    // Invalid state
                    CLASS_ASSERTION_FAILURE();
                    break;
                }
#elif defined (SIGMA_BD_CPU)
                ProcessBkEvent(newBkEventName, newBkErrorCode);
#endif
                break;
     

            case BackgroundMsg::LOG_DIAGNOSTIC_MSG:
            // $[TI4]
                LogDiagnosticCodeUtil_(newBkEventName, newBkErrorCode);
                break;

            default:
            // $[TI5]
                CLASS_ASSERTION( FALSE );
                break;
        }

#ifndef SIGMA_UNIT_TEST
    } // end if forever loop
#endif // SIGMA_UNIT_TEST

}

//============================ M E T H O D   D E S C R I P T I O N ============
//@ Method: ProcessBkEvent
//
//@ Interface-Description
//  This method accepts two parameters of type BkEventName and Uint16
//  and returns no value.
//
//  This method process all Background events by logging the fault, notifying
//  Alarms Subsystem, and initiating the appropriate system response to the
//  current BTAT.
//------------------------------------------------------------------------------
//@ Implementation-Description
//  A BkEventName Id is used to identify the event to be processed.
//  An ID of BK_NO_EVENT implies no background error.
//
//  This static method takes a BkEventName and determines if an alarm
//  associated with the event currently exists.  If it does not, a technical
//  alarm is issued to the alarm manager.
//
//  $[05048] $[05049] $[05050] $[05051] $[05052] $[05053] $[05054] $[05055]
//  $[05056] $[05057] $[05060] $[05062] $[05063] $[05066] $[05068] $[05071]
//  $[05073] $[05074] $[05075] $[06001] 
//-----------------------------------------------------------------------------
//@ PreCondition
//  The Event name must be valid Background event.
//-----------------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=============================================================================

void
BackgroundFaultHandler::ProcessBkEvent(const BkEventName eventName, const Uint16 errorCode)
{
	CALL_TRACE("BackgroundFaultHandler::ProcessBkEvent(const BkEventName eventName, const Uint16 errorCode)");
    OperatingParameterEventName alarmType;
    DiagnosticCode              bkDiagCode;
    BtatEventState              btatEvents;
    Int16                       inactiveBtatIndex;

#if defined( SIGMA_SAFETY_NET )
printf("BackgroundFaultHandler EVENT OCCURRED: ID = %d [%s], ALARM ID = %d\n",
        eventName,
        BK_EVENT_DEFINITON_TABLE_[ eventName ].pDebugText,
        BK_EVENT_DEFINITON_TABLE_[ eventName ].alarmType );
#endif // defined( SIGMA_SAFETY_NET )

    CLASS_PRE_CONDITION( eventName < BK_MAX_EVENT );

    // Record failures to system or information diagnostic log
    LogDiagnosticCodeUtil_(eventName, errorCode);

    // Get the alarm associated with the BackgroundFaultHandler event
    alarmType = BK_EVENT_DEFINITON_TABLE_[ eventName ].alarmType;

    // Limit this to BTAT events only
    CLASS_PRE_CONDITION( ( alarmType >= BTAT_1_EN ) && ( alarmType <= BTAT_17_EN ) );

	// no further processing of event if we're in service mode if Event 4 or 7
#if defined( SIGMA_BD_CPU )
	if ( Post::IsServiceModeRequested() 
		&& alarmType != BTAT_4_EN
		&& alarmType != NOT_BTAT_4_EN )
#elif defined( SIGMA_GUI_CPU )
	if ( TaskControlAgent::GetMyState() == STATE_SERVICE ||
		 TaskControlAgent::GetTargetState() == STATE_SERVICE 
		&& alarmType != BTAT_7_EN
		&& alarmType != NOT_BTAT_7_EN )
#endif
	{ // $[TI5.1]
		return;
	} // $[TI5.2]

    NovRamManager::GetActiveBtatEvents( btatEvents );

    ////////////////////////////////////////////////////////////////
    // if air and O2 flow sensors have both failed, alarm type
    // must be upgraded to a Vent Inop (BTAT_1_EN).
    ////////////////////////////////////////////////////////////////
    if ( ( ( alarmType == BTAT_9_EN ) &&
           ( btatEvents.findEntry( BTAT_11_EN ) != NOT_FOUND ) ) 
         ||
         ( ( alarmType == BTAT_11_EN ) &&
           ( btatEvents.findEntry( BTAT_9_EN ) != NOT_FOUND ) )
       )
    {
    // $[TI1]
            alarmType = BTAT_1_EN;
    }
    // $[TI2]

    ////////////////////////////////////////////////////////
    // Check to see if the alarm in the list of active
    // alarms in NOVRAM is already present.
    ////////////////////////////////////////////////////////
    const Int16 INDEX = btatEvents.findEntry( alarmType );

    ///////////////////////////////////////////////////////////////////////
    // If a match was found, processing is done, the alarm has
    // already been processed.  
    ///////////////////////////////////////////////////////////////////////
    if ( NOT_FOUND == INDEX )
    {
    // $[TI3]
        ///////////////////////////////////////////////////////////////////
        // Search for an empty entry in the ActiveAlarm list 
        // update the the active alarms with this alarm type
        ///////////////////////////////////////////////////////////////////

        inactiveBtatIndex = btatEvents.findEntry( EVENT_NAME_NULL );

        CLASS_ASSERTION( inactiveBtatIndex != NOT_FOUND );

        btatEvents.setBtatState( (Uint8)inactiveBtatIndex, alarmType );

        ////////////////////////////////////////////////////////////////
        // Check the current BackgroundFaultHandler event to see if
        // it is a reversible event.
        // Note: If the converse alarm does not exist nothing occurs.    
        ////////////////////////////////////////////////////////////////
        ProcessReversibleBtat_( alarmType, btatEvents );

        if ( BTAT_1_EN == alarmType )
        {
        // $[TI3.1]
            // disable NOVRAM Mutex for BTAT_1_EN (VENT INOP)
            NovRamSemaphore::DisableBlocking();
            NovRamManager::UpdateActiveBtatEvents( btatEvents );
            NovRamSemaphore::EnableBlocking();
        }
        else
        {
        // $[TI3.2]
            NovRamManager::UpdateActiveBtatEvents( btatEvents );
        }

        if ( BTAT_1_EN == alarmType )
        {
        // $[TI3.3]

#if defined( SIGMA_PRODUCTION )
#if defined( SIGMA_BD_CPU )

            // BD Vent Inop set flag in POST NOVRAM so on
            // next power cycle Sys_Init will know to go Inop
            Post::SetBackgroundFailed( ::MAJOR );

#elif defined( SIGMA_GUI_CPU )
            // GUI Vent Inop (TUV) notifiy Sys_Init
            TaskControlAgent::FailBd();

#endif // defined( SIGMA_GUI_CPU )
#endif // defined( SIGMA_PRODUCTION )

        }
#if defined( SIGMA_GUI_CPU )
        // Force Sys_Init to declare GUI Inop on the next power cycle
        else if ( alarmType == BTAT_16_EN )
        {
        // $[TI3.4]
            Post::SetBackgroundFailed( ::MAJOR );
        }
        // $[TI3.5]
#endif // defined( SIGMA_GUI_CPU )

        SystemResponseToBtat_( alarmType );

        // Notify Alarm-Analysis Subsystem of current Background Event
        Background::QueueAlarmEvent( alarmType );
    }
    // $[TI4]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GuiReadyCallback 
//
//@ Interface-Description
//  This method accepts a GuiReadyMessage reference and returns no 
//  value.
//  This method is called whenever GUI CPU is transition into ready
//  state.  Active BTATs are redeclared once this condition is detected.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method initiates activaties required after a changed state.
//  When GUI state changes to online, inop, or timeout, it re-issues
//  active BTATs currently in NOVRAM.
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

void
BackgroundFaultHandler::GuiReadyCallback( const GuiReadyMessage& rMessage )
{

    switch ( rMessage.getState() )
    {
        case STATE_ONLINE:
#if defined( SIGMA_GUI_CPU )
        case STATE_INOP:
        case STATE_TIMEOUT:
#endif // defined( SIGMA_GUI_CPU )

        // $[TI1]
            ReissueActiveBtat_();
            break;

#if defined( SIGMA_BD_CPU )
        case STATE_INOP:
        case STATE_TIMEOUT:
#endif // defined( SIGMA_GUI_CPU )
        case STATE_START:
        case STATE_SERVICE:
        case STATE_SST:
        case STATE_INIT:
        case STATE_UNKNOWN:
        case STATE_FAILED:
        // $[TI2]
            break;

        default:
            CLASS_ASSERTION( FALSE );
            break;
    }
}


//==============================================================================
//
//  Protected Methods...
//
//==============================================================================
 
//==============================================================================
//
//  Private Methods...
//
//==============================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ReissueActiveBtat_ 
//
//@ Interface-Description
//  This method accepts no parameter and returns no value.
//  This method will redeclare any active BTATs that require to past
//  EST to reset.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method redeclares any BTAT1 (Major Failures).  In addition,
//  on the GUI CPU, BTAT16 (Major GUI Failures) are also redeclared.
//  When active BTATs that require passing EST to reset is detected,
//  a EST required "Device Alert" is issued to Alarms-Analysis 
//  Subsystem.
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

void
BackgroundFaultHandler::ReissueActiveBtat_( void )
{
    BtatEventState btatEvents;

    // check for existing POST minor errors
    Background::CheckAndProcessPostErrorStatus();
 
    // Get the active alarm events from NOVRAM
    NovRamManager::GetActiveBtatEvents( btatEvents );
 
    // Re-issue active BTAT1 alarm on GUI
    if ( btatEvents.findEntry( BTAT_1_EN ) != NOT_FOUND
#if defined( SIGMA_BD_CPU )
         ||
         TRUE == Post::IsPostFailed()
#endif // defined( SIGMA_BD_CPU )
       )
    {
    // $[TI1]
#if defined( SIGMA_GUI_CPU )
        ///////////////////////////////////////////////////////////
        // Note: If Vent Inop was declared by BD, on subsequent
        // power-up, Sys-Init has already put BD into INOP
        // Hence, this call is only for GUI declared Vent-Inop
        ///////////////////////////////////////////////////////////
        DeclareVentInop_();
#endif // defined( SIGMA_GUI_CPU )
 
        // Reissue a BD Major failure 
        Background::QueueAlarmEvent( BTAT_1_EN );
    }
    // $[TI2]
 
#if defined( SIGMA_GUI_CPU )
    // Re-issue active BTAT16 alarm on GUI
    if ( btatEvents.findEntry( BTAT_16_EN ) != NOT_FOUND ||
         TRUE == Post::IsPostFailed() )
    {
    // $[TI3]
        // Reissue a GUI Major failure 
        Background::QueueAlarmEvent( BTAT_16_EN );
    }
    // $[TI4]
#endif // defined( SIGMA_GUI_CPU )

    if ( btatEvents.findEntry( BTAT_5_EN ) != NOT_FOUND )
    {
    // $[TI5]
        // issue a EST required device alert
        Background::QueueAlarmEvent( BTAT_5_EN );
    }
    // $[TI6]

    // Re-issue all minor alarms in NOVRAM
    for ( Uint8 btatNdx = 0; btatNdx < MAX_MINOR_BTAT_LIST_ITEMS; ++btatNdx )
    {
        Int16 tmpNdx = btatEvents.findEntry( MINOR_FAULT_BTAT_LIST_[ btatNdx ] );
 
        if ( tmpNdx != NOT_FOUND )
        {
        // $[TI7]
            Background::QueueAlarmEvent( MINOR_FAULT_BTAT_LIST_[ btatNdx ] );
        }
        // $[TI8]
    }
}

//============================ M E T H O D   D E S C R I P T I O N ============
//@ Method: LogDiagnosticCodeUtil_ 
//
//@ Interface-Description
//  This method accepts an enum BkEventName and a Uint16 and returns no value.
//  This method logs a Background event to the system diagnostic log or to
//  the information diagnostic log.
//----------------------------------------------------------------------------
//@ Implementation-Description
//  This method logs Background Events to the system diagnostics entry.  If
//  a Background Event has already been logged and it does not have the
//  attribute for mulitple logging (i.e. BK_COMPR_ELAPSED_TIMER) it is not
//  logged again.
//----------------------------------------------------------------------------
//@ PreCondition
//  eventId < BK_MAX_EVENT
//----------------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=============================================================================
 
void
BackgroundFaultHandler::LogDiagnosticCodeUtil_(const BkEventName eventId, const Uint16 errorCode)
{
	CALL_TRACE("BackgroundFaultHandler::LogDiagnosticCodeUtil_(const BkEventName eventId, const Uint16 errorCode)");
    CLASS_PRE_CONDITION( eventId < BK_MAX_EVENT );

    DiagnosticCode bkDiagCode;

#if defined( SIGMA_SAFETY_NET )
printf("\nBackgroundFaultHandler::LogDiagnosticCodeUtil: %s.\n",
          BackgroundFaultHandler::GetBkEventName( eventId ) );
#endif // defined( SIGMA_SAFETY_NET )

#if	defined(SIGMA_BD_CPU) 
BkEventName  commInitResumeId = BK_INIT_RESUME_GUI_COMM;
#elif   defined(SIGMA_GUI_CPU)
BkEventName  commInitResumeId = BK_INIT_RESUME_BD_COMM;
#endif //defined(SIGMA_GUI_CPU)

    // Prevent multiple entries for the same
    // Background fault, except for informational events
    if ( ( FALSE == BkEventStatusTable_[ eventId ] ) ||
         ( TRUE == BK_EVENT_DEFINITON_TABLE_[ eventId ].multipleLoggingRequired ) )
    {
    // $[TI1]
        // Update status table for this Background Event
        BkEventStatusTable_[ eventId ] = TRUE;
      
        // Record failures to diagnostic code log
        bkDiagCode.setBackgroundTestCode(
            BK_EVENT_DEFINITON_TABLE_[ eventId ].failureType, eventId, errorCode);

        if ( eventId == commInitResumeId ||
            ( BK_EVENT_DEFINITON_TABLE_[ eventId ].alarmType == EVENT_NAME_NULL &&
              BK_EVENT_DEFINITON_TABLE_[ eventId ].failureType == ::NOT_APPLICABLE_TEST_RESULT_ID ) )
        {
        // $[TI1.1]
            NovRamManager::LogSystemInformation( bkDiagCode );
        }
        else
        {
        // $[TI1.2]
			FaultHandler::LogDiagnosticCode( bkDiagCode );
        }
    }
    // $[TI2]
}

//============================ M E T H O D   D E S C R I P T I O N ============
//@ Method: SystemResponseToBtat_ 
//
//@ Interface-Description
//  This method accepts an OperatingParameterEventName enumeration
//  and returns no value.
//
//  This method executes the system response to various Background events.
//----------------------------------------------------------------------------
//@ Implementation-Description
//  This method intiates the system response to a paritcular Background Event.
//  Based on the severity of the Background Event, this method will perform
//  anything from taking the ventilator to inoperative state to doing nothing.
//  
//  $[05048] $[05049] $[05050] $[05051] $[05052] $[05053] $[05054] $[05055]
//  $[05056] $[05057] $[05060] $[05062] $[05063] $[05066] $[05068] $[05071]
//  $[05073] $[05074] $[05075]
//----------------------------------------------------------------------------
//@ PreCondition
//  alarmType must be valid and not BTAT_5_EN 
//----------------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=============================================================================

void
BackgroundFaultHandler::SystemResponseToBtat_
(
    const OperatingParameterEventName alarmType
)
{
    switch ( alarmType )
    {
        case BTAT_1_EN:
        // $[TI1]

#if defined( SIGMA_PRODUCTION )
            NovRamManager::UpdateBackgroundTestFailureFlag( TRUE );
#endif // defined( SIGMA_PRODUCTION )
            DeclareVentInop_();
            break;
 
#if defined( SIGMA_BD_CPU )
        case BTAT_2_EN:
        // $[TI2]
            VentStatus::DirectVentStatusService( VentStatus::BD_AUDIO_ALARM,
                                                 VentStatus::ACTIVATE );
            break;

        case BTAT_4_EN:
        // $[TI3]
            VentStatus::DirectVentStatusService( VentStatus::BD_AUDIO_ALARM,
                                                 VentStatus::ACTIVATE );
            VentStatus::DirectVentStatusService( VentStatus::LOSS_OF_GUI_LED,
                                                 VentStatus::ACTIVATE );
            break;

        case NOT_BTAT_4_EN:
        // $[TI4]
            VentStatus::DirectVentStatusService( VentStatus::BD_AUDIO_ALARM,
                                                 VentStatus::DEACTIVATE );
            VentStatus::DirectVentStatusService( VentStatus::LOSS_OF_GUI_LED,
                                                 VentStatus::DEACTIVATE );
            break;
 
        case BTAT_3_EN:
        case BTAT_8_EN:
        case NOT_BTAT_8_EN:
        case BTAT_9_EN:
        case BTAT_10_EN:
        case BTAT_11_EN:
        case BTAT_12_EN:
        case BTAT_13_EN:
        case BTAT_14_EN:
        case BTAT_15_EN:
        // $[TI5]
            break;
 
#elif defined( SIGMA_GUI_CPU )
 
        case BTAT_6_EN:
        case NOT_BTAT_6_EN:
        case BTAT_7_EN:
        case NOT_BTAT_7_EN:
        case BTAT_15_EN:
        // $[TI6]
            break;
 
        case BTAT_16_EN:
        // $[TI7]
            TaskControlAgent::ChangeState( STATE_INOP );
            BdGuiEvent::RequestUserEvent( EventData::GUI_FAULT, EventData::START );
            break;
 
        case BTAT_17_EN:
        // $[TI8]
            BdGuiEvent::RequestUserEvent( EventData::GUI_FAULT, EventData::START );
            break;
 
#endif // defined( SIGMA_GUI_CPU )
 
        case BTAT_5_EN: // this alarm type should not be report externally !
        default:
            CLASS_ASSERTION( FALSE );
            break;
    }
}

//============================ M E T H O D   D E S C R I P T I O N ============
//@ Method: ProcessReversibleBtat_ 
//
//@ Interface-Description
//  This method accepts an OperatingParameterEventName enumeration and a
//  reference to a BtatEventState object and returns no value.
//
//  This method checks to see if the current background event is a reversible.
//  If it is reversible, its compliment alarm log entry is retrieved and
//  replaced with the current background event.
//-----------------------------------------------------------------------------
//@ Implementation-Description
//  With the current background event, it determines whether it is reversible.
//  If it is reversible, it compliment alarm type is retrieved and updated with
//  the current background event.
//-----------------------------------------------------------------------------
//@ PreCondition
//  None 
//-----------------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=============================================================================
 
void
BackgroundFaultHandler::ProcessReversibleBtat_
(
    const OperatingParameterEventName alarmType,
    BtatEventState& rBtatEventStat
)
{
    Int16 reversibleBtatIndex;

    ////////////////////////////////////////////////////////////////////
    // Check the current BackgroundFaultHandler event to see if
    // it is a revisble event.
    // Note: If the converse alarm does not exist nothing occurs.
    ////////////////////////////////////////////////////////////////////
    switch( alarmType )
    {

#if defined( SIGMA_BD_CPU )

        case BTAT_4_EN:
        // $[TI1]
            reversibleBtatIndex = rBtatEventStat.findEntry( NOT_BTAT_4_EN );
            break;
 
        case NOT_BTAT_4_EN:
        // $[TI2]
            reversibleBtatIndex = rBtatEventStat.findEntry( BTAT_4_EN );
            break;
 
        case BTAT_8_EN:
        // $[TI3]
            reversibleBtatIndex = rBtatEventStat.findEntry( NOT_BTAT_8_EN );
            break;
 
        case NOT_BTAT_8_EN:
        // $[TI4]
            reversibleBtatIndex = rBtatEventStat.findEntry( BTAT_8_EN );
            break;
 
#elif defined( SIGMA_GUI_CPU )

        case BTAT_6_EN:
        // $[TI5]
            reversibleBtatIndex = rBtatEventStat.findEntry( NOT_BTAT_6_EN );
            break;
 
        case NOT_BTAT_6_EN:
        // $[TI6]
            reversibleBtatIndex = rBtatEventStat.findEntry( BTAT_6_EN );
            break;
 
        case BTAT_7_EN:
        // $[TI7]
            reversibleBtatIndex = rBtatEventStat.findEntry( NOT_BTAT_7_EN );
            break;
 
        case NOT_BTAT_7_EN:
        // $[TI8]
            reversibleBtatIndex = rBtatEventStat.findEntry( BTAT_7_EN );
            break;
 
#endif // defined( SIGMA_GUI_CPU )

        default:
        // $[TI9]
            reversibleBtatIndex = NOT_FOUND;
            break;
 
    }  //  case switch
 
    ////////////////////////////////////////////////////////////////////
    // If an entry was found from the above search,  purge it
    // from the active alarms.
    ////////////////////////////////////////////////////////////////////
    if ( reversibleBtatIndex != NOT_FOUND )
    {
    // $[TI10]
        rBtatEventStat.setBtatState( (Uint8)reversibleBtatIndex, EVENT_NAME_NULL );
    }
    // $[TI11]
}
      
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  DeclareVentInop_
//
//@ Interface-Description
//  This method accepts no parameters and returns no value.
//
//  This method brings the system to a VENT INOP state.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Tasking is controlled by changing the system state to STATE_INOP,
//  this will cause the Sys-Init task to broadcast to each affected task
//  to transition into an INOP condition.
//
//  On the BD CPU the VENT-INOP condition will put the vent head into
//  a safe state, all pnuematics are neutralized, safety valve is opened,
//  audio alarm is activated, and the safety valve open and VENT INOP
//  LEDs are lit.
//
//  After an INOP condition is declared, the software applications
//  continues to run and communication to the GUI is maintained.
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

void
BackgroundFaultHandler::DeclareVentInop_( void )
{
 
#if defined( SIGMA_BD_CPU )

   	// $[TI1.1]
   	// This will cause all other tasks to transition to their INOP state
	TaskControlAgent::ChangeState( STATE_INOP );
      
#if defined(SIGMA_PRODUCTION) && !defined(SIGMA_UNIT_TEST)

   	VentStatus::DirectVentStatusService( VentStatus::VENT_INOP_STATE,
                                       	VentStatus::ACTIVATE );
#endif // defined(SIGMA_PRODUCTION) && !defined(SIGMA_UNIT_TEST)

#elif defined( SIGMA_GUI_CPU ) && defined( SIGMA_PRODUCTION )

	// $[TI1.2]
   	SetVentInop();

#endif // defined( SIGMA_BD_CPU )
                                                                        
}


#if defined( SIGMA_SAFETY_NET )
const char*
BackgroundFaultHandler::GetBkEventName( const enum BkEventName eventName )
{
    return ( BK_EVENT_DEFINITON_TABLE_[ eventName ].pDebugText );
}
#endif // defined( SIGMA_SAFETY_NET )


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//  and 'lineNumber' are essential pieces of information.  The
//  'pFileName' and 'pPredicate' strings may be defaulted in the macro
//  to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//  None 
//---------------------------------------------------------------------
//@ PostCondition 
//  None 
//@ End-Method 
//===================================================================== 

void
BackgroundFaultHandler::SoftFault( const SoftFaultID  softFaultID,
                                   const Uint32       lineNumber,
                                   const char*        pFileName,
                                   const char*        pPredicate )
{
    CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

    FaultHandler::SoftFault( softFaultID, SAFETY_NET, BACKGROUNDFAULTHANDLER,
                             lineNumber, pFileName, pPredicate );
}
