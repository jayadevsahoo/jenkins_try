#ifndef KeyPanel_HH
#define KeyPanel_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: KeyPanel - Routes events from off-screen keys to application objects.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Foundations/vcssrc/KeyPanel.hhv   25.0.4.0   19 Nov 2013 14:11:42   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006   By: sah    Date: 11-Jan-1999   DR Number: 5321
//  Project:  Sigma (R8027)
//  Description:
//      Changed 'SoftFault()' method to non-inline method.
//
//  Revision: 005  By: clw      Date: 20-Mar-1995  DCR Number: 1950
//    Project:  Sigma (R8027)
//    Description:
//      Added new method EncodeGuifKey_() to class def. Also added
//      include line for SaasSound.hh.
//
//  Revision: 004  By: gdc      Date: 15-Feb-1996  DR Number: 674
//    Project:  Sigma (R8027)
//    Description:
//	Removed ifndef / endif around include directives
//
//  Revision: 003  By: yyy      Date: 17-Jul-1995   DCR Number: 1951
//    Project:  Sigma (R8027)
//    Description:
//      Replaced RIGHT_SPARE_KEY with INSP_PAUSE_KEY.
//
//  Revision: 002  By: yyy      Date: 06-Jul-1995   DCR Number: 1952
//    Project:  Sigma (R8027)
//    Description:
//      Modified due to the addition of KeyEnabled_[] static variable, which controls
//      the enabling/disabling of a specific key functions.
//
//  Revision: 001  By: yyy      Date: 01-Jan-1995  DR Number: NONE
//    Project:  Sigma (R8027)
//    Description:
//	Integration baseline.
//====================================================================

#include "Sigma.hh"
#include "GuiFoundation.hh"
#include "Keys.hh"
#include "Sound.hh"  

//@ Usage-Classes
class KeyPanelTarget;
//@ End-Usage

class KeyPanel 
{
public:

	//@ Type: KeyId
	// This enum just lists all of the offscreen keys that may be accessed
	// through this class. Arrangement of the enums does not have any impact on the mapping.
	// All the mapping with the hardware is performed in KeyPanel.cc/KeyMapPass[]
	// Note: The last member of the enum is NUMBER_OF_KEYS.

	enum KeyId
	{
		ACCEPT_KEY,
		CLEAR_KEY,
		INSP_PAUSE_KEY,
		EXP_PAUSE_KEY,
		MAN_INSP_KEY,
		HUNDRED_PERCENT_O2_KEY,
		INFO_KEY,
		ALARM_RESET_KEY,
		ALARM_SILENCE_KEY,
		ALARM_VOLUME_KEY,
		SCREEN_BRIGHTNESS_KEY,
		SCREEN_CONTRAST_KEY,
		SCREEN_LOCK_KEY,
		LEFT_SPARE_KEY,
		HOME_KEY,

		NUMBER_OF_KEYS
	};

	static void Initialize(void);
	static void SetCallback(KeyId keyCode, KeyPanelTarget *pTarget);
	static void SetLight(KeyId keyCode, Boolean isIlluminated);

	static void KeyPressHappened(GuiKeys guiIoKeyCode);
	static void KeyReleaseHappened(GuiKeys guiIoKeyCode);

	inline static void EnableAllKeys(void);
	inline static void EnableKey(KeyId keyCode);
	inline static void DisableAllKeys(void);
	inline static void DisableKey(KeyId keyCode);
	
	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:

private:
	// these methods are purposely declared, but not implemented...
	KeyPanel(void);						// not implemented...
	KeyPanel(const KeyPanel&);			// not implemented...
	~KeyPanel(void);					// not implemented...
	void operator=(const KeyPanel&);	// not implemented...
	
	static KeyId DecodeGuiIoKey_(GuiKeys guiIoKeyCode);
	static GuiKeys EncodeGuifKey_(KeyId guifKeyCode);


	static KeyPanelTarget *KeyTargets_[NUMBER_OF_KEYS];
	static Boolean KeyEnabled_[NUMBER_OF_KEYS];
};

// Inlined methods
#include "KeyPanel.in"

#endif // KeyPanel_HH 
