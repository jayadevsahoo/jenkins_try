#ifndef MutEx_HH
#define MutEx_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: MutEx - Mutual exclusion mechanism to protect shared data
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/OS-Foundation/vcssrc/MutEx.hhv   25.0.4.0   19 Nov 2013 14:15:16   pvcs  $
//
//@ Modification-Log
// 
//  Revision: 003   By: sah   Date: 13-Jan-1999  DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 002    By: Gary Cederquist  Date: 15-APR-1997  DR Number: 1943
//    Project: Sigma (R8027)
//    Description:
//       Added revision headers.
//
//  Revision: 001    By: Greg Hamm        Date: 01-OCT-1996  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//====================================================================

#include "Sigma.hh"
#include "IpcIds.hh"
#include "OsFoundation.hh"

#ifndef HANDLE
typedef void* HANDLE;
#endif

//@ Usage-Classes
//@ End-Usage

class MutEx
{
  public:

    //@ Constant: MAX_MUTEX_ID
    // Maximum value for a MUTEX ID, constrains size of MutEx lookup
    // table.
    enum {MAX_MUTEX_ID = ::MAX_QID};

    //@ Constant: MUTEX_LENGTH
    // Length of MUTEX. 
    enum {MUTEX_LENGTH = 1};

    MutEx(const Int32 id);
    ~MutEx(void);

    Int32 request(const Uint32 timeout=Ipc::NO_TIMEOUT);
    void  release(void);

    static void Initialize(void);

    static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL, 
			  const char*       pPredicate = NULL);
  protected:

  private:
    // these methods are purposely declared, but not implemented...
    MutEx(void);		        // not implemented...
    MutEx(const MutEx&);		// not implemented...
    void   operator=(const MutEx&);	// not implemented...

    //@ Data-Member: error_
    // Contains the last return code from VRTX32 for this MutEx.
    Int32 error_;
 
    //@ Data-Member: ID_
    // MUTEX identifier specified to the constructor.  Corresponds
    // to the ids defined in IpcIds.hh
    const Int32 ID_;
 
    //@ Data-Member: handle_
    // Mutex handle returned by the OS when created.
    HANDLE handle_;

};


#endif // MutEx_HH 
