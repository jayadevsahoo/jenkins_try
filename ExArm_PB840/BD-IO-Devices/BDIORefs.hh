#ifndef BDIORefs_HH
#define BDIORefs_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Header: BDIORefs - All the external references of miscellaneous references.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/BDIORefs.hhv   25.0.4.0   19 Nov 2013 13:53:58   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By:  syw    Date:  28-Jul-1999    DR Number: 5416
//       Project:  840
//       Description:
//			Added REventFilter object declaration
//
//  Revision: 002  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 001  By:  syw    Date: 08-Nov-1996    DR Number: INITIAL RELEASE
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

class Configuration ;
extern Configuration& RSystemConfiguration;

class SafetyNetSensorData ;
extern SafetyNetSensorData& RLowVoltageRefSNData ;

class BinaryCommand;
extern BinaryCommand& RExhHeater;

class EventFilter ;
extern EventFilter& REventFilter ;

class AioDioDriver_t;
extern AioDioDriver_t		AioDioDriver;

class BDIOTest_t;
extern BDIOTest_t			BdioTest;

class SpiFlash_t;
extern SpiFlash_t			SpiFlash;

#endif // BDIORefs_HH
