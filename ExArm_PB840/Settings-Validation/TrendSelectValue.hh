#ifndef TrendSelectValue_HH
#define TrendSelectValue_HH

//====================================================================
//
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//====================================================================

//====================================================================
//@ Class:  TrendSelectValue - Values for the TrendSelect
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/TrendSelectValue.hhv   25.0.4.0   19 Nov 2013 14:27:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002   By: rpr    Date: 07-Nov-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support Leak Compensation and Work of Breathing
// 
//  Revision: 001   By: rhj    Date:  05-Jun-2007    SCR Number: 6237
//  Project: Trend
//  Description:
//      Initial version
//====================================================================

//@ Usage-Classes
//@ End-Usage


struct TrendSelectValue
{
	//@ Type:  TrendSelectValue
	// All of the possible values of the TrendSelect.
	// $[TR01112] -- The Trend parameters shall appear in the predefined order... 
	enum TrendSelectValueId
	{
		TREND_EMPTY,
		TREND_DYNAMIC_COMPLIANCE,
		TREND_PAV_COMPLIANCE,
		TREND_STATIC_LUNG_COMPLIANCE,
		TREND_PAV_ELASTANCE,
		TREND_END_EXPIRATORY_FLOW,
		TREND_PEAK_EXPIRATORY_FLOW_RATE,
		TREND_PEAK_SPONTANEOUS_FLOW_RATE,
		TREND_TOTAL_RESPIRATORY_RATE,
		TREND_I_E_RATIO,
		TREND_NEGATIVE_INSPIRATORY_FORCE,
		TREND_OXYGEN_PERCENTAGE,
		TREND_P100,
		TREND_END_EXPIRATORY_PRESSURE,
		TREND_INTRINSIC_PEEP,
		TREND_PAV_INTRINSIC_PEEP,
		TREND_TOTAL_PEEP,
		TREND_MEAN_CIRCUIT_PRESSURE,
		TREND_PEAK_CIRCUIT_PRESSURE,
		TREND_PLATEAU_PRESSURE,
		TREND_SPONTANEOUS_RAPID_SHALLOW_BREATHING_INDEX,
		TREND_DYNAMIC_RESISTANCE,
		TREND_PAV_RESISTANCE,
		TREND_STATIC_AIRWAY_RESISTANCE,
		TREND_PAV_TOTAL_AIRWAY_RESISTANCE,
		TREND_SPONTANEOUS_INSPIRATORY_TIME,
		TREND_SPONTANEOUS_INSPIRATORY_TIME_RATIO,
		TREND_VITAL_CAPACITY,
		TREND_EXHALED_MINUTE_VOLUME,
		TREND_EXHALED_SPONTANEOUS_MINUTE_VOLUME,
		TREND_EXHALED_TIDAL_VOLUME,
		TREND_EXHALED_SPONTANEOUS_TIDAL_VOLUME,
		TREND_EXHALED_MANDATORY_TIDAL_VOLUME,
		TREND_INSPIRED_TIDAL_VOLUME,
		TREND_TOTAL_WORK_OF_BREATHING,
		TREND_LEAK,
		TREND_PERCENT_LEAK,
		TREND_ILEAK,

		TREND_SET_EXPIRATORY_SENSITIVITY,
		TREND_SET_RESPIRATORY_RATE,
		TREND_SET_PEAK_INSPIRATORY_FLOW,
		TREND_SET_I_E_RATIO,
		TREND_SET_OXYGEN_PERCENTAGE,
		TREND_SET_PERCENT_SUPPORT,
		TREND_SET_PEEP,
		TREND_SET_PEEP_HIGH,
		TREND_SET_PEEP_LOW,
		TREND_SET_INSPIRATORY_PRESSURE,
		TREND_SET_PRESSURE_SUPPORT_LEVEL,
		TREND_SET_FLOW_ACCELERATION_OVER_RISE_TIME,
		TREND_SET_FLOW_SENSITIVITY,
		TREND_SET_PRESSURE_SENSITIVITY,
		TREND_SET_PEEP_HIGH_TIME,
		TREND_SET_TH_TL_RATIO,
		TREND_SET_INSPIRATORY_TIME,
		TREND_SET_PEEP_LOW_TIME,
		TREND_SET_TIDAL_VOLUME,
		TREND_SET_VOLUME_SUPPORT_LEVEL_VS,
		TOTAL_TREND_SELECT_VALUES,
		TREND_DATA_NOT_AVAILABLE
	};
};


#endif // TrendSelectValue_HH 
