#ifndef	GUI_Serial_Interface_HH
#define	GUI_Serial_Interface_HH

//====================================================================
// This is a proprietary work to which Nellcor Puritan Bennett
// corporation of California claims exclusive right.  No part of this
// work may be used, disclosed, reproduced, sorted in an information
// retrieval system, or transmitted by any means, electronic,
// mechanical, photocopying, recording, or otherwise without the prior
// written permission of Nellcor Puritan Bennett Corporation.
//
//     Copyright (c) 1997, Nellcor Puritan Bennett Corporation
//=====================================================================

//=====================================================================
//@ Class:  GUI_Serial_Interface - Subsystem definitions
//---------------------------------------------------------------------
//@ Version-Information
// @ $Header:   /840/Baseline/GUI-Serial-Interface/vcssrc/GUI_Serial_Interface.hhv   25.0.4.0   19 Nov 2013 14:12:44   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004   By: gdc    Date: 16-Aug-2010     SCR Number: 6663
//  Project: API/MAT
//  Description:
//  Added support for VentSet API.
// 
//  Revision: 003    By: erm       Date: 24-Oct-2008   SCR Number: 6441
//  Project: GuiComms
//  Description:
//  Added support for RT Waveform to the serial port
//  
//  Revision: 002   By:  gdc       Date: 07-APR-2008   SCR Number: 6407
//  Project:  TREND2
//  Description:
//  Removed redefinition of countof macro to resolve compiler warning.
// 
//  Revision: 001    By: gdc       Date: 07-Feb-2001   DCS Number: 5493
//  Project: GuiComms
//  Description:
//    Initial version.
//=====================================================================

//@ Usage-Classes
//@ End-Usage

#include "Sigma.hh"

class GUI_Serial_Interface
{
  public:

	//@ Type: ClassIds
	// class idenfitication for all SerialInterface-related calls to
	// FaultHandler::SoftFault()
	enum ClassIds 
	{
		  SERIAL_INTERFACE_CLASS         = 1
		, SERIAL_PORT_CLASS              = 2
		, SMCP_INTERFACE_CLASS           = 3
		, DCI_INTERFACE_CLASS            = 4
		, SERIAL_BUFFER_CLASS            = 5
		, SMCP_REQUEST_BUFFER_CLASS      = 6
		, SMCP_RESPONSE_BUFFER_CLASS     = 7
		, PRINTER_INTERFACE_CLASS        = 8
		, SCREEN_CAPTURE_CLASS           = 9
		, SETTINGS_CAPTURE_CLASS         = 10
		, MODE_SETTINGS_CAPTURE_CLASS    = 11
		, WAVEFORMS_CAPTURE_CLASS        = 12
		, PATIENT_DATA_CAPTURE_CLASS     = 13
		, ALARM_STATUS_CAPTURE_CLASS     = 14
		, LOWER_SUB_SCREEN_CAPTURE_CLASS = 15
        , SENSOR_INTERFACE_CLASS         = 16
        , SENSOR_DATA_BUFFER_CLASS       = 17
        , VSET_SERVER_CLASS              = 18
        , VSET_TEXT_CLASS                = 19
	};
};

#endif // GUI_Serial_Interface_HH
