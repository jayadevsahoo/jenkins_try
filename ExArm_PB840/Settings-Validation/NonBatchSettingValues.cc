#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: NonBatchSettingValues - Manager of the Non-Batch Setting Values.
//---------------------------------------------------------------------
//@ Interface-Description
//  This class manages the values of Non-Batch Settings.  Instances of
//  this class can be used to store all of the non-batch setting values.
//  This class provides methods to query and set any of the non-batch
//  setting values.  It also has an assignment operator to do copies between
//  instances of this class -- this method copies ALL values and does NOT
//  generate any callbacks.
//
//  This class inherits from 'SettingValueMgr' which provides the callback
//  functionality.
//---------------------------------------------------------------------
//@ Rationale
//  This class provides a general-purpose class for managing, as a set,
//  non-batch setting values.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Internally, instances of this class contain arrays of type
//  'NonBatchValue'.  'NonBatchValue' is a union of the three different types
//  of non-batch values:  bounded, discrete and sequential values.  This
//  array is big enough to store all of the non-batch setting values, where
//  the setting IDs are used as indices into this array.  However, since the
//  ids of the non-batch settings do not start at a value of zero, the ids
//  have to be "converted" to array indices, before using them to index into
//  the array.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/NonBatchSettingValues.ccv   25.0.4.0   19 Nov 2013 14:27:32   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005 By: srp    Date: 28-May-2002   DR Number: 5898
//  Project:  VCP
//	Description:
//		Incorporated Remediation code review issue(s) -- commentary changes only.
//
//  Revision: 004   By: sah  Date:  2-Feb-2000    DR Number:  5327
//  Project:  NeoMode
//  Description:
//  Updated for NeoMode
//
//  Revision: 003   By: sah  Date:  11-Jan-1999    DR Number:  5321
//  Project:  ATC
//  Description:
//	As part of making all 'SoftFault()' methods non-inlined, I'm
//	also making those 'SoftFault()' methods that are NOT called
//	in PRODUCTION mode, DEVELOPMENT-only.
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//=====================================================================

#include "NonBatchSettingValues.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: NonBatchSettingValues(nonBatchValues)  [Copy Constructor]
//
//@ Interface-Description
//  Construct a non-batch setting value's instance, using 'nonBatchValues'
//  for initialization.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

NonBatchSettingValues::NonBatchSettingValues(
				const NonBatchSettingValues& nonBatchValues
					    )
				: SettingValueMgr(NULL)
{
  CALL_TRACE("NonBatchSettingValues(nonBatchValues)");

  // copy 'nonBatchValues' into this new instance (no callbacks are called)...
  operator=(nonBatchValues);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: NonBatchSettingValues(pInitCallBack)  [Constructor]
//
//@ Interface-Description
//  Construct a non-batch setting value's instance, using 'pInitCallback'
//  as the pointer to the value-change callback function.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

NonBatchSettingValues::NonBatchSettingValues(
		       const SettingValueMgr::ChangeCallbackPtr pInitCallback
					    )
		       : SettingValueMgr(pInitCallback)
{
  CALL_TRACE("NonBatchSettingValues(pInitCallback)");
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: NonBatchSettingValues()  [Default Constructor]
//
//@ Interface-Description
//  Construct a default non-batch setting value's instance.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

NonBatchSettingValues::NonBatchSettingValues(void)
					     : SettingValueMgr(NULL)
{
  CALL_TRACE("NonBatchSettingValues()");
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~NonBatchSettingValues(void)  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this non-batch setting values instance.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

NonBatchSettingValues::~NonBatchSettingValues(void)
{
  CALL_TRACE("~NonBatchSettingValues()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getSettingValue(settingId)  [const]
//
//@ Interface-Description
//  Return the setting value, that is given by 'settingId'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (SettingId::IsNonBatchId(settingId))
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

const SettingValue&
NonBatchSettingValues::getSettingValue(
				const SettingId::SettingIdType settingId
				      ) const
{
  CALL_TRACE("getSettingValue(settingId)");
  SAFE_CLASS_PRE_CONDITION((SettingId::IsNonBatchId(settingId)));

  // since the non-batch IDs don't start at '0', the IDs must be "converted"
  // to array indices (i.e., "shifted" towards '0')...
  Uint  idx = SettingId::ConvertNonBatchIdToIndex(settingId);
  SAFE_CLASS_POST_CONDITION((idx < SettingId::NUM_NON_BATCH_IDS), SettingId);

  return(arrNonBatchValues_[idx]);   // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  setSettingValue(settingId, newValue)
//
//@ Interface-Description
//  Set the value of the setting represented by 'settingId', to 'newValue'.
//  This activates this instance's callback, if any.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (SettingId::IsNonBatchId(settingId))
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
NonBatchSettingValues::setSettingValue(
				    const SettingId::SettingIdType settingId,
				    const SettingValue&            newValue
				      )
{
  CALL_TRACE("setSettingValue(settingId, newValue)");
  SAFE_CLASS_PRE_CONDITION((SettingId::IsNonBatchId(settingId)));

  // since the non-batch IDs don't start at '0', the IDs must be "converted"
  // to array indices (i.e., "shifted" towards '0')...
  Uint  idx = SettingId::ConvertNonBatchIdToIndex(settingId);
  SAFE_CLASS_POST_CONDITION((idx < SettingId::NUM_NON_BATCH_IDS), SettingId);

  // store new value...
  arrNonBatchValues_[idx] = newValue;

  // activate this instance's callback, if any...
  activateCallback_(settingId);
} // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getBoundedValue(boundedId)  [const]
//
//@ Interface-Description
//  Return the bounded setting value and precision, that is given by
//  'boundedId'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (SettingId::IsNonBatchBoundedId(boundedId))
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

BoundedValue
NonBatchSettingValues::getBoundedValue(
				const SettingId::SettingIdType boundedId
				      ) const
{
  CALL_TRACE("getBoundedValue(boundedId)");
  SAFE_CLASS_PRE_CONDITION((SettingId::IsNonBatchBoundedId(boundedId)));

  // since the non-batch IDs don't start at '0', the IDs must be "converted"
  // to array indices (i.e., "shifted" towards '0')...
  Uint  idx = SettingId::ConvertNonBatchIdToIndex(boundedId);
  SAFE_CLASS_POST_CONDITION((idx < SettingId::NUM_NON_BATCH_IDS), SettingId);

  return(arrNonBatchValues_[idx]);   // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  setBoundedValue(boundedId, newValue)
//
//@ Interface-Description
//  Set the value of the bounded setting represented by 'boundedId',
//  to 'newValue'.  This activates this instance's callback, if any.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (SettingId::IsNonBatchBoundedId(boundedId))
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
NonBatchSettingValues::setBoundedValue(
				    const SettingId::SettingIdType boundedId,
				    const BoundedValue&            newValue
				      )
{
  CALL_TRACE("setBoundedValue(boundedId, newValue)");
  SAFE_CLASS_PRE_CONDITION((SettingId::IsNonBatchBoundedId(boundedId)));

  // since the non-batch IDs don't start at '0', the IDs must be "converted"
  // to array indices (i.e., "shifted" towards '0')...
  Uint  idx = SettingId::ConvertNonBatchIdToIndex(boundedId);
  SAFE_CLASS_POST_CONDITION((idx < SettingId::NUM_NON_BATCH_IDS), SettingId);

  // store new value...
  arrNonBatchValues_[idx] = newValue;

  // activate this instance's callback, if any...
  activateCallback_(boundedId);
} // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getDiscreteValue(discreteId)  [const]
//
//@ Interface-Description
//  Return the discrete setting value that is given by 'discreteId'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (SettingId::IsNonBatchDiscreteId(discreteId))
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

DiscreteValue
NonBatchSettingValues::getDiscreteValue(
				const SettingId::SettingIdType discreteId
				     ) const
{
  CALL_TRACE("getDiscreteValue(discreteId)");
  SAFE_CLASS_PRE_CONDITION((SettingId::IsNonBatchDiscreteId(discreteId)));

  // since the non-batch IDs don't start at '0', the IDs must be "converted"
  // to array indices (i.e., "shifted" towards '0')...
  Uint  idx = SettingId::ConvertNonBatchIdToIndex(discreteId);
  SAFE_CLASS_POST_CONDITION((idx < SettingId::NUM_NON_BATCH_IDS), SettingId);

  return(arrNonBatchValues_[idx]);  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  setDiscreteValue(discreteId, newValue)
//
//@ Interface-Description
//  Set the value of the discrete setting represented by 'discreteId',
//  to 'newValue'.  This activates this instance's callback, if any.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (SettingId::IsNonBatchDiscreteId(discreteId))
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
NonBatchSettingValues::setDiscreteValue(
				  const SettingId::SettingIdType discreteId,
				  const DiscreteValue            newValue
				       )
{
  CALL_TRACE("setDiscreteValue(discreteId, newValue)");
  SAFE_CLASS_PRE_CONDITION((SettingId::IsNonBatchDiscreteId(discreteId)));

  // since the non-batch IDs don't start at '0', the IDs must be "converted"
  // to array indices (i.e., "shifted" towards '0')...
  Uint  idx = SettingId::ConvertNonBatchIdToIndex(discreteId);
  SAFE_CLASS_POST_CONDITION((idx < SettingId::NUM_NON_BATCH_IDS), SettingId);

  // store new value...
  arrNonBatchValues_[idx] = newValue;

  // activate this instance's callback, if any...
  activateCallback_(discreteId);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getSequentialValue(sequentialId)  [const]
//
//@ Interface-Description
//  Return the sequential setting value that is given by 'sequentialId'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (SettingId::IsNonBatchSequentialId(sequentialId))
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SequentialValue
NonBatchSettingValues::getSequentialValue(
				const SettingId::SettingIdType sequentialId
					 ) const
{
  CALL_TRACE("getSequentialValue(sequentialId)");
  SAFE_CLASS_PRE_CONDITION((SettingId::IsNonBatchSequentialId(sequentialId)));

  // since the non-batch IDs don't start at '0', the IDs must be "converted"
  // to array indices (i.e., "shifted" towards '0')...
  Uint  idx = SettingId::ConvertNonBatchIdToIndex(sequentialId);
  SAFE_CLASS_POST_CONDITION((idx < SettingId::NUM_NON_BATCH_IDS), SettingId);

  return(arrNonBatchValues_[idx]);  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  setSequentialValue(sequentialId, newValue)
//
//@ Interface-Description
//  Set the value of the sequential setting represented by 'sequentialId',
//  to 'newValue'.  This activates this instance's callback, if any.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (SettingId::IsNonBatchSequentialId(sequentialId))
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
NonBatchSettingValues::setSequentialValue(
				  const SettingId::SettingIdType sequentialId,
				  const SequentialValue          newValue
				       )
{
  CALL_TRACE("setSequentialValue(sequentialId, newValue)");
  SAFE_CLASS_PRE_CONDITION((SettingId::IsNonBatchSequentialId(sequentialId)));

  // since the non-batch IDs don't start at '0', the IDs must be "converted"
  // to array indices (i.e., "shifted" towards '0')...
  Uint  idx = SettingId::ConvertNonBatchIdToIndex(sequentialId);
  SAFE_CLASS_POST_CONDITION((idx < SettingId::NUM_NON_BATCH_IDS), SettingId);

  // store new value...
  arrNonBatchValues_[idx] = newValue;

  // activate this instance's callback, if any...
  activateCallback_(sequentialId);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  operator=(nonBatchValues)  [Assignment Operator]
//
//@ Interface-Description
//  Copy all of the non-batch setting values from 'nonBatchValues' into
//  this instance.  No callbacks are generated.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
NonBatchSettingValues::operator=(const NonBatchSettingValues& nonBatchValues)
{
  CALL_TRACE("operator=(nonBatchValues)");

  if (this != &nonBatchValues)
  {   // $[TI1]
    // convert the "low" and "high" non-batch setting ids to indices...
    const Uint  LOW_NON_BATCH_INDEX =
		   SettingId::ConvertNonBatchIdToIndex(
				    SettingId::LOW_NON_BATCH_ID_VALUE
						      );
    const Uint  HIGH_NON_BATCH_INDEX =
		   SettingId::ConvertNonBatchIdToIndex(
				    SettingId::HIGH_NON_BATCH_ID_VALUE
						      );
    SAFE_CLASS_POST_CONDITION((HIGH_NON_BATCH_INDEX <
                               SettingId::NUM_NON_BATCH_IDS), SettingId);
    SAFE_CLASS_ASSERTION((LOW_NON_BATCH_INDEX <= HIGH_NON_BATCH_INDEX));

    // for each non-batch setting...
    for (Uint idx = LOW_NON_BATCH_INDEX; idx <= HIGH_NON_BATCH_INDEX; idx++)
    {
      // copy this bounded value...
      arrNonBatchValues_[idx] = nonBatchValues.arrNonBatchValues_[idx];
    }  // end of for...
  }   // $[TI2] -- 'nonBatchValues' IS this instance...
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//	    [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
NonBatchSettingValues::SoftFault(const SoftFaultID  softFaultID,
				 const Uint32       lineNumber,
				 const char*        pFileName,
				 const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
			  NON_BATCH_SETTING_VALUES, lineNumber, pFileName,
			  pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)
