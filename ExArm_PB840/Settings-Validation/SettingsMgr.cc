#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: SettingsMgr - Manager of the Setting Instances.
//---------------------------------------------------------------------
//@ Interface-Description
//  This class is a static class that provides access, via pointers, to
//  each, and every, setting instance.  The responsibilities of this class
//  include initializing ALL of the setting instances, and providing access
//  to each of those instances.  There are five access methods, four that
//  return a pointer to an individual setting -- either as a 'Setting'
//  pointer, or as a pointer to either a 'BoundedSetting', 'DiscreteSetting',
//  or 'SequentialSetting'.  Another access method returns a pointer to the
//  entire array of setting pointers, for easy iteration through a set
//  of settings.
//---------------------------------------------------------------------
//@ Rationale
//  This class provides a central repository for all of the setting
//  instances.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class contains a static array of setting pointers ('SettingPtr'),
//  that is 'SettingId::TOTAL_SETTING_IDS' big.  As settings are added or
//  deleted, the initialization method must be altered to reflect that
//  change.  All of the other methods are resistant to additions/deletions
//  of settings.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/SettingsMgr.ccv   25.0.4.0   19 Nov 2013 14:27:44   pvcs  $
//
//@ Modification-Log
//   
//  Revision: 016   By: rhj   Date:  26-Jan-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//       Added Prox enabled setting
//       
//  Revision: 015   By: rhj   Date: 07-July-2008    SCR Number: 6435
//  Project:  LEAK
//  Description:
//       Leak Compensation project-related changes.
//       
//  Revision: 014   By: rhj   Date:  23-May-2007    SCR Number: 6237 
//  Project:  Trend
//  Description:
//      Trend related changes.
//   
//  Revision: 013   By: gdc   Date:  27-May-2005    SCR Number: 6170 
//  Project:  NIV2
//  Description:
//      Added High Ti spont limit setting for NIV SPONT/SIMV.
//
//  Revision: 012   By: gdc   Date:  18-Feb-2005    SCR Number: 6144 
//  Project:  NIV1
//  Description:
//      Changes to support NIV.
//
//  Revision: 011   By: quf    Date: 17-Sep-2001    DR Number: 5493
//  Project:  GUIComms
//  Description:
//	Removed code relating to obsoleted print config setting.
//
//  Revision: 010   By: sah   Date:  18-Dec-2000    DR Number: 5805
//  Project:  VCP
//  Description:
//	Added support for new shadow trace enable/disable setting.
//
//  Revision: 009  By: sah     Date:  05-May-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      *  deleted apnea minute volume
//      *  added volume support level, Vt-to-IBW rato and Vsupp-to-
//         IBW ratio
//
//  Revision: 008   By: hct    Date: 03-DEC-1999    DR Number: 5493
//  Project:  GUIComms
//  Description:
//	Incorporated initial specifications for GUIComms Project.
//
//  Revision: 007   By: sah    Date: 20-Jul-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//      NeoMode project-related changes:
//      *  obsoleted patient type setting
//      *  added unit-test-only location of static data block into
//         stacks region
//
//  Revision: 006   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new BiLevel-specific settings
//	*  added call to new 'settingObserverInit' method for initializing
//	   those settings that are observers as well
//
//  Revision: 005   By: sah   Date:  04-Mar-1999    DR Number: 5322
//  Project:  ATC
//  Description:
//	Project-related changes:
//	*  added new ATC-specific settings
//
//  Revision: 004   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 003   By: dosman    Date:  06-Jan-1998    DR Number: 
//  Project:  BILEVEL
//  Description:
//	Initial BiLevel implementation, add PEEP_HI.
//
//  Revision: 002   By: sah    Date:  22-Sep-1997    DR Number: 2410
//  Project: Sigma (R8027)
//  Description:
//	Added new Atmospheric Pressure Setting.  Also, added auxillary
//	code to DEVELOPMENT assertions.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//=====================================================================

#include "SettingsMgr.hh"

//@ Usage-Classes

// Breath-Delivery Batch, Bounded Settings...
#include "ApneaFlowAccelPercentSetting.hh"
#include "ApneaInspPressSetting.hh"
#include "ApneaInspTimeSetting.hh"
#include "ApneaIntervalSetting.hh"
#include "ApneaMinInspFlowSetting.hh"
#include "ApneaO2PercentSetting.hh"
#include "ApneaPeakInspFlowSetting.hh"
#include "ApneaPlateauTimeSetting.hh"
#include "ApneaRespRateSetting.hh"
#include "ApneaTidalVolSetting.hh"
#include "DiscoSensSetting.hh"
#include "ExpSensSetting.hh"
#include "FlowAccelPercentSetting.hh"
#include "FlowSensSetting.hh"
#include "HighCctPressSetting.hh"
#include "HighInspTidalVolSetting.hh"
#include "HlRatioSetting.hh"
#include "HumidVolumeSetting.hh"
#include "IdealBodyWeightSetting.hh"
#include "InspPressSetting.hh"
#include "InspTimeSetting.hh"
#include "LowCctPressSetting.hh"
#include "MinInspFlowSetting.hh"
#include "O2PercentSetting.hh"
#include "PeakInspFlowSetting.hh"
#include "PercentSupportSetting.hh"
#include "PlateauTimeSetting.hh"
#include "PeepHighSetting.hh"
#include "PeepHighTimeSetting.hh"
#include "PeepLowSetting.hh"
#include "PeepLowTimeSetting.hh"
#include "PeepSetting.hh"
#include "PressSensSetting.hh"
#include "PressSuppSetting.hh"
#include "RespRateSetting.hh"
#include "TidalVolumeSetting.hh"
#include "TubeIdSetting.hh"
#include "VolumeSuppSetting.hh"
#include "HighSpontInspTimeSetting.hh"
#include "LeakCompEnabledSetting.hh"
#include "ProxEnabledSetting.hh"

// Breath-Delivery Batch, Discrete Settings...
#include "ApneaFlowPatternSetting.hh"
#include "ApneaMandTypeSetting.hh"
#include "FlowPatternSetting.hh"
#include "HumidTypeSetting.hh"
#include "MandTypeSetting.hh"
#include "ModeSetting.hh"
#include "NominalLineVoltSetting.hh"
#include "PatientCctTypeSetting.hh"
#include "SupportTypeSetting.hh"
#include "TriggerTypeSetting.hh"
#include "TubeTypeSetting.hh"
#include "VentTypeSetting.hh"

// NON-Breath-Delivery Batch, Bounded Settings...
#include "ApneaExpTimeSetting.hh"
#include "ApneaIeRatioSetting.hh"
#include "AtmPressureSetting.hh"
#include "ExpTimeSetting.hh"
#include "HighExhMinVolSetting.hh"
#include "HighExhTidalVolSetting.hh"
#include "HighRespRateSetting.hh"
#include "IeRatioSetting.hh"
#include "LowMandTidalVolSetting.hh"
#include "LowExhMinVolSetting.hh"
#include "LowSpontTidalVolSetting.hh"
#include "MinuteVolumeSetting.hh"
#include "VsuppIbwRatioSetting.hh"
#include "VtIbwRatioSetting.hh"

// NON-Breath-Delivery Batch, Discrete Settings...
#include "ApneaConstantParmSetting.hh"
#include "ConstantParmSetting.hh"
#include "DciBaudRateSetting.hh"
#include "DciDataBitsSetting.hh"
#include "DciParityModeSetting.hh"
#include "ComPortConfigSetting.hh"
#include "Fio2EnabledSetting.hh"
#include "MonthSetting.hh"
#include "PressUnitsSetting.hh"
#include "LanguageSetting.hh"
#include "ServiceBaudRateSetting.hh"
#include "TrendPresetTypeSetting.hh"
#include "DateFormatSetting.hh"

// NON-Breath-Delivery Batch, Sequential Settings...
#include "DaySetting.hh"
#include "DisplayContrastDeltaSetting.hh"
#include "HourSetting.hh"
#include "MinuteSetting.hh"
#include "YearSetting.hh"


// NON-Batch, Bounded Settings...
#include "PressVolLoopBaseSetting.hh"
#include "TrendCursorPositionSetting.hh"

// NON-Batch, Discrete Settings...
#include "FlowPlotScaleSetting.hh"
#include "Plot1TypeSetting.hh"
#include "Plot2TypeSetting.hh"
#include "TrendSelectSetting.hh"
#include "TrendTimeScaleSetting.hh"
#include "ShadowTraceEnableSetting.hh"

#include "PressPlotScaleSetting.hh"
#include "TimePlotScaleSetting.hh"
#include "VolumePlotScaleSetting.hh"

// NON-Batch, Sequential Settings...
#include "AlarmVolumeSetting.hh"
#include "DisplayBrightnessSetting.hh"
#include "DisplayContrastScaleSetting.hh"
//@ End-Usage


//@ Code...

#if defined(SIGMA_DEVELOPMENT)  &&  defined(SIGMA_UNIT_TEST)
// for unit test only, locate all static data items in this module to
// the 'stacks' data section -- to allow for more space for development
// code...
#  pragma  option -NZstacks
#endif // defined(SIGMA_DEVELOPMENT)  &&  defined(SIGMA_UNIT_TEST)

//=====================================================================
//
//  Static Data Definitions...
//
//=====================================================================

//===================================================================
//  Define memory for each of the Bounded, Batch Settings (BD)...
//===================================================================

static Uint PApneaFlowAccelPercentSetting_[
   (sizeof(ApneaFlowAccelPercentSetting) + sizeof(Uint) - 1) / sizeof(Uint)
					  ];
static Uint PApneaInspPressSetting_[(sizeof(ApneaInspPressSetting) +
				     sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PApneaInspTimeSetting_[(sizeof(ApneaInspTimeSetting) +
				    sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PApneaIntervalSetting_[(sizeof(ApneaIntervalSetting) +
				    sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PApneaMinInspFlowSetting_[(sizeof(ApneaMinInspFlowSetting) +
				       sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PApneaO2PercentSetting_[(sizeof(ApneaO2PercentSetting) +
				     sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PApneaPeakInspFlowSetting_[(sizeof(ApneaPeakInspFlowSetting) +
					sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PApneaPlateauTimeSetting_[(sizeof(ApneaPlateauTimeSetting) +
				       sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PApneaRespRateSetting_[(sizeof(ApneaRespRateSetting) +
				    sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PApneaTidalVolSetting_[(sizeof(ApneaTidalVolSetting) +
				    sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PDiscoSensSetting_[(sizeof(DiscoSensSetting) +
				sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PExpSensSetting_[(sizeof(ExpSensSetting) +
			      sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PFlowAccelPercentSetting_[(sizeof(FlowAccelPercentSetting) +
				       sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PFlowSensSetting_[(sizeof(FlowSensSetting) +
			       sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PHighCctPressSetting_[(sizeof(HighCctPressSetting) +
				   sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PHighExhTidalVolSetting_[(sizeof(HighExhTidalVolSetting) +
				      sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PHighInspTidalVolSetting_[(sizeof(HighInspTidalVolSetting) +
				  sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PHlRatioSetting_[(sizeof(HlRatioSetting) +
				      sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PHumidVolumeSetting_[(sizeof(HumidVolumeSetting) +
				      sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PIbwSetting_[(sizeof(IdealBodyWeightSetting) +
			  sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PInspPressSetting_[(sizeof(InspPressSetting) +
				sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PInspTimeSetting_[(sizeof(InspTimeSetting) +
			       sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PMinInspFlowSetting_[(sizeof(MinInspFlowSetting) +
				  sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PO2PercentSetting_[(sizeof(O2PercentSetting) +
				sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PPeakInspFlowSetting_[(sizeof(PeakInspFlowSetting) +
				   sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PPercentSupportSetting_[(sizeof(PercentSupportSetting) +
				   sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PPlateauTimeSetting_[(sizeof(PlateauTimeSetting) +
				  sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PPeepHighSetting_[(sizeof(PeepHighSetting) +
                           sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PPeepHighTimeSetting_[(sizeof(PeepHighTimeSetting) +
                           sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PPeepLowSetting_[(sizeof(PeepLowSetting) +
                           sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PPeepLowTimeSetting_[(sizeof(PeepLowTimeSetting) +
                           sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PPeepSetting_[(sizeof(PeepSetting) +
			   sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PPressSensSetting_[(sizeof(PressSensSetting) +
				sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PPressSuppSetting_[(sizeof(PressSuppSetting) +
				sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PRespRateSetting_[(sizeof(RespRateSetting) +
			       sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PTidalVolumeSetting_[(sizeof(TidalVolumeSetting) +
				  sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PTubeIdSetting_[(sizeof(TubeIdSetting) +
				  sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PVolumeSuppSetting_[(sizeof(VolumeSuppSetting) +
				sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PHighSpontInspTimeSetting_[(sizeof(HighSpontInspTimeSetting) +
				sizeof(Uint) - 1) / sizeof(Uint)];
//===================================================================
//  Define memory for each of the Discrete, Batch Settings (BD)...
//===================================================================

static Uint PApneaFlowPatternSetting_[(sizeof(ApneaFlowPatternSetting) +
				       sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PApneaMandTypeSetting_[(sizeof(ApneaMandTypeSetting) +
				    sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PFlowPatternSetting_[(sizeof(FlowPatternSetting) +
				  sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PHumidTypeSetting_[(sizeof(HumidTypeSetting) +
				sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PMandTypeSetting_[(sizeof(MandTypeSetting) +
			       sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PModeSetting_[(sizeof(ModeSetting) +
			   sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PNominalLineVoltSetting_[(sizeof(NominalLineVoltSetting) +
				      sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PPatientCctTypeSetting_[(sizeof(PatientCctTypeSetting) +
				     sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PSupportTypeSetting_[(sizeof(SupportTypeSetting) +
				  sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PTriggerTypeSetting_[(sizeof(TriggerTypeSetting) +
				  sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PTubeTypeSetting_[(sizeof(TubeTypeSetting) +
				  sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PVentTypeSetting_[(sizeof(VentTypeSetting) +
				  sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PLeakCompEnabledSetting_[(sizeof(LeakCompEnabledSetting) +
				  sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PProxEnabledSetting_[(sizeof(ProxEnabledSetting) +
				  sizeof(Uint) - 1) / sizeof(Uint)];


//===================================================================
//  Define memory for each of the Bounded, Batch Settings (Non-BD)...
//===================================================================

static Uint PApneaExpTimeSetting_[(sizeof(ApneaExpTimeSetting) +
				   sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PApneaIeRatioSetting_[(sizeof(ApneaIeRatioSetting) +
				   sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PAtmPressureSetting_[(sizeof(AtmPressureSetting) +
				   sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PExpTimeSetting_[(sizeof(ExpTimeSetting) +
			      sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PHighExhMinVolSetting_[(sizeof(HighExhMinVolSetting) +
				    sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PHighRespRateSetting_[(sizeof(HighRespRateSetting) +
				   sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PIeRatioSetting_[(sizeof(IeRatioSetting) +
			      sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PLowCctPressSetting_[(sizeof(LowCctPressSetting) +
				   sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PLowMandTidalVolSetting_[(sizeof(LowMandTidalVolSetting) +
				      sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PLowExhMinVolSetting_[(sizeof(LowExhMinVolSetting) +
				   sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PLowSpontTidalVolSetting_[(sizeof(LowSpontTidalVolSetting) +
				       sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PMinuteVolumeSetting_[(sizeof(MinuteVolumeSetting) +
				   sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PVsuppIbwRatioSetting_[(sizeof(VsuppIbwRatioSetting) +
				   sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PVtIbwRatioSetting_[(sizeof(VtIbwRatioSetting) +
				   sizeof(Uint) - 1) / sizeof(Uint)];

//===================================================================
//  Define memory for each of the Discrete, Batch Settings (Non-BD)...
//===================================================================

static Uint PApneaConstantParmSetting_[(sizeof(ApneaConstantParmSetting) +
					sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PConstantParmSetting_[(sizeof(ConstantParmSetting) +
				   sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PDciBaudRateSetting_[(sizeof(DciBaudRateSetting) +
				 sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PDciDataBitsSetting_[(sizeof(DciDataBitsSetting) +
				 sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PDciParityModeSetting_[(sizeof(DciParityModeSetting) +
				   sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PFio2EnabledSetting_[(sizeof(Fio2EnabledSetting) +
				 sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PMonthSetting_[(sizeof(MonthSetting) +
			    sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PPressUnitsSetting_[(sizeof(PressUnitsSetting) +
				 sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PLanguageSetting_[(sizeof(LanguageSetting) +
				 sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PServiceBaudRateSetting_[(sizeof(ServiceBaudRateSetting) +
				     sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PCom2BaudRateSetting_[(sizeof(DciBaudRateSetting) +
                 sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PCom2DataBitsSetting_[(sizeof(DciDataBitsSetting) +
                 sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PCom2ParityModeSetting_[(sizeof(DciParityModeSetting) +
                 sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PCom3BaudRateSetting_[(sizeof(DciBaudRateSetting) +
                 sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PCom3DataBitsSetting_[(sizeof(DciDataBitsSetting) +
                 sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PCom3ParityModeSetting_[(sizeof(DciParityModeSetting) +
                 sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PCom1PortConfigSetting_[(sizeof(ComPortConfigSetting) +
                 sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PCom2PortConfigSetting_[(sizeof(ComPortConfigSetting) +
                 sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PCom3PortConfigSetting_[(sizeof(ComPortConfigSetting) +
                 sizeof(Uint) - 1) / sizeof(Uint)];

static Uint PDateFormatSetting_[(sizeof(DateFormatSetting) +
                 sizeof(Uint) - 1) / sizeof(Uint)];

//===================================================================
//  Define memory for each of the Sequential, Batch Settings (Non-BD)...
//===================================================================

static Uint PDaySetting_[(sizeof(DaySetting) +
			  sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PDisplayContrastDeltaSetting_[
    (sizeof(DisplayContrastDeltaSetting) + sizeof(Uint) - 1) / sizeof(Uint)
  					 ];
static Uint PHourSetting_[(sizeof(HourSetting) +
			   sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PMinuteSetting_[(sizeof(MinuteSetting) +
			     sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PYearSetting_[(sizeof(YearSetting) +
			   sizeof(Uint) - 1) / sizeof(Uint)];


//===================================================================
//  Define memory for each of the Bounded, Non-Batch Settings...
//===================================================================

static Uint PPressVolLoopBaseSetting_[(sizeof(PressVolLoopBaseSetting) +
				       sizeof(Uint) - 1) / sizeof(Uint)];

static Uint PTrendCursorPositionSetting_[(sizeof(TrendCursorPositionSetting) +
				       sizeof(Uint) - 1) / sizeof(Uint)];
//===================================================================
//  Define memory for each of the Discrete, Non-Batch Settings...
//===================================================================

static Uint PFlowPlotScaleSetting_[(sizeof(FlowPlotScaleSetting) +
				   sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PPlot1TypeSetting_[(sizeof(Plot1TypeSetting) +
				sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PPlot2TypeSetting_[(sizeof(Plot2TypeSetting) +
				sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PShadowTraceEnableSetting_[(sizeof(ShadowTraceEnableSetting) +
				sizeof(Uint) - 1) / sizeof(Uint)];

static Uint PTrendSelectSetting1_[(sizeof(TrendSelectSetting) +
                sizeof(Uint) - 1) / sizeof(Uint)];

static Uint PTrendSelectSetting2_[(sizeof(TrendSelectSetting) +
                sizeof(Uint) - 1) / sizeof(Uint)];

static Uint PTrendSelectSetting3_[(sizeof(TrendSelectSetting) +
                sizeof(Uint) - 1) / sizeof(Uint)];

static Uint PTrendSelectSetting4_[(sizeof(TrendSelectSetting) +
                sizeof(Uint) - 1) / sizeof(Uint)];

static Uint PTrendSelectSetting5_[(sizeof(TrendSelectSetting) +
                sizeof(Uint) - 1) / sizeof(Uint)];

static Uint PTrendSelectSetting6_[(sizeof(TrendSelectSetting) +
                sizeof(Uint) - 1) / sizeof(Uint)];

static Uint PTrendTimeScaleSetting_[(sizeof(TrendTimeScaleSetting) +
                sizeof(Uint) - 1) / sizeof(Uint)];

static Uint PTrendPresetTypeSetting_[(sizeof(TrendPresetTypeSetting) +
                 sizeof(Uint) - 1) / sizeof(Uint)];


static Uint PPressPlotScaleSetting_[(sizeof(PressPlotScaleSetting) +
				  sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PTimePlotScaleSetting_[(sizeof(TimePlotScaleSetting) +
				  sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PVolumePlotScaleSetting_[(sizeof(VolumePlotScaleSetting) +
				 sizeof(Uint) - 1) / sizeof(Uint)];

//===================================================================
//  Define memory for each of the Sequential, Non-Batch Settings...
//===================================================================

static Uint PAlarmVolumeSetting_[(sizeof(AlarmVolumeSetting) +
				  sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PDisplayBrightnessSetting_[(sizeof(DisplayBrightnessSetting) +
				        sizeof(Uint) - 1) / sizeof(Uint)];
static Uint PDisplayContrastScaleSetting_[
    (sizeof(DisplayContrastScaleSetting) + sizeof(Uint) - 1) / sizeof(Uint)
  					 ];


//=====================================================================
//
//  Static Member Definitions...
//
//=====================================================================

SettingPtr  SettingsMgr::ArrSettingPtrs_[SettingId::TOTAL_SETTING_IDS];


//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  Initialize(void)  [static]
//
//@ Interface-Description
//  Initialize the instances of each, and every, setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
SettingsMgr::Initialize(void)
{
  CALL_TRACE("Initialize()");

  //====================================================================
  //
  // Initialize each of the batch settings...
  //
  //====================================================================

  //====================================================================
  // Initialize each of the Bounded, Batch Settings (BD)...
  //====================================================================

  SAFE_CLASS_ASSERTION((sizeof(::PApneaFlowAccelPercentSetting_) >=
			sizeof(ApneaFlowAccelPercentSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::APNEA_FLOW_ACCEL_PERCENT] =
      new (::PApneaFlowAccelPercentSetting_) ApneaFlowAccelPercentSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PApneaInspPressSetting_) >=
			sizeof(ApneaInspPressSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::APNEA_INSP_PRESS] =
		new (::PApneaInspPressSetting_) ApneaInspPressSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PApneaInspTimeSetting_) >=
			sizeof(ApneaInspTimeSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::APNEA_INSP_TIME] =
		new (::PApneaInspTimeSetting_) ApneaInspTimeSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PApneaIntervalSetting_) >=
			sizeof(ApneaIntervalSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::APNEA_INTERVAL] =
		new (::PApneaIntervalSetting_) ApneaIntervalSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PApneaMinInspFlowSetting_) >=
			sizeof(ApneaMinInspFlowSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::APNEA_MIN_INSP_FLOW] =
		new (::PApneaMinInspFlowSetting_) ApneaMinInspFlowSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PApneaO2PercentSetting_) >=
			sizeof(ApneaO2PercentSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::APNEA_O2_PERCENT] =
		new (::PApneaO2PercentSetting_) ApneaO2PercentSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PApneaPeakInspFlowSetting_) >=
			sizeof(ApneaPeakInspFlowSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::APNEA_PEAK_INSP_FLOW] =
		new (::PApneaPeakInspFlowSetting_) ApneaPeakInspFlowSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PApneaPlateauTimeSetting_) >=
			sizeof(ApneaPlateauTimeSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::APNEA_PLATEAU_TIME] =
		new (::PApneaPlateauTimeSetting_) ApneaPlateauTimeSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PApneaRespRateSetting_) >=
			sizeof(ApneaRespRateSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::APNEA_RESP_RATE] =
		new (::PApneaRespRateSetting_) ApneaRespRateSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PApneaTidalVolSetting_) >=
			sizeof(ApneaTidalVolSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::APNEA_TIDAL_VOLUME] =
		new (::PApneaTidalVolSetting_) ApneaTidalVolSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PDiscoSensSetting_) >=
			sizeof(DiscoSensSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::DISCO_SENS] =
		new (::PDiscoSensSetting_) DiscoSensSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PExpSensSetting_) >=
			sizeof(ExpSensSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::EXP_SENS] =
		new (::PExpSensSetting_) ExpSensSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PFlowAccelPercentSetting_) >=
			sizeof(FlowAccelPercentSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::FLOW_ACCEL_PERCENT] =
		new (::PFlowAccelPercentSetting_) FlowAccelPercentSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PFlowSensSetting_) >=
			sizeof(FlowSensSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::FLOW_SENS] =
		new (::PFlowSensSetting_) FlowSensSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PHighCctPressSetting_) >=
			sizeof(HighCctPressSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::HIGH_CCT_PRESS] =
		new (::PHighCctPressSetting_) HighCctPressSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PHlRatioSetting_) >=
			sizeof(HlRatioSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::HL_RATIO] =
		new (::PHlRatioSetting_) HlRatioSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PHumidVolumeSetting_) >=
			sizeof(HumidVolumeSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::HUMID_VOLUME] =
		new (::PHumidVolumeSetting_) HumidVolumeSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PIbwSetting_) >=
			sizeof(IdealBodyWeightSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::IBW] =
		new (::PIbwSetting_) IdealBodyWeightSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PInspPressSetting_) >=
			sizeof(InspPressSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::INSP_PRESS] =
		new (::PInspPressSetting_) InspPressSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PInspTimeSetting_) >=
			sizeof(InspTimeSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::INSP_TIME] =
		new (::PInspTimeSetting_) InspTimeSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PMinInspFlowSetting_) >=
			sizeof(MinInspFlowSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::MIN_INSP_FLOW] =
		new (::PMinInspFlowSetting_) MinInspFlowSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PO2PercentSetting_) >=
			sizeof(O2PercentSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::OXYGEN_PERCENT] =
		new (::PO2PercentSetting_) O2PercentSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PPeakInspFlowSetting_) >=
			sizeof(PeakInspFlowSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::PEAK_INSP_FLOW] =
		new (::PPeakInspFlowSetting_) PeakInspFlowSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PPercentSupportSetting_) >=
			sizeof(PercentSupportSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::PERCENT_SUPPORT] =
		new (::PPercentSupportSetting_) PercentSupportSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PPeepSetting_) >= sizeof(PeepSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::PEEP] =
		new (::PPeepSetting_) PeepSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PPeepHighSetting_) >= sizeof(PeepHighSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::PEEP_HIGH] =
		new (::PPeepHighSetting_) PeepHighSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PPeepHighTimeSetting_) >= sizeof(PeepHighTimeSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::PEEP_HIGH_TIME] =
		new (::PPeepHighTimeSetting_) PeepHighTimeSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PPeepLowSetting_) >= sizeof(PeepLowSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::PEEP_LOW] =
		new (::PPeepLowSetting_) PeepLowSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PPeepLowTimeSetting_) >= sizeof(PeepLowTimeSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::PEEP_LOW_TIME] =
		new (::PPeepLowTimeSetting_) PeepLowTimeSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PPlateauTimeSetting_) >=
			sizeof(PlateauTimeSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::PLATEAU_TIME] =
		new (::PPlateauTimeSetting_) PlateauTimeSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PPressSensSetting_) >=
			sizeof(PressSensSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::PRESS_SENS] =
		new (::PPressSensSetting_) PressSensSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PPressSuppSetting_) >=
			sizeof(PressSuppSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::PRESS_SUPP_LEVEL] =
		new (::PPressSuppSetting_) PressSuppSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PRespRateSetting_) >=
			sizeof(RespRateSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::RESP_RATE] =
		new (::PRespRateSetting_) RespRateSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PTidalVolumeSetting_) >=
			sizeof(TidalVolumeSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::TIDAL_VOLUME] =
		new (::PTidalVolumeSetting_) TidalVolumeSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PTubeIdSetting_) >=
			sizeof(TubeIdSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::TUBE_ID] =
		new (::PTubeIdSetting_) TubeIdSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PHighInspTidalVolSetting_) >=
			sizeof(HighInspTidalVolSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::HIGH_INSP_TIDAL_VOL] =
		new (::PHighInspTidalVolSetting_) HighInspTidalVolSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PVolumeSuppSetting_) >=
			sizeof(VolumeSuppSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::VOLUME_SUPPORT] =
		new (::PVolumeSuppSetting_) VolumeSuppSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PHighSpontInspTimeSetting_) >=
			sizeof(HighSpontInspTimeSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::HIGH_SPONT_INSP_TIME] =
		new (::PHighSpontInspTimeSetting_) HighSpontInspTimeSetting();

  //====================================================================
  // Initialize each of the Discrete, Batch Settings (BD)...
  //====================================================================

  SAFE_CLASS_ASSERTION((sizeof(::PApneaFlowPatternSetting_) >=
			sizeof(ApneaFlowPatternSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::APNEA_FLOW_PATTERN] =
		new (::PApneaFlowPatternSetting_) ApneaFlowPatternSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PApneaMandTypeSetting_) >=
			sizeof(ApneaMandTypeSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::APNEA_MAND_TYPE] =
		new (::PApneaMandTypeSetting_) ApneaMandTypeSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PFio2EnabledSetting_) >=
			sizeof(Fio2EnabledSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::FIO2_ENABLED] =
		    new (::PFio2EnabledSetting_) Fio2EnabledSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PFlowPatternSetting_) >=
			sizeof(FlowPatternSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::FLOW_PATTERN] =
		new (::PFlowPatternSetting_) FlowPatternSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PHumidTypeSetting_) >=
			sizeof(HumidTypeSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::HUMID_TYPE] =
		new (::PHumidTypeSetting_) HumidTypeSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PMandTypeSetting_) >=
			sizeof(MandTypeSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::MAND_TYPE] =
		new (::PMandTypeSetting_) MandTypeSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PSupportTypeSetting_) >=
			sizeof(SupportTypeSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::SUPPORT_TYPE] =
		new (::PSupportTypeSetting_) SupportTypeSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PTriggerTypeSetting_) >=
			sizeof(TriggerTypeSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::TRIGGER_TYPE] =
		new (::PTriggerTypeSetting_) TriggerTypeSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PModeSetting_) >=
			sizeof(ModeSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::MODE] =
		new (::PModeSetting_) ModeSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PNominalLineVoltSetting_) >=
			sizeof(NominalLineVoltSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::NOMINAL_VOLT] =
		  new (::PNominalLineVoltSetting_) NominalLineVoltSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PPatientCctTypeSetting_) >=
			sizeof(PatientCctTypeSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::PATIENT_CCT_TYPE] =
		new (::PPatientCctTypeSetting_) PatientCctTypeSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PTubeTypeSetting_) >=
			sizeof(TubeTypeSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::TUBE_TYPE] =
		new (::PTubeTypeSetting_) TubeTypeSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PVentTypeSetting_) >=
			sizeof(VentTypeSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::VENT_TYPE] =
		new (::PVentTypeSetting_) VentTypeSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PLeakCompEnabledSetting_) >=        
			sizeof(LeakCompEnabledSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::LEAK_COMP_ENABLED] =
		new (::PLeakCompEnabledSetting_) LeakCompEnabledSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PProxEnabledSetting_) >=
			sizeof(ProxEnabledSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::PROX_ENABLED] =
		new (::PProxEnabledSetting_) ProxEnabledSetting();

  //====================================================================
  // Initialize each of the Bounded, Batch Settings (Non-BD)...
  //====================================================================

  SAFE_CLASS_ASSERTION((sizeof(::PApneaExpTimeSetting_) >=
			sizeof(ApneaExpTimeSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::APNEA_EXP_TIME] =
		new (::PApneaExpTimeSetting_) ApneaExpTimeSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PApneaIeRatioSetting_) >=
			sizeof(ApneaIeRatioSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::APNEA_IE_RATIO] =
		new (::PApneaIeRatioSetting_) ApneaIeRatioSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PAtmPressureSetting_) >=
			sizeof(AtmPressureSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::ATM_PRESSURE] =
		new (::PAtmPressureSetting_) AtmPressureSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PExpTimeSetting_) >=
			sizeof(ExpTimeSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::EXP_TIME] =
		new (::PExpTimeSetting_) ExpTimeSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PHighExhMinVolSetting_) >=
			sizeof(HighExhMinVolSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::HIGH_EXH_MINUTE_VOL] =
		new (::PHighExhMinVolSetting_) HighExhMinVolSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PHighExhTidalVolSetting_) >=
			sizeof(HighExhTidalVolSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::HIGH_EXH_TIDAL_VOL] =
		new (::PHighExhTidalVolSetting_) HighExhTidalVolSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PHighRespRateSetting_) >=
			sizeof(HighRespRateSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::HIGH_RESP_RATE] =
		new (::PHighRespRateSetting_) HighRespRateSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PIeRatioSetting_) >=
			sizeof(IeRatioSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::IE_RATIO] =
		new (::PIeRatioSetting_) IeRatioSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PLowCctPressSetting_) >=
			sizeof(LowCctPressSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::LOW_CCT_PRESS] =
		new (::PLowCctPressSetting_) LowCctPressSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PLowMandTidalVolSetting_) >=
			sizeof(LowMandTidalVolSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::LOW_EXH_MAND_TIDAL_VOL] =
		new (::PLowMandTidalVolSetting_) LowMandTidalVolSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PLowExhMinVolSetting_) >=
			sizeof(LowExhMinVolSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::LOW_EXH_MINUTE_VOL] =
		new (::PLowExhMinVolSetting_) LowExhMinVolSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PLowSpontTidalVolSetting_) >=
			sizeof(LowSpontTidalVolSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::LOW_EXH_SPONT_TIDAL_VOL] =
		new (::PLowSpontTidalVolSetting_) LowSpontTidalVolSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PMinuteVolumeSetting_) >=
			sizeof(MinuteVolumeSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::MINUTE_VOLUME] =
		new (::PMinuteVolumeSetting_) MinuteVolumeSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PVsuppIbwRatioSetting_) >=
			sizeof(VsuppIbwRatioSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::VSUPP_IBW_RATIO] =
		new (::PVsuppIbwRatioSetting_) VsuppIbwRatioSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PVtIbwRatioSetting_) >=
			sizeof(VtIbwRatioSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::VT_IBW_RATIO] =
		new (::PVtIbwRatioSetting_) VtIbwRatioSetting();

  //====================================================================
  // Initialize each of the Discrete, Batch Settings (Non-BD)...
  //====================================================================

  SAFE_CLASS_ASSERTION((sizeof(::PApneaConstantParmSetting_) >=
			sizeof(ApneaConstantParmSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::APNEA_CONSTANT_PARM] =
	      new (::PApneaConstantParmSetting_) ApneaConstantParmSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PConstantParmSetting_) >=
			sizeof(ConstantParmSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::CONSTANT_PARM] =
	      new (::PConstantParmSetting_) ConstantParmSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PDciBaudRateSetting_) >=
			sizeof(DciBaudRateSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::DCI_BAUD_RATE] =
              new (::PDciBaudRateSetting_) 
              DciBaudRateSetting(SettingId::DCI_BAUD_RATE);

  SAFE_CLASS_ASSERTION((sizeof(::PDciDataBitsSetting_) >=
			sizeof(DciDataBitsSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::DCI_DATA_BITS] =
              new (::PDciDataBitsSetting_)
              DciDataBitsSetting(SettingId::DCI_DATA_BITS);

  SAFE_CLASS_ASSERTION((sizeof(::PDciParityModeSetting_) >=
			sizeof(DciParityModeSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::DCI_PARITY_MODE] =
              new (::PDciParityModeSetting_) 
              DciParityModeSetting(SettingId::DCI_PARITY_MODE);

  SAFE_CLASS_ASSERTION((sizeof(::PMonthSetting_) >=
			sizeof(MonthSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::MONTH] =
		    new (::PMonthSetting_) MonthSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PPressUnitsSetting_) >=
			sizeof(PressUnitsSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::PRESS_UNITS] =
		    new (::PPressUnitsSetting_) PressUnitsSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PLanguageSetting_) >=
			sizeof(LanguageSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::LANGUAGE] =
		    new (::PLanguageSetting_) LanguageSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PServiceBaudRateSetting_) >=
			sizeof(ServiceBaudRateSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::SERVICE_BAUD_RATE] =
		  new (::PServiceBaudRateSetting_) ServiceBaudRateSetting();


  SAFE_CLASS_ASSERTION((sizeof(::PCom2BaudRateSetting_) >=
            sizeof(DciBaudRateSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::COM2_BAUD_RATE] =
            new (::PCom2BaudRateSetting_) 
            DciBaudRateSetting(SettingId::COM2_BAUD_RATE);

  SAFE_CLASS_ASSERTION((sizeof(::PCom2DataBitsSetting_) >=
            sizeof(DciDataBitsSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::COM2_DATA_BITS] =
            new (::PCom2DataBitsSetting_)  
            DciDataBitsSetting(SettingId::COM2_DATA_BITS);

  SAFE_CLASS_ASSERTION((sizeof(::PCom2ParityModeSetting_) >=
            sizeof(DciParityModeSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::COM2_PARITY_MODE] =
            new (::PCom2ParityModeSetting_) 
            DciParityModeSetting(SettingId::COM2_PARITY_MODE);

  SAFE_CLASS_ASSERTION((sizeof(::PCom3BaudRateSetting_) >=
            sizeof(DciBaudRateSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::COM3_BAUD_RATE] =
            new (::PCom3BaudRateSetting_) 
            DciBaudRateSetting(SettingId::COM3_BAUD_RATE);

  SAFE_CLASS_ASSERTION((sizeof(::PCom3DataBitsSetting_) >=
            sizeof(DciDataBitsSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::COM3_DATA_BITS] =
            new (::PCom3DataBitsSetting_) 
            DciDataBitsSetting(SettingId::COM3_DATA_BITS);

  SAFE_CLASS_ASSERTION((sizeof(::PCom3ParityModeSetting_) >=
            sizeof(DciParityModeSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::COM3_PARITY_MODE] =
            new (::PCom3ParityModeSetting_) 
            DciParityModeSetting(SettingId::COM3_PARITY_MODE);

  SAFE_CLASS_ASSERTION((sizeof(::PCom1PortConfigSetting_) >=
            sizeof(ComPortConfigSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::COM1_CONFIG] =
            new (::PCom1PortConfigSetting_) 
            ComPortConfigSetting(SettingId::COM1_CONFIG);

  SAFE_CLASS_ASSERTION((sizeof(::PCom2PortConfigSetting_) >=
            sizeof(ComPortConfigSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::COM2_CONFIG] =
            new (::PCom2PortConfigSetting_) 
            ComPortConfigSetting(SettingId::COM2_CONFIG);

  SAFE_CLASS_ASSERTION((sizeof(::PCom3PortConfigSetting_) >=
            sizeof(ComPortConfigSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::COM3_CONFIG] =
            new (::PCom3PortConfigSetting_) 
            ComPortConfigSetting(SettingId::COM3_CONFIG);



  SAFE_CLASS_ASSERTION((sizeof(::PDateFormatSetting_) >=
			sizeof(DateFormatSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::DATE_FORMAT] =
		  new (::PDateFormatSetting_) DateFormatSetting();


  //====================================================================
  // Initialize each of the Sequential, Batch Settings (Non-BD)...
  //====================================================================

  SAFE_CLASS_ASSERTION((sizeof(::PDaySetting_) >=
			sizeof(DaySetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::DAY] =
		  new (::PDaySetting_) DaySetting();

  SAFE_CLASS_ASSERTION((sizeof(::PDisplayContrastDeltaSetting_) >=
			sizeof(DisplayContrastDeltaSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::DISPLAY_CONTRAST_DELTA] =
	new (::PDisplayContrastDeltaSetting_) DisplayContrastDeltaSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PHourSetting_) >=
			sizeof(HourSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::HOUR] =
		  new (::PHourSetting_) HourSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PMinuteSetting_) >=
			sizeof(MinuteSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::MINUTE] =
		  new (::PMinuteSetting_) MinuteSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PYearSetting_) >=
			sizeof(YearSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::YEAR] =
		  new (::PYearSetting_) YearSetting();


  //====================================================================
  //
  // Initialize each of the Non-Batch Settings...
  //
  //====================================================================

  //====================================================================
  // Initialize each of the Bounded, Non-Batch Settings...
  //====================================================================

  SAFE_CLASS_ASSERTION((sizeof(::PPressVolLoopBaseSetting_) >=
			sizeof(PressVolLoopBaseSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::PRESS_VOL_LOOP_BASE] =
		new (::PPressVolLoopBaseSetting_) PressVolLoopBaseSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PTrendCursorPositionSetting_) >=
			sizeof(TrendCursorPositionSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::TREND_CURSOR_POSITION] =
		new (::PTrendCursorPositionSetting_) TrendCursorPositionSetting();

  //====================================================================
  // Initialize each of the Discrete, Non-Batch Settings...
  //====================================================================

  SAFE_CLASS_ASSERTION((sizeof(::PFlowPlotScaleSetting_) >=
			sizeof(FlowPlotScaleSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::FLOW_PLOT_SCALE] =
		    new (::PFlowPlotScaleSetting_) FlowPlotScaleSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PPlot1TypeSetting_) >=
			sizeof(Plot1TypeSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::PLOT1_TYPE] =
			new (::PPlot1TypeSetting_) Plot1TypeSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PPlot2TypeSetting_) >=
			sizeof(Plot2TypeSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::PLOT2_TYPE] =
			new (::PPlot2TypeSetting_) Plot2TypeSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PTrendSelectSetting1_) >=
			sizeof(TrendSelectSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::TREND_SELECT_1] =
  new (::PTrendSelectSetting1_) TrendSelectSetting(SettingId::TREND_SELECT_1);

  SAFE_CLASS_ASSERTION((sizeof(::PTrendSelectSetting2_) >=
			sizeof(TrendSelectSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::TREND_SELECT_2] =
  new (::PTrendSelectSetting2_) TrendSelectSetting(SettingId::TREND_SELECT_2);

  SAFE_CLASS_ASSERTION((sizeof(::PTrendSelectSetting3_) >=
			sizeof(TrendSelectSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::TREND_SELECT_3] =
  new (::PTrendSelectSetting3_) TrendSelectSetting(SettingId::TREND_SELECT_3);

  SAFE_CLASS_ASSERTION((sizeof(::PTrendSelectSetting4_) >=
			sizeof(TrendSelectSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::TREND_SELECT_4] =
  new (::PTrendSelectSetting4_) TrendSelectSetting(SettingId::TREND_SELECT_4);

  SAFE_CLASS_ASSERTION((sizeof(::PTrendSelectSetting5_) >=
			sizeof(TrendSelectSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::TREND_SELECT_5] =
  new (::PTrendSelectSetting5_) TrendSelectSetting(SettingId::TREND_SELECT_5);

  SAFE_CLASS_ASSERTION((sizeof(::PTrendSelectSetting6_) >=
			sizeof(TrendSelectSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::TREND_SELECT_6] =
  new (::PTrendSelectSetting6_) TrendSelectSetting(SettingId::TREND_SELECT_6);

  SAFE_CLASS_ASSERTION((sizeof(::PTrendTimeScaleSetting_) >=
			sizeof(TrendTimeScaleSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::TREND_TIME_SCALE] =
  new (::PTrendTimeScaleSetting_) TrendTimeScaleSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PTrendPresetTypeSetting_) >=
			sizeof(TrendPresetTypeSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::TREND_PRESET] =
	      new (::PTrendPresetTypeSetting_) TrendPresetTypeSetting();
                      
  SAFE_CLASS_ASSERTION((sizeof(::PShadowTraceEnableSetting_) >=
			sizeof(ShadowTraceEnableSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::SHADOW_TRACE_ENABLE] =
		new (::PShadowTraceEnableSetting_) ShadowTraceEnableSetting();


  SAFE_CLASS_ASSERTION((sizeof(::PPressPlotScaleSetting_) >=
			sizeof(PressPlotScaleSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::PRESSURE_PLOT_SCALE] =
		    new (::PPressPlotScaleSetting_) PressPlotScaleSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PTimePlotScaleSetting_) >=
			sizeof(TimePlotScaleSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::TIME_PLOT_SCALE] =
		    new (::PTimePlotScaleSetting_) TimePlotScaleSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PVolumePlotScaleSetting_) >=
			sizeof(VolumePlotScaleSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::VOLUME_PLOT_SCALE] =
		  new (::PVolumePlotScaleSetting_) VolumePlotScaleSetting();

  //====================================================================
  // Initialize each of the Sequential, Non-Batch Settings...
  //====================================================================

  SAFE_CLASS_ASSERTION((sizeof(::PAlarmVolumeSetting_) >=
			sizeof(AlarmVolumeSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::ALARM_VOLUME] =
		  new (::PAlarmVolumeSetting_) AlarmVolumeSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PDisplayBrightnessSetting_) >=
			sizeof(DisplayBrightnessSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::DISPLAY_BRIGHTNESS] =
	    new (::PDisplayBrightnessSetting_) DisplayBrightnessSetting();

  SAFE_CLASS_ASSERTION((sizeof(::PDisplayContrastScaleSetting_) >=
			sizeof(DisplayContrastScaleSetting)));
  SettingsMgr::ArrSettingPtrs_[SettingId::DISPLAY_CONTRAST_SCALE] =
	new (::PDisplayContrastScaleSetting_) DisplayContrastScaleSetting();


  for (Uint idx = 0; idx < SettingId::TOTAL_SETTING_IDS; idx++)
  {
    // make sure we initialized each, and every, setting...
    AUX_CLASS_ASSERTION((SettingsMgr::ArrSettingPtrs_[idx] != NULL),
    			idx);
    AUX_CLASS_ASSERTION((SettingsMgr::ArrSettingPtrs_[idx]->getId() == idx),
		    ((SettingsMgr::ArrSettingPtrs_[idx]->getId() << 16) | idx));

    // now that all settings have been constructed, allow those setting
    // subjects that are also observers to set up their monitoring...
    (SettingsMgr::ArrSettingPtrs_[idx])->settingObserverInit();
  }
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
SettingsMgr::SoftFault(const SoftFaultID  softFaultID,
		       const Uint32       lineNumber,
		       const char*        pFileName,
		       const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION, SETTINGS_MGR,
  			  lineNumber, pFileName, pPredicate);
}
