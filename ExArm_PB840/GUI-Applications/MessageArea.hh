#ifndef MessageArea_HH
#define MessageArea_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: MessageArea - The area at the bottom left of the Lower
// Screen currently reserved for displaying help messages explaining
// the symbols on setting buttons, patient data, alarm messages etc..
// This class is part of the GUI-Apps Framework cluster.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/MessageArea.hhv   25.0.4.0   19 Nov 2013 14:08:06   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006  By:  gdc    Date:  09-Aug-2010    SCR Number: 6663
//  Project:  API/MAT
//  Description:
//	Modified to allow for more low priority messages in message area.
//
//  Revision: 005  By:  gdc    Date:  10-Dec-2008    SCR Number: 6377
//  Project:  840S
//  Description:
//  Removed DELTA project single screen support.
//
//  Revision: 004   By: gdc    Date: 24-Sep-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal non-invasive CPAP.
//
//   Revision 003   By:   yyy      Date: 08/18/99         DR Number:   5513
//      Project:   ATC
//      Description:
//         Modified to handle alarm lock and break through help messages.
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "Container.hh"

//@ Usage-Classes
#include "TextField.hh"
#include "GuiAppClassIds.hh"
#include "GuiTimerTarget.hh"
//@ End-Usage

class MessageArea : public Container, public GuiTimerTarget
{
public:
	MessageArea();
	~MessageArea(void);

	//@ Type: MessagePriority
	// An enumerated type listing the different priorities of messages.  The
	// highest priority message must be listed first, the second highest
	// next and so on.
	enum MessagePriority
	{
		//@ Constant: MA_ALARM
		// Indicates a alarm priority message.
		MA_ALARM = 0,

		//@ Constant: MA_HIGH
		// Indicates a high priority message.
		MA_HIGH,

		//@ Constant: MA_MEDIUM
		// Indicates a medium priority message.
		MA_MEDIUM,

		//@ Constant: MA_MEDIUM_LOW
		// Indicates a medium low priority message.
		MA_MEDIUM_LOW,

		//@ Constant: MA_LOW
		// Indicates a low priority message.
		MA_LOW,

		//@ Constant: MA_NUMBER_OF_PRIORITIES
		// Evaluates to the number of defined message priorities.  Must be
		// the last definition in the enumerated type.
		MA_NUMBER_OF_PRIORITIES
	};

	// GuiTimerTarget virtual
	virtual void timerEventHappened(GuiTimerId::GuiTimerIdType timerId);

	void setMessage(StringId msgId, MessagePriority priority = MA_LOW);
	void clearMessage(MessagePriority priority = MA_LOW);

	static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL, 
			  const char*       pPredicate = NULL);

protected:

private:
	// these methods are purposely declared, but not implemented...
	MessageArea(const MessageArea&);		// not implemented...
	void   operator=(const MessageArea&);	// not implemented...

	void updateArea_(void);					// Formats UI of Message Area

	//@ Data-Member: stringIds_
	// An  array storing the string ids of each message of a different
	// priority.
	StringId stringIds_[MA_NUMBER_OF_PRIORITIES];

	//@ Data-Member: messageText_
	// A text field which displays the highest priority message.
	TextField messageText_;

	//@ Data-Member: pSingleMsg_
	// Pointer to string when only a single message is displayed in message area
	// used to not refresh message to eliminate flashing with single message
	StringId pSingleMsg_;

};

#endif // MessageArea_HH 
