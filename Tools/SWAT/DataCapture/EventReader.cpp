//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file EventReader.cpp
//----------------------------------------------------------------------------

//-------------------------------------
// I N C L U D E S
//-------------------------------------
#include "EventReader.h"
#include "CLogger.h"

#include <stdio.h>
#include <stdlib.h>
#include<string.h>

#include "checkloop.h"

#define MAX_PACKET_SIZE 8192*2

//-------------------------------------
// C O D E
//-------------------------------------

//---------------------------
// ThreadEntranceProc
//---------------------------
void EventReader::ThreadEntranceProc()
{


	WSADATA info;
	if (WSAStartup(MAKEWORD( 2, 2 ), &info) != 0)
	{
		int nRetCode = WSAGetLastError();
	    LOG_MESSAGE(LM_ERROR, "EventReader::ThreadEntranceProc() - failed for WSAStartup with error code %d", nRetCode); 
		assert(1);
	}

	boost::shared_ptr<Message> pMessage;

    ssize_t iLen;


    LOG_MESSAGE(LM_INFO,"EventReader(%X)::ThreadEntranceProc thread started.",this);

	boost::shared_ptr<char> g_buffer( new char[MAX_PACKET_SIZE] );

	m_bRunning = true;

//	CheckLoop check;


	while(m_bRunning)
    {
//		check.AddCount();
//		assert(check.TestLoop(2000, 1));//asserts here

        iLen = m_pSocketServer->recv(g_buffer.get(),MAX_PACKET_SIZE);

        if(iLen>0)
        {
            boost::shared_ptr<char> raw_data(new char [iLen+1]);
            memcpy(raw_data.get(), g_buffer.get(), iLen);
            raw_data.get()[iLen]='\0';

			boost::shared_ptr<Message> pMessage(new Message(raw_data , iLen ) );


          //  m_pEventLogger->enqueueEvent(pMessage);

	    std::vector<EventLogger  *>::iterator it=m_pEventLoggerList.begin();
	    for(;it!=m_pEventLoggerList.end();it++)
        {
			(*it)->enqueueEvent(pMessage);
	    }
	   
        } 
        else 
        {
            // Do nothing
			Sleep(1);
        }

	}

    LOG_MESSAGE(LM_INFO,"EventReader(%X)::ThreadEntranceProc thread stopped.",this);
}

//---------------------------
// ThreadEntranceProc
//---------------------------
void *EventReader::ThreadEntranceProc    (void *pVoid)
{
	((EventReader* )pVoid)->ThreadEntranceProc ();	

    Thread::exit();

	return NULL;
}


//---------------------------
// EventReader
//---------------------------

EventReader::EventReader(EventLogger *pEventLogger, SocketServer *pSocketServer):
//	m_pEventLogger(pEventLogger),
    m_pSocketServer(pSocketServer),
	m_bRunning(false)
{
    LOG_MESSAGE(LM_INFO,"EventReader(%X):EventReader() constructor called.",this);

    m_pThread = new Thread(ThreadEntranceProc, (void *)this);

    m_pEventLoggerList.push_back(pEventLogger);
}




//---------------------------
// EventReader
//---------------------------

EventReader::EventReader(EventLogger **pEventLogger, SocketServer *pSocketServer):
//	m_pEventLogger(pEventLogger),
    m_pSocketServer(pSocketServer),
	m_bRunning(false)
{

    LOG_MESSAGE(LM_INFO,"EventReader(%X):EventReader constructor called.",this);

    m_pThread = new Thread(ThreadEntranceProc, (void *)this);

    for(;*pEventLogger;pEventLogger++)
    {
	    m_pEventLoggerList.push_back(*pEventLogger);
    }
   // m_pEventLoggerList.push_back(pEventLogger);
}




//---------------------------
// ~EventReader
//---------------------------

EventReader::~EventReader()
{
    LOG_MESSAGE(LM_INFO,"EventReader(%X):~EventReader() destructor called.",this);


    m_bRunning = false;
    m_pThread->join();
    delete m_pThread;
}

