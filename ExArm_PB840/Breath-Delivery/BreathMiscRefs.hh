
#ifndef BreathMiscRefs_HH
#define BreathMiscRefs_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// File: BreathMiscRefs - All the external references of various breath 
// related data. 
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/BreathMiscRefs.hhv   25.0.4.0   19 Nov 2013 13:59:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 018   By: erm    Date: 5-May-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//      Removed ProxBreathRecord
// 
//  Revision: 018   By: rpr    Date: 23-Jan-2009    SCR Number: 6435  
//  Project:  840S
//  Description:
//      Modified for code review comments.
// 
//  Revision: 017   By: rhj   Date:  18-Nov-2008    SCR Number: 6435 
//  Project:  840S
//  Description:
//		Leak Compensation project-related changes:
//		Added RLeakCompMgr object references.
//
//  Revision: 016   By: rpr   Date:  24-Oct-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//		+20 O2 project-related changes:
//		Added RCALO2Request object references.
//
//  Revision: 015   By: gdc   Date:  22-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Added RM object references.
//
//  Revision: 014   By: syw   Date:  19-Jun-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//		PAV project-related changes:
//		Added RPavManager.
//
//  Revision: 013   By: syw   Date:  19-Jun-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//		VTPC project-related changes:
//		Added VolumeTargetedManager.
//
//  Revision: 012  By: yyy     Date:  5-May-1999    DR Number: 5322
//  Project:  ATC
//  Description:
//      Added TC required objects eg RLungData and RTube.
//
//  Revision: 011  By:  syw    Date:  14-Jan-1999    DR Number: none
//       Project:  ATC
//       Description:
//		 	ATC initial version.
//
//  Revision: 010  By:  iv    Date:  18-Jan-1998    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//           Bilevel initial version.
//           Added RInspiratoryPauseEvent.
//
//  Revision: 009  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 008  By:  iv    Date:  07-Feb-1997    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Rework per formal code review.
//
//  Revision: 007  By:  iv    Date:  24-Jan-1997    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Rework per formal code review.
//
//  Revision: 006 By:  iv    Date:  09-Oct-1996    DR Number: DCS 600 
//       Project:  Sigma (R8027)
//       Description:
//             Added UserEvent& rGuiFaultEvent.
//
//  Revision: 005 By:  iv    Date:  15-Apr-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Deleted UserEvent& rO2CalibrationRequest
//
//  Revision: 004 By:  iv    Date:  03-Mar-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Deleted UserEvent& rVentSetupRequest
//
//  Revision: 003 By:  kam   Date:  06-Nov-1995    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Updated for SST confirmation
//
//  Revision: 002 By:  kam   Date:  25-Sep-1995    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Updated for interface with Alarm-Analysis subsystem
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

// Breath Data
class BreathData;
extern BreathData& RBreathData;

//Breath Set
class BreathSet;
extern BreathSet& RBreathSet;

//Averaged Breath Data
class AveragedBreathData;
extern AveragedBreathData& RAveragedBreathData;

// Breath delivery system state
//class BdSystemState;
//extern BdSystemState& RBdSystemState;

// Lung data
class LungData;
extern LungData& RLungData;

// Pav manager
class PavManager;
extern PavManager& RPavManager;

// Volume targeted manager
class VolumeTargetedManager ;
extern VolumeTargetedManager& RVolumeTargetedManager ;

// Dynamic Mechanics data
class DynamicMechanics;
extern DynamicMechanics& RDynamicMechanics;

//Events
class UiEvent;
extern UiEvent& RExpiratoryPauseEvent;
extern UiEvent& RInspiratoryPauseEvent;
extern UiEvent& RNifManeuverEvent;
extern UiEvent& RP100ManeuverEvent;
extern UiEvent& RVitalCapacityManeuverEvent;
extern UiEvent& RManualInspEvent;
extern UiEvent& RAlarmResetEvent;
extern UiEvent& RO2Request;
extern UiEvent& RCalO2Request;
extern UiEvent& RServiceRequest;
extern UiEvent& RGuiFaultEvent;
extern UiEvent& RPurgeRequest;
extern UiEvent& RSstExitToOnlineEvent;
 
//BD Alarms
class BdAlarms;
extern BdAlarms& RBdAlarms;
 
//Apnea Interval
class ApneaInterval;
extern ApneaInterval& RApneaInterval;

//O2 Mixture
class O2Mixture;
extern O2Mixture& RO2Mixture;

//Service Mode Switch Confirmation
class SmSwitchConfirmation;
extern SmSwitchConfirmation& RSmSwitchConfirmation;

//Peep
class Peep;
extern Peep& RPeep;

//Circuit Compliance
class CircuitCompliance;
extern CircuitCompliance& RCircuitCompliance;

// Endotracheal/Tracheostomy Tube
class Tube;
extern Tube& RTube;

// Pressure Transducer Autozero
class PressureXducerAutozero;
extern PressureXducerAutozero& RPressureXducerAutozero;

// Waveform
class Waveform;
extern Waveform& RWaveform;

// AveragedBreathData
class AveragedBreathData;
extern AveragedBreathData& RAveragedBreathData;

// Leak Compensation manager
class LeakCompMgr;
extern LeakCompMgr& RLeakCompMgr;

class ProxManager;
extern ProxManager& RProxManager;

#endif // BreathMiscRefs_HH








