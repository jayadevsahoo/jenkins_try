#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: TrendDataSetAgent - Trending dataset agent
//---------------------------------------------------------------------
//@ Interface-Description 
// 
//  The TrendDataSetAgent class encapsulates all the methods and objects
//  required to request and receive a TrendDataSet from the
//  Trend-Database subsystem. This class is used by the TrendSubScreen
//  to retrieve a TrendDataSet based on current trend parameters, the
//  trend time scale and the trend cursor position settings. This class
//  is a SettingObserver that inherits the valueUpdate method and
//  registers to receive setting change callbacks for all trend related
//  settings. The TrendSubScreen need only change these settings and
//  signal this agent via its requestTrendData method to retrieve new
//  trend data from the trend database.
// 
//  This class contains a single TrendDataSet object that it uses to
//  pass the TrendRequestInfo request to and receive the trend data from
//  the Trend Data Manager. It creates this request based on the current
//  trend settings and seek frame positioning algorithm found in its
//  requestTrendData method. This algorithm implements the desired
//  zooming, scrolling and data update behavior for both the table and
//  graph views of the data. The seek frame positioning and resultant
//  cursor positioning algorithms are the same for both and provides
//  for consistent behavior when viewing either the graph or table.
// 
//  This class uses TrendDataMgr::RequestData method to request a new
//  TrendDataSet from the Trend-Database subsystem. It passes the
//  address of the TrendDataSet member to the Trend Data Manager using
//  this function that then fulfills the request and passes the
//  TrendDataSet back to this class.
// 
//  This class is a TrendEventTarget that inherits the trendDataReady
//  method to receive a callback from the TrendEventRegistrar when the
//  Trend Data Manager completes a database retrieval request. The
//  address of the TrendDataSet is passed in to this callback.
// 
//  Upon receiving a new TrendDataSet via trendDataReady, this class
//  will reposition the cursor to the nearest sample (in time) in the
//  new data to where it was positioned in the trend dataset in use when
//  the request was formulated. This way the cursor stays positioned
//  near the same point in time no matter what timescale is selected.
//  In the unique case when the cursor is positioned at the newest
//  collected sample time in the database, the cursor is repositioned to
//  the newest sample every time there is a new dataset. This change in
//  cursor position is communicated to all SettingObservers of the trend
//  cursor position setting.
// 
//  After repositioning the trend cursor and updating its internal data
//  members including the current "frame" for the new dataset, this
//  class will issues callbacks via the trendDataReady method to all
//  clients that registered with this class using the RegisterTarget
//  method. These clients are the TrendSubScreens.
// 
//  The TrendSubScreen target classes access the TrendDataSet contained
//  in this class using the getTrendDataSet method that returns a const
//  reference to the TrendDataSet contained in this class. The const
//  reference assures that this data is read-only to the clients.
// 
//  This class is a GuiTimerTarget that inherits the timerEventHappened
//  method to receive timer callbacks for periodically refreshing the
//  trend data every minute and for retrying the trend data retrieval if
//  the TrendSubScreen is busy at the time of the request or if the
//  Trend Data Manager is busy when a request is made. In either case,
//  the retry timer is started and the request retried every five
//  seconds until the request is accepted.
// 
//---------------------------------------------------------------------
//@ Rationale 
//  Encapsulates all trend data set handling in a single class for the
//  TrendSubScreens.
// 
//---------------------------------------------------------------------
//@ Implementation-Description 
// 
//  The most seemingly complicated implementation for the
//  TrendDataSetAgent class is the circuitous routing of the trend data
//  request itself. When this class processes the periodic timer to
//  refresh the trend data (every minute) it uses the
//  TrendSubScreen::RequestTrendData method to actually start the
//  request process that eventually ends up back here in this
//  TrendDataSetAgent's requestTrendData method. It does this so the
//  TrendSubScreen can disable the user interface while new data is
//  retrieved into the TrendDataSet. If the user interface wasn't
//  disabled during this request process, the data would be changing at
//  the same time the user is attempting to access it. This wouldn't be
//  good. This circuitous routing prevents the user from accessing the
//  data while the request is active. 
// 
//---------------------------------------------------------------------
//@ Fault-Handling
//  No special fault handling strategies apart from the usual assertions
//  and pre-conditions to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
//  There is a single instance of this class accessed using the static
//  GetTrendDataSetAgent() method.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/TrendDataSetAgent.ccv   25.0.4.0   19 Nov 2013 14:08:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By: gdc    Date:  05-Mar-2007   SCR Number: 6237
//  Project:  Trend
//  Description:
//  Initial Version
//=====================================================================

#include "TrendDataSetAgent.hh"

//@ Usage-Classes
#include "AdjustPanel.hh"
#include "BoundedValue.hh"
#include "BoundedSetting.hh"
#include "DiscreteValue.hh"
#include "TrendTimeScaleValue.hh"
#include "TrendSelectValue.hh"
#include "GuiTimerRegistrar.hh"
#include "TrendEventRegistrar.hh"
#include "SettingSubject.hh"
#include "SettingId.hh"
#include "TrendSubScreen.hh"

// TODO E600 removed
//#include "Ostream.hh"

//@ End-Usage

//@ Code...

static const Int32 SEEK_CURRENT_ = 0;
static const Int32 RESET_CURSOR_FRAME_ = 0;

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TrendDataSetAgent()  [Constructor]
//
//@ Interface-Description
//	Constructor.
//---------------------------------------------------------------------
//@ Implementation-Description 
//  Initialize member data. Register for settings callbacks and trend
//  events.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

TrendDataSetAgent::TrendDataSetAgent(void) 
:   trendDataRequest_(),
	trendDataSet_(),
	cursorFrame_(RESET_CURSOR_FRAME_),
	currentFrame_(0),
	isFirstRequest_(TRUE),
	isDataSetMostCurrent_(FALSE),
	numTargets_(0)
{
	// The trend cursor setting position defaults to zero on powerup so
	// setting zeroing cursorPosition_ is valid here
	cursorPosition_ = 0;

	// initialize the target array
	for (Int32 i=0; i<countof(targetArray_); i++)
	{
		targetArray_[i] = NULL;
	}

	// begin monitoring these setting changes...
	attachToSubject_(SettingId::TREND_CURSOR_POSITION, Notification::VALUE_CHANGED);
	attachToSubject_(SettingId::TREND_TIME_SCALE, Notification::VALUE_CHANGED);

	// Register this class to the trend event registrar
	TrendEventRegistrar::RegisterTarget(this);  

	GuiTimerRegistrar::RegisterTarget(GuiTimerId::TREND_DATA_SET_TIMER_TICK, this);
	GuiTimerRegistrar::StartTimer(GuiTimerId::TREND_DATA_SET_TIMER_TICK);

	GuiTimerRegistrar::RegisterTarget(GuiTimerId::TREND_DATA_SET_RETRY_TIMER, this);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~TrendDataSetAgent  [Destructor]
//
//@ Interface-Description
// Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  n/a
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

TrendDataSetAgent::~TrendDataSetAgent(void) 
{
	// do nothing
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetTrendDataSetAgent
//
//@ Interface-Description
//  Instantiates the singleton object and returns a reference to it.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Instantiates the singleton object the first time through this
//  method. Subsequent calls just return the reference.
//---------------------------------------------------------------------
//@ PreCondition
// 	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
TrendDataSetAgent& TrendDataSetAgent::GetTrendDataSetAgent(void)
{
	static TrendDataSetAgent TrendDataSetAgent_;

	return TrendDataSetAgent_;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: RegisterTarget
//
//@ Interface-Description
//  Register a TrendEventTarget with the TrendDataSetAgent singleton
//  object.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Calls registerTarget() of the singleton TrendDataSetAgent class.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void TrendDataSetAgent::RegisterTarget( TrendEventTarget* pTarget )
{
	GetTrendDataSetAgent().registerTarget_(pTarget);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: registerTarget [private]
//
//@ Interface-Description
//  Register a TrendEventTarget
//---------------------------------------------------------------------
//@ Implementation-Description
//  The target pointer is stored in targetArray_[].  Increment the count
//	of registered targets.
//---------------------------------------------------------------------
//@ PreCondition
//  The target pointer must be non-null and there must be space to store
//	it in the array.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void TrendDataSetAgent::registerTarget_( TrendEventTarget* pTarget )
{
	CLASS_PRE_CONDITION(pTarget != NULL);
	CLASS_PRE_CONDITION(numTargets_ < countof(targetArray_));

	// Store the pointer	
	targetArray_[numTargets_++] = pTarget;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: trendDataReady [virtual]
//
//@ Interface-Description
//  This TrendEventTarget virtual method is called from the
//  TrendEventRegistrar when the TrendDataMgr has fulfilled a request
//  for trending data and returned it in the referenced TrendDataSet.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Upon receiving a valid data set, positions the cursor in the new
//  dataset to the sample nearest in time to where the cursor was
//  positioned previously. When the cursor position setting is changed,
//  this results in a setting value changed callback notifiying all
//  clients of the change. After setting the cursor position, walks the
//  targetArray_ to notify all registered clients that the dataset
//  itself has changed.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void TrendDataSetAgent::trendDataReady(TrendDataSet& rTrendDataSet )
{
	// is this the data we requested?
	if (!isDataRequestPending_ || (&rTrendDataSet != &trendDataSet_))
	{
		// spurious data - it's not the dataset we requested -- ignore it
		// this branch allows for more than one dataset requestor 
	}
	else if (trendDataSet_.getStatus() == TrendDataSet::PROCESS_ERROR)
	{
		// a compact flash error gets us here --- we don't really need to do
		// anything since a background fault will be logged in the system
		// information log, the trend sub-screen deactivated and the trend
		// sub-screen select buttons flattened. The trending parameter buttons
		// are also flattened when the request was made so they'll remain flat
		// indefinitely waiting on a database response. So... do nothing.
#ifdef TRENDDATASETAGENT_DEBUG
		cout << "Trend Data Set Error!\n";
#endif
	}
	else
	{
		// verify we got the data that we requested
		Boolean isDataCurrent = (trendDataSet_.getTimescale() == requestedTimeScale_);

		for (Uint column=0; (column < numColumnsRequested_) && isDataCurrent; column++)
		{
			isDataCurrent = (trendDataSet_.getTrendDataValues(column).getId() == trendSelectValue_[column]);
		}

		if (!isDataCurrent)
		{
#ifdef TRENDDATASETAGENT_DEBUG
			cout << "Dataset out of sync - request new data\n";
#endif
			// request updated data to match current parameter set
			isDataRequestPending_ = FALSE;
			GuiTimerRegistrar::StartTimer(GuiTimerId::TREND_DATA_SET_RETRY_TIMER);
		}
		else
		{
			// we received a valid dataset
			isDataRequestPending_ = FALSE;

			currentFrame_ = trendDataSet_.getSequenceNumber(0);

			// Peg the cursor to the most recent data in the new table (RHS on the
			// graph) if the cursor was positioned at the most recent data sample
			// in the previous dataset
			if (cursorFrame_ == RESET_CURSOR_FRAME_)
			{
				// reset the cursorFrame_ to the newest frame number
				// (i.e. sequence number) in the new dataset
				cursorFrame_ = currentFrame_;
			}

#ifdef TRENDDATASETAGENT_DEBUG
			cout << "returned start = " << currentFrame_ << endl;
#endif

			// update the trend cursor setting and the cursor button location
			Int32 framesPerSample = TrendDataMgr::GetTableFrequency(trendDataSet_.getTimescale());

			isDataSetMostCurrent_ = rTrendDataSet.isMostCurrent();

			// reposition cursor based on the time scale of the new dataset
			BoundedSetting * pSetting = SettingsMgr::GetBoundedSettingPtr(SettingId::TREND_CURSOR_POSITION);
			BoundedValue trendCursorPosition;
			trendCursorPosition.value = -Real32(currentFrame_ - cursorFrame_) / framesPerSample;

			// Warp to nearest value down from the computed new cursor position and set it.
			// Warp down so the cursor position moves backward in time.
			pSetting->warpToRange(trendCursorPosition, BoundedRange::WARP_DOWN);
			pSetting->setAcceptedValue(trendCursorPosition);

			// cursor will update when cursor position setting change occurs so no
			// need to do it here

			// Walk the 'targetArray_[]' array and call the trendDataReady()
			// method on each target.
			for (Uint32 i = 0; i < numTargets_; i++)
			{
				targetArray_[i]->trendDataReady(rTrendDataSet);
			}                                                   
		}
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: timerEventHappened
//
//@ Interface-Description 
//  Handles the one-minute update timer as well as the data request
//  retry timer. Initiates a trend data request if conditions allow.
//---------------------------------------------------------------------
//@ Implementation-Description 
//  Upon receiving a valid timer, this method attempts to initiate a
//  trend dataset request. Before issuing the request, it checks to
//  assure that the AdjustPanel is not focused on a feedback sensitive
//  target. These targets include settings buttons and scrollbars where
//  a delay would be too disturbing to the user if the screen is
//  repainted while the user is changing them. If the focus is OK then
//  it forwards the dataset request through the active TrendSubScreen to
//  allow it to inactivate its user interface while the dataset request
//  is pending so the user is not manipulating data as it's changing.
//  This single-threaded process could be improved by double buffering
//  the dataset requests with one buffer used for the active dataset
//  display and another to fulfill the next request and then swapping
//  them when the request completes.
//---------------------------------------------------------------------
//@ PreCondition 
//	none 
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void TrendDataSetAgent::timerEventHappened(GuiTimerId::GuiTimerIdType timerId)
{

	switch (timerId)
	{
	case GuiTimerId::TREND_DATA_SET_TIMER_TICK:           
	case GuiTimerId::TREND_DATA_SET_RETRY_TIMER:           
		{
			// Have the trend sub-screen make the trend data request. It will
			// make the request through the dataset agent if there's no menu
			// present and nothing is accessing the current dataset. 
			// Forwarding this request through the TrendSubScreen allows it to
			// inactivate its buttons to lock out user access to the dataset
			// while the data request is pending.

			// no timed updates when focus is on a user settable object
			if (!AdjustPanel::IsFocusOnSetting())
			{
				TrendSubScreen::Status status = TrendSubScreen::RequestTrendData();

				if (status == TrendSubScreen::NOT_ACTIVE)
				{
					// no active trend sub-screen, cancel retry timer
					GuiTimerRegistrar::CancelTimer(GuiTimerId::TREND_DATA_SET_RETRY_TIMER);
				}
				else if (status == TrendSubScreen::BUSY)
				{
					// the screen is busy so start the retry timer
					GuiTimerRegistrar::StartTimer(GuiTimerId::TREND_DATA_SET_RETRY_TIMER);
				}
				else
				{
					CLASS_ASSERTION(status == TrendSubScreen::REQUEST_ACCEPTED);
				}
			}
			else
			{
				// let's try again later
				GuiTimerRegistrar::StartTimer(GuiTimerId::TREND_DATA_SET_RETRY_TIMER);
			}
		}
		break;

	default:
		AUX_CLASS_ASSERTION_FAILURE(timerId);
		break;
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setTrendSelectValue_
//
//@ Interface-Description 
//  This method sets up the trendDataRequest_ for the specified column
//  with the specified setting. This method is called once for each
//  column requested in requestTrendData().
//---------------------------------------------------------------------
//@ Implementation-Description 
//  Gets the accepted value of the trend setting parameter specified by
//  the setting ID. If the selected trend parameter is not available
//  with the current option set, the requested trend selection is
//  changed to "data not available" so the data receiver knows to
//  display the appropriate "data not available" message to the user.
//---------------------------------------------------------------------
//@ PreCondition
//  The specified column must be in range of the arrays referenced
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 

void TrendDataSetAgent::setTrendSelectValue_(Int32 column,
											 SettingId::SettingIdType settingId)
{
	AUX_CLASS_PRE_CONDITION(column < countof(trendSelectValue_), column);

	const SettingSubject* pTrendSelectSetting = getSubjectPtr_(settingId);

	trendSelectValue_[column] = 
		TrendSelectValue::TrendSelectValueId(DiscreteValue(pTrendSelectSetting->getAcceptedValue()));

	if (!pTrendSelectSetting->isEnabledValue(trendSelectValue_[column]))
	{
		trendSelectValue_[column] = TrendSelectValue::TREND_DATA_NOT_AVAILABLE;
	}

	trendDataRequest_.setColumnId( column, trendSelectValue_[column] );
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: requestTrendData
//
//@ Interface-Description
//  Requests new trend data from the Trend Data Manager. This method
//  builds a trend data request using the current trend parameter
//  settings, timescale setting and cursor position and passes this
//  request in a TrendDataSet to the the Trend Data Manager (TDM). The
//  TDM handles the database retrieval, supplies the requested data in
//  the dataset and passes it back to GuiApp and back to trendDataReady
//  callback via the TrendEventRegistrar.
// 
//---------------------------------------------------------------------
//@ Implementation-Description
//  Retrieves the current trend button setting values, populates the
//  trend data set parameters in the trendDataRequest which is then
//  loaded into the TrendDataSet and sent to the Trend Data Manager to
//  supply the data.
// 
//  The most complicated implementation detail is in how the "seek
//  frame" is calculated. The "seek frame" is the frame number in the
//  database where we want to retrieve backwards (in time) from. The
//  source code provides the best documentation of this implementation.
// 
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void TrendDataSetAgent::requestTrendData()
{
	if (isDataRequestPending_)
	{
		return;
	}

	// restart the data request timer to update one minute from this request
	GuiTimerRegistrar::StartTimer(GuiTimerId::TREND_DATA_SET_TIMER_TICK);

	// cancel the retry timer to avoid redundant updates
	GuiTimerRegistrar::CancelTimer(GuiTimerId::TREND_DATA_SET_RETRY_TIMER);

	// Initialize "previous" values if this is the first time through here.
	// Normally this kind of initialization is done at construction, but we
	// do it here because we need the TrendDataMgr running to initialize.
	if (isFirstRequest_)
	{
		previousEndFrame_ = TrendDataMgr::GetEndOfDataPosition();

		const SettingSubject* pTrendTimeScale = getSubjectPtr_(SettingId::TREND_TIME_SCALE);
		previousTimeScale_ =
			TrendTimeScaleValue::TrendTimeScaleValueId(DiscreteValue(pTrendTimeScale->getAcceptedValue()));

		isFirstRequest_ = FALSE;
	}

	// set up the dataset request
	trendDataRequest_.clear();

	numColumnsRequested_ = 0;
	setTrendSelectValue_(numColumnsRequested_++, SettingId::TREND_SELECT_1);
	setTrendSelectValue_(numColumnsRequested_++, SettingId::TREND_SELECT_2);
	setTrendSelectValue_(numColumnsRequested_++, SettingId::TREND_SELECT_3);
	setTrendSelectValue_(numColumnsRequested_++, SettingId::TREND_SELECT_4);
	setTrendSelectValue_(numColumnsRequested_++, SettingId::TREND_SELECT_5);
	setTrendSelectValue_(numColumnsRequested_++, SettingId::TREND_SELECT_6);
	CLASS_ASSERTION(numColumnsRequested_ <= TrendDataMgr::MAX_COLUMNS);

	const SettingSubject* pTrendTimeScale = getSubjectPtr_(SettingId::TREND_TIME_SCALE);
	requestedTimeScale_ = 
		TrendTimeScaleValue::TrendTimeScaleValueId(DiscreteValue(pTrendTimeScale->getAcceptedValue()));

	trendDataRequest_.setTimescale(requestedTimeScale_);

	Int32 seekFrame = 0;
	Int32 framesPerSample = TrendDataMgr::GetTableFrequency(requestedTimeScale_);

	Int32 currentEndFrame = TrendDataMgr::GetEndOfDataPosition();

#ifdef TRENDDATASETAGENT_DEBUG
	cout << "=== forming request" << endl;
	cout << "previousEndFrame_ = " << previousEndFrame_ << endl;
	cout << "currentEndFrame = " << currentEndFrame << endl;
	cout << "currentFrame_ = " << currentFrame_ << endl;
	cout << "previousTimeScale_ = " << previousTimeScale_ << endl;
	cout << "requestedTimeScale_ = " << requestedTimeScale_ << endl;
	cout << "cursorPosition_ = " << cursorPosition_ << endl;
	cout << "cursorFrame_ = " << cursorFrame_ << endl;
	cout << "isDataSetMostCurrent_ = " << isDataSetMostCurrent_ << endl;
	cout << "===" << endl;
#endif

	// calculate the seek frame ----

	if (requestedTimeScale_ == TrendTimeScaleValue::TREND_TIME_SCALE_72HR)
	{
		// zooming out to 72 hours - request most recent data
		seekFrame = SEEK_CURRENT_;
	}
	else if (currentFrame_ == 0)
	{
		// currentFrame_ is uninitialized - request most recent data
		seekFrame = SEEK_CURRENT_;
		// force cursorFrame_ to be reset to most recent sequence number
		// when trend data received
		cursorFrame_ = RESET_CURSOR_FRAME_;
	}
	else if (previousTimeScale_ != requestedTimeScale_)
	{
		// changing timescales --
		// seek to the frame that will center the cursor when trendDataReady()
		// receives the data and repositions the cursor to "cursor frame"
		seekFrame = cursorFrame_ + (TrendDataMgr::MAX_ROWS / 2) * framesPerSample;
	}
	else if (isDataSetMostCurrent_)
	{
		// Note: old method: && currentFrame_ + 100 * framesPerSample > currentEndFrame)
		// if the cursor is positioned at the current time in the most recent
		// data window then it shall remain positioned at the current time
		seekFrame = SEEK_CURRENT_;
	}
	else
	{
		// this handles the case when the "window" or dataset is not the
		// most recent one. We only seek from the current location plus the
		// number of records added since the last update.
		seekFrame = currentFrame_ + (currentEndFrame - previousEndFrame_);
	}

	if (cursorPosition_ == 0 && isDataSetMostCurrent_)
	{
		// tell trendDataReady_() to reposition the cursor to newest data
        // frame upon receiving data back from the Trend Data Manager
		cursorFrame_ = RESET_CURSOR_FRAME_;
	}

#ifdef TRENDDATASETAGENT_DEBUG
	cout << "requested start = " << seekFrame << endl;
#endif

	trendDataRequest_.setStartFrameNumber( seekFrame );

	// Load the request into the trendDataSet
	trendDataSet_.clear();
	trendDataSet_.setRequestInfo( trendDataRequest_ );

	// make the request
	TrendDataMgr::RequestStatus status = TrendDataMgr::RequestData(trendDataSet_);


	if (status == TrendDataMgr::REQUEST_ACCEPTED)
	{
		isDataRequestPending_ = TRUE;
		previousEndFrame_ = currentEndFrame;
		previousTimeScale_ = requestedTimeScale_;
	}
	else if (status == TrendDataMgr::MANAGER_BUSY)
	{
		// retry request later
		isDataRequestPending_ = FALSE;
		GuiTimerRegistrar::StartTimer(GuiTimerId::TREND_DATA_SET_RETRY_TIMER);
	}
	else if (status == TrendDataMgr::MANAGER_NOT_ENABLED)
	{
		// retry request later
		isDataRequestPending_ = FALSE;
		GuiTimerRegistrar::StartTimer(GuiTimerId::TREND_DATA_SET_RETRY_TIMER);
	}
	else
	{
		AUX_CLASS_ASSERTION_FAILURE(status);
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: valueUpdate
//
//@ Interface-Description
//  Called when a trend parameter setting is changed
//
//  qualifierId		The context in which the change happened, either
//                  ACCEPTED or ADJUSTED.
// 
//  pSubject		The pointer to the Setting's Context subject.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Update the "cursor frame" to the new cursor position by calling
//  updateCursorFrame_().
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void TrendDataSetAgent::valueUpdate(const Notification::ChangeQualifier qualifierId,
									const SettingSubject* pSubject)
{
	if (qualifierId == Notification::ACCEPTED)
	{
		const SettingId::SettingIdType  SETTING_ID = pSubject->getId();

		if (SETTING_ID == SettingId::TREND_CURSOR_POSITION)
		{
			updateCursorFrame_();
		}
		else if (SETTING_ID == SettingId::TREND_TIME_SCALE)
		{
			// TrendSubScreen requests a dataset update after panel focus is
			// released so we do nothing here but observe the time scale change
		}
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateCursorFrame_
//
//@ Interface-Description 
//  Update the "cursor frame" or the data frame (10 second sample
//  number) that the cursor is pointing to. This method is called
//  whenever the trend cursor position setting has changed and the
//  "cursor frame" needs to be synchronized.
//---------------------------------------------------------------------
//@ Implementation-Description 
//
//  The frame number that the cursor points to is calculated according
//  to following formula:
// 
//  cursorFrame_ = currentFrame_ - cursorPosition_ * framesPerSample
// 
//  where:
// 
//  currentFrame_ 	is the frame number of the newest sample in the 
//                  current trend dataset. This is the frame number
//                  of the sample at the right-hand side of the
//                  graph or top of the trend table. If the cursor is
//                  positioned at this sample then the cursorFrame_
//                  will equal the currentFrame_.
// 
//  cursorPosition_	is the displacement of the cursor from the newest
//                  sample or frame in the dataset.
// 
//  framesPerSample	is the number of frames per sample for the timescale
//                  if the trend dataset. There is 1 frame/sample for
//                  data collected at the 1 hour time scale, 2
//                  frames/sample for 2 hour data, 4 frames/sample for 4
//                  hour data, etc...
// 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void TrendDataSetAgent::updateCursorFrame_(void)
{
	// update the frame number that the cursor points to

	BoundedValue trendCursorPosition = 
		getSubjectPtr_(SettingId::TREND_CURSOR_POSITION)->getAcceptedValue();

	// cursorPosition_ is the displacement from the right hand side of the graph
	cursorPosition_ = -trendCursorPosition.value;

	Int32 framesPerSample = TrendDataMgr::GetTableFrequency(trendDataSet_.getTimescale());

	cursorFrame_ = currentFrame_ - cursorPosition_ * framesPerSample;

#ifdef TRENDDATASETAGENT_DEBUG
	cout << "TrendDataSetAgent::updateCursorFrame:cursorFrame_ = " << cursorFrame_ << endl;
#endif
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//        [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition   
//  none
//@ End-Method
//=====================================================================

void TrendDataSetAgent::SoftFault(const SoftFaultID  softFaultID,
								  const Uint32       lineNumber,
								  const char*        pFileName,
								  const char*        pPredicate)  
{
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, TRENDDATASETAGENT,
							lineNumber, pFileName, pPredicate);
}

