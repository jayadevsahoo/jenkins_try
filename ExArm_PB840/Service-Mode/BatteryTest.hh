#ifndef BatteryTest_HH
#define BatteryTest_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: BatteryTest -- Perform system battery test
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/BatteryTest.hhv   25.0.4.0   19 Nov 2013 14:23:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 001  By:  mad    Date:  15-Mar-1996    DR Number:
//	Project:  Sigma (R8027)
//	Description:
//		Initial version (Integration baseline).
//
//====================================================================

#include "ServiceMode.hh"

//@ Usage-Classes
//@ End-Usage

extern const Real32 MIN_INIT_VOLTS_DISCHARGING;
extern const Real32 MIN_FINAL_VOLTS_DISCHARGING;
extern const Real32 MAX_DELTA_VOLTAGE;

class BatteryTest
{
  public:

    BatteryTest(void) ;
    ~BatteryTest(void) ;

    static void SoftFault(const SoftFaultID	softFaultID,
						  const Uint32		lineNumber,
						  const char			*pFileName  = NULL, 
						  const char			*pPredicate = NULL) ;

	void runTest(void) ;

  protected:

  private:
    BatteryTest(const BatteryTest&) ;		// not implemented...
    void operator=(const BatteryTest&) ;	// not implemented...

	Boolean checkDischarging_(void);

} ;


#endif // BatteryTest_HH 
