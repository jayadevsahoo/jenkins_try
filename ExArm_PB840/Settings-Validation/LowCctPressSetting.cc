#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2005, Puritan-Bennett Corporation
//=====================================================================

//================= C L A S S   D E S C R I P T I O N =================
//@ Class:  LowCctPressSetting - High Circuit Pressure Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, bounded setting that contains the lower limit of
//  circuit pressure (in cmH2O), above which the "low circuit pressure
//  alarm" shall be activated (this low pressure is not offset by PEEP).
//  This class inherits from 'BoundedSetting' and provides the specific
//  information needed for representation of low circuit pressure.  This
//  information includes the interval and range of this setting's values,
//  and this batch setting's new-patient value (see 'getNewPatientValue()').
//
//  This setting is an alarm limit setting, therefore this class overrides
//  the 'getMaxLimit()' and 'getMinLimit()' methods to provide access (for
//  the GUI) to this alarm's maximum and minimum limits.
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class has NO dependent settings, but 'updateConstraints_()' is
//  overridden to update this setting's dynamic bounds.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/LowCctPressSetting.ccv   25.0.4.0   19 Nov 2013 14:27:28   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: gdc    Date: 02-Mar-2009    SCR Number: 6160
//  Project:  S840
//  Description:
//      Enhanced to set this alarm limit OFF when set less than PEEP+5
//      in VC+ per modified SRS requirements.
//
//  Revision: 001  By: gdc    Date: 20-Feb-2005    SCR Number: 6144
//  Project:  NIV1
//  Description:
//      Initial version.
//=====================================================================

#include "LowCctPressSetting.hh"
#include "SettingConstants.hh"

//@ Usage-Classes
#include "SettingsMgr.hh"
#include "PatientCctTypeValue.hh"
#include "MandTypeValue.hh"
#include "ModeValue.hh"
#include "SupportTypeValue.hh"
#include "VentTypeValue.hh"
//@ End-Usage


//@ Code...

//====================================================================
//
//  Static Data Definitions...
//
//====================================================================

#define DEFAULT_MIN_VALUE  Real32(0.5f)
#define LOW_RANGE_MAX_VALUE Real32(20.0f)

//  $[NI02001] -- The setting's range ...
//  $[NI02003] -- The setting's resolution ...
static const BoundedInterval  LAST_NODE_ =
{
	DEFINED_LOWER_ALARM_LIMIT_OFF,// absolute minimum...
	0.0f,		// unused...
	ONES,		// unused...
	NULL
};
static BoundedInterval  NODE2_ =
{
	DEFAULT_MIN_VALUE,	// change-point... if VC+ and (PEEP+5 < 20) then change this switch point to (PEEP+5)
	(DEFAULT_MIN_VALUE - DEFINED_LOWER_ALARM_LIMIT_OFF),// resolution...  if VC+ and (PEEP+5 < 20) then change this resolution to (PEEP+5 - DEFINED_LOWER_ALARM_LIMIT_OFF)
	TENTHS,		// precision...
	&::LAST_NODE_
};
static BoundedInterval  NODE1_ =
{
	LOW_RANGE_MAX_VALUE,		// change-point... if VC+ and (PEEP+5 >= 20) then change the switch point to (PEEP+5)
	0.5f,		// resolution...  if VC+ and (PEEP+5 >= 20) then change this resolution to (PEEP+5 - DEFINED_LOWER_ALARM_LIMIT_OFF)
	TENTHS,		// precision...
	&::NODE2_
};
static const BoundedInterval  INTERVAL_LIST_ =
{
	100.0f,		// absolute maximum...
	1.0f,		// resolution...
	ONES,		// precision...
	&::NODE1_
};


//====================================================================
//
//  Public Methods...
//
//====================================================================

//============= M E T H O D   D E S C R I P T I O N ==================
//@ Method: LowCctPressSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This setting inherits from BoundedSetting.  The method passes to
//  BoundedSetting it's default information.  Also, this method initializes
//  the value interval range.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

LowCctPressSetting::LowCctPressSetting(void)
: BatchBoundedSetting(SettingId::LOW_CCT_PRESS,
					  Setting::NULL_DEPENDENT_ARRAY_,
					  &::INTERVAL_LIST_,
					  NULL_SETTING_BOUND_ID,  // maxConstraintId...
					  LOW_CIRC_MIN_ID)		  // minConstraintId...
{
	CALL_TRACE("LowCctPressSetting()");
}	// $[TI1]


//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  ~LowCctPressSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

LowCctPressSetting::~LowCctPressSetting(void)
{
	CALL_TRACE("~LowCctPressSetting()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This setting is applicable only when NIV tube type is selected.
//
//  $[NI02005] Low Ppeak present only for NIV and VC+
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id LowCctPressSetting::getApplicability(	const Notification::ChangeQualifier qualifier) const
{
	CALL_TRACE("getApplicability(qualifierId)");

	Applicability::Id applicability = Applicability::INAPPLICABLE;

	const Setting*  pVentType = SettingsMgr::GetSettingPtr(SettingId::VENT_TYPE);
	const Setting*  pMandType = SettingsMgr::GetSettingPtr(SettingId::MAND_TYPE);

	DiscreteValue ventType;
	DiscreteValue mandType;

	switch ( qualifier )
	{
		case Notification::ACCEPTED :						   // $[TI1.1]
			ventType = pVentType->getAcceptedValue();
			mandType = pMandType->getAcceptedValue();
			break;
		case Notification::ADJUSTED :						   // $[TI1.2]
			ventType = pVentType->getAdjustedValue();
			mandType = pMandType->getAdjustedValue();
			break;
		default :
			AUX_CLASS_ASSERTION_FAILURE(qualifier);
			break;
	}

	if ( (ventType == VentTypeValue::NIV_VENT_TYPE)
		 || (mandType == MandTypeValue::VCP_MAND_TYPE) )
	{													   // $[TI2.1]
		applicability = Applicability::CHANGEABLE;
	}													   // $[TI2.1]

	return(applicability);

}	// $[TI1]


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [const, virtual]
//
//@ Interface-Description
//  Return the setting's new patient value. 
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[NI02002] -- new-patient value...
//  Must be called after the new patient initialization of vent-type 
//  and mandatory-type since it uses those values to determine if the 
//  limit is applicable or not. The value of PEEP is used directly 
//  since its new patient initialization occurs before this call.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue LowCctPressSetting::getNewPatientValue(void) const
{
	BoundedValue  lowCctPress;

	if ( getApplicability(Notification::ADJUSTED) != Applicability::INAPPLICABLE )
	{
		const Setting*  pPeep = SettingsMgr::GetSettingPtr(SettingId::PEEP);
		const BoundedValue PEEP_VALUE = pPeep->getAdjustedValue();

		// The following logic returns a new patient value one click value
		// above the value PEEP+5 value that turns the setting OFF when in VC+
		if ( PEEP_VALUE < LOW_RANGE_MAX_VALUE - SettingConstants::LOW_PRESS_PEEP_DELTA )
		{
			lowCctPress.value = PEEP_VALUE + SettingConstants::LOW_PRESS_PEEP_DELTA + 0.5;
		}
		else
		{
			lowCctPress.value = PEEP_VALUE + SettingConstants::LOW_PRESS_PEEP_DELTA + 1.0;
		}
		// warp the calculated value to upper "click" boundary with clipping
		// and set precision
		getBoundedRange().warpValue(lowCctPress, BoundedRange::WARP_UP);
	}
	else
	{													   // $[I1.2]
		// set OFF if not applicable
		lowCctPress.value     = DEFINED_LOWER_ALARM_LIMIT_OFF;
		lowCctPress.precision = ONES;
	}

	return(lowCctPress);
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method: acceptPrimaryChange(primaryId)
//
//@ Interface-Description
//  One of this setting's primary settings have changed, therefore update
//  this setting's value based on the new value of the primary setting
//  -- indicated by 'primaryId'.  If this setting's bound is violated by
//  the primary setting's newly "adjusted" value, a pointer to this
//  setting's bound status is returned, and this setting's value is "clipped"
//  to that bound.  Otherwise, a value of 'NULL' is returned to indicate no
//  dependent bound was violated.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  primaryId == SettingId::PEEP
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

const BoundStatus*
LowCctPressSetting::acceptPrimaryChange(const SettingId::SettingIdType primaryId)
{
	AUX_CLASS_PRE_CONDITION(primaryId == SettingId::PEEP, primaryId);

	if (BoundedValue(getAdjustedValue()).value != DEFINED_LOWER_ALARM_LIMIT_OFF)
	{
		updateConstraints_();
		setAdjustedValue(getNewPatientValue());
	}

	// never in violation since constraints and new adjusted value adapt to new 
	// PEEP value every time or it's OFF
	return NULL;
}


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method: acceptTransition(settingId, newValue, currValue) [virtual]
//
//@ Interface-Description
//  This method is responsible for updating this setting based on the
//  the Main Control Setting identified by 'settingId' and the transition
//  of its value from 'currValue' to 'newValue'.
//
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[NI02010]\e\ -- transitioning from Invasive to NIV
//  $[NI02011]\a\ -- transitioning from NIV to Invasive
//  $[VC02006]   -- transitioning from PC/VC to VC+
//  $[NI02005]   -- active in NIV and VC+ only
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================

void LowCctPressSetting::acceptTransition( const SettingId::SettingIdType settingId,
										   const DiscreteValue            toValue,
										   const DiscreteValue            fromValue)
{
	CALL_TRACE("acceptTransition(settingId, newValue, currValue)");

	Boolean isAdjustmentRequired = FALSE;

	switch ( settingId )
	{
		case SettingId::VENT_TYPE :						  // $[TI1.1]
			isAdjustmentRequired = TRUE;
			break;

		case SettingId::MAND_TYPE :						  // $[TI1.2]
			switch ( fromValue )
			{
				case MandTypeValue::PCV_MAND_TYPE :
				case MandTypeValue::VCV_MAND_TYPE :				// $[TI2.1]
					if ( toValue == MandTypeValue::VCP_MAND_TYPE )
					{											  // $[TI3.1]
						isAdjustmentRequired	 = TRUE;
					}											  // $[TI3.2]
					break;
				case MandTypeValue::VCP_MAND_TYPE :				// $[TI2.1]
					isAdjustmentRequired = TRUE;
					break;

				default:
					AUX_CLASS_ASSERTION_FAILURE(fromValue);
					break;
			}
			break;

		default: 
			AUX_CLASS_ASSERTION_FAILURE(settingId);
			break;
	}

	if ( isAdjustmentRequired ) 
	{
		updateConstraints_();
		setAdjustedValue(getNewPatientValue());
	}

}

//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//	      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void LowCctPressSetting::SoftFault(const SoftFaultID  softFaultID,
								   const Uint32       lineNumber,
								   const char*        pFileName,
								   const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

	FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
							LOW_CIRCUIT_PRESS_SETTING, lineNumber, pFileName,
							pPredicate);
}


//====================================================================
//
//  Protected Methods...
//
//====================================================================


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  updateConstraints_()  [virtual]
//
//@ Interface-Description
//  Update the dynamic bounds of this setting.  This method sets this
//  setting's maximum constraint to the most restrictive of this setting's
//  upper bounds, and, correspondingly, set this setting's minimum
//  constraint to its most restrictive lower bound.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[NI02001] -- The setting's range ...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void LowCctPressSetting::updateConstraints_(void)
{
	CALL_TRACE("updateConstraints_()");

	//-------------------------------------------------------------------
	// Determine minimum constraint...
	//-------------------------------------------------------------------
	{
		BoundedRange::ConstraintInfo  minConstraintInfo;

		// set up the default bounded intervals
		NODE2_.value = DEFAULT_MIN_VALUE;
		NODE2_.resolution = (DEFAULT_MIN_VALUE - DEFINED_LOWER_ALARM_LIMIT_OFF);
		NODE2_.pNext = &LAST_NODE_;
		NODE1_.value = LOW_RANGE_MAX_VALUE;
		NODE1_.resolution = 0.5;
		NODE1_.pNext = &NODE2_;

		const Setting*  pMandType = SettingsMgr::GetSettingPtr(SettingId::MAND_TYPE);

		if ( DiscreteValue(pMandType->getAdjustedValue()) == MandTypeValue::VCP_MAND_TYPE )
		{
			//-------------------------------------------------------------------
			// update intervals based on PEEP in VC+
			//-------------------------------------------------------------------
			const Setting*  pPeep = SettingsMgr::GetSettingPtr(SettingId::PEEP);
			BoundedValue peepBasedMin = pPeep->getAdjustedValue();

			// for low Ppeak settings <= (PEEP+5+one-click) the setting goes to OFF
			peepBasedMin.value += 5.0;

			BoundStatus dummyStatus(getId());
			// warp the minimum calculated value to upper "click" boundary with no clipping
			getBoundedRange().warpValue(peepBasedMin, BoundedRange::WARP_UP, FALSE);
			getBoundedRange().increment(1, peepBasedMin, dummyStatus);

			if ( peepBasedMin.value < LOW_RANGE_MAX_VALUE )
			{
				NODE2_.value = peepBasedMin;
				NODE2_.resolution = (peepBasedMin - DEFINED_LOWER_ALARM_LIMIT_OFF);
			}
			else
			{
				NODE1_.value = peepBasedMin;
				NODE1_.resolution = (peepBasedMin - DEFINED_LOWER_ALARM_LIMIT_OFF);
				NODE1_.pNext = &LAST_NODE_;
			}
		}

		minConstraintInfo.id    = LOW_CIRC_MIN_ID;
		minConstraintInfo.value = getAbsoluteMinValue_();  // default low bound to OFF
		minConstraintInfo.isSoft = FALSE;

		getBoundedRange_().updateMinConstraint(minConstraintInfo);
	}


	//-------------------------------------------------------------------
	// Determine maximum constraint...
	//-------------------------------------------------------------------
	{
		const Real32  ABSOLUTE_MAX = getAbsoluteMaxValue_();

		BoundedRange::ConstraintInfo  maxConstraintInfo;

		const BoundedSetting*  P_HIGH_CIRC_SETTING =
			SettingsMgr::GetBoundedSettingPtr(SettingId::HIGH_CCT_PRESS);

		// get the current value of high circuit pressure setting; Low circ setting
		// must always be lower than the current "high circuit pressure" setting
		BoundedValue  highCctPressBasedMax = P_HIGH_CIRC_SETTING->getAdjustedValue();

		// NOTE:  a '<=' comparison is used instead of a '<' comparison because
		//        of the decrement done to the value within the if clause, which
		//        equates an '<=' into an '<'...
		if ( highCctPressBasedMax.value <= ABSOLUTE_MAX )
		{														// $[TI3.1]
			BoundStatus  dummyStatus(getId());

			// reset this range's maximum constraint to allow for "warping" of a
			// new maximum value...
			getBoundedRange_().resetMaxConstraint();

			// warp the calculated value to a "click" boundary...
			getBoundedRange().warpValue(highCctPressBasedMax);

			// calculate the value one "click" below this current high circuit pressure
			// alarm value, by using low circuit pressure bounded range instance...
			getBoundedRange().decrement(1, highCctPressBasedMax, dummyStatus);

			// the high circuit pressure maximum is more restrictive,
			// therefore save its value and id as the maximum bound value and id...
			maxConstraintInfo.id    = LOW_CIRC_MAX_BASED_HIGH_ID;
			maxConstraintInfo.value = highCctPressBasedMax.value;
		}
		else
		{														// $[TI3.2]
			// the absolute maximum is more restrictive, therefore save its value
			// and id as the maximum bound value and id...
			maxConstraintInfo.id    = LOW_CIRC_MAX_ID;
			maxConstraintInfo.value = ABSOLUTE_MAX;
		}

		// this setting has no maximum soft bounds...
		maxConstraintInfo.isSoft = FALSE;

		getBoundedRange_().updateMaxConstraint(maxConstraintInfo);
	}
}
