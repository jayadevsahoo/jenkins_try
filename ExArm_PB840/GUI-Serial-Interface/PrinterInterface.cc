#include "stdafx.h"
//====================================================================
// This is a proprietary work to which Nellcor Puritan Bennett
// corporation of California claims exclusive right.  No part of this
// work may be used, disclosed, reproduced, sorted in an information
// retrieval system, or transmitted by any means, electronic,
// mechanical, photocopying, recording, or otherwise without the prior
// written permission of Nellcor Puritan Bennett Corporation.
//
//     Copyright (c) 1997, Nellcor Puritan Bennett Corporation
//=====================================================================

// =========================== M O D U L E   D E S C R I P T I O N ====
//@ Class: PrinterInterface - HP-PCL Printer Driver
//---------------------------------------------------------------------
//@ Interface-Description
//  Provides the HP-PCL printer driver for the 840 ventilator. This class 
//  contains the methods that output the HP-PCL sequence commands representing 
//  patient data, waveforms and settings graphical data to a printer through
//  the serial port.
//---------------------------------------------------------------------
//@ Rationale
//  Provides 840 to printer processing.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The SerialInterface::Task instatiates this class when the ventilator
//  transitions to a normal (ie. non-SERVICE) state and a communcication
//  port setting is set to PRINTER.  When the PRINT button is pressed, this
//  class captures the patient data, waveforms and settings graphical data
//  from the VGA framebuffer and converts it into HP-PCL command sequences.
//  The HP-PCL command sequences are then sent to the printer via the zmodem
//  protocol. 
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness of 
//  parameterized data.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Serial-Interface/vcssrc/PrinterInterface.ccv   25.0.4.0   19 Nov 2013 14:12:44   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006    By: gdc        Date: 13-Jan-2009  SCR Number: 5944
//  Project: 840S
//  Description:
//  	Modified to allow changes to service mode baud rate without
//  	requiring power cycle.
//
//  Revision: 005    By: quf         Date: 04-Oct-2001  DR Number: 5963
//  Project:  GUIComms
//  Description:
//	Fixed processRequests_() while loop argument to ensure that when
//	a print job has been requested it will be honored, even if a
//	change of the com1 device setting from Printer to DCI is pending.
//
//  Revision: 004    By: hlg         Date: 03-Oct-2001  DR Number: 5966
//  Project: GuiComms
//  Description:
//  Added mapping to SRS print requirements.
//
//  Revision: 003    By: quf         Date: 26-Sep-2001  DR Number: 5942
//  Project: GuiComms
//  Description:
//  Code/comment/header cleanup.
//
//  Revision: 002    By: quf         Date: 13-Sep-2001  DR Number: 5493
//  Project: GUIComms
//  Description:
//	Print implementation changes:
//	- Cleaned up the print cancel and print done mechanisms.
//
//  Revision: 001    By: gdc         Date: 20-Feb-2001  DR Number: 5493
//  Project: GuiComms
//  Description:
//  Initial version.
//
//=====================================================================

#include "Sigma.hh"
#include "PrinterInterface.hh"
#include <stdio.h>

//@ Usage-Classes
#include "DisplayContext.hh"
#include "UpperSubScreenArea.hh"
#include "GuiFoundation.hh"
#include "BaseContainer.hh"
#include "TimeStamp.hh"
#include "TextUtil.hh"
#include "SerialPort.hh"
#include "Task.hh"
#include "AlarmStatusCapture.hh"
#include "LowerSubScreenCapture.hh"
#include "SettingsCapture.hh"
#include "ModeSettingsCapture.hh"
#include "PatientDataCapture.hh"
#include "WaveformsCapture.hh"
#include <string.h>
//@ End-Usage

//@ Code...

static const char CR = 13;

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: PrinterInterface [constructor]
//
//@ Interface-Description
//  Constructor for the PrinterInterface class.  Instantiates the HP-PCL 
//  printer interface on the specified SerialPort.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
 
PrinterInterface::PrinterInterface( SerialPort & rSerialPort,
								    SerialInterface::PortNum portNum ) 
  : SerialInterface( rSerialPort, portNum )
{
  //$[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: PrinterInterface [destructor]
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
 
PrinterInterface::~PrinterInterface()
{
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processRequests_
//
//@ Interface-Description
//  Captures, converts to HP-PCL and sends graphical representations of patient 
//  data, waveforms and settings through a serial port to a printer. 
//---------------------------------------------------------------------
//@ Implementation-Description
//  processRequests_ is called to perform processing initiated by the user 
//  pressing the PRINT button on the user interface.  Pixel data from the
//	patient data area, alarm and status area, waveforms subscreen, main
//  settings area, and lower subscreen area is taken from the VGA
//  framebuffer and converted into HP-PCL command sequences and sent to
//  a printer connected to a serial port.
//
// $[GC00401] When initiated, the screen data will be captured ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
PrinterInterface::processRequests_(void)
{
  // If any setting changes for this portNum_, exit this driver so that
  // the new setting(s) can be phased in.  However, if a print is requested
  // do not exit the driver, even if a setting change is pending, until the
  // print request is fulfilled.

  while ( !areSettingsChanged_ || isPrintRequested_ )
  {
  // $[TI1]
    if ( isPrintRequested_ )
    {
    // $[TI1.1]
	  IsPrintInProgress_ = TRUE;

	  purgeline();
	  // Send the printer the commands to begin a new printout.
	  setupPrinter_();

	  printWhiteSpace_();
	  printRasterData_(PatientDataCapture::GetPatientDataCapture());
	  printDivider_();
	  printRasterData_(AlarmStatusCapture::GetAlarmStatusCapture());
	  printRasterData_(WaveformsCapture::GetWaveformsCapture());
	  printDivider_();
	  printWhiteSpace_();
	  printWhiteSpace_();
	  printRasterData_(ModeSettingsCapture::GetModeSettingsCapture());
	  printRasterData_(SettingsCapture::GetSettingsCapture());
	  printRasterData_(LowerSubScreenCapture::GetLowerSubScreenCapture());
	  printDivider_();

	  // Send the end-of-printout commands to the printer.
	  finishPrinting_();

	  endPrint();

	  IsPrintInProgress_ = FALSE;
    }
    else
    {
    // $[TI1.2]
      Task::Delay( 0, 200 );
    }
  }
  // $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setupPrinter_
//
//@ Interface-Description
//  Sends setup commands to printer.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses the SerialPort class to send setup commands to the printer.
//
// $[GC00401] When initiated, the screen data will be captured ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
PrinterInterface::setupPrinter_(void)
{
  if ( isPrintRequested_ )
  {
  // $[TI1]
    // E = Reset
    // &l0O = Print orientation : Portrait
    // &a8L = Set left margin at column 8
    static const char* PRINTER_SETUP_ = "E&l0O&a8L";
    static const Uint32 PRINTER_SETUP_LEN_ = strlen(PRINTER_SETUP_);
    write(PRINTER_SETUP_, PRINTER_SETUP_LEN_, 15000);
  
    // *p0X = Absolute horizontal move : 0 dots
    // *p0Y = Absolute vertical move : 0 dots
    // *t100R = Graphics Resolution : 100 dpi
    // *r0F = Graphics Presentation Mode : Logical Orientation
    // *v7S = Set Foreground color : Index = 7
    static const char* RASTER_SETUP_ = "*p0X*p0Y*t100R*r0F*v7S";
    static const Uint32 RASTER_SETUP_LEN_ = strlen(RASTER_SETUP_);
    write(RASTER_SETUP_, RASTER_SETUP_LEN_, 15000);
  }
  // $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: finishPrinting_
//
//@ Interface-Description
//  Sends setup commands to printer.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses the SerialPort class to send setup commands to the printer.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
PrinterInterface::finishPrinting_(void)
{
  // $[TI1]

  // E   = Printer reset
  static const char* END_RASTER_ = "E";
  static const Uint32 END_RASTER_LEN_ = strlen(END_RASTER_);
  write(END_RASTER_, END_RASTER_LEN_, 15000);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: printDivider_
//
//@ Interface-Description
//  Sends setup commands to printer to send a black divider line to 
//  the printer.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses the SerialPort class to send setup commands to the printer.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
PrinterInterface::printDivider_(void)
{
  if ( isPrintRequested_ )
  {
  // $[TI1]
    // *v0O = Set pattern transparency mode : Transparent
    // *v0T = Select current pattern : Solid black
    // *c1920A = Horizontal rectangle size : 1920 dots wide @ 300dpi
    // *c10B    = Vertical rectangle size : 10 dots high
    // *c100G   = Area Fill ID (100)
    // *c2P    = Fill rectangular area : Shaded fill
    static const char* LINE_ = "*v0O*v0T*c1920A*c10B*c100G*c2P";
    static const Uint32 LINE_LEN_ = strlen(LINE_);

    write(LINE_, LINE_LEN_, 15000);
  }
  // $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: printWhiteSpace_
//
//@ Interface-Description
//  Sends setup commands to printer to print white space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses the SerialPort class to send setup commands to the printer.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
PrinterInterface::printWhiteSpace_(void)
{
  if ( isPrintRequested_ )
  {
  // $[TI1]
    // *p+50Y = Relative Horizontal Move = 50 dots
    static const char* WHITE_SPACE_ = "*p+50Y";
	static const Int32 WHITE_SPACE_LEN_ = strlen(WHITE_SPACE_);

    write(WHITE_SPACE_, WHITE_SPACE_LEN_, 15000);
    write(&CR, 1, 15000);
  }
  // $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: printRasterData_
//
//@ Interface-Description
//  Formats and sends a line of raster graphics to printer.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses the SerialPort class to send setup commands to the printer.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
PrinterInterface::printRasterData_(ScreenCapture& rScreenCapture)
{
  rScreenCapture.resetByteCount();

  // *r1A Start raster graphics : at CAP
  static const char* START_RASTER_ = "*r1A";
  static const Uint32 START_RASTER_LEN_ = strlen(START_RASTER_);

  write(START_RASTER_, START_RASTER_LEN_, 15000);

  while (isPrintRequested_)
  {
  // $[TI1]
	char rasterData[80];
	Int32 nbytes = rScreenCapture.copyRasterLine(rasterData);
	CLASS_ASSERTION(nbytes <= sizeof(rasterData));

	if ( nbytes == 0 )
	{
	// $[TI1.1]
	  break;
	}
	// $[TI1.2]

	// *b0M Set compression mode : Unencoded
	// *b#W Transfer Raster Data
	char rasterCmd[40];
	Int32 byteCount = sprintf(rasterCmd, "*b0M*b%dW", nbytes);
	CLASS_ASSERTION(byteCount <= sizeof(rasterCmd));

	write(rasterCmd, byteCount, 15000);
	write(rasterData, nbytes, 15000);
  }
  // $[TI2]

  // *rB = End raster graphics
  static const char* END_RASTER_ = "*rB";
  static const Uint32 END_RASTER_LEN_ = strlen(END_RASTER_);
  write(END_RASTER_, END_RASTER_LEN_, 15000);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//@ Interface-Description
// Implicitly gain execution control when a software assertion macro
// detects and error.
//---------------------------------------------------------------------
//@ Implementation-Description
// Automatically invoked when CLASS_ASSERTION and CLASS_PRE_CONDITON
// macros (among others) detect a fault.  We just call FaultHandler::SoftFault
// to display pertinent information to the user.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
 
void    
PrinterInterface::SoftFault(  const SoftFaultID softFaultID
                           , const Uint32      lineNumber
                           , const char *      pFileName
                           , const char *      pPredicate )
{
    FaultHandler::SoftFault(softFaultID, GUI_SERIAL_INTERFACE,
		GUI_Serial_Interface::PRINTER_INTERFACE_CLASS, 
		lineNumber, pFileName, pPredicate);
}
