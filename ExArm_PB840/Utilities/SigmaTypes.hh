
#ifndef SigmaTypes_HH
#define SigmaTypes_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
// Filename:  SigmaTypes - System-Wide Types.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Utilities/vcssrc/SigmaTypes.hhv   25.0.4.0   19 Nov 2013 14:35:14   pvcs  $
//
//@ Modifications
//
//   Revision: 004   By: rhj   Date:  20-Jun-2007  SCR Number: 6237
//   Project:  Trend
//   Description:
//      Added Trend_Database subsystem id
//
//   Revision: 003   By: sah   Date: 05-Feb-1996   DCS Number: 675
//   Project:  Sigma (R8027)
//   Description:
//	All subsystem IDs must have a hard-coded value.
//
//   Revision: 002   By: sah   Date: 03-Mar-1995   DCS Number: 398
//   Project:  Sigma (R8027)
//   Description:
//	All subsystem IDs were not identified.
//
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//=====================================================================

//@ Usage-Class
//@ End-Usage

//=====================================================================
//
//  Preprocessor Constants...
//
//=====================================================================

#ifdef NULL
  // 'NULL' is defined explicitly by us to make sure that it is not defined
  // as '(void*)0' -- which is the ANSI C definition.
#  undef NULL
#endif

#ifdef TRUE
#  undef TRUE
#endif

#ifdef FALSE
#  undef FALSE
#endif

//@ Constant:  NULL
// It is guaranteed that a pointer containing this address represents a
// pointer that is distinguishable from a pointer to any object or function.
#define NULL 0

//@ Constant:  FALSE
// This constant is guaranteed to represent the result of an invalid
// condition.
enum { FALSE = 0 };  // anonymous enum...

//@ Constant:  TRUE
// This constant is guaranteed to represent the result of a valid
// condition.
enum { TRUE = !FALSE };  // anonymous enum...


//=====================================================================
//
//  Sigma Standard Types...
//
//=====================================================================

//@ Type:  Byte
// An unsigned, 8-bit data type.
typedef unsigned char	Byte;

//@ Type:  Char
// A reserved type of characters.
typedef char	Char;

//@ Type:  Boolean
// A logical value (true or false) data type.
typedef unsigned char	Boolean;

//@ Type:  Uint8
// An unsigned, 8-bit integer data type.
typedef unsigned char	Uint8;

//@ Type:  Uint16
// An unsigned, 16-bit integer data type.
typedef unsigned short	Uint16;

//@ Type:  Uint32
// An unsigned, 32-bit integer data type.
typedef unsigned int	Uint32;

//@ Type:  Uint
// An unsigned, integer data type that is representative of a native long
// word.
typedef unsigned int	Uint;

//@ Type:  Int8
// A signed, 8-bit integer data type.
typedef char	Int8;

//@ Type:  Int16
// A signed, 16-bit integer data type.
typedef short	Int16;

//@ Type:  Int32
// A signed, 32-bit integer data type.
typedef int	Int32;

//@ Type:  Int
// A signed, integer data type that is representative of a native long word.
typedef int	Int;

//@ Type:  Real32
// A 32-bit, floating-point data type.
typedef float	Real32;

//@ Type:  MemPtr
// A generic pointer to memory.
typedef void*	MemPtr;

// Standard defintions that are part of C99.
typedef unsigned char   uint8_t;
typedef signed char     int8_t;
typedef unsigned short  uint16_t;
typedef short           int16_t;
typedef unsigned long   uint32_t;
typedef long            int32_t;
typedef unsigned __int64   uint64_t;
typedef __int64            int64_t;


//=====================================================================
//
//  Global Enumerations...
//
//=====================================================================

//@ Type:  SigmaStatus
// Standard, system-wide status type.
enum SigmaStatus
{
  FAILURE,
  SUCCESS
};

//@ Type:  SubSystemID
// Identifications for each of the subsystems of the Sigma System.
enum SubSystemID
{
  ALARM_ANALYSIS	   = 0,
  BD_IO_DEVICES		   = 1,
  BREATH_DELIVERY	   = 2,
  DCI_DEVICE		   = 3,
  FOUNDATION		   = 4,
  GUI_APPLICATIONS	   = 5,
  GUI_FOUNDATIONS	   = 6,
  GUI_IO_DEVICES	   = 7,
  OS_FOUNDATION		   = 8,
  OS_PLATFORM		   = 9,
  NETWORK_APPLICATION  = 10,
  PATIENT_DATA		   = 11,
  PERSISTENT_OBJS	   = 12,
  POST			       = 13,
  SAFETY_NET		   = 14,
  SERVICE_DATA		   = 15,
  SERVICE_MODE		   = 16,
  SETTINGS_VALIDATION  = 17,
  STACK_WARE		   = 18,
  SYS_INIT	 	       = 19,
  VENTILATOR_SELF_TEST = 20,
  VENTILATOR_OBJECTS   = 21,
  VGA_GRAPHICS_DRIVERS = 22,
  UTILITIES		       = 23,
  DOWNLOAD		       = 24,
  GUI_SERIAL_INTERFACE = 25,
  TREND_DATABASE	   = 26,
  TOOLS                = 27,
  TINYXML              = 28,
  SWAT                 = 29
  // new subsystems to be added below...
};

//This macro is defined only to remove warnings
#if !defined(UNUSED_SYMBOL)
#define UNUSED_SYMBOL(P) UNREFERENCED_PARAMETER(P);  //UNREFERENCED_PARAMETER provided byWinNT.h file
#endif


#endif  // SigmaTypes_HH
