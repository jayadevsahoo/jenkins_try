#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//================= C L A S S   D E S C R I P T I O N =================
//@ Class:  IeRatioSetting - Inspiratory-to-Expiratory Ratio Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, bounded setting that contains the ratio between the
//  inspiratory and expiratory times.  This class inherits from
//  'BatchBoundedSetting' and provides the specific information needed for
//  representation of I:E ratio.  This information includes the interval
//  and range of this setting's values, this setting's response to a change
//  of its primary setting (see 'acceptPrimaryChange()'), this batch
//  setting's and range of this setting's values, this batch setting's
//  new-patient value (see 'getNewPatientValue()'), and the dependent
//  settings of this setting.
//
//  The value stored by this setting is based on one of the numbers of the
//  ratio, rather than the ratio itself.  That is, the number is stored
//  according to:
//>Von
//     xxx:1;  where xxx = (Ti/Te) when Ti > Te,
//     1.00:1; when Ti=Te,
//     1:xxx;  where xxx = (Te/Ti) when Ti < Te,
//>Voff
//  and xxx:1 > 1.00:1 > 1:xxx
//
//  This setting also dynamically monitors the constant parm setting,
//  and updates its applicability accordingly.
//
//  For "xxx:1" ratios, the internal value of this setting is a positive
//  number, while for "1:xxx" the internal value is negative.
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/IeRatioSetting.ccv   25.0.4.0   19 Nov 2013 14:27:26   pvcs  $
//
//@ Modification-Log
//
//  Revision: 019   By: gdc    Date: 24-Sep-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal non-invasive CPAP.
//
//  Revision: 018 By: srp    Date: 28-May-2002   DR Number: 5898
//  Project:  VCP
//	Description:
//		Incorporated Remediation code review issue(s) -- commentary changes only.
//
//  Revision: 017  By: sah     Date:  05-May-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//	VTPC project-related changes:
//      *  changed array of dependent ids to a non-constant array, which
//         can include peak flow, for 'VC+'
//      *  removed code that would re-activate soft limit with primary
//         setting changes (this caused the assertion listed in VTPC-DCS #40)
//      *  added new transition rule requirement mappings
//      *  added new 'calcDependentValues_()' methods for determining list
//         of dependents
//
//  Revision: 016   By: sah    Date: 29-Jun-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  modified to use newly-modified 'BoundedRange' class to handle
//         range, via linked list of interval nodes
//      *  added new 1.00:1 soft limit
//      *  fixed improper implementation of the allowance of a 4.0% error
//         in the DEVELOPMENT-only 'is{Adjusted,Accepted}Value()' methods
//
//  Revision: 015   By: dosman    Date:  15-Jan-1999    DR Number: 5187
//  Project:  ATC
//  Description:
//    fixed method of computing IeRatio.  Before, it was taking
//    absolute value of Insp Time, ABS(TI), and absolute value of Exp Time, ABS(TE),
//    and computing the ratio, ABS(TI)/ABS(TE).  However, this approach
//    results in I:E ratio values which do not give any useful notion 
//    of the relative ratio when the intermediate TE value used is negative.
//    Therefore, whenever TE is negative, we use a value for TE slightly below
//    the minimum Exp Time value, and compute the ratio ABS(TI)/ABS( MIN(TE)-DELTA  )
//    where DELTA is the 10^precision of TE at that value.
//
//  Revision: 014   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  moved BiLevel-specific implementation (including all soft-bound
//	   implementation) to new I:E ratio setting
//	*  added new 'getApplicability()' method
//	*  added 'acceptTransition()' method due to new Transition Rule
//	   implementation
//	*  cleaned up DEVELOPMENT-only 'is{Adjusted,Accepted}Valid()' methods
//	*  now an observer of constant parm setting, therefore 'valueUpdate()',
//	   'doRetainAttachment()' and 'settingObserverInit()' overridden from
//	   observer base class
//
//  Revision: 013   By: dosman Date:  11-Nov-1998    DR Number: 5252
//  Project:  BILEVEL
//  Description:
//	Added SRS requirement numbers that were missing.
//
//  Revision: 012   By: dosman Date:  10-Nov-1998    DR Number: 5234 & 5258
//  Project:  BILEVEL
//  Description:
//	changed algorithm slightly for reactivating soft bound; before it was
//      not re-activating the soft-bound when we decreased the soft bound
//      into the non-violation region...it waited until we started increasing,
//      and that behavior explains both bugs.  Now it re-activates the soft-bound
//      as soon as we enter the non-violation region.
//
//  Revision: 011   By: dosman Date:  16-Sep-1998    DR Number: BILEVEL 66 & 144
//  Project:  BILEVEL
//  Description:
//      The intermediate value of inspiratory time computed in
//      calcBasedOnSetting_ was changed to be warped in the same
//      direction as the direction the "setting being calculated".
//      Also, special processing was added in BoundedSetting::calcNewValue()
//
//  Revision: 010   By: dosman Date:  08-Sep-1998    DR Number: BILEVEL 130
//  Project:  BILEVEL
//  Description:
//	Added overrideAllSoftBoundStates_ because sometimes it is necessary to disable
//	all the soft bounds for this setting.
//	Changed name of method to updateAllSoftBoundStates_ to clarify that all 
//	soft bounds for this setting are updated through this method.
//
//  Revision: 009   By: dosman Date:  16-Jun-1998    DR Number: 116
//  Project:  BILEVEL
//  Description:
//	Fix updateSoftBoundState_() so that it specifically handles the case
//	when the setting value is not changing
//
//  Revision: 008   By: dosman Date:  02-Jun-1998    DR Number: none
//  Project:  BILEVEL
//  Description:
//	Fix soft bounds to fit requirements: that they should be reset
//	whenever the user moves the knob back into the non-violation region
//
//  Revision: 007   By: dosman Date:  19-May-1998    DR Number: 79, 101
//  Project:  BILEVEL
//  Description:
//	For DCS 101, changed resolution to be same as in spec
//	For DCS 79, changed max value from 150 to 149, but that caused an assertion:
//	fixed the assertion by changing the resolution (i.e., the fix for DCS 101
//	helps fix this problem) because the system was trying to clip itself
//	to the maximum value of 149 but then was warping to 150.
//
//  Revision: 006   By: dosman Date:  29-Apr-1998    DR Number: 34
//  Project:  BILEVEL
//  Description:
//	Added explicit change of bound id for max/min I:E versus TH:TL based on mode bilevel.
//	Changed bound id for the soft bound to be bilevel-related
//
//  Revision: 005   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 004   By: dosman Date:  03-Feb-1998    DR Number: 
//  Project:  BILEVEL
//  Description:
//    Initial bilevel version.
//    Changed absolute max to reflect new,
//      less stringent max, for bilevel mode.
//    Used symbolic constants.
//    Added assertion in updateConstraints_() 
//      to check that absolute maximum is updated/correct 
//      based on mode.
//    Added public virtual method updateAbsValues_() 
//      (see Method Description for details)
//    Added acceptTransition() to ensure that IeRatio updates
//      its absolute maximum value when we change modes from 
//      or to bilevel.
//    Removed acceptTransition() and put this functionality
//      into ModeSetting::calcTransition()
//    Changed constructor to handle SeptupleIntervalRange rather
//      than QuintupleIntervalRange
//      Added requirement numbers for tracing to SRS. 
//
//  Revision: 003   By: sah    Date:  18-Sep-1997    DR Number: 2360
//  Project: Sigma (R8027)
//  Description:
//	Intermediate calculations of NON-CONSTRAINT values of this setting
//	no longer need to be "warped" to a resolution value, therefore replace
//	'warpValue()' with the new 'getValuePrecision()' method, and
//	remove the 'warpDirection' parameter from the calculation
//	method(s).  Also, update the two DEVELOPMENT-only methods for
//	testing the validity of the adjusted/accepted values to use
//	'IsEquivalent()'.
//
//  Revision: 002   By: sah    Date:  10-Aug-1997    DCS Number: 2359
//  Project: Sigma (R8027)
//  Description:
//	Added warning message for future maintenance.  Also, removed
//	unneeded code.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//=====================================================================

#include "IeRatioSetting.hh"
#include "SettingConstants.hh"
#include "ModeValue.hh"
#include "MandTypeValue.hh"
#include "ConstantParmValue.hh"
#include "FlowPatternValue.hh"

//@ Usage-Classes
#include "SettingsMgr.hh"
#include "RespRateSetting.hh"
#include "ConstantParmSetting.hh"
//@ End-Usage


//@ Code...

//====================================================================
//
//  Static Data Definitions...
//
//====================================================================

// $[02166] -- this setting's dependent settings...
static SettingId::SettingIdType  ArrDependentSettingIds_[] =
{
	SettingId::EXP_TIME,
	SettingId::INSP_TIME,
	SettingId::PEAK_INSP_FLOW,

	SettingId::NULL_SETTING_ID
};


// $[02162] -- max value for I:E in mode...
static const Real32  ABSOLUTE_MAX_VALUE_    = 4.00f;

// $[NE02005] -- I:E ratio's soft range...
static const Real32  MAX_SOFT_BOUND_VALUE_  = 1.00f;


//  $[02162] -- The setting's range ...
//  $[02164] -- The setting's resolution ...
static const BoundedInterval  LAST_NODE_ =
{
	-10000.0f,		// absolute minimum...
	0.0f,		// unused...
	ONES,		// unused...
	NULL
};
static const BoundedInterval  NODE4_ =
{
	-100.0f,		// change-point...
	1.0f,		// resolution...
	ONES,		// precision...
	&::LAST_NODE_
};
static const BoundedInterval  NODE3_ =
{
	-10.0f,		// change-point...
	0.1f,		// resolution...
	TENTHS,		// precision...
	&::NODE4_
};
static const BoundedInterval  NODE2_ =
{
	-1.01f,		// change-point...
	0.01f,		// resolution...
	HUNDREDTHS,		// precision...
	&::NODE3_
};
static const BoundedInterval  NODE1_ =
{
	1.00f,		// change-point...
	(1.00f - (-1.01f)),// resolution...
	HUNDREDTHS,		// precision...
	&::NODE2_
};
static const BoundedInterval  INTERVAL_LIST_ =
{
	4.0f,		// absolute maximum...
	0.01f,		// resolution...
	HUNDREDTHS,		// precision...
	&::NODE1_
};



//====================================================================
//
//  Public Methods...
//
//====================================================================

//============= M E T H O D   D E S C R I P T I O N ==================
//@ Method: IeRatioSetting()  [Default Constructor]
//
//@ Interface-Description
//  Construct a default I:E ratio setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This setting inherits from BoundedSetting.  The method passes to
//  BoundedSetting it's default information. Also, this method initializes
//  the value interval.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

IeRatioSetting::IeRatioSetting(void)
: BatchBoundedSetting(SettingId::IE_RATIO,
					  ::ArrDependentSettingIds_,
					  &::INTERVAL_LIST_,
					  IE_RATIO_MAX_ID,		// maxConstraintId...
					  NULL_SETTING_BOUND_ID,// minConstraintId...
					  FALSE)		// no epsilon factor...
{
	CALL_TRACE("IeRatioSetting()");

	// register this setting's soft bound...
	registerSoftBound_(::IE_RATIO_SOFT_MAX_ID, Setting::UPPER);
}	// $[TI1]


//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  ~IeRatioSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

IeRatioSetting::~IeRatioSetting(void)
{
	CALL_TRACE("~IeRatioSetting()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[01049] -- I:E not shown in SPONT mode
//  $[01071] -- main settings applicability
//  $[01081] -- breath settings applicability
//---------------------------------------------------------------------
//@ PreCondition
//  (qualifierId == Notification::ACCEPTED  ||
//   qualifierId == Notification::ADJUSTED)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
	IeRatioSetting::getApplicability(const Notification::ChangeQualifier qualifierId) const
{
	CALL_TRACE("getApplicability(qualifierId)");

	const Setting*  pMode     = SettingsMgr::GetSettingPtr(SettingId::MODE);
	const Setting*  pMandType = SettingsMgr::GetSettingPtr(SettingId::MAND_TYPE);
	const Setting*  pConstantParm =
		SettingsMgr::GetSettingPtr(SettingId::CONSTANT_PARM);

	DiscreteValue  modeValue;
	DiscreteValue  mandTypeValue;
	DiscreteValue  constantParmValue;

	switch ( qualifierId )
	{
		case Notification::ACCEPTED :	  // $[TI1]
			modeValue         = pMode->getAcceptedValue();
			mandTypeValue     = pMandType->getAcceptedValue();
			constantParmValue = pConstantParm->getAcceptedValue();
			break;
		case Notification::ADJUSTED :	  // $[TI2]
			modeValue         = pMode->getAdjustedValue();
			mandTypeValue     = pMandType->getAdjustedValue();
			constantParmValue = pConstantParm->getAdjustedValue();
			break;
		default :
			AUX_CLASS_ASSERTION_FAILURE(qualifierId);
			break;
	}

	Applicability::Id  applicability;

	switch ( modeValue )
	{
		case ModeValue::AC_MODE_VALUE :
		case ModeValue::SIMV_MODE_VALUE :	  // $[TI3]
			switch ( mandTypeValue )
			{
				case MandTypeValue::VCP_MAND_TYPE :
				case MandTypeValue::PCV_MAND_TYPE :		// $[TI3.1]
					applicability =
						(constantParmValue == ConstantParmValue::IE_RATIO_CONSTANT)
						? Applicability::CHANGEABLE	   // $[TI3.1.1]
						: Applicability::VIEWABLE;	   // $[TI3.1.2]
					break;
				case MandTypeValue::VCV_MAND_TYPE :		// $[TI3.2]
					applicability = Applicability::VIEWABLE;
					break;
				default :
					// unexpected mandatory type...
					AUX_CLASS_ASSERTION_FAILURE(mandTypeValue);
					break;
			}
			break;
		case ModeValue::SPONT_MODE_VALUE :
		case ModeValue::CPAP_MODE_VALUE :
		case ModeValue::BILEVEL_MODE_VALUE :	  // $[TI4]
			applicability = Applicability::INAPPLICABLE;
			break;
		default :
			// unexpected mode...
			AUX_CLASS_ASSERTION_FAILURE(modeValue);
			break;
	}

	return(applicability);
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [const, virtual]
//
//@ Interface-Description
//  Return this setting's new-patient value.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
	IeRatioSetting::getNewPatientValue(void) const
{
	CALL_TRACE("getNewPatientValue()");

	BoundedValue  newPatient;

	// the I:E ratio new patient value is based on the new-patient values of
	// inspiratory time and respiratory rate...
	newPatient = calcBasedOnSetting_(SettingId::INSP_TIME,
									 BoundedRange::WARP_NEAREST,
									 BASED_ON_NEW_PATIENT_VALUES);

	return(newPatient);
}	// $[TI1]


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method: acceptPrimaryChange(primaryId)
//
//@ Interface-Description
//  One of 'this' objects primary setting values has changed, so the settings
//  value will be based on the new values, indicaticated by primaryId. 
//  If this setting's bound is violated by the primary setting's newly 
//  "adjusted" value, then the setting's value is "clipped" to that bound. 
//  Otherwise, a value of 'NULL' is returned to indicate no dependent 
//  bound was violated.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02002] -- new dependent settings based on proposed primary settings...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

const BoundStatus*
	IeRatioSetting::acceptPrimaryChange(const SettingId::SettingIdType primaryId)
{
	CALL_TRACE("acceptPrimaryChange(primaryId)");

	BoundStatus&  rBoundStatus = getBoundStatus_();

	// initialize to holding this setting's id...
	rBoundStatus.setViolationId(getId());

	BoundedValue  newIeRatio;

	newIeRatio = calcBasedOnSetting_(primaryId, BoundedRange::WARP_NEAREST,
									 BASED_ON_ADJUSTED_VALUES);

	// update dynamic constraints...
	updateConstraints_();

	// test 'newIeRatio' against this setting's bounded range; the bound
	// violation state, if any, is returned in 'rBoundStatus', while
	// 'newIeRatio' is "clipped" if a bound is violated...
	getBoundedRange_().testValue(newIeRatio, rBoundStatus);

	// store the value...
	setAdjustedValue(newIeRatio);

	const BoundStatus*  pBoundStatus;

	// if there is a bound violation return a pointer to this setting's
	// bound status, otherwise return 'NULL'...
	pBoundStatus = (rBoundStatus.isInViolation()) ? &rBoundStatus // $[TI1]
				   : NULL;	   // $[TI2]

	return(pBoundStatus);
}


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method: acceptTransition(settingId, newValue, currValue) [virtual]
//
//@ Interface-Description
//  This method is responsible for updating this setting based on the
//  the Main Control Setting identified by 'settingId' and the transition
//  of its value from 'currValue' to 'newValue'.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02026]\b\   -- from-SPONT-to-AC/SIMV transition rules
//  $[BL02000]\g\ -- from-BILEVEL-to-AC/SIMV transition rules
//  $[02022]\i\   -- from-PC-to-VC transition rules
//  $[VC02006]\d\ -- from-VC-to-VC+ transition rules
//  $[VC02006]\k\ -- from-PC-to-VC+ transition rules
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	IeRatioSetting::acceptTransition(const SettingId::SettingIdType,
									 const DiscreteValue,
									 const DiscreteValue)
{
	CALL_TRACE("acceptTransition(settingId, newValue, currValue)");

	if ( getApplicability(Notification::ADJUSTED) != Applicability::INAPPLICABLE )
	{  // $[TI1] -- this setting is applicable, process transition...
		// temporarily disable all soft bounds so they don't limit the calculated
		// value (this also updates the constraints)...
		overrideAllSoftBoundStates_();

		BoundedValue  newIeRatio;

		newIeRatio = calcBasedOnSetting_(SettingId::INSP_TIME,
										 BoundedRange::WARP_NEAREST,
										 BASED_ON_ADJUSTED_VALUES);

		// store the transition value into the adjusted context...
		setAdjustedValue(newIeRatio);

		// now that the calculated value has been stored, re-enable all applicable
		// soft bounds...
		updateAllSoftBoundStates_(Setting::NON_CHANGING);

		setForcedChangeFlag();
	}  // $[TI2] -- this setting is inapplicable, leave unchanged...
}


//==================== M E T H O D   D E S C R I P T I O N =============
//@ Method:  valueUpdate(qualifierId, pSubject)  [virtual]
//
//@ Interface-Description
//  Respond to subject's value changes, by, possibly, updating this setting's
//  applicability.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (pSubject->getId() == SettingId::CONSTANT_PARM)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	IeRatioSetting::valueUpdate(const Notification::ChangeQualifier qualifierId,
								const SettingSubject*               pSubject)
{
	CALL_TRACE("valueUpdate(qualifierId, pSubject)");
	SAFE_AUX_CLASS_PRE_CONDITION((pSubject->getId() == SettingId::CONSTANT_PARM),
								 pSubject->getId());

	// update this instance's applicability...
	updateApplicability(qualifierId);
}  // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: doRetainAttachment(pSubject)  [virtual, const]
//
//@ Interface-Description
//  When task-level, state changes occur, all subjects are notified
//  to detach from all observers.  This is done because the GUI-Apps
//  observers may be overwritten by new class instances, without any
//  explicit destructor/detachment calls.
//
//  This method is defined here to indicate to any of this instance's
//  subjects, not to detach this observer.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean
	IeRatioSetting::doRetainAttachment(const SettingSubject*) const
{
	CALL_TRACE("doRetainAttachment(pSubject)");

	return(TRUE);
}  // $[TI1]


//==================== M E T H O D   D E S C R I P T I O N ============
//@ Method:  settingObserverInit()  [virtual]
//
//@ Interface-Description
//  This setting needs to monitor the value of Constant Parm setting,
//  therefore this virtual method is overridden to provide a point during
//  initialization to attach to that (subject) setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	IeRatioSetting::settingObserverInit(void)
{
	CALL_TRACE("settingObserverInit()");

	// monitor changes in constant parm's value...
	attachToSubject_(SettingId::CONSTANT_PARM, Notification::VALUE_CHANGED);
}  // $[TI1]


#if defined(SIGMA_DEVELOPMENT)

//============== M E T H O D   D E S C R I P T I O N =================
// Method:  isAcceptedValid()  [const, virtual]
//
// Interface-Description
//  Is the accepted value of this setting valid as determined by its
//  dependencies?
//---------------------------------------------------------------------
// Implementation-Description
//  The setting determines, and returns its validity.
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

Boolean
	IeRatioSetting::isAcceptedValid(void) const
{
	CALL_TRACE("isAcceptedValid()");

	Boolean  isValid = BoundedSetting::isAcceptedValid();

	if ( isValid )
	{
		if ( getApplicability(Notification::ACCEPTED) !=
			 Applicability::INAPPLICABLE )
		{
			const Real32  IE_RATIO_VALUE = BoundedValue(getAcceptedValue()).value;

			const DiscreteValue  CONSTANT_PARM_VALUE =
				ConstantParmSetting::GetValue(BASED_ON_ACCEPTED_VALUES);

			SettingId::SettingIdType  basedOnSettingId;

			switch ( CONSTANT_PARM_VALUE )
			{
				case ConstantParmValue::INSP_TIME_CONSTANT :
				case ConstantParmValue::IE_RATIO_CONSTANT :
					basedOnSettingId = SettingId::INSP_TIME;
					break;
				case ConstantParmValue::EXP_TIME_CONSTANT :
					basedOnSettingId = SettingId::EXP_TIME;
					break;
				default : 
					AUX_CLASS_ASSERTION_FAILURE(CONSTANT_PARM_VALUE);
					break;
			};

			const BoundedValue  CALC_IE_RATIO  =
				calcBasedOnSetting_(basedOnSettingId,
									BoundedRange::WARP_NEAREST,
									BASED_ON_ACCEPTED_VALUES);

			isValid = ::IsEquivalent(IE_RATIO_VALUE,
									 CALC_IE_RATIO.value,
									 CALC_IE_RATIO.precision);

			if ( !isValid )
			{
				// a 4.0% error is allowed between the inspiratory time-based
				// calculation and the current I:E ratio values...
				const Real32  CALCULATION_ERROR =
					ABS_VALUE((CALC_IE_RATIO.value * 0.04));

				// calculate the difference between the actual and calculated values...
				const Real32  DIFFERENCE =
					ABS_VALUE(ABS_VALUE(IE_RATIO_VALUE) - ABS_VALUE(CALC_IE_RATIO.value));

				isValid = (DIFFERENCE <= CALCULATION_ERROR);
			}
		}
	}

	return(isValid);
}


//============== M E T H O D   D E S C R I P T I O N =================
// Method:  IsAdjustedValid()  [static]
//
// Interface-Description
//  Is the adjusted value of this setting valid as determined by its
//  dependencies?  Return "True" if the setting is valid, returns "False"
//  if the setting is not valid.
//---------------------------------------------------------------------
// Implementation-Description
//  The setting determines, and returns its validity.
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

Boolean
	IeRatioSetting::isAdjustedValid(void)
{
	CALL_TRACE("isAdjustedValid()");

	Boolean  isValid = BoundedSetting::isAdjustedValid();

	if ( isValid )
	{
		if ( getApplicability(Notification::ADJUSTED) !=
			 Applicability::INAPPLICABLE )
		{
			const Real32  IE_RATIO_VALUE = BoundedValue(getAdjustedValue()).value;

			const DiscreteValue  CONSTANT_PARM_VALUE =
				ConstantParmSetting::GetValue(BASED_ON_ADJUSTED_VALUES);

			SettingId::SettingIdType  basedOnSettingId;

			switch ( CONSTANT_PARM_VALUE )
			{
				case ConstantParmValue::IE_RATIO_CONSTANT :
				case ConstantParmValue::INSP_TIME_CONSTANT :
					basedOnSettingId = SettingId::INSP_TIME;
					break;
				case ConstantParmValue::EXP_TIME_CONSTANT :
					basedOnSettingId = SettingId::EXP_TIME;
					break;
				default : 
					AUX_CLASS_ASSERTION_FAILURE(CONSTANT_PARM_VALUE);
					break;
			};

			const BoundedValue  CALC_IE_RATIO  =
				calcBasedOnSetting_(basedOnSettingId,
									BoundedRange::WARP_NEAREST,
									BASED_ON_ADJUSTED_VALUES);

			isValid = ::IsEquivalent(IE_RATIO_VALUE,
									 CALC_IE_RATIO.value,
									 CALC_IE_RATIO.precision);

			if ( !isValid )
			{
				// since the calculation of an I:E ratio is inherently inaccurate,
				// a percentage error is allowed between the inspiratory time-based
				// calculation and the current I:E ratio values...
				const Real32  CALCULATION_ERROR =
					ABS_VALUE((CALC_IE_RATIO.value * 0.04));  //4% error

				// calculate the difference between the actual and calculated values...
				const Real32  DIFFERENCE =
					ABS_VALUE(ABS_VALUE(IE_RATIO_VALUE) - ABS_VALUE(CALC_IE_RATIO.value));

				isValid = (DIFFERENCE <= CALCULATION_ERROR);
			}

			if ( !isValid )
			{
				const Setting*  pInspTime =
					SettingsMgr::GetSettingPtr(SettingId::INSP_TIME);
				const Setting*  pRespRate =
					SettingsMgr::GetSettingPtr(SettingId::RESP_RATE);

				cout << "INVALID I:E Ratio:\n";
				cout << "CALC = " << CALC_IE_RATIO << endl;
				cout << *this << *pInspTime << *pRespRate << endl;
			}
		}
	}

	return(isValid);
}

#endif // defined (SIGMA_DEVELOPMENT)


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//	      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
	IeRatioSetting::SoftFault(const SoftFaultID  softFaultID,
							  const Uint32       lineNumber,
							  const char*        pFileName,
							  const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

	FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
							IE_RATIO_SETTING, lineNumber, pFileName,
							pPredicate);
}


//====================================================================
//
//  Protected Methods...
//
//====================================================================

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method: calcDependentValues_(isValueIncreasing)
//
//@ Interface-Description
//  Notify this setting's dependent settings of an update to this
//  primary setting.  If a dependent bound is violated, this setting's
//  bound status is updated with the dependent bound information.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02166] -- this setting's dependent settings...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean
	IeRatioSetting::calcDependentValues_(const Boolean isValueIncreasing)
{
	CALL_TRACE("calcDependentValues_(isValueIncreasing)");

	const Setting*  pMandType =
		SettingsMgr::GetSettingPtr(SettingId::MAND_TYPE);

	const DiscreteValue  MAND_TYPE_VALUE = pMandType->getAdjustedValue();

	Uint  idx = 0u;

	switch ( MAND_TYPE_VALUE )
	{
		case MandTypeValue::PCV_MAND_TYPE :
		case MandTypeValue::VCV_MAND_TYPE :	  // $[TI1]
			// all timing dependents are active here...
			ArrDependentSettingIds_[idx++] = SettingId::EXP_TIME;
			ArrDependentSettingIds_[idx++] = SettingId::INSP_TIME;
			break;
		case MandTypeValue::VCP_MAND_TYPE :	  // $[TI2]
			// all dependents are active here...
			ArrDependentSettingIds_[idx++] = SettingId::EXP_TIME;
			ArrDependentSettingIds_[idx++] = SettingId::INSP_TIME;
			ArrDependentSettingIds_[idx++] = SettingId::PEAK_INSP_FLOW;
			break;
		default :
			AUX_CLASS_ASSERTION_FAILURE(MAND_TYPE_VALUE);
			break;
	}

	// terminate with the null id...
	ArrDependentSettingIds_[idx] = SettingId::NULL_SETTING_ID;

	// forward on to base class...
	return(Setting::calcDependentValues_(isValueIncreasing));
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  updateConstraints_()  [virtual]
//
//@ Interface-Description
//  Update the dynamic bounds of this setting.  This method sets this
//  setting's maximum constraint to the most restrictive of this setting's
//  upper bounds, and, correspondingly, set this setting's minimum
//  constraint to its most restrictive lower bound.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Determine the minimum upper bound, and set the upper limit to it.
//  $[02162]   -- setting's range ...
//  $[NE02005] -- setting's soft range...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	IeRatioSetting::updateConstraints_(void)
{
	CALL_TRACE("updateConstraints_()");

	//===================================================================
	// determine I:E ratio's maximum constraint...
	//===================================================================

	BoundedRange::ConstraintInfo  maxConstraintInfo;

	if ( isSoftBoundActive_(IE_RATIO_SOFT_MAX_ID) )
	{	// $[TI1] -- 'IE_RATIO_SOFT_MAX_ID' soft bound is currently active...
		// the soft-bound is ALWAYS more restrictive than the hard bound...
		maxConstraintInfo.id     = IE_RATIO_SOFT_MAX_ID;
		maxConstraintInfo.value  = ::MAX_SOFT_BOUND_VALUE_;
		maxConstraintInfo.isSoft = TRUE;
	}
	else
	{	// $[TI2] -- 'IE_RATIO_SOFT_MAX_ID' soft bound is NOT active...
		// initialize to absolute maximum bound id...
		maxConstraintInfo.id     = IE_RATIO_MAX_ID;
		maxConstraintInfo.value  = ::ABSOLUTE_MAX_VALUE_;
		maxConstraintInfo.isSoft = FALSE;
	}

	getBoundedRange_().updateMaxConstraint(maxConstraintInfo);
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  findSoftMinMaxValues_(rSoftMinValue, rSoftMaxValue)  [const]
//
//@ Interface-Description
//  Return, via 'rSoftMinValue' and 'rSoftMaxValue', the soft bound lower
//  and upper limit values, respectively.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[NE02005] -- setting's soft range...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	IeRatioSetting::findSoftMinMaxValues_(Real32& rSoftMinValue,
										  Real32& rSoftMaxValue) const
{
	CALL_TRACE("findSoftMinMaxValues_(rSoftMinValue, rSoftMaxValue)");

	rSoftMinValue = getAbsoluteMinValue_();
	rSoftMaxValue = ::MAX_SOFT_BOUND_VALUE_;
}  // $[TI1]


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method: calcBasedOnSetting_(basedOnSettingId, warpDirection, basedOnCategory)  [const]
//
//@ Interface-Description
//  This method is responsible for calculating, and returning, a new
//  "adjusted" value for this setting, based on the "adjusted" value
//  of the dependent setting given by 'basedOnSettingId'.  This method will
//  calculate values for each of this setting's dependent settings,
//  based on values in the Adjusted Context.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02163] -- formula for calculating this setting's new-patient value...
//---------------------------------------------------------------------
//@ PreCondition
//  (basedOnSettingId == SettingId::INSP_TIME  &&
//   (basedOnCategory == BASED_ON_ADJUSTED_VALUES  ||
//    basedOnCategory == BASED_ON_NEW_PATIENT_VALUES))
//			||
//  ((basedOnSettingId == SettingId::RESP_RATE  ||
//    basedOnSettingId == SettingId::EXP_TIME  ||
//    basedOnSettingId == SettingId::TIDAL_VOLUME  ||
//    basedOnSettingId == SettingId::PEAK_INSP_FLOW  ||
//    basedOnSettingId == SettingId::PLATEAU_TIME  ||
//    basedOnSettingId == SettingId::FLOW_PATTERN)  &&
//    basedOnCategory == BASED_ON_ADJUSTED_VALUES)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

BoundedValue
	IeRatioSetting::calcBasedOnSetting_(
									   const SettingId::SettingIdType basedOnSettingId,
									   const BoundedRange::WarpDir    warpDirection,
									   const BasedOnCategory          basedOnCategory
									   ) const
{
	CALL_TRACE("calcBasedOnSetting_(basedOnSettingId, warpDirection, basedOnCategory)");

	SettingId::SettingIdType  finalBasedOnSettingId;

	if ( basedOnSettingId == SettingId::RESP_RATE )
	{	// $[TI1] -- convert 'RESP_RATE' to "based-on" id...
		const DiscreteValue  CONSTANT_PARM_VALUE =
			ConstantParmSetting::GetValue(basedOnCategory);

		switch ( CONSTANT_PARM_VALUE )
		{
			case ConstantParmValue::INSP_TIME_CONSTANT :	// $[TI1.1]
				// respiratory rate is being changed while holding inspiratory time
				// constant, therefore calculate the new I:E ratio from the
				// current inspiratory time...
				finalBasedOnSettingId = SettingId::INSP_TIME;
				break;
			case ConstantParmValue::EXP_TIME_CONSTANT :	   // $[TI1.2]
				// respiratory rate is being changed while holding expiratory time
				// constant, therefore calculate the new I:E ratio from the
				// current expiratory time...
				finalBasedOnSettingId = SettingId::EXP_TIME;
				break;
			case ConstantParmValue::IE_RATIO_CONSTANT :
			case ConstantParmValue::TOTAL_CONSTANT_PARMS :
			default :
				// invalid value for 'CONSTANT_PARM_VALUE'...
				AUX_CLASS_ASSERTION_FAILURE(CONSTANT_PARM_VALUE);
				break;
		};
	}
	else
	{	// $[TI2]
		// use passed in parameter...
		finalBasedOnSettingId = basedOnSettingId;
	}

	// get the breath period from the respiratory rate...
	const Real32  BREATH_PERIOD_VALUE =
		RespRateSetting::GetBreathPeriod(basedOnCategory);

	const BoundedSetting*  pInspTime =
		SettingsMgr::GetBoundedSettingPtr(SettingId::INSP_TIME);
	const BoundedSetting*  pExpTime  =
		SettingsMgr::GetBoundedSettingPtr(SettingId::EXP_TIME);

	BoundedValue  ieRatio;
	BoundedValue  inspTime;

	Real32  expTimeValue;

	switch ( finalBasedOnSettingId )
	{
		//-------------------------------------------------------------------
		// $[02014](1) -- new I:E ratio while changing insp. time...
		// $[02018](1) -- new I:E ratio while keeping insp. time constant...
		//-------------------------------------------------------------------
		case SettingId::INSP_TIME :
			{	// $[TI3] -- base I:E ratio's calculation on inspiratory time...
				switch ( basedOnCategory )
				{
					case BASED_ON_NEW_PATIENT_VALUES :	  // $[TI3.1]
						// get the new-patient insp time value...
						inspTime.value = BoundedValue(pInspTime->getNewPatientValue()).value;
						break;
					case BASED_ON_ADJUSTED_VALUES :		  // $[TI3.2]
						// get the "adjusted" insp time value...
						inspTime.value = BoundedValue(pInspTime->getAdjustedValue()).value;
						break;
					case BASED_ON_ACCEPTED_VALUES :
						// fall through to 'default:' when not in DEVELOPMENT mode...
#if defined(SIGMA_DEVELOPMENT)
						// get the "accepted" insp time value...
						inspTime.value = BoundedValue(pInspTime->getAcceptedValue()).value;
						break;
#endif // defined(SIGMA_DEVELOPMENT)
					default :
						// unexpected 'basedOnCategory' value..
						AUX_CLASS_ASSERTION_FAILURE(basedOnCategory);
						break;
				};

				// calculate an expiratory time, based on the "adjusted" inspiratory
				// time...
				expTimeValue = BREATH_PERIOD_VALUE - inspTime.value;
			}
			break;

			//-------------------------------------------------------------------
			// $[02012](1) -- new I:E ratio while changing exp. time...
			// $[02016](1) -- new I:E ratio while keeping exp. time constant...
			//-------------------------------------------------------------------
		case SettingId::EXP_TIME :
			{	// $[TI4] -- base I:E ratio's calculation on exp time...
				switch ( basedOnCategory )
				{
					case BASED_ON_ADJUSTED_VALUES :
						// get the "adjusted" exp time value...
						expTimeValue = BoundedValue(pExpTime->getAdjustedValue()).value;
						break;
					case BASED_ON_ACCEPTED_VALUES :
						// fall through to 'default:' when not in DEVELOPMENT mode...
#if defined(SIGMA_DEVELOPMENT)
						// get the "accepted" exp time value...
						expTimeValue = BoundedValue(pExpTime->getAcceptedValue()).value;
						break;
#endif // defined(SIGMA_DEVELOPMENT)
					case BASED_ON_NEW_PATIENT_VALUES :
					default :
						// unexpected 'basedOnCategory' value..
						AUX_CLASS_ASSERTION_FAILURE(basedOnCategory);
						break;
				};

				// calculate an inspiratory time, based on the "adjusted" expiratory
				// time...
				inspTime.value = BREATH_PERIOD_VALUE - expTimeValue;
			}
			break;

			//-------------------------------------------------------------------
			// $[02019](3) -- new I:E ratio with new VCV parameter...
			//-------------------------------------------------------------------
		case SettingId::TIDAL_VOLUME :
		case SettingId::PEAK_INSP_FLOW :
		case SettingId::PLATEAU_TIME :
		case SettingId::FLOW_PATTERN :
			{	// $[TI5] -- base I:E ratio's calculation on the VCV settings...
				// I:E ratio is to base its value on the "adjusted" VCV
				// settings, only...
				AUX_CLASS_ASSERTION((basedOnCategory == BASED_ON_ADJUSTED_VALUES),
									basedOnCategory);

				// get pointers to each of the VCV parameters...
				const Setting*  pFlowPattern =
					SettingsMgr::GetSettingPtr(SettingId::FLOW_PATTERN);
				const Setting*  pPeakInspFlow =
					SettingsMgr::GetSettingPtr(SettingId::PEAK_INSP_FLOW);
				const Setting*  pPlateauTime =
					SettingsMgr::GetSettingPtr(SettingId::PLATEAU_TIME);
				const Setting*  pTidalVolume =
					SettingsMgr::GetSettingPtr(SettingId::TIDAL_VOLUME);

				const DiscreteValue  FLOW_PATTERN_VALUE =
					pFlowPattern->getAdjustedValue();

				const Real32  PEAK_INSP_FLOW_VALUE =
					BoundedValue(pPeakInspFlow->getAdjustedValue()).value;
				const Real32  PLATEAU_TIME_VALUE =
					BoundedValue(pPlateauTime->getAdjustedValue()).value;
				const Real32  TIDAL_VOLUME_VALUE =
					BoundedValue(pTidalVolume->getAdjustedValue()).value;

				switch ( FLOW_PATTERN_VALUE )
				{
					case FlowPatternValue::SQUARE_FLOW_PATTERN : 
						{	// $[TI5.1] 
							static const Real32  SQUARE_FACTOR_ = (60000.0f * 0.001f);

							// $[02019]b -- formula for calculating inspiratory time
							//              from the VCV parameters...
							inspTime.value = ((SQUARE_FACTOR_ * TIDAL_VOLUME_VALUE) /
											  PEAK_INSP_FLOW_VALUE);
						}
						break;
					case FlowPatternValue::RAMP_FLOW_PATTERN :  
						{	// $[TI5.2] 
							static const Real32  RAMP_FACTOR_ = (60000.0f * 0.001f * 2.0f);

							const Setting*  pMinInspFlow =
								SettingsMgr::GetSettingPtr(SettingId::MIN_INSP_FLOW);

							const Real32  MIN_INSP_FLOW_VALUE =
								BoundedValue(pMinInspFlow->getAdjustedValue()).value;

							// $[02019]c -- formula for calculating inspiratory time from
							//              the VCV parameters...
							inspTime.value = ((RAMP_FACTOR_ * TIDAL_VOLUME_VALUE) /
											  (PEAK_INSP_FLOW_VALUE + MIN_INSP_FLOW_VALUE));
						}
						break;
					default :
						// unexpected 'FLOW_PATTERN_VALUE' value...
						AUX_CLASS_ASSERTION_FAILURE(FLOW_PATTERN_VALUE);
						break;
				};

				// add the plateau time to the calculated inspiratory
				// time -- irregardless of the flow pattern...$[02019]b...
				inspTime.value += PLATEAU_TIME_VALUE;

				BoundedSetting*  pInspTime =
					SettingsMgr::GetBoundedSettingPtr(SettingId::INSP_TIME);

				// because inspiratory time has a different resolution in VCV
				// than expiratory time, the "raw" insp time value must be rounded
				// to a resolution boundary, before calculating expiratory time...
				pInspTime->warpToRange(inspTime, warpDirection, FALSE);

				// calculate expiratory time...
				expTimeValue = BREATH_PERIOD_VALUE - inspTime.value;

				// check for a negative expiratory time...
				if ( expTimeValue < 0.0f )
				{
					// $[TI5.3]
					// a negative Expiratory Time does not make sense...
					// it means that the calculated Inspiratory Time is invalid, too big.
					// Use a minimum Expiratory Time, and recompute Inspiratory Time.
					BoundedValue expTime;

					expTime.value = expTimeValue;

					// warpDirection is irrelevant; we will be clipping to minimum
					BoundedSetting*  pExpTime =
						SettingsMgr::GetBoundedSettingPtr(SettingId::EXP_TIME);

					pExpTime->warpToRange(expTime, warpDirection, TRUE);

					// Special case to help fix DCS 5187
					// If Expiratory Time is below it's minimum value, but still positive, 
					// we use the I:E ratio computed from ABS(TI)/ABS(TE) even though
					// the TE value is invalid because we do not want the I:E ratio
					// value to be clipped at this point; we want the value to be 
					// clipped later when it is tested so that we know a bound violation
					// occurred.
					// In contrast, for negative Expiratory Time, the I:E ratio would be bogus 
					// computed from ABS(TI)/ABS(TE).  Therefore, we first determine
					// the minimum Expiratory time, MIN(TE), and use a TE value
					// below that so that we do not clip the I:E value based on TE
					// at this point and we implicitly store the fact that TE is 
					// below it's minimum value.
					expTimeValue = (expTime.value * 0.5f);
					inspTime.value = BREATH_PERIOD_VALUE - expTimeValue;
				}
				// $[TI5.4] -- implied else
			}
			break;

		default :
			// unexpected dependent setting id...
			AUX_CLASS_ASSERTION_FAILURE(finalBasedOnSettingId);
			break;
	};

	//====================================================================
	// a LARGE knob delta can cause Th or Tl to "jump" passed a bound
	// constraint, causing a Th or Tl value less than its minimum; clip at
	// minimum values...
	//====================================================================

	inspTime.value = MAX_VALUE(inspTime.value,
							   pInspTime->getBoundedRange().getMinValue());
	expTimeValue   = MAX_VALUE(expTimeValue,
							   pExpTime->getBoundedRange().getMinValue());

	if ( inspTime.value == expTimeValue )
	{	// $[TI6]
		// when the inspiratory time is equivalent to the expiratory time
		// (to be shown as "1.00:1"), we store a value of one...
		ieRatio.value = 1.00f;
	}
	else if ( inspTime.value > expTimeValue )
	{	// $[TI7]
		// when the inspiratory time is greater than the expiratory time
		// (to be shown as "xx:1"), we store the positive insp.-to-exp. ratio...
		ieRatio.value = (inspTime.value / expTimeValue);
	}
	else
	{	// $[TI8]
		// when the expiratory time is greater than the inspiratory time
		// (to be shown as "1:xx"), we store the negative exp.-to-insp. ratio...
		ieRatio.value = -(expTimeValue / inspTime.value);
	}

	getBoundedRange().warpValue(ieRatio, warpDirection, FALSE);

	if ( ieRatio.value == 1.00f  &&  inspTime.value > expTimeValue )
	{  // $[TI9] -- SPECIAL CASE...
		// the rounding of the Ti/Te calculation resulted in '1.00' -- which
		// may be an active soft-bound -- but since Ti > Te, we don't want to
		// return '1.00', force up...
		ieRatio.value += 0.005f;  // modify value...

		// warp modified value "up" to the next resolution...
		getBoundedRange().warpValue(ieRatio, BoundedRange::WARP_UP, FALSE);
	}  // $[TI10] -- no special processing needed...

	return(ieRatio);
}
