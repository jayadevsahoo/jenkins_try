#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ class BdTasks - Breath Delivery tasks.
//---------------------------------------------------------------------
//@ Interface-Description
//
//    This class contains tasks that are responsible for breath delivery.
//    The tasks includes BdExecutiveTask, BdSecondaryTask, BdStatusTask and
//    XmitFlashTableTask.
//    BdExecTask is responsible for controlling BD apps operational mode.
//    BdMainCycle is responsible for controlling the mode of breathing
//    and delivery of breath.
//    BdSecondaryTask is responsible for activating BD secondary cycle
//    every 4 BD cycles.
//    BdStatusTask is responsible for sending User and Ventilator status
//    to the GUI via NetworkApps and is responsible for for sending
//    alarm status to the Alarm-Analysis subsystem.
//    XmitExhValveTableTask is responsible for transmitting exhalation
//    valve table to GUI CPU.
//---------------------------------------------------------------------
//@ Rationale
//    Tasks definition.
//---------------------------------------------------------------------
//@ Implementation-Description
//    Each task has its own initialization method and all tasks can not
//    be terminated.
//---------------------------------------------------------------------
//@ Fault-Handling
//    n/a.
//---------------------------------------------------------------------
//@ Restrictions
//    This is a satic class.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/BdTasks.ccv   25.0.4.1   20 Nov 2013 17:34:44   rhjimen  $
//
//@ Modification-Log
//
//  Revision: 021  By: rpr    Date:  6-Nov-2012    SCR Number: 6777
//  Project:  BI
//  Description:
//      Removed all monitor software.
//
//  Revision: 020  By: rhj    Date: 31-Mar-2011     SCR Number: 6436
//  Project:  PROX
//  Description:
//      Added code to ensure that PProxiInterface is not null.
//
//  Revision: 019   By: rpr   Date: 04-Oct-2010    	SCR Number: 6436
//  Project:  PROX
//  Description:
//      Augment the logging algorithm of extended cycles.
//
//  Revision: 018   By: rpr   Date: 28-Jun-2010         SCR Number: 6436
//  Project:  PROX
//  Description:
//      Never log the first extended cycles.  Augment the logging algorithm of
//      extended cycles.  Too many logs can occur and the queues fill.
//
//  Revision: 017   By: rhj   Date: 15-Jun-2010    	SCR Number: 6436
//  Project:  PROX
//  Description:
//      Added Prox newCycle method to run every 5 ms.
//
//  Revision: 016   By: gdc   Date:  18-Aug-2009    SCR Number: 6147
//  Project:  XB
//  Description:
//		Removed unnecessary and possibly problematic synchronization of
// 		BdMonitors when GUI-Ready Online message received. Removed
// 		test for vent-inop in BdExecutive that would skip cycles when
// 		vent-inop signal "bounced". The safety-net checks now test for
// 		vent-inop after failing its sensor tests. If the vent is inop,
// 		reports a backgrounf forced vent-inop instead of a failed
// 		vent component.
//
//  Revision: 015   By: rpr   Date:  07-May-2008    SCR Number: 6508
//  Project:  840SBUILD2
//  Description:
//		Backed out Masked Ethernet interrupts during the execution of
//		BdExecutiveTask fix.
//
//  Revision: 014   By: rpr   Date:  08-April-2008    SCR Number: 6446
//  Project:  840SBUILD2
//  Description:
//		Masked Ethernet interrupts during the execution of BdExecutiveTask.
//		This reduces the overhead of LAN activity during Bd's cycle.  There for
//		reducing the number of Bd Extended cycles.
//
//  Revision: 013   By: gdc   Date:  07-Aug-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//		Changed reporting mechanism for timing tests to use syslog
//		in another task. Only timing measurements taken in main task
//		instead of including logging overhead.
//
//  Revision: 012   By: yab   Date:  27-Feb-2002    DR Number: 5854
//  Project:  VTPC
//  Description:
//		VTPC project-related changes:
//		Do not log 5 msec extended main loop cycle for the very first
//      cycle of the beginning of each breath in the normal data key
//      mode (STANDARD option).
//
//  Revision: 011   By: syw   Date:  14-Sep-2000    DR Number: 5695
//  Project:  VTPC
//  Description:
//		VTPC project-related changes:
//		Added BD_GRAPHICS handle
//
//  Revision: 010  By: iv     Date:  19-May-1999    DR Number: 5397
//  Project:  ATC
//  Description:
//      In BdExecutiveTask(), added a counter to force vent inop only after
//      4 consequtive occurences.
//
//  Revision: 008  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 007  By:  yyy    Date:  01-Oct-1998    DCS Number:
//  Project:  BiLevel
//  Description:
//			Bilevel initial version.
//
//  Revision: 006  By: syw   Date:  02-Apr-1998    DR Number: 5048
//  	Project:  Sigma (840)
//		Description:
//			Removed old TIMING_TEST code and added new code to measure timing
//				based on HiResTimer class.
//
//  Revision: 005  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 004  By:   iv   Date:   28-Apr-1997    DR Number: DCS 1996
//       Project:  Sigma (840)
//       Description:
//          Added trace to SRS req. 00529, 00543
//
//  Revision: 003  By:  syw   Date:   08-Apr-1997    DR Number: DCS NONE
//       Project:  Sigma (840)
//       Description:
//          Deactivate safe state when BdExecutive starts per BD-IO-Devices
//				code inspection.
//
//  Revision: 002  By:  sp   Date:   27-Feb-1996    DR Number: DCS NONE
//       Project:  Sigma (840)
//       Description:
//             Code inspection.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: DCS NONE
//       Project:  Sigma (840)
//       Description:
//             Initial version
//
//=====================================================================

#include "BdTasks.hh"

//@ Usage-Classes
#include "BD_IO_Devices.hh"
#include "BreathMiscRefs.hh"
#include "ModeTriggerRefs.hh"
#include "MsgQueue.hh"
#include "CycleTimer.hh"
#include "TaskControlAgent.hh"
#include "BdQueuesMsg.hh"
#include "GuiReadyMessage.hh"
#include "BdReadyMessage.hh"
#include "ChangeStateMessage.hh"
#include "BdGuiCommSync.hh"
#include "BdAlarms.hh"
#include "SmTasks.hh"
#include "VentAndUserEventStatus.hh"
#include "ServiceMode.hh"
#include "ImmediateModeTrigger.hh"
#include "VentStatus.hh"
#include "TaskMonitor.hh"
#include "TaskMonitorQueueMsg.hh"
#include "NetworkMsgId.hh"
#include "BdMonitorRefs.hh"
#include "CalInfoDuplication.hh"
#include "SmRefs.hh"
#include "Background.hh"
#include "Fio2Monitor.hh"
#include "VentObjectRefs.hh"
#include "PatientDataMgr.hh"
#include "BitAccessGpioMediator.hh"
#include "RegisterRefs.hh"
#include "BdSystemStateHandler.hh"
#include"BdSignal.hh"
#include "BreathRecord.hh"
#include "SoftwareOptions.hh"
#include "ProxiInterface.hh"

#if defined (_WIN32_WCE)
#include <winbase.h>
#include <tchar.h>
#endif

#if defined( TIMING_TEST )
	#include "HiResTimer.hh"
#endif // defined( TIMING_TEST )
#define SAFETY_NET_READ_REG ((volatile unsigned char *) (0xFFBEB00C))
#define SERVICE_SWITCH_PRESSED_BIT (0x01)

// globals defined to support timing test - define TIMING_TEST to enable test
Uint32 CycleTimeUs;
Uint32 PeakCycle;
Uint32 AvgCycle;

//@ End-Usage
extern Boolean BdTimerEnabled;
extern ProxiInterface* PProxiInterface;

//@ Code...

///////////////////////////////////////////////////////////////////////////////
//    BD Constant Definition
///////////////////////////////////////////////////////////////////////////////

//@ Constant: REPORT_TIMEUP_MS
// Time in msec to report to TaskMonitor object.
const Int32 REPORT_TIMEUP_MS = 60;

// Initialization of static class members
Boolean BdTasks::IsSstRequired_ = FALSE;
Boolean BdTasks::IsServiceModeBackgroundEnabled_ = TRUE;

//=====================================================================
//
//  Public Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BdMainCycle
//
//@ Interface-Description
// This method takes no parameter and has no return value.
// This task is responsible to invoke all BD main cycle and secondary cycle
// activities.
//---------------------------------------------------------------------
//@ Implementation-Description
// This is BD's main cycle control loop. This task has to be executed
// at the beginning of the BD main cycle. All the details are
// being handles by CycleTimer::SignalNewCycle.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  The task has to be completed within BD cycle msec.
//  Return status of MsgQueue::PutMsg is Ipc::OK
//@ End-Method
//=====================================================================

void
BdTasks::BdMainCycle(void)
{
	static Int32 bd20MsTimer = SECONDARY_CYCLE_TIME_MS;

#if defined( TIMING_TEST )
	static HiResTimer timer ;

	static Boolean isFirstTime = TRUE;
	static Uint32 cycleSum = 0 ;
	static Uint32 counter = 0 ;

	if ( isFirstTime )
	{
		PeakCycle = 0;
		AvgCycle = 0;
		isFirstTime = FALSE;
	}

	// START TIMING
	timer.resetCounter() ;

#endif // defined( TIMING_TEST )

    CycleTimer::SignalNewCycle();                       // Do all the BD 5ms breath operations, including gathering analog data.

	bd20MsTimer += CYCLE_TIME_MS;

#if defined( TIMING_TEST )
	Uint32 count = timer.getElapsedCount() ;
	// TIMING DONE

	static Uint32 nsPerCount = timer.getNsPerCount() ;

	CycleTimeUs = count * nsPerCount / 1000 ;

	PeakCycle = MAX_VALUE(PeakCycle, CycleTimeUs);

	counter++ ;
	cycleSum += CycleTimeUs ;
	AvgCycle = cycleSum / counter;

	if ( RServiceSwitch.getState() == BinaryIndicator::ON )
	{
		counter = 0 ;
		cycleSum = 0 ;
		PeakCycle = 0 ;
	}

#endif // defined( TIMING_TEST )

	if ( bd20MsTimer >= SECONDARY_CYCLE_TIME_MS )
	{
		// $[TI1]
		BdQueuesMsg msgSend;
		msgSend.event.eventType = BdQueuesMsg::BD_SECONDARY_CYCLE_EVENT;
		
		Int32 result = MsgQueue::PutMsg(::TIMER_20MS_Q, msgSend.qWord);
		CLASS_ASSERTION(result == Ipc::OK);
		bd20MsTimer = 0;
	} // $[TI2]

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BdSecondaryTask
//
//@ Interface-Description
//
//      This method takes no parameter and has no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//
// This is BD Secondary Task control loop. This task has to be
// awaken by the BdExecutiveTask() once every 20MS. It wakes up, reports
// to TaskMonitor object when TASK_MONITOR_MSG is received.
// A call to CycleTimer::SignalNewSecondaryCycle() is made when
// BD_SECONDARY_CYCLE_EVENT is received.
// CycleTimer::SignalNewSecondaryCycle() handles all breath delivery details.
// At anytime, therecan be at most 2 messages on its queue,
// BD_SECONDARY_CYCLE_EVENT and TASK_MONITOR_MSG.  When 3 messages
// are detected, an error is logged.  When 4 or more messages are
// detected, this task asserts.
//---------------------------------------------------------------------
//@ PreCondition
// CLASS_ASSERTION ( msgCount <= 2 );
// Return status of MsgQueue::pendForMsg is Ipc::OK
// The event recieved has to be a TASK_MONITOR_MSG or
// BD_SECONDARY_CYCLE_EVENT message.
//---------------------------------------------------------------------
//@ PostCondition
//  The task has to be completed within 20 msec.
//@ End-Method
//=====================================================================

void
BdTasks::BdSecondaryTask( void )
{
	BdQueuesMsg msgRecieved;
	MsgQueue timerQue20Ms( ::TIMER_20MS_Q );
	Int32 qEvent, msgCount;
	Int32 status;

#if !defined( SIGMA_UNIT_TEST )
	timerQue20Ms.flush();
#endif // !defined ( SIGMA_UNIT_TEST )

	for(;;)
	{
		msgCount = timerQue20Ms.numMsgsAvail();

		if ( msgCount == 3 )
		{
			// $[TI1]
			Background::LogDiagnosticCodeUtil(::BK_BD_MULT_SECOND_CYCLE_MSGS);
			timerQue20Ms.acceptMsg( qEvent );
		}
		else
		{
			// $[TI2]
#if defined ( SIGMA_PRODUCTION )
			CLASS_ASSERTION ( msgCount <= 2 );
#else
			timerQue20Ms.flush();
#endif // defined ( SIGMA_PRODUCTION )
		}
		Int32 status = timerQue20Ms.pendForMsg( qEvent );
		CLASS_ASSERTION ( status == Ipc::OK );

		msgRecieved.qWord = qEvent;

		switch ( msgRecieved.event.eventType )
		{
			case TaskMonitorQueueMsg::TASK_MONITOR_MSG:
				// $[TI3]
				TaskMonitor::Report();
				break;

			case BdQueuesMsg::BD_SECONDARY_CYCLE_EVENT:
				// $[TI4]
				CycleTimer::SignalNewSecondaryCycle();
				break;

			default:
				// $[TI5]
				// invalid state
				CLASS_ASSERTION(FALSE);
		}

#if defined( SIGMA_UNIT_TEST )
		break;
#endif // defined ( SIGMA_UNIT_TEST )

	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BdExecutiveTask
//
//@ Interface-Description
//  $[00529] $[00543]
//  This method takes no parameter and has no return value.
//  This task is responsible for controlling the BD apps operational mode.
//  Operational mode may be one of the following: SERVICE, SST, VENT INOP, ONLINE.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This task has to be executed at the beginning of the BD main cycle. It determines
//  the operational mode and takes action accordingly. For Vent inop, the task
//  command the H/W to inop state. For SERVICE or SST, the task queues a message
//  on the service mode task, every 2 BD cycles. On transition to service mode,
//  safe state and audible alarm are activated. For ONLINE state, the task queues a
//  message on the main bd task every BD cycle, if SST Major fault is registered,
//  the trigger SstMajorRequestTrigger is enabled.
//---------------------------------------------------------------------
//@ PreCondition
//  Return status of MsgQueue::pendForMsg is Ipc::OK.
//  timerQue's msgCount < 2.
//  BdState and BdQueuesMsg are valid.
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
BdTasks::BdExecutiveTask(void)
{
	// TODO E600_LL: to be reviewed when service mode is ready
	// Boolean isServiceRequired = ServiceMode::IsServiceRequired() ;
   	Boolean isServiceRequired = FALSE;

	// the result of ServiceMode::IsSstRequired is store here
	// because the method accesses NOVRAM.
	// TODO E600_LL: to be reviewed when SST is ready
	//IsSstRequired_ = ServiceMode::IsSstRequired();
    IsSstRequired_ = FALSE;

	Uint32 toggle = 1;
	MsgQueue timerQue(::TIMER_5MS_Q);
	Int32 reportTime = 0;
	Uint32 initService = 1 ;
	Uint32 initOnline = 1 ;
	Boolean done = FALSE;
	Int32 qEvent;
	Int32 status = timerQue.pendForMsg(qEvent);
	CLASS_ASSERTION (status == Ipc::OK);

	BdQueuesMsg msg;

	Int32 msgRecv = 0;
	Int32 msgCount = 0;
	BdQueuesMsg bdMsg;

	// Set up callback from Sys-Init when all BD registered tasks
	// are ready.
	AppContext appContext;

	appContext.setCallback(BdTasks::BdChangeStateCallback);
	appContext.setCallback(BdTasks::BdExecBdReadyCallback);
	appContext.setCallback(BdTasks::GuiReadyCallback);

	// turn safe state off and force immediate update
	// TODO E600_LL: to be reviewed when VentStatus is ready
	// VentStatus::DirectVentStatusService( VentStatus::SVO_STATE, VentStatus::DEACTIVATE );
    RBitAccessGpio.newCycle() ;

	for(;;)
	{
		status = timerQue.pendForMsg(qEvent);
		CLASS_ASSERTION (status == Ipc::OK);

		msg.qWord = qEvent;

		msgRecv = 0;
		msgCount = 0;

		switch ( msg.event.eventType )
		{
			case BdQueuesMsg::TASK_CONTROL_EVENT:
				// $[TI1]
				// dispatchEvent will invoke the proper callback method
				appContext.dispatchEvent(qEvent);
				break;

				// BD Main task request to send ventilator or user event status
			case BdQueuesMsg::BD_MAIN_CYCLE_EVENT:
				// $[TI2]
				if ( reportTime >= REPORT_TIMEUP_MS )
				{ // $[TI2.1]
					reportTime = 0;
					TaskMonitor::Report();
				} // $[TI2.2]

				switch ( TaskControlAgent::GetBdState())
				{
					case (STATE_INOP):
						// $[TI2.3]
						VentStatus::DirectVentStatusService( VentStatus::VENT_INOP_STATE,
															 VentStatus::ACTIVATE );
                        RBitAccessGpio.newCycle() ;
						break;

					case (STATE_SST):
					case (STATE_SERVICE):
						// $[TI2.4]

						if ( initService )
						{ // $[TI2.4.1]
							initService = 0 ;
							VentStatus::DirectVentStatusService(VentStatus::BD_AUDIO_ALARM,
																VentStatus::DEACTIVATE);
						} // $[TI2.4.2]

						if ( toggle )
						{ // $[TI2.4.3]

							if ( IsServiceModeBackgroundEnabled() )
							{ // $[TI2.4.3.1]
								SmTasks::ServiceModeBackgroundCycle() ;
							}
							else
							{	// $[TI2.4.3.2]
                                RBitAccessGpio.newCycle() ;
							}
						} // $[TI2.4.4]

						toggle ^= 1;

						// Prox newCycle() must run every 5 ms.
						if ( IsServiceModeBackgroundEnabled() )
						{
                            if (PProxiInterface)
                            {
                                //Prox support
                                ProxiInterface& rProxiInterface = *PProxiInterface;
                                rProxiInterface.newCycle();
                            }
						}


						break;

					case (STATE_ONLINE):
						{
							// $[TI2.5]
							if ( initOnline )
							{ // $[TI2.5.1]

								initOnline = 0 ;
								if ( IsSstRequired_ ) // $[TI2.5.1.1]
								{
									VentStatus::DirectVentStatusService( VentStatus::BD_AUDIO_ALARM,
																		 VentStatus::ACTIVATE);
								}
								else if ( isServiceRequired ) // $[TI2.5.1.2]
								{
									VentStatus::DirectVentStatusService( VentStatus::VENT_INOP_STATE,
																		 VentStatus::ACTIVATE);
								}
								else // $[TI2.5.1.3]
								{
									VentStatus::DirectVentStatusService(VentStatus::BD_AUDIO_ALARM, VentStatus::DEACTIVATE);
								}
							}
							// $[TI2.5.2]

							BdMainCycle();
							done = FALSE;
//this code section does not work for WinCE because peekAtMsg() has to be implemented..
#if !defined(_WIN32_WCE)
#if defined(SIGMA_PRODUCTION) || defined(SIGMA_UNIT_TEST)
							// check for more than 1 BD_MAIN_CYCLE_EVENT
							// message on queue
							while ( (peekAtMsg(msgRecv) == Ipc::OK) && !done )
							{ // $[TI2.5.4.1]

								bdMsg.qWord = msgRecv;
								if ( bdMsg.event.eventType ==
									 BdQueuesMsg::BD_MAIN_CYCLE_EVENT )
								{ // $[TI2.5.4.1.1]
									msgCount++;
									timerQue.acceptMsg(msgRecv);
									// with more time being used up reduce the amount of BD extended cycles by not logging first one
									// this is the heavest used cycle and will likely trip a log entry
									// also schedule a log  every 10 extended cycles, 100, 1000 etc.
									// with aggressive settings and factor = 10 a log will be produced approximately every 7 minutes
									if ( BreathRecord::GetIntervalNumber() != 1)
									{
										static unsigned int numberOfExtenedLogs = 0;
										static unsigned int factor = 10;
										numberOfExtenedLogs++;
										if (numberOfExtenedLogs % factor == 0)
										{
											factor = factor * 10;
											Background::LogDiagnosticCodeUtil(::BK_BD_MULT_MAIN_CYCLE_MSGS, BreathRecord::GetIntervalNumber());
										}
									}
									CLASS_ASSERTION(msgCount < 2);
								}
								else
								{ // $[TI2.5.4.1.2]
									done = TRUE;
								}

							} // $[TI2.5.4.2]
#endif // defined(SIGMA_PRODUCTION) || defined(SIGMA_UNIT_TEST)
#endif //!defined(_WIN32_WCE)
						}
						break;

					case (STATE_UNKNOWN):
					case (STATE_TIMEOUT):
					case (STATE_INIT):
					case (STATE_START):
						// $[TI2.6]
						// do nothing
						break;

					default:
						// $[TI2.7]
						// invalid state
						CLASS_ASSERTION(FALSE);
						break;
				};
				reportTime += CYCLE_TIME_MS;
				break;

			default:
				// $[TI3]
				CLASS_ASSERTION(FALSE);
				break;
		}
#if defined(SIGMA_UNIT_TEST)
		break;
#endif    // defined(SIGMA_UNIT_TEST)
	}

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BdStatusTask
//
//@ Interface-Description
//      This method is the entry point for the BdStatusTask.  This method
//      takes no input and returns nothing.  The BdStatusTask is an
//      event driven task that is pending for something to be
//      put on its BD_STATUS_TASK_Q.
//
//      The task is responsible for coordinating the reporting of
//      ventilator and user event status to the GUI and for reporting
//      BD Alarm status to the Alarm-Analysis subsystem.
//
//      Task control messages are placed on the BD_STATUS_TASK_Q
//      when there is a change in communication status
//    between the GUI and the BD.
//
//      Event and alarm status messages are placed on the BD_STATUS_TASK_Q
//      by the BD Main task.  Depending on the status of communciations,
//      the BD Status task determines whether or not to send the message
//      on.  When communications are established after being down, this
//      task is responsible for determining the status of alarms and user
//      and ventilator status and passing that information on to the GUI
//      Alarm-Analysis.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method sets up callbacks with AppContext for the GuiReady and
//      CommDown messages.  It then enters an infinite loop where
//      it is pending on the BD_STATUS_TASK_Q.  Once a message
//      is received on the queue, this task invokes the appropriate method
//      to process the message.
//---------------------------------------------------------------------
//@ PreCondition
//  Return status of MsgQueue::pendForMsg is Ipc::OK.
//  The event type message received is valid.
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================

void
BdTasks::BdStatusTask(void)
{
	// Handle to the BD_STATUS_TASK_Q
	MsgQueue msgQueue(BD_STATUS_TASK_Q);

	// Variables used to handle the message
	Int32 qEvent;
	Int32 status;
	Real32 calO2Delivered;
	BdQueuesMsg msg;

	AppContext appContext;

	// Set up callbacks for calls from Sys-Init when communications breaks
	// between the BD and the GUI
	appContext.setCallback(BdGuiCommSync::CommDownCallback);

	// Set up callbacks for calls from Sys-Init when Gui is ready to receive
	// application level communication
	appContext.setCallback(BdGuiCommSync::GuiReadyCallback);

	// register a callback to synch this task with the bd exec task
	appContext.setCallback(BdTasks::BdChangeStateCallback);

	for(;;)
	{
		// Block until a message is received on the queue
		status = msgQueue.pendForMsg(qEvent);
		CLASS_ASSERTION (status == Ipc::OK);

		// Put the event data into the BdQueuesMsg
		msg.qWord = qEvent;

		switch ( msg.event.eventType )
		{
			// Change in communcations status
			case BdQueuesMsg::TASK_CONTROL_EVENT:
				// $[TI1]

				// dispatchEvent will invoke the proper callback method
				appContext.dispatchEvent(qEvent);
				break;

				// BD Main task request to send ventilator or user event status
			case BdQueuesMsg::BD_EVENT_STATUS_EVENT:
				// $[TI2]

				if ( (TaskControlAgent::GetBdState() == (SigmaState) STATE_ONLINE) ||
					 (TaskControlAgent::GetBdState() == (SigmaState) STATE_INOP) )
				{
					// $[TI2.1]
					// Send the status to the GUI
					VentAndUserEventStatus::SendStatusToGui(
														   (EventData::EventId) msg.eventStatusEvent.id,
														   (EventData::EventStatus) msg.eventStatusEvent.status,
														   (EventData::EventPrompt) msg.eventStatusEvent.prompt );
				}
				// $[TI2.2]
				break;

				// BD Main task request to send BD Alarm information
			case BdQueuesMsg::BD_ALARM_EVENT:
				// $[TI3]

				if ( (TaskControlAgent::GetBdState() == (SigmaState) STATE_ONLINE) ||
					 (TaskControlAgent::GetBdState() == (SigmaState) STATE_INOP) )
				{
					// $[TI3.1]
					// Send alarm information to the Alarm-Analysis subsystem
					RBdAlarms.sendStatusToAlarmAnalysis(
													   (BdAlarmId::BdAlarmIdType) msg.bdAlarmEvent.id);
				}
				// $[TI3.2]
				break;

				// Task Monitor message
			case TaskMonitorQueueMsg::TASK_MONITOR_MSG:
				// $[TI4]
				TaskMonitor::Report();
				break;

			case BdQueuesMsg::BD_GRAPHICS:
				// $[TI8]
				BdSignal::Send();
				break;

			case BdQueuesMsg::BD_FIO2_CYCLE_EVENT:
				// $[TI5]
				if ( STATE_ONLINE == TaskControlAgent::GetBdState() )
				{
					// $[TI5.1]
					calO2Delivered = RFio2Monitor.getCurrentCalculatedO2Percent();
					PatientDataMgr::NewFiO2Data( calO2Delivered );
				}
				// $[TI5.2]
				break;

			case BdQueuesMsg::BD_NOVRAM_UPDATE_EVENT:
				// $[TI7]
				BdSystemStateHandler::UpdateNovRam();
				break;

			default:
				// $[TI6]
				// Invalid event type
				CLASS_ASSERTION( FALSE );
		}

#if defined( SIGMA_UNIT_TEST)
		break;
#endif    // defined( SIGMA_UNIT_TEST)
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: IsServiceModeBackgroundEnabled
//
//@ Interface-Description
//      This method has no arguments and returns a Boolean.
//      It determines whether or not the service mode background task
//      will execute.
//      This method is not inline because it is externally defined in
//	BD-IO subsystem.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method returns the member IsServiceModeBackgroundEnabled_
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

Boolean
BdTasks::IsServiceModeBackgroundEnabled( void )
{
	// $[TI1]
	CALL_TRACE("BdTasks::IsServiceModeBackgroundEnabled( void )");

	return(IsServiceModeBackgroundEnabled_);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SetServiceModeBackgroundEnabled
//
//@ Interface-Description
//      This method has a Boolean argument and has no returned value.
//      It sets a flag that determines whether or not the service mode
//      background task will run.
//      This method is not inline because it is externally defined in
//	BD-IO subsystem.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method sets the member IsServiceModeBackgroundEnabled_ to the
//      passed argument.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
BdTasks::SetServiceModeBackgroundEnabled( const Boolean enableState )
{
	// $[TI1]
	CALL_TRACE("BdTasks::SetServiceModeBackgroundEnabled( Boolean enableState )");

	IsServiceModeBackgroundEnabled_ = enableState;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: XmitFlashTableTask
//
//@ Interface-Description
//    This method takes no parameter and has no return value.
//    This task is responsible for communicating the exhalation valve
//    table over to the GUI CPU.
//---------------------------------------------------------------------
//@ Implementation-Description
//    Once a message is posted to the XMIT_EV_TABLE_TASK_Q, the
//    table is then sent.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//@ End-Method
//=====================================================================

void
BdTasks::XmitFlashTableTask( void)
{
	CALL_TRACE("BdTasks::XmitFlashTableTask( void)") ;

	Int32 status ;
	Int32 msgPend ;

	MsgQueue XmitEvTableMsgQue(::XMIT_EV_TABLE_TASK_Q) ;

	for(;;)
	{ // $[TI1]
		status = XmitEvTableMsgQue.pendForMsg(msgPend) ;
		CLASS_ASSERTION (status == Ipc::OK) ;

#ifdef SIGMA_UNIT_TEST
		break;
#else
		RCalInfoDuplication.sendCalInfoFlashData() ;

#endif // SIGMA_UNIT_TEST
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BdExecBdReadyCallback
//
//@ Interface-Description
//    This static method takes a constant reference BdReadyMessage as an
//    input and returns nothing.  It is invoked when the BdReadyMessage
//    is received by the BD task that was registered for it.
//    The current BD state is determined by invoking the getState() method
//    of BdReadyMessage.
//    If the current Bd State is not START or TIME_OUT, the BD cycle timer
//    is enabled.
//---------------------------------------------------------------------
//@ Implementation-Description
//    A switch is performed on the current Bd state.
//    If the current Bd State is not START or TIME_OUT, the BD cycle timer
//    is enabled.
//---------------------------------------------------------------------
//@ PreCondition
//    BD State is valid
//---------------------------------------------------------------------
//@ PostCondition
//    None
//@ End-Method
//=====================================================================
void
BdTasks::BdExecBdReadyCallback(const BdReadyMessage& rMessage)
{
	CALL_TRACE("BdTasks::BdExecBdReadyCallback(const BdReadyMessage& rMessage)");

	switch ( rMessage.getState() )
	{
		case STATE_ONLINE:
		case STATE_INOP:
		case STATE_SERVICE:
		case STATE_SST:
			// $[TI1]
			// synchronize with 5ms timer
			BdTimerEnabled = TRUE;
			break;

		case STATE_START:
		case STATE_TIMEOUT:
			// $[TI2]
			// Do nothing in these states
			break;

		default:
			// $[TI3]
			// Invalid state
			AUX_CLASS_ASSERTION_FAILURE(rMessage.getState())
			break;
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BdChangeStateCallback
//
//@ Interface-Description
//    This static method takes a const reference ChangeStateMessage as an
//    input and returns nothing.  It is invoked when the ChangeStateMessage
//    is received by the BD task that registered for it.
//    The current BD state is determined by invoking the getState() method
//    of ChangeStateMessage.
//    If the current Bd State is not START or TIME_OUT, the current state
//    is reported back to Sys-Init.
//---------------------------------------------------------------------
//@ Implementation-Description
//    A switch is performed on the current Bd state.
//    If the current Bd State is not START or TIME_OUT, the current state
//    is reported back to Sys-Init.
//---------------------------------------------------------------------
//@ PreCondition
//    BD State is valid
//---------------------------------------------------------------------
//@ PostCondition
//    None
//@ End-Method
//=====================================================================
void
BdTasks::BdChangeStateCallback(const ChangeStateMessage& rMessage)
{
	CALL_TRACE("BdTasks::BdChangeStateCallback(const ChangeStateMessage& rMessage)");

	switch ( rMessage.getState() )
	{
		case STATE_ONLINE:
		case STATE_INOP:
		case STATE_SERVICE:
		case STATE_SST:
			// $[TI1]
			TaskControlAgent::ReportTaskReady(rMessage);
			break;

		case STATE_START:
		case STATE_TIMEOUT:
			// $[TI2]
			// Do nothing in these states
			break;

		default:
			// $[TI3]
			// Invalid state
			AUX_CLASS_ASSERTION_FAILURE(rMessage.getState())
			break;
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GuiReadyCallback
//
//@ Interface-Description
//    This static method takes a reference to a GuiReadyMessage as an
//    input and returns nothing.  It is invoked when the GuiReadyMessage
//    is received by the BD task that registered for it.
//    The current GUI state is determined by invoking the getState() method
//    of GuiReadyMessage.
//    If the current GUI state is STATE_INOP, a request is made to VentStatus
//    object to acitvate BD audio alarm and loss of GUI LED.
//---------------------------------------------------------------------
//@ Implementation-Description
//    A switch is performed on the current Gui state.
//    If the current GUI state is STATE_INOP, a request is made to VentStatus
//    object to acitvate BD audio alarm and loss of GUI LED.
//---------------------------------------------------------------------
//@ PreCondition
//    GUI State is valid
//---------------------------------------------------------------------
//@ PostCondition
//    None
//@ End-Method
//=====================================================================
void
BdTasks::GuiReadyCallback(const GuiReadyMessage& rMessage)
{
	CALL_TRACE("BdTasks::GuiReadyCallback(const GuiReadyMessage& rMessage)");

	switch ( rMessage.getState() )
	{
		case STATE_INOP:
			// $[TI2]
			VentStatus::LatchVentStatusService(VentStatus::BD_AUDIO_ALARM,
											   VentStatus::ACTIVATE);
			VentStatus::LatchVentStatusService(VentStatus::LOSS_OF_GUI_LED,
											   VentStatus::ACTIVATE);
			break ;

		case STATE_ONLINE:
		case STATE_INIT:
		case STATE_SERVICE:
		case STATE_START:
		case STATE_TIMEOUT:
		case STATE_SST:
			// $[TI3]
			// Do nothing in these states
			break;

		default:
			// $[TI4]
			// Invalid state
			AUX_CLASS_ASSERTION_FAILURE(rMessage.getState())
			break;
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
BdTasks::SoftFault(const SoftFaultID  softFaultID,
				   const Uint32       lineNumber,
				   const char*        pFileName,
				   const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, BDTASKS,
							lineNumber, pFileName, pPredicate);
}



//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//=====================================================================
//
//  Private Methods...
//
//=====================================================================


