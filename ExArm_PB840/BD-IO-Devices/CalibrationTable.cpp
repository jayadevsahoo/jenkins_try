#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
//  Class: CalibrationTable_t - The implementation for CalibrationTable_t class.

#include "CalibrationTable.h"
#include "crc.h"
#include "ServosGlobal.h"
#include "ValveRefs.hh"
#include "Psol.hh"

extern AioDioDriver_t AioDioDriver;

const InspValveTable_t AirDefaultFlowTab =
{
	//	float FlowTable[INSP_VALVE_SUBTABLE_SIZE]
{0.00, 1.01, 2.02, 3.00, 4.04, 5.02, 5.99, 6.99, 8.03, 9.00,
10.08, 11.06, 11.98, 13.05, 14.06, 15.00, 16.05, 17.06, 17.96, 19.12,
20.06, 25.04, 30.15, 35.00, 39.97, 44.96, 49.98, 55.08, 60.09, 65.06,
70.07, 75.25, 80.05, 84.78, 89.97, 94.92, 100.00, 104.66, 110.04, 115.28,
119.96, 124.66, 129.79, 135.15, 139.66, 144.62, 149.78, 154.94, 159.52, 165.62,
170.01, 174.74, 180.20, 184.15, 188.57, 194.48, 199.85 },

	//	float DacTable[INSP_VALVE_SUBTABLE_SIZE]
{821, 940, 972, 1001, 1027, 1050, 1071, 1093, 1114, 1132,
1151, 1169, 1184, 1203, 1220, 1238, 1256, 1273, 1289, 1307,
1323, 1401, 1476, 1549, 1621, 1690, 1761, 1839, 1909, 1966,
2034, 2097, 2152, 2211, 2273, 2328, 2385, 2435, 2511, 2571,
2626, 2677, 2736, 2789, 2840, 2884, 2941, 2990, 3035, 3093,
3135, 3183, 3240, 3277, 3314, 3375, 3442 }
};

const InspValveTable_t O2DefaultFlowTab =
{
	//	float FlowTable[INSP_VALVE_SUBTABLE_SIZE]
{0.00, 1.02, 2.03, 3.03, 4.00, 5.04, 6.07, 7.04, 8.05, 9.07,
10.00, 11.00, 12.03, 13.03, 14.04, 15.02, 16.01, 17.04, 17.97, 19.09,
20.04, 25.20, 30.17, 35.08, 40.07, 44.97, 49.87, 55.21, 59.93, 65.07,
69.93, 75.06, 79.85, 84.99, 89.92, 94.80, 99.75, 105.23, 109.77, 114.84,
119.83, 125.37, 129.40, 134.45, 140.22, 144.60, 150.22, 154.72, 159.87, 164.86,
170.02, 175.36, 179.30, 184.67, 189.24, 194.87, 198.36 },

	//	float DacTable[INSP_VALVE_SUBTABLE_SIZE]
{857, 959, 995, 1025, 1051, 1078, 1101, 1124, 1146, 1167,
1187, 1205, 1225, 1243, 1262, 1281, 1297, 1316, 1333, 1353,
1369, 1455, 1532, 1608, 1695, 1769, 1845, 1923, 1996, 2059,
2124, 2188, 2248, 2307, 2370, 2426, 2499, 2560, 2610, 2672,
2729, 2786, 2838, 2890, 2957, 3001, 3061, 3111, 3160, 3213,
3260, 3313, 3359, 3406, 3450, 3510, 3548 }
};


static const ExhValveTable_t DefaultExhValveTab =
{
	//float PdrvTable[EXH_VALVE_SUBTABLE_SIZE] =
{ 0.49, 1.38, 2.28, 3.26, 4.26, 5.22, 6.09, 6.93, 7.80, 8.64, 
9.60, 10.44, 11.34, 12.10, 13.00, 13.88, 14.83, 15.57, 16.46, 17.22, 
18.12, 18.90, 19.70, 20.62, 21.41, 22.46, 23.23, 24.12, 24.78, 25.73, 
26.61, 27.28, 28.37, 29.12, 29.94, 30.76, 31.54, 32.28, 33.41, 34.06, 
34.89, 35.84, 36.74, 37.34, 38.37, 39.28, 39.91, 40.76, 41.56, 42.50, 
43.36, 44.21, 45.33, 45.98, 46.58, 47.51, 48.36, 49.43, 50.23, 50.90,
34.89, 35.84, 36.74, 37.34, 38.37, 39.28, 39.91, 40.76, 41.56, 42.50, 
43.36, 44.21, 45.33, 45.98, 46.58, 47.51, 48.36, 49.43, 50.23, 50.90,
34.89, 35.84, 36.74, 37.34, 38.37, 39.28, 39.91, 40.76, 41.56, 42.50, 
43.36, 44.21, 45.33, 45.98, 46.58, 47.51, 48.36, 49.43, 50.23, 50.90,
69.13 },

	//float P2Table[EXH_VALVE_SUBTABLE_SIZE] =
{ 0.30, 0.98, 1.99, 3.00, 3.98, 4.98, 5.99, 6.95, 7.96, 8.87, 
9.98, 10.92, 11.98, 12.91, 13.94, 14.95, 15.94, 16.93, 18.00, 18.91, 
19.75, 20.95, 21.85, 22.91, 23.96, 24.93, 25.85, 26.78, 27.90, 28.96, 
29.93, 30.93, 31.91, 32.99, 33.78, 34.97, 35.93, 36.76, 37.99, 38.73, 
39.99, 40.94, 41.93, 42.75, 43.58, 44.73, 45.88, 46.94, 47.70, 48.85, 
49.88, 50.96, 51.93, 52.83, 53.39, 54.95, 55.88, 56.94, 57.98, 58.84, 
39.99, 40.94, 41.93, 42.75, 43.58, 44.73, 45.88, 46.94, 47.70, 48.85, 
49.88, 50.96, 51.93, 52.83, 53.39, 54.95, 55.88, 56.94, 57.98, 58.84, 
39.99, 40.94, 41.93, 42.75, 43.58, 44.73, 45.88, 46.94, 47.70, 48.85, 
49.88, 50.96, 51.93, 52.83, 53.39, 54.95, 55.88, 56.94, 57.98, 58.84, 
79.96 },

	//float DacTable[EXH_VALVE_SUBTABLE_SIZE] =
{ 1328, 1361, 1387, 1411, 1433, 1452, 1468, 1482, 1496, 1509,  
1523, 1535, 1547, 1557, 1568, 1579, 1590, 1598, 1608, 1616, 
1626, 1634, 1642, 1651, 1659, 1669, 1677, 1685, 1691, 1700, 
1708, 1714, 1723, 1730, 1737, 1744, 1751, 1757, 1767, 1772, 
1779, 1787, 1795, 1800, 1809, 1816, 1821, 1828, 1835, 1843, 
1851, 1857, 1866, 1872, 1877, 1885, 1892, 1901, 1908, 1913, 
1779, 1787, 1795, 1800, 1809, 1816, 1821, 1828, 1835, 1843, 
1851, 1857, 1866, 1872, 1877, 1885, 1892, 1901, 1908, 1913, 
1779, 1787, 1795, 1800, 1809, 1816, 1821, 1828, 1835, 1843, 
1851, 1857, 1866, 1872, 1877, 1885, 1892, 1901, 1908, 1913, 
2105 },

//float PdrvTable_l2h[EXH_VALVE_SUBTABLE_SIZE] =
{ 0.49, 1.38, 2.28, 3.26, 4.26, 5.22, 6.09, 6.93, 7.80, 8.64, 
9.60, 10.44, 11.34, 12.10, 13.00, 13.88, 14.83, 15.57, 16.46, 17.22, 
18.12, 18.90, 19.70, 20.62, 21.41, 22.46, 23.23, 24.12, 24.78, 25.73, 
26.61, 27.28, 28.37, 29.12, 29.94, 30.76, 31.54, 32.28, 33.41, 34.06, 
34.89, 35.84, 36.74, 37.34, 38.37, 39.28, 39.91, 40.76, 41.56, 42.50, 
43.36, 44.21, 45.33, 45.98, 46.58, 47.51, 48.36, 49.43, 50.23, 50.90, 
34.89, 35.84, 36.74, 37.34, 38.37, 39.28, 39.91, 40.76, 41.56, 42.50, 
43.36, 44.21, 45.33, 45.98, 46.58, 47.51, 48.36, 49.43, 50.23, 50.90, 
34.89, 35.84, 36.74, 37.34, 38.37, 39.28, 39.91, 40.76, 41.56, 42.50, 
43.36, 44.21, 45.33, 45.98, 46.58, 47.51, 48.36, 49.43, 50.23, 50.90, 
69.13 },

	//float P2Table_l2h[EXH_VALVE_SUBTABLE_SIZE] =
{ 0.30, 0.98, 1.99, 3.00, 3.98, 4.98, 5.99, 6.95, 7.96, 8.87, 
9.98, 10.92, 11.98, 12.91, 13.94, 14.95, 15.94, 16.93, 18.00, 18.91, 
19.75, 20.95, 21.85, 22.91, 23.96, 24.93, 25.85, 26.78, 27.90, 28.96, 
29.93, 30.93, 31.91, 32.99, 33.78, 34.97, 35.93, 36.76, 37.99, 38.73, 
39.99, 40.94, 41.93, 42.75, 43.58, 44.73, 45.88, 46.94, 47.70, 48.85, 
49.88, 50.96, 51.93, 52.83, 53.39, 54.95, 55.88, 56.94, 57.98, 58.84,
39.99, 40.94, 41.93, 42.75, 43.58, 44.73, 45.88, 46.94, 47.70, 48.85, 
49.88, 50.96, 51.93, 52.83, 53.39, 54.95, 55.88, 56.94, 57.98, 58.84,
39.99, 40.94, 41.93, 42.75, 43.58, 44.73, 45.88, 46.94, 47.70, 48.85, 
49.88, 50.96, 51.93, 52.83, 53.39, 54.95, 55.88, 56.94, 57.98, 58.84,
79.96 },

	//float DacTable_l2h[EXH_VALVE_SUBTABLE_SIZE] =
{ 1328, 1361, 1387, 1411, 1433, 1452, 1468, 1482, 1496, 1509, 
1523, 1535, 1547, 1557, 1568, 1579, 1590, 1598, 1608, 1616, 
1626, 1634, 1642, 1651, 1659, 1669, 1677, 1685, 1691, 1700, 
1708, 1714, 1723, 1730, 1737, 1744, 1751, 1757, 1767, 1772, 
1779, 1787, 1795, 1800, 1809, 1816, 1821, 1828, 1835, 1843, 
1851, 1857, 1866, 1872, 1877, 1885, 1892, 1901, 1908, 1913, 
1779, 1787, 1795, 1800, 1809, 1816, 1821, 1828, 1835, 1843, 
1851, 1857, 1866, 1872, 1877, 1885, 1892, 1901, 1908, 1913, 
1779, 1787, 1795, 1800, 1809, 1816, 1821, 1828, 1835, 1843, 
1851, 1857, 1866, 1872, 1877, 1885, 1892, 1901, 1908, 1913, 
2105 }
};


static const ExhSensorTable_t DefaultFexTab =
{
	//Flow_t DefaultFexFlowTab[EXH_FLOW_SUBTABLE_SIZE] =
{0.0, 0.11, 0.22, 0.32, 0.42, 0.50, 0.62, 0.72, 0.81, 0.92, 
1.03, 1.12, 1.23, 1.32, 1.41, 1.54, 1.63, 1.73, 1.83, 1.93, 
2.04, 2.14, 2.24, 2.31, 2.42, 2.52, 2.63, 2.73, 2.85, 2.92, 
3.03, 3.14, 3.21, 3.33, 3.41, 3.53, 3.61, 3.74, 3.80, 3.91, 
4.05, 4.13, 4.23, 4.31, 4.40, 4.53, 4.62, 4.75, 4.84, 4.94, 
5.06, 5.15, 5.26, 5.30, 5.44, 5.49, 5.64, 5.73, 5.85, 5.98, 
6.07, 6.18, 6.26, 6.39, 6.48, 6.51, 6.61, 6.77, 6.85, 6.92, 
7.04, 7.19, 7.29, 7.42, 7.45, 7.56, 7.66, 7.72, 7.84, 7.98, 
8.04, 8.14, 8.26, 8.33, 8.47, 8.55, 8.61, 8.68, 8.84, 8.94, 
9.13, 9.16, 9.23, 9.36, 9.56, 9.58, 9.66, 9.83, 9.90, 10.04, 
10.33, 12.30, 14.33, 16.16, 18.27, 20.22, 22.14, 24.42, 26.39, 28.22, 
30.38, 32.13, 34.23, 36.37, 38.14, 40.19, 42.36, 44.12, 46.25, 48.38, 
50.57, 54.85, 60.36, 65.69, 70.91, 76.19, 80.70, 85.31, 90.77, 95.47, 
100.90, 105.89, 109.82, 116.60, 121.97, 125.71, 130.70, 135.90, 140.29, 145.63, 
150.40, 153.86, 160.12, 165.64, 168.65, 176.20, 180.86, 186.81, 191.44, 195.46, 
201.96 },

	//uint16_t DefaultFexDacTab[EXH_FLOW_SUBTABLE_SIZE] =
{ 48, 81, 94, 106, 115, 124, 134, 141, 148, 154, 
160, 166, 175, 179, 182, 187, 191, 195, 199, 203, 
207, 211, 215, 217, 221, 224, 228, 232, 235, 237, 
240, 244, 246, 249, 252, 255, 257, 260, 262, 264, 
268, 270, 271, 273, 275, 278, 279, 282, 283, 285, 
288, 290, 293, 293, 296, 296, 300, 300, 303, 305, 
307, 309, 310, 313, 313, 316, 316, 318, 320, 322, 
323, 326, 328, 329, 331, 331, 334, 334, 336, 338, 
339, 341, 342, 343, 345, 346, 347, 349, 350, 351, 
355, 355, 356, 357, 359, 360, 361, 362, 365, 365, 
369, 392, 414, 431, 452, 466, 483, 499, 514, 524, 
539, 549, 562, 573, 583, 594, 604, 614, 624, 634, 
645, 663, 685, 699, 719, 735, 749, 761, 776, 787, 
801, 812, 821, 836, 848, 856, 866, 876, 883, 893, 
902, 911, 923, 932, 936, 948, 956, 965, 973, 979, 
989}
};


const CircuitTable_t DefaultCircuitTab = 
{
//float FlowTable[CIRCUIT_SUBTABLE_SIZE]
	//{0.00, 6.11, 21.34, 31.24, 41.26, 50.98, 60.48, 80.86},
	{0.00, 5.84, 20.69, 30.14, 39.75, 49.25, 58.15, 77.40},
//float DP1Table[CIRCUIT_SUBTABLE_SIZE]
	//{0.00, 0.01, 0.16, 0.27, 0.46, 0.73, 1.12, 1.57},
	{0.00, 1.53, 1.65, 1.73, 1.90, 2.17, 2.35, 2.96},
//float DP2Table[CIRCUIT_SUBTABLE_SIZE]
	//{0.00, 0.03, 0.17, 0.38, 0.57, 0.79, 1.08, 2.11},
	{0.00, 0.00, 0.11, 0.27, 0.40, 0.57, 0.96, 1.55},
	1.13,	// Circuit compliance	
	2.0,	// Insp. circuit resistance
	2.0		// Exh. circuit resistance
};

// Default values for Manifold board
#define P1_OFF_COUNT	0
#define P2_OFF_COUNT	0
#define PD_OFF_COUNT	0

const TransducerTable_t DefaultP1Tab = {0x708+P1_OFF_COUNT, 0xa2d+P1_OFF_COUNT};
const TransducerTable_t DefaultP2Tab = {0x701+P2_OFF_COUNT, 0xa23+P2_OFF_COUNT};
const TransducerTable_t DefaultPdrvTab = {8+PD_OFF_COUNT, 0x5f9+PD_OFF_COUNT};
const TransducerTable_t DefaultPAirTab = {0, 4000};
const TransducerTable_t DefaultPO2Tab = {0, 4000};
const TransducerTable_t DefaultPSuplyTab = {0x10, 0xa04};
const O2SensorTable_t DefaultO2SensorTab  = {1000, 4000};


#define	STRINGID_P1				_T("P1")
#define	STRINGID_P2				_T("P2")
#define	STRINGID_PDRIVE			_T("PDRIVE")
#define	STRINGID_AIRVALVE		_T("AIR_VALVE")
#define	STRINGID_O2VALVE		_T("O2_VALVE")
#define	STRINGID_EXDRIVE		_T("EX_DRIVE")
#define	STRINGID_EXFLOW			_T("EX_FLOW")
#define	STRINGID_O2SENSOR		_T("O2_SENSOR")
#define	STRINGID_CIRCUIT		_T("CIRCUIT")
#define	STRINGID_AIRPSI			_T("AIR_PSI")
#define	STRINGID_O2PSI			_T("O2_PSI")
#define	STRINGID_SUPPLY		    _T("P_SUPPLY")

// Manifold data are on the EEPROM of manifold board
#define P1_TABLE_FNAME			_T("\\FlashFX Disk\\ProgramFiles\\P1Cal.Tbl")        /*  Manifold */
#define P2_TABLE_FNAME			_T("\\FlashFX Disk\\ProgramFiles\\P2Cal.Tbl")        /*  Manifold */
#define PDRIVE_TABLE_FNAME		_T("\\FlashFX Disk\\ProgramFiles\\PdriveCal.Tbl")    /*  Manifold */
#define AIR_VALVE_TABLE_FNAME	_T("\\FlashFX Disk\\ProgramFiles\\AirValveCal.Tbl")
#define O2_VALVE_TABLE_FNAME	_T("\\FlashFX Disk\\ProgramFiles\\O2ValveCal.Tbl")
#define EXH_VALVE_TABLE_FNAME	_T("\\FlashFX Disk\\ProgramFiles\\ExhValveCal.Tbl")
#define EXH_FLOW_TABLE_FNAME	_T("\\FlashFX Disk\\ProgramFiles\\ExhFlowCal.Tbl")   
#define O2_SENSOR_TABLE_FNAME	_T("\\FlashFX Disk\\ProgramFiles\\O2SensorCal.Tbl") 
#define CIRCUIT_TABLE_FNAME		_T("\\FlashFX Disk\\ProgramFiles\\CircuitCal.Tbl")
#define PAIR_TABLE_FNAME		_T("\\FlashFX Disk\\ProgramFiles\\AirPSICal.Tbl")    /*  Manifold */
#define PO2_TABLE_FNAME			_T("\\FlashFX Disk\\ProgramFiles\\O2PSICal.Tbl")     /*  Manifold */
#define PSUPLY_TABLE_FNAME		_T("\\FlashFX Disk\\ProgramFiles\\PsuplyPSICal.Tbl") /*  Manifold */

const CalibrationInfo_t DefaultCalibrationInfo[NUM_CAL_IDS] =
{
	{CAL_ID_P1, STRINGID_P1, P1_TABLE_FNAME, 2, P1_CAL_TABLE_SIZE, (float *)&DefaultP1Tab, SPI_FLASH_STORAGE, P1_CAL_INDEX},
	{CAL_ID_P2, STRINGID_P2, P2_TABLE_FNAME, 2, P2_CAL_TABLE_SIZE, (float *)&DefaultP2Tab, SPI_FLASH_STORAGE, P2_CAL_INDEX},
	{CAL_ID_PDRIVE, STRINGID_PDRIVE, PDRIVE_TABLE_FNAME, 2, PDRIVE_CAL_TABLE_SIZE, (float *)&DefaultPdrvTab, SPI_FLASH_STORAGE, PDRIVE_CAL_INDEX},
	{CAL_ID_PAIR, STRINGID_AIRPSI, PAIR_TABLE_FNAME, 1, PAIR_CAL_TABLE_SIZE, (float *)&DefaultPAirTab, SPI_FLASH_STORAGE, PAIR_CAL_INDEX},
	{CAL_ID_PO2, STRINGID_O2PSI, PO2_TABLE_FNAME, 1, PO2_CAL_TABLE_SIZE, (float *)&DefaultPO2Tab, SPI_FLASH_STORAGE, PO2_CAL_INDEX},
	{CAL_ID_AIR_VALVE, STRINGID_AIRVALVE, AIR_VALVE_TABLE_FNAME, 1, INSP_VALVE_TABLE_SIZE, (float *)&AirDefaultFlowTab, MAIN_FLASH_STORAGE, 0},
	{CAL_ID_O2_VALVE, STRINGID_O2VALVE, O2_VALVE_TABLE_FNAME, 1, INSP_VALVE_TABLE_SIZE, (float *)&O2DefaultFlowTab, MAIN_FLASH_STORAGE, 0},
	// TODO E600 MS. Remove when the fate of the old e600 calibration is decided.
	//{CAL_ID_EXH_VALVE, STRINGID_EXDRIVE, EXH_VALVE_TABLE_FNAME, 1, EXP_VALVE_TABLE_SIZE, (float *)&DefaultExhValveTab, MAIN_FLASH_STORAGE, 0},
	{CAL_ID_EXH_FLOW_SENSOR, STRINGID_EXFLOW, EXH_FLOW_TABLE_FNAME, 1, EXH_FLOW_TABLE_SIZE, (float *)&DefaultFexTab, MAIN_FLASH_STORAGE, 0},
	{CAL_ID_O2_SENSOR, STRINGID_O2SENSOR, O2_SENSOR_TABLE_FNAME, 1, O2_SENSOR_TABLE_SIZE, (float *)&DefaultO2SensorTab, MAIN_FLASH_STORAGE, 0},
	{CAL_ID_CIRCUIT, STRINGID_CIRCUIT, CIRCUIT_TABLE_FNAME, 2, CIRCUIT_TABLE_SIZE, (float *)&DefaultCircuitTab, MAIN_FLASH_STORAGE, 0},
	{CAL_ID_PSUPLY, STRINGID_SUPPLY, PSUPLY_TABLE_FNAME, 1, PSUPLY_CAL_TABLE_SIZE, (float *)&DefaultPSuplyTab, SPI_FLASH_STORAGE, PSUP_CAL_INDEX}
};

//*****************************************************************************
CalibrationTable_t::CalibrationTable_t(CalibrationID_t CalID)
{
	bool Return = true;
	CalibrationStatus = CAL_PASSED;
    bool	Return1 = true;
    uint8_t  EpromStructurePtr[CALIBRATION_STRUCTURE_LENGTH];
    EpromCalibrationStructure_t * Ptr = (EpromCalibrationStructure_t *)EpromStructurePtr;
	
	if(!AioDioDriver.ReadSpiEprom(EpromStructurePtr, CALIBRATION_STRUCTURE_LENGTH, MANIFOLD_CALIBRATION_DATA_ADDRESS))
    {
       RETAILMSG(TRUE, (L"Failed reading EpromStructurePtr Data from EEPROM \r\n"));
	   Return = false;
    }

    uint8_t OneByteCrcVer;
    uint8_t OneByteCrc = calcCRC8(EpromStructurePtr, CALIBRATION_STRUCTURE_LENGTH);
    AioDioDriver.ReadSpiEprom(&OneByteCrcVer, 1, MANIFOLD_CALIBRATION_CRC_ADDRESS);
    if(OneByteCrc != OneByteCrcVer)
    {
         RETAILMSG(TRUE, (L"CRC mismatched reading CalDat from EEPROM in UpdateSlopeAndSpan \r\n"));
		 Return = false;
    }
	for(int i = 0; i < NUM_CAL_IDS; i++)
	{
		if(CalID == DefaultCalibrationInfo[i].CalibrationID)
		{
			memcpy(&CalibrationInfo, &DefaultCalibrationInfo[i], sizeof(CalibrationInfo_t));
			TableByteSize = DefaultCalibrationInfo[i].TableSize * sizeof(float);
			pTable = new float[DefaultCalibrationInfo[i].TableSize];
			
			if(RetrieveCalibrationTable())
			{
				Return1 = true; // Calibrated
			}
			else
			{
				for(int i = 0; i < CalibrationInfo.TableSize; i++)
				{
					pTable[i] = CalibrationInfo.pDefaultTable[i];
				}
				Crc = ::CalculateCrc((uint8_t *)pTable, TableByteSize);
				Return1 = false; // Not calibrated
			}
            
			// Overwrite Cal Table from EEPROM
              switch (CalID)
              {
                case CAL_ID_P1:
                    pTable[SPAN_INDEX] = Ptr->InternalPressure; 
                    pTable[ZERO_INDEX] = Ptr->EndInternalPressure; 
                    if(Return)
                    {
                        RETAILMSG(1, (L"Table ID = %d, Calibrated\r\n", CalID));
                    }
                    break;

                case CAL_ID_P2:
                    pTable[SPAN_INDEX] = Ptr->ExhalePressure; 
                    pTable[ZERO_INDEX] = Ptr->EndExhalePressure; 
                    if(Return)
                    {
                        RETAILMSG(1, (L"Table ID = %d, Calibrated\r\n", CalID));
                    }
                    break;
    
                case CAL_ID_PDRIVE:
                    pTable[SPAN_INDEX] = Ptr->ExhDrivePressure; 
                    pTable[ZERO_INDEX] = Ptr->EndExhDrivePressure;  
                    if(Return)
                    {
                        RETAILMSG(1, (L"Table ID = %d, Calibrated\r\n", CalID));
                    }
                    break;

                case CAL_ID_PAIR:
                    pTable[SPAN_INDEX] = Ptr->AirPressure; 
                    pTable[ZERO_INDEX] = Ptr->EndAirPressure; 
                    if(Return)
                    {
                        RETAILMSG(1, (L"Table ID = %d, Calibrated\r\n", CalID));
                    }
                  break;

                case CAL_ID_PO2:
                    pTable[SPAN_INDEX] = Ptr->O2Pressure; 
                    pTable[ZERO_INDEX] = Ptr->EndO2Pressure; 
                    if(Return)
                    {
                        RETAILMSG(1, (L"Table ID = %d, Calibrated\r\n", CalID));
                    }
                    break;

                case CAL_ID_PSUPLY:
                    pTable[SPAN_INDEX] = Ptr->ExhSupply; 
                    pTable[ZERO_INDEX] = Ptr->EndExhSupply; 
                    if(Return)
                    {
                        RETAILMSG(1, (L"Table ID = %d, Calibrated\r\n", CalID));
                    }
                    break;

                default:
                    if(Return1)
                    {
                        RETAILMSG(1, (L"Table ID = %d, Calibrated\r\n", CalID));
                    }
                    else
                    {
        				RETAILMSG(1, (L"Table ID = %d, NOT Calibrated\r\n", CalID));
                    }
                   break;
              } // end switch
		} // end 		if(CalID == DefaultCalibrationInfo[i].CalibrationID)
	}  // end for(int i = 0; i < NUM_CAL_IDS; i++)
}

//*****************************************************************************
CalibrationTable_t::~CalibrationTable_t()
{
	if(pTable)
		delete pTable;
}

//*****************************************************************************
CalibrationStatus_t CalibrationTable_t::Calibrate(const ConvertedA2dReadings_t *pConvertedData)
{
	if(CalibrationStatus != CAL_PENDING)
	{
		CalibrationStatus = CAL_PENDING;
		CalibrationDone = false;
	}

	DoCalibration(pConvertedData);
	
	return CalibrationStatus;
}

//*****************************************************************************
CalibrationStatus_t CalibrationTable_t::Calibrate(A2dWithStatus_t A2dData)
{
	if(CalibrationStatus != CAL_PENDING)
	{
		CalibrationStatus = CAL_PENDING;
		CalibrationDone = false;
	}

	DoCalibration(A2dData);
	
	return CalibrationStatus;
}

//*****************************************************************************
CalibrationStatus_t	CalibrationTable_t::Calibrate(Flow_t Flow, InspFlowType_t FlowType)
{
	if(CalibrationStatus != CAL_PENDING)
	{
		CalibrationStatus = CAL_PENDING;
		CalibrationDone = false;
	}

	DoCalibration(Flow, FlowType);
	
	return CalibrationStatus;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
bool CalibrationTable_t::RetrieveCalibrationTable()
{
	bool Result = false;
	FILE *File;

    errno_t errno = _wfopen_s(&File, CalibrationInfo.FileName, _T("rb"));
	if (errno == 0)
	{
		size_t BytesRead = fread((uint8_t *)pTable, 1, TableByteSize, File);	// Read the entire table
		if(BytesRead == TableByteSize)
		{
			uint32_t TableCrc;
			BytesRead = fread((uint8_t *)&TableCrc, 1, sizeof(uint32_t), File);
			uint32_t CalcCrc = ::CalculateCrc((uint8_t *)pTable, TableByteSize);
			
			if((TableCrc == CalcCrc)  && (BytesRead == sizeof(TableCrc)))
			{
				Result = true;
				bCalibrated = true;
				Crc = CalcCrc;
			}
		}
		fclose(File);
	}
	return Result;
}

//*****************************************************************************
// Write the Tables and CRCs
bool CalibrationTable_t::SaveCalibrationTable()
{
	bool Result;
	FILE *File;

    errno_t errno = _wfopen_s(&File, CalibrationInfo.FileName, _T("wb"));
    if (errno == 0)                                                              // is the file there?
    {
        size_t BytesWritten = fwrite(pTable, 1, TableByteSize, File);// Write the full table
        if (BytesWritten != TableByteSize)
        {
            bCalibrated = false;
            fclose(File);
            return false;
        }

        // Calculate the CRC of the table
        uint32_t CalcCrc = ::CalculateCrc((uint8_t *)pTable, TableByteSize);

        BytesWritten = fwrite(&CalcCrc, 1, sizeof(CalcCrc), File);              // Write the file CRC
        if (BytesWritten != sizeof(CalcCrc))
        {
            bCalibrated = false;
            fclose(File);
            return false;
        }

        bCalibrated = true;
		fclose(File);
    }

    Result = RetrieveCalibrationTable();                                                   // veify what we wrote is good
	return Result;
}

//*****************************************************************************
void CalibrationTable_t::GetTableInfo(uint8_t *pTab, uint16_t &TabSize)
{
	pTab = (uint8_t *)pTable;
	TabSize = TableByteSize;
}

//*****************************************************************************

void CalibrationTable_t::ControlPSOL(AnalogOutputs_t Type, DacCounts Value)
{
	switch(Type)
	{
	case DAC_AIR_VALVE:
		RAirPsol.updatePsol(Value);
		break;
	case DAC_O2_VALVE:
		RO2Psol.updatePsol(Value);
		break;
	case DAC_EXHALATION_VALVE:
		RExhPsol.updatePsol(Value);
		break;
	case DAC_DESIRED_FLOW: default:
		break;
	}
}
