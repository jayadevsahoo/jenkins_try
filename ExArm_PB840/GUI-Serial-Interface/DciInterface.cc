#include "stdafx.h"
//====================================================================
// This is a proprietary work to which Nellcor Puritan Bennett
// corporation of California claims exclusive right.  No part of this
// work may be used, disclosed, reproduced, sorted in an information
// retrieval system, or transmitted by any means, electronic,
// mechanical, photocopying, recording, or otherwise without the prior
// written permission of Nellcor Puritan Bennett Corporation.
//
//     Copyright (c) 1997, Nellcor Puritan Bennett Corporation
//=====================================================================

// =========================== M O D U L E   D E S C R I P T I O N ====
//@ Filename: DciInterface - Protocol Driver for the Digital Communications
//			     Interface (DCI) serial protocol.
//---------------------------------------------------------------------
//@ Interface-Description
//  Provides the Digital Communications Interface (DCI) protocol driver
//  for the 840 ventilator. This class contains the methods that
//  receive and decode incoming commands from the host over the serial
//  port. It also contains the interface for outputing data to the DCI
//  host through the serial port.
//---------------------------------------------------------------------
//@ Rationale
//  Provides DCI command processing.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The SerialInterface::Task instatiates this class when the 
//  ventilator transitions to a normal (ie. non-SERVICE) state. It
//  first flushes the serial port of any random data that might be
//  present, then proceeds to wait for incoming data on the serial
//  port. It then interprets any incoming serial data as DCI commands
//  and routes the processing of these commands to the appropriate
//  processing function.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness of 
//  parameterized data. Incoming commands are verified for correctness
//  and an error report issued for commands in error.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Serial-Interface/vcssrc/DciInterface.ccv   25.0.4.0   19 Nov 2013 14:12:44   pvcs  $
//
//@ Modification-Log
//
//  Revision: 013   By: gdc    Date: 11-Aug-2010    SCR Number: 6663
//  Project:  API/MAT
//  Description:
//      Modified to support VentSet command line interface on DCI port 
//      when using development/manufacturing data key. Removed error
//      processing for parity and overrun errors that is now done in 
//      the serial interrupt handler. Parity and overrun errors will
//      be handled as a command error.
//
//  Revision: 012   By: gdc    Date: 26-Jan-2009    SCR Number: 6407
//  Project:  840S
//  Description:
//      Modified to bring source into strict ARM compliance.
//
//  Revision: 011    By: gdc        Date: 13-Jan-2009  SCR Number: 5944
//  Project: 840S
//  Description:
//  	Modified to allow changes to service mode baud rate without
//  	requiring power cycle.
//
//  Revision: 010    By: rhj              Date: 12-Feb-2008  SCR Number: 6427
//  Project: Baseline
//  Description:
//      Added functionality in the processRequests_() method to reset the serial port,
//      when it did not receive a valid DCI command in 3 mins.
//
//  Revision: 009    By: rhj              Date: 11-Jul-2006  DR Number: 5928
//  Project: RESPM
//  Description:
//      Added SNDF command to generate a MISCF report.
//
//  Revision: 008    By: ljs              Date: 8-Dec-2003  DR Number: 6127
//  Project: Baseline
//  Description:
//      Modified to make Serial Communications more robust by:
//      - Removed 10 second delay (from SDCR# 5512).
//      - Log error into NOVRAM if it was the first error of that type,
//        or if using an Engineering/Production DataKey.
//      - Ignore UART parity error when serial port configuration has parity==NONE;
//        But, log that error if using an Engineering/Production DataKey.
//
//  Revision: 007    By: quf              Date: 26-Jun-2001  DR Number: 5493
//  Project: GuiComms
//  Description:
//      Add GuiComms functionality.
//
//  Revision: 006    By: hct              Date: 26-OCT-00    DR Number: 5784
//    Project: Baseline
//    Description:
//       Modified for Spacelabs monitor compatibility:
//       - Serial command is now taken as last four (or fewer) characters
//         between <CR>'s.
//       - Overflow error is now triggered by a UART overflow instead of by
//         an overflow of the 4-char buffer (which can no longer overflow).
//
//  Revision: 005    By: hct              Date: 19-OCT-00    DR Number: 5757
//    Project: Baseline
//    Description:
//       Added SER, SNID and SLMT reports to interface properly with the 
//       Marquette monitor.
//
//  Revision: 004    By: Gary Cederquist  Date: 19-Aug-1999  DR Number: 5512
//    Project: Sigma (R8027)
//    Description:
//       Added 10 second delay and cleared input buffer after receiving
//       parity error or buffer overflow.
//
//  Revision: 003    By: Gary Cederquist  Date: 24-Nov-1997  DR Number: 2605
//    Project: Sigma (R8027)
//    Description:
//       Complete restructuring of GUI-Serial-Interface to fix
//       several problems.
//
//  Revision: 002  By: gdc   Date: 01-Jul-1997    DR Number: 2232
//      Project:  Sigma (840)
//      Description:
//          Yield CPU to other lowest priority tasks periodically
//          to cooperatively multitask.
//
//  Revision: 1.X	By: jdm	Date: X/XX/96	DR Number: 
//	Project:  Sigma (R8027)
//	Description:
//		Initial version to coding standards (Integration baseline).
//=====================================================================

#include "Sigma.hh"
#include "DciInterface.hh"
#include <stdio.h>

//@ Usage-Classes
#include "Dci.hh"

#include "IpcIds.hh"
#include "SerialPort.hh"
#include "Task.hh"
#include "TaskInfo.hh"
#include "SoftwareOptions.hh"
#include "OsTimeStamp.hh"
#include "VsetServer.hh"
//@ End-Usage

//@ Code...
static const Uint32 SERIAL_RESET_TIMEOUT_ = 180000; // 3 min.

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: DciInterface [constructor]
//
//@ Interface-Description
//  Constructor for the DciInterface class. Instantiates the DCI 
//  protocol interface on the specified SerialPort.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
 
DciInterface::DciInterface( SerialPort & rSerialPort,
                            SerialInterface::PortNum portNum ) :
	                    SerialInterface( rSerialPort, portNum )
{
	//$[TI1] 
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: DciInterface [destructor]
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
 
DciInterface::~DciInterface()
{
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: startPrint
//
//@ Interface-Description
//  Someone's trying to print on the DCI interface. Cancel the 
//  operation and continue.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
 
Boolean
DciInterface::startPrint()
{
// $[TI1]
  return TRUE;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setCurrentPrinter_
//
//@ Interface-Description
//  Someone's trying to assign the printer to a DCI interface. Do
//  nothing so the assignment has no real effect.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
 
void
DciInterface::setCurrentPrinter_()
{
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processRequests_
//
//@ Interface-Description
//  Reads and processes serial command input from the serial port. 
//  Decodes the incoming command string and calls the appropriate
//  processing method. Valid DCI commands contain up to four characters
//  followed by a carriage return <CR> character. The only valid 
//  commands are SNDA, SNDF and RSET. Any other command results in a command
//  error returned to the host. A parity error or input buffer overflow
//  also results in an error report returned to the host. The processing
//  of valid DCI commands and generating error reports is implemented
//  in the DCI subsystem called from this method. The SNDA command
//  returns a MISCA report to the host. The SNDF command
//  returns a MISCF report to the host. The RSET command resets the 
//  serial port.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Dci::generateSNDA() is called to generate the MISCA report.
//  Dci::generateSNDF() is called to generate the MISCF report.
//  Dci::generateSNID() is called to generate the IDENT report.
//  Dci::generateSER() is called to generate the ER report.
//  Dci::generateSLMT() is called to generate the LMT report.
//	Dci::error() is called to generate the appropriate error report.
//	SerialPort::reset() is called to reset the serial port.
//
//  $[00463] The host messages supported are: SNDA, SLMT, SER, SNID and RSET. 
//  Sigma shall respond to these host messages as long as the GUI can determine
//  that the BD is in the online state.
//  $[00464] In response to the SNDA<CR> host message the ventilator
//  shall reply with the MISCA report.


//  $[?????] In response to the SNDF<CR> host message the ventilator


//  shall reply with the MISCF report.
//  $[MC00400] In response to the SNID<CR> host message the ventilator
//  shall reply with the IDENT report.
//  $[MC00401] In response to the SER <CR> host message the ventilator
//  shall reply with the ER report.
//  $[MC00402] In response to the SLMT<CR> host message the ventilator
//  shall reply with the LMT report.
//  $[00465] In response to any unknown messages a communications error
//  report ("??") shall be returned.
//  $[00467] In response to the "RSET<CR>" host message the ventilator
//  shall reset the RS-232 channel and discard and data in the input 
//  buffer.
//  $[00469] The code 7081 shall be returned when the communication
//  error is detected to be a parity error and 7088 when an overflow
//  of the input buffer occurs.
//  $[00470] All other error shall return the 7031 error code.
//  $[00471] The last four characters prior to the <CR> in the host
//  message shall be contained within square brackets of the Error Data 
//  field.
//  $[00475] An overflow of the input buffer shall result in an "??"
//  message being transmitted and the input buffer being flushed
//  (emptied).
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
DciInterface::processRequests_(void)
{
	char   buf[256];
	Uint   commandBytesRead = 0;
    buf[0] = '\0';

	Dci & rDci = Dci::GetDci(portNum_);
	VsetServer & rVsetServer = VsetServer::GetVsetServer(portNum_);

	OsTimeStamp serialResetTimer; 

	while ( !areSettingsChanged_ )
	{				      //$[TI1]
        Uint8 ch;

        Uint bytesRead = read( &ch, 1, 200 );

        // Reset the serial port, when it did not receive 
		// a valid DCI command in 3 mins.
        if ( (serialResetTimer.getPassedTime()) >= SERIAL_RESET_TIMEOUT_)
        {
            break;  // restart process requests.
        }

		if ( !bytesRead ) 
		{
			// $[TI2.1]
			continue;
		}
		// $[TI2.2]

		ch &= 0x7f;

   		switch( ch )
   		{
       		case 0x0D: 	       // $[TI4.1] <CR>
				if ( commandBytesRead > 0 )
				{
                    Boolean isCommandValid = TRUE;
                    // ignore all leading characters in the input buffer when looking for DCI commands
                    static const Int32 DCI_CMD_LEN_ = 4;
                    char * pCmdIn = buf + commandBytesRead - MIN_VALUE( commandBytesRead, DCI_CMD_LEN_ );

                    // look at the beginning of the command buffer for a VSET command
					if ( SoftwareOptions::GetSoftwareOptions().isStandardDevelopmentFunctions()
                         && !strncmp( buf, "VSET", DCI_CMD_LEN_ ) )
					{
						rVsetServer.processCmd( buf );
					}
					else if ( !strncmp( pCmdIn, "RSET", DCI_CMD_LEN_ ) )	// $[00467]
                	{
						rSerialPort_.reset();
					}
                	else if ( !strncmp( pCmdIn, "SNDA", DCI_CMD_LEN_ ) )
                	{
                    	rDci.generateSNDA();
				    }
                	else if ( !strncmp( pCmdIn, "SNID", DCI_CMD_LEN_ ) )
                	{
						rDci.generateSNID();
					}
                	else if ( !strncmp( pCmdIn, "SER ", DCI_CMD_LEN_ ) )
                	{
						rDci.generateSER();
			  	    }
                	else if ( !strncmp( pCmdIn, "SLMT", DCI_CMD_LEN_ ) )
                	{
						rDci.generateSLMT();
					}
                	else if ( !strncmp( pCmdIn, "SNDF", DCI_CMD_LEN_ ) )
                	{						
                    	rDci.generateSNDF();
					}
                  	else
                  	{
						// don't reset the serial port here since that negates CTS 
						// external monitors see this as a communications failure
						// and start alarming
						purgeline();
                        isCommandValid = FALSE;
                        static Boolean  IsErrorLogged_ = FALSE;
                        
                        Boolean isLoggingRequired = !IsErrorLogged_ || 
                            SoftwareOptions::GetSoftwareOptions().isStandardDevelopmentFunctions();

                        IsErrorLogged_ = TRUE;
                        
						// $[TI6.3]
						// $[00465] $[00470]
						rDci.error( pCmdIn, Dci::NON_SPECIFIC_ERROR, isLoggingRequired);
                  	}

                    if ( isCommandValid )
                    {
                        serialResetTimer.now();
                    }
					commandBytesRead = 0;
                    buf[0] = '\0';
              	}
				// $[TI5.2]
	          	break;
		
	        default :          // $[TI4.2] Any other character
				if ( commandBytesRead < sizeof( buf ) - 1 )
              	{
                	// $[TI7.1]
                  	buf[ commandBytesRead++ ] = ch;
                  	buf[ commandBytesRead   ] = 0;   // NULL terminate
				}
              	else
				{                                        
                	// $[TI7.2]   
					memcpy(buf, buf + 1, sizeof(buf) - 2);
					buf[sizeof(buf)-2] = ch;
					buf[sizeof(buf)-1] = 0;   // NULL terminate
              	}
		}
	}

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//@ Interface-Description
// Implicitly gain execution control when a software assertion macro
// detects and error.
//---------------------------------------------------------------------
//@ Implementation-Description
// Automatically invoked when CLASS_ASSERTION and CLASS_PRE_CONDITON
// macros (among others) detect a fault.  We just call FaultHandler::SoftFault
// to display pertinent information to the user.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
 
void    
DciInterface::SoftFault(  const SoftFaultID softFaultID
                           , const Uint32      lineNumber
                           , const char *      pFileName
                           , const char *      pPredicate )
{
    FaultHandler::SoftFault(softFaultID, GUI_SERIAL_INTERFACE,
		GUI_Serial_Interface::DCI_INTERFACE_CLASS, 
		lineNumber, pFileName, pPredicate);
}
