#ifndef Queue_HH
#define Queue_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Nellcor/Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: Queue - class for providing ways to send Int32 bit messages
//         between tasks.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Download/vcssrc/Queue.hhv   25.0.4.0   19 Nov 2013 14:02:50   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date: 15-Jan-99    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001  By:  jhv    Date:  25-Jan-1996    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//====================================================================

#include "DownloadTypes.hh"
#include "DownloadFault.hh"
#include "DownloadClassIds.hh"

enum QUEUE
{
  	FLASH_MEMORY_Q = 1,
	QUEUE_LENGTH = 30
};

class Queue
{
  public:
	Queue(Int qId);
    ~Queue(void);

    void  putMsg(const Int32 msg) ;
    Int32  pendForMsg( Uint32 waitTime= 0 );

    static void SoftFault(const FaultType softFaultID,
		const Uint32      lineNumber,
		const char*       pFileName  = NULL, 
		const char*       pPredicate = NULL);
  protected:

  private:
    // these methods are purposely declared, but not implemented...
    Queue(void);			// not implemented...
    Queue(const Queue&);		// not implemented...
    void   operator=(const Queue&);	// not implemented...
 
    //@ Data-Member: QID_
    // Holds the VRTX queue  id for this queue.
    const Int32 QID_;

    //@ Data-Member: error_
    // Holds the last VRTX error encountered by this object
    Int32 error_;
};


#endif // Queue_HH 
