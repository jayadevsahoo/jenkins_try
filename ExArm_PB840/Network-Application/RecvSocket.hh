#ifndef	RecvSocket_HH
#define	RecvSocket_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1994, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
//@ Filename: RecvSocket - Class definition for a socket connection
//      between the GUI and BD boards that is mainly used to receive
//      data from the other Sigma board.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Network-Application/vcssrc/RecvSocket.hhv   25.0.4.0   19 Nov 2013 14:14:34   pvcs  $
//@  Modification-Log
//
//  Revision: 005  By: gdc     Date: 30-Dec-2010  SCR Number: 6716
//  Project:  XENA2
//  Description:
//      Changed to use thread-safe socket library.
//
//  Revision: 002  By: sah     Date: 13-Jan-99    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 001  By:  <jhv>    Date:  06-June-95    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//====================================================================


#include "Sigma.hh"
#include "NetworkSocket.hh"
#include "NetworkAppClassIds.hh"

class NetworkApp;


class RecvSocket : public NetworkSocket
{
#if	defined(SIGMA_UNIT_TEST)
  	friend  void NetworkAppUnitTest();
#endif //defined(SIGMA_UNIT_TEST)

  public:
    RecvSocket();
	Int readMainData();
	Boolean isMaxActiveMsgDelay();
	virtual SigmaStatus verifyConnection(XmitDataMsgId thisActive,
			 	XmitDataMsgId otherActive);
	virtual void reInit(Int id);
	void setMsgDelayCounter(Int count);
	void incrementDelayCounter();
	virtual void  down(void);

    static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);
  
  protected:

  private:
    RecvSocket(const RecvSocket&);	// not implemented...
    void   operator=(const RecvSocket&);	// not implemented...
    ~RecvSocket(void);						// not implemented...

	// Data-Member: activeMsgDelay 
	//  The activeMsgDelay variable records the number of consecutive 
	//  misses of the "I am alive" message or any network data from the
	//  other sigma board through this socket.
	//  
	Uint activeMsgDelay_ ;
	
	//@ Data-Member: recvBuffer_[]
	//  Word boundry memory space that accepts one network message
	//  at a time from the network and passes the data to other
	//  callback routines. There is only one active message at a time.
	Uint recvBuffer_[(MAX_NETWORK_BUF_SIZE + sizeof(Uint32)-1)/sizeof(Uint32)];

};




#endif // RecvSocket_HH 
