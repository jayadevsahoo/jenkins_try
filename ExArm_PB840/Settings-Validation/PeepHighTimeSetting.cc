#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//================= C L A S S   D E S C R I P T I O N =================
//@ Class:  PeepHighTimeSetting - Inspiratory Time Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, bounded setting that contains the time (in milliseconds)
//  a that the PEEP-high pressure value is maintained.  This class inherits
//  from 'BatchBoundedSetting' and provides the specific information needed
//  for representation of PEEP-high time.  This information includes the
//  interval and range of this setting's values, this setting's response to a
//  Main Control Setting's transition (see 'acceptTransition()'), this
//  setting's response to a change of its primary setting (see
//  'acceptPrimaryChange()'), this batch setting's new-patient value (see
//  'getNewPatientValue()'), and the contraints and dependent settings of
//  this setting.
//
//  This setting also dynamically monitors the constant parm setting,
//  and updates its applicability accordingly.
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class defines a 'calcDependentValues_()' method for updating
//  this setting's dependent settings, and an 'updateConstraints_()' method
//  for updating the constraints of this setting.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/PeepHighTimeSetting.ccv   25.0.4.0   19 Nov 2013 14:27:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004   By: gdc    Date: 16-Aug-2010     SCR Number: 6663
//  Project: API/MAT
//  Description:
//  Added support for VentSet API.
// 
//  Revision: 003   By: sah    Date: 22-Sep-1999    DR Number: 5424
//  Project:  NeoMode
//  Description:
//	Obsoleted 'SettingOptions' class, to use new global
//      'SoftwareOptions' class.
//
//  Revision: 002   By: sah   Date:  22-Jul-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  modified to use newly-modified 'BoundedRange' class to handle
//         range, via linked list of interval nodes
//
//  Revision: 001   By: sah   Date:  26-Jan-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//     Created to work with new applicability functionality.
//
//=====================================================================

#include "PeepHighTimeSetting.hh"
#include "ModeValue.hh"
#include "ConstantParmValue.hh"

//@ Usage-Classes
#include "SettingsMgr.hh"
#include "RespRateSetting.hh"
#include "ConstantParmSetting.hh"
#include "SoftwareOptions.hh"
//@ End-Usage


//@ Code...

//====================================================================
//
//  Static Data Definitions...
//
//====================================================================

// $[TC02011] -- this setting's dependent settings...
static const SettingId::SettingIdType  ARR_DEPENDENT_SETTING_IDS_[] =
  {
    SettingId::PEEP_LOW_TIME,
    SettingId::HL_RATIO,

    SettingId::NULL_SETTING_ID
  };


// $[TC02006] The setting's range ...
// $[TC02009] The setting's resolution ...
static const BoundedInterval  LAST_NODE_ =
  {
    200.0f,		// absolute minimum...
    0.0f,		// unused...
    TENS,		// unused...
    NULL
  };
static const BoundedInterval  INTERVAL_LIST_ =
  {
    30000.0f,		// absolute maximum...
    10.0f,		// resolution...
    TENS,		// precision...
    &::LAST_NODE_
  };


//====================================================================
//
//  Public Methods...
//
//====================================================================

//============= M E T H O D   D E S C R I P T I O N ==================
//@ Method: PeepHighTimeSetting()  [Default Constructor]
//
//@ Interface-Description
//  Construct an instance of an inspiratory time setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

PeepHighTimeSetting::PeepHighTimeSetting(void)
       : BatchBoundedSetting(SettingId::PEEP_HIGH_TIME,
			     ::ARR_DEPENDENT_SETTING_IDS_,
			     &::INTERVAL_LIST_,
			     PEEP_HIGH_TIME_MAX_ID,	// maxConstraintId...
			     PEEP_HIGH_TIME_MIN_ID)	// minConstraintId...

{
  CALL_TRACE("PeepHighTimeSetting()");
	isATimingSetting_ = TRUE;
}   // $[TI1]


//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  ~PeepHighTimeSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

PeepHighTimeSetting::~PeepHighTimeSetting(void)
{
  CALL_TRACE("~PeepHighTimeSetting()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[01071] -- main settings applicability
//  $[01081] -- breath settings applicability
//---------------------------------------------------------------------
//@ PreCondition
//  (qualifierId == Notification::ACCEPTED  ||
//   qualifierId == Notification::ADJUSTED)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
PeepHighTimeSetting::getApplicability(const Notification::ChangeQualifier qualifierId) const
{
  CALL_TRACE("getApplicability(qualifierId)");

  Applicability::Id  applicability;

  if (SoftwareOptions::IsOptionEnabled(SoftwareOptions::BILEVEL))
  {  // $[TI1] -- BILEVEL is an active option...
    const Setting*  pMode = SettingsMgr::GetSettingPtr(SettingId::MODE);
    const Setting*  pConstantParm =
			  SettingsMgr::GetSettingPtr(SettingId::CONSTANT_PARM);

    DiscreteValue  modeValue;
    DiscreteValue  constantParmValue;

    switch (qualifierId)
    {
    case Notification::ACCEPTED :		// $[TI1.1]
      modeValue         = pMode->getAcceptedValue();
      constantParmValue = pConstantParm->getAcceptedValue();
      break;
    case Notification::ADJUSTED :		// $[TI1.2]
      modeValue         = pMode->getAdjustedValue();
      constantParmValue = pConstantParm->getAdjustedValue();
      break;
    default :
      AUX_CLASS_ASSERTION_FAILURE(qualifierId);
      break;
    }

    if (modeValue == ModeValue::BILEVEL_MODE_VALUE)
    {  // $[TI1.3]
      applicability =
	    (constantParmValue == ConstantParmValue::PEEP_HIGH_TIME_CONSTANT)
		       ? Applicability::CHANGEABLE	// $[TI1.3.1]
		       : Applicability::VIEWABLE;	// $[TI1.3.2]
    }
    else
    {  // $[TI1.4]
      applicability = Applicability::INAPPLICABLE;
    }
  }
  else
  {  // $[TI2] -- BILEVEL is not an active option...
    applicability = Applicability::INAPPLICABLE;
  }

  return(applicability);
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [const, virtual]
//
//@ Interface-Description
//  Return this setting's new-patient value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[TC02007] -- new-patient value...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
PeepHighTimeSetting::getNewPatientValue(void) const
{
  CALL_TRACE("getNewPatientValue()");

  const Setting*  pInspTime = SettingsMgr::GetSettingPtr(SettingId::INSP_TIME);

  const BoundedValue  NEW_PATIENT = pInspTime->getNewPatientValue();

  return(NEW_PATIENT);
}  // $[TI1]


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method: acceptPrimaryChange(primaryId)
//
//@ Interface-Description
//  One of this setting's primary settings have changed, therefore update
//  this setting's value based on the new value of the primary setting
//  -- indicated by 'primaryId'.  If this setting's bound is violated by
//  the primary setting's newly "adjusted" value, a pointer to this
//  setting's bound status is returned, and this setting's value is "clipped"
//  to that bound.  Otherwise, a value of 'NULL' is returned to indicate no
//  dependent bound was violated.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

const BoundStatus*
PeepHighTimeSetting::acceptPrimaryChange(const SettingId::SettingIdType primaryId)
{
  CALL_TRACE("acceptPrimaryChange(primaryId)");

  // update dynamic constraints...
  updateConstraints_();

  BoundStatus&  rBoundStatus = getBoundStatus_();

  // initialize to holding this setting's id...
  rBoundStatus.setViolationId(getId());

  BoundedValue  newPeepHighTime;

  newPeepHighTime = calcBasedOnSetting_(primaryId, BoundedRange::WARP_NEAREST,
					BASED_ON_ADJUSTED_VALUES);

  // test 'newPeepHighTime' against this setting's bounded range; the bound
  // violation state, if any, is returned in 'rBoundStatus', while
  // 'newPeepHighTime' is "clipped" if a bound is violated...
  getBoundedRange_().testValue(newPeepHighTime, rBoundStatus);

  // store the value...
  setAdjustedValue(newPeepHighTime);

  const BoundStatus*  pBoundStatus;

  // if there is a bound violation return a pointer to this setting's
  // bound status, otherwise return 'NULL'...
  pBoundStatus = (rBoundStatus.isInViolation()) ? &rBoundStatus	// $[TI1]
						: NULL;		// $[TI2]

  return(pBoundStatus);
}


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method: acceptTransition(settingId, newValue, currValue) [virtual]
//
//@ Interface-Description
//  This method is responsible for updating this setting based on the
//  the Main Control Setting identified by 'settingId' and the transition
//  of its value from 'currValue' to 'newValue'.
//
//  The only transitions that inspiratory time is interested in 
//  are 
//  (1) the transition of mandatory type from "PCV" to "VCV ventilation;
//  (2) the transition of mode from or to "BiLevel". 
//  This method will conduct the appropriate transition, 
//  and -- unlike other responses to a transition -- 
//  this method updates this setting's
//  dependent settings (i.e., PEEP low time and H:L ratio).
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02025]\f\   -- transitioning from A/C to BiLevel
//  $[TC02000]\c\ -- transitioning from SIMV to BiLevel
//  $[02026]\e\   -- transitioning from SPONT to BiLevel
//---------------------------------------------------------------------
//@ PreCondition
//  (settingId == SettingId::MODE  &&
//   newValue == ModeValue::BILEVEL_MODE_VALUE)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
PeepHighTimeSetting::acceptTransition(const SettingId::SettingIdType settingId,
				      const DiscreteValue            newValue,
				      const DiscreteValue            currValue)
{
  CALL_TRACE("acceptTransition(settingId, newValue, currValue)");

  // the only mode transition that Th is concerned with is a transition
  // to BiLevel...
  AUX_CLASS_PRE_CONDITION((settingId == SettingId::MODE  &&
			   newValue == ModeValue::BILEVEL_MODE_VALUE),
			  settingId);

  BoundedValue  newPeepHighTime;

  const Setting*  pInspTime = SettingsMgr::GetSettingPtr(SettingId::INSP_TIME);

  // set PEEP high time to inspiratory time's value...
  newPeepHighTime = pInspTime->getAdjustedValue();

  // store the transition value into the adjusted context...
  setAdjustedValue(newPeepHighTime);

  setForcedChangeFlag();
}  // $[TI1]


//==================== M E T H O D   D E S C R I P T I O N =============
//@ Method:  valueUpdate(qualifierId, pSubject)  [virtual]
//
//@ Interface-Description
//  Respond to subject's value changes, by, possibly, updating this setting's
//  applicability.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (pSubject->getId() == SettingId::CONSTANT_PARM)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
PeepHighTimeSetting::valueUpdate(const Notification::ChangeQualifier qualifierId,
			         const SettingSubject*               pSubject)
{
  CALL_TRACE("valueUpdate(qualifierId, pSubject)");
  SAFE_AUX_CLASS_PRE_CONDITION((pSubject->getId() == SettingId::CONSTANT_PARM),
			       pSubject->getId());

  if (SoftwareOptions::IsOptionEnabled(SoftwareOptions::BILEVEL))
  {  // $[TI1] -- BiLevel option is active...
    // update this instance's applicability...
    updateApplicability(qualifierId);
  }  // $[TI2] -- BiLevel option is NOT active...
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: doRetainAttachment(pSubject)  [virtual, const]
//
//@ Interface-Description
//  When task-level, state changes occur, all subjects are notified
//  to detach from all observers.  This is done because the GUI-Apps
//  observers may be overwritten by new class instances, without any
//  explicit destructor/detachment calls.
//
//  This method is defined here to indicate to any of this instance's
//  subjects, not to detach this observer.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean
PeepHighTimeSetting::doRetainAttachment(const SettingSubject*) const
{
  CALL_TRACE("doRetainAttachment(pSubject)");

  return(TRUE);
}  // $[TI1]


//==================== M E T H O D   D E S C R I P T I O N ============
//@ Method:  settingObserverInit()  [virtual]
//
//@ Interface-Description
//  This setting needs to monitor the value of Constant Parm setting,
//  therefore this virtual method is overridden to provide a point during
//  initialization to attach to that (subject) setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
PeepHighTimeSetting::settingObserverInit(void)
{
  CALL_TRACE("settingObserverInit()");

  // monitor changes in constant parm's value...
  attachToSubject_(SettingId::CONSTANT_PARM, Notification::VALUE_CHANGED);
}  // $[TI1]


#if defined(SIGMA_DEVELOPMENT)

//============== M E T H O D   D E S C R I P T I O N =================
// Method:  isAcceptedValid()  [const, virtual]
//
// Interface-Description
//  Is the accepted value of this setting valid as determined by its
//  dependencies?
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

Boolean
PeepHighTimeSetting::isAcceptedValid(void) const
{
  CALL_TRACE("isAcceptedValid()");

  Boolean  isValid = BoundedSetting::isAcceptedValid();

  if (isValid)
  {
    if (getApplicability(Notification::ACCEPTED) !=
					    Applicability::INAPPLICABLE)
    {
      Setting*  pPeepLowTime =
		      SettingsMgr::GetSettingPtr(SettingId::PEEP_LOW_TIME);
      Setting*  pHlRatio = SettingsMgr::GetSettingPtr(SettingId::HL_RATIO);

      // forward on to PEEP high time's dependent settings...
      isValid = (pPeepLowTime->isAcceptedValid()  &&
		 pHlRatio->isAcceptedValid());

      if (!isValid)
      {
	cout << "INVALID PEEP High Time:\n";
	cout << *this << *pPeepLowTime << *pHlRatio << endl;
      }
    }
  }
  else
  {
    cout << "INVALID PEEP High Time:\n";
    cout << *this << endl;
  }

  return(isValid);
}


//============== M E T H O D   D E S C R I P T I O N =================
// Method:  isAdjustedValid()  [const, virtual]
//
// Interface-Description
//  Is the adjusted value of this setting valid as determined by its
//  dependencies?
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

Boolean
PeepHighTimeSetting::isAdjustedValid(void)
{
  CALL_TRACE("isAdjustedValid()");

  Boolean  isValid = BoundedSetting::isAdjustedValid();

  if (isValid)
  {
    if (getApplicability(Notification::ADJUSTED) !=
					    Applicability::INAPPLICABLE)
    {
      Setting*  pPeepLowTime =
		      SettingsMgr::GetSettingPtr(SettingId::PEEP_LOW_TIME);
      Setting*  pHlRatio = SettingsMgr::GetSettingPtr(SettingId::HL_RATIO);

      // forward on to PEEP high time's dependent settings...
      isValid = (pPeepLowTime->isAdjustedValid()  &&
		 pHlRatio->isAdjustedValid());

      if (!isValid)
      {
	cout << "INVALID PEEP High Time:\n";
	cout << *this << *pPeepLowTime << *pHlRatio << endl;
      }
    }
  }
  else
  {
    cout << "INVALID PEEP High Time:\n";
    cout << *this << endl;
  }

  return(isValid);
}

#endif // defined (SIGMA_DEVELOPMENT)


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//	      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
PeepHighTimeSetting::SoftFault(const SoftFaultID  softFaultID,
			   const Uint32       lineNumber,
			   const char*        pFileName,
			   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
			  PEEP_HIGH_TIME_SETTING, lineNumber, pFileName,
			  pPredicate);
}


//====================================================================
//
//  Protected Methods...
//
//====================================================================

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method: calcBasedOnSetting_(basedOnSettingId, warpDirection, basedOnCategory)  [const]
//
//@ Interface-Description
//  This method is responsible for calculating, and returning, a new
//  "adjusted" value for this setting, based on the "adjusted" value
//  of the dependent setting given by 'basedOnSettingId'.  This method will
//  calculate values for each of this setting's dependent settings,
//  based on values in the Adjusted Context.
//---------------------------------------------------------------------
//@ Implementation-Description
//  All new-patient calculations are to be based on the VCV setting values.
//
// $[TC02007] -- this Setting's new-patient calculation...
//---------------------------------------------------------------------
//@ PreCondition
//  (basedOnCategory == BASED_ON_ADJUSTED_VALUES)  &&
//  (basedOnSettingId == SettingId::RESP_RATE  ||
//   basedOnSettingId == SettingId::HL_RATIO  ||
//   basedOnSettingId == SettingId::PEEP_LOW_TIME)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

BoundedValue
PeepHighTimeSetting::calcBasedOnSetting_(
			  const SettingId::SettingIdType basedOnSettingId,
			  const BoundedRange::WarpDir    warpDirection,
			  const BasedOnCategory          basedOnCategory
				    ) const
{
  CALL_TRACE("calcBasedOnSetting_(basedOnSettingId, warpDirection, basedOnCategory)");
  AUX_CLASS_PRE_CONDITION((basedOnCategory == BASED_ON_ADJUSTED_VALUES),
			  basedOnCategory);

  SettingId::SettingIdType  finalBasedOnSettingId;

  if (basedOnSettingId == SettingId::RESP_RATE)
  {   // $[TI1] -- convert 'RESP_RATE' to "based-on" id...
    const DiscreteValue  CONSTANT_PARM_VALUE =
			       ConstantParmSetting::GetValue(basedOnCategory);

    switch (CONSTANT_PARM_VALUE)
    {
    case ConstantParmValue::PEEP_LOW_TIME_CONSTANT :    // $[TI1.1]
      // respiratory rate is being changed while holding PEEP low time
      // constant, therefore calculate the new inspiratory time from the
      // current PEEP low time...
      finalBasedOnSettingId = SettingId::PEEP_LOW_TIME;
      break;
    case ConstantParmValue::HL_RATIO_CONSTANT :    // $[TI1.2]
      // respiratory rate is being changed while holding H:L ratio
      // constant, therefore calculate the new inspiratory time from the
      // current H:L ratio...
      finalBasedOnSettingId = SettingId::HL_RATIO;
      break;
    case ConstantParmValue::PEEP_HIGH_TIME_CONSTANT :
    case ConstantParmValue::TOTAL_CONSTANT_PARMS :
    default :
      // invalid value for 'CONSTANT_PARM_VALUE'...
      AUX_CLASS_ASSERTION_FAILURE(CONSTANT_PARM_VALUE);
      break;
    };
  }
  else
  {   // $[TI2]
    // use passed in parameter...
    finalBasedOnSettingId = basedOnSettingId;
  }

  // get the breath period from the respiratory rate...
  const Real32  BREATH_PERIOD_VALUE =
			  RespRateSetting::GetBreathPeriod(basedOnCategory);

  BoundedValue  peepHighTime;

  switch (finalBasedOnSettingId)
  {
  //-------------------------------------------------------------------
  // $[02013](2) -- new PEEP high time while changing H:L ratio...
  // $[02017](1) -- new PEEP high time while keeping H:L ratio constant...
  //-------------------------------------------------------------------
  case SettingId::HL_RATIO :
    {   // $[TI3] -- base PEEP high time's calculation on H:L ratio...
      // PEEP high time is to base its value on the "adjusted" H:L ratio,
      // only...

      const Setting*  pHlRatio =
			  SettingsMgr::GetSettingPtr(SettingId::HL_RATIO);

      // get the H:L ratio value from the Adjusted Context...
      const Real32  HL_RATIO_VALUE =
			    BoundedValue(pHlRatio->getAdjustedValue()).value;

      // get the breath period from the respiratory rate...
      const Real32  BREATH_PERIOD_VALUE =
			    RespRateSetting::GetBreathPeriod(basedOnCategory);

      Real32  lRatioValue;
      Real32  hRatioValue;

      if (HL_RATIO_VALUE < 0.0f)
      {   // $[TI3.1]
	// The H:L ratio value is negative, therefore we have 1:xx with
	// "xx" the PEEP low portion of the ratio...
	hRatioValue = 1.0f;
	lRatioValue = -(HL_RATIO_VALUE);
      }
      else
      {   // $[TI3.2]
	// The H:L ratio value is positive, therefore we have xx:1 with
	// "xx" the PEEP high portion of the ratio...
	hRatioValue = HL_RATIO_VALUE;
	lRatioValue = 1.0f;
      }

      // calculate the PEEP high time from the H:L ratio...
      peepHighTime.value = (hRatioValue * BREATH_PERIOD_VALUE) /
			   (hRatioValue + lRatioValue);
    }
    break;

  //-------------------------------------------------------------------
  // $[02012](2) -- new PEEP high time while changing PEEP low time...
  // $[02016](2) -- new PEEP high time while keeping PEEP low time constant...
  //-------------------------------------------------------------------
  case SettingId::PEEP_LOW_TIME :
    {   // $[TI4] -- base PEEP high time's calculation on PEEP low time...
      // PEEP high time is to base its value on the "adjusted" PEEP low
      // time, only...
      AUX_CLASS_ASSERTION((basedOnCategory == BASED_ON_ADJUSTED_VALUES),
      			  basedOnCategory);

      const Setting*  pPeepLowTime =
			  SettingsMgr::GetSettingPtr(SettingId::PEEP_LOW_TIME);

      // get the PEEP low time value from the Adjusted Context...
      const Real32  PEEP_LOW_TIME_VALUE =
      			BoundedValue(pPeepLowTime->getAdjustedValue()).value;

      // calculate PEEP high time...
      peepHighTime.value = BREATH_PERIOD_VALUE - PEEP_LOW_TIME_VALUE;
    }
    break;

  default :
    // unexpected dependent setting id...
    AUX_CLASS_ASSERTION_FAILURE(finalBasedOnSettingId);
    break;
  };

  // place calculated value on a "click" boundary...
  getBoundedRange().warpValue(peepHighTime, warpDirection, FALSE);

  return(peepHighTime);
}
