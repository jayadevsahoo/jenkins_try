#ifndef NetworkDefs_HH
#define NetworkDefs_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1994, Puritan-Bennett Corporation
//=====================================================================

#include "NetworkMsgId.hh"

enum NetworkAppConstants
{
    TOTAL_MSG_NUMBER=HIGHEST_NETWORK_MSG_ID-LOWEST_NETWORK_MSG_ID + 1,
    TOTAL_RECVMSG_CALLBACKS = TOTAL_MSG_NUMBER*2, 
    TOTAL_OPEN_CALLBACKS = 2,
    COMMUNICATION_RETRY = 3,
    MAX_NETWORK_WAIT_TIME = 10000,  // maximum wait interval in ms before
    // declaring a communication down state
    // when the system powers up.
    INITIAL_COMMUNICATION_LINK_RETRY = 5,
    MAX_NETWORK_BUF_SIZE = 2000,
    COMM_OPEN_ITERATION_TIME = 2000,    // 2000 milliseconds.
    KEEP_ALIVE_INTERVAL = 200,          // 200 milli seconds.
    MAX_INPUT_WAIT_DURATION = 2000,     // 2000 milli seconds.
    MAX_ALIVE_MSG_MISS = 7,             // 1.4 seconds
    MAX_BROADCAST_MSG_SIZE = 1000
};

#endif // NetworkDefs_HH