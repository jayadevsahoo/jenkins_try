
#ifndef NonBatchBoundedSetting_HH
#define NonBatchBoundedSetting_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  NonBatchBoundedSetting - Non-Batch Bounded Setting.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/NonBatchBoundedSetting.hhv   25.0.4.0   19 Nov 2013 14:27:32   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004 By: srp    Date: 28-May-2002   DR Number: 5898
//  Project:  VCP
//	Description:
//		Incorporated Remediation code review issue(s) -- commentary changes only.
//
//  Revision: 003   By: sah  Date:  17-April-2000    DR Number:  5327
//  Project:  NeoMode
//  Description:
//  Updated for NeoMode
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Initial version.
//
//====================================================================

//@ Usage-Classes
#include "BoundedSetting.hh"
//@ End-Usage


class NonBatchBoundedSetting : public BoundedSetting 
{
  public:
    virtual ~NonBatchBoundedSetting(void);

    virtual Boolean  isChanged(void) const;

    virtual SettingValue  getDefaultValue   (void) const = 0;
    virtual SettingValue  getNewPatientValue(void) const;

    virtual SettingValue  getAdjustedValue(void) const;

    virtual void  setAdjustedValue(const SettingValue& newAdjustedValue);

    virtual void  updateToNewPatientValue(void);

    virtual void  acceptTransition(const SettingId::SettingIdType settingId,
                                   const DiscreteValue            newValue,
                                   const DiscreteValue            currValue);

#if defined(SIGMA_DEVELOPMENT)
    virtual Boolean  isAdjustedValid(void);
#endif // defined(SIGMA_DEVELOPMENT)

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL, 
			   const char*       pPredicate = NULL);

  protected:
    NonBatchBoundedSetting(
		      const SettingId::SettingIdType  settingId,
		      const SettingId::SettingIdType* arrDependentSettingIds,
		      const BoundedInterval*          pIntervalList,
		      const SettingBoundId            maxConstraintId,
		      const SettingBoundId            minConstraintId,
		      const Boolean                   useEpsilonFactoring = TRUE
			  );

  private:
    // these methods are declared, but not implemented...
    NonBatchBoundedSetting(const NonBatchBoundedSetting&);
    NonBatchBoundedSetting(void);
    void operator=(const NonBatchBoundedSetting&);
};


#endif // NonBatchBoundedSetting_HH 
