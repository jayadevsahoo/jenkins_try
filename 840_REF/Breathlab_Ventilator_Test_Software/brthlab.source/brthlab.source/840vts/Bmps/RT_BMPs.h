#ifndef _RT_BMPS_H
#define _RT_BMPS_H

/*****************/
/**** ENUMS ******/
/*****************/

enum BMP_File_IDs {
	BMP_Cancel192,
	BMP_Check,
	BMP_Okay192,
	BMP_PC2000_Icon,
	BMP_PC840_Icon,
	BMP_QMark192,
	BMP_X,
	BMP_XCancel79
};


int setButtonBMP(int panel, int control, int bitmapid);

#endif // #ifndef _RT_BMPS_H
