#ifndef SubScreenButton_HH
#define SubScreenButton_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: SubScreenButton -  A subscreen selection button.  All
// Main Settings buttons and all buttons in the Upper and Lower Screen
// Select Areas are subscreen buttons.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/SubScreenButton.hhv   25.0.4.0   19 Nov 2013 14:08:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003   By: sah    Date:  20-Apr-2000    DCS Number: 5705
//  Project:  NeoMode
//  Description:
//      Modified constructor to take arguments for auxillary title text
//      and position, and forward on to TextButton base class.  This
//      functionality will be used to plug-in the "above PEEP" phrases
//      to the "Pi" and "Psupp" setting buttons.
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "TextButton.hh"

//@ Usage-Classes
#include "GuiAppClassIds.hh"
//@ End-Usage

class SubScreen;

class SubScreenButton : public TextButton
{
public:
	SubScreenButton(Uint16 xOrg, Uint16 yOrg, Uint16 width, Uint16 height,
		ButtonType buttonType, Uint8 bevelSize, CornerType cornerType,
		BorderType borderType, StringId title, SubScreen *pSubScreen = NULL,
		StringId auxTitleText = ::NULL_STRING_ID,
		Gravity  auxTitlePos  = ::GRAVITY_RIGHT);
    ~SubScreenButton(void);

    inline void setSubScreen(SubScreen *pSubScreen);

    static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL, 
			  const char*       pPredicate = NULL);
  
protected:
	virtual void downHappened_(Boolean byOperationAction);
	virtual void upHappened_(Boolean byOperatorAction);

    inline SubScreen* getSubScreen_(void);

private:
    // these methods are purposely declared, but not implemented...
    SubScreenButton(const SubScreenButton&);		// not implemented...
    void   operator=(const SubScreenButton&);	// not implemented...

	//@ Data-Member: pSubScreen_
	// This is a pointer to the subscreen which is controlled
	// (activated/deactivated) by this button.  It is set to NULL if this
	// button does not control a subscreen (most instances of derived
	// SubScreenButton classes do not control subscreens).
	SubScreen	*pSubScreen_;
};


// Inlined methods
#include "SubScreenButton.in"

#endif // SubScreenButton_HH
