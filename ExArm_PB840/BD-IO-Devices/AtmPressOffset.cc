#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: AtmPressOffset - Atmospheric pressure sensor offset.
//---------------------------------------------------------------------
//@ Interface-Description
//      This class is used by NOVRAM to store the atmospheric pressure
//		sensor's offset value.  Access methods to get and set the offset
//		value are provided.
//---------------------------------------------------------------------
//@ Rationale
//      Abstraction of atmospheric pressure sensor offset value.
//---------------------------------------------------------------------
//@ Implementation-Description
//      Provides default values to interface with NOVRAM initialization.
//---------------------------------------------------------------------
//@ Fault-Handling
//		n/a
//---------------------------------------------------------------------
//@ Restrictions
//		None
//---------------------------------------------------------------------
//@ Invariants
//		None
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/AtmPressOffset.ccv   25.0.4.0   19 Nov 2013 13:53:56   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002 By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//	Description:
//		Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 001  By:  syw    Date: 25-Sep-1997    DR Number: DCS 2410
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "AtmPressOffset.hh"

//@ Usage-Classes

//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: AtmPressOffset()
//
//@ Interface-Description
//      Constructor.  This method has no arguments and has no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//      The offset default values is stored.
//---------------------------------------------------------------------
//@ PreCondition
//      None
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================
AtmPressOffset::AtmPressOffset( void)
{
	CALL_TRACE("AtmPressOffset::AtmPressOffset( void)") ;

   	// $[TI1]

   	offset_ = 0.0 ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~AtmPressOffset ()
//
//@ Interface-Description
//      Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//      None
//---------------------------------------------------------------------
//@ PreCondition
//      None
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================
AtmPressOffset::~AtmPressOffset( void)
{
	CALL_TRACE("AtmPressOffset::~AtmPressOffset( void)") ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: AtmPressOffset( const AtmPressOffset&)
//
//@ Interface-Description
//		Copy constructor.  This method takes a AtmPressOffset as an argument and
//		returns nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//      The object passed in is copied to the current object.
//---------------------------------------------------------------------
//@ PreCondition
//      None
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================
AtmPressOffset::AtmPressOffset( const AtmPressOffset& rAtmPressOffset)
{
	CALL_TRACE("AtmPressOffset::AtmPressOffset( const AtmPressOffset& rAtmPressOffset)");

   	// $[TI2]
	offset_ = rAtmPressOffset.offset_ ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: operator=( const AtmPressOffset& rAtmPressOffset)
//
//@ Interface-Description
//		This method has AtmPressOffset as an argument and returns nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//      The object passed in is copied to the current object.
//---------------------------------------------------------------------
//@ PreCondition
//      None
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================
void
AtmPressOffset::operator=(const AtmPressOffset& rAtmPressOffset) 
{
	CALL_TRACE("AtmPressOffset::operator=(const AtmPressOffset& rAtmPressOffset)");

	if (this != &rAtmPressOffset)
	{
	   	// $[TI1]
		offset_ = rAtmPressOffset.offset_ ;
	}
	else
	{
    	CLASS_PRE_CONDITION( FALSE) ;
	}
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault()
//
//@ Interface-Description
//      This method takes four arguments: softFaultID, lineNumber, pFileName,
//      and pPredicate and is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//      'lineNumber' are essential pieces of information.  The 'pFileName'
//      and 'pPredicate' strings may be defaulted in the macro to reduce
//      code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
AtmPressOffset::SoftFault( const SoftFaultID  softFaultID,
						   const Uint32       lineNumber,
						   const char*        pFileName,
						   const char*        pPredicate)
{
    CALL_TRACE("SoftFault( softFaultID, lineNumber, pFileName, pPredicate)") ;
    
    FaultHandler::SoftFault( softFaultID, BD_IO_DEVICES, ATMPRESSOFFSET, lineNumber,
                             pFileName, pPredicate) ;
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//=====================================================================
//
//  Private Methods...
//
//=====================================================================




