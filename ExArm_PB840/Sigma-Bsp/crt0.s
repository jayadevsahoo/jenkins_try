        LIST
;=====================================================================
; This is a proprietary work to which Nellcor Puritan Bennett
; corporation of California claims exclusive right.  No part of this
; work may be used, disclosed, reproduced, stored in an information
; retrieval system, or transmitted by any means, electronic,
; mechanical, photocopying, recording, or otherwise without the prior
; written permission of Nellcor Puritan Bennett Corporation of
; California.
;
;     Copyright (c) 1995, Nellcor Puritan Bennett Corporation
;=====================================================================
;
; =========================== M O D U L E   D E S C R I P T I O N ====
;@ Filename: crt0.asm - Boot Process Entry Point
;---------------------------------------------------------------------
;@ Interface-Description
;  This is the entry point to the boot process.  
;---------------------------------------------------------------------
;@ Rationale
;  This module contains the "glue" logic between Kernel POST and
;  VRTX boot process.
;---------------------------------------------------------------------
;@ Implementation-Description
;  See Interface-Descriptions
;---------------------------------------------------------------------
;@ Fault-Handling
;  not applicable
;---------------------------------------------------------------------
;@ Restrictions
;  none
;---------------------------------------------------------------------
;@ Invariants
;  not applicable
;---------------------------------------------------------------------
;@ End-Preamble
;
;@ Version-Information
; @(#) $Header:   /840/Baseline/Sigma-Bsp/vcssrc/crt0.s_v   25.0.4.0   19 Nov 2013 14:29:24   pvcs  $
;
;@ Modification-Log
;
;  Revision: 002  By:  gdc      Date:  05-Sep-2000  DR Number: 5493
;       Project:  GUIComms
;       Description:
;       Changed to support two OS boot images, one for baseline boards
;		and one for Comms boards containing additional serial ports.
;
;  Revision: 001  By:  gdc      Date:  07-JUL-1997  DR Number: 2176
;       Project:  Sigma (R8027)
;       Description:
;             Initial version.
;=====================================================================
 
 
;====================== P R O C E D U R E   D E S C R I P T I O N ====
;@ Procedure:  _OsInit
;
;@ Interface-Description
;  The procedure is entry point to the boot load image.  POST branches
;  here using a well-known (fixed) address where this procedure is 
;  linked.  This procedure resets the stack pointer and calls 
;  boot_sigma_crt0() to initialize the boot process.
;
;  INPUT:    none
;
;  OUTPUT:   none
;
;---------------------------------------------------------------------
;@ Implementation-Description
;  See Interface-Description
;---------------------------------------------------------------------
;@ PreCondition
;  none
;---------------------------------------------------------------------
;@ PostCondition
;  none
;@ End-Procedure
;=====================================================================
 
		INCLUDE	kernel.inc

_crt0_enet_addr         EQU     $FF07FE10
_crt0_init_stktop       EQU     $8000

DELTA_OS_ADRS			EQU   $FF690000

CODE_SECTION    MACRO
        SECTION 9
        ENDM

DATA_SECTION    MACRO
        SECTION 14
        ENDM

        XREF      _bzero
        XREF      _boot_sys_crt0
        XREF      _boot_sigma_crt0

        XDEF      _crt0_cold_start
        XDEF      _crt0_enet_addr
        XDEF      _crt0_init_stktop


        CODE_SECTION

        IFDEF     SIGMA_DEVELOPMENT   ;{
        move.l    #_crt0_init_stktop,sp

; zero zerovars used by boot PROM and now needed by this OS boot
        move.l    #.sizeof.(zerovars),-(sp)
        move.l    #.startof.(zerovars),-(sp)
        jsr       _bzero
        addq.l    #8,sp

;  xtrace entry point - warm start works using this entry point
_crt0_cold_start:
        WRITE_LED  $81
        move.l    #_crt0_init_stktop,sp
        bra.l     _boot_sys_crt0      ; initializes boot exception vectors
                                      ; then calls boot_warm_start
        illegal
        ENDC                          ;}




        IFDEF     SIGMA_PRODUCTION    ;{
        
_crt0_base:
; at this point zerovar memory has been cleared by POST
        move.l    #_crt0_init_stktop,sp

; boot Sigma-Os if we're a BD or a single serial port GUI
		move.b   IO_REGISTER_1,d0
		andi.b   #IO1_BD_CPU,d0             ; GUI or BD?
		bne.s    _crt0_start_boot           ; // $[TI1.1] GUI
											; // $[TI1.2] BD
		move.b    NMI_SOURCE_REGISTER,d0
		andi.b    #NMI_REV_MASK,d0
		cmpi.b    #NMI_REV_COST_REDUCE,d0
		bne.s     _crt0_start_boot			; // $[TI2.1] Sigma-Os
		                             		; // $[TI2.2] Delta-Os 

; jump to this location in the Delta-Os containing additional i/o
		jmp.l     DELTA_OS_ADRS+_crt0_start_boot-_crt0_base
		
_crt0_start_boot:
        bra.l     _boot_sigma_crt0    ; uses Kernel exception vectors until
                                      ; VRTX and application boot

_crt0_cold_start:
        illegal                       ; should never branch here in production
        ENDC                          ;}


        END
