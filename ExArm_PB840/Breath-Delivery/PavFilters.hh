#ifndef PavFilters_HH
#define PavFilters_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2000, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: PavFilters - Filters used for PAV processing.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header::   /840/Baseline/Breath-Delivery/vcssrc/PavFilters.hhv   10.7   08/17/07 09:40:26   pvcs  
//
//@ Modification-Log
//
//  Revision: 001   By: syw   Date:  19-Jun-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//		PAV initial version.
//
//====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"

//@ Usage-Classes

//@ End-Usage

const Uint32 BUFF_SIZE = 10 ;

class PavFilters {
  public:
    PavFilters( void) ;
    ~PavFilters( void) ;

    inline void resetStatisticalFilter( void) ;
    inline void resetDirectionalFilter( const Real32 initValue) ;

    static void SoftFault(  const SoftFaultID softFaultID,
							const Uint32      lineNumber,
							const char*       pFileName  = NULL, 
			 				const char*       pPredicate = NULL) ;
			 				
	static Real32 SlopeEstimator( const Real32 input, Real32 &input_1,
						   Real32 &output_1, Real32 &output_2,
						   const Real32 gain) ;
	
	Real32 statisticalFilter( const Real32 patientData, const Real32 input) ;
	Real32 directionalFilter( const Real32 input, const Real32 alphaUp,
								const Real32 alphaDown) ;
	
  protected:

  private:
    PavFilters( const PavFilters&) ;  		// not implemented...
    void   operator=( const PavFilters&) ; 	// not implemented...

	//@ Data-Member: filteredInput_
	// filtered input via alpha filter
	Real32 filteredInput_ ;

	//@ Data-Member: outputBuffer_[BUFF_SIZE]
	// arrray to buffer the output of the statistical filter
	Real32 outputBuffer_[BUFF_SIZE] ;

	//@ Data-Member: index_
	// index into the outputBuffer_
	Uint32 index_ ;

	//@ Data-Member: sum_
	// running sum of contents in outputBuffer_
	Real32 sum_ ;

	//@ Data-Member: sumSquared_
	// running sum of squared contents in outputBuffer_ 
	Real32 sumSquared_ ;

	//@ Data-Member: bufferFull_
	// flag to indicate that buffer is full (i.e. BUFF_SIZE item has been stored)
	Boolean bufferFull_ ;

	//@ Data-Member: alphaUp_
	// alpha value for up directional filter
	Real32 alphaUp_ ;

	//@ Data-Member: alphaDown_
	// alpha value for down directional filter
	Real32 alphaDown_ ;

	//@ Data-Member: directionalFilterOutput_
	// output of directional filter
	Real32 directionalFilterOutput_ ;

} ;

#include "PavFilters.in"

#endif // PavFilters_HH 


