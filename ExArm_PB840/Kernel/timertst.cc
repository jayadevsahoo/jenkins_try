#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================ M O D U L E   D E S C R I P T I O N ====
//@ Filename:  timertst.cc - test code for the 82C54 timers
//
//---------------------------------------------------------------------
//@ Interface-Description
//
//  >Von
//    This module contains routines to test the two 82C54 timer devices.
//    A total of 5 independant timers are tested: 3 16 bit timers on
//    82C54 timer #1 and 2 - 16 bit and 1 - 32 bit timer on 82C54 timer
//    #2.
//
//  Free Functions:
//    TimerTest            -  Performs the tests
//    TimerModeTest        -  Verifies modes can be set and read
//    TimerCountTest       -  verifies a count can be set and read
//    TimerAccuracyTest    -  checks timer accuracy against Time of Day Clock
//
//@ Implementation-Description
//
//@ Restrictions
//
// The Time of Day Clock must be working and running.  This can be ensured
// by running this test after the Time of Day Clock test.
//
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
//
//@(#) $Header:   /840/Baseline/Kernel/vcssrc/timertst.ccv   25.0.4.0   19 Nov 2013 14:13:56   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005    By: Mitesh Raval   Date: 14-Oct-2010  SCR Number: 6671
//  Project: XENA2
//  Description:
//       Occurrences of DS1286 renamed to Delay.
//
//  Revision: 004    By: Gary Cederquist  Date: 26-Apr-1999  DCS Number: 5370
//    Project: 840 Cost Reduction
//    Description:
//       Corrected revision code conflict.
//
//  Revision: 003    By: Gary Cederquist  Date: 15-Dec-1998  DR Number: 5311
//    Project: 840 Cost Reduction
//    Description:
//       Initial version.
//
//  Revision: 002    By: Gary Cederquist  Date: 15-AUG-1997  DR Number: 1875
//    Project: Sigma (R8027)
//    Description:
//       Added testable item annotations.
//
//  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//=====================================================================

#include "timertst.hh"
#include "kernel.hh"
#include "postnv.hh"
#include "I82C54.hh"
#include "Delay.hh"
#include "CpuDevice.hh"
#include "MemoryMap.hh"
//TODO E600 #include "NmiSource.hh"
#include "Post_Library.hh"

#if defined(SIGMA_DEBUG)
#include <stdio.h>
#include "misc.hh"
#endif

static Criticality TimerModeTest(void);
static Criticality TimerCountTest(void);
static Criticality TimerAccuracyTest(void);
static Criticality TimerInterruptTest(void);
static void        DisableTimers(void);

static volatile Boolean Autovec3Seen_;
static volatile Boolean Autovec4Seen_;
static volatile Boolean Autovec6Seen_;

volatile Boolean timer_interrupt_seen;

//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Free-Function: TestTimerCount - utility for timer count test
//
//@ Interface-Description
//
//  This routine tests the ability of an 82C54 timer to be programmed with
//  a count that can then be read back.
//
//  Note: the timer cannot be stopped, therefore it is important to read
//  back the count as quickly as possible.  The test assumes some counts
//  will elapse.
//
//    Inputs:
//        TestTimerCount(  I82C54 * timer
//                       , Uint32 counterIndex
//                       , Uint16 initCount
//
//        timer        - pointer to the base address of the counter chip
//        counterIndex - counter number [0..2] of the I82C54
//        initCount    - the count to load and read back
//
//    Outputs: Returns: FALSE - failed
//                      TRUE  - passed
//
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//    None
//---------------------------------------------------------------------
//@ PostCondition
//    None
//
//@ End-Free-Function
//=====================================================================

#define  MAX_TIMER_TEST_COUNT  (16)

Boolean
TestTimerCount(I82C54RegMapAlt * pTimer, Uint32 counterIndex, Uint16 initCount)
{
	//TODO E600 port this
	return TRUE; //temp

/*    volatile Uint8 & counter_reg = pTimer->counterReg[counterIndex].counter;
    volatile Uint8 & control_reg = pTimer->control;

    register Uint8 latchCounter = counterIndex << 6;

    // load counter and go
    counter_reg = initCount & 0xff;
    counter_reg = (initCount >> 8) & 0xff;

    // latch the counter value
    control_reg = latchCounter;

    //  force CPU to synchronize bus access so read occurs after 
    //  writing to control register
    asm(" nop ");

    Uint16 finalCount  = counter_reg;       // LSB 
           finalCount += counter_reg << 8;  // MSB

UNIT_TEST_POINT(POSTUT_12)

    if (initCount - finalCount < 0)
    {
        // $[TI1.1]
        return FALSE;
    }
    // $[TI1.2]

    if (initCount - finalCount > MAX_TIMER_TEST_COUNT)
    {
        // $[TI2.1]
        return FALSE;
    }
    // $[TI2.2]

    return TRUE;*/
}


//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Free-Function:   TimerModeTest  - test the ability to set timer modes
//
//@ Interface-Description
//
// This function test the ability of the 82C54 timers to be programmed.
// A series of counter modes, read/write modes, and binary/BCD count
// modes are written to and read back from each of the 6 timers.
//
//    Inputs:  NONE
//
//    Outputs: NONE
//
//    Returns: PASSED, MAJOR
//
//---------------------------------------------------------------------
//@ Implementation-Description
//
// This function uses an array of mode values and an array of timer
// pointers to test each of the valid mode,r/w,bcd bit patterns that
// can be written into the mode registers of the 82C54.  For each test
// mode,r/w,BCD pattern:
//    1. the mode registers on each 82C54 are written
//    2. the Read Back command is used to latch the control register status
//    3. the mode,r/w,BCD bit patterns are read back and compared to
//       the values set
//
// if on any test, the mode,r/w,BCD value read back do not match the
// values written, the function return with statis of MAJOR to indicate
// that a failure has occured.
//
//---------------------------------------------------------------------
//@ PreCondition
//    None
//---------------------------------------------------------------------
//@ PostCondition
//    None
//
//@ End-Free-Function
//=====================================================================

static I82C54RegMap * const timers[] =
{
    (I82C54RegMap *)I82C541_BASE
};

static const Uint8 mode_pattern[] =
{             (3<<4)     //mode=0  rw=3  bcd=0
    ,(1<<1) + (3<<4)     //mode=1  rw=3  bcd=0
    ,(2<<1) + (3<<4)     //mode=2  rw=3  bcd=0
    ,(3<<1) + (3<<4)     //mode=3  rw=3  bcd=0
    ,(4<<1) + (3<<4)     //mode=4  rw=3  bcd=0
    ,(5<<1) + (3<<4)     //mode=5  rw=3  bcd=0
    ,(1<<1) + (1<<4)     //mode=1  rw=1  bcd=0
    ,(1<<1) + (2<<4)     //mode=1  rw=2  bcd=0
    ,(1<<1) + (3<<4) +1  //mode=1  rw=3  bcd=1
    ,(1<<1) + (3<<4)     //mode=1  rw=3  bcd=0
};

Criticality TimerModeTest(void)
{
   Uint8 timer_index,index,mode;
   Uint8 mode0,mode1,mode2;

   I82C54RegMap * timer;
   for (timer_index = 0 ; timer_index < countof(timers) ; timer_index++)
   {
      timer = timers[timer_index];
      for (index = 0 ; index < countof(mode_pattern) ; index++)
      {
         mode = mode_pattern[index];
         // **** set the counter control pattern
         timer->control = COUNTER_0 | mode;
         timer->control = COUNTER_1 | mode;
         timer->control = COUNTER_2 | mode;

UNIT_TEST_POINT(POSTUT_11)

         // **** set readback mode and latch current status of all counters
         timer->control = READ_BACK_STATUS | ALL_CTRS;

         // synchronize bus access so control reg is written before mode read
//TODO E600 port         asm(" nop ");

         mode0 = timer->counter_0 & STATUS_MASK;
         mode1 = timer->counter_1 & STATUS_MASK;
         mode2 = timer->counter_2 & STATUS_MASK;
         // **** read back the counter control pattern
         if ( (mode0 != mode) || (mode1 != mode) || (mode2 != mode) )
         {
            // $[TI1.1]
#if defined( SIGMA_DEBUG )
            dprintf("82C54 Timer Mode test failed:   Modes read = [%04X] [%04X] [%04X]\n"
                    "Desired mode= [%04X] timer index=[%d] pattern index=[%d]\n",
                     mode0, mode1, mode2, mode, timer_index, index);
#endif
            return MAJOR;
         }
         // $[TI1.2]
      }
   }

   return PASSED;
}


//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Free-Function:   TimerCountTest - test the timers count registers
//
//@ Interface-Description
//
// This function tests the ability of the CPU to write to and read from
// the 82C54 timers.
//
//    Inputs:  NONE
//
//    Outputs: NONE
//
//    Returns: PASSED, MAJOR
//---------------------------------------------------------------------
//@ Implementation-Description
//
// This function uses an array of count values and an array of timer
// pointers to test each timer can have it's count register written
// and read back.
//
// For each test count:
//
//    1. the timers on the 82C54 are set to mode=4 rw=3 bcd=0, which
//       means they will countdown from the time 16 bit counts are
//       loaded into their count registers.  The will count in binary
//       not Binary Coded Decimal.
//
//    2. The count registers are loaded and read back as quickly as
//       possible
//
//    3. Since the internal timers are always counting, a few counts
//       may have gone by between the write and the read back. If
//       the read back value is less than the written value by a specified
//       amount, the function returns with a status of MAJOR to indicate
//       that a failure has occured.
//
//---------------------------------------------------------------------
//@ PreCondition
//    None
//---------------------------------------------------------------------
//@ PostCondition
//    None
//
//@ End-Free-Function
//=====================================================================
const Uint16  timerTestCount[] = {0x0010,0xffff};

Criticality TimerCountTest(void)
{
   static const Uint8 mode = (4<<1) + (3<<4); //mode=4  rw=3  bcd=0

   for (Uint index = 0 ; index < countof(timerTestCount) ; index++)
   {
      Uint16 count = timerTestCount[index];
      for (Uint timer_index = 0 ; timer_index < countof(timers) ; timer_index++)
      {
         I82C54RegMap * timer = timers[timer_index];
         // **** set the timer modes
         timer->control = COUNTER_0 | mode;
         timer->control = COUNTER_1 | mode;
         timer->control = COUNTER_2 | mode;

         //  for each counter, set the count and read it back
         //  as quickly as possible

         if (   !TestTimerCount((I82C54RegMapAlt*)timer, 0, count)
             || !TestTimerCount((I82C54RegMapAlt*)timer, 1, count)
             || !TestTimerCount((I82C54RegMapAlt*)timer, 2, count)
            )
         {
             // $[TI1.1]
             return MAJOR;
         }
         // $[TI1.2]
      }
   }
   return PASSED;
}

//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Free-Function: TimerTestInterruptHandler -  flag AutoVector 6 interrupts
//
//@ Interface-Description
//
//  INPUTS: NONE
//
//  OUPUTS: NONE
//---------------------------------------------------------------------
//@ Implementation-Description
//
//  This handler sets the flag timer_interrupt_seen to TRUE, then
//  returns.
//
//---------------------------------------------------------------------
//@ PreCondition
//     none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Free-Function
//=====================================================================

//TODO E600 port
//interrupt void
void TimerTestInterruptHandler(void)
{
    CpuDevice::ResetAutovector6();
    timer_interrupt_seen = TRUE;
    // $[TI1]
}



//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Free-Function: TimerAccuracyTest - test the accuracy of a timer
//
//@ Interface-Description
//
// This function measures the frequency (accuracy) of the 82C54 counter 2.
// This counter is specifically tested since it is used to drive the
// operating system and breath delivery cycle times (ie. the 5ms timer).
// It measures the timer's accuracy against the Time of Day Clock. $[11043]
//
//    Inputs:  NONE
//
//    Outputs: NONE
//
//    Returns: NONE
//
//---------------------------------------------------------------------
//@ Implementation-Description
//
// This test:
//
//    1. programs the counter 2 to mode=2 r/w=3 bcd=0
//       That means the timers count down in binary (not binary coded decimal)
//       from the specified count starting at the time that the 16 bit count
//       is loaded into them.
//    2. The specified count is loaded into the counter except for the
//       last byte which, when loaded will start the counter.
//    3. Wait for a transition on the Time of Day clock 1/100th second tick.
//    4. Write the last byte. The counter latches the specified count into
//       the actual counters and start counting down.
//    5. Wait for a transition of the time of day clock.
//
//    Note: The DS1286 does not provide a true 10 millisec count in
//          the 1/100ths sec register.  The count is an "average" 100 Hz.
//          The time between updates of the 1/100th sec register are
//          usually 10 millisec, but occasionally one is short by
//          .24 milliseconds.
//
//          A possible measurement error of 2.4% is introduced by using
//          only one tick of the time of day clock.  However, since the
//          crystal oscillator driving the 82C54 counter should be
//          accurate to within 0.01%, this measurement inaccuracy is
//          acceptable.  A "short" clock will simply force a higher
//          accuracy (2.6%) instead of 5% for the nominal case.
//
//    6. The counter is latched using a read back command.
//
//    7. The number of counts that have elapsed in the timers is calculated.
//       if the count is high or low by an amount greater than the allowed
//       margins, this function returns with a status of MAJOR to
//       indicate the timers are out of spec.
//
//
//---------------------------------------------------------------------
//@ PreCondition
//    None
//---------------------------------------------------------------------
//@ PostCondition
//    None
//
//@ End-Free-Function
//=====================================================================
// ****************************************************************************
// ****          Calculate the limits of the counts expected.                **
// **** Note:                                                                **
// ****   The following #define calculations are (and must be) entirely      **
// ****   resolved at compile time.                                          **
// **** Warning:                                                             **
// ****   Be careful of the values for TEST_COUNT_LOW_WORD and               **
// ****   TEST_COUNT_HIGH_WORD. For Example TEST_COUNT_LOW_WORD cannot be 0. **
// ****   Also (TEST_COUNT_HIGH_WORD+1)*TEST_COUNT_LOW_WORD                  **
// ****   should be (at least) > (1+ACCURACY_PERCENT)*NUMBER_OF_CLOCKS.      **
// ****************************************************************************
#define TIMER_CLK                       (4e6)          //   4 Mhz
#define NUMBER_OF_TICKS_FOR_MEASUREMENT (1)
#define TIMER_MEASUREMENT_TIME          (.01*NUMBER_OF_TICKS_FOR_MEASUREMENT)
#define NUMBER_OF_CLOCKS        ((Uint32)(TIMER_CLK * TIMER_MEASUREMENT_TIME))

#define ACCURACY_PERCENT  (.05)  // +- 5%

#define STARTING_COUNT       (0xFFFF)
#define MAX_NUMBER_OF_CLOCKS ((Uint32)((1.0+ACCURACY_PERCENT)*NUMBER_OF_CLOCKS))
#define MIN_NUMBER_OF_CLOCKS ((Uint32)((1.0-ACCURACY_PERCENT)*NUMBER_OF_CLOCKS))

#define TIMER_INTERRUPT_VECTOR (30)

Criticality TimerAccuracyTest(void)
{
   Criticality    status = PASSED;
   I82C54RegMap * timer = (I82C54RegMap *)I82C541_BASE;

   Uint32 minCount = MIN_NUMBER_OF_CLOCKS;
   Uint32 maxCount = MAX_NUMBER_OF_CLOCKS;
   Uint32 finalCount;
   Uint32 elapsedCount;
   Boolean timerRolledOver;
   Boolean clockOk;

   //  Disable interrupts during setup
   DISABLE_INTERRUPTS();

   timer_interrupt_seen = FALSE;

#ifdef E600_840_TEMP_REMOVED
   Handler old_isr = InstallHandler(  TIMER_INTERRUPT_VECTOR
                                    , TimerTestInterruptHandler);
#endif

   // *****************************************************************
   // ****** Setup counter 2 of the 82C54                         *****
   // ******                                                      *****
   // ****** Write every thing to the counter but the last byte.  *****
   // ****** This will freeze the counter until the high order    *****
   // ****** byte is written.                                     *****
   // *****************************************************************
   timer->control = (Uint8)(COUNTER_2 | (2<<1) | (3<<4)); //mode=2 rw=3 bcd=0

   timer->counter_2 = ((Uint8)((STARTING_COUNT   ) & 0xff));

   // **** wait for a tick transition on the Time of Day Clock
   clockOk = Delay::WaitForTicks(1);

UNIT_TEST_POINT(POSTUT_13_1)
   if ( !clockOk )
   {
      // $[TI1.1]
      status = MAJOR;
   }
   else
   {
      // $[TI1.2]
      // **** clear any previous interrupts
      CpuDevice::ResetAutovector6();

      // **** write the MSB - that will start the timed interval
      timer->counter_2 = ((Uint8)((STARTING_COUNT>>8 )  & 0xff));

      CpuDevice::EnableAutovector(ENABLE_AUTOVEC_6);  // counter 2

      //  enable interrupts
      ENABLE_INTERRUPTS();

      // **** wait for tick transitions on the Time of Day Clock
      if (!Delay::WaitForTicks(NUMBER_OF_TICKS_FOR_MEASUREMENT))
      {
         // $[TI2.1]
         status = MAJOR;
      }
      else
      {
         // $[TI2.2]
         // **** set readback mode and latch current count of all counters
         timer->control = READ_BACK_COUNTS | ALL_CTRS;

         // **** construct the 16 bit number of counts elapsed
         finalCount  = timer->counter_2;
         finalCount += timer->counter_2<<8;

         timerRolledOver = timer_interrupt_seen;

         //  Disable interrupts
         DISABLE_INTERRUPTS();

         // **** reset any AutoVector interrupts that might have been set ****
         CpuDevice::ResetAutovector3();
         CpuDevice::ResetAutovector4();
         CpuDevice::ResetAutovector6();

         //  Mask all autovector interrupts
         CpuDevice::EnableAutovector(ENABLE_AUTOVEC_NONE);

         //  Disable counter 2 by initializing the LSB and not the MSB
         //  mode=2 rw=3 bcd=0
         timer->control = (Uint8)(COUNTER_2 | (2<<1) | (3<<4));
         timer->counter_2 = 0;

#ifdef E600_840_TEMP_REMOVED
         (void) InstallHandler( TIMER_INTERRUPT_VECTOR, old_isr );
#endif

         elapsedCount = STARTING_COUNT - finalCount;

UNIT_TEST_POINT(POSTUT_13)

         if (elapsedCount > maxCount)
         {
            // $[TI3.1]
            status = MAJOR;
         }
         // $[TI3.2]

         if (elapsedCount < minCount)
         {
            // $[TI4.1]
            status = MAJOR;
         }
         // $[TI4.2]

         if (timerRolledOver)
         {
            // $[TI5.1]
            status = MAJOR;
         }
         // $[TI5.2]
      }
   }
   ENABLE_INTERRUPTS();

   return status;
}


//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Free-Function:  TimerTest
//
//@ Interface-Description
//
// This function test thte 82C54 timers by performing a mode test, counter
// test, and accuracy test.
//
//    Inputs:  NONE
//
//    Outputs: NONE
//
//---------------------------------------------------------------------
//@ Implementation-Description
//
// This function call the three tests that make up the timer test suite.
// If any of them fail, this function returns with the appropriate
// status.
//
//---------------------------------------------------------------------
//@ PreCondition
//    None
//---------------------------------------------------------------------
//@ PostCondition
//    None
//
//@ End-Free-Function
//=====================================================================
Criticality   TimerTest(void)
{
	//TODO E600 port
  Criticality status = PASSED;

/*
  if ( CpuDevice::GetBoardRevision() != NMI_COST_REDUCTION )
  {										// $[TI5.1]
	CpuDevice::StrobeWatchdog();
	status = TimerModeTest();
	if (status == PASSED)
	{ 									// $[TI1.1]
	  CpuDevice::StrobeWatchdog();
	  status = TimerCountTest();
	  if (status == PASSED)
	  { 								// $[TI2.1]
		CpuDevice::StrobeWatchdog();
		status = TimerInterruptTest();
		if (status == PASSED)
		{ 								// $[TI3.1]
		  CpuDevice::StrobeWatchdog();
		  status = TimerAccuracyTest();
		} 								// $[TI3.2]
	  } 								// $[TI2.2]
	}									// $[TI1.2]
	DisableTimers();
  }										// $[TI5.2]
*/

  // obsolete test items $[TI4.1] $[TI4.2]

  return status;
}


//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Free-Function:  DisableTimers
//
//@ Interface-Description
//
//  Utility for disabling all 82C54 timers.  The timers are hung in
//  mode 2 indefinately after this is called.
//
//  Inputs:  NONE
//
//  Outputs: NONE
//
//---------------------------------------------------------------------
//@ Implementation-Description
//
//  This function effectively disables the timers by putting the timers
//  into mode2, without completing the operation.
//
//---------------------------------------------------------------------
//@ PreCondition
//    None
//---------------------------------------------------------------------
//@ PostCondition
//    None
//
//@ End-Free-Function
//=====================================================================
void DisableTimers(void)
{
   DISABLE_INTERRUPTS();

   Uint8          mode = (2<<1) + (3<<4);   // Mode 2, rw=3  bcd = 0
   I82C54RegMap * timer = (I82C54RegMap *)I82C541_BASE;

   timer->control = COUNTER_0 | mode;
   timer->counter_0 = 0;
   timer->control = COUNTER_1 | mode;
   timer->counter_1 = 0;
   timer->control = COUNTER_2 | mode;
   timer->counter_2 = 0;

   CpuDevice::ResetAutovector3();
   CpuDevice::ResetAutovector4();
   CpuDevice::ResetAutovector6();

   ENABLE_INTERRUPTS();
   // $[TI1]
}


//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Free-Function:   Autovec3Handler
//
//@ Interface-Description
//  This is the interrupt handler to process autovector level 3 interrupts
//  during the TimerInterruptTest.  It sets the Autovec3Seen_ TRUE.
//
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//    None
//---------------------------------------------------------------------
//@ PostCondition
//    None
//
//@ End-Free-Function
//=====================================================================

//TODO E600 port
//interrupt void
void Autovec3Handler(void)
{
    CpuDevice::ResetAutovector3();
    Autovec3Seen_ = TRUE;
    // $[TI1]
}

//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Free-Function:   Autovec4Handler
//
//@ Interface-Description
//  This is the interrupt handler to process autovector level 4 interrupts
//  during the TimerInterruptTest.  It sets the Autovec4Seen_ TRUE.
//
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//    None
//---------------------------------------------------------------------
//@ PostCondition
//    None
//
//@ End-Free-Function
//=====================================================================

//TODO E600 port
//interrupt void
void Autovec4Handler(void)
{
    CpuDevice::ResetAutovector4();
    Autovec4Seen_ = TRUE;
    // $[TI1]
}

//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Free-Function:   Autovec6Handler
//
//@ Interface-Description
//  This is the interrupt handler to process autovector level 6 interrupts
//  during the TimerInterruptTest.  It sets the Autovec6Seen_ TRUE.
//
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//    None
//---------------------------------------------------------------------
//@ PostCondition
//    None
//
//@ End-Free-Function
//=====================================================================

//TODO E600 port
//interrupt void
void Autovec6Handler(void)
{
    CpuDevice::ResetAutovector6();
    Autovec6Seen_ = TRUE;
    // $[TI1]
}


//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Free-Function:   TimerInterruptTest  - Test Timer Interrupt Capability
//
//@ Interface-Description
//  This test assures make sure all counters can generate an interrupt
//  within a specified time.  This ensures that the interrupts used to
//  time the watchdog reset circuit will occur.  This also ensures that
//  if one interrupt line is shorted to another, POST will not continue.
//  Disabling the watchdog during POST depends on the interrupt working
//  correctly.  Without the strobing interrupt, POST would enter a
//  continuous reset condition.  $[11044]
//
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//    None
//---------------------------------------------------------------------
//@ PostCondition
//    None
//
//@ End-Free-Function
//=====================================================================

enum
{
   TIMER_INTERRUPT_DELAY_COUNT = 50000   // about 15ms == 50000 with
                                         // instruction cache on
};

enum
{
      AUTOVEC3_VECTOR = 27
    , AUTOVEC4_VECTOR = 28
    , AUTOVEC6_VECTOR = 30
};

static Criticality
TimerInterruptTest(void)
{
   I82C54RegMap  * timer  = (I82C54RegMap *)I82C541_BASE;
   Criticality     status = PASSED;

UNIT_TEST_POINT(POSTUT_24)
   //  Disable interrupts during setup
   DISABLE_INTERRUPTS();

   Autovec3Seen_ = FALSE;
   Autovec4Seen_ = FALSE;
   Autovec6Seen_ = FALSE;

#ifdef E600_840_TEMP_REMOVED
   Handler old_isr3 = InstallHandler( AUTOVEC3_VECTOR, Autovec3Handler );
   Handler old_isr4 = InstallHandler( AUTOVEC4_VECTOR, Autovec4Handler );
   Handler old_isr6 = InstallHandler( AUTOVEC6_VECTOR, Autovec6Handler );
#endif

   // **** clear any AutoVector interrupts that may be set ****
   CpuDevice::ResetAutovector3();
   CpuDevice::ResetAutovector4();
   CpuDevice::ResetAutovector6();

   //  set the counters to expire within 1 ms
   //  counter 0 is clocked at 8Mhz
   timer->control   = (COUNTER_0 | (2<<1) | (3<<4)); //mode=2 rw=3 bcd=0
   timer->counter_0 = 0x40;   // LSB
   timer->counter_0 = 0x1f;   // MSB
   //  counter 1 is clocked at 8Mhz
   timer->control   = (COUNTER_1 | (2<<1) | (3<<4)); //mode=2 rw=3 bcd=0
   timer->counter_1 = 0x40;   // LSB
   timer->counter_1 = 0x1f;   // MSB
   //  counter 2 is clocked at 4Mhz
   timer->control   = (COUNTER_2 | (2<<1) | (3<<4)); //mode=2 rw=3 bcd=0
   timer->counter_2 = 0xa0;   // LSB
   timer->counter_2 = 0x0f;   // MSB

   CpuDevice::EnableAutovector(  ENABLE_AUTOVEC_3    // counter 0
                               | ENABLE_AUTOVEC_4    // counter 1
                               | ENABLE_AUTOVEC_6 ); // counter 2

   ENABLE_INTERRUPTS();

UNIT_TEST_POINT(POSTUT_24_1)
   //  error if interrupts occur immediately
   if ( Autovec3Seen_ || Autovec4Seen_ || Autovec6Seen_ )
   {
       // $[TI1.1]
       status = MAJOR;
   }
   else
   {
       // $[TI1.2]
       for (  int i=TIMER_INTERRUPT_DELAY_COUNT
            ; i>0 && (!Autovec3Seen_ || !Autovec4Seen_ || !Autovec6Seen_)
            ; i--)
       {
           // delay
       }

       DISABLE_INTERRUPTS();

UNIT_TEST_POINT(POSTUT_24_2)
       if ( !Autovec3Seen_ || !Autovec4Seen_ || !Autovec6Seen_ )
       {
           // $[TI2.1]
           status = MAJOR;
       }
       // $[TI2.2]

   }

   CpuDevice::EnableAutovector( ENABLE_AUTOVEC_NONE );

   // **** reset any AutoVector interrupts that might have been set ****
   CpuDevice::ResetAutovector3();
   CpuDevice::ResetAutovector4();
   CpuDevice::ResetAutovector6();

#ifdef E600_840_TEMP_REMOVED
   InstallHandler( AUTOVEC3_VECTOR, old_isr3 );
   InstallHandler( AUTOVEC4_VECTOR, old_isr4 );
   InstallHandler( AUTOVEC6_VECTOR, old_isr6 );
#endif

   ENABLE_INTERRUPTS();

   return status;
}
