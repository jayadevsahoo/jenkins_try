
#ifndef Uint64_HH
#define Uint64_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sotred in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class:  Uint64 - Unsigned 64-bit integer structure.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Utilities/vcssrc/Uint64.hhv   25.0.4.0   19 Nov 2013 14:35:16   pvcs  $
//
//@ Modification-Log
//
//   Revision: 002   By: sah   Date: 06-Feb-1996   DCS Number: 674
//   Project:  Sigma (R8027)
//   Description:
//	Removed 'ifndef' and 'endif' from include line.
//  
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//====================================================================

#include "Sigma.hh"

//@ Usage-Classes
//@ End-Usage


//@ Type:  Uint64
// This defines a unsigned, 64-bit "integer", which is the grouping of two
// unsigned, 32-bit integers.
struct Uint64
{
  //@ Data-Member:  upperLongWord_
  // This contains the value of the upper long word.
  Uint32  upperLongWord;

  //@ Data-Member:  lowerLongWord_
  // This contains the value of the lower long word.
  Uint32  lowerLongWord;
};


#endif // Uint64_HH 
