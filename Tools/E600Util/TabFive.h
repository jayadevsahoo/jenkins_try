#pragma once

#include "TabOne.h"
#include "BdTxPacket.h"

struct PC_GainsStruct_t
{
	float ki;
	float kp;
};
struct PC_GainsPacket_t
{
	UINT messageId;
	UINT dataSize;
	PC_GainsStruct_t data;
};

struct FC_FactorsStruct_t
{
	float aFactor;
	float bFactor;
	float cFactor;
	float dFactor;
};
struct FC_FactorsPacket_t
{
	UINT messageId;
	UINT dataSize;
	FC_FactorsStruct_t data;
};

struct EVC_GainsStruct_t
{
	float ki;
	float kp;
};
struct EVC_GainsPacket_t
{
	UINT messageId;
	UINT dataSize;
	EVC_GainsStruct_t data;
};

struct PEEP_GainsStruct_t
{
	float ki;
};
struct PEEP_GainsPacket_t
{
	UINT messageId;
	UINT dataSize;
	PEEP_GainsStruct_t data;
};

struct PID_GainsStruct_t
{
	float ki;
	float kp;
	float kd;
};
struct PID_GainsPacket_t
{
	UINT messageId;
	UINT dataSize;
	PID_GainsStruct_t data;
};

struct PBP_GainsStruct_t
{
	float ki;
	float kp;
};
struct PBP_GainsPacket_t
{
	UINT messageId;
	UINT dataSize;
	PBP_GainsStruct_t data;
};

struct ALL_GainsStruct_t
{
	PC_GainsStruct_t PcGains;
	FC_FactorsStruct_t FcFactors;
	EVC_GainsStruct_t EvGains;
	PEEP_GainsStruct_t PeepGains;
	PID_GainsStruct_t PidGains;
	PBP_GainsStruct_t PbpGains;
};
struct ALL_GainsPacket_t
{
	UINT messageId;
	UINT dataSize;
	ALL_GainsStruct_t data;
};

struct GainRequestPacket_t
{
	UINT16 messageId;
	UINT16 dataSize;
};

struct GainEnablePacket_t
{
	UINT16 messageId;
	UINT16 dataSize;
};

union Uinon32bits_t
{
	float	floatValue;
	INT		int32Value;
};

#define NUM_WRITE_DEBUG_VARS	5
#define NUM_READ_DEBUG_VARS		5

struct WriteDebugPacket_t
{
	UINT16 messageId;
	UINT16 dataSize;
	Uinon32bits_t debugArr[NUM_WRITE_DEBUG_VARS];
};

struct ReadDebugPacket_t
{
	UINT16 messageId;
	UINT16 dataSize;
	Uinon32bits_t debugArr[NUM_READ_DEBUG_VARS];
};

enum CEditGainIndex_t
{
	FIRST_GAIN_INDEX=0,
	PC_KI=FIRST_GAIN_INDEX,
	PC_KP,
	FC_AFACTOR,
	FC_BFACTOR,
	FC_CFACTOR,
	FC_DFACTOR,
	EV_KI,
	EV_KP,
	PEEP_KI,
	PID_KI,
	PID_KP,
	PID_KD,
	PBP_KI,
	PBP_KP,
	NUM_CEDIT_GAIN_INDEXES
};

// TabFive dialog

class CTabFive : public CDialog
{
	DECLARE_DYNAMIC(CTabFive)

public:
	CTabFive(CWnd* pParent);   // standard constructor
	virtual ~CTabFive();

// Dialog Data
	enum { IDD = IDD_DIALOG_TAB5 };

	CTabOne *pTabOne;

protected:
	virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	void SendPacketToBD(char *pBuff, int Size);
	void SaveGainFile();

	DECLARE_MESSAGE_MAP()

	CEdit CEditGains[NUM_CEDIT_GAIN_INDEXES];
	CEdit CEditWriteDebugs[NUM_WRITE_DEBUG_VARS];
	CEdit CEditReadDebugs[NUM_WRITE_DEBUG_VARS];
	CString GainFilePath;
	CStdioFile GainFile;

	afx_msg LRESULT OnAllGainsReceived(WPARAM x, LPARAM y);

public:
	afx_msg void OnBnClickedButtonPcUpdate();
	afx_msg void OnBnClickedButtonFcUpdate();
	afx_msg void OnBnClickedButtonEvUpdate();
	afx_msg void OnBnClickedButtonPeepUpdate();
	afx_msg void OnBnClickedButtonPidUpdate();
	afx_msg void OnBnClickedButtonAllGainsUpdate();
	afx_msg void OnBnClickedButtonAllGainsRead();
	afx_msg void OnBnClickedButtonPbpUpdate();
	afx_msg void OnBnClickedButtonLoadGainFile();
	afx_msg void OnBnClickedButtonSaveGainFile();
	afx_msg void OnBnClickedButtonSaveNewGainFile();
	afx_msg void OnBnClickedButtonEnableGainSetting();
	afx_msg void OnBnClickedButtonSetDb();
};
