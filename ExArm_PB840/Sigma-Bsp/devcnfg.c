//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1998, Puritan-Bennett Corporation
//=====================================================================

//============================ M O D U L E   D E S C R I P T I O N ====
//@ Filename: devcnfg.c - VRTX Device Configuration
//
//---------------------------------------------------------------------
//@ Interface-Description
//
//  This file contains the data structures and initialization sequences
//  for the VRTX device drivers defined and used by the 840 application.
//  This file is contained in the Sigma-Bsp subsystem, but it is compiled
//  and linked into the boot image in the Kernel-Os and Sigma-Os
//  subsystems. This file is derived from a template provided by Microtec
//  Research. As much of the original template is maintained so future
//  versions may be more easily compared and updated as required.
//
//  By design, this file contains all the specific device addresses as
//  part of the device configuration. It does not depend on any other
//  source other than this file for this information. The device driver
//  header files only contain information specific to the device and
//  the device driver, but not the configuration of that device within
//  the target system.
//
//  Free Functions:
//
//	logio_device_install - Called during the VRTX boot process to
//							install and initialize the VRTX device
//							drivers.
//
//---------------------------------------------------------------------
//@ Rationale
//	This module provides the required definition and initialization
//  code for VRTX device drivers.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ Fault Handling
//  Faults during VRTX boot are handled by POST exception handlers.
//---------------------------------------------------------------------
//@ Restrictions
//    none
//---------------------------------------------------------------------
//@ Invariants
//    None
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Sigma-Bsp/vcssrc/devcnfg.c_v   25.0.4.0   19 Nov 2013 14:29:24   pvcs  $
//
//@ Modification-Log
//
//   Revision: 006  By: Gary Cederquist  Date: 11-Aug-2010  SCR Number: 6663
//      Project: MAT
//      Description:
//         Changed to support serial channel reset during port init. Removed
//         unused code.
//
//   Revision: 005  By: Gary Cederquist  Date: 24-Mar-2008  SCR Number: 6430
//      Project: TREND2
//      Description:
//         Changed baud rate to 38400 for serial console.
//
//   Revision: 004  By: Gary Cederquist  Date: 22-Mar-2000  DR Number: 5493
//      Project: GuiComms
//      Description:
//         Add GuiComms functionality. Additional serial ports.
//
//   Revision 003   By: Gary Cederquist  Date: 15-DEC-1998  DR Number: 5311
//      Project:   840 Cost Reduction
//      Description:
//         Initial version. Included FPGA timer initialization.
//
//   Revision 002   By: Gary Cederquist  Date: 08-JUL-1997  DR Number: 2332
//      Project:   Sigma   (R8027)
//      Description:
//         Changed to use Spectra 4.Aaa BSP template.
//
//   Revision 001   By: Gary Cederquist  Date: 08-JUL-1997  DR Number: 2176
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version.
//
//=====================================================================


/***************************************************************************
*
*		Copyright (c) 1992-1997 MICROTEC - A Mentor Graphics Company
*
*	All rights reserved. MICROTEC's source code is an unpublished
*	work and the use of a copyright notice does not imply otherwise.
*	This source code contains confidential, trade secret material of
*	MICROTEC. Any attempt or participation in deciphering,
*	decoding, reverse engineering or in any way altering the source code
*	is strictly prohibited, unless the prior written consent of
*	MICROTEC is obtained.
*
*
*	Module Name:		devcnfg.c
*
*	Identification:		devcnfg.c for mo302enf
*
*	History:
*
*	Date		Name			Description
*	----------------------------------------------------------------------
*	1994-11-28	Siprian Rodrigues	divided board.c into 2 files:
*						board.c and devcnfg.c
*	1996-04-26	Cindy Gin		added memory console to help
						with debugging
*	1997-01-03	D.N. Lynx Crowe		cleaned up for chicago release
*	1997-02-12	D.N. Lynx Crowe		changed mem_console_dev refs
*						to logio_memcons_1_id
*
****************************************************************************
*/

/*
** File:
**
**    devcnfg.c
**
** Contents:
**
**    This file will define the devices that will be used in the sigma
**    Board Support Package.
**
**    For every device on the board, five general items are required:
**       1) ROM (configuration)
**       2) RAM (device descriptor and device specific RAM)
**       3) method (an association of interface code and device descriptor RAM)
**       4) id ( a pointer to a method)
**       5) install function (called in logio_board_init() )
**
**    This file will create the structures required for every device.  For each
**    device, an install function will be placed in logio_device_install().
**
**    This file will define the seven required logio functions for the board.
**
**    This file will contain any "glue" code required to connect device drivers
**    into the board environment.
**
**
**    These sections exist:
**       development only defines
**       include files
**       serial devices
**       ethernet devices
**       timer devices
**       serial glue logic functions
**       ethernet glue logic functions
**
*/


/* ----------------------------------------------------------------------------
**   INSTRUCTION:  update the following comments with board specifics.
**   This is just a good thing to do for people in here after you...
** --------------------------------------------------------------------------*/

/*
** The board logio devices currently supported are:
**
**    1. TIMER_1
**    2. ETHER_1
**    3. SERIAL_1
**
** The board has the following devices:
**
**    1. in82596a Ethernet controller
**    2. zi8530 serial controller chip (has 2 ports)
**    3. in8254 countdown timer (has 3 counters)
**
** The mapping of physical devices to logical devices is as follows:
**
**    1. TIMER_1....... in8254
**    2. ETHER_1....... in82596a
**    3. SERIAL_1...... zi8530
**
** Each *physical device* has associated with it a "device
** descriptor".
**
** The declarations below are for the 4 *physical devices* listed
** above.
**
** If you wish to see what information the "device descriptors"
** have in them, refer to the following device files:
**
**    1. For info on ETHER, look at in82596a.h and in82596a.c
**    2. For info on SERIAL, look at zi8530.h and zi8530.c
**    3. For info on TIMER, look at in8254a.h and in8254a.c
*/


/******************************************************************************
*********************** DEVELOPMENT DEFINITIONS   *****************************
******************************************************************************/
#define BSP 1

#ifdef BSPBUILDER
#include "bspbuild.h"                   /* this file is only for bspbuilder testing */
#endif
                                        /* used in Spectra 3.1.2 and above */

#ifdef VME_SM
#include <shmem_1.h>
#include <vme_sm.h>
#endif

#if SERIAL_POLL_TEST == 1
#define ETHER_DEVICE                    0
#define SERIAL_DEVICE                   1
#define SERIAL_INTERRUPT                0
#define TIMER_DEVICE                    0
#define SOFTWARE_TIMER_DEVICE           1
#define BUS_INIT                        0
#define OTHER_INIT                      0
#define BRIDGE                          0
#endif


#if SERIAL_INTERRUPT_TEST == 1
#define ETHER_DEVICE                    0
#define SERIAL_DEVICE                   1
#define SERIAL_INTERRUPT                1
#define TIMER_DEVICE                    0
#define SOFTWARE_TIMER_DEVICE           1
#define BUS_INIT                        0
#define OTHER_INIT                      0
#define BRIDGE                          0
#endif


#if SERIAL_BRIDGE == 1
#define ETHER_DEVICE                    0
#define SERIAL_DEVICE                   1
#define SERIAL_INTERRUPT                1
#define TIMER_DEVICE                    0
#define SOFTWARE_TIMER_DEVICE           1
#define BUS_INIT                        0
#define OTHER_INIT                      1
#define BRIDGE                          1
#endif


#if ETHER_POLL_TEST == 1
#define ETHER_DEVICE                    1
#define SERIAL_DEVICE                   0
#define SERIAL_INTERRUPT                0
#define TIMER_DEVICE                    0
#define SOFTWARE_TIMER_DEVICE           0
#define BUS_INIT                        0
#define OTHER_INIT                      0
#define BRIDGE                          0
#endif


#if ETHER_BRIDGE == 1
#define ETHER_DEVICE                    1
#define SERIAL_DEVICE                   0
#define SERIAL_INTERRUPT                0
#define TIMER_DEVICE                    0
#define SOFTWARE_TIMER_DEVICE           1
#define BUS_INIT                        0
#define OTHER_INIT                      0
#define BRIDGE                          1
#endif

#if SERIAL_CONSOLE_TEST == 1
#define ETHER_DEVICE                    0
#define SERIAL_DEVICE                   1
#define SERIAL_INTERRUPT                0
#define TIMER_DEVICE                    0
#define SOFTWARE_TIMER_DEVICE           0
#define BUS_INIT                        0
#define OTHER_INIT                      0
#define BRIDGE                          0
#endif


#if TIMER_TEST == 1
#define ETHER_DEVICE                    0
#define SERIAL_DEVICE                   0
#define SERIAL_INTERRUPT                0
#define TIMER_DEVICE                    1
#define SOFTWARE_TIMER_DEVICE           0
#define BUS_INIT                        0
#define OTHER_INIT                      0
#define BRIDGE                          0
#endif

#if BSP == 1
#define ETHER_DEVICE                    1
#define SERIAL_DEVICE                   1
#define SERIAL_INTERRUPT                1
#define TIMER_DEVICE                    1
#define SOFTWARE_TIMER_DEVICE           0
#define BUS_INIT                        0
#define OTHER_INIT                      0
#define BRIDGE                          1
#endif


#if SERIAL_DEVICE == 1
#include "zi8530.h"
#endif

#if ETHER_DEVICE == 1
#include "in82596a.h"
#endif

#if TIMER_DEVICE == 1
#include "in8254a.h"
#include "PbTimer.h"
#endif

#if SOFTWARE_TIMER_DEVICE == 1
#include "sw_timer.h"
#endif


#if MEM_CONSOLE == 1
#include "memcons.h"
#include "serial_2.h"
#endif


/******************************************************************************
****************************  SERIAL    DEVICES   *****************************
******************************************************************************/

#if SERIAL_DEVICE == 1

/* ----------------------------------------------------------------------------
**   INSTRUCTION:  Add your serial constants here.
**   This may include interrupt vector values & port addresses.
** --------------------------------------------------------------------------*/

#define P_IO_REGISTER_1  ((volatile unsigned char*)(0xffbe8000))
#define BD_CPU_BIT       (0x80)

#define ZI8530_1_BASE_VECTOR        0x50
#define ZI8530_1_CH2_TX_VECTOR      0x50  /* zi8530 port B transmit interrupt vector      */
#define ZI8530_1_CH2_EXT_VECTOR     0x52  /* zi8530 port B external/status change vector  */
#define ZI8530_1_CH2_RX_VECTOR      0x54  /* zi8530 port B receive interrupt vector       */
#define ZI8530_1_CH2_RXE_VECTOR     0x56  /* zi8530 port B receive error interrupt vector */
#define ZI8530_1_CH1_TX_VECTOR      0x58  /* zi8530 port A transmit interrupt vector      */
#define ZI8530_1_CH1_EXT_VECTOR     0x5A  /* zi8530 port A external/status change vector  */
#define ZI8530_1_CH1_RX_VECTOR      0x5C  /* zi8530 port A receive interrupt vector       */
#define ZI8530_1_CH1_RXE_VECTOR     0x5E  /* zi8530 port A receive error interrupt vector */
#define ZI8530_1_BASE_ADDR          (0xFFBE8021) /* SCC base address */
#define ZI8530_1_CH2_CTRL_PORT      (ZI8530_1_BASE_ADDR + 0x00) /* port B control */
#define ZI8530_1_CH2_DATA_PORT      (ZI8530_1_BASE_ADDR + 0x02) /* port B data */
#define ZI8530_1_CH1_CTRL_PORT      (ZI8530_1_BASE_ADDR + 0x04) /* port A control */
#define ZI8530_1_CH1_DATA_PORT      (ZI8530_1_BASE_ADDR + 0x06) /* port A data */

#define ZI8530_2_BASE_VECTOR        0x60
#define ZI8530_2_CH2_TX_VECTOR      0x60  /* zi8530 port B transmit interrupt vector      */
#define ZI8530_2_CH2_EXT_VECTOR     0x62  /* zi8530 port B external/status change vector  */
#define ZI8530_2_CH2_RX_VECTOR      0x64  /* zi8530 port B receive interrupt vector       */
#define ZI8530_2_CH2_RXE_VECTOR     0x66  /* zi8530 port B receive error interrupt vector */
#define ZI8530_2_CH1_TX_VECTOR      0x68  /* zi8530 port A transmit interrupt vector      */
#define ZI8530_2_CH1_EXT_VECTOR     0x6A  /* zi8530 port A external/status change vector  */
#define ZI8530_2_CH1_RX_VECTOR      0x6C  /* zi8530 port A receive interrupt vector       */
#define ZI8530_2_CH1_RXE_VECTOR     0x6E  /* zi8530 port A receive error interrupt vector */
#define ZI8530_2_BASE_ADDR          (0xFFBE9021) /* SCC base address */
#define ZI8530_2_CH2_CTRL_PORT      (ZI8530_2_BASE_ADDR + 0x00) /* port B control */
#define ZI8530_2_CH2_DATA_PORT      (ZI8530_2_BASE_ADDR + 0x02) /* port B data */
#define ZI8530_2_CH1_CTRL_PORT      (ZI8530_2_BASE_ADDR + 0x04) /* port A control */
#define ZI8530_2_CH1_DATA_PORT      (ZI8530_2_BASE_ADDR + 0x06) /* port A data */

/* ----------------------------------------------------------------------------
**   INSTRUCTION:  Check the order of the sections int zi8530_config_t for your
**   device carefully.  This is the default order, but older device drivers
**   may not adhere to this standard.  See zi8530.h for details.
** --------------------------------------------------------------------------*/

//  <<<<<<<<<<   FIRST SERIAL PORT   >>>>>>>>>>>

const zi8530_config_t zi8530_config_1_rom = {

	/* serial_config_t (from serial_2.h) */
	{

#if SERIAL_POLL_TEST == 1 || SERIAL_INTERRUPT_TEST == 1 || SERIAL_BRIDGE == 1
		serial_packet_device,               /* the default uses serial_1 for a bridge */
#else
		serial_tty_device,
#endif
		10000000,                           /* clock speed in Mhz */
		38400,                              /* baud rate cnt */
		PARITY_NONE,                        /* parity */
		BITS_8,                             /* data bits */
		STOP_BITS_1,                        /* stop bit(s) */
		ZI8530_1_CH1_RX_VECTOR,             /* zi8530 receive interrupt vector */
		ZI8530_1_CH1_TX_VECTOR,             /* zi8530 transmit interrupt vector */
		ZI8530_1_CH1_RXE_VECTOR,            /* zi8530 receive error interrupt vector */
		(void*) 0                           /* void pointer for interface expansion */
	},

	/* zi8530_reg_map_t (from zi8530.h) */
	{
        (BYTE *) ZI8530_1_CH1_CTRL_PORT,  // control port
        (BYTE *) ZI8530_1_CH1_DATA_PORT   // data port
	},

	/* ----------------------------------------------------------------------
	**   INSTRUCTION:
	**      Fill in the board specific function handlers for the device.
	**      These are for any board specific functions required during
	**      register accesses or functions to interface to an external
	**      interrupt controller.  Refer to extio_1.h.
	** --------------------------------------------------------------------*/

	/* extern_fops_t from extio_1.h */
	{
		0,                      /* register write byte */
		0,                      /* register write word */
		0,                      /* register write long */
		0,                      /* register read  byte */
		0,                      /* register read  word */
		0,                      /* register read  long */
		0,                      /* int_strt function pointer */
		0,                      /* int_end function pointer */
		0,                      /* board specific int disable handling */
		0,                      /* board specific int enable handling */
		0
	},


	/* fill in the specific initialization */
	/* zi8530_specific_t (from zi8530.h) */
	{
        ZI8530_1_BASE_VECTOR,       // base vector for the zi8530 chip
        0,                          // shadow wr1, always init to zero
        BAUD_RATE_GENERATOR,        // DO NOT CHANGE
        SCC_CHANNEL_A
	}
};

serial_dev_desc_t	serial_1_dev_desc;

zi8530_config_t		zi8530_config_1_ram;

const logio_method_t	logio_serial_1_method = {

      &logio_serial_fops,
      (void *)&serial_1_dev_desc
};

const logio_device_id_t	logio_serial_1_id = &logio_serial_1_method;

//  <<<<<<<<<< END FIRST SERIAL PORT >>>>>>>>>>>

//  <<<<<<<<<<  SECOND SERIAL PORT   >>>>>>>>>>>

const zi8530_config_t zi8530_config_2_rom = {

	/* serial_config_t (from serial_2.h) */
	{

#if SERIAL_POLL_TEST == 1 || SERIAL_INTERRUPT_TEST == 1 || SERIAL_BRIDGE == 1
		serial_packet_device,               /* the default uses serial_1 for a bridge */
#else
		serial_tty_device,
#endif
		10000000,                           /* clock speed in Mhz */
		38400,                              /* baud rate cnt */
		PARITY_NONE,                        /* parity */
		BITS_8,                             /* data bits */
		STOP_BITS_1,                        /* stop bit(s) */
		ZI8530_1_CH2_RX_VECTOR,             /* zi8530 receive interrupt vector */
		ZI8530_1_CH2_TX_VECTOR,             /* zi8530 transmit interrupt vector */
		ZI8530_1_CH2_RXE_VECTOR,            /* zi8530 receive error interrupt vector */
		(void*) 0                           /* void pointer for interface expansion */
	},

	/* zi8530_reg_map_t (from zi8530.h) */
	{
        (BYTE *) ZI8530_1_CH2_CTRL_PORT,  // control port
        (BYTE *) ZI8530_1_CH2_DATA_PORT   // data port
	},

	/* ----------------------------------------------------------------------
	**   INSTRUCTION:
	**      Fill in the board specific function handlers for the device.
	**      These are for any board specific functions required during
	**      register accesses or functions to interface to an external
	**      interrupt controller.  Refer to extio_1.h.
	** --------------------------------------------------------------------*/

	/* extern_fops_t from extio_1.h */
	{
		0,                      /* register write byte */
		0,                      /* register write word */
		0,                      /* register write long */
		0,                      /* register read  byte */
		0,                      /* register read  word */
		0,                      /* register read  long */
		0,                      /* int_strt function pointer */
		0,                      /* int_end function pointer */
		0,                      /* board specific int disable handling */
		0,                      /* board specific int enable handling */
		0
	},


	/* fill in the specific initialization */
	/* zi8530_specific_t (from zi8530.h) */
	{
        ZI8530_1_BASE_VECTOR,       // base vector for the zi8530 chip
        0,                          // shadow wr1, always init to zero
        BAUD_RATE_GENERATOR,        // DO NOT CHANGE
        SCC_CHANNEL_B
	}
};

serial_dev_desc_t	serial_2_dev_desc;

zi8530_config_t		zi8530_config_2_ram;

const logio_method_t	logio_serial_2_method = {

      &logio_serial_fops,
      (void *)&serial_2_dev_desc
};

const logio_device_id_t	logio_serial_2_id = &logio_serial_2_method;

//  <<<<<<<<<< END SECOND SERIAL PORT >>>>>>>>>>

//  <<<<<<<<<<  THIRD SERIAL PORT    >>>>>>>>>>>

const zi8530_config_t zi8530_config_3_rom = {

	/* serial_config_t (from serial_2.h) */
	{

#if SERIAL_POLL_TEST == 1 || SERIAL_INTERRUPT_TEST == 1 || SERIAL_BRIDGE == 1
		serial_packet_device,               /* the default uses serial_1 for a bridge */
#else
		serial_tty_device,
#endif
		10000000,                           /* clock speed in Mhz */
		38400,                              /* baud rate cnt */
		PARITY_NONE,                        /* parity */
		BITS_8,                             /* data bits */
		STOP_BITS_1,                        /* stop bit(s) */
		ZI8530_2_CH1_RX_VECTOR,             /* zi8530 receive interrupt vector */
		ZI8530_2_CH1_TX_VECTOR,             /* zi8530 transmit interrupt vector */
		ZI8530_2_CH1_RXE_VECTOR,            /* zi8530 receive error interrupt vector */
		(void*) 0                           /* void pointer for interface expansion */
	},

	/* zi8530_reg_map_t (from zi8530.h) */
	{
        (BYTE *) ZI8530_2_CH1_CTRL_PORT,  // control port
        (BYTE *) ZI8530_2_CH1_DATA_PORT   // data port
	},

	/* ----------------------------------------------------------------------
	**   INSTRUCTION:
	**      Fill in the board specific function handlers for the device.
	**      These are for any board specific functions required during
	**      register accesses or functions to interface to an external
	**      interrupt controller.  Refer to extio_1.h.
	** --------------------------------------------------------------------*/

	/* extern_fops_t from extio_1.h */
	{
		0,                      /* register write byte */
		0,                      /* register write word */
		0,                      /* register write long */
		0,                      /* register read  byte */
		0,                      /* register read  word */
		0,                      /* register read  long */
		0,                      /* int_strt function pointer */
		0,                      /* int_end function pointer */
		0,                      /* board specific int disable handling */
		0,                      /* board specific int enable handling */
		0
	},


	/* fill in the specific initialization */
	/* zi8530_specific_t (from zi8530.h) */
	{
        ZI8530_2_BASE_VECTOR,       // base vector for the zi8530 chip
        0,                          // shadow wr1, always init to zero
        BAUD_RATE_GENERATOR,        // DO NOT CHANGE
        SCC_CHANNEL_A
	}
};

serial_dev_desc_t	serial_3_dev_desc;

zi8530_config_t		zi8530_config_3_ram;

const logio_method_t	logio_serial_3_method = {

      &logio_serial_fops,
      (void *)&serial_3_dev_desc
};

logio_device_id_t	logio_serial_3_id = &logio_serial_3_method;

//  <<<<<<<<<< END THIRD SERIAL PORT  >>>>>>>>>>
//  <<<<<<<<<<  FOURTH SERIAL PORT    >>>>>>>>>>>

const zi8530_config_t zi8530_config_4_rom = {

	/* serial_config_t (from serial_2.h) */
	{

#if SERIAL_POLL_TEST == 1 || SERIAL_INTERRUPT_TEST == 1 || SERIAL_BRIDGE == 1
		serial_packet_device,               /* the default uses serial_1 for a bridge */
#else
		serial_tty_device,
#endif
		10000000,                           /* clock speed in Mhz */
		38400,                              /* baud rate cnt */
		PARITY_NONE,                        /* parity */
		BITS_8,                             /* data bits */
		STOP_BITS_1,                        /* stop bit(s) */
		ZI8530_2_CH2_RX_VECTOR,             /* zi8530 receive interrupt vector */
		ZI8530_2_CH2_TX_VECTOR,             /* zi8530 transmit interrupt vector */
		ZI8530_2_CH2_RXE_VECTOR,            /* zi8530 receive error interrupt vector */
		(void*) 0                           /* void pointer for interface expansion */
	},

	/* zi8530_reg_map_t (from zi8530.h) */
	{
        (BYTE *) ZI8530_2_CH2_CTRL_PORT,  // control port
        (BYTE *) ZI8530_2_CH2_DATA_PORT   // data port
	},

	/* ----------------------------------------------------------------------
	**   INSTRUCTION:
	**      Fill in the board specific function handlers for the device.
	**      These are for any board specific functions required during
	**      register accesses or functions to interface to an external
	**      interrupt controller.  Refer to extio_1.h.
	** --------------------------------------------------------------------*/

	/* extern_fops_t from extio_1.h */
	{
		0,                      /* register write byte */
		0,                      /* register write word */
		0,                      /* register write long */
		0,                      /* register read  byte */
		0,                      /* register read  word */
		0,                      /* register read  long */
		0,                      /* int_strt function pointer */
		0,                      /* int_end function pointer */
		0,                      /* board specific int disable handling */
		0,                      /* board specific int enable handling */
		0
	},


	/* fill in the specific initialization */
	/* zi8530_specific_t (from zi8530.h) */
	{
        ZI8530_2_BASE_VECTOR,       // base vector for the zi8530 chip
        0,                          // shadow wr1, always init to zero
        BAUD_RATE_GENERATOR,        // DO NOT CHANGE
        SCC_CHANNEL_B
	}
};

serial_dev_desc_t	serial_4_dev_desc;

zi8530_config_t		zi8530_config_4_ram;

const logio_method_t	logio_serial_4_method = {

      &logio_serial_fops,
      (void *)&serial_4_dev_desc
};

const logio_device_id_t	logio_serial_4_id = &logio_serial_4_method;

//  <<<<<<<<<< END THIRD SERIAL PORT  >>>>>>>>>>

#endif  // SERIAL_DEVICE == 1



/******************************************************************************
****************************  ETHERNET  DEVICES   *****************************
******************************************************************************/

#if ETHER_DEVICE == 1
/* ----------------------------------------------------------------------------
**   INSTRUCTION:  Declare functions for getting the Ethernet address.
**   These are board specific.
**
**   INSTRUCTION:  Add any function prototypes for board interrupt
**   enable/disable handling for the ethernet device.  If there is
**   anything that needs to be done to service an ethernet interrupt
**   that is board specific there should be a function for that
**   and it should be prototyped here.
** --------------------------------------------------------------------------*/

void sigma_get_ethaddr();
vbsp_return_t sigma_in82596a_int_start();	/* int_strt function pointer */
vbsp_return_t sigma_in82596a_int_end();	/* int_end function pointer */
vbsp_return_t sigma_in82596a_int_dis();	/* board specific int disable handling */
vbsp_return_t sigma_in82596a_int_en();	/* board specific int enable handling */

/* ----------------------------------------------------------------------------
**   INSTRUCTION:  Declare/define the specific config for the ether device.
**   Refer to ether.h & the device specific in82596a.h.
**   Remember that in82596a_specific_t is defined in in82596a.h.
** --------------------------------------------------------------------------*/

#define ETHERNET_TX_VECTOR    29   /* tx   vector */
#define ETHERNET_RX_VECTOR    29   /* rx   vector */
#define ETHERNET_ERR_VECTOR   29   /* err  vector */
#define ETHERNET_INIT_VECTOR  0    /* init vector */

#define ETHERNET_NUM_RXBUF      64    /* number rx buffers */
#define ETHERNET_NUM_TXBUF       8    /* number tx buffers */
#define ETHERNET_NUM_FREETXBUF   4    /* number of free tx buffers */

#define ETHERNET_LAN_BASE           (0xFFBE8028)
#define ETHERNET_LAN_UCW            (ETHERNET_LAN_BASE)
#define ETHERNET_LAN_LCW            (ETHERNET_LAN_BASE+2)
#define ETHERNET_LAN_CA             (ETHERNET_LAN_BASE+0x10)

in82596a_specific_t RAM_in82596a_specific;

const in82596a_config_t in82596a_1_config_rom = {

	/* ROM CONFIGURATION VALUES (will be placed in RAM as DEFAULT) */
	{
		0,                         /* interface revision filled in by driver */

		/* MEMORY CONFIGURATION */
		1,                         /* use system buffers */
		0,                         /* base memory */
		0,                         /* memory size */
		0,                         /* do not get all packets on receive */

		/* VECTOR CONFIGURATION */
		ETHERNET_TX_VECTOR,        /* tx   vector */
		ETHERNET_RX_VECTOR,        /* rx   vector */
		ETHERNET_ERR_VECTOR,       /* err  vector */
		ETHERNET_INIT_VECTOR,      /* init vector */

		/* BUFFER CONFIGURATION */
		ETHERNET_NUM_RXBUF,           /* number rx buffers */
		ETHERNET_NUM_TXBUF,           /* number tx buffers */
		ETHERNET_NUM_FREETXBUF,       /* number of free tx buffers */

		/* ETHERNET CONFIGURATION */
		sigma_get_ethaddr,
		0,                         /* sigma_set_ethaddr, */
		0,0,0,0,0,0,               /* ethernet address */
		&RAM_in82596a_specific     /* attach specific in82596a RAM structure here */
	},

	/* ----------------------------------------------------------------------
	**   INSTRUCTION:
	**      Fill in the board specific function handlers for the device.
	**      These are for any board specific functions required during
	**      register accesses or functions to interface to an external
	**      interrupt controller.  Refer to extio_1.h.
	** --------------------------------------------------------------------*/

	/* extern_fops_t from extio_1.h */
	{
		0,                      /* register write byte */
		0,                      /* register write word */
		0,                      /* register write long */
		0,                      /* register read  byte */
		0,                      /* register read  word */
		0,                      /* register read  long */

#if SERIAL_INTERRUPT == 1
		(vbsp_return_t (*)(void *,unsigned char))sigma_in82596a_int_start,  /* int_strt function pointer */
		(vbsp_return_t (*)(void *,unsigned char))sigma_in82596a_int_end,    /* int_end function pointer */
		sigma_in82596a_int_dis,    /* board specific int disable handling */
		sigma_in82596a_int_en,     /* board specific int enable handling */
#else
		0,                      /* int_strt function pointer */
		0,                      /* int_end function pointer */
		0,                      /* board specific int disable handling */
		0,                      /* board specific int enable handling */
#endif
		0
	},

	/* REGISTER MAP */
	{
        (unsigned short *) ETHERNET_LAN_UCW,
        (unsigned short *) ETHERNET_LAN_LCW,
        (unsigned long *)  ETHERNET_LAN_CA
	}
};

ether_1_dev_desc_t	ether_1_dev_desc;	/* ID for an ethernet controller */

in82596a_config_t		ether_1_config_ram;

const logio_method_t	logio_ether_1_method = {

	&logio_ether_1_fops,
	&ether_1_dev_desc
};

const logio_device_id_t	logio_ether_1_id = &logio_ether_1_method;
#endif



/******************************************************************************
****************************  TIMER     DEVICES   *****************************
******************************************************************************/
#if SOFTWARE_TIMER_DEVICE == 1

/* ----------------------------------------------------------------------------
**   INSTRUCTION:  A software timer should not need any modifications.
**   The software timer is provided for ease of initial development.
**   It is not required to provide a timer driver to support the
**   initial timeout/retry requirements of Xtdaemon.  The timeout
**   periods will be longer than they need to be when you first
**   implement a Spectra bridge connection.  But, not to worry
**   the real timer can be added later.  This was done to simplify
**   the hard requirements for getting started.  The software
**   timer should be removed eventually as an O/S will need more
**   accurate timing mechanisms.
** --------------------------------------------------------------------------*/

#define TIMER_SW_VECTOR   0x70
#define TIMER1_PRIORITY   SW_TIMER_IRQ_3

unsigned short rega, regb;

const sw_timer_config_t sw_timer_config_rom = {

	{&rega ,&regb}, /* reg map */
	{TIMER_SW_VECTOR, 122000, 0, 0x52, 0},       /* timer config */
	{TIMER1_PRIORITY, 0}
};

sw_timer_config_t	sw_timer_config_ram;
#endif

/******************************************************************************
****************************  TIMER     DEVICES   *****************************
******************************************************************************/

#if TIMER_DEVICE == 1

// NMI source register contains revision information that drives which
// timer is instantiated and initialized.
#define P_NMI_SOURCE_REGISTER  ((unsigned char*)(0xffbe8008))
#define NMI_REV_MASK           (0xc0)
#define NMI_REV_COST_REDUCTION (0x00)

vbsp_return_t sigma_timer_int_end(void *devid, unsigned char intrpt);  /* int_end function pointer */

#define TIMER_1_CLK             (4000000)  /* 4Mhz */
#define TIMER_1_CNTR_0_VEC      (27)       /* AUTOVECTOR 3 */
#define TIMER_1_CNTR_1_VEC      (28)       /* AUTOVECTOR 4 */
#define TIMER_1_CNTR_2_VEC      (30)       /* AUTOVECTOR 6 */
#define TIMER_1_BASE            (0xFFBE8011)
#define TIMER_1_CNT             (20000)
#define TIMER_1_CNTR_0          ((unsigned char *) (TIMER_1_BASE+0))
#define TIMER_1_CNTR_1          ((unsigned char *) (TIMER_1_BASE+2))
#define TIMER_1_CNTR_2          ((unsigned char *) (TIMER_1_BASE+4))
#define TIMER_1_CNTRL           ((unsigned char *) (TIMER_1_BASE+6))
#define TIMER_1_CNTR_0_EOI      ((unsigned char *) (0xffbe8030))
#define TIMER_1_CNTR_1_EOI      ((unsigned char *) (0xffbe8031))
#define TIMER_1_CNTR_2_EOI      ((unsigned char *) (0xffbe8032))

typedef struct
{
   unsigned char * clearInterruptReg;
} SigmaTimerSpecific;

SigmaTimerSpecific  sigma_timer_1_specific =
{
    TIMER_1_CNTR_2_EOI
};

in8254_specific_t	in8254_specific_RAM1;

in8254_config_t		in8254_config_1_rom = {

	/* timer_config_t */
	{
		TIMER_1_CNTR_2_VEC,              /* VECTOR */
		(1e9)/TIMER_1_CLK,               /* FIXED:    clock period in nanoseconds     */
		0,                               /* NOT USED: initial counter value           */
		TIMER_1_CNT,                     /* initial preload value                     */
		(void *)&in8254_specific_RAM1    /* additional configuration                  */
	},

	/* extern_fops_t glue logic */
	{
		0,0,0,0,0,0,                     /* register read/write byte/word long  */
		0,                               /* int_strt function pointer           */
		sigma_timer_int_end,             /* int_end function pointer            */
		0,                               /* board specific int disable handling */
		0,                               /* board specific int enable handling  */
		&sigma_timer_1_specific          /* specific information for this timer */
	},

	/* in8254_reg_map_t  */
	{
		(BYTE *) TIMER_1_CNTR_2,         /* in8254_cnt */
		(BYTE *) TIMER_1_CNTRL           /* in8254_cnt */
	},

	 /* in8254_specific_t */
	{
        PIT_COUNTER_2,                   /* channel           */
        PIT_MODE_2,                      /* mode              */
        0                                /* flag_int_enabled  */
	}
};

/* TIMER FOR COST REDUCTION BOARD */

#define PBTIMER_3_CNT		(5000000/PBTIMER_NS_PER_CLK)   /* 5ms */

#define P_PBTIMER_1_PRELOAD	((unsigned long *) (0xffbe8100))
#define P_PBTIMER_1_COUNTER	((unsigned long *) (0xffbe8104))
#define P_PBTIMER_1_CONTROL	((unsigned char *) (0xffbe8108))
#define P_PBTIMER_2_PRELOAD	((unsigned long *) (0xffbe8110))
#define P_PBTIMER_2_COUNTER	((unsigned long *) (0xffbe8114))
#define P_PBTIMER_2_CONTROL	((unsigned char *) (0xffbe8118))
#define P_PBTIMER_3_PRELOAD	((unsigned long *) (0xffbe8120))
#define P_PBTIMER_3_COUNTER	((unsigned long *) (0xffbe8124))
#define P_PBTIMER_3_CONTROL	((unsigned char *) (0xffbe8128))
#define PBTIMER_1_VEC       (27)       /* AUTOVECTOR 3 */
#define PBTIMER_2_VEC       (28)       /* AUTOVECTOR 4 */
#define PBTIMER_3_VEC       (30)       /* AUTOVECTOR 6 */

PbTimer_specific_t	PbTimer_specific_RAM1;

PbTimer_config_t	PbTimer_config_1_rom =
{

	// timer_config_t
	{
		PBTIMER_3_VEC,               	 // VECTOR
		PBTIMER_NS_PER_CLK,           	 // FIXED:  clock period in nanoseconds
		0,                               // NOT USED: initial counter value
		PBTIMER_3_CNT,                	 // initial preload value (5ms timer)
		(void *)&PbTimer_specific_RAM1   // additional configuration
	},

	// extern_fops_t glue logic
	{
		0,0,0,0,0,0,                     // register read/write byte/word long
		0,                               // int_strt function pointer
		sigma_timer_int_end,             // int_end function pointer
		0,                               // board specific int disable handling
		0,                               // board specific int enable handling
		&sigma_timer_1_specific          // specific information for this timer
	},

	// PbTimer_reg_map_t
	{
		P_PBTIMER_3_PRELOAD,  			 // pTimerPreloadReg
		P_PBTIMER_3_COUNTER,  			 // pTimerCountReg
		P_PBTIMER_3_CONTROL			 	 // pTimerControlReg
	},

	// PbTimer_specific_t
	{
        0                                // shadowIntReg
	}
};

#endif


#if (TIMER_DEVICE == 1) || (SOFTWARE_TIMER_DEVICE == 1)
timer_dev_desc_t	timer_1_dev_desc;

const logio_method_t	logio_timer_1_method = {

        &logio_timer_fops,
        (void *)&timer_1_dev_desc
};

const logio_device_id_t	logio_timer_1_id = &logio_timer_1_method;

in8254_config_t  in8254_config_1_ram;

PbTimer_config_t  PbTimer_config_1_ram;

#endif

/******************************************************************************
**************************  MEMORY CONSOLE DEVICE   ***************************
******************************************************************************/

#if MEM_CONSOLE == 1

memcons_buf_t  mc1_bufs;

/*
 * INSTRUCTION:
 * When bringing up the bridge, you can set mc1_inbuf_data to be empty
 * string or set it to be a set of boot shell command.  If it is set to
 * a set of boot shell command then make sure 'shell' method is in
 * boot.env.boot_order, eg. 'boot.env.boot_order: shell:xtrace'.
 * Each command in the string needs to be followed by '\n'
 * and the last command in the string should be the 'exit' command.
 */

const char	mc1_inbuf_data[] = "\nprintenv\nexit\n";	/* ROM */

char		mc1_outbuf_data[ MEMCONS_OUTBUF_SIZE ];		/* RAM */


const memcons_config_t	mc1_config_rom = {

	/* serial config - of 'serial_config_t' type */
	{
		serial_tty_device,   /* serial_flavor_t, 'tty' or 'packet' */
		0,                   /* clock speed in hz */
		0,                   /* baud rate cnt */
		0,                   /* PARITY_NONE, _EVEN, _ODD */
		0,                   /* BITS_8, BITS_7 */
		0,                   /* STOP_BITS_1 */
		0,                   /* Channel Rx Vector number 0 - 255  */
		0,                   /* Channel Tx Vector number 0 - 255  */
		0,                   /* Channel RxError Vector # 0 - 255  */
		0
	},


	/* Memory Console buffers - of 'memcons_bufs_t' type */
	{
		mc1_inbuf_data,     /* Ptr to Console Input buffer (ROM) */
		mc1_outbuf_data,    /* Ptr to Console Output buffer (RAM) */
		&mc1_bufs           /* Ptr to Memory Console buffer struct */
	}
};

serial_dev_desc_t	desc_memcons_1;

memcons_config_t	mc1_config;

const logio_method_t	logio_memcons_1_method ={

	&logio_serial_fops,
	(void *)&desc_memcons_1
};

const logio_device_id_t	logio_memcons_1_id = &logio_memcons_1_method;

#endif



/******************************************************************************
****************************  OTHER     DEVICES   *****************************
******************************************************************************/

/* place other devices here */


/******************************************************************************
****************************  logio device installation   *********************
******************************************************************************/

/*
 ** logio_device_install(void)
 **
 ** Description:
 **
 **    This function will call the install functions for each device.
 **    Any ROM to RAM copies of configuration structures are performed
 **    here.
 *
 *     For example, there are two serial ports on each Z8530. A
 *     call to logio_serial_1_init will only initialize portA of
 *     the Z8530. The Z8530, however, has "chip common" initializa-
 *     tion that must be performed before attempting to initialize
 *     its ports. This type of initialization that does not fall
 *     under specific logio_device_init initializations belongs in
 *     logio_device_install().
 *
 *     In addition, logio_device_install() is expected to disable the
 *     board's devices so they will not generate any interrupts.
 *
 */

logio_status_t logio_device_install(void)
{


/* ----------------------------------------------------------------------------
**   INSTRUCTION:  At this point it becomes more difficult to tell
**   you what specifically to modify as each board is different.
**   There are some general rules to follow, however.  Each device
**   will need to be installed.  This requires, not suprisingly, that
**   the install function be called for every device.  It will
**   take as input the device descriptor and the specific device
**   configuration structure pointer.  Rather than sanitizing this
**   too much the mvme162 board init is left here for your reference.
**   Maybe the example will help.  Remember that it's just that,
**   an example.  The code will need to be modified for your board
**   specifically.  You'll notice that the vmechip & pcc are
**   initialized directly.  They don't conform to the prescribed
**   device driver interface as they are not logio drivers.  They
**   are examples of board specific devices that take part in the
**   traditional devices, but are not directly supported through
**   logio device interfaces.  The examples you will be interested
**   in are the serial, ethernet & timer device installation.
**   Notice that the function sigma_get_ethaddr() will need to
**   be implemented for ethernet devices.  The instructions show
**   where the function is called from the logio_board_init()
**   routine.  This is different from the mvme162 functions just
**   to highlight the fact that it's required and where.
**
**   Creating a RAM versions of config structures is NOT required,
**   however compiler will emit warning message, when address of a const
**   stucture is used. If RAM space is at premium, donot use RAM versions
**   of the structures

**   VME_SM is not supported in this release of the bspbuilder.
** --------------------------------------------------------------------------*/

#ifdef VME_SM
int VMin82596a_install();
extern shmem_1_dev_desc_t desc_shmem_1;
extern vme_sm_config_t vme_cfg_rom;
#endif


#if MEM_CONSOLE == 1
	extern logio_device_id_t	mem_console_dev;
	extern int			stat;

	mc1_config = mc1_config_rom;

	if( memcons_install(&mc1_config, &desc_memcons_1) )
		return( LOGIO_STATUS_NOTINITIALIZED );

#if BRIDGE == 0

	/*
	 * In the INITIAL stages of developing a serial/ethernet driver the memory console
	 * device must be created and initialized before being used by the serial/ethernet
	 * driver.
	 */

	/* create the memory console device */

	if((stat = logio_device_create("MEM_CONSOLE_TEST", logio_memcons_1_id)) !=
	    LOGIO_STATUS_OK)
		return(LOGIO_STATUS_NOTINITIALIZED);

	/* Initializing the device will make it available to use */
	/* initializing memory console device always returns LOGIO_STATUS_OK */

	(void)logio_device_init(mem_console_dev);

#endif	/* BRIDGE == 0 */

#endif	/* MEM_CONSOLE == 1 */


#if SOFTWARE_TIMER_DEVICE == 1
	sw_timer_config_ram = sw_timer_config_rom;

	if(sw_timer_install(&sw_timer_config_ram, &timer_1_dev_desc))
		return(LOGIO_STATUS_NOTINITIALIZED);
#endif SOFTWARE_TIMER_DEVICE == 1


#if ETHER_DEVICE == 1

	/* place the install function for the ethernet device here */

	/* WARNING WARNING EARNING!!!!!!!!!!
	 *
	 * Previous versions of BSPBuilder used had the arguments
	 * in the reverse order.
	 *
	 * This has been changed to have uniform interface;
	 * However if ethernet driver was built using a
	 * previous version of the BSPBuilder, then you need to
	 * reverse the arguments passed to install.
	 */

	ether_1_config_ram = in82596a_1_config_rom;

	// if (in82596a_install(&ether_1_config_ram, &ether_1_dev_desc))
	if (in82596a_install(&ether_1_dev_desc, &ether_1_config_ram))
		return (LOGIO_STATUS_NOTINITIALIZED);

#endif	/* ETHER_DEVICE == 1 */


#if SERIAL_DEVICE == 1

	zi8530_config_1_ram = zi8530_config_1_rom;
	zi8530_config_2_ram = zi8530_config_2_rom;

	if (zi8530_install(&zi8530_config_1_ram, &serial_1_dev_desc))
		return (LOGIO_STATUS_NOTINITIALIZED);
	if (zi8530_install(&zi8530_config_2_ram, &serial_2_dev_desc))
		return (LOGIO_STATUS_NOTINITIALIZED);

	zi8530_com_init(&serial_1_dev_desc);

	// install four serial ports on GUI Cost Reduction board
	if (   (*P_IO_REGISTER_1 & BD_CPU_BIT) != BD_CPU_BIT
		&& (*P_NMI_SOURCE_REGISTER & NMI_REV_MASK) == NMI_REV_COST_REDUCTION)
	{
	  zi8530_config_3_ram = zi8530_config_3_rom;
	  zi8530_config_4_ram = zi8530_config_4_rom;

	  if (zi8530_install(&zi8530_config_3_ram, &serial_3_dev_desc))
		  return (LOGIO_STATUS_NOTINITIALIZED);
	  if (zi8530_install(&zi8530_config_4_ram, &serial_4_dev_desc))
		  return (LOGIO_STATUS_NOTINITIALIZED);

	  zi8530_com_init(&serial_3_dev_desc);
	}


#endif	/* SERIAL_DEVICE == 1 */

#if TIMER_DEVICE == 1
	// rev bits indicate which timer to instantiate
	if ( (*P_NMI_SOURCE_REGISTER & NMI_REV_MASK) != NMI_REV_COST_REDUCTION)
	{
		// this is a pre cost reduction board
    	in8254_config_1_ram = in8254_config_1_rom;

    	if (in8254_install(&in8254_config_1_ram, &timer_1_dev_desc))
		{
			return (LOGIO_STATUS_NOTINITIALIZED);
		}
	}
	else
	{
		// this is the cost reduction board
    	PbTimer_config_1_ram = PbTimer_config_1_rom;

    	if (PbTimer_install(&PbTimer_config_1_ram, &timer_1_dev_desc))
		{
			return (LOGIO_STATUS_NOTINITIALIZED);
		}
	}
#endif	/* TIMER_DEVICE == 1 */

#if SERIAL_POLL_TEST == 1
#endif	/* SERIAL_POLL_TEST == 1 */


#if SERIAL_INTERRUPT_TEST == 1
#endif	/* SERIAL_INTERRUPT_TEST == 1 */


#if SERIAL_BRIDGE == 1
#endif	/* SERIAL_BRIDGE == 1 */


#if ETHER_POLL_TEST == 1
#endif	/* ETHER_POLL_TEST == 1 */


#if ETHER_BRIDGE == 1
#endif	/* ETHER_BRIDGE == 1 */


#if SERIAL_CONSOLE_TEST == 1
#endif	/* SERIAL_CONSOLE_TEST == 1 */


#if TIMER_TEST == 1
#endif	/* TIMER_TEST == 1 */


#ifdef VME_SM
	VMin82596a_install(&vme_cfg_rom, &desc_shmem_1);
#endif	/* VME_SM */

	return(LOGIO_STATUS_OK);
}


#if ETHER_DEVICE == 1

#define SERIAL_NO_SIZE          (10)

//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function:  GetEthernetAddress_
//
//@ Interface-Description
//  This function configures the board device during the board
//  initialization.  It computes a unique Ethernet address of the
//  current board and returns it to the caller.  An unique Ethernet
//  address is computed based on a board's serial number programmed
//  into FLASH memory.
//
//  A legitimate Sigma serial number consists of 10 digit numbers.
//  If a serial number in the flash memory is not a legitimate serial
//  number, then use a default BD Ethernet Address or a default GUI
//  Ethernet address. If there is a legitimate Sigma serial number
//  in the Flash memory, first two digits of a serial number is
//  converted to an 8 bit unsigned integer number. The last 5
//  digits are converted to a 16 bit unsigned integer number. These
//  two numbers are concated to form a 18 bit unsigned integer number.
//  The 18 bit number is appended to the "0x00a061" to form a 6byte
//  long Ethernet address.  "0x00a061" is the Sigma Ethernet address
//  group.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Free-Function
//=====================================================================

unsigned char *
GetEthernetAddress_()
{
    static unsigned char etherAddress[6];

#if defined(SIGMA_PRODUCTION)
    int               i;
    char              c;
    int               isAllDigit = 1;
    unsigned long     uniqueNum = 0;
    char *            pSerial = (char *) FLASH_SERIAL_NO_ADDR;
    unsigned char     manufactSiteId = 0;

    for (i = 0; i < SERIAL_NO_SIZE; i++)
    {
        c = *pSerial++;
        if ( ( c  >= '0') && ( c <= '9' ))
        {
            if ( i < 2 )
            {
                manufactSiteId *= 10;
                manufactSiteId += c & 0xf;
            }
            // Parse the last 5 digits as a unique id.
            if ( i >= 5 )
            {
                uniqueNum *= 10;
                uniqueNum += c & 0xf;
            }
        }
        else
        {
            isAllDigit = 0;
            break;
        }
    }
    uniqueNum  &= 0xffff;

    if ( !isAllDigit || ( uniqueNum == 0xffff ) )
    {
        if ( (*P_IO_REGISTER_1 & BD_CPU_BIT) == BD_CPU_BIT )
        {
            uniqueNum = 0x91c7;  /* 145.199  */
        }
        else
        {
            uniqueNum = 0x91c8;  /* 145.200 */
        }
        manufactSiteId = 0xff;
    }
    etherAddress[0] = 0x00;
    etherAddress[1] = 0xa0;
    etherAddress[2] = 0x61;
    etherAddress[3] = manufactSiteId;
    pSerial = (char *) &uniqueNum;
    etherAddress[4] = pSerial[2];
    etherAddress[5] = pSerial[3];

#elif   defined(SIGMA_DEVELOPMENT)
    extern  char  crt0_enet_addr[6];

    etherAddress[0] = crt0_enet_addr[0];
    etherAddress[1] = crt0_enet_addr[1];
    etherAddress[2] = crt0_enet_addr[2];
    etherAddress[3] = crt0_enet_addr[3];
    etherAddress[4] = crt0_enet_addr[4];
    etherAddress[5] = crt0_enet_addr[5];
#endif
    return etherAddress;
}

/* ----------------------------------------------------------------------------
**   INSTRUCTION:  The get ethernet address function was put here
**   FOR REFERENCE ONLY.  Obviuosly, you'll need to modify this to
**   return the board specific address.
** --------------------------------------------------------------------------*/

/*
**
** Function name:
**
**    sigma_get_ethaddr
**
** Description:
**
**      This function returns the Ethernet address of the board.
**
** Inputs:
**
** Outputs:
**
**      data: the Ethernet address.
**
** Return codes:
**
** Side-effects:
** Global variables:
**
*/

void
sigma_get_ethaddr(unsigned char * data)
{
    static unsigned char * pAddr;

    if ( !pAddr )
    {
        pAddr = GetEthernetAddress_();
    }

	*data++ = pAddr[0];
	*data++ = pAddr[1];
	*data++ = pAddr[2];
	*data++ = pAddr[3];
	*data++ = pAddr[4];
	*data++ = pAddr[5];
}

/*
**  sigma_in82596a_int_en
**
**  This routine does the board specific interrupt enable of the
**  in82596a chip.  This function may be required if the ethernet chip is connected
**  to the CPU through an external interrupt controller.
**
**  This routine is not mandatory.  In some cases no special logic is necessary
**  to interface the ethernet chip to the CPU, and, therefore, no function is
**  necessary to drive the logic.
**
** Inputs:
**     devid          id of the device
**     intrpt         interrupt structure
** Outputs:
**     none
** Return Codes:
**     VBSP_SUCCESS
*/

vbsp_return_t
sigma_in82596a_int_en(ether_1_dev_desc_t *devid, logio_int_entry_t *intrpt)
{

	return (VBSP_SUCCESS);
}

/*
**  sigma_in82596a_int_dis
**
**  This routine does the board specific interrupt disable of the
**  in82596a chip.  This function may be required if the ethernet chip is connected
**  to the CPU through an external interrupt controller.
**
**  This routine is not mandatory.  In some cases no special logic is necessary
**  to interface the ethernet chip to the CPU, and, therefore, no function is
**  necessary to drive the logic.
**
** Inputs:
**     devid          id of the device
**     intrpt         interrupt structure
** Outputs:
**     none
** Return Codes:
**     VBSP_SUCCESS
*/

vbsp_return_t
sigma_in82596a_int_dis(ether_1_dev_desc_t *devid, logio_int_entry_t *intrpt)
{
	return (VBSP_SUCCESS);
}

/*
**  sigma_in82596a_int_start
**
**  This routine executes code required at the start of the ethernet chip ISRs.
**  This function may be required if the ethernet chip is connected to the CPU
**  through an external interrupt controller.
**
**  This routine is not mandatory.  In some cases no special logic is necessary
**  to interface the ethernet chip to the CPU, and, therefore, no function is
**  necessary to drive the logic.
**
**
** Inputs:
**     devid          id of the device
**     intrpt         interrupt structure
** Outputs:
**     none
** Return Codes:
**     VBSP_SUCCESS
*/

vbsp_return_t
sigma_in82596a_int_start(ether_1_dev_desc_t *devid, logio_int_entry_t *intrpt)
{
	return (VBSP_SUCCESS);
}

/*
**  sigma_in82596a_int_end
**
**  This routine executes code required at the end of the ethernet chip ISRs.
**  This function may be required if the ethernet chip is connected to the CPU
**  through an external interrupt controller.
**
**  This routine is not mandatory.  In some cases no special logic is necessary
**  to interface the ethernet chip to the CPU, and, therefore, no function is
**  necessary to drive the logic.
**
**
** Inputs:
**     devid          id of the device
**     intrpt         interrupt structure
** Outputs:
**     none
** Return Codes:
**     VBSP_SUCCESS
*/

#define RESET_LAN_AUTOVECTOR_5_REG  ((unsigned char *)(0xFFBE8033))

vbsp_return_t
sigma_in82596a_int_end(ether_1_dev_desc_t *devid, logio_int_entry_t *intrpt)
{
    *RESET_LAN_AUTOVECTOR_5_REG = 0;
	return (VBSP_SUCCESS);
}

#endif	/* ETHER_DEVICE == 1 */


/******************************************************************************
****************** TIMER CHIP GLUE LOGIC FUNCTIONS ****************************
******************************************************************************/

#if TIMER_DEVICE == 1

/*
**  sigma_timer_int_end
**
**  This routine executes code required at the end of the timer chip ISRs.
**  This function may be required if the timer chip is connected to the CPU
**  through an external interrupt controller.
**
**  This routine is not mandatory.  In some cases no special logic is necessary
**  to interface the ethernet chip to the CPU, and, therefore, no function is
**  necessary to drive the logic.
**
**
** Inputs:
**     devid          id of the device
**     intrpt         interrupt structure
** Outputs:
**     none
** Return Codes:
**     VBSP_SUCCESS
*/
vbsp_return_t
sigma_timer_int_end(void *devid, unsigned char intrpt)
{
  SigmaTimerSpecific * specific = ((timer_dev_desc_t*)devid)->externio.specific;
  *(specific->clearInterruptReg) = 0;

  return (VBSP_SUCCESS);
}
#endif	/* TIMER_DEVICE == 1 */


/* End of file:	devcnfg.c */
