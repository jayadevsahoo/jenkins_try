#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2001, Puritan-Bennett Corporation
//=====================================================================  

// =========================== M O D U L E   D E S C R I P T I O N ====
//@ Class: VgaDisplayContext -- Display Context for VGA Library Functions
//---------------------------------------------------------------------
//@ Interface-Description
//  This class provides an abstract interface for the application to 
//  control the VGA display. It is intended to allow device independent
//  drawing methods. Using the VgaDisplayContext interface class, the 
//  application should be able to draw to any type of display or memory
//  device using the same interface. In its current form, VgaDisplayContext
//  only interfaces to the VGA device, but it maintains a generic
//  interface that can be used for any device.
//---------------------------------------------------------------------
//@ Rationale
//  Contains a context information and drawing interface for the VGA
//  display device.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The VgaDisplayContext uses the Vga class to implement most of the
//  hardware specific control of the VGA device.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ $Header:   /840/Baseline/VGA-Graphics-Driver/vcssrc/VgaDisplayContext.ccv   25.0.4.0   19 Nov 2013 14:37:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 009   By: rpr     Date: 11-Mar-2011     SCR Number: 6752
//  Project: XENA2
//      ExpandFont and ExpandIcon passes the original witdth of the font/bitmap
//		so clipping can be accounted for properly.
// 
//  Revision: 008   By: mnr    Date: 24-Sep-2010    SCR Number: 6672
//  Project: XENA2
//  Description:
//      New method expandIcon() added.
//
//  Revision: 007   By: gdc    Date:  29-Jan-2007    SCR Number: 6237
//  Project: Trend
//  Description:
//  Added off-screen image storage/recovery for cursor movement in 
//  emulated overlay plane used to implement the trend cursor line.
//
//  Revision: 006		By:erm	Date: 15-Oct-2001	DCS Number 5959
//  Project:GuiComms
//  Description:
//  Code CleanUp
//
//  Revision: 005   By: gdc    Date:  24-Sep-2001    DCS Number: 5493
//  Project: GuiComms
//  Description:
//  Initial Version.
//	Moved VGA specific functionality from DisplayContext to this class.
//
//  Revision: 004   By: gdc    Date:  20-Sep-2000    DCS Number 5770
//  Project: GuiComms
//  Description:
//  Copy additional longword in expandFont to account for non-longword
//  alignment of bitmaps. Limit font dimension instead of asseting in
//  expandFont.
//
//  Revision: 003  By gdc      Date 28-Aug-2000       DCS Number 5493
//  Project: GuiComms
//  Description:
//  Implemented single screen compatibility.
//
//  Revision: 002   By hct     Date 28-Mar-2000       DCS Number 5493
//  Project: GuiComms
//  Description:
//  Add GuiComms functionality.
//
//  Revision: 001   By: gdc		Date: 27-Jan-2000		DCS Number 5337
//  Project: 840 Neonatal
//  Optimized VGA library for Neonatal project to use BitBlt and Windows
//  Bitmap structures.
//=====================================================================  
#include "VgaDisplayContext.hh"
#include "GraphicsContext.hh"
#include "VgaDib.hh"
#include "VgaFontData.hh"
#include "VgaPoint.hh"
#include "VgaLine.hh"
#include "VgaRectangle.hh"
#include "VgaTriangle.hh"
#include <string.h>

//@ Usage-Classes
//@ End-Usage

// Microsoft Windows defined raster ops
static const Uint32 ROP_SOURCE_COPY  = (0xCC);
//static const Uint32 ROP_PATTERN_COPY = (0xF0);
//static const Uint32 ROP_SOURCE_PAINT = (0xEE);

// Control Register DR04 (BitBlt) bit definitions
static const Uint32 INC_Y            = (0x100);
static const Uint32 INC_X            = (0x200);
static const Uint32 MONO_DATA        = (0x800);
static const Uint32 BG_TRANSPARENT   = (0x2000);
static const Uint32 FG_REG_SOURCE    = (0x400);

//=====================================================================  
//
//  Public Methods...
//
//=====================================================================  

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: VgaDisplayContext [constructor]
//
//@ Interface-Description
//  Contructor for the VgaDisplayContext class. 
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
VgaDisplayContext::VgaDisplayContext( VgaDisplayId displayId, Int frame )
  : DisplayContext( 640, 480, 8 ), isOffScreenImageAllocated_(FALSE)
{
	pOffScreenMemory_ = new OffScreenMemory();
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~VgaDisplayContext [destructor]
//
//@ Interface-Description
//  Destructor for the VgaDisplayContext class.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
VgaDisplayContext::~VgaDisplayContext()
{
	if(pOffScreenMemory_ != NULL) 
	{
	   delete pOffScreenMemory_;
	   pOffScreenMemory_ = NULL;
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: accessDisplay
//
//@ Interface-Description
//  Assures the display and display memory is ready to be accessed.
//  For the VGA controller, this assures that the BitBlt engine is
//  idle.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
void
VgaDisplayContext::accessDisplay() const
{

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: lock
//
//@ Interface-Description
//  Locks the display for accessing display memory or display registers.
//  The client enters its critical section.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
void
VgaDisplayContext::lock(void)
{
  // $[TI1]
	//TODO BVP - E600 Need to make the Context thread-safe
	//  rVga_.lock();
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: unlock
//
//@ Interface-Description
//  Unlocks the display for after accessing display memory or display 
//  registers. The client leaves its critical section.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
void
VgaDisplayContext::unlock()
{
	// $[TI1]
	//TODO BVP - E600 Need to make the Context thread-safe
	//  rVga_.unlock();
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: moveBlock
//
//@ Interface-Description
//  Moves a rectangular block from one display memory location to 
//  another using the BitBlt engine of the VGA controller.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
void 
VgaDisplayContext::moveBlock(Int xSrc, Int ySrc, Int xDest, Int yDest, 
						  Int xWidth, Int yHeight)
{
	//TODO: BVP - E600 This needs testing. The legacy code used VGA hardware
	//and specific control registers to copy from one area to another. 
	//The following code has been verified to work on a stand-alone program,
	//using the same CDC* for source and destination. The MSDN documentation
	//doesnt say anything about (or against) having both source and destination
	//being the same. Once the screens are up, we have to test this again. It 
	//looks like the API comes into play for scroll. It may also be worth
	//exploring the the CDC::ScrollDC() API.
	pCDC_->BitBlt(xDest, yDest, xWidth, yHeight, pCDC_, xSrc, ySrc, SRCCOPY);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: allocateImage
//
//@ Interface-Description
// Allocates the off-screen image memory of the size specified.
// Returns NULL if the request cannot be satisfied.
//---------------------------------------------------------------------
//@ Implementation-Description
// In the current impelementation, there is only one off-screen memory
// image available to the application so it just returns a pointer to 
// the beginning of this area.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
void * 
VgaDisplayContext::allocateImage(Uint32 size)
{
	void * pImage = NULL;

	if (!isOffScreenImageAllocated_ && size <= sizeof(pOffScreenMemory_->offScreenImage))
	{                          
		pImage = pOffScreenMemory_->offScreenImage;
		isOffScreenImageAllocated_ = TRUE;
	}

	return pImage;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: freeImage
//
//@ Interface-Description
// Frees the image in off-screen memory specified by the memory pointer
// parameter.
//---------------------------------------------------------------------
//@ Implementation-Description
// There is a single block of off-screen memory shared by all clients.
// If the address passed to this method is the address of the off-screen
// memory block then reset isOffScreenImageAllocated_. If the address
// is invalid then assert.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
void
VgaDisplayContext::freeImage(void * pImage)
{

	AUX_CLASS_ASSERTION(pImage == pOffScreenMemory_->offScreenImage, Uint32(pImage));
	CLASS_ASSERTION(isOffScreenImageAllocated_); 

	isOffScreenImageAllocated_ = FALSE;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: storeImage
//
//@ Interface-Description
//  Copies from on-screen memory to an allocated off-screen memory
//  block. Client specifies the block's x and y origin, width and 
//  height, and destination memory location returned by allocateImage.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses the BitBLT engine to move the data inside VGA memory and 
//  efficiently compress the data into the offscreen area.
//---------------------------------------------------------------------
//@ PreCondition
// (xSrc >= 0 && xSrc + xWidth <= width_, xSrc)
// && (ySrc >= 0 && ySrc + yHeight <= height_, ySrc)
// && (xWidth * yHeight <= sizeof(pOffScreenMemory_->offScreenImage), xWidth * yHeight)
// && (isInDisplayMemory_(pDest), Uint32(pDest))
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
void 
VgaDisplayContext::storeImage(void * pDest, Int xSrc, Int ySrc, Int xWidth, Int yHeight)
{
	//TODO: BVP - E600 This needs to be implemented eventually
	//This API is invoked from Drawable while doing overlays. The API
	//signature suggests that it is a generic mechanism to save a
	//screen area to any input buffer. In practice, the input buffer 
	//is allocated (there is just one buffer - preallocated, and assigned
	//to one caller at a time) in this class itself, and passed back to this
	//API. We should probably maintain a memory-based CDC internally and
	//just alter teh API signature to not provide a buffer.

	//For now, do an assert.. this has be implemented soon enough (before
	//the waveforms can be displayed).
	CLASS_PRE_CONDITION(FALSE);

}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: recoverImage
//
//@ Interface-Description
//  Recreates the block stored in off-screen memory by storeImage back
//  onto the screen. Client specifies the block's x and y destination, 
//  width and height, and source off-screen memory location.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses the BitBLT engine to move the data inside VGA memory and 
//  expand the data to on-screen memory.
//---------------------------------------------------------------------
//@ PreCondition
// (xDest >= 0 && xDest + xWidth <= width_, xDest)
// && (yDest >= 0 && yDest + yHeight <= height_, yDest)
// && (xWidth * yHeight <= sizeof(pOffScreenMemory_->offScreenImage), xWidth * yHeight)
// && (isInDisplayMemory_(pSrc), Uint32(pSrc))
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
void 
VgaDisplayContext::recoverImage(Int xDest, Int yDest, const void * pSrc, 
								Int xWidth, Int yHeight)
{

	//TODO: BVP - E600
	//Read the note on storeImage() earlier. We need to revisit this
	//API and probably change the signature before implementing it.
	CLASS_PRE_CONDITION(FALSE);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: fillSolid
//
//@ Interface-Description
//  Performs a "solid fill" to the area specified using the BitBlt
//  engine of the VGA controller.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
void 
VgaDisplayContext::fillSolid(Int xDest, Int yDest, Int xWidth, Int yHeight, Int color)
{

  SAFE_AUX_CLASS_PRE_CONDITION(xDest >= 0, xDest);
  SAFE_AUX_CLASS_PRE_CONDITION(yDest >= 0, yDest);
  SAFE_AUX_CLASS_PRE_CONDITION(0 <= xWidth && xWidth <= width_, xWidth);
  SAFE_AUX_CLASS_PRE_CONDITION(0 <= yHeight && yHeight <= height_, yHeight);
  SAFE_AUX_CLASS_PRE_CONDITION( yDest * width_ + xDest + (xWidth * yHeight) 
	  <= width_ * height_, xWidth * yHeight);

  RgbQuad rgb = {0,0,0,0};
  getColor(color,rgb);
  COLORREF crWndCol = RGB(rgb.rgbRed,rgb.rgbGreen,rgb.rgbBlue);
  

  pCDC_->FillSolidRect(xDest,yDest,xWidth,yHeight,crWndCol);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: expandFont
//
//@ Interface-Description
//  Performs a font expansion to display memory from the font data
//  specified using the BitBlt engine of the VGA controller.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
void 
VgaDisplayContext::expandFont(Int xDest, Int yDest, Int xWidth, Int yHeight,
		                   const Byte * pFontData, Uint32 fontOffset,
						   Int color, Int orignalWidth)
{
	//TODO: BVP - E600
	//We are not using the fonts the way 840 used to. There is no need
	//to "expand" font to display memory in Windows. Just keeping this
	//around to see if are missing something. We should remove this once 
	//all the screens are ported over.
	CLASS_PRE_CONDITION(FALSE);

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: expandIcon
//
//@ Interface-Description
//  Performs a icon expansion to display memory from the bitmap data
//  specified using the BitBlt engine of the VGA controller.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
void 
VgaDisplayContext::expandIcon(Int xDest, Int yDest, Int xWidth, Int yHeight,
		                   const Byte * pFontData, Uint32 fontOffset,
						   Int color, Int orignalWidth)
{
	//TODO: BVP - E600
	//See the note on expandFont() above.
	CLASS_PRE_CONDITION(FALSE);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: allocateColor
//
//@ Interface-Description
//  Allocates a colormap entry for the specified RgbQuad.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses Vga::allocateColor() method to allocate the entry in the VGA
//  colormap.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
void
VgaDisplayContext::allocateColor(Byte & index, const RgbQuad & rgb)
{
  
    // $[TI1]
	//rVga_.allocateColor(index, rgb);
	//Create RGB for bitmap
  Uint i = 0; 
  for (i=0; i<countof(colormapCache_); i++)  
  {		// $[TI1.1]
    // VGA can only handle 6 bits in palette regs so only compare 6 bits
    if (   isSameVideo_( colormapCache_[i], rgb )
        && (colormapCache_[i].rgbReserved & (CM_READ_ONLY | CM_ALLOCATED) ) 
		    == (CM_READ_ONLY | CM_ALLOCATED) )
    {	// $[TI2.1]
      break;
    }	// $[TI2.2]
  }		// $[TI1.2]

  if ( i < countof(colormapCache_) )
  {		// $[TI3.1]
    // color already exists in map
    index = i;
  }
  else
  {		// $[TI3.2]
    // allocate a new color map entry
    for (i=0; i<countof(colormapCache_); i++)
    {
      if ( !colormapCache_[i].rgbReserved )
      {		// $[TI4.1]
        break;
      }		// $[TI4.2]
    }//End of for
   
    if ( i < countof(colormapCache_) )
    {	// $[TI5.1]
      storeColor( i, rgb );
	  index = i;
    }
    else
    {	// $[TI5.2]
	  // As a future capability, if there are no more allocable 
	  // colormap entries, this method could search and return 
	  // the "closest" color. For now, we just return color index
	  // zero
      index = 0;  // for no more entries - return index zero
    }
  }

	
}
 
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: storeColor
//
//@ Interface-Description
//  Sets the specified colormap entry with the colors specified. 
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses Vga::storeColor() method to store the entry in the VGA
//  colormap.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
void 
VgaDisplayContext::storeColor(Byte index, const RgbQuad & rgb, Boolean readOnly)
{
  // $[TI1]
  colormapCache_[index].rgbRed = rgb.rgbRed;
  colormapCache_[index].rgbGreen = rgb.rgbGreen;
  colormapCache_[index].rgbBlue = rgb.rgbBlue;
  colormapCache_[index].rgbReserved = CM_ALLOCATED;

  if ( readOnly )
  {		// $[TI1.1]
	colormapCache_[index].rgbReserved |= CM_READ_ONLY;
  }		// $[TI1.2]
 
    
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getColor
//
//@ Interface-Description
//  Gets the specified colormap entry for specified index and stores it
//  in the specified RgbQuad.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses Vga::getColor() method to get the RgbQuad entry from the VGA
//  colormap.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
void 
VgaDisplayContext::getColor(Byte index, RgbQuad & rgb)
{

	rgb.rgbRed = colormapCache_[index].rgbRed;
	rgb.rgbGreen = colormapCache_[index].rgbGreen;
	rgb.rgbBlue = colormapCache_[index].rgbBlue;
	rgb.rgbReserved = colormapCache_[index].rgbReserved;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: showFrame
//
//@ Interface-Description
//  Commands the VGA controller to display this VgaDisplayContext's
//  frame buffer on the panel.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls the Vga::SetDisplayStartAddress with the pointer to the
//  frame buffer.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
void
VgaDisplayContext::showFrame(void) 
{
  // $[TI1]
	
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: drawPoint
//
//@ Interface-Description
//  Renders a point on the VGA display at the specified coordinates.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses VgaPoint::DrawPoint() to draw the point on the VGA display.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
void  
VgaDisplayContext::drawPoint(
					GraphicsContext & rGc, 
					Int x, Int y)
{
  // $[TI1]
  VgaPoint::DrawPoint(*this, rGc, x, y);  
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: drawLine
//
//@ Interface-Description
//  Renders a line to the VGA display at the specified coordinates.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses VgaLine::DrawLine() to draw the line on the VGA display.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
void  
VgaDisplayContext::drawLine(
					GraphicsContext & rGc, 
					Int x0, Int y0, Int x1, Int y1)
{
  // $[TI1]
  VgaLine::DrawLine(*this, rGc, x0, y0, x1, y1);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: fillRectangle
//
//@ Interface-Description
//  Renders a filled rectangle to the VGA display at the specified
//  coordinates.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses VgaRectangle::FillBox() to draw the rectangle on the VGA display.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
void  
VgaDisplayContext::fillRectangle( 
					GraphicsContext & rGc, 
					Int x0, Int y0, Int x1, Int y1 )
{
  // $[TI1]
  VgaRectangle::FillBox(*this, rGc, x0, y0, x1, y1);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: drawTriangle
//
//@ Interface-Description
//  Renders a non-filled triangle to the VGA display at the specified
//  coordinates.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses VgaTriangle::DrawTriangle() to draw the triangle on the VGA display.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
void  
VgaDisplayContext::drawTriangle( 
					GraphicsContext & rGc,
					Int x0, Int y0,
					Int x1, Int y1,
					Int x2, Int y2 )
{
  // $[TI1]
  VgaTriangle::DrawTriangle(*this, rGc, x0, y0, x1, y1, x2, y2);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: fillTriangle
//
//@ Interface-Description
//  Renders a filled triangle to the VGA display at the specified
//  coordinates.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses VgaTriangle::FillTriangle() to draw the filled triangle on the
//  VGA display.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
void  
VgaDisplayContext::fillTriangle( 
					GraphicsContext & rGc,
					Int x0, Int y0,
					Int x1, Int y1,
					Int x2, Int y2 )
{
  // $[TI1]
  VgaTriangle::FillTriangle(*this, rGc, x0, y0, x1, y1, x2, y2);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: drawBitmap
//
//@ Interface-Description
//  Renders the specified bitmap to the VGA display.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses VgaDib::DisplayBitmap() to draw the bitmap on the VGA display.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
void  
VgaDisplayContext::drawBitmap( 
					GraphicsContext & rGc,
					DibInfo & rDibInfo,
					Int xPos, Int yPos )
{
  // $[TI1]
  VgaDib::DisplayBitmap(*this, rGc, rDibInfo, xPos, yPos);  
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: drawText
//
//@ Interface-Description
//  Writes the specified character string to the VGA display.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses VgaFont::TextOut() to render the character string to the display.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
 
void  
VgaDisplayContext::drawText(
					GraphicsContext & rGc,
					Int xPos, Int yPos, Uint fontId, const wchar_t* pString)
{

	CFont* font = VgaFontData::FontTable[fontId];
	pCDC_->SelectObject(font);
	pCDC_->SetTextAlign(TA_BASELINE);
	pCDC_->ExtTextOutW(xPos, yPos, 0, NULL, pString, wcslen(pString), NULL); 
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: drawJapaneseText
//
//@ Interface-Description
//  Writes the specified Japanese character string to the VGA display.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses VgaFont::TextOutJ() to render the character string to the display.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
void  
VgaDisplayContext::drawJapaneseText(
					GraphicsContext & rGc,
					Int xPos, Int yPos, Uint fontId, const char* pString)
{
	//TODO: BVP - E600
	//We should be able to handle all languges, including Japanese with 
	//a single API eventually. However, the single API (i.e. drawText()
	//above) needs an interface change to support wide characters. For now,
	//just assert for any request of Japanese text display.
	CLASS_PRE_CONDITION(FALSE);

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
VgaDisplayContext::SoftFault(const SoftFaultID softFaultID,
		   const Uint32 lineNumber,
		   const char *pFileName,
		   const char *pPredicate)
{
  // $[TI1]

	FaultHandler::SoftFault(softFaultID, VGA_GRAPHICS_DRIVERS,
			  VgaGraphicsDriver::VGA_DISPLAY_CONTEXT_ID, 
			  lineNumber, pFileName, pPredicate);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  setTextColor(RgbQuad& color)
//
//@ Interface-Description
// Set the text color. This change is effective until changed through
// another invocation of this API.
//---------------------------------------------------------------------
//@ Implementation-Description
// Sets the text color on the underlying CDC.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void VgaDisplayContext::setTextColor(RgbQuad& color)
{
	COLORREF cRef = RGB(color.rgbRed, color.rgbGreen, color.rgbBlue);
	pCDC_->SetTextColor(cRef);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getCDC(void) const
//
//@ Interface-Description
// Return the underlying device context.
//---------------------------------------------------------------------
//@ Implementation-Description
// Just returns the member variable.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
CDC* VgaDisplayContext::getCDC(void) const
{
    return pCDC_; 
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  setCDC(CDC* pCDC)
//
//@ Interface-Description
// Sets the underlying device context. If one is set already, it is 
// overwritten.
//---------------------------------------------------------------------
//@ Implementation-Description
// Sets the member variable. It also sets the default background mode to
// be 'transparent'
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void VgaDisplayContext::setCDC(CDC* pCDC)
{
    pCDC_ = pCDC;
	//Initialize the background mode to transparent
	//TODO: BVP - E600 We may have to revisit this. Is there any time that we
	//do not want transparent background (as per MSDN help, this this API only 
	//applies to the behavavior of "drawing text, hatched brushes, or any pen style 
	//that is not a solid line."
	pCDC_->SetBkMode(TRANSPARENT);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isSameVideo_
//
//@ Interface-Description
//  Returns TRUE if the two RGB values specified are equivalent when
//  displayed using a 6-bit VGA palette.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
Boolean
VgaDisplayContext::isSameVideo_( const RgbQuad & rgb, const RgbQuad & key ) const
{
  // $[TI1.1] $[TI1.2]
  return (   key.rgbRed >> 2 == rgb.rgbRed >> 2
          && key.rgbGreen >> 2 == rgb.rgbGreen >> 2
          && key.rgbBlue >> 2 == rgb.rgbBlue >> 2 );
}
