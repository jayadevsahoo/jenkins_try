#include "stdafx.h"
#if	defined(SIGMA_CLIENT)
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Nellcor/Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: Queue - class provides inter-task communication,
//  synchronization and buffering  Int32 messages through a common
//  message queue.
//---------------------------------------------------------------------
//@ Interface-Description
//   An Int32 size message can be sent to a Queue object and the message
//   can be extracted in the order that it was queued with or without
//   a maximum wait time.
//---------------------------------------------------------------------
//@ Rationale
//  The Queue class was written to hide the details of using the VRTX32  
//  queue mechanism, to isolate problems related to inter-task communication,
//  and to create a safe and simple-to-use interface for passing messages 
//  between tasks.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Queue uses the VRTX32 ::sc_q* calls to create and communicate to 
//  queues, which live in the VRTX32 Workspace.  
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions  and pre-conditions are used to
//  ensure correctness of software conditons.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Download/vcssrc/Queue.ccv   25.0.4.0   19 Nov 2013 14:02:50   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date: 15-Jan-99    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001  By:  jhv    Date:  25-Jan-1996    DR Number:
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//=====================================================================

#include "Queue.hh"
#include "Download.hh"

//@ Code...

//=====================================================================
//@ Method: Queue(Int qId) 
//
//@ Interface-Description
//    Creates a VRTX queue.
//---------------------------------------------------------------------
//@ Implementation-Description
//	 The ::sc_qecreate function is called to create a VRTX queue.
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
Queue::Queue(Int qId) 
    : QID_(qId)
{
	// $[TI1]
    ::sc_qecreate(QID_, QUEUE_LENGTH, PRIORITY_ORDER, &error_);
	if ( error_ != RET_OK )
	{
		printf("Queue::Queue %d \n", error_ );
	}
	CLASS_ASSERTION(error_ == RET_OK );
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~Queue(void)
//
//@ Interface-Description
//		Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
Queue::~Queue(void)
{
	// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: putMsg(const Int32 msg)
//
//@ Interface-Description
//  Puts the message on the end of the queue depending on the success of
//  the operation. Takes a constant Int32 msg as the input parameter. 
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls the static method MsgQueue::PutMsg with a queue ID and the
//  passed in message.
//---------------------------------------------------------------------
//@ PreCondition
//    CLASS_PRE_CONDITION( msg );
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
Queue::putMsg(const Int32 msg)
{
	// $[TI1]
	CLASS_PRE_CONDITION( msg );

    ::sc_qpost((int)QID_,(VRTX_MSG)msg,(int *)&error_);
    CLASS_ASSERTION( error_ == RET_OK);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: pendForMsg(Uint32 waitTime)
//
//@ Interface-Description
//  Returns the next message, if any, from the queue in the specified
//  time period. Uses a reference to an Int32 which holds the returned
//  message. Also takes a Uint32 timeout value in milliseconds, which 
//  if it expires without a message arriving, NO_ERROR status returns.
//  The value zero can be passed as the timeout value, and is the default.
//  This causes the pendForMsg to wait until a message is received. 
//---------------------------------------------------------------------
//@ Implementation-Description
//  The pendForMsg() method calls ::sc_qpend with the timeout that 
//  is passed in converted to VRTX32 ticks.
//  To avoid an being indefinitely suspended, the method calls ::sc_qaccept
//  system call when waitTime is specified to be zero.
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
Int32 Queue::pendForMsg(Uint32 waitTime)
{
Int32 rMsg;
Uint32 waitTics;

    waitTics=(waitTime+(MILLI_SEC_PER_TICK - 1))/MILLI_SEC_PER_TICK;

	if ( !waitTics ) // $[TI1]
    	{
		rMsg = (Int32)::sc_qaccept((int)QID_, (int *)&error_);
	}
	else // $[TI2]
    	{
		rMsg = (Int32)::sc_qpend((int)QID_,(long)waitTics,(int *)&error_);
	}

    CLASS_ASSERTION(error_ == RET_OK || error_ == ER_TMO || error_ == ER_NMP);

	if ( error_ == ER_NMP || error_ == ER_TMO ) // $[TI3]
	{
		return(NO_ERROR);
	}
	else // $[TI4]
    	{
		return rMsg;
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 
void
Queue::SoftFault(const FaultType  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  	DownloadFault::SoftFault(softFaultID, 
  	DOWNLOAD, QUEUE_CLASS, lineNumber, pFileName, pPredicate); 
}

#endif	//defined(SIGMA_CLIENT)
