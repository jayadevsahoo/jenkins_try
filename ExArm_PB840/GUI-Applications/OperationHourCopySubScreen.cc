#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: OperationHourCopySubScreen - Activated by selecting Copy
// Operation hour button on the the Service lower subScreen tab button.
//---------------------------------------------------------------------
//@ Interface-Description
// This subscreen is activated by selecting Copy Operation hour
// button in the ServiceLowerOtherScreen. 
// Only one instance of this class is created by LowerSubScreenArea.  The
// interface is pretty generic, as a SubScreen it defines the activate()
// and deactivate() methods and then receives button events
// through via the usual means (i.e., the buttonDownHappened(),
// buttonUpHappened() and adjustPanelAcceptPressHappened()) 
//---------------------------------------------------------------------
//@ Rationale
// Groups the components of the datakey update subscreen in a single class.
//---------------------------------------------------------------------
//@ Implementation-Description
// The main operation of this class is to invoke the BD to start the
// datakey update test via the adjustPanelAcceptPressHappened() and the
// adjustPanelClearPressHappened() events.  It also waits for test result data
// and test commands received from Service-Data and dispatched to the virtual
// methods processTestPrompt(), processTestKeyAllowed(), 
// processTestResultStatus(), processTestResultCondition(), and 
// processTestData().  
// The display of this subscreen is organized into many different screen
// layouts corresponding to different stages of the test process.
// The user's responses to the prompts displayed are what drives
// the process forward.
//
// The ServiceDataRegistrar is used to register for servicd data events.
// All other functionality is inherited from the SubScreenArea class.
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and
// pre-conditions to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
// Only one instance of this class should be created.  It should only be
// displayed in the LowerSubScreenArea.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/OperationHourCopySubScreen.ccv   25.0.4.0   19 Nov 2013 14:08:12   pvcs  $ 
//
//@ Modification-Log
//
//  Revision: 002  By:  hct    Date:  15-DEC-1999    DR Number: 5562
//       Project:  BiLevel (R8027)
//       Description:
//             Add requirement numbers.
//
//  Revision: 001  By:  yyy    Date:  01-Sep-1998    DR Number:
//       Project:  BiLevel (R8027)
//       Description:
//             Initial coding.
//=====================================================================

#include "OperationHourCopySubScreen.hh"

#include "MiscStrs.hh"
#include "PromptStrs.hh"
#include "SmTestId.hh"
#include "LowerScreen.hh"
#include "ServiceLowerScreen.hh"
#include "ServiceLowerScreenSelectArea.hh"
#include "ServiceUpperScreen.hh"
#include "UpperSubScreenArea.hh"

#if defined FAKE_SM
#	include "FakeServiceModeManager.hh"
#   define SmManager FakeServiceModeManager
#else		
#	include "SmManager.hh"
#endif	// FAKE_SM

//@ Usage-Classes
#include "AdjustPanel.hh"
#include "PromptArea.hh"
#include "Sound.hh"
#include "Colors.hh"
class SubScreenArea;
//@ End-Usage

//@ Code...

// Initialize static constants.
static const Int32 STATUS_LABEL_X_ = 379;
static const Int32 STATUS_LABEL_Y_ = 0;
static const Int32 STATUS_WIDTH_ = 255;
static const Int32 STATUS_HEIGHT_ = 31;
static const Int32 ERR_DISPLAY_AREA_X1_ = 0;
static const Int32 ERR_DISPLAY_AREA_Y1_ = 100;
static const Int32 ERR_DISPLAY_ROW_WIDTH_ = 20;

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: OperationHourCopySubScreen()  [Default Constructor]
//
//@ Interface-Description
// Creates the Flow Sensor Calibration Subscreen.  The given `pSubScreenArea' is 
// the SubScreenArea in which it will be displayed (in this case the upper 
// subscreen area).
//---------------------------------------------------------------------
//@ Implementation-Description
// Creates, positions, and initializes all the graphical components of this
// subscreen:  title and a number of static and dynamic textual items.
//  Also, register for callback for the Start Test button.
// $[NE07000] $[NE07001] $[NE07002] $[NE07003]
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

OperationHourCopySubScreen::OperationHourCopySubScreen(SubScreenArea *pSubScreenArea) :
	SubScreen(pSubScreenArea),
	calStatus_(STATUS_LABEL_X_, STATUS_LABEL_Y_,
				STATUS_WIDTH_, STATUS_HEIGHT_,
				MiscStrs::EXH_V_CAL_STATUS_LABEL,
				MiscStrs::OPER_TIME_INCOMPLETE_MSG),
	errorHappened_(FALSE),
	errorIndex_(0),
	keyAllowedId_(-1),
	userKeyPressedId_(SmPromptId::NULL_ACTION_ID),
	guiTestMgr_(this),
	titleArea_(MiscStrs::OPERATION_HOUR_COPY_SUBSCREEN_TITLE)
{
	CALL_TRACE("OperationHourCopySubScreen::OperationHourCopySubScreen(pSubScreenArea)");

	// Size and position the sub-screen
	setX(0);
	setY(0);
	setWidth(SERVICE_LOWER_SUB_SCREEN_AREA_WIDTH);
	setHeight(SERVICE_LOWER_SUB_SCREEN_AREA_HEIGHT);

	for (int i=0; i<MAX_CONDITION_ID; i++)
	{
		errDisplayArea_[i].setColor(Colors::WHITE);
		errDisplayArea_[i].setY(ERR_DISPLAY_AREA_Y1_+(i*ERR_DISPLAY_ROW_WIDTH_));
		errDisplayArea_[i].setX(ERR_DISPLAY_AREA_X1_);
		addDrawable(&errDisplayArea_[i]);
	}

	// Add the title area
	addDrawable(&titleArea_);

	// Add status line text objects to main container
	addDrawable(&calStatus_);

	// Initialize the promptName, promptId arrays and prompts after an error
	// condition has occured
	setPromptTable_();
	setErrorTable_();

	guiTestMgr_.setupTestResultDataArray();
	guiTestMgr_.setupTestCommandArray();
													// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~OperationHourCopySubScreen  [Destructor]
//
//@ Interface-Description
//  Destroys the Enter Service subscreen.  Needs to do nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Empty 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

OperationHourCopySubScreen::~OperationHourCopySubScreen(void)
{
	CALL_TRACE("OperationHourCopySubScreen::~OperationHourCopySubScreen(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activate
//
//@ Interface-Description
//  Called by LowerSubScreenArea just before this subscreen is displayed
//  on the screen.  No parameters needed.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The first thing to do is to register the subscreen with
//  Gui Test manager for test events.  Then we call the generic screen layout
//  method to display the initial subscreen configuration.  Finally,
//  the start test command is sent to BD to execute this operation.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
OperationHourCopySubScreen::activate(void)
{
	CALL_TRACE("OperationHourCopySubScreen::activate(void)");

	//
	// Register all general possible test results for test events, but first
	// we have to initialize ServiceDataRegistrar.
	//
	guiTestMgr_.registerTestResultData();
	guiTestMgr_.registerTestCommands();

	// Let the service mode  manager knows the type of Service mode.
	SmManager::DoFunction(SmTestId::SET_TEST_TYPE_MISC_ID);

	layoutScreen_(DK_OP_HOUR_START);
	
	// Start the test.
	SmManager::DoTest(SmTestId::DK_COPY_HOURS_ID);
	errorHappened_ = FALSE;
													// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: deactivate
//
//@ Interface-Description
//  Called by LowerSubScreenArea just after this subscreen has been
//  removed from the screen.  No parameters are needed.
//---------------------------------------------------------------------
//@ Implementation-Description
//  We perform any necessary cleanup.  First signal to Gui Test manager
//  about the end of test.  Release the AdjustPanel focus, then clear
//  the prompt area.  Finally, the LowerScreen Select area is shown.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
OperationHourCopySubScreen::deactivate(void)
{
	CALL_TRACE("OperationHourCopySubScreen::deactivate(void)");

	// Make sure that nothing in this subscreen keeps focus.
	AdjustPanel::TakeNonPersistentFocus(NULL, TRUE);

	// Clear the Primary prompt
	guiTestMgr_.setTestPrompt(PromptArea::PA_PRIMARY,
		 PromptArea::PA_HIGH, NULL_STRING_ID);

	guiTestMgr_.setTestPrompt(PromptArea::PA_PRIMARY,
		 PromptArea::PA_LOW, NULL_STRING_ID);

	// Clear the Advisory prompts
	guiTestMgr_.setTestPrompt(PromptArea::PA_ADVISORY,
		 PromptArea::PA_HIGH, NULL_STRING_ID);

	// Clear the Advisory prompts
	guiTestMgr_.setTestPrompt(PromptArea::PA_ADVISORY,
		 PromptArea::PA_LOW, NULL_STRING_ID);

	// Clear the Secondary prompt
	guiTestMgr_.setTestPrompt(PromptArea::PA_SECONDARY,
		PromptArea::PA_HIGH, NULL_STRING_ID);

	// Clear the Secondary prompt
	guiTestMgr_.setTestPrompt(PromptArea::PA_SECONDARY,
		PromptArea::PA_LOW, NULL_STRING_ID);


	// Activate lower subscreen area.
	ServiceLowerScreen::RServiceLowerScreen.getServiceLowerScreenSelectArea()->setBlank(FALSE);
												// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelAcceptPressHappened
//
//@ Interface-Description
//  This is a virtual method inherited from being an AdjustPanelTarget.  It is
//  normally called when the operator presses down the Accept key to signal the
//  accepting of continuing the test process.
//---------------------------------------------------------------------
//@ Implementation-Description
//  First reset the Adjust Panel focus, then clear the prompt area.
//  Finally, layout the screen accordingly.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
OperationHourCopySubScreen::adjustPanelAcceptPressHappened(void)
{
	CALL_TRACE("OperationHourCopySubScreen::adjustPanelAcceptPressHappened(void)");

	// Make sure that nothing in this subscreen keeps focus.
	AdjustPanel::TakeNonPersistentFocus(NULL, TRUE);

	// Make sure the prompt area's background is dehighlighted.
	guiTestMgr_.setTestPrompt(PromptArea::PA_PRIMARY,
				 PromptArea::PA_HIGH, NULL_STRING_ID);

	guiTestMgr_.setTestPrompt(PromptArea::PA_ADVISORY,
			 PromptArea::PA_HIGH, NULL_STRING_ID);

	guiTestMgr_.setTestPrompt(PromptArea::PA_SECONDARY,
			PromptArea::PA_HIGH, NULL_STRING_ID); 

	// Set user key pressed id.
	userKeyPressedId_ = SmPromptId::KEY_ACCEPT;
		
	// ACCEPT key being pressed in response to the test PROMPT.
	layoutScreen_(PROMPT_ACCEPT_BUTTON_PRESSED);
	// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelClearPressHappened
//
//@ Interface-Description
//  This is a virtual method inherited from being an AdjustPanelTarget.  It is
//  normally called when the operator presses down the Cleart key to signal the
//  rejection of continuing the test process.
//---------------------------------------------------------------------
//@ Implementation-Description
//  First reset the Adjust Panel focus, then clear the prompt area.
//  Next, set userKeyPressedId_ to KEY_CLEAR.  Finally, layout the
//  screen accordingly.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
OperationHourCopySubScreen::adjustPanelClearPressHappened(void)
{
	CALL_TRACE("OperationHourCopySubScreen::adjustPanelClearPressHappened(void)");

	// Make sure that nothing in this subscreen keeps focus.
	AdjustPanel::TakeNonPersistentFocus(NULL, TRUE);

	// Make sure the prompt area's background is dehighlighted.
	guiTestMgr_.setTestPrompt(PromptArea::PA_PRIMARY,
				 PromptArea::PA_HIGH, NULL_STRING_ID);

	guiTestMgr_.setTestPrompt(PromptArea::PA_ADVISORY,
			 PromptArea::PA_HIGH, NULL_STRING_ID);

	guiTestMgr_.setTestPrompt(PromptArea::PA_SECONDARY,
			PromptArea::PA_HIGH, NULL_STRING_ID); 

	// Set user key pressed id.
	userKeyPressedId_ = SmPromptId::KEY_CLEAR;
		
	// CLEAR key being pressed in response to the test PROMPT.
	layoutScreen_(PROMPT_CLEAR_BUTTON_PRESSED);
								// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelRestoreFocusHappened
//
//@ Interface-Description
// This is a virtual method inherited from being an AdjustPanelTarget.
// It is normally called when the operator releases the off-screen
// keyboard key and the AdjustPanel needs to restore the default
// AdjustPanel target.  It is the duty of this inherited method to restore
// the previous test prompts in order to continue the Calibration
// process.  
//---------------------------------------------------------------------
//@ Implementation-Description
//  Call GuiManager's virtual method to do the job.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
OperationHourCopySubScreen::adjustPanelRestoreFocusHappened(void)
{
	CALL_TRACE("OperationHourCopySubScreen::adjustPanelRestoreFocusHappened(void)");

#if defined FORNOW
printf("adjustPanelRestoreFocusHappened at line  %d\n", __LINE__);
#endif	// FORNOW

	guiTestMgr_.restoreTestPrompt();
	// $[TI1]
}


//============================ m e t h o d   d e s c r i p t i o n ====
//@ Method: processTestPrompt_
//
//@ Interface-Description
//  Process the prompt command given by Service-Mode.
//---------------------------------------------------------------------
//@ Implementation-Description
//  First, we grab the Adjust Panel focus, then we update the prompt area
//  and the status area.  Next, we update the screen display id.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
OperationHourCopySubScreen::processTestPrompt(Int command)
{
	CALL_TRACE("OperationHourCopySubScreen::processTestPrompt(Int command)");

#if defined FORNOW
printf("alibrationSetupSubScreen::processTestPrompt() command=%d\n", command);
#endif	// FORNOW

	// Grab Adjust Panel focus
	AdjustPanel::TakeNonPersistentFocus(this, TRUE);

	// Disable the knob sound.
	AdjustPanel::DisableKnobRotateSound();

	// Error checking the prompt received 
	Int idx = 0;
	for (idx = 0;
			 idx < MAX_CAL_PROMPTS && promptId_[idx] != command;
			 idx++);
	SAFE_CLASS_ASSERTION(idx < MAX_CAL_PROMPTS);

	// Set primary prompt to the corresponding message.
	guiTestMgr_.setTestPrompt(PromptArea::PA_PRIMARY, 
								PromptArea::PA_HIGH,
								promptName_[idx]);

	guiTestMgr_.setTestPrompt(PromptArea::PA_ADVISORY,
								PromptArea::PA_HIGH,
								NULL_STRING_ID); 

	switch (keyAllowedId_)
	{
	case SmPromptId::ACCEPT_AND_CANCEL_ONLY:		// $[TI1]
		guiTestMgr_.setTestPrompt(PromptArea::PA_SECONDARY, 
								PromptArea::PA_HIGH,
								PromptStrs::OP_HOUR_PROMPT_ACCEPT_CANCEL_S);
		break;

	default:
		CLASS_ASSERTION_FAILURE();			// Illegal event
		break;
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processTestKeyAllowed
//
//@ Interface-Description
//  Process keyAllowed command given by Service-Mode.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Simply remember the command which represents the key allowed for
//  the operator to press.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
OperationHourCopySubScreen::processTestKeyAllowed(Int command)
{
	CALL_TRACE("OperationHourCopySubScreen::processTestKeyAllowed(Int command)");

#if defined FORNOW
printf("OperationHourCopySubScreen::processTestKeyAllowed() command=%d\n", command);
#endif	// FORNOW

	// Remember the expected user key response ID
	keyAllowedId_ = command;
	// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processTestResultStatus
//
//@ Interface-Description
//  Process the test result status given by Service Mode. 
//---------------------------------------------------------------------
//@ Implementation-Description
//  Dispatch the given command and make the corresponding layoutScreen_
//  call to update the screen display.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
OperationHourCopySubScreen::processTestResultStatus(Int resultStatus, Int testResultId)
{
	CALL_TRACE("OperationHourCopySubScreen::processTestResultStatus_"
								"(Int resultStatus Int testResultId)");

#if defined FORNOW
printf("alibrationSetupSubScreen::processTestResultStatus() resultStatus=%d\n", resultStatus);
#endif	// FORNOW

	switch (resultStatus)
	{
	case SmStatusId::TEST_END:			// $[TI1]
		if (!errorHappened_ && userKeyPressedId_ != SmPromptId::KEY_CLEAR)
		{								// $[TI1.1]
			// Always display complete status message
			calStatus_.setStatus(MiscStrs::OPER_TIME_COMPLETE_MSG);
		}								// $[TI1.2]

		// Activate the lower screen 's select area.
		ServiceLowerScreen::RServiceLowerScreen.getServiceLowerScreenSelectArea()->setBlank(FALSE);

		// Set primary prompt to start test.
		guiTestMgr_.setTestPrompt(PromptArea::PA_PRIMARY,
					PromptArea::PA_LOW, 
					NULL_STRING_ID);

		// Erase the advisory prompt
		guiTestMgr_.setTestPrompt(PromptArea::PA_ADVISORY,
					PromptArea::PA_HIGH,
					NULL_STRING_ID);

		// Set the secondary prompt to To cancel touch other screen.
		guiTestMgr_.setTestPrompt(PromptArea::PA_SECONDARY,
					PromptArea::PA_LOW, 
					PromptStrs::OTHER_SCREENS_CANCEL_S);
		break;

	case SmStatusId::TEST_START:		// $[TI2]
		break;

	case SmStatusId::TEST_ALERT:		// $[TI3]
	case SmStatusId::TEST_FAILURE:
		errorHappened_ = TRUE;

		// Always display update failed status message
		calStatus_.setStatus(MiscStrs::OPER_TIME_UPDATE_FAILED_MSG);
		break;

	default:
		CLASS_ASSERTION_FAILURE();
		break;
	}
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processTestResultCondition
//
//@ Interface-Description
//  Process test result condition passed by Service-Mode.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Display the text for the corresponding result condition on the screen
//  and update the prompt area.
//---------------------------------------------------------------------
//@ PreCondition
//  Result condition has to be in the range [0, MAX_CONDITION_ID]
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
OperationHourCopySubScreen::processTestResultCondition(Int resultCondition)
{
	CALL_TRACE("OperationHourCopySubScreen::processTestResultCondition(Int resultCondition)");

	SAFE_CLASS_ASSERTION((resultCondition >= 0) && (resultCondition <= MAX_CONDITION_ID));

#if defined FORNOW
printf("OperationHourCopySubScreen::processTestResultCondition() resultCondition=%d\n", resultCondition);
#endif	// FORNOW

	if (resultCondition > 0)
	{						// $[TI1]
		errDisplayArea_[errorIndex_].setText(conditionText_[resultCondition-1]);
		errDisplayArea_[errorIndex_].setShow(TRUE);
		errorIndex_++;

		guiTestMgr_.setTestPrompt(PromptArea::PA_ADVISORY,
				PromptArea::PA_LOW, PromptStrs::EXH_V_CAL_SEE_OP_MANUAL_A);

		guiTestMgr_.setTestPrompt(PromptArea::PA_SECONDARY,
				PromptArea::PA_HIGH, NULL_STRING_ID);
	}						// $[TI2]
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processTestData
//
//@ Interface-Description
//  Process data given by Service-Mode
//---------------------------------------------------------------------
//@ Implementation-Description
//  Since this subscreen is designed not to display test data,
//  this remains an empty method
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
OperationHourCopySubScreen::processTestData(Int dataIndex, TestResult *pResult)
{
	CALL_TRACE("OperationHourCopySubScreen::processTestData(Int dataIndex, TestResult *pResult)");
	// Empty method
	// OperationHourCopy SubScreen doesn't display test data
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: layoutScreen_
//
//@ Interface-Description
// Sets the subscreen into one of three display configurations.
//---------------------------------------------------------------------
//@ Implementation-Description
//  If screen id is DK_OP_HOUR_START, we display incomplete status message,
//  hide the lower screen select area and clear any previous errors.
//  show the Start Test button.
//  If screen id is PROMPT_ACCEPT_BUTTON_PRESSE, we update the prompt, status,
//  then communicate this state to Service Mode manager.
//  If screen id is PROMPT_CLEAR_BUTTON_PRESSED, we update the prompt, status,
//  then communicate this state to Service Mode manager.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
OperationHourCopySubScreen::layoutScreen_(CalScreenId calScreenId)
{
	CALL_TRACE("OperationHourCopySubScreen::layoutScreen_(CalScreenId calScreenId)");

	switch (calScreenId)
	{
	case DK_OP_HOUR_START:				// $[TI1]
		// Always display incomplete status message
		calStatus_.setStatus(MiscStrs::OPER_TIME_INCOMPLETE_MSG);

		// Hide the lower screen select area.
		ServiceLowerScreen::RServiceLowerScreen.getServiceLowerScreenSelectArea()->setBlank(TRUE);

		// Clear any previous errors
		setErrorTable_();
		break;

	case PROMPT_ACCEPT_BUTTON_PRESSED:	// $[TI2]

		// Set Primary prompt to "Please Wait...."
		guiTestMgr_.setTestPrompt(PromptArea::PA_PRIMARY,
			PromptArea::PA_LOW, PromptStrs::EXH_V_CAL_PLEASE_WAIT_P);

		guiTestMgr_.setTestPrompt(PromptArea::PA_ADVISORY,
				 PromptArea::PA_HIGH, NULL_STRING_ID);

		guiTestMgr_.setTestPrompt(PromptArea::PA_SECONDARY,
				PromptArea::PA_HIGH, NULL_STRING_ID);

		// Signal the Service Test Manager to continue the test.
		SmManager::OperatorAction(SmTestId::DK_COPY_HOURS_ID,
						  userKeyPressedId_);
		break;

	case PROMPT_CLEAR_BUTTON_PRESSED:	// $[TI3]
		// Always display complete status message
		calStatus_.setStatus(MiscStrs::OPER_TIME_INCOMPLETE_MSG);

		// Set Primary prompt to "Please Wait...."
		guiTestMgr_.setTestPrompt(PromptArea::PA_PRIMARY,
			PromptArea::PA_LOW, PromptStrs::EXH_V_CAL_PLEASE_WAIT_P);

		guiTestMgr_.setTestPrompt(PromptArea::PA_ADVISORY,
				 PromptArea::PA_HIGH, NULL_STRING_ID);

		guiTestMgr_.setTestPrompt(PromptArea::PA_SECONDARY,
				PromptArea::PA_HIGH, NULL_STRING_ID);

		SmManager::OperatorAction(SmTestId::DK_COPY_HOURS_ID,
						  userKeyPressedId_);
		break;

	default:
		CLASS_ASSERTION_FAILURE();
		break;
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setPromptTable_
//
//@ Interface-Description
//  Initialize the promptId_, promptName_ and conditionText_ tables.  
//  The promptId_ array contains Service-Mode prompt ids.
//  The promptName_ table contains translation text of Service-Mode prompt code.
//  The conditionText_ table contains translation text of Service-Mode
//  error conditions.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Simply set the proper text and ids to the corresponding tables.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
OperationHourCopySubScreen::setPromptTable_(void)
{
	Int testIndex = 0;

	CALL_TRACE("OperationHourCopySubScreen::setPromptTable_(void)");

	// First clear each entry in the array.
	for (testIndex = 0; testIndex < MAX_CAL_PROMPTS; testIndex++)
	{
		promptId_[testIndex]	= SmPromptId::NULL_PROMPT_ID;
		promptName_[testIndex] 	= NULL_STRING_ID;
	}

	testIndex = 0;

	promptId_[testIndex] = SmPromptId::INSERT_DK_TO_BE_COPIED_FROM_PROMPT;
	promptName_[testIndex++] = PromptStrs::INSERT_DK_TO_BE_COPIED_FROM_P;

	promptId_[testIndex] = SmPromptId::INSERT_DK_TO_BE_COPIED_TO_PROMPT;
	promptName_[testIndex++] = PromptStrs::INSERT_DK_TO_BE_COPIED_TO_P;

#ifdef SIGMA_DEVELOPMENT

	// Make sure total test count is within the maximum allowed.
	SAFE_CLASS_ASSERTION(testIndex == MAX_CAL_PROMPTS);

	// Do some safe assertions that all entries in the array have been filled
	for (testIndex = 0; testIndex < MAX_CAL_PROMPTS; testIndex++)
	{
		SAFE_CLASS_ASSERTION(promptId_[testIndex] != SmPromptId::NULL_PROMPT_ID);
		SAFE_CLASS_ASSERTION(promptName_[testIndex] != NULL_STRING_ID);
	}
#endif // SIGMA_DEVELOPMENT
											// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setErrorTable_
//
//@ Interface-Description
// 	Sets the error text in the error table.  Any condition that arises
//	has a unique id which is used as an index into this table of condition
//	text.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Assign condition text (non-cheap text) to error table.
//	We also initialize the error display area's show flag.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
OperationHourCopySubScreen::setErrorTable_(void)
{
	Int testIndex = 0;

	CALL_TRACE("OperationHourCopySubScreen::setErrorTable_(void)");


#if defined FORNOW
printf("OperationHourCopySubScreen::setErrorTable_() at line  %d, this = %d\n", __LINE__, this);
#endif	// FORNOW

	// Reset status flag
	errorHappened_ = FALSE;
	errorIndex_ = 0;


	conditionText_[testIndex++] = MiscStrs::OP_HOUR_DK_NOT_INSTALLED;
	conditionText_[testIndex++] = MiscStrs::OP_HOUR_DK_WRITE_ERROR;
	conditionText_[testIndex++] = MiscStrs::OP_HOUR_DK_CONFIRMATION_ERROR;
	conditionText_[testIndex++] = MiscStrs::OP_HOUR_DK_SN_NOT_THE_SAME;

	// Make sure total test count is within the maximum allowed.
	CLASS_ASSERTION(testIndex <= MAX_CONDITION_ID);

	for (int i=0; i < MAX_CONDITION_ID; i++)
	{	
		errDisplayArea_[i].setShow(FALSE);
	}	

	// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
OperationHourCopySubScreen::SoftFault(const SoftFaultID  softFaultID,
							 const Uint32       lineNumber,
							 const char*        pFileName,
							 const char*        pPredicate)  
{
	CALL_TRACE("OperationHourCopySubScreen::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS,
							OPERATIONHOURCOPYSUBSCREEN,
							lineNumber, pFileName, pPredicate);
}
