//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file Message.cpp
//----------------------------------------------------------------------------

//-------------------------------------
// I N C L U D E S
//-------------------------------------
#include"Message.h"


//-------------------------------------
// C O D E
//-------------------------------------

//---------------------------
// Message
//---------------------------
Message::Message(boost::shared_ptr<char> data, unsigned int iLength):
    m_time(),m_raw_data(data),m_iLength(iLength)
{
}

//---------------------------
// Message
//---------------------------
Message::Message():
m_time(),
m_raw_data(),
m_iLength()
{
}

//---------------------------
// ~Message
//---------------------------
Message::~Message()
{
}

//---------------------------
// get
//---------------------------
boost::shared_ptr<char> Message::get()
{
    return m_raw_data;
}

//---------------------------
// set
//---------------------------
void Message::set(boost::shared_ptr<char> data, unsigned int iLength)
{    
}

//---------------------------
// getTimeStamp
//---------------------------
EpochTimeStamp &Message::getTimeStamp()
{
    return  m_time;
}

//---------------------------
// setTimeStamp
//---------------------------
void  Message::setTimeStamp(const EpochTimeStamp &rTimeStamp)
{
    //TODO[ME]:
}

//---------------------------
// setTimeStamp
//---------------------------
void  Message::setTimeStamp()
{
    //TODO[ME]:
}




