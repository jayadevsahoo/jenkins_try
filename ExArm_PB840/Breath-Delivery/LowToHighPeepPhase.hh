
#ifndef LowToHighPeepPhase_HH
#define LowToHighPeepPhase_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: LowToHighPeepPhase - Implements transition from low to high
//		peep during BiLevel.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/LowToHighPeepPhase.hhv   25.0.4.0   19 Nov 2013 13:59:52   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 002  By:  iv    Date: 27-Sep-1998     DR Number:DCS 5140
//       Project:  BiLevel
//       Description:
//             Redefined relinquishControl().
//
//  Revision: 001  By:  syw    Date:  08-Dec-1997    DR Number: none
//       Project:  Sigma (840)
//       Description:
//		 	BiLevel initial version.
//
//====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"

//@ Usage-Classes

#include "PsvPhase.hh"

//@ End-Usage

class LowToHighPeepPhase : public PsvPhase
{
  public:
    LowToHighPeepPhase( void) ;
    virtual ~LowToHighPeepPhase( void) ;

    static void SoftFault( const SoftFaultID softFaultID,
							   const Uint32      lineNumber,
							   const char*       pFileName  = NULL, 
							   const char*       pPredicate = NULL) ;
    virtual void relinquishControl( const BreathTrigger& trigger) ;
  
  protected:
	virtual void determineEffectivePressureAndBiasOffset_( void) ;
	virtual void determineTimeToTarget_( void) ;
	virtual void enableExhalationTriggers_( void) ;

  private:
	LowToHighPeepPhase( const LowToHighPeepPhase&) ;	// not implemented...
    void   operator=( const LowToHighPeepPhase&) ;		// not implemented...

} ;


#endif // LowToHighPeepPhase_HH 
