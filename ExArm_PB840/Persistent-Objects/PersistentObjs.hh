
#ifndef PersistentObjs_HH
#define PersistentObjs_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  PersistentObjs - The Persistent Objects Subsystem's Class.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Persistent-Objects/vcssrc/PersistentObjs.hhv   25.0.4.0   19 Nov 2013 14:18:52   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By:  sah    Date:  01-Jan-1999    DCS Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' to a non-inlined method.
//
//  Revision: 001  By:  sah    Date:  15-Mar-1997    DCS Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//          Initial version (Integration baseline).
//  
//====================================================================

#include "Sigma.hh"
#include "PersistentObjsId.hh"

#if defined(SIGMA_DEVELOPMENT)
#include "BitUtilities.hh"
#endif  // defined(SIGMA_DEVELOPMENT)

//@ Usage-Classes
//@ End-Usage


class PersistentObjs
{
  public:
    static void  Initialize(void);

#if defined(SIGMA_DEVELOPMENT)
    static inline Boolean  IsDebugOn(const PersistentObjsId classId);

    static inline void  TurnDebugOn (const PersistentObjsId classId);
    static inline void  TurnDebugOff(const PersistentObjsId classId);

    static void  TurnAllDebugOn (void);
    static void  TurnAllDebugOff(void);
#endif  // defined(SIGMA_DEVELOPMENT)

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL, 
			   const char*       pPredicate = NULL);

  private:
    PersistentObjs(const PersistentObjs&);		// not implemented...
    PersistentObjs(void);				// not implemented...
    ~PersistentObjs(void);				// not implemented...
    void  operator=(const PersistentObjs&);		// not implemented...

    //@ Constant:  NUM_WORDS_
    // The number of words in 'ArrDebugFlags_[]' to contain all of this
    // subsystem's debug flags.
    enum
    {
      NUM_WORDS_ = (NUM_PERS_OBJS_CLASSES + sizeof(Uint) - 1) / sizeof(Uint)
    };

    //@ Data-Member:  ArrDebugFlags_
    // A static array of debug flags (one bit each) to be used by this
    // subsystem's code.
    static Uint  ArrDebugFlags_[PersistentObjs::NUM_WORDS_];
};


// Inlined Methods...
#include "PersistentObjs.in"


#endif // PersistentObjs_HH 
