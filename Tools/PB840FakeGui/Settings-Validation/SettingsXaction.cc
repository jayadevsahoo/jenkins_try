#include "StdAfx.h"
#include "SettingsXaction.hh"
#include "NetworkApp.hh"
#include "PB840SettingsDoc.h"

// NOTE:  MRI compiler won't let me use 'CalcXactionBlockSize_()' here...
const Uint  SettingsXaction::ZERO_ELEMS_BLOCK_SIZE_ =
((Uint)&(((SettingXactionBlock*)NULL)->arrXmitElems[0u]));

const Uint  SettingsXaction::BLOCK_RECV_WAIT_TIME_ = 600u;

SettingsXaction::SettingsXaction(void)
{
}

SettingsXaction::~SettingsXaction(void)
{
}

// for SettingXmitData archive
CArchive& operator<<(CArchive& arc, SettingXmitData& data)
{   
	arc << data.settingXmitId << data.reserved << data.boundedPrec << data.boundedValue;
	return arc;   
}

CArchive& operator>>(CArchive& arc, SettingXmitData& data)
{   
	arc >> data.settingXmitId >> data.reserved >> data.boundedPrec >> data.boundedValue;   
	return arc;  
} 

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TaskDriver()
//
//@ Interface-Description
//  Task loop for the Settings Transaction.  It is this thread in which
//  all of the Settings' Transactions occur.  This task responds to both
//  Task-Control and Task-Monitor messages, and to its internal messages
//  relating to the initiation of a transaction.
//---------------------------------------------------------------------
//@ Implementation-Description
//  When a transaction fails while communications is still up, the info
//  regarding the failed transaction is saved, and a time-limited pend
//  on this task's queue is done.  There are really three different ways
//  for the failure response to be implemented:  do a delay before trying
//  the same transaction again (without pending on the task's queue); put
//  the message back on the task's queue, to try again; or store the info
//  and do a time-limited pend on the task's queue (which is what is being
//  done).  The reason the time-limited pend is used, as opposed to the
//  other two, is because this approach eliminates any negative side-effects.
//  If the "delay" technique were used, responses task monitor and task
//  control messages would be delayed with each failure.  Whereas,  the
//  putting of the message back on the queue corrupts the order of the
//  incoming transaction requests (e.g., you could get a user-update
//  event before that vent-startup event).  The time-limited pend, on the
//  other hand, solves both of these negative side-effects.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
SettingsXaction::TaskDriver()
{
	// create a message queue
	MSG msg;
    PeekMessage(&msg, NULL, WM_USER, WM_USER, PM_NOREMOVE);

	// notify the application, I'm ready to receive message
	HANDLE hEvent = Event::Query( Event::EVENT_SETTING_XACTION );
	SetEvent( hEvent );


	// loop will end only if WM_QUIT be received 
	while ( GetMessage(&msg,0,0,0) )
	{
		switch( msg.message )
		{
		case WM_PUSH_SETTING:

			{

				InterTaskMessage itm;
				memcpy( &itm, &msg.wParam, sizeof(InterTaskMessage) );

			// save before "clearing"...
                const SettingsXactionId  XACTION_ID =
                (SettingsXactionId)SettingsXaction::GetXactionInfo_( itm );

			SettingsXaction::BeginClientXaction_( XACTION_ID );
			break;
			}
		}
	}

	// notify the application, I'm ready to exit
	SetEvent( hEvent );

	/*
	 CALL_TRACE("TaskDriver()");

#if defined(SIGMA_BD_CPU)
    // register for the opening of the Settings-Transaction Socket...
    NetworkApp::RegisterForOpenSocket(&SettingsXaction::EstablishConnection_);
   
#ifdef SIGMA_DEBUG 
    openlog("debug", LOG_PID|LOG_CONS|LOG_PERROR, LOG_USER);
    syslog(LOG_INFO,"BD Debugger started");
#endif

#endif // defined(SIGMA_BD_CPU)


    static AppContext  AppContext_;

#if defined(SIGMA_GUI_CPU)
    AppContext_.setCallback(SettingsXaction::BdReadyCallback_);
#endif // defined(SIGMA_GUI_CPU)
    AppContext_.setCallback(SettingsXaction::ChangeStateCallback_);

    MsgQueue          settingsXactionQ(SETTINGS_XACTION_Q);
    InterTaskMessage  msg;
    Int32             qWord, rval;
    Uint32            waitTime = Ipc::NO_TIMEOUT;

    while( ::DoInfiniteLoop_() )
    {   // $[TI1] -- loop forever; this path is ALWAYS taken...
        // wait for the next event on the BD Server Task Queue...
        rval = settingsXactionQ.pendForMsg(qWord, waitTime);

        if( rval == Ipc::OK )
        {   // $[TI1.1] -- got a new queue message...
            msg = *((struct InterTaskMessage*)&qWord);
        }
#if defined(SIGMA_GUI_CPU)
        else if( rval == Ipc::TIMED_OUT )
        {   // $[TI1.2] -- timeout occurred (GUI only)...
            // timeout should only occur when a previous transaction failed, and
            // that transaction's information is stored in 'FailedXactionId_'...
            AUX_CLASS_ASSERTION(
                               (SettingsXaction::FailedXactionId_ != ::SETTINGS_XACTION_COMPLETE),
                               (Uint)SettingsXaction::FailedXactionId_
                               );

            // setup 'msg' to initiate another transaction attempt...
            SettingsXaction::SetupQueueMsg_(msg, ::INITIATE_TRANSACTION,
                                            SettingsXaction::FailedXactionId_);
        }
#endif // defined(SIGMA_GUI_CPU)
        else
        {
            // unexpected error...
            AUX_CLASS_ASSERTION_FAILURE(rval);
        }

        switch( msg.msgId )
        {
        case TaskControlQueueMsg::TASK_CONTROL_MSG :
        case TaskMonitorQueueMsg::TASK_MONITOR_MSG :    // $[TI1.3]
            // the received message is a task-control or task-monitor message,
            // therefore activate the dispatch of this message...
            AppContext_.dispatchEvent(qWord);
            break;

        case SettingsXaction::XACTION_INITIATION_MSG :  // $[TI1.4]
            {
                const SettingsXactionId  INITIATION_ID =
                SettingsXaction::GetXactionState_(msg);

#if defined(SIGMA_GUI_CPU)
                //-------------------------------------------------------------
                //  BEGIN:  GUI-only block of code...
                //-------------------------------------------------------------

                SAFE_FREE_ASSERTION((INITIATION_ID == ::INITIATE_TRANSACTION),
                                    SETTINGS_VALIDATION, SETTINGS_XACTION);

                // save before "clearing"...
                const SettingsXactionId  XACTION_ID =
                (SettingsXactionId)SettingsXaction::GetXactionInfo_(msg);

                Boolean  isXactionSuccessful;

                // begin a Settings' Transaction...
                isXactionSuccessful = SettingsXaction::BeginClientXaction_(
                                                                          XACTION_ID
                                                                          );

                if( !isXactionSuccessful  &&
                    TaskControlAgent::GetPeerState() != ::STATE_UNKNOWN )
                {   // $[TI1.4.1] -- the transaction failed, but comm is still up...
                    // store failed transaction ID, for new attempts...
                    SettingsXaction::FailedXactionId_ = XACTION_ID;

                    // pend on the queue for the blocked-receive timeout time...
                    waitTime = SettingsXaction::BLOCK_RECV_WAIT_TIME_;
                }
                else
                {   // $[TI1.4.2] -- the transaction succeeded, OR comm is down...
                    // ensure this failure flag is clear...
                    SettingsXaction::FailedXactionId_ = ::SETTINGS_XACTION_COMPLETE;

                    // set back to indefinite pending...
                    waitTime = Ipc::NO_TIMEOUT;
                }

                //-------------------------------------------------------------
                //  END:  GUI-only block of code...
                //-------------------------------------------------------------
#elif defined(SIGMA_BD_CPU)
                //-------------------------------------------------------------
                //  BEGIN:  BD-only block of code...
                //-------------------------------------------------------------

                SAFE_FREE_ASSERTION((INITIATION_ID == ::CONNECTION_INITIATED),
                                    SETTINGS_VALIDATION, SETTINGS_XACTION);

                const Int  XACTION_SOCKET_ID = SettingsXaction::GetXactionInfo_(msg);

                // a connection is being initiated on the Settings-Transaction
                // Socket...
                SettingsXaction::BeginServerXaction_(XACTION_SOCKET_ID);

                //-------------------------------------------------------------
                //  END:  BD-only block of code...
                //-------------------------------------------------------------
#endif // defined(SIGMA_GUI_CPU)
            }   // end of 'SettingsXaction::XACTION_INITIATION_MSG' block...
            break;
        default :
            // 'msg.msgId' is an invalid message ID...
            AUX_CLASS_ASSERTION_FAILURE(msg.msgId);
            break;
        };
    }   // end of infinite 'while' loop...	
	*/
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  BeginClientXaction_(initXactionId)
//
//@ Interface-Description
//  This method conducts the "client-side" transaction.  This is a 7-stage
//  transaction that results in ALL of the currently-accepted settings
//  being transmitted to the BDU.  A 'TRUE' is returned when successful,
//  while a 'FALSE' is returned when the transaction fails.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Only one attempt at completing the transaction is conducted before a
//  failure is reported.
//
//  $[02031] -- all BD settings shall be delivered to the BDU...
//---------------------------------------------------------------------
//@ PreCondition
//  (initXactionId == ::VENT_STARTUP_UPDATE_XMITTED ||
//   initXactionId == ::USER_UPDATE_XMITTED         ||
//   initXactionId == ::ONLINE_SYNC_XMITTED)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean
SettingsXaction::BeginClientXaction_(const SettingsXactionId initXactionId)
{
    CALL_TRACE("BeginClientXaction_(initXactionId)");
    AUX_CLASS_PRE_CONDITION((initXactionId == ::USER_UPDATE_XMITTED  ||
                             initXactionId == ::ONLINE_SYNC_XMITTED  ||
                             initXactionId == ::VENT_STARTUP_UPDATE_XMITTED),
                            initXactionId);

#if defined(SIGMA_DEVELOPMENT)
    const Boolean  IS_DEBUG_ON =
    Settings_Validation::IsDebugOn(::SETTINGS_XACTION);
#endif // defined(SIGMA_DEVELOPMENT)

    //===================================================================
    //  create the transaction block...
    //===================================================================

    SettingXactionBlock  initXactionBlock;

    // set up the transaction block with ALL of the accepted BD values...
    initXactionBlock.xactionId    = initXactionId;
    initXactionBlock.numXmitElems = ::NUM_BD_SETTING_IDS;
 
#ifdef SIGMA_DEBUG
 
#if defined(SIGMA_GUI_CPU)
    // DEBUG ONLY - Purposely fail settings transaction.
    if (FailSettingsTransaction > 0)
    {
       initXactionBlock.numXmitElems = ::NUM_BD_SETTING_IDS - 1;
       printf(  "Changed!!!!!\n\n" );
       FailSettingsTransaction --;
    }
#endif // defined(SIGMA_GUI_CPU)

#endif // define SIGMA_DEBUG

    Uint  idx;

    //-------------------------------------------------------------------
    //  NOTE:  the 'settingId' fields of 'initXactionBlock' are NOT set
    //         because all transactions originating from the GUI send
    //         across ALL settings in their relative indexed location.
    //         Only external interfacing (e.g., EPT testing) need define
    //         the 'settingId' fields.  The message IDs indicate whether
    //         an internal transmission, or external transmission
    //         ('EXTERNAL_UPDATE_XMITTED') are sent.
    //-------------------------------------------------------------------

	CPB840SettingsDoc* p840SettingDoc = CPB840SettingsDoc::GetDocInstance();
	SettingXmitData xmitData;

    // for ALL of the BD, bounded setting values...
    for( idx  = SettingId::LOW_BATCH_BD_BOUNDED_ID_VALUE;
       idx <= SettingId::HIGH_BATCH_BD_BOUNDED_ID_VALUE; idx++ )
    {
		
		p840SettingDoc->GetSettingElement( idx, xmitData );

        // store the data of this setting into this array...
        initXactionBlock.arrXmitElems[idx].boundedPrec = xmitData.boundedPrec;
        initXactionBlock.arrXmitElems[idx].boundedValue  = xmitData.boundedValue;
    }

    // the IDs are to be continuous between the different categories...
    SAFE_CLASS_ASSERTION((idx == SettingId::LOW_BATCH_BD_DISCRETE_ID_VALUE));

    // for ALL of the BD, discrete setting values...
    for( idx  = SettingId::LOW_BATCH_BD_DISCRETE_ID_VALUE;
       idx <= SettingId::HIGH_BATCH_BD_DISCRETE_ID_VALUE; idx++ )
    {
		p840SettingDoc->GetSettingElement( idx, xmitData );

        // store the data of this setting into this array...
        initXactionBlock.arrXmitElems[idx].discreteValue = xmitData.discreteValue;
    }

    // to ensure that there are no new "categories" of BD settings...
    SAFE_CLASS_ASSERTION((idx == ::NUM_BD_SETTING_IDS));

#if defined(SIGMA_GUI_CPU)

    //===================================================================
    //  conduct transaction...
    //===================================================================

#if defined(SIGMA_DEVELOPMENT)
    if( IS_DEBUG_ON )
    {
        cout << "BEGIN TRANSACTION..." << endl;
    }
#endif // defined(SIGMA_DEVELOPMENT)

    SettingXactionBlock  recvBlock;
    Int                  recvBlockSize;

    Int  xmitSize;

    //===================================================================
    // OPEN Settings Transaction Socket...
    //===================================================================

    SOCKET  xactionSocketId;

#if defined(SIGMA_DEVELOPMENT)
    if( IS_DEBUG_ON )
    {
        cout << "<<CLIENT>>  Opening connection:";
    }
#endif // defined(SIGMA_DEVELOPMENT)

    xactionSocketId = NetworkApp::BOpenSocket();

#if defined(SIGMA_DEVELOPMENT)
    if( IS_DEBUG_ON )
    {
        cout << "  socket ID = " << xactionSocketId << endl;
    }
#endif // defined(SIGMA_DEVELOPMENT)

    if( xactionSocketId == INVALID_SOCKET )
    {   // $[TI1]
        SettingsXaction::ReportXactionStatus_(
                                             SettingsXaction::OPEN_SOCKET_FAILURE
                                             );
        #ifdef SIGMA_DEBUG
            printf(  "Failed open socket!!!!\n" );
        #endif

		PRINTF(("Failed open socket!!!!\n"));

        return(FALSE);
    }   // $[TI2]

    //===================================================================
    // RECEIVE BD's response to the OPEN...
    //===================================================================

    recvBlockSize = NetworkApp::BRecvMsg(xactionSocketId,
                                         ::SETTINGS_TRANSACTION_MSG,
                                         &recvBlock, ZERO_ELEMS_BLOCK_SIZE_,
                                         BLOCK_RECV_WAIT_TIME_);

#if defined(SIGMA_DEVELOPMENT)
    if( IS_DEBUG_ON )
    {
        cout << "<<CLIENT>>  Received response:"
        << recvBlock << ", size = " << recvBlockSize << endl;
    }
#endif // defined(SIGMA_DEVELOPMENT)

    if( recvBlockSize != ZERO_ELEMS_BLOCK_SIZE_  ||
        recvBlock.xactionId != ::CONNECTION_COMPLETED )
    {   // $[TI3]
        SettingsXaction::ProcessFailedRecv_(xactionSocketId);
 
       #ifdef SIGMA_DEBUG
           printf(  "Failed recvBlockSize(%d) || xactionId(%d) !!!!\n",recvBlockSize, recvBlock.xactionId  );
       #endif

        return(FALSE);
    }   // $[TI4]

    //===================================================================
    // Step #1:  SEND the first message of the four-way transaction...
    //===================================================================

    // calculate the size of the data block...
    const Uint  SETTINGS_BLOCK_SIZE =
    SettingsXaction::CalcXactionBlockSize_(initXactionBlock.numXmitElems);

#if defined(SIGMA_DEVELOPMENT)
    if( IS_DEBUG_ON )
    {
        cout << "<<CLIENT>>  Transmitting settings:"
        << initXactionBlock << ", size = " << SETTINGS_BLOCK_SIZE
        << endl;
    }
#endif // defined(SIGMA_DEVELOPMENT)

    xmitSize = NetworkApp::BXmitMsg(xactionSocketId,
                                    ::SETTINGS_TRANSACTION_MSG,
                                    (void*)&initXactionBlock,
                                    SETTINGS_BLOCK_SIZE);

    if( xmitSize != SETTINGS_BLOCK_SIZE )
    {   // $[TI5]
        SettingsXaction::ProcessFailedXmit_(xactionSocketId);
 
        #ifdef SIGMA_DEBUG
           printf(  "Failed xmitSize(%d) !!!!\n",xmitSize  );
        #endif

        return(FALSE);
    }   // $[TI6]

    //===================================================================
    // Step #2:  RECEIVE the first response from the BD...
    //===================================================================

    recvBlockSize = NetworkApp::BRecvMsg(xactionSocketId,
                                         ::SETTINGS_TRANSACTION_MSG,
                                         &recvBlock, SETTINGS_BLOCK_SIZE,
                                         BLOCK_RECV_WAIT_TIME_);

#if defined(SIGMA_DEVELOPMENT)
    if( IS_DEBUG_ON )
    {
        cout << "<<CLIENT>>  Received response:"
        << recvBlock << ", size = " << recvBlockSize << endl;
    }
#endif // defined(SIGMA_DEVELOPMENT)

    // get the size (in bytes) of the setting data...
    const Uint  SETTING_VALUES_SIZE = (SETTINGS_BLOCK_SIZE - 
                                       ZERO_ELEMS_BLOCK_SIZE_);

    if( recvBlockSize != SETTINGS_BLOCK_SIZE                ||
        recvBlock.xactionId != ::VALIDATE_XMITTED_SETTINGS  ||
        ::memcmp(initXactionBlock.arrXmitElems,
                 recvBlock.arrXmitElems, SETTING_VALUES_SIZE) != 0 )
    {   // $[TI7]
        SettingsXaction::ProcessFailedRecv_(xactionSocketId);

        #ifdef SIGMA_DEBUG
            printf(  "Failed Step2 recvBlockSize(%d) || xactionId(%d) !!!!\n",recvBlockSize, recvBlock.xactionId  );
        #endif

        return(FALSE);
    }   // $[TI8]

    //===================================================================
    // Step #3:  SEND the third message of the four-way transaction...
    //===================================================================

    SettingXactionBlock  sendBlock;

    sendBlock.xactionId    = ::ACCEPT_XMITTED_SETTINGS;
    sendBlock.numXmitElems = 0u;

#if defined(SIGMA_DEVELOPMENT)
    if( IS_DEBUG_ON )
    {
        cout << "<<CLIENT>>  Settings verified:"
        << sendBlock << ", size = " << ZERO_ELEMS_BLOCK_SIZE_ << endl;
    }
#endif // defined(SIGMA_DEVELOPMENT)

    xmitSize = NetworkApp::BXmitMsg(xactionSocketId,
                                    ::SETTINGS_TRANSACTION_MSG, &sendBlock,
                                    ZERO_ELEMS_BLOCK_SIZE_);

    if( xmitSize != ZERO_ELEMS_BLOCK_SIZE_ )
    {   // $[TI9]
        SettingsXaction::ProcessFailedXmit_(xactionSocketId);
 
        #ifdef SIGMA_DEBUG
           printf(  "Failed Step 3 xmitSize(%d) !!!!\n",xmitSize  );
        #endif

        return(FALSE);
    }   // $[TI10]

    //===================================================================
    // Step #4:  RECEIVE the final response from the BD...
    //===================================================================

    recvBlockSize = NetworkApp::BRecvMsg(xactionSocketId,
                                         ::SETTINGS_TRANSACTION_MSG,
                                         &recvBlock, ZERO_ELEMS_BLOCK_SIZE_,
                                         BLOCK_RECV_WAIT_TIME_);

#if defined(SIGMA_DEVELOPMENT)
    if( IS_DEBUG_ON )
    {
        cout << "<<CLIENT>>  Received response:"
        << recvBlock << ", size = " << recvBlockSize << endl;
    }
#endif // defined(SIGMA_DEVELOPMENT)

    if( recvBlock.xactionId != ::SETTINGS_XACTION_COMPLETE )
    {   // $[TI11]
        SettingsXaction::ProcessFailedRecv_(xactionSocketId);
  
        #ifdef SIGMA_DEBUG
            printf(  "Failed Step 4 xactionId(%d) !!!!\n",recvBlock.xactionId  );
        #endif

        return(FALSE);
    }   // $[TI12]

    //===================================================================
    // CLOSE Settings Transaction Socket...
    //===================================================================

#if defined(SIGMA_DEVELOPMENT)
    if( IS_DEBUG_ON )
    {
        cout << "<<CLIENT>>  Closing connection!" << endl;
    }
#endif // defined(SIGMA_DEVELOPMENT)

    NetworkApp::BCloseSocket(xactionSocketId);

#elif defined(SIGMA_COMMON)

    ::PInitXactionBlock_ = &initXactionBlock;
    SettingsXaction::BeginServerXaction_(-1);

#endif // defined(SIGMA_GUI_CPU)

    SettingsXaction::ReportXactionStatus_(SettingsXaction::XACTION_SUCCESSFUL);

    return(TRUE);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ReportXactionStatus_(xactionStatusId)
//
//@ Interface-Description
//  This method is responsible for reporting any transaction failure, and
//  any successful transaction following a failure.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
SettingsXaction::ReportXactionStatus_(
                                     const SettingsXaction::XactionStatusId_ xactionStatusId
                                     )
{
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ProcessFailedRecv_(xactionSocketId)
//
//@ Interface-Description
//  This method processes the failure of an attempted receival, during
//  the Settings' Transaction.  This handles the logging of the failure, and
//  the "cleanup" for the next attempt.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
SettingsXaction::ProcessFailedRecv_(const SOCKET xactionSocketId)
{
    CALL_TRACE("ProcessFailedRecv_(xactionSocketId)");
    CLASS_ASSERTION((xactionSocketId != INVALID_SOCKET));

    SettingXactionBlock  nackSendBlock;

    nackSendBlock.xactionId    = ::NACK_OR_ERROR;
    nackSendBlock.numXmitElems = 0u;

#if defined(SIGMA_UNIT_TEST)
    if( FALSE )
    {  // when I Unit Test this class, I don't want to do this transmission...
#endif // defined(SIGMA_UNIT_TEST)

        NetworkApp::BXmitMsg(xactionSocketId, ::SETTINGS_TRANSACTION_MSG,
                             &nackSendBlock, ZERO_ELEMS_BLOCK_SIZE_);

#if defined(SIGMA_UNIT_TEST)
    }
#endif // defined(SIGMA_UNIT_TEST)

    NetworkApp::BCloseSocket(xactionSocketId);

#if defined(SIGMA_GUI_CPU)
    SettingsXaction::ReportXactionStatus_(SettingsXaction::RECV_FAILURE);
#endif // defined(SIGMA_GUI_CPU)

#if defined(SIGMA_DEVELOPMENT)
    if( Settings_Validation::IsDebugOn(::SETTINGS_XACTION) )
    {
        cout << "RECEIVE FAILURE!" << endl;
    }
#endif // defined(SIGMA_DEVELOPMENT)
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  CalcXactionBlockSize_(numXmitElems)  [static]
//
//@ Interface-Description
//  Using the number of transmitted elements -- given by 'numXmitElems --
//  calculate, and return, the total transaction block size.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Use the fact that the offset (in bytes) from 'NULL' of the element
//  identified by 'numXmitElems' is equivalent to the total size of the
//  transaction block.
//---------------------------------------------------------------------
//@ PreCondition
//  (numXmitElems <= ::NUM_BD_SETTING_IDS)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

inline Uint
SettingsXaction::CalcXactionBlockSize_(const Uint numXmitElems)
{
  CALL_TRACE("CalcXactionBlockSize_(numXmitElems)");
  SAFE_CLASS_PRE_CONDITION((numXmitElems <= ::NUM_BD_SETTING_IDS));
  return((Uint)&(((SettingXactionBlock*)NULL)->arrXmitElems[numXmitElems]));
}   // $[TI1]

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ProcessFailedXmit_(xactionSocketId)
//
//@ Interface-Description
//  This method processes the failure of an attempted transmission during
//  the Settings' Transaction.  This handles the logging of the failure, and
//  the "cleanup" for the next attempt.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
SettingsXaction::ProcessFailedXmit_(const SOCKET xactionSocketId)
{
    CALL_TRACE("ProcessFailedXmit_(xactionSocketId)");

    NetworkApp::BCloseSocket(xactionSocketId);

#if defined(SIGMA_GUI_CPU)
    SettingsXaction::ReportXactionStatus_(SettingsXaction::XMIT_FAILURE);
#endif // defined(SIGMA_GUI_CPU)

#if defined(SIGMA_DEVELOPMENT)
    if( Settings_Validation::IsDebugOn(::SETTINGS_XACTION) )
    {
        cout << "TRANSMIT FAILURE!" << endl;
    }
#endif // defined(SIGMA_DEVELOPMENT)
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SetupQueueMsg_(rQueueMsg, xactionState, xactionInfo)  [static]
//
//@ Interface-Description
//  Initialize the inter-task message given by 'rQueueMsg', with the
//  information given by 'xactionState' and 'xactionInfo'.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Divide the 'msgData' area of an InterTaskMessage instance between
//  'xactionState' and 'xactionInfo'.  This is done by allowing each
//  of these two values to reside in half (12 bits) of the 'msgData'
//  field.  This layout allows for values up to 4095.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  (GetXactionState_(rQueueMsg) == xactionState)
//  (GetXactionInfo_(rQueueMsg) == xactionInfo)
//@ End-Method
//=====================================================================

void
SettingsXaction::SetupQueueMsg_(InterTaskMessage&       rQueueMsg,
			        const SettingsXactionId xactionState,
			        const Uint16            xactionInfo)
{
  CALL_TRACE("SetupQueueMsg_(rQueueMsg, xactionState, xactionInfo)");
  rQueueMsg.msgId   = SettingsXaction::XACTION_INITIATION_MSG;
  rQueueMsg.msgData = ((xactionInfo << 12) | xactionState);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetXactionState_(rQueueMsg)  [static]
//
//@ Interface-Description
//  Return the transaction state ID that is stored in 'rQueueMsg'.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Extract the transaction state value from the lower 12 bits of the
//  inter-task message.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingsXactionId
SettingsXaction::GetXactionState_(const InterTaskMessage& rQueueMsg)
{
  CALL_TRACE("GetXactionState_(rQueueMsg)");
  return((SettingsXactionId)(rQueueMsg.msgData & 0x00000fff));
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetXactionInfo_(rQueueMsg)  [static]
//
//@ Interface-Description
//  Return the transaction information byte that is stored in 'rQueueMsg'.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Extract the transaction info value from the upper 12 bits of the
//  inter-task message.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Uint16
SettingsXaction::GetXactionInfo_(const InterTaskMessage& rQueueMsg)
{
  CALL_TRACE("GetXactionInfo_(rQueueMsg)");
  return((Uint8)((rQueueMsg.msgData >> 12) & 0x00000fff));
}   // $[TI1]



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  Initiate(xactionId)
//
//@ Interface-Description
//  This method is used by the Accepted Context to initiate a Settings'
//  Transaction due to an update of the accepted settings.  This method
//  notifies the Settings Xaction Task to conduct the transaction, passing
//  'xactionId' as an identifier as to the type of acceptance (vent-startup,
//  or not) that occurred.
//---------------------------------------------------------------------
//@ Implementation-Description
//  If this method is called when a previously-initiated transaction hasn't
//  yet completed, the currently requested transaction ID will not be
//  saved in the static data member if a Vent-Startup is requested.  This
//  is because Vent-Startup transmissions have special processing on the
//  BD CPU (see 'PendingContext'), whereas the other transaction types
//  don't.
//
//  This method of this class is run in the GUI Task thread.
//---------------------------------------------------------------------
//@ PreCondition
//  (xactionId == ::USER_UPDATE_XMITTED  ||
//   xactionId == ::ONLINE_SYNC_XMITTED  ||
//   xactionId == ::VENT_STARTUP_UPDATE_XMITTED)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
SettingsXaction::Initiate(const SettingsXactionId xactionId)
{
    CALL_TRACE("Initiate(xactionId)");
    AUX_CLASS_PRE_CONDITION((xactionId == ::USER_UPDATE_XMITTED  ||
                             xactionId == ::ONLINE_SYNC_XMITTED  ||
                             xactionId == ::VENT_STARTUP_UPDATE_XMITTED),
                            xactionId);

#if defined(SIGMA_GUI_CPU)

    InterTaskMessage  beginXactionMsg;

    SettingsXaction::SetupQueueMsg_(beginXactionMsg, ::INITIATE_TRANSACTION,
                                    xactionId);

#if defined(SIGMA_UNIT_TEST)
    if( SettingsXaction::DoInitiateXaction )
    {  // may not post to the queue during Unit Testing...
#endif // defined(SIGMA_UNIT_TEST)

		WPARAM w;
		memcpy( &w, &beginXactionMsg, sizeof(WPARAM) );

        // post the transaction request to the setting transaction queue...
		theApp.PostWorkerThreadMessage(CPB840FakeGuiApp::XactionThread, WM_PUSH_SETTING, w, 0);

#if defined(SIGMA_UNIT_TEST)
        }
#endif // defined(SIGMA_UNIT_TEST)


#elif defined(SIGMA_COMMON)

    SettingsXaction::BeginClientXaction_(xactionId);

#endif // defined(SIGMA_GUI_CPU)
}
