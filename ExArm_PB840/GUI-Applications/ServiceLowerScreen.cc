#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
// Class: ServiceLowerScreen - Contains and displays the
// four areas that constitute the Service Lower screen during 
// Service mode:  Service Lower Subscreen Area, Service Lower Screen Select
// Area, Message Area and Prompt Area.
//---------------------------------------------------------------------
//@ Interface-Description
// Only one instance of this class is created.  No parameters are
// needed to construct.  All areas of the screen which go to make up
// the Lower screen are created here.
//
// There are numerous methods provided which must be called when particular
// events occur.  It is then left up to the ServiceLowerScreen to decide what to do
// for each event.  Specifically, these methods are: beginSst(),
// beginServiceInitialization(), and beginServiceMode().
//
// ServiceLowerScreen is used as a container for the five objects that implement
// most of the Lower screen's functionality, i.e. LowerSubScreenArea,
// ServiceLowerScreenSelectArea, PromptArea and MessageArea.
// There is an inline method provided to access pointers to each instance.
//---------------------------------------------------------------------
//@ Rationale
// The main purpose of this class is to create and display the various
// areas of the Lower screen.  The areas are as follows: LowerSubScreenArea,
// ServiceLowerScreenSelectArea, PromptArea and MessageArea.
//---------------------------------------------------------------------
//@ Implementation-Description
// All Lower screen areas are created automatically and are then added to the
// Lower screen container causing them to be displayed as soon as the Lower
// screen is displayed (by itself being appended to GUI-Foundation's Lower
// basecontainer).
//
// On SST startup, beginSst() method will be called and the SstSetupSubScreen
// will be automatically activated.
// On normal service mode startup, the beginServiceMode() method will be called
// and the lower screen select area with a blanked serviceLowerScreenSelectArea
// will be automatically activated.
// However, at startup, if the system failed any one of the following criteria
// then the beginServiceInitialization() and the appropriated subscreen will be
// activated depending the failing factors.  The list below are the criteria
// we checked for proceeding to beginServiceInitialization():
// IsFlowSensorInfoRequired, IsBdAndGuiFlashTheSame, AreSerialNumberMatches,
// and IsBdVentInopTestInProgress.
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and pre-conditions
// to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
// Only one instance of this class should be created.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/ServiceLowerScreen.ccv   25.0.4.0   19 Nov 2013 14:08:26   pvcs  $
//
//@ Modification-Log
//
//  Revision: 007   By: gdc   Date:  26-May-2007    SCR Number: 6330
//  Project:  Trend
//  Description:
//      Removed SUN prototype code.
//
//  Revision: 006   By: srp   Date:  24-Apr-2002    DR Number: 5327 & 5908
//  Project:  NeoMode
//  Description:
//      Added Modification log. Updated for NeoMode  11/17/99
//
//  Revision: 005  By:  sah	   Date:  11-Jan-1999    DCS Number: 5321
//  Project:  ATC
//  Description:
//		Added 'SIGMA_DEVELOPMENT' conditionals around this class (unused)
//		'SoftFault()' method.
//
//  Revision: 004  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 003 By:  yyy    Date:  25-SEP-97    DR Number: 2522
//    Project:  Sigma (R8027)
//    Description:
//      Pass the transaction successful message to interested subscreen before test starts.
//
//  Revision: 002  By: yyy      Date: 20-May-1997  DR Number: 2107
//    Project:  Sigma (R8027)
//    Description:
//    Description:
//      20-May-1997 Disabled the EST tab buttom being set to flat.
//      01-Aug-1997 Re-enabled the EST tab buttom per changes in 2107
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "ServiceLowerScreen.hh"

//@ Usage-Classes
#include "SettingContextHandle.hh"
#include "GuiEventRegistrar.hh"
#include "MemoryStructs.hh"
#include "SstSetupSubScreen.hh"
#include "ServiceInitializationSubScreen.hh"
#include "SstSetupSubScreen.hh"
#include "PressureXducerCalibrationSubScreen.hh"
#include "LowerSubScreenArea.hh"
#include "PromptStrs.hh"

//@ End-Usage

//@ Code...

// Initialize static constants.
static const Int32 CONTAINER_X_ = 0;
static const Int32 CONTAINER_Y_ = 0;
static const Int32 CONTAINER_WIDTH_ = 640;
static const Int32 CONTAINER_HEIGHT_ = 480;

ServiceLowerScreen& ServiceLowerScreen::RServiceLowerScreen = *((ServiceLowerScreen *)
		(((ScreenMemoryUnion *)::ScreenMemory)->serviceModeMemory.lowerScreen));

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ServiceLowerScreen()  [Default Constructor]
//
//@ Interface-Description
// Creates the ServiceLowerScreen and all its contents.  No construction
// parameters needed.
//---------------------------------------------------------------------
//@ Implementation-Description
// All areas of the screen are created automatically.  The Lower
// screen is colored white and all areas are added to the Lower
// screen's container list.  We register for callbacks from the
// Restart button.
// $[01262] The lower screen shall provide the following ...
// $[07003] The lower screen shall provide the following ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

ServiceLowerScreen::ServiceLowerScreen(void) :
                currentScreen_(SERVICE_NO_SCREEN)
{
	CALL_TRACE("ServiceLowerScreen::ServiceLowerScreen(void)");

#if defined FORNOW
printf("ServiceLowerScreen::ServiceLowerScreen(void)\n");
#endif	// FORNOW


	// Size and position the area
	setX(CONTAINER_X_);
	setY(CONTAINER_Y_);
	setWidth(CONTAINER_WIDTH_);
	setHeight(CONTAINER_HEIGHT_);

	// Default background color is White
	setFillColor(Colors::FRAME_COLOR);

	// Add the various areas to the screen, $[01001]
	addDrawable(&serviceLowerSubScreenArea_);
	addDrawable(&serviceLowerScreenSelectArea_);
	addDrawable(&promptArea_);
	addDrawable(&messageArea_);

	// Initialize Service Lower Screen Select Area
	serviceLowerScreenSelectArea_.initialize();

	// Register for GUI state change callbacks.
	GuiEventRegistrar::RegisterTarget(this);

	// Set the default prompt
	promptArea_.setDefaultPrompt(PromptStrs::TOUCH_A_BUTTON_D);
                                                       //  $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~ServiceLowerScreen()  [Destructor]
//
//@ Interface-Description
// The destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

ServiceLowerScreen::~ServiceLowerScreen(void)
{
	CALL_TRACE("ServiceLowerScreen::~ServiceLowerScreen(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: beginSst
//
//@ Interface-Description
// Sets up the Lower screen for SST mode.
//---------------------------------------------------------------------
//@ Implementation-Description
// We begin SST with blanking the Lower Screen Select Area, and activating
// the SstSetupSubScreen.
// $[01263] The lower screen select area shall remain blank ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ServiceLowerScreen::beginSst(void)
{
	CALL_TRACE("ServiceLowerScreen::beginSst(void)");

#if defined FORNOW
printf("ServiceLowerScreen::beginSst(void)\n");
#endif	// FORNOW

	// Hide the drawables within the lower screen select area.
	serviceLowerScreenSelectArea_.setBlank(TRUE);

	// Activate the SST Start subscreen
	serviceLowerSubScreenArea_.activateSubScreen(
			LowerSubScreenArea::GetSstSetupSubScreen(), NULL);

	// Set the current active mode.
	currentScreen_ = BEGIN_SST_MODE;

												//   $[TI1]
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: beginServiceInitialization
//
//@ Interface-Description
// Sets up the Lower screen for service mode when it did not pass the
// service initialization criteria.
//---------------------------------------------------------------------
//@ Implementation-Description
// We begin service mode with activating the ServiceInitializationSubScreen().
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ServiceLowerScreen::beginServiceInitialization(void)
{
	CALL_TRACE("ServiceLowerScreen::beginServiceInitialization(void)");

#if defined FORNOW
printf("ServiceLowerScreen::beginServiceInitialization(void)\n");
#endif	// FORNOW

	// Activate the ServiceInitialization Start subscreen
	serviceLowerSubScreenArea_.activateSubScreen(
			LowerSubScreenArea::GetServiceInitializationSubScreen(), NULL);

												//   $[TI1]
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: beginServiceMode
//
//@ Interface-Description
// Sets up the Lower screen for service mode when it did pass the
// service initialization criteria.
//---------------------------------------------------------------------
//@ Implementation-Description
// We begin service mode with blanking the Lower Screen Select Area, and
// deactivating any possible subScreens.
//---------------------------------------------------------------------
//@ Implementation-Description
// We begin service mode with the an empty lower subscreen.  So we first
// blank the Lower Screen Select Area, then set the lower screen to 
// blank.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ServiceLowerScreen::beginServiceMode(void)
{
	CALL_TRACE("ServiceLowerScreen::beginServiceMode(void)");

#if defined FORNOW
printf("ServiceLowerScreen::beginServiceMode(void)\n");
#endif	// FORNOW

	// Show the drawables within the lower screen select area.
	serviceLowerScreenSelectArea_.setBlank(FALSE);

	// Deactivate any subscreen if exists.
	serviceLowerSubScreenArea_.deactivateSubScreen();

	// Set the current active mode.
	currentScreen_ = BEGIN_SERVICE_MODE;

                                                //    $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: guiEventHappened	[virtual] 
//
//@ Interface-Description
// Called when a change in the GUI App state happens.  Passed the id of the
// state which changed. 
//---------------------------------------------------------------------
//@ Implementation-Description
//  If new state is SERVICE_READY, after we set the EST tab button to
//  the proper state (up or flat), we check the initial Service
//  state.  If all initial states are not OK, we invoke a method
//  to activate the ServiceInitialization Start subscreen.
//  Otherwise, we invoke the class's beginServiceMode() method to
//  display the ServiceLowerScreen.  If state is SST, we invoke
//  beginSST() method to setup and display the SST subscreen.
//  Any other states are ignored.
//  If we recieve COMMUNICATION DOWN event, then we will deactivate all
//  screens and buttos and display the appropriated prompts.
// $[07036] The EST select button shall not be displayed if exhalation ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

#ifdef SIGMA_GUIPC_CPU
extern bool g_bSvcMode;//TODO E600 TT
#endif

void
ServiceLowerScreen::guiEventHappened(GuiApp::GuiAppEvent eventId)
{
	CALL_TRACE("ServiceLowerScreen::guiEventHappened(GuiApp::GuiAppEvent eventId)");

#if defined FORNOW
printf("ServiceLowerScreen::guiEventHappened(void), eventId=%d\n", eventId);
#endif
	TabButton *estTabButton;

	estTabButton = serviceLowerScreenSelectArea_.getEstTestTabButton();

	switch (eventId)
	{
	case GuiApp::SERVICE_READY:
		// Make sure Lower Screen Select Area is visible
		serviceLowerScreenSelectArea_.setBlank(FALSE);

		// Set prompts to nothing
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY,
					PromptArea::PA_LOW, NULL_STRING_ID);
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_ADVISORY,
					PromptArea::PA_LOW, NULL_STRING_ID);
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
							PromptArea::PA_LOW, NULL_STRING_ID);

		// Set the EST tab button to appropriated state based on the
		// exhalation calibration result.
		if (GuiApp::IsExhValveCalRequired())
		{											 // $[TI1]
			estTabButton->setToFlat();
		}
		else
		{											 // $[TI2]
			estTabButton->setToUp();
		}

#if defined SERVICE_FORNOW
		if (!GuiApp::IsServiceInitialStateOK())
		{
			beginServiceInitialization();
		}
		else
		{
			beginServiceMode();
		}
		
#elif defined SST_FORNOW
		beginSst();
#else
		if (GuiApp::GetGuiState() == STATE_SERVICE)
		{
			if (!GuiApp::IsServiceInitialStateOK() 			
#ifdef SIGMA_GUIPC_CPU
				&& !g_bSvcMode	//TODO E600 TT
#endif
				)
			{										 // $[TI3]
				beginServiceInitialization();
			}
			else
			{										 // $[TI4]
				beginServiceMode();
			}
		}
		else if (GuiApp::GetGuiState() == STATE_SST)
		{											 // $[TI5]
			beginSst();
		}
		else
		{											 // $[TI6]
			SAFE_CLASS_ASSERTION(FALSE);
		}
#endif
		break;

	case GuiApp::INOP:
	case GuiApp::COMMUNICATIONS_DOWN:
		// Deactivate any subscreen if exists.
		serviceLowerSubScreenArea_.deactivateSubScreen();

 		if (GuiApp::GetGuiState() == STATE_SERVICE)
 		{											// $[TI7]
			// Make sure Lower Screen Select Area is blank
			serviceLowerScreenSelectArea_.setBlank(TRUE);
	 	}
 		else if (GuiApp::GetGuiState() == STATE_SST)
	 	{											// $[TI8]
	 		;
	 	}
 		else
	 	{
			SAFE_CLASS_ASSERTION(FALSE);
	 	}

		// Set Primary prompt to nothing
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY,
					PromptArea::PA_HIGH, NULL_STRING_ID);
		// Set Primary prompt to nothing
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY,
					PromptArea::PA_LOW, NULL_STRING_ID);
		// Set Advisory prompt to "Settings have been locked out."
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_ADVISORY,
					PromptArea::PA_LOW, PromptStrs::SETTINGS_LOCKED_OUT_A);
		// Set Secondary prompt to nothing
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
					PromptArea::PA_LOW, NULL_STRING_ID);
		break;

	case GuiApp::SETTINGS_TRANSACTION_SUCCESS:
		if (serviceLowerSubScreenArea_.isCurrentSubScreen(
							LowerSubScreenArea::GetSstSetupSubScreen()))
		{											// $[TI9.1]
			LowerSubScreenArea::GetSstSetupSubScreen()
										->executeSettingTransactionHappened();
		}
		else if (serviceLowerSubScreenArea_.isCurrentSubScreen(
							LowerSubScreenArea::GetPressureXducerCalibrationSubScreen()))
		{											// $[TI9.2]
			LowerSubScreenArea::GetPressureXducerCalibrationSubScreen()
										->executeSettingTransactionHappened();
		}											// $[TI9.3]
		break;

	default:
		// Ignore any other BD messages.			 
		break;
	}
}




#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
ServiceLowerScreen::SoftFault(const SoftFaultID  softFaultID,
		   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, SERVICELOWERSCREEN,
									lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)

