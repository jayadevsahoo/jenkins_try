#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//============== C L A S S   D E S C R I P T I O N ====================
//@ Class:  Fio2EnabledSetting - FIO2 Sensor Enabled Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, discrete setting that indicates the whether the optional
//  FIO2 Sensor is enabled.  This class inherits from 'BatchDiscreteSetting'
//  and provides the specific information needed for the representation of
//  the FIO2's activation.  This information includes the set of values
//  that this setting can have (see 'Fio2EnabledValue.hh'), and this batch
//  setting's new-patient value.
//
//  
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class has no data members, and no dependent settings.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/Fio2EnabledSetting.ccv   25.0.4.0   19 Nov 2013 14:27:22   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006   By: gdc    Date: 16-Aug-2010     SCR Number: 6663
//  Project: API/MAT
//  Description:
//  Added support for VentSet API.
// 
//  Revision: 005 By: srp    Date: 28-May-2002   DR Number: 5898
//  Project:  VCP
//	Description:
//		Incorporated Remediation code review issue(s) -- commentary changes only.
//
//  Revision: 005   By: sah  Date:  03-Jan-00    DR Number:  5327
//  Project:  NeoMode
//  Description:
//  Updated for NeoMode
//
//  Revision: 004   By: sah  Date:  11-Jan-1999    DR Number:  5321
//  Project:  ATC
//  Description:
//	As part of making all 'SoftFault()' methods non-inlined, I'm
//	also making those 'SoftFault()' methods that are NOT called
//	in PRODUCTION mode, DEVELOPMENT-only.
//
//  Revision: 003   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'getApplicability()' method
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Initial version.
//
//=====================================================================

#include "Fio2EnabledSetting.hh"
#include "Fio2EnabledValue.hh"

//@ Usage-Classes
#include "ContextMgr.hh"
#include "AcceptedContext.hh"
//@ End-Usage

//@ Code...
static const DiscreteParameterItem ParameterLookup_[] = 
{
	{"ENA", Fio2EnabledValue::YES_FIO2_ENABLED},
	{"DIS", Fio2EnabledValue::NO_FIO2_ENABLED},
	{"CAL", Fio2EnabledValue::CALIBRATE_FIO2},
	{NULL , NULL}
};

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  Fio2EnabledSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02274] -- setting range
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Fio2EnabledSetting::Fio2EnabledSetting(void)
	: BatchDiscreteSetting(SettingId::FIO2_ENABLED,
			       Setting::NULL_DEPENDENT_ARRAY_,
			       Fio2EnabledValue::TOTAL_FIO2_ENABLED_VALUES)
{
  CALL_TRACE("Fio2EnabledSetting()");
	setLookupTable(ParameterLookup_);
}  // $[TI1]


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method:  ~Fio2EnabledSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Fio2EnabledSetting::~Fio2EnabledSetting(void)
{
  CALL_TRACE("~Fio2EnabledSetting(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  FiO2-enabled is ALWAYS changeable.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
Fio2EnabledSetting::getApplicability(const Notification::ChangeQualifier) const
{
  CALL_TRACE("getApplicability(qualifierId)");

  return(Applicability::CHANGEABLE);
}   // $[TI1]


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [const, virtual]
//
//@ Interface-Description
//  Get the setting's new-patient value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02275] The setting's new-patient value ...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
Fio2EnabledSetting::getNewPatientValue(void) const
{
  CALL_TRACE("getNewPatientValue()");

  DiscreteValue  newPatientValue;

  // $[02275] -- new-patient value...
  newPatientValue = Fio2EnabledValue::YES_FIO2_ENABLED;

  return(newPatientValue);
}  // $[TI1]


#if defined(SIGMA_DEVELOPMENT)

//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//			[static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
Fio2EnabledSetting::SoftFault(const SoftFaultID  softFaultID,
			      const Uint32       lineNumber,
			      const char*        pFileName,
			      const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, ::SETTINGS_VALIDATION,
                          FIO2_ENABLED_SETTING, lineNumber, pFileName,
                          pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)
