
#ifndef PatientDataClassIds_HH
#define PatientDataClassIds_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Filename:  PatientDataClassIds - Ids of all of the Patient-Data Classes.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Patient-Data/vcssrc/PatientDataClassIds.hhv   25.0.4.0   19 Nov 2013 14:17:58   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003   By: syw   Date:  01-Jun-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      * hard-coded class ids, as per coding guidelines
//
//  Revision: 002  By:  sah    Date:  01-27-94    DR Number:
//	Project:  BiLevel (R8027)
//	Description:
//		Initial version for Bilevel
//
//  Revision: 001  By:  sah    Date:  06-16-94    DR Number:
//	Project:  Sigma (R8027)
//	Description:
//		Initial version 
//  
//====================================================================


//@ Type:  PatientDataClassId
// Ids for all of Patient Data Subsystem classes.
enum PatientDataClassId
{
  PATIENT_DATA_MGR		= 0,
  BREATH_DATUM_HANDLE		= 1,
  BREATH_ITEM_DATA_INFO		= 2,
  BOUNDED_DATA_INFO		= 3,
  DISCRETE_DATA_INFO		= 4,
  TIMED_BOUNDED_DATA_INFO	= 5,
  PEEP_DATA_INFO		= 6,
  INSP_PAUSE_DATA_INFO		= 7,
  WAVEFORM_DATA_INFO		= 8,
  PATIENT_DATA_TIMER_REGISTRAR	= 9,
  PATIENT_DATA_TIMER_TARGET	= 10,
  RATIO_BOUNDED_DATA_INFO	= 11,
  VARIABLE_BOUNDED_INFO		= 12,

  NUM_PATIENT_DATA_CLASSES
};


#endif // PatientDataClassIds_HH 
