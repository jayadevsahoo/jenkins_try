#ifndef _ERROR_CODES_H
#define _ERROR_CODES_H

#define SUCCESS                 0
#define FAILURE                 1
#define NO_BYTES_RECEIVED       2
#define BAD_PACKET_FORMAT       3
#define NO_COM_PORT             4
#define TEST_STRING_FAILED      5

#endif // #ifndef _ERROR_CODES_H
