
#ifndef Subject_HH
#define Subject_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  Subject -  Subject Class.
//---------------------------------------------------------------------
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/Subject.hhv   25.0.4.0   19 Nov 2013 14:27:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002   By: rhj   Date:  31-May-2007    SCR Number: 6237 
//  Project:  Trend
//  Description:
//     Added Trend Time Frame setting
//
//  Revision: 001   By: sah   Date:  24-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//     Created to work with new applicability functionality.
//
//====================================================================

#include "Sigma.hh"
#include "Notification.hh"
#include "SettingClassId.hh"

//@ Usage-Classes
//@ End-Usage


class Subject
{
  public:
    virtual ~Subject(void);

    static void  DetachFromAllObservers(void);

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL, 
			   const char*       pPredicate = NULL);

  protected:
    Subject(void);

    virtual void  detachFromAllObservers_(void) = 0;

  private:
    Subject(const Subject&);     	// not implemented...
    void  operator=(const Subject&);    // not implemented...

    //@ Constant:  MAX_INSTANCES_
    // Constant used for defining the size of the array of
    // instance pointers.
    enum { MAX_INSTANCES_ = 120 };

    //@ Data-Member:  ArrInstances_
    // Static array of subject pointers, one for each subject instance.
    static Subject*  ArrInstances_[MAX_INSTANCES_];
};


#endif // Subject_HH 
