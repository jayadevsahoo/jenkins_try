#ifndef DisplayContext_HH
#define DisplayContext_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: DisplayContext -- Clipping for VGA library.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/VGA-Graphics-Driver/vcssrc/DisplayContext.hhv   25.0.4.0   19 Nov 2013 14:37:28   pvcs  $
//
//@ Modification-Log
//
//  Revision: 007   By: rpr     Date: 11-Mar-2011     SCR Number: 6752
//  Project: XENA2
//      ExpandFont and ExpandIcon passes the original witdth of the font/bitmap
//		so clipping can be accounted for properly.
// 
//  Revision: 006   By: mnr    Date: 24-Sep-2010    SCR Number: 6672
//  Project: XENA2
//  Description:
//      New method expandIcon() added.
//
//  Revision: 005   By: gdc    Date: 01-Feb-2007     SCR Number: 6237
//  Project: Trend
//  Description:
//  Trend project related changes. Moved the no-operation methods from
//  NullDisplayContext to the base class DisplayContext so
//  NullDisplayContext won't have to be modified every time a method 
//  is added to DisplayContext. Added off-screen image storage and
//  recovery to implement the trend cursor overlay.
//
//  Revision: 004   By: erm    Date: 12-Oct-2001     DCS Number: 5959
//  Project: GUIComms
//  Description:
//  Code clean-up
//
//  Revision: 003   By: gdc    Date:  28-Aug-2000    DCS Number: 5493
//  Project: GUIComms
//  Description:
//	Implemented single screen compatibility.
//
//  Revision: 002    By: hct   Date: 28-MAR-2000     DCS Number: 5493
//  Project: GuiComms
//  Description:
//  Add GuiComms functionality.
//
//  Revision: 001   By: gdc    Date:  27-Jan-2000    DCS Number: 5337
//  Project: 840 Neonatal
//  Description:
//	Optimized VGA library for Neonatal project to use BitBlt and Windows
//  Bitmap structures.
//
//====================================================================
#include "Sigma.hh"
#include "VgaGraphicsDriver.hh"

//@ Usage-Classes
#include "RgbQuad.hh"
//@ End-Usage

class GraphicsContext;
struct DibInfo;

class DisplayContext 
{
  public:
	enum Whence
	{
	  WHENCE_SEEK_SET = 0,
	  WHENCE_SEEK_CUR = 1,
	  WHENCE_SEEK_END = 2
	};

	DisplayContext( Uint32 width, Uint32 height, Uint32 bitsPerPixel );

	virtual ~DisplayContext();

	virtual void lock(void);
	virtual void unlock(void);
	virtual void accessDisplay(void) const;
	
	virtual void fillSolid(
					Int xDest, Int yDest, Int xWidth, Int yHeight, Int color);

	virtual void expandFont(Int xDest, Int yDest, Int xWidth, Int yHeight, 
					const Byte * pFontData, Uint fontOffset, Int color, Int orignalWidth);

	virtual void expandIcon(Int xDest, Int yDest, Int xWidth, Int yHeight, 
					const Byte * pFontData, Uint fontOffset, Int color, Int orignalWidth);

	virtual void moveBlock(Int xSrc, Int ySrc, Int xDest, 
					Int yDest, Int xWidth, Int yHeight);

	virtual void *allocateImage(Uint size);

	virtual void freeImage(void *pBlock);

	virtual void storeImage(void *pDest, Int xSrc, Int ySrc, Int xWidth, Int yHeight);

	virtual void recoverImage(Int xDest, Int yDest, const void *pSrc, Int xWidth, Int yHeight);

	virtual void allocateColor(Byte & index, const RgbQuad & rgb );

	virtual void storeColor(Byte index, const RgbQuad & rgb, 
					Boolean readOnly=TRUE);

	virtual void getColor(Byte index, RgbQuad & rgb);

	virtual void showFrame(void);

    virtual void  setClip(
					GraphicsContext & rGc, Int x0, Int y0, Int x1, Int y1);

    virtual void  drawPoint(
					GraphicsContext & rGc, Int x, Int y);

    virtual void  drawLine(
					GraphicsContext & rGc,
					Int x0, Int y0, Int x1, Int y1);

    virtual void  fillRectangle(
					GraphicsContext & rGc,
					Int x0, Int y0, Int x1, Int y1 );

    virtual void  drawTriangle(
					GraphicsContext & rGc,
					Int x0, Int y0,
					Int x1, Int y1,
					Int x2, Int y2 );

    virtual void  fillTriangle(
					GraphicsContext & rGc,
					Int x0, Int y0,
					Int x1, Int y1,
					Int x2, Int y2 );

    virtual void  drawBitmap(
					GraphicsContext & rGc,
					DibInfo & rDibInfo,
			        Int xPos, Int yPos );
 
    virtual void  drawText(
					GraphicsContext & rGc,
					Int xPos, Int yPos, Uint fontId, const wchar_t* pString);

    virtual void  drawJapaneseText(
					GraphicsContext & rGc,
					Int xPos, Int yPos, Uint fontId, const char* pString);

	inline Uint	 getWidth(void) const;
	inline Uint	 getHeight(void) const;
	inline Uint	 getBitsPerPixel(void) const;
	inline Byte* getDisplayFramePtr(void) const;
	inline Byte* getDisplayMemoryPtr(void) const;
	inline Uint  getMemorySize(void) const;

	inline Boolean	isLockPending(void) const;
	inline void	    setLockPending(Boolean lockPending);

	//Sets the color for use while rendering text.
	//This class already uses multiple conventions to specify 
	//color. I am just using the more flexible option for this new
	//API
	virtual void setTextColor(RgbQuad& color) {}
	virtual CDC* getCDC(void) const;
	virtual void setCDC(CDC* pCDC);

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL, 
			   const char*       pPredicate = NULL);

  protected:

    //@ Data-Member:  pDisplayMemory_
    // contains the pointer to display memory
    Byte * pDisplayMemory_;

    //@ Data-Member:  pDisplayFrame_
    // contains the pointer to display frame memory
    Byte * pDisplayFrame_;

    //@ Data-Member:  memorySize_
    // contains the display memory size in bytes
    Uint memorySize_;

    //@ Data-Member:  width_
    // contains the display width in pixels
    Uint width_;

    //@ Data-Member:  height_
    // contains the display height in pixels
    Uint height_;

    //@ Data-Member:  bitsPerPixel_
    // contains the number of bits per pixel
    Uint bitsPerPixel_;

    //@ Data-Member:  lockPending_
    // TRUE if a task is waiting for this display context lock
    Boolean lockPending_;
	
	//@ Data-Member:  colormapCache_
	//  Cached RGB values stored in the VGA palette
	RgbQuad  colormapCache_[256];



  private:
	DisplayContext(); // declared only
    DisplayContext(const DisplayContext&); // declared only
	void operator=(const DisplayContext&); // declared only
};

#include "DisplayContext.in"

#endif // DisplayContext_HH 
