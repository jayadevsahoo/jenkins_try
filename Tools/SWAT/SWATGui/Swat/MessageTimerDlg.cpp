// MessageTimerDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Swat.h"
#include "MessageTimerDlg.h"


// CMessageTimerDlg dialog

IMPLEMENT_DYNAMIC(CMessageTimerDlg, CDialog)

CMessageTimerDlg::CMessageTimerDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CMessageTimerDlg::IDD, pParent)
	, m_nSeconds(30)
	, m_strMessage(_T(""))
	, m_strActual(_T(""))
	, m_strParam(_T(""))
{
	m_nTestResult = 0;
	m_strActual = "Type actual here.";
}

CMessageTimerDlg::~CMessageTimerDlg()
{
}

void CMessageTimerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_SECONDS, m_nSeconds);
	DDX_Text(pDX, IDC_STATIC_MESSAGE, m_strMessage);
	DDX_Control(pDX, IDC_EDIT_ACTUAL, m_EditBoxCtrl);
	DDX_Text(pDX, IDC_EDIT_ACTUAL, m_strActual);
	DDX_Text(pDX, IDC_STATIC_TITLE, m_strParam);
}


BEGIN_MESSAGE_MAP(CMessageTimerDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CMessageTimerDlg::OnBnClickedOk)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON_PASS, &CMessageTimerDlg::OnBnClickedButtonPass)
	ON_BN_CLICKED(IDC_BUTTON_FAIL, &CMessageTimerDlg::OnBnClickedButtonFail)
	ON_BN_CLICKED(IDC_BUTTON_CLOSE, &CMessageTimerDlg::OnBnClickedButtonClose)
	ON_BN_CLICKED(IDC_BUTTON_STOP_SCRIPT, &CMessageTimerDlg::OnBnClickedButtonStopScript)
END_MESSAGE_MAP()


// CMessageTimerDlg message handlers

void CMessageTimerDlg::OnBnClickedOk()
{
	// TODO: Add your control notification handler code here
	OnOK();
}

void CMessageTimerDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: Add your message handler code here and/or call default

	m_nSeconds = m_nSeconds-1;
	if(m_nSeconds == 0)
	{
		KillTimer(1234);
		GetDlgItem(IDC_BUTTON_PASS)->EnableWindow(FALSE);
		GetDlgItem(IDC_BUTTON_FAIL)->EnableWindow(FALSE);
		GetDlgItem(IDC_BUTTON_CLOSE)->EnableWindow(FALSE);
		Sleep(1500);
		m_nTestResult = 2;//fail
		m_strActual = "NO USER INPUT RCVD.";
		OnOK();
		return;
	}
	char ch[50];
	_itoa(m_nSeconds, ch, 10);
	GetDlgItem(IDC_EDIT_SECONDS)->SetWindowTextA(ch);

	CDialog::OnTimer(nIDEvent);
}

BOOL CMessageTimerDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_EditBoxCtrl.ShowWindow(FALSE);

	SetTimer(1234,1000,NULL);


	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CMessageTimerDlg::OnBnClickedButtonPass()
{
	m_EditBoxCtrl.ShowWindow(FALSE);
	GetDlgItem(IDC_STATIC_MESSAGE)->EnableWindow(TRUE);
	m_nTestResult = 1;
	m_strActual = m_strMessage;
	Sleep(500);
	OnOK();
}

void CMessageTimerDlg::OnBnClickedButtonFail()
{
	KillTimer(1234);
	

	m_EditBoxCtrl.ShowWindow(TRUE);
	m_nTestResult = 2;

	GetDlgItem(IDC_BUTTON_FAIL)->EnableWindow(FALSE);
	GetDlgItem(IDC_BUTTON_CLOSE)->EnableWindow(TRUE);

//	OnOK();
}

void CMessageTimerDlg::OnBnClickedButtonClose()
{
	UpdateData(true);
	OnOK();
}

void CMessageTimerDlg::OnBnClickedButtonStopScript()
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	pApp->LogStatus("-->Stop triggered by the user. Please wait...");
	pApp->m_bRun = false; //stop running script
	pApp->m_bRecord = false; //stop recording

	UpdateData(true);
	OnOK();
}
