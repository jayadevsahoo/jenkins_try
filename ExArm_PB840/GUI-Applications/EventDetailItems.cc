#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: EventDetailItems -   This class contains the event's 
//                              description of each menu item.
//---------------------------------------------------------------------
//@ Interface-Description
//  The updateEventDetailTable() method updates the event detail table
//  with the up-to-date event detail strings and ids given by the parameter.
//  The getMenuEntryColumn() method provides the event description as
//  a string based on the given entry number and column number.
//---------------------------------------------------------------------
//@ Rationale
//  Need to store and maintain event detail menu item.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and pre-conditions
// to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/EventDetailItems.ccv   25.0.4.0   19 Nov 2013 14:07:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 022  By: mnr   Date: 17-Feb-2010   SCR Number: 6436
//  Project:  PROX4
//  Description:
//      Trending events for PROX enable/disable added.
//
//  Revision: 005   By: mnr    Date: 07-Jan-2010    SCR Number: 6437
//  Project:  NEO
//  Description:
//      SCR number added in previous header.
//
//  Revision: 004   By: mnr    Date: 23-Nov-2009    SCR Number: 6437
//  Project:  NEO
//  Description:
//      New Event strings added.
//
//  Revision: 003   By: gdc    Date: 24-Sep-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal non-invasive CPAP.
//
//  Revision: 002  By: rhj    Date: 20-Jul-2007    SCR Number: 6383
//  Project:  Trend
//  Description:
//      Added Expiratory/Inspiratory pause events 
//
//  Revision: 001  By: rhj    Date: 06-Jun-2007    SCR Number: 6237
//  Project:  Trend
//  Description:
//      Initial version.
//=====================================================================

#include "EventDetailItems.hh"
#include "MiscStrs.hh"
#include "LanguageValue.hh"


//@ Usage-Classes
//@ End-Usage

//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: EventDetailItems()  [Constructor]
//
//@ Interface-Description
//  Constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Set default values for the eventDetailTable and initialize the
//  event menu items.
//  $[TR01155] The Event Selection subscreen shall contain...
//  $[TR011179] The system shall automatically generate the following...  
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
EventDetailItems::EventDetailItems(void)
{

	// Initialize to default values
	for (Uint32 index = 0; index < TrendEvents::TOTAL_EVENT_IDS; index ++)
	{
		eventDetailStrs_[index] = MiscStrs::EMPTY_STRING;
		eventDetailTable_.eventId[index]  = -1;
		eventDetailTable_.eventDetailStr[index]  = MiscStrs::EMPTY_STRING;
	}

	eventDetailStrs_[TrendEvents::USER_EVENT_SUCTION] = MiscStrs::USER_EVENT_SUCTION_LABEL;
	eventDetailStrs_[TrendEvents::USER_EVENT_RX] = MiscStrs::USER_EVENT_RX_LABEL;
	eventDetailStrs_[TrendEvents::USER_EVENT_BLOOD_GAS] = MiscStrs::USER_EVENT_BLOOD_GAS_LABEL;
	eventDetailStrs_[TrendEvents::USER_EVENT_RESPIRATORY_MANEUVER ] = MiscStrs::USER_EVENT_RESPIRATORY_MANEUVER_LABEL;
	eventDetailStrs_[TrendEvents::USER_EVENT_CIRCUIT_CHANGE] = MiscStrs::USER_EVENT_CIRCUIT_CHANGE_LABEL;
	eventDetailStrs_[TrendEvents::USER_EVENT_START_WEANING] = MiscStrs::USER_EVENT_START_WEANING_LABEL;
	eventDetailStrs_[TrendEvents::USER_EVENT_STOP_WEANING] = MiscStrs::USER_EVENT_STOP_WEANING_LABEL;
	eventDetailStrs_[TrendEvents::USER_EVENT_BRONCHOSCOPY] = MiscStrs::USER_EVENT_BRONCHOSCOPY_LABEL;
	eventDetailStrs_[TrendEvents::USER_EVENT_X_RAY ] = MiscStrs::USER_EVENT_X_RAY_LABEL;
	eventDetailStrs_[TrendEvents::USER_EVENT_RECRUITMENT_MANEUVER] = MiscStrs::USER_EVENT_RECRUITMENT_MANEUVER_LABEL;
	eventDetailStrs_[TrendEvents::USER_EVENT_REPOSITION_PATIENT] = MiscStrs::USER_EVENT_REPOSITION_PATIENT_LABEL;
	eventDetailStrs_[TrendEvents::USER_EVENT_OTHER ] = MiscStrs::USER_EVENT_OTHER_LABEL;
	eventDetailStrs_[TrendEvents::USER_EVENT_SURFACTANT_ADMIN ] = MiscStrs::USER_EVENT_SURFACTANT_ADMIN_LABEL;
	eventDetailStrs_[TrendEvents::USER_EVENT_PRONE_POSITION ] = MiscStrs::USER_EVENT_PRONE_POSITION_LABEL;
	eventDetailStrs_[TrendEvents::USER_EVENT_SUPINE_POSITION ] = MiscStrs::USER_EVENT_SUPINE_POSITION_LABEL;
	eventDetailStrs_[TrendEvents::USER_EVENT_LEFT_SIDE_POSITION ] = MiscStrs::USER_EVENT_LEFT_SIDE_POSITION_LABEL;
	eventDetailStrs_[TrendEvents::USER_EVENT_RIGHT_SIDE_POSITION ] = MiscStrs::USER_EVENT_RIGHT_SIDE_POSITION_LABEL;
	eventDetailStrs_[TrendEvents::USER_EVENT_MANUAL_STIMULATION ] = MiscStrs::USER_EVENT_MANUAL_STIMULATION_LABEL;
	eventDetailStrs_[TrendEvents::AUTO_EVENT_VENTILATION_INVASIVE] = MiscStrs::AUTO_EVENT_VENTILATION_INVASIVE_LABEL;
	eventDetailStrs_[TrendEvents::AUTO_EVENT_VENTILATION_NIV ] = MiscStrs::AUTO_EVENT_VENTILATION_NIV_LABEL;
	eventDetailStrs_[TrendEvents::AUTO_EVENT_MODE_AC ] = MiscStrs::AUTO_EVENT_MODE_AC_LABEL;
	eventDetailStrs_[TrendEvents::AUTO_EVENT_MODE_SIMV  ] = MiscStrs::AUTO_EVENT_MODE_SIMV_LABEL;
	eventDetailStrs_[TrendEvents::AUTO_EVENT_MODE_SPONT] = MiscStrs::AUTO_EVENT_MODE_SPONT_LABEL;
	eventDetailStrs_[TrendEvents::AUTO_EVENT_MODE_BILEVEL] = MiscStrs::AUTO_EVENT_MODE_BILEVEL_LABEL;
	eventDetailStrs_[TrendEvents::AUTO_EVENT_MANDATORY_VC] = MiscStrs::AUTO_EVENT_MANDATORY_VC_LABEL;
	eventDetailStrs_[TrendEvents::AUTO_EVENT_MANDATORY_VC_PLUS] = MiscStrs::AUTO_EVENT_MANDATORY_VC_PLUS_LABEL;
	eventDetailStrs_[TrendEvents::AUTO_EVENT_MANDATORY_PC] = MiscStrs::AUTO_EVENT_MANDATORY_PC_LABEL;
	eventDetailStrs_[TrendEvents::AUTO_EVENT_SPONTANEOUS_PS] = MiscStrs::AUTO_EVENT_SPONTANEOUS_PS_LABEL;
	eventDetailStrs_[TrendEvents::AUTO_EVENT_SPONTANEOUS_VS] = MiscStrs::AUTO_EVENT_SPONTANEOUS_VS_LABEL;
	eventDetailStrs_[TrendEvents::AUTO_EVENT_SPONTANEOUS_NONE] = MiscStrs::AUTO_EVENT_SPONTANEOUS_NONE_LABEL;
	eventDetailStrs_[TrendEvents::AUTO_EVENT_SPONTANEOUS_PA] = MiscStrs::AUTO_EVENT_SPONTANEOUS_PA_LABEL;
	eventDetailStrs_[TrendEvents::AUTO_EVENT_SPONTANEOUS_TC] = MiscStrs::AUTO_EVENT_SPONTANEOUS_TC_LABEL;
	eventDetailStrs_[TrendEvents::AUTO_EVENT_TIME_CHANGE] = MiscStrs::AUTO_EVENT_TIME_CHANGE_LABEL;
	eventDetailStrs_[TrendEvents::AUTO_EVENT_SAME_PATIENT] = MiscStrs::AUTO_EVENT_SAME_PATIENT_LABEL;
	eventDetailStrs_[TrendEvents::AUTO_EVENT_PATIENT_OCCLUSION] = MiscStrs::AUTO_EVENT_PATIENT_OCCLUSION_LABEL;
	eventDetailStrs_[TrendEvents::AUTO_EVENT_PATIENT_DISCONNECT] = MiscStrs::AUTO_EVENT_PATIENT_DISCONNECT_LABEL;
	eventDetailStrs_[TrendEvents::AUTO_NIF_MANEUVER_ACCEPTED] = MiscStrs::AUTO_NIF_MANEUVER_ACCEPTED_LABEL;
	eventDetailStrs_[TrendEvents::AUTO_P100_MANEUVER_ACCEPTED] = MiscStrs::AUTO_P100_MANEUVER_ACCEPTED_LABEL;
	eventDetailStrs_[TrendEvents::AUTO_VC_MANEUVER_ACCEPTED] = MiscStrs::AUTO_VC_MANEUVER_ACCEPTED_LABEL;
	eventDetailStrs_[TrendEvents::AUTO_EVENT_APNEA] = MiscStrs::AUTO_EVENT_APNEA_LABEL;
	eventDetailStrs_[TrendEvents::AUTO_EVENT_INSP_MANEUVER] = MiscStrs::AUTO_EVENT_INSP_MANEUVER_LABEL;
	eventDetailStrs_[TrendEvents::AUTO_EVENT_EXP_MANEUVER] = MiscStrs::AUTO_EVENT_EXP_MANEUVER_LABEL;
	eventDetailStrs_[TrendEvents::AUTO_EVENT_MODE_CPAP] = MiscStrs::AUTO_EVENT_MODE_CPAP_LABEL;
	eventDetailStrs_[TrendEvents::AUTO_EVENT_O2_SUCTION_BUTTON] = MiscStrs::AUTO_EVENT_O2_SUCTION_BUTTON_LABEL;
	eventDetailStrs_[TrendEvents::AUTO_EVENT_ALARM_VOLUME_CHANGE] = MiscStrs::AUTO_EVENT_ALARM_VOLUME_CHANGE_LABEL;
	eventDetailStrs_[TrendEvents::AUTO_EVENT_PROX_ENABLED] = MiscStrs::AUTO_EVENT_PROX_ENABLED_LABEL;
	eventDetailStrs_[TrendEvents::AUTO_EVENT_PROX_DISABLED] = MiscStrs::AUTO_EVENT_PROX_DISABLED_LABEL;
	eventIdStr_[0] = L'\0';
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~EventDetailItems()  [Destructor]
//
//@ Interface-Description
// Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

EventDetailItems::~EventDetailItems(void)
{
	CALL_TRACE("EventDetailItems::~EventDetailItems(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateEventDetailTable
//
//@ Interface-Description
//  This method updates the event detail table based on the given parameters:
// >Von
//  eventId[]             The pointer to the event id array.
//  totalNumberOfEvents   The total number of events in the array.
//  maxNumRowsDisplay     The maximum number of rows that can be 
//                        displayed in the event window.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//  Initializes the table with default values.  Then populates the 
//  table with the new event ids and the associated description of 
//  the event ids.  
//---------------------------------------------------------------------
//@ PreCondition
//  The totalNumberOfEvents must be less than TOTAL_EVENT_IDS.
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
EventDetailItems::updateEventDetailTable( TrendEvents::EventId eventId[], 
										  Uint32 totalNumberOfEvents)
{
	AUX_CLASS_ASSERTION(totalNumberOfEvents < TrendEvents::TOTAL_EVENT_IDS, 
						totalNumberOfEvents); 
	Uint32 row = 0;
	// Initialize the table with default values.
	for (Uint32 index = 0; index < TrendEvents::TOTAL_EVENT_IDS; index ++)
	{
		eventDetailTable_.eventId[index]  = -1;
		eventDetailTable_.eventDetailStr[index]  = MiscStrs::EMPTY_STRING;
	}

	// Populate the event detail table with the updated event ids.
	// The event ids must be a valid event number.
	for ( row = 0; row < totalNumberOfEvents; row++)
	{

		AUX_CLASS_ASSERTION( eventId[row] >= TrendEvents::BEGIN_USER_EVENT_ID &&  
							 eventId[row] < TrendEvents::TOTAL_EVENT_IDS, eventId[row]);

		eventDetailTable_.eventId[row] = TrendFormatData::FormatEvent(eventId[row]);
		eventDetailTable_.eventDetailStr[row] = eventDetailStrs_[eventId[row]];

	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getLogEntryColumn - revise description
//
//@ Interface-Description
//  This method takes an entryNumber, columnNumber and a return
//  string. If the requested column number is zero, then return 
//  an event id string. Otherwise this method returns a description of the
//  event id.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns the event id or event detail string from the 
//  eventDetailTable depending from the given parameters.
//---------------------------------------------------------------------
//@ PreCondition
//  EntryNumber must be greater than BEGIN_USER_EVENT_ID and must be 
//  less than TOTAL_EVENT_IDS.
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
 
void EventDetailItems::getLogEntryColumn(Uint16 entryNumber, 
										 Uint16 columnNumber,
										 Boolean &rIsCheapText, 
										 StringId &rString1,
										 StringId &rString2, 
										 StringId &rString3,
										 StringId &rString1Message)
{
	AUX_CLASS_ASSERTION( entryNumber >= TrendEvents::BEGIN_USER_EVENT_ID &&  
						 entryNumber < TrendEvents::TOTAL_EVENT_IDS, entryNumber);

	// Reinitialize the string
	eventIdStr_[0] = L'\0';

	// Populate the return string with the eventIdStr if the column
	// number is zero.  Otherwise populate the return
	// string with the detail description of the event id.
	if (columnNumber == 0)
	{
		// if the event id is an invalid number, return an empty 
		// string.
		if (eventDetailTable_.eventId[entryNumber] == -1)
		{
			swprintf(eventIdStr_, L"{p=8,y=10,T: }");

		}
		else
		{
			if (GuiApp::GetLanguage() == LanguageValue::CHINESE)
			{
				swprintf(eventIdStr_, L"{p=10,y=14,N:%d}", 
						eventDetailTable_.eventId[entryNumber]);
			}
			else
			{
				swprintf(eventIdStr_, L"{p=8,y=10,T:%d}", 
						eventDetailTable_.eventId[entryNumber]);
			}
		}


		rString1 = eventIdStr_;
	}
	else if (columnNumber == 1)
	{
		rString1 = eventDetailTable_.eventDetailStr[entryNumber];
	}
	else
	{
		AUX_CLASS_ASSERTION_FAILURE(columnNumber);
	}

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//        [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition   
//  none
//@ End-Method
//=====================================================================

void
EventDetailItems::SoftFault(const SoftFaultID  softFaultID,
							const Uint32       lineNumber,
							const char*        pFileName,
							const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, EVENTDATAITEMS,
							lineNumber, pFileName, pPredicate);
}

