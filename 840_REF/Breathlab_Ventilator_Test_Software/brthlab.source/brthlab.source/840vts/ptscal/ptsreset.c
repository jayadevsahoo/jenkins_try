/*****************************************************************************/
/*   Module Name:    ptsreset.c                                           
/*   Purpose:      Purpose                                 
/*                                                                          
/*                                                                           
/*   Requirements:   This module implements requirements detailed in         
/*          <75359-93>                                                       
/*                                                                           
/*   $Revision::   1.1      $                                                
/*                                                                          
/*****************************************************************************/

/*****************************************************************************
THIS IS A PROPRIETARY WORK TO WHICH NELLCOR PURITAN BENNETT CORPORATION CLAIMS
EXCLUSIVE RIGHT.  NO PART OF THIS WORK MAY BE USED, DISCLOSED, REPRODUCED,
STORED IN AN INFORMATION RETIEVAL SYSTEM, OR TRANSMITTED, IN ANY FORM OR BY ANY
MEANS ELECTRONIC, MECHANICAL, PHOTOCOPYING, RECORDING OR OTHERWISE WITHOUT THE
PRIOR WRITTEN PERMISSION OF NELLCOR PURITAN BENNETT CORPORATION.
 
Copyright (c) 1999, Mallinckrodt Nellcor Puritan Bennett
All rights reserved.
*******************************************************************************/

/***********************************************/
/****************** INCLUDES *******************/
/***********************************************/

#include <ansi_c.h>
#include <userint.h>
#include <utility.h>
#include "globals.h"
#include "72500.h"
#include "cpvtmain.h"
#include "ldb.h"
#include "mainpnls.h"
#include "MsgPopup.h"
#include "pts.h"
#include "pts2000.h"
#include "sysmenu.h"

/***********************************************/
/************ FUNCTION DECLARATION  ************/
/***********************************************/

/*****************************************************************************/
/* Name:     UpdatePTSInfo                                                   
/*                                                                           
/* Purpose:  Updates all PTS information
/*                                          
/*                                                                           
/* Example Call:   int UpdatePTSInfo();                                    
/*                                                                           
/* Input:    <None>       
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   0 if success >0 if faILURE                                                         
/*                                                                           
/*****************************************************************************/
int UpdatePTSInfo(void)
{   	
	int status;

	// Get PTS 2000 software revision
	status = GetPTS_SoftwareRev(szVersionNumber);
	if(status > 0 || status == CVI_ERROR){
		CPVTMessagePopup(LDBf(Ldb_PTS_Comm_Rcv_Error), LDBf(Ldb_PTS_Ver_Error), OK_ONLY_DLG, 0);
		sprintf(szVersionNumber, "%s", LDBf(Ldb_invalid));
	}

	// Get PTS 2000 serial number	
	status = GetPTS_SerialID(szSerialNumber);
	if(status > 0 || status == CVI_ERROR){
		CPVTMessagePopup(LDBf(Ldb_PTS_Comm_Rcv_Error), LDBf(Ldb_PTS_SN_Error), OK_ONLY_DLG, 0);
		sprintf(szSerialNumber, "%s", LDBf(Ldb_invalid));
	}

	// Get PTS 2000 calibrationd date
	status = GetPTS_CalDate(szCalDate);
	if(status > 0 || status == CVI_ERROR){
		CPVTMessagePopup(LDBf(Ldb_PTS_Comm_Rcv_Error), LDBf(Ldb_PTS_Date_Error), OK_ONLY_DLG, 0);
		sprintf(szCalDate, "%s", LDBf(Ldb_invalid));
		// Can't calculate NEXT if current is bad:
		sprintf(szNextCalDate, "%s", LDBf(Ldb_invalid));
	}
	else{
		SetPTSNextCalDate(szCalDate, szNextCalDate);
	}
		
	return status;
}


/*****************************************************************************/
/* Name:    Reset_PTS_Device                                                   
/*                                                                           
/* Purpose:  Resets the PTS 2000 device 
/*                                          
/*                                                                           
/* Example Call:   int Reset_PTS_Device();                                    
/*                                                                           
/* Input:    void           
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:     NONE                                                        
/*                                                  
/*                                                                           
/* Return:   int 0 = success                                                              
/*              -1 = error                                                        
/*                                                                           
/*****************************************************************************/
int Reset_PTS_Device(void)
{
	char cBuf[50];
	int i;

	// Asserting break resets the PTS2000
	ComBreakPTS(255);
	Delay(0.250);

	for(i = 10; i > 0; i--){
		sprintf(cBuf, LDBf(Ldb_Reseting_PTS), i);
		SetSysPanelMessage(cBuf);
		Delay(1.0);
	}

	// Double check PTS is found....
	SetSysPanelMessage(LDBf(Ldb_EstabCom_PTS));
	if(EstablishPTS(NO_AUTO_DETECT) != SUCCESS){
		SetSysPanelMessage("");
		CPVTMessagePopup(LDBf(Ldb_Com_Error), LDBf(Ldb_PTS_Com_Error), OK_ONLY_DLG, NO_HELP);
	}
	else{
		#ifndef DEMOMODE
		SetBarPrsOn();
		SetPTSSmplRate(10); // milliseconds
		#endif
		UpdatePTSInfo();
	}
	SetSysPanelMessage("");

	return 0;
}


/*****************************************************************************/
/* Name:     CmdPTSReset                                                 
/*                                                                           
/* Purpose:  This function handles callback for the Reset PTS-2000 button.
/*                                          
/*                                                                           
/* Example Call:   int CVICALLBACK CmdPTSReset(int panel, int control, int event,
/*		void *callbackData, int eventData1, int eventData2);                                    
/*                                                                           
/* Input:   standard callback arguments
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/	
int CVICALLBACK CmdPTSReset(int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch(event){
		case EVENT_COMMIT:
			SetCtrlAttribute(panel, control, ATTR_DIMMED, TRUE);
			Reset_PTS_Device();
			SetCtrlAttribute(panel, control, ATTR_DIMMED, FALSE);
			break;
	}

	return 0;
}

