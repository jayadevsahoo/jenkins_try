#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//      Copyright (c) 1994-1995, Puritan-Bennett Corporation
//=====================================================================  

// =========================== M O D U L E   D E S C R I P T I O N ====
//@ Filename: VgaDib.cc - Device Independent Bitmap drawing routines.
//---------------------------------------------------------------------
//@ Interface-Description
//  This module contains the functions used to display Windows 
//  standard device independent bitmaps (DIB). The DIB is converted to
//  C++ code by a utility program (bmpdecode) and compiled into the 
//  application. This class provides functions to allocate the colors
//  in the color palette and display the bitmap on the VGA.
//---------------------------------------------------------------------
//@ Rationale
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ $Header:   /840/Baseline/VGA-Graphics-Driver/vcssrc/VgaDib.ccv   25.0.4.0   19 Nov 2013 14:37:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004   By: rpr     Date: 11-Mar-2011     SCR Number: 6752
//  Project: XENA2
//      ExpandFont and ExpandIcon passes the original witdth of the font/bitmap
//		so clipping can be accounted for properly.
// 
//  Revision: 003   By: mnr    Date: 24-Sep-2010    SCR Number: 6672
//  Project: XENA2
//  Description:
//      New parameter added to DisplayBitmap().
//
//  Revision: 002   By: gdc    Date:  28-Aug-2000    DCS Number: 5493
//  Project: GuiComms
//  Description:
//	Implemented single screen compatibility.
//
//  Revision: 001   By: gdc    Date:  27-Jan-2000    DCS Number: 5337
//  Project: 840 Neonatal
//  Description:
//	Optimized VGA library for Neonatal project to use BitBlt and Windows
//  Bitmap structures.
//
//=====================================================================  

#include <math.h>
#include "VgaGraphicsDriver.hh"
#include "VgaDib.hh"

//@ Usage-Classes
#include "VgaDevice.hh"
#include "DisplayContext.hh"
#include "GraphicsContext.hh"
#include "DibInfo.hh"
//@ End-Usage

//=====================================================================  
//
//  Public Methods...
//
//=====================================================================  

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  AllocateColors
//---------------------------------------------------------------------
//@ Interface-Description
//  This method allocates the colors for the specified DIB represented
//  by RGB values in the RgbQuad structure to the color map hardware of
//  both VGA displays.  The allocated color index is then written to
//  the rgbReserved field of the RgbQuad struct which is used by the
//  DrawBitmap256_ method to write the color index to display memory.
//  This function allocates colors for both displays so a bitmap can be
//  displayed on either screen without remapping the color indexes in
//  the DIB.
//---------------------------------------------------------------------
//@ Implementation-Description
//  see Interface-Description
//----------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================  
void
VgaDib::AllocateColors( DibInfo & rDibInfo )
{
  DisplayContext & rUpperDisplay = VgaGraphicsDriver::GetUpperDisplayContext();
  DisplayContext & rLowerDisplay = VgaGraphicsDriver::GetLowerDisplayContext();

  RgbQuad * pRgbQuad = rDibInfo.pRgbQuads;

  if ( pRgbQuad )
  {									// $[TI1.1]
	for (Uint i=0; i<rDibInfo.biHeader.biClrUsed; i++)
	{								// $[TI2.1]
	  if ( !pRgbQuad->rgbReserved )
	  {								// $[TI3.1]
		rUpperDisplay.allocateColor(pRgbQuad->rgbReserved, *pRgbQuad);
		rLowerDisplay.allocateColor(pRgbQuad->rgbReserved, *pRgbQuad);
	  }								// $[TI3.2]
	  pRgbQuad++;
	}								// $[TI2.2] loop not executed
  }									// $[TI1.2]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  DisplayBitmap
//---------------------------------------------------------------------
//@ Interface-Description
//  This method displays a DIB on the specified DisplayContext at the 
//  xStart and yStart coordinates. Prior to displaying the bitmap, it 
//  clips the DIB as required by the GraphicsContext.
//---------------------------------------------------------------------
//@ Implementation-Description
//----------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================  

void
VgaDib::DisplayBitmap(
				  DisplayContext & rDc,
				  GraphicsContext & rGc,
				  DibInfo & rDibInfo,
			      Int x0, Int y0, Boolean isFont )
{
  const BitmapInfoHeader & rBitmapInfo = rDibInfo.biHeader;

  Uint colorv = rGc.getForegroundColor();
  Int  width  = rBitmapInfo.biWidth;
  Int  height = rBitmapInfo.biHeight;

  if (x0 <= rGc.getClipX1()  &&  y0 <= rGc.getClipY1()
      && (x0 + width) >= rGc.getClipX0()  && (y0 + height) >= rGc.getClipY0()  
	  &&  height > 0  &&  width > 0)
  {   // $[TI1.1] -- within clipping area...
	
    Int    yStart = 0;
    Int    xStart = 0;

	// volatile to silence compiler warning for unused var
    volatile Boolean  isClipped = FALSE;

    //adjust for clipping bounds
    if (x0 < rGc.getClipX0())
    {   // $[TI2.1]
      isClipped = TRUE;
      xStart    = rGc.getClipX0() - x0;
      width    -= xStart;
    }   // $[TI2.2]

    const Int  MAX_WIDTH = (rGc.getClipX1() - x0 - xStart + 1);

    if (width > MAX_WIDTH)
    {   // $[TI3.1]
      isClipped = TRUE;
      width   = MAX_WIDTH;
    }   // $[TI3.2]

    const Int  MIN_YSTART = (rGc.getClipY0() - y0);

    if (yStart < MIN_YSTART)
    {   // $[TI4.1]
       isClipped = TRUE;
       yStart  = MIN_YSTART;
       height   -= yStart;
    }   // $[TI4.2]

    const Int  MAX_HEIGHT = (rGc.getClipY1() - y0 - yStart + 1);

    if (height > MAX_HEIGHT)
    {   // $[TI5.1]
       isClipped = TRUE;
       height  = MAX_HEIGHT;
    }   // $[TI5.2]

	Uint fontOffset = rBitmapInfo.biSizeImage / rBitmapInfo.biHeight;
	Int  row = yStart * fontOffset;

#ifdef DEBUG_CLIPPING
	if ( isClipped )
	{
	  colorv = 9;
	  printf("clipped bitmap:\n");
	  printf("  width before = %d, width after = %d\n", rBitmapInfo.biWidth, width);
	  printf("  height before = %d, height after = %d\n", rBitmapInfo.biHeight, height);
	  printf("  xStart = %d\n", xStart);
	  printf("  yStart = %d\n", yStart);
	  printf("  fontOffset = %d\n", fontOffset);
	}
#endif // DEBUG_CLIPPING

    if ( rBitmapInfo.biBitCount == 1 )
    {   								// $[TI6.1]
	  const Byte * pFontData = (Byte*)rDibInfo.pBitmapBits + row;

	  // remove left-side clipping delta (left side clipping not practical
	  // using BitBlt hardware
	  width += xStart;
	  xStart = 0;

	  if (isFont)
	  {
	    rDc.expandFont(x0+xStart, y0+yStart, width, height, 
					   pFontData, fontOffset, colorv, rBitmapInfo.biWidth);
	  }
	  else //Its an Icon
	  {
		rDc.expandIcon(x0+xStart, y0+yStart, width, height, 
					   pFontData, fontOffset, colorv,  rBitmapInfo.biWidth);
	  }
	}
	else if ( rBitmapInfo.biBitCount == 4 )
    {	 								// $[TI6.2]
	  // 4bit color bitmap
      const Byte * pDataByte = (Byte*)rDibInfo.pBitmapBits + row + xStart/2;

	  Draw24BitWndBitmap (rDc, x0+xStart, y0+yStart, width, height,
				    pDataByte, rDibInfo);
    }
	else if ( rBitmapInfo.biBitCount == 8 )
    { 									// $[TI6.3]
	  // 8bit color bitmap
      const Byte * pDataByte = (Byte*)rDibInfo.pBitmapBits + row + xStart;

	  Draw24BitWndBitmap(rDc, x0+xStart, y0+yStart, width, height,
				    pDataByte, rDibInfo);
    }
    else 
    {
      // not supported in current version   
      AUX_CLASS_ASSERTION_FAILURE(rBitmapInfo.biBitCount);
    }
  }   // $[TI1.2] -- NOT within clipping area...
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
VgaDib::SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName,
			  const char*       pPredicate)
{
  FaultHandler::SoftFault(softFaultID, VGA_GRAPHICS_DRIVERS,
			  VgaGraphicsDriver::VGA_DIB_ID, 
			  lineNumber, pFileName, pPredicate);
}


//=====================================================================  
//
//  Private Methods...
//
//=====================================================================  

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetEightHexDigits()
//---------------------------------------------------------------------
//@ Interface-Description
//  This fucntion extract the eight values into array  [in/out]array 
//  (i.e. 0,4,0,0,5,0,0,6) from given Hex DWORD (i.e.0x04005006).
//  These (i.e. 0,4,0,0,5,0,0,6) each values will be used as index to 
//  determine the pixel color at that location.
//---------------------------------------------------------------------
//@ Implementation-Description GetEightHexDigits
//----------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================  
void VgaDib::GetEightHexDigits(Uint32 i8DigitHexData,int* iHexDigit) 
{
    int iMask = 0x0000000F;
	for(int i = 7;i>=0;i--)
	{
		int iData = i8DigitHexData & iMask;
		iMask = iMask << 4;
		if(i<7)
		  iHexDigit[i] = iData >> ((7-i) * 4);
		else
		  iHexDigit[i] = iData;
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  Draw24BitWndBitmap
//---------------------------------------------------------------------
//@ Interface-Description
//  This method draws the specified DIB to the VGA display at the
//  specified coordinates and size.
//  It accepts either a 24 bit per pixel DIB with color values
//---------------------------------------------------------------------
//@ Implementation-Description
//----------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================  
void
VgaDib::Draw24BitWndBitmap(
		DisplayContext & rDc,
		Int     xDest,		Int  yDest,
		Int     xWidth,		Int yHeight,
		const Byte* pBitmapByte, 
		const DibInfo & rDibInfo)
{
   const BitmapInfoHeader & rBitmapInfo = rDibInfo.biHeader;
   //Initializing the Bitpmap header
   BITMAPINFOHEADER bmih;
   bmih.biSize			=   rBitmapInfo.biSize; // Size of BitmapInfoHeader structure
   bmih.biWidth			=   rBitmapInfo.biWidth; // Width of bitmap in pixels
   bmih.biHeight		=   rBitmapInfo.biHeight; // Height of bitmap in pixels
   bmih.biPlanes		=   rBitmapInfo.biPlanes; // Set to 1
   bmih.biBitCount		=   24; // Color bits per pixel
   bmih.biCompression	=   BI_RGB; // Compression scheme
   bmih.biXPelsPerMeter	=	rBitmapInfo.biXPelsPerMeter; // Horizontal resolution in pixels per meter
   bmih.biYPelsPerMeter	=	rBitmapInfo.biYPelsPerMeter; // Vertical resolution in pixels per meter
   bmih.biClrUsed		=   rBitmapInfo.biClrUsed; // Number of colors used in image   
   bmih.biClrImportant	=   rBitmapInfo.biClrImportant; // Number of important colors in image  
   //Calculate number of bytes per row.	   
   int RowSize = (((bmih.biBitCount * bmih.biWidth) + 31)/32) *4;
   //Calculate Image size in pixels
   bmih.biSizeImage = ((((bmih.biWidth	 * bmih.biBitCount) + 31) & ~31) >> 3) * bmih.biHeight;
  
  //TODO E600 :- Original 840 code has bitmaps with maximum 7 colors, hence limited to 7.
  //When GUI will be re-designed or resurfaced this may not be required.
   RGBQUAD    rgb[7];
   for(int iIndex = 0 ; iIndex <rBitmapInfo.biClrUsed;iIndex++) {
	   rgb[iIndex].rgbBlue = rDibInfo.pRgbQuads[iIndex].rgbBlue;
	   rgb[iIndex].rgbGreen  = rDibInfo.pRgbQuads[iIndex].rgbGreen ;
	   rgb[iIndex].rgbRed  = rDibInfo.pRgbQuads[iIndex].rgbRed ;
	   rgb[iIndex].rgbReserved = rDibInfo.pRgbQuads[iIndex].rgbReserved;
   }

   BITMAPINFO bmi;
   bmi.bmiHeader = bmih;
   bmi.bmiColors[0]  = rgb[0];

 // ***** Buffer allocation for the Bitmap ***
 //TODO E600 :- Regarding memory allocation Location and its frequency of allocation can be revisited for performance purpose.
  char* ptrBmpData = new char[bmih.biSizeImage];  
  memset(ptrBmpData,'\0',sizeof(char)*bmih.biSizeImage);
 ////////////////////////////////////
  unsigned int i =0;
  //Determine mumber of words per row in  *.cc bitmap data files.
  float fNumberOfWordPerRow = rBitmapInfo.biWidth/8.0f;
  Uint32 NumberOfWordsPerRow = ceil(fNumberOfWordPerRow);
  //Calulate the Number of Bytes to be padded in each Bitmap Row.
  int iNoPadingBytes = RowSize - (rBitmapInfo.biWidth*3);
  BOOL bIsPaddingRequired = TRUE;

#ifndef SIGMA_GUIPC_CPU
  if(iNoPadingBytes < 2 ) {
	bIsPaddingRequired = TRUE;
  }
  else{
	bIsPaddingRequired = FALSE;
  }
#endif
//Nested loop to creatre a data buffer requierd for Windows BPM format.
#ifdef SIGMA_GUIPC_CPU
  for (Uint y = yHeight; y > 0; y--)
#else
  for (Uint y = 1; y <= yHeight; y++)
#endif
  {				
    Uint iRowByteCounter = 0;
	for (Uint x=0; x < NumberOfWordsPerRow ; x++)
	{	
	    int iIndex = ((y*NumberOfWordsPerRow ) - NumberOfWordsPerRow )+x;	
		int iOldHexData = rDibInfo.pBitmapBits[iIndex];
		int iHexDigit[8] = {0,0,0,0,0,0,0,0};
		int iBinary4Digit[4] = {0,0,0,0};
		GetEightHexDigits(iOldHexData,iHexDigit);
		for(int k=0;k<8;k++) {
			int iDecimalVal = iHexDigit[k];
			if(iRowByteCounter < (bmih.biWidth*3)) {
				ptrBmpData[i] = rDibInfo.pRgbQuads[iHexDigit[k]].rgbBlue;
				ptrBmpData[i+1] = rDibInfo.pRgbQuads[iHexDigit[k]].rgbGreen;
				ptrBmpData[i+2] = rDibInfo.pRgbQuads[iHexDigit[k]].rgbRed;
				i+=3;
				iRowByteCounter+=3;
			}
			else {
				if(bIsPaddingRequired) {
					ptrBmpData[i] = 0;
					i++;
					iRowByteCounter++;
				}
			}
			if(iRowByteCounter >= RowSize) {
				break;
			}
		}//End of for(k)
	}// end for width
  }// end for height

  CDC* pCDC = rDc.getCDC();
  HDC hRefDC = rDc.getCDC()->GetSafeHdc();

 //To get DI Bitmap Handle
  HDC hdc = ::GetDC(NULL); 
  HBITMAP hBitmap = 0;

#ifdef SIGMA_GUIPC_CPU
 hBitmap  = CreateDIBitmap(
		hdc, 
		&bmih, 
		CBM_INIT, 
		ptrBmpData, 
		&bmi, 
		DIB_RGB_COLORS); 
#else
	hBitmap = CreateBitmap( bmih.biWidth, bmih.biHeight, bmih.biPlanes,bmih.biBitCount,ptrBmpData);
#endif

  ::ReleaseDC(NULL,hdc);
  //Create memory compatible DC
  HDC hMemDC = ::CreateCompatibleDC(hRefDC );
  HBITMAP hOldBitmap = (HBITMAP)::SelectObject( hMemDC, hBitmap );

  //To determine the trasnperent color.
  COLORREF crWndCol;
  if( rDibInfo.pRgbQuads != NULL){
		crWndCol = RGB(rDibInfo.pRgbQuads[rDibInfo.transparentColor].rgbRed,
		    rDibInfo.pRgbQuads[rDibInfo.transparentColor].rgbGreen,
		    rDibInfo.pRgbQuads[rDibInfo.transparentColor].rgbBlue);
   }
   //Display the Bitmap on screen.
   if(rDibInfo.isTransparent) {
		::TransparentBlt(hRefDC, xDest, yDest, xWidth, yHeight,
					hMemDC, 0, 0, xWidth, yHeight, crWndCol );
   }
   else{
		::BitBlt( hRefDC, xDest, yDest, xWidth, yHeight,
			       hMemDC, 0, 0, SRCCOPY );
	   
   }
	
  ::SelectObject( hMemDC, hOldBitmap );
  ::DeleteObject( hBitmap );
	
 //TODO E600:- Regarding memory allocation Location and its frequency of allocation can be revisited for performance purpose.
  if(ptrBmpData != NULL) {
    delete[] ptrBmpData;
	ptrBmpData = NULL;
  }
  //////////////////////////////////////
}
