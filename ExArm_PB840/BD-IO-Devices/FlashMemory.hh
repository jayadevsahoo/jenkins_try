#ifndef FlashMemory_HH
#define FlashMemory_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: Flash Memory - Utilities to erase, write to flash memory.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/FlashMemory.hhv   25.0.4.0   19 Nov 2013 13:54:06   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By: rhj    Date:  11-April-2006    DR Number: 6192
//  Project:  XENA
//  Description:
//      Added support for the Spansion Flash memory chip.
//      These changes only supports the XENA GUI board with the
//      non-XENA BD board.
//
//  Revision: 002  By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 001  By:  syw    Date: 08-Nov-1996    DR Number: INITIAL RELEASE
//		Project:  Sigma (R8027)
//		Description:
//			Initial version (Integration baseline).
//
//====================================================================

#include "Sigma.hh"
#include "BD_IO_Devices.hh"
//TODO E600 port if need or obsolete #include "FlashProgrammer.hh"

//@ Usage-Classes
//@ End-Usage

//TODO E600 port
#define MAX_TOTAL_NUM_SUB_BLOCKS	40	//TODO E600 added this def here temporarily, find original location..


class FlashMemory {
  public:

    static void Initialize(void);

    static void SoftFault( const SoftFaultID	softFaultID,
						   const Uint32		lineNumber,
						   const char		*pFileName  = NULL, 
						   const char		*pPredicate = NULL) ;

	static Uint32 EraseFlash( Uint32 *start, Uint32 *end ) ;
	static Uint32 FlashWrite( Uint32 *writeAddress, Uint32 data) ;
	static void BackupCheckSumTable( void) ;
	static Uint32 RestoreCheckSumTable( void) ;

#ifdef SIGMA_DEBUG

	static void DumpFlash( Uint32 *start, Uint32 *end ) ;
	static void VerifyFlash( Uint32 *start, Uint32 *end, Uint32 data) ;
	static void FlashTestDriver( void) ;

#endif // SIGMA_DEBUG

  
  protected:

  private:
    FlashMemory( void) ;					// not implemented...
    ~FlashMemory( void) ;					// not implemented...
    FlashMemory( const FlashMemory&) ;		// not implemented...
    void operator=( const FlashMemory&) ;	// not implemented...

	//@ Data-Member:  FlashBlockAddress_
	// Stores the Block Address of each block of Flash
	static Uint32 * FlashBlockAddress_[(MAX_TOTAL_NUM_SUB_BLOCKS + 1)] ;

	//@ Data-Member:  initializationFlag_
	// Flag indicating whether initialize() is called, or not.
	static Boolean  initializationFlag_;

} ;


#endif // FlashMemory_HH 
