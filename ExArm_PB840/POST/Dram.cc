#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: Dram - Dynamic Ram Memory Test Power On Self Test
//---------------------------------------------------------------------
//@ Interface-Description
//  >Von
//    The non-kernel DRAM is tested in this module.
//
//    Test                        - Performs the POST test
//    FastExchangeRAMTest         - Fast memory test algorithm
//    ParityTest                  - Tests the parity generating system
//
//  >Voff
//---------------------------------------------------------------------
//@ Rationale
//    These tests satisfy the RAM test functions of POST.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//    The DRAM test of POST tests the remaining RAM that was not tested during
//    the Kernel stack RAM test.  This test uses the same cell integrity test
//    as described previously.  (reference: Kernel Stack Ram Test)
//
//    POST will assume that there is 4MB of DRAM.
//
//    The address bus and data bus tests as well as the odd offset addressing
//    test will have already been performed during the Kernel DRAM test, so
//    that all that remains is the cell integrity test on the portion of
//    memory that is not part of the Kernel memory.  Memory initialization
//    is performed by using an appropriate initial pattern in the cell
//    integrity test.   The processor instruction cache will be enabled
//    for this test, but for obvious reasons the data cache is disabled.
//
//    This test is  one of the most time consuming parts of POST, and the
//    time requirements on POST are challenging.  Although many alternative
//    algorithms are available for RAM testing, a 2 pattern cell integrity
//    test along with the data  bus, address bus, and odd offset tests has
//    been analyzed extensively as the most reasonable, effective tests
//    given the real time constraints of this system.  The safety net design
//    combinations of memory parity, bus timeouts of unmapped memory, the
//    watchdog mechanism, assertion and exception handling mechanisms
//    mitigate the need for doing a more involved DRAM test during POST.
//
//    The dynamic RAM refresh timer of the 68HC901 timer is tested implicitly
//    in this test.   A desired effect of the cell integrity test is a
//    high speed exercising of DRAM refreshing timing.   A failure of the
//    901 refresh timer will result in a failure of the Kernel Stack DRAM
//    or full DRAM test.
//
//    The fast exchange memory test verifies that DRAM can hold a 1 and
//    a zero in each bit.  All DRAM memory except the stack assigned to POST
//    during the kernel test is tested, the post stack is tested earlier
//    in the Kernel.  This test overwrites the contents of memory that
//    is tested, leaving a known pattern.
//
//    A parity test is performed by generating an expected parity error,
//    using a special hardware register designed for this purpose.
//---------------------------------------------------------------------
//@ Fault-Handling
//    If the fastEchangeRAMTest fails, the test fails MAJOR.
//    If an expected parity error does not occur, this test fails MAJOR.
//---------------------------------------------------------------------
//@ Restrictions
//    This test must be called from the postExecutive, using only pretested
//    stack space that will not be tested during this test.
//---------------------------------------------------------------------
//@ Invariants
//    None
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/POST/vcssrc/Dram.ccv   25.0.4.0   19 Nov 2013 14:17:04   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002    By: Gary Cederquist  Date: 15-AUG-1997  DR Number: 1875
//    Project: Sigma (R8027)
//    Description:
//       Added testable item annotations.
//
//  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//=====================================================================
// no instruction reordering required for DRAM Parity Test
#pragma  option  -nOr

#include "Dram.hh"
#include "Post_Library.hh"

#ifdef E600_840_TEMP_REMOVED
extern Criticality FastExchangeRAMTest(void);
#endif

static volatile Uint8    ParityOccurred_;
static volatile Uint32   ParityTestLongWord_;
static volatile Uint8  * ParityTestAddress_;

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Clear(void)
//
//@ Interface-Description
//  Clears DRAM and initializes parity for the address
//  range $8000 to $3fffff.  Kernel POST tests the $0 to $7fff.
//  This method is used during short POST since the memory must
//  still be initialized, but need not take the time to test it.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//     none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Free-Function
//=====================================================================
Criticality
Dram::Clear(void)
{
#ifdef E600_840_TEMP_REMOVED
   ClearMemory((Uint*)APP_DRAM_BASE, (Uint)APP_DRAM_LENGTH);
#endif

   // $[TI1]
   return PASSED;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Free-Function: Test(void)
//
//@ Interface-Description
//  Perform the fast exchange test and parity test over the DRAM space
//  $8000 to $3fffff.  The 1st 32K is tested during Kernel testing.
//
//  Force parity errors and shows that the appropriate exceptions result.
//  A bit in a memory mapped register will be provided that forces a parity
//  error. Memory is organized into 4 byte words, so there will be 4 parity
//  generator units, and a separate error force bit for each. Thus this
//  test must test each of the 4 parity generators. Parity errors
//  cause an access fault, so the interrupt handler will have to be flagged
//  to expect the error.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//     none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Free-Function
//=====================================================================
Criticality
Dram::Test(void)
{
#ifdef E600_840_TEMP_REMOVED
   if (FastExchangeRAMTest() != PASSED)
   {
      // $[TI1.1]
      return MAJOR;
   }
      // $[TI1.2]
#endif
   return PASSED;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ParityTestExceptionHandler
//
//@ Interface-Description
//  The DRAM parity test installs this handler to process an access
//  fault during the test.  Upon entry, the NMI source register should
//  indicate a parity error occurred.  The handler fixes the byte that
//  had a parity error, sets a flag to indicate the exception occurred
//  and returns.  Prior to returning, the instruction pipeline is
//  flushed to force completion of any pending write operations
//  required for processing to continue.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//     none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
#define  ACCESS_FAULT_VECTOR                  (2) //parity errors cause an
                                                //access fault
#define  WRITE_WRONG_PARITY_REG            ((Uint8 *) 0xFFBE8034)
#define  WRONG_PARITY_AND_BYTE_ENABLE_BITS (0x14)
#define  CLEAR_PARITY_ERROR_BIT            (0x08)

#define  PARITY_ERROR_BIT_MASK             (0x01)

//TODO E600 removed interrupt keyword before void
void
Dram::ParityTestExceptionHandler_(void)
{
#ifdef E600_840_TEMP_REMOVED
   Uint8 nmi = *NMI_REGISTER & PARITY_ERROR_BIT_MASK;
#endif
   Uint8 nmi = 0;

UNIT_TEST_POINT(POSTUT_20_4)

   // check that the parity error bit is set in the NMI source
   // register.  If it is not set, don't set the parity occurred flag
   // and the main thread test will report a failure

   if (nmi == PARITY_ERROR_BIT_MASK)
   {
      // $[TI1.1]
      ParityOccurred_ = TRUE;
   }
      // $[TI1.2]

   // remove the parity error at the test location
   *ParityTestAddress_ = 0;

#if defined(SIGMA_UNIT_TEST)
volatile   // so clear_parity is not optimized away
#endif
   Uint8 clear_parity = CLEAR_PARITY_ERROR_BIT;

UNIT_TEST_POINT(POSTUT_20_5)

   // clear the parity error from the control register
   *WRITE_WRONG_PARITY_REG = clear_parity;
   *WRITE_WRONG_PARITY_REG = 0;

   //  force bus synchronization before return

   //TODO E600 removed asm
   //asm(" nop ");
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ParityTest_
//
//@ Interface-Description
//
//  This method tests the DRAM parity checking and interrupt circuitry.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//  This test DRAM parity circuit by intentionally generating a parity
//  error and verfying a NMI occurs.  It does that by:
//
//    1. Installing a special test interrupt handler to the NMI interrupt
//       vector.
//
//    2. Using the write wrong parity register to intentionally write
//       a byte with bad parity to DRAM.
//
//    3. Reading the bad byte back. That should cause an NMI with the
//       bad parity bit set in the NMI source register.
//
//    4. The special interrupt handler should entered.  If checks that
//       the parity error bit (and only that bit) is set in the NMI
//       source register.  If fixes the parity error byte (that needed
//       otherwise the NMI will occur again).  It then sets a flag to
//       tell the main thread that the parity error was seen.
//
//    5. The main thread checks the parity error flag. If it is not set
//       thats and error.
//
//    6. The parity circuit works on eack byte in a DRAM long word.
//       Therefore, this test is performed on each byte in a test
//       long word.
//
// Note: A variable >>ParityTestAddress_<< is shared with the special
//       interupt handler to tell it what address to fix when the
//       parity error is seen.
//
// About writing bad parity:
//
//   The Write Wrong Parity Register looks like this:
//
//     ---------------------------------------------------------------------|
//     | Bit |  function                                                    |
//     |-----|--------------------------------------------------------------|
//     |  7  |  none                                                        |
//     |-----|--------------------------------------------------------------|
//     |  6  |  none                                                        |
//     |-----|--------------------------------------------------------------|
//     |  5  |  none                                                        |
//     |-----|--------------------------------------------------------------|
//     |  4  |  Parity Test Bit   - this is the value that will be written  |
//     |     |                      to the parity bit                       |
//     |-----|--------------------------------------------------------------|
//     |  3  |  Clear Parity Error - setting this bit will clear the        |
//     |     |                       bit that records parity errors         |
//     |-----|--------------------------------------------------------------|
//     |  2  |  Byte Enable        - this bit must be set to enable writing |
//     |     |                       bad parity                             |
//     |-----|--------------------------------------------------------------|
//     |  1  |  Byte Select        - 00 - byte 0                            |
//     |     |                       01 - byte 1                            |
//     |  0  |                       10 - byte 2                            |
//     |     |                       11 - byte 3                            |
//     ----------------------------------------------------------------------
//
//    To write wrong parity to a specific byte:
//
//        1. Write to the Write Wrong Parity register with:
//          a. the byte select lines selecting the byte desired
//          b. the byte enable bit must be 1
//          c. choose the value of the Parity Test Bit so that
//             the number of bits in the value written to the test byte
//             that are 1's + the value of the Parity Test Bit is odd
//       2. write the value to the byte.
//       3. write all 0's to the Write Wrong Parity register to stop
//          writing bad parity.
//
//    To clear the parity error (and remove the NMI that is causes)
//    write the Write Wrong Parity Error register with just the
//    Clear Parity Error bit set. Then write all zero's to the
//    Write Wrong Parity register. NOTE: This does NOT fix the memory
//    location that has bad parity. It only clears the parity error
//    bit in the NMI register.
//
//    To clear a parity error in memory, that location must be written
//    with good parity.
//
//    NOTE: Since write wrong parity is enabled for ALL memory operations
//          for the duration of the test, no bus master should be
//          allowed to write to memory during the parity test.
//          Specifically, the Ethernet self-test that writes its results
//          to memory must not be allowed to run concurrently.
//          Interrupts are also disabled during this test so
//          interrupt stacking does not generate bad parity.
//
//---------------------------------------------------------------------
//@ PreCondition
//     none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
#define  ALL_BYTES   (4)   //all 4 bytes
Criticality
Dram::ParityTest(void)
{
	//TODO E600 port

   Criticality status = PASSED;
/*
   Uint16 i;
   Uint8 nmi;
#if defined(SIGMA_UNIT_TEST)
   volatile   //  so compiler doesn't optimize away symbol wrong_parity_value
              //  allowing emulator to change its value
#endif
   Uint8 wrong_parity_value;
   Handler handler = Dram::ParityTestExceptionHandler_;

   DISABLE_INTERRUPTS();

UNIT_TEST_POINT(POSTUT_20_1)

   // **** connect the test interrupt handler ****
#ifdef E600_840_TEMP_REMOVED
   Handler isr = InstallHandler( ACCESS_FAULT_VECTOR, handler );
#endif

   ParityTestAddress_ = (Uint8 *) &ParityTestLongWord_;

   // **** test each byte in a long word *****
   for (i = 0 ; i < ALL_BYTES ; i++, ParityTestAddress_++)
   {
      // **** clear the flag that will be set by the interrupt routine ****
      ParityOccurred_ = FALSE;

      wrong_parity_value =   WRONG_PARITY_AND_BYTE_ENABLE_BITS
                           | ((Uint32) ParityTestAddress_ & 0x3);

UNIT_TEST_POINT(POSTUT_20_2)

      // ****************************************
      // **** write the byte with bad parity ****
      // ****************************************
      *WRITE_WRONG_PARITY_REG = wrong_parity_value;

//TODO E600 removed asm
      //asm(" nop "); //  force bus synchronization

      *ParityTestAddress_ = 0;
      //asm(" nop "); //  force bus synchronization

      *WRITE_WRONG_PARITY_REG = 0; // stop writing bad parity
      //asm(" nop "); //  force bus synchronization

      // *********************************************************************
      // **** Read back the location written, should cause a parity error ****
      // *********************************************************************
      *ParityTestAddress_;

UNIT_TEST_POINT(POSTUT_20_3)

      // ************************************************
      // **** if the flag is not set, thats an error ****
      // ************************************************
      if (!ParityOccurred_)
      {
         // $[TI1.1]
         break;
      }
         // $[TI1.2]

      // ************************************************
      // **** if the parity error bit is still set   ****
      // **** in the NMI register, thats an error.   ****
      // ************************************************
#ifdef E600_840_TEMP_REMOVED
      nmi = *NMI_REGISTER & PARITY_ERROR_BIT_MASK;
#endif

UNIT_TEST_POINT(POSTUT_20_7)
      if (nmi)
      {
         // $[TI2.1]
         break;
      }
         // $[TI2.2]

UNIT_TEST_POINT(POSTUT_20_6)

      // ****************************************************
      // **** If the memory location still causes parity ****
      // **** errors, thats an error.                    ****
      // ****************************************************
      ParityOccurred_ = FALSE;
      *ParityTestAddress_;
      if (ParityOccurred_)
      {
         // $[TI3.1]
         break;
      }
         // $[TI3.2]
   }

   // **** disconnect the test interrupt handler ****
#ifdef E600_840_TEMP_REMOVED
   (void) InstallHandler(ACCESS_FAULT_VECTOR, isr);
#endif

   if (i != ALL_BYTES)
   {
      // $[TI4.1]
      status = MAJOR;
   }
      // $[TI4.2]

   ENABLE_INTERRUPTS();
*/

   return status;
}

