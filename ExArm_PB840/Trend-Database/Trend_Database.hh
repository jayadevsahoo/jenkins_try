#ifndef Trend_Database_HH
#define Trend_Database_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
//@ File: Trend_Database.hh - Trend Database class Ids and static Initialize
//---------------------------------------------------------------------
//  Version-Information
// @(#) $Header:   /840/Baseline/Trend-Database/vcssrc/Trend_Database.hhv   25.0.4.0   19 Nov 2013 14:34:42   pvcs  $
//
//  Modification-Log
//
//
//  Revision: 001  By:  mjf    Date:  27-Feb-2007    SCR Number: 6237
//       Project:  Trending
//       Description:
//             Subsystem header file
//=====================================================================

#include "Sigma.hh"    // Utilities

//TODO E600 added for cout, endl etc.
#include <iostream>
using namespace std;


//@ Type: TrendDatabaseClassId
// Ids of all the classes of this subsystem.
enum TrendDatabaseClassId
{
	// public access modules
	TREND_DATA_MGR         = 1,
	TREND_DATA_SET         = 2,
	TREND_DATA_VALUES      = 3,
	TREND_EVENTS           = 4,
	TREND_REQUEST_INFO     = 5,

	// private implementation modules
	TREND_DB_APPLICABILITY         = 20,     
	TREND_DB_CONFIG                = 21,  
	TREND_DB_PATIENT_RECORD        = 22,
	TREND_DB_PATIENT_REPOSITORY    = 23,
	TREND_DB_RECORD                = 24,
	TREND_DB_SAMPLE_AVG            = 25,
	TREND_DB_SAMPLE_DATA           = 26,
	TREND_DB_SAMPLE_EVENTS         = 27,
	TREND_DB_SAMPLE_NON_AVG        = 28,
	TREND_DB_SETTING_RECORD        = 29,
	TREND_DB_SETTING_REPOSITORY    = 30,
	TREND_DB_TABLE                 = 31,
	TREND_DB_TABLE_BUFFER          = 32,
	TREND_DB_TABLE_VARS            = 33,
	TREND_DB_EVENT_RECEIVER        = 34,
	TREND_DB_SAMPLE_MANEUVER       = 35,
	TREND_DB_CONTROL_BLOCK         = 36,
	
	NUM_TREND_DATABASE_CLASSES
};


// Static method used to initialize the TrendDataMgr 
extern  void    TrendDatabaseInitialize(void);


// DEBUG macro
#include <stdio.h>

#define TREND_MGR_DBG1(m, a)			    \
	if(m & TrendDataMgr::DebugMask) DEBUG_MSG(a);
#define TREND_MGR_DBG2(m, a, b)		    \
	if(m & TrendDataMgr::DebugMask) DEBUG_MSG(a << b);
#define TREND_MGR_DBG3(m, a, b, c)		\
	if(m & TrendDataMgr::DebugMask) DEBUG_MSG(a << b << c);
#define TREND_MGR_DBG4(m, a, b, c, d)		\
	if(m & TrendDataMgr::DebugMask) DEBUG_MSG(a << b << c << d);
#define TREND_MGR_DBG6(m, a, b, c, d, e, f) \
	if( m & TrendDataMgr::DebugMask) DEBUG_MSG(a << b << c << d << e << f);


#endif	// Trend_Database_HH
