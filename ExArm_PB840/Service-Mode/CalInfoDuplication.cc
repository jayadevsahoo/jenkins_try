#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: CalInfoDuplication - Copy the CalInfoFlashBlock from BD to GUI
//---------------------------------------------------------------------
//@ Interface-Description
//	This is a static class which contains methods to copy calibration
//	information from BD FLASH to GUI FLASH.
//---------------------------------------------------------------------
//@ Rationale
//	This class encapsulates the methods required to copy calibration
//	information from BD FLASH to GUI FLASH.
//---------------------------------------------------------------------
//@ Implementation-Description
//	The method execute() is invoked by the GUI to perform the duplication.
//	A callback method is registered with the NetworkApp class to allow
//	the necessary communications for data transfer.  The actual data
//	transfer occurs in a separate task from the task which invokes execute().
//	NOTE: as a Service Mode utility class, there is no direct SRS
//	traceability.
//---------------------------------------------------------------------
//@ Fault-Handling
//	n/a
//---------------------------------------------------------------------
//@ Restrictions
//	Only one instance of this class may be created.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/CalInfoDuplication.ccv   25.0.4.0   19 Nov 2013 14:23:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 005  By:  quf    Date: 11-Nov-1997    DR Number: DCS 2621
//       Project:  Sigma (840)
//       Description:
//			useDefaultEvTable() is now compiled all the time, i.e. no longer
//			conditionally compiled using the SIGMA_DEVELOPMENT and
//			SIGMA_PSUEDO_OFFICIAL compile flags.
//
//  Revision: 004  By:  quf    Date: 29-Sep-1997    DR Number: DCS 2525
//       Project:  Sigma (840)
//       Description:
//			Fixed method for setting EV cal novram flag in useDefaultEvTable().
//
//  Revision: 003  By:  quf    Date: 29-Aug-1997    DR Number: DCS 2435
//       Project:  Sigma (840)
//       Description:
//            Added useDefaultEvTable() to create a default exh valve
//            calibration table for SIGMA_DEVELOPMENT and SIGMA_PSUEDO_OFFICIAL
//            builds only.
//
//  Revision: 002  By:  quf    Date: 22-May-1997    DR Number: DCS 2131
//       Project:  Sigma (840)
//       Description:
//            Modification-Log fix.
//            Fixed global reference names (i.e. "r" changed to "R").
//            Stubbed the NetworkApp interfaces, fixed requestBdFlashData_()
//            to use the stubbed NetworkApp::XmitMsg() rather than
//            conditionally compiling it out during unit test.
//
//  Revision: 001  By:  syw    Date:  25-Apr-1996    DR Number: none
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//=====================================================================

#include "CalInfoDuplication.hh"

//@ Usage-Classes

#include "CalInfoFlashBlock.hh"
#include "CalInfoRefs.hh"
#include "NetworkApp.hh"
#include "Task.hh"
#include "NetworkSocket.hh"

#if defined(SIGMA_GUI_CPU)
# include "SerialNumber.hh"
# include "Configuration.hh"
# include "SmManager.hh"
# include "SmRefs.hh"
# include "BDIORefs.hh"
#endif // defined(SIGMA_GUI_CPU)

#if defined(SIGMA_BD_CPU)
# include "MsgQueue.hh"
# include "NetworkMsgId.hh"
# include "ServiceModeNovramData.hh"
#endif // defined(SIGMA_BD_CPU)


#ifdef SIGMA_UNIT_TEST
# include "CalInfoDuplication_UT.hh"
# include "FakeNetworkApp.hh"
#endif  // SIGMA_UNIT_TEST

//@ End-Usage


//@ Code...

#if defined(SIGMA_GUI_CPU)

Uint32 CalInfoDuplication::NumBytesReceived_ = 0 ;

#endif // defined(SIGMA_GUI_CPU)

// size of the data that is sent each block
static const Uint32 BLOCK_SIZE = 1024 ;

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: CalInfoDuplication()  
//
//@ Interface-Description
//  Constructor.
//-----------------------------------------------------------------------
//@ Implementation-Description
//	Check pre-condition.  No arguments.
//-----------------------------------------------------------------------
//@ PreCondition
//	Maximum data block size <= maximum network buffer size
//-----------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=======================================================================
CalInfoDuplication::CalInfoDuplication( void)
{
// $[TI1]
	CALL_TRACE("CalInfoDuplication::CalInfoDuplication( void)") ;

	CLASS_PRE_CONDITION( BLOCK_SIZE <= MAX_NETWORK_BUF_SIZE) ;
}


// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: ~CalInfoDuplication()  [Destructor]
//
//@ Interface-Description
//	Destructor.
//-----------------------------------------------------------------------
//@ Implementation-Description
//-----------------------------------------------------------------------
//@ PreCondition
//  none
//-----------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=======================================================================
CalInfoDuplication::~CalInfoDuplication( void)
{
	CALL_TRACE("CalInfoDuplication::~CalInfoDuplication( void)") ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Initialize()
//
//@ Interface-Description
//	This static method is used to register the callback which allows
//	the GUI to request then receive the calibration info from the BD.
//  No arguments and no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Register callback.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
CalInfoDuplication::Initialize( void)
{
// $[TI1]
	CALL_TRACE("CalInfoDuplication::Initialize( void)") ;

	NetworkApp::RegisterRecvMsg( EXH_VALVE_PRESS_MSG,
    	CalInfoDuplication::ProcessCalInfoFlashDataMsg) ;

#ifdef SIGMA_UNIT_TEST
	CalInfoDuplication_UT::Initialized = TRUE;
#endif  // SIGMA_UNIT_TEST

}


#if defined(SIGMA_GUI_CPU)

// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: execute()
//
//@ Interface - Description
//	This method is used by the GUI only to initiate the copying of calibration
//	information from the BD to the GUI.  No arguments and no return value.
//-----------------------------------------------------------------------
//@ Implementation - Description
//	Send the request for calibration info to the BD, then wait until the
//	the data is transmitted or until a timeout occurs.  If the data was
//	transmitted successfully, program it into FLASH.
//-----------------------------------------------------------------------
//@ PreCondition
//	none
//-----------------------------------------------------------------------
//@ PostCondition
//	none
//@ End - Method
//=======================================================================

void
CalInfoDuplication::execute( void)
{
	CALL_TRACE("CalInfoDuplication::execute( void)") ;

	// send request for cal info to BD
	requestBdFlashData_() ;

	// Wait until cal data transfer is completed or until timeout occurs
	Int32 loopCounter = 0;
	while (	NumBytesReceived_ < sizeof( BDIOFlashMap) && loopCounter < 10)
	{
	// $[TI1]

		// delay here to allow other tasks to run.  Timeout after 10 delays
		// of 1 second.
#ifndef SIGMA_UNIT_TEST
		// Don't delay here during unit test for greater speed
		Task::Delay(1);
#endif  // SIGMA_UNIT_TEST

		loopCounter++;
	}

	// program data into FLASH if timeout didn't occur
	if (loopCounter < 10)
	{
	// $[TI2]
		Uint32 error = programFlash_() ;

		if (error)
		{
		// $[TI2.1]
			// error burning flash
			RSmManager.sendStatusToServiceData(
				SmStatusId::TEST_FAILURE, SmStatusId::CONDITION_2);
		}
		// $[TI2.2]
		// implied else: no error
	}
	else
	{
	// $[TI3]
		// did not receive flash data
		RSmManager.sendStatusToServiceData(
			SmStatusId::TEST_FAILURE, SmStatusId::CONDITION_1);
	}
}

#endif // defined(SIGMA_GUI_CPU)


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ProcessCalInfoFlashDataMsg()
//
//@ Interface-Description
//	This method is used on the BD to initiate calibration data transmission,
//	and on the GUI to receive the data.  It is invoked by the communication
//  link when an EXH_VALVE_PRESS_MSG message is received.  The method
//	takes as arguments the message Id, a pointer to the message data and
//	the message size.  It has no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	BD implementation:
//	Put request on the XMIT_EV_TABLE_TASK_Q to initiate the data
//	transmission.
//
//	GUI implementation:
//	Store the received data block into the FLASH image.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
CalInfoDuplication::ProcessCalInfoFlashDataMsg( XmitDataMsgId, void *pData, Uint)
{
	CALL_TRACE("CalInfoDuplication::ProcessCalInfoFlashData( XmitDataMsgId, \
				void *pData, Uint)") ;

// ***** GUI CODE *****
#if defined(SIGMA_GUI_CPU)

# ifdef SIGMA_DEBUG	
	printf( "GUI: Receiving CalInfoFlashData\n");
# endif // SIGMA_DEBUG	

	// setup pointer to FLASH image starting addr and pointer to
	// position within the image to store the next data block
	char *pFlashImage = (char *)&RCalInfoFlashBlock.FlashImage;
	char *pStart = pFlashImage + NumBytesReceived_;

	// If this is last data block, set the size variable to the
	// # of actual data bytes in the block
	Uint32 size = BLOCK_SIZE;
	if (NumBytesReceived_ + BLOCK_SIZE > sizeof (BDIOFlashMap))
	{
	// $[TI1]
		size = sizeof (BDIOFlashMap) - NumBytesReceived_;
	}
	// $[TI2]
	// implied else: not the last data block, so all data
	// in the block is valid (size = BLOCK_SIZE)
	
	// copy data into the FLASH image for later burning into FLASH
	memcpy (pStart, pData, size);
	
	NumBytesReceived_ += size;


// ***** BD CODE *****
#else

# ifdef SIGMA_DEBUG	
	printf( "BD: Notifying CalInfoFlashData xmit task\n") ;
# endif // SIGMA_DEBUG	

	const Uint32 MESSAGE = 1 ;	// assignment of MESSAGE is arbitrary
	
// $[TI3]
    // BD: notify the cal table xmit task to send data to GUI
    CLASS_ASSERTION( MsgQueue::PutMsg( XMIT_EV_TABLE_TASK_Q, MESSAGE) == Ipc::OK) ;
	UNUSED_SYMBOL(pData);	
	
#endif // defined(SIGMA_GUI_CPU)
	
}


#if defined(SIGMA_BD_CPU)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  sendCalInfoFlashData()
//
//@ Interface-Description
//	This method is used by the BD only to transmit the calibration info
//	to the GUI.  A callback is used to signal the GUI to process each
//	data block sent.  No arguments and no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Send the BD calibration data to the GUI one block at a time.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
CalInfoDuplication::sendCalInfoFlashData( void)
{
	CALL_TRACE("CalInfoDuplication::sendCalInfoFlashData( void)") ;

#ifdef SIGMA_DEBUG
	printf( "BD: Sending CalData over\n") ;
#endif // SIGMA_DEBUG	

	char *pFlashImage = (char *)RCalInfoFlashBlock.getPFlashMap() ;

	for ( Uint32 bytesSent = 0;
		  bytesSent < sizeof (BDIOFlashMap);
		  bytesSent += BLOCK_SIZE )
	{
	// $[TI1]
		// update data block pointer
		char *pStart = pFlashImage + bytesSent ;

		// send data block to GUI
	    NetworkApp::XmitMsg( EXH_VALVE_PRESS_MSG, pStart, BLOCK_SIZE) ;

		// delay to prevent too much data being sent over all at once
		// to NetworkApp task.
	    Task::Delay( 0, 50) ;

#ifdef SIGMA_UNIT_TEST
		// flush the xmit table task Q during unit test only
		MsgQueue XmitEvTableMsgQue(::XMIT_EV_TABLE_TASK_Q);
		XmitEvTableMsgQue.flush();

		CalInfoDuplication_UT::SendLoopExecuted = TRUE;
#endif  // SIGMA_UNIT_TEST

	}
	// end for
}

#endif // defined(SIGMA_BD_CPU)


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  useDefaultEvTable()
//
//@ Interface-Description
//	This method is used to create a default exh valve table for
//	development/test purposes.  No argument, no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Create and store a default exh valve table and set the BD RAM and
//	NOVRAM exh valve cal status flags to indicate a valid calibration.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
CalInfoDuplication::useDefaultEvTable( void)
{
	CALL_TRACE("CalInfoDuplication::useDefaultEvTable( void)") ;
	
	// EV command count vs pressure default gain and offset:
	const Real32 DEFAULT_EV_GAIN = 2.41;  // DAC counts per 0.1 cmH2O
	const Real32 DEFAULT_EV_OFFSET = 161; // DAC counts

	// EV loopback vs command count default gain and offset:
	const Real32 DEFAULT_EV_CURRENT_GAIN = 0.210402; // ADC counts per DAC count
	const Real32 DEFAULT_EV_CURRENT_OFFSET = -30.0;  // ADC counts

	static 	ExhValveTable evTable ;

	evTable.initializeToDefault();


    RCalInfoFlashBlock.FlashImage.exhValveCalInfo = evTable ;
	RCalInfoFlashBlock.FlashImage.exhValveCalInfoSignature =
					CalInfoFlashSignature::EXH_VALVE_SIGNATURE;

	RCalInfoFlashBlock.FlashImage.exhValveCalInfoCrc =
			CalculateCrc(
			(Byte *) &(RCalInfoFlashBlock.FlashImage.exhValveCalInfo),
			sizeof (ExhValveTable) );

// Don't burn flash during unit test!!
#ifndef SIGMA_UNIT_TEST
    RCalInfoFlashBlock.programFlashBlock();
#endif  // SIGMA_UNIT_TEST

#if defined(SIGMA_BD_CPU)
	ServiceModeNovramData::SetExhValveCalStatus(ServiceMode::PASSED);
	ServiceMode::SetExhValveCalValid(TRUE);
#endif // defined(SIGMA_BD_CPU)
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
CalInfoDuplication::SoftFault( const SoftFaultID  softFaultID,
                   		const Uint32       lineNumber,
		   				const char*        pFileName,
		   				const char*        pPredicate)
{
	CALL_TRACE("CalInfoDuplication::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  	FaultHandler::SoftFault( softFaultID, SERVICE_MODE, CALINFODUPLICATION,
  							 lineNumber, pFileName, pPredicate) ;
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//=====================================================================
//
//  Private Methods...
//
//=====================================================================

#if defined(SIGMA_GUI_CPU)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: programFlash_()
//
//@ Interface-Description
//	This method is used by the GUI only to program the calibration
//	information from the FLASH image into FLASH. No arguments, 
//	returns an error code indicating status of FLASH programming.
//---------------------------------------------------------------------
//@ Implementation-Description
//	First copy the serial number into the FLASH image, then program the
//	CalInfoFlashBlock into FLASH from the image.  Return FLASH programming
//	status.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Uint32
CalInfoDuplication::programFlash_( void)
{
	CALL_TRACE("CalInfoDuplication::programFlash_( void)") ;

	Uint32 error = 0 ;

	// copy serial number in flash to image
	SerialNumber flashSerNum = RSystemConfiguration.getFlashSerialNumber() ;

#ifdef SIGMA_UNIT_TEST
	flashSerNum = CalInfoDuplication_UT::MySerialNumber;
#endif  // SIGMA_UNIT_TEST

	char *pStart = (char *)&RCalInfoFlashBlock.FlashImage.serialNumber ;

	for (Uint32 ii=0; ii < SERIAL_NO_SIZE; ii++)
	{
	// $[TI1]
		*(pStart + ii) = flashSerNum[ii] ;
	}
		
// Don't burn flash during unit test!!
#ifndef SIGMA_UNIT_TEST

	error = RCalInfoFlashBlock.programFlashBlock();

#else
	error = CalInfoDuplication_UT::FlashProgrammingError;

#endif  // SIGMA_UNIT_TEST

	return (error);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: requestBdFlashData_()
//
//@ Interface-Description
//	This method is used by the GUI only to request calibration info
//	transmission from the BD, which is performed via a callback using
//	the message ID EXH_VALVE_PRESS_MSG.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Initiate the bytes received counter to 0 then signal the BD to
//	to begin data transmission.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
CalInfoDuplication::requestBdFlashData_( void)
{
// $[TI1]
	CALL_TRACE("CalInfoDuplication::requestBdFlashData_( void)") ;

#ifdef SIGMA_DEBUG
	printf( "Sending request for flash data\n") ;
#endif // SIGMA_DEBUG
	
	NumBytesReceived_ = 0 ;

    NetworkApp::XmitMsg (EXH_VALVE_PRESS_MSG, NULL, 0) ;

#ifdef SIGMA_UNIT_TEST
	NumBytesReceived_ = CalInfoDuplication_UT::SimulatedNumBytesRcvd;
#endif  // SIGMA_UNIT_TEST

}

#endif // defined(SIGMA_GUI_CPU)
