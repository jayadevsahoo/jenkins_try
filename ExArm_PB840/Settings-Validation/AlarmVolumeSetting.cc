#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//============== C L A S S   D E S C R I P T I O N ====================
//@ Class:  AlarmVolumeSetting - Alarm Volume Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a non-batch, sequential setting that contains the volume level
//  of the alarms.  This class inherits from 'NonBatchSequentialSetting' and
//  provides the specific information needed for representation of the alarm
//  volume.  This information includes the interval and range of this
//  setting's values, and this non-batch setting's default value (see
//  'getDefaultValue()').
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class contains a sequential range class ('SequentialRange')
//  to manage the resolution and range of this setting.  This class has NO
//  dependent settings, or dynamic bounds.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/AlarmVolumeSetting.ccv   25.0.4.0   19 Nov 2013 14:27:10   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added 'getApplicability()' method
//
//  Revision: 003   By: sah  Date:  11-Jan-1999    DR Number:  5321
//  Project:  ATC
//  Description:
//	As part of making all 'SoftFault()' methods non-inlined, I'm
//	also making those 'SoftFault()' methods that are NOT called
//	in PRODUCTION mode, DEVELOPMENT-only.
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Initial version.
//
//=====================================================================

#include "AlarmVolumeSetting.hh"
#include "Sound.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Static Data Definitions...
//
//=====================================================================

static const Uint  MAX_ALARM_VOLUME_VALUE_ = Sound::MAX_ALARM_VOL;
static const Uint  MIN_ALARM_VOLUME_VALUE_ = Sound::MIN_ALARM_VOL;


//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  AlarmVolumeSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02036] -- The setting's range ...
//  $[02038] -- The setting's resolution ...
//  $[02039] -- The setting's purpose ...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

AlarmVolumeSetting::AlarmVolumeSetting(void)
	 : NonBatchSequentialSetting(SettingId::ALARM_VOLUME,
				     Setting::NULL_DEPENDENT_ARRAY_,
	 			     sequentialRange_),

		    sequentialRange_(::MAX_ALARM_VOLUME_VALUE_,
				     ALARM_VOL_MAX_ID,	// minConstraintId...
				     1,			// resolution...
				     ALARM_VOL_MIN_ID,	// maxConstraintId...
				     ::MIN_ALARM_VOLUME_VALUE_)
{
  CALL_TRACE("AlarmVolumeSetting()");
}   // $[TI1]


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method:  ~AlarmVolumeSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

AlarmVolumeSetting::~AlarmVolumeSetting(void)
{
  CALL_TRACE("~AlarmVolumeSetting(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This setting is ALWAYS changeable.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
AlarmVolumeSetting::getApplicability(const Notification::ChangeQualifier) const
{
  CALL_TRACE("getApplicability(qualifierId)");

  return(Applicability::CHANGEABLE);
}   // $[TI1]


//============== M E T H O D   D E S C R I P T I O N ================
//@ Method:  calcNewValue(knobDelta)  [virtual]
//
//@ Interface-Description
//  This method determines the new setting value from the knob delta, while
//  also checking the new value against its bounds.  A constant reference to
//  this setting's bound status is returned; the bound status will contain
//  bound information about this setting's violated bound, if any.
//
//  This method is overridden by this class to enhance the modifying of
//  the alarm volume.  Since this setting has only eight possible values,
//  a user would only have to turn the knob a very short distance to cover
//  the entire volume range.  Therefore, this method is overridden to
//  remove any acceleration from the knob delta, and make sure that only
//  changes of magnitude one are performed.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

const BoundStatus&
AlarmVolumeSetting::calcNewValue(const Int16 knobDelta)
{
  CALL_TRACE("calcNewValue(knobDelta)");

  Int16  appliedKnobDelta = knobDelta;

  if (appliedKnobDelta > 1)
  {   // $[TI1] -- "clip" this positive knob delta to '1'...
    appliedKnobDelta = 1;
  }
  else if (appliedKnobDelta < -1)
  {   // $[TI2] -- "clip" this negative knob delta to '-1'...
    appliedKnobDelta = -1;
  }   // $[TI3] -- no "clipping" needed...

  // forward request to the base class's method...
  return(SequentialSetting::calcNewValue(appliedKnobDelta));
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getDefaultValue()  [virtual]
//
//@ Interface-Description
//  Return this non-batch setting's default value.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
AlarmVolumeSetting::getDefaultValue(void) const
{
  CALL_TRACE("getDefaultValue()");

  // $[02037] -- use maximum value as default value...
  const SequentialValue  DEFAULT_VALUE = ::MAX_ALARM_VOLUME_VALUE_;

  return(DEFAULT_VALUE);
}   // $[TI1]


#if defined(SIGMA_DEVELOPMENT)

//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//			[static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
AlarmVolumeSetting::SoftFault(const SoftFaultID  softFaultID,
			      const Uint32       lineNumber,
			      const char*        pFileName,
			      const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
                          ALARM_VOLUME_SETTING, lineNumber, pFileName,
                          pPredicate);
} 

#endif // defined(SIGMA_DEVELOPMENT)
