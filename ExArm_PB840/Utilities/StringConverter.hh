//----------------------------------------------------------------------------
//            Copyright (c) 2014 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------


/*! \file StringConverter.hh
    \brief This file contains String conversion API.
*/

#ifndef STRING_CONVERTER_H_
#define STRING_CONVERTER_H_

#include<string>
#include"SigmaTypes.hh"
#include "FaultHandlerMacros.hh"


/*! \class StringConverter
    \brief This class provides multibyte to wide character string conversion APIs 
			and vice versa
	
	The StringConverter class provides two APIs ToString and ToWString
	to convert char* strings to wchar_t* strings and vice versa.
*/

class StringConverter
{
public:
	/*! \fn static void ToWString( wchar_t* pDestWcharStr, UInt32 iMaxDestSize, const char* pSourceChrStr )
		\brief Converts narrow-charaters (i.e.char) string to wide-character string.
		\param[in]	pSourceChrStr - source character string
		\param[in]	iMaxDestSize -  Maximum destination buffer size
		\param[in]	iDestCount - 
		\param[out] pDestWcharStr - destination wide character string
		\return		none
	*/
	static void ToWString( wchar_t* pDestWcharStr, Uint32 iMaxDestSize, const char* pSourceChrStr );

	/*! \fn static void ToString( char* pDestCharStr, UInt32 iMaxDestSize, const wchar_t* pSourceWcharStr )
		\brief Converts wide-character(i.e.wchar_t) string to narrow-charaters string.
		\param[in]	pSourceChrStr - source character string
		\param[in]	iMaxDestSize -  Maximum destination buffer size
		\param[out] pDestWcharStr - destination wide character string
		\return		none
	*/
	static void ToString( char* pDestCharStr, Uint32 iMaxDestSize, const wchar_t* pSourceWcharStr );

	/*! \fn static void  SoftFault(const SoftFaultID softFaultID,
							const Uint32      lineNumber,
							const Char*       pFileName  = NULL, 
							const Char*       pPredicate = NULL)
		\brief  This method is called when a software fault is detected by the
				fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
				and 'lineNumber' are essential pieces of information.  The
				'pFileName' and 'pPredicate' strings may be defaulted in the macro
				to reduce code space.
		\param[in]	softFaultID - The ID of the softFault
		\param[in]	lineNumber - lineNumber The line number of the file
		\param[in]	pFileName -  The filename string
		\param[in]	pPredicate - The boolean expression causing the fault
		\param[out] none
		\return		none
	*/
	
	static void  SoftFault(const SoftFaultID softFaultID,
							const Uint32      lineNumber,
							const Char*       pFileName  = NULL, 
							const Char*       pPredicate = NULL);
private:
	//! A constructor.
    /*!
      Declared only
    */
	
	StringConverter(void);	// not implemented...

	//! A destructor.
    /*!
      Declared only
    */
	~StringConverter(void);	// not implemented...

	//! A copy constructor.
    /*!
      Declared only
    */
	StringConverter(StringConverter&); // not implemented...

	/*! \fn void operator=(const StringConverter&)
		\brief  Assignment operator, Declared only.
	*/
	void operator=(const StringConverter&); 

};


#endif //STRING_CONVERTER_H_