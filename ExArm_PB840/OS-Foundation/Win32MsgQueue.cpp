#include "stdafx.h"
/* 
This file has implementation for the Windows Message Queue APIs.
The Microsoft's implementation is only available for Windows CE environment
This implementation was created as as to keep our source code the same for 
both desktop and target environments.
*/
#if !defined(_WIN32_WCE) && defined(_WIN32)

#include <afxmt.h>
#include <list>
#include <map>
#include "Sigma.hh"

#if !defined(UNUSED_VAR)
#define UNUSED_VAR(x) do { (void)(x); } while(0)
#endif


template <class MessageType>
class Win32MsgQueue
{
    public:
        Win32MsgQueue(Int32 id, const std::wstring& name, DWORD size, bool closeOnZero=false)
            :m_id(id), m_name(name), m_closeOnZeroCount(closeOnZero), m_maxSize(size), m_semaphore(0, size)
        {
        }
        ~Win32MsgQueue()
        {
            //CLASS_ASSERTION(m_referenceCount == 0);
        }
        bool open()
        {
			m_cs.Lock();
            m_referenceCount++;
			m_cs.Unlock();
            return true;
        }
        bool close()
        {
			m_cs.Lock();
            m_referenceCount--;
            if(m_referenceCount == 0 && m_closeOnZeroCount)
            {
                m_queue.clear();
                m_id = -1;
                //We need to signal the threads that are waiting
                //that we are closing the queue. For now, we are
                //overlooking that case
            }
			m_cs.Unlock();
            return true;
        }

        bool read(MessageType& msg, Int32 timeout=0)
        {
            bool ret = false;
            DWORD t = timeout;
            if(timeout == 0)
                t = INFINITE;
            if(m_semaphore.Lock(t) != 0)
            {
                //Got a message in the queue.
                //First, enter the critical section to prevent multiple
                //threads reading from the list at the same time.
                if(m_cs.Lock(INFINITE) != 0)
                {
                    if(m_queue.size() > 0)
					{
                        msg = m_queue.front();
						m_queue.pop_front();
						ret = true;
					}
                    m_cs.Unlock();
                }
            }
			else
			{
				SetLastError(ERROR_TIMEOUT);
			}
			return ret;
        }
        bool peek(MessageType& msg)
        {
            bool ret = false;
			//Do not wait long
            if(m_semaphore.Lock(1) != 0)
            {
                //Got a message in the queue.
                //First, enter the critical section to prevent multiple
                //threads reading from the list at the same time.
                if(m_cs.Lock(INFINITE) != 0)
                {
                    if(m_queue.size() > 0)
					{
                        msg = m_queue.front();
						//Do not delete.. just return what's in front
						ret = true;
					}
                    m_cs.Unlock();
                }
            }
			else
			{
				SetLastError(ERROR_TIMEOUT);
			}
			return ret;
        }
        bool write(const MessageType& msg)
        {
            bool ret = false;
            //Insert a message into the queue.
            //First, enter the critical section to prevent multiple
            //threads writing to the list at the same time.
            if(m_cs.Lock(INFINITE) != 0)
            {
				if(m_queue.size() < m_maxSize)
				{
					m_queue.push_back(msg);
					//Wake up anyone who may be waiting for a message..
					m_semaphore.Unlock();
					ret = true;
				}
				else
				{
					SetLastError(ERROR_INSUFFICIENT_BUFFER);
				}
				m_cs.Unlock();
            }
			return ret;
        };

        const std::wstring& getName() { return m_name; }
        const Int32 getId() { return m_id; }
		const Int32 getCurrentMsgCount() { return m_queue.size(); }

    protected:
        Int32 m_id;
        std::wstring m_name;
        Int32 m_referenceCount;
        bool m_closeOnZeroCount;
		CCriticalSection m_cs;
		DWORD m_maxSize;
		CSemaphore m_semaphore;
        std::list<MessageType> m_queue;
};


typedef Win32MsgQueue<Int32> PB840MsgQueue;
typedef std::map<Int32,PB840MsgQueue*> MsgQueueMap;

static MsgQueueMap gMsgQueueMap;
static CCriticalSection gMsgQueMapLock;


static Int32 gQId = 0;
BOOL gShutdownRequested = FALSE;

HANDLE WINAPI CreateMsgQueue(LPCWSTR lpName, LPMSGQUEUEOPTIONS lpOptions)
{
	if(gShutdownRequested) return NULL;
    //The queue doesnt exist.. create it.
    std::wstring qName = lpName;
	if(qName.compare(L"") == 0)
		return NULL;

	HANDLE ret = NULL;
    gMsgQueMapLock.Lock();
    MsgQueueMap::iterator iter = gMsgQueueMap.begin();
	//Check if the queue exists already
	while(iter != gMsgQueueMap.end())
	{
		if(iter->second->getName().compare(qName) == 0)
			break;
		iter++;
	}
    if(iter == gMsgQueueMap.end())
    {
        Int32 qId = ++gQId;
		PB840MsgQueue* queue = new PB840MsgQueue(qId, qName, lpOptions->dwMaxMessages);
        gMsgQueueMap.insert(std::pair<Int32,PB840MsgQueue*>(qId, queue));
        ret = (HANDLE)qId;
    }
    else
    {
		//Asking to create handle to an existing queue.. just return the same one,
		//after "opening" again.
		iter->second->open();
		ret = (HANDLE)(iter->second->getId());
    }

    gMsgQueMapLock.Unlock();
    return ret;
}

BOOL WINAPI ReadMsgQueue(HANDLE hMsgQ, __out_bcount(cbBufferSize) LPVOID lpBuffer, DWORD cbBufferSize,LPDWORD lpNumberOfBytesRead, DWORD dwTimeout, DWORD *pdwFlags)
{
	UNUSED_VAR(pdwFlags);
	UNUSED_VAR(cbBufferSize);

	if(gShutdownRequested) return FALSE;
    BOOL ret = FALSE;
	PB840MsgQueue* queue = NULL;
    Int32 qId = (Int32)hMsgQ;

	gMsgQueMapLock.Lock();
    MsgQueueMap::iterator iter = gMsgQueueMap.find(qId);
	if(iter != gMsgQueueMap.end())
        queue = iter->second;

    gMsgQueMapLock.Unlock();
	//This can be a problem if we also supported dynamic deleting of
	//queue instances, because we are holding onto the queue instance
	//without a lock to the map. The reason why we cant hold the lock is that
	//it would prevent anyone else from accessing any other queue while the write
	//is in progress. 
	//The queue itself has built-in protection for multi-thread access, but 
	//if we were deleting the queue, there is not protection. It is Ok here, 
	//because we arent deleting any queue in 880 code.
	if(queue != NULL)
	{
        Int32& msg = *(Int32*)lpBuffer;
        ret = queue->read(msg, dwTimeout);
		if(ret)
			*lpNumberOfBytesRead = sizeof(Int32);
	}
	else
	{
		SetLastError(ERROR_PIPE_NOT_CONNECTED);
	}
    return ret;
}
BOOL WINAPI WriteMsgQueue(HANDLE hMsgQ, LPVOID lpBuffer, DWORD cbDataSize,DWORD dwTimeout, DWORD dwFlags)
{
	UNUSED_VAR(dwFlags);
	UNUSED_VAR(dwTimeout);
	UNUSED_VAR(cbDataSize);

	if(gShutdownRequested) return FALSE;
    BOOL ret = FALSE;
	PB840MsgQueue* queue = NULL;
    Int32 qId = (Int32)hMsgQ;

	gMsgQueMapLock.Lock();
    MsgQueueMap::iterator iter = gMsgQueueMap.find(qId);
	if(iter != gMsgQueueMap.end())
        queue = iter->second;
    gMsgQueMapLock.Unlock();

	//Like the ReadMsgQueue API here, the following code can be trouble if we are
	//to support dynamic delete of msg queues.
	if(queue != NULL)
	{
        Int32& msg = *(Int32*)lpBuffer;
        ret = queue->write(msg);
    }
	else
	{
		SetLastError(ERROR_PIPE_NOT_CONNECTED);
	}
    return ret;
}

BOOL WINAPI CloseMsgQueue(HANDLE hMsgQ)
{
	if(gShutdownRequested) return FALSE;
    BOOL ret = FALSE;
    gMsgQueMapLock.Lock();
    Int32 qId = (Int32)hMsgQ;
    MsgQueueMap::iterator iter = gMsgQueueMap.find(qId);
    if(iter != gMsgQueueMap.end())
    {
        ret = iter->second->close();
    }
	else
	{
		SetLastError(ERROR_PIPE_NOT_CONNECTED);
	}

    gMsgQueMapLock.Unlock();
    return ret;
}

BOOL WINAPI GetMsgQueueInfo(HANDLE hMsgQ, LPMSGQUEUEINFO lpInfo)
{
	if(gShutdownRequested) return FALSE;
    BOOL ret = FALSE;
    gMsgQueMapLock.Lock();
    Int32 qId = (Int32)hMsgQ;
    MsgQueueMap::iterator iter = gMsgQueueMap.find(qId);
    if(iter != gMsgQueueMap.end())
    {
        //Ideally we should set the rigth values, but for this implementation
        //just set everything to zero
        memset(lpInfo, 0, sizeof(MSGQUEUEINFO));
		lpInfo->dwCurrentMessages = iter->second->getCurrentMsgCount();
		ret = TRUE;
    }
	else
	{
		SetLastError(ERROR_PIPE_NOT_CONNECTED);
	}

    gMsgQueMapLock.Unlock();
    return ret;
}

BOOL WINAPI PeekAtMsg(HANDLE hMsgQ, __out_bcount(cbBufferSize) LPVOID lpBuffer, DWORD cbBufferSize,LPDWORD lpNumberOfBytesRead, DWORD dwTimeout, DWORD *pdwFlags)
{
	UNUSED_VAR(pdwFlags);
	UNUSED_VAR(dwTimeout);
	UNUSED_VAR(cbBufferSize);

	if(gShutdownRequested) return FALSE;
    BOOL ret = FALSE;
	PB840MsgQueue* queue = NULL;
    Int32 qId = (Int32)hMsgQ;

	gMsgQueMapLock.Lock();
    MsgQueueMap::iterator iter = gMsgQueueMap.find(qId);
	if(iter != gMsgQueueMap.end())
        queue = iter->second;

    gMsgQueMapLock.Unlock();
	//This can be a problem.. See comment inside ReadMsgQueue
	if(queue != NULL)
	{
        Int32& msg = *(Int32*)lpBuffer;
        ret = queue->peek(msg);
		if(ret)
			*lpNumberOfBytesRead = sizeof(Int32);
	}
	else
	{
		SetLastError(ERROR_PIPE_NOT_CONNECTED);
	}
    return ret;
}
#endif
