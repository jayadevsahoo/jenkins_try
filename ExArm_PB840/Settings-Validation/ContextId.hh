
#ifndef ContextId_HH
#define ContextId_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  ContextId - Ids for the Contexts.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/ContextId.hhv   25.0.4.0   19 Nov 2013 14:27:18   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Initial version.
//  
//====================================================================

#include "Sigma.hh"
#include "SettingClassId.hh"

//@ Usage-Classes
//@ End-Usage


class ContextId
{
  public:
    //@ Type:  ContextIdType
    // Ids of all of the contexts.
    enum ContextIdType
    {
#if defined(SIGMA_GUI_CPU)  ||  defined(SIGMA_COMMON)
      ACCEPTED_CONTEXT_ID,
      ADJUSTED_CONTEXT_ID,
      RECOVERABLE_CONTEXT_ID,
#endif // defined(SIGMA_GUI_CPU)  ||  defined(SIGMA_COMMON)

#if defined(SIGMA_BD_CPU)  ||  defined(SIGMA_COMMON)
      PENDING_CONTEXT_ID,
      PHASED_IN_CONTEXT_ID,
#endif // defined(SIGMA_BD_CPU)  ||  defined(SIGMA_COMMON)

      TOTAL_CONTEXT_IDS
    };

    static inline Boolean  IsId(const ContextId::ContextIdType contextId);

#if defined(SIGMA_DEVELOPMENT)
    static const char*  GetContextName(const ContextId::ContextIdType contextId);
#endif  // defined(SIGMA_DEVELOPMENT)

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL,
			   const char*       pPredicate = NULL);
};


// Inlined Functions...
#include "ContextId.in"


#endif // ContextId_HH 
