
#ifndef BreathDatumHandle_HH
#define BreathDatumHandle_HH

#if		defined(SIGMA_GUI_CPU) || defined(SIGMA_COMMON)
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, NellCor/Puritan-Bennett Corporation
//=====================================================================

//====================================================================
//@ Class:  BreathDatumHandle -  Provides a dedicated data access handle 
//          for each specific breath data item type.
//---------------------------------------------------------------------
//
//@ Version
// @(#) $Header:   /840/Baseline/Patient-Data/vcssrc/BreathDatumHandle.hhv   25.0.4.0   19 Nov 2013 14:17:56   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah   Date:  13-Jan-99    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001  By:  jhv    Date:  16-Feb-95    DR Number: 
//    Project:  Sigma (R8027)
//    Description:
//        Initial version. 
//
//====================================================================

#include  "BreathDatum.hh"
#include  "PatientDataId.hh"
#include  "BreathItemDataInfo.hh"
#include  "PatientDataClassIds.hh"

class BreathDatumHandle
{
  public:
    BreathDatumHandle(const PatientDataId::PatientItemId breathDataId);
    ~BreathDatumHandle(void);
    inline const BoundedBreathDatum &getBoundedValue();
    inline const DiscreteBreathDatum &getDiscreteValue();

    static void  SoftFault(const SoftFaultID softFaultID,
						   const Uint32      lineNumber,
						   const char*       pFileName  = NULL, 
						   const char*       pPredicate = NULL);

  private:
    BreathDatumHandle(void);                     // not implemented...
    BreathDatumHandle(const BreathDatumHandle&); // not implemented...
    void operator=(const BreathDatumHandle&);  // not implemented.

   //@Data-Member : pAgent_;
   // A pointer to a data processing agent object for the breath item
   // data type being handled.
    BreathItemDataInfo *pAgent_;
};


// Inlined methods...
#include "BreathDatumHandle.in"
#endif		//defined(SIGMA_GUI_CPU) || defined(SIGMA_COMMON)

#endif // BreathDatumHandle_HH 
