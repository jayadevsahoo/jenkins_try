#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: PrintTarget -  Printing callbacks virtual base class
//---------------------------------------------------------------------
//@ Interface-Description 
//  PrintTarget is a virtual base class that declares the methods
//  required for implementing printing callbacks in any class. This
//  class is inherited by the WaveformsSubScreen and TrendSubScreen to
//  receive printing callbacks from GuiApp::TaskExecutor(). The printing
//  sequence is controlled by the TaskExecutor and communicated to
//  PrintTarget objects using these methods. PrintTarget objects
//  register for these callbacks with GuiApp using its SetPrintTarget()
//  method.
//---------------------------------------------------------------------
//@ Rationale 
//  Single class encapsulates printing callbacks. 
//---------------------------------------------------------------------
//@ Implementation-Description 
//  The printing callback methods are declared as pure virtual
//  methods. The derived classes must define each of these methods.
//---------------------------------------------------------------------
//@ Fault-Handling 
//  none 
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/PrintTarget.ccv   25.0.4.0   19 Nov 2013 14:08:16   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By: gdc      Date:  15-Mar-2007   SCR Number: 6237
//  Project:  Trend
//  Description:
//  Initial Version
//=====================================================================

#include "PrintTarget.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

// Initialize static constants.

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: PrintTarget()  [Constructor]
//
//@ Interface-Description 
//  Constructor. 
//---------------------------------------------------------------------
//@ Implementation-Description 
//  none 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

PrintTarget::PrintTarget(void) 
{
	CALL_TRACE("PrintTarget::PrintTarget(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~PrintTarget  [Destructor]
//
//@ Interface-Description
// Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

PrintTarget::~PrintTarget(void)	
{
	CALL_TRACE("PrintTarget::~PrintTarget(void)");
}
