#ifndef TestData_IN
#define TestData_IN

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
// Class: TestData - Compute processed sensor data
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/TestData.inv   25.0.4.0   19 Nov 2013 14:23:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 003  By:  syw    Date: 11-Aug-1997    DR Number: DCS 2356
//       Project:  Sigma (840)
//       Description:
//            Defined resetPeakPexh() and getPeakPexh() methods.
//
//  Revision: 002  By:  quf    Date: 22-May-1997    DR Number: DCS 2131
//       Project:  Sigma (840)
//       Description:
//            Modification-Log fix only.
//
//  Revision: 001  By:  mad    Date: 21-Aug-1995    DR Number: none
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//=====================================================================

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: resetDeliveredVolume
//
//@ Interface-Description
//    This method takes no arguments and returns nothing.  It is used
//    to reset the delivered volume and net delivered volume.
//---------------------------------------------------------------------
//@ Implementation-Description
//    Set deliveredVolume_ and netDeliveredVolume_ to zero.
//---------------------------------------------------------------------
//@ PreCondition
//	  none
//---------------------------------------------------------------------
//@ PostCondition
//	  none
//@ End-Method
//=====================================================================

inline void
TestData::resetDeliveredVolume( void)
{
// $[TI1]
	CALL_TRACE("TestData::resetDeliveredVolume( void)");

	deliveredVolume_ = 0.0;
	netDeliveredVolume_ = 0.0;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getDeliveredVolume
//
//@ Interface-Description
//    This method takes no arguments and returns the delivered volume.
//---------------------------------------------------------------------
//@ Implementation-Description
//    Return deliveredVolume_
//---------------------------------------------------------------------
//@ PreCondition
//	  none
//---------------------------------------------------------------------
//@ PostCondition
//	  none
//@ End-Method
//=====================================================================

inline Real32
TestData::getDeliveredVolume( void)
{
// $[TI1]
	CALL_TRACE("getDeliveredVolume( void)");

	return( deliveredVolume_);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getNetDeliveredVolume
//
//@ Interface-Description
//    This method takes no arguments and returns the net delivered volume.
//---------------------------------------------------------------------
//@ Implementation-Description
//    Return netDeliveredVolume_
//---------------------------------------------------------------------
//@ PreCondition
//	  none
//---------------------------------------------------------------------
//@ PostCondition
//	  none
//@ End-Method
//=====================================================================

inline Real32
TestData::getNetDeliveredVolume( void)
{
// $[TI1]
	CALL_TRACE("getNetDeliveredVolume( void)");

	return( netDeliveredVolume_);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: resetPeakPexh
//
//@ Interface-Description
//		This has no arguments and has no return value.  This method is called
//		to reset the peak pressure as seen by the expiratory pressure sensor to
//		zero.
//---------------------------------------------------------------------
//@ Implementation-Description
//    	Sets peakPexh_ data member to zero.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//	  none
//@ End-Method
//=====================================================================
inline void
TestData::resetPeakPexh( void)
{
// $[TI1]
	CALL_TRACE("TestData::resetPeakPexh( void)") ;

	peakPexh_ = 0.0 ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getPeakPexh
//
//@ Interface-Description
//		This method has no arguments and returns the peak pressure as seen
//		by the epiratory pressure sensor.
//---------------------------------------------------------------------
//@ Implementation-Description
//    	Returns the data member peakPexh_.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//	  none
//@ End-Method
//=====================================================================
inline Real32
TestData::getPeakPexh( void) const
{
// $[TI1]
	CALL_TRACE("TestData::getPeakPexh( void)") ;

	return( peakPexh_) ;
}

//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//=====================================================================
//
//  Private Methods...
//
//=====================================================================


#endif // TestData_IN
