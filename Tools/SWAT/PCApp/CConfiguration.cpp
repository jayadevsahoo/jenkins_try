//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file CConfiguration.cpp
//----------------------------------------------------------------------------


//-------------------------------------
// I N C L U D E S
//-------------------------------------
#define _CRT_SECURE_NO_WARNINGS

#include"CConfiguration.h"
#include "XMLUtil.h"
#include "SettingXmitData.h"
#include "CLogger.h"
#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/dom/DOMDocument.hpp>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <Winsock2.h>
using namespace XERCES_CPP_NAMESPACE;


#define _CRT_SECURE_NO_WARNINGS

#include "ICConfiguration.h"
#include "ISettingsCapture.h"
#include <sys/types.h>

#include <Winsock2.h>
#include <ws2tcpip.h>

#include <cstring>
#include <cstdio>
#include <cstdlib>
#include <string>
#include "pav.h"

#include "CLogger.h"

#ifndef socklen_t
#define socklen_t unsigned int
#endif


//-------------------------------------
// C O D E
//-------------------------------------

//---------------------------
// ~IConfiguration
//---------------------------
IConfiguration::~IConfiguration(){
}

//---------------------------
// destroy
//---------------------------
void CConfiguration::destroy(){
    delete this;
}

//---------------------------
// CConfiguration
//---------------------------
CConfiguration::CConfiguration()
{

}

//---------------------------
// ~CConfiguration
//---------------------------
CConfiguration::~CConfiguration()
{

}


//---------------------------
// readContext
//---------------------------
const bool
CConfiguration::readContext (const char *ptr, const int iSize) {


    std::map < int, CParameter * >pV;
    char pszBuffer[100];

    struct NetworkMsgHeader *phdr;
    struct SettingXactionBlock *pXaction;

    phdr = (struct NetworkMsgHeader *) ptr;
    ptr += sizeof (struct NetworkMsgHeader);

    pXaction = (struct SettingXactionBlock *) ptr;

    int iExpected = ntohs (pXaction->numXmitElems)
        * sizeof (SettingXmitData)
        + sizeof (SettingXactionBlock) + sizeof (NetworkMsgHeader);


    if (iExpected != iSize) {

        LOG_MESSAGE(LM_CRITICAL,"CConfiguration::readContext() BARF.");
        //fprintf (stdout, "Size Mismatch %i %i\n",iExpected,iSize);
        return false;
    }

    int iXactionId = (int)ntohs(pXaction->xactionId);

    if (iXactionId == 12){
   
        char xptr[10000];
        char *ptr=xptr;
        int iRet;

        struct NetworkMsgHeader *phdr;
        struct SettingXactionBlock *pXaction_old;

        iRet = makeContext (ptr, 10000);

        phdr = (struct NetworkMsgHeader *) ptr;
        ptr += sizeof (struct NetworkMsgHeader);
        pXaction_old = (struct SettingXactionBlock *) ptr;

        if(iRet != -1) {

            int max_cnt = (int)ntohs (pXaction->numXmitElems);

            for(int i=0;i<max_cnt;i++){
                int arr_id;
                unsigned int id, old_id;

                id     =(int)ntohs (pXaction    ->arrXmitElems[i ].settingXmitId);
                arr_id= xlateXidToId(id);

                if(arr_id == -1){
                    return false;
                }

                old_id =(int)ntohs (pXaction_old->arrXmitElems[arr_id].settingXmitId); 
                
                if(id != old_id){
                    return false;
                }

                pXaction_old->arrXmitElems[arr_id].discreteValue
                    =pXaction->arrXmitElems[i].discreteValue;
            }

            readContext (xptr, iRet); 
        }

        return true;
    }


    std::set < boost::shared_ptr < CParameter >,
        CParameterOps >::const_iterator cii;
    for (cii = m_CParameterSet.begin (); cii != m_CParameterSet.end (); cii++) {
        pV[cii->get ()->getId ()] = cii->get ();
    }


    std::map < int, CParameter * >::iterator ciiv;
    int i;
    for (ciiv = pV.begin (), i = 0;
         (ciiv != pV.end ()) && (i < ntohs (pXaction->numXmitElems));
         ciiv++, i++) {

        unsigned int temp;
        temp = ntohl (pXaction->arrXmitElems[i].discreteValue);

        switch (ciiv->second->getTypeId ()) {
        case CParameter::FLOAT:
            sprintf_s (pszBuffer,sizeof(pszBuffer), "%f", *((float *) (&temp)));
            ciiv->second->updateAttribute ("default", pszBuffer);
            break;

        case CParameter::INT:
            sprintf_s (pszBuffer,sizeof(pszBuffer), "%f", *((int *) (&temp)));
            ciiv->second->updateAttribute ("default", pszBuffer);
            break;


        case CParameter::ENUM:
            const char *pTemp = ciiv->second->getEnumName (*((int *) (&temp)));
            if (pTemp) {
                ciiv->second->updateAttribute ("default", pTemp);
            }

            break;
        }
    }

    return true;
}

//---------------------------
// makeContext
//---------------------------
const int
CConfiguration::makeContext (char *pBuffer, const int iLength) const {



    int iMaxId = 0;
    int iMinId = 0xffffff;
    int iCurId;

    /*Determine Range of ID's */
      std::set < boost::shared_ptr < CParameter >,
        CParameterOps >::const_iterator cii;
    for (cii = m_CParameterSet.begin (); cii != m_CParameterSet.end ();
         cii++)
    {
        iCurId = cii->get ()->getId ();

        if (iCurId > iMaxId)
        {
            iMaxId = iCurId;
        }

        if (iCurId < iMinId)
        {
            iMinId = iCurId;
        }
    }


    //Compute context size
    int iRequiredMemory = (iMaxId + 1)
        * sizeof (SettingXmitData)
        + sizeof (SettingXactionBlock) + sizeof (NetworkMsgHeader);


    //Buffer to small
    if (iLength < iRequiredMemory) {
        LOG_MESSAGE(LM_CRITICAL,"CConfiguration(%X)::makeContext() BARF.",this);
        return -1;
    }


    char *ptr = pBuffer;

    struct NetworkMsgHeader *pHdr;
    struct SettingXactionBlock *pXaction;

    pHdr = (struct NetworkMsgHeader *) ptr;
    ptr += sizeof (struct NetworkMsgHeader);

    pXaction = (struct SettingXactionBlock *) ptr;


    for (cii = m_CParameterSet.begin (); cii != m_CParameterSet.end ();
         cii++) {
        int iCurId  = cii->get ()->getId  ();
        int iCurXId = cii->get ()->getXid ();
        int iTypeId = cii->get ()->getTypeId ();

        unsigned int uiVal;

        switch (iTypeId) {
        case CParameter::FLOAT:
            {

                float fVal;
                bool s = cii->get ()->getDefaultFloat (&fVal);
                uiVal = htonl (*((unsigned int *) (&fVal)));
                break;
            }

        case CParameter::INT:
            {
                int iVal;
                bool s = cii->get ()->getDefaultInt (&iVal);
                uiVal = htonl (*((unsigned int *) (&iVal)));
                break;
            }

        case CParameter::ENUM:
            int iVal;
            bool s = cii->get ()->getDefaultInt (&iVal);
            uiVal = htonl (*((unsigned int *) (&iVal)));
            break;
        }

        pXaction->arrXmitElems[iCurId].discreteValue = uiVal;
        pXaction->arrXmitElems[iCurId].boundedPrec =
            (CParameter::FLOAT == iTypeId);
        pXaction->arrXmitElems[iCurId].reserved = 0;
        pXaction->arrXmitElems[iCurId].settingXmitId = htons (iCurXId);
        pXaction->xactionId = htons (VENT_STARTUP_UPDATE_XMITTED);
        pHdr->msgId = htons (9906);
        pHdr->pktSize =
            htons (iRequiredMemory - sizeof (NetworkMsgHeader));
        pHdr->seqNumber = htonl (1);

    }

    pXaction->numXmitElems = htons (iMaxId + 1);


    return iRequiredMemory;
}

//---------------------------
// loadXMLDocument
//---------------------------
const bool
CConfiguration::loadXMLDocument (const char *fileName) {

    XMLPlatformUtils::Initialize ();

    //Load template document
    XMLUtil::DOMDocument * doc = XMLUtil::loadXMLDocument ((char *) fileName);

    if (doc == NULL) {
        return false;
    }

    //Parse XML
    XMLUtil::forEachXpath ((char *) "/settingid/param", doc,
                           CConfiguration::addParam, this);

    doc->release ();

    XMLPlatformUtils::Terminate ();

    return true;
}

//---------------------------
// loadContextFile
//---------------------------
const bool
CConfiguration::loadContextFile (const char *fileName) {
    struct stat stat_buffer;

    FILE *fp = NULL;

    int iRetval = stat (fileName, &stat_buffer);

    if (iRetval != -1) {
        fp = fopen(fileName, "rb");
    }
    if ((iRetval == -1) || (fp == NULL)) {
        return false;
    }


    char *pTemp = new char[stat_buffer.st_size];


    int iSize = stat_buffer.st_size;
    int iCnt = 0;

    while (iSize) {
        iRetval = fread (&pTemp[iCnt], 1, iSize, fp);

        if (iRetval < 1) {
            break;
        }
        iCnt += iRetval;
        iSize -= iRetval;
    }

    fclose (fp);


    if (iSize != 0 || readContext (pTemp, iCnt) == false) {
        delete[]pTemp;
        return false;
    }

    delete[]pTemp;

    return true;
}

//---------------------------
// writeContextFile
//---------------------------
const bool
CConfiguration::writeContextFile (const char *fileName) {
    char pBuffer[2000];
    int iRetval;

      memset (pBuffer, 0, sizeof (pBuffer));
    int iRet2 = makeContext (pBuffer, 2000);

    if(iRet2 == -1){
        LOG_MESSAGE(LM_ERROR,"CConfiguration(%X)::writeContextFile() could not write to file %s.",this,fileName);
    }

    bool status= readContext(pBuffer,iRet2);

    FILE *fp = fopen (fileName, "wb");

    int iSize = iRet2;
    int iCnt = 0;



    while (iSize)
    {
        iRetval = fwrite (&pBuffer[iCnt], 1, iSize, fp);

        if (iRetval < 1)
        {
            break;
        }
        iCnt += iRetval;
        iSize -= iRetval;
    }

    if(iSize){
        LOG_MESSAGE(LM_ERROR,"CConfiguration(%X)::writeContextFile() could not"
                " write compleate file %s.",this,fileName);
    }

    fclose (fp);

    return true;
}

//---------------------------
// writeXMLFile
//---------------------------
const bool
CConfiguration::writeXMLFile (const char fileName) const {
    return true;
}

//---------------------------
// setParam
//---------------------------
const bool
CConfiguration::setParam (const char *pParamName,
                               const char *pszValue) {
    boost::shared_ptr < CParameter > pTemp (new CParameter (pParamName));

    std::set < boost::shared_ptr < CParameter >,
        CParameterOps >::const_iterator cii = m_CParameterSet.find (pTemp);

    if (cii == m_CParameterSet.end ()) {
        return false;
    }

    return cii->get ()->updateAttribute ("default", pszValue);
}

//---------------------------
// setParam
//---------------------------
const bool
CConfiguration::setParam (const int iValue, const char *pszValue) {
    std::set < boost::shared_ptr < CParameter >,
        CParameterOps >::const_iterator cii;
    for (cii = m_CParameterSet.begin (); cii != m_CParameterSet.end (); cii++) {
        int iD = cii->get ()->getId ();

        if (iD == iValue) {
            return cii->get ()->updateAttribute ("default", pszValue);
        }

    }
    return false;
}

//---------------------------
// xlateXidToId
//---------------------------
const int CConfiguration::xlateXidToId(const int iValue) const{
    std::set < boost::shared_ptr < CParameter >,
        CParameterOps >::const_iterator cii;
    for (cii = m_CParameterSet.begin (); cii != m_CParameterSet.end (); cii++) {
        int iD = cii->get ()->getXid ();

        if (iD == iValue) {
            return cii->get ()->getId ();
        }
    }
    return -1;
}

//---------------------------
// setParam
//---------------------------
const bool
CConfiguration::setParam (const int iValue, const float fValue) {

    char buffer[100];
    sprintf_s (buffer,sizeof(buffer), "%f", fValue);
    return setParam (iValue, buffer);
}

//---------------------------
// setParam
//---------------------------
const bool
CConfiguration::setParam (const char *pParamName, const float fValue) {

    char buffer[100];
    sprintf_s (buffer,sizeof(buffer), "%f", fValue);
    return setParam (pParamName, buffer);
}

//---------------------------
// printContext
//---------------------------
const bool
CConfiguration::printContext () const {

    std::set < boost::shared_ptr < CParameter >,
        CParameterOps >::const_iterator cii;

    std::map < int, CParameter * >pV;

    for (cii = m_CParameterSet.begin (); cii != m_CParameterSet.end ();
         cii++)
    {
        int iD = cii->get ()->getId ();
          pV[iD] = cii->get ();
    }

    std::map < int, CParameter * >::const_iterator ciiv;

    for (ciiv = pV.begin (); ciiv != pV.end (); ciiv++) {

        int iD = ciiv->second->getTypeId ();

        if (iD == CParameter::FLOAT) {
            float x;

            bool s = ciiv->second->getDefaultFloat (&x);
            if (s) {
                fprintf (stdout, "%-4i %-25s %-10s %-8.2f\n",
                         ciiv->second->getId (), ciiv->second->getName (),
                         ciiv->second->getTypeName (), x);
            }
            else {

                fprintf (stdout, "%-4i %-25s %-10s \n",
                         ciiv->second->getId (), ciiv->second->getName (),
                         ciiv->second->getTypeName ());
            }
        }
        else if (iD == CParameter::INT) {

            int x;
            bool s = ciiv->second->getDefaultInt (&x);
            if (s) {
                fprintf (stdout, "%-4i %-25s %-10s %-5i\n",
                         ciiv->second->getId (), ciiv->second->getName (),
                         ciiv->second->getTypeName (), x);
            }
            else {

                fprintf (stdout, "%-4i %-25s %-10s \n",
                         ciiv->second->getId (), ciiv->second->getName (),
                         ciiv->second->getTypeName ());
            }

        }

        else if (iD == CParameter::ENUM) {

            int x;
            bool s;

            s = ciiv->second->getDefaultInt (&x);
            const char *ss = ciiv->second->getEnumName (x);
            if (s && ss) {
                fprintf (stdout, "%-4i %-25s %-10s %-15s\n",
                         ciiv->second->getId (), ciiv->second->getName (),
                         ciiv->second->getTypeName (), ss);
            }
            else {

                fprintf (stdout, "%-4i %-25s %-10s %-5i\n",
                         ciiv->second->getId (), ciiv->second->getName (),
                         ciiv->second->getTypeName (), x);
            }
        }
    }
    return true;
}

//---------------------------
// getCsv
//---------------------------
const char *
CConfiguration::getCsv () const {


    static std::string resultString;
    char buffer[300];

    std::set < boost::shared_ptr < CParameter >,
        CParameterOps >::const_iterator cii;

    std::map < int, CParameter * >pV;

    resultString="";

    for (cii = m_CParameterSet.begin (); cii != m_CParameterSet.end ();
         cii++)
    {
        int iD = cii->get ()->getId ();
          pV[iD] = cii->get ();
    }

    std::map < int, CParameter * >::const_iterator ciiv;

    for (ciiv = pV.begin (); ciiv != pV.end (); ciiv++) {

        int iD = ciiv->second->getTypeId ();
        buffer[0]='\0';

        if (iD == CParameter::FLOAT) {
            float x;

            bool s = ciiv->second->getDefaultFloat (&x);
            if (s) {
                
                sprintf (buffer, "%i,%s,%s,%.2f\n",
                         ciiv->second->getId (), ciiv->second->getName (),
                         ciiv->second->getTypeName (), x);
            }
            else {

                sprintf (buffer, "%i,%s,%s,\n",
                         ciiv->second->getId (), ciiv->second->getName (),
                         ciiv->second->getTypeName ());
            }
        }
        else if (iD == CParameter::INT) {

            int x;
            bool s = ciiv->second->getDefaultInt (&x);
            if (s) {
                sprintf (buffer, "%i,%s,%s,%i\n",
                         ciiv->second->getId (), ciiv->second->getName (),
                         ciiv->second->getTypeName (), x);

            }
            else {

                sprintf (buffer, "%i,%s,%s,\n",
                         ciiv->second->getId (), ciiv->second->getName (),
                         ciiv->second->getTypeName ());
            }


        }

        else if (iD == CParameter::ENUM) {

            int x;
            bool s;

            s = ciiv->second->getDefaultInt (&x);
            const char *ss = ciiv->second->getEnumName (x);
            if (s && ss) {
                sprintf (buffer, "%i,%s,%s,%s\n",
                         ciiv->second->getId (), ciiv->second->getName (),
                         ciiv->second->getTypeName (), ss);
            }
            else {

                sprintf (buffer, "%i,%s,%s,%i\n",
                         ciiv->second->getId (), ciiv->second->getName (),
                         ciiv->second->getTypeName (), x);
            }
        }


        resultString.append(buffer);
    }
    return resultString.c_str();
}


//---------------------------
// addParam 
//---------------------------
int
CConfiguration::addParam (XERCES_CPP_NAMESPACE::DOMNode * ptr, void *pThis) {
    ((CConfiguration *) pThis)->m_CParameterSet.insert (boost::shared_ptr <
                                                        CParameter >
                                                        (new CParameter (ptr)));

    return 0;
}

/** 
 * Connect to remote server.
 * 
 * @param cli_ip    A string containing the IP address of server.
 * @param cli_port  An integer containing the port number to connect to.
 * 
 * @return          A Socket descriptor.
 * 
 */
int
CConfiguration::connect_to_server (char *cli_ip, short cli_port) {
    struct sockaddr_in cli_sa;
    int cli_socket;
    int iRet;

    /* Create the socket */
    cli_socket = socket (AF_INET, SOCK_STREAM, 0);
    if (cli_socket < 0) {
        LOG_MESSAGE(LM_ERROR, "connect_to_server:- connect");
		return -1;
    }

    /*Build socketaddt struct */
    memset(&cli_sa, 0,sizeof (cli_sa));
    cli_sa.sin_family = AF_INET;
    cli_sa.sin_port = htons (cli_port);

	cli_sa.sin_addr.S_un.S_addr = inet_addr (cli_ip);

    iRet =
        connect (cli_socket, (struct sockaddr *) &cli_sa,
                 (socklen_t) sizeof (cli_sa));

    if (iRet == -1) {
        //perror ("connect_to_server:- connect");
		LOG_MESSAGE(LM_ERROR, "connect_to_server:- connect");
        //exit (-1);
		return -1;
    }

    return cli_socket;
}


int
CConfiguration::readblock (int sock, char *buff, size_t len) {
    int rcnt = 0;
    int tcnt;

    while (len) {
        tcnt = recv (sock, buff + rcnt, len, 0);
        if (tcnt <= 0) {
            break;
        }
        rcnt += tcnt;
        len -= tcnt;
    }
    return rcnt;
}



int
CConfiguration::writeblock (int sock, char *buff, size_t len) {
    int rcnt = 0;
    int tcnt;

    while (len) {
        tcnt = send (sock, buff + rcnt, len, 0);
        if (tcnt <= 0) {
            break;
        }
        rcnt += tcnt;
        len -= tcnt;
    }
    return rcnt;
}


int
CConfiguration::pushSettingsToHost (IConfiguration * pX, char *pServerName, char *pPort) {

    int recv;

    char tBuffer[1000];
    char buffer[1000];


    memset (buffer, 0, sizeof (buffer));
    int iLen = pX->makeContext (buffer, 2000);



    /* Open transaction socket */
    int serv_socket = connect_to_server ((char *) pServerName, atoi (pPort));

	if(serv_socket == -1)
	{
		LOG_MESSAGE(LM_CRITICAL, "connect_to_server failed");
		return -1;
	}

    /* RECEIVE BD's response to the OPEN... */

    recv = readblock (serv_socket, tBuffer, 8);
    recv = readblock (serv_socket, tBuffer + 8, 4);


    /* Step #1:  SEND the first message of the four-way transaction... */
    recv = writeblock (serv_socket, buffer, 8);
    recv = writeblock (serv_socket, buffer + 8, iLen - 8);


    /* Step #2:  RECEIVE the first response from the BD... */
    recv = readblock (serv_socket, tBuffer, 8);
    recv = readblock (serv_socket, tBuffer + 8, iLen - 8);


    /* Step #3:  SEND the third message of the four-way transaction... */
    recv = writeblock (serv_socket, (char *)peer0_2, 8);
    recv = writeblock (serv_socket, (char *)peer0_3, 4);

    /* Step #4:  RECEIVE the final response from the BD... */
    recv = readblock (serv_socket, tBuffer, 8);
    recv = readblock (serv_socket, tBuffer + 8, 4);

    /*CLOSE Settings Transaction Socket... */
    closesocket (serv_socket);

    return 0;

}







 
// str = "O2=20 MODE=1 ..."
//
int
CConfiguration::sendSettings(char **str, char *pHost, char *pPort) {
	
	IConfiguration *pX;

    std::string xmlConfigInFile     = "setting_id.xml";


 
    pX= getConfigurationInstance();

    bool bRetval = pX->loadXMLDocument (xmlConfigInFile.c_str ());

    if (bRetval == false) 
	{
       LOG_MESSAGE(LM_CRITICAL,"Could not load configuration."); 
	   return -1;
    }

  

	//Call for each parameter in string
	//TODO:You will need to parse.
	
   //Set parameter
    for (int i = 0; str[i]!=NULL ; i++) {
		
        char *pName = strtok (str[i], "=");
        char *pValue = strtok (NULL, "");


        if (pName && pValue) {
            if (pX->setParam (pName, pValue) == false) {
                fprintf (stderr, "Could not set \"%s=%s\".\n\n", pName, pValue);
          
                return -1;
            }
        }
    }

    int n = pushSettingsToHost (pX, pHost, pPort);
	if(n == -1)
	{
		return -1;
	}

    pX->destroy();

    return 0;
}














#ifdef __cplusplus    // If used by C++ code, 
extern "C" {          // we need to export the C interface
#endif

//---------------------------
// getConfigurationInstance
//---------------------------
__declspec(dllexport) IConfiguration * __cdecl getConfigurationInstance()
{
    return new CConfiguration();
}
 
#ifdef __cplusplus
}
#endif
