#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: ExhHeaterTest - Test the exhalation heater
//---------------------------------------------------------------------
//@ Interface-Description
//  Verify that the exhalation heater is operating by checking for
//	expiratory temperature to rise when heater is turned on and
//	for temp drop when heater is turned off.
//---------------------------------------------------------------------
//@ Rationale
//  This class encapsulates the exhalation heater test.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The method runTest() is responsible for testing the exhalation
//  heater.
//---------------------------------------------------------------------
//@ Fault-Handling
//	n/a
//---------------------------------------------------------------------
//@ Restrictions
//	Only one instance of this class may be created.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/ExhHeaterTest.ccv   25.0.4.0   19 Nov 2013 14:23:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 002  By:  quf    Date: 23-May-1997    DR Number: DCS 2131
//       Project:  Sigma (840)
//       Description:
//            Fixed global reference names (i.e. "r" changed to "R").
//
//  Revision: 001  By:  mad    Date:  02-Nov-1994    DR Number: none
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//=====================================================================

#include "ExhHeaterTest.hh"

//@ Usage-Classes

#include "Task.hh"
#include "MainSensorRefs.hh"
#include "VentObjectRefs.hh"
#include "BDIORefs.hh"
#include "BinaryCommand.hh"

#include "SmRefs.hh"
#include "SmFlowController.hh"
#include "TemperatureSensor.hh"
#include "SmManager.hh"

#ifdef SIGMA_DEVELOPMENT
# include <stdio.h>
#endif  // SIGMA_DEVELOPMENT

#ifdef SIGMA_UNIT_TEST
# include "FakeIo.hh"
# include "FakeControllers.hh"
#endif  // SIGMA_UNIT_TEST

//@ End-Usage

//@ Code...

//@ Constant: MIN_TEMP_RISE
// minimum temp rise when heater is turned on
const Real32 MIN_TEMP_RISE = 3.0;  // deg C

//@ Constant: MIN_TEMP_DROP
// minimum temp drop after heater is shut off
const Real32 MIN_TEMP_DROP = 2.0;  // deg C

//@ Constant: TEMP_RISE_TIMEOUT
// max time allowed for temp to rise by MIN_TEMP_RISE
const Int32 TEMP_RISE_TIMEOUT = 20;  // sec

//@ Constant: TEMP_DROP_TIMEOUT
// max time allowed for temp to drop by MIN_TEMP_DROP
const Int32 TEMP_DROP_TIMEOUT = 60;  // sec


//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ExhHeaterTest()  [Default Constructor]
//
//@ Interface-Description
//	Constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
ExhHeaterTest::ExhHeaterTest(void)
{
	CALL_TRACE("ExhHeaterTest()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~ExhHeaterTest()  [Destructor]
//
//@ Interface-Description
//	Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
ExhHeaterTest::~ExhHeaterTest(void)
{
	CALL_TRACE("~ExhHeaterTest()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: runTest()
//
//@ Interface-Description
//  This method takes no arguments and returns nothing.  It is used to
//  test the exhalation heater.
//---------------------------------------------------------------------
//@ Implementation-Description
//	$[09159]
//	Establish test flow.
//	If test flow does not reach stability within the timeout,
//	generate FAILURE, stop the flow and
//	Read exh heater initial temp.
//	Perform heater-on test by checking temp rise, and if temp doesn't
//	rise by the minimum amount within the timeout, generate a FAILURE,
//	stop the flow, and exit.
//	Perform heater-off test by checking temp drop, and if temp doesn't
//	drop by the minimum amount within the timeout, generate FAILURE.
//	Stop the flow.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
ExhHeaterTest::runTest(void)
{
	CALL_TRACE("ExhHeaterTest::runTest()");

	const Real32 FLOW = 60.0;  // lpm

	// Establish an air flow
	RSmFlowController.establishFlow( FLOW, DELIVERY, AIR);

	// Verify the flow is established
	if (!RSmFlowController.waitForStableFlow())
	{
	// $[TI1]
		// Unable to establish the air flow.
		RSmManager.sendStatusToServiceData(
			SmStatusId::TEST_FAILURE,
			SmStatusId::CONDITION_1);
	}
	else
	{
	// $[TI2]
		// Read the initial air temp and send it to the gui
// E600 BDIO
#if 0
		Real32 exhHeaterTemp1 = RExhHeaterTemperatureSensor.getValue();

		RSmManager.sendTestPointToServiceData(
			SmDataId::EXH_HEATER_EXHALATION_TEMPERATURE1, exhHeaterTemp1);

		// Turn on exhalation heater
        RExhHeater.setBit( BitAccessGpio::ON);

		// Monitor temp until it rises the desired amount or until
		// timeout occurs
		Real32 currentTemp = exhHeaterTemp1;
		Real32 deltaTemp = 0;

		Int32 timer;
		for ( timer = 1;
			  timer <= TEMP_RISE_TIMEOUT && deltaTemp < MIN_TEMP_RISE;
			  timer++ )
		{
		// $[TI2.1]
			// Delay then read temp
// Skip the delay for faster unit testing
#ifndef SIGMA_UNIT_TEST
			Task::Delay(1);
#endif  // SIGMA_UNIT_TEST

			currentTemp = RExhHeaterTemperatureSensor.getValue();
			deltaTemp = currentTemp - exhHeaterTemp1;

#ifdef SIGMA_DEBUG
			printf("%3d  %5.2f  %5.2f\n", timer, currentTemp, deltaTemp);
#endif  // SIGMA_DEBUG
		}
		// end for

		// Store the heated air temp and send it to the gui
		Real32 exhHeaterTemp2 = currentTemp;

		RSmManager.sendTestPointToServiceData(
			SmDataId::EXH_HEATER_EXHALATION_TEMPERATURE2, exhHeaterTemp2);

		// Turn off exhalation heater
        RExhHeater.setBit( BitAccessGpio::OFF);

		// Verify the temperature rises
		if (deltaTemp < MIN_TEMP_RISE)
		{
		// $[TI2.2]
			// Unable to turn on heater.
			RSmManager.sendStatusToServiceData(
				SmStatusId::TEST_FAILURE,
				SmStatusId::CONDITION_2);
		}
		else
		{
		// $[TI2.3]
			// Temp rise met spec, so check temp drop

			// Monitor temp until it drops the desired amount or until
			// timeout occurs
			currentTemp = RExhHeaterTemperatureSensor.getValue();
			deltaTemp = 0;
			for ( timer = 1;
				  timer <= TEMP_DROP_TIMEOUT && deltaTemp < MIN_TEMP_DROP;
				  timer++ )
			{
			// $[TI2.3.1]
				// Delay then read temp
// Skip the delay for faster unit testing
#ifndef SIGMA_UNIT_TEST
				Task::Delay(1);
#endif  // SIGMA_UNIT_TEST

				currentTemp = RExhHeaterTemperatureSensor.getValue();
				deltaTemp = exhHeaterTemp2 - currentTemp;

#ifdef SIGMA_DEBUG
				printf("%3d  %5.2f  %5.2f\n", timer, currentTemp, deltaTemp);
#endif  // SIGMA_DEBUG
			}
			// end for

			// Store the cooled air temp and send it to the gui
			Real32 exhHeaterTemp3 = currentTemp;

			RSmManager.sendTestPointToServiceData(
				SmDataId::EXH_HEATER_EXHALATION_TEMPERATURE3, exhHeaterTemp3);

			// Verify the temperature drops
			if (deltaTemp < MIN_TEMP_DROP)
			{
			// $[TI2.3.2]
				// Unable to turn off heater.
				RSmManager.sendStatusToServiceData(
					SmStatusId::TEST_FAILURE,
					SmStatusId::CONDITION_3);
			}
			// $[TI2.3.3]
			// implied else: test passes
		}
		// end else
// E600 BDIO
#endif
	}
	// end else

	// Turn off the air flow
	RSmFlowController.stopFlowController();

} // runTest


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
ExhHeaterTest::SoftFault(const SoftFaultID  softFaultID,
			 const Uint32       lineNumber,
			 const char*        pFileName,
			 const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, SERVICE_MODE, EXHHEATERTEST,
                          lineNumber, pFileName, pPredicate);

}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//=====================================================================
//
//  Private Methods...
//
//=====================================================================
