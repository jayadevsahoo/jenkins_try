#ifndef Post_HH
#define Post_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//=====================================================================
// Class: Post - Power on self test executive and accessor functions
//---------------------------------------------------------------------
//
//@ Version
// @(#) $Header:   /840/Baseline/POST/vcssrc/Post.hhv   25.0.4.0   19 Nov 2013 14:17:04   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006   By: gdc    Date: 26-Jan-2009    SCR Number: 6407
//  Project:  840S
//  Description:
//      Modified to bring source into strict ARM compliance.
//
//  Revision: 005    By: Gary Cederquist  Date: 05-MAY-1999  DR Number: 5373
//    Project: 840 ATC
//    Description:
//       Deconflicted LONG enumerator by defining a "classed" enumerator
//		 for PostType.
//
//  Revision: 004    By: sah   Date: 14-Jan-1999  DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 003    By: Gary Cederquist  Date: 14-Oct-1998  DR Number: 5105
//    Project: BiLevel
//    Description:
//       Added ClearStrikesLog() method.
//
//  Revision: 002    By: Gary Cederquist  Date: 09-JUN-1997  DR Number: 1902
//    Project: Sigma (R8027)
//    Description:
//       Declared IsNovramInitialized function for detecting when a
//       download operation has occured so initial NOVRAM verification 
//       failures can be ignored.
//
//  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//=====================================================================

#include "Sigma.hh"
#include "kpost.hh"
#include "NominalLineVoltValue.hh"

//  Typedef for POST error codes.  
//  The error code enumerations are defined in:
//    kerrors.hh (for Kernel subsystem) 
//    PostErrors.hh (for POST subsystem).
typedef Int PostErrorCode;
 
// Enumeration indicating the length of the last downtime period.
enum DowntimeStatus
{
	DOWN_SHORT,
	DOWN_LONG
};



// ------------------------------------------------------------------------
// A PostTestItem is a struct containing information needed to process
// an individual test.  The post test executive uses this structure to 
// schedule the test, and process the test results. 
// ------------------------------------------------------------------------

struct  PostTestItem
{
   char     	*testName;                 // Test name string
   Uint8	    testNumber;                // Diagnostic LED number
   Criticality	(*pTestProcedure) (void);  // Test procedure
};

//@ Begin-Free-Declarations

//void     		InitiateReboot          (void);

//@ End-Free-Declarations


class TimeStamp;

class Post
{
  public:
 
    enum ClassIds
    {
        ID_POST         = 1,
        ID_POSTAGENT    = 2
    };

	enum PostType
	{
		POST_SHORT,
		POST_LONG
	};

    static void                 Initialize(void);
    static void                 Executive(void);

    static void                 Phase3Exec(void);

    static Post::PostType       GetPostType(void);

    static Boolean              IsPostFailed(void);

	//TODO E600 changed all variables with name bool to boolean
	// since bool is a type in windows 
    static void                 SetPostFailed(Boolean boolean);

    static void                 SetBdFailed(Boolean boolean);
    static Boolean              IsBdFailed(void);

    static void                 SetBackgroundFailed(Criticality status);
    static Criticality          GetBackgroundFailed(void);

    static Boolean              IsServiceModeRequested(void);
 
    static const TimeStamp&     GetPowerDownTime(void);
    static const TimeStamp&     GetPowerUpTime(void);

    static DowntimeStatus       GetDowntimeStatus(void);

    static ShutdownState        GetStartupState(void);

    static Boolean              IsSysInitComplete(void);
    static void                 SetSysInitComplete(Boolean boolean);

    static Boolean              IsDownloadScheduled(void);
    static void                 SetDownloadScheduled(Boolean boolean);

    static Uint32               GetOperationalTime(void);
    static void                 SetOperationalTime(Uint32 hours);

    static void                 SetAcLineVoltage(NominalLineVoltValue::NominalLineVoltValueId nominalValue);

    static NominalLineVoltValue::NominalLineVoltValueId
                                GetAcLineVoltage(void);

    static void                 ClearResultsLog(void);
    static void                 ClearRollingThunderCount(void);

    static const char *         GetKernelPartNumber();
    static Boolean              IsPostActive();
    static Boolean              IsNovramInitialized();

	static void					ClearStrikesLog(void);

    static void          SoftFault(const SoftFaultID  softFaultID,
				   const Uint32       lineNumber,
				   const char*        pFileName = NULL,
				   const char*        pPredicate = NULL);
 

    friend void InitiateReboot(ShutdownState state);

  protected:

    static void           InitializeNovram_(void);
    static Criticality    TestNovram_(void);

    static ShutdownState  GetShutdownState_(void);
    static void           SetShutdownState_(ShutdownState state);
    static void           SetStartupState_(ShutdownState state);
    static void           SetPowerDownTime_(void);
    static void           SetPostType_(Post::PostType postType);

  private:

    Post(void);            // not implemented
    virtual ~Post(void);   // not implemented - virtual to keep compiler happy
	Post(const Post& rPost);  //not implemented 

    static DowntimeStatus GetDowntimeStatus_(void);
};
 

#endif	// #ifndef Post_HH
