#include "stdafx.h"
//=====================================================================
//   This is a proprietary work to which Puritan-Bennett corporation
//   of California claims exclusive right.  No part of this work may
//  be used, disclosed, reproduced, sorted in an information retrieval
//     system, or transmitted by any means, electronic, mechanical,
//   photocopying, recording, or otherwise without the prior written
//      permission of Puritan-Bennett Corporation of California.
//
//          Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: Sys_Init - Sys-Init Subsystem Class.
//---------------------------------------------------------------------
//@ Interface-Description
//  The Sys-Init class is the entry to the Sys-Init subsystem.  It
//  contains static methods called during system initialization for
//  system initialization and task creation.
//
//  InitializeSigma calls the Create entry point prior to VRTX tasking
//  to initialize the tasking classes and to create the Sys-Init task.
//  The Initialize method entered upon start of VRTX tasking
//  initializes the Sigma subsystems.  Sys-Init then creates the
//  remaining tasks in the TaskTable and transfers control to the
//  TaskControl::MainLoop where it waits for messages.
//
//---------------------------------------------------------------------
//@ Rationale
//  The Sys-Init class encapsulates the Sys-Init subsystem creation and
//  initialization.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The Create function is called prior to VRTX tasking.  It creates
//  the Task-Control task that starts executing in the Initialize
//  function.  As much initialization as possible is done after VRTX
//  tasking as more debug facilities including console output are
//  then available where they are not prior to tasking.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Sys-Init/vcssrc/Sys_Init.ccv   25.0.4.0   19 Nov 2013 14:33:56   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005   By: gdc    Date: 03-Jan-2011    SCR Number: 6728
//  Project:  XENA2
//  Description:
//      Removed local communications logic from development only code.
//
//  Revision: 004   By: gdc    Date: 26-Jan-2009    SCR Number: 6461
//  Project:  840S
//  Description:
//      Added call to main() to execute static initializers after
//      vrtx_go but prior to application initialization.
//
//  Revision: 003  By:  gdc    Date:  12-Dec-2008    SCR Number: 6068
//  Project:  840S
//  Description:
//		Removed SUN prototype code.
//
//   Revision 002   By:   sah      Date: 01/13/99    DR Number: 5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to a non-inlined method.
//
//   Revision 001   By:   gdc      Date: 07/06/95    DR Number:   n/a
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//=====================================================================

//=====================================================================
//
//      Includes...
//
//=====================================================================

#include "Sys_Init.hh"
#include "TaskFlags.hh"

//@ Usage-Classes
#include "FsmState.hh"
#include "TaskControlMessage.hh"
#include "InitConditions.hh"
#include "IpcTable.hh"
#include "MailBox.hh"
#include "MsgQueue.hh"
#include "MutEx.hh"
#include "NetworkApp.hh"
#include "Post.hh"
#include "StackCheck.hh"
#include "TaskAgentManager.hh"
#include "TaskControl.hh"
#include "Task.hh"
#include "TaskTable.hh"
#include "TimerTbl.hh"

#ifdef SIGMA_DEVELOPMENT
#  include "Ostream.hh"
#endif

#ifdef SIGMA_UNIT_TEST
#  include "TestDriver.hh"
#endif

#ifdef SIGMA_GUIPC_CPU
/* TODO e600 TT
The service mode is usually entered by pressing a hardware button that BD detects. 
GUI goes into service mode based on the BD's request. Eventially the clean way of doing that for 
GUIPC would be to add this functionality in BD simulator, basically let BD simulator come up in 
service mode and send the message to GUI. That way there is no special code in GUI to handle this scenario.

A temporary solution for now: service mode is entered by holding SHIFT key, 
and following header files and g_bSvcMode are needed for now
*/
#include "GuiFsm.hh"
#include "GuiServiceInit.hh"
bool g_bSvcMode = FALSE;//a global flag indicating service mode has been entered 
#endif
//@ End-Usage

//=====================================================================
//
//      Static Data...
//
//=====================================================================

Boolean Sys_Init::SystemInitialized_    = FALSE;
Boolean Sys_Init::TaskingOn_            = FALSE;
Boolean Sys_Init::DebugLevel_[];

//@ Code...

//=====================================================================
//
//      Public Methods...
//
//=====================================================================

void Sys_Init::InitializeSigma(void)
{
    // run POST Phase 3 Tests
	//TODO E600 do we need this? Or rewrite POST for what is needed
    //Post::Phase3Exec();

    // install Sigma exception handlers
//TODO E600 port    Exception::InstallHandlers();

    // generic board initialization
	//TODO E600 VM: do we still need this? #define from Tony F
#ifdef E600_840_TEMP_REMOVED
    BoardInit();
#endif
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: InitializeTasks
//
//@ Interface-Description
//  This method is called prior to tasking.
//  It initializes the TaskTable, and Task classes that is used later
//  to create tasks.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls the Initialize method for TaskTable and TaskInfo
//  classes.
//---------------------------------------------------------------------
//@ PreCondition
//  !Sys_Init::TaskingOn_
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
Sys_Init::InitializeTasks(void)
{
    CLASS_PRE_CONDITION(!Sys_Init::TaskingOn_);

	//TaskTable MUST be initialized before TaskInfo
    TaskTable::Initialize();
	//Initialize the threads (initialized TaskInfo) but do not "create" them..
	//they will be created later when the state-machine starts
    TaskInfo::Initialize();
#if defined(SIGMA_UNIT_TEST) || defined(SIGMA_DEVELOPMENT)
    //OstreamTbl::Initialize();
#endif

	//next step would be to "create" one thread only which is TaskControl::MainLoop
	//after that, other threads are "created" when the state-machine starts
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Initialize
//
//@ Interface-Description
//  This method is the entry point for the Task-Control task that is
//  the only task created prior to vrtx_go.  Upon entry,
//  this method initializes the OS-Foundation classes that were not
//  initialized by Sys_Init::Create prior to tasking.  It initializes
//  the inter-process communication facilities and configuration then
//  calls Sys_Init::InitializeSubsystems to initialize the various
//  Sigma subsystems.  Upon returning, this method initializes the
//  Task-Control classes required to receive and process events.  It
//  activates the Task-Control finite state machine and calls the
//  TaskControl main processing loop.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  !Sys_Init::SystemInitialized_
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
Sys_Init::Initialize(void)
{
	// TODO E600 VM: for PC simmulator project, port to PC platform because
	// obviously not everything in Initialize is applicable in PC platform!
    CLASS_PRE_CONDITION(!Sys_Init::SystemInitialized_);

	// execute the static initializers/constructors

#ifdef SIGMA_DEVELOPMENT
    TaskInfo& taskInfo = Task::GetTaskInfo_(1);

#ifdef TRACE_ON
    Trace::SetTraceOff();
    if (taskInfo.isFlagOn(::F_TRACE))
        Trace::SetTraceOn();
#endif

    if (taskInfo.isFlagOn(::F_DEBUG1))
        Sys_Init::SetDebugLevel(1);
    if (taskInfo.isFlagOn(::F_DEBUG2))
        Sys_Init::SetDebugLevel(2);
    if (taskInfo.isFlagOn(::F_DEBUG3))
        Sys_Init::SetDebugLevel(3);
    if (taskInfo.isFlagOn(::F_DEBUG4))
        Sys_Init::SetDebugLevel(4);
#endif

    IpcTable::Initialize();
    MsgQueueList::Initialize();
    MutEx::Initialize();
    MailBoxList::Initialize();
    TimerTbl::Initialize();
    Post::Initialize();

    Sys_Init::InitializeSubsystems_();

	InitConditions::Initialize();
    TaskControlMessage::Initialize();
	TaskControl::Initialize();
    TaskAgentManager::Initialize();

    FsmState::Initialize();

	//Multithreading officially starts now.. The TaskControl thread will be running as soon
	//as it gets created in the next line. Other threads will be created in FsmState::Start
	//Other threads will be created when the state-mashine statrs (see FsmState::Activate_())
	Sys_Init::TaskingOn_ = TRUE;
	
	// FsmState::Start() initializes the state machine and create all tasks with the expception
	// of Task Control. Note: IT IS CRUCIAL THAT THIS IS DONE BEFORE TaskInfo::GetTaskInfo(1).Create()
	// Otherwise the BD will not be able to successfully transition to Online Mode unless the GUI is
	// also online.
	FsmState::Start();

	//Create and launch ONLY main thread (TaskControl::MainLoop) which handles messaging
	//between threads. When the state-machine (Fsm) starts, it creates all OTHER threads
	//This guarantees that none of the threads is running until TaskControl is running
	//and that the state-machine is in the right state.
	TaskInfo::GetTaskInfo(1).Create();
  
	//  Set the SystemInitialized flag which prohibits
    //  further instantiations of OS objects such queues,
    //  mailboxes, etc...
    Sys_Init::SystemInitialized_ = TRUE;

#ifdef SIGMA_GUIPC_CPU
	/* TODO e600 TT
	The service mode is usually entered by pressing a hardware button that BD detects. 
	GUI goes into service mode based on the BD's request. Eventially the clean way of doing that for 
	GUIPC would be to add this functionality in BD simulator, basically let BD simulator come up in 
	service mode and send the message to GUI. That way there is no special code in GUI to handle this scenario.
	For now, service mode is entered by holding SHIFT key when boot up */
	SHORT wHiBitMask = 0x80;
	SHORT wKeySM = GetKeyState(VK_SHIFT);

	if(wKeySM & wHiBitMask)
	{
		g_bSvcMode = TRUE;
		GuiFsm::GetGuiServiceInitState().startTransition();
	}
#endif

    //E600 removed TaskControl::MainLoop() because it is now a thread entry point
	//and will be automatically executed when the thread is Entered

#ifdef SIGMA_UNIT_TEST
    TestDriver::MainLoop();
#endif
}


#ifdef SIGMA_DEVELOPMENT

//============================ M E T H O D   D E S C R I P T I O N ====
//  Method: SetDebugLevel
//
//  Interface-Description
//  Set all of this subsystem's debug levels.
//---------------------------------------------------------------------
//  Implementation-Description
//---------------------------------------------------------------------
//  PreCondition
//  none
//---------------------------------------------------------------------
//  PostCondition
//  (Sys_Init::GetDebugLevel(...for all class ids...) == level)
//  End-Method
//=====================================================================

void
Sys_Init::SetDebugLevel(const Int32 level)
{
  CALL_TRACE("SetDebugLevel(level)");

  for (Int32 i=0; i<Sys_Init::NUM_INIT_CLASSES; i++)
  {
    Sys_Init::DebugLevel_[i] = level;
  }
}


//============================ M E T H O D   D E S C R I P T I O N ====
//  Method: GetDebugLevel
//
//  Interface-Description
//  Is the debug flag for the class represented by 'initClassId'
//  currently turned on.
//---------------------------------------------------------------------
//  Implementation-Description
//---------------------------------------------------------------------
//  PreCondition
//  (initClassId < Sys_Init::NUM_INIT_CLASSES)
//---------------------------------------------------------------------
//  PostCondition
//  none
//  End-Method
//=====================================================================

Int32
Sys_Init::GetDebugLevel(const Sys_Init::InitClassId initClassId)
{
  CALL_TRACE("GetDebugLevel(initClassId)");
  CLASS_PRE_CONDITION(initClassId < Sys_Init::NUM_INIT_CLASSES);

  return  Sys_Init::DebugLevel_[initClassId];
}


//============================ M E T H O D   D E S C R I P T I O N ====
//  Method: SetDebugLevel
//
//  Interface-Description
//  Set the debug flag for the class represented by 'initClassId'.
//---------------------------------------------------------------------
//  Implementation-Description
//---------------------------------------------------------------------
//  PreCondition
//  (initClassId < Sys_Init::NUM_INIT_CLASSES)
//---------------------------------------------------------------------
//  PostCondition
//  (Sys_Init::GetDebugLevel(initClassId) == level)
//  End-Method
//=====================================================================

void
Sys_Init::SetDebugLevel(const Sys_Init::InitClassId initClassId,
                        const Int32 level)
{
  CALL_TRACE("SetDebugLevel(initClassId, level)");
  CLASS_PRE_CONDITION(initClassId < Sys_Init::NUM_INIT_CLASSES);
  CLASS_PRE_CONDITION(level <= 0xff);

  Sys_Init::DebugLevel_[initClassId] = level & 0xff;
}


#endif  // SIGMA_DEVELOPMENT



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//@ Interface-Description
//   This method is called when a software fault is detected by the
//   fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//   'lineNumber' are essential pieces of information.  The 'pFileName'
//   and 'pPredicate' strings may be defaulted in the macro to reduce
//   code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This method receives the call for the SoftFault, adds it sub-system
//   and class name ID and sends the message to the non-member function
//   SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================

void
Sys_Init::SoftFault(const SoftFaultID  softFaultID,
                                const Uint32       lineNumber,
                                const char*        pFileName,
                                const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, SYS_INIT,
                          Sys_Init::ID_SYS_INIT,
                          lineNumber, pFileName, pPredicate);
}


//=====================================================================
//
//      Protected Methods...
//
//=====================================================================

//=====================================================================
//
//      Private Methods...
//
//=====================================================================


