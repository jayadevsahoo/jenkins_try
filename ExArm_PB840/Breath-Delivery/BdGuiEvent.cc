#include "stdafx.h"
#if defined(SIGMA_GUI_CPU) || defined(SIGMA_UNIT_TEST)
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: BdGuiEvent - handles event requests to the BD from the GUI
//    and event and ventilator status from the BD to the GUI
//---------------------------------------------------------------------
//@ Interface-Description
//    This class provides a method to send user event requests (eg. starting 
//    or stopping an expiratory pause) to the BD and a method to receive the 
//    user event status and ventilator event status (eg. start of Apnea 
//    Ventilation) from the BD.  The method Initialize registers the
//    ReceiveEventStatus method with the Network Applications subsystem
//    so that all EVENT_STATUS_MSG messages are routed to that method
//    for processing.  ReceiveEventStatus then puts the event status
//    on the USER_ANNUNCIATION_Q.  The method RequestUserEvent is invoked
//    from GUI-Applications when one of the user event keys is touched to
//    transmit a user event request to the BD via Network Applications.
//---------------------------------------------------------------------
//@ Rationale
//    This class defines how each user event is originated and serviced
//    by the GUI sub-system.
//---------------------------------------------------------------------
//@ Implementation-Description
//    The method Initialize registers the ReceiveEventStatus method with
//    the Network Applications subsystem.  ReceiveEventStatus translates
//    the status message recieved from BD into a message that is placed on the
//    USER_ANNUNCIATION_Q.  The method RequestUserEvent translates a user event
//    request into a message that is transmitted to the BD.
//---------------------------------------------------------------------
//@ Fault-Handling
//    n/a
//---------------------------------------------------------------------
//@ Restrictions
//    This is a static class; it is not instanciated.
//---------------------------------------------------------------------
//@ Invariants
//    none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/BdGuiEvent.ccv   25.0.4.0   19 Nov 2013 13:59:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 010   By: rpr    Date: 22-Jan-2009    SCR Number: 6435
//  Project:  840S
//  Description:
//      Applied code review comments.
// 
//  Revision: 009   By: rpr    Date: 10-Oct-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal Plus 20 O2.
// 
//  Revision: 008   By: gdc   Date:  13-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Refactored code into CancelManeuver method for canceling any maneuver.
//
//  Revision: 007  By: syw     Date:  24-Jan-2000    DR Number: 5327
//  Project:  NeoMode
//  Description:
//		Handle the case during CommsDown() to re-initialize PERCENT_O2 events
//		when active; otherwise when comm is up again and an PERCENT_O2 is ACTIVE,
//		no PERCENT_O2 message will be posted.
//
//  Revision: 006  By: yyy     Date:  08-Jul-1999    DR Number: 5458
//  Project:  ATC
//  Description:
//      Added condition checking to avoid of sending message to GuiApp queue
//		when inspiratory or expiratory has been completed, canceled or rejected.
//
//  Revision: 005  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 004  By:  yyy    Date:  01-Oct-1998    DCS Number:
//  Project:  BiLevel
//  Description:
//			Bilevel initial version.
//
//  Revision: 003  By:  iv    Date:  03-Apr-1997    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Rework per formal code review for BD Schedulers.
//
//  Revision: 002  By:  iv    Date:  07-Feb-1997    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Rework per Breath-Delivery formal code review
//
//  Revision: 001  By:  kam    Date:  20-Sep-1995    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================
#include "BdGuiEvent.hh"

//@ Usage-Classes
#include "UserAnnunciationMsg.hh"
#include "IpcIds.hh"
#include "MsgQueue.hh"
#include <memory>
//@ End-Usage

//@ Code...
EventData::EventStatus BdGuiEvent::EventStatus_[NUM_EVENT_IDS];

//=====================================================================
//
//  //@ Data-Member: Methods..
//
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Initialize
//
//@ Interface-Description
//    This static method takes no parameters and returns nothing.
//    This method registers the ReceiveEventStatus method
//    with Network Apps so that ReceiveEventStatus() is invoked when
//    a EVENT_STATUS_MSG is received from the BD, and it registers
//    the ReceiveAllEventStatus method with Network Apps so that
//    ReceiveAllEventStatus() is invoked when the ALL_EVENT_STATUS_MSG
//    is received from the BD.  The EVENT_STATUS_MSG is used when
//    single event status is transmitted; the ALL_EVENT_STATUS_MSG is
//    used when all of the event statuses are sent at one time on
//    comms up.  This method also initializes static data.
//---------------------------------------------------------------------
//@ Implementation-Description
//    This method invokes NetworkApp::RegisterRecvMsg() to register
//    the ReceiveUserEventStatus() and ReceiveAllUserEventStatus() methods
//    with Network Applications to receive EVENT_STATUS_MSG and
//    ALL_EVENT_STATUS_MSG messages, respectively.  All elements of the
//    static array EventStatus_ are intialized to IDLE.
//---------------------------------------------------------------------
//@ PreCondition
//    None
//---------------------------------------------------------------------
//@ PostCondition
//    None
//@ End-Method
//=====================================================================
void
BdGuiEvent::Initialize(void)
{
    CALL_TRACE("BdGuiEvent::Initialize(void)");

    // Register the BdGuiEvent::ReceiveEventStatus static method
    // with Network Apps so that all status messages are sent to that
    // method for processing
    NetworkApp::RegisterRecvMsg(EVENT_STATUS_MSG, BdGuiEvent::ReceiveEventStatus);

    // Register the BdGuiEvent::ReceiveAllEventStatus static method
    // with Network Apps so that event status messages are sent to that
    // method for processing.  The ALL_EVENT_STATUS_MSG is sent with all of the
    // user event and ventilator status messages when communications is established
    NetworkApp::RegisterRecvMsg(ALL_EVENT_STATUS_MSG, BdGuiEvent::ReceiveAllEventStatus);

    // Init the status array
    for (int ii = 0; ii < NUM_EVENT_IDS; ii++ )
    {
        // $[TI1]
        BdGuiEvent::EventStatus_[ii] = EventData::IDLE;
    }
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: CommsDown
//
//@ Interface-Description
//    This method is invoked from GUI Applications when Communications
//    between the BD and the GUI goes down.  
//---------------------------------------------------------------------
//@ Implementation-Description
//    This method resets ventilator breathing status events to idle.
//    During Comms Down, the GUI doesn't know what breathing mode the
//    BD is in so instead of reporting potentially invalid vent status,
//    don't report the status.
//---------------------------------------------------------------------
//@ PreCondition
//    None
//---------------------------------------------------------------------
//@ PostCondition
//    None
//@ End-Method
//=====================================================================
void
BdGuiEvent::CommsDown(void)
{
    CALL_TRACE("BdGuiEvent::CommsDown(void)");

    UserAnnunciationMsg msg;
    msg.bdEventParts.eventType = UserAnnunciationMsg::BD_GUI_EVENT;

    if (BdGuiEvent::EventStatus_[EventData::APNEA_VENT] == EventData::ACTIVE)
    {
        // $[TI1]
        BdGuiEvent::EventStatus_[EventData::APNEA_VENT] = EventData::IDLE;

        // Prepare message for the USER_ANNUNCIATION_Q
        msg.bdEventParts.eventId = EventData::APNEA_VENT;
        msg.bdEventParts.eventStatus = EventData::CANCEL;

        // Put the message on the User Annunciation Queue
        CLASS_ASSERTION (MsgQueue::PutMsg(USER_ANNUNCIATION_Q, (Int32)msg.qWord) == Ipc::OK);
    }
	// $[TI2]

    if (BdGuiEvent::EventStatus_[EventData::SAFETY_VENT] == EventData::ACTIVE)
    {
        // $[TI3]
        BdGuiEvent::EventStatus_[EventData::SAFETY_VENT] = EventData::IDLE;

        // Prepare message for the USER_ANNUNCIATION_Q
        msg.bdEventParts.eventId = EventData::SAFETY_VENT;
        msg.bdEventParts.eventStatus = EventData::CANCEL;

        // Put the message on the User Annunciation Queue
        CLASS_ASSERTION (MsgQueue::PutMsg(USER_ANNUNCIATION_Q, (Int32)msg.qWord) == Ipc::OK);
    }
	// $[TI4]

	CancelManeuver(EventData::EXPIRATORY_PAUSE);
	CancelManeuver(EventData::INSPIRATORY_PAUSE);

	// $[RM12077] A NIF maneuver shall be canceled if communication is lost... 
	// $[RM12094] A P100 maneuver shall be canceled if communication is lost... 
	// $[RM12062] A VC maneuver shall be canceled if communication is lost... 
	CancelManeuver(EventData::NIF_MANEUVER);
	CancelManeuver(EventData::P100_MANEUVER);
	CancelManeuver(EventData::VITAL_CAPACITY_MANEUVER);

    if (BdGuiEvent::EventStatus_[EventData::PERCENT_O2] == EventData::ACTIVE)
	{	// $[TI9]
        BdGuiEvent::EventStatus_[EventData::PERCENT_O2] = EventData::IDLE;
	}	// $[TI10]	

    if (BdGuiEvent::EventStatus_[EventData::CALIBRATE_O2] == EventData::ACTIVE)
	{  
        BdGuiEvent::EventStatus_[EventData::CALIBRATE_O2] = EventData::IDLE;
	}  
		
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ReceiveEventStatus
//
//@ Interface-Description
//    This method is invoked by the communication link when an EVENT_STATUS_MSG
//    message is received.  The method takes the message Id, a pointer 
//    to the message data and the message size as inputs.  It has no return value.
//    The message Id is not used but is required to conform to the
//    prototype defined in NetworkApp for the callback method.
//
//    The message data contains the EventId and the EventStatus.
//    This method takes that information out of the message and places
//    it on the USER_ANNUNCIATION_Q.  It also maintains the EventStatus
//    in the static array EventStatus_.
//---------------------------------------------------------------------
//@ Implementation-Description
//    This method takes the user event ID and status out of the message
//    data and builds a UserAnnunciationMsg; it then invokes
//    MsgQueue::PutMsg() to put the message on the USER_ANNUNCIATION_Q.
//    And, it updates the status of the event in the EventStatus_ array.
//---------------------------------------------------------------------
//@ PreCondition
//   ((pData) && ( sizeof(EventData) == size ))
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

void
BdGuiEvent::ReceiveEventStatus(XmitDataMsgId, void *pData, Uint size)
{
    // $[TI1]
        
    CALL_TRACE("BdGuiEvent::ReceiveEventStatus(XmitDataMsgId msgId,\
                void *pData, Uint size)");
                
    CLASS_PRE_CONDITION((pData) && ( sizeof(EventData) == size ));
    EventData *pEventData = (EventData *) pData;
    
    // Prepare message for the USER_ANNUNCIATION_Q
    UserAnnunciationMsg msg;
    msg.bdEventParts.eventType = UserAnnunciationMsg::BD_GUI_EVENT;
    msg.bdEventParts.eventId = pEventData->bdToGuiEvent.id;
    msg.bdEventParts.eventStatus = pEventData->bdToGuiEvent.status;
    msg.bdEventParts.eventPrompt = pEventData->bdToGuiEvent.prompt;

    // Put the message on the User Annunciation Queue
    CLASS_ASSERTION (MsgQueue::PutMsg(USER_ANNUNCIATION_Q, (Int32)msg.qWord) == Ipc::OK);

    // Maintain status in class data member
    BdGuiEvent::EventStatus_[pEventData->bdToGuiEvent.id] = pEventData->bdToGuiEvent.status;
}    


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ReceiveAllEventStatus
//
//@ Interface-Description
//    This method is invoked by the communication link when an ALL_EVENT_STATUS_MSG
//    is received.  The method takes the message Id, a pointer 
//    to the message data and the message size as inputs.  It has no return value.
//    The message Id and size are not used but are required to conform to the
//    prototype defined in NetworkApp for the callback method.
//
//    The message contains all of the user event statuses.
//    This method takes that information out of the message and places
//    it on the USER_ANNUNCIATION_Q.  It also maintains the EventStatus
//    in the static array EventStatus_.
//---------------------------------------------------------------------
//@ Implementation-Description
//    This method takes the user event ID and status out of the message
//    data and builds a UserAnnunciationMsg; it then invokes
//    MsgQueue::PutMsg() to put the message on the USER_ANNUNCIATION_Q.
//    And, it updates the status of the event in the EventStatus_ array.
//---------------------------------------------------------------------
//@ PreCondition
//    pData != NULL
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

void
BdGuiEvent::ReceiveAllEventStatus(XmitDataMsgId, void *pData, Uint size)
{
	EventData::EventStatus newEventStatus[NUM_EVENT_IDS];
        
    CALL_TRACE("BdGuiEvent::ReceiveAllEventStatus(XmitDataMsgId,\
                void *pData, Uint size)");

    CLASS_PRE_CONDITION((pData) && ( sizeof(EventStatus_) == size ));
                
    memcpy (newEventStatus, pData, sizeof(newEventStatus));

    // Prepare message for the USER_ANNUNCIATION_Q for all changed statuses
    UserAnnunciationMsg msg;
    
    for (int ii=0; ii < NUM_EVENT_IDS; ii++)
    {
        if (BdGuiEvent::EventStatus_[ii] != newEventStatus[ii])
        {
            // $[TI1]
            msg.bdEventParts.eventType = UserAnnunciationMsg::BD_GUI_EVENT;
            msg.bdEventParts.eventId = ii;
            msg.bdEventParts.eventStatus = newEventStatus[ii];
            
            // Put the message on the User Annunciation Queue
            CLASS_ASSERTION (MsgQueue::PutMsg(USER_ANNUNCIATION_Q, (Int32)msg.qWord) == Ipc::OK);

            // Maintain status in class data member
            BdGuiEvent::EventStatus_[ii] = newEventStatus[ii];
        }
        // $[TI2]
    }
    
}    


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: RequestUserEvent
//
//@ Interface-Description
//    This method takes a EventId and an EventData::RequestedState as
//    inputs and returns nothing. It builds a USER_EVENT_REQUEST_MSG
//    and transmits it to the BD.
//---------------------------------------------------------------------
//@ Implementation-Description
//    This method builds an EventData message that contains the
//    user event Id and user event requested state.  It invokes
//    NetworkApp::XmitMsg() to send the message to Breath-Delivery.
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

void
BdGuiEvent::RequestUserEvent(const EventData::EventId id,
                             const EventData::RequestedState request,
							 const EventData::MoreData moredata)
     
{
    // $[TI1]
        
    CALL_TRACE("BdGuiEvent::RequestUserEvent(const EventData::EventId id, \
                             const EventData::RequestedState request)");

    // Prepare message for BD based on the input parameters
    EventData msg;
    msg.guiToBdUserEvent.id = id;
    msg.guiToBdUserEvent.request = request;
    msg.guiToBdUserEvent.moredata = moredata;

    // Transmit the message
    NetworkApp::XmitMsg(USER_EVENT_REQUEST_MSG, &msg, sizeof(msg));
  
}    


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: CancelManeuver
//
//@ Interface-Description
//    This method takes a EventId and returns nothing. It cancels the
//    specified maneuver by resetting its status to IDLE and sending
//    an CANCEL event to the USER_ANNUNCIATION_Q. It is invoked when
//    communication is lost with the BD.
//---------------------------------------------------------------------
//@ Implementation-Description
//    This method builds an EventData message that contains the
//    user event Id and user event requested state.  It invokes
//    MsgQueue::PutMsg to send the message to the GuiApp task.
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

void
BdGuiEvent::CancelManeuver(const EventData::EventId eventId)
{
  UserAnnunciationMsg msg;
  msg.bdEventParts.eventType = UserAnnunciationMsg::BD_GUI_EVENT;

  if (BdGuiEvent::EventStatus_[eventId] != EventData::IDLE &&
	  BdGuiEvent::EventStatus_[eventId] != EventData::COMPLETE &&
	  BdGuiEvent::EventStatus_[eventId] != EventData::CANCEL &&
	  BdGuiEvent::EventStatus_[eventId] != EventData::REJECTED)
  {
	  BdGuiEvent::EventStatus_[eventId] = EventData::IDLE;

	  // Prepare message for the USER_ANNUNCIATION_Q
	  msg.bdEventParts.eventId = eventId;
	  msg.bdEventParts.eventStatus = EventData::CANCEL;

	  // Put the message on the User Annunciation Queue
	  CLASS_ASSERTION (MsgQueue::PutMsg(USER_ANNUNCIATION_Q, (Int32)msg.qWord) == Ipc::OK);
  }
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
BdGuiEvent::SoftFault(const SoftFaultID  softFaultID,
					  const Uint32       lineNumber,
					  const char*        pFileName,
					  const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, BDGUIEVENT,
                          lineNumber, pFileName, pPredicate);
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================



//=====================================================================
//
//  Private Methods...
//
//=====================================================================


#endif // defined(SIGMA_GUI_CPU) || defined(SIGMA_UNIT_TEST)
