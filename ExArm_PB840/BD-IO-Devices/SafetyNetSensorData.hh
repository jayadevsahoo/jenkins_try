#ifndef SafetyNetSensorData_HH
#define SafetyNetSensorData_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
//  Class: SafetyNetSensorData - Sensor safety net data
//---------------------------------------------------------------------
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/SafetyNetSensorData.hhv   25.0.4.0   19 Nov 2013 13:54:10   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006 By: erm    Date: 15-MAY-2003   DR Number: 6055
//  Project:  AVER
//	Description: Modified 5 & 12 volt Gui Sentry
//
//  Revision: 005 By: syw    Date: 16-Mar-2000   DR Number: 5611
//  Project:  NeoMode
//	Description:
//		Eliminate SV_SWITCHED_MIN_COUNTS and SV_SWITCHED_MAX_COUNTS
//		references.
//
//  Revision: 004  By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 003 By: iv     Date: 10-Mar-1998   DR Number: DCS 5041
//  	Project:  Sigma (R8027)
//		Description:
//			Added lastRawCountReading_ data member and access method.
//
//  Revision: 002 By: syw    Date: 07-Oct-1997   DR Number: DCS 2537
//  	Project:  Sigma (R8027)
//		Description:
//			Modified checkRange() to have no return value.  Added
//			lastRawCountBeforeFailure_ data member.
//
//  Revision: 001  By:  by    Date:  25-Jul-1995    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Initial version
//
//=====================================================================

#include "Sigma.hh"
#include "BD_IO_Devices.hh"

//@ Usage-Classes
#include "Background.hh"
//@ End-Usage

//=====================================================================
//
//  Min/Max ADC Count Values for Sensors
//
//====================================================================

//@ Constant: NO_DEFAULT_VALUE_USED 
//  A zero constant value used in place of default value in the constructor
extern const AdcCounts NO_DEFAULT_VALUE_USED;

//@ Constant: Q_TEMP_MIN_COUNTS
// The minimum valid flow sensor temperature reading in counts --
// used for the air, oxygen and exhalation flow sensors
extern const AdcCounts Q_TEMP_MIN_COUNTS;

//@ Constant: Q_TEMP_MAX_COUNTS
// The maximum valid flow sensor temperature reading in counts --
// used for the air, oxygen and exhalation flow sensors
extern const AdcCounts Q_TEMP_MAX_COUNTS;

//@ Constant: Q_AIR_O2_TEMP_DEFAULT
// The default air/o2 flow sensor temperature reading in counts
extern const AdcCounts Q_AIR_O2_TEMP_DEFAULT;

//@ Constant: Q_EXH_TEMP_DEFAULT
// The default exhaled flow sensor temperature reading in counts
extern const AdcCounts Q_EXH_TEMP_DEFAULT;

//@ Constant: SENTRY_12V_MIN_COUNTS
// The minimum valid 12V Sentry  reading in counts
extern const AdcCounts SENTRY_12V_MIN_COUNTS;

//@ Constant: SENTRY_12V_MAX_COUNTS
// The maximum valid 12V Sentry  reading in counts
extern const AdcCounts SENTRY_12V_MAX_COUNTS;

//@ Constant: ALARM_CABLE_MIN_COUNTS
// The minimum valid alarm cable voltage  reading in counts
extern const AdcCounts ALARM_CABLE_MIN_COUNTS;

//@ Constant: ALARM_CABLE_MAX_COUNTS
// The maximum valid alarm cable voltage  reading in counts
extern const AdcCounts ALARM_CABLE_MAX_COUNTS;

//@ Constant: PSOL_CURR_MIN_COUNTS
// The minimum valid PSOL current reading in counts --
// used for the air and oxygen PSOLs
extern const AdcCounts PSOL_CURR_MIN_COUNTS;

//@ Constant: PSOL_CURR_MAX_COUNTS
// The maximum valid PSOL current reading in counts --
// used for the air and oxygen PSOLs
extern const AdcCounts PSOL_CURR_MAX_COUNTS;

//@ Constant: EXH_MOTOR_CURR_MIN_COUNTS
// The minimum valid exhalation valve motor current reading in counts
extern const AdcCounts EXH_MOTOR_CURR_MIN_COUNTS;

//@ Constant: EXH_MOTOR_CURR_MAX_COUNTS
// The maximum valid exhalation valve motor current reading in counts
extern const AdcCounts EXH_MOTOR_CURR_MAX_COUNTS;

//@ Constant: SENTRY_10V_MIN_COUNTS
// The minimum valid 10V Sentry  reading in counts
extern const AdcCounts SENTRY_10V_MIN_COUNTS;

//@ Constant: SENTRY_10V_MAX_COUNTS
// The maximum valid 10V Sentry  reading in counts
extern const AdcCounts SENTRY_10V_MAX_COUNTS;

//@ Constant: EXH_COIL_TEMP_MIN_COUNTS
// The minimum exhalation coil temperature reading in counts
extern const AdcCounts EXH_COIL_TEMP_MIN_COUNTS;

//@ Constant: EXH_COIL_TEMP_MAX_COUNTS
// The maximum exhalation coil temperature reading in counts
extern const AdcCounts EXH_COIL_TEMP_MAX_COUNTS;

//@ Constant: O2_SENSOR_MIN_COUNTS
// The minimum oxygen sensor reading in counts
extern const AdcCounts O2_SENSOR_MIN_COUNTS;

//@ Constant: O2_SENSOR_MAX_COUNTS
// The maximum oxygen sensor reading in counts
extern const AdcCounts O2_SENSOR_MAX_COUNTS;

//@ Constant: SENTRY_5V_MIN_COUNTS
// The minimum valid 5V Sentry  reading in counts
extern const AdcCounts SENTRY_5V_MIN_COUNTS;

//@ Constant: SENTRY_5V_MAX_COUNTS
// The maximum valid 5V Sentry  reading in counts
extern const AdcCounts SENTRY_5V_MAX_COUNTS;

//@ Constant: SV_LOOPBACK_MIN_COUNTS
// The minimum valid safety valve loopback current reading in counts
extern const AdcCounts SV_LOOPBACK_MIN_COUNTS;

//@ Constant: SV_LOOPBACK_MAX_COUNTS
// The maximum valid safety valve loopback current reading in counts
extern const AdcCounts SV_LOOPBACK_MAX_COUNTS;

//@ Constant: SENTRY_15V_MIN_COUNTS
// The minimum valid 15V and -15V Sentry  reading in counts
extern const AdcCounts SENTRY_15V_MIN_COUNTS;

//@ Constant: SENTRY_15V_MAX_COUNTS
// The maximum valid 15V and -15V Sentry  reading in counts
extern const AdcCounts SENTRY_15V_MAX_COUNTS;

//@ Constant: PF_CAP_VOLT_MIN_COUNTS
// The minimum valid powerfail capacitor reading in counts
extern const AdcCounts PF_CAP_VOLT_MIN_COUNTS;

//@ Constant: PF_CAP_VOLT_MAX_COUNTS
// The maximum valid powerfail capacitor reading in counts
extern const AdcCounts PF_CAP_VOLT_MAX_COUNTS;

//@ Constant: EXH_MAN_TEMP_MIN_COUNTS
// The minimum exhalation manafold temperature reading in counts
extern const AdcCounts EXH_MAN_TEMP_MIN_COUNTS;

//@ Constant: EXH_MAN_TEMP_MAX_COUNTS
// The maximum exhalation manafold temperature reading in counts
extern const AdcCounts EXH_MAN_TEMP_MAX_COUNTS;

//@ Constant: BPS_BATT_VOLT_MIN_COUNTS
// The minimum BPS battery voltage reading in counts
extern const AdcCounts BPS_BATT_VOLT_MIN_COUNTS;

//@ Constant: BPS_BATT_VOLT_MAX_COUNTS
// The maximum BPS battery voltage reading in counts
extern const AdcCounts BPS_BATT_VOLT_MAX_COUNTS;

//@ Constant: BPS_BATT_CURR_MIN_COUNTS
// The minimum BPS battery current reading in counts
extern const AdcCounts BPS_BATT_CURR_MIN_COUNTS;

//@ Constant: BPS_BATT_CURR_MAX_COUNTS
// The maximum BPS battery current reading in counts
extern const AdcCounts BPS_BATT_CURR_MAX_COUNTS;

//@ Constant: LV_REF_MIN_COUNTS
// The minimum low voltage reference reading in counts
extern const AdcCounts LV_REF_MIN_COUNTS;

//@ Constant: LV_REF_MAX_COUNTS
// The maximum low voltage reference reading in counts
extern const AdcCounts LV_REF_MAX_COUNTS;

//@ Constant: BPS_MODEL_MIN_COUNTS
// The minimum BPS model signal reading in counts
extern const AdcCounts BPS_MODEL_MIN_COUNTS;

//@ Constant: BPS_MODEL_MAX_COUNTS
// The maximum BPS model signal reading in counts
extern const AdcCounts BPS_MODEL_MAX_COUNTS;

//@ Constant: ATM_PRESS_MIN_COUNTS
// The minimum atmospheric pressure reading in counts
extern const AdcCounts ATM_PRESS_MIN_COUNTS;

//@ Constant: ATM_PRESS_MAX_COUNTS
// The maximum atmospheric pressure reading in counts
extern const AdcCounts ATM_PRESS_MAX_COUNTS;

//@ Constant: SENTRY_GUI_12V_MIN_COUNTS
// The minimum valid GUI 12V Sentry  reading in counts
extern const AdcCounts SENTRY_GUI_12V_MIN_COUNTS;

//@ Constant: SENTRY_GUI_12V_MAX_COUNTS
// The maximum valid GUI 12V Sentry  reading in counts
extern const AdcCounts SENTRY_GUI_12V_MAX_COUNTS;

//@ Constant: SENTRY_GUI_5V_MIN_COUNTS
// The minimum valid GUI 5V Sentry  reading in counts
extern const AdcCounts SENTRY_GUI_5V_MIN_COUNTS;

//@ Constant: SENTRY_GUI_5V_MAX_COUNTS
// The maximum valid GUI 5V Sentry  reading in counts
extern const AdcCounts SENTRY_GUI_5V_MAX_COUNTS;


class SafetyNetSensorData
{
  public:

    SafetyNetSensorData (const AdcCounts minAdcCounts,
                         const AdcCounts maxAdcCounts,
                         const AdcCounts defaultAdcCounts,
                         const Int16 maxCyclesOor,
                         const BkEventName eventId,
                         const Boolean checkedInSensorNewCycle,
                         const Boolean useDefaultValue);

    ~SafetyNetSensorData (void);

    static void SoftFault (const SoftFaultID softFaultID,
						   const Uint32 lineNumber,
						   const char* pFileName  = NULL,
						   const char* pPredicate = NULL) ;

    void checkRange (AdcCounts &rawCount);    

    inline BkEventName getBackgndEventId (void);
    inline void resetNumCyclesOor (void);
    inline Boolean getBackgndEventReported (void) const;
    inline void setBackgndEventReported (const Boolean flag);
    inline void setBackgndCheckFailed (const Boolean checkStatus);
    inline Boolean getBackgndCheckFailed (void) const;
    inline void setCheckedInSensorNewCycle(const Boolean newStatus);
	inline Uint16 getLastRawCountReading(void) const;
  
    void sensorReadingRangeCheck (const AdcCounts rawCount);
  
  protected:

  private:

    // these methods are purposely declared, but not implemented...
    SafetyNetSensorData (void);                         // not implemented
    SafetyNetSensorData (const SafetyNetSensorData&);   // not implemented...
    void operator= (const SafetyNetSensorData&);        // not implemented...
   
    //@ Data-Member: maxAdcCounts_
    //the maximum allowed ADC count (after ADC offset correction) value for the sensor
    AdcCounts maxAdcCounts_ ;

    //@ Data-Member: minAdcCounts_
    //the minimum allowed ADC count (after ADC offset correction)  value for the sensor
    AdcCounts minAdcCounts_ ;

    //@ Data-Member: maxCyclesOor_
    //the maximum number of BD cycles, breath or time cycles that the sensor reading
    //is allowed to be less than minAdcCounts_ or greater than maxAdcCounts_;
    //if the check is performed by the checkRange() method, this value always
    //corresponds to the maximum number of BD cycles but if the check is performed
    //via a SafetyNetRangeTest check, maxCyclesOor_ represents BD cycles or breath
    //cycles depending upon implementation; and if the check is performed by
    //the Background susbystem maxCyclesOor_ represents time cycles (eg. number of
    //10 second intervals)
    Int16  maxCyclesOor_ ;

    //@ Data-Member: numCyclesOorHigh_
    //the number of consecutive BD, breath or time  cycles that the sensor reading has
    //been out of range on the high end
    Int16 numCyclesOorHigh_ ;

    //the number of consecutive BD, breath or time  cycles that the sensor reading has
    //been out of range on the low end
    Int16 numCyclesOorLow_ ;

    //@ Data-Member: backgndEventId_
    //the Background Identifier used to inform the Background subsystem when
    // the sensor is OOR for maxCyclesOor_
    BkEventName backgndEventId_ ;

    //@ Data-Member: backgndCheckFailed_
    //the Background Check Failed indicator; once the background test for a
    //sensor has failed, this flag is set to TRUE
    Boolean backgndCheckFailed_ ;

    //@ Data-Member: defaultAdcCounts_
    //the sensor counts value used if the test fails
    AdcCounts defaultAdcCounts_ ;

    //@ Data-Member: useDefaultValue_
    //flag that indicates if the defaultAdcCounts_ value should be used if the
    //background test fails
    Boolean useDefaultValue_ ;

    //@ Data-Member: checkedInSensorNewCycle_
    //flag that indicates if the sensor is checked via the newCycle method of the Sensor
    //class; if the check only depends upon the sensor reading and needs to be performed
    //every BD cycle, this flag is TRUE; otherwise, the flag is FALSE to indicate that
    //the check is performed somewhere else (by Background or the SafetyNetTestMediator)
    Boolean checkedInSensorNewCycle_ ;

    //@ Data-Member: backgndEventReported_
    //flag that indicates if the background event has already been reported to the
    //Background subsystem; the Background subsystem is only notified once of a
    //background event
    Boolean backgndEventReported_ ;

    //@ Data-Member: firstReadingStored_
    //flag that indicates if at least one sensor reading has been stored -- ie.
    //the values have been initialized to something.
    Boolean firstReadingStored_ ;

	//@ Data-Member: lastRawCountBeforeFailure_
	// last raw count before reporting error
	AdcCounts lastRawCountBeforeFailure_ ;
	
	//@ Data-Member: lastRawCountReading_
	// last raw count store for error code
	AdcCounts lastRawCountReading_ ;
};


// Inlined methods
#include "SafetyNetSensorData.in"

#endif // SafetyNetSensorData_HH 




