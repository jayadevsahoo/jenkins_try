
#ifndef PatientCctTypeValue_HH
#define PatientCctTypeValue_HH

//====================================================================
//
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  PatientCctTypeValue - Values of Patient Circuit Type Setting.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/PatientCctTypeValue.hhv   25.0.4.0   19 Nov 2013 14:27:32   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002   By: sah    Date: 29-Jun-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  added new 'NEONATAL_CIRCUIT' value
//	*  re-ordered values to separate standard values from optional
//         values; now matches order in SRS
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//      Initial version.
//  
//====================================================================

//@ Usage-Classes
//@ End-Usage


struct PatientCctTypeValue
{
  //@ Type:  PatientCctTypeValueId
  // All of the possible values of the Patient Circuit Type Setting.
  enum PatientCctTypeValueId
  {
    // $[02213] -- values of the range of Patient Circuit Type Setting...

    // standard values...
    ADULT_CIRCUIT,
    PEDIATRIC_CIRCUIT,

    // optional values...
    NEONATAL_CIRCUIT,

    TOTAL_CIRCUIT_TYPES
  };
};


#endif // PatientCctTypeValue_HH 
