//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
//
// Use of this sample source code is subject to the terms of the Microsoft
// license agreement under which you licensed this sample source code. If
// you did not accept the terms of the license agreement, you are not
// authorized to use this sample source code. For the terms of the license,
// please see the license agreement between you and Microsoft or, if applicable,
// see the LICENSE.RTF on your install media or the root of your tools installation.
// THE SAMPLE SOURCE CODE IS PROVIDED "AS IS", WITH NO WARRANTIES OR INDEMNITIES.
//

#ifndef _MOUHID_H_
#define _MOUSEHID_H_

#include "hidpi.h"
#include "hiddi.h"
#include <cedrv_guid.h>
#include <tchddsi.h>
#include <tchstream.h>
#include <tchstreamddsi.h>

#define HID_MOUSE_SIG 'uoMH' // "HKbd"

// Hid mouse data structure. One for each mouse TLC.
typedef struct _HID_MOUSE {
    DWORD dwSig;
    HID_HANDLE hDevice;
    PCHID_FUNCS pHidFuncs;
    PHIDP_PREPARSED_DATA phidpPreparsedData;
    HIDP_CAPS hidpCaps;
    HANDLE hThread;

    DWORD dwMaxUsages;
    DWORD dwInstanceId;
    PUSAGE puPrevUsages;
    PUSAGE puCurrUsages;
    PUSAGE puBreakUsages;
    PUSAGE puMakeUsages;

    USAGE usageWheel; // The usage used to get the wheel movement.
    
#ifdef DEBUG
    BOOL fhThreadInited;
#endif
} HID_MOUSE, *PHID_MOUSE;

#define VALID_HID_MOUSE( p ) \
   ( p && (HID_MOUSE_SIG == p->dwSig) )

#define HAS_NO_WHEEL 0

#define HID_MOUSE_NAME TEXT("HID MOUSE")

#ifndef dim
// Prefer to use _countof() directly, replacing dim()
#pragma deprecated("dim")
#define dim(x) _countof(x)
#endif

#ifdef DEBUG

extern DBGPARAM dpCurSettings;

#define SETFNAME(name)          const TCHAR * const pszFname = (name)

//#define ZONE_ERROR              DEBUGZONE(0)
//#define ZONE_WARNING            DEBUGZONE(1)
//#define ZONE_INIT               DEBUGZONE(2)
//#define ZONE_FUNCTION           DEBUGZONE(3)

#define ZONE_USAGES             DEBUGZONE(4)

#else

#define SETFNAME(name)

#endif // DEBUG

#pragma pack(1)

typedef struct point_data {
    char tip_tid;
    unsigned short int x;
    unsigned short int y;
} POINT_DATA;

#define TP_NUM		5

struct hid_tr_as01_packet {
    char report_id;
    POINT_DATA touch_point[TP_NUM];
    unsigned short unknown;
    unsigned short point_num;
};

#pragma pack()


//------------------------------------------------------------------------------
// Sample rate during polling.
#define DEFAULT_SAMPLE_RATE                 200               // Hz.
#define TOUCHPANEL_SAMPLE_RATE_LOW          DEFAULT_SAMPLE_RATE
#define TOUCHPANEL_SAMPLE_RATE_HIGH         DEFAULT_SAMPLE_RATE
#define DEFAULT_THREAD_PRIORITY             109

#define CAL_DELTA_RESET             20
#define CAL_HOLD_STEADY_TIME        1500
#define RANGE_MIN                   0
#define RANGE_MAX                   4096

//------------------------------------------------------------------------------
// local data structures
//

typedef struct
{
    BOOL        bInitialized;

    DWORD       nSampleRate;
    DWORD       nInitialSamplesDropped;
    DWORD       PenUpDebounceMS;
//    DWORD       dwSysIntr;
    DWORD       dwSamplingTimeOut;
    BOOL        bTerminateIST;
    HANDLE      hTouchPanelEvent;
//    DWORD       dwPowerState;
    HANDLE      hIST;
    DWORD       dwISTPriority;
}TOUCH_DEVICE;


//------------------------------------------------------------------------------
// global variables
//
static TOUCH_DEVICE s_TouchDevice =  {
    FALSE,                                          //bInitialized
    DEFAULT_SAMPLE_RATE,                            //nSampleRate
    0,                                              //nInitialSamplesDropped
    0,                                              //PenUpDebounceMS
    0,                                              //dwSamplingTimeOut
    FALSE,                                          //bTerminateIST
    0,                                              //hTouchPanelEvent
    0,                                              //hIST
    DEFAULT_THREAD_PRIORITY                         //dwISTPriority
};


//TCH PDD DDSI functions
extern "C" DWORD WINAPI TchPdd_Init(
    LPCTSTR pszActiveKey,
    TCH_MDD_INTERFACE_INFO* pMddIfc,
    TCH_PDD_INTERFACE_INFO* pPddIfc,
    DWORD hMddContext
    );

void WINAPI TchPdd_Deinit(DWORD hPddContext);
void WINAPI TchPdd_PowerUp(DWORD hPddContext);
void WINAPI TchPdd_PowerDown(DWORD hPddContext);
BOOL WINAPI TchPdd_Ioctl(
    DWORD hPddContext,
    DWORD dwCode,
    PBYTE pBufIn,
    DWORD dwLenIn,
    PBYTE pBufOut,
    DWORD dwLenOut,
    PDWORD pdwActualOut
    );

#endif

