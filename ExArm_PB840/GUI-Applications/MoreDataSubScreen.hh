#ifndef MoreDataSubScreen_HH
#define MoreDataSubScreen_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1994, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: MoreDataSubScreen - Subscreen for displaying supplemental Patient Data.
//---------------------------------------------------------------------
//@	Version
//@(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/MoreDataSubScreen.hhv   25.0.4.0   19 Nov 2013 14:08:08   pvcs  $
//
//@ Modification-Log
//
//  Revision: 019   By: rhj    Date: 05-Apr-2011   SCR Number: 6759
//  Project:  PROX
//  Description:
//      Changed updateInspiredVolumeLabel_ to updateVolumeLabel_.
//  
//  Revision: 018   By: rhj    Date: 10-Sept-2010   SCR Number: 6631 
//  Project:  PROX
//  Description:
//      Added proxStatusMsg_.
//
//  Revision: 017   By: rhj    Date: 18-Feb-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//      Modified to support PROX related changes.
//
//  Revision: 016   By: gdc    Date:  11-Aug-2008   SCR Number: 6439
//  Project:  840S
//  Description:
//      Removed touch screen "drill-down" work-around.
//
//  Revision: 015   By: rhj    Date: 07-July-2008   SCR Number: 6435
//  Project:  LEAK
//  Description:
//      Modified to support Leak Compensation.
//       
//  Revision: 014   By: rhj    Date:  10-May-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//      RESPM related changes.
//
//  Revision: 013   By: gdc    Date:  15-Feb-2005    DR Number: 6144
//  Project:  NIV1
//  Description:
//      DCS 6144 - NIV1
//      added PEEP display for NIV
//
//  Revision: 012   By: emccoy Date:  27-July-2004    DR Number: 6138
//  Project:  PAV3
//  Description:
//      Project PAV3 DCS 6138
//      added support for ft/vte/kg
//
//  Revision: 011   By: heatherw Date:  08-Apr-2002    DR Number: 5806
//  Project:  VCP
//  Description:
//      Added a method to handle setting applicabilty.  This method
//      was needed to handle settings after a reset condition.
//
//  Revision: 010   By: yakovb   Date:  12-Mar-2002    DR Number: 5860
//  Project:  VCP
//  Description:
//      Handle VC+/VS Startup messages.
//
//  Revision: 009   By: sah   Date:  16-Aug-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      *  added a high-level, state-management method, 'updateShowStates_()',
//         and a method for updating the various data values,
//         'updateDataValue_()'
//      *  added new SPONT-based data items:  inspiratory time, inspiratory
//         time ratio, and rapid/shallow breathing index
//
//  Revision: 008   By: sah   Date:  18-Jul-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//      PAV project-related changes:
//      *  added new PAV-based data items:  compliance, resistance,
//         elastance and work-of-breathing
//
//  Revision: 007  By:  sah	   Date:  10-Apr-2000    DCS Number: 5691
//  Project:  NeoMode
//  Description:
//      Removed 'PEEP' from this subscreen; PEEP is now back in VPDA.
//
//  Revision: 006  By:  yyy	   Date:  29-Jun-1998    DCS Number: 5327
//  Project:  NeoMode
//  Description:
//      Initial version.
//
//  Revision: 005  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 003  By: yyy      Date: 01-Dec-1998  DR Number: 5286
//    Project:  BiLevel (R8027)
//    Description:
//     Added a previousPData_ data member.
//
//  Revision: 002  By:  yyy    Date:  01-Sep-1998    DR Number:
//       Project:  BiLevel (R8027)
//       Description:
//             Initial release.
//
// Revision: 001  By:  wks    Date:  01-APR-94    DR Number: 
// Project:  Sigma (R8027)
// Description:
//             Integration baseline.
//====================================================================

#include "BdEventTarget.hh"
#include "PatientDataTarget.hh"
#include "SubScreen.hh"
#include "GuiEventTarget.hh"

//@ Usage-Classes
#include "NumericField.hh"
#include "TouchableText.hh"
#include "ContextObserver.hh"
//@ End-Usage
#include "TextButton.hh"
#include "RmDataSubScreen.hh"

class MoreDataSubScreen : public SubScreen,
							public BdEventTarget,
							public GuiEventTarget,
							public PatientDataTarget,
							public ContextObserver
{
public:
	MoreDataSubScreen(SubScreenArea* pSubScreenArea);
	~MoreDataSubScreen(void);

	virtual void activate(void);
	virtual void deactivate(void);

	// ContextObserver virtual method...
	virtual void  batchSettingUpdate(
							 const Notification::ChangeQualifier qualifierId,
							 const ContextSubject*               pSubject,
							 const SettingId::SettingIdType      settingId
									);

	virtual void patientDataChangeHappened(
							PatientDataId::PatientItemId patientDataId);

	virtual void guiEventHappened(GuiApp::GuiAppEvent eventId);

	virtual void bdEventHappened(EventData::EventId eventId,
				                	EventData::EventStatus eventStatus,
									EventData::EventPrompt eventPrompt=EventData::NULL_EVENT_PROMPT);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

    virtual void buttonDownHappened(Button* pButton,
									Boolean isByOperatorAction);

    static inline void setLastDisplay(Uint32 classId);

protected:

private:
	// these methods are purposely declared, but not implemented...
	MoreDataSubScreen(void);						// not implemented...
	MoreDataSubScreen(const MoreDataSubScreen&);	// not implemented...
	void operator=(const MoreDataSubScreen&);		// not implemented...

	void  updateShowStates_(void);
	void  updateDataValue_ (const PatientDataId::PatientItemId  patientDataId);
	void  updateVolumeLabel_(void);
	void  updateSettingApplicability_(void);
    void  displayRmDataSubScreen_(void);

	//@ Data-Member: endInspiratoryPressureLabel_
	// Label for end inspiratory pressure field.
	TouchableText endInspiratoryPressureLabel_;

	//@ Data-Member: endInspiratoryPressureValue_
	// Value for end inspiratory pressure field.
   	NumericField endInspiratoryPressureValue_;

	//@ Data-Member: endInspiratoryPressureUnit_
	// Label for end inspiratory pressure unit.
	TextField  endInspiratoryPressureUnit_;

	//@ Data-Member: endExhalationPressureLabel_
	// Label for end exhalation pressure field.
	TouchableText endExhalationPressureLabel_;

	//@ Data-Member: endExhalationPressureValue_
	// Value for end exhalation pressure field.
   	NumericField endExhalationPressureValue_;

	//@ Data-Member: endExhalationPressureUnit_
	// Label for end exhalation pressure unit.
	TextField  endExhalationPressureUnit_;

	//@ Data-Member:: spontMinuteVolumeLabel_
	// label for the spontaneous minute volume
	TouchableText spontMinuteVolumeLabel_;

	//@ Data-Member:: spontMinuteVolumeValue_
	// value for the spontaneous minute volume
	NumericField spontMinuteVolumeValue_;

	//@ Data-Member:: spontMinuteVolumeUnit_
	// label for the spontaneous minute volume unit
	TextField  spontMinuteVolumeUnit_;

    //@ Data-Member:: deliveredOxygenPercentLabel_
    // label for Delivered Oxygen Percentage  
    TouchableText deliveredOxygenPercentLabel_;
 
    //@ Data-Member:: deliveredOxygenPercentValue_
    // value for Delivered Oxygen Percentage  
    NumericField deliveredOxygenPercentValue_;
    
    //@ Data-Member:: deliveredOxygenPercentUnit_
    // unit for Delivered Oxygen Percentage  
    TextField  deliveredOxygenPercentUnit_;

    //@ Data-Member:: inspTidalVolLabel_
    // label for the Insp Tidal Volume
    TouchableText inspTidalVolLabel_;

    //@ Data-Member:: inspTidalVolValue_
    // value for Insp Tidal Volume
    NumericField inspTidalVolValue_;

    //@ Data-Member:: inspTidalVolUnit_
    // label for the Insp Tidal Volume unit
    TextField inspTidalVolUnit_;

    //@ Data-Member:  pavCompliance{Label,Value,Unit,Marker}_
    // Label/Value/Unit for the PAV Compliance.
    TouchableText  pavComplianceLabel_;
    NumericField   pavComplianceValue_;
    TextField      pavComplianceUnit_;
    TextField      pavComplianceMarker_;

    //@ Data-Member:  pavElastance{Label,Value,Unit,Marker}_
    // Label/Value/Unit for the PAV Elastance.
    TouchableText  pavElastanceLabel_;
    NumericField   pavElastanceValue_;
    TextField      pavElastanceUnit_;
    TextField      pavElastanceMarker_;

    //@ Data-Member:  pavResistance{Label,Value,Unit,Marker}_
    // Label/Value/Unit for the PAV Resistance.
    TouchableText  pavResistanceLabel_;
    NumericField   pavResistanceValue_;
    TextField      pavResistanceUnit_;
    TextField      pavResistanceMarker_;

    //@ Data-Member:  totalResistance{Label,Value,Unit,Marker}_
    // Label/Value/Unit for the Total Resistance.
    TouchableText  totalResistanceLabel_;
    NumericField   totalResistanceValue_;
    TextField      totalResistanceUnit_;
    TextField      totalResistanceMarker_;

    //@ Data-Member:  pavIntrinsicPeep{Label,Value,Unit}_
    // Label/Value/Unit for the PAV Work of Breathing.
    TouchableText  pavIntrinsicPeepLabel_;
    NumericField   pavIntrinsicPeepValue_;
    TextField      pavIntrinsicPeepUnit_;

	//@ Data-Member: pavStatusMsg_
	// Status message displaying the current state of PAV.
	TextField pavStatusMsg_;

    //@ Data-Member:  totalWorkOfBreathing{Label,Value,Unit}_
    // Label/Value/Unit for the total Work of Breathing.
    TouchableText  totalWorkOfBreathingLabel_;
    NumericField   totalWorkOfBreathingValue_;
    TextField      totalWorkOfBreathingUnit_;

	//@ Data-Member: vcpStatusMsg_
	// Status message displaying the current state of VCP.
	TextField vcpStatusMsg_;

	//@ Data-Member: vsStatusMsg_
	// Status message displaying the current state of VS.
	TextField vsStatusMsg_;

    //@ Data-Member:  spontPercentTi{Label,Value}_
    // Label/Value for Spontaneous Percent Insp Time.
    TouchableText  spontPercentTiLabel_;
    NumericField   spontPercentTiValue_;

    //@ Data-Member:  spontFvRatio{Label,Value}_
    // Label/Value for Spontaneous Rapid/Shallow Breathing Index.
    TouchableText  spontFvRatioLabel_;
    NumericField   spontFvRatioValue_;

    //@ Data-Member:  normalizeSpontRatio{Label,Value}_
    // Label/Value for normalize Rapid/Shallow Breathing Index.
    TouchableText  normalizeSpontFvRatioLabel_;
    NumericField   normalizeSpontFvRatioValue_;
    
    //@ Data-Member:  spontInspTime{Label,Value,Unit}_
    // Label/Value/Unit for Spontaneous Inspiratory Time.
    TouchableText  spontInspTimeLabel_;
    NumericField   spontInspTimeValue_;
    TextField      spontInspTimeUnit_;

    //@ Data-Member:  percentLeak{Label,Value,Unit}_
    // Label/Value/Unit for the Percent Leak.
    TouchableText  percentLeakLabel_;
    NumericField   percentLeakValue_;
    TextField      percentLeakUnit_;

	//@ Data-Member:  exhLeakRate{Label,Value,Unit}_
    // Label/Value/Unit for the exhalation leak rate.
    TouchableText  exhLeakRateLabel_;
    NumericField   exhLeakRateValue_;
    TextField      exhLeakRateUnit_;

	//@ Data-Member:  inspLeakVol{Label,Value,Unit}_
    // Label/Value/Unit for the inspiratory leak volume.
    TouchableText  inspLeakVolLabel_;
    NumericField   inspLeakVolValue_;
    TextField      inspLeakVolUnit_;

    //@ Data-Member:  RmButton_
	// A RM button to allow transition from the More Data Subscreen screen
	// to the RM data Subscreen.
	TextButton RmButton_;

    //@ Data-Member:: previousPData_
    // Flag indicates the previous patient data value.
	Int32 previousPData_;

    //@ Data-Member:: lastDisplay_
    // Flag indicates the previous subscreen.
    static Uint32 lastDisplay_;

	//@ Data-Member: isProxInop_
	// Is proximal board inoperative?
	Boolean isProxInop_;

	//@ Data-Member: proxStatusMsg_
	// Status message displaying the current state of PROX.
	TextField proxStatusMsg_;

};
#include "MoreDataSubScreen.in"

#endif // MoreDataSubScreen_HH 
