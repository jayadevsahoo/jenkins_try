
// EBMDlg.h : header file
//

#pragma once

#include "UdpCommunication.h"

#define		WM_WAIT_DATA		(WM_APP+0x01)
#define		WM_BATTERY_INFO		(WM_APP+0x02)
#define     WM_PARTIAL_DATA     (WM_APP+0x03)

#define     COLOR_YELLOW        RGB(255, 255, 0)
#define     COLOR_RED           RGB(255, 0,   0)
#define     COLOR_GREEN         RGB(0,   255, 0)
#define     COLOR_GRAY          RGB(128, 128, 128)
#define     LIGHT_COUNT         (3)

enum StatusMsgId
{
	INITIALIZATING,
	WAITING_FOR_NETWORK_DATA,
	DATA_RECEVING,
    PARTIAL_DATA_RECEIVED,
};

// CEBMDlg dialog
class CEBMDlg : public CDialog
{
// Construction
public:
	CEBMDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_EBM_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support

	static UINT BatteryInfoThreadProc( LPVOID lParam );


// Implementation
protected:
	HICON	m_hIcon;

	StatusMsgId	statusMsgId_;
	
	static	UdpCommunication	UdpComm_;
	static	void InitializeUdp_();

	void	setStatusMsg_( StatusMsgId statusMsgId, bool forceUpdate = false );
	const TCHAR* getMsgText_( StatusMsgId statusMsgId );

    void    setIndicatorColor_( COLORREF color );

    CRect   indicatorRect_;
    CRect   indicatorRectUp_;
    CRect   indicatorRectDown_;

    CRect   lightsRect_[LIGHT_COUNT];

    COLORREF    indicatorColor_;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();

	LRESULT OnWaitData(WPARAM w, LPARAM l);
	LRESULT OnBatteryInfoReceived(WPARAM w, LPARAM l);
    LRESULT OnPartialDataReceived(WPARAM w, LPARAM l);

	DECLARE_MESSAGE_MAP()
public:
    afx_msg BOOL OnEraseBkgnd(CDC* pDC);
    afx_msg void OnBnClickedCancel();
};
