#ifndef BiLevelScheduler_HH
#define BiLevelScheduler_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: BiLevelScheduler - the active BreathPhaseScheduler when the 
//  current mode is BiLevel.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/BiLevelScheduler.hhv   25.0.4.0   19 Nov 2013 13:59:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001  By:  yyy    Date:  10-Dec-1997    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//====================================================================

#  include "Sigma.hh"
#  include "Breath_Delivery.hh"
#  include "BreathType.hh"
#  include "BreathPhaseScheduler.hh"
#  include "TimerTarget.hh"
#  include "BreathPhaseType.hh"
#  include "Peep.hh"

//@ Usage-Classes
class Trigger;
class BreathPhase;
class IntervalTimer;
//@ End-Usage

class BiLevelScheduler : public BreathPhaseScheduler, public TimerTarget
{
  public:
	BiLevelScheduler(IntervalTimer& rSyncTimer);
	virtual ~BiLevelScheduler(void);

	enum BiLevelInterval {NULL_INTERVAL, SYNC_INTERVAL, SPONT_INTERVAL};

	static void SoftFault(const SoftFaultID softFaultID,
			      const Uint32      lineNumber,
			      const char*       pFileName  = NULL, 
			      const char*       pPredicate = NULL);

    ::BreathType getFirstBreathType(void) const;  
	virtual void determineBreathPhase(const BreathTrigger& breathTrigger);
	virtual void relinquishControl(const ModeTrigger& modeTrigger);
	virtual void timeUpHappened(const IntervalTimer& timer);
	virtual void takeControl(const BreathPhaseScheduler& scheduler);

  protected:
	virtual EventData::EventStatus reportEventStatus_(const EventData::EventId id,
													 const Boolean eventStatus);
	virtual void settingChangeHappened_(const SettingId::SettingIdType id);
	virtual void enableTriggers_(void);

  private:

    BiLevelScheduler(const BiLevelScheduler&);  // Declared but not implemented
    void operator=(const BiLevelScheduler&); // Declared but not implemented
    BiLevelScheduler(void); // Declared but not implemented

	void phaseInPendingBatch_(void);
	void handleInspBreath_(Trigger::TriggerId triggerId,
							::BreathType &breathType);
	void startInspiration_(const BreathTrigger& breathTrigger,
							BreathPhase* pBreathPhase,
							BreathType breathType);
								 
	void handleExpBreath_(Trigger::TriggerId triggerId,
							::BreathType breathType);
	void startExhalation_(const BreathTrigger& breathTrigger,
							BreathPhase* pBreathPhase,
							const Boolean noInspiration=FALSE);

	void setPeepTransition_(BreathType breathType= NON_MEASURED);
	void enableBilevelTrigger_(void);
	void adjustIntervalForRateChange_(void);
	void setSyncCycleTime_(void);
	void determineBiLevelPhase_(const BreathTrigger& breathTrigger,
								BreathPhaseType::PhaseType phase,
								Boolean isFirstMandBreath,
								Boolean noInspiration=FALSE);
	
    //@ Data-Member: peepState_
    // current peep state.
    Peep::PeepState peepState_;

    //@ Data-Member: newPeepState_
    // next peep state.
    Peep::PeepState newPeepState_;

    //@ Data-Member: readyForSettingChange_
    // Boolean flag indicates setting change is executable.
    Boolean readyForSettingChange_;

    //@ Data-Member: firstBreathType_
	//mandatory breath type on transition to high peep
    ::BreathType firstBreathType_;

    //@ Data-Member: currentBreathType_
	// The breath type of the current breath
    ::BreathType currentBreathType_;

	//@ Data-Member:&rCycleTimer_
	// a reference to the server IntervalTimer
	IntervalTimer& rCycleTimer_;

	//@ Data-Member: biLevelPeepCycleTimeMs_
	// length of current peep cycle interval
	Int32 biLevelPeepCycleTimeMs_;

	//@ Data-Member: syncCycleTimeMs_
	// length of current sync cycle interval
	Int32 syncCycleTimeMs_;

	//@ Data-Member: highPeepSpontCounter_
	// A flag to setup the bilevel phase in breath record when
	// peep is high.  When this value is 1, we let the breath
	// record know that the bilevel is in inspiration pase for
	// the first spont breath.  When this value is greater than
	// 1, we know let breath record know that the bilevel is
	// handling a normal breath (not first spont breath).
	Int32 highPeepSpontCounter_;

    //@ Data-Member: peepHighToLowTransitionTime_
    // length of time when peep stays high
    Int32 peepHighToLowTransitionTime_;

    //@ Data-Member: startTimeOfSpontBreath_
    // length of time when spont breath starts
    Int32 startTimeOfSpontBreath_;

    //@ Data-Member: spontOccuredAtPeepLow_
    // Boolean flag indicates a spont breath occured at peep low
    Boolean spontOccuredAtPeepLow_;
    
    //@ Data-Member: peepLowChanged_
    // Boolean flag indicates peep low time is changed.
    Boolean peepLowChanged_;
    
    //@ Data-Member: peepHighChanged_
    // Boolean flag indicates peep high time is changed.
    Boolean peepHighChanged_;

	//@ Data-Member: pendingPeepHighTimeMs_
	// length of pending peep cycle interval during peep high.
	Int32 pendingPeepHighTimeMs_;
    
	//@ Data-Member: pendingPeepLowTimeMs_
	// length of pending peep cycle interval during peep low.
	Int32 pendingPeepLowTimeMs_;
    
	//@ Data-Member: phasedInPeepHighTimeMs_
	// length of current peep cycle interval during peep high.
	Int32 phasedInPeepHighTimeMs_;
    
	//@ Data-Member: phasedInPeepLowTimeMs_
	// length of current peep cycle interval during peep low.
	Int32 phasedInPeepLowTimeMs_;
	//@ Data-Member: highPressureOccured_
	// Boolean flag indicates the high peak condition occurred.
	Boolean highPressureOccured_;
    
};


#endif // BiLevelScheduler_HH 
