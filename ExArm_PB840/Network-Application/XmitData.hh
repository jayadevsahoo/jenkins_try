#ifndef	XmitData_HH
#define	XmitData_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1994, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Filename: XmitData.hh - indirect way of including NetworkApp.hh
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Network-Application/vcssrc/XmitData.hhv   25.0.4.0   19 Nov 2013 14:14:36   pvcs  $
//@ Modification-Log
//
//  Revision: 001  By:  <jhv>    Date:  06-June-95    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//====================================================================
#include "NetworkMsgHeader.hh"
#include "NetworkDefs.hh"

typedef struct  XBuf
{
    NetworkMsgHeader header;
	char data[MAX_NETWORK_BUF_SIZE];

	XBuf()
	{
		header.msgId = 0;
		header.pktSize = 0;
		header.seqNumber = 0;
		memset( (void*) data, 0, MAX_NETWORK_BUF_SIZE );
	}

	void setHeader(Uint16 id, Uint16 nBytes, Uint32 seqNum)
	{
		header.msgId = id;
		header.pktSize = nBytes;
		header.seqNumber = seqNum;
	}

	void headerHtoN()
	{
		header.convHtoN();
	}

	void headerNtoH()
	{
		header.convNtoH();
	}

} XmitBuf;

#endif	// XmitData_HH
