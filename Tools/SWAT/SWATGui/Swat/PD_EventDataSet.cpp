// PD_EventDataSet.h : Implementation of the CPD_EventDataSet class



// CPD_EventDataSet implementation

// code generated on Friday, December 11, 2009, 9:35 AM

#include "stdafx.h"
#include "PD_EventDataSet.h"
IMPLEMENT_DYNAMIC(CPD_EventDataSet, CRecordset)

CPD_EventDataSet::CPD_EventDataSet(CDatabase* pdb)
	: CRecordset(pdb)
{
	m_event_sequence_id = 0;
	m_event_descriptor = "";
	m_event_time;
	m_save_data = "";
	m_nFields = 4;
	m_nDefaultType = dynaset;
}

CString CPD_EventDataSet::GetDefaultConnect()
{
	return _T("DSN=PostgreSQL30;DATABASE=viking_integration_test;SERVER=127.0.0.1;PORT=5432;UID=postgres;PWD=its4me@1234;SSLmode=disable;ReadOnly=0;Protocol=7.4;FakeOidIndex=0;ShowOidColumn=0;RowVersioning=0;ShowSystemTables=0;ConnSettings=;Fetch=100;Socket=4096;UnknownSizes=0;MaxVarcharSize=255;MaxLongVarcharSize=8190;Debug=0;CommLog=0;Optimizer=0;Ksqo=1;UseDeclareFetch=0;TextAsLongVarchar=1;UnknownsAsLongVarchar=0;BoolsAsChar=1;Parse=0;CancelAsFreeStmt=0;ExtraSysTablePrefixes=dd_;;LFConversion=1;UpdatableCursors=1;DisallowPremature=0;TrueIsMinus1=0;BI=0;ByteaAsLongVarBinary=0;UseServerSidePrepare=0;LowerCaseIdentifier=0;XaOpt=1");
}

CString CPD_EventDataSet::GetDefaultSQL()
{
	return _T("[PatientData].[event_data]");
}

void CPD_EventDataSet::DoFieldExchange(CFieldExchange* pFX)
{
	pFX->SetFieldType(CFieldExchange::outputColumn);
// Macros such as RFX_Text() and RFX_Int() are dependent on the
// type of the member variable, not the type of the field in the database.
// ODBC will try to automatically convert the column value to the requested type
	RFX_BigInt(pFX, _T("[event_sequence_id]"), m_event_sequence_id);
	RFX_Text(pFX, _T("[event_descriptor]"), m_event_descriptor);
	RFX_Date(pFX, _T("[event_time]"), m_event_time);
	RFX_Text(pFX, _T("[save_data]"), m_save_data);

}
/////////////////////////////////////////////////////////////////////////////
// CPD_EventDataSet diagnostics

#ifdef _DEBUG
void CPD_EventDataSet::AssertValid() const
{
	CRecordset::AssertValid();
}

void CPD_EventDataSet::Dump(CDumpContext& dc) const
{
	CRecordset::Dump(dc);
}
#endif //_DEBUG


