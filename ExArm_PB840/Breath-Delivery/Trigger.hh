#ifndef Trigger_HH
#define Trigger_HH
//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: Trigger - A trigger is an object that monitors the system for
//    a specific condition that, when it occurs, causes the state of
//    the system to change.  This class is an abstract base class for
//    any type of trigger the Breath Delivery system may need.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/Trigger.hhv   25.0.4.0   19 Nov 2013 14:00:14   pvcs  $
//
//@ Modification-Log
// 
//  Revision: 013   By:   rhj    Date: 07-Nov-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//        Added NET_FLOW_BACKUP_INSP.
// 
//  Revision: 012   By:   rhj    Date: 05-March-2007      DR Number: 6359
//  Project:  RESPM
//  Description:
//        Resolve merge conflict for RM to Baseline 
//
//  Revision: 011   By: gdc   Date:  13-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Added RM maneuver triggers.
//
//  Revision: 010   By: gdc   Date:  17-June-2005    DR Number: 6170
//  Project:  NIV2
//  Description:
//      DCS6170 - NIV2
//      Modifications to support sending trigger id to GUI patient-data
//
//  Revision: 009  By: syw     Date:  26-Aprn-1999    DR Number: 5367
//  Project:  ATC
//  Description:
//      Added PAUSE_PRESS to trigger id list
//
//  Revision: 008  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//		ATC initial release.
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 007  By:  iv    Date:  18-Jan-1998    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Bilevel initial version.
//             Added PEEP_REDUCTION_EXP and PAUSE_PRESS_EXP.
//
//  Revision: 006  By: iv    Date:  18-Jul-1997   DR Number: DCS 2298
//  	Project:  Sigma (R8027)
//		Description:
//          Added an id to RPeepRecoveryMandInspTrigger.
//
//  Revision: 005  By:  sp    Date: 2-Apr-1997    DR Number: DCS NONE
//       Project:  Sigma (R8027)
//       Description:
//             Inspection rework.
//
//  Revision: 004  By:  sp    Date:  27-Sept-1996    DR Number:600
//       Project:  Sigma (R8027)
//       Description:
//             Removed unit test method.
//
//  Revision: 003  By:  sp    Date:  4-Mar-1996    DR Number:674
//       Project:  Sigma (R8027)
//       Description:
//             Remove #ifndef.
//
//  Revision: 002  By:  kam   Date:  25-Sep-1995    DR Number:600
//       Project:  Sigma (R8027)
//       Description:
//             Updated for interface with Alarm-Analysis subsystem.
//             Add FIRST_BREATH_TRIGGER and LAST_BREATH_TRIGGER to the TriggerId.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//====================================================================

#  include "Sigma.hh"
 
#  include "Breath_Delivery.hh"

//@ End-Usage

class Trigger 
{
  public:

    enum TriggerId {
		NULL_TRIGGER_ID = 0,
    // breath triggers:
        FIRST_BREATH_TRIGGER,
        DELIVERED_FLOW_EXP = FIRST_BREATH_TRIGGER,
        HIGH_CIRCUIT_PRESSURE_EXP,
        HIGH_CIRCUIT_PRESSURE_INSP,
        IMMEDIATE_BREATH, // trigger in or out an emergency mode
        IMMEDIATE_EXP, // starts exhalation after a mandatory inspiration
        NET_FLOW_EXP, 
        NET_FLOW_INSP, 
		NET_FLOW_BACKUP_INSP,
        PAUSE_PRESS,
        PRESSURE_BACKUP_INSP, 
        PRESSURE_INSP, 
        PRESSURE_EXP, 
        SIMV_TIME_INSP,
        BL_LOW_TO_HIGH_INSP,
        BL_HIGH_TO_LOW_EXP,
        PEEP_REDUCTION_EXP, // starts exhalation to reduce peep
        ASAP_INSP, 
        OPERATOR_INSP,
        TIME_INSP,
        PAUSE_TIMEOUT,
        PAUSE_COMPLETE, 
        BACKUP_TIME_EXP, 
        HIGH_VENT_PRESSURE_EXP, 
        OSC_TIME_BACKUP_INSP,
        SVC_TIME, 
        PRESSURE_SVC,
        OSC_TIME_INSP, 
        SVC_COMPLETE,
        PEEP_RECOVERY_MAND_INSP,
        PEEP_RECOVERY,
        HIGH_PRESS_COMP_EXP,
        LUNG_FLOW_EXP,
        LUNG_VOLUME_EXP,
		ARM_MANEUVER,
		DISARM_MANEUVER,
		P100_MANEUVER,
		P100_COMPLETE,
        LAST_BREATH_TRIGGER = P100_COMPLETE,
        
	// mode triggers:
	APNEA, 
        APNEA_AUTORESET, 
        DISCONNECT, 
        DISCONNECT_AUTORESET, 
        MANUAL_RESET, 
        OCCLUSION, 
        OCCLUSION_AUTORESET, 
        SAFETY_PCV, 
        SETTING_CHANGE_MODE,
        STARTUP, 
        SVO, 
        SVO_RESET, 
        VENT_SETUP_COMPLETE,
        NUM_TRIGGER
    };

    Trigger(const TriggerId id);
    virtual ~Trigger(void);

    static void SoftFault(const SoftFaultID softFaultID,
				  const Uint32      lineNumber,
				  const char*       pFileName  = NULL,
				  const char*       pPredicate = NULL);
    inline Trigger::TriggerId getId(void) const;
    Boolean determineState(void);
    virtual void enable(void);
    virtual void disable(void);
	
#ifdef SIGMA_UNIT_TEST
    virtual void write(void);
#endif //SIGMA_UNIT_TEST


  protected:
    virtual Boolean triggerCondition_(void) = 0; //The condition monitored by the trigger
    virtual void triggerAction_(const Boolean trigCond) = 0;

    //@ Data-Member: triggerId_
    // Used for trigger identification
    TriggerId triggerId_; 

    //@ Data-Member: isEnabled_
    //Used to disable or enable a Trigger
    Boolean isEnabled_; 

  private:
	
    Trigger(void); //declared but not implemented
    Trigger(const Trigger&);    // Declared but not implemented
    void operator=(const Trigger&);   // Declared but not implemented

};

// Inlined methods
#include "Trigger.in"

#endif // Trigger_HH 
