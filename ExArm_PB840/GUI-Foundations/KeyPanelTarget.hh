#ifndef KeyPanelTarget_HH
#define KeyPanelTarget_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: KeyPanelTarget - This abstract class allows callbacks from
// the KeyPanel.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Foundations/vcssrc/KeyPanelTarget.hhv   25.0.4.0   19 Nov 2013 14:11:44   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By: yyy      Date: 01-Jan-1995  DR Number: NONE
//    Project:  Sigma (R8027)
//    Description:
//	Integration baseline.
//
//  Revision: 002  By: gdc      Date: 15-Feb-1996  DR Number: 674
//    Project:  Sigma (R8027)
//    Description:
//	Removed ifndef / endif around include directives
//====================================================================

#include "Sigma.hh"

//@ Usage-Classes
#include "KeyPanel.hh"
//@ End-Usage

class KeyPanelTarget 
{
public:
	virtual void keyPanelPressHappened(KeyPanel::KeyId keyCode);
	virtual void keyPanelReleaseHappened(KeyPanel::KeyId keyCode);

protected:
	KeyPanelTarget(void);
	virtual ~KeyPanelTarget(void);

private:
	KeyPanelTarget(const KeyPanelTarget&);	// not implemented...
	void operator=(const KeyPanelTarget&);	// not implemented...
};


#endif // KeyPanelTarget_HH 
