// LogHeaderSet.cpp : implementation file
//

#include "stdafx.h"
#include "daocpvtdb.h"
#include "LogHeaderSet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLogHeaderSet

IMPLEMENT_DYNAMIC(CLogHeaderSet, CDaoRecordset)

CLogHeaderSet::CLogHeaderSet(CDaoDatabase* pdb)
	: CDaoRecordset(pdb)
{
	//{{AFX_FIELD_INIT(CLogHeaderSet)
	m_ID = 0;
	m_FileName = _T("");
	m_BDSN = _T("");
	m_GUISN = _T("");
	m_nFields = 4;
	//}}AFX_FIELD_INIT
	m_nDefaultType = dbOpenDynaset;
}


CString CLogHeaderSet::GetDefaultDBName()
{
	return _T(DB_PATH_NAME);
}

CString CLogHeaderSet::GetDefaultSQL()
{
	return _T("[LogHeader]");
}

void CLogHeaderSet::DoFieldExchange(CDaoFieldExchange* pFX)
{
	//{{AFX_FIELD_MAP(CLogHeaderSet)
	pFX->SetFieldType(CDaoFieldExchange::outputColumn);
	DFX_Long(pFX, _T("[ID]"), m_ID);
	DFX_Text(pFX, _T("[FileName]"), m_FileName);
	DFX_Text(pFX, _T("[BDSN]"), m_BDSN);
	DFX_Text(pFX, _T("[GUISN]"), m_GUISN);
	//}}AFX_FIELD_MAP
}

/////////////////////////////////////////////////////////////////////////////
// CLogHeaderSet diagnostics

#ifdef _DEBUG
void CLogHeaderSet::AssertValid() const
{
	CDaoRecordset::AssertValid();
}

void CLogHeaderSet::Dump(CDumpContext& dc) const
{
	CDaoRecordset::Dump(dc);
}
#endif //_DEBUG
