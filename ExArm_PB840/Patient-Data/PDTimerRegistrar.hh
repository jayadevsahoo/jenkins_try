#ifndef PDTimerRegistrar_HH
#define PDTimerRegistrar_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1994, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: PDTimerRegistrar - The central registration, control, and
// dispatch point for all Patient Data timer events.  The timer events are
// received from the OS-Foundation subsystem via the GUI task's User
// Annunciation queue.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Patient-Data/vcssrc/PDTimerRegistrar.hhv   25.0.4.0   19 Nov 2013 14:17:58   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By:  gdc    Date:  12-Dec-2008    SCR Number: 6068
//  Project:  840S
//  Description:
//		Removed SUN prototype code.
//
//  Revision: 002  By: sah   Date:  13-Jan-99    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001  By:  jhv    Date:  26-June-95    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "Sigma.hh"
#include "PatientDataClassIds.hh"
#include "PDTimerRegistrar.hh"
#include "UserAnnunciationMsg.hh"
#include "PDTimerId.hh"

class PDTimerTarget;
#ifndef MsgTimer_HH
#	include "MsgTimer.hh"
#endif // MsgTimer_HH


class PDTimerRegistrar
{
public:

	struct PDTimerRecord
	{
		PDTimerRecord();
		MsgTimer 		msgTimer;	// timer object provided by Operating-Systems	
		PDTimerTarget*	pTarget;	// ptr to callback object, if any
		Boolean			isActive;	// TRUE when this is an active timer
	};

	static void TimerEventHappened(UserAnnunciationMsg &eventMsg);
	static void RegisterTarget(PDTimerId::PDTimerIdType timerId,
							   PDTimerTarget* pTarget);
	static void StartTimer(PDTimerId::PDTimerIdType timerId);
	static void CancelTimer(PDTimerId::PDTimerIdType timerId);
	static void Initialize(void);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:

private:
	// these methods are purposely declared, but not implemented...
	PDTimerRegistrar(void);						// not implemented...
	PDTimerRegistrar(const PDTimerRegistrar&);	// not implemented...
	~PDTimerRegistrar(void);						// not implemented...
	void operator=(const PDTimerRegistrar&);		// not implemented...

	//@ Data-Member: TimerArray_
	// A static array of PDTimerRecord's, indexed by the
	// PDTimerId::PDTimerIdType enum.  Each array element contains a MsgTimer
	// object, a pointer to a registered callback object and a flag showing
	// whether the timer is currently active.
	static PDTimerRecord* TimerArray_;
};

typedef PDTimerRegistrar::PDTimerRecord PDTimerRecordInRegistrar;


#endif // PDTimerRegistrar_HH 
