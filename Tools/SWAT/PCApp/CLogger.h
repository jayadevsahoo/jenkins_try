//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file CLogger.h
//----------------------------------------------------------------------------

#ifndef CLOGGER_H
#define CLOGGER_H

#include "ILogger.h"

class CLogger
{
    public:
    CLogger();
    void set(ILogger *pILogger);
    static ILogger *getLogger(){return m_ILogger;}

    private:
    static ILogger *m_ILogger;
};


#define LOG_MESSAGE(__TYPE,...)  do{                                                   \
    ILogger *pILogger=CLogger::getLogger();                                            \
    if(pILogger){                                                                      \
        pILogger->logMessage(__LINE__, __TYPE ,__FILE__, __VA_ARGS__);                 \
    }                                                                                  \
}while(0)


extern CLogger g_GlobalLogger;

#endif
