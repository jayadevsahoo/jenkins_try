
#ifndef BitAccessGpioMediator_HH
#define BitAccessGpioMediator_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
//  Class: BitAccessGpioMediator - To manage the write operations of
//			all the bit access registers.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/BitAccessGpioMediator.hhv   25.0.4.0   19 Nov 2013 13:54:00   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 001  By:  syw    Date: 19-Dec-1996    DR Number: INITIAL RELEASE
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "Sigma.hh"

//@ Usage-Classes

#include "BitAccessGpio.hh"

//@ End-Usage


class BitAccessGpioMediator {
  public:

    BitAccessGpioMediator( BitAccessGpio& rBitAccessGpio) ;
    virtual ~BitAccessGpioMediator( void) ;

    static void SoftFault( const SoftFaultID softFaultID,
						   const Uint32      lineNumber,
						   const char*       pFileName  = NULL, 
						   const char*       pPredicate = NULL) ;
	void newCycle( void) ;
	
  protected:

  private:
   	//@ Data-Member: 
   	// pointer to the BitAccessGpio instance
   	BitAccessGpio *pBitAccessGpio_;

} ;


#endif // BitAccessGpioMediator_HH 
