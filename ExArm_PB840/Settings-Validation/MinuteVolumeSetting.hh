
#ifndef MinuteVolumeSetting_HH
#define MinuteVolumeSetting_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  MinuteVolumeSetting - (Set) Minute Volume Setting.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/MinuteVolumeSetting.hhv   25.0.4.0   19 Nov 2013 14:27:30   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003   By: sah   Date:  16-Aug-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//	VTPC project-related changes:
//      *  changed this setting from a "dependent setting" of peak
//         insp flow, to using the Observer/Subject mechanism as a
//         "tracking setting"; as a tracking setting, this now needs to
//         override some 'SettingObserver' methods ('valueUpdate()',
//         'doRetainAttachment()' and 'settingObserverInit()'), and no
//         longer needs dependent and transition methods
//         ('acceptPrimaryChange()' and 'acceptTransition()')
//
//  Revision: 002   By: sah   Date:  22-Jul-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  modified to use newly-modified 'BoundedRange' class to handle
//         range, via linked list of interval nodes
//
//  Revision: 001   By: sah   Date:  26-Jan-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//     Created to work with new applicability functionality.
//
//====================================================================

//@ Usage-Classes
#include "BatchBoundedSetting.hh"
#include "SettingObserver.hh"
//@ End-Usage


class MinuteVolumeSetting : public BatchBoundedSetting, public SettingObserver
{
  public:
    MinuteVolumeSetting(void); 
    virtual ~MinuteVolumeSetting(void);

    virtual Applicability::Id  getApplicability(
			    const Notification::ChangeQualifier qualifierId
					       ) const;

    virtual SettingValue  getNewPatientValue(void) const;

    // SettingObserver methods...
    virtual void  valueUpdate(const Notification::ChangeQualifier qualifierId,
			      const SettingSubject*               pSubject);
    virtual Boolean  doRetainAttachment(const SettingSubject* pSubject) const;
    virtual void  settingObserverInit(void);

#if defined(SIGMA_DEVELOPMENT)
    virtual Boolean  isAcceptedValid(void) const;
    virtual Boolean  isAdjustedValid(void);
#endif // defined (SIGMA_DEVELOPMENT)

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL, 
			   const char*       pPredicate = NULL);

  protected:
    virtual BoundedValue  calcBasedOnSetting_(
			    const SettingId::SettingIdType basedOnSettingId,
			    const BoundedRange::WarpDir    warpDirection,
			    const BasedOnCategory          basedOnCategory
					     ) const;

  private:
    MinuteVolumeSetting(const MinuteVolumeSetting&);	// not implemented...
    void  operator=(const MinuteVolumeSetting&);	// not implemented...
};


#endif // MinuteVolumeSetting_HH 
