#ifndef OperandName_HH
#define OperandName_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Filename: OperandName - Enumerated names for AlarmOperands.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Alarm-Analysis/vcssrc/OperandName.hhv   25.0.4.0   19 Nov 2013 13:51:22   pvcs  $
//
//@ Modification-Log
//
//   Revision 001   By:   hct      Date: 01/01/95         DR Number:   n/a
//      Project:   Sigma   (R8O27)
//      Description:
//         Initial version (Integration baseline)
//  
//   Revision 002   By:   hct    Date: 05/22/95         DR Number:   7
//      Project:   Sigma   (R8027)
//      Description:
//         Added operand names to support changes to Non-Technical Alarm 
//         structure as per changes to SRS.
//  
//   Revision 003   By:   hct    Date: 08/07/96         DR Number:  1201
//      Project:   Sigma   (R8027)
//      Description:
//         Remove 1 second time delay for detection/reset of supply alarms.
//  
//   Revision 004   By:   hct    Date: 09/09/96         DR Number:  1209
//      Project:   Sigma   (R8027)
//      Description:
//         Added G4503 and G5503.
//  
//   Revision 005   By:   hct    Date: 02/26/97         DR Number:  1793
//      Project:   Sigma   (R8027)
//      Description:
//         Delete WEAK BATTERY alarm.
//  
//   Revision 006   By:   hct    Date: 09/29/97         DR Number:  1475
//      Project:   Sigma   (R8027)
//      Description:
//         Add VOLUME LIMIT alarm.
//  
//   Revision 007   By:   hct    Date: 12/16/97         DR Number:  2688
//      Project:   Sigma   (R8027)
//      Description:
//         Add hysteresis to LSP alarms.
//  
//   Revision 008   By:   hct    Date: 06/23/98         DR Number:  5109
//      Project:   Sigma   (R8027)
//      Description:
//         Changed High Circuit Pressure alarm per DCS.
//  
//   Revision 009   By:   hct      Date: 01/13/99       DR Number: 5322
//      Project:   Sigma   (R8O27)
//      Description:
//         Add ATC functionality.
//
//   Revision 010   By:   hct    Date: 08/27/99         DR Number:  5513
//      Project:   NewAlarms
//      Description:
//         Added new methods to model escalation changes in patient data alarms.
//  
//   Revision 011   By:   hct    Date: 08/11/00         DR Number:  5313
//      Project:   PAV
//      Description:
//         Added PAV functionality.
//  
//   Revision: 012  By:   hct    Date:  30-Aug-2000     DR Number: 5755
//   Project:  VTPC
//   Description:
//         Create VTPC alarms.
//
//   Revision: 012  By:   hct    Date:  11-AWP-2000     DR Number: 5755
//   Project:  VTPC
//   Description:
//         Changed VTPC alarms.
//
//   Revision: 013  By: hct      Date:  24-OCT-2000     DR Number: 5755
//   Project:  VTPC
//   Description:
//         Differentiate between spont and mand breaths for VTPC alarms.
//
//   Revision: 014  By: heatherw Date:  8-MAR-2002     DR Number: 5790
//   Project:  VTPC
//   Description:
//         Added alarms and groups for Vti Spont Pressure and Volume
//         alarms.
//
//   Revision: 015  By: heatherw Date:  11-MAR-2002     DR Number: 5790
//   Project:  VTPC
//   Description:
//         Added  support for inspired mand alarm
//
//   Revision: 016  By: ljstar   Date:  21-Aug-2003     DR Number: 6086
//   Project:  PAV3
//   Description:
//        SDCR 6086, added LOG keyword
//
//   Revision: 017  By: gdc   Date:  21-Feb-2005     DR Number: 6144
//   Project:  NIV1
//   Description:
//        DCS 6144 - NIV1
//        Modifications to support NIV.
//
//   Revision: 018  By: gdc   Date:  27-May-2005     DR Number: 6170
//   Project:  NIV2
//   Description:
//       DCS 6170 - NIV2
//       Removed Insp Too Long alarm for NIV. Added High Ti spont alert.
// 
//   Revision: 019   By:   rhj    Date: 05-March-2007      DR Number: 6359
//   Project:  RESPM
//   Description:
//       Resolve merge conflict for RM to Baseline 
//
//   Revision: 020   By:   rhj   Date: 17-Feb-2010   SCR Number: 6436
//   Project:  PROX
//   Description:
//       Added C1000 and G100000
//
//====================================================================

#  include "Sigma.hh"
#  include "Alarm_Analysis.hh"

//@ Usage-Classes
//@ End-Usage

//@ Type: OperandName
// OperandName enumerates the name_ attribute of the OperatingCondition and
// OperatingGroup objects derived from AlarmOperand.
enum OperandName
{
   OPERAND_NAME_NULL = -1,  
C5,     C10,    C15,    C30,    C35,    C40,    C45,    C50,    C55,    C60,   
C65,    C70,    C75,    C80,    C85,    C90,    C95,    C100,   C105,   C110,  
C135,   C136,   C140,   C145,   C150,   C155,   C170,   C190,   C193,   C195,
C200,   C205,   C210,   C215,   C220,   C225,   C260,   C261,   C262,   C263,
C265,   C270,   C290,   C295,   C300,   C305,   C315,   C320,   C321,   C325,
C330,   C335,   C345,   C355,   C360,   C365,   C366,   C380,
C385,   C390,   C400,	C901,   C902,   C1000, 
G500,   G1000,  G1500,  G2000,  G2500,  G3000,  G3500,  G4000,  G4500,  G4501, 
G4502,  G4503,  G5000,  G5001,  G5500,  G5501,  G5502,  G5503,  G6000,  G6001,  
G6500,  G6501,  G6502,  G7000,  G7001,  G7500,  G8000,  G8500,  G11000, G20500, 
G20501, G20502, G20503, G20504, G20505, G20508, G21000, G21001, G21002, G21003, 
G21500, G21501, G21502, G21503, G21504, G21505, G22000, G22001, G22002, G22003, 
G22004, G22500, G22508, G22509, G22510, G22511, G22512, G22513, G22514, G22520, 
G22521, G22522, G22523, G22524, G22530, G22531, G22532, G22533, G22534, G22535, 
G22536, G22537, G22538, G22539, G22540, G22550, G22551, G22552, G22560, G23000, 
G23001, G23002, G23003, G23004, G23005, G23500, G23501, G23502, G23503, G24000, 
G24001, G24002, G24100, G24300, G25000, G25001, G25500, G25501, G25502, G25503, 
G25504, G25505, G26000, G26001, G26002, G26003, G26500, G26501, G26502, G26503, 
G26504, G26505, G27000, G27001, G27002, G28000, G28001, G28002, G28003, G28004, 
G28005, G28006, G28500, G28501, G28502, G28503, G29000, G29001, G29002, G29003, 
G29004, G29500, G30000, G30001, G30500, G31000, G31001, G31002, G31003, G31004, 
G31005, G31500, G32000, G32001, G32002, G32003, G32004, G32005, G33500, G33501, 
G33502, G34500, G34501, G34502, G34503, G34504, G34505, G34506, G34507, 
G35500, G35501, G35502, G35503, G35504, G35505, G36000,
G36001, G36002, G36003, G36004, G36500, G36501, G36502, G36503, G36504, G36600,
G36601, G36602, G36603, G36604, 
G38000, G38001, G38002, G38003, G38004, G38005, G38006, G38007,
G38500, G38501, G38502, G38503, G38504, G38505,
G39000, G39001, G39002, G39003, G39004, G39005,
G40000, G40001, G40002, G40003, G40004, G40005, G100000,

MAX_NUM_OPERANDS
};



#endif // OperandName_HH 
