#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: Peep - Manages the value of Peep actually used in ventilation.
//---------------------------------------------------------------------
//@ Interface-Description
//	This class is used by any object that needs to use peep.  This class does
//	not affect the setting value.  Methods are provided to set and get the
//	actual peep and also to reset it to the setting value.
//---------------------------------------------------------------------
//@ Rationale
//  Sometimes it is necessary to adjust the value of Peep used in
//  ventilation.  For example, during Occlusion Status Cycling, it
//  must be set to 0 for the duration of Occlusion, and then reset
//  to the operator set level when occlusion resets.  This class
//  provides that capability.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The private data member actualPeep_ stores the value of Peep to be
//  used.  There are inline methods provided to get and set the value
//  of actualPeep_.  The method updatePeep() updates actualPeep_ with
//  the operator set value of Peep.
//---------------------------------------------------------------------
//@ Fault-Handling
// n/a
//---------------------------------------------------------------------
//@ Restrictions
//  Only one instance of this class should be created.  The actualPeep_
//  data member must be initialized before this class can be used.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/Peep.ccv   25.0.4.0   19 Nov 2013 14:00:02   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006  By: syw     Date:  20-Jan-2000    DR Number: 5457
//  Project:  NeoMode
//  Description:
//      Use PEEP_LOW value during BiLevel.
//
//  Revision: 005  By: sah     Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//      Added support for new PEEP low setting.
//
//  Revision: 006  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 005  By:  syw   Date:  16-Jul-1998    DR Number: DCS 5120
//       Project:  BiLevel (840)
//       Description:
//			Update peepChange_ when peep changes.
//
//  Revision: 004  By:  syw   Date:  09-Dec-1997    DR Number: none
//       Project:  BiLevel (840)
//       Description:
//		 	BiLevel initial version.  Added updatePeep() for BiLevel.
//
//  Revision: 003  By:  iv    Date:  07-Feb-1997    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Rework per Breath Delivery formal code review.
//
//  Revision: 002 By:  iv   Date:   15-Feb-1996    DR Number: DCS 674 
//       Project:  Sigma (R8027)
//       Description:
//             Removed #ifndef directives surrounding #include directives.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

# include "Peep.hh"
# include "BreathMiscRefs.hh"

//@ Usage-Classes

# include "PhasedInContextHandle.hh"
#include "MathUtilities.hh"
#include "ModeValue.hh"

//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Peep()  [Default Constructor]
//
//@ Interface-Description
// Constructor
//---------------------------------------------------------------------
//@ Implementation-Description
//  The actual peep value is initialized to the set peep value.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Peep::Peep(void)
{
	// $[TI1]
	CALL_TRACE("Peep()");

	actualPeep_ = PhasedInContextHandle::GetBoundedValue( SettingId::PEEP).value;
	peepChange_ = FALSE ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~Peep()  [Destructor]
//
//@ Interface-Description
// Destructor
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Peep::~Peep(void)
{
	CALL_TRACE("~Peep()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updatePeep
//
//@ Interface-Description
// 	This method is called at the beginning of inspiration or exhalation.  This
// 	method takes BreathPhaseType::PhaseType argument and has no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	If the peep value changes from the last time it was updated, a flag is
//	set to indicate this event.  This flag is cleared every exhalation.
//	$[02232]
//---------------------------------------------------------------------
//@ PreCondition
// (phase == BreathPhaseType::INSPIRATION) ||
//								(phase == BreathPhaseType::EXHALATION)
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
Peep::updatePeep(const BreathPhaseType::PhaseType phase)
{
	CALL_TRACE("Peep::updatePeep(const BreathPhaseType::PhaseType phase)");
	UNUSED_SYMBOL(phase);	

    const DiscreteValue mode = PhasedInContextHandle::GetDiscreteValue( SettingId::MODE) ;

	Real32 setPeep ;

	if (mode == ModeValue::BILEVEL_MODE_VALUE)
	{
		// $[TI3]
		setPeep = PhasedInContextHandle::GetBoundedValue( SettingId::PEEP_LOW).value ;
	}
	else
	{
		// $[TI4]
		setPeep = PhasedInContextHandle::GetBoundedValue( SettingId::PEEP).value;
	}
	
	if (!IsEquivalent( setPeep, actualPeep_, HUNDREDTHS))
	{
	// $[TI1]
		peepChange_ = TRUE ;
	}
	// $[TI2]

	actualPeep_ = setPeep ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updatePeep
//
//@ Interface-Description
// This method is called whenever it is necessary to update peep value.  This
// method takes BreathPhaseType::PhaseType, current and new peep state as
// arguments.  This method has no return value. This method is intended
// to be called during BiLevel.
//---------------------------------------------------------------------
//@ Implementation-Description
// 1) Update the actualPeep_ data member only if the breath phase is not
//    inspiration or the set peep value is less than the actual value when
//	  currPeep = newPeep = PEEP_LOW.
//	  Set the peepChange_ flag to TRUE when the actual peep and peep low value
//	  are not the equivalent.
// 2) use previous actualPeep_ data member when currPeep = LOW_PEEP and
//	  newPeep = HIGH_PEEP.
//	  Set the peepChange_ flag to TRUE when the actual peep and peep low value
//	  are not the equivalent.
// 3) Update the actualPeep_ data member to low peep when currPeep = PEEP_HIGH
//	  and newPeep = PEEP_LOW.
// 4) Update the actualPeep_ data member to high peep when currPeep = PEEP_HIGH
//	  and newPeep = PEEP_HIGH.
//	  Set the peepChange_ flag to TRUE when the actual peep and peep high value
//	  are not the equivalent.
//---------------------------------------------------------------------
//@ PreCondition
// phase == BreathPhaseType::INSPIRATION ||	phase == BreathPhaseType::EXHALATION
// newPeepState == PEEP_HIGH || newPeepState == PEEP_LOW
// currPeepState == PEEP_HIGH || currPeepState == PEEP_LOW
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
Peep::updatePeep(const BreathPhaseType::PhaseType phase, const PeepState currPeepState,
				 const PeepState newPeepState)
{
	CALL_TRACE("Peep::updatePeep(const BreathPhaseType::PhaseType phase, \
				const PeepState currPeepState, const PeepState newPeepState)") ;
	
	CLASS_PRE_CONDITION( newPeepState == PEEP_HIGH || newPeepState == PEEP_LOW) ;
	CLASS_PRE_CONDITION( currPeepState == PEEP_HIGH || currPeepState == PEEP_LOW) ;
	
	Real32 lowPeep = PhasedInContextHandle::GetBoundedValue( SettingId::PEEP_LOW).value ;
	Real32 highPeep = PhasedInContextHandle::GetBoundedValue( SettingId::PEEP_HIGH).value ;

	if (currPeepState == PEEP_LOW && newPeepState == PEEP_LOW)
	{
		if (!IsEquivalent( lowPeep, actualPeep_, HUNDREDTHS))
		{
			// $[TI1]
			peepChange_ = TRUE ;
		}	// implied else $[TI2]

		actualPeep_ = lowPeep ;
	}
	else if (currPeepState == PEEP_LOW && newPeepState == PEEP_HIGH)
	{
		CLASS_ASSERTION( phase == BreathPhaseType::INSPIRATION) ;
		if (!IsEquivalent( lowPeep, actualPeep_, HUNDREDTHS))
		{
			// $[TI3]
			peepChange_ = TRUE ;
		}	// implied else $[TI4]
		actualPeep_ = lowPeep ;
	}
	else if (currPeepState == PEEP_HIGH && newPeepState == PEEP_LOW)
	{
		if ( !IsEquivalent( lowPeep, actualPeep_, HUNDREDTHS) ) 
		{
			peepChange_ = TRUE ;
		}
	    // $[BL04022] 
		actualPeep_ = lowPeep ;
	}
	else	//	currPeepState == PEEP_HIGH && newPeepState == PEEP_HIGH
	{
		if (!IsEquivalent( highPeep, actualPeep_, HUNDREDTHS))
		{
			// $[TI6]
			peepChange_ = TRUE ;
		}	// implied else $[TI7]

		actualPeep_ = highPeep ;
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
Peep::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, PEEP,
                          lineNumber, pFileName, pPredicate);
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================



//=====================================================================
//
//  Private Methods...
//
//=====================================================================

