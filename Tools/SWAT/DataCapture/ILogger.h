//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file ILogger.h
//----------------------------------------------------------------------------

#ifndef ILOGGER_H
#define ILOGGER_H


class ILogger{
    public:
    virtual void logMessage(unsigned int line, int logtype, char *filename,
            char *str, ...) = 0;
};


//
// Type of message
//
#define LM_DEBUG    0
#define LM_INFO     1
#define LM_TRACE    2

#define LM_NOTICE   3
#define LM_WARNING  4


#define LM_ERROR    5
#define LM_CRITICAL 6
#define LM_ALERT    7
#define LM_LOGINFO  8
#define LM_GENERAL  9
#endif
