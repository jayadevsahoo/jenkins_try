#include "stdafx.h"
//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: OperatingGroupList - Derived from Foundation container class.
//---------------------------------------------------------------------
//@ Interface-Description
// OperatingGroupList is derived from a Foundation container class;
// specifically, a singly linked list of constant references to class
// OperatingGroup objects.  It is derived from the Foundation 
// SListR_OperatingGroup_NUM_TLGROUP class and all list methods are 
// available.
//---------------------------------------------------------------------
//@ Rationale
// OperatingGroupList provides a simple naming convention for files accessing a
// Foundation macro class.
//
// Additionally, if C++ Templates replace the macro classes in the future,
// these List classes will be the only ones affected by the change.
//---------------------------------------------------------------------
//@ Implementation-Description
// See the SListR_OperatingGroup_NUM_TLGROUP Foundation class.
//---------------------------------------------------------------------
//@ Fault-Handling
//    none
//---------------------------------------------------------------------
//@ Invariants
//    none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Alarm-Analysis/vcssrc/OperatingGroupList.ccv   25.0.4.0   19 Nov 2013 13:51:24   pvcs  $
//
//@ Modification-Log

//  
//   Revision 001   By:   hct      Date: 01/01/95         DR Number:   n/a
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//  
//   Revision 002   By:   sah      Date: 01/12/99         DR Number:   5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to non-inlined method.
//  
//=====================================================================

#include "Sigma.hh"
#include "Alarm_Analysis.hh"
#include "OperatingGroupList.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: OperatingGroupList
//
//@ Interface-Description
//    Default constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

OperatingGroupList::OperatingGroupList(void)
{
  CALL_TRACE("OperatingGroupList(void)");
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: OperatingGroupList
//
//@ Interface-Description
//    Copy constructor.
//
// >Von
//    rList  Reference to the object to be copied from.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

OperatingGroupList::OperatingGroupList(const OperatingGroupList& rList) :
                    FixedSListR(OperatingGroup,NUM_TLGROUP)(rList)
{
  CALL_TRACE("OperatingGroupList(rList)");
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~OperatingGroupList
//
//@ Interface-Description
//    Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

OperatingGroupList::~OperatingGroupList(void)
{
  CALL_TRACE("~OperatingGroupList(void)");
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//@ Interface-Description
//    This method is called when a software fault is detected by the
//    fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//    'lineNumber' are essential pieces of information.  The 'pFileName'
//    and 'pPredicate' strings may be defaulted in the macro to reduce
//    code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//    This method receives the call for the SoftFault, adds it sub-system
//    and class name ID and sends the message to the non-member function
//    SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition    
//    none
//@ End-Method
//=====================================================================

void
OperatingGroupList::SoftFault(const SoftFaultID  softFaultID,
                              const Uint32       lineNumber,
                              const char*        pFileName,
                              const char*        pPredicate)  
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, ALARM_ANALYSIS, 
                          Alarm_Analysis::OPERATINGGROUPLIST, lineNumber,
                          pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)

