#!/usr/bin/csh -f
#

#=====================================================================
# This is a proprietary work to which Puritan-Bennett corporation of
# California claims exclusive right.  No part of this work may be used,
# disclosed, reproduced, stored in an information retrieval system, or
# transmitted by any means, electronic, mechanical, photocopying,
# recording, or otherwise without the prior written permission of
# Puritan-Bennett Corporation of California.
#
#            Copyright (c) 1993, Puritan-Bennett Corporation
#=====================================================================

#=====================================================================
#@ Script:  processStrings.x -- used by Makefile to filter through the
#                               '.IN' files, generating '.in' files.
#---------------------------------------------------------------------
# This script first creates a filter script including the substitutions
# for all of the language-specific symbols, concatenates the substitutions
# in 'SymbolProcessFile.sed', then filters the corresponding language '.IN'
# files (passed in as argument #1), storing the filtered results into
# corresponding '.in' files.
#---------------------------------------------------------------------
#
#@ Version-Information
# @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/processStrings.x_v   25.0.4.0   19 Nov 2013 14:08:48   pvcs  $
#
#@ Modification-Log
#
#  Revision: 022   By: rhj    Date: 07-Apr-2011    SCR Number: 6759
#  Project:  PROX
#  Description:
#      Remove extra space in Vdot e spont for Russian
#
#  Revision: 021   By: rhj    Date: 01-Mar-2011    SCR Number: 6738
#  Project:  PROX
#  Description:
#      Added ML symbols.
#
#  Revision: 020   By: mnr    Date: 22-Jan-2010    SCR Number: NA
#  Project:  Baseline
#  Description:
#      S840BUILD3 to Baseline Merge conflict resolved.
#
#  Revision: 019   By: mnr    Date: 30-Nov-2009    SCR Number: 6525
#  Project:  S840BUILD3
#  Description:
#      FRENCH NEONATAL has proper accents now.
#
#  Revision: 018   By: mnr    Date: 2-Nov-2009    SCR Number: 
#  Project:  S840BUILD3
#  Description:
#      Previous header author changed from gdc to mnr.
#
#  Revision: 017   By: mnr    Date: 23-Oct-2009    SCR Number: 6517
#  Project:  S840BUILD3
#  Description:
#      Modified to support S840 langs and new symbols, terms etc.
#
#  Revision: 016   By: gdc    Date: 24-Sep-2008    SCR Number: 6535
#  Project:  840S
#  Description:
#      Modified to support neonatal non-invasive CPAP.
#
#  Revision: 015  By: mnr     Date:  28-Apr-2008    SCR Number: 6433
#  Project: TREND3
#  Description:
#      Trending: 'e' updated to 'E' for VteSpont symbol for Russian
#
#  Revision: 014  By: gdc     Date:  14-Jan-2008    SCR Number: 6407
#  Project:  
#  Description:
#      Changed FILTER_FILE to process unique name to allow for
#      parallel builds.
#
#  Revision: 013  By: gdc     Date:  27-Jun-2007    SCR Number: 6237
#  Project:  TREND
#  Description:
#      Added PEEPi pav symbol
#
#  Revision: 012  By: gdc     Date:  15-Feb-2005    DR Number: 6144
#  Project:  NIV1
#  Description:
#      Added low Ppeak symbols
#
#  Revision: 011  By: ccy     Date:  28-Mar-2003    DR Number: 5313
#  Project:  PAV
#  Description:
#      Integrated with PAV tip.
#          /840/Baseline/GUI-Applications/vcssrc/processStrings.x_v  Rev "Baseline" (9.0)
#
#  Revision: 010  By: Boris Kuznetsov    Date:  03-May-2003    SCR Number: 6036 
#  Project:  Russian
#  Description:
#       Added Russian symbol translations
#
#
#  Revision: 010  By: erm     Date:  22-Oct-2002    DR Number: 5818 
#  Project:  VTPC
#  Description:
#       Added  HI Vti mand
#       Added  Vti mand
#       Added  HI Vti mand LIMIT
#
#  Revision: 009  By: SRP     Date:  20-Aug-2002    DR Number: 6092 
#  Project:  Polish
#  Description:
#       Added Polish symbol translations
#
#  Revision: 009-PAV  By: sah     Date:  10-Jan-2001    DR Number: 5805
#  Project:  PAV
#  Description:
#      Added substitution for a number of discrete setting values so
#      that foreign-language files can use the English version more
#      often.
#
#  Revision: 008  By: sah     Date:  05-May-2000    DR Number: 5755
#  Project:  VTPC
#  Description:
#      VTPC project-related changes:
#      *  added new symbol for target support volume
#      *  changed IBW into a symbol
#
#  Revision: 007  By: sah	Date: 10-Apr-2000   DCS Number:  5702
#  Project:  NeoMode
#  Description:
#      Change to have French version of "Ppeak" to be "Ppointe", as per SME
#      request.  Also, moved 'Th' and 'Tl' substitutions for clearer association
#      with term definition.
#
#  Revision 006  By:  sph    Date: 15-Feb-00     DR Number: 5327
#  Project:  NeoMode
#  Description:
#      Added to variations to PEAK and MEAN subscript.
#
#  Revision 005  By:  sah    Date: 27-Aug-99     DR Number: 5513
#  Project:  ATC
#  Description:
#      Changed symbols, as per customer feedback.
#
#  Revision 004  By:  gdc    Date: 22-Jul-99     DR Number: 5499
#  Project:  ATC
#  Description:
#      Changed 'gp' to 'p' and split commands into SymbolProcessFile1.sed 
#      and SymbolProcessFile2.sed for Solaris compatibility.
#
#  Revision: 003  By: btray    	Date:  03-Nov-1998    DR Number: 
#  Project:  Sigma (R8027)
#  Description:
#	Fix for DCS # 5250; Added language specific processing for
#	[Tl], [Th], [PEEPl] and [PEEPh].
#
#  Revision: 002  By: dosman    Date:  31-Mar-1998    DR Number: 
#  Project:  Sigma (R8027)
#  Description:
#       for BiLevel
#	added processing of PEEP(H) and PEEP(L)
#
#  Revision: 001  By: sah    Date:  11-Feb-1998    DR Number: 5004
#  Project:  Sigma (R8027)
#  Description:
#       Integration baseline.
#
#=====================================================================

set USAGE="usage:  ${0} language raw-symbol-file [-y]"

if ($#argv < 2  ||  $#argv > 3) then
  echo "$USAGE"
  exit 1
endif

if (`testProj` != 1) then
  echo "${0}:  '$PROJ_840': invalid project."
  exit 2
endif

unset LANGUAGE
unset RAW_SYMBOL_FILE
set FORCE_YES=FALSE

while ($#argv > 0)
  switch ("$1")
  case "-y" :
    set FORCE_YES=TRUE
    breaksw
  default :
    if ($?LANGUAGE == 0) then
      set LANGUAGE="$1"
    else
      set RAW_SYMBOL_FILE="$1"
    endif
    breaksw
  endsw
  shift # shift to next argument
end

echo "For '${PROJ_840}' Project, processing '$RAW_SYMBOL_FILE' for '$LANGUAGE'."

if ($FORCE_YES == FALSE) then
  echo -n "  Is this correct? (y/n) "
  set RESPONSE=$<
  if ("$RESPONSE" == ''  ||  ($RESPONSE != 'y'  &&  $RESPONSE != 'Y')) then
    echo "${0}:  aborted."
    exit 3 
  endif
endif

set PPID=$$
set FILTER_FILE=StringProcessFile$PPID.tmp
rm -f ${FILTER_FILE}
touch ${FILTER_FILE}

############################################################
# Variations of the 'END' subscript
############################################################

if ($LANGUAGE == FRENCH || $LANGUAGE == ITALIAN || $LANGUAGE == SPANISH) then
  set END_TERM="FIN"
else
  set END_TERM="END"
endif

echo "s/\[Pi end\]/P{S:I ${END_TERM}}/g" >> ${FILTER_FILE}

############################################################
# Variations of 'mL' and 'L/min'
############################################################

if ($LANGUAGE == RUSSIAN ) then
  set ML_TERM="{A:\\354\\353}"
  set LPM_TERM="{A:\\353}\/{A:\\354\\350\\355}"
else
 set ML_TERM="mL"
 set LPM_TERM="L\/min"
endif

echo "s/\[mL\]/${ML_TERM}/g" >> ${FILTER_FILE}
echo "s/\[ML\]/${ML_TERM}/g" >> ${FILTER_FILE}
echo "s/\[LPM\]/${LPM_TERM}/g" >> ${FILTER_FILE}
  
############################################################
# Variations of the 'MAND' subscript
############################################################

if ($LANGUAGE == FRENCH || $LANGUAGE == ITALIAN) then
  set MAND_TERM="CONT"
else if ($LANGUAGE == RUSSIAN) then
  set MAND_TERM="{A:\\317\\320\\310\\315}"
else
  set MAND_TERM="MAND"
endif

echo "s/\[Vte mand\]/V{S:TE ${MAND_TERM}}/g" >> ${FILTER_FILE}
echo "s/\[HI Vte mand\]/'UP_ARROW'V{S:TE ${MAND_TERM}}/g" >> ${FILTER_FILE}
echo "s/\[LO Vte mand\]/'DN_ARROW'V{S:TE ${MAND_TERM}}/g" >> ${FILTER_FILE}
echo "s/\[LO Vte mand LIMIT\]/'DN_ARROW_BAR'V{S:TE ${MAND_TERM}}/g" >> ${FILTER_FILE}

echo "s/\[Vti mand\]/V{S:TI ${MAND_TERM}}/g" >> ${FILTER_FILE}
echo "s/\[HI Vti mand\]/'UP_ARROW'V{S:TI ${MAND_TERM}}/g" >> ${FILTER_FILE}
echo "s/\[HI Vti mand LIMIT\]/'UP_ARROW_BAR'V{S:TI ${MAND_TERM}}/g"  >> ${FILTER_FILE}

############################################################
# Variations of the 'HIGH' and 'LOW' terms
############################################################

if ($LANGUAGE == FRENCH) then
 set HIGH_TERM="H"
 set LOW_TERM="B"
else if ($LANGUAGE == ITALIAN) then
 set HIGH_TERM="A"
 set LOW_TERM="B"
else  
 set HIGH_TERM="H"
 set LOW_TERM="L"
endif

echo "s/\[Th\]/T{S:${HIGH_TERM}}/g" >> ${FILTER_FILE}
echo "s/\[Tl\]/T{S:${LOW_TERM}}/g" >> ${FILTER_FILE}


############################################################
# Variations of the 'PEEP' term
############################################################

if ($LANGUAGE == FRENCH) then
  set PEEP_TERM="PEP"
else
  set PEEP_TERM="PEEP"
endif

echo "s/\[PEEP\]/${PEEP_TERM}/g" >> ${FILTER_FILE}
echo "s/\[PEEPi\]/${PEEP_TERM}{S:I}/g" >> ${FILTER_FILE}
echo "s/\[PEEPi pav\]/${PEEP_TERM}{S:I PAV}/g" >> ${FILTER_FILE}
echo "s/\[PEEPtot\]/${PEEP_TERM}{S:TOT}/g" >> ${FILTER_FILE}
echo "s/\[PEEPh\]/${PEEP_TERM}{S:${HIGH_TERM}}/g" >> ${FILTER_FILE}
echo "s/\[PEEPl\]/${PEEP_TERM}{S:${LOW_TERM}}/g" >> ${FILTER_FILE}


############################################################
# Variations of the 'PL' subscript
############################################################

if ($LANGUAGE == SPANISH) then
  set PLATEAU_TERM="PI"
else
  set PLATEAU_TERM="PL"
endif

echo "s/\[Tpl\]/T{S:${PLATEAU_TERM}}/g" >> ${FILTER_FILE}
echo "s/\[Ppl\]/P{S:${PLATEAU_TERM}}/g" >> ${FILTER_FILE}


############################################################
# Variations of the 'SUPP' subscript
############################################################

if ($LANGUAGE == FRENCH) then
  set SUPP_TERM="AI"
else if ($LANGUAGE == SPANISH) then
  set SUPP_TERM="SOP"
else
  set SUPP_TERM="SUPP"
endif

echo "s/\[Psupp\]/P{S:${SUPP_TERM}}/g" >> ${FILTER_FILE}
echo "s/\[Vt supp\]/V{S:T ${SUPP_TERM}}/g" >> ${FILTER_FILE}


############################################################
# Variations of the 'PEAK' subscript
############################################################

if ($LANGUAGE == FRENCH) then
  set PEAK_TERM="POINTE"
else if ($LANGUAGE == ITALIAN) then
  set PEAK_TERM="MAX"
else
  set PEAK_TERM="PEAK"
endif

echo "s/\[Ppeak\]/P{S:${PEAK_TERM}}/g" >> ${FILTER_FILE}
echo "s/\[HI Ppeak\]/'UP_ARROW'P{S:${PEAK_TERM}}/g" >> ${FILTER_FILE}
echo "s/\[HI Ppeak LIMIT\]/'UP_ARROW_BAR'P{S:${PEAK_TERM}}/g" >> ${FILTER_FILE}

echo "s/\[LO Ppeak\]/'DN_ARROW'P{S:${PEAK_TERM}}/g" >> ${FILTER_FILE}
echo "s/\[LO Ppeak LIMIT\]/'DN_ARROW_BAR'P{S:${PEAK_TERM}}/g" >> ${FILTER_FILE}


############################################################
# Variations of the 'MEAN' subscript
############################################################

if ($LANGUAGE == ITALIAN) then
  set MEAN_TERM="MEDIA"
else if ($LANGUAGE == FRENCH) then
  set MEAN_TERM="MOY"
else
  set MEAN_TERM="MEAN"
endif

echo "s/\[Pmean\]/P{S:${MEAN_TERM}}/g" >> ${FILTER_FILE}


############################################################
# Variations of 'IBW'
############################################################

if ($LANGUAGE == FRENCH) then
  set IBW_TERM="PIDP"
else if ($LANGUAGE == ITALIAN) then
  set IBW_TERM="PCI"
else if ($LANGUAGE == SPANISH) then
  set IBW_TERM="PIC"
else if ($LANGUAGE == RUSSIAN) then
  set IBW_TERM="{A:\\310\\302\\322}"
else
  set IBW_TERM="IBW"
endif

echo "s/\[IBW\]/${IBW_TERM}/g" >> ${FILTER_FILE}


############################################################
# POLISH
############################################################
if ($LANGUAGE == POLISH) then
# note HI Pvent is also in SymbolProcessFile2.sed, but this
# will circumvent that translation
echo "s/\[HI Pvent\]/'UP_ARROW'P{S:RESP}/g" >> ${FILTER_FILE}

echo "s/\[AV\]/BEZDECH/g" >> ${FILTER_FILE}
echo "s/\[PS spont\]/PS/g" >> ${FILTER_FILE}
echo "s/\[TC spont\]/TC/g" >> ${FILTER_FILE}
echo "s/\[VS spont\]/VS/g" >> ${FILTER_FILE}
echo "s/\[PA spont\]/PA/g" >> ${FILTER_FILE}
echo "s/\[PC mand\]/PC/g" >> ${FILTER_FILE}

echo "s/\[VC mand\]/VC/g" >> ${FILTER_FILE}
echo "s/\[VC+ mand\]/VC+/g" >> ${FILTER_FILE}
echo "s/\[SIMV mode\]/SIMV/g" >> ${FILTER_FILE}
echo "s/\[SPONT mode\]/SPONT/g" >> ${FILTER_FILE}

echo "s/\[NONE\]/BRAK/g" >> ${FILTER_FILE}
echo "s/\[None\]/Brak/g" >> ${FILTER_FILE}
echo "s/\[ET tube\]/ET/g" >> ${FILTER_FILE}
echo "s/\[TRACH tube\]/TRACH/g" >> ${FILTER_FILE}
echo "s/\[Disabled\]/Wy\{A:\\263\\261\}czono/g" >> ${FILTER_FILE}
echo "s/\[DISABLED\]/WY\{A:\\243\\101\}CZONO/g" >> ${FILTER_FILE}
echo "s/\[Enabled\]/W\{A:\\263\\261\}czono/g" >> ${FILTER_FILE}
echo "s/\[ENABLED\]/W\{A:\\243\\101\}CZONO/g" >> ${FILTER_FILE}
echo "s/\[ADULT\]/DOROS\{A\:\\243\}Y/g" >> ${FILTER_FILE}
echo "s/\[Adult\]/Doros\{A\:\\263\}y/g" >> ${FILTER_FILE}
echo "s/\[PEDIATRIC\]/PEDIATRYCZNY/g" >> ${FILTER_FILE}
echo "s/\[Pediatric\]/Pediatryczny/g" >> ${FILTER_FILE}
echo "s/\[NEONATAL\]/NEONATALNY/g" >> ${FILTER_FILE}
echo "s/\[Neonatal\]/Neonatalny/g" >> ${FILTER_FILE}
echo "s/\[Even\]/Parzysty/g" >> ${FILTER_FILE}
echo "s/\[Odd\]/Nieparzysty/g" >> ${FILTER_FILE}
endif

############################################################
# Variations of the 'MAND' subscript
############################################################

if ($LANGUAGE == RUSSIAN ) then

echo "s/\[LO Vte\]/'DN_ARROW'V{S:TE}/g" >> ${FILTER_FILE}
echo "s/\[HI Vte\]/'UP_ARROW'V{S:TE}/g" >> ${FILTER_FILE}
echo "s/\[HI Vti\]/'UP_ARROW'V{S:TI}/g" >> ${FILTER_FILE}
echo "s/\[HI Pvent\]/'UP_ARROW'P{S:VENT}/g" >> ${FILTER_FILE}

  set SPONT_TERM="{A:\\321\\317\\316\\315\\322}"

echo "s/\[Vte spont\]/V{S:TE ${SPONT_TERM}}/g" >> ${FILTER_FILE}
echo "s/\[HI Vte spont\]/'UP_ARROW'V{S:TE ${SPONT_TERM}}/g" >> ${FILTER_FILE}
echo "s/\[LO Vte spont\]/'DN_ARROW'V{S:TE ${SPONT_TERM}}/g" >> ${FILTER_FILE}
echo "s/\[LO Vte spont LIMIT\]/'DN_ARROW_BAR'V{S:TE ${SPONT_TERM}}/g" >> ${FILTER_FILE}

echo "s/\[Vti spont\]/V{S:TI ${SPONT_TERM}}/g" >> ${FILTER_FILE}
echo "s/\[HI Vti spont\]/'UP_ARROW'V{S:TI ${SPONT_TERM}}/g" >> ${FILTER_FILE}
echo "s/\[HI Vti spont LIMIT\]/'UP_ARROW_BAR'V{S:TI ${SPONT_TERM}}/g"  >> ${FILTER_FILE}

echo "s/\[Ti spont\]/T{S:I ${SPONT_TERM}}/g" >> ${FILTER_FILE}
echo "s/\[Vdot e spont\]/'V_DOT'{S:E ${SPONT_TERM}}/g" >> ${FILTER_FILE}

endif

############################################################
# Variations of Mode Setting values...
############################################################

# setup default (English) term values...
set AC_TERM="A\/C"
set SIMV_TERM="SIMV"
set SPONT_TERM="SPONT"
set BILEVEL_TERM="BILEVEL"
set CPAP_TERM="CPAP"
set APNEA_TERM="AV"

if ($LANGUAGE == FRENCH) then
  set AC_TERM="VAC"
  set SIMV_TERM="VACI"
  set APNEA_TERM="VA"
else if ($LANGUAGE == ITALIAN) then
  set APNEA_TERM="VA"
else if ($LANGUAGE == PORTUGUESE) then
  set SPONT_TERM="ESPONT"
else if ($LANGUAGE == SPANISH) then
  set SPONT_TERM="ESPONT"
  set APNEA_TERM="VA"
endif

echo "s/\[AC mode\]/${AC_TERM}/g" >> ${FILTER_FILE}
echo "s/\[SIMV mode\]/${SIMV_TERM}/g" >> ${FILTER_FILE}
echo "s/\[SPONT mode\]/${SPONT_TERM}/g" >> ${FILTER_FILE}
echo "s/\[BILEVEL mode\]/${BILEVEL_TERM}/g" >> ${FILTER_FILE}
echo "s/\[CPAP mode\]/${CPAP_TERM}/g" >> ${FILTER_FILE}
echo "s/\[APNEA mode\]/${APNEA_TERM}/g" >> ${FILTER_FILE}


############################################################
# Variations of Mandatory Type Setting values...
############################################################

# setup default (English) term values...
set PC_TERM="PC"
set VC_TERM="VC"

if ($LANGUAGE == FRENCH) then
  set PC_TERM="VPC"
  set VC_TERM="VVC"
else if ($LANGUAGE == ITALIAN) then
  set PC_TERM="PCV"
  set VC_TERM="VCV"
else if ($LANGUAGE == SPANISH) then
  set PC_TERM="CP"
  set VC_TERM="CV"
endif

set VCP_TERM="${VC_TERM}+"

echo "s/\[PC mand type\]/${PC_TERM}/g" >> ${FILTER_FILE}
echo "s/\[VC mand type\]/${VC_TERM}/g" >> ${FILTER_FILE}
echo "s/\[VC+ mand type\]/${VCP_TERM}/g" >> ${FILTER_FILE}


############################################################
# Variations of Spontaneous Type Setting values...
############################################################

# setup default (English) term values...
set OFF_TERM="NONE"
set PS_TERM="PS"
set TC_TERM="TC"
set VS_TERM="VS"
set PA_TERM="PA"

if ($LANGUAGE == FRENCH) then
  set OFF_TERM="AUCUNE"
  set PS_TERM="AI"
else if ($LANGUAGE == GERMAN) then
  set OFF_TERM="AUS"
else if ($LANGUAGE == ITALIAN) then
  set OFF_TERM="NESSUNA"
  set PS_TERM="PSV"
else if ($LANGUAGE == PORTUGUESE) then
  set OFF_TERM="NENHUM"
else if ($LANGUAGE == SPANISH) then
  set OFF_TERM="NINGUNO"
endif

echo "s/\[OFF spont type\]/${OFF_TERM}/g" >> ${FILTER_FILE}
echo "s/\[PS spont type\]/${PS_TERM}/g" >> ${FILTER_FILE}
echo "s/\[TC spont type\]/${TC_TERM}/g" >> ${FILTER_FILE}
echo "s/\[VS spont type\]/${VS_TERM}/g" >> ${FILTER_FILE}
echo "s/\[PA spont type\]/${PA_TERM}/g" >> ${FILTER_FILE}


############################################################
# Variations of Flow Pattern Setting values...
############################################################

# setup default (English) term values...
set RAMP_TERM="RAMP"
set SQUARE_TERM="SQUARE"

if ($LANGUAGE == FRENCH) then
  set RAMP_TERM="DECR"
  set SQUARE_TERM="RECTANGUL."
else if ($LANGUAGE == GERMAN) then
  set RAMP_TERM="DEZELER."
  set SQUARE_TERM="RECHTECK"
else if ($LANGUAGE == ITALIAN) then
  set RAMP_TERM="DISCEN"
  set SQUARE_TERM="QUADRA"
else if ($LANGUAGE == PORTUGUESE) then
  set RAMP_TERM="RAMP"
  set SQUARE_TERM="SQUARE"
else if ($LANGUAGE == SPANISH) then
  set RAMP_TERM="RAMP"
  set SQUARE_TERM="SQUARE"
endif

echo "s/\[RAMP value\]/${RAMP_TERM}/g" >> ${FILTER_FILE}
echo "s/\[SQUARE value\]/${SQUARE_TERM}/g" >> ${FILTER_FILE}


############################################################
# Variations of Tube Type Setting values...
############################################################

# setup default (English) term values...
set ET_TERM="ET"
set TRACH_TERM="TRACH"

if ($LANGUAGE == PORTUGUESE) then
  set TRACH_TERM="TRAQ"
else if ($LANGUAGE == SPANISH) then
  set TRACH_TERM="TR�Q"
endif

echo "s/\[ET tube type\]/${ET_TERM}/g" >> ${FILTER_FILE}
echo "s/\[TRACH tube type\]/${TRACH_TERM}/g" >> ${FILTER_FILE}


############################################################
# Variations of DCI Pariry Mode Setting values...
############################################################

# setup default (English) term values...
set EVEN_TERM="Even"
set ODD_TERM="Odd"
set NONE_TERM="None"

if ($LANGUAGE == FRENCH) then
  set EVEN_TERM="Pair"
  set ODD_TERM="Impair"
  set NONE_TERM="Aucun"
else if ($LANGUAGE == GERMAN) then
  set EVEN_TERM="Gerade"
  set ODD_TERM="Ungerade"
  set NONE_TERM="Keine"
else if ($LANGUAGE == ITALIAN) then
  set EVEN_TERM="Pari"
  set ODD_TERM="Dispari"
  set NONE_TERM="Nessuno"
else if ($LANGUAGE == PORTUGUESE) then
  set EVEN_TERM="Par"
  set ODD_TERM="�mpar"
  set NONE_TERM="Nenhuma"
else if ($LANGUAGE == SPANISH) then
  set EVEN_TERM="Par"
  set ODD_TERM="Impar"
  set NONE_TERM="Ninguna"
endif

echo "s/\[EVEN parity\]/${EVEN_TERM}/g" >> ${FILTER_FILE}
echo "s/\[ODD parity\]/${ODD_TERM}/g" >> ${FILTER_FILE}
echo "s/\[NO parity\]/${NONE_TERM}/g" >> ${FILTER_FILE}


############################################################
# Variations of Disabled/Enabled setting values...
############################################################

# setup default (English) term values...
set DISABLED_TERM="Disabled"
set ENABLED_TERM="Enabled"
set CALIBRATION_TERM="Calibration"

if ($LANGUAGE == FRENCH) then
  set DISABLED_TERM="D�sactiv�"
  set ENABLED_TERM="Activ�"
else if ($LANGUAGE == GERMAN) then
  set DISABLED_TERM="Deaktiviert"
  set ENABLED_TERM="Aktiviert"
  set CALIBRATION_TERM="Kalibration"
else if ($LANGUAGE == ITALIAN) then
  set DISABLED_TERM="Disattivato"
  set ENABLED_TERM="Attivato"
  set CALIBRATION_TERM="Calibrazione"
else if ($LANGUAGE == PORTUGUESE) then
  set DISABLED_TERM="Desativado"
  set ENABLED_TERM="Ativado"
  set CALIBRATION_TERM="Calibra��o"
else if ($LANGUAGE == SPANISH) then
  set DISABLED_TERM="Desactivado"
  set ENABLED_TERM="Activado"
  set CALIBRATION_TERM="Calibraci�n"
else if ($LANGUAGE == RUSSIAN) then
  set CALIBRATION_TERM="{A:\\312\\340\\353\\350\\341\\360\\356\\342\\352\\340}"
else if ($LANGUAGE == POLISH) then
  set DISABLED_TERM="Wy\{A:\\263\\261\}czono"
  set ENABLED_TERM="W\{A:\\263\\261\}czono"
  set CALIBRATION_TERM="Kalibracja"
endif

echo "s/\[DISABLED value\]/${DISABLED_TERM}/g" >> ${FILTER_FILE}
echo "s/\[ENABLED value\]/${ENABLED_TERM}/g" >> ${FILTER_FILE}
echo "s/\[CALIBRATION value\]/${CALIBRATION_TERM}/g" >> ${FILTER_FILE}

############################################################
# Variations of circuit type setting values...
############################################################

# setup default (English) term values...
set ADULT_TERM="ADULT"
set Adult_TERM="Adult"
set PEDIATRIC_TERM="PEDIATRIC"
set Pediatric_TERM="Pediatric"
set NEONATAL_TERM="NEONATAL"
set Neonatal_TERM="Neonatal"

if ($LANGUAGE == FRENCH) then
  set ADULT_TERM="ADULTE"
  set Adult_TERM="Adulte"
  set PEDIATRIC_TERM="P�DIATRIQUE"
  set Pediatric_TERM="P�diatrique"
  set NEONATAL_TERM="N�ONATAL"
  set Neonatal_TERM="N�onatal"
else if ($LANGUAGE == GERMAN) then
  set ADULT_TERM="Erwachsener"  #this is intentionally mixed case
  set Adult_TERM="Erwachsener"
  set PEDIATRIC_TERM="KIND"
  set Pediatric_TERM="Kind"
  set NEONATAL_TERM="NEONATAL"
  set Neonatal_TERM="Neonatal"
else if ($LANGUAGE == ITALIAN) then
  set ADULT_TERM="ADULTO"
  set Adult_TERM="Adulto"
  set PEDIATRIC_TERM="PEDIATRICO"
  set Pediatric_TERM="Pediatrico"
  set NEONATAL_TERM="NEONATALE"
  set Neonatal_TERM="Neonatale"
else if ($LANGUAGE == PORTUGUESE) then
  set ADULT_TERM="ADULT"
  set Adult_TERM="Adult"
  set PEDIATRIC_TERM="PEDI�TRICO" 
  set Pediatric_TERM="Pedi�trico"
  set NEONATAL_TERM="NEONATAL"
  set Neonatal_TERM="Neonatal"
else if ($LANGUAGE == SPANISH) then
  set ADULT_TERM="ADULTO"
  set Adult_TERM="Adulto"
  set PEDIATRIC_TERM="PEDI�TRICO"
  set Pediatric_TERM="Pedi�trico"
  set NEONATAL_TERM="NEONATAL"
  set Neonatal_TERM="Neonatal"
endif

echo "s/\[ADULT cct type\]/${ADULT_TERM}/g" >> ${FILTER_FILE}
echo "s/\[Adult cct type\]/${Adult_TERM}/g" >> ${FILTER_FILE}
echo "s/\[PEDIATRIC cct type\]/${PEDIATRIC_TERM}/g" >> ${FILTER_FILE}
echo "s/\[Pediatric cct type\]/${Pediatric_TERM}/g" >> ${FILTER_FILE}
echo "s/\[NEONATAL cct type\]/${NEONATAL_TERM}/g" >> ${FILTER_FILE}
echo "s/\[Neonatal cct type\]/${Neonatal_TERM}/g" >> ${FILTER_FILE}

############################################################
# Perform the filtering...
############################################################

echo " " >> ${FILTER_FILE}

# process all of the language-invariant symbols, then pipe that through
# the language-specific filter, storing the result into a file the same
# name as the given file, sans its last extension...
sed -f ${FILTER_FILE} ${RAW_SYMBOL_FILE} >! partA$PPID.tmp
sed -f SymbolProcessFile1.sed partA$PPID.tmp >! partB$PPID.tmp
sed -f SymbolProcessFile2.sed partB$PPID.tmp >! ${RAW_SYMBOL_FILE:r}.in

# remove temporaries
rm -f *$PPID.tmp

exit 0
