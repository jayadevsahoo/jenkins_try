#ifndef PatientDataRegistrar_HH
#define PatientDataRegistrar_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: PatientDataRegistrar - The entry point to the
// GUI-Applications for the Breath-Delivery subsystem to register
// changes to GUI App's patient data.  Multiple GUI objects can be notified
// of a single datum change.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/PatientDataRegistrar.hhv   25.0.4.0   19 Nov 2013 14:08:12   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 003  By:  yyy    Date:  27-Oct-98    DR Number: 5216
//    Project:  BiLevel (R8027)
//    Description:
//      Bilevel initial version.
//      Removed the IsSamePatientValue_(), changed UpdateHappened() to
//		return void type.
//
//  Revision: 002  By:  yyy    Date:  10-Oct-97    DR Number: 2523
//    Project:  Sigma (R8027)
//    Description:
//      Added the method IsDataChanged() to allow the interested subscreen
//		to query the data availability status of the specified data Id.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================


//@ Usage-Classes
#include "PatientDataId.hh"
#include "GuiAppClassIds.hh"
class PatientDataCascade;
class BreathDatumHandle;
//@ End-Usage

class PatientDataTarget;

//@ Type: 
union PatientDataHandleStruct_
{
	Int16		discreteType;
	Real32		numericType;
	Boolean		booleanType;
};

class PatientDataRegistrar
{
public:
	static void UpdateHappened(Int16 taskType);
	static void ChangeHappened(const PatientDataId::PatientItemId patientDataId);
	static void RegisterTarget(PatientDataId::PatientItemId patientDataId,
							   PatientDataTarget* pTarget);
	static void ResetPatientValue(void);
	static Boolean IsDataChanged(PatientDataId::PatientItemId patientDataId);

	static void Initialize(void);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:

private:
	// these methods are purposely declared, but not implemented...
	PatientDataRegistrar(void);							// not implemented...
	PatientDataRegistrar(const PatientDataRegistrar&);	// not implemented...
	~PatientDataRegistrar(void);						// not implemented...
	void operator=(const PatientDataRegistrar&);		// not implemented...

	//@ Data-Member: CascadeArray_
	// A static array of PatientDataCascade objects, implemented as
	// a simple pointer to an object.  The index to the array corresponds
	// to the PatientDataId provided by the Breath-Delivery subsystem.
	// This allows a fast lookup.
	static PatientDataCascade *CascadeArray_;

	//@ Data-Member: IsDataChangedArray_
	// An array that parallels the CascadeArray_ and indicates which patient
	// data items have been changed since the last call to UpdateHappened().
	static Boolean IsDataChangedArray_[PatientDataId::NUM_TOTAL_PATIENT_DATA];

	//@ Data-Member: DataArray_
	// An array that parallels the CascadeArray_ and indicates which patient
	// data items have been changed since the last call to UpdateHappened().
	static PatientDataHandleStruct_ DataArray_[PatientDataId::NUM_TOTAL_PATIENT_DATA];

	//@ Data-Member: PatientDataHandle_
	// A handle to the patient datum value, used for accessing the Patient
	// Data subsystem.
	static BreathDatumHandle *PatientDataHandleArray_;

	//@ Data-Member: PutUpdateEvent_
	// A flag indicating whether the registrar should put a Patient Data Update
	// event on the User Annunciation queue.  When ChangeHappened() is first
	// called an update event should be put on the queue and this flag set
	// to false.  Only when UpdateHappened() is called is the flag set to true
	// allowing an update event to again be put on the queue on the next
	// call to ChangeHappened().
	static Boolean PutUpdateEvent_;

	//@ Data-Member: FirstInitialization_
	// A flag that is true only before the first call to the Initialize()
	// method.  Prevents running code twice that should be run once only,
	// if Initialize() is called more than once.
	static Boolean FirstInitialization_;
};


#endif // PatientDataRegistrar_HH 
