#include "stdafx.h"
//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file  GainsManager.cc
/// @brief Implementation of the GainsManager class
//----------------------------------------------------------------------------

//-------------------------------------
// I N C L U D E S
//-------------------------------------
#include "GainsManager.hh"
#include "FaultHandlerMacros.hh"
#include "CallTraceMacros.hh"
#include "UtilityClassId.hh"
#include "GainsConfig.hh"
#include "GainsConfigFuncPtrs.hh"
#include "GainsConfigPressureBased.hh"
#include "CriticalSection.hh"

using namespace std;
using namespace GainsManagerNS;

//-----------------------------------------------------------------------------
// Static Variables
//-----------------------------------------------------------------------------
GainsManager* GainsManager::m_instance_ptr = new GainsManager();

//---------------------------
// getInstance
//---------------------------
GainsManager& GainsManager::getInstance(void)
{
    CLASS_ASSERTION( m_instance_ptr != 0 );
    return (*m_instance_ptr);
} // getInstance

//---------------------------
// getActive
//---------------------------
GainsManager::GainsManagerActive_t GainsManager::getActive(void) const
{
    CALL_TRACE( "GainsManager::getActive()" );

    return(m_active);
} // getActive

//--------------------------------------------------------------------
// setActive
//--------------------------------------------------------------------
void GainsManager::setActive( GainsManager::GainsManagerActive_t active )
{
    CALL_TRACE("GainsManager::setActive( GainsManager::GainsManagerActive_t active )");

    CriticalSection cs(GAINS_MANAGER_MT);

    m_active = active;

	if( m_active == GainsManager::GAINS_MGR_INACTIVE )
	{
		 m_distStatus = GainsManager::GAINS_MGR_NONE;
	}
} // setActive

//-------------------------------------
// SoftFault
//-------------------------------------
void GainsManager::SoftFault(const SoftFaultID  softFaultID,
                      const Uint32       lineNumber,
                      const Char*        pFileName,
                      const Char*        pPredicate)
{
    FaultHandler::SoftFault(softFaultID, UTILITIES, GAINSMANAGER_CLASS,
          lineNumber, pFileName, pPredicate);
} // SoftFault

//-------------------------------------
// loadGains
//-------------------------------------
Boolean GainsManager::loadGains(void)
{
    static Char* funcName = "loadGains";
    CALL_TRACE( funcName );

    //
    // Enter critical section but don't wait
    // if we can't get the mutex we'll get it next time.
    //
    MutEx m_mutex(GAINS_MANAGER_MT);
	if( m_mutex.request(0) != Ipc::OK )
    {
        /*NETWORK_LOG( UTILITIES, NetworkLoggerNS::NETLOGLEVEL_WARNING,
           funcName, "Couldn't get mutex. Didn't load. Retry..." );*/
        // Since we didn't get the mutex, no need to release.
        return (FALSE);
    }

	Boolean retValue = TRUE;

    // Only load the gains during initialization
    if( m_initStatus == GAINS_MGR_INITSUCCESS )
    {
        /*NETWORK_LOG( UTILITIES, NetworkLoggerNS::NETLOGLEVEL_WARNING,
           funcName, "Init is complete. Can't load." );*/

        // Gains loading should only happen once.
        // If the system is trying to reload gains after the Gains Manager
        // is initialized then there is an out of sequence operation.
        retValue = FALSE;
    }
    else
    {
        if( m_initStatus != GAINS_MGR_REGCOMPLETE )
        {
            /*NETWORK_LOG( UTILITIES, NetworkLoggerNS::NETLOGLEVEL_WARNING,
               funcName, "Registration is not complete. Can't load." );*/

            // Loading gains happens after registration is complete
            retValue = FALSE;
        }
    }

    if( retValue == TRUE )
    {
        // gains loading is only done once.
        /*NETWORK_LOG( UTILITIES, NetworkLoggerNS::NETLOGLEVEL_INFO,
           funcName, "Initialization Complete" );*/

        m_initStatus = GAINS_MGR_INITSUCCESS;

        if( m_gainsMap.empty() == TRUE )
        {
            /*NETWORK_LOG( UTILITIES, NetworkLoggerNS::NETLOGLEVEL_INFO,
               funcName, "No gains to load." );*/
        }
        else
        {
            /*NETWORK_LOG( UTILITIES, NetworkLoggerNS::NETLOGLEVEL_INFO,
               funcName, "Loading gains through registered callbacks" );*/

            for( map<GainsNames, GainsConfig*>::iterator itr = m_gainsMap.begin();
                 itr != m_gainsMap.end(); ++itr)
            {
                GainsConfig* gc = itr->second;
                CLASS_ASSERTION( gc != NULL );

                gc->initialize();
            }
        }
    }

    // Exit the critical section
    m_mutex.release();

    return (retValue);
} // loadGains

//-------------------------------------
// restoreOriginalGains
//-------------------------------------
Boolean GainsManager::restoreOriginalGains(void)
{
    static Char* funcName = "restoreOriginalGains";
    CALL_TRACE( funcName );

    CriticalSection cs(GAINS_MANAGER_MT);

    Boolean retValue = TRUE;

    if( m_initStatus != GainsManager::GAINS_MGR_INITSUCCESS )
    {
        /*NETWORK_LOG( UTILITIES, NetworkLoggerNS::NETLOGLEVEL_WARNING,
            funcName, "Initialization not complete. Can't restore gains." );*/
        retValue = FALSE;
    }

    if( m_gainsMap.empty() == TRUE )
    {
        /*NETWORK_LOG( UTILITIES, NetworkLoggerNS::NETLOGLEVEL_WARNING,
            funcName, "No gains to restore." );*/
        retValue = FALSE;
    }

    if( retValue == TRUE )
    {
        /*NETWORK_LOG( UTILITIES, NetworkLoggerNS::NETLOGLEVEL_INFO,
            funcName, "Restoring original gains." );*/

        m_distStatus = GAINS_MGR_GAINS_TO_DISTRIBUTE;

        for( map<GainsNames, GainsConfig*>::iterator itr = m_gainsMap.begin();
             itr != m_gainsMap.end(); ++itr)
        {
            GainsConfig* gc = itr->second;
            CLASS_ASSERTION( gc != NULL );

            /*NETWORK_LOG( UTILITIES, NetworkLoggerNS::NETLOGLEVEL_DEBUG,
               funcName, "Address of GainsConfig(%d)", gc );*/

            gc->restore();
        }
    }

    return (retValue);
} // restoreOriginalGains

//-------------------------------------
// distributeGains
//-------------------------------------
Boolean GainsManager::distributeGains(void)
{
    static Char* funcName = "distributeGains";
    CALL_TRACE( funcName );

    //
    // Enter critical section but don't wait
    // if we can't get the mutex we'll get it next time.
    //
	MutEx m_mutex(GAINS_MANAGER_MT);
    if( m_mutex.request(0) != Ipc::OK )
    {
        /*NETWORK_LOG( UTILITIES, NetworkLoggerNS::NETLOGLEVEL_WARNING,
           funcName, "Couldn't get mutex. Didn't distribute. Retry..." );*/
        // Since we didn't get the mutex, no need to release.
        return (FALSE);
    }

    Boolean retValue = TRUE;

    if( m_initStatus != GainsManager::GAINS_MGR_INITSUCCESS )
    {
        /*NETWORK_LOG( UTILITIES, NetworkLoggerNS::NETLOGLEVEL_WARNING,
           funcName, "Initialization not complete" );*/
        retValue = FALSE;
    }

    if( m_active == GainsManager::GAINS_MGR_INACTIVE )
    {
        /*NETWORK_LOG( UTILITIES, NetworkLoggerNS::NETLOGLEVEL_DEBUG,
           funcName, "GainsManager is not active. Not distributing gains." );*/
        retValue = FALSE;
    }

    if( m_gainsMap.empty() == TRUE )
    {
        /*NETWORK_LOG( UTILITIES, NetworkLoggerNS::NETLOGLEVEL_INFO,
           funcName, "No gains to distribute." );*/
        retValue = FALSE;
    }

    if( retValue == TRUE && m_distStatus == GAINS_MGR_GAINS_TO_DISTRIBUTE )
    {
        /*NETWORK_LOG( UTILITIES, NetworkLoggerNS::NETLOGLEVEL_INFO,
           funcName, "Distributing gains through registered callbacks" );*/

        for( map<GainsNames, GainsConfig*>::iterator itr = m_gainsMap.begin();
             itr != m_gainsMap.end(); ++itr)
        {
            GainsConfig* gc = itr->second;
            CLASS_ASSERTION( gc != NULL );

            /*NETWORK_LOG( UTILITIES, NetworkLoggerNS::NETLOGLEVEL_DEBUG,
               funcName, "Address of GainsConfig(%d)", gc );*/

            gc->distribute();
        }

        /*NETWORK_LOG( UTILITIES, NetworkLoggerNS::NETLOGLEVEL_DEBUG,
           funcName, "Gains distribution complete." );*/
        m_distStatus = GAINS_MGR_GAINS_DISTRIBUTED;
    }

    // Exit the critical section
    m_mutex.release();

    return (retValue);
} // distributeGains

//-------------------------------------
// getGains
//-------------------------------------
Boolean GainsManager::getGains( map<GainsNames, Real32>& gmap )
{
    static Char* funcName = "getGains";
    CALL_TRACE( funcName );

    CriticalSection cs(GAINS_MANAGER_MT);

    if( m_initStatus != GainsManager::GAINS_MGR_INITSUCCESS )
    {
        /*NETWORK_LOG( UTILITIES, NetworkLoggerNS::NETLOGLEVEL_WARNING,
            funcName, "Initialization not complete" );*/
        return (FALSE);
    }

    if( m_gainsMap.empty() == TRUE )
    {
        /*NETWORK_LOG( UTILITIES, NetworkLoggerNS::NETLOGLEVEL_WARNING,
           funcName, "No gains available. Can't get." );*/
        return (FALSE);
    }

    if( gmap.empty() == FALSE )
    {
        gmap.clear();
    }

    /*NETWORK_LOG( UTILITIES, NetworkLoggerNS::NETLOGLEVEL_INFO,
       funcName, "Providing gains via the API" );*/

    for( map<GainsNames, GainsConfig*>::iterator itr = m_gainsMap.begin();
         itr != m_gainsMap.end(); ++itr)
    {
        GainsConfig* gc = itr->second;
        CLASS_ASSERTION( gc != NULL );

        /*NETWORK_LOG( UTILITIES, NetworkLoggerNS::NETLOGLEVEL_DEBUG,
            funcName, "Address of GainsConfig(%d)", gc );*/

        gmap.insert( pair<GainsNames, Real32>(itr->first, gc->getGain() ) );
    }

    return (TRUE);
} // getGains

//-------------------------------------
// getGains
//-------------------------------------
Boolean GainsManager::getOriginalGains( map<GainsNames, Real32>& gmap )
{
    static Char* funcName = "getOriginalGains";
    CALL_TRACE( funcName );

    CriticalSection cs(GAINS_MANAGER_MT);

    if( m_initStatus != GainsManager::GAINS_MGR_INITSUCCESS )
    {
        /*NETWORK_LOG( UTILITIES, NetworkLoggerNS::NETLOGLEVEL_WARNING,
            funcName, "Initialization not complete" );*/
        return (FALSE);
    }

    if( m_gainsMap.empty() == TRUE )
    {
        /*NETWORK_LOG( UTILITIES, NetworkLoggerNS::NETLOGLEVEL_WARNING,
           funcName, "No gains available. Can't get." );*/
        return (FALSE);
    }

    if( gmap.empty() == FALSE )
    {
        gmap.clear();
    }

    /*NETWORK_LOG( UTILITIES, NetworkLoggerNS::NETLOGLEVEL_INFO,
       funcName, "Providing gains via the API" );*/

    for( map<GainsNames, GainsConfig*>::iterator itr = m_gainsMap.begin();
         itr != m_gainsMap.end(); ++itr)
    {
        GainsConfig* gc = itr->second;
        CLASS_ASSERTION( gc != NULL );

        /*NETWORK_LOG( UTILITIES, NetworkLoggerNS::NETLOGLEVEL_DEBUG,
            funcName, "Address of GainsConfig(%d)", gc );*/

        gmap.insert( pair<GainsNames, Real32>(itr->first, gc->getOriginalGain() ) );
    }

    return (TRUE);
} // getOriginalGains

//-------------------------------------
// setGain
//-------------------------------------
Boolean GainsManager::setGain( GainsNames name, Real32 gain)
{
    static Char* funcName = "setGain";
    CALL_TRACE( funcName );

    CriticalSection cs(GAINS_MANAGER_MT);

    if( m_initStatus != GainsManager::GAINS_MGR_INITSUCCESS )
    {
        /*NETWORK_LOG( UTILITIES, NetworkLoggerNS::NETLOGLEVEL_WARNING,
           funcName, "Initialization not complete" );*/
        return (FALSE);
    }

    if( m_gainsMap.empty() == TRUE )
    {
        /*NETWORK_LOG( UTILITIES, NetworkLoggerNS::NETLOGLEVEL_WARNING,
           funcName, "No gains available. Can't set." );*/
        return (FALSE);
    }

    /*NETWORK_LOG( UTILITIES, NetworkLoggerNS::NETLOGLEVEL_INFO,
       funcName, "Setting a gain (%d) (%f)", name, gain );*/

    for( map<GainsNames, GainsConfig*>::iterator itr = m_gainsMap.begin();
         itr != m_gainsMap.end(); ++itr)
    {
        /*NETWORK_LOG( UTILITIES, NetworkLoggerNS::NETLOGLEVEL_DEBUG,
           funcName, "Checking (%d) v. (%d)", itr->first, name );*/

        if( itr->first == name )
        {
            GainsConfig* gc = itr->second;
            CLASS_ASSERTION( gc != NULL );

            /*NETWORK_LOG( UTILITIES, NetworkLoggerNS::NETLOGLEVEL_DEBUG,
               funcName, "Address of GainsConfig(%d)", gc );*/

            m_distStatus = GAINS_MGR_GAINS_TO_DISTRIBUTE;
            gc->setGain( gain );
            return (TRUE);
        }
    }

    /*NETWORK_LOG( UTILITIES, NetworkLoggerNS::NETLOGLEVEL_WARNING,
       funcName, "Gain not found." );*/

    return (FALSE);
} // setGain

//-------------------------------------
// getGain
//-------------------------------------
Boolean GainsManager::getGain( GainsNames name, Real32& gain )
{
    static Char* funcName = "getGain";
    CALL_TRACE( funcName );

    CriticalSection cs(GAINS_MANAGER_MT);

    if( m_initStatus != GainsManager::GAINS_MGR_INITSUCCESS )
    {
        /*NETWORK_LOG( UTILITIES, NetworkLoggerNS::NETLOGLEVEL_INFO, funcName,
           "Initialization not complete" );*/
        return (FALSE);
    }

    if( m_gainsMap.empty() == TRUE )
    {
        return (FALSE);
    }

    /*NETWORK_LOG( UTILITIES, NetworkLoggerNS::NETLOGLEVEL_INFO, funcName,
       "Providing a gain via the API (%d)", name );*/

    for( map<GainsNames, GainsConfig*>::iterator itr = m_gainsMap.begin();
         itr != m_gainsMap.end(); ++itr)
    {
        if( itr->first == name )
        {
            GainsConfig* gc = itr->second;
            CLASS_ASSERTION( gc != NULL );

            /*NETWORK_LOG( UTILITIES, NetworkLoggerNS::NETLOGLEVEL_DEBUG, funcName,
               "Address of GainsConfig(%d)", gc );*/

            gain = gc->getGain();
            return (TRUE);
        }
    }

    /*NETWORK_LOG( UTILITIES, NetworkLoggerNS::NETLOGLEVEL_WARNING,
           funcName, "Gain not found." );*/

    return (FALSE);
} // getGain

//-------------------------------------
// registerGain
//-------------------------------------
Boolean GainsManager::registerGain( GainsNames gainId )
{
    static Char* funcName = "registerGain";
    CALL_TRACE( funcName );

    // Registration happens during system initialization
    // when threads have yet to start executing.

    if( m_initStatus != GainsManager::GAINS_MGR_INPROGRESS )
    {
        /*NETWORK_LOG( UTILITIES, NetworkLoggerNS::NETLOGLEVEL_WARNING,
            funcName, "Initialization is not in progress" );*/
        return (FALSE);
    }

    map<GainsNames, GainsConfig*>::iterator
        itr = m_gainsMap.find(gainId);

    // If the gain is already in the collection then return FALSE
    if(itr != m_gainsMap.end())
    {
        /*NETWORK_LOG( UTILITIES, NetworkLoggerNS::NETLOGLEVEL_WARNING,
           funcName, "Gain (%d) is already registered", gainId );*/
        return (FALSE);
    }

    /*NETWORK_LOG( UTILITIES, NetworkLoggerNS::NETLOGLEVEL_DEBUG,
            funcName, "Registering a gain(%d)", gainId );*/

    GainsConfig* gc = NULL;

    switch( gainId )
    {
        case PEEP_KI:
        case FLOW_A_FACTOR:
        case FLOW_B_FACTOR:
        case FLOW_C_FACTOR:
        case FLOW_D_FACTOR:
        case PRESSURE_KP:
        case PRESSURE_KI:
		case PID_KP:
        case PID_KI:
        case PID_KD:
            gc = new  GainsConfigFuncPtrs( gainId );
            break;

        case PCV_PED_KP:
        case PCV_ADU_KP:
        case PCV_PED_KI:
        case PCV_ADU_KI:
            gc = new GainsConfigPressureBased( gainId );
            break;

        default:
            /*NETWORK_LOG( UTILITIES, NetworkLoggerNS::NETLOGLEVEL_WARNING,
                funcName, "Invalid gain" );*/
            return (FALSE);
    }

    if( gc != NULL )
    {
        if( gc->setup() == GainsConfig::SUCCESS )
        {
            pair<map<GainsNames, GainsConfig*>::iterator, Char> ret
                = m_gainsMap.insert( pair<GainsNames, GainsConfig*>(gainId, gc) );
        }
        else
        {
            /*NETWORK_LOG( UTILITIES, NetworkLoggerNS::NETLOGLEVEL_WARNING,
                funcName, "Error setting up GainsConfig" );*/
            return (FALSE);
        }
    }
    else
    {
        /*NETWORK_LOG( UTILITIES, NetworkLoggerNS::NETLOGLEVEL_WARNING,
            funcName, "Error constructing GainsConfig" );*/
        return (FALSE);
    }

    return (TRUE);
} // registerGain

//-------------------------------------
// registrationComplete
//-------------------------------------
Boolean GainsManager::registrationComplete(bool regSuccess)
{
    // Registration happens during system initialization
    // when threads have yet to start executing.

    if( m_initStatus != GainsManager::GAINS_MGR_INPROGRESS )
    {
        /*NETWORK_LOG( UTILITIES, NetworkLoggerNS::NETLOGLEVEL_WARNING,
            "registrationComplete", "Initialization is not in progress" );*/
        return (FALSE);
    }

	if(!regSuccess)
    {
        /*NETWORK_LOG( UTILITIES, NetworkLoggerNS::NETLOGLEVEL_WARNING,
            "registrationComplete", "Gain registration failed" );*/

        m_initStatus = GainsManager::GAINS_MGR_REGFAILED;
		return (FALSE);
    }

    m_initStatus = GainsManager::GAINS_MGR_REGCOMPLETE;
    return (TRUE);
} // registrationComplete

//-------------------------------------
// reset
//-------------------------------------
void GainsManager::reset(void)
{
    static char* funcName = "reset";
    CALL_TRACE( funcName );

    // Reseting only happens during system initialization
    // or during testing when threads have yet to start executing.

    if( m_initStatus != GainsManager::GAINS_MGR_INITSUCCESS )
    {
        /*NETWORK_LOG( UTILITIES, NetworkLoggerNS::NETLOGLEVEL_INFO,
           funcName, "Initialization not complete. Still reseting..." );*/
    }
    else
    {
        // If there are gains registered then restore the original
        // values prior to re-initialization
        if( m_gainsMap.empty() == FALSE )
        {
            if( restoreOriginalGains() == FALSE )
            {
                /*NETWORK_LOG( UTILITIES, NetworkLoggerNS::NETLOGLEVEL_WARNING,
                   funcName, "Unable to restore original gains" );*/
            }
        }
    }

    // delete the objects that were allocated on the heap.
    for( map<GainsNames, GainsConfig*>::iterator itr = m_gainsMap.begin();
                 itr != m_gainsMap.end(); ++itr)
    {
        GainsConfig* gc = itr->second;
        CLASS_ASSERTION( gc != NULL );

        /*NETWORK_LOG( UTILITIES, NetworkLoggerNS::NETLOGLEVEL_DEBUG,
           funcName, "Address of GainsConfig(%d)", gc );*/

		delete gc;
    }

    // clean up the map
    m_gainsMap.clear();

    // re-initialize the state of the Gains Manager.
    m_active = GAINS_MGR_INACTIVE;
    m_initStatus = GAINS_MGR_INPROGRESS;
    m_distStatus = GAINS_MGR_NONE;

    /*NETWORK_LOG( UTILITIES, NetworkLoggerNS::NETLOGLEVEL_DEBUG,
       funcName, "Gains manager reset complete." );*/
} // reset

//-------------------------------------
// GainsManager
//-------------------------------------
GainsManager::GainsManager(void)
: //m_mutex( GAINS_MANAGER_MT ),
  m_active( GAINS_MGR_INACTIVE ),
  m_initStatus( GAINS_MGR_INPROGRESS ),
  m_distStatus( GAINS_MGR_NONE )
{
	
} // GainsManager
