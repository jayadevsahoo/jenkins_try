#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: KeyPanelTarget - This abstract class allows callbacks from
// the KeyPanel.
//---------------------------------------------------------------------
//@ Interface-Description
// Any object that receives off-screen Key events must have this class
// as one of its parent classes.  Although multiple inheritance is not
// normally allowed, adding this class as an additional parent class is
// acceptable because its only use is to implement a callback interface
// to another class.
//---------------------------------------------------------------------
//@ Rationale
// This class was created to provide a framework for the callback
// mechanism for the KeyPanel class.  Classes derived from this base
// class will inherit the required interface behavior, and thus will be
// capable of registering for and receiving callbacks from the KeyPanel.
//---------------------------------------------------------------------
//@ Implementation-Description
// The member functions are all virtual functions but do nothing.
// The constructor is protected function so it is not possible to create
// an instance of this class.
//---------------------------------------------------------------------
//@ Fault-Handling
// None.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Foundations/vcssrc/KeyPanelTarget.ccv   25.0.4.0   19 Nov 2013 14:11:42   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By: yyy      Date: 01-Jan-1995  DR Number: NONE
//    Project:  Sigma (R8027)
//    Description:
//	Integration baseline.
//=====================================================================

#include "KeyPanelTarget.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...


//=====================================================================
//
// Public methods
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: keyPanelPressHappened [virtual]
//
//@ Interface-Description
// This is a virtual method that does nothing.  It must be defined by a derived
// class if that class needs to perform processing in response to any offScreen
// key press.  When the KeyPanel receives notification that an offscreen key
// has transitioned to the down state, it executes this method in the
// registered target for that key (if any).  Different keys on the key panel
// can have different targets.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
KeyPanelTarget::keyPanelPressHappened(KeyPanel::KeyId keyCode)
{
	CALL_TRACE("KeyPanelTarget::keyPanelPressHappened(KeyPanel::KeyId keyCode)");
	// Do nothing
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: keyPanelReleaseHappened [virtual]
//
//@ Interface-Description
// This is a virtual method that does nothing.  It must be defined by a derived
// class if that class needs to perform processing in response to any offScreen
// key release.  When the KeyPanel receives notification that an offscreen key
// has transitioned to the unpressed state, it executes this method in the
// registered target for that key (if any).  Different keys on the key panel
// can have different targets.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
KeyPanelTarget::keyPanelReleaseHappened(KeyPanel::KeyId keyCode)
{
	CALL_TRACE("KeyPanelTarget::keyPanelReleaseHappened(KeyPanel::KeyId keyCode)");
	// Do nothing
}



//=====================================================================
//
// Protected methods
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: KeyPanelTarget  [Default Constructor]
//
//@ Interface-Description
// Construct and initialize a KeyPanelTarget object. 
// Since this is an abstract base class, it is not necessary for the
// constructor to do any processing.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Does nothing.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

KeyPanelTarget::KeyPanelTarget(void)
{
	CALL_TRACE("KeyPanelTarget::KeyPanelTarget(void)");
	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~KeyPanelTarget  [Destructor]
//
//@ Interface-Description
// Destroy object.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This destructor does nothing.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

KeyPanelTarget::~KeyPanelTarget(void)
{
	CALL_TRACE("KeyPanelTarget::~KeyPanelTarget(void)");
	// Do nothing
}

