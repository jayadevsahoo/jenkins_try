
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
// Class: VolatileMemory - Volatile Memory Block.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Persistent-Objects/vcssrc/VolatileMemory.inv   25.0.4.0   19 Nov 2013 14:18:56   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By:  sah    Date:  01-Jan-1999    DCS Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' to a non-inlined method.
//
//  Revision: 001  By:  sah    Date:  15-Mar-1997    DCS Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//          Initial version (Integration baseline).
//
//=====================================================================

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetAddress()  [static]
//
//@ Interface-Description
//  Return the starting address of the one instance of this class.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  none 
//---------------------------------------------------------------------
//@ PostCondition 
//  (GetAddress() != NULL)
//@ End-Method 
//===================================================================== 

inline VolatileMemory*
VolatileMemory::GetAddress(void)
{
  CALL_TRACE("GetAddress()");
  return(VolatileMemory::P_INSTANCE_);
}   // $[TI1]
