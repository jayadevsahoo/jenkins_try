#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: PromptArea - The Prompt Area at the bottom right of the
// Lower screen.  Divided into three zones for displaying different
// types of prompts.
//---------------------------------------------------------------------
//@ Interface-Description
// The Prompt Area (derived from GUI-Foundation's Container class) is used to
// display different types of prompts.  This class is a member of the GUI-Apps
// Framework cluster.  There are four types of prompts that can be displayed:
// Primary, Advisory, Secondary and Default.  The prompts are displayed in
// three zones (of almost equal sizes) which divide the Prompt Area
// horizontally.
//
// This class handles the management of these three zones.  The first zone
// displays the Primary prompt, the second zone the Advisory prompt and the
// third zone the Secondary prompt.  If a Default prompt is set and no other
// prompts are set then the Default prompt is displayed in the first zone
// (though it normally extends across all zones, with default prompts being
// large).
//
// There are two different priorities of each prompt that can be displayed,
// high or low (although there is only one priority of Default prompt).  If a
// high priority prompt is defined then it is displayed otherwise the low
// priority prompt is displayed.  Normally, objects which have the Adjust Panel
// focus display high priority prompts with all other objects displaying low
// priority ones.  The existence of a high priority Primary prompt causes the
// background of the Prompt Area to go white in order to alert the operator to
// the activation of the Adjust Panel.  The normal background color is light
// grey.
//
// Primary, Advisory and Secondary prompts are set and cleared using the
// setPrompt() method and the Default prompt is set using the
// setDefaultPrompt() method.
//---------------------------------------------------------------------
//@ Rationale
// This class encapsulates all the functionality of the Prompt Area in
// the Lower screen in a single class.
//---------------------------------------------------------------------
//@ Implementation-Description
// All text fields are added, on construction, to the Prompt Area container
// list and are displayed or removed from display by setting their "show"
// state.
//
// The two priorities of Primary, Advisory and Secondary prompts are stored in
// the two-dimensional array, stringsIds_[].  A null entry in the array implies
// that that particular prompt/priority combination does not exist. The
// setPrompt() method controls the content of this array.  The default string
// is stored in defaultStringId_ and set via the setDefaultPrompt() method.
//
// Anytime a change occurs to either stringsIds_[] or defaultStringId_ the
// private method updateArea_() is called.  This, the most important method of
// PromptArea, formulates the correct display of the Prompt Area depending on
// the content of the two data members.
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and pre-conditions
// to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
// Only one instance of this class should be created.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/PromptArea.ccv   25.0.4.0   19 Nov 2013 14:08:16   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005  By: sah  Date: 08-Feb-1999  DR Number: 5314
//  Project:  ATC (R8027)
//  Description:
//      Added in new BiLevel-specific, H:L ratio setting.
//
//  Revision: 004  By:  yyy    Date:  01-APR-98    DR Number: 5252
//       Project:  Sigma (R8027)
//       Description:
//             Corrected mapping SRS to code.
//
//  Revision: 003  By:  dosman Date:  29-APR-98    DR Number: 34
//  Project:  BiLevel (R8027)
//  Description:
//		using different name for I:E ratio soft bound in bilevel
//     Merged /840/Baseline/GUI-Applications/vcssrc/PromptArea.ccv   1.7.1.0   07/30/98 10:19:14   gdc
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "PromptArea.hh"
#include "SettingBoundId.hh"

//@ Usage-Classes
#include "SettingSubject.hh"
//@ End-Usage

//@ Code...

// Initialize static constants.
static const Int32 TOP_LINE_WIDTH_ = 2;
static const Int32 CONTAINER_X_ = 367;
static const Int32 CONTAINER_Y_ = 410;
static const Int32 CONTAINER_WIDTH_ = 273;
static const Int32 CONTAINER_HEIGHT_ = 70;
static const Int32 ZONE_X_ = 3;
static const Int32 ZONE_Y_OFFSET_ = 4;
static const Int32 ZONE_1_HEIGHT_ = 16;
static const Int32 ZONE_2_HEIGHT_ = 24;
const Int32 PromptArea::BUTTON_X_ = 226;
const Int32 PromptArea::BUTTON_Y_ = 4;
const Int32 PromptArea::BUTTON_WIDTH_ = 44;
const Int32 PromptArea::BUTTON_HEIGHT_ = 36;
const Int32 PromptArea::BUTTON_BORDER_ = 4;




//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: PromptArea()  [Default Constructor]
//
//@ Interface-Description
// Constructs the PromptArea.  Only one instance is created, by LowerScreen.
// No construction parameters needed.
//---------------------------------------------------------------------
//@ Implementation-Description
// All TextFields are constructed with empty strings, added to the container
// list and set to invisible (setShow(FALSE)).  The container is sized and
// positioned and the background color is defaulted to Light Grey.  A "line" at
// the top of the area (actually implemented as a thin box) separates the
// PromptArea from the Lower Subscreen Area border and is colored to match
// the background of the Lower Screen Select Area as well as the Message Area.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

PromptArea::PromptArea(void) :
			defaultStringId_(NULL_STRING_ID),
			topLineBox_(0, 0, CONTAINER_WIDTH_, TOP_LINE_WIDTH_),
			softBoundOverrideB_(BUTTON_X_, BUTTON_Y_,
								BUTTON_WIDTH_, BUTTON_HEIGHT_,
								Button::LIGHT, BUTTON_BORDER_, Button::SQUARE,
								Button::NO_BORDER,
								MiscStrs::SOFT_BOUND_OVERRIDE_BUTTON_TITLE),
			pSoftBoundSubject_(NULL),
			softBoundId_(::NULL_SETTING_BOUND_ID)
{
	CALL_TRACE("PromptArea::PromptArea(void)");

	// Size and position the area
	setX(CONTAINER_X_);
	setY(CONTAINER_Y_);
	setWidth(CONTAINER_WIDTH_);
	setHeight(CONTAINER_HEIGHT_);
	
	// Initialize string id array
	for (Uint16 i = 0; i < PA_NUMBER_OF_PRIORITIES; i++)
	{
		for (Uint16 j = 0; j < PA_NUMBER_OF_TYPES; j++)
		{
			stringIds_[i][j] = NULL_STRING_ID;
		}
	}

	// Default background color is Light Grey
	setFillColor(Colors::LIGHT_BLUE);

	// Add line at top of area
	topLineBox_.setColor(Colors::EXTRA_DARK_BLUE);
	topLineBox_.setFillColor(Colors::EXTRA_DARK_BLUE);
	addDrawable(&topLineBox_);

	// Add the various drawables to the screen
	addDrawable(&primaryText_);
	addDrawable(&advisoryText_);
	addDrawable(&secondaryText_);
	addDrawable(&defaultText_);

	// Make them invisible
	primaryText_.setShow(FALSE);
	advisoryText_.setShow(FALSE);
	secondaryText_.setShow(FALSE);
	defaultText_.setShow(FALSE);

	// Position texts
	primaryText_.setX(ZONE_X_);
	primaryText_.setY(ZONE_Y_OFFSET_);
	advisoryText_.setX(ZONE_X_);
	advisoryText_.setY(ZONE_Y_OFFSET_ + ZONE_1_HEIGHT_);
	secondaryText_.setX(ZONE_X_);
	secondaryText_.setY(ZONE_Y_OFFSET_ + ZONE_1_HEIGHT_ + ZONE_2_HEIGHT_);
	defaultText_.setX(ZONE_X_);
	defaultText_.setY(ZONE_Y_OFFSET_);					// $[TI1]

	
    // Register for callbacks for all buttons in which we are interested.
    softBoundOverrideB_.setButtonCallback(this);
	addDrawable(&softBoundOverrideB_);

	// Hide the softbound override button
	softBoundOverrideB_.setShow(FALSE);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~PromptArea()  [Destructor]
//
//@ Interface-Description
// Called when an instance of this class is destroyed.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

PromptArea::~PromptArea(void)
{
	CALL_TRACE("PromptArea::~PromptArea(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setPrompt
//
//@ Interface-Description
// Sets the prompt to be displayed for a given priority/type (i.e. a high
// or low priority Primary, Advisory or Secondary prompt).
// Passed the following parameters:
// >Von
//	promptType	The type of the prompt, one of PA_PRIMARY, PA_ADVISORY or
//				PA_SECONDARY.
//	priority	The priority of the prompt, either PA_HIGH or PA_LOW.
//	stringId	The StringId of the prompt to display.
// >Voff
// Note: a high priority Primary prompt must always be displayed whenever a
// setting button is selected or whenever the rotary knob or the Accept key
// is active.
//---------------------------------------------------------------------
//@ Implementation-Description
// The string is simply stored in the appropriate element of stringIds_
// (if different from the previous value) and the Prompt Area is updated
// (by calling updateArea_()).
//---------------------------------------------------------------------
//@ PreCondition
// The prompt type and priority must be valid.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
PromptArea::setPrompt(PromptType promptType, PromptPriority priority,
														StringId stringId)
{
	CALL_TRACE("PromptArea::setPrompt(PromptType promptType, "
							"PromptPriority priority, StringId stringId)");

	CLASS_PRE_CONDITION((promptType >= 0) &&
										(promptType < PA_NUMBER_OF_TYPES));
	CLASS_PRE_CONDITION((priority >= 0) &&
										(priority < PA_NUMBER_OF_PRIORITIES));

	// Store prompt if different to old one
	if (stringIds_[priority][promptType] != stringId)
	{													// $[TI1]
		stringIds_[priority][promptType] = stringId;

		if (promptType == PromptArea::PA_PRIMARY && stringId == NULL_STRING_ID)
		{												// $[TI1.1]
			// ensure that soft-bound, override button not shown
			hideOverrideButton();
		}												// $[TI1.2]

		// Update Prompt Area
		updateArea_();
	}													// $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setDefaultPrompt
//
//@ Interface-Description
// This method is usually called once only to set the Default prompt to
// be "Touch a button."  This is the prompt that is displayed over the
// full Prompt Area only if no other prompts are set.
//---------------------------------------------------------------------
//@ Implementation-Description
// The string is stored in defaultStringId_ (if different to previous
// value) and the Prompt Area updated (by calling updateArea_()).
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
PromptArea::setDefaultPrompt(StringId stringId)
{
	CALL_TRACE("PromptArea::setDefaultPrompt(StringId stringId)");

	// Store the information if different to previous
	if (defaultStringId_ != stringId)
	{													// $[TI1]
		defaultStringId_ = stringId;

		// Update the Prompt Area
		updateArea_();
	}													// $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateArea_
//
//@ Interface-Description
// Needs no parameters.  Called when some part of the Prompt Area
// changes.  It updates the Prompt Area to reflect the current data.
//---------------------------------------------------------------------
//@ Implementation-Description
// Displays defaultStringId_ if no other prompts are set.  Sets the background
// of the area to be white if a HIGH priority Primary prompt is set.  Displays
// the Primary prompt (if set) in zone 1.  Displays the Advisory prompt (if
// set) in zone 2.  Displays the Secondary prompt (if set) in zone 3.  High
// priority prompts (if set) are displayed in preference to low priority
// prompts.
//
// $[01123] The Prompt Area must be able to display up to 3 kinds of prompts ...
// $[01124] Prompt Area's background must be white when ...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
PromptArea::updateArea_(void)
{
	CALL_TRACE("PromptArea::updateArea_(void)");

	Boolean defaultOnly = TRUE;			// Is only the default prompt set?
	StringId stringId;					// Temporary StringId variable.

	// Check to see if no prioritized prompt has been set.  If so then
	// we can display the default prompt (as long as there is also no
	// confirmation prompt).
	for (Uint16 i = 0; i < PA_NUMBER_OF_PRIORITIES; i++)
	{
		for (Uint16 j = 0; j < PA_NUMBER_OF_TYPES; j++)
		{
			if (stringIds_[i][j] != NULL_STRING_ID)
			{											// $[TI1]
				defaultOnly = FALSE;
				break;
			}											// $[TI2]
		}
	}

	// Set a bright background color if there exists a high priority Primary
	// prompt
	if (stringIds_[PA_HIGH][PA_PRIMARY] != NULL_STRING_ID)
	{													// $[TI3]
		setFillColor(Colors::GOLDENROD);
	}
	else
	{													// $[TI4]
		setFillColor(Colors::LIGHT_BLUE);
	}

	// Set the Primary prompt.
	stringId = stringIds_[PA_HIGH][PA_PRIMARY] != NULL_STRING_ID ?
			stringIds_[PA_HIGH][PA_PRIMARY] : stringIds_[PA_LOW][PA_PRIMARY];
														// $[TI5], $[TI6]
	if (stringId != NULL_STRING_ID)
	{													// $[TI7]
		primaryText_.setText(stringId);
		primaryText_.setShow(TRUE);
	}
	else
	{													// $[TI8]
		primaryText_.setShow(FALSE);
	}

	// Set the Advisory prompt
	stringId = stringIds_[PA_HIGH][PA_ADVISORY] != NULL_STRING_ID ?
			stringIds_[PA_HIGH][PA_ADVISORY] : stringIds_[PA_LOW][PA_ADVISORY];
														// $[TI9], $[TI10]
	if (stringId != NULL_STRING_ID)
	{													// $[TI11]
		advisoryText_.setText(stringId);
		advisoryText_.setShow(TRUE);
	}
	else
	{													// $[TI12]
		advisoryText_.setShow(FALSE);
	}

	// Set the Secondary prompt
	stringId = stringIds_[PA_HIGH][PA_SECONDARY] != NULL_STRING_ID ?
							stringIds_[PA_HIGH][PA_SECONDARY] :
										stringIds_[PA_LOW][PA_SECONDARY];
														// $[TI13], $[TI14]
	if (stringId != NULL_STRING_ID)
	{													// $[TI15]
		secondaryText_.setText(stringId);
		secondaryText_.setShow(TRUE);
	}
	else
	{													// $[TI16]
		secondaryText_.setShow(FALSE);
	}

	// Check if we should only display the Default prompt
	if (defaultOnly)
	{													// $[TI17]
		if (defaultStringId_ != NULL_STRING_ID)
		{												// $[TI18]
			defaultText_.setText(defaultStringId_);
			defaultText_.setShow(TRUE);
			setFillColor(Colors::EXTRA_DARK_BLUE);
			defaultText_.setColor(Colors::GOLDENROD);
		}
		else
		{												// $[TI19]
			defaultText_.setShow(FALSE);
		}
	}
	else
	{													// $[TI20]
		defaultText_.setShow(FALSE);
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: buttonDownHappened
//
//@ Interface-Description
// When the override button ("OK") is pressed down under soft bound violation.
// Passed a pointer to the button as well as 'byOperatorAction', a flag which
// indicates whether the operator pressed the button down or whether the
// setToDown() method was called.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	pButton != NULL && byOperatorAction == TRUE
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
PromptArea::buttonDownHappened(Button *pButton, Boolean byOperatorAction)
{
    CALL_TRACE("buttonDownHappened(pButton, byOperatorAction)");
	CLASS_ASSERTION(pButton != NULL && byOperatorAction);

    if (pButton == &softBoundOverrideB_)
    {						// $[TI1]
		// Restore "Use knob to adjust" message to Prompt Area
		setPrompt(PromptArea::PA_PRIMARY, PromptArea::PA_HIGH,
			      PromptStrs::USE_KNOB_TO_ADJUST_P);

		setPrompt(PromptArea::PA_ADVISORY, PromptArea::PA_HIGH,
			      NULL_STRING_ID);

		// User confirmed the intention to override soft bound, so
		// communicate to the setting...
		pSoftBoundSubject_->overrideSoftBound(softBoundId_);

		// this must be called AFTER the override call above...
		hideOverrideButton();
	 }						// $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  showOverrideButton
//
//@ Interface-Description
//  Called when we want to show the SoftBound Override button.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Simply set the Show flag of this button to TRUE.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
PromptArea::showOverrideButton(SettingSubject*      pSoftBoundSubject,
							   const SettingBoundId softBoundId)
{
	softBoundOverrideB_.setShow(TRUE);
	softBoundOverrideB_.setToUp();

	pSoftBoundSubject_ = pSoftBoundSubject;
	softBoundId_       = softBoundId;
				// $[TI1]
}

	
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  hideOverrideButton(void)
//
//@ Interface-Description
//  Called when we want to hide the SoftBound Override button. 
//---------------------------------------------------------------------
//@ Implementation-Description
//  Simply set the Show flag of this button to FALSE and send the button
//  to the UP state.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
PromptArea::hideOverrideButton(void) 
{
	softBoundOverrideB_.setShow(FALSE);

	pSoftBoundSubject_ = NULL;
	softBoundId_       = NULL_SETTING_BOUND_ID;
				// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
PromptArea::SoftFault(const SoftFaultID  softFaultID,
				      const Uint32       lineNumber,
				      const char*        pFileName,
				      const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, PROMPTAREA,
									lineNumber, pFileName, pPredicate);
}
