#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: TrendDbRecord
//---------------------------------------------------------------------
//@ Interface-Description
//  Base class for internal Trend DB records.
//  This class provides the generalized database record interface for
//  the trended settings and patient data. This class is not
//  instantiated. Derived classes must overload the following pure
//  virtual methods:
//  size()
//  get/setSeqNumber()
//	get/setTimeOfDay()
//	get/setTrendValue()
//	setChecksum()
// 
//---------------------------------------------------------------------
//@ Rationale
//  To generalize the interface for the different types of data records.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class defines a method interface that provides read/write
//  access to the individual records (setting or patient data).
//---------------------------------------------------------------------
//@ Fault-Handling
//  SoftFault is used
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Trend-Database/vcssrc/TrendDbRecord.ccv   25.0.4.0   19 Nov 2013 14:34:38   pvcs  $
//
//@ Modification-Log
// 
//  Revision: 001  By:  ksg    Date:  18-June-2007    SCR Number: 6237
//       Project:  Trend
//       Description: Initial version
//
//=====================================================================

#include "CompactFlash.hh"          // GUI-IO-Devices

#include "Trend_Database.hh"
#include "TrendDbRecord.hh"


//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TrendDbRecord [constructor]
//
//@ Interface-Description
//  Intializes the record schema pointer to NULL
//---------------------------------------------------------------------
//@ Implementation-Description
//  Sets up the schema structure but does not allocate memory
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
TrendDbRecord::TrendDbRecord( void )
	: pRecordSchema_( NULL )
{
	// do nothing
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TrendDbRecord [destructor]
//
//@ Interface-Description
//  Does no action  
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
TrendDbRecord::~TrendDbRecord() 
{
	// do nothing
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: size [pure virtual]
//
//@ Interface-Description
//  Returns the size in bytes of the configured record  
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getSeqNumber [pure virtual]
//
//@ Interface-Description
//  Returns the sequence number
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setSeqNumber [pure virtual]
//
//@ Interface-Description
//  Sets the sequence number
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getTimeOfDay [pure virtual]
//
//@ Interface-Description
//  Returns the timestamp
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setTimeOfDay [pure virtual]
//
//@ Interface-Description
//  Sets the timestamp
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getTrendValue [pure virtual]
//
//@ Interface-Description
//  Returns the Trend value at the slot number
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setTrendValue [pure virtual]
//
//@ Interface-Description
//  Sets the Trend value at the slot number
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setChecksum [pure virtual]
//
//@ Interface-Description
//  Sets the checksum in the record
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
 

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isValid
//
//@ Interface-Description
//  Validates that the memory pointer exists and that the
//  checksum of the record is correct
//  
//---------------------------------------------------------------------
//@ Implementation-Description
//  If record is valid, checksum calculated including the checksum in the 
//  last 2 bytes is equal to 0. 
//  Ensures that the record sequence number is valid (> 0).
//  Ensures that the timestamp is valid.
//---------------------------------------------------------------------
//@ PreCondition
//	
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
Boolean TrendDbRecord::isValid( void ) const
{
	Boolean isValid = FALSE;

	// First check to see if the record is a mapped "set" in memory
	if (pRecordSchema_)
	{
		// If non-zero sequence number, record contains data
		Uint32 seqNumber = getSeqNumber();
		if (seqNumber != 0)
		{
			// Validate the record checksum
			Uint16 chkSum = computeChecksum_();
			// Validate the record timestamp
			TimeStamp timeStamp( getTimeOfDay() );
			if (chkSum == 0 && !(timeStamp.isInvalid()))
			{
				// record is valid
				isValid = TRUE;
			}
		}
	}
	return( isValid );
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: dump
//
//@ Interface-Description
//  Debug method, used to print record info to the serial terminal
//---------------------------------------------------------------------
//@ Implementation-Description
//  Loops through record and prints all the bytes to the serial port.
//---------------------------------------------------------------------
//@ PreCondition
//	Ensures record pointer is defined
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDbRecord::dump( void )
{
#ifdef TREND_MGR_DEBUG
	AUX_CLASS_PRE_CONDITION( pRecordSchema_, (Uint32) pRecordSchema_ );

	Uint16                   recSize;
	Uint8 *                  ptrRecord;

	recSize = size();
	ptrRecord = (Uint8 *) pRecordSchema_;

	TREND_MGR_DBG6( 64, "Trend Raw Dump, size ", recSize,", addr ", hex, ptrRecord, dec );

	for (Int16 idx = 0; idx < recSize; idx++)
	{
		if (TrendDataMgr::DebugMask & 64)
		{
			if (!(idx % 16))
			{
				printf( "\n  %d: ", idx );
			}
			printf( " %x ", *(ptrRecord + idx) );
		}
	}
	TREND_MGR_DBG1( 64, "\n--------------------------------------" );
#endif
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: computeChecksum_ [private]
//
//@ Interface-Description
//  computes the checksum
//  
//---------------------------------------------------------------------
//@ Implementation-Description
//  Based on a XOR checksum
//---------------------------------------------------------------------
//@ PreCondition
//	Ensures record pointer is defined
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
Uint16 TrendDbRecord::computeChecksum_( void ) const
{
	AUX_CLASS_PRE_CONDITION( pRecordSchema_, (Uint32) pRecordSchema_ );

	Uint16                   idx;
	Uint16                   checksum;
	Uint16 *                 ptrRecord;

	// Compute the record checksum 4 bytes at a time
	idx = size() / sizeof( Uint16 );
	ptrRecord = (Uint16 *) pRecordSchema_;
	checksum = 0;

	while (idx)
	{
		checksum ^= *(ptrRecord + (idx - 1));
		idx--;
	}

	return( checksum );
}




//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void TrendDbRecord::SoftFault(const SoftFaultID  softFaultID,
							  const Uint32       lineNumber,
							  const char*        pFileName,
							  const char*        pPredicate)  
{
	CALL_TRACE("TrendDbRecord::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, TREND_DATABASE, TREND_DB_RECORD,
							lineNumber, pFileName, pPredicate);
}
