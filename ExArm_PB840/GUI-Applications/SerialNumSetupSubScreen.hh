#ifndef SerialNumSetupSubScreen_HH
#define SerialNumSetupSubScreen_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
//@ Class: SerialNumSetupSubScreen - Activated automatically at start up
// time in Service Mode if BD or/and GUI serial numbers don't match those
// on data key.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/SerialNumSetupSubScreen.hhv   25.0.4.0   19 Nov 2013 14:08:24   pvcs  $ 
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 001  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//====================================================================

#include "AdjustPanelTarget.hh"
#include "GuiTestManagerTarget.hh"
#include "SubScreen.hh"

//@ Usage-Classes
#include "GuiTestManager.hh"
#include "ServiceStatusArea.hh"
#include "SubScreenTitleArea.hh"
#include "TextButton.hh"
#include "GuiAppClassIds.hh"
class TestResult;
//@ End-Usage

class SerialNumSetupSubScreen : public SubScreen,
							public AdjustPanelTarget,
							public GuiTestManagerTarget 
{
public:
	SerialNumSetupSubScreen(SubScreenArea *pSubScreenArea);
	~SerialNumSetupSubScreen(void);

	// Redefine SubScreen activation/deactivation methods
	virtual void activate(void);
	virtual void deactivate(void);

    // ButtonTarget virtual method
    virtual void buttonDownHappened(Button *pButton,
												Boolean byOperatorAction);
    virtual void buttonUpHappened(Button *pButton,
												Boolean byOperatorAction);

	// AdjustPanelTarget virtual methods
	virtual void adjustPanelAcceptPressHappened(void);
	virtual void adjustPanelRestoreFocusHappened(void);

	// GuiTestMangerTarget virtual methods
	virtual void processTestPrompt(Int command);
	virtual void processTestKeyAllowed(Int keyAllowed);
	virtual void processTestResultStatus(Int resultStatus, Int testResultId=-1);
	virtual void processTestResultCondition(Int resultCondition);
	virtual void processTestData(Int dataIndex, TestResult *pResult);


	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:

private:
	//@ Type: ConditionId_
	// Lists the maximum condition counts.
	enum ConditionId_
	{
		MAX_CONDITION_ID = 2
	};

	//@ Type: SerialNoStringSize_
	// Lists the maximum string size for serial number.
	enum SerialNoStringSize_ 
	{
		SERIAL_NO_STRING_SIZE = SERIAL_NO_SIZE + 1
	};

	// these methods are purposely declared, but not implemented...
	SerialNumSetupSubScreen(void);						// not implemented...
	SerialNumSetupSubScreen(const SerialNumSetupSubScreen&);	// not implemented...
	void operator=(const SerialNumSetupSubScreen&);		// not implemented...

	void endTest_(void);
	void nextTest_(void);
	void startTest_(SmTestId::ServiceModeTestId);

	Boolean areSerialNumbersProgrammable_();
	void displayDiagnostic_(StringId);
	void returnToNormalService_();
	void setErrorTable_(void);
	void setSNDisplayArea();
	void setupErrorDisplay_();

	//@ Data-Member: setSNStatus_
	// A container for displaying the current SerialNumSetup status
	ServiceStatusArea setSNStatus_;

	//@ Data-Member: pCurrentButton_
	// The pCurrentButton_ is used for remembering the previously button
	// which was pressed by the user.
	Button *pCurrentButton_;

	//@ Data-Member: errDisplayArea_
	// An array of TextField objects used to display test conditions in the
	// lower screen
	TextField errDisplayArea_[2*MAX_CONDITION_ID];

    //@ Data-Member: conditionText_
	// Text for error condition prompts
    StringId conditionText_[MAX_CONDITION_ID];

    //@ Data-Member: currentTestId_
	// Current test id
	SmTestId::ServiceModeTestId currentTestId_;

	//@ Data-Member: errorHappened_
	// Flag that indicates status of test
	int	errorHappened_;
	
	//@ Data-Member: errorIndex_
	// Keep tracks of index into TextFields in the error display area
	int	errorIndex_;

	//@ Data-Member: titleArea_
	// The sub-screen's title at the top left of the screen.
	SubScreenTitleArea titleArea_;

	//@ Data-Member: testManager_
	// Object instance of GuiTestManager
	GuiTestManager testManager_;

	//@ Data-Member: serNumberSettingButton_
	// The serial number setting button
	TextButton serNumberSettingButton_;

	//@ Data-Member: serNumberBypassingButton_
	// The serial number bypassing button.
	TextButton serNumberBypassingButton_;

	//@ Data-Member: snTroubleShootingField_
	// Serial Number trouble shooting button.
	TextField snTroubleShootingField_;

	//@ Data-Member: serNumberDisplayField1_ 
	// Serial Number display.
	TextField serNumberDisplayField1_;

	//@ Data-Member: serNumberDisplayField2_ 
	// Serial Number display.
	TextField serNumberDisplayField2_;

	//@ Data-Member: troubleShootingFlag_
	// Indicating this state at acceptance time 
	int troubleShootingFlag_;

};


#endif // SerialNumSetupSubScreen_HH 
