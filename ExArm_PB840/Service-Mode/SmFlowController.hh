#ifndef SmFlowController_HH
#define SmFlowController_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: SmFlowController - Flow controller for Service Mode
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/SmFlowController.hhv   25.0.4.0   19 Nov 2013 14:23:44   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004   By: syw   Date:  14-Sep-2000    DR Number: 5695
//  Project:  VTPC
//  Description:
//		Added getFlowTarget() if development options are allowed
//
//  Revision: 003  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 002  By:  quf    Date: 22-May-1997    DR Number: DCS 2131
//       Project:  Sigma (840)
//       Description:
//            Modification-Log fix.
//            Changed stableFlowHighBound_, stableFlowLowBound_,
//            flowBecameUnstable_, and flowInstabilityCounts_ to
//            static variables to avoid object size change in
//            production vs development builds.
//            Removed use of "this" pointer to determine flow control
//            owner, use an ID (initialized during construction) instead.
//
//  Revision: 001  By:  mad    Date: 21-Aug-1995    DR Number: none
//	Project:  Sigma (R8027)
//	Description:
//		Initial version (Integration baseline).
//
//====================================================================

#include "ServiceMode.hh"

//@ Usage-Classes

#include "FlowSensor.hh"
#include "Psol.hh"

#ifdef SIGMA_UNIT_TEST
# include "FakeSensor.hh"
# include "FakePsol.hh"
#endif  // SIGMA_UNIT_TEST

//@ End-Usage

extern const Int32 MIN_CYCLES_FOR_STABILITY;
extern const Real32 FLOW_STABLE_BOUND_ABS;
extern const Real32 FLOW_STABLE_BOUND_REL;
extern const Real32 FEEDFORWARD_GAIN;


class SmFlowController
{
  public:
	enum FlowControlOwnerId
	{
		NONE,
		FLOW,
		PRESSURE
	};

    SmFlowController(void);
	virtual ~SmFlowController(void);

    static void SoftFault(const SoftFaultID softFaultID,
		  const Uint32      lineNumber,
		  const char*       pFileName  = NULL, 
		  const char*       pPredicate = NULL);

    virtual void newCycle( void);
    virtual void establishFlow( Real32   desiredFlow,
                        MeasurementSide  desiredSide,
                        GasType          desiredGas);
    virtual void stopFlowController( Boolean keepFlowOn = FALSE);
    virtual Boolean waitForStableFlow( Int32 timeoutMS = 2000);
    virtual inline Boolean isFlowStable( void) const;
    virtual inline GasType gasUsed( void) const;
    virtual inline Real32 getFlowTarget() const;

#ifdef SIGMA_DEVELOPMENT
	inline Real32 getStableFlowHighBound(void);
	inline Real32 getStableFlowLowBound(void);
	inline void resetFlowStabilityParams(void);
	inline Boolean flowBecameUnstable(void);
	inline Int32 getFlowInstabilityCounts(void);
#endif  // SIGMA_DEVELOPMENT

  protected:
    //@ Data-Member:  flowTarget_
    // The requested flow target
    Real32      flowTarget_;

    //@ Data-Member:  flowError_
    // The integrated flow error
    Real32      flowError_;

    //@ Data-Member:  startupGain_
    // Startup gain for flow controller
    Real32      startupGain_;

    //@ Data-Member:  requestForFlowControl_
    // TRUE when flow control has been requested
    Boolean     requestForFlowControl_;

    //@ Data-Member:  flowControllerActive_
    // TRUE when actually controlling flow
    Boolean     flowControllerActive_;

    //@ Data-Member:  pFlowSensor_
    // The flow sensor being used to control flow
#ifndef SIGMA_UNIT_TEST
    FlowSensor* pFlowSensor_;
#else
	FakeSensor* pFlowSensor_;
#endif  // SIGMA_UNIT_TEST

    //@ Data-Member:  pPsol_
    // The PSOL being used to control flow
#ifndef SIGMA_UNIT_TEST
    Psol*       pPsol_;
#else
	FakePsol* 	pPsol_;
#endif  // SIGMA_UNIT_TEST

	//@ Data-Member: flowStable_
	// flag indicating flow reached stability
	Boolean flowStable_ ;

	//@ Data-Member: flowCount_
	// counter for flow stability testing
	Uint32 flowCount_ ;

	//@ Data-Member: establishingFlow_
	// flag signalling that "foreground" task is
	// establishing a new flow
	Boolean establishingFlow_;

	//@ Data-Member: 
	// Indicates type of flow controller object (flow or pressure)
	FlowControlOwnerId flowControlId_;

	//@ Data-Member: FlowControlOwner_
	// Indicates whether pressure controller object or
	// flow controller object owns the flow controller
	static FlowControlOwnerId FlowControlOwner_ ;

	//@ Data-Member: keepFlowOn_
	// flag signalling flow controller to keep the PSOL at its
	// last command instead of setting it to 0 when stopping
	// flow controller
	Boolean keepFlowOn_;

	//@ Data-Member: gasUsed_
	// flag indicating which gas is being used
	GasType gasUsed_;

#ifdef SIGMA_DEVELOPMENT
	static Real32 stableFlowHighBound_;
	static Real32 stableFlowLowBound_;	
	static Boolean flowBecameUnstable_;
	static Int32 flowInstabilityCounts_;
#endif  // SIGMA_DEVELOPMENT

  private:
    SmFlowController(const SmFlowController&);	// not implemented...
    void   operator=(const SmFlowController&);	// not implemented...
};

// Inlined methods...
#include "SmFlowController.in"

#endif // SmFlowController_HH 
