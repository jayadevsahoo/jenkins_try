/*****************************************************************************/
/*   Module Name:   pb_util.c                                                */
/*                                                                           */
/*   Purpose:                                                                */
/*                                                                           */
/*   Requirements:   This module implements requirements detailed in         */
/*                   <75359-93>                                              */
/*                                                                           */
/*   Modification History:                                                   */
/*                                                                           */
/*   Description of Change                  By                Date           */
/*   ---------------------                  -----             ------         */
/*                                                                           */
/*   The parameters of all the              Hamsavally(CGS)   12 Jan,2001    */
/*   functions are modifed.                                                  */
/*                                                                           */
/*   The TXEngine related files are         Hamsavally(CGS)   20 Dec,2000    */
/*   removed and the required header files                                   */
/*   are added.                                                              */
/*                                                                           */
/*   Updated comment blocks format          Dave Richardson   19 Nov, 2002   */
/*                                                                           */
/*   $Revision::   1.2   $                                                   */
/*                                                                           */
/*   $Log:   //pleasa1apo2/swqa/data/arc/840_CPVT_VTS/Program/tests/Pb_util.c-arc  $
 * 
 *    Rev 1.2   04 Dec 2002 15:36:26   Buchman
 * 840 VTS build done
 * 
 *    Rev 1.1   20 Nov 2002 11:16:26   Richardson
 * Updated per SCRs # 32
                                                                             */
/*****************************************************************************/

/*****************************************************************************
THIS IS A PROPRIETARY WORK TO WHICH PURITAN-BENNETT CORPORATION CLAIMS
EXCLUSIVE RIGHT.  NO PART OF THIS WORK MAY BE USED, DISCLOSED, REPRODUCED,
STORED IN AN INFORMATION RETIEVAL SYSTEM, OR TRANSMITTED, IN ANY FORM OR BY ANY
MEANS ELECTRONIC, MECHANICAL, PHOTOCOPYING, RECORDING OR OTHERWISE WITHOUT THE
PRIOR WRITTEN PERMISSION OF PURITAN-BENNETT CORPORATION.
 
Copyright (c) 1999-2002, Puritan-Bennett
All rights reserved.
*******************************************************************************/

/***********************************************/
/****************** INCLUDES *******************/
/***********************************************/

#include <ansi_c.h>
#include <formatio.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <toolbox.h>
#include <userint.h>
#include <utility.h>

#include "globals.h"
#include "cpvtmain.h"
#include "dbapi.h"
#include "ldb.h"
#include "mainpnls.h"
#include "pb_util.h"
#include "testpnl.h"

/***********************************************/
/******************* DEFINES *******************/
/***********************************************/

#define QUOTES '\"'
#define FAILED_LIMITS 1
#define FOUND_QUOTES  2
#define OUT_OF_MEMORY 3
#define FACT_START_SIZE 1024

#define MEMORY_BLOCK 4096

#define MAX_LINE 81 /* Maximum number of characters per line */

/***********************************************/
/************** GLOBAL VARIABLES ***************/
/***********************************************/

int gReportScreenOpt = RPT_ALL;

/***********************************************/
/************ FUNCTION DECLARATION  ************/
/***********************************************/

/*****************************************************************************/
/* Name:     UpdateRevisionInfo                                              */
/*                                                                           */
/* Purpose:  This function updates the program revision on the test exec's   */
/*           main panel.                                                     */
/*                                                                           */
/* Example Call:   int UpdateRevisionInfo(char *rev_string);                 */
/*                                                                           */
/* Input:    PVCS revision string for test program                           */
/*                                                                           */
/* Output:   <None>                                                          */
/*                                                                           */
/* Globals Changed:                                                          */
/*           gProgramRev                                                     */
/*                                                                           */
/* Globals Used:                                                             */
/*                                                                           */
/*                                                                           */
/* Return:   <> 0   If error occurs.                                         */
/*              0   If successful.                                           */
/*                                                                           */
/*****************************************************************************/
int UpdateRevisionInfo(char *rev_string)
{
	double rev;
	char file_rev[5];

	if(sscanf(rev_string, "$Revision:: %lf ", &rev) == 0){
		return 1;
	}

	/* Place revision into string */
	sprintf(file_rev, "%2.1f", rev);

	/* Copy program revision to global program revision variable */
	#ifndef DEMOMODE
	strcpy(gProgramRev, file_rev);
	#else
	strcpy(gProgramRev, "840VTS(E) DEMO");
	#endif

	return 0;
}


/*****************************************************************************/
/* Name:     PostInt                                                         */
/*                                                                           */
/* Purpose:  The function is used to post integer type fact information.     */
/*           The function reformats and logs the test data to the output     */
/*           buffer, stores a pass/fail into the data->result field, alot    */
/*           needed memory to the output buffer, checks the "Fact"           */
/*           variable's string format and stores a fail into the             */
/*           data->result if quotation marks are found in the string field,  */
/*           checks if the measured value is within the min/max limits, and  */
/*           returns a defined status to the calling function.               */
/*                                                                           */
/* Example Call:   int PostInt(int iIndex,char **data, char *Fact,           */
/*                             int MinRdg, int Reading, int MaxRdg);         */
/*                                                                           */
/* Input:    The parameter list passed to the function is in the sequence    */
/*           and defined as: Address to the tTestData structure, address to  */
/*           Fact string, Min. integer limit, Measured value,                */
/*           Max integer limit.                                              */
/*                                                                           */
/* Output:   Reformatted contents of data->outBuffer, and store pass/fail    */
/*           into data->result field of the tTestData structure .            */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Globals Used:                                                             */
/*                                                                           */
/*                                                                           */
/* Return:    Predefined status number.                                      */
/*                                                                           */
/*****************************************************************************/
int PostInt(int iIndex, char **data, char *Fact,
            int MinRdg, int Reading, int MaxRdg)
{
	char pf_Flag;
	int status = 0;
	int index;
	char *tmp;

	if(strchr(Fact, QUOTES)){
		tDataRec[iIndex].result = FAIL;
		status = FOUND_QUOTES;
	}

	if(Reading <= MaxRdg && Reading >= MinRdg){
		pf_Flag = 'P';
		tDataRec[iIndex].result = PASS;
	}
	else{
		pf_Flag = 'F';
		tDataRec[iIndex].result = FAIL;
		status = FAILED_LIMITS;
	}

	if(*data == NULL){
		if((*data = malloc(MEMORY_BLOCK)) == NULL){
			return OUT_OF_MEMORY;
		}
		else{
			**data = NULL;
		}
	}

	sprintf(*data + strlen(*data),
	        FACT_INT ", %c, \"%s\", %d, %d, %d\n", pf_Flag, Fact,
	        MinRdg, Reading, MaxRdg);

#if 0
	GetCtrlIndex(gPanelHandle[TESTPANEL], TESTPANEL_SEQUENCEDISPLAY, &index);

	//                       NO_DB  NO_LOCAL           NO_JET
	tmp = Tfmtdisplay(*data, FALSE, FALSE, NULL, NULL, FALSE, index);

	/* Post results to the screen */
	PostResults(tmp);

	/* Free the buffer allocated by Tfmtdisplay */
	free(tmp);
#endif

	/* Redisplay panel */
	DisplayTestPanel();

	return status;
}


/*****************************************************************************/
/* Name:     PostReal                                                        */
/*                                                                           */
/* Purpose:  The function is used to post real type fact information.        */
/*           The function reformats and logs the test data to the output     */
/*           buffer, stores a pass/fail into the data->result field, alot    */
/*           needed memory to the output buffer, checks the "Fact"           */
/*           variable's string format and stores a fail into the             */
/*           data->result if quotation marks are found in the string field,  */
/*           checks if the measured value is within the min/max limits, and  */
/*           returns a defined status to the calling function.               */
/*                                                                           */
/* Example Call:  int PostReal(int iIndex, char **data, char *Fact,          */
/*                             double MinRdg, double Reading, double MaxRdg) */
/*                                                                           */
/* Input:    The parameter list passed to the function is in the sequence    */
/*           and defined as: Address to the tTestData structure, address to  */
/*           Fact string, Min. real limit, Measured value, Max real limit.   */
/*                                                                           */
/* Output:   Reformatted contents of data->outBuffer, and store              */
/*           pass/fail into data->result field of the tTestData structure .  */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Globals Used:                                                             */
/*                                                                           */
/*                                                                           */
/* Return:   Predefined status number.                                       */
/*                                                                           */
/*****************************************************************************/
int PostReal(int iIndex, char **data, char *Fact,
             double MinRdg, double Reading, double MaxRdg)
{
	char pf_Flag;
	int status = 0;
	int index;
	char *tmp;

	if(strchr(Fact, QUOTES)){
		tDataRec[iIndex].result = FAIL;
		status = FOUND_QUOTES;
	}

	if(Reading <= MaxRdg && Reading >= MinRdg){
		pf_Flag = 'P';
		tDataRec[iIndex].result = PASS;
	}
	else{
		pf_Flag = 'F';
		tDataRec[iIndex].result = FAIL;
		status = FAILED_LIMITS;
	}

	if(*data == NULL){
		if((*data = malloc(MEMORY_BLOCK)) == NULL){
			return OUT_OF_MEMORY;
		}
		else{
			**data = NULL;
		}
	}

	sprintf(*data + strlen(*data),
	        FACT_REAL ", %c, \"%s\", %.4g,  %.4g, %.4g\n", pf_Flag, Fact,
	        MinRdg, Reading, MaxRdg);

	/* Redisplay panel */
	DisplayTestPanel();

	return status;
}


/*****************************************************************************/
/* Name:     PostStringFail                                                  */
/*                                                                           */
/* Purpose:  The function is used to post failing test and comment           */
/*           information. The function reformats and logs the test data to   */
/*           the output buffer, alot needed memory for the output buffer,    */
/*           checks the "Fact" and the comment variable's string format and  */
/*           stores a fail into the data->result if quotation marks are      */
/*           found in the string field, finally, returns a defined status to */
/*           the calling function.                                           */
/*                                                                           */
/* Example Call: int PostStringFail(int iIndex, char **data,                 */
/*                                  char * Fact, char *str)                  */
/*                                                                           */
/* Input:    The parameter list passed to the function is in the sequence    */
/*           and defined as: Address to the tTestData structure, address to  */
/*           Fact string, address to the comment string.                     */
/*                                                                           */
/* Output:   Reformatted contents of data->outBuffer tTestData structure,    */
/*           store pass/fail into data->result field.                        */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Globals Used:                                                             */
/*                                                                           */
/*                                                                           */
/* Return:   Predefined status number.                                       */
/*                                                                           */
/*****************************************************************************/
int PostStringFail(int iIndex, char **data, char *Fact, char *str)
{
	int status = 0;
	int index;
	char *tmp;

	if((strchr(Fact, QUOTES)) || (strchr(str, QUOTES))){
		tDataRec[iIndex].result = FAIL;
		status = FOUND_QUOTES;
	}

	if(*data == NULL){
		if((*data = malloc(MEMORY_BLOCK)) == NULL){
			return OUT_OF_MEMORY;
		}
		else{
			**data = NULL;
		}
	}

	sprintf(*data + strlen(*data),
	        FACT_STR_F ", %c, \"%s\", \"%s\"\n", 'F', Fact, str);

	/* Redisplay panel */
	DisplayTestPanel();

	return status;
}


/*****************************************************************************/
/* Name:     PostStringPass                                                  */
/*                                                                           */
/* Purpose:  The function is used to post passing test and comment           */
/*           information. The function reformats and logs the test data to   */
/*           the output buffer, alot needed memory for the output buffer,    */
/*           checks the "Fact" and the comment variable's string format and  */
/*           stores a fail into the data->result if quotation marks are      */
/*           found in the string field, finally, returns a defined status to */
/*           the calling function.                                           */
/*                                                                           */
/* Example Call:   int PostStringPass(int iIndex,char **data,                */
/*                                    char * Fact, char *str)                */
/*                                                                           */
/* Input:    The parameter list passed to the function is in the sequence    */
/*           and defined as: Address to the tTestData structure, address to  */
/*           Fact string, address to the comment string.                     */
/*                                                                           */
/* Output:   Reformatted contents of data->outBuffer tTestData structure,    */
/*           store pass/fail into data->result field.                        */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Globals Used:                                                             */
/*                                                                           */
/*                                                                           */
/* Return:   Predefined status number.                                       */
/*                                                                           */
/*****************************************************************************/
int PostStringPass(int iIndex, char **data, char *Fact, char *str)
{
	int status = 0;
	int index;
	char *tmp;

	if((strchr(Fact, QUOTES)) || (strchr(str, QUOTES))){
		tDataRec[iIndex].result = FAIL;
		status = FOUND_QUOTES;
	}

	if(*data == NULL){
		if((*data = malloc(MEMORY_BLOCK)) == NULL){
			return OUT_OF_MEMORY;
		}
		else{
			**data = NULL;
		}
	}

	sprintf(*data + strlen(*data),
	        FACT_STR_P ", %c, \"%s\", \"%s\"", 'P', Fact, str);

	/* Redisplay panel */
	DisplayTestPanel();

	return status;
}


/*****************************************************************************/
/* Name:     PostStringNoMsg                                                 */
/*                                                                           */
/* Purpose:  The function is used to post general test and comment           */
/*           information. The function reformats and logs the test data to   */
/*           the output buffer, alot needed memory for the output buffer,    */
/*           checks the "Fact" and the comment variable's string format and  */
/*           finally, returns a defined status to the calling function.      */
/*                                                                           */
/* Example Call:   int PostStringNoMsg(char **data, char *Fact)              */
/*                                                                           */
/* Input:    The parameter list passed to the function is in the sequence    */
/*           and defined as: Address to the tTestData structure, address to  */
/*           Fact string.                                                    */
/*                                                                           */
/* Output:   Reformatted contents of data->outBuffer tTestData structure     */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Globals Used:                                                             */
/*                                                                           */
/*                                                                           */
/* Return:    Predefined status number                                       */
/*                                                                           */
/*****************************************************************************/
int PostStringNoMsg(char **data, char *Fact)
{
	int status = 0;
	int index;
	char *tmp;

	// remove leading white space from "fact", some vent strings have leading space, some don't.
	while(*Fact == ' '){
		Fact++;
	}

	if(strchr(Fact, QUOTES)){
		status = FOUND_QUOTES;
	}

	if(*data == NULL){
		if((*data = malloc(MEMORY_BLOCK)) == NULL){
			return OUT_OF_MEMORY;
		}
		else{
			**data = NULL;
		}
	}

	sprintf(*data + strlen(*data),
	        FACT_STR_N ", %c, \"%s\"", 'N', Fact);


	/* Redisplay panel */
	DisplayTestPanel();
	

	return status;
}


/*****************************************************************************/
/* Name:     PostStringSkip                                                  */
/*                                                                           */
/* Purpose:  The function is used to post passing test and comment           */
/*           information. The function reformats and logs the test data to   */
/*           the output buffer, alot needed memory for the output buffer,    */
/*           checks the "Fact" and the comment variable's string format and  */
/*           stores a fail into the data->result if quotation marks are      */
/*           found in the string field, finally, returns a defined status to */
/*           the calling function.                                           */
/*                                                                           */
/* Example Call:  int PostStringSkip(int iIndex, char **data,                */
/*                                   char *Fact, char *str)                  */
/*                                                                           */
/* Input:    The parameter list passed to the function is in the sequence    */
/*           and defined as: Address to the tTestData structure, address to  */
/*           Fact string, address to the comment string.                     */
/*                                                                           */
/* Output:   Reformatted contents of data->outBuffer tTestData structure,    */
/*           store pass/fail into data->result field.                        */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Globals Used:                                                             */
/*                                                                           */
/*                                                                           */
/* Return:   Predefined status number.                                       */
/*                                                                           */
/*****************************************************************************/
int PostStringSkip(int iIndex, char **data, char *Fact, char *str)
{
	int status = 0;
	int index;
	char *tmp;

	if((strchr(Fact, QUOTES)) || (strchr(str, QUOTES))){
		tDataRec[iIndex].result= FAIL;
		status = FOUND_QUOTES;
	}

	if(*data == NULL){
		if((*data = malloc(MEMORY_BLOCK)) == NULL){
			return OUT_OF_MEMORY;
		}
		else{
			**data = NULL;
		}
	}

	sprintf(*data + strlen(*data),
	        FACT_STR_P ", %c, \"%s\", \"%s\"\n", 'S', Fact, str);

	/* Redisplay panel */
	DisplayTestPanel();

	return status;
}


/*****************************************************************************/
/* Name:     PostTitle                                                       */
/*                                                                           */
/* Purpose:  This function posts a title only message                        */
/*           to the test result window                                       */
/*                                                                           */
/* Example Call:   void PostTitle("Test Title");                             */
/*                                                                           */
/* Input:    char *title - title to display                                  */
/*                                                                           */
/* Output:   <None>                                                          */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Globals Used:  <None>                                                     */
/*                                                                           */
/*                                                                           */
/* Return:   <None>                                                          */
/*                                                                           */
/*****************************************************************************/
void PostTitle(char *title)
{
	BOOL nLocalTitle = FALSE;

	if(title == NULL){
		title = malloc(1);
		*title = '\0';
		nLocalTitle = TRUE;
	}
	InsertTextBoxLine(gPanelHandle[TESTPANEL], TESTPANEL_TESTDISPLAY, -1, title);
	AutoScrollResults();

	if(nLocalTitle){
		free(title);
		title = NULL;
	}

	return;
}


/*****************************************************************************/
/* Name:     PostResults                                                     */
/*                                                                           */
/* Purpose:  This function posts results to the test results window          */
/*                                                                           */
/* Example Call:   void PostResults(int iIndex, char *Results)               */
/*                                                                           */
/* Input:    data    - data to display                                       */
/*           Results - supporting data description                           */
/*                                                                           */
/* Output:   <None>                                                          */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Globals Used:   gReportScreenOpt                                          */
/*                                                                           */
/*                                                                           */
/* Return:    <None>                                                         */
/*                                                                           */
/*****************************************************************************/
void PostResults(int iIndex, char *Results)
{
	switch(gReportScreenOpt){
		case RPT_ALL:
			InsertTextBoxLine(gPanelHandle[TESTPANEL], TESTPANEL_TESTDISPLAY, -1, Results);
			break;
		case RPT_PASSONLY:
			if(tDataRec[iIndex].result == PASS){
				InsertTextBoxLine(gPanelHandle[TESTPANEL], TESTPANEL_TESTDISPLAY, -1, Results);
			}
			break;
		case RPT_FAILONLY:
			if(tDataRec[iIndex].result == FAIL){
				InsertTextBoxLine(gPanelHandle[TESTPANEL], TESTPANEL_TESTDISPLAY, -1, Results);
			}
			break;
		case RPT_NONE:
			break;
	}

	return;
}


/*****************************************************************************/
/* Name:     PostInfo2Window                                                 */
/*                                                                           */
/* Purpose:  This is a wrapper function which displays Title and results     */
/*           to test results window.                                         */
/*                                                                           */
/* Example Call:   void PostInfo2Window(int index, char *Results,            */
/*                                      char *title)                         */
/*                                                                           */
/* Input:    data    - data to display                                       */
/*			 Results - supporting data description                           */
/*           title   - title of test                                         */
/*                                                                           */
/* Output:   <None>                                                          */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Globals Used:                                                             */
/*                                                                           */
/*                                                                           */
/* Return:    <None>                                                         */
/*                                                                           */
/*****************************************************************************/
void PostInfo2Window(int index, char *Results, char *title)
{
	PostTitle(title);
	PostResults(index, Results);
	AutoScrollResults();

	return;
}


/*****************************************************************************/
/* Name:     Tfmtdisplay                                                     */
/*                                                                           */
/* Purpose:  This function accepts a string and returns a reformatted string */
/*           to the calling function. The function parses through a string   */
/*           which contains a PASS/FAIL flag, message, and measurement info. */
/*           This information is reassembled to be more readable for the     */
/*           operator display. This function also creates a string to be     */
/*           logged to the test step database file when the db_format flag   */
/*           is set.                                                         */
/*                                                                           */
/* Example Call:   char *Tfmtdisplay(char *rec_string, int db_format,        */
/*                                   char *serial_num, char *rev_num,        */
/*                                   int index);                             */
/*                                                                           */
/* Input:    rec_string - for test program                                   */
/*           db_format  - TRUE if formatting the test step string            */
/*           serial_num - serial number of UUT                               */
/*           rev_num    - revision of UUT                                    */
/*           DB_Jet     - TRUE if storing to access database                 */
/*           index      - test step number                                   */
/*                                                                           */
/* Output:   gDatabaseBuffer  - when db_format is TRUE                       */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Globals Used:                                                             */
/*                                                                           */
/*                                                                           */
/* Return:   char *  -  reformatted string                                   */
/*                                                                           */
/*****************************************************************************/
char *Tfmtdisplay(char *rec_string, BOOL db_format, BOOL nLocalized, 
                  char *serial_num, char *rev_num, BOOL nDB_Jet, int index)
{
	char *temp_buf;  // ptr to char buffer used to build formatted display of test results
	char *start_ptr; // &temp_buf

	int    res_type; // FACT_INT = 1, FACT_REAL = 2, FACT_STR_P = 3, FACT_STR_F = 4, FACT_STR_N = 5
	char   pass_fail;           // 'P' or 'F'
	int    min, measured, max;  // measurement & limits when res_type = FACT_INT
	double low, reading,  high; // measurement & limits when res_type = FACT_REAL
	char   szLow[16], szReading[16], szHigh[16];
	char   temp_str[150];       // receives text of test name
	char   *temp_ptr;           // used to address end of test name in rec_string
	int    i;                   // counter to copy test name chars from rec_string to temp_str
	char   *temp_string;        // text for gDatabaseBuffer

	/* Ensure that we have a valid string */
	if(rec_string == NULL){
		return NULL;
	}

	/* Grab memory for strings and Extra memory is for the PASS or FAIL string. */
	temp_buf = (char *)malloc(strlen(rec_string) + 256);   
	start_ptr = temp_buf;

	if(db_format){
		/* Grab extra memory just in case database string is long */
		temp_string = (char *)malloc(strlen(rec_string) + 100);
		gDatabaseBuffer = (char *)malloc(strlen(rec_string) * 9);
		*gDatabaseBuffer = NULL;
	}

	do{
		switch(*rec_string){
			case '1': // FACT_INT
			case '2': // FACT_REAL
				/* Strip out the type of post and pass/fail flag */
				sscanf(rec_string, "%d, %c, ", &res_type, &pass_fail);

				/* Skip to the start of the string and find the */
				/* other end of the string and store it         */
				rec_string += 7;
				temp_ptr = strchr(rec_string, '\"');

				/* Copy the string */
				for(i = 0; rec_string < temp_ptr; i++){
					temp_str[i] = *rec_string++;
				}
				temp_str[i] = '\0'; // add end-of-string token
				if(nLocalized){
					LocalizeNumberInString(temp_str);
				}

				/* Skip to the readings and scan them in */
				rec_string += 3;

				if(res_type == 1){ // FACT_INT
					sscanf(rec_string, "%d, %d, %d\n", &min, &measured, &max);
				}
				else if(res_type == 2){ // FACT_REAL
					sscanf(rec_string, "%lg, %lg, %lg\n", &low, &reading, &high);
				}

				/* Find the end of this line */
				rec_string = strchr(rec_string, '\n');
				rec_string++;

				/* Create the string that will be displayed */
				*temp_buf = NULL;
				Fmt(temp_buf, "%s[a]<%s", "   ");

				/* Place the pass/fail info in string */
				if(pass_fail == 'P'){
					Fmt(temp_buf, "%s[a]<%s \0", nLocalized ? LDBf(Ldb_Pass_caps) : LDB(Ldb_Pass_caps, ENGLISH));
				}
				if(pass_fail == 'F'){
					Fmt(temp_buf, "%s[a]<%s \0", nLocalized ? LDBf(Ldb_Fail_caps) : LDB(Ldb_Fail_caps, ENGLISH));
				}

				/* Place the quoted text into the string */
				/* Place the limits and readings into the string */
				if(res_type == 1){ // FACT_INT
					sprintf(temp_buf + strlen(temp_buf), "%s %d <=  %d  <= %d \n", temp_str, min, measured, max);
				}
				else if(res_type == 2){ // FACT_REAL
					sprintf(szLow, "%.4g", low);
					if(nLocalized){
						LocalizeNumberInString(szLow);
					}
					sprintf(szReading, "%.4g", reading);
					if(nLocalized){
						LocalizeNumberInString(szReading);
					}
					sprintf(szHigh, "%.4g", high);
					if(nLocalized){
						LocalizeNumberInString(szHigh);
					}
					sprintf(temp_buf + strlen(temp_buf), "%s %s <=  %s  <= %s \n", temp_str, szLow, szReading, szHigh);
				}
				temp_buf = strchr(temp_buf, '\n');
				temp_buf++;

				/* Check to see if we are updating the database file */
				if(db_format){
					/* Run #, Serial #, Part #, Rev, Test #, Name, Low, Measured, High, Pass/Fail */
					if(res_type == 1){ // FACT_INT
						sprintf(temp_string, "%ld, %s, %s, %s, %d, %s, %d, %d, %d, %c \n",
						        gRunNumber, serial_num, gUUTPN, rev_num, index + 1, temp_str, 
						        min, measured, max, pass_fail);
					}
					else if(res_type == 2){ // FACT_REAL
						sprintf(temp_string, "%ld, %s, %s, %s, %d, %s, %lg, %lg, %lg, %c \n",
						        gRunNumber, serial_num, gUUTPN, rev_num, index + 1, temp_str, 
						        low, reading, high, pass_fail);
					}
					Fmt(gDatabaseBuffer, "%s[a]<%s", temp_string);
				}
				if(nDB_Jet){
					if(res_type == 1){ // FACT_INT
						LoadTestInfo(index + 1, pass_fail, temp_str, (double)min, (double)max, (double)measured, res_type);
						WriteTestDB_Jet();
					}
					else if(res_type == 2){ // FACT_REAL
						LoadTestInfo(index + 1, pass_fail, temp_str, low, high, reading, res_type);
						WriteTestDB_Jet();
					}
				}
				break;

			case '3': // FACT_STR_P
			case '4': // FACT_STR_F
			case '5': // FACT_STR_N
				/* Strip out the type of post and pass/fail flag */
				sscanf(rec_string, "%d, %c, ", &res_type, &pass_fail);

				/* Skip to the start of the string and find the */
				/* other end of the string and store it         */
				rec_string += 7;
				temp_ptr = strchr(rec_string, '\"');

				/* Copy the string */
				for(i = 0; rec_string < temp_ptr; i++){
					temp_str[i] = *rec_string++;
				}
				temp_str[i] = '\0'; // add end-of-string token
				if(nLocalized){
					LocalizeNumberInString(temp_str);
				}

				/* Create the string that will be displayed */
				*temp_buf = NULL;
				Fmt(temp_buf, "%s[a]<%s", "   ");

				/* Place the pass/fail info in string */
				if(pass_fail == 'P'){
					Fmt(temp_buf, "%s[a]<%s ", nLocalized ? LDBf(Ldb_Pass_caps) : LDB(Ldb_Pass_caps, ENGLISH));
				}
				if(pass_fail == 'F'){
					Fmt(temp_buf, "%s[a]<%s ", nLocalized ? LDBf(Ldb_Fail_caps) : LDB(Ldb_Fail_caps, ENGLISH));
				}
				if(pass_fail == 'N'){
					;
				//	Fmt(temp_buf, "%s[a]<  \0");
				}

				/* Place the quoted text into the string */
				Fmt(temp_buf, "%s[a]<%s", temp_str);

				temp_buf = strchr(temp_buf, '\0');
				*temp_buf = '\n';
				temp_buf++;

				/* Check to see if we are updating database file */
				if(db_format){
					/* Run #, Serial #, Part #, Rev, Test #, Name, Low, Measured, High, Pass/Fail */
					sprintf(temp_string, "%ld, %s, %s, %s, %d, %s, %lg, %lg, %lg, %c \n",
					        gRunNumber, serial_num, gUUTPN, rev_num, index + 1, temp_str,
					        0.0, 0.0, 0.0, pass_fail);
					Fmt(gDatabaseBuffer, "%s[a]<%s", temp_string);
				}
				if(nDB_Jet){
					LoadTestInfo(index + 1, pass_fail, temp_str, 0.0, 0.0, 0.0, res_type);
					WriteTestDB_Jet();
				}
				break;

			default:
				rec_string++;
				break;

		} // end switch(*rec_string)

	} while(*rec_string);

	*temp_buf = '\0'; /* insert end of string terminator */
	if(db_format){
		free(temp_string);
	}

	return start_ptr; // &temp_buf
}

