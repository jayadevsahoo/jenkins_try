#ifndef DiagLogMenuSubScreen_HH
#define DiagLogMenuSubScreen_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: DiagLogMenuSubScreen - The screen selected by pressing the
// Diagnostic Code Log button on the Upper Other Screens Subscreen.
// Displays menu for accessing diagnostic logs.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/DiagLogMenuSubScreen.hhv   25.0.4.0   19 Nov 2013 14:07:42   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 002  By:  yyy    Date:  10-Sep-97    DR Number: 1923
//    Project:  Sigma (R8027)
//    Description:
//      Changed the name of communications Diagnostic log to the system
//		information log.
//
//  Revision: 001  By:  wks    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "SubScreen.hh"
#include "LogTarget.hh"

//@ Usage-Classes
#include "SubScreenTitleArea.hh"
#include "GuiAppClassIds.hh"
class ScrollableLog;
//@ End-Usage

class SubScreenArea;

class DiagLogMenuSubScreen : public SubScreen, public LogTarget 
{
public:
	DiagLogMenuSubScreen(SubScreenArea *pSubScreenArea);
	~DiagLogMenuSubScreen(void);

	// Inherited from SubScreen
	virtual void activate(void);
	virtual void deactivate(void);

	// Inherited from LogTarget
	virtual void getLogEntryColumn(Uint16 entryNumber, Uint16 colNumber,
							Boolean &rIsCheapText,
							StringId &rString1, StringId &rString2, StringId &, StringId &);

	virtual void currentLogEntryChangeHappened(
							Uint16 changedEntry, Boolean isSelected);

	static void SoftFault(const SoftFaultID softFaultID,
								const Uint32      lineNumber,
								const char*       pFileName  = NULL, 
								const char*       pPredicate = NULL);

protected:

private:
	// these methods are purposely declared, but not implemented...
	DiagLogMenuSubScreen(SubScreen *pSubScreen);		// not implemented..
	DiagLogMenuSubScreen(const DiagLogMenuSubScreen&);	// not implemented...
	void operator=(const DiagLogMenuSubScreen&);		// not implemented...


	//@  MenuEntryTopics_
	//	Array of Menu Entry topics 
	StringId MenuEntryTopics_[3];

	//@ Data-Member: titleArea_
	// The sub-screen's title at the top left of the screen
	SubScreenTitleArea titleArea_;

	//@ Data-Member: pMenu_
	// Diagnostic Log menu
	ScrollableLog *pMenu_;

};

#endif // DiagLogMenuSubScreen_HH 
