#ifndef XENA2TODCLOCK_HH
#define XENA2TODCLOCK_HH
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2011, Puritan-Bennett Corporation
//=====================================================================


//============================ H E A D E R   D E S C R I P T I O N ====
//@ Filename: STM48T58 - Register declarations for ST M48T58 device
//
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/POST-Library/vcssrc/STM48T58.hhv   25.0.4.0   19 Nov 2013 14:16:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001    By: gdc     Date: 26-Jan-2011 SCR Number: 6671
//  Project: XENA2
//  Description:
//		Initial version
//
//=====================================================================

#include "Sigma.hh"

struct STM48T58RegMap
{
	Uint write_bit:1;
	Uint read_bit:1;
	Uint calibration_sign:1;
	Uint calibration:5;
	
	Uint disable_real_time_oscillator:1;  // set to one by manufacturer
										  // must be set to 0 to run the
										  // the real time clock
	Uint second_tens:3;
	Uint second_ones:4;
	
	Uint minute_unused:1;
	Uint minute_tens:3;
	Uint minute_ones:4;
	
	Uint hour_unused:2;
	Uint hour_tens_msb:1;
	Uint hour_tens_lsb:1;
	Uint hour_ones:4;
	
	Uint day_of_wk_unused:1;
	Uint freq_test_mode_enable:1;
	Uint day_of_week_unused:3;
	Uint day_of_week:3;
	
	Uint date_unused:2;
	Uint date_tens:2;
	Uint date_ones:4;
	
	Uint month_unused:3;
	Uint month_tens:1;
	Uint month_ones:4;
	
	Uint year_tens:4;
	Uint year_ones:4;
};

#endif // XENA2TODCLOCK_HH

