#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//     Copyright (c) 1995, Nellcor Puritan Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: NovRamEventRegistrar - The central registration, control, and
// dispatch point for NovRam update NovRam events received by GUI-Apps.  The
// update events are received from the Persistent-Objects subsystem via the GUI
// task's User Annunciation queue.
//---------------------------------------------------------------------
//@ Interface-Description
// Objects which handle GUI NovRam events use the RegisterTarge() method to
// register for a callback.  The GUI-Applications subsystem calls the
// RegisterTarget() method at initialization time to setup the lists of 
// NovRam event targets.  Any object which needs to know when a NovRam event
// occured uses RegisterTarget() to register itself for receiving update notices.
//
// Before registering of targets can occur, though, the Initialize()
// method must be called to do one-time initialization of this class.
// All unique GUI NovRam events are specified with the NovRamUpdateManagerclass
// enum: UpdateTypeId.
//---------------------------------------------------------------------
//@ Rationale
// This class is necessary to register, set, and centrally dispatch NovRam
// events which are received from the Persistent-Objects subsystem.
//---------------------------------------------------------------------
//@ Implementation-Description
// Only one instance of this class is created.  The class is implemented with
// the CascadeArray_[] as the centerpiece.  This is an array of
// NovRamEventCascade objects -- one for each unique NovRam event type.  The
// NovRamUpdateManager::UpdateTypeId for a event acts as the index into the
// CascadeArray_[].  The RegisterTarget() method is used to register
// CascadeArray_[] for update notices through callback call.  The EventHappened()
// method accesses the CascadeArray_[] at run time to forward the external Novram
// events to the proper GUI objects.
//
// So, say, an object is interested in a particular NovRam event.  The object
// needs to multiply inherit from NovRamEventTarget and call the RegisterTarget()
// method of this class specifying the id of the particular NovRam event to register
// for update notices.  It will then be notified ever after of changes to this
// setting via its novRamUpdateEventHappened() method.
///---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and pre-conditions
// to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
// Due to a limitation in NovRamEventCascade, there is a limit to the number of
// targets that can be registered with a single NovRam event..  This number can be
// increased if the need arises.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/NovRamEventRegistrar.ccv   25.0.4.0   19 Nov 2013 14:08:10   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 002  By:  yyy    Date:  04-AUG-1997    DCS Number: 1803
//  Project:  Sigma (R8027)
//  Description:
//      Registered for DATE/TIME settings to capture the setting change
//		events and update the date/time accordingly.
//
//  Revision: 001  By:  mpm    Date:  16-OCT-95    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "NovRamEventRegistrar.hh"

//@ Usage-Classes
#include "NovRamEventTarget.hh"
#include "UserAnnunciationMsg.hh"
//@ End-Usage

//@ Code...

//
// Word-aligned memory pools for static member data.
//
static Uint CascadeArrayMemory_ [
		((sizeof(NovRamEventCascade) + sizeof(Uint) - 1) / sizeof(Uint))
		* NovRamUpdateManager::NUM_UPDATE_ITEMS];

//
// Initializations of static member data.
//
NovRamEventCascade *NovRamEventRegistrar::CascadeArray_ =
								(NovRamEventCascade *)CascadeArrayMemory_;


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: EventHappened
//
//@ Interface-Description
// Called due to a NovRam update event message from the Persistent-Objects
// subsystem.  It is passed the event message sent to this task.  We decode the
// update id (id of the NovRam data that was updated) from this message.  All
// targets which are interested in the specified NovRam event will then be
// informed of the change (via their novRamUpdateEventHappened() method).
//---------------------------------------------------------------------
//@ Implementation-Description
// Looks up the proper NovRamEventCascade object in the CascadeArray_[] (using
// NovRam event as an index) and calls the cascade's novRamUpdateEventHappened()
// method.
//---------------------------------------------------------------------
//@ PreCondition
// The target object for the given 'updateId' must exist.  The update id
// must be valid.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
NovRamEventRegistrar::EventHappened(UserAnnunciationMsg &eventMsg)
{
	CALL_TRACE("NovRamEventRegistrar::TimerEventHappened(NovRamId)");

	NovRamUpdateManager::UpdateTypeId updateId =
		(NovRamUpdateManager::UpdateTypeId)eventMsg.novRamUpdateParts.updateId;

	CLASS_PRE_CONDITION(updateId >= 0 &&
				updateId < NovRamUpdateManager::NUM_UPDATE_ITEMS);

	CascadeArray_[updateId].novRamUpdateEventHappened(updateId);
														// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: RegisterTarget [public, static]
//
//@ Interface-Description
// Objects needing to know when NovRam update events occur use this method to
// register themselves for update notices.  The parameters are as follows:
// >Von
//	updateId	The enum id of the NovRam update data type which identifies
//				the changed data collection.
//	pTarget		ptr to the object to be notified when an update event
//				'updateId' occurs.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//  Registers the given target with the "cascade" associated with the
//  given 'updateId'.  This is done by calling the addTarget() method
//  of NovRamEventCascade.  updateId is used as an index into
//  CascadeArray_[] to find the associated cascade object.
//---------------------------------------------------------------------
//@ PreCondition
// updateId must be valid and pTarget must be non-null.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
NovRamEventRegistrar::RegisterTarget(NovRamUpdateManager::UpdateTypeId updateId,
								  NovRamEventTarget* pTarget)
{
	CALL_TRACE("NovRamEventRegistrar::RegisterTarget(NovRamUpdateManager::UpdateTypeId updateId, "
								  "NovRamEventTarget* pTarget)");

	CLASS_PRE_CONDITION(updateId >= 0 &&
						updateId < NovRamUpdateManager::NUM_UPDATE_ITEMS);
	CLASS_PRE_CONDITION(pTarget != NULL);

	CascadeArray_[updateId].addTarget(pTarget);		// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Initialize [public, static]
//
//@ Interface-Description
// Called at startup by the GUI-Apps subsystem in order to initialize
// static member data.  No parameters needed.
//---------------------------------------------------------------------
//@ Implementation-Description
// Creates a NovRamEventCascade object for each NovRam event id.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
NovRamEventRegistrar::Initialize(void)
{
	CALL_TRACE("NovRamEventRegistrar::Initialize(void)");

	SAFE_CLASS_ASSERTION(sizeof(CascadeArrayMemory_) >=
				(sizeof(NovRamEventCascade) * NovRamUpdateManager::NUM_UPDATE_ITEMS));

	// Run placement news on each array element.
	for (Uint16 i = 0; i < NovRamUpdateManager::NUM_UPDATE_ITEMS; i++)
	{												// $[TI1]
		new (CascadeArray_ + i) NovRamEventCascade;
	}												// $[TI2]
		

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
NovRamEventRegistrar::SoftFault(const SoftFaultID  softFaultID,
						     const Uint32       lineNumber,
						     const char*        pFileName,
						     const char*        pPredicate)  
{
	CALL_TRACE("NovRamEventRegistrar::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, NOVRAMEVENTREGISTRAR,
							lineNumber, pFileName, pPredicate);
}
