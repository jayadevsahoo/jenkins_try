#include "stdafx.h"
//====================================================================
// This is a proprietary work to which Nellcor Puritan Bennett
// corporation of California claims exclusive right.  No part of this
// work may be used, disclosed, reproduced, sorted in an information
// retrieval system, or transmitted by any means, electronic,
// mechanical, photocopying, recording, or otherwise without the prior
// written permission of Nellcor Puritan Bennett Corporation.
//
//     Copyright (c) 1997, Nellcor Puritan Bennett Corporation
//=====================================================================

//====================================================================
// Class:  SerialBuffer - Circular Buffer for Serial Data
//---------------------------------------------------------------------
//@ Interface-Description
//  This class is the base class for the SmcpRequestBuffer and 
//  SmcpResponseBuffer. It contains the functions that manage the
//  transfer of serial data between the application task and the 
//  Serial-Interface task though it can be used in other contexts
//  where a circular packet buffer is required.
//
//  Users of the SerialBuffer write message packets into the circular
//  buffer using the write() method and extract them using the
//  read() method. The read method provides a timeout mechanism to 
//  wait for a message packet if the buffer is empty. The write method
//  provides a timeout to wait for an available packet when the 
//  buffer is full. This class uses a mutex to protect shared control
//  resources but keeps resource locking to a minimum to maximize
//  performance.
//
//---------------------------------------------------------------------
//@ Rationale
//  Encapsulates the serial data circular buffer.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//     The data structure used for the SerialBuffer is a circular buffer
//     of pointers to message packets.
//
//     The structure looks like:
//
//             +----------------------------+
//         +---+-o     pPStartPacket_       |      +-->+---------------+
//         |   +----------------------------+      |   | packet        |
//         |   |       pPEndPacket_       o-+---+  |   |               |
//         |   +----------------------------+   |  |   |               |
//         |   |       packetCount_         |   |  |   |               |
//         |   +----------------------------+   |  |   |               |
//         +-->| pPacketMap_[0]           o-+------+   +---------------+
//             +----------------------------+   |      | packet        |
//             | pPacketMap_[1]           o-+--------->|               |
//             +----------------------------+   |      |               |
//             | pPacketMap_[2]             |   |      |               |
//             +----------------------------+   |      |               |
//             |             ...            |<--+      |               |
//             +----------------------------+          +---------------+
//             | pPacketMap_[numPackets_-1] |
//             +----------------------------+
//
//     An empty buffer is indicated by the condition where the
//     start pointer and the end pointer point to the same array
//     position. In order to prevent ambiguity in distinguishing
//     between a completely full buffer and a completely empty
//     buffer, the buffer is only allowed to grow to a length of
//     numPackets_-1 characters.
//
//     In addition, we keep a "cnt" of the number of characters in
//     the buffer, to make it easier to check to see if the buffer
//     is full. Without the "cnt" field you must increment the end
//     pointer (possibly wrapping around the end of the array), and
//     see if it is the same as the start pointer. Since increment
//     and decrement are cheap operations, this implementation
//     makes it easier to check boundary conditions.
//
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header::   /840/Baseline/GUI-Serial-Interface/vcssrc/SerialBuffer.ccv   10.7   08/17/07 10:18:42   pvcs  
//
//@ Modification-Log
//
//  Revision: 004   By: gdc    Date: 16-Aug-2010     SCR Number: 6663
//  Project: API/MAT
//  Description:
//  Modified to support for VentSet API. Increased idle wait time to
//  100ms from 10ms based since the buffer cannot be filled faster then
//  every 300ms based on the minimum breath time.
// 
//  Revision: 003    By: gdc       Date: 27-Mar-2008  SCR Number: 6407
//  Project: TREND2
//  Description:
//  Removed (re)definition of countof macro to resolve compiler warning.
//  Changed to use Sigma standard MIN_VALUE instead of using local 
//  MIN macro that provides identical functionality.
//
//  Revision: 002    By: quf       Date: 16-Aug-2001  DR Number: 5493
//  Project: GuiComms
//  Description:
//  Introduced encapsulated version of class IDs.
//
//  Revision: 001    By: Gary Cederquist  Date: 24-Nov-1997  DR Number: 2605
//    Project: Sigma (R8027)
//    Description:
//       Complete restructuring of GUI-Serial-Interface to fix
//       several problems.
//
//====================================================================

#include <string.h>    // for memcpy
#include "SerialBuffer.hh"
#include "CriticalSection.hh"
#include "OsTimeStamp.hh"
#include "Task.hh"


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SerialBuffer [constructor]
//
//@ Interface-Description
//  Constructs the SerialBuffer base class. Storage for the packets and
//  the packet map is provided by the derived class which the base class
//  initializes when constructed. This constructor accepts pointers to
//  these structures, a mutex ID for access to the shared control 
//  data, the size of each packet and the number of packets.
//
//  Initializes the packet map that maintains pointers to each packet
//  in packet memory. Initializes the pointers to the start and end 
//  packet in the circular buffer.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  packetSize_ % 4 == 0
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
 

SerialBuffer::SerialBuffer(  IpcId mutexId
	                       , Uint * pPacketMemory
	                       , Uint packetSize
	                       , Uint numPackets 
	                       , Uint ** pPacketMap )

	:   mutexId_( mutexId )
	  , pPacketMemory_( pPacketMemory )
	  , packetSize_( packetSize )
	  , numPackets_( numPackets )
	  , pPacketMap_( pPacketMap )
{
	CLASS_PRE_CONDITION( packetSize_ % 4 == 0 );

	for (Uint i=0; i<numPackets_; i++)
	{	
		pPacketMap_[i] = pPacketMemory_ + packetSize_/sizeof(Uint) * i;
	}
	pPStartPacket_ = pPacketMap_;
	pPEndPacket_   = pPacketMap_;
	packetCount_  = 0;

	// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isEmpty_
//
//@ Interface-Description
//  Returns TRUE if the circular buffer is empty, otherwise returns FALSE.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
 
Boolean
SerialBuffer::isEmpty_(void) const
{
	// $[TI1]
	return ( packetCount_ == 0 );
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isFull_
//
//@ Interface-Description
//  Returns TRUE if the circular buffer is full, otherwise returns FALSE.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
 
Boolean
SerialBuffer::isFull_(void) const
{
	// $[TI1]
	return (   ( pPEndPacket_+1 == pPStartPacket_ ) 
            || ( pPEndPacket_ == pPStartPacket_ + numPackets_-1) );
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: inc_
//
//@ Interface-Description
//  Increments the specified pointer to the next packet pointer in the
//  circular packet map. Returns the incremented pointer.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
 
Uint **
SerialBuffer::inc_( Uint ** pPacket )
{
    if ( pPacket == ( pPacketMap_ + numPackets_-1) ) 
    {
		// $[TI1.1]
        pPacket = pPacketMap_;
    }
    else
    {  
		// $[TI1.2]
        pPacket = pPacket + 1;
    }

	return pPacket;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~SerialBuffer [destructor]
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
 
SerialBuffer::~SerialBuffer()
{
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: read
//
//@ Interface-Description
//  Copies data from the head (starting) packet in the packet buffer
//  to the caller's buffer. Waits up to the specified timeout (in
//  milliseconds) for a packet to be written to the buffer before
//  returning. Transfers up to the number of bytes specified. Returns 
//  the number of bytes actually transferred.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
 
Uint
SerialBuffer::read( void * pMemory, Uint count, Uint milliseconds )
{
	OsTimeStamp startTime;
	Uint byteCount = 0;

	while ( isEmpty_() )
	{
		// $[TI1.1]
		if ( !milliseconds || startTime.getPassedTime() > milliseconds )
		{
			// $[TI2.1]
			break;
		}
		// $[TI2.2]
		idleFunction_();
	}
	// $[TI1.2]
	
	if ( !isEmpty_() )
	{
		// $[TI3.1]
		Uint * pPacket = *pPStartPacket_;
		Uint messageSize = *pPacket; // first word of packet contains byte count
		byteCount = MIN_VALUE( count, messageSize );
    	memcpy( pMemory, pPacket + 1, byteCount );

		pPStartPacket_ = inc_( pPStartPacket_ );
		{
			CriticalSection mutex( mutexId_ );
			--packetCount_;
		}
	}
	// $[TI3.2]
	
    return byteCount;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: write
//
//@ Interface-Description
//  Allocates a packet from the tail (end) of the circular buffer and 
//  copies data from the caller's buffer into the packet buffer.
//  Waits up to the specified timeout (in milliseconds) for a packet 
//  to become available if the buffer is full.  Transfers up to the 
//  number of bytes specified.  Returns the number of bytes actually 
//  transferred.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
 

Uint
SerialBuffer::write( const void * pMemory, Uint count, Uint milliseconds )
{
	OsTimeStamp startTime;
	Uint byteCount = 0;

	while ( isFull_() )
	{
		// $[TI1.1]
		if ( !milliseconds || startTime.getPassedTime() >= milliseconds )
		{
			// $[TI2.1]
			break;
		}
		// $[TI2.2]
		idleFunction_();
	}
	// $[TI1.2]

	if ( !isFull_() )
	{
		// $[TI3.1]
		Uint * pPacket = *pPEndPacket_;
		byteCount = MIN_VALUE(count, packetSize_ - sizeof(Uint));
		*pPacket = byteCount;
		memcpy( pPacket + 1, pMemory, byteCount );

		pPEndPacket_ = inc_( pPEndPacket_ );
		
		{
			CriticalSection mutex( mutexId_ );
			++packetCount_;
		}
	}
	// $[TI3.2]

	return byteCount;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: idleFunction_
//
//@ Interface-Description
//  Provides the idle activity for the caller when a wait is required.
//  Delays the task until the next tick of the OS clock (approximately 
//  100ms) before returning.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
 
void
SerialBuffer::idleFunction_(void) const
{
	Task::Delay(0,100);
	// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
void
SerialBuffer::SoftFault(const SoftFaultID softFaultID,
	                         const Uint32      lineNumber,
	                         const char*       pFileName,
	                         const char*       pPredicate)
{
  FaultHandler::SoftFault(softFaultID, GUI_SERIAL_INTERFACE, 
	  GUI_Serial_Interface::SERIAL_BUFFER_CLASS,
	  lineNumber, pFileName, pPredicate);
}


