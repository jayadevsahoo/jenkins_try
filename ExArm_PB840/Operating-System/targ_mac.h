#ifndef TARG_MAC_68K
#define TARG_MAC_68K
//====================================================================
// This is a proprietary work to which the Covidien corporation claims 
// exclusive right.  No part of this work may be used, disclosed, 
// reproduced, sorted in an information retrieval system, or 
// transmitted by any means, electronic, mechanical, photocopying, 
// recording, or otherwise without the prior written permission of 
// Covidien Corporation.
//
//              Copyright (c) 2009, Covidien Corporation
//=====================================================================

//=====================================================================
//@ Filename: targ_mac.h - target dependent macros (68000 processor)
//---------------------------------------------------------------------
//@ Version-Information
//  @(#) $Header:   /840/Baseline/Operating-System/vcssrc/targ_mac.h_v   25.0.4.0   19 Nov 2013 14:16:22   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By:  gdc    Date:  27-Jan-2009    SCR Number: 6461 
//  Project:  840S
//  Description:
//	Initial version.
//=====================================================================

//***********************************************************************
// THIS INFORMATION IS PROPRIETARY TO
// MICROTEC RESEARCH, INC.
// 2350 Mission College Blvd.
// Santa Clara, CA 95054
// USA
//-----------------------------------------------------------------------
// Copyright (c) 1991-1993 Microtec Research, Inc.
// All rights reserved.
//***********************************************************************
//
//	@(#)targ_mac.h	1.2 2/10/92
//
//***********************************************************************
//
//  This header file defines several target dependent macros
//  which are not normally shipped with a product
//
//***********************************************************************


#define	GET_SIZE ( /* Get size of initfini section */ \
	(ASM (unsigned long, " move.l #.sizeof.(initfini),d0")))

#define GET_START_ADDR ( /* Get start of initfini section */ \
	(ASM (void (**)(), " move.l #.startof.(initfini),d0")))

#define	GET_SIZE_PIX ( /* Get size of pixinit section */ \
	(ASM (unsigned long, " move.l #.sizeof.(pixinit),d0")))

#define GET_START_ADDR_PIX ( /* Get start of initfini section */ \
	(ASM (void (**)(), " move.l #.startof.(pixinit),d0")))




#endif // TARG_MAC_68K
