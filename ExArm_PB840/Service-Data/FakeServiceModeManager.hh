#if defined FAKE_SM
// This class is used only for test simulation.

#ifndef FakeServiceModeManager_HH
#define FakeServiceModeManager_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: FakeServiceModeManager - Fakes out the interface between the
// Service Data subsystem and GUI.  Generates randomly changing
// pieces of Service Data.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Service-Data/vcssrc/FakeServiceModeManager.hhv   25.0.4.0   19 Nov 2013 14:21:02   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================


#include "Sigma.hh"

//@ Usage-Classes
#include "SmTestId.hh"
#include "TestDataPacket.hh"
#include "SmPromptId.hh"
#include "FakeSMControlTimer.hh"
#include "SmStatusId.hh"
#include  "NetworkApp.hh"
//@ End-Usage


class FakeServiceModeManager
{
public:

	FakeServiceModeManager(void);
	~FakeServiceModeManager(void);

#if defined (SIGMA_BD_CPU)
    static void RecvTestMode(XmitDataMsgId, void* pData, Uint size);
    static void RecvActionData(XmitDataMsgId, void* pData, Uint size);
#endif 	// SIGMA_BD_CPU

	static void Initialize(void);
	static void Task(void);
	static void DoTest(SmTestId::ServiceModeTestId testId,
				Boolean testRepeat = FALSE,
				const char * const pParameters = NULL);
	static void DoFunction(SmTestId::FunctionId functionId);
	static void OperatorAction(SmTestId::ServiceModeTestId smTestId,
								SmPromptId::ActionId responseId);

    static Boolean IsGuiTest(SmTestId::ServiceModeTestId smId);
	static void sMTimerEventHappened(void);
	static Boolean randomResult(Int count);
	
	static void initializeFlowSensor(void);
	static void gasSupply(void);
	static void circuitResistance(void);
	static void sstFilter(void);
	static void guiKeyboard(void);
	static void guiKnob(void);
	static void guiLamp(void);
	static void bdLamp(void);
	static void guiAudio(void);
	static void bdAudio(void);
	static void fsCrossCheck(void);
	static void sstFsCcTest(void);
	static void lowFlowFsCc(void);
	static void highFlowFsCc(void);
	static void circuitPressure(void);
	static void safetySystem(void);
	static void exhValveSeal(void);
	static void exhValve(void);
	static void exhValveCalib(void);
	static void smLeak(void);
	static void sstLeak(void);
	static void complianceCalib(void);
	static void exhHeater(void);
	static void generalElectronics(void);
	static void guiTouch(void);
	static void guiSerialPort(void);
	static void battery(void);
	static void restartBd(void);
	static void restartGui(void);
	static void bdSerialNumber(void);
	static void guiSerialNumber(void);
	static void calInfoDuplication(void);
	static void BdventInop(void);
	static void GuiventInop(void);
	static void bdInitDefaultSerialNumber(void);
	static void guiInitDefaultSerialNumber(void);
	static void fsCalibration(void);
	static void guiNurseCall(void);
	static void atmPressureTransducerCal(void);
	static void atmPressureTransducerRead(void);
	
	static void generateTestDataPacket(Int dataCount, Int dataIdx, Real32 dataValue=0, Boolean setExectData=FALSE);
	static void generateTestPromptPacket(SmPromptId::PromptId promptId,
										SmPromptId::KeysAllowedId keyAllowe);
	static void generateTestStatusPacket(SmStatusId::TestStatusId testStatus,
										SmStatusId::TestConditionId condition);
	static void generateFailedTestStatusPacket(SmStatusId::TestStatusId testStatus,
										SmStatusId::TestConditionId condition);
	static void generateSmLeakDataPacket(Int dataCount, Int dataIdx);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:

private:
	// these methods are purposely declared, but not implemented...
	FakeServiceModeManager(const FakeServiceModeManager&);	// not implemented...
	void operator=(const FakeServiceModeManager&);			// not implemented...


	//@ Type: SetTestStatus_
	// This enum lists the possible returned status for each test.
	enum SetTestStatus_
	{
		START_TEST=0
	};

	//@ Data-Member: counter
	// This variable provides means to control which test branch to execute.
	static int counter;
	
	//@ Data-Member: setExactData
	// The flag provides means to send a exact data value.
	static Boolean setExactData_;
	
	//@ Data-Member: MaxPromptSequence_
	// This maximum number of prompt needed for each of the service test.
	static Int MaxPromptSequence_[SmTestId::NUM_SERVICE_TEST_ID];

	//@ Data-Member: CurrentTestId_
	// The current service test id.
	static SmTestId::ServiceModeTestId CurrentTestId_;

	//@ Data-Member: RepeatTestRequested_
	// The flag indicates if the test is a repeated or new test.
	static Boolean RepeatTestRequested_;

	//@ Data-Member: CurrentPromptSequence_
	// The current prompt being execured by the service test id.
	static Int CurrentPromptSequence_;

	//@ Data-Member: CurrentPromptSequence_
	// The current prompt being execured by the service test id.
	static SmPromptId::ActionId UserRequestToStopTest_;
	
	//@ Data-Member: PTestFunction_
	// The current prompt being execured by the service test id.
	static void (*PTestFunction_[SmTestId::NUM_SERVICE_TEST_ID])(void);

	//@ Data-Member: isGuiTest_
	// The current prompt being execured by the service test id.
	static Boolean isGuiTest_[SmTestId::NUM_SERVICE_TEST_ID];
	
	//@ Data-Member: setTestSteps
	// The current data being execured by the service test id.
	static Int setTestSteps;

	//@ Data-Member: testTimer_
	// 
	static FakeSMControlTimer &testTimer_;

#if defined SIGMA_UNIT_TEST
	static SmStatusId::TestStatusId TestStatusId_;
	static SmStatusId::TestConditionId TestConditionId_;
#endif	// SIGMA_UNIT_TEST

};


#endif // FakeServiceModeManager_HH 


#endif	// FAKE_SM
