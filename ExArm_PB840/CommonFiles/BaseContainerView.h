#pragma once

#include "TouchDriver.hh"
// BaseContainerView view

class BaseContainerView : public CView
{
	DECLARE_DYNCREATE(BaseContainerView)
private:
	TouchDriver touchDriver_;

public:
	void CreateLowerBaseViewCDC();
	virtual LONG GetOriginY();

public:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view

protected:
	BaseContainerView();           // protected constructor used by dynamic creation
	virtual ~BaseContainerView();


protected:
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	DECLARE_MESSAGE_MAP()
public:
};


