
#ifndef PatientDataUnitTestIds_HH
#define PatientDataUnitTestIds_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Filename:  PatientDataUnitTestIds - Ids of all of the Patient-Data 
//  		  unit test Classes.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Patient-Data/vcssrc/PatientDataUnitTestIds.hhv   25.0.4.0   19 Nov 2013 14:18:00   pvcs  $
//
//@ Modification-Log
//
//  Revision: 000  By:  jhv    Date:  03-13-95    DR Number:
//	Project:  Sigma (R8027)
//	Description:
//		Initial version 
//  
//====================================================================


//@ Type:  PatientDataUnitTestId
// Ids for all of Patient Data Subsystem classes.
enum PatientDataUnitTestIds {
  PATIENT_DATA_UNITTEST,
  FAKE_BD_DATA,
  FAKE_END_OF_BD,
  FAKE_WAVEFORM_BD,
  FAKE_REALTIME_BD,
  FAKE_AVERAGED_BD,
  FAKE_PEEND_BD,
  NUM_PATIENT_UNIT_CLASSES
};


#endif // PatientDataUnitTestIds_HH 
