#include "stdafx.h"
////////////////Copyright (c) 2013 Covidien/////////////////////////////////////
/*******************************************************************************
Name:		Handlers.cpp

Purpose:
	Implement SWAT command handler classes.

REVISION HISTORY:

REVISION: 001	BY: SF		DATE: 19-AUG-2013
PROJECT: e600 vent
DR NUMBER: N / A
DESCRIPTION:
	Initialize version.

END OF REVISIONS

*******************************************************************************/

#include "Handlers.h"
#include "SWATAgent.h"

///////////////////////////////////////////////////////////////////////
CoordinateHandler::CoordinateHandler() : x_(-1), y_(-1)
{

}

void CoordinateHandler::ParseCommand(TiXmlElement* ele)
{
	// parse x
	TiXmlElement* child = NULL;
	child = ele->FirstChildElement("p1");
	if (!child)
	{
		Error( SWAT_COMMAND_SYNTAX_ERROR );
		return;
	}
	if (sscanf_s(child->GetText(), "%d", &this->x_) == 0)
    {
        Error( SWAT_COMMAND_SYNTAX_ERROR );
        return;
    }

	// parse y
	child = ele->FirstChildElement("p2");
	if (!child)
	{
		Error( SWAT_COMMAND_SYNTAX_ERROR );
		return;
	}
	if (sscanf_s(child->GetText(), "%d", &this->y_) == 0)
    {
        Error( SWAT_COMMAND_SYNTAX_ERROR );
        return;
    }
}


///////////////////////////////////////////////////////////////////////

void SimulateTouchHandler::EexecuteCommand()
{
	SWATStatus status = PSWATService_->SimulateTouch(this->x_, this->y_);

    Error( status );
}

///////////////////////////////////////////////////////////////////////
SimulateBezelKeyHandler::SimulateBezelKeyHandler()
    : keyId_(-1), onOff_(false)
{
}

void SimulateBezelKeyHandler::ParseCommand(TiXmlElement * ele)
{
	// parse onOff
	TiXmlElement* child = NULL;
	child = ele->FirstChildElement("on_off");
	if (!child)
	{
		Error( SWAT_COMMAND_SYNTAX_ERROR );
		return;
	}
	if (sscanf_s(child->GetText(), "%d", &this->onOff_) == 0)
    {
        Error( SWAT_COMMAND_SYNTAX_ERROR );
        return;
    }

	// parse bezel key id
	child = ele->FirstChildElement("key_id");
	if (!child)
	{
		Error( SWAT_COMMAND_SYNTAX_ERROR );
		return;
	}
	if (sscanf_s(child->GetText(), "%d", &this->keyId_) == 0)
    {
        Error( SWAT_COMMAND_SYNTAX_ERROR );
        return;
    }
}

void SimulateBezelKeyHandler::EexecuteCommand()
{
	SWATStatus status = PSWATService_->SimulateBezelKey(this->keyId_, this->onOff_);

    Error( status );
}


///////////////////////////////////////////////////////////////////////
SimulateKnobHandler::SimulateKnobHandler() : delta_(0)
{

}

void SimulateKnobHandler::ParseCommand(TiXmlElement * ele)
{
	// parse knob rotate delta
	TiXmlElement* child = NULL;
	child = ele->FirstChildElement("p1");
	if (!child)
	{
		Error( SWAT_COMMAND_SYNTAX_ERROR );
		return;
	}
	if (sscanf_s(child->GetText(), "%d", &this->delta_) == 0)
    {
        Error( SWAT_COMMAND_SYNTAX_ERROR );
        return;
    }
}

void SimulateKnobHandler::EexecuteCommand()
{
	SWATStatus status = PSWATService_->SimulateKnob( this->delta_ );

    Error( status );
}

////////////////////////////////////////////////////////////////////////////////
ScreenCaptureHandler::ScreenCaptureHandler() 
{
	memset( fileName_, 0, sizeof( fileName_ ) );
}

void ScreenCaptureHandler::ParseCommand(TiXmlElement * ele)
{
	TiXmlElement* child = NULL;
	child = ele->FirstChildElement("file_name");
	if (!child)
	{
		Error( SWAT_COMMAND_SYNTAX_ERROR );
		return;
	}
 
	strcpy(this->fileName_, child->GetText());
}

void ScreenCaptureHandler::EexecuteCommand()
{
	SWATStatus status = PSWATService_->CaptureScreen( fileName_ );

    Error( status );
}



///////////////////////////////////////////////////////////////////////
ScreenCaptureBufferInfoHandler::ScreenCaptureBufferInfoHandler()
    : screenQueueSize_(0)
{

}

void ScreenCaptureBufferInfoHandler::ParseCommand(TiXmlElement * ele)
{
	// no content to parse
}

void ScreenCaptureBufferInfoHandler::EexecuteCommand()
{
    SWATStatus status = PSWATService_->QueryScreenshotQueueSize( screenQueueSize_ );

    Error( status );
}

void ScreenCaptureBufferInfoHandler::BuildResponse()
{
    sprintf(resp_, "<screen_capture_buffer_info_resp>"
        "<buffer_size>%d</buffer_size>"
        "</screen_capture_buffer_info_resp>", this->screenQueueSize_);
}

///////////////////////////////////////////////////////////////////////
void ClearScreensHandler::ParseCommand(TiXmlElement * ele)
{
    // no content to parse
}

void ClearScreensHandler::EexecuteCommand()
{
    SWATStatus status = PSWATService_->ClearScreenshots();

    Error( status );
}


///////////////////////////////////////////////////////////////////////
QueryScreenshotFileNameHandler::QueryScreenshotFileNameHandler() 
    : index_(-1)
{
    memset(ScreenAddr_, 0, sizeof(ScreenAddr_));
}

void QueryScreenshotFileNameHandler::ParseCommand(TiXmlElement * ele)
{
    TiXmlElement* child = NULL;
    child = ele->FirstChildElement("p1");
    if (!child)
    {
        Error( SWAT_COMMAND_SYNTAX_ERROR );
        return;
    }
    if (sscanf_s(child->GetText(), "%d", &this->index_) == 0)
    {
        Error( SWAT_COMMAND_SYNTAX_ERROR );
        return;
    }
}

void QueryScreenshotFileNameHandler::EexecuteCommand()
{
    SWATStatus status = PSWATService_->QueryScreenshotPath( index_, ScreenAddr_ );

    Error( status );
}

void QueryScreenshotFileNameHandler::BuildResponse()
{
    sprintf(resp_, "<query_sc_ftp_addr_resp>"
        "<index>%d</index>"
        "<ftp_addr>%s</ftp_addr>"
        "</query_sc_ftp_addr_resp>", this->index_, this->ScreenAddr_);
}

