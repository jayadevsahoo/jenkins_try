//------------------------------------------------------------------------------
//                   Copyright (c) 2008 - 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
/// @file pmap.h
///
/// This file contains an fixed block allocator used with the std::map
/// types so that any dynamic storage requested by the STL library is obtained
/// from a fixed memory pool and not the global heap. The class pmap
/// is used in place of std::map. Use pmap exactly the same as
/// std::map except that the maximum string size is limited to the pool
///	size limit.
///
/// Fixed block allocators are used to eliminate dynamic storage allocation
/// heap fragmentation issues.
///
/// Example usage:
///
/// pair< pmap<int, pstring>::iterator, bool > ret;
/// pmap<int, pstring> testmap;
///	ret = testmap.insert(pair<int, pstring>(1, "test string"));
//------------------------------------------------------------------------------
#ifndef PMAP_ALLOCATOR_H
#define PMAP_ALLOCATOR_H

//------------------------------------------------------------------------------
// I N C L U D E S
//------------------------------------------------------------------------------
#include "singlepool_allocator.hh"
#include <map>

//------------------------------------------------------------------------------
// pmap class implementation inherits from std::map with the only difference being
// pmap uses block_allocator as the default allocator.
//------------------------------------------------------------------------------
template<class _Kty,
	class _Ty,
	class _Pr = std::less<_Kty>,
	class _Alloc = singlepool_allocator<std::pair<const _Kty, _Ty> > >
	class pmap
		: public std::map<_Kty, _Ty, _Pr, _Alloc>
	{
	};

//------------------------------------------------------------------------------
// pmultimap class implementation inherits from std::map with the only difference being
// pmultimap uses block_allocator as the default allocator.
//------------------------------------------------------------------------------
template<class _Kty,
	class _Ty,
	class _Pr = std::less<_Kty>,
	class _Alloc = singlepool_allocator<std::pair<const _Kty, _Ty> > >
	class pmultimap
		: public std::multimap<_Kty, _Ty, _Pr, _Alloc>
	{
	};

#endif // PMAP_ALLOCATOR_H

