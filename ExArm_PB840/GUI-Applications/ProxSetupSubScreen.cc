#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2010, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: ProxSetupSubScreen - Subscreen for activating or deactivating a
//                              Prox setup subscreen.
//---------------------------------------------------------------------
//@ Interface-Description
//
// The activate()/deactivate() methods are called automatically on
// display/removal of this subscreen by our subscreen area owner.  The
// BD event is passed into updatePanel_() main display engine to control 
// messages being displayed based on the various input. 
//---------------------------------------------------------------------
//@ Rationale
// Implements the complete functionality of the ProxSetupSubScreen
// in a single class.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and pre-conditions
// to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
// Only one instance of this class should be created.  It should only
// be displayed in LowerSubScreenArea.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/ProxSetupSubScreen.ccv   25.0.4.0   19 Nov 2013 14:08:18   pvcs  $
//
//@ Modification-Log 
// 
//  Revision: 007   By: rhj   Date: 10-Aug-2010     SCR Number: 6638
//  Project:  PROX
//  Description:
//      Added Prox unavailable message during NIV or Bilevel.     
//
//  Revision: 006   By: rhj   Date: 16-July-2010     SCR Number: 6601
//  Project:  PROX
//  Description:
//      Added Prox unavailable message when PROX SST was skipped or 
//      failed.
// 
//  Revision: 005   By: rpr   Date: 07-July-2010     SCR Number: 6590
//  Project:  PROX
//  Description:
//      Modified the elasped time for the manual purge in progress calculation  
//		based on 40 seconds.
// 
//  Revision: 004   By: mnr   Date: 24-May-2010     SCR Number: 6436
//  Project:  PROX
//  Description:
//      Added isProxInstalled() and isProxInstalled_.
//
//  Revision: 003   By: mnr   Date: 10-May-2010     SCR Number: 6436
//  Project:  PROX
//  Description:
//      Only log TREND EVENT for PROX_DISABLED if it is ENABLED.
//
//  Revision: 002   By: mnr   Date: 10-May-2010     SCR Number: 6436
//  Project:  PROX
//  Description:
//      If PROX SST did not PASS or is not OVERRIDDEN, disable PROX
//      and do not allow the user to change it.
// 
//  Revision: 001   By: rhj   Date:  26-Jan-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//       PROX Project Initial Version
//
//=====================================================================

#include "ProxSetupSubScreen.hh"

//@ Usage-Classes
#include "GuiApp.hh"
#include "PromptStrs.hh"
#include "PromptArea.hh"
#include "ButtonTarget.hh"
#include "BdEventRegistrar.hh"
#include "BdGuiEvent.hh"
#include "Sound.hh"
#include "MiscStrs.hh"
#include "SubScreenArea.hh"
#include "UpperSubScreenArea.hh"
#include "MessageArea.hh"
#include "WaveformsSubScreen.hh"
#include "TextUtil.hh"
#include "BoundedValue.hh"
#include "BreathDatumHandle.hh"
#include "BreathDatum.hh"
#include "LowerScreen.hh"
#include "TrendDataMgr.hh"
#include "TrendEvents.hh"
#include "TrendValue.hh"
#include "ProxEnabledValue.hh"
#include "ContextSubject.hh"
#include "GuiTimerRegistrar.hh"
#include "SettingContextHandle.hh"
#include "ProxEnabledValue.hh"
#include "AcceptedContext.hh"
#include "ContextMgr.hh"
#include "AdjustedContext.hh"
#include "NovRamManager.hh"
#include "SmTestId.hh"
#include "ProxEnabledSetting.hh"
//@ End-Usage
//@ Code...

// Initialize static constants.
static const Int32 PANEL_HEIGHT = LOWER_SUB_SCREEN_AREA_HEIGHT;
static const Int32 HALF_PANEL_WIDTH = LOWER_SUB_SCREEN_AREA_WIDTH / 2;
static Int32 TOP_PANEL_Y = 0;
static const Int32 LEFT_PANEL_X = 1;
static const Int32 RIGHT_PANEL_X = LEFT_PANEL_X + HALF_PANEL_WIDTH;

static const Int32 BUTTON_HEIGHT_ = 40;
static const Int32 BUTTON_WIDTH_ = 90;
static const Int32 PANEL_TITLE_Y_ = 8;

static const Int32 PANEL_MSG_Y_ = PANEL_TITLE_Y_ + 21;
static const Int32 PANEL_COMMAND_Y_ = PANEL_MSG_Y_ + 21;
static const Int32 BUTTON_X_ = 0;
static const Int32 BUTTON_Y_ = PANEL_COMMAND_Y_ + 21;
static const Int32 BUTTON_BORDER_ = 3;


static const Int32 PROX_BUTTON_HEIGHT_ = 46;
static const Int32 PROX_BUTTON_WIDTH_ = 152;
static const Int32 BAR_Y_ = PANEL_MSG_Y_+ 10;
static const Int32 BAR_HEIGHT_ = 15;
static const Int32 BAR_WIDTH_ = 122;


static Uint  ProxEnabledInfoMemory_[(sizeof(DiscreteSettingButton::ValueInfo) +
									 (sizeof(StringId) * (ProxEnabledValue::TOTAL_PROX_ENABLED_VALUES - 1)))];


static DiscreteSettingButton::ValueInfo*  PProxEnabledInfo_ =
	(DiscreteSettingButton::ValueInfo*)::ProxEnabledInfoMemory_;

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ProxSetupSubScreen()  [Constructor]
//
//@ Interface-Description
// Creates the ProxSetupSubScreen.  Passed a pointer to the subscreen
// area which creates it (LowerSubScreenArea). 
//---------------------------------------------------------------------
//@ Implementation-Description
// Sizes, positions and colors the subscreen container.
// Creates the various panels displayed in the subscreen and adds them
// to the container.
// $[PX07017]  Subscreen shall have a button to trigger a manual purge 
//             request to be sent to the Prox system.
// $[PX02001]  Subscreen shall have a Settings button to enable or 
//             disable the Prox system.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ProxSetupSubScreen::ProxSetupSubScreen(SubScreenArea *pSubScreenArea) :
BatchSettingsSubScreen(pSubScreenArea),
	titleArea_(NULL_STRING_ID, SubScreenTitleArea::SSTA_CENTER),
	startButton_( BUTTON_X_, BUTTON_Y_,
				  BUTTON_WIDTH_, BUTTON_HEIGHT_,
				  Button::LIGHT, BUTTON_BORDER_,
				  Button::ROUND, Button::NO_BORDER,
				  MiscStrs::PROX_START_MANUAL_BUTTON_TITLE,
				  ::NULL_STRING_ID,
				  ::GRAVITY_RIGHT,
				  Button::PB_TOGGLE),
	proxEnableButton_(
					 BUTTON_X_,
					 BUTTON_Y_,
					 PROX_BUTTON_WIDTH_,
					 PROX_BUTTON_HEIGHT_, Button::LIGHT, BUTTON_BORDER_, Button::ROUND, 
					 Button::NO_BORDER, 
					 MiscStrs::PROX_ENABLED_BUTTON_TITLE,
					 SettingId::PROX_ENABLED, this,
					 MiscStrs::PROX_ENABLED_HELP_MESSAGE, 18),

	controlPanel_(),
	settingsPanel_(),
	panelTitle_(::NULL_STRING_ID),
	timeProgressBox_(0, BAR_Y_ + 1, BAR_WIDTH_, BAR_HEIGHT_ - 2),
	timeBox_(0, BAR_Y_, BAR_WIDTH_, BAR_HEIGHT_),
	panelMsg_(::NULL_STRING_ID),
	errorMsg_(::NULL_STRING_ID),
    errorMsg2_(::NULL_STRING_ID),
	notAvailableMsg_(::NULL_STRING_ID),
	isProxInstalled_(FALSE)
{
	CALL_TRACE("ProxSetupSubScreen::ProxSetupSubScreen(SubScreenArea*)");

	setX(0);
	setY(0);
	setWidth(LOWER_SUB_SCREEN_AREA_WIDTH);
	setHeight(LOWER_SUB_SCREEN_AREA_HEIGHT);
	setFillColor(Colors::LIGHT_BLUE);

	addDrawable(&titleArea_);
	addDrawable(&controlPanel_);

	// Specify control panel's attributes.
	controlPanel_.setWidth(HALF_PANEL_WIDTH-1);
	controlPanel_.setHeight(PANEL_HEIGHT-titleArea_.getHeight());
	controlPanel_.setX(LEFT_PANEL_X);
	controlPanel_.setY(TOP_PANEL_Y+titleArea_.getHeight());
	controlPanel_.setFillColor(Colors::MEDIUM_BLUE);

	// Add the Panel Title
	controlPanel_.addDrawable(&panelTitle_);
	panelTitle_.setText(MiscStrs::PROX_MANUAL_TITLE);
	panelTitle_.positionInContainer(GRAVITY_CENTER);
	panelTitle_.setY(PANEL_TITLE_Y_);
	panelTitle_.setColor(Colors::WHITE);

	// Add the Panel status Message.
	controlPanel_.addDrawable(&panelMsg_);
	panelMsg_.setText(MiscStrs::PROX_MANUAL_PURGE_START_MSG);
	panelMsg_.positionInContainer(GRAVITY_CENTER);
	panelMsg_.setY(PANEL_MSG_Y_);
	panelMsg_.setColor(Colors::WHITE);

	controlPanel_.addDrawable(&startButton_);
	startButton_.positionInContainer(GRAVITY_CENTER);


	// Add the time box
	controlPanel_.addDrawable(&timeBox_);
	timeBox_.setColor(Colors::LIGHT_BLUE);
	timeBox_.setFillColor(Colors::LIGHT_BLUE);
	timeBox_.positionInContainer(GRAVITY_CENTER);
	timeBox_.setShow(FALSE);

	// Add the time progress box
	controlPanel_.addDrawable(&timeProgressBox_);
	timeProgressBox_.setColor(Colors::EXTRA_DARK_BLUE);
	timeProgressBox_.setFillColor(Colors::EXTRA_DARK_BLUE);
	timeProgressBox_.setX(timeBox_.getX() + 1);
	timeProgressBox_.setShow(FALSE);


	// Register for callbacks from start button
	startButton_.setButtonCallback(this);




	// Specify settings panel's attributes.
	TOP_PANEL_Y = titleArea_.getHeight();
	addDrawable(&settingsPanel_);
	settingsPanel_.setWidth(HALF_PANEL_WIDTH-1);
	settingsPanel_.setHeight(PANEL_HEIGHT-titleArea_.getHeight());
	settingsPanel_.setX(RIGHT_PANEL_X);
	settingsPanel_.setY(TOP_PANEL_Y);
	settingsPanel_.setFillColor(Colors::MEDIUM_BLUE);
	settingsPanel_.addDrawable(&proxEnableButton_);
	proxEnableButton_.positionInContainer(GRAVITY_CENTER);


	// Add the Panel Title
	settingsPanel_.addDrawable(&errorMsg_);
	errorMsg_.setText(MiscStrs::PROX_ERROR_MESSAGE_TITLE1);
	errorMsg_.positionInContainer(GRAVITY_CENTER);
	errorMsg_.setY(PANEL_TITLE_Y_);
	errorMsg_.setColor(Colors::WHITE);

	// Add the Panel status Message.
	settingsPanel_.addDrawable(&errorMsg2_);
	errorMsg2_.setText(MiscStrs::PROX_ERROR_MESSAGE_TITLE2);
	errorMsg2_.positionInContainer(GRAVITY_CENTER);
	errorMsg2_.setY(PANEL_MSG_Y_);
	errorMsg2_.setColor(Colors::WHITE);

	// Add the Panel not available Message.
	settingsPanel_.addDrawable(&notAvailableMsg_);
	notAvailableMsg_.setText(MiscStrs::PROX_NOT_AVAILABLE_MSG);
	notAvailableMsg_.positionInContainer(GRAVITY_CENTER);
	notAvailableMsg_.setY(PANEL_MSG_Y_);
	notAvailableMsg_.setColor(Colors::WHITE);

	notAvailableMsg_.setShow(FALSE);

	// set up values for Prox Disable/Enable....
	::PProxEnabledInfo_->numValues = ProxEnabledValue::TOTAL_PROX_ENABLED_VALUES;
	::PProxEnabledInfo_->arrValues[ProxEnabledValue::PROX_DISABLED] =
		MiscStrs::PROX_DISABLED_VALUE;
	::PProxEnabledInfo_->arrValues[ProxEnabledValue::PROX_ENABLED] =
		MiscStrs::PROX_ENABLE_VALUE;
	proxEnableButton_.setValueInfo(::PProxEnabledInfo_);
	proxEnableButton_.setShowViewable(TRUE);


	// Register for Event Data changes for the manual purge.
	BdEventRegistrar::RegisterTarget(EventData::PROX_FAULT, this);
	// Register for PROX INSTALLED Event Data 
	BdEventRegistrar::RegisterTarget(EventData::PROX_INSTALLED, this);

	// Register for PROX_MANUAL_PURGE_TIMEOUT and PROX_MANUAL_PURGE_PROGRESS_TIMER_TICK.
	GuiTimerRegistrar::RegisterTarget(GuiTimerId::PROX_MANUAL_PURGE_PROGRESS_TIMER_TICK, this);
	GuiTimerRegistrar::RegisterTarget(GuiTimerId::PROX_MANUAL_PURGE_TIMEOUT, this);

	isProxInop_ = FALSE;
	isProxManualPurgeActive_ = FALSE;
	isProxEnabled_ = FALSE;


	ResultEntryData resultData;
	NovRamManager::GetSstResultEntries(sstResTableEntries_);

	sstResTableEntries_[SmTestId::SST_TEST_PROX_ID].getResultData(resultData);
	TestResultId testResult = resultData.getResultId();

    // Only allow the user to enable prox if 
	// SST result is either a pass or overridden.
	if (testResult == OVERRIDDEN_TEST_ID || PASSED_TEST_ID == testResult)
	{
		isProxSstPassedOverridden_  = TRUE;
		errorMsg_.setShow(FALSE);
		errorMsg2_.setShow(FALSE);

	}
	else
	{
		isProxSstPassedOverridden_ = FALSE;
		errorMsg_.setShow(TRUE);
		errorMsg2_.setShow(TRUE);

		ProxEnabledSetting * rProxEnabledSetting = (ProxEnabledSetting *)  SettingsMgr::GetDiscreteSettingPtr(SettingId::PROX_ENABLED);
		rProxEnabledSetting->disableProx();

		// Set the prox setting to disabled.
		AdjustedContext&  rAdjContext = ContextMgr::GetAdjustedContext();
		rAdjContext.setDiscreteValue(SettingId::PROX_ENABLED, ProxEnabledValue::PROX_DISABLED);
		SettingContextHandle::AcceptBatch();

		// [PX04003] Post related trending event 
		TrendDataMgr::PostEvent(TrendEvents::AUTO_EVENT_PROX_DISABLED);
	}

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~ProxSetupSubScreen  [Destructor]
//
//@ Interface-Description
// Destroys ProxSetupSubScreen.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ProxSetupSubScreen::~ProxSetupSubScreen(void)	
{
	CALL_TRACE("ProxSetupSubScreen::~ProxSetupSubScreen(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activateHappened_
//
//@ Interface-Description
// Called by our subscreen area before this subscreen is to be displayed
// allowing us to do any necessary setup. Needs no parameters.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Set a default panel message and update the prompt area during this 
//  subscreen's activation.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	ProxSetupSubScreen::activateHappened_(void)
{
	CALL_TRACE("ProxSetupSubScreen::activateHappened_(void)");

	// Begin adjusting settings
	SettingContextHandle::AdjustBatch();

	titleArea_.setTitle(MiscStrs::CURRENT_PROX_SETUP_SUBSCREEN_TITLE);

	// Activate prox enable button
	proxEnableButton_.activate();

	// Set a default panel message and position.
	panelMsg_.setText(MiscStrs::PROX_MANUAL_PURGE_START_MSG);
	panelMsg_.positionInContainer(GRAVITY_CENTER);

	panelTitle_.setText(MiscStrs::PROX_MANUAL_TITLE);
	panelTitle_.positionInContainer(GRAVITY_CENTER);

	// [4-070139-00 section 37] If PROX SST did not pass, disable PROX and do not allow 
	//                          the user to change it.
	if (!isProxSstPassedOverridden_ || !isProxInstalled_)
	{
		proxEnableButton_.setToFlat();
		startButton_.setToFlat();

		GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY,
							PromptArea::PA_LOW, NULL_STRING_ID);
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_ADVISORY,
							PromptArea::PA_LOW, NULL_STRING_ID);
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
							PromptArea::PA_LOW, NULL_STRING_ID);

		return;
	}

	const ContextSubject *const  P_ACCEPTED_SUBJECT =
		getSubjectPtr_(ContextId::ACCEPTED_CONTEXT_ID);

	const DiscreteValue PROX_ENABLED_VALUE =
		P_ACCEPTED_SUBJECT->getSettingValue(SettingId::PROX_ENABLED);

	isProxEnabled_ = (PROX_ENABLED_VALUE == ProxEnabledValue::PROX_ENABLED);

	// Update the Prompt Area
	if (isProxEnabled_)
	{
		// Set Primary prompt to "Touch START button to begin manual purge.
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY,
									   PromptArea::PA_LOW, PromptStrs::PROX_START_ADVISORY_P);

	}
	else
	{
		// Set Primary prompt to "Adjust settings as needed."
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY,
									   PromptArea::PA_LOW, PromptStrs::ADJUST_SETTINGS_P);
	}


	Boolean isProxManualPurgeCanceled = FALSE;

	Applicability::Id  PROX_ENABLED_APPLIC =
		SettingContextHandle::GetSettingApplicability(
													 ContextId::ACCEPTED_CONTEXT_ID,
													 SettingId::PROX_ENABLED
													 );
	if (PROX_ENABLED_APPLIC == Applicability::CHANGEABLE)
	{
		// Only allow the user to access prox manual purge if
		// prox is enabled and there are no prox faults 
		// reported by Breath Delivery.
		if (isProxEnabled_ && !isProxInop_)
		{

			// During a prox manual purge, 
			// show the time progress bar, flat the start 
			// and prox enabled button and display the current
			// status.  Otherwise enable the start and prox 
			// buttons and hide the time progress bar.
			if (isProxManualPurgeActive_)
			{
				startButton_.setToFlat();
				proxEnableButton_.setToFlat();
				timeBox_.setShow(TRUE);
				timeProgressBox_.setShow(TRUE);
				panelMsg_.setShow(FALSE);
				panelTitle_.setText(MiscStrs::PROX_MANUAL_PROGRESS_TITLE);          
				panelTitle_.positionInContainer(GRAVITY_CENTER);
			}
			else
			{
				startButton_.setToUp();
				proxEnableButton_.setToUp();
				timeBox_.setShow(FALSE);
				timeProgressBox_.setShow(FALSE);
				panelMsg_.setShow(TRUE);
				GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
											   PromptArea::PA_LOW, PromptStrs::OTHER_SCREENS_CANCEL_S);
			}
		}
		else
		{
			isProxManualPurgeCanceled = TRUE;
		}

		notAvailableMsg_.setShow(FALSE);
	}
	else
	{
		isProxManualPurgeCanceled = TRUE;
        notAvailableMsg_.setShow(TRUE);
	}

	// When prox manual purge is canceled due
	// to an invalid setting, cancel all timers,
	// force the start button to be flattened,
	// and do not display the progress bar.
	if (isProxManualPurgeCanceled)
	{

		if (GuiTimerRegistrar::isActive(GuiTimerId::PROX_MANUAL_PURGE_TIMEOUT))
		{
			GuiTimerRegistrar::CancelTimer(GuiTimerId::PROX_MANUAL_PURGE_TIMEOUT);

		}

		if (GuiTimerRegistrar::isActive(GuiTimerId::PROX_MANUAL_PURGE_PROGRESS_TIMER_TICK))
		{
			GuiTimerRegistrar::CancelTimer(GuiTimerId::PROX_MANUAL_PURGE_PROGRESS_TIMER_TICK);
		}

		isProxManualPurgeActive_ = FALSE;
		startButton_.setToFlat();
		timeBox_.setShow(FALSE);
		timeProgressBox_.setShow(FALSE);
		panelMsg_.setShow(TRUE);
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: deactivateHappened_
//
//@ Interface-Description
// Called by our subscreen area just after this subscreen is removed from
// the display allowing us to do any necessary cleanup. Needs no parameters.
//---------------------------------------------------------------------
//@ Implementation-Description
// Clears any prompts which may have been displayed. 
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	ProxSetupSubScreen::deactivateHappened_(void)
{
	CALL_TRACE("ProxSetupSubScreen::deactivateHappened_(void)");

	// Clear the Primary, Advisory and Secondary prompts
	GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY, PromptArea::PA_LOW,
								   NULL_STRING_ID);
	GuiApp::PPromptArea->setPrompt(PromptArea::PA_ADVISORY, PromptArea::PA_LOW,
								   NULL_STRING_ID);
	GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
								   PromptArea::PA_LOW, NULL_STRING_ID);
	proxEnableButton_.deactivate();

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: acceptHappened
//
//@ Interface-Description
// Called by the Proceed button when the operator presses the Accept key
// when the Proceed button is pressed down.  We accept the settings and
// display this screen with the new settings displayed as the current
// settings.
//---------------------------------------------------------------------
//@ Implementation-Description
// Make the "Accept" sound, clear the Adjust Panel focus, accept the
// adjusted settings, deactivate this subscreen and inform the Lower
// screen which Apnea settings, if any, were corrected as a result
// of the user's settings changes.
//
// $[01056] When batch settings are being setup ventilator settings
//			will not be affected
// $[01118] User must select Proceed and press Accept key to accept settings.
// $[01257] The Accept key shall be the ultimate confirmation step for all ...
// $[01258] The GUI shall accept all adjusted settings.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
	ProxSetupSubScreen::acceptHappened(void)
{
	CALL_TRACE("MoreSettingsSubScreen::acceptHappened(void)");

	if (areSettingsCurrent_())
	{													// $[TI1.1]
		Sound::Start(Sound::INVALID_ENTRY);
	}
	else
	{													// $[TI1.2]
		// Clear the Adjust Panel focus, make the Accept sound, store the
		// current settings, accept the adjusted settings and deactivate
		// this subscreen.
		Sound::Start(Sound::ACCEPT);

		AdjustPanel::TakeNonPersistentFocus(NULL, TRUE);

		ApneaCorrectionStatus apneaCorrectionStatus;
		apneaCorrectionStatus = SettingContextHandle::AcceptBatch();
		deactivate();
		activate();			// Reactive this subscreen

		if (!isProxInop_)
		{
			if (isProxEnabled_)
			{
				// [PX04003] Post related trending event 
				TrendDataMgr::PostEvent(TrendEvents::AUTO_EVENT_PROX_ENABLED);
			}
			else
			{
				// [PX04003] Post related trending event 
				TrendDataMgr::PostEvent(TrendEvents::AUTO_EVENT_PROX_DISABLED);
			}
		}

		//
		// The settings subsystem may have auto-adjusted some apnea
		// parameters upon acceptance of the settings above.  Pass the
		// correction status to the Lower Screen so that it can decide
		// whether or not to auto-launch the apnea setup screen.
		//
		LowerScreen::RLowerScreen.reviewApneaSetup(apneaCorrectionStatus);
	}
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: buttonDownHappened
//
//@ Interface-Description
// Handles button down events for all of the "instant action" screen
// select buttons on this subscreen. The `pButton' parameter is the button 
// that was pressed down.  The `isByOperatorAction' flag is TRUE if the 
// given button was pressed by operator.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//@ PreCondition
// The isByOperatorAction must be set to TRUE and the pButton cannot be NULL.
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
	ProxSetupSubScreen::buttonDownHappened(Button *pButton, Boolean byOperatorAction)
{
	CALL_TRACE("ProxSetupSubScreen::buttonDownHappened(Button *pButton, Boolean byOperatorAction)");

	CLASS_PRE_CONDITION(pButton != NULL);

	if ((pButton == &startButton_) && byOperatorAction)
	{
		// Notify Breath-Delivery of a manual purge request
		BdGuiEvent::RequestUserEvent(EventData::MANUAL_PURGE, EventData::START);
		proxEnableButton_.setToUp();
		AdjustPanel::TakeNonPersistentFocus(NULL);// Release focus      
		proxEnableButton_.setToFlat();
		startButton_.setToFlat();
		panelTitle_.setText(MiscStrs::PROX_MANUAL_PROGRESS_TITLE);
		panelTitle_.positionInContainer(GRAVITY_CENTER);
		timeProgressBox_.setWidth(1);
		timeBox_.setShow(TRUE);
		timeProgressBox_.setShow(TRUE);
		panelMsg_.setShow(FALSE);

		// Inform TimerRegistrar of event
		GuiTimerRegistrar::StartTimer(GuiTimerId::PROX_MANUAL_PURGE_PROGRESS_TIMER_TICK);
		GuiTimerRegistrar::StartTimer(GuiTimerId::PROX_MANUAL_PURGE_TIMEOUT);
		startTime_.now();
		isProxManualPurgeActive_ = TRUE;
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
									   PromptArea::PA_LOW, NULL_STRING_ID);
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: buttonUpHappened
//
//@ Interface-Description
// Called when the start button is set to up. 
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	ProxSetupSubScreen::buttonUpHappened(Button *pButton, Boolean byOperatorAction)
{
	CALL_TRACE("ProxSetupSubScreen::buttonUpHappened(Button *pButton, Boolean byOperatorAction)");

	// Check if the event is operator initiated
	if (pButton == &startButton_)
	{
		// Clear the help message
		GuiApp::PMessageArea->clearMessage(MessageArea::MA_HIGH);
	}
	// do nothing with the other buttons
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: bdEventHappened
//
//@ Interface-Description
//  Called when Breath-Delivery notifies us of a PROX_FAULT event.
//---------------------------------------------------------------------
//@ Implementation-Description
//  When PROX_FAULT event is active, cancel all active prox timers 
//  and reactivate the subscreen.  
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
	ProxSetupSubScreen::bdEventHappened(EventData::EventId eventId,
										EventData::EventStatus eventStatus,
										EventData::EventPrompt eventPrompt)
{
	CALL_TRACE("ProxSetupSubScreen::bdEventHappened(EventData::EventId "
			   ", EventData::EventStatus eventStatus)");

	if (EventData::PROX_FAULT == eventId)
	{

		// When breath delivery send a prox fault message with an active flag, 
		// cancel all prox timers and reactivate the subscreen.
		if (eventStatus == EventData::ACTIVE)
		{
			isProxInop_ = TRUE;

			if (GuiTimerRegistrar::isActive(GuiTimerId::PROX_MANUAL_PURGE_TIMEOUT))
			{
				GuiTimerRegistrar::CancelTimer(GuiTimerId::PROX_MANUAL_PURGE_TIMEOUT);

			}

			if (GuiTimerRegistrar::isActive(GuiTimerId::PROX_MANUAL_PURGE_PROGRESS_TIMER_TICK))
			{
				GuiTimerRegistrar::CancelTimer(GuiTimerId::PROX_MANUAL_PURGE_PROGRESS_TIMER_TICK);
			}

			isProxManualPurgeActive_ = FALSE;
		}
		else
		{
			isProxInop_ = FALSE;
		}


		if (isVisible())
		{
			deactivate();
			activate();
		}

	}
	else if (EventData::PROX_INSTALLED == eventId)
	{
		if (eventStatus == EventData::CANCEL)
		{
			ProxEnabledSetting * rProxEnabledSetting = (ProxEnabledSetting *)  SettingsMgr::GetDiscreteSettingPtr(SettingId::PROX_ENABLED);
			rProxEnabledSetting->disableProx();


			isProxInstalled_ = FALSE;
			AcceptedContext&  rAccContext = ContextMgr::GetAcceptedContext();

			const DiscreteValue PROX_ENABLED_VALUE = rAccContext.getBatchDiscreteValue(
																					  SettingId::PROX_ENABLED);  

			if (PROX_ENABLED_VALUE == ProxEnabledValue::PROX_ENABLED)
			{

				// Set the prox setting to disabled.
				AdjustedContext&  rAdjContext = ContextMgr::GetAdjustedContext();

				rAdjContext.setDiscreteValue(SettingId::PROX_ENABLED, ProxEnabledValue::PROX_DISABLED);

				SettingContextHandle::AcceptBatch();
				// [PX04003] Post related trending event 
				TrendDataMgr::PostEvent(TrendEvents::AUTO_EVENT_PROX_DISABLED);


			}
		}
		else if (eventStatus == EventData::ACTIVE)
		{
			isProxInstalled_ = TRUE;
		}

	}

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isProxInstalled
//
//@ Interface-Description
//  Returns TRUE if PROX board is installed, FALSE otherwise.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Accessor method for isProxInstalled_.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
Boolean
	ProxSetupSubScreen::isProxInstalled(void)
{
	return isProxInstalled_;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: timerEventHappened
//
//@ Interface-Description
// Called when the PROX_MANUAL_PURGE_TIMEOUT, PROX_MANUAL_PURGE_PROGRESS_TIMER_TICK,
// or SUBSCREEN_SETTING_CHANGE_TIMEOUT timer runs out.
//---------------------------------------------------------------------
//@ Implementation-Description
// When PROX_MANUAL_PURGE_TIMEOUT timer runs out, cancel the 
// PROX_MANUAL_PURGE_PROGRESS_TIMER_TICK timer, and reactivate the 
// subscreen.  When PROX_MANUAL_PURGE_PROGRESS_TIMER_TICK runs out,
// update the progress bar if this subscreen is visible.
// 
//---------------------------------------------------------------------
//@ PreCondition
// The timer id must be the subscreen setting change timout.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
	ProxSetupSubScreen::timerEventHappened(GuiTimerId::GuiTimerIdType timerId)
{
	CALL_TRACE("ProxSetupSubScreen::timerEventHappened(GuiTimerId::GuiTimerIdType)");

	switch (timerId)
	{
	case GuiTimerId::PROX_MANUAL_PURGE_TIMEOUT:


		if (GuiTimerRegistrar::isActive(GuiTimerId::PROX_MANUAL_PURGE_PROGRESS_TIMER_TICK))
		{
			GuiTimerRegistrar::CancelTimer(GuiTimerId::PROX_MANUAL_PURGE_PROGRESS_TIMER_TICK);
		}

		isProxManualPurgeActive_ = FALSE;
		Sound::Start(Sound::CONTACT);

		if (isVisible())
		{
			deactivate();
			activate();
		}

		break;

	case GuiTimerId::PROX_MANUAL_PURGE_PROGRESS_TIMER_TICK:

		// If timer is already canceled, this event callback might still be called.
		// To be sure, we prevent the OffKey subscreen from being redrawn here.
		if (GuiTimerRegistrar::isActive(GuiTimerId::PROX_MANUAL_PURGE_PROGRESS_TIMER_TICK))
		{
			// based on 40 second timer
			Int32 elapseTime = (startTime_.getPassedTime()/1000) * 3;
			if (elapseTime == 0)
			{
				elapseTime = 1;
			}

			timeProgressBox_.setWidth(elapseTime);

			// Refresh the time box.
			if (isVisible())
			{
				timeBox_.setShow(TRUE);
				timeProgressBox_.setShow(TRUE);
			}

		}
		break;

	case GuiTimerId::SUBSCREEN_SETTING_CHANGE_TIMEOUT:
		// Check if the subscreen is visible
		if (isVisible())
		{													// $[TI1]
			// Clear the Adjust Panel focus
			AdjustPanel::TakeNonPersistentFocus(NULL);

			// Reactivate this subscreen.
			deactivate();
			activate();
		}
		break;

	default:
		AUX_CLASS_ASSERTION(FALSE, timerId);    
		break;
	}
}




//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//        [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition   
//  none
//@ End-Method
//=====================================================================

void
	ProxSetupSubScreen::SoftFault(const SoftFaultID  softFaultID,
								  const Uint32       lineNumber,
								  const char*        pFileName,
								  const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS,
							PROXSETUPSUBSCREEN, lineNumber, pFileName, pPredicate);
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  valueUpdateHappened_
//
//@ Interface-Description
//  Called when a settings value is updated.  Passed in a transition id,
//  a qualifier id, and pointer to the ContextSubject.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Change the screen title and the secondary prompt according to whether
//  change is from the current value to the proposed value or from the 
//  proprosed value back to the original value. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
	ProxSetupSubScreen::valueUpdateHappened_(
											const TransitionId_                 transitionId,
											const Notification::ChangeQualifier qualifierId,
											const ContextSubject*               pSubject
											)
{
	CALL_TRACE("valueUpdateHappened_(transitionId, qualifierId, pSubject)");


	if (transitionId == BatchSettingsSubScreen::CURRENT_TO_PROPOSED)
	{
		// Display proposed title
		titleArea_.setTitle(MiscStrs::PROPOSED_PROX_SETUP_SUBSCREEN_TITLE);

		// Change the Secondary prompt to explain the use of the Proceed
		// button
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
									   PromptArea::PA_LOW, PromptStrs::OTHER_SCREENS_PROCEED_CANCEL_S);
		startButton_.setToFlat();

	}
	else if (transitionId == BatchSettingsSubScreen::PROPOSED_TO_CURRENT)
	{
		// Display current title
		titleArea_.setTitle(MiscStrs::CURRENT_PROX_SETUP_SUBSCREEN_TITLE);

		// Reset the Secondary prompt
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
									   PromptArea::PA_LOW, PromptStrs::OTHER_SCREENS_CANCEL_S);
		if (isProxEnabled_ && !isProxInop_)
		{
			startButton_.setToUp();
		}

	}
}  



