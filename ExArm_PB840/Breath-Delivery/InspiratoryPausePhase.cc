#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: InspiratoryPausePhase -  Implements an Inspiratory Pause Phase.
//---------------------------------------------------------------------
//@ Interface-Description
//		This class is derived from the base class BreathPhase.  Virtual
//      methods are implemented to do initialization before inspiratory pause
//		can begin, to execute the inspiratory pause every BD cycle, to wrap up
//      the pause phase to be executed at the end of inspiratory phase.
//---------------------------------------------------------------------
//@ Rationale
//		This class implements inspiratory pause phase.
//---------------------------------------------------------------------
//@ Implementation-Description
//      The exhalation valve is closed during this phase and the psols are
//		commanded closed.
//---------------------------------------------------------------------
//@ Fault-Handling
//		N/A
//---------------------------------------------------------------------
//@ Restrictions
//		Only limited to one instantiation.
//---------------------------------------------------------------------
//@ Invariants
//		none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/InspiratoryPausePhase.ccv   25.0.4.0   19 Nov 2013 13:59:50   pvcs  $
//
//@ Modification-Log
//
//  Revision: 008  By: syw     Date:  09-Oct-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//		VTPC project-related changes:
//		Handle reporting begin and end pause pressures during VTPC.
//
//  Revision: 007  By: yyy     Date:  6-Jul-1999    DR Number: 5463
//  Project:  ATC
//  Description:
//      Lenghthen the Apnea interval by 100 ms to avoid apnea from
//		triggering when pause starts.
//
//  Revision: 006  By: yyy     Date:  26-May-1999    DR Number: 5393
//  Project:  ATC
//  Description:
//      Fixed false circuit disconnects due to desired flow not being
//		reset during inspiratoryPausePhase.
//
//  Revision: 005  By: yyy     Date:  29-Apr-1999    DR Number: 5367
//  Project:  ATC
//  Description:
//      Use same pause trigger for both inspiration and expiration
//		pause handles.
//
//  Revision: 004  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 003  By:  iv     Date:  13-Oct-1998    DR Number: DCS 5207,5206
//       Project:  BiLevel
//       Description:
//		 	Changed the constant MAX_INSP_PAUSE_INTERVAL to 7000,
//          Mapped req. BL04014.
//
//  Revision: 002  By:  syw    Date:  07-Jul-1998    DR Number: DCS 5158
//       Project:  Sigma (840)
//       Description:
//		 	Interface with Maneuver class.
//
//  Revision: 001  By:  syw    Date:  08-Dec-1997    DR Number: none
//       Project:  Sigma (840)
//       Description:
//		 	BiLevel initial version.
//
//=====================================================================

#include "InspiratoryPausePhase.hh"

#include "MainSensorRefs.hh"
#include "TriggersRefs.hh"
//#include "ModeTriggerRefs.hh"
#include "ValveRefs.hh"
#include "BreathMiscRefs.hh"

//@ Usage-Classes

#include "PhasedInContextHandle.hh"
#include "ApneaInterval.hh"
#include "PressureSensor.hh"
#include "Trigger.hh"
#include "TimerBreathTrigger.hh"
#include "ExhValveIController.hh"
#include "ControllersRefs.hh"
#include "FlowController.hh"
#include "HighCircuitPressureExpTrigger.hh"
#include "BreathRecord.hh"
#include "PausePressTrigger.hh"
#include "InspPauseManeuver.hh"
#include "ManeuverRefs.hh"
#include "DisconnectTrigger.hh"
#include "ModeTriggerRefs.hh"
#include "VolumeTargetedManager.hh"
#include "ManeuverTimes.hh"

//@ End-Usage

//@ Constant: MAX_INSP_PAUSE_INTERVAL
// max time for inspiratory pause in msec
#ifdef E600_840_TEMP_REMOVED
extern const Int32 MAX_INSP_PAUSE_INTERVAL ;
const Int32 MAX_INSP_PAUSE_INTERVAL = 7000 ;
#endif

//@ Constant: PAUSE_INTERVAL_EXTENSION
// pause extension in msec
extern const Int32 PAUSE_INTERVAL_EXTENSION ;

//@ Constant: PRESSURE_TO_SEAL
// pressure above pat press for exhalation valve to seal
extern const Real32 PRESSURE_TO_SEAL ;

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: InspiratoryPausePhase()
//
//@ Interface-Description
//		Constructor.  This method has no arguments and returns nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The base class constructor is called with the phase type argument.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

InspiratoryPausePhase::InspiratoryPausePhase(BreathPhaseType::PhaseType phaseType)
 : BreathPhase(phaseType) 	   	// $[TI1]
{
	CALL_TRACE("InspiratoryPausePhase::InspiratoryPausePhase( void)") ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~InspiratoryPausePhase()
//
//@ Interface-Description
//      Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//      none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

InspiratoryPausePhase::~InspiratoryPausePhase(void)
{
	CALL_TRACE("InspiratoryPausePhase::~InspiratoryPausePhase(void)") ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: relinquishControl()
//
//@ Interface-Description
//      This method has a BreathTrigger& as an argument and has no return
//      value.  This method is called at the end of the inspiratory pause
//      phase to wrap up the phase.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Readjust RApneaInterval timer to the inspiratory pause duration.
//      Calculate the lastValidEipForCc - last valid End of Inspiratory
//      pressure for compliance compensation and set this value to VcvPhase.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
void
InspiratoryPausePhase::relinquishControl( const BreathTrigger& trigger)
{
   	CALL_TRACE("relinquishControl(BreathTrigger& trigger)");

    RApneaInterval.extendInterval( elapsedTimeMs_ - MAX_INSP_PAUSE_INTERVAL);

	Trigger::TriggerId trigId = trigger.getId() ;

	if (trigId != Trigger::PAUSE_COMPLETE)
	{
	   	// $[TI1]
		Maneuver::CancelManeuver();
	}  	// implied else $[TI2]

	// $[VC24017]
	// $[VC24020]
	RVolumeTargetedManager.setEndPlateauPress( RExhPressureSensor.getValue()) ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newCycle()
//
//@ Interface-Description
//      This method has no arguments and has no return value.  This method is
//      called every BD cycle during an inspiratory pause to control the
//      exhalation valve and PSOLS to perform an inspiratory pause.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The Psols are closed.  The exhalation valve is commanded to patient
//		pressure plus PRESSURE_TO_SEAL with a maximum of HCP.
// 		$[BL04010]
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
void
InspiratoryPausePhase::newCycle( void)
{
    CALL_TRACE("newCycle(void)");

   	// $[TI1]

	updateFlowControllerFlags_() ;

	RDisconnectTrigger.setDesiredFlow(0.0);
    RAirFlowController.updatePsol( 0.0F) ;
   	RO2FlowController.updatePsol( 0.0F) ;

	Real32 highCircuitPressure = PhasedInContextHandle::GetBoundedValue( SettingId::HIGH_CCT_PRESS).value ;

	Real32	targetPressure = MIN_VALUE(
				PRESSURE_TO_SEAL + RExhPressureSensor.getFilteredValue(),
				highCircuitPressure) ;

   	RExhValveIController.updateExhalationValve( targetPressure, 0.0F) ;

	elapsedTimeMs_ += CYCLE_TIME_MS ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newBreath()
//
//@ Interface-Description
//      This method has no arguments and has no return value.  This method is
//      called at the beginning of the inspiratory pause to initialize the
//      phase.
//---------------------------------------------------------------------
//@ Implementation-Description
//		$[BL04014]
//		The apnea interval is extended to the maximum inspiratory pause interval
//		(to avoid triggering apnea).  Maximum interval is used because
//		we do not know how long the pause will actually last.  The pause
//		timer is also set to the maximum and the timeout trigger is enabled.
//		The ExhValveIController and Psols are initalized via newBreath() method
//		calls.
// 		$[BL04011]
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
void
InspiratoryPausePhase::newBreath(void)
{
    CALL_TRACE("newBreath(void)");

	// $[VC24020] The begin pause pressure is recorded when the pause begins
	//	in VcvPhase class for VCP.  No need to record begin pause pressure for PC
	//  since it is not used.

    RApneaInterval.extendInterval(MAX_INSP_PAUSE_INTERVAL + PAUSE_INTERVAL_EXTENSION);

    RExhValveIController.newBreath() ;
    RAirFlowController.newBreath() ;
   	RO2FlowController.newBreath() ;

    RPausePressTrigger.enable();
    RHighCircuitPressureExpTrigger.enable();
	elapsedTimeMs_ = 0 ;
	BreathRecord::SetFirstCycleOfPlateauFlag() ;

   	if (RInspPauseManeuver.getManeuverState() == PauseTypes::PENDING)
   	{
	   	// $[TI1]
		RInspPauseManeuver.activate();
   	}
   	else
   	{
		// $[TI2]
		AUX_CLASS_PRE_CONDITION( RInspPauseManeuver.getManeuverState() == PauseTypes::ACTIVE,
					RInspPauseManeuver.getManeuverState()) ;
  	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
InspiratoryPausePhase::SoftFault( const SoftFaultID  softFaultID,
                   				 const Uint32       lineNumber,
		   						 const char*        pFileName,
		  						 const char*        pPredicate)
{
  	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)") ;
  	FaultHandler::SoftFault( softFaultID, BREATH_DELIVERY, INSPIRATORYPAUSEPHASE,
    	                     lineNumber, pFileName, pPredicate) ;
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateFlowControllerFlags_
//
//@ Interface-Description
//		This method has no arguments and has no return value.  This method is
//		called to set the controller shutdown flag of the flow controller to
//		TRUE.  This method overloads the base class method.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Set flags to true.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
InspiratoryPausePhase::updateFlowControllerFlags_( void)
{
	CALL_TRACE("InspiratoryPausePhase::updateFlowControllerFlags_( void)") ;

   	// $[TI1]
	RO2FlowController.setControllerShutdown( TRUE) ;
	RAirFlowController.setControllerShutdown( TRUE) ;
}

//=====================================================================
//
//  Private Methods...
//
//=====================================================================







