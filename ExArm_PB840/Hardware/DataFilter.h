
//*****************************************************************************
//
//  PressureFilter.h - Header file for the PressureFilter_t class. This class
//      is used by the servo thread for filtering pressure data from Paw, Pint, etc.
//
//*****************************************************************************

#ifndef DATA_FILTER_H

#define DATA_FILTER_H

#include "Sigma.hh"

template <class T>
class DataFilter_t
{
    public:
		DataFilter_t() { FullyFilteredValue = 0; }

		~DataFilter_t() { }

        T FilterValue(T UnfilteredValue, uint16_t TimeConst)
		{
			FullyFilteredValue = ((FullyFilteredValue * TimeConst + UnfilteredValue) - FullyFilteredValue) / TimeConst;
			return FullyFilteredValue;
		}

        T GetFullyFilteredValue()
        {
            return FullyFilteredValue;
        }


    protected:
        T FullyFilteredValue;
};


#endif

