#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-2007, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: GuiTimerRegistrar - The central registration, control, and
// dispatch point for all GUI timer events.  The timer events are
// received from the OS-Foundation subsystem via the GUI task's User
// Annunciation queue.
//---------------------------------------------------------------------
//@ Interface-Description
// Objects which handle GUI timer events use the RegisterTimer() method to
// register for a callback.  The callback is the virtual method
// GuiTimerTarget::timerEventHappened() -- therefore all objects which need to
// receive timer callbacks are derived from the GuiTimerTarget class.  All
// unique GUI timer events are specified with the GuiTimerId class enum.
//
// When timer events are received by the GUI Application's event loop
// this registrar is informed via TimerEventHappened() which will then
// look after informing the interested party (if any).
//
// Objects can start and stop timers via the StartTimer() and CancelTimer()
// methods.
//
// Before registering of targets can occur, though, the Initialize() method
// must be called to do one-time construction of the MsgTimer objects in this
// class.
//
// Note: only one object at a time can be registered for any one timer.
//---------------------------------------------------------------------
//@ Rationale
// This class is necessary to register, set, and centrally dispatch timer
// events which are received from the Operating-System subsystem.
//---------------------------------------------------------------------
//@ Implementation-Description
// Only one instance of this class is created.  The class is implemented
// with the TimerArray_[] as the centerpiece.  This is an array of
// private GuiTimerRecord_ records -- one for each unique timer id.  The 
// GuiTimerId acts as the index into the TimerArray_[].
// The RegisterTarget() method is used to initialize the pointer
// slots of the TimerArray_[] -- this is done on-the-fly as needed.
//
// Timer events are communicated to the registrar via the
// TimerEventHappened() method.  This then calls the
// timerEventHappened() of the object which is registered as a target
// of the particular timer event.
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and pre-conditions
// to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
// Unlike PatientDataRegistrar, GuiTimerRegistrar
// allows only one object at a time to be registered for a particular
// timer.  "Un-registering" is not allowed.  If you register for a timer
// you stay registered for it until another object registers for the same
// timer.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/GuiTimerRegistrar.ccv   25.0.4.0   19 Nov 2013 14:07:54   pvcs  $
//
//@ Modification-Log
//
//  Revision: 015   By: rpr   Date: 07-July-2010     SCR Number: 6590
//  Project:  PROX
//  Description:
//      Modified the elasped time for the manual purge in progress calculation  
//		based on 40 seconds.
// 
//  Revision: 014  By:  rhj    Date:  19-Mar-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//	Added PROX_STATUS_TIMEOUT
// 
//  Revision: 013  By:  rhj    Date:  18-Feb-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//	Added PROX_MANUAL_PURGE_TIMEOUT and PROX_MANUAL_PURGE_PROGRESS_TIMER_TICK
//
//  Revision: 012  By:  gdc    Date:  31-Aug-2010    SCR Number: 6663
//  Project:  API/MAT
//  Description:
//	Changed to accomodate more low priority messages in message area.
//
//  Revision: 011  By:  gdc    Date:  10-Dec-2008    SCR Number: 6377
//  Project:  840S
//  Description:
//	Removed DELTA project single screen support.
//
//  Revision: 010   By: gdc    Date: 24-Sep-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal non-invasive CPAP.
//
//  Revision: 010   By: rhj    Date: 07-July-2008   SCR Number: 6435
//  Project:  LEAK
//  Description:
//      Modified to support Leak Compensation.
//       
//  Revision: 009  By: gdc    Date: 20-Jun-2007   SCR Number:  6330
//  Project:  Trend
//  Description:
//		Removed SUN prototype code.
//
//  Revision: 008  By: rhj    Date: 08-Jan-2007   SCR Number:  6237
//  Project:  Trend
//  Description:
//		Added Trending timers.
//
//  Revision: 007  By: gdc    Date: 30-May-2005   SCR Number:  6170
//  Project:  NIV
//  Description:
//		Added 30 second timeout for High Spontaneous Inspiratory Alert.
//		Changed High Ti,spont alert timeout to 15 seconds.
//
//  Revision: 006  By: gdc    Date: 28-Aug-2000   DCS Number:  5753
//  Project:  Delta
//  Description:
//      Implemented Single Screen option.
//
//  Revision: 005  By:  yyy	   Date:  29-Jun-1998    DCS Number: 5327
//  Project:  NeoMode
//  Description:
//		Initial version.
//
//  Revision: 004  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 003  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 002  By:  yyy    Date:  10-Sep-97    DR Number: 1796
//    Project:  Sigma (R8027)
//    Description:
//      Removed all references to PREVIOUS SETUP RECOVERY.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "GuiTimerRegistrar.hh"

#include "Sigma.hh"

//@ Usage-Classes
#include "GuiTimerTarget.hh"
#include "IpcIds.hh"
#include "GuiApp.hh"
//@ End-Usage

//@ Code...

//
// Word-aligned memory pools for static member data.
//
static Uint TimerArrayMemory_ [
		((sizeof(GuiTimerRegistrar::GuiTimerRecord_)  + sizeof(Uint) - 1)
		 / sizeof(Uint)) * GuiTimerId::NUM_TIMER_IDS];

//
// Initializations of static member data.
//
GuiTimerRegistrar::GuiTimerRecord_ *GuiTimerRegistrar::TimerArray_ =
					(GuiTimerRegistrar::GuiTimerRecord_ *) ::TimerArrayMemory_;

Int32 GuiTimerRegistrar::IsDataChangedArray_[2*GuiTimerId::NUM_TIMER_IDS];
Int32 GuiTimerRegistrar::NextArrayIndex_;

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GuiTimerRecord_()  [Default Constructor]
//
//@ Interface-Description
// Constructs and initializes the private GuiTimerRecord_ structure within
// GuiTimerRegistrar.  Needs no parameters.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

GuiTimerRecordInRegistrar::GuiTimerRecord_(void) :
	msgTimer(0, 0, 0)
{
	CALL_TRACE("GuiTimerRecordInRegistrar::GuiTimerRecord_(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TimerEventHappened
//
//@ Interface-Description
// Called due to a timer event message from the operating system (comes via a
// timer task).  It is passed the event message sent by the timer task.  We
// decode the timer id from this message.  The object associated with
// this timer is then informed of the event by calling the object's
// corresponding timerEventHappened() method.
//---------------------------------------------------------------------
//@ Implementation-Description
// This is the central dispatch point for all GUI timer events.
// Since a timer event has just been received, execute the callback
// to the target object, as long as the timer is marked as active (there
// is a small chance that the timer was deactivated just as the timer
// expired and that we receive an expired event for this deactivated
// timer, we ignore this event).
//
// timerId is used as an index into TimerArray_[] to find the target
// object.
//---------------------------------------------------------------------
//@ PreCondition
// The target object for the given 'timerId' must exist.  timerId
// must be a valid timer id.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Boolean
GuiTimerRegistrar::TimerEventHappened(void)
{
	CALL_TRACE("GuiTimerRegistrar::TimerEventHappened(void)");

	Boolean eventHappened = FALSE;
	
	// GuiApp has constructed all its objects.
	// Inform targets of event
	for (Int32 index = 0; index < NextArrayIndex_; index++)
	{													// $[TI1]
		GuiTimerId::GuiTimerIdType timerId = (GuiTimerId::GuiTimerIdType)
												IsDataChangedArray_[index];

		CLASS_PRE_CONDITION(timerId >= 0 && timerId < GuiTimerId::NUM_TIMER_IDS);
		CLASS_PRE_CONDITION(TimerArray_[timerId].pTarget != NULL);

		TimerArray_[timerId].pTarget->timerEventHappened(timerId);
		eventHappened = TRUE;
	}													// $[TI2]

	// Reset the index so we will start from beginning of the
	// array.
	NextArrayIndex_ = 0;
	return(eventHappened);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TimerChangeHappened
//
//@ Interface-Description
// Called due to a timer event message from the operating system (comes via a
// timer task).  It is passed the event message sent by the timer task.  We
// decode the timer id from this message.  Then we mark the IsDataChangedArray_
// with the timer's Id to indicate a change had happened.
//---------------------------------------------------------------------
//@ Implementation-Description
// This is the preprocessing step before actually executing the timer
// event.  We only update the IsDataChangedArray_[] with the most
// recent timer Id.  The actuall processing of the timer event is
// delayed until it is time to update it.
//---------------------------------------------------------------------
//@ PreCondition
// The target object for the given 'timerId' must exist.  timerId
// must be a valid timer id.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
GuiTimerRegistrar::TimerChangeHappened(UserAnnunciationMsg &eventMsg)
{
	CALL_TRACE("GuiTimerRegistrar::TimerChangeHappened(timerId)");

	GuiTimerId::GuiTimerIdType timerId =
					(GuiTimerId::GuiTimerIdType)eventMsg.timerParts.timerId;

	CLASS_PRE_CONDITION(timerId >= 0 && timerId < GuiTimerId::NUM_TIMER_IDS);
	CLASS_PRE_CONDITION(TimerArray_[timerId].pTarget != NULL);

	//
	// Execute the callback to the registered target.  AlarmSilence timeout is a
	// special case which is handled by Alarm-Analysis; its isActive flag is 
	// is not set at all. 
	//
	if ((TimerArray_[timerId].stateMask & GuiApp::GetGuiState()) &&
		((timerId == GuiTimerId::ALARM_SILENCE_TIMEOUT) ||
		 (TimerArray_[timerId].isActive))
	   )
	{													// $[TI1]
		// Flag the data item as being changed
		IsDataChangedArray_[NextArrayIndex_] = timerId;
		NextArrayIndex_++;
	}													// $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: RegisterTarget [public, static]
//
//@ Interface-Description
// Objects needing to know when timer events occur use this method to
// register themselves for timer notices.  The parameters are as follows:
// >Von
//	timerId		The enum id of the timer associated with the target object.
//	pTarget		ptr to the object to be notified when the timer event given by
//				'timerId' occurs.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
// Registers the given target with the associated with the given 'timerId' by
// storing the given pointer into the appropriate slot of the TimerArray_[].
//---------------------------------------------------------------------
//@ PreCondition
// timerId must be valid and pTarget must be non-null.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
GuiTimerRegistrar::RegisterTarget(GuiTimerId::GuiTimerIdType timerId,
								  GuiTimerTarget* pTarget)
{
	CALL_TRACE("GuiTimerRegistrar::RegisterTarget("
									"GuiTimerId::GuiTimerIdType timerId, "
									"GuiTimerTarget* pTarget)");

	CLASS_PRE_CONDITION(timerId >= 0 && timerId < GuiTimerId::NUM_TIMER_IDS);
	CLASS_PRE_CONDITION(pTarget != NULL);

	// Cancel an existing timer, if any.
	CancelTimer(timerId);
	SAFE_CLASS_ASSERTION(FALSE == TimerArray_[timerId].isActive);

	// Register the new target.
	TimerArray_[timerId].pTarget = pTarget;				// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: StartTimer [public, static]
//
//@ Interface-Description
// Starts the specified timer and simultaneously registers an object as the
// target of the timer.  This is just a handy shortcut for RegisterTarget()
// followed by the (overloaded) simpler StartTimer() call.  The parameters are
// as follows:
// >Von
//	timerId		The enum id of the timer associated with the target object.
//	pTarget		ptr to the object to be notified when the timer event given by
//				'timerId' occurs.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
// Registers the given target with the timer associated with the given
// 'timerId' by storing the given pointer into the appropriate slot of the
// TimerArray_[].  Then, starts the corresponding timer.
//---------------------------------------------------------------------
//@ PreCondition
// timerId must be valid and pTarget must be non-null.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
GuiTimerRegistrar::StartTimer(GuiTimerId::GuiTimerIdType timerId,
								GuiTimerTarget *pTarget)
{
	CALL_TRACE("GuiTimerRegistrar::StartTimer("
			"GuiTimerId::GuiTimerIdType timerId, GuiTimerTarget *pTarget)");

	CLASS_PRE_CONDITION(timerId >= 0 && timerId < GuiTimerId::NUM_TIMER_IDS);
	CLASS_PRE_CONDITION(pTarget != NULL);

	RegisterTarget(timerId, pTarget);
	StartTimer(timerId);								// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: StartTimer [public, static]
//
//@ Interface-Description
// Starts the specified timer.  It is passed 'timerId', the enum id of
// the timer associated with the target object.
//---------------------------------------------------------------------
//@ Implementation-Description
// Starts the corresponding timer as long as something is registered
// for it.
//---------------------------------------------------------------------
//@ PreCondition
//	The timerId must be valid.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
GuiTimerRegistrar::StartTimer(GuiTimerId::GuiTimerIdType timerId)
{
	CALL_TRACE("GuiTimerRegistrar::StartTimer("
									"GuiTimerId::GuiTimerIdType timerId)");

	CLASS_PRE_CONDITION(timerId >= 0 && timerId < GuiTimerId::NUM_TIMER_IDS);

	// Start timer only if we have a registered target for it
	if (TimerArray_[timerId].pTarget != NULL)
	{														// $[TI1]
		// Set the timer going
		TimerArray_[timerId].msgTimer.set();
		TimerArray_[timerId].isActive = TRUE;
	}														// $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: CancelTimer [public, static]
//
//@ Interface-Description
//  Cancels the specified timer.  It is passed 'timerId', the enum id of
//  the timer.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Cancels the MsgTimer pointed to by timerId and marks the timer as
//  inactive.
//---------------------------------------------------------------------
//@ PreCondition
//	timerId must be a valid timer.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
GuiTimerRegistrar::CancelTimer(GuiTimerId::GuiTimerIdType timerId)
{
	CALL_TRACE("GuiTimerRegistrar::CancelTimer("
									"GuiTimerId::GuiTimerIdType timerId)");

	CLASS_PRE_CONDITION(timerId >= 0 && timerId < GuiTimerId::NUM_TIMER_IDS);

	if (TimerArray_[timerId].pTarget != NULL)
	{													// $[TI1]
		TimerArray_[timerId].msgTimer.cancel();
		TimerArray_[timerId].isActive = FALSE;
	}													// $[TI2]

	SAFE_CLASS_ASSERTION(FALSE == TimerArray_[timerId].isActive);
	SAFE_CLASS_ASSERTION(FALSE == TimerArray_[timerId].msgTimer.isSet());
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Initialize [public, static]
//
//@ Interface-Description
// Called at startup by the GUI-Apps subsystem in order to initialize
// static member data.  No parameters needed.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Performs initialization of static member data.  This function can be
// called more than once.  Only the first time must we construct the
// MsgTimer objects.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
GuiTimerRegistrar::Initialize(void)
{
	CALL_TRACE("GuiTimerRegistrar::Initialize(void)");

	SAFE_CLASS_ASSERTION(sizeof(::TimerArrayMemory_) >=
				(sizeof(GuiTimerRegistrar::GuiTimerRecord_)
											* GuiTimerId::NUM_TIMER_IDS));

	static Boolean Initialized = FALSE;		// Indicates if this function has
											// executed before
	UserAnnunciationMsg timerEventMsg;		// 32 bits of data that each
											// MsgTimer will put on our input
											// queue to identify timeout event

	NextArrayIndex_ = 0;
	timerEventMsg.timerParts.eventType = UserAnnunciationMsg::TIMER_EVENT;

	// First, clear all flags and target pointers.
	for (Int32 i = 0; i < GuiTimerId::NUM_TIMER_IDS; i++)
	{													// $[TI1]
		TimerArray_[i].pTarget = NULL;
		TimerArray_[i].isActive = FALSE;
	}													// $[TI2]

	// If we've previously constructed the MsgTimer objects via a previous call
	// to this method then make sure all MsgTimers are cancelled and just
	// return.
	if (Initialized)
	{													// $[TI3]
		for (Int32 i = 0; i < GuiTimerId::NUM_TIMER_IDS; i++)
		{												// $[TI3.1]
			TimerArray_[i].msgTimer.cancel();
		}												// $[TI3.2]
		return;
	}													// $[TI4]

	// Run placement new for each timer object.

	// No Ventilation Click tick timer, 1 second.
	timerEventMsg.timerParts.timerId = GuiTimerId::NO_VENT_CLOCK;
	TimerArray_[GuiTimerId::NO_VENT_CLOCK].stateMask = STATE_ONLINE |
													STATE_INOP | STATE_TIMEOUT;
	new (&(TimerArray_[GuiTimerId::NO_VENT_CLOCK].msgTimer))
		MsgTimer(USER_ANNUNCIATION_Q, timerEventMsg.qWord, 1000, MsgTimer::PERIODIC);

	// Wall Clock timer, every 15 seconds.
	timerEventMsg.timerParts.timerId = GuiTimerId::WALL_CLOCK;
	TimerArray_[GuiTimerId::WALL_CLOCK].stateMask = STATE_ONLINE | STATE_SERVICE |
													STATE_SST | STATE_INOP | STATE_TIMEOUT;
	new (&(TimerArray_[GuiTimerId::WALL_CLOCK].msgTimer))
		MsgTimer(USER_ANNUNCIATION_Q, timerEventMsg.qWord, 15000, MsgTimer::PERIODIC);

	// Auto repeat timeout, 1 second.
	timerEventMsg.timerParts.timerId = GuiTimerId::AUTO_REPEAT_CLOCK;
	TimerArray_[GuiTimerId::AUTO_REPEAT_CLOCK].stateMask = STATE_ONLINE |STATE_SERVICE |
													STATE_SST | STATE_INOP | STATE_TIMEOUT;
	new (&(TimerArray_[GuiTimerId::AUTO_REPEAT_CLOCK].msgTimer))
		MsgTimer(USER_ANNUNCIATION_Q, timerEventMsg.qWord, 1000, MsgTimer::PERIODIC);

	timerEventMsg.timerParts.timerId = GuiTimerId::LAPTOP_COUNTDOWN_CLOCK;
	TimerArray_[GuiTimerId::LAPTOP_COUNTDOWN_CLOCK].stateMask =	STATE_SERVICE;
	new (&(TimerArray_[GuiTimerId::LAPTOP_COUNTDOWN_CLOCK].msgTimer))
		MsgTimer(USER_ANNUNCIATION_Q, timerEventMsg.qWord, 5000, MsgTimer::ONE_SHOT);

	// Main Setting Change timeout, 2 minutes.
	timerEventMsg.timerParts.timerId = GuiTimerId::MAIN_SETTING_CHANGE_TIMEOUT;
	TimerArray_[GuiTimerId::MAIN_SETTING_CHANGE_TIMEOUT].stateMask = STATE_ONLINE;
	new (&(TimerArray_[GuiTimerId::MAIN_SETTING_CHANGE_TIMEOUT].msgTimer))
		MsgTimer(USER_ANNUNCIATION_Q, timerEventMsg.qWord, 120000, MsgTimer::ONE_SHOT);

#ifdef SIGMA_UNIT_TEST
	// Subscreen Setting Change timeout for unit test purposes, 1 minute
	timerEventMsg.timerParts.timerId =
								GuiTimerId::SUBSCREEN_SETTING_CHANGE_TIMEOUT;
	TimerArray_[GuiTimerId::SUBSCREEN_SETTING_CHANGE_TIMEOUT].stateMask = STATE_ONLINE |STATE_SERVICE;
	new (&(TimerArray_[GuiTimerId::SUBSCREEN_SETTING_CHANGE_TIMEOUT].msgTimer))
		MsgTimer(USER_ANNUNCIATION_Q, timerEventMsg.qWord, 60000, MsgTimer::ONE_SHOT);
#else // SIGMA_UNIT_TEST
	// Subscreen Setting Change timeout, 3 minutes
	timerEventMsg.timerParts.timerId =
								GuiTimerId::SUBSCREEN_SETTING_CHANGE_TIMEOUT;
	TimerArray_[GuiTimerId::SUBSCREEN_SETTING_CHANGE_TIMEOUT].stateMask = STATE_ONLINE | STATE_SERVICE;
	new (&(TimerArray_[GuiTimerId::SUBSCREEN_SETTING_CHANGE_TIMEOUT].msgTimer))
		MsgTimer(USER_ANNUNCIATION_Q, timerEventMsg.qWord, 180000, MsgTimer::ONE_SHOT);
#endif // SIGMA_UNIT_TEST

	// Short Self Test Inactivity timer, 30 seconds.
	timerEventMsg.timerParts.timerId = GuiTimerId::SST_INACTIVITY_TIMEOUT;
	TimerArray_[GuiTimerId::SST_INACTIVITY_TIMEOUT].stateMask = STATE_SERVICE;
	new (&(TimerArray_[GuiTimerId::SST_INACTIVITY_TIMEOUT].msgTimer))
		MsgTimer(USER_ANNUNCIATION_Q, timerEventMsg.qWord, 30000,
					MsgTimer::ONE_SHOT);

	// Alarm Silence timeout, 1 second.  This MsgTimer is never used 
	// (ALARM_SILENCE_TIMEOUT is used only by the Alarms task) but we need
	// to create it so that no error occurs.
	timerEventMsg.timerParts.timerId = GuiTimerId::ALARM_SILENCE_TIMEOUT;
	TimerArray_[GuiTimerId::ALARM_SILENCE_TIMEOUT].stateMask = STATE_ONLINE |
													STATE_INOP | STATE_TIMEOUT;
	new (&(TimerArray_[GuiTimerId::ALARM_SILENCE_TIMEOUT].msgTimer))
		MsgTimer(USER_ANNUNCIATION_Q, timerEventMsg.qWord, 1000,
					MsgTimer::ONE_SHOT);

	// More Alarms subscreen activation timeout, 3 minutes
	timerEventMsg.timerParts.timerId = GuiTimerId::MORE_ALARMS_TIMEOUT;
	TimerArray_[GuiTimerId::MORE_ALARMS_TIMEOUT].stateMask = STATE_ONLINE |
													STATE_INOP | STATE_TIMEOUT;
	new (&(TimerArray_[GuiTimerId::MORE_ALARMS_TIMEOUT].msgTimer))
		MsgTimer(USER_ANNUNCIATION_Q, timerEventMsg.qWord, 180000,
					MsgTimer::ONE_SHOT);

	// EST test pause timeout, 2 seconds
	timerEventMsg.timerParts.timerId = GuiTimerId::SERVICE_MODE_TEST_PAUSE_TIMEOUT;
	TimerArray_[GuiTimerId::SERVICE_MODE_TEST_PAUSE_TIMEOUT].stateMask = STATE_SERVICE | STATE_SST;
	new (&(TimerArray_[GuiTimerId::SERVICE_MODE_TEST_PAUSE_TIMEOUT].msgTimer))
		MsgTimer(USER_ANNUNCIATION_Q, timerEventMsg.qWord, 2000,
					MsgTimer::ONE_SHOT);

    timerEventMsg.timerParts.timerId = GuiTimerId::TOUCH_RELEASE_TIMEOUT;
    TimerArray_[GuiTimerId::TOUCH_RELEASE_TIMEOUT].stateMask = 0xffff;
    new (&(TimerArray_[GuiTimerId::TOUCH_RELEASE_TIMEOUT].msgTimer))
        MsgTimer(USER_ANNUNCIATION_Q, timerEventMsg.qWord, 100, MsgTimer::ONE_SHOT);
 
    timerEventMsg.timerParts.timerId = GuiTimerId::DELAYED_PAUSE_TIMEOUT;
    TimerArray_[GuiTimerId::DELAYED_PAUSE_TIMEOUT].stateMask = 0xffff;
    new (&(TimerArray_[GuiTimerId::DELAYED_PAUSE_TIMEOUT].msgTimer))
        MsgTimer(USER_ANNUNCIATION_Q, timerEventMsg.qWord, 5000, MsgTimer::ONE_SHOT);
 
    timerEventMsg.timerParts.timerId = GuiTimerId::ALARM_PROGRESS_TIMER_TICK;
    TimerArray_[GuiTimerId::ALARM_PROGRESS_TIMER_TICK].stateMask = 0xffff;
    new (&(TimerArray_[GuiTimerId::ALARM_PROGRESS_TIMER_TICK].msgTimer))
        MsgTimer(USER_ANNUNCIATION_Q, timerEventMsg.qWord, 2000, MsgTimer::PERIODIC);
 
    timerEventMsg.timerParts.timerId = GuiTimerId::O2_PROGRESS_TIMER_TICK;
    TimerArray_[GuiTimerId::O2_PROGRESS_TIMER_TICK].stateMask = 0xffff;
    new (&(TimerArray_[GuiTimerId::O2_PROGRESS_TIMER_TICK].msgTimer))
        MsgTimer(USER_ANNUNCIATION_Q, timerEventMsg.qWord, 2000, MsgTimer::PERIODIC);
 
	// timer for high spontaneous inspiratory alert (NIV)
    timerEventMsg.timerParts.timerId = GuiTimerId::HIGH_SPONT_INSP_TIME_ALERT_TIMER;
    TimerArray_[GuiTimerId::HIGH_SPONT_INSP_TIME_ALERT_TIMER].stateMask = STATE_ONLINE;
    new (&(TimerArray_[GuiTimerId::HIGH_SPONT_INSP_TIME_ALERT_TIMER].msgTimer))
        MsgTimer(USER_ANNUNCIATION_Q, timerEventMsg.qWord, 15000, MsgTimer::ONE_SHOT);

	// Auto repeat timeout, 60 second.
	timerEventMsg.timerParts.timerId = GuiTimerId::TREND_DATA_SET_TIMER_TICK;
	TimerArray_[GuiTimerId::TREND_DATA_SET_TIMER_TICK].stateMask = STATE_ONLINE |
													       STATE_INOP | STATE_TIMEOUT;
	new (&(TimerArray_[GuiTimerId::TREND_DATA_SET_TIMER_TICK].msgTimer))
		MsgTimer(USER_ANNUNCIATION_Q, timerEventMsg.qWord, 60000, MsgTimer::PERIODIC);

    // one shot timeout, 700 milliseconds
	timerEventMsg.timerParts.timerId = GuiTimerId::TREND_TABLE_TIMER;
	TimerArray_[GuiTimerId::TREND_TABLE_TIMER].stateMask = STATE_ONLINE |
													       STATE_INOP | STATE_TIMEOUT;
	new (&(TimerArray_[GuiTimerId::TREND_TABLE_TIMER].msgTimer))
		MsgTimer(USER_ANNUNCIATION_Q, timerEventMsg.qWord, 700, MsgTimer::ONE_SHOT);

	// walk-away timer for automatically accepting the Nif Maneuver 
    timerEventMsg.timerParts.timerId = GuiTimerId::RESPM_NIF_MANEUVER_TIMEOUT;
    TimerArray_[GuiTimerId::RESPM_NIF_MANEUVER_TIMEOUT].stateMask = STATE_ONLINE;
    new (&(TimerArray_[GuiTimerId::RESPM_NIF_MANEUVER_TIMEOUT].msgTimer))
        MsgTimer(USER_ANNUNCIATION_Q, timerEventMsg.qWord, 180000, MsgTimer::ONE_SHOT);

	// walk-away timer for automatically accepting the P100 Maneuver 
    timerEventMsg.timerParts.timerId = GuiTimerId::RESPM_P100_MANEUVER_TIMEOUT;
    TimerArray_[GuiTimerId::RESPM_P100_MANEUVER_TIMEOUT].stateMask = STATE_ONLINE;
    new (&(TimerArray_[GuiTimerId::RESPM_P100_MANEUVER_TIMEOUT].msgTimer))
        MsgTimer(USER_ANNUNCIATION_Q, timerEventMsg.qWord, 180000, MsgTimer::ONE_SHOT);

	// walk-away timer for automatically accepting the VC Maneuver 
    timerEventMsg.timerParts.timerId = GuiTimerId::RESPM_VC_MANEUVER_TIMEOUT;
    TimerArray_[GuiTimerId::RESPM_VC_MANEUVER_TIMEOUT].stateMask = STATE_ONLINE;
    new (&(TimerArray_[GuiTimerId::RESPM_VC_MANEUVER_TIMEOUT].msgTimer))
        MsgTimer(USER_ANNUNCIATION_Q, timerEventMsg.qWord, 180000, MsgTimer::ONE_SHOT);

	// One shot timer to retry data set request if a setting has focus, 5 seconds
	timerEventMsg.timerParts.timerId = GuiTimerId::TREND_DATA_SET_RETRY_TIMER;
	TimerArray_[GuiTimerId::TREND_DATA_SET_RETRY_TIMER].stateMask = STATE_ONLINE | STATE_INOP | STATE_TIMEOUT;
	new (&(TimerArray_[GuiTimerId::TREND_DATA_SET_RETRY_TIMER].msgTimer))
		MsgTimer(USER_ANNUNCIATION_Q, timerEventMsg.qWord, 5000, MsgTimer::ONE_SHOT);

	// One shot timer to retry displaying data if trend button is pressed or
	// if data request is pending, 1 second
	timerEventMsg.timerParts.timerId = GuiTimerId::TREND_DISPLAY_RETRY_TIMER;
	TimerArray_[GuiTimerId::TREND_DISPLAY_RETRY_TIMER].stateMask = STATE_ONLINE | STATE_INOP | STATE_TIMEOUT;
	new (&(TimerArray_[GuiTimerId::TREND_DISPLAY_RETRY_TIMER].msgTimer))
		MsgTimer(USER_ANNUNCIATION_Q, timerEventMsg.qWord, 1000, MsgTimer::ONE_SHOT);

	// timer to toggle between the mode and LEAK COMP in main settings area
    timerEventMsg.timerParts.timerId = GuiTimerId::LEAK_COMP_LOWER_DISPLAY_TIMER;
    TimerArray_[GuiTimerId::LEAK_COMP_LOWER_DISPLAY_TIMER].stateMask = STATE_ONLINE | STATE_INOP | STATE_TIMEOUT;
    new (&(TimerArray_[GuiTimerId::LEAK_COMP_LOWER_DISPLAY_TIMER].msgTimer))
        MsgTimer(USER_ANNUNCIATION_Q, timerEventMsg.qWord, 2000, MsgTimer::PERIODIC);

	// timer to retrieve and scroll through the low priority messages
	timerEventMsg.timerParts.timerId = GuiTimerId::MESSAGE_AREA_TIMER;
	TimerArray_[GuiTimerId::MESSAGE_AREA_TIMER].stateMask = STATE_ONLINE | STATE_SERVICE | STATE_SST;
	new (&(TimerArray_[GuiTimerId::MESSAGE_AREA_TIMER].msgTimer))
		MsgTimer(USER_ANNUNCIATION_Q, timerEventMsg.qWord, 2000, MsgTimer::PERIODIC);

    // Update the prox manual progress bar every 500 ms.
    timerEventMsg.timerParts.timerId = GuiTimerId::PROX_MANUAL_PURGE_PROGRESS_TIMER_TICK;
    TimerArray_[GuiTimerId::PROX_MANUAL_PURGE_PROGRESS_TIMER_TICK].stateMask = STATE_ONLINE;
    new (&(TimerArray_[GuiTimerId::PROX_MANUAL_PURGE_PROGRESS_TIMER_TICK].msgTimer))
        MsgTimer(USER_ANNUNCIATION_Q, timerEventMsg.qWord, 500, MsgTimer::PERIODIC);

	// One shot timer 40 sec timeout is dependent on the number of delays
	// in ProxInterface::executeManualPurge_
    timerEventMsg.timerParts.timerId = GuiTimerId::PROX_MANUAL_PURGE_TIMEOUT;
    TimerArray_[GuiTimerId::PROX_MANUAL_PURGE_TIMEOUT].stateMask = STATE_ONLINE;
    new (&(TimerArray_[GuiTimerId::PROX_MANUAL_PURGE_TIMEOUT].msgTimer))
        MsgTimer(USER_ANNUNCIATION_Q, timerEventMsg.qWord, 40000, MsgTimer::ONE_SHOT);

	// Update the prox status text on the waveformsubscreen for a min of 4 secs.
    timerEventMsg.timerParts.timerId = GuiTimerId::PROX_STATUS_TIMEOUT;
    TimerArray_[GuiTimerId::PROX_STATUS_TIMEOUT].stateMask = STATE_ONLINE;
    new (&(TimerArray_[GuiTimerId::PROX_STATUS_TIMEOUT].msgTimer))
        MsgTimer(USER_ANNUNCIATION_Q, timerEventMsg.qWord, 4000, MsgTimer::ONE_SHOT);


    // Fully initialized
	Initialized = TRUE;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  isActive [public, static]
//
//@ Interface-Description
//  Called whenever users want to check the activity status of a timer id.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Return the 'isActive' value associated with that given timer id.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Boolean
GuiTimerRegistrar::isActive(GuiTimerId::GuiTimerIdType timerId)
{
	return(TimerArray_[timerId].isActive);
	// $[TI1]
}


#ifdef SIGMA_SUN
//============================ M E T H O D   D E S C R I P T I O N ====
// Method: SunTimerEventHappened [public, static]
//
// Interface-Description
// Called due to a timer event from the MsgTimer class for the Sun only
// implementation of the GUI application.  Passes event info onto
// TimerEventHappened().
//---------------------------------------------------------------------
// Implementation-Description
// Simply passes event information onto TimerEventHappened while blocking
// screen updates until the event is handled.
//---------------------------------------------------------------------
// PreCondition
//	none
//---------------------------------------------------------------------
// PostCondition
//	none
// End-Method
//=====================================================================

void
GuiTimerRegistrar::SunTimerEventHappened(Int32 msg)
{
	UserAnnunciationMsg eventMsg;

	eventMsg.qWord = msg;
	GuiTimerId::GuiTimerIdType timerId =
					(GuiTimerId::GuiTimerIdType)eventMsg.timerParts.timerId;

	ScreenBlock();
	IsDataChangedArray_[NextArrayIndex_] = timerId;
	NextArrayIndex_++;
	TimerEventHappened();
	ScreenUnBlock();
}
#endif // SIGMA_SUN


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
GuiTimerRegistrar::SoftFault(const SoftFaultID  softFaultID,
						     const Uint32       lineNumber,
						     const char*        pFileName,
						     const char*        pPredicate)  
{
	CALL_TRACE("GuiTimerRegistrar::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, GUITIMERREGISTRAR,
							lineNumber, pFileName, pPredicate);
}
