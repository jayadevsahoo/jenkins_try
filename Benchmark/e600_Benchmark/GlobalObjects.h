#pragma once

enum GlobalPenType
{
	Pen_Curve,
	Pen_OldCurve,
	Pen_Wiper
};

class GlobalObjects
{
	void initialize_( );
	GlobalObjects(void);
	static GlobalObjects* Instance_;

	CFont Arial_16_Bold_;
	CFont Arial_24_Bold_;
	CFont Arial_48_Bold_;
	CPen  PenForCurve_;
	CPen  PenForOldCurve_;
	CPen  PenForWiper_;

public:

	static GlobalObjects* getInstance();

	CFont* getArial_16_Bold();

	CFont* getArial_24_Bold();

	CFont* getArial_48_Bold();

	CPen* getPen( GlobalPenType penType );

	~GlobalObjects(void);
};
