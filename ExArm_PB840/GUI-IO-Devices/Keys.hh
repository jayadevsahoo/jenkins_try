#ifndef Keys_HH
#define Keys_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1994-1995, Puritan-Bennett Corporation
//=====================================================================

//@ Type:  Uint32
// An unsigned, 32-bit integer data type.
typedef unsigned int	GuiKeys;

// Number of bits in the keyboard register(s). This is the only 
// place in the software which lists all of the GUI I/O keyboard keys that may be
// pressed. See KeyMapPass[] in GUI-Foundations\KeyPanel.cc to see how hardware
// register bits are mapped to logical membrane buttons.
#ifdef SIGMA_GUIPC_CPU
#define    NUMBER_OF_GUI_KEYS 14
#else //On Hardware
#define    NUMBER_OF_GUI_KEYS 8
#endif

#endif	// Keys_HH
