#pragma once
#include "resource.h"

// CVentStatusDlg dialog

struct PDData
{
	CString strLabel;
	CString strValue;
	CString strUnit; 
} ;



class CVentStatusDlg : public CDialog
{
	DECLARE_DYNAMIC(CVentStatusDlg)

public:
	CVentStatusDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CVentStatusDlg();




// Dialog Data
	enum { IDD = IDD_VENT_STATUS_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButtonRefresh();
	CString m_strPD1;
	CString m_strButtonID;
	afx_msg void OnBnClickedButtonGettext();

	void UpdateHeader();
	void UpdatePD();
	void UpdateCentre();
	void UpdateMetaData();

	PDData GetPDData(CString);
	CString m_strMainCentre;
	CString m_strBanner;
	CString m_strBanner_2;
	CString m_strBanner_3;
	CString m_strBanner_4;
	CString m_strBanner_5;
	CString m_strBanner_6;
	CString m_strPD2;
	CString m_strPD3;
	CString m_strPD4;
	CString m_strCtr_1;
	CString m_strCtr_2;
	CString m_strCtr_3;
	CString m_strCtr_4;
	CString m_strCtr_5;
	CString m_strCtr_6;
	CString m_strCtr_7;
	CString m_strMeta;
};
