#ifndef P100Maneuver_HH
#define P100Maneuver_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2006, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: P100Maneuver - P100 Maneuver Handler
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/P100Maneuver.hhv   25.0.4.0   19 Nov 2013 13:59:58   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001   By: gdc   Date:  22-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Initial version.
//
//====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"
#include "EventData.hh"
#include "Maneuver.hh"
#include "TimerTarget.hh"
#include "PauseTypes.hh"

//@ Usage-Classes

//@ End-Usage



class P100Maneuver :  public Maneuver, public TimerTarget
{
  public:

	P100Maneuver(ManeuverId maneuverId, IntervalTimer& rPauseTimer);
    ~P100Maneuver(void);

    static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL, 
			  const char*       pPredicate = NULL);

	// virtual from Maneuver
	virtual void activate(void);
	virtual Boolean complete(void);
	virtual void clear(void);
	virtual void terminateManeuver(void);
	virtual void newCycle(void);
	virtual Boolean processUserEvent(UiEvent& rUiEvent);

	EventData::EventStatus handleManeuver(const Boolean isStartRequest);

	inline Real32 getP100Pressure(void) const;
  
	virtual void timeUpHappened(const IntervalTimer& timer);

  protected:

  private:
    P100Maneuver(const P100Maneuver&);		// not implemented...
    void   operator=(const P100Maneuver&);	// not implemented...
	P100Maneuver(void);

	//@ Data-Member: &rPauseTimer_
	// a reference to the server IntervalTimer.
	IntervalTimer& rPauseTimer_;

    //@ Data-Member: pauseTime_
    // pause timer count.
    Int32 pauseTime_;
    
	//@ Data-Member: p100ElapsedTime_
	// Number of milliseconds since P100 measurement started
	Int32 p100ElapsedTime_;
	
	//@ Data-Member: isP100Started_
	// TRUE upon starting P100 measurement timer
	Boolean isP100TimerStarted_;
	
	//@ Data-Member: isTimedOut_
	// TRUE when interval timer expires during maneuver
	Boolean isTimedOut_;
	
	//@ Data-Member: p100Pressure_
	// Pressure at 100ms after patient effort sensed at PEEP-0.5 cmH2O 
	Real32 p100Pressure_;

	//@ Data-Member: referencePressure_
	// Pressure measured activation (after valves closed)
	Real32 referencePressure_;

	//@ Data-Member: activeElapsedTime_
	// Number of milliseconds since P100 maneuver was activated (valves were closed)
	Int32 activeElapsedTime_;
	
	//@ Data-Member: isExhalationPhaseReady_
	// TRUE when exhalation phase has reduced target flow to minimum 
	Boolean isExhalationPhaseReady_;
	
};



// Inlined methods...
#include "P100Maneuver.in"


#endif // P100Maneuver_HH 
