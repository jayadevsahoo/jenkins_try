
#ifndef MathUtilities_HH
#define MathUtilities_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
// Filename:  MathUtilities - System-Wide Math Utilities.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Utilities/vcssrc/MathUtilities.hhv   25.0.4.0   19 Nov 2013 14:35:14   pvcs  $
//
//@ Modifications
//
//   Revision: 005   By: gdc   Date: 24-Aug-2010   SCR Number: 6663
//   Project:  API/MAT
//   Description:
//		Added Fletcher32 function.
//
//   Revision: 004   By: gdc   Date: 26-Sep-2006   DCS Number: 6236
//   Project:  RESPM
//   Description:
//		Added IsFpError function.
//  
//   Revision: 003   By: gdc   Date: 16-Jun-1998   DCS Number: 5055
//   Project:  Color
//   Description:
//		Initial version.
//  
//   Revision: 002   By: sah   Date: 06-Feb-1996   DCS Number: 674
//   Project:  Sigma (R8027)
//   Description:
//	Removed 'ifndef' and 'endif' from include line.
//  
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//=====================================================================

#include "Sigma.hh"
#include "Precision.hh"

//@ Usage-Class
//@ End-Usage


//@ Begin-Free-Declarations
extern Boolean  IsEquivalent(const Real32    value1,
			     const Real32    value2,
			     const Precision precision);

extern Real32  PreciseValue(const Real32 value, const Precision precision);

extern Int32   WholePart(const Real32 value);
extern Real32  FractPart(const Real32 value, const Precision precision);

extern Real32  Modulus(const Real32    x,
		       const Real32    y,
		       const Precision precision);

extern Real32  Magnitude(const Real32 value, const Int32 magnitude);

extern Int32   DecimalPlaces(const Precision precision);

extern Boolean IsFpError(Real32 value);

extern Uint32  Fletcher32( const Uint16 *pData, Uint len );
extern Boolean IsFletcherChecksumValid(const unsigned char *buffer, Uint16 length);

namespace MathUtils
{
template<class T>
void clampValue(T& value, const T min, const T max)
{
	if(value < min)
		value = min;
	else if(value > max)
		value = max;
}
};

//@ End-Free-Declarations


#endif  // MathUtilities_HH
