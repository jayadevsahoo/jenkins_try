#ifndef DataKeyAgent_HH
#define DataKeyAgent_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//====================================================================
 
//====================================================================
// Class: DataKeyAgent - Interface Agent to the Datakey.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/DataKeyAgent.hhv   25.0.4.0   19 Nov 2013 13:54:02   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By: sah    Date: 01-Feb-2000   DR Number: 5424
//  Project:  NeoMode
//  Description:
//      Modified to use new static interface to 'SoftwareOptions' class.
//
//  Revision: 003  By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 002  By:  syw    Date: 04-Dec-1997    DR Number: none
//      Project:   Sigma   (R8027)
//      Description:
//         BiLevel initial version.  Added getSoftwareOptions() and
//		   SoftwareOptions options_ data member.
//
//  Revision: 001  By:  syw    Date: 08-Nov-1996    DR Number: INITIAL RELEASE
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//====================================================================
#include "BD_IO_Devices.hh"

//@ Usage-Classes

#include "SoftwareOptions.hh"
#include "DataKey.hh"
#include "SerialNumber.hh"

//@ End-Usage

class DataKeyAgent
{
	public:
		DataKeyAgent(void);
    	~DataKeyAgent(void);

    	DataKeyAgent(const DataKeyAgent&);

    	inline DataKey::DataKeyType getDataKeyType(void) const;
    	inline const SerialNumber & getBdSerialNumber(void) const;
    	inline const SerialNumber & getGuiSerialNumber(void) const;
    	inline Boolean isInstalled(void) const;
		
    	static void SoftFault( const SoftFaultID softFaultID,
							   const Uint32      lineNumber,
							   const char*       pFileName  = NULL,
							   const char*       pPredicate = NULL );

  	private:
    	void operator=(const DataKeyAgent&); 

    	// Data-Member: bdSerialNumber_ 
    	// BDU serial number stored in the data key.
    	const SerialNumber bdSerialNumber_;
 
    	// Data-Member: guiSerialNumber_ 
    	// GUI serial number stored in the data key.
    	const SerialNumber guiSerialNumber_;
 
    	// Data-Member: dataKeyType_
    	// Type of data key.
    	DataKey::DataKeyType dataKeyType_;

    	//@ Data-Member: isInstalled_
    	// is data key installed
    	Boolean isInstalled_ ;

        //@ Data-Member: options_
        // software options stored in the data key
        const SoftwareOptions options_ ;
};

#include "DataKeyAgent.in"

#endif // DataKeyAgent_HH


