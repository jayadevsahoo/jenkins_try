#ifndef MsgQueue_HH
#define MsgQueue_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: MsgQueue - A handle class derived from IpcQueue class for 
// passing Int32 messages between tasks.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/OS-Foundation/vcssrc/MsgQueue.hhv   25.0.4.0   19 Nov 2013 14:15:16   pvcs  $
//
//@ Modification-Log
// 
//  Revision: 002   By: sah   Date: 13-Jan-1999  DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001  By:  djl    Date:  6-Oct-1993    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//====================================================================

#include "Sigma.hh"
#include "OsFoundation.hh"

//@ Usage-Classes
#include "IpcQueue.hh"
//@ End-Usage

//This is an interface to a list of all created Msg queues in the
//IPC queue list
class MsgQueueList
{
public:
	static void    Initialize(void);
    static Int32   PutMsg(const Int32 qId, const Int32 msg);

    static void SoftFault(const SoftFaultID softFaultID,
							const Uint32      lineNumber,
							const char*       pFileName  = NULL, 
							const char*       pPredicate = NULL);
};

//This is an object of type IpcQueue that can be instantiated
//by the relevant IPC queue ID
class MsgQueue : public IpcQueue
{
  public:
    MsgQueue(const Int32 qId);
    ~MsgQueue(void);
 
    Int32          putMsg(const Int32 msg);

    void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL, 
			  const char*       pPredicate = NULL);
  
	//The old code was mixing between object-oriented and C style.. On one hand, this class is-a IpcQueue
	//that we could use to instanciate any queue by ID, and on the other hand the class is used as static to
	//get access to any OTHER queue object by ID!! this is a recipes for spaghettii code. Therefore, all static
	//interfaces were relocated to their own class, and this one was kept only to avoid changing all its callers
	//all over the code, and was mapped to the actual new static call.
	//TODO E600 VM: eliminate this function and change callers to call the new
	// MsgQueueList::PutMsg(qId, msg)
	static Int32 PutMsg(const Int32 qId, const Int32 msg) { return MsgQueueList::PutMsg(qId, msg); }

  private:
    // these methods are purposely declared, but not implemented...
    MsgQueue(void);			// not implemented...
    MsgQueue(const MsgQueue&);		// not implemented...
    void   operator=(const MsgQueue&);	// not implemented...
};


#endif // MsgQueue_HH 
