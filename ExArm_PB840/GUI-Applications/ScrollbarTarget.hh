#ifndef ScrollbarTarget_HH
#define ScrollbarTarget_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: ScrollbarTarget -  A target class for receiving scrollbar
// manipulation events.  Classes derive from this class and use it to
// learn of scrolling interactions with a Scrollbar object.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/ScrollbarTarget.hhv   25.0.4.0   19 Nov 2013 14:08:24   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 001  By:  mpm    Date:  06-JUN-95    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

//@ Usage-Classes
#include "GuiAppClassIds.hh"
//@ End-Usage

class ScrollbarTarget
{
public:
	virtual void scrollHappened(Uint16 firstDisplayedEntry);

    static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);
  
protected:
	ScrollbarTarget(void);
    virtual ~ScrollbarTarget(void);

private:
    // these methods are purposely declared, but not implemented...
    ScrollbarTarget(const ScrollbarTarget&);		// not implemented...
    void   operator=(const ScrollbarTarget&);		// not implemented...

};


#endif // ScrollbarTarget_HH
