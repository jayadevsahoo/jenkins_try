#include "stdafx.h"
//====================================================================
// This is a proprietary work to which Nellcor Puritan Bennett
// corporation of California claims exclusive right.  No part of this
// work may be used, disclosed, reproduced, sorted in an information
// retrieval system, or transmitted by any means, electronic,
// mechanical, photocopying, recording, or otherwise without the prior
// written permission of Nellcor Puritan Bennett Corporation.
//
//     Copyright (c) 1995, Nellcor Puritan Bennett Corporation
//=====================================================================


//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: SerialNumber - a string that contains serial number information.
//---------------------------------------------------------------------
//@ Interface-Description
//		The serial number class contains a string of length SERIAL_NO_SIZE.
//		The	default serial number is "XXXXXXXXXX".  Methods are provided to
//		copy, compare, and get the string of the serial number.
//---------------------------------------------------------------------
//@ Rationale
//		Abstraction of serial number.
//---------------------------------------------------------------------
//@ Implementation-Description
//		A default serial number of "XXXXXXXXXX" can be constructed or a serial
//		number with any string.  The serial number is stored as a char[]
//		of length SERIAL_NO_SIZE + 1 (NULL terminated).
//---------------------------------------------------------------------
//@ Fault-Handling
//		n/a
//---------------------------------------------------------------------
//@ Restrictions
//  	None
//---------------------------------------------------------------------
//@ Invariants
//  	None
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/SerialNumber.ccv   25.0.4.0   19 Nov 2013 13:54:12   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003 By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//	Description:
//		Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 002 By: syw    Date: 22-May-1997   DR Number: DCS 2165
//  	Project:  Sigma (R8027)
//		Description:
//			Moved TI labels.
//
//  Revision: 001  By:  syw    Date: 08-Nov-1996    DR Number: INITIAL RELEASE
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//=====================================================================

#include "SerialNumber.hh"
#include <string.h>

//@ Usage-Classes
//@ End-Usage

//@ Code...

//=====================================================================
//
//      Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SerialNumber
//
//@ Interface-Description
//  	Default constructor.  This method has no arguments and returns nothing.
//		This method will initialize the serial number to "XXXXXXXXXX".
//---------------------------------------------------------------------
//@ Implementation-Description
//		Initialize data_ to "XXXXXXXXXX".
//---------------------------------------------------------------------
//@ PreCondition
//		None
//---------------------------------------------------------------------
//@ PostCondition
//  	None
//@ End-Method
//=====================================================================

SerialNumber::SerialNumber()
{
	CALL_TRACE("SerialNumber::SerialNumber()") ;
	
    // $[TI1]
    strncpy( data_, "XXXXXXXXXX", sizeof( data_)) ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SerialNumber
//
//@ Interface-Description
//  	Constructor.  This method takes a pointer to a char[] as an argument
//		and returns nothing.  This method initializes the serial number
//		to the string passed in.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Initialize data_ to the string passed in.  Null terminate string.
//---------------------------------------------------------------------
//@ PreCondition
//		None
//---------------------------------------------------------------------
//@ PostCondition
//  	None
//@ End-Method
//=====================================================================

SerialNumber::SerialNumber(const char * pString)
{
	CALL_TRACE("SerialNumber::SerialNumber(const char * pString)") ;
    // $[TI2]
    strncpy( data_, pString, sizeof( data_) - 1) ;
    data_[sizeof( data_) - 1] = '\0' ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SerialNumber
//
//@ Interface-Description
//  	Copy constructor.  This method has a SerialNumber as an argument
//		and returns nothing.  
//---------------------------------------------------------------------
//@ Implementation-Description
//		The SerialNumber passed in is copied to the	newly instantiated one.
//---------------------------------------------------------------------
//@ PreCondition
//		None
//---------------------------------------------------------------------
//@ PostCondition
//  	None
//@ End-Method
//=====================================================================

SerialNumber::SerialNumber(const SerialNumber& rSerialNumber)
{
	CALL_TRACE("SerialNumber::SerialNumber(const SerialNumber& rSerialNumber)") ;
		 
    // $[TI3]
    strncpy( data_, rSerialNumber.data_, sizeof( data_)) ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~SerialNumber
//
//@ Interface-Description
//  	Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//		None
//---------------------------------------------------------------------
//@ PreCondition
//  	None
//---------------------------------------------------------------------
//@ PostCondition
//  	None
//@ End-Method
//=====================================================================

SerialNumber::~SerialNumber(void)
{
	CALL_TRACE("SerialNumber::~SerialNumber(void)") ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: operator=
//
//@ Interface-Description
//  	= operator.  This method has a SerialNumber as an argument and
//		returns nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The data members are set equal to the object passed in.
//---------------------------------------------------------------------
//@ PreCondition
//		None
//---------------------------------------------------------------------
//@ PostCondition
//  	None
//@ End-Method
//=====================================================================

SerialNumber &
SerialNumber::operator=(const SerialNumber& rSerialNumber)
{
	CALL_TRACE("SerialNumber::operator=(const SerialNumber& rSerialNumber)") ;
		  
    if (this != &rSerialNumber)
    {
        // $[TI1.1]
        strncpy( data_, rSerialNumber.data_, sizeof( data_));
    }	// implied else $[TI1.2] 

    return (*this);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: operator==
//
//@ Interface-Description
//  	== operator.  This method has a SerialNumber as an argument and
//		returns nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The SerialNumber passed in is compared against the current
//		SerialNumber.  If they are the same return true, else false.
//---------------------------------------------------------------------
//@ PreCondition
//		None
//---------------------------------------------------------------------
//@ PostCondition
//  	None
//@ End-Method
//=====================================================================

Boolean
SerialNumber::operator==(const SerialNumber& rSerialNumber) const
{
	CALL_TRACE("SerialNumber::operator==(const SerialNumber& rSerialNumber)") ;
		 
    // $[TI1.1] FALSE
    // $[TI1.2]	TRUE
  	return (strncmp( data_, rSerialNumber.data_, sizeof( data_)) ? FALSE : TRUE) ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: operator!=
//
//@ Interface-Description
//  	!= operator.  This method has a SerialNumber as an argument and
//		returns nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The SerialNumber passed in is compared against the current
//		SerialNumber.  If they are not the same return true, else false.
//---------------------------------------------------------------------
//@ PreCondition
//		None
//---------------------------------------------------------------------
//@ PostCondition
//  	None
//@ End-Method
//=====================================================================

Boolean
SerialNumber::operator!=(const SerialNumber& rSerialNumber) const
{
	CALL_TRACE("SerialNumber::operator!=(const SerialNumber& rSerialNumber)") ;
		  
    // $[TI1.1] FALSE
    // $[TI1.2]	TRUE
    return( !operator==(rSerialNumber)) ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getString
//
//@ Interface-Description
//  	This method has no arguments and returns a const char * which
//		the serial number is stored.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Returns the data member data_.
//---------------------------------------------------------------------
//@ PreCondition
//  	None
//---------------------------------------------------------------------
//@ PostCondition
//  	None
//@ End-Method
//=====================================================================
const char *
SerialNumber::getString(void) const
{
	CALL_TRACE("SerialNumber::getString(void)") ;

    // $[TI1]
	
    return data_;
}

#ifdef SIGMA_DEVELOPMENT

ostream&
operator<< (ostream& theStream, const SerialNumber& rNumber)
{
	CALL_TRACE("operator<< (ostream& theStream, const SerialNumber& rNumber)") ;
	
    theStream << rNumber.data_;
    
    return (theStream) ;
}

#endif // SIGMA_DEVELOPMENT

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: operator[]
//
//@ Interface-Description
//  	[] operator.  This method has index as an argument and returns the
//		character that the index points to.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The index is checked for bounds and the data_ that the index points
//		to is returned.
//---------------------------------------------------------------------
//@ PreCondition
//		index < SERIAL_NO_SIZE
//---------------------------------------------------------------------
//@ PostCondition
//  	None
//@ End-Method
//=====================================================================
 
char
SerialNumber::operator[](const Uint index) const
{
	CALL_TRACE("SerialNumber::operator[](const Uint index)") ;
	 
    CLASS_ASSERTION( index < SERIAL_NO_SIZE) ;

    // $[TI1]
 
    return( data_[index]) ;
}
 

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//
//@ Interface-Description
//   This method is called when a software fault is detected by the
//   fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//   'lineNumber' are essential pieces of information.  The 'pFileName'
//   and 'pPredicate' strings may be defaulted in the macro to reduce
//   code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This method receives the call for the SoftFault, adds it sub-system
//   and class name ID and sends the message to the non-member function
//   SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
void
SerialNumber::SoftFault(const SoftFaultID  softFaultID,
                      const Uint32       lineNumber,
                      const char*        pFileName,
                      const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
 
  FaultHandler::SoftFault(softFaultID, BD_IO_DEVICES, SERIALNUMBER, 
                          lineNumber, pFileName, pPredicate);
}


//=====================================================================
//
//      Protected Methods...
//
//=====================================================================

//=====================================================================
//
//      Private Methods...
//
//=====================================================================

