#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmtted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: VolumeTargetedManager - Central location to manage volume targeted
//					related data required for Vtpcv or Vsv phases.
//---------------------------------------------------------------------
//@ Interface-Description
//	This class manages all the activities to deliver volume targeted breaths
//	(mandatory or spontaneous).  It provides methods to determine the
//	type of breath to deliver: test breath (volume or pressure based) or
//	volume targeted breath, to reset when volume targeted needs to be
//	restarted, and provides methods to inform this class the status of
//	settings, exhalation triggers, pressures and volumes.
//---------------------------------------------------------------------
//@ Rationale
//	This class provides the high level managegment of volume targeted
//	breaths.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Only VtpcvPhase or VsvPhase can be active at a given time and depending
//	on which one is active, the proper test breaths are scheduled.  The
//	schedulers interface with this class to reset and to update specific
//	data as the events occur.  This class also can reset the
//	VolumeTargetedController at the proper conditions.  During VCP_MAND_TYPE,
//	the testBreathState_ can be VC_BASED, PC_BASED or VT_BASED.  During
//	VSV_SUPPORT_TYPE, the testBreathState_ can be PS_BASED or VS_BASED.
//---------------------------------------------------------------------
//@ Fault-Handling
//	n/a.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header::   /840/Baseline/Breath-Delivery/vcssrc/VolumeTargetedManager.ccv   10.7   08/17/07 09:45:40   pvcs  
//
//@ Modification-Log
//
//  Revision: 009  By:  gfu    Date:  06-May-2002    DR Number: 6006
//  Project:  VCP
//  Description:
//	Created resetAlorithm as calling class' reset method when resetting the algorithm based upon 
//	Hips, Vti alarms, etc. caused too many variables to reset resulting in two test breaths as the algorithm
//	was actualy reset twice.
//		
//  Revision: 008  By:  jja    Date:  02-May-2002    DR Number: 6004
//  Project:  VCP
//  Description:  Reset vtiLastBreath_ in reset(). 
//		
//
//  Revision: 007  By:  jja    Date:  04-Apr-2002    DR Number: 5790
//  Project:  VCP
//  Description:
//		Increased MAX_HCP_COUNTS and MAX_HVTI_COUNTS  to 4.
//
//  Revision: 006  By:  yakovb Date:  22-Mar-2002    DR Number: 5790
//  Project:  VCP
//  Description:
//		Added reset algorithm by VTi alarm during volume targeted breaths.
//
//  Revision: 005  By:  yakovb Date:  08-Mar-2002    DR Number: 5860
//  Project:  VCP
//  Description:
//		Updated to send VTPCV state information, i.e. startup vs normal.
//
//  Revision: 004  By:  quf    Date:  24-Jan-2001    DR Number: 5790
//  Project:  VTPC
//  Description:
//		Added handling of blocked wye during volume targeted breaths.
//
//  Revision: 003  By:  syw    Date:  14-Dec-2000    DR Number: 5826
//       Project:  VTPC
//       Description:
//			Changed suspectCount_ threshold value from 3 to 4.  Use
//				const variable MAX_SUSPECT_COUNTS.
//			Use const variable MAX_HCP_COUNTS for hcpCounter_ threshold.
//			Changed 1.5 * IBW_VALUE for PC test breaths.
//
//  Revision: 002  By:  syw    Date:  08-Nov-2000    DR Number: 5794
//       Project:  VTPC
//       Description:
//			Added requirement tracings.
//			VsvPhase::reinitAlpha() to reset alpha.
//			Changed to 1.5 ml/kg for VS case.
//
//  Revision: 001  By:  syw    Date:  03-Sep-2000    DR Number: 5755
//       Project:  VTPC
//       Description:
//             Initial version
//
//=====================================================================

#include "VolumeTargetedManager.hh"

#include "PhaseRefs.hh"
#include "ControllersRefs.hh"
#include "BreathMiscRefs.hh"

//@ Usage-Classes

#include "MandTypeValue.hh"
#include "SupportTypeValue.hh"
#include "PhasedInContextHandle.hh"
#include "BreathRecord.hh"
#include "LungData.hh"
#include "VolumeTargetedController.hh"
#include "BreathSet.hh"
#include "PatientCctTypeValue.hh"
#include "Peep.hh"
#include "VsvPhase.hh"

//@ End-Usage

//@ Code...

static const Real32 MAX_PEEP_DROP = 10.0 ;
static const Real32 MIN_PATIENT_COMPLIANCE_IBW_FACTOR = 0.1;
static const Real32 MIN_PATIENT_COMPLIANCE_UPPER_LIMIT = 7.0;
static const Real32 MIN_PATIENT_COMPLIANCE_LOWER_LIMIT = 0.15;
static const Real32 MAX_PATIENT_COMPLIANCE_IBW_FACTOR = 2.3;
static const Real32 MAX_COMPLIANCE_INCREASE_FACTOR = 2.0;
static const Uint32 MAX_SUSPECT_COUNTS = 4 ;
static const Uint32 MAX_HCP_COUNTS = 4 ;
static const Uint32 MAX_HVTI_COUNTS = 4 ;



//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: VolumeTargetedManager()  
//
//@ Interface-Description
//		Default Constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  	Data members are initialized via reset() method.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

VolumeTargetedManager::VolumeTargetedManager(void)
{
	CALL_TRACE("VolumeTargetedManager::VolumeTargetedManager(void)") ;

    // $[TI1]
	reset() ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~VolumeTargetedManager()  
//
//@ Interface-Description
//		Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  	none
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

VolumeTargetedManager::~VolumeTargetedManager(void)
{
	CALL_TRACE("VolumeTargetedManager::~VolumeTargetedManager(void)") ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: reset()  
//
//@ Interface-Description
//		This method has no arguments and returns nothing.  This method
//		is called to reset volume targeted breaths (VTPC or VS) to their
//		initial state.
//---------------------------------------------------------------------
//@ Implementation-Description
//  	Initialize the testBreathState_ to the corresponding test breath
//		type and initialize other data members.  Reset
//		VolumeTargetedController.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
void
VolumeTargetedManager::reset( void)
{
	CALL_TRACE("VolumeTargetedManager::reset( void)") ;
	
	// $[VC24012]
	if (PhasedInContextHandle::GetDiscreteValue(SettingId::SUPPORT_TYPE) ==
			SupportTypeValue::VSV_SUPPORT_TYPE)
	{
	    // $[TI1]
		testBreathState_ = PS_BASED ;
	}
	else
	{
	    // $[TI2]
		testBreathState_ = VC_BASED ;
	}
	
	volumeTestBreathCounter_ = 0 ;
	exhTriggerId_ = Trigger::IMMEDIATE_BREATH ;
	beginPlateauPress_ = 0.0 ;
	endPlateauPress_ = 0.0 ;
	hcpCounter_  = 0 ;
	hvtiCounter_ = 0 ;
	mandType_ = MandTypeValue::VCV_MAND_TYPE ;
	supportType_ = SupportTypeValue::OFF_SUPPORT_TYPE ;
	isVolumeTargetedBreath_ = FALSE ;
	endInspPress_ = 0.0 ;
	endExhPress_ = 0.0 ;
	inspLungVolume_ = 0.0 ;
	exhLungVolume_ = 0.0 ;
	vtiLastBreath_ = FALSE;
	
	RVolumeTargetedController.reset() ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: resetAlgorithm()  
//
//@ Interface-Description
//		This method has no arguments and returns nothing.  This method
//		is called to reset the volume targeted phase based upon
//		internally genearted conditions.
//---------------------------------------------------------------------
//@ Implementation-Description
//  	Initialize data members and reset VolumeTargetedController.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
void
VolumeTargetedManager::resetAlgorithm( void)
{

	volumeTestBreathCounter_ = 0 ;
	beginPlateauPress_ = 0.0 ;
	endPlateauPress_ = 0.0 ;
	hcpCounter_  = 0 ;
	hvtiCounter_ = 0 ;
	endInspPress_ = 0.0 ;
	endExhPress_ = 0.0 ;
	inspLungVolume_ = 0.0 ;
	exhLungVolume_ = 0.0 ;
	vtiLastBreath_ = FALSE;
	
	RVolumeTargetedController.reset() ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: determineBreathType()  
//
//@ Interface-Description
//		This method has a reference to a pointer to the current breath
//		phase and a reference to the mandatory type as arguments and
//		returns nothing.  This class is called by the scheduler class
//		to determine the type of breath to deliver (test breath or volume
//		targeted).
//---------------------------------------------------------------------
//@ Implementation-Description
//		If this method is called, it must mean that either mandType_ is
//		VCP_MAND_TYPE or supportType_ is VSV_SUPPORT_TYPE.  The arguments
//		passed in are updated with the breath type to deliver.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
void
VolumeTargetedManager::determineBreathType( BreathPhase* &pBreathPhase,
								 DiscreteValue &mandatoryType)
{
	CALL_TRACE("VtpcvPhase::determineBreathType( BreathPhase* &pBreathPhase, \
									DiscreteValue &mandatoryType)") ;

	const Real32 IBW_VALUE =
				PhasedInContextHandle::GetBoundedValue(SettingId::IBW).value ;

	// $[VC24001]
	endExhPress_ = BreathRecord::GetEndExpPressureComp() ;

	// at this point RPeep.updatePeep() has not been called yet
	Real32 prevPeep = RPeep.getActualPeep() ;
	Real32 newPeep = PhasedInContextHandle::GetBoundedValue( SettingId::PEEP).value ;
		
	// $[VC24003]
	const Real32 PATIENT_COMPLIANCE = getInspLungVolumeLimited() / getDeltaPLimited() ;

	calcMinAndMaxPatientCompliance_(IBW_VALUE);

	// the next inspiration is a volume targeted breath or a test breath
	isVolumeTargetedBreath_ = TRUE ;

	if (mandType_ == MandTypeValue::VCP_MAND_TYPE)
	{
	    // $[TI1]
		// $[VC24017]
		// $[VC24018]
		Boolean volumeCondition = (testBreathState_ == VC_BASED &&
					exhTriggerId_ == Trigger::IMMEDIATE_EXP &&
					endInspPress_ - endExhPress_ >= 1.5 &&
					beginPlateauPress_ - endPlateauPress_ >= -1.0 &&
					PATIENT_COMPLIANCE >= minPatientCompliance_ ) ;

		Boolean pressureCondition = ( testBreathState_ == PC_BASED &&
					exhTriggerId_ == Trigger::IMMEDIATE_EXP	&&
					inspLungVolume_ >= 1.5 * IBW_VALUE &&
					PATIENT_COMPLIANCE >= minPatientCompliance_ ) ;
				
		// $[VC24013e]
		// $[VC24013d]
		// $[VC24013f]
		// $[VC24013g]

		if (hcpCounter_ >= MAX_HCP_COUNTS || hvtiCounter_ >= MAX_HVTI_COUNTS || RVolumeTargetedController.getSuspectCount() >= MAX_SUSPECT_COUNTS ||
				prevPeep - newPeep > MAX_PEEP_DROP ||
				neoComplianceIncreaseExcessive_( PATIENT_COMPLIANCE) )
		{
		    // $[TI2]
			testBreathState_ = VC_BASED ;
			resetAlgorithm() ;
		}	

		else if (volumeCondition || pressureCondition)
		{
		    // $[TI3]
			testBreathState_ = VT_BASED ;
		}
		else if (volumeTestBreathCounter_ >= 3)
		{
		    // $[TI4]
			// $[VC24017]
			// $[VC24015] 
			testBreathState_ = PC_BASED ;
		}
	    // $[TI5]


		switch (testBreathState_)
		{
			case VC_BASED:
			    // $[TI6]
				pBreathPhase = (BreathPhase*)&RVcvPhase ;
				mandatoryType = MandTypeValue::VCV_MAND_TYPE ;
				volumeTestBreathCounter_++ ;
				break ;

			case PC_BASED:
			    // $[TI7]
				pBreathPhase = (BreathPhase*)&RPcvPhase ;
				mandatoryType = MandTypeValue::PCV_MAND_TYPE ;
				volumeTestBreathCounter_ = 0 ;
				break ;

			case VT_BASED:
			    // $[TI8]
				pBreathPhase = (BreathPhase*)&RVtpcvPhase ;
				mandatoryType = MandTypeValue::VCP_MAND_TYPE ;
				volumeTestBreathCounter_ = 0 ;
				break ;

			default:
				AUX_CLASS_ASSERTION_FAILURE( testBreathState_) ;
				break ;
		}
	}
	else if (supportType_ == SupportTypeValue::VSV_SUPPORT_TYPE)
	{
	    // $[TI9]
		// $[VC24013e]
		// $[VC24013d]
		// $[VC24013f]
		// $[VC24013g]
		// $[VC24018]
		if (hcpCounter_ >= MAX_HCP_COUNTS || hvtiCounter_ >= MAX_HVTI_COUNTS || RVolumeTargetedController.getSuspectCount() >= MAX_SUSPECT_COUNTS ||
				prevPeep - newPeep > MAX_PEEP_DROP ||
				neoComplianceIncreaseExcessive_( PATIENT_COMPLIANCE) )
		{
		    // $[TI10]
			testBreathState_ = PS_BASED ;
			resetAlgorithm() ;
		}
		else if (testBreathState_ == PS_BASED &&
					(exhTriggerId_ == Trigger::DELIVERED_FLOW_EXP ||
							exhTriggerId_ == Trigger::PRESSURE_EXP) &&
							inspLungVolume_ >= 1.5 * IBW_VALUE &&
							PATIENT_COMPLIANCE >= minPatientCompliance_ )
		{
		    // $[TI11]
			// $[VC24017]
			testBreathState_ = VS_BASED ;
		}
	    // $[TI12]

		switch (testBreathState_)
		{
			case PS_BASED:
			    // $[TI13]
				pBreathPhase = (BreathPhase*)&RPsvPhase ;
				mandatoryType = ::NULL_MANDATORY_TYPE ;

				// $[VC24006]
				RVsvPhase.reinitAlpha() ;
				break ;

			case VS_BASED:
			    // $[TI14]
				pBreathPhase = (BreathPhase*)&RVsvPhase ;
				mandatoryType = ::NULL_MANDATORY_TYPE ;
				break ;

			default:
				AUX_CLASS_ASSERTION_FAILURE( testBreathState_) ;
				break ;
		}				
	}
	else
	{
		// this method only called only if VCP_MAND_TYPE or VSV_SUPPORT_TYPE active 
		CLASS_ASSERTION_FAILURE() ;
	}
	MsgQueue::PutMsg(::BREATH_DATA_Q, BdQueuesMsg::SEND_VTPCV_STATE);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateData()  
//
//@ Interface-Description
//		This method has the mandatory type and the support type as arguments
//		and returns nothing.  This method is called by the scheduler class
//		at the start of inspiration to notify this class of the current
//		settings and that the previous breath is complete and a new inspiration
//		is about to begin.
//---------------------------------------------------------------------
//@ Implementation-Description
//		If the last inspiration was a volume targeted breath, record the
//		plateau pressures and inspired and expired lng volumes.  Reset if
//		there was a mandatory type or support type setting change.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
VolumeTargetedManager::updateData( const DiscreteValue mandType,
							const DiscreteValue supportType)
{
	CALL_TRACE("VolumeTargetedManager::updateData( const DiscreteValue mandType, \
							const DiscreteValue supportType)") ;

	// store data if last breath was a volume targeted breath or test breath
	if (isVolumeTargetedBreath_)
	{
	    // $[TI1]
		isVolumeTargetedBreath_ = FALSE ;

		// $[VC24001]
		// $[VC24002]
		endInspPress_ = BreathRecord::GetEndInspPressureComp() ;

		BreathRecord* pCurrentRecord = RBreathSet.getCurrentBreathRecord() ;
		
		inspLungVolume_ = pCurrentRecord->getInspiredVolume() ;

		Real32 inspTime = BreathRecord::GetInspiratoryTime() ;

		const DiscreteValue CIRCUIT_TYPE =
				PhasedInContextHandle::GetDiscreteValue(SettingId::PATIENT_CCT_TYPE) ;
		
		if (CIRCUIT_TYPE != PatientCctTypeValue::NEONATAL_CIRCUIT &&
				(pCurrentRecord->getMandatoryType() == MandTypeValue::PCV_MAND_TYPE ||
					pCurrentRecord->getMandatoryType() == MandTypeValue::VCP_MAND_TYPE))
   		{
		    // $[TI5]
			inspTime *= PCV_TIME_FACTOR ;
		}
	    // $[TI6]
         		
		inspLungVolume_ = pCurrentRecord->calculateComplianceBtpsVol(
				inspLungVolume_, endInspPress_, endExhPress_,
				(Int32)inspTime, BreathRecord::INSP_BTPS) ;

		// $[VC24004]
      	exhLungVolume_ = pCurrentRecord->calculateComplianceBtpsVol(
      			pCurrentRecord->getExpiredVolume(), endInspPress_, endExhPress_,(Int32) inspTime) ;
	}
    // $[TI2]

	// $[VC24013c]
	if (mandType == MandTypeValue::VCP_MAND_TYPE &&
				mandType_ != MandTypeValue::VCP_MAND_TYPE ||
			supportType == SupportTypeValue::VSV_SUPPORT_TYPE &&
				supportType_ != SupportTypeValue::VSV_SUPPORT_TYPE)
	{
	    // $[TI3]
			reset() ;
	}
    // $[TI4]
	
	mandType_ = mandType ;
	supportType_ = supportType ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setExhalationTrigger()  
//
//@ Interface-Description
//		This method has the trigger id that caused the inspiration to
//		terminate and a pointer to the current breath phase as arguments.
//		This method is called by determineBreathPhase() of the current
//		scheduler class.
//---------------------------------------------------------------------
//@ Implementation-Description
//		If the last inspiration was a volume targeted breath, record the
//		trigger id that caused the inspiration to end.  If it was due to
//		a high circuit pressure in VtpcvPhase or VsvPhase, increment the
//		counter. If it was due to a VTi alarm in VtpcvPhase or VsvPhase, 
//		increment the counter.

//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
void
VolumeTargetedManager::setExhalationTrigger( const Trigger::TriggerId triggerId,
							const BreathPhase* pBreathPhase)
{
	CALL_TRACE("VolumeTargetedManager::setExhalationTrigger( \
					const Trigger::TriggerId triggerId, \
					const BreathPhase* pBreathPhase)") ;

	vtiLastBreath_ = FALSE;
	if (isVolumeTargetedBreath_)
	{
	    // $[TI1]
		// $[VC24013e]

		if (triggerId == Trigger::HIGH_CIRCUIT_PRESSURE_EXP)
		{
		    // $[TI2]

			if (pBreathPhase == (BreathPhase*)&RVsvPhase ||
					pBreathPhase == (BreathPhase*)&RVtpcvPhase)
			{
			    // $[TI3]
				hcpCounter_++ ;
			}
		}
		else
		{
		    // $[TI4]
			hcpCounter_ = 0 ;
		}
	    // $[TI5]
		
		if (triggerId == Trigger::LUNG_VOLUME_EXP)
		{
			if (pBreathPhase == (BreathPhase*)&RVsvPhase ||
					pBreathPhase == (BreathPhase*)&RVtpcvPhase)
			{
				hvtiCounter_++ ;
				vtiLastBreath_ = TRUE;
			}
		}
		else
		{
			hvtiCounter_ = 0 ;
		}
	
		exhTriggerId_ =	triggerId ;
	}
	// $[TI6]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
VolumeTargetedManager::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  	FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, VOLUMETARGETEDMANAGER,
                          lineNumber, pFileName, pPredicate);
}

//=====================================================================
//
//  Protected Methods...
//
//=====================================================================
//=====================================================================
//
//  Private Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: neoComplianceIncreaseExcessive_()
//
//@ Interface-Description
//	This method takes the patient compliance as an argument and
//	returns a Boolean indicating if an excessive compliance increase
//	occurred for a neonatal patient during VC+ or VS breaths.
//---------------------------------------------------------------------
//@ Implementation-Description
//	If circuit type is neonatal, last volume targeted breath was not
//	a test breath, and the new patient compliance is more than
//	MAX_COMPLIANCE_INCREASE_FACTOR times the previous filtered
//	patient compliance, then return TRUE; else return FALSE.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Boolean
VolumeTargetedManager::neoComplianceIncreaseExcessive_(
										Real32 patientCompliance ) const
{
	CALL_TRACE("VolumeTargetedManager::neoComplianceIncreaseExcessive_( \
										Real32 patientCompliance )") ;

	const DiscreteValue CIRCUIT_TYPE =
			PhasedInContextHandle::GetDiscreteValue(SettingId::PATIENT_CCT_TYPE);

	// $[VC24013g]
	const Boolean IS_CRITERIA_MET =
		CIRCUIT_TYPE == PatientCctTypeValue::NEONATAL_CIRCUIT &&
		(testBreathState_ == VT_BASED || testBreathState_ == VS_BASED) &&
		patientCompliance > MAX_COMPLIANCE_INCREASE_FACTOR *
								RVolumeTargetedController.getCPatientFiltered() ;
	// $[TI1] TRUE
	// $[TI2] FALSE

	return ( IS_CRITERIA_MET);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: calcMinAndMaxPatientCompliance_()
//
//@ Interface-Description
//	This method takes patient IBW as an argument and has no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Calculate and store the minimum and maximum patient compliance values.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
VolumeTargetedManager::calcMinAndMaxPatientCompliance_( Real32 ibw)
{
	CALL_TRACE("VolumeTargetedManager::calcMinAndMaxPatientCompliance_( \
														Real32 ibw)") ;

	// $[VC24004]
	minPatientCompliance_ = MIN_PATIENT_COMPLIANCE_IBW_FACTOR * ibw;

	if (minPatientCompliance_ > MIN_PATIENT_COMPLIANCE_UPPER_LIMIT)
	{
	// $[TI1]
		minPatientCompliance_ = MIN_PATIENT_COMPLIANCE_UPPER_LIMIT;
	}
	else if (minPatientCompliance_ < MIN_PATIENT_COMPLIANCE_LOWER_LIMIT)
	{
	// $[TI2]
		minPatientCompliance_ = MIN_PATIENT_COMPLIANCE_LOWER_LIMIT;
	}		
	// $[TI3]

	// $[VC24005]
	maxPatientCompliance_ = MAX_PATIENT_COMPLIANCE_IBW_FACTOR * ibw;
}


