#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sotred in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//============================ M O D U L E   D E S C R I P T I O N ====
//@ Class: CodeLogEntry - Entry for an Diagnostic Code Log.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is an entry item for the Diagnostic Code Logs.  Instances of
//  this class contain diagnostic information, and a time-stamp for
//  the time of the event.  This class is used only by the Diagnostic
//  Code Logs, and has no multi-tasking issues.
//---------------------------------------------------------------------
//@ Rationale
//  This is a common class to be used by all of the diagnostic code
//  logs.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class contains a time-stamp for recording when a diagnostic
//  code is logged, a diagnostic code for indicating what type of
//  diagnostic event occurred, and a CRC value for validating the
//  integrity of this entry.
//
//  $[00613] -- system diagnostic information, including a time-stamp...
//  $[00617] -- comm. diagnostic information, including a time-stamp...
//  $[00645] -- EST/SST diagnostic information, including a time-stamp...
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//  (isClear()  ||  isValid())
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Persistent-Objects/vcssrc/CodeLogEntry.ccv   25.0.4.0   19 Nov 2013 14:18:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By:  sah    Date:  01-Jan-1999    DCS Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' to a non-inlined method.
//
//  Revision: 001  By:  sah    Date:  15-Mar-1997    DCS Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//          Initial version (Integration baseline).
//
//=====================================================================

#include "CodeLogEntry.hh"

//@ Usage-Classes...
#include "NovRamManager.hh"
#include "TimeStamp.hh"
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  CodeLogEntry()  [Default Constructor]
//
//@ Interface-Description
//  Construct a default log entry.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  (isClear()  &&  !isValid())
//@ End-Method
//=====================================================================

CodeLogEntry::CodeLogEntry(void)
			   : compilerPadding_(0xff),
			     entryCrc_(0u)  // some illegal value...
{
  CALL_TRACE("CodeLogEntry()");

  // make sure that there is no unexpected "padding" between the data
  // members of this class...
  SAFE_AUX_CLASS_ASSERTION((sizeof(CodeLogEntry) ==
		      (sizeof(sequenceNumber_) + sizeof(whenOccurred_) +
		       sizeof(diagnosticCode_) + sizeof(compilerPadding_) +
		       sizeof(entryCrc_))),
			   sizeof(CodeLogEntry));

  // clear this entry...
  clear();

  SAFE_CLASS_INVARIANT((isClear()));
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ~CodeLogEntry()  [Destructor]
//
//@ Interface-Description
//  Destroy this log entry.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

CodeLogEntry::~CodeLogEntry(void)
{
  CALL_TRACE("~CodeLogEntry()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  isCorrected()  [const]
//
//@ Interface-Description
//  Is this a "corrected" entry?
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition 
//  none
//@ End-Method 
//===================================================================== 

Boolean
CodeLogEntry::isCorrected(void) const
{
  CALL_TRACE("isCorrected()");

  Boolean  isEntryCorrected = FALSE;

  if (diagnosticCode_.getDiagCodeTypeId() ==
					DiagnosticCode::SYSTEM_EVENT_DIAG)
  {   // $[TI1]
    const DiagnosticCode::SystemEventBits  SYSTEM_EVENT_BITS =
				      diagnosticCode_.getSystemEventCode();

    // $[TI1.1] (TRUE)  $[TI1.2] (FALSE)...
    isEntryCorrected = (SYSTEM_EVENT_BITS.systemEventId ==
				  DiagnosticCode::DIAG_LOG_ENTRY_CORRECTED);
  }   // $[TI2]

  return(isEntryCorrected);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  correct()
//
//@ Interface-Description
//  Set this entry to a "corrected" state.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition 
//  (isCorrected())
//@ End-Method 
//===================================================================== 

void
CodeLogEntry::correct(void)
{
  CALL_TRACE("correct()");

  DiagnosticCode  corruptedDiagCode;

  corruptedDiagCode.setSystemEventCode(
  				DiagnosticCode::DIAG_LOG_ENTRY_CORRECTED
				      );

  logDiagnosticCode(corruptedDiagCode);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  logDiagnosticCode(newDiagnosticCode)
//
//@ Interface-Description
//  Log the occurrence of the diagnosic event indicated by
//  'newDiagnosticCode'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  (!isClear())
//@ End-Method
//=====================================================================

void
CodeLogEntry::logDiagnosticCode(const DiagnosticCode& newDiagnosticCode)
{
  CALL_TRACE("logDiagnosticCode(newDiagnosticCode)");

  sequenceNumber_ = NovRamManager::GetNewSequenceNumber();

  whenOccurred_.now();  // set the time stamp to "now"...

  // initialize this code log entry with the given fault code...
  diagnosticCode_ = newDiagnosticCode;  // store the fault code...

  // calculate, and store, this entry's CRC value...
  entryCrc_ = ::CalculateCrc((const Byte*)this, getCrcOffset_());

  SAFE_CLASS_INVARIANT((isValid()));
}   // $[TI1]



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  operator=(logEntry)  [Assignment Operator]
//
//@ Interface-Description
//  Copy 'logEntry' into this log entry.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
CodeLogEntry::operator=(const CodeLogEntry& logEntry)
{
  CALL_TRACE("operator=(logEntry)");
  SAFE_CLASS_INVARIANT((isClear()  ||  isValid()));

  sequenceNumber_  = logEntry.sequenceNumber_;
  whenOccurred_    = logEntry.whenOccurred_;
  diagnosticCode_  = logEntry.diagnosticCode_;
  compilerPadding_ = logEntry.compilerPadding_;
  entryCrc_        = logEntry.entryCrc_;

  SAFE_CLASS_INVARIANT((isClear()  ||  isValid()));
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  operator>(logEntry)  [const]
//
//@ Interface-Description
//  Is this entry greater-than 'logEntry'?  An entry is "greater-than"
//  another log entry when it was logged at a later time than the other
//  entry (i.e., when it is more recent).
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//  When an entry has an "invalid" time stamp, it is automatically
//  considered "more recent".  This is because it will better preserve
//  the order of diagnostic events, than considering "invalid" time stamps
//  as the least recent.
//--------------------------------------------------------------------- 
//@ PreCondition
//  (!isClear()  &&  !logEntry.isClear())
//---------------------------------------------------------------------
//@ PostCondition 
//  none
//@ End-Method 
//===================================================================== 

Boolean
CodeLogEntry::operator>(const CodeLogEntry& logEntry) const
{
  CALL_TRACE("operator>(logEntry)");
  AUX_CLASS_PRE_CONDITION((!isClear()  &&  !logEntry.isClear()),
  			  !isClear());

  Boolean  thisIsMoreRecent;

  if (whenOccurred_.isInvalid())
  {   // $[TI1] -- this time stamp is invalid; consider more recent...
    thisIsMoreRecent = TRUE;
  }
  else if (logEntry.whenOccurred_.isInvalid())
  {   // $[TI2] -- that time stamp is invalid; consider more recent...
    thisIsMoreRecent = FALSE;
  }
  else
  {   // $[TI3] -- neither time stamp is invalid; test relative time...
    // $[TI3.1] (TRUE)  $[TI3.2] (FALSE)...
    thisIsMoreRecent = (whenOccurred_ > logEntry.whenOccurred_);
  }

  return(thisIsMoreRecent);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  operator==(logEntry)  [const]
//
//@ Interface-Description
//  Is 'logEntry' equivalent to this log entry?
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition 
//  none
//@ End-Method 
//===================================================================== 

Boolean
CodeLogEntry::operator==(const CodeLogEntry& logEntry) const
{
  CALL_TRACE("operator==(logEntry)");

  // either both are "cleared", or the elements of both are equal...
  const Boolean  IS_EQUIV_FLAG = 
			 ((isClear()  &&  logEntry.isClear())  ||
			  (sequenceNumber_ == logEntry.sequenceNumber_  &&
			   entryCrc_       == logEntry.entryCrc_        &&
			   diagnosticCode_ == logEntry.diagnosticCode_  &&
			   whenOccurred_   == logEntry.whenOccurred_));

  return(IS_EQUIV_FLAG);
}   // $[TI1] (TRUE)  $[TI2] (FALSE)


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                              [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//  and 'lineNumber' are essential pieces of information.  The
//  'pFileName' and 'pPredicate' strings may be defaulted in the macro
//  to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
CodeLogEntry::SoftFault(const SoftFaultID  softFaultID,
                        const Uint32       lineNumber,
                        const char*        pFileName,
                        const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, PERSISTENT_OBJS, CODE_LOG_ENTRY,
                          lineNumber, pFileName, pPredicate);
}


//=====================================================================
//
//  Friend Functions...
//
//=====================================================================

#if defined(SIGMA_DEVELOPMENT)

//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
// Free-Function: operator<<(ostr, logEntry)  [friend]
//
// Interface-Description
//  Dump the contents of 'logEntry' out to 'ostr'.
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Free-Function
//=====================================================================

Ostream&
operator<<(Ostream& ostr, const CodeLogEntry& logEntry)
{
  CALL_TRACE("operator<<(ostr, logEntry)");

  if (!logEntry.isClear())
  {
#if defined(SIGMA_GUI_CPU)
    char  diagString[32];

    logEntry.diagnosticCode_.getDiagCodeString(diagString);
#endif // defined(SIGMA_GUI_CPU)

    TimeOfDay  timeOfDay = logEntry.whenOccurred_.getTimeOfDay();

    ostr << "{"   << timeOfDay.month
	 << "-"   << timeOfDay.date
	 << "-"   << timeOfDay.year
	 << " @ " << timeOfDay.hour
	 << ":"   << timeOfDay.minute
	 << ":"   << timeOfDay.second
#if defined(SIGMA_GUI_CPU)
	 << ", "  << diagString
#endif // defined(SIGMA_GUI_CPU)
	 << " ("  << (void*)((Uint)logEntry.entryCrc_)
	 << ")}";
  }
  else
  {
    ostr << "{CLEARED}";
  }

  return(ostr);
}

#endif  // defined(SIGMA_DEVELOPMENT)
