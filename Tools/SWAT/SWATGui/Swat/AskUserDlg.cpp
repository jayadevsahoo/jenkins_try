// AskUserDlg.cpp : implementation file
//

#include "stdafx.h"
#include "AskUserDlg.h"
#include "Swat.h"


// CAskUserDlg dialog

IMPLEMENT_DYNAMIC(CAskUserDlg, CDialog)

CAskUserDlg::CAskUserDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CAskUserDlg::IDD, pParent)
{

}

CAskUserDlg::~CAskUserDlg()
{
}

void CAskUserDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CAskUserDlg, CDialog)
	ON_BN_CLICKED(IDC_BUTTON_STOP_SCRIPT, &CAskUserDlg::OnBnClickedButtonStopScript)
	ON_BN_CLICKED(IDC_BUTTON1, &CAskUserDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &CAskUserDlg::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON3, &CAskUserDlg::OnBnClickedButton3)
	ON_BN_CLICKED(IDC_BUTTON4, &CAskUserDlg::OnBnClickedButton4)
END_MESSAGE_MAP()


// CAskUserDlg message handlers

BOOL CAskUserDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	GetDlgItem(IDC_BUTTON1)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_BUTTON2)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_BUTTON3)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_BUTTON4)->ShowWindow(SW_HIDE);

	GetDlgItem(IDC_STATIC)->SetWindowTextA(m_strMessage);

	if(!m_strButton1.IsEmpty())
	{
		GetDlgItem(IDC_BUTTON1)->ShowWindow(SW_RESTORE);
		GetDlgItem(IDC_BUTTON1)->SetWindowTextA(m_strButton1);
	}

	if(!m_strButton2.IsEmpty())
	{
		GetDlgItem(IDC_BUTTON2)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_BUTTON2)->SetWindowTextA(m_strButton2);
	}

	if(!m_strButton3.IsEmpty())
	{
		GetDlgItem(IDC_BUTTON3)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_BUTTON3)->SetWindowTextA(m_strButton3);
	}

	if(!m_strButton4.IsEmpty())
	{
		GetDlgItem(IDC_BUTTON4)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_BUTTON4)->SetWindowTextA(m_strButton4);
	}


	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CAskUserDlg::OnBnClickedButtonStopScript()
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	pApp->LogStatus("-->Stop triggered by the user. Please wait...");

	pApp->m_bRun = false; //stop running script
	pApp->m_bUserStopped = true;

	pApp->m_bRecord = false; //stop recording

	UpdateData(true);
	OnOK();
}

void CAskUserDlg::OnBnClickedButton1()
{
	m_nReturn = 1;
	OnOK();
	
}

void CAskUserDlg::OnBnClickedButton2()
{
	m_nReturn = 2;
	OnOK();
	
}

void CAskUserDlg::OnBnClickedButton3()
{
	m_nReturn = 3;
	OnOK();
	
}

void CAskUserDlg::OnBnClickedButton4()
{
	m_nReturn = 4;
	OnOK();
}


//to disable escape or close x on the dlg box
void CAskUserDlg::OnCancel()
{
	// TODO: Add your specialized code here and/or call the base class

//	CDialog::OnCancel();
}
