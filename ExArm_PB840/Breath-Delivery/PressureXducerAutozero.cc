#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: PressureXducerAutozero - Performs the pressure transducer
//			autozero maneuver.
//---------------------------------------------------------------------
//@ Interface-Description
//		Methods are implemented to get the autozero in progress status,
//		to manage the autozero timer when the timer has expired, to open
//		the autozero solenoids and to initialize the maneuver, to perform
//		the maneuver every BD cycle during exhalation, and to obtain an initial
//		sample of the zero offset.  The later method is intended to be executed
//		following POST.
//---------------------------------------------------------------------
//@ Rationale
//		This class implements the algorithms to perform the pressure
//		transducer autozero maneuver.
//---------------------------------------------------------------------
//@ Implementation-Description
//		An instance of an IntervalTimer is used to schedule the autozero
//		events.  Once the timer has expired, a flag is set and a new target
//		time corresponding to the next scheduled autozero is set for the
//		timer.  At the start of the next exhalation, the solenoids (inspiration
//		and exhalation) are opened and a flag for autozeroInProgress is set.
//		Data is sampled	during a specified range in time since the beginning
//		and an average value is obtained.  The zero offset must fall within
//		specific ranges in order for it to be valid.  Once the maneuver is
//		completed, the autozero solenoids are closed and the autozeroInProgress
//		flag is cleared.
//
//		Following POST, a zero offset value must be known before ventilating.
//		A method is implemented that samples the zero offset value and accepts the
//		value if it is within the specified range.  Since the BD Task is not active at
//		this time, this method uses the MsgQueue to establish a 5 ms clock.
//---------------------------------------------------------------------
//@ Fault-Handling
//		N/A
//---------------------------------------------------------------------
//@ Restrictions
//		none
//---------------------------------------------------------------------
//@ Invariants
//		none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/PressureXducerAutozero.ccv   25.0.4.0   19 Nov 2013 14:00:06   pvcs  $
//
//@ Modification-Log
//
//  Revision: 011  By: erm     Date:  22-Nov-2002    DR Number: 6028
//  Project:  EMI
//  Description: 
//               Allow 3 strikes for out of range/drift faults
//
//  Revision: 010  By: syw/jja Date:  07-Apr-2000    DR Number: 5699
//  Project:  NeoMode
//  Description:
//      Changed error code to be able to decode whether a failure is due to
//		OOR or drift failure.
//
//  Revision: 009  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 008 By: iv     Date: 10-Mar-1998   DR Number: DCS 5041
//  	Project:  Sigma (R8027)
//		Description:
//			Added error code to the fault report.
//
//  Revision: 007 By: syw    Date: 10-Sep-1997   DR Number: DCS 2421
//  	Project:  Sigma (R8027)
//		Description:
//			Initialize autozeroSide_.  Added logic to autozero insp side
//			first and then exh side on the following newBreath() call.
//
//  Revision: 006  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 005 By: syw    Date: 03-Feb-1997   DR Number: DCS 1700
//  	Project:  Sigma (R8027)
//		Description:
//			Added calls to rWriteIOPort.writeNewCycle() since
//			CycleTimer::SignalNewCycle() is not running yet which
//			would have updated the bit access register.
//
//  Revision: 004 By: syw    Date: 27-Feb-1996   DR Number: DCS 600
//  	Project:  Sigma (R8027)
//		Description:
//			Initialize filtered pressure values to current value once
//			autozero is complete.
//
//  Revision: 003 By: syw    Date: 27-Feb-1996   DR Number: DCS 600
//  	Project:  Sigma (R8027)
//		Description:
//			Made changes to handle SafetyNetTests design of reporting
//			autozero failures independent of side (insp or exh).
//			Previous design had the failures coupled. 
//
//  Revision: 002 By: syw    Date: 21-Feb-1996   DR Number: DCS 674
//  	Project:  Sigma (R8027)
//		Description:
//			Removed #ifndef around #include.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "PressureXducerAutozero.hh"

#include "ValveRefs.hh"
#include "IpcIds.hh"
#include "SafetyNetRefs.hh"
#include "MainSensorRefs.hh"
#include "RegisterRefs.hh"

//@ Usage-Classes

#include "MsgQueue.hh"
#include "Solenoid.hh"
#include "PressureSensor.hh"
#include "IntervalTimer.hh"
#include "AdcChannels.hh"
#include "Trigger.hh"
#include "SensorMediator.hh"
#include "SafetyNetTestMediator.hh"
#include "BD_IO_Devices.hh"
#include "BitAccessGpio.hh"
#include "SoftwareOptions.hh"

#ifdef INTEGRATION_TEST_ENABLE
#if defined (SIGMA_BD_CPU)
#include "EventManager.h"
#endif // SIGMA_BD_CPU
#endif // INTEGRATION_TEST_ENABLE

//@ End-Usage

//@ Code...

#ifdef SIGMA_PAV_UNIT_TEST
extern Uint32 ErrorCode_UT ;
#endif // SIGMA_PAV_UNIT_TEST

// $ [04059]
//@ Constant: LESS_THAN_20_MIN_INTERVAL
// frequency of autozero for 1st 20 minutes is every minute
static const Int32 LESS_THAN_20_MIN_INTERVAL = 1 * 60 * 1000  ;

//@ Constant: LESS_THAN_60_MIN_INTERVAL
// frequency of autozero for up to 60 minutes is every 2 minutes
static const Int32 LESS_THAN_60_MIN_INTERVAL = 2 * 60 * 1000 ;

//@ Constant: GREATER_THAN_60_MIN_INTERVAL
// frequency of autozero after 60 minutes is every 5 minutes
static const Int32 GREATER_THAN_60_MIN_INTERVAL = 5 * 60 * 1000 ;

//@ Constant: TWENTY_MINUTES
// 20 minutes in millisecs
static const Int32 TWENTY_MINUTES = 20 * 60 * 1000 ;

//@ Constant: SIXTY_MINUTES
// 60 minutes in millisecs
static const Int32 SIXTY_MINUTES = 60 * 60 * 1000 ;

// $[04062]
//@ Constant: BEGIN_SAMPLING
// time (in msec) after opening solenoids to begin sampling
static const Int32 BEGIN_SAMPLING = 35 ;

// $[04063]
//@ Constant: END_SAMPLING
// time (in msec) after opening solenoids to end sampling
static const Int32 END_SAMPLING = 65 ;

// $[04064]
//@ Constant: MANEUVER_COMPLETE
// time (in msec) when maneuver is complete
static const Int32 MANEUVER_COMPLETE = 100 ;

//@ Constant:MAX_AZ_CYCLES_00R
// max consecutive faults in AZ readings
static const Uint16 MAX_AZ_CYCLES_OOR = 3;

//@ Constant:MFG_MAX_AZ_CYCLES_OOR
static const Uint16 MFG_MAX_AZ_CYCLES_OOR = 1;

//=====================================================================
//
//  Public Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: PressureXducerAutozero()
//
//@ Interface-Description
//		Constructor.  This method has an IntervalTimer& as an argument which
//		will be used to to time the next autozero event.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The data member rPressureXducerAutozeroTimer_ is initialized
//		to the value passed in.  Other data members are also inititialized
//		to ensure proper execution of this class.  The time	interval for the
//		timer is set and the timer is started.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

PressureXducerAutozero::PressureXducerAutozero( IntervalTimer& rTimer)
: TimerTarget(),
  rPressureXducerAutozeroTimer_(rTimer)		// $[TI1]
{
	CALL_TRACE("PressureXducerAutozero::PressureXducerAutozero( IntervalTimer& rTimer)") ;

	// $[TI2]
	timeSincePostMs_ = 0 ;
	numInspAZCyclesDriftOOR_ = 0;
	numExhAZCyclesDriftOOR_ = 0;
	numInspAZCyclesRangeOOR_ = 0;
	numExhAZCyclesRangeOOR_ = 0;
	autozeroIntervalMs_ = LESS_THAN_20_MIN_INTERVAL ;
	isTimeToAutozero_ = FALSE ;
	isAutozeroInProgress_ = FALSE ;

	rPressureXducerAutozeroTimer_.setTargetTime( autozeroIntervalMs_) ;
	rPressureXducerAutozeroTimer_.restart() ;
	autozeroSide_ = INSP ;
     
	if ((SoftwareOptions::GetSoftwareOptions().getDataKeyType()) ==
		  SoftwareOptions::MANUFACTURING)
	  {
		 allowableZeroShift_ = MFG_ALLOWABLE_ZERO_SHIFT;
		 maxAZCyclesOOR_ = MFG_MAX_AZ_CYCLES_OOR;
	  }
	else
	 {
		 allowableZeroShift_ = ALLOWABLE_ZERO_SHIFT;
		 maxAZCyclesOOR_ = MAX_AZ_CYCLES_OOR;
	 }
		
	
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~PressureXducerAutozero()
//
//@ Interface-Description
//		Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//		none
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

PressureXducerAutozero::~PressureXducerAutozero(void)
{
	CALL_TRACE("PressureXducerAutozero::~PressureXducerAutozero(void)") ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: timeUpHappened()
//
//@ Interface-Description
//		This method has an IntervalTimer& as an argument and has no return
//		value.  This method is called when the IntervalTimer passed in as
//		an argument has expired.
//---------------------------------------------------------------------
//@ Implementation-Description
//		timeSincePostMs_ is updated and the next time interval
//		for the next autozero is determined based on timeSincePostMs_.
//		The autozero timer, rPressureXducerAutozeroTimer_, is reset with
//		the new time interval and the timer is started.  The isTimeToAutozero_
//		flag is set TRUE to indicate that an autozero maneuver is to be
//		executed on the next beginning exhalation.  timeSincePostMs_ is
//		limited to SIXTY_MINUTES to prevent the	clock from overflowing.
// 		$[04059]
//---------------------------------------------------------------------
//@ PreCondition
//		&timer == &rPressureXducerAutozeroTimer_
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
PressureXducerAutozero::timeUpHappened( const IntervalTimer& timer)
{
	CALL_TRACE("PressureXducerAutozero::timeUpHappened( const IntervalTimer& timer)") ;
	
	CLASS_PRE_CONDITION(&timer == &rPressureXducerAutozeroTimer_) ;
       
	timeSincePostMs_ += autozeroIntervalMs_ ;
	
	// $ [04059]
	if (timeSincePostMs_ < TWENTY_MINUTES)
	{
		// $[TI1.1]
		autozeroIntervalMs_ = LESS_THAN_20_MIN_INTERVAL ;
	}
	else if (timeSincePostMs_ < SIXTY_MINUTES)
	{
		// $[TI1.2]
		autozeroIntervalMs_ = LESS_THAN_60_MIN_INTERVAL ;
	}
	else
	{
		// $[TI1.3]
		autozeroIntervalMs_ = GREATER_THAN_60_MIN_INTERVAL ;
		timeSincePostMs_ = SIXTY_MINUTES ;	// limit to prevent overflow
	}
        
	rPressureXducerAutozeroTimer_.setTargetTime( autozeroIntervalMs_) ;
	rPressureXducerAutozeroTimer_.restart();

	isTimeToAutozero_ = TRUE ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newBreath()
//
//@ Interface-Description
//		This method has no arguments and has no return value.  This method
//		is called at the beginning of each exhalation phase to initialize an
//		autozero maneuver if required.  Occlusion triggering are disabled during
//		the maneuver.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The autozeroing of both the pressure trandsducers is complete after
//		two exhalations or 2 newBreath() calls.  The INSP and EXH side are
//		toggled.  The INSP side is zeroed first when isTimeToAutozero_ is set
//		true.  On the next newBreath() call, the EXH is zeroed.
//
//		Nothing is done if the flag, isTimeToAutozero_, is set FALSE which
//		implies no autozero is requested.
//
//		If TRUE, the flag is reset if the current side to autozero is INSP
//		to acknowledge the request of an autozero.
//		Another flag, isAutozeroInProgress_, is set TRUE to indicate that
//		the maneuver is in progress.  The clock for the maneuver is
//		initialized to zero along with the summers.  The autozeroSide_ solenoid
//		is commanded open.  The OcclusionTrigger is disabled.
// 		$[04061]
//---------------------------------------------------------------------
//@ PreCondition
//	autozeroSide_ == INSP || autozeroSide_ == EXH
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
PressureXducerAutozero::newBreath( void)
{
	CALL_TRACE("PressureXducerAutozero::newBreath( void)") ;
	
	// $ [04060]
	if (isTimeToAutozero_ || autozeroSide_ == EXH)
	{
		// $[TI1.1]
		if (autozeroSide_ == INSP)
		{
			// $[TI2.1]
			isTimeToAutozero_ = FALSE ;
			RInspAutozeroSolenoid.open() ;

#ifdef INTEGRATION_TEST_ENABLE
    // Signal Event for swat
    swat::EventManager::signalEvent(swat::EventManager::START_OF_AUTOZERO_INSP);
#endif // INTEGRATION_TEST_ENABLE

		}
		else if (autozeroSide_ == EXH)
		{
			// $[TI2.2]
			RExhAutozeroSolenoid.open() ;

#ifdef INTEGRATION_TEST_ENABLE
    // Signal Event for swat
    swat::EventManager::signalEvent(swat::EventManager::START_OF_AUTOZERO_EXH);
#endif // INTEGRATION_TEST_ENABLE
		}
		else
		{
			CLASS_PRE_CONDITION( autozeroSide_ == INSP || autozeroSide_ == EXH) ;
		}
		
		isAutozeroInProgress_ = TRUE ;
		elapsedTimeMs_ = 0 ;
		rawInspPressureSum_ = 0 ;
		rawExhPressureSum_ = 0 ;
	}	// implied else $[TI1.2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newCycle()
//
//@ Interface-Description
//		This method has no arguments and has no return value.  This method is
//		called every BD cycle during the exhalation phase.
//---------------------------------------------------------------------
//@ Implementation-Description
//		If the flag, isAutozeroInProgress_, is FALSE, nothing is done.
//
//		If TRUE, an average of the sampled values for each pressure sensor
//		is obtained for BEGIN_SAMPLING <= elapsedTimeMs_ <= END_SAMPLING.
//		After END_SAMPLING,	both solenoids are commanded close.  The average
//		zero offset values are ranged checked and drift range checked.  If
//		within specification, it is stored as the new autozero values, else
//		reported to SafetyNetTest.  After MANEUVER_COMPLETE,
//		isAutozeroInProgress_ is set FALSE to indicate maneuver is complete
//		and OcclusionTrigger is re-enabled.  elapsedTimeMs_ is incremented by
//		CYCLE_TIME_MS.
// 		$[04062] $[04063] $[04064] $[04286] $[04297]
//---------------------------------------------------------------------
//@ PreCondition
//	autozeroSide_ == INSP || autozeroSide_ == EXH
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
PressureXducerAutozero::newCycle( void)
{
	CALL_TRACE("PressureXducerAutozero::newCycle( void)") ;
	
	Uint16 errorCode = 0 ;
	
	if (isAutozeroInProgress_)
	{
		// $[TI1.1]
		if (elapsedTimeMs_ <= END_SAMPLING
				&& elapsedTimeMs_ >= BEGIN_SAMPLING)
		{
			// $[TI2.1]
			rawInspPressureSum_ += RInspPressureSensor.getCount() ;
			rawExhPressureSum_ += RExhPressureSensor.getCount() ;
		}	// implied else $[TI2.2]
		
		if (elapsedTimeMs_ == END_SAMPLING)
		{
			// $[TI3.1]
			RInspAutozeroSolenoid.close() ;
			RExhAutozeroSolenoid.close() ;

			Boolean rangeOkay = FALSE ;
			Boolean driftOkay = FALSE ;

			if (autozeroSide_ == INSP)
			{
				// $[TI7.1]
				Uint16 aveInspZero = (Uint16)(
										(Real32)rawInspPressureSum_
											/ ((END_SAMPLING - BEGIN_SAMPLING)
											/ CYCLE_TIME_MS + 1) + 0.5F
									) ;
									
				rangeOkay = isAutozeroInRange_( aveInspZero) ;
				driftOkay = isAutozeroWithinDriftRange_( aveInspZero, RInspPressureSensor.zeroOffset_) ;
				
				/* any range or drift fault restart autozero's for 1 min increments*/
				if(!rangeOkay || !driftOkay)
				{
					autozeroIntervalMs_ = LESS_THAN_20_MIN_INTERVAL;
					rPressureXducerAutozeroTimer_.setTargetTime( autozeroIntervalMs_) ;
	                                rPressureXducerAutozeroTimer_.restart();
                                }
					
				if (rangeOkay && driftOkay)
				{
					// $[TI4.1]
					// autozero value is within spec, store new value
					if( (SoftwareOptions::GetSoftwareOptions().getDataKeyType()) !=
							SoftwareOptions::MANUFACTURING)
					  {	
					     RInspPressureSensor.zeroOffset_ = alphaFilter_(aveInspZero,RInspPressureSensor.zeroOffset_);
					  }
					 else
					  {
						RInspPressureSensor.zeroOffset_ = aveInspZero; 
					  }
					 
					numInspAZCyclesDriftOOR_ = 0;
					numInspAZCyclesRangeOOR_ = 0;
					
				}
				else if( !rangeOkay && (++numInspAZCyclesRangeOOR_ >= maxAZCyclesOOR_) )
				{
				    numInspAZCyclesRangeOOR_ = 0;	
               			    errorCode = aveInspZero ;
               			    
				    RSafetyNetTestMediator.logBdSafetyNetEvent( SafetyNetTestMediator::PI_AZ_OOR, errorCode) ;
#ifdef SIGMA_PAV_UNIT_TEST
				    ErrorCode_UT = errorCode ;
#endif // SIGMA_PAV_UNIT_TEST
				}
					
				else if (!driftOkay && (++numInspAZCyclesDriftOOR_ >= maxAZCyclesOOR_))
					{
						numInspAZCyclesDriftOOR_ = 0;
						// $[TI10]
						if (aveInspZero > RInspPressureSensor.zeroOffset_)
						{
							// $[TI11]
							errorCode = aveInspZero - RInspPressureSensor.zeroOffset_ ;
						}
						else
						{
							// $[TI12]
							errorCode = RInspPressureSensor.zeroOffset_ - aveInspZero ;
						}
						errorCode |= 0x8000 ;
					
	                                           
				 	    RSafetyNetTestMediator.logBdSafetyNetEvent( SafetyNetTestMediator::PI_AZ_OOR, errorCode) ;
#ifdef SIGMA_PAV_UNIT_TEST
					    ErrorCode_UT = errorCode ;
#endif // SIGMA_PAV_UNIT_TEST
				        }
			}
			else if (autozeroSide_ == EXH)
			{
				// $[TI7.2]
				Uint16 aveExhZero = (Uint16)(
										(Real32)rawExhPressureSum_
											/ ((END_SAMPLING - BEGIN_SAMPLING)
											/ CYCLE_TIME_MS + 1) + 0.5F
									) ;

				rangeOkay = isAutozeroInRange_( aveExhZero) ;
				driftOkay = isAutozeroWithinDriftRange_( aveExhZero, RExhPressureSensor.zeroOffset_) ;
                                
                                if(!rangeOkay || !driftOkay)
				{
					autozeroIntervalMs_ = LESS_THAN_20_MIN_INTERVAL;
					rPressureXducerAutozeroTimer_.setTargetTime( autozeroIntervalMs_) ;
	                                rPressureXducerAutozeroTimer_.restart();
                                }
                                
				if (rangeOkay && driftOkay)
				{
					// $[TI5.1]
					// autozero value is within spec, store new valuea
					if( (SoftwareOptions::GetSoftwareOptions().getDataKeyType()) != 
							SoftwareOptions::MANUFACTURING)
					  {
					     RExhPressureSensor.zeroOffset_ = alphaFilter_(aveExhZero,RExhPressureSensor.zeroOffset_) ;
					  }
					else
					  {
						  RExhPressureSensor.zeroOffset_ = aveExhZero;
					  }

					numExhAZCyclesDriftOOR_ = 0;
					numExhAZCyclesRangeOOR_ = 0;
                   
			
				}
				else if(!rangeOkay && (++numExhAZCyclesRangeOOR_ >= maxAZCyclesOOR_))
				{
                                        numExhAZCyclesRangeOOR_ = 0;
				        errorCode = aveExhZero ;
					  
					RSafetyNetTestMediator.logBdSafetyNetEvent( SafetyNetTestMediator::PE_AZ_OOR, errorCode) ;
#ifdef SIGMA_PAV_UNIT_TEST
					ErrorCode_UT = errorCode ;
#endif // SIGMA_PAV_UNIT_TEST
				}
			       else if (!driftOkay && (++numExhAZCyclesDriftOOR_ >= maxAZCyclesOOR_))
				 {
					numExhAZCyclesDriftOOR_ = 0;
						// $[TI14]
					if (aveExhZero > RExhPressureSensor.zeroOffset_)
					    {
						// $[TI15]
						errorCode = aveExhZero - RExhPressureSensor.zeroOffset_ ;
					    }
					else
					   {
							// $[TI16]
					       errorCode = RExhPressureSensor.zeroOffset_ - aveExhZero ;
					   }
						
					   errorCode |= 0x8000 ;
					 					

					RSafetyNetTestMediator.logBdSafetyNetEvent( SafetyNetTestMediator::PE_AZ_OOR, errorCode) ;
#ifdef SIGMA_PAV_UNIT_TEST
					ErrorCode_UT = errorCode ;
#endif // SIGMA_PAV_UNIT_TEST
                                  }
				    
		     }
			else
			{
				CLASS_PRE_CONDITION( autozeroSide_ == INSP || autozeroSide_ == EXH) ;
			}
		}
		else
		{
			// $[TI3.2]
			if (elapsedTimeMs_ == MANEUVER_COMPLETE)
			{
				// $[TI6.1]
				isAutozeroInProgress_ = FALSE ;

				if (autozeroSide_ == INSP)
				{
					// $[TI8.1]
				    RInspPressureSensor.initValues( RInspPressureSensor.getValue(), 0.0, 0.0,
    												RInspPressureSensor.getValue()) ;
    				autozeroSide_ = EXH ;
    			}
    			else if (autozeroSide_ == EXH)
    			{
					// $[TI8.2]
				    RExhPressureSensor.initValues( RExhPressureSensor.getValue(), 0.0, 0.0,
    											   RExhPressureSensor.getValue());
    				autozeroSide_ = INSP ;
    			}
			}	// implied else $[TI6.2]
		}
		elapsedTimeMs_ += CYCLE_TIME_MS ;
	}	// implied else $[TI1.2]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: initialize()
//
//@ Interface-Description
//		This method has no arguments and has no return value.  This method is
//		called after POST to obtain an initial zero offset value for the
//		pressure transducers.
//---------------------------------------------------------------------
//@ Implementation-Description
//		A MsgQueue is tied to a 5 ms clock.  A sample of measurements are
//		obtained of the insp/exh pressure transducers.  The solenoids are closed
//		when sampling is complete.	An average is computed and checked if it is
//		within the autozero	range.  If it is, it is stored as the zero offset
//		values, else log to SafetyNet subsystem.  
// 		$[04062] $[04063] $[04286] $[04297]
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
PressureXducerAutozero::initialize( void)
{
	CALL_TRACE("PressureXducerAutozero::initialize( void)") ;
	
	// synch with BD timer
	MsgQueue bdCycleMsgQ( ::TIMER_5MS_Q) ;	

	Int32 msgPend ;

	rawInspPressureSum_ = 0 ;
	rawExhPressureSum_ = 0 ;
	elapsedTimeMs_ = 0 ;
	
	RInspAutozeroSolenoid.open() ;
	RExhAutozeroSolenoid.open() ;

#ifdef INTEGRATION_TEST_ENABLE
    // Signal Event for swat
    swat::EventManager::signalEvent(swat::EventManager::START_OF_AUTOZERO_INSP);
    swat::EventManager::signalEvent(swat::EventManager::START_OF_AUTOZERO_EXH);
#endif // INTEGRATION_TEST_ENABLE

// E600 BDIO	RWriteIOPort.writeNewCycle() ;
	
#ifndef SIGMA_UNIT_TEST
	// call bdCycleMsgQ() twice to ensure that 5 ms has elapsed
	bdCycleMsgQ.pendForMsg( msgPend) ;
	bdCycleMsgQ.pendForMsg( msgPend) ;
#endif // SIGMA_UNIT_TEST
  	
	while (elapsedTimeMs_ <= MANEUVER_COMPLETE)
	{
		// $[TI1.1]
		if (elapsedTimeMs_ <= END_SAMPLING
				&& elapsedTimeMs_ >= BEGIN_SAMPLING)
		{
			// $[TI2.1]
			rawInspPressureSum_ += AdcChannels::GetNewSample( RInspPressureSensor.getAdcId()) ;
			rawExhPressureSum_ += AdcChannels::GetNewSample( RExhPressureSensor.getAdcId()) ;
		}	// implied else $[TI2.2]
		
		if (elapsedTimeMs_ == END_SAMPLING)
		{
			// $[TI3.1]
			RInspAutozeroSolenoid.close() ;
			RExhAutozeroSolenoid.close() ;
// E600 BDIO				RWriteIOPort.writeNewCycle() ;
	
			Uint16 aveInspZero = (Uint16)(
									(Real32)rawInspPressureSum_
										/ ((END_SAMPLING - BEGIN_SAMPLING)
											/ CYCLE_TIME_MS + 1) + 0.5F
								) ;
							
			if (isAutozeroInRange_( aveInspZero))
			{
				// $[TI4.1]
				// autozero value is within spec, store new value
				RInspPressureSensor.zeroOffset_ =  alphaFilter_(aveInspZero,PRIMARY_AZ_VALUE) ;
			}
			else
			{
				// $[TI4.2]
				RSafetyNetTestMediator.logBdSafetyNetEvent( SafetyNetTestMediator::PI_AZ_OOR, aveInspZero) ;
			}

			Uint16 aveExhZero = (Uint16)(
									(Real32)rawExhPressureSum_
										/ ((END_SAMPLING - BEGIN_SAMPLING)
										/ CYCLE_TIME_MS + 1) + 0.5F
								) ;

			if (isAutozeroInRange_( aveExhZero))
			{
				// $[TI5.1]
				// autozero value is within spec, store new value
				RExhPressureSensor.zeroOffset_ = alphaFilter_(aveExhZero,PRIMARY_AZ_VALUE) ;
			}
			else
			{
				// $[TI5.2]
				RSafetyNetTestMediator.logBdSafetyNetEvent( SafetyNetTestMediator::PE_AZ_OOR, aveExhZero) ;
			}

			
		}	// implied else $[TI3.2]
		elapsedTimeMs_ += CYCLE_TIME_MS ;

#ifndef SIGMA_UNIT_TEST
		bdCycleMsgQ.pendForMsg( msgPend) ;
#endif // SIGMA_UNIT_TEST
	}	// implied else $[TI1.2]
	// initialize filtered values
	
	RSensorMediator.newCycle() ;
    RInspPressureSensor.initValues( RInspPressureSensor.getValue(), 0.0, 0.0,
									RInspPressureSensor.getValue()) ;
    RExhPressureSensor.initValues( RExhPressureSensor.getValue(), 0.0, 0.0,
								   RExhPressureSensor.getValue());
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
PressureXducerAutozero::SoftFault( const SoftFaultID  softFaultID,
                   				   const Uint32       lineNumber,
		   						   const char*        pFileName,
		   						   const char*        pPredicate)
{
	CALL_TRACE("SoftFault( softFaultID, lineNumber, pFileName, pPredicate)") ;
  	FaultHandler::SoftFault( softFaultID, BREATH_DELIVERY, PRESSUREXDUCERAUTOZERO,
                          lineNumber, pFileName, pPredicate) ;
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//=====================================================================
//
//  Private Methods...
//
//=====================================================================






