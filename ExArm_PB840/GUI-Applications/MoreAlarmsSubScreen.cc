#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: MoreAlarmsSubScreen - Displays, on the Upper Screen, alarm messages
// supplemental to the Alarm and Status Area, specifically the 3rd through 8th
// highest urgency alarms.
//---------------------------------------------------------------------
//@ Interface-Description
// This is a SubScreen which contains six AlarmMsg container objects
// which display the 3rd thru 8th highest urgency alarm messages
// (remember that the AlarmAndStatusArea displays the two highest
// urgency alarm messages).
// 
// During normal GUI operation, this area relies on the data provided by
// the Alarms subsystem for the current alarm messages.
//
// The activate()/deactivate() methods are called automatically on
// display/removal of this subscreen by our subscreen area owner.  As
// alarm events occur the updateMoreAlarmsDisplay() method should be
// called, allowing this subscreen to update itself appropriately (if
// currently displayed).  timerEventHappened() is called when the
// More Alarms activation timer expires.  This is used to deactivate
// the subscreen when it has been displayed for a certain period, allowing
// viewing of the Status Area.
//---------------------------------------------------------------------
//@ Rationale
// For both the normal GUI and the Service GUI, this class is dedicated
// to managing the display of the More Alarms SubScreen on the
// UpperSubScreenArea.
//---------------------------------------------------------------------
//@ Implementation-Description
// An alarm message is a complete summary of a current alarm condition.
// When updated alarm messages are available, an external message must
// be sent to this class to force the internal alarms display processing
// to occur (i.e., somebody must call the updateMoreAlarmsDisplay()
// method).  The updateMoreAlarmsDisplay() method takes care of querying
// the Alarms subsystem for the number of current alarms.  Based on the
// number of current alarms, the alarm messages are made visible.  The
// AlarmMsg objects take care of the details of rendering the individual
// alarm messages.
//
// The six AlarmMsg objects are added to the Container when it is
// constructed. 
//---------------------------------------------------------------------
//@ Fault-Handling
// Normal logic assertions.
//---------------------------------------------------------------------
//@ Restrictions
// None.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/MoreAlarmsSubScreen.ccv   25.0.4.0   19 Nov 2013 14:08:08   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004   By: srp   Date:  24-Apr-2002    DR Number: 5327 & 5908
//  Project:  NeoMode
//  Description:
//      Added Modification log. Updated for NeoMode  03/01/00
//
//  Revision: 003  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 002  By:  sah   Date:  28-Jul-1997   DCS Number:  2320
//	Project:  Sigma (R8027)
//	Description:
//      Now using 'AlarmStateManger' for retrieving the list of current
//		alarms.  Also eliminated some PRODUCTION-mode warnings.
//
//  Revision: 001  By:  wks    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

//
// Sigma includes.
//
#include "MoreAlarmsSubScreen.hh"

//@ Usage-Classes
#include "AlarmAnnunciator.hh"
#include "SortIterC_AlarmUpdate.hh"
#include "GuiTimerRegistrar.hh"
#include "UpperScreen.hh"
#include "UpperSubScreenArea.hh"
#include "AlarmStateManager.hh"
//@ End-Usage

//@ Code...

// Initialize static constants.
static const Int32 NUM_ROWS_ = 6;
static const Int32 ALARM_MSG_X_ = -3;


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: MoreAlarmsSubScreen()  [Constructor]
//
//@ Interface-Description
// Default constructor.  Passed a pointer to the subscreen area which creates
// it (the upper subscreen area).
//---------------------------------------------------------------------
//@ Implementation-Description
// Initializes all members and initializes the colors, sizes, and
// positions of all graphical objects.  Adds all graphical objects to
// the parent container for display.  Register for the More Alarms
// activation timer events.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

MoreAlarmsSubScreen::MoreAlarmsSubScreen(SubScreenArea* pSubScreenArea) :
	SubScreen(pSubScreenArea)
{
	CALL_TRACE("MoreAlarmsSubScreen::MoreAlarmsSubScreen(pSubScreenArea)");


	// Size and position the area
	setX(0);
	setY(0);
	setWidth(UpperSubScreenArea::CONTAINER_WIDTH);
	setHeight(UpperSubScreenArea::CONTAINER_HEIGHT);

	// Set subscreen background color
	setFillColor(Colors::BLACK);

	//
	// Set position of the alarm message containers.  We need to set a
	// negative x-coordinate since the AlarmMsg container is actually wider
	// than this subscreen container.
	//
	Int32 alarmMsgHeight = alarmMsg3_.getHeight();
	CLASS_ASSERTION((alarmMsgHeight * NUM_ROWS_) <=
								UpperSubScreenArea::CONTAINER_HEIGHT);

	alarmMsg3_.setX(ALARM_MSG_X_);
	alarmMsg3_.setY(alarmMsgHeight*0);
	alarmMsg4_.setX(ALARM_MSG_X_);
	alarmMsg4_.setY(alarmMsgHeight*1);
	alarmMsg5_.setX(ALARM_MSG_X_);
	alarmMsg5_.setY(alarmMsgHeight*2);
	alarmMsg6_.setX(ALARM_MSG_X_);
	alarmMsg6_.setY(alarmMsgHeight*3);
	alarmMsg7_.setX(ALARM_MSG_X_);
	alarmMsg7_.setY(alarmMsgHeight*4);
	alarmMsg8_.setX(ALARM_MSG_X_);
	alarmMsg8_.setY(alarmMsgHeight*5);

	//
	// Add containers.
	//
	addDrawable(&alarmMsg3_);
	alarmMsg3_.setShow(FALSE);
	addDrawable(&alarmMsg4_);
	alarmMsg4_.setShow(FALSE);
	addDrawable(&alarmMsg5_);
	alarmMsg5_.setShow(FALSE);
	addDrawable(&alarmMsg6_);
	alarmMsg6_.setShow(FALSE);
	addDrawable(&alarmMsg7_);
	alarmMsg7_.setShow(FALSE);
	addDrawable(&alarmMsg8_);
	alarmMsg8_.setShow(FALSE);

	// Register for More Alarms activation timer
	GuiTimerRegistrar::RegisterTarget(GuiTimerId::MORE_ALARMS_TIMEOUT, this);
														// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~MoreAlarmsSubScreen  [Destructor]
//
//@ Interface-Description
//  Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

MoreAlarmsSubScreen::~MoreAlarmsSubScreen(void)
{
	CALL_TRACE("MoreAlarmsSubScreen::~MoreAlarmsSubScreen(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activate()
//
//@ Interface-Description
// Prepares this subscreen for display.
//---------------------------------------------------------------------
//@ Implementation-Description
// Informs the adjacent AlarmAndStatusArea to switch to its "more
// alarms" display mode.  This causes the AlarmAndStatusArea to appear
// to merge with this MoreAlarmSubScreen.  Then, calls the
// updateMoreAlarmsDisplay() method to refresh the contents of this
// subscreen before it is displayed.  Start the More Alarms activation
// timer.
//
// $[01187] More Alarms subscreen should merge with Alarm and Status Area.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
MoreAlarmsSubScreen::activate(void)
{
	CALL_TRACE("MoreAlarmsSubScreen::activate(void)");

	//
	// Inform the AlarmAndStatusArea that the MoreAlarmsSubScreen is being
	// activated.  This causes the AlarmAndStatusArea to display "window
	// dressing" that matches the look of this subscreen.
	//
	UpperScreen::RUpperScreen.getAlarmAndStatusArea()->
		setDisplayMode(AlarmAndStatusArea::AASA_MODE_MORE_ALARMS);

	//
	// Start the More Alarms activation timer
	//
	GuiTimerRegistrar::StartTimer(GuiTimerId::MORE_ALARMS_TIMEOUT);

	//
	// Update the alarm message slots with the current alarms.
	//
	updateMoreAlarmsDisplay();							// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: deactivate()
//
//@ Interface-Description
// Prepares this subscreen for removal from the display.
//---------------------------------------------------------------------
//@ Implementation-Description
// Informs the adjacent AlarmAndStatusArea to switch back to its
// "normal" display mode.  This causes the AlarmAndStatusArea to again
// appear separate from the MoreAlarmSubScreen.  Cancel the More Alarms
// activation timer.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
MoreAlarmsSubScreen::deactivate(void)
{
	CALL_TRACE("MoreAlarmsSubScreen::deactivate(void)");

	//
	// Inform the AlarmAndStatusArea that the MoreAlarmsSubScreen is being
	// deactivated.  This removes the special "window dressing".
	//
	UpperScreen::RUpperScreen.getAlarmAndStatusArea()->
		setDisplayMode(AlarmAndStatusArea::AASA_MODE_NORMAL);

	//
	// Cancel the More Alarms activation timer
	//
	GuiTimerRegistrar::CancelTimer(GuiTimerId::MORE_ALARMS_TIMEOUT);
													// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateMoreAlarmsDisplay
//
//@ Interface-Description
// Refreshes the contents of this subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Forces an update of the MoreAlarmSubScreen.  Queries the Alarm
// subsystem for the number of current alarms.  If there are more than 2
// current alarms, then fetch the 3rd, 4th, etc.  active alarms and pass
// each of them onto the alarmMsg3_, alarmMsg4_, etc.  alarm message
// containers for rendering.  Typically, there will be fewer than eight
// active alarms, so the unused alarm containers remain hidden.  If
// there are more than eight alarms, the extra alarms are simply not
// displayed.  It is possible for this method to be called when this
// subscreen is not currently displayed, so this processing only occurs
// when this is the currently active subscreen.
//
// $[01188] More Alarms and Alarm and Status Area should display up to 8 ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
MoreAlarmsSubScreen::updateMoreAlarmsDisplay(void)
{
	CALL_TRACE("MoreAlarmsSubScreen::updateMoreAlarmsDisplay(void)");

	//
	// If this subscreen is the currently active subscreen, then
	// update the graphics display as necessary.
	//
	if (UpperScreen::RUpperScreen.getUpperSubScreenArea()->isCurrentSubScreen(this))
	{													// $[TI1]
		Int32 numCurrentAlarms = 0;

		//
		// Fetch current alarm list from AlarmAnnunciation subsystem, then determine
		// how many entries there are in the list.
		//
		SortIterC_AlarmUpdate alarmUpdates(AlarmStateManager::GetCurrAlarmList());
		numCurrentAlarms = alarmUpdates.getNumItems();

		//
		// To start, hide everything.
		//
		alarmMsg3_.setShow(FALSE);
		alarmMsg4_.setShow(FALSE);
		alarmMsg5_.setShow(FALSE);
		alarmMsg6_.setShow(FALSE);
		alarmMsg7_.setShow(FALSE);
		alarmMsg8_.setShow(FALSE);

		//
		// We only have to display overflow message if there are more than two
		// messages to start with.  If there are more than two entries, skip
		// the first two entries so that we are positioned at the third entry,
		// if any.
		//
		if (numCurrentAlarms > 2)
		{												// $[TI2]
#if defined(SIGMA_DEVELOPMENT)
			SigmaStatus status =
#endif // defined(SIGMA_DEVELOPMENT)

			alarmUpdates.goFirst();

#if defined(SIGMA_DEVELOPMENT)
			SAFE_CLASS_ASSERTION(SUCCESS == status);

			status =
#endif // defined(SIGMA_DEVELOPMENT)

			alarmUpdates.goNext();

#if defined(SIGMA_DEVELOPMENT)
			SAFE_CLASS_ASSERTION(SUCCESS == status);
#endif // defined(SIGMA_DEVELOPMENT)
			
			//
			// Walk down the available alarm rows, until we run out
			// of messages or rows, whichever comes first.
			//
			for (Int32 i = 0; i < NUM_ROWS_; i++)
			{											// $[TI14.1]
				AlarmMsg*	pAlarmMsg = NULL;

				if (SUCCESS != alarmUpdates.goNext())
				{										// $[TI3]
					break;		// no more alarms, so we're outta here
				}										// $[TI4]

				//
				// Update alarm message and display in its assigned slot.
				//
				switch (i)
				{
				case 0:									// $[TI5]
					pAlarmMsg = &alarmMsg3_;
					break;

				case 1:									// $[TI6]
					pAlarmMsg = &alarmMsg4_;
					break;

				case 2:									// $[TI7]
					pAlarmMsg = &alarmMsg5_;
					break;

				case 3:									// $[TI8]
					pAlarmMsg = &alarmMsg6_;
					break;

				case 4:									// $[TI9]
					pAlarmMsg = &alarmMsg7_;
					break;

				case 5:									// $[TI10]
					pAlarmMsg = &alarmMsg8_;
					break;

				default:								// $[TI11]
					CLASS_ASSERTION(FALSE);
					break;
				}

				SAFE_CLASS_ASSERTION(NULL != pAlarmMsg);
				pAlarmMsg->setShow(TRUE);
				pAlarmMsg->updateDisplay(alarmUpdates.currentItem());
			}											// $[TI14.2]
		}												// $[TI12]
	}													// $[TI13]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: timerEventHappened
//
//@ Interface-Description
// Called when a timer that we started runs out, in this case the More Alarms
// activation timer.  We must deactivate this subscreen if this subscreen is
// currently being displayed.
//---------------------------------------------------------------------
//@ Implementation-Description
// If this subscreen is the currently active upper subscreen we deactivate
// it.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
MoreAlarmsSubScreen::timerEventHappened(GuiTimerId::GuiTimerIdType)
{
	CALL_TRACE("MoreAlarmsSubScreen::timerEventHappened("
										"GuiTimerId::GuiTimerIdType timerId)");

	// $[01337] The more alarms subscreen shall be automatically dismissed ...
	if (UpperScreen::RUpperScreen.getUpperSubScreenArea()->isCurrentSubScreen(this))
	{													// $[TI1]
		// Deactivate this subscreen.
		UpperScreen::RUpperScreen.getUpperSubScreenArea()->deactivateSubScreen();
	}													// $[TI2]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
MoreAlarmsSubScreen::SoftFault(const SoftFaultID  softFaultID,
							   const Uint32       lineNumber,
							   const char*        pFileName,
							   const char*        pPredicate)  
{
	CALL_TRACE("MoreAlarmsSubScreen::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, MOREALARMSSUBSCREEN,
							lineNumber, pFileName, pPredicate);
}
