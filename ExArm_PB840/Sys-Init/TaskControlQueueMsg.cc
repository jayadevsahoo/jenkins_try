#include "stdafx.h"
//=====================================================================
//   This is a proprietary work to which Puritan-Bennett corporation
//   of California claims exclusive right.  No part of this work may
//  be used, disclosed, reproduced, sorted in an information retrieval
//     system, or transmitted by any means, electronic, mechanical,
//   photocopying, recording, or otherwise without the prior written
//      permission of Puritan-Bennett Corporation of California.
//
//          Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: TaskControlQueueMsg - VRTX queue messages for Task-Control.
//---------------------------------------------------------------------
//@ Interface-Description
//  The TaskControlQueueMsg contains Task Control event information 
//  that can be represented in a single 32-bit word and passed between 
//  tasks using a MsgQueue.  The TaskControlQueueMsg contains an object 
//  identifier and object data initialized during construction.  
//  An accessor method returns a combined object 
//  identifier and object pointer as a single 32-bit word.  This class
//  safely combines a single byte identifier with up to three bytes
//  for an object pointer.
//---------------------------------------------------------------------
//@ Rationale
//  This class safely combines an object identifier and object pointer
//  into a single 32-bit queue event.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The constructor accepts a single byte object identifier and three
//  byte object pointer.  It combines them to provide a single 32-bit
//  data word.  An accessor method returns the combined representation.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  In most cases, this class will be used to combine an object 
//  identifier with an object address.  During object construction the
//  object identifier is masked to assure there is no data in the 
//  upper three bytes of the word parameter.  The object address is
//  masked to assure no data is lost in the upper byte of the parameter.
//  This restricts object identifiers to the range 0 to 255 inclusive. 
//  It restricts object addresses (data) to the range 0 to 0xFFFFFF.
//  Addresses above 0xFFFFFF will cause a pre-condition assertion.
//  For Sigma, all RAM is located below 0xFFFFFF with only ROM located
//  above this address.  Therefore, this class essentially restricts
//  object addresses to less than 0xFFFFFF which should not be a 
//  problem unless the memory map is altered to allow RAM objects above
//  0xFFFFFF.
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Sys-Init/vcssrc/TaskControlQueueMsg.ccv   25.0.4.0   19 Nov 2013 14:33:58   pvcs  $
//
//@ Modification-Log
//
//   Revision 002   By:   sah      Date: 01/13/99    DR Number: 5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to a non-inlined method.
//
//   Revision 001   By:   gdc      Date: 07/06/95    DR Number:   n/a  
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//=====================================================================

//=====================================================================
//
//      Includes...
//
//=====================================================================

#include "TaskControlQueueMsg.hh"
#include "MemoryHandle.hh"

//@ Usage-Classes
//@ End-Usage

//=====================================================================
//
//      Static Data...
//
//=====================================================================

//@ Code...

//=====================================================================
//
//      Public Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TaskControlQueueMsg [constructor]
//
//@ Interface-Description
//  TaskControlQueueMsg constructor constructs an object from the 
//  specified objectId and objectData.  It accepts a single byte object
//  identifier and three bytes of object data.  The object identifier
//  must not exceed a single byte and the object data must not exceed
//  three bytes.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Masks the objectId and objectData arguments to ensure information
//  is not lost when combining the data into a single word.  Stores 
//  the objectData and objectId into its private msg_ union.  The first
//  eight bits of msg_ contain the objectId with the remaining 24 bits
//  containing the objectData.
//---------------------------------------------------------------------
//@ PreCondition
//  (objectData & 0xFF000000) == 0 
//  (objectId & 0xFFFFFF00) == 0
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

TaskControlQueueMsg::TaskControlQueueMsg(Int32 objectId, Int32 objectData) 
{
    CALL_TRACE("TaskControlQueueMsg(Int32, Int32)");

    CLASS_PRE_CONDITION((objectId & 0xFFFFFF00) == 0);

	msg_.event.msgData	  = objectData;
    msg_.event.msgId      = objectId;
    // $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TaskControlQueueMsg [constructor]
//
//@ Interface-Description
//  This TaskControlQueueMsg contructor accepts a 32-bit queue event
//  which assumedly contains an object identifier in the upper byte
//  and three bytes of object data in the lower bytes.  Through its
//  accessor methods, this class then provides access to these fields.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================


TaskControlQueueMsg::TaskControlQueueMsg(Int32 queueData)
{
    CALL_TRACE("TaskControlQueueMsg(const TaskControlQueueMsg&)");

    msg_.queueData = queueData;
    // $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TaskControlQueueMsg [copy constructor]
//
//@ Interface-Description
//  TaskControlQueueMsg copy constructor copies the referenced message
//  contents to the constructed object.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================



TaskControlQueueMsg::TaskControlQueueMsg(const TaskControlQueueMsg& rMessage) :
        msg_(rMessage.msg_)
{
    CALL_TRACE("TaskControlQueueMsg(const TaskControlQueueMsg&)");
    // $[TI3]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~TaskControlQueueMsg [destructor]
//
//@ Interface-Description
//  TaskControlQueueMsg destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

TaskControlQueueMsg::~TaskControlQueueMsg()
{
    CALL_TRACE("~TaskControlQueueMsg()");
}

 

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//@ Interface-Description
//   This method is called when a software fault is detected by the
//   fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//   'lineNumber' are essential pieces of information.  The 'pFileName'
//   and 'pPredicate' strings may be defaulted in the macro to reduce
//   code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This method receives the call for the SoftFault, adds it sub-system
//   and class name ID and sends the message to the non-member function
//   SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
 
void
TaskControlQueueMsg::SoftFault(const SoftFaultID  softFaultID,
                               const Uint32       lineNumber,
                               const char*        pFileName,
                               const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
 
  FaultHandler::SoftFault(softFaultID, SYS_INIT,
                          Sys_Init::ID_TASKCONTROLQUEUEMSG,
                          lineNumber, pFileName, pPredicate);
}


//=====================================================================
//
//      Protected Methods...
//
//=====================================================================

//=====================================================================
//
//      Private Methods...
//
//=====================================================================




