
//#############################################################################
//
//  Crc.h - Header file for CRC functions.
//
//#############################################################################

#ifndef CRC_H

#define CRC_H

    // CRC using start address and number of bytes
    uint32_t CalculateCrc(uint8_t *Buffer, uint32_t NumberOfBytes);          // Calculate CRC

    // CRC using start and end addresses for the data
    uint32_t CalculateCrc(uint8_t *BeginBuffer, uint8_t *StopAddress);

    // Crc, continuing a previous CRC calc, using a single byte of input
    uint32_t CalculateCrc(uint32_t CalculatedCrc, uint8_t byte);

    // Crc, continuing a previous CRC calc, using a start address and number of bytes
    uint32_t CalculateCrc(uint32_t CalculatedCrc, uint8_t *Buffer, int NumberOfBytes);

    // Crc calculation for XML data sent to Bernoulli
    uint16_t CalculateCrc(uint8_t *Buffer,uint32_t NumberOfBytes, uint16_t CalculatedCrc);

    // Crc calculation for data sent to Vuelink
    uint16_t CalculateCrcForVuelink(uint8_t *Buffer, uint16_t NumberOfBytes);

	// Crc calculation for data sent through intra vent serial port
	uint8_t calcCRC8( const uint8_t *inBuf, const unsigned int inLen);

#endif

