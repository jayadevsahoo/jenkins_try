
#ifndef VentTypeSetting_HH
#define VentTypeSetting_HH

//====================================================================
//
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  VentTypeSetting - Vent Type Setting.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/VentTypeSetting.hhv   25.0.4.0   19 Nov 2013 14:27:50   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002   By: gdc    Date: 11-Aug-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support changing IBW while ventilating.
//
//  Revision: 001  By: gdc    Date: 20-Feb-2005    SCR Number: 6144
//  Project:  NIV1
//  Description:
//      Initial version.
//====================================================================

//@ Usage-Classes
#include "BatchDiscreteSetting.hh"
#include "SettingObserver.hh"
//@ End-Usage


class VentTypeSetting : public BatchDiscreteSetting, public SettingObserver
{
	public:
		VentTypeSetting(void);
		virtual ~VentTypeSetting(void);

		virtual Applicability::Id  getApplicability(
												   const Notification::ChangeQualifier qualifierId
												   ) const;

		virtual void  calcTransition(const DiscreteValue toValue,
									 const DiscreteValue fromValue);

		virtual SettingValue  getNewPatientValue(void) const;

		// SettingObserver methods...
		virtual void  valueUpdate(const Notification::ChangeQualifier qualifierId,
								  const SettingSubject*               pSubject);

		virtual Boolean  doRetainAttachment(const SettingSubject* pSubject) const;

		virtual void  settingObserverInit(void);

		static void  SoftFault(const SoftFaultID softFaultID,
							   const Uint32 lineNumber,
							   const char*  pFileName  = NULL, 
							   const char*  pPredicate = NULL);
	private:
		VentTypeSetting(const VentTypeSetting&);	// not implemented...
		void  operator=(const VentTypeSetting&);		// not implemented...
};


#endif // VentTypeSetting_HH 
