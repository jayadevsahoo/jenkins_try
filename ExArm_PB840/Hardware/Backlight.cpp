#include "stdafx.h"
//#############################################################################
//
//  Backlight_t.cpp - Implementingr file for Backlight_t class.
//
//
//#############################################################################

#include "Backlight.h"

BackLight_t::BackLight_t(HANDLE FileHandle, uint8_t BusAddress, I2cSubAddressMode_t SubAddrMode, I2cBaudIndex_t BaudIndex, DWORD Timeout)
                         : I2C(FileHandle, BusAddress, SubAddrMode, BaudIndex, Timeout)
{
}
BackLight_t::~BackLight_t(void)
{
}
//#############################################################################
//
//  Backlight_t::Write - This performs 8 byte writes to the 24LCxx.
//      Returns true if successful.
//
//#############################################################################

bool BackLight_t::Write(uint8_t *SourceAddress)
{
        uint8_t Value[2];
        bool RetVal = true;
        Value[0] = (*SourceAddress) >> 4;
        Value[1] = (*SourceAddress) << 4;
   
        if (I2C::Write(Value, sizeof(WORD)) != I2C_SUCCESSFUL)        
        {
                RetVal = false;
        }
    return RetVal;
}


