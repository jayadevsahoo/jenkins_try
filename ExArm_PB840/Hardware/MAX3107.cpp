#include "stdafx.h"
#include "AioDioDriver.h"
#include "sdk_spi.h"

// TODO E600 remove unnecessary and dead code later
//#include "sdk_spi.h"
//#include <gpio_ioctls.h>
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifdef SIGMA_GUI_CPU
        enum MAX3107_REGISTERS_ADDR_t
        {
            // FIFO DATA
            RHR = 0, THR = 0, 
            //Interrupts
            IRQEn = 0x01, ISR, LSRIntEn, LSR, SpclChrIntEn, SpclCharInt, STSIntEn, STSInt,
            // UART MODES
            MODE1 = 0x09, MODE2, LCR, RxTimeOut, HDplxDelay, IrDA,
            // FIFO CONTROL
            FlowLvl = 0x0F, FIFOTrgLvl, TxFIFOLvl, RxFIFOLvl,
            // FLOW CONTROL
            FlowCtrl = 0x13, XON1, XON2 , XOFF1, XOFF2,
            // GPIOs
            GPIOConfg = 0x18, GPIOData,
            // CLOCK CONFIGURATION
            PLLConfig = 0x1A, BRGConfig, DIVLSB, DIVMSB, CLKSource,
            // REVISION
            RevID = 0x1F
        };
static const BYTE READ_COMMAND = 0x00;
static const BYTE WRITE_COMMAND = 0x80;

void AioDioDriver_t::InitSPI_UART(void)
{
            BYTE IRQEn_Bits = 0;
            BYTE ISR_LEVEL = 0;
            BYTE LSRIntEn_Bits = 0;
            BYTE LSR_Bits = 0;
            BYTE SpclChrIntEn_Bits = 0;
            BYTE SpclCharInt_Bits = 0;
            BYTE STSIntEn_Bits = 0;
            BYTE STSInt_Bits = 0;
            BYTE IrDA_Bits = 0;
            BYTE MODE1_Bits = 0; 
//            BYTE MODE2_Bits = 0x20; //Loopback
            BYTE MODE2_Bits = 0x00; 
            BYTE LCR_Bits = 0x03;
            BYTE RxTimeOut_Bits = 0;
            BYTE HDplxDelay_Bits = 0;
            BYTE FlowLvl_Bits = 0;
            BYTE FIFOTrgLvl_Bits = 0;
            BYTE TxFIFOLvl_Bits = 0;
            BYTE RxFIFOLvl_Bits = 0;
            BYTE FlowCtrl_Bits = 0x03;
            BYTE XON1_Bits = 0;
            BYTE XON2_Bits = 0;
            BYTE XOFF1_Bits = 0;
            BYTE XOFF2_Bits = 0;
            BYTE GPIOConfg_Bits = 0;
            BYTE GPIOData_Bits = 0;
//            BYTE PLLConfig_Bits = 0xC6; // PLL: x144, Predever is 1. For 11059200 bod 
            BYTE PLLConfig_Bits = 0x84; // 10 stays for multiplayer 96. Predever is 4. For 921600 bod // for 115200 bod 0x06;
//            BYTE BRGConfig_Bits = 0x10;  // For 11059200 bod mode is 2x.
            BYTE BRGConfig_Bits = 0; // For 115200 and for 921600 mode is normal
//            BYTE DIVLSB_Bits = 1; // For 11059200 bod 
            BYTE DIVLSB_Bits = 6; // For 921600 bod // for 115200 bod2;
            BYTE DIVMSB_Bits = 0;
            BYTE CLKSource_Bits = 0x16;
//--------------------------------------------------------
            WORD SendData;
            WORD Address;
            WORD ReadData;
            Address = MODE2<<8;  //Reset
            SendData = (WRITE_COMMAND << 8 | Address | 0x01); 
            SendSpiToUART(&SendData, &ReadData);
            Sleep(200);
            SendData = (WRITE_COMMAND << 8 | Address | MODE2_Bits); 
            SendSpiToUART(&SendData, &ReadData);
            // REVISION reading confirm reset is ready
            Address = RevID<<8;
            while(true)
            {
               SendData = (READ_COMMAND << 8 | Address); 
               if(SendSpiToUART(&SendData, &ReadData))
               { 
                  if(ReadData == 0xA1)
                     break;
               }
            }
//--------------------------------------------------------------------------------------------------------
          // Start initialize registers of the chip
            //Interrupts
            Address = IRQEn<<8;
            SendData = (WRITE_COMMAND << 8 | Address | IRQEn_Bits); // No interruptions are served
            SendSpiToUART(&SendData, &ReadData);
 //  Read Only           //Address = ISR<<8;
            //SendData = (WRITE_COMMAND << 8 | Address | ISR_LEVEL); // No interruptions are served
            //SendSpiToUART(&SendData);
            Address = LSRIntEn<<8;
            SendData = (WRITE_COMMAND << 8 | Address | LSRIntEn_Bits); // No interruptions are served
            SendSpiToUART(&SendData, &ReadData);
//  Read Only            //Address = LSR<<8;
            //SendData = (WRITE_COMMAND << 8 | Address | LSR_Bits); // No interruptions are served
            //SendSpiToUART(&SendData);
            Address = SpclChrIntEn<<8;
            SendData = (WRITE_COMMAND << 8 | Address | SpclChrIntEn_Bits); // No interruptions are served
            SendSpiToUART(&SendData, &ReadData);
//  Read Only            //Address = SpclCharInt<<8;
            //SendData = (WRITE_COMMAND << 8 | Address | SpclChrIntEn_Bits); // No interruptions are served
            //SendSpiToUART(&SendData);
            Address = STSIntEn<<8;
            SendData = (WRITE_COMMAND << 8 | Address | STSIntEn_Bits); // No interruptions are served
            SendSpiToUART(&SendData, &ReadData);
 //  Read Only           //Address = STSInt<<8;
            //SendData = (READ_COMMAND << 8 | Address | STSInt_Bits); // No report from clock stabilazing
            //SendSpiToUART(&SendData);

            // CLOCK CONFIGURATION
            Address = PLLConfig<<8;
            SendData = (WRITE_COMMAND << 8 | Address | PLLConfig_Bits); 
            SendSpiToUART(&SendData, &ReadData);
            Address = BRGConfig<<8;
            SendData = (WRITE_COMMAND << 8 | Address | BRGConfig_Bits); 
            SendSpiToUART(&SendData, &ReadData);
            Address = DIVLSB<<8;
            SendData = (WRITE_COMMAND << 8 | Address | DIVLSB_Bits); 
            SendSpiToUART(&SendData, &ReadData);
            Address = DIVMSB<<8;
            SendData = (WRITE_COMMAND << 8 | Address | DIVMSB_Bits); 
            SendSpiToUART(&SendData, &ReadData);        
            //set clock source
            Address = CLKSource<<8;
            SendData = (WRITE_COMMAND << 8 | Address | CLKSource_Bits); 
            SendSpiToUART(&SendData, &ReadData);
           
            //check clock ready
            Address = STSInt << 8;
            while(true)
            {
                Sleep(200);
                // Waiting for Clock Ready
                SendData = (READ_COMMAND << 8 | Address); 
                SendSpiToUART(&SendData, &ReadData);
                if((ReadData & 0x20) != 0)
                    break;
            }
            // UART MODES
            Address = MODE1<<8;
            SendData = (WRITE_COMMAND << 8 | Address | MODE1_Bits); 
            SendSpiToUART(&SendData, &ReadData);
            Address = MODE2<<8;
            SendData = (WRITE_COMMAND << 8 | Address | MODE2_Bits); 
            SendSpiToUART(&SendData, &ReadData);
            Address = LCR<<8;
            SendData = (WRITE_COMMAND << 8 | Address | LCR_Bits); 
            SendSpiToUART(&SendData, &ReadData);
            Address = RxTimeOut<<8;
            SendData = (WRITE_COMMAND << 8 | Address | RxTimeOut_Bits); 
            SendSpiToUART(&SendData, &ReadData);
            Address = HDplxDelay<<8;
            SendData = (WRITE_COMMAND << 8 | Address | HDplxDelay_Bits); 
            SendSpiToUART(&SendData, &ReadData);
            Address = IrDA<<8;
            SendData = (WRITE_COMMAND << 8 | Address | IrDA_Bits); 
            SendSpiToUART(&SendData, &ReadData);
            // FIFO CONTROL
            Address = FlowLvl<<8;
            SendData = (WRITE_COMMAND << 8 | Address | FlowLvl_Bits); 
            SendSpiToUART(&SendData, &ReadData);
            Address = FIFOTrgLvl<<8;
            SendData = (WRITE_COMMAND << 8 | Address | FIFOTrgLvl_Bits); 
            SendSpiToUART(&SendData, &ReadData);
//  Read Only          Address = TxFIFOLvl<<8;
//            SendData = (WRITE_COMMAND << 8 | Address | TxFIFOLvl_Bits); 
//            SendSpiToUART(&SendData, &ReadData);
//  Read Only            Address = RxFIFOLvl<<8;
//            SendData = (WRITE_COMMAND << 8 | Address | RxFIFOLvl_Bits); 
//            SendSpiToUART(&SendData, &ReadData);
            // FLOW CONTROL
            Address = FlowCtrl<<8;
            SendData = (WRITE_COMMAND << 8 | Address | FlowCtrl_Bits); 
            SendSpiToUART(&SendData, &ReadData);
            Address = XON1<<8;
            SendData = (WRITE_COMMAND << 8 | Address | XON1_Bits); 
            SendSpiToUART(&SendData, &ReadData);
            Address = XON2<<8;
            SendData = (WRITE_COMMAND << 8 | Address | XON2_Bits); 
            SendSpiToUART(&SendData, &ReadData);
            Address = XOFF1<<8;
            SendData = (WRITE_COMMAND << 8 | Address | XOFF1_Bits); 
            SendSpiToUART(&SendData, &ReadData);
            Address = XOFF2<<8;
            SendData = (WRITE_COMMAND << 8 | Address | XOFF2_Bits); 
            SendSpiToUART(&SendData, &ReadData);
            // GPIOs
            Address = GPIOConfg<<8;
            SendData = (WRITE_COMMAND << 8 | Address | GPIOConfg_Bits); 
            SendSpiToUART(&SendData, &ReadData);
            Address = GPIOData<<8;
            SendData = (WRITE_COMMAND << 8 | Address | GPIOData_Bits); 
            SendSpiToUART(&SendData, &ReadData);

/* for debugging only 
// Write and Read All FIFO
       for(int j =0; j < 120; j++)
       {
            SendData = (WRITE_COMMAND << 8) | j;
            SendSpiToUART(&SendData, &ReadData);
            Sleep(20);
            SendData = ((READ_COMMAND | 0x11) << 8);
            SendSpiToUART(&SendData, &ReadData);
            Sleep(20);
            SendData = ((READ_COMMAND | 0x12) << 8);
            SendSpiToUART(&SendData, &ReadData);
            Sleep(20);
       }
      while(true)
       {
            SendData = (READ_COMMAND << 8) | SendValue_C ;
            SendSpiToUART(&SendData, &ReadData);
            Sleep(20);
        }
*/
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////
//#############################################################################
//
//  AioDioDriver_t::SendSpiToUART - Send one of char to SPI / UART
//
//#############################################################################
bool AioDioDriver_t::SendSpiToUART(WORD *SendValue, WORD *ReceivedValue)
{
	UINT32 WriteData;
	UINT32 ReadData = 0;
	WriteData = *SendValue; 
//    WriteData = WriteData  | *SendValue; // Write is 0
	bool	 bResult = false;
	uint32_t DwordResult;


    DwordResult = DeviceIoControl(hSPI3_CH0, IOCTL_SPI_WRITEREAD, &ReadData, sizeof(WORD), &WriteData, sizeof(WORD), NULL, NULL);
	if(DwordResult == 0)
	{
        bResult = false;
		RETAILMSG(TRUE, (L"Failed reading SPI input %d\r\n", ReadData));
	}
	else
	{		
        RETAILMSG(0, (TEXT("SendSpiToUART W %x  R %x\r\n"), WriteData, ReadData));
        *ReceivedValue = (uint16_t)ReadData;
        bResult = true;
    }
    return bResult;

}
//-----------------------------------------------------------------------------
//#############################################################################
//
//  AioDioDriver_t::WriteSpiToUART - Send array of char to SPI / UART
//
//#############################################################################
bool AioDioDriver_t::WriteSpiToUART(BYTE* WriteValue, BYTE *Lenght)
{
    WORD ReceivedValue;
    WORD SentValue;
    bool ReturnValue = true;
    for(int i = 0; i < *Lenght; i++)
    {
         SentValue = WriteValue[i] | (WRITE_COMMAND << 8 );
         if(!SendSpiToUART(&SentValue, &ReceivedValue))
         {
             ReturnValue = false;
             break;
         }
    }
    return ReturnValue;
}

//#############################################################################
//
//  AioDioDriver_t::ReadSpiFromUART - Read one by one char through SPI from UART FIFO 
//  untill FIFO is empty. Save number of chars to Lenght
//
//#############################################################################
bool AioDioDriver_t::ReadSpiFromUART(BYTE* ReaddValue, BYTE *Lenght)
{
    WORD SentValue;
    WORD ReceivedValue;
    BYTE lenght = GetUART_FIFO();
    bool ReturnValue = true;
    *Lenght = lenght;
    for(BYTE i = 0; i < lenght; i++)
    {
         SentValue = READ_COMMAND << 8;
         if(!SendSpiToUART(&SentValue, &ReceivedValue))
         {
             ReturnValue = false;
             break;
         }
         else
         {
             ReaddValue[i] = (BYTE)ReceivedValue;
         }
    }
    return ReturnValue;

}
//#############################################################################
//
//  AioDioDriver_t::GetUART_FIFO - 
//
//#############################################################################
BYTE AioDioDriver_t::GetUART_FIFO(void)
{
   WORD ReadData;
   WORD SendData = ((READ_COMMAND | RxFIFOLvl) << 8);
   SendSpiToUART(&SendData, &ReadData);
   return (BYTE)ReadData;
}

#endif	// SIGMA_GUI_CPU

