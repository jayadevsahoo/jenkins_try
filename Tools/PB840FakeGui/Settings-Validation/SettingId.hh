
#ifndef SettingId_HH
#define SettingId_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  SettingId - Ids of all of the Settings.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/SettingId.hhv   25.0.4.0   19 Nov 2013 14:27:42   pvcs  $
//
//@ Modification-Log
//
//  Revision: 017   By: rhj    Date: 11-Feb-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//      Modified to support PROX.
//
//  Revision: 016   By: gdc    Date: 24-Sep-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal non-invasive CPAP.
//
//  Revision: 015   By: rhj    Date: 07-July-2008   SCR Number: 6435
//  Project:  LEAK
//  Description:
//       Leak Compensation project-related changes.
//       
//  Revision: 014   By: gdc   Date:  13-Feb-2007    SCR Number: 6237
//  Project:  Trend  
//  Description:
//	Changes to support Trending.
//
//  Revision: 013   By: gdc   Date:  18-Feb-2007    SCR Number: 6144
//  Project:  NIV  
//  Description:
//	Changes to support NIV.
//
//  Revision: 012   By: erm   Date:  23-Apr-2002    DR Number: 5848
//  Project:  VCP
//  Description:
//	Added 'SHADOW_TRACE_ENABLE' id for supporting the enable/disable
//      of shadow traces.
//
//  Revision: 011   By: jja   Date:  04-Mar-2002    DR Number: 5790
//  Project:  VTPC
//  Description:
//	Revise comments for HIGH_INSP_TIDAL_VOL
//
//  Revision: 010   By: quf    Date: 17-Sep-2001    DR Number: 5493
//  Project:  GUIComms
//  Description:
//	Removed code relating to obsoleted print config setting.
//
//  Revision: 009   By: sah   Date:  09-May-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//	VTPC project-related changes:
//      *  obsoleted apnea minute volume
//      *  added volume support level, Vt-to-IBW ratio and Vsupp-to-IBW
//         ratio
//
//  Revision: 008   By: hct    Date: 14-FEB-2000    DR Number: 5493
//  Project:  GUIComms
//  Description:
//	Incorporated initial specifications for GUIComms Project.
//
//  Revision: 007   By: sah    Date: 20-Jul-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//      NeoMode project-related changes:
//      *  obsoleted patient type setting
//
//  Revision: 006   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new BiLevel-specific settings
//
//  Revision: 005   By: sah   Date:  04-Mar-1999    DR Number: 5322
//  Project:  ATC
//  Description:
//	Project-related changes:
//	*  added new ATC-specific settings
//
//  Revision: 004   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 003  By:  syw    Date:  08-Dec-1997    DR Number: none
//  Project:  BILEVEL
//  Description:
// 	BiLevel initial version.  Added PEEP_HI id.
//
//  Revision: 002   By: sah    Date:  22-Sep-1997    DR Number: 2410
//  Project: Sigma (R8027)
//  Description:
//	Added new Atmospheric Pressure Setting as a Batch, non-BD setting.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//            Initial version 
//  
//====================================================================

#include "SettingClassId.hh"

//@ Usage-Classes
//@ End-Usage


class SettingId
{
	public:
		//@ Type:  SettingIdType
		// Ids for all of the Settings.
		enum SettingIdType
		{
			NULL_SETTING_ID      = -1,		  // undefined setting id...
			LOW_SETTING_ID_VALUE =  0,		  // lowest setting value...

			//==================================================================
			// Batch Setting Ids...
			//==================================================================

			LOW_BATCH_ID_VALUE = LOW_SETTING_ID_VALUE,	// must start at 0!!

			// Batch Settings that are sent to Breath-Delivery...
			LOW_BATCH_BD_ID_VALUE = LOW_BATCH_ID_VALUE,
			LOW_BATCH_BD_BOUNDED_ID_VALUE = LOW_BATCH_BD_ID_VALUE, 

			APNEA_FLOW_ACCEL_PERCENT = LOW_BATCH_BD_BOUNDED_ID_VALUE,
			APNEA_INSP_PRESS,	  // Apnea Insp. Press.
			APNEA_INSP_TIME,	  // Apnea Inspiratory Time...
			APNEA_INTERVAL,		  // Apnea Interval Time...
			APNEA_MIN_INSP_FLOW,  // Apnea Minimum Inspiratory Flow...
			APNEA_O2_PERCENT,	  // Apnea Oxygen Percentage...
			APNEA_PEAK_INSP_FLOW, // Apnea Peak Inspiratory Flow...
			APNEA_PLATEAU_TIME,	  // Apnea Plateau Time...
			APNEA_RESP_RATE,	  // Apnea Respiratory Rate...
			APNEA_TIDAL_VOLUME,	  // Apnea Tidal Volume...
			DISCO_SENS,		  // Disconnection Sensitivity...
			EXP_SENS,		  // Expiratory Sensitivity...
			FLOW_ACCEL_PERCENT,	  // Flow Acceleration Percent...
			FLOW_SENS,		  // Flow Sensitivity...
			HIGH_CCT_PRESS,		  // High Circuit Pressure...
			HIGH_INSP_TIDAL_VOL,  // High Inspired Tidal Volume (ATC/PA/VS/VC+)...
			HUMID_VOLUME,	  // Humidifier Volume (ATC)...
			IBW,		  // Ideal Body Weight...
			INSP_PRESS,		  // Inspiratory Pressure...
			INSP_TIME,		  // Inspiratory Time...
			MIN_INSP_FLOW,		  // Minimum Inspiratory Flow...
			OXYGEN_PERCENT,		  // Oxygen Percentage...
			PEAK_INSP_FLOW,		  // Peak Inspiratory Flow...
			PERCENT_SUPPORT,	  // Percent (Work) Support (ATC)...
			PLATEAU_TIME,	  // Plateau Time...
			PEEP,			  // Positive End-Expiratory Press...
			PEEP_HIGH,		  // High PEEP (BiLevel)...
			PEEP_HIGH_TIME,		  // High PEEP time (BiLevel)...
			PEEP_LOW,		  // Low PEEP (BiLevel)...
			PRESS_SENS,		  // Pressure Sensitivity...
			PRESS_SUPP_LEVEL,	  // Pressure Support Level...
			RESP_RATE,		  // Respiratory Rate...
			TIDAL_VOLUME,	  // Tidal Volume...
			TUBE_ID,		  // Tube Id (ATC)...
			VOLUME_SUPPORT,		  // Volume Support (VTPC)...
			HIGH_SPONT_INSP_TIME, // High spontaneous inspiratory time (NIV)...
      HIGH_BATCH_BD_BOUNDED_ID_VALUE = HIGH_SPONT_INSP_TIME,
			LOW_BATCH_BD_DISCRETE_ID_VALUE, 

			APNEA_FLOW_PATTERN = LOW_BATCH_BD_DISCRETE_ID_VALUE,// Apnea Flow Pattern
			APNEA_MAND_TYPE,	  // Apnea Mandatory Type...
			FIO2_ENABLED,	  // FIO2 Option Enabled...
			FLOW_PATTERN,	  // Flow Pattern...
			HUMID_TYPE,		  // Humidification Type...
			NOMINAL_VOLT,	  // Nominal Line Voltage...

			// Maintain the order of the main vent setup settings that follow
			// as they are required to be in order of dependency for proper 
			// processing by their valueUpdate callbacks.
			// BEGIN -- main vent setup settings
			PATIENT_CCT_TYPE,		// Patient Circuit Type...
			VENT_TYPE,				// Vent Type (NIV)... 
			MODE,					// Mode...
			MAND_TYPE,				// Mandatory Type..
			SUPPORT_TYPE,			// Support Type...
			TRIGGER_TYPE,			// Trigger Type...
			// END -- main vent setup settings

			TUBE_TYPE,		  		// Tube Type (ATC)...
            PROX_ENABLED,           // PROX enabled...
			LEAK_COMP_ENABLED,		// Leak Compensation enabled...

			HIGH_BATCH_BD_DISCRETE_ID_VALUE = LEAK_COMP_ENABLED,
			HIGH_BATCH_BD_ID_VALUE          = HIGH_BATCH_BD_DISCRETE_ID_VALUE,

			// Batch Settings that are NOT sent to Breath-Delivery...
			LOW_BATCH_NONBD_ID_VALUE,
			LOW_BATCH_NONBD_BOUNDED_ID_VALUE = LOW_BATCH_NONBD_ID_VALUE,

			APNEA_EXP_TIME = LOW_BATCH_NONBD_BOUNDED_ID_VALUE,// Apnea Exp. Time...
			APNEA_IE_RATIO,		  // Apnea Inspiratory-to-Expiratory Time Ratio
			ATM_PRESSURE,	  // Atmospheric Pressure...
			EXP_TIME,		  // Expiratory Time...
			HIGH_EXH_MINUTE_VOL,  // High Exhaled Minute Volume...
			HIGH_EXH_TIDAL_VOL,	  // High Exhaled Tidal Volume...
			HIGH_RESP_RATE,		  // High Respiratory Rate...
			HL_RATIO,		  // Ratio of Th to Tl (BiLevel)...
			IE_RATIO,		  // Inspiratory-to-Expiratory Time Ratio...
			LOW_CCT_PRESS,		  // Low Circuit Pressure...
			LOW_EXH_MAND_TIDAL_VOL,	  // Low Exhale Mandatory Tidal Volume...
			LOW_EXH_MINUTE_VOL,	  // Low Exhale Minute Volume...
			LOW_EXH_SPONT_TIDAL_VOL,  // Low Exhale Spontaneous Tidal Volume...
			MINUTE_VOLUME,		  // Minute Volume...
			PEEP_LOW_TIME,		  // Low PEEP time (BiLevel)...
			VSUPP_IBW_RATIO,	  // Vsupp-to-IBW Ratio...
			VT_IBW_RATIO,	  // Vt-to-IBW Ratio...

			HIGH_BATCH_NONBD_BOUNDED_ID_VALUE = VT_IBW_RATIO,
			LOW_BATCH_NONBD_DISCRETE_ID_VALUE,

			APNEA_CONSTANT_PARM = LOW_BATCH_NONBD_DISCRETE_ID_VALUE,// Apnea Constant
			CONSTANT_PARM,			  // Constant-During-Rate-Change...
			DCI_BAUD_RATE,			  // DCI's Baud Rate...
			DCI_DATA_BITS,			  // DCI's Data Bits...
			DCI_PARITY_MODE,		  // DCI's Parity Mode...
			COM1_BAUD_RATE=DCI_BAUD_RATE, // COM1's Baud Rate...
			COM1_DATA_BITS,			  // COM1's Data Bits...
			COM1_PARITY_MODE,		  // COM1's Parity Mode...
			MONTH,				  // Month of the Year...
			PRESS_UNITS,		  // Units to use for pressures...
			SERVICE_BAUD_RATE,		  // Service Mode's Baud Rate...
			COM2_BAUD_RATE,			  // COM2's Baud Rate...
			COM2_DATA_BITS,			  // COM2's Data Bits...
			COM2_PARITY_MODE,		  // COM2's Parity Mode...
			COM3_BAUD_RATE,			  // COM3's Baud Rate...
			COM3_DATA_BITS,			  // COM3's Data Bits...
			COM3_PARITY_MODE,		  // COM3's Parity Mode...
			COM1_CONFIG,			  // COM1's configuration
			COM2_CONFIG,			  // COM2's configuration
			COM3_CONFIG,			  // COM3's configuration
			DATE_FORMAT,			  // Date format


			HIGH_BATCH_NONBD_DISCRETE_ID_VALUE = DATE_FORMAT,
			LOW_BATCH_NONBD_SEQUENTIAL_ID_VALUE,

			DAY = LOW_BATCH_NONBD_SEQUENTIAL_ID_VALUE,// Day of the Week...
			DISPLAY_CONTRAST_DELTA,		  // Display Contrast Delta...
			HOUR,			  // Hour of the Day...
			MINUTE,				  // Minute of the Hour...
			YEAR,			  // Year...

			HIGH_BATCH_NONBD_SEQUENTIAL_ID_VALUE = YEAR,

			HIGH_BATCH_NONBD_ID_VALUE = HIGH_BATCH_NONBD_SEQUENTIAL_ID_VALUE,
			HIGH_BATCH_ID_VALUE       = HIGH_BATCH_NONBD_ID_VALUE,

			NUM_BATCH_IDS = (HIGH_BATCH_ID_VALUE - LOW_BATCH_ID_VALUE + 1),

			//==================================================================
			// Non-Batch Setting Ids...
			//==================================================================

			LOW_NON_BATCH_ID_VALUE = HIGH_BATCH_ID_VALUE + 1,
			LOW_NON_BATCH_BOUNDED_ID_VALUE = LOW_NON_BATCH_ID_VALUE,

			PRESS_VOL_LOOP_BASE = LOW_NON_BATCH_BOUNDED_ID_VALUE,//Baseline for P-V Loop Plot...
			TREND_CURSOR_POSITION,								 //Trend cursor position
			HIGH_NON_BATCH_BOUNDED_ID_VALUE = TREND_CURSOR_POSITION,
			LOW_NON_BATCH_DISCRETE_ID_VALUE,

			FLOW_PLOT_SCALE = LOW_NON_BATCH_DISCRETE_ID_VALUE,// Flow Axis Scale...
			PLOT1_TYPE,			  // Type of Waveform Plot #1...
			PLOT2_TYPE,			  // Type of Waveform Plot #2...
			TREND_SELECT_1,			   // Trend display 1
			TREND_SELECT_2,			   // Trend display 2
			TREND_SELECT_3,			   // Trend display 3
			TREND_SELECT_4,			   // Trend display 4
			TREND_SELECT_5,			   // Trend display 5
			TREND_SELECT_6,			   // Trend display 6
			TREND_TIME_SCALE,		   // Trend time scale to display
			TREND_PRESET,			   // Trend preset                   
			SHADOW_TRACE_ENABLE,	  // Enable/Disable Shadow Trace...
			PRESSURE_PLOT_SCALE,	  // Scale of the Pressure Axis...
			TIME_PLOT_SCALE,		  // Scale of the Time Axis...
			VOLUME_PLOT_SCALE,		  // Scale of the Volume Axis...

			HIGH_NON_BATCH_DISCRETE_ID_VALUE  = VOLUME_PLOT_SCALE,
			LOW_NON_BATCH_SEQUENTIAL_ID_VALUE,

			ALARM_VOLUME = LOW_NON_BATCH_SEQUENTIAL_ID_VALUE,// Alarm Volume...
			DISPLAY_BRIGHTNESS,		  // Display Brightness...
			DISPLAY_CONTRAST_SCALE,		  // Display Contrast Scale...

			HIGH_NON_BATCH_SEQUENTIAL_ID_VALUE = DISPLAY_CONTRAST_SCALE,
			HIGH_NON_BATCH_ID_VALUE = HIGH_NON_BATCH_SEQUENTIAL_ID_VALUE,

			NUM_NON_BATCH_IDS = (HIGH_NON_BATCH_ID_VALUE -
								 LOW_NON_BATCH_ID_VALUE + 1),



			//==================================================================
			// Totals...
			//==================================================================

			HIGH_SETTING_ID_VALUE = HIGH_NON_BATCH_ID_VALUE,

			TOTAL_SETTING_IDS = (HIGH_SETTING_ID_VALUE  + 1)
		};

		static inline Boolean  IsId          (
											 const SettingId::SettingIdType settingId
											 );
		static Boolean         IsBoundedId   (
											 const SettingId::SettingIdType settingId
											 );
		static Boolean         IsDiscreteId  (
											 const SettingId::SettingIdType settingId
											 );
		static Boolean         IsSequentialId(
											 const SettingId::SettingIdType settingId
											 );

		static inline Boolean  IsBdId          (
											   const SettingId::SettingIdType bdSettingId
											   );
		static inline Boolean  IsBdBoundedId   (
											   const SettingId::SettingIdType bdSettingId
											   );
		static inline Boolean  IsBdDiscreteId  (
											   const SettingId::SettingIdType bdSettingId
											   );
		static inline Boolean  IsBdSequentialId(
											   const SettingId::SettingIdType bdSettingId
											   );

		static inline Boolean  IsBatchId          (
												  const SettingId::SettingIdType batchId
												  );
		static inline Boolean  IsBatchBoundedId   (
												  const SettingId::SettingIdType batchId
												  );
		static inline Boolean  IsBatchDiscreteId  (
												  const SettingId::SettingIdType batchId
												  );
		static inline Boolean  IsBatchSequentialId(
												  const SettingId::SettingIdType batchId
												  );

		static inline Boolean  IsNonBatchId          (
													 const SettingId::SettingIdType nonBatchId
													 );
		static inline Boolean  IsNonBatchBoundedId   (
													 const SettingId::SettingIdType nonBatchId
													 );
		static inline Boolean  IsNonBatchDiscreteId  (
													 const SettingId::SettingIdType nonBatchId
													 );
		static inline Boolean  IsNonBatchSequentialId(
													 const SettingId::SettingIdType nonBatchId
													 );

		static Boolean  IsAlarmLimitId(
									  const SettingId::SettingIdType alarmSettingId
									  );

		static inline Uint32  ConvertNonBatchIdToIndex(
													  const SettingId::SettingIdType nonBatchId
													  );

#if defined(SIGMA_DEVELOPMENT)
		static const char*  GetSettingName  (const SettingId::SettingIdType id);
		static const char*  GetCategoryName (const SettingId::SettingIdType id);
		static const char*  GetValueTypeName(const SettingId::SettingIdType id);
#endif  // defined(SIGMA_DEVELOPMENT)

		static void  SoftFault(const SoftFaultID softFaultID,
							   const Uint32      lineNumber,
							   const char*       pFileName  = NULL,
							   const char*       pPredicate = NULL);
};


//====================================================================
//
//  Global Constants...
//
//====================================================================

//@ Constant:  NUM_BATCH_SETTING_IDS
// Need this for the bit array class ('BitArray(NUM_BATCH_SETTING_IDS)'),
// because template parameters need to be globals.
enum
{
	NUM_BATCH_SETTING_IDS = SettingId::NUM_BATCH_IDS
};

//@ Constant:  NUM_BD_SETTING_IDS
// Need this for the bit array class ('BitArray(NUM_BD_SETTING_IDS)'),
// because template parameters need to be globals.
enum
{
	NUM_BD_SETTING_IDS = (SettingId::HIGH_BATCH_BD_ID_VALUE -
						  SettingId::LOW_BATCH_BD_ID_VALUE + 1)
};



// Inlined Methods...
#include "SettingId.in"


#endif // SettingId_HH 
