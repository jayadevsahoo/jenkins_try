#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1994, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: UpperScreenSelectArea - Area which contains screen select
// buttons for Waveforms, More Data, Alarm Log, More Alarms, Other
// Screens, Events and Trending.
//---------------------------------------------------------------------
//@ Interface-Description
// This class is a container, derived from the GUI-Foundation Container
// class.  This class contains TabButton objects of various types, as
// well as a Box object used for the "tab" graphics.
//
// There are four main interface methods: 1) the initialize() method performs
// one-time post-construction initialization, 2) the updateButtonDisplay()
// method passes a 'display update' notice to the contained buttons, 3) the
// getWaveformsTabButton() method provides 'global' access to the Waveforms
// button and 4) getOtherScreensTabButton() which does the same for the Other
// Screens button.
//---------------------------------------------------------------------
//@ Rationale
// A convenient organizer for the screen select buttons for the upper
// LCD screen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Normally, a screen select area contains TabButton button objects.
// However, the two alarm-related screen select buttons
// ('alarmLogTabButton_', 'moreAlarmsTabButton_') have unique behavior
// in that their appearance can change based on the state of the current
// alarms and alarm log.  The specialized display behavior is captured
// in two different classes, AlarmLogTabButton and MoreAlarmsTabButton,
// which are both derivations of TabButton.  The updateButtonDisplay()
// method calls the virtual updateDisplay() methods of the specialized
// TabButton's whenever there is a change in the current alarms.
//
// Another specialized TabButton is WaveformsTabButton which alters
// it color scheme depending on the state of the Waveforms subscreen.
//
// When selected, a TabButton normally draws a thick white border around
// itself which appears to merge with the border of the associated
// SubScreenArea.  This gives the illusion that the button is "attached
// to" the SubScreenArea.  To do this, the screen select area must be
// positioned such that it overlaps the subscreen area.  However, the
// screen select area has a dark gray background and the subscreen area
// has a white background.  To compensate for this, we add a fake white
// border along the top of the screen select area ('topLineBox_').
//
// Note that this area does not implement the mutual-exclusivity
// property of screen select buttons.  Instead, this is enforced by the
// SubScreenArea.  The initialize() method properly associates each
// TabButton with its SubScreenArea.
//
// Finally, the OtherScreens button has a specialized need to remain
// selected whenever any subsidiary subscreen to the OtherScreens
// subscreen is displayed.  Thus, the getUpperOtherScreensTabButton()
// method provides access to the button.  The getWaveformsTabButton() is
// needed for similar reasons.
// $[BL00400]
//---------------------------------------------------------------------
//@ Fault-Handling
// Usual assertions to verify code correctness.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/UpperScreenSelectArea.ccv   25.0.4.0   19 Nov 2013 14:08:40   pvcs  $
//
//@ Modification-Log
//
//  Revision: 013  By:  gdc    Date:  10-Dec-2008    SCR Number: 6377
//  Project:  840S
//  Description:
//	Removed DELTA project single screen support.
//
//  Revision: 012   By: rhj   Date:  01-May-2007    SCR Number: 6237
//  Project:  Trend
//  Description:
//      Added a trendManagerStatus() to handle database errors and 
//      moved the software sales labels to MessageArea.cc.
//
//  Revision: 011   By: rhj   Date:  20-Sept-2006    SCR Number: 6236
//  Project:  RESPM
//  Description:
//      Added the ability to get the More Data Tabs Button.
//  
//  Revision: 010  By: gdc    Date: 28-Aug-2000   DCS Number:  5753
//  Project:  Delta
//  Description:
//      Implemented Single Screen option.
//
//  Revision: 009  By:  hhd	   Date:  08-May-2000    DCS Number: 5716 
//  Project: Neonatal 
//  Description:
//  	Extended TempStr_ size to fix problems with Alarm History Log.
//
//  Revision: 008   By: sah    Date:  04-May-2000    DCS Number: 5711
//  Project: NeoMode
//  Description:
//      Based on investigation of actual returned ids of
//      'SoftwareOptions::getDataKeyType()', I modified processing of
//      "ORIGINAL" data keys.  Also, fixed incorrect indentation in
//      'updateButtonDisplay()' method.
//
//  Revision: 007   By: sah    Date:  07-Apr-2000    DCS Number: 5700
//  Project: NeoMode
//  Description:
//      Modified criteria by which the "Property of Mallinckrodt" message
//      is displayed, and its content.
//
//  Revision: 006   By: gdc    Date:  29-Jun-1999    DCS Number: 5327
//  Project: 840 Neonatal
//  Description:
//      Optimized graphics library. Implemented Direct Draw mode for drawing
//      outside of BaseContainer refresh cycle.
//
//  Revision: 005  By:  sah	   Date:  11-Jan-1999    DCS Number: 5321
//  Project:  ATC
//  Description:
//		Added 'SIGMA_DEVELOPMENT' conditionals around this class (unused)
//		'SoftFault()' method.
//
//  Revision: 004  By:  syw   Date:  16-Nov-1998    DR Number: 5252
//       Project:  BiLevel
//       Description:
//			Added requirement tracing.
//
//  Revision: 003  By:  syw	   Date:  26-Aug-1998    DCS Number: 
//  Project:  BiLevel
//  Description:
//		Initial version.  Added "Software Property of NPB" message.
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  wks    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "UpperScreenSelectArea.hh"
#ifdef SIGMA_SUN
#	include "XSetUp.hh"
#endif // SIGMA_SUN

//@ Usage-Classes
#include "Image.hh"
#include "UpperSubScreenArea.hh"
#include "SoftwareOptions.hh"
#include "DataKey.hh"
#include "WaveformsSubScreen.hh"
#include "MoreDataSubScreen.hh"
#include "AlarmLogSubScreen.hh"
#include "MoreAlarmsSubScreen.hh"
#include "UpperOtherScreensSubScreen.hh"
#include "VgaGraphicsDriver.hh"
#include "TrendGraphsSubScreen.hh"
#include "EventSubScreen.hh"
#include "UpperSubScreenArea.hh"
#include "TrendTableSubScreen.hh"
#include "UpperScreen.hh"
//@ End-Usage

//@ Code...

// Initialize static constants.
static const Int32 CONTAINER_X_ = 0;
static const Int32 CONTAINER_Y_ = 431;
static const Int32 CONTAINER_WIDTH_ = 640;
static const Int32 CONTAINER_HEIGHT_ = 49;
static const Int32 BUTTON_WIDTH_ = 91;
static const Int32 BUTTON_HEIGHT_ = 49;
static const Int32 BUTTON_BORDER_ = 3;
static const Int32 WAVEFORMS_BITMAP_X_ = 10;
static const Int32 WAVEFORMS_BITMAP_Y_ = 4;
static const Int32 MORE_DATA_BITMAP_X_ = 27;
static const Int32 MORE_DATA_BITMAP_Y_ = 1;
static const Int32 OTHER_SCREENS_BITMAP_X_ = 20;
static const Int32 OTHER_SCREENS_BITMAP_Y_ = 5;
static const Int32 TREND_BITMAP_X_ = 10;
static const Int32 TREND_BITMAP_Y_ = 4;


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: UpperScreenSelectArea()  [Default Constructor]
//
//@ Interface-Description
// Default constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
// Initializes all members and initializes the colors, sizes, and
// positions of all graphical objects.  Adds all graphical objects to
// the parent container for display.
//
// $[01066] The Upper Screen Select Area displays the following ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

UpperScreenSelectArea::UpperScreenSelectArea(void) :
	waveformsTabButton_(0, 0, BUTTON_WIDTH_, BUTTON_HEIGHT_,
						Button::LIGHT_NON_LIT, BUTTON_BORDER_,
						Button::SQUARE, NULL, NULL), 
	moreDataTabButton_(BUTTON_WIDTH_, 0, BUTTON_WIDTH_,
						BUTTON_HEIGHT_, Button::LIGHT_NON_LIT,
						BUTTON_BORDER_, Button::SQUARE, NULL, NULL), 
	alarmLogTabButton_(BUTTON_WIDTH_ * 2, 0, BUTTON_WIDTH_,
						BUTTON_HEIGHT_, Button::LIGHT_NON_LIT,
						BUTTON_BORDER_, Button::SQUARE, NULL, NULL), 
	moreAlarmsTabButton_(BUTTON_WIDTH_ * 3, 0, BUTTON_WIDTH_,
						BUTTON_HEIGHT_, Button::LIGHT_NON_LIT,
						BUTTON_BORDER_, Button::SQUARE, NULL, NULL), 
	upperOtherScreensTabButton_(BUTTON_WIDTH_ * 4, 0, BUTTON_WIDTH_,
						BUTTON_HEIGHT_, Button::LIGHT_NON_LIT,
						BUTTON_BORDER_, Button::SQUARE, NULL, NULL), 
	mainSettingsTabButton_(BUTTON_WIDTH_ * 5, 0, BUTTON_WIDTH_,
						BUTTON_HEIGHT_, Button::LIGHT_NON_LIT,
						BUTTON_BORDER_, Button::SQUARE, NULL, NULL), 
	trendsTabButton_(BUTTON_WIDTH_ * 5, 0, BUTTON_WIDTH_,
						BUTTON_HEIGHT_, Button::LIGHT_NON_LIT,
						BUTTON_BORDER_, Button::SQUARE,
						NULL, NULL),
	eventTabButton_(BUTTON_WIDTH_ * 6, 0, BUTTON_WIDTH_,
						BUTTON_HEIGHT_, Button::LIGHT_NON_LIT,
						BUTTON_BORDER_, Button::SQUARE,
						NULL, NULL),
    trendBitmap_(Image::RTrendIcon),
	waveformsBitmap_(Image::RWaveformIcon),
	moreDataBitmap_(Image::RMoreDataIcon),
	upperOtherScreensBitmap_(Image::ROtherScreensIcon),
	topLineBox_(0, 0, CONTAINER_WIDTH_, BUTTON_BORDER_)
{
	CALL_TRACE("UpperScreenSelectArea::UpperScreenSelectArea(void)");

	// Size and position the area
	setX(CONTAINER_X_);
	setY(CONTAINER_Y_);
	setWidth(CONTAINER_WIDTH_);
	setHeight(CONTAINER_HEIGHT_);

	// Default background color is extra dark grey
	setFillColor(Colors::EXTRA_DARK_BLUE);

	// Add white line at top
	topLineBox_.setColor(Colors::FRAME_COLOR);
	topLineBox_.setFillColor(Colors::FRAME_COLOR);
	addDrawable(&topLineBox_);

	//
	// Position, then add the bitmaps to the screen select buttons.
	//
	waveformsBitmap_.setX(WAVEFORMS_BITMAP_X_);
	waveformsBitmap_.setY(WAVEFORMS_BITMAP_Y_);
	waveformsTabButton_.addLabel(&waveformsBitmap_);

	moreDataBitmap_.setX(MORE_DATA_BITMAP_X_);
	moreDataBitmap_.setY(MORE_DATA_BITMAP_Y_);
	moreDataTabButton_.addLabel(&moreDataBitmap_);

	upperOtherScreensBitmap_.setX(OTHER_SCREENS_BITMAP_X_);
	upperOtherScreensBitmap_.setY(OTHER_SCREENS_BITMAP_Y_);
	upperOtherScreensTabButton_.addLabel(&upperOtherScreensBitmap_);

	mainSettingsTabButton_.setTitleText(MiscStrs::VENT_CURRENT_SETUP_TAB_BUTTON_LABEL);
	//
	// Add screen select buttons to container.
	//
	addDrawable(&waveformsTabButton_);
	addDrawable(&moreDataTabButton_);
	addDrawable(&alarmLogTabButton_);
	addDrawable(&moreAlarmsTabButton_);
	addDrawable(&upperOtherScreensTabButton_);

	trendBitmap_.setX(TREND_BITMAP_X_);
	trendBitmap_.setY(TREND_BITMAP_Y_);
	trendsTabButton_.addLabel(&trendBitmap_);


    eventTabButton_.setTitleText(MiscStrs::TREND_EVENT_TAB_BUTTON_TITLE);

    // If the Trend Option is enabled, we allow to 
    // display the trend tab button and event tab
    // button.
    if (SoftwareOptions::IsOptionEnabled(SoftwareOptions::TRENDING) )
    {
        addDrawable(&trendsTabButton_);
        addDrawable(&eventTabButton_);

        trendsTabButton_.setToFlat();
        eventTabButton_.setToFlat();
    }


	// Register this class to the trend event registrar
	TrendEventRegistrar::RegisterTarget(this);  

    // Make sure button display is up to date
	updateButtonDisplay();								// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~UpperScreenSelectArea  [Destructor]
//
//@ Interface-Description
// Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

UpperScreenSelectArea::~UpperScreenSelectArea(void)
{
	CALL_TRACE("UpperScreenSelectArea::UpperScreenSelectArea(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: initialize
//
//@ Interface-Description
// One-time post-construction initialization which associates each
// screen select button with its subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Called once at application initialization time to register the
// subscreens owned by the upper subscreen area with their screen select
// buttons.
//
// $[01067] The buttons shall form a mutually exclusive group ...
// $[00501] GUI provide access to the alarm history log within 1 navigational
// step.
// $[00502] GUI provide access to the more alarm function within 1 navigational
// step.
//---------------------------------------------------------------------
//@ PreCondition
// The construction of the UpperScreen::RUpperScreen object must be complete.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
UpperScreenSelectArea::initialize(void)
{
	CALL_TRACE("UpperScreenSelectArea::initialize(void)");

	waveformsTabButton_.setSubScreen(
		UpperSubScreenArea::GetWaveformsSubScreen());
	moreDataTabButton_.setSubScreen(
		UpperSubScreenArea::GetMoreDataSubScreen());
	alarmLogTabButton_.setSubScreen(
		UpperSubScreenArea::GetAlarmLogSubScreen());
	moreAlarmsTabButton_.setSubScreen(
		UpperSubScreenArea::GetMoreAlarmsSubScreen());
	upperOtherScreensTabButton_.setSubScreen(
		UpperSubScreenArea::GetUpperOtherScreensSubScreen());

    trendsTabButton_.setSubScreen(
        UpperSubScreenArea::GetTrendGraphsSubScreen());

    eventTabButton_.setSubScreen(
        UpperSubScreenArea::GetEventSubScreen());
														// $[TI1]
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: trendManagerStatus
//
//@ Interface-Description
//  This method provides the trend manager status based on the 
//  given parameters provided from TrendEventTarget class.
//  
//     isEnabled - Will the manager process data request messages
//     isRunning - Is the manager collecting trend data
//  
//  If the manager cannot process data request messages, this method
//  disables the trend and event tab buttons.  It enables the trend 
//  and event tab buttons when the manager can process data request messages.
//---------------------------------------------------------------------
//@ Implementation-Description
//  When the Trending option is installed, the isEnabled flag is TRUE,
//  then enable the Trend tab button.  If isRunning flag is TRUE, the
//  Trending option is installed, and the isEnabled flag is TRUE, then 
//  enable the Event tab button.  Disable both Event and Trend tab buttons, 
//  when the isEnabled flag is set to FALSE.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void UpperScreenSelectArea::trendManagerStatus( Boolean isEnabled, Boolean isRunning )
{ 

	// Only update the trend and event tab buttons when 
	// the Trending option is enabled.
	if (SoftwareOptions::IsOptionEnabled(SoftwareOptions::TRENDING))
	{

		// Enable the trend tab button when 
		// the trend manager status is enabled.
		if (isEnabled)
	    {
		    if (trendsTabButton_.getButtonState() == Button::FLAT)
			{
			    trendsTabButton_.setToUp();
			}
			
			// Enable the event tab button when the 
			// trend manager status is enabled and running.
			if (isRunning && (eventTabButton_.getButtonState() == Button::FLAT))
			{
				eventTabButton_.setToUp();           			  
			}
			
		}
		else
		{

			// If the Table, Graph or Event subscreen is displayed,
			// we need to deactivate it and set the trend and event tabs
			// flat.
			if ( UpperSubScreenArea::GetTrendGraphsSubScreen()->isVisible() ||
				 UpperSubScreenArea::GetTrendTableSubScreen()->isVisible() ||
				 UpperSubScreenArea::GetEventSubScreen()->isVisible()
			   )
			{
				UpperScreen::RUpperScreen.getUpperSubScreenArea()->deactivateSubScreen();
			}

			trendsTabButton_.setToFlat();
			eventTabButton_.setToFlat();
		}

	}

}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateButtonDisplay
//
//@ Interface-Description
// Forces display updates for the Waveforms, Alarm Log and More Alarms
// tab buttons.
//---------------------------------------------------------------------
//@ Implementation-Description
// Whenever there is a change in the current alarms, this method is called to
// update the appearance of the alarm-related screen select buttons.  It is
// also called whenever the Waveforms subscreen switches between displaying
// waveforms and the plot setup screen.  Calls the virtual
// TabButton::updateDisplay() methods of 'waveformsTabButton_',
// 'alarmLogTabButton_' and 'moreAlarmsTabButton_' members.
////---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
UpperScreenSelectArea::updateButtonDisplay(void)
{
	CALL_TRACE("UpperScreenSelectArea::updateButtonDisplay(void)");

    //
	// Pass the message along to the contained TabButton objects.
	//
	waveformsTabButton_.updateDisplay();
	//TODO E600 Alarm logs do not seem to work
	//alarmLogTabButton_.updateDisplay();
	moreAlarmsTabButton_.updateDisplay();
}




//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
UpperScreenSelectArea::SoftFault(const SoftFaultID  softFaultID,
								 const Uint32       lineNumber,
								 const char*        pFileName,
								 const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, UPPERSCREENSELECTAREA,
							lineNumber, pFileName, pPredicate);
}

