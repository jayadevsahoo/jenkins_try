
#ifndef TrendTableSubScreen_HH
#define TrendTableSubScreen_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: TrendTableSubScreen - Trend Table sub-screen
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/TrendTableSubScreen.hhv   25.0.4.0   19 Nov 2013 14:08:40   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By:  gdc	   Date:  30-Jun-2007    DCS Number: 6237
//  Project:  TREND
//  Description:
//		Trend project initial version.
//
//====================================================================
#include "TrendSubScreen.hh"
#include "LogTarget.hh"
#include "TrendEventTarget.hh"

//@ Usage-Classes
#include "DropDownSettingButton.hh"
#include "FocusButton.hh"
#include "GuiAppClassIds.hh"
#include "ScrollableMenuSettingButton.hh"
#include "SubScreenTitleArea.hh"
#include "TextField.hh"
#include "TouchableText.hh"
#include "TrendCursorSlider.hh"
#include "TrendDataSet.hh"
#include "TrendingLog.hh"
//@ End-Usage

class TrendDataSetAgent;

class TrendTableSubScreen 
:	public TrendSubScreen, 
	public LogTarget, 
	public TrendEventTarget
{
public:
	TrendTableSubScreen(SubScreenArea* pSubScreenArea);
	~TrendTableSubScreen(void);

	// TrendSubScreen pure virtuals
	virtual void updateCursorPosition(void);
	virtual void disableScrolling(void);
	virtual void update(void);

	// SubScreen virtuals via TrendSubScreen
	virtual void activate(void);
	virtual void deactivate(void);

	// ButtonTarget virtuals via TrendSubScreen
	virtual void buttonDownHappened(Button *pButton,
									Boolean byOperatorAction);

	virtual void buttonUpHappened(Button *pButton,
								  Boolean byOperatorAction);

	// SettingObserver virtual via TrendSubScreen
	void valueUpdate(const Notification::ChangeQualifier qualifierId,
					 const SettingSubject* pSubject);

	// TrendEventTarget virtual
	virtual void trendDataReady(TrendDataSet& rTrendDataSet);

	// LogTarget virtual
	virtual void getLogEntryColumn(Uint16 entryNumber, Uint16 columnNumber,
								   Boolean &rIsCheapText, StringId &rString1,
								   StringId &rString2, StringId &rString3,
								   StringId &rString1Message);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:
	// TrendSubScreen pure virtuals
	virtual Boolean isAnyButtonPressed_(void) const;
	virtual void requestTrendData_(void);

private:
	// these methods are purposely declared, but not implemented...
	TrendTableSubScreen(void);					 // not implemented
	TrendTableSubScreen(const TrendTableSubScreen&);	// not implemented...
	void operator=(const TrendTableSubScreen&);	 // not implemented...

	enum
	{
		NUM_TREND_TABLE_COLUMNS_ = 8,
		NUM_TREND_TABLE_SETTING_BUTTONS_ = 8
	};

	//@ Data-Member: graphScreenNavButton_
	// The button to switch to the Trend graph view sub-screen
	TextButton graphScreenNavButton_;

	//@ Data-Member: presetsButton_
	// The button to automatically populate the trend parameters
    // with preset values.
	DropDownSettingButton presetsButton_;

	//@ Data-Member: printButton_
	// The button to print the screen
	TextButton printButton_;

	//@ Data-Member: printingText_;
	// The text that flashes the 'PRINTING' message
	TextField printingText_;

	//@ Data-Member: pTable_
	// The Trending log that displays the trending table and manages its
	// scrolling and selection mechanisms
	TrendingLog trendingLog_;

	//@ Data-Member: timeScaleButton_
	// The button to adjust the time scale of the trend data table
	ScrollableMenuSettingButton  timeScaleButton_;

    //@ Data-Member: trendSelectButton1_
    // Trend parameter setting button
	ScrollableMenuSettingButton  trendSelectButton1_;

    //@ Data-Member: trendSelectButton2_
    // Trend parameter setting button
	ScrollableMenuSettingButton  trendSelectButton2_;

    //@ Data-Member: trendSelectButton3_
    // Trend parameter setting button
	ScrollableMenuSettingButton  trendSelectButton3_;

    //@ Data-Member: trendSelectButton4_
    // Trend parameter setting button
	ScrollableMenuSettingButton  trendSelectButton4_;

    //@ Data-Member: trendSelectButton5_
    // Trend parameter setting button
	ScrollableMenuSettingButton  trendSelectButton5_;

    //@ Data-Member: trendSelectButton6_
    // Trend parameter setting button
	ScrollableMenuSettingButton  trendSelectButton6_;

	//@ Data-Member: eventDetailButton_
	// EVENT Detail button - active when cursor is on an event
	// pops up the event detail window when pressed
	FocusButton  eventDetailButton_;

	//@ Data-Member: trendCursorSlider_
	// Contains the trend cursor button and timeline
	TrendCursorSlider trendCursorSlider_;

	//@ Data-Member: datasetColumn_
	// The column in the dataset for each table column 
	Int32 datasetColumn_[NUM_TREND_TABLE_COLUMNS_];

	//@ Data-Member: isDataRequestPending_
	// TRUE if a data request has been issued to the TrendDataSetAgent
	Boolean isDataRequestPending_;

	//@ Data-Member: isCallbackActive_
	// TRUE if a menu callback is active that will inhibit screen refresh
	Boolean isCallbackActive_;

	// The subscreen buttons.
	SettingButton*  arrSettingButtonPtrs_[NUM_TREND_TABLE_SETTING_BUTTONS_ + 1];

	//@ Data-Member: isTableColumnErased_
	// latched TRUE when a table column has been erased due to a setting change
	Boolean isTableColumnErased_[NUM_TREND_TABLE_COLUMNS_];

	//@ Data-Member: areAllTableColumnsErased_
	// latched TRUE when all table columns have been erased due to a setting change
	Boolean areAllTableColumnsErased_;

};

#endif // TrendTableSubScreen_HH 
