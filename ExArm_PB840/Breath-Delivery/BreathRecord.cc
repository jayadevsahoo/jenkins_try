#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================
// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: BreathRecord - Records vital information about delivered and
//		current breaths.
//---------------------------------------------------------------------
//@ Interface-Description
//   This class serves as a repository for information that is required
//   to be stored about breaths delivered to the patient.  Some of the
//   data stored is unique to each breath, and some are not (static data).
//   This class is updated every BD cycle, on transition from inspiration
//   to exhalation, and exhalation to inspiration.
//   All of the information stored here is used by the breath delivery
//   subsystem.  At the end of each breath, this class updates the
//   BreathData class used to transmit required "patient data" to the
//   Patient-Data-Management subsystem, with the data collected and stored
//   here.  The data in the record transmitted to BreathData is mixed,
//   partially from the new breath, and partially from the last breath,
//   since that is how the user interface requires it to be transmitted.
//   However, the data in the records stored in the BreathSet all pertains
//   to one breath.
//---------------------------------------------------------------------
//@ Rationale
//   This class provides the storage necessary for data used by breathing,
//   scheduling and triggering algorithms in the breath delivery subsystem.
//   It also provides updates to the BreathData class used for communicating
//   patient data to the Patient-Data subsystem.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This class contains information that is stored for each breath (non-static),
//   and also information that is stored just for the current active
//   breath (static).
//   Every BD cycle, data is collected that is necessary to calculate
//   the mean and peak data that is required.  These values are updated
//   on the transitions to inspiration and exhalation, where applicable.
//   Access methods are provided for non-local data members.
//   At the end of each breath, this class must update the
//   BreathData object with the data that is to be communicated to the
//   Patient-Data-Management subsystem.
//---------------------------------------------------------------------
//@ Fault-Handling
//   n/a.
//---------------------------------------------------------------------
//@ Restrictions
//   The calculation of the following data is  completed
//   on transition to inspiration:
//	complianceExhVolume_
//      expiredVolume_
//      maxExpiredVolume_
//      breathDuration_
//      AirwayPressure_
//      PeakAirwayPressure_
//      meanAirwayPressure_
//      PeakExpiratoryFlow_
//      EndExpPressureComp_
//      EndExpPressurePatData_
//   The calculation of the following data is completed after the transition
//   to exhalation:
//      inspiredVolume_
//      inspSideMinVol_
//      InspiratoryTime_
//      EndInspPressure_
//      EndInspPressurePatData_
//      EndInspPressureComp_
//      PeakAirwayPressure_
//      IsExhFlowFinished_
//   The following data is accurate at any time during the breath:
//      breathType_
//      mandType_
//      schedulerId_
//      PhaseType_
//      IsPhaseRestricted_
//      IntervalNumber_
//      IsPeepRecovery_
//      IsMandBreath_
//      LastEndExpPressure_
//      AirwayPressure_
//   The following data are used locally:
//      PeakInspiratoryFlow_
//      AirwayPressureAccum_
//      FlowTarget_
//	AirFlowSum_
//	O2FlowSum_
//      PrevNetFlow_
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/BreathRecord.ccv   25.0.4.0   19 Nov 2013 13:59:40   pvcs  $
//
//@ Modification-Log
//
//  Revision: 101  By:  rhj    Date:  05-Oct-2010    SCR Number: 6627
//  Project:  PROX
//  Description:
//      Added WyePressEstPrev_ and WyePressEst_
//
//  Revision: 100   By:  rhj    Date:  21-Sept-2010    SCR Number: 6627
//  Project:  PROX
//  Description:
//      Modified to calculate prox pressure substitue regardless
//      whether prox is enabled or disabled.
//
//  Revision: 099   By:  rhj    Date:  20-Apr-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//      Added Prox data to the waveform packet
//
//  Revision: 098   By:   rhj    Date: 12-May-2009      SCR Number: 6511
//  Project:  840S
//  Description:
//      Added RLeakCompMgr.isBreathingMode method to prevent
//      overriding netflow calculations.
//
//  Revision: 097   By:   rhj    Date: 16-Mar-2009     SCR Number: 6492
//  Project:  840S
//  Description:
//      Added EndDryExhFlow_ data member to keep track of the
//      dry exhalation flow at the end of exhalation.
//
//  Revision: 096   By: rhj   Date:  19-Jan-2009    SCR Number: 6455
//  Project:  840S
//  Description:
//		Enhanced Leak Compensation algorithms to handle no leaks.
//
//  Revision: 095   By:   rhj    Date: 07-Nov-2008      SCR Number: 6435
//  Project:  840S
//  Description:
//       Leak Compensation project-related changes.
//
//  Revision: 094   By:   rhj    Date: 05-March-2007      DR Number: 6359
//  Project:  RESPM
//  Description:
//        Resolve merge conflict for RM to Baseline
//
//  Revision: 093   By: gdc   Date:  22-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Added RM data items.
//
//  Revision: 092  By: gdc     Date:  17-June-2005   DR Number:6170
//  Project:  NIV2
//  Description:
//   Modifications to support sending trigger id to GUI patient-data
//
//  Revision: 091  By: rhj     Date:  27-Jan-2005   DR Number:6144
//  Project:  NIV
//  Description:
//      Update capturing PEEP for Patient Data in NIV.
//
//  Revision: 090  By: jja     Date:  05-Sep-2001    DR Number: 5748
//  Project:  GUIComms
//  Description:
//		Added trueExhTime_ data member to keep track of actual exh time
//		for the IsPhaseRestricted logic.  The NULL_INSPIRATION during
//		Bilevel was continuing the exh time from high peep to low peep,
//		thus setting IsPhaseRestricted to false causing a stacked breath.
//
//  Revision: 089   By: erm   Date:  23-Apr-2002    DR Number: 5848
//  Project:  VCP
//  Description:
//      Added support for the new 'isApneaActive_' and 'isErrorState_' fields.
//
//  Revision: 088   By: syw   Date:  01-Jun-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//		PAV project-related changes:
//		Added lung pressure and lung flow (or wye), lung volume.
//		Changes to accommodate PAV_INSPIRATORY_PAUSE enum.
//		Added isPeepRecovery to waveforms.
//
//  Revision: 087  By: syw     Date:  09-Oct-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//		VTPC project-related changes:
//		Avoid zeroing EndInspPressureComp_ so value will be valid until
//			the next end insp event.
//		Added filtering to lung pressure
//
//  Revision: 086  By: syw     Date:  07-Sep-2000    DR Number: 5748
//  Project:  Delta
//  Description:
//		Added trueExhTime_ data member to keep track of actual exh time
//		for the IsPhaseRestricted logic.  The NULL_INSPIRATION during
//		Bilevel was continuing the exh time from high peep to low peep,
//		thus setting IsPhaseRestricted to false causing a stacked breath.
//
//  Revision: 085  By: syw     Date:  31-Oct-2000    DR Number: 5793
//  Project:  Metabolics
//  Description:
//		Changed spiro terminate condition to reflect new controller.
//
//  Revision: 084  By: jja     Date:  27-Apr-2000    DR Number: 5715
//  Project:  NeoMode
//  Description:
//		Remove 6ml offset in VCV volumes for Neonatal circuit.
//
//  Revision: 083  By: jja     Date:  24-Apr-2000    DR Number: 5708
//  Project:  NeoMode
//  Description:
//		Revise Pmean to be averaged value instead of breath to breath.
//              Delete static MeanAirwayPressure_, replace with
//              meanAirwayPressure_, store save data in BreathRecord.
//
//  Revision: 082  By: jja     Date:  19-Apr-2000    DR Number: 5707
//  Project:  NeoMode
//  Description:
//		For PCV with Neo circuit, do not use PCV_TIME_FACTOR when
//              calculatingComplianceBtpsVol.
//
//  Revision: 081  By: syw     Date:  17-Feb-2000    DR Number: 5621
//  Project:  NeoMode
//  Description:
//		Eliminate setting net flow to zero when IntervalNumber_ == 0.
//
//  Revision: 080  By: sah     Date:  22-Sep-1999    DR Number: 5327, 5599
//  Project:  NeoMode
//  Description:
//      NeoMode-specific changes:
//      *  added neonatal-specific minimum flow value
//      *  added exhalation compliance factor, whereby neonatal has a
//         partial compliance for exhalation capacitance
//		* added fix for IsPhaseRestricted.  Needed to subtract CYCLE_TIME_MS
//			from MIN_EXH_TIME since IsPhaseRestricted is held off for 205 msecs.
//
//  Revision: 079  By: syw     Date:  18-Jul-1999    DR Number: 5471, 5481
//  Project:  ATC
//  Description:
//		Update BreathTypeDisplay to manage the breath type bar on the GUI.
//
//  Revision: 078  By:  syw    Date:  07-Jul-1999    DR Number: DCS 5464
//       Project:  ATC
//       Description:
//			Added crossing condition to terminating spiro.
//
//  Revision: 077  By: syw     Date:  26-Apr-1999    DR Number: 5322
//  Project:  ATC
//  Description:
//      In NewCycle(), changed local variables for for Pwye estimates
//      based on Pi and Pe to data members.
//		Eliminate the conditions to reset exh volume during bilevel since
//		new PeepController class is not dependent on isPulseFlowOn().
//
//  Revision: 076  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
// 	merged /840/Baseline/Breath-Delivery/vcssrc/BreathRecord.ccv  Rev "Color" (1.134.1.0)
//
//  Revision: 075  By:  syw   Date:  23-Nov-1998    DR Number: 5236
//       Project:  BiLevel
//       Description:
//			Added conditions to reset exh volume during bilevel.
//
//  Revision: 074  By:  syw   Date:  12-Nov-1998    DR Number: 5236
//       Project:  BiLevel
//       Description:
//			Retain EndInspPressureComp_ value if NULL_INSPIRATION (back to back
//			exhalation phases during Bilevel).  Update only if back to back
//			exhalation is from a mandatory BiLevel breath.
//
//  Revision: 073  By:  syw   Date:  12-Nov-1998    DR Number: 5252
//       Project:  BiLevel
//       Description:
//			Added requirement tracing.
//
//  Revision: 072  By:  iv   Date:  13-Aug-1998    DR Number: None
//       Project:  Sigma (840)
//       Description:
//			Fixed merge problems.
//
//  Revision: 071  By:  syw   Date:  16-Jul-1998    DR Number: DCS 5120
//       Project:  Sigma (840)
//       Description:
//			Added IsPeepTransition_ data member and access methods.
//
//  Revision: 070  By:  syw    Date:  07-Jul-1998    DR Number: DCS 5157
//       Project:  Sigma (840)
//       Description:
//			Added handle for BILEVEL_PAUSE_PHASE.
//
//  Revision: 069  By: iv     Date:   30-Jun-1998    DR Number: DCS 5114
//  	Project:  Color
//		Description:
//			In method newCycle(): changed the condition for measuring
//          EndExpPressurePatData_ so that the pressure measured is not
//          the trigger pressure.
//
//  Revision: 068  By:  iv    Date:   28-May-1998    DR Number: 5100
//       Project:  Sigma (840)
//       Description:
//			In newCycle(): Change bound for the comparison of the 2 filtered flow
//          signals from 0.01 to 0.2. Also, added a peak detection for the 1st
//          filtered flow signal and add a condition for end exhalation flow to
//          check for 20% of the peak.
//
//  Revision: 067  By: gdc     Date:  18-May-1998    DR Number:
//  	Project:  Color
//		Description:
//			Added breath type to WaveformRecord.
//
//  Revision: 066  By: syw     Date:  19-May-1998    DR Number: DCS 5095
//  	Project:  Sigma (840)
//		Description:
//			Expand tolerance on when to use maxExpiredVolume_.
//
//  Revision: 065  By: syw     Date:  14-May-1998    DR Number: DCS 5095
//  	Project:  Sigma (840)
//		Description:
//			Update maxExpiredVolume_ and use it for spirometry.
//
//  Revision: 064  By:  syw    Date:  07-May-1998    DR Number: 5087
//       Project:  Sigma (840)
//       Description:
//			Add PeakAirwayPressure_ and AirwayPressureAccum_ updates to
//			EXPIRATORY_PAUSE.
//
//  Revision: 063  By:  syw    Date:  07-May-1998    DR Number: 5086
//       Project:  Sigma (840)
//       Description:
//			Maintain the inspTime_ as a separate counter in newCycle() in order
//			to handle insp pause during biLevel.  Prevent initializing
//			FirstCycleOfExhalation_ in newExhalation if noInspiration_ is
//			TRUE to avoid starting exhalation again.
//
//  Revision: 062  By:  syw    Date:  13-Apr-1998    DR Number: DCS 5070
//       Project:  Sigma (840)
//       Description:
//			Removed code that limitted negative volumes on waveform display.
//
//  Revision: 061  By:  iv    Date:  18-Jan-1998    DR Number: None
//       Project:  Sigma (840)
//       Description:
//          Bilevel initial version.
//          Eliminate SpiroTime_.  Added INSPIRATORY_PAUSE case to new cycle.
//			Added IsMandBreath_.
//
//  Revision: 060  By: iv     Date:  20-Oct-1997    DR Number: DCS 2572
//  	Project:  Sigma (840)
//		Description:
//			In method newInspiration(), use insp time dependent variable
//          for compliance compensation.
//
//  Revision: 059  By: iv     Date:  10-Oct-1997    DR Number: DCS 2414
//  	Project:  Sigma (840)
//		Description:
//			In method calculateComplianceBtpsVol(), set expired volume to
//          zero if it was negative.
//
//  Revision: 058  By: syw    Date:  25-Sep-1997    DR Number: DCS 1135
//  	Project:  Sigma (840)
//		Description:
//			Added variable alpha filter to waveform display data.  The
//			filter has an alpha of 1.0 at the beginning of each inspiration
//			exhalation, and inspiratory pause phase.  The alpha is reduced
//			by a contant factor each cycle.  Added
//			RPressureXducerAutozero.getIsAutozeroInProgress()
//			condition to autozero side condition to prevent the DEFAULT
//			value from being set when AutozeroInProgress is true.
//
//  Revision: 057  By: syw    Date:  10-Sep-1997    DR Number: DCS 2421
//  	Project:  Sigma (840)
//		Description:
//			Use insp estimate if exh side autozero, use exh side estimate if
//			insp side autozero.
//
//  Revision: 056  By: iv    Date:  09-Sep-1997    DR Number: DCS 2417
//  	Project:  Sigma (840)
//		Description:
//			In method newCycle() - changed the way EEP for patient data is
//          calculated.
//
//  Revision: 055  By: iv    Date:  03-Sep-1997    DR Number: NONE
//  	Project:  Sigma (840)
//		Description:
//			Reworked per formal code review.
//
//  Revision: 054  By: iv    Date:  20-Aug-1997    DR Number: DCS 2405
//  	Project:  Sigma (840)
//		Description:
//			Fixed testable items and formating.
//
//  Revision: 053  By: syw   Date:  12-Aug-1997    DR Number: DCS 2356
//  	Project:  Sigma (840)
//		Description:
//			Use adiabatic factor in compliance compensation algorithm.
//
//	Revision: 052  By:  syw    Date:  12-Aug-1997    DR Number: DCS 2357
//      Project:  Sigma (R8027)
//      Description:
//			Use only insp time in call to calculateComplianceBtpsVol() ;
//
//	Revision: 051  By:  syw    Date:  28-Jul-1997    DR Number: DCS 2331
//      Project:  Sigma (R8027)
//      Description:
//			Changed the inspTime value for pressure based to
//				RCircuitCompliance.getLowFlowTime() * 10.0 / 3.0 ;
//
//	Revision: 050  By:  syw    Date:  28-Jul-1997    DR Number: DCS 2329
//      Project:  Sigma (R8027)
//      Description:
//			Changed value where the two filters converge to terminate spiro.
//
//	Revision: 049  By:  iv    Date:  24-Jul-1997    DR Number: DCS 2301
//      Project:  Sigma (R8027)
//      Description:
//			Changed newCycle() to eliminate the impact of autozero on pressure
//          calculations.
//
//	Revision: 048  By:  iv    Date:  18-Jul-1997    DR Number: DCS 2301
//      Project:  Sigma (R8027)
//      Description:
//			Corrected formula for airway pressure.
//
//	Revision: 047  By:  syw    Date:  17-Jul-1997    DR Number: DCS 2309
//      Project:  Sigma (R8027)
//      Description:
//			Added condition to terminate spiro after condition is met for 20 msec.
//
//	Revision: 046  By:  syw    Date:  15-Jul-1997    DR Number: DCS 2297
//      Project:  Sigma (R8027)
//      Description:
//			Remove local declaration of ExhTime_ in newCycle().
//
//	Revision: 045  By:  syw    Date:  03-Jul-1997    DR Number: DCS 2282
//      Project:  Sigma (R8027)
//      Description:
//			Remved call to RExhFlowSensor.updateSpiroTemperature().  Removed
//			exhDrySpiroFlow, use exhDryFlow instead.  Removed spiroNetFlow,
//			same as netFlow.
//
//  Revision: 044  By: syw   Date:  23-May-1997    DR Number: DCS 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 043  By:  syw    Date: 19-May-1997    DR Number: DCS 1620
//       Project:  Sigma (R8027)
//       Description:
//       		Since Btps::GetInspBtpsCf() can return 0.0, it is not wise to assert.
//				If it is zero, set pLastBreath->inspiredVolume_ to 0.0
//
//  Revision: 042  By:  sp    Date: 16-Apr-1997    DR Number: DCS 1975, 1746
//       Project:  Sigma (R8027)
//       Description:
//             Change the accuracy for comparing the flow target and desired flow.
//             Correct condition for restricted phase.
//
//  Revision: 041  By:  sp    Date: 15-Apr-1997    DR Number: DCS 1012, 1039, 1403, 1414
//       Project:  Sigma (R8027)
//       Description:
//             Code was corrected to reflect above DR numbers.
//             (entered for tracability purposes.)
//
//  Revision: 040  By:  sp    Date: 10-Apr-1997    DR Number: DCS 1867
//       Project:  Sigma (R8027)
//       Description:
//             Fixed exhale spont min vol calculation.
//
//  Revision: 039  By:  sp    Date: 1-Apr-1997    DR Number: DCS NONE
//       Project:  Sigma (R8027)
//       Description:
//             Inspection rework.
//
//  Revision: 038  By:  sp    Date: 27-Mar-1997    DR Number: DCS 1780
//       Project:  Sigma (R8027)
//       Description:
//             Compliance volume based on time.
//
//  Revision: 037  By:  syw    Date: 13-Mar-1997    DR Number: DCS 1780
//       Project:  Sigma (R8027)
//       Description:
//             Changed rExhDryFlowSensor.getValue() to rExhFlowSensor.getDryValue().
//             Call rExhFlowSensor.updateSpiroTemperature() when spiro stops integrating.
//
//  Revision: 036  By:  syw    Date: 11-Mar-1997    DR Number: DCS 1827
//       Project:  Sigma (R8027)
//       Description:
//             Inform exh temp sensor to update modified temperature.
//
//  Revision: 035  By:  sp    Date: 18-Feb-1997    DR Number: DCS 1746, 1780
//       Project:  Sigma (R8027)
//       Description:
//             Added base flow to the restricted phase condition (1746).
//             Changed termination condition of spirometry (1780).
//
//  Revision: 034  By:  sp    Date: 13-Feb-1997    DR Number: NONE
//       Project:  Sigma (R8027)
//       Description:
//             Add unit test item (unit test review).
//
//  Revision: 033  By:  sp    Date: 20-Dec-1996    DR Number: NONE
//       Project:  Sigma (R8027)
//       Description:
//             Inspection rework.
//
//  Revision: 032  By:  sp    Date: 12-Dec-1996    DR Number: DCS 1611
//       Project:  Sigma (R8027)
//       Description:
//             Add PreviousBreathType_.
//
//  Revision: 031  By:  sp    Date: 24-Oct-1996    DR Number: NONE
//       Project:  Sigma (R8027)
//       Description:
//             Inspection rework.
//
//  Revision: 030  By:  sp    Date:  17-Oct-1996    DR Number: DCS 976
//       Project:  Sigma (R8027)
//       Description:
//             Do not send end of breath data when peep recovery is true.
//
//  Revision: 029  By:  iv    Date:  12-Oct-1996    DR Number: DCS 1293
//       Project:  Sigma (R8027)
//       Description:
//             newCycle(): Changed the way airway pressure is calculated -
//             airwayPressureFiltered.
//             newExhalation(): set EndInspPressureComp_ = AirwayPressure_ .
//             Used AirwayPressure_ for EndInspPressure_ and LastEndExpPressure_.
//
//  Revision: 028  By:  sp    Date:  10-Oct-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Added const MIN_EXH_TIME.
//
//  Revision: 027  By:  sp    Date:  2-Oct-1996    DR Number: DCS 1293
//       Project:  Sigma (R8027)
//       Description:
//             EndInspPressureComp_ and EndExpPressureComp_ are updated
//             with the same formula as patient data with non-filtered
//             pressure value.
//
//  Revision: 026  By:  sp    Date:  9-Oct-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Added data member RInspResistance_ and RExhResistance_.
//             Removed unit test methods.
//
//  Revision: 025  By:  sp    Date:  23-Sept-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             EndInspPressurePatData_ is updated every inspiration cycle.
//             Add EndInspPressure_.
//
//  Revision: 024  By:  sp    Date:  20-Sept-1996    DR Number: DCS 1293
//       Project:  Sigma (R8027)
//       Description:
//             Interface with NovRamManager to get tubing resistance.
//
//  Revision: 023  By:  sp    Date:  19-Sept-1996    DR Number: DCS 1293
//       Project:  Sigma (R8027)
//       Description:
//             Changed GetLastEndExpPressurePatData to GetLastEndExpPressure.
//             New formula for the calculation of patient's pressure.
//
//  Revision: 022  By:  sp    Date:  13-Sept-1996    DR Number:DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Add unit test method.
//
//  Revision: 021  By:  sp    Date:  3-Sept-1996    DR Number:DCS 1281
//       Project:  Sigma (R8027)
//       Description:
//             Change the calculation for PeakAirwayPressure_, MeanAirwayPressure_,
//             waveform circuit pressure and EndInspPressurePatData_.
//
//  Revision: 020  By:  sp    Date:  3-Sept-1996    DR Number:DCS 1293
//       Project:  Sigma (R8027)
//       Description:
//             Change peep recovery breath type from SPONT to CONTROL for
//             spirometry calculation. Added LastEndExpPressurePatData_
//             and IsFlowResumed_. Update net flow integration during exhalation
//             per controller spec rev 5.0.
//
//  Revision: 019  By:  sp    Date:  2-Aug-1996    DR Number: DCS 967
//       Project:  Sigma (R8027)
//       Description:
//             No end inspiration data is sent during peep recovery.
//
//  Revision: 018  By:  sp    Date:  1-Aug-1996    DR Number: DCS 967
//       Project:  Sigma (R8027)
//       Description:
//             New peep recovery interface with PatientData.
//
//  Revision: 017  By:  sp    Date:  18-July-1996    DR Number:DCS 1135
//       Project:  Sigma (R8027)
//       Description:
//             Compensate inspiredVolume_ with BTPS at the factor
//             end of exhalation.
//
//  Revision: 016  By:  sp    Date:  6-Jun-1996    DR Number: DCS 967
//       Project:  Sigma (R8027)
//       Description:
//             Add argument peepRecovery to newInspiration.
//
//  Revision: 015  By:  sp    Date:  6-Jun-1996    DR Number:DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Added requirement number 4052.
//
//  Revision: 014  By:  sp    Date:  3-Jun-1996    DR Number:DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Modified unit test method.
//
//  Revision: 013  By:  sp    Date:  24-May-1996    DR Number:940
//       Project:  Sigma (R8027)
//       Description:
//             Fixed the breath type problem.
//
//  Revision: 012  By:  sp    Date:  8-May-1996    DR Number:600
//       Project:  Sigma (R8027)
//       Description:
//             Add unit test method setMandType.
//
//  Revision: 011  By:  sp   Date:  30-Apr-1996    DR Number: 967
//       Project:  Sigma (R8027)
//       Description:
//             update data DiscDeliveredVolume_ in newCycle.
//             change restricted phase condition in method newCyle.
//
//  Revision: 010  By:  sp    Date: 11-Apr-1996    DR Number:721
//       Project:  Sigma (R8027)
//       Description:
//             Fixed error in BreathData update that the breath type is half breath
//             behind.
//
//  Revision: 009  By:  sp    Date:  5-Apr-1996    DR Number:721
//       Project:  Sigma (R8027)
//       Description:
//             Change rev 002 comments.
//
//  Revision: 008  By:  sp    Date:  5-Apr-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             TUV interface has been moved to Waveform module.
//
//  Revision: 007  By:  sp    Date:  1-Apr-1996    DR Number:DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Missed a "=" in comparison.
//
//  Revision: 006  By:  sp    Date:  7-Mar-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Add TUV interface with BdMonitoredData.
//
//  Revision: 005  By:  sp    Date:  4-Mar-1996    DR Number:DCS 674
//       Project:  Sigma (R8027)
//       Description:
//             Remove #ifndef.
//
//  Revision: 004  By:  sp   Date:  20-Feb-1995    DR Number: DCS 718
//       Project:  Sigma (R8027)
//       Description:
//             Add autozero as a part of the waveform data.
//
//  Revision: 003  By:  sp   Date:  15-Dec-1995    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Change BreathPhaseScheduler to SchedulerId (600).
//             Add unit test methods (600).
//
//  Revision: 002  By:  sp   Date:  15-Dec-1995    DR Number: DCS 617, 623, 624, 702, 703, 704, 721
//       Project:  Sigma (R8027)
//       Description:
//             Add new data member inspSideMinVol_ to store both air and o2 volume
//             accumulated throughout a breath in method newCycle (624).
//             Update the condition discrepancy in integrating expiredVolume_ in
//             method newCycle(617).
//             Bug fix in adding one waveform record when IntervalNumber_ is 0 in
//             method newCycle. The addition of the waveform record will be handle
//             by Waveform object(702).
//             Update method newExhalation to be able to update PhaseType_ if the
//             breathType_ is NON_MEASURED(703).
//             Post SEND_END_EXH_DATA or SEND_END_INSP_DATA msg on BREATH_DATA_Q in method
//             newInspiration or newExhalation (704).
//             Add condition (breathType != ::NON_MEASURED) in the if statement in
//             method newInspiration(#3).
//             EndExpPressurePatData_ is initialized to exh pressure sensor value
//             instead of 0 in method newExhalation(623).
//             Change const PEAK_FLOW_MEAS_DELAY_MS to 100 msec(721).
//             Remove the "=" sign in the comparison for [TI10] in method newCycle(721).
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number:
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "BreathRecord.hh"

#ifdef SIGMA_DEBUG
//#include "PrintQueue.hh"
#endif // SIGMA_DEBUG

//@ Usage-Classes
#include "MainSensorRefs.hh"
#include "SchedulerRefs.hh"
#include "PressureSensor.hh"
#include "Btps.hh"
#include "SchedulerId.hh"
#include "ExhFlowSensor.hh"
#include "BreathSet.hh"
#include "CircuitCompliance.hh"
#include "Waveform.hh"
#include "PhasedInContextHandle.hh"
#include "MathUtilities.hh"
#include "HumidTypeValue.hh"
#include "PressureXducerAutozero.hh"
#include "PhaseRefs.hh"
#include "ExhalationPhase.hh"
#include "BreathMiscRefs.hh"
#include "FlowController.hh"
#include "ControllersRefs.hh"
#include "NovRamManager.hh"
#include "MandTypeValue.hh"
#include "BreathPhaseScheduler.hh"
#include "BiLevelScheduler.hh"
#include "Maneuver.hh"
#include "PatientCctTypeValue.hh"
#include "PeepController.hh"
#include "LungData.hh"
#include "DynamicMechanics.hh"
#include "BdSystemStateHandler.hh"
#include "LeakCompMgr.hh"

#include "ProxManager.hh"
#include "ProxEnabledValue.hh"
#include "SchedulerId.hh"
//@ End-Usage

#if defined(SIGMA_UNIT_TEST) || defined(SIGMA_DEBUG)
#include <stdio.h>
#include "BreathRecord_UT.hh"
#endif //SIGMA_UNIT_TEST

//@ Code...

static const Int8 SPIRO_TERMINATE_COUNT = 4;
// instantiate and initialize static data members
BreathType BreathRecord::PreviousBreathType_ = ::NON_MEASURED;
BreathType BreathRecord::BreathTypeDisplay_ = ::NON_MEASURED;
Resistance BreathRecord::RInspResistance_;
Resistance BreathRecord::RExhResistance_;
Real32 BreathRecord::AirwayPressureAccum_ = 0.0F;
Int32 BreathRecord::IntervalNumber_ = 0;
Real32 BreathRecord::AirwayPressure_ = 0.0F;
Real32 BreathRecord::PeakAirwayPressure_ = 0.0F;
Real32 BreathRecord::PressWyeInspEst_ = 0.0F;
Real32 BreathRecord::PressWyeExpEst_ = 0.0F;
Real32 BreathRecord::PeakExpiratoryFlow_ = 0.0F;
Real32 BreathRecord::PeakInspiratoryFlow_ = 0.0F;
BreathPhaseType::PhaseType BreathRecord::PhaseType_ = BreathPhaseType::NON_BREATHING;
Real32 BreathRecord::EndInspPressure_ = 0.0F;
Real32 BreathRecord::EndInspPressureComp_ = 0.0F;
Real32 BreathRecord::EndInspPressurePatData_ = 0.0F;
Real32 BreathRecord::EndExpPressureComp_ = 0.0F;
Real32 BreathRecord::EndExpPressurePatData_ = 0.0F;
Real32 BreathRecord::LastEndExpPressure_ = 0.0F;
Real32 BreathRecord::InspiratoryTime_ = 0.0F;
Boolean BreathRecord::IsPhaseRestricted_ = TRUE;
Boolean BreathRecord::IsExhFlowFinished_ = FALSE;
Int32 BreathRecord::ExhTime_ = 0;
Boolean BreathRecord::IsPeepRecovery_ = FALSE;
Boolean BreathRecord::IsMandBreath_ = FALSE ;
Real32 BreathRecord::BreathPeriod_ = 0.0F;
Real32 BreathRecord::FlowTarget_ = 0.0F;
Real32 BreathRecord::AirFlowSum_ = 0.0F;
Real32 BreathRecord::O2FlowSum_ = 0.0F;
Real32 BreathRecord::PrevNetFlow1_ = 0.0F;
Real32 BreathRecord::PrevNetFlow2_ = 0.0F;
Real32 BreathRecord::PeakPrevNetFlow1_ = 0.0F;

Uint32 BreathRecord::SpiroTerminateCount_ = 0 ;
Boolean BreathRecord::FirstCycleOfExhalation_ = TRUE ;
Boolean BreathRecord::FirstCycleOfInspiration_ = FALSE ;
Boolean BreathRecord::FirstCycleOfPlateau_ = FALSE ;
Real32 BreathRecord::DisplayValue_ = 0.0 ;
Real32 BreathRecord::DisplayLungPressure_ = 0.0 ;
Real32 BreathRecord::DisplayAlpha_ = 0.0 ;

Real32 BreathRecord::BiLevelEndExhVolume_ = 0.0;

BreathRecord::BiLevelPhases BreathRecord::BiLevelPhase_ = BreathRecord::NORMAL_INSP_EXH;
BreathRecord::BiLevelPhases BreathRecord::PreviousBiLevelPhase_ = BreathRecord::NORMAL_INSP_EXH;
BreathType BreathRecord::BiLevelFirstBreathType_ = ::NON_MEASURED;
Boolean BreathRecord::IsHighPeep_ = FALSE;

Real32 BreathRecord::BiLevelEndMandPressure_ = 0.0 ;
Uint32 BreathRecord::BiLevelMandTime_ = 0 ;

Boolean BreathRecord::IsPeepTransition_ = FALSE ;
Trigger::TriggerId BreathRecord::BreathTriggerId_ = Trigger::NULL_TRIGGER_ID ;

// respiratory mechanics data
Real32 BreathRecord::EndExpiratoryFlow_ = 0.0;

Uint32 BreathRecord::TrueExhTime_ = 0;
Real32 BreathRecord::EndDryExhFlow_ =0.0f;

Boolean BreathRecord::StartProxInspCycleCount_ = FALSE;
Uint8 BreathRecord::ProxInspCycleCount_ = 0;
Boolean BreathRecord::StartProxExpCycleCount_ = FALSE;
Uint8 BreathRecord::ProxExpCycleCount_ = 0;
Real32 BreathRecord::DistalPressure_ = 0.0f;


Real32 BreathRecord::ProxPressSub_ = 0.0f;
Real32 BreathRecord::WyePressEst_ = 0.0f;
Real32 BreathRecord::WyePressEstPrev_ = 0.0f;
Real32 BreathRecord::ProxPressSubPrev_ = 0.0f;
Boolean BreathRecord::IsProxSubPressEnabled_ = FALSE;
Real32 BreathRecord::ProxPrevPressure_ = 0.0f;
Real32 BreathRecord::WaveformPrevPressure_ = 0.0f;
//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BreathRecord()
//
//@ Interface-Description
//   Default constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//   Initialize data members RInspResistance_ and RExhResistance_.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================

BreathRecord::BreathRecord(void)
{
    CALL_TRACE("BreathRecord()");
    // $[TI1]
    NovRamManager::GetInspResistance(RInspResistance_);
    NovRamManager::GetExhResistance(RExhResistance_);

	proxExpiredVolume_ = 0;
	isProxFlowBreath_ = FALSE;

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~BreathRecord()
//
//@ Interface-Description
//	Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

BreathRecord::~BreathRecord(void)
{
  CALL_TRACE("~BreathRecord()");

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newInspiration
//
//@ Interface-Description
//   This method is called on transition to a new inspiration.  It takes
//   as parameters, schedulerId which is the Id of the BreathPhaseScheduler
//   currently active when this call is made, the mandatoryType, which
//   distinguishes between the types of mandatory breaths available, the
//   phaseType distinguishes breathing phase from non-breathing phase, the
//   breathType, which indicates if this breath is mandatory, assisted,
//   spontaneous or a non-measured type of breath phase, the peepRecovery,
//   indicates if peep recovery is in progress, and the isMandBreath indicates
//	 if bilevel low to high transition is in progress.
//   This method also triggers the update of the BreathData object used
//   to communicate "patient data" to the Patient-Data-Management subsystem.
//   The BreathData object is only updated if there is a previous breath,
//   previous breath is NON_BREATHING, and the current breath is NON_BREATHING
//   and NON_MEASURED.
//---------------------------------------------------------------------
//@ Implementation-Description
//   If there was a breath delivered previously to this one just beginning,
//   then update the complianceExhVolume_, inspiredVolume_, meanAirwayPressure_,
//   and send the data over to BreathData.  Then, the rest of the data members
//   of this class must be initialized for the new breath.
//   PeakAirwayPressure_ is initialized to -20
//   because the pressure sensor's reading can be negative.
//   $[03005] $[03018] $[03027] $[03038] $[03041] $[03050] $[03051]
//	 $[BL04070] $[03030] $[03042] $[03045] $[03061]
// 	 $[BL03010]
// 	 $[BL04082]
//---------------------------------------------------------------------
//@ PreCondition
//   (IntervalNumber_ > 0)
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
void
BreathRecord::newInspiration(
        const SchedulerId::SchedulerIdValue schedulerId,
        const DiscreteValue mandatoryType,
        const BreathType breathType,
        const BreathPhaseType::PhaseType phaseType,
        const Boolean peepRecovery,
		const Boolean isMandBreath,
        const Boolean isUsingProxFlow)		
{
   CALL_TRACE("newInspiration()");

	// used to determine the mand exhaled volume during BiLevel only
	Real32 biLevelMandVolume = 0.0 ;

	const DiscreteValue  CIRCUIT_TYPE =
		PhasedInContextHandle::GetDiscreteValue(SettingId::PATIENT_CCT_TYPE);

  // update the last breath's record, if there is one
   BreathRecord* pLastBreath = RBreathSet.getLastBreathRecord();
   if ( (pLastBreath != NULL) &&
        (PhaseType_ != BreathPhaseType::NON_BREATHING) &&
        (phaseType != BreathPhaseType::NON_BREATHING) &&
        (breathType != ::NON_MEASURED) )
   {
      // $[TI1]
      CLASS_PRE_CONDITION(IntervalNumber_ > 0);

		// don't update if phaseType == NULL_INSPIRATION (in another words,
		// during BiLevel mode transitioning from high peep exh to low peep exh
		if (phaseType != BreathPhaseType::NULL_INSPIRATION)
		{
			// $[TI13]
			if (IsHighPeep_ == FALSE || BiLevelPhase_ == LOW_TO_HIGH_INSP)
			{
				// do the following if the current breath is a low to high
				// mand breath or a breath during peep low.
				// $[TI14]
	    		if (!IsEquivalent( BiLevelEndExhVolume_, 0.0, THOUSANDTHS))
				{
					// a non-zero BiLevelEndExhVolume_ implies that there was
					// at least one spont breath during the high peep.  Thus, we must
					// determine the compliance compensated mandatory inspired volume
					// (biLevelMandVolume) to used in the mand fraction calculation to
					// follow.
					// $[TI15]
					biLevelMandVolume = calculateComplianceBtpsVol( BiLevelEndExhVolume_,
												BiLevelEndMandPressure_,
            	    		                    EndExpPressureComp_, BiLevelMandTime_, INSP_BTPS) ;

				}	// implied else $[TI16]

				// reset volume offset for volume - time plot
				BiLevelEndExhVolume_ = 0.0 ;
			}
			else
			{
				// this is a breath during the high peep
				// $[TI17]
				if (BiLevelPhase_ == FIRST_HIGH_SPONT_INSP)
				{
					// This is the first spont breath during peep high.  Record
					// the previous breath's inspired net volume, which is the
					// low to high mand breath's inspired net volume (BiLevelEndExhVolume_).
					// Also record the BiLevelEndMandPressure_ for compliance compensation
					// calculations.
					// $[TI18]
					BiLevelEndExhVolume_ = pLastBreath->inspiredVolume_ - pLastBreath->expiredVolume_ ;
					BiLevelEndMandPressure_ = EndExpPressureComp_ ;
					BiLevelMandTime_ = (Uint32)(pLastBreath->inspTime_) ;
				}	// implied else $[TI19]
			}

	      	Real32 inspBtpsCf = Btps::GetInspBtpsCf();

    		if (!IsEquivalent(inspBtpsCf, 0.0, TENTHS))
      		{
	    		// $[TI11]
	      		pLastBreath->inspiredVolume_ /= inspBtpsCf;
	  		}
	  		else
	  		{
	      		// $[TI12]
	      		pLastBreath->inspiredVolume_ = 0.0 ;
	  		}

      		// calculateComplianceBtpsVol require the matching
      		// complianceExhVolume_ of the same BreathRecord.
      		Real32 inspTime;
      		if (pLastBreath->mandType_ == MandTypeValue::VCV_MAND_TYPE)
      		{
         		// $[TI9]
         		inspTime = InspiratoryTime_;
      		}
      		else if (pLastBreath->noInspiration_ == TRUE)
      		{
				// $[TI20]
         		inspTime = pLastBreath->inspTime_ ;
			}
      		else
      		{
         		// $[TI10]
         		if (CIRCUIT_TYPE == PatientCctTypeValue::NEONATAL_CIRCUIT)
         		{
         			inspTime  = InspiratoryTime_;
         		}
         		else
         		{
         			inspTime  = InspiratoryTime_ * PCV_TIME_FACTOR;
         		}
      		}

      		pLastBreath->complianceExhVolume_ =
          		calculateComplianceBtpsVol(pLastBreath->expiredVolume_, EndInspPressureComp_,
                		                     EndExpPressureComp_, (Int32)inspTime) ;

    		if (!IsEquivalent( biLevelMandVolume, 0.0, THOUSANDTHS))
			{
				// a non-zero biLevelMandVolume implies that the previous exhalation
				// occured from a high peep spont to an exhalation ending at a low
				// peep.  The fraction is calculated to reflect this.

				// $[TI21]
				pLastBreath->mandatoryVolumeFraction_ = biLevelMandVolume /	pLastBreath->complianceExhVolume_ ;

				if (pLastBreath->mandatoryVolumeFraction_ > 1.0)
				{
					// $[TI36]
					pLastBreath->mandatoryVolumeFraction_ = 1.0 ;
				}
			}	// implied else $[TI22]

      		pLastBreath->meanAirwayPressure_ = AirwayPressureAccum_ / IntervalNumber_;

			//send data over to BreathData so it will be available to the user interface.
      		// The breath type for the peep recovery breath is set to NON_MEASURED
      		// to exclude it from spirometry calculation.
      		if (IsPeepRecovery_)
      		{
        		// $[TI3]
        		pLastBreath->breathType_ = ::NON_MEASURED;
        		BreathTypeDisplay_ = ::NON_MEASURED;
      		}
      		// $[TI4]
		}	// implied else $[TI23]
		const Real32 VOL_OFFSET = 10.0 ;
		Real32 diff = pLastBreath->maxExpiredVolume_ - pLastBreath->expiredVolume_ ;
		Real32 tolerance = 0.1f * pLastBreath->expiredVolume_ + VOL_OFFSET ;

        // $[LC24032] -- Use the same algorithm as currently implemented in 840 for Vte...
		if ( (pLastBreath->maxExpiredVolume_ > pLastBreath->expiredVolume_) &&
		 	(diff > tolerance) &&
			(!RLeakCompMgr.isEnabled())
		    )
		{
	         // $[TI13]
			pLastBreath->expiredVolume_ = pLastBreath->maxExpiredVolume_ ;
		}   // implied else $[TI14]

	  //If we are using prox flow sensor, get the exhaled volume calculated by prox manager
	  // and update the record.
	  if (isUsingProxFlow == TRUE)
	  {
		  pLastBreath->proxExpiredVolume_ = RProxManager.getExpiredVolume();
		  pLastBreath->isProxFlowBreath_ = TRUE;
	  }
	  else
	  {
		  pLastBreath->proxExpiredVolume_ = 0;
		  pLastBreath->isProxFlowBreath_ = FALSE;
	  }

      // PhaseType_ need to be updated before using BreathData
      // NOTE: that the breath type need to be sent has to be the current breath
      //       type. Update the last breath type with current breath type and restore
      //       the last breath type after BreathData has been updated.
      PreviousBreathType_ = pLastBreath->breathType_;

	  if (phaseType != BreathPhaseType::NULL_INSPIRATION)
	  {
		  // $[TI41]
	      BreathTypeDisplay_ = breathType ;
	  }
	  else
	  {
		  // $[TI42]
	  	  // transition from peep high to low, use BiLevelFirstBreathType_
	      BreathTypeDisplay_ = BiLevelFirstBreathType_ ;
	  }

      pLastBreath->breathType_ = breathType;
      PhaseType_ = BreathPhaseType::INSPIRATION;
      RBreathData.update(*pLastBreath);
      pLastBreath->breathType_ = PreviousBreathType_;

      // It is possible that peep recovery breath
      // may satisfy the above condition.
      if (!peepRecovery)
      {
        // $[TI7]
			if (phaseType != BreathPhaseType::NULL_INSPIRATION)
			{
				// $[TI24]
				if (BiLevelPhase_ == FIRST_HIGH_SPONT_INSP)
				{
					// this is the first spont breath during peep high, send
					// over the end mand data
					// $[TI25]
					// first spont effort during peep high
			        MsgQueue::PutMsg(::BREATH_DATA_Q, BdQueuesMsg::SEND_BL_MAND_DATA) ;
				}
				else
				{
					// send end of exhalation data
					// $[TI26]
					// $[TI13]

					// When PROX is enabled, start counting the number of
					// breath-delivery cycles to prospone sending of the
					// end of exhalation message to Breath Data.
					if (RProxManager.isAvailable())
					{
						StartProxExpCycleCount_ = TRUE;
						ProxExpCycleCount_ = 0;
					}
					else
					{
						MsgQueue::PutMsg(::BREATH_DATA_Q, BdQueuesMsg::SEND_END_EXH_DATA);
					}
			    }
		    }	// implied else $[TI27]
      }
	  else
	  {
	      	// $[TI5]
    		MsgQueue::PutMsg(::BREATH_DATA_Q, BdQueuesMsg::SEND_PEEP_RECV_DATA);
	  }

		if (BreathPhaseScheduler::GetBlPeepTransitionState() == BreathPhaseScheduler::LOW_TO_HIGH)
      	{
			// $[TI28]
          	BreathPhaseScheduler::SetBlPeepTransitionState(
            	                   BreathPhaseScheduler::NULL_TRANSITION);
   	  	}	// implied else $[TI29]
   }
   // $[TI2]

   	//update data to reflect the new breath
   	PhaseType_ = phaseType;
	schedulerId_ = schedulerId;
   	mandType_ = mandatoryType;
   	breathType_ = breathType;

	if (breathType_ == ::SPONT)
	{
		// $[TI30]
		mandatoryVolumeFraction_ = 0.0 ;
	}
	else
	{
		// $[TI31]
		mandatoryVolumeFraction_ = 1.0 ;
	}

    //initializing data
	complianceExhVolume_ = 0.0F;
	FlowTarget_ = 0.0F;
	IsPeepRecovery_ = peepRecovery;
   	IsMandBreath_ = isMandBreath ;
   	IsPhaseRestricted_ = TRUE;
	TrueExhTime_ = 0 ;
   	if (phaseType == BreathPhaseType::NULL_INSPIRATION)
   	{
		// $[TI32]
   		// this is a transition to peep low breath

		noInspiration_ = TRUE;
		breathDuration_ = pLastBreath->breathDuration_ ;
		inspTime_ = pLastBreath->inspTime_ ;

		if (PreviousBiLevelPhase_ == LOW_TO_HIGH_EXH)
		{
			// $[TI33]
			// this is a transition to peep low with no sponts during peep high
			EndInspPressurePatData_ = AirwayPressure_ ;
		}
		else
		{
			// $[TI34]
			// this is a transition to peep low with at least one spont during peep high
			// keep the same EndInspPressurePatData_
		}

		// keep the same EndInspPressureComp_
		// keep the same PeakAirwayPressure_
		// keep the same AirwayPressureAccum_
		// keep the same IntervalNumber_
		// don't update LastEndExpPressure_
		inspiredVolume_ = pLastBreath->inspiredVolume_ ;
		inspSideMinVol_ = pLastBreath->inspSideMinVol_ ;
		expiredVolume_ = pLastBreath->expiredVolume_ ;


		Real32 minFlow = 0.0 ;

        switch (CIRCUIT_TYPE)
        {
	        case PatientCctTypeValue::PEDIATRIC_CIRCUIT :
    	        // $[TI38]
        	    minFlow = 15.0 ;
            	break ;

	        case PatientCctTypeValue::ADULT_CIRCUIT :
    	        // $[TI37]
        	    minFlow = 25.0 ;
            	break ;

	        case PatientCctTypeValue::NEONATAL_CIRCUIT :
				// $[TI43]
        	    minFlow = 10.0 ;
            	break ;

	        default :
    	        AUX_CLASS_ASSERTION_FAILURE(CIRCUIT_TYPE);
        	    break ;
        }

		const Real32 MIN_VOL = 20.0 ;

		if (PreviousBreathType_ == ::CONTROL ||
			PreviousBreathType_ == ::ASSIST && expiredVolume_ <= MIN_VOL && PeakExpiratoryFlow_ <= minFlow)
		{
			// $[TI39]
			expiredVolume_ = 0.0 ;
		}	// $[TI40]

	   	// keep the same PeakExpiratoryFlow_
		// keep the same PeakInspiratoryFlow_
		// keep the same InspiratoryTime_
	   	FirstCycleOfInspiration_ = FALSE ;
		// keep the same AirFlowSum_
   		// keep the same O2FlowSum_

	   	// call newExhalation() after all data has been reinitialized
       	newExhalation();
	}
	else
	{
		// $[TI35]
		// keep the same EndInspPressureComp_
		BreathPeriod_ = 0.0F;
		meanAirwayPressure_ = 0.0F;
		maxExpiredVolume_ = 0.0F;
		noInspiration_ = FALSE;
		EndInspPressurePatData_ = 0.0F;
		breathDuration_ = 0.0F;
		RLeakCompMgr.setInspTime((Uint32)inspTime_);
		inspTime_ = 0;
		PeakAirwayPressure_ = -20.0F;
		AirwayPressureAccum_ = 0.0F;
		IntervalNumber_ = 0;
		LastEndExpPressure_ =  AirwayPressure_;
		inspiredVolume_ = 0.0F;
		inspSideMinVol_ = 0.0F;
		expiredVolume_ = 0.0F;
		// $[RM24014] \a\ PSF_singlebreath  is initialized to zero at beginning of inhalation
		PeakInspiratoryFlow_ = 0.0F;
		InspiratoryTime_ = 0;
	   	FirstCycleOfInspiration_ = TRUE ;
		AirFlowSum_ = 0.0F;
   		O2FlowSum_ = 0.0F;
		BreathTriggerId_ = Trigger::NULL_TRIGGER_ID;
	}

    RLeakCompMgr.newInspiration();

	IsProxSubPressEnabled_ = RProxManager.IsProxSubPressEnabled();
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newExhalation
//
//@ Interface-Description
//   This method takes no parameters and has no return value.  It must be
//   called on the same BD cycle as the transition to exhalation.  Data
//   for end inspiratory pressure and inspired volume must be updated
//   if the breath is not non-breathing or the transition is not from
//   exhalation to exhalation (possible on transition to spont mode).
//   The update of the BreathData class is triggered from this method.
//   The breath phase is considered to be restricted
//   at the beginning of exhalation.
//---------------------------------------------------------------------
//@ Implementation-Description
//   The phaseType_ is set to exhalation since the transition to exhalation
//   has just occured.  The EndInspPressureComp_ and EndInspPressurePatData_
//   are calculated.  EndExpPressureComp_ is initialized to 0.
//   PrevNetFlow_ is set to current net flow and
//   IsExhFlowFinished_ is set to FALSE;
// 	 If Leak Compensation is enabled, use the expiratory estimated wye pressure
//   for end inspiratory pressure compliance. $[LC24041]
//   $[03004] $[03005] $[03023] $[03024]
//	 $[BL04041] $[03007] $[03022] $[03027]
//---------------------------------------------------------------------
//@ PreCondition
//   (!IsEquivalent(inspBtpsCf, 0.0, TENTHS))
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
void
BreathRecord::newExhalation(void)
{
	CALL_TRACE("BreathRecord::newExhalation(void)");

	Real32 inspFlow = RO2FlowSensor.getValue() + RAirFlowSensor.getValue();
	Real32 exhDryFlow = RExhFlowSensor.getDryValue();
	BreathPhaseType::PhaseType previousPhaseType = PhaseType_;

	// update EndInspPressureComp_ if phase is an exhalation to exhalation of
	// a mandatory breath during BiLevel or a normal exhalation.
	if ((PhaseType_ == BreathPhaseType::NULL_INSPIRATION &&
		 IsEquivalent( BiLevelEndExhVolume_, 0.0, THOUSANDTHS)) ||
		PhaseType_ != BreathPhaseType::NULL_INSPIRATION)
	{

		// If Leak Compensation is enabled, use the expiratory estimated wye pressure
		// for end inspiratory pressure compliance. $[LC24041]
		if (RLeakCompMgr.isEnabled())
		{
			EndInspPressureComp_ = PressWyeExpEst_;
		}
		else
		{
			// $[TI18]
			EndInspPressureComp_ = 0.5F * (AirwayPressure_ + 0.5F *
										   (RInspPressureSensor.getValue() + RExhPressureSensor.getValue()) );
		}
	} // implied else $[TI19]

	EndInspPressure_ = AirwayPressure_;

	// do not update breath data and breath record items if
	// the breath is non breathing or start from exhalation
	if ( previousPhaseType != BreathPhaseType::EXHALATION &&
		 previousPhaseType != BreathPhaseType::NON_BREATHING)
	{
		// $[TI1]
		PhaseType_ = BreathPhaseType::EXHALATION;

		if (previousPhaseType != BreathPhaseType::NULL_INSPIRATION)
		{
			// $[TI7]
			if (breathType_ == ::ASSIST || breathType_ == ::CONTROL)
			{
				// $[TI8]
				mandatoryVolumeFraction_ = 1.0 ;
			}
			else if (breathType_ == SPONT)
			{
				// $[TI9]
				mandatoryVolumeFraction_ = 0.0 ;
			}
		} // implied else $[TI10]

		if (BreathPhaseScheduler::GetBlPeepTransitionState() == BreathPhaseScheduler::HIGH_TO_LOW)
		{
			// $[TI11]
			BreathTypeDisplay_ = BiLevelFirstBreathType_ ;
			BreathPhaseScheduler::SetBlPeepTransitionState(BreathPhaseScheduler::NULL_TRANSITION);
		} // implied else $[TI12]

		if (breathType_ != ::NON_MEASURED)
		{
			// $[TI3]

			// update dynamic mechanics data to be included in BreathData
			RDynamicMechanics.update();

			RBreathData.update(*this);

			if (!IsPeepRecovery_)
			{
				// $[TI5]
				if (BiLevelPhase_ == NORMAL_INSP_EXH)
				{
					// $[TI13]

					// When PROX is enabled, start counting the number of
					// breath-delivery cycles to prospone sending of the
					// end of inhalation message to Breath Data.
					if (RProxManager.isAvailable())
					{
						StartProxInspCycleCount_ = TRUE;
						ProxInspCycleCount_ = 0;
					}
					else
					{
						MsgQueue::PutMsg(::BREATH_DATA_Q, BdQueuesMsg::SEND_END_INSP_DATA);
					}
				}
				else if (BiLevelPhase_ == HIGH_SPONT_EXH_TO_LOW_EXH)
				{
					// $[TI14]
					MsgQueue::PutMsg(::BREATH_DATA_Q, BdQueuesMsg::SEND_BL_MAND_EXH_DATA);
				}
				else
				{
					// $[TI15]
					CLASS_PRE_CONDITION( BiLevelPhase_ == LOW_TO_HIGH_INSP ||
										 BiLevelPhase_ == LOW_TO_HIGH_EXH ||
										 BiLevelPhase_ == FIRST_HIGH_SPONT_INSP) ;
				}
			}
			// $[TI6]
		}
		// $[TI4]
	}
	// $[TI2]

	PeakPrevNetFlow1_ = 0.0 ;
	PrevNetFlow1_ = exhDryFlow - inspFlow;
	PrevNetFlow2_ = PrevNetFlow1_;
	EndExpPressureComp_ = EndInspPressure_;
	EndExpPressurePatData_ = EndInspPressurePatData_;
	IsExhFlowFinished_ = FALSE;
	SpiroTerminateCount_ = 0 ;
	PeakExpiratoryFlow_ = 0.0F;
	EndExpiratoryFlow_ = 0.0F;

	if (!noInspiration_)
	{
		// $[TI16]
		FirstCycleOfExhalation_ = TRUE ;
	}	// implied else $[TI17]
    RLeakCompMgr.newExhalation((Real32)ExhTime_);
	RProxManager.setDistalPressure(DistalPressure_);

	ExhTime_ = 0 ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newCycle(void)
//
//@ Interface-Description
//   This method takes no parameters, and has no return value.
//   It is called every BD cycle.  Data members are updated depending
//   upon the phase type (inspiration or exhalation).  If the phase type
//   is non-breathing, then this method will not update.  No updates
//   to the BreathData class are made on a cycle to cycle basis.
//   Waveform data such as net volume, net flow and phase type are sent to
//   Waveform object every cycle. In addition, leak compensation algorithms
//   are updated every cycle depending upon the phase type.
//---------------------------------------------------------------------
//@ Implementation-Description
//   inspiredVolume_ is continously integrated during inspiration cycles.
//   expiredVolume_ is continously integrated during exhalation cycles.
//   Peak inspiratory pressure is based on maximum expiratory pressure
//   sensor reading during inspiration cycles. Peak expiratory pressure
//   is based on maximum inspiratory pressure sensor during exhalation
//   cycles. PeakAirwayPressure_ is the maximum of peak inspiratory
//   pressure and Peak expiratory pressure.
//   The PeakInspiratoryFlow_ is the maximum of inspiratory flow during
//   inspiration cycles. PeakExpiratoryFlow_ is the maximum of expiratory net
//   flow during exhalation cycles.  There is PEAK_FLOW_MEAS_DELAY_MS ms
//   delay in the evaluation of peak flow is introduced to avoid
//   detecting premature peaks because of spikes.
//   The breathDuration_ and IntervalNumber_ are incremented every cycle.
//   IsPhaseRestricted_ is set to true during inspiration and updated
//   every cycle during exhalation.
//   Otherwise, the data member is set true.
//   All data members which are used in leak compensation algorithms are
//   updated every cycle to cycle.
// $[03012] $[03019] $[04012] $[03058] $[03062] $[04038] $[04057] $[04289]
// $[03047] $[03050] $[03051] $[04052] $[04325] $[04329]
// $[04329] $[03075]
// $[BL03013] $[BL04016]
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
const Int32 MIN_EXH_TIME = 200; //msec
static const Int32 PEAK_FLOW_MEAS_DELAY_MS = 100; //msec
static const Real32 NET_FLOW_ALPHA_FILTER_1 = 0.975F;
static const Real32 NET_FLOW_ALPHA_FILTER_2 = 0.90F;
static const Real32 FILTERED_FLOW_BOUND = 0.20F;
static const Uint32 PROX_SEND_COUNT = 20; // $[PX] cycles
const Real32 DEFAULT = -1000.0 ;	// used to use other value in MAX_VALUE
void
BreathRecord::newCycle(void)
{
    CALL_TRACE("newCycle()");

    WaveformRecord waveformRecord;

    Real32  bacteriaFilterCompliance = RCircuitCompliance.getBacteriaFilterCompliance() ;

    // $[04056]
    Real32 inspFlow = RO2FlowSensor.getValue() + RAirFlowSensor.getValue();
    Real32 exhDryFlow = RExhFlowSensor.getDryValue();
    Real32 exhFlow = RExhFlowSensor.getValue();

	// note that netFlow is positive for exhaled flow which is not the
	// the convention used in the literature or other classes such as LungData
	// where exhaled flow is negative
    Real32 netFlow = exhDryFlow - inspFlow;
    Real32 exhPressureSlope = RExhPressureSensor.getSlope();
    Real32 exhPressureSlopeFiltered = RExhPressureSensor.getFilteredSlopeValue();

	Real32 exhCapacitanceFlow = (bacteriaFilterCompliance *
								 exhPressureSlope * 60.0F) / CYCLE_TIME_MS;

	Real32 exhCapacitanceFilteredFlow = (bacteriaFilterCompliance *
										 exhPressureSlopeFiltered * 60.0F) / CYCLE_TIME_MS;

	Real32 pWyeInspEstFilt = RInspPressureSensor.getFilteredValue() -
							 RInspResistance_.calculateDeltaP(inspFlow);

	Real32 pWyeExpEstFilt = RExhPressureSensor.getFilteredValue() +
							RExhResistance_.calculateDeltaP(exhFlow + exhCapacitanceFilteredFlow);

	PressWyeInspEst_ = RInspPressureSensor.getValue() -
					   RInspResistance_.calculateDeltaP(inspFlow);

	PressWyeExpEst_ = RExhPressureSensor.getValue() +
					  RExhResistance_.calculateDeltaP(exhFlow + exhCapacitanceFlow);


    RLeakCompMgr.newCycle();

	// $[LC24013] -- Modify the net flow calculation...
    if (RLeakCompMgr.isEnabled() && RLeakCompMgr.isBreathingMode())
    {
        // RLeakCompMgr.newCycle() must be first before calculating
        // netflow.
		netFlow = RLeakCompMgr.getNetFlow();
    }


    // The following code extends the use of the non-aautozeroed side for one extra cycle
    // since the PressureXducerAutozero class will re-initialize the filtered values
    // which will cause a "glitch" in the calculations.

    static Boolean AutozeroInProgress = FALSE ;

    if ( RPressureXducerAutozero.getIsAutozeroInProgress() || AutozeroInProgress )
	{

		if (RPressureXducerAutozero.getAutozeroSide() == PressureXducerAutozero::INSP &&
			RPressureXducerAutozero.getIsAutozeroInProgress())
		{
			// $[TI32]
			pWyeInspEstFilt = DEFAULT ;
			PressWyeInspEst_ = DEFAULT ;
		}
		else if (RPressureXducerAutozero.getAutozeroSide() == PressureXducerAutozero::EXH &&
				 RPressureXducerAutozero.getIsAutozeroInProgress())
		{
			// $[TI33]
			pWyeExpEstFilt = DEFAULT ;
			PressWyeExpEst_ = DEFAULT ;
		} 	// implied else $[TI34]

        AutozeroInProgress = TRUE;
	}	// implied else $[TI31]


	if ( !RPressureXducerAutozero.getIsAutozeroInProgress() )
    {
	// $[TI25]
        AutozeroInProgress = FALSE;
    }
	// $[TI9]

    Real32 airwayPressureFiltered = MAX_VALUE(pWyeInspEstFilt, pWyeExpEstFilt);
    AirwayPressure_ = MAX_VALUE(PressWyeInspEst_, PressWyeExpEst_);
	DistalPressure_ = pWyeExpEstFilt;

	// $[LC24041] If Leak Compensation is enabled, report only the exhalation pressure
	// to the waveforms.
	if (RLeakCompMgr.isEnabled())
	{
		if (RPressureXducerAutozero.getAutozeroSide() == PressureXducerAutozero::EXH &&
				 RPressureXducerAutozero.getIsAutozeroInProgress())
		{
			airwayPressureFiltered = pWyeInspEstFilt;
			AirwayPressure_ = PressWyeInspEst_;
		}
		else
		{
			airwayPressureFiltered = pWyeExpEstFilt;
			AirwayPressure_ = PressWyeExpEst_;
		}
	}

    if (PhaseType_ != BreathPhaseType::NON_BREATHING)
    {
	// $[TI1]
        if (PhaseType_ == BreathPhaseType::INSPIRATION ||
        	PhaseType_ == BreathPhaseType::VITAL_CAPACITY_INSP ||
        	PhaseType_ == BreathPhaseType::PAV_INSPIRATORY_PAUSE ||
        	PhaseType_ == BreathPhaseType::INSPIRATORY_PAUSE)
        {
        // $[TI2]

        	if (FirstCycleOfInspiration_ || FirstCycleOfPlateau_)
        	{
		        // $[TI35]
        		DisplayAlpha_ = 1.0 ;
        	}   // implied else $[TI36]

            inspiredVolume_ -= netFlow * CYCLE_TIME_MS / 60.0F;   // mL
            if (inspiredVolume_ < 0.0F)
            {
            // $[TI21]
                inspiredVolume_ = 0.0F;
            }
            // $[TI22]

            waveformRecord.netVolume_ = inspiredVolume_ + BiLevelEndExhVolume_ ;
		    waveformRecord.lungVolume_ = RLungData.getInspLungBtpsVolume() ;

			// $[RM24014] \b\ PSF_singlebreath = max[PSF_singlebreath,Qnet]
			PeakInspiratoryFlow_ = MAX_VALUE(PeakInspiratoryFlow_, -netFlow);
            PeakAirwayPressure_ = MAX_VALUE(PeakAirwayPressure_, airwayPressureFiltered);
            AirwayPressureAccum_ += airwayPressureFiltered;
            EndInspPressurePatData_ = airwayPressureFiltered;

            InspiratoryTime_ += CYCLE_TIME_MS;
            inspTime_ += CYCLE_TIME_MS;
			FirstCycleOfInspiration_ = FALSE ;
			FirstCycleOfPlateau_ = FALSE ;
        }
        else // EXHALATION, EXPIRATORY_PAUSE, NIF, P100, BILEVEL_PAUSE_PHASE
        {
        // $[TI23]
            if (PhaseType_ == BreathPhaseType::EXHALATION)
            {
	        // $[TI5]

				if (BiLevelPhase_ == LOW_TO_HIGH_EXH)
	        	{
					// $[TI41]
	        		// this is during first exhalation during peep high, extend inspTime_
	        		// to reflect the insp time if there is no spont breath before transitioning to
	        		// peep low.  Increment the ExhTime_ also since the current phase is
	        		// really exhalation.
        		    inspTime_ += CYCLE_TIME_MS; ;
		        	ExhTime_ += CYCLE_TIME_MS ;
        		}
        		else
        		{
					// $[TI42]
		        	ExhTime_ =(Int32) (breathDuration_ - inspTime_ );
        		}
				TrueExhTime_ += CYCLE_TIME_MS ;		// this is the true exh time for IsPhaseRestricted_

                PeakAirwayPressure_ = MAX_VALUE(PeakAirwayPressure_, airwayPressureFiltered);
                AirwayPressureAccum_ += airwayPressureFiltered;

                PrevNetFlow1_ = PrevNetFlow1_ * NET_FLOW_ALPHA_FILTER_1 +
                                          netFlow * (1.0F - NET_FLOW_ALPHA_FILTER_1);
                PrevNetFlow2_ = PrevNetFlow2_ * NET_FLOW_ALPHA_FILTER_2 +
                                          netFlow * (1.0F - NET_FLOW_ALPHA_FILTER_2);

                PeakPrevNetFlow1_ = MAX_VALUE(PeakPrevNetFlow1_, PrevNetFlow1_);

                Real32 desiredFlow = RAirFlowController.getDesiredFlow() + RO2FlowController.getDesiredFlow();

                if ( (ABS_VALUE(PrevNetFlow1_ - PrevNetFlow2_) < FILTERED_FLOW_BOUND) &&
                     (PrevNetFlow1_ < 0.2 * PeakPrevNetFlow1_) &&
                     (RPeepController.isControllerFrozen()) &&
                     (ExhTime_ >= 200) &&
                     IsEquivalent(desiredFlow, RExhalationPhase.getFlowTarget(), THOUSANDTHS) &&
                     !IsExhFlowFinished_ )
                {
                // $[TI10]
					if (++SpiroTerminateCount_ >= SPIRO_TERMINATE_COUNT)
					{
                    // $[TI10.1]
	                    IsExhFlowFinished_ = TRUE;
    	         	}
                    // $[TI10.2]
                }
                else
                {
                // $[TI11]
					SpiroTerminateCount_ = 0 ;
				}

				// Check for end exhalation to prevent VTE inaccuracy
                // during leak comp enabled.
				if (RLeakCompMgr.isEnabled() && !IsExhFlowFinished_)
				{
					IsExhFlowFinished_ = RLeakCompMgr.isExhFlowFinished();
				}

                if (FirstCycleOfExhalation_)
                {
                // $[TI5.1]
	        		DisplayAlpha_ = 1.0 ;

                    const Real32 OFFSET = 6.0 ;

                    // integrate as if it were inspiration cycle
                    FirstCycleOfExhalation_ = FALSE ;

                    // $[LC24033]
                    inspiredVolume_ -= netFlow * CYCLE_TIME_MS / 60.0F;

					const DiscreteValue  CIRCUIT_TYPE =
					PhasedInContextHandle::GetDiscreteValue(SettingId::PATIENT_CCT_TYPE);

                    // reset volume
                    if (mandType_ == MandTypeValue::VCV_MAND_TYPE &&
						CIRCUIT_TYPE != PatientCctTypeValue::NEONATAL_CIRCUIT)
                    {
                    // $[TI5.1.1]
                        expiredVolume_ = OFFSET ;
                    }
                    else
                    {
                    // $[TI5.1.2]
                        expiredVolume_ = 0.0 ;
                    }
        	    	maxExpiredVolume_ = expiredVolume_ ;
            	}
            	else
            	{
	       	        // $[TI5.2]
	                if (!IsExhFlowFinished_)
   		            {
    		        	// $[TI26]
	        		    expiredVolume_ += netFlow * CYCLE_TIME_MS / 60.0F;
	        	    	if (expiredVolume_ > maxExpiredVolume_)
	        	    	{
							// $[TI41]
	        	    		maxExpiredVolume_ = expiredVolume_ ;
	        	    	}	// $[TI42]
	            		EndExpPressureComp_ = AirwayPressure_;

                        // $[RM12014] \a\ EEF shall be the flow when Exhaled flow integration is terminated or
                        // $[RM12014] \b\ EEF shall be the flow at start of inspiration
						EndExpiratoryFlow_ = netFlow;

						EndDryExhFlow_ = exhDryFlow;
			        }
    	            // $[TI27]

                    // Get the current vent type.
                    VentTypeValue::VentTypeValueId ventType_ ;
                    ventType_ = (VentTypeValue::VentTypeValueId)PhasedInContextHandle::GetDiscreteValue(
                                SettingId::VENT_TYPE) ;

                    if ((!IsExhFlowFinished_ &&
                         (netFlow > 0.5F || (ventType_ == VentTypeValue::NIV_VENT_TYPE) )))
                    {
                        // $[TI28]

						RLeakCompMgr.updateAvgExhPress();

						// $[TI29]
                        EndExpPressurePatData_ = airwayPressureFiltered;
                    }
					else
					{
						RLeakCompMgr.updateAvgExhPress(RPeep.getActualPeep());

					}
                    // $[TI29]

                }

                //avoid premature max because of spikes
                if (ExhTime_ > PEAK_FLOW_MEAS_DELAY_MS)
                {
                // $[TI12]
					// $[RM12010] PEF shall be the peak flow measured after the first 100 msec of any exhalation.
                    PeakExpiratoryFlow_ = MAX_VALUE(PeakExpiratoryFlow_, netFlow);
                }
                // $[TI13]

                if ( ( ( (netFlow <= 0.50F * PeakExpiratoryFlow_ ) || (exhDryFlow <= 0.50F) ) &&
                       (TrueExhTime_ >= MIN_EXH_TIME - CYCLE_TIME_MS) ) ||
                       (TrueExhTime_ >= 5000) )
                {
                // $[TI14]
                    IsPhaseRestricted_ = FALSE;
                }
                // $[TI15]

            }
            else // EXPIRATORY_PAUSE, BILEVEL_PAUSE_PHASE or NIF_PAUSE
            {
            // $[TI16]
				AUX_CLASS_PRE_CONDITION( PhaseType_ == BreathPhaseType::EXPIRATORY_PAUSE ||
										 PhaseType_ == BreathPhaseType::BILEVEL_PAUSE_PHASE ||
										 PhaseType_ == BreathPhaseType::NIF_PAUSE ||
										 PhaseType_ == BreathPhaseType::P100_PAUSE,
										 PhaseType_) ;

                PeakAirwayPressure_ = MAX_VALUE(PeakAirwayPressure_, airwayPressureFiltered);
                AirwayPressureAccum_ += airwayPressureFiltered;
                EndExpPressurePatData_ = airwayPressureFiltered;
            }

            waveformRecord.netVolume_ = inspiredVolume_ - expiredVolume_ + BiLevelEndExhVolume_;
		    waveformRecord.lungVolume_ = RLungData.getInspLungBtpsVolume() - RLungData.getExhLungBtpsVolume() ;
        }

        // A waveform record with zero values has to be inserted at the
        // beginning of each breath.
        if (IntervalNumber_ == 0)
        {
  	    // $[TI17]
            waveformRecord.netVolume_ = BiLevelEndExhVolume_ ;
		    waveformRecord.lungVolume_ = 0.0 ;
            //A new record has to be inserted at the beginning of a new breath
            //The method Waveform::synchronize will cause Waveform::newCycle to insert
            // a new waveform record.
            RWaveform.synchronize();
        }
        // $[TI18]

        // $[TI19]
        AirFlowSum_ += RAirFlowSensor.getValue();
        O2FlowSum_ += RO2FlowSensor.getValue();
        inspSideMinVol_ = (AirFlowSum_ + O2FlowSum_) * CYCLE_TIME_MS / 60.0F;
        breathDuration_ += CYCLE_TIME_MS;
        IntervalNumber_++;
    }
    else
    {
    // $[TI20]
        waveformRecord.netVolume_ = 0.0;
	    waveformRecord.lungVolume_ = 0.0 ;
    }

	 DisplayValue_ = DisplayValue_ * DisplayAlpha_ + airwayPressureFiltered * (1.0f - DisplayAlpha_) ;
	 DisplayLungPressure_ = DisplayLungPressure_ * DisplayAlpha_ + RLungData.getLungPressure() * (1.0f - DisplayAlpha_) ;

	 const Real32 ALPHA_DECREMENT = 0.02 ;

	 DisplayAlpha_ -= ALPHA_DECREMENT ;

	 if (DisplayAlpha_ < 0.0)
	 {
		// $[TI39]
	 	DisplayAlpha_ = 0.0 ;
	 }
	 // $[TI40]

	RLeakCompMgr.update();


	// Send the end of inspiratory message to Breath Data after a delay of
	//  PROX_SEND_COUNT cycles.
	if ( (ProxInspCycleCount_++ >= PROX_SEND_COUNT) && StartProxInspCycleCount_)
	{
	   ProxInspCycleCount_ = 0;
       StartProxInspCycleCount_ = FALSE;
	   MsgQueue::PutMsg(::BREATH_DATA_Q, BdQueuesMsg::SEND_END_INSP_DATA);

	}



	// Send the end of exhalation message to Breath Data after a delay of
	//  PROX_SEND_COUNT cycles.
	if ( (ProxExpCycleCount_++ >= PROX_SEND_COUNT) && StartProxExpCycleCount_)
	{
	   ProxExpCycleCount_ = 0;
       StartProxExpCycleCount_ = FALSE;
	   MsgQueue::PutMsg(::BREATH_DATA_Q, BdQueuesMsg::SEND_END_EXH_DATA);

	}



     //update waveform
	waveformRecord.patientPressure_ = DisplayValue_ ;
	waveformRecord.netFlow_ = -netFlow;
	waveformRecord.lungPressure_ = DisplayLungPressure_;
	waveformRecord.lungFlow_ = RLungData.getLungFlow() ;
	waveformRecord.phaseType_ = PhaseType_;
	waveformRecord.isAutozeroActive_ = RPressureXducerAutozero.getIsAutozeroInProgress();
	waveformRecord.breathType_ = breathType_;
	waveformRecord.isPeepRecovery_ = IsPeepRecovery_ ;
	waveformRecord.isApneaActive_ = BdSystemStateHandler::IsApneaActive();

	waveformRecord.modeSetting_ = PhasedInContextHandle::GetDiscreteValue( SettingId::MODE) ;
	waveformRecord.supportTypeSetting_ = PhasedInContextHandle::GetDiscreteValue( SettingId::SUPPORT_TYPE) ;
	const SchedulerId::SchedulerIdValue  SCHED_ID = BreathPhaseScheduler::GetCurrentScheduler().getId();

	waveformRecord.isErrorState_ = (SCHED_ID >= SchedulerId::FIRST_NONBREATHING_SCHEDULER &&
									SCHED_ID <= SchedulerId::LAST_NONBREATHING_SCHEDULER);

	waveformRecord.proxManuever_ = 0;
    waveformRecord.proxPatientPressure_ = 0;

	Real32 pressureWyeExpEst = PressWyeExpEst_;

	if ( IsEquivalent(PressWyeExpEst_, DEFAULT, ONES ))
	{
		pressureWyeExpEst = AirwayPressure_;
	}

	// Calculate the prox substitute pressure.
	ProxPressSub_ = (0.07f * pressureWyeExpEst) + (0.93f * ProxPressSubPrev_);

    // Calculate the estimated wye pressure
	WyePressEst_ = (0.14f * pressureWyeExpEst) + (0.86f * WyePressEstPrev_);


	// When prox is enabled, populate the waveform packet with prox
	// information such as flor and volume.
    if (RProxManager.isAvailable())
	{
		Int32 proxBreathPhase = (Int32)  RProxManager.getBreathPhase();

		// Replace Prox's pressure with the calculated
		// prox substitute pressure when they are inaccurate.
		if (IsProxSubPressEnabled_)
		{
			if (PhaseType_ == BreathPhaseType::EXHALATION &&
				proxBreathPhase == BreathPhaseType::INSPIRATION )
			{
				waveformRecord.proxPatientPressure_ =  WaveformPrevPressure_ + RProxManager.getPressure()
				                                      - ProxPrevPressure_;
			}
			else
			{
				waveformRecord.proxPatientPressure_ =  ProxPressSub_;
			}
		}
		else
		{
			waveformRecord.proxPatientPressure_ =  RProxManager.getPressure();

		}

		ProxPrevPressure_ = RProxManager.getPressure();
		WaveformPrevPressure_ = waveformRecord.proxPatientPressure_;

        waveformRecord.netFlow_  = RProxManager.getFlow();
		waveformRecord.netVolume_ = RProxManager.getVolume();
		waveformRecord.proxManuever_ = RProxManager.proxManeuverStatus();

		if ( proxBreathPhase == BreathPhaseType::EXHALATION)
		{
			waveformRecord.phaseType_ = BreathPhaseType::EXHALATION;
		}
		else if ( proxBreathPhase == BreathPhaseType::INSPIRATION )
		{
			waveformRecord.phaseType_ = BreathPhaseType::INSPIRATION;
		}
		else
		{
            waveformRecord.phaseType_ = PhaseType_;
		}

	}
	else
	{
		ProxPrevPressure_ = 0.0f;
		WaveformPrevPressure_ = 0.0f;
	}

	ProxPressSubPrev_ = ProxPressSub_;
    WyePressEstPrev_ = WyePressEst_;
	RWaveform.newCycle(waveformRecord);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: calculateComplianceBtpsVol
//
//@ Interface-Description
//  This method takes expired volume, end inspiratory pressure, end
//  expiratory pressure, inspiratory time, and btps type as arguments
//  and returns btps volume of type Real32.
//  The compliance is calculated based on measured pressure, and btps is
//  calculated based on the circuit humidification type.  The value of
//  exhaled volume that has been compensated for compliance and btps
//  is returned.
//  This method needs to be reentrant because it is called from the main
//  and spirometry task.
// $[04037] $[04038] $[04058] $[04283] [$04325]
//---------------------------------------------------------------------
//@ Implementation-Description
//  The compliance volume is calculated based on the end inspiratory and
//  end expiratory pressure measurements.  Then the btps correction factor
//  is calculated based on the type of humidification device in the patient
//  circuit.  The accumulated exhaled volume is corrected for btps
//  by multiplying the volume by the correction factor.  The volume is
//  then compliance compensated by subtracting off the compliance volume.
//---------------------------------------------------------------------
//@ PreCondition
//	(humidType == HumidTypeValue::NON_HEATED_TUBING_HUMIDIFIER ||
//	(humidType == HumidTypeValue::HEATED_TUBING_HUMIDIFIER) ||
//	(humidType == HumidTypeValue::HME_HUMIDIFIER)
//
// 	btpsType == EXP_BTPS || btpsType == INSP_BTPS
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Real32
BreathRecord::calculateComplianceBtpsVol(
		const Real32 vol,
		const Real32 endInspPress,
		const Real32 endExpPress,
		const Int32 inspTime,
		const BtpsTypes btpsType
)
{
	CALL_TRACE("BreathRecord::calculateComplianceBtpsVol( \
		const Real32 vol, const Real32 endInspPress, \
		const Real32 endExpPress, const Int32 inspTime, \
		const BtpsTypes btpsType) ;") ;

    Real32 btpsVolume = 0;

    //compliance compensation
    Real32 deltaPressure = (endInspPress - endExpPress) * BreathPhase::GetAdiabaticFactor() ;
    Real32 complianceVolume = RCircuitCompliance.getComplianceVolume(deltaPressure, inspTime) ;

    if (complianceVolume < 0.0F)
    {
        // $[TI3]
	complianceVolume = 0.0F;
    }


	// Calculate a new compliance volume, if Leak Compensation is enabled,
	// and the circuit type is neonatal. $[LC24034]
    complianceVolume = RLeakCompMgr.computeCompliance(complianceVolume) ;

    // $[TI4]

	Real32 btpsFactor = 1.0 ;

	if (btpsType == EXP_BTPS)
	{
	    // $[TI7]
		btpsFactor = Btps::GetExpBtpsCf() ;
	}
	else if (btpsType == INSP_BTPS)
	{
	    // $[TI8]
		btpsFactor = Btps::GetInspBtpsCf() ;

		// avoid divide by zero.  Set factor to one if factor is near zero.
		if (btpsFactor > 0.001)
		{
		    // $[TI9]
			btpsFactor = 1.0f / btpsFactor ;
		}
		else
		{
		    // $[TI10]
			btpsFactor = 1.0 ;
		}
	}
	else
	{
		CLASS_PRE_CONDITION( btpsType == EXP_BTPS || btpsType == INSP_BTPS) ;
	}

#ifdef SIGMA_UNIT_TEST
	BreathRecord_UT::BtpsFactor = btpsFactor ;
#endif // SIGMA_UNIT_TEST

    // BTPS compensated
    DiscreteValue humidType =
	PhasedInContextHandle::GetDiscreteValue(SettingId::HUMID_TYPE);
    if ( (humidType == HumidTypeValue::NON_HEATED_TUBING_HUMIDIFIER) ||
         (humidType == HumidTypeValue::HEATED_TUBING_HUMIDIFIER) )
    {
        // $[TI1]
	btpsVolume = vol * btpsFactor -	complianceVolume ;
    }
    else if (humidType == HumidTypeValue::HME_HUMIDIFIER)
    {
        // $[TI2]
	btpsVolume = (vol - complianceVolume) * btpsFactor;
    }
    else
	CLASS_PRE_CONDITION(
		(humidType == HumidTypeValue::NON_HEATED_TUBING_HUMIDIFIER) ||
		(humidType == HumidTypeValue::HEATED_TUBING_HUMIDIFIER) ||
		(humidType == HumidTypeValue::HME_HUMIDIFIER) );

    if (btpsVolume < 0.0F)
    {
        // $[TI5]
        btpsVolume = 0.0F;
    }
    // $[TI6]

    return (btpsVolume);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
BreathRecord::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, BREATHRECORD,
                          lineNumber, pFileName, pPredicate);
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//=====================================================================
//
//  Private Methods...
//
//=====================================================================


#if defined(SIGMA_UNIT_TEST) || defined(SIGMA_DEBUG)
void
BreathRecord::write1(void)
{
    printf ("----- BreathRecord Non-Static Member -----\n");
    printf ("PhaseType_: %d\n", (int)PhaseType_);
    printf ("mandType_: %d\n", (int)mandType_);
    printf ("schedulerId_: %d\n", (int)schedulerId_);
    printf ("breathType_: %d\n", (int)breathType_);
    printf ("inspiredVolume_: %f\n", inspiredVolume_);
    printf ("inspSideMinVol_: %f\n", inspSideMinVol_);
    printf ("expiredVolume_: %f\n", expiredVolume_);
    printf ("complianceExhVolume_: %f\n", complianceExhVolume_);
    printf ("breathDuration_: %f\n", breathDuration_);
    printf ("meanAirwayPressure_: %f\n", meanAirwayPressure_);
}

void
BreathRecord::write2(void)
{
    printf("----- BreathRecord Static Members -----\n");
    printf ("AirwayPressure_: %f\n", AirwayPressure_);
    printf ("PeakAirwayPressure_: %f\n", PeakAirwayPressure_);
    printf ("AirwayPressureAccum_: %f\n", AirwayPressureAccum_);
    printf ("IntervalNumber_: %f\n", IntervalNumber_);
    printf ("PeakExpiratoryFlow_: %f\n", PeakExpiratoryFlow_);
    printf ("PeakInspiratoryFlow_: %f\n", PeakInspiratoryFlow_);
    printf ("EndInspPressureComp_: %f\n", EndInspPressureComp_);
    printf ("EndInspPressurePatData_: %f\n", EndInspPressurePatData_);
    printf ("EndExpPressureComp_: %f\n", EndExpPressureComp_);
    printf ("EndExpPressurePatData_: %f\n", EndExpPressurePatData_);
    printf ("InspiratoryTime_: %f\n", InspiratoryTime_);
    printf ("IsPhaseRestricted_: %d\n", (int) IsPhaseRestricted_);
    printf ("BreathPeriod_: %f\n", BreathPeriod_);
    printf ("FlowTarget_: %f\n", FlowTarget_);
    printf ("AirFlowSum_: %f\n", AirFlowSum_);
    printf ("O2FlowSum_: %f\n", O2FlowSum_);
    printf ("IsExhFlowFinished_: %d\n", (int)IsExhFlowFinished_);
    printf ("IsPeepRecovery_: %d\n", (int)IsPeepRecovery_);
    printf ("PrevNetFlow1_: %f\n", PrevNetFlow1_);
    printf ("PrevNetFlow2_: %f\n", PrevNetFlow2_);
}

#endif // SIGMA_UNIT_TEST || SIGMA_DEBUG
