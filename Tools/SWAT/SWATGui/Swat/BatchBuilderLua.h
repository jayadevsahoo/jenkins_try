#ifndef BATCHBUILDER_LUA_H
#define BATCHBUILDER_LUA_H
#include "batchbuilder.h"
#include "luna.h"

/*
INSP_SIDE_PRESSURE_SENSOR                   {yylval.iValue = 0;return T_INTEGER;}
EXH_PRESSURE_FILTERED                       {yylval.iValue = 1;return T_INTEGER;}
INSP_FLOW_SENSOR                            {yylval.iValue = 2;return T_INTEGER;}
INSP_TEMPERATURE_SENSOR                     {yylval.iValue = 3;return T_INTEGER;}
EXHALATION_FLOW_SENSOR                      {yylval.iValue = 4;return T_INTEGER;}
EXHALATION_GAS_TEMPERATURE_SENSOR           {yylval.iValue = 5;return T_INTEGER;}
SUBMUX_SENSORS                              {yylval.iValue = 6;return T_INTEGER;}
SAFETY_VALVE_SWITCHED_SIDE                  {yylval.iValue = 7;return T_INTEGER;}
DC_12V_GUI_SENTRY                           {yylval.iValue = 8;return T_INTEGER;}
ALARM_CABLE_VOLTAGE                         {yylval.iValue = 9;return T_INTEGER;}
INSP_PSOL_CURRENT                           {yylval.iValue = 10;return T_INTEGER;}
LOW_VOLTAGE_REFERENCE                       {yylval.iValue = 11;return T_INTEGER;}
ATMOSPHERIC_PRESSURE_SENSOR                 {yylval.iValue = 12;return T_INTEGER;}
EXHALATION_COIL_TEMPERATURE_SENSOR          {yylval.iValue = 13;return T_INTEGER;}
EXH_SIDE_PRESSURE_SENSOR                    {yylval.iValue = 14;return T_INTEGER;}
O2_SENSOR                                   {yylval.iValue = 15;return T_INTEGER;}
DC_5V_GUI_SENTRY                            {yylval.iValue = 16;return T_INTEGER;}
DC_12V_SENTRY                               {yylval.iValue = 17;return T_INTEGER;}
SAFETY_VALVE_STATE                          {yylval.iValue = 18;return T_INTEGER;}
DC_15V_SENTRY                               {yylval.iValue = 19;return T_INTEGER;}
DC_NEG_15V_SENTRY                           {yylval.iValue = 20;return T_INTEGER;}
POWER_FAIL_CAP_VOLTAGE                      {yylval.iValue = 21;return T_INTEGER;}
EXHALATION_HEATER_TEMPERATURE               {yylval.iValue = 22;return T_INTEGER;}
SYSTEM_BATERY_VOLTAGE                       {yylval.iValue = 23;return T_INTEGER;}
DC_5V_VENTHEAD                              {yylval.iValue = 24;return T_INTEGER;}
SYSTEM_BATTERY_CURRENT                      {yylval.iValue = 25;return T_INTEGER;}
AC_LINE_VOLTAGE                             {yylval.iValue = 26;return T_INTEGER;}
EXHALATION_MOTOR_CURRENT                    {yylval.iValue = 27;return T_INTEGER;}
DC_10V_SENTRY                               {yylval.iValue = 28;return T_INTEGER;}
EXHALATION_POPPET_POSITION                  {yylval.iValue = 29;return T_INTEGER;}
EXHALATION_POPPET_VELOCITY                  {yylval.iValue = 30;return T_INTEGER;}
EXHALATION_POPPET_ACCELERATION              {yylval.iValue = 31;return T_INTEGER;}
NUM_ADC_CHANNELS                            {yylval.iValue = 32;return T_INTEGER;}
ADC_COUNTER_PORT                            {yylval.iValue = 33;return T_INTEGER;}
DISCRETE_DIO_READ_PORT                      {yylval.iValue = 34;return T_INTEGER;}
SERIAL_DEVICE_READ_PORT                     {yylval.iValue = 35;return T_INTEGER;}


START_OF_INSP	                    {yylval.iValue = 6;return T_EVENT_ID;}
START_OF_EXH	                    {yylval.iValue = 7;return T_EVENT_ID;}


*/
class Batch {

public:

	//Stack state management
	int mark(lua_State *L);
	

	//Index management
	int setindex   (lua_State *L );
	int skipindex  (lua_State *L );
	int repeatn    (lua_State *L );	
	int repeattime (lua_State *L );
	int waittime   (lua_State *L );


	//Event
	int repeatevent(lua_State *L);
	int waitevent  (lua_State *L);
	int signalevent(lua_State *L);



	//Sensor
	int setsensor(lua_State *L);
	int addsensor  (lua_State *L);
    int unsetsensor(lua_State *L);

	//Raw Sensor
	int setrawsensor(lua_State *L);
    int unsetrawsensor(lua_State *L);

    int addrawsensor(lua_State *L);

    int simulateguitouch(lua_State *L);
    int simulatebezelkey(lua_State *L);


	//Common
	int WriteToVent(int nCommandType );
	int send(lua_State *L);

	int end(lua_State *L);
	int resetall(lua_State *L);

	int setbatchID(lua_State *L);


	Batch(lua_State *L) 
	{
		m_batch = new BatchBuilder;
		LOG_MESSAGE(LM_INFO, "Enabling vent BD for batch file commands");
		WriteToVent(1);
	}

	~Batch() 
	{
		
	}

	BatchBuilder * m_batch;

	static const char className[];
	static const Luna<Batch>::RegType Register[];
	char chBatchID[80];
};


#endif