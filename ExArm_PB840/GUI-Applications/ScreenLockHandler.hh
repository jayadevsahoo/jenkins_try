#ifndef ScreenLockHandler_HH
#define ScreenLockHandler_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: ScreenLockHandler - Handles control of Screen Lock
// (initiated by the Screen Lock off-screen key).
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/ScreenLockHandler.hhv   25.0.4.0   19 Nov 2013 14:08:22   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 001  By:  hhd    Date:  16-MAY-95    DR Number: 
//  Project:  Sigma (R8027)
//  Description:
//             Integration baseline.
//====================================================================

#include "AdjustPanelTarget.hh"
#include "KeyPanelTarget.hh"

#include "GuiAppClassIds.hh"

class ScreenLockHandler :	public KeyPanelTarget, public AdjustPanelTarget
{
public:
	ScreenLockHandler(void);
	~ScreenLockHandler(void);

	// Key Panel Target virtual methods
	virtual void keyPanelPressHappened(KeyPanel::KeyId key);
	virtual void keyPanelReleaseHappened(KeyPanel::KeyId key);

	// Enable the screen
	void unlockScreen(void);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber, 
						  const char*       pFileName  = NULL,
						  const char*       pPredicate = NULL);
protected:

private:
	// these methods are purposely declared, but not implemented...
	ScreenLockHandler(const ScreenLockHandler&);	// not implemented...
	void   operator=(const ScreenLockHandler&);		// not implemented...

	void clearPrompts_(void);						// Clear existing prompts
	void unlock_(void);								// Enable screen

	//@ Data-Member: isScreenLocked_
	// Flag which stores the screen locked state
	Boolean isScreenLocked_;

	// to flag screen's lock state  
};


#endif // ScreenLockHandler_HH
