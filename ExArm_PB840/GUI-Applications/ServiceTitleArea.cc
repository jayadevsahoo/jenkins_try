#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1994, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
// Class: ServiceTitleArea - A area at the top of the Upper screen used
// for displaying current system status, and date/time.
//---------------------------------------------------------------------
//@ Interface-Description
// This class is a container, derived from the GUI-Foundation Container class.
// It contains textual fields, and lines.  It also inherits timer callbacks
// (timerEventHappened()) via the abstract GuiTimerTarget class.
//
// The current display configuration for this area is determined by the
// setDisplayMode() method.
//
// During Service GUI operation, this area depends on periodic timer callbacks
// to dynamically update the date/time readout in the status portion of the
// display.  Events from the GuiEventRegistrar should result in a call to
// a call to setDisplayMode() in order that we may display the new system
// mode.  The novRamUpdateEventHappened() is called whenever the NovRamManger
// issures an REAL_TIME_CLOCK_UPDATE event to update the date/time change to the log.
//---------------------------------------------------------------------
//@ Rationale
// For the Service GUI, this class is dedicated to managing the display
// of the second from the top portion of the Upper Screen.
//---------------------------------------------------------------------
//@ Implementation-Description
// To support Service Mode operations, this area continuously displays
// one of the following configurations.
// >Von
//	1.	Mode status.
//	2.	Date/time display.
//  3.  Circuit type.
//  4.  Humidifier type.
// >Voff
// When updated status is requested via the setDisplayMode(), we simply
// mapping the status to the appropriate message.  The current date/time
// display is updated via a timer callback which is set to fire several times a
// minute to keep the time display reasonably accurate.  The timer
// callback and associated processing is handled by  timerEventHappened().
// Changes to the circuit  and humidification types are updated via callbacks 
// from the Settings-Validation subsystem.
//
// The various display objects are added to the Container when it is
// constructed.  The display configurations are implemented by
// hiding/showing the contained objects in the proper combinations.
//---------------------------------------------------------------------
//@ Fault-Handling
// Checks for out-of-range enum values with assertions.
//---------------------------------------------------------------------
//@ Restrictions
// None.  In particular, there is no restriction in changing from one
// display mode to another at any time.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/ServiceTitleArea.ccv   25.0.4.0   19 Nov 2013 14:08:26   pvcs  $
//
//@ Modification-Log
//
//  Revision: 007   By: gdc   Date:  26-May-2007    SCR Number: 6330
//  Project:  Trend
//  Description:
//      Removed SUN prototype code.
//    
//  Revision: 006   By: srp   Date:  24-Apr-2002    DR Number: 5327 & 5908
//  Project:  NeoMode
//  Description:
//      Added Modification log. Updated for NeoMode  03/01/00
//    
//  Revision: 005  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 004  By:  clw    Date:  19-May-1998    DCS Number: 
//  Project:  Sigma (R8027)
//  Description:
//      For Japanese project: for japanese only (conditional compilation) the
//      date format has been changed to Japanese format.
//
//  Revision: 003  By:  yyy    Date:  08-Sep-1997    DCS Number: 2269
//  Project:  Sigma (R8027)
//  Description:
//      Used REAL_TIME_CLOCK_UPDATE the newly added callbacks registered for real-time
//		clock changes to update the timer.
//
//  Revision: 002  By:  yyy    Date:  04-AUG-1997    DCS Number: 1803
//  Project:  Sigma (R8027)
//  Description:
//      Registered for DATE/TIME settings to capture the setting change
//		events and update the date/time accordingly.
//
//  Revision: 001  By:  wks    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "ServiceTitleArea.hh"

//@ Usage-Classes
#include "GuiTimerRegistrar.hh"
#include "GuiTimerId.hh"
#include "NovRamEventRegistrar.hh"
#include "MiscStrs.hh"
#include "TimeStamp.hh"
#include "TextUtil.hh"
#include "PatientCctTypeValue.hh"
#include "HumidTypeValue.hh"
#include "SettingContextHandle.hh"
//@ End-Usage

//@ Code...

// Initialize static constants.
static const Int32 CONTAINER_X_ = 0;
static const Int32 CONTAINER_Y_ = 0;
static const Int32 CONTAINER_WIDTH_ = 640;
static const Int32 CONTAINER_HEIGHT_ = 152;
static const Int32 DIVIDING_LINE_Y_ = 65;
static const Int32 DATE_TIME_Y_ = 135;
static const Int32 DATE_TIME_X_ = 470;
static const Int32 PT_CCT_TYPE_Y_ = 108;
static const Int32 HUMID_TYPE_Y_ = 108;


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ServiceTitleArea  [Default Constructor]
//
//@ Interface-Description
// Default constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
// Initializes all members and initializes the colors, sizes, and
// positions of all graphical objects.  Adds all graphical objects to
// the parent container for display.  Registers for timer callback for
// the date/time status display.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

ServiceTitleArea::ServiceTitleArea(void) :
	displayText_(NULL),
	displayMode_(SERVICE_NOT_START_MODE),
	dateTimeText_(NULL),
	dividingLine_(CONTAINER_X_, DIVIDING_LINE_Y_, CONTAINER_WIDTH_, DIVIDING_LINE_Y_)
{
	CALL_TRACE("ServiceTitleArea::ServiceTitleArea(void)");

    // Size and position the area
	setX(CONTAINER_X_);
	setY(CONTAINER_Y_);
	setWidth(CONTAINER_WIDTH_);
	setHeight(CONTAINER_HEIGHT_);
					 
	// Set default background color
	setFillColor(Colors::EXTRA_DARK_BLUE);

	//
	// Add the service title string.
	//
    addDrawable(&displayText_);
	displayText_.setColor(Colors::WHITE);
	displayText_.positionInContainer(GRAVITY_CENTER);
	
	//
	// Add the date/time string.
	//
	dateTimeText_.setX(DATE_TIME_X_);
	dateTimeText_.setY(DATE_TIME_Y_);
	dateTimeText_.setColor(Colors::WHITE);
	addDrawable(&dateTimeText_);

	//
	// Add the dividing line.
	//
	dividingLine_.setColor(Colors::WHITE);
	addDrawable(&dividingLine_);

	// Now for the Circuit and Humidification Type stuff
	circuitTypePrompt_.setText(MiscStrs::STATUS_CCT_TYPE_LABEL);
	circuitTypePrompt_.setColor(Colors::WHITE);
	circuitTypePrompt_.setY(PT_CCT_TYPE_Y_);
	circuitTypeText_.setColor(Colors::WHITE);
	circuitTypeText_.setY(PT_CCT_TYPE_Y_);
	humidificationTypePrompt_.setText(MiscStrs::STATUS_HUMID_TYPE_LABEL);
	humidificationTypePrompt_.setColor(Colors::WHITE);
	humidificationTypePrompt_.setY(HUMID_TYPE_Y_);
	humidificationTypeText_.setColor(Colors::WHITE);
	humidificationTypeText_.setY(HUMID_TYPE_Y_);
	addDrawable(&circuitTypePrompt_);
	addDrawable(&circuitTypeText_);
	addDrawable(&humidificationTypePrompt_);
	addDrawable(&humidificationTypeText_);
	
	//
	// Register myself as a callback target for wall clock events, then kick off the
	// "forever" wall clock timer.
	//
	GuiTimerRegistrar::RegisterTarget(GuiTimerId::WALL_CLOCK, this);
	GuiTimerRegistrar::StartTimer(GuiTimerId::WALL_CLOCK);
	timerEventHappened(GuiTimerId::WALL_CLOCK);	// initialize date/time strings


	// Attach itself to Context Subject
	attachToSubject_(ContextId::ACCEPTED_CONTEXT_ID,
					 Notification::BATCH_CHANGED);

	// Register all logs with NovRam for receiving event updates
	// for changes to the date/time setting changes
	NovRamEventRegistrar::RegisterTarget(NovRamUpdateManager::REAL_TIME_CLOCK_UPDATE,
										this);

	updatePtCctTypeDisplay_();
	updateHumidTypeDisplay_();

														// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~ServiceTitleArea  [Destructor]
//
//@ Interface-Description
// Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
// n/a
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

ServiceTitleArea::~ServiceTitleArea(void)
{
	CALL_TRACE("ServiceTitleArea::~ServiceTitleArea(void)");

	detachFromSubject_(ContextId::ACCEPTED_CONTEXT_ID,
					   Notification::BATCH_CHANGED);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setDisplayMode
//
//@ Interface-Description
// Sets the ServiceTitleArea into one of its four display modes.
// Passed `displayMode', the new display mode value (enum).
//---------------------------------------------------------------------
//@ Implementation-Description
// Display the appropriate message for one of the four modes:
// SERVICE_MODE, SST_MODE, SST_MODE_COMM_DOWN, and SERVICE_MODE_COMM_DOWN.
//
// There are no restrictions and no error checking on changing from
// one mode to a different mode.
//---------------------------------------------------------------------
//@ PreCondition
//	The given displayMode value must be one of the expected enum values.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void 
ServiceTitleArea::setDisplayMode(ServiceTitleMode displayMode)
{
	CALL_TRACE("ServiceTitleArea::setDisplayMode(displayMode)");

	//
	// Only do the mode change if new mode is different than 
	// previous mode.
	//
	if (displayMode_ == displayMode)
	{													// $[TI1]
		return;
	}													// $[TI2]

	switch (displayMode_ = displayMode)
	{	
	case SST_MODE:									// $[TI3]
		displayText_.setText(MiscStrs::SST_NOT_VENTILATING);
		break;

	case SERVICE_MODE:								// $[TI4]
		displayText_.setText(MiscStrs::SERVICE_MODE_LABEL);
		break;

	case SST_MODE_COMM_DOWN:						// $[TI5]
		displayText_.setText(MiscStrs::SST_COMM_DOWN);
		break;

	case SERVICE_MODE_COMM_DOWN:					// $[TI6]
		displayText_.setText(MiscStrs::SERVICE_COMM_DOWN);
		break;

	default:
		CLASS_ASSERTION(FALSE);		// bogus displayMode_ value
		break;
	}
	displayText_.positionInContainer(GRAVITY_CENTER);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: timerEventHappened
//
//@ Interface-Description
// Called when the Wall Clock timer fires.  We must refresh the date/time
// fields whenever the date/time fields are visible.
//---------------------------------------------------------------------
//@ Implementation-Description
// Determines the current wall clock time using a TimeStamp object, then
// passes the wall clock time to the convertDateTime_() routines for
// conversion to raw C string format.  This raw C string is then
// converted to Cheap Text format and then rendered for display.
//
// $[01144] GUI shall update status display at least once per minute.
// $[01145] No compensation for Daylight Savings time needed.
//---------------------------------------------------------------------
//@ PreCondition
// The given 'timerId' must be the WALL_CLOCK value.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void 
ServiceTitleArea::timerEventHappened(GuiTimerId::GuiTimerIdType timerId)
{
	CALL_TRACE("ServiceTitleArea::timerEventHappened(timerId)");

	switch (timerId)
	{
	case GuiTimerId::WALL_CLOCK:						// $[TI1]
		{
			// Refresh the visible time/date fields.
			TimeStamp timeNow;
			wchar_t      cheapText[100];

			TextUtil::FormatTime(cheapText, MiscStrs::TIME_DATE_TITLE_FORMAT,
																	timeNow);
			dateTimeText_.setText(cheapText);
		}
		break;

	default:
		CLASS_ASSERTION(FALSE);
		break;
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: novRamUpdateEventHappened
//
//@ Interface-Description
// Called by NovRam manager, this function updates the display with
// the latest, real-time data in the log.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply call timerEventHappened to process it.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void 
ServiceTitleArea::novRamUpdateEventHappened(NovRamUpdateManager::UpdateTypeId updateTypeId)
{
	// update our display.
	if (updateTypeId == NovRamUpdateManager::REAL_TIME_CLOCK_UPDATE)
	{ 		// $[TI1.1]
		timerEventHappened(GuiTimerId::WALL_CLOCK);
	}		// $[TI1.2]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: batchSettingUpdate
//
//@ Interface-Description
// Called when the Circuit type, Humidification Type changed. Passed the
// following parameters:
// >Von
//	settingId	The enum id of the setting which changed.
//	contextId	The context in which the change happened, either
//				ACCEPTED_CONTEXT_ID or ADJUSTABLE_CONTEXT_ID.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
// Only react to changes in the Accepted context.  Update the appropriate
// graphics according to which setting change and what the new (current) value
// of that setting is.
//---------------------------------------------------------------------
//@ PreCondition
// 	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ServiceTitleArea::batchSettingUpdate(
							 const Notification::ChangeQualifier qualifierId,
							 const ContextSubject*               pSubject,
							 const SettingId::SettingIdType		 settingId
									  )
{
	CALL_TRACE("batchSettingUpdate(qualifierId, pSubject, settingId)");

	if (qualifierId == Notification::ACCEPTED)
	{													// $[TI1]														
		if ( settingId == SettingId::PATIENT_CCT_TYPE ) 
		{			  									// $[TI1.1]
			updatePtCctTypeDisplay_();
		}
		else if ( settingId == SettingId::HUMID_TYPE ) 
		{												// $[TI1.2]
			updateHumidTypeDisplay_();

		}
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateHumidTypeDisplay_
//
//@ Interface-Description
// Update Humidification type in the service title area.
//---------------------------------------------------------------------
//@ Implementation-Description
// Update the display according to the humidification type.
//
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void 
ServiceTitleArea::updateHumidTypeDisplay_()
{
	DiscreteValue humidType =
				SettingContextHandle::GetSettingValue(ContextId::ACCEPTED_CONTEXT_ID,
													SettingId::HUMID_TYPE);

 	switch (humidType)
 	{
 		case HumidTypeValue::NON_HEATED_TUBING_HUMIDIFIER:   // $[TI1]
			humidificationTypeText_.
						setText(MiscStrs::STATUS_HUMID_TYPE_NON_HEATED_VALUE);
			break;

		case HumidTypeValue::HEATED_TUBING_HUMIDIFIER:		  // $[TI2]

			humidificationTypeText_.
						setText(MiscStrs::STATUS_HUMID_TYPE_HEATED_VALUE);
			break;

		case HumidTypeValue::HME_HUMIDIFIER:					  // $[TI3]

			humidificationTypeText_.
						setText(MiscStrs::STATUS_HUMID_TYPE_HME_VALUE);
			break;

		default:
			CLASS_ASSERTION(FALSE);
			break;
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updatePtCctTypeDisplay_
//
//@ Interface-Description
//  Update Patient Circuit Type on service title area.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Update the display according to the changed circuit type.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void 
ServiceTitleArea::updatePtCctTypeDisplay_()
{
	DiscreteValue patientCctType = 
				SettingContextHandle::GetSettingValue(ContextId::ACCEPTED_CONTEXT_ID,
													SettingId::PATIENT_CCT_TYPE);

	// Circuit Type changed so display the new value
  	switch (patientCctType)
	{
		case PatientCctTypeValue::PEDIATRIC_CIRCUIT:	// $[TI1]
			circuitTypeText_.setText(MiscStrs::STATUS_CCT_TYPE_PEDIATRIC_VALUE);
			break;

	 	case PatientCctTypeValue::ADULT_CIRCUIT:		// $[TI2]
	 		circuitTypeText_.setText(MiscStrs::STATUS_CCT_TYPE_ADULT_VALUE);
	 		break;

	 	case PatientCctTypeValue::NEONATAL_CIRCUIT:		// $[TI3]
	 		circuitTypeText_.setText(MiscStrs::STATUS_CCT_TYPE_NEONATAL_VALUE);
	 		break;

	 	default:
	 		circuitTypeText_.setText(MiscStrs::EMPTY_STRING);
	 		break;
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
ServiceTitleArea::SoftFault(const SoftFaultID  softFaultID,
							  const Uint32       lineNumber,
							  const char*        pFileName,
							  const char*        pPredicate)  
{
	CALL_TRACE("ServiceTitleArea::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS,
							SERVICETITLEAREA,
							lineNumber, pFileName, pPredicate);
}

