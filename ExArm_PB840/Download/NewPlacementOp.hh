#ifndef NewPlacementOp_HH
#define NewPlacementOp_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1993, Nellcor/Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
// Filename:  NewPlacementOp - Global Placement New Operator.
// 	Provides global new/delete override operators.
//---------------------------------------------------------------------
//@ Interface-Description
//  Overrides the new and delete operators. There is no dynamic allocation
//  of memory (heap free store) allowed in Sigma. If an attempt is made to
//  allocate or deallocate memory an assertion will cause termination of
//  the calling task.
//---------------------------------------------------------------------
//@ Rationale
//  All needed memory for creating new objects should be reserved
//  ahead of time.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Will utilize assertions to prevent dynamic allocation and deallocation
//  of memory.
//---------------------------------------------------------------------
//@ Fault-Handling
//  Assertions are used to catch fault conditions, along with pre and
//  post conditions where applicable.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Download/vcssrc/NewPlacementOp.hhv   25.0.4.0   19 Nov 2013 14:02:50   pvcs  $ 
//
//@ Modifications
//  
//   Revision: 001   By: jhv   Date: 9-Feb-1996   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//=====================================================================

#include <stddef.h>
#include "DownloadClassIds.hh"


//@ Begin-Free-Declarations
void*  operator new(size_t, void* pMemory);
void*  operator new(size_t pMemory);
void  operator delete(void *);
//@ End-Free-Declarations


#endif  // NewPlacementOp_HH
