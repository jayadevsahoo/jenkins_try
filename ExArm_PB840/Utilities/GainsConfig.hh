//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file  GainsConfig.hh
/// @brief Definition of the GainsConfig class
//----------------------------------------------------------------------------
#ifndef GAINSCONFIG_HH_
#define GAINSCONFIG_HH_

//-------------------------------------
// I N C L U D E S
//-------------------------------------
#include "GainsNames.hh"
#include "SigmaTypes.hh"

namespace GainsManagerNS
{
    //----------------------------------------------------------------------------
    // C L A S S   D E C L A R A T I O N
    // Declare this class as part of the GainsManagerNS namespace
    //----------------------------------------------------------------------------
    /// @class GainsConfig
    /// @brief Abstract base class for all gains that are managed by the Gains
    /// Manager
    ///
    /// This class allows for the Gains Manager to have a collection
    /// of GainsConfigs and operate on them in the same way without having to
    /// know the details of the derived behavior.
    //-------------------------------------------------------------------------
    class GainsConfig;
};

//-------------------------------------
// C L A S S    D E F I N I T I O N
//-------------------------------------
class GainsManagerNS::GainsConfig
{
public:
    typedef enum GainsConfigOpStatus
    {
        SUCCESS,
        FAILURE
    } GainsConfigOpStatus;

    //--------------------------------------------------------------------
    /// Destructor
    //--------------------------------------------------------------------
    virtual ~GainsConfig(void);

    //--------------------------------------------------------------------
    /// Initializes the object with the gains that are in the controller
    //--------------------------------------------------------------------
    void initialize(void);

    //--------------------------------------------------------------------
    /// Restores the object with the original gain from the controller
    //--------------------------------------------------------------------
    void restore(void);

    //--------------------------------------------------------------------
    /// Sets the stored gain into the controller
    ///
    /// @return whether the operation was successful or not
    //--------------------------------------------------------------------
    virtual GainsConfigOpStatus distribute(void) = 0;

    //--------------------------------------------------------------------
    /// Gets the gain from the controller and stores it
    ///
    /// @return whether the operation was successful or not
    //--------------------------------------------------------------------
    virtual GainsConfigOpStatus load(void) = 0;

    //--------------------------------------------------------------------
    /// Initializes the GainsConfig object
    /// @return SUCCESS if initialization was successful, FAILURE otherwise.
    //--------------------------------------------------------------------
    virtual GainsConfigOpStatus setup(void) = 0;

    //--------------------------------------------------------------------
    /// Gets the gain stored in this object
    //--------------------------------------------------------------------
    Real32 getGain(void) const;

    //--------------------------------------------------------------------
    /// Gets the gain stored in this object
    //--------------------------------------------------------------------
    Real32 getOriginalGain(void) const;

    //--------------------------------------------------------------------
    /// Sets the gain into this object
    //--------------------------------------------------------------------
    void setGain( Real32 r );

    //--------------------------------------------------------------------
    /// Gets the name of the gain
    //--------------------------------------------------------------------
    GainsManagerNS::GainsNames getGainName(void) const;

protected:
    //--------------------------------------------------------------------
    /// Constructor
    //--------------------------------------------------------------------
    GainsConfig(void);

    //--------------------------------------------------------------------
    /// Constructor
    //--------------------------------------------------------------------
    GainsConfig( GainsManagerNS::GainsNames name );

    //--------------------------------------------------------------------
    /// Assignment operator
    ///
    /// @param rhs - Right hand side.
    ///
    /// @return A reference to a GainsConfig.
    //--------------------------------------------------------------------
    GainsConfig& operator=(const GainsConfig &rhs);

    //--------------------------------------------------------------------
    /// Indication that the gain has changed and needs to be set
    /// @return the changed state of the gain
    //--------------------------------------------------------------------
    Boolean isChanged(void) const;

    //--------------------------------------------------------------------
    /// Sets the changed state of the gain
    //--------------------------------------------------------------------
    void setChanged( Boolean b );

    //--------------------------------------------------------------------
    /// Sets the name of the gain
    //--------------------------------------------------------------------
    void setGainName( GainsManagerNS::GainsNames name );

private:
    //--------------------------------------------------------------------
    /// Copy constructor
    /// Derived classes should use the assignment operator to copy.
    ///
    /// @param[in] cb - GainsConfig to copy
    //--------------------------------------------------------------------
    GainsConfig(const GainsConfig& cb );

private:
    /// Name of the gain this object is controlling
    GainsManagerNS::GainsNames m_name;

	Real32 m_gain;         /// The gain will be set into
	Real32 m_originalGain; /// The original gain obtained from the controller
	Boolean m_changed;     /// Indicates whether m_gain has changed
};

#endif // GAINSCONFIG_HH_
