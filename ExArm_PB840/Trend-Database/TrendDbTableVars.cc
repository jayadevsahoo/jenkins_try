#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: TrendDbTableVars
//---------------------------------------------------------------------
//@ Interface-Description
//  The TrendDbTableVars keeps track of the table information such as
//  the data size, insertion point "head" and the starting block on
//  the compact flash.  The table vars also stores the capacity of the 
//  table in blocks.  
// 
//  The TrendDbTableVars are configured using the config() method, this 
//  method takes the starting block, table capacity in blocks and the data 
//  size.  The client can then use a set of const methods to get properties of
//  the table such as head, tail, size, start block, end block, and capacity.
//  The client can also find the block on the compact flash for a given table 
//  position (where position 0 is the head, and position = size is the oldest),
//  via the getPositionLba() call.
// 
//  The client can update the table information by calling incLba(), that 
//  increments the table one block.  The data size is incremented until table
//  capacity is reached and the insertion point "head" will wrap around to the 
//  beginning when the end is reached.  The client can also clear/reset the table
//  back to an "empty" state via the clear() method.
//  
//---------------------------------------------------------------------
//@ Rationale
//  Encapsulates the logic to implement a wrap around table on the compact flash
// 
//---------------------------------------------------------------------
//@ Implementation-Description
//  The constructor of the TrendDbTableVars does no configuration, this is 
//  deferred until the config() method is called which allows more control
//  of the construction and initialization to the clients.  All of the get...
//  methods are implemented as const. And the update methods are implemented
//  to support a circular table on the compact flash.
//---------------------------------------------------------------------
//@ Fault-Handling
//  SoftFault is used
//---------------------------------------------------------------------
//@ Restrictions
//  incLba() updates the table for one block on the table, this method
//  must be called following each block that is flushed before the next
//  block can be written to the table
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Trend-Database/vcssrc/TrendDbTableVars.ccv   25.0.4.0   19 Nov 2013 14:34:40   pvcs  $
//
//@ Modification-Log
// 
//  Revision: 001  By:  mjf    Date:  18-Jun-2007    SCR Number: 6237
//    Project:  Trend
//    Description:
//      Initial version
//
//=====================================================================

//@ Usage-Classes
//@ End-Usage
//
#include <string.h>

#include "Trend_Database.hh"

#include "TrendDbTableVars.hh"
#include "TrendDataMgr.hh"
#include "TrendDbRecord.hh"




//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TrendDbTableVars [constructor]
//
//@ Interface-Description
//  Clears the values to zero
//  config() must be used to initialize the object with the correct values
//  for the trend database table before the table can be used
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
TrendDbTableVars::TrendDbTableVars(  )
	:   headLba_( 0 ),
		tailLba_( 0 ),
		sizeLba_( 0 ),
		startLba_( 0 ),
		capLba_( 0 )
{
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TrendDbTableVars [destructor]
//
//@ Interface-Description
//  Does no action  
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
TrendDbTableVars::~TrendDbTableVars()	
{
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: config
//
//@ Interface-Description
//  Initialize the table variables with the start block, capacity,
//  insertion point "head" and the amount of data stored in the table.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The headLba and tailLba offsets are computed relative for startBlock
//---------------------------------------------------------------------
//@ PreCondition
//	The headBlock must be greater or equal to the startBlock
//  The data size must be less than the table capacity
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDbTableVars::config( Uint32 startBlock, Uint32 capacityLba,
							   Uint32 headBlock, Uint32 sizeLba )
{
	AUX_CLASS_PRE_CONDITION( (headBlock >= startBlock),
							 ((headBlock << 16) | startBlock) );
	AUX_CLASS_PRE_CONDITION( (sizeLba < capacityLba),
							 ((sizeLba << 16) | capacityLba ) );

	startLba_ = startBlock;
	capLba_ = capacityLba;
	sizeLba_ = sizeLba;
	headLba_ = headBlock - startBlock;

	// Compute the position of the database tail
	// If the data size has not reached the capacity then the tail will be at
	// the start of the trend table (offset 0 from the start block), if the
	// data size has reached capacity (ie table is rolled) then the tail will
	// be one block ahead of the head (the data goes back in time from the head to
	// the start of the table rap around to the end of the table back to the head) 
	if (sizeLba_ == (capLba_ - 1) && headLba_ != getEndLba())
	{
		tailLba_ = headLba_ + 1;
	}
	else
	{
		tailLba_ = 0;
	}

	TREND_MGR_DBG4( 16, "  TableVars start ", startLba_,
					" capacity ", capLba_ );
	TREND_MGR_DBG6( 16, "  TableVars size ", sizeLba_,
					" head offset ", headLba_,
					" tail offset ", tailLba_ );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: clear
//
//@ Interface-Description
//  Sets the headLba and tailLba offsets to zero
//  Sets the data size to zero
//---------------------------------------------------------------------
//@ Implementation-Description
//  There is no change to the table start block or capacity
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDbTableVars::clear( void )
{
	headLba_ = 0;
	tailLba_ = 0;
	sizeLba_ = 0;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: incLba
//
//@ Interface-Description
//  Used when a block is flushed to the table and the next block in the
//  table needs to be setup.  The incLba() method will update the insertion
//  point, with wrap around.  It will also update the data size of the table.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Updates head and tail of the table with a wrap around implementation 
//  Data size is updated until the capacity of the table is reached
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDbTableVars::incLba( void )
{
	// Update the headLba_, roll if needed
	headLba_++;
	if (headLba_ >= capLba_)
	{
		// wrap the head back to the start
		headLba_ = 0;
	}

	// Update the size, only grows to capacity
	if (sizeLba_ < (capLba_ - 1))
	{
		sizeLba_++;
	}
	else
	{
		// database full, and started to wrap
		// Update the tail
		tailLba_++;
		if (tailLba_ >= capLba_)
		{
			// wrap tail to start
			tailLba_ = 0;
		}
	}

	TREND_MGR_DBG4( 32, "  TableVars inc, head ", headLba_, ", tail ", tailLba_ );
	TREND_MGR_DBG4( 32, "    size ", sizeLba_, ", capacity ", capLba_ );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getPositionLba
//
//@ Interface-Description
//  Computes the LBA block on the compact flash given an offset from the
//  current "head" of the table.  Position 0 is the most current data.
//  Returns -1 if the position is out of range.
//---------------------------------------------------------------------
//@ Implementation-Description
//  First the position is validated
//  Then an offset is computed from the head, if this offset goes negative
//  then the position is rapped around from the end of the table.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
Int32 TrendDbTableVars::getPositionLba( Uint32 position ) const
{
	Int32 offset;

	if (position <= sizeLba_)
	{
		offset = headLba_ - position;

		// offset can go negative when wrapping around
		if (offset < 0)
		{
			offset += capLba_;

			TREND_MGR_DBG4( 32, "TableDbVars position wrap ", position, ", offset ", offset );
			TREND_MGR_DBG6( 32, "    head ", headLba_, ", tail ", tailLba_, ", size ", sizeLba_ );
		}
	}
	else
	{
		// Invalid range, position is beyond the data size
		offset = -1;
	}

	// If not in error, adjust offset to LBA number
	if (offset != -1)
	{
		offset += startLba_;
	}

	TREND_MGR_DBG2( 32, "TableDbVars position return LBA ", offset );

	return( offset );
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================
void TrendDbTableVars::SoftFault(const SoftFaultID  softFaultID,
								 const Uint32       lineNumber,
								 const char*        pFileName,
								 const char*        pPredicate)  
{
	FaultHandler::SoftFault(softFaultID, TREND_DATABASE, TREND_DB_TABLE_VARS,
							lineNumber, pFileName, pPredicate);
}





