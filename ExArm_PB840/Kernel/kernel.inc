;=====================================================================
; This is a proprietary work to which Puritan-Bennett corporation of
; California claims exclusive right.  No part of this work may be used,
; disclosed, reproduced, sorted in an information retrieval system, or
; transmitted by any means, electronic, mechanical, photocopying,
; recording, or otherwise without the prior written permission of
; Puritan-Bennett Corporation of California.
;
;            Copyright (c) 1995, Puritan-Bennett Corporation
;=====================================================================
;
;
;============================ M O D U L E   D E S C R I P T I O N ====
;@ Filename: kernel.inc - include file for Kernel phase of POST 
;
;---------------------------------------------------------------------
;@ Interface-Description
;  This contains the defintions required by the POST code.
;
;@ Implementation-Description
;
;  This module contains all the EQU's, MACRO's, and other assembly language
;  constructs required by the POST code.  These include, among other things,
;  the addresses of the devices tested, register bit patterns for programming
;  devices, and constants used to initialize devices.
;
;@ Restrictions
;  
;---------------------------------------------------------------------
;@ End-Preamble
;
;@ Version
;
;@(#) $Header:   /840/Baseline/Kernel/vcssrc/kernel.inv   25.0.4.0   19 Nov 2013 14:13:54   pvcs  $
;
;@ Modification-Log
;
;  Revision: 006    By: Gary Cederquist  Date: 23-Sep-1999  DR Number: 5507
;    Project: 840 Cost Reduction
;    Description:
;       Modified 'SIMTEK_NOVRAM_BANK0_RECALL?' to use correct addresses needed
;       for a Manual Recall to occur.
;
;  Revision: 005    By: Gary Cederquist  Date: 14-Dec-1998  DR Number: 5311
;    Project: 840 Cost Reduction
;    Description:
;       Initial Version.
;
;  Revision: 004    By: Gary Cederquist  Date: 04-DEC-1997  DR Number: 2516
;    Project: Sigma (R8027)
;    Description:
;       Added definitions for I/O register 2 and the NV_RECALL register.
;
;  Revision: 003    By: Gary Cederquist  Date: 19-AUG-1997  DR Number: 2361
;    Project: Sigma (R8027)
;    Description:
;       Added check for presence of 68901 to enable the DRAM refresh
;       strobe or to ignore it for boards without a 68901.
;
;  Revision: 002    By: Gary Cederquist  Date: 15-MAY-1997  DR Number: 2010
;    Project: Sigma (R8027)
;    Description:
;       Changed refresh rate to 15us per DRAM specification.
;
;  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997  DR Number:
;    Project: Sigma (R8027)
;    Description:
;       Initial version
;
;=====================================================================
;

IO_REGISTER_1               EQU  $FFBE8000

; IO Register 1 Read bit assignments
IO1_BD_CPU                  EQU  %10000000
IO1_BD_NV_RECALL_NOT_DONE   EQU  %01000000
IO1_FAN_ALARM_ACTIVE        EQU  %00010000
IO1_NOVRAMNE                EQU  %00001000
IO1_BD_WATCHDOG_BITE        EQU  %00000100
IO1_LAN_BACKOFF             EQU  %00000010
IO1_FLASH_VPP_ENABLED       EQU  %00000001

; IO Register 1 Write bit assignments
IO1_ENABLE_AUDIO            EQU  %00100000
IO1_ENABLE_LAN              EQU  %00010000
IO1_ENABLE_NONVOLATILITY    EQU  %00001000
IO1_ENABLE_FLASH1_VPP       EQU  %00000100
IO1_ENABLE_FLASH2_VPP       EQU  %00000010
IO1_ENABLE_FLASH_VPP        EQU  %00000001

; IO Register 2 implemented on the GUI only
IO_REGISTER_2               EQU  $FFBE8001

; IO Register 2 Read bit assignments
IO2_GUI_AUDIOACK_OFF        EQU  %10000000
IO2_GUI_NV_RECALL_DONE      EQU  %01000000
IO2_GUI_AUDIOATTN_OFF       EQU  %00100000
IO2_GUI_WATCHDOG_BITE       EQU  %00001000
IO2_GUI_LED_NOT_CONNECTED   EQU  %00000100
IO2_GUI_KBD_NOT_CONNECTED   EQU  %00000010
IO2_GUI_AUDIOBUSY_OFF       EQU  %00000001

; Writing a byte to this register asserts the RECALL_DONE state
IO_NV_RECALL_DONE_REGISTER  EQU  $FFBE800D

;  The 256K x 16 DRAM requires a 512-cycle refresh in 8ms (9 rows/9 cols)
;  
;    Refresh Period = 8ms / 512 = 15.625us / refresh cycle
;
;  Assuming an 8Mhz clock = .125us / count
;
;  The 901 output toggles when timer count expires.  The DRAM controller
;  looks at the leading edge so:
;
;    901 Counts = (15.625us per refresh cycle) / (2 * .125us per count) 
;               = 62.5 counts per refresh cycle
;
;  The minimum prescaler for the 901 is /4 so:
;
;    901 Count Down Value = 62.5 / 4 = 15.625 counts
;      
;  A count down value of 15 gives an effective refresh rate of:
;
;    Refresh Rate = 2 * 15 * 4 * .125us = 15us
;

P_DRAM_ENABLE_ADR       EQU  $FFBE8298  ;used to turn on the DRAM contoller
P_REFRESH_TIMER_COUNT   EQU  15         ;15 counts of the timer
P_REFRESH_MODE_VALUE    EQU  %00000001  ;delay mode, Divide by 4, the output
                                        ; toggles when 4 * timer_count clock
                                        ; cycles occur. Therefore, since the
                                        ; DRAM controller looks at one edge
                                        ; the refresh rate = clk/(8*count)

P_BOOT_MAP_DISABLE_ADR  EQU  $FFBE802C  ;used to disable the special mapping
                                        ;of the boot prom to 0 that is used
                                        ;during the boot up process. Any
                                        ;read or write to this location will
                                        ;disable the boot mapping.  This is
                                        ;the same location as the autovec 
                                        ;mask register.

P_AUTOVECTOR_MASK_REG   EQU  $FFBE802C  ;also used for disabling auto-vector
                                        ;interrupts

IO_MASK_AUTOVECTOR3     EQU  %00000001  ;watchdog timer test
IO_MASK_AUTOVECTOR4     EQU  %00000010  ;watchdog timer test
IO_MASK_AUTOVECTOR5     EQU  %00000100  ;ethernet
IO_MASK_AUTOVECTOR6     EQU  %00001000  ;system timer 5ms

;CPU runs non-cached until VRTX initializes the MMU

CACR_INITIAL_VALUE      EQU  $00008000  ;bit 31 = 1 means Data Cache Enabled
                                        ;bit 15 = 1 means Code Cache Enabled


;Constants for the Kernel DRAM test
TOO_MANY_ADDRESS_BITS EQU          22      ;maximum number of address bits + 1
                                           ;that can be tested in a 
                                           ;4 Mbyte system

N_DRAM_ADR_BITS            EQU     21      ;number of bits for address bus test
DRAM_BASE_ADR              EQU     $0      ;start of entire DRAM
DRAM_BYTES                 EQU     $400000 ;total bytes of DRAM - 4MB

; Kernel definitions
KRAM_START_ADR             EQU     $0      ;start of DRAM used by POST
KRAM_BYTES                 EQU     $8000   ;number of bytes of DRAM for POST

; POST definitions
DRAM_TEST_START            EQU     DRAM_BASE_ADR+KRAM_BYTES   ; $8000
DRAM_TEST_BYTES            EQU     DRAM_BYTES-DRAM_TEST_START ; $3f8000



; definitions for the SGS-Thomson  MK68901 Multi-Function Peripheral
M68901_BASE_ADR         EQU     $FFBE8080
M68901_GPDR             EQU     M68901_BASE_ADR+$0
M68901_AER              EQU     M68901_BASE_ADR+$1
M68901_DDR              EQU     M68901_BASE_ADR+$2
M68901_IERA             EQU     M68901_BASE_ADR+$3
M68901_IERB             EQU     M68901_BASE_ADR+$4
M68901_IPRA             EQU     M68901_BASE_ADR+$5
M68901_IPRB             EQU     M68901_BASE_ADR+$6
M68901_ISRA             EQU     M68901_BASE_ADR+$7
M68901_ISRB             EQU     M68901_BASE_ADR+$8
M68901_IMRA             EQU     M68901_BASE_ADR+$9
M68901_IMRB             EQU     M68901_BASE_ADR+$A
M68901_VR               EQU     M68901_BASE_ADR+$B
M68901_TACR             EQU     M68901_BASE_ADR+$C
M68901_TBCR             EQU     M68901_BASE_ADR+$D
M68901_TCDCR            EQU     M68901_BASE_ADR+$E
M68901_TADR             EQU     M68901_BASE_ADR+$F
M68901_TBDR             EQU     M68901_BASE_ADR+$10
M68901_TCDR             EQU     M68901_BASE_ADR+$11
M68901_TDDR             EQU     M68901_BASE_ADR+$12
M68901_SCR              EQU     M68901_BASE_ADR+$13
M68901_UCR              EQU     M68901_BASE_ADR+$14
M68901_RSR              EQU     M68901_BASE_ADR+$15
M68901_TSR              EQU     M68901_BASE_ADR+$16
M68901_UDR              EQU     M68901_BASE_ADR+$17

; definitions for the 2 Intel 82C54 Timers
I82C541_BASE_ADR        EQU     $FFBE8011
I82C541_CNT0_ADR        EQU     I82C541_BASE_ADR+0
I82C541_CNT1_ADR        EQU     I82C541_BASE_ADR+2
I82C541_CNT2_ADR        EQU     I82C541_BASE_ADR+4
I82C541_CTRL_ADR        EQU     I82C541_BASE_ADR+6

I82C54_SELECT0          EQU     %00000000
I82C54_SELECT1          EQU     %01000000
I82C54_SELECT2          EQU     %10000000

I82C54_LATCH_CNTR       EQU     %00000000
I82C54_RW_LSB           EQU     %00010000
I82C54_RW_MSB           EQU     %00100000
I82C54_RW_BOTH          EQU     %00110000

I82C54_MODE0            EQU     %00000000
I82C54_MODE1            EQU     %00000010
I82C54_MODE2            EQU     %00000100
I82C54_MODE3            EQU     %00000110
I82C54_MODE4            EQU     %00001000
I82C54_MODE5            EQU     %00001010

I82C54_BCD_ON           EQU     %00000001

* Value to which the other 82C54 timers are initialized.
* They need to be initialized since they come up in potentially
* random modes and may be interrupting
I82C54_INIT_VAL         EQU     (I82C54_RW_BOTH!I82C54_MODE2)
I82C54_COUNT            EQU     0

; Autovector reset registers

AUTOVEC3_RESET_ADR      EQU     $FFBE8030
AUTOVEC4_RESET_ADR      EQU     $FFBE8031
AUTOVEC5_RESET_ADR      EQU     $FFBE8033
AUTOVEC6_RESET_ADR      EQU     $FFBE8032

;  Exception vector assignments (Table B1 of M68000 programmers reference manual)
RESET_INITIAL_SP_VECTOR   	EQU    $0
RESET_INITIAL_PC_VECTOR   	EQU    $4
BUS_ERROR_VECTOR		EQU    $8
ADDRESS_ERROR_VECTOR_VECTOR 	EQU    $C		
ILLEGAL_INSTRUCTION_VECTOR   	EQU   $10
DIVIDE_BY_ZERO_VECTOR     	EQU   $14
CHK_INSTRUCTION_VECTOR          EQU   $18
TRAPCC_INSTRUCTION_VECTOR       EQU   $1C
PRIVILEGE_VIOLATION_VECTOR      EQU   $20
TRACE_VECTOR         		EQU   $24
A_LINE_OPCODE_VECTOR 		EQU   $28
F_LINE_OPCODE_VECTOR 		EQU   $2C 
RESERVED_VECTOR 		EQU   $30  
COPROCESSOR_PROTOCOL_VECTOR 	EQU   $34   
FORMAT_ERROR_VECTOR 		EQU   $38    
UNITIALIZED_INTERRUPT_VECTOR 	EQU   $3C     

SPURIOUS_INTERRUPT_VECTOR 	EQU   $60      
LEVEL_1_INTERRUPT_VECTOR        EQU   $64			        
LEVEL_2_INTERRUPT_VECTOR        EQU   $68				 
LEVEL_3_INTERRUPT_VECTOR        EQU   $6C				  
LEVEL_4_INTERRUPT_VECTOR        EQU   $70				   
LEVEL_5_INTERRUPT_VECTOR        EQU   $74				    
LEVEL_6_INTERRUPT_VECTOR        EQU   $78				     
LEVEL_7_INTERRUPT_VECTOR        EQU   $7C				      
TRAP_0_VECTOR                   EQU   $80
TRAP_1_VECTOR                   EQU   $84
TRAP_2_VECTOR                   EQU   $88
TRAP_3_VECTOR                   EQU   $8C
TRAP_4_VECTOR                   EQU   $90
TRAP_5_VECTOR                   EQU   $94
TRAP_6_VECTOR                   EQU   $98
TRAP_7_VECTOR                   EQU   $9C
TRAP_8_VECTOR                   EQU   $A0
TRAP_9_VECTOR                   EQU   $A4
TRAP_A_VECTOR                   EQU   $A8
TRAP_B_VECTOR                   EQU   $AC
TRAP_C_VECTOR                   EQU   $B0
TRAP_D_VECTOR                   EQU   $B4
TRAP_E_VECTOR                   EQU   $B8
TRAP_F_VECTOR                   EQU   $BC
FP_BRANCH_VECTOR                EQU   $C0
FP_INEXACT_RESULT_VECTOR        EQU   $C4       
FP_DIVIDE_BY_ZERO_VECTOR        EQU   $C8         
FP_UNDERFLOW_VECTOR             EQU   $CC
FP_OPERAND_ERROR_VECTOR         EQU   $D0
FP_OVERFLOW_VECTOR              EQU   $D4
FP_NAN_VECTOR                   EQU   $D8
FP_UNIMPLEMENTED_DATA_VECTOR    EQU   $DC
MMU_CONFIG_ERROR_VECTOR         EQU   $E0
MMU_ILLEGAL_OP_ERROR_VECTOR     EQU   $E4
MMU_ACCESS_ERROR_VECTOR         EQU   $E8
     


Z85230_BASE_ADDR        EQU      $FFBE8021
Z85230_COMMAND_B        EQU      Z85230_BASE_ADDR+0
Z85230_DATA_B           EQU      Z85230_BASE_ADDR+2
Z85230_COMMAND_A        EQU      Z85230_BASE_ADDR+4
Z85230_DATA_A           EQU      Z85230_BASE_ADDR+6



; definitions for the Non-volatile RAM location and size
NOVRAM_START            EQU   $1000000
NOVRAM_LEN              EQU      $4000
OFFSET_FOR_ICE          EQU       $100 ;amount to offset the initial stack
                                       ; pointer down from the top of NOVRAM
                                       ; to avoid problems with the Applied
                                       ; Microsystems In Circuit Emulator

;P_INITIAL_STACK         EQU   NOVRAM_START+NOVRAM_LEN-OFFSET_FOR_ICE
P_INITIAL_STACK         EQU   NOVRAM_START+$400     ; end of POST NOVRAM (1K)
KERNEL_STACK_POINTER    EQU   KRAM_START_ADR+KRAM_BYTES-OFFSET_FOR_ICE




P_MAX_TIMER_TEST_COUNT  EQU  16  ;maximum amount that the counter is 
                                 ; allowed to change during the timer
                                 ; count test



; Definitions for the Diagnostic Leds - these provide the ability
; to display the progress that the POST has made by writing coded
; patterns to the LEDs as PORT proceeds.
; NOTE:  The led register requires inverted logic.  That is; to turn
;        an LED on it's bit in the register must be set to 0.  The macro
;        WRITE_LED handles the inversion of the logic.
;
; IMPORTANT:  Any changes to this portion need to be reflected in kerrors.hh

P_DIAG_LED_REG                               EQU  $FFBE800C

P_NOVRAM_BASE                                EQU  $01000000
P_LAST_TEST_IN_PROGRESS                      EQU  P_NOVRAM_BASE+0
P_ERROR_CODE                                 EQU  P_NOVRAM_BASE+1

POST_ERROR_PROCESSOR_INITIALIZATION              EQU          1
POST_ERROR_INTEGER_UNIT                          EQU          2
POST_ERROR_DRAM_REFRESH_TIMER                    EQU          3
POST_ERROR_DRAM_STACK                            EQU          4
POST_ERROR_BOOT_EPROM                            EQU          5
POST_ERROR_TEST_FAILURE                          EQU          $cc
   

WRITE_LED           MACRO  value
                    move.b #"value,P_DIAG_LED_REG ;set the register to the
                    ENDM                          ;bit-wise inversion of the 
                                                  ;value passed in
POST_STEP           MACRO  value
                    move.b #value,P_LAST_TEST_IN_PROGRESS  ;set current step
                    move.b #POST_ERROR_TEST_FAILURE,P_ERROR_CODE ;LED error code
                    move.b #"value,P_DIAG_LED_REG          ;display current step
                    ENDM

STROBE_WATCHDOG     MACRO  
                    clr.b  $ffbe9000 
                    ENDM  

UNIT_TEST_POINT     MACRO    name        ;{
                    XDEF     UT_&&name
UT_&&name:
                    ENDM                 ;}UNIT_TEST_POINT


; Return Codes for POST tests
PASSED               EQU   0
MINOR                EQU   1
SERVICE_REQUIRED     EQU   2
MAJOR                EQU   3

; Safety Net Control Registers
SAFETY_NET_CONTROL_A      EQU   $ffbeb008
SAFETY_NET_CONTROL_B      EQU   $ffbeb00a
DISABLE_TEN_SECOND_TIMER  EQU   %00000001

; NOVRAM Recall Addresses
NOVRAM_BANK0_RECALL      EQU   $1000000
NOVRAM_BANK1_RECALL      EQU   $1001000
NOVRAM_BANK2_RECALL      EQU   $1002000
NOVRAM_BANK3_RECALL      EQU   $1003000

; NOVRAM Command Addresses
NOVRAM_BANK0_COMMAND0    EQU   $1000AAA
NOVRAM_BANK0_COMMAND1    EQU   $1000554
NOVRAM_BANK1_COMMAND0    EQU   $1001AAA
NOVRAM_BANK1_COMMAND1    EQU   $1001554
NOVRAM_BANK2_COMMAND0    EQU   $1002AAA
NOVRAM_BANK2_COMMAND1    EQU   $1002554
NOVRAM_BANK3_COMMAND0    EQU   $1003AAA
NOVRAM_BANK3_COMMAND1    EQU   $1003554

; NOVRAM Commands
NOVRAM_COMMAND_ENABLE    EQU   $AAAA
NOVRAM_COMMAND_CONFIRM   EQU   $5555
NOVRAM_ENABLE_AUTOSTORE  EQU   $CCCC
NOVRAM_RESET_AUTOSTORE   EQU   $CDCD
NOVRAM_STORE_IMMEDIATE   EQU   $3333

; Simtek NOVRAM Recall Addresses
SIMTEK_NOVRAM_BANK0_RECALL1 EQU   P_NOVRAM_BASE+($0E38<<1)
SIMTEK_NOVRAM_BANK0_RECALL2 EQU   P_NOVRAM_BASE+($31C7<<1)
SIMTEK_NOVRAM_BANK0_RECALL3 EQU   P_NOVRAM_BASE+($03E0<<1)
SIMTEK_NOVRAM_BANK0_RECALL4 EQU   P_NOVRAM_BASE+($3C1F<<1)
SIMTEK_NOVRAM_BANK0_RECALL5 EQU   P_NOVRAM_BASE+($303F<<1)
SIMTEK_NOVRAM_BANK0_RECALL6 EQU   P_NOVRAM_BASE+($0C63<<1)

; NMI Source Register
;
; the upper two bits contain a version code 
; - older revision boards contain a non-zero code in these bits
; - newer revision boards contain zeros in these bits to
;   indicate that another register should be read to determine
;   the revision
; - the kernel is only interested in whether the board is an older
;   board which contains a MC68901 or a newer revision which does not

NMI_SOURCE_REGISTER      EQU   $FFBE8008
NMI_REV_MASK             EQU   $C0
NMI_REV_COST_REDUCE      EQU   0
