
#ifndef ProxEnabledSetting_HH
#define ProxEnabledSetting_HH

//====================================================================
//
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2010, Covidien
//====================================================================

//====================================================================
// Class:  ProxEnabledSetting - Prox Enabled Setting.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/ProxEnabledSetting.hhv   25.0.4.0   19 Nov 2013 14:27:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003   By: rhj   Date: 10-Aug-2010     SCR Number: 6601
//  Project:  PROX
//  Description:
//      Added disableProx_ and disableProx_() method.
//
//  Revision: 002   By: rhj    Date: 02-Aug-2010     SCR Number: 6624
//  Project:  PROX
//  Description:
//      Changed the default setting from disabled to enabled.
//
//  Revision: 001   By: rhj    Date:  25-Jan-2010    SCR Number: 6436
//  Project: PROX
//  Description:
//		Initial version.
//====================================================================

//@ Usage-Classes
#include "BatchDiscreteSetting.hh"
//@ End-Usage


class ProxEnabledSetting : public BatchDiscreteSetting
{
public:
	ProxEnabledSetting(void);
	virtual ~ProxEnabledSetting(void);

	virtual Applicability::Id  getApplicability(
											   const Notification::ChangeQualifier qualifierId
											   ) const;

	virtual SettingValue  getNewPatientValue(void) const;

	virtual void  acceptTransition(const SettingId::SettingIdType settingId,
								   const DiscreteValue            newValue,
								   const DiscreteValue            currValue);


	static void  SoftFault(const SoftFaultID softFaultID,
						   const Uint32 lineNumber,
						   const char*  pFileName  = NULL, 
						   const char*  pPredicate = NULL);

	void disableProx();
private:
	ProxEnabledSetting(const ProxEnabledSetting&);	// not implemented...
	void  operator=(const ProxEnabledSetting&);		// not implemented...

	//@ Data-Member: disableProx_
	// Flag which disables prox.
	Boolean disableProx_;
};


#endif // ProxEnabledSetting_HH 
