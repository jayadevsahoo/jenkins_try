#ifndef AtmPresXducerCalibration_HH
#define AtmPresXducerCalibration_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
// Class:  AtmPresXducerCalibration - calibrate the atmospheric
//         pressure transducer.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/AtmPresXducerCalibration.hhv   25.0.4.0   19 Nov 2013 14:23:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 001  By:  quf    Date: 24-Sep-1997    DR Number: DCS 2410
//       Project:  Sigma (840)
//       Description:
//		 	Initial version (Integration baseline).
//
//=====================================================================

#include "ServiceMode.hh"

//@ Usage-Classes
//@ End-Usage


class AtmPresXducerCalibration {
  public:
    AtmPresXducerCalibration( void);
    ~AtmPresXducerCalibration( void);

    static void SoftFault( const SoftFaultID	softFaultID,
						   const Uint32		lineNumber,
						   const char		*pFileName  = NULL, 
						   const char		*pPredicate = NULL) ;

	void readXducer( const char * const pParameters );
	void runCalibration( const char * const pParameters );

  protected:

  private:
    AtmPresXducerCalibration( const AtmPresXducerCalibration&);  // not implemented...
    void operator=( const AtmPresXducerCalibration&);       // not implemented...

} ;


#endif // AtmPresXducerCalibration_HH 
