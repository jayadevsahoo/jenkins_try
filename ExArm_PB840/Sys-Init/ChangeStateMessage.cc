#include "stdafx.h"
//=====================================================================
//   This is a proprietary work to which Puritan-Bennett corporation
//   of California claims exclusive right.  No part of this work may
//  be used, disclosed, reproduced, sorted in an information retrieval
//     system, or transmitted by any means, electronic, mechanical,
//   photocopying, recording, or otherwise without the prior written
//      permission of Puritan-Bennett Corporation of California.
//
//          Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: ChangeStateMessage - Change-State Message.
//---------------------------------------------------------------------
//@ Interface-Description
//  The ChangeStateMessage class is a TaskControlMessage used to direct
//  task control and application tasks to different SigmaStates.
//  Applications (specifically the Breath-Delivery subsystem) invokes 
//  TaskControlAgent::ChangeState to create and queue this message to
//  the Task-Control task.  The Task-Control task invokes 
//  TaskAgent::changeState to create and queue this message to the
//  applications informing them of a completed state transition.  
//  This class uses the AppContext callback mechanism to dispatch the 
//  message to the receiving task's callback function.
//---------------------------------------------------------------------
//@ Rationale
//  The ChangeStateMessage contains information for directing or
//  informing of a state transition.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class is a derived TaskControlMessage.  The base class new and
//  delete operators override the library operators to allocate memory
//  for the message in the global BufferPool.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Sys-Init/vcssrc/ChangeStateMessage.ccv   25.0.4.0   19 Nov 2013 14:33:50   pvcs  $
//
//@ Modification-Log
//
//   Revision 002   By:   sah      Date: 01/13/99    DR Number: 5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to a non-inlined method.
//
//   Revision 001   By:   gdc      Date: 07/06/95    DR Number:   n/a  
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//=====================================================================

//=====================================================================
//
//      Includes...
//
//=====================================================================

#include "ChangeStateMessage.hh"
#include <string.h>

//@ Usage-Classes
#include "AppContext.hh"
#include "SigmaState.hh"
//@ End-Usage

//=====================================================================
//
//      Static Data...
//
//=====================================================================

//@ Code...

//=====================================================================
//
//      Public Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ChangeStateMessage [constructor]
//
//@ Interface-Description
//  The ChangeStateMessage constructor initializes the message with the
//  specified SigmaState.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls the base class TaskControlMessage constructor with the 
//  classId for this message.  Sets the internal state_.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================


ChangeStateMessage::ChangeStateMessage(  SigmaState state
                                       , const TaskAgent * const pTaskAgent
                                       , Uint32 sequence )

    :   TaskControlMessage(Sys_Init::ID_CHANGESTATEMESSAGE)
      , state_(state) 
      , pTaskAgent_(pTaskAgent)
      , sequenceNumber_(sequence)

{
    CALL_TRACE("ChangeStateMessage(SigmaState)");
    // $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ChangeStateMessage [copy constructor]
//
//@ Interface-Description
//  Copy constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Initializes the TaskControlMessage base class and private state
//  from reference object.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ChangeStateMessage::ChangeStateMessage(const ChangeStateMessage& rMessage)

    :   TaskControlMessage(rMessage)
      , state_(rMessage.state_) 
      , pTaskAgent_(rMessage.pTaskAgent_)
      , sequenceNumber_(rMessage.sequenceNumber_)
{
    CALL_TRACE("ChangeStateMessage(ChangeStateMessage&)");
    // $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~ChangeStateMessage [destructor]
//
//@ Interface-Description
//  ChangeStateMessage destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ChangeStateMessage::~ChangeStateMessage()
{
    CALL_TRACE("~ChangeStateMessage()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
// Method: nameOf
//
// Interface-Description
//  Returns the name of the class.
//---------------------------------------------------------------------
// Implementation-Description
//  Returns a pointer to the const string containing the class name.
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================
 
const char*
ChangeStateMessage::nameOf(void) const
{
    CALL_TRACE("nameOf()");
    switch ( state_ )
    {
        case (STATE_START):           // $[TI1]
            return "ChangeState-Start";
        case (STATE_ONLINE):          // $[TI2]
            return "ChangeState-Online";
        case (STATE_SERVICE):         // $[TI3]
            return "ChangeState-Service";
        case (STATE_SST):             // $[TI4]
            return "ChangeState-SST";
        case (STATE_INOP):            // $[TI5]
            return "ChangeState-Inop";
        case (STATE_TIMEOUT):         // $[TI6]
            return "ChangeState-Timeout";
        case (STATE_INIT):            // $[TI8]
            return "ChangeState-Init";
        default:                      // $[TI7]
            break;
    };
    return "ChangeState-Invalid";
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: dispatch
//
//@ Interface-Description
//  The method dispatches this ChangeStateMessage to the callback
//  function registered with the specified AppContext.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls the AppContext dispatch method with a reference to this
//  ChangeStateMessage.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
ChangeStateMessage::dispatch(const AppContext& context) const
{
    CALL_TRACE("dispatch(const AppContext&)");
    context.dispatch(*this);
    // $[TI1]
}

 

#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//
//@ Interface-Description
//   This method is called when a software fault is detected by the
//   fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//   'lineNumber' are essential pieces of information.  The 'pFileName'
//   and 'pPredicate' strings may be defaulted in the macro to reduce
//   code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This method receives the call for the SoftFault, adds it sub-system
//   and class name ID and sends the message to the non-member function
//   SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
void
ChangeStateMessage::SoftFault(const SoftFaultID  softFaultID,
                              const Uint32       lineNumber,
                              const char*        pFileName,
                              const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
 
  FaultHandler::SoftFault(softFaultID, SYS_INIT,
                          Sys_Init::ID_CHANGESTATEMESSAGE, 
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)

//=====================================================================
//
//      Protected Methods...
//
//=====================================================================

//=====================================================================
//
//      Private Methods...
//
//=====================================================================



