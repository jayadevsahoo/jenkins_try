#include "stdafx.h"
#include <bsp_version.h>

const char OsVersionString[NMI_VERSION_STRING_SIZE] = {__DATE__};

//------------------------------------------------------------------------------
//
//  Function:  Convert_MonthString_To_Month_Digit
//
DWORD Convert_MonthString_To_Month_Digit(WCHAR *InputString)
{
    if(wcscmp(InputString, _T("Jan")) == 0)
    {
        return 1;
    }
    else if(wcscmp(InputString, _T("Feb")) == 0)
    {
        return 2;
    }
    else if(wcscmp(InputString, _T("Mar")) == 0)
    {
        return 3;
    }
    else if(wcscmp(InputString, _T("Apr")) == 0)
    {
        return 4;
    }
    else if(wcscmp(InputString, _T("May")) == 0)
    {
        return 5;
    }
    else if(wcscmp(InputString, _T("Jun")) == 0)
    {
        return 6;
    }
    else if(wcscmp(InputString, _T("Jul")) == 0)
    {
        return 7;
    }
    else if(wcscmp(InputString, _T("Aug")) == 0)
    {
        return 8;
    }
    else if(wcscmp(InputString, _T("Sep")) == 0)
    {
        return 9;
    }
    else if(wcscmp(InputString, _T("Oct")) == 0)
    {
        return 10;
    }
    else if(wcscmp(InputString, _T("Nov")) == 0)
    {
        return 11;
    }
    else if(wcscmp(InputString, _T("Dec")) == 0)
    {
        return 12;
    }
    else
    {
        return 13;  // something is wrong!!
    }
}

//------------------------------------------------------------------------------
//
//  Function:  myatoi
//
//	Convert a wide character string to integer
//
UINT16 myatoi(const WCHAR *str)
{
	UINT16 i, num = 0;
	int strlen;	
	
	strlen = wcslen(str);
	for(i = 0; i < strlen; i++) 
	{ 
		if(str[i] >= '0' && str[i] <= '9') 
			num = num * 10 + str[i] - '0';
	}

	return(num);
}

VOID GetOsVersion(NMI_VERSION *pOsVersion)
{
	WCHAR *pStr;
	WCHAR MonthString[MONTH_STRING_LENGTH+1];
	WCHAR DayString[DAY_STRING_LENGTH+1];
	WCHAR YearString[YEAR_STRING_LENGTH+1];

	// Converting the version string from ASCII to wide character
	for (int i = 0; i < NMI_VERSION_STRING_SIZE - 1; i++)
    {
        pOsVersion->VersionString[i] = 0;
        pOsVersion->VersionString[i] |= OsVersionString[i];
    }
    pOsVersion->VersionString[NMI_VERSION_STRING_SIZE - 1] = 0;		// make sure the version string is terminated

	pStr = pOsVersion->VersionString;
	wcsncpy(MonthString, pStr, MONTH_STRING_LENGTH);
	MonthString[MONTH_STRING_LENGTH] = 0;
	pOsVersion->Month = Convert_MonthString_To_Month_Digit(MonthString);
	
	pStr += 4;
	wcsncpy(DayString, pStr, DAY_STRING_LENGTH);
	DayString[DAY_STRING_LENGTH] = 0;
	pOsVersion->Day = myatoi(DayString);
	
	pStr += 3;
	wcsncpy(YearString, pStr, YEAR_STRING_LENGTH);
	YearString[YEAR_STRING_LENGTH] = 0;
	pOsVersion->Year = myatoi(YearString) - 2000;
}