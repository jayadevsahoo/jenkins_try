#include "stdafx.h"
//=====================================================================
//   This is a proprietary work to which Puritan-Bennett corporation
//   of California claims exclusive right.  No part of this work may
//  be used, disclosed, reproduced, sorted in an information retrieval
//     system, or transmitted by any means, electronic, mechanical,
//   photocopying, recording, or otherwise without the prior written
//      permission of Puritan-Bennett Corporation of California.
//
//          Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: SyncClockTimer - Synchronize System Clocks Timer
//---------------------------------------------------------------------
//@ Interface-Description
//  The SyncClockTimer is a TaskControlTimer created by GUI task
//  control and used as a periodic timer for synchronizing the BD
//  system clock.  It is set by GuiStart after receiving the first
//  BdReadyMessage from the BD.  The timer expires every 1 hour
//  at which time MsgTimer queues the SyncClockTimer message to task
//  control.  GUI task control processes the message through its finite
//  state machine and sends a SyncClockMessage to BD task control.  It
//  then resets the timer for the next interval.  The static function
//  GetTimer() contains the single static instantiation of
//  SyncClockTimer.  The TaskControlTimer functions set() and cancel()
//  start and stop the timer respectively.  This class uses the 
//  AppContext callback mechanism to dispatch the message to the
//  application callback function.
//---------------------------------------------------------------------
//@ Rationale
//  The SyncClockTimer is the timer for periodically synchronizing the
//  BD system clock to the GUI clock.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The SyncClockTimer is derived from the TaskControlTimer class which
//  provides the timing mechanism.  The GetTimer() function initializes
//  a static SyncClockTimer and returns its reference to the caller.
//  The constructor and destructor are private methods allowing object
//  construction only from the static member function GetTimer().  The
//  dispatch() method uses the AppContext to call the task's
//  SyncClockTimer callback function.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Sys-Init/vcssrc/SyncClockTimer.ccv   25.0.4.0   19 Nov 2013 14:33:56   pvcs  $
//
//@ Modification-Log
//
//   Revision 003   By:   sah      Date: 01/13/99    DR Number: 5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to a non-inlined method.
//
//   Revision 002   By:   gdc      Date: 11-JUN-1997 DR Number: 1902
//      Project:   Sigma   (R8027)
//      Description:
//         Modified to provide queue number to TaskControlTimer base
//         class where timer is queued upon expiring.
//
//   Revision 001   By:   gdc      Date: 07/06/95    DR Number:   n/a  
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//=====================================================================

//=====================================================================
//
//      Includes...
//
//=====================================================================

#include "SyncClockTimer.hh"
#include <string.h>
#include "IpcIds.hh"

//@ Usage-Classes
#include "AppContext.hh"
#include "MsgTimer.hh"
#include "TaskControlQueueMsg.hh"
//@ End-Usage

//=====================================================================
//
//      Static Data...
//
//=====================================================================

#ifdef SIGMA_PRODUCTION
const Uint SYNC_CLOCK_TIMER_MS = 60 * 60 * 1000;
#else
const Uint SYNC_CLOCK_TIMER_MS = 5 * 60 * 1000;
#endif

//@ Code...

//=====================================================================
//
//      Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetTimer
//
//@ Interface-Description
//  This method initializes a static SyncClockTimer and returns its
//  reference.  It initializes the static SyncClockTimer on the first
//  pass through this method.  On subsequent calls, it returns a
//  reference to the initialized SyncClockTimer.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
SyncClockTimer&
SyncClockTimer::GetTimer(void)
{
    CALL_TRACE("GetTimer(void)");

    static SyncClockTimer syncClockTimer(SYNC_CLOCK_TIMER_MS);
    return syncClockTimer;
    // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SyncClockTimer [constructor]
//
//@ Interface-Description
//  This private constructor creates a SyncClockTimer with the
//  specified timeout value (in milliseconds).
//---------------------------------------------------------------------
//@ Implementation-Description 
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SyncClockTimer::SyncClockTimer(const Int32 timeout)
    : TaskControlTimer(Sys_Init::ID_STARTUPTIMER, timeout, TASK_CONTROL_Q)
{
    CALL_TRACE("SyncClockTimer(void)");
    // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~SyncClockTimer [destructor]
//
//@ Interface-Description
//  SyncClockTimer private destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SyncClockTimer::~SyncClockTimer()
{
    CALL_TRACE("~SyncClockTimer()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
// Method: nameOf
//
// Interface-Description
//  Returns the name of the class.
//---------------------------------------------------------------------
// Implementation-Description
//  Returns a pointer to the const string containing the class name.
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================
 
const char*
SyncClockTimer::nameOf(void) const
{
    CALL_TRACE("nameOf(void)");
    return "SyncClockTimer";
    // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: dispatch
//
//@ Interface-Description
//  The method dispatches this SyncClockTimer to the callback
//  function registered with the specified AppContext.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls the AppContext dispatch method with a reference to this
//  SyncClockTimer.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
void
SyncClockTimer::dispatch(const AppContext& context) const
{
    CALL_TRACE("dispatch(void)");
    context.dispatch(*this);
    // $[TI1]
}

 

#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//
//@ Interface-Description
//   This method is called when a software fault is detected by the
//   fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//   'lineNumber' are essential pieces of information.  The 'pFileName'
//   and 'pPredicate' strings may be defaulted in the macro to reduce
//   code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This method receives the call for the SoftFault, adds it sub-system
//   and class name ID and sends the message to the non-member function
//   SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
void
SyncClockTimer::SoftFault(const SoftFaultID  softFaultID,
                            const Uint32       lineNumber,
                            const char*        pFileName,
                            const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
 
  FaultHandler::SoftFault(softFaultID, SYS_INIT,
                          Sys_Init::ID_SYNCCLOCKTIMER, 
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)

//=====================================================================
//
//      Protected Methods...
//
//=====================================================================

//=====================================================================
//
//      Private Methods...
//
//=====================================================================
