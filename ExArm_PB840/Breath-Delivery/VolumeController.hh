#ifndef VolumeController_HH
#define VolumeController_HH
//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: VolumeController - Implements an inspiratory flow controller
//		for volume based breaths.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/VolumeController.hhv   25.0.4.0   19 Nov 2013 14:00:16   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005  By: syw     Date:  09-Mar-2000    DR Number: 5684
//  Project:  NeoMode
//  Description:
//		Added PsolSide to constructor arguments.
//		Added Boolean argument to updatePsol().
//
//  Revision: 004  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 003 By: syw    Date: 26-Feb-1996   DR Number: DCS 600
//  	Project:  Sigma (R8027)
//		Description:
//			Controls Rev 04 changes:
//			- eliminated isControllerActive since the controller will
//			  handle based on desiredFlow.
//
//  Revision: 002 By: syw    Date: 21-Feb-1996   DR Number: DCS 674
//  	Project:  Sigma (R8027)
//		Description:
//			Removed #ifndef around #include.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"

#include "FlowController.hh"

//@ Usage-Classes
//@ End-Usage

//@ Constant: VOLUME_CYCLE_TIME_MS
// cycle time for volume main control loop
extern const Int32 VOLUME_CYCLE_TIME_MS ;      // millisecond

//@ Constant: MAX_INSP_CYCLES
// maximum number of volume controller cycles during inspiration.  One additional
// cycles is needed to store the response of the very last inspiratory cycle when
// the inspiratory time is a maximum.
//(Int32) (MAX_INSP_TIME / VOLUME_CYCLE_TIME_MS) + 1 ;   
static const Int32 MAX_INSP_CYCLES = 401 ;

class VolumeController: public FlowController
{
  public:
    enum ControllerTypes {FF_PLUS_I, INIT_SUMMERS_FF_PLUS_I, FF_PLUS_SUMMERS} ;
			  
    VolumeController( Psol& psol, const FlowSensor& flowSensor, const PsolLiftoff::PsolSide side) ;
    virtual ~VolumeController( void) ;
 
    static void SoftFault(const SoftFaultID softFaultID,
 const Uint32      lineNumber,
 const char*       pFileName  = NULL, 
 const char*       pPredicate = NULL) ;

    virtual void newBreath( void) ;
    virtual void updatePsol( const Real32 desiredFlow, const Boolean usePsolLookup = FALSE) ;
    void updateLastErrorSum( const Real32 desiredFlow) ;
    inline static void SetControllerType( const ControllerTypes controllerType) ;
    inline static ControllerTypes GetControllerType( void) ;
    inline static void SetBreathLikenessStatus( const Boolean likenessStatus) ;
    inline static Boolean GetBreathLikenessStatus( void) ;
    void updateErrorBand( const Real32 desiredPeakFlow) ;
	inline void setPeakDesiredFlow( const Real32 peakDesiredFlow) ;
	
  protected:

  private:

    VolumeController(const VolumeController&) ; // Declared but not implemented
    void operator=(const VolumeController&) ;   // Declared but not implemented

    VolumeController( void) ;          			// Declared but not implemented

    void determineErrorSumGain_( void) ;

    //@ Data-Member: ControllerType_
    // volume controller types
    //	1) Feedforward + integral
    //	2) Feedforward + integral, store summers
    //	3) Feedforward + integral + summers, integral disabled until breaths are not
    //	alike 
    static ControllerTypes ControllerType_ ;

	//@ Data-Member: BreathLikenessStatus_
    // set true if two breaths are evaluated to be alike, else false
	static Boolean BreathLikenessStatus_ ;

    //@ Data-Member: intervalNumber_
    //	number of volume control cycles elapsed
    Int32 intervalNumber_ ;

    //@ Data-Member: errorSumGain_
    // errorSum gain in counts/lpm
    Real32 errorSumGain_ ;

    //@ Data-Member: errorGain_
    // gain used in controllerType = FF_PLUS_SUMMERS to enable/disable integration
    // to disable integration: errorGain_ = 0
    // to enable integration:  errorGain_ = 1
    Real32 errorGain_ ;

    //@ Data-Member: intervalLikenessStatus_
    // set true if the error for a breath interval evaluates to be alike else false	 
    Boolean intervalLikenessStatus_ ;

    //@ Data-Member: maxAbsoluteError_
    // stores the maximum absolute error that has occured.
    Real32 maxAbsoluteError_ ;

    //@ Data-Member: controlIntervalError_[MAX_INSP_CYCLES]
    // Array to store the errors for each control interval
    Real32 controlIntervalError_[MAX_INSP_CYCLES] ;

    //@ Data-Member: errorSum_[MAX_INSP_CYCLES]
    // Array to store the summers of the errors for each control interval
    Real32 errorSum_[MAX_INSP_CYCLES] ;  

    //@ Data-Member: errorBand_
    // tolerable amount of error variation used in evaluating breath likeness in
    // controllerType = FF_PLUS_SUMMERS
    Real32 errorBand_ ;

	//@ Data-Member: peakDesiredFlow_
	// peak desired flow for the current breath
	Real32 peakDesiredFlow_ ;
} ;

// Inlined methods
#include "VolumeController.in"

#endif // VolumeController















