#include "stdafx.h"

#if defined(SIGMA_GUI_CPU)

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: VolatileMemory - Volatile Memory Block.
//---------------------------------------------------------------------
//@ Interface-Description
//  This class contains (as non-static data members) each of the different
//  data items that are to reside in RAM, as copies of their corresponding
//  BD data items.  By containing the items in this class, there is no need
//  to manage the locating of each of the individual data items -- the
//  whole instance is located at once.
//
//  This class provides a static method for retrieving the starting address
//  (in RAM) that this class's one instance is located at.
//---------------------------------------------------------------------
//@ Rationale
//  This class provides a simple means of adding/deleting data items
//  into/from GUI's BD-copy management.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class has an initialization method that runs the constructor on
//  this instance, and therefore each of its data members.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Persistent-Objects/vcssrc/VolatileMemory.ccv   25.0.4.0   19 Nov 2013 14:18:56   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003   By:  sah    Date:  13-Aug-1999    DCS Number: 5327
//  Project:  NeoMode
//  Description:
//	Added GUI development only code for locating static memory in
//      extended memory section, thereby allowing development downloads.
//
//  Revision: 002  By:  sah    Date:  07-Jan-1999    DCS Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' to a non-inlined method.
//	Also, as part of making all 'SoftFault()' methods non-inlined,
//	I'm also making those 'SoftFault()' methods that are NOT called
//	in PRODUCTION mode, DEVELOPMENT-only.
//
//  Revision: 001  By:  sah    Date:  15-Mar-1997    DCS Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//          Initial version (Integration baseline).
//
//=====================================================================

#include "VolatileMemory.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

#if defined(SIGMA_GUI_CPU)  &&  defined(SIGMA_DEVELOPMENT)
   // for development only, locate this module's static data into
   // development unit's extended memory...
#  pragma options -NZstacks
#endif // defined(SIGMA_GUI_CPU)  &&  defined(SIGMA_DEVELOPMENT)


//=====================================================================
//
//  Static Data Definitions...
//
//=====================================================================

// the one instance of 'VolatileMemory' resides in static memory...
static Uint  PInstanceMemory_[(sizeof(VolatileMemory) + 3) / 4];


//=====================================================================
//
//  Static Member Definitions...
//
//=====================================================================

Boolean  VolatileMemory::IsInitialized_ = FALSE;

VolatileMemory *const  VolatileMemory::P_INSTANCE_ =
					(VolatileMemory*)::PInstanceMemory_;


//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  VolatileMemory()  [Default Constructor]
//
//@ Interface-Description
//  Initiate the constructor of all of the data items contained in
//  this memory block.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

VolatileMemory::VolatileMemory(void)
{
  CALL_TRACE("VolatileMemory()");

  VolatileMemory::IsInitialized_ = TRUE;
}  // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ~VolatileMemory()  [Destructor]
//
//@ Interface-Description
//  Destroy this instance.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

VolatileMemory::~VolatileMemory(void)
{
  CALL_TRACE("~VolatileMemory()");
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//            [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//  and 'lineNumber' are essential pieces of information.  The
//  'pFileName' and 'pPredicate' strings may be defaulted in the macro
//  to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//  none 
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

void
VolatileMemory::SoftFault(const SoftFaultID  softFaultID,
			  const Uint32       lineNumber,
			  const char*        pFileName,
			  const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, PERSISTENT_OBJS, VOLATILE_MEMORY,
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)


//=====================================================================
//
//  Private Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  initialize_()  [static]
//
//@ Interface-Description
//  This method is provided for consistency, and is used to initialize
//  the block of volatile memory that is managed by 'NovRamManager' for
//  this instance.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
VolatileMemory::initialize_(void)
{
  CALL_TRACE("initialize_()");
  SAFE_CLASS_PRE_CONDITION((this == VolatileMemory::P_INSTANCE_));

  // run default constructor...
  new (this) VolatileMemory();
}   // $[TI1]


#endif // defined(SIGMA_GUI_CPU)
