//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file SocketServer.h
//----------------------------------------------------------------------------

#ifndef SOCKETSERVER_H
#define SOCKETSERVER_H

//-------------------------------------
// I N C L U D E S
//-------------------------------------
#include <stdlib.h>
#include <sys/types.h>
#include "EpochTimestamp.h"
#include <winsock2.h>

#ifndef ssize_t
#define ssize_t int
#endif

#ifndef socklen_t
#define socklen_t int 
#endif

//-------------------------------------
// C L A S S  D E C L
//-------------------------------------
class SocketServer
{

    public:

    /// @brief SocketServer
    /// 
    /// @param port port number as a charactor array.
    /// @param type type, UDP/TCP
	SocketServer(const char *port, const char *type);


    /// @brief ~SocketServer Destructor
    ~SocketServer();

    /// @brief recv data from socket.
    /// 
    /// @param pData  Pointer to an array where to recv data.
    /// @param length Length of array.
    /// 
    /// @return size of data sent.
    ssize_t recv(char *pData, size_t length);


    /// @brief close
    void  close();

	private:

    SocketServer(); ///Not implimented.

	int m_port;
	int m_socket_id;
	int m_socket_options;
	struct sockaddr_in m_sockaddr;
	struct sockaddr_in m_sockaddr_client;
	bool m_bListening;
};


#endif
