#ifndef DownloadTask_HH
#define DownloadTask_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996-1997, Nellcor/Puritan-Bennett Corporation
//=====================================================================
 
// =========================== M O D U L E   D E S C R I P T I O N ====
//@ Class: DownloadTask - Handles all the download tasks.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Download/vcssrc/DownloadTask.hhv   25.0.4.0   19 Nov 2013 14:02:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date: 15-Jan-99    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001  By:  jhv      Date:  23-Feb-96    DR Number:
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//=====================================================================
 
//@ Usage-Classes
#include "DownloadTaskCons.hh"
#include "DownloadClassIds.hh"
#include "DownloadTypes.hh"
#include "DownloadFault.hh"

//TODO E600 removed
//#include <vrtxil.h>
//#include <vrtxvisi.h>
//@ End-Usage

// was not found in any VRTX header file
typedef void (*EntryPtr)(void);
extern "C" void sys_sc_tcreateu(EntryPtr func, int tid, int pri, int *errp);
extern "C" void sys_insert_hooks(void * c, void * d, void * s);

// structure for sc_tinquiry
struct TaskInfo
{
    TaskId id;
	Int32 priority;
	Int32 status;
};

 
class DownloadTask
{
  public:
    DownloadTask(TaskId tId, TaskMode exMode, unsigned char *pStack,
		TaskPri priority, EntryPtr pTaskFunction);      
	inline unsigned char *getStack(void);
	static void   CreateTasks(void);
	static void   TaskCreateHook(TCB * pCreateeTcb, TCB * pCreatorTcb);
	static void   TaskDeleteHook(TCB * pDeleteeTcb, TCB * pDeletorTcb);
	static void   TaskSwitchHook(TCB * pOldTcb    , TCB * pNewTcb    );
    static inline Boolean IsDownloadTaskId( TaskId taskId);
    static void     SoftFault(const FaultType softFaultID,
                                     const Uint32      lineNumber,
                                     const char*       pFileName  = NULL,
                                     const char*       pPredicate = NULL);
 
  private:
    ~DownloadTask(void);     // declared only
	void createTask_(void);
    DownloadTask(const DownloadTask&);       // declared only
    void operator=(const DownloadTask&);  // declared only
	static void   Enter_(void);
 
    //@ Data-Member:    PTasks_[NUM_TASKS+1]
    //  Array of Task objects for the down load.
    static DownloadTask *PTasks_[NUM_TASKS+1];

    //@ Data-Member: taskId_
    // VRTX task id.
    Int32 taskId_;

    //@ Data-Member: priority_
    // VRTX task priority.
    Int32 priority_;

    //@ Data-Member: exMode_
    //  a VRTX task execution mode, a user or supervisor mode.
    TaskMode exMode_;

    //@ Data-Member: entryPoint_
    // Internal variable that holds a pointer to the function that runs
    // as the task.
    EntryPtr entryPoint_;

    //@ Data-Member: pStack_;
    // Internal variable pointing to the beginning of the 
    // task's stack area
    unsigned char *pStack_;
};
 
// Inlined methods...
#include "DownloadTask.in"

#endif   // DownloadTask_HH
