#ifndef AdcChannels_HH
#define AdcChannels_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
//@ Class: AdcChannels - Controls the A/D Conversion system so that
// sensor information can be retrieved.  Also allows the ADC system
// to be put in different operation modes.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/AdcChannels.hhv   26.0.1.0   04 Feb 2013 10:36:30   pvcs  $
//
//@ Modification-Log
//
//  Revision: 011  By: sah   Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 010 By: syw    Date: 22-Jan-1998   DR Number: DCS 2720
//  	Project:  Sigma (R8027)
//		Description:
//			Swapped EXH_SIDE_PRESSURE_SENSOR and EXH_PRESSURE_FILTERED channels.
//
//  Revision: 009 By: syw    Date: 02-Jan-1997   DR Number: DCS 1651
//  	Project:  Sigma (R8027)
//		Description:
//			Eliminate SoftReset() since rSoftResetPort is eliminated.
//
//  Revision: 008 By: iv     Date: 21-May-1996   DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Made changes to channels per Rev 03 AI board - permanent.
//
//  Revision: 007 By: syw    Date: 10-Apr-1996   DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Changes to channels per Rev 03 AI board.
//
//  Revision: 006 By: syw    Date: 04-Mar-1996   DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Moved enum AdcState to public to eliminate compiler warning.
//
//  Revision: 005 By: syw    Date: 04-Mar-1996   DR Number: DCS 562>
//  	Project:  Sigma (R8027)
//		Description:
//			Added AdcInfo_ to store encoded data along with counts.
//			Added access method GetAdcInfo().
//
//  Revision: 004 By: syw    Date: 01-Mar-1996   DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Removed SetResynchDelay() and GetResynchDelay() since it
//			is no longer supported.  Removed StartAdc() method since it
//			only needs to call Resynch().
//
//  Revision: 003 By: syw    Date: 20-Feb-1996   DR Number: DCS 674
//  	Project:  Sigma (R8027)
//		Description:
//			Removed #ifndef around #include.
//
//  Revision: 002  By:  syw   Date:  05-Dec-1995    DR Number: DCS 562
//      Project:  Sigma (R8027)
//      Description:
//			Changed DACWRAP to SUBMUX_SENSORS.  Changed SYSTEM_BATTERY_MODEL
//			to LOW_VOLTAGE_REFERENCE.  Comment out Repeat() method and REPEAT
//			AdcState.  Added Offset_ data member.	
//
//	Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: INITIAL RELEASE
//      Project:  Sigma (R8027)
//      Description:
//      	Initial version
//
//=====================================================================
#include "Sigma.hh"

//@ Usage-Classes

#include "A2dReadingsStructs.h"
#include "AdcData.h"
#include "DataFilter.h"

//@ End-Usage

static const Uint8 NUM_ADC_READS_PER_CYCLE	= 5;

class AdcChannels {
  public:

    //@ Type: AdcChannelId
    // ID of the analog to digital converter channels
    // The order corresponds to actual channel numbers from 0 to 15 for raw data channels
	// The filtered data channels are added at the end to support the existing 840 implementation
    
    enum AdcChannelId {
		ADC_FIRST_CHANNEL,
		ADC_O2_TEMP=ADC_FIRST_CHANNEL,		//O2_TEMPERATURE_SENSOR,
		ADC_AIR_TEMP,		//AIR_TEMPERATURE_SENSOR,
		ADC_O2_FLOW,		//O2_FLOW_SENSOR,
		ADC_AIR_FLOW,		//AIR_FLOW_SENSOR,
		ADC_EXT_BATTERY,
		ADC_INT_BATTERY,
		ADC_FIO2_SENSOR,	//O2_SENSOR,
		ADC_SIGNAL_TEST,
		ADC_O2_PRESSURE_TRANSDUCER,
		ADC_AIR_PRESSURE_TRANSDUCER,
		ADC_EXP_PRESSURE_TRANSDUCER,
		ADC_INSP_PRESSURE_TRANSDUCER,		//INSP_SIDE_PRESSURE_SENSOR,
		ADC_EXH_DRIVE_PRESSURE_TRANSDUCER,
		ADC_EXH_SUPPLY_PRESSURE_TRANSDUCER,
		ADC_VOLTAGE_2P5V,
		ADC_VOLTAGE_3P16V,
		NUM_ADC_CHANNELS_RAW,

		ADC_INSP_PRESSURE_FILTERED=NUM_ADC_CHANNELS_RAW,
		ADC_EXP_PRESSURE_FILTERED,
		ADC_AIR_FLOW_FILTERED,
		ADC_O2_FLOW_FILTERED,
        
		NUM_ADC_CHANNELS_RAW_AND_FILTERED,
		NUM_ADC_CHANNELS_FILTERED = NUM_ADC_CHANNELS_RAW_AND_FILTERED-NUM_ADC_CHANNELS_RAW,
    } ;

	AdcChannels() ;
	~AdcChannels() ;

   static AdcCounts GetNewSample( const AdcChannelId id) ;
   static inline AdcCounts GetBdCycleSample( const AdcChannelId id) ;
   static void NewCycle( void) ;
   static void SetRawAdcData(AdcData_t &adcData);
   static void Initialize();

   static void SoftFault(const SoftFaultID softFaultID,
					  const Uint32      lineNumber,
					  const char*       pFileName  = NULL, 
					  const char*       pPredicate = NULL) ;

  protected:

  private:
    // these methods are purposely declared, but not implemented...
    AdcChannels(const AdcChannels&) ;         // not implemented...
    void   operator=(const AdcChannels&) ;    // not implemented...

	static AdcCounts GetCycleAvg(const AdcChannels::AdcChannelId id);
 
    //@ Data-Member: Channel_[NUM_ADC_CHANNELS]
    // The last group of ADC values read from the ADC system.
    static AdcCounts Channel_[NUM_ADC_CHANNELS_RAW_AND_FILTERED] ;

	static AdcData_t	rawDataBuf_[NUM_ADC_READS_PER_CYCLE];
	static Int8			rawDataBufIndex_;
	static Uint32		numRawDataSamples_;
	static DataFilter_t<AdcCounts>	filteredDataBuf_[NUM_ADC_CHANNELS_RAW];

	static bool initialized_;

} ;

// Inlined methods
#include "AdcChannels.in"

#endif // AdcChannels_HH
