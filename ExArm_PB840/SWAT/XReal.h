//-------------------------------------------------------------------------------
//                        Copyright (c) 2010 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//-------------------------------------------------------------------------------
/// @file XReal.h
///
/// @brief
/// Class definition for XReal and YReal templates

#ifndef XREAL_H
#define XREAL_H

#include "VirtualSensor.h"
#include "FaultHandlerMacros.hh"

namespace swat
{
    enum SwatClassID
    {
        YREAL = 0,
        YRAW  = 1,
    };

    class XRealBase:public VirtualSensorBuffer
    {
    };


    class XRawBase:public VirtualRawSensorBuffer
    {
    };

    /// @class YReal
    ///
    /// @brief Template class YReal.
    class YReal: private XRealBase
    {
        public:

            YReal(float &m, int iSensorId);
            ~YReal();

            void update();

        private:

            SOFT_FAULT_DEFN();

            int m_iSensorId;
    };

    /// @class YRaw
    ///
    /// @brief Template class YRaw.
    class YRaw: private XRawBase
    {
        public:

            YRaw(int &m, int iSensorId);
            ~YRaw();

            void update();

        private:

            SOFT_FAULT_DEFN();

            int m_iSensorId;
    };
}

#endif
