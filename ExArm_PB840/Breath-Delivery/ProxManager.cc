#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2010, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: ProxManager - Manage data to/from Prox 
//---------------------------------------------------------------------
//@ Interface-Description
// // This class is responsible for managaing the interface to the  
// Prox system low level layers.
//---------------------------------------------------------------------
//@ Rationale
// This class is responsible to calc volumes and determine faults  
// with the prox Subsystem 
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ Fault-Handling
// n/a.
//---------------------------------------------------------------------
//@ Restrictions
// 
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/ProxManager.ccv   25.0.4.0   19 Nov 2013 14:00:06   pvcs  $  
//
//@ Modification-Log
// 
//  Revision: 021   By: erm    Date: 28-Mar-2011   SCR Number: 6610 
//  Project:  PROX
//  Description:
//      Changed Purge Error mask from 0x1E to 0x3E to cover 
//      the pump disabled error bit
// 
//  Revision: 021   By: rhj    Date: 16-Feb-2011   SCR Number: 6745 
//  Project:  PROX
//  Description:
//      Changed prox's maxChargeTime from 800ms to 2 secs.
//
//  Revision: 020   By: gdc    Date: 21-Jan-2011   SCR Number: 6733 
//  Project:  PROX
//  Description:
//      Included VCO2 study code for development reference.
//
//  Revision: 019   By: rhj    Date: 19-Jan-2011   SCR Number: 6619 
//  Project:  PROX
//  Description:
//     Added getstartupFailure() method. 
//
//  Revision: 018   By: rhj    Date: 13-Jan-2010   SCR Number: 6730
//  Project:  PROX
//  Description:
//     Modified newCycle() to use StartUpMessageSendOut in order
//     to send the prox ready message correctly.
// 
//  Revision: 017   By: rhj    Date: 22-Dec-2010   SCR Number: 6631
//  Project:  PROX
//  Description:
//     Added isProxStartup_ flag to the isAvailable() method and
//     numBreaths to delay the startup message.
//  
//  Revision: 016   By: rhj   Date: 14-Dec-2010    SCR Number: 6702
//  Project:  PROX
//  Description:
//     Fixed the PROX_READY event message when prox is disabled.
// 
//  Revision: 015   By: rpr    Date:  14-Dec-2010    SCR Number: 6604
//  Project:  PROX
//  Description:
//      Make largest inhalation to be 1 BPM + 30 second manuever. 
//
//  Revision: 014   By: erm   Date: 22-Oct-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//     Added start-up time out.
// 
//  Revision: 013   By: rhj   Date: 05-Oct-2010    SCR Number: 6627
//  Project:  PROX
//  Description:
//     Changed the pressure alarms for prox by comparing the 
//     maximum pressures of both 840 and prox during a whole breath.
//     Also the check shall only be executed during the start 
//     of inhalation.
// 
//  Revision: 012   By: erm   Date: 14-Sep-2010    SCR Number: 6677
//  Project:  PROX
//  Description:
//     Added Prox correction factor
// 
//  Revision: 011   By: rhj   Date: 10-Sept-2010   SCR Number: 6627
//  Project:  PROX
//  Description:
//     Added  getMaxInspProxPressure(), getMaxInspWyePressure(),
//            maxInspWyePressure,_maxInspProxPressure,
//           _maxInspProxPressTemp and_ maxInspWyePressureTemp.
// 
//  Revision: 010   By: erm   Date: 9-Sep-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//     Added lab gas condition when using development key
// 
//  Revision: 009   By: mnr   Date: 14-Jun-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//     SRS tracing number added.
//  
//  Revision: 008   By: erm   Date: 10-May-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//     added test to check for missng I/E cable 
//     
//
//  Revision: 007   By: erm   Date: 10-May-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//     change accumlator test to only dump when > 2.0 cmh20, also log any 
//     Reservoir errors
//
//  Revision: 006   By: mnr    Date: 10-May-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//  	If PROX is not installed, update NOVRAM config info.
//
//  Revision: 005   By: mnr    Date: 22-Apr-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//  	Declare fault if Serial Number does not match last SST value.
//
//  Revision: 004   By: mnr    Date: 13-Apr-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//  	Declare fault if revision string does not match last SST value.
//
//  Revision: 003   By: rpr    Date: 05-Apr-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//  	Update Airway gas condition with targetO2 instead of O2 setting.
// 
//  Revision: 002   By: mnr    Date: 24-Mar-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//  	Serial Number and Firmware rev related updates.
//
//  Revision: 001   By: erm    Date: 1-Feb-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//  Initial revision
//=====================================================================
#include "ProxManager.hh"
#include "ProxiInterface.hh"
#include "BreathPhaseType.hh"
#include "ExhalationPhase.hh"
#include "PeepController.hh"
#include "MiscSensorRefs.hh"
#include "MainSensorRefs.hh"
#include "PressureSensor.hh"
#include "ExhFlowSensor.hh"
#include "PhaseRefs.hh"
#include "Sensor.hh"
#include "PhasedInContextHandle.hh"
#include "BreathRecord.hh"
#include "ValveRefs.hh"
#include "SettingConstants.hh"
#include "HumidTypeValue.hh"
#include "ProxEnabledValue.hh"
#include "PatientCctTypeValue.hh"
#include "BdAlarms.hh"
#include "VentAndUserEventStatus.hh"
#include "EventData.hh"
#include "TaskControlAgent.hh"
#include "FlowController.hh"
#include "BreathMiscRefs.hh"
#include "ControllersRefs.hh"
#include "MathUtilities.hh"
#include "SoftwareOptions.hh"
#include "AveragedBreathData.hh"
#include "O2Mixture.hh"
#include "NovRamManager.hh"
#include "SchedulerId.hh"
#include "BreathPhaseScheduler.hh"
#include "SoftwareOptions.hh"
#include "Barometer.hh"

//@ Code...
extern ProxiInterface* PProxiInterface;

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ProxManager
//
//@ Interface-Description
//Construct a default ProxManager
//---------------------------------------------------------------------
//@ Implementation-Description
// //
//---------------------------------------------------------------------
//@ PreCondition
// None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================
const Uint32 PROX_TRANSMIT_DELAY = 12;
ProxManager::ProxManager()  
{

    expiredVolume_ =  0.0;
    inspiredVolume_ = 0.0;
    qualifiedExhVolume_ = 0.0;
    qualifiedInspVolume_ = 0.0;

    rawFlow_ = 0.0;
    rawVolume_ = 0.0;
    rawPressure_ = 0.0;

    delayTarget_ = PROX_TRANSMIT_DELAY;                       

    myPurge_.mode = ProxiInterface::PROX_MANUAL_MODE;
    myPurge_.maxWaitTime = 10.0;


    myPurge_.resTargetPressure = 3.0;
    myPurge_.resMaxPressure =  myPurge_.resTargetPressure + 2.0F;
    myPurge_.cycleRepCount = 4;
    myPurge_.releaseValveOpenTime = 0.075f;
    myPurge_.releaseValveCloseTime = 0.125f;
    myPurge_.intervalTime = 180;
    myPurge_.maxChargeTime = 2.0f;

    netVolume_ = 0.0;

    prevNetFlow1_ = 0.0;
    prevNetFlow2_ = 0.0;
    peakPrevNetFlow1_ = 0.0;

    isActiveFault_ = FALSE;

    retPoint_ = 0;
    index_ = 0;

    isAvailable_ = FALSE;
    isExhFlowFinished_ = FALSE;
    isValidInspVolume_  = TRUE;
    isValidExhVolume_  = TRUE;

    distalPressure_ = 0.0;

    inspiredVolumeFactor_ = 1.0;
    expiredVolumeFactor_ = 1.0;

    resetFaultCounter_ = TRUE;

    enableProxSubPress_ = FALSE;
    waveformDataStallCounter_ = 0;
    isWaveformReady_ = FALSE;

    currentBreathPhase_ = BreathPhaseType::INSPIRATION;
    prevPhase_ =  BreathPhaseType::INSPIRATION;
    previousFaultPhase_ = BreathPhaseType::INSPIRATION;
    clearDelayedValues_();

    maxWyePressureBreath_ = 0.0f;
    maxProxPressureBreath_ = 0.0f;
    maxProxPressBreathTemp_ = 0.0f;
    maxWyePressureBreathTemp_ = 0.0f;

    startupFailure_ = FALSE;
    isProxStartupComplete_ = FALSE;

    startUpMessageSendOut_ = FALSE;
    sendOneProxInstalledMessage_ = TRUE;
    sendAllParameters_ = TRUE;
    checkStartUpTime_ = TRUE;
    previousPhase_ = FALSE;
    doAccumCheck_ = TRUE;



    revChecked_ = FALSE;
    serialNumChecked_ = FALSE;
    isVersionFailure_ = FALSE;
    whichVersionFailure_ = 0;

    resultPress_ = 0;

    checkTimer_.now();
    startTimeCheck_ = TRUE;

    numBreaths_ = 0;

    //fault Counter
    negVolumeCounter_ = 0;
    pressureFaultCount_ = 0;
    overAccumPressureFaultHit_ = 0;
    reservoirFaultHit_ = 0;
    currentOverPressureCount_ = 0;
    inphaseCounter_ = 0;

    //fault logging
    isSensorErrorOccured_ =  FALSE; 
    isPurgeErrorOccured_  =  FALSE; 
    isBoardErrorOccured_  =  FALSE; 
    isVolumeErrorOccured_ =  FALSE; 
    isPressureErrorOccured_ =  FALSE; 
    isAccumulatorErrorOccured_ =  FALSE; 
    isVersionErrorOccured_   =  FALSE; 
    isStalledDataErrorOccured_=  FALSE; 


    isFirstTime_ = TRUE;
    updatedOnce_ = FALSE;


    //volume  vars
    prevInspiredVolume_ = 0.0;
    endInspiredFlag_ = FALSE;
    endExhFlag_ = FALSE;
    netVolumeFactor_ = 1.0;


    gasStarted_ = FALSE;
    prevHumidType_ = HumidTypeValue::HME_HUMIDIFIER;
    prevFio2_ = 0;



}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~ProxManager
//
//@ Interface-Description
//	Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
// None
//---------------------------------------------------------------------
//@ PreCondition
// None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================
ProxManager::~ProxManager() 
{
    //do nothing 
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setDelay
//
//@ Interface-Description
// return void, accepts Uint32 for the new delay.
//---------------------------------------------------------------------
//@ Implementation-Description
// set the delay of control cycles, the delay is 840 vs Prox
//---------------------------------------------------------------------
//@ PreCondition
// None
//---------------------------------------------------------------------
//@ PostCondition
//  None 
//@ End-Method
//=====================================================================
void ProxManager::setDelay(Uint32 delay)
{
    delayTarget_ = delay;


}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newCycle
//
//@ Interface-Description
// return void, accepts void.
//---------------------------------------------------------------------
//@ Implementation-Description
// To be run on the 840 5 ms cycle. If  data is ready to be collected 
// get Pressure and Flow Data. Each cycle check alarm conditions.
// ProxManager also has the responisablilty to allow low level Prox system
// to acquire readings.
//---------------------------------------------------------------------
//@ PreCondition
//None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================
void ProxManager::newCycle(void)
{
    if (!PProxiInterface ||
        (TaskControlAgent::GetBdState() != STATE_ONLINE))
    {
        return;
    }


    ProxiInterface& rProxiInterface = *PProxiInterface;


    Boolean isProxInstalled = rProxiInterface.isProxBoardInstalled();

    // Send a PROX_INSTALL message to gui so that it shall
    // show or not show prox related screens.
    if (sendOneProxInstalledMessage_)
    {
        if (isProxInstalled)
        {
            VentAndUserEventStatus::PostEventStatus (EventData::PROX_INSTALLED, EventData::ACTIVE);
        }
        else
        {
            //If PROX is not installed, update NOVRAM config info.
            NovRamManager::UpdateLastSstProxFirmwareRev( RProxManager.getFirmwareRev() );
            NovRamManager::UpdateLastSstProxSerialNum( RProxManager.getBoardSerialNumber() );
            VentAndUserEventStatus::PostEventStatus (EventData::PROX_INSTALLED, EventData::CANCEL);
        }
        sendOneProxInstalledMessage_ = FALSE;
    }


    // Check if the Prox board is physically installed.
    // If it doesn't exist, return.
    if (!isProxInstalled)
    {
        return;
    }

    Boolean currentlyAvailable  =  checkAvailabity_();
    if (isAvailable_ != currentlyAvailable)   //check for a change
    {
        if (currentlyAvailable)    //restart proxiInterface
        {
            if (!startUpMessageSendOut_)
            {
                VentAndUserEventStatus::PostEventStatus (EventData::PROX_READY, EventData::CANCEL);
            }
            rProxiInterface.resume();
        }
        else
        {
            VentAndUserEventStatus::PostEventStatus (EventData::PROX_READY, EventData::ACTIVE);
            isProxStartupComplete_ = TRUE;
            rProxiInterface.suspend();  
            resetFaultCounter_ = TRUE;
            isWaveformReady_ = FALSE;
        }
    }
    isAvailable_ = currentlyAvailable;

    if (isAvailable_)
    {
        if (rProxiInterface.isProxSuspended())
        {
            rProxiInterface.resume();
        }


        rProxiInterface.newCycle();  

        updateDelayedQues_();

        if (rProxiInterface.isDataFresh())
        {
            isWaveformReady_ = TRUE;
            waveformDataStallCounter_ = 0;
            rawFlow_ = rProxiInterface.getFlow();
            rawPressure_ = rProxiInterface.getPressure();

            //find transition
            Boolean isStartOfInsp  = FALSE; 
            Boolean transitionHappened = FALSE;

            if (prevPhase_ != currentBreathPhase_)
            {
                prevPhase_ = currentBreathPhase_;
                transitionHappened = TRUE;
            }

            if (transitionHappened && (currentBreathPhase_ == BreathPhaseType::INSPIRATION))
            {
                isStartOfInsp = TRUE;
                if (!startUpMessageSendOut_)
                {
                    numBreaths_++;
                }
            }


            if (isStartOfInsp)
            {
                maxProxPressBreathTemp_ = 0.0f;
                maxWyePressureBreathTemp_ = 0.0f;

            }
            else
            {
                maxProxPressBreathTemp_ = MAX_VALUE(rawPressure_, maxProxPressBreathTemp_);
                maxProxPressureBreath_ = maxProxPressBreathTemp_;

                maxWyePressureBreathTemp_ = MAX_VALUE(BreathRecord::GetWyePressEst(), maxWyePressureBreathTemp_);
                maxWyePressureBreath_ = maxWyePressureBreathTemp_;
            }

            calculateVolume_();
        }

        if (isProxAtFault_())
        {
            // TURN ON ALARM
            if (!isActiveFault_)
            {
                if (!startUpMessageSendOut_)
                {
                    VentAndUserEventStatus::PostEventStatus (EventData::PROX_READY, EventData::ACTIVE);
                    startUpMessageSendOut_ = TRUE;
                    isProxStartupComplete_ = TRUE;
                }
                BdAlarms::PostBdAlarm(BdAlarmId::BDALARM_NON_FUNCTIONAL_PROX, FALSE);
                VentAndUserEventStatus::PostEventStatus (EventData::PROX_FAULT, EventData::ACTIVE);
                isActiveFault_ = TRUE;
            }

        }
        else
        {
            if (isActiveFault_)
            {
                //TURN OFF ALARM
                isActiveFault_ = FALSE;
                BdAlarms::PostBdAlarm(BdAlarmId::BDALARM_FUNCTIONAL_PROX, FALSE);
                VentAndUserEventStatus::PostEventStatus (EventData::PROX_FAULT, EventData::CANCEL);
            }
        }


        if (rProxiInterface.isProxReady())
        {
            // Delay the prox ready event message by NumBreaths
            // in order to calculate accurate prox volumes.
            if (!startUpMessageSendOut_ && numBreaths_ > 1)
            {
                startUpMessageSendOut_ = TRUE;
                startupFailure_ = FALSE;
                VentAndUserEventStatus::PostEventStatus (EventData::PROX_READY, EventData::ACTIVE);
                isProxStartupComplete_ = TRUE;
            }
            setProxGasConditions_();
            if (updatePurgeParams_() || sendAllParameters_)
            {
                rProxiInterface.updatePurgeParams(myPurge_,sendAllParameters_);
                sendAllParameters_ = FALSE;
            }
        }
        else
        {

            if (startTimeCheck_)
            {
                checkTimer_.now();
                startTimeCheck_ = FALSE;
            }
            static const Uint32 MAX_PROX_START_UP_TIME = (1000 * 120);    // millseconds * numberOfSeconds = seconds
            if ((checkTimer_.getPassedTime() > MAX_PROX_START_UP_TIME) && checkStartUpTime_)
            {
                checkStartUpTime_ = FALSE;
                startupFailure_ = TRUE;

                VentAndUserEventStatus::PostEventStatus (EventData::PROX_READY, EventData::ACTIVE);
                startUpMessageSendOut_ = TRUE;
                isProxStartupComplete_ = TRUE;

            }

        }
    }

    if (isActiveFault_ && !isAvailable_)
    {
        //TURN OFF ALARM
        isActiveFault_ = FALSE;
        BdAlarms::PostBdAlarm(BdAlarmId::BDALARM_FUNCTIONAL_PROX, FALSE);
        VentAndUserEventStatus::PostEventStatus (EventData::PROX_FAULT, EventData::CANCEL);
    }

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getBoardSerialNumber
//
//@ Interface-Description
// returns Uint32 for the Serial number which RESIDES on the Prox Board.
//---------------------------------------------------------------------
//@ Implementation-Description
//  surrogate for proxiInterface
//---------------------------------------------------------------------
//@ PreCondition
// None
//---------------------------------------------------------------------
//@ PostCondition
//None
//@ End-Method
//=====================================================================
Uint32 ProxManager::getBoardSerialNumber(void)
{
	Uint32 serialNumber = 0;

	if( PProxiInterface )
	{
      ProxiInterface& rProxiInterface = *PProxiInterface;
	  serialNumber = rProxiInterface.getBoardSerialNumber();
	}
    
    return(serialNumber);

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: checkAvailabity_
//
//@ Interface-Description
// Returen a Boolean, accpets void
//---------------------------------------------------------------------
//@ Implementation-Description
// checks to see if the settings are okay to enable prox.
//---------------------------------------------------------------------
//@ PreCondition
// None
//---------------------------------------------------------------------
//@ PostCondition
// None
//@ End-Method
//=====================================================================
Boolean ProxManager::checkAvailabity_(void)
{

    Boolean available = FALSE;

    const DiscreteValue  PROX_ENABLED_VALUE =
        PhasedInContextHandle::GetDiscreteValue(SettingId::PROX_ENABLED);  

    const DiscreteValue  CIRCUIT_TYPE =
        PhasedInContextHandle::GetDiscreteValue(SettingId::PATIENT_CCT_TYPE);

    // Get the current vent type.
    VentTypeValue::VentTypeValueId ventType_ ;
    ventType_ = (VentTypeValue::VentTypeValueId)PhasedInContextHandle::GetDiscreteValue(
                                                                                       SettingId::VENT_TYPE) ;

    SchedulerId::SchedulerIdValue schedulerId = BreathPhaseScheduler::GetCurrentScheduler().getId();

    if (PROX_ENABLED_VALUE == ProxEnabledValue::PROX_ENABLED  &&
        CIRCUIT_TYPE == PatientCctTypeValue::NEONATAL_CIRCUIT &&
        ventType_ != VentTypeValue::NIV_VENT_TYPE &&
        schedulerId != SchedulerId::SAFETY_PCV &&
        schedulerId != SchedulerId::STANDBY &&
        schedulerId != SchedulerId::POWER_UP)
    {
        available = TRUE;
    }
#if defined(VCO2_STUDY)

    available = PROX_ENABLED_VALUE == ProxEnabledValue::PROX_ENABLED &&
                ventType_ != VentTypeValue::NIV_VENT_TYPE &&
                schedulerId != SchedulerId::SAFETY_PCV &&
                schedulerId != SchedulerId::STANDBY &&
                schedulerId != SchedulerId::POWER_UP;

#endif

    return(available);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isAvailable
//
//@ Interface-Description
// return Boolean, accepts void
//---------------------------------------------------------------------
//@ Implementation-Description
// To ensure the prox system readings are valid during use.
//---------------------------------------------------------------------
//@ PreCondition
//
//---------------------------------------------------------------------
//@ PostCondition
//
//@ End-Method
//=====================================================================
Boolean ProxManager::isAvailable(void)
{
    return(isProxStartupComplete_ && isAvailable_ && !isActiveFault_ );
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isProxAtFault_
//
//@ Interface-Description
//  Return Boolean, accpets void
//---------------------------------------------------------------------
//@ Implementation-Description
//  If any of the contain checks are active then Prox substem data and 
// reading are in question. Reset criteria is when the condition clears
// it self.
//---------------------------------------------------------------------
//@ PreCondition
// None
//---------------------------------------------------------------------
//@ PostCondition
// None
//@ End-Method
//=====================================================================
Boolean ProxManager::isProxAtFault_(void)
{
    Boolean fault = FALSE;
    Uint8 faultData = 0;
    SensorFault faultId ;

    ProxiInterface& rProxiInterface = *PProxiInterface;

    //Reset alarms if in none breathing mode
    if (BreathPhaseScheduler::GetCurrentScheduler().getId() > SchedulerId::LAST_BREATHING_SCHEDULER 
        || !isWaveformReady_  ||  rProxiInterface.isProxSuspended())
    {
        resetFaultCounter_ = TRUE;
    }

#if defined(VCO2_STUDY)
    Mercury::SensorType sensorType = rProxiInterface.getSensorType();
    if (sensorType == Mercury::UNPLUGGED_SENSOR)
    {
        faultData = sensorType;
        faultId = SENSOR_ERROR;
        fault = TRUE;
    }
#else
    Mercury::SensorType sensorType = rProxiInterface.getSensorType();
    if (sensorType != Mercury::NEONATAL_FLOW_SENSOR)
    {
        faultData = (Uint8)sensorType;
        faultId = SENSOR_ERROR;
        fault = TRUE;
    }
#endif

    if (startupFailure_ == TRUE)
    {
        fault = TRUE;
        faultId = DATA_ERROR;
        faultData = START_UP_TOO_LONG; 
    }



    //reset fault counter is requested
    if (resetFaultCounter_)
    {
        negVolumeCounter_ = 0;
        pressureFaultCount_ = 0;
        overAccumPressureFaultHit_ = 0;
        reservoirFaultHit_ = 0;
        currentOverPressureCount_ = 0;
        inphaseCounter_ = 0;
        waveformDataStallCounter_ = 0;
        resetFaultCounter_ = FALSE;
    }

    if (!revChecked_ && rProxiInterface.isRevStrReady())
    {
        revChecked_ = TRUE;
        BigSerialNumber fromNVRAM;
        NovRamManager::GetLastSstProxFirmwareRev(fromNVRAM);

        revStrFromBoard_ = getFirmwareRev();

        if (revStrFromBoard_ != fromNVRAM)
        {
            //[PX00411] revision does not match, set fault condition
            isVersionFailure_ = TRUE;
            whichVersionFailure_ = REVISION_ERROR;

        }
    }

    if (!serialNumChecked_ && rProxiInterface.isSerialNumReady())
    {
        serialNumChecked_ = TRUE;
        Uint32 fromNVRAM, serialNumFromBoard_;
        NovRamManager::GetLastSstProxSerialNum(fromNVRAM);

        serialNumFromBoard_ = rProxiInterface.getBoardSerialNumber();

        if (serialNumFromBoard_ != fromNVRAM)
        {
            //[PX00411] Serial number does not match, set fault condition
            isVersionFailure_ = TRUE;
            whichVersionFailure_ = SERIAL_ERROR;
        }
    }

    if (isVersionFailure_)
    {
        fault = TRUE;
        faultId = VERSION_ERROR;
        faultData = whichVersionFailure_;
    }

    //check for I/E connectivity
    // maximum inhalation can be 1 bpm + 30 secs for a manuever
    static const  Uint32 MAX_IN_PHASE = (60 + 30) * 1000 / CYCLE_TIME_MS;  // ms


    Boolean currentIE = rProxiInterface.getFromProxIE();
    if (previousPhase_ == currentIE)
    {
        inphaseCounter_++;
    }
    else
    {
        inphaseCounter_ = 0;
        previousPhase_ = currentIE;
    }

    if (inphaseCounter_ > MAX_IN_PHASE)
    {
        fault = TRUE;
        faultId = DATA_ERROR;
        faultData = IE_CABLE_MISSING; 
    }

    if (rProxiInterface.isPurgeError())
    {
        faultData  = rProxiInterface.getPurgeErrorByte();
        static const Uint32 MAX_OVERPRESSURE_COUNT = 4000;  // 20 SECOND ->(1000ms * 20seconds) / 5ms 
        static const Uint8 OVER_PRESSURE_MASK = 0x1;
		static const Uint8 PURGE_ERROR_MASK = 0x3E;

        if (faultData & OVER_PRESSURE_MASK)
        {
            if (currentOverPressureCount_++ > MAX_OVERPRESSURE_COUNT)
            {
                rProxiInterface.dumpAccumalator();
                currentOverPressureCount_ = 0;
            }
        }

        if (faultData & PURGE_ERROR_MASK) //everything else that is valid
        {
            faultId = PURGE_ERROR;
            fault = TRUE;
        }
    }

    Uint8 fStatus = (Uint8)rProxiInterface.getFlowStatus();
// E600 BDIO 
#if 0
    if (fStatus !=  ProxiInterface::FLOW_PURGE_IN_PROGRESS &&
        fStatus !=  ProxiInterface::FLOW_ZERO_IN_PROGRESS &&
        fStatus !=  ProxiInterface::FLOW_STATUS_NO_ERROR &&
        fStatus !=  ProxiInterface::UNDEFINED  &&
        !fault)
    {
        faultId = BOARD_ERROR;
        faultData  = fStatus;
        fault = TRUE;
    }
#endif



//find transition
    Boolean isNewInspCompleted  = FALSE; 
    Boolean transitionHappened = FALSE;
    Boolean isStartOfInsp  = FALSE; 

    if (previousFaultPhase_ != currentBreathPhase_)
    {
        previousFaultPhase_ = currentBreathPhase_;
        transitionHappened = TRUE;
    }

    if (transitionHappened && (currentBreathPhase_ != BreathPhaseType::INSPIRATION))
    {
        isNewInspCompleted = TRUE;
    }
    else if (transitionHappened && currentBreathPhase_ == BreathPhaseType::INSPIRATION)
    {
        isStartOfInsp = TRUE;
    }


//Data update 
    static const Uint MAX_MISSED_WAVEFORM_DATA =  (30 * 1000) / 5 ;   //30 Seconds no data

    if (waveformDataStallCounter_++  >  MAX_MISSED_WAVEFORM_DATA)
    {
        fault = TRUE;
        faultData = NO_PROX_DATA ;
        faultId = DATA_ERROR;
    }

    //Data of range
    static const Uint MAX_OUT_OF_RANGE = 3;
    Uint32 flowDataOOR = rProxiInterface.getFlowOOR();
    Uint32 pressureOOR = rProxiInterface.getPressureOOR();

    if (flowDataOOR > MAX_OUT_OF_RANGE)
    {
        fault = TRUE;
        faultId = DATA_ERROR;
        faultData = FLOW_DATA_OUT_OF_RANGE;
    }

    if (pressureOOR > MAX_OUT_OF_RANGE)
    {
        fault = TRUE;
        faultId = DATA_ERROR;
        faultData = PRESSURE_DATA_OUT_OF_RANGE;
    }


// Negative Volume 
    static const Uint MAX_NEG_VOLUME_ALLOWED = 3;
    Real32 resultVolume = getInspiredVolume();

    if (isNewInspCompleted)
    {
        if (resultVolume < 0.0)
        {
            if (!proxManeuverStatus())
            {
                negVolumeCounter_++;
            }

        }
        else
        {
            negVolumeCounter_ = 0;
        }
    }

    if (negVolumeCounter_ >= MAX_NEG_VOLUME_ALLOWED)
    {
        fault = TRUE;
        Real32 tempVolume = ABS_VALUE(resultVolume);
        faultData = (Uint8)(tempVolume); 
        faultId = VOLUME_ERROR;
    }

//Pressure Readings
    static const Real32 MAX_PRESS_DELTA  = 5.0F;
    static const Uint MAX_PRESS_FAULT_COUNT = 3;
    static const Real32 MAX_PLOT_PRESS_DELTA = 2.0f;
    static const Real32 MAX_PROX_VTI = 50.0f;


    if (isStartOfInsp)
    {
        if (( distalPressure_ - rawPressure_  > MAX_PLOT_PRESS_DELTA ) &&
            ( resultVolume > MAX_PROX_VTI))
        {
            enableProxSubPress_ = TRUE;
        }
        else
        {
            enableProxSubPress_ = FALSE;
        }

        resultPress_ = maxProxPressureBreath_ - maxWyePressureBreath_ ;

        if ((resultVolume < MAX_PROX_VTI) || resultPress_ > 0)
        {

            if (ABS_VALUE(resultPress_)  > MAX_PRESS_DELTA)
            {
                if (!proxManeuverStatus())
                {
                    pressureFaultCount_++ ;
                }
            }
            else
            {
                pressureFaultCount_ = 0;
            }

        }
        else
        {
            if ((resultPress_ < (-1 * MAX_VALUE(MAX_PRESS_DELTA, 0.3f * distalPressure_ ))))
            {
                if (!proxManeuverStatus())
                {
                    pressureFaultCount_++ ;
                }
            }
            else
            {
                pressureFaultCount_ = 0;                          
            }
        }

    }
    if (pressureFaultCount_ >= MAX_PRESS_FAULT_COUNT)
    {
        fault = TRUE;
        Real32 tempPressure = ABS_VALUE(resultPress_);
        faultData = (Uint8) tempPressure; 
        faultId = PRESSURE_ERROR;
    }


    // ensure accum Pressure settles below 2cmH20
    // and report  resevoir errors

    static const Uint8 MAX_ACC_VIOLATION = 5;   // check for with control Spec
    static const Real32 MAX_ACC_PRESSURE = 2.0F;

    if (!rProxiInterface.isPurgeActive() && doAccumCheck_)
    {

        if (rProxiInterface.getResevoirPressure() > MAX_ACC_PRESSURE)
        {
            overAccumPressureFaultHit_++;   
        }
        else
        {
            overAccumPressureFaultHit_ = 0;
        }

        if (rProxiInterface.getReservoirError())
        {
            reservoirFaultHit_++;
        }
        else
        {
            reservoirFaultHit_ = 0;
        }

        doAccumCheck_ = FALSE;
    }

    if (rProxiInterface.isPurgeActive())
    {
        doAccumCheck_  = TRUE;   
    }

    if ((overAccumPressureFaultHit_ > MAX_ACC_VIOLATION))
    {
        overAccumPressureFaultHit_ = 0;
        rProxiInterface.dumpAccumalator();
    }

    if (reservoirFaultHit_ > MAX_ACC_VIOLATION)
    {
        fault  = TRUE;
        faultId = ACCUMULATOR_ERROR;
        faultData =  rProxiInterface.getReservoirError();
    }

    Boolean logData = FALSE;

    if (fault)
    {

        switch (faultId)
        {
        case SENSOR_ERROR:
            if (!isSensorErrorOccured_)
            {
                isSensorErrorOccured_ = TRUE;
                logData = TRUE;
            }
            break;
        case PURGE_ERROR:
            if (!isPurgeErrorOccured_)
            {
                isPurgeErrorOccured_ = TRUE;
                logData = TRUE;
            }
            break;
        case BOARD_ERROR:
            if (!isBoardErrorOccured_)
            {
                isBoardErrorOccured_ = TRUE;
                logData = TRUE;
            }
            break;
        case VOLUME_ERROR:
            if (!isVolumeErrorOccured_)
            {
                isVolumeErrorOccured_ = TRUE;
                logData = TRUE;
            }
            break;
        case PRESSURE_ERROR:
            if (!isPressureErrorOccured_)
            {
                isPressureErrorOccured_ = TRUE;
                logData = TRUE;
            }
            break;
        case ACCUMULATOR_ERROR:
            if (!isAccumulatorErrorOccured_)
            {
                isAccumulatorErrorOccured_ = TRUE;
                logData = TRUE;
            }
            break;
        case VERSION_ERROR:
            if (!isVersionErrorOccured_)
            {
                isVersionErrorOccured_ = TRUE;
                logData = TRUE;
            }
            break;
        case DATA_ERROR:
            if (!isStalledDataErrorOccured_)
            {
                isStalledDataErrorOccured_ = TRUE;
                logData = TRUE;
            }
            break;

        }

    }


    if (logData)
    {

        Uint16 errorCode = (Uint16) faultId;
        errorCode = errorCode << 8;
        errorCode += faultData;
        Background::LogDiagnosticCodeUtil(::BK_PROX_ERROR_INFO,errorCode );

    }
    return(fault);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getFirmwareRev(void)
//
//@ Interface-Description
// Returns BigSerialNumber, accepts void
//---------------------------------------------------------------------
//@ Implementation-Description
// surrogate for ProxiInterface
//---------------------------------------------------------------------
//@ PreCondition
// None
//---------------------------------------------------------------------
//@ PostCondition
// None   
//@ End-Method
//=====================================================================
BigSerialNumber ProxManager::getFirmwareRev(void)
{

    if (isFirstTime_ && PProxiInterface )
    {
        ProxiInterface& rProxiInterface = *PProxiInterface;
        rProxiInterface.copyMainRevStr(revStrFromBoard_);
        isFirstTime_ = FALSE;
    }

    return revStrFromBoard_;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateDelayedQues_(void)
//
//@ Interface-Description
// accepts and returns void
//---------------------------------------------------------------------
//@ Implementation-Description
// Update ques with current 840 data.
//---------------------------------------------------------------------
//@ PreCondition
// None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================
void ProxManager::updateDelayedQues_(void)
{

    updateDelayedValues_( BreathRecord::GetPhaseType() );

    currentBreathPhase_ = getDelayedValue_();

}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getBreathPhase
//
//@ Interface-Description
// returns Uint32 the current Breath phase from the delayed que.
//---------------------------------------------------------------------
//@ Implementation-Description
// getter of the current BreathBrease
//---------------------------------------------------------------------
//@ PreCondition
// None
//---------------------------------------------------------------------
//@ PostCondition
// None
//@ End-Method
//=====================================================================
BreathPhaseType::PhaseType ProxManager::getBreathPhase(void)
{
    return(currentBreathPhase_);

}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getInspiredVolume
//
//@ Interface-Description
//Returns Real32 inspired volume.
//---------------------------------------------------------------------
//@ Implementation-Description
// returns the current Inspired Volume.
//---------------------------------------------------------------------
//@ PreCondition
// None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================
Real32 ProxManager::getInspiredVolume(void)
{
    return(qualifiedInspVolume_ );
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getExpiredVolume
//
//@ Interface-Description
//  Returns Real32 for volume , accepts noone.
//---------------------------------------------------------------------
//@ Implementation-Description
// Returns the current expired Volume
//---------------------------------------------------------------------
//@ PreCondition
// None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================
Real32 ProxManager::getExpiredVolume(void)
{
    return(qualifiedExhVolume_);
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getRawFlow
//
//@ Interface-Description
// return Real32 of current flow reading  
//---------------------------------------------------------------------
//@ Implementation-Description
// return current flow
//---------------------------------------------------------------------
//@ PreCondition
// None
//---------------------------------------------------------------------
//@ PostCondition
// None  
//@ End-Method
//=====================================================================
Real32 ProxManager::getFlow(void)
{

    return(rawFlow_);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getRawPressure
//
//@ Interface-Description
//Return Real32 of Pressure, requires nothing 
//---------------------------------------------------------------------
//@ Implementation-Description
// return  pressure
//---------------------------------------------------------------------
//@ PreCondition
// None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================
Real32 ProxManager::getPressure(void)
{
    return(rawPressure_);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getVolume
//
//@ Interface-Description
// return Real32 to represent netVolume. accepts void
//---------------------------------------------------------------------
//@ Implementation-Description
// returns Net Volume.
//---------------------------------------------------------------------
//@ PreCondition
// None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================
Real32 ProxManager::getVolume(void)
{
    return(netVolume_);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getstartupFailure
//
//@ Interface-Description
//   This method takes no parameters, and returns the prox startup 
//   status.
//---------------------------------------------------------------------
//@ Implementation-Description
//   The value of the data member startupFailure_ is returned.
//---------------------------------------------------------------------
//@ PreCondition
// None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================
Boolean ProxManager::getstartupFailure(void)
{
    return(startupFailure_);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getCurrentDelay
//
//@ Interface-Description
//  Return Uint32 the current delay
//---------------------------------------------------------------------
//@ Implementation-Description
// return the current delay.
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================
Uint32 ProxManager::getCurrentDelay(void)
{
    return(delayTarget_);
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: calculateVolume_
//
//@ Interface-Description
// return void, accepts void
//---------------------------------------------------------------------
//@ Implementation-Description
//  calcualte inspired volume ,expired volume and net volume. Inspired
//  and Expired Volume must be clear of any Prox autozero and purges manuevurs.
// volume factors are set during gas conditions updates.
//---------------------------------------------------------------------
//@ PreCondition
//None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================
void ProxManager::calculateVolume_(void)
{
    static const Real32 VolumeFactor = 16.67;
    static const Real32 MAX_SENSOR_OFFSET = 3.0;
    static const Real32 ProxAcqCycle = .010;  //for now hold constant


    if (currentBreathPhase_ == BreathPhaseType::INSPIRATION)
    {
        if (!endExhFlag_)
        {
            endExhFlag_ = TRUE;
            if (isValidExhVolume_)
            {
                qualifiedExhVolume_ = expiredVolume_ * expiredVolumeFactor_;

            }
            isValidExhVolume_ = TRUE;

            Real32 DeltaTV = prevInspiredVolume_ - expiredVolume_;

            if (DeltaTV < -(MAX_SENSOR_OFFSET))
            {
                DeltaTV = -(MAX_SENSOR_OFFSET);
            }
            else if (DeltaTV > MAX_SENSOR_OFFSET)
            {
                DeltaTV = MAX_SENSOR_OFFSET;
            }

            if( IsEquivalent(expiredVolume_, 0.0, THOUSANDTHS) )
            {
                netVolumeFactor_ = 1.0;
            }
            else
            {
                netVolumeFactor_ = (DeltaTV/expiredVolume_) + 1.0F;
            }


        }

        expiredVolume_ = 0.0;
        inspiredVolume_ += rawFlow_ * VolumeFactor *  ProxAcqCycle;
        netVolume_ = inspiredVolume_;
        endInspiredFlag_ = FALSE;
        if (proxManeuverStatus())
        {
            isValidInspVolume_ = FALSE;
        }
    }
    else
    {
        if (proxManeuverStatus())
        {
            isValidExhVolume_ = FALSE;
        }

        if (!endInspiredFlag_)
        {
            prevInspiredVolume_ = inspiredVolume_;
            endInspiredFlag_ = TRUE;
            prevNetFlow1_ = rawFlow_;
            prevNetFlow2_ = prevNetFlow1_;
            peakPrevNetFlow1_ = 0.0;
            isExhFlowFinished_ = FALSE;
            spiroTerminateCount_ = 0 ;
            exhTime_ = 0 ;
            if (isValidInspVolume_)
            {
                qualifiedInspVolume_ = inspiredVolume_ * inspiredVolumeFactor_;

            }
            isValidInspVolume_ = TRUE;
        }
        exhTime_ += 10 ;   //cycle time in MS use const
        inspiredVolume_ = 0.0;
        checkActiveExhalationCompete_();
        endExhFlag_ = FALSE;
        if (!isExhFlowFinished_)
        {
            expiredVolume_ +=  -(rawFlow_) * VolumeFactor *  ProxAcqCycle;
            netVolume_ = prevInspiredVolume_ - ( expiredVolume_ * netVolumeFactor_ ); 

        }
        else
        {
            netVolume_  = netVolume_ * 0.99F;


        }
    }

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:setProxGasConditions_
//
//@ Interface-Description
//requires nothing, returns nothing
//---------------------------------------------------------------------
//@ Implementation-Description
// When a gas condition changes is dectected inform ProxiInterface.
//---------------------------------------------------------------------
//@ PreCondition
// None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================
void ProxManager::setProxGasConditions_(void)
{

    static const Real32 HME_FACTOR_TEST = ((273.0f + 37.0f) / (273.0f + 22.0f));
    static const Real32 HME_FACTOR = ((273.0f + 37.0f) / (273.0f + 35.0f));

    Boolean changed = FALSE;
    DiscreteValue humidType = 
        PhasedInContextHandle::GetDiscreteValue(SettingId::HUMID_TYPE);
    Real32 atmPressure = RAtmosphericPressureSensor.getValue() ;

    gasConditions_.fio2 = (Uint8) RO2Mixture.getTargetO2Percent();


    if (!gasStarted_)
    {
        prevHumidType_ = humidType;
        prevFio2_ = gasConditions_.fio2;
        gasStarted_ = TRUE;
        changed = TRUE;
    }


    if (humidType == HumidTypeValue::HME_HUMIDIFIER)
    {
        gasConditions_.inspHumidity = 50;
        gasConditions_.inspGasTemperature = 22;
        gasConditions_.expGasParam = 3;
        inspiredVolumeFactor_ = ((atmPressure - 14.04F) / (atmPressure - 64.68F)) * HME_FACTOR;
        expiredVolumeFactor_  = ((atmPressure - 58.73F) / (atmPressure - 64.68F)) * HME_FACTOR;
    }
    else      //for all others
    {
        gasConditions_.inspHumidity = 100;
        gasConditions_.inspGasTemperature = 37;
        gasConditions_.expGasParam = 0;

        inspiredVolumeFactor_ = 1.0; 
        expiredVolumeFactor_ =  1.0;
    }

    if (humidType != prevHumidType_ ||  prevFio2_ != gasConditions_.fio2)
    {
        changed = TRUE;
        prevHumidType_ = humidType;
        prevFio2_ = gasConditions_.fio2;
    }

    if (SoftwareOptions::GetSoftwareOptions().isStandardDevelopmentFunctions())
    {
        gasConditions_.inspHumidity = 20;
        gasConditions_.inspGasTemperature = 22;
        gasConditions_.expGasParam = 0;
        inspiredVolumeFactor_ =  ((atmPressure - 5.6F) / (atmPressure - 64.68F)) * HME_FACTOR_TEST;
        expiredVolumeFactor_ =   ((atmPressure - 5.6F) / (atmPressure - 64.68F)) * HME_FACTOR_TEST;    
    }

    if (changed)
    {
        ProxiInterface& rProxiInterface = *PProxiInterface;
        rProxiInterface.updateGasConditions(gasConditions_);
    }

    //use factor per SCR 6677
    static const Real32 PROX_CORRECTION_FACTOR =  1.00;
    inspiredVolumeFactor_ = inspiredVolumeFactor_ * PROX_CORRECTION_FACTOR ;
    expiredVolumeFactor_  = expiredVolumeFactor_  * PROX_CORRECTION_FACTOR ;

    ProxiInterface& rProxiInterface = *PProxiInterface;
    rProxiInterface.setInspiredVolumeFactor(inspiredVolumeFactor_);
    rProxiInterface.setExpiredVolumeFactor(expiredVolumeFactor_);
    rProxiInterface.setAtmPressure(atmPressure);
    rProxiInterface.setHmeFactorTest(HME_FACTOR_TEST);
    rProxiInterface.setHmeFactor(HME_FACTOR);
    rProxiInterface.setProxCorrectionFactor(PROX_CORRECTION_FACTOR);

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updatePurgeParams_
//
//@ Interface-Description
// Return Boolean to show a change had happen and in need of update
// requires nothing
//---------------------------------------------------------------------
//@ Implementation-Description
// detect  any required changes in purge parameters
//---------------------------------------------------------------------
//@ PreCondition
// None
//---------------------------------------------------------------------
//@ PostCondition
// None
//@ End-Method
//=====================================================================
Boolean ProxManager::updatePurgeParams_(void)
{
    static const Uint8 HI_FREQ_RATE = 55;
    static const Uint8 LOW_FREQ_RATE = 45;
    static const Uint8 HIGH_PURGE_INTERVAL = 120;
    static const Uint8 LOW_PURGE_INTERVAL = 180;

    static const Uint8 HIGH_REP_RATE = 2;
    static const Uint8 LOW_REP_RATE = 4;

    Boolean isChanged = FALSE;

    int ftot = RAveragedBreathData.getPrevRespiratoryRate();


    if (ftot > HI_FREQ_RATE)
    {

        if (myPurge_.intervalTime != HIGH_PURGE_INTERVAL)
        {
            myPurge_.intervalTime = HIGH_PURGE_INTERVAL;
            isChanged  = TRUE;
        }
        if (myPurge_.cycleRepCount != HIGH_REP_RATE)
        {
            myPurge_.cycleRepCount = HIGH_REP_RATE;
            isChanged  = TRUE;
        }

    }
    else if (ftot <= LOW_FREQ_RATE)
    {
        if (myPurge_.intervalTime != LOW_PURGE_INTERVAL)
        {
            myPurge_.intervalTime = LOW_PURGE_INTERVAL;
            isChanged  = TRUE;
        }
        if (myPurge_.cycleRepCount != LOW_REP_RATE)
        {
            myPurge_.cycleRepCount = LOW_REP_RATE;
            isChanged  = TRUE;
        }
    }



    if (!updatedOnce_)
    {
        isChanged = TRUE;
        updatedOnce_ = TRUE;
    }

    return isChanged ;
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: proxManeuverStatus
//
//@ Interface-Description
// Return boolean to indicate a maneuver is in progress, requires nothing
//---------------------------------------------------------------------
//@ Implementation-Description
// check  if a purge or autozero is in process
//---------------------------------------------------------------------
//@ PreCondition
// None
//---------------------------------------------------------------------
//@ PostCondition
// None
//@ End-Method
//=====================================================================
Uint8 ProxManager::proxManeuverStatus(void)
{
    Uint8 fStatus = 0;
    Uint8 outStatus = 0;
    static const Uint32 PURGE_ACTIVE = 6;
    static const Uint32 ZERO_ACTIVE = 7;
    if (PProxiInterface)
    {
        ProxiInterface& rProxiInterface_ = *PProxiInterface;

        if (rProxiInterface_.isPurgeActive())
        {
            fStatus = PURGE_ACTIVE;    
        }

        if (ProxiInterface::ZERO_IN_PROGRESS == rProxiInterface_.getZeroStatus())
        {
            fStatus = ZERO_ACTIVE;
        }


        if (fStatus == PURGE_ACTIVE || fStatus == ZERO_ACTIVE)
        {
            outStatus = fStatus;
        }

    }

    return(outStatus);
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: RequestManualPurge
//
//@ Interface-Description
// Return the Eventstatus, reuires the EventId and eventStatus
//---------------------------------------------------------------------
//@ Implementation-Description
// notice of request to execute a manual purge. the manual purge with 
// new purge parameters is sent to the Prox Interface.
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
EventData::EventStatus
    ProxManager::RequestManualPurge (const EventData::EventId id,
                                     const Boolean eventStatus)
{
    CALL_TRACE("ProxManager::RequestManualPurge(EventData::EventId id,Boolean eventStatus)");
	UNUSED_SYMBOL(eventStatus);
	UNUSED_SYMBOL(id);

    static const Real32 MANUAL_PURGE_PRESSURE_TARGET = 6.0;  
    static const Real32 MANUAL_MAX_PRESSURE_TARGET = 8.0;


    EventData::EventStatus newStatus =  EventData::COMPLETE;

	if( PProxiInterface )
    {
		ProxiInterface& rProxiInterface_ = *PProxiInterface;
        rProxiInterface_.requestManualPurge(MANUAL_PURGE_PRESSURE_TARGET, MANUAL_MAX_PRESSURE_TARGET); 
	}


    return(newStatus);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: checkActiveExhalationCompete_
//
//@ Interface-Description
// Return nothing and requires nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
// find flow stabliblity during exhalation phase and once achieved
// terminate active exhalation.
//---------------------------------------------------------------------
//@ PreCondition
// None
//---------------------------------------------------------------------
//@ PostCondition
// None
//@ End-Method
//=====================================================================
void ProxManager::checkActiveExhalationCompete_(void)
{ 
    static const Real32 NET_FLOW_ALPHA_FILTER_1 = 0.97F;
    static const Real32 NET_FLOW_ALPHA_FILTER_2 = 0.80F;
    static const Int8 SPIRO_TERMINATE_COUNT = 0;
    static const Real32 FILTERED_FLOW_BOUND = 0.20F;

    prevNetFlow1_ = prevNetFlow1_ * NET_FLOW_ALPHA_FILTER_1 -
                    rawFlow_ * (1.0F - NET_FLOW_ALPHA_FILTER_1);
    prevNetFlow2_ = prevNetFlow2_ * NET_FLOW_ALPHA_FILTER_2 -
                    rawFlow_ * (1.0F - NET_FLOW_ALPHA_FILTER_2);

    peakPrevNetFlow1_ = MAX_VALUE(peakPrevNetFlow1_, prevNetFlow1_);

    Real32 desiredFlow = RAirFlowController.getDesiredFlow() + RO2FlowController.getDesiredFlow();

    if ((ABS_VALUE(prevNetFlow1_ - prevNetFlow2_) < FILTERED_FLOW_BOUND) &&
        (prevNetFlow1_ < 0.2 * peakPrevNetFlow1_) &&
        (RPeepController.isControllerFrozen()) &&
        (exhTime_ >= 200) &&
        IsEquivalent(desiredFlow, RExhalationPhase.getFlowTarget(), THOUSANDTHS) &&
        !isExhFlowFinished_)
    {
        if (++spiroTerminateCount_ >= SPIRO_TERMINATE_COUNT)
        {
            isExhFlowFinished_ = TRUE;
        }
    }
    else
    {

        spiroTerminateCount_ = 0 ;
    }
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  setDistalPresure
//Return nothing, recieves Uint32 for the new pressure reading
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
// update pressure from 840 pressure 
//---------------------------------------------------------------------
//@ PreCondition
// None
//---------------------------------------------------------------------
//@ PostCondition
//    None
//@ End-Method
//=====================================================================
void ProxManager::setDistalPressure(Real32 pressure)
{
    distalPressure_ = pressure;

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: IsProxSubPressEnabled
//
//@ Interface-Description
//  	This method has no arguments and returns whether to use 
//      prox substitute or not.
//---------------------------------------------------------------------
//@ Implementation-Description
//   returns data member enableProxSubPress_.
//---------------------------------------------------------------------
//@ PreCondition
//   None.
//---------------------------------------------------------------------
//@ PostCondition
//   None.
//@ End-Method
//=====================================================================
Boolean ProxManager::IsProxSubPressEnabled(void)
{
    return enableProxSubPress_;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
    ProxManager::SoftFault( const SoftFaultID  softFaultID,
                            const Uint32       lineNumber,
                            const char*        pFileName,
                            const char*        pPredicate)
{
    CALL_TRACE("SoftFault( softFaultID, lineNumber, pFileName, pPredicate)") ;
    FaultHandler::SoftFault( softFaultID, BREATH_DELIVERY, PROXMANAGER,
                             lineNumber, pFileName, pPredicate) ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateDelayedValues_
//
//@ Interface-Description
//     This method takes BreathPhaseType::PhaseType as a argument and returns nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//	insert a new element in to the buffer. 
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
    ProxManager::updateDelayedValues_(BreathPhaseType::PhaseType phase)
{


    buffer_[index_] = phase;

    if (index_ > delayTarget_)
        retPoint_ = index_ - delayTarget_;
    else
    {
        retPoint_ = ( MAX_DELAYED - (delayTarget_ - index_)) % MAX_DELAYED;
    }
    index_++;

    if (index_ > (MAX_DELAYED -1))
    {
        index_ = 0;
    }
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: clearDelayedValues_
//
//@ Interface-Description
//   This method has no arguments and returns nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//  set each element to zero	
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void ProxManager::clearDelayedValues_(void)
{

    for (Int x = 0; x < MAX_DELAYED; x++)
        buffer_[x] = BreathPhaseType::INSPIRATION;  
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getDelayedValue_
//
//@ Interface-Description
//   This method has no arguments and returns a Real32.
//---------------------------------------------------------------------
//@ Implementation-Description
//	return the delayed value.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
BreathPhaseType::PhaseType 
    ProxManager::getDelayedValue_(void)
{

    return( buffer_[retPoint_]);
}
#if 0
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  DoTimedManualPurge
//
//@ Interface-Description
// Requires nothing, send nothing
//---------------------------------------------------------------------
//@ Implementation-Description
// count control cycle up to a determined time and send a dump Accumlator
// 
//---------------------------------------------------------------------
//@ PreCondition
// None
//---------------------------------------------------------------------
//@ PostCondition
//    None
//@ End-Method
//=====================================================================
void ProxManager::DoTimedManualPurge(void)
{
    //testing only
    //count 5ms ticks
    static Uint32 timeCount = 0;
    static const Uint32 TRIG_AMOUNT = (200) * 2 ; // (1000ms / 5ms) * second required
    if (timeCount++ > TRIG_AMOUNT && PProxiInterface )
    {
        ProxiInterface& rProxiInterface = *PProxiInterface;
        rProxiInterface.dumpAccumalator(TRUE);
        timeCount = 0;
    }
    return;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getMaxProxPressureBreath
//
//@ Interface-Description
//  	This method has no arguments and returns the maximum 
//      prox pressure.
//---------------------------------------------------------------------
//@ Implementation-Description
//   returns data member maxProxPressureBreath_.
//---------------------------------------------------------------------
//@ PreCondition
//   None.
//---------------------------------------------------------------------
//@ PostCondition
//   None.
//@ End-Method
//=====================================================================
Real32 ProxManager::getMaxProxPressureBreath(void)
{
    return maxProxPressureBreath_;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getMaxWyePressureBreath
//
//@ Interface-Description
//  	This method has no arguments and returns the maximum 
//      prox pressure.
//---------------------------------------------------------------------
//@ Implementation-Description
//   returns data member maxWyePressureBreath_.
//---------------------------------------------------------------------
//@ PreCondition
//   None.
//---------------------------------------------------------------------
//@ PostCondition
//   None.
//@ End-Method
//=====================================================================
Real32 ProxManager::getMaxWyePressureBreath(void)
{
    return maxWyePressureBreath_;
}


#endif



