#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: ForceVentInopTest - performs the forced vent inop test.
//---------------------------------------------------------------------
//@ Interface-Description
//	This class performs the Service Mode force vent inop test, which
//	tests each of the 5 methods of forcing a vent inop:
//		1. GUI vent inop
//		2. force vent inop register A
//		3. force vent inop register B
//		4. 10-second POST timer A
//		5. 10-second POST timer B
//	This test requires powering down and back up into Service Mode
//	between each test phase.  The ServiceModeNovramData class is used
//	to store the test phase ID and test status in novram.
//
//	NOTE: this test is called the "Force Safe State Test" in the SRS and
//	the 840 Ventilator Self-Tests Specification, and called the "Vent
//	Inop Test" on the GUI display.
//---------------------------------------------------------------------
//@ Rationale
//	This class encapsulates the methods required to perform the force
//	vent inop test.
//---------------------------------------------------------------------
//@ Implementation-Description
//	$[09167]  $[09168]  $[09191]
//	The method initialize() is performed during system initialization
//	to initialize the test phase ID and test status from novram.
//	The method runTest() performs the force vent inop test.
//	The method DisablePostTimers() is used by the TaskMonitor to
//	disable one or both of the 10-second POST timers depending on the
//	current test phase.
//---------------------------------------------------------------------
//@ Fault-Handling
//	n/a
//---------------------------------------------------------------------
//@ Restrictions
//	Only one instance of this class may be created.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/ForceVentInopTest.ccv   25.0.4.0   19 Nov 2013 14:23:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003   By: gdc    Date: 26-Jan-2009    SCR Number: 6407
//  Project:  840S
//  Description:
//      Modified to bring source into strict ARM compliance.
//
//  Revision: 002  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 001  By:  syw    Date:  05-Apr-96    DR Number: 1861, 1865, 2262, 2525
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//=====================================================================

#include "ForceVentInopTest.hh"

//@ Usage-Classes

#if defined (SIGMA_GUI_CPU)

// ***** GUI includes *****
#include "OsUtil.hh"

#ifdef SIGMA_UNIT_TEST
# define SetVentInop FakeSetVentInop
#endif  // SIGMA_UNIT_TEST

#else

// ***** BD includes *****
#include "SmRefs.hh"
#include "SmConstants.hh"
#include "SmPromptId.hh"
#include "SmManager.hh"
#include "ServiceModeNovramData.hh"

#include "ValveRefs.hh"
#include "MiscSensorRefs.hh"
#include "RegisterRefs.hh"
#include "VentStatus.hh"
#include "Psol.hh"
#include "LinearSensor.hh"
#include "Solenoid.hh"

#include "Task.hh"
#include "TaskControlAgent.hh"

#ifdef SIGMA_UNIT_TEST
# include "FakeVentStatus.hh"
# include "FakeTask.hh"
# include "FakeIo.hh"
#endif  // SIGMA_UNIT_TEST

#endif // defined (SIGMA_GUI_CPU)

#ifdef SIGMA_UNIT_TEST
# include "ForceVentInopTest_UT.hh"
#endif  // SIGMA_UNIT_TEST

#ifdef SIGMA_DEVELOPMENT
# include <stdio.h>
#endif  // SIGMA_DEVELOPMENT

#if defined(SIGMA_DEBUG) || defined (VENT_INOP_TEST)
# include <stdio.h>
# include <stdlib.h>
# ifdef VENT_INOP_TEST
#   include <ctype.h>
# endif  // VENT_INOP_TEST
#endif // defined(SIGMA_DEBUG) || defined (VENT_INOP_TEST)

//@ End-Usage


//@ Code...

#if defined (SIGMA_BD_CPU)

// error condition bit flags
const Uint32 ACTUATOR_DRIVE_FAILURE 	= 1 ;
const Uint32 AUDIO_FAILURE 				= 2 ;
const Uint32 LED_FAILURE 				= 4 ;

// from AI Spec 70550-45
const Real32 PSOL_LOOPBACK_OFFSET = 10.0;  // mV
const Real32 PSOL_LOOPBACK_AMPS_TO_VOLTS = 10.0;  // V/A
const Real32 EV_LOOPBACK_AMPS_TO_VOLTS = 1.0;  // V/A
const Real32 SV_LOOPBACK_OFFSET = 200.0;  // mV
const Real32 SV_LOOPBACK_AMPS_TO_VOLTS = 10.0;  // V/A
const Real32 ADC_OFFSET = 19.5;  // mV

#endif // defined (SIGMA_BD_CPU)

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ForceVentInopTest()  [Default Constructor]
//
//@ Interface-Description
//	Constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//	For BD only, initialize private data members.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

ForceVentInopTest::ForceVentInopTest( void)
{
// $[TI1]
	CALL_TRACE("ForceVentInopTest::ForceVentInopTest( void)") ;

#if defined (SIGMA_BD_CPU)

	phaseId_ = START ;
	testPassed_ = TRUE;

#if defined(VENT_INOP_TEST)
	printf("**** ForceVentInopTest constructed *****\n");
#endif // defined(VENT_INOP_TEST)

#endif // defined (SIGMA_BD_CPU)

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~ForceVentInopTest()  [Destructor]
//
//@ Interface-Description
//	Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

ForceVentInopTest::~ForceVentInopTest( void)
{
	CALL_TRACE("ForceVentInopTest::~ForceVentInopTest( void)") ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: initialize
//
//@ Interface-Description
//	Initializes vent inop test variables during system initialization.
//	No argument, no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Initialize phaseId_ and testPassed_ from novram.  Set the
//	ServiceMode-class vent inop in-progress flag based on the phaseId_.
//---------------------------------------------------------------------
//@ PreCondition
//	phaseId_ must be one of the legal ID's defined in enum PhaseId.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

#if defined (SIGMA_BD_CPU)

void
ForceVentInopTest::initialize( void)
{
	CALL_TRACE("ForceVentInopTest::initialize( void)") ;

	phaseId_ = (PhaseId) ServiceModeNovramData::GetVentInopTestPhaseId();
	testPassed_ = ServiceModeNovramData::IsVentInopTestPassed();

	CLASS_PRE_CONDITION(	phaseId_ == START ||
							phaseId_ == VENT_INOP_A ||
							phaseId_ == VENT_INOP_B ||
							phaseId_ == TEN_SECOND_TIMER_A ||
							phaseId_ == TEN_SECOND_TIMER_B );

#ifdef SIGMA_DEBUG
	printf( "initialize() phaseId_ = %d\n", phaseId_) ;
	printf( "initialize() testPassed_ = %d\n", testPassed_);
#endif// SIGMA_DEBUG

	if (phaseId_ == VENT_INOP_A || phaseId_ == VENT_INOP_B
		|| phaseId_ == TEN_SECOND_TIMER_A
		|| phaseId_ == TEN_SECOND_TIMER_B)
	{
	// $[TI1]
		ServiceMode::SetVentInopTestInProgress( TRUE) ;
	}
	else
	{
	// $[TI2]
		ServiceMode::SetVentInopTestInProgress( FALSE) ;
	}
	// end else
}

#endif // defined (SIGMA_BD_CPU)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: runTest
//
//@ Interface-Description
//	Perform the appropriate phase of the vent inop test.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Perform the appropriate phase of the vent inop test based on phaseId_,
//	generating error conditions as required.
//	On completion of test phase for the BD only, save the next phase ID
//	and the outcome of this test phase in novram, then force operator to
//	power down the system and restart Service Mode by prompting to do so
//	and then entering an infinite loop (to prevent further system actions).
//
//	NOTE: the GUI vent inop test phase consists of two separate invocations
//	of runTest(): one on the GUI and one on the BD (phaseId_ = GUI_VENT_INOP).
//	The GUI invocation causes the GUI to invoke a vent inop condition,
//	and the BD invocation tests that the GUI-invoked vent inop took effect.
//---------------------------------------------------------------------
//@ PreCondition
//	phaseId_ must be one of the legal ID's defined in enum PhaseId.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
ForceVentInopTest::runTest( void)
{
	CALL_TRACE("ForceVentInopTest::runTest( void)") ;

#if defined (SIGMA_GUI_CPU)
// $[TI7]
// ***** GUI-only *****
	SetVentInop() ;

#else
// ***** BD-only *****
	CLASS_PRE_CONDITION(	phaseId_ == START ||
							phaseId_ == VENT_INOP_A ||
							phaseId_ == VENT_INOP_B ||
							phaseId_ == TEN_SECOND_TIMER_A ||
							phaseId_ == TEN_SECOND_TIMER_B );

	// assume test will pass, prove otherwise
	Uint32 error = 0 ;
	testPassed_ = TRUE;

#ifdef SIGMA_DEBUG
	printf( "runTest() phaseId_ = %d\n", phaseId_) ;
#endif // SIGMA_DEBUG

	// display a momentary prompt message indicating the current
	// test phase
	displayCurrentPhase_() ;

#ifdef VENT_INOP_TEST
	Boolean simulateFailure = FALSE;
	char answer[20];
	if (phaseId_ != GUI_VENT_INOP)
	{
		printf("Simulate vent inop drive failure? ");
		scanf("%s", answer);
		if (toupper(answer[0]) == 'Y') simulateFailure = TRUE;
	}
#endif // VENT_INOP_TEST

	// perform the appropriate test phase
	switch (phaseId_)
	{
		// NOTE: GUI_VENT_INOP has same value as START
		case GUI_VENT_INOP:
		// $[TI1]
			// allow time to read test phase prompt
			Task::Delay(2);
			error = checkVentInopStates_( TRUE) ;

			if (!error)
			{
			// $[TI1.1]
				phaseId_ = VENT_INOP_A ;
			}
			else
			{
			// $[TI1.2]
				processError_( error) ;
				phaseId_ = START ;
			}
			// end else

			break ;

		case VENT_INOP_A:
		// $[TI2]
#ifdef VENT_INOP_TEST
			if(!simulateFailure)
#endif // VENT_INOP_TEST
			VentStatus::DirectVentStatusService( VentStatus::VENT_INOP_REG_A,
		 										 VentStatus::ACTIVATE) ;
			// allow time to read test phase prompt
			Task::Delay(2);
			error = checkVentInopStates_( FALSE) ;

			if (!error)
			{
			// $[TI2.1]
				phaseId_ = VENT_INOP_B ;
			}
			else
			{
			// $[TI2.2]
				processError_( error) ;
				phaseId_ = START ;
			}
			// end else

			break ;

		case VENT_INOP_B:
		// $[TI3]
#ifdef VENT_INOP_TEST
			if(!simulateFailure)
#endif // VENT_INOP_TEST
			VentStatus::DirectVentStatusService( VentStatus::VENT_INOP_REG_B,
		 										 VentStatus::ACTIVATE) ;
			// allow time to read test phase prompt
			Task::Delay(2);
			error = checkVentInopStates_( FALSE) ;

			if (!error)
			{
			// $[TI3.1]
				phaseId_ = TEN_SECOND_TIMER_A ;
			}
			else
			{
			// $[TI3.2]
				processError_( error) ;
				phaseId_ = START ;
			}
			// end else

			break ;

		case TEN_SECOND_TIMER_A:
		// $[TI4]
#ifdef VENT_INOP_TEST
			if(simulateFailure)
				VentStatus::DirectVentStatusService(
						VentStatus::TEN_SECOND_TIMER_REG_A,
						VentStatus::DEACTIVATE) ;
#endif // VENT_INOP_TEST
			// wait for timer to timeout
			Task::Delay( 13, 500) ;
			error = checkVentInopStates_( FALSE) ;

			if (!error)
			{
			// $[TI4.1]
				phaseId_ = TEN_SECOND_TIMER_B ;
			}
			else
			{
			// $[TI4.2]
				processError_( error) ;
				phaseId_ = START ;
			}
			// end else

			break ;

		case TEN_SECOND_TIMER_B:
		// $[TI5]
#ifdef VENT_INOP_TEST
			if(simulateFailure)
				VentStatus::DirectVentStatusService(
						VentStatus::TEN_SECOND_TIMER_REG_B,
						VentStatus::DEACTIVATE) ;
#endif // VENT_INOP_TEST
			// wait for timer to timeout
			Task::Delay( 13, 500) ;
			error = checkVentInopStates_( FALSE) ;

			if (error)
			{
			// $[TI5.1]
				processError_( error) ;
			}
			// $[TI5.2]
			// implied else

			// test complete
			phaseId_ = START ;

#ifdef SIGMA_DEBUG
			printf( "Test complete\n") ;
#endif // SIGMA_DEBUG

			break ;

		default:
		// $[TI6]
		// illegal phase ID
			CLASS_ASSERTION_FAILURE();
	}
	// end switch

	// store next phase ID and test status in novram
	ServiceModeNovramData::SetVentInopTestPassed( testPassed_);
	ServiceModeNovramData::SetVentInopTestPhaseId( (Int16)phaseId_);

	// prompt to power down then enter infinite loop
	waitForPowerDown_() ;

#endif // defined (SIGMA_GUI_CPU)

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  DisablePostTimers  [static]
//
//@ Interface-Description
//	Disable the POST 10-second timers as required for the current
//	test phase and ventilator state.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Disable timer(s) as follows:
//	- If vent is not in service state, disable both timers.
//	- Else if vent is in service state:
//		-- For 10 sec timer A test phase only, disable timer B only.
//		-- For 10 sec timer B test phase only, disable timer A only.
//		-- For all other test phases, disable both timers.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

#ifdef SIGMA_BD_CPU

void
ForceVentInopTest::DisablePostTimers(void)
{
	CALL_TRACE("ForceVentInopTest::DisablePostTimers( void)") ;

	if (TaskControlAgent::GetMyState() != STATE_SERVICE)
	{
	// $[TI1]
		// turn off both timers if not in service state
		VentStatus::DirectVentStatusService(
						VentStatus::TEN_SECOND_TIMER,
						VentStatus::DEACTIVATE);
	}
	else
	{
	// $[TI2]
		// vent is in service state: determine which timer(s) to deactivate
		// based on vent inop test phase ID
		switch (RForceVentInopTest.phaseId_)
		{
			case TEN_SECOND_TIMER_A:
			// $[TI2.1]
				// disable timer B only to cause timer A vent inop
				VentStatus::DirectVentStatusService(
								VentStatus::TEN_SECOND_TIMER_REG_B,
		 						VentStatus::DEACTIVATE) ;
				break;

			case TEN_SECOND_TIMER_B:
			// $[TI2.2]
				// disable timer A only to cause timer B vent inop
				VentStatus::DirectVentStatusService(
								VentStatus::TEN_SECOND_TIMER_REG_A,
		 						VentStatus::DEACTIVATE) ;
				break;

			case GUI_VENT_INOP:
			case VENT_INOP_A:
			case VENT_INOP_B:
			default:
			// $[TI2.3]
				// turn off both timers
				VentStatus::DirectVentStatusService(
								VentStatus::TEN_SECOND_TIMER,
								VentStatus::DEACTIVATE);
				break;

		}
		// end switch
	}
	// end else
}

#endif  // SIGMA_BD_CPU


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getStatus
//
//@ Interface-Description
//	No arguments, returns the current status of the vent inop test.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Based on the stored test phase and the test passed flag,
//	return the status of the vent inop test.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

#ifdef SIGMA_BD_CPU

ServiceMode::TestStatus
ForceVentInopTest::getStatus( void) const
{
	CALL_TRACE("ForceVentInopTest::getStatus( void)" const) ;

	ServiceMode::TestStatus status;
	if (phaseId_ == START)
	{
	// $[TI1]
		if (!testPassed_)
		{
		// $[TI1.1]
			status = ServiceMode::FAILED;
		}
		else
		{
		// $[TI1.2]
			status = ServiceMode::PASSED;
		}
		// end else
	}
	else
	{
	// $[TI2]
		CLASS_ASSERTION(	phaseId_ == START ||
							phaseId_ == VENT_INOP_A ||
							phaseId_ == VENT_INOP_B ||
							phaseId_ == TEN_SECOND_TIMER_A ||
							phaseId_ == TEN_SECOND_TIMER_B );

		status = ServiceMode::IN_PROGRESS;
	}
	// end else

	return( status);
}

#endif  // SIGMA_BD_CPU


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  reset
//
//@ Interface-Description
//	Reset the test state.
//---------------------------------------------------------------------
//@ Implementation-Description
//	set phaseId_ = START and testPassed_ = TRUE and store in novram.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

#ifdef SIGMA_BD_CPU

void
ForceVentInopTest::reset( void)
{
// $[TI1]
	CALL_TRACE("ForceVentInopTest::reset( void)") ;

	phaseId_ = START ;
	ServiceModeNovramData::SetVentInopTestPhaseId( START);
	testPassed_ = TRUE ;
	ServiceModeNovramData::SetVentInopTestPassed( TRUE);

#ifdef SIGMA_DEBUG
	printf("***** Resetting ForceVentInopTest state *****\n");
#endif  // SIGMA_DEBUG
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
ForceVentInopTest::SoftFault( const SoftFaultID  softFaultID,
                  			   const Uint32 lineNumber,
		   					   const char *pFileName,
		   					   const char *pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)") ;
	FaultHandler::SoftFault( softFaultID, SERVICE_MODE, FORCEVENTINOPTEST,
                          	 lineNumber, pFileName, pPredicate);
}

#endif  // SIGMA_BD_CPU


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//=====================================================================
//
//  Private Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: checkVentInopStates_
//
//@ Interface-Description
//	Check if vent inop state took effect.  Takes Boolean argument indicating
//	if user prompting for error determination is required, returns an
//	error flag.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Initialize error flag to 0.
//	If the current test phase requires user prompting to verify a vent inop
//	conditions (e.g. is bd audio on), display the prompts and if user
//	indicates error(s), update the error flag appropriately.
//	Attempt to activate system valves/solenoids and determine via
//	loopback current if any are being activated -- if so, update the
//	error flag to indicate this error.
//	Return the error flag.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

#if defined (SIGMA_BD_CPU)

Uint32
ForceVentInopTest::checkVentInopStates_( const Boolean userPrompt)
{
	CALL_TRACE("ForceVentInopTest::checkVentInopStates_( const Boolean userPrompt)") ;

	Uint32 error = 0 ;
	Real32 airCurrent ;
	Real32 o2Current ;
// E600 BDIO    Real32 svCurrent ;
	Real32 evCurrent ;

	if (userPrompt)
	{
	// $[TI1]
		error = getUserResponse_() ;
	}
	// $[TI2]
	// implied else

	// calculate the max offset currents
	const Real32 MAX_PSOL_OFFSET_CURRENT =
		(PSOL_LOOPBACK_OFFSET + ADC_OFFSET) / PSOL_LOOPBACK_AMPS_TO_VOLTS;
	// NOTE: EV_LOOPBACK_OFFSET, defined in SmConstants.hh, is already in mA
	const Real32 MAX_EV_OFFSET_CURRENT =
		EV_LOOPBACK_OFFSET + ADC_OFFSET / EV_LOOPBACK_AMPS_TO_VOLTS;	
	   const Real32 MAX_SV_OFFSET_CURRENT =
	      (SV_LOOPBACK_OFFSET + ADC_OFFSET) / SV_LOOPBACK_AMPS_TO_VOLTS; 
	
	const Uint16 DAC_INCREMENT = 50;  // DAC counts

	Uint16 dacCount = 0 ;
	Boolean exit = FALSE ;

	// attempt to command the actuators and check loopback currents
	// to see if any of them actuated
	RSafetyValve.close() ;
	while (dacCount < MAX_COUNT_VALUE && !exit)
	{
	// $[TI3]
		dacCount += DAC_INCREMENT ;
		RAirPsol.updatePsol( dacCount) ;
		RO2Psol.updatePsol( dacCount) ;
// E600 BDIO    	RExhValveDacPort.writeRegister( dacCount) ;

		Task::Delay( 0, 10) ;

		airCurrent = RAirPsolCurrent.getValue() ;
		o2Current = RO2PsolCurrent.getValue() ;
// E600 BDIO        svCurrent = RSafetyValveCurrent.getValue() ;
		evCurrent = RExhMotorCurrent.getValue() ;

		if (ABS_VALUE(airCurrent) > MAX_PSOL_OFFSET_CURRENT ||
			ABS_VALUE(o2Current) > MAX_PSOL_OFFSET_CURRENT ||
// E600 BDIO            ABS_VALUE(svCurrent) > MAX_SV_OFFSET_CURRENT ||
			ABS_VALUE(evCurrent) > MAX_EV_OFFSET_CURRENT )
		{
		// $[TI3.1]
			error |= ACTUATOR_DRIVE_FAILURE ;
			exit = TRUE ;
		}
		// implied else
		// $[TI3.2]
	}
	// end while

#ifdef SIGMA_DEBUG
		const Real32 MAX_SV_OFFSET_CURRENT =
				(SV_LOOPBACK_OFFSET + ADC_OFFSET) / SV_LOOPBACK_AMPS_TO_VOLTS;

		printf( "air, o2, sv, ev     = %8.3f %8.3f %8.3f %8.3f\n",
					airCurrent, o2Current, svCurrent, evCurrent);
		printf( "MAX air, o2, sv, ev = %8.3f %8.3f %8.3f %8.3f\n",
			MAX_PSOL_OFFSET_CURRENT, MAX_PSOL_OFFSET_CURRENT,
			MAX_SV_OFFSET_CURRENT, MAX_EV_OFFSET_CURRENT);
#endif // SIGMA_DEBUG

	return( error) ;
}

#endif // defined (SIGMA_BD_CPU)


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: waitForPowerDown_
//
//@ Interface-Description
//	Prompt user to power down the vent then wait until this happens.
//	No arguments, no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Prompt to power down the vent, then enter infinite loop.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

#if defined (SIGMA_BD_CPU)

void
ForceVentInopTest::waitForPowerDown_( void)
{
	CALL_TRACE("ForceVentInopTest::waitForPowerDown_( void)") ;

	RSmManager.promptOperator (SmPromptId::POWER_DOWN_PROMPT,
		SmPromptId::NO_KEYS) ;

	for (;;)
	{
	// $[TI1]
		Task::Delay( 1) ;

#ifdef SIGMA_UNIT_TEST
		ForceVentInopTest_UT::WaitForPowerDownExecuted = TRUE;
		// need to exit the infinite loop for automated unit testing
		break;
#endif  // SIGMA_UNIT_TEST
	}
	// end for
}

#endif // defined (SIGMA_BD_CPU)


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: displayCurrentPhase_
//
//@ Interface-Description
//	Display the current test phase as a prompt message.  No arguments,
//	no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Display the appropriate prompt message for the current phase ID.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
#if defined (SIGMA_BD_CPU)

void
ForceVentInopTest::displayCurrentPhase_( void)
{
	CALL_TRACE("ForceVentInopTest::displayCurrentPhase_( void)") ;

	switch (phaseId_)
	{
		case GUI_VENT_INOP:
		// $[TI1]
			RSmManager.promptOperator (SmPromptId::GUI_SIDE_TEST_PROMPT,
				SmPromptId::NO_KEYS) ;
			break ;

		case VENT_INOP_A:
		// $[TI2]
			RSmManager.promptOperator (SmPromptId::VENT_INOP_A_TEST_PROMPT,
				SmPromptId::NO_KEYS) ;
			break ;

		case VENT_INOP_B:
		// $[TI3]
			RSmManager.promptOperator (SmPromptId::VENT_INOP_B_TEST_PROMPT,
				SmPromptId::NO_KEYS) ;
			break ;

		case TEN_SECOND_TIMER_A:
		// $[TI4]
			RSmManager.promptOperator (SmPromptId::TEN_SEC_A_TEST_PROMPT,
				SmPromptId::NO_KEYS) ;
			break ;

		case TEN_SECOND_TIMER_B:
		// $[TI5]
			RSmManager.promptOperator (SmPromptId::TEN_SEC_B_TEST_PROMPT,
				SmPromptId::NO_KEYS) ;
			break ;

		default:
		// $[TI6]
		// illegal phase ID
			CLASS_ASSERTION_FAILURE();
	}
	// end switch
}

#endif // defined (SIGMA_BD_CPU)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processError_
//
//@ Interface-Description
//	Generate an error message if any error type occurred. One argument:
//	bit-encoded error word.  No return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Generate the appropriate error from the input error flag.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
#if defined (SIGMA_BD_CPU)

void
ForceVentInopTest::processError_( Uint32 error)
{
	CALL_TRACE("ForceVentInopTest::processError_( error)") ;

	if (error)
	{
	// $[TI1]
		RSmManager.sendStatusToServiceData(
			SmStatusId::TEST_FAILURE, SmStatusId::CONDITION_1) ;

		testPassed_ = FALSE ;
	}
	// $[TI2]
	// implied else

#ifdef SIGMA_DEBUG
	printf( "error       = 0x%x\n", error) ;
	printf( "testPassed_ = %s\n", testPassed_ ? "TRUE" : "FALSE");
#endif // SIGMA_DEBUG

}

#endif // defined (SIGMA_BD_CPU)


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getUserResponse_
//
//@ Interface-Description
//	Prompts for user to verify vent inop conditions that cannot be
//	verified automatically.  No arguments, returns an error flag.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Initialize error flag to 0.
//	Prompt the user to indicate if BD alarm is on and if vent inop and
//	SVO LEDs are lit, update error flag based on user response.
//	Return error flag.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
#if defined (SIGMA_BD_CPU)

Uint32
ForceVentInopTest::getUserResponse_( void)
{
	CALL_TRACE("ForceVentInopTest::getUserResponse_( void)") ;

	Uint32 error = 0 ;

	// Ask the operator if the sound is on
	RSmManager.promptOperator (SmPromptId::SOUND_PROMPT,
		SmPromptId::ACCEPT_AND_CANCEL_ONLY) ;
	SmPromptId::ActionId operatorAction = RSmManager.getOperatorAction() ;

	if (operatorAction == SmPromptId::KEY_CLEAR)
	{
	// $[TI1]
		error |= AUDIO_FAILURE ;
	}
	// $[TI2]
	// implied else

	// Ask the operator if the vent inop and svo leds are lit
	RSmManager.promptOperator (SmPromptId::VENTILATOR_INOPERATIVE_PROMPT,
		SmPromptId::ACCEPT_AND_CANCEL_ONLY) ;
	operatorAction = RSmManager.getOperatorAction() ;

	if (operatorAction == SmPromptId::KEY_CLEAR)
	{
	// $[TI3]
		error |= LED_FAILURE ;
	}
	// $[TI4]
	// implied else

	return( error) ;
}

#endif // defined (SIGMA_BD_CPU)
