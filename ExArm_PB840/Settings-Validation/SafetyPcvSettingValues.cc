#include "stdafx.h"


//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: SafetyPcvSettingValues - Manager of the Safety-PCV Setting Values.
//---------------------------------------------------------------------
//@ Interface-Description
//  This class provides access to the Safety-PCV setting values.  This is
//  a static class, with static methods, which provide read-only access
//  to the Safety-PCV setting values.  This class MUST be initialized
//  at startup-time, whereby, it pulls the values from Flash into this
//  class's static array of values.
//
//  The batch values that are NOT applicable during Safety-PCV (e.g., the VCV
//  settings), are represented by their respective new-patient values.  For
//  example, a query of the value of Plateau Time would produce a bounded
//  value equal to '0.0'.
//---------------------------------------------------------------------
//@ Rationale
//  This class provides a general-purpose class for providing access to
//  the Safety-PCV setting values.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Internally, this class contains a static array of 'SettingValue' types.
//  'SettingValue' is a union of the three different types of batch values:
//  bounded, discrete  and sequential values.  This array is big enough to
//  store all of the batch setting values, where the setting IDs are used
//  as indices into this array.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/SafetyPcvSettingValues.ccv   25.0.4.0   19 Nov 2013 14:27:40   pvcs  $
//
//@ Modification-Log
//
//  Revision: 024   By: rhj   Date:  26-Jan-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//       Added Prox enabled setting.
//        
//  Revision: 023   By: gdc   Date: 02-Mar-2009     SCR Number: 6478
//  Project:  840S
//  Description:
//       Changed to flow triggering for neo circuits in development
//       mode only to allow for full error checking of settings.
//       
//  Revision: 022   By: rhj   Date: 07-July-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//       Leak Compensation project-related changes.
//       
//  Revision: 021   By: rhj   Date:  23-May-2007    SCR Number: 6237 
//  Project:  Trend
//  Description:
//      Added Date International feature.
//
//  Revision: 020   By: gdc   Date:  27-May-2005    SCR Number: 6170 
//  Project:  NIV2
//  Description:
//      Added High Ti spont limit setting for NIV SPONT/SIMV.
//
//  Revision: 019   By: gdc   Date:  22-Feb-2005    SCR Number: 6148 
//  Project:  NIV1
//  Description:
//      Updated comment for low circ pressure setting.
//
//  Revision: 018   By: gdc   Date:  18-Feb-2005    SCR Number: 6144 
//  Project:  NIV1
//  Description:
//      Changes to support NIV.
//
//  Revision: 017 By: srp    Date: 28-May-2002   DR Number: 5898
//  Project:  VCP
//	Description:
//		Incorporated Remediation code review issue(s) -- commentary changes only.
//
//  Revision: 016   By: hlg    Date: 01-Oct-2001    DR Number: 5966
//  Project:  GUIComms
//  Description:
//	Added mapping to SRS print requirements.
//
//  Revision: 015   By: quf    Date: 17-Sep-2001    DR Number: 5493
//  Project:  GUIComms
//  Description:
//	Removed code relating to obsoleted print config setting.
//
//  Revision: 014   By: hlg    Date: 30-AUG-2001    DR Number: 5950
//  Project:  GUIComms
//  Description:
//	Changed variable comp1PortConfigValue to the appropriate variables
//      comp2PortConfigValue, comp3PortConfigValue, and printConfigValue
//      in Method Initialize.
//
//  Revision: 013  By: sah    Date:  11-Nov-2000    DR Number: 5789
//  Project:  VTPC
//  Description:
//      As part of this DCS, fixing testable item discrepencies found
//      during unit test review.
//
//  Revision: 012  By: sah     Date:  05-May-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      *  added new volume support level setting
//      *  deleted apnea minute volume
//      *  fixed testable item labels
//      *  added new Vsupp-to-IBW and Vt-to-IBW ratios
//
//  Revision: 011   By: hct    Date: 03-DEC-1999    DR Number: 5493
//  Project:  GUIComms
//  Description:
//	Incorporated initial specifications for GUIComms Project.
//
//  Revision: 010   By: sah    Date: 20-Jul-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//      NeoMode project-related changes:
//      *  obsoleted patient type setting
//      *  modified values based on requirement changes
//
//  Revision: 009   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new BiLevel-specific settings
//	*  for simpler implementation elsewhere, replaced old type-specific
//	   query methods with new generic query method ('GetValue()')
//
//  Revision: 008   By: sah   Date:  04-Feb-1999    DR Number: 5322
//  Project:  ATC
//  Description:
//	Project-related changes:
//	*  added new ATC-specific settings
//	*  added special handling of humidifier volume value, in which,
//	   when available, its stored value is used, rather than some
//	   hard-coded default value
//
//  Revision: 007   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 006   By: dosman Date: 16-Jan-1998    DCS Number: 
//  Project:  BILEVEL
//  Description:
//	Initial BiLevel version.
//      Added requirement numbers for tracing to SRS. 
//      changed SafetyPCV value for PeepHigh to fit new patient value.
//      accidentally removed precision definition for PEEP_HI & put back
//
//  Revision: 005   By: sah    Date:  28-May-1998    DR Number: 5058
//  Project:  COLOR
//  Description:
//	--- Merged Rev "Color" (1.40.1.0) into Rev "BiLevel" (1.40.2.0)
//	Modified range/resolution for improving the user's ability to
//	accurately change this setting's value.
//
//  Revision: 004   By: sah    Date: 22-Sep-1997    DCS Number: 2410
//  Project: Sigma (R8027)
//  Description:
//	Added default value for the new Atmospheric Pressure Setting.
//	Also, corrected redundant testable item numbers.
//
//  Revision: 003   By: sah    Date: 03-Jul-1997    DCS Number: 2194
//  Project: Sigma (R8027)
//  Description:
//	Change apnea interval's Safety-PCV value from 20 seconds, to
//      10 seconds, thereby eliminating problem with transition from
//      Safety-PCV to (SPONT) normal mode.
//
//  Revision: 002   By: sah    Date: 23 Jan 1997    DR Number: 1223
//  Project: Sigma (R8027)
//  Description:
//	Mismatch in Safety-PCV values for IBW and Patient Type were causing
//	problems in our breath-delivery.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//=====================================================================

#include "SafetyPcvSettingValues.hh"

#include "SettingConstants.hh"
#include "SettingId.hh"
#include "ConstantParmValue.hh"
#include "BaudRateValue.hh"
#include "DciDataBitsValue.hh"
#include "DciParityModeValue.hh"
#include "Fio2EnabledValue.hh"
#include "FlowPatternValue.hh"
#include "HumidTypeValue.hh"
#include "LanguageValue.hh"
#include "MandTypeValue.hh"
#include "ModeValue.hh"
#include "MonthValue.hh"
#include "NominalLineVoltValue.hh"
#include "PatientCctTypeValue.hh"
#include "PressUnitsValue.hh"
#include "SupportTypeValue.hh"
#include "TriggerTypeValue.hh"
#include "TubeTypeValue.hh"
#include "VentTypeValue.hh"
#include "ComPortConfigValue.hh"
#include "DateFormatValue.hh"
#include "ProxEnabledValue.hh"

//@ Usage-Classes
#include "NovRamManager.hh"
#if defined(SIGMA_GUI_CPU)  ||  defined(SIGMA_COMMON)
	#include "BatchSettingValues.hh"
#elif defined(SIGMA_BD_CPU)
	#include "BdSettingValues.hh"
#endif // defined(SIGMA_GUI_CPU)  ||  defined(SIGMA_COMMON)
//@ End-Usage
#include "LeakCompEnabledValue.hh"

//@ Code...

//=====================================================================
//
//  Static Data Initialization...
//
//=====================================================================

//=====================================================================
//  NOTE:  The array is initialized with static memory, this eliminates the
//  running of 'SettingValue' constructors during static initialization.
//  Similarly, the 'BoundedValue' constants used for the initialization of
//  the array are defined as 'Real32'/'Precision' pairs to eliminate the
//  calling of 'BoundedValue' constructors.
//=====================================================================

//=====================================================================
//  Bounded Safety-PCV values...
//=====================================================================

static const Real32     APNEA_INTERVAL_PCV_VALUE_ = 10000.0f;
static const Precision  APNEA_INTERVAL_PCV_PREC_  = THOUSANDS;

// $[02280] -- Safety-PCV value...
static const Real32     DISCO_SENS_PCV_VALUE_ = 75.0f;
static const Precision  DISCO_SENS_PCV_PREC_  = ONES;

// $[02122] -- Safety-PCV value...
static const Real32     FLOW_ACCEL_PERCENT_PCV_VALUE_ = 50.0f;
static const Precision  FLOW_ACCEL_PERCENT_PCV_PREC_  = ONES;

// $[02135] -- Safety-PCV value...
static const Real32     HIGH_CCT_PRESS_PCV_VALUE_ = 20.0f;
static const Precision  HIGH_CCT_PRESS_PCV_PREC_  = ONES;

// $[02159] -- Safety-PCV value...
static const Real32     IBW_PCV_VALUE_ = 7.0f;
static const Precision  IBW_PCV_PREC_  = TENTHS;

// $[02169] -- Safety-PCV value...
static const Real32     INSP_PRESS_PCV_VALUE_ = 10.0f;
static const Precision  INSP_PRESS_PCV_PREC_  = ONES;

// $[02174] -- Safety-PCV value...
static const Real32     INSP_TIME_PCV_VALUE_ = 1000.0f;
static const Precision  INSP_TIME_PCV_PREC_  = TENS;

// $[02210] -- Safety-PCV value...
static const Real32     NEO_O2_PERCENT_PCV_VALUE_   =  40.0f;
static const Real32     ADULT_O2_PERCENT_PCV_VALUE_ = 100.0f;
static const Precision  OXYGEN_PERCENT_PCV_PREC_    = ONES;

// $[02230] -- Safety-PCV value...
static const Real32     PEEP_PCV_VALUE_ = 3.0f;
static const Precision  PEEP_PCV_PREC_  = TENTHS;

// $[02235] -- Safety-PCV value...
static const Real32     PRESS_SENS_PCV_VALUE_ = 2.0f;
static const Precision  PRESS_SENS_PCV_PREC_  = TENTHS;

// $[02244] -- Safety-PCV value...
static const Real32     RESP_RATE_PCV_VALUE_ = 16.0f;
static const Precision  RESP_RATE_PCV_PREC_  = ONES;

#if defined(SIGMA_GUI_CPU)  ||  defined(SIGMA_COMMON)

// $[02184] -- Safety-PCV value...
static const Real32     LOW_EXH_MINUTE_VOL_PCV_VALUE_ = 0.050f;
static const Precision  LOW_EXH_MINUTE_VOL_PCV_PREC_  = THOUSANDTHS;

#endif // defined(SIGMA_GUI_CPU)  ||  defined(SIGMA_COMMON)

//=====================================================================
//  Discrete Safety-PCV values...
//=====================================================================

// $[02276] -- Safety-PCV value...
static const DiscreteValue  FIO2_ENABLED_PCV_VALUE_ =
	Fio2EnabledValue::YES_FIO2_ENABLED;

// $[02192] -- Safety-PCV value...
static const DiscreteValue  MAND_TYPE_PCV_VALUE_ =
	MandTypeValue::PCV_MAND_TYPE;

// $[02202] -- Safety-PCV value...
static const DiscreteValue  MODE_PCV_VALUE_ = ModeValue::AC_MODE_VALUE;



//=====================================================================
//
//  Static Data Initialization...
//
//=====================================================================

static Uint  PValuesBuffer_[((sizeof(SettingValue) + 3) / 4) *
#if defined(SIGMA_GUI_CPU)  ||  defined(SIGMA_COMMON)
							::NUM_BATCH_SETTING_IDS];
#elif defined(SIGMA_BD_CPU)
							::NUM_BD_SETTING_IDS];
#endif // defined(SIGMA_GUI_CPU)  ||  defined(SIGMA_COMMON)


SettingValue*  SafetyPcvSettingValues::ArrSafetyPcvValues_ =
	(SettingValue*)::PValuesBuffer_;


//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  Initialize()  [static]
//
//@ Interface-Description
//  Initialize the table of the Safety-PCV values.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Those setting values that are applicable during Safety-PCV, will be
//  initialized to their Safety-PCV values that are stored in static
//  constants, defined above.
//
//  Some of the alarm settings have a value of "off" during Safety-PCV.
//  This "off" value is defined by 'SettingConstants::UPPER_ALARM_LIMIT_OFF'
//  for the "high" alarms and 'SettingConstants::LOWER_ALARM_LIMIT_OFF' for
//  the "low" alarms.  Since these "off" constants are external constants
//  (defined in 'SettingConstants.cc'), the compiler can't know the value
//  of them at compile time.  Therefore, the alarms that are to be turned
//  "off" are initialized differently than all of the other Safety-PCV
//  values; since a static constant can't be defined in this file containing
//  the "off" values defined elsewhere, non-constant static variables are
//  used within the scope of the "switch" cases for each of these alarm
//  values.
//
// $[GC01001] The Communication Setup subscreen shall allow ...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	SafetyPcvSettingValues::Initialize(void)
{
	CALL_TRACE("Initialize()");

	// some settings are to use their respective persistent values (when
	// present) for Safety-PCV ventilation...

	// are the persistent values initialized?...
	const Boolean  ARE_PERS_VALUES_INITIALIZED =
		NovRamManager::AreBatchSettingsInitialized();

#if defined(SIGMA_GUI_CPU)  ||  defined(SIGMA_COMMON)
	BatchSettingValues  persistentValues;
#elif defined(SIGMA_BD_CPU)
	BdSettingValues  persistentValues;
#endif // defined(SIGMA_GUI_CPU)  ||  defined(SIGMA_COMMON)

	DiscreteValue  patientCctTypePcvValue;

	// $[02215] -- patient circuit type is to use its persistent value when
	//             present, otherwise its default value...
	if ( ARE_PERS_VALUES_INITIALIZED )
	{	// $[TI16] -- persistent settings are available, initialize to them...
		// get the persistent BATCH setting values from NOVRAM...
		NovRamManager::GetAcceptedBatchSettings(persistentValues);

		// get the persistent patient circuit type value...
		patientCctTypePcvValue = persistentValues.getDiscreteValue(
																  SettingId::PATIENT_CCT_TYPE
																  );
	}
	else
	{	// $[TI17] -- NO settings are available; leave uninitialized...
		// $[02214] -- default value...
		patientCctTypePcvValue = PatientCctTypeValue::ADULT_CIRCUIT;
	}

	BoundedValue   boundedValue;
	DiscreteValue  discreteValue = 0;

	Uint  idx;

	//=================================================================
	// Bounded, BD settings...
	//=================================================================

	BoundedValue   humidVolumePcv;

	// $[NE02013] -- humidifier volume is to use its persistent value when
	//               present, otherwise its default value...
	if ( ARE_PERS_VALUES_INITIALIZED )
	{	// $[TI18]
		// get the persistent humidifier volume value...
		humidVolumePcv = persistentValues.getBoundedValue(SettingId::HUMID_VOLUME);
	}
	else
	{	// $[TI19] -- there are no persistent values; use default values...
		// $[TC02003] -- default value...
		// $[TC02004] -- resolution...
		humidVolumePcv.value     = 480.0f;
		humidVolumePcv.precision = TENS;
	}

	for ( idx = SettingId::LOW_BATCH_BD_BOUNDED_ID_VALUE; 
		idx <= SettingId::HIGH_BATCH_BD_BOUNDED_ID_VALUE; idx++ )
	{	// $[TI1] -- this path is ALWAYS taken...
		switch ( idx )
		{
			case SettingId::APNEA_FLOW_ACCEL_PERCENT :			// $[TI1.1]
				// this bounded settings is NOT applicable during Safety-PCV,
				// therefore initialize to its new-patient value -- this is NEVER
				// intended for use, it is for safety...
				boundedValue.value     = 50.0f;
				boundedValue.precision = ONES;
				break;
			case SettingId::APNEA_INSP_PRESS :				// $[TI1.2]
				// this bounded settings is NOT applicable during Safety-PCV,
				// therefore initialize to its new-patient value -- this is NEVER
				// intended for use, it is for safety...
				boundedValue.value     = 15.0f;
				boundedValue.precision = ONES;
				break;
			case SettingId::APNEA_INSP_TIME :				// $[TI1.3]
				// this bounded settings is NOT applicable during Safety-PCV,
				// therefore initialize to its new-patient value -- this is NEVER
				// intended for use, it is for safety...
				boundedValue.value     = 1000.0f;
				boundedValue.precision = TENS;
				break;
			case SettingId::APNEA_INTERVAL :				// $[TI1.4]
				boundedValue.value     = ::APNEA_INTERVAL_PCV_VALUE_;
				boundedValue.precision = ::APNEA_INTERVAL_PCV_PREC_;
				break;
			case SettingId::APNEA_MIN_INSP_FLOW :			// $[TI1.5]
				// this bounded settings is NOT applicable during Safety-PCV,
				// therefore initialize to its new-patient value -- this is NEVER
				// intended for use, it is for safety...
				boundedValue.value     = 2.20f;
				boundedValue.precision = HUNDREDTHS;
				break;
			case SettingId::APNEA_O2_PERCENT :				// $[TI1.6]
				// this bounded settings is NOT applicable during Safety-PCV,
				// therefore initialize to its new-patient value -- this is NEVER
				// intended for use, it is for safety...
				switch ( patientCctTypePcvValue )
				{
					case PatientCctTypeValue::NEONATAL_CIRCUIT :	  // $[TI1.6.1]
						boundedValue.value = ::NEO_O2_PERCENT_PCV_VALUE_;
						break;
					case PatientCctTypeValue::PEDIATRIC_CIRCUIT :
					case PatientCctTypeValue::ADULT_CIRCUIT :		  // $[TI1.6.2]
						boundedValue.value = ::ADULT_O2_PERCENT_PCV_VALUE_;
						break;
					default :
						// unexpected circuit type value...
						AUX_CLASS_ASSERTION_FAILURE(patientCctTypePcvValue);
						break;
				}
				boundedValue.precision = ONES;
				break;
			case SettingId::APNEA_PEAK_INSP_FLOW :			// $[TI1.7]
				// this bounded settings is NOT applicable during Safety-PCV,
				// therefore initialize to its new-patient value -- this is NEVER
				// intended for use, it is for safety...
				boundedValue.value     = 22.0f;
				boundedValue.precision = ONES;
				break;
			case SettingId::APNEA_PLATEAU_TIME :			// $[TI1.8]
				// this bounded settings is NOT applicable during Safety-PCV,
				// therefore initialize to its new-patient value -- this is NEVER
				// intended for use, it is for safety...
				boundedValue.value     = 0.0f;
				boundedValue.precision = HUNDREDS;
				break;
			case SettingId::APNEA_RESP_RATE :				// $[TI1.9]
				// this bounded settings is NOT applicable during Safety-PCV,
				// therefore initialize to its new-patient value -- this is NEVER
				// intended for use, it is for safety...
				boundedValue.value     = 16.0f;
				boundedValue.precision = ONES;
				break;
			case SettingId::APNEA_TIDAL_VOLUME :			// $[TI1.10]
				// this bounded settings is NOT applicable during Safety-PCV,
				// therefore initialize to its new-patient value -- this is NEVER
				// intended for use, it is for safety...
				boundedValue.value     = 360.0f;
				boundedValue.precision = ONES;
				break;
			case SettingId::DISCO_SENS :				// $[TI1.11]
				boundedValue.value     = ::DISCO_SENS_PCV_VALUE_;
				boundedValue.precision = ::DISCO_SENS_PCV_PREC_;
				break;
			case SettingId::EXP_SENS :					// $[TI1.12]
				// this bounded settings is NOT applicable during Safety-PCV,
				// therefore initialize to its new-patient value -- this is NEVER
				// intended for use, it is for safety...
				boundedValue.value     = 25.0f;
				boundedValue.precision = ONES;
				break;
			case SettingId::FLOW_ACCEL_PERCENT :			// $[TI1.13]
				boundedValue.value     = ::FLOW_ACCEL_PERCENT_PCV_VALUE_;
				boundedValue.precision = ::FLOW_ACCEL_PERCENT_PCV_PREC_;
				break;
			case SettingId::FLOW_SENS :					// $[TI1.14]
				// this bounded settings is NOT applicable during Safety-PCV,
				// therefore initialize to its new-patient value -- this is NEVER
				// intended for use, it is for safety...
				boundedValue.value     = 2.0f;
				boundedValue.precision = TENTHS;
				break;
			case SettingId::HIGH_CCT_PRESS :				// $[TI1.15]
				boundedValue.value     = ::HIGH_CCT_PRESS_PCV_VALUE_;
				boundedValue.precision = ::HIGH_CCT_PRESS_PCV_PREC_;
				break;
			case SettingId::HIGH_INSP_TIDAL_VOL :			// $[TI1.32]
				// this bounded settings is NOT applicable during Safety-PCV,
				// therefore initialize to its new-patient value -- this is NEVER
				// intended for use, it is for safety...
				boundedValue.value     = 50.0f;
				boundedValue.precision = ONES;
				break;
			case SettingId::HUMID_VOLUME :				// $[TI1.29]
				boundedValue = humidVolumePcv;
				break;
			case SettingId::IBW :					// $[TI1.16]
				boundedValue.value     = ::IBW_PCV_VALUE_;
				boundedValue.precision = ::IBW_PCV_PREC_;
				break;
			case SettingId::INSP_PRESS :				// $[TI1.17]
				boundedValue.value     = ::INSP_PRESS_PCV_VALUE_;
				boundedValue.precision = ::INSP_PRESS_PCV_PREC_;
				break;
			case SettingId::INSP_TIME :					// $[TI1.18]
				boundedValue.value     = ::INSP_TIME_PCV_VALUE_;
				boundedValue.precision = ::INSP_TIME_PCV_PREC_;
				break;
			case SettingId::MIN_INSP_FLOW :				// $[TI1.19]
				// this bounded settings is NOT applicable during Safety-PCV,
				// therefore initialize to its new-patient value -- this is NEVER
				// intended for use, it is for safety...
				boundedValue.value     = 2.20f;
				boundedValue.precision = HUNDREDTHS;
				break;
			case SettingId::OXYGEN_PERCENT :				// $[TI1.20]
				switch ( patientCctTypePcvValue )
				{
					case PatientCctTypeValue::NEONATAL_CIRCUIT :	  // $[TI1.20.1]
						boundedValue.value = ::NEO_O2_PERCENT_PCV_VALUE_;
						break;
					case PatientCctTypeValue::PEDIATRIC_CIRCUIT :
					case PatientCctTypeValue::ADULT_CIRCUIT :		  // $[TI1.20.2]
						boundedValue.value = ::ADULT_O2_PERCENT_PCV_VALUE_;
						break;
					default :
						// unexpected circuit type value...
						AUX_CLASS_ASSERTION_FAILURE(patientCctTypePcvValue);
						break;
				}
				boundedValue.precision = ::OXYGEN_PERCENT_PCV_PREC_;
				break;
			case SettingId::PEAK_INSP_FLOW :				// $[TI1.21]
				// this bounded settings is NOT applicable during Safety-PCV,
				// therefore initialize to its new-patient value -- this is NEVER
				// intended for use, it is for safety...
				boundedValue.value     = 22.0f;
				boundedValue.precision = ONES;
				break;
			case SettingId::PERCENT_SUPPORT :				// $[TI1.30]
				// this bounded settings is NOT applicable during Safety-PCV,
				// therefore initialize to its new-patient value -- this is NEVER
				// intended for use, it is for safety...
				boundedValue.value     = 100.0f;
				boundedValue.precision = ONES;
				break;
			case SettingId::PLATEAU_TIME :				// $[TI1.22]
				// this bounded settings is NOT applicable during Safety-PCV,
				// therefore initialize to its new-patient value -- this is NEVER
				// intended for use, it is for safety...
				boundedValue.value     = 0.0f;
				boundedValue.precision = HUNDREDS;
				break;
			case SettingId::PEEP :					// $[TI1.23]
				boundedValue.value     = ::PEEP_PCV_VALUE_;
				boundedValue.precision = ::PEEP_PCV_PREC_;
				break;
			case SettingId::PEEP_HIGH :									// $[TI1.28]
				// this bounded settings is NOT applicable during Safety-PCV,
				// therefore initialize to its new-patient value -- this is NEVER
				// intended for use, it is for safety...
				boundedValue.value     = ::PEEP_PCV_VALUE_ +
										 SettingConstants::DEFAULT_MAND_PRESS_DELTA;
				boundedValue.precision = TENTHS;
				break;
			case SettingId::PEEP_HIGH_TIME :							// $[TI1.34]
				// this bounded settings is NOT applicable during Safety-PCV,
				// therefore initialize to its new-patient value -- this is NEVER
				// intended for use, it is for safety...
				boundedValue.value     = 1000.0f;
				boundedValue.precision = TENS;
				break;
			case SettingId::PEEP_LOW :								   // $[TI1.33]
				// this bounded settings is NOT applicable during Safety-PCV,
				// therefore initialize to its new-patient value -- this is NEVER
				// intended for use, it is for safety...
				boundedValue.value     = ::PEEP_PCV_VALUE_;
				boundedValue.precision = TENTHS;
				break;
			case SettingId::PRESS_SENS :				// $[TI1.24]
				boundedValue.value     = ::PRESS_SENS_PCV_VALUE_;
				boundedValue.precision = ::PRESS_SENS_PCV_PREC_;
				break;
			case SettingId::PRESS_SUPP_LEVEL :				// $[TI1.25]
				// this bounded settings is NOT applicable during Safety-PCV,
				// therefore initialize to its new-patient value -- this is NEVER
				// intended for use, it is for safety...
				boundedValue.value     = 0.0f;
				boundedValue.precision = ONES;
				break;
			case SettingId::RESP_RATE :					// $[TI1.26]
				boundedValue.value     = ::RESP_RATE_PCV_VALUE_;
				boundedValue.precision = ::RESP_RATE_PCV_PREC_;
				break;
			case SettingId::TIDAL_VOLUME :				// $[TI1.27]
				// this bounded settings is NOT applicable during Safety-PCV,
				// therefore initialize to its new-patient value -- this is NEVER
				// intended for use, it is for safety...
				boundedValue.value     = 360.0f;
				boundedValue.precision = ONES;
				break;
			case SettingId::TUBE_ID :					// $[TI1.31]
				// this bounded settings is NOT applicable during Safety-PCV,
				// therefore initialize to its new-patient value -- this is NEVER
				// intended for use, it is for safety...
				boundedValue.value     = 10.0f;
				boundedValue.precision = TENTHS;
				break;
			case SettingId::VOLUME_SUPPORT :				// $[TI1.35]
				// this bounded settings is NOT applicable during Safety-PCV,
				// therefore initialize to its new-patient value -- this is NEVER
				// intended for use, it is for safety...
				boundedValue.value     = 360.0f;
				boundedValue.precision = ONES;
				break;
			case SettingId::HIGH_SPONT_INSP_TIME :
				// this bounded settings is NOT applicable during Safety-PCV,
				// therefore initialize to a safe value -- this is NEVER
				// intended for use, it is for safety...
				boundedValue.value     = 3.00f;
				boundedValue.precision = TENS;
				break;
			default :
				// illegal setting id...
				AUX_CLASS_ASSERTION_FAILURE(idx);
				break;
		};	// end of switch...

		// store the bounded value...
		SafetyPcvSettingValues::ArrSafetyPcvValues_[idx] = boundedValue;
	}  // end of for ([TI1])...

	//=================================================================
	// discrete, BD settings...
	//=================================================================

	DiscreteValue  humidTypePcvValue;
	DiscreteValue  nominalLineVoltPcvValue;

	// $[02155] -- humidification type is to use its persistent value when
	//             present, otherwise its default value...
	// nominal line voltage is to use its persistent value when present,
	// otherwise its default value...
	if ( ARE_PERS_VALUES_INITIALIZED )
	{	// $[TI2]
		// get the persistent humidifier type value...
		humidTypePcvValue = persistentValues.getDiscreteValue(
															 SettingId::HUMID_TYPE
															 );
		// get the persistent nominal line voltage value...
		nominalLineVoltPcvValue = persistentValues.getDiscreteValue(
																   SettingId::NOMINAL_VOLT
																   );
	}
	else
	{	// $[TI3] -- there are no persistent values; use default values...
		// $[02154] -- default value...
		humidTypePcvValue = HumidTypeValue::NON_HEATED_TUBING_HUMIDIFIER;

		// $[02207] -- DEFAULT value...
		nominalLineVoltPcvValue = NominalLineVoltValue::VAC_120;
	}

	for ( idx = SettingId::LOW_BATCH_BD_DISCRETE_ID_VALUE; 
		idx <= SettingId::HIGH_BATCH_BD_DISCRETE_ID_VALUE; idx++ )
	{	// $[TI6] -- this path is ALWAYS taken...
		switch ( idx )
		{
			case SettingId::APNEA_FLOW_PATTERN :			// $[TI6.1]
				// this discrete settings is NOT applicable during Safety-PCV,
				// therefore initialize to its new-patient value -- this is NEVER
				// intended for use, it is for safety...
				discreteValue = FlowPatternValue::SQUARE_FLOW_PATTERN;
				break;
			case SettingId::APNEA_MAND_TYPE :				// $[TI6.2]
				// this discrete settings is NOT applicable during Safety-PCV,
				// therefore initialize to its new-patient value -- this is NEVER
				// intended for use, it is for safety...
				discreteValue = MandTypeValue::PCV_MAND_TYPE;
				break;
			case SettingId::FIO2_ENABLED :				// $[TI6.3]
				discreteValue = ::FIO2_ENABLED_PCV_VALUE_;
				break;
			case SettingId::FLOW_PATTERN :				// $[TI6.4]
				// this discrete settings is NOT applicable during Safety-PCV,
				// therefore initialize to its new-patient value -- this is NEVER
				// intended for use, it is for safety...
				discreteValue = FlowPatternValue::SQUARE_FLOW_PATTERN;
				break;
			case SettingId::HUMID_TYPE :				// $[TI6.5]
				discreteValue = humidTypePcvValue;
				break;
			case SettingId::MAND_TYPE :					// $[TI6.6]
				discreteValue = ::MAND_TYPE_PCV_VALUE_;
				break;
			case SettingId::MODE :					// $[TI6.7]
				discreteValue = ::MODE_PCV_VALUE_;
				break;
			case SettingId::NOMINAL_VOLT :				// $[TI6.8]
				discreteValue = nominalLineVoltPcvValue;
				break;
			case SettingId::PATIENT_CCT_TYPE :				// $[TI6.9]
				discreteValue = patientCctTypePcvValue;
				break;
			case SettingId::SUPPORT_TYPE :				// $[TI6.11]
				// this discrete settings is NOT applicable during Safety-PCV,
				// therefore initialize to its new-patient value -- this is NEVER
				// intended for use, it is for safety...
				discreteValue = SupportTypeValue::PSV_SUPPORT_TYPE;
				break;
			case SettingId::TRIGGER_TYPE :				// $[TI6.12]
				// $[02258] -- Safety-PCV value...
#ifdef SIGMA_DEVELOPMENT
				if (patientCctTypePcvValue == PatientCctTypeValue::NEONATAL_CIRCUIT)
				{
					discreteValue = TriggerTypeValue::FLOW_TRIGGER;
				}
				else
#endif // SIGMA_DEVELOPMENT
				{
					discreteValue = TriggerTypeValue::PRESSURE_TRIGGER;
				}
				break;
			case SettingId::TUBE_TYPE :					// $[TI6.13]
				// this discrete settings is NOT applicable during Safety-PCV,
				// therefore initialize to its new-patient value -- this is NEVER
				// intended for use, it is for safety...
				discreteValue = TubeTypeValue::ET_TUBE_TYPE;
				break;
			case SettingId::VENT_TYPE :									// $[TI6.14]
				// this discrete settings is NOT applicable during Safety-PCV,
				// therefore initialize to its new-patient value -- this is NEVER
				// intended for use, it is for safety...
				discreteValue = VentTypeValue::INVASIVE_VENT_TYPE;
				break;
			case SettingId::LEAK_COMP_ENABLED:     
				// $[LC02011] 	
				discreteValue = LeakCompEnabledValue::LEAK_COMP_DISABLED;
				break;

            case SettingId::PROX_ENABLED :
				  // $[PX02002]
                   discreteValue = ProxEnabledValue::PROX_DISABLED;
                break;
			default :
				// illegal setting id...
				AUX_CLASS_ASSERTION_FAILURE(idx);
				break;
		};	// end of switch...

		// store the discrete value...
		SafetyPcvSettingValues::ArrSafetyPcvValues_[idx] = discreteValue;
	}  // end of for ([TI6])...

#if defined(SIGMA_GUI_CPU)  ||  defined(SIGMA_COMMON)

	//=================================================================
	// bounded, non-BD settings...
	//=================================================================

	BoundedValue  atmPressure;

	// this setting is to use its respective persistent values (when
	// present) for Safety-PCV ventilation...
	if ( ARE_PERS_VALUES_INITIALIZED )
	{	// $[TI14]
		atmPressure = persistentValues.getBoundedValue(SettingId::ATM_PRESSURE);
	}
	else
	{	// $[TI15] -- there are no persistent values; use default values...
		atmPressure.value     = 760.0f;
		atmPressure.precision = TENTHS;
	}

	for ( idx = SettingId::LOW_BATCH_NONBD_BOUNDED_ID_VALUE; 
		idx <= SettingId::HIGH_BATCH_NONBD_BOUNDED_ID_VALUE; idx++ )
	{	// $[TI7]
		switch ( idx )
		{
			case SettingId::APNEA_EXP_TIME :				// $[TI7.1]
				// this bounded settings is NOT applicable during Safety-PCV,
				// therefore initialize to its new-patient value -- this is NEVER
				// intended for use, it is for safety...
				boundedValue.value     = 2750.0f;
				boundedValue.precision = TENS;
				break;
			case SettingId::APNEA_IE_RATIO :				// $[TI7.2]
				// this bounded settings is NOT applicable during Safety-PCV,
				// therefore initialize to its new-patient value -- this is NEVER
				// intended for use, it is for safety...
				boundedValue.value     = -2.75f;
				boundedValue.precision = HUNDREDTHS;
				break;
			case SettingId::ATM_PRESSURE :				// $[TI7.11]
				boundedValue = atmPressure;
				break;
			case SettingId::EXP_TIME :					// $[TI7.3]
				// this bounded settings is NOT applicable during Safety-PCV,
				// therefore initialize to its new-patient value -- this is NEVER
				// intended for use, it is for safety...
				boundedValue.value     = 2750.0f;
				boundedValue.precision = TENS;
				break;
			case SettingId::HIGH_EXH_MINUTE_VOL :			// $[TI7.4]
				// $[02140] -- Safety-PCV value of high exhaled minute volume is
				//             "off"...
				boundedValue.value     = SettingConstants::UPPER_ALARM_LIMIT_OFF;
				boundedValue.precision = TENTHS;
				break;
			case SettingId::HIGH_EXH_TIDAL_VOL :			 // $[TI7.5]
				// $[02144] -- Safety-PCV value of high exhaled tidal volume is
				//             "off"...
				boundedValue.value     = SettingConstants::UPPER_ALARM_LIMIT_OFF;
				boundedValue.precision = TENS;
				break;
			case SettingId::HIGH_RESP_RATE :				// $[TI7.6]
				// $[02148] -- Safety-PCV value of high respiratory rate is "off"...
				boundedValue.value     = SettingConstants::UPPER_ALARM_LIMIT_OFF;
				boundedValue.precision = ONES;
				break;
			case SettingId::HL_RATIO :									// $[TI7.12]
				// this bounded settings is NOT applicable during Safety-PCV,
				// therefore initialize to its new-patient value -- this is NEVER
				// intended for use, it is for safety...
				boundedValue.value     = -2.75f;
				boundedValue.precision = HUNDREDTHS;
				break;
			case SettingId::IE_RATIO :					// $[TI7.7]
				// this bounded settings is NOT applicable during Safety-PCV,
				// therefore initialize to its new-patient value -- this is NEVER
				// intended for use, it is for safety...
				boundedValue.value     = -2.75f;
				boundedValue.precision = HUNDREDTHS;
				break;
			case SettingId::LOW_CCT_PRESS :								// $[TI7.17]
				// this bounded settings is NOT applicable during Safety-PCV,
				// therefore set it OFF.
				boundedValue.value     = SettingConstants::LOWER_ALARM_LIMIT_OFF;
				boundedValue.precision = ONES;
				break;
			case SettingId::LOW_EXH_MAND_TIDAL_VOL :			// $[TI7.8]
				// $[02180] -- Safety-PCV value of low exhaled mandatory tidal
				//             volume is "off"...
				boundedValue.value     = SettingConstants::LOWER_ALARM_LIMIT_OFF;
				boundedValue.precision = ONES;
				break;
			case SettingId::LOW_EXH_MINUTE_VOL :			// $[TI7.9]
				boundedValue.value     = ::LOW_EXH_MINUTE_VOL_PCV_VALUE_;
				boundedValue.precision = ::LOW_EXH_MINUTE_VOL_PCV_PREC_;
				break;
			case SettingId::LOW_EXH_SPONT_TIDAL_VOL :			// $[TI7.10]
				// $[02188] -- Safety-PCV value of low exhaled spontaneous tidal
				//             volume is "off"...
				boundedValue.value     = SettingConstants::LOWER_ALARM_LIMIT_OFF;
				boundedValue.precision = ONES;
				break;
			case SettingId::MINUTE_VOLUME :				// $[TI7.13]
				// this bounded settings is NOT applicable during Safety-PCV,
				// therefore initialize to its new-patient value -- this is NEVER
				// intended for use, it is for safety...
				boundedValue.value     = 5.76f;
				boundedValue.precision = HUNDREDTHS;
				break;
			case SettingId::PEEP_LOW_TIME :								// $[TI7.14]
				// this bounded settings is NOT applicable during Safety-PCV,
				// therefore initialize to its new-patient value -- this is NEVER
				// intended for use, it is for safety...
				boundedValue.value     = 2750.0f;
				boundedValue.precision = TENS;
				break;
			case SettingId::VSUPP_IBW_RATIO :				// $[TI7.15]
				// this bounded settings is NOT applicable during Safety-PCV,
				// therefore initialize to its new-patient value -- this is NEVER
				// intended for use, it is for safety...
				boundedValue.value     = 7.25f;
				boundedValue.precision = HUNDREDTHS;
				break;
			case SettingId::VT_IBW_RATIO :				// $[TI7.16]
				// this bounded settings is NOT applicable during Safety-PCV,
				// therefore initialize to its new-patient value -- this is NEVER
				// intended for use, it is for safety...
				boundedValue.value     = 7.25f;
				boundedValue.precision = HUNDREDTHS;
				break;
			default :
				// illegal setting id...
				AUX_CLASS_ASSERTION_FAILURE(idx);
				break;
		};	// end of switch...

		// store the bounded value...
		SafetyPcvSettingValues::ArrSafetyPcvValues_[idx] = boundedValue;
	}  // end of for ([TI7])...

	//=================================================================
	// discrete, non-BD settings...
	//=================================================================

	DiscreteValue  dciBaudRatePcvValue;
	DiscreteValue  dciDataBitsPcvValue;
	DiscreteValue  dciParityModePcvValue;
	DiscreteValue  pressUnitsPcvValue;
	DiscreteValue  languagePcvValue;
	DiscreteValue  serviceBaudRatePcvValue;
	DiscreteValue  comp2BaudRatePcvValue;
	DiscreteValue  comp2DataBitsPcvValue;
	DiscreteValue  comp2ParityModePcvValue;
	DiscreteValue  comp3BaudRatePcvValue;
	DiscreteValue  comp3DataBitsPcvValue;
	DiscreteValue  comp3ParityModePcvValue;
	DiscreteValue  comp1PortConfigValue;
	DiscreteValue  comp2PortConfigValue;
	DiscreteValue  comp3PortConfigValue;
	DiscreteValue  dateFormatValue;
	if ( ARE_PERS_VALUES_INITIALIZED )
	{	// $[TI8]
		// get the persistent DCI baud rate value...
		dciBaudRatePcvValue = persistentValues.getDiscreteValue(
															   SettingId::DCI_BAUD_RATE
															   );

		// get the persistent DCI data bits value...
		dciDataBitsPcvValue = persistentValues.getDiscreteValue(
															   SettingId::DCI_DATA_BITS
															   );

		// get the persistent DCI parity mode value...
		dciParityModePcvValue = persistentValues.getDiscreteValue(
																 SettingId::DCI_PARITY_MODE
																 );

		// get the persistent pressure units value...
		// $[NE02014]
		pressUnitsPcvValue = persistentValues.getDiscreteValue(
															  SettingId::PRESS_UNITS
															  );

		// get the persistent language value...
		languagePcvValue = persistentValues.getDiscreteValue(
															  SettingId::LANGUAGE
															  );

		// get the persistent service baud rate value...
		serviceBaudRatePcvValue = persistentValues.getDiscreteValue(
																   SettingId::SERVICE_BAUD_RATE
																   );

		// get the persistent COM2 baud rate value...
		comp2BaudRatePcvValue = persistentValues.getDiscreteValue(
																 SettingId::COM2_BAUD_RATE
																 );

		// get the persistent COM2 data bits value...
		comp2DataBitsPcvValue = persistentValues.getDiscreteValue(
																 SettingId::COM2_DATA_BITS
																 );

		// get the persistent COM2 parity mode value...
		comp2ParityModePcvValue = persistentValues.getDiscreteValue(
																   SettingId::COM2_PARITY_MODE
																   );

		// get the persistent COM3 baud rate value...
		comp3BaudRatePcvValue = persistentValues.getDiscreteValue(
																 SettingId::COM3_BAUD_RATE
																 );

		// get the persistent COM3 data bits value...
		comp3DataBitsPcvValue = persistentValues.getDiscreteValue(
																 SettingId::COM3_DATA_BITS
																 );

		// get the persistent COM3 parity mode value...
		comp3ParityModePcvValue = persistentValues.getDiscreteValue(
																   SettingId::COM3_PARITY_MODE
																   );

		// get the persistent DCI com port configu value...
		comp1PortConfigValue = persistentValues.getDiscreteValue(
																SettingId::COM1_CONFIG
																);

		// get the persistent COM2 com port configu value...
		comp2PortConfigValue = persistentValues.getDiscreteValue(
																SettingId::COM2_CONFIG
																);

		// get the persistent COM3 com port configu value...
		comp3PortConfigValue = persistentValues.getDiscreteValue(
																SettingId::COM3_CONFIG
																);
		// get the persistent Date Format value...
		dateFormatValue = persistentValues.getDiscreteValue(
														   SettingId::DATE_FORMAT
														   );
	}
	else
	{	// $[TI9] -- there are no persistent values; use default values...
		// $[01191] -- new-patient value...
		dciBaudRatePcvValue = BaudRateValue::BAUD_9600_VALUE;

		// $[01191] -- new-patient value...
		dciDataBitsPcvValue = DciDataBitsValue::BITS_8_VALUE;

		// $[01191] -- new-patient value...
		dciParityModePcvValue = DciParityModeValue::NO_PARITY_VALUE;

		// $[02271] -- new-patient value...
		pressUnitsPcvValue = PressUnitsValue::CMH2O_UNIT_VALUE;

		// $[07046] -- new-patient value...
		serviceBaudRatePcvValue = BaudRateValue::BAUD_9600_VALUE;

		comp2BaudRatePcvValue = BaudRateValue::BAUD_9600_VALUE;

		comp2DataBitsPcvValue = DciDataBitsValue::BITS_8_VALUE;

		comp2ParityModePcvValue = DciParityModeValue::NO_PARITY_VALUE;

		comp3BaudRatePcvValue = BaudRateValue::BAUD_9600_VALUE;

		comp3DataBitsPcvValue = DciDataBitsValue::BITS_8_VALUE;

		comp3ParityModePcvValue = DciParityModeValue::NO_PARITY_VALUE;

		comp1PortConfigValue = ComPortConfigValue::DCI_VALUE;

		comp2PortConfigValue = ComPortConfigValue::DCI_VALUE;

		comp3PortConfigValue = ComPortConfigValue::DCI_VALUE;

		dateFormatValue = DateFormatValue::DATE_FORMAT_DD_MMM_YY;
#ifdef SIGMA_CHINESE
		languagePcvValue = LanguageValue::ENGLISH;
#elif SIGMA_ENGLISH
		languagePcvValue = LanguageValue::ENGLISH;
#elif SIGMA_FRENCH
    	languagePcvValue = LanguageValue::FRENCH;
#elif SIGMA_GERMAN
    	languagePcvValue = LanguageValue::GERMAN;
#elif SIGMA_ITALIAN
    	languagePcvValue = LanguageValue::ITALIAN;
#elif SIGMA_JAPANESE
    	languagePcvValue = LanguageValue::JAPANESE;
#elif SIGMA_POLISH
    	languagePcvValue = LanguageValue::POLISH;
#elif SIGMA_PORTUGUESE
    	languagePcvValue = LanguageValue::PORTUGUESE;
#elif SIGMA_RUSSIAN
    	languagePcvValue = LanguageValue::RUSSIAN;
#elif SIGMA_SPANISH
    	languagePcvValue = LanguageValue::SPANISH;
#else
		#pragma error "LANGUAGE not defined"
#endif
	}

	for ( idx = SettingId::LOW_BATCH_NONBD_DISCRETE_ID_VALUE; 
		idx <= SettingId::HIGH_BATCH_NONBD_DISCRETE_ID_VALUE; idx++ )
	{	// $[TI10]
		switch ( idx )
		{
			case SettingId::APNEA_CONSTANT_PARM :		// $[TI10.1]
				// this discrete settings is NOT applicable during Safety-PCV,
				// therefore initialize to its new-patient value -- this is NEVER
				// intended for use, it is for safety...
				discreteValue = ConstantParmValue::INSP_TIME_CONSTANT;
				break;
			case SettingId::CONSTANT_PARM :			// $[TI10.2]
				// this discrete settings is NOT applicable during Safety-PCV,
				// therefore initialize to its new-patient value -- this is NEVER
				// intended for use, it is for safety...
				discreteValue = ConstantParmValue::INSP_TIME_CONSTANT;
				break;
			case SettingId::DCI_BAUD_RATE :			// $[TI10.3]
				discreteValue = dciBaudRatePcvValue;
				break;
			case SettingId::DCI_DATA_BITS :			// $[TI10.4]
				discreteValue = dciDataBitsPcvValue;
				break;
			case SettingId::DCI_PARITY_MODE :			// $[TI10.5]
				discreteValue = dciParityModePcvValue;
				break;
			case SettingId::MONTH :				// $[TI10.6]
				// this discrete settings is NOT applicable during Safety-PCV,
				// therefore initialize to its new-patient value -- this is NEVER
				// intended for use, it is for safety...
				discreteValue = MonthValue::JAN;
				break;
			case SettingId::PRESS_UNITS :			// $[TI10.7]
				discreteValue = pressUnitsPcvValue;
				break;
			case SettingId::LANGUAGE :
				discreteValue = languagePcvValue;
				break;
			case SettingId::SERVICE_BAUD_RATE :			// $[TI10.8]
				discreteValue = serviceBaudRatePcvValue;
				break;
			case SettingId::COM2_BAUD_RATE :			// $[TI10.9]
				discreteValue = comp2BaudRatePcvValue;
				break;
			case SettingId::COM2_DATA_BITS :			// $[TI10.10]
				discreteValue = comp2DataBitsPcvValue;
				break;
			case SettingId::COM2_PARITY_MODE :			// $[TI10.11]
				discreteValue = comp2ParityModePcvValue;
				break;
			case SettingId::COM3_BAUD_RATE :			// $[TI10.12]
				discreteValue = comp3BaudRatePcvValue;
				break;
			case SettingId::COM3_DATA_BITS :			// $[TI10.13]
				discreteValue = comp3DataBitsPcvValue;
				break;
			case SettingId::COM3_PARITY_MODE :			// $[TI10.14]
				discreteValue = comp3ParityModePcvValue;
				break;
			case SettingId::COM1_CONFIG :				// $[TI10.15]
				discreteValue = comp1PortConfigValue;
				break;
			case SettingId::COM2_CONFIG :				// $[TI10.16]
				discreteValue = comp2PortConfigValue;
				break;
			case SettingId::COM3_CONFIG :				// $[TI10.17]
				discreteValue = comp3PortConfigValue;
				break;
			case SettingId::DATE_FORMAT :				// $[TI10.17]
				discreteValue = dateFormatValue;
				break;
			default :
				// illegal setting id...
				AUX_CLASS_ASSERTION_FAILURE(idx);
				break;
		};	// end of switch...

		// store the discrete value...
		SafetyPcvSettingValues::ArrSafetyPcvValues_[idx] = discreteValue;
	}  // end of for ([TI10])...

	//=================================================================
	// sequential, non-BD settings...
	//=================================================================

	SequentialValue  sequentialValue;
	SequentialValue  displayContrastDeltaPcvValue;

	// this setting is to use its respective persistent values (when
	// present) for Safety-PCV ventilation...
	if ( ARE_PERS_VALUES_INITIALIZED )
	{	// $[TI11]
		// get the persistent display contrast delta value...
		displayContrastDeltaPcvValue = persistentValues.getSequentialValue(
																		  SettingId::DISPLAY_CONTRAST_DELTA
																		  );
	}
	else
	{	// $[TI12] -- there are no persistent values; use default values...
		// $[02268] -- the setting's new-patient value ...
		displayContrastDeltaPcvValue = 0;
	}

	for ( idx = SettingId::LOW_BATCH_NONBD_SEQUENTIAL_ID_VALUE; 
		idx <= SettingId::HIGH_BATCH_NONBD_SEQUENTIAL_ID_VALUE; idx++ )
	{	// $[TI13]
		switch ( idx )
		{
			case SettingId::DAY :				// $[TI13.1]
				// this discrete settings is NOT applicable during Safety-PCV,
				// therefore initialize to its new-patient value -- this is NEVER
				// intended for use, it is for safety...
				sequentialValue = 1u;
				break;
			case SettingId::DISPLAY_CONTRAST_DELTA :		// $[TI13.2]
				sequentialValue = displayContrastDeltaPcvValue;
				break;
			case SettingId::HOUR :				// $[TI13.3]
				// this discrete settings is NOT applicable during Safety-PCV,
				// therefore initialize to its new-patient value -- this is NEVER
				// intended for use, it is for safety...
				sequentialValue = 12u;
				break;
			case SettingId::MINUTE :				// $[TI13.4]
				// this discrete settings is NOT applicable during Safety-PCV,
				// therefore initialize to its new-patient value -- this is NEVER
				// intended for use, it is for safety...
				sequentialValue = 0u;
				break;
			case SettingId::YEAR :				// $[TI13.5]
				// this discrete settings is NOT applicable during Safety-PCV,
				// therefore initialize to its new-patient value -- this is NEVER
				// intended for use, it is for safety...
				sequentialValue = 1995u;
				break;
			default :
				// illegal setting id...
				AUX_CLASS_ASSERTION_FAILURE(idx);
				break;
		};	// end of switch...

		// store the sequential value...
		SafetyPcvSettingValues::ArrSafetyPcvValues_[idx] = sequentialValue;
	}  // end of for...
#endif // defined(SIGMA_GUI_CPU)  ||  defined(SIGMA_COMMON)
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	SafetyPcvSettingValues::SoftFault(const SoftFaultID  softFaultID,
									  const Uint32       lineNumber,
									  const char*        pFileName,
									  const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

	FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
							SAFETY_PCV_SETTING_VALUES, lineNumber, pFileName,
							pPredicate);
}
