
#ifndef Heap_SNodeC_Area_MAX_AREA_LIST_NUM_HH
#define Heap_SNodeC_Area_MAX_AREA_LIST_NUM_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: Heap_SNodeC_Area_MAX_AREA_LIST_NUM - Fixed Heap of Memory Blocks.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Foundation/vcssrc/FixedHeap.H_v   25.0.4.0   19 Nov 2013 14:03:46   pvcs  $
//
//@ Modification-Log
//
//   Revision: 003   By: sah   Date: 07-Jan-1999   DR Number: 5321
//   Project:  ATC
//   Description:
//	Changed 'SoftFault()' method to an non-inlined method.
//  
//   Revision: 002   By: sah   Date: 06-Feb-1996   DCS Number: 674
//   Project:  Sigma (R8027)
//   Description:
//	Removed 'ifndef' and 'endif' from around includes.
//
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//====================================================================

#include "GuiSize.hh"
#include "GuiSize.hh"

//@ Usage-Classes
#include "SNodeC_Area.hh"
#include "Heap.hh"
//@ End-Usage


class Heap_SNodeC_Area_MAX_AREA_LIST_NUM : public Heap
{
  public:
    Heap_SNodeC_Area_MAX_AREA_LIST_NUM(void);
    virtual ~Heap_SNodeC_Area_MAX_AREA_LIST_NUM(void);

    static void  SoftFault(const SoftFaultID softFaultID, 
			   const Uint32      lineNumber, 
			   const char*       pFileName = NULL, 
			   const char*       pBoolTest = NULL); 

  protected:
    virtual const Node*  getFirstNode_(void) const;

#if defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)
    virtual Ostream&  print_(Ostream& ostr) const;
#endif  // defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)

  private:
    Heap_SNodeC_Area_MAX_AREA_LIST_NUM(const Heap_SNodeC_Area_MAX_AREA_LIST_NUM&);	// not implemented...
    void  operator=(const Heap_SNodeC_Area_MAX_AREA_LIST_NUM&);	// not implemented...

    //@ Constant:  BLOCK_NODES_
    // The number of 'Nodes' that are in a single memory block.
    enum { BLOCK_NODES_ = ((sizeof(SNodeC_Area) + sizeof(Node)-1) / sizeof(Node)) };

    //@ Data-Member:  pMemBlock_
    // A block of nodes provides the memory block.
    Node  pMemBlock_[BLOCK_NODES_ * MAX_AREA_LIST_NUM];
};


#endif  // Heap_SNodeC_Area_MAX_AREA_LIST_NUM_HH
