#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============= C L A S S   D E S C R I P T I O N ====================
//@ Class:  NonBatchDiscreteSetting - Non-Batch Discrete Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This class provides the setting's framework for the Non-Batch Discrete
//  Settings.  All non-batch discrete settings derive from this setting.  This
//  class overrides some of the virtual methods of 'DiscreteSetting'
//  including those to:  get this non-batch setting's "adjusted" value,
//  and indicate whether this setting's value has been changed before being
//  being accepted.  These methods are overridden and given a definition
//  that is common to all non-batch discrete settings, but different from
//  all batch discrete setting (e.g., there is no "adjusted" value of a
//  non-batch setting, therefore these values are retrieved from and stored
//  into the Accepted Context).  These methods are overridden, if needed, by
//  the derived non-batch discrete settings to provide the specific behavior
//  needed.
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to establish the non-batch discrete
//  setting's framework for the rest of the settings to build upon.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class defines no instances -- it just overrides some behavior.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/NonBatchDiscreteSetting.ccv   25.0.4.0   19 Nov 2013 14:27:32   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005 By: srp    Date: 28-May-2002   DR Number: 5898
//  Project:  VCP
//	Description:
//		Incorporated Remediation code review issue(s) -- commentary changes only.
//
//  Revision: 004   By: sah  Date:  03-Jan-00    DR Number:  5327
//  Project:  NeoMode
//  Description:
//  Updated for NeoMode
//
//  Revision: 003   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 002   By: sah    Date:  28-May-1998    DR Number: 5092
//  Project:  COLOR
//  Description:
//	--- Merged Rev "Color" (1.11.1.0) into Rev "BiLevel" (1.11.2.0)
//	Added the passing of new constructor parameters from caller
//	(derived class), to base class.  These new parameters are used
//	for allowing discrete settings to have bounded ranges.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Initial version.
//
//=====================================================================

#include "NonBatchDiscreteSetting.hh"

//@ Usage-Classes
#include "ContextMgr.hh"
#include "AcceptedContext.hh"
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method: ~NonBatchDiscreteSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this discrete setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

NonBatchDiscreteSetting::~NonBatchDiscreteSetting(void)
{
  CALL_TRACE("~NonBatchDiscreteSetting()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  isChanged()  [const, virtual]
//
//@ Interface-Description
//  Has this non-batch setting been changed?  Since a non-batch setting's
//  "adjusted" value IS the "accepted" value, this method will ALWAYS
//  return 'FALSE'.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[01038] -- is changed whenever the value is different than accepted...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean
NonBatchDiscreteSetting::isChanged(void) const
{
  CALL_TRACE("isChanged()");

  return(FALSE);
}   // $[TI1]


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method: getDefaultValue()  [const, pure virtual]
//
//@ Interface-Description
//  This is a pure virtual method that is to be overridden by ALL non-batch
//  settings.  This method is to return this setting's default value.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

//@ Not-Implemented


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method: getNewPatientValue()  [const, pure virtual]
//
//@ Interface-Description
//  This is a virtual method that is NEVER to be overidden by any of this
//  class's derived classes.  This method is NEVER to be called for non-batch
//  settings, and will fail its assertion if called.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (FALSE)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
NonBatchDiscreteSetting::getNewPatientValue(void) const
{
  CALL_TRACE("getNewPatientValue()");
  SAFE_CLASS_ASSERTION((SettingId::IsNonBatchDiscreteId(getId())));
  CLASS_ASSERTION_FAILURE();

  // this line will NEVER run...
  DiscreteValue  dummyValue = 0u;

  return(dummyValue);
}


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method: getAdjustedValue(void)
//
//@ Interface-Description
//  Return this non-batch discrete setting's "adjusted" value.  This
//  method is overridden to get ALL non-batch setting's "adjusted"
//  value from the Accepted Context.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (SettingId::IsNonBatchDiscreteId(getId()))
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
NonBatchDiscreteSetting::getAdjustedValue(void) const
{
  CALL_TRACE("getAdjustedValue()");
  SAFE_CLASS_PRE_CONDITION((SettingId::IsNonBatchDiscreteId(getId())));

  return(getAcceptedValue());
}  // $[TI1]


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  setAdjustedValue(newAdjustedValue)  [virtual]
//
//@ Interface-Description
//  Set this non-batch discrete setting's accepted value to
//  'newAdjustedValue'.  This is overridden by this class so that ALL
//  non-batch settings set their "adjusted" value into the Accepted
//  Context.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
NonBatchDiscreteSetting::setAdjustedValue(const SettingValue& newAdjustedValue)
{
  CALL_TRACE("setAdjustedValue(newAdjustedValue)");

  setAcceptedValue(newAdjustedValue);
}   // $[TI1]


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  updateToNewPatientValue()  [virtual]
//
//@ Interface-Description
//  This is a virtual method that is NOT to be overridden by any derived
//  class, and it is NEVER to be called.  This method is reserved for all
//  batch discrete settings.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (FALSE)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
NonBatchDiscreteSetting::updateToNewPatientValue(void)
{
  CALL_TRACE("updateToNewPatientValue()");
  CLASS_ASSERTION_FAILURE();
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  calcTransition(newValue, currValue)  [virtual]
//
//@ Interface-Description
//  This is a virtual method that is NOT to be overridden by any derived
//  class.  This method is reserved for use by batch settings, therefore
//  this version of this virtual method will ALWAYS assert.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (FALSE)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
NonBatchDiscreteSetting::calcTransition(const DiscreteValue,
				        const DiscreteValue)
{
  CALL_TRACE("calcTransition(newValue, currValue)");
  CLASS_ASSERTION_FAILURE();

  // will NEVER get here...
}


//================ M E T H O D   D E S C R I P T I O N ================
//@ Method:  acceptTransition(settingId, newValue, currValue)  [virtual]
//
//@ Interface-Description
//  This is a virtual method that is NOT to be overridden by any derived
//  class, and it is NEVER to be called.  This method is reserved for all
//  batch settings.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (FALSE)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
NonBatchDiscreteSetting::acceptTransition(const SettingId::SettingIdType,
					  const DiscreteValue,
					  const DiscreteValue)
{
  CALL_TRACE("acceptTransition(settingId, newValue, currValue)");
  AUX_CLASS_ASSERTION_FAILURE(getId());

  // NEVER gets here...
}


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//        [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
NonBatchDiscreteSetting::SoftFault(const SoftFaultID  softFaultID,
				   const Uint32       lineNumber,
				   const char*        pFileName,
				   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
  			  NON_BATCH_DISCRETE_SETTING,
                          lineNumber, pFileName, pPredicate);
}



//=====================================================================
//
//  Protected Methods..
//
//=====================================================================

//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  NonBatchDiscreteSetting(...)
//
//@ Interface-Description
//  Construct a non-batch discrete setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

NonBatchDiscreteSetting::NonBatchDiscreteSetting(
			    const SettingId::SettingIdType nonBatchDiscreteId,
		      	    const SettingId::SettingIdType* arrDependentSettingIds,
			    const Uint                     totalNumValues,
			    const Boolean                  useNonWrappingMode,
			    const SettingBoundId              upperLimitBoundId,
			    const SettingBoundId              lowerLimitBoundId
						)
	   : DiscreteSetting(nonBatchDiscreteId, 
				arrDependentSettingIds, 
				totalNumValues, 
				useNonWrappingMode,
	   		     	upperLimitBoundId, 
				lowerLimitBoundId)
{
  CALL_TRACE("NonBatchDiscreteSetting(...)");
  AUX_CLASS_PRE_CONDITION((SettingId::IsNonBatchDiscreteId(nonBatchDiscreteId)),
  			  nonBatchDiscreteId);
}   // $[TI1]
