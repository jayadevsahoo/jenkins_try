#ifndef	ScreenCapture_HH
# define ScreenCapture_HH

//====================================================================
// This is a proprietary work to which Nellcor Puritan Bennett
// corporation of California claims exclusive right.  No part of this
// work may be used, disclosed, reproduced, sorted in an information
// retrieval system, or transmitted by any means, electronic,
// mechanical, photocopying, recording, or otherwise without the prior
// written permission of Nellcor Puritan Bennett Corporation.
//
//     Copyright (c) 1997, Nellcor Puritan Bennett Corporation
//=====================================================================

//=====================================================================
//@ Class:  ScreenCapture - Capture pixel data from VGA memory
//---------------------------------------------------------------------
//@ Version-Information
// @ $Header:   /840/Baseline/GUI-Serial-Interface/vcssrc/ScreenCapture.hhv   25.0.4.0   19 Nov 2013 14:12:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002    By: quf         Date: 26-Sep-2001  DR Number: 5942
//  Project: GuiComms
//  Description:
//  Code/comment/header cleanup.
//
//  Revision: 001    By: gdc         Date: 23-Jan-2001  DR Number: 5493
//  Project: GuiComms
//  Description:
//      Add GuiComms functionality.
//
//=====================================================================

#include "GUI_Serial_Interface.hh"

//@ Usage-Classes
//@ End-Usage

class DisplayContext;

class ScreenCapture
{
  public:
	Int32 copyRasterLine(void* pDest);
	void  resetByteCount(void);
    void  captureData(void);

	static void  SoftFault(const SoftFaultID softFaultID,
					       const Uint32	lineNumber,
					       const char *	pFileName = NULL,
					       const char *	pPredicate = NULL);

  protected:
	ScreenCapture( Uint32* pRasterData, 
				   Uint32  sizeofRasterData,
				   DisplayContext& rDisplayContext,
				   Int32 x0, Int32 y0,
	               Uint32 xWidth, Uint32 yHeight );
	~ScreenCapture();

	Uint32* const pRasterData_;

	virtual Byte  filterPixel_(Byte pixel) const = 0;

	Int32 copyRasterData_(void* pDest, Uint32 nbytes);

  private:
	ScreenCapture();

	DisplayContext & rDisplayContext_;
    const Int32 x0_;
	const Int32 y0_;
    const Uint32 xWidth_;
    const Uint32 yHeight_;
    const Uint32 sizeofRasterLine_;
    const Uint32 sizeofBitmap_;
    Uint32 byteCount_;
};

#endif	// ! ScreenCapture_HH
