#include "stdafx.h"
#include "Swat.h"
#include "GuiIOServer.h"
#include <stdio.h>
#include <conio.h>
#include <process.h>
#include <mmsystem.h>
#include "MessageBox.h"
#include "AskUserDlg.h"

static GuiIOError err;
static CString strErr;

//logs message in the log status window
static int log_messageEx(lua_State *L)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	
	if(lua_isstring(L, 1) == 0)
		return 0;

	CString str(lua_tostring(L, 1));

	int n;
	n = (int)(lua_tonumber(L, 2));

	
	//don't keep logging if script stop triggered
	if(pApp->m_bRun )
	{
		pApp->LogStatus(str, n);
	}
	
	
	return 1;

}

static int SWATGetScriptPath(lua_State *L)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	lua_pushstring(L, pApp->m_strTestScriptPath);
	return 1;
}

//Pops up a message box and waits for ok/cancel
static int show_message(lua_State *L)
{
	//Play sound
	//::PlaySound("chimes.wav",NULL,SND_FILENAME|SND_ASYNC);

	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	CString str(lua_tostring(L, 1));
	CMessageBox msgDlg;
	msgDlg.m_strMessage = str;
	msgDlg.m_nBoxTypeRepeat = 0;

	msgDlg.DoModal();
	

	return 1;
}

static int repeat_message(lua_State *L)
{
	//Play sound
	//::PlaySound("chimes.wav",NULL,SND_FILENAME|SND_ASYNC);

	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	CString str(lua_tostring(L, 1));
	CMessageBox msgDlg;
	msgDlg.m_strMessage = str;
	msgDlg.m_nBoxTypeRepeat = 1;

	msgDlg.DoModal();

	lua_pushnumber(L, msgDlg.m_nReturn);





	return 1;
}

static int ask_user(lua_State *L)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	CString strMessage(lua_tostring(L, 1));
	CString str1(lua_tostring(L, 2));
	CString str2(lua_tostring(L, 3));
	CString str3(lua_tostring(L, 4));
	CString str4(lua_tostring(L, 5));

	CAskUserDlg usrDlg;
	usrDlg.m_strMessage = strMessage;
	usrDlg.m_strButton1 = str1;
	usrDlg.m_strButton2 = str2;
	usrDlg.m_strButton3 = str3;
	usrDlg.m_strButton4 = str4;

	usrDlg.DoModal();

	lua_pushnumber(L, usrDlg.m_nReturn);

	return 1;
}


//Pops up a message box and resumes script without waiting
static int show_and_continue_message(lua_State *L)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	char buff[500];
	strcpy_s(buff, lua_tostring(L, 1));
	CString strMessage(buff);


	CString* p = new CString(strMessage);
	if(pApp->m_pMainWnd->m_hWnd)
	{
		::PostMessage(pApp->m_pMainWnd->m_hWnd,WM_SHOW_MSG, 0, reinterpret_cast<LPARAM>(p));
	}
	else
	{
		LOG_MESSAGE(LM_ERROR,"Window Handle not valid ");
		delete p;
	}

	
	return 1;
}

//Clears message box
static int clear_message(lua_State *L)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();

	if(pApp->m_pMainWnd->m_hWnd)
		::PostMessage(pApp->m_pMainWnd->m_hWnd,WM_HIDE_MSG, 0, 0);
	else
		LOG_MESSAGE(LM_ERROR,"Window Handle not valid ");
	
	return 1;
}

static int resetsettings( lua_State *L )
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	pApp->m_bReset = 1;
	return 1;


}

static int sendsettings(lua_State *L)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	int n(0);
	
 
	return 1;


}


static int gui_MousePosition(lua_State *L)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();

	char cmd[500];
	strcpy_s(cmd, lua_tostring(L, 1));
	CString strX(cmd);

	char val[500];
	strcpy_s(val, lua_tostring(L, 2));
	CString strY(val);

	err = GuiIOMousePos(atoi(strX), atoi(strY));

	if(err == GUIIO_ERROR) 
	{
		strErr.Format("ERROR : GuiIOMousePos failed on %s, %s.",strX,strY );
		pApp->LogStatus(strErr);
		return 1;
	}

	return 1;

}
static int gui_IOGetXYWH(lua_State *L)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	char cmd[500];
	strcpy_s(cmd, lua_tostring(L, 1));

	lua_pushstring(L, pApp->GUI_IOGetXYWH(cmd));
	return 1;
}

static int gui_IOClickXY(lua_State *L)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	
	char X[500];
	strcpy_s(X, lua_tostring(L, 1));
	
	char Y[500];
	strcpy_s(Y, lua_tostring(L, 2));

	char nVariable[500];
	strcpy_s(nVariable, lua_tostring(L, 3));

	return pApp->GUI_IOClickXY(atoi(X), atoi(Y), static_cast<float>(atof(nVariable)));
}


static int gui_IOClickXYDirect(lua_State *L)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	
	char X[500];
	strcpy_s(X, lua_tostring(L, 1));
	
	char Y[500];
	strcpy_s(Y, lua_tostring(L, 2));

	return pApp->GUI_IOClickXYDirect(atoi(X), atoi(Y));
}


static int gui_IOGetValue(lua_State *L)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	char cmd[500];
	strcpy_s(cmd, lua_tostring(L, 1));

	lua_pushstring(L, pApp->GUI_IOGetValue(cmd));
	return 1;
}

static int gui_IOGetText(lua_State *L)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	char cmd[500];
	strcpy_s(cmd, lua_tostring(L, 1));

	lua_pushstring(L, pApp->GUI_IOGetText(cmd));
	return 1;
}

static int gui_IOGetRowText(lua_State *L)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	char cmd[500];
	strcpy_s(cmd, lua_tostring(L, 1));
	char cmd2[500];
	strcpy_s(cmd2, lua_tostring(L, 2));
	
	lua_pushstring(L, pApp->GUI_IOGetRowText(cmd, atoi(cmd2)));
	return 1;
}

static int gui_IOGetCellText(lua_State *L)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	char cmd[500];
	strcpy_s(cmd, lua_tostring(L, 1));
	char cmd2[500];
	strcpy_s(cmd2, lua_tostring(L, 2));
	char cmd3[500];
	strcpy_s(cmd3, lua_tostring(L, 3));
	
	lua_pushstring(L, pApp->GUI_IOGetCellText(cmd, atoi(cmd2), atoi(cmd3)));
	return 1;
}


static int gui_IOKnob(lua_State *L)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	char cmd[500];
	strcpy_s(cmd, lua_tostring(L, 1));

	lua_pushnumber(L, pApp->GUI_IOKnob(atoi(cmd)));
	return 1;
}

static int gui_IODisconnect(lua_State *L)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	pApp->GUI_IODisconnect();
	return 1;
}

static int gui_IOConnect(lua_State *L)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	lua_pushnumber(L, pApp->GUI_IOConnect());
	return 1;
}
static int gui_LButtonDown(lua_State *L)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	char cmd[500];
	strcpy_s(cmd, lua_tostring(L, 1));
	CString strX(cmd);
	char val[500];
	strcpy_s(val, lua_tostring(L, 2));
	CString strY(val);
	err = GuiIOLButtonDown(atoi(strX), atoi(strY));

	if(err != GUIIO_SUCCESS) 
	{
		strErr.Format("ERROR : GuiIOLButtonDown failed on %s, %s, error code %d",strX,strY, err );
		pApp->LogStatus(strErr);
		return 1;
	}

	return 1;
}
static int gui_LButtonUp(lua_State *L)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	char cmd[500];
	strcpy_s(cmd, lua_tostring(L, 1));
	CString strX(cmd);
	char val[500];
	strcpy_s(val, lua_tostring(L, 2));
	CString strY(val);
	err = GuiIOLButtonUp(atoi(strX), atoi(strY));

	if(err != GUIIO_ERROR) 
	{
		strErr.Format("ERROR : GuiIOLButtonUp failed on %s, %s, error code %d",strX,strY, err );
		pApp->LogStatus(strErr);
		return 0;
	}
	return 1;
}

static int over_write_pd_valueEx(lua_State *L)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	char cmd[500];
	strcpy_s(cmd, lua_tostring(L, 1));
	CString strParameter(cmd);
	
	char val[500];
	strcpy_s(val, lua_tostring(L, 2));
	CString strNewValue(val);
	
	return pApp->Over_Write_PD_ValueEx(strParameter, strNewValue);
}

static int get_raw_pd_valueEx(lua_State *L)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	char cmd[500];
	strcpy_s(cmd, lua_tostring(L, 1));
	CString strParameter(cmd);

	lua_pushstring(pApp->m_L,pApp->Get_Raw_PD_ValueEx(strParameter));

	return 1;
}

static int reset_pd_valuesEx(lua_State *L)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	return pApp->Reset_PD_ValuesEx( );
}

static int gui_setvalue(lua_State *L)
{

	

	return 1;

	
}

static int pause(lua_State *L)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	lua_getglobal(L, "showtrace");
	lua_call(L, 0, 1);

	CString str;
	char chVariable[500];
	double dValue;
	
	lua_Debug ar;

	int nLevel = 1;
	while(lua_getstack (L, nLevel, &ar) != 0)
	{
		int nIndex = 1;
		while(lua_getlocal (L, &ar, nIndex) != NULL)
		{
			strcpy(chVariable, lua_getlocal (L, &ar, nIndex));

			if(lua_isnumber(L, -1))
			{
				dValue = lua_tonumber(L, -1);
				str.Format(">>>Local var : %s = %f", chVariable, dValue);
			}else if(lua_isstring(L, -1))
			{
				str.Format(">>>Local var : %s = %s", chVariable, lua_tostring(L, -1));
			}
			pApp->LogStatus(str);
			nIndex ++;
		}
		nLevel ++;
	}
	
	pApp->m_bPause = true;
	while(pApp->m_bPause)
	{
		Sleep(50);
	}
	
	return 1;

}

static int stop_script(lua_State *L)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();

	if(!pApp->m_bRun)
	{
		lua_pushnumber(L, !pApp->m_bRun);
	}
	else
	{
		if(pApp->m_bPause)
		{
			pause(L);
		}


/*		if(pApp->m_bPause)
		{
			lua_getglobal(L, "showtrace");
			lua_call(L, 0, 1);
		}
		while(pApp->m_bPause)
		{
			Sleep(50);
		}
*/		lua_pushnumber(L, !pApp->m_bRun);
	}
	
	return 1;
}

static int error_occuredEx(lua_State *L)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	pApp->LogStatus("Error occured in the script execution. Script stop triggered.");
	pApp->m_bRun = false;

	return 1;

}

static int BDPushSettings(lua_State *L)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	char ch[1500];

	int nParam = lua_gettop(L);
	if(nParam!=3) //Need only two parameters, clean Lua stack and give error
	{
		for (int m=1; m<nParam-1;)
		{
			strcpy_s(ch, lua_tostring(L, m));
		}
		pApp->LogStatus("Required parameters not provided for 'BDPushSettings'.");
		return 1;
	}
	
	strcpy_s(ch, lua_tostring(L, 1));
	CString strSettings(ch);

	strcpy_s(ch, lua_tostring(L, 2));
	CString strHost(ch);

	strcpy_s(ch, lua_tostring(L, 3));
	CString strPort(ch);

	
	int n = pApp->BDPushSettingsToHost(strSettings, strHost, strPort);
	lua_pushnumber(L, n);
	LOG_MESSAGE(LM_INFO, "Sending settings returned %i",n);

	return 1;

}

static int enable_db_logging(lua_State *L)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	pApp->Enable_DB_Logging();

	return 1;
}

static int disable_db_logging(lua_State *L)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	pApp->Disable_DB_Logging();

	return 1;
}


static int gui_is_activated(lua_State *L)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	char ch[1500];
	int nParam = lua_gettop(L);
	if(nParam!=1) //Need only one parameters, clean Lua stack and give error
	{
		for (int m=1; m<nParam-1;)
		{
			strcpy_s(ch, lua_tostring(L, m));
		}
		pApp->LogStatus("Required parameters not provided for 'gui_is_activated'.");
		return 1;
	}
	
	strcpy_s(ch, lua_tostring(L, 1));
	
	lua_pushnumber(L, pApp->GUI_IsActivated(ch));

	return 1;

}


static int gui_is_blinking(lua_State *L)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	char ch[1500];
	int nParam = lua_gettop(L);
	if(nParam!=1) //Need only one parameters, clean Lua stack and give error
	{
		for (int m=1; m<nParam-1;)
		{
			strcpy_s(ch, lua_tostring(L, m));
		}
		pApp->LogStatus("Required parameters not provided for 'gui_is_blinking'.");
		return 1;
	}
	
	strcpy_s(ch, lua_tostring(L, 1));
	
	lua_pushnumber(L, pApp->GUI_IsBlinking(ch));

	return 1;

}

static int gui_is_adornment(lua_State *L)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	char ch[1500];
	int nParam = lua_gettop(L);
	if(nParam!=1) //Need only one parameters, clean Lua stack and give error
	{
		for (int m=1; m<nParam-1;)
		{
			strcpy_s(ch, lua_tostring(L, m));
		}
		pApp->LogStatus("Required parameters not provided for 'gui_is_adornment'.");
		return 1;
	}
	
	strcpy_s(ch, lua_tostring(L, 1));
	
	lua_pushnumber(L, pApp->GUI_IsAdornment(ch));

	return 1;

}
static int gui_is_selected(lua_State *L)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	char ch[1500];
	int nParam = lua_gettop(L);
	if(nParam!=1) //Need only one parameters, clean Lua stack and give error
	{
		for (int m=1; m<nParam-1;)
		{
			strcpy_s(ch, lua_tostring(L, m));
		}
		pApp->LogStatus("Required parameters not provided for 'gui_is_selected'.");
		return 1;
	}
	
	strcpy_s(ch, lua_tostring(L, 1));
	
	lua_pushnumber(L, pApp->GUI_IsSelected(ch));

	return 1;

}
static int get_breath_phase(lua_State *L)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();

	lua_pushstring(L, pApp->m_CurrentBreathData.m_chBreathPhase);

	return 1;

/*
"EXHALATION";
"INSPIRATION";
"NULL_INSPIRATION";
"EXPIRATORY_PAUSE";
"INSPIRATORY_PAUSE";
"BILEVEL_PAUSE_PHASE";
"PAV_INSPIRATORY_PAUSE";
"NIF_PAUSE";
"P100_PAUSE";
"VITAL_CAPACITY_INSP";
"NON_BREATHING"; */

}

static int get_breath_type(lua_State *L)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	if(pApp->m_CurrentBreathData.m_chBreathType == NULL)
	{
		lua_pushstring(L, "error");
	}
	else
	{
		lua_pushstring(L, pApp->m_CurrentBreathData.m_chBreathType);
	}

	return 1;
}

static int get_breath_duration(lua_State *L)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	lua_pushnumber(L, pApp->m_CurrentBreathData.m_fBreathDuration);
	return 1;
}

static int is_autozero_active(lua_State *L)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	lua_pushnumber(L, pApp->m_CurrentWaveformData.n_isAutozeroActive);
	return 1;
}

static int get_breath_ie(lua_State *L)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	lua_pushnumber(L, pApp->m_CurrentBreathData.m_fIERatio);
	return 1;
}

static int get_focus_button(lua_State *L)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();

	char ch[500];
	if(pApp->GetFocusButtonID(ch))
	{
		lua_pushstring(L, ch);
	}
	else
	{
		lua_pushstring(L, "ERROR");

	}
	
	return 1;
	
}

static int include(lua_State *L)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	if(!pApp->m_bRun) return 1;

	CString str(lua_tostring(L, 1));

	pApp->LogStatus("Loading: "+ str, REPORT_RESULT_FOOTER);
	if(pApp->m_strRecordSWinReport=="true")
	{
		pApp->AddFileToRecordVersion(str);
	}
	
	if(luaL_dofile(pApp->m_L, str) == 1)
	{
		str.Format(">>>%s<<<", lua_tostring(pApp->m_L, -1));
		pApp->LogStatus(str);
		LOG_MESSAGE(LM_ERROR, "%s", str);
		pApp->LogStatus("Error occured loading file. Script stop triggered...", REPORT_ERROR);
		pApp->m_bRun = false;
	}

	return 1;
	
}

static int bezel_key_click(lua_State *L)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	char ch[500];
	char val2[500];
	strcpy_s(ch, lua_tostring(L, 1));
	strcpy_s(val2, lua_tostring(L, 2));

	lua_pushnumber(L, pApp->GUI_Bezel_Key_Click(ch, atoi(val2)));

	
	return 1;
	
}

//Opens a dialogbox to ask tester to verify manually and pass/fail the test case. 
//times out in 30 seconds, skips the test case and moves with the script
static int verifyManually_Ex(lua_State *L)
{
	//Play sound
	//::PlaySound("chimes.wav",NULL,SND_FILENAME|SND_ASYNC);

	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	CString str1(lua_tostring(L, 1));

	CString str2(lua_tostring(L, 2));

	lua_pushstring(L, pApp->VerifyManually_Ex(str1, str2));
	/*	Pass = 1
	fail = 2
	*/

	return 1;
}



static int setting_capture_get_valueEx(lua_State *L)
{
	CString strSetting(lua_tostring(L, 1));

	CSwatApp* pApp = (CSwatApp*)AfxGetApp();

	CString strFromSettingCapture = pApp->m_strSettings0; //Latest settings
	for(int n = 1; n < 50; n++)
	{
		int nNewLine = strFromSettingCapture.Find('\n');
		if( nNewLine == -1) {
			lua_pushstring(L, "ERROR");
			return 1;
		}

		CString strLine = strFromSettingCapture.Left(nNewLine); //Display each line

		if(strLine.Find(strSetting) != -1)
		{
			CString strID, strParam, strType, strValue;
			int n = strLine.Find(',');
			strID = strLine.Left(n);
			strLine = strLine.Right(strLine.GetLength()-n-1);

			n = strLine.Find(',');
			strParam = strLine.Left(n);
			strLine = strLine.Right(strLine.GetLength()-n-1);

			n = strLine.Find(',');
			strType = strLine.Left(n);
			strValue = strLine.Right(strLine.GetLength()-n-1);
			
			lua_pushstring(L, strValue);

			return 1;
		}
		strFromSettingCapture = strFromSettingCapture.Right(strFromSettingCapture.GetLength()-nNewLine -1);// -1 for new line
	}

	lua_pushstring(L, "ERROR");

	return 1;
}

static int power_on_off_vent(lua_State *L)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	
	char ip[500];
	strcpy_s(ip, lua_tostring(L, 1));
	CString strIP(ip);

	lua_Integer nPort =  (lua_tointeger(L, 2));
	lua_Number bPower_on_or_off = (lua_tonumber(L, 3));

	

	pApp->Power_on_off_vent("script", nPort, bPower_on_or_off ? true : false);

	return 1;
}

//get the setting last sent from GUI to BD
static int get_sent_settingEx(lua_State *L)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	char cmd[500];
	strcpy_s(cmd, lua_tostring(L, 1));
	CString strParameter(cmd);

	lua_pushstring(L, pApp->Get_Sent_Setting(strParameter));

	return 1;
		
}

//Trigger screen capture in the vent with given filename
static int gui_capture_screen(lua_State *L)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	char szFileName[500];
	strcpy_s(szFileName, lua_tostring(L, 1));
	CString strFileName(szFileName);

	lua_pushnumber(L, pApp->GUI_CaptureScreen(strFileName));

	return 1;
		
}

//Get number of files in vent GUI buffer
static int gui_get_screen_capture_status(lua_State *L)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	
	lua_pushnumber(L, pApp->GUI_CaptureScreenStatus());

	return 1;
		
}

//plays wave file   c:\swat\swat.wav
static int sound(lua_State *L)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	char strPath[500];
	strcpy_s(strPath, lua_tostring(L, 1));

	pApp->PlayWaveFile(strPath);
	return 1;
}



static int testLua(lua_State *L)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();

	char ch[500];
	strcpy_s(ch, lua_tostring(L, 1));
	CString str1(ch);

	strcpy_s(ch, lua_tostring(L, 2));
	CString str2(ch);

	
	pApp->TestLua(str1, str2);	

	return 1;
}

static int getuserinput(lua_State *L)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	char title[500];
	strcpy_s(title, lua_tostring(L, 1));

	lua_pushstring(L, pApp->GetUserInput(title));
	return 1;
}

static int ping_vent(lua_State *L)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	char showError[500];
	strcpy_s(showError, lua_tostring(L, 1));
	BOOL b = (BOOL)atoi(showError);
	lua_pushnumber(L, pApp->PingVent(b));
	return 1;
}


static int gui_simulate_alarm(lua_State *L)
{

	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	char ch[1500];
	
	strcpy_s(ch, lua_tostring(L, 1));
	CString strAlarmId(ch);

	strcpy_s(ch, lua_tostring(L, 2));
	CString strCondAugOptions(ch);

	strcpy_s(ch, lua_tostring(L, 3));
	CString strDepenAugOptions(ch);

	
	int n = pApp->GUI_SimulateBDAlarm(strAlarmId, strCondAugOptions, strDepenAugOptions);
	lua_pushnumber(L, n);
	LOG_MESSAGE(LM_INFO, "Sending settings returned %i",n);

	return 1;


}
//turns on the vent and runs macro to hit 's' for service mode
static int turn_on_hit_s(lua_State *L)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	int n = pApp->TurnOnHitS(true);
	lua_pushnumber(L, n);
	return 1;
}


//turns on off the water mark
static int watermarkonoff(lua_State *L)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	char ch[50];
	
	strcpy_s(ch, lua_tostring(L, 1));
	int n = atoi(ch);
	pApp->WaterMarkOnOff(n);

	return 1;
}
//set altia widget's properties
static int setwidgetproperties(lua_State *L)
{

	CSwatApp* pApp = (CSwatApp*)AfxGetApp();


	pApp->SetWidgetProperties(lua_tostring(L, 1), static_cast<int>(lua_tonumber(L, 2)));

	return 1;
}

//turns on off the mose display
static int mouseonoff(lua_State *L)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	char ch[50];
	
	strcpy_s(ch, lua_tostring(L, 1));
	int n = atoi(ch);
	pApp->MouseOnOff(n);

	return 1;
}




//One image or two with one in English
static int set_report_for_language_comparison(lua_State *L)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	char ch[50];
	
	strcpy_s(ch, lua_tostring(L, 1));
	int n = atoi(ch);
	pApp->m_bTwoImagesForComparison = (BOOL)n;

	return 1;
}

//One image or two with one in English
static int get_count_png_to_copy(lua_State *L)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	int n = pApp->m_listCapturedImages.GetCount();

	lua_pushnumber(L, n);

	return 1;
}


static int get_vent_language(lua_State *L)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
		
	lua_pushstring(L, pApp->GetVentLanguage());
	return 1;
}

