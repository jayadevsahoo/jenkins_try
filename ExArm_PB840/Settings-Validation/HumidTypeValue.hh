
#ifndef HumidTypeValue_HH
#define HumidTypeValue_HH

//====================================================================
//
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  HumidTypeValue - Values of the Humidification Type Setting.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/HumidTypeValue.hhv   25.0.4.0   19 Nov 2013 14:27:26   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//		Initial version.
//  
//====================================================================

//@ Usage-Classes
//@ End-Usage

struct HumidTypeValue
{
  //@ Type:  HumidTypeValueId
  // All of the possible values of the Humidification Type Setting.
  enum HumidTypeValueId
  {
    // $[02153] -- values of the range of Humidifier Type Setting...
    NON_HEATED_TUBING_HUMIDIFIER,
    HEATED_TUBING_HUMIDIFIER,
    HME_HUMIDIFIER,

    TOTAL_HUMIDIFIER_TYPES  
  };
};


#endif // HumidTypeValue_HH 
