
#ifndef IdealBodyWeightSetting_HH
#define IdealBodyWeightSetting_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  IdealBodyWeightSetting - Ideal Body weight setting 
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/IdealBodyWeightSetting.hhv   25.0.4.0   19 Nov 2013 14:27:26   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005   By: gdc    Date: 11-Aug-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support changing IBW while ventilating.
//
//  Revision: 004   By: sah    Date: 29-Jun-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  modified to use newly-modified 'BoundedRange' class to handle
//         range, via linked list of interval nodes
//      *  incorporated new circuit-based soft-limits, new-patient values
//         and dynamic ranges
//
//  Revision: 003   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'getApplicability()' method
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Initial version.
//
//====================================================================

//@ Usage-Classes
#include "BatchBoundedSetting.hh"
#include "SettingObserver.hh"
//@ End-Usage


class IdealBodyWeightSetting : public BatchBoundedSetting, public SettingObserver
{
	public:
		IdealBodyWeightSetting(void); 
		virtual ~IdealBodyWeightSetting(void);

		virtual Applicability::Id  getApplicability(
												   const Notification::ChangeQualifier qualifierId
												   ) const;

		// BatchBoundedSetting virtual
		virtual Boolean isChanged(void) const;

		virtual SettingValue  getNewPatientValue(void) const;

		// SettingObserver methods...
		virtual void  valueUpdate(const Notification::ChangeQualifier qualifierId,
								  const SettingSubject*               pSubject);

		virtual Boolean  doRetainAttachment(const SettingSubject* pSubject) const;

		virtual void  settingObserverInit(void);

		static void  SoftFault(const SoftFaultID softFaultID,
							   const Uint32      lineNumber,
							   const char*       pFileName  = NULL, 
							   const char*       pPredicate = NULL);

	protected:
		virtual void  updateConstraints_(void);

		virtual void  findSoftMinMaxValues_(Real32& rSoftMinValue,
											Real32& rSoftMaxValue) const;

		virtual Real32  getAbsoluteMaxValue_(void) const;
		virtual Real32  getAbsoluteMinValue_(void) const;

	private:
		// not implemented...
		IdealBodyWeightSetting(const IdealBodyWeightSetting&);
		void  operator=(const IdealBodyWeightSetting&);

		void  updateMaxConstraint_(void);
		void  updateMinConstraint_(void);

		//@ Data-Member:  cctTypeBasedSoftMax_
		Real32  cctTypeBasedSoftMax_;

		//@ Data-Member:  cctTypeBasedSoftMin_
		Real32  cctTypeBasedSoftMin_;
};


#endif // IdealBodyWeightSetting_HH 
