#ifndef VitalPatientDataArea_HH
#define VitalPatientDataArea_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-2007, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: VitalPatientDataArea - Upper screen area for vital pt data and misc messages.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/VitalPatientDataArea.hhv   25.0.4.0   19 Nov 2013 14:08:44   pvcs  $
//
//@ Modification-Log
// 
//  Revision: 012   By: rhj    Date: 30-Mar-2011    SCR Number: 6746  
//  Project: PROX
//      Added isVentNotInStartupOrWaitForPT method.
// 
//  Revision: 011   By: rhj    Date: 10-Sept-2010    SCR Number: 6631
//  Project: PROX
//  Description:
//      Added getIsProxReady().
//
//  Revision: 010   By: mnr    Date: 24-Feb-2010    SCR Number: 6436  
//  Project: PROX
//  Description:
//      Added accessor method for isProxInop_.
//
//  Revision: 009   By: rhj   Date: 17-Feb-2010   SCR Number: 6436
//  Project:  PROX
//  Description:
//      Prox project related changes. Added wye symbols for each volume 
//      label.
//
//  Revision: 008   By: gdc    Date: 24-Sep-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal non-invasive CPAP.
//
//  Revision: 007  By:  gdc	   Date:  25-Jan-2007    DCS Number: 6237
//  Project:  TREND
//  Description:
//      Trend project related changes. Changed to a LargeContainer from
//      old Container class.
//
//  Revision: 006   By: gdc    Date:  20-Jun-2007    SCR Number: 6144
//  Project:  NeoMode
//  Description:
//      Display Vti instead of PEEP during NIV.
//
//  Revision: 005   By: dph    Date:  04-Apr-2000    DCS Number: 5691
//  Project:  NeoMode
//  Description:
//      Added PEEP to vital patient data area.
//
//  Revision: 004  By:  yyy	   Date:  29-Jun-1998    DCS Number: 5327
//  Project:  NeoMode
//  Description:
//		Initial version.
//
//  Revision: 003  By: yyy      Date: 05-Jan-1998  DR Number: 
//    Project:  BiLevel (R8027)
//    Description:
//     Initial release for BiLevel adding software option.
//     Merged /840/Baseline/GUI-Applications/vcssrc/VitalPatientDataArea.hhv   1.18.1.0   07/30/98 10:22:46   gdc
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  wks    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "BdEventTarget.hh"
#include "BreathPhaseType.hh"
#include "BreathType.hh"
#include "Container.hh"
#include "LargeContainer.hh"
#include "GuiEventTarget.hh"
#include "GuiTimerTarget.hh"
#include "PatientDataTarget.hh"
#include "ContextObserver.hh"

//@ Usage-Classes
#include "Box.hh"
#include "Line.hh"
#include "TimeStamp.hh"
#include "NumericField.hh"
#include "TextField.hh"
#include "TouchableText.hh"
#include "GuiAppClassIds.hh"
class BreathDatumHandle;
//@ End-Usage

class VitalPatientDataArea : public Container,
							 public BdEventTarget,
							 public GuiEventTarget,
							 public GuiTimerTarget,
							 public PatientDataTarget,
							 public ContextObserver
{
public:
	VitalPatientDataArea(void);
	~VitalPatientDataArea(void);

	//@ Type: VpdaDisplayMode
	// Identifies the current/new display mode for this area.
	enum VpdaDisplayMode
	{
		MODE_NORMAL = 0,
		MODE_VENT_STARTUP,
		MODE_WAITING_FOR_PT,
		MODE_NO_VENT,
		MODE_MALFUNCTION,
		MODE_INOP,
		MODE_SERVICE_REQUIRED,
		MODE_SVO,
		MODE_INOP_NO_VENT,
		MAX_MODE
	};

	void setDisplayMode(VpdaDisplayMode displayMode);
	inline VpdaDisplayMode getDisplayMode(void);
	inline Boolean getIsProxInop(void);
    inline Boolean getIsProxReady(void);
    inline Boolean isVentNotInStartupOrWaitForPT(void);

	virtual void bdEventHappened(EventData::EventId eventId,
			                	EventData::EventStatus eventStatus,
								EventData::EventPrompt eventPrompt=EventData::NULL_EVENT_PROMPT);
	virtual void guiEventHappened(GuiApp::GuiAppEvent eventId);
	virtual void timerEventHappened(GuiTimerId::GuiTimerIdType timerId);
	virtual void patientDataChangeHappened(
								PatientDataId::PatientItemId patientDataId);

	// ContextObserver virtual method...
	virtual void  batchSettingUpdate(
							 const Notification::ChangeQualifier qualifierId,
							 const ContextSubject*               pSubject,
							 const SettingId::SettingIdType      settingId
									);

	static void SoftFault(const SoftFaultID softFaultID,
					     const Uint32      lineNumber,
					     const char*       pFileName  = NULL, 
					     const char*       pPredicate = NULL);

protected:

private:
	// these methods are purposely declared, but not implemented...
	VitalPatientDataArea(const VitalPatientDataArea&);	// not implemented...
	void operator=(const VitalPatientDataArea&);		// not implemented...

	void setMode_(void);
	void updateNoVentDisplay_(void);
	void removeDataContainerDrawables_(void);
	void addDataContainerDrawables_(void);
	void updateBreathBar_(void);
	void updateDataComponents_(void);

	//@ Data-Member: dataContainer_
	// A container the size of VitalPatientDataArea that contains all the
	// drawables which display patient data.
	LargeContainer dataContainer_;

	//@ Data-Member: breathStatusBox_
	// Box for the breath status portion of the display.
	Box breathStatusBox_;

	//@ Data-Member: breathStatusText_
	// Text for the current breath status (CONTROL, ASSIST, SPONT).
	TouchableText breathStatusText_;

	//@ Data-Member: ieRatioLabel_
	// Label for I:E ratio field.
	TouchableText ieRatioLabel_;

	//@ Data-Member: ieRatioValue_
	// Value for I:E ratio field.
	TextField ieRatioValue_;

	//@ Data-Member: totalRespiratoryRateLabel_
	// Label for total respiratory rate field.
	TouchableText totalRespiratoryRateLabel_;

	//@ Data-Member: totalRespiratoryRateValue_
	// Value for total respiratory rate field.
	NumericField totalRespiratoryRateValue_;

	//@ Data-Member: endInspiratoryPressureLabel_
	// Label for end inspiratory pressure field.
	TouchableText endExhalationPressureLabel_;

	//@ Data-Member: endInspiratoryPressureValue_
	// Value for end inspiratory pressure field.
	NumericField endExhalationPressureValue_;

	//@ Data-Member: exhaledTidalVolumeLabel_
	// Label for exhaled tidal volume field.
	TouchableText exhaledTidalVolumeLabel_;

	//@ Data-Member: exhaledTidalVolumeValue_
	// Value for exhaled tidal volume field.
	NumericField exhaledTidalVolumeValue_;

	//@ Data-Member: exhaledMinuteVolumeLabel_
	// Label for exhaled minute volume field.
	TouchableText exhaledMinuteVolumeLabel_;

	//@ Data-Member: exhaledMinuteVolumeValue_
	// Value for exhaled minute volume field.
	NumericField exhaledMinuteVolumeValue_;

	//@ Data-Member:: peakCircPressureLabel_
	// label for the peak circuit pressure
	TouchableText peakCircPressureLabel_;

	//@ Data-Member:: peakCircPressureValue_
	// value for the peak circuit pressure
	NumericField peakCircPressureValue_;

	//@ Data-Member:: meanCircPressureLabel_
	// label for the end exhalation pressure
	TouchableText meanCircPressureLabel_;

	//@ Data-Member:: meanCircPressureValue_
	// value for the end exhalation pressure
	NumericField meanCircPressureValue_;

	//@ Data-Member:: deliveredVolumeLabel_
	// label for the inhalation volume (NIV)
	TouchableText deliveredVolumeLabel_;

	//@ Data-Member:: deliveredVolumeValue_
	// value for the inhalation volume (NIV)
	NumericField deliveredVolumeValue_;

	//@ Data-Member: bigMessage_
	// TextField used to display big messages such as those needed for
	// Vent Startup and No Ventilation.
	TextField bigMessage_;

	//@ Data-Member: noVentStartTime_
	// OS time (unaffected by changes in wall clock time) when
	// "No Vent" mode started.
	TimeStamp noVentStartTime_;

	//@ Data-Member: displayMode_
	// The display mode for the VitalPatientDataArea.
	VpdaDisplayMode displayMode_;

	//@ Data-Member: dividingLine_
	// The white line at the bottom of the vital patient data area
	Line dividingLine_;

	//@ Data-Member: currentBreathPhase_
	// Breath phase (eg. INHALATION,EXHALATION) from the latest patient 
	// data update
	BreathPhaseType::PhaseType currentBreathPhase_;

	//@ Data-Member: currentBreathType
	// Breath type (eg. SPONT,CONTROL,ASSIST) from the latest patient 
	// data update
	BreathType currentBreathType_;

	//@ Data-Member:: proximalDeliveredVolumeLabel_
	// Label for proximal delivered volume field.
	TouchableText proximalDeliveredVolumeLabel_;

	//@ Data-Member:: proximalDeliveredVolumeValue_
	// Value for proximal delivered volume field.
	NumericField proximalDeliveredVolumeValue_;

	//@ Data-Member: proximalExhaledTidalVolumeLabel_
	// Label for proximal Exhaled tidal volume field.
	TouchableText proximalExhaledTidalVolumeLabel_;

	//@ Data-Member: proximalExhaledTidalVolumeValue_
	// Value for proximal Exhaled tidal volume field.
	NumericField proximalExhaledTidalVolumeValue_;

	//@ Data-Member: proximalExhaledMinuteVolumeLabel_
	// Label for proximal Exhaled minute volume field.
	TouchableText proximalExhaledMinuteVolumeLabel_;

	//@ Data-Member: proximalExhaledMinuteVolumeValue_
	// Value for proximal Exhaled minute volume field.
	NumericField proximalExhaledMinuteVolumeValue_;

	//@ Data-Member: isProxInop_
	// Is proximal board inoperative?
	Boolean isProxInop_;

	//@ Data-Member: isProxReady_
	// Is proximal board ready?
	Boolean isProxReady_;

};

// Inlined methods
#include "VitalPatientDataArea.in"

#endif // VitalPatientDataArea_HH 
