
#if !defined(CallTrace_HH)  &&  defined(CALL_TRACE_ON)  &&  !defined(CALL_TRACE_OFF)
#define CallTrace_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sotred in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: CallTrace - provides call tracing -- for debug use only.
//---------------------------------------------------------------------
// Version
// @(#) $Header:   /840/Baseline/Utilities/vcssrc/CallTrace.hhv   25.0.4.0   19 Nov 2013 14:35:12   pvcs  $
//
// Modification-Log
//  
//  Revision: 001  By:  sah    Date:  07-15-93    DR Number: 
//	Project:  Sigma (R8027) 
//	Description:
//		Initial version (Integration baseline). 
//
//====================================================================

#include "SigmaTypes.hh"

// Usage-Classes
// End-Usage


class CallTrace {
  public:
    static void  Initialize(void);

    inline CallTrace (const char*  funcName,
		      const char*  fileName,
		      const Uint32 lineNumber);
    inline ~CallTrace(void);

    static void  ChangeVerboseMode(const Boolean newVerboseFlag);
    static void  DumpTrace(void);
    static void  InsertMarker(const char* pMarkerText);

  private: 
    CallTrace(const CallTrace&);		// not implemented...
    CallTrace(void);				// not implemented...
    void  operator=(const CallTrace&);		// not implemented...

    void  processEntry_(const char* fileName, const Uint32 lineNumber);
    void  processExit_ (void);

    // Constant:  FUNC_NAME_
    // This is a constant pointer to a constant string that represents
    // the function name.
    const char* const  FUNC_NAME_;

    // Data-Member:  pNext_
    // This pointer is used to keep a linked-list of call-trace instances
    // so that non-verbose call-tracing can print out a call-stack, on
    // request...
    const CallTrace*  pNext_;

    // Data-Member:  PLast_
    // This pointer points to the last call-trace instance that was
    // instantiated...
    static const CallTrace*  PLast_;

    // Data-Member:  IsVerboseMode_
    // This is a static boolean that indicates whether the processing is
    // in verbose mode, or not...
    static Boolean  IsVerboseMode_;

    // Data-Member:  Depth_
    // This is the static member that keeps track of the depth of the
    // traced call stack.
    static Uint32  Depth_; 
};


// Inlined Methods...
#include "CallTrace.in"


#endif // !defined(CallTrace_HH)  &&  defined(CALL_TRACE_ON)  &&  !defined(CALL_TRACE_OFF)
