// SwatView.cpp : implementation of the CSwatView class
//

#include "stdafx.h"
#include "Swat.h"
#include "SwatView.h"
#include "MainFrm.h"

#include "mmsystem.h"
#include "MultiTimeScptExecDlg.h"
#include "ViewStoredResultsDlg.h"

#include "TabStatusDlg.h"

#include "PictureEx.h"

#include "GuiIOServer.h"

#include "CheckLoop.h"

#include "FileOperations.h"

#define OPEN_AUTO 857

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CSwatView

IMPLEMENT_DYNCREATE(CSwatView, CFormView)

BEGIN_MESSAGE_MAP(CSwatView, CFormView)
	ON_COMMAND(ID_VTREE_RUNSELECTEDFILES, &CSwatView::OnVtreeRunselectedfiles)
	ON_BN_CLICKED(IDC_BUTTON_MOVE_TOLIST, &CSwatView::OnBnClickedButtonMoveTolist)
	ON_BN_CLICKED(IDC_BUTTON_UP, &CSwatView::OnBnClickedButtonUp)
	ON_WM_TIMER()
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_CONTROL, &CSwatView::OnNMDblclkListControl)
	ON_COMMAND(ID_VTREE_STOP, &CSwatView::OnVtreeStop)
	ON_NOTIFY(NM_CLICK, IDC_TREE, &CSwatView::OnNMClickTree)
	ON_NOTIFY(TVN_KEYDOWN, IDC_TREE, &CSwatView::OnTvnKeydownTree)
	ON_BN_CLICKED(IDC_CHECK_AUTO_UPDATE, &CSwatView::OnBnClickedCheckAutoUpdate)
	ON_BN_CLICKED(IDC_BUTTON_CLEAR, &CSwatView::OnBnClickedButtonClear)
	ON_NOTIFY(NM_RCLICK, IDC_LIST_CONTROL, &CSwatView::OnNMRClickListControl)
	ON_NOTIFY(NM_RDBLCLK, IDC_LIST_CONTROL, &CSwatView::OnNMRDblclkListControl)
	ON_UPDATE_COMMAND_UI(ID_VTREE_RUNSELECTEDFILES, &CSwatView::OnUpdateVtreeRunselectedfiles)
	ON_UPDATE_COMMAND_UI(ID_VTREE_STOP, &CSwatView::OnUpdateVtreeStop)
	ON_WM_CLOSE()
	ON_NOTIFY(NM_DBLCLK, IDC_TREE, &CSwatView::OnNMDblclkTree)
	ON_COMMAND(ID_SWAT_RUNMULTIPLETIMES, &CSwatView::OnSwatRunmultipletimes)
	ON_COMMAND(ID_SWATSETTINGS_INIT, &CSwatView::OnSwatsettingsInit)
	ON_COMMAND(ID_SWATSETTINGS_SQL_LIB, &CSwatView::OnSwatsettingsSQL_lib)
	ON_COMMAND(ID_SWAT_VIEWSTOREDRESULTS, &CSwatView::OnSwatViewstoredresults)
	ON_NOTIFY(NM_RCLICK, IDC_TREE, &CSwatView::OnNMRClickTree)
	ON_COMMAND(ID_POP_UP_EXPLORE, &CSwatView::OnPopUpExplore)
	ON_COMMAND(ID_POP_UP_REFRESH_TREE, &CSwatView::OnPopUpRefreshTree)
	ON_COMMAND(ID_SWATSETTINGS_PCAPP, &CSwatView::OnSwatsettingsPcapp)
	ON_COMMAND(ID_POP_UP_EDIT_SCRIPT, &CSwatView::OnPopUpEditScript)
	ON_UPDATE_COMMAND_UI(ID_SWAT_RUNMULTIPLETIMES, &CSwatView::OnUpdateSwatRunmultipletimes)
	ON_COMMAND(ID_SWAT_RECORD, &CSwatView::OnSwatRecord)
	ON_UPDATE_COMMAND_UI(ID_SWAT_RECORD, &CSwatView::OnUpdateSwatRecord)
	ON_COMMAND(ID_OPEN_LAST_RESULT_LOG, &CSwatView::OnOpenLastResultLog)
	ON_COMMAND(ID_POP_UP_CHANGE_SCRIPTPATH, &CSwatView::OnPopUpChangeScriptpath)
	ON_COMMAND(ID_SWAT_PAUSE, &CSwatView::OnSwatPause)
	ON_UPDATE_COMMAND_UI(ID_SWAT_PAUSE, &CSwatView::OnUpdateSwatPause)
	ON_NOTIFY(LVN_KEYDOWN, IDC_LIST_CONTROL, &CSwatView::OnLvnKeydownListControl)
	ON_COMMAND(ID__RUNONLYTHISSCRIPT, &CSwatView::OnPopUpRunOnlyThisOne)
END_MESSAGE_MAP()

// CSwatView construction/destruction

CSwatView::CSwatView()
	: CFormView(CSwatView::IDD)
	, m_strScript(_T(""))
	, m_strTestExecStatus(_T(""))
	, m_strStatusString(_T(""))
	, m_bAutoScroll(TRUE)
	, m_strSWATLog(_T(""))
{

	m_pLuaThread = NULL;
	m_pStatusDlg = NULL;
	m_pLogDlg = NULL;
	m_pPowerDlg = NULL;
	
}

CSwatView::~CSwatView()
{
}

void CSwatView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TREE, m_TreeCtrl);
	DDX_Control(pDX, IDC_PROGRESS, m_progressbar);
	DDX_Control(pDX, IDC_LIST_CONTROL, m_List);
	DDX_Text(pDX, IDC_EDIT_STATUS, m_strTestExecStatus);
	DDX_Text(pDX, IDC_EDIT_STATUS_STR, m_strStatusString);
	DDX_Check(pDX, IDC_CHECK_AUTO_UPDATE, m_bAutoScroll);
	DDX_Control(pDX, IDC_TAB_CTRL, m_Tab);
	DDX_Control(pDX, IDC_TAB_CTRL, m_Tab);
	DDX_Text(pDX, IDC_EDIT_LOG, m_strSWATLog);
	DDX_Control(pDX, IDC_MYPIC, m_Picture);
}

BOOL CSwatView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CFormView::PreCreateWindow(cs);
}

void CSwatView::OnInitialUpdate()
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();

	CFormView::OnInitialUpdate();
	GetParentFrame()->RecalcLayout();
	ResizeParentToFit();

	//setting veiw pointer in fram to call for WM messages
	((CMainFrame *)AfxGetMainWnd())->m_pMainView = this;

	//Title
	(AfxGetMainWnd( ))->SetWindowText(SWAT_APPLICATION_TITLE);

	//Populate the tree control 
	UpdateTree(pApp->m_strTestScriptPath);
	InsertColumns();

	
	pApp->m_bScriptRunning = false;

	m_List.SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

	UpdateData(0);


	
    m_Tab.InitDialogs();
    m_Tab.InsertItem(0,"Test Execution Status");
    m_Tab.InsertItem(1,"SWAT Logging");
	m_Tab.InsertItem(2,"Vent Power");

	m_Tab.ActivateTabDialogs();

	m_pStatusDlg = (CTabStatusDlg*)(m_Tab.m_Dialog[0]);
	m_pLogDlg	 = (CTabLogDlg*)(m_Tab.m_Dialog[1]);
	m_pPowerDlg	 = (CTabPowerDlg*)(m_Tab.m_Dialog[2]);


//		SetTimer(LAOD_FILES_FOR_END2END, 500, NULL);
		


	//if started from command promp, set timer to open list of scripts when GUI  is up
	if(pApp->m_lpCmdLine[0] != _T('\0'))
	{	
		SetTimer(OPEN_AUTO, 3000, NULL);
	}













}


// CSwatView diagnostics

#ifdef _DEBUG
void CSwatView::AssertValid() const
{
	CFormView::AssertValid();
}

void CSwatView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CSwatDoc* CSwatView::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CSwatDoc)));
	return (CSwatDoc*)m_pDocument;
}
#endif //_DEBUG


void CSwatView::UpdateTree(CString str)
{
	LOG_MESSAGE(LM_INFO,"Entering %s with path %s", __FUNCTION__, str);

	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	
	TVINSERTSTRUCT tvInsert;
	tvInsert.hParent = NULL;
	tvInsert.hInsertAfter = NULL;
	tvInsert.item.mask = TVIF_TEXT;
	tvInsert.item.pszText = _T(pApp->m_strTestScriptPath.GetBuffer());
	pApp->m_strTestScriptPath.ReleaseBuffer();

	HTREEITEM hRoot = m_TreeCtrl.InsertItem(&tvInsert);
	Recurse(pApp->m_strTestScriptPath, hRoot);
	m_TreeCtrl.Expand(m_TreeCtrl.GetFirstVisibleItem(),TVE_EXPAND);
	m_TreeCtrl.SortChildren(TVI_ROOT);

}


void CSwatView::Recurse(LPCTSTR pstr, HTREEITEM hParentTree)
{
	m_TreeCtrl.SortChildren(hParentTree);
   CFileFind finder;
   // build a string with wildcards
   CString strWildcard(pstr);
   strWildcard += _T("\\*.*");

   // start working for files
   BOOL bWorking = finder.FindFile(strWildcard);

   while (bWorking)
   {
      bWorking = finder.FindNextFile();

      // skip . and .. files; otherwise, we'd
      // recur infinitely!
      if (finder.IsDots())
	  {
		 CString str = finder.GetFilePath();
         continue;
	  }
	  if (finder.IsNormal())
	  {
 		 CString str = finder.GetFilePath();
		 if(str.Right(4).CompareNoCase(".lua") == 0)
		 {
			m_TreeCtrl.InsertItem(finder.GetFilePath(), hParentTree, TVI_SORT);
		 }
		 continue;
	  }

	  // if it's a directory, recursively search it
      if (finder.IsDirectory())
      {
         CString str = finder.GetFileName();
		 HTREEITEM hFolder;
		 hFolder = m_TreeCtrl.InsertItem(TVIF_TEXT, str, 0, 0, 0, 0, 0, hParentTree, NULL);
		 m_TreeCtrl.SortChildren(hParentTree);
		 Recurse(finder.GetFilePath(), hFolder);
      }
	  else
	  {
		 CString str = finder.GetFilePath();
		 if(str.Right(4).CompareNoCase(".lua") == 0)
		 {
			m_TreeCtrl.InsertItem(finder.GetFilePath(), hParentTree, TVI_SORT);
			m_TreeCtrl.SortChildren(hParentTree);
		 }
		 continue;
	  }
   }
   finder.Close();
}



//Copies the slected lua extension files from tree control to the list box
void CSwatView::OnBnClickedButtonMoveTolist()
{
	//clear the list control first
	m_List.DeleteAllItems();
	HTREEITEM hItem = m_TreeCtrl.GetRootItem();
	RecurseReadTree(hItem , m_TreeCtrl.GetItemText(hItem));
}




void CSwatView::RecurseReadTree(HTREEITEM hParentTree, CString str){
	HTREEITEM hCurrent;

	if (hParentTree==NULL) return;

	for(hCurrent=hParentTree;hCurrent!=NULL;hCurrent=m_TreeCtrl.GetNextSiblingItem(hCurrent))
	{
		if (m_TreeCtrl.ItemHasChildren(hCurrent))
		{
//			m_TreeCtrl.Expand(hCurrent,TVE_EXPAND);
			HTREEITEM hChild = m_TreeCtrl.GetChildItem(hCurrent);
			RecurseReadTree(hChild, m_TreeCtrl.GetItemText(hChild));
		}
		else {
			if(m_TreeCtrl.GetCheck(hCurrent))
			{
				CString str = m_TreeCtrl.GetItemText(hCurrent);
				if(str.Right(4).CompareNoCase(".lua")==0)
					AddListEntry(str);
			}
		}

	}
}




void CSwatView::InsertColumns()
{
	LV_COLUMN Column;
	int nCol;

	Column.mask = LVCF_TEXT|LVCF_WIDTH ;
	CString szText;
	
	szText = "Test Scripts";
	Column.pszText = (TCHAR*)(LPCTSTR)szText;
	Column.cchTextMax = szText.GetLength();
	
	Column.cx = 320;
	nCol = m_List.InsertColumn( 0, &Column );


	Column.mask = LVCF_TEXT|LVCF_WIDTH ;
	
	szText = "Pass/Fail/Total";
	Column.pszText = (TCHAR*)(LPCTSTR)szText;
	Column.cchTextMax = szText.GetLength();
	
	Column.cx = 90;
	nCol = m_List.InsertColumn( 1, &Column );

	
	Column.mask = LVCF_TEXT|LVCF_WIDTH ;
	szText = "W/Errors";
	Column.pszText = (TCHAR*)(LPCTSTR)szText;
	Column.cchTextMax = szText.GetLength();
	
	Column.cx = 55;
	nCol = m_List.InsertColumn( 2, &Column );

}
void CSwatView::OnBnClickedButtonUp()
{
	//to be implemented
	
}



void CSwatView::AddListEntry(CString str)
{
	str.TrimRight();
	if(str.IsEmpty())
		return;
	if(str.Right(4).CompareNoCase(".lua") != 0)
		return;

	LV_ITEM lvi;
	lvi.mask = LVIF_TEXT | LVIF_PARAM;
	lvi.iItem = m_List.GetItemCount();
	lvi.pszText = (LPSTR)(LPCTSTR)str;
	lvi.iSubItem = 0;
	int nIndex = m_List.InsertItem( &lvi );
	m_List.EnsureVisible(nIndex, TRUE);
}


void CSwatView::AddListResult(void)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	LVFINDINFO FindInfo;
	FindInfo.flags = LVFI_PARTIAL|LVFI_STRING;
	FindInfo.psz = pApp->m_ScriptResults.m_strFileName;
	int nIndex = m_List.FindItem(&FindInfo);
	
	CString strResult, strWarning;
	strResult.Format("%i/%i/%i", pApp->m_ScriptResults.m_nPass, 
									pApp->m_ScriptResults.m_nFail, 
									pApp->m_ScriptResults.m_nTotal);
	m_List.SetItem(nIndex, 1, LVIF_TEXT, strResult, 0, 0, 0, 0);

//	if(pApp->m_ScriptResults.m_nWarnings + pApp->m_ScriptResults.m_nErrors >0 )
	{

//		strWarning.Format("%i", pApp->m_ScriptResults.m_nWarnings+
//								pApp->m_ScriptResults.m_nErrors);

		pApp->m_ScriptResults.m_timeStop = CTime::GetCurrentTime();
		CTimeSpan timeSpan = pApp->m_ScriptResults.m_timeStop - pApp->m_ScriptResults.m_timeStart;
		strWarning.Format("%i (%s)", pApp->m_ScriptResults.m_nWarnings+
								pApp->m_ScriptResults.m_nErrors,
								timeSpan.Format("%H:%M:%S"));

		m_List.SetItem(nIndex, 2, LVIF_TEXT, strWarning, 0, 0, 0, 0);
	}

}

void CSwatView::ClearListResult(void)
{
	LOG_MESSAGE(LM_INFO,"Enter %s", __FUNCTION__);
	int nCount = m_List.GetItemCount();

	for(int nIndex = 0; nIndex < nCount; nIndex++)
	{
		m_List.SetItem(nIndex, 1, LVIF_TEXT, "", 0, 0, 0, 0);
		m_List.SetItem(nIndex, 2, LVIF_TEXT, "", 0, 0, 0, 0);
	}
	LOG_MESSAGE(LM_INFO,"Exit %s", __FUNCTION__);
}



CString CSwatView::GetScriptNameToRun(void)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();

	CString strScriptName;
	int nCount = m_List.GetItemCount();

	//return the first string in list which has no results yet
	for(int n = 0; n<nCount; n++)
	{
		CString str = m_List.GetItemText(n,1);
		if(str.IsEmpty())
		return m_List.GetItemText(n,0);
	}
	return "";

}

void CSwatView::OnVtreeRunselectedfiles()
{
	//run once only
	Run_scripts_multi_times(1);
}


void CSwatView::OnSwatRunmultipletimes()
{
	CMultiTimeScptExecDlg dlg;
	dlg.m_nNumberTimes = 1;
	if(dlg.DoModal() == IDCANCEL)
		return;

	Run_scripts_multi_times(dlg.m_nNumberTimes);
}

bool CSwatView::Run_scripts_multi_times(int nTimes)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();

	//Don't run if a script already running
	if(pApp->m_bScriptRunning == true)
	{
		return 0;
	}

	int nCount = m_List.GetItemCount();
	if(nCount ==0) return false;//nothing to run

	if(pApp->m_bRecording)
	{
		OnVtreeStop();
	}

	if(pApp->PingVent(TRUE)==FALSE) 
	{
		pApp->m_bRun = false; //stop currently running script
		pApp->m_bUserStopped = true; //stop all following scripts in the loop
		pApp->m_bPause = false;
		return 0;
	}
	

	pApp->m_bRun = true;
	pApp->m_bScriptRunning = true;
	pApp->m_bUserStopped = false;

	m_strTestExecStatus.Empty();

	pApp->m_strArrayFiles.RemoveAll();


	for (int m = 0; m<nTimes; m++)
	{
		for (int n =0; n<nCount; n++)
		{
			pApp->m_strArrayFiles.Add(m_List.GetItemText(n,0));
		}
	}

	if(pApp->m_bRecording){
		GuiIOUIOutputOnOff(false);}//stop recording

	//Clear the status log in the window
	m_pStatusDlg->ClearTestStatus();

	//Clear the pass/fail/total in the script list
	ClearListResult();

	m_pLuaThread = AfxBeginThread((AFX_THREADPROC)Thread_Lua, (LPVOID)this);


	
	return true;
}


void CSwatView::OnUpdateVtreeRunselectedfiles(CCmdUI *pCmdUI)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	pCmdUI->Enable(!pApp->m_bScriptRunning && !pApp->m_bRecording);
}

void CSwatView::OnUpdateSwatRunmultipletimes(CCmdUI *pCmdUI)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	//pCmdUI->Enable(!pApp->m_bScriptRunning && !pApp->m_bRecording);
	pCmdUI->Enable(0);
}

void CSwatView::OnUpdateVtreeStop(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	pCmdUI->Enable(pApp->m_bScriptRunning || pApp->m_bRecording);

	GetDlgItem(IDC_BUTTON_MOVE_TOLIST)->EnableWindow(!pApp->m_bScriptRunning && !pApp->m_bRecording);

	
}

void CSwatView::OnTimer(UINT_PTR nIDEvent)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	if(pApp->m_bRecording == true)//update here only if recording. 
	{
		if(nIDEvent == 111)
		{
			GetDlgItem(IDC_EDIT_STATUS_STR)->SetWindowTextA("Recording...");
		}
		else if(nIDEvent == 222)
		{
			GetDlgItem(IDC_EDIT_STATUS_STR)->SetWindowTextA("   ");
		}
	}

	//SWAT was opened thru cmd line, now load the scripts to run
	if(nIDEvent == OPEN_AUTO)
	{
		KillTimer(OPEN_AUTO);
		OpenScriptFile(pApp->m_lpCmdLine);
		Run_scripts_multi_times(1);
		
	}


	CFormView::OnTimer(nIDEvent);
}

void CSwatView::OnNMDblclkListControl(NMHDR *pNMHDR, LRESULT *pResult)
{
//	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<NMITEMACTIVATE>(pNMHDR);
	// TODO: Add your control notification handler code here
	LPNMITEMACTIVATE lpnmitem = (LPNMITEMACTIVATE) pNMHDR;

	//return if no results for this test case yet
	if(m_List.GetItemText(lpnmitem->iItem,1).IsEmpty())return;


	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	CString str = m_List.GetItemText(lpnmitem->iItem,0);
	
	pApp->GenerateReport(str, pApp->m_strCurrentSessionID);

	*pResult = 0;
}

void CSwatView::OnNMRClickListControl(NMHDR *pNMHDR, LRESULT *pResult)
{
	//LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<NMITEMACTIVATE>(pNMHDR);
	LPNMITEMACTIVATE lpnmitem = (LPNMITEMACTIVATE) pNMHDR;

	*pResult = 0;
}

void CSwatView::OnNMRDblclkListControl(NMHDR *pNMHDR, LRESULT *pResult)
{
	CSwatApp *pApp = (CSwatApp *)AfxGetApp();

	LPNMITEMACTIVATE lpnmitem = (LPNMITEMACTIVATE) pNMHDR;
	//LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<NMITEMACTIVATE>(pNMHDR);
	// TODO: Add your control notification handler code here

	CString str = m_List.GetItemText(lpnmitem->iItem,0);
	if(str.IsEmpty())return;

	//if lua file, open it for edit
	if(str.Right(4).CompareNoCase(".lua")==0)
	{
		
		ShellExecute(0, "open", pApp->m_strEditorPath, "\"" + str+"\"", 0, SW_SHOW);

	}

	*pResult = 0;
}


void CSwatView::OnVtreeStop()
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	pApp->LogStatus("-->Stop triggered by the user. Please wait...");
	

	pApp->m_bRun = false; //stop currently running script

	

	pApp->m_bUserStopped = true; //stop all following scripts in the loop

	pApp->m_bPause = false;

	//clean the list of images to be copied from vent
	pApp->m_listCapturedImages.RemoveAll();


	if(pApp->m_bRecord == true) //if recording
	{
		pApp->m_bRecord = false; //stop recording and kill timer to show "recording"
		KillTimer(111);
		KillTimer(222);
		GetDlgItem(IDC_EDIT_STATUS_STR)->SetWindowTextA("   ");
	}

}

//Called when new SWAT log is received
void CSwatView::RefreshSWATLogWindow(CString& str)
{
	m_pLogDlg->UpdateSWATLog(str);
}

//Called when new test execution log is received
void CSwatView::RefreshStatusWindow(CString& pstr, int nMsgType)
{

	CSwatApp* pApp = (CSwatApp*)AfxGetApp();


	//select the currently running script in the list box
	LVFINDINFO FindInfo;
	FindInfo.flags = LVFI_PARTIAL|LVFI_STRING;
	FindInfo.psz = pApp->m_ScriptResults.m_strFileName;
	int nIndex = m_List.FindItem(&FindInfo);
	m_List.SetItem(nIndex, 0, LVIF_STATE, NULL, 0, LVIS_SELECTED, LVIS_SELECTED, 0);

	//set the progressbar position
	m_progressbar.SetRange(0, pApp->m_nTotalScripts);
	m_progressbar.SetPos(pApp->m_nCurentScriptNumber-1);   

	//create status text
	CString strSessionPassFail;
	strSessionPassFail.Format("Pass/fail/total : %d/%d/%d; WErrors : %d", pApp->m_SessionResults.m_nPass,
		pApp->m_SessionResults.m_nFail,
		pApp->m_SessionResults.m_nTotal,
		pApp->m_SessionResults.m_nWarning+
		pApp->m_SessionResults.m_nError);

	if(pApp->m_bScriptRunning == true)
		m_strStatusString.Format("Running %d of total %d scripts. %s",
									pApp->m_nCurentScriptNumber,
									pApp->m_nTotalScripts, 
									strSessionPassFail);
	else
	{	m_strStatusString.Format("Completed %d of total %d scripts. %s",
									pApp->m_nCurentScriptNumber,
									pApp->m_nTotalScripts,
									strSessionPassFail);
		if(pApp->m_nCurentScriptNumber == pApp->m_nTotalScripts)
		{
			m_progressbar.SetPos(pApp->m_nCurentScriptNumber);   
		}






	}
/*	if(pApp->m_SessionResults.m_nWarning > 0 || pApp->m_SessionResults.m_nError > 0)
	{
		CString strWarning;
		strWarning.Format(", Warnings: %d, Errors: %d",pApp->m_SessionResults.m_nWarning, pApp->m_SessionResults.m_nError);
		m_strStatusString = m_strStatusString + strWarning;
	}
*/
	if(pApp->m_nTotalScripts == 0) //nothing was run, no status...make it blank 
	{
		m_strStatusString = "    ";
	}
	if(pApp->m_bRecording == false)//update here only if not recording. If recording then OnTimer will update
	{
		GetDlgItem(IDC_EDIT_STATUS_STR)->SetWindowTextA(m_strStatusString);
	}

	//set tab control status
	if(m_pStatusDlg != NULL)
	{
		m_pStatusDlg->UpdateTestStatus(pstr, nMsgType);
	}

	GetDlgItem(IDC_EDIT_TEST_OBJECTIVE)->SetWindowTextA(pApp->m_strTestObjective);

	


}

void CSwatView::OnNMClickTree(NMHDR *pNMHDR, LRESULT *pResult)
{

	POINT pt;
    GetCursorPos(&pt);
    m_TreeCtrl.ScreenToClient(&pt);
    TVHITTESTINFO hti;
    hti.pt.x = pt.x;
    hti.pt.y = pt.y;
    m_TreeCtrl.HitTest(&hti);
    
	if (hti.flags & TVHT_ONITEMSTATEICON)
    {
		int bCheck = m_TreeCtrl.GetCheck(hti.hItem);

		if (m_TreeCtrl.ItemHasChildren(hti.hItem))
		{
			CheckTreeChildren(hti.hItem, !bCheck);
			*pResult = 1;
			return;
		}
	}

	*pResult = 0;
}

void CSwatView::CheckTreeChildren(HTREEITEM hParentTree, BOOL bCheck)
{
	//check box the parent
	m_TreeCtrl.SetCheck(hParentTree, bCheck);

	HTREEITEM hCurrent;
	if (hParentTree==NULL) return;

	hParentTree = m_TreeCtrl.GetChildItem(hParentTree);

	//check box all children
	for(hCurrent=hParentTree;hCurrent!=NULL;hCurrent=m_TreeCtrl.GetNextSiblingItem(hCurrent))
	{
		if (m_TreeCtrl.ItemHasChildren(hCurrent))
		{
			m_TreeCtrl.Expand(hCurrent,TVE_EXPAND);
			m_TreeCtrl.SetCheck(hCurrent, bCheck);
			HTREEITEM hChild = m_TreeCtrl.GetChildItem(hCurrent);
			CheckTreeChildren(hChild, bCheck);
		}
		else 
		{
			m_TreeCtrl.SetCheck(hCurrent, bCheck);
		}
	}
	return ;
}

void CSwatView::OnTvnKeydownTree(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMTVKEYDOWN pTVKeyDown = reinterpret_cast<LPNMTVKEYDOWN>(pNMHDR);
	// TODO: Add your control notification handler code here
	*pResult = 0;
}

void CSwatView::OnBnClickedCheckAutoUpdate()
{
	//get the valuse of the checkbox
	UpdateData(TRUE);
}


//called after a script completes to update the pass/fail in the listbox
void CSwatView::UpdateScriptResult(CString strFile)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();

	//update the script results in the list box
	AddListResult();

}

//clear the status text
void CSwatView::OnBnClickedButtonClear()
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	pApp->m_strLog.Empty();
	m_strTestExecStatus.Empty();

}

//does the prep work for starting session 
UINT CSwatView::Session_Starter(LPVOID lp)
{
	LOG_MESSAGE(LM_INFO,"Entering Session_Starter ");
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();

	pApp->m_bScriptRunning = true;


	//if GUI connected, disconnect so fresh session is started with GUI calls
	//pApp->GUI_IODisconnect();

	
	pApp->m_nTotalScripts = pApp->m_strArrayFiles.GetCount();
	//create batch session ID for the batch 
	pApp->CreateSessionID();
	pApp->m_SessionResults.reset();
	//create init file for session to pass the sessionID as global
	pApp->Create_Lua_Init_Exit_Files("batch");


	//create a session file
	CString strLogPath("c:\\SWAT\\temp\\log\\");
	CFileOperation cf;
	if(cf.CheckPath(strLogPath)!= PATH_IS_FOLDER)
	{
		::CreateDirectory(strLogPath, NULL);
	}



	CString strFileName;
	strFileName = "c:\\SWAT\\temp\\log\\"+ pApp->m_strCurrentSessionID +".log";
	pApp->m_SessionFile.Open( strFileName, CFile::modeCreate | CFile::modeWrite | CFile::typeText | CFile::shareDenyNone);
	if(pApp->m_SessionFile.m_pStream == NULL)
	{
		AfxMessageBox("Could NOT create/open session file. \n" + strFileName);
		return 0;
	}


	pApp->m_strVentBDSWVersion = "not enabled in SWAT";
	pApp->m_strVentGUISWVersion = "not enabled in SWAT";
//	pApp->m_strCircuitType =   "not enabled in SWAT"; 
	if(pApp->m_strRecordSWinReport == "true")
	{
		GuiIOVentInfoResp out;
		pApp->GUI_IOGetVentSWVersion(&out);

		CString strBDver(out.bdSoftwareVer);
		CString strGUIver(out.bdSoftwareVer);
		CString strVentSerial(out.serialNum);

		pApp->m_strVentBDSWVersion = strBDver;
		pApp->m_strVentGUISWVersion = strBDver;
		pApp->m_strVentLanguage = pApp->GetLanguageInString(out.language);
		pApp->m_strVentSerialNumber = strVentSerial;


		//CString strVer = pApp->GUI_IOGetText("ID_META_AREA_CIRCUIT_TYPE"); // "Value=VIKING_1_2.1.0.43, Italic=False, Label=na, Unit=na"
		//int nStart = strVer.Find('{');
		//pApp->m_strCircuitType = strVer.Left(nStart);
	}


	//Create session init file
	try
	{
		CString strTemp;
		CStdioFile file( TEMP_SESSION_INIT, CFile::modeCreate | CFile::modeWrite | CFile::typeText );
		strTemp.Format("G_Vent_SW_Version = \"%s\" \r\n", pApp->m_strVentBDSWVersion);
		file.WriteString(strTemp);
		file.Close();
	}
	catch( CFileException* e )
	{
		CString str;
		str.Format("TEMP_SESSION_INT could not be opened or created, cause = %d\n", e->m_cause);
		AfxMessageBox(str);
		e->Delete();
	}





	pApp->LogStatus("##### Starting session ID: \""+ pApp->m_strCurrentSessionID + pApp->m_timeSessionStart.Format("\"  @%H:%M:%S"));

	LOG_MESSAGE(LM_INFO,"Exiting Session_Starter ");


	return 1;
}
//does the prep work for each script for starting Lua
UINT CSwatView::Script_Starter(LPVOID lp)
{
	LOG_MESSAGE(LM_INFO,"Entering Script_Starter ");
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	CString strLua;

	pApp->InitLua();
	
	if(luaL_dofile(pApp->m_L, SWAT_SETTING_FILE))//local machine/IP settings etc
	{
		//Error in running swatinit.lua file
		CString str;
		str.Format("%s", lua_tostring(pApp->m_L, -1));
		AfxMessageBox(str);
		LOG_MESSAGE(LM_ERROR,"%s", str);
		LOG_MESSAGE(LM_INFO,"Exiting Thread_Lua 5");
		pApp->LogStatus("Error in loading local SWAT settings file, test execution cancelled");
		pApp->LogStatus("---------------------------------------------------------------");
		pApp->CloseLua();
		pApp->m_bScriptRunning = false;
		return 0;
	}

	if(luaL_dofile(pApp->m_L, TEMP_SESSION_INIT))//get sw version in lua env
	{
		//Error in running session init.lua file
		CString str;
		str.Format("%s", lua_tostring(pApp->m_L, -1));
		AfxMessageBox(str);
		LOG_MESSAGE(LM_ERROR,"%s", str);
		LOG_MESSAGE(LM_INFO,"Exiting Thread_Lua 5");
		pApp->LogStatus("Error in loading session init file, test execution cancelled");
		pApp->LogStatus("---------------------------------------------------------------");
		pApp->CloseLua();
		pApp->m_bScriptRunning = false;
		return 0;
	}

	//setting lua paths and load other libs in init.lua
	strLua.Format("%s\\00_libs\\init.lua",pApp->m_strTestScriptPath );
	LOG_MESSAGE(LM_INFO,"Running %s", strLua);
	if(luaL_dofile(pApp->m_L, strLua))
	{
		//Error in running init.lua file
		CString str;
		str.Format("%s", lua_tostring(pApp->m_L, -1));
		AfxMessageBox(str);
		LOG_MESSAGE(LM_ERROR,"%s", str);
		LOG_MESSAGE(LM_INFO,"Exiting Thread_Lua 5.1");
		pApp->LogStatus("Error in loading init.lua file, test execution cancelled");
		pApp->LogStatus("---------------------------------------------------------------");
		pApp->CloseLua();
		pApp->m_bScriptRunning = false;
		return 0;
	}

	strLua.Format("%s\\00_libs\\id.lua",pApp->m_strTestScriptPath );
	LOG_MESSAGE(LM_INFO,"Running %s", strLua);
	if(luaL_dofile(pApp->m_L, strLua))
	{
		//Error in running id.lua file
		CString str;
		str.Format("%s", lua_tostring(pApp->m_L, -1));
		AfxMessageBox(str);
		LOG_MESSAGE(LM_ERROR,"%s", str);
		LOG_MESSAGE(LM_INFO,"Exiting Thread_Lua 5.1");
		pApp->LogStatus("Error in loading id.lua file, test execution cancelled");
		pApp->LogStatus("---------------------------------------------------------------");
		pApp->CloseLua();
		pApp->m_bScriptRunning = false;
		return 0;
	}




	LOG_MESSAGE(LM_INFO,"Exiting Script_Starter ");
	return 1;
}

//Lua Script execution thread, runs all the scripts one by one
UINT CSwatView::Thread_Lua(LPVOID lp)
{ 
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	if(Session_Starter(NULL)==0) return 0;
	
	CString strFile;//current running script file name
	int n = 0;//to be removed
	CString str;//for logs

//	for(int n = 0; n < pApp->m_nTotalScripts; n++)
	while(1)
	{
		strFile=pApp->GetScriptNameToRun();
		if(strFile.IsEmpty()) 
		{
			break;
		}//else start running

		if(pApp->m_bUserStopped) break;
		//if(pApp->m_bRun == false) break; //stop pressed
		pApp->m_bRun = true;
		pApp->m_bScriptRunning = true;


		
		
//		strFile= pApp->m_strArrayFiles.GetAt(n);
		pApp->m_nCurentScriptNumber = n+1;

		pApp->m_ScriptResults.reset();
		pApp->m_ScriptResults.m_strFileName = strFile;
		pApp->m_ScriptResults.m_timeStart = CTime::GetCurrentTime();
		pApp->GetScriptResult(strFile); //clear the numbers if ruuning in a different session. 
		pApp->ResetImageCounter();


		//logs data for the report header
		if(pApp->LogReportHeader(strFile)==0)
		{
			
			//***********WARNING- --- DO-  - - - NOT-- - - - Change this line, used in status tab dbl click//
			str.Format("Report file read only, dbl click here to unlock > %s", pApp->GetReportFileName(strFile));
			//************WARNING OVER ----------------------------------------------------------------------//


			pApp->LogStatus(str, REPORT_ERROR);
			pApp->LogStatus("---------------------------------------------------------------");
			pApp->CloseLua();
			pApp->m_bScriptRunning = false;
			//return 0;
			continue;
		}

		CString strMessage("==== STARTING PROCEDURE : "+ strFile);
		pApp->LogStatus(strMessage);
		LOG_MESSAGE(LM_INFO,strMessage.GetBuffer());
		strMessage.ReleaseBuffer();

		//create lua instance 
		if(Script_Starter(NULL)==0) return 0; //script starter failed, stop running

		//create init file for each script, passing the script name as global
		pApp->Create_Lua_Init_Exit_Files(strFile);

		//run the created temp script init file
		LOG_MESSAGE(LM_INFO,"Running %s", TEMP_SCRIPT_INIT);
		if(luaL_dofile(pApp->m_L, TEMP_SCRIPT_INIT))
		{
			//Error in running init file
			CString str;
			str.Format("%s", lua_tostring(pApp->m_L, -1));
			AfxMessageBox(str);
			
			LOG_MESSAGE(LM_ERROR, "%s", str);
			LOG_MESSAGE(LM_INFO,"Exiting Thread_Lua 4");
			pApp->LogStatus(str);
			pApp->LogStatus("Error in loading temp init file, test execution cancelled", REPORT_ERROR);
			pApp->LogReportFooter(strFile, true);

			pApp->CloseLua();
			pApp->m_bScriptRunning = false;
			return 0;
		}


//		pApp->LogStatus("Starting execution: " + strFile);
//		pApp->LogStatus("------------------------- STARTING SCRIPT EXECUTION -------------------------------", REPORT_RESULT_FOOTER);
		
		

		pApp->LogStatus("Running: "+ strFile, REPORT_RESULT_FOOTER);
		if(pApp->m_strRecordSWinReport=="true")
		{
			pApp->AddFileToRecordVersion(strFile);
		}
		
		
		if(luaL_dofile(pApp->m_L, strFile))
		{
			//Error in running the file
			CString str;
			str.Format("%s", lua_tostring(pApp->m_L, -1));
//			AfxMessageBox(str);
//			pApp->m_SessionResults.m_nError++ ;

			str.Replace("\n", "\r\n"); 

			//DONOT change the >>> and <<< format, lua file is opened from parsing this. Details in: CTabStatusDlg::OnLbnDblclkListBox()
			pApp->LogStatus(">>>"+str+"<<<"); 

		
			//get the result of the script from database
			pApp->m_ScriptResults.m_strFileName = strFile;
			pApp->GetScriptResult(strFile);

			pApp->LogStatus("Error occured in the script execution. Script stop triggered.", REPORT_ERROR);

/*			pApp->m_bScriptRunning = false;
			LOG_MESSAGE(LM_INFO,"Exiting Thread_Lua 3");

			pApp->LogReportFooter(strFile, true);

			pApp->CloseLua();
			pApp->m_bScriptRunning = false;
			return 0;
*/		}
		//else
		//{

			//run the script exit file to record the script_stop_time
			LOG_MESSAGE(LM_INFO,"Running %s", TEMP_SCRIPT_EXIT);
			if(luaL_dofile(pApp->m_L, TEMP_SCRIPT_EXIT))
			{
				//Error in running temp_script_exit file
				CString str;
				str.Format("%s", lua_tostring(pApp->m_L, -1));
				pApp->LogStatus(str, REPORT_ERROR);
				LOG_MESSAGE(LM_INFO,"Exiting Thread_Lua 2");
				LOG_MESSAGE(LM_INFO,"Error in running temp_script_exit file");
				pApp->CloseLua();
				pApp->m_bScriptRunning = false;
				return 0;
			}




		//}

		pApp->CopyAllCapturedImages();
		//wait till all captured images are saved to SWAT report
		//while(pApp->m_nImagesYetToCopyOverFTP>0)
		int nCopying = pApp->m_listCapturedImages.GetCount();
		while(nCopying > 0)
		{
			if(pApp->m_bRun == false) break;
			str.Format("Please wait, getting captured images from vent, %d remaining...",nCopying);
			pApp->LogStatus(str);
			Sleep(4000);
			nCopying = pApp->m_listCapturedImages.GetCount();
		}
		
		//get the result of the script from database
		pApp->m_ScriptResults.m_strFileName = strFile;
		
		pApp->LogReportFooter(strFile, false);//it might also log another error for objective not defined


		pApp->GetScriptResult(strFile); //total errors to be updated

		//Call session exit file to close database connections
		LOG_MESSAGE(LM_INFO,"Running %s", TEMP_SESSION_EXIT);
		if(luaL_dofile(pApp->m_L, TEMP_SESSION_EXIT))
		{
			//Error in running exit file
			str.Format("%s", lua_tostring(pApp->m_L, -1));
			pApp->LogStatus(str, REPORT_ERROR);
			LOG_MESSAGE(LM_INFO,"Error in running temp_script_exit file");
		}

		pApp->CloseLua();

	
	}
	

	


	

	pApp->m_timeSessionEnd = CTime::GetCurrentTime();
	CTimeSpan timeSpan = pApp->m_timeSessionEnd - pApp->m_timeSessionStart;
	CString strTime;
	strTime.Format("@%s in %s", pApp->m_timeSessionEnd.Format("%H:%M:%S"), timeSpan.Format("%H:%M:%S"));
	

	pApp->LogStatus("##### Completed session: \"" + pApp->m_strCurrentSessionID + "\""+ strTime );

	//Copy the log life as html in the testscript folder for each session
	/*CString strBckup = pApp->m_strTestScriptPath+"\\"+pApp->m_strCurrentSessionID+".htm";
	LOG_MESSAGE(LM_INFO,"copying %s to %s", SWAT_TEST_LOG_FILE, strBckup);
	if(!CopyFile( SWAT_TEST_LOG_FILE, strBckup, false))
	{
		LOG_MESSAGE(LM_ERROR,"copying %s to %s failed", SWAT_TEST_LOG_FILE, strBckup);
	}*/

	
	pApp->m_bScriptRunning = false;

	pApp->LogStatus("   ");
	
	LOG_MESSAGE(LM_INFO,"##### Completed session ID: %s", TEMP_SESSION_EXIT);


	
	//pApp->CloseLua();

	
	if(pApp->m_bSendMail) //email the session file
	{
		CString strResults;
		strResults.Format("Total %d | PASS : %d | FAIL : %d | W/Error : %d",  
			pApp->m_SessionResults.m_nTotal, pApp->m_SessionResults.m_nPass, 
			pApp->m_SessionResults.m_nFail, pApp->m_SessionResults.m_nWarning+pApp->m_SessionResults.m_nError);
		pApp->EmailUser(SWAT_TEST_LOG_FILE, strResults, TRUE);
	}

	LOG_MESSAGE(LM_INFO,"Exiting Thread_Lua");



	
	return 0;
	
    
} 


void CSwatView::OnClose()
{
	CFormView::OnClose();
}



void CSwatView::OnNMDblclkTree(NMHDR *pNMHDR, LRESULT *pResult)
{
	
	POINT pt;
    GetCursorPos(&pt);
    m_TreeCtrl.ScreenToClient(&pt);
    TVHITTESTINFO hti;
    hti.pt.x = pt.x;
    hti.pt.y = pt.y;
    HTREEITEM hItem = m_TreeCtrl.HitTest(&hti);
	CString strFileName (m_TreeCtrl.GetItemText(hItem));

	if (m_TreeCtrl.ItemHasChildren(hItem))
		return;

	   
	if (hti.flags & TVHT_ONITEM)
    {
		LVFINDINFO FindInfo;
		FindInfo.flags = LVFI_PARTIAL|LVFI_STRING;
		FindInfo.psz = strFileName;

		if(m_List.FindItem(&FindInfo) == -1)
		{
			//AddListEntry(m_TreeCtrl.GetItemText(hItem));
			if(m_TreeCtrl.GetItemText(hItem).Right(4).CompareNoCase(".lua")==0)
			{
				AddListEntry(m_TreeCtrl.GetItemText(hItem));
				m_TreeCtrl.SetCheck(hItem, true);

			}

		}
	}
	*pResult = 0;
}





void CSwatView::OnSwatsettingsInit()
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	ShellExecute(0, "open", pApp->m_strEditorPath, SWAT_SETTING_FILE, 0, SW_SHOW);

}

void CSwatView::OnSwatsettingsSQL_lib()
{
	//ShellExecute(0, "open", "C:\\Program Files\\Lua\\5.1\\SciTE\\scite.exe", "C:\\SWAT\\Lua\\libs\\sql_lib.lua", 0, SW_SHOW);
}

void CSwatView::OnSwatsettingsPcapp()
{
	//ShellExecute(0, "open", "C:\\Program Files\\Lua\\5.1\\SciTE\\scite.exe", "C:\\SWAT\\Lua\\libs\\pcapp.lua", 0, SW_SHOW);
}

void CSwatView::OnSwatViewstoredresults()
{
	ViewStoredResultsDlg dlg;

	dlg.DoModal();

}




void CSwatView::OnNMRClickTree(NMHDR *pNMHDR, LRESULT *pResult)
{
	 // For showing pop up menu
	CMenu obMenu;
	obMenu.LoadMenu(IDR_LIST_POPUP); 

	CMenu* pPopupMenu = obMenu.GetSubMenu(0);
	ASSERT(pPopupMenu); 
	
	// Get the cursor position
	CPoint obCursorPoint = (0, 0);
	GetCursorPos(&obCursorPoint);

	//select the item even though right click
	POINT pt;
    GetCursorPos(&pt);
    m_TreeCtrl.ScreenToClient(&pt);
    TVHITTESTINFO hti;
    hti.pt.x = pt.x;
    hti.pt.y = pt.y;
    HTREEITEM hT = m_TreeCtrl.HitTest(&hti);
    
	if (hti.flags & TVHT_ONITEM)
    {
		m_TreeCtrl.Select(hti.hItem, TVGN_CARET);
	}

	/*open the pop up*/
	// Track the popup menu
	pPopupMenu->TrackPopupMenu(TPM_LEFTALIGN|TPM_LEFTBUTTON|TPM_RIGHTBUTTON, obCursorPoint.x, 
									obCursorPoint.y, this);


	
	*pResult = 0;
}



void CSwatView::OnPopUpExplore()
{
	CSwatApp *pApp = (CSwatApp *)AfxGetApp();
	CString strFileName = m_TreeCtrl.GetItemText(m_TreeCtrl.GetSelectedItem());
	CString strPath;
	if(strFileName.Right(4).Find('.') == 0)
	{
		int n = strFileName.ReverseFind('\\');
		strPath = strFileName.Left(n);
	}
	else
	{
		strPath = pApp->m_strTestScriptPath;
	}

	ShellExecute(0, "explore", strPath, 0, 0, SW_SHOW);
}


void CSwatView::OnPopUpRefreshTree()
{
	CSwatApp *pApp = (CSwatApp *)AfxGetApp();

	//Clear the tree control
	m_TreeCtrl.SetRedraw(FALSE);
	m_TreeCtrl.DeleteAllItems();
	m_TreeCtrl.SetRedraw();
	m_TreeCtrl.InvalidateRect(NULL);
	m_TreeCtrl.UpdateWindow();

	//Populate fresh
	UpdateTree(pApp->m_strTestScriptPath);
}
void CSwatView::OnPopUpEditScript()
{
	CSwatApp *pApp = (CSwatApp *)AfxGetApp();
	CString strFileName = m_TreeCtrl.GetItemText(m_TreeCtrl.GetSelectedItem());
	if(strFileName.Right(4).CompareNoCase(".lua") == 0)
	{
		ShellExecute(0, "open", pApp->m_strEditorPath, "\""+strFileName+"\"", 0, SW_SHOW);
	}
	else if(strFileName.Right(4).CompareNoCase(".doc") == 0)
	{
		ShellExecute(0, "open", "winword", strFileName, 0, SW_SHOW);
	}
	else if(strFileName.Right(4).CompareNoCase(".xls") == 0)
	{
		ShellExecute(0, "open", "excel", strFileName, 0, SW_SHOW);
	}
	else if(strFileName.Right(4).CompareNoCase(".tsl") == 0)
	{
		ShellExecute(0, "open", "C:\\Program Files\\Lua\\5.1\\SciTE\\scite.exe", strFileName, 0, SW_SHOW);
	}
	else if(strFileName.Right(4).CompareNoCase(".txt") == 0)
	{
		ShellExecute(0, "open", "notepad", strFileName, 0, SW_SHOW);
	}
	else if(strFileName.Right(4).CompareNoCase(".qpr") == 0)
	{
		ShellExecute(0, "open", "C:\\Program Files\\Quotix Software\\QDE 0.7.5\\QDE.exe", strFileName, 0, SW_SHOW);
	}
}

void CSwatView::ShowBreathPhase(CString str)
{
	
	if(str.CompareNoCase("INSPIRATION") == 0)
	{
		if (m_Picture.Load(MAKEINTRESOURCE(IDR_JPG1),_T("JPG")))
			m_Picture.Draw();
	}

	else if(str.CompareNoCase("EXHALATION") == 0)
	{
		if (m_Picture.Load(MAKEINTRESOURCE(IDR_JPG2),_T("JPG")))
		m_Picture.Draw();
	}
	else if(str.CompareNoCase("NON_BREATHING") == 0)
	{
		if (m_Picture.Load(MAKEINTRESOURCE(IDR_JPG3),_T("JPG")))
		m_Picture.Draw();
	}
/*	else if(str.CompareNoCase("PAV_INSPIRATORY_PAUSE") == 0)
	{
		if (m_Picture.Load(MAKEINTRESOURCE(IDR_JPG3),_T("JPG")))
		m_Picture.Draw();
	}*/
}

void CSwatView::OnSwatRecord()
{
	CWaitCursor wait;
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	pApp->m_bRecord = true;

	if(SWAT_SUCCESS != pApp->GUI_IOConnect())
	{
		return;
	}
	GuiIOUIOutputOnOff(true);
	pApp->m_bRecording = true;
	pApp->LogStatus("Recording started.");


	SetTimer(111,1000,NULL);
	Sleep(500);
	SetTimer(222,1000,NULL);

	m_pRecordThread = AfxBeginThread((AFX_THREADPROC)RecordThread, (LPVOID)this);
}

void CSwatView::OnUpdateSwatRecord(CCmdUI *pCmdUI)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	pCmdUI->Enable((!pApp->m_bRecording) && (!pApp->m_bScriptRunning));
	pCmdUI->SetCheck(pApp->m_bRecording);
}


UINT CSwatView::RecordThread(LPVOID lp)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	GuiIOUIOutputResp resp;
	GuiIOError err;
	
	while(pApp->m_bRecord)
	{
		err = GuiIOReceiveUIOutput(1000, &resp);
		if(err == GUIIO_SUCCESS)
		{
			ParseRecordLog(resp);
		}
		else if(err == GUIIO_ERROR)
		{
			LOG_MESSAGE(LM_INFO, "GuiIOReceive returned GUIIO_ERROR");
			pApp->LogStatus("GuiIOReceiveUIOutput failed. Exiting thread.");
			break;
		}
		
	}
	GuiIOUIOutputOnOff(false);
	pApp->m_bRecording = false;
	pApp->LogStatus(">>>Recording stopped.");

	return 0;
}


UINT CSwatView::ParseRecordLog(GuiIOUIOutputResp &resp)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
/*
For the CONTROL_UP, CONTROLS_CLICKED, CONTROL_DOWN output types, the objectId and value structure elements are valid.
For the KNOB output type, the objectId, value and state structure elements are valid. The state value is the knob delta.
For the KEY output type, the state and state2 elements are valid. The state value is the key input ID and the state2 is either up or down. 
For the KEY output type, the state and state2 elements are valid. The state value is the key input ID and the state2 is either up or down.


typedef GUIIOSERVER_API struct
{
	GuiIOOutputType outputType;
	CHAR objectId[GUI_IO_MAX_STR_LEN];
	CHAR value[GUI_IO_MAX_STR_LEN];
	INT state;
	INT state2;
} GuiIOUIOutputResp;
typedef GUIIOSERVER_API enum
{
	CONTROL_DOWN = 0,
	CONTROL_CLICKED = 1,
	CONTROL_UP = 2,
	KNOB = 3,
	KEY = 4
} GuiIOOutputType;


typedef enum KeyInputIds

{
      KIID_NONE = 0,
      KIID_ALARM_SILENCE,
      KIID_ALARM_RESET,
      KIID_SCREEN_LOCK
}; state
typedef enum KeyInputActions
{
      KIACT_NONE = 0,
      KIACT_KEY_PRESS,
      KIACT_KEY_RELEASE
}; state2

*/

	SYSTEMTIME systime;
	GetLocalTime(&systime);

	CString strTime;
	strTime.Format("%d:%d:%d:%d",systime.wHour,systime.wMinute, systime.wSecond, systime.wMilliseconds);

	CString strValue(resp.value);
	pApp->ConvertSpecialChar(strValue);

	CString strLog;
	switch(resp.outputType){
		case CONTROL_DOWN: 
			strLog.Format("%s %s : Control_down : %s ", strTime, resp.objectId, strValue);
			break;
		case CONTROL_CLICKED: 
			strLog.Format("%s %s : Control_clicked : %s ", strTime, resp.objectId, strValue);
			break;
		case CONTROL_UP: 
			strLog.Format("%s %s : Control_up : %s ", strTime, resp.objectId, strValue);
			break;
		case KNOB: 
			strLog.Format("%s Kob %s Delta: %d  Value: %s ", strTime, resp.objectId, resp.state, strValue);
			break;
		case KEY:
			{	
				CString strKeyInputId;
				switch(resp.state)
				{
					case 0:	//KIID_NONE 
							strKeyInputId = "None (0)";
							break;
					case 1:	//KIID_ALARM_SILENCE
							strKeyInputId = "KIID_ALARM_SILENCE(1)";
							break;
					case 2:	//KIID_ALARM_RESET
							strKeyInputId = "KIID_ALARM_RESET(2)";
							break;
					case 3:	//KIID_SCREEN_LOCK
							strKeyInputId = "KIID_SCREEN_LOCK(3)";
							break;
				}

				CString strKeyInputAction;
				switch(resp.state2)
				{
					case 0:	//KIACT_NONE 
							strKeyInputAction = "KIACT_NONE(0)";
							break;
					case 1:	//KIACT_KEY_PRESS
							strKeyInputAction = "KIACT_KEY_PRESS(1)";
							break;
					case 2:	//KIACT_KEY_RELEASE
							strKeyInputAction = "KIACT_KEY_RELEASE(2)";
							break;
				}
				
				strLog.Format("%s Key input ID: %s  State: %s ", strTime, strKeyInputId, strKeyInputAction);
				break;
			}

	}

	
	pApp->LogStatus(strLog);

	return 0;

}

void CSwatView::WriteReportLog(CString &str)
{
			
}
/*
void CSwatView::TurnOnOffVent(bool bOnOff)
{
	m_pPowerDlg->Refresh();
}*/

void CSwatView::OnOpenLastResultLog()
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();

	CString strFileName = m_TreeCtrl.GetItemText(m_TreeCtrl.GetSelectedItem());
	if(strFileName.Right(4).CompareNoCase(".lua") == 0)
	{
		CString strLogFile = pApp->GetReportFileName(strFileName);
		FILE * pFile;
		pFile = fopen (strLogFile,"r");
		if (pFile!=NULL)
		{
			// file exists.  close and open in explorer
			fclose (pFile);
			ShellExecute(0, "open", "iexplore.exe", strLogFile, 0, SW_SHOW);
		}
		else 
		{
			AfxMessageBox("Report not found: \n" + strLogFile);
		}
	}
	
}


void CSwatView::OnPopUpChangeScriptpath()
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	
	CFileDialog dlg(TRUE, NULL, NULL, NULL, "ID.lua (lua)|ID.lua|", NULL);

	while(dlg.GetPathName().Find("ID.lua")==-1)
	{
		if(dlg.DoModal() == IDCANCEL)		
			return;
	}
		
	CString strPath(dlg.GetPathName());

	int n = strPath.ReverseFind('\\');
	strPath = strPath.Left(n);

	n = strPath.ReverseFind('\\');
	strPath = strPath.Left(n);


	pApp->m_strTestScriptPath = strPath.Left(n);

	
	//Clear the tree control
	m_TreeCtrl.SetRedraw(FALSE);
	m_TreeCtrl.DeleteAllItems();
	m_TreeCtrl.SetRedraw();
	m_TreeCtrl.InvalidateRect(NULL);
	m_TreeCtrl.UpdateWindow();

	//Populate fresh
	UpdateTree(pApp->m_strTestScriptPath);

	pApp->SaveRegistrySetting(SWAT_REG_PARAM_TREE_PATH, pApp->m_strTestScriptPath);

}

void CSwatView::OnSwatPause()
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	pApp->m_bPause = !pApp->m_bPause;
}

void CSwatView::OnUpdateSwatPause(CCmdUI *pCmdUI)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	pCmdUI->Enable(pApp->m_bScriptRunning);
}

void CSwatView::OpenScriptFile(CString strFileName)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	CStdioFile file( strFileName, CFile::modeRead | CFile::typeText );
	CString str; BOOL bRead;
	m_List.DeleteAllItems();
	do
	{
		bRead = file.ReadString(str);
		if(!bRead) break;
		AddListEntry(str);
	}
	while(bRead);

	file.Close();
	pApp->m_strCurrentOpenScriptListFile = strFileName;

	(AfxGetMainWnd( ))->SetWindowText(SWAT_APPLICATION_TITLE + strFileName);


	
}
void CSwatView::SaveScriptFile(CString strFileName)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	int nCount = m_List.GetItemCount();
	
	if(nCount ==0) 
	{
		AfxMessageBox("List is empty, can not be saved");
		return;//nothing to save
	}

	CStdioFile file( strFileName, CFile::modeCreate | CFile::modeWrite | CFile::typeText );
	for (int n =0; n<nCount; n++)
	{
		file.WriteString(m_List.GetItemText(n,0) + "\r\n");
	}
	file.Close();

	pApp->m_strCurrentOpenScriptListFile = strFileName;
}

void CSwatView::NewScriptFile( )
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	pApp->m_strCurrentOpenScriptListFile.Empty();
	(AfxGetMainWnd( ))->SetWindowText(SWAT_APPLICATION_TITLE);

}
void CSwatView::OnLvnKeydownListControl(NMHDR *pNMHDR, LRESULT *pResult)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	
	LPNMLVKEYDOWN pLVKeyDow = reinterpret_cast<LPNMLVKEYDOWN>(pNMHDR);

	if(pLVKeyDow->wVKey == VK_DELETE)
	{
		if(pApp->m_bScriptRunning) //if batch running only remove the scripts not started execution
		{

			POSITION pos = m_List.GetFirstSelectedItemPosition();
			if (pos != NULL)
			{
				int nItem = m_List.GetNextSelectedItem(pos);
				CString str = m_List.GetItemText(nItem,1);
				if(str.IsEmpty())
				{
					m_List.DeleteItem(nItem);
				}
				else
				{
					AfxMessageBox("Only scripts which have not started execution yet can be removed.");
				}
			}
		}
		else
		{
			POSITION pos = m_List.GetFirstSelectedItemPosition();
			if (pos != NULL)
			{
				int nItem = m_List.GetNextSelectedItem(pos);
				m_List.DeleteItem(nItem);
			}
		}
	}

	*pResult = 0;
}


void CSwatView::AddSessionTestResultsToFile()
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	if(!pApp->m_SessionFile.m_pStream)
	{
		return;
	}
	int nCount = m_List.GetItemCount();

	for (int n =0; n<nCount; n++)
	{
		pApp->m_SessionFile.WriteString(m_List.GetItemText(n,0) + "\r\n");
		pApp->m_SessionFile.WriteString(m_List.GetItemText(n,1) + "\r\n");
		pApp->m_SessionFile.WriteString(m_List.GetItemText(n,2) + "\r\n");
	}

}

void CSwatView::ReadSessionTestResultsFromFile(CStdioFile *file)
{
	m_List.DeleteAllItems();

	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	int nIndex = 0;
	CString str;
	while(file->ReadString(str))
	{
		if(str.CompareNoCase("--SessionID--")==0)
		{
			file->ReadString(str);
			break;
		}
		AddListEntry(str);

		file->ReadString(str);
		m_List.SetItemText(nIndex, 1, str.TrimRight());

		file->ReadString(str);
		m_List.SetItemText(nIndex, 2, str.TrimRight());
	}


}

void CSwatView::OnPopUpRunOnlyThisOne()
{
	CSwatApp *pApp = (CSwatApp *)AfxGetApp();

	if(pApp->m_bRecording || pApp->m_bScriptRunning)
	{
		return;
	}
	
	CString strFileName = m_TreeCtrl.GetItemText(m_TreeCtrl.GetSelectedItem());
	
	//Don't do anything if not a lua file
	if(strFileName.Right(4).CompareNoCase(".LUA")!=0) 
		return;

	//add to the list and start running
	m_List.DeleteAllItems();
	AddListEntry(strFileName);
	OnVtreeRunselectedfiles();
}
