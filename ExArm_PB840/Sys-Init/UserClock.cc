#include "stdafx.h"
//=====================================================================
//   This is a proprietary work to which Puritan-Bennett corporation
//   of California claims exclusive right.  No part of this work may
//  be used, disclosed, reproduced, sorted in an information retrieval
//     system, or transmitted by any means, electronic, mechanical,
//   photocopying, recording, or otherwise without the prior written
//      permission of Puritan-Bennett Corporation of California.
//
//          Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: UserClock - Operator clock (wall clock) interface class
//---------------------------------------------------------------------
//@ Interface-Description
//  The UserClock class provides the application interface to set the
//  system clock and to synchronize the BD and GUI clocks.  It provides
//  a Set method to set and synchronize the clocks.
//---------------------------------------------------------------------
//@ Rationale
//  The UserClock class encapsulates the methods to set and synchronize
//  the BD and GUI user clocks.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The Set method provides access to set and synchronize the clocks.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Sys-Init/vcssrc/UserClock.ccv   25.0.4.1   20 Nov 2013 17:37:48   rhjimen  $
//
//@ Modification-Log
//
//  Revision: 008  By: rpr    Date:  6-Nov-2012    SCR Number: 6777
//  Project:  BI
//  Description:
//      Removed all monitor software.
//
//  Revision: 007  By:  rpr    Date:  03-Feb-2011    SCR Number: 6740
//  Project:  Zena II
//  Description:
//		Only log clock syncs when the user intiates them.
//
//  Revision: 006  By:  gdc    Date:  28-Jan-2011    SCR Number: 6671
//  Project:  XENA2
//  Description:
//		Removed call to BdMonitoredData::TimeHasChanged.
//
//  Revision: 005  By:  gdc    Date:  18-Aug-2009    SCR Number: 6147
//  Project:  XB
//  Description:
//		Moved time change operation inside high priority gate to
//		prevent time monitors from using a "changing" time in its
//		cycle and interval time monitoring functions.
//
//  Revision: 004  By:  gdc    Date:  12-Dec-2008    SCR Number: 6068
//  Project:  840S
//  Description:
//		Removed SUN prototype code.
//
//   Revision 003   By:   gdc      Date: 17 Jun 99   DR Number: 5422
//      Project:   ATC
//      Description:
//         Gated the set clock operation to avoid possibility of lower
//         priority task reading clock while it's being set.
//
//   Revision 002   By:   sah      Date: 01/13/99    DR Number: 5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to a non-inlined method.
//
//   Revision 001   By:   gdc      Date: 07/06/95    DR Number:   n/a
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//=====================================================================

//=====================================================================
//
//      Includes...
//
//=====================================================================

#include "UserClock.hh"
#ifdef SIGMA_DEVELOPMENT
# include "Ostream.hh"
#endif

//@ Usage-Classes
#include "Background.hh"
#include "NovRamManager.hh"
#include "TimeStamp.hh"
#include "Task.hh"
#include "TaskInfo.hh"
#include "TaskControlAgent.hh"
//TODO E600 port #include "Watchdog.hh"
//@ End-Usage

//=====================================================================
//
//      Static Data...
//
//=====================================================================

//  Clock is set iff the current time differs from the input time
//  by more than this threshold (in milliseconds)
const Uint32  SET_CLOCK_THRESHOLD = 1000;

//@ Code...

//=====================================================================
//
//      Public Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Set
//
//@ Interface-Description
//  The UserClock Set static method sets the user (wall time) clock to
//  the time and date specified.  When invoked from the GUI, this method
//  sends a SyncClockMessage to the BD to synchronize its clock with
//  the GUI's.  The clock is not reset unless the current time differs
//  from the input time by more than CLOCK_SET_THRESHOLD. $[00432]
//---------------------------------------------------------------------
//@ Implementation-Description
//  Converts TimeStamp to TimeOfDay struct and calls the Operating-
//  System SetTimeOfDay function.  On the GUI, invokes TaskControl::
//  SyncClocks to send the SyncClockMessage to BD task control.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
UserClock::Set(const TimeStamp& inputTime, Boolean userInitiated)
{
    CALL_TRACE("Set(TimeStamp&)");

    Int32      deltaMilliseconds;
    TimeStamp  currentTime;

    deltaMilliseconds = currentTime - inputTime;
    if (deltaMilliseconds < 0) // $[TI1]
        deltaMilliseconds = -deltaMilliseconds;
    // else $[TI2]

    if (deltaMilliseconds >= SET_CLOCK_THRESHOLD) // $[TI3]
    {
        NovRamManager::RegisterPendingClockUpdate(userInitiated);

        // create a TimeOfDay struct since Operating-System doesn't talk C++
        struct TimeOfDay setTime = inputTime.getTimeOfDay();

        //  suspend watchdog strobing interval verification while we
        //  change the time, otherwise it posts a system diagnostic
#ifdef E600_840_TEMP_REMOVED
        Watchdog::SetIntervalVerify(FALSE);
#endif

		// Save current priorit, Boost my priority to system high so no
		// task can read clock while I'm setting it, then restore priority
		Int32 myPriority = Task::GetPriority();
		if(myPriority < 0)	//GetPriority returned error
		{
			//in case GetPriority failed, we have to restore to something..
			myPriority = Task::GetTaskBaseInfo().GetBasePriority();
		}
		Task::SetPriority(TaskInfo::HIGHEST_PRIORITY);

		SetTimeOfDay(&setTime);

        //  resume watchdog strobing interval verification
		//TODO E600 VM: trace this #define from Tony F
#ifdef E600_840_TEMP_REMOVED
        Watchdog::SetIntervalVerify(TRUE);
#endif

        Background::TimeHasChanged();

		//  Restore priority
		Task::SetPriority(myPriority);

        NovRamManager::RegisterCompletedClockUpdate(userInitiated);

#ifdef SIGMA_DEVELOPMENT
        TimeStamp  nowTime;
        if (Sys_Init::GetDebugLevel(Sys_Init::ID_USERCLOCK) >= 1)
            cout << "CLOCK NOW SET TO: "
                 << nowTime.getYear() << "/"
                 << nowTime.getMonth() << "/"
                 << nowTime.getDayOfMonth() << " "
                 << nowTime.getHour() << ":"
                 << nowTime.getMinutes() << ":"
                 << nowTime.getSeconds() << "."
                 << nowTime.getHundredths() << endl;
#endif // SIGMA_DEVELOPMENT

    }
    // else $[TI4]

#ifdef SIGMA_GUI_CPU
    TaskControlAgent::SyncClocks(userInitiated);
#endif
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//@ Interface-Description
//   This method is called when a software fault is detected by the
//   fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//   'lineNumber' are essential pieces of information.  The 'pFileName'
//   and 'pPredicate' strings may be defaulted in the macro to reduce
//   code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This method receives the call for the SoftFault, adds it sub-system
//   and class name ID and sends the message to the non-member function
//   SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================

void
UserClock::SoftFault(const SoftFaultID  softFaultID,
                         const Uint32       lineNumber,
                         const char*        pFileName,
                         const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, SYS_INIT,
                          Sys_Init::ID_USERCLOCK,
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)

//=====================================================================
//
//      Protected Methods...
//
//=====================================================================

//=====================================================================
//
//      Private Methods...
//
//=====================================================================

