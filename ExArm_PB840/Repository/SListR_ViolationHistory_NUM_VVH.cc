#include "stdafx.h"

//====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class:  SListR_ViolationHistory_NUM_VVH - Fixed Singly-Linked List of 'ViolationHistory' References.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a derived class of an abstract, size-independent
//  singly-linked list of 'ViolationHistory' references ('AbstractSListR').  This
//  class provides an abstraction for a singly-linked list of generic
//  data items each of type 'ViolationHistory'.  The data items of the list are
//  references to the data items that are added to the list.  There
//  are non-constant, public methods to iterate through the contents of
//  the list, and there's also an iterator class (see 'SIteratorR<ViolationHistory>')
//  to allow iteration of a constant list.  This is a fixed list where,
//  at most, 'NUM_VVH' elements can be in the list at one time.
//---------------------------------------------------------------------
//@ Rationale
//  This class provides a standard abstraction for storing data items
//  in a dynamic collection, where initial placement within the
//  collection is quick and easy.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Each list has an internal heap containing the singly-linked
//  nodes ('SNodeC_MemPtr').  The nodes are allocated from
//  the heap to contain references to the inserted data.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  The restrictions have to do with the data type that is chosen for
//  this list.  The data type must provide the following mechanisms,
//  to be allowed to be used in this list:
//>Von
//      ViolationHistory::operator==(const ViolationHistory& aType) const;// equivalence operator...
//      ViolationHistory::operator!=(const ViolationHistory& aType) const;// inequivalence operator...
//>Voff
//  For primitive (built-in) types this is already supplied, but for
//  complex (class) types, these must be explicitly supplied.
//
//      Since these lists contain pointers to the inserted items, those
//      items that are inserted into these lists must have a lifetime that
//      is longer than the lifetime of these lists' reference to them.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Foundation/vcssrc/FixedSListR.C_v   25.0.4.0   19 Nov 2013 14:03:48   pvcs  $
//
//@ Modification-Log
//
//   Revision: 003   By: sah   Date: 08-Feb-1999   DR Number: 5339
//   Project:  ATC
//   Description:
//	Removed 'SIGMA_DEVELOPMENT' flags from around 'SoftFault()'
//	method, to allow for mixed-mode builds.
//
//   Revision: 002   By: sah   Date: 07-Jan-1999   DR Number: 5321
//   Project:  ATC
//   Description:
//	Changed 'SoftFault()' method to an non-inlined method.
//	Also, as part of making all 'SoftFault()' methods non-inlined,
//	I'm also making those 'SoftFault()' methods that are NOT called
//	in PRODUCTION mode, DEVELOPMENT-only.
//
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//=====================================================================

#include "SListR_ViolationHistory_NUM_VVH.hh"
#include "ViolationHistory.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SListR_ViolationHistory_NUM_VVH(sList)  [Copy Constructor]
//
//@ Interface-Description
//  Construct this list of references using 'sList' for initialization.
//---------------------------------------------------------------------
//@ Implementation-Description
//  A reference to this list's internal list of pointers is passed
//  to the base class's constructor.  The copying of 'sList' MUST
//  be initiated from this constructor, as opposed to the base class,
//  because 'ptrList_' can't run until the base class's constructor
//  is complete.
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  (*this == sList  &&  (isEmpty()  ||
//                        currentItem() == sList.currentItem()))
//@ End-Method
//=====================================================================

SListR_ViolationHistory_NUM_VVH::SListR_ViolationHistory_NUM_VVH(
				     const SListR_ViolationHistory_NUM_VVH& sList
					      )
				     : SListR_ViolationHistory(&ptrList_),
				       ptrList_(sList.ptrList_)
{
  CALL_TRACE("SListR_ViolationHistory_NUM_VVH(sList)");
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SListR_ViolationHistory_NUM_VVH()  [Default Constructor]
//
//@ Interface-Description
//  Create an empty reference list.
//---------------------------------------------------------------------
//@ Implementation-Description
//  A reference to this list's internal list of pointers is passed
//  to the base class's constructor.
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  (isEmpty())
//@ End-Method
//=====================================================================

SListR_ViolationHistory_NUM_VVH::SListR_ViolationHistory_NUM_VVH(void)
			      : SListR_ViolationHistory(&ptrList_),
				ptrList_()
{
  CALL_TRACE("SListR_ViolationHistory_NUM_VVH()");
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ~SListR_ViolationHistory_NUM_VVH()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this list of references--but NOT the objects it
//  references.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

SListR_ViolationHistory_NUM_VVH::~SListR_ViolationHistory_NUM_VVH(void)
{
  CALL_TRACE("~SListR_ViolationHistory_NUM_VVH()");
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pBoolTest)
//                      [static]
//
//@ Interface-Description
//      Report the software fault that occured within the source code
//      of this class.  The fault, indicated by 'softFaultID', occured
//      at 'lineNumber' in 'pFileName'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//      none
//---------------------------------------------------------------------
//@ PostConditions
//      none
//@ End-Method
//=====================================================================

void
SListR_ViolationHistory_NUM_VVH::SoftFault(const SoftFaultID softFaultID,
                                   const Uint32      lineNumber,
                                   const char*       pFileName,
                                   const char*       pBoolTest)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pBoolTest)");
  FaultHandler::SoftFault(softFaultID, FOUNDATION, FIXED_SLISTR,
                          lineNumber, pFileName, pBoolTest);
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

#if defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)

#include "Ostream.hh"

//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  print_(ostr)  [const, virtual]
//
// Interface-Description
//  This method overrides the base class's pure virtual method, and
//  dumps out an ASCII representation of the current state of this
//  list to 'ostr'.
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreConditions
//  none
//---------------------------------------------------------------------
// PostConditions
//  none
// End-Method
//=====================================================================

Ostream&
SListR_ViolationHistory_NUM_VVH::print_(Ostream& ostr) const
{
  CALL_TRACE("print_(ostr)");

  ostr << "SListR_ViolationHistory_NUM_VVH {\n";

  if (!isEmpty())
  {
    const SNodeC_MemPtr*  pNode = getFirst_();

    do
    {
      ostr << ((pNode == getCurrent_()) ? "->" : "  ")
	   << *((const ViolationHistory*)(pNode->getItem())) << '\n';
    } while ((pNode = pNode->getNextPtr()) != NULL);
  }

  ostr << "};\n";
  ostr.flush();

  return(ostr);
}

#endif  // defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)
