#ifndef TrendGraphsSubScreen_HH
#define TrendGraphsSubScreen_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: TrendGraphsSubScreen - Trending Graphs sub-screen
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/TrendGraphsSubScreen.hhv   25.0.4.0   19 Nov 2013 14:08:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By: gdc	    Date:  20-Nov-06    SCR Number: 6237
//  Project:  Trend
//  Description:
//  Initial Version
//=====================================================================
#include "GuiApp.hh"
#include "TrendSubScreen.hh"
#include "TrendEventTarget.hh"

//@ Usage-Classes
#include "ButtonTarget.hh"
#include "DropDownSettingButton.hh"
#include "FocusButton.hh"
#include "GuiAppClassIds.hh"
#include "Notification.hh"
#include "NumericField.hh"
#include "ScrollableMenuSettingButton.hh"
#include "SettingsMgr.hh"
#include "TextField.hh"
#include "TouchableText.hh"
#include "TrendCursorSlider.hh"
#include "TrendPlot.hh"

class SubScreenArea;
class TrendDataSetAgent;
//@ End-Usage

class TrendGraphsSubScreen :
	public TrendSubScreen, 
	public TrendEventTarget
{
public:
	TrendGraphsSubScreen(SubScreenArea* pSubScreenArea);
	~TrendGraphsSubScreen(void);

	// TrendSubScreen pure virtual
	virtual void updateCursorPosition(void);
	virtual void disableScrolling(void);
	virtual void update(void);

	// SubScreen virtuals via TrendSubScreen
	virtual void activate(void);
	virtual void deactivate(void);

	// ButtonTarget virtuals via TrendSubScreen
	virtual void buttonDownHappened(Button *pButton,
									Boolean byOperatorAction);
	virtual void buttonUpHappened(Button *pButton,
								  Boolean byOperatorAction);

	// SettingObserver virtual via TrendSubScreen
	void valueUpdate(const Notification::ChangeQualifier qualifierId,
					 const SettingSubject* pSubject);

	// TrendEventTarget virtual method
	virtual void trendDataReady(TrendDataSet& rTrendDataSet);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:
	// TrendSubScreen pure virtual
	virtual Boolean isAnyButtonPressed_(void) const;
	virtual void requestTrendData_(void);

private:
	// these methods are purposely declared, but not implemented...
	TrendGraphsSubScreen(void);						// not implemented
	TrendGraphsSubScreen(const TrendGraphsSubScreen&);	  // not implemented...
	void operator=(const TrendGraphsSubScreen&);	// not implemented...

	void updateEventDrawables_(void);

	enum
	{
		NUM_TREND_PLOTS_ = 3,
		NUM_TREND_SETTING_BUTTONS = 5
	};

	//@ Data-Member: tableScreenNavButton_
	// The button to switch to the Trend table view
	TextButton tableScreenNavButton_;

	//@ Data-Member: presetsButton_
	// The button to automatically populate the trend parameters
	// with preset values.
	DropDownSettingButton presetsButton_;

	//@ Data-Member: printButton_
	// The button to print the screen
	TextButton printButton_;

	//@ Data-Member: printingText_;
	// The text that flashes the 'PRINTING' message
	TextField printingText_;

	//@ Data-Member: trendPlot1_
	// Trend Plot 1
	TrendPlot trendPlot1_;

	//@ Data-Member: trendPlot2_
	// Trend Plot 2
	TrendPlot trendPlot2_;

	//@ Data-Member: trendPlot3_
	// Trend Plot 3
	TrendPlot trendPlot3_;

	//@ Data-Member: trendPlot3_
	// Trend Plot 3
	TrendPlot* trendPlots_[NUM_TREND_PLOTS_];

	//@ Data-Member: timeScaleButton_
	// The button to adjust the time scale for the time plots.
	ScrollableMenuSettingButton timeScaleButton_;

	//@ Data-Member: trendSelectButton1_
	// Trend parameter setting button
	ScrollableMenuSettingButton  trendSelectButton1_;

	//@ Data-Member: trendSelectButton2_
	// Trend parameter setting button
	ScrollableMenuSettingButton  trendSelectButton2_;

	//@ Data-Member: trendSelectButton3_
	// Trend parameter setting button
	ScrollableMenuSettingButton  trendSelectButton3_;

	//@ Data-Member: eventDetailButton_
	// EVENT Detail button - active when cursor is on an event
	// displays the event detail log (pop-up window) when touched
	FocusButton  eventDetailButton_;

	//@ Data-Member: eventContainer_
	// Trend EVENT container - contains the event box, label and cell
	Container   eventContainer_;

	//@ Data-Member: eventBox_
	// Trend EVENT box inside the eventContainer_ 
	Box   eventBox_;

	//@ Data-Member: eventLabel_
	// The "EVENT NUMBERS" label inside the eventContainer_
	TextField   eventLabel_;

	//@ Data-Member: eventCell_
	// Trend EVENT cell containing the event numbers
	LogCell   eventCell_;

	//@ Data-Member:  arrSettingButtonPtrs_
	// The subscreen buttons.
	SettingButton*  arrSettingButtonPtrs_[NUM_TREND_SETTING_BUTTONS + 1];

	//@ Data-Member: trendCursorSlider_
	// A slider that contains the trend cursor button and timeline drawables
	TrendCursorSlider trendCursorSlider_;

	//@ Data-Member: pDataValue_
	// Text field for the Trend Data Value
	TextField pAvgTrendDataValue_[NUM_TREND_PLOTS_];

	//@ Data-Member: datasetColumn_
	// The column number in the dataset used for each plot.
	Int32 datasetColumn_[NUM_TREND_PLOTS_];

	//@ Data-Member: isDataRequestPending_
	// TRUE if a data request has been issued to the TrendDataSetAgent
	Boolean isDataRequestPending_;

	//@ Data-Member: isCallbackActive_
	// TRUE if a menu button callback is active that will inhibit data refresh
	Boolean isCallbackActive_;

	//@ Data-Member: isPlotErased_
	// latched TRUE when a trendPlot is erased due to a setting change
	Boolean isPlotErased_[NUM_TREND_PLOTS_];

	//@ Data-Member: areAllPlotsErased_
	// latched TRUE when all trend plots are erased due to a setting change
	Boolean areAllPlotsErased_;

};

#endif // TrendGraphsSubScreen_HH 
