// MultiTimeScptExecDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Swat.h"
#include "MultiTimeScptExecDlg.h"


// CMultiTimeScptExecDlg dialog

IMPLEMENT_DYNAMIC(CMultiTimeScptExecDlg, CDialog)

CMultiTimeScptExecDlg::CMultiTimeScptExecDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CMultiTimeScptExecDlg::IDD, pParent)
	, m_nNumberTimes(0)
{

}

CMultiTimeScptExecDlg::~CMultiTimeScptExecDlg()
{
}

void CMultiTimeScptExecDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_MULTI_TIMES, m_nNumberTimes);
	DDV_MinMaxInt(pDX, m_nNumberTimes, 1, 9999);
}


BEGIN_MESSAGE_MAP(CMultiTimeScptExecDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CMultiTimeScptExecDlg::OnBnClickedOk)
END_MESSAGE_MAP()


// CMultiTimeScptExecDlg message handlers

void CMultiTimeScptExecDlg::OnBnClickedOk()
{
	// TODO: Add your control notification handler code here
	OnOK();
}
