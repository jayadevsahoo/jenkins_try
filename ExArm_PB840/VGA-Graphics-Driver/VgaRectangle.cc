#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1994-1995, Puritan-Bennett Corporation
//=====================================================================  

// =========================== M O D U L E   D E S C R I P T I O N ====
//@ Class: VgaRectangle - VGA line drawing routines.
//---------------------------------------------------------------------
//@ Interface-Description
//  This module contains the rectangle drawing routines for the VGA graphics
//  library.  This includes a public method for drawing a filled rectangle,
//  and two private methods used by VgaSuperBitmap for drawing and reading
//  a bitmap.
//---------------------------------------------------------------------
//@ Rationale
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ $Header:   /840/Baseline/VGA-Graphics-Driver/vcssrc/VgaRectangle.ccv   25.0.4.0   19 Nov 2013 14:38:06   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002   By: gdc    Date:  27-Jan-2000    DCS Number: 5337
//  Project: 840 Neonatal
//  Description:
//	Optimized VGA library for Neonatal project to use BitBlt and Windows
//  Bitmap structures.
//
//  Revision: 001   By: sah   Date: 09-Jun-1997   DCS Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//      Initial version to coding standards (Integration baseline).
//
//=====================================================================  

#include "VgaRectangle.hh" 

//@ Usage-Classes
#include "VgaDevice.hh" 
#include "DisplayContext.hh"
#include "GraphicsContext.hh"
//@ End-Usage

//=====================================================================  
//
//  Public Methods...
//
//=====================================================================  

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  FillBox
//
//@ Interface-Description
//  Fill a solid rectangular area with one color.
//  Box is specified with corner coordinates.
//
//  NOTE:  This is a filled box. No border is drawn. The box is filled
//         to the very edge.  Therefore if you call for a box of zero
//         width, you should get a line.  A box of zero width and zero
//         height should result in a point.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Checks clipping boundries, adjusts if necessary.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================  

void
VgaRectangle::FillBox(DisplayContext & rDc, GraphicsContext & rGc,
	Int x0, Int y0, Int x1, Int y1 )
{
  Int tmp;
 
  if (x0 > x1) 
  {   // $[TI1] -- swap 'x' points...
    tmp = x0;
    x0  = x1;
    x1  = tmp;
  }   // $[TI2]

  if (y0 > y1) 
  {   // $[TI3] -- swap 'y' points...
    tmp = y0;
    y0  = y1;
    y1  = tmp;
  }   // $[TI4]

  if (x0 <= rGc.getClipX1()  &&  y0 <= rGc.getClipY1()  &&
      x1 >= rGc.getClipX0()  &&  y1 >= rGc.getClipY0())
  {   // $[TI5] -- within clipping bounds...
    if (rGc.getClipX0() > x0) 
    {   // $[TI5.1]
      x0 = rGc.getClipX0();
    }   // $[TI5.2]

    if (rGc.getClipX1() < x1) 
    {   // $[TI5.3]
      x1 = rGc.getClipX1();
    }   // $[TI5.4]

    if (rGc.getClipY0() > y0) 
    {   // $[TI5.5]
      y0 = rGc.getClipY0();
    }   // $[TI5.6]

    if (rGc.getClipY1() < y1) 
    {   // $[TI5.7]
      y1 = rGc.getClipY1();
    }   // $[TI5.8]

    Int width  = x1 - x0 + 1;
    Int height = y1 - y0 + 1;
	
	rDc.fillSolid(x0, y0, width, height, rGc.getForegroundColor());

  }   // $[TI6] -- NOT within clipping bounds...

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
VgaRectangle::SoftFault(const SoftFaultID softFaultID,
			const Uint32      lineNumber,
			const char*       pFileName,
			const char*       pPredicate)
{
  FaultHandler::SoftFault(softFaultID, VGA_GRAPHICS_DRIVERS,
			  VgaGraphicsDriver::VGA_RECTANGLE_ID, 
			  lineNumber, pFileName, pPredicate);
}


//=====================================================================  
//
//  Private Methods...
//
//=====================================================================  
