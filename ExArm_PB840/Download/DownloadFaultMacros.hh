#ifndef DownloadFaultMacros_HH
#define DownloadFaultMacros_HH

#ifdef FaultHandlerMacros_HH
@pragma error #include DownloadFaultMacro.hh before FaultHandlerMacros.hh
#endif

// define FaultHandlerMacros_HH to override Utilities/FaultHandlerMacros.hh
#define FaultHandlerMacros_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1996, Nellcor/Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
// Filename:  DownloadFaultMacros - Fault-Handler Macros.
//---------------------------------------------------------------------
//@ Version
//@(#) $Header:   /840/Baseline/Download/vcssrc/DownloadFaultMacros.hhv   25.0.4.0   19 Nov 2013 14:02:48   pvcs  $ 
//
//@ Modification-Log
//
//   Revision: 002   By: gdc   Date: 15-AUG-1997   DR Number: 2197
//   Project:  Sigma (R8027)
//   Description:
//	Force include DownloadFaultMacro.hh before Utilities/FaultHandlerMacro.hh
//  to override Utilities fault handling with Download subsystem
//  fault handling.  Allows other subsystem includes to use system 
//  standard includes.
//
//   Revision: 001   By: jhv   Date: 24-Jan-1996   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//=====================================================================

//=====================================================================
//
// Reporting Macros...
//
//=====================================================================

//=====================================================================
// Reporting macro for CLASS pre-conditions, invariants and general
// assertions...
//=====================================================================

#ifndef SIGMA_PRODUCTION

// These macros generate a software fault during the development
// phase since the system restart process is not well thought out
// as the Sigma Application environment.

#  define  REPORT_CLASS_ASSERTION(softFaultId, boolExpr)	\
	     SoftFault(softFaultId, __LINE__, __FILE__, #boolExpr);
#else
// when in 'PRODUCTION' mode, the filename and stringized boolean
// expression are NOT passed to the fault-handling utility...
#  define  REPORT_CLASS_ASSERTION(softFaultId, boolExpr)	\
	     SoftFault(softFaultId, __LINE__);
#endif // defined(SIGMA_DEVELOPMENT)

//=====================================================================
//
// Testing Macros...
//
//=====================================================================

//=====================================================================
// Pre-Condition Testing Macros...
//=====================================================================

//@ Macro:  CLASS_PRE_CONDITION(boolExpr)
// This macro provides a test of the pre-condition identified by 'boolExpr'
// for a class.
// $[TI1]
#define CLASS_PRE_CONDITION(boolExpr)				\
	{							\
	  if (!(boolExpr))					\
	  {							\
	    REPORT_CLASS_ASSERTION(PRE_CONDITION, boolExpr);	\
	  }							\
	  else						\
	  {							\
	  }							\
	}


//=====================================================================

//@ Macro:  CLASS_ASSERTION(boolExpr)
// This macro provides a test of the assertion identified by 'boolExpr' for
// a class.
// $[TI1]
#define CLASS_ASSERTION(boolExpr)				\
	{							\
	  if (!(boolExpr))					\
	  {							\
	    REPORT_CLASS_ASSERTION(ASSERTION, boolExpr);	\
	  }							\
	  else						\
	  {							\
	  }							\
    }

//=====================================================================
// Forced Assertion Macros...
//=====================================================================

//@ Macro:  CLASS_ASSERTION_FAILURE()
// This macro provides a forced assertion identified for a class.
#define CLASS_ASSERTION_FAILURE()				\
	{							\
	  REPORT_CLASS_ASSERTION(ASSERTION, (FALSE));	\
	}

#if defined(SIGMA_DEVELOPMENT)

#  define  REPORT_FREE_ASSERTION(FaultId, subSystemId, freeId, boolExpr)\
	     DownloadFault::SoftFault(FaultId, subSystemId, freeId,	\
				     __LINE__, __FILE__, #boolExpr);
#else
// when in 'PRODUCTION' mode, the filename and stringized boolean
// expression are NOT passed to the fault-handling utility...
#  define  REPORT_FREE_ASSERTION(softFaultId, subSystemId, freeId, boolExpr)\
	     DownloadFault::SoftFault(softFaultId, subSystemId, freeId,	\
				     __LINE__);
#endif // defined(SIGMA_PRODUCTION)

//@ Macro:  FREE_ASSERTION(boolExpr, subSystemId, freeId)
// This macro provides a test of the assertion identified by 'boolExpr'
// for the free-function identified by 'subSystemId' and 'freeId'.
// $[TI1]
#define FREE_ASSERTION(boolExpr, subSystemId, freeId)	\
	{							\
	  if (!(boolExpr))					\
	  {							\
	    REPORT_FREE_ASSERTION(ASSERTION,			\
				  subSystemId, freeId,		\
				  boolExpr);			\
	  }							\
	  else						\
	  {							\
	  }							\
    }

//@ Macro:  FREE_ASSERTION_FAILURE(subSystemId, freeId)
//   Always generate an assertion failure.
#define FREE_ASSERTION_FAILURE( subSystemId, freeId)	\
	{							\
	    REPORT_FREE_ASSERTION(ASSERTION,		\
				  subSystemId, freeId,		\
				  boolExpr);			\
    }
#endif  // DownloadFaultMacros_HH
