
//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file ICConfiguration.h
//----------------------------------------------------------------------------


//-------------------------------------
// C L A S S  D E C L
//-------------------------------------

/// Container of all the configuration parameters.
#pragma once
class IConfiguration{


    public:

	/// Read in a context from memory.
	/// 
	/// @param ptr A pointer to a context.
	/// @param iSize Size of context.
	/// 
	/// @return 
    virtual const bool readContext(const char *ptr, const int iSize) = 0;


	///@{
	/// Actions that can be preformed on a context.

    ///TODO Make private
    virtual const int  makeContext(char *pBuffer, const int iLength) const    = 0;
    virtual const bool printContext() const                                   = 0;
    virtual const char * getCsv () const                                      = 0; 
    virtual const bool writeContextFile(const char *fileName)            = 0;
    virtual const bool loadContextFile(const char *fileName)                  = 0;
    ///@}

	/// Write out an XML settings file.
	///
	/// @param fileName A pointer to a string containing the filename. 
	/// 
	/// @return true if able to write out an XML config file, otherwise false.
    virtual const bool writeXMLFile(const char fileName) const                = 0;


	/// Load settings into configuration from an XML document. 
	///
	/// @param fileName A pointer to a string containing the filename.
	/// 
	/// @return true if able to load settings from XML, otherwise false.
    virtual const bool loadXMLDocument(const char *fileName)                  = 0;

	///@{
	/// Set a parameter givin parmeter name and value.
	/// 
	/// @return true if was able to set the parameter. 
    virtual const bool setParam(const char *pParamName, const char *pszValue) = 0;
    virtual const bool setParam(const int iValue, const char *pszValue)       = 0;
    virtual const bool setParam(const int iValue, const float fValue)         = 0;
    virtual const bool setParam(const char *pParamName, const float fValue)   = 0;
	///@}

    virtual void destroy()                                                    = 0;

	 virtual int sendSettings(char **str, char *pHost, char *pPort)             = 0;

//    static IConfiguration *getInstance();

    
    virtual ~IConfiguration() = 0;

};


typedef IConfiguration   * (IConfiguration_F)() ;

extern "C" __declspec(dllexport) IConfiguration * __cdecl getConfigurationInstance();

