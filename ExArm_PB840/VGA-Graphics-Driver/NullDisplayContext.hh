#ifndef NullDisplayContext_HH
#define NullDisplayContext_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2001, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: NullDisplayContext -- Display context with no display.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/VGA-Graphics-Driver/vcssrc/NullDisplayContext.hhv   25.0.4.0   19 Nov 2013 14:37:32   pvcs  $
//
//@ Modification-Log
// 
//  Revision: 003   By: gdc    Date: 01-Feb-2007     SCR Number: 6237
//  Project: Trend
//  Description:
//  Trend project related changes. Moved the no-operation methods from
//  NullDisplayContext to the base class DisplayContext so this class 
//  won't have to be modified every time a method is added to 
//  DisplayContext.
//
//  Revision: 002    By: erm   Data: 15-Oct-2001     DCS Number: 5959
//  Project: GUIComms
//  Description:
//  Code CleanUp
//
//  Revision: 001    By: gdc   Date: 27-Sep-2001     DCS Number: 5493
//  Project: GuiComms
//  Description:
//  Initial Version.
//
//====================================================================

#include "VgaGraphicsDriver.hh"
#include "DisplayContext.hh"

//@ Usage-Classes
//@ End-Usage

class GraphicsContext;

class NullDisplayContext : public DisplayContext
{
  public:
	NullDisplayContext( Uint32 width, Uint32 height, Uint32 bitsPerPixel );
	
	virtual ~NullDisplayContext();

  private:
	NullDisplayContext(); //declared only
    NullDisplayContext(const NullDisplayContext&); // declared only
	void operator=(const NullDisplayContext&); // declared only
};

#endif // NullDisplayContext_HH 
