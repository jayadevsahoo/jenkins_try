
#ifndef PcvPhase_HH
#define PcvPhase_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: PcvPhase - Implements pressure control ventilation phase.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/PcvPhase.hhv   25.0.4.0   19 Nov 2013 14:00:02   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 002 By: syw    Date: 21-Feb-1996   DR Number: DCS 674
//  	Project:  Sigma (R8027)
//		Description:
//			Removed #ifndef around #include.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"

//@ Usage-Classes

#include "SettingId.hh"
#include "PressureBasedPhase.hh"

//@ End-Usage

class PcvPhase : public PressureBasedPhase
{
  public:
    PcvPhase( const SettingId::SettingIdType inspPressId,
    	      const SettingId::SettingIdType inspTimeId,
    	      const SettingId::SettingIdType flowAccelPercentId) ;
    virtual ~PcvPhase( void) ;

    static void SoftFault( const SoftFaultID softFaultID,
							   const Uint32      lineNumber,
							   const char*       pFileName  = NULL, 
							   const char*       pPredicate = NULL) ;
  
  protected:
	virtual void determineEffectivePressureAndBiasOffset_( void) ;
	virtual void determineTimeToTarget_( void)  ;
	virtual void determineFlowAccelerationPercent_( void) ;
	virtual void enableExhalationTriggers_( void) ;

  private:
    PcvPhase( void) ;						// not implemented...
    PcvPhase( const PcvPhase&) ;			// not implemented...
    void   operator=( const PcvPhase&) ;	// not implemented...

	//@ Data-Member: inspPressId_
	// apnea or normal inspPressId
	SettingId::SettingIdType inspPressId_ ;

	//@ Data-Member: inspTimeId_
	// apnea or normal inspTimeId
	SettingId::SettingIdType inspTimeId_ ;

	//@ Data-Member: flowAccelPercentId_
	// apnea or normal flowAccelPercentId
	SettingId::SettingIdType flowAccelPercentId_ ;

	//@ Data-Member: inspTimeMs_
	// apnea or normal inspTimeMs
	Real32 inspTimeMs_ ;
} ;


#endif // PcvPhase_HH 
