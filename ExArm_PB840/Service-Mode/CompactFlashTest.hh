#ifndef CompactFlashTest_HH
#define CompactFlashTest_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: CompactFlashTest - Performs the compact flash test
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/CompactFlashTest.hhv   25.0.4.0   19 Nov 2013 14:23:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By:  gdc    Date:  26-Mar-2007    SCR Number: 6237
//  Project:  Trend
//  Description:
//      Initial version.
//====================================================================

#include "ServiceMode.hh"

//@ Usage-Classes
//@ End-Usage

class CompactFlashTest 
{
public:

	CompactFlashTest( void) ;
	~CompactFlashTest( void) ;

	void runTest( void) ;

	static CompactFlashTest& GetCompactFlashTest(void);

	static void SoftFault( const SoftFaultID softFaultID,
						   const Uint32 lineNumber,
						   const char *pFileName  = NULL, 
						   const char *pPredicate = NULL) ;
private:

	CompactFlashTest( const CompactFlashTest&) ;					   // not implemented
	void  operator=( const CompactFlashTest&) ;						   // not implemented 

};


#endif // CompactFlashTest_HH 
