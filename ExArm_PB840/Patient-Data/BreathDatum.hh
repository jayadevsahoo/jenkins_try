
#ifndef BreathDatum_HH
#define BreathDatum_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Filename:  BreathDatum - data structure that defines a complete
//            set of any patient data type. 
//---------------------------------------------------------------------
//@ Version
// @(#) $Header::   /840/Baseline/Patient-Data/vcssrc/BreathDatum.hhv   10.7   08/17/07 10:27:14   pvcs  
//
//@ Modification-Log
//
//  Revision: 001   By: gdc      Date:  16-May-2006    SCR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project related changes.
//
//  Revision: 000  By:  jhv    Date:  28-Jan-95    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//            Initial version 
//  
//====================================================================

#include "BoundedValue.hh"
#include "DiscreteValue.hh"

struct BoundedBreathDatum
{
    BoundedValue                 data;
    BoundedValue::ClippingState  clippingState;
    Boolean                      timedOut;
};

struct DiscreteBreathDatum
{
    DiscreteValue  data;
    Boolean timedOut;
};



union BreathDatum 
{
  BoundedBreathDatum   bValue;
  DiscreteBreathDatum  dValue;
};


/*****
#if defined(SIGMA_UNIT_TEST)
class  ostream;  // forward declaration...
ostream&  operator<<(ostream& ostr, const BreathDatum& patientItemValue);
#endif  // defined(SIGMA_UNIT_TEST)
*****/


#endif // BreathDatum_HH 
