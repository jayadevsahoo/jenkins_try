#pragma once

#define     MAX_NETWORK_BUF_SIZE            (2000)
#define     TOUCHED_POS_BROADCAST_PORT      (7018)
#define     TOUCHED_POS_DATA                (9950)

#define     Uint16  unsigned short
#define     Uint32  unsigned int
#define     Int16   short

struct NetworkMsgHeader
{
  //@ Data-Member: msgId;
  // msgId identifies the type of the network message of the following
  // packet.
  Uint16  msgId;

  //@ Data-Member: pktSize;
  // pktSize indicates size of the following network packet in bytes.
  Uint16 pktSize;

  //@ Data-Member seqNumber
  //  Sequence number for the following network packet.
  Uint32 seqNumber;
};


typedef struct  XBuf
{
    NetworkMsgHeader header;
	char data[MAX_NETWORK_BUF_SIZE];
} XmitBuf;


struct TouchedPosition
{
    Int16 x;
    Int16 y;
};