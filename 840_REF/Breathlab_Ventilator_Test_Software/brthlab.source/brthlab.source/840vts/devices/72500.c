/*****************************************************************************************
 *   Part Number:   72500
 *   Module Name:   72500.c                                                             
 *   Purpose:       This module is the device driver for the PTS2000 measurement        
 *                  system it provides the measurement of pressure, flow, Oxygen %     
 *                  Barometric pressure                                                  
 *                                                                                       
 *   Requirements:  This module implements requirement detailed in 72500-93              
 *                                                                                        
 *   $Revision::   1.2 $                                                                         
 *                                                                      
 *
 *   History:
 *                                                                         
 *   Author: 		Danny L. Lamb Test Engineering                                              
 *   Release: 		Original Release
 *
 *   REV         DESCRIPTION
 *  ____        ___________________
 *   A          ORIGINAL RELEASE                                                          
 *   B          Added check sum to return data for new firmware support                                            
 *   C          Added Zero functions to the low and High Pressure ports    
 *
 *
 *   Modification History:
 *
 *   Description of Change			        By			      Date 
 *   ---------------------					-----   		  ------
 *	 Critical Observations during 			Kathirvel(CGS)    27 Sep,2000
 *	 Phase1 is incorportated. 
 *      
 *   Modified several routines for PTS 2000 serial communications so as to process 
 *   not just the CarriageReturn character at the end of a message, but also the LineFeed 
 *   which was being left behind in the incoming buffer.  Routines affected include 
 *   AdjOxy21, AdjOxy100, SetBarPrsOn, SetPTSSmplRate, ZeroHighPressure, and ZeroLowPressure.
 *                                          Dave Richardson   18 Nov, 2002
 *
 *   THIS IS A PROPRIETARY WORK TO WHICH PURITAN-BENNETT CORPORATION CLAIMS               
 *   EXCLUSIVE RIGHT.  NO PART OF THIS WORK MAY BE USED, DISCLOSED, REPRODUCED,           
 *   STORED IN AN INFORMATION RETIEVAL SYSTEM, OR TRANSMITTED, IN ANY FORM OR BY ANY      
 *   MEANS ELECTRONIC, MECHANICAL, PHOTOCOPYING, RECORDING OR OTHERWISE WITHOUT THE       
 *   PRIOR WRITTEN PERMISSION OF PURITAN-BENNETT CORPORATION.                             
 *                                                                                         
 *   Copyright (c) 1995-2002, Puritan-Bennett
 *   All rights reserved.                                                                                                                                                * 
 *                                                                                        
 *                                                                                        
 *****************************************************************************************/

/*************** Include files **************/
#include <ansi_c.h>
#include <rs232.h>
#include <userint.h>
#include <utility.h>
#include "72500.h"
#include "pts.h"

#define ERROR         1
#define NOERROR       0

/********* Varaible Declarations **********/


#include "mainpnls.h"				  

#define LINEFEED        0x0A
#define CARRIAGE_RETURN 0x0D
#define NAK             0x15
#define ACK             0x06

#ifndef NULL
#define NULL 0
#endif


#ifndef FALSE
#define FALSE 0
#endif

#ifndef TRUE
#define TRUE 1
#endif


#define OFF 0
#define ON  1

#define POSITIVE  1
#define NEGATIVE -1

#define SUCCESS 0

#define CVI_NOERROR           0
#define CVI_ERROR            -1
#define CVI_INVALID_PORTNUM  -2
#define CVI_IO_TIMEOUT      -99
#define CVI_CHECKSUM_ERROR -265

char   szSerialNumber[SERIAL_NUM_LENGTH] = "41*DEMO *";      // default provided for simulator
char   szVersionNumber[VERSION_NUM_LENGTH] = "Revision 3.5"; // default provided for simulator
static double dVersionNumber = 3.5;
char   szCalDate[CAL_DATE_LENGTH] = "01/01/2000";            // default provided for simulator
char   szNextCalDate[CAL_DATE_LENGTH] = "01/01/2001";        // default provided for simulator

/***************************************************************************************
* Name: reverse                                                                        *
* Purpose: Reverse a string                                                            *
* Example Call: reverse(s)                                                             *
* Input: char s[] = address of string to reverse                                       *
* Return: void                                                                         *
* Global: NONE                                                                         *
***************************************************************************************/

static void reverse(char s[])
{ // reverse string s in place
    int c, i, j;

    for( i = 0, j = strlen(s)-1; i<j; i++, j-- )
    {
        c = s[i];
        s[i] = s[j];
        s[j] = c;
    }
}

/***************************************************************************************
* Name: itoa                                                                           *
* Purpose: Convert an integer to ascii text                                            *
* Example Call: itoa(n, s)                                                             *
* Input: int n = integer to convert                                                    *
*        char s[] = address of string space for result                                 *
* Return: void                                                                         *
* Global: NONE                                                                         *
***************************************************************************************/

char *itoa(int n, char s[])
{ // convert n to characters in s
    int i, sign;

    if( (sign = n) < 0 )
        n = -n;
    i = 0;
    do
    {
        s[i++] = n % 10 + '0';
    }
    while( (n /= 10) > 0);
    if(sign < 0)
        s[i++] = '-';
    s[i] = '\0';
    reverse(s);

    return s;
}


/***************************************************************************************
* Name: IsNextCalOverdue                                                               *
* Purpose: Determines if the next calibration date has passed or not.			       *
* Example Call: IsNextCalOverdue( NextCalDate)                                         *
* Input: char *NextCalDate = address of storage area for Next CalDate                  *
* Return: 0: date is not past, 1:date has passed                                       *
* Global: NONE                                                                         *
***************************************************************************************/

int IsNextCalOverdue(char *NextCalDate)
{
    char *szMonth = "MM", *szDay = "DD", *szYear = "YYYY";
    int nMonth, nDay, nYear;
	int currMonth, currDay, currYear;
    char cBuf[5];
    char NextCalDateCopy[CAL_DATE_LENGTH];
    char *pToken;
    char *DateSeparators = "/-";

    // Y2K Compliant
    strcpy(NextCalDateCopy, NextCalDate);
    pToken = strtok(NextCalDateCopy, DateSeparators);
    if(strcmp(pToken, "Invalid") == 0)
    	return 0;
    	
    if(pToken != NULL)
    {
        strcpy(szMonth, pToken);
        nMonth = atoi(szMonth);
    }
    pToken = strtok(NULL, DateSeparators);
    if(pToken != NULL)
    {
        strcpy(szDay, pToken);
        nDay = atoi(szDay);
    }
    if(nMonth == 2 && nDay == 29)
    { // If last calibration on Feb. 29th (Leap year)
        // Next cal due on Feb. 28th of the following year
        strcpy(szDay, itoa(28, cBuf));
    }
    pToken = strtok(NULL, DateSeparators);
    if(pToken != NULL)
    {
        strcpy(szYear, pToken);
        nYear = atoi(szYear);
    }
    strcpy(szYear, itoa(nYear, cBuf));
	GetSystemDate(&currMonth, &currDay, &currYear);
    if(currYear>nYear)
    	return 1;
    if(currMonth>nMonth && currYear >= nYear)
    	return 1;
    if(currDay>nDay && currMonth>=nMonth && currYear >= nYear)
    	return 1;
    // assemble and store Next PTS Calibration Due Date
//    sprintf(NextCalDate, "%s/%s/%s", szMonth, szDay, szYear);

	return 0;
}

/***************************************************************************************
* Name: SetPTSNextCalDate                                                              *
* Purpose: Calculates and stores the next CalDate based on a given date + 1 year       *
* Example Call: SetPTSNextCalDate(CalDate, NextCalDate)                                *
* Input: char *CalDate = address of storage area for CalDate                           *
* Input: char *NextCalDate = address of storage area for Next CalDate                  *
* Return: void                                                                         *
* Global: NONE                                                                         *
***************************************************************************************/

void SetPTSNextCalDate(char *CalDate, char *NextCalDate)
{
    char *szMonth = "MM", *szDay = "DD", *szYear = "YYYY";
    int nMonth, nDay, nYear;
    char cBuf[5];
    char szCalDateCopy[CAL_DATE_LENGTH];
    char *pToken;
    char *DateSeparators = "/-";

    // Y2K Compliant
    strcpy(szCalDateCopy, CalDate);
    pToken = strtok(szCalDateCopy, DateSeparators);
    if(pToken != NULL)
    {
        strcpy(szMonth, pToken);
        nMonth = atoi(szMonth);
    }
    pToken = strtok(NULL, DateSeparators);
    if(pToken != NULL)
    {
        strcpy(szDay, pToken);
        nDay = atoi(szDay);
    }
    if(nMonth == 2 && nDay == 29)
    { // If last calibration on Feb. 29th (Leap year)
        // Next cal due on Feb. 28th of the following year
        strcpy(szDay, itoa(28, cBuf));
    }
    pToken = strtok(NULL, DateSeparators);
    if(pToken != NULL)
    {
        strcpy(szYear, pToken);
        nYear = atoi(szYear);
    }
    // add 1 to year
    nYear = atoi(szYear) +1;
    strcpy(szYear, itoa(nYear, cBuf));
    // assemble and store Next PTS Calibration Due Date
    sprintf(NextCalDate, "%s/%s/%s", szMonth, szDay, szYear);
    return; // Modified the statement due to Critical Observation.
}

/***************************************************************************************
* Name: GetPTS_CalDate                                                                 *
* Purpose: Obtains CalDate from a PTS 2000 device                                      *
* Example Call: result = GetPTS_CalDate(buffer)                                        *
* Input: char *buffer = address of storage area for CalDate                            *
* Return: int CVI_NOERROR = success                                                    *
*             CVI_ERROR = incorrect CalDate format or failure while sending command    *
*             negative other than CVI_ERROR = communications failure while reading     *
* Global: NONE                                                                         *
***************************************************************************************/

int GetPTS_CalDate(char *buffer)
{
    int i;
    int nByte = CAL_DATE_LENGTH_VALID;
    char cBuf[255];
    char *Month = "MM", *Day = "DD", *Year = "YYYY";
    int status;

    FlushInQPTS();
    FlushOutQPTS();

    /* Send the command to the PTS 2000 */
    if(SendPTS("V\r",2) != SUCCESS)
        return CVI_ERROR;

	// Read PTS 2000 response
	status = RecvPTS(buffer, CAL_DATE_LENGTH, &nByte);
	if(status != SUCCESS)
		return CVI_ERROR;
		
	if(nByte!= CAL_DATE_LENGTH_VALID)
		return nByte;

	buffer[nByte-2]='\0';

    return CVI_NOERROR;
}


/***************************************************************************************
* Name: AdjOxy21                                                                       *
* Purpose: Sends a command to a PTS 2000 device to adjust the oxygen sensor for 21%    *
* Example Call: result = AdjOxy21()                                                    *
* Input: void                                                                          *
* Return: int result CVI_NOERROR = success                                             *
*             result CVI_ERROR = fail                                                  *
* Global: NONE                                                                         *
***************************************************************************************/

int AdjOxy21(void)
{
	int b1, b2, b3;

	FlushInQPTS();

	if(SendPTS("or\r",3) != SUCCESS)
		return CVI_ERROR;

	Delay(1);

	if( (b1 = RecvPTSByte()) < 0) // Acknowledge byte
		return CVI_ERROR;
	if( (b2 = RecvPTSByte()) < 0) // carriage return (end of message)
		return CVI_ERROR;
	if( (b3 = RecvPTSByte()) < 0) // linefeed (end of message)
		return CVI_ERROR;

	if(((unsigned char)b1&0xff) == ACK)
		return NOERROR;
	else
		return ERROR;
}


/***************************************************************************************
* Name: AdjOxy100                                                                      *
* Purpose: Sends a command to a PTS 2000 device to adjust the oxygen sensor for 100%   *
* Example Call: result = AdjOxy100()                                                   *
* Input: void                                                                          *
* Return: int result CVI_NOERROR = success                                             *
*             result CVI_ERROR = fail                                                  *
* Global: NONE                                                                         *
***************************************************************************************/

int AdjOxy100(void)
{
    int b1, b2, b3;

    FlushInQPTS();

    if(SendPTS("oh\r",3) != SUCCESS)
        return CVI_ERROR;

    Delay(1);

    if( (b1 = RecvPTSByte()) < 0) // Acknowledge byte
        return CVI_ERROR;
    if( (b2 = RecvPTSByte()) < 0) // carriage return (end of message)
        return CVI_ERROR;
    if( (b3 = RecvPTSByte()) < 0) // linefeed (end of message)
        return CVI_ERROR;

    if(((unsigned char)b1&0xff) == ACK)
        return NOERROR;
    else
        return ERROR;
}

/***************************************************************************************
* Name: GetPTS_SoftwareRev                                                             *
* Purpose: Obtains Software Revision from a PTS 2000 device                            *
* Example Call: result = GetPTS_SoftwareRev(buffer)                                    *
* Input: char *buffer = address of storage area for Software Revision                  *
* Return: int CVI_NOERROR = success                                                    *
*             CVI_ERROR = incorrect Revision format or failure while sending command   *
*             negative other than CVI_ERROR = communications failure while reading     *
* Global: NONE                                                                         *
***************************************************************************************/

int GetPTS_SoftwareRev(char *buffer)
{
    int i;
    int nByte = VERSION_NUM_LENGTH_VALID;
    char *endptr;
    int status;

    FlushInQPTS();
    FlushOutQPTS();

    /* Send the command to the PTS 2000 */
    if(SendPTS("v\r",2) != SUCCESS)
        return CVI_ERROR;

    // Read PTS 2000 response:
	status = RecvPTS(buffer, VERSION_NUM_LENGTH, &nByte);
	if(status != SUCCESS)
		return CVI_ERROR;
	if(nByte!= VERSION_NUM_LENGTH_VALID)
		return nByte;
		
	buffer[nByte-2]='\0';

    dVersionNumber = strtod(buffer+9, &endptr);
    return CVI_NOERROR;
}
/***************************************************************************************
* Name: StopContinuousData                                                             *
* Purpose: Sends a command to a PTS 2000 device to stop sending data continuously      *
* Example Call: result = StopContinuousData()                                          *
* Input: void                                                                          *
* Return: int result CVI_NOERROR = command sent                                        *
*             result negative = fail                                                   *
* Global: NONE                                                                         *
***************************************************************************************/

int StopContinuousData(void)
{

    if(SendPTS("X\r",2) != SUCCESS)
        return CVI_ERROR;
    else
    {
        FlushInQPTS();
        return CVI_NOERROR;
    }
}

/***************************************************************************************
* Name: GetPTS_SerialID                                                                *
* Purpose: Obtains Serial ID from a PTS 2000 device                                    *
* Example Call: result = GetPTS_SerialID(buffer)                                       *
* Input: char *buffer = address of storage area for Serial ID                          *
* Return: int CVI_NOERROR = success                                                    *
*             CVI_ERROR = incorrect Serial format or failure while sending command     *
*             negative other than CVI_ERROR = communications failure while reading     *
* Global: NONE                                                                         *
***************************************************************************************/

int GetPTS_SerialID(char *buffer)
{
    int i;
    int nByte = SERIAL_NUM_LENGTH_VALID;
    char cBuf[255];
    int status;

    FlushInQPTS();
    FlushOutQPTS();

    /* Send the command to the PTS 2000 */
    if(SendPTS("s\r",2) != SUCCESS)
        return CVI_ERROR;

    // Read PTS 2000 response
	status = RecvPTS(buffer, SERIAL_NUM_LENGTH, &nByte);
	if(status != SUCCESS)
		return CVI_ERROR;
		
	if(nByte!= SERIAL_NUM_LENGTH_VALID)
		return nByte;
	buffer[nByte-2]='\0';

    return CVI_NOERROR;
}

/*******************************************************************
 * Function Name:PTS_Revision() Get PTS-2000 Driver Revision       * 
 *                                                                 * 
 * Function Purpose: To obtain driver revision                     * 
 *                                                                 * 
 * Synop: This function will return the PVCS revision string       *
 *        for the PTS-2000 driver                                  *
 *                                                                 * 
 * Variables Passed: None                                          * 
 *                                                                 * 
 * Variables Returned: PVCS revision string                        * 
 *                                                                 * 
 * Globals Affected: None                                          * 
 *                                                                 * 
 * Call:    rev_ptr = PTS_Revision();                              * 
 *******************************************************************/

char *PTS_Revision(void)
{

    return "$Revision::   1.2    $";

}


/*******************************************************************
 * Function Name: SetBarPrsOn									   *
 *																   *
 * Funtion Purpose: To set barmametric compensation to ON		   *
 *******************************************************************/
int SetBarPrsOn(void)
{
	int b1, b2, b3;

	FlushInQPTS();
	FlushOutQPTS();

	/* Send the command to the PTS 2000 */
	if(SendPTS("ap\r",3) != SUCCESS)
		return CVI_ERROR;

	if( (b1 = RecvPTSByte()) < 0) // Acknowledge byte
		return CVI_ERROR;
	if( (b2 = RecvPTSByte()) < 0) // carriage return (end of message)
		return CVI_ERROR;
	if( (b3 = RecvPTSByte()) < 0) // linefeed (end of message)
		return CVI_ERROR;

	if(((unsigned char)b1&0xff) == ACK)
		return NOERROR;
	else
		return ERROR;
}


int SetPTSSmplRate(int rate)
{
	char cBuf[16];
	int b1, b2, b3;

	FlushInQPTS();
	FlushOutQPTS();

	sprintf(cBuf, "SS%d\r", rate);
	if(SendPTS(cBuf, strlen(cBuf)) != SUCCESS)
		return CVI_ERROR;

	if( (b1 = RecvPTSByte()) < 0) // Acknowledge byte
		return CVI_ERROR;
	if( (b2 = RecvPTSByte()) < 0) // carriage return (end of message)
		return CVI_ERROR;
	if( (b3 = RecvPTSByte()) < 0) // linefeed (end of message)
		return CVI_ERROR;

	if(((unsigned char)b1&0xff) == ACK)
		return NOERROR;
	else
		return ERROR;
}

/*******************************************************************
 * Function Name: MeasureHighPressure                              * 
 *                                                                 * 
 * Function Purpose: To request a High pressure reading            * 
 *                                                                 *
 * Synop: This function will send a command string to the PTS2000  * 
 *        the instrument will then read the pressure and return    * 
 *        the reading a two binary words. This function will then  * 
 *        convert the reading to a floating point number and       * 
 *        assign it to the location passed to it.                  * 
 *                                                                 * 
 *                                                                 * 
 *                                                                 * 
 *                                                                 * 
 * Variables Passed: A pointer type double for the reading         * 
 *                                                                 * 
 *                                                                 * 
 *                                                                 * 
 * Variables Returned: error condition a non zero reading indicates* 
 *                     an error occured                            * 
 *                                                                 * 
 * Globals Affected: None                                          * 
 *                                                                 * 
 *                                                                 * 
 * Call:   error = MeasureHighPressure(&Measurment);               * 
 *******************************************************************/

int MeasureHighPressure(double *MeasuredVal)
{
    unsigned int reading;
    int b1, b2, b3;

    FlushInQPTS();

    if(SendPTS("P\r",2) != SUCCESS)
        return ERROR;

    b1 = RecvPTSByte();
    b2 = RecvPTSByte();
    b3 = RecvPTSByte();
    if((b1 < 0) || (b2 < 0) || (b3 < 0))
        return ERROR;

    reading = (unsigned int)b1;
    reading = ( (reading<<8) + ((((unsigned int)b2 ) & 0x00ff)));
    *MeasuredVal = (double)reading/100;

    if((b1+b2&0xff)== b3)
        return NOERROR;
    else
        return ERROR;
}

/*******************************************************************
 * Function Name: ZeroHighPressure                                 * 
 *                                                                 * 
 * Function Purpose: To Zero the High pressure Port                * 
 *                                                                 *
 * Synop: This function will send a command string to the PTS2000  * 
 *        the instrument will then Zero the  High pressure port    * 
 *        The PTS-2000 will return ACK or NAK status.              * 
 
 *                                                                 * 
 *                                                                 * 
 *                                                                 * 
 *                                                                 * 
 * Variables Passed: None                                          * 
 *                                                                 * 
 *                                                                 * 
 *                                                                 * 
 * Variables Returned: error condition a non zero reading indicates* 
 *                     an error occured                            * 
 *                                                                 * 
 * Globals Affected: None                                          * 
 *                                                                 * 
 *                                                                 * 
 * Call:   error = ZeroHighPressure();                             * 
 *******************************************************************/

int ZeroHighPressure(void)
{
	int b1, b2, b3;

	FlushInQPTS();

	if(SendPTS("Z\r",2) != SUCCESS)
		return CVI_ERROR;

	if( (b1 = RecvPTSByte()) < 0) // Acknowledge byte
		return CVI_ERROR;
	if( (b2 = RecvPTSByte()) < 0) // carriage return (end of message)
		return CVI_ERROR;
	if( (b3 = RecvPTSByte()) < 0) // linefeed (end of message)
		return CVI_ERROR;

	if(((unsigned char)b1&0xff) == ACK)
		return NOERROR;
	else
		return ERROR;
}

/*******************************************************************
 * Function Name: MeasureHighFlow()                                *  
 *                                                                 *  
 * Function Purpose: Measure High Flow Port selecting Air only     *  
 *                                                                 *  
 * Synop: This function will send a command string to the          *  
 *        PTS2000. Which will start the measurement of air         *  
 *        flow at the High flow port. The PTS2000 will use         *  
 *        an internal look-up table to convert the reading         *  
 *        based on air and then return the reading as two          *  
 *        binary bytes. This function will then convert the        *  
 *        reading into a floating point number assign it to        *  
 *        the location passed by the pointer.                      *  
 *                                                                 *  
 *                                                                 *  
 *                                                                 *  
 *                                                                 *  
 * Variables Passed: A pointer of type double used to return       *  
 *                   the reading                                   *  
 *                                                                 *  
 *                                                                 *  
 * Variables Returned: Interger type will be returned Non zero     *  
 *                     will indicate an error.                     *  
 *                                                                 *  
 * Globals Affected: None                                          *  
 *                                                                 *  
 *                                                                 *  
 * Call:   error = MeasureHighFlow(&Measurment);                   *  
 *******************************************************************/

int MeasureHighFlow(double *MeasuredVal)
{
    unsigned int reading;
    int b1, b2, b3;

    FlushInQPTS();

    if(SendPTS("F0\r",3) != SUCCESS)
        return ERROR;

    b1 = RecvPTSByte();
    b2 = RecvPTSByte();
    b3 = RecvPTSByte();
    if((b1 < 0) || (b2 < 0) || (b3 < 0))
        return ERROR;

    reading = (unsigned int)b1;
    reading = ((reading<<8)+((unsigned int) b2&0x00ff));
    *MeasuredVal = (double)reading/100;
    if((b1+b2&0xff)== b3)
        return NOERROR;
    else
        return ERROR;
}

/*******************************************************************
 * Function Name: MeasureHighFlowAvg()                             *  
 *                                                                 *  
 * Function Purpose: Measure High Flow Port selecting Air only     *  
 *                                                                 *  
 * Synop: This function will send a command string to the          *  
 *        PTS2000. Which will start the measurement of air         *  
 *        flow at the High flow port. The PTS2000 will use         *  
 *        an internal look-up table to convert the reading         *  
 *        based on air and then return the reading as two          *  
 *        binary bytes. This function will then convert the        *  
 *        reading into a floating point number assign it to        *  
 *        the location passed by the pointer.                      *  
 *                                                                 *  
 *        In this case the PTS2000 will return the AVG of          *
 *        10 readings                                              *  
 *                                                                 *  
 * Variables Passed: A pointer of type double used to return       *  
 *                   the reading                                   *  
 *                                                                 *  
 *                                                                 *  
 * Variables Returned: Interger type will be returned Non zero     *  
 *                     will indicate an error.                     *  
 *                                                                 *  
 * Globals Affected: None                                          *  
 *                                                                 *  
 *                                                                 *  
 * Call:   error = MeasureHighFlowAvg(&Measurment);                *  
 *******************************************************************/

int MeasureHighFlowAvg(double *MeasuredVal)
{
    unsigned int reading;
    int b1, b2, b3;

    FlushInQPTS();

    if(SendPTS("FA0\r",4) != SUCCESS)
        return ERROR;

    b1 = RecvPTSByte();
    b2 = RecvPTSByte();
    b3 = RecvPTSByte();
    reading = (unsigned int)b1;
    reading = ((reading<<8)+((unsigned int) b2&0x00ff));
    *MeasuredVal = (double)reading/100;

    if((b1+b2&0xff)== b3)
        return NOERROR;
    else
        return ERROR;
}

/*******************************************************************
 * Function Name: MeasureHighFlowO2Avg()                           *  
 *                                                                 *  
 * Function Purpose: Measure High Flow Port selecting Air only     *  
 *                                                                 *  
 * Synop: This function will send a command string to the          *  
 *        PTS2000. Which will start the measurement of air         *  
 *        flow at the High flow port. The PTS2000 will use         *  
 *        an internal look-up table to convert the reading         *  
 *        based on air and then return the reading as two          *  
 *        binary bytes. This function will then convert the        *  
 *        reading into a floating point number assign it to        *  
 *        the location passed by the pointer.                      *  
 *                                                                 *  
 *        In this case the PTS2000 will return the AVG of          *
 *        10 readings                                              *  
 *                                                                 *  
 * Variables Passed: A pointer of type double used to return       *  
 *                   the reading                                   *  
 *                                                                 *  
 *                                                                 *  
 * Variables Returned: Interger type will be returned Non zero     *  
 *                     will indicate an error.                     *  
 *                                                                 *  
 * Globals Affected: None                                          *  
 *                                                                 *  
 *                                                                 *  
 * Call:   error = MeasureHighFlowAvg(&Measurment);                *  
 *******************************************************************/

int MeasureHighFlowO2Avg(double *MeasuredVal)
{
    unsigned int reading;
    int b1, b2, b3;

    FlushInQPTS();


    if(SendPTS("FA1\r",4) != SUCCESS)
        return ERROR;

    b1 = RecvPTSByte();
    b2 = RecvPTSByte();
    b3 = RecvPTSByte();
    if(b1 < 0 || b2 < 0 || b3 < 0)
        return ERROR;

    reading = (unsigned int)b1;
    reading = ((reading<<8)+((unsigned int) b2&0x00ff));
    *MeasuredVal = (double)reading/100;

    if((b1+b2&0xff)== b3)
        return NOERROR;
    else
        return ERROR;
}

/*******************************************************************
 * Function Name: MeasureHighFlowO2                                * 
 *                                                                 * 
 * Function Purpose: Measure High Flow Port selecting Oxygen only  * 
 *                                                                 * 
 * Synop: This function will send a command string to the          * 
 *        PTS2000. Which will start the measurement of Oxygen      * 
 *        flow at the High flow port. The PTS2000 will use         * 
 *        an internal look-up table to convert the reading         * 
 *        based on Oxygen and then return the reading as two       * 
 *        binary bytes. This function will then convert the        * 
 *        reading into a floating point number assign it to        * 
 *        the location passed by the pointer.                      * 
 *                                                                 * 
 *                                                                 * 
 *                                                                 * 
 *                                                                 * 
 * Variables Passed: A pointer of type double used to return       * 
 *                   the reading                                   * 
 *                                                                 * 
 *                                                                 * 
 * Variables Returned: Interger type will be returned Non zero     * 
 *                     will indicate an error.                     * 
 *                                                                 * 
 * Globals Affected: None                                          * 
 *                                                                 * 
 *                                                                 * 
 * Call:   error = MeasureHighFlowO2(&Measurment);                 * 
 *******************************************************************/

int MeasureHighFlowO2(double *MeasuredVal)
{
    unsigned int reading;
    int b1, b2, b3;

    FlushInQPTS();

    if(SendPTS("F1\r",3) != SUCCESS)
        return ERROR;

    b1 = RecvPTSByte();
    b2 = RecvPTSByte();
    b3 = RecvPTSByte();
    if(b1 < 0 || b2 < 0 || b3 < 0)
        return ERROR;

    reading = (unsigned int)b1;
    reading = ((reading<<8)+((unsigned int) b2&0x00ff));
    *MeasuredVal = (double)reading/100;

    if((b1+b2&0xff)== b3)
        return NOERROR;
    else
        return ERROR;
}

/*******************************************************************
 * Function Name: MeasureLowPressure                               * 
 *                                                                 * 
 * Function Purpose: To request a Low pressure reading             * 
 *                                                                 * 
 * Synop: This function will send a command string to the PTS2000  * 
 *        the instrument will then read the pressure and return    * 
 *        the reading a two binary words. This function will then  * 
 *        convert the reading to a floating point number and       * 
 *        assign it to the location passed to it. This value       * 
 *        is a signed value due to the fact the low pressure       * 
 *        port is a diffrential pressure port.                     * 
 *                                                                 * 
 *                                                                 * 
 *                                                                 * 
 *                                                                 * 
 *                                                                 * 
 * Variables Passed: A pointer type double for the reading         * 
 *                                                                 * 
 *                                                                 * 
 *                                                                 * 
 * Variables Returned: error condition a non zero reading indicates* 
 *                     an error occured                            * 
 *                                                                 * 
 * Globals Affected: None                                          * 
 *                                                                 * 
 *                                                                 * 
 * Call:   error = MeasureLowPressure(&Measurment);                * 
 *******************************************************************/

int MeasureLowPressure(double *MeasuredVal)
{
    unsigned int reading;
    int b1, b2, b3;

    FlushInQPTS();

    if(SendPTS("p\r",2) != SUCCESS)
        return ERROR;

    b1 = RecvPTSByte();
    b2 = RecvPTSByte();
    b3 = RecvPTSByte();
    if(b1 < 0 || b2 < 0 || b3 < 0)
        return ERROR;

    reading = (unsigned int)b1;
    reading = ((reading<<8)+(b2&0x00ff));
    if(reading & 0x8000)
    {
        *MeasuredVal = -1*(double)(~reading&0x7fff)/100;
    }
    else
        *MeasuredVal = (double)(reading&0x7fff)/100;


    if((b1+b2&0xff)== b3)
        return NOERROR;
    else
        return ERROR;
}

/*******************************************************************
 * Function Name: ZeroLowPressure                                  * 
 *                                                                 * 
 * Function Purpose: To Zero the Low pressure Port                 * 
 *                                                                 *
 * Synop: This function will send a command string to the PTS2000  * 
 *        the instrument will then Zero the  Low pressure port     * 
 *        The PTS-2000 will return ACK or NAK status.              * 
 *                                                                 *
 *                                                                 * 
 *                                                                 * 
 *                                                                 * 
 *                                                                 * 
 * Variables Passed: None                                          * 
 *                                                                 * 
 *                                                                 * 
 *                                                                 * 
 * Variables Returned: error condition a non zero reading indicates* 
 *                     an error occured                            * 
 *                                                                 * 
 * Globals Affected: None                                          * 
 *                                                                 * 
 *                                                                 * 
 * Call:   error = ZeroLowPressure();                              * 
 *******************************************************************/

int ZeroLowPressure(void)
{
	int b1, b2, b3;

	FlushInQPTS();

	if(SendPTS("z\r",2) != SUCCESS)
		return ERROR;

	if( (b1 = RecvPTSByte()) < 0) // Acknowledge byte
		return CVI_ERROR;
	if( (b2 = RecvPTSByte()) < 0) // carriage return (end of message)
		return CVI_ERROR;
	if( (b3 = RecvPTSByte()) < 0) // linefeed (end of message)
		return CVI_ERROR;

	if(((unsigned char)b1&0xff) == ACK)
		return NOERROR;
	else
		return ERROR;
}

/*******************************************************************
 * Function Name: MeasureLowFlow()                                 *  
 *                                                                 *  
 * Function Purpose: Measure Low Flow Port selecting Air only      *  
 *                                                                 *  
 * Synop: This function will send a command string to the          *  
 *        PTS2000. Which will start the measurement of air         *  
 *        flow at the low flow port. The PTS2000 will use          *  
 *        an internal look-up table to convert the reading         *  
 *        based on air and then return the reading as two          *  
 *        binary bytes. This function will then convert the        *  
 *        reading into a floating point number assign it to        *  
 *        the location passed by the pointer.                      *  
 *                                                                 *  
 *                                                                 *  
 *                                                                 *  
 *                                                                 *  
 * Variables Passed: A pointer of type double used to return       *  
 *                   the reading                                   *  
 *                                                                 *  
 *                                                                 *  
 * Variables Returned: Interger type will be returned Non zero     *  
 *                     will indicate an error.                     *  
 *                                                                 *  
 * Globals Affected: None                                          *  
 *                                                                 *  
 *                                                                 *  
 * Call:   error = MeasureLowFlow(&Measurment);                    *  
 *******************************************************************/

int MeasureLowFlow(double *MeasuredVal)
{
    unsigned int reading;
    int b1, b2, b3;

    FlushInQPTS();

    if(SendPTS("f0\r",3) != SUCCESS)
        return ERROR;

    b1 = RecvPTSByte();
    b2 = RecvPTSByte();
    b3 = RecvPTSByte();

    if(b1 < 0 || b2 < 0 || b3 < 0)
        return ERROR;

    reading = (unsigned int)b1;
    reading = ((reading<<8)+((unsigned int) b2&0x00ff));
    *MeasuredVal = (double)reading/1000;

    if((b1+b2&0xff)== b3)
        return NOERROR;
    else
        return ERROR;
}

/*******************************************************************
 * Function Name: MeasureLowFlowO2                                 * 
 *                                                                 * 
 * Function Purpose: Measure Low Flow Port selecting Oxygen only   * 
 *                                                                 * 
 * Synop: This function will send a command string to the          * 
 *        PTS2000. Which will start the measurement of Oxygen      * 
 *        flow at the low flow port. The PTS2000 will use          * 
 *        an internal look-up table to convert the reading         * 
 *        based on Oxygen and then return the reading as two       * 
 *        binary bytes. This function will then convert the        * 
 *        reading into a floating point number assign it to        * 
 *        the location passed by the pointer.                      * 
 *                                                                 * 
 *                                                                 * 
 *                                                                 * 
 *                                                                 * 
 * Variables Passed: A pointer of type double used to return       * 
 *                   the reading                                   * 
 *                                                                 * 
 *                                                                 * 
 * Variables Returned: Interger type will be returned Non zero     * 
 *                     will indicate an error.                     * 
 *                                                                 * 
 * Globals Affected: None                                          * 
 *                                                                 * 
 *                                                                 * 
 * Call:   error = MeasureLowFlowO2(&Measurment);                  * 
 *******************************************************************/

int MeasureLowFlowO2(double *MeasuredVal)
{
    unsigned int reading;
    int b1, b2, b3;

    FlushInQPTS();


    if(SendPTS("f1\r",3) != SUCCESS)
        return ERROR;

    b1 = RecvPTSByte();
    b2 = RecvPTSByte();
    b3 = RecvPTSByte();

    if(b1 < 0 || b2 < 0 || b3 < 0)
        return ERROR;

    reading = (unsigned int)b1;
    reading = ((reading<<8)+((unsigned int) b2&0x00ff));
    *MeasuredVal = (double)reading/1000;

    if((b1+b2&0xff)== b3)
        return NOERROR;
    else
        return ERROR;

}

/*******************************************************************
 * Function Name: MeasureLowFlowAvg()                              *  
 *                                                                 *  
 * Function Purpose: Measure Low Flow Port selecting Air only      *  
 *                                                                 *  
 * Synop: This function will send a command string to the          *  
 *        PTS2000. Which will start the measurement of air         *  
 *        flow at the low flow port. The PTS2000 will use          *  
 *        an internal look-up table to convert the reading         *  
 *        based on air and then return the reading as two          *  
 *        binary bytes. This function will then convert the        *  
 *        reading into a floating point number assign it to        *  
 *        the location passed by the pointer.                      *  
 *                                                                 *  
 *        Return the average of 10 readings                        *  
 *                                                                 *  
 *                                                                 *  
 * Variables Passed: A pointer of type double used to return       *  
 *                   the reading                                   *  
 *                                                                 *  
 *                                                                 *  
 * Variables Returned: Interger type will be returned Non zero     *  
 *                     will indicate an error.                     *  
 *                                                                 *  
 * Globals Affected: None                                          *  
 *                                                                 *  
 *                                                                 *  
 * Call:   error = MeasureLowFlow(&Measurment);                    *  
 *******************************************************************/

int MeasureLowFlowAvg(double *MeasuredVal)
{
    unsigned int reading;
    int b1, b2, b3;


    FlushInQPTS();

    if(SendPTS("fA0\r",4) != SUCCESS)
        return ERROR;

    b1 = RecvPTSByte();
    b2 = RecvPTSByte();
    b3 = RecvPTSByte();
    if(b1 < 0 || b2 < 0 || b3 < 0)
        return ERROR;
    
    reading = (unsigned int)b1;
    reading = ((reading<<8)+((unsigned int) b2&0x00ff));
    *MeasuredVal = (double)reading/1000;

    if((b1+b2&0xff)== b3)
        return NOERROR;
    else
        return ERROR;

}


/*******************************************************************
 * Function Name: MeasureLowFlowO2Avg                              * 
 *                                                                 * 
 * Function Purpose: Measure Low Flow Port selecting Oxygen only   * 
 *                                                                 * 
 * Synop: This function will send a command string to the          * 
 *        PTS2000. Which will start the measurement of Oxygen      * 
 *        flow at the low flow port. The PTS2000 will use          * 
 *        an internal look-up table to convert the reading         * 
 *        based on Oxygen and then return the reading as two       * 
 *        binary bytes. This function will then convert the        * 
 *        reading into a floating point number assign it to        * 
 *        the location passed by the pointer.                      * 
 *                                                                 * 
 *        Average 10 readings                                      * 
 *                                                                 * 
 *                                                                 * 
 * Variables Passed: A pointer of type double used to return       * 
 *                   the reading                                   * 
 *                                                                 * 
 *                                                                 * 
 * Variables Returned: Interger type will be returned Non zero     * 
 *                     will indicate an error.                     * 
 *                                                                 * 
 * Globals Affected: None                                          * 
 *                                                                 * 
 *                                                                 * 
 * Call:   error = MeasureLowFlowO2(&Measurment);                  * 
 *******************************************************************/

int MeasureLowFlowO2Avg(double *MeasuredVal)
{
    unsigned int reading;
    int b1, b2, b3;

    FlushInQPTS();


    if(SendPTS("fA1\r",4) != SUCCESS)
        return ERROR;

    b1 = RecvPTSByte();
    b2 = RecvPTSByte();
    b3 = RecvPTSByte();
    if(b1 < 0 || b2 < 0 || b3 < 0)
        return ERROR;

    reading = (unsigned int)b1;
    reading = ((reading<<8)+((unsigned int) b2&0x00ff));
    *MeasuredVal = (double)reading/1000;

    if((b1+b2&0xff)== b3)
        return NOERROR;
    else
        return ERROR;

}

/*******************************************************************
 * Function Name: MeasureOxygen                                    *  
 *                                                                 *  
 * Function Purpose: To read the Oxygen % at the High Flow Port    *  
 *                                                                 *  
 * Synop: This function will send a command string to the PTS2000  *  
 *        requesting a measurement of the oxygen % at the high     *  
 *        flow port. The unit will return a reading as two binary  *
 *        bytes. This function will then convert the reading into  *  
 *        a floating point value to be assigned to the location    *  
 *        passed by the pointer.                                   *  
 *                                                                 *  
 *        Note: The measurement requires a delay to become         *  
 *              stable. The delay is not provided by               *  
 *              this function.                                     *  
 *                                                                 *  
 *                                                                 *  
 * Variables Passed: pointer of type double                        *  
 *                                                                 *  
 *                                                                 *  
 *                                                                 *  
 * Variables Returned: error code non zero for error               *  
 *                                                                 *  
 *                                                                 *  
 * Globals Affected: none                                          *  
 *                                                                 *  
 *                                                                 *  
 * Call:   error = MeasureOxygen(&Measurment);                     *  
 *******************************************************************/

int MeasureOxygen(double *MeasuredVal)
{
    unsigned int reading;
    int b1, b2, b3;

    FlushInQPTS();

    if(SendPTS("O\r",2) != SUCCESS)
        return ERROR;

    b1 = RecvPTSByte();
    b2 = RecvPTSByte();
    b3 = RecvPTSByte();
    if(b1 < 0 || b2 < 0 || b3 < 0)
        return ERROR;

    reading = (unsigned int)b1;
    reading = ((reading<<8)+(b2&0x00ff));
    *MeasuredVal = (double)reading/10;


    if((b1+b2&0xff)== b3)
        return NOERROR;
    else
        return ERROR;
}

/*******************************************************************
 * Function Name: MeasureBarometricPressure                        *
 *                                                                 *
 * Function Purpose: Measure current Barometric Pressure           *
 *                                                                 * 
 * Synop: This function will send a command string to the PTS2000  *
 *        requesting a measurement of the Barometric pressure      * 
 *        The unit will return a reading as two binary bytes       * 
 *        This function will then convert the reading into         * 
 *        a floating point value to be assigned to the location    * 
 *        passed by the pointer.                                   * 
 *                                                                 * 
 *                                                                 * 
 *                                                                 * 
 *                                                                 * 
 *                                                                 * 
 *                                                                 * 
 * Variables Passed: pointer of type double                        * 
 *                                                                 * 
 *                                                                 * 
 *                                                                 * 
 * Variables Returned: Non zero indicates an error                 *
 *                                                                 * 
 *                                                                 * 
 * Globals Affected: none                                          * 
 *                                                                 *  
 *                                                                 *  
 * Call:   error = MeasureBarometricPressure(&Measurment);         *
 *******************************************************************/

int MeasureBarometricPressure(double *MeasuredVal)
{
    unsigned int reading;
    int b1, b2, b3;

    FlushInQPTS();

    if(SendPTS("B\r",2) != SUCCESS)
        return ERROR;

    b1 = RecvPTSByte();
    b2 = RecvPTSByte();
    b3 = RecvPTSByte();
    if(b1 < 0 || b2 < 0 || b3 < 0)
        return ERROR;

    reading = (unsigned int)b1;
    reading = ( (reading<<8) + ((unsigned int)(b2&0x00ff )) );
    *MeasuredVal = (double)reading/100;

    if((b1+b2&0xff)== b3)
        return NOERROR;
    else
        return ERROR;
}





