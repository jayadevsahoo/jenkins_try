#ifndef ExhHeaterController_HH
#define ExhHeaterController_HH
//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: ExhHeaterController - Implements an exhalation heater controller.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/ExhHeaterController.hhv   25.0.4.0   19 Nov 2013 13:59:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001  By:  syw    Date:  06-Nov-1996    DR Number:
//       Project:  Sigma (R8027)
//       Description:
//             Initial version.
//
//=====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"

//@ Usage-Classes

class TemperatureSensor ;
class BinaryCommand ;

//@ End-Usage

class ExhHeaterController 
{
  public:
	ExhHeaterController( TemperatureSensor& rTempSensor,
				         BinaryCommand& rExhHeater) ;
	virtual ~ExhHeaterController( void) ;
    	
	static void SoftFault( const SoftFaultID softFaultID,
							   const Uint32      lineNumber,
							   const char*       pFileName  = NULL, 
							   const char*       pPredicate = NULL) ;

	void newCycle( void) ;

  protected:

  private:
	ExhHeaterController( const ExhHeaterController&) ;	// Declared but not implemented
	ExhHeaterController( void) ;   						// Declared but not implemented
	void operator=( const ExhHeaterController&) ;   	// Declared but not implemented

	//@ Data-Member:& rTempSensor_
	// reference to temperature sensor used for monitoring
	TemperatureSensor& rTempSensor_ ;

	//@ Data-Member:& rExhHeater_
	// reference to exhalation heater to control
	BinaryCommand& rExhHeater_ ;
	
	//@ Data-Member: clockTimer_
	// keeps track of elapsed time, gets reset every EXH_HEATER_CONTROL_CYCLE_MS
	Uint32 clockTimer_ ;
} ;


#endif // ExhHeaterController_HH 
