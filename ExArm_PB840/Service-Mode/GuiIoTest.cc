#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: GuiIoTest - Test GUI IO
//---------------------------------------------------------------------
//@ Interface-Description
//	This class implements all GUI IO tests -- keyboard, knob,
//	LED, audio, touch screen, serial port.
//---------------------------------------------------------------------
//@ Rationale
//  This class encapsulates all GUI IO tests.
//---------------------------------------------------------------------
//@ Implementation-Description
//	The following methods implement the various GUI IO tests:
//  runKeyboardTest()		- performs keyboard test.
//  runKnobTest()		- performs knob test.
//  runTouchScreenTest()	- performs touch screen test.
//  runGuiLedTest()		- performs GUI LED test
//  runGuiAudioTest()		- performs GUI audio test.
//  runExternalSerialIoTest()	- performs GUI external serial device test.
//  runSerialIoTest()		- performs GUI serial device test.
//  runNurseCallTest()		- performs GUI nurse call test.
//---------------------------------------------------------------------
//@ Fault-Handling
//  n/a
//---------------------------------------------------------------------
//@ Restrictions
//	Only one instance of this class may be created.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/GuiIoTest.ccv   25.0.4.0   19 Nov 2013 14:23:40   pvcs  $
//
//@ Modification-Log
//
//  Revision: 016  By: erm       Date: 16-Jan-2002     DR Number: 5984
//  Project: GuiComms
//  Description:
//  Added External Serial LoopBack test
//
//  Revision: 015  By: erm       Date: 07-Jan-2002     DR Number: 5983
//  Project: GuiComms
//  Description:
//  Modified runKeyboardTest function, adjusted condition array
//
//  Revision: 014  By: gdc       Date: 04-Jan-2001     DR Number: 5493
//  Project:  GuiComms
//  Description:
//	Modified for new TouchDriver interface. Testing aux serial ports.
//
//  Revision: 013  By: dosman    Date: 25-May-1999     DR Number: 5322
//  Project:  ATC
//  Description:
//	ATC Initial Release.
//	Updated/fixed to use a FakeSerialInterface stub.
//
//  Revision: 012  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 011  By: gdc     Date: 24-Nov-1997    DR Number: 2605
//       Project:  Sigma (R8027)
//       Description:
//            Changed interface to GUI-Serial-Interface as part of its 
//            restructuring.
//
//  Revision: 010  By:  quf    Date: 03-Nov-1997    DR Number: DCS 2169
//       Project:  Sigma (840)
//       Description:
//            Deleted SRS req # 09153.
//
//  Revision: 009  By:  quf    Date: 11-Sep-1997    DR Number: DCS 2436
//       Project:  Sigma (840)
//       Description:
//            Nurse Call Test ALERTs were converted to FAILUREs.
//
//  Revision: 008  By:  quf    Date: 14-Aug-1997    DR Number: DCS 2213
//       Project:  Sigma (840)
//       Description:
//            Knob Test now uses KeysAllowedId USER_EXIT_ONLY when
//            prompting to turn the knob.
//
//  Revision: 007  By:  quf    Date: 11-Aug-1997    DR Number: DCS 2342
//       Project:  Sigma (840)
//       Description:
//            Changed GUI Audio Test condition 1 error from Alert to
//            Failure per SRS.
//
//  Revision: 006  By:  quf    Date: 05-Aug-1997    DR Number: DCS 2251
//       Project:  Sigma (840)
//       Description:
//            Changed GUI Touch Test to have a single Alert condition.
//
//  Revision: 005  By:  quf    Date: 31-Jul-1997    DR Number: DCS 2013
//       Project:  Sigma (840)
//       Description:
//            Removed nurse call test from GUI audio test and made it
//            a separate test.
//
//  Revision: 004  By:  quf    Date: 16-Jun-1997    DR Number: DCS 2208
//       Project:  Sigma (840)
//       Description:
//            Changed "Blink.hh" to "VgaBlink.hh" for VGA-Graphics-Driver
//            change.
//
//  Revision: 003  By:  quf    Date: 27-May-1997    DR Number: DCS 2042
//       Project:  Sigma (840)
//       Description:
//            Implemented GUI Audio Test.
//            Implemented GUI Serial Port Test.  Deleted testSerialIoTest_(),
//            added clearString_().
//
//  Revision: 002  By:  quf    Date: 22-May-1997    DR Number: DCS 2131
//       Project:  Sigma (840)
//       Description:
//            Modification-Log fix.
//            Fixed global reference names (i.e. "r" changed to "R").
//
//  Revision: 001  By:  sp    Date:  26-Feb-1995    DR Number: none
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//=====================================================================
//TODO E600 added GUI only
#if defined(SIGMA_GUI_CPU)

#include "GuiIoTest.hh"

//@ Usage-Classes
#include "SmRefs.hh"
#include "SmManager.hh"
#include "UserAnnunciationMsg.hh"
#include "SmPromptId.hh"
#include "IpcIds.hh"
#include "GuiIo.hh"
#include "GuiIoLed.hh"
#include "VgaBlink.hh"
#include "TouchDriver.hh"
#include "Task.hh"
#include "Sound.hh"
#include "OsUtil.hh"
#include "Led.hh"
#include "OsTimeStamp.hh"
#include "SerialPort.hh"

#if defined(SIGMA_DEVELOPMENT)
#include <stdio.h>
#include <string.h>
#endif // defined(SIGMA_DEVELOPMENT)

#if defined(SIGMA_UNIT_TEST) && defined(SIGMA_GUI_CPU)
#include "GuiIoTest_UT.hh"
#include "FakeSerialInterface.hh"
#else
#include "SerialInterface.hh"
#endif // defined(SIGMA_UNIT_TEST) && defined(SIGMA_GUI_CPU)

//@ End-Usage


//@ Code...
#ifdef SIGMA_UNIT_TEST
// use shorter timeout for unit testing only
static const Uint32 GUI_IO_TEST_TIMEOUT = 2000;  // msec
#else
static const Uint32 GUI_IO_TEST_TIMEOUT = 15000;  // msec
#endif

//@ Constant: MIN_KNOB_DELTA
// minimum accumulated knob delta to flag knob turn detection
const Int16 MIN_KNOB_DELTA = 20;


//@ Constant: TEST_MSG
// test message for serial port test
const char TEST_MSG[]
	= { 0xa5, 0x5a, 0xa5, 0x5a, 0xa5, 0x5a, 0xa5, 0x5a,
	    0xa5, 0x5a, 0xa5, 0x5a, 0xa5, 0x5a, 0xa5, 0x5a,
	    0xa5, 0x5a, 0xa5, 0x5a, 0xa5, 0x5a, 0xa5, 0x5a,
	    0xa5, 0x5a, 0xa5, 0x5a, 0xa5, 0x5a, 0xa5, 0x5a,
	    0xff, 0x00, 0xff, 0x00, 0xff, 0x00, 0xff, 0x00,
	    0xff, 0x00, 0xff, 0x00, 0xff, 0x00, 0xff, 0x00,
	    0xff, 0x00, 0xff, 0x00, 0xff, 0x00, 0xff, 0x00,
	    0xff, 0x00, 0xff, 0x00, 0xff, 0x00, 0xff, 0x00,
        0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80,
	    0xfe, 0xfd, 0xfb, 0xf7, 0xef, 0xdf, 0xbf, 0x7f
	  };

const Uint TEST_MSG_SIZE = sizeof(TEST_MSG);

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GuiIoTest()  [Default Constructor]
//
//@ Interface-Description
//	Constructor.  No argument.  Initializes the private data member.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Initialize guiIoTestQ_.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
GuiIoTest::GuiIoTest(void)
 : guiIoTestQ_(::SERVICE_MODE_GUIIO_Q)
{
	// $[TI1]
	CALL_TRACE("GuiIoTest::GuiIoTest(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~GuiIoTest()  [Destructor]
//
//@ Interface-Description
//	Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

GuiIoTest::~GuiIoTest(void)
{
	CALL_TRACE("GuiIoTest::~GuiIoTest(void)");

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: runKeyboardTest()
//
//@ Interface-Description
//  This method performs the GUI keyboard test.  It takes no argument
//  and has no return value. 
//---------------------------------------------------------------------
//@ Implementation-Description
//	$[09152]
//	Test each GUI key by prompting user to press each key individually
//	(press ACCEPT key, press CANCEL key, etc.).  A failure is detected
//	via a timeout or the wrong key pressed during a given prompt.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
GuiIoTest::runKeyboardTest(void)
{
	CALL_TRACE("GuiIoTest::runKeyboardTest(void)");

	// setup arrays which map iteration # to the proper
	// prompt ID, key ID, and error condition ID
	const Uint32 MAX_NUM_KEYS = 13;
	Uint32 numKeys = IsGuiCommsConfig() ? 11 : MAX_NUM_KEYS;
	SmPromptId::PromptId prompt[MAX_NUM_KEYS];
	GuiKeys guiKey[MAX_NUM_KEYS];
	SmStatusId::TestConditionId condition[MAX_NUM_KEYS];
	Uint32 idx = 0;

	prompt[idx] = SmPromptId::KEY_ACCEPT_PROMPT;
//	guiKey[idx] = GUI_KEY1;
	condition[idx++] = SmStatusId::CONDITION_1;
	prompt[idx] = SmPromptId::KEY_CLEAR_PROMPT;
//	guiKey[idx] = GUI_KEY2;
	condition[idx++] = SmStatusId::CONDITION_2;
	prompt[idx] = SmPromptId::KEY_INSP_PAUSE_PROMPT;
//	guiKey[idx] = GUI_KEY3;
	condition[idx++] = SmStatusId::CONDITION_3;
	prompt[idx] = SmPromptId::KEY_EXP_PAUSE_PROMPT;
//	guiKey[idx] = GUI_KEY4;
	condition[idx++] = SmStatusId::CONDITION_4;
	prompt[idx] = SmPromptId::KEY_MAN_INSP_PROMPT;
//	guiKey[idx] = GUI_KEY5;
	condition[idx++] = SmStatusId::CONDITION_5;
	prompt[idx] = SmPromptId::KEY_HUNDRED_PERCENT_O2_PROMPT;
//	guiKey[idx] = GUI_KEY6;
	condition[idx++] = SmStatusId::CONDITION_6;
	prompt[idx] = SmPromptId::KEY_INFO_PROMPT;
//	guiKey[idx] = GUI_KEY7;
	condition[idx++] = SmStatusId::CONDITION_7;
	prompt[idx] = SmPromptId::KEY_ALARM_RESET_PROMPT;
//	guiKey[idx] = GUI_KEY8;
	condition[idx++] = SmStatusId::CONDITION_8;
	prompt[idx] = SmPromptId::KEY_ALARM_SILENCE_PROMPT;
//	guiKey[idx] = GUI_KEY9;
	condition[idx++] = SmStatusId::CONDITION_9;
	prompt[idx] = SmPromptId::KEY_ALARM_VOLUME_PROMPT;
//	guiKey[idx] = GUI_KEY10;
	condition[idx++] = SmStatusId::CONDITION_10;
	if ( !IsGuiCommsConfig() )
	{
	  prompt[idx] = SmPromptId::KEY_SCREEN_BRIGHTNESS_PROMPT;
//	  guiKey[idx] = GUI_KEY11;
	  condition[idx++] = SmStatusId::CONDITION_11;
	  prompt[idx] = SmPromptId::KEY_SCREEN_CONTRAST_PROMPT;
//	  guiKey[idx] = GUI_KEY12;
	  condition[idx++] = SmStatusId::CONDITION_12;
	}
	prompt[idx] = SmPromptId::KEY_SCREEN_LOCK_PROMPT;
//	guiKey[idx] = GUI_KEY13;
	condition[idx++] = SmStatusId::CONDITION_13;

	// make sure index is correct
	CLASS_ASSERTION( idx == numKeys);

	// Steal keyboard events from GUI
	RouteKeyEvents(::SERVICE_MODE_GUIIO_Q);

	// Test each key
	Boolean continueTest = TRUE;
	for ( idx = 0; idx < numKeys && continueTest; idx++)
	{
	// $[TI1]
		if ( !testKeyPress_(prompt[idx], guiKey[idx]) )
		{
		// $[TI1.1]
			RSmManager.sendStatusToServiceData(
				SmStatusId::TEST_FAILURE, condition[idx]);

			// exit as soon as a key fails
			continueTest = FALSE;
		}
		// $[TI1.2]
		// implied else: tested key passed
	}
	// end for

	// Restore keyboard events back to GUI
	RouteKeyEvents();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: runKnobTest()
//
//@ Interface-Description
//  This method performs the GUI knob test.  It takes no argument and
//	has no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	$[09151]
//	Prompt then test for counterclockwise turn, repeat for clockwise turn.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
GuiIoTest::runKnobTest(void)
{
	CALL_TRACE("GuiIoTest::runKnobTest(void)");

#ifdef SIGMA_UNIT_TEST
	KnobTestResult leftResult = GuiIoTest_UT::LeftResult;
#else
	KnobTestResult leftResult = testKnob_(LEFT);
#endif  // SIGMA_UNIT_TEST

	CLASS_ASSERTION( leftResult == OPERATOR_EXIT ||
					 leftResult == FAILED ||
					 leftResult == PASSED );

	if (leftResult == OPERATOR_EXIT)
	{ // $[TI1]
		RSmManager.sendOperatorExitToServiceData();
	}
	else if (leftResult == PASSED)
	{ // $[TI2]
#ifdef SIGMA_UNIT_TEST
		KnobTestResult rightResult = GuiIoTest_UT::RightResult;
#else
		KnobTestResult rightResult = testKnob_(RIGHT);
#endif  // SIGMA_UNIT_TEST

		CLASS_ASSERTION( rightResult == OPERATOR_EXIT ||
						 rightResult == FAILED ||
						 rightResult == PASSED );

		if (rightResult == OPERATOR_EXIT)
		{ // $[TI2.1]
			RSmManager.sendOperatorExitToServiceData();
		}
		else if (rightResult == FAILED)
		{ // $[TI2.2]
			// knob test failed
			RSmManager.sendStatusToServiceData(
				SmStatusId::TEST_FAILURE,
				SmStatusId::CONDITION_1);
		} // $[TI2.3]
		// implied else: pass test
	}
	else
	{ // $[TI3]
		// knob test failed
		RSmManager.sendStatusToServiceData(
			SmStatusId::TEST_FAILURE,
			SmStatusId::CONDITION_1);
	}
	// end else
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: runGuiLedTest()
//
//@ Interface-Description
//  This method performs test on the GUI LEDs. It takes no argument and
//	has no return value. 
//---------------------------------------------------------------------
//@ Implementation-Description
//	$[09099]
//	Prompt to verify that each LED is lit (is high urgency LED on,
//	is medium urgency LED on, etc).  ACCEPT = yes, CANCEL = no.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
GuiIoTest::runGuiLedTest(void)
{
	CALL_TRACE("GuiIoTest::runGuiLedTest(void)");

	// setup arrays which map iteration # to the proper
	// prompt ID, led ID, and error condition ID
	//TODO E600 Currently supports only Alarms LED. Update
	//when rest of the LED's are enabled
	const Uint32 NUM_LEDS = 3;
	SmPromptId::PromptId prompt[NUM_LEDS];
	Led::LedId led[NUM_LEDS];
	SmStatusId::TestConditionId condition[NUM_LEDS];
	Uint32 idx = 0;
	prompt[idx] = SmPromptId::HIGH_ALARM_PROMPT;
	led[idx] = Led::HIGH_ALARM_URGENCY;
	condition[idx++] = SmStatusId::CONDITION_1;
	prompt[idx] =  SmPromptId::MEDIUM_ALARM_PROMPT;
	led[idx] = Led::MEDIUM_ALARM_URGENCY;
	condition[idx++] = SmStatusId::CONDITION_2;
	prompt[idx] = SmPromptId::LOW_ALARM_PROMPT;
	led[idx] =  Led::LOW_ALARM_URGENCY;
	condition[idx++] = SmStatusId::CONDITION_3;
#if 0
	prompt[idx] =  SmPromptId::NORMAL_PROMPT;
	led[idx] = Led::NORMAL;
	condition[idx++] = SmStatusId::CONDITION_4;
	prompt[idx] = SmPromptId::BATTERY_BACKUP_READY_PROMPT;
	led[idx] = Led::BATTERY_BACKUP_READY;
	condition[idx++] = SmStatusId::CONDITION_5;
	prompt[idx] = SmPromptId::ON_BATTERY_POWER_PROMPT;
	led[idx] = Led::ON_BATTERY_POWER;
	condition[idx++] = SmStatusId::CONDITION_6;
	prompt[idx] = SmPromptId::COMPRESSOR_READY_PROMPT;
	led[idx] = Led::COMPRESSOR_READY;
	condition[idx++] = SmStatusId::CONDITION_7;
	prompt[idx] = SmPromptId::COMPRESSOR_OPERATING_PROMPT;
	led[idx] = Led::COMPRESSOR_OPERATING;
	condition[idx++] = SmStatusId::CONDITION_8;
	prompt[idx] = SmPromptId::HUNDRED_PERCENT_O2_PROMPT;
	led[idx] = Led::HUNDRED_PERCENT_O2;
	condition[idx++] = SmStatusId::CONDITION_9;
	prompt[idx] = SmPromptId::ALARM_SILENCE_PROMPT;
	led[idx] = Led::ALARM_SILENCE;
	condition[idx++] = SmStatusId::CONDITION_10;
	prompt[idx] = SmPromptId::SCREEN_LOCK_PROMPT;
	led[idx] = Led::SCREEN_LOCK;
	condition[idx++] = SmStatusId::CONDITION_11;
#endif

	// make sure index is correct
	CLASS_ASSERTION( idx == NUM_LEDS);

	// Make sure all gui leds are off initially
	for ( idx = 0; idx < NUM_LEDS; idx++ )
	{
	// $[TI1]
		Led::SetState( led[idx], Led::OFF_STATE);
	}
	// end for

	// Test each gui led
	Boolean operatorExit = FALSE;
	for ( idx = 0; idx < NUM_LEDS && !operatorExit; idx++)
	{
	// $[TI2]
		// turn on led
		Led::SetState( led[idx] , Led::ON_STATE);

		// prompt operator "Is XXX LED on?"
		RSmManager.promptOperator( prompt[idx],
			SmPromptId::ACCEPT_PASS_AND_CANCEL_FAIL_ONLY);
	
		// get operator keypress and process it
		SmPromptId::ActionId operatorAction =
							 RSmManager.getOperatorAction();

		CLASS_ASSERTION (
				operatorAction == SmPromptId::KEY_ACCEPT ||
				operatorAction == SmPromptId::KEY_CLEAR ||
				operatorAction == SmPromptId::USER_EXIT );

		if (operatorAction == SmPromptId::KEY_CLEAR)
		{
		// $[TI2.1]
			// LED failed
			RSmManager.sendStatusToServiceData(
				SmStatusId::TEST_ALERT, condition[idx] );
		}
		else if (operatorAction == SmPromptId::USER_EXIT)
		{
		// $[TI2.2]
			// EXIT button hit
			operatorExit = TRUE;
		}	
		// $[TI2.3]
		// implied else: tested LED passes

		// turn off led
		Led::SetState( led[idx], Led::OFF_STATE );
	}
	// end for

	if (operatorExit)
	{
	// $[TI3]
		RSmManager.sendOperatorExitToServiceData();
	}
	// $[TI4]
	// implied else
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: runGuiAudioTest()
//
//@ Interface-Description
//	This method tests the GUI audio device.  No arguments, no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	$[09100]
//	Activate high alarm urgency sound and prompt operator if alarm is
//	audible, then deactivate alarm.  Generate appropriate status message
//	based on operator response to the prompt.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
GuiIoTest::runGuiAudioTest(void)
{
	CALL_TRACE("GuiIoTest::runGuiAudioTest(void)");

	// Activate high urgency alarm sound
	Sound::Start(Sound::SoundId::HIGH_URG_ALARM);

	RSmManager.promptOperator(
					SmPromptId::SOUND_PROMPT,
					SmPromptId::ACCEPT_PASS_AND_CANCEL_FAIL_ONLY);

	SmPromptId::ActionId actionId = RSmManager.getOperatorAction();

	CLASS_ASSERTION(
		actionId == SmPromptId::KEY_ACCEPT ||
		actionId == SmPromptId::KEY_CLEAR ||
		actionId == SmPromptId::USER_EXIT );

	if (actionId == SmPromptId::USER_EXIT)
	{
	// $[TI1]
		RSmManager.sendOperatorExitToServiceData();		
	}
	else if (actionId == SmPromptId::KEY_CLEAR)
	{ 
	// $[TI2]
		RSmManager.sendStatusToServiceData(
			SmStatusId::TEST_FAILURE,
			SmStatusId::CONDITION_1 );
	}
	// $[TI3]
	// implied else: test passed

	// Deactivate high urgency alarm sound
	Sound::CancelAlarm();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: runExternalSerialIoTest()
//
//@ Interface-Description
//	This method tests the GUI serial device comm2 and comm3. It takes no arguments
//  and has no return value. 
//---------------------------------------------------------------------
//@ Implementation-Description
//	Set the serial devices to send and recieve a test message string.
//	Read the serial device input string and check that the input string
//	matches the output string.  Generate an ALERT if there is no input
//	string or if the input string doesn't match the output string.
//	
// $[GC09000] For GUI units with 3 external serial ports, Service Mode...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
GuiIoTest::runExternalSerialIoTest(void)
{
	CALL_TRACE("GuiIoTest::runExternalSerialIoTest(void)");

	SerialInterface::PortNum sendPortNum;
	SerialInterface::PortNum recvPortNum;
	char recvByte;
	Boolean testFailed = FALSE;

    // Ensure we are in a GuiComms configuration
	CLASS_ASSERTION( IsGuiCommsConfig() );

	// Ask the operator to install loop through cable
	RSmManager.promptOperator (SmPromptId::CONNECT_CABLE_PROMPT,
			SmPromptId::ACCEPT_KEY_ONLY);
	SmPromptId::ActionId operatorAction = RSmManager.getOperatorAction();
    CLASS_ASSERTION ( operatorAction == SmPromptId::KEY_ACCEPT );

	// Notify serial port tasks to enable external loopback
	SerialInterface::EnableExternalLoopback(SerialInterface::SERIAL_PORT_2);
	SerialInterface::EnableExternalLoopback(SerialInterface::SERIAL_PORT_3);

	for(Int32 idx = 0; idx < 2; idx++)
	{
	// $[TI1]

		Task::Delay( 0, 100);

	    if(idx == 0)
	    {
		// $[TI1.1]
	       // Send test message from comm2 -> comm3
	       sendPortNum = SerialInterface::SERIAL_PORT_2;
	       recvPortNum = SerialInterface::SERIAL_PORT_3;
	    }
	    else
		{
		// $[TI1.2]
			// Send test message from comm3 -> comm2
			sendPortNum = SerialInterface::SERIAL_PORT_3;
			recvPortNum = SerialInterface::SERIAL_PORT_2;
		}

		SerialPort& rSendPort = SerialInterface::GetSerialPort(sendPortNum);
		SerialPort& rRecvPort = SerialInterface::GetSerialPort(recvPortNum);

		for ( Uint ii=0; ii<TEST_MSG_SIZE; ii++ )
		{
		// $[TI1.3]
			// Transmit 1 byte of the test message on the send port
			rSendPort.write( &TEST_MSG[ii], 1, 20 );
			rSendPort.wait( 20 );

			// Receive 1 byte from the receive port
			Uint bytesRead = rRecvPort.read( &recvByte, 1, 20 );

			// Send alert status if no byte is received or the
			// transmit and receive bytes mismatch.
			// 
			if ( !bytesRead || TEST_MSG[ii] != recvByte )
			{
			// $[TI1.3.1]
				RSmManager.sendStatusToServiceData(
					SmStatusId::TEST_ALERT, SmStatusId::CONDITION_1 );

				// No need to continue testing, so set flag to break out
				// of outer for loop then break out of inner for loop.
				testFailed = TRUE;
				break;
			}
			// $[TI1.3.2]
		}

		// if test failed break out of outer for loop
		if (testFailed)
		{
		// $[TI1.4]
			break;
		}
		// $[TI1.5]
	}

	// Notify serial port tasks to disable external loopback
	SerialInterface::DisableExternalLoopback(SerialInterface::SERIAL_PORT_2);
	SerialInterface::DisableExternalLoopback(SerialInterface::SERIAL_PORT_3);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: runSerialIoTest()
//
//@ Interface-Description
//	This method tests the GUI serial device.  It takes no arguments
//  and has no return value. 
//---------------------------------------------------------------------
//@ Implementation-Description
//	$[09165]
//	Set the serial device to loopback mode and output a test string.
//	Read the serial device input string and check that the input string
//	matches the output string.  Generate an ALERT if there is no input
//	string or if the input string doesn't match the output string.
//	Disable loopback mode.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
GuiIoTest::runSerialIoTest(void)
{
	CALL_TRACE("GuiIoTest::runSerialIoTest(void)");

	UserAnnunciationMsg convertMsg;
	Int32 msgRecv;

	const Int32 STRING_ARRAY_SIZE = sizeof( TEST_MSG );
	const Int32 SERIAL_TEST_TIMEOUT = 1000;

	char inString[STRING_ARRAY_SIZE];
	Int32 inStringLength;

	const Int32 NUM_SERIAL_PORTS = IsGuiCommsConfig() ? 3 : 1;

	for (Int32 idx = 0; idx < NUM_SERIAL_PORTS; idx++)
	{
	  Int32 conditionId = SmStatusId::CONDITION_1 + idx;
	  SerialInterface::PortNum portNum = SerialInterface::PortNum(idx);

	  // clear the input string array
	  clearString_( inString, STRING_ARRAY_SIZE);

	  // flush test Q of any old messages
	  guiIoTestQ_.flush();

	  SerialInterface::EnableLoopback(portNum);

	  // put test string (including terminating null) into the
	  // serial device output buffer
	  Uint byteCount = SerialInterface::Write( 
						  TEST_MSG, sizeof(TEST_MSG), 0, portNum );
	  CLASS_ASSERTION( byteCount == sizeof(TEST_MSG) );

#ifdef SIGMA_UNIT_TEST
	  // unit test driver needs to put messages into the test Q
	  // and the serial input buffer to force each TI to execute
	  GuiIoTest_UT::SerialIoTestInsertion();
#endif  // SIGMA_UNIT_TEST

	  // wait for serial device to indicate that it received
	  // an input string
	  Int32 status = guiIoTestQ_.pendForMsg( msgRecv, SERIAL_TEST_TIMEOUT);
	  convertMsg.qWord = msgRecv;
	  CLASS_ASSERTION (status == Ipc::OK || status == Ipc::TIMED_OUT);

	  // disable loopback here so we don't loopback our status report!
	  SerialInterface::DisableLoopback(portNum);
#ifdef SIGMA_UNIT_TEST
	  GuiIoTest_UT::TestQStatus = status;
#endif  // SIGMA_UNIT_TEST

	  if( status == Ipc::TIMED_OUT)
	  { 											// $[TI1]
		RSmManager.sendStatusToServiceData(
			  SmStatusId::TEST_ALERT,
			  (SmStatusId::TestConditionId) conditionId );

#ifdef SIGMA_DEBUG
		printf("testSerialIoTest_() TIMED OUT!!\n");
#endif // SIGMA_DEBUG

	  }
	  else
	  { 											// $[TI2]
		// only a serial request event is expected
		CLASS_ASSERTION( 
			   convertMsg.serialRequestParts.eventType ==
				UserAnnunciationMsg::SERIAL_REQUEST_EVENT );
		CLASS_ASSERTION(convertMsg.serialRequestParts.portNum == idx);

		// read the input string from serial device input buffer
		inStringLength = SerialInterface::Read( 
							inString, sizeof(inString), 0, portNum );

#ifdef SIGMA_DEBUG
		printf("inString = %s\n", inString);
		printf("strlen(inString) = %d\n", strlen(inString));
		printf("inStringLength = %d\n", inStringLength);
#endif //SIGMA_DEBUG

		// if input string length exceeds array size, we're hosed
		CLASS_ASSERTION( inStringLength <= STRING_ARRAY_SIZE);

		// compare input and output strings
		if (memcmp( inString, TEST_MSG, sizeof(TEST_MSG) ) != 0)
		{
		// $[TI2.1]
		  RSmManager.sendStatusToServiceData(
				SmStatusId::TEST_ALERT,
				(SmStatusId::TestConditionId) conditionId );

#ifdef SIGMA_DEBUG
			printf("input and output strings don't match!!\n");
#endif // SIGMA_DEBUG

		}
		// $[TI2.2]
		// implied else: test passed
	  }
	// end else
	}

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: runNurseCallTest()
//
//@ Interface-Description
//	This method tests the nurse call.  It takes no argument and has no
//	return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	$[09157]
//	Prompt user if nurse call is to be tested: ACCEPT = test it,
//	CANCEL = generate "not installed" status and skip test.
//	Turn nurse call off, then prompt user if nurse call is off: ACCEPT
//	= true (pass), CANCEL = false (fail).
//	Turn nurse call on,  then prompt user if nurse call is off: ACCEPT
//	= true (pass), CANCEL = false (fail).
//	Turn nurse call off.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
GuiIoTest::runNurseCallTest(void)
{
	CALL_TRACE("GuiIoTest::runNurseCallTest(void)");

	// Perform nurse call relay test
	Boolean operatorExit = FALSE;
	Boolean skipTest = FALSE;

	// deactivate nurse call
	ClearRemoteAlarm();

	// ask operator if he wants to test nurse call
	RSmManager.promptOperator (SmPromptId::NURSE_CALL_TEST_PROMPT,
								SmPromptId::ACCEPT_AND_CANCEL_ONLY);
	SmPromptId::ActionId operatorAction = RSmManager.getOperatorAction();

	CLASS_ASSERTION( operatorAction == SmPromptId::KEY_ACCEPT ||
					 operatorAction == SmPromptId::KEY_CLEAR ||
					 operatorAction == SmPromptId::USER_EXIT );

	if (operatorAction == SmPromptId::USER_EXIT)
	{
	// $[TI1]
		// EXIT button pressed
		operatorExit = TRUE;
	}
	else if (operatorAction == SmPromptId::KEY_CLEAR)
   	{
	// $[TI2]
		// nurse call test skipped -- generate "not installed" status
		RSmManager.sendOptionNotInstalledToServiceData();
		skipTest = TRUE;
	}
	// $[TI3]
	// implied else: test the nurse call

	if (!operatorExit && !skipTest)
	{
	// $[TI4]
		// prompt to test nurse call off
		RSmManager.promptOperator (SmPromptId::NURSE_CALL_OFF_PROMPT,
							SmPromptId::ACCEPT_PASS_AND_CANCEL_FAIL_ONLY);
		SmPromptId::ActionId operatorAction = RSmManager.getOperatorAction();

		CLASS_ASSERTION( operatorAction == SmPromptId::KEY_ACCEPT ||
						 operatorAction == SmPromptId::KEY_CLEAR ||
						 operatorAction == SmPromptId::USER_EXIT );

		if (operatorAction == SmPromptId::USER_EXIT)
		{
		// $[TI4.1]
			operatorExit = TRUE;
		}
		else if (operatorAction == SmPromptId::KEY_CLEAR)
	   	{
	   	// $[TI4.2]
			// nurse call stuck ON
			RSmManager.sendStatusToServiceData(
				SmStatusId::TEST_FAILURE,
				SmStatusId::CONDITION_1);

			skipTest = TRUE;
		}
		// $[TI4.3]
		// implied else: nurse call off passes
	}
	// $[TI5]
	// implied else

	if (!operatorExit && !skipTest)
	{
	// $[TI6]
		// activate nurse call
		SetRemoteAlarm();

		// prompt to test nurse call on
		RSmManager.promptOperator (SmPromptId::NURSE_CALL_ON_PROMPT,
							SmPromptId::ACCEPT_PASS_AND_CANCEL_FAIL_ONLY);
		SmPromptId::ActionId operatorAction = RSmManager.getOperatorAction();

		CLASS_ASSERTION( operatorAction == SmPromptId::KEY_ACCEPT ||
						 operatorAction == SmPromptId::KEY_CLEAR ||
						 operatorAction == SmPromptId::USER_EXIT );

		if (operatorAction == SmPromptId::USER_EXIT)
		{
		// $[TI6.1]
			operatorExit = TRUE;
		}
		else if (operatorAction == SmPromptId::KEY_CLEAR)
	   	{
	   	// $[TI6.2]
			// nurse call stuck OFF
			RSmManager.sendStatusToServiceData(
				SmStatusId::TEST_FAILURE,
				SmStatusId::CONDITION_2);

			skipTest = TRUE;
		}
		// $[TI6.3]
		// implied else: nurse call on passes

		// deactivate nurse call
		ClearRemoteAlarm();
	}
	// $[TI7]
	// implied else

	if (operatorExit)
	{
	// $[TI8]
		RSmManager.sendOperatorExitToServiceData();
	}
	// $[TI9]
	// implied else
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//  and 'lineNumber' are essential pieces of information.  The
//  'pFileName' and 'pPredicate' strings may be defaulted in the macro
//  to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//  none 
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 
void
GuiIoTest::SoftFault(const SoftFaultID  softFaultID,
			  const Uint32       lineNumber,
			  const char*        pFileName,
			  const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

	FaultHandler::SoftFault(softFaultID, SERVICE_MODE, GUIIOTEST,
		                      lineNumber, pFileName, pPredicate);
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//=====================================================================
//
//  Private Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: testKeyPress_()
//
//@ Interface-Description
//	This method is used to test an individual GUI key.  It takes
//	two arguments: testId of type SmPromptId::PromptId and
//	guiKeyCode of type GuiKeys. The testId indicates the user prompt for
//	the key to be tested and the guiKeyCode is the expected
//	key code returned by GUI ISR. The key code returned by the
//	GUI IO routine should correspond to the argument guiKeyCode for
//	both the key press and key release actions.  Return TRUE if the
//	correct key press and release are detected, else return FALSE.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Assumption: calling method has already routed GUI key messages to
//	the SERVICE_MODE_GUIIO_Q.
//
//	Prompt the user to press the appropriate key based on testId argument.
//	Wait for the appropriate key press and key release messages on the
//	SERVICE_MODE_GUIIO_Q, timeout if key press or key release takes
//	too long to arrive.  Return TRUE only if press and release messages
//	from the correct key are detected, else return FALSE.
//---------------------------------------------------------------------
//@ PreCondition
//	Arguments testId and guiKeyCode are verified to be acceptable values.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Boolean
GuiIoTest::testKeyPress_(SmPromptId::PromptId testId, GuiKeys guiKeyCode)
{
	CALL_TRACE("GuiIoTest::testKeyPress_(void)");

	// make sure prompt ID's are valid
	CLASS_PRE_CONDITION(
					testId == SmPromptId::KEY_ACCEPT_PROMPT ||
					testId == SmPromptId::KEY_CLEAR_PROMPT ||
					testId == SmPromptId::KEY_INSP_PAUSE_PROMPT ||
					testId == SmPromptId::KEY_EXP_PAUSE_PROMPT ||
					testId == SmPromptId::KEY_MAN_INSP_PROMPT ||
					testId == SmPromptId::KEY_HUNDRED_PERCENT_O2_PROMPT ||
					testId == SmPromptId::KEY_INFO_PROMPT ||
					testId == SmPromptId::KEY_ALARM_RESET_PROMPT ||
					testId == SmPromptId::KEY_ALARM_SILENCE_PROMPT ||
					testId == SmPromptId::KEY_ALARM_VOLUME_PROMPT ||
					testId == SmPromptId::KEY_SCREEN_BRIGHTNESS_PROMPT ||
					testId == SmPromptId::KEY_SCREEN_CONTRAST_PROMPT ||
					testId == SmPromptId::KEY_SCREEN_LOCK_PROMPT );

	Int32 msgRecv;
	Boolean rtnValue = TRUE;
	UserAnnunciationMsg convertMsg;

#if !defined(SIGMA_UNIT_TEST)
	guiIoTestQ_.flush();  // flush test Q of any old messages
#endif 	// !defined(SIGMA_UNIT_TEST)

	RSmManager.promptOperator(testId, SmPromptId::NO_KEYS);

	Int32 status = guiIoTestQ_.pendForMsg(msgRecv, GUI_IO_TEST_TIMEOUT);
	if(status == Ipc::TIMED_OUT)
	{
	// $[TI1]
		rtnValue = FALSE;
	}
	else
	{
	// $[TI2]
		CLASS_ASSERTION (status == Ipc::OK);
	
		convertMsg.qWord = msgRecv;

		// make sure we got a key press message for the proper key
		if ( (convertMsg.keyParts.eventType !=
							UserAnnunciationMsg::INPUT_FROM_KEY) ||
		     (convertMsg.keyParts.action !=
		     				UserAnnunciationMsg::KEY_PRESS) ||
		     (convertMsg.keyParts.keyCode != guiKeyCode) )
		{
		// $[TI2.1]
			rtnValue = FALSE;
		}
		// $[TI2.2]
		// implied else

		status = guiIoTestQ_.pendForMsg(msgRecv, GUI_IO_TEST_TIMEOUT);
		// has to set after key press event has been received
		KeyInputAllowed = TRUE;	
		if(status == Ipc::TIMED_OUT)
		{
		// $[TI2.3]
			rtnValue = FALSE;
		}
		else
		{
		// $[TI2.4]
			CLASS_ASSERTION (status == Ipc::OK);

			convertMsg.qWord = msgRecv;

			// make sure we got a key release message for the proper key
			if ( (convertMsg.keyParts.eventType !=
								UserAnnunciationMsg::INPUT_FROM_KEY) ||
			     (convertMsg.keyParts.action !=
			     				UserAnnunciationMsg::KEY_RELEASE) ||
			     (convertMsg.keyParts.keyCode != guiKeyCode) )
			{
			// $[TI2.4.1]
				rtnValue = FALSE;
			}
			// $[TI2.4.2]
			// implied else
		}
		// end else
	}
	// end else

	return (rtnValue);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: testKnob_()
//
//@ Interface-Description
//	This method is used to test the GUI knob.  It takes an argument
//	indicating direction of turn to test for, and returns an ID indicating
//	either knob turn passed, failed, or EXIT button pressed.
//---------------------------------------------------------------------
//@ Implementation-Description
//	First redirect knob events to the SERVICE_MODE_GUIIO_Q.
//	Prompt user to turn the knob in the direction idicated by the argument.
//	Loop and wait for knob to be turned by the minimum amount in the
//	correct direction, or until user hits EXIT button (i.e. exit EST),
//	or until timeout occurs.  Restore the knob events to GUI application,
//	then return the appropriate ID: "passed", "failed", or "operator exit".
//---------------------------------------------------------------------
//@ PreCondition
//	direction == LEFT or RIGHT
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
GuiIoTest::KnobTestResult
GuiIoTest::testKnob_ (GuiIoTest::KnobDirection direction)
{
	CALL_TRACE("GuiIoTest::testKnob_(GuiIoTest::KnobDirection)");

	CLASS_PRE_CONDITION( direction == LEFT || direction == RIGHT);

	UserAnnunciationMsg convertMsg;
	Int32 msgRecv = Ipc::NO_MESSAGE;
	Int32 testQueueStatus;

	static MsgQueue smActionQ(::SERVICE_MODE_ACTION_Q);
	Int32 exitMsg = Ipc::NO_MESSAGE;
	Int32 actionQueueStatus;

#if !defined(SIGMA_UNIT_TEST)
	guiIoTestQ_.flush();  // flush test Q of any old messages
	smActionQ.flush();  // flush action Q of any old messages
#endif 	// !defined(SIGMA_UNIT_TEST)

	Boolean operatorExit = FALSE;

	// prompt for turn
	if (direction == LEFT)
	{
	// $[TI1]
		RSmManager.promptOperator(
			SmPromptId::TURN_KNOB_LEFT_PROMPT, SmPromptId::USER_EXIT_ONLY);
	}
	else if (direction == RIGHT)
	{
	// $[TI2]
		// prompt for right turn
		RSmManager.promptOperator(
			SmPromptId::TURN_KNOB_RIGHT_PROMPT, SmPromptId::USER_EXIT_ONLY);
	}
	// end else
	
	// Steal knob events from GUI
	RouteKnobEvents(::SERVICE_MODE_GUIIO_Q); 

	OsTimeStamp	delayTimer;

	// wait for either operator exit, knob turn detection, or timeout
	Int16 knobSum = 0;
	Boolean turnedKnob = FALSE;
	while ( delayTimer.getPassedTime() < GUI_IO_TEST_TIMEOUT &&
			!turnedKnob && !operatorExit )
	{
	// $[TI3]
#ifdef SIGMA_UNIT_TEST
		// Use longer loop delay for UT only since we don't want
		// to loop too many times before forcing a timeout failure
		Task::Delay( 0, 500);
#else
		Task::Delay( 0, 10);
#endif  // SIGMA_UNIT_TEST

		// poll status of test and action queues
		testQueueStatus = guiIoTestQ_.acceptMsg(msgRecv);
		CLASS_ASSERTION (testQueueStatus == Ipc::OK ||
									testQueueStatus == Ipc::NO_MESSAGE);
		actionQueueStatus = smActionQ.acceptMsg(exitMsg);
		CLASS_ASSERTION (actionQueueStatus == Ipc::OK ||
									actionQueueStatus == Ipc::NO_MESSAGE);

		// extract messages
		SmPromptId::ActionId actionId = (SmPromptId::ActionId) exitMsg;
		convertMsg.qWord = msgRecv;

		// check for operator exit on the action Q
		if (actionQueueStatus == Ipc::OK)
		{
		// $[TI3.1]
			// only operator exit is a legal keypress
			CLASS_ASSERTION (actionId == SmPromptId::USER_EXIT);

			operatorExit = TRUE;				
		}
		// $[TI3.2]
		// implied else: no action Q message

		// check for knob turn message on the test Q
		if (testQueueStatus == Ipc::OK)
		{
		// $[TI3.3]
			// make sure we got a knob event
			CLASS_ASSERTION (convertMsg.knobParts.eventType ==
							UserAnnunciationMsg::INPUT_FROM_KNOB);

			// get knob delta
#ifdef SIGMA_UNIT_TEST
			Int16 deltaKnob = GuiIoTest_UT::GetKnobDelta();
#else
			Int16 deltaKnob = GetKnobDelta();
            AckKnobEvent();   // acknowledge/ungate knob event message
#endif  // SIGMA_UNIT_TEST

			// sum up knob deltas
			knobSum += deltaKnob;

			// limit sum to 0 if knob is being turned
			// in the wrong direction
			knobSum = limitKnobSum_( knobSum, direction);

#ifdef SIGMA_DEBUG
	printf("%d  %d\n", deltaKnob, knobSum);
#endif //SIGMA_DEBUG

			// detect minimum knob turn
			if (ABS_VALUE(knobSum)  > MIN_KNOB_DELTA)
			{
			// $[TI3.3.1]
				turnedKnob = TRUE;
#ifdef SIGMA_DEBUG
				printf("%s TURN DETECTED\n",
							direction == LEFT ? "LEFT" : "RIGHT");
#endif //SIGMA_DEBUG
			}
			// $[TI3.3.2]
			// implied else: minimum knob turn not detected yet
		}
		// $[TI3.4]
		// implied else: no test Q message
	}
	// end while

	// Restore knob events back to GUI
	RouteKnobEvents();

	// return the test result
	KnobTestResult result;
	if (operatorExit)
	{ // $[TI4]
		result = OPERATOR_EXIT;
	}
	else if (turnedKnob)
	{ // $[TI5]
		result = PASSED;
	}
	else
	{ // $[TI6]
		result = FAILED;
	}	

	return( result);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: limitknobSum_
//
//@ Interface-Description
//	This method is used to limit the sum of knob deltas to zero
//	in the case that the knob is being turned in the wrong direction.
//	Two arguments: the knob sum to be tested and a KnobDirection.
//	The value of the KnobDirection is a unit knob delta in the direction
//	to be tested (LEFT = -1, RIGHT = +1).  Returns the knob sum
//	if its in the correct direction, or zero if the knob sum is in the
//	wrong direction.
//---------------------------------------------------------------------
//@ Implementation-Description
//	If knobSum * (Int32) direction < 0, set knobSum = 0, else don't change
//	it.  Return knobSum.
//---------------------------------------------------------------------
//@ PreCondition
//	direction == LEFT or RIGHT
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Int16
GuiIoTest::limitKnobSum_(Int16 knobSum, KnobDirection direction)
{
	CALL_TRACE("GuiIoTest::limitKnobSum_(Int16 knobSum, \
										KnobDirection direction)");
	
	CLASS_PRE_CONDITION( direction == LEFT ||
						 direction == RIGHT );

	// if knob is being turned in wrong direction, limit
	// the knob sum to 0
	if (knobSum * (Int32) direction < 0)
	{
	// $[TI1]
		knobSum = 0;
	}
	// $[TI2]
	// implied else

	return( knobSum);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: clearString_()
//
//@ Interface-Description
//	This method sets all elements in a string array to nulls.
//	Arguments are the string array address and array length, no
//	return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Set all array elements to 0.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
GuiIoTest::clearString_(char* string, Int32 length)
{
	CALL_TRACE("GuiIoTest::clearString_(char* string, Int32 length)");

	for (Int32 idx = 0; idx < length; idx++)
	{
	// $[TI1]
		string[idx] = 0;
	}
}

#endif //defined(SIGMA_GUI_CPU)
