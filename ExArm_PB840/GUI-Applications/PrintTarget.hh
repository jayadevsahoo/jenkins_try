#ifndef PrintTarget_HH
#define PrintTarget_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: PrintTarget -  Printing callbacks virtual base class
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/PrintTarget.hhv   25.0.4.0   19 Nov 2013 14:08:16   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By: gdc      Date:  30-Mar-2007   SCR Number: 6237
//  Project:  Trend
//  Description:
//  Initial Version
//=====================================================================
#include "GuiApp.hh"

//@ Usage-Classes
//@ End-Usage

class PrintTarget
{
public:
	PrintTarget(void);
	~PrintTarget(void);

	virtual void cancelPrintRequest(void) = 0;
	virtual void transitionCaptureToPrint(void) = 0;
	virtual void printDone(void) = 0;
	virtual void updatePrintButton(void) = 0;

	static void SoftFault(const SoftFaultID softFaultID,
							     const Uint32      lineNumber,
							     const char*       pFileName  = NULL, 
							     const char*       pPredicate = NULL);

private:
	// these methods are purposely declared, but not implemented...
	PrintTarget(const PrintTarget&);	// not implemented...
	void operator=(const PrintTarget&);	// not implemented...

};

#endif // PrintTarget_HH 
