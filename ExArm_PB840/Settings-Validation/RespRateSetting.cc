#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//================= C L A S S   D E S C R I P T I O N =================
//@ Class:  RespRateSetting - Respiratory Rate Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, bounded setting that contains the rate of respiration
//  (in breaths-per-minute) that is to be applied to the patient.  This
//  class inherits from 'BoundedSetting' and provides the specific
//  information needed for representation of respiratory rate.  This
//  information includes the interval and range of this setting's values,
//  this setting's response to a Main Control Setting's transition (see
//  'acceptTransition()'), this batch setting's new-patient value (see
//  'getNewPatientValue()'), and the dependent settings of this setting.
//
//  This class provides a static method for calculating the breath period
//  based on the current respiratory rate.  This calculation method provides
//  a standard way for all settings (as needed) to calculate a breath period.
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class defines a 'calcDependentValues_()' method for updating
//  this setting's dependent settings, and an 'updateConstraints_()' method
//  for updating the constraints of this setting.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/RespRateSetting.ccv   25.0.4.0   19 Nov 2013 14:27:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 008   By: gdc    Date: 24-Sep-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal non-invasive CPAP.
//
//  Revision: 007  By: sah    Date:  22-Aug-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//	VTPC project-related changes:
//      *  eliminated minute volume as a dependent setting
//      *  modified to support peak flow as a dependent with VC+
//
//  Revision: 006   By: sah    Date: 29-Jun-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//      *  modified to use newly-modified 'BoundedRange' class to handle
//         range, via linked list of interval nodes
//      *  incorporated circuit-specific new-patient values and ranges
//
//  Revision: 005   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new minute volume setting as a dependent setting
//	*  added new 'getApplicability()' method
//	*  added new BiLevel-specific dependent settings
//	*  cleaned up DEVELOPMENT-only 'is{Adjusted,Accepted}Valid()' methods
//
//  Revision: 004   By: dosman Date:  28-Jul-1998    DR Number: BILEVEL 141
//  Project:  BILEVEL
//  Description:
//	"round" breath period to a resolution value every time
//
//  Revision: 003   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 002   By: sah    Date:  18-Sep-1997    DR Number: 2360
//  Project: Sigma (R8027)
//  Description:
//	No longer "round" the calculated breath period to a resolution
//	value.  Also, modified 'calcDependentValus()' (see Note), to
//	take into account that though a setting is held constant one of
//	the other settings may declare a bound violation of that setting,
//	when close to its bounds.  Now those reported violations are
//	ignored.  DCS #2359 (below) ignored bounds when I:E ratio is
//	held constant, due to changes for this DCS, the other two settings
//	being held constant need this extra logic, also.
//
//  Revision: 002   By: sah    Date:  10-Aug-1997    DCS Number: 2359
//  Project: Sigma (R8027)
//  Description:
//	Added the checking for ALL of the dependent bounds, because they
//	can happen.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//=====================================================================

#include "RespRateSetting.hh"
#include "SettingConstants.hh"
#include "ConstantParmValue.hh"
#include "MandTypeValue.hh"
#include "ModeValue.hh"
#include "PatientCctTypeValue.hh"

//@ Usage-Classes
#include "SettingsMgr.hh"
#include "ConstantParmSetting.hh"
//@ End-Usage

//@ Code...

//====================================================================
//
//  Static Data Definitions...
//
//====================================================================

// This array is set up dynamically via this class's 'calcDependentValues_()',
// because only two of the three timing parameters are dependents of this
// setting, at any one time...
static SettingId::SettingIdType  ArrDependentSettingIds_[] =
  {
    SettingId::NULL_SETTING_ID,
    SettingId::NULL_SETTING_ID,
    SettingId::NULL_SETTING_ID,

    SettingId::NULL_SETTING_ID
  };


//  $[02242] -- The setting's range ...
//  $[02245] -- The setting's resolution ...
static const BoundedInterval  LAST_NODE_ =
  {
    1.0f,		// absolute minimum...
    0.0f,		// unused...
    TENTHS,		// unused...
    NULL
  };
static const BoundedInterval  NODE1_ =
  {
    10.0f,		// change-point...
    0.1f,		// resolution...
    TENTHS,		// precision...
    &::LAST_NODE_
  };
static const BoundedInterval  INTERVAL_LIST_ =
  {
    150.0f,		// absolute maximum...
    1.0f,		// resolution...
    ONES,		// precision...
    &::NODE1_
  };


//====================================================================
//
//  Public Methods...
//
//====================================================================

//============= M E T H O D   D E S C R I P T I O N ==================
//@ Method: RespRateSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This setting inherits from BoundedSetting.  The method passes to
//  BoundedSetting it's default information. Also, the value interval
//  is initialized.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

RespRateSetting::RespRateSetting(void)
	  : BatchBoundedSetting(SettingId::RESP_RATE,
				::ArrDependentSettingIds_,
			        &::INTERVAL_LIST_,
			        RESP_RATE_MAX_ID,	// maxConstraintId...
			        RESP_RATE_MIN_ID)	// minConstraintId...
{
  CALL_TRACE("RespRateSetting()");
}   // $[TI1]


//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  ~RespRateSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

RespRateSetting::~RespRateSetting(void)
{
  CALL_TRACE("~RespRateSetting()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[01071] -- main settings applicability
//  $[01081] -- breath settings applicability
//---------------------------------------------------------------------
//@ PreCondition
//  (qualifierId == Notification::ACCEPTED  ||
//   qualifierId == Notification::ADJUSTED)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
RespRateSetting::getApplicability(const Notification::ChangeQualifier qualifierId) const
{
  CALL_TRACE("getApplicability(qualifierId)");

  const Setting*  pMode = SettingsMgr::GetSettingPtr(SettingId::MODE);

  DiscreteValue  modeValue;
  
  switch (qualifierId)
  {
  case Notification::ACCEPTED :		// $[TI1]
    modeValue = pMode->getAcceptedValue();
    break;
  case Notification::ADJUSTED :		// $[TI2]
    modeValue = pMode->getAdjustedValue();
    break;
  default :
    AUX_CLASS_ASSERTION_FAILURE(qualifierId);
    break;
  }
  
  return((modeValue == ModeValue::AC_MODE_VALUE    ||
	  modeValue == ModeValue::SIMV_MODE_VALUE  ||
	  modeValue == ModeValue::BILEVEL_MODE_VALUE)
	   ? Applicability::CHANGEABLE		// $[TI3]
	   : Applicability::INAPPLICABLE);	// $[TI4]
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [const, virtual]
//
//@ Interface-Description
// $[02243] The setting's new-patient value ...
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns a constant for the new patient value.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
RespRateSetting::getNewPatientValue(void) const
{
  CALL_TRACE("getNewPatientValue()");

  BoundedValue  newPatient;

  const Setting*  pPatientCctType =
		    SettingsMgr::GetSettingPtr(SettingId::PATIENT_CCT_TYPE);

  const DiscreteValue  PATIENT_CCT_TYPE_VALUE =
					pPatientCctType->getAdjustedValue();

  switch (PATIENT_CCT_TYPE_VALUE)
  {
  case PatientCctTypeValue::NEONATAL_CIRCUIT :		// $[TI1]
    newPatient.value = SettingConstants::NP_RESP_RATE_NEO_VALUE;
    break;
  case PatientCctTypeValue::PEDIATRIC_CIRCUIT :		// $[TI2]
    newPatient.value = SettingConstants::NP_RESP_RATE_PED_VALUE;
    break;
  case PatientCctTypeValue::ADULT_CIRCUIT :		// $[TI3]
    newPatient.value = SettingConstants::NP_RESP_RATE_ADULT_VALUE;
    break;
  default :
    // unexpected patient circuit type value...
    AUX_CLASS_ASSERTION_FAILURE(PATIENT_CCT_TYPE_VALUE);
    break;
  }

  newPatient.precision = ONES;

  return(newPatient);
}


//============== M E T H O D   D E S C R I P T I O N ================
//@ Method:  calcNewValue(knobDelta)  [virtual]
//
//@ Interface-Description
//  This method determines the new setting value from the knob delta.
//  It also checks the new value against its bounds, and updates any
//  dependent settings.  A constant reference to this setting's bound
//  status is returned; the bound status will contain bound information
//  about this setting's violated bound, if any.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method is overridden to modify the changing of this setting.
//  Due to the dual dependency of respiratory rate's dependent settings
//  on respiratory rate's value for both the dependent setting VALUES
//  and their bounds, an implementation that a set of changes by '1' is
//  necessary.  Otherwise, as respiratory rate changes, the dependent
//  settings' values and bounds move towards each other, which requires
//  a complex balancing of changing respiratory rate, then backing out,
//  etc.  This overridden behavior of changing by deltas of only '1' at
//  a time, greatly simplifies the implementation.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

const BoundStatus&
RespRateSetting::calcNewValue(const Int16 knobDelta)
{
  CALL_TRACE("calcNewValue(knobDelta)");

  const Uint  ABS_KNOB_DELTA   = ABS_VALUE(knobDelta);
  const Uint  MAX_DELTA_COUNTS = MIN_VALUE(ABS_KNOB_DELTA,	// $[TI1]
					   10);			// $[TI2]
  const Int   KNOB_CLICK       = (knobDelta > 0) ?  1		// $[TI3]
  						 : -1;		// $[TI4]

  const BoundStatus&  rBoundStatus = getBoundStatus();
  Uint                deltaCount = 0u;

  do
  {
    BoundedSetting::calcNewValue(KNOB_CLICK);
  } while (!rBoundStatus.isInViolation()  &&
  	   ++deltaCount < MAX_DELTA_COUNTS);

  return(rBoundStatus);
}


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method: acceptTransition(settingId, toValue, fromValue) [virtual]
//
//@ Interface-Description
//  This method is responsible for updating this setting based on the
//  the Main Control Setting identified by 'settingId' and the transition
//  of its value from 'fromValue' to 'toValue'.
//
//  The setting is interested in the transition of mode from "SPONT".
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02026]\a\ -- transitioning from SPONT to non-BILEVEL...
//  $[02026]\c\ -- transitioning from SPONT to BILEVEL...
//---------------------------------------------------------------------
//@ PreCondition
//  (settingId == SettingId::MODE)
//  (fromValue == ModeValue::SPONT_MODE_VALUE) 
//  ||  (fromValue == ModeValue::CPAP_MODE_VALUE)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
RespRateSetting::acceptTransition(const SettingId::SettingIdType settingId,
				  const DiscreteValue,
				  const DiscreteValue            fromValue)
{
  CALL_TRACE("acceptTransition(settingId, toValue, fromValue)");

  AUX_CLASS_PRE_CONDITION((settingId == SettingId::MODE), settingId);
  AUX_CLASS_PRE_CONDITION((fromValue == ModeValue::SPONT_MODE_VALUE) 
		  || (fromValue == ModeValue::CPAP_MODE_VALUE), fromValue);

  const Setting*  pInspTime = SettingsMgr::GetSettingPtr(SettingId::INSP_TIME);

  const Real32  INSP_TIME_VALUE =
  			BoundedValue(pInspTime->getAdjustedValue()).value;

  // calculate the (30 / Ti) upper limit...
  const Real32  RESP_RATE_UPPER_LIMIT =
			SettingConstants::MSEC_TO_MIN_CONVERSION /
				(2.0f * INSP_TIME_VALUE);

  const BoundedValue  RESP_RATE = getAdjustedValue();

  if (RESP_RATE.value > RESP_RATE_UPPER_LIMIT)
  {   // $[TI1] -- combining Ti with the f would result in inverse ratio...
    BoundedValue  newRespRate;
  
    // use limited respiratory rate...
    newRespRate.value = RESP_RATE_UPPER_LIMIT;

    // reset both constraints BEFORE the warping...
    getBoundedRange_().resetMinConstraint();
    getBoundedRange_().resetMaxConstraint();

    // warp the calculated value to a LOWER "click", because "rounding"
    // may still leave an inverse ratio value...
    getBoundedRange_().warpValue(newRespRate, BoundedRange::WARP_DOWN);

    // store the transition value into the adjusted context...
    setAdjustedValue(newRespRate);
  }   // $[TI2] -- no change of respiratory rate is needed, but...

  setForcedChangeFlag();
}


#if defined(SIGMA_DEVELOPMENT)

//============== M E T H O D   D E S C R I P T I O N =================
// Method:  isAcceptedValid()  [const, virtual]
//
// Interface-Description
//  Is the accepted value of this setting valid as determined by its
//  dependencies?
//---------------------------------------------------------------------
// Implementation-Description
//  The setting determines, and returns its validity.
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

Boolean
RespRateSetting::isAcceptedValid(void) const
{
  CALL_TRACE("isAcceptedValid()");

  Boolean  isValid = BoundedSetting::isAcceptedValid();

  if (isValid)
  {
    if (getApplicability(Notification::ACCEPTED) != Applicability::INAPPLICABLE)
    {
      const Setting*  pExpTime =
			  SettingsMgr::GetSettingPtr(SettingId::EXP_TIME);
      const Setting*  pIeRatio =
			  SettingsMgr::GetSettingPtr(SettingId::IE_RATIO);
      const Setting*  pInspTime =
			  SettingsMgr::GetSettingPtr(SettingId::INSP_TIME);

      isValid = (pExpTime->isAcceptedValid()  &&
		 pIeRatio->isAcceptedValid()  &&
		 pInspTime->isAcceptedValid());
    }
  }

  return(isValid);
}


//============== M E T H O D   D E S C R I P T I O N =================
// Method:  isAdjustedValid()  [const, virtual]
//
// Interface-Description
//  Is the adjusted value of this setting valid as determined by its
//  dependencies?
//---------------------------------------------------------------------
// Implementation-Description
//  The setting determines, and returns its validity.
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

Boolean
RespRateSetting::isAdjustedValid(void)
{
  CALL_TRACE("isAdjustedValid()");

  Boolean  isValid = BoundedSetting::isAdjustedValid();

  if (isValid)
  {
    if (getApplicability(Notification::ADJUSTED) != Applicability::INAPPLICABLE)
    {
      Setting*  pExpTime  = SettingsMgr::GetSettingPtr(SettingId::EXP_TIME);
      Setting*  pIeRatio  = SettingsMgr::GetSettingPtr(SettingId::IE_RATIO);
      Setting*  pInspTime = SettingsMgr::GetSettingPtr(SettingId::INSP_TIME);

      isValid = (pExpTime->isAdjustedValid()  &&
		 pIeRatio->isAdjustedValid()  &&
		 pInspTime->isAdjustedValid());
    }
  }

  return(isValid);
}

#endif  // defined(SIGMA_DEVELOPMENT)


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  GetBreathPeriod(basedOnCategory)  [static]
//
//@ Interface-Description
//  Return the breath period that is based on the either the "proposed"
//  respiratory rate or the "new-patient" respiratory rate.  The "accepted"
//  breath period is only available during DEVELOPMENT mode.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//  Notes: 
//  The breathPeriod is rounded/warped using EXP_TIME, as opposed to
//  INSP_TIME, because INSP_TIME has a coarser resolution with VCV.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Real32
RespRateSetting::GetBreathPeriod(const BasedOnCategory basedOnCategory)
{
  CALL_TRACE("GetBreathPeriod()");

  const Setting*  pRespRate = SettingsMgr::GetSettingPtr(SettingId::RESP_RATE);

  Real32  respRateValue;

  switch (basedOnCategory)
  {
  case BASED_ON_NEW_PATIENT_VALUES :	// $[TI1]
    // get respiratory rate's "new-patient" value...
    respRateValue = BoundedValue(pRespRate->getNewPatientValue()).value;
    break;
  case BASED_ON_ADJUSTED_VALUES :	// $[TI2]
    // get respiratory rate's "adjusted" value...
    respRateValue = BoundedValue(pRespRate->getAdjustedValue()).value;
    break;
  case BASED_ON_ACCEPTED_VALUES :
    // fall through to the 'default' case, if not DEVELOPMENT...
#if defined(SIGMA_DEVELOPMENT)
    // get respiratory rate's "accepted" value...
    respRateValue = BoundedValue(pRespRate->getAcceptedValue()).value;
    break;
#endif // defined(SIGMA_DEVELOPMENT)
  default :
    // unexpected 'basedOnCategory' value...
    AUX_CLASS_ASSERTION_FAILURE(basedOnCategory);
    break;
  };

  BoundedValue breathPeriod;

  // calculate the breath period based on respiratory rate...
  breathPeriod.value = SettingConstants::MSEC_TO_MIN_CONVERSION /
				 respRateValue;
  // round breath period
  BoundedSetting*  pExpTime =
                  SettingsMgr::GetBoundedSettingPtr(SettingId::EXP_TIME);

  pExpTime->warpToRange(breathPeriod, BoundedRange::WARP_NEAREST, FALSE);

  return(breathPeriod.value);
}


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//	      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
RespRateSetting::SoftFault(const SoftFaultID  softFaultID,
			   const Uint32       lineNumber,
			   const char*        pFileName,
			   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
			  RESP_RATE_SETTING, lineNumber, pFileName,
			  pPredicate);
}


//====================================================================
//
//  Protected Methods...
//
//====================================================================

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method: calcDependentValues_(isValueIncreasing)
//
//@ Interface-Description
//  Notify this setting's dependent settings of an update to this
//  primary setting.  If a dependent bound is violated, this setting's
//  bound status is updated with the dependent bound information.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02247] -- this setting's dependent settings...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean
RespRateSetting::calcDependentValues_(const Boolean isValueIncreasing)
{
  CALL_TRACE("calcDependentValues_()");

  const Setting*  pMode = SettingsMgr::GetSettingPtr(SettingId::MODE);

  const DiscreteValue  MODE_VALUE = DiscreteValue(pMode->getAdjustedValue());
  const DiscreteValue  CONSTANT_PARM_VALUE =
		     ConstantParmSetting::GetValue(BASED_ON_ADJUSTED_VALUES);

  Uint  idx = 0u;

  switch (MODE_VALUE)
  {
  case ModeValue::AC_MODE_VALUE :
  case ModeValue::SIMV_MODE_VALUE :			// $[TI1]
    {
      Setting*  pMandType = SettingsMgr::GetSettingPtr(SettingId::MAND_TYPE);

      const DiscreteValue  MAND_TYPE_VALUE = pMandType->getAdjustedValue();

      switch (CONSTANT_PARM_VALUE)
      {
      case ConstantParmValue::INSP_TIME_CONSTANT :	// $[TI1.1]
	//=================================================================
	// adjusting respiratory rate while keeping inspiratory time constant...
	//=================================================================
	ArrDependentSettingIds_[idx++] = SettingId::EXP_TIME;
	ArrDependentSettingIds_[idx++] = SettingId::IE_RATIO;
	break;
      case ConstantParmValue::EXP_TIME_CONSTANT :		// $[TI1.2]
	//==================================================================
	// adjusting respiratory rate while keeping expiratory time constant...
	//==================================================================
	ArrDependentSettingIds_[idx++] = SettingId::IE_RATIO;
	ArrDependentSettingIds_[idx++] = SettingId::INSP_TIME;

	if (MAND_TYPE_VALUE == MandTypeValue::VCP_MAND_TYPE)
	{  // $[TI1.2.1] -- add peak insp flow as a dependent...
	  ArrDependentSettingIds_[idx++] = SettingId::PEAK_INSP_FLOW;
	}  // $[TI1.2.2] -- leave dependent list alone...
	break;
      case ConstantParmValue::IE_RATIO_CONSTANT :		// $[TI1.3]
	//==================================================================
	// adjusting respiratory rate while keeping I:E ratio constant...
	//==================================================================
	ArrDependentSettingIds_[idx++] = SettingId::EXP_TIME;
	ArrDependentSettingIds_[idx++] = SettingId::INSP_TIME;

	if (MAND_TYPE_VALUE == MandTypeValue::VCP_MAND_TYPE)
	{  // $[TI1.3.1] -- add peak insp flow as a dependent...
	  ArrDependentSettingIds_[idx++] = SettingId::PEAK_INSP_FLOW;
	}  // $[TI1.3.2] -- leave dependent list alone...
	break;
      case ConstantParmValue::TOTAL_CONSTANT_PARMS :
      default :
	// unexpected constant parm value...
	AUX_CLASS_ASSERTION_FAILURE(CONSTANT_PARM_VALUE);
	break;
      }
    }
    break;

  case ModeValue::BILEVEL_MODE_VALUE :			// $[TI2]
    switch (CONSTANT_PARM_VALUE)
    {
    case ConstantParmValue::PEEP_HIGH_TIME_CONSTANT :	// $[TI2.1]
      //=================================================================
      // adjusting respiratory rate while keeping PEEP high time constant...
      //=================================================================
      ArrDependentSettingIds_[idx++] = SettingId::PEEP_LOW_TIME;
      ArrDependentSettingIds_[idx++] = SettingId::HL_RATIO;
      break;
    case ConstantParmValue::PEEP_LOW_TIME_CONSTANT :	// $[TI2.2]
      //==================================================================
      // adjusting respiratory rate while keeping PEEP low time constant...
      //==================================================================
      ArrDependentSettingIds_[idx++] = SettingId::HL_RATIO;
      ArrDependentSettingIds_[idx++] = SettingId::PEEP_HIGH_TIME;
      break;
    case ConstantParmValue::HL_RATIO_CONSTANT :		// $[TI2.3]
      //==================================================================
      // adjusting respiratory rate while keeping H:L ratio constant...
      //==================================================================
      ArrDependentSettingIds_[idx++] = SettingId::PEEP_LOW_TIME;
      ArrDependentSettingIds_[idx++] = SettingId::PEEP_HIGH_TIME;
      break;
    case ConstantParmValue::TOTAL_CONSTANT_PARMS :
    default :
      // unexpected constant parm value...
      AUX_CLASS_ASSERTION_FAILURE(CONSTANT_PARM_VALUE);
      break;
    }
    break;

  case ModeValue::SPONT_MODE_VALUE :
  case ModeValue::CPAP_MODE_VALUE :
  case ModeValue::TOTAL_MODE_VALUES :
  default :
    // unexpected mode value...
    AUX_CLASS_ASSERTION_FAILURE(MODE_VALUE);
    break;
  }

  // make sure the array is null-id terminated...
  ArrDependentSettingIds_[idx] = SettingId::NULL_SETTING_ID;

  // forward on to base class...
  return(Setting::calcDependentValues_(isValueIncreasing));
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  updateConstraints_()  [virtual]
//
//@ Interface-Description
//  Update the dynamic bounds of this setting.  This method sets this
//  setting's maximum constraint to the most restrictive of this setting's
//  upper bounds, and, correspondingly, set this setting's minimum
//  constraint to its most restrictive lower bound.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
RespRateSetting::updateConstraints_(void)
{
  CALL_TRACE("updateConstraints_()");

  BoundedRange::ConstraintInfo  maxConstraintInfo;

  maxConstraintInfo.id     = RESP_RATE_MAX_ID;
  maxConstraintInfo.value  = getAbsoluteMaxValue_();
  maxConstraintInfo.isSoft = FALSE;

  getBoundedRange_().updateMaxConstraint(maxConstraintInfo);
}  // $[TI1]


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method: calcBasedOnSetting_(basedOnSettingId, warpDirection, basedOnCategory)  [const]
//
//@ Interface-Description
//  This method is responsible for calculating, and returning, a new
//  "adjusted" value for this setting, based on the "adjusted" value
//  of the dependent setting given by 'basedOnSettingId'.  This method will
//  calculate values for each of this setting's dependent settings,
//  based on values in the Adjusted Context.
//---------------------------------------------------------------------
//@ Implementation-Description
//  NOTE:  there are three duplicitous references in the code below:  when
//         referring to "insp time", it could actually be either inspiratory
//         time, or it may be PEEP high time; and similar relationships
//         between "exp time", expiratory time and PEEP low time, and
//         "I:E ratio", I:E ratio and H:L ratio.
//---------------------------------------------------------------------
//@ PreCondition
//  (basedOnCategory == BASED_ON_ADJUSTED_VALUES)
//  (basedOnSettingId == SettingId::INSP_TIME  ||
//   basedOnSettingId == SettingId::IE_RATIO   ||
//   basedOnSettingId == SettingId::EXP_TIME)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

BoundedValue
RespRateSetting::calcBasedOnSetting_(
			  const SettingId::SettingIdType basedOnSettingId,
			  const BoundedRange::WarpDir    warpDirection,
			  const BasedOnCategory          basedOnCategory
				    ) const
{
  CALL_TRACE("calcBasedOnSetting_(basedOnSettingId, warpDirection, basedOnCategory)");

#if defined(SIGMA_DEVELOPMENT)
  if (Settings_Validation::IsDebugOn(::RESP_RATE_SETTING)) 
  {
    cout << "RRS::cBOS bOSI==" << basedOnSettingId
       << " wD==" << warpDirection
       << " bOC==" << basedOnCategory
       << endl;
  }
#endif // defined (SIGMA_DEVELOPMENT)

  AUX_CLASS_ASSERTION((basedOnCategory == BASED_ON_ADJUSTED_VALUES),
		      basedOnCategory);

  const DiscreteValue  CONSTANT_PARM_VALUE =
		     ConstantParmSetting::GetValue(BASED_ON_ADJUSTED_VALUES);

  const Setting*  pInspTime = SettingsMgr::GetSettingPtr(SettingId::INSP_TIME);

  SettingId::SettingIdType  inspTimeId;
  SettingId::SettingIdType  expTimeId;
  SettingId::SettingIdType  ieRatioId;

  if (pInspTime->getApplicability(Notification::ADJUSTED) !=
						    Applicability::INAPPLICABLE)
  {  // $[TI4] -- inspiratory time is applicable...
    inspTimeId = SettingId::INSP_TIME;
    expTimeId  = SettingId::EXP_TIME;
    ieRatioId  = SettingId::IE_RATIO;
  }
  else
  {  // $[TI5] -- PEEP high time must be applicable...
    inspTimeId = SettingId::PEEP_HIGH_TIME;
    expTimeId  = SettingId::PEEP_LOW_TIME;
    ieRatioId  = SettingId::HL_RATIO;
  }

  Real32  breathPeriodValue;

  switch (basedOnSettingId)
  {
  case SettingId::PEAK_INSP_FLOW :
  case SettingId::INSP_TIME :
  case SettingId::PEEP_HIGH_TIME :
    {   // $[TI1] -- calculate based on Ti and the "constant" setting...
      pInspTime = SettingsMgr::GetSettingPtr(inspTimeId);

      // get the inspiratory time value from the Adjusted Context...
      const Real32  INSP_TIME_VALUE =
			   BoundedValue(pInspTime->getAdjustedValue()).value;

      switch (CONSTANT_PARM_VALUE)
      {
      // $[02017](1) -- using Ti and I:E, calculate Ttot...
      case ConstantParmValue::IE_RATIO_CONSTANT :
	{   // $[TI1.1]
	  const Setting*  pIeRatio = SettingsMgr::GetSettingPtr(ieRatioId);

	  // get the I:E ratio value from the Adjusted Context...
	  const Real32  IE_RATIO_VALUE =
			    BoundedValue(pIeRatio->getAdjustedValue()).value;

	  Real32  eRatioValue;
	  Real32  iRatioValue;

	  if (IE_RATIO_VALUE < 0.0f)
	  {   // $[TI1.1.1]
	    // the I:E ratio value is negative, therefore we have 1:xx with
	    // "xx" the expiratory portion of the ratio...
	    iRatioValue = 1.0f;
	    eRatioValue = -(IE_RATIO_VALUE);
	  }
	  else
	  {   // $[TI1.1.2]
	    // the I:E ratio value is positive, therefore we have xx:1 with
	    // "xx" the inspiratory portion of the ratio...
	    iRatioValue = IE_RATIO_VALUE;
	    eRatioValue = 1.0f;
	  }

	  // calculate the breath period from Ti and I:E...
	  breathPeriodValue = (INSP_TIME_VALUE * (iRatioValue + eRatioValue)) /
					      iRatioValue;
	}
	break;

      // $[02016](1) -- using Ti and Te, calculate Ttot...
      case ConstantParmValue::EXP_TIME_CONSTANT :
	{   // $[TI1.2]
	  const Setting*  pExpTime = SettingsMgr::GetSettingPtr(expTimeId);
	  const Real32  EXP_TIME_VALUE =
			   BoundedValue(pExpTime->getAdjustedValue()).value;

	  // calculate the breath period from Ti and Te...
	  breathPeriodValue = INSP_TIME_VALUE + EXP_TIME_VALUE;

#if defined(SIGMA_DEVELOPMENT)
          if (Settings_Validation::IsDebugOn(::RESP_RATE_SETTING)) 
          {
            cout << "RRS::cBOS ITV==" << INSP_TIME_VALUE
              << " ETV==" << EXP_TIME_VALUE
              << " BPV==" << breathPeriodValue
              << endl;
          }
#endif // defined (SIGMA_DEVELOPMENT)
	}
	break;

      case ConstantParmValue::INSP_TIME_CONSTANT :
      default :
	// unexpected 'CONSTANT_PARM_VALUE' value...
	AUX_CLASS_ASSERTION_FAILURE(CONSTANT_PARM_VALUE);
	break;
      };
    }
    break;

  case SettingId::IE_RATIO :
  case SettingId::HL_RATIO :
    {   // $[TI2] -- calculate based on I:E ratio and the "constant" setting...
      const Setting*  pIeRatio = SettingsMgr::GetSettingPtr(ieRatioId);

      // get the I:E ratio value from the Adjusted Context...
      const Real32  IE_RATIO_VALUE =
			    BoundedValue(pIeRatio->getAdjustedValue()).value;

      Real32  eRatioValue;
      Real32  iRatioValue;

      if (IE_RATIO_VALUE < 0.0f)
      {   // $[TI2.1]
	// the I:E ratio value is negative, therefore we have 1:xx with
	// "xx" the expiratory portion of the ratio...
	iRatioValue = 1.0f;
	eRatioValue = -(IE_RATIO_VALUE);
      }
      else
      {   // $[TI2.2]
	// the I:E ratio value is positive, therefore we have xx:1 with
	// "xx" the inspiratory portion of the ratio...
	iRatioValue = IE_RATIO_VALUE;
	eRatioValue = 1.0f;
      }

      switch (CONSTANT_PARM_VALUE)
      {
      // $[02018](1) -- using I:E and Ti, calculate Ttot...
      case ConstantParmValue::INSP_TIME_CONSTANT :
	{   // $[TI2.3]
	  pInspTime = SettingsMgr::GetSettingPtr(inspTimeId);

	  const Real32  INSP_TIME_VALUE =
			   BoundedValue(pInspTime->getAdjustedValue()).value;

	  // calculate the breath period from I:E and Ti...
	  breathPeriodValue = (INSP_TIME_VALUE * (iRatioValue + eRatioValue)) /
					      iRatioValue;
	}
	break;

      // $[02016](1) -- using I:E and Te, calculate Ttot...
      case ConstantParmValue::EXP_TIME_CONSTANT :
	{   // $[TI2.4]
	  const Setting*  pExpTime = SettingsMgr::GetSettingPtr(expTimeId);
	  const Real32  EXP_TIME_VALUE =
			   BoundedValue(pExpTime->getAdjustedValue()).value;

	  // calculate the breath period from I:E and Te...
	  breathPeriodValue = (EXP_TIME_VALUE * (iRatioValue + eRatioValue)) /
					      eRatioValue;
	}
	break;

      case ConstantParmValue::IE_RATIO_CONSTANT :
      default :
	// unexpected 'CONSTANT_PARM_VALUE' value...
	AUX_CLASS_ASSERTION_FAILURE(CONSTANT_PARM_VALUE);
	break;
      };
    }
    break;

  case SettingId::EXP_TIME :
  case SettingId::PEEP_LOW_TIME :
    {   // $[TI3] -- calculate based on Te and the "constant" setting...
      const Setting*  pExpTime = SettingsMgr::GetSettingPtr(expTimeId);

      // get the expiratory time value from the Adjusted Context...
      const Real32  EXP_TIME_VALUE =
			    BoundedValue(pExpTime->getAdjustedValue()).value;

      switch (CONSTANT_PARM_VALUE)
      {
      // $[02018](2) -- using Te and Ti, calculate Ttot...
      case ConstantParmValue::INSP_TIME_CONSTANT :
	{   // $[TI3.1]
	  pInspTime = SettingsMgr::GetSettingPtr(inspTimeId);

	  const Real32  INSP_TIME_VALUE =
			   BoundedValue(pInspTime->getAdjustedValue()).value;

	  // calculate the breath period from Te and Ti...
	  breathPeriodValue = INSP_TIME_VALUE + EXP_TIME_VALUE;

#if defined(SIGMA_DEVELOPMENT)
          if (Settings_Validation::IsDebugOn(::RESP_RATE_SETTING)) 
          {
            cout << "RRS::cBOS "
              << " ETV==" << EXP_TIME_VALUE
              << " ITV==" << INSP_TIME_VALUE
              << " BPV==" << breathPeriodValue
              << endl;
          }
#endif // defined (SIGMA_DEVELOPMENT)

	}
	break;

      // $[02017](2) -- using Te and I:E, calculate Ttot...
      case ConstantParmValue::IE_RATIO_CONSTANT :
	{   // $[TI3.2]
	  const Setting*  pIeRatio = SettingsMgr::GetSettingPtr(ieRatioId);

	  // get the I:E ratio value from the Adjusted Context...
	  const Real32  IE_RATIO_VALUE =
			    BoundedValue(pIeRatio->getAdjustedValue()).value;

	  Real32  eRatioValue;
	  Real32  iRatioValue;

	  if (IE_RATIO_VALUE < 0.0f)
	  {   // $[TI3.2.1]
	    // the I:E ratio value is negative, therefore we have 1:xx with
	    // "xx" the expiratory portion of the ratio...
	    iRatioValue = 1.0f;
	    eRatioValue = -(IE_RATIO_VALUE);
	  }
	  else
	  {   // $[TI3.2.2]
	    // the I:E ratio value is positive, therefore we have xx:1 with
	    // "xx" the inspiratory portion of the ratio...
	    iRatioValue = IE_RATIO_VALUE;
	    eRatioValue = 1.0f;
	  }

	  // calculate the breath period from Te and I:E...
	  breathPeriodValue = (EXP_TIME_VALUE * (iRatioValue + eRatioValue)) /
					      eRatioValue;
	}
	break;

      case ConstantParmValue::EXP_TIME_CONSTANT :
      default :
	// unexpected 'CONSTANT_PARM_VALUE' value...
	AUX_CLASS_ASSERTION_FAILURE(CONSTANT_PARM_VALUE);
	break;
      };
    }
    break;

  default :
    // unexpected dependent setting id...
    AUX_CLASS_ASSERTION_FAILURE(basedOnSettingId);
    break;
  };

  BoundedValue  respRate;

  // calculate an respiratory rate from the breath period calculated
  // above...
  respRate.value = (SettingConstants::MSEC_TO_MIN_CONVERSION /
				 breathPeriodValue);

#if defined(SIGMA_DEVELOPMENT)
  if (Settings_Validation::IsDebugOn(::RESP_RATE_SETTING))
  {
    cout << "RRS::cBOS bef warp eTv==" << respRate.value << endl;
  }
#endif // defined (SIGMA_DEVELOPMENT)

  // place resp rate on a "click" boundary...
  getBoundedRange().warpValue(respRate, warpDirection, FALSE);

#if defined(SIGMA_DEVELOPMENT)
  if (Settings_Validation::IsDebugOn(::RESP_RATE_SETTING))
  {
    cout << "RRS::cBOS aft warp eTv==" << respRate.value << endl;
  }
#endif // defined (SIGMA_DEVELOPMENT)

  return(respRate);
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getAbsoluteMaxValue_()  [const, virtual]
//
//@ Interface-Description
//  Return this setting's absolute maximum value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This is a virtual method that is overridden to support this setting's
//  dynamic maximum constraint.
//
//  $[02242] -- The setting's range ...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Real32
RespRateSetting::getAbsoluteMaxValue_(void) const
{
  CALL_TRACE("getAbsoluteMaxValue_()");

  Real32  absoluteMaxValue;

  const Setting*  pPatientCctType =
		    SettingsMgr::GetSettingPtr(SettingId::PATIENT_CCT_TYPE);

  const DiscreteValue  PATIENT_CCT_TYPE_VALUE =
					pPatientCctType->getAdjustedValue();

  switch (PATIENT_CCT_TYPE_VALUE)
  {
  case PatientCctTypeValue::NEONATAL_CIRCUIT :		// $[TI1]
    absoluteMaxValue = 150.0f;
    break;
  case PatientCctTypeValue::PEDIATRIC_CIRCUIT :
  case PatientCctTypeValue::ADULT_CIRCUIT :		// $[TI2]
    absoluteMaxValue = 100.0f;
    break;
  default :
    // unexpected patient circuit type value...
    AUX_CLASS_ASSERTION_FAILURE(PATIENT_CCT_TYPE_VALUE);
    break;
  }

  return(absoluteMaxValue);
}
