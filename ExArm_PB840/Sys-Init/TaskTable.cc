#include "stdafx.h"
//=====================================================================
//   This is a proprietary work to which Puritan-Bennett corporation
//   of California claims exclusive right.  No part of this work may
//  be used, disclosed, reproduced, sorted in an information retrieval
//     system, or transmitted by any means, electronic, mechanical,
//   photocopying, recording, or otherwise without the prior written
//      permission of Puritan-Bennett Corporation of California.
//
//          Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: TaskTable - VRTX Task Thread Definitions.
//---------------------------------------------------------------------
//@ Interface-Description
//  The TaskTable class defines the VRTX tasks which are started 
//  during system initialization.  All tasks including Task Control
//  are defined in the task table.  This class provides access to
//  a TaskTableEntry based on the VRTX task ID.  The TaskTableEntry
//  contains the task's name, entry point, priority, task control 
//  queue and other information required by Task Control.
//---------------------------------------------------------------------
//@ Rationale
//  The TaskTable class is a repository for task thread definitions.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The tasking configuration is contained in the private Table_
//  defined in IpcTableDef.cc.  GetTaskTableEntry returns a TaskTableEntry
//  reference for a specified task id.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Sys-Init/vcssrc/TaskTable.ccv   25.0.4.0   19 Nov 2013 14:33:58   pvcs  $
//
//@ Modification-Log
//
//   Revision 002   By:   sah      Date: 01/13/99    DR Number: 5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to a non-inlined method.
//
//   Revision 001   By:   gdc      Date: 07/06/95    DR Number:   n/a  
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//=====================================================================

//=====================================================================
//
//      Includes...
//
//=====================================================================

#include "TaskTable.hh"
#include "IpcIds.hh"
#ifdef SIGMA_DEVELOPMENT
#  include "Ostream.hh"
#  include <stdio.h>
#endif

//@ Usage-Classes
//@ End-Usage

//=====================================================================
//
//      Static Data...
//
//=====================================================================

//  TaskTable::Table_ is defined in TaskTableDef.cc

Boolean   TaskTable::IsInitialized_ = FALSE;

//@ Code...

//=====================================================================
//
//      Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Initialize
//
//@ Interface-Description
//  This method initializes the TaskTable class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Sets the IsInitialized_ flag TRUE.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
TaskTable::Initialize(void)
{
    CALL_TRACE("TaskTable::Initialize(void)");

    TaskTable::IsInitialized_ = TRUE;
    // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetNumEntries
//
//@ Interface-Description
//  Returns the TaskTableEntry reference at taskId into Table
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
Int32
TaskTable::GetNumEntries(void)
{
    CALL_TRACE("GetNumEntries(void)");
    return TaskTable::NumEntries_;
    // $[TI1]
}
 
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetTaskTableEntry
//
//@ Interface-Description
//  Returns reference to TaskTableEntry corresponding to taskId
//  argument.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
const TaskTableEntry &
TaskTable::GetTaskTableEntry(const Int32 taskId)
{
    CALL_TRACE("GetTaskTable(void)");
 
    AUX_CLASS_ASSERTION(0 < taskId && taskId <= TaskTable::NumEntries_, taskId);
    return TaskTable::Table_[taskId-1];
    // $[TI1]
}
 

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: IsInitialized
//
//@ Interface-Description
//  Returns initialization state of the TaskTable class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns value of private member data IsInitialized_.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
Boolean
TaskTable::IsInitialized(void)
{
    CALL_TRACE("IsInitialized(void)"); 
 
    return TaskTable::IsInitialized_;
    // $[TI1]
}
 

#ifdef SIGMA_DEVELOPMENT

//============================ M E T H O D   D E S C R I P T I O N ====
// Method: Print()
//
// Interface-Description
//  Prints out information about all the queues that were created from
//  the file TaskTable.cc
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================
void 
TaskTable::Print(void)
{
    cout << "TASK Table:" << endl;
    for (int taskId = 1; taskId <= TaskTable::NumEntries_; taskId++)
    {
        const TaskTableEntry & entry = TaskTable::GetTaskTableEntry(taskId);
        cout << entry.taskName
             << ": priority = " << entry.priority
             << ", stack size = " << entry.stackSize
             << ", debug = " << (void*)entry.flags
             << ", queue = " << (int)entry.taskIpq << endl;
    }
    printf("\n");
}

#endif // SIGMA_DEVELOPMENT

 

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//@ Interface-Description
//   This method is called when a software fault is detected by the
//   fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//   'lineNumber' are essential pieces of information.  The 'pFileName'
//   and 'pPredicate' strings may be defaulted in the macro to reduce
//   code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This method receives the call for the SoftFault, adds it sub-system
//   and class name ID and sends the message to the non-member function
//   SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
 
void
TaskTable::SoftFault(const SoftFaultID  softFaultID,
                    const Uint32       lineNumber,
                    const char*        pFileName,
                    const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
 
  FaultHandler::SoftFault(softFaultID, SYS_INIT,
                          Sys_Init::ID_TASKTABLE, 
                          lineNumber, pFileName, pPredicate);
}


//=====================================================================
//
//      Protected Methods...
//
//=====================================================================

//=====================================================================
//
//      Private Methods...
//
//=====================================================================




