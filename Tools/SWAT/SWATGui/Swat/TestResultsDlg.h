#pragma once
#include "ntgraphctrl.h"
#include "afxwin.h"
#include "afxcmn.h"

#define MAX_X 96000

// CTestResultsDlg dialog


struct Plots
{
	double x;
	double y;
};


class CTestResultsDlg : public CDialog
{
	DECLARE_DYNAMIC(CTestResultsDlg)

public:
	CTestResultsDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CTestResultsDlg();

// Dialog Data
	enum { IDD = IDD_TEST_RESULT_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	CTime m_start_time;
	CTime m_stop_time;

	double m_graphStart;
	double m_graphStop;

//	double m_Ymin;
//	double m_Ymax;

	int m_nElement;
	int m_nAnnotation;

	CString m_strResultsSessionID;
	CString m_strResultsScriptName;


	DECLARE_MESSAGE_MAP()


public:
	CNtgraphctrl m_Graph;
	virtual BOOL OnInitDialog();
	void GetElementReady(int nElement);
	void FillGraphTable(CString strTable, CString strParam);
	void DrawGraphTable(BOOL bWaveForm); //waveform number is generated evey 5 ms
	void PlotGraph();
	
	Plots *m_Plots;

	void SetTimeLimit(CTime, CTime);

	void SetResultParams(CString strSessionID, CString strScriptName);
	void PopulateList();





	afx_msg void OnBnClickedResetGraph();
	
	CString m_strResults;
	CString m_strComboVar;
	CComboBox m_comboCtrl;
	afx_msg void OnBnClickedButtonPlotGraph();
	void InsertColumns();
	CListCtrl m_List;
	afx_msg void OnCustomDrawList1(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedButtonOpenReport();
	afx_msg void OnBnClickedButtonRefresh();
};
