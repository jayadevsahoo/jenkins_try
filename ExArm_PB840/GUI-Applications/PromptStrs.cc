//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: PromptStrs - Language-independent repository for
// strings displayed in the Prompt Area of the Lower Screen.
//---------------------------------------------------------------------
//@ Interface-Description
// This class exists for encapsulation purposes only.  All prompt
// strings that can be displayed in the Prompt Area are stored as
// static public members of this class and can be accessed globally.
//---------------------------------------------------------------------
//@ Rationale
// Convenient encapsulator for prompt strings and provides for language
// independence.
//---------------------------------------------------------------------
//@ Implementation-Description
// none
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/PromptStrs.ccv   25.0.4.0   19 Nov 2013 14:08:16   pvcs  $
//
//@ Modification-Log
//
//
//  Revision: 006   By:   Boris Kuznetsov      Date: 05-May-2003       DR Number: 6036
//  Project:   Russian
//  Description:
//      Addded include file for PolishPromptStrs
//
//
//  Revision: 005   By:   S. Peters      Date: 16-Oct-2002       DR Number: 6029
//  Project:   Polish
//  Description:
//      Addded include file for PolishPromptStrs
//
//
//  Revision: 004  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//   Revision 003   By:  hhd	Date: 09-Apr-1998        DR Number:  
//   Project:   Sigma   (R8027)
//   Description:
//		Included JapanesePromptStrs.in for Japanese version.	
//
//   Revision 002   By:  sah    Date: 15-Jan-1998        DR Number:  5004
//   Project:   Sigma   (R8027)
//   Description:
//       Removed include of now-obsoleted 'Symbols.hh'.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#define DEFINE_STRS
#include "PromptStrs.hh"

#include "Sigma.hh"

//@ Usage-Classes
#ifdef SIGMA_ASIA
	#include "AsiaPromptStrs.in"
#elif SIGMA_EUROPE
	#include "EuropePromptStrs.in"
#elif SIGMA_CHINESE
	#include "ChinesePromptStrs.in"
#elif SIGMA_ENGLISH
	#include "EnglishPromptStrs.in"
#elif SIGMA_FRENCH
	#include "FrenchPromptStrs.in"
#elif SIGMA_GERMAN
	#include "GermanPromptStrs.in"
#elif SIGMA_ITALIAN
	#include "ItalianPromptStrs.in"
#elif SIGMA_JAPANESE
	#include "JapanesePromptStrs.in"
#elif SIGMA_POLISH
	#include "PolishPromptStrs.in"
#elif SIGMA_PORTUGUESE
	#include "PortuguesePromptStrs.in"
#elif SIGMA_RUSSIAN
	#include "RussianPromptStrs.in"
#elif SIGMA_SPANISH
	#include "SpanishPromptStrs.in"
#endif


//@ End-Usage
