
#ifndef MiscSensorRefs_HH
#define MiscSensorRefs_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Header: MiscSensorRefs - All the external references of various Sensor
//		   references.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/MiscSensorRefs.hhv   25.0.4.0   19 Nov 2013 13:54:08   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 002  By: syw    Date:  05-Dec-1995    DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Removed rO2Sensor, added rAiPcbaRevision and rLowVoltageReference.
//
//	Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: INITIAL RELEASE
//      Project:  Sigma (R8027)
//      Description:
//      	Initial version
//
//=====================================================================

class LinearSensor;
class Barometer;

extern LinearSensor& RO2PsolCurrent;
extern LinearSensor& RAirPsolCurrent;
extern LinearSensor& RExhMotorCurrent;
extern Barometer& RAtmosphericPressureSensor;
extern LinearSensor& RDc10vSentry;
extern LinearSensor& RDc15vSentry;
extern LinearSensor& RDcNeg15vSentry;
extern LinearSensor& RDc5vGui;
extern LinearSensor& RDc12vGui;
extern LinearSensor& RDc12vSentry;
extern LinearSensor& RAlarmCableVoltage;
extern LinearSensor& RDc5vVentHead;
extern LinearSensor& RSystemBatteryVoltage;
extern LinearSensor& RSystemBatteryCurrent;
extern LinearSensor& RSystemBatteryModel;
extern LinearSensor& RPowerFailCapVoltage;
extern LinearSensor& RSafetyValveCurrent;
extern LinearSensor& RSafetyValveSwitchedSide ;
extern LinearSensor& RPowerSourceVoltage;

class Sensor;
extern Sensor& RDacWrap ;
extern Sensor& RAiPcbaRevision ;
extern Sensor& RLowVoltageReference ;

#endif // MiscSensorRefs_HH 
