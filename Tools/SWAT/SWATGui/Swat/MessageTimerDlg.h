#pragma once
#include "afxwin.h"


// CMessageTimerDlg dialog

class CMessageTimerDlg : public CDialog
{
	DECLARE_DYNAMIC(CMessageTimerDlg)

public:
	CMessageTimerDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CMessageTimerDlg();

// Dialog Data
	enum { IDD = MESS_TIMER_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	int m_nSeconds;
	CString m_strParam;
	CString m_strMessage;
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedButtonPass();
	afx_msg void OnBnClickedButtonFail();
	afx_msg void OnBnClickedButtonClose();
	int m_nTestResult;
	CEdit m_EditBoxCtrl;
	CString m_strActual;
	afx_msg void OnBnClickedButtonStopScript();
};
