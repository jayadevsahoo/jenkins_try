#ifndef FlashS29AL008D_HH
#define FlashS29AL008D_HH
//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
//  Filename: FlashS29AL008D.hh - FLASH Memory Programmer for (i.e) Spansion
//---------------------------------------------------------------------
//@ Version-Information
// @ $Header:   /840/Baseline/Operating-System/vcssrc/FlashS29AL008D.hhv   25.0.4.0   19 Nov 2013 14:16:18   pvcs  $
//
//@ Modification-Log
//
//  Revision 001  By: Rhomere Jimenez  Date: 07-March-2006  DR Number: 6192
//   Project:   Xena
//   Description:
//      Initial version.
//=====================================================================

#include "Sigma.hh"
#include "OsClassId.hh"

#define FlashS29AL008D_WSM_BUSY 1

Int FlashS29AL008D_EraseBlock(volatile Uint32 * address);
Int FlashS29AL008D_WriteWord(volatile Uint32 * address, const Uint data);
Int FlashS29AL008D_EraseBlockNonBlocking(volatile Uint32 * address);
Int FlashS29AL008D_Check(volatile Uint32 * address);
Int FlashS29AL008D_GetDeviceId( volatile Uint32 * address );
Uint32 * FlashS29AL008D_GetFlashBlockAddress(Uint8 index);
Int      FlashS29AL008D_GetFlashBlockSize(Uint8 index);
Uint32 * FlashS29AL008D_GetFlashBlock1Size(void);
Uint32   FlashS29AL008D_GetNumSubBlocks(void);
Uint32   FlashS29AL008D_GetTotalSubBlocks(void);
Uint32  FlashS29AL008D_ConvertFlashAddress(Uint32 logicalAddress, Uint32 offsetAddress);
Int FlashS29AL008D_FullChipErase(void);

#endif // FlashS29AL008D_HH
