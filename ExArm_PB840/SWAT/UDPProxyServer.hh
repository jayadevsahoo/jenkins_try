//------------------------------------------------------------------------------
//                   Copyright (c) 2008 - 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
/// @file UDPProxyServer.hh
///
/// This file contains class UDPProxyServer definition.
//------------------------------------------------------------------------------

#ifndef UDPPROXYSERVER_H
#define UDPPROXYSERVER_H


//#if defined( INTEGRATION_TEST_ENABLE )

#include <Windows.h>
#include <winsock2.h>

#include "OsTimeStamp.hh"
#include "SigmaTypes.hh"
#include "NetworkApp.hh"

namespace swat{


class UDPProxyServer{
    public:
        static void UDPProxyServerTask( void );

    private:

	void ServerLoop();

	Boolean OnRecvData(char *pBuffer, ULONG puiValue);
	void WaitForDevKey();
	void SetupSocket();
	void WaitForMessage();
	void Restart();
	void Reset();
	void Halt();

	Int32 m_iState;
	SOCKET m_Socket;

	char m_Buffer[28000];

        static OsTimeStamp m_LastMsgOsTimeStamp;

        enum {
            SETUP_SOCKET           = 1,
            WAIT_FOR_MESSAGE       = 2,
            HALT                   = 3,
            RESET                  = 4,
            RESTART                = 5,
            WAIT_FOR_DATA          = 6,
            WAIT_FOR_DEVKEY        = 7
        };


        static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL,
			  const char*       pPredicate = NULL);


};

}

//#endif

#endif
