
#ifndef HighVentPressureExpTrigger_HH
#define HighVentPressureExpTrigger_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: HighVentPressureExpTrigger - Triggers exhalation to prevent 
//	safety valve from cracking.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/HighVentPressureExpTrigger.hhv   25.0.4.0   19 Nov 2013 13:59:50   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 003  By:  iv    Date:  23-Apr-1996    DR Number:600
//       Project:  Sigma (R8027)
//       Description:
//             Made HIGH_VENT_PRESS_LIMIT extern to be used by BD
//             monitor.
//
//  Revision: 002  By:  sp    Date:  4-Mar-1996    DR Number:674
//       Project:  Sigma (R8027)
//       Description:
//             Remove #ifndef.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#  include "Sigma.hh"

#  include "Breath_Delivery.hh"

//@ Usage-Classes
#  include "BreathTrigger.hh"

//@ End-Usage

//@ Constant: HIGH_VENT_PRESS_LIMIT
// upper limit for ventilator pressure
extern const Real32 HIGH_VENT_PRESS_LIMIT;


class HighVentPressureExpTrigger : public BreathTrigger{
  public:
    HighVentPressureExpTrigger(void);
    virtual ~HighVentPressureExpTrigger(void);

    static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL, 
			  const char*       pPredicate = NULL);

  protected:
    virtual Boolean triggerCondition_(void);

  private:
    HighVentPressureExpTrigger(const HighVentPressureExpTrigger&);		// not implemented...
    void   operator=(const HighVentPressureExpTrigger&);	// not implemented...

};


#endif // HighVentPressureExpTrigger_HH 
