#ifndef CursorSettingButton_HH
#define CursorSettingButton_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: CursorSettingButton - A setting button that displays a
// formatted time
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/CursorSettingButton.hhv   25.0.4.0   19 Nov 2013 14:07:40   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001    By:  rhj    Date:  30-JAN-07    SCR Number: 6237
//  Project:  Trend
//  Description:
//  	Initial Release
//====================================================================

#include "Sigma.hh"
#include "SettingButton.hh"
#include "TextField.hh"

class TimeStamp;

class CursorSettingButton : public SettingButton
{
public:
	CursorSettingButton(SettingId::SettingIdType settingId, 
						StringId messageId,
						SubScreen * pFocusSubScreen);
	~CursorSettingButton(void);

	void setTime(const TimeStamp & rTimeStamp, Boolean isCurrentTime);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:
	// SettingButton virtual method
	virtual void updateDisplay_(Notification::ChangeQualifier qualifierId,
								const SettingSubject*         pSubject);

private:
	// these methods are purposely declared, but not implemented...
	CursorSettingButton(const CursorSettingButton&);	// not implemented...
	void   operator=(const CursorSettingButton&);	 // not implemented...

	//@ Data-Member: buttonText_
	// The text field that displays this button's time setting value.
	TextField  buttonText_;
};


#endif // CursorSettingButton_HH 
