#ifndef InspResistanceTest_HH
#define InspResistanceTest_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class:  InspResistanceTest - Test inspiratory and expiratory limb
//	resistance
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/InspResistanceTest.hhv   25.0.4.0   19 Nov 2013 14:23:40   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006  By: syw    Date: 02-Feb-2000     DR Number: 5327
//  Project:  NeoMode
//  Description:
//      Added extern declarations.
//
//  Revision: 005  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 004  By:  quf    Date: 08-Sep-1997    DR Number: DCS 2445
//       Project:  Sigma (840)
//       Description:
//            Changed adult and ped low resistance FAILURE criteria and
//            added low resistance ALERT criteria, changed ped occlusion
//            ALERT criteria.
//
//  Revision: 003  By:  quf    Date: 05-Aug-1997    DR Number: DCS 2130
//       Project:  Sigma (840)
//       Description:
//            Moved total circuit resistance measurement here from the
//            Expiratory Filter Test (SstFilterTest.cc).  Added an
//            initial wye blocked check during test repeat only.  Added
//            testFlow_, setTestFlow_(), and isWyeBlocked_().
//            Recoded runTest(), measureInspLimbResistance_() and
//            evaluateExhLimbResistance_() for easier code maintenance
//            and unit testing.
//
//  Revision: 002  By:  quf    Date: 22-May-1997    DR Number: DCS 2131
//       Project:  Sigma (840)
//       Description:
//            Modification-Log fix only.
//
//  Revision: 001  By:  mad    Date:  2-Nov-1994    DR Number: none
//	Project:  Sigma (R8027)
//	Description:
//		Initial version (Integration baseline).
//
//====================================================================

#include "ServiceMode.hh"

//@ Usage-Classes

#include "DiscreteValue.hh"
#include "Resistance.hh"

//@ End-Usage

extern const Real32 ADULT_LIMB_OCCL_FAILURE;
extern const Real32 ADULT_LIMB_OCCL_ALERT;
extern const Real32 ADULT_LIMB_LOW_RESISTANCE_FAILURE;
extern const Real32 ADULT_LIMB_LOW_RESISTANCE_ALERT;

extern const Real32 PED_LIMB_OCCL_FAILURE;
extern const Real32 PED_LIMB_OCCL_ALERT;
extern const Real32 PED_LIMB_LOW_RESISTANCE_FAILURE;
extern const Real32 PED_LIMB_LOW_RESISTANCE_ALERT;

extern const Real32 NEO_LIMB_OCCL_FAILURE;
extern const Real32 NEO_LIMB_OCCL_ALERT;
extern const Real32 NEO_LIMB_LOW_RESISTANCE_FAILURE;
extern const Real32 NEO_LIMB_LOW_RESISTANCE_ALERT;

extern const Real32 ADULT_CIRCUIT_TEST_FLOW;
extern const Real32 PED_CIRCUIT_TEST_FLOW;
extern const Real32 NEO_CIRCUIT_TEST_FLOW;


class InspResistanceTest
{
  public:
    InspResistanceTest( void) ;
    ~InspResistanceTest( void) ;

    static void SoftFault( const SoftFaultID	softFaultID,
						   const Uint32		lineNumber,
						   const char		*pFileName  = NULL, 
						   const char		*pPredicate = NULL) ;

	void runTest( void) ;
	
  protected:

  private:
    InspResistanceTest( const InspResistanceTest&) ;	// not implemented...
    void operator=( const InspResistanceTest&) ;		// not implemented...

	Boolean isWyeBlocked_(void);

	Boolean measureInspLimbResistance_(DiscreteValue circuitType);

	Boolean evaluateExhLimbResistance_( Resistance& exhResistance,
									DiscreteValue circuitType);

	void setTestFlow_(DiscreteValue circuitType);

	// test flow used to measure insp/exp limb delta P
	Real32 testFlow_;

} ;


#endif // InspResistanceTest_HH 
