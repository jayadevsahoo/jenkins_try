//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file IPatientDataSet.h
//----------------------------------------------------------------------------

#ifndef IPATIENTDATAEL_H
#define IPATIENTDATAEL_H


#ifndef ENUMDEF_H
namespace Enum
{
	struct EnumDef;
}
#endif 

//-------------------------------------
// C L A S S  D E C L
//-------------------------------------

class IPatientDataEl
{
    public:
    virtual char *getName()               =0;
    virtual bool invalidate()             =0;
    virtual bool isValid()                =0;
    virtual bool unsetValue()             =0; 

    virtual bool setInt  (int)            =0;
    virtual int   getIntValue()           =0;


    virtual bool setFloat(float)          =0;
    virtual float getFloatValue()         =0;


    virtual bool setEnum (unsigned int)   =0;
    virtual char *getEnumValue()          =0;
    virtual char * getEnum(int cnt)       =0;


    virtual bool isTrigger()             = 0;
    virtual bool isWaveform()            = 0;
    virtual bool isInt()                  =0;
    virtual bool isFloat()                =0;
    virtual bool isEnum()                 =0;


    virtual bool   isSinSignal()            = 0;
    virtual double getFrequency()         = 0;
    virtual double getPhase()             = 0;
    virtual double getMax()               = 0;
    virtual double getMin()               = 0;
    virtual bool   setSinSignal    (double dFrequency,double dPhase,double dMin,double dMax) = 0;
    virtual bool   setTriggerSignal(double dFrequency,double dPhase,double dMin,double dMax, double trigger_high, double trigger_low) = 0;

    virtual double getTriggerHigh() = 0;
    virtual double getTriggerLow () = 0;
};

class IPatientDataSet{
    public:
    virtual IPatientDataEl * getPatientDataElement(unsigned int) = 0;
    virtual IPatientDataEl * getWaveformElement   (unsigned int) = 0;
    static IPatientDataSet *getInstance();
};


typedef IPatientDataSet   * (IPatientDataSet_F)() ;


extern "C" __declspec(dllexport) IPatientDataSet * __cdecl  getPatientDataSetInstance();
#endif
