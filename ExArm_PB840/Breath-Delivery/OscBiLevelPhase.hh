
#ifndef OscBiLevelPhase_HH
#define OscBiLevelPhase_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: OscBiLevelPhase - Implements OSC test breath during BiLevel.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/OscBiLevelPhase.hhv   25.0.4.0   19 Nov 2013 13:59:56   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001  By:  syw    Date:  10-Mar-1998    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             BiLevel Initial version
//
//====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"

//@ Usage-Classes

#include "PressureBasedPhase.hh"

//@ End-Usage

class OscBiLevelPhase : public PressureBasedPhase
{
  public:
    OscBiLevelPhase( void) ;
    virtual ~OscBiLevelPhase( void) ;

    static void SoftFault( const SoftFaultID softFaultID,
							   const Uint32      lineNumber,
							   const char*       pFileName  = NULL, 
							   const char*       pPredicate = NULL) ;
  
  protected:
	virtual void determineEffectivePressureAndBiasOffset_( void) ;
	virtual void determineTimeToTarget_( void)  ;
	virtual void determineFlowAccelerationPercent_( void) ;
	virtual void enableExhalationTriggers_( void) ;

  private:
    OscBiLevelPhase( const OscBiLevelPhase&) ;		// not implemented...
    void   operator=( const OscBiLevelPhase&) ;		// not implemented...

	//@ Data-Member: inspTimeMs_
	// inspiration time of the OSC BiLevel breath
	Real32 inspTimeMs_ ;
} ;


#endif // OscBiLevelPhase_HH 
