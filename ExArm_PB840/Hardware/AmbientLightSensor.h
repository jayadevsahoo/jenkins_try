#ifndef AMBIENT_LIGHT
#define AMBIENT_LIGHT
#include "i2c.h"

class AmbientLightSensor_t :
    public I2C
{
public:
    AmbientLightSensor_t(HANDLE FileHandle, uint8_t BusAddress, I2cSubAddressMode_t SubAddrMode, I2cBaudIndex_t BaudIndex, DWORD Timeout);
    ~AmbientLightSensor_t(void);
    void            ToggleTheAmbientLightSensor(void);
    bool            ReadAmbientLightSensor(uint16_t *Reading);
    bool            ProgramAmbientLightSensor(BYTE ResolutionMode);
    BYTE        GetResolutionModeSetting(void) {return ResolutionModeSetting;};
    
protected:
    BYTE ResolutionModeSetting;

};
#endif
