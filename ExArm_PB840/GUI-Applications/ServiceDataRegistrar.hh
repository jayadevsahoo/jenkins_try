#ifndef ServiceDataRegistrar_HH
#define ServiceDataRegistrar_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
//@ Class: ServiceDataRegistrar - The entry point to the
// GUI-Applications for the Service-Data subsystem to register
// changes to GUI App's service data.  Only one GUI object can be notified
// of a single datum change.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/ServiceDataRegistrar.hhv   25.0.4.0   19 Nov 2013 14:08:24   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "TestDataId.hh"

//@ Usage-Classes
#include "GuiAppClassIds.hh"
//@ End-Usage

class ServiceDataTarget;

class ServiceDataRegistrar
{
public:
	static void UpdateHappened(TestDataId::TestDataItemId testId);
	static void ChangeHappened(TestDataId::TestDataItemId testId);
	static void RegisterTarget(ServiceDataTarget* pTarget,
								TestDataId::TestDataItemId testId);

	static void Initialize(void);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:

private:
	// these methods are purposely declared, but not implemented...
	ServiceDataRegistrar(void);							// not implemented...
	ServiceDataRegistrar(const ServiceDataRegistrar&);	// not implemented...
	~ServiceDataRegistrar(void);						// not implemented...
	void operator=(const ServiceDataRegistrar&);		// not implemented...

    //@ Data-Member: pTargets_
    // The array which holds pointers to the ServiceDataTarget objects.
    static ServiceDataTarget *pTargets_[TestDataId::NUM_TEST_DATA_ITEM];

	//@ Data-Member: IsDataChangedArray_
	// An array that indicates which service data items have been changed
	// since the last call to ChangeHappened().
	static Boolean IsDataChangedArray_[TestDataId::NUM_TEST_DATA_ITEM];

};


#endif // ServiceDataRegistrar_HH 
