//----------------------------------------------------------------------------
//            Copyright (c) 2010 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------
/// @file HighResolutionTimeStamp.cc

#include <stdafx.h>
#include "HighResolutionTimeStamp.hh"

// Intialize variable by zero
LARGE_INTEGER HighResolutionTimeStamp::liFrequency_ = {0};

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  HighResolutionTimeStamp
//
//@ Interface-Description
// Constructor, this will assign value to internal counter when object is created 
//---------------------------------------------------------------------
//@ Implementation-Description
//  None
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================
HighResolutionTimeStamp::HighResolutionTimeStamp()
{
	// Verify whether object is already initializd or not 
	if ( liFrequency_.QuadPart == 0 )
	{
		// Get frequency at the time of construction and use for futher calcualtion
		QueryPerformanceFrequency(&liFrequency_); 	
	}

	// Get time and intialize value
	QueryPerformanceCounter(&usecCounter_);

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ~HighResolutionTimeStamp
//
//@ Interface-Description
// Destructor
//---------------------------------------------------------------------
//@ Implementation-Description
//  None
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================
HighResolutionTimeStamp::~HighResolutionTimeStamp()
{

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Update
//
//@ Interface-Description
// Update the counter i.e.  usecCounter_ to latest value. 
// Return the time in micro seconds since previous update 
//---------------------------------------------------------------------
//@ Implementation-Description
//  None
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================
uint64_t 
HighResolutionTimeStamp::Update(void)
{
	// Stores previous value to temp variable 
	LARGE_INTEGER prev = usecCounter_; 

	// Update value with latest value 
	QueryPerformanceCounter(&usecCounter_);

	// return difference of previous update and current time 
	return diffTime_(usecCounter_, prev);

}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetTimeUsec
//
//@ Interface-Description
// Return difference of current counter and last counter i.e. usecCounter_
//---------------------------------------------------------------------
//@ Implementation-Description
//  None
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================
uint64_t 
HighResolutionTimeStamp::GetTimeUsec(void) const
{
	LARGE_INTEGER now = {0};
	
	QueryPerformanceCounter(&now);
	
	// return difference of previous update and current time 
	return diffTime_(now, const_cast<LARGE_INTEGER &>(usecCounter_));

}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: operator-(const HighResolutionTimeStamp &rHRTime) const
//
//@ Interface-Description
//  Subtraction operator, to find difference between to HighResolutionTimeStamp.
//---------------------------------------------------------------------
//@ Implementation-Description
//  None
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================
uint64_t
HighResolutionTimeStamp::operator-(const HighResolutionTimeStamp &rHRTime) const
{
	// Difference of start time i.e. stored in micro secomds
	return diffTime_(const_cast<LARGE_INTEGER &>(usecCounter_), 
								const_cast<LARGE_INTEGER &>(rHRTime.usecCounter_) );


}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: diffTime_
//
//@ Interface-Description
//  Returns the difference beteween two time 
//---------------------------------------------------------------------
//@ Implementation-Description
//  None
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================
int64_t HighResolutionTimeStamp::diffTime_(LARGE_INTEGER &liTime1, LARGE_INTEGER &liTime2) const
{
	LARGE_INTEGER liDiff = {0};
	uint64_t uResult = 0;

	liDiff.QuadPart = liTime1.QuadPart - liTime2.QuadPart;

	// To guard against loss-of-precision, first convert to microseconds 
	liDiff.QuadPart = liDiff.QuadPart * 1000000;
	
	// Dividing by ticks-per-second and Assign value 
	uResult = liDiff.QuadPart / liFrequency_.QuadPart;

	return uResult;
}