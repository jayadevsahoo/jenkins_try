#ifndef TcvPhase_HH
#define TcvPhase_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: TcvPhase - Implements tube compensation ventilation inspiration.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header::   /840/Baseline/Breath-Delivery/vcssrc/TcvPhase.hhv   10.7   08/17/07 09:44:08   pvcs  
//
//@ Modification-Log
//
//  Revision: 003   By: syw   Date:  17-Jul-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//		Redesigned to use CompensationBasedPhase as base class.
//
//  Revision: 002  By:  syw    Date:  23-Jun-1999    DR Number: DCS 5447
//       Project:  ATC
//       Description:
//			Add DCS 5322 to header
//
//  Revision: 001  By:  syw    Date:  14-Jan-1999    DR Number: DCS 5322
//       Project:  ATC
//       Description:
//             ATC initial version.
//
//====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"
#include "CompensationBasedPhase.hh"

//@ Usage-Classes

//@ End-Usage

class TcvPhase	 : public CompensationBasedPhase
{
  public:
    TcvPhase( void) ;
    ~TcvPhase( void) ;

    static void SoftFault(  const SoftFaultID softFaultID,
							const Uint32      lineNumber,
							const char*       pFileName  = NULL, 
			 				const char*       pPredicate = NULL) ;

  protected:
	virtual void determineResistivePressure_( const Real32 lungFlow) ;
	virtual void determineElasticPressure_( const Real32 lungVolume) ;

  private:
    TcvPhase( const TcvPhase&) ;  			// not implemented...
    void   operator=( const TcvPhase&) ; 	// not implemented...

} ;

#endif // TcvPhase_HH 
