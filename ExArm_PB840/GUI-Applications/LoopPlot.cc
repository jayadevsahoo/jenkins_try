#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: LoopPlot - A graphical class that can plot a stream of
// waveform data against another stream of patient data for use in the
// Waveforms subscreen.
//---------------------------------------------------------------------
//@ Interface-Description
// The purpose of LoopPlot is to display a waveform of a specified patient data
// stream (such as pressure or flow) against another patient data stream.
// Typically such a waveform forms a loop when drawn.
//
// The plot is displayed on a grid whose X and Y axes are numbered with the
// appropriate units.  Successive data points are connected to the previous
// data point by a line thus forming a continuous waveform.  The waveform
// display is limited to the area of the grid with out-of-range plot points
// simply not being displayed.
//
// This class derives from WaveformPlot which handles the drawing of the grid
// (and the various labels of the grid) on which the waveform is plotted.
// LoopPlot is devoted purely to the plotting of the actual waveform.
//
// Before display of a loop plot the setXYUnits() method must be called.  This
// method informs LoopPlot which waveform data streams it should plot, e.g.
// pressure against volume.
//
// Typically a loop plot is plotted in segments via successive calls to
// the update() method.  endPlot() is called when the full waveform has
// been plotted allowing us to complete drawing of the waveform.  erase()
// erases the waveform and the activate() method should always be called
// before LoopPlot is to be displayed.
//---------------------------------------------------------------------
//@ Rationale
// This class gathers all the functionality for displaying a patient data
// stream against another stream in the one place.
//---------------------------------------------------------------------
//@ Implementation-Description
// The basic functionality of LoopPlot is contained in the update() method
// which is responsible for almost all the waveform plotting.  See update()
// for more information.
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and pre-conditions
// which verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/LoopPlot.ccv   25.0.4.0   19 Nov 2013 14:08:04   pvcs  $
//
//@ Modification-Log
//
//  Revision: 008   By: rhj    Date:  24-Jul-2007    SCR Number: 6393 
//  Project: Trend
//  Description:
//  Fixed a bug where the Flow-Volume and Pressure-Volume loops 
//  are not being plotted.
//
//  Revision: 007   By: gdc    Date:  26-May-2007    SCR Number: 6330
//  Project: Trend
//  Description:
//  Removed SUN prototype code.
//
//  Revision: 006   By: rhj    Date:  02-Oct-2006    SCR Number: 6236
//  Project: RESPM
//  Description:
//  Added Nif, P100 and VC Breath Phase Type
//
//  Revision: 005   By: gdc    Date:  29-Jun-1999    DCS Number: 5327
//  Project: 840 Neonatal
//  Description:
//	Optimized graphics library. Implemented Direct Draw mode for drawing
//  outside of BaseContainer refresh cycle.
//
//  Revision: 004  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 003  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 002  By: yyy      Date: 14-Oct-1997  DR Number: 2421
//    Project:  Sigma (R8027)
//    Description:
//      Removed all references related to auto zero.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "LoopPlot.hh"

//@ Usage-Classes
#include <math.h>
#include "BreathPhaseType.hh"
#include "BreathType.hh"
#include "WaveformIntervalIter.hh"
//@ End-Usage

//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: LoopPlot  [Constructor]
//
//@ Interface-Description
// Default constructor for LoopPlot.
//---------------------------------------------------------------------
//@ Implementation-Description
// Initializes all data members.  Adds the auto-zero message, hidden, to the
// plot container ready for subsequent drawing.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

LoopPlot::LoopPlot(void) :
					xUnits_(PatientDataId::NULL_PATIENT_DATA_ITEM),
					yUnits_(PatientDataId::NULL_PATIENT_DATA_ITEM),
					currX_(0),
					currY_(0),
					lastX_(0),
					lastY_(0),
					isLastPointSet_(FALSE),	
					lineColor_(Colors::GREEN),
					currPhaseType_(BreathPhaseType::INSPIRATION),
					prevPhaseType_(BreathPhaseType::INSPIRATION)
{
	CALL_TRACE("LoopPlot::LoopPlot(void)");				// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~LoopPlot  [Destructor]
//
//@ Interface-Description
// Destroys LoopPlot.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

LoopPlot::~LoopPlot(void)
{
	CALL_TRACE("LoopPlot::LoopPlot(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activate
//
//@ Interface-Description
// Activates a LoopPlot.  Should be called just before display of the plot.
//---------------------------------------------------------------------
//@ Implementation-Description
// Call the base class activate() method and call erase() to do some
// initializing.
//---------------------------------------------------------------------
//@ PreCondition
// The TimePlot should already be tied to particular X and Y units via a call
// to setXYUnits().
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
LoopPlot::activate(void)
{
	CLASS_PRE_CONDITION((PatientDataId::NULL_PATIENT_DATA_ITEM != xUnits_)
						&& (PatientDataId::NULL_PATIENT_DATA_ITEM != yUnits_));

	WaveformPlot::activate();
	erase();											// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: update
//
//@ Interface-Description
// Draws successive segments of a loop plot.  It is passed a reference to a
// WaveformIntervalIter.  This specifies the interval of waveform data that
// must be plotted.
//---------------------------------------------------------------------
//@ Implementation-Description
// If this method is called while this drawable is not visible then the call
// is ignored.
//
// We go through the data in the segment and for each X and Y unit calculate
// the pixel coordinate corresponding to that sample.  Once we calculate the
// first two points of the plot we can begin plotting.
//
// One efficiency check is if successive samples generate the same pixel
// coordinate no line is draw between them, the second sample is effectively
// ignored.
//
// If an auto-zero event is detected for the first time in the current
// breath then an auto-zero message must be displayed.  This message is
// not displayed in the normal way, i.e. by adding a TextField object to
// a container and relying on the BaseContainer method repaint() to draw
// the object when it changes state.  This is because graphical updates
// via the repaint() method could cause areas of the plot area to be erased.
// Instead the auto-zero message is added to the PlotContainer in a hidden
// state (show flag is FALSE) and when needed to be drawn its draw() method
// is directly called which results in the auto-zero message being directly
// plotted on top of the PlotContainer without any underlying area being
// cleared/refreshed.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
LoopPlot::update(WaveformIntervalIter& segmentIter)
{
	CALL_TRACE("LoopPlot::update(WaveformIntervalIter& segmentIter)");

	Real32	currXSampleValue;		// X value of current sample
	Real32	currYSampleValue;		// Y value of current sample
	Int32	currSampleX;			// X pixel coordinate of X value
	Int32	currSampleY;			// Y pixel coordinate of Y value
	Boolean displayLocked = FALSE;  // TRUE when we have the display lock

	// Ignore call if not visible
	if (!isVisible())
	{	
		return;
	}													// $[TI2]

	// Go through each sample in the segment
	for (SigmaStatus status = segmentIter.goFirst(); SUCCESS == status;
											status = segmentIter.goNext())
	{													// $[TI3]
		// Get the X and Y values of the current sample
		currXSampleValue = segmentIter.getBoundedValue(xUnits_);
		currYSampleValue = segmentIter.getBoundedValue(yUnits_);

		// Translate the X and Y values to X and Y pixel coordinates
		currSampleX = xValueToPoint_(currXSampleValue);
		currSampleY = yValueToPoint_(currYSampleValue);

		// Get the breath phase of the current sample
		currPhaseType_ = (BreathPhaseType::PhaseType)segmentIter.
				getDiscreteValue(PatientDataId::BREATH_PHASE_ITEM);

		BreathType currBreathType_ = (BreathType)segmentIter.
				getDiscreteValue(PatientDataId::BREATH_TYPE_ITEM);

		if ( prevPhaseType_ == currPhaseType_ )
		{												// $[TI16.1]
			if ( currBreathType_ == NON_MEASURED )
			{											// $[TI17.1]
				lineColor_ = Colors::WHITE;
			}
			else
			{											// $[TI17.2]
				switch ( currPhaseType_ )
				{
					case BreathPhaseType::EXHALATION:	// $[TI18.1]
						lineColor_ = Colors::YELLOW;
						break;
					case BreathPhaseType::INSPIRATION:	// $[TI18.2]
						switch ( currBreathType_ )
						{
						case CONTROL:
						case ASSIST:					// $[TI19.1]
							lineColor_ = Colors::GREEN;
							break;
						case SPONT:						// $[TI19.2]
						default:
							lineColor_ = Colors::GOLDENROD;
							break;
						}
						break;
					case BreathPhaseType::EXPIRATORY_PAUSE:
					case BreathPhaseType::NIF_PAUSE:
					case BreathPhaseType::P100_PAUSE:
					case BreathPhaseType::VITAL_CAPACITY_INSP:
					case BreathPhaseType::NON_BREATHING:
					default:							// $[TI18.3]
						lineColor_ = Colors::WHITE;
						break;
				}
			}
		}												// $[TI16.2]
		prevPhaseType_ = currPhaseType_;

		// We can't plot a line without at least two points being calculated
		// so check to see if we have another point for plotting the line.
		if (!isLastPointSet_)
		{												// $[TI4]
			// We don't so remember the current point for later use
			currX_ = currSampleX;
			currY_ = currSampleY;
			lastX_ = currSampleX;
			lastY_ = currSampleY;
			isLastPointSet_ = TRUE;
		}
		// Ignore the latest sample if it has the same X and Y coordinates of
		// the current point.
		else if ((currSampleX != currX_) || (currSampleY != currY_))
		{												// $[TI5]
			// We're onto a new point, plot last point
			lastX_ = currX_;
			lastY_ = currY_;
			currX_ = currSampleX;
			currY_ = currSampleY;

			// Got a line segment so let's plot it. 
			// First lock the display if we don't have the lock.
			if ( !displayLocked )
			{											// $[TI6]
				setDirectDraw(TRUE);
				plotLine_.setShow(TRUE );
				displayLocked = TRUE;
			}											// $[TI7]

			plotLine_.setVertices( lastX_, lastY_, currX_, currY_ );
			plotLine_.setColor( lineColor_ );
			plotLine_.drawDirect();

			// yield to a higher priority display request
			if ( isDisplayLockPending() )
			{											// $[TI8]
				plotLine_.setShow(FALSE );
				setDirectDraw(FALSE);
				displayLocked = FALSE;
			}											// $[TI9]
		}												// $[TI10]
	}													// $[TI13]

	// If we have access to the display we must release it.
	if ( displayLocked )
	{													// $[TI14]
		plotLine_.setShow(FALSE );
		setDirectDraw(FALSE);
	}													// $[TI15]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: endPlot	
//
//@ Interface-Description
// This is called at the end of drawing a complete waveform plot and it allows
// the derived plot to do any tidy up needed to complete the waveform drawing.
//---------------------------------------------------------------------
//@ Implementation-Description
// No tidy up neeed.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
LoopPlot::endPlot(void)
{
	CALL_TRACE("LoopPlot::endPlot(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: erase
//
//@ Interface-Description
// Erases a loop plot.
//---------------------------------------------------------------------
//@ Implementation-Description
// Call the base class's method first and then initialize data members to
// indicate that the plot has been erased and we'll be starting anew if
// update() is called next. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
LoopPlot::erase(void)
{
	CALL_TRACE("TimePlot::erase(void)");

	WaveformPlot::erase();
	isLastPointSet_ = FALSE;					// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
LoopPlot::SoftFault(const SoftFaultID  softFaultID,
		   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, LOOPPLOT,
									lineNumber, pFileName, pPredicate);
}
