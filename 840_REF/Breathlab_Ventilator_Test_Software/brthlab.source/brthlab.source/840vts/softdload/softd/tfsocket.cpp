// TfSocket.cpp : implementation file
//

#include "stdafx.h"
#include "softd.h"
#include "TfSocket.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// TftpSocket

//////////////////////////////////////////////////////////////////////
TftpSocket::TftpSocket()
{
	bpServer = NULL;

	return;
}

//////////////////////////////////////////////////////////////////////
TftpSocket::~TftpSocket()
{
	bpServer = NULL;

	return;
}

//////////////////////////////////////////////////////////////////////
TftpSocket::TftpSocket(BootPServer * server)
{
	bpServer = server;

	return;
}

// Do not edit the following lines, which are needed by ClassWizard.
#if 0
BEGIN_MESSAGE_MAP(TftpSocket, CAsyncSocket)
	//{{AFX_MSG_MAP(TftpSocket)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()
#endif	// 0

/////////////////////////////////////////////////////////////////////////////
// TftpSocket member functions

//////////////////////////////////////////////////////////////////////
void TftpSocket::OnAccept(int nErrorCode) 
{
	CAsyncSocket::OnAccept(nErrorCode);

	if(bpServer){
		bpServer->AcceptTftpConnection();
	}

	return;
}
