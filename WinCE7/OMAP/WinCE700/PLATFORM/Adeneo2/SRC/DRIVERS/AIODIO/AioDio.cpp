
//*****************************************************************************
//
//  AioDioDriver.cpp - Implementation file for AioDio_t class.
//
//*****************************************************************************
//#include "stdafx.h"
#include <omap3530.h>
#include <ceddkex.h>
#include <ceddk.h>
#include <tps659xx.h>
#include <pm.h>
#include <oal_io.h>
#include <sdk_spi.h>
#include "AioDio.h"
#include <sdk_gpio.h>
#include <sdk_i2c.h>
#include <oalex.h>
#include <bsp_version.h>
#include <nkintr.h>
#include <gpio_ioctls.h>

#include "tps659xx_internals.h"
#include <initguid.h>
#include <twl.h>
#include "tps659xx_tled.h"
#include "tps659xx_audio.h"

typedef enum
{
    CS_NULL=0,
	CS_SPI_DAC,
	CS_SPI_ADC,
    CS_DIGITAL_PORT_0,
    CS_DIGITAL_PORT_1,
    CS_DIGITAL_PORT_2,
    CS_DIGITAL_PORT_3,
    CS_DIGITAL_PORT_4,
	CS_DIGITAL_PORT_5,
    CS_DIGITAL_PORT_6,
    CS_DIGITAL_PORT_7,
	CS_SPI_11,
    CS_12,
    CS_13,
	CS_14,
    CS_15,
	NUM_CS
}CHIP_SELECT_SETTINGS;

//static const UINT32	NUM_DIO_PORTS		= 8;

static DWORD DIOChipSelect[NUM_DIO_PORTS] =	{	CS_DIGITAL_PORT_0,
												CS_DIGITAL_PORT_1,
												CS_DIGITAL_PORT_2,
												CS_DIGITAL_PORT_3,
												CS_DIGITAL_PORT_4,
												CS_DIGITAL_PORT_5,
												CS_DIGITAL_PORT_6,
												CS_DIGITAL_PORT_7
											};

static const UINT32 DOUT_DIO_A0_BIT      = 14;               // EIO0
static const UINT32 DOUT_DIO_A1_BIT      = 15;               // EIO1
static const UINT32 DOUT_DIO_A2_BIT      = 16;               // EIO2

static const UINT32 DOUT_DIO_GATE_BIT    = 17;               // EIO3

static const UINT32 DINOUT_CRYPTO_DATA_BIT = 104;				// EIO4
static const UINT32 DOUT_CS_0_BIT        = 109;               // EIO5
static const UINT32 DOUT_CS_1_BIT        = 110;               // EIO6
static const UINT32 DOUT_CS_2_BIT        = 111;               // EIO7
static const UINT32 DOUT_CS_3_BIT        = 138;               // S3

static const UINT32 DOUT_OUT_DATA_BIT    = 20;               // EIO8
static const UINT32 DIN_IN_DATA_BIT      = 21;               // EIO9

static const UINT32 DOUT_BL_ENA_BIT		= 19;
static const UINT32 DOUT_BL_CS_BIT		= 134;				// Backlight control chip select

static const UINT32 DOUT_TPS_BL_ENA_BIT		= 269;

static const UINT32 DOUT_DIO_A0_BIT_MASK      = (1 << DOUT_DIO_A0_BIT);				// EIO0
static const UINT32 DOUT_DIO_A1_BIT_MASK      = (1 << DOUT_DIO_A1_BIT);				// EIO1
static const UINT32 DOUT_DIO_A2_BIT_MASK      = (1 << DOUT_DIO_A2_BIT);				// EIO2
static const UINT32 DOUT_DIO_GATE_BIT_MASK    = (1 << DOUT_DIO_GATE_BIT);				// EIO3
static const UINT32 DOUT_CS_0_BIT_MASK        = (1 << (DOUT_CS_0_BIT % 32));			// EIO5
static const UINT32 DOUT_CS_1_BIT_MASK        = (1 << (DOUT_CS_1_BIT % 32));			// EIO6
static const UINT32 DOUT_CS_2_BIT_MASK        = (1 << (DOUT_CS_2_BIT % 32));			// EIO7
static const UINT32 DOUT_CS_3_BIT_MASK        = (1 << (DOUT_CS_3_BIT % 32));			// S3
static const UINT32 DOUT_OUT_DATA_BIT_MASK    = (1 << DOUT_OUT_DATA_BIT);				// EIO8
static const UINT32 DIN_IN_DATA_BIT_MASK      = (1 << DOUT_OUT_DATA_BIT);				// EIO9

#define CS_CLEAR_INDEX	0
#define CS_SET_INDEX	1

static const DWORD CS_Table[NUM_CS][2] =
{
	{DOUT_CS_0_BIT_MASK|DOUT_CS_1_BIT_MASK|DOUT_CS_2_BIT_MASK, 0},		// 0
    {DOUT_CS_1_BIT_MASK|DOUT_CS_2_BIT_MASK, DOUT_CS_0_BIT_MASK},		// 1
	{DOUT_CS_0_BIT_MASK|DOUT_CS_2_BIT_MASK, DOUT_CS_1_BIT_MASK},		// 2
	{DOUT_CS_2_BIT_MASK, DOUT_CS_0_BIT_MASK|DOUT_CS_1_BIT_MASK},		// 3			
	{DOUT_CS_0_BIT_MASK|DOUT_CS_1_BIT_MASK, DOUT_CS_2_BIT_MASK},		// 4
	{DOUT_CS_1_BIT_MASK, DOUT_CS_0_BIT_MASK|DOUT_CS_2_BIT_MASK},		// 5
	{DOUT_CS_0_BIT_MASK, DOUT_CS_1_BIT_MASK|DOUT_CS_2_BIT_MASK},		// 6
	{0, DOUT_CS_0_BIT_MASK|DOUT_CS_1_BIT_MASK|DOUT_CS_2_BIT_MASK},		// 7
	{DOUT_CS_0_BIT_MASK|DOUT_CS_1_BIT_MASK|DOUT_CS_2_BIT_MASK, 0},		// 8
	{DOUT_CS_1_BIT_MASK|DOUT_CS_2_BIT_MASK, DOUT_CS_0_BIT_MASK},		// 9
	{DOUT_CS_0_BIT_MASK|DOUT_CS_2_BIT_MASK, DOUT_CS_1_BIT_MASK},		// 10
	{DOUT_CS_2_BIT_MASK, DOUT_CS_0_BIT_MASK|DOUT_CS_1_BIT_MASK},		// 11
	{DOUT_CS_0_BIT_MASK|DOUT_CS_1_BIT_MASK, DOUT_CS_2_BIT_MASK},		// 12
	{DOUT_CS_1_BIT_MASK, DOUT_CS_0_BIT_MASK|DOUT_CS_2_BIT_MASK},		// 13
	{DOUT_CS_0_BIT_MASK, DOUT_CS_1_BIT_MASK|DOUT_CS_2_BIT_MASK},		// 14
	{0, DOUT_CS_0_BIT_MASK|DOUT_CS_1_BIT_MASK|DOUT_CS_2_BIT_MASK},		// 15
};

// A/D structures and constants - These are used in reading the AD7927.
// Note: The 12 bit control register is written using the high 12 bits in the following constants.
/*
static const UINT16 AinControlRegisterAddressTable[] =
{
    0x0000,                                             // ADC_CH0,
    0x0400,                                             // ADC_CH1
    0x0800,                                             // ADC_CH2
    0x0c00,                                             // ADC_CH3
    0x1000,                                             // ADC_CH4
    0x1400,                                             // ADC_CH5
    0x1800,                                             // ADC_CH6
    0x1c00,                                             // ADC_Ch7
	0x1d00,                                             // ADC_CH8
	0x1e00,                                             // ADC_CH9
	0x1c00,                                             // ADC_CH10
	0x1c00,                                             // ADC_CH11
	0x1c00,                                             // ADC_CH12
	0x1c00,                                             // ADC_CH13
	0x1c00,                                             // ADC_CH14
	0x1c00                                              // ADC_CH15
};

// When the input is read, the 16 bit of input include a leading 0 bit, 3 address bits and 12 bits of data.
// The following corresponds to the expected address bits when masked with 0xf000.
static const UINT16 AinReadDataRegisterAddressTable[] =
{
    0x0000,                                             // ADC_AIRWAY_PRESSURE_TRANSDUCER,
    0x1000,                                             // ADC_INTERNAL_PRESSURE_TRANSDUCER
    0x2000,                                             // ADC_INTERNAL_CASE_TEMPERATURE
    0x3000,                                             // ADC_XD3_SPARE
    0x4000,                                             // ADC_SIGNAL_TEST
    0x5000,                                             // ADC_FIO2_SENSOR
    0x6000,                                             // ADC_SPARE_2
    0x7000                                              // ADC_SPARE_3
};*/

#if BSP_E600_BD_P3_BUILD

#define ADC_SETUP_CHANNEL_ADDRESS(x)		(x << 7)
#define ADC_IN_CHANNEL_ADDRESS(x)			(x << 12)

#define ADC_SELECT_MANUAL_MODE              (0x1000)
#define ADC_SELECT_AUTO1_MODE               (0x0010)
#define ADC_SELECT_AUTO2_MODE               (0x0011)
#define ADC_PROGRAM_AUTO1_MODE              (0x1000)
#define ADC_PROGRAM_AUTO2_MODE              (0x1001)
#define ADC_ENABLE_PROGRAM_DI10_DI00        (0x0800)
#define ADC_ENABLE_PROGRAM_DI06_DI00        (0x0800)
#define ADC_SELECT_5V_REF_RANGE           (0x0040)

#define ADC_NORMAL_OPERATION                (0x0000)
#define ADC_POWER_DOWN                      (0x0020)

#define ADC_DATA_OUTPUT_SERIAL              (0x0000)
#define ADC_DATA_OUTPUT_GPIO                (0x0010)

#define ADC_GPIO_OUTPUT                     (0x0000)

#define ADC_STANDARD_WRITE_SETTINGS     (ADC_SELECT_MANUAL_MODE | ADC_ENABLE_PROGRAM_DI06_DI00 | ADC_SELECT_5V_REF_RANGE | ADC_NORMAL_OPERATION | ADC_DATA_OUTPUT_SERIAL | ADC_GPIO_OUTPUT)

#else

#define ADC_SETUP_CHANNEL_ADDRESS(x)		(x << 10)
#define ADC_IN_CHANNEL_ADDRESS(x)			(x << 12)


// Note: The 12 bit control register is written using the high 12 bits in the following constants.
#define ADC_COMMAND_WRITE                   (0x8000)
#define ADC_COMMAND_READ                    (0x0000)

#define ADC_COMMAND_SEQ_NONE                (0x0000)
#define ADC_COMMAND_SEQ_NO_INT              (0x4000)
#define ADC_COMMAND_SEQ_CONTINUOUS          (0x4080)

#define ADC_COMMAND_PM_NORMAL               (0x0300)
#define ADC_COMMAND_PM_FULL_SHUTDOWN        (0x0200)
#define ADC_COMMAND_PM_AUTO_SHUTDOWN        (0x0100)

#define ADC_COMMAND_RANGE_0_2X_REF          (0x0000)
#define ADC_COMMAND_RANGE_0_REF             (0x0020)

#define ADC_COMMAND_CODING_SIGNED           (0x0000)
#define ADC_COMMAND_CODING_UNSIGNED         (0x0010)

#define ADC_STANDARD_WRITE_SETTINGS         (ADC_COMMAND_WRITE | ADC_COMMAND_SEQ_NONE | ADC_COMMAND_PM_NORMAL | ADC_COMMAND_RANGE_0_2X_REF | ADC_COMMAND_CODING_UNSIGNED)

#endif

#define ANALOG_INPUT_ADDRESS_MASK           (0xF000)
#define ANALOG_INPUT_DATA_MASK              (0x0FFF)

#define DAC_COMMAND_WRITE					(0xF000)


// AD7927 clock can be up to 20 MHz, Data is 12-bit long and shifted in on the falling edge of the clock; CS is active low.
// According to Tony, clock was configured for 2.6 MHz on old hardware.
static const IOCTL_SPI_CONFIGURE_IN spi3_ADC_config = {0, 0x000107C9};	/*	TRM :		0		- TXRX
																			WL :		0xF		- 16 bits
																			CLKD :		2		- Divide Ref Clock of 48Mbps by 4 = 12 Mbps
																			POL PHA :	01		- clk active high and sampling on falling edge
																		*/

// DAC LTC1448 clock can be up to 12.5 MHz, Data is 24-bit long and shifted in on the rising edge of the clock; CS is active low.
// According to Tony, clock was configured for 6.5 MHz on old hardware.
static const IOCTL_SPI_CONFIGURE_IN spi3_DAC_config = {0, 0x00012BC8};	/*	TRM :		2		- TX only
																			WL :		0x17	- 24 bits
																			CLKD :		2		- Divide Ref Clock of 48Mbps by 4 = 12 Mbps
																			POL PHA :	00		- clk active high and sampling on rising edge
																		*/

// DAC MAX553X clock can be up to 16.7 MHz, Data is 16-bit long and shifted in on the rising edge of the clock; CS is active low.
static const IOCTL_SPI_CONFIGURE_IN spi3_DAC_config_16bits = {0, 0x000127C8}; // 0000 0000 0000 0001 0010 0111 1100 1000	
																		/*	TRM :		2		- TX only
																			WL :		0xF		- 16 bits
																			CLKD :		2		- Divide Ref Clock of 48Mbps by 4 = 12 Mbps
																			POL PHA :	00		- clk active high and sampling on rising edge
																		*/

// DAC MAX5384 clock can be up to 10 MHz, Data is 16-bit long and shifted in on the rising edge of the clock; CS is active low.
static const IOCTL_SPI_CONFIGURE_IN spi3_BL_config = {1, 0x000127CC};	//0000 0000 0000 0001 0010 0111 1100 1100
																		/*	TRM :		2		- TX only
																			WL :		0xF		- 16 bits
																			CLKD :		8		- Divide Ref Clock of 48Mbps by 8 = 6 Mbps
																			POL PHA :	00		- clk active high and sampling on rising edge
																		*/

// HoneyWell Pressure Sensor - Max clock is 800KHz
static const IOCTL_SPI_CONFIGURE_IN spi3_SENSOR_config = {1, 0x00010FDC};	//0000 0000 0000 0001 0000 1111 1101 1100
																		/*	TRM :		00		- TXRX
																			WL :		0x1F	- 32 bits
																			CLKD :		7		- Divide Ref Clock of 48Mbps by 128 = 375 Kbps
																			POL PHA :	00		- clk active high and sampling on rising edge
																		*/

#if BSP_E600_BD_P3_BUILD

// E600 DAC DAC7565, Max clock = 50 MHz, 24-bit word (8 control bits and 16 data bits; however, D0-D3 are don't-care bits)
// Data shifted into the DAC on the falling edge of SCLK
static const IOCTL_SPI_CONFIGURE_IN spi1_CH3_DAC_config = {3, 0x00010BC9};		// 0000 0000 0000 0001 0000 1011 1100 1001
																		/*	TRM :		00		- TXRX
																			WL :		0x17	- 24 bits
																			EPOL (6):	1		- SPIM_CSX held low during active state
																			CLKD 5:2):	2		- Divide Ref Clock of 48Mbps by 4 = 12 Mbps
																			POL PHA :	01		- clk active high and sampling on falling edge
																		*/

// ADS7951 - Max clock is 200KHz, used to read ADC on the BD board
static const IOCTL_SPI_CONFIGURE_IN spi1_CH0_ADC_config = {0, 0x000107D0};		//0000 0000 0000 0001 0000 0111 1110 0000
																		/*	TRM (13:12)		:	00		- TXRX
																			WL (11:7)		:	0x0F	- 16 bits
																			EPOL (6)		:	1		- SPIM_CSX held low during active state
																			CLKD (5:2)		:	8		- Divide Ref Clock of 48Mbps by 256 = 187.5 Kbps
																			POL PHA (1:0)	:	00		- clk active high and sampling on rising edge
																		*/
// ADS7951 - Max clock is 200KHz, used to read ADC on Manifold board
static const IOCTL_SPI_CONFIGURE_IN spi3_CH0_ADC_config = {0, 0x000107D0};		//0000 0000 0000 0001 0000 0111 1110 0000
																		/*	TRM (13:12)		:	00		- TXRX
																			WL (11:7)		:	0x0F	- 16 bits
																			EPOL (6)		:	1		- SPIM_CSX held low during active state
																			CLKD (5:2)		:	8		- Divide Ref Clock of 48Mbps by 256 = 187.5 Kbps
																			POL PHA (1:0)	:	00		- clk active high and sampling on rising edge
																		*/
#else

// E600 DAC MAX5135, Max clock = 30 MHz, 24-bit word (8 control bits and 16 data bits; however, D0-D3 are don't-care bits)
// Data shifted into the DAC on the falling edge of SCLK
static const IOCTL_SPI_CONFIGURE_IN spi1_CH3_DAC_config = {3, 0x00010BC8};		// 0000 0000 0000 0001 0000 1011 1100 1000	
																		/*	TRM :		00		- TXRX
																			WL :		0x17	- 24 bits
																			EPOL (6):	1		- SPIM_CSX held low during active state
																			CLKD :		2		- Divide Ref Clock of 48Mbps by 4 = 12 Mbps
																			POL PHA :	00		- clk active high and sampling on rising edge
																		*/

// MAX1068 - Max clock is 200KHz, used to read ADC on the BD board
static const IOCTL_SPI_CONFIGURE_IN spi1_CH0_ADC_config = {0, 0x00010FD0};		//0000 0000 0000 0001 0000 1111 1110 0000
																		/*	TRM (13:12)		:	00		- TXRX
																			WL (11:7)		:	0x1F	- 32 bits
																			EPOL (6)		:	1		- SPIM_CSX held low during active state
																			CLKD (5:2)		:	8		- Divide Ref Clock of 48Mbps by 256 = 187.5 Kbps
																			POL PHA (1:0)	:	00		- clk active high and sampling on rising edge
																		*/
// MAX1068 - Max clock is 200KHz, used to read ADC on Manifold board
static const IOCTL_SPI_CONFIGURE_IN spi3_CH0_ADC_config = {0, 0x00010FD0};		//0000 0000 0000 0001 0000 1111 1110 0000
																		/*	TRM (13:12)		:	00		- TXRX
																			WL (11:7)		:	0x1F	- 32 bits
																			EPOL (6)		:	1		- SPIM_CSX held low during active state
																			CLKD (5:2)		:	8		- Divide Ref Clock of 48Mbps by 256 = 375 Kbps
																			POL PHA (1:0)	:	00		- clk active high and sampling on rising edge
																		

                                                                        */
#endif

// Currently not used
static const IOCTL_SPI_CONFIGURE_IN spi3_CH1_ADC_config = {1, 0x00010FD0};		//0000 0000 0000 0001 0000 0111 1110 0000
																		/*	TRM (13:12)		:	00		- TXRX
																			WL (11:7)		:	0x1F	- 32 bits
																			EPOL (6)		:	1		- SPIM_CSX held low during active state
																			CLKD (5:2)		:	8		- Divide Ref Clock of 48Mbps by 256 = 375 Kbps
																			POL PHA (1:0)	:	00		- clk active high and sampling on rising edge
																		*/

static const LPCTSTR lpSPI1Name = TEXT("SPI1:");
static const LPCTSTR lpSPI3Name = TEXT("SPI3:");
static const LPCTSTR lpGPIOName = TEXT("GIO1:");

static const DWORD ADC_MAX1068_WRITE_COMMAND	= 0x00000000;

#define BYTES_TO_READ_SENSOR		4
#define READ_SENSOR_WORD_LENGTH		32
#define SENSOR_READ_STATUS_MASK		0xC000
#define	SENSOR_STATUS_NORMAL		0

const UINT32 GPIO_MEM_BASE[OMAP_GPIO_BANK_COUNT] = {OMAP_GPIO1_REGS_PA, OMAP_GPIO2_REGS_PA, OMAP_GPIO3_REGS_PA,
													OMAP_GPIO4_REGS_PA, OMAP_GPIO5_REGS_PA, OMAP_GPIO6_REGS_PA};
													//0x48310000, 0x49050000, 0x49052000, 0x49054000, 0x49056000, 0x49058000};
#define SPI_TRANSFER_WAIT_COUNT		768

static AioDio_t	*pAioDio = NULL;

OMAP_GPIO_REGS			*ppGpioRegs[OMAP_GPIO_BANK_COUNT]; //We have 6 GPIO banks
OMAP_MCSPI_REGS			*pSPI3Regs;
OMAP_MCSPI_REGS			*pSPI1Regs;
OMAP_MCSPI_CHANNEL_REGS	*pSPI3Channel0Regs;
OMAP_MCSPI_CHANNEL_REGS	*pSPI3Channel1Regs;
OMAP_MCSPI_CHANNEL_REGS	*pSPI1Channel0Regs;
OMAP_MCSPI_CHANNEL_REGS	*pSPI1Channel3Regs;

void TestPHY();
extern VOID GetOsVersion(NMI_VERSION *pOsVersion);

DWORD BSPtype;
AioDio_t AioDioObj;

HANDLE			hEncIntrThread  = NULL;
HANDLE			hEncIntrEvent = NULL;
HANDLE			hGIO = NULL;
DWORD			dwEncGioIntr;
DWORD			dwEncSysIntr = (DWORD)SYSINTR_UNDEFINED;
DWORD			dwKnobCount = 0;
DWORD			dwKnobDir = 0;
CRITICAL_SECTION EncoderCriticalSection;
BOOL			bEncoderInitialized = FALSE;
BOOL InitEncoderGPIO();
VOID DeinitEncoderGPIO();

#define	ENC_DIR_GPIO_BIT				18
#define ENC_PHASEA_GPIO_BIT				58
#define ENC_DEBOUNCE_TIME				10
#define NUM_ENCODER_EDGES_PER_COUNT		2

#if BSP_E600_UI_P2_BUILD

#define CONSOLE_BUTTON_IRQ_GPIO_BIT		55

HANDLE			hButtonIntrThread;
HANDLE			hButtonIntrEvent;
DWORD			dwButtonSysIntr;
CRITICAL_SECTION ButtonCriticalSection;

#endif

//*****************************************************************************
//
//  AioDio_t::AioDio_t - Construct the class
//
//*****************************************************************************
AioDio_t::AioDio_t()
{
	hGIOPort = NULL;
	hSPI3_CH0 = NULL;
	hSPI3_CH1 = NULL;
		
#if AIODIO_TESTING
	hI2CPort = NULL;
#endif
	hTled = NULL;
}

BOOL
AIO_Deinit(
    DWORD context
    )
{
    BOOL rc = FALSE;
	AioDio_t *pDevice = (AioDio_t*)context;
	int i;

    RETAILMSG(1, (L"+AIODIO_Deinit(0x%08x)\r\n", context));

    // Check if we get correct context
    if (pDevice == NULL)
        {
        RETAILMSG (1, (L"ERROR: AIODIO_Deinit: "
            L"Incorrect context parameter\r\n"
            ));
            goto cleanUp;
        }

	//LocalFree(pAioDio);	//delete(pAioDioObj);
	
	// Unmap module registers
    for (i = 0 ; i < OMAP_GPIO_BANK_COUNT; i++)
    {
       if (ppGpioRegs[i] != NULL)
        {
            MmUnmapIoSpace((VOID*)ppGpioRegs[i], sizeof(OMAP_GPIO_REGS));
        }
    }

	// Unmap SPI controller registers
    if (pSPI3Regs != NULL)
    {
        MmUnmapIoSpace((VOID*)pSPI3Regs, sizeof(OMAP_MCSPI_REGS));
    }
	
	if(bEncoderInitialized)
	{
		DeinitEncoderGPIO();
	}
	
	rc = TRUE;

cleanUp:
    RETAILMSG(1, (L"-AIODIO_Deinit(rc = %d)\r\n", rc));
    return rc;
}

DWORD
AIO_Init(
    LPCTSTR szContext,
    LPCVOID pBusContext
    )
{
	DWORD rc = NULL;
	PHYSICAL_ADDRESS pa;
	
	UNREFERENCED_PARAMETER(szContext);
	UNREFERENCED_PARAMETER(pBusContext);

	RETAILMSG(0, (L"+AIODIO_Init\r\n"));

	for(int i = 0; i < OMAP_GPIO_BANK_COUNT; i++)
	{
		pa.QuadPart = GPIO_MEM_BASE[i];
		ppGpioRegs[i] = (OMAP_GPIO_REGS*)MmMapIoSpace(pa, sizeof(OMAP_GPIO_REGS), FALSE);
	}

	// Map SPI controller
    pa.QuadPart = OMAP_MCSPI3_REGS_PA;
    pSPI3Regs = (OMAP_MCSPI_REGS*)MmMapIoSpace(pa, sizeof(OMAP_MCSPI_REGS), FALSE);
	if(pSPI3Regs == NULL)
		goto cleanUp;

	pSPI3Channel0Regs = (OMAP_MCSPI_CHANNEL_REGS*)(&pSPI3Regs->MCSPI_CHCONF0);
	pSPI3Channel1Regs = (OMAP_MCSPI_CHANNEL_REGS*)(&pSPI3Regs->MCSPI_CHCONF1);
	
	
	pa.QuadPart = OMAP_MCSPI1_REGS_PA;
    pSPI1Regs = (OMAP_MCSPI_REGS*)MmMapIoSpace(pa, sizeof(OMAP_MCSPI_REGS), FALSE);
	if(pSPI1Regs == NULL)
		goto cleanUp;
	
	pSPI1Channel0Regs = (OMAP_MCSPI_CHANNEL_REGS*)(&pSPI1Regs->MCSPI_CHCONF0);
	pSPI1Channel3Regs = (OMAP_MCSPI_CHANNEL_REGS*)(&pSPI1Regs->MCSPI_CHCONF3);
	
	KernelIoControl(IOCTL_HAL_GET_BSP_TYPE, NULL, 0, &BSPtype, sizeof(DWORD), NULL);

	if (BSPtype == BSP_E600_UI)
	{
		if(!InitEncoderGPIO())
		{
			RETAILMSG(1, (L"AIODIO: Failed InitEncGPIO()\r\n"));
			goto cleanUp;
		}
		else
		{
			RETAILMSG(0, (L"AIODIO: InitEncGPIO() success\r\n"));
		}
	}
	
	/*pAioDio = (AioDio_t*)LocalAlloc(LPTR, sizeof(AioDio_t));	//new(AioDio_t);
	if (pAioDio == NULL)
        {
        RETAILMSG(1, (L"ERROR: AIODIO_Init: "
            L"Failed allocate AIODIO driver structure\r\n"
            ));
        goto cleanUp;
        }*/
	pAioDio = &AioDioObj;
	return((DWORD)pAioDio);
	
cleanUp:
    RETAILMSG(1, (L"-AIODIO_Init failed (rc = %d\r\n", rc));
    return rc;
}

//------------------------------------------------------------------------------
//
//  Function:  AIO_Open
//
//  Called by device manager to open a device for reading and/or writing.
//
DWORD
AIO_Open(
    DWORD context,
    DWORD accessCode,
    DWORD shareMode
    )
{
    UNREFERENCED_PARAMETER(accessCode);
	UNREFERENCED_PARAMETER(shareMode);
	return context;
}

//------------------------------------------------------------------------------
//
//  Function:  AIO_Close
//
//  This function closes the device context.
//
BOOL
AIO_Close(
    DWORD context
    )
{
    UNREFERENCED_PARAMETER(context);
	return TRUE;
}

//*****************************************************************************
//
//  AioDio_t::~AioDio_t -
//
//*****************************************************************************

AioDio_t::~AioDio_t()
{
    DeleteCriticalSection(&AioDioCriticalSection);
}

//*****************************************************************************
//
//  AioDio_t::SetDioBitPosition - This function sets the three address lines
//      going to the 74xx251 or 74xx259 parts (lines are shared by all).
//
//*****************************************************************************

void AioDio_t::SetDioBitPosition(DWORD Position)
{
    DWORD CsSetValue = 0, CsClearValue = 0;

	switch (Position)
    {
        case 0:
            // Clear
            CsClearValue = DOUT_DIO_A0_BIT_MASK | DOUT_DIO_A1_BIT_MASK | DOUT_DIO_A2_BIT_MASK;
            break;

        case 1:
            // Set
            CsSetValue = DOUT_DIO_A0_BIT_MASK;
            // Clear
            CsClearValue = DOUT_DIO_A1_BIT_MASK | DOUT_DIO_A2_BIT_MASK;
            break;

        case 2:
            // Set
            CsSetValue = DOUT_DIO_A1_BIT_MASK;
            // Clear
            CsClearValue = DOUT_DIO_A0_BIT_MASK | DOUT_DIO_A2_BIT_MASK;
            break;

        case 3:
            // Set
            CsSetValue = DOUT_DIO_A0_BIT_MASK | DOUT_DIO_A1_BIT_MASK;
            // Clear
            CsClearValue = DOUT_DIO_A2_BIT_MASK;
            break;

        case 4:
            // Set
            CsSetValue = DOUT_DIO_A2_BIT_MASK;
            // Clear
            CsClearValue = DOUT_DIO_A0_BIT_MASK | DOUT_DIO_A1_BIT_MASK;
            break;

        case 5:
            // Set
            CsSetValue = DOUT_DIO_A0_BIT_MASK | DOUT_DIO_A2_BIT_MASK;
            // Clear
            CsClearValue = DOUT_DIO_A1_BIT_MASK;
            break;

        case 6:
            // Set
            CsSetValue = DOUT_DIO_A1_BIT_MASK | DOUT_DIO_A2_BIT_MASK;
            // Clear
            CsClearValue = DOUT_DIO_A0_BIT_MASK;
            break;

        case 7:
            // Set
            CsSetValue = DOUT_DIO_A0_BIT_MASK | DOUT_DIO_A1_BIT_MASK | DOUT_DIO_A2_BIT_MASK;
            break;
    }
	if(CsSetValue != 0)
		SetGpioMultiBits(DOUT_DIO_A0_BIT, CsSetValue);
	if(CsClearValue != 0)
		ClrGpioMultiBits(DOUT_DIO_A0_BIT, CsClearValue);
}


//*****************************************************************************
//
//  AioDio_t::Read - This version of the read function reads the
//      speed counter which is a combination of multiple digital inputs.
//
//*****************************************************************************

UINT32 AioDio_t::ReadCounterInput(DWORD CounterInput0, DWORD CounterInput1, DWORD Port)
{
    EnterCriticalSection(&AioDioCriticalSection);                       // protect access to the chip select lines

    if(DIOChipSelect[Port] != LastPortAccessed)
	{
		WriteChipSelect(DIOChipSelect[Port]);
	}
	
    UINT32 Data[2];
    int TryCount = 0;

    // We read the data twice to verify a bit didn't toggle while we are reading.
    // If they don't match, we'll try one more time.
    do
    {
        for(int i = 0; i < 2; i++)
		{
			// Set the address lines into the 74xx251, setting the bit position
			SetDioBitPosition(CounterInput0);

			// clock in the data
			ClrGpioBit(DOUT_DIO_GATE_BIT);					// Enable
			Data[i] = GetGpioBit(DIN_IN_DATA_BIT);          // Read the bit, mask it, and set the value
			SetGpioBit(DOUT_DIO_GATE_BIT);                  // Disable

			// Set the address lines into the 74xx251, setting the bit position
			SetDioBitPosition(CounterInput1);

			// clock in the data
			ClrGpioBit(DOUT_DIO_GATE_BIT);					// Enable
			Data[i] += GetGpioBit(DIN_IN_DATA_BIT) << 1;	// Read the bit, mask it, and add in the value
			SetGpioBit(DOUT_DIO_GATE_BIT);					// Disable
		}
    }
    while (Data[0] != Data[1] && (++TryCount < 2));

    LeaveCriticalSection(&AioDioCriticalSection);           // Allow access to the chip selects

    return (Data[0]);
}


//*****************************************************************************
//
//  AioDio_t::Read - This version of the read function reads one signal
//      of the digital inputs.
//
//      Return 1 if signal is high,
//             0 if signal is low
//
//*****************************************************************************

UINT32 AioDio_t::ReadDigitalInput(DWORD DigitalInput, DWORD Port)
{
    EnterCriticalSection(&AioDioCriticalSection);       // protect access to the chip select lines

    SetDioBitPosition(DigitalInput);

    if(DIOChipSelect[Port] != LastPortAccessed)
	{
		WriteChipSelect(DIOChipSelect[Port]);
	}
	
    // clock in the data
    ClrGpioBit(DOUT_DIO_GATE_BIT);						// Enable
    UINT32 Data = GetGpioBit(DIN_IN_DATA_BIT);			// Read the bit
    SetGpioBit(DOUT_DIO_GATE_BIT);						// Disable

    LeaveCriticalSection(&AioDioCriticalSection);       // Allow access to the chip selects

    return Data;
}


//*****************************************************************************
//
//  AioDio_t::ReadButtons - The caller must allocate an array of bool type for output
//
//*****************************************************************************

void AioDio_t::ReadButtons(DWORD FirstButton, DWORD NumButtons, DWORD Port, DWORD *ButtonPressedArray)
{
    DWORD i;

	EnterCriticalSection(&AioDioCriticalSection);                                   // protect access to the chip select lines

    // This assumes all buttons are on the same 8 bit port
    if(LastPortAccessed != DIOChipSelect[Port])
	{
		WriteChipSelect(DIOChipSelect[Port]);
	}

	for(i = FirstButton; i < NumButtons; i++)
	{
		SetDioBitPosition(i);
		
		// clock in the data
		ClrGpioBit(DOUT_DIO_GATE_BIT);								// Enable
		ButtonPressedArray[i] = GetGpioBit(DIN_IN_DATA_BIT);
		SetGpioBit(DOUT_DIO_GATE_BIT);								// Disable
	}

	LeaveCriticalSection(&AioDioCriticalSection);									// Allow access to the chip selects
}


//*****************************************************************************
//
//  AioDio_t::Write - This version of the write function updates one line
//      of the digital outputs.
//
//*****************************************************************************

void AioDio_t::WriteDigitalOutput(DWORD DigitalOutput, DWORD State, DWORD Port)
{
    EnterCriticalSection(&AioDioCriticalSection);                       // protect access to the chip select lines

    // Set the address lines into the 74xx259s, setting the bit position
    SetDioBitPosition(DigitalOutput);

    // Set the value into the data output line
    if (State)
    {
		SetGpioBit(DOUT_OUT_DATA_BIT);
    }
    else
    {
		ClrGpioBit(DOUT_OUT_DATA_BIT);
    }

    WriteChipSelect(DIOChipSelect[Port]);

    // clock in the data
    ClrGpioBit(DOUT_DIO_GATE_BIT);
	SetGpioBit(DOUT_DIO_GATE_BIT);

    WriteChipSelect(CS_NULL);

    LeaveCriticalSection(&AioDioCriticalSection);                               // Allow access to the chip selects
}


//*****************************************************************************
//
//  AioDio_t::Read - Read the last loaded channel and at the same time
//          load the next channel to be read.
//
//*****************************************************************************

bool AioDio_t::ReadAnalogInput(DWORD AnalogInput, DWORD NextChannel, DWORD *Reading)
{
    bool ReadSuccess = false;

    if(BSPtype == BSP_E600_BD)
	{
#if BSP_E600_BD_P3_BUILD
		DWORD ReadData, WriteData;
		DWORD ReadCount;

		NextChannel = NextChannel % 8;
		WriteData = (WORD)(ADC_STANDARD_WRITE_SETTINGS | ADC_SETUP_CHANNEL_ADDRESS(NextChannel));

        // If input is on the manifold board
		if(AnalogInput >= 8)
		{
			OUTREG32(&pSPI3Channel0Regs->MCSPI_CHCONF, spi3_CH0_ADC_config.config);
			ReadCount = WriteReadSPI(pSPI3Channel0Regs, &ReadData, &WriteData, sizeof(DWORD), 16);
		}
		else
		{
			OUTREG32(&pSPI1Channel0Regs->MCSPI_CHCONF, spi1_CH0_ADC_config.config);
			ReadCount = WriteReadSPI(pSPI1Channel0Regs, &ReadData, &WriteData, sizeof(DWORD), 16);				
		}

		if(ReadCount != sizeof(ReadData))
		{
			RETAILMSG(0, (L"AIO - Analog Read Failed, ReadCount=%d\r\n", ReadCount));
		}
		else
		{
			*Reading = (ReadData & 0x0FFF);
			RETAILMSG(0, (L"AIO - Analog Read: In=%x, Out=%x, ReadCount=%d\r\n", ReadData, WriteData, ReadCount));
		}

		if ((ReadData & ANALOG_INPUT_ADDRESS_MASK) == (DWORD)ADC_IN_CHANNEL_ADDRESS(AnalogInput % 8))
		{
			RETAILMSG(0, (L"AIO - Correct ADC Channel Read, Cur:%d, Next:%d, Read:%d\r\n",
				AnalogInput, NextChannel, (ReadData & ANALOG_INPUT_ADDRESS_MASK) >> 12));
			ReadSuccess = TRUE;
		}
		else
		{
			RETAILMSG(0, (L"AIO - Wrong ADC Channel Read, Cur:%d, Next:%d, Read:%d\r\n",
				AnalogInput, NextChannel, (ReadData & ANALOG_INPUT_ADDRESS_MASK) >> 12));
		}
#else
		DWORD ReadData, WriteData;
		DWORD ReadCount;

		WriteData = ((AnalogInput % 8) << 29) | ADC_MAX1068_WRITE_COMMAND;

		// If input is on the manifold board
		if(AnalogInput >= NextChannel)
		{
			OUTREG32(&pSPI3Channel0Regs->MCSPI_CHCONF, spi3_CH0_ADC_config.config);
			ReadCount = WriteReadSPI(pSPI3Channel0Regs, &ReadData, &WriteData, sizeof(DWORD), 32);
		}
		else
		{
			OUTREG32(&pSPI1Channel0Regs->MCSPI_CHCONF, spi1_CH0_ADC_config.config);
			ReadCount = WriteReadSPI(pSPI1Channel0Regs, &ReadData, &WriteData, sizeof(DWORD), 32);				
		}

		if(ReadCount != sizeof(DWORD))
			DEBUGMSG(1, (L"AIO - Analog Read Failed\r\n"));
		else
		{
			*Reading = (ReadData & 0xFFFF) >> 2;
			ReadSuccess = true;
			DEBUGMSG(0, (L"AIO - Analog Read: In=%x, Out=%x\n", ReadData, WriteData));
		}
#endif
	}
	else
	{
		UINT16 InBuf, OutBuf;
	
		EnterCriticalSection(&AioDioCriticalSection);                       // protect access to the chip select lines

		OutBuf = (UINT16)(ADC_SETUP_CHANNEL_ADDRESS(NextChannel) | ADC_STANDARD_WRITE_SETTINGS);

		// Make sure the ADC chip select gets activated
		if (LastPortAccessed != CS_SPI_ADC)
		{
			// set the control registers for the DAC
			WriteChipSelect(CS_SPI_ADC);		// set the chip select to the DAC
		}

		//Configure the SPI Channel 0 for ADC
		OUTREG32(&pSPI3Channel0Regs->MCSPI_CHCONF, spi3_ADC_config.config);

		if (!WriteReadSPI(pSPI3Channel0Regs, &InBuf, &OutBuf, 2, 16))
			DEBUGMSG(1, (L"AIO - Analog Read Failed\r\n"));
		else
			DEBUGMSG(0, (L"AIO - Analog Read: In=%x, Out=%x\n", InBuf, OutBuf));
		
		*Reading = InBuf & ANALOG_INPUT_DATA_MASK;
		if ((InBuf & ANALOG_INPUT_ADDRESS_MASK) == (UINT16)ADC_IN_CHANNEL_ADDRESS(AnalogInput))
			ReadSuccess = TRUE;

		LeaveCriticalSection(&AioDioCriticalSection);                       // Allow access to the chip selects
	}
    return ReadSuccess;  
}

//*****************************************************************************
//
//  AioDio_t::ReadSensor - 
//
//*****************************************************************************
bool AioDio_t::ReadSensor(DWORD cs, DWORD *pReadData)
{
    DWORD WriteData = 0x5555;			//don't-care write data
	bool ReadSuccess = false;

	EnterCriticalSection(&AioDioCriticalSection);                       // protect access to the chip select lines

    // Make sure the ADC chip select gets activated
    if (LastPortAccessed != cs)
    {
        WriteChipSelect(cs);		// set the chip select to the ADC
    }

    //Configure the SPI Channel 0 for ADC
	OUTREG32(&pSPI3Channel0Regs->MCSPI_CHCONF, spi3_SENSOR_config.config);

	if (!WriteReadSPI(pSPI3Channel0Regs, pReadData, &WriteData, BYTES_TO_READ_SENSOR, READ_SENSOR_WORD_LENGTH))
		DEBUGMSG(1, (L"AIO - Read Sensor Failed\r\n"));
	
	if ((*pReadData & SENSOR_READ_STATUS_MASK) == SENSOR_STATUS_NORMAL)
		ReadSuccess = true;
	else
		DEBUGMSG(1, (L"AIO - Read Sensor Failed, Status = 0x%x\r\n", *pReadData & SENSOR_READ_STATUS_MASK));

    LeaveCriticalSection(&AioDioCriticalSection);                       // Allow access to the chip selects

    return ReadSuccess;  
}


//*****************************************************************************
//
//  AioDio_t::Write - Write the data to the specified analog device.
//      When the DAC_MOTOR is the device, we save the value, then write
//      both the DAC_MOTOR and the DAC_EXHALATION_VALVE together to the
//      LTC1448.
//
//*****************************************************************************

bool AioDio_t::WriteAnalogOutput(DWORD Value)
{
    bool Success = false;

	if(BSPtype == BSP_E600_BD)
	{
		OUTREG32(&pSPI1Channel3Regs->MCSPI_CHCONF, spi1_CH3_DAC_config.config);
		if (!WriteSPI(pSPI1Channel3Regs, &Value, sizeof(DWORD), 24))
			DEBUGMSG(1, (L"AIO - E600 BD DAC Write Failed\r\n"));
		else
		{
			DEBUGMSG(0, (L"AIO - E600 BD DAC Write = %x\r\n", Value));
			Success = true;
		}
	}    
	else
	{
		DWORD OutBuf;
		UINT16 uwOutBuf;

		EnterCriticalSection(&AioDioCriticalSection);                       // protect access to the chip select lines

		if((BSPtype == BSP_AURA_G) || (BSPtype == BSP_AURA_C))
		{
			if (LastPortAccessed != CS_SPI_DAC)
			{
				WriteChipSelect(CS_SPI_DAC);
			}
		
			//Configure the SPI Channel 0 for ADC
			OUTREG32(&pSPI3Channel0Regs->MCSPI_CHCONF, spi3_DAC_config_16bits.config);

			uwOutBuf = (UINT16)(Value);
			if (!WriteSPI(pSPI3Channel0Regs, &uwOutBuf, sizeof(UINT16), 16))
				DEBUGMSG(1, (L"AIO - AURA DAC Write Failed\r\n"));
			else
			{
				DEBUGMSG(0, (L"AIO - AURA DAC Write = %x\r\n", OutBuf));
				Success = true;
			}
		}
		else
		{
			if (LastPortAccessed != CS_SPI_DAC)
			{
				WriteChipSelect(CS_SPI_DAC);
			}
		
			OUTREG32(&pSPI3Channel0Regs->MCSPI_CHCONF, spi3_DAC_config.config);
			OutBuf = Value;
			if (!WriteSPI(pSPI3Channel0Regs, &OutBuf, sizeof(DWORD), 24))
				DEBUGMSG(1, (L"AIO - HT70 DAC Write Failed\r\n"));
			else
			{
				DEBUGMSG(0, (L"AIO - HT70 DAC Write = %x\r\n", OutBuf));
				Success = true;
			}
		}

		LeaveCriticalSection(&AioDioCriticalSection);                       // Allow access to the chip selects
	}
	return Success;
}


//*****************************************************************************
//
//  AioDio_t::Write - Set the chip select lines
//
//*****************************************************************************
#if 0
void AioDio_t::WriteChipSelect(DWORD CsSetting)
{
    DWORD CsSetValue = 0, CsClearValue = 0;

	switch (CsSetting)
	{
		case CS_NULL:		// None
			CsClearValue = DOUT_CS_0_BIT_MASK | DOUT_CS_1_BIT_MASK | DOUT_CS_2_BIT_MASK; // Clear all
			break;

		case CS_SPI_DAC:		// (Y1)
			CsSetValue = DOUT_CS_0_BIT_MASK;                               // Set
			CsClearValue = DOUT_CS_1_BIT_MASK | DOUT_CS_2_BIT_MASK;          // Clear
			break;

		case CS_SPI_ADC:		// (Y2)
			CsClearValue = DOUT_CS_0_BIT_MASK | DOUT_CS_2_BIT_MASK;          // Clear
			CsSetValue = DOUT_CS_1_BIT_MASK;                               // Set
			break;

		case CS_DIGITAL_PORT_0:		// (Y3)
			CsClearValue = DOUT_CS_2_BIT_MASK;                               // Clear
			CsSetValue = DOUT_CS_0_BIT_MASK | DOUT_CS_1_BIT_MASK;          // Set
			break;

		case CS_DIGITAL_PORT_1:		// (Y4)
			CsSetValue = DOUT_CS_2_BIT_MASK;                               // Set
			CsClearValue = DOUT_CS_1_BIT_MASK | DOUT_CS_0_BIT_MASK;          // Clear
			break;

		case CS_DIGITAL_PORT_2:		// (Y5)
			CsSetValue = DOUT_CS_0_BIT_MASK | DOUT_CS_2_BIT_MASK;          // Set
			CsClearValue = DOUT_CS_1_BIT_MASK;                               // Clear
			break;

		case CS_DIGITAL_PORT_3:		// (Y6)
			CsClearValue = DOUT_CS_0_BIT_MASK;                               // Clear
			CsSetValue = DOUT_CS_1_BIT_MASK | DOUT_CS_2_BIT_MASK;          // Set
			break;

		case CS_DIGITAL_PORT_4:		// (Y7)
			CsSetValue = DOUT_CS_0_BIT_MASK | DOUT_CS_1_BIT_MASK | DOUT_CS_2_BIT_MASK; // Set
			break;
	}
	if(CsSetValue != 0)
		SetGpioMultiBits(DOUT_CS_0_BIT, CsSetValue);
	if(CsClearValue != 0)
		ClrGpioMultiBits(DOUT_CS_0_BIT, CsClearValue);
	
	LastPortAccessed = CsSetting;
}
#endif

void AioDio_t::WriteChipSelect(DWORD CsSetting)
{
    DWORD CsSetValue, CsClearValue;
#define CH8_CS_SETTING	8

	CsClearValue = CS_Table[CsSetting][CS_CLEAR_INDEX];
	CsSetValue = CS_Table[CsSetting][CS_SET_INDEX];

    if(CsSetValue != 0)
		SetGpioMultiBits(DOUT_CS_0_BIT, CsSetValue);
	if(CsClearValue != 0)
		ClrGpioMultiBits(DOUT_CS_0_BIT, CsClearValue);

	if(CsSetting >= CH8_CS_SETTING)
		SetGpioBit(DOUT_CS_3_BIT);
	else
		ClrGpioBit(DOUT_CS_3_BIT);
	
	LastPortAccessed = CsSetting;
}

//*****************************************************************************
//
//  AioDio_t::Write - This version of the write function writes one GPIO,
//      of the dual direction GPIOs.
//
//*****************************************************************************

void AioDio_t::WriteDigitalIO(DWORD DigitalOutput, DWORD State)
{
	if(State)
		SetGpioBit(DigitalOutput);
	else
		ClrGpioBit(DigitalOutput);
}


//*****************************************************************************
//
//  AioDio_t::Read - This version of the read function reads one GPIO.
//
//*****************************************************************************

UINT32 AioDio_t::ReadDigitalIO(DWORD DigitalInput)
{    
	UINT32 InputState;
	
	InputState = GetGpioBit(DigitalInput);

	return InputState;
}


//*****************************************************************************
//
//  AioDio_t::SetDirection - Set a GPIO pin's direction.
//
//*****************************************************************************

void AioDio_t::SetDirection(DWORD DigitalInputOutput, DWORD Direction)
{
	switch (Direction)
    {
        case GPIO_DIR_INPUT:
			GPIOSetMode(hGIOPort, DigitalInputOutput, GPIO_DIR_INPUT);
			break;

        case GPIO_DIR_OUTPUT:
			GPIOSetMode(hGIOPort, DigitalInputOutput, GPIO_DIR_OUTPUT);
            break;

		default:
			break;
    }
}

//*****************************************************************************
//
//  AioDio_t::SetBrightness 
//
//*****************************************************************************
void AioDio_t::SetBrightness(DWORD data)
{
	EnableBacklight();
		
	if((BSPtype == BSP_AURA_G) || (BSPtype == BSP_AURA_C))
	{
		if(!TLEDSetDutyCycle(hTled, data))
			DEBUGMSG(0, (L"AIO - Failed Setting Duty Cycle\r\n"));
	}
	else
	{
		SPIWrite(hSPI3_CH1, sizeof(UINT16), &data);
	}
}

//*****************************************************************************
//
//  AioDio_t::EnableBacklight 
//
//*****************************************************************************
void AioDio_t::EnableBacklight()
{
	if((BSPtype == BSP_AURA_G) || (BSPtype == BSP_AURA_C))
		GPIOClrBit(hGIOPort, DOUT_TPS_BL_ENA_BIT);	// Use GPIO func since pin is on the TPS
	else
		SetGpioBit(DOUT_BL_ENA_BIT);
}

//*****************************************************************************
//
//  AioDio_t::DisableBacklight 
//
//*****************************************************************************
void AioDio_t::DisableBacklight()
{
	if((BSPtype == BSP_AURA_G) || (BSPtype == BSP_AURA_C))
		GPIOSetBit(hGIOPort, DOUT_TPS_BL_ENA_BIT);	// Use GPIO func since pin is on the TPS
	else
		ClrGpioBit(DOUT_BL_ENA_BIT);
}

//*****************************************************************************
//
//  AioDio_t::DeinitializeAioDio 
//
//*****************************************************************************
void AioDio_t::DeinitializeAioDio()
{
	RETAILMSG(0, (L"AIODIO: DeinitializeAioDio()+\r\n"));
	// Close SPI device
    if (hSPI3_CH0 != NULL)
    {
        SPIClose(hSPI3_CH0);
        hSPI3_CH0 = NULL;
    }

	// Close SPI device
    if (hSPI3_CH1 != NULL)
    {
        SPIClose(hSPI3_CH1);
        hSPI3_CH1 = NULL;
    }
    
#if AIODIO_TESTING
	if(hI2CPort)
	{
        I2CClose(hI2CPort);
        hI2CPort = NULL;
    }
#endif

	// Close GPIO device
    if (hGIOPort != NULL)
    {
        GPIOClose(hGIOPort);
        hGIOPort = NULL;
    }

	if(hTled)
	{
		TLEDClose(hTled);
		hTled = NULL;
	}

	if(bEncoderInitialized)
		DeinitEncoderGPIO();
}

//*****************************************************************************
//
//  AioDio_t::InitAioDioE600BD - Get the ADC powered up and ready to go.
//      Initialize the digital inputs and outputs. This is all done once
//      a thread is up and running.
//
//*****************************************************************************
BOOL AioDio_t::InitializeAioDioE600BD()
{
	OUTREG32(&pSPI1Channel0Regs->MCSPI_CHCONF, spi1_CH0_ADC_config.config);
	OUTREG32(&pSPI1Channel3Regs->MCSPI_CHCONF, spi1_CH3_DAC_config.config);	
	OUTREG32(&pSPI3Channel0Regs->MCSPI_CHCONF, spi3_CH0_ADC_config.config);
	OUTREG32(&pSPI3Channel1Regs->MCSPI_CHCONF, spi3_CH1_ADC_config.config);
	return TRUE;
}


DWORD EncIntrThread(LPVOID lpParam)
{
	DWORD CurrDir;
	UINT bit = GPIO_BIT(ENC_DIR_GPIO_BIT);
	UINT bank = GPIO_BANK(ENC_DIR_GPIO_BIT);
	UINT32 *p = (UINT32 *)&(ppGpioRegs[bank]->DATAIN);
	static UINT8 count = 0;
	static BOOL DirChanged = FALSE;

	UNREFERENCED_PARAMETER(lpParam);
	for(;;)
	{
		// Wait for event
		if (WaitForSingleObject(hEncIntrEvent, INFINITE) == WAIT_OBJECT_0)
		{
			EnterCriticalSection(&EncoderCriticalSection);
			CurrDir = (INREG32(p) >> (bit)) & 0x01;
			if(CurrDir == dwKnobDir)
			{
				if(++count >= NUM_ENCODER_EDGES_PER_COUNT)
				{
					dwKnobCount++;
					count = 0;
				}
			}
			else
			{
				// if direction changed for the first time,
				// keep track of it but don't take any action yet
				if(!DirChanged)
				{
					DirChanged = TRUE;
				}
				else	// at the 2nd time, confirm direction change and set count
				{
					dwKnobCount = 1;
					count = 0;
					dwKnobDir = CurrDir;
					DirChanged = FALSE;		// reinitialize direction change flag
				}
			}

			GPIOInterruptDone(hGIO, ENC_PHASEA_GPIO_BIT, dwEncSysIntr);

			LeaveCriticalSection(&EncoderCriticalSection);
			//RETAILMSG(1, (L"EncA: GotI, Cnt=%d, Dir=%d\r\n", dwKnobCount, dwKnobDir));
		}
	}
}

BOOL GetEncoderValues(UINT8 *pOutBuffer)
{
	EnterCriticalSection(&EncoderCriticalSection);
	*(DWORD *)pOutBuffer = dwKnobCount;
	dwKnobCount = 0;
	*(DWORD *)(pOutBuffer + sizeof(DWORD)) = dwKnobDir;
	LeaveCriticalSection(&EncoderCriticalSection);
	return TRUE;
}


VOID DeinitEncoderGPIO()
{
	bEncoderInitialized = FALSE;
	// Release SYSINTR
    if (dwEncSysIntr != SYSINTR_UNDEFINED)
    {
        KernelIoControl(IOCTL_HAL_RELEASE_SYSINTR, &dwEncSysIntr, sizeof(DWORD), NULL, 0, NULL);
        InterruptDisable(dwEncSysIntr);
        dwEncSysIntr = (DWORD) SYSINTR_UNDEFINED;
    }
	
	if(hGIO)
		CloseHandle(hGIO);
	if(hEncIntrEvent)
		CloseHandle(hEncIntrEvent);
	if(hEncIntrThread)
		SuspendThread(hEncIntrThread);
}


#define ENC_DEBUG_MSG	0

BOOL InitEncoderGPIO()
{
	BOOL rc = FALSE;

	if(bEncoderInitialized)
		return TRUE;

	InitializeCriticalSection(&EncoderCriticalSection);

	hGIO = GPIOOpen();
	if (hGIO == NULL) 
	{
		RETAILMSG(1, (L"ERROR: Failed open GPIO device driver for Encoder read - %d\r\n", GetLastError()));
		goto cleanup;
	}

	GPIOSetMode(hGIO, ENC_DIR_GPIO_BIT, GPIO_DIR_INPUT);
	GPIOSetMode(hGIO, ENC_PHASEA_GPIO_BIT, GPIO_DIR_INPUT|GPIO_INT_HIGH_LOW|GPIO_DEBOUNCE_ENABLE);
	dwEncGioIntr = GPIOGetSystemIrq(hGIO, ENC_PHASEA_GPIO_BIT);
	RETAILMSG (ENC_DEBUG_MSG, (L"AIODIO: GPIOIntr=0x%x\r\n", dwEncGioIntr));

	// Set debounce time on GPIO
	IOCTL_GPIO_SET_DEBOUNCE_TIME_IN debounce;
    
	debounce.gpioId = ENC_PHASEA_GPIO_BIT;
	debounce.debounceTime = ENC_DEBOUNCE_TIME;
	GPIOIoControl(hGIO, ENC_PHASEA_GPIO_BIT,
				  IOCTL_GPIO_SET_DEBOUNCE_TIME, (UCHAR*)&debounce, sizeof(debounce), NULL, 0, NULL);

	hEncIntrEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
	if(hEncIntrEvent)
		RETAILMSG (ENC_DEBUG_MSG, (L"AIODIO: Enc Event created sucessfully - IntrEvent=0x%x\r\n", hEncIntrEvent));
	else
	{
		RETAILMSG (1, (L"ERROR: Failed creating Enc Event - %d\r\n", GetLastError()));
		goto cleanup;
	}
#if 0	
	if(GPIOInterruptInitialize(hGIO, ENC_PHASEA_GPIO_BIT, &dwEncSysIntr, hEncIntrEvent))
		RETAILMSG (1, (L"AIODIO: Enc Int Inited sucessfully - SysIntr=0x%x\r\n", dwEncSysIntr));
	else
	{
		RETAILMSG (1, (L"ERROR: Failed Init Enc Int\r\n"));
		goto cleanup;
	}
#else
	// Translate IRQ to SYSINTR
    if (KernelIoControl(IOCTL_HAL_REQUEST_SYSINTR, &dwEncGioIntr, sizeof(dwEncGioIntr),
        &dwEncSysIntr, sizeof(dwEncSysIntr), NULL))
		RETAILMSG (ENC_DEBUG_MSG, (L"AIODIO: Enc Int translated sucessfully - SysIntr=0x%x\r\n", dwEncSysIntr));
	else
    {      
		RETAILMSG (1, (L"AIODIO: Failed translating Enc Int\r\n"));
		goto cleanup;
    }
    
    if(InterruptInitialize(dwEncSysIntr, hEncIntrEvent, NULL, 0))
		RETAILMSG (ENC_DEBUG_MSG, (L"AIODIO: Enc Int Inited sucessfully\r\n"));
	else
    {
        RETAILMSG (1, (L"ERROR: Failed Init Enc Int\r\n"));
		goto cleanup;
    }
	
#endif

	hEncIntrThread = CreateThread(NULL, 0, EncIntrThread, NULL, 0, NULL);	// run immediately
	if (hEncIntrThread)
		RETAILMSG (ENC_DEBUG_MSG, (L"AIODIO: Creating Enc PhaseA Int thread successfully - Thread=0x%x\r\n", hEncIntrThread));
	else
	{
		RETAILMSG (1, (L"ERROR: Failed creating Enc PhaseA Int thread - %d\r\n", GetLastError()));
		goto cleanup;
	}

	// Set thread priority
	CeSetThreadPriority(hEncIntrThread, 13);
	
	bEncoderInitialized = TRUE;
	rc = TRUE;
	return rc;

cleanup:
	DeinitEncoderGPIO();
	return rc;
}

#if BSP_E600_UI_P2_BUILD

#define BUTTON_ON_IRQ_EVENT_NAME	TEXT("BUTTON_ON_IRQ_EVENT")
#define BUTTON_OFF_IRQ_EVENT_NAME	TEXT("BUTTON_OFF_IRQ_EVENT")


DWORD ButtonIntrThread(LPVOID lpParam)
{
	UNREFERENCED_PARAMETER(lpParam);
	for(;;)
	{
		// Wait for event
		if (WaitForSingleObject(hButtonIntrEvent, INFINITE) == WAIT_OBJECT_0)
		{
			EnterCriticalSection(&EncoderCriticalSection);
			

			GPIOInterruptDone(hGIO, CONSOLE_BUTTON_IRQ_GPIO_BIT, dwEncSysIntr);

			LeaveCriticalSection(&ButtonCriticalSection);
			//RETAILMSG(1, (L"EncA: Got Interrupt, Count=%d, Dir=%d\r\n", dwKnobCount, dwKnobDir));
		}
	}
}


BOOL InitButtonIRQ()
{
	BOOL rc = FALSE;
	
	InitializeCriticalSection(&ButtonCriticalSection);

	GPIOSetMode(hGIO, CONSOLE_BUTTON_IRQ_GPIO_BIT, GPIO_DIR_INPUT|GPIO_INT_HIGH_LOW|GPIO_DEBOUNCE_ENABLE);
	dwButtonSysIntr = GPIOGetSystemIrq(hGIO, CONSOLE_BUTTON_IRQ_GPIO_BIT);

	// Set debounce time on GPIO
	IOCTL_GPIO_SET_DEBOUNCE_TIME_IN debounce;
    
	debounce.gpioId = CONSOLE_BUTTON_IRQ_GPIO_BIT;
	debounce.debounceTime = 10;
	GPIOIoControl(hGIO, CONSOLE_BUTTON_IRQ_GPIO_BIT,
				  IOCTL_GPIO_SET_DEBOUNCE_TIME, (UCHAR*)&debounce, sizeof(debounce), NULL, 0, NULL);

	hButtonIntrEvent = CreateEvent(NULL, FALSE, FALSE, BUTTON_ON_IRQ_EVENT_NAME);
	GPIOInterruptInitialize(hGIO, CONSOLE_BUTTON_IRQ_GPIO_BIT, &dwButtonSysIntr, hButtonIntrEvent);

	hButtonIntrThread = CreateThread(NULL, 0, ButtonIntrThread, NULL, 0, NULL);	// run immediately
	if (hButtonIntrThread == NULL)
	{
		RETAILMSG (1, (L"ERROR: Failed create Button interrupt thread\r\n"));
		goto cleanup;
	}
	else
		RETAILMSG (1, (L"AIODIO: Creating Button interrupt thread successfully\r\n"));
	// Set thread priority
	CeSetThreadPriority(hButtonIntrThread, 99);
	
	rc = TRUE;

cleanup:
	return rc;
}

#endif


//*****************************************************************************
//
//  AioDio_t::SetCodecGain - Set speaker gain.
//
//*****************************************************************************
bool AioDio_t::SetCodecGain(DWORD Value)
{
      
      UINT8 regWriteValLeft;
	  UINT8 regWriteValRight;
	  UINT8 regReadValLeft;
	  UINT8 regReadValRight;
            
      HANDLE hTritonDriver = TWLOpen();
      if(hTritonDriver)
      {
            regWriteValLeft = (PREDL_OUTLOW_EN | PREDL_GAIN((UINT8)Value) | PREDL_AL2_EN);
            TWLWriteRegs(hTritonDriver, TWL_PREDL_CTL, &regWriteValLeft, sizeof(UINT8));
			regWriteValRight = (PREDR_OUTLOW_EN | PREDR_GAIN(1) | PREDR_AR2_EN);
			TWLWriteRegs(hTritonDriver, TWL_PREDR_CTL, &regWriteValRight, sizeof(UINT8));

			TWLReadRegs(hTritonDriver, TWL_PREDL_CTL, &regReadValLeft, sizeof(UINT8));
			TWLReadRegs(hTritonDriver, TWL_PREDR_CTL, &regReadValRight, sizeof(UINT8));
			CloseHandle(hTritonDriver);

			RETAILMSG (1, (L"AIODIO(SetCodecGain): WriteValL=%d, ReadValL=%d, WriteValR=%d, ReadValR=%d\r\n", 
				regWriteValLeft, regReadValLeft, regWriteValRight, regReadValRight));
			return TRUE;
      }
      return FALSE;
}


//*****************************************************************************
//
//  AioDio_t::InitAioDio - Get the ADC powered up and ready to go.
//      Initialize the digital inputs and outputs. This is all done once
//      a thread is up and running.
//
//*****************************************************************************

BOOL AioDio_t::InitializeAioDio()
{
	DWORD InData, OutData;
	BOOL   rc = FALSE;
	
#if AIODIO_TESTING
	hI2CPort = I2COpen(OMAP_DEVICE_I2C2);
	if (hI2CPort == NULL) 
    {
		RETAILMSG(1, (L"ERROR: Failed open I2C port 2\r\n"));
		goto cleanup;
    }
#endif

	if(!hGIOPort)
	{
		hGIOPort = GPIOOpen();
		if (hGIOPort == NULL) 
		{
			RETAILMSG(1, (L"ERROR: Failed open GPIO device driver\r\n"));
			goto cleanup;
		}
	}

	if(BSPtype == BSP_E600_BD)
	{
		rc = InitializeAioDioE600BD();
		return rc;
	}
	else if (BSPtype == BSP_E600_UI)
	{
		rc = InitEncoderGPIO();
#if 0//++++BSP_E600_UI_P2_BUILD		
		if(rc)
			rc = InitButtonIRQ();
#endif
		return rc;
	}

	if(!hSPI3_CH0)
	{
		hSPI3_CH0 = SPIOpen(L"SPI3:");
		if (hSPI3_CH0 == NULL) 
		{
			RETAILMSG(1, (L"ERROR: Failed open SPI3 CH0 device driver\r\n"));
			goto cleanup;
		}
	}
	
	if(!hSPI3_CH1)
	{
		hSPI3_CH1 = SPIOpen(L"SPI3:");
		if (hSPI3_CH1 == NULL) 
		{
			RETAILMSG(1, (L"ERROR: Failed open SPI3 CH1 device driver\r\n"));
			goto cleanup;
		}
	}

	InitializeCriticalSection(&AioDioCriticalSection);
	
	//*****************************************************************************
    // Setup the GPIO ports
    //*****************************************************************************

    // Init each output port invloved with the Ex DIO expansion
	// Set the direction bits to output
	GPIOSetMode(hGIOPort, DOUT_DIO_A0_BIT, GPIO_DIR_OUTPUT);
	GPIOSetMode(hGIOPort, DOUT_DIO_A1_BIT, GPIO_DIR_OUTPUT);
	GPIOSetMode(hGIOPort, DOUT_DIO_A2_BIT, GPIO_DIR_OUTPUT);
    GPIOSetMode(hGIOPort, DOUT_DIO_GATE_BIT, GPIO_DIR_OUTPUT);
	GPIOSetMode(hGIOPort, DOUT_CS_0_BIT, GPIO_DIR_OUTPUT);
	GPIOSetMode(hGIOPort, DOUT_CS_1_BIT, GPIO_DIR_OUTPUT);
	GPIOSetMode(hGIOPort, DOUT_CS_2_BIT, GPIO_DIR_OUTPUT);
	GPIOSetMode(hGIOPort, DOUT_OUT_DATA_BIT, GPIO_DIR_OUTPUT);
	GPIOSetMode(hGIOPort, DOUT_BL_CS_BIT, GPIO_DIR_OUTPUT);
	GPIOSetMode(hGIOPort, DOUT_BL_ENA_BIT, GPIO_DIR_OUTPUT);

    // Set the output drivers low
    ClrGpioMultiBits(DOUT_DIO_A0_BIT, DOUT_DIO_A0_BIT_MASK | DOUT_DIO_A1_BIT_MASK | DOUT_DIO_A2_BIT_MASK);
    ClrGpioMultiBits(DOUT_CS_0_BIT, DOUT_DIO_GATE_BIT_MASK | DOUT_CS_0_BIT_MASK |
              DOUT_CS_1_BIT_MASK | DOUT_CS_2_BIT_MASK | DOUT_OUT_DATA_BIT_MASK);

    GPIOSetBit(hGIOPort, DOUT_DIO_GATE_BIT);                                           // Turn on gate for default outputs

    // Init the input port invloved with the Ex DIO expansion
    GPIOSetMode(hGIOPort, DIN_IN_DATA_BIT, GPIO_DIR_INPUT);


	if (LastPortAccessed != CS_SPI_ADC)
    {
        WriteChipSelect(CS_SPI_ADC);                                              // set the chip select to the DAC
    }
	
	//Configure the SPI Channel 0 for ADC
	if (!SPIConfigure(hSPI3_CH0, spi3_ADC_config.address, spi3_ADC_config.config))
	{
		RETAILMSG(1, (L"ERROR: Failed to configure SPI3 device driver Channel 0\r\n"));
		goto cleanup;
    }

	//Configure the SPI Channel 1 for backlight brightness
	if (!SPIConfigure(hSPI3_CH1, spi3_BL_config.address, spi3_BL_config.config))
	{
		RETAILMSG(1, (L"ERROR: Failed to configure SPI3 device driver Channel 1\r\n"));
		goto cleanup;
    }

	OutData = (UINT16)(ADC_SETUP_CHANNEL_ADDRESS(0) | ADC_STANDARD_WRITE_SETTINGS);
	
	// Do 2 dummy conversions
	if (!SPIWriteRead(hSPI3_CH0, sizeof(UINT16), &OutData, &InData))
	{
        RETAILMSG(1, (L"ERROR: Failed to do first dummy conversion\r\n"));
        goto cleanup;
    }
	
	if (!SPIWriteRead(hSPI3_CH0, sizeof(UINT16), &OutData, &InData))
	{
        RETAILMSG(1, (L"ERROR: Failed to do second dummy conversion\r\n"));
        goto cleanup;
    }
	
	if((BSPtype == BSP_AURA_G) || (BSPtype == BSP_AURA_C))
	{
		GPIOSetMode(hGIOPort, DOUT_CS_3_BIT, GPIO_DIR_OUTPUT);
		GPIOClrBit(hGIOPort, DOUT_CS_3_BIT_MASK);

		hTled = TLEDOpen();
		if (hTled == NULL)
			{
			RETAILMSG(1, (L"AIO ERROR: Failed allocate TLED handle\r\n"));
			goto cleanup;
			}

		if(!TLEDSetChannel(hTled, 0))
		{
			RETAILMSG(1, (L"AIO ERROR: Failed setting LED Channel\r\n"));
			goto cleanup;
		}

		GPIOSetMode(hGIOPort, DOUT_TPS_BL_ENA_BIT,GPIO_DIR_OUTPUT);
		SetBrightness(100);
	}
	
	rc = TRUE;

cleanup:
	if(rc == FALSE)
	{
		DeinitializeAioDio();
		RETAILMSG(1, (L"ERROR: Failed intializing AioDio\r\n"));
	}

	return rc;
}

//------------------------------------------------------------------------------
//
//  Function:  AIO_IOControl
//
//  This function sends a command to a device.
//
BOOL
AIO_IOControl(
    DWORD context,
    DWORD code,
    UCHAR *pInBuffer,
    DWORD inSize,
    UCHAR *pOutBuffer,
    DWORD outSize,
    DWORD *pOutSize
    )
{
    BOOL rc = FALSE;
    DWORD inValue, outValue, InBuf[16], OutBuf[16];
	DWORD *pOutData;
	DWORD *pInData;
	NMI_VERSION version;
	DWORD NumChannels;
	int Index;
	DWORD DataSize;

#if AIODIO_TESTING
	UCHAR pchInBuf[256], pchOutBuf[256];
#endif	

	DEBUGMSG(0,
        (L"+AIO_IOControl(0x%08x, 0x%08x, 0x%08x, %d, 0x%08x, %d, 0x%08x)\r\n",
        context, code, pInBuffer, inSize, pOutBuffer, outSize, pOutSize));

    // Check if we get correct context
    if (!context)
        {
        DEBUGMSG(1, (L"ERROR: AIO_IOControl: "
            L"Incorrect context parameter\r\n"
            ));
        goto cleanUp;
        }

    switch (code)
        {
        case IOCTL_AIODIO_READ_COUNTER_INPUT:
			if (pOutSize != 0) *pOutSize = sizeof(DWORD);
			if ((pInBuffer == NULL) || (pOutBuffer == NULL) || 
				(inSize < READ_COUNTER_INPUT_PARAMS * sizeof(DWORD)) ||
                (outSize < sizeof(DWORD)) 
				)
				{
                SetLastError(ERROR_INVALID_PARAMETER);
                break;
                }
			pInData = (DWORD *)pInBuffer;
			*((DWORD *)pOutBuffer) = pAioDio->ReadCounterInput(pInData[0], pInData[1], pInData[2]);
            rc = TRUE;
            break;

		case IOCTL_AIODIO_READ_DIGITAL_INPUT:						
			if (pOutSize != 0) *pOutSize = sizeof(DWORD);
            if ((pInBuffer == NULL) || (pOutBuffer == NULL) || 
				(inSize < READ_DIGITAL_INPUT_PARAMS * sizeof(DWORD)) ||
                (outSize < sizeof(DWORD)) 
				)
                {
                SetLastError(ERROR_INVALID_PARAMETER);
                break;
                }
			pInData = (DWORD *)pInBuffer;
			*((DWORD *)pOutBuffer) = pAioDio->ReadDigitalInput(pInData[0], pInData[1]);
            rc = TRUE;
            break;

		case IOCTL_AIODIO_WRITE_DIGITAL_OUTPUT:						
			if (pOutSize != 0) *pOutSize = 0;
			if ((pInBuffer == NULL) || (inSize < WRITE_DIGITAL_OUTPUT_PARAMS * sizeof(DWORD)) 
				)
                {
                SetLastError(ERROR_INVALID_PARAMETER);
                break;
                }
			pInData = (DWORD *)pInBuffer;
			pAioDio->WriteDigitalOutput(pInData[0], pInData[1], pInData[2]);
            rc = TRUE;
            break;

        case IOCTL_AIODIO_READ_ANALOG_CHANNEL:
            if (pOutSize != 0) *pOutSize = sizeof(DWORD);					// return size
            if ((pInBuffer == NULL) || (pOutBuffer == NULL) || 
				(inSize < READ_ANALOG_CHANNEL_PARAMS * sizeof(DWORD)) ||
                (outSize < sizeof(DWORD)) 
				)
                {
                SetLastError(ERROR_INVALID_PARAMETER);
                break;
                }
			pInData = (DWORD *)pInBuffer;
			rc = pAioDio->ReadAnalogInput(pInData[0], pInData[1], (DWORD *)pOutBuffer);
            break;

		case IOCTL_AIODIO_READ_MULTIPLE_ANALOG_CHANNELS:
            if ((pInBuffer == NULL) || (pOutBuffer == NULL) || 
				(inSize < READ_MULTIPLE_ANALOG_CHANNELS_PARAMS * sizeof(DWORD)))
                {
                SetLastError(ERROR_INVALID_PARAMETER);
                break;
                }
			
			pInData = (DWORD *)pInBuffer;
			NumChannels = pInData[1] - pInData[0] + 1;
			DataSize = NumChannels * 2 * sizeof(DWORD);
			
			if (outSize < DataSize)
				break;

			if (pOutSize != 0)
				*pOutSize = DataSize;	// return size
            
            pOutData = (DWORD *)pOutBuffer;
			Index = 0;
			for(DWORD Channel = pInData[0]; Channel <= pInData[1]; Channel++)
			{
#if BSP_E600_BD_P3_BUILD
				if(pAioDio->ReadAnalogInput(Channel, Channel+1, &outValue))
#else
				if(pAioDio->ReadAnalogInput(Channel, pInData[2], &outValue))
#endif
				{
					*pOutData++ = TRUE;
					*pOutData++ = outValue;
				}
				else
				{
					*pOutData++ = FALSE;
					*pOutData++ = 0;
				}
			}
			rc = TRUE;
            
			break;

        case IOCTL_AIODIO_WRITE_DAC_MOTOR:
            if (pOutSize != 0) *pOutSize = 0;
            if ((pInBuffer == NULL) || (inSize < WRITE_DAC_MOTOR_PARAMS * sizeof(DWORD)) 
				)
                {
                SetLastError(ERROR_INVALID_PARAMETER);
                break;
                }
			pInData = (DWORD *)pInBuffer;
			rc = pAioDio->WriteAnalogOutput(pInData[0]);
            break;

        case IOCTL_AIODIO_INIT_HARDWARE:
            if (pOutSize != 0) *pOutSize = 0;            
			if(pAioDio)
			{
				rc = pAioDio->InitializeAioDio();
			}
            break;

		case IOCTL_AIODIO_READ_BUTTONS:						
			if ((pOutBuffer == NULL) || (outSize > sizeof(OutBuf)) || 
				(inSize < READ_BUTTONS_PARAMS * sizeof(DWORD))
				)
                {
                SetLastError(ERROR_INVALID_PARAMETER);
                break;
                }

			pInData = (DWORD *)pInBuffer;

#if BSP_AURA_PROTOTYPE			
			if((BSPtype == BSP_AURA_G) || (BSPtype == BSP_AURA_C))
			{
				if(ReadKeypad(BOTH_KEYPADS, OutBuf))
					if(CeSafeCopyMemory(pOutBuffer, OutBuf, InBuf[1] * sizeof(DWORD)))
						rc = TRUE;
			}
			else
#endif
			{
				pAioDio->ReadButtons(pInData[0], pInData[1], pInData[2], (DWORD *)pOutBuffer);
				rc = TRUE;
			}
            break;

#if BSP_AURA_PROTOTYPE

		case IOCTL_AIODIO_READ_KEYPAD:						
			if ((pOutBuffer == NULL) || (outSize > sizeof(OutBuf)) || 
				(inSize < sizeof(DWORD)) ||
                !CeSafeCopyMemory(InBuf, pInBuffer, sizeof(DWORD)))
                {
                SetLastError(ERROR_INVALID_PARAMETER);
                break;
                }
            if(ReadKeypad(InBuf[0], OutBuf))
			{
				if (!CeSafeCopyMemory(pOutBuffer, OutBuf, sizeof(DWORD)))
					{
					SetLastError(ERROR_INVALID_PARAMETER);
					break;
					}
				rc = TRUE;
			}
            break;
#endif

		case IOCTL_AIODIO_SET_DIGITAL_IO_DIR:						
			if ((pInBuffer == NULL) || (inSize < SET_DIGITAL_IO_DIR_PARAMS * sizeof(DWORD)) 
				)
                {
                SetLastError(ERROR_INVALID_PARAMETER);
                break;
                }
			pInData = (DWORD *)pInBuffer;
			pAioDio->SetDirection(pInData[0], pInData[1]);
			rc = TRUE;
            break;

		case IOCTL_AIODIO_READ_DIGITAL_IO:						
			if (pOutSize != 0) *pOutSize = sizeof(DWORD);
            if ((pInBuffer == NULL) || (pOutBuffer == NULL) || 
				(inSize < READ_DIGITAL_IO_PARAMS * sizeof(DWORD)) ||
                (outSize < sizeof(DWORD)) 
				)
                {
                SetLastError(ERROR_INVALID_PARAMETER);
                break;
                }
			inValue = *((DWORD *)pInBuffer);
			*((DWORD *)pOutBuffer) = pAioDio->ReadDigitalIO(inValue);
            rc = TRUE;
            break;

		case IOCTL_AIODIO_WRITE_DIGITAL_IO:						
			if (pOutSize != 0) *pOutSize = 0;
			if ((pInBuffer == NULL) || (inSize < WRITE_DIGITAL_IO_PARAMS * sizeof(DWORD))
				)
                {
                SetLastError(ERROR_INVALID_PARAMETER);
                break;
                }
            pInData = (DWORD *)pInBuffer;
			pAioDio->WriteDigitalIO(pInData[0], pInData[1]);
            rc = TRUE;
            break;

		case IOCTL_AIODIO_SET_BRIGHTNESS:
			if ((pInBuffer == NULL) || (inSize < sizeof(DWORD)) 
				)
                {
                SetLastError(ERROR_INVALID_PARAMETER);
                break;
                }
			pAioDio->SetBrightness(*((DWORD *)pInBuffer));
			rc = TRUE;
            break;

		case IOCTL_AIODIO_ENA_BACKLIGHT:
			pAioDio->EnableBacklight();
			rc = TRUE;
			break;

		case IOCTL_AIODIO_DIS_BACKLIGHT:
			pAioDio->DisableBacklight();
			rc = TRUE;
			break;

		case IOCTL_AIODIO_GET_NMI_EBOOT_VER:
			if ((pOutBuffer == NULL) || (outSize < sizeof(NMI_VERSION)))
                {
                SetLastError(ERROR_INVALID_PARAMETER);
                break;
                }
			if((!KernelIoControl(IOCTL_HAL_GET_NMI_EBOOT_VER, NULL, 0, &version, sizeof(NMI_VERSION), NULL)) ||
				(!CeSafeCopyMemory(pOutBuffer, &version, sizeof(NMI_VERSION))))
                {
                SetLastError(ERROR_INVALID_PARAMETER);
                break;
                }
			rc = TRUE;
			break;

		case IOCTL_AIODIO_GET_NMI_OS_VER:
			if ((pOutBuffer == NULL) || (outSize < sizeof(NMI_VERSION)))
                {
                SetLastError(ERROR_INVALID_PARAMETER);
                break;
                }
            GetOsVersion(&version);
			if (!CeSafeCopyMemory(pOutBuffer, &version, sizeof(NMI_VERSION)))
                {
                SetLastError(ERROR_INVALID_PARAMETER);
                break;
                }
			rc = TRUE;
			break;

		case IOCTL_AIODIO_DEINIT:
			pAioDio->DeinitializeAioDio();
			rc = TRUE;
			break;

		case IOCTL_AIODIO_GET_BUTTON_FLAGS:
			if ((pOutBuffer == NULL) || (outSize < sizeof(DWORD)))
                {
                SetLastError(ERROR_INVALID_PARAMETER);
                break;
                }
			if(KernelIoControl(IOCTL_HAL_GET_BUTTON_FLAGS, NULL, 0, &outValue, sizeof(DWORD), NULL))
			{
				if (!CeSafeCopyMemory(pOutBuffer, &outValue, sizeof(DWORD)))
					{
					SetLastError(ERROR_INVALID_PARAMETER);
					break;
					}
				rc = TRUE;
			}			
			break;

		case IOCTL_AIODIO_SUSPEND_POWER:
			SetSystemPowerState(NULL, POWER_STATE_SUSPEND, POWER_FORCE);
			rc = TRUE;
			break;

#if BSP_AURA_PROTOTYPE

		case IOCTL_AIODIO_INIT_KEYPAD_I2C:
			if ((pInBuffer == NULL) || (inSize < sizeof(DWORD)) ||
                !CeSafeCopyMemory(InBuf, pInBuffer, sizeof(DWORD)))
                {
                SetLastError(ERROR_INVALID_PARAMETER);
                break;
                }
			rc = InitKeypadI2C(InBuf[0]);
			break;

		case IOCTL_AIODIO_INIT_KEYPAD_GPIO:
			if ((pInBuffer == NULL) || (inSize < sizeof(DWORD)) ||
                !CeSafeCopyMemory(InBuf, pInBuffer, sizeof(DWORD)))
                {
                SetLastError(ERROR_INVALID_PARAMETER);
                break;
                }
			rc = InitKeypadGPIO(InBuf[0]);
			break;

		case IOCTL_AIODIO_DEINIT_KEYPAD:
			if ((pInBuffer == NULL) || (inSize < sizeof(DWORD)) ||
                !CeSafeCopyMemory(InBuf, pInBuffer, sizeof(DWORD)))
                {
                SetLastError(ERROR_INVALID_PARAMETER);
                break;
                }
			DeinitKeypad(InBuf[0]);
			rc = TRUE;
			break;

		case IOCTL_AIODIO_WRITE_KEYPAD_LED:
			if ((pInBuffer == NULL) || (inSize < WRITE_KEYPAD_LED_PARAMS * sizeof(DWORD)) ||
                !CeSafeCopyMemory(InBuf, pInBuffer, WRITE_KEYPAD_LED_PARAMS * sizeof(DWORD)))
                {
                SetLastError(ERROR_INVALID_PARAMETER);
                break;
                }
			rc = WriteKeypadLED(InBuf[0], InBuf[1], (UINT8)InBuf[2]);
			break;

		case IOCTL_AIODIO_RESET_KEYPAD:
			if ((pInBuffer == NULL) || (inSize < sizeof(DWORD)) ||
                !CeSafeCopyMemory(InBuf, pInBuffer, sizeof(DWORD)))
                {
                SetLastError(ERROR_INVALID_PARAMETER);
                break;
                }
			ResetKeypad(InBuf[0]);
			rc = TRUE;
			break;
#endif

		case IOCTL_AIODIO_GET_BSPTYPE:
			if ((pOutBuffer == NULL) || (outSize < sizeof(DWORD)))
                {
                SetLastError(ERROR_INVALID_PARAMETER);
                break;
                }
			if (!CeSafeCopyMemory(pOutBuffer, &BSPtype, sizeof(DWORD)))
				{
				SetLastError(ERROR_INVALID_PARAMETER);
				break;
				}
				rc = TRUE;
			break;

		case IOCTL_AIODIO_GET_BOOT_ERROR_CODE:
			if ((pOutBuffer == NULL) || (outSize < sizeof(DWORD)))
                {
                SetLastError(ERROR_INVALID_PARAMETER);
                break;
                }
			if(KernelIoControl(IOCTL_HAL_GET_BOOT_ERROR_CODE, NULL, 0, &outValue, sizeof(DWORD), NULL))
			{
				if (!CeSafeCopyMemory(pOutBuffer, &outValue, sizeof(DWORD)))
					{
					SetLastError(ERROR_INVALID_PARAMETER);
					break;
					}
				rc = TRUE;
			}			
			break;

		case IOCTL_AIODIO_READ_SENSOR:						
			if (pOutSize != 0) *pOutSize = sizeof(DWORD);
            if ((pInBuffer == NULL) || (pOutBuffer == NULL) || 
				(inSize < READ_SENSOR_PARAMS * sizeof(DWORD)) ||
                (outSize < sizeof(DWORD)) || 
				!CeSafeCopyMemory(InBuf, pInBuffer, READ_SENSOR_PARAMS * sizeof(DWORD)))
                {
                SetLastError(ERROR_INVALID_PARAMETER);
                break;
                }
            if(!pAioDio->ReadSensor(InBuf[0], &outValue))
				break;

			if (!CeSafeCopyMemory(pOutBuffer, &outValue, sizeof(DWORD)))
				{
				SetLastError(ERROR_INVALID_PARAMETER);
				break;
				}
            rc = TRUE;
            break;

		case IOCTL_AIODIO_READ_ENCODER:						
			if ((pOutBuffer == NULL) || (outSize < (2 * sizeof(DWORD))))
				{
                SetLastError(ERROR_INVALID_PARAMETER);
                break;
				}
            rc = GetEncoderValues(pOutBuffer);
			break;

		case IOCTL_AIODIO_SET_CODEC_GAIN:
			if ((pInBuffer == NULL) || (inSize < sizeof(DWORD)) 
				)
                {
                SetLastError(ERROR_INVALID_PARAMETER);
                break;
                }
			rc = pAioDio->SetCodecGain(*((DWORD *)pInBuffer));
			break;

#if AIODIO_TESTING
// For Testing Only ---- BEGIN ----		
		case IOCTL_AIODIO_WRITE_BATTERY:
			if ((pInBuffer == NULL) || 
                !CeSafeCopyMemory(pchInBuf, &pInBuffer[3], pInBuffer[1]))
                {
                SetLastError(ERROR_INVALID_PARAMETER);
                break;
                }
			rc = pAioDio->WriteBattery(pInBuffer[0], pchInBuf, pInBuffer[1], pInBuffer[2]);
			break;

		case IOCTL_AIODIO_WRITE_BATTERY_MUX:
			if ((pInBuffer == NULL) || 
                !CeSafeCopyMemory(pchInBuf, &pInBuffer[3], pInBuffer[1]))
                {
                SetLastError(ERROR_INVALID_PARAMETER);
                break;
                }
			rc = pAioDio->WriteBatteryMux(pInBuffer[0], pchInBuf, pInBuffer[1], pInBuffer[2]);
			break;

		case IOCTL_AIODIO_READ_BATTERY:
			if ((pInBuffer == NULL) || 
				(pOutBuffer == NULL) || (outSize <= 0)){
                SetLastError(ERROR_INVALID_PARAMETER);
                break;
                }
			rc = pAioDio->ReadBattery(pInBuffer[0], pchOutBuf, pInBuffer[1], pInBuffer[2]);
			if (!CeSafeCopyMemory(pOutBuffer, pchOutBuf, pInBuffer[1]))
                {
                SetLastError(ERROR_INVALID_PARAMETER);
				rc = FALSE;
                }
			break;

		case IOCTL_AIODIO_READ_BATTERY_MUX:
			if ((pInBuffer == NULL) || 
				(pOutBuffer == NULL) || (outSize <= 0)){
                SetLastError(ERROR_INVALID_PARAMETER);
                break;
                }
			rc = pAioDio->ReadBatteryMux(pInBuffer[0], pchOutBuf, pInBuffer[1], pInBuffer[2]);
			if (!CeSafeCopyMemory(pOutBuffer, pchOutBuf, pInBuffer[1]))
                {
                SetLastError(ERROR_INVALID_PARAMETER);
				rc = FALSE;
                }
			break;

		case IOCTL_AIODIO_WRITE_IO_POS:
			CeSafeCopyMemory(InBuf, pInBuffer, sizeof(DWORD));
			pAioDio->SetDioBitPosition(InBuf[0]);
			rc = TRUE;
            break;

		case IOCTL_AIODIO_WRITE_CS:
			CeSafeCopyMemory(InBuf, pInBuffer, sizeof(DWORD));
			pAioDio->WriteChipSelect(InBuf[0]);
			rc = TRUE;
            break;

		case IOCTL_AIODIO_TEST_PHY:
			TestPHY();
			rc = TRUE;
			break;
// For Testing Only ---- END ----
#endif
	}
cleanUp:
	DEBUGMSG(0, (L"-AIO_IOControl(rc = %d)\r\n", rc));
    return rc;
}

//------------------------------------------------------------------------------
//
//  Function:  DllMain
//
//  Standard Windows DLL entry point.
//
BOOL
__stdcall
DllMain(
    HANDLE hDLL,
    DWORD reason,
    VOID *pReserved
    )
{
    UNREFERENCED_PARAMETER(pReserved);
	switch (reason)
        {
        case DLL_PROCESS_ATTACH:
            DisableThreadLibraryCalls((HMODULE)hDLL);
            break;
        }
    return TRUE;
}

//------------------------------------------------------------------------------
//
//  Function:  GetGpioBit
//
//  Get state of the specified GPIO pin.
//------------------------------------------------------------------------------
DWORD AioDio_t::GetGpioBit(DWORD id)
{
	UINT bit = GPIO_BIT(id);
    UINT bank = GPIO_BANK(id);
	volatile UINT *p = &(ppGpioRegs[bank]->DATAIN);
	UINT Value;

	Value = (INREG32(p) >> (bit)) & 0x01;
	return Value;
}

//------------------------------------------------------------------------------
//
//  Function:  SetGpioBit
//
//  Set the specified GPIO pin.
//------------------------------------------------------------------------------
void AioDio_t::SetGpioBit(DWORD id)
{
	UINT bit = GPIO_BIT(id);
    UINT bank = GPIO_BANK(id);
	volatile UINT *p = &(ppGpioRegs[bank]->DATAOUT);

	SETREG32(p, 1 << (bit));
}

//------------------------------------------------------------------------------
//
//  Function:  ClrGpioBit
//
//  Clear the specified GPIO pin.
//------------------------------------------------------------------------------
void AioDio_t::ClrGpioBit(DWORD id)
{
	UINT bit = GPIO_BIT(id);
    UINT bank = GPIO_BANK(id);
	volatile UINT *p = &(ppGpioRegs[bank]->DATAOUT);

	CLRREG32(p, 1 << (bit));
}

//------------------------------------------------------------------------------
//
//  Function:  SetGpioMultiBits
//
//  Set multiple GPIO pins.
//------------------------------------------------------------------------------
void AioDio_t::SetGpioMultiBits(DWORD id, DWORD pattern)
{
    UINT bank = GPIO_BANK(id);
	volatile UINT *p = &(ppGpioRegs[bank]->DATAOUT);

	*p = *p | pattern;
}

//------------------------------------------------------------------------------
//
//  Function:  ClrGpioMultiBits
//
//  Clear multiple GPIO pins.
//------------------------------------------------------------------------------
void AioDio_t::ClrGpioMultiBits(DWORD id, DWORD pattern)
{
    UINT bank = GPIO_BANK(id);
	volatile UINT *p = &(ppGpioRegs[bank]->DATAOUT);

	*p = *p & ~pattern;
}

//------------------------------------------------------------------------------
//
//  Function:  WriteSPI
//
//  Send data to specified SPI channel
//------------------------------------------------------------------------------
DWORD AioDio_t::WriteSPI(OMAP_MCSPI_CHANNEL_REGS *pChannelRegs,
							  void *pInBuffer,
							  DWORD size, DWORD dwWordLen)
{
	DWORD dwCount = 0;
	DWORD dwWait;
	UCHAR* pData = (UCHAR*)pInBuffer;

	SETREG32(&pChannelRegs->MCSPI_CHCTRL, MCSPI_CHCONT_EN);

	// Write out the data
    for( dwCount = 0; dwCount < size; )
    {
        //  Write out data on byte/word/dword boundaries
        if( dwWordLen > 16 )
        {
            OUTREG32(&pChannelRegs->MCSPI_TX, *(UINT32*)(&pData[dwCount]));   
            dwCount += sizeof(UINT32);
        }
        else if( dwWordLen > 8 )
        {
            OUTREG32(&pChannelRegs->MCSPI_TX, *(UINT16*)(&pData[dwCount]));   
            dwCount += sizeof(UINT16);
        }
        else
        {
            OUTREG32(&pChannelRegs->MCSPI_TX, *(UINT8*)(&pData[dwCount]));   
            dwCount += sizeof(UINT8);
        }   

        //  Wait for TX register to empty out
        dwWait = SPI_TRANSFER_WAIT_COUNT;
        while(dwWait && !(INREG32(&pChannelRegs->MCSPI_CHSTATUS) & MCSPI_CHSTAT_TX_EMPTY))
        {
            StallExecution(1);
            dwWait--;
        }
    }
    
    // Disable the channel.
    CLRREG32(&pChannelRegs->MCSPI_CHCTRL, MCSPI_CHCONT_EN);

//clean:
    //DEBUGMSG(ZONE_FUNCTION, (L"-SPI_Write(rc = %d)\r\n", dwCount));
    return dwCount;
}

//------------------------------------------------------------------------------
//
//  Function:  ReadSPI
//
//  Read data from specified SPI channel
//------------------------------------------------------------------------------
DWORD AioDio_t::ReadSPI(OMAP_MCSPI_CHANNEL_REGS *pChannelRegs,
							 void *pOutBuffer,
							 DWORD size, DWORD dwWordLen)
{
	DWORD dwCount = 0;
	DWORD dwWait;
	UCHAR* pData = (UCHAR*)pOutBuffer;

	SETREG32(&pChannelRegs->MCSPI_CHCTRL, MCSPI_CHCONT_EN);

    // Write out the data
    for( dwCount = 0; dwCount < size; )
    {
        //  Wait for RX register to fill
        dwWait = SPI_TRANSFER_WAIT_COUNT;
        while(dwWait && !(INREG32(&pChannelRegs->MCSPI_CHSTATUS) & MCSPI_CHSTAT_RX_FULL))
        {
            StallExecution(1);
            dwWait--;
        }

        //  Check if timeout occured
        if( dwWait == 0 )
        {
            //DEBUGMSG(ZONE_ERROR, (L"SPI_Read timeout\r\n"));

            // Disable the channel.
            CLRREG32(&pChannelRegs->MCSPI_CHCTRL, MCSPI_CHCONT_EN);
            goto clean;
        }

        //  Read in data on byte/word/dword boundaries
        if( dwWordLen > 16 )
        {
            *(UINT32*)(&pData[dwCount]) = INREG32(&pChannelRegs->MCSPI_RX);   
            dwCount += sizeof(UINT32);
        }
        else if( dwWordLen > 8 )
        {
            *(UINT16*)(&pData[dwCount]) = (UINT16) INREG32(&pChannelRegs->MCSPI_RX);   
            dwCount += sizeof(UINT16);
        }
        else
        {
            *(UINT8*)(&pData[dwCount]) = (UINT8) INREG32(&pChannelRegs->MCSPI_RX);   
            dwCount += sizeof(UINT8);
        }   
    }
    
    // Disable the channel.
    CLRREG32(&pChannelRegs->MCSPI_CHCTRL, MCSPI_CHCONT_EN);

clean:
	//DEBUGMSG(0, (L"-AIODIO: ReadSPI(dwCount = %d)\r\n", dwCount));
    return dwCount;
}

//------------------------------------------------------------------------------
//
//  Function:  WriteReadSPI
//
//  Send and receive data to/from specified SPI channel
//------------------------------------------------------------------------------
DWORD AioDio_t::WriteReadSPI(OMAP_MCSPI_CHANNEL_REGS *pChannelRegs,
								  void *pInBuffer,
								  void *pOutBuffer,
								  DWORD size, DWORD dwWordLen)
{
	DWORD dwCount;
	DWORD dwWait;
	UCHAR* pInData = (UCHAR*)pInBuffer;
    UCHAR* pOutData = (UCHAR*)pOutBuffer;

	SETREG32(&pChannelRegs->MCSPI_CHCTRL, MCSPI_CHCONT_EN);

	// Write out the data
    for( dwCount = 0; dwCount < size; )
    {
        //  Write out data on byte/word/dword boundaries
        if( dwWordLen > 16 )
        {
            OUTREG32(&pChannelRegs->MCSPI_TX, *(UINT32*)(&pOutData[dwCount]));   
        }
        else if( dwWordLen > 8 )
        {
            OUTREG32(&pChannelRegs->MCSPI_TX, *(UINT16*)(&pOutData[dwCount]));   
        }
        else
        {
            OUTREG32(&pChannelRegs->MCSPI_TX, *(UINT8*)(&pOutData[dwCount]));   
        }   

        //  Wait for TX register to empty out
        dwWait = SPI_TRANSFER_WAIT_COUNT;
        while(dwWait && !(INREG32(&pChannelRegs->MCSPI_CHSTATUS) & MCSPI_CHSTAT_TX_EMPTY))
        {
            StallExecution(1);
            dwWait--;
        }

        //  Check if timeout occured
        if( dwWait == 0 )
        {
            //DEBUGMSG(0, (L"WriteReadSPI write timeout\r\n"));
            
            // Disable the channel.
            CLRREG32(&pChannelRegs->MCSPI_CHCTRL, MCSPI_CHCONT_EN);

            goto clean;
        }

        //  Wait for RX register to fill
        dwWait = SPI_TRANSFER_WAIT_COUNT;
        while(dwWait && !(INREG32(&pChannelRegs->MCSPI_CHSTATUS) & MCSPI_CHSTAT_RX_FULL))
        {
            StallExecution(1);
            dwWait--;
        }

        //  Check if timeout occured
        if( dwWait == 0 )
        {
            DEBUGMSG(0, (L"SPI_WriteRead read timeout\r\n"));
            
            // Disable the channel.
            CLRREG32(&pChannelRegs->MCSPI_CHCTRL, MCSPI_CHCONT_EN);
            goto clean;
        }

        //  Read in data on byte/word/dword boundaries
        if( dwWordLen > 16 )
        {
            *(UINT32*)(&pInData[dwCount]) = INREG32(&pChannelRegs->MCSPI_RX);   
            dwCount += sizeof(UINT32);
        }
        else if( dwWordLen > 8 )
        {
            *(UINT16*)(&pInData[dwCount]) = (UINT16) INREG32(&pChannelRegs->MCSPI_RX);   
            dwCount += sizeof(UINT16);
        }
        else
        {
            *(UINT8*)(&pInData[dwCount]) = (UINT8) INREG32(&pChannelRegs->MCSPI_RX);   
            dwCount += sizeof(UINT8);
        }   
    }
    
    // Disable the channel.
    CLRREG32(&pChannelRegs->MCSPI_CHCTRL, MCSPI_CHCONT_EN);
clean:
    return dwCount;
}

#if AIODIO_TESTING
//------For testing only-------------------------------------------------------START
void PrintPadConfig()
{
	static OMAP_SYSC_PADCONFS_REGS *pConfig;
	PHYSICAL_ADDRESS pa;

	pa.QuadPart   = OMAP_SYSC_PADCONFS_REGS_PA;
    pConfig       = (OMAP_SYSC_PADCONFS_REGS *)MmMapIoSpace(pa, sizeof(OMAP_SYSC_PADCONFS_REGS), FALSE);

	RETAILMSG(1,(TEXT("CONTROL_PADCONF_MCBSP1_CLKX        (0x%x) 0x%x\r\n"),
		&pConfig->CONTROL_PADCONF_MCBSP1_CLKX,  pConfig->CONTROL_PADCONF_MCBSP1_CLKX  ));
	RETAILMSG(1,(TEXT("CONTROL_PADCONF_CAM_HS        (0x%x) 0x%x\r\n"),
		&pConfig->CONTROL_PADCONF_CAM_HS,  pConfig->CONTROL_PADCONF_CAM_HS  ));
	RETAILMSG(1,(TEXT("CONTROL_PADCONF_ETK_D0        (0x%x) 0x%x\r\n"),
		&pConfig->CONTROL_PADCONF_ETK_D0,  pConfig->CONTROL_PADCONF_ETK_D0  ));
}

bool AioDio_t::WriteBatteryMux(DWORD subaddr, BYTE *data, DWORD size, BYTE SlaveAddr)
{
	I2CSetSlaveAddress(hI2CPort, SlaveAddr);
	I2CSetSubAddressMode(hI2CPort, 1);
	I2CSetBaudIndex(hI2CPort, SLOWSPEED_MODE);
	return((I2CWrite(hI2CPort, subaddr, (VOID*)data, size) == size) ? TRUE : FALSE);
}

bool AioDio_t::ReadBatteryMux(DWORD subaddr, BYTE *data, DWORD size, BYTE SlaveAddr)
{
	I2CSetSlaveAddress(hI2CPort, SlaveAddr);
	I2CSetSubAddressMode(hI2CPort, 1);
	I2CSetBaudIndex(hI2CPort, SLOWSPEED_MODE);
	return((I2CRead(hI2CPort, subaddr, (VOID*)data, size) == size) ? TRUE : FALSE);
}

bool AioDio_t::WriteBattery(DWORD subaddr, BYTE *data, DWORD size, BYTE SlaveAddr)
{
	I2CSetSlaveAddress(hI2CPort, SlaveAddr);
	I2CSetSubAddressMode(hI2CPort, 1);
	I2CSetBaudIndex(hI2CPort, SLOWSPEED_MODE);
	return((I2CWrite(hI2CPort, subaddr, (VOID*)data, size) == size) ? TRUE : FALSE);
}

bool AioDio_t::ReadBattery(DWORD subaddr, BYTE *data, DWORD size, BYTE SlaveAddr)
{
	I2CSetSlaveAddress(hI2CPort, SlaveAddr);
	I2CSetSubAddressMode(hI2CPort, 1);
	I2CSetBaudIndex(hI2CPort, SLOWSPEED_MODE);
	return((I2CRead(hI2CPort, subaddr, (VOID*)data, size) == size) ? TRUE : FALSE);
}

//----------------------TEST PHY BEGIN----------------------------------------------------------------------
#define START_WRITE_ULPI_DATA(port, reg, data)  (0x80000000 | ((port) << 24) | (2 << 22) | ((reg) << 16) | data)
#define START_READ_ULPI_DATA(port, reg)			(0x80000000 | ((port) << 24)  | (3 << 22)| ((reg) << 16))

BOOL UlpiReadReg(DWORD EhciRegBase, DWORD Port, DWORD Register, BYTE *pData)
{
	volatile DWORD * pEhciUlpiAccess = (volatile DWORD *)(EhciRegBase + 0xa4);
	DWORD ReadData;
	DWORD attempts = 10;
	 
	*pEhciUlpiAccess = START_READ_ULPI_DATA(Port, Register);
	// wait for register access done
	do 
	{
		ReadData = *pEhciUlpiAccess;
		//RETAILMSG(1, (TEXT("EHCI ULPI access Port %d, register %d = 0x%x\r\n"), Port, Register, ReadData));
	}while ((ReadData & 0x80000000) && --attempts>0);
	
	*pData = (BYTE)ReadData;
	return (attempts>0);
}

void UlpiWriteReg(DWORD EhciRegBase, DWORD Port, DWORD Register, BYTE Data)
{
    volatile DWORD * pEhciUlpiAccess = (volatile DWORD *)(EhciRegBase + 0xa4);
    DWORD ReadData;

    *pEhciUlpiAccess = START_WRITE_ULPI_DATA(Port, Register, Data);

    // wait for register access done
    do
    {
        ReadData = *pEhciUlpiAccess;
        //DEBUGMSG(ZONE_INFO, (TEXT("EHCI ULPI access port %d, register 0x%x = 0x%x\r\n"), Port, Register, ReadData));
    }
    while (ReadData & 0x80000000);
}

void TestPHY()
{
	PHYSICAL_ADDRESS pa;
	DWORD	PHY_Reg_Base;
	DWORD	Port = 2;
	BYTE	ReadData;
	DWORD	i, j, ErrorCount = 0, LoopCount = 0;

#define EHCI_BASE_ADDR				0x48064800
#define ULPI_VENDOR_ID_LO_OFFS		0
#define ULPI_VENDOR_ID_HI_OFFS		1
#define ULPI_SCRATCH_OFFS			0x16

#define USB3320_VENDOR_ID_LO		0x24
#define USB3320_VENDOR_ID_HI		0x04
	
	pa.QuadPart = EHCI_BASE_ADDR;
	PHY_Reg_Base = (DWORD)MmMapIoSpace(pa, 0x1000, FALSE);
	
	if(UlpiReadReg(PHY_Reg_Base, Port, ULPI_VENDOR_ID_LO_OFFS, &ReadData))
		RETAILMSG(1,(TEXT("Ulpi PHY Vendor ID Lo: %x\r\n"), ReadData));
	else
		RETAILMSG(1,(TEXT("Read Ulpi PHY Vendor ID Lo Failed\r\n")));

	if(UlpiReadReg(PHY_Reg_Base, Port, ULPI_VENDOR_ID_HI_OFFS, &ReadData))
		RETAILMSG(1,(TEXT("Ulpi PHY Vendor ID Lo: %x\r\n"), ReadData));
	else
		RETAILMSG(1,(TEXT("Read Ulpi PHY Vendor ID Hi Failed\r\n")));

	for(i = 0; i < 256; i++)
	{
		for(j = 0; j < 256; j++)
		{
			LoopCount++;
			
			UlpiWriteReg(PHY_Reg_Base, Port, ULPI_SCRATCH_OFFS, (BYTE)j);
			UlpiReadReg(PHY_Reg_Base, Port, ULPI_SCRATCH_OFFS, &ReadData);
			if(ReadData != j)
			{
				ErrorCount++;
			}
		}
		RETAILMSG(1,(TEXT("LoopCount=%d, ErrCount=%d\r\n"), LoopCount, ErrorCount));
	}
	RETAILMSG(1,(TEXT("PHY Test Done - ErrorCount=%d\r\n"), ErrorCount));
	MmUnmapIoSpace(&PHY_Reg_Base, 0x1000);
}
//----------------------TEST PHY END----------------------------------------------------------------------
#endif
//------For testing only-------------------------------------------------------END