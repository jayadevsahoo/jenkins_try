
#ifndef NovRamManager_HH
#define NovRamManager_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmit by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class:  NovRamManager - NOVRAM Manager.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Persistent-Objects/vcssrc/NovRamManager.hhv   25.0.4.0   19 Nov 2013 14:18:50   pvcs  $
//
//@ Modification-Log
//
//  Revision: 016   By: mnr   Date: 25-Feb-2010   SCR Number: 6436
//  Project:  PROX4
//  Description:
//      PROX firmware revision and serial number related updates.
// 
//  Revision: 015  By:  rhj    Date:  04-Apr-2009    SCR Number: 6495 
//  Project:  840S2
//  Description:
//		Added setAllTestsUndefined().
//
//  Revision: 014  By:  gdc    Date:  10-Dec-2008    SCR Number: 6377
//  Project:  840S
//  Description:
//	Removed DELTA project single screen support.
//
//  Revision: 013   By: gdc  Date:  27-Mar-2007    SCR Number: 6237
//  Project:  Trend
//  Description:
//      Added compact flash test status for Trending option.
//
//  Revision: 012   By: sah  Date:  25-Aug-2000    DR Number:  5753
//  Project:  Delta
//  Description:
//      Delta project-specific changes:
//      *  added a screen-configuration flag, for determining whether this
//         is a single-screen configuration, or not
//
//  Revision: 011   By: sah  Date:  07-Mar-2000    DR Number:  5677
//  Project:  NeoMode
//  Description:
//      Eliminated functionality tied to 'SIGMA_PSUEDO_OFFICIAL' build
//      flag.
//
//  Revision: 010   By: sah  Date:  25-Jan-2000    DR Number:  5424
//  Project:  NeoMode
//  Description:
//      Added access methods to the user-controlled options.
//
//  Revision: 009  By:  sah    Date:  07-Jan-1999    DCS Number: 5321
//  Project:  Sigma (R8027)
//  Description:
//      Obsoleted this class's '.in' file because there are no longer
//	any inlined methods.
//
//  Revision: 008  By:  sah    Date:  05-May-1998    DCS Number: 5041
//  Project:  Sigma (R8027)
//  Description:
//	Added an error-code argument to 'VerifySelf()' so that more
//	information can be stored in the error diagnostics.  Also, added
//	new error reporting method, 'ReportAuxillaryInfo()', for
//	logging system information entries.
//
//  Revision: 007  By:  sah    Date:  29-Sep-1997    DCS Number: 2525
//  Project:  Sigma (R8027)
//  Description:
//	Removed methods for accessing 'exhCalInProgressState_', and
//	Vent-Inop Test Info.
//
//  Revision: 006  By:  sah    Date:  24-Sep-1997    DCS Number: 2513
//  Project:  Sigma (R8027)
//  Description:
//	Eliminated pre-processor variables around the 'OverrideAll?stTests()'
//	for use by the Service Laptop.  Also, added new methods for clearing
//	out all of the diagnostic entries within the various diagnostic logs.
//
//  Revision: 005  By:  sah    Date:  16-Sep-1997    DCS Number: 2385 & 1861
//  Project:  Sigma (R8027)
//  Description:
//	Added new methods for accessing 'serviceModeState_', and added
//	accessing methods for 'atmPressOffset_'.
//
//  Revision: 004  By:  sah    Date:  04-Sep-1997    DCS Number: 1923
//  Project:  Sigma (R8027)
//  Description:
//      Replacing the specific Communication Diagnostic Log, with a more
//	general System Information Log.
//
//  Revision: 003  By:  sah    Date:  09-Jul-1997    DCS Number: 2279
//  Project:  Sigma (R8027)
//  Description:
//      Added access to two new BD data items, via
//      'Get{Air,O2}FlowSensorOffset()' and
//      'Update{Air,O2}FlowSensorOffset()'.
//
//  Revision: 002  By:  sah    Date:  01-Jul-1997    DCS Number: 2066 & 2107
//  Project:  Sigma (R8027)
//  Description:
//	Moved '{Get,Update}LastSstPatientCctType()' from the GUI side to
//	the BD side, and replaced the 'IsExhCalInProgress()' and
//	'UpdateExhCalInProgressFlag()' with 'GetExhCalInProgressState()'
//	and 'UpdateExhCalInProgressState()'.
//
//  Revision: 001  By:  sah    Date:  15-Mar-1997    DCS Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//          Initial version (Integration baseline).
//
//====================================================================

#include "Sigma.hh"
#include "PersistentObjs.hh"
#include "DiscreteValue.hh"
#include "NovRamStatus.hh"
#include "TemplateMacros.hh"
#include "IpcIds.hh"
#include "TestResultId.hh"

//@ Usage-Classes
// forward declarations...
class  DiagnosticCode;
class  NonVolatileMemory;
class  VolatileMemory;
class  BtatEventState;
class  ResultEntryData;
class  AbstractArray(CodeLogEntry);
class  AbstractArray(ResultTableEntry);
class  Resistance;
class  SerialNumber;
class  BigSerialNumber;
class  UserOptions;
#if defined(SIGMA_GUI_CPU)  ||  defined(SIGMA_COMMON)
class  AlarmAction;
class  FixedArray(CodeLogEntry,MAX_CODE_LOG_ENTRIES);
class  FixedArray(ResultTableEntry,MAX_TEST_ENTRIES);
class  FixedArray(AlarmLogEntry,MAX_ALARM_ENTRIES);
class  BatchSettingValues;
class  NonBatchSettingValues;
class  EstTestResultData;
class  SstTestResultData;
class  DataCorruptionInfo;
class  TimeStamp;
#endif // defined(SIGMA_GUI_CPU)  ||  defined(SIGMA_COMMON)
#if defined(SIGMA_BD_CPU)  ||  defined(SIGMA_COMMON)
class  BdSettingValues;
class  CircuitCompliance;
class  BdSystemState;
class  PsolLiftoff;
class  Fio2CalInfo;
class  FlowSensorOffset;
class  ServiceModeNovramData;
class  AtmPressOffset;
#endif // defined(SIGMA_BD_CPU)  ||  defined(SIGMA_COMMON)
//@ End-Usage

class NovRamManager
{
  public:
    static Boolean  IsNonVolatileAddr(const void* addr);
    static Boolean  IsVolatileAddr   (const void* addr);

    static NovRamStatus::InitializationId  Initialize(void);
    static NovRamStatus::VerificationId    VerifySelf(Uint16& rErrorCode);

    static void  SetEstTestToNotApplicable(const Uint estTestNumber);
    static void  SetSstTestToNotApplicable(const Uint estTestNumber);

    static void  RegisterPendingClockUpdate(
				      const Boolean isUserInitiated = TRUE
					   );
    static void  RegisterCompletedClockUpdate(
				      const Boolean isUserInitiated = TRUE
					     );

    static void  GetActiveBtatEvents   (BtatEventState&       rBtatEvents);
    static void  UpdateActiveBtatEvents(const BtatEventState& newBtatEvents);

    static Boolean  HasBackgroundTestFailed(void);
    static void     UpdateBackgroundTestFailureFlag(
    						const Boolean newFlagValue
						   );

    static void  LogSystemInformation(const DiagnosticCode& diagnosticCode);
    static void  LogSystemDiagnostic (const DiagnosticCode& diagnosticCode);
    static void  LogEstSstDiagnostic (const DiagnosticCode& diagnosticCode);

    static void  ClearSystemInformation(void);
    static void  ClearSystemDiagnostic (void);
    static void  ClearEstSstDiagnostic (void);

    static void  StartingEstTest    (const Uint estTestNumber);
    static void  CompletedEstTest   (const Uint estTestNumber);
    static void  RepeatingEstTest   (const Uint estTestNumber);
    static void  EstOverridden      (void);
    static void  CompletedEst       (void);
    static void  GetOverallEstResult(ResultEntryData& rOverallEstResultData);

    static void  StartingSstTest    (const Uint sstTestNumber);
    static void  CompletedSstTest   (const Uint sstTestNumber);
    static void  RepeatingSstTest   (const Uint sstTestNumber);
    static void  SstOverridden      (void);
    static void  CompletedSst       (void);
    static void  GetOverallSstResult(ResultEntryData& rOverallSstResultData);
    static void  SetAllTestsUndefined(void);

    static Boolean  ArePatientSettingsAvailable(void);
    static void     UpdatePatientSettingsFlag  (const Boolean newFlagValue);

    static Boolean  AreBatchSettingsInitialized(void);
    static void     UpdateBatchSettingsFlag    (const Boolean newFlagValue);

    static void  GetInspResistance(Resistance& rInspCctResistance);
    static void  GetExhResistance (Resistance& rExhCctResistance);

    static void  GetUnitSerialNumber   (SerialNumber&       rSerialNum);
    static void  UpdateUnitSerialNumber(const SerialNumber& serialNum);

    static void  OverrideAllEstTests(void);
    static void  OverrideAllSstTests(void);

    static void  GetUserOptions  (UserOptions& rUserOptions);
    static void  StoreUserOptions(const UserOptions& userOptions);

#if defined(SIGMA_GUI_CPU)  ||  defined(SIGMA_COMMON)
    //=================================================================
    //  GUI-only interface...
    //=================================================================

    static void  GetSystemInfoEntries(
	        FixedArray(CodeLogEntry,MAX_CODE_LOG_ENTRIES)& rArrEntries
				     );
    static void  GetSystemLogEntries(
	        FixedArray(CodeLogEntry,MAX_CODE_LOG_ENTRIES)& rArrEntries
				    );
    static void  GetEstSstLogEntries(
	        FixedArray(CodeLogEntry,MAX_CODE_LOG_ENTRIES)& rArrEntries
		    		    );

    static Boolean  HasGuiEstPassed(void);

    static void  GetEstResultEntries(
	      FixedArray(ResultTableEntry,MAX_TEST_ENTRIES)& rArrEntries
		    		    );
    static void  GetSstResultEntries(
	      FixedArray(ResultTableEntry,MAX_TEST_ENTRIES)& rArrEntries
		    		    );

    static void  GetEstTestDiagnostics(
	      const Uint                                     estTestNumber,
	      FixedArray(CodeLogEntry,MAX_CODE_LOG_ENTRIES)& rArrEntries
		    		      );
    static void  GetSstTestDiagnostics(
	      const Uint                                     sstTestNumber,
	      FixedArray(CodeLogEntry,MAX_CODE_LOG_ENTRIES)& rArrEntries
		    		      );

    static void  GetAlarmLogEntries (
		   FixedArray(AlarmLogEntry,MAX_ALARM_ENTRIES)& rArrEntries
		    		    );

    static void  LogAlarmAction(const AlarmAction& alarmAction);
    static void  ClearAlarmLog(void);

    static void  GetAcceptedBatchSettings(
				BatchSettingValues& rCurrAcceptedBatchValues
					 );
    static void  UpdateAcceptedBatchSettings(
			    const BatchSettingValues& newAcceptedBatchValues
					    );

    static Boolean  AreNonBatchSettingsInitialized(void);
    static void     UpdateNonBatchSettingsFlag(const Boolean newFlagValue);

    static void  GetAcceptedNonBatchSettings(
			  NonBatchSettingValues& rCurrAcceptedNonBatchValues
					    );
    static void  UpdateAcceptedNonBatchSettings(
			      const NonBatchSettingValues& newNonBatchValues
					       );

    static DiscreteValue  GetLastSstHumidifierType   (void);
    static void           UpdateLastSstHumidifierType(
					  const DiscreteValue newHumidifierType
						     );

    static Uint32  GetCompressorOperationalHours(void);
    static Uint32  GetSystemOperationalHours    (void);

    static void  GetEstTestResultData(EstTestResultData& rEstTestResultData);
    static void  UpdateEstTestResultData(
			      const EstTestResultData& newEstTestResultData
					);

    static void  GetSstTestResultData(SstTestResultData& rSstTestResultData);
    static void  UpdateSstTestResultData(
			      const SstTestResultData& newSstTestResultData
					);

    static void  GetDataCorruptionInfo   (DataCorruptionInfo& rCorruptionInfo);
    static void  UpdateDataCorruptionInfo(
				  const DataCorruptionInfo& newCorruptionInfo
					 );

    static void  GetNoVentStartTime  (TimeStamp& rStartTime);
    static void  SetNoVentStartTime  (void);
    static void  ClearNoVentStartTime(void);

    static DiscreteValue GetCompactFlashTestStatus(void);
    static void UpdateCompactFlashTestStatus(const DiscreteValue newStatus);

#endif // defined(SIGMA_GUI_CPU)  ||  defined(SIGMA_COMMON)


#if defined(SIGMA_BD_CPU)  ||  defined(SIGMA_COMMON)
    //=================================================================
    //  BD-only interface...
    //=================================================================

    static BdSystemState  GetBdSystemState   (void);
    static void           UpdateBdSystemState(
				    const BdSystemState& newBdSystemState
					     );

    static void  GetAcceptedBatchSettings(
				  BdSettingValues& rCurrAcceptedBatchValues
					 );
    static void  UpdateAcceptedBatchSettings(
			      const BdSettingValues& newAcceptedBatchValues
					    );

    static void  GetPsolLiftoffCurrents   (PsolLiftoff& rPsolLiftoff);
    static void  UpdatePsolLiftoffCurrents(const PsolLiftoff& psolLiftoff);

    static void  GetCircuitComplianceTable   (
				        CircuitCompliance& rComplianceTable
					     );
    static void  UpdateCircuitComplianceTable(
				  const CircuitCompliance& complianceTable
					     );

    static void  GetFio2CalInfo   (Fio2CalInfo&       rFio2CalInfo);
    static void  UpdateFio2CalInfo(const Fio2CalInfo& fio2CalInfo);

    static Uint32  GetCompressorOperationalMinutes   (void);
    static void    UpdateCompressorOperationalMinutes(const Uint32 newMinutes);

    static Uint32  GetSystemOperationalMinutes   (void);
    static void    UpdateSystemOperationalMinutes(const Uint32 newMinutes);

    static void  GetCompressorSerialNumber   (SerialNumber&       rSerialNum);
    static void  UpdateCompressorSerialNumber(const SerialNumber& serialNum);

    static void  UpdateInspResistance(const Resistance& inspCctResistance);
    static void  UpdateExhResistance (const Resistance& exhCctResistance);

    static void  GetAirFlowSensorOffset(FlowSensorOffset& rOffset);
    static void  GetO2FlowSensorOffset (FlowSensorOffset& rOffset);

    static void  UpdateAirFlowSensorOffset(const FlowSensorOffset& offset);
    static void  UpdateO2FlowSensorOffset (const FlowSensorOffset& offset);

    static void  GetServiceModeState   (ServiceModeNovramData& rState);
    static void  UpdateServiceModeState(const ServiceModeNovramData& newState);

    static void  GetAtmPressOffset   (AtmPressOffset& rOffset);
    static void  UpdateAtmPressOffset(const AtmPressOffset& newOffset);

    static DiscreteValue  GetLastSstPatientCctType   (void);
    static void           UpdateLastSstPatientCctType(
					   const DiscreteValue newPatientCctType
						     );

	static void  GetLastSstProxFirmwareRev(BigSerialNumber&  rProxFirmwareRev);
	static void  UpdateLastSstProxFirmwareRev(const BigSerialNumber& newProxFirmwareRev);

	static void  GetLastSstProxSerialNum(Uint32& rProxSerialNum);
	static void  UpdateLastSstProxSerialNum(const Uint32& newProxSerialNum);

#endif // defined(SIGMA_BD_CPU)  ||  defined(SIGMA_COMMON)

    static Uint32  GetNewSequenceNumber(void);

    static void  ReportAuxillaryInfo(const void *const  beginAddr,
    				     const Uint         dataSize,
    				     const Uint         statusId = 0u);

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL, 
			   const char*       pPredicate = NULL);
  
  private:
    NovRamManager(const NovRamManager&);	// not implemented...
    NovRamManager (void);			// not implemented...
    ~NovRamManager(void);			// not implemented...
    void  operator=(const NovRamManager&);	// not implemented...

    //@ Type:  AuxInfoLayout_
    //
    struct AuxInfoLayout_
    {
      Uint32  statusId   :  2;
      Uint32  dataSize   : 14;
      Uint32  addrOffset : 16;
    };

    //@ Type:  AuxInfoCode_
    //
    union AuxInfoCode_
    {
      AuxInfoLayout_  layoutFields;
      Uint32  	      value;
    };
};


#endif // NovRamManager_HH
