#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: LinearSensor - Basic sensor that only requires simple linear 
//      interpolation to determine the engineering units of the
//      measurement.
//---------------------------------------------------------------------
//@ Interface-Description
//      The LinearSensor class is derived from the Sensor class and
//      is used for converting linear sensor values from ADC counts
//      to engineering values.  The conversion factors are defined
//      when the object is created.  A method is implemented to convert
//		the ADC counts to engineering units.
//---------------------------------------------------------------------
//@ Rationale
//      Abstraction of various sensors that require linear interpolation.
//---------------------------------------------------------------------
//@ Implementation-Description
//      When the object is constructed, the conversion factors are
//      computed.  The method rawToEngValue_ is used to convert
//      ADC counts to engineering values.
// $[00401]
//---------------------------------------------------------------------
//@ Fault-Handling
//		n/a
//---------------------------------------------------------------------
//@ Restrictions
//      None
//---------------------------------------------------------------------
//@ Invariants
//      None
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/LinearSensor.ccv   25.0.4.0   19 Nov 2013 13:54:08   pvcs  $
//
//@ Modification-Log
//
//  Revision: 007 By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//	Description:
//		Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 006 By: syw    Date: 29-Sep-1997   DR Number: DCS 2410
//  	Project:  Sigma (R8027)
//		Description:
//			Initialize offset_ to 0.0 and add offset to rawToEngValue_().
//
//  Revision: 004 By: syw    Date: 22-Aug-1996   DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Eliminate compiler warnings.
//
//  Revision: 003 By: syw    Date: 20-Feb-1996   DR Number: DCS 674
//  	Project:  Sigma (R8027)
//		Description:
//			Removed #ifndef around #include.
//
//  Revision: 002  By: syw    Date:  05-Dec-1995    DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Added SafetyNetSensorData * argument to constructor.
//
//	Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: INITIAL RELEASE
//      Project:  Sigma (R8027)
//      Description:
//      	Initial version
//
//=====================================================================

#include "LinearSensor.hh"

//@ Usage-Classes

#include "MathUtilities.hh"

//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: LinearSensor()
//
//@ Interface-Description
//      Constructor.  Six arguments are needed to construct a linear
//      sensor: the pointer to a SafetyNetSensorData, the ADC channel,
//		the minimum and maximum voltage, and the engineering values at the
//		minimum and maximum voltage range.
//---------------------------------------------------------------------
//@ Implementation-Description
//      The ADC channel id and pointer to SafetyNetSensorData is used to
//		construct the sensor object.  The ADC voltage range and the
//		corresponding engineering range are used to compute the minimum
//		count value, and the count to engineering conversion factor.  The
//		engineering value at the minimum voltage range is also saved.
//---------------------------------------------------------------------
//@ PreCondition
//      0.0 volts <= voltsMin < voltsMax <= MAX_COUNT_VALUE volts
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================

LinearSensor::LinearSensor(SafetyNetSensorData *pData,
							const AdcChannels::AdcChannelId adcId,
                            const Real32 voltsMin,
                            const Real32 voltsMax,
                            const Real32 engValueAtMin,
                            const Real32 engValueAtMax)
: Sensor(adcId, pData)
{
	CALL_TRACE("LinearSensor::LinearSensor( \
				SafetyNetSensorData *pData, \
				const AdcChannels::AdcChannelId adcId, const Real32 voltsMin, \
				const Real32 voltsMax, const Real32 engValueAtMin, \
                const Real32 engValueAtMax)") ;
                
    CLASS_PRE_CONDITION( (voltsMin < voltsMax) &&
                         (voltsMin >= 0.0) && (voltsMax <= MAX_VOLT_VALUE) &&
                         !IsEquivalent(voltsMin, voltsMax, HUNDREDTHS)) ;

    // $[TI1]

    countsMin_ = voltsMin / COUNT_TO_VOLT_FACTOR ;
    engValueAtMin_ = engValueAtMin ;
    countToEngFactor_ = (engValueAtMax - engValueAtMin)
                         / (voltsMax - voltsMin) * COUNT_TO_VOLT_FACTOR ;
    offset_ = 0.0 ;
}

#ifdef INTEGRATION_TEST_ENABLE
#if defined (SIGMA_BD_CPU)
//------------------------------------------------------------------------------
// updateValue
//------------------------------------------------------------------------------
void LinearSensor::updateValue( void)
{
    Sensor::updateValue() ;
    Sensor::updateVirtualValue();
}

#endif // defined (SIGMA_BD_CPU)
#endif // INTEGRATION_TEST_ENABLE

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~LinearSensor()
//
//@ Interface-Description
//      Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//        None
//---------------------------------------------------------------------
//@ PreCondition
//        None
//---------------------------------------------------------------------
//@ PostCondition
//        None
//@ End-Method
//=====================================================================

LinearSensor::~LinearSensor( void)
{
	CALL_TRACE("LinearSensor::~LinearSensor( void)") ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault()
//
//@ Interface-Description
//      This method takes four arguments: softFaultID, lineNumber, pFileName,
//      and pPredicate and is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
LinearSensor::SoftFault( const SoftFaultID  softFaultID,
                         const Uint32       lineNumber,
                         const char*        pFileName,
                         const char*        pPredicate)
{
    CALL_TRACE("SoftFault( softFaultID, lineNumber, pFileName, pPredicate)") ;
    
    FaultHandler::SoftFault( softFaultID, BD_IO_DEVICES, LINEARSENSOR,
                             lineNumber, pFileName, pPredicate) ;
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: rawToEngValue_()
//
//@ Interface-Description
//      This method takes one argument, count and returns the
//      engineering value equivalent to count.  
//---------------------------------------------------------------------
//@ Implementation-Description
//      Range check the value, convert to engineering units using the
//      conversion values defined during object construction, and
//      return the result.
//---------------------------------------------------------------------
//@ PreCondition
//      The argument, count is limited to count <= MAX_COUNT_VALUE.
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================

Real32
LinearSensor::rawToEngValue_( const AdcCounts count)    const
{
	CALL_TRACE("LinearSensor::rawToEngValue_( const AdcCounts count)") ;
  
    CLASS_PRE_CONDITION( count <= MAX_COUNT_VALUE ) ;

    // $[TI1]
  
    return (countToEngFactor_ * (Real32)(count - countsMin_) + engValueAtMin_ + offset_) ;
}


//=====================================================================
//
//  Private Methods...
//
//=====================================================================

