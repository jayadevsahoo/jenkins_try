#ifndef SmManager_IN
#define SmManager_IN

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
// Class: SmManager - Service Mode Manager
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/SmManager.inv   25.0.4.0   19 Nov 2013 14:23:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 007  By: dosman    Date: 17-Mar-1999     DR Number: 5322
//  Project:  ATC
//  Description:
//	ATC initial release.
//	Added method to determine the current test type.
//	Added method to reset the testpoint count.
//
//  Revision: 006  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 005  By:  gdc    Date: 24-Sep-1997    DR Number: DCS 2513
//       Project:  Sigma (840)
//       Description:
//            Provided for parameter string in DoTest function.
//
//  Revision: 004  By:  quf    Date: 05-Aug-1997    DR Number: DCS 2130
//       Project:  Sigma (840)
//       Description:
//            Added inline method isTestRepeat().
//
//  Revision: 002  By:  quf    Date: 22-May-1997    DR Number: DCS 2131
//       Project:  Sigma (840)
//       Description:
//            Modification-Log fix.
//            Fixed global reference names (i.e. "r" changed to "R").
//
//  Revision: 001  By:  mad    Date:  08-Nov-1995    DR Number: none
//       Project:  Sigma (R8027)
//       Description:
//             Initial version.
//
//=====================================================================

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getCurrentTestType()
//
//@ Interface-Description
//	This method returns information to allow a user
//	to know the current test type.
//
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
SmTestId::ServiceModeTestTypeId 
SmManager::getCurrentTestType(void) const
{
	// $[TI1]
	CALL_TRACE("SmManager::getCurrentTestType(void)");
	return testType_;
}





//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: advanceTestPoint()
//
//@ Interface-Description
//	This method sets the data member testPointCount_ equal to the
//	ItemizedTestDataId, no return value.  In other words, it "resyncs"
//	the testPointCount_ in case the next data ID is not equal to the
//	testPointCount_.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Set data member testPointCount_ equal to the ItemizedTestDataId argument.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

inline void
SmManager::advanceTestPoint( SmDataId::ItemizedTestDataId nextTestPoint)
{
// $[TI1]
	CALL_TRACE("SmManager::advanceTestPoint( \
		SmDataId::ItemizedTestDataId nextTestPoint)");

	testPointCount_ = nextTestPoint;
}









//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: resetTestPoint()
//
//@ Interface-Description
//      This method resets the data member testPointCount_ to its initial value,
//      no return value.  In other words, it "resyncs"
//      the testPointCount_ in case we want to start over collecting data.
//---------------------------------------------------------------------
//@ Implementation-Description
// 	Set data member testPointCount_ equal to zero.
//	The rationale for this method is to prevent overflow
//	of the data packet being sent over the serial interface.
//	For example, in sending data over the serial interface,
//	each time data is sent within a given test the data's location 
//	within the packet (array) is incremented. If we want to allow 
//	unlimited resending of the same data, then not resetting 
//	this counter would mean that consecutive attempts to send
//	the data would eventually be out of range of the array.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

inline void
SmManager::resetTestPoint(void)
{
	// $[TI1]
	CALL_TRACE("SmManager::resetTestPoint(void)");

	testPointCount_ = 0;
}






//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isTestRepeat()
//
//@ Interface-Description
//	This static method returns the state of the Boolean testRepeat_
//	data member, indicating if test is being repeated.  No arguments.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Return the value of testRepeat_.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

inline Boolean
SmManager::isTestRepeat(void)
{
// $[TI1]
	CALL_TRACE("SmManager::isTestRepeat(void)");

	return( testRepeat_);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: CommDied()
//
//@ Interface-Description
//	This static method returns the state of the Boolean CommDiedDuringTest_
//	data member, indicating if comm went down anytime during a test.
//	No arguments.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Return the value of CommDiedDuringTest_.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

inline Boolean
SmManager::CommDied(void)
{
// $[TI1]
	CALL_TRACE("SmManager::CommDied(void)");

	return( CommDiedDuringTest_);
}


#if defined(SIGMA_GUI_CPU) || defined(SM_TEST) || defined(SIGMA_UNIT_TEST)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  DoTest()
//
//@ Interface-Description
//	This static GUI-only method is used by an external subsystem to
//	request a Service-Mode test.  Arguments are a ServiceModeTestId
//	and a Boolean testRepeat flag indicating if this test is a repeat
//	of the last test, no return value.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//	Invoke the non-static doTest() with the same arguments.
//--------------------------------------------------------------------- 
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition 
//	none
//@ End-Method 
//===================================================================== 
inline void
SmManager::DoTest(SmTestId::ServiceModeTestId testId,
		Boolean testRepeat, const char * const pParameters )
{
// $[TI1]
	CALL_TRACE("SmManager::DoTest(SmTestId::ServiceModeTestId testId,\
		Boolean testRepeat, const char * const pParameters )");

	RSmManager.doTest(testId, testRepeat, pParameters);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  OperatorAction()
//
//@ Interface-Description
//	This static GUI-only method is used by an external subsystem to
//	send an operator keypress (in response to an operator prompt) to
//	Service-Mode.  Arguments are a ServiceModeTestId and an ActionId,
//	no return value.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//	Invoke the non-static operatorAction() with the same arguments.
//--------------------------------------------------------------------- 
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition 
//	none
//@ End-Method 
//===================================================================== 
inline void
SmManager::OperatorAction(SmTestId::ServiceModeTestId testId,
		  SmPromptId::ActionId actionId)
{
// $[TI1]
	CALL_TRACE("SmManager::OperatorAction(SmTestId::ServiceModeTestId \
			testId, SmPromptId::ActionId actionId)") ;

	RSmManager.operatorAction(testId, actionId);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  DoFunction()
//
//@ Interface-Description
//	This static GUI-only method is used by an external subsystem to
//	request a Service-Mode function.  Argument is a FunctionId,
//	no return value.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//	Invoke the non-static doFunction() with the same argument.
//--------------------------------------------------------------------- 
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition 
//	none
//@ End-Method 
//===================================================================== 
inline void
SmManager::DoFunction(SmTestId::FunctionId functionId)
{
// $[TI1]
	CALL_TRACE("SmManager::DoFunction(SmTestId::FunctionId functionId)");

	RSmManager.doFunction(functionId);
}

#endif  // defined(SIGMA_GUI_CPU) || defined(SM_TEST) || defined(SIGMA_UNIT_TEST)


#if defined (SIGMA_BD_CPU)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: signalTestPoint()
//
//@ Interface-Description
//	This method is used to set the testSignalRequested_ flag TRUE, which
//	causes the test signal bit to be turned on momentarily via
//	processTestSignal().  No return value.
//
//  $[08048] During EST tests invoked through the GUI serial interface, a
//  real-time external trigger, indicating when EST is sampling test
//  data points, is provided.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Set testSignalRequested_ TRUE.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

inline void
SmManager::signalTestPoint(void)
{
// $[TI1]
	CALL_TRACE("SmManager::signalTestPoint(void)");

	testSignalRequested_ = TRUE;
}

#endif  // defined(SIGMA_BD_CPU)


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//=====================================================================
//
//  Private Methods...
//
//=====================================================================


#endif // SmManager_IN 
