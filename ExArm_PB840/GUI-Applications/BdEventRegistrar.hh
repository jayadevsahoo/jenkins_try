#ifndef BdEventRegistrar_HH
#define BdEventRegistrar_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: BdEventRegistrar - The entry point in the GUI-Applications 
// subsystem to register for breath events.  Stores a BdEventCascade 
// for every event to allow multiple GUI objects to be notified of 
// a new breath event.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/BdEventRegistrar.hhv   25.0.4.0   19 Nov 2013 14:07:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By: sah    Date:  07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 002  By:  yyy    Date:  08-Sep-1997    DCS Number: 2401
//  Project:  Sigma (R8027)
//  Description:
//		Added eventStatus as the second parameter for ChangeHappened().
//      Use the event status coming from the BD event message rather than 
//		to inquire the event status at the time  the EventHappened() to
//		be be processed.
//
//  Revision: 001  By:  hhd		Date:  22-MAY-95    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================


//@ Usage-Classes
#include "EventData.hh"
#include "GuiAppClassIds.hh"
class BdEventCascade;
class BdEventTarget;
//@ End-Usage


class BdEventRegistrar
{
public:
	static void Initialize(void);
	static void RegisterTarget(EventData::EventId eventId,
							   BdEventTarget* pTarget);
	static Boolean EventHappened(void);
	static void ChangeHappened(EventData::EventId eventId, EventData::EventStatus eventStatus,
								 EventData::EventPrompt eventPrompt);
	
	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:

private:
	// these methods are purposely declared, but not implemented...
	BdEventRegistrar(void);							// not implemented...
	BdEventRegistrar(const BdEventRegistrar&);	// not implemented...
	~BdEventRegistrar(void);							// not implemented...
	void operator=(const BdEventRegistrar&);			// not implemented...

	//@ Data-Member: CascadeArray_
	// A static array of BdEventCascade objects, implemented as
	// a simple pointer to an object.  The index to the array corresponds
	// to the BDGuiEventId::BDGuiEventIdType and provides fast lookup.
	static BdEventCascade *CascadeArray_;

	//@ Data-Member: IsDataChangedArray_
	// An array that parallels the CascadeArray_ and indicates which patient
	// data items have been changed since the last call to UpdateHappened().
	static Int32 IsDataChangedArray_[2*(EventData::END_OF_EVENT_IDS + 1)];

	//@ Data-Member: StatusDataArray_
	// An array that parallels the CascadeArray_ and holds the staus for the 
	// corresponding eventId in the IsDataChangedArray_.
	static Int32 StatusDataArray_[2*(EventData::END_OF_EVENT_IDS + 1)];

	//@ Data-Member: PromptDataArray_
	// An array that parallels the CascadeArray_ and holds the prompt for the 
	// corresponding eventId in the IsDataChangedArray_.
	static Int32 PromptDataArray_[2*(EventData::END_OF_EVENT_IDS + 1)];

	//@ Data-Member: NextArrayIndex_
	// An index into IsDataChangedArray_ array that represents the next available
	// slot in IsDataChangedArray_ array to register the bd event.
	static Int32 NextArrayIndex_;

};


#endif // BdEventRegistrar_HH 
