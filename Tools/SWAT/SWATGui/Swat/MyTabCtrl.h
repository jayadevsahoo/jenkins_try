#if !defined(AFX_CMyTabCtrl_H__BAF69081_1BBC_476D_A303_7660CCB9A9B7__INCLUDED_)
#define AFX_CMyTabCtrl_H__BAF69081_1BBC_476D_A303_7660CCB9A9B7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CMyTabCtrl.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CMyTabCtrl window

class CMyTabCtrl : public CTabCtrl
{
// Construction
public:
	CMyTabCtrl();

// Attributes
public:
	int m_DialogID[3];
	CDialog *m_Dialog[3];

	int m_nPageCount;

	void ActivateTabDialogs();
	void InitDialogs();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMyTabCtrl)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMyTabCtrl();

	// Generated message map functions
protected:
	//{{AFX_MSG(CMyTabCtrl)
	afx_msg void OnSelchange(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CMyTabCtrl_H__BAF69081_1BBC_476D_A303_7660CCB9A9B7__INCLUDED_)
