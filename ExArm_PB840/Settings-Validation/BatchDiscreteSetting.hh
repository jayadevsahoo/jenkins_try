 
#ifndef BatchDiscreteSetting_HH
#define BatchDiscreteSetting_HH

//====================================================================
//
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  BatchDiscreteSetting - Batch Discrete Setting.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/BatchDiscreteSetting.hhv   25.0.4.0   19 Nov 2013 14:27:14   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 002   By: sah    Date:  28-May-1998    DR Number: 5092
//  Project:  COLOR
//  Description:
//	--- Merged Rev "Color" (1.4.1.0) into Rev "BiLevel" (1.4.2.0)
//	Added new constructor parameters that may be called by derived
//	class to instill bounds for this discrete class.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Initial version.
//  
//====================================================================

//@ Usage-Classes
#include "DiscreteSetting.hh"
//@ End-Usage


class BatchDiscreteSetting : public DiscreteSetting 
{
  public:
    virtual ~BatchDiscreteSetting(void);

    virtual Boolean  isChanged(void) const;

    virtual SettingValue  getDefaultValue   (void) const;
    virtual SettingValue  getNewPatientValue(void) const = 0;

    virtual void  calcTransition(const DiscreteValue newValue,
				 const DiscreteValue currValue);
 
    virtual void  acceptTransition(const SettingId::SettingIdType settingId,
                                   const DiscreteValue            newValue,
                                   const DiscreteValue            currValue);

    static void   SoftFault(const SoftFaultID softFaultID,
			    const Uint32      lineNumber,
			    const char*       pFileName  = NULL, 
			    const char*       pPredicate = NULL);

  protected:
    BatchDiscreteSetting(
      const SettingId::SettingIdType batchDiscreteId,
      const SettingId::SettingIdType* arrDependentSettingIds,
      const Uint                     totalNumValues,
      const Boolean                  useNonWrappingMode = FALSE,
      const SettingBoundId              upperLimitBoundId  = ::NULL_SETTING_BOUND_ID,
      const SettingBoundId              lowerLimitBoundId  = ::NULL_SETTING_BOUND_ID
      			);

#if defined(SIGMA_DEVELOPMENT)
    virtual Ostream&  print_(Ostream& ostr) const;
#endif  // defined(SIGMA_DEVELOPMENT)

  private:
    BatchDiscreteSetting(const BatchDiscreteSetting&);	// not implemented...
    BatchDiscreteSetting(void);                         // not implemented...
    void  operator=(const BatchDiscreteSetting&);	// not implemented...
};


#endif // BatchDiscreteSetting_HH 
