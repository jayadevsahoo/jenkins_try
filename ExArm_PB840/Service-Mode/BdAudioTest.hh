#ifndef BdAudioTest_HH
#define BdAudioTest_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: BdAudioTest - Test the BD audio alarm
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/BdAudioTest.hhv   25.0.4.0   19 Nov 2013 14:23:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 003  By:  quf    Date: 07-Oct-1997    DR Number: DCS 2203
//       Project:  Sigma (840)
//       Description:
//			Incorporated new power fail cap discharge procedure.
//
//  Revision: 002  By:  quf    Date: 22-May-1997    DR Number: DCS 2131
//       Project:  Sigma (840)
//       Description:
//            Modification-Log fix only.
//
//  Revision: 001  By:  mad    Date:  2-Nov-1994    DR Number: none
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//====================================================================

#include "ServiceMode.hh"

//@ Usage-Classes

//@ End-Usage

extern const Real32 MIN_CABLE_VOLTAGE;
extern const Real32 MAX_CABLE_VOLTAGE;
extern const Real32 MIN_CAP_CHARGED_VOLTAGE;
extern const Real32 MAX_CAP_CHARGED_VOLTAGE;
extern const Real32 MIN_TIME_CONSTANT;
extern const Uint32 MIN_DISCHARGE_TIME;

class BdAudioTest
{
  public:
    BdAudioTest(void);
    ~BdAudioTest(void);

    static void SoftFault(const SoftFaultID softFaultID,
		  const Uint32      lineNumber,
		  const char*       pFileName  = NULL, 
		  const char*       pPredicate = NULL);

    void runTest(void);
  
  protected:

  private:
    BdAudioTest(const BdAudioTest&);		// not implemented...
    void   operator=(const BdAudioTest&);	// not implemented...

};


#endif // BdAudioTest_HH 
