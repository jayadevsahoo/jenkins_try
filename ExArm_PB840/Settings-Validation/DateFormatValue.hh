
#ifndef DateFormatValue_HH
#define DateFormatValue_HH

//====================================================================
//
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  DateFormatValue - Values of all different types of 
//                           date formats.
// $[00643] - Sigma must support the following short date formats...
// $[00644] - Sigma must support the following long date formats...
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/DateFormatValue.hhv   25.0.4.0   19 Nov 2013 14:27:20   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001   By: rhj    Date:  01-Jun-2007    SCR Number: 6237
//  Project: Trend
//  Description:
//      Initial version.
//====================================================================

//@ Usage-Classes
//@ End-Usage


struct DateFormatValue
{
  //@ Type:  DateFormatValueId
  // All of the possible values of the Date Format Setting.
  enum DateFormatValueId
  {
     DATE_FORMAT_YY_MM_DD,  
     DATE_FORMAT_MM_DD_YY_WITH_DASH,
     DATE_FORMAT_MM_DD_YY_WITH_SLASH,
     DATE_FORMAT_DD_MM_YY,
     DATE_FORMAT_DD_MMM_YY,
     DATE_FORMAT_YY_MMM_DD,    
     NUM_DATE_FORMAT_VALUES
  };
};


#endif // DateFormatValue_HH 
