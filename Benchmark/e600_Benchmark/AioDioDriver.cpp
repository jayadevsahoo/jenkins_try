#include "StdAfx.h"
#include "AioDioDriver.h"
#include <aiodio.h>
#include <bsp_version.h>
#include "sdk_spi.h"

static const IOCTL_SPI_CONFIGURE_IN spi4_CH0_ADC_config = {0, 0x000107D0};	//0000 0000 0000 0001 0000 0111 1101 0000
																		/*	TRM :		0		- TXRX mode
																			WL :		0xF		- 16 bits
																			CLKD :		8		- Divide Ref Clock of 48Mbps by 16 = 3 Mbps
																			POL PHA :	00		- clk active high and sampling on rising edge
																		*/

// E600 DAC MAX5135, Max clock = 30 MHz, 24-bit word (8 control bits and 16 data bits; however, D0-D3 are don't-care bits)
// Data shifted into the DAC on the falling edge of SCLK                                                    111098 7654 3210
static const IOCTL_SPI_CONFIGURE_IN spi1_CH3_DAC_config = {3, 0x00010BC9};		// 0000 0000 0000 0001 0000 1011   1100 1000	
//                                                                                                          WL      EPOL(6)
																		/*	TRM :		00		- TXRX
																			WL(7:11) :  0x17	- 24 bits
																			EPOL (6):	  1		- SPIM_CSX held low during active state
																			CLKD :(15:16) 1		- Divide Ref Clock of 48Mbps by 2 = 24 Mbps (Spec 50)
																			POL PHA :	 01		- clk active high and sampling on falling edge
																		*/

// MAX1068 - Max clock is 200K samples per second, used to read ADC on the BD board
static const IOCTL_SPI_CONFIGURE_IN spi1_CH0_ADC_config = {0, 0x00010FD0};		//0000 0000 0000 0001 0000 1111 1101 0000
																		/*	TRM (13:12)		:	00		- TXRX
																			WL (11:7)		:	0x1F	- 32 bits
																			EPOL (6)		:	1		- SPIM_CSX held low during active state
																			CLKD (5:2)		:	4		- Divide Ref Clock of 48Mbps by 16 = 3Mbps
																			POL PHA (1:0)	:	00		- clk active high and sampling on rising edge
																		*/
// MAX1068 - Max clock is 200KHz, used to read ADC on Manifold board
static const IOCTL_SPI_CONFIGURE_IN spi3_CH0_ADC_config = {0, 0x00010FD0};		//0000 0000 0000 0001 0000 0111 1101 0000
																		/*	TRM (13:12)		:	00		- TXRX
																			WL (11:7)		:	0x1F	- 32 bits
																			EPOL (6)		:	1		- SPIM_CSX held low during active state
																			CLKD (5:2)		:	8		- Divide Ref Clock of 48Mbps by 256 = 375 Kbps
																			POL PHA (1:0)	:	00		- clk active high and sampling on rising edge
																		*/
// Currently not used
static const IOCTL_SPI_CONFIGURE_IN spi3_CH1_ADC_config = {1, 0x00010FD0};		//0000 0000 0000 0001 0000 0111 1110 0000
																		/*	TRM (13:12)		:	00		- TXRX
																			WL (11:7)		:	0x1F	- 32 bits
																			EPOL (6)		:	1		- SPIM_CSX held low during active state
																			CLKD (5:2)		:	8		- Divide Ref Clock of 48Mbps by 256 = 375 Kbps
																			POL PHA (1:0)	:	00		- clk active high and sampling on rising edge
																		*/

static const LPCTSTR lpSPI1Name = TEXT("SPI1:");
static const LPCTSTR lpSPI3Name = TEXT("SPI3:");
static const LPCTSTR lpSPI4Name = TEXT("SPI4:");
static const LPCTSTR lpGPIOName = TEXT("GIO1:");

AioDioDriver_t::AioDioDriver_t(void)
{
	InitializeCriticalSection(&AioDioCriticalSection);
	LPCTSTR lpAIOFileName = TEXT("AIO1:");
	hAioDioDriver = CreateFile( lpAIOFileName,  GENERIC_READ | GENERIC_WRITE, 0, 0, OPEN_EXISTING, 0, 0);
	BSPType = BSP_INVALID;
	DeviceIoControl(hAioDioDriver, IOCTL_AIODIO_GET_BSPTYPE, NULL, 0, &BSPType, sizeof(DWORD), NULL, NULL); 
	
	InitAioDio();  
}

AioDioDriver_t::~AioDioDriver_t(void)
{
	DeviceIoControl(hAioDioDriver, IOCTL_AIODIO_DEINIT, NULL, 0, NULL, 0, NULL, NULL);
}

bool AioDioDriver_t::ReadKnob( unsigned char *KnobCount, unsigned char *KnobDir )
{
	bool	bResult;
	DWORD	DataOut[2];
#pragma warning(disable:4800)
	bResult = (bool)DeviceIoControl(hAioDioDriver, IOCTL_AIODIO_READ_ENCODER, NULL, 0, &DataOut, sizeof(DataOut), NULL, NULL);
#pragma warning(default:4800)
	*KnobCount = (unsigned char)DataOut[0];
	*KnobDir = (unsigned char)DataOut[1];	
	return(bResult);
}

void AioDioDriver_t::InitAioDio( void )
{
	DeviceIoControl(hAioDioDriver, IOCTL_AIODIO_DEINIT, NULL, 0, NULL, 0, NULL, NULL);

	DeviceIoControl(hAioDioDriver, IOCTL_AIODIO_INIT_HARDWARE, NULL, 0, NULL, 0, NULL, NULL);       
	// Init SPI driver
	hSPI1_CH0 = CreateFile( lpSPI1Name,  GENERIC_READ | GENERIC_WRITE, 0, 0, OPEN_EXISTING, 0, 0);	
	if(hSPI1_CH0 == INVALID_HANDLE_VALUE)
	{
		return;
	}
	if(!DeviceIoControl(hSPI1_CH0, IOCTL_SPI_CONFIGURE, (LPVOID)&spi1_CH0_ADC_config, sizeof(IOCTL_SPI_CONFIGURE_IN), NULL, 0, NULL, NULL))
	{
		return;
	}

	hSPI4_CH0 = CreateFile( lpSPI4Name,  GENERIC_READ | GENERIC_WRITE, 0, 0, OPEN_EXISTING, 0, 0);	
	if(hSPI4_CH0 == INVALID_HANDLE_VALUE) 
	{
		return;
	}
	if(!DeviceIoControl(hSPI4_CH0, IOCTL_SPI_CONFIGURE, (LPVOID)&spi4_CH0_ADC_config, sizeof(IOCTL_SPI_CONFIGURE_IN), NULL, 0, NULL, NULL))
	{
		return;
	}

	hSPI3_CH0 = CreateFile( lpSPI3Name,  GENERIC_READ | GENERIC_WRITE, 0, 0, OPEN_EXISTING, 0, 0);	
	if(hSPI3_CH0 == INVALID_HANDLE_VALUE)
	{
		return;
	}
	if(!DeviceIoControl(hSPI3_CH0, IOCTL_SPI_CONFIGURE, (LPVOID)&spi3_CH0_ADC_config, sizeof(IOCTL_SPI_CONFIGURE_IN), NULL, 0, NULL, NULL))
	{
		return;
	}


	hSPI3_CH1 = CreateFile( lpSPI3Name,  GENERIC_READ | GENERIC_WRITE, 0, 0, OPEN_EXISTING, 0, 0);	
	if(hSPI3_CH0 == INVALID_HANDLE_VALUE)
	{
		return;
	}
	if(!DeviceIoControl(hSPI3_CH1, IOCTL_SPI_CONFIGURE, (LPVOID)&spi3_CH1_ADC_config, sizeof(IOCTL_SPI_CONFIGURE_IN), NULL, 0, NULL, NULL))
	{
		return;
	}
}
