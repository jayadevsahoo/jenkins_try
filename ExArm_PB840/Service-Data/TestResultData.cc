#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an processing retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//              Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//================== C L A S S   D E S C R I P T I O N ================
//@ Class:  TestResultData -  Manage the service test data to be saved into
//                            or loaded from NovRam.
//---------------------------------------------------------------------
//@ Interface-Description
//>Von
// 
//  Description:
//  ------------
//    This class provides the following services for NovRam:
//    1. Load the previous test result data from NovRam for upper level
//       application (GuiApp) usage.
//    2. Save the current test result data from upper level application to
//       NovRam.
//
//    The test data are stored int both GUI and BD boards based on the test
//    itself.  All the GUI tests store their test data on GUI board while all
//    the BD tests store their test data on BD board.  The TestResultData class
//    exists on GUI boards only.  It gets its test data from Service Mode and
//    stores them into its own local data structure.  When TestResultData
//    is informed of test being completed,  it will then notify NovRam to
//    copy its local data structure to NovRam.  It is the NovRam's
//    responsibility to separate the test data into GUI and BD part.
//
//    When upper level application needs test result data from NovRam,
//    TestResultData class will request NovRam to copy the information into
//    TestResultData's local data structure.
//    
//  Class Key Methods:
//  -------------------
//     1. loadDataFromNovRam -- Request NovRam to fill in the test data result.
//        Note this method should never be called.  The derived class shall
//        execute this functionality.
//	   2. testCompleted -- Called by upper level applications to inform the
//        NovRam the completion of the test set.
//        Note this method should never be called.  The derived class shall
//        execute this functionality.
//	   3. testStart -- Called by upper level applications to inform the 
//        NovRam of the start of a test.
//     4. getTestResultData -- Request one test datum for a specified test ID.
//	   5. setTestResultData -- Store one test datum for a specified test ID.
//	   6. getTestDataCount -- Get the maximun possible data count for a 
//        specified test ID.
//
//>Voff
//---------------------------------------------------------------------
//@ Rationale
//    The purpose of this class is to act as a parent class for retriving and
//    storing the test result data for all possible derived children class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  To support the information accessibility, TestResultData has two
//  important data members: pTestInfo_ pointer and pTestDataArray_.
//  The 'pTestInfo_' is a pointer to the array of items which keep information
//  about the test data array.
//  The 'pTestDataArray_' is a pointer to the derived class's array of test data
//  entries.
// 
//  Once the upper level application signifies the start of a specific test ID,
//  the TestResultData will set all the array elements corresponding to this
//  test ID to initial value.  During the test, each test data can be saved by
//  calling setTestResultData.  When the upper level application indicates the
//  completion of the specific test ID, the NovRam is informed to copy the
//  test result over.  
//
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Data/vcssrc/TestResultData.ccv   25.0.4.0   19 Nov 2013 14:21:06   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah     Date:  15-Jan-99   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' to a non-inlined method.
//
//  Revision: 001  By:  yyy    Date:  16-May-95   DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//====================================================================

#include "Sigma.hh"
#include "limits.h"
#include "SmTestId.hh"
#include "TestResultData.hh"
#include "FaultHandler.hh"

//@ Code...
//====================================================================
//
//  Public Methods...
//

//====================================================================
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TestResultData()  [Default Constructor]
//
//@ Interface-Description
//	This is the contructor of class TestResultData.
//---------------------------------------------------------------------
//@ Implementation-Description
//	On construction of this class, three data items get assigned values:
//	the pTestInfo_, the pTestDataArray_, and the MAX_ENTRIES_.
//	The body of the constructor itself is empty.  
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

TestResultData::TestResultData(TestDataStruct *pTestInfo, Real32 *pArrEntries, Int32 maxEntries) :
	pTestInfo_(pTestInfo), pTestDataArray_(pArrEntries), MAX_ENTRIES_(maxEntries)	
{
	CALL_TRACE("TestResultData::TestResultData(void)");
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~TestResultData  [Destructor]
//
//@ Interface-Description
//  Empty
//---------------------------------------------------------------------
//@ Implementation-Description
//  Empty
//---------------------------------------------------------------------
//@ PreCondition
//	 none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

TestResultData::~TestResultData(void)
{
	CALL_TRACE("TestResultData::~TestResultData(void)");
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:	operator=
//
//@ Interface-Description
//  Assignment operator.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Set the maximum possible test data counts into MAX_ENTRIES_.  Return
//	 the object instance.
//---------------------------------------------------------------------
//@ PreCondition
//	 none
//---------------------------------------------------------------------
//@ PostCondition
//	 none
//@ End-Method
//=====================================================================
TestResultData& 
TestResultData::operator=(const TestResultData &testResultData)
{
	CALL_TRACE("TestResultData::operator=(const TestResultData &testResultData");

	if (this != &testResultData)
	{													// $[TI1]
		MAX_ENTRIES_ = testResultData.MAX_ENTRIES_;
	}													// $[TI2]
	return(*this);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getTestResultData [public] 
//
//@ Interface-Description
//	 This method is called from GUI-Applications.  It returns the Service
//	 data for a given test id and a specified index.
//---------------------------------------------------------------------
//@ Implementation-Description
//  We return the content of the array entry which corresponds to the given
//	 test id and data index.
//---------------------------------------------------------------------
//@ PreCondition
//	 The testId has to be in the valid range.
//---------------------------------------------------------------------
//@ PostCondition
//	 none
//@ End-Method
//=====================================================================

Real32
TestResultData::getTestResultData(
				Uint testId,
				SmDataId::ItemizedTestDataId dataIndex)
{
	CALL_TRACE("TestResultData::getTestResultData("
				"Uint testId,"
				"SmDataId::ItemizedTestDataId dataIndex)");

	CLASS_ASSERTION( (testId >= 0) && (testId < MAX_ENTRIES_) );

	Real32 *pRetData = pTestInfo_[testId].pData +dataIndex;

	return( *pRetData); 
														// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setTestResultData 
//
//@ Interface-Description
//	 This method stores the given test datum into its private data structure.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Store the given test data in the local data structure at the specified
//  location.
//	 We use the test id and the data index (with respect to the start of this
//  test's data) as offset into the pTestDataArray_
//---------------------------------------------------------------------
//@ PreCondition
//	 none
//---------------------------------------------------------------------
//@ PostCondition
//	 none
//@ End-Method
//=====================================================================

void 
TestResultData::setTestResultData(Real32 testData,
								Uint testId,
								SmDataId::ItemizedTestDataId dataIndex)
{
	CALL_TRACE("TestResultData::setTestResultData(Real32 testData,"
			"Uint testId, "
			"SmDataId::ItemizedTestDataId dataIndex)");

	CLASS_ASSERTION( (testId >= 0) && (testId < MAX_ENTRIES_) );

	*((pTestInfo_[testId].pData) + dataIndex) = testData;
														// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:	testStart
//
//@ Interface-Description
//	 Called by Service-Data when an test is starting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Erase the data for this test and initialize it with an initial value. 
// $[07052] At the start fo each test in EST, the EST test Result subscreen ...
//---------------------------------------------------------------------
//@ PreCondition
//	 The testId has to be in valid range.
//---------------------------------------------------------------------
//@ PostCondition
//	 none
//@ End-Method
//=====================================================================
void 
TestResultData::testStart(Uint testId)
{
	CALL_TRACE("TestResultData::testStart(Uint testId)");

	CLASS_ASSERTION( (testId >= 0) && (testId < MAX_ENTRIES_) );

	int count = pTestInfo_[testId].count;
	Real32 *pDataEntry = pTestInfo_[testId].pData;

	for (int i=0; i<count; i++, pDataEntry++)
	{													// $[TI1]
		*pDataEntry = INT_MIN;
	}													// $[TI2]
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: operator<< 
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Ostream&  operator<< (Ostream& ostr, TestResultData& testResultData)
{
	Real32 *pArray;
	int		count = 0;

	ostr << "testResultData.pTestInfo_[0].pData = " << testResultData.pTestInfo_[0].pData << "\n";
	ostr << "testResultData.pTestInfo_[0].count = " << testResultData.pTestInfo_[0].count << "\n";

	ostr << "Real Data in pTestDataArray_:" << "\n";


	for (int i = 0; i < testResultData.MAX_ENTRIES_; i++)
	{
		pArray = &(testResultData.pTestDataArray_[i]);
		ostr << (*pArray); 
		count++;
		if (count < 6)
		{
			ostr << " ";	// Insert blank separators between data 
		}
		else 
		{
			ostr << "\n";	// Insert blank separators between data 
		}

	}
	return ostr;
}
#endif 	// #if defined(SIGMA_DEVELOPMENT)


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:	Copy constructor 
//
//@ Interface-Description
//  Copy the contents of the argument into this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Store the contents of the argument into this class.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
TestResultData::TestResultData(const TestResultData& testResultData)
{
	CALL_TRACE("TestResultData::TestResultData(const TestResultData"
					"&testResultData");

	*this = testResultData;
												// $[TI1]
}


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                      [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//      'lineNumber' are essential pieces of information.  The 'pFileName'
//      and 'pPredicate' strings may be defaulted in the macro to reduce
//      code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
TestResultData::SoftFault(const SoftFaultID  softFaultID,
			  const Uint32       lineNumber,
			  const char*        pFileName,
			  const char*        pPredicate)
{
	CALL_TRACE("TestResultData::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, SERVICE_DATA, TEST_RESULT_DATA, 
							lineNumber, pFileName, pPredicate);
}
