#ifndef CALIBRATION_H

#define CALIBRATION_H

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
//  Class: CalibrationTable_t - The base class for all calibration tables.
//---------------------------------------------------------------------

#include <stdlib.h>
#include <wchar.h>

#include "A2dReadingsStructs.h"
#include "CalibrationIDs.h"
#include "AioDioDriver.h"

// index into the EpromCalibrationStruct_t below
#define P2_CAL_INDEX		0
#define P1_CAL_INDEX		1
#define PDRIVE_CAL_INDEX	2
#define PAIR_CAL_INDEX		3
#define PO2_CAL_INDEX		4
#define PSUP_CAL_INDEX		5

struct EpromCalibrationStructure_t {
                        // 120 Cm H2O
          WORD          ExhalePressure;
          WORD          InternalPressure;
          WORD          ExhDrivePressure;
                       // 15 PSI
          WORD          AirPressure;
          WORD          O2Pressure;
          WORD          ExhSupply;
                        // 120 Cm H2O
          WORD         EndExhalePressure;
          WORD         EndInternalPressure;
          WORD         EndExhDrivePressure;
                       // 15 PSI
          WORD         EndAirPressure;
          WORD         EndO2Pressure;
          WORD         EndExhSupply;

} ;

static const DWORD CALIBRATION_STRUCTURE_LENGTH = sizeof(EpromCalibrationStructure_t);
static const WORD MANIFOLD_CALIBRATION_DATA_ADDRESS = 0;
static const WORD MANIFOLD_CALIBRATION_CRC_ADDRESS = MANIFOLD_CALIBRATION_DATA_ADDRESS + CALIBRATION_STRUCTURE_LENGTH;

enum StorageType_t
{
	MAIN_FLASH_STORAGE,
	SPI_FLASH_STORAGE
};

struct CalibrationInfo_t
{
	CalibrationID_t CalibrationID;
	TCHAR			CalName[24];
	TCHAR			FileName[256];
	uint16_t		nCalSteps;
	uint16_t		TableSize;
	const float		*pDefaultTable;
	StorageType_t	Storage;
	uint8_t			CalIndex;
};

enum CalibrationStatus_t
{
	CAL_FAILED,
	CAL_PASSED,
	CAL_PENDING,
	CAL_PENDING2
};

enum InspFlowType_t
{
	FLOW_TYPE_AIR,
	FLOW_TYPE_O2
};

static const uint16_t EXH_VALVE_CLOSE_TIME      = 2000;  // 500 ms between 5V and Exh valve closed
static const uint16_t P1_CAL_TABLE_SIZE			= 2;
static const uint16_t P2_CAL_TABLE_SIZE			= 2;
static const uint16_t PDRIVE_CAL_TABLE_SIZE		= 2;
static const uint16_t PAIR_CAL_TABLE_SIZE		= 2;
static const uint16_t PO2_CAL_TABLE_SIZE		= 2;
static const uint16_t PSUPLY_CAL_TABLE_SIZE 	= 2;
static const uint16_t INSP_VALVE_TABLE_SIZE		= 114;	// 57 * 2
static const uint16_t CIRCUIT_TABLE_SIZE		= 27;	// (8 * 3) + 3
static const uint16_t EXP_VALVE_TABLE_SIZE		= 606; //243;//101*6; 303;	// 81 * 3
static const uint16_t EXH_FLOW_TABLE_SIZE		= 302;	// 151 * 2
static const uint16_t O2_SENSOR_TABLE_SIZE		= 2;

static const uint16_t NUM_INSP_VALVE_SUBTABLES	= 2;
static const uint16_t NUM_EXH_VALVE_SUBTABLES	= 6; //3;
static const uint16_t NUM_EXH_FLOW_SUBTABLES	= 2;
static const uint16_t NUM_CIRCUIT_SUBTABLES		= 3;

static const uint16_t INSP_VALVE_SUBTABLE_SIZE	= (INSP_VALVE_TABLE_SIZE / NUM_INSP_VALVE_SUBTABLES);
static const uint16_t EXH_VALVE_SUBTABLE_SIZE	= (EXP_VALVE_TABLE_SIZE / NUM_EXH_VALVE_SUBTABLES);
static const uint16_t EXH_FLOW_SUBTABLE_SIZE	= (EXH_FLOW_TABLE_SIZE / NUM_EXH_FLOW_SUBTABLES);
static const uint16_t CIRCUIT_SUBTABLE_SIZE		= ((CIRCUIT_TABLE_SIZE - 3) / NUM_CIRCUIT_SUBTABLES);

static const uint16_t MAX_DAC_VALUE				= 4095;
static const uint16_t MAX_ADC_VALUE				= 4095;
static const float	  DAC_REFERENCE_VOLTAGE		= 2.50;
static const float    ADC_REFERENCE_VOLTAGE		= 5.0;
static const uint16_t ZERO_INDEX				= 0;
static const uint16_t SPAN_INDEX				= 1;

static const uint16_t FIO2_100_INDEX			= 0;
static const uint16_t FIO2_21_INDEX				= 1;

struct ExhValveTable_t
{
		// EV cal table from high to low
	float PdrvTable[EXH_VALVE_SUBTABLE_SIZE];
	float P2Table[EXH_VALVE_SUBTABLE_SIZE];
	float DacTable[EXH_VALVE_SUBTABLE_SIZE];
			// EV cal table from low to high
	float PdrvTable_l2h[EXH_VALVE_SUBTABLE_SIZE];
	float P2Table_l2h[EXH_VALVE_SUBTABLE_SIZE];
	float DacTable_l2h[EXH_VALVE_SUBTABLE_SIZE];

};

struct InspValveTable_t
{
	float FlowTable[INSP_VALVE_SUBTABLE_SIZE];
	float DacTable[INSP_VALVE_SUBTABLE_SIZE];
};

struct ExhSensorTable_t
{
    float FlowTable[EXH_FLOW_SUBTABLE_SIZE];
	float DacTable[EXH_FLOW_SUBTABLE_SIZE];
};

struct ExhSensorTableStorage_t
{
    Uint32 serialNumber_ ;

    Uint32 getSerialNumber()
    {
        return serialNumber_;
    }

    ExhSensorTable_t ExhSensorTable;
};

struct CircuitTable_t
{
	float FlowTable[CIRCUIT_SUBTABLE_SIZE];
	float DP1Table[CIRCUIT_SUBTABLE_SIZE];
	float DP2Table[CIRCUIT_SUBTABLE_SIZE];
	float CircuitCompliance;
	float Ri;
	float Re;
};

struct TransducerTable_t
{
	float ZeroValue;
	float SpanValue;
};

struct O2SensorTable_t
{
	float Value100;
	float Value21;
};

class CalibrationTable_t
{
public:
	CalibrationTable_t(CalibrationID_t CalID);
	~CalibrationTable_t();

	void					GetTableInfo(uint8_t *pTab, uint16_t &TabSize);
	float*					GetTablePointer() { return pTable; }
	bool					IsCalibrated(){ return bCalibrated; }
	CalibrationStatus_t		GetCalibrationStatus(){ return CalibrationStatus; }

	virtual CalibrationStatus_t		Calibrate(A2dWithStatus_t A2dData);
	virtual CalibrationStatus_t		Calibrate(Flow_t Flow, InspFlowType_t FlowType);
	virtual CalibrationStatus_t		Calibrate(const ConvertedA2dReadings_t *pConvertedData);
	virtual void			CancelCalibration() {}
	virtual void			AcceptCalibration() {}
	virtual void            UpdateSlopeAndSpan(CalibrationID_t CAL_ID)
	{
		UNUSED_SYMBOL(CAL_ID); 
	}


protected:
	CalibrationInfo_t		CalibrationInfo;
	float					*pTable;
	uint16_t				TableByteSize;
	bool					bCalibrated;
	CalibrationStatus_t		CalibrationStatus;
	bool					CalibrationDone;
	uint32_t				Crc;

	virtual bool			DoCalibration(const ConvertedA2dReadings_t *pConvertedData) 
	{ 
	UNUSED_SYMBOL(pConvertedData);
	return false; 
	}

	virtual bool			DoCalibration(A2dWithStatus_t A2dData) 
	{
	UNUSED_SYMBOL(A2dData); 
	return false; 
	}
	virtual bool			DoCalibration(Flow_t Flow, InspFlowType_t FlowType)
	{
		UNUSED_SYMBOL(Flow);
		UNUSED_SYMBOL(FlowType); 
		return false; 
	}
	virtual bool			RetrieveCalibrationTable();
	virtual bool			SaveCalibrationTable();

	void					ControlPSOL(AnalogOutputs_t Type, DacCounts Value);
};

#endif
