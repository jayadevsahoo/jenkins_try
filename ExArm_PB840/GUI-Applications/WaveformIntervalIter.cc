#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: WaveformIntervalIter - An iterator class designed not to
// iterate through a full WaveformDataBuffer, but through a specified
// interval of data in WaveformDataBuffer.
//---------------------------------------------------------------------
//@ Interface-Description
// WaveformIntervalIter is designed to facilitate the Waveforms subscreen
// accessing waveform data in a simple manner.  The Waveforms subscreen
// requirement is to access blocks of waveform data, either a block which
// represents a full waveform or a block which represents a waveform
// segment, typically a segment which is the latest waveform data that needs
// to be plotted.  These requirements drive much of the interface of
// WaveformIntervalIter and have lead to this iterator class having some
// untypical iterator methods.
//
// This class will not work until it is associated with a WaveformDataBuffer.
// This is done through the attachToBuffer() method.  Once attached, the
// first thing to do would be to set the beginning and end of the current
// interval, thus allowing iteration to begin.  There are various methods
// which help in doing this, namely beginInterval(), endInterval(),
// setToNextInterval(), beginNextInterval(), beginIntervalAtOffset() and
// endIntervalAtOffset().
//
// Iteration is performed with the typical goFirst(), goLast(), goNext() and
// goPrev() methods.  Patient data values from the current sample in the buffer
// can be retrieved via getBoundedValue() and getDiscreteValue().
//
// An important method of setting an interval is by using time, e.g.
// the beginIntervalAtOffset() method takes a value which is specified
// in milliseconds and it changes the interval start by the number of samples
// that would be measured in that time.  Since a sample is read every 20
// milliseconds there is a simple and direct relationship between the
// number of samples in an interval and the real time spanned by the interval.
// The getLength() function returns this value, the time spanned by the
// current interval.
//
// The Waveforms subscreen requires various ways of keeping various
// WaveformIntervalIter objects synchronized with each other.  The methods
// sync(), syncFirstWithCurrent(), syncLast() and syncLastWithCurrent()
// provides the needed functionality.
//
// reset() simply 'resets' the iterator so that it does not point to an
// interval.
//---------------------------------------------------------------------
//@ Rationale
// Provides the only external way to iterate through and access the data stored
// in WaveformDataBuffer objects.
//---------------------------------------------------------------------
//@ Implementation-Description
// This class is a friend of the WaveformDataBuffer.  It has private
// knowledge of how data storage is implemented in WaveformDataBuffer
// allowing it to efficiently iterate through and access WaveformDataBuffer
// objects.
//
// All this iterator needs to store is a pointer to the WaveformDataBuffer
// through which it iterates as well as markers for the start, end and current
// position in the waveform interval.  Most of the implementation deals with
// modifying the start, end and current position.  The values that these
// members can take depends on the implementation of WaveformDataBuffer.  See
// the WaveformDataBuffer implementation description for an explanation of how
// these numbers are managed.
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and pre-conditions
// to verify the validity of arguments.  All assertions and pre-conditions
// are made 'safe' due to the necessity for fast waveform data access.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/WaveformIntervalIter.ccv   25.0.4.0   19 Nov 2013 14:08:44   pvcs  $
//
//@ Modification-Log
//
//  Revision: 009   By: rhj    Date: 15-Mar-2011   SCR Number: 6755 
//  Project:  PROX
//  Description:
//      Display 840's circuit pressure during waiting for patient 
//      connect even if prox is enabled.
//  
//  Revision: 008   By: rhj    Date: 13-Jan-2010   SCR Number: 6631
//  Project:  PROX
//  Description:
//     Added isProxReady flag
//  
//  Revision: 007   By: rpr    Date:  14-Dec-2010    SCR Number: 6604
//  Project:  PROX
//  Description:
//      Ensure plots are not iterated through endlessly. 
//
//  Revision: 006   By: rhj    Date:  20-Apr-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//      Added Prox pressure 
//
//  Revision: 005   By: erm    Date:  23-Apr-2002    DCS Number: 5848
//  Project:  VCP
//  Description:
//      Merge PAV into VCP
//
//  Revision: 004   By: sah   Date:  02-Jun-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      *  added lung pressure, lung flow (or wye) and lung volume.
//      *  added 'IS_PEEP_RECOVERY_ITEM' for detecting PEEP-recovery breaths.
//      *  added mode and support type values for use with frozen
//         shadow traces
//
//  Revision: 003  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

//@ Usage-Classes
#include "WaveformIntervalIter.hh"
#include "BdEventRegistrar.hh"
#include "ProxEnabledValue.hh"
#include "SettingsMgr.hh"
#include "Setting.hh"
#include "WaveformsSubScreen.hh"
#include "UpperSubScreenArea.hh"
//@ End-Usage

//@ Code...


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: WaveformIntervalIter()  [Default Constructor]
//
//@ Interface-Description
// Constructs a WaveformIntervalIter.  However, the iterator cannot be
// used to iterate until attachToBuffer() and some of the begin/end
// interval methods are called.
//---------------------------------------------------------------------
//@ Implementation-Description
// Initialize all members to the null/empty state.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

WaveformIntervalIter::WaveformIntervalIter(void) :
		pWaveformDataBuffer_(NULL),
		firstSample_(WaveformDataBuffer::WDB_INVALID_INDEX_),
		lastSample_(WaveformDataBuffer::WDB_INVALID_INDEX_),
		currSample_(WaveformDataBuffer::WDB_INVALID_INDEX_)
{
	CALL_TRACE("WaveformIntervalIter::WaveformIntervalIter(WaveformDataBuffer *)");

														// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~WaveformIntervalIter  [Destructor]
//
//@ Interface-Description
// Destroys a WaveformIntervalIter.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

WaveformIntervalIter::~WaveformIntervalIter(void)
{
	CALL_TRACE("WaveformIntervalIter::WaveformIntervalIter(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: attachToBuffer
//
//@ Interface-Description
// Attaches the iterator to its waveform data buffer.  Passed a pointer to the
// waveform data buffer.
//---------------------------------------------------------------------
//@ Implementation-Description
// Store the passed in pointer and reset the interval.
//---------------------------------------------------------------------
//@ PreCondition
// Parameter must be non-null.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
WaveformIntervalIter::attachToBuffer(WaveformDataBuffer *pWaveformDataBuffer)
{
	CALL_TRACE("WaveformIntervalIter::attachToBuffer(WaveformDataBuffer *pWaveformDataBuffer)");

	SAFE_CLASS_PRE_CONDITION(NULL != pWaveformDataBuffer);

	// Store waveform data buffer pointer
	pWaveformDataBuffer_ = pWaveformDataBuffer;

	// Initialize interval
	reset();											// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: beginInterval
//
//@ Interface-Description
// Points the start of the iterator's interval to the first sample in the
// attached waveform data buffer.
//---------------------------------------------------------------------
//@ Implementation-Description
// Set firstSample_ equal to pWaveformDataBuffer_'s and invalidate the other
// indexes.
//---------------------------------------------------------------------
//@ PreCondition
// pWaveformDataBuffer_ must be non-null.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
WaveformIntervalIter::beginInterval(void)
{
	CALL_TRACE("WaveformIntervalIter::beginInterval(void)");

	SAFE_CLASS_PRE_CONDITION(NULL != pWaveformDataBuffer_);

	// Point the interval to the start of the waveform data buffer and
	// invalidate the end and current point of the interval.
	firstSample_ = pWaveformDataBuffer_->firstSample_;
	lastSample_  = WaveformDataBuffer::WDB_INVALID_INDEX_;
	currSample_	 = WaveformDataBuffer::WDB_INVALID_INDEX_;
														// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setToNextInterval
//
//@ Interface-Description
// Sets the interval to point to the 'next' interval in the data buffer.
// Specifically, this is the interval that begins one sample after the end of
// the current interval and ends at the latest sample in the waveform data
// buffer.  Used to access the latest unread interval of waveform data.
// Returns the success or failure to do so (failure occurs when the specified
// end is the same as the end of the waveform data buffer, i.e. this interval
// already includes the latest waveform data).  If the current interval does
// not point anywhere then the interval is set to the whole of the
// waveform buffer data.
//---------------------------------------------------------------------
//@ Implementation-Description
// Check for the failure conditions first and return FAILURE if found.
// Otherwise point firstSample_ to the sample after lastSample_ and lastSample_
// to be the same as pWaveformDataBuffer_'s.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SigmaStatus
WaveformIntervalIter::setToNextInterval(void)
{
	CALL_TRACE("WaveformIntervalIter::setToNextInterval(void)");

	SigmaStatus returnStatus = SUCCESS;

	// Check to see if the current interval is valid
	if ((WaveformDataBuffer::WDB_INVALID_INDEX_ == firstSample_)
		|| (WaveformDataBuffer::WDB_INVALID_INDEX_ == lastSample_))
	{													// $[TI1]
		// It's not so set it to point to all the accumulated buffer data (if
		// any).
		beginInterval();
		endInterval();

		// If the interval is still not valid then return failure.
		if ((WaveformDataBuffer::WDB_INVALID_INDEX_ == firstSample_)
			|| (WaveformDataBuffer::WDB_INVALID_INDEX_ == lastSample_))
		{									// $[TI2]
			returnStatus = FAILURE;
		}												// $[TI3]
	}
	// If the interval is already pointing to the end of data then fail.
	else if (pWaveformDataBuffer_->lastSample_ == lastSample_)
	{													// $[TI4]
		returnStatus = FAILURE;
	}
	else
	{													// $[TI5]
		// Point the start to the point beyond the current interval end.
		firstSample_ = lastSample_ + 1;

		// Check for wraparound
		if (firstSample_ >= NUM_WAVEFORM_SAMPLES)
		{												// $[TI6]
			firstSample_ = 0;
		}												// $[TI7]

		lastSample_ = pWaveformDataBuffer_->lastSample_;
	}

	return (returnStatus);								// $[TI8], $[TI9]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: beginNextInterval
//
//@ Interface-Description
// Points the start of the iterval to the point in waveform data buffer which
// is one beyond the current end of the iterval.  The end of the iterval
// is invalidated.
//---------------------------------------------------------------------
//@ Implementation-Description
// Point firstSample_ to lastSample plus 1 and adjust for wraparound.
// Invalidate lastSample_.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
WaveformIntervalIter::beginNextInterval(void)
{
	CALL_TRACE("WaveformIntervalIter::beginNextInterval(void)");

	SAFE_CLASS_PRE_CONDITION(NULL != pWaveformDataBuffer_);
	SAFE_CLASS_PRE_CONDITION(WaveformDataBuffer::WDB_INVALID_INDEX_ !=
																lastSample_);

	// Point the start to the point beyond the current interval end.
	firstSample_ = lastSample_ + 1;

	// Check for wraparound
	if (firstSample_ >= NUM_WAVEFORM_SAMPLES)
	{													// $[TI1]
		firstSample_ = 0;
	}													// $[TI2]

	// Invalidate end
	lastSample_ = WaveformDataBuffer::WDB_INVALID_INDEX_;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: beginIntervalAtOffset
//
//@ Interface-Description
// Sets a new interval beginning at an offset from the current point in the
// interval.  The offset is the only parameter and this is specified in
// milliseconds.  Knowing that waveform samples are measured every
// MSECS_PER_WAVEFORM_SAMPLE milliseconds we can determine how many samples to
// offset from the current point.  The offset can be positive or negative
// corresponding to going forward or rewinding.  The end point of the interval
// cannot be crossed.  If the offset is such that it would cause the end point
// to be crossed then the beginning is moved as far as possible and FAILURE is
// returned indicating failure to move the beginning the desired amount.
// SUCCESS is returned otherwise.
//
// The current sample in the interval is always set to the first sample
// in the interval.
//---------------------------------------------------------------------
//@ Implementation-Description
// First calculate what's the maximum offset back or forward that we can move
// the beginning and then offset the beginning by the desired number of samples
// but limited by the maximum offset.  Return SUCCESS if we were able to
// offset by the desired amount, otherwise return FAILURE.
//---------------------------------------------------------------------
//@ PreCondition
// We must be attached to a waveform data buffer, firstSample_, lastSample_ and
// currSample_ must be valid and the offsetMSecs parameter must be divisible by
// MSECS_PER_WAVEFORM_SAMPLE.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SigmaStatus
WaveformIntervalIter::beginIntervalAtOffset(Int32 offsetMSecs)
{
	CALL_TRACE("WaveformIntervalIter::beginIntervalAtOffset(Int32 "
		"offsetMSecs)");
		
	SAFE_CLASS_PRE_CONDITION(NULL != pWaveformDataBuffer_);
	SAFE_CLASS_PRE_CONDITION( (WaveformDataBuffer::WDB_INVALID_INDEX_ != firstSample_) );
	SAFE_CLASS_PRE_CONDITION( (WaveformDataBuffer::WDB_INVALID_INDEX_ != lastSample_) );
	SAFE_CLASS_PRE_CONDITION( (WaveformDataBuffer::WDB_INVALID_INDEX_ != currSample_) );
	SAFE_CLASS_PRE_CONDITION(0 == offsetMSecs % MSECS_PER_WAVEFORM_SAMPLE);

	Int32 desiredSamplesToOffset = offsetMSecs / MSECS_PER_WAVEFORM_SAMPLE;
								// Divide number of millisecs per waveform
								// sample into offsetMSecs to get the number
								// of desired samples to offset
	Int32 samplesToOffset;		// This is the number of samples we'll really
								// offset by (can be less than desired number
								// if we hit the limit).
	Int32 limit;				// The biggest number of samples that we can
								// offset by.

	// Calculate what our limit is, i.e. what is the maximum that we can
	// go forward or rewind.  Check first if it's forward or backward that
	// we have to go.
	if (desiredSamplesToOffset >= 0)
	{													// $[TI1]
		// Calculate the farthest we can go forward, which is essentially
		// the position of the last sample in the interval minus the current
		// sample (note: we add NUM_WAVEFORM_SAMPLES and get the modulus here
		// so as to keep the limit to a number between 0 and
		// NUM_WAVEFORM_SAMPLES - 1).
		limit = (lastSample_ - currSample_ + NUM_WAVEFORM_SAMPLES)
													% NUM_WAVEFORM_SAMPLES;
	}
	else
	{													// $[TI2]
		// Calculate the farthest we can rewind, as a negative offset
		limit = - ((currSample_ - pWaveformDataBuffer_->firstSample_
							+ NUM_WAVEFORM_SAMPLES) % NUM_WAVEFORM_SAMPLES);
	}

	// Now set the samplesToOffset value to the desired value but limited
	// by 'limit'.
	samplesToOffset = ABS_VALUE(desiredSamplesToOffset) > ABS_VALUE(limit) ?
							limit : desiredSamplesToOffset;
														// $[TI3], $[TI4]

	// Set firstSample_ to the desired offset from currSample_
	firstSample_ = (currSample_ + samplesToOffset + NUM_WAVEFORM_SAMPLES)
													% NUM_WAVEFORM_SAMPLES;
	
	currSample_ = firstSample_;			// Make current the first sample

	// Return SUCCESS if we offset by the desired number of samples,
	// FAILURE otherwise.
	return (samplesToOffset == desiredSamplesToOffset ? SUCCESS : FAILURE);
														// $[TI5], $[TI6]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: endIntervalAtOffset
//
//@ Interface-Description
// Sets a new interval end at an offset from the current point in the interval.
// The offset is the only parameter and this is specified in milliseconds.
// Knowing that waveform samples are measured every MSECS_PER_WAVEFORM_SAMPLE
// milliseconds we can determine how many samples to offset from the current
// point.  The offset can be positive or negative corresponding to going
// forward or rewinding.  The starting point of the interval cannot be crossed.
// If the offset is such that it would cause the starting point to be crossed
// then the end is moved as far as possible and FAILURE is returned indicating
// failure to move the end the desired amount.  SUCCESS is returned otherwise.
//
// The current sample in the interval is always set to the last sample
// in the interval.
//---------------------------------------------------------------------
//@ Implementation-Description
// First calculate what's the maximum offset back or forward that we can move
// the end and then offset the end by the desired number of samples but limited
// by the maximum offset.  Return SUCCESS if we were able to offset by the
// desired amount, otherwise return FAILURE.
//---------------------------------------------------------------------
//@ PreCondition
// We must be attached to a waveform data buffer, firstSample_, lastSample_ and
// currSample_ must be valid and the offsetMSecs parameter must be divisible by
// MSECS_PER_WAVEFORM_SAMPLE.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SigmaStatus
WaveformIntervalIter::endIntervalAtOffset(Int32 offsetMSecs)
{
	CALL_TRACE("WaveformIntervalIter::beginIntervalAtOffset(Int32 "
															"offsetMSecs)");
	
	SAFE_CLASS_PRE_CONDITION(NULL != pWaveformDataBuffer_);
	SAFE_CLASS_PRE_CONDITION(
				(WaveformDataBuffer::WDB_INVALID_INDEX_ != firstSample_) &&
				(WaveformDataBuffer::WDB_INVALID_INDEX_ != lastSample_) &&
				(WaveformDataBuffer::WDB_INVALID_INDEX_ != currSample_));
	SAFE_CLASS_PRE_CONDITION(0 == offsetMSecs % MSECS_PER_WAVEFORM_SAMPLE);

	Int32 desiredSamplesToOffset = offsetMSecs / MSECS_PER_WAVEFORM_SAMPLE;
								// Divide number of millisecs per waveform
								// sample into offsetMSecs to get the number
								// of desired samples to offset
	Int32 samplesToOffset;		// This is the number of samples we'll really
								// offset by (can be less than desired number
								// if we hit a limit).
	Int32 limit;				// The biggest number of samples that we can
								// offset by.

	// Calculate what our limit is, i.e. what is the maximum that we can
	// go forward or rewind.  Check first if it's forward or backward that
	// we have to go.
	if (desiredSamplesToOffset >= 0)
	{													// $[TI1]
		// Calculate the farthest we can go forward, which is essentially
		// the position of the last sample in the interval minus the current
		// sample (note: we add NUM_WAVEFORM_SAMPLES and get the modulus here
		// so as to keep the limit to a number between 0 and
		// NUM_WAVEFORM_SAMPLES - 1).
		limit = (pWaveformDataBuffer_->lastSample_ - currSample_
							+ NUM_WAVEFORM_SAMPLES ) % NUM_WAVEFORM_SAMPLES;
	}
	else
	{													// $[TI2]
		// Calculate the farthest we can rewind, as a negative offset
		limit = - ((currSample_ - firstSample_ + NUM_WAVEFORM_SAMPLES)
													% NUM_WAVEFORM_SAMPLES);
	}

	// Now set the samplesToOffset value to the desired value but limited
	// by 'limit'.
	samplesToOffset = ABS_VALUE(desiredSamplesToOffset) > ABS_VALUE(limit) ?
							limit : desiredSamplesToOffset;
														// $[TI3], $[TI4]

	// Set firstSample_ to the desired offset from currSample_
	lastSample_ = (currSample_ + samplesToOffset + NUM_WAVEFORM_SAMPLES)
													% NUM_WAVEFORM_SAMPLES;
	
	currSample_ = lastSample_;			// Make current the last sample

	// Return SUCCESS if we offset by the desired number of samples,
	// FAILURE otherwise.
	return (samplesToOffset == desiredSamplesToOffset ? SUCCESS : FAILURE);
														// $[TI5], $[TI6]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: goFirst
//
//@ Interface-Description
// Moves the current value pointer to the first point in the interval,
// if possible.  Returns SUCCESS if the move was successful, FAILURE
// otherwise.
//---------------------------------------------------------------------
//@ Implementation-Description
// If firstSample_ is a valid value then set currSample_ to it and return
// SUCCESS, otherwise just return FAILURE.
//---------------------------------------------------------------------
//@ PreCondition
// We must be attached to a waveform data buffer.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SigmaStatus
WaveformIntervalIter::goFirst(void)
{
	CALL_TRACE("WaveformIntervalIter::goFirst(void)");

	SAFE_CLASS_PRE_CONDITION(NULL != pWaveformDataBuffer_);

	// Go to the first sample if there is one
	if (firstSample_ != WaveformDataBuffer::WDB_INVALID_INDEX_)
	{													// $[TI1]
		currSample_ = firstSample_;
		return (SUCCESS);
	}													// $[TI2]

	// Invalid interval
	return (FAILURE);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: goLast
//
//@ Interface-Description
// Moves the current value pointer to the last point in the interval,
// if possible.  Returns SUCCESS if the move was successful, FAILURE
// otherwise.
//---------------------------------------------------------------------
//@ Implementation-Description
// If lastSample_ is a valid value then set currSample_ to it and return
// SUCCESS, otherwise just return FAILURE.
//---------------------------------------------------------------------
//@ PreCondition
// We must be attached to a waveform data buffer.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SigmaStatus
WaveformIntervalIter::goLast(void)
{
	CALL_TRACE("WaveformIntervalIter::goLast(void)");

	SAFE_CLASS_PRE_CONDITION(NULL != pWaveformDataBuffer_);

	// Go to the last sample if there is one
	if (lastSample_ != WaveformDataBuffer::WDB_INVALID_INDEX_)
	{													// $[TI1]
		currSample_ = lastSample_;
		return (SUCCESS);
	}													// $[TI2]

	// Invalid interval
	return (FAILURE);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: goNext
//
//@ Interface-Description
// Moves the current value pointer to the next point in the interval,
// if possible.  Returns SUCCESS if the move was successful, FAILURE
// otherwise.  Failure is also returned if firstSample_, lastSample
// and currSample_ are invalid
//---------------------------------------------------------------------
//@ Implementation-Description
// If currSample_ is already at the last sample then we can't go to the
// next point, invalidate currSample_ and return FAILURE.  Otherwise
// move currSample_ forward one and return SUCCESS.
//---------------------------------------------------------------------
//@ PreCondition
// We must be attached to a valid waveform data buffer.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SigmaStatus
WaveformIntervalIter::goNext(void)
{
	CALL_TRACE("WaveformIntervalIter::goNext(void)");

	SAFE_CLASS_PRE_CONDITION(NULL != pWaveformDataBuffer_);

	// if the first, last or current sample is invalid just return failure 
	// otherwise plots will continuously loop forever.
	if ((WaveformDataBuffer::WDB_INVALID_INDEX_ == firstSample_) ||
		(WaveformDataBuffer::WDB_INVALID_INDEX_ == lastSample_) ||
		(WaveformDataBuffer::WDB_INVALID_INDEX_ == currSample_))
	{
		return (FAILURE);
	}

	// If we're at the end of the interval then we can't go to the next point
	if (currSample_ == lastSample_)
	{													// $[TI1]
		currSample_ = WaveformDataBuffer::WDB_INVALID_INDEX_;
		return (FAILURE);
	}													// $[TI2]

	// Go to the next point
	currSample_++;
	if (currSample_ >= NUM_WAVEFORM_SAMPLES)
	{													// $[TI3]
		currSample_ = 0;
	}													// $[TI4]

	return (SUCCESS);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: goPrev
//
//@ Interface-Description
// Moves the current value pointer to the previous point in the interval,
// if possible.  Returns SUCCESS if the move was successful, FAILURE
// otherwise.  Failure is also returned if firstSample_, lastSample
// and currSample_ are invalid
//---------------------------------------------------------------------
//@ Implementation-Description
// If currSample_ is already at the first sample then we can't go to the
// previous point, invalidate currSample_ and return FAILURE.  Otherwise
// move currSample_ back one and return SUCCESS.
//---------------------------------------------------------------------
//@ PreCondition
// We must be attached to a waveform data buffer, firstSample_, lastSample_ and
// currSample_ must be valid.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SigmaStatus
WaveformIntervalIter::goPrev(void)
{
	CALL_TRACE("WaveformIntervalIter::goPrev(void)");

	SAFE_CLASS_PRE_CONDITION(NULL != pWaveformDataBuffer_);

	// if the first, last or current sample is invalid just return failure 
	// otherwise plots will continuously loop forever.
	if ((WaveformDataBuffer::WDB_INVALID_INDEX_ == firstSample_) ||
		(WaveformDataBuffer::WDB_INVALID_INDEX_ == lastSample_) ||
		(WaveformDataBuffer::WDB_INVALID_INDEX_ == currSample_))
	{
		return (FAILURE);
	}

	// If we're at the start of the interval then we can't go to the previous
	// point
	if (currSample_ == firstSample_)
	{													// $[TI1]
		currSample_ = WaveformDataBuffer::WDB_INVALID_INDEX_;
		return (FAILURE);
	}													// $[TI2]

	// Go to the previous point
	currSample_--;
	if (currSample_ < 0)
	{													// $[TI3]
		currSample_ = NUM_WAVEFORM_SAMPLES - 1;
	}													// $[TI4]

	return (SUCCESS);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getBoundedValue
//
//@ Interface-Description
// Returns the floating point value of the specified patient datum in the
// sample pointed to by the current sample in the iterator.
//---------------------------------------------------------------------
//@ Implementation-Description
// Depending on which patient datum id is passed return the corresponding
// floating point data member of the current sample pointed to in the waveform
// data buffer.
//---------------------------------------------------------------------
//@ PreCondition
// We must be attached to a waveform data buffer and currSample_ must be valid.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Real32
WaveformIntervalIter::getBoundedValue(
						PatientDataId::PatientItemId patientDataId)
{
	CALL_TRACE("WaveformIntervalIter::getBoundedValue("
						"PatientDataId::PatientDataIdType patientDataId)");

	SAFE_CLASS_PRE_CONDITION(pWaveformDataBuffer_);
	SAFE_CLASS_PRE_CONDITION(WaveformDataBuffer::WDB_INVALID_INDEX_ !=
																currSample_);
	Setting*  pSetting = SettingsMgr::GetSettingPtr(SettingId::PROX_ENABLED);
	const DiscreteValue  PROX_ENABLED_VALUE = pSetting->getAcceptedValue();

	Boolean isProxInop = UpperSubScreenArea::GetWaveformsSubScreen()->isProxInop();       
	Boolean isProxInStartupMode = UpperSubScreenArea::GetWaveformsSubScreen()->isProxInStartupMode();  


	switch (patientDataId)
	{
	case PatientDataId::CIRCUIT_PRESS_ITEM:				// $[TI1]

		// $[PX01004] Add the prox symbol when PROX is enabled.
		if ((PROX_ENABLED_VALUE == ProxEnabledValue::PROX_ENABLED) && !isProxInop && !isProxInStartupMode)
		{
			return (pWaveformDataBuffer_->samples_[currSample_].proxCircuitPressure);
		}
		else
		{
			return (pWaveformDataBuffer_->samples_[currSample_].circuitPressure);
		}

	case PatientDataId::NET_FLOW_ITEM:					// $[TI2]
		return (pWaveformDataBuffer_->samples_[currSample_].netFlow);

	case PatientDataId::NET_VOL_ITEM:					// $[TI3]
		return (pWaveformDataBuffer_->samples_[currSample_].netVolume);

	case PatientDataId::LUNG_PRESS_ITEM:				// $[TI4]
		return (pWaveformDataBuffer_->samples_[currSample_].lungPressure);

	case PatientDataId::LUNG_FLOW_ITEM:					// $[TI5]
		return (pWaveformDataBuffer_->samples_[currSample_].lungFlow);

	case PatientDataId::LUNG_VOLUME_ITEM:				// $[TI6]
		return (pWaveformDataBuffer_->samples_[currSample_].lungVolume);
		
	default:
		return (0.0);
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getDiscreteValue
//
//@ Interface-Description
// Returns the discrete point value of the specified patient data id in the
// sample pointed to by the current sample in the iterator.
//---------------------------------------------------------------------
//@ Implementation-Description
// Depending on which patient datum id is passed return the corresponding
// discrete data member of the current sample pointed to in the waveform
// data buffer.
//---------------------------------------------------------------------
//@ PreCondition
// We must be attached to a waveform data buffer and currSample_ must be valid.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Int32
WaveformIntervalIter::getDiscreteValue(
						PatientDataId::PatientItemId patientDataId)
{
	CALL_TRACE("WaveformIntervalIter::getBoundedValue("
						"PatientDataId::PatientItemId)");

	SAFE_CLASS_PRE_CONDITION(NULL != pWaveformDataBuffer_);
	SAFE_CLASS_PRE_CONDITION(WaveformDataBuffer::WDB_INVALID_INDEX_ !=
																currSample_);

	switch (patientDataId)
	{
	case PatientDataId::BREATH_PHASE_ITEM:				// $[TI1]
		return ((Int32)pWaveformDataBuffer_->samples_[currSample_].bdPhase);

	case PatientDataId::AUTOZERO_ACTIVE_ITEM:			// $[TI2]
		return ((Int32)pWaveformDataBuffer_->
									samples_[currSample_].isAutozeroActive);

	case PatientDataId::BREATH_TYPE_ITEM:				// $[TI3]
		return ((Int32)pWaveformDataBuffer_->samples_[currSample_].breathType);

	case PatientDataId::IS_PEEP_RECOVERY_ITEM:			// $[TI4]
		return ((Int32)pWaveformDataBuffer_->
									samples_[currSample_].isPeepRecovery);

	case PatientDataId::MODE_SETTING_ITEM :				// $[TI5]
		return ((Int32)pWaveformDataBuffer_->
									samples_[currSample_].modeSetting);

	case PatientDataId::SUPPORT_TYPE_SETTING_ITEM :		// $[TI6]
		return ((Int32)pWaveformDataBuffer_->
									samples_[currSample_].supportTypeSetting);

	case PatientDataId::IS_APNEA_ACTIVE_ITEM:			// $[TI7]
		return ((Int32)pWaveformDataBuffer_->
									samples_[currSample_].isApneaActive);

	case PatientDataId::IS_ERROR_STATE_ITEM:			// $[TI8]
		return ((Int32)pWaveformDataBuffer_->
									samples_[currSample_].isErrorState);

	default:
		return (0);
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
WaveformIntervalIter::SoftFault(const SoftFaultID  softFaultID,
		   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, WAVEFORMINTERVALITER,
									lineNumber, pFileName, pPredicate);
}

