#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.    No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S     D E S C R I P T I O N ====
//@ Class:  SmSensorMediator - Collects sensor data for Service Mode
//---------------------------------------------------------------------
//@ Interface-Description
//	This class collects sensor data for use by the Service Mode tests
//	via the SensorMediator class.
//---------------------------------------------------------------------
//@ Rationale
//	This class is required to collect sensor data for the Service Mode
//	tests.
//---------------------------------------------------------------------
//@ Implementation-Description
//	The method newCycle() executes each cycle and collects sensor
//	data.
//---------------------------------------------------------------------
//@ Fault-Handling
//	n/a
//---------------------------------------------------------------------
//@ Restrictions
//	Only one instance of this class may be created.
//---------------------------------------------------------------------
//@ Invariants
//	none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/SmSensorMediator.ccv   25.0.4.0   19 Nov 2013 14:23:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006  By: dosman    Date: 16-Apr-1999     DR Number: 5322
//  Project:  ATC
//  Description:
//	ATC initial release.
//
//  Revision: 005  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 004  By:  quf    Date: 15-Jan-1998    DR Number: DCS 2720
//       Project:  Sigma (840)
//       Description:
//			Added update for secondary Pexp sensor object in method newCycle().
//
//  Revision: 003  By:  quf    Date: 01-Dec-1997    DR Number: DCS 2656
//       Project:  Sigma (840)
//       Description:
//			Added update for low voltage reference sensor in method newCycle().
//
//  Revision: 002  By:  quf    Date: 23-May-1997    DR Number: DCS 2131
//       Project:  Sigma (840)
//       Description:
//            Fixed global reference names (i.e. "r" changed to "R").
//
//  Revision: 001  By:  mad    Date: 12-Sep-1995    DR Number: none
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//=====================================================================

#include "SmSensorMediator.hh"

//@ Usage-Classes

#ifdef SIGMA_UNIT_TEST
#include "FakeSensorMediator.hh"
#include "FakeFlowSensor.hh"
#include "FakeExhFlowSensor.hh"
#else
#include "SensorMediator.hh"
#include "ExhFlowSensor.hh"
#endif // endelse !SIGMA_UNIT_TEST

#include "Sensor.hh"
#include "PressureSensor.hh"
#include "MainSensorRefs.hh"
#include "MiscSensorRefs.hh"

#include "SmRefs.hh"

#ifdef SIGMA_UNIT_TEST
# include "SmSensorMediator_UT.hh"
#endif  // SIGMA_UNIT_TEST

//@ End-Usage

//@ Code...

//=====================================================================
//
//    Public Methods...
//
//=====================================================================

//============================ M E T H O D     D E S C R I P T I O N ====
//@ Method: SmSensorMediator()    [Default Constructor]
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

SmSensorMediator::SmSensorMediator(void)
{
    CALL_TRACE("SmSensorMediator::SmSensorMediator(void)");
}


//============================ M E T H O D     D E S C R I P T I O N ====
//@ Method: ~SmSensorMediator()    [Destructor]
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

SmSensorMediator::~SmSensorMediator(void)
{
    CALL_TRACE("SmSensorMediator::~SmSensorMediator(void)");
}


//============================ M E T H O D     D E S C R I P T I O N ====
//@ Method: newCycle(void)
//
//@ Interface-Description
//	This method is executed every cycle to collect sensor data via
//	the SensorMediator and FlowSensor classes.  No arguments and no
//	return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Scan the secondary sensors
//	Scan the primary sensors
//	Scan the exh O2 flow sensor
//	Scan the low voltage reference sensor
//	Scan the secondary exp pressure sensor
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

void
SmSensorMediator::newCycle(void)
{
// $[TI1]
	CALL_TRACE("SmSensorMediator::newCycle(void)");

	// The order of the SensorMediator method calls are important:
	// newSecondaryCycle() updates the temperature sensors and
	// newCycle() updates the flow sensors which are dependent
	// on the temperature of gas
// E600 BDIO	RSensorMediator.newSecondaryCycle();
	RSensorMediator.newCycle();

	// Update the exh O2 flow sensor, low voltage reference sensor,
	// and second exh pressure sensor separately because
	// they're not updated in the above methods
// E600 BDIO    RExhO2FlowSensor.updateValue();

	// TODO E600 MS not sure if e600 supports this.
	//RLowVoltageReference.updateValue();
	//RSecondExhPressureSensor.updateValue();

#ifdef SIGMA_UNIT_TEST
	SmSensorMediator_UT::NewCyclePerformed = TRUE;
#endif  // SIGMA_UNIT_TEST

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
SmSensorMediator::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, SERVICE_MODE, SMSENSORMEDIATOR,
                          lineNumber, pFileName, pPredicate);
}


//=====================================================================
//
//    Protected Methods...
//
//=====================================================================


//=====================================================================
//
//    Private Methods...
//
//=====================================================================
