#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class:  SettingConstants - Defines subsystem-wide constants.
//---------------------------------------------------------------------
//@ Interface-Description
//  This structure provides a central repository for all Settings-wide
//  constants.  The constants include various scaling factors for some
//  settings (e.g., the scaling factor used for the new-patient value
//  of High Exhaled Minute Volume), general "state" constants (e.g., the
//  constant used to identify a "high" alarm as being "off"), and general
//  conversion factors (e.g., millisecond-to-minute conversion factor).
//---------------------------------------------------------------------
//@ Rationale
//  This structure provides a central repository for all Settings-wide
//  constants.
//---------------------------------------------------------------------
//@ Implementation-Description  
//  All of the constants are defined as static within a structure.
//  Therefore, all of the constants are defined, and initialized, within
//  the '.cc' file.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/SettingConstants.ccv   25.0.4.0   19 Nov 2013 14:27:42   pvcs  $
//
//@ Modification-Log
//
//  Revision: 020   By: mnr    Date: 23-Nov-2009    SCR Number: 6437
//  Project:  NEO
//  Description:
//		Newly added TIDAL_VOL_NEONATAL_MIN_SQUARE_FLOW_PATTERN_VALUE
//		is being initialized.
//
//  Revision: 019   By: mnr    Date: 21-Jul-2009    SCR Number: 6437
//  Project:  NEO
//  Description:
//		Newly added IBW_ABSOLUTE_MIN_NEO_VALUE is being initialized.
//
//  Revision: 018   By: mnr    Date: 10-Jul-2009    SCR Number: 6437
//  Project:  NEO
//  Description:
//	Initializing new SettingConstants with #defines for distinct lower 
//  tidal volume bounds based on MAND TYPE.
//
//  Revision: 017  By: mnr     Date: 24-Jun-2009    SCR Number: 6437
//  Project:  NEO
//  Description: 
//		IBW min soft bound lowered to 0.3f as part of 840 Neomode feature.
//      
//  Revision: 016  By: gdc    Date: 02-Mar-2009    SCR Number: 6160
//  Project:  S840
//  Description:
//      Enhanced to set this alarm limit OFF when set less than PEEP+5
//      in VC+ per modified SRS requirements.
//
//  Revision: 015   By: gdc    Date: 26-Jan-2009    SCR Number: 6461
//  Project:  840S
//  Description:
//      Implemented #define workaround for compiler bug initializing
//      static const structs.
//
//  Revision: 014   By: gdc    Date: 21-Oct-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal non-invasive CPAP.
//
//  Revision: 013   By: gdc    Date: 11-Aug-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support changing IBW while ventilating.
//  
//  Revision: 012  By: gdc    Date:  18-Feb-2005    DR Number: 6144
//  Project:  NIV1
//  Description:
//      Changes to support NIV.
//
//  Revision: 011  By: gdc    Date:  10-Feb-2005    DR Number: 6144
//  Project:  NIV1
//  Description:
//      Added low circuit pressure default = PEEP + 6
//
//  Revision: 010  By: gfu    Date:  11-Aug-2004    DR Number: 6132
//  Project:  PAV3
//  Description:
//      Modified code per SDCR #6132.  Also added keyword "Log" to the file header
//      as requested during code review.
//
//  Revision: 009  By: sah    Date:  07-Nov-2000    DR Number: 5789
//  Project:  VTPC
//  Description:
//      Added new peak flow min/max constants for use internally, and by
//      Breath-Delivery.
//
//  Revision: 008  By: sah    Date:  05-Oct-2000    DR Number: 5730
//  Project:  VTPC
//  Description:
//      Modified 'NP_PEAK_FLOW_NEO_IBW_SCALE' to new scale value,
//      and updated SRS mappings.
//
//  Revision: 007   By: sah    Date: 09-Feb-2000    DR Number: 5635
//  Project:  NeoMode
//  Description:
//      Eliminated IBW NEONATAL soft-bound constant, and changed hard
//      bound to 7kg.
//
//  Revision: 006   By: sah    Date: 29-Jun-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//      *  re-organized to allow for common formulas and values to
//         be defined once, but used throughout file
//      *  defined new, circuit-specific factors and values
//
//  Revision: 005   By: sah   Date:  04-Mar-1999    DR Number: 5322
//  Project:  ATC
//  Description:
//	Project-related changes:
//	*  added new ATC-specific constants
//
//  Revision: 004   By: dosman    Date: 11-Nov-1998    DR Number: 5252
//  Project:  BILEVEL
//  Description:
//	Removed SRS requirement number that no longer exists in SRS.
//
//  Revision: 003   By: dosman    Date: 24-Dec-1997    DR Number: 
//  Project:  BILEVEL
//  Description:
//      Initial BiLevel version.
//	Added symbolic constants.
//
//  Revision: 002   By: sah    Date: 11-Dec-1996    DR Number: 1589 & 1635
//  Project: Sigma (R8027)
//  Description:
//      Change display min/max values to match the SRS.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//      Initial version
//
//=====================================================================

#include "SettingConstants.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Static Data Definitions...
//
//=====================================================================

#define  NP_TIDAL_VOL_IBW_FACTOR_  Real32(7.25f)

#define  NP_HIGH_TIDAL_VOL_ALARM_FACTOR_  \
				Real32((NP_TIDAL_VOL_IBW_FACTOR_ * 1.30f))
#define  NP_LOW_TIDAL_VOL_ALARM_FACTOR_   \
				Real32((NP_TIDAL_VOL_IBW_FACTOR_ * 0.70f))

#define  NP_HIGH_MINUTE_VOL_ALARM_FACTOR_ \
		   Real32((NP_HIGH_TIDAL_VOL_ALARM_FACTOR_ * 0.001f))
#define  NP_LOW_MINUTE_VOL_ALARM_FACTOR_  \
		  Real32((NP_LOW_TIDAL_VOL_ALARM_FACTOR_ * 0.001f))

// $[02243] -- respiratory rate's new-patient values
// $[02091] -- apnea respiratory rate's new-patient values
#define  NP_RESP_RATE_NEO_VALUE_    Real32(20.0f)
#define  NP_RESP_RATE_PED_VALUE_    Real32(14.0f)
#define  NP_RESP_RATE_ADULT_VALUE_  Real32(10.0f)


//=====================================================================
//
//  Static Member Definitions...
//
//=====================================================================

//=====================================================================
//   Pressure-Based Breath Constants...
//=====================================================================

// $[BL02007]
// $[02167]
// $[02228]
// $[02238]
const Real32 SettingConstants::MAX_ALLOWED_PRESSURE_SETTING_VALUE = 90.0f;

// $[02228]
// $[02133]
const Real32 SettingConstants::MIN_NON_MAND_PRESS_BUFF = 7.0f;

// $[02133]
// $[02167]
// $[02228]
// $[02238]
// $[BL02007]
const Real32 SettingConstants::MIN_MAND_PRESS_BUFF = 2.0f;

// $[02167]
// $[02167]
// $[BL02007]
const Real32 SettingConstants::MIN_MAND_PRESS_DELTA = 5.0f;

// $[BL02008] -- High Peep New Patient Value
// $[02168] -- Inspiratory Pressure New Patient Value
// $[02059] -- Apnea Inspiratory Pressure New Patient Value
const Real32 SettingConstants::DEFAULT_MAND_PRESS_DELTA = 15.0f;

// $[02229]
const Real32 SettingConstants::NP_PEEP_VALUE = 3.0f;

// $[NI02002]
const Real32 SettingConstants::LOW_PRESS_PEEP_DELTA = 5.0f;

//====================================================================
//  Respiratory Rate Constants...
//====================================================================

// $[02243] -- respiratory rate's new-patient values
// $[02091] -- apnea respiratory rate's new-patient values
const Real32  SettingConstants::NP_RESP_RATE_NEO_VALUE   =
					NP_RESP_RATE_NEO_VALUE_; // 1/min
const Real32  SettingConstants::NP_RESP_RATE_PED_VALUE   =
					NP_RESP_RATE_PED_VALUE_; // 1/min
const Real32  SettingConstants::NP_RESP_RATE_ADULT_VALUE =
				      NP_RESP_RATE_ADULT_VALUE_; // 1/min


//====================================================================
//  Ideal Body Weight Constants...
//====================================================================

//---------------------------------------------------------------------
// Neonatal IBW limits...
//---------------------------------------------------------------------

// $[02157]   -- IBW's range...
// $[NE02004] -- IBW's soft bounds...
const Real32  SettingConstants::IBW_ABSOLUTE_MIN_NEO_VALUE = 0.3f; // kg
const Real32  SettingConstants::IBW_ABSOLUTE_MIN_VALUE     = 0.5f; // kg
const Real32  SettingConstants::IBW_NEO_CCT_HARD_MAX_VALUE = 7.0f; // kg

//---------------------------------------------------------------------
// Pediatric IBW limits...
//---------------------------------------------------------------------

// $[02157]   -- IBW's range...
// $[NE02004] -- IBW's soft bounds...
const Real32  SettingConstants::IBW_PED_CCT_HARD_MIN_VALUE =  3.5f; // kg
const Real32  SettingConstants::IBW_PED_CCT_SOFT_MIN_VALUE =  7.0f; // kg
const Real32  SettingConstants::IBW_PED_CCT_SOFT_MAX_VALUE = 24.0f; // kg
const Real32  SettingConstants::IBW_PED_CCT_HARD_MAX_VALUE = 35.0f; // kg

//---------------------------------------------------------------------
// Adult IBW limits...
//---------------------------------------------------------------------

// $[02157]   -- IBW's range...
// $[NE02004] -- IBW's soft bounds...
const Real32  SettingConstants::IBW_ADULT_CCT_HARD_MIN_VALUE =   7.0f; // kg
const Real32  SettingConstants::IBW_ADULT_CCT_SOFT_MIN_VALUE =  25.0f; // kg
const Real32  SettingConstants::IBW_ABSOLUTE_MAX_VALUE       = 150.0f; // kg


//=====================================================================
//  Peak Inspiratory Flow Constants...
//=====================================================================

// $[NE02000] -- NEONATAL transition of peak inspiratory flow to VC or VC+...
// $[NE02007] -- NEONATAL transition of apnea peak inspiratory flow to VC...
// $[02084]   -- NEONATAL new-patient scaling factor of apnea peak flow...
// $[02219]   -- NEONATAL new-patient scaling factor of peak inspiratory flow...
const Real32  SettingConstants::NP_PEAK_FLOW_NEO_IBW_SCALE   = 0.750f;

// $[NE02001] -- PEDIATRIC transition of peak inspiratory flow to VC or VC+...
// $[NE02008] -- PEDIATRIC transition of apnea peak inspiratory flow to VC...
// $[02084]   -- PEDIATRIC new-patient scaling factor of apnea peak flow...
// $[02219]   -- PEDIATRIC new-patient scaling factor of peak flow...
const Real32  SettingConstants::NP_PEAK_FLOW_PED_IBW_SCALE   = 0.572f;

// $[NE02002] -- ADULT transition of peak inspiratory flow to VC or VC+...
// $[NE02009] -- ADULT transition of apnea peak inspiratory flow to VC...
// $[02084]   -- ADULT new-patient scaling factor of apnea peak flow...
// $[02219]   -- ADULT new-patient scaling factor of peak inspiratory flow...
const Real32  SettingConstants::NP_PEAK_FLOW_ADULT_IBW_SCALE = 0.435f;

// $[02083]   -- NEONATAL apnea peak inspiratory flow range...
// $[02218]   -- NEONATAL peak inspiratory flow range...
const Real32  SettingConstants::MIN_PEAK_FLOW_NEO_VALUE =  1.0f;
const Real32  SettingConstants::MAX_PEAK_FLOW_NEO_VALUE = 30.0f;

// $[02083]   -- PEDIATRIC apnea peak inspiratory flow range...
// $[02218]   -- PEDIATRIC peak inspiratory flow range...
const Real32  SettingConstants::MIN_PEAK_FLOW_PED_ADULT_VALUE = 3.0f;

// $[02083]   -- PEDIATRIC apnea peak inspiratory flow range...
// $[02218]   -- PEDIATRIC peak inspiratory flow range...
const Real32  SettingConstants::MAX_PEAK_FLOW_PED_VALUE = 60.0f;

// $[02083]   -- ADULT apnea peak inspiratory flow range...
// $[02218]   -- ADULT peak inspiratory flow range...
const Real32  SettingConstants::MAX_PEAK_FLOW_ADULT_VALUE = 150.0f;


//=====================================================================
//  Tidal Volume Constants...
//=====================================================================

// $[02022] -- transition of tidal volume from PCV to VCV...
// $[02024] -- transition of apnea tidal volume from PCV to VCV...
// $[02096] -- new-patient scaling factor of apnea tidal volume...
// $[02252] -- new-patient scaling factor of tidal volume...
const Real32  SettingConstants::NP_TIDAL_VOL_IBW_SCALE  =
					NP_TIDAL_VOL_IBW_FACTOR_; // mL/kg...

// $[02095] -- minimum range of apnea tidal volume...
// $[02251] -- minimum range of tidal volume...
const Real32  SettingConstants::TIDAL_VOL_STANDARD_MIN_VALUE = 25.0f; // mL...
const Real32  SettingConstants::TIDAL_VOL_NEONATAL_MIN_VALUE_NON_VC_PLUS =  
					DEFINED_TIDAL_VOL_NEONATAL_MIN_VALUE_NON_VC_PLUS; // mL...

const Real32  SettingConstants::TIDAL_VOL_NEONATAL_MIN_VALUE_VC_PLUS =  
					DEFINED_TIDAL_VOL_NEONATAL_MIN_VALUE_VC_PLUS; // mL...

const Real32  SettingConstants::TIDAL_VOL_NEONATAL_MIN_VALUE_MISC =  
					DEFINED_TIDAL_VOL_NEONATAL_MIN_VALUE_MISC; // mL...

const Real32  SettingConstants::TIDAL_VOL_NEONATAL_MIN_SQUARE_FLOW_PATTERN_VALUE =
					DEFINED_TIDAL_VOL_NEONATAL_MIN_SQUARE_FLOW_PATTERN_VALUE; //mL...

// $[02022] -- transition of tidal volume from PCV to VCV...
// $[02024] -- transition of apnea tidal volume from PCV to VCV...
// $[02095] -- minimum range of apnea tidal volume...
// $[02251] -- minimum range of tidal volume...
const Real32  SettingConstants::MAX_TIDAL_VOL_IBW_SCALE = 45.70f; // mL/kg...
const Real32  SettingConstants::MIN_TIDAL_VOL_IBW_SCALE =  1.16f; // mL/kg...

// $[NE02006] -- tidal volume's soft bound limits...
// $[NE02003] -- apnea tidal volume's soft bound limits...
const Real32  SettingConstants::MAX_TIDAL_VOL_SOFT_IBW_SCALE = 15.0f; // mL/kg
const Real32  SettingConstants::MIN_TIDAL_VOL_SOFT_IBW_SCALE =  3.0f; // mL/kg


// $[TC02039] -- new patient high Vti...
const Real32  SettingConstants::NP_HIGH_INSP_TIDAL_VOL_IBW_SCALE = 15.0f; // mL/kg
// $[TC02038] -- high Vti limit range...
const Real32  SettingConstants::MIN_HIGH_INSP_TIDAL_VOL_IBW_SCALE = 5.0f; // mL/kg

//=====================================================================
//  High Exhaled Minute Volume Constants...
//=====================================================================

//---------------------------------------------------------------------
// $[02139] -- new-patient scaling factor of high exhaled minute volume...
//---------------------------------------------------------------------

const Real32  SettingConstants::NP_HIGH_EXH_MINUTE_VOL_NEO_SCALE =   // L/kg-min
	  (NP_RESP_RATE_NEO_VALUE_ * NP_HIGH_MINUTE_VOL_ALARM_FACTOR_);

const Real32  SettingConstants::NP_HIGH_EXH_MINUTE_VOL_PED_SCALE =   // L/kg-min
	  (NP_RESP_RATE_PED_VALUE_ * NP_HIGH_MINUTE_VOL_ALARM_FACTOR_);

const Real32  SettingConstants::NP_HIGH_EXH_MINUTE_VOL_ADULT_SCALE = // L/kg-min
	  (NP_RESP_RATE_ADULT_VALUE_ * NP_HIGH_MINUTE_VOL_ALARM_FACTOR_);


//=====================================================================
//  Low Exhaled Minute Volume Constants...
//=====================================================================

//---------------------------------------------------------------------
// $[02183] -- new-patient scaling factor of low exh. minute tidal volume...
//---------------------------------------------------------------------

const Real32  SettingConstants::NP_LOW_EXH_MINUTE_VOL_NEO_SCALE =   // L/kg-min
	    (NP_RESP_RATE_NEO_VALUE_ * NP_LOW_MINUTE_VOL_ALARM_FACTOR_);

const Real32  SettingConstants::NP_LOW_EXH_MINUTE_VOL_PED_SCALE =   // L/kg-min
	    (NP_RESP_RATE_PED_VALUE_ * NP_LOW_MINUTE_VOL_ALARM_FACTOR_);

const Real32  SettingConstants::NP_LOW_EXH_MINUTE_VOL_ADULT_SCALE = // L/kg-min
	    (NP_RESP_RATE_ADULT_VALUE_ * NP_LOW_MINUTE_VOL_ALARM_FACTOR_);


//=====================================================================
//  High Exhaled Tidal Volume Constants...
//=====================================================================

// $[02143] -- new-patient scaling factor of high exhaled tidal volume...
const Real32  SettingConstants::NP_HIGH_EXH_TIDAL_VOL_IBW_SCALE =
				 NP_HIGH_TIDAL_VOL_ALARM_FACTOR_;  // mL/kg...


//=====================================================================
//  Low Exhaled Mandatory Tidal Volume Constants...
//=====================================================================

// $[02179] -- new-patient scaling factor of low exh. mandatory tidal vol...
const Real32  SettingConstants::NP_LOW_MAND_TIDAL_VOL_IBW_SCALE =
				  NP_LOW_TIDAL_VOL_ALARM_FACTOR_;  // mL/kg...


//=====================================================================
//  Low Exhaled Spontaneous Tidal Volume Constants...
//=====================================================================

// $[02187] -- new-patient scaling factor of low exh. spontaneous tidal vol..
const Real32  SettingConstants::NP_LOW_SPONT_TIDAL_VOL_IBW_SCALE =
				  NP_LOW_TIDAL_VOL_ALARM_FACTOR_;  // mL/kg...


//=====================================================================
//  Alarm Limit Constants...
//=====================================================================

// any obscure -- NOT within legal ranges of any alarm settings -- positive
// value will do...
const Real32  SettingConstants::UPPER_ALARM_LIMIT_OFF =
						DEFINED_UPPER_ALARM_LIMIT_OFF;

// any obscure -- NOT within legal ranges of any alarm settings -- negative
// value will do...
const Real32  SettingConstants::LOWER_ALARM_LIMIT_OFF =
						DEFINED_LOWER_ALARM_LIMIT_OFF;


//=====================================================================
//  Display Setting Range Constants...
//=====================================================================

// $[02108] -- maximum display contrast value...
const SequentialValue  SettingConstants::MAX_DISPLAY_CONTRAST_SCALE = 100;

// $[02108] -- minimum display contrast value...
const SequentialValue  SettingConstants::MIN_DISPLAY_CONTRAST_SCALE = 0;

// $[02267] -- maximum display contrast delta value...
const SequentialValue  SettingConstants::MAX_DISPLAY_CONTRAST_DELTA = 100;

// $[02267] -- minimum display contrast delta value...
const SequentialValue  SettingConstants::MIN_DISPLAY_CONTRAST_DELTA = -100;

// $[02105] -- maximum display brightness value...
const SequentialValue  SettingConstants::MAX_DISPLAY_BRIGHTNESS = 100;

// $[02105] -- minimum display brightness value...
const SequentialValue  SettingConstants::MIN_DISPLAY_BRIGHTNESS = 25;


//=====================================================================
//  Tube ID constants...
//=====================================================================

// $[TC02030] -- minimum tube id value...
const Real32  SettingConstants::MIN_TUBE_ID_VALUE = 4.5f;


//=====================================================================
//  General Arithmetic Constants...
//=====================================================================

// millisecond-to-minute conversion factor...
const Real32  SettingConstants::MSEC_TO_MIN_CONVERSION = 60000.0f;


//=====================================================================
//  PAV support Constants...
//=====================================================================

// New patient default percent support values

const Real32 SettingConstants::NEW_PATIENT_PERCENT_SUPP_PAV     = 50.0f;
const Real32 SettingConstants::NEW_PATIENT_PERCENT_SUPP_NON_PAV = 100.0f;

// Soft and  hard bounds for percent support setting in PA mode

const Real32 SettingConstants::MAX_SOFT_PERCENT_SUPP_PAV        = 80.0f;
const Real32 SettingConstants::ABS_MAX_PERCENT_SUPP_PAV         = 95.0f;

// Min percent support value for ATC mode

const Real32 SettingConstants::ABS_MIN_PERCENT_SUPP_ATC         = 10.0f;

