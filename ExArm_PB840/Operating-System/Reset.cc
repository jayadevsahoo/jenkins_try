#include "stdafx.h"
//====================================================================
// This is a proprietary work to which Nellcor Puritan Bennett
// corporation of California claims exclusive right.  No part of this
// work may be used, disclosed, reproduced, sorted in an information
// retrieval system, or transmitted by any means, electronic,
// mechanical, photocopying, recording, or otherwise without the prior
// written permission of Nellcor Puritan Bennett Corporation.
//
//     Copyright (c) 1995, Nellcor Puritan Bennett Corporation
//=====================================================================


//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: Reset - System Reset/Reboot Control
//---------------------------------------------------------------------
//@ Interface-Description
//  This class provides a method to the 840 application to reset the
//  processor and log the reason for the reset in the System Diagnostic
//  Log.
//---------------------------------------------------------------------
//@ Rationale
//  Contains the application interface to reset the processor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Operating-System/vcssrc/Reset.ccv   25.0.4.0   19 Nov 2013 14:16:20   pvcs  $
//
//@ Modification-Log
//
//   Revision 001   By: Gary Cederquist Date: 28-MAY-1997 DR Number: 2176
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version.
//
//=====================================================================

//=====================================================================
//
//      Includes...
//
//=====================================================================

#include "Reset.hh"
#include "DiagnosticCode.hh"
#include "FaultHandler.hh"
// TODO E600 remove
//#include "Ostream.hh"
#include "InitiateReboot.hh"
#include <stdlib.h>

//=====================================================================
//
//      Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Initiate [Static]
//
//@ Interface-Description
//  This method called from the 840 application performs an orderly
//  reset of the processor.  If the shutdown is INTENTIONAL, it logs
//  the specified resetCode in the System Diagnostic Log.  It then
//  calls InitiateReboot to actually initiate the reset and subsequent
//  reboot of the processor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
Reset::Initiate(ShutdownState shutdownState, ResetCode  resetCode)
{

    if (shutdownState == INTENTIONAL)
    {
        // $[TI1.1]
        DiagnosticCode  diagnosticCode;
        diagnosticCode.setSystemEventCode(
               DiagnosticCode::INTENTIONAL_RESET_FORCED, resetCode);
        FaultHandler::LogDiagnosticCode( diagnosticCode );

#ifdef SIGMA_DEVELOPMENT
        const char * pCode = NULL;

        cout << endl;
        cout << endl;

        cout << "RESET INITIATED {" << endl;

        if (shutdownState == INTENTIONAL)
        {
            switch (resetCode)
            {
               case GUIINOP:
                   pCode = "GUIINOP";
                   break;
               case GUIONLINE:
                   pCode = "GUIONLINE";
                   break;
               case GUIONLINEINIT:
                   pCode = "GUIONLINEINIT";
                   break;
               case GUISERVICE:
                   pCode = "GUISERVICE";
                   break;
               case GUISERVICEINIT:
                   pCode = "GUISERVICEINIT";
                   break;
               case GUISSTINIT:
                   pCode = "GUISSTINIT";
                   break;
               case GUITIMEOUT:
                   pCode = "GUITIMEOUT";
                   break;
               case BDINOP:
                   pCode = "BDINOP";
                   break;
               case BDSST:
                   pCode = "BDSST";
                   break;
               case BDSERVICE:
                   pCode = "BDSERVICE";
                   break;
               case SERVICEMODE:
                   pCode = "SERVICEMODE";
                   break;
               default:
                   pCode = "UNKNOWN";
                   break;
            }
        }

        cout << "   ResetCode:  " << pCode << endl;
        cout << "}" << endl;

        cout.flush();

#endif  // SIGMA_DEVELOPMENT

    }
    // $[TI1.2] implied else


    InitiateReboot(shutdownState);

    // does not return
}

