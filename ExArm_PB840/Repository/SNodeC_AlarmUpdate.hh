
#ifndef SNodeC_AlarmUpdate_HH
#define SNodeC_AlarmUpdate_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: SNodeC_AlarmUpdate - Node for Singly-Linked Lists of Generic Data
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Foundation/vcssrc/SLinkedNodeC.H_v   25.0.4.0   19 Nov 2013 14:03:50   pvcs  $
//
//@ Modification-Log
//
//   Revision: 003   By: sah   Date: 07-Jan-1999   DR Number: 5321
//   Project:  ATC
//   Description:
//	Changed 'SoftFault()' method to an non-inlined method.
//  
//   Revision: 002   By: sah   Date: 06-Feb-1996   DCS Number: 674
//   Project:  Sigma (R8027)
//   Description:
//	Removed 'ifndef' and 'endif' from around includes.
//
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//====================================================================

#include "Sigma.hh"
#include "FoundationClassId.hh"
#include "TemplateMacros.hh"
#include "AlarmUpdate.hh"

//@ Usage-Classes
//@ End-Usage


class SNodeC_AlarmUpdate
{
    //@ Friend:  SListC_AlarmUpdate
    // 'SListC_AlarmUpdate' needs access to the constructor, destructor,
    // and private members of this class.
    friend class SListC_AlarmUpdate;

  public:
    inline const AlarmUpdate&  getItem(void) const;
    inline AlarmUpdate&        getItem(void);

    inline const SNodeC_AlarmUpdate*  getNextPtr(void) const;

    static void  SoftFault(const SoftFaultID softFaultID, 
			   const Uint32      lineNumber, 
			   const char*       pFileName = NULL, 
			   const char*       pBoolTest = NULL); 

  private:
    // only 'SListC_AlarmUpdate' can create/destroy this class...
    inline SNodeC_AlarmUpdate(const SNodeC_AlarmUpdate& sNodeC);
    inline SNodeC_AlarmUpdate(const AlarmUpdate& newItem);
    inline ~SNodeC_AlarmUpdate(void);

    SNodeC_AlarmUpdate(void);				// not implemented...
    void  operator=(const SNodeC_AlarmUpdate&);		// not implemented...

    //@ Data-Member:  nodeItem_
    // The stored item for this list node.
    AlarmUpdate  nodeItem_;

    //@ Data-Member:  pNext_
    // A pointer to the next node.
    SNodeC_AlarmUpdate*  pNext_;
};


// Inline methods...
#include "SNodeC_AlarmUpdate.in"


#endif  // SNodeC_AlarmUpdate_HH
