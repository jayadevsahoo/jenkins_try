
#ifndef ImmediateModeTrigger_HH
#define ImmediateModeTrigger_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: ImmediateModeTrigger - Triggers immediately upon being enabled.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/ImmediateModeTrigger.hhv   25.0.4.0   19 Nov 2013 13:59:50   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 002  By:  sp    Date:  4-Mar-1996    DR Number:674
//       Project:  Sigma (R8027)
//       Description:
//             Remove #ifndef.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================
#  include "Breath_Delivery.hh"

//@ Usage-Classes
#  include "ModeTrigger.hh"

//@ End-Usage

class ImmediateModeTrigger : public ModeTrigger{
  public:
    ImmediateModeTrigger(const Trigger::TriggerId id);
    virtual ~ImmediateModeTrigger(void);

    static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL, 
			  const char*       pPredicate = NULL);

  protected:
    virtual Boolean triggerCondition_(void);
    virtual void triggerAction_(const Boolean trigCond);

  private:
    ImmediateModeTrigger(void);
    ImmediateModeTrigger(const ImmediateModeTrigger&);	// not implemented...
    void   operator=(const ImmediateModeTrigger&);	// not implemented...

};


#endif // ImmediateModeTrigger_HH
