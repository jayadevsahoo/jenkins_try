
#ifndef PressUnitsValue_HH
#define PressUnitsValue_HH

//====================================================================
//
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  PressUnitsValue - Values of the Pressure Units Setting.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/PressUnitsValue.hhv   25.0.4.0   19 Nov 2013 14:27:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//		Initial version.
//  
//====================================================================

//@ Usage-Classes
//@ End-Usage


struct PressUnitsValue
{
  //@ Type:  PressUnitsValueId 
  // All of the possible values of the Pressure Units Setting.
  enum PressUnitsValueId
  {
    // $[02270] -- values of the range of Pressure Units Setting...
    CMH2O_UNIT_VALUE,
    HPA_UNIT_VALUE,

    TOTAL_UNIT_VALUES
  };
};


#endif // PressUnitsValue_HH 
