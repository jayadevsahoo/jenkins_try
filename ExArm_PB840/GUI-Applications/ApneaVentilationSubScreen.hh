#ifndef ApneaVentilationSubScreen_HH
#define ApneaVentilationSubScreen_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: ApneaVentilationSubScreen - An Upper screen subscreen
// which appears automatically when the patient enters apnea.  It
// displays the current apnea vent settings.
//---------------------------------------------------------------------
//@ Version
//@(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/ApneaVentilationSubScreen.hhv   25.0.4.0   19 Nov 2013 14:07:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004   By: srp   Date:  24-Apr-2002    DR Number: 5327 & 5908
//  Project:  NeoMode
//  Description:
//      Added Modification log. Updated for NeoMode  03/01/00
//
//  Revision: 003  By:  sah	   Date:  23-Feb-1999    DCS Number:  5314
//  Project:  ATC
//  Description:
//      Added integrated with new dynamic, setting applicability functionality.
//      This allows for looser coupling between settings and GUI, and the
//      need for significantly less "setting knowledge" in the GUI subsystem.
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
// Revision: 001  By:  hhd Data:   01-MAY-95   DR Number:
// Project:  Sigma (R8027)
// Description:  Integration baseline.
//
//====================================================================

#include "SubScreen.hh"

//@ Usage-Classes
#include "Box.hh"
#include "Line.hh"
#include "DiscreteSettingButton.hh"
#include "NumericSettingButton.hh"
#include "TextField.hh"
#include "TimingSettingButton.hh"
#include "ContextObserver.hh"
//@ End-Usage

class ApneaVentilationSubScreen : public SubScreen, public ContextObserver
{
public:
	ApneaVentilationSubScreen(SubScreenArea* pSubScreenArea);
	~ApneaVentilationSubScreen(void);

	virtual void activate(void);
	virtual void deactivate(void);

	// ContextObserver virtual method...
	virtual void  batchSettingUpdate(
							 const Notification::ChangeQualifier qualifierId,
							 const ContextSubject*               pSubject,
							 const SettingId::SettingIdType      settingId
									);

	static void SoftFault(const SoftFaultID softFaultID,
								const Uint32      lineNumber,
								const char*       pFileName  = NULL, 
								const char*       pPredicate = NULL);

private:
	// these methods are purposely declared, but not implemented...
	ApneaVentilationSubScreen();						// not implemented...
	ApneaVentilationSubScreen(const ApneaVentilationSubScreen&);// not implemented...
	void operator=(const ApneaVentilationSubScreen&);	// not implemented...

	//@ Data-Member: titleText_
	// Subscreen title text.
	TextField titleText_;

	//@ Data-Member: subTitleText_
	// Subscreen sub-title text.
	TextField subTitleText_;

	//@ Data-Member: footnoteText1_
	// First subscreen footnote text.
	TextField footnoteText1_;

    //@ Data-Member: footnoteText2_
    // Second subscreen footnote text.
    TextField footnoteText2_;

    //@ Data-Member: modeText_
    // Mode readout for mode indicator bar (always AV for Apnea Vent).
    TextField modeText_;
	
	//@ Data-Member: mandatoryTypeText_
	// Mandatory type indicator for fake status bar (PCV or VCV)
	TextField mandatoryTypeText_;

	//@ Data-Member: triggerTypeText_
	// A text field for displaying the current Trigger Type.
	TextField triggerTypeText_;

	//@ Data-Member: leftWingtip_
	// Left-hand "wingtip" line for the fake status bar. 
	Line leftWingtip_;

	//@ Data-Member: leftWing_
	// Left-hand "wing" line for the fake status bar.
	Line leftWing_;

	//@ Data-Member: rightWing_
	// Right-hand "wing" line for the fake status bar.
	Line rightWing_;

	//@ Data-Member: rightWingtip_
	// Right-hand "wingtip" line for the fake status bar.
	Line rightWingtip_;
     
	//@ Data-Member: modeBackground_            
	// Black background for the mode indicator bar.                            
    Box modeBackground_;   

	//@ Data-Member: buttonBackground_
	// Dark gray background for the fake button area.
	Box buttonBackground_;

	//@ Data-Member: flowPatternButton_
	// The fake flow Pattern button.
	DiscreteSettingButton flowPatternButton_;
	
	//@ Data-Member: inspiratoryPressureButton_
	// The fake inspiratory Pressure button (PCV only).
	NumericSettingButton inspiratoryPressureButton_;

	//@ Data-Member: inspiratoryTimeButton_
	// The fake inspiratory Time button (PCV only).
	TimingSettingButton inspiratoryTimeButton_;

	//@ Data-Member: oxygenPercentageButton_
	// The fake oxygen Percentage button.
	NumericSettingButton oxygenPercentageButton_;

	//@ Data-Member: peakFlowButton_
	// The fake peak Flow button (VCV only).
	NumericSettingButton peakFlowButton_;

	//@ Data-Member: apneaRespiratoryRateButton_
	// The fake respiratory Rate button.
	NumericSettingButton respiratoryRateButton_;

	//@ Data-Member: tidalVolumeButton_
	// The fake Tidal Volume button (VCV only).
	NumericSettingButton tidalVolumeButton_;

	//@ Data-Member: peepButton_
	// The Peep main setting button.
	NumericSettingButton peepButton_;

	//@ Data-Member: peepLowButton_
	// The Peep Low main setting button.
	NumericSettingButton peepLowButton_;

	//@ Data-Member: flowSensitivityButton_
	// The Flow Sensitivity main setting button.
	NumericSettingButton flowSensitivityButton_;

	//@ Data-Member: pressureSensitivityButton_
	// The Pressure Sensitivity main setting button.
	NumericSettingButton pressureSensitivityButton_;

	enum { MAX_SETTING_BUTTONS_ = 12 };

	//@ Data-Member: arrSettingBtnPtrs_
	// All buttons that could be shown in this subscreen.
	SettingButton*  arrSettingBtnPtrs_[MAX_SETTING_BUTTONS_ + 1];
};


#endif // ApneaVentilationSubScreen_HH 
