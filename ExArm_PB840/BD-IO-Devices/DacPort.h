#pragma once

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
//@ Class: DacPort - To provide access to the hardware to control Psol
//---------------------------------------------------------------------

#include "Sigma.hh"
#include "AioDioDriver.h"

const Int16  MAX_DAC_COUNT_VALUE  =  4095 ;

class DacPort
{
public:
	DacPort();
	DacPort(AnalogOutputs_t id);
	void setDacId(AnalogOutputs_t id) { dacId_ = id; }
	void setDac(DacCounts value);

private:
	AnalogOutputs_t		dacId_;
};