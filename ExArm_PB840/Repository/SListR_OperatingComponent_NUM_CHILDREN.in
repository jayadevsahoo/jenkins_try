
//====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: SListR_OperatingComponent_NUM_CHILDREN - Fixed Singly-Linked List of 'OperatingComponent' References.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Foundation/vcssrc/FixedSListR.I_v   25.0.4.0   19 Nov 2013 14:03:48   pvcs  $
//
//@ Modification-Log
//
//   Revision: 002   By: sah   Date: 07-Jan-1999   DR Number: 5321
//   Project:  ATC
//   Description:
//	Changed 'SoftFault()' method to an non-inlined method.
//  
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//====================================================================

//====================================================================
//
//  Public Methods...
//
//====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  operator=(list)  [Assignment Operator]
//
//@ Interface-Description
//  Copy all of the items from 'list' into this list.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  (*this == list)
//@ End-Method
//=====================================================================

inline void
SListR_OperatingComponent_NUM_CHILDREN::operator=(const SListR_OperatingComponent_NUM_CHILDREN& list)
{
  CALL_TRACE("operator=(list)");
  SListR_OperatingComponent::operator=(list);
}
