
#ifndef NonVolatileMemory_HH
#define NonVolatileMemory_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class:  NonVolatileMemory - NOVRAM Memory Block.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Persistent-Objects/vcssrc/NonVolatileMemory.hhv   25.0.4.0   19 Nov 2013 14:18:50   pvcs  $
//
//@ Modification-Log
//
//  Revision: 017   By: mnr   Date: 25-Feb-2010   SCR Number: 6436
//  Project:  PROX4
//  Description:
//      PROX Firmware Rev and serial number related updates.
//
//  Revision: 016  By:  gdc    Date:  10-Dec-2008    SCR Number: 6377
//  Project:  840S
//  Description:
//	Removed DELTA project single screen support.
//
//  Revision: 015   By: gdc  Date:  27-Mar-2007    SCR Number: 6237
//  Project:  Trend
//  Description:
//      Added compact flash test status for Trending option.
//
//  Revision: 013   By: sah  Date:  25-Aug-2000    DR Number:  5753
//  Project:  Delta
//  Description:
//      Delta project-specific changes:
//      *  added a screen-configuration flag, for determining whether this
//         is a single-screen configuration, or not
//
//  Revision: 012   By: sah  Date:  25-Jan-2000    DR Number:  5424
//  Project:  NeoMode
//  Description:
//      Added storing of user-controlled options, via the Development
//      Options subscreen.
//
//  Revision: 011  By:  sah    Date:  08-May-1998    DCS Number: 5041
//  Project:  Sigma (R8027)
//  Description:
//	Changed 'SoftFault()' to a non-inlined method.
//
//  Revision: 007  By:  sah    Date:  29-Sep-1997    DCS Number: 2525
//  Project:  Sigma (R8027)
//  Description:
//	Removed 'exhCalInProgressState_' and 'ventInopTestInfo_' data
//	items.
//
//  Revision: 006  By:  sah    Date:  16-Sep-1997    DCS Number: 2385 & 1861
//  Project:  Sigma (R8027)
//  Description:
//	Added new Service-Mode data instance ('ServiceModeNovramData').
//	Also, add an instance of 'AtmPressOffset', which is available
//	on the BD CPU, only.
//
//  Revision: 005  By:  sah    Date:  04-Sep-1997    DCS Number: 1923
//  Project:  Sigma (R8027)
//  Description:
//	Replaced the Communication Diagnostic Log with the new System
//	Information Log.
//
//  Revision: 004  By:  sah    Date:  17-Jul-1997    DCS Number: 2279
//  Project:  Sigma (R8027)
//  Description:
//	Added two new BD data items, 'airFlowSensorOffset_' and
//	o2FlowSensorOffset_'.
//
//  Revision: 003  By:  sah    Date:  15-Jul-1997    DCS Number: 2175
//  Project:  Sigma (R8027)
//  Description:
//	Removed four static methods whose only responsibility was to forward
//	the requests to the one instance of the corresponding class.  These
//	methods are now the responsibility of the corresponding classes.  Also,
//	added those corresponding classes as friends of this class.
//
//  Revision: 002  By:  sah    Date:  01-Jul-1997    DCS Number: 2066 & 2107
//  Project:  Sigma (R8027)
//  Description:
//	Moved 'lastSstPatientCctType_' from the GUI side to the BD side, and
//	replaced the 'exhCalInProgressFlag_' boolean data item with a
//	'exhCalInProgressState_' discrete data item.
//
//  Revision: 001  By:  sah    Date:  15-Mar-1997    DCS Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//          Initial version (Integration baseline).
//
//====================================================================

#include "Sigma.hh"
#include "NovRamStatus.hh"

//@ Usage-Classes
#include "SystemInfoLog.hh"
#include "SystemDiagLog.hh"
#include "NovRamBoolean.hh"
#include "NovRamDiscrete.hh"
#include "NovRamSequenceNum.hh"
#include "EstResultTable.hh"
#include "EstDiagLog.hh"
#include "SstResultTable.hh"
#include "SstDiagLog.hh"
#include "DblBuff_BtatEventState.hh"
#include "DblBuff_OperationalTime.hh"
#include "DblBuff_SerialNumber.hh"
#include "DblBuff_UserOptions.hh"
#include "DblBuff_BigSerialNumber.hh"

#if defined(SIGMA_GUI_CPU)
#include "AlarmHistoryLog.hh"
#include "DblBuff_BatchSettingValues.hh"
#include "DblBuff_NonBatchSettingValues.hh"
#include "DblBuff_ResultEntryData.hh"
#include "DblBuff_EstTestResultData.hh"
#include "DblBuff_SstTestResultData.hh"
#include "DblBuff_DataCorruptionInfo.hh"
#include "DblBuff_TimeStamp.hh"
#endif // defined(SIGMA_GUI_CPU)

#if defined(SIGMA_BD_CPU)
#include "DblBuff_BdSettingValues.hh"
#include "DblBuff_CircuitCompliance.hh"
#include "DblBuff_BdSystemState.hh"
#include "DblBuff_Fio2CalInfo.hh"
#include "DblBuff_PsolLiftoff.hh"
#include "DblBuff_ResultTableEntry.hh"
#include "DblBuff_Resistance.hh"
#include "DblBuff_FlowSensorOffset.hh"
#include "DblBuff_ServiceModeNovramData.hh"
#include "DblBuff_AtmPressOffset.hh"
#include "DblBuff_Uint32.hh"
#endif // defined(SIGMA_BD_CPU)
//@ End-Usage


class NonVolatileMemory
{
    //@ Friend:  NovRamManager
    // 'NovRamManager' needs access to ALL of this class's members.
    friend class NovRamManager;

    //@ Friend:  NovRamXaction
    // 'NovRamXaction' needs access to the data items.
    friend class NovRamXaction;

    //@ Friend:  EstDiagLog
    // 'EstDiagLog' needs its one instance.
    friend class EstDiagLog;

    //@ Friend:  SstDiagLog
    // 'SstDiagLog' needs its one instance.
    friend class SstDiagLog;

    //@ Friend:  EstResultTable
    // 'EstResultTable' needs its one instance.
    friend class EstResultTable;

    //@ Friend:  SstResultTable
    // 'SstResultTable' needs its one instance.
    friend class SstResultTable;

    //@ Friend:  NV_MemoryStorageThread
    // 'NV_MemoryStorageThread' needs the address of P_INSTANCE_.
    friend class NV_MemoryStorageThread;

  public:
    static inline class NonVolatileMemory*  GetAddress(void);

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL,
			   const char*       pPredicate = NULL);

  private:
    // Everything is private, because only this class's friends should
    // be using this class directly.
    NonVolatileMemory(const NonVolatileMemory&);// not implemented...
    NonVolatileMemory(void);			// not implemented...
    ~NonVolatileMemory(void);			// not implemented...
    void  operator=(const NonVolatileMemory&);	// not implemented...

    NovRamStatus::InitializationId initialize_(void);
    NovRamStatus::VerificationId   verifySelf_(void);

    inline void  processInitStatusId_ (
			const NovRamStatus::InitializationId initStatusId
				      );
    inline void  resetVerifStatusId_  (void);
    inline void  processVerifStatusId_(
			 const NovRamStatus::VerificationId verifStatusId
				     );

    //@ Data-Member:  sequenceCount_
    // This is the sequence number count for ALL of the "sequenced"
    // items of NOVRAM.
    NovRamSequenceNum  sequenceCount_;

    //@ Data-Member:  systemInfoLog_
    // System Information Log.
    SystemInfoLog  systemInfoLog_;

    //@ Data-Member:  systemDiagLog_
    // System Diagnostic Code Log.
    SystemDiagLog  systemDiagLog_;

    //@ Data-Member:  activeBtatEvents_
    // Active Background Test events.
    DoubleBuff(BtatEventState)  activeBtatEvents_;

    //@ Data-Member:  backgroundFailureFlag_
    // A boolean flag to indicate whether a (major) background failure
    // has occurred -- thereby requiring the running of EST.
    NovRamBoolean  backgroundFailureFlag_;

    //@ Data-Member:  estDiagLog_
    // EST Diagnostic Code Log.
    EstDiagLog  estDiagLog_;

    //@ Data-Member:  estResultTable_
    // EST Result Table.
    EstResultTable  estResultTable_;

    //@ Data-Member:  sstDiagLog_
    // SST Diagnostic Code Log.
    SstDiagLog  sstDiagLog_;

    //@ Data-Member:  sstResultTable_
    // SST Result Table.
    SstResultTable  sstResultTable_;

    //@ Data-Member:  patientSettingsAvailFlag_
    // Indicator as to whether the patient setting values in NOVRAM have
    // are available for use, or not.
    NovRamBoolean  patientSettingsAvailFlag_;

    //@ Data-Member:  batchSettingsInitFlag_
    // Indicator as to whether the all of the batch setting values in
    // NOVRAM have been initialized, or not.
    NovRamBoolean  batchSettingsInitFlag_;

    //@ Data-Member:  unitSerialNum_
    // The serial number of the GUI (in GUI's NOVRAM), or BD (in BD's
    // NOVRAM) unit.
    DoubleBuff(SerialNumber)  unitSerialNum_;

    //@ Data-Member:  userOptions_
    // For use with Development Options Subscreen only, this stores the user
    // configured software option bits.
    DoubleBuff(UserOptions)  userOptions_;

#if defined(SIGMA_GUI_CPU)
    //=================================================================
    //  GUI-only data members...
    //=================================================================

    //@ Data-Member:  alarmHistoryLog_
    // Alarm History History Log.
    AlarmHistoryLog  alarmHistoryLog_;

    //@ Data-Member:  acceptedBatchValues_
    // The accepted batch setting values stored on the GUI CPU.
    DoubleBuff(BatchSettingValues)  acceptedBatchValues_;

    //@ Data-Member:  nonBatchSettingsInitFlag_
    // Indicator as to whether the non-batch setting values in NOVRAM have
    // been initialized, or not.
    NovRamBoolean  nonBatchSettingsInitFlag_;

    //@ Data-Member:  acceptedNonBatchValues_
    // The accepted non-batch setting values.
    DoubleBuff(NonBatchSettingValues)  acceptedNonBatchValues_;

    //@ Data-Member:  lastSstHumidifierType_
    // The value of Humidifier Type Setting at the last SST/EST run.
    NovRamDiscrete  lastSstHumidifierType_;

    //@ Data-Member:  estTestResultData_
    // The result data of the last run of EST.
    DoubleBuff(EstTestResultData)  estTestResultData_;

    //@ Data-Member:  sstTestResultData_
    // The result data of the last run of SST.
    DoubleBuff(SstTestResultData)  sstTestResultData_;

    //@ Data-Member:  dataCorruptionInfo_
    // The data corruption information used by Safety-Net.
    DoubleBuff(DataCorruptionInfo)  dataCorruptionInfo_;

    //@ Data-Member:  noVentStartTime_
    // Time stamp containing the start time of "No Ventilation".
    DoubleBuff(TimeStamp)  noVentStartTime_;

    //@ Data-Member:  compactFlashTestStatus_
    // The status of the compact flash test
    NovRamDiscrete  compactFlashTestStatus_;

#endif // defined(SIGMA_GUI_CPU)


#if defined(SIGMA_BD_CPU)
    //=================================================================
    //  BD-only data members...
    //=================================================================

    //@ Data-Member:  bdSystemState_
    // State information for the Breath-Delivery System.
    DoubleBuff(BdSystemState)  bdSystemState_;

    //@ Data-Member:  acceptedBatchValues_
    // The accepted batch setting values stored on the BD CPU.
    DoubleBuff(BdSettingValues)  acceptedBatchValues_;

    //@ Data-Member:  psolLiftoff_
    // The PSOL lift-off current values (both air and O2).
    DoubleBuff(PsolLiftoff)  psolLiftoff_;

    //@ Data-Member:  compressorOperationalMinutes_
    // The compressor's elapsed time (in minutes).
    DoubleBuff(OperationalTime)  compressorOperationalMinutes_;

    //@ Data-Member:  systemOperationalMinutes_
    // The ventilator's operational minutes.
    DoubleBuff(OperationalTime)  systemOperationalMinutes_;

    //@ Data-Member:  circuitComplianceTable_
    // The patient circuit's compliance table.
    DoubleBuff(CircuitCompliance)  circuitComplianceTable_;

    //@ Data-Member:  fio2CalInfo_
    // The calibration information for the FIO2 sensor.
    DoubleBuff(Fio2CalInfo)  fio2CalInfo_;

    //@ Data-Member:  compressorSerialNum_
    // The serial number of the compressor, if any.
    DoubleBuff(SerialNumber)  compressorSerialNum_;

    //@ Data-Member:  inspCctResistance_
    // The inspiratory circuit's resistance determined via SST.
    DoubleBuff(Resistance)  inspCctResistance_;

    //@ Data-Member:  exhCctResistance_
    // The exhalation circuit's resistance determined via SST.
    DoubleBuff(Resistance)  exhCctResistance_;

    //@ Data-Member:  airFlowSensorOffset_
    // The Air Flow Sensor's offset.
    DoubleBuff(FlowSensorOffset)  airFlowSensorOffset_;

    //@ Data-Member:  o2FlowSensorOffset_
    // The O2 Flow Sensor's offset.
    DoubleBuff(FlowSensorOffset)  o2FlowSensorOffset_;

    //@ Data-Member:  serviceModeState_
    // A storage class for Service-Mode data.
    DoubleBuff(ServiceModeNovramData)  serviceModeState_;

    //@ Data-Member:  atmPressOffset_
    // The value of the Atmpospheric Pressure Offset.
    DoubleBuff(AtmPressOffset)  atmPressOffset_;

    //@ Data-Member:  lastSstPatientCctType_
    // The value of Patient Circuit Type Setting at the last SST/EST run.
    NovRamDiscrete  lastSstPatientCctType_;

	//@ Data-Member:  proxFirmwareRev_
    // The firmware revision of the PROX board, if any.
    DoubleBuff(BigSerialNumber)  proxFirmwareRevision_;

	//@ Data-Member:  proxSerialNum_
    // The seiral number of the PROX board, if any.
    DoubleBuff(Uint32)  proxSerialNumber_;

#endif // defined(SIGMA_BD_CPU)

    //@ Data-Member:  IsInitialized_
    // This is a static boolean flag to inidicate whether this class's
    // data members are ready for "interaction" (i.e., it indicates whether
    // 'initialize_()' has run to completion).
    static Boolean  IsInitialized_;

    //@ Data-Member:  InitializationStatusId_
    // This is a static variable used to indicate whether there was any
    // invalid data items detected during initialization.
    static NovRamStatus::InitializationId  InitializationStatusId_;

    //@ Data-Member:  VerificationStatusId_
    // This is a static variable used to indicate whether there was any
    // invalid data items detected during verification.
    static NovRamStatus::VerificationId  VerificationStatusId_;

    //@ Data-Member:  MAX_BYTES_AVAILABLE_
    // This indicates the maximum size available for the non-volatile data
    // items.
    static const Uint  MAX_BYTES_AVAILABLE_;

    //@ Data-Member:  P_INSTANCE_
    // This pointer points to the one, and only, instance of this class.
    static NonVolatileMemory* P_INSTANCE_;
};


// Inlined methods...
#include "NonVolatileMemory.in"


#endif // NonVolatileMemory_HH
