#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: Settings_Validation - Subsystem class.
//---------------------------------------------------------------------
//@ Interface-Description
//  Each subsystem defines a "subsystem" class, using the same name as
//  the subsystem.  This class is responsible for providing an initialize
//  method, which activates the initialization of the entire subsystem.
//
//  This class will reside on both CPUs.  The initialization is slightly
//  different between the CPUs, however.
//---------------------------------------------------------------------
//@ Rationale
//  This class provides a means for the system initialization functions
//  to initialize all of the aspects of the Settings-Validation Subsystem,
//  without "knowing" WHAT initialization is needed.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This is a static class whose only responsibility is the activation
//  of all of the initialization for the Settings-Validation Subsystem.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/Settings_Validation.ccv   25.0.4.0   19 Nov 2013 14:27:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003   By: sah    Date: 22-Sep-1999    DR Number: 5424
//  Project:  NeoMode
//  Description:
//	Obsoleted 'SettingOptions' class, to use new global
//      'SoftwareOptions' class.
//
//  Revision: 002   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  'SettingCallbackMgr' mechanism now obsoleted on GUI
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//      Initial version
//
//=====================================================================

#include "Settings_Validation.hh"

//@ Usage-Classes
#include "SettingXmitId.hh"
#include "SafetyPcvSettingValues.hh"
#include "SettingCallbackMgr.hh"
#include "ContextMgr.hh"

#if defined(SIGMA_GUI_CPU)  ||  defined(SIGMA_COMMON)
#include "SettingsMgr.hh"
#endif // defined(SIGMA_GUI_CPU)  ||  defined(SIGMA_COMMON)
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Static Member Initialization...
//
//=====================================================================

Uint  Settings_Validation::ArrDebugFlags_[];


//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  Initialize()  [static]
//
//@ Interface-Description
//  Initialize this subsystem.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
Settings_Validation::Initialize(void)
{
  CALL_TRACE("Initialize()");

  SettingXmitId::Initialize();

#if defined(SIGMA_BD_CPU)  ||  defined(SIGMA_COMMON)
  SettingCallbackMgr::Initialize();
#endif // defined(SIGMA_BD_CPU)  ||  defined(SIGMA_COMMON)

  SafetyPcvSettingValues::Initialize();

#if defined(SIGMA_GUI_CPU)  ||  defined(SIGMA_COMMON)
  SettingsMgr::Initialize();
#endif // defined(SIGMA_GUI_CPU)  ||  defined(SIGMA_COMMON)

  ContextMgr::Initialize();
}  // $[TI1] -- GUI only...


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  TurnAllDebugOn()  [static]
//
// Interface-Description
//  Turn all of this subsystem's debug flags on.
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  (Settings_Validation::IsDebugOn(...for all ids...))
// End-Method
//=====================================================================

void
Settings_Validation::TurnAllDebugOn(void)
{
  CALL_TRACE("TurnAllDebugOn()");

  for (Uint32 idx = 0; idx < Settings_Validation::NUM_WORDS_; idx++)
  {
    Settings_Validation::ArrDebugFlags_[idx] = ~(0u);
  }
}


//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  TurnAllDebugOff()  [static]
//
// Interface-Description
//  Turn all of this subsystem's debug flags off.
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  (!Settings_Validation::IsDebugOn(...for all ids...))
// End-Method
//=====================================================================

void
Settings_Validation::TurnAllDebugOff(void)
{
  CALL_TRACE("TurnAllDebugOff()");

  for (Uint32 idx = 0; idx < Settings_Validation::NUM_WORDS_; idx++)
  {
    Settings_Validation::ArrDebugFlags_[idx] = 0u;
  }
}

#endif  // defined(SIGMA_DEVELOPMENT)
