//
// 840tests.c
//

/***********************************************/
/****************** INCLUDES *******************/
/***********************************************/

#include <cvidef.h>
#include "txengine.h"
#include "72361.h"

tTXEngine ggTXEngine = {
/*	LLNode* hRoot;           */   NULL
/*	LLNode* hEnd;            */ , NULL
/*	LLNode* hCurrent;        */ , NULL

/*	LLNode* hSeqPreTest;     */ , NULL
/*	LLNode* hSeqPostTest;	 */ , NULL

/*	LLNode* hMetaPreTest;    */ , NULL
/*	LLNode* hMetaPostTest;   */ , NULL

/*	int size;                */ , 0
/*	int engineStatus;        */ , ON

/*	LLNode *hClipboard;      */ , NULL
/*	Boolean clipboardStatus; */ , EMPTY

/*	char *seqDescription;    */ , NULL

/*	char *reportFile;        */ , "c:\\BRTHLAB\\840VTS\\logs\\72362.log"
/*	short rptFileMode;       */ , 1
/*	short rptFileLock;       */ , 1

/*	char *seqFileName;       */ , "c:\\brthlab\\840vts\\840VTS_ENG.squ"

/*	char *generalPurpose;    */ , NULL
};

// Sequence Pre Test
// NULL
// NULL
// c:\brthlab\840vts\tests\72361.c
// StartSeq
// TX_AddPrePostTest isMetaTest FALSE  isPreTest TRUE
// assign to gTXEngine.hSeqPreTest

// Sequence Post Test
// NULL
// NULL
// c:\brthlab\840vts\tests\72361.c
// StopSeq
// TX_AddPrePostTest isMetaTest FALSE  isPreTest FALSE
// assign to gTXEngine.hSeqPostTest

// Meta Pre Test
// NULL
// NULL
// c:\brthlab\840vts\tests\72361.c
// OpenSeq
// TX_AddPrePostTest isMetaTest TRUE  isPreTest TRUE
// assign to gTXEngine.hMetaPreTest

// Meta Post Test
// NULL
// NULL
// c:\brthlab\840vts\tests\72361.c
// CloseSeq
// TX_AddPrePostTest isMetaTest TRUE  isPreTest FALSE
// assign to gTXEngine.hMetaPostTest

LLNode tNodeOpenSeq = {
/*	void (*hCurrTest)           */   NULL
/*	int (*hPreTest)             */ , NULL
/*	int (*hPostTest)            */ , NULL
/*	int modId;                  */ , -1
/*	int preModId;               */ , -1
/*	int postModId;              */ , -1

/*	char *testName;             */ , ""
/*	char *testDesc;             */ , ""
/*	char *fileName;             */ , "c:\\brthlab\\840vts\\tests\\72361.c"
/*	char *funcName;             */ , "OpenSeq"
/*	char *preFileName;          */ , ""
/*	char *preFuncName;          */ , ""
/*	char *postFileName;         */ , ""
/*	char *postFuncName;         */ , ""

/*	struct LLnode_class* hNext; */ , NULL
/*	struct LLnode_class* hPrev; */ , NULL

/*	char *parameters;           */ , ""
/*	char *preTestParameters;    */ , ""
/*	char *postTestParameters;   */ , ""

/*	short runMode;              */ , RUN_MODE_NORMAL
/*	double testLimit1;          */ , 0.0
/*	double testLimit2;          */ , 0.0
/*	short limitSpec;            */ , -1
/*	short passAction;           */ , -1
/*	short failAction;           */ , -1
/*	int maxLoops;               */ , 1
/*	int loopCount;              */ , 0
/*	short isGoto;               */ , FALSE
/*	int gotoTarget;             */ , -1
/*	int testFailMakesSeqFail;   */ , FALSE
};


