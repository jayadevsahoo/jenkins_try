
//*****************************************************************************
//
//  SpiFlash.h - Header file for SpiFlash_t class.
//
//*****************************************************************************
#ifndef SPI_FLASH_H

#define SPI_FLASH_H

#include "Sigma.hh"

static const Uint8 READ_STATUS_MASK_SPRL = 0x80;
static const Uint8 READ_STATUS_MASK_RES  = 0x40;
static const Uint8 READ_STATUS_MASK_EPE  = 0x20;
static const Uint8 READ_STATUS_MASK_WPP  = 0x10;
static const Uint8 READ_STATUS_MASK_SWP  = 0x0C;
static const Uint8 READ_STATUS_MASK_WEL  = 0x02;
static const Uint8 READ_STATUS_MASK_RDY_BSY = 0x01;

enum SpiFlashEraseBlockSize_t
{
    ERASE_4KB,
    ERASE_32KB,
    ERASE_64KB
};

// Whether using 8, 16 or 32 bit objects, the number of items in
// an SPI flash transfer is 32, the size of the FIFO in full duplex mode.
// See section 19.5.4 (FIFO Buffer Management) of OMAP3530 Technical Reference
// for an explanation of the FIFO size.
//static const Uint8 MAX_SPI_FLASH_ITEMS = 32; - The OMAP should suppoert this as per the technical reference manual.
static const Uint8 MAX_SPI_FLASH_ITEMS = 24;


class SpiFlash_t
{

    public:
        SpiFlash_t(void);
        ~SpiFlash_t(void){}

        Boolean    LockController();
        Boolean    UnLockController();
        Boolean    DeviceExists();                     // Return true if the device is there
        int     CS;                                 // Chip Select

        // This returns the device info. Use this if you want to display the info.
        // If you want to verify the correct values, call DeviceExists().
        Boolean    ReadManufacturerAndDeviceId(Uint8 &ManufacturerId,
                                            Uint8 &DeviceId1,
                                            Uint8 &DeviceId2,
                                            Uint8 &ExtendedInfoLength);

        Boolean    GlobalProtect();
        Boolean    GlobalUnprotect();
        Boolean    OpenDevice();

        Boolean    SpiFlashInitComplete();
        Boolean    WriteStatus(Uint8 Status);
        Boolean    ReadStatus(Uint8 &Status);

        Boolean    ReadProtection(uint32_t FlashAddress, Uint8 &Status);

        Boolean    Read(uint32_t FlashAddress, Uint8 *ReadData, int NumberOfBytes);
        Boolean    Write(uint32_t FlashAddress, Uint8 *WriteData, int NumberOfBytes);
        Boolean    ReadByte(uint32_t FlashAddress, Uint8 &ReadData);

        Boolean    EraseBlock(uint32_t FlashAddress, SpiFlashEraseBlockSize_t BlockSize);
        Boolean    EraseChip();

        Boolean    WriteEnable();
        Boolean    WriteDisable();
        Boolean    ConfigureDevice(Boolean assert);
    protected:
        Boolean    WriteBytes(uint32_t FlashAddress, Uint8 *WriteData, int NumberOfBytes);
        Boolean    ReadBytes(uint32_t FlashAddress, Uint8 *ReadData, int NumberOfBytes);
        Boolean    WaitForOperationCompletion(int Timeout);

        Boolean    DeviceExistsInitialized;
        Boolean    SavedDeviceExists;
		HANDLE	hActiveSPI;
};


#endif

