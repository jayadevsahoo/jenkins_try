#include "stdafx.h"
#include "TransducerCalibrationTable.h"
#include "GlobalObjects.h"
#include "AioDioDriver.h"
#include "Crc.h"

static const size_t NUMBER_CAL_READINGS_REQUIRED = 10;

//*****************************************************************************
TransducerCalibrationTable_t::TransducerCalibrationTable_t(CalibrationID_t CalID, Cal_t Zero, Cal_t Span) :   
								CalibrationTable_t(CalID)
{ 
	ZeroSetValue = Zero;
	SpanSetValue = Span;
	ZeroCalValue = (uint16_t)( pTable[ZERO_INDEX]);
	SpanCalValue = (uint16_t)(pTable[SPAN_INDEX]);
 	
	int CountsPerUnit = (int)((SpanCalValue-ZeroCalValue)/(Span-Zero));
	RETAILMSG(1, (L"CalID: %d, Zero=%d, Span=%d, CountsPerUnit=%d\r\n",CalID,(int)ZeroCalValue,(int)SpanCalValue, CountsPerUnit));
	CalStage = CAL_TRANSDUCER_IDLE;

    int CountDifferential = (int)(SpanCalValue - ZeroCalValue);
	if (CountDifferential <= 0)
    { 
        CountDifferential = 1;                              // prevent divide by 0
    }
	Slope    =  (float)(SpanSetValue - ZeroSetValue) / CountDifferential;	// cmH20 / count
    Offset   =  ZeroSetValue - (Slope * ZeroCalValue);		// pressure offset
}

//*****************************************************************************
CalibrationStatus_t TransducerCalibrationTable_t::Calibrate(A2dWithStatus_t A2dData)
{
	if(CalibrationStatus != CAL_PENDING)
	{
		CalibrationStatus = CAL_PENDING;
	}

	DoCalibration(A2dData);
	
	return CalibrationStatus;
}

//*****************************************************************************
bool TransducerCalibrationTable_t::DoCalibration(A2dWithStatus_t A2dData)
{
	switch(CalStage)
	{
	case CAL_TRANSDUCER_IDLE :
		if(CalibrationInfo.nCalSteps == 1)
		{
			CalStage = CAL_TRANSDUCER_SPAN;
			ZeroCalValue =(uint16_t)( pTable[ZERO_INDEX]);
		}
		else
			CalStage = CAL_TRANSDUCER_ZERO;
		A2dDataSum = 0;
		NumReadingsTaken = 0;
		break;

	case CAL_TRANSDUCER_ZERO :
		A2dDataSum += A2dData.Value;
		RETAILMSG(1, (L"TransducerCalibration: ZeroSample = %d\r\n", A2dData.Value));
		if(++NumReadingsTaken == NUMBER_CAL_READINGS_REQUIRED)
		{
			ZeroCalValue = (uint16_t)(A2dDataSum / NUMBER_CAL_READINGS_REQUIRED);
			RETAILMSG(1, (L"TransducerCalibration: Avg. ZeroValue = %d\r\n", (int)ZeroCalValue));

			A2dDataSum = 0;
			NumReadingsTaken = 0;
			CalStage = CAL_TRANSDUCER_SPAN;
			CalibrationStatus = CAL_PENDING2;
		}
		break;
	
	case CAL_TRANSDUCER_SPAN :
		A2dDataSum += A2dData.Value;
		RETAILMSG(1, (L"TransducerCalibration: SpanSample = %d\r\n", A2dData.Value));
		if(++NumReadingsTaken == NUMBER_CAL_READINGS_REQUIRED)
		{
			SpanCalValue = (uint16_t)(A2dDataSum / NUMBER_CAL_READINGS_REQUIRED);
			RETAILMSG(1, (L"TransducerCalibration: Avg. SpanValue = %d\r\n", (int)SpanCalValue));

			pTable[ZERO_INDEX] = ZeroCalValue;
			pTable[SPAN_INDEX] = SpanCalValue;
			A2dDataSum = 0;
			CalStage = CAL_TRANSDUCER_IDLE;
			CalibrationStatus = CAL_PASSED;
			CalibrationDone = true;
			SaveCalibrationTable();
		}
		break;
	}

	return CalibrationDone;
}

//*****************************************************************************
bool TransducerCalibrationTable_t::RetrieveCalibrationTable()
{
	//return false;
	return CalibrationTable_t::RetrieveCalibrationTable();
}

//*****************************************************************************
bool TransducerCalibrationTable_t::SaveCalibrationTable()
{
	//return false;
	return CalibrationTable_t::SaveCalibrationTable();
}

//*****************************************************************************
Cal_t TransducerCalibrationTable_t::GetZeroCounts()
{
	return  (uint16_t)(pTable[ZERO_INDEX]);
}

//*****************************************************************************
Cal_t TransducerCalibrationTable_t::GetSpanCounts()
{
	return (uint16_t)(pTable[SPAN_INDEX]);
}

//*****************************************************************************
Cal_t TransducerCalibrationTable_t::GetCountsPerUnit()
{
	Cal_t CountsPerUnit = (SpanCalValue - ZeroCalValue) / (SpanSetValue - ZeroSetValue);
	return CountsPerUnit;
}
