#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//============== C L A S S   D E S C R I P T I O N ====================
//@ Class:  Plot2TypeSetting - Type of Wavform for Plot #2 Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a non-batch, discrete setting that indicates the type of plot
//  (e.g., pressure-vs.-time) shown for Plot #2.  This class inherits from
//  'NonBatchDiscreteSetting' and provides the specific information needed
//  for the representation of type of Waveform Plot #2.  This information
//  includes the set of values that this setting can have (see
//  'Plot2TypeValue.hh'), and this non-batch setting's default value.
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class has no data members, and no dependent settings.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/Plot2TypeSetting.ccv   25.0.4.0   19 Nov 2013 14:27:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 007  By: rhj    Date: 05-Jun-2006    SCR Number: 6236
//  Project:  RESPM
//  Description:
//      Modified to support Flow Volume Loop.
//   
//  Revision: 006   By: gfu   Date:  27-Jul-2004    DR Number: 6139
//  Project:  PAV
//  Description:
//   Modified code per SDCR #6139.  Based upon input from the Field Evaluations in Canada 
//   the following PAV specific waveform changes are needed:
//   1.  Whenever PAV is selected as teh active spont type, the WOB graphic should be auto-
//   matically selected for Plot 2.  Once this change is made initially, the operator can use
//   PLOT SETUP to change these settings.
//   2.  When PAV is no longer active, and the WOB had been displayed, Plot 2 will be auto-
//   matically changed to P-T, F-T or V-T but different from the Plot 1 value.
//
//  Revision: 005   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'getApplicability()' method
//	*  now an observer of plot #1 type setting, therefore
//	   'valueUpdate()', 'doRetainAttachment()' and
//	   'settingObserverInit()' overridden from observer base class
//
//  Revision: 004   By: sah  Date:  11-Jan-1999    DR Number:  5321
//  Project:  ATC
//  Description:
//	As part of making all 'SoftFault()' methods non-inlined, I'm
//	also making those 'SoftFault()' methods that are NOT called
//	in PRODUCTION mode, DEVELOPMENT-only.
//
//  Revision: 003   By: dosman    Date:  07-Apr-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Fixed incorrect default value caused by getDefaultValue()
//	returning SettingValue: casted it to DiscreteValue
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Initial version.
//
//=====================================================================

#include "Plot2TypeSetting.hh"
#include "Plot2TypeValue.hh"
#include "Plot1TypeValue.hh"
#include "SupportTypeValue.hh"

//@ Usage-Classes
#include "ContextMgr.hh"
#include "AcceptedContext.hh"
#include "SettingsMgr.hh"
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  Plot2TypeSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[01151] -- setting range
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Plot2TypeSetting::Plot2TypeSetting(void)
	   : NonBatchDiscreteSetting(SettingId::PLOT2_TYPE,
				     Setting::NULL_DEPENDENT_ARRAY_,
                                     Plot2TypeValue::TOTAL_PLOT2_TYPES)
{
  CALL_TRACE("Plot2TypeSetting()");
}   // $[TI1]


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method:  ~Plot2TypeSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Plot2TypeSetting::~Plot2TypeSetting(void)
{
  CALL_TRACE("~Plot2TypeSetting(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//
//  [SRS2553][PA01015][P10.1.1.k] When PA becomes the spontaneous breath type and
//  plot 1 is NOT the pressure-volume loop, the Work-of-breathing plot 
//   become plot 2. 
//
//  SDCR # 6139  Based upon input from the Field Evaluations in Canada, the following
//               waveform changes are needed:
//             
//               1. Whenever PAV is selected as the active spontaneous type, the WOB
//                  graphic should be automatically selected for Plot2.  Once this
//                  change is made initially, the operator can use PLOT SETUP to change
//                  these settings.
//               2. When PAV is no longer active, and the WOB had been displayed, Plot 2
//                  will be automatically changed to Pressure-Time, Flow-Time or 
//                  Volume-Time, but different from the Plot 1 value.

//---------------------------------------------------------------------
//@ Implementation-Description
//

//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
Plot2TypeSetting::getApplicability(const Notification::ChangeQualifier) const
{
  CALL_TRACE("getApplicability(qualifierId)");

  const Setting*  pPlot1Type =
			SettingsMgr::GetSettingPtr(SettingId::PLOT1_TYPE);

  const DiscreteValue  PLOT1_TYPE_VALUE = pPlot1Type->getAcceptedValue();

  Applicability::Id  applicability;

  switch (PLOT1_TYPE_VALUE)
  {
  case Plot1TypeValue::PRESSURE_VS_TIME :
  case Plot1TypeValue::VOLUME_VS_TIME :
  case Plot1TypeValue::FLOW_VS_TIME :				// $[TI1]
    // always changeable here...
    applicability = Applicability::CHANGEABLE;
    break;
  case Plot1TypeValue::FLOW_VS_VOLUME :			  
  case Plot1TypeValue::PRESSURE_VS_VOLUME :			// $[TI2]
    // inapplicable if plot1 type is a P-V loop...
    
    applicability = Applicability::INAPPLICABLE;
    break;

  case Plot1TypeValue::TOTAL_PLOT1_TYPES :
  default :
    // unexpected plot1 type value...
    AUX_CLASS_ASSERTION_FAILURE(PLOT1_TYPE_VALUE);
    break;
  }

  return(applicability);
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  isEnabledValue()  [const, virtual]
//
//@ Interface-Description
//  Is this setting's value given by 'value', a currently-enabled value?
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none 
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
Boolean
Plot2TypeSetting::isEnabledValue(const DiscreteValue value) const
{
  Boolean  isEnabled;

  switch (value)
  {
  case Plot2TypeValue::PRESSURE_VS_TIME :
  case Plot2TypeValue::VOLUME_VS_TIME :
  case Plot2TypeValue::FLOW_VS_TIME :
  case Plot2TypeValue::NO_PLOT2 :			// $[TI1]
    // these values are always enabled...
    isEnabled = TRUE;
    break;

  case Plot2TypeValue::WOB_GRAPHIC :			// $[TI2]
    if (!ContextMgr::GetAcceptedContext().isBdInSafetyPcvState())
    {  // $[TI2.1] -- spont type's accepted value can be used...
      const Setting*  pSpontType =
			  SettingsMgr::GetSettingPtr(SettingId::SUPPORT_TYPE);

      const DiscreteValue  SPONT_TYPE_VALUE = pSpontType->getAcceptedValue();

      switch (SPONT_TYPE_VALUE)
      {
      case SupportTypeValue::OFF_SUPPORT_TYPE :
      case SupportTypeValue::PSV_SUPPORT_TYPE :
      case SupportTypeValue::ATC_SUPPORT_TYPE :
      case SupportTypeValue::VSV_SUPPORT_TYPE :		// $[TI2.1.1]
	// WOB graphic not applicable to these spont type values...
	isEnabled = FALSE;
	break;
      case SupportTypeValue::PAV_SUPPORT_TYPE :		// $[TI2.1.2]
	// WOB graphic only applicable for 'PA' spont type value...
	isEnabled = TRUE;
	break;
      default :
	// unexpected spont type value...
	AUX_CLASS_ASSERTION_FAILURE(SPONT_TYPE_VALUE);
	break;
      }
    }
    else
    {  // $[TI2.2] -- new-patient setup not yet complete...
      // accepted settings not yet in affect, therefore we can't have a case
      // where 'PA' is active...
      isEnabled = FALSE;
    }
    break;

  case Plot2TypeValue::TOTAL_PLOT2_TYPES :
  default :
    // unexpected plot type value...
    AUX_CLASS_ASSERTION_FAILURE(value);
    break;
  }
 
  return(isEnabled);
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getDefaultValue()  [const, virtual]
//
//@ Interface-Description
//  Get the setting's new-patient value.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
Plot2TypeSetting::getDefaultValue(void) const
{
  CALL_TRACE("getDefaultValue()");

  // $[01162] The new-patient value for Y-axis scale for Plot 2 ...
  return(DiscreteValue(Plot2TypeValue::FLOW_VS_TIME));    // $[TI1]
}


//==================== M E T H O D   D E S C R I P T I O N =============
//@ Method:  valueUpdate(qualifierId, pSubject)  [virtual]
//
//@ Interface-Description
//  Respond to subject's value changes, by, possibly, updating this setting's
//  applicability.
//---------------------------------------------------------------------
//@ Implementation-Description
//  When changing this setting's value from a newly-disabled value (e.g.,
//  "WOB Graphic"), bias the transition towards this setting's default value,
//  otherwise use plot #1's default value.
//
//  [SRS2553][PA01015][P10.1.1.k] When PA becomes the spontaneous breath 
//  type and plot 1 is not the pressure-volume loop, the Work-of-breathing 
//  plot shall become plot 2. 
//
//  [SRS2553][PA01014][P10.1.1.k] When the spontaneous breath type is changed
//  from PA and plot 2 is the Work-of-breathing plot, plot 2 shall set to a 
//  selection different than plot 1. 
//
//  SDCR # 6139  Based upon input from the Field Evaluations in Canada, the following
//               waveform changes are needed:
//             
//               1. Whenever PAV is selected as the active spontaneous type, the WOB
//                  graphic should be automatically selected for Plot2.  Once this
//                  change is made initially, the operator can use PLOT SETUP to change
//                  these settings.
//               2. When PAV is no longer active, and the WOB had been displayed, Plot 2
//                  will be automatically changed to Pressure-Time, Flow-Time or 
//                  Volume-Time, but different from the Plot 1 value.
// 

//---------------------------------------------------------------------
//@ PreCondition
//  (pSubject->getId() == SettingId::PLOT1_TYPE  ||
//   pSubject->getId() == SettingId::SUPPORT_TYPE)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
Plot2TypeSetting::valueUpdate(const Notification::ChangeQualifier qualifierId,
			      const SettingSubject*               pSubject)
{
  CALL_TRACE("valueUpdate(qualifierId, pSubject)");

  if (qualifierId == Notification::ACCEPTED)
  {  // $[TI1] -- only interested in ACCEPTED changes...
    if (pSubject->getId() == SettingId::SUPPORT_TYPE)
    {  // $[TI1.1] -- a new spont type value has been accepted...
       // 
      const Setting* pSupportType = 
		         SettingsMgr::GetSettingPtr(SettingId::SUPPORT_TYPE); 
			  
      const DiscreteValue SUPPORT_TYPE_VALUE = pSupportType->getAcceptedValue();
      
      DiscreteValue  plot2CurrValue; 	// local value of Plot 2 type
      

// Check to determine the value of Support Type - if PAV_SUPPORT_TYPE
      // is selected, we want to default PLOT2_TYPE to WOB_GRAPHIC
      
      if (SUPPORT_TYPE_VALUE == SupportTypeValue::PAV_SUPPORT_TYPE)
      {
         // force plot2 to WOB Graphic in PA Support  - the applicability
	 // of the setting will be checked via the method getApplicability().
	 // This method makes WOB graphics inapplicable when plot1 is set
	 // to display the P-V loop. 
	 
         plot2CurrValue = (DiscreteValue)Plot2TypeValue::WOB_GRAPHIC;
	 setAcceptedValue(plot2CurrValue);
	 
      }
      else
      {
	 // Support type is not PAV - get currently accepted value.
	 // current setting will be check for validity below.
         plot2CurrValue = getAcceptedValue();
      
      }

      if (!isEnabledValue(plot2CurrValue))
      {  // $[TI1.1.1] -- current plot #2 type is not allowed...
	const Setting*  pPlot1Type =
			    SettingsMgr::GetSettingPtr(SettingId::PLOT1_TYPE);

	const DiscreteValue  PLOT1_TYPE_VALUE = pPlot1Type->getAcceptedValue();
	const DiscreteValue  PLOT2_DEFAULT_VALUE = getDefaultValue();

	if (PLOT1_TYPE_VALUE != PLOT2_DEFAULT_VALUE)
	{  // $[TI1.1.1.1] -- set plot #2 to its default value...
	  setAcceptedValue(PLOT2_DEFAULT_VALUE);
	}
	else
	{  // $[TI1.1.1.2] -- set to plot #1's default value...
	  setAcceptedValue(DiscreteValue(pPlot1Type->getDefaultValue()));
	}
      }  // $[TI1.1.2] -- current plot #2 type is OK...
    }
    else if (pSubject->getId() == SettingId::PLOT1_TYPE)
    {  // $[TI1.2] -- Plot #1's value has changed...
      // update this instance's applicability...
      updateApplicability(qualifierId);
    }
    else
    {
      // unexpected setting subject...
      AUX_CLASS_ASSERTION_FAILURE(pSubject->getId());
    }
  }  // $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: doRetainAttachment(pSubject)  [virtual, const]
//
//@ Interface-Description
//  When task-level, state changes occur, all subjects are notified
//  to detach from all observers.  This is done because the GUI-Apps
//  observers may be overwritten by new class instances, without any
//  explicit destructor/detachment calls.
//
//  This method is defined here to indicate to any of this instance's
//  subjects, not to detach this observer.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean
Plot2TypeSetting::doRetainAttachment(const SettingSubject*) const
{
  CALL_TRACE("doRetainAttachment(pSubject)");

  return(TRUE);
}  // $[TI1]


//==================== M E T H O D   D E S C R I P T I O N ============
//@ Method:  settingObserverInit()  [virtual]
//
//@ Interface-Description
//  This setting needs to monitor the value of Plot1 Type setting,
//  therefore this virtual method is overridden to provide a point during
//  initialization to attach to that (subject) setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
Plot2TypeSetting::settingObserverInit(void)
{
  CALL_TRACE("settingObserverInit()");

  // monitor changes in plot1 type's value...
  attachToSubject_(SettingId::PLOT1_TYPE, Notification::VALUE_CHANGED);

  // monitor changes in spont type's value...
  attachToSubject_(SettingId::SUPPORT_TYPE,
		   Notification::VALUE_CHANGED);
}  // $[TI1]


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//			[static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
Plot2TypeSetting::SoftFault(const SoftFaultID  softFaultID,
			    const Uint32       lineNumber,
			    const char*        pFileName,
			    const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, ::SETTINGS_VALIDATION,
                          PLOT_2_TYPE_SETTING, lineNumber, pFileName,
                          pPredicate);
}





