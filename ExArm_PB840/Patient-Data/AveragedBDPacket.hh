
#ifndef AveragedBDPacket_HH
#define AveragedBDPacket_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Nellcor/Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Struct : AveragedBDPacket -  structure definition for an Averaged
//                             Breath Data packet.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Patient-Data/vcssrc/AveragedBDPacket.hhv   25.0.4.0   19 Nov 2013 14:17:56   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003   By: syw   Date:  15-Aug-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      * added new averaged data item, 'spontFtotVtRatio'.
//
//  Revision: 002  By:  jja    Date:  20-Apr-00    DR Number: 5708 
//	Project:  NeoMode
//	Description:
//		Revise Pmean to be an averaged value instead of  
//              single breath.
//
//  Revision: 001  By:  jhv    Date:  20-Jan-95    DR Number: 
//	Project:  Sigma (R8027)
//	Description:
//		Initial version (Integration baseline).
//
//====================================================================
#include "SocketWrap.hh"

//@Type : AveragedBDPacket
struct   AveragedBDPacket{
	Real32  exhaledMinuteVol;
	Real32  exhaledSpontMinuteVol;
	Real32  totalRespRate;
	Real32  meanCircuitPressure;
	Real32	spontFtotVtRatio;

	inline void convHtoN()
	{
		SocketWrap sock;
		exhaledMinuteVol = sock.HTONF(exhaledMinuteVol);
		exhaledSpontMinuteVol = sock.HTONF(exhaledSpontMinuteVol);
		totalRespRate = sock.HTONF(totalRespRate);
		meanCircuitPressure = sock.HTONF(meanCircuitPressure);
		spontFtotVtRatio = sock.HTONF(spontFtotVtRatio);
	}

	inline void convNtoH()
	{
		SocketWrap sock;
		exhaledMinuteVol = sock.NTOHF(exhaledMinuteVol);
		exhaledSpontMinuteVol = sock.NTOHF(exhaledSpontMinuteVol);
		totalRespRate = sock.NTOHF(totalRespRate);
		meanCircuitPressure = sock.NTOHF(meanCircuitPressure);
		spontFtotVtRatio = sock.NTOHF(spontFtotVtRatio);
	}
};

#endif // AveragedBDPacket_HH 
