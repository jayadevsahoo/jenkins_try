#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: AveragedBreathData - Averaged spirometric data.
//---------------------------------------------------------------------
//@ Interface-Description
// This class calculates the averaged spirometric values for the past 
// one minute interval or numBreaths (based on breath type) breaths, whichever comes first. 
// If one minute of data is not available, then the
// averages are normalized over one minute.  This class is updated every
// new inspiration but no sooner than .5 seconds from the last update, and
// no longer than ten seconds from the last update.  After the averages
// have been calculated, the values are transmitted to the
// Patient-Data subsystem.
//---------------------------------------------------------------------
//@ Rationale
// This class is responsible for calculating the averaged spirometric 
// data.
//---------------------------------------------------------------------
//@ Implementation-Description
// The method provided for updates is responsible for all the averaged breath
// data calculations. Exhaled minute volume is stored in exhMinuteVol_.
// Exhaled spontaneous minute volume is stored in exhSpontMinuteVol_.
// Total respiratory rate is stored in respiratoryRate_. Total 
// inspiration side minute volume is stored in inspMinuteVolume_. All
// volume calculations are compliance and BTPS compensated.
// Note: 
// =====
// The implementation of this class is dependent on the fact that the maximum
// duration of inspiration is less than 10 seconds. If changes to inspiratory
// time are made so that this fact is no longer valid, the method
// AveragedBreathData::updateAveragedBreathData will have to be modified.
//
// $[03030] $[03031] $[03034] $[03035] $[03065] $[03066] $[04052] $[04053]
//---------------------------------------------------------------------
//@ Fault-Handling
// n/a.
//---------------------------------------------------------------------
//@ Restrictions
//  There are 2 restrictions for updating this class:
//	(1) This class should not be updated more frequently than 500 msec.
//	(2) This class must be updated at least every 10 sec.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/AveragedBreathData.ccv   25.0.4.0   19 Nov 2013 13:59:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 041  By: rhj     Date:  29-Mar-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//		Added minute volume for prox; 
// 
//  Revision: 040  By: rhj     Date:  24-Feb-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//		Added prevRespiratoryRate_ and getPrevRespiratoryRate();
// 
//  Revision: 038   By: gdc    Date: 24-Sep-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal non-invasive CPAP.
//
//  Revision: 037   By:   rhj    Date: 05-March-2007      DR Number: 6359
//  Project:  RESPM
//  Description:
//        Resolve merge conflict for RM to Baseline 
//
//  Revision: 036   By: gdc   Date:  13-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Added NIF and P100 pause phases into averaged data.
//
//  Revision: 035   By: gfu   Date:  27-July-2004    DR Number: 6134
//  Project:  PAV
//  Description:
//      Modified module per SDCR #6134.  Based on input from the Field Evaluations in Canada:
//      Due to the breath-to-breath variability in PAV, the averaged breath data calculations
//      need to encompass more breaths than other breath types.  The variability results in
//      the values jumping and annunciation of the associated alarms (e.g. High RR).  The
//      number of breaths should be increased from 8 to 16.
//
//  Revision: 034  By: gfu     Date:  22-Apr-2002    DR Number: 5822
//  Project:  VTPC
//  Description:
//		Modified calucation of f/Vt so it is only calculated at the end of spontanous inspirations;
//	not every 10 seconds.  Also updated header information in updateAveragedBreathData.
//
//  Revision: 033  By: cep     Date:  17-Apr-2002    DR Number: 5899
//  Project:  VTPC
//  Description:
//		Changed variable name lastMeanPressure to LastMeanPressure_
//      according to the coding standard.
//
//  Revision: 032  By: jja     Date:  30-June-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//		VTPC project-related changes:
//		Added spontFtotVtRatio_ calculation.
//
//  Revision: 031  By:  jja    Date:  19-May-00    DR Number: 5727 
//	Project:  NeoMode
//	Description:
//		Correct Pmean display at 10sec. update following setting change.
//
//  Revision: 030  By:  jja    Date:  20-Apr-00    DR Number: 5708 
//	Project:  NeoMode
//	Description:
//		Revise Pmean to be an averaged value instead of  
//              single breath.
//
//  Revision: 029  By: jja     Date:  19-Apr-2000    DR Number: 5707
//  Project:  NeoMode
//  Description:
//		For PCV with Neo circuit, do not use PCV_TIME_FACTOR when  
//              calculatingComplianceBtpsVol.
//
//  Revision: 028  By: syw     Date:  07-Feb-2000    DR Number: 5327
//  Project:  NeoMode
//  Description:
//      Removed unused PatientCctTypeValue.hh
//
//  Revision: 027  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 026  By:  iv    Date:  31-Jan-1998    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Bilevel initial version.
//
//  Revision: 025  By: iv     Date:  20-Oct-1997    DR Number: DCS 2572
//  	Project:  Sigma (840)
//		Description:
//			In method updateAverageBreathData(), for non vcv based breath,
//          use instTime = insp time * PCV_TIME_FACTOR for compliance compensation
//          calculations.
//
//  Revision: 024  By:  iv    Date:  10-Sep-1997    DR Number: DCS 1726
//  	Project:  Sigma (840)
//		Description:
//			In method timeUpHappened(), added the check for 
//          !BreathRecord::IsPeepRecovery().
//
//  Revision: 023  By:  iv    Date:  03-Sep-1997    DR Number: NONE
//  	Project:  Sigma (840)
//		Description:
//			Reworked per formal code review.
//
//	Revision: 022  By:  iv    Date:  21-Aug-1997    DR Number: DCS 2408
//      Project:  Sigma (R8027)
//      Description:
//			Fixed testable items
//
//	Revision: 021  By:  syw    Date: 12-Aug-1997    DR Number: DCS 2357
//      Project:  Sigma (R8027)
//      Description:
//			Use only insp time in call to calculateComplianceBtpsVol() 
//
//  Revision: 020  By: syw   Date:  28-Jul-1997    DR Number: DCS 2328
//  	Project:  Sigma (840)
//      Description:
//             Changed factor to 1.0 for HME else 0.92.
//
//  Revision: 019  By: syw   Date:  3-Jun-1997    DR Number: DCS 1867, 2187
//  	Project:  Sigma (840)
//      Description:
//             Limit minute volume calculation to >= 0.
//
//  Revision: 018  By: syw   Date:  2-Jun-1997    DR Number: DCS 2116
//  	Project:  Sigma (840)
//      Description:
//             Maintained a separate time intervals for spont and mandatory breaths.
//
//  Revision: 017  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 016  By:  syw    Date: 14-May-1997    DR Number: DCS 2084
//       Project:  Sigma (R8027)
//       Description:
//       	Insure operand of >> is Uint32.  a Int32 will shift 1's to
//			maintain sign bit.
//
//  Revision: 015  By:  sp    Date: 1-Apr-1997    DR Number: DCS NONE
//       Project:  Sigma (R8027)
//       Description:
//             Inspection rework.
//
//  Revision: 014  By:  sp    Date: 27-Mar-1997    DR Number: DCS 1780
//       Project:  Sigma (R8027)
//       Description:
//             update interface with BreathRecord::calculateComplianceBtpsVol.
//
//  Revision: 013  By:  syw    Date: 13-Mar-1997    DR Number: DCS 1780
//       Project:  Sigma (R8027)
//       Description:
//             call rExhFlowSensor.setDryFactor() instead of FlowSensor::SetDryFactor().
//
//  Revision: 012  By:  sp    Date: 18-DeC-1996    DR Number: DCS 1611
//       Project:  Sigma (R8027)
//       Description:
//             Stop spirometry calculation when breath type in non-measured.
//
//  Revision: 011  By:  sp    Date: 24-Oct-1996    DR Number:
//       Project:  Sigma (R8027)
//       Description:
//             Inspection rework.
//
//  Revision: 010  By:  sp    Date:  9-Oct-1996    DR Number:600
//       Project:  Sigma (R8027)
//       Description:
//             Removed unit test methods.
//
//  Revision: 009  By:  sp    Date:  27-Aug-1996    DR Number: DCS 1264, 1260
//       Project:  Sigma (R8027)
//       Description:
//             Fixed overflow calculation in total respiratory rate.
//
//  Revision: 008  By:  iv    Date:  6-Jul-1996    DR Number: DCS 1116, 10004
//       Project:  Sigma (R8027)
//       Description:
//             Added a call to update the BTPS factor: Btps::CalculateBtps();
//
//  Revision: 007  By:  sp    Date:  6-Jun-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Added requirement number 4057.
//
//  Revision: 006  By:  sp    Date:  8-Apr-1996    DR Number: DCS 786
//       Project:  Sigma (R8027)
//       Description:
//             Limit total minute volume, spontaneous minute volume
//             and respiratory rate to 8 breaths.
//
//  Revision: 005  By:  iv    Date:  4-Mar-1996    DR Number: DCS 674
//       Project:  Sigma (R8027)
//       Description:
//             Remove #ifndef.
//
//  Revision: 004  By:  sp   Date:  7-Feb-1995    DR Number: DCS 701
//       Project:  Sigma (R8027)
//       Description:
//             Fix bug in calculation of respiratoryRate_.
//
//  Revision: 003  By:  sp   Date:  31-Dec-1995    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Change the lastUpdateTS_ type to OsTimeStamp.
//             Change BreathPhaseScheduler to SchedulerId.
//             Change PatientTypeValue to PatientCctTypeValue.
//             Change timeElpased to timeElapsed.
//             Add unit test methods.
//
//  Revision: 002  By:  sp   Date:  15-Dec-1995    DR Number: DCS 624
//       Project:  Sigma (R8027)
//       Description:
//             Use method getInspSideMinVol from BreathRecord to calculate
//             total inspiration side minute volume.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "AveragedBreathData.hh"

//@ Usage-Classes
#include "MathUtilities.hh"
#include "BreathSetIterator.hh"
#include "SchedulerId.hh"
#include "BreathSet.hh"
#include "BreathPhaseType.hh"
#include "PhasedInContextHandle.hh"
#include "HumidTypeValue.hh"
#include "ExhFlowSensor.h"
#include "MainSensorRefs.hh"
#include "BreathMiscRefs.hh"
#include "IntervalTimer.hh"
#include "MsgQueue.hh"
#include "PatientDataMgr.hh"
#include "AveragedBDPacket.hh"
#include "Btps.hh"
#include "MandTypeValue.hh"
#include "ModeValue.hh"
#include "SettingId.hh"
#include "BreathMiscRefs.hh"
#include "CircuitCompliance.hh"
#include "PatientCctTypeValue.hh"
#include "SupportTypeValue.hh"
#include "ProxManager.hh"

#ifdef SIGMA_UNIT_TEST
#include "AveragedBreathData_UT.hh"
#endif // SIGMA_UNIT_TEST

//@ End-Usage

//@ Code...

#ifdef SIGMA_UNIT_TEST
 
#include <stdio.h>
 
#endif //SIGMA_UNIT_TEST

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: AveragedBreathData()  
//
//@ Interface-Description
//	The constructor takes an interval timer as an argument. It initializes the
//	interval timer reference and the time stamp which are both members of the class.
//---------------------------------------------------------------------
//@ Implementation-Description
//	The data member rAvgBreathDataTimer_ and lastUpdateTS_ are initialized.
//	The TimerTarget base class is constructed.
//---------------------------------------------------------------------
//@ PreCondition
//	MAX_INSP_TIME_MS < 10000
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

AveragedBreathData::AveragedBreathData(IntervalTimer& rTimer)
: rAvgBreathDataTimer_(rTimer),
  TimerTarget()
{
  CALL_TRACE("AveragedBreathData()");

  CLASS_PRE_CONDITION(MAX_INSP_TIME_MS < 10000);
  lastUpdateTS_.now();
  spontFtotVtRatio_ = 0.0 ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~AveragedBreathData()  
//
//@ Interface-Description
//	Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

AveragedBreathData::~AveragedBreathData(void)
{
  CALL_TRACE("~AveragedBreathData()");

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateAveragedBreathData
//
//@ Interface-Description
//	This method takes Int32 msg parameter and has no return value.
//	The msg is then passed to BreathSetIterator object.
//	The purpose of that method is to perform spirometric calculations for
//	breath parameters.
//	This method is called at every ten seconds or every inspiration, whichever
//	comes first. Exhale minute volume, exhale spontaneous minute volume,
//	inspired minute volume, and total respiratory rate are calculated.
//	Inspired minute volume is used to update the dry factor of the 
//	flow sensor and the rest of the data are sent to the Patient Data
//	subsystem.
//
//	Additionally, this method calculates the f/Vt ratio.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Calculation for minute volume must be compliance and BTPS 
//	compensated. Exhaled minute volume is stored in exhMinuteVol_.
//      Exhaled spontaneous minute volume is stored in exhSpontMinuteVol_.
//      Total respiratory rate is stored in respiratoryRate_. The data
//      member inspMinuteVolume_ is used to store the total inspiration 
//	side minute volume.
//	The calculations for exhSpontMinuteVol_, respiratoryRate_ and
//	exhMinuteVol_ are based on a group of previous breaths or one minute period,
//	which ever come first.
//	When the one minute period includes a partial breath, all the
//	calculations described above will be normalized to one minute.
//	There is a special case when updateAveragedBreathData is invoked 
//	by 10 sec limitation then the expired volume in current breath 
//	record has to be compensated for compliance and BTPS.
//	The calculation of f/Vt is based on a spontanous respiratory
//	rate calculation but only in SPONT mode.  It also does not follow
//	the 10 second update rule.
//	$[03030] $[03031] $[03034] $[03035] $[03065] $[03066] $[04052] 
//      $[04053] $[04057] $[03042] $[03051] $[VC03002]
//---------------------------------------------------------------------
//@ PreCondition
//      (pBreathRecord != NULL);
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
static const Int32 NUM_BREATH_REC_LIMIT = 8;
static const Int32 NUM_BREATH_REC_LIMIT_PAV = 16;

void
AveragedBreathData::updateAveragedBreathData(const Int32 msg)
{
    CALL_TRACE("updateAveragedBreathData(const Int32 msg)");

    static Real32 LastMeanPressure_;


    Int32 numBreaths = NUM_BREATH_REC_LIMIT;  // default to normal number of breaths
	    
    unsigned long timeElapsed = lastUpdateTS_.getPassedTime();
    AveragedBDPacket avgDataPacket;
    BreathSetIterator breathSetIterator;
    breathSetIterator.reset(msg);
    BreathPhaseType::PhaseType phaseType = (BreathPhaseType::PhaseType) ((Uint32)msg >> 30);
 
    Real32 totalDuration = 0;
    Real32 totalSpontDuration = 0;
    Real32 totalMinuteVolDuration = 0;
    Real32 totalRespRateDuration = 0;
    Int32 allBreathsCounter = 0;
    Int32 spontBreathCounter = 0;
    BreathRecord* pBreathRecord = breathSetIterator.getPreviousBreathRecord();

    Real32 mandFraction = 1.0 ;

    // There should always be at least one non NULL breath record. This is guaranteed
    // by setting the initial number of breath records
    // in BreathSet::checkSettingChange_ to 1 for a tidal volume change.
    CLASS_PRE_CONDITION(pBreathRecord != NULL);

    Boolean updateSpontFtotVtRatio = FALSE ;
    Boolean tenSecondUpdate = FALSE;

	// Determine number of breaths to use in the averages based on the breath type
 	if (PhasedInContextHandle::GetDiscreteValue(SettingId::MODE) == 
 			ModeValue::SPONT_MODE_VALUE &&
		PhasedInContextHandle::GetDiscreteValue(SettingId::SUPPORT_TYPE) ==
			SupportTypeValue::PAV_SUPPORT_TYPE)
	{
		numBreaths = NUM_BREATH_REC_LIMIT_PAV;
	}
	else
	{
		numBreaths = NUM_BREATH_REC_LIMIT;
	}
		
    if (timeElapsed > 500)
    {
#ifdef SIGMA_UNIT_TEST
				AveragedBreathData_UT::TI13 = TRUE ;
#endif // SIGMA_UNIT_TEST				
        // $[TI13]
        SchedulerId::SchedulerIdValue schedId = pBreathRecord->getSchedulerId();
        // $[04213] $[04223]
        CLASS_PRE_CONDITION(schedId >= SchedulerId::FIRST_BREATHING_SCHEDULER &&
                            schedId <= SchedulerId::LAST_BREATHING_SCHEDULER );

        //if it is exhalation phase, then this method must have been invoked
        //by the 10 sec limitation. The exhMinuteVol_ has to be compliance and BTPS 
        //compensated.
        if ( (phaseType == BreathPhaseType::EXHALATION) ||
             (phaseType == BreathPhaseType::NIF_PAUSE) ||
             (phaseType == BreathPhaseType::P100_PAUSE) ||
             (phaseType == BreathPhaseType::EXPIRATORY_PAUSE) )
        {
#ifdef SIGMA_UNIT_TEST
				AveragedBreathData_UT::TI1 = TRUE ;
#endif // SIGMA_UNIT_TEST				
            // $[TI1]
            respiratoryRate_ = 1; //is also used as a counter for NUM_BREATH_REC_LIMIT
       	    meanCircuitPressure_ = LastMeanPressure_;      //covers the case when a setting change occured
			allBreathsCounter = 1;
			inspMinuteVolume_ = pBreathRecord->getInspSideMinVol();
			Real32 inspTime = pBreathRecord->getInspTime();

			const DiscreteValue  CIRCUIT_TYPE =
			PhasedInContextHandle::GetDiscreteValue(SettingId::PATIENT_CCT_TYPE);

			tenSecondUpdate = TRUE;
            
            if ((pBreathRecord->getMandatoryType() != MandTypeValue::VCV_MAND_TYPE) &&
                 (CIRCUIT_TYPE != PatientCctTypeValue::NEONATAL_CIRCUIT))
            {
               // $[TI23]
#ifdef SIGMA_UNIT_TEST
				AveragedBreathData_UT::TI23 = TRUE ;
#endif // SIGMA_UNIT_TEST				
               
              inspTime *= PCV_TIME_FACTOR;
             }
               // $[TI24]

			if (pBreathRecord->getIsProxFlowBreath() == TRUE)
			{

				exhMinuteVol_ = (pBreathRecord->getProxExpiredVolume());

				if (exhMinuteVol_ < 0.0f)
				{
					exhMinuteVol_ = 0.0f;
				}


			}
			else
			{
            exhMinuteVol_ = pBreathRecord->calculateComplianceBtpsVol(
                    pBreathRecord->getExpiredVolume(),
                    BreathRecord::GetEndInspPressureComp(),
                    BreathRecord::GetEndExpPressureComp(),
                    (Int32)inspTime) ;
			}


            mandFraction = pBreathRecord->getMandatoryVolumeFraction() ;

            totalDuration = pBreathRecord->getBreathDuration();
            totalMinuteVolDuration = totalDuration;
            totalRespRateDuration= totalDuration;
            totalSpontDuration = totalDuration;
            exhSpontMinuteVol_ = 0.0F;

            if (!IsEquivalent( mandFraction, 1.0, THOUSANDTHS))
            {
                // $[TI9]
#ifdef SIGMA_UNIT_TEST
				AveragedBreathData_UT::TI9 = TRUE ;
#endif // SIGMA_UNIT_TEST				

                exhSpontMinuteVol_ = exhMinuteVol_ * (1.0f - mandFraction) ;
                spontBreathCounter = 1;
            }
            // $[TI10]

			if (pBreathRecord->isNoInspiration())
			{
	            // $[TI34]
				// if current breath is a no inspiration, skip the next breath record since the
				// no inspiration breath records already contains the info of the next breath
	            pBreathRecord = breathSetIterator.getPreviousBreathRecord();
    	        pBreathRecord = breathSetIterator.getPreviousBreathRecord();

#ifdef SIGMA_UNIT_TEST
				AveragedBreathData_UT::TI34 = TRUE ;
#endif // SIGMA_UNIT_TEST				
			}
			else
			{
	            // $[TI35]
    	        pBreathRecord = breathSetIterator.getPreviousBreathRecord();
			}
        }
        else
        {
            // $[TI2]
            exhMinuteVol_ = 0.0F;
            exhSpontMinuteVol_ = 0.0F;
            respiratoryRate_ = 0;
            inspMinuteVolume_ = 0.0F;
            meanCircuitPressure_ = 0.0F;
            totalDuration = 0;
            allBreathsCounter = 0;
            totalSpontDuration = 0;
            totalMinuteVolDuration = 0;
            spontBreathCounter = 0;
            pBreathRecord = breathSetIterator.getPreviousBreathRecord();
        }

        if (pBreathRecord != NULL)
        {
#ifdef SIGMA_UNIT_TEST
				AveragedBreathData_UT::TI3 = TRUE ;
#endif // SIGMA_UNIT_TEST				
            // $[TI3]

			// last breath was a spont, update spont f/Vt calculation flag
            if (pBreathRecord->getBreathType() == ::SPONT)
            {
	            // $[TI38]
				updateSpontFtotVtRatio = TRUE ;
            }
            // $[TI41]
            
            schedId = pBreathRecord->getSchedulerId();
            while ( (totalDuration < 60000) && (pBreathRecord != NULL) &&
                    (allBreathsCounter < NUM_PREV_BREATH_RECORDS) &&
                    (respiratoryRate_ < numBreaths ||
                     spontBreathCounter < numBreaths) &&
                    (pBreathRecord->getBreathType() != NON_MEASURED &&
                     schedId >= SchedulerId::FIRST_BREATHING_SCHEDULER &&
                     schedId <= SchedulerId::LAST_BREATHING_SCHEDULER ) )
            {
#ifdef SIGMA_UNIT_TEST
				AveragedBreathData_UT::TI4 = TRUE ;
#endif // SIGMA_UNIT_TEST				
                // $[TI4]
                totalDuration += pBreathRecord->getBreathDuration();
	            mandFraction = pBreathRecord->getMandatoryVolumeFraction() ;
	            Boolean isProxFlowBreath = pBreathRecord->getIsProxFlowBreath();

#ifdef SIGMA_UNIT_TEST
				if (AveragedBreathData_UT::Test8)
				{
					respiratoryRate_ = NUM_BREATH_REC_LIMIT ;
				}
#endif // SIGMA_UNIT_TEST				

                if ( respiratoryRate_ < numBreaths )
                {
#ifdef SIGMA_UNIT_TEST
				AveragedBreathData_UT::TI25 = TRUE ;
#endif // SIGMA_UNIT_TEST				
                    // $[TI25]
                	Real32 exhVol;

					if (isProxFlowBreath == TRUE)
					{
                        exhVol = pBreathRecord->getProxExpiredVolume();
						if (exhVol < 0.0f)
						{
                            exhVol = 0.0f;
						}
					}
					else
					{
						exhVol = pBreathRecord->getComplianceExhVolume();
					}

					exhMinuteVol_ += exhVol;

                    inspMinuteVolume_ += pBreathRecord->getInspSideMinVol();
                    meanCircuitPressure_ += pBreathRecord->getMeanAirwayPressure();
                    totalMinuteVolDuration = totalDuration;
                    totalRespRateDuration = totalDuration;
                    respiratoryRate_++; 
                }
                    // $[TI28]
                if (!IsEquivalent( mandFraction, 1.0, THOUSANDTHS) &&
                    spontBreathCounter < numBreaths)
                {
                    // $[TI11]
#ifdef SIGMA_UNIT_TEST
				AveragedBreathData_UT::TI11 = TRUE ;
#endif // SIGMA_UNIT_TEST				

					if (isProxFlowBreath == TRUE)
					{
						Real32 exhMinuteVolTemp = (pBreathRecord->getProxExpiredVolume()) ;
						if (exhMinuteVolTemp < 0.0f)
						{
                            exhMinuteVolTemp = 0.0f;
						}

						exhSpontMinuteVol_ += ( exhMinuteVolTemp * (1.0f - mandFraction)) ;

					}
					else
					{

                    exhSpontMinuteVol_ += (pBreathRecord->getComplianceExhVolume() * (1.0f - mandFraction)) ;
					}

                    totalSpontDuration = totalDuration;
                    spontBreathCounter++;
                }
                    // $[TI29]

				if (pBreathRecord->isNoInspiration())
				{
		            // $[TI30]
					// if current breath is a no inspiration, skip the next breath record since the
					// no inspiration breath records already contains the info of the next breath
	            	pBreathRecord = breathSetIterator.getPreviousBreathRecord();
	            	
	    	        pBreathRecord = breathSetIterator.getPreviousBreathRecord();
#ifdef SIGMA_UNIT_TEST
				AveragedBreathData_UT::TI30 = TRUE ;
#endif // SIGMA_UNIT_TEST				
				}
				else
				{
		            // $[TI31]
    	        	pBreathRecord = breathSetIterator.getPreviousBreathRecord();
				}

                if (pBreathRecord != NULL)
                {
                    // $[TI19]
#ifdef SIGMA_UNIT_TEST
				AveragedBreathData_UT::TI19 = TRUE ;
#endif // SIGMA_UNIT_TEST				
                    schedId = pBreathRecord->getSchedulerId();
                }
                // $[TI20]
                allBreathsCounter++;
                
            } 
            // $[TI5]

            BreathRecord *pLastBreathRecord = pBreathRecord;
            pBreathRecord = breathSetIterator.getPrevRespRateBreathRecord();

            // if the previous loop is not terminated by totalDuration, numBreaths 
            // breath records has been used or non-matching schedId, then continue to 
            // execute the second loop.
            // The pLastBreathRecord has to be null to execute the 2nd loop.
            if ( (pBreathRecord != NULL) && (pLastBreathRecord == NULL) )
            {
#ifdef SIGMA_UNIT_TEST
				AveragedBreathData_UT::TI15 = TRUE ;
#endif // SIGMA_UNIT_TEST				
                // $[TI15]
                //The case when tidal volume changed occured after respiratory 
                //rate change.
                //If previous loop totalDuration already exceed 60000 msec
                //or the number of breath records used has reached numBreaths,
                //then the foolowing loop will not be executed.
                schedId = pBreathRecord->getSchedulerId();
                while ( totalRespRateDuration < 60000 &&
                        pBreathRecord != NULL &&
                        respiratoryRate_ < numBreaths &&
                        pBreathRecord->getBreathType() != NON_MEASURED &&
                        schedId >= SchedulerId::FIRST_BREATHING_SCHEDULER &&
                        schedId <= SchedulerId::LAST_BREATHING_SCHEDULER )
                {
                    // $[TI16]
                    respiratoryRate_ ++;
                    meanCircuitPressure_ += pBreathRecord->getMeanAirwayPressure();
                    totalRespRateDuration += pBreathRecord->getBreathDuration();

					if (pBreathRecord->isNoInspiration())
					{
			            // $[TI32]
						// if current breath is a no inspiration, skip the next breath record since the
						// no inspiration breath records already contains the info of the next breath
                	    pBreathRecord = breathSetIterator.getPrevRespRateBreathRecord();
                    	pBreathRecord = breathSetIterator.getPrevRespRateBreathRecord();
#ifdef SIGMA_UNIT_TEST
				AveragedBreathData_UT::TI32 = TRUE ;
#endif // SIGMA_UNIT_TEST				
					}
					else
					{
			            // $[TI33]
            	        pBreathRecord = breathSetIterator.getPrevRespRateBreathRecord();
					}

                    if (pBreathRecord != NULL)
                    {
                        // $[TI21]
#ifdef SIGMA_UNIT_TEST
				AveragedBreathData_UT::TI21 = TRUE ;
#endif // SIGMA_UNIT_TEST				
                        schedId = pBreathRecord->getSchedulerId();
                    }
                    // $[TI22]
                }
                // $[TI17]
            }
            // $[TI18]
        }
        // $[TI8]

        if (totalDuration > 0)
        {
            // $[TI6]
#ifdef SIGMA_UNIT_TEST
				AveragedBreathData_UT::TI6 = TRUE ;
#endif // SIGMA_UNIT_TEST

			Real32 spontExhVolSum = exhSpontMinuteVol_ ;
			
            if (totalSpontDuration > 0)
            {
            // $[TI26]
#ifdef SIGMA_UNIT_TEST
				AveragedBreathData_UT::TI26 = TRUE ;
#endif // SIGMA_UNIT_TEST				
                exhSpontMinuteVol_ = exhSpontMinuteVol_ * 60.0F / totalSpontDuration;
            }
            else
            {
            // $[TI27]
                exhSpontMinuteVol_ = 0.0F;
            }             
            exhMinuteVol_ = exhMinuteVol_ * 60.0F / totalMinuteVolDuration;
            inspMinuteVolume_ = inspMinuteVolume_ * 60.0F / totalMinuteVolDuration;
            meanCircuitPressure_ = meanCircuitPressure_ / respiratoryRate_;          //respiratoryRate_ at this point is a breath count
            LastMeanPressure_ = meanCircuitPressure_;
            respiratoryRate_ = respiratoryRate_ * 60000.0F / totalRespRateDuration;
            
		    DiscreteValue mode = PhasedInContextHandle::GetDiscreteValue(SettingId::MODE) ;

	    // update if last breath was a spont breath and this is not a 10 second update
	    if ( ( (mode == ModeValue::SPONT_MODE_VALUE) || (mode == ModeValue::CPAP_MODE_VALUE) )
	         && updateSpontFtotVtRatio && !tenSecondUpdate)
	    {
		// $[TI36]
		Real32 spontRespRate = respiratoryRate_ ;
				
		if (spontBreathCounter > 0 && spontExhVolSum > 0.0)
            	{
			// $[TI39]
			Real32 spontTidalVolume = spontExhVolSum / spontBreathCounter / 1000.0f ;
			spontFtotVtRatio_ = spontRespRate / spontTidalVolume ;  // Result is in Bpm/L
		}
	    // $[TI40]
	    }
	    // $[TI37]

            // update the dry factor with the new value for the inspMinuteVolune_:
            updateDryFactor_();
            lastUpdateTS_.now();

            // update the BTPS factors
            Btps::CalculateBtps();

			// Store the current respiratory rate
			prevRespiratoryRate_ = respiratoryRate_;

            // Communication to the Gui.
            avgDataPacket.exhaledMinuteVol = MAX_VALUE(exhMinuteVol_, 0.0F);
            avgDataPacket.exhaledSpontMinuteVol = MAX_VALUE(exhSpontMinuteVol_, 0.0F);
            avgDataPacket.totalRespRate = respiratoryRate_;
            avgDataPacket.meanCircuitPressure = meanCircuitPressure_;
            avgDataPacket.spontFtotVtRatio = spontFtotVtRatio_;
            PatientDataMgr::NewAveragedBreathData(&avgDataPacket,sizeof(avgDataPacket));
        }
        // $[TI7]

    }
    // $[TI14]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: timeUpHappened
//
//@ Interface-Description
//  It takes an interval timer as an argument and has no
//  return value. The method posts a msg extracted from BreathSet to 
//  SPIROMETRY_TASK_Q.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The object referenced to by the member rAvgBreathDataTimer_ is 
//  instructed to restart. If the current scheduler id is valid,
//  and it is not peep recovery breath, a msg extracted from BreathSet
//  to SPIROMETRY_TASK_Q.
// $[03030] $[03034] $[03065]
//---------------------------------------------------------------------
//@ PreCondition
//  (&rAvgBreathDataTimer_ == &timer)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
AveragedBreathData::timeUpHappened(const IntervalTimer& timer)
{
    CALL_TRACE("AveragedBreathData::timeUpHappened(const IntervalTimer& timer)");
    CLASS_PRE_CONDITION(&rAvgBreathDataTimer_ == &timer);
 
    SchedulerId::SchedulerIdValue schedulerId =
	(RBreathSet.getCurrentBreathRecord())->getSchedulerId();

    // notify that the spirometry task can start
    if ((schedulerId != SchedulerId::DISCONNECT) &&
        (schedulerId != SchedulerId::STANDBY) &&
        (schedulerId != SchedulerId::OCCLUSION) &&
        (schedulerId != SchedulerId::POWER_UP) &&
        (schedulerId != SchedulerId::SVO) &&
         !BreathRecord::IsPeepRecovery())
    {
       // $[TI1]
       MsgQueue::PutMsg(::SPIROMETRY_TASK_Q, RBreathSet.getQueMsg());
    }
    // $[TI2]

    rAvgBreathDataTimer_.restart();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getPrevRespiratoryRate
//
//@ Interface-Description
//   This method takes no parameters. It returns the value of the 
//   previous respiratory rate for mandatory and spont breaths.
//---------------------------------------------------------------------
//@ Implementation-Description
//   Returns the value of  data member prevRespiratoryRate_.
//---------------------------------------------------------------------
//@ PreCondition
//   none.
//---------------------------------------------------------------------
//@ PostCondition
//   none.
//@ End-Method
//=====================================================================
Real32 AveragedBreathData::getPrevRespiratoryRate(void)
{
	return prevRespiratoryRate_;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
AveragedBreathData::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, AVERAGEDBREATHDATA,
                          lineNumber, pFileName, pPredicate);
}



//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//=====================================================================
//
//  Private Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateDryFactor_
//
//@ Interface-Description
//	This method takes no arguments and has no return value.
//	This method is called to update the dry factor of the flow sensors
//	when the averaged breath data calculation is completed.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Calculates the dry factor based on the humidification type, 
//	patient type and inspired minute volume. the result is then used
//	as an argument in the static method FlowSensor::SetDryFactor.
// $[04051]
//---------------------------------------------------------------------
//@ PreCondition
//	None
//---------------------------------------------------------------------
//@ PostCondition
//	None
//@ End-Method
//=====================================================================
void
AveragedBreathData::updateDryFactor_(void)
{
    CALL_TRACE("AveragedBreathData::updateDryFactor_(void)");
 
    Real32 factor;

    DiscreteValue humidType =  
	PhasedInContextHandle::GetDiscreteValue(SettingId::HUMID_TYPE);

	if (humidType == HumidTypeValue::HME_HUMIDIFIER)
    {
    // $[TI1]
        factor = 1.0F;
    }
    else
    {
    // $[TI2]
	  	CLASS_PRE_CONDITION( humidType == HumidTypeValue::NON_HEATED_TUBING_HUMIDIFIER ||
							humidType == HumidTypeValue::HEATED_TUBING_HUMIDIFIER) ;
		factor = 0.92 ;
	}

// E600 BDIO    RExhFlowSensor.setDryFactor(factor);
}

#ifdef SIGMA_UNIT_TEST
void
AveragedBreathData::write(void)
{
  CALL_TRACE("write(void)");
 
  printf("---- Class AveragedBreathData -----\n");
  printf("exhSpontMinuteVol_:%f\n",exhSpontMinuteVol_);
  printf("exhMinuteVol_:%f\n",exhMinuteVol_);
  printf("inspMinuteVolume_:%f\n",inspMinuteVolume_);
  printf("respiratoryRate_:%f\n",respiratoryRate_);
}
 
#endif //SIGMA_UNIT_TEST
