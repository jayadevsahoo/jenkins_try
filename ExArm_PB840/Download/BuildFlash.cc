#include "stdafx.h"
#if		defined(SIGMA_SUN)
//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: BuildFlash - Class definition that builds an absolute 
//    Flash memory image from a set of Motorola Srecord format
//    object files.
//---------------------------------------------------------------------
//@ Interface-Description
//    The class recognizes three target flash memory types,
//    Sigma BD, Sigma GUI and Sigma Boot prom.	
//	  It builds a complete executable image for a given type target
//	  type.  The un-initialized portions of a target flash memory by
//	  the given S records are filled with a predefined OxFF 
//    fill pattern. The class also detects if there is any
//	  over-lapped address between input S records.
//    A checksum table is built based on the flash memory image built
//	  at the end of a build process and is added to the end of the
//    flash image file.
//	   
//---------------------------------------------------------------------
//@ Rationale
//	 This class encapsulates all the activities to build a Sigma target
//   flash memory image.
//---------------------------------------------------------------------
//@ Implementation-Description
//	 Depending on the target flash memory type, size and starting
//	 address of the flash memory differ.  A large block of dynamic
//   memory is allocated to hold a complete flash memory image.
//   The entire memory is initialized with the 0xFF fill pattern initially
//   and then image data parsed in from the input Srecord files are placed
//   on the memory.
//---------------------------------------------------------------------
//@ Fault-Handling
//  A standard pre-condition test is used to ensure correctness.
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Download/vcssrc/BuildFlash.ccv   25.0.4.0   19 Nov 2013 14:02:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005   By:   rhj    Date: 05-March-2007      DR Number: 6359
//  Project:  RESPM
//  Description:
//        Resolve merge conflict for RM to Baseline 
//
//  Revision: 004  By: gdc    Date: 22-Feb-05    DR Number: 6148
//  Project:  NIV1
//  Description:
//       DCS 6148 - move to solaris
//       Corrected bad literal flagged by GNU compiler as error.
//
//  Revision: 003  By: sah    Date: 15-Jan-99    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 002  By: Gary Cederquist  Date: 28-JUL-1997  DR Number: 2197
//       Project:  Sigma (R8027)
//       Description:
//             Changed to use SigmaTypes.hh and MemoryMap.hh
//
//  Revision: 001  By:  <jhv>    Date:  22-Jan-96    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//=====================================================================

#include "BuildFlash.hh"
#include "DownloadTypes.hh"
#include "DownloadFault.hh"
#include "FlashMemCons.hh"
#include "BootFile.hh"
#include "cktable.hh"
#include <memory.h>
#include <string.h>

//=====================================================================
//
//  Static Data Initialization...
//
//=====================================================================


static TypeTable tTable[] = 
{
	{ "bd", BD_FLASH },
	{ "BD", BD_FLASH },
	{ "BD_CPU", BD_FLASH },
	{ "gui", GUI_FLASH },
	{ "GUI", GUI_FLASH },
	{ "GUI_CPU", GUI_FLASH },
	{ "prom", BOOT_PROM },
	{ "PROM", BOOT_PROM },
	{ "COMMON", BOOT_PROM },
	{ NULL, NO_TARGET  },
};

TypeTable *BuildFlash::PtypeTable_ = tTable;

char *BuildFlash::ProgName;
static Byte Data_[DATA_BLOCK_SIZE];

//@ Code...
//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BuildFlash()  [Constructor]
//
//@ Interface-Description
//    Initialize the instance variables to appropriate initial values.
//---------------------------------------------------------------------
//@ Implementation-Description
//    Depending on the target type the flash image is built for,
//    a flash memory size, the checksum table address  and flash
//    memory starting address are computed based on the configuration
//    values.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
BuildFlash::BuildFlash(TargetType type,char **sRecordFiles, char *oFile,
	OutputImageFormat oForm, NoChkSumRange range, Boolean perform)
{
	type_ = type;
	pFileNames_ = sRecordFiles;
	pImageMem_ = NULL;
	flashEndImageAddr_ = NULL;
	pOutFileName_ = oFile;
	outputFormat_ = oForm;
	exclude_ = range;
	performance_ = perform;

	switch(type)
	{
		case BD_FLASH:
			flashStartAddr_ = (Byte *) FLASH_CHECKSUM_BASE; 
			downloadSize_ = BD_FLASH_MEM_SIZE - SERVICE_DATA_SPACE;
			flashEndAddr_ = flashStartAddr_ + downloadSize_;
			break;

		case GUI_FLASH:
			flashStartAddr_ = (Byte *) FLASH_CHECKSUM_BASE;
			downloadSize_ = GUI_FLASH_MEM_SIZE - SERVICE_DATA_SPACE;
			flashEndAddr_ = flashStartAddr_ + downloadSize_;
			break;

		case BOOT_PROM:
			flashStartAddr_ = (Byte *) BOOT_PROM_BASE;
			downloadSize_ = BOOT_PROM_LENGTH;
			flashEndAddr_ = flashStartAddr_ + downloadSize_;
			break;
		default:
			CLASS_ASSERTION(0);
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~BuildFlash()  [Destructor]
//
//@ Interface-Description
//   Default Destructor. Free the dynamic memory if still not freed.
//---------------------------------------------------------------------
//@ Implementation-Description
//	 none.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

BuildFlash::~BuildFlash(void)
{
	if ( pImageMem_ )
	{
		free( pImageMem_ );
	}
	pImageMem_ = NULL;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: task(void)
//
//@ Interface-Description
//    The method provides a main task loop for the Flash Image build
//	  object. The task mainly allocates a large memory to hold
//    a complete flash memory image. Then opens up individual
//    input Srecord files and parses Srecord data into the in-memory
//    flash image.
//    At the end, a checksum table is built based on the in-memory
//    flash image. The checksum table is written out as a part of the 
//    downloadable image to an output file.
//---------------------------------------------------------------------
//@ Implementation-Description
//    In order to speed up parsing of the input Srecord files, file is read
//    in 4K byte block at a time and one input Ascii character at a time
//    is passed down to the Srecord parser object.
//    The Srecord parser knows parses the input data and locates image
//    data into the in-memory flash image.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
SigmaStatus
BuildFlash::task(void)
{
char *pFile;
Int i, j,  size;

	sParser_.setBuilder(this);
	initMemory_();
	for ( i = 0; pFile = pFileNames_[i]; i++)
	{
		if ( inputFile_.open(pFile) == FAILURE )
		{
			PrintErr("Failed in opening file, %s\n", pFile);
			return(FAILURE);
		}
		else
		{
		    printf("%s file size %d.\n", inputFile_.getFileName(),
			inputFile_.getFileSize());
		}
		sParser_.initStates();
		while( ( size = inputFile_.read( Data_, DATA_BLOCK_SIZE )) > 0 )
		{
			for(j= 0; j< size; j++)
			{
				if ( sParser_.parse(Data_[j] ) == FAILURE )
				{
					PrintErr("Parsing error in the %s file\n", pFile);
					return(FAILURE);
				}
			}
		}
		if ( size == 0 )
		{
			if (sParser_.endOfSfile() == FALSE )
			{
				PrintErr("The %s input file doesn't contain an ending S record\n", pFile);
				return(FAILURE);
			}
		}else if ( size < 0 )
		{
			PrintErr("IO error while reading the input file, %s\n", pFile);
			return(FAILURE);
		}
	}

	if ( type_ == BOOT_PROM )
	{
		compBootpromChkSum_();
	}
	else
	{
		buildChkSumTbl_();
	}
	if ( writeImageToFile_() == FAILURE )
	{
		return(FAILURE);
	}
	return(SUCCESS);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: writeImageToFile_(void)
//
//@ Interface-Description
//	  Write out the flash image in the memory to a specified output
//    file. For BD and GUI flash memory type, the binary format
//    data is sequentially written out. The first 1k bytes contains
//    
//--------------------------------------------------------------------
//@ Implementation-Description
//    If the performance flag is true, do not write out the entire
//    image, only up to the highest memory that is not fill characters.
//    The flash memory is expected to be filled with the FILL 
//    characters each flash memory block is erased.
//---------------------------------------------------------------------
//@ PreCondition
//	 	none.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
SigmaStatus
BuildFlash::writeImageToFile_(void)
{
	if ( outputFile_.creat(pOutFileName_) < 0 )
	{
		PrintErr("Cannot create the output file %s\n", pOutFileName_);
		return(FAILURE);
	}
	if ( type_ != BOOT_PROM )
	{
		if ( performance_ )
		{
			downloadSize_ =  flashEndImageAddr_ - flashStartAddr_ ;
			downloadSize_ = 
		 		((downloadSize_ + DATA_BLOCK_SIZE -1 )/DATA_BLOCK_SIZE)*DATA_BLOCK_SIZE;
		}
	}

	if ( outputFormat_ == BINARY_FORMAT )
	{
		if ( outputFile_.write(pImageMem_, downloadSize_) != downloadSize_)
		{
			PrintErr("an IO error while writing the image out to %s\n",
				pOutFileName_);
			outputFile_.close();
			return(FAILURE);
		}
	}
	else if ( writeImageAsSrecord_() == FAILURE )
	{
		outputFile_.close();
		return(FAILURE);
	}
	outputFile_.close();
	return(SUCCESS);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: convertAddr_(Byte *flashAddr)
//
//@ Interface-Description
//	  Convert a flash memory address to an equivalent address of
//    the in-memory flash image.
//--------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	 	none.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Byte *
BuildFlash::convertAddr_(Byte *fAddr)
{
Int offset; 

	CLASS_PRE_CONDITION((fAddr<=flashEndAddr_)&&(fAddr>=flashStartAddr_));

	offset = (Int)fAddr - (Int)flashStartAddr_;
	return(pImageMem_ + offset);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: writeImageAsSrecord_(void)
//
//@ Interface-Description
//	  Write out the absolute binary image in the memory in a Srecord
//    format file.
//    For BD and GUI flash memory type, the binary format
//    data is sequentially written out. The first 1k bytes contains
//    
//--------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	 	none.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
SigmaStatus
BuildFlash::writeImageAsSrecord_(void)
{
Byte record[MAX_SRECORD_SIZE];
Int remainder, rsize, size;
Byte *currFlashAddr, *currMemAddr;

	remainder = downloadSize_;
	currFlashAddr = flashStartAddr_;
	currMemAddr = pImageMem_;

    while( remainder > 0 )
	{
		if ( performance_ )
		{
			while(remainder && ( *currMemAddr == FLASH_FILL_CHAR )) // Skip the fill chars.
			{
				currMemAddr++;
				currFlashAddr++;
				remainder--;
			}
		}

		if ( remainder >= MAX_SRECORD_DATA_LENGTH )
		{
			size = MAX_SRECORD_DATA_LENGTH;
		}
		else
		{
			size = remainder;
		}
		rsize = sParser_.buildDataRecord(currFlashAddr, currMemAddr,
			&record[0], size);

		if ( outputFile_.write(record, rsize) != rsize ) 
		{
			PrintErr("Writing a Srecord encountered an IO error\n");
			return(FAILURE);
		}
		remainder -= size;
		currFlashAddr += size;
		currMemAddr += size;
	}
	size = sParser_.buildEofRecord(record);
	if ( outputFile_.write(record, size) != size ) 
	{
		PrintErr("Writing an EOF  Srecord encountered an IO error.\n");
		return(FAILURE);
	}
	return(SUCCESS);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: initMemory_(void)
//
//@ Interface-Description
//	  Dynamically allocate a memory block big enough to hold an 
//    absolute flash image and its checksum table.
//    Initialize the entire memory with a M68040 trap instruction
//    pattern.
//--------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	 	none.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
SigmaStatus
BuildFlash::initMemory_(void)
{
	if (!(pImageMem_ = (Byte *) malloc(downloadSize_)))
	{
		PrintErr("%d bytes of memory allocation failed\n",downloadSize_);
		return(FAILURE);
	}
	memset(pImageMem_, FLASH_FILL_CHAR, downloadSize_);
	return(SUCCESS);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: writeByte_(Byte *flashAddr, Byte data)
//
//@ Interface-Description
//     Write a data byte to the the in-memory flash memory image
//     is built. If a given flash memory address was already
//     written, the method returns an error status.
//--------------------------------------------------------------------
//@ Implementation-Description
//     The memory that holds the flash memory image being built
//     has been initialized with a prefixed pattern. The method
//     converts the given flash memory address to an in-memory
//     image address. If the in-memory address doesn't contain
//     a pre-filled pattern, it indicates there is an overwrap in
//     the S record addresses.
//
//     The code makes an assumption that data value is not same
//     as the memory fill pattern.
//---------------------------------------------------------------------
//@ PreCondition
//	 	none.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
SigmaStatus
BuildFlash::writeByte(Byte *targetAddr, Byte data )
{
Byte *pMemPtr;
char *pFileName;
static int i = 0;

	if ( targetAddr == NULL )
	{
		return(SUCCESS);
	}
	if ((targetAddr>flashEndAddr_) || (targetAddr<flashStartAddr_))
	{
		pFileName = inputFile_.getFileName();
		PrintErr("an out of range S record address 0x%X.\n",
			(Int)targetAddr);
		return(FAILURE);
	}
	pMemPtr = convertAddr_((Byte *)targetAddr);

	if ( targetAddr > flashEndImageAddr_ )
	{
		flashEndImageAddr_ = targetAddr;
	}
	if ( *pMemPtr != FLASH_FILL_CHAR )
	{
		pFileName = inputFile_.getFileName();
		PrintErr("contains an over-wrapping S record address 0x%X\n",
			 (Int)targetAddr);
		return(FAILURE);
	}
	*pMemPtr = data;
	return(SUCCESS);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: compBootpromChkSum_(void)
//
//@ Interface-Description
//	  Compute an addictive checksum value of the bootprom image.
//--------------------------------------------------------------------
//@ Implementation-Description
//    Compute an addictive checksum value of an entire bootprom 
//    image by adding up each word of a bootprom image from the
//    start address to the word before the last word.
//    The computed checksum value is put into the last word of
//    the bootprom image.
//---------------------------------------------------------------------
//@ PreCondition
//	 	none.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
BuildFlash::compBootpromChkSum_(void)
{
Int nWords;
Uint32 cValue, j, *pData;

		nWords =   BOOT_PROM_LENGTH/sizeof(Uint32);
		pData =  (Uint32 *) convertAddr_( flashStartAddr_ );
		cValue = 0;
		for ( j = 0; j< ( nWords - 1) ; j++)
		{
			cValue += *pData++;
		}
		*pData = cValue;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: buildChkSumTbl_(void)
//
//@ Interface-Description
//	  Build a checksum table for the flash memory image built.
//--------------------------------------------------------------------
//@ Implementation-Description
//    Depending on the target type being built, the size of the
//    flash memory is different. Flash memory is divided into blocks 
//    of memory, an additive checksum value is computed for a block
//    of memory. Portion of the flash memory that will hold its
//    checksum table is excluded from the checksum computation.
//
//    And each checksum table entry indicates the starting address
//    and size of the block. Most of the blocks are 256k bytes
//    long except those ones that are adjacent to the checksum
//    table and service mode calibration data. These are excluded
//    from checksum computation.
//    Depending on the flash memory size, a different number of
//    checksum table entries are computed.
//   
//    For GUI and BD target, the checksum table is located beginning
//    of the flash memory. For the boot prom, the checksum table is
//    located at the end of the flash memory. The method builds
//    the checksum table right into the memory that holds the
//    flash memory image built.
//    The convertAddr_ method converts a flash memory address to
//    the corresponding memory address that holds the flash memory
//    image.
//---------------------------------------------------------------------
//@ PreCondition
//	 	none.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
BuildFlash::buildChkSumTbl_(void)
{
Int i, nWords;
Uint32 cValue, j, *pData;
EepromStruct *pTbl;
Byte *pStartAddr, *pBlockEndAddr;
Int blockLength = CHECKSUM_BLOCK_SIZE;

	pStartAddr = flashStartAddr_;
	if ( type_ == BOOT_PROM )
	{
		pTbl=(EepromStruct *)(convertAddr_((Byte *)BOOT_PROM_CHECKSUM));
	}
	else
	{
		pTbl = (EepromStruct *) (convertAddr_(flashStartAddr_));
		pStartAddr += (Int) FLASH_CHECKSUM_SPACE;

	}

	pTbl->numChecksums=(downloadSize_ + blockLength -1)/blockLength;

	// Let's initialize each checksum memory block information in the
	// POST checksum table.
	for ( i= 0; i < pTbl->numChecksums; i++)
	{
		if ( i == 0 && ( type_ != BOOT_PROM ) )
		{
   			pTbl->checksumBlock[0].length = 
				blockLength - FLASH_CHECKSUM_SPACE - SERVICE_DATA_SPACE;
			if ( pTbl->checksumBlock[0].length <= 0 )
			{
				PrintErr("Service Data space too large");
				CLASS_ASSERTION(0);
			}
	    } else if ( ( type_ == BOOT_PROM ) && 
					(i == pTbl->numChecksums - 1) )
		{
      		pTbl->checksumBlock[i].length  =
				blockLength - FLASH_CHECKSUM_SPACE ;
		}
		else
		{
      		pTbl->checksumBlock[i].length  = blockLength;
		}

		// a block length should be a word boundary.
		CLASS_ASSERTION((pTbl->checksumBlock[i].length%sizeof(Int32))== 0);

      	pTbl->checksumBlock[i].pStartAddress    = 
	  			(Uint32 *) pStartAddr;
      	pTbl->checksumBlock[i].additiveChecksum = 0;
		pBlockEndAddr = pStartAddr+pTbl->checksumBlock[i].length-sizeof(Int32);
      	pStartAddr += pTbl->checksumBlock[i].length;

		// Do not allow the change of the checksum block  starting address.
		// If there is any overwrap between the exclusion area and the checksum
		// block address range. Do not checksum the entire block.
		if(exclude_.pStart)
		{
			if (( pTbl->checksumBlock[i].pStartAddress  <= exclude_.pEnd) &&
					( pBlockEndAddr >= (Byte *)exclude_.pStart)) 
			{

				if ( exclude_.pStart > pTbl->checksumBlock[i].pStartAddress )
				{
					pTbl->checksumBlock[i].length -=
								pBlockEndAddr - (Byte *)exclude_.pStart ;
					pTbl->checksumBlock[i].length  &= (Uint) 0xFFFFFFC0; //make mod 64.
					if ( exclude_.pEnd < (Uint32 *) pBlockEndAddr )
					{
						pTbl->checksumBlock[i].length = 0;
					}
				}	
				else // ( exclude_.pStart <= pTbl->checksumBlock[i].pStartAddress )
				{
					if ( pBlockEndAddr  >=  (Byte *) exclude_.pEnd )
					{
						pTbl->checksumBlock[i].length -=
								(Byte *)exclude_.pEnd - 
									(Byte *)pTbl->checksumBlock[i].pStartAddress;
						pTbl->checksumBlock[i].pStartAddress = exclude_.pEnd;
					}
					else
					{
						pTbl->checksumBlock[i].length = 0;
					}
				}
			}
		}
#if	defined(SIGMA_PRODUCTION)
		//CLASS_ASSERTION(pTbl->checksumBlock[i].length > 0);
#endif //defined(SIGMA_PRODUCTION)
	}

	for ( i= 0; i < pTbl->numChecksums; i++)
	{

		nWords =  pTbl->checksumBlock[i].length/sizeof(Uint32);
		pData =  (Uint32 *) convertAddr_(
			(Byte *)pTbl->checksumBlock[i].pStartAddress);
		cValue = 0;
		for ( j = 0; j< nWords; j++)
		{
			cValue += *pData++;
		}

#if	defined(SIGMA_UNIT_TEST)
		printf("i %d StartAddr %X length %d cValue %X\n", i,
			pTbl->checksumBlock[i].pStartAddress,
			pTbl->checksumBlock[i].length, cValue);
#endif //defined(SIGMA_UNIT_TEST)

      	pTbl->checksumBlock[i].additiveChecksum = cValue;
	}
	pTbl->endImageAddr = (Uint32 *) flashEndImageAddr_;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: PrintErr(char *pMsg)
//
//@ Interface-Description
//	  Print out an error message for the flash image builder program.
//--------------------------------------------------------------------
//@ Implementation-Description
//    Always, prefix the command name to a given error message.
//---------------------------------------------------------------------
//@ PreCondition
//	 	none.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
BuildFlash::PrintErr(char *pMsg)
{
	printf("%s : fatal error %s", ProgName, pMsg);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: PrintErr(char *pFormat, Int Arg)
//
//@ Interface-Description
//	  Print out an error message for the flash image builder program.
//--------------------------------------------------------------------
//@ Implementation-Description
//    Always, prefix the command name to a given error message.
//---------------------------------------------------------------------
//@ PreCondition
//	 	none.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
BuildFlash::PrintErr(char *pFormat, Int Arg)
{
	printf("%s : fatal error ", ProgName);
	printf(pFormat, Arg);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: PrintErr(char *pFormat, char *pArg)
//
//@ Interface-Description
//	  Print out an error message for the flash image builder program.
//--------------------------------------------------------------------
//@ Implementation-Description
//    Always, prefix the command name to a given error message.
//---------------------------------------------------------------------
//@ PreCondition
//	 	none.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
BuildFlash::PrintErr(char *pFormat, char *pArg)
{
	printf("%s : fatal error ", ProgName);
	printf(pFormat, pArg);
}
 
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: FindType(char *pName)
//
//@ Interface-Description
//	  Find a target that corresponds to a given name.
//--------------------------------------------------------------------
//@ Implementation-Description
//    Search through the target type table to find a target type.
//---------------------------------------------------------------------
//@ PreCondition
//	 	none.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
TargetType
BuildFlash::FindType(char *pName)
{
Int i;

	for ( i = 0; PtypeTable_[i].name; i++)
	{
		if ( strcmp(pName, PtypeTable_[i].name ) == 0 )
		{
			return(PtypeTable_[i].type);
		}
	}
	return(NO_TARGET);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 
void
BuildFlash::SoftFault(const FaultType  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  DownloadFault::SoftFault(softFaultID, DOWNLOAD,
  	BUILD_FLASH_CLASS, lineNumber, pFileName, pPredicate);
}

#endif //defined(SIGMA_SUN)
