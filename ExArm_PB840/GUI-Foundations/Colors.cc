#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: Colors - This class defines the colors used in the application.
//---------------------------------------------------------------------
//@ Interface-Description
// This static class stores the colors used by the GUI.  The ColorS enum
// lists all colors and flash combinations supported by GUI-Foundations.
// The Initialize() method initializes an internal color table.
//---------------------------------------------------------------------
//@ Rationale
// This class defines the colors that are used everywhere so that if a
// particular color needs to be modified, this is the only place the change
// needs to be made.  Exception: if the values assigned in the ColorS enum are
// changed, it may be necessary to make corresponding changes in the Image.cc
// file, because the initializers for color bitmaps include explicit numeric
// color values -- they don't refer to this enum.
//---------------------------------------------------------------------
//@ Implementation-Description
// This is a static class that encapsulates the Colors_[] table, which contains
// all colors supported by GUI-Foundations.  The Initialize() method must be
// called before any other methods are called to initialize the Colors_[] table
// and perform platform-specific setup (i.e., X-Windows vs VGA).  The
// Initialize() method uses low-level VGA routines to define the RGB values for
// each defined color.
//---------------------------------------------------------------------
//@ Fault-Handling
// This class uses the "contract programming" technique.  This technique
// utilizes the class assertion and class precondition macros provided by
// the Utilities subsystem to generate error messages in response to software
// faults.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Foundations/vcssrc/Colors.ccv   25.0.4.0   19 Nov 2013 14:11:40   pvcs  $
//
//@ Modification-Log
//
//  Revision: 017   By: mnr    Date: 24-Sep-2010    SCR Number: 6672
//  Project: XENA2
//  Description:
//      Using new method VgaGraphicsDriver::GetDisplayContext() to 
//      make backwards compatibility easier.
//
//  Revision: 016   By: mnr    Date: 03-Mar-2010    SCR Number: 6555
//  Project: NEO
//  Description:
//      Added new blink color for Screen lock message.
// 
//  Revision: 015   By: mnr    Date: 23-Feb-2010    SCR Number: 6555
//  Project:  NEO
//  Description:
//      Added YELLOW_ON_BLACK_BLINK_SLOW.
//
//  Revision: 014   By: rhj    Date: 10-Dec-2009    SCR Number: 6523
//  Project:  NEO
//  Description:
//      Added BLACK_ON_LIGHT_BLUE_BLINK.
//
//  Revision: 013   By: gdc    Date: 26-Jan-2009    SCR Number: 6407
//  Project:  840S
//  Description:
//      Modified to bring source into strict ARM compliance.
//
//  Revision: 012  By:  gdc    Date:  10-Dec-2008    SCR Number: 6377
//  Project:  840S
//  Description:
//	Removed DELTA project single screen support.
//
//  Revision: 011   By: gdc    Date: 07-Mar-2008   SCR Number: 6430
//  Project:  TREND2
//	Description:
//		Changed RgbQuad byte ordering from RGB to BGR for compatibity
//		with Microsoft RgbQuad definition.
//
//  Revision: 010   By: gdc    Date: 21-May-2007   SCR Number: 6330
//  Project:  Trend
//	Description:
//		Removed SUN prototype code.
//
//  Revision: 009   By: gdc    Date: 01-Feb-2007   SCR Number: 6237
//  Project:  Trend
//	Description:
//		Trend project related changes. Removed SUN prototype code.
//
//  Revision: 008   By: hlg    Date:04-Sep-2001      DCS Number: 5940
//  Project: GuiComms
//  Description:
//	* Made InitializeMap_ a class member.
//      * Rearranged methods to be under "Public" or "Private" method headers.
//
//  Revision: 007   By: gdc    Date:  29-Aug-2000    DCS Number: 5493
//  Project: GuiComms
//  Description:
//	Added Single Screen Compatibility.
//
//  Revision: 006   By: gdc    Date:  29-Jun-1999    DCS Number: 5337
//  Project: 840 Neonatal
//  Description:
//	Optimized VGA library for Neonatal project to use BitBlt and Windows
//  Bitmap structures.
//
//  Revision: 005   By: sah    Date: 11-Jan-1999   DR Number: 5321
//  Project:  Sigma (R8027)
//  Description:
//      Changed 'SoftFault()' method to non-inline method.
//
//  Revision: 004  By: gdc      Date: 16-Jun-1998    DCS Number: 5055
//  Project:  Color
//  Description:
//      Initial release.
//
//  Revision: 003  By: sah      Date: 12-Jun-1997    DCS Number: 2208
//    Project:  Sigma (R8027)
//    Description:
//      Incorporated new VGA interface methods, files and types.  Also
//      changed an 'int' declarator to 'Uint'.
//
//  Revision: 002  By: mpm      Date: 14-Aug-1995    DCS Number: 521
//    Project:  Sigma (R8027)
//    Description:
//      Setup of VGA colors changed in order to solve DCS #521, Noise on GUI Screens.
//
//  Revision: 001  By: yyy      Date: 01-Jan-1995    DCS Number: NONE
//    Project:  Sigma (R8027)
//    Description:
//	Integration baseline.
//=====================================================================

#include "Colors.hh"
#include "OsUtil.hh"    // for IsUpperDisplayColor()
#include "VgaDisplayContext.hh"
#include "VgaBlink.hh"
#include "RgbQuad.hh"
#include "BaseContainer.hh"

//
// Initializations of static data members.
//

struct ColorMapInfo
{
  Colors::ColorS  colorIndex;
  RgbQuad		  rgb;
};

static const ColorMapInfo StandardColorMap_[] =
{
  { Colors::WHITE,            {255, 255, 255} },	
  { Colors::LIGHT_BLUE,       {240, 160, 160} },
  { Colors::MEDIUM_BLUE,      {200,   0,   0} },
  { Colors::DARK_BLUE,        {128,  32,  32} },
  { Colors::EXTRA_DARK_BLUE,  { 64,  16,  16} },
  { Colors::BLACK,            {  0,   0,   0} },
  { Colors::YELLOW,           {  0, 255, 255} },
  { Colors::RED,              {  0,   0, 255} },
  { Colors::GREEN,            {  0, 255,   0} },
  { Colors::DARK_RED,         {  0,   0, 112} },
  { Colors::MIDNIGHT_BLUE,    { 32,   0,   0} },
  { Colors::GOLDENROD,        { 60, 128, 255} },
  { Colors::COPPER,           { 16,  64, 192} }
};

static const ColorMapInfo GrayscaleColorMap_[] =
{
  { Colors::WHITE, {255, 255, 255} },	
  { Colors::LIGHT_BLUE, {144, 144, 144} },
  { Colors::MEDIUM_BLUE, {32, 32, 32} },
  { Colors::DARK_BLUE, {80, 80, 80} },
  { Colors::EXTRA_DARK_BLUE, {0, 0, 0} },
  { Colors::BLACK, {0, 0, 0} },
  { Colors::YELLOW, {255, 255, 255} },
  { Colors::RED, {255, 255, 255} },
  { Colors::GREEN, {255, 255, 255} },
  { Colors::DARK_RED, {48, 48, 48} },
  { Colors::MIDNIGHT_BLUE, {16, 16, 16} },
  { Colors::GOLDENROD, {255, 255, 255} },
  { Colors::COPPER, {64, 64,  64} }
};


struct BlinkColorMapInfo
{
  Colors::ColorS  colorIndex;
  VgaBlink::BlinkStatus blinkState;
  RgbQuad		  rgbOn;
  RgbQuad		  rgbOff;
};

static const BlinkColorMapInfo StandardBlinkColorMap_[] =
{
  { Colors::BLACK_ON_LIGHT_GREY_BLINK, 
	VgaBlink::BLINK_SLOW, {0, 0, 0}, {144, 144, 144} },
  { Colors::WHITE_ON_EXTRA_DARK_GREY_BLINK, 
	VgaBlink::BLINK_SLOW, {255, 255, 255}, {32, 32, 32} },
  { Colors::WHITE_ON_BLACK_BLINK_FAST, 
	VgaBlink::BLINK_FAST, {255, 255, 255}, {0, 0, 0} },
  { Colors::BLACK_ON_YELLOW_BLINK_SLOW, 
	VgaBlink::BLINK_SLOW, {0, 0, 0}, {  0, 255, 255} },
  { Colors::BLACK_ON_LIGHT_BLUE_BLINK, 
	VgaBlink::BLINK_SLOW, {0, 0, 0}, {240, 160, 160} },
  { Colors::YELLOW_ON_MEDIUM_BLUE_BLINK_SLOW, 
	VgaBlink::BLINK_SLOW, {0, 255, 255}, {200, 0, 0} }
};

static const BlinkColorMapInfo GrayscaleBlinkColorMap_[] =
{
  { Colors::BLACK_ON_LIGHT_GREY_BLINK, 
	VgaBlink::BLINK_SLOW, {0, 0, 0}, {144, 144, 144} },
  { Colors::WHITE_ON_EXTRA_DARK_GREY_BLINK, 
	VgaBlink::BLINK_SLOW, {255, 255, 255}, {8, 8, 8} },
  { Colors::WHITE_ON_BLACK_BLINK_FAST, 
	VgaBlink::BLINK_FAST, {255, 255, 255}, {0, 0, 0} },
  { Colors::BLACK_ON_YELLOW_BLINK_SLOW, 
	VgaBlink::BLINK_SLOW, {255, 255, 255}, {0, 0, 0} },
  { Colors::BLACK_ON_LIGHT_BLUE_BLINK, 
	VgaBlink::BLINK_SLOW, {0, 0, 0}, {144, 144, 144} },
  { Colors::YELLOW_ON_MEDIUM_BLUE_BLINK_SLOW, 
	VgaBlink::BLINK_SLOW, {0, 0, 0}, {32, 32, 32} }
};

//=====================================================================
//
// Public methods
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Initialize [static]
//
//@ Interface-Description
// This method initializes the color palettes of the two VGA chip sets. The
// palettes of the upper and lower VGAs are initialized separately.  Also,
// setup for each of the special "flashing" colors is done. This involves
// specifying two colors and a flash rate; a special background task will
// continuously remap each flashing color back and forth between these 2
// colors at the specified rate.
//---------------------------------------------------------------------
//@ Implementation-Description
// Calls are made to low-level routines in the VGA-Graphics-Driver subsystem
// to set up the color map.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
Colors::Initialize(void)
{
	CALL_TRACE("Colors::Initialize(void)");

	// Program the R, G and B values for each color on the screen.
	// This must be done seperately for the upper and lower VGA screens.

	InitializeMap_(::UPPER_VGA_DISPLAY);

  	InitializeMap_(::LOWER_VGA_DISPLAY);

}




//=====================================================================
//
// Private methods
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: InitializeMap_ [static]
//
//@ Interface-Description
// This method initializes the color palettes of the two VGA chip sets. The
// palettes of the upper and lower VGAs are initialized separately.  Also,
// setup for each of the special "flashing" colors is done. This involves
// specifying two colors and a flash rate; a special background task will
// continuously remap each flashing color back and forth between these 2
// colors at the specified rate.
//---------------------------------------------------------------------
//@ Implementation-Description
// Calls are made to low-level routines in the VGA-Graphics-Driver subsystem
// to set up the color map.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
Colors::InitializeMap_( VgaDisplayId displayIndex )
{

  DisplayContext* dc = NULL;
  if (displayIndex == UPPER_VGA_DISPLAY) 
  {
	  dc = &(GuiFoundation::GetUpperBaseContainer()).getDisplayContext();
  }
  else
  {
	  dc = &(GuiFoundation::GetLowerBaseContainer()).getDisplayContext();
  }


  Uint i =0; 
  if ( IsUpperDisplayColor() )
  {	//													$[TI1.1]
	// initialize the standard color map		$[TI1.1]
	const ColorMapInfo * pColorInfo = StandardColorMap_;
	for (i=0; i<countof(StandardColorMap_); i++)
	{
	  dc->storeColor(pColorInfo->colorIndex, pColorInfo->rgb); 
	  ++pColorInfo;
	}
	
	// initialize the standard blink colors
	const BlinkColorMapInfo * pBlinkInfo = StandardBlinkColorMap_;
	for (i=0; i<countof(StandardBlinkColorMap_); i++)
	{
	  // reserve a color map entry
	  dc->storeColor(pBlinkInfo->colorIndex, pBlinkInfo->rgbOn, FALSE); 
	  VgaBlink::SetBlinkColor(pBlinkInfo->colorIndex, 
							  pBlinkInfo->blinkState,
							  pBlinkInfo->rgbOn,
							  pBlinkInfo->rgbOff);
	  ++pBlinkInfo;
	}
  }
  else
  {   //                                                  $[TI1.2]
	// initialize the grayscale color map
	const ColorMapInfo * pColorInfo = GrayscaleColorMap_;
	for (i=0; i<countof(GrayscaleColorMap_); i++)
	{
		
	  dc->storeColor(pColorInfo->colorIndex, pColorInfo->rgb);  
	  ++pColorInfo;
	}

	// initialize the grayscale blink colors
	const BlinkColorMapInfo * pBlinkInfo = GrayscaleBlinkColorMap_;
	for (i=0; i<countof(GrayscaleBlinkColorMap_); i++)
	{
	  // reserve a color map entry
	  dc->storeColor(pBlinkInfo->colorIndex, pBlinkInfo->rgbOn, FALSE);  
	  VgaBlink::SetBlinkColor(pBlinkInfo->colorIndex, 
							  pBlinkInfo->blinkState,
							  pBlinkInfo->rgbOn,
							  pBlinkInfo->rgbOff);
	  ++pBlinkInfo;
	}
  }
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SoftFault [public, static]
//
// Interface-Description
// This method forwards software exceptions to the fault-handling utility.
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
// none
//---------------------------------------------------------------------
// PostCondition
// none
// End-Method
//=====================================================================

void
Colors::SoftFault(const SoftFaultID  softFaultID,
				  const Uint32       lineNumber,
				  const char*        pFileName,
				  const char*        pPredicate)  
{
	CALL_TRACE("Colors::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_FOUNDATIONS, COLORS, 
							lineNumber, pFileName, pPredicate);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: toStandardColor [static]
//
//@ Interface-Description
// Returns the RgbQuad equivalent of the color.
//---------------------------------------------------------------------
//@ Implementation-Description
// Returns the RgbQuad equivalent of the color.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
RgbQuad Colors::toStandardColor(ColorS color)
{
	RgbQuad rgbv;
	Boolean colorfound = FALSE;

	const ColorMapInfo* pColorInfo = StandardColorMap_;
	for (Uint16 i=0; i<countof(StandardColorMap_) && (colorfound == FALSE) ; i++)
	{
	  if(pColorInfo->colorIndex == color)
	  {
		  rgbv = pColorInfo->rgb;
		  colorfound = TRUE;
	  }
	  ++pColorInfo;
	}

	CLASS_ASSERTION(colorfound);		// color not supported.

	return rgbv;
}
