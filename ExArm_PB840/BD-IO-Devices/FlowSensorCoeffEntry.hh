
#ifndef FlowSensorCoeffEntry_HH
#define FlowSensorCoeffEntry_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
//  Class: FlowSensorCoeffEntry - Defines the information structure for
//     the flow calibration entry as stored in the FlowSensorCalInfo object.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/FlowSensorCoeffEntry.hhv   26.0.1.0   04 Feb 2013 10:36:40   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005  By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 004 By: syw    Date: 20-Feb-1996   DR Number: DCS 674
//  	Project:  Sigma (R8027)
//		Description:
//			Removed #ifndef around #include.
//
//  Revision: 003  By: syw    Date:  25-Jan-1996    DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Added also SIGMA_DEBUG to the write() method.
//
//  Revision: 002  By: syw    Date:  05-Dec-1995    DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Added setFlowSensorCoeffEntry() method for service mode usage.
//
//	Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: INITIAL RELEASE
//      Project:  Sigma (R8027)
//      Description:
//      	Initial version
//
//=====================================================================

#include "Sigma.hh"

//@ Usage-Classes

//@ End-Usage

class FlowSensorCoeffEntry {

    //@ Friend: FlowSensorCalInfo
    // This class is only use by FlowSensorCalInfo
    friend class FlowSensorCalInfo;

  public:
    FlowSensorCoeffEntry( void) ;
    ~FlowSensorCoeffEntry( void) ;
    void   operator=( const FlowSensorCoeffEntry& coeffEnntry) ;
    
    static void SoftFault( const SoftFaultID softFaultID,
						   const Uint32      lineNumber,
						   const char*       pFileName  = NULL, 
						   const char*       pPredicate = NULL) ;

	void setFlowSensorCoeffEntry( const Real32 volt, const Real32 a, const Real32 b, const Real32 c) ;
	
#ifdef SIGMA_DEBUG
    void write( void) ;
#endif // SIGMA_DEBUG
  
  protected:

  private:
    // these methods are purposely declared, but not implemented...
    FlowSensorCoeffEntry(const FlowSensorCoeffEntry&) ;    // not implemented...

    //@ Data-Member: calVoltage_
    // the voltage at which these voltage to flow conversion coefficients
    // were defined
    Real32 calVoltage_ ;

    //@ Data-Member: coeffA_
    // first coefficient of the voltage to flow conversion
    Real32 coeffA_ ;

    //@ Data-Member: coeffB_
    // second coefficient of the voltage to flow conversion
    Real32 coeffB_ ;

    //@ Data-Member: coeffC_
    // third coefficient of the voltage to flow conversion
    Real32 coeffC_ ;
} ;


#endif // FlowSensorCoeffEntry_HH 
