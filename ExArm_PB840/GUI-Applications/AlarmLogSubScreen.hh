#ifndef AlarmLogSubScreen_HH
#define AlarmLogSubScreen_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1994, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: AlarmLogSubScreen - Displays a scrollable history of alarm events.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/AlarmLogSubScreen.hhv   25.0.4.0   19 Nov 2013 14:07:32   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005  By:  rhj	   Date:  25-Sept-2006    SCR Number: 6169
//  Project:  RESPM
//  Description:
//		Implement the ability to download alarm logs.
//
//  Revision: 004  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 003  By:  yyy    Date:  04-AUG-1997    DCS Number: 1803
//  Project:  Sigma (R8027)
//  Description:
//      Registered for DATE/TIME settings to capture the setting change
//		events and update the date/time accordingly.
//
//  Revision:  002  By: yyy   Date:  22-May-1997      DR Number: DCS 1917
//    Project:    Sigma (R8027)
//    Description:
//      Added data members: numLogEntries_, lastTimeStamp_, and access
//		method getAlarmLogInfo().
//
//  Revision: 001  By:  wks    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "SubScreen.hh"

//@ Usage-Classes
#include "Array_AlarmLogEntry_MAX_ALARM_ENTRIES.hh"
#include "LogTarget.hh"
#include "NovRamEventTarget.hh"
#include "GuiAppClassIds.hh"
#include "NovRamUpdateManager.hh"
#include "LaptopEventId.hh"
#include "LaptopMessageHandler.hh"
#include "LaptopEventRegistrar.hh"
#include "AlarmStrs.hh"
#include "LaptopEventTarget.hh"

class ScrollableLog;
//@ End-Usage

class AlarmLogSubScreen : public SubScreen, 
                          public NovRamEventTarget,
                          public LogTarget,
                          public LaptopEventTarget 
{
public:
	AlarmLogSubScreen(SubScreenArea* pSubScreenArea);
	~AlarmLogSubScreen(void);

	// Inherited from SubScreen
	virtual void activate(void);
	virtual void deactivate(void);

	void updateAlarmLogDisplay(void);
	inline void getAlarmLogInfo(Int32 &rNumLogEntries, TimeStamp &rLastTimeStamp);

	// Inherited from LogTarget
	virtual void getLogEntryColumn(Uint16 entryNumber, Uint16 columnNumber,
						Boolean &rIsCheapText, StringId &rString1,
						StringId &rString2, StringId &rString3,
						StringId &rString1Message);

	// Inherited from NovRamEventTarget
	virtual void novRamUpdateEventHappened(NovRamUpdateManager::UpdateTypeId
																	updateId);

    // LaptopEventTarget virtual methods
    virtual void laptopRequestHappened(LaptopEventId::LTEventId eventId,
                                       Uint8 *pMsgData);

	static void SoftFault(const SoftFaultID softFaultID,
								 const Uint32      lineNumber,
								 const char*       pFileName  = NULL, 
								 const char*       pPredicate = NULL);

protected:

private:
	enum AlarmLogColumnId
	{
		TIME_COLUMN,
		EVENT_COLUMN,
		URGENCY_COLUMN,
		ALARM_COLUMN,
		ANALYSIS_COLUMN
	};

	// these methods are purposely declared, but not implemented...
	AlarmLogSubScreen(void);						// not implemented...
	AlarmLogSubScreen(const AlarmLogSubScreen&);	// not implemented...
	void operator=(const AlarmLogSubScreen&);		// not implemented...

	//@ Data-Member: pLog_
	// Scrollable log which handles display of the alarm log.
	ScrollableLog *pLog_;

	//@ Data-Member: latestLogEntries_
	// A fixed array which is a copy of the latest alarm log entries stored
	// in NVRAM.
	FixedArray(AlarmLogEntry,MAX_ALARM_ENTRIES) latestLogEntries_;

	//@ Data-Member: numLogEntries_
	// Keeps track of the number of log entries in the alarm log.  This
	// is used to do a quick test to see if the alarm log has changed
	// while the alarm log is not displayed (i.e., the alarm log button
	// is in the up state).  When the alarm log reaches its capacity,
	// this counter is no longer useful since the alarm log contents
	// could change while the number of entries stays constant.
	Int32 numLogEntries_;

	//@ Data-Member: lastTimeStamp_
	// A copy of the time stamp for the "newest" alarm log entry.  This
	// is used for a finer-grained test to see if the alarm log has
	// changed while the alarm log is not displayed (i.e., the alarm log
	// button is in the up state).  
	TimeStamp lastTimeStamp_;

     //@ Data-Member: stringPos_
    // Keeps track of the current string position of destStr array.
    Uint32 stringPos_;
	 
    // Appends a character to the destStr pointer.
    void updateAsciiString_(wchar_t * destStr, Int32 stringLen, wchar_t character);

    // Converts Alarm messages to ASCII Text
    void convertAlarmMsgToAscii_(wchar_t* destStr, Int32 stringLen, const wchar_t * srcStr);
	 

};

// Inlined methods
#include "AlarmLogSubScreen.in"

#endif // AlarmLogSubScreen_HH 
