#ifndef MERCURY_HH
#define MERCURY_HH
//====================================================================
// This is a proprietary work to which the Covidien corporation claims 
// exclusive right.  No part of this work may be used, disclosed, 
// reproduced, sorted in an information retrieval system, or 
// transmitted by any means, electronic, mechanical, photocopying, 
// recording, or otherwise without the prior written permission of 
// Covidien Corporation.
//
//              Copyright (c) 2008, Covidien Corporation
//=====================================================================

//=====================================================================
//@ Filename: Mercury - Mercury Module commands/status header file
//---------------------------------------------------------------------
//@ Version-Information
//
//@ Modification-Log
//
//  Revision: 002  By:  gdc    Date:  21-Jan-2011    SCR Number: 6733
//  Project:  PROX
//  Description:
//      Added VCO2 study code for development reference.
//
//  Revision: 001  By:  gdc    Date:  25-APR-2008    SCR Number: 6436
//  Project:  PROX
//  Description:
//      Initial version.
//=====================================================================

class Mercury
{
public:

	enum PacketId
	{
		START_WAVEFORM_MODE                 = 0x90,
		WAVEFORM_AND_PARAMETER_DATA         = 0x91,
		ADD_REMOVE_WAVEFORM_DATA_PARAMETER  = 0x92,		// Enable/disable data sent by ContinuousMode command
		DPI_DATA_PARAMETER                  = 0x93,		// Retrieve single DPI 

		CAPNOSTAT5_ROOM_AIR_ZERO            = 0xA0,
		SET_SPO2_AND_PULSE_RATE             = 0xBF,

		NACK_ERROR                          = 0xC8,		// Command error
		STOP_CONTINUOUS_MODE                = 0xC9,		// Stop the transmission of continuous waveform and data
		GET_SOFTWARE_REVISION               = 0xCA,		// Current software revision
		RESET_NO_RESPIRATION_FLAG           = 0xCC,
		START_MANUAL_FLOW_ZERO_AND_PURGE    = 0xCD,
		CUSTOM_PURGE                        = 0xCE,

		GET_SET_AIRWAY_GAS_CONDITIONS       = 0xD1,		// Get / set current gas conditions in the patient circuit
		GET_SET_INSTRUMENT_SETTINGS         = 0xD4,		// Get / set various instrument settings 
		GET_FLOW_SENSOR_TYPE                = 0xD8,		// Identify the type of flow sensor
		MARK_BLOOD_GAS_EVENT                = 0xD9,
		GET_MARKED_BLOOD_GAS_PARAMETERS     = 0xDA,
		SET_LAST_BREATH_TYPE                = 0xE1,

		RESET_MERCURY_MODULE                = 0xF8,
		BOOTCODE_COMMAND_SYNC1  			= 0xFE,
		BOOTCODE_COMMAND_SYNC2				= 0x55,

        PURGE_COMMAND_OPTIONS               = 0xCE
	};

	enum BootCodePacketId
	{
		BOOTCODE_NACK_ERROR					= 0x40,
		IN_BOOTCODE							= 0x41,
		EXIT_BOOTCODE						= 0x42,
		DOWNLOAD_MEMORY_BLOCK				= 0x43,
		GET_FLASH_TYPE						= 0x44,
		GET_BOOTCODE_REVISION				= 0x45,
		READ_MEMORY_BLOCK					= 0x46,
		GET_INSTRUMENT_TYPE					= 0x47,
		SERIAL_BOOT_CONNECT					= 0x48,
		SET_MEMORY_BLOCK					= 0x49,
		SET_DOWNLOAD_SPEED					= 0x4A,
		ENUMERATE_MODULES					= 0x4B,
		SET_ACTIVE_MODULE					= 0x4C,
		ALLOW_BOOTCODE_PROGRAMMING			= 0x50,
		GET_BOOTCODE_CAPABILITIES			= 0x51
	};

	enum CommandAction
	{
		COMMAND_RECEIVED				= 1,
		PROGRAM_SUCCESSFUL				= 2,
		PROGRAM_FAILURE					= 3,
		ADDRESS_FAILURE					= 4,
		SECTOR_PROGRAMMING_FAULT		= 5,
		BOOTCODE_PROGRAMMING_PROTECTION = 6,
		ERASE_ERROR						= 7,
		ERASE_SUCCESSFUL				= 8,
		ERASE_IN_PROGRESS				= 9,
		ERASE_ERROR_WAIT				= 10
	};

	enum DataRateId
	{
		W200D200                    	= 0x03,		// 200 Waveform pts/sec and 200 Data parameters per second
		W100D200                    	= 0x12,		// 100 Waveform pts/sec and 200 Data parameters per second
		W100D100                    	= 0x02,		// 100 Waveform pts/sec and 100 Data parameters per second
		W50D200                     	= 0x21,		// 50 Waveform pts/sec and 200 Data parameters per second
		W50D100                     	= 0x11,		// 50 Waveform pts/sec and 100 Data parameters per second
		W50D50                      	= 0x01		// 50 Waveform pts/sec and 50 Data parameters per second
	};

	enum WaveformModeId
	{
		WMB1                            = 0x00,		// waveform mode byte 1 always 0
		WMB1_ACC                        = 0x10,      //used with Accum Pressure
		FLOW_WAVEFORM                   = 0x01,
		PRESSURE_WAVEFORM               = 0x02,
		VOLUME_WAVEFORM                 = 0x04,
		CO2_WAVEFORM                    = 0x08,
		O2_PERCENT_WAVEFORM             = 0x10,
		O2_MMHG_WAVEFORM                = 0x20,
		ACCUM_PRESSURE_WAVEFORM         = 0x10
	};

	enum RevisionFormat
	{
		MAIN_SOFTWARE_REVISION			= 0,
		BOOT_CODE_REVISION				= 2,
		CAPNOSTAT5_MAIN_REVISION		= 0x10,
		CAPNOSTAT5_BOOT_CODE_REVISION	= 0x11
	};

	enum DataParameterId
	{
		NO_PACKET_DATA_AVAIL            = 0x0000,

		FLOW_STATUS                     = 0x0101,
		BAROMETRIC_PRESSURE             = 0x0102,
		START_OF_INSPIRATION_MARK       = 0x0103,
		START_OF_EXPIRATION_MARK        = 0x0104,
		ABSOLUTE_MECHANICAL_THRESHOLD   = 0x0105,
		BREATH_TYPE                     = 0x0106,
		PEAK_INSPIRATORY_FLOW           = 0x010A,
		PEAK_EXPIRATORY_FLOW            = 0x010B,
		INSPIRATORY_VOLUME              = 0x010C,
		EXPIRATORY_VOLUME               = 0x010D,
		INSPIRED_TIDAL_VOLUME_SPONT_AVG = 0x010E,
		INSPIRED_TIDAL_VOLUME_MECH_AVG  = 0x010F,
		INSPIRED_TIDAL_VOLUME_TOTAL_AVG = 0x0110,
		EXPIRED_TIDAL_VOLUME_SPONT_AVG  = 0x0111,
		EXPIRED_TIDAL_VOLUME_MECH_AVG   = 0x0112,
		EXPIRED_TIDAL_VOLUME_TOTAL_AVG  = 0x0113,
		INSPIRED_MINUTE_VOLUME_SPONT_AVG= 0x0114,
		INSPIRED_MINUTE_VOLUME_MECH_AVG = 0x0115,
		INSPIRED_MINUTE_VOLUME_TOTAL_AVG= 0x0116,
		EXPIRED_MINUTE_VOLUME_SPONT_AVG = 0x0117,
		EXPIRED_MINUTE_VOLUME_MECH_AVG  = 0x0118,
		EXPIRED_MINUTE_VOLUME_TOTAL_AVG = 0x0119,
		PEAK_INSPIRATORY_PRESSURE       = 0x0120,
		MEAN_AIRWAY_PRESSURE            = 0x0121,
		P100_PRESSURE                   = 0x0122,
		POSITIVE_END_EXPIRATORY_PRESS   = 0x0123,
		PLATEAU_PRESSURE                = 0x0124,
		NEGATIVE_INSPIRATORY_PRESSURE   = 0x0125,
		RESPIRATORY_RATE_SPONT          = 0x0128,
		RESPIRATORY_RATE_MECH           = 0x0129,
		RESPIRATORY_RATE_TOTAL          = 0x012A,
		RAPID_SHALLOW_BREATHING_INDEX   = 0x012B,
		I_TO_E_VOLUME_RATIO             = 0x012C,
		INSPIRED_TIME                   = 0x012D,
		EXPIRED_TIME                    = 0x012E,
		TOTAL_BREATH_TIME               = 0x012F,
		INSPIRED_AIRWAY_RESISTANCE      = 0x0134,
		EXPIRED_AIRWAY_RESISTANCE       = 0x0135,
		TOTAL_LUNG_COMPLIANCE_DYNAMIC   = 0x0136,
		LUNG_COMPLIANCE_STATIC          = 0x0137,
		EXPIRED_TIDAL_VOLUME_PER_KG_MECH= 0x0138,
		CAPNOSTAT5_CO2_PRIORITY_STATUS  = 0x0148,
		END_TIDAL_CO2                   = 0x0149,
		INSPIRED_CO2                    = 0x014A,
		VOLUME_CO2_EXPIRED_PER_MINUTE   = 0x014C,
		ALVEOLAR_TIDAL_VOLUME_SPONT     = 0x014D,
		ALVEOLAR_TIDAL_VOLUME_MECH      = 0x014E,
		ALVEOLAR_TIDAL_VOLUME_TOTAL     = 0x014F,
		ALVEOLAR_MINUTE_VOLUME_SPONT    = 0x0150,
		ALVEOLAR_MINUTE_VOLUME_MECH     = 0x0151,
		ALVEOLAR_MINUTE_VOLUME_TOTAL    = 0x0152,
		AIRWAY_DEADSPACE                = 0x0155,
		MIXED_EXPIRED_CO2               = 0x0156,
		//ERM 
		ENHANCED_PURGE_STATUS           = 0x0401,
        CURRENT_RESERVOIR_PRESSURE      = 0x0402,
		PURGE_START_RESERVOIR           = 0x0403,
		PURGE_COMPLETE_RESERVOIR        = 0x0404,
		PURGE_START_DELAY_TIME          = 0x0405,
		PURGE_DISCHARGE_TIME            = 0x0406,
		PURGE_RESULTS                   = 0x0408,

		REPORTED_ZERO_INTERVAL          = 0x0721,

		DIFFERENTIAL_PRESS              = 0x0701,
        DIFFERENTIAL_PRESS_ZERO         = 0x0702,
		FLOW_COUNTS                     = 0x0703,
		AWP_ADC                         = 0x0708,
		AWP_ZERO_POINT                  = 0x0709,
		AWP_COUNTS                      = 0x070A,
		AWP_AVERAGE                     = 0x070B,
		VOLTAGE_3                       = 0x0714,
		PNEUMATIC_VOLTAGE               = 0x0715,
		VOLTAGE_18                      = 0x0716,
		VOLTAGE_12                      = 0x0717,
		CORE_VOLTAGE_12                 = 0x0718,
		VALVE_1_VOLTAGE                 = 0x0719,
		VALVE_2_VOLTAGE                 = 0x071A,
		VALVE_4_VOLTAGE                 = 0x071B,
		VALVE_3_VOLTAGE                 = 0x071C,
		AMBIENT_TEMP_DEG_C              = 0x0720,
		ZERO_DRIFTS_COUNTS              = 0x0722,
		HARDWARE_STATUS                 = 0x0724,
      
		END_OF_PARAMETERS_FOR_BREATH    = 0x077F
	};

	enum Isb
	{
		DEMO_MODE                       = 10,
		INSTALLED_SOFTWARE_OPTIONS		= 13,
		SET_GET_ETCO2_TIME_PERIOD		= 21,
		SET_GET_VCO2_AVERAGING_TIME		= 22,
		MODULE_SERIAL_NUMBER            = 61,
		MIN_PURGE_ISB                   = 114,
		MAX_INPUT_LINE_WAIT_TIME		= MIN_PURGE_ISB,
		OEM_ID							= 115,
		ZERO_PURGE_MODE					= 116,
		RESERVOIR_TARGET_PRESSURE		= 117,
		RESERVOIR_MAX_PRESSURE			= 118,
		CYCLE_REPETITION_COUNT			= 119,
		RELEASE_VALVE_OPEN_TIME			= 120,
		RELEASE_VALVE_CLOSE_TIME		= 121,
		PURGE_INTERVAL_TIME				= 122,
		FLOW_ZERO_VALVE_OPEN_TIME		= 123,
		UNUSED                          = 124,
		RESERVOIR_PRESSURE_TRANSDUCER_OFFSET = 125,
		MAX_CHARGE_TIME					= 126,
		MAX_PURGE_ISB                   = 127	
	};

	enum SensorType
	{
		FLOW_SENSOR_TYPE_MASK			= 0x07,
		UNPLUGGED_SENSOR				= 0x00,
		NEONATAL_FLOW_SENSOR			= 0x01,
		ADULT_FLOW_SENSOR				= 0x02,
		NEONATAL_FLOW_CO2_SENSOR		= 0x04,
		ADULT_FLOW_CO2_SENSOR			= 0x05,
		PEDIATRIC_FLOW_CO2_SENSOR		= 0x06,
		INVALID_SENSOR					= 0x07      //ERM DUPLICATE ENTRY
	};
	enum PMB
	{
		INIT_MANUAL_PURGE                = 0,
		INIT_DISCHARGE_RESERVOIR         = 1,
		INIT_SYNCED_ZERO                 = 2,
		SUSPEND_CUSTOM_PURGE             = 3,
		RESUME_CUSTOM_PURGE              = 4,
		INIT_RESERVOIR_PRESSURE_CAL      = 20
	};


	enum DEMO
	{
		DEMO_OFF                         = 0,  // default
		RESPIRATORY_MECH_ADULT_MODE      = 1,
		RESPIRATORY_MECH_NEO_MODE        = 2,
		CARDIAC_OUTPUT_ICU_MODE          = 20,
		CARDIAC_OUTPUT_OR_MODE           = 21,
		SQUARE_WAVE_TEST_MODE            = 64
	};


	enum ETCO2TimePeriod
	{
		ETCO2_ONE_BREATH_PERIOD			= 1,
		ETCO2_TEN_SECOND_PERIOD			= 10,
		ETCO2_TWENTY_SECOND_PERIOD		= 20
	};

	enum VCO2AveragingPeriod
	{
		VCO2_ONE_BREATH_AVERAGE			= 1,
		VCO2_EIGHT_BREATH_AVERAGE		= 2,
		VCO2_ONE_MINUTE_AVERAGE			= 3,
		VCO2_THREE_MINUTE_AVERAGE		= 4,
		VCO2_FIVE_MINUTE_AVERAGE		= 5,
		VCO2_TEN_MINUTE_AVERAGE			= 6
	};

	enum PrioritizedCO2Status
	{
		CO2_SENSOR_OK					= 0x00,
		CO2_SENSOR_OVER_TEMP			= 0x01,
		CO2_SENSOR_FAULTY				= 0x02,
		CO2_NO_MESSAGE					= 0x03,
		CO2_CAPNOSTAT_IN_SLEEP_MODE		= 0x04,
		CO2_ZERO_IN_PROGRESS			= 0x05,
		CO2_SENSOR_WARM_UP				= 0x06,
		CO2_CHECK_SAMPLING_LINE			= 0x0A,
		CO2_ZERO_REQUIRED				= 0x07,
		CO2_OUT_OF_RANGE				= 0x08,
		CO2_CHECK_AIRWAY_ADAPTER		= 0x09
	};

};

#endif // MERCURY_HH
