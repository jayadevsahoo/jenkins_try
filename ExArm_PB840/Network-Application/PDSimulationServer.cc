#include "stdafx.h"
//------------------------------------------------------------------------------
//                   Copyright (c) 2008 - 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
/// @file PDSimulationServer.cc
///
/// This file contains implementation of class PDSimulationServer. This file is
/// conditionaly complied for GUI INTEGRATION TEST purposes to overwrite
/// patient data on GUI
//------------------------------------------------------------------------------
#if defined( SIGMA_GUI_CPU )

#if defined( INTEGRATION_TEST_ENABLE )

#include "PDSimulationServer.hh"
#include "MsgQueue.hh"
#include "MsgTimer.hh"
#include "SafetyNet.hh"
#include "Task.hh"
#include "SoftwareOptions.hh"
#include "UtilityClassId.hh"

static PDSimulationServer g_PDSimulationServerInstance;

OsTimeStamp PDSimulationServer::m_LastMsgOsTimeStamp;



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  Boolean PDSimulationServer::OnRecvData()
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Boolean PDSimulationServer::OnRecvData(char *pBuffer, ULONG uiSize)
{

   NetworkMsgHeader *header=(NetworkMsgHeader *)pBuffer;
    NetworkApp::CallbackMsg* pBuf;

    if ( uiSize         <   sizeof(NetworkMsgHeader) )
    {
        return FALSE;
    }

    header->convNtoH();

    if ( header->pktSize != (uiSize - sizeof(NetworkMsgHeader)) )
    {
        return FALSE;
    }

    if ( header->pktSize >  MAX_NETWORK_BUF_SIZE )
    {
        return FALSE;
    }

    if (!NetworkApp::IsValidId(((XmitDataMsgId) header->msgId)))
    {
        return FALSE;
    }



    pBuf=NetworkApp::PCallbackTbl_[header->msgId - LOWEST_NETWORK_MSG_ID];

    NetworkApp::m_iIsCallbackTbl[header->msgId - LOWEST_NETWORK_MSG_ID] = 3;

    for(;pBuf;pBuf=pBuf->pNext)
    {
        (*pBuf->callback)((XmitDataMsgId)header->msgId,
                   (void*)(pBuffer
                       +sizeof(NetworkMsgHeader)
                       ), header->pktSize);

    }

    return TRUE;
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  void PDSimulationServer::WaitForDevKey()
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void PDSimulationServer::WaitForDevKey()
{
    while(TRUE)
    {
        Task::Delay(0,5000);

        if (SoftwareOptions::GetSoftwareOptions().isStandardDevelopmentFunctions())
        {
            m_iState = SETUP_SOCKET;
            break;

        }
    }
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  void PDSimulationServer::Halt()
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void PDSimulationServer::Halt()
{

	memset(NetworkApp::m_iIsCallbackTbl,0,sizeof(NetworkApp::m_iIsCallbackTbl));

    while(TRUE)
    {
        Task::Delay(0,5000);
    }

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  PDSimulationServer::SetupSocket()
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void PDSimulationServer::SetupSocket()
{

    UINT status;

    // Create a UDP socket.
    m_Socket = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if ( m_Socket == INVALID_SOCKET )
    {
        m_iState = HALT;
        return;
    }

    SOCKADDR_IN listen_addr;

	memset(&listen_addr, 0, sizeof(listen_addr));
	listen_addr.sin_family = AF_INET;
	listen_addr.sin_port = htons( SWAT_PD_SIMULATION_UDP_PORT );
	listen_addr.sin_addr.s_addr = INADDR_ANY;

	if(bind(m_Socket, (SOCKADDR*)&listen_addr,sizeof(SOCKADDR_IN)) < 0) 
	{
		RETAILMSG(true, (_T("ERROR binding in the server socket.\r\n")));
        m_iState = HALT;
        closesocket( m_Socket );
		return;
	}

    m_iState = WAIT_FOR_MESSAGE;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  PDSimulationServer::WaitForMessage()
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void PDSimulationServer::WaitForMessage()
{
    int uiBytesRead;
    OsTimeStamp clMsgOsTimeStamp;

    // initialize fd_set
    fd_set fds;
    FD_ZERO(&fds);
    FD_SET(m_Socket, &fds);

    // Wait until a message is received
    int selectResult = select(sizeof(fds)*8, &fds, NULL, NULL, NULL /*forever*/);

    // SOCKET_ERROR or TIMEOUT. Because we already set the TIMEOUT parameter
    // to 'forever', so 'select' will never timeout.
    if ( selectResult <= 0 )
    {
        RETAILMSG(true, 
            ( _T("WaitForMessage: ERROR in select, %d\r\n"), WSAGetLastError()) );

        m_iState = RESTART;
        return;
    }

    // selectResult > 0
    SOCKADDR_IN clientaddr;
	int len = sizeof(clientaddr); 
	
    //Read message out of buffer
    int recvResult = recvfrom(m_Socket, m_Buffer, sizeof(m_Buffer), 0, 
		(sockaddr*)&clientaddr, &len );

    if ( recvResult <= 0 )
    {
        // recvResult = SOCKET_ERROR, use WSAGetLastError to get error code
		RETAILMSG(true, 
            ( _T("WaitForMessage: ERROR in recvfrom, %d\r\n"), WSAGetLastError()) );
        
        m_iState = RESTART;
        return;
    }

    // recvResult > 0
    uiBytesRead = recvResult;	

    //Get now time.
    clMsgOsTimeStamp.now();


    //Process incomming data
    if(OnRecvData((char *)m_Buffer,uiBytesRead))
    {
        m_LastMsgOsTimeStamp=clMsgOsTimeStamp;
    }

    //Change state
    m_iState = WAIT_FOR_DATA;

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  PDSimulationServer::WaitForData()
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void PDSimulationServer::WaitForData()
{
    ULONG uiBytesRead;
    OsTimeStamp clMsgOsTimeStamp;
    Uint32      iDurration;

	bool		forever = true;	
	while( forever )
	{
		// initialize fd_set
		fd_set fds;
		FD_ZERO(&fds);
		FD_SET(m_Socket, &fds);

		// initialize time 200ms
		timeval t = { 0, 200 * 1000 };

		//Wait up to 2s or until a message is received
		int selectResult = select(sizeof(fds)*8, &fds, NULL, NULL, &t);

		if ( selectResult == 0 )		// timed out
		{
			m_iState = RESET;
			return;
		}
		else if ( selectResult < 0 )	// SOCKET_ERROR happened
		{
			RETAILMSG(true, 
				( _T("WaitForMessage: ERROR in select, %d\r\n"), WSAGetLastError()) );

			m_iState = RESTART;
			return;
		}

		// selectResult > 0
		SOCKADDR_IN clientaddr;
		int len = sizeof(clientaddr); 

		//Read message out of buffer
		int recvResult = recvfrom(m_Socket, m_Buffer, sizeof(m_Buffer), 0, 
			(sockaddr*)&clientaddr, &len );

		if ( recvResult <= 0 )
		{
			// recvResult = SOCKET_ERROR, use WSAGetLastError to get error code
			RETAILMSG(true, 
				( _T("WaitForData: ERROR in recvfrom, %d\r\n"), WSAGetLastError()) );

			m_iState = RESTART;
			return;
		}	

        uiBytesRead = recvResult;

		//Get now time.
		clMsgOsTimeStamp.now();

		//Calculate durration since last packet.
		iDurration = m_LastMsgOsTimeStamp.diffTime(clMsgOsTimeStamp);

		//If 2s has expired
		if(iDurration > 2000)
		{
			m_iState = RESET;
			return;
		}

		//Process incomming data
		if (OnRecvData(m_Buffer,uiBytesRead) )
		{
			//Set m_LastMsgOsTimeStamp to now
			m_LastMsgOsTimeStamp=clMsgOsTimeStamp;
		}
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  PDSimulationServer::Restart()
//
//@ Interface-Description
//      This method recreates the listening socket.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void PDSimulationServer::Restart()
{
    int status = 0;

    // since Winsock doesn't support socket unbind, here we close it.
    status = closesocket( m_Socket );

    // make sure no error occurred
    if ( status == SOCKET_ERROR )
    {
        m_iState = HALT;
        return;
    }

    Reset();

    m_iState = SETUP_SOCKET;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  PDSimulationServer::Reset()
//
//@ Interface-Description
//      This method resets the IsCallback table.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void PDSimulationServer::Reset()
{
    //Reset all flags in system to accept data.

	memset(NetworkApp::m_iIsCallbackTbl,0,sizeof(NetworkApp::m_iIsCallbackTbl));

    m_iState = WAIT_FOR_MESSAGE;

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  PDSimulationServer::ServerLoop()
//
//@ Interface-Description
//      This method is the loop in which the PDSimulationServer thread
//      executes.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void PDSimulationServer::ServerLoop()
{

    m_iState = WAIT_FOR_DEVKEY;

    while(TRUE)
    {
        switch(m_iState)
        {
            case WAIT_FOR_DEVKEY:
                WaitForDevKey();
                break;

            case WAIT_FOR_MESSAGE:
                WaitForMessage();
                break;

			case WAIT_FOR_DATA:
				WaitForData();
				break;

            case SETUP_SOCKET:
                SetupSocket();
                break;

            case RESET:
                 Reset();
                 break;

            case RESTART:
                 Restart();
                 break;

            case HALT:
                 Halt();
                 break;
        }
    }

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  PDSimulationServer::PDSimulationServerTask(Uint32 iJunk)
//                [static]
//
//@ Interface-Description
//      This method is the entry function to the PDSimulationServer task.
//---------------------------------------------------------------------
//@ Implementation-Description
//      Call ServerLoop().
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void PDSimulationServer::PDSimulationServerTask( void )
{
    g_PDSimulationServerInstance.ServerLoop();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
PDSimulationServer::SoftFault(const SoftFaultID  softFaultID,
					  const Uint32       lineNumber,
					  const char*        pFileName,
					  const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, NETWORK_APPLICATION, PDSIMULATIONSERVER_CLASS,
                          lineNumber, pFileName, pPredicate);
}

#endif	//INTEGRATION_TEST_ENABLE

#endif	//SIGMA_GUI_CPU
