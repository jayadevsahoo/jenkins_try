#ifndef RgbQuad_HH
#define RgbQuad_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2008, Covidien, Inc.
//====================================================================

//====================================================================
// Class: RgbQuad -  struct containing RGB video values based on 
//                   Windows standard Device Independent Bitmaps (DIB)
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/VGA-Graphics-Driver/vcssrc/RgbQuad.hhv   25.0.4.0   19 Nov 2013 14:37:32   pvcs  $
//
//@ Modification-Log
//  
//  Revision: 002   By: gdc    Date:  07-Mar-2008    SCR Number: 6430
//  Project: TREND2
//  Description:
//	Changed RGB byte ordering to BGR for compatibility with Windows
//	BMP files.
//  
//  Revision: 001   By: gdc    Date:  27-Jan-2000    DCS Number: 5337
//  Project: 840 Neonatal
//  Description:
//	Optimized VGA library for Neonatal project to use BitBlt and Windows
//  Bitmap structures.
//
//====================================================================

#include "Sigma.hh"

//@ Usage-Classes
//@ End-Usage

struct RgbQuad
{

  Byte    rgbBlue;
  Byte    rgbGreen;
  Byte    rgbRed;
  Byte    rgbReserved;

};
 
#endif // RgbQuad_HH 
