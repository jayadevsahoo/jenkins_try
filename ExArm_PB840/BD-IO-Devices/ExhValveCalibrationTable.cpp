#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: ExhValveCalibrationTable_t - Implements exhalation valve calibration.
//---------------------------------------------------------------------
//@ Interface-Description
//      
//---------------------------------------------------------------------
//@ Rationale
//      
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ Fault-Handling
//		n/a
//---------------------------------------------------------------------
//@ Restrictions
//      None
//---------------------------------------------------------------------
//@ Invariants
//      None
//---------------------------------------------------------------------
//@ End-Preamble
//
//*********************************************************************
#include "ExhValveCalibrationTable.h"
#include "GlobalObjects.h"
#include "ServosGlobal.h"
#include "UtilityFunctions.h"
#include "SmRefs.hh"
#include "SmFlowController.hh"

static const Uint32 NUM_TEST_PRESSURE = 5;
static const Real32 TEST_PRESSURE[NUM_TEST_PRESSURE] = {5, 25, 50, 75, 100};

//*****************************************************************************
ExhValveCalibrationTable_t::ExhValveCalibrationTable_t(CalibrationID_t CalID) : CalibrationTable_t(CalID)
{
}

//*****************************************************************************
CalibrationStatus_t ExhValveCalibrationTable_t::Calibrate(const ConvertedA2dReadings_t *pConvertedData)
{
	if(CalibrationStatus != CAL_PENDING)
	{
		CalibrationStatus = CAL_PENDING;
	//	CalState = PC_INIT;
		//CalState = PC_INIT_L2H;
		CalState = PC_START_FLOW;
		CalibrationDone = false;
		PdrvSum = 0;
		PexhSum = 0;
		PexhAvg = 0;
		PdrvAvg = 0;
		CalId = 0;
	}

	DoCalibration(pConvertedData);

	return CalibrationStatus;
}


static const Uint8 NUM_AVG_SAMPLES							= 3;
static const Uint8 EXH_VALVE_CAL_DAC_INCREMENT				= 1;
static const Pressure_t EXH_CAL_STEP						= 1.0;
static const DWORD EXH_CAL_WAIT_COUNT						= 20;
static const DWORD EXH_FLOW_WAIT_COUNT						= 200;
static const DWORD EXH_MOVE_CHECK_WAIT_COUNT				= 150;
static const DWORD EXH_VALVE_CAL_INITIAL_DELAY				= 1000;
static const DWORD EXH_VALVE_CAL_ZERO_UP_L2H_DELAY			= 2000;
static const DWORD EXH_VALVE_CAL_ZERO_DOWN_L2H_START_DAC	= 1000;

static const uint16_t INITIAL_PDRIVE_CAL_DAC		= 0;
static const Flow_t EXH_VALVE_CAL_FLOW				= 6.0;
static const Flow_t EXH_VALVE_CAL_MIN_FLOW			= 5.0;

// lowest calibration pressure
const Uint32 MIN_CALIB_PRESSURE = 1;  // cm H2O
//@ Constant: NORMAL_PRESSURE_TIMEOUT
// timeout between successive integer preesure points
const Int32 NORMAL_PRESSURE_TIMEOUT = 1500;		// ms

//@ Constant: INITIAL_PRESSURE_TIMEOUT
// timeout to reach the initial 1 cmH2O pressure point
const Int32 INITIAL_PRESSURE_TIMEOUT = 71500;	// ms

//@ Constant: REL_PRESSURE_ERROR
// pressure transducer relative error
const Real32 REL_PRESSURE_ERROR = 0.031;

//@ Constant: ABS_PRESSURE_ERROR
// pressure transducer absolute error
const Real32 ABS_PRESSURE_ERROR = 0.35;

//*****************************************************************************
// Calibration from high pressure to low pressure
bool ExhValveCalibrationTable_t::DoCalibration(const ConvertedA2dReadings_t *pConvertedData)
{
	static int16_t CalIndex;
	static uint16_t CalDac;
	static Pressure_t CalPtgt;
	static Pressure_t Pexh[EXH_VALVE_SUBTABLE_SIZE];
	static Pressure_t Pdrv[EXH_VALVE_SUBTABLE_SIZE];
	static uint16_t DacCount[EXH_VALVE_SUBTABLE_SIZE];
	static Pressure_t PexhPrev;
	static PC_CAL_STATE NextCalState;

	WCHAR StrPdrive[10], StrPexh[10];
	static uint16_t DelayCount = 0;
	static DWORD PreviousTickCount;
	float *pCalData = pTable;

	static Pressure_t PexhDebug;
	static Int32 timer = 0;
	static DWORD preTimer = 0;
	Boolean timeoutOccurred = FALSE;
	FlowSensorTestResult fsTestResult;
	PressureSensorTestResult psTestResult;

	PdrvSum = PdrvSum + pConvertedData->FilteredExhDrivePressure - PdrvAvg;
	PdrvAvg = PdrvSum / NUM_AVG_SAMPLES;			// Average Pdrv
	PexhSum = PexhSum + pConvertedData->FilteredExpPressure - PexhAvg;
	PexhAvg = PexhSum / NUM_AVG_SAMPLES;			// Average P2

	
	switch(CalState)
	{
		//	************** start low to high calibration *************
	case PC_START_FLOW:
		RSmFlowController.establishFlow( EXH_VALVE_CAL_FLOW, DELIVERY, AIR);

		// TODO E600_LL: NOTE 2 - when called from Service Mode, change the way of updating psol throughout file. Use
		// RExhalationValve.updateValve(...) instead of
		CalDac = INITIAL_PDRIVE_CAL_DAC;
		ControlPSOL(DAC_EXHALATION_VALVE, CalDac);
		
		// wait some time for flow to be established
		CalState = PC_WAIT_L2H;
		NextCalState = PC_INIT_L2H;
		DelayCount = EXH_VALVE_CAL_INITIAL_DELAY;
		PreviousTickCount = GetTickCount();
		break;
	case PC_INIT_L2H:
		if( fabs(pConvertedData->FilteredAirFlow - EXH_VALVE_CAL_FLOW) < 1.0
			&& fabs(pConvertedData->FilteredExhFlow - EXH_VALVE_CAL_FLOW) < 2.0
			)
		{
			WCHAR StrAirFlow[10], StrExhFlow[10];
			wsprintf(StrAirFlow, L"%3.2f",pConvertedData->FilteredAirFlow);
			wsprintf(StrExhFlow, L"%3.2f",pConvertedData->FilteredExhFlow);
			RETAILMSG(1, (_T("PC_INIT_L2H: AirFlow=%s, ExhFlow=%s\r\n"), StrAirFlow, StrExhFlow));

			CalId = 0;
	    	CalPtgt = TEST_PRESSURE[CalId];
			CalIndex = 0;
			L2hCalCompleted = false;
			PexhPrev = PexhAvg;
			fsTestResult = checkFlowSensors_(pConvertedData);
			if (fsTestResult == FLOW_SENSORS_OK)
			{
				CalState = PC_WAIT_L2H;
				NextCalState = PC_ZERO_UP_L2H;
				RETAILMSG(1, (_T("Flow Sensor OK\r\n")));
			}
			else
			{
				CalState = PC_WAIT_L2H;
				NextCalState = PC_ABORT_L2H;
				RETAILMSG(1, (_T("Flow Sensor failed\r\n")));
			}
			DelayCount = EXH_FLOW_WAIT_COUNT;
			PreviousTickCount = GetTickCount();
			PexhDebug = 0;
		}
		break;

	case PC_ZERO_UP_L2H:
        //pressure sensor cross check

		
		if(PexhAvg >= CalPtgt)
		{
			CalPtgt = TEST_PRESSURE[++CalId];
			DelayCount = EXH_VALVE_CAL_ZERO_UP_L2H_DELAY;
			PreviousTickCount = GetTickCount();
			PexhDebug = 21;

			CalDac += EXH_VALVE_CAL_DAC_INCREMENT;
			if (CalId >= NUM_TEST_PRESSURE)
			{
				CalState = PC_WAIT_L2H;
				NextCalState = PC_ZERO_DOWN_L2H;	
				CalPtgt = 0;
				CalDac = EXH_VALVE_CAL_ZERO_DOWN_L2H_START_DAC;
			}
			wsprintf(StrPdrive, L"%3.2f",PdrvAvg);
			wsprintf(StrPexh, L"%3.2f",PexhAvg);
			RETAILMSG(1, (_T("PC_ZERO_UP: Pdrv=%s, Pexh=%s, Dac=%d\r\n"), StrPdrive, StrPexh, CalDac));

			//Pressure sensor cross check at each pressure cross check point
			psTestResult = checkPressureSensors_(pConvertedData);
			if (psTestResult == PRESSURE_SENSORS_OK)
			{
				CalState = PC_WAIT_L2H;
				RETAILMSG(1, (_T("Pressure Sensor OK\r\n")));
			}
			else
			{
				CalState = PC_WAIT_L2H;
				NextCalState = PC_ABORT_L2H;
				RETAILMSG(1, (_T("Pressure Sensor failed\r\n")));
			}
		}
		else 
		{
			CalDac += EXH_VALVE_CAL_DAC_INCREMENT;
		
#ifdef DEBUG
			if(PexhAvg >= PexhDebug)
			{
				WCHAR StrP1[10];
				wsprintf(StrP1, L"%3.2f",pConvertedData->InspPressure);
				wsprintf(StrPdrive, L"%3.2f",pConvertedData->ExhDrivePressure);
				wsprintf(StrPexh, L"%3.2f",pConvertedData->ExpPressure);
				RETAILMSG(1, (_T("Pd=%s, P1=%s, Pex=%s, Dac=%d \r\n"), StrPdrive, StrP1, StrPexh, CalDac));
				PexhDebug += 1;
			}
#endif
			if(CalDac > MAX_DAC_VALUE)
			{
				wsprintf(StrPdrive, L"%3.2f",PdrvAvg);
				wsprintf(StrPexh, L"%3.2f",PexhAvg);

				RETAILMSG(1, (_T("PC_ZERO_UP: Could not get to target, Pdrive=%s, Pexh=%s \r\n"), StrPdrive, StrPexh));
				CalState = PC_ABORT_L2H;
			}
			else if(PexhAvg > 0)
			{
				DelayCount = 1;
				PreviousTickCount = GetTickCount();
				CalState = PC_WAIT_L2H;
				NextCalState = PC_ZERO_UP_L2H;
			}
		}
		break;

	case PC_ZERO_DOWN_L2H:
		if(PexhAvg < 1.00)
		{
			CalDac -= EXH_VALVE_CAL_DAC_INCREMENT;
			DelayCount = EXH_MOVE_CHECK_WAIT_COUNT;
			CalState = PC_WAIT_L2H;
			NextCalState = PC_MOVE_L2H;

			PexhPrev = PexhAvg;
			wsprintf(StrPdrive, L"%3.2f",PdrvAvg);
			wsprintf(StrPexh, L"%3.2f",PexhAvg);
			RETAILMSG(1, (_T("PC_ZERO_DOWN: Pdrive=%s, Pexh=%s, Dac=%d\r\n"), StrPdrive, StrPexh, CalDac));
		}
		else if(CalDac == 0)
		{
			RETAILMSG(1, (L"PdriveCal failed at PC_ZERO_DOWN\r\n"));
			CalState = PC_ABORT_L2H;
		}
		else
		{
#ifdef DEBUG
			if(PexhAvg <= PexhDebug)
			{
				WCHAR StrP1[10];
				wsprintf(StrP1, L"%3.2f",pConvertedData->InspPressure);
				wsprintf(StrPdrive, L"%3.2f",pConvertedData->ExhDrivePressure);
				wsprintf(StrPexh, L"%3.2f",pConvertedData->ExpPressure);
				RETAILMSG(1, (_T("Pd=%s, P1=%s, Pex=%s, Dac=%d \r\n"), StrPdrive, StrP1, StrPexh, CalDac));
				PexhDebug -= 1;
			}
#endif
			CalDac -= EXH_VALVE_CAL_DAC_INCREMENT;
			DelayCount = EXH_MOVE_CHECK_WAIT_COUNT;
			CalState = PC_WAIT_L2H;
			NextCalState = PC_ZERO_DOWN_L2H;
		}
		PreviousTickCount = GetTickCount();
		break;

	case PC_MOVE_L2H:
		wsprintf(StrPdrive, L"%3.2f",PdrvAvg);
		wsprintf(StrPexh, L"%3.2f",PexhAvg);

		// TODO E600_LL: need to review the following criteria
		// Verify that the pressure changes and also less than a threshold
		if(fabs(PexhAvg - PexhPrev) <= 0.1)
		{
			if((GetTickCount() - PreviousTickCount) > EXH_MOVE_CHECK_WAIT_COUNT)
			{
				Pdrv[CalIndex] = pConvertedData->FilteredExhDrivePressure;
				Pexh[CalIndex] = pConvertedData->FilteredExpPressure;
				DacCount[CalIndex] = CalDac;
				CalIndex++;
				CalPtgt += EXH_CAL_STEP;
				CalState = PC_WAIT_L2H;
				NextCalState = PC_CHECK_L2H;

				RETAILMSG(1, (_T("PC_MOVE: Pdrive=%s, Pexh=%s, Dac=%d\r\n"), StrPdrive, StrPexh, CalDac));
				preTimer = GetTickCount();
			}
		}
		else
		{
			PreviousTickCount = GetTickCount();
			CalDac -= EXH_VALVE_CAL_DAC_INCREMENT;
		}

		if(CalDac == 0)
		{
			RETAILMSG(1, (L"PdriveCal failed at PC_MOVE, Pdrive=%s, Pexh=%s\r\n", StrPdrive, StrPexh));
			CalState = PC_ABORT_L2H;
		}
		PexhPrev = PexhAvg;
		break;

	case PC_WAIT_L2H:
		if((GetTickCount() - PreviousTickCount) >= DelayCount)
		{
			CalState = NextCalState;
		}
		break;

	case PC_CHECK_L2H:
		wsprintf(StrPdrive, L"%3.2f",PdrvAvg);
		wsprintf(StrPexh, L"%3.2f",PexhAvg);
		
		if(PexhAvg >= CalPtgt)
		{
			timer = GetTickCount()- preTimer;		
			RETAILMSG(1, (_T("Pressurization Time = %d\r\n"), timer));
			timeoutOccurred = FALSE;
			timeoutOccurred = checkTimeout_(CalPtgt, timer);
			preTimer = GetTickCount();

			RETAILMSG(1, (_T("Pdrive=%s, Pexh=%s, Dac=%d\r\n"), StrPdrive, StrPexh, CalDac));

			Pdrv[CalIndex] = PdrvAvg;
			Pexh[CalIndex] = PexhAvg;
			DacCount[CalIndex] = CalDac;

			if(++CalIndex >= EXH_VALVE_SUBTABLE_SIZE)
			{
				CalState = PC_PRINT_L2H;
				L2hCalCompleted = true;
			}
			else
			{
				CalPtgt += EXH_CAL_STEP;
			}					
		}

		if(!L2hCalCompleted)
		{
			if(++CalDac > MAX_DAC_VALUE)
			{
				for(int i = CalIndex; i < EXH_VALVE_SUBTABLE_SIZE; i++)
				{
					Pdrv[i] = Pdrv[i - 1];
					Pexh[i] = Pexh[i - 1];
					DacCount[i] = MAX_DAC_VALUE;
				}
				CalState = PC_PRINT_L2H;
			}
			else	// otherwise,
			{
				DelayCount = EXH_CAL_WAIT_COUNT;
				PreviousTickCount = GetTickCount();
				CalState = PC_WAIT_L2H;
				if (timeoutOccurred == TRUE)
				{
					RETAILMSG(1, (L"Pressurization Time out Failure\r\n"));
					NextCalState = PC_ABORT_L2H;
				}
				else
				{
					NextCalState = PC_CHECK_L2H;
				}
			}
		}
		break;

	case PC_PRINT_L2H:
		RETAILMSG(1, (L"\r\nfloat Pdrv_Tab[EXH_VALVE_SUBTABLE_SIZE] =\r\n{ "));
		for(int i=0; i < EXH_VALVE_SUBTABLE_SIZE; i++)
		{
			if((i != 0) && ((i % 10) == 0))
				RETAILMSG(1, (L"\r\n"));

			wsprintf(StrPdrive, L"%3.2f", Pdrv[i]);
			if(i < EXH_VALVE_SUBTABLE_SIZE - 1)
				RETAILMSG(1, (L"%s, ", StrPdrive));
			else
				RETAILMSG(1, (L"%s };\r\n", StrPdrive));
		}

		RETAILMSG(1, (L"\r\nfloat P2_Tab[EXH_VALVE_SUBTABLE_SIZE] =\r\n{ "));
		for(int i=0; i < EXH_VALVE_SUBTABLE_SIZE; i++)
		{
			if((i != 0) && ((i % 10) == 0))
				RETAILMSG(1, (L"\r\n"));

			wsprintf(StrPexh, L"%3.2f", Pexh[i]);
			if(i < EXH_VALVE_SUBTABLE_SIZE - 1)
				RETAILMSG(1, (L"%s, ", StrPexh));
			else
				RETAILMSG(1, (L"%s };\r\n", StrPexh));
		}

		RETAILMSG(1, (L"\r\nfloat Dac_Tab[EXH_VALVE_SUBTABLE_SIZE] =\r\n{ "));
		for(int i=0; i < EXH_VALVE_SUBTABLE_SIZE; i++)
		{
			if((i != 0) && ((i % 10) == 0))
				RETAILMSG(1, (L"\r\n"));

			if(i < EXH_VALVE_SUBTABLE_SIZE - 1)
				RETAILMSG(1, (L"%d, ", DacCount[i]));
			else
				RETAILMSG(1, (L"%d };\r\n\r\n", DacCount[i]));
		}

		CalState = PC_COMPLETE_L2H;
		break;

	case PC_ABORT_L2H:
		RSmFlowController.establishFlow( 0, DELIVERY, AIR);

		// TODO E600_LL: see NOTE 2 above
		ControlPSOL(DAC_EXHALATION_VALVE, 0);
		CalibrationDone = true;
		CalibrationStatus = CAL_FAILED;
		CalState = PC_INIT_L2H;
		break;

	case PC_COMPLETE_L2H:
		for(int i = 0; i < EXH_VALVE_SUBTABLE_SIZE; i++)
			pCalData[EXH_VALVE_SUBTABLE_SIZE*3+i] = Pdrv[i];
		for(int i = 0; i < EXH_VALVE_SUBTABLE_SIZE; i++)
			pCalData[EXH_VALVE_SUBTABLE_SIZE*4+i] = Pexh[i];
		for(int i = 0; i < EXH_VALVE_SUBTABLE_SIZE; i++)
			pCalData[EXH_VALVE_SUBTABLE_SIZE*5+i] = (float)DacCount[i];

		L2hCalCompleted = true;
		CalState = PC_INIT;
		break;

//	************** start high to low calibration *************
	case PC_INIT:
		if(pConvertedData->FilteredAirFlow > EXH_VALVE_CAL_MIN_FLOW)
		{
			CalPtgt = (EXH_VALVE_SUBTABLE_SIZE * EXH_CAL_STEP) - EXH_CAL_STEP;
			CalIndex = EXH_VALVE_SUBTABLE_SIZE - 1;
			CalCompleted = false;
			PexhPrev = PexhAvg;
			CalState = PC_WAIT;
			NextCalState = PC_ZERO_DOWN;
			DelayCount = EXH_FLOW_WAIT_COUNT;
			PreviousTickCount = GetTickCount();
			PexhDebug = 0;
		}
		// TODO E600_LL: see NOTE 2 above
		ControlPSOL(DAC_EXHALATION_VALVE, CalDac);
		break;

	case PC_ZERO_UP:
		if(PexhAvg > CalPtgt)
		{
			CalDac -= EXH_VALVE_CAL_DAC_INCREMENT;
			DelayCount = EXH_MOVE_CHECK_WAIT_COUNT;
			PreviousTickCount = GetTickCount();
			CalState = PC_WAIT;
			NextCalState = PC_ZERO_DOWN;
			PexhDebug = CalPtgt + 1;

			wsprintf(StrPdrive, L"%3.2f",PdrvAvg);
			wsprintf(StrPexh, L"%3.2f",PexhAvg);
			RETAILMSG(1, (_T("PC_ZERO_UP: Pd=%s, P2=%s, Dac=%d\r\n"), StrPdrive, StrPexh, CalDac));
		}
		else
		{
			CalDac += EXH_VALVE_CAL_DAC_INCREMENT;
#ifdef DEBUG
			if(PexhAvg >= PexhDebug)
			{
				WCHAR StrP1[10];
				wsprintf(StrP1, L"%3.2f",pConvertedData->InspPressure);
				wsprintf(StrPdrive, L"%3.2f",pConvertedData->ExhDrivePressure);
				wsprintf(StrPexh, L"%3.2f",pConvertedData->ExpPressure);
				RETAILMSG(1, (_T("Pd=%s, P1=%s, P2=%s, Dac=%d\r\n"), StrPdrive, StrP1, StrPexh, CalDac));
				PexhDebug += 1;
			}
#endif
			if(CalDac > MAX_DAC_VALUE)
			{
				wsprintf(StrPdrive, L"%3.2f",PdrvAvg);
				wsprintf(StrPexh, L"%3.2f",PexhAvg);

				RETAILMSG(1, (_T("PC_ZERO_UP: Could not get to target, Pd=%s, P2=%s \r\n"), StrPdrive, StrPexh));
				CalState = PC_ABORT;
			}
			else if(PexhAvg > 0)
			{
				DelayCount = EXH_MOVE_CHECK_WAIT_COUNT;
				PreviousTickCount = GetTickCount();
				CalState = PC_WAIT;
				NextCalState = PC_ZERO_UP;
			}
		}
		break;

	case PC_ZERO_DOWN:
		if(PexhAvg < CalPtgt)
		{
			CalDac -= EXH_VALVE_CAL_DAC_INCREMENT;
			DelayCount = EXH_MOVE_CHECK_WAIT_COUNT;
			CalState = PC_WAIT;
			NextCalState = PC_MOVE;

			PexhPrev = PexhAvg;
			wsprintf(StrPdrive, L"%3.2f",PdrvAvg);
			wsprintf(StrPexh, L"%3.2f",PexhAvg);
			RETAILMSG(1, (_T("PC_ZERO_DOWN: Pd=%s, P2=%s, Dac=%d\r\n"), StrPdrive, StrPexh, CalDac));
		}
		else if(CalDac == 0)
		{
			RETAILMSG(1, (L"PdriveCal failed at PC_ZERO_DOWN\r\n"));
			CalState = PC_ABORT;
		}
		else
		{
#ifdef DEBUG
			if(PexhAvg <= PexhDebug)
			{
				WCHAR StrP1[10];
				wsprintf(StrP1, L"%3.2f",pConvertedData->InspPressure);
				wsprintf(StrPdrive, L"%3.2f",pConvertedData->ExhDrivePressure);
				wsprintf(StrPexh, L"%3.2f",pConvertedData->ExpPressure);
				RETAILMSG(1, (_T("Pd=%s, P1=%s, Pex=%s, Dac=%d \r\n"), StrPdrive, StrP1, StrPexh, CalDac));
				PexhDebug -= 1;
			}
#endif
			CalDac -= EXH_VALVE_CAL_DAC_INCREMENT;
			DelayCount = EXH_MOVE_CHECK_WAIT_COUNT;
			CalState = PC_WAIT;
			NextCalState = PC_ZERO_DOWN;
		}
		PreviousTickCount = GetTickCount();
		break;

	case PC_MOVE:
		if(fabs(PexhAvg - PexhPrev) < 0.1)
		{
			if((GetTickCount() - PreviousTickCount) > EXH_MOVE_CHECK_WAIT_COUNT)
			{
				Pdrv[CalIndex] = pConvertedData->FilteredExhDrivePressure;
				Pexh[CalIndex] = pConvertedData->FilteredExpPressure;
				DacCount[CalIndex] = CalDac;
				CalIndex--;
				CalPtgt -= EXH_CAL_STEP;
				CalState = PC_WAIT;
				NextCalState = PC_CHECK;

				wsprintf(StrPdrive, L"%3.2f",PdrvAvg);
				wsprintf(StrPexh, L"%3.2f",PexhAvg);
				RETAILMSG(1, (_T("PC_MOVE: Pd=%s, P2=%s, Dac=%d\r\n"), StrPdrive, StrPexh, CalDac));
			}
		}
		else
		{
			PreviousTickCount = GetTickCount();
			CalDac -= EXH_VALVE_CAL_DAC_INCREMENT;
		}

		PexhPrev = PexhAvg;
		if(CalDac == 0)
		{
			RETAILMSG(1, (L"PdriveCal failed at PC_MOVE\r\n"));
			CalState = PC_ABORT;
		}
		break;

	case PC_WAIT:
		if((GetTickCount() - PreviousTickCount) >= DelayCount)
		{
			CalState = NextCalState;
		}
		break;

	case PC_CHECK:
		wsprintf(StrPdrive, L"%3.2f",PdrvAvg);
		wsprintf(StrPexh, L"%3.2f",PexhAvg);

		if(PexhAvg <= CalPtgt)
		{
			RETAILMSG(1, (_T("Pd=%s, P2=%s, Dac=%d\r\n"), StrPdrive, StrPexh, CalDac));

			Pdrv[CalIndex] = PdrvAvg;
			Pexh[CalIndex] = PexhAvg;
			DacCount[CalIndex] = CalDac;
			if(--CalIndex < 0)
			{
				CalState = PC_PRINT;
				CalCompleted = true;
			}
			else
			{
				CalPtgt -= EXH_CAL_STEP;
			}
		}

		if(!CalCompleted)
		{
			// if the pressure is low enough and we are at the last index, complete it
			if((PdrvAvg < 0.3 || PexhAvg < 0.3) && (CalIndex == 0))
			{
				Pdrv[CalIndex] = PdrvAvg;
				Pexh[CalIndex] = PexhAvg;
				DacCount[CalIndex] = CalDac;
				CalState = PC_PRINT;
			}
			else	// otherwise,
			{
				CalDac -= EXH_VALVE_CAL_DAC_INCREMENT;
				if(CalDac == 0)
				{
					RETAILMSG(1, (_T("Pressure not drop low enough at PC_CHECK, Pdrive=%s, Pexh=%s\r\n"), StrPdrive, StrPexh));
					// For current pneumatics, P2 might be higher than the minimum limit because of purge flow even though exhalation valve is fully open.
					// In this case, just save the current pressure for both P2 and Pdrv for the remaining indexes.
					for(int i = 0; i <= CalIndex; i++)
					{
						Pdrv[i] = PdrvAvg;
						Pexh[i] = PexhAvg;
						DacCount[i] = 0;
					}
					CalState = PC_PRINT;
				}
				else
				{
					DelayCount = EXH_CAL_WAIT_COUNT;
					PreviousTickCount = GetTickCount();
					CalState = PC_WAIT;
					NextCalState = PC_CHECK;
				}
			}
		}
		break;

	case PC_PRINT:
		RETAILMSG(1, (L"\r\nfloat PdrvTable[EXH_VALVE_SUBTABLE_SIZE] =\r\n{ "));

		for(int i=0; i < EXH_VALVE_SUBTABLE_SIZE; i++)
		{
			if((i != 0) && ((i % 10) == 0))
				RETAILMSG(1, (L"\r\n"));

			wsprintf(StrPdrive, L"%3.2f", Pdrv[i]);
			if(i < EXH_VALVE_SUBTABLE_SIZE - 1)
				RETAILMSG(1, (L"%s, ", StrPdrive));
			else
				RETAILMSG(1, (L"%s };\r\n", StrPdrive));
		}

		RETAILMSG(1, (L"\r\nfloat P2Table[EXH_VALVE_SUBTABLE_SIZE] =\r\n{ "));
		for(int i=0; i < EXH_VALVE_SUBTABLE_SIZE; i++)
		{
			if((i != 0) && ((i % 10) == 0))
				RETAILMSG(1, (L"\r\n"));

			wsprintf(StrPexh, L"%3.2f", Pexh[i]);
			if(i < EXH_VALVE_SUBTABLE_SIZE - 1)
				RETAILMSG(1, (L"%s, ", StrPexh));
			else
				RETAILMSG(1, (L"%s };\r\n", StrPexh));
		}

		RETAILMSG(1, (L"\r\nfloat DacTable[EXH_VALVE_SUBTABLE_SIZE] =\r\n{ "));
		for(int i=0; i < EXH_VALVE_SUBTABLE_SIZE; i++)
		{
			if((i != 0) && ((i % 10) == 0))
				RETAILMSG(1, (L"\r\n"));

			if(i < EXH_VALVE_SUBTABLE_SIZE - 1)
				RETAILMSG(1, (L"%d, ", DacCount[i]));
			else
				RETAILMSG(1, (L"%d };\r\n\r\n", DacCount[i]));
		}

		CalState = PC_COMPLETE;
		break;

	case PC_ABORT:
		RSmFlowController.establishFlow( 0, DELIVERY, AIR);

		// TODO E600_LL: see NOTE 2 above
		ControlPSOL(DAC_EXHALATION_VALVE, 0);
		CalibrationDone = true;
		CalibrationStatus = CAL_FAILED;
		CalState = PC_START_FLOW;
		break;

	case PC_COMPLETE:
		RSmFlowController.establishFlow( 0, DELIVERY, AIR);
		
		// TODO E600_LL: see NOTE 2 above
		ControlPSOL(DAC_EXHALATION_VALVE, 0);

		for(int i = 0; i < EXH_VALVE_SUBTABLE_SIZE; i++)
			pCalData[i] = Pdrv[i];
		for(int i = 0; i < EXH_VALVE_SUBTABLE_SIZE; i++)
			pCalData[EXH_VALVE_SUBTABLE_SIZE+i] = Pexh[i];
		for(int i = 0; i < EXH_VALVE_SUBTABLE_SIZE; i++)
			pCalData[EXH_VALVE_SUBTABLE_SIZE*2+i] = (float)DacCount[i];

        CalibrationStatus = CAL_PASSED;
        CalCompleted = true;
		CalibrationDone = true;
		CalState = PC_START_FLOW;
		break;

	default:
		break;
		}
		
	if(!CalibrationDone)
	{
		// TODO E600_LL: see NOTE 2 above
		ControlPSOL(DAC_EXHALATION_VALVE, CalDac);
	}
	return CalibrationDone;
}

//*****************************************************************************
uint16_t ExhValveCalibrationTable_t::GetDriveDAC(Pressure_t P2)  //high to low table for PEEP controller
{
	ExhValveTable_t *pExhTab = (ExhValveTable_t *)pTable;
	float PdrvDac;
	float Pdrive;

	LookUpFloat(&Pdrive, pExhTab->PdrvTable, P2, pExhTab->P2Table, EXH_VALVE_SUBTABLE_SIZE-1);
	LookUpFloat(&PdrvDac, pExhTab->DacTable, Pdrive, pExhTab->PdrvTable, EXH_VALVE_SUBTABLE_SIZE-1);

	return (uint16_t) PdrvDac;
}

//*****************************************************************************
uint16_t ExhValveCalibrationTable_t::GetDriveDAC_l2h(Pressure_t P2)  //low to high table for pressure controller
{
	ExhValveTable_t *pExhTab = (ExhValveTable_t *)pTable;
	float PdrvDac;
	float Pdrive;

	LookUpFloat(&Pdrive, pExhTab->PdrvTable_l2h, P2, pExhTab->P2Table_l2h, EXH_VALVE_SUBTABLE_SIZE-1);
	LookUpFloat(&PdrvDac, pExhTab->DacTable_l2h, Pdrive, pExhTab->PdrvTable_l2h, EXH_VALVE_SUBTABLE_SIZE-1);

	return (uint16_t) PdrvDac;
}

//*****************************************************************************
//Flow sensor cross check
ExhValveCalibrationTable_t::FlowSensorTestResult
ExhValveCalibrationTable_t::checkFlowSensors_ (const ConvertedA2dReadings_t *pConvertedData)
{
	
	FlowSensorTestResult result = FLOW_SENSORS_OK;

	// read filtered flow values
	Real32 inspFlow = pConvertedData->FilteredAirFlow;
	Real32 exhFlow = pConvertedData->FilteredExhFlow;
	// TODO E600_LL: triple the error limit for now and review later
	//Real32 ErrorLimit = 0.6;  //0.1 insp + 0.5 exp
	Real32 ErrorLimit = 1.8;

	if ((inspFlow - exhFlow) > ErrorLimit || 
		(inspFlow - exhFlow) < -ErrorLimit)
	{
		result = FS_CROSS_CHECK_FAILURE;
	}
	else
	{
		result = FLOW_SENSORS_OK;
	}
	// implied else: flow cross-check passed

	return( result);
}

//*****************************************************************************
Boolean
ExhValveCalibrationTable_t::checkTimeout_( const Uint32 tgtPressure,
									const Int32 timer)
{
	Boolean timeoutOccurred;

	if (tgtPressure == MIN_CALIB_PRESSURE)
	{
		// check the timeout for the 1st calibration pressure
		if (timer >= INITIAL_PRESSURE_TIMEOUT)
		{
			timeoutOccurred = TRUE;
		}
		else
		{
			timeoutOccurred = FALSE;
		}		
		// end else
	}
	else
	{
		// check the timeout for all other calibration pressures
		if (timer >= NORMAL_PRESSURE_TIMEOUT)
		{
			timeoutOccurred = TRUE;
		}
		else
		{
			timeoutOccurred = FALSE;
		}		
		// end else
	}		
	// end else

	return( timeoutOccurred);
}

//*****************************************************************************
ExhValveCalibrationTable_t::PressureSensorTestResult
ExhValveCalibrationTable_t::checkPressureSensors_ (const ConvertedA2dReadings_t *pConvertedData)
{
	
	PressureSensorTestResult result = PRESSURE_SENSORS_OK;

	Real32 inspPres = pConvertedData->FilteredInspPressure;
	Real32 exhPres = pConvertedData->FilteredExpPressure;

	if ( ABS_VALUE(inspPres - exhPres) >
			(inspPres + exhPres) * REL_PRESSURE_ERROR +
			(2 * ABS_PRESSURE_ERROR) )
	{
		result = PS_CROSS_CHECK_FAILURE;
	}
	else if ( ABS_VALUE(inspPres - exhPres) >
			(inspPres + exhPres) / 2.0 * REL_PRESSURE_ERROR +
			ABS_PRESSURE_ERROR )
	{
		result = PS_CROSS_CHECK_ALERT;
	}
	// implied else: pressure cross-check passed this iteration

	return( result);
}