
#ifndef SafetyPcvSettingValues_HH
#define SafetyPcvSettingValues_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: SafetyPcvSettingValues - Manager of the Safety-PCV Setting Values.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/SafetyPcvSettingValues.hhv   25.0.4.0   19 Nov 2013 14:27:40   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality.
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//  
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//====================================================================

#include "SettingId.hh"

//@ Usage-Classes
#include "SettingValue.hh"
//@ End-Usage


class SafetyPcvSettingValues
{
  public:
    static void  Initialize(void);

    static inline SettingValue  GetValue(
				  const SettingId::SettingIdType settingId
					);

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL, 
			   const char*       pPredicate = NULL);

  private:
    SafetyPcvSettingValues(const SafetyPcvSettingValues&);// not implemented
    SafetyPcvSettingValues(void);			// not implemented...
    ~SafetyPcvSettingValues(void);			// not implemented...
    void  operator=(const SafetyPcvSettingValues&);	// not implemented...

    //@ Data-Member:  ArrSafetyPcvValues_
    // Static pointer to the array containing the Safety-PCV values for each
    // of the batch settings that are applicable during safe-mode
    // ventilation.
    static SettingValue*  ArrSafetyPcvValues_;
};


// Inlined methods
#include "SafetyPcvSettingValues.in"


#endif // SafetyPcvSettingValues_HH 
