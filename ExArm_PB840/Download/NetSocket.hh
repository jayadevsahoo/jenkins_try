#ifndef NetSocket_HH
#define NetSocket_HH

//=====================================================================
//
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: NetSocket - provides a network socket functionality to
//  	transmit and receive data.
//---------------------------------------------------------------------
//@ Version
//@(#) $Header:   /840/Baseline/Download/vcssrc/NetSocket.hhv   25.0.4.0   19 Nov 2013 14:02:50   pvcs  $ 
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date: 15-Jan-99    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001  By:  <jhv>    Date:  05-Jan-96    DR Number: 
//	Project:  Sigma (R8027)
//	Description:
//		Initial version (Integration baseline).
//
//====================================================================

#include "sockintf.hh"
#include "DownloadFault.hh"
#include "DownloadTypes.hh"
#include "DownloadPortIds.hh"

//TODO E600 removed
//#include <netdb.h>
//#include <arpa/inet.h>

// TODO E600 replaced #include <netinet/in.h> with #include <Winsock2.h>
//#include <netinet/in.h>
#include <Winsock2.h>

#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

// TODO E600 include the correct library
//#include "socket.h"

#if	defined(SIGMA_SERVER)
	extern Int errno;
#else
	extern short  soError;
#define errno  soError
#endif //defined(SIGMA_CLIENT)

//union Saddr 
//{
//	struct  sockaddr   sa;
//	struct  sockaddr_in  in;
//};

class NetSocket
{
public:
	NetSocket(void);
	virtual ~NetSocket(void);
	int readData(void *buf,int size);
	int sendData(char *buf,int size);
	inline int getSocketId(void);
#ifdef SIGMA_SERVER
	Uint32 getHostIpAddr(void);
#endif // SIGMA_SERVER
	inline Boolean opened(void);
	virtual Int open( PortId portId, Int nListens);
	void close(void);
#ifdef SIGMA_UNIT_TEST
	Int callTransmit(char *buf, Int nbytes);
	Int callReceive(char *pData, Int nbytes);
	Int callCreateSocket(Int protocol, PortId portId, Int nListens);
	void setSocketIdOnly(Int socketId);
#endif // SIGMA_UNIT_TEST

    static void SoftFault(const FaultType softFaultID,
		const Uint32      lineNumber,
		const char*       pFileName  = NULL, 
		const char*       pPredicate = NULL);

protected:
	virtual Int transmit_(char *buf, Int nbytes);
	virtual Int receive_(char *pData, Int nbytes);
	Int createSocket_(Int protocol, PortId portId, Int nListens);

    //@ Data-Member:  socketId_
    // NetSocket file descriptor id.
		Int	socketId_;

private:
    NetSocket(const NetSocket&);				// not implemented...
    void   operator=(const NetSocket&);	// not implemented...
};

//include inline methods
#include "NetSocket.in"

#endif // NetSocket_HH
