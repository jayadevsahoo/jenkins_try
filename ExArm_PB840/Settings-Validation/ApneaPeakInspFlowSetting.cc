#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//================= C L A S S   D E S C R I P T I O N =================
//@ Class:  ApneaPeakInspFlowSetting - Apnea Peak Inspiratory Flow Setting
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, bounded setting that contains the maximum rate of
//  delivery (in liters-per-minute) of the specified "tidal volume" during
//  mandatory, volume-based apnea breaths (apnea VCV).  This class inherits
//  from 'BatchBoundedSetting' and provides the specific information needed
//  for representation of apnea peak inspiratory flow.  This information
//  includes the interval and range of this setting's values, this
//  setting's response to a Main Control Setting's transition (see
//  'acceptTransition()'), this batch setting's new-patient value (see
//  'getNewPatientValue()'), and the contraints and dependent settings of
//  this setting.
//
//  This class provides a static method for calculating apnea peak
//  inspiratory flow based on the value of other settings.  This
//  calculation method provides a standard way for all settings (as needed)
//  to calculate an apnea peak inspiratory flow value.
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class defines an 'updateConstraints_()' method for updating
//  the constraints of this setting.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/ApneaPeakInspFlowSetting.ccv   25.0.4.0   19 Nov 2013 14:27:12   pvcs  $
//
//@ Modification-Log
//
//  Revision: 010   By: mnr    Date: 10-Jul-2009    SCR Number: 6437
//  Project:  NEO
//  Description:
//		Corrected comment regarding mode transition.
//
//  Revision: 009  By: sah    Date:  05-Oct-2000    DR Number: 5730
//  Project:  VTPC
//  Description:
//	Modified new-patient formula to use twice the IBW factor for
//      RAMP flow patterns, and modified transition limits based on
//      new NEONATAL constant.
//
//  Revision: 008  By: sah    Date:  22-Aug-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//	VTPC project-related changes:
//      *  removed apnea min insp flow as a dependent setting, now
//         automatically tracks changes to this setting
//      *  updated pre-condition comment, and removed obsoleted
//         pre-condition, of 'calcBasedOnSetting_()'
//
//  Revision: 007   By: sah    Date: 29-Jun-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  modified to use newly-modified 'BoundedRange' class to handle
//         range, via linked list of interval nodes
//	*  incorporated circuit-specific new-patient values and ranges
//	*  re-wrote 'acceptTransition()' to handle NEONATAL circuit
//	*  added new 'getAbsolute{Min,Max}Value_()' methods to accomodate
//         new, dynamic limits
//
//  Revision: 006   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'getApplicability()' method
//	*  cleaned up DEVELOPMENT-only 'is{Adjusted,Accepted}Valid()' methods
//
//  Revision: 005   By: dosman    Date:  23-Sep-1998    DR Number: BILEVEL 66 & 144
//  Project:  BILEVEL
//  Description:
//      The intermediate value of inspiratory time computed in
//      calcBasedOnSetting_ was changed to be warped in the same
//      direction as the direction the "setting being calculated".
//      Also, special processing was added in BoundedSetting::calcNewValue()
//
//  Revision: 004   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 003   By: sah    Date:  28-May-1998    DR Number: 5058
//  Project:  COLOR
//  Description:
//	--- Merged Rev "Color" (1.39.1.0) into Rev "BiLevel" (1.39.2.0)
//	Modified range/resolution for improving the user's ability to
//	accurately change this setting's value.
//
//  Revision: 002   By: sah    Date: 23 Dec 1996    DR Number: 1624
//  Project: Sigma (R8027)
//  Description:
//	Incorporated new PC-to-VC formulas.
//
//  Revision: 001   By: sah    Date: 12 Dec 1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//=====================================================================

#include "ApneaPeakInspFlowSetting.hh"
#include "SettingConstants.hh"
#include "MandTypeValue.hh"
#include "FlowPatternValue.hh"
#include "PatientCctTypeValue.hh"

//@ Usage-Classes
#include "SettingsMgr.hh"
#include "ApneaRespRateSetting.hh"
//@ End-Usage


//@ Code...

//====================================================================
//
//  Static Data Definitions...
//
//====================================================================

// $[02087] -- this setting's dependent settings...
static const SettingId::SettingIdType  ARR_DEPENDENT_SETTING_IDS_[] =
  {
    SettingId::APNEA_IE_RATIO,
    SettingId::APNEA_INSP_TIME,
    SettingId::APNEA_EXP_TIME,

    SettingId::NULL_SETTING_ID
  };


//  $[02083] -- The setting's range ...
//  $[02085] -- The setting's resolution ...
static const BoundedInterval  LAST_NODE_ =
  {
    1.0f,		// absolute minimum...
    0.0f,		// unused...
    TENTHS,		// unused...
    NULL
  };
static const BoundedInterval  NODE1_ =
  {
    20.0f,		// change-point...
    0.1f,		// resolution...
    TENTHS,		// precision...
    &::LAST_NODE_
  };
static const BoundedInterval  INTERVAL_LIST_ =
  {
    150.0f,		// absolute maximum...
    1.0f,		// resolution...
    ONES,		// precision...
    &::NODE1_
  };


//====================================================================
//
//  Public Methods...
//
//====================================================================

//============= M E T H O D   D E S C R I P T I O N ==================
//@ Method: ApneaPeakInspFlowSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This setting inherits from BoundedSetting.  The method passes to
//  BoundedSetting it's default information. Also, this method initializes
//  the value interval.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ApneaPeakInspFlowSetting::ApneaPeakInspFlowSetting(void)
		: BatchBoundedSetting(SettingId::APNEA_PEAK_INSP_FLOW,
				      ::ARR_DEPENDENT_SETTING_IDS_,
				      &::INTERVAL_LIST_,
				      APNEA_PEAK_FLOW_MAX_BASED_CCT_TYPE_ID,
				      APNEA_PEAK_FLOW_MIN_BASED_CCT_TYPE_ID)
{
  CALL_TRACE("ApneaPeakInspFlowSetting()");
}   // $[TI1]


//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  ~ApneaPeakInspFlowSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Default destructor.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ApneaPeakInspFlowSetting::~ApneaPeakInspFlowSetting(void)
{
  CALL_TRACE("~ApneaPeakInspFlowSetting()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[01096] -- apnea breath settings applicability
//---------------------------------------------------------------------
//@ PreCondition
//  (qualifierId == Notification::ACCEPTED  ||
//   qualifierId == Notification::ADJUSTED)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
ApneaPeakInspFlowSetting::getApplicability(
			      const Notification::ChangeQualifier qualifierId
				      ) const
{
  CALL_TRACE("getApplicability(qualifierId)");

  const Setting*  pApneaMandType =
		      SettingsMgr::GetSettingPtr(SettingId::APNEA_MAND_TYPE);

  DiscreteValue  apneaMandTypeValue;

  switch (qualifierId)
  {
  case Notification::ACCEPTED :		// $[TI1]
    apneaMandTypeValue = pApneaMandType->getAcceptedValue();
    break;
  case Notification::ADJUSTED :		// $[TI2]
    apneaMandTypeValue = pApneaMandType->getAdjustedValue();
    break;
  default :
    AUX_CLASS_ASSERTION_FAILURE(qualifierId);
    break;
  }

  return((apneaMandTypeValue == MandTypeValue::VCV_MAND_TYPE)
	   ? Applicability::CHANGEABLE		// $[TI3]
	   : Applicability::INAPPLICABLE);	// $[TI4]
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [const, virtual]
//
//@ Interface-Description
//  Return this setting's new-patient value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02084] -- new-patient value...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
ApneaPeakInspFlowSetting::getNewPatientValue(void) const
{
  CALL_TRACE("getNewPatientValue()");

  const Setting*  pPatientCctType =
		    SettingsMgr::GetSettingPtr(SettingId::PATIENT_CCT_TYPE);
  const Setting*  pApneaFlowPattern =
		    SettingsMgr::GetSettingPtr(SettingId::APNEA_FLOW_PATTERN);

  const DiscreteValue  PATIENT_CCT_TYPE_VALUE =
					pPatientCctType->getAdjustedValue();
  const DiscreteValue  APNEA_FLOW_PATTERN_VALUE =
				    pApneaFlowPattern->getNewPatientValue();

  Real32  ibwScalingFactor;

  switch (PATIENT_CCT_TYPE_VALUE)
  {
  case PatientCctTypeValue::NEONATAL_CIRCUIT :		// $[TI1]
    ibwScalingFactor = SettingConstants::NP_PEAK_FLOW_NEO_IBW_SCALE;
    break;
  case PatientCctTypeValue::PEDIATRIC_CIRCUIT :		// $[TI2]
    ibwScalingFactor = SettingConstants::NP_PEAK_FLOW_PED_IBW_SCALE;
    break;
  case PatientCctTypeValue::ADULT_CIRCUIT :		// $[TI3]
    ibwScalingFactor = SettingConstants::NP_PEAK_FLOW_ADULT_IBW_SCALE;
    break;
  default :
    // unexpected patient circuit type value...
    AUX_CLASS_ASSERTION_FAILURE(PATIENT_CCT_TYPE_VALUE);
    break;
  }

  // The adjusted context contains the new-patient value for IBW.
  const Setting*  pIbw = SettingsMgr::GetSettingPtr(SettingId::IBW);

  const Real32  IBW_VALUE = BoundedValue(pIbw->getAdjustedValue()).value;

  const Real32  ABSOLUTE_MIN = getAbsoluteMinValue_();

  BoundedValue  newPatient;

  // calculate the new patient value...
  newPatient.value = (IBW_VALUE * ibwScalingFactor);

  if (APNEA_FLOW_PATTERN_VALUE == FlowPatternValue::RAMP_FLOW_PATTERN)
  {  // $[TI6]
    newPatient.value *= 2.0f;
  }  // $[TI7]

  // clip to absolute minimum...
  newPatient.value = MAX_VALUE(ABSOLUTE_MIN,			// $[TI4]
			       newPatient.value);		// $[TI5]

  // warp the calculated value to a "click" boundary...
  getBoundedRange().warpValue(newPatient);

  return(newPatient);
}


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method: acceptTransition(settingId, newValue, currValue) [virtual]
//
//@ Interface-Description
//  This method is responsible for updating this setting based on the
//  the Main Control Setting identified by 'settingId' and the transition
//  of its value from 'currValue' to 'newValue'.
//
//  This setting is only interested in changes of apnea mandatory type
//  from "PCV" to "VCV".
//---------------------------------------------------------------------
//@ Implementation-Description
//  The analysis that documents the definition of the "regions", and
//  their corresponding formulas, can be found in the Design History
//  File.  The document titled "Summary Analysis of Mandatory Type's
//  PC-to-VC Transition Rules" was submitted to the DHF in December 1996.
//
//  All of the testing of region-occupancy is done in such a way as to
//  take into account the inherent inaccuracies of floating-point numbers.
//  For example, when testing to see if respiratory rate is greater-than
//  or equal-to '(4.64 * IBW)', a small "epsilon" (approximately one-tenth
//  of the resolution) is subtracted to ensure the "equal-to" part of
//  the region is included.
//
//  $[02024]\c\   - transitioning based on circuit type...
//  $[NE02007]\b\ - transitioning from PC to VC (NEONATAL circuit)...
//  $[NE02008]\b\ - transitioning from PC to VC (PEDIATRIC circuit)...
//  $[NE02009]\b\ - transitioning from PC to VC (ADULT circuit)...
//---------------------------------------------------------------------
//@ PreCondition
//  (settingId == SettingId::APNEA_MAND_TYPE    &&
//   newValue  == MandTypeValue::VCV_MAND_TYPE)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
ApneaPeakInspFlowSetting::acceptTransition(
				   const SettingId::SettingIdType settingId,
				   const DiscreteValue            newValue,
				   const DiscreteValue
				          )
{
  CALL_TRACE("acceptTransition(settingId, newValue, currValue)");


  // apnea peak inspiratory flow's only transition is due to an apnea
  // mandatory type change from 'PCV' to 'VCV'...
  AUX_CLASS_PRE_CONDITION((settingId == SettingId::APNEA_MAND_TYPE),
  			  settingId);
  AUX_CLASS_PRE_CONDITION((newValue  == MandTypeValue::VCV_MAND_TYPE),
			  newValue);

  const Setting*  pApneaFlowPattern =
		    SettingsMgr::GetSettingPtr(SettingId::APNEA_FLOW_PATTERN);
  const Setting*  pIbw = SettingsMgr::GetSettingPtr(SettingId::IBW);
  const Setting*  pApneaRespRate =
		       SettingsMgr::GetSettingPtr(SettingId::APNEA_RESP_RATE);

  const DiscreteValue  APNEA_FLOW_PATTERN_VALUE =
				    pApneaFlowPattern->getAdjustedValue();
  const Real32  IBW_VALUE = BoundedValue(pIbw->getAdjustedValue()).value;
  const Real32  APNEA_RESP_RATE_VALUE =
		      BoundedValue(pApneaRespRate->getAdjustedValue()).value;

  const Setting*  pPatientCctType =
		    SettingsMgr::GetSettingPtr(SettingId::PATIENT_CCT_TYPE);

  const DiscreteValue  PATIENT_CCT_TYPE_VALUE =
					pPatientCctType->getAdjustedValue();

  BoundedValue  newApneaPeakFlow;

  //-------------------------------------------------------------------
  // NOTE:  the formulas below only consider the affects of respiratory
  //        rate, tidal volume and IBW, following this giant switch statement
  //        the resulting value is clipped based on this settings bounds...
  //-------------------------------------------------------------------

  switch (APNEA_FLOW_PATTERN_VALUE)
  {
  //-------------------------------------------------------------------
  // apnea flow pattern = 'DESCENDING RAMP'...
  //-------------------------------------------------------------------
  case FlowPatternValue::RAMP_FLOW_PATTERN :		// $[TI1]
    switch (PATIENT_CCT_TYPE_VALUE)
    {
    case PatientCctTypeValue::NEONATAL_CIRCUIT :	// $[TI1.1]
      if (APNEA_RESP_RATE_VALUE >= 40.0f)
      {   // $[TI1.1.1] -- f >= 40...
	const Setting*  pApneaTidalVolume =
		     SettingsMgr::GetSettingPtr(SettingId::APNEA_TIDAL_VOLUME);

	const Real32  APNEA_TIDAL_VOL_VALUE =
		     BoundedValue(pApneaTidalVolume->getAdjustedValue()).value;


	if (APNEA_TIDAL_VOL_VALUE >= 15.0f)
	{  // $[TI1.1.1.1.1] -- apnea min flow is 10% of apnea peak flow...
	  newApneaPeakFlow.value = (0.53f * APNEA_TIDAL_VOL_VALUE);
	}
	else
	{  // $[TI1.1.1.1.2] -- apnea min flow is fixed at 0.8 L/min...
	  newApneaPeakFlow.value = ((0.60f * APNEA_TIDAL_VOL_VALUE) - 0.8f);
	}
      }
      else
      {   // $[TI1.1.2] -- f < 40...
	newApneaPeakFlow.value =
	      (2.0f * SettingConstants::NP_PEAK_FLOW_NEO_IBW_SCALE * IBW_VALUE);
      }
      break;
    case PatientCctTypeValue::PEDIATRIC_CIRCUIT :	// $[TI1.2]
      if (APNEA_RESP_RATE_VALUE >= 34.0f)
      {   // $[TI1.2.1] -- f >= 34...
	const Setting*  pApneaTidalVolume =
		     SettingsMgr::GetSettingPtr(SettingId::APNEA_TIDAL_VOLUME);

	const Real32  APNEA_TIDAL_VOL_VALUE =
		     BoundedValue(pApneaTidalVolume->getAdjustedValue()).value;

	newApneaPeakFlow.value = (0.39f * APNEA_TIDAL_VOL_VALUE);
      }
      else
      {   // $[TI1.2.2] -- f < 34...
	newApneaPeakFlow.value =
	    (2.0f * SettingConstants::NP_PEAK_FLOW_PED_IBW_SCALE * IBW_VALUE);
      }
      break;
    case PatientCctTypeValue::ADULT_CIRCUIT :		// $[TI1.3]
      if (APNEA_RESP_RATE_VALUE >= 32.0f)
      {   // $[TI1.3.1] -- f >= 32...
	const Setting*  pApneaTidalVolume =
		     SettingsMgr::GetSettingPtr(SettingId::APNEA_TIDAL_VOLUME);

	const Real32  APNEA_TIDAL_VOL_VALUE =
		     BoundedValue(pApneaTidalVolume->getAdjustedValue()).value;

	newApneaPeakFlow.value = (0.39f * APNEA_TIDAL_VOL_VALUE);
      }
      else
      {   // $[TI1.3.2] -- f < 32...
	newApneaPeakFlow.value =
	  (2.0f * SettingConstants::NP_PEAK_FLOW_ADULT_IBW_SCALE * IBW_VALUE);
      }
      break;
    default :
      // unexpected patient circuit type value...
      AUX_CLASS_ASSERTION_FAILURE(PATIENT_CCT_TYPE_VALUE);
      break;
    }
    break;

  //-------------------------------------------------------------------
  // apnea flow pattern = 'SQUARE'...
  //-------------------------------------------------------------------
  case FlowPatternValue::SQUARE_FLOW_PATTERN :		// $[TI2]
    switch (PATIENT_CCT_TYPE_VALUE)
    {
    case PatientCctTypeValue::NEONATAL_CIRCUIT :	// $[TI2.1]
      newApneaPeakFlow.value =
		    (SettingConstants::NP_PEAK_FLOW_NEO_IBW_SCALE * IBW_VALUE);
      break;
    case PatientCctTypeValue::PEDIATRIC_CIRCUIT :	// $[TI2.2]
      if (APNEA_RESP_RATE_VALUE >= 38.0f)
      {   // $[TI2.2.1] -- f >= 38...
	const Setting*  pApneaTidalVolume =
		     SettingsMgr::GetSettingPtr(SettingId::APNEA_TIDAL_VOLUME);

	const Real32  APNEA_TIDAL_VOL_VALUE =
		     BoundedValue(pApneaTidalVolume->getAdjustedValue()).value;

	newApneaPeakFlow.value = (0.21f * APNEA_TIDAL_VOL_VALUE);
      }
      else
      {   // $[TI2.2.2] -- f < 38...
	newApneaPeakFlow.value =
		    (SettingConstants::NP_PEAK_FLOW_PED_IBW_SCALE * IBW_VALUE);
      }
      break;
    case PatientCctTypeValue::ADULT_CIRCUIT :		// $[TI2.3]
      if (APNEA_RESP_RATE_VALUE >= 35.0f)
      {   // $[TI2.3.1] -- f >= 35...
	const Setting*  pApneaTidalVolume =
		     SettingsMgr::GetSettingPtr(SettingId::APNEA_TIDAL_VOLUME);

	const Real32  APNEA_TIDAL_VOL_VALUE =
		     BoundedValue(pApneaTidalVolume->getAdjustedValue()).value;

	newApneaPeakFlow.value = (0.21f * APNEA_TIDAL_VOL_VALUE);
      }
      else
      {   // $[TI2.3.2] -- f < 35...
	newApneaPeakFlow.value =
		  (SettingConstants::NP_PEAK_FLOW_ADULT_IBW_SCALE * IBW_VALUE);
      }
      break;
    default :
      // unexpected patient circuit type value...
      AUX_CLASS_ASSERTION_FAILURE(PATIENT_CCT_TYPE_VALUE);
      break;
    }
    break;

  default :
    // invalid apnea flow pattern value...
    AUX_CLASS_ASSERTION_FAILURE(APNEA_FLOW_PATTERN_VALUE);
    break;
  };

  // ensure limits are updated...
  updateConstraints_();

  // round/clip calculated value..
  getBoundedRange_().warpValue(newApneaPeakFlow);

  // store the transition value into the adjusted context...
  setAdjustedValue(newApneaPeakFlow);

  setForcedChangeFlag();
}


#if defined(SIGMA_DEVELOPMENT)

//============== M E T H O D   D E S C R I P T I O N =================
// Method:  isAcceptedValid()  [const, virtual]
//
// Interface-Description
//  Is the accepted value of this setting valid as determined by its
//  dependencies?
//---------------------------------------------------------------------
// Implementation-Description
//  The setting determines, and returns its validity.
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

Boolean
ApneaPeakInspFlowSetting::isAcceptedValid(void) const
{
  CALL_TRACE("isAcceptedValid()");

  Boolean  isValid = BoundedSetting::isAcceptedValid();

  if (isValid)
  {
    if (getApplicability(Notification::ACCEPTED) !=
					    Applicability::INAPPLICABLE)
    {
      // is the the accepted apnea peak insp. flow value consistent with the
      // accepted apnea inspiratory time?...
      const Setting*  pApneaInspTime =
		SettingsMgr::GetSettingPtr(SettingId::APNEA_INSP_TIME);

      isValid = pApneaInspTime->isAcceptedValid();
    }
  }

  return(isValid);
}


//============== M E T H O D   D E S C R I P T I O N =================
// Method:  isAdjustedValid()  [const, virtual]
//
// Interface-Description
//  Is the adjusted value of this setting valid as determined by its
//  dependencies?
//---------------------------------------------------------------------
// Implementation-Description
//  The setting determines, and returns its validity.
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

Boolean
ApneaPeakInspFlowSetting::isAdjustedValid(void)
{
  CALL_TRACE("isAdjustedValid()");

  Boolean  isValid = BoundedSetting::isAdjustedValid();

  if (isValid)
  {
    if (getApplicability(Notification::ADJUSTED) !=
					    Applicability::INAPPLICABLE)
    {
      // is the the adjusted apnea peak insp. flow value consistent with the
      // adjusted apnea inspiratory time?...
      Setting*  pApneaInspTime =
			SettingsMgr::GetSettingPtr(SettingId::APNEA_INSP_TIME);

      isValid = pApneaInspTime->isAdjustedValid();
    }
  }

  return(isValid);
}

#endif // defined(SIGMA_DEVELOPMENT)


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//	      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ApneaPeakInspFlowSetting::SoftFault(const SoftFaultID  softFaultID,
				    const Uint32       lineNumber,
				    const char*        pFileName,
				    const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
			  APNEA_PEAK_INSP_FLOW_SETTING, lineNumber,
			  pFileName, pPredicate);
}


//====================================================================
//
//  Protected Methods...
//
//====================================================================

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  updateConstraints_()  [virtual]
//
//@ Interface-Description
//  Update the dynamic bounds of this setting.  This method sets this
//  setting's maximum constraint to the most restrictive of this setting's
//  upper bounds, and, correspondingly, set this setting's minimum
//  constraint to its most restrictive lower bound.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Determine the minimum upper bound, and set the upper limit to it.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
ApneaPeakInspFlowSetting::updateConstraints_(void)
{
  CALL_TRACE("updateConstraints_()");

  BoundedRange::ConstraintInfo  constraintInfo;

  //-------------------------------------------------------------------
  // update maximum constraint...
  //-------------------------------------------------------------------

  constraintInfo.id     = ::APNEA_PEAK_FLOW_MAX_BASED_CCT_TYPE_ID;
  constraintInfo.value  = getAbsoluteMaxValue_();
  constraintInfo.isSoft = FALSE;

  getBoundedRange_().updateMaxConstraint(constraintInfo);

  //-------------------------------------------------------------------
  // update minimum constraint...
  //-------------------------------------------------------------------

  constraintInfo.id     = ::APNEA_PEAK_FLOW_MIN_BASED_CCT_TYPE_ID;
  constraintInfo.value  = getAbsoluteMinValue_();
  constraintInfo.isSoft = FALSE;

  getBoundedRange_().updateMinConstraint(constraintInfo);
}  // $[TI1]


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method: calcBasedOnSetting_(basedOnSettingId, warpDirection, basedOnCategory)  [const]
//
//@ Interface-Description
//  This method is responsible for calculating, and returning, a new
//  "adjusted" value for this setting, based on the "adjusted" value
//  of the dependent setting given by 'basedOnSettingId'.  This method will
//  calculate values for each of this setting's dependent settings,
//  based on values in the Adjusted Context.
//---------------------------------------------------------------------
//@ Implementation-Description
//  All new-patient calculations are to be based on the apnea VCV setting
//  values.
//---------------------------------------------------------------------
//@ PreCondition
//  (basedOnSettingId == SettingId::APNEA_EXP_TIME  ||
//   basedOnSettingId == SettingId::APNEA_IE_RATIO  ||
//   basedOnSettingId == SettingId::APNEA_INSP_TIME)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

BoundedValue
ApneaPeakInspFlowSetting::calcBasedOnSetting_(
			  const SettingId::SettingIdType basedOnSettingId,
			  const BoundedRange::WarpDir    warpDirection,
			  const BasedOnCategory          basedOnCategory
					     ) const
{
  CALL_TRACE("calcBasedOnSetting_(basedOnSettingId, warpDirection, basedOnCategory)");

  BoundedValue  apneaInspTime;
  
  // determine a value for 'apneaInspTime', based on which timing
  // parameter is forcing this call...
  switch (basedOnSettingId)
  {
  case SettingId::APNEA_EXP_TIME :
    {   // $[TI1] -- calculate an apnea insp time from apnea exp time...
      const Setting*  pApneaExpTime =
		    SettingsMgr::GetSettingPtr(SettingId::APNEA_EXP_TIME);

      const Real32  APNEA_EXP_TIME_VALUE =
		      BoundedValue(pApneaExpTime->getAdjustedValue()).value;
      const Real32  APNEA_BREATH_PERIOD_VALUE =
		  ApneaRespRateSetting::GetApneaBreathPeriod(basedOnCategory);

      apneaInspTime.value = (APNEA_BREATH_PERIOD_VALUE - APNEA_EXP_TIME_VALUE);
    }
    break;
  case SettingId::APNEA_IE_RATIO :
    {   // $[TI2] -- calculate an apnea insp time from apnea I:E ratio...
      const Setting*  pApneaIeRatio =
		    SettingsMgr::GetSettingPtr(SettingId::APNEA_IE_RATIO);

      const Real32  APNEA_IE_RATIO_VALUE =
		      BoundedValue(pApneaIeRatio->getAdjustedValue()).value;
      const Real32  APNEA_BREATH_PERIOD_VALUE =
		  ApneaRespRateSetting::GetApneaBreathPeriod(basedOnCategory);

      Real32  eRatioValue;
      Real32  iRatioValue;

      if (APNEA_IE_RATIO_VALUE < 0.0f)
      {   // $[TI2.1]
	// The apnea I:E ratio value is negative, therefore we have 1:xx with
	// "xx" the expiratory portion of the ratio...
	iRatioValue = 1.0f;
	eRatioValue = -(APNEA_IE_RATIO_VALUE);
      }
      else
      {   // $[TI2.2]
	// The apnea I:E ratio value is positive, therefore we have xx:1 with
	// "xx" the inspiratory portion of the ratio...
	iRatioValue = APNEA_IE_RATIO_VALUE;
	eRatioValue = 1.0f;
      }

      // calculate the apnea insp time from the apnea I:E ratio...
      apneaInspTime.value = (iRatioValue * APNEA_BREATH_PERIOD_VALUE) /
				   (iRatioValue + eRatioValue);

      BoundedSetting*  pApneaInspTime =
                SettingsMgr::GetBoundedSettingPtr(SettingId::APNEA_INSP_TIME);

      // because apnea inspiratory time has a "coarse" resolution in VCV,
      // the "raw" apnea insp time value must be rounded to a resolution
      // boundary, before using the value in the calculations, below...

      BoundedRange::WarpDir actualWarpDirection;

      // the warping of inspiratory time must be in the opposite direction
      switch (warpDirection)
      {
      case BoundedRange::WARP_NEAREST :		// $[TI2.3]
	actualWarpDirection = BoundedRange::WARP_NEAREST;
	break;
      case BoundedRange::WARP_UP :		// $[TI2.4]
	actualWarpDirection = BoundedRange::WARP_DOWN;
	break;
      case BoundedRange::WARP_DOWN :		// $[TI2.5]
	actualWarpDirection = BoundedRange::WARP_UP;
	break;
      default:
	AUX_CLASS_ASSERTION_FAILURE(warpDirection);
	break;
      }

      pApneaInspTime->warpToRange(apneaInspTime, actualWarpDirection, FALSE);
    }
    break;
  case SettingId::APNEA_INSP_TIME :
    {   // $[TI3] -- get the "adjusted" apnea insp time value...
      const Setting*  pApneaInspTime =
		    SettingsMgr::GetSettingPtr(SettingId::APNEA_INSP_TIME);

      apneaInspTime.value =
		      BoundedValue(pApneaInspTime->getAdjustedValue()).value;
    }
    break;
  default :
    // unexpected 'basedOnSettingId' value...
    AUX_CLASS_ASSERTION_FAILURE(basedOnSettingId);
    break;
  };

  // get pointers to each of the apnea VCV parameters...
  const Setting*  pApneaFlowPattern =
		    SettingsMgr::GetSettingPtr(SettingId::APNEA_FLOW_PATTERN);
  const Setting*  pApneaPlateauTime =
		    SettingsMgr::GetSettingPtr(SettingId::APNEA_PLATEAU_TIME);
  const Setting*  pApneaTidalVolume =
		    SettingsMgr::GetSettingPtr(SettingId::APNEA_TIDAL_VOLUME);

  const DiscreteValue  APNEA_FLOW_PATTERN_VALUE =
  					pApneaFlowPattern->getAdjustedValue();

  const Real32  APNEA_PLATEAU_TIME_VALUE =
		    BoundedValue(pApneaPlateauTime->getAdjustedValue()).value;
  const Real32  APNEA_TIDAL_VOLUME_VALUE =
		    BoundedValue(pApneaTidalVolume->getAdjustedValue()).value;

  BoundedValue  apneaPeakInspFlow;

  switch (APNEA_FLOW_PATTERN_VALUE)
  {
  case FlowPatternValue::SQUARE_FLOW_PATTERN :	// $[TI4] 
    {
      static const Real32  FACTOR_ = (60000.0f * 0.001f);

      apneaPeakInspFlow.value = ((FACTOR_ * APNEA_TIDAL_VOLUME_VALUE) /
			    (apneaInspTime.value - APNEA_PLATEAU_TIME_VALUE));
    }
    break;
 
  case FlowPatternValue::RAMP_FLOW_PATTERN :	// $[TI5]
    {
      static const Real32  FACTOR_ = (60000.0f * 0.001f * 2.0f);

      const Real32  PRELIM_VALUE = ((FACTOR_ * APNEA_TIDAL_VOLUME_VALUE) / 
			    (apneaInspTime.value - APNEA_PLATEAU_TIME_VALUE));

      // The formula above is incomplete, the full formula would include
      // the subtraction of apnea minimum inspiratory flow from the entire
      // argument.  Apnea minimum inspiratory flow is purely based on value of
      // apnea peak inspiratory flow -- we have a "chicken and egg" problem.
      // Therefore, to solve this problem apnea peak inspiratory flow is
      // calculated WITHOUT apnea minimum inspiratory flow (result held in
      // 'PRELIM_VALUE'), and the apnea minimum inspiratory flow is determined
      // by using the minimum of the two apnea peak inspiratory flow values that
      // would result from the two possible apnea minimum inspiratory flow
      // values.
      // (NOTE:  apneaMinInspFlow = MAX(0.8, (apneaPeakInspFlow * 0.10)).)...
      apneaPeakInspFlow.value = MIN_VALUE((PRELIM_VALUE - 0.8f),   // $[TI5.1]
					  (PRELIM_VALUE / 1.1f));  // $[TI5.2]
    }
    break;
  
  default :
    AUX_CLASS_ASSERTION_FAILURE(APNEA_FLOW_PATTERN_VALUE);
    break;
  };

  // place apnea peak inspiratory flow on a "click" boundary...
  getBoundedRange().warpValue(apneaPeakInspFlow, warpDirection, FALSE);

  return(apneaPeakInspFlow);
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getAbsoluteMinValue_()  [const, virtual]
//
//@ Interface-Description
//  Return this setting's absolute minimum value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This is a virtual method that is overridden to support this setting's
//  dynamic minimum constraint.
//
//  $[02083] The setting's range ...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Real32
ApneaPeakInspFlowSetting::getAbsoluteMinValue_(void) const
{
  CALL_TRACE("getAbsoluteMinValue_()");

  Real32  absoluteMinValue;

  const Setting*  pPatientCctType =
		    SettingsMgr::GetSettingPtr(SettingId::PATIENT_CCT_TYPE);

  const DiscreteValue  PATIENT_CCT_TYPE_VALUE =
					pPatientCctType->getAdjustedValue();

  switch (PATIENT_CCT_TYPE_VALUE)
  {
  case PatientCctTypeValue::NEONATAL_CIRCUIT :		// $[TI1]
    absoluteMinValue = 1.0f;
    break;
  case PatientCctTypeValue::PEDIATRIC_CIRCUIT :
  case PatientCctTypeValue::ADULT_CIRCUIT :		// $[TI2]
    absoluteMinValue = 3.0f;
    break;
  default :
    // unexpected patient circuit type value...
    AUX_CLASS_ASSERTION_FAILURE(PATIENT_CCT_TYPE_VALUE);
    break;
  }

  return(absoluteMinValue);
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getAbsoluteMaxValue_()  [const, virtual]
//
//@ Interface-Description
//  Return this setting's absolute maximum value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This is a virtual method that is overridden to support this setting's
//  dynamic maximum constraint.
//
//  $[02083] The setting's range ...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Real32
ApneaPeakInspFlowSetting::getAbsoluteMaxValue_(void) const
{
  CALL_TRACE("getAbsoluteMaxValue_()");

  Real32  absoluteMaxValue;

  const Setting*  pPatientCctType =
		    SettingsMgr::GetSettingPtr(SettingId::PATIENT_CCT_TYPE);

  const DiscreteValue  PATIENT_CCT_TYPE_VALUE =
					pPatientCctType->getAdjustedValue();

  switch (PATIENT_CCT_TYPE_VALUE)
  {
  case PatientCctTypeValue::NEONATAL_CIRCUIT :		// $[TI1]
    absoluteMaxValue = 30.0f;
    break;
  case PatientCctTypeValue::PEDIATRIC_CIRCUIT :		// $[TI2]
    absoluteMaxValue = 60.0f;
    break;
  case PatientCctTypeValue::ADULT_CIRCUIT :		// $[TI3]
    absoluteMaxValue = 150.0f;
    break;
  default :
    // unexpected patient circuit type value...
    AUX_CLASS_ASSERTION_FAILURE(PATIENT_CCT_TYPE_VALUE);
    break;
  }

  return(absoluteMaxValue);
}
