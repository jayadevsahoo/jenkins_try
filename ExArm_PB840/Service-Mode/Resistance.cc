#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class:  Resistance - Calculate and store tubing resistance
//---------------------------------------------------------------------
//@ Interface-Description
//	This class is used to calculate and store circuit resistance
//	slope and offset.  The insp and exp limb resistances are measured
//	and stored during SST, and are used to estimate the pressure at
//	the patient wye during ventilation.
//	There is no limitation on the number of objects of this class
//	that may be instantiated.
//	NOTE: as a Service Mode utility class, there is no direct SRS
//	traceability.
//---------------------------------------------------------------------
//@ Rationale
//	This class is required to encapsulate circuit resistance
//	parameters and methods.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//	Default and copy constructors are defined.
//
//	The resistance slope and offset values are private data members.
//	Access methods are provided to store and retrieve these values:
//		getSlope() and setSlope(), and
//		getOffset() and setOffset().
//
//	The method calculateDeltaP() is used to calculate the pressure
//	drop across a circuit limb based on previously stored slope and
//	offset values and a passed-in flow value.
//
//	The - and = operators are overloaded to allow resistance calculations
//	by the calling function.
//---------------------------------------------------------------------
//@ Fault-Handling
//	n/a
//---------------------------------------------------------------------
//@ Restrictions
//	none
//---------------------------------------------------------------------
//@ Invariants
//	none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/Resistance.ccv   25.0.4.0   19 Nov 2013 14:23:42   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006  By: syw    Date: 02-Feb-2000     DR Number: 5327
//  Project:  NeoMode
//  Description:
//      Incorporation of NeoMode Project requirements.
//
//  Revision: 005  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 004  By:  quf    Date: 03-Nov-1997    DR Number: DCS 2602
//       Project:  Sigma (840)
//       Description:
//			Added data member resistanceValid_ and access methods
//			setResistanceValid() and isResistanceValid(),
//			updated default and copy constructors and operator=().
//
//  Revision: 003  By:  quf    Date: 18-Aug-1997    DR Number: DCS 2130
//       Project:  Sigma (840)
//       Description:
//            Fixed for unit test only.
//
//  Revision: 002  By:  quf    Date: 22-May-1997    DR Number: DCS 2131
//       Project:  Sigma (840)
//       Description:
//            Modification-Log fix.
//            Fixed global reference names (i.e. "r" changed to "R").
//
//  Revision: 001  By:  quf    Date:  18-Sep-1996    DR Number: none
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "Resistance.hh"

//@ Usage-Classes

// ***** COMMON INCLUDES *****
#ifdef SIGMA_UNIT_TEST
# include "Resistance_UT.hh"
#endif  // SIGMA_UNIT_TEST


// ***** BD-ONLY INCLUDES *****
#ifdef SIGMA_BD_CPU
# include "ValveRefs.hh"
# include "MainSensorRefs.hh"
# include "PressureSensor.hh"
# include "Psol.hh"
# include "FlowSensor.hh"

# include "PhasedInContextHandle.hh"
# include "PatientCctTypeValue.hh"

# include "Task.hh"

# include "SmRefs.hh"
# include "SmUtilities.hh"

# if defined(SIGMA_DEVELOPMENT)
#  include <stdio.h>
# endif

# ifdef SIGMA_UNIT_TEST
#  include "FakeIo.hh"
#  include "FakeControllers.hh"
#  include "Resistance_UT.hh"
# endif  // SIGMA_UNIT_TEST
#endif  // SIGMA_BD_CPU

#ifdef SIGMA_NEO_UNIT_TEST
extern Real32 FlowLimit ;
#endif  // SIGMA_NEO_UNIT_TEST

//@ End-Usage


//@ Code...

//@ Constant: ADULT_RI_SLOPE
// default resistance slope, adult circuit, insp limb
const Real32 ADULT_RI_SLOPE = 0.000808;

//@ Constant: ADULT_RI_OFFSET
// default resistance offset, adult circuit, insp limb
const Real32 ADULT_RI_OFFSET = 0.021417;

//@ Constant: CHARACTERIZATION_MIN_FLOW
// flow at which to start making resistance measurements as flow is ramped
const Real32 CHARACTERIZATION_MIN_FLOW  =  1.0;  // lpm

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  Resistance()  [Default constructor]
//
//@ Interface-Description
//	Constructor.  No arguments.  Initializes private data members.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Initialize slope_ and offset_ with adult insp resistance values.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Resistance::Resistance(void) :
		slope_(ADULT_RI_SLOPE),
		offset_(ADULT_RI_OFFSET),
		resistanceValid_(FALSE)
{
// $[TI1]
	CALL_TRACE("Resistance::Resistance()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  Resistance()  [Copy constructor]
//
//@ Interface-Description
//	Copy constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Equate the slope and offset values of the new object to those
//	of the existing object.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Resistance::Resistance(const Resistance& resistance2)
{
// $[TI2]
	CALL_TRACE("Resistance::Resistance(const Resistance& resistance2)");

	slope_ = resistance2.slope_;
	offset_ = resistance2.offset_;
	resistanceValid_ = resistance2.resistanceValid_;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ~Resistance()  [Destructor]
//
//@ Interface-Description
//	Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Resistance::~Resistance(void)
{
	CALL_TRACE("Resistance::~Resistance()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  operator-()
//
//@ Interface-Description
//	This method overloads the - operator, performing a subtraction
// 	of corresponding data members.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Create a temp resistance object.
//	temp object slope = lvalue slope - rvalue slope
//	temp object offset = lvalue offset - rvalue offset
//	Return the temp object.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Resistance
Resistance::operator- (const Resistance& resistance2)
{
// $[TI1]
	CALL_TRACE("Resistance::operator- (const Resistance& resistance2)");

	Resistance temp;

	temp.slope_ = slope_ - resistance2.slope_;
	temp.offset_ = offset_ - resistance2.offset_;

	return( temp);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  operator=()
//
//@ Interface-Description
//	This method overloads the = operator, performing an equate of
// 	corresponding data members.
//---------------------------------------------------------------------
//@ Implementation-Description
//	If lvalue object is not the same as the rvalue object, set
//	lvalue slope and offset equal to rvalue slope and offset, else
//	leave the lvalue object alone.
//	Return the lvalue object.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Resistance&
Resistance::operator= (const Resistance& resistance2)
{
	CALL_TRACE("Resistance::operator= (const Resistance& resistance2)");

	if (this != &resistance2)
	{
	// $[TI1]
		slope_ = resistance2.slope_;
		offset_ = resistance2.offset_;
		resistanceValid_ = resistance2.resistanceValid_;

#if defined( SIGMA_UNIT_TEST) && defined(SIGMA_BD_CPU)
		Resistance_UT::ObjectsEquated = TRUE;
#endif  // defined(SIGMA_UNIT_TEST) && defined(SIGMA_BD_CPU)
	}
	// $[TI2]
	// implied else: object is being equated to itself, so do nothing

	return( *this);
}


#ifdef SIGMA_BD_CPU

// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: measureCircuitResistance()
//
//@ Interface - Description
//	This BD-only method is used to measure the circuit resistance slope
//	and offset.  No arguments, returns the peak flow achieved during the
//	flow ramping.
//-----------------------------------------------------------------------
//@ Implementation - Description
//	Assumption: the calling function has set the exhalation valve and safety
//	valve appropriately.
//
//	Select the maximum test flow based on patient type (adult or ped).
//	Select the gas to be used.
//	Command PSOL to the starting value, make flow and pressure measurements
//	as PSOL is ramped to create a linear regression, starting when
//	the minimum flow is reached and ending when one of the following
//	criteria is reached:
//	1) flow reaches the max flow,
//	2) pressure reaches the max pressure, or
//	3) PSOL command reaches max value, whichever comes first.
//	While the flow is ramping, determine the peak flow achieved, and
//	after the loop is exited, return the peak flow.
//-----------------------------------------------------------------------
//@ PreCondition
//-----------------------------------------------------------------------
//@ PostCondition
//@ End - Method
//=======================================================================
Real32
Resistance::measureCircuitResistance(void)
{
	CALL_TRACE("Resistance::measureCircuitResistance(void)");
	
#ifndef SIGMA_UNIT_TEST
	Psol* pPsol =  &RO2Psol;
	FlowSensor* pInspFlowSensor = &RO2FlowSensor;
#else
	FakePsol* pPsol = &RO2Psol;
	FakeSensor* pInspFlowSensor = &RO2FlowSensor;
#endif  // SIGMA_UNIT_TEST

	// get current circuit type from Settings-Validation

	const DiscreteValue  CIRCUIT_TYPE =
		PhasedInContextHandle::GetDiscreteValue(SettingId::PATIENT_CCT_TYPE);
	
	Real32  flowLimit = PatientCctTypeValue::TOTAL_CIRCUIT_TYPES;	

	// set resistance characterization flow limit
	switch (CIRCUIT_TYPE)
	{
		case PatientCctTypeValue::PEDIATRIC_CIRCUIT :
			// $[TI1]
			// pediatric circuit has lower flow limit due to
			// loud whistling at wye during insp limb
			// characterization
			flowLimit = 100.0;  // lpm
			break;
			
		case PatientCctTypeValue::ADULT_CIRCUIT :
			// $[TI2]
			// adult circuit has higher flow limit
			flowLimit = 200.0;  // lpm
			break;
			
		case PatientCctTypeValue::NEONATAL_CIRCUIT :
			// $[TI6]
			// neonatal circuit flow limit
			flowLimit = 40.0;  // lpm
			break;
			
		default :
			// unexpected circuit type value...
			AUX_CLASS_ASSERTION_FAILURE(CIRCUIT_TYPE);
			break;
	}

#ifdef SIGMA_UNIT_TEST
	Resistance_UT::FlowLimit = flowLimit;
#endif  // SIGMA_UNIT_TEST

#ifdef SIGMA_NEO_UNIT_TEST
	FlowLimit = flowLimit ;
#endif  // SIGMA_NEO_UNIT_TEST

	// select a gas
	if (RSmUtilities.selectGasType() == AIR)
	{
	// $[TI3]
		pPsol = &RAirPsol;
		pInspFlowSensor = &RAirFlowSensor;
	}
	// $[TI4]
	// implied else: default to O2 flow sensor & psol

	// start count from just under the liftoff count
	const Int16 INITIAL_COUNT = pPsol->getLiftoff() - 100;

	// increment psol count at 1 count/5 msecs and adjust
	// for actual service mode cycle time
	const Int16 COUNT_INCREMENT = (Int16)(SM_CYCLE_TIME/CYCLE_TIME_MS);  // msec

	Real32 inspFlow = 0;
	Real32 peakInspFlow = 0;
	Real32 inspPressure = 0;
	Real32 exhPressure;
	Real32 deltaPressure;
	Int16 numSamples = 0;

#ifdef SIGMA_DEBUG
	Real32 initialInspFlow;
	Real32 initialInspPressure;
	Real32 initialExhPressure;
#endif  // SIGMA_DEBUG

	// linear regression sums
	Real32 sumX = 0;  // sum of flow
	Real32 sumY = 0;  // sum of (deltaPressure/flow)
	Real32 sumXY = 0; // sum of deltaPressure
	Real32 sumX2 = 0; // sum of (flow^2)

	// ramp psol from just below liftoff
	for (Int16 count = INITIAL_COUNT;
		count <= MAX_COUNT_VALUE && inspFlow <= flowLimit && inspPressure <= 100;
		count += COUNT_INCREMENT)
	{
	// $[TI5]
		// update psol count
		pPsol->updatePsol(count);

#ifndef SIGMA_UNIT_TEST
		// wait before measuring
		// Don't delay here during unit testing for greater speed
		Task::Delay(0, SM_CYCLE_TIME);
#endif  // SIGMA_UNIT_TEST

		// take flow and pressure samples
		inspFlow = pInspFlowSensor->getValue();
		inspPressure = RInspPressureSensor.getValue();
		exhPressure = RExhPressureSensor.getValue();


#ifdef SIGMA_DEBUG
		if (count == INITIAL_COUNT)
		{
			initialInspFlow = inspFlow;
			initialInspPressure = inspPressure;
			initialExhPressure = exhPressure;
		}			
#endif  // SIGMA_DEBUG

		// ignore samples until flow reaches min flow
		if (inspFlow > CHARACTERIZATION_MIN_FLOW)
		{
		// $[TI5.1]
			deltaPressure = inspPressure - exhPressure;

			// update linear regression values
			sumX += inspFlow;
			sumY += deltaPressure/inspFlow;
			sumXY += deltaPressure;
			sumX2 += inspFlow * inspFlow;
			numSamples++;

			// get peak flow
			if (peakInspFlow < inspFlow)
			{
			// $[TI5.1.1]
				peakInspFlow = inspFlow;
			}		
			// $[TI5.1.2]
			// implied else: last stored peak flow is greater than or
			// equal to the current insp flow
		}
		// $[TI5.2]
		// implied else: didn't reach initial flow yet

	}
	// end for

	// turn off psol
	pPsol->updatePsol(0);

#ifdef SIGMA_DEBUG
	printf("initial insp flow = %f\n", initialInspFlow);
	printf("initial insp pressure = %f\n", initialInspPressure);
	printf("initial exh pressure = %f\n", initialExhPressure);
#endif  // SIGMA_DEBUG

	// perform linear regression to compute slope and offset
	Real32 xBar = sumX / numSamples;
	Real32 yBar = sumY / numSamples;

	slope_ = (sumXY - numSamples * xBar * yBar) /
				   (sumX2 - numSamples * xBar * xBar);
	offset_ = yBar - slope_ * xBar;

#if defined(SIGMA_DEBUG)
	printf("Peak flow = %f\n", peakInspFlow);
#endif  // defined(SIGMA_DEBUG)

	return(peakInspFlow);
}


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                      [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//      'lineNumber' are essential pieces of information.  The 'pFileName'
//      and 'pPredicate' strings may be defaulted in the macro to reduce
//      code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
Resistance::SoftFault(const SoftFaultID  softFaultID,
			  const Uint32       lineNumber,
			  const char*        pFileName,
			  const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

	FaultHandler::SoftFault(softFaultID, SERVICE_MODE,
		        RESISTANCE, lineNumber,pFileName, pPredicate);
}

#endif  // SIGMA_BD_CPU


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//=====================================================================
//
//  Private Methods...
//
//=====================================================================
