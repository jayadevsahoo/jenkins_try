#include "UILogger.h"
#include "Clogger.h"
#include "STDLogger.h"

#include<stdio.h>

UILogger  clUILogger;
STDLogger clSTDLogger;

STDLogger clSTDFileLogger("LogFile.txt");


int main(){
    
    g_GlobalLogger.set(&clUILogger);

    printf("Disable LM_ERROR\n");
    clUILogger.disable(LM_ERROR);
    LOG_MESSAGE(LM_ERROR, "THIS IS AN ERROR" ); 
    LOG_MESSAGE(LM_WARNING, "THIS IS A WARNING"); 

    printf("\nEnable LM_ERROR\n");
    clUILogger.enable(LM_ERROR);
    LOG_MESSAGE(LM_ERROR, "THIS IS AN ERROR" ); 
    LOG_MESSAGE(LM_WARNING, "THIS IS A WARNING"); 


    printf("\nSwitch Logger\n");
    g_GlobalLogger.set(&clSTDLogger);

    LOG_MESSAGE(LM_ERROR, "THIS IS AN ERROR" ); 
    LOG_MESSAGE(LM_WARNING, "THIS IS A WARNING"); 


    printf("\nSwitch Logger\n");
    g_GlobalLogger.set(&clSTDFileLogger);

    LOG_MESSAGE(LM_ERROR, "THIS IS AN ERROR" ); 
    LOG_MESSAGE(LM_WARNING, "THIS IS A WARNING"); 

}
