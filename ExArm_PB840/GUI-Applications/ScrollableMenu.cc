#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: ScrollableMenu - A menu class providing a scrollable list of
//							selectable menu items
//---------------------------------------------------------------------
//@ Interface-Description
// ScrollableMenu is a GuiTable used to display a scrollable list of
// menu items. This class is normally instantiated and controlled by the
// ScrollableMenuSettingButton class to provide a pop-up menu of setting
// selections when the user presses the setting button. The user can
// scroll through this list using the off-screen knob to select a
// desired setting value. This class provides enhanced menu formatting
// and performance characteristics required for selecting Trending
// parameters, but may be used for displaying a selection menu for any
// setting value.
// 
// The GuiTable base class provides most of the methods for formatting
// the table and scrolling through the rows of data. The ScrollableMenu
// class defines the virtual method refreshCell_() to set up and format
// the individual LogCells according to the specific display
// requirements of the scrollable menu.
// 
// On construction the ScrollableMenu class is told how many rows and
// columns will be in the table that it displays. It is also passed a
// a LogTarget object. ScrollableMenu retrieves the textual content of
// any cell in the table via this LogTarget. The textual content can be
// specified in a number of different ways, either as specific cheap
// text or generic text that is auto-formatted within ScrollableMenu,
// thus relieving the application of some of the burden of text
// formatting.
//
// To begin displaying a scrollable log the minimum that must be done
// is: construct the ScrollableMenu, call setColumnInfo() once for each
// column of the ScrollableMenu, call setEntryInfo() to specify the
// number of entries in the log and call activate() before it will be
// displayed.
//
// If the log is scrolled by the user, the ScrollableMenu automatically
// retrieves the contents of any newly displayed rows by calling
// LogTarget's getLogEntryColumn() method. 
//---------------------------------------------------------------------
//@ Rationale
// Encapsulates the table cell formatting for scrollable menus.
//---------------------------------------------------------------------
//@ Implementation-Description 
// The client constructs a ScrollableMenu by specifying a LogTarget,
// the number of rows, columns and row height of the table.
// ScrollableMenu passes these formatting options along to the base
// class constructor that controls the size and shape of the table.
// 
// ScrollableMenu is passed a LogTarget on construction. When
// refreshCell_ is called, it calls this LogTarget via the
// getLogEntryColumn() method to retrieve the contents of each cell.
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and
// pre-conditions to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
// None of the construction parameters can subsequently be changed after
// construction e.g. you cannot change the number of rows displayed by
// ScrollableMenu after construction.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/ScrollableMenu.ccv   25.0.4.0   19 Nov 2013 14:08:22   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By:  gdc	   Date:  14-Jun-2007    SCR Number: 6237
//  Project:  Trend
//  Description:
//		Initial version.
//=====================================================================

#include "ScrollableMenu.hh"
#include "TrendSelectValue.hh"
#include "TrendTimeScaleValue.hh"

//@ Usage-Classes
// TODO E600 removed
//#include "Ostream.hh"
#include "Setting.hh"
#include "SettingId.hh"
#include "SettingMenuItems.hh"
#include "TrendSelectMenuItems.hh"
#include "TrendTimeScaleMenuItems.hh"
//@ End-Usage

#pragma instantiate GuiTable<MAX_MENU_ROWS,MAX_MENU_COLS>

//=====================================================================
//
//		Static Data...
//
//=====================================================================

// Initialize static constants.
//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ScrollableMenu()  [Constructor]
//
//@ Interface-Description
// Constructs a ScrollableMenu.  Passed the following parameters:
// >Von
//  settingId			The setting ID associated with this menu's
//  					list of setting values.
// 
//  menuItems   		A LogTarget through which cell contents are
//                      retrieved.
// 
//  numDisplayRows		Number of rows displayed by the log.
//
//  numDisplayColumns	Number of columns displayed by the log.
// 
//  rowHeight			The height in pixels of a row (includes the one
//                      pixel of the horizontal dividing line between
//						rows).
// 
//  displayScrollbar_	Pass FALSE if you do not want ScrollableMenu to
//  					have a scrollbar.  Defaults to TRUE.
// 
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//  Initialize all private data members. Pass along the table size
//  parameters and scrollbar to the GuiTable base class.
//---------------------------------------------------------------------
//@ PreCondition
//	The Settings pointer (pSetting_) cannot be NULL.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

ScrollableMenu::ScrollableMenu( SettingId::SettingIdType settingId,
								LogTarget& menuItems, 
								Uint16 numDisplayRows,
								Uint16 numDisplayColumns, 
								Uint16 rowHeight,
								Boolean displayScrollbar)
:   GuiTable<MAX_MENU_ROWS,MAX_MENU_COLS>(numDisplayRows,
										  numDisplayColumns,
										  rowHeight,
										  scrollbar_, 
										  displayScrollbar),
	scrollbar_(             rowHeight * numDisplayRows - 1, numDisplayRows),
	pSetting_(              SettingsMgr::GetSettingPtr(settingId)),
	rLogTarget_(            menuItems)
{
    CLASS_PRE_CONDITION( pSetting_ != NULL);
	horizontalLineColor_ = Colors::MEDIUM_BLUE;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~ScrollableMenu()  [Destructor]
//
//@ Interface-Description
// ScrollableMenu destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

ScrollableMenu::~ScrollableMenu(void)
{
	// Do nothing
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetTrendSelectMenu	[static]
//
//@ Interface-Description
// Instantiates (one time) a Trend Select menu and returns its reference.
//---------------------------------------------------------------------
//@ Implementation-Description
// Construct a ScrollableMenu object for the Trend Select menu.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
ScrollableMenu& ScrollableMenu::GetTrendSelectMenu(void)
{
	static const Uint32 TREND_SELECT_NUM_ROWS_ = 7;
	static const Uint32 TREND_SELECT_NUM_COLUMNS_ = 1;
	static const Uint32 TREND_SELECT_ROW_HEIGHT_ = 22;
	static const Uint32 TREND_SELECT_SCROLLWIDTH_ = 78;
	static const Boolean TREND_SELECT_DISPLAY_SCROLLBAR_ = TRUE;

	static ScrollableMenu menu(SettingId::TREND_SELECT_1,
							   TrendSelectMenuItems::GetMenuItems(),
							   TREND_SELECT_NUM_ROWS_, 
							   TREND_SELECT_NUM_COLUMNS_, 
							   TREND_SELECT_ROW_HEIGHT_,
							   TREND_SELECT_DISPLAY_SCROLLBAR_);
 
	menu.setColumnInfo(0, L"", TREND_SELECT_SCROLLWIDTH_, 
					   CENTER, 0, TextFont::NORMAL, TextFont::TEN, TextFont::TEN);
	menu.setEntryInfo(0, TrendSelectValue::TOTAL_TREND_SELECT_VALUES);

	menu.activate();

	return menu;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetTrendSelectMenu	[static]
//
//@ Interface-Description
// Instantiates (one time) a Trend Select menu and returns its reference.
//---------------------------------------------------------------------
//@ Implementation-Description
// Construct a ScrollableMenu object for the Trend Select menu.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
ScrollableMenu& ScrollableMenu::GetTrendTimeScaleMenu(void)
{
	static const Uint32 TREND_TIME_SCALE_NUM_ROWS_ = 8;
	static const Uint32 TREND_TIME_SCALE_NUM_COLUMNS_ = 1;
	static const Uint32 TREND_TIME_SCALE_ROW_HEIGHT_ = 22;
	static const Uint32 TREND_TIME_SCALE_SCROLLWIDTH_ = 48;
	static const Boolean TREND_TIME_SCALE_DISPLAY_SCROLLBAR_ = FALSE;

	static ScrollableMenu menu(SettingId::TREND_TIME_SCALE,
							   TrendTimeScaleMenuItems::GetMenuItems(),
							   TREND_TIME_SCALE_NUM_ROWS_, 
							   TREND_TIME_SCALE_NUM_COLUMNS_, 
							   TREND_TIME_SCALE_ROW_HEIGHT_,
							   TREND_TIME_SCALE_DISPLAY_SCROLLBAR_);
 
	menu.setColumnInfo(0, L"", TREND_TIME_SCALE_SCROLLWIDTH_, 
					   CENTER, 0, TextFont::NORMAL, TextFont::TEN, TextFont::TEN);
	menu.setEntryInfo(0, TrendTimeScaleValue::TOTAL_TREND_TIME_SCALE_VALUES);

	menu.activate();

	return menu;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: refreshCell_ [virtual, private]
//
//@ Interface-Description
// Refreshes the contents of a single cell.  Passed the row and column
// number of the cell.
//---------------------------------------------------------------------
//@ Implementation-Description 
// Calculates the entry number passed to the LogTarget from the
// specified row number and the current firstDisplayedEntry_.
// 
// Applies formatting options to each cell based on several factors. If
// the cell is in the "selected row" and the entry is an enabled setting
// value then format the cell as black text on a white background. If
// the cell is in the selected row and the entry is not an enabled
// setting value then format the cell with light-blue text on a white
// background. For non-selected rows, format the cell with white text
// on a blue background for enabled setting values and light-blue on a
// blue background for disabled setting values.
// 
// Uses the LogTarget::getLogEntryColumn to retrieve the data contents
// of the cell and LogCell::setText to format the text and present it in
// the table cell. Uses Setting::isEnabledValue to determine if the
// entry is enabled or disabled.
//---------------------------------------------------------------------
//@ PreCondition
// The specified row and column must be in range of the displayed table.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void ScrollableMenu::refreshCell_(Uint16 row, Uint16 col)
{
	AUX_CLASS_ASSERTION(row < NUM_DISPLAY_ROWS_, row);
	AUX_CLASS_ASSERTION(col < NUM_DISPLAY_COLUMNS_, col);

	Boolean isCheapText = FALSE;
	StringId formatString = NULL_STRING_ID;
	StringId string2 = NULL_STRING_ID;
	StringId string3 = NULL_STRING_ID;
	StringId string1Message = NULL_STRING_ID;
	Uint16 entryNumber = firstDisplayedEntry_ + row;

	// First, check if cell is on a row that corresponds to a log entry
	if (entryNumber >= numEntries_)
	{
		// we should never get here in TrendingLog
		AUX_CLASS_ASSERTION_FAILURE(entryNumber);
	}
	else
	{
		if (selectedEntry_ == entryNumber)
		{
			// It is so highlight it.
			cells_[row][col].setFillColor(Colors::WHITE);

			if (pSetting_->isEnabledValue(entryNumber))
			{
				cells_[row][col].setContentsColor(Colors::BLACK);
			}
			else
			{
				cells_[row][col].setContentsColor(Colors::LIGHT_BLUE);
			}
		}
		else
		{
			// Not selected so render in normal color
			cells_[row][col].setFillColor(Colors::MEDIUM_BLUE);

			if (pSetting_->isEnabledValue(entryNumber))
			{
				cells_[row][col].setContentsColor(Colors::WHITE);
			}
			else
			{
				cells_[row][col].setContentsColor(Colors::LIGHT_BLUE);
			}
		}

		// Ask the SettingMenuItems for the contents of the menu's column
		rLogTarget_.getLogEntryColumn(entryNumber,col,isCheapText,formatString,
									  string2,string3,string1Message);

		// Since we use the same formatting string as the setting button, we must
		// sprintf the format string to fill in the the appropriate
		// formatting characters just like the DiscreteSettingButton does.

 
		wchar_t formatChar = TextFont::GetFontStyleChar(columnInfos_[col].fontStyle); 
		wchar_t string1[256];

		Int ascent = TextFont::GetMaxAscent(columnInfos_[col].oneLineFontSize, 
											columnInfos_[col].fontStyle);

		swprintf(string1, formatString,
				columnInfos_[col].oneLineFontSize, 
				ascent,
				formatChar);
 

		// A cheap text (formatted) string is now in string1.
		// We call LogCell::setText to set the text field with
		// isCheapText FALSE even though string1 is actually cheap text 
		// since we want the LogCell to justify (center) the field in
		// the LogCell. This works fine if the font size specified in 
		// cheap text matches the font size in the setText() call.
		cells_[row][col].setText(isCheapText,
								 string1, string2, string3,
								 columnInfos_[col].alignment,
								 columnInfos_[col].fontStyle,
								 columnInfos_[col].oneLineFontSize,
								 columnInfos_[col].margin,
								 string1Message);
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void ScrollableMenu::SoftFault(const SoftFaultID  softFaultID,
							   const Uint32       lineNumber,
							   const char*        pFileName,
							   const char*        pPredicate)  
{
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, SCROLLABLEMENU,
							lineNumber, pFileName, pPredicate);
}

