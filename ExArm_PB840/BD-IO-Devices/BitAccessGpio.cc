#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@  Class: BitAccessGpio - To manage the bit operations of an I/O addresses.
//---------------------------------------------------------------------
//@ Interface-Description
//		This class is derived from Register class.  It manages the i/o bits
//		of an i/o address.  Requests to set/clear a bit by a client are
//		updated and are serviced all at once at the end of the cycle.  Service
//		to allow latching of a bit is available.  A on/off counting scheme can be
//		enabled such that the final bit output will be the net count of on vs.
//		offs.
//---------------------------------------------------------------------
//@ Rationale
//      To manage the individual bits of a register.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Each status of a bit is maintained in a structure that contains the
//		current state, the number of net ON/OFF requests, the status of
//		whether to latch high or low, and the status of whether to use the on/off
//		counting scheme.
//---------------------------------------------------------------------
//@ Fault-Handling
//		n/a
//---------------------------------------------------------------------
//@ Restrictions
//      none
//---------------------------------------------------------------------
//@ Invariants
//      none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/BitAccessGpio.ccv   25.0.4.0   19 Nov 2013 13:53:58   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005   By: gdc    Date: 26-Jan-2009    SCR Number: 6407
//  Project:  840S
//  Description:
//      Modified to bring source into strict ARM compliance.
//
//  Revision: 004 By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//	Description:
//		Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 003  By: syw   Date:  27-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 002  By:  syw    Date: 23-Jun-1997    DR Number: DCS 2244
//       Project:  Sigma (R8027)
//       Description:
//			Added method to obtain the status of a bit in the register.
//
//  Revision: 001  By:  syw    Date: 08-Nov-1996    DR Number: INITIAL RELEASE
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================
#include "Sigma.hh"
#include "BitAccessGpio.hh"

#ifdef SIGMA_DEBUG

#include <stdio.h>
#include "RegisterRefs.hh"
#include "Ostream.hh"

#endif // SIGMA_DEBUG

//@ Usage-Classes
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BitAccessGpio()
//
//@ Interface-Description
//      Constructor.  This method has the register type, the I/O address,
//		and the initial image as arguments.  This method returns nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Calls the base class constructor with the arguments passed in.
//		The bit structure is initialized.  The state of each bit is
//		determined from initImage.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

BitAccessGpio::BitAccessGpio( const BitState initState)
{
	CALL_TRACE("BitAccessGpio::BitAccessGpio( const RegisterType regType, \
				Uint16 * const ioAddress, const Uint16 initImage)");

	for(int i=0; i<MAX_BITS; i++)
	{
		Latched[i] = FALSE ;                // Nothing latched to start
        pGpio[i] = 0;                       // Clear the GPIO pointers
	}

#ifdef SIGMA_BD_CPU
    // Init the BD digital inputs
    InitGpioEntry(DIN_NOAFS_ID);
    InitGpioEntry(DIN_ENET_INT);
    InitGpioEntry(DIN_ENET_PME);
    InitGpioEntry(DIN_UI_ACK);
    InitGpioEntry(DIN_UI_OK);
    InitGpioEntry(DIN_PB_OUT);
    InitGpioEntry(DIN_ACDC_ON);
    InitGpioEntry(DIN_FAN_ERR);
    InitGpioEntry(DIN_PWRGD_3V3);
    InitGpioEntry(DIN_DIN_PWRGD_5V0);
    InitGpioEntry(DIN_PWRGD_6V0);
    InitGpioEntry(DIN_DY1);
    InitGpioEntry(DIN_FLOW_SENSOR_CONNECTED);
    InitGpioEntry(DIN_Device_Alert);
    InitGpioEntry(DIN_ID3);
    InitGpioEntry(DIN_ID1);

    // Init the BD digital outputs
    InitGpioEntry(DOUT_BD_ACK, initState);
    InitGpioEntry(DOUT_LED4, initState);
    InitGpioEntry(DOUT_CRYPTO_PD, initState);
    InitGpioEntry(DOUT_AMB_LED, initState);
    InitGpioEntry(DOUT_RED_LED, initState);
    InitGpioEntry(DOUT_BUZZER, ON);                     // Keeps the buzzer off
    InitGpioEntry(DOUT_LED5, initState);
    InitGpioEntry(DOUT_LED6, initState);
    InitGpioEntry(DOUT_DIS_PB_LED, initState);
    InitGpioEntry(DOUT_NURSECALL, initState);
    InitGpioEntry(DOUT_KILL_PWR, ON);                   // Keeps power on
    InitGpioEntry(DOUT_ENET_CS, initState);
    InitGpioEntry(DOUT_EN_HEATER, initState);
    InitGpioEntry(DOUT_FAN_ON, initState);
    InitGpioEntry(DOUT_P2_SOLENOID, initState);
    InitGpioEntry(DOUT_CROSSOVER_SOLENOID, initState);
    InitGpioEntry(DOUT_SAFETY_SOLENOID, (BitAccessGpio::BitState)ON);
    InitGpioEntry(DOUT_WATCH_DOG, initState);
    InitGpioEntry(DOUT_WD_TIMER_SET, initState);
    InitGpioEntry(DOUT_MNFLD_SEL, initState);
    InitGpioEntry(DOUT_P1_SOLENOID, initState);
	
	// E600 IE PHASE OUT set to Zero at Init state
	InitGpioEntry(DOUT_IE_PHASE_OUT, (BitAccessGpio::BitState)OFF); 

	InitGpioEntry(GPIO_SPI1_OFF_BD, (BitAccessGpio::BitState)ON);
	InitGpioEntry(GPIO_SPI1_ON_BD_ENABLED, (BitAccessGpio::BitState)ON);
    InitGpioEntry(GPIO_SPI1_ON_BD, (BitAccessGpio::BitState)OFF);
#endif
}


void  BitAccessGpio::InitGpioEntry(const ApplicationDigitalInputs_t bit)
{
    pGpio[bit] = new Gpio_t(bit);

    CLASS_ASSERTION( pGpio[bit] != 0 );
}

void  BitAccessGpio::InitGpioEntry(const ApplicationDigitalOutputs_t bit, const BitState initState)
{
    pGpio[bit] = new Gpio_t(bit,(Uint8)initState);

    CLASS_ASSERTION( pGpio[bit] != 0 );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~BitAccessGpio()
//
//@ Interface-Description
//      Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//      none
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

BitAccessGpio::~BitAccessGpio( void)
{
	CALL_TRACE("BitAccessGpio::~BitAccessGpio( void)") ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: requestBitUpdate
//
//@ Interface-Description
//		This method has a bit number and the state of the bit requested
//		as arguments.  This method returns nothing.  The bit number has
//		0 as LSB.
//---------------------------------------------------------------------
//@ Implementation-Description
// 		The request is update if the bit is not in a latch state.  The count
//		is incremented if state is ON else it is decremented.
//---------------------------------------------------------------------
//@ PreCondition
//		bit < BITS_IN_BYTE
//		bit < BITS_IN_WORD
//		regType_ == Register::BYTE || regType_ == Register::WORD
// 		state == OFF || state == ON
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
BitAccessGpio::requestBitUpdate( const ApplicationDigitalOutputs_t bit, const BitState state)
{
	CALL_TRACE("BitAccessGpio::requestBitUpdate( const Uint16 bit, \
				const BitState state)") ;

    // don't try accessing the GPIO if not allocated
    if (pGpio[bit] == 0)
    {
        CLASS_ASSERTION( false);                        // Why did the app try to access an undefined bit??
        return;
    }

	// update request if not in latch state
	if (!Latched[bit])
	{
		pGpio[bit]->SetPendingState((Uint8)state);             // Set the pending state, it will get written on the next cycle
	}	// implied else $[TI2.2]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: enableLatch
//
//@ Interface-Description
//      This method has a GPIO ID and the state of the GPIO to latch
//      as arguments.  This method returns nothing.
//      Once a bit it latched, it can not be latched again with
//		any state.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The bit is set to latch to hi or low.
//---------------------------------------------------------------------
//@ PreCondition
//		bit < BITS_IN_BYTE
//		bit < BITS_IN_WORD
//		regType_ == Register::BYTE || regType_ == Register::WORD
// 		state == OFF || state == ON
//		bitInfo_.latchHigh == FALSE
//		bitInfo_.latchLow == FALSE
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
BitAccessGpio::enableLatch(const ApplicationDigitalOutputs_t bit, const BitState state)
{
	CALL_TRACE("BitAccessGpio::enableLatch( const Uint16 bit, \
				const BitState state)") ;

    Latched[bit] = TRUE ;

    // don't try accessing the GPIO if not allocated
    if (pGpio[bit] != 0)
    {
    	pGpio[bit]->SetPendingState((Uint8)state);             // Set the pending state, it will get written on the next cycle
    }
    else
    {
        CLASS_ASSERTION( false);                        // Why did the app try to access an undefined bit??
    }

}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: NewCycle
//
//@ Interface-Description
//		This method has no arguments and returns nothing.  This method is
//		called to write out the bit images to the hardware register.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The bit structure states are constructed to form a 16 bit image
//		and is written to the hardware address if a update is needed.
//		The bit is set high if count > 0 or the count is 0 and the state
//		is on or latch high when onOffRequestLogic is active.  If not
//		active then it is set high if the state is high or latch high.
//		Otherwise the bit is low.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
BitAccessGpio::newCycle( void)
{
	CALL_TRACE("BitAccessGpio::writeNewCycle( void)") ;

    int i;
    for(i=0; i<MAX_BITS; i++)
    {
        // don't try accessing the GPIO if not allocated
        if (pGpio[i] == 0)
        {
            continue;
        }

        // We let the Gpio_t determine if it should read or write.
        pGpio[i]->newCycle();
    }
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getBitState
//
//@ Interface-Description
//		This method has a mask of the register as an argument and returns
//		whether the bit is on or off.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Returns whether the bit is on or off corresponding to the bit mask
//		passed in.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

BitAccessGpio::BitState
BitAccessGpio::getBitState(const ApplicationDigitalInputs_t bit)
{
    CALL_TRACE("BitAccessGpio::getBitState(ApplicationDigitalInputs_t)") ;

    if (pGpio[bit] != 0)
    {
        return( (BitAccessGpio::BitState)pGpio[bit]->Get() );
    }
    else
    {
        CLASS_ASSERTION( false);                        // Why did the app try to access an undefined bit??
        return (BitAccessGpio::BitState)0;
    }
	
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getBitState
//
//@ Interface-Description
//		This method has a mask of the register as an argument and returns
//		whether the bit is on or off.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Returns whether the bit is on or off corresponding to the bit mask
//		passed in.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

BitAccessGpio::BitState
BitAccessGpio::getBitState(const ApplicationDigitalOutputs_t bit)
{
    CALL_TRACE("BitAccessGpio::getBitState(ApplicationDigitalOutputs_t)") ;

    if (pGpio[bit] != 0)
    {
        return( (BitAccessGpio::BitState)pGpio[bit]->Get() );
    }
    else
    {
        CLASS_ASSERTION( false);                        // Why did the app try to access an undefined bit??
        return (BitAccessGpio::BitState)0;
    }
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault()
//
//@ Interface-Description
//      This method takes four arguments: softFaultID, lineNumber, pFileName,
//      and pPredicate and is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
BitAccessGpio::SoftFault( const SoftFaultID  softFaultID,
                     const Uint32       lineNumber,
                     const char*        pFileName,
                     const char*        pPredicate)
{
    CALL_TRACE("SoftFault( softFaultID, lineNumber, pFileName, pPredicate)") ;

	FaultHandler::SoftFault( softFaultID, BD_IO_DEVICES, BITACCESSGPIO,
                             lineNumber, pFileName, pPredicate) ;
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//=====================================================================
//
//  Private Methods...
//
//=====================================================================
