#ifndef TrendFormatData_HH
#define TrendFormatData_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: TrendFormatData
//---------------------------------------------------------------------
//@ Version
// @(#) $Header::   /840/Baseline/GUI-Applications/vcssrc/TrendFormatData.hhv   10.7   08/17/07 10:11:04   pvcs  
//
//  @ Modification-Log
//
//  Revision: 001  By:  ksg    Date:  19-June-2007    SCR Number: 6237
//  Project:  Trend
//  Description: Initial version.
//
//====================================================================

#include "TrendEvents.hh"
#include "TrendSelectValue.hh"

class TrendFormatData 
{
public:
	static void FormatData(wchar_t* rtnBuffer, TrendSelectValue::TrendSelectValueId trendId, Real32 data);  
	static Int32 FormatEvent(TrendEvents::EventId eventId);
    static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);
  
private:
	TrendFormatData(void);
	~TrendFormatData(void);
    // these methods are purposely declared, but not implemented...
    TrendFormatData(const TrendFormatData&);				// not implemented...
    void   operator=(const TrendFormatData&);			// not implemented...
    void formatData_(wchar_t* rtnBuffer, TrendSelectValue::TrendSelectValueId trendId, Real32 data);  
	Int32 formatEvent_(TrendEvents::EventId eventId) const;
	static TrendFormatData& GetTrendFormatData(void);
	//@ Data-Member:  fractionalDigitArray_
	// Array containing information on the number of 
	// digits to display for each of the trendIds
	Uint8  fractionalDigitArray_[TrendSelectValue::TOTAL_TREND_SELECT_VALUES];
};
#endif // TrendFormatData_HH


 
	

