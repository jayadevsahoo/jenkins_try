#ifndef AsapInspTrigger_HH
#define AsapInspTrigger_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: AsapInspTrigger -  To trigger inspiration as soon as
//    possible.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/AsapInspTrigger.hhv   25.0.4.0   19 Nov 2013 13:59:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 004  By:  yyy    Date:  10-Nov-98    DR Number: 5263, 5264
//       Project:  Sigma (R8027)
//       Description:
//			Added new parameter: isInspiratoryPhase in setBiLevelModeChangeCondition()
//			to identify when bilevel transitions to other modes, the breath phase
//			is in inspiration or not.  Added new data member:isInspiratoryPhase_.
//			Added new method resetBiLevelModeChangeCondition() to reset the
//			biLevelNoticeWasReceived_ flag during relinquish control of Spont mode.
//
//  Revision: 003  By:  yyy    Date:  09-Oct-1998    DR Number: 
//       Project:  BiLevel
//       Description:
//             Initial bilevel version
//
//  Revision: 002  By:  iv    Date:  4-Mar-1996    DR Number:674
//       Project:  Sigma (R8027)
//       Description:
//             Remove #ifndef.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#  include "Sigma.hh"
#  include "Breath_Delivery.hh"


//@ Usage-Classes
#  include "BreathTrigger.hh"
#  include "BreathType.hh"

//@ End-Usage

class AsapInspTrigger : public BreathTrigger {
  public:
    AsapInspTrigger(void);
    virtual ~AsapInspTrigger(void);

    static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL, 
			  const char*       pPredicate = NULL);

	void setBiLevelModeChangeCondition(Int32 peepHighToLowTransitionTime,
									   Int32 startTimeOfSpontBreath,
									   Boolean isInspiratoryPhase,
									   Boolean spontAndModeChangeOccuredAtPeepLow);
	void resetBiLevelModeChangeCondition(void);
    virtual void enable(void);

  protected:
    virtual Boolean triggerCondition_(void);

  private:
    AsapInspTrigger(const AsapInspTrigger&);	// not implemented...
    void   operator=(const AsapInspTrigger&);	// not implemented...

    //@ Data-Member: peepHighToLowTransitionTime_
    // length of time when peep stays high
    Int32 peepHighToLowTransitionTime_;

    //@ Data-Member: spontAndModeChangeOccuredAtPeepLow_
    // Boolean flag indicates a spont breath and mode change occured at peep low
    Boolean spontAndModeChangeOccuredAtPeepLow_;

    //@ Data-Member: biLevelModeChangeHappened_
    // Flag indicating bilevel mode change occured
    Boolean biLevelModeChangeHappened_;

    //@ Data-Member: biLevelNoticeWasReceived_
    // Flag indicating bilevel scheduler notified this trigger about a mode
    Boolean biLevelNoticeWasReceived_;

    //@ Data-Member: biLevelBreathType_
    // Type of breath when bilevel mode change occures. Breath type in BiLevel
    // scheduler may not be the same as breath type in BreathRecord. This
    // happens for a spont breath during peep high. On a mode change, the
    // breathRecord records a mandatory type (ASSIST or CONTROL) but for this
    // trigger, spont breath must be considered.
    ::BreathType biLevelBreathType_;

    //@ Data-Member: isInspiratoryPhase_
    // Flag indicating if the current breath phase is an inspiratory phase when
    // bilevel scheduler notified this trigger about a mode change
    Boolean isInspiratoryPhase_;

};


#endif // AsapInspTrigger_HH 
