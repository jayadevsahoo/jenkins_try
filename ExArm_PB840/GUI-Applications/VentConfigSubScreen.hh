#ifndef VentConfigSubScreen_HH
#define VentConfigSubScreen_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: VentConfigSubScreen - The screen selected by pressing the
// Ventilator Configuration button on the Upper Other Screens Subscreen.
// Allows user to view the current ventilator hardware/software configuration.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/VentConfigSubScreen.hhv   25.0.4.0   19 Nov 2013 14:08:42   pvcs  $
//
//@ Modification-Log
//
//  Revision: 014  By:  mnr   Date:  25-Feb-2010    SCR Number: 6436
//  Project:  PROX4
//  Description:
//		Yet another major layout change to accomodate PROX config info.
//
//  Revision: 013 By:  mnr    Date:  28-Dec-2009    SCR Number: 6437
//  Project:  NEO
//  Description:
//		Major layout changes to accomodate new options.
//
//  Revision: 012  By:  gdc    Date:  18-Feb-2009    SCR Number: 6476
//  Project:  840S
//  Description:
//		Implemented NeoMode Update option.
//
//  Revision: 011  By:  gdc    Date:  10-Dec-2008    SCR Number: 6377
//  Project:  840S
//  Description:
//	Removed DELTA project single screen support.
//
//  Revision: 010   By: rhj    Date: 20-Oct-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//  Added Leak Comp label.
// 
//  Revision: 009  By: rhj    Date: 19-Mar-2007   DCS Number:  6363
//  Project:  RESPM
//  Description:
//	  Removed Trending label for SCR 6363
//
//  Revision: 010   By: rhj   Date:  24-Jun-2007    SCR Number: 6237
//  Project:  Trend
//  Description:
//    Added Trending Label
//
//  Revision: 009  By: rhj    Date: 19-Mar-2007   DCS Number:  6363
//  Project:  RESPM
//  Description:
//	  Removed Trending label for SCR 6363
//   
//  Revision: 008  By: btray    Date: 28-Aug-2000   DCS Number:  5753
//  Project:  Delta
//  Description:
//	Implemented Single Screen option.
//
//  Revision: 007  By: sah    Date: 17-Jul-2000   DCS Number:  5755
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      *  added display of VTPC option state
//
//  Revision: 006   By: sah   Date:  23-May-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//      PAV project-related changes:
//      *  added display of PAV option state
//
//  Revision: 005  By: btray    Date: 27-Jul-1999   DCS Number:  5327
//  Project:  NeoMode
//  Description:
//	Improvements made for display of data in Ventilator Configuration Subscreen. 
//
//  Revision: 004  By: syw      Date: 27-Oct-1998  DR Number: 5215
//    Project:  BiLevel
//    Description:
//		Changed configurationCodeLabel_ to ventilatorOptionsLabel_.
//
//  Revision: 003  By:  syw	   Date:  20-Aug-1998    DCS Number: 
//  Project:  Color
//  Description:
//		BiLevel Initial version.  Added configurationCodeLabel_.
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  mpm    Date:  07-NOV-95    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

//@ Usage-Classes
#include "SubScreen.hh"
#include "SubScreenTitleArea.hh"
#include "TextField.hh"
#include "GuiAppClassIds.hh"
#include "Bitmap.hh"
#include "Box.hh"
//@ End-Usage

class SubScreenArea;

class VentConfigSubScreen : public SubScreen
{
public:
	VentConfigSubScreen(SubScreenArea* pSubScreenArea);
	~VentConfigSubScreen(void);

	virtual void activate(void);
	virtual void deactivate(void);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:

private:
	// these methods are purposely declared, but not implemented...
	VentConfigSubScreen(void);								// not implemented...
	VentConfigSubScreen(const VentConfigSubScreen&);	// not implemented...
	void operator=(const VentConfigSubScreen&);				// not implemented...

	//@ Data-Member: titleArea_
	// The sub-screen's title at the top left of the screen
	SubScreenTitleArea titleArea_;

	//@ Data-Member: verticalDividingLine_
	// Line that splits the sub-screen vertically 
	Line verticalDividingLine_;

	//@ Data-Member: horizontalDividingLine_
	// Line that splits the sub-screen horizontically 
	Line horizontalDividingLine1_;

	//@ Data-Member: horizontalDividingLine_
	// Line that splits the sub-screen horizontically 
	Line horizontalDividingLine2_;

	//@ Data-Member: softwareOptionsLabel_;
	// The text field for displaying the label of the Software Options
	TextField softwareOptionsLabel_;

	//@ Data-Member: softwareOptionBiLevelLabel_;
	// The text field for displaying the label of the BiLevel Software Option
	TextField softwareOptionBiLevelLabel_;

	//@ Data-Member: softwareOptionTCLabel_;
	// The text field for displaying the label of the TC Software Option
	TextField softwareOptionTCLabel_;

	//@ Data-Member: softwareOptionNeoModeLabel_;
	// The text field for displaying the label of the NeoMode Software Option
	TextField softwareOptionNeoModeLabel_;

	//@ Data-Member: softwareOptionVtpcLabel_;
	// The text field for displaying the label of the VTPC Software Option
	TextField softwareOptionVtpcLabel_;

	//@ Data-Member: softwareOptionPavLabel_;
	// The text field for displaying the label of the PAV Software Option
	TextField softwareOptionPavLabel_;

	//@ Data-Member: softwareOptionRespMechLabel_;
	// The text field for displaying the label of the RespMech Software Option
	TextField softwareOptionRespMechLabel_;

	//@ Data-Member: softwareOptionTrendingLabel_;
	// The text field for displaying the label of the Trending Software Option
	TextField softwareOptionTrendingLabel_;

	//@ Data-Member: softwareOptionLeakCompLabel_;
	// The text field for displaying the label of the Leak Comp Software Option
	TextField softwareOptionLeakCompLabel_;

	//@ Data-Member: softwareOptionNeoModeUpdateLabel_;
	// The text field for displaying the label of the NeoUpdate Software Option
	TextField softwareOptionNeoModeUpdateLabel_;

	//@ Data-Member: softwareOptionNeoModeAdvancedLabel_;
	// The text field for displaying the label of the NeoAdvance Software Option
	TextField softwareOptionNeoModeAdvancedLabel_;

	//@ Data-Member: softwareOptionNeoModeLockoutLabel_;
	// The text field for displaying the label of the NeoLockout Software Option
	TextField softwareOptionNeoModeLockoutLabel_;

	//@ Data-Member: softwareOptionBiLevelBox_;
	// The box whose contents determine if the BiLevel Option is available 
	Box softwareOptionBiLevelBox_;

	//@ Data-Member: softwareOptionTCBox_;
	// The box whose contents determine if the TC Option is available 
	Box softwareOptionTCBox_;

	//@ Data-Member: softwareOptionNeoModeBox_;
	// The box whose contents determine if the NeoMode Option is available 
	Box softwareOptionNeoModeBox_;

	//@ Data-Member: softwareOptionVtpcBox_;
	// The box whose contents determine if the VTPC Option is available 
	Box softwareOptionVtpcBox_;

	//@ Data-Member: softwareOptionPavBox_;
	// The box whose contents determine if the PAV Option is available 
	Box softwareOptionPavBox_;

	//@ Data-Member: softwareOptionRespMechBox_;
	// The box whose contents determine if the RespMech Option is available 
	Box softwareOptionRespMechBox_;

	//@ Data-Member: softwareOptionTrendingBox_;
	// The box whose contents determine if the Trending Option is available 
	Box softwareOptionTrendingBox_;

	//@ Data-Member: softwareOptionLeakCompBox_;
	// The box whose contents determine if the Leak Comp Option is available 
	Box softwareOptionLeakCompBox_;

	//@ Data-Member: softwareOptionNeoModeUpdateBox_;
	// The box whose contents determine if the NeoUpdate Option is available 
	Box softwareOptionNeoModeUpdateBox_;

	//@ Data-Member: softwareOptionNeoModeAdvancedBox_;
	// The box whose contents determine if the NeoAdvance Option is available 
	Box softwareOptionNeoModeAdvancedBox_;

	//@ Data-Member: softwareOptionNeoModeLockoutBox_;
	// The box whose contents determine if the NeoLockout Option is available 
	Box softwareOptionNeoModeLockoutBox_;

	//@ Data-Member: guiSymbolBitmap_
	// The bitmap that depicts GUI symbol
	Bitmap guiSymbolBitmap_;

	//@ Data-Member: bdSymbolBitmap_
	// The bitmap that depicts BD symbol
	Bitmap bdSymbolBitmap_;

	//@ Data-Member: optionBiLevelEnabledBitmap_
	// The bitmap that depicts that the BiLevel option is enabled.
	Bitmap optionBiLevelEnabledBitmap_;

   	//@ Data-Member: optionTCEnabledBitmap_
	// The bitmap that depicts that the TC option is enabled.
	Bitmap optionTCEnabledBitmap_;

    //@ Data-Member: optionNeoModeEnabledBitmap_
	// The bitmap that depicts that the NeoMode option is enabled.
	Bitmap optionNeoModeEnabledBitmap_;

    //@ Data-Member: optionVtpcEnabledBitmap_
	// The bitmap that depicts that the VTPC option is enabled.
	Bitmap optionVtpcEnabledBitmap_;

   	//@ Data-Member: optionPavEnabledBitmap_
	// The bitmap that depicts that the PAV option is enabled.
	Bitmap optionPavEnabledBitmap_;

   	//@ Data-Member: optionRespMechEnabledBitmap_
	// The bitmap that depicts that the RespMech option is enabled.
	Bitmap optionRespMechEnabledBitmap_;

   	//@ Data-Member: optionTrendingEnabledBitmap_
	// The bitmap that depicts that the Trending option is enabled.
	Bitmap optionTrendingEnabledBitmap_;

   	//@ Data-Member: optionLeakCompEnabledBitmap_
	// The bitmap that depicts that the Leak Comp option is enabled.
	Bitmap optionLeakCompEnabledBitmap_;

    //@ Data-Member: optionNeoModeUpdateEnabledBitmap_
	// The bitmap that depicts that the NeoMode update option is enabled.
	Bitmap optionNeoModeUpdateEnabledBitmap_;

	//@ Data-Member: optionNeoModeAdvancedEnabledBitmap_
	// The bitmap that depicts that the NeoMode Advanced option is enabled.
	Bitmap optionNeoModeAdvancedEnabledBitmap_;

	//@ Data-Member: optionNeoModeLockoutEnabledBitmap_
	// The bitmap that depicts that the NeoMode Lockout option is enabled.
	Bitmap optionNeoModeLockoutEnabledBitmap_;

	//@ Data-Member: guiVersionLabel_
	// GUI version label
	TextField guiVersionLabel_;

	//@ Data-Member: guiPostPartlNumberLabel_
	// GUI Kernel Post Partl Number Label
	TextField guiPostPartlNumberLabel_;

	//@ Data-Member: bdVersionLabel_
	// BD version label
	TextField bdVersionLabel_;

	//@ Data-Member: bdPostPartlNumberLabel_
	// BD Kernel Post Partl Number Label
	TextField bdPostPartlNumberLabel_;

	//@ Data-Member: guiSerialNumberLabel_
	// GUI Serial Number Label
	TextField guiSerialNumberLabel_;

	//@ Data-Member: bdSerialNumberLabel_
	// BD Serial Number Label
	TextField bdSerialNumberLabel_;

	//@ Data-Member: compressorSerialNumberLabel_
	// compressor Serial Number Label
	TextField compressorSerialNumberLabel_;

	//@ Data-Member: proxConfigTitleLabel_
	// Prox Firmware Config Title Label
	TextField proxConfigTitleLabel_;

	//@ Data-Member: proxFirmwareRevLabel_
	// Prox Firmware Revision Label
	TextField proxFirmwareRevLabel_;

	//@ Data-Member: proxSerialNumLabel_
	// Prox Serial Number Label
	TextField proxSerialNumberLabel_;
};

#endif // VentConfigSubScreen_HH 
