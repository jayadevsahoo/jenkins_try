#pragma once

#include "SettingXmitData.hh"

class SettingsXaction
{
public:
	    //@ Type:  XactionMsg
    // Enumerator of queue message IDs.
    enum XactionMsg
    {
      XACTION_INITIATION_MSG = ::FIRST_APPLICATION_MSG
    };

	static void  Initiate(const SettingsXactionId xactionId);

	SettingsXaction(void);
	~SettingsXaction(void);

	static void  TaskDriver(void);


private:
	//@ Type:  XactionStatusId_
	enum XactionStatusId_
	{
		XACTION_SUCCESSFUL,
		OPEN_SOCKET_FAILURE,
		XMIT_FAILURE,
		RECV_FAILURE
	};

	static Boolean  BeginClientXaction_ (const SettingsXactionId xactionId);

	static void  ProcessFailedXmit_(const SOCKET xactionSocketId);
	static void  ProcessFailedRecv_(const SOCKET xactionSocketId);

	static inline Uint  CalcXactionBlockSize_(const Uint numXmitElems);

	static void     ReportXactionStatus_(
		const XactionStatusId_ xactionStatusId
		);

	static void  SetupQueueMsg_(
				    InterTaskMessage&       rQueueMsg,
				    const SettingsXactionId xactionState,
				    const Uint16            xactionInfo
				      );

    static SettingsXactionId  GetXactionState_(
    					const InterTaskMessage& queueMsg
						     );
    static Uint16             GetXactionInfo_(
    					const InterTaskMessage& queueMsg
						    );


	//@ Constant:  ZERO_ELEMS_BLOCK_SIZE_
	// Static constant that identifies the transaction block size when NO
	// elements are transmitted/received.
	static const Uint  ZERO_ELEMS_BLOCK_SIZE_;

	//@ Constant:  BLOCK_RECV_WAIT_TIME_
	// Static constant that identifies the amount of time (in milliseconds)
	// that a blocked receive will wait.
	static const Uint  BLOCK_RECV_WAIT_TIME_;
};

