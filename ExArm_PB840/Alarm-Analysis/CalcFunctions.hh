#ifndef CalcFunctions_HH
#define CalcFunctions_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================
//====================================================================
// Class: CalcFunctions - Alarm-Analysis class
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Alarm-Analysis/vcssrc/CalcFunctions.hhv   25.0.4.0   19 Nov 2013 13:51:20   pvcs  $
//
//@ Modification-Log

//  
//   Revision 001   By:   hct      Date: 01/01/95         DR Number:   n/a
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//  
//   Revision 002   By:   hct    Date: 12/16/97         DR Number:  2688
//      Project:   Sigma   (R8027)
//      Description:
//         Add hysteresis to LSP alarms.
//  
//   Revision 003   By:   sah      Date: 01/12/99         DR Number:   5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to non-inlined method.
//
//   Revision 004   By:   hct    Date: 08/27/99         DR Number:  5513
//      Project:   NewAlarms
//      Description:
//         Added new methods to model escalation changes in patient data alarms.
//  
//   Revision 005   By:   hct    Date: 08/11/00         DR Number:  5313
//      Project:   PAV
//      Description:
//         Added new methods for PAV alarms.
//  
//====================================================================

#include "Sigma.hh"
#include "Alarm_Analysis.hh"

//@ Usage-Classes
//@ End-Usage


class CalcFunctions 
{
  public:

    static Int32 Return1Event(void);
    static Int32 Return2Events(void);
    static Int32 Return3Events(void);
    static Int32 Return4Events(void);
    static Int32 Return5Events(void);
    static Int32 Return10Events(void);

    static Int32 Return1Sec(void);
    static Int32 Return3Sec(void);
    static Int32 Return5Sec(void);
    static Int32 Return10Sec(void);
    static Int32 Return20Sec(void);
    static Int32 Return30Sec(void);
    static Int32 Return40Sec(void);
    static Int32 Return45Sec(void);
    static Int32 Return75Sec(void);
    static Int32 Return90Sec(void);
    
    static Int32 Return2Min(void);
    static Int32 Return10Min(void);
    static Int32 Return15Min(void);
    
    static Int32 ReturnFourApneaIntervals(void);
    static Int32 ReturnTenApneaIntervals(void);
    static Int32 ReturnMax60or3ApneaInts(void);

    static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);
  
  protected:

  private:
    // these methods are purposely declared, but not implemented...
    CalcFunctions(void);                    // not implemented
    ~CalcFunctions(void);                   // not implemented

    CalcFunctions(const CalcFunctions&);    // not implemented...
    void   operator=(const CalcFunctions&); // not implemented...
};


#endif // CalcFunctions_HH 
