#include "stdafx.h"
//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: AlarmOperand - Pure virtual base class for OperatingGroup/Condition.
//---------------------------------------------------------------------
//@ Interface-Description
// AlarmOperand provides the data and functionality that is common to its
// derived classes, OperatingCondition and OperatingGroup.  This includes
// fields to indicate the state of the operand, when it changed state, whether
// or not reevaluation or reset is necessary, a list of parent operands and a
// name used to identify the operand.  Methods to access and update these
// fields are provided.
//---------------------------------------------------------------------
//@ Rationale
// The alarm handling strategy is organized around the goal of detecting and
// communicating root causes of alarms.  A single root problem can cause a
// number of secondary alarms to be detected.  Therefore, instead of
// annunciating all of these as separate alarms, as has commonly been done in
// the past, the Sigma Alarm Analysis subsystem does the detective work for the
// operator and annunciates the primary alarm and adds to that annunciation the
// secondary alarms.
//
// To accomplish this, and also to make the Alarm Analysis subsystem easily
// extensible, a hierarchy of operating parameters was developed.  This
// hierarchy is an inverted forest of interconnected trees.  The lowermost
// level of leaves is modeled through the OperatingCondition class.  It
// sometimes requires a number of simple conditions to detect an alarm or
// augment an alarm.  The OperatingGroup class collects these conditions (and
// other groups) through the use of the OperatingComponent class.  The
// OperatingGroup class includes an ordered list of OperatingComponents, each
// of which in turn points to a single OperatingCondition or OperatingGroup.
// The OperatingComponent includes two fields to determine how the states of
// the child AlarmOperand objects should be combined; the negate_ field acts as
// a NOT operator on the state of the child AlarmOperand and the logOp_ field
// indicates whether to AND or OR the state of the child AlarmOperand with the
// other children.
//
// To extend or change alarms, the initialization of the alarm hierarchy is all
// that needs to be changed.  The framework which processes the hierarchy
// doesn't change.
//---------------------------------------------------------------------
//@ Implementation-Description
// When an OperatingCondition undergoes a change or an expired timer indicates
// a need to examine an AlarmOperand, the Operating Parameter hierarchy is
// evaluated.  The AlarmOperand object's ancestor OperatingGroup objects are
// processed and marked to indicate the need to be evaluated.
// When all AlarmOperand ancestors have been marked, starting at the top level
// OperatingGroups, the marked OperatingGroup objects and OperatingCondition
// objects are evaluated.  If any OperatingGroup objects that augment or cause 
// an Alarm change state, the Alarm is marked to indicate it needs to be 
// evaluated.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//   none
//---------------------------------------------------------------------
//@ Invariants
//   none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Alarm-Analysis/vcssrc/AlarmOperand.ccv   25.0.4.0   19 Nov 2013 13:51:16   pvcs  $
//
//@ Modification-Log
//  
//   Revision 001   By:   hct      Date: 01/01/95         DR Number:   n/a
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//  
//   Revision 002   By:   hct      Date: 01/18/96         DR Number:   659
//      Project:   Sigma   (R8027)
//      Description:
//         Use OsTimeStamp for interval timing instead of TimeStamp.
//  
//   Revision 003   By:   sah      Date: 01/12/99         DR Number:   5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to non-inlined method.
//  
//   Revision 004   By:   gdc      Date: 01/04/00         DR Number:   5588
//      Project:   840 NeoMode
//      Description:
//         Using TimeStamp::GetRNullTimeStamp() instead of global 
//         reference.
//
//   Revision 005   By:   gdc      Date: 02/21/05         DR Number:   6144
//      Project:  NIV1
//      Description:
//         DCS 6144 - NIV1
//         Modifications to support NIV.
//         Added auxilliary information to assertion to aid debugging.
//
//   Revision 006   By:   rhj    Date: 05-March-2007      DR Number: 6359
//      Project:  RESPM
//      Description:
//         Resolve merge conflict for RM to Baseline 
//
//=====================================================================

#include "Sigma.hh"
#include "AlarmOperand.hh"

//@ Usage-Classes
//@ End-Usage

AlarmOperand* 
AlarmOperand::Operands_[MAX_NUM_OPERANDS];

//@ Code...

//=====================================================================
//
//      Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: AlarmOperand
//
//@ Interface-Description
//      Default constructor.
//
// >Von
//      n  Enumerated name of the AlarmOperand to be constructed.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      ((OPERAND_NAME_NULL < n) &&
//       (n < MAX_NUM_OPERANDS))
//@ End-Method
//=====================================================================
AlarmOperand::AlarmOperand(const OperandName n): name_(n)
{
  CALL_TRACE("AlarmOperand(n)");

  CLASS_PRE_CONDITION((OPERAND_NAME_NULL < n) && 
                      (n < MAX_NUM_OPERANDS));

  update_ = FALSE;
  reset_ = FALSE;
  intervalTime_.invalidate();
  whenOccurred_ = TimeStamp::GetRNullTimeStamp();
  isActive_ = FALSE;
  // $[TI1]
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: AlarmOperand
//
//@ Interface-Description
//      Copy constructor.
//
// >Von
//      rOp  Reference to the object to be copied from.  
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
AlarmOperand::AlarmOperand(const AlarmOperand& rOp): name_(rOp.name_)
{
  CALL_TRACE("AlarmOperand(rOp)");

  update_ = rOp.update_;
  reset_ = rOp.reset_;
  intervalTime_  = rOp.intervalTime_;
  whenOccurred_ = rOp.whenOccurred_;
  isActive_ = rOp.isActive_;
  parents_ = rOp.parents_;  
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~AlarmOperand 
//
//@ Interface-Description
//      Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
AlarmOperand::~AlarmOperand(void)
{
  CALL_TRACE("~AlarmOperand(void)");
}




//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Initialize
//
//@ Interface-Description
//      Method called during system initialization.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
AlarmOperand::Initialize(void)
{
  CALL_TRACE("Initialize()");
  
  for (int i = 0; i < MAX_NUM_OPERANDS; i++)
  {
     AlarmOperand::Operands_[i] = NULL;
  }
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: InitGroups
//
//@ Interface-Description
//      After all objects to be instantiated during system initialization are
//      created, method InitGroups is messaged to initialize the OperatingGroup
//      objects.  If an OperatingGroup has no parents, it is at the top of
//      the Operating Parameter Hierarchy and is marked accordingly.  For all
//      OperatingGroup objects which point to an object derived from
//      ViolationHistoryManager, the maximum number of violations the
//      ViolationHistoryManager tracks is initialized by this method.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
AlarmOperand::InitGroups(void) 
{
  CALL_TRACE("InitGroups(void) ");

  for (int i = 0; i < MAX_NUM_OPERANDS; i++)
  {
    AUX_CLASS_ASSERTION(AlarmOperand::Operands_[i] != NULL, i);

    // Initialize the topLevel data member
    AlarmOperand::Operands_[i]->initTopLevel_();

    // Initialize the maxNumViolations data member of the ViolationHistoryList
    AlarmOperand::Operands_[i]->initMaxViolations_();    
  }
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: AddOperand
//
//@ Interface-Description
//      During system initialization, after the AlarmOperand objects are
//      instantiated, a pointer (pOperand) to the AlarmOperand is put
//      on the static list of pointers to the AlarmOperand objects, Operands_.
//
// >Von
//      pOperand  Pointer to the AlarmOperand to be added to the class list of 
//                pointers to AlarmOperand ojbects.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      (pOperand != NULL)
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
AlarmOperand::AddOperand(AlarmOperand* pOperand)
{
  CALL_TRACE("AddOperand(pOperand)");
  CLASS_PRE_CONDITION(pOperand != NULL);

  AlarmOperand::Operands_[int(pOperand->getName())] = pOperand;
  // $[TI1]
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: evaluate
//
//@ Interface-Description
//      There are two modes of AlarmOperand evaluation: update and reset.
//      Update mode involves the receipt of OperatingParameterEventName
//      messages on the OperatingParameterEventQueue which are translated into 
//      OperandName enumerators or pointers to ViolationHistory objects.  The 
//      AlarmOperand objects named by the enumerators or pointed to indirectly 
//      by the ViolationHistory objects and all of their ancestors are marked
//      as needing to be evaluated.  Reset mode occurs when an Alarm causing 
//      Loss of Patient Data autoresets and all alarms whose 
//      detection algorithms are based on patient data must be reset.  When the 
//      user presses the Alarm Reset key, all active Non-Technical alarms must 
//      be reset.  Both are accomplished by evaluating the OperatingParameter
//      Hierarchy in reset mode.  For Patient Data Reset, the list of Alarm
//      objects is examined and for each Alarm which has attribute 
//      patientDataAlarm_ set to TRUE the OperandName enumerator naming the 
//      OperatingCondition which causes the Alarm is placed on the 
//      AlarmManager::OperandNameList_.  For Alarm Reset, the list of Alarm
//      objects is examined and for each active Non-Technical Alarm, the 
//      OperandName enumerator naming the OperatingCondition which actually 
//      causes the Alarm is placed on the AlarmManager::OperandNameList_.  
//      Operating Parameter Hierarchy processing sets attribute reset_ for all 
//      named OperatingConditions and all of their ancestors.  When the 
//      hierarchy is being evaluated, the isActive_ attribute of 
//      OperatingCondition objects with attribute reset_ set to TRUE are 
//      negated which leads to the Alarm they cause to become inactive.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method evaluates the state of the AlarmOperand by examining its
//      children, ViolationHistoryManager objects and/or Constraint objects.
//      In reset mode, AlarmOperand objects which cause alarms are reset so the 
//      Alarm object will be evaluated as inactive.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Boolean
AlarmOperand::evaluate(void) 
{
  CALL_TRACE("evaluate(void)");

  // If this AlarmOperand object's reset_ attribute is TRUE, the Operating
  // Parameter Hierarchy is being evaluated in reset mode.
  if (reset_) // $[TI1]
  {
    reset_ = FALSE;

    // If the current state of this AlarmOperand is not equal to the evaluation
    // of its children, then it is changing state.
    if (isActive_ != evaluateReset_()) // $[TI1.1]
    {
      // Record the time the AlarmOperand changed state.
      intervalTime_.now();
      whenOccurred_.now();
      isActive_ = !isActive_;

      // If this AlarmOperand causes any Alarm objects, set the Alarm object's
      // causeUpdate_ attribute.
      setCauseUpdate_();

      // If this AlarmOperand augments any Alarm objects, set the Alarm
      // object's augmentUpdate_ attribute.
      setAugmentUpdate_();
    } // else $[TI1.2]

    // If there is an object derived from ViolationHistoryManager attached to
    // to this AlarmOperand, reset it.
    resetViolationHistory_();
  }
  // If this AlarmOperand object's update_ attribute is TRUE, the Operating
  // Parameter Hierarchy is being evaluated in update mode.
  else if (update_) // $[TI2]
  {
    update_ = FALSE;

    // If the current state of this AlarmOperand is not equal to the evauation
    // of its children, then it is changing state.
    if (isActive_ != evaluateUpdate_()) // $[TI2.1]
    {

      // Record the time the AlarmOperand changed state.
      intervalTime_.now();
      whenOccurred_.now();
      isActive_ = !isActive_;
      
      // If this AlarmOperand causes any Alarm objects, set the Alarm object's
      // causeUpdate_ attribute.
      setCauseUpdate_();
      
      // If this AlarmOperand augments any Alarm objects, set the Alarm
      // object's augmentUpdate_ attribute.
      setAugmentUpdate_();
    } // $[TI2.2]
    // If there is an object derived from ViolationHistoryManager attached to
    // to this AlarmOperand, update it.
    updateViolationHistory_();
  } // else $[TI3]
  return(isActive_);
}

//=====================================================================
//
//      Protected Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setParentUpdate_
//
//@ Interface-Description
//      Set the AlarmOperand::update_ attribute of all parent AlarmOperand
//      objects of this AlarmOperand.
//---------------------------------------------------------------------
//@ Implementation-Description
//      none
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
AlarmOperand::setParentUpdate_(void)
{
  CALL_TRACE("setParentUpdate_(void)");

  SigmaStatus status = parents_.goFirst();
  
  while (status == SUCCESS) // $[TI1]
  {
    parents_.currentItem().setUpdate();
    status = parents_.goNext();
  } // No while statement executed. $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setParentReset_
//
//@ Interface-Description
//      Set the AlarmOperand::reset_ attribute of all parent AlarmOperand
//      objects of this AlarmOperand.
//---------------------------------------------------------------------
//@ Implementation-Description
//      none
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
AlarmOperand::setParentReset_(void)
{
  CALL_TRACE("setParentReset_(void)");

  SigmaStatus status = parents_.goFirst();

  while (status == SUCCESS) // $[TI1]
  {
    parents_.currentItem().setReset();
    status = parents_.goNext();
  } // No while statement executed. $[TI2]
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//@ Interface-Description
//   This method is called when a software fault is detected by the
//   fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//   'lineNumber' are essential pieces of information.  The 'pFileName'
//   and 'pPredicate' strings may be defaulted in the macro to reduce
//   code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This method receives the call for the SoftFault, adds it sub-system
//   and class name ID and sends the message to the non-member function
//   SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition   
//   none
//@ End-Method
//=====================================================================

void
AlarmOperand::SoftFault(const SoftFaultID  softFaultID,
                        const Uint32       lineNumber,
                        const char*        pFileName,
                        const char*        pPredicate)  
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, ALARM_ANALYSIS, 
                          Alarm_Analysis::ALARMOPERAND, lineNumber,
                          pFileName, pPredicate);
}
