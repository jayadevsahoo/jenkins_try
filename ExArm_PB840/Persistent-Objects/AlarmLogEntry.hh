
#ifndef AlarmLogEntry_HH
#define AlarmLogEntry_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sotred in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class:  AlarmLogEntry - Entry for an Alarm History Log.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Persistent-Objects/vcssrc/AlarmLogEntry.hhv   25.0.4.0   19 Nov 2013 14:18:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By:  sah    Date:  01-Jan-1999    DCS Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' to a non-inlined method.
//
//  Revision: 001  By:  sah    Date:  15-Mar-1997    DCS Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//          Initial version (Integration baseline).
//
//====================================================================

#include "PersistentObjsId.hh"

//@ Usage-Classes
#include "AlarmAction.hh"
#include "CrcUtilities.hh"
#if defined(SIGMA_DEVELOPMENT)
#include "Ostream.hh"
#endif  // defined(SIGMA_DEVELOPMENT)
//@ End-Usage


class AlarmLogEntry
{
#if defined(SIGMA_DEVELOPMENT)
    friend ostream&  operator<<(ostream&             ostr,
				const AlarmLogEntry& logEntry);
#endif  // defined(SIGMA_DEVELOPMENT)

  public:
    AlarmLogEntry(void);
    ~AlarmLogEntry(void);

    inline Boolean  isClear(void) const;
    inline void     clear  (void);

    inline Boolean  isCorrected(void) const;
    void            correct    (void);

    inline Boolean  isTimeDateEntry(void) const;

    inline Boolean  isValid(void) const;

    inline const AlarmAction&  getAlarmAction(void) const;

    void  logAction(const AlarmAction& newAlarmAction);

    inline void  operator=(const AlarmLogEntry& logEntry);

    inline Boolean  operator>(const AlarmLogEntry& logEntry) const;

    inline Boolean  operator==(const AlarmLogEntry& logEntry) const;
    inline Boolean  operator!=(const AlarmLogEntry& logEntry) const;

    static void  SoftFault(const SoftFaultID softFaultID, 
			   const Uint32      lineNumber, 
			   const char*       pFileName  = NULL, 
			   const char*       pPredicate = NULL);

  private:
    AlarmLogEntry(const AlarmLogEntry&);	// not implemented...

    inline size_t  getCrcOffset_(void) const;

    //@ Data-Member:  alarmAction_
    // Alarm event.
    AlarmAction  alarmAction_;

    //@ Data-Member:  paddingBlock_
    // Since 'CrcValue' is a 16-bit type, the compiler adds 16 bits of
    // padding to this structure.  By defining this data member, the
    // calculation of the CRC value includes the padding.  This ensures
    // that any corruption within the alarm history log can be detected.
    Uint16  paddingBlock_;

    //@ Data-Member:  entryCrc_
    // CRC value for this log entry.
    CrcValue  entryCrc_;
};


// Inlined methods..
#include "AlarmLogEntry.in"


//====================================================================
//
//  Global Constants...
//
//====================================================================

//@ Constant:  MAX_ALARM_ENTRIES
// Constant that indicates the maximum number of alarm actions that are to be
// stored in the Alarm History Log.
//
//  $[00629] -- consists of a minimum of 50 entries...
enum { MAX_ALARM_ENTRIES = 80 };


#endif // AlarmLogEntry_HH 
