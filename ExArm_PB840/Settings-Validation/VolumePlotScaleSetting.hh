
#ifndef VolumePlotScaleSetting_HH
#define VolumePlotScaleSetting_HH

//====================================================================
//
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
//@ Class:  VolumePlotScaleSetting - Y Axis Scale for the Volume Waveform.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/VolumePlotScaleSetting.hhv   25.0.4.0   19 Nov 2013 14:27:50   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'getApplicability()' method
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Initial version.
//  
//====================================================================

//@ Usage-Classes
#include "NonBatchDiscreteSetting.hh"
//@ End-Usage


class VolumePlotScaleSetting : public NonBatchDiscreteSetting
{
  public:
    VolumePlotScaleSetting(void);
    virtual ~VolumePlotScaleSetting(void);

    virtual Applicability::Id  getApplicability(
			    const Notification::ChangeQualifier qualifierId
					       ) const;

    virtual SettingValue  getDefaultValue(void) const;

    static void   SoftFault(const SoftFaultID softFaultID,
			    const Uint32 lineNumber,
			    const char* pFileName  = NULL, 
			    const char* pPredicate = NULL);

  private:
    VolumePlotScaleSetting(const VolumePlotScaleSetting&);// not implemented
    void  operator=(const VolumePlotScaleSetting&);    // not implemented...
};


#endif // VolumePlotScaleSetting_HH 
