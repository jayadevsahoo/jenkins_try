////////////////Copyright (c) 2013 Covidien/////////////////////////////////////
/*******************************************************************************
Name:		HandlerFactory.h

Purpose:
	Header file for CommandHandlerFactory class.

REVISION HISTORY:

REVISION: 001	BY: SF		DATE: 20-AUG-2013
PROJECT: e600 vent
DR NUMBER: N / A
DESCRIPTION:
	Initialize version.

END OF REVISIONS

*******************************************************************************/

#pragma once

#include "CommandHandlerBase.h"

class CommandHandlerFactory
{
public:
	static CommandHandlerBase* QueryHandler(const char* type);
};