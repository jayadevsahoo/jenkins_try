#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//================= C L A S S   D E S C R I P T I O N =================
//@ Class:  InspPressSetting - Inspiratory Pressure Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, bounded setting that contains the pressure (in cmH2O)
//  to which gas is delivered to the patient during pressure-controlled
//  inspirations (PCV).  This class inherits from 'BoundedSetting' and
//  provides the specific information needed for representation of
//  inspiratory pressure.  This information includes the interval and range
//  of this setting's values, this setting's response to a Main Control
//  Setting's transition (see 'acceptTransition()'), this batch setting's
//  new-patient value (see 'getNewPatientValue()'), and the contraints of
//  this setting.
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class defines an 'updateConstraints_()' method for updating
//  the constraints of this setting.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/InspPressSetting.ccv   25.0.4.0   19 Nov 2013 14:27:28   pvcs  $
//
//@ Modification-Log
//
//  Revision: 011   By: gdc    Date: 24-Sep-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal non-invasive CPAP.
//
//  Revision: 010   By: sah    Date: 29-Jun-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  modified to use newly-modified 'BoundedRange' class to handle
//         range, via linked list of interval nodes
//
//  Revision: 009   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'getApplicability()' method
//	*  incorporated new BiLevel-specific settings
//	*  removed unnecessary 'isAcceptedValid()' method
//
//  Revision: 008   By: sah   Date: 11-Nov-1998    DR Number:  5257
//  Project:  BILEVEL
//  Description:
//	Modified 'acceptTransition()' to use Pi's 'updateConstraints()'
//	method for determining limits, rather than explicitly calculating
//	limits with redundant code.
//
//  Revision: 007   By: dosman    Date:  07-May-1997    DR Number: 
//  Project:  BILEVEL
//  Description:
//	add/fix TIs
//
//  Revision: 006   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 005   By: dosman    Date:  24-Dec-1997    DR Number: 
//  Project:  BILEVEL
//  Description:
//	initial bilevel version
//	added symbolic constants
//	fix value to pi=peephigh-peeplow upon transition
//	from bilevel to some other mode with PCV
//	changed acceptTransition() so that if we are transitioning
//      mand type from VCV to PCV and new mode is bilevel, 
//      then we will not do anything because PI is not applicable in bilevel
//      Added requirement numbers for tracing to SRS. 
//
//  Revision: 004   By: sah    Date:  28-May-1998    DR Number: 5058
//  Project:  COLOR
//  Description:
//	--- Merged Rev "Color" (1.24.1.0) into Rev "BiLevel" (1.24.2.0)
//	Modified range/resolution for improving the user's ability to
//	accurately change this setting's value.
//
//  Revision: 003   By: sah    Date:  11-Feb-1997    DR Number: 1748
//  Project: Sigma (R8027)
//  Description:
//	Added absolute bound, for when PEEP is zero.
//
//  Revision: 002   By: sah    Date:  08-Nov-1996    DR Number: 1534 & 1562
//  Project: Sigma (R8027)
//  Description:
//	Fixed "warping" in 'updateConstraints_()' to be UP/DOWN, instead
//	of NEAREST; NEAREST causes problems due to incompatible resolutions.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//=====================================================================

#ifdef SIGMA_DEVELOPMENT
	#include "Ostream.hh"
#endif // SIGMA_DEVELOPMENT

#include "InspPressSetting.hh"
#include "MandTypeValue.hh"
#include "ModeValue.hh"
#include "SettingConstants.hh"

//@ Usage-Classes
#include "SettingsMgr.hh"
//@ End-Usage


//@ Code...

//====================================================================
//
//  Static Data Definitions...
//
//====================================================================

// $[02167] The setting's range ...
// $[02170] The setting's resolution ...
static const BoundedInterval  LAST_NODE_ =
{
	5.0f,		// absolute minimum...
	0.0f,		// unused...
	ONES,		// unused...
	NULL
};
static const BoundedInterval  INTERVAL_LIST_ =
{
	90.0f,		// absolute maximum...
	1.0f,		// resolution...
	ONES,		// precision...
	&::LAST_NODE_
};


//====================================================================
//
//  Public Methods...
//
//====================================================================

//============= M E T H O D   D E S C R I P T I O N ==================
//@ Method: InspPressSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This setting inherits from BoundedSetting.  The method passes to
//  BoundedSetting it's default information.  Also, the value interval
//  range is initialized.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

InspPressSetting::InspPressSetting(void)
: BatchBoundedSetting(SettingId::INSP_PRESS,
					  Setting::NULL_DEPENDENT_ARRAY_,
					  &::INTERVAL_LIST_,
					  INSP_PRESS_MAX_BASED_PEEP_ID,	// upperBoundId
					  INSP_PRESS_MIN_ID)		// minConstraintId...
{
	CALL_TRACE("InspPressSetting()");
}	// $[TI1]


//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  ~InspPressSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

InspPressSetting::~InspPressSetting(void)
{
	CALL_TRACE("~InspPressSetting()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[01071] -- main settings applicability
//  $[01081] -- breath settings applicability
//---------------------------------------------------------------------
//@ PreCondition
//  (qualifierId == Notification::ACCEPTED  ||
//   qualifierId == Notification::ADJUSTED)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
	InspPressSetting::getApplicability(const Notification::ChangeQualifier qualifierId) const
{
	CALL_TRACE("getApplicability(qualifierId)");

	const Setting*  pMode     = SettingsMgr::GetSettingPtr(SettingId::MODE);
	const Setting*  pMandType = SettingsMgr::GetSettingPtr(SettingId::MAND_TYPE);

	DiscreteValue  modeValue;
	DiscreteValue  mandTypeValue;

	switch ( qualifierId )
	{
		case Notification::ACCEPTED :	  // $[TI1]
			modeValue     = pMode->getAcceptedValue();
			mandTypeValue = pMandType->getAcceptedValue();
			break;
		case Notification::ADJUSTED :	  // $[TI2]
			modeValue     = pMode->getAdjustedValue();
			mandTypeValue = pMandType->getAdjustedValue();
			break;
		default :
			AUX_CLASS_ASSERTION_FAILURE(qualifierId);
			break;
	}

	Applicability::Id  applicability;

	switch ( modeValue )
	{
		case ModeValue::AC_MODE_VALUE :
		case ModeValue::SIMV_MODE_VALUE :
		case ModeValue::SPONT_MODE_VALUE :	  // $[TI3]
		case ModeValue::CPAP_MODE_VALUE :
			applicability = (mandTypeValue == MandTypeValue::PCV_MAND_TYPE)
							? Applicability::CHANGEABLE	   // $[TI3.1]
							: Applicability::INAPPLICABLE; // $[TI3.2]
			break;

		case ModeValue::BILEVEL_MODE_VALUE :  // $[TI4]
			applicability = Applicability::INAPPLICABLE;
			break;

		default :
			// unexpected mode value...
			AUX_CLASS_ASSERTION_FAILURE(modeValue);
			break;
	}

	return(applicability);
}


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method: acceptTransition(settingId, newValue, currValue) [virtual]
//
//@ Interface-Description
//  This method is responsible for updating this setting based on the
//  the Main Control Setting identified by 'settingId' and the transition
//  of its value from 'currValue' to 'newValue'.
//
//  This setting is only interested in 
//  changes of mandatory type from "VCV" to "PCV",
//  or
//  changes of mode from BiLevel
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02021]\b\   -- transition from VCV...
//  $[BL02000]\a\ -- transition from BILEVEL to SPONT...
//  $[BL02000]\d\ -- transition from BILEVEL to non-SPONT...
//---------------------------------------------------------------------
//@ PreCondition
//  (
//  (settingId == SettingId::MAND_TYPE  &&
//   newValue  == MandTypeValue::PCV_MAND_TYPE)
//  ||
//  (settingId == SettingId::MODE  &&
//   currValue  == ModeValue::BILEVEL_MODE_VALUE)
//  )
//---------------------------------------------------------------------
//@ PostCondition
//   None
//@ End-Method
//=====================================================================

void
	InspPressSetting::acceptTransition(const SettingId::SettingIdType settingId,
									   const DiscreteValue            newValue,
									   const DiscreteValue            currValue)
{
	CALL_TRACE("acceptTransition(settingId, newValue, currValue)");

	BoundedValue  inspPress;

	switch ( settingId )
	{
		case SettingId::MODE :
			{ // $[TI1] 
			  // transition is caused by change of mode out of BiLevel...
				AUX_CLASS_PRE_CONDITION((currValue == ModeValue::BILEVEL_MODE_VALUE),
										currValue);

				const Setting*  pPeepHigh =
					SettingsMgr::GetSettingPtr(SettingId::PEEP_HIGH);
				const Setting*  pPeepLow =
					SettingsMgr::GetSettingPtr(SettingId::PEEP_LOW);

				const Real32  PEEP_HIGH_VALUE =
					BoundedValue(pPeepHigh->getAdjustedValue()).value;
				const Real32  PEEP_LOW_VALUE =
					BoundedValue(pPeepLow->getAdjustedValue()).value;

				inspPress.value = (PEEP_HIGH_VALUE - PEEP_LOW_VALUE);

				// reset both constraints for the warping below...
				getBoundedRange_().resetMinConstraint();
				getBoundedRange_().resetMaxConstraint();

				// warp the calculated value to a "click"...
				getBoundedRange_().warpValue(inspPress);
			}
			break;

		case SettingId::MAND_TYPE :
			{  // $[TI2] 
				// transition is caused by change of MANDATORY TYPE
				// inspiratory pressure's transition is due to a mandatory type change
				// from 'VCV' to 'PCV'...
				AUX_CLASS_PRE_CONDITION((newValue == MandTypeValue::PCV_MAND_TYPE),
										newValue);

				// We can assume, if we reach this code, that mand type is being changed 
				// from VCV to PCV (the assertion above checked that 
				// assumption).  
				// We could be having a transition from VCV to PCV either
				// by explicit choice of the user or changing into bilevel from
				// non-bilevel mode.
				// This code will be executed even when we transition from a non-bilevel
				// mode to bilevel mode. However, we do not want to compute PI if we are
				// going into bilevel because (1) PI will not be used in bilevel, and 
				// (2) PI will be changed when we transition out of bilevel.
				// Therefore, skip over all this code if adjusted mode is bilevel.
				Setting*  pMode = SettingsMgr::GetSettingPtr(SettingId::MODE);

				const DiscreteValue  MODE_VALUE =
					DiscreteValue(pMode->getAdjustedValue());

				if ( MODE_VALUE != ModeValue::BILEVEL_MODE_VALUE )
				{	// $[TI2.1]
					//====================================================================
					// mandatory type transition from VCV to PCV...
					//====================================================================

					// use new-patient value...
					inspPress = getNewPatientValue();

					BoundStatus  dummyStatus(getId());

					// calculate bound constraints...
					updateConstraints_();

					// use the calculation of the maximum bound in 'updateConstraints_()'
					// to limit the current adjusted value to a valid range...
					getBoundedRange_().testValue(inspPress, dummyStatus);
				}
				else
				{	// $[TI2.2]
					// no need to change Pi when in BiLevel mode...
					inspPress = getAdjustedValue();
				}
			}
			break;

		default:
			AUX_CLASS_ASSERTION_FAILURE(settingId);
			break;
	} // end switch

	// store the transition value into the adjusted context...
	setAdjustedValue(inspPress);

	// activation of transition rule must be italicized...
	setForcedChangeFlag();
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [const, virtual]
//
//@ Interface-Description
//  Get the setting's new-patient value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns a constant
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
	InspPressSetting::getNewPatientValue(void) const
{
	CALL_TRACE("getNewPatientValue()");

	BoundedValue  newPatient;

	// $[02168] The setting's new-patient value ...
	newPatient.value     = SettingConstants::DEFAULT_MAND_PRESS_DELTA;
	newPatient.precision = ONES;

	return(newPatient);	  // $[TI1]
}


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//	      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	InspPressSetting::SoftFault(const SoftFaultID  softFaultID,
								const Uint32       lineNumber,
								const char*        pFileName,
								const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

	FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
							INSP_PRESS_SETTING, lineNumber, pFileName,
							pPredicate);
}


//====================================================================
//
//  Protected Methods...
//
//====================================================================

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  updateConstraints_()  [virtual]
//
//@ Interface-Description
//  Update the dynamic bounds of this setting.  This method sets this
//  setting's maximum constraint to the most restrictive of this setting's
//  upper bounds, and, correspondingly, set this setting's minimum
//  constraint to its most restrictive lower bound.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Determine the minimum upper bound, and set the upper limit to it.
//
//  $[02167]
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	InspPressSetting::updateConstraints_(void)
{
	CALL_TRACE("updateConstraints_()");

	const Real32  ABSOLUTE_MAX = getAbsoluteMaxValue_();

	BoundedValue  maxLimit;

	BoundedRange::ConstraintInfo  maxConstraintInfo;

	// set to absolute maximum bound...
	maxConstraintInfo.id    = INSP_PRESS_MAX_ID;
	maxConstraintInfo.value = ABSOLUTE_MAX;

	//===================================================================
	// calculate inspiratory pressure's maximum bound that is based
	// on PEEP...
	//===================================================================

	const Setting*  pPeep = SettingsMgr::GetSettingPtr(SettingId::PEEP);

	const Real32  PEEP_VALUE = BoundedValue(pPeep->getAdjustedValue()).value;

	maxLimit.value = (SettingConstants::MAX_ALLOWED_PRESSURE_SETTING_VALUE -
					  PEEP_VALUE);

	if ( maxLimit.value < maxConstraintInfo.value )
	{	// $[TI1]
		// reset this range's maximum constraint to allow for "warping" of this
		// new maximum value...
		getBoundedRange_().resetMaxConstraint();

		// warp the calculated maximum value to a lower "click" boundary...
		getBoundedRange_().warpValue(maxLimit, BoundedRange::WARP_DOWN);

		// the PEEP-based maximum is more restrictive than
		// 'maxConstraintInfo.value', therefore save its value and id as the
		// maximum bound value and id...
		maxConstraintInfo.id    = INSP_PRESS_MAX_BASED_PEEP_ID;
		maxConstraintInfo.value = maxLimit.value;
	}  // $[TI2] -- insp. pressure's absolute max is more restrictive...

	//===================================================================
	// calculate inspiratory pressure's maximum bound that is based
	// on high circuit pressure and PEEP...
	//===================================================================

	const Setting*  pHighCctPress =
		SettingsMgr::GetSettingPtr(SettingId::HIGH_CCT_PRESS);

	const Real32  HIGH_CCT_VALUE =
		BoundedValue(pHighCctPress->getAdjustedValue()).value;

	maxLimit.value = (HIGH_CCT_VALUE - PEEP_VALUE -
					  SettingConstants::MIN_MAND_PRESS_BUFF );

	if ( maxLimit.value < maxConstraintInfo.value )
	{	// $[TI3]
		// reset this range's maximum constraint to allow for "warping" of this
		// new maximum value...
		getBoundedRange_().resetMaxConstraint();

		// warp the calculated maximum value to a lower "click" boundary...
		getBoundedRange_().warpValue(maxLimit, BoundedRange::WARP_DOWN);

		// the high circuit pressure-based maximum is more restrictive than
		// 'maxConstraintInfo.value', therefore save its value and id as the maximum
		// bound value and id...
		maxConstraintInfo.id    = INSP_PRESS_MAX_BASED_PCIRC_PEEP_ID;
		maxConstraintInfo.value = maxLimit.value;
	}  // $[TI4] -- 'maxConstraintInfo.value' is more restrictive...

	// this setting has no maximum soft bounds...
	maxConstraintInfo.isSoft = FALSE;

	// update the maximum bound to the most restrictive bound...
	getBoundedRange_().updateMaxConstraint(maxConstraintInfo);
}
