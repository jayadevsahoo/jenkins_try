#pragma once

class Plot
{
	enum RectExtMethod
	{
		ExtLeft,
		ExtRight,
		ExtUp,
		ExtDown,
		ExtCenter
	};
protected:
	int m_width;			// plot width in pixels
	int m_height;			// plot height in pixels
	int m_timeLen;			// in seconds
	HWND m_hWnd;

	int m_topMargin;
	int m_rightMargin;
	int m_dataDelta;
	int m_markLength;
	int m_waveWidth;
	int m_waveHeight;
	int m_gridLines;

	CRect m_waveRect;

	double m_maxData;
	double m_baseData;
	double m_minData;

	int m_dataSpeed;

	double m_pixelPerData;
	double m_slicePixels;

	CPoint m_Origin;
	CString m_strBuf;
	bool m_bWiperReset;

	// when the data was reset
	DWORD m_dwWiperResetTime;

	// draw the gradient background
	void drawBackground(CDC* pMemDC);

	// draw the axis
	void drawCoordinate(CDC* pMemDC);

	// draw the plot
	void drawWaveForm(CDC* pMemDC, const deque<float>& data, const deque<float>& oldData);

	// the function is used to get a rectangle from a point
	void getRectByPt(const CPoint& ptBase, int width, int height, RectExtMethod extMethod, CRect& rectRtn);
	void getRectByPt(int x, int y, int width, int height, RectExtMethod extMethod, CRect& rectRtn);
	int  getYPos(float data);
	int  getXPos(int index);

	CDC m_dcMem;
	CDC m_dcBkgnd;
	CBitmap m_bmpBkgnd;
	CBitmap m_bmpMem;
	
	CBitmap* m_pOldMemBmp;
	CBitmap* m_pOldBkgndBmp;
	CDC* m_pWndDC;
	bool m_bBkgndInvalid;
	
	deque<float> m_data;
	deque<float> m_oldData;

public:
	Plot(HWND hWnd, int width, int height, int timeSpan, int maxScale, int midScale, int minScale);
	~Plot(void);
	
	void resetWiper();
	void getWaveRect(CRect& rectWaveForm);
	int  getDataCount();
	void setY(double min, double base, double max);
	void draw(CPaintDC* pPaintDC, int x, int y);
	void appendData(float data);
	void setDataSpeed(int speed);
};
