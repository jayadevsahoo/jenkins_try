
#ifndef PsvPhase_HH
#define PsvPhase_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: PsvPhase - Implements pressure support inspiration ventilation.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/PsvPhase.hhv   25.0.4.0   19 Nov 2013 14:00:06   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005   By: gdc   Date:  22-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		removed getExhSens_() methid as dead code, never called.
//
//  Revision: 004  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 003 By: syw    Date: 06-May-1996   DR Number: DCS 600
//  	Project:  Sigma (R8027)
//		Description:
//			Added getExhSens_().  Made maxInspTimeMs_ protected.
//
//  Revision: 002 By: syw    Date: 21-Feb-1996   DR Number: DCS 674
//  	Project:  Sigma (R8027)
//		Description:
//			Removed #ifndef around #include.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"

//@ Usage-Classes

#include "PressureBasedPhase.hh"

//@ End-Usage

class PsvPhase : public PressureBasedPhase
{
  public:
    PsvPhase( void) ;
    virtual ~PsvPhase( void) ;

    static void SoftFault( const SoftFaultID softFaultID,
							   const Uint32      lineNumber,
							   const char*       pFileName  = NULL, 
							   const char*       pPredicate = NULL) ;
  
  protected:
	virtual void determineEffectivePressureAndBiasOffset_( void) ;
	virtual void determineTimeToTarget_( void) ;
	virtual void determineFlowAccelerationPercent_( void) ;
	virtual void enableExhalationTriggers_( void) ;

	//@ Data-Member: maxInspTimeMs_
	// maximum inspiratory time
	Int32 maxInspTimeMs_ ;
	
  private:
    PsvPhase( const PsvPhase&) ;			// not implemented...
    void   operator=( const PsvPhase&) ;	// not implemented...

} ;


#endif // PsvPhase_HH 
