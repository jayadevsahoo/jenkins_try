#ifndef Fio2CalInfo_IN
#define Fio2CalInfo_IN

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
// Class: Fio2CalInfo - This class is the interface between the
//                      Fio2Monitor and the NovRamManager for
//                      retrieval and updates of calibration
//                      information.
//---------------------------------------------------------------------
// @ Version-Information
//
// @ Modification-Log
//
//  Revision: 002 By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//	Description:
//		Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 001  By:  by    Date:  05-Jan-1996    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getNovRamFio2CalInfo()
//
//@ Interface-Description
//  This method accepts no parameter and returns a constant reference
//  to an Fio2SensorCalInfo object.
//
//  This method returns of the contents of data member
//  fio2SensorCalInfo_ as a constant reference.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=====================================================================
 
inline const Fio2SensorCalInfo&
Fio2CalInfo::getNovRamFio2CalInfo( void ) const
{
    CALL_TRACE("Fio2CalInfo::getNovRamFio2CalInfo( void )");
 
    // $[TI1]
    return( fio2SensorCalInfo_ );
}
 
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setNovRamFio2CalInfo()
//
//@ Interface-Description
//  This method accepts a constant Fio2SensorCalInfo and returns
//  no value.
//
//  This method sets the content of private data member equal to
//  the values passed in.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=====================================================================
 
inline void
Fio2CalInfo::setNovRamFio2CalInfo( const Fio2SensorCalInfo newFio2SensorCalInfo
)
{
    CALL_TRACE("Fio2CalInfo::getNovRamFio2CalInfo( const Fio2SensorCalInfo newFio2SensorCalInfo )");
 
    // $[TI1]
    fio2SensorCalInfo_ = newFio2SensorCalInfo;
}
 

//=====================================================================
//
//  Protected Methods...
//
//=====================================================================
  
//=====================================================================
//
//  Private Methods...
//
//=====================================================================
   
#endif // Fio2CalInfo_IN

