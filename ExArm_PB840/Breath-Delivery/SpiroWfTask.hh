
#ifndef SpiroWfTask_HH
#define SpiroWfTask_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: SpiroWfTask - Spirometry, waveform and breath data tasks.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/SpiroWfTask.hhv   25.0.4.0   19 Nov 2013 14:00:10   pvcs  $
//
//@ Modification-Log
//
// 
//  Revision: 04   By:   rpr    Date: 21-Jul-2009     SCR Number: 6615
//  Project:  MAT
//  Description:
//      Removed assertions for when data is not being process fast enough
//		for queue protection. Instead just flush the queue maybe losing data
//		points for spirometry etc.
//
//  Revision: 003  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 002  By:  sp   Date:  25-Feb-1997    DR Number: DCS NONE
//       Project:  Sigma (840)
//       Description:
//             Inspection rework.
//
//  Revision: 001  By:  sp    Date:  10-Oct-1994    DR Number: DCS NONE
//       Project:  Sigma (840)
//       Description:
//             Initial version
//
//=====================================================================

# include "Sigma.hh"
# include "Breath_Delivery.hh"
# include "IpcIds.hh"

//@ Usage-Classes
//@ End-Usage


class SpiroWfTask 
{
  public:

    static void SpirometryTask( void );

    static void WaveformTask( void );

    static void BreathDataTask( void );

    static void SoftFault( const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL, 
			   const char*       pPredicate = NULL );
  
  protected:

  private:

    SpiroWfTask(void);				// not implemented...
    ~SpiroWfTask(void);				// not implemented...
    SpiroWfTask(const SpiroWfTask&);		// not implemented...
    void   operator=(const SpiroWfTask&);	// not implemented...

};


#endif // SpiroWfTask_HH 
