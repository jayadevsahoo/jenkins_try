#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//================= C L A S S   D E S C R I P T I O N =================
//@ Class:  BoundStatus - Status for a Bound.
//---------------------------------------------------------------------
//@ Interface-Description
//  Every setting contains a bound status.  As knob clicks are sent to
//  a setting, the setting's bound status is returned.
//
//  This bound status contains the status of applying the knob click's
//  to the setting value. The bound status class encapsulates the
//  bound state, bound identification, and it's associated setting id.
//
//  Bound states are "NO_BOUND_VIOLATION", "LOWER_HARD_BOUND",
//  "LOWER_SOFT_BOUND", "UPPER_SOFT_BOUND", and "UPPER_HARD_BOUND".
//  If a bound violation occurs the bound state will reflect this condition,
//  and the bound id will be set to the bound id of the bound with the
//  violation.
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for the bound status.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The class contains various set and retrieval methods for the setting's
//  bound statuses.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/BoundStatus.ccv   25.0.4.0   19 Nov 2013 14:27:16   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004   By: sah    Date: 06-Jul-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	Eliminated use of unneeded 'INVALID_DISCRETE_VALUE' bound state.
//
//  Revision: 003   By: sah  Date:  11-Jan-1999    DR Number:  5321
//  Project:  ATC
//  Description:
//	As part of making all 'SoftFault()' methods non-inlined, I'm
//	also making those 'SoftFault()' methods that are NOT called
//	in PRODUCTION mode, DEVELOPMENT-only.
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//=====================================================================

#include "BoundStatus.hh"

//@ Usage-Classes
//@ End-Usage


//@ Code...

//====================================================================
//
//  Public Methods...
//
//====================================================================

//============= M E T H O D   D E S C R I P T I O N ==================
//@ Method:  BoundStatus(settingId)  [Constructor]
//
//@ Interface-Description
//  Construct a bound status for the setting represented by 'settingId'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  (!isInViolation())
//@ End-Method
//=====================================================================

BoundStatus::BoundStatus(const SettingId::SettingIdType settingId)
			 : violatedSettingId_(settingId),
			   boundState_(::NO_BOUND_VIOLATION),
			   boundId_(::NULL_SETTING_BOUND_ID)
{
  CALL_TRACE("BoundStatus(settingId)");
}  // $[TI1]


//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  ~BoundStatus()  [Destructor]
//
//@ Interface-Description
//  Destroy this bound status.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

BoundStatus::~BoundStatus(void)
{
  CALL_TRACE("~BoundStatus()");
}


#if defined(SIGMA_DEVELOPMENT)

//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//        		[static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
BoundStatus::SoftFault(const SoftFaultID  softFaultID,
		       const Uint32       lineNumber,
		       const char*        pFileName,
		       const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION, BOUND_STATUS,
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)
