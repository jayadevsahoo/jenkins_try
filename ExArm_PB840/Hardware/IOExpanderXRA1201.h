
//*****************************************************************************
//
//  OExpanderMCP23016.h - Header file for IOExpanderMCP23016_t class.
//
//
//*****************************************************************************
#ifndef IOEXPANDER_XRA1201_H

#define IOEXPANDER_XRA1201_H

// TODO E600 remove unnecessary and dead code later
#include "I2C.h"
#include "Sigma.hh"

/*
    BYTE PIN_DIRECTION[16] =
    {
        0x01,  //ACCEPT_BUTTON  KEY1
        0x02,  //CANCEL_BUTTON  KEY2
        0x04,  //HOME_BUTTON    KEY3
        0x08,  //R1_BUTTON      KEY4
        0x10,  //R2_BUTTON      KEY5
        0X20,  //RESET_BUTTON   KEY6
        0x40,  //SILENCE_BUTTON KEY7
        0x80,  //SPARE_BUTTON   KEY8
        0xFE,  // RED LED
        0xFD,  // AMBER LED
        0xFB,  // TEST POINT (OUTPUT)
        0xF7,  // BACLKIGHT ON/OFF
        0x10,  // INPUT
        0x20,  // INPUT
        0x40,  // INPUT
        0x7F  // AMBIENT LIGHT SENSOR
    };
*/

static const uint32_t MICROSECONDS_PER_SECOND = 1000000;

// command byte to register definition
static const uint8_t ACCESS_TO_GP0      = 0x00; // command to access GPIO port Register 0
static const uint8_t ACCESS_TO_GP1      = 0x01; // command to access GPIO port Register 1
static const uint8_t ACCESS_TO_OLAT0    = 0x02; // command to access Output Latch Register 0
static const uint8_t ACCESS_TO_OLAT1    = 0x03; // command to access Output Latch Register 1
static const uint8_t ACCESS_TO_IPOL0    = 0x04; // command to access Input Polarity Register 0
static const uint8_t ACCESS_TO_IPOL1    = 0x05; // command to access Input Polarity Register 1
static const uint8_t ACCESS_TO_IODIR0   = 0x06; // command to access I/O Direction Register 0
static const uint8_t ACCESS_TO_IODIR1   = 0x07; // command to access I/O Direction Register 1
static const uint8_t ACCESS_TO_INCON0   = 0x08; // command to Pull Up Resistors Enable 0
static const uint8_t ACCESS_TO_INCON1   = 0x09; // command to Pull Up Resistors Enable  1
static const uint8_t ACCESS_TO_INTCAP0  = 0x0A; // command to access Interrupt Capture Register 0
static const uint8_t ACCESS_TO_INTCAP1  = 0x0B; // command to access Interrupt Capture Register 1

enum LedStatus_t
{
    ALARM_LED_OFF,
    ALARM_LED_ON
};
enum GpioPinStatus_t
{
    GPIO_OFF,
    GPIO_ON
};
#define     NUUMBER_OF_EXPANDER_PORT_GPIO_PINS 8
static const BYTE GpioPortControlsTable[NUUMBER_OF_EXPANDER_PORT_GPIO_PINS] =
{
    {0x01},              //PIN_0 8
    {0x02},              //PIN_1 9
    {0x04},              //PIN_2 10
    {0x08},              //PIN_3 11
    {0x10},              //PIN_4 12
    {0x20},              //PIN_5 13
    {0x40},              //PIN_6 14
    {0x80}               //PIN_7 15
};
enum GpioPorts_t
{
    GPIO_PORT_0,
    GPIO_PORT_1,

    NUMBER_OF_GPIO_PORTS
};
class IOExpanderXRA1201_t : public I2C
{
	public:
        IOExpanderXRA1201_t(HANDLE FileHandle, uint8_t BusAddress, I2cSubAddressMode_t SubAddrMode, I2cBaudIndex_t BaudIndex, DWORD Timeout);

        ~IOExpanderXRA1201_t();

        // set direction of bi-directional GPIOs
        bool SetPinDirection (BYTE Port);

        // set pin status of bi-directional GPIOs
        bool SetPinStatus    (BYTE DigitalInputOutput, GpioPinStatus_t PinStatus);

        // read direction of bi-directional GPIOs
        bool ReadPinStatus   (BYTE DigitalInputOutput, GpioPinStatus_t *PinStatus);
        bool ReadPort0Status (BYTE *PortStatus);

        void StartInitialSelfTest();

        bool IOExpanderInitialSelfTestResult()
        {
            return InitialSelfTestResult;
        }
        bool EnableIRQ(void);
		void SetAmberLed(LedStatus_t State);
		void SetRedLed(LedStatus_t State);


    protected:

        void MicroSecondsDelay(const uint16_t MicroSeconds);                                            // a help member function
        BYTE PinStateRegister1; // To read Input Pins
        BYTE PinStateRegister0; // To read Input Pins
        BYTE OutputRegister1;   // To set Output Pins
        BYTE OutputRegister0;   // To set Output Pins
        uint8_t GpioPinStatusData[NUUMBER_OF_EXPANDER_PORT_GPIO_PINS];

        bool InitialSelfTestResult;
};

#endif

