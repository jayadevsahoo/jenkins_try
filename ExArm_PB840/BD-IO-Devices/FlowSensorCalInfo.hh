
#ifndef FlowSensorCalInfo_HH
#define FlowSensorCalInfo_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: FlowSensorCalInfo - Stores calibration and other specific 
//     information about the flow sensors, as received from the serial EPROM.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/FlowSensorCalInfo.hhv   26.0.1.0   04 Feb 2013 10:36:40   pvcs  $
//
//@ Modification-Log
//
//  Revision: 011  By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 010 By: syw    Date: 07-Apr-1998   DR Number: DCS 5065
//  	Project:  Sigma (840)
//		 	BiLevel initial version.  Added code to support NEW_SENSOR
//			(conditionally compiled).  Eliminate modelNumber_.
//
//  Revision: 009 By: syw    Date: 27-Aug-1997   DR Number: DCS 2426 
//  	Project:  Sigma (R8027)
//		Description:
//			Changed integer data types to Uint32.
//
//  Revision: 008 By: syw    Date: 03-Jul-1997   DR Number: DCS 1908 
//  	Project:  Sigma (R8027)
//		Description:
//			Added back temperatureToCf_[] and methods associated to improve
//			thruput by avoiding sqrt calculation on the fly.  Changed all integers
//			to Uint32.
//
//  Revision: 007 By: syw    Date: 01-Apr-1997   DR Number: DCS 1827 
//  	Project:  Sigma (R8027)
//		Description:
//			Remove temperatureToCf_[] and methods associated.
//
//  Revision: 006 By: syw    Date: 10-Sep-1996   DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Remove setTemperatureCorrection_().  Call public method
//			setTemperatureCorrection( kFactor) instead.
//
//  Revision: 005 By: syw    Date: 18-Mar-1996   DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Remove setFlowSensorCalInfoTable().  Initialize flow sensor data
//			to prevent having garbage in the table.  Eventually, the data will
//			store the actual data.
//
//  Revision: 004 By: syw    Date: 20-Feb-1996   DR Number: DCS 674
//  	Project:  Sigma (R8027)
//		Description:
//			Removed #ifndef around #include.
//
//  Revision: 003  By: syw    Date:  25-Jan-1996    DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Added SIGMA_DEBUG printCalInfoTable() method.
//
//  Revision: 002  By: syw    Date:  05-Dec-1995    DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Moved copy constructor to private.  Added the following methods
//			for service mode usage:
//				- setSerialNumber()
//				- setTemperatureCorrection()
//				- setKFactor()
//				- setZFactor()
//				- setChecksum()
//				- setNumFlowCoeffSets()
//				- setTemperatureToCfTable()
//				- setCoeffEntry()
//
//	Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: INITIAL RELEASE
//      Project:  Sigma (R8027)
//      Description:
//      	Initial version
//
//=====================================================================

#include "Sigma.hh"
#include "BD_IO_Devices.hh"

//@ Usage-Classes
#include "FlowSensorCoeffEntry.hh"
//#include "Sensor.hh"

//@ End-Usage

//@ Constant: MAX_TEMPERATURE
// The maximum gas temperature (in degrees C) corrected for.
static const Uint32   MAX_TEMPERATURE = 70 ;

//@ Constant: MIN_TEMPERATURE
// The minimum gas temperature (in degrees C) corrected for.
static const Uint32   MIN_TEMPERATURE = 2 ;

//@ Constant: STEPS_PER_DEGREE_C
// The number of entries per degrees C
static const Uint32   STEPS_PER_DEGREE_C = 10 ;

//@ Constant: NUM_TEMPERATURE_ENTRIES
// The number of entries in the flow sensor temperature correction table
static const Uint32		 NUM_TEMPERATURE_ENTRIES = 
                       (MAX_TEMPERATURE - MIN_TEMPERATURE) *
                       STEPS_PER_DEGREE_C + 1;

//@ Constant: MAX_FLOW_SENSOR_COEFF_SETS
// The maximum number of coefficients sets in the flow sensor EEPROM
static const Uint32   MAX_FLOW_SENSOR_COEFF_SETS = 12 ;

static const Uint32   NUM_K_FACTORS = 5 ;
static const Uint32		TEMPTAB_SIZE = 41;


class FlowSensorCalInfo {
    // friend class FlowSensorCoeffEntry
  public:
    void   operator=( const FlowSensorCalInfo&) ;
   
    FlowSensorCalInfo( void) ;
    ~FlowSensorCalInfo( void) ;

    static void SoftFault( const SoftFaultID  softFaultID,
						   const Uint32       lineNumber,
						   const char*        pFileName = NULL,
						   const char*        pPredicate = NULL) ;

    inline Uint32 getSerialNumber( void) const ;
    inline Real32 getTemperatureCorrection( void) const ;
    inline Real32 getKFactor( const Uint32 index) const ;
    inline Uint32 getChecksum( void) const ;
    
	Real32 convertTemperature( const Uint32 counts) ;
	Real32 getCorrectionFactor( const Real32 temperature) ;

	inline Uint32 getRevision( void) const ;
	inline Uint8 getCalDate( void) const ;
	inline Uint8 getCalMon( void) const ;
	inline Uint16 getCalYear( void) const ;
	inline Real32 getSpan( void) const ;
	inline Real32 getZero( void) const ;
	inline Real32 getCalTemp( void) const ;

    inline void setSerialNumber( const Uint32 serialNumber) ;
    inline void setTemperatureCorrection( const Real32 tempCorr) ;
    inline void setKFactor( const Real32 kFactor, const Uint32 index) ;
    inline void setChecksum( const Uint16 checkSum) ;
    inline void setNumFlowCoeffSets( const Uint32 numCoeff) ;
    
	inline void setRevision( Uint32 revision) ;
	inline void setCalDate( Uint8 calDate) ;
	inline void setCalMon( Uint8 calMon) ;
	inline void setCalYear( Uint16 calYear) ;
	inline void setCalTemp( Real32 calTemp) ;
	inline void setSpan(Real32 span);
	inline void setZero(Real32 zero);
	inline void setModel(Uint32 model);

    void setCoeffEntry( FlowSensorCoeffEntry coeffSet[MAX_FLOW_SENSOR_COEFF_SETS]) ;

    Real32 getFlow( const AdcCounts flowCounts, const AdcCounts tempCounts) const;


#ifdef SIGMA_DEBUG
    void printCalInfoTable( void) ;
#endif  // SIGMA_DEBUG
    
  protected:

  private:
    FlowSensorCalInfo( const FlowSensorCalInfo&) ;

	void genTempCompFactorTab_();
	Real32 tempCompensateFlow_(const AdcCounts flowCounts, const AdcCounts tempCounts) const;
	Real32 calculateFlow_(Real32 Vfstd) const;
  
    Uint16 checksum_;

	//@ Data-Member: serialNumber_
    // The serial number read from the flow sensor
    Uint32 serialNumber_ ;

    //@ Data-Member: temperatureCorrection_
    // The temperature correction read from the flow sensor
    Real32 temperatureCorrection_ ;

    //@ Data-Member: kFactor_
    // The K factor read from the flow sensor
    Real32 kFactor_[NUM_K_FACTORS] ;

    //@ Data-Member: numFlowCoeffSets_
    //the number of accessible entries in airEntry_ table 
    //as defined in EEPROM
    Uint32 numFlowCoeffSets_ ;

	//@ Data-Member: model_
	// flow sensor model, 840201 for air and 840202 for O2
	Uint32 model_ ;

	//@ Data-Member: revision_
	// flow sensor revision
	Uint32 revision_ ;

	//@ Data-Member: calDate_
	// calibration date yymmdd
	Uint8 calDate_ ;
	Uint8 calMon_ ;
	Uint16 calYear_ ;

	//@ Data-Member: span_
	// temperature coefficient
	Real32 span_ ;

	//@ Data-Member: zero_
	// temperature coefficient
	Real32 zero_ ;

	//@ Data-Member: calTemp_
	// calibration temperature
	Real32 calTemp_ ;

    //@ Data-Member: FlowSensorCoeffEntry coeffSet_[MAX_FLOW_SENSOR_COEFF_SETS]
    // The flow coefficients read from the flow sensor
    FlowSensorCoeffEntry coeffSet_[MAX_FLOW_SENSOR_COEFF_SETS] ;

    //@ Data-Member: TempADTable[TEMPTAB_SIZE]
    // The table containing ADC values representing temperature
	Uint16 tempADTable_[TEMPTAB_SIZE];

	//@ Data-Member: TempCompFactorTab[TEMPTAB_SIZE]
    // The table containing values for temperature compensation 
	Real32 tempCompFactorTab_[TEMPTAB_SIZE];
} ;



// Inlined methods
#include "FlowSensorCalInfo.in"

#endif // FlowSensorCalInfo_HH 
