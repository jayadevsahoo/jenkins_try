
#ifndef SequentialSetting_HH
#define SequentialSetting_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  SequentialSetting - Sequential Setting.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/SequentialSetting.hhv   25.0.4.0   19 Nov 2013 14:27:40   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 002   By: sah    Date:  28-May-1998    DR Number: 5079
//  Project:  COLOR
//  Description:
//	--- Merged Rev "Color" (1.5.1.0) into Rev "BiLevel" (1.5.2.0)
//	Added ability of sequential settings to disable their bounds,
//	thereby allowing their values to cycle around.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Initial version.
//
//====================================================================

//@ Usage-Classes
#include "Setting.hh"

class SequentialRange;	// forward declaration...
//@ End-Usage


class SequentialSetting : public Setting 
{
  public:
    virtual ~SequentialSetting(void);

    inline const SequentialRange&  getSequentialRange(void) const;

    virtual const BoundStatus&  calcNewValue(const Int16 knobDelta);

#if defined(SIGMA_DEVELOPMENT)
    virtual Boolean  isAcceptedValid(void) const;
    virtual Boolean  isAdjustedValid(void);
#endif  // defined(SIGMA_DEVELOPMENT)

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL, 
			   const char*       pPredicate = NULL);

  protected:
    SequentialSetting(const SettingId::SettingIdType  settingId,
		      const SettingId::SettingIdType* arrDependentSettingIds,
		      SequentialRange&               rSequentialRange,
		      const Boolean                  useWrappingMode);

    inline SequentialRange&  getSequentialRange_(void);

#if defined(SIGMA_DEVELOPMENT)
    virtual Ostream&  print_(Ostream& ostr) const;
#endif  // defined(SIGMA_DEVELOPMENT)

  private:
    SequentialSetting(const SequentialSetting&);// not implemented...
    SequentialSetting(void);			// not implemented...
    void operator=(const SequentialSetting&);	// not implemented...

    //@ Data-Member:  rSequentialRange_
    // This manages the range of this sequential setting.
    SequentialRange&  rSequentialRange_;

    //@ Constant:  IS_IN_WRAPPING_MODE_
    // Boolean constant that indicates whether this instance's values
    // are to be cycled through, or bounded.
    const Boolean  IS_IN_WRAPPING_MODE_;
};


// Inlined methods...
#include "SequentialSetting.in"


#endif // SequentialSetting_HH 
