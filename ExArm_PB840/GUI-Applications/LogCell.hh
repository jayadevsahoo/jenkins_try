#ifndef LogCell_HH
#define LogCell_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: LogCell -  A container that handles the text formatting and display
// for a cell of ScrollableLog.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/LogCell.hhv   25.0.4.0   19 Nov 2013 14:08:04   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 001  By:  mpm    Date:  06-JUN-95    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

//@ Usage-Classes
#include "Container.hh"
#include "TouchableText.hh"
#include "TextField.hh"
#include "TextFont.hh"
#include "GuiAppClassIds.hh"
//@ End-Usage

class LogCell : public Container
{
public:
	LogCell(void);
    ~LogCell(void);

	void setText(Boolean isCheapText, StringId string1,
				StringId string2 = NULL_STRING_ID,
				StringId string3 = NULL_STRING_ID,
				Alignment alignment = CENTER,
				TextFont::Style fontStyle = TextFont::NORMAL,
				TextFont::Size fontSize = TextFont::TWELVE, Uint16 margin = 0,
				StringId string1Message = NULL_STRING_ID);

    static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);
  
protected:

private:
    // these methods are purposely declared, but not implemented...
    LogCell(const LogCell&);				// not implemented...
    void   operator=(const LogCell&);		// not implemented...

	//@ Data-Member: field1_
	// The textfield for rendering the first, and possibly only, line of
	// text.
	TouchableText field1_;

	//@ Data-Member: field2_
	// The textfield for rendering the optional second line of text.
	TextField field2_;

	//@ Data-Member: field3_
	// The textfield for rendering the optional third line of text.
	// This line can only be used when the text is specified with cheap text.
	TextField field3_;
};


#endif // LogCell_HH
