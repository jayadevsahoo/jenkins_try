#include "stdafx.h"

#if		defined(SIGMA_GUI_CPU) || defined(SIGMA_COMMON)
//=====================================================================
//
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//              Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//================== C L A S S   D E S C R I P T I O N ================
//@ Class:  BreathDatumHandle -  Provides a dedicated data access handle 
//          for each specific patient data type.
//---------------------------------------------------------------------
//@ Interface-Description
//     This class provies a dedicated data access handle for a specified
//     patient data type.
//---------------------------------------------------------------------
//@ Rationale
//   This class encapsulates data accessing interface to one specific
//   patient data type.
//---------------------------------------------------------------------
//@ Implementation-Description
//    Each handle maintains a pointer to the data processing agent
//    which actually manages a specified patient data type.
//    Since this class is mainly an interface to an actual data
//    processing agent object, most of the access methods are inline
//    methods to avoid extra performance overhead.
//---------------------------------------------------------------------
//@ Fault-Handling
//   Standard CLASS_PRE_CONDITION and CLASS_ASSERTION macros are used
//   to detect some of the obvious error conditions. These macros
//   end up calling BreathDatumHandle::SoftFault method.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header::   /840/Baseline/Patient-Data/vcssrc/BreathDatumHandle.ccv   10.7   08/17/07 10:27:16   pvcs  
//
//@ Modification-Log
//
//  Revision: 002  By: sah   Date:  13-Jan-99    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001  By:  jhv    Date:  16-Feb-95   DR Number: 
//    Project:  Sigma (R8027)
//    Description:
//        Initial version
//
//====================================================================


#include "BreathDatumHandle.hh"
#include  "PatientDataMgr.hh"



//@ Code...
//====================================================================
//
//  Public Methods...
//
//====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BreathDatumHandle()  [ Constructor]
//
//@ Interface-Description
//    Establishes exact breath data type this handle is going to
//    provide an access to.
//---------------------------------------------------------------------
//@ Implementation-Description
//    Initializes the 'pAgent_' member to the patient data processing
//    agent that handles this breath item data type.
//---------------------------------------------------------------------
//@ PreCondition
//  ( PatientDatamgr::IsBreathItemId(id) )
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

BreathDatumHandle::BreathDatumHandle(const PatientDataId::PatientItemId id)
{
  CALL_TRACE("BreathDatumHandle(PatientDataId::PatientItemId id)");
  CLASS_PRE_CONDITION( PatientDataMgr::IsBreathItemId(id) );

  pAgent_ = PatientDataMgr::GetDataAgent(id);
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~BreathDatumHandle()  [Destructor]
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

BreathDatumHandle::~BreathDatumHandle(void)
{
  CALL_TRACE("~BreathDatumHandle()");
	// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
BreathDatumHandle::SoftFault(const SoftFaultID  softFaultID,
							 const Uint32       lineNumber,
							 const char*        pFileName,
							 const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, PATIENT_DATA,
			  BREATH_DATUM_HANDLE, lineNumber,
			  pFileName, pPredicate);
}

#endif		//defined(SIGMA_GUI_CPU) || defined(SIGMA_COMMON)
