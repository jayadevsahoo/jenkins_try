/*****************************************************************************/
/*   Module Name:		     autozero.c                                          
/*   Purpose:				 Encodes behaviours for the buttons on the autozero panel                                 
/*                                                                          
/*                                                                           
/*   Requirements:  		 This module implements requirements detailed in  
/*
/*   Modification History:
/*   
/*   Description of Change			        By			      Date 
/*   ---------------------					-----   		  ------
/*	 version.h is added in the file for
/* 	 the purpose of demo mode.				Hamsavally(CGS)   12 Dec,2000
/*   functionality. 
/*     
/*							 
/*
/*                                                                           
/*   $Revision::   1.1      $                                                
/*                                                                          
/*****************************************************************************/

/*****************************************************************************
THIS IS A PROPRIETARY WORK TO WHICH NELLCOR PURITAN BENNETT CORPORATION CLAIMS
EXCLUSIVE RIGHT.  NO PART OF THIS WORK MAY BE USED, DISCLOSED, REPRODUCED,
STORED IN AN INFORMATION RETIEVAL SYSTEM, OR TRANSMITTED, IN ANY FORM OR BY ANY
MEANS ELECTRONIC, MECHANICAL, PHOTOCOPYING, RECORDING OR OTHERWISE WITHOUT THE
PRIOR WRITTEN PERMISSION OF NELLCOR PURITAN BENNETT CORPORATION.
 
Copyright (c) 1999, Mallinckrodt Nellcor Puritan Bennett
All rights reserved.
*******************************************************************************/

/***********************************************/
/****************** INCLUDES *******************/
/***********************************************/

#include <userint.h>
#include <utility.h>

#include "globals.h"
#include "72500.h"
#include "autozero.h"
#include "cpvtmain.h"
#include "helpinit.h"
#include "ldb.h"
#include "mainpnls.h"
#include "MsgPopup.h"
#include "pts.h"
#include "pts2000.h"
#include "rt_bmps.h"
#include "sysmenu.h"
#include "toolbox.h"
#include "version.h"

/***********************************************/
/******************* DEFINES *******************/
/***********************************************/

#define DIALOG_BGND_COLOR 	0XB0B0B0
#define NOERROR       0

#define CAL_PNL_WIDTH  500

#define CAL_BOX_TOP     18
#define CAL_BOX_LEFT    11
#define CAL_BOX_WIDTH  (CAL_PNL_WIDTH - (CAL_BOX_LEFT * 2)) //376
#define CAL_BOX_HEIGHT 180

#define CAL_OK_TOP     (CAL_BOX_TOP + CAL_BOX_HEIGHT + 12) //202
#define CAL_XIT_LEFT   (CAL_BOX_LEFT + CAL_BOX_WIDTH - 58) //331
#define CAL_OK_LEFT    (CAL_XIT_LEFT - 69) //262

#define CAL_PNL_HEIGHT (CAL_OK_TOP + 50 /*btn height*/ + 12)

#define CAL_CMD_BTN_WIDTH  192
#define CAL_CMD_BTN_HEIGHT  30
#define CAL_CMD_BTN_LEFT   100
#define CAL_CMD_HPBTN_TOP   41
#define CAL_CMD_LPBTN_TOP   86
#define CAL_CMD_O2BTN_TOP  131

#define CAL_INSTRUCT_LEFT   (CAL_BOX_LEFT + 15) //37
#define CAL_INSTRUCT_WIDTH  (CAL_PNL_WIDTH - (CAL_INSTRUCT_LEFT * 2)) //296
#define CAL_INSTRUCT_HEIGHT  18

#define CAL_INSTRUCT_LINE1_TOP (CAL_BOX_TOP + 20) //38
#define CAL_INSTRUCT_LINE2_TOP (CAL_INSTRUCT_LINE1_TOP + CAL_INSTRUCT_HEIGHT + 4) //60

#define CAL_STATUS_LEFT CAL_INSTRUCT_LEFT

#define O2_INSTRUCT_LEFT    CAL_INSTRUCT_LEFT // 27
#define O2_INSTRUCT_TOP     CAL_INSTRUCT_LINE1_TOP // 29
#define O2_INSTRUCT_WIDTH   CAL_INSTRUCT_WIDTH // 350
#define O2_INSTRUCT_HEIGHT  85

#define O2_CMD021_TOP     (O2_INSTRUCT_TOP + O2_INSTRUCT_HEIGHT + 0) //117
#define O2_CMD021_LEFT    O2_INSTRUCT_LEFT
#define O2_CMD021_WIDTH   185
#define O2_CMD021_HEIGHT  CAL_CMD_BTN_HEIGHT

#define O2_CMD100_TOP     (O2_CMD021_TOP + O2_CMD021_HEIGHT + 4)
#define O2_CMD100_LEFT    O2_INSTRUCT_LEFT
#define O2_CMD100_WIDTH   O2_CMD021_WIDTH
#define O2_CMD100_HEIGHT  O2_CMD021_HEIGHT

#define O2_STATUS021_TOP     (O2_CMD021_TOP + 5)
#define O2_STATUS021_LEFT    (O2_CMD021_LEFT + O2_CMD021_WIDTH + 4)
#define O2_STATUS021_WIDTH   10
#define O2_STATUS021_HEIGHT  18

#define O2_STATUS100_TOP     (O2_CMD100_TOP + 5)
#define O2_STATUS100_LEFT    (O2_CMD100_LEFT + O2_CMD100_WIDTH + 4)
#define O2_STATUS100_WIDTH   10
#define O2_STATUS100_HEIGHT  18

#define O2_LBL_TOP  (CAL_BOX_TOP + CAL_BOX_HEIGHT + 5)
#define O2_LBL_LEFT 105

#define O2_PCT_TOP  (O2_LBL_TOP + 20)
#define O2_PCT_LEFT O2_LBL_LEFT

/***********************************************/
/************** GLOBAL VARIABLES ***************/
/***********************************************/

static char* szEmptyString = "";

static HPNL AutoZeroPanel   = NULL;
static HPNL AutoZeroPanelHP = NULL;
static HPNL AutoZeroPanelLP = NULL;
static HPNL AutoZeroPanelO2 = NULL;

//-------------------------------------------------------------------------------------------------
// Declare identifiers for Panel:  AUTOZERO
// ------------------------------------------------------------------------------------------------

static int AUTOZERO_DECORATION                      = 0;  // control identifier
static int AUTOZERO_COMMANDBUTTON_HP                = 0;  // control identifier
static int AUTOZERO_COMMANDBUTTON_LP                = 0;  // control identifier
static int AUTOZERO_COMMANDBUTTON_O2                = 0;  // control identifier
static int AUTOZERO_COMMAND_CANCEL                  = 0;  // control identifier

//-------------------------------------------------------------------------------------------------
// Declare identifiers for Panel:  AUTOZEROHP
// ------------------------------------------------------------------------------------------------

static int AUTOZEROHP_DECORATION                    = 0;  // control identifier
static int AUTOZEROHP_INSTRUCT_LINE_1               = 0;  // control identifier
static int AUTOZEROHP_INSTRUCT_LINE_2               = 0;  // control identifier
static int AUTOZEROHP_STATUS                        = 0;  // control identifier
static int AUTOZEROHP_COMMANDBUTTON_OK              = 0;  // control identifier
static int AUTOZEROHP_COMMAND_CANCEL                = 0;  // control identifier

//-------------------------------------------------------------------------------------------------
// Declare identifiers for Panel:  AUTOZEROLP
// ------------------------------------------------------------------------------------------------

static int AUTOZEROLP_DECORATION                    = 0;  // control identifier
static int AUTOZEROLP_INSTRUCT_LINE_1               = 0;  // control identifier
static int AUTOZEROLP_INSTRUCT_LINE_2               = 0;  // control identifier
static int AUTOZEROLP_STATUS                        = 0;  // control identifier
static int AUTOZEROLP_COMMANDBUTTON_OK              = 0;  // control identifier
static int AUTOZEROLP_COMMAND_CANCEL                = 0;  // control identifier

//-------------------------------------------------------------------------------------------------
// Declare identifiers for Panel:  AUTOZEROO2
// ------------------------------------------------------------------------------------------------

static int AUTOZEROO2_DECORATION                    = 0;  // control identifier
static int AUTOZEROO2_CAL_O2_TEXT                   = 0;  // control identifier
static int AUTOZEROO2_CMD_CAL_O2_21                 = 0;  // control identifier
static int AUTOZEROO2_STATUS_21                     = 0;  // control identifier
static int AUTOZEROO2_CMD_CAL_O2_100                = 0;  // control identifier
static int AUTOZEROO2_STATUS_100                    = 0;  // control identifier
static int AUTOZEROO2_TIMER                         = 0;  // control identifier
static int AUTOZEROO2_PERCENT_O2_LBL                = 0;  // control identifier
static int AUTOZEROO2_PERCENT_O2                    = 0;  // control identifier
static int AUTOZEROO2_COMMAND_CANCEL                = 0;  // control identifier

/***********************************************/
/************ FUNCTION DECLARATION  ************/
/***********************************************/

static void ErrorMessageLoadPanel(HPNL hPanel);
static HPNL MakeAutozeroPanel(    HPNL hParentPanel);
static HPNL MakeAutozeroHPPanel(  HPNL hParentPanel);
static HPNL MakeAutozeroLPPanel(  HPNL hParentPanel);
static HPNL MakeAutozeroO2Panel(  HPNL hParentPanel);
HPNL AutoZero(void);
int CVICALLBACK CmdAutoZero(      int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int CVICALLBACK ExitAutoZeroCMD( int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int CVICALLBACK AutoZeroSelect(  int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
static HPNL LoadAutoZeroPanelHP(void);
int CVICALLBACK ExitAutoZeroCMDHP(int panelHandle, int control, int event, void *callbackData, int eventData1, int eventData2);
int CVICALLBACK AutoZeroCMDHP(    int panelHandle, int control, int event, void *callbackData, int eventData1, int eventData2);
static HPNL LoadAutoZeroPanelLP(void);
int CVICALLBACK ExitAutoZeroCMDLP(int panelHandle, int control, int event, void *callbackData, int eventData1, int eventData2);
int CVICALLBACK AutoZeroCMDLP(    int panelHandle, int control, int event, void *callbackData, int eventData1, int eventData2);
static HPNL LoadAutoZeroPanelO2(void);
int CVICALLBACK ExitAutoZeroCMDO2(int panelHandle, int control, int event, void *callbackData, int eventData1, int eventData2);
int CVICALLBACK OxygenTimer(      int panelHandle, int control, int event, void *callbackData, int eventData1, int eventData2);
int CVICALLBACK cmd_Cal_O2_21(    int panelHandle, int control, int event, void *callbackData, int eventData1, int eventData2);
int CVICALLBACK cmd_Cal_O2_100(   int panelHandle, int control, int event, void *callbackData, int eventData1, int eventData2);


static void ErrorMessageLoadPanel(HPNL hPanel)
{
	CPVTMessagePopup(LDBf(Ldb_Error), GetUILErrorString(hPanel), OK_ONLY_DLG, NO_HELP);
}


/*****************************************************************************/
/* Name:    MakeAutozeroPanel                                                     
/*                                                                           
/* Purpose:  Creates the autozero panel
/*                                          
/*                                                                           
/* Example Call:   HPNL MakeAutozeroPanel(HPNL hParentPanel);                                    
/*                                                                           
/* Input:    hParentPanel - the autozero panels parent panel        
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   handle of the new autozero panel                                                         
/*                                                                           
/*****************************************************************************/
static HPNL MakeAutozeroPanel(HPNL hParentPanel)
{
	HPNL hPanel = NULL;
	int error = 0;
	int nBtnWidth;
	int nMaxBtnWidth = CAL_CMD_BTN_WIDTH;
	int nBtnLeft;

	// Create the panel
	errChk(hPanel = NewPanel(hParentPanel, LDBf(Ldb_Calibrate), 30, 10, CAL_PNL_HEIGHT, CAL_PNL_WIDTH));

	// Set the panel's attributes
	errChk(SetPanelAttribute(hPanel, ATTR_BACKCOLOR,    DIALOG_BGND_COLOR));
	errChk(SetPanelAttribute(hPanel, ATTR_SIZABLE,      FALSE));
	errChk(SetPanelAttribute(hPanel, ATTR_MOVABLE,      TRUE));
	errChk(SetPanelAttribute(hPanel, ATTR_CAN_MINIMIZE, FALSE));
	errChk(SetPanelAttribute(hPanel, ATTR_CAN_MAXIMIZE, FALSE));
	if(hParentPanel){
		errChk(SetPanelAttribute(hPanel, ATTR_FRAME_COLOR,        DIALOG_BGND_COLOR));
		errChk(SetPanelAttribute(hPanel, ATTR_TITLEBAR_THICKNESS, 19));
		errChk(SetPanelAttribute(hPanel, ATTR_FRAME_STYLE,        VAL_BEVELLED_FRAME));
		errChk(SetPanelAttribute(hPanel, ATTR_FRAME_THICKNESS,    2));
		errChk(SetPanelAttribute(hPanel, ATTR_TITLE_FONT,         VAL_DIALOG_FONT));
		errChk(SetPanelAttribute(hPanel, ATTR_TITLE_POINT_SIZE,   14));
		errChk(SetPanelAttribute(hPanel, ATTR_TITLE_BOLD,         TRUE));
	}
	if(!hParentPanel){
		errChk(SetPanelAttribute(hPanel, ATTR_FLOATING, FALSE));
	}

	// Build control: AUTOZERO_DECORATION
	errChk(AUTOZERO_DECORATION = NewCtrl(hPanel, CTRL_RECESSED_FRAME, szEmptyString, CAL_BOX_TOP, CAL_BOX_LEFT));
	errChk(SetCtrlAttribute(hPanel, AUTOZERO_DECORATION, ATTR_WIDTH,  CAL_BOX_WIDTH));
	errChk(SetCtrlAttribute(hPanel, AUTOZERO_DECORATION, ATTR_HEIGHT, CAL_BOX_HEIGHT));

	// Build control: AUTOZERO_COMMANDBUTTON_HP
	errChk(AUTOZERO_COMMANDBUTTON_HP = NewCtrl(hPanel, CTRL_SQUARE_COMMAND_BUTTON, LDBf(Ldb_AutoZeroHiPres), CAL_CMD_HPBTN_TOP, CAL_CMD_BTN_LEFT));
	errChk(SetCtrlAttribute(hPanel, AUTOZERO_COMMANDBUTTON_HP, ATTR_CALLBACK_FUNCTION_POINTER, AutoZeroSelect));
	errChk(SetCtrlAttribute(hPanel, AUTOZERO_COMMANDBUTTON_HP, ATTR_WIDTH,            CAL_CMD_BTN_WIDTH));
	errChk(SetCtrlAttribute(hPanel, AUTOZERO_COMMANDBUTTON_HP, ATTR_HEIGHT,           CAL_CMD_BTN_HEIGHT));
	errChk(SetCtrlAttribute(hPanel, AUTOZERO_COMMANDBUTTON_HP, ATTR_LABEL_FONT,       VAL_DIALOG_FONT));
	errChk(SetCtrlAttribute(hPanel, AUTOZERO_COMMANDBUTTON_HP, ATTR_LABEL_POINT_SIZE, 14));
	errChk(SetCtrlAttribute(hPanel, AUTOZERO_COMMANDBUTTON_HP, ATTR_LABEL_BOLD,       TRUE));
	errChk(DefaultCtrl(     hPanel, AUTOZERO_COMMANDBUTTON_HP));

	// Build control: AUTOZERO_COMMANDBUTTON_LP
	errChk(AUTOZERO_COMMANDBUTTON_LP = NewCtrl(hPanel, CTRL_SQUARE_COMMAND_BUTTON, LDBf(Ldb_AutoZeroLoPres), CAL_CMD_LPBTN_TOP, CAL_CMD_BTN_LEFT));
	errChk(SetCtrlAttribute(hPanel, AUTOZERO_COMMANDBUTTON_LP, ATTR_CALLBACK_FUNCTION_POINTER, AutoZeroSelect));
	errChk(SetCtrlAttribute(hPanel, AUTOZERO_COMMANDBUTTON_LP, ATTR_WIDTH,            CAL_CMD_BTN_WIDTH));
	errChk(SetCtrlAttribute(hPanel, AUTOZERO_COMMANDBUTTON_LP, ATTR_HEIGHT,           CAL_CMD_BTN_HEIGHT));
	errChk(SetCtrlAttribute(hPanel, AUTOZERO_COMMANDBUTTON_LP, ATTR_LABEL_FONT,       VAL_DIALOG_FONT));
	errChk(SetCtrlAttribute(hPanel, AUTOZERO_COMMANDBUTTON_LP, ATTR_LABEL_POINT_SIZE, 14));
	errChk(SetCtrlAttribute(hPanel, AUTOZERO_COMMANDBUTTON_LP, ATTR_LABEL_BOLD,       TRUE));
	errChk(DefaultCtrl(     hPanel, AUTOZERO_COMMANDBUTTON_LP));

	// Build control: AUTOZERO_COMMANDBUTTON_O2
	errChk(AUTOZERO_COMMANDBUTTON_O2 = NewCtrl(hPanel, CTRL_SQUARE_COMMAND_BUTTON, LDBf(Ldb_CaliOxygen), CAL_CMD_O2BTN_TOP, CAL_CMD_BTN_LEFT));
	errChk(SetCtrlAttribute(hPanel, AUTOZERO_COMMANDBUTTON_O2, ATTR_CALLBACK_FUNCTION_POINTER, AutoZeroSelect));
	errChk(SetCtrlAttribute(hPanel, AUTOZERO_COMMANDBUTTON_O2, ATTR_WIDTH,            CAL_CMD_BTN_WIDTH));
	errChk(SetCtrlAttribute(hPanel, AUTOZERO_COMMANDBUTTON_O2, ATTR_HEIGHT,           CAL_CMD_BTN_HEIGHT));
	errChk(SetCtrlAttribute(hPanel, AUTOZERO_COMMANDBUTTON_O2, ATTR_LABEL_FONT,       VAL_DIALOG_FONT));
	errChk(SetCtrlAttribute(hPanel, AUTOZERO_COMMANDBUTTON_O2, ATTR_LABEL_POINT_SIZE, 14));
	errChk(SetCtrlAttribute(hPanel, AUTOZERO_COMMANDBUTTON_O2, ATTR_LABEL_BOLD,       TRUE));
	errChk(DefaultCtrl(     hPanel, AUTOZERO_COMMANDBUTTON_O2));

	// Adjust command btn sizes
	GetCtrlAttribute(hPanel, AUTOZERO_COMMANDBUTTON_HP, ATTR_WIDTH, &nBtnWidth);
	if(nBtnWidth > nMaxBtnWidth){
		nMaxBtnWidth = nBtnWidth;
	}
	GetCtrlAttribute(hPanel, AUTOZERO_COMMANDBUTTON_LP, ATTR_WIDTH, &nBtnWidth);
	if(nBtnWidth > nMaxBtnWidth){
		nMaxBtnWidth = nBtnWidth;
	}
	GetCtrlAttribute(hPanel, AUTOZERO_COMMANDBUTTON_O2, ATTR_WIDTH, &nBtnWidth);
	if(nBtnWidth > nMaxBtnWidth){
		nMaxBtnWidth = nBtnWidth;
	}
	nBtnLeft = ((CAL_PNL_WIDTH - (nMaxBtnWidth)) / 2);
	SetCtrlAttribute(hPanel, AUTOZERO_COMMANDBUTTON_HP, ATTR_WIDTH, nMaxBtnWidth);
	SetCtrlAttribute(hPanel, AUTOZERO_COMMANDBUTTON_HP, ATTR_LEFT,  nBtnLeft);
	SetCtrlAttribute(hPanel, AUTOZERO_COMMANDBUTTON_LP, ATTR_WIDTH, nMaxBtnWidth);
	SetCtrlAttribute(hPanel, AUTOZERO_COMMANDBUTTON_LP, ATTR_LEFT,  nBtnLeft);
	SetCtrlAttribute(hPanel, AUTOZERO_COMMANDBUTTON_O2, ATTR_WIDTH, nMaxBtnWidth);
	SetCtrlAttribute(hPanel, AUTOZERO_COMMANDBUTTON_O2, ATTR_LEFT,  nBtnLeft);

	// Build Cancel bitmap button
	errChk(AUTOZERO_COMMAND_CANCEL = NewCtrl(hPanel, CTRL_PICTURE_COMMAND_BUTTON, szEmptyString, CAL_OK_TOP, CAL_XIT_LEFT));					
	errChk(SetPanelAttribute(hPanel, ATTR_CLOSE_CTRL,         AUTOZERO_COMMAND_CANCEL));
	errChk(SetCtrlAttribute( hPanel, AUTOZERO_COMMAND_CANCEL, ATTR_CALLBACK_FUNCTION_POINTER, ExitAutoZeroCMD));
	errChk(SetCtrlAttribute( hPanel, AUTOZERO_COMMAND_CANCEL, ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT));
	errChk(SetCtrlAttribute( hPanel, AUTOZERO_COMMAND_CANCEL, ATTR_FIT_MODE,         VAL_SIZE_TO_IMAGE));
	errChk(setButtonBMP(     hPanel, AUTOZERO_COMMAND_CANCEL, BMP_Cancel192));

	// Set up ZPLANE order
	errChk(SetCtrlAttribute(hPanel, AUTOZERO_COMMANDBUTTON_O2, ATTR_ZPLANE_POSITION, 0));
	errChk(SetCtrlAttribute(hPanel, AUTOZERO_COMMANDBUTTON_LP, ATTR_ZPLANE_POSITION, 1));
	errChk(SetCtrlAttribute(hPanel, AUTOZERO_COMMANDBUTTON_HP, ATTR_ZPLANE_POSITION, 2));
	errChk(SetCtrlAttribute(hPanel, AUTOZERO_DECORATION,       ATTR_ZPLANE_POSITION, 3));
	errChk(SetCtrlAttribute(hPanel, AUTOZERO_COMMAND_CANCEL,   ATTR_ZPLANE_POSITION, 4));

	// Set up TAB order
	errChk(SetCtrlAttribute(hPanel, AUTOZERO_COMMANDBUTTON_HP, ATTR_CTRL_TAB_POSITION, 0));
	errChk(SetCtrlAttribute(hPanel, AUTOZERO_COMMANDBUTTON_LP, ATTR_CTRL_TAB_POSITION, 1));
	errChk(SetCtrlAttribute(hPanel, AUTOZERO_COMMANDBUTTON_O2, ATTR_CTRL_TAB_POSITION, 2));
	errChk(SetCtrlAttribute(hPanel, AUTOZERO_COMMAND_CANCEL,   ATTR_CTRL_TAB_POSITION, 3));

	// Finalize panel colors, positioning, and sizing
	errChk(SetPanelAttribute(hPanel, ATTR_CONFORM_TO_SYSTEM, TRUE));
	errChk(SetPanelAttribute(hPanel, ATTR_TOP,               VAL_AUTO_CENTER));
	errChk(SetPanelAttribute(hPanel, ATTR_LEFT,              VAL_AUTO_CENTER));

	//We're done!
	return hPanel;
Error:
	return error;
}


/*****************************************************************************/
/* Name:     MakeAutozeroHPPanel                                                  
/*                                                                           
/* Purpose:  creates the autozero high pressure panel.
/*                                          
/*                                                                           
/* Example Call:   HPNL MakeAutozeroHPPanel(HPNL hParentPanel);                                    
/*                                                                           
/* Input:    hParentPanel - the new panels parent     
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   handle or standard UI error code.                                                      
/*                                                                           
/*****************************************************************************/
static HPNL MakeAutozeroHPPanel(HPNL hParentPanel)
{
	HPNL hPanel;
	int error = 0;

	// Create the panel
	errChk(hPanel = NewPanel(hParentPanel, LDBf(Ldb_CaliHiPres), 30, 10, CAL_PNL_HEIGHT, CAL_PNL_WIDTH));

	// Set the panel's attributes
	errChk(SetPanelAttribute(hPanel, ATTR_BACKCOLOR,    DIALOG_BGND_COLOR));
	errChk(SetPanelAttribute(hPanel, ATTR_SIZABLE,      FALSE));
	errChk(SetPanelAttribute(hPanel, ATTR_MOVABLE,      TRUE));
	errChk(SetPanelAttribute(hPanel, ATTR_CAN_MINIMIZE, FALSE));
	errChk(SetPanelAttribute(hPanel, ATTR_CAN_MAXIMIZE, FALSE));
	if(hParentPanel){
		errChk(SetPanelAttribute(hPanel, ATTR_FRAME_COLOR,        VAL_GRAY));
		errChk(SetPanelAttribute(hPanel, ATTR_TITLEBAR_THICKNESS, 19));
		errChk(SetPanelAttribute(hPanel, ATTR_FRAME_STYLE,        VAL_BEVELLED_FRAME));
		errChk(SetPanelAttribute(hPanel, ATTR_FRAME_THICKNESS,    2));
		errChk(SetPanelAttribute(hPanel, ATTR_TITLE_FONT,         VAL_DIALOG_FONT));
		errChk(SetPanelAttribute(hPanel, ATTR_TITLE_POINT_SIZE,   14));
		errChk(SetPanelAttribute(hPanel, ATTR_TITLE_BOLD,         TRUE));
	}
	if(!hParentPanel){
		errChk(SetPanelAttribute(hPanel, ATTR_FLOATING, FALSE));
	}

	// Build control: AUTOZEROHP_DECORATION
	errChk(AUTOZEROHP_DECORATION = NewCtrl(hPanel, CTRL_RECESSED_FRAME, szEmptyString, CAL_BOX_TOP, CAL_BOX_LEFT));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROHP_DECORATION, ATTR_WIDTH,     CAL_BOX_WIDTH));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROHP_DECORATION, ATTR_HEIGHT,    CAL_BOX_HEIGHT));

	// Build control: AUTOZEROHP_INSTRUCT_LINE_1
	errChk(AUTOZEROHP_INSTRUCT_LINE_1 = NewCtrl(hPanel, CTRL_TEXT_MSG, szEmptyString, CAL_INSTRUCT_LINE1_TOP, CAL_INSTRUCT_LEFT));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROHP_INSTRUCT_LINE_1, ATTR_WIDTH,           CAL_INSTRUCT_WIDTH));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROHP_INSTRUCT_LINE_1, ATTR_HEIGHT,          18));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROHP_INSTRUCT_LINE_1, ATTR_CTRL_MODE,       VAL_INDICATOR));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROHP_INSTRUCT_LINE_1, ATTR_TEXT_FONT,       VAL_DIALOG_FONT));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROHP_INSTRUCT_LINE_1, ATTR_TEXT_POINT_SIZE, 14));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROHP_INSTRUCT_LINE_1, ATTR_TEXT_BGCOLOR,    DIALOG_BGND_COLOR));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROHP_INSTRUCT_LINE_1, ATTR_DFLT_VALUE,      LDBf(Ldb_Discon_Hi_Instr)));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROHP_INSTRUCT_LINE_1, ATTR_SIZE_TO_TEXT,    TRUE));
	errChk(DefaultCtrl(     hPanel, AUTOZEROHP_INSTRUCT_LINE_1));

	// Build control: AUTOZEROHP_INSTRUCT_LINE_2
	errChk(AUTOZEROHP_INSTRUCT_LINE_2 = NewCtrl(hPanel, CTRL_TEXT_MSG, szEmptyString, CAL_INSTRUCT_LINE2_TOP, CAL_INSTRUCT_LEFT));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROHP_INSTRUCT_LINE_2, ATTR_WIDTH,           CAL_INSTRUCT_WIDTH));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROHP_INSTRUCT_LINE_2, ATTR_HEIGHT,          18));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROHP_INSTRUCT_LINE_2, ATTR_CTRL_MODE,       VAL_INDICATOR));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROHP_INSTRUCT_LINE_2, ATTR_TEXT_FONT,       VAL_DIALOG_FONT));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROHP_INSTRUCT_LINE_2, ATTR_TEXT_POINT_SIZE, 14));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROHP_INSTRUCT_LINE_2, ATTR_TEXT_BGCOLOR,    DIALOG_BGND_COLOR));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROHP_INSTRUCT_LINE_2, ATTR_DFLT_VALUE,      LDBf(Ldb_OK_When_Ready)));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROHP_INSTRUCT_LINE_2, ATTR_SIZE_TO_TEXT,    TRUE));
	errChk(DefaultCtrl(     hPanel, AUTOZEROHP_INSTRUCT_LINE_2));

	// Build control: AUTOZEROHP_STATUS
	errChk(AUTOZEROHP_STATUS = NewCtrl(hPanel, CTRL_TEXT_MSG, szEmptyString, 115, CAL_STATUS_LEFT));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROHP_STATUS, ATTR_WIDTH,           10));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROHP_STATUS, ATTR_HEIGHT,          18));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROHP_STATUS, ATTR_CTRL_MODE,       VAL_INDICATOR));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROHP_STATUS, ATTR_TEXT_FONT,       VAL_DIALOG_FONT));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROHP_STATUS, ATTR_TEXT_POINT_SIZE, 14));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROHP_STATUS, ATTR_TEXT_BGCOLOR,    DIALOG_BGND_COLOR));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROHP_STATUS, ATTR_DFLT_VALUE,      szEmptyString));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROHP_STATUS, ATTR_SIZE_TO_TEXT,    TRUE));
	errChk(DefaultCtrl(     hPanel, AUTOZEROHP_STATUS));

	// Build control: AUTOZEROHP_COMMANDBUTTON_OK
	errChk(AUTOZEROHP_COMMANDBUTTON_OK = NewCtrl(hPanel, CTRL_PICTURE_COMMAND_BUTTON, "", CAL_OK_TOP, CAL_OK_LEFT));					
	errChk(SetCtrlAttribute(hPanel, AUTOZEROHP_COMMANDBUTTON_OK, ATTR_CALLBACK_FUNCTION_POINTER, AutoZeroCMDHP));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROHP_COMMANDBUTTON_OK, ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROHP_COMMANDBUTTON_OK, ATTR_FIT_MODE,         VAL_SIZE_TO_IMAGE));
	errChk(setButtonBMP(    hPanel, AUTOZEROHP_COMMANDBUTTON_OK, BMP_Okay192));	
	errChk(DefaultCtrl(     hPanel, AUTOZEROHP_COMMANDBUTTON_OK));

	// Build control: AUTOZEROHP_COMMAND_CANCEL
	errChk(AUTOZEROHP_COMMAND_CANCEL = NewCtrl(hPanel,CTRL_PICTURE_COMMAND_BUTTON,"", CAL_OK_TOP, CAL_XIT_LEFT));					
	errChk(SetPanelAttribute(hPanel, ATTR_CLOSE_CTRL,           AUTOZEROHP_COMMAND_CANCEL));
	errChk(SetCtrlAttribute( hPanel, AUTOZEROHP_COMMAND_CANCEL, ATTR_CALLBACK_FUNCTION_POINTER, ExitAutoZeroCMDHP));
	errChk(SetCtrlAttribute( hPanel, AUTOZEROHP_COMMAND_CANCEL, ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT));
	errChk(SetCtrlAttribute( hPanel, AUTOZEROHP_COMMAND_CANCEL, ATTR_FIT_MODE,         VAL_SIZE_TO_IMAGE));
	errChk(setButtonBMP(     hPanel, AUTOZEROHP_COMMAND_CANCEL, BMP_Cancel192));
   
	// Set up ZPLANE order
	errChk(SetCtrlAttribute(hPanel, AUTOZEROHP_STATUS,           ATTR_ZPLANE_POSITION, 0));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROHP_INSTRUCT_LINE_2,  ATTR_ZPLANE_POSITION, 1));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROHP_INSTRUCT_LINE_1,  ATTR_ZPLANE_POSITION, 2));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROHP_COMMANDBUTTON_OK, ATTR_ZPLANE_POSITION, 3));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROHP_DECORATION,       ATTR_ZPLANE_POSITION, 4));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROHP_COMMAND_CANCEL,   ATTR_ZPLANE_POSITION, 5));

	// Set up TAB order
	errChk(SetCtrlAttribute(hPanel, AUTOZEROHP_COMMANDBUTTON_OK, ATTR_CTRL_TAB_POSITION, 0));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROHP_COMMAND_CANCEL,   ATTR_CTRL_TAB_POSITION, 1));

	// Finalize panel colors, positioning, and sizing
	errChk(SetPanelAttribute(hPanel, ATTR_CONFORM_TO_SYSTEM, TRUE));
	errChk(SetPanelAttribute(hPanel, ATTR_TOP,               VAL_AUTO_CENTER));
	errChk(SetPanelAttribute(hPanel, ATTR_LEFT,              VAL_AUTO_CENTER));

	//We're done!
	return hPanel;
Error:
	return error;
}


/*****************************************************************************/
/* Name:    MakeAutozeroLPPanel                                                      
/*                                                                           
/* Purpose:   Build UI object AUTOZEROLP
/*                                          
/*                                                                           
/* Example Call:   HPNL MakeAutozeroLPPanel(HPNL hParentPanel);                                    
/*                                                                           
/* Input:    hParentPanel -	new panel's parent          
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   handle or standard UI error code.                                                   
/*                                                                           
/*****************************************************************************/
static HPNL MakeAutozeroLPPanel(HPNL hParentPanel)
{
	HPNL hPanel;
	int error = 0;

	// Create the panel
	errChk(hPanel = NewPanel(hParentPanel, LDBf(Ldb_CaliLoPres), 30, 10, CAL_PNL_HEIGHT, CAL_PNL_WIDTH));

	// Set the panel's attributes
	errChk(SetPanelAttribute(hPanel, ATTR_BACKCOLOR,    DIALOG_BGND_COLOR));
	errChk(SetPanelAttribute(hPanel, ATTR_SIZABLE,      FALSE));
	errChk(SetPanelAttribute(hPanel, ATTR_MOVABLE,      TRUE));
	errChk(SetPanelAttribute(hPanel, ATTR_CAN_MINIMIZE, FALSE));
	errChk(SetPanelAttribute(hPanel, ATTR_CAN_MAXIMIZE, FALSE));

	if(hParentPanel){
		errChk(SetPanelAttribute(hPanel, ATTR_FRAME_COLOR,        VAL_GRAY));
		errChk(SetPanelAttribute(hPanel, ATTR_TITLEBAR_THICKNESS, 19));
		errChk(SetPanelAttribute(hPanel, ATTR_FRAME_STYLE,        VAL_BEVELLED_FRAME));
		errChk(SetPanelAttribute(hPanel, ATTR_FRAME_THICKNESS,    2));
		errChk(SetPanelAttribute(hPanel, ATTR_TITLE_FONT,         VAL_DIALOG_FONT));
		errChk(SetPanelAttribute(hPanel, ATTR_TITLE_POINT_SIZE,   14));
		errChk(SetPanelAttribute(hPanel, ATTR_TITLE_BOLD,         TRUE));
	}
	else{
		errChk(SetPanelAttribute(hPanel, ATTR_FLOATING, FALSE));
	
	}

	// Build control: AUTOZEROLP_DECORATION
	errChk(AUTOZEROLP_DECORATION = NewCtrl(hPanel, CTRL_RECESSED_FRAME, szEmptyString, CAL_BOX_TOP, CAL_BOX_LEFT));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROLP_DECORATION, ATTR_WIDTH,  CAL_BOX_WIDTH));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROLP_DECORATION, ATTR_HEIGHT, CAL_BOX_HEIGHT));

	// Build control: AUTOZEROLP_INSTRUCT_LINE_1
	errChk(AUTOZEROLP_INSTRUCT_LINE_1 = NewCtrl(hPanel, CTRL_TEXT_MSG, szEmptyString, CAL_INSTRUCT_LINE1_TOP, CAL_INSTRUCT_LEFT));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROLP_INSTRUCT_LINE_1, ATTR_WIDTH,           CAL_INSTRUCT_WIDTH));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROLP_INSTRUCT_LINE_1, ATTR_HEIGHT,          18));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROLP_INSTRUCT_LINE_1, ATTR_CTRL_MODE,       VAL_INDICATOR));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROLP_INSTRUCT_LINE_1, ATTR_TEXT_FONT,       VAL_DIALOG_FONT));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROLP_INSTRUCT_LINE_1, ATTR_TEXT_POINT_SIZE, 14));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROLP_INSTRUCT_LINE_1, ATTR_TEXT_BGCOLOR,    DIALOG_BGND_COLOR));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROLP_INSTRUCT_LINE_1, ATTR_DFLT_VALUE,      LDBf(Ldb_Discon_Lo_Instr)));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROLP_INSTRUCT_LINE_1, ATTR_SIZE_TO_TEXT,    TRUE));
	errChk(DefaultCtrl(     hPanel, AUTOZEROLP_INSTRUCT_LINE_1));

	// Build control: AUTOZEROLP_INSTRUCT_LINE_2
	errChk(AUTOZEROLP_INSTRUCT_LINE_2 = NewCtrl(hPanel, CTRL_TEXT_MSG, szEmptyString, CAL_INSTRUCT_LINE2_TOP, CAL_INSTRUCT_LEFT));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROLP_INSTRUCT_LINE_2, ATTR_WIDTH,           CAL_INSTRUCT_WIDTH));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROLP_INSTRUCT_LINE_2, ATTR_HEIGHT,          18));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROLP_INSTRUCT_LINE_2, ATTR_CTRL_MODE,       VAL_INDICATOR));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROLP_INSTRUCT_LINE_2, ATTR_TEXT_FONT,       VAL_DIALOG_FONT));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROLP_INSTRUCT_LINE_2, ATTR_TEXT_POINT_SIZE, 14));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROLP_INSTRUCT_LINE_2, ATTR_TEXT_BGCOLOR,    DIALOG_BGND_COLOR));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROLP_INSTRUCT_LINE_2, ATTR_DFLT_VALUE,      LDBf(Ldb_OK_When_Ready)));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROLP_INSTRUCT_LINE_2, ATTR_SIZE_TO_TEXT,    TRUE));
	errChk(DefaultCtrl(     hPanel, AUTOZEROLP_INSTRUCT_LINE_2));

	// Build control: AUTOZEROLP_STATUS
	errChk(AUTOZEROLP_STATUS = NewCtrl(hPanel, CTRL_TEXT_MSG, szEmptyString, 115, CAL_STATUS_LEFT));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROLP_STATUS, ATTR_WIDTH,           10));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROLP_STATUS, ATTR_HEIGHT,          18));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROLP_STATUS, ATTR_CTRL_MODE,       VAL_INDICATOR));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROLP_STATUS, ATTR_TEXT_FONT,       VAL_DIALOG_FONT));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROLP_STATUS, ATTR_TEXT_POINT_SIZE, 14));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROLP_STATUS, ATTR_TEXT_BGCOLOR,    DIALOG_BGND_COLOR));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROLP_STATUS, ATTR_DFLT_VALUE,      szEmptyString));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROLP_STATUS, ATTR_SIZE_TO_TEXT,    TRUE));
	errChk(DefaultCtrl(     hPanel, AUTOZEROLP_STATUS));

	// Build control: AUTOZEROLP_COMMANDBUTTON_OK
	errChk(AUTOZEROLP_COMMANDBUTTON_OK = NewCtrl(hPanel,CTRL_PICTURE_COMMAND_BUTTON,"", CAL_OK_TOP, CAL_OK_LEFT));					
	errChk(SetCtrlAttribute(hPanel, AUTOZEROLP_COMMANDBUTTON_OK, ATTR_CALLBACK_FUNCTION_POINTER, AutoZeroCMDLP));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROLP_COMMANDBUTTON_OK, ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROLP_COMMANDBUTTON_OK, ATTR_FIT_MODE,         VAL_SIZE_TO_IMAGE));
	errChk(setButtonBMP(    hPanel, AUTOZEROLP_COMMANDBUTTON_OK, BMP_Okay192));
	errChk(DefaultCtrl(     hPanel, AUTOZEROLP_COMMANDBUTTON_OK));

	// Build control: AUTOZEROLP_COMMAND_CANCEL
	errChk(AUTOZEROLP_COMMAND_CANCEL = NewCtrl(hPanel,CTRL_PICTURE_COMMAND_BUTTON,"", CAL_OK_TOP, CAL_XIT_LEFT));					
	errChk(SetPanelAttribute(hPanel, ATTR_CLOSE_CTRL,           AUTOZEROLP_COMMAND_CANCEL));
	errChk(SetCtrlAttribute( hPanel, AUTOZEROLP_COMMAND_CANCEL, ATTR_CALLBACK_FUNCTION_POINTER, ExitAutoZeroCMDLP));
	errChk(SetCtrlAttribute( hPanel, AUTOZEROLP_COMMAND_CANCEL, ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT));
	errChk(SetCtrlAttribute( hPanel, AUTOZEROLP_COMMAND_CANCEL, ATTR_FIT_MODE,         VAL_SIZE_TO_IMAGE));
	errChk(setButtonBMP(     hPanel, AUTOZEROLP_COMMAND_CANCEL, BMP_Cancel192));

	// Set up ZPLANE order
	errChk(SetCtrlAttribute(hPanel, AUTOZEROLP_STATUS,           ATTR_ZPLANE_POSITION, 0));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROLP_INSTRUCT_LINE_2,  ATTR_ZPLANE_POSITION, 1));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROLP_INSTRUCT_LINE_1,  ATTR_ZPLANE_POSITION, 2));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROLP_COMMANDBUTTON_OK, ATTR_ZPLANE_POSITION, 3));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROLP_DECORATION,       ATTR_ZPLANE_POSITION, 4));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROLP_COMMAND_CANCEL,   ATTR_ZPLANE_POSITION, 5));

	// Set up TAB order
	errChk(SetCtrlAttribute(hPanel, AUTOZEROLP_COMMANDBUTTON_OK, ATTR_CTRL_TAB_POSITION, 0));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROLP_COMMAND_CANCEL,   ATTR_CTRL_TAB_POSITION, 1));

	// Finalize panel colors, positioning, and sizing
	errChk(SetPanelAttribute(hPanel, ATTR_CONFORM_TO_SYSTEM, TRUE));
	errChk(SetPanelAttribute(hPanel, ATTR_TOP,               VAL_AUTO_CENTER));
	errChk(SetPanelAttribute(hPanel, ATTR_LEFT,              VAL_AUTO_CENTER));

	//We're done!
	return hPanel;
Error:
	return error;
}


/*****************************************************************************/
/* Name:     MakeAutozeroO2Panel                                               
/*                                                                           
/* Purpose:  Build UI object AUTOZEROO2.
/*                                          
/*                                                                           
/* Example Call:  HPNL MakeAutozeroO2Panel(HPNL hParentPanel);                                    
/*                                                                           
/* Input:    hParentPanel -	new panel's parent
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   handle or standard UI error code.                                                       
/*                                                                           
/*****************************************************************************/
static HPNL MakeAutozeroO2Panel(HPNL hParentPanel)
{
	HPNL hPanel;
	int error = 0;
	char cBuf[512];
	int nBtnWidth;
	int nMaxBtnWidth = O2_CMD021_WIDTH;
	int nTextLeft;

	// Create the panel
	errChk(hPanel = NewPanel(hParentPanel, LDBf(Ldb_CaliOxygen), 30, 10, CAL_PNL_HEIGHT, CAL_PNL_WIDTH));

	// Set the panel's attributes
	errChk(SetPanelAttribute(hPanel, ATTR_BACKCOLOR,    DIALOG_BGND_COLOR));
	errChk(SetPanelAttribute(hPanel, ATTR_SIZABLE,      FALSE));
	errChk(SetPanelAttribute(hPanel, ATTR_MOVABLE,      TRUE));
	errChk(SetPanelAttribute(hPanel, ATTR_CAN_MINIMIZE, FALSE));
	errChk(SetPanelAttribute(hPanel, ATTR_CAN_MAXIMIZE, FALSE));
	if(hParentPanel){
		errChk(SetPanelAttribute(hPanel, ATTR_FRAME_COLOR,        VAL_GRAY));
		errChk(SetPanelAttribute(hPanel, ATTR_TITLEBAR_THICKNESS, 19));
		errChk(SetPanelAttribute(hPanel, ATTR_FRAME_STYLE,        VAL_BEVELLED_FRAME));
		errChk(SetPanelAttribute(hPanel, ATTR_FRAME_THICKNESS,    2));
		errChk(SetPanelAttribute(hPanel, ATTR_TITLE_FONT,         VAL_DIALOG_FONT));
		errChk(SetPanelAttribute(hPanel, ATTR_TITLE_POINT_SIZE,   14));
		errChk(SetPanelAttribute(hPanel, ATTR_TITLE_BOLD,         TRUE));
	}
	else{
		errChk(SetPanelAttribute(hPanel, ATTR_FLOATING, FALSE));
	}

	// Build control: AUTOZEROO2_DECORATION
	errChk(AUTOZEROO2_DECORATION = NewCtrl(hPanel, CTRL_RECESSED_FRAME, szEmptyString, CAL_BOX_TOP, CAL_BOX_LEFT));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_DECORATION, ATTR_WIDTH,     CAL_BOX_WIDTH));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_DECORATION, ATTR_HEIGHT,    CAL_BOX_HEIGHT));

	// Build control: AUTOZEROO2_CAL_O2_TEXT
	errChk(AUTOZEROO2_CAL_O2_TEXT = NewCtrl(hPanel, CTRL_TEXT_MSG, szEmptyString, 29, 27));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_CAL_O2_TEXT, ATTR_WIDTH,           O2_INSTRUCT_WIDTH));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_CAL_O2_TEXT, ATTR_HEIGHT,          O2_INSTRUCT_HEIGHT));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_CAL_O2_TEXT, ATTR_CTRL_MODE,       VAL_INDICATOR));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_CAL_O2_TEXT, ATTR_TEXT_FONT,       VAL_DIALOG_FONT));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_CAL_O2_TEXT, ATTR_TEXT_POINT_SIZE, 14));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_CAL_O2_TEXT, ATTR_TEXT_BGCOLOR,    DIALOG_BGND_COLOR));

	sprintf(cBuf, "%s%s%s", LDBf(Ldb_AutoZero_Cbuf1), LDBf(Ldb_AutoZero_Cbuf2), LDBf(Ldb_AutoZero_Cbuf3));

	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_CAL_O2_TEXT, ATTR_DFLT_VALUE, cBuf));
	errChk(DefaultCtrl(     hPanel, AUTOZEROO2_CAL_O2_TEXT));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_CAL_O2_TEXT, ATTR_SIZE_TO_TEXT, FALSE));

	// Build control: AUTOZEROO2_CMD_CAL_O2_21
	errChk(AUTOZEROO2_CMD_CAL_O2_21 = NewCtrl(hPanel, CTRL_SQUARE_COMMAND_BUTTON, LDBf(Ldb_Adj_21_O2), O2_CMD021_TOP, O2_CMD021_LEFT));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_CMD_CAL_O2_21, ATTR_CALLBACK_FUNCTION_POINTER, cmd_Cal_O2_21));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_CMD_CAL_O2_21, ATTR_WIDTH,            O2_CMD021_WIDTH));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_CMD_CAL_O2_21, ATTR_HEIGHT,           O2_CMD021_HEIGHT));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_CMD_CAL_O2_21, ATTR_LABEL_FONT,       VAL_DIALOG_FONT));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_CMD_CAL_O2_21, ATTR_LABEL_POINT_SIZE, 14));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_CMD_CAL_O2_21, ATTR_LABEL_BOLD,       TRUE));
	errChk(DefaultCtrl(hPanel, AUTOZEROO2_CMD_CAL_O2_21));

	// Build control: AUTOZEROO2_CMD_CAL_O2_100
	errChk(AUTOZEROO2_CMD_CAL_O2_100 = NewCtrl(hPanel, CTRL_SQUARE_COMMAND_BUTTON, LDBf(Ldb_Adj_100_O2), O2_CMD100_TOP, O2_CMD100_LEFT));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_CMD_CAL_O2_100, ATTR_CALLBACK_FUNCTION_POINTER, cmd_Cal_O2_100));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_CMD_CAL_O2_100, ATTR_WIDTH,            O2_CMD100_WIDTH));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_CMD_CAL_O2_100, ATTR_HEIGHT,           O2_CMD100_HEIGHT));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_CMD_CAL_O2_100, ATTR_LABEL_FONT,       VAL_DIALOG_FONT));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_CMD_CAL_O2_100, ATTR_LABEL_POINT_SIZE, 14));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_CMD_CAL_O2_100, ATTR_LABEL_BOLD,       TRUE));
	errChk(DefaultCtrl(hPanel, AUTOZEROO2_CMD_CAL_O2_100));

	// Build control: AUTOZEROO2_STATUS_21
	errChk(AUTOZEROO2_STATUS_21 = NewCtrl(hPanel, CTRL_TEXT_MSG, szEmptyString, O2_STATUS021_TOP, O2_STATUS021_LEFT));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_STATUS_21, ATTR_WIDTH,           10));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_STATUS_21, ATTR_HEIGHT,          18));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_STATUS_21, ATTR_CTRL_MODE,       VAL_INDICATOR));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_STATUS_21, ATTR_TEXT_FONT,       VAL_DIALOG_FONT));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_STATUS_21, ATTR_TEXT_POINT_SIZE, 14));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_STATUS_21, ATTR_TEXT_BGCOLOR,    DIALOG_BGND_COLOR));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_STATUS_21, ATTR_DFLT_VALUE,      szEmptyString));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_STATUS_21, ATTR_SIZE_TO_TEXT,    TRUE));
	errChk(DefaultCtrl(     hPanel, AUTOZEROO2_STATUS_21));

	// Build control: AUTOZEROO2_STATUS_100
	errChk(AUTOZEROO2_STATUS_100 = NewCtrl(hPanel, CTRL_TEXT_MSG, szEmptyString, O2_STATUS100_TOP, O2_STATUS100_LEFT));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_STATUS_100, ATTR_WIDTH,           10));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_STATUS_100, ATTR_HEIGHT,          18));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_STATUS_100, ATTR_CTRL_MODE,       VAL_INDICATOR));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_STATUS_100, ATTR_TEXT_FONT,       VAL_DIALOG_FONT));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_STATUS_100, ATTR_TEXT_POINT_SIZE, 14));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_STATUS_100, ATTR_TEXT_BGCOLOR,    DIALOG_BGND_COLOR));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_STATUS_100, ATTR_DFLT_VALUE,      szEmptyString));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_STATUS_100, ATTR_SIZE_TO_TEXT,    TRUE));
	errChk(DefaultCtrl(     hPanel, AUTOZEROO2_STATUS_100));

	// Adjust command btn sizes
	GetCtrlAttribute(hPanel, AUTOZEROO2_CMD_CAL_O2_21, ATTR_WIDTH, &nBtnWidth);
	if(nBtnWidth > nMaxBtnWidth){
		nMaxBtnWidth = nBtnWidth;
	}
	GetCtrlAttribute(hPanel, AUTOZEROO2_CMD_CAL_O2_100, ATTR_WIDTH, &nBtnWidth);
	if(nBtnWidth > nMaxBtnWidth){
		nMaxBtnWidth = nBtnWidth;
	}
	nTextLeft = O2_CMD021_LEFT + nMaxBtnWidth + 4;

	SetCtrlAttribute(hPanel, AUTOZEROO2_CMD_CAL_O2_21, ATTR_WIDTH, nMaxBtnWidth);
	SetCtrlAttribute(hPanel, AUTOZEROO2_STATUS_21,     ATTR_LEFT,  nTextLeft);

	SetCtrlAttribute(hPanel, AUTOZEROO2_CMD_CAL_O2_100, ATTR_WIDTH, nMaxBtnWidth);
	SetCtrlAttribute(hPanel, AUTOZEROO2_STATUS_100,     ATTR_LEFT,  nTextLeft);

	// Build control: AUTOZEROO2_TIMER
	errChk(AUTOZEROO2_TIMER = NewCtrl(hPanel, CTRL_TIMER, szEmptyString, CAL_OK_TOP, CAL_BOX_LEFT));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_TIMER, ATTR_CALLBACK_FUNCTION_POINTER, OxygenTimer));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_TIMER, ATTR_WIDTH,              38));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_TIMER, ATTR_HEIGHT,             38));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_TIMER, ATTR_CTRL_MODE,          VAL_INDICATOR));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_TIMER, ATTR_LABEL_LEFT,         CAL_BOX_LEFT));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_TIMER, ATTR_LABEL_TOP,          CAL_OK_TOP - 15));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_TIMER, ATTR_LABEL_WIDTH,        9));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_TIMER, ATTR_LABEL_HEIGHT,       15));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_TIMER, ATTR_LABEL_BGCOLOR,      DIALOG_BGND_COLOR));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_TIMER, ATTR_LABEL_SIZE_TO_TEXT, TRUE));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_TIMER, ATTR_ENABLED,            FALSE));

	// Build control: AUTOZEROO2_PERCENT_O2_LBL
	errChk(AUTOZEROO2_PERCENT_O2_LBL = NewCtrl(hPanel, CTRL_TEXT_MSG, szEmptyString, O2_LBL_TOP, O2_LBL_LEFT));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_PERCENT_O2_LBL, ATTR_WIDTH,           70));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_PERCENT_O2_LBL, ATTR_HEIGHT,          18));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_PERCENT_O2_LBL, ATTR_CTRL_MODE,       VAL_INDICATOR));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_PERCENT_O2_LBL, ATTR_TEXT_FONT,       VAL_DIALOG_FONT));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_PERCENT_O2_LBL, ATTR_TEXT_POINT_SIZE, 14));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_PERCENT_O2_LBL, ATTR_TEXT_BGCOLOR,    VAL_TRANSPARENT));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_PERCENT_O2_LBL, ATTR_DFLT_VALUE,      LDBf(Ldb_O2_prcnt)));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_PERCENT_O2_LBL, ATTR_SIZE_TO_TEXT,    FALSE));
	errChk(DefaultCtrl(     hPanel, AUTOZEROO2_PERCENT_O2_LBL));

	// Build control: AUTOZEROO2_PERCENT_O2
	errChk(AUTOZEROO2_PERCENT_O2 = NewCtrl(hPanel, CTRL_TEXT_MSG, szEmptyString, O2_PCT_TOP, O2_PCT_LEFT));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_PERCENT_O2, ATTR_WIDTH,           70));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_PERCENT_O2, ATTR_HEIGHT,          31));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_PERCENT_O2, ATTR_CTRL_MODE,       VAL_INDICATOR));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_PERCENT_O2, ATTR_TEXT_FONT,       VAL_DIALOG_FONT));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_PERCENT_O2, ATTR_TEXT_POINT_SIZE, 24));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_PERCENT_O2, ATTR_TEXT_BGCOLOR,    VAL_TRANSPARENT));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_PERCENT_O2, ATTR_DFLT_VALUE,      szEmptyString));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_PERCENT_O2, ATTR_SIZE_TO_TEXT,    FALSE));
	errChk(DefaultCtrl(     hPanel, AUTOZEROO2_PERCENT_O2));

	// Build control: AUTOZEROO2_COMMAND_CANCEL
	errChk(AUTOZEROO2_COMMAND_CANCEL = NewCtrl(hPanel,CTRL_PICTURE_COMMAND_BUTTON,"", CAL_OK_TOP, CAL_XIT_LEFT));					
	errChk(SetPanelAttribute(hPanel, ATTR_CLOSE_CTRL,           AUTOZEROO2_COMMAND_CANCEL));
	errChk(SetCtrlAttribute( hPanel, AUTOZEROO2_COMMAND_CANCEL, ATTR_CALLBACK_FUNCTION_POINTER, ExitAutoZeroCMDO2));
	errChk(SetCtrlAttribute( hPanel, AUTOZEROO2_COMMAND_CANCEL, ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT));
	errChk(SetCtrlAttribute( hPanel, AUTOZEROO2_COMMAND_CANCEL, ATTR_FIT_MODE,         VAL_SIZE_TO_IMAGE));
	errChk(setButtonBMP(     hPanel, AUTOZEROO2_COMMAND_CANCEL, BMP_Cancel192));

	// Set up ZPLANE order if necessary
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_TIMER,          ATTR_ZPLANE_POSITION, 0));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_PERCENT_O2,     ATTR_ZPLANE_POSITION, 1));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_PERCENT_O2_LBL, ATTR_ZPLANE_POSITION, 2));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_STATUS_21,      ATTR_ZPLANE_POSITION, 3));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_STATUS_100,     ATTR_ZPLANE_POSITION, 4));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_CMD_CAL_O2_100, ATTR_ZPLANE_POSITION, 5));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_CMD_CAL_O2_21,  ATTR_ZPLANE_POSITION, 6));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_CAL_O2_TEXT,    ATTR_ZPLANE_POSITION, 7));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_DECORATION,     ATTR_ZPLANE_POSITION, 8));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_COMMAND_CANCEL, ATTR_ZPLANE_POSITION, 9));

	// Set up TAB order if necessary
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_CMD_CAL_O2_21,  ATTR_CTRL_TAB_POSITION, 0));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_CMD_CAL_O2_100, ATTR_CTRL_TAB_POSITION, 1));
	errChk(SetCtrlAttribute(hPanel, AUTOZEROO2_COMMAND_CANCEL, ATTR_CTRL_TAB_POSITION, 2));

	// Finalize panel colors, positioning, and sizing
	errChk(SetPanelAttribute(hPanel, ATTR_CONFORM_TO_SYSTEM, TRUE));
	errChk(SetPanelAttribute(hPanel, ATTR_TOP,               VAL_AUTO_CENTER));
	errChk(SetPanelAttribute(hPanel, ATTR_LEFT,              VAL_AUTO_CENTER));

	//We're done!
	return hPanel;
Error:
	return error;
}


HPNL AutoZero(void)
{
	// Load AutoZero Screen
	if( (AutoZeroPanel = MakeAutozeroPanel(0)) >= 0){
		InstallPopup(AutoZeroPanel);
	}

	return AutoZeroPanel;
}


/*****************************************************************************/
/* Name:     CmdAutoZero                                                        
/*                                                                           
/* Purpose:  Display the AutoZero Screen
/*                                          
/*                                                                           
/* Example Call:   int CVICALLBACK CmdAutoZero (panel, control, event,                                 
/*                                             callbackData, eventData1, eventData2)
/*                              
/* Input:    int panel = ID of the panel where an event occurred                           
/*           int control = ID of the control where an event occurred                       
/*           int event = ID of the event that occurred                                     
/*           void *callbackData = NULL (not used)                                          
/*           int eventData1 = 0 (not used)                                                 
/*           int eventData2 = 0 (not used)                                                           
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:  NONE                                                           
/*                                                  
/*                                                                           
/* Return:   0                                                      
/*                                                                           
/*****************************************************************************/
int CVICALLBACK CmdAutoZero(int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int status;
//	int nBreakOnLibErrsFlag; // flag to break/notbreak on library errors

	switch(event){
		case EVENT_COMMIT:
			
			// DIMM the PTSPANEL_CALIBRATE control, unDIMM upon AutoZero exit
			SetCtrlAttribute(gPanelHandle[PTSPANEL], PTSPANEL_CALIBRATE, ATTR_DIMMED, TRUE);
			SetSysPanelMessage(LDBf(Ldb_EstabCom_PTS));
			
			// Make sure PTS 2000 is there....
			if(EstablishPTS(NO_AUTO_DETECT) != SUCCESS){
				SetSysPanelMessage("");
				CPVTMessagePopup(LDBf(Ldb_Com_Error), LDBf(Ldb_PTS_Com_Error), OK_ONLY_DLG, NO_HELP);
			}
			else{
				SetSysPanelMessage("");
				// Display AutoZero Screen
				if( (status = AutoZero()) < 0)
					ErrorMessageLoadPanel(status);
			}
			break;
		case EVENT_KEYPRESS:
			switch(eventData1){
				case VAL_F1_VKEY:
				case VAL_F1_VKEY | VAL_SHIFT_MODIFIER:
					ShowHelp(panel, GetActiveCtrl(panel), eventData1);
					break;
				default:
					break;
			} // end switch(eventData1)
			break;
		// end case EVENT_KEYPRESS
		default:
			break;
	} // end switch(event)

	return 0;
}


/*****************************************************************************/
/* Name:     ExitAutoZeroCmd                                                          
/*                                                                           
/* Purpose:  Discard the AutoZero Screen
/*                                          
/*                                                                           
/* Example Call:   int CVICALLBACK ExitAutoZeroCmd(panel, control, event,                
/*                                               callbackData, eventData1, eventData2)                                 
/*                                                                           
/* Input:    int panel = ID of the panel where an event occurred                           
/*           int control = ID of the control where an event occurred                       
/*           int event = ID of the event that occurred                                     
/*           void *callbackData = NULL (not used)                                          
/*           int eventData1 = 0 (not used)                                                 
/*           int eventData2 = 0 (not used)                                                         
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:    NONE                                                          
/*                                                  
/*                                                                           
/* Return:   0                                                       
/*                                                                           
/*****************************************************************************/
int CVICALLBACK ExitAutoZeroCMD(int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch(event){
		case EVENT_COMMIT:
			DiscardPanel(panel);
			AutoZeroPanel = NULL;
			SetCtrlAttribute(gPanelHandle[PTSPANEL], PTSPANEL_CALIBRATE, ATTR_DIMMED, FALSE);
			break;
		case EVENT_KEYPRESS:
			switch(eventData1){
				case VAL_F1_VKEY:
				case VAL_F1_VKEY | VAL_SHIFT_MODIFIER:
					ShowHelp(panel, GetActiveCtrl(panel), eventData1);
					break;
				default:
					break;
			} // end switch(eventData1)
			break;
		// end case EVENT_KEYPRESS
		default:
			break;
	} // end switch(event)

	return 0;
}


/*****************************************************************************/
/* Name:     AutoZeroSelect                                                      
/*                                                                           
/* Purpose:   Callback for autozero command buttons
/*                                          
/*                                                                           
/* Example Call:   int CVICALLBACK AutoZeroSelect(panel, control, event,                 
/*                                               callbackData, eventData1, eventData2)                                
/*                                                                           
/* Input:    int panel = ID of the panel where an event occurred                           
/*           int control = ID of the control where an event occurred                       
/*           int event = ID of the event that occurred                                    
/*           void *callbackData = NULL (not used)                                          
/*           int eventData1 = 0 (not used)                                                 
/*           int eventData2 = 0 (not used)                                                        
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:   NONE                                                           
/*                                                  
/*                                                                           
/* Return:   0                                                         
/*                                                                           
/*****************************************************************************/
int CVICALLBACK AutoZeroSelect(int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int status = 0;

	switch(event){
		case EVENT_COMMIT:
			if(control ==  AUTOZERO_COMMANDBUTTON_HP){
				if( (status = LoadAutoZeroPanelHP()) < 0){
					ErrorMessageLoadPanel(status);
				}
			}
			else if(control == AUTOZERO_COMMANDBUTTON_LP){
				if( (status = LoadAutoZeroPanelLP()) < 0){
					ErrorMessageLoadPanel(status);
				}
			}
			else if(control == AUTOZERO_COMMANDBUTTON_O2){
				if( (status = LoadAutoZeroPanelO2()) < 0){
					ErrorMessageLoadPanel(status);
				}
			}
			break;
		case EVENT_KEYPRESS:
			switch(eventData1){
				case VAL_F1_VKEY:
				case VAL_F1_VKEY | VAL_SHIFT_MODIFIER:
					ShowHelp(panel, GetActiveCtrl(panel), eventData1);
					break;
				default:
					break;
			} // end switch(eventData1)
			break;
		// end case EVENT_KEYPRESS
		default:
			break;
	} // end switch(event)

	return 0;
}


static HPNL LoadAutoZeroPanelHP(void)
{
	// Load and display AutoZero Screen for High Pressure
	if( (AutoZeroPanelHP = MakeAutozeroHPPanel(0)) >= 0){
		InstallPopup(AutoZeroPanelHP);
	}

	return AutoZeroPanelHP;
}


/*****************************************************************************/
/* Name:      ExitAutoZeroCMDHP                                                  
/*                                                                           
/* Purpose:  callback for autozero exit button
/*                                          
/*                                                                           
/* Example Call:   int CVICALLBACK ExitAutoZeroCMDHP(int panel, int control, int event,
/*		void *callbackData, int eventData1, int eventData2);                                    
/*                                                                           
/* Input:    int panel = ID of the panel where an event occurred                           
/*           int control = ID of the control where an event occurred                       
/*           int event = ID of the event that occurred                                    
/*           void *callbackData = NULL (not used)                                          
/*           int eventData1 = 0 (not used)                                                 
/*           int eventData2 = 0 (not used)                                                        
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
int CVICALLBACK ExitAutoZeroCMDHP(int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch(event){
		case EVENT_COMMIT:
			DiscardPanel(panel);
			AutoZeroPanelHP = NULL;
			break;
		case EVENT_KEYPRESS:
			switch(eventData1){
				case VAL_F1_VKEY:
				case VAL_F1_VKEY | VAL_SHIFT_MODIFIER:
					ShowHelp(panel, GetActiveCtrl(panel), eventData1);
					break;
				default:
					break;
			} // end switch(eventData1)
			break;
		// end case EVENT_KEYPRESS
		default:
			break;
	} // end switch(event)

	return 0;
}


/*****************************************************************************/
/* Name:     AutoZeroCMDHP                                                   
/*                                                                           
/* Purpose:  Sends command to autozero the PTS2000 high pressure sensor.  
/*                                          
/*                                                                           
/* Example Call:   int CVICALLBACK AutoZeroCMDHP(panel, control, event,                  
/*                                              callbackData, eventData1, eventData2)                                     
/*                                                                           
/* Input:    int panel = ID of the panel where an event occurred                           
/*           int control = ID of the control where an event occurred 
/*           int event = ID of the event that occurred            
/*           void *callbackData = NULL (not used)                                          
/*           int eventData1 = 0 (not used)                                                 
/*           int eventData2 = 0 (not used)           
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:  NONE                                                          
/*                                                  
/*                                                                           
/* Return:   0                                                 
/*                                                                           
/*****************************************************************************/
int CVICALLBACK AutoZeroCMDHP(int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int status;
	
	switch(event){
		case EVENT_COMMIT:
			#ifndef DEMOMODE
			status = ZeroHighPressure();
			#else
			status = NOERROR;
			#endif
			if(status == NOERROR){
				SetCtrlVal(panel, AUTOZEROHP_STATUS, LDBf(Ldb_HiPres_AutoZero_Succ));
			}
			else{
				SetCtrlVal(panel, AUTOZEROHP_STATUS, LDBf(Ldb_HiPres_AutoZero_Fail));
			}
			break;
		case EVENT_KEYPRESS:
			switch(eventData1){
				case VAL_F1_VKEY:
				case VAL_F1_VKEY | VAL_SHIFT_MODIFIER:
					ShowHelp(panel, GetActiveCtrl(panel), eventData1);
					break;
				default:
					break;
			} // end switch(eventData1)
			break;
		// end case EVENT_KEYPRESS
		default:
			break;
	} // end switch(event)

	return 0;
}


static HPNL LoadAutoZeroPanelLP(void)
{
	// Load and display AutoZero Screen for Low Pressure
	if( (AutoZeroPanelLP = MakeAutozeroLPPanel(0)) >= 0){
		InstallPopup(AutoZeroPanelLP);
	}

	return AutoZeroPanelLP;
}


/*****************************************************************************/
/* Name:     ExitAutoZeroCMDLP                                                  
/*                                                                           
/* Purpose:  This function does what.....
/*                                          
/*                                                                           
/* Example Call:   int CVICALLBACK ExitAutoZeroCMDLP(int panel, int control, int event,
/*		void *callbackData, int eventData1, int eventData2);                                    
/*                                                                           
/* Input:    input1 -	description          
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
int CVICALLBACK ExitAutoZeroCMDLP(int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch(event) {
		case EVENT_COMMIT:
			DiscardPanel(panel);
			AutoZeroPanelLP = NULL;
			break;
		case EVENT_KEYPRESS:
			switch(eventData1){
				case VAL_F1_VKEY:
				case VAL_F1_VKEY | VAL_SHIFT_MODIFIER:
					ShowHelp(panel, GetActiveCtrl(panel), eventData1);
					break;
				default:
					break;
			} // end switch(eventData1)
			break;
		// end case EVENT_KEYPRESS
		default:
			break;
	} // end switch(event)

	return 0;
}


/*****************************************************************************/
/* Name:     AutoZeroCMDLP                                                      
/*                                                                           
/* Purpose:  Sends command to autozero the PTS2000 low pressure sensor.
/*                                          
/*                                                                           
/* Example Call:  nt CVICALLBACK AutoZeroCMDLP(panel, control, event,                  
/*                                              callbackData, eventData1, eventData2)                                    
/*                                                                           
/* Input:    int panel = ID of the panel where an event occurred                           
/*           int control = ID of the control where an event occurred                       
/*           int event = ID of the event that occurred                                     
/*           void *callbackData = NULL (not used)                                          
/*           int eventData1 = 0 (not used)                                                 
/*           int eventData2 = 0 (not used)         
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:   NONE                                                           
/*                                                  
/*                                                                           
/* Return:   0                                                         
/*                                                                           
/*****************************************************************************/
int CVICALLBACK AutoZeroCMDLP(int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int status;
	
	switch(event){
		case EVENT_COMMIT:
			#ifndef DEMOMODE
			status = ZeroLowPressure();
			#else
			status = NOERROR;
			#endif
			if(status == NOERROR){
				SetCtrlVal(panel, AUTOZEROLP_STATUS, LDBf(Ldb_LoPres_AutoZero_Succ));
			}
			else{
				SetCtrlVal(panel, AUTOZEROLP_STATUS, LDBf(Ldb_LoPres_AutoZero_Fail));
			}
			break;
		case EVENT_KEYPRESS:
			switch(eventData1){
				case VAL_F1_VKEY:
				case VAL_F1_VKEY | VAL_SHIFT_MODIFIER:
					ShowHelp(panel, GetActiveCtrl(panel), eventData1);
					break;
				default:
					break;
			} // end switch(eventData1)
			break;
		// end case EVENT_KEYPRESS
		default:
			break;
	} // end switch(event)

	return 0;
}


static HPNL LoadAutoZeroPanelO2(void)
{
	// Load and display AutoZero Screen for Oxygen
	if( (AutoZeroPanelO2 = MakeAutozeroO2Panel(0)) >= 0){
		SetCtrlAttribute(AutoZeroPanelO2, AUTOZEROO2_CMD_CAL_O2_100, ATTR_DIMMED, TRUE);
		InstallPopup(AutoZeroPanelO2);
		#ifndef DEMOMODE
		SetCtrlAttribute(AutoZeroPanelO2, AUTOZEROO2_TIMER, ATTR_ENABLED, TRUE);
		#endif
	}

	return AutoZeroPanelO2;
}


/*****************************************************************************/
/* Name:    ExitAutoZeroCMDO2                                                    
/*                                                                           
/* Purpose:  autozero callback for exit
/*                                          
/*                                                                           
/* Example Call:   int CVICALLBACK ExitAutoZeroCMDO2(int panel, int control, int event,
/*		void *callbackData, int eventData1, int eventData2);                                    
/*                                                                           
/* Input:    int panel = ID of the panel where an event occurred                           
/*           int control = ID of the control where an event occurred                       
/*           int event = ID of the event that occurred                                    
/*           void *callbackData = NULL (not used)                                          
/*           int eventData1 = 0 (not used)                                                 
/*           int eventData2 = 0 (not used)                                                        
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
int CVICALLBACK ExitAutoZeroCMDO2(int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch(event) {
		case EVENT_COMMIT:
			SetCtrlAttribute(AutoZeroPanelO2, AUTOZEROO2_TIMER, ATTR_ENABLED, FALSE);
			DiscardPanel(panel);
			AutoZeroPanelO2 = NULL;
			break;
		case EVENT_KEYPRESS:
			switch(eventData1){
				case VAL_F1_VKEY:
				case VAL_F1_VKEY | VAL_SHIFT_MODIFIER:
					ShowHelp(panel, GetActiveCtrl(panel), eventData1);
					break;
				default:
					break;
			} // end switch(eventData1)
			break;
		// end case EVENT_KEYPRESS
		default:
			break;
	} // end switch(event)

	return 0;
}


/*****************************************************************************/
/* Name:       OxygenTimer                                                   
/*                                                                           
/* Purpose:  Oxy timer callback to display 02 percent on autozero panel
/*                                          
/*                                                                           
/* Example Call:   int CVICALLBACK OxygenTimer(int panel, int control, int event,
/*		void *callbackData, int eventData1, int eventData2);                                    
/*                                                                           
/* Input:    int panel = ID of the panel where an event occurred                           
/*           int control = ID of the control where an event occurred                       
/*           int event = ID of the event that occurred                                    
/*           void *callbackData = NULL (not used)                                          
/*           int eventData1 = 0 (not used)                                                 
/*           int eventData2 = 0 (not used)                                                        
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
int CVICALLBACK OxygenTimer(int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int status;
	double dData;
	char cBuf[255];

	switch (event) {
		case EVENT_TIMER_TICK:
			status = MeasureOxygen(&dData);
			if(status != CVI_NOERROR){
				SetCtrlAttribute(panel, AUTOZEROO2_PERCENT_O2_LBL, ATTR_VISIBLE, FALSE);
				cBuf[0] = '\0';
			}
			else{
				SetCtrlAttribute(panel, AUTOZEROO2_PERCENT_O2_LBL, ATTR_VISIBLE, TRUE);
				sprintf(cBuf,"% 3.3f", dData);
			}
			SetCtrlVal(panel, AUTOZEROO2_PERCENT_O2, cBuf);
			break;
	}
	return 0;
}


/*****************************************************************************/
/* Name:     cmd_Cal_O2_21                                                     
/*                                                                           
/* Purpose:  Sends command to adjust the PTS2000 O2 sensor for 21% (room) air.
/*                                          
/*                                                                           
/* Example Call:   int CVICALLBACK cmd_Cal_O2_21(panel, control, event,                  
/*                                              callbackData, eventData1, eventData2)                                   
/*                                                                           
/* Input:    int panel = ID of the panel where an event occurred                           
/*        int control = ID of the control where an event occurred                       
/*        int event = ID of the event that occurred                                     
/*        void *callbackData = NULL (not used)                                          
/*        int eventData1 = 0 (not used)                                                 
/*        int eventData2 = 0 (not used)          
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:    NONE                                                         
/*                                                  
/*                                                                           
/* Return:   0                                                         
/*                                                                           
/*****************************************************************************/
int CVICALLBACK cmd_Cal_O2_21(int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int status;
	
	switch(event){
		case EVENT_COMMIT:
			#ifndef DEMOMODE
			status = AdjOxy21();
			#else
			status = NOERROR;
			#endif
			if(status == NOERROR){
				SetCtrlVal(panel, AUTOZEROO2_STATUS_21, LDBf(Ldb_Adj_21_O2_Succ));
				SetCtrlAttribute(AutoZeroPanelO2, AUTOZEROO2_CMD_CAL_O2_100, ATTR_DIMMED, FALSE);
			}
			else{
				SetCtrlVal(panel, AUTOZEROO2_STATUS_21, LDBf(Ldb_Adj_21_O2_Fail));
			}
			break;
		case EVENT_KEYPRESS:
			switch(eventData1){
				case VAL_F1_VKEY:
				case VAL_F1_VKEY | VAL_SHIFT_MODIFIER:
					ShowHelp(panel, GetActiveCtrl(panel), eventData1);
					break;
				default:
					break;
			} // end switch(eventData1)
			break;
		// end case EVENT_KEYPRESS
		default:
			break;
	} // end switch(event)

	return 0;
}


/*****************************************************************************/
/* Name:     cmd_Cal_O2_100                                                     
/*                                                                           
/* Purpose:  Sends command to adjust the PTS2000 O2 sensor for 100% oxygen gas.   
/*                                          
/*                                                                           
/* Example Call:   int CVICALLBACK cmd_Cal_O2_100(panel, control, event,                 
/*                                               callbackData, eventData1, eventData2)                                  
/*                                                                           
/* Input:    int panel = ID of the panel where an event occurred                           
/*        int control = ID of the control where an event occurred                       
/*        int event = ID of the event that occurred                                     
/*        void *callbackData = NULL (not used)                                          
/*        int eventData1 = 0 (not used)                                                 
/*        int eventData2 = 0 (not used)              
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:     NONE                                                        
/*                                                  
/*                                                                           
/* Return:   0                                                     
/*                                                                           
/*****************************************************************************/
int CVICALLBACK cmd_Cal_O2_100(int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int status;
	
	switch(event){
		case EVENT_COMMIT:
			#ifndef DEMOMODE
			status = AdjOxy100();
			#else
			status = NOERROR;
			#endif
			if(status == NOERROR){
				SetCtrlVal(panel, AUTOZEROO2_STATUS_100, LDBf(Ldb_Adj_100_O2_Succ));
			}
			else{
				SetCtrlVal(panel, AUTOZEROO2_STATUS_100, LDBf(Ldb_Adj_100_O2_Fail));
			}
			break;
		case EVENT_KEYPRESS:
			switch(eventData1){
				case VAL_F1_VKEY:
				case VAL_F1_VKEY | VAL_SHIFT_MODIFIER:
					ShowHelp(panel, GetActiveCtrl(panel), eventData1);
					break;
				default:
					break;
			} // end switch(eventData1)
			break;
		// end case EVENT_KEYPRESS
		default:
			break;
	} // end switch(event)

	return 0;
}

