#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============= C L A S S   D E S C R I P T I O N ====================
//@ Class:  BatchDiscreteSetting - Batch Discrete Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This class provides the setting's framework for the Batch, Discrete
//  Settings.  All batch, discrete settings are derived from this setting.
//  This class overrides some of the virtual methods of 'DiscreteSetting'
//  including those to:  get this batch discrete setting's new-patient
//  value, get this batch discrete setting's "accepted" and "adjusted"
//  value, activating the Transition Rules associated with this setting
//  (Main Control Settings only), responding to transition of the Main
//  Control Settings, calculate (update) this settings dependent settings,
//  and store a new value for this setting into the Adjusted Context.
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to establish the setting's batch discrete
//  framework for the rest of the settings to build upon.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class defines no instances -- it just overrides some behavior.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/BatchDiscreteSetting.ccv   25.0.4.0   19 Nov 2013 14:27:14   pvcs  $
//
//@ Modification-Log
//
//  Revision: 007   By: gdc    Date: 21-Jan-2009    SCR Number: 6458
//  Project:  840S
//  Description:
//      Modified to use isNewlyApplicable() method to determine if
//		setting's applicability has changed from non-applicable to
//		an applicable state. Resolves problem of setting getting
//		into a changed state and not being able to get out of it when
//		even when the applicability reverts to non-applicable.
//
//  Revision: 006   By: gdc   Date:  08-Jan-2009    SCR Number: 6435
//  Project:  840S
//  Description:
//		Modified to support changeable IBW.
//	
//  Revision: 005   By: sah   Date:  04-Mar-1999    DR Number: 5322
//  Project:  ATC
//  Description:
//	Project-related changes:
//	*  using SafetyPcvSettingValue's new, generic interface
//
//  Revision: 004   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 003   By: dosman Date:  24-Feb-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//      Inital BiLevel version.
//      Ensure that absolute max/min values are updated during 
//      update to new patient mechanism.
//
//  Revision: 002   By: sah    Date:  28-May-1998    DR Number: 5092
//  Project:  COLOR
//  Description:
//	--- Merged Rev "Color" (1.14.1.0) into Rev "BiLevel" (1.14.2.0)
//	Added the passing of new constructor parameters from caller
//	(derived class), to base class.  These new parameters are used
//	for allowing discrete settings to have bounded ranges.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//      Inital version.
//
//=====================================================================

#include "BatchDiscreteSetting.hh"

//@ Usage-Classes
#include "ContextMgr.hh"
#include "AdjustedContext.hh"
#include "AcceptedContext.hh"
#include "SafetyPcvSettingValues.hh"
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method: ~BatchDiscreteSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this batchDiscrete setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

BatchDiscreteSetting::~BatchDiscreteSetting(void)
{
	CALL_TRACE("~BatchDiscreteSetting()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  isChanged()  [const, virtual]
//
//@ Interface-Description
//  Has this setting been changed? When a batch setting's "adjusted"
//  value has been changed -- either auto-set or by the operator -- to a
//  value that is different than the setting's "accepted" value, or if 
//  the setting is now applicable in the adjusted context and it isn't
//  in the accepted context, this method will return TRUE, otherwise it
//  returns FALSE.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[01038] -- is changed whenever the value is different than accepted...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean	BatchDiscreteSetting::isChanged(void) const
{
	CALL_TRACE("isChanged()");

	Boolean  isChanged;

	if ( !getForcedChangeFlag_() )
	{	// $[TI1]
		// the override flag is NOT set, therefore the "change" status is based
		// on whether the accepted and adjusted values are different...
		const DiscreteValue  ACCEPTED_VALUE = getAcceptedValue();
		const DiscreteValue  ADJUSTED_VALUE = getAdjustedValue();

		isChanged = (ACCEPTED_VALUE != ADJUSTED_VALUE) || isNewlyApplicable();
	}
	else
	{	// $[TI2] -- the override flag IS set, therefore return 'TRUE'.
		isChanged = TRUE;
	}

	return(isChanged);
}


//================ M E T H O D   D E S C R I P T I O N ================
//@ Method:  getDefaultValue()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is NOT to be overridden by any of the
//  derived classes.  This is only used to initialize the Accepted Context
//  to some set of valid values, BEFORE a New-Patient Setup has been run.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This just returns the setting's Safety-PCV value.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
	BatchDiscreteSetting::getDefaultValue(void) const
{
	CALL_TRACE("getDefaultValue()");

	return(SafetyPcvSettingValues::GetValue(getId()));
}	// $[TI1]


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method: getNewPatientValue()  [const, pure virtual]
//
//@ Interface-Description
//  This is a pure virtual method that is to be overridden by ALL batch
//  settings.  This method is to return this setting's new-patient value.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

//@ Not-Implemented


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  calcTransition(newValue, currValue)  [virtual]
//
//@ Interface-Description
//  This is a virtual method that is to be overridden by all derived
//  Main Control Setting classes, to provide appropriate behavior regarding
//  the transition of this Main Control Setting.  This version of this
//  virtual method is NEVER to be called -- it asserts this fact.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (FALSE)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	BatchDiscreteSetting::calcTransition(const DiscreteValue,
										 const DiscreteValue)
{
	CALL_TRACE("calcTransition(newValue, currValue)");
	AUX_CLASS_ASSERTION_FAILURE(getId());

	// will NEVER get here...
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  acceptTransition(settingId, newValue, currValue)  [virtual]
//
//@ Interface-Description
//  This is a virtual method that is to be overridden by all derived
//  classes that respond to Main Control Setting transitions.  This version
//  of this virtual method is NEVER to be called -- it asserts this fact.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (FALSE)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	BatchDiscreteSetting::acceptTransition(const SettingId::SettingIdType,
										   const DiscreteValue,
										   const DiscreteValue)
{
	CALL_TRACE("acceptTransition(settingId, newValue, currValue)");
	AUX_CLASS_ASSERTION_FAILURE(getId());

	// will NEVER get here...
}


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//        [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	BatchDiscreteSetting::SoftFault(const SoftFaultID  softFaultID,
									const Uint32       lineNumber,
									const char*        pFileName,
									const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

	FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
							BATCH_DISCRETE_SETTING, lineNumber,
							pFileName, pPredicate);
}


//=====================================================================
//
//  Protected Methods..
//
//=====================================================================

//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method: BatchDiscreteSetting(...)
//
//@ Interface-Description
//  Construct a default batch, discrete setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (SettingId::IsBatchDiscreteId(discreteId))
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

BatchDiscreteSetting::BatchDiscreteSetting(
										  const SettingId::SettingIdType  discreteId,
										  const SettingId::SettingIdType* arrDependentSettingIds,
										  const Uint                     totalNumValues,
										  const Boolean                  useNonWrappingMode,
										  const SettingBoundId              upperLimitBoundId,
										  const SettingBoundId              lowerLimitBoundId)
: DiscreteSetting(discreteId, 
				  arrDependentSettingIds,
				  totalNumValues,
				  useNonWrappingMode,
				  upperLimitBoundId,
				  lowerLimitBoundId)
{
	CALL_TRACE("BatchDiscreteSetting(...)");
	AUX_CLASS_PRE_CONDITION((SettingId::IsBatchDiscreteId(discreteId)),
							discreteId);
}	// $[TI1]


#if defined(SIGMA_DEVELOPMENT)

	#ifndef Ostream_HH
		#include "Ostream.hh"
	#endif // Ostream_HH

//============== M E T H O D   D E S C R I P T I O N =================
// Method:  print_(ostr)  [const]
//
// Interface-Description
//  Print the contents of this batchDiscrete setting into 'ostr'.
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

Ostream&
	BatchDiscreteSetting::print_(Ostream& ostr) const
{
	CALL_TRACE("print_(ostr)");

	DiscreteSetting::print_(ostr);

	ostr << "  Adjusted Value:  " << (DiscreteValue)getAdjustedValue() << endl;
	ostr << "  isChanged():     " << ((isChanged()) ? "TRUE" : "FALSE")
	<< endl;

	return(ostr);
}

#endif  // defined(SIGMA_DEVELOPMENT)
