#ifndef PiStuckTest_HH
#define PiStuckTest_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
//  Class: PiStuckTest - BD Safety Net Test for the detection of
//                       inspiratory pressure transducer stuck condition
//                       due to a malfunctioning solenoid
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/PiStuckTest.hhv   25.0.4.0   19 Nov 2013 14:00:02   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005  By: iv     Date:  18-May-1999    DR Number: 5322
//       Project:  ATC
//       Description:
//           Added a flag to indicate possible Pi stuck during exhalation
//           and access method for this flag.
//
//  Revision: 004  By: sah     Date:  12-Jan-1999    DR Number: 5321
//       Project:  ATC
//       Description:
//           Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 003  By:  iv    Date: 17-Oct-1997     DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//           Reworked per formal code review.
//
//  Revision: 002  By:  iv    Date: 15-Oct-1997     DR Number:DCS 1649
//       Project:  Sigma (R8027)
//       Description:
//           Revise detection algorithm to eliminate dependency on disconnect.
//
//  Revision: 001  By:  by    Date:  20-Sep-1996    DR Number: None
//    Project:  Sigma (840)
//    Description:
//        Initial version.
//
//====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"
#include "SafetyNetTest.hh"
#include "BreathPhaseType.hh"

//@ Usage-Classes
#include "Resistance.hh"
#include "Background.hh"
//@ End-Usage

class PiStuckTest : public SafetyNetTest
{
    public:

        PiStuckTest( void );
        ~PiStuckTest( void );

        static void SoftFault( const SoftFaultID softFaultID,
			       const Uint32      lineNumber,
			       const char*       pFileName  = NULL,
			       const char*       pPredicate = NULL );

        virtual BkEventName checkCriteria( void );
		static Boolean IsPiStuckDuringExhalation(void);
  

    protected:

    private:

        // these methods are purposely declared, but not implemented...
        PiStuckTest( const PiStuckTest& );        // not implemented...
        void operator=( const PiStuckTest& );    // not implemented...

        void resetCriteria_( void );

        //@ Data-Member: inspiratoryResistance_ 
        // stores the resistance of the inspiratory limb caluclated during EST
        Resistance inspiratoryResistance_;

        //@ Data-Member: expiratoryResistance_ 
        // stores the resistance of the expiratory limb caluclated during EST
        Resistance expiratoryResistance_;

        //@ Data-Member: piStuckFlag_
        // Indicates whether Pe stuck condition has been detected
        Boolean piStuckFlag_;

        //@ Data-Member: bkEventReported_
        // Indicates whether Pe stuck condition has been reported 
        Boolean bkEventReported_;

        //@ Data-Member: detectionDisqualified_ 
	    // Used to disqualify detection of Pi stuck
        Boolean detectionDisqualified_;

        //@ Data-Member: numPiStuckBreaths_ 
	    // Number of breaths with Pi stuck condition detected.
        Int8 numPiStuckBreaths_; 

        //@ Data-Member: phaseType_ 
	    // The current breath phase type, e.g. - inspiration, exhalation - is stored
        BreathPhaseType::PhaseType phaseType_;

        //@ Data-Member: PiStuckDuringExhalation_ 
	    // Flags suspect Pi during exhalation.
        static Boolean PiStuckDuringExhalation_;
};

// Inlined methods...
#include "PiStuckTest.in"

#endif // PiStuckTest_HH 
