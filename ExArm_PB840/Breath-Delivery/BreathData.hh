#ifndef BreathData_HH
#define BreathData_HH
//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: BreathData -  This class stores and allows access to all data
//  required by subsystems other than breath delivery, that pertains to
//  a single breath.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/BreathData.hhv   25.0.4.0   19 Nov 2013 13:59:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 023   By:  rhj     Date: 18-Mar-2009     SCR Number: 6492 
//  Project:  840S2
//  Description:
//       Added  getInspiredLungVolume().
//       
//  Revision: 022   By:  rhj     Date: 07-Nov-2008     SCR Number: 6435
//  Project:  840S
//  Description:
//       Leak Compensation project-related changes:
//       Added  getExhaledTidalVolume()
//       
//  Revision: 021   By:   rhj    Date: 05-March-2007      DR Number: 6359
//  Project:  RESPM
//  Description:
//        Resolve merge conflict for RM to Baseline 
//
//  Revision: 020   By: gdc      Date:  20-Feb-2007    SCR Number: 6345
//  Project:  RESPM
//  Description:
//		Removed C20/C related code.
//
//  Revision: 019   By: gdc      Date:  22-Oct-2006    SCR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Added processing of RM data items.
//
//  Revision: 018   By: gdc      Date:  10-Jun-2005    SCR Number: 6170
//  Project:  NIV
//  Description:
//		NIV related changes. Added breath trigger data to end of inspiration
//		data packet.
// 
//  Revision: 017   By: gdc      Date:  10-Feb-2005    SCR Number: 6144
//  Project:  NIV
//  Description:
//		NIV related changes. Added vent-type to breath data.
// 
//  Revision: 016   By: yakovb   Date:  19-Mar-2002    DR Number: 5863
//  Project:  VCP
//  Description:
//      Handle VC+ mandatory messages.
//
//  Revision: 015  By: jja     Date:  30-June-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//		VTPC project-related changes:
//		Added spontTiTtotRatio_ and spontInspTime_.
//
//  Revision: 014  By:  jja    Date:  20-Apr-00    DR Number: 5708 
//	Project:  NeoMode
//	Description:
//		Revise Pmean to be an averaged value instead of  
//              single breath.
//
//  Revision: 013  By: jja     Date:  06-Apr-00    DR Number: 5692
//  Project:  NeoMode
//  Description:
//		Revise when peakCircuitPressure is updated.
//
//  Revision: 012  By: syw     Date:  18-Jul-1999    DR Number: 5471, 5481
//  Project:  ATC
//  Description:
//		Added breathTypeDisplay to manage the breath type bar on the GUI.
//
//  Revision: 011  By: syw     Date:  26-Apr-1999    DR Number: 5322
//  Project:  ATC
//  Description:
//		Added inspiredLungVolume_ and supportType_ support for TC.
//
//  Revision: 010  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 009  By:  syw    Date: 14-Aug-1997    DR Number: DCS 2382
//       Project:  Sigma (R8027)
//       Description:
//             Added BUFFER_SIZE pt running average of exhaledTidalVolume data.
//
//  Revision: 008  By:  sp    Date: 12-Dec-1996    DR Number: DCS 1611
//       Project:  Sigma (R8027)
//       Description:
//             Add new data member previousBreathType_.
//
//  Revision: 007  By:  sp    Date: 24-Oct-1996    DR Number:
//       Project:  Sigma (R8027)
//       Description:
//             Inspection rework.
//
//  Revision: 006  By:  sp    Date:  16-Oct-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Removed unit test methods.
//
//  Revision: 005  By:  sp    Date:  24-Jun-1996    DR Number: DCS 967
//       Project:  Sigma (R8027)
//       Description:
//             Add new const SEND_PEEP_RECV_DATA.
//
//  Revision: 004  By:  sp    Date:  8-May-1996    DR Number:600
//       Project:  Sigma (R8027)
//       Description:
//             Rename SEND_END_EXH_PAUSE_DATA to SEND_BEGIN_EXP_PAUSE_DATA.
//
//  Revision: 003  By:  iv    Date:  4-Mar-1996    DR Number:674
//       Project:  Sigma (R8027)
//       Description:
//             Remove #ifndef.
//
//  Revision: 002  By:  sp    Date:  8-Feb-1996    DR Number: 704 
//       Project:  Sigma (R8027)
//       Description:
//             Add const SEND_END_INSP_DATA, SEND_END_EXH_DATA, SEND_END_EXH_PAUSE_DATA.
//             Add method sendPatientData.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "Sigma.hh"

#include "Breath_Delivery.hh"

//@ Usage-Classes
#include "BreathType.hh"
#include "PauseTypes.hh"
#include "BreathPhaseType.hh"
#include "SupportTypeValue.hh"
#include "MandTypeValue.hh"
#include "VentTypeValue.hh"
#include "Trigger.hh"

class BreathRecord;
//@ End-Usage

class BreathData {
  public:
    BreathData(void);
    ~BreathData(void);

    static void SoftFault(const SoftFaultID softFaultID, 
			  const Uint32 lineNumber, 
			  const char* pFileName = NULL, 
			  const char* pPredicate = NULL);

    void update(const BreathRecord& br); 

    void sendPatientData(const Int32 msg);
    Real32 getExhaledTidalVolume( void);
    Real32 getInspiredLungVolume( void);

#ifdef SIGMA_UNIT_TEST
    void write( void );
#endif //SIGMA_UNIT_TEST

  protected:

  private:

    enum 
    {
        BUFFER_SIZE = 5
    };

    BreathData(const BreathData&);          // Declared but not implemented
    void operator=(const BreathData&);   // Declared but not implemented

    //@ Data-Member: breathDuration_
    // will be sent during inspiration
    Real32 breathDuration_;

    //@ Data-Member: exhaledTidalVolume_
    // will be sent during inspiration
    Real32 exhaledTidalVolume_;

    //@ Data-Member: endInspiratoryPressure_
    // will be sent during expiration
    Real32 endInspiratoryPressure_;

    //@ Data-Member: ExpiratoryPressure_
    // will be sent during inspiration
    Real32 endExpiratoryPressure_;

    //@ Data-Member: expiratoryPausePressure_
    // will be sent during expiratory pause
    Real32 expiratoryPausePressure_;

    //@ Data-Member: ieRatio_
    // will be sent during inspiration
    Real32 ieRatio_;

    //@ Data-Member: peakCircuitPressure_
    // will be sent during expiration
    Real32 peakCircuitPressure_;

    //@ Data-Member: breathType_
    // will be sent during inspiration
    BreathType breathType_;

    //@ Data-Member: previousBreathType_
    // will be sent during inspiration
    BreathType previousBreathType_;

    //@ Data-Member: breathTypeDisplay_
    // will be sent during inspiration
    BreathType breathTypeDisplay_ ;

     //@ Data-Member: mandFraction_
     // portion of the exhaled volume that is considered mand
     Real32 mandFraction_ ;
     
	//@ Data-Member: exhaledTvBuffer_[BUFFER_SIZE]
	// buffer to hold BUFFER_SIZE pt running average
	Real32 exhaledTvBuffer_[BUFFER_SIZE] ;

	//@ Data-Member: bufferIndex_
	// index into exhaledTvBuffer_ ;
	Uint32 bufferIndex_ ;

	//@ Data-Member: averagedExhaledTv_
	// BUFFER_SIZE pt running average
	Real32 averagedExhaledTv_ ;

	//@ Data-Member: numPointsInBuffer_
	// number of points in buffer 
	Uint32 numPointsInBuffer_ ;

	//@ Data-Member: endExpPauseData_
	// end expiratory pause data 
	PauseTypes::PpiState endExpPauseData_;

	//@ Data-Member: inspiredLungVolume_
	// inspired lung volume
	Real32 inspiredLungVolume_ ;

	//@ Data-Member:::SupportTypeValueId supportType_
	// support type (PSV, TC, PA, VS, none)
	SupportTypeValue::SupportTypeValueId supportType_ ;

	//@ Data-Member:::MandTypeValueId MandType_
	// mandatory type (VCP)
	MandTypeValue::MandTypeValueId mandType_ ;

	//@ Data-Member:::VentTypeValueId ventType_
	VentTypeValue::VentTypeValueId ventType_ ;

	//@ Data-Member:  spontInspTime_
	// calculated inspiration time for spont breaths only.
	Real32  spontInspTime_;	

	//@ Data-Member:  spontTiTtotRatio_
	// calculated insp to breath period ratio for spont breaths only.
	Real32  spontTiTtotRatio_;	
 	
	//@ Data-Member:  breathTriggerId_
	// set to the current breath trigger
	Trigger::TriggerId breathTriggerId_;
 	
	//@ Data-Member:  peakSpontInspFlow_
	// peak inspiratory net flow for spontaneous breath
	Real32  peakSpontInspFlow_;	
 	
	//@ Data-Member:  peakExpiratoryFlow_
	// peak expiratory net flow
	Real32  peakExpiratoryFlow_;	
 	
	//@ Data-Member:  endExpiratoryFlow_
	// end expiratory net flow
	Real32  endExpiratoryFlow_;	
 	
	//@ Data-Member:  dynamicCompliance_
	// dynamic compliance valid for mand breaths only
	Real32  dynamicCompliance_;	
 	
	//@ Data-Member:  dynamicResistance_
	// dynamic resistance valid for mand breaths only
	Real32  dynamicResistance_;	
};



#endif // BreathData_HH 
