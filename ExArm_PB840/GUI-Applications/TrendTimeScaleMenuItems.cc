#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: TrendTimeScaleMenuItems -  Trend time scale menu items 
//---------------------------------------------------------------------
//@ Interface-Description
//  TrendTimeScaleMenuItems is a SettingMenuItems class that provides
//  the trend time scale selection menu strings displayed by the
//  DiscreteSettingButton and the ScrollableMenu classes.
// 
//  TrendTimeScaleMenuItems provides a single static method
//  GetTrendTimeScaleMenuItems() that instantiates a single instance of
//  this class and returns a reference to it. During construction, this
//  class initializes the static DiscreteSettingButton::ValueInfo data
//  structure containing a single header and an array of strings for
//  each trend time scale setting value.
// 
//  This ValueInfo structure is referenced by DiscreteSettingButton
//  through the SettingMenuItems::getValueInfo method that returns a
//  reference to this static ValueInfo structure.
// 
//  The ScrollableMenu class retrieves the menu strings defined in
//  ValueInfo using the SettingMenuItems::getLogEntryInfo() method.
//---------------------------------------------------------------------
//@ Rationale
//	Needed to store and maintain trending time scale menu data.
//---------------------------------------------------------------------
//@ Implementation-Description 
//  A static method GetTrendTimeScaleMenuItems instantiates a single
//  instance of this class and returns a reference to it. The class
//  constructor is private so only this class can construct and
//  instance.
//---------------------------------------------------------------------
//@ Fault-Handling 
//	none 
//---------------------------------------------------------------------
//@ Restrictions 
//	none 
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/TrendTimeScaleMenuItems.ccv   25.0.4.0   19 Nov 2013 14:08:40   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By: gdc    Date: 10-Jan-2007    SCR Number: XXXX
//  Project:  TREND
//  Description:
//		Initial version.
//=====================================================================

#include "TrendTimeScaleMenuItems.hh"
#include "DiscreteSettingButton.hh"
#include "TrendTimeScaleValue.hh"
#include "MiscStrs.hh"

static Uint TrendMenuInfoMemory_[sizeof(DiscreteSettingButton::ValueInfo) +
								 sizeof(StringId) * (TrendTimeScaleValue::TOTAL_TREND_TIME_SCALE_VALUES - 1)];

static DiscreteSettingButton::ValueInfo*  PMenuInfo_ =
	(DiscreteSettingButton::ValueInfo*)::TrendMenuInfoMemory_;

//@ Usage-Classes
//@ End-Usage

//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TrendTimeScaleMenuItems()  [Constructor]
//
//@ Interface-Description
//	Constructor.
//---------------------------------------------------------------------
//@ Implementation-Description 
//  Initializes the static DiscreteSettingButton::ValueInfo structure
//  with the strings defined for each trend time scale menu entry.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
TrendTimeScaleMenuItems::TrendTimeScaleMenuItems(void)
:   SettingMenuItems(PMenuInfo_)
{
	// set up values for trend type scale....
	::PMenuInfo_->numValues = TrendTimeScaleValue::TOTAL_TREND_TIME_SCALE_VALUES;

	PMenuInfo_->arrValues[TrendTimeScaleValue::TREND_TIME_SCALE_1HR] = MiscStrs::TREND_TIME_SCALE_1HR_LABEL;
	PMenuInfo_->arrValues[TrendTimeScaleValue::TREND_TIME_SCALE_2HR] = MiscStrs::TREND_TIME_SCALE_2HR_LABEL;
	PMenuInfo_->arrValues[TrendTimeScaleValue::TREND_TIME_SCALE_4HR] = MiscStrs::TREND_TIME_SCALE_4HR_LABEL;
	PMenuInfo_->arrValues[TrendTimeScaleValue::TREND_TIME_SCALE_8HR] = MiscStrs::TREND_TIME_SCALE_8HR_LABEL;
	PMenuInfo_->arrValues[TrendTimeScaleValue::TREND_TIME_SCALE_12HR] = MiscStrs::TREND_TIME_SCALE_12HR_LABEL;
	PMenuInfo_->arrValues[TrendTimeScaleValue::TREND_TIME_SCALE_24HR] = MiscStrs::TREND_TIME_SCALE_24HR_LABEL;
	PMenuInfo_->arrValues[TrendTimeScaleValue::TREND_TIME_SCALE_48HR] = MiscStrs::TREND_TIME_SCALE_48HR_LABEL;
	PMenuInfo_->arrValues[TrendTimeScaleValue::TREND_TIME_SCALE_72HR] = MiscStrs::TREND_TIME_SCALE_72HR_LABEL;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~TrendTimeScaleMenuItems()  [Destructor]
//
//@ Interface-Description
// Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

TrendTimeScaleMenuItems::~TrendTimeScaleMenuItems(void)
{
	CALL_TRACE("TrendTimeScaleMenuItems::~TrendTimeScaleMenuItems(void)");

	// Do nothing
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetTrendSelectMenuItems
//
//@ Interface-Description
//  Constructs static instance of TrendTimeScaleMenuItems and returns a
//	reference to it.
//---------------------------------------------------------------------
//@ Implementation-Description
//	none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
TrendTimeScaleMenuItems& TrendTimeScaleMenuItems::GetMenuItems(void)
{
	static TrendTimeScaleMenuItems MenuItems_;

	return MenuItems_;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void TrendTimeScaleMenuItems::SoftFault(const SoftFaultID  softFaultID,
										const Uint32       lineNumber,
										const char*        pFileName,
										const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, TRENDTIMESCALEMENUITEMS,
							lineNumber, pFileName, pPredicate);
}
