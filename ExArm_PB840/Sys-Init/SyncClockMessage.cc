#include "stdafx.h"
//=====================================================================
//   This is a proprietary work to which Puritan-Bennett corporation
//   of California claims exclusive right.  No part of this work may
//  be used, disclosed, reproduced, sorted in an information retrieval
//     system, or transmitted by any means, electronic, mechanical,
//   photocopying, recording, or otherwise without the prior written
//      permission of Puritan-Bennett Corporation of California.
//
//          Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: SyncClockMessage - Synchronize System Clocks Message
//---------------------------------------------------------------------
//@ Interface-Description
//  The SyncClockMessage is a TaskControlMessage used to synchronize 
//  the BD and GUI clocks.  GUI Task Control creates this message
//  and sends it to BD Task Control when the clock is set, when 
//  communication is established, and at periodic intervals to keep 
//  the BD clock synchronized with the GUI.  The BD synchronizes to the
//  time contained in the message.  This class uses the
//  AppContext callback mechanism to dispatch the message to the
//  receiving task's callback function.
//---------------------------------------------------------------------
//@ Rationale
//  The SyncClockMessage contains the information required to 
//  synchronize the BD and GUI clocks.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class is a derived TaskControlMessage.  The base class new and
//  delete operators override the library operators to allocate memory
//  for the message in the global BufferPool.  The copy constructor is
//  defined so the receiving task can reconstruct the message in its
//  own address space with valid virtual pointers.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  The SyncClockMessage does not account for transmission delays 
//  between the GUI and BD.  Therefore the BD clock synchronizes to
//  the GUI clock after transmission, reception, and processing delays
//  which are assumed small (less than 10ms).  This meets the spec
//  requirement for synchronization within 1 sec.
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Sys-Init/vcssrc/SyncClockMessage.ccv   25.0.4.0   19 Nov 2013 14:33:56   pvcs  $
//
//@ Modification-Log
//
//   Revision 002   By:   sah      Date: 01/13/99    DR Number: 5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to a non-inlined method.
//
//   Revision 001   By:   gdc      Date: 07/06/95    DR Number:   n/a  
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//=====================================================================

//=====================================================================
//
//      Includes...
//
//=====================================================================

#include "SyncClockMessage.hh"
#include <string.h>

//@ Usage-Classes
#include "AppContext.hh"
#include "TimeStamp.hh"
//@ End-Usage

//=====================================================================
//
//      Static Data...
//
//=====================================================================

//@ Code...

//=====================================================================
//
//      Public Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SyncClockMessage [default constructor]
//
//@ Interface-Description
//  The SyncClockMessage class default constructor initializes the
//  TaskControlMessage base class and sets the encapsulated TimeStamp
//  to the current time.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls TaskControlMessage constructor.  Constructs current time
//  TimeStamp.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SyncClockMessage::SyncClockMessage(Boolean userInitiated)
    :  TaskControlMessage(Sys_Init::ID_SYNCCLOCKMESSAGE)
      ,userInitiated_(userInitiated)
      ,timeConstructed_()
{
    CALL_TRACE("SyncClockMessage(void)");
    // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SyncClockMessage [copy constructor]
//
//@ Interface-Description
//  SyncClockMessage copy constructor.  The new SyncClockMessage
//  retains the time constructed from the referenced SyncClockMessage.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Initializes the base class TaskControlMessage from the referenced
//  SyncClockMessage.  Initializes time of construction from the
//  referenced SyncClockMessage time of construction.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SyncClockMessage::SyncClockMessage(const SyncClockMessage& rMessage)
    :  TaskControlMessage(rMessage)
      ,userInitiated_(rMessage.userInitiated_)
      ,timeConstructed_(rMessage.timeConstructed_)
{
    CALL_TRACE("SyncClockMessage(const SyncClockMessage&)");
    // $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~SyncClockMessage [destructor]
//
//@ Interface-Description
//  SyncClockMessage destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SyncClockMessage::~SyncClockMessage()
{
    CALL_TRACE("~SyncClockMessage()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
// Method: nameOf
//
// Interface-Description
//  Returns the name of the class.
//---------------------------------------------------------------------
// Implementation-Description
//  Returns a pointer to the const string containing the class name.
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================
 
const char*
SyncClockMessage::nameOf(void) const
{
    CALL_TRACE("nameOf(void)");
    return "SyncClock";
    // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: dispatch
//
//@ Interface-Description
//  This method dispatches this SyncClockMessage to the callback
//  function registered with the specified AppContext.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls the AppContext dispatch method with a reference to this
//  SyncClockMessage.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
void
SyncClockMessage::dispatch(const AppContext& context) const
{
    CALL_TRACE("dispatch(void)");
    context.dispatch(*this);
    // $[TI1]
}

 

#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//
//@ Interface-Description
//   This method is called when a software fault is detected by the
//   fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//   'lineNumber' are essential pieces of information.  The 'pFileName'
//   and 'pPredicate' strings may be defaulted in the macro to reduce
//   code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This method receives the call for the SoftFault, adds it sub-system
//   and class name ID and sends the message to the non-member function
//   SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
void
SyncClockMessage::SoftFault(const SoftFaultID  softFaultID,
                            const Uint32       lineNumber,
                            const char*        pFileName,
                            const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
 
  FaultHandler::SoftFault(softFaultID, SYS_INIT,
                          Sys_Init::ID_SYNCCLOCKMESSAGE, 
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)

//=====================================================================
//
//      Protected Methods...
//
//=====================================================================

//=====================================================================
//
//      Private Methods...
//
//=====================================================================
