#ifndef IpcIds_HH
#define IpcIds_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1994, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Filename: IpcIds - List of InterProcess Communication (IPC) IDs
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Sys-Init/vcssrc/IpcIds.hhv   25.0.4.2   20 Nov 2013 17:37:40   rhjimen  $
//
//@ Modification-Log
// 
//  Revision: 013  By: rpr    Date:  6-Nov-2012    SCR Number: 6777
//  Project:  BI
//  Description:
//      Removed all monitor software.
// 
//  Revision: 012   By: gdc    Date: 16-Aug-2010     SCR Number: 6663
//  Project: API/MAT
//  Description:
//  Added support for VentSet API.
// 
//  Revision: 011   By: mjfullm    Date:  07-Jun-2007    SCR Number: 6237
//  Project: Trend
//  Description:
//		Added TREND_DATA_MGR_Q, COMPACT_FLASH_MT, and TREND_EVENT_MT 
//
//  Revision: 010   By: gdc    Date:  23-Jan-2001    DCS Number: 5493
//  Project: GuiComms
//  Description:
//		Added SMCP interface queues for aux serial ports.
//
//  Revision: 009   By: gdc    Date:  29-Jun-1999    DCS Number: 5327
//  Project: 840 Neonatal
//  Description:
//		Optimized VGA library for Neonatal project. Used an additional
//  	mutex so lower and upper screens can have separate contexts.
//
//  Revision: 008  By:  gdc    Date:  24-Nov-1997    DCS Number: 2605
//  Project:  Sigma (R8027)
//  Description:
//      Removed DCI queues since DCI no longer contains its own task.
//
//  Revision: 007  By:  sah    Date:  30-Sep-1997    DCS Number: 2530
//  Project:  Sigma (R8027)
//  Description:
//      Added a 'NOVRAM_SEQ_NUM_BUFFER_MT' so that the 'NovRamSequenceNum'
//	class can use the 'DoubleBuffer(TYPE)' template, internally.
//
//  Revision: 006  By:  sah    Date:  25-Jun-1997    DCS Number: 2021
//  Project:  Sigma (R8027)
//  Description:
//      Added a 'TOUCH_TASK_Q' so that the touch task can register into
//      the task-control's state-change mechanism.
//
//  Revision: 005  By:  sah    Date:  25-Jun-1997    DCS Number: 1908
//  Project:  Sigma (R8027)
//  Description:
//      Added three new semaphores for use with double-buffered data
//      items.
//
//  Revision: 004  By:  gdc    Date:  11-JUN-1997    DCS Number: 1902
//  Project:  Sigma (R8027)
//  Description:
//      Added TASK_CONTROL_TRANS_Q for Task Control's transition queue.
//
//  Revision: 003  By:  yyy    Date:  10-May-1997    DCS Number: 2037
//  Project:  Sigma (R8027)
//  Description:
//      Added one new NOVRAM mutex.
//
//  Revision: 002  By:  sah    Date:  09-Apr-1997    DCS Number: 1894
//  Project:  Sigma (R8027)
//  Description:
//      Added two new NOVRAM mutexes.
//
//  Revision: 001  By:  gdc    Date:  01-Mar-1997    DCS Number:  <NONE>
//  Project:  Sigma (R8027)
//  Description:
//      Initial version (Integration baseline).
//
//====================================================================
 
// This list is to ensure uniqueness of Queue, MailBox and MutEx IDs.
// Queues end in _Q, Mutexs in _MT and MailBoxs in _MB

// IMPORTANT!!: Add your new id to the end of the list before MAX_QID,
// so that other libs won't have to be rebuilt due to changing id values.

// PLEASE NOTE: More than the default 16 IPCs and VRTX must be reconfigured 
//              Os.def must override the default queue count:
//  Example:
//              vrtx.queue_count:               30

//@ Type:IpcId
//  Enumeration for each Sigma interprocess communication
//  object including MsgQueue, MutEx, and MailBox.
enum IpcId
{
        NULL_ID                         =  0,   // NULL_ID must be first;
                                                // ie: 0 is not a valid IPC ID
        NOVRAM_SEQ_NUM_BUFFER_MT        =  1,
        TASKING_MT                      =  2,
        NOVRAM_DIAG_LOG_MT              =  3,
        NOVRAM_READ_COUNTER_MT          =  4,
        OSTREAM_MT                      =  5,
        RA_PUTMSG_MT                    =  6,
        USER_ANNUNCIATION_Q             =  7,
        NOVRAM_RESULT_TABLE_ARRAY_MT    =  8,
        NOVRAM_DIAG_LOG_ARRAY_MT        =  9,
        NOVRAM_DATA_CORR_INFO_MT        = 10,
        KEYBOARD_POLL_Q                 = 11,
        TASK_CONTROL_TRANS_Q            = 12,
        DEBUG_IO_Q                      = 13,
        TIMER_TBL_MT                    = 14,
        UPPER_VGA_ACCESS_MT             = 15,
        OPERATING_PARAMETER_EVENT_Q     = 16,
        BUFFER_LIST_MT                  = 17,
        TASK_CONTROL_Q                  = 18,
        TASK_CONTROL_SIM_Q              = 19,
        TIMER_5MS_Q                     = 20,
        SPIROMETRY_TASK_Q               = 21,
        WAVEFORM_TASK_Q                 = 22,
        NOVRAM_FIO2_CAL_INFO_MT         = 23,
        SETTINGS_XACTION_Q              = 25,
        NETAPP_QUEUE_ACCESS_MT          = 26,
        NETAPP_ACKMSG_ACCESS_MT         = 27,
        NETAPP_SENDMSG_ACCESS_MT        = 28,
        LOWER_VGA_ACCESS_MT             = 29,
        TC_BUFFER_POOL_MT               = 30,
        NOVRAM_ALARM_LOG_MT             = 31,
        TREND_DATA_MGR_Q                = 32,
        NOVRAM_SEQUENCE_NUM_MT          = 33,
        NOVRAM_XACTION_Q                = 34,
        SERVICE_MODE_TASK_Q             = 35,
        KNOB_DELTA_MT                   = 36,
        BD_STATUS_TASK_Q                = 37,
        TASK_MONITOR_Q                  = 38,
        BACKGROUND_CYCLE_Q              = 39,
        BREATH_DATA_Q                   = 40,
        SERVICE_MODE_TEST_Q             = 41,
        SERVICE_MODE_ACTION_Q           = 42,
        NOVRAM_DOUBLE_BUFF_MT           = 43,
        NOVRAM_RESULT_TABLE_MT          = 44,
        SERIAL_INTERFACE_Q              = 45,
        SERIAL_INPUT_MT                 = 46,
        SERIAL_OUTPUT_MT                = 47,
        WAVEFORM_DATA_POOL_MT           = 48,
        SERVICE_MODE_GUIIO_Q            = 49,
        COMPACT_FLASH_MT                = 50,
        XMIT_EV_TABLE_TASK_Q            = 51,
        SM_GUI_TASK_CTRL_MSG_Q          = 53,
        TIMER_20MS_Q                    = 55,
        PATIENT_DATA_TIMEOUT_MT         = 56,
        NOVRAM_DIAG_LOG_BUFFER_MT       = 57,
        NOVRAM_RESULT_TABLE_BUFFER_MT   = 58,
        NOVRAM_MISC_DATA_BUFFER_MT      = 59,
        BACKGROUND_FAULT_HANDLER_Q      = 60,
        SERVICE_DATA_TASK_Q             = 61,
        GUIAPP_SCHEDULER_Q              = 62,
        GUIAPP_SCHEDULER_MT             = 63,
        GUIAPP_BREATH_BAR_Q             = 64,
        SERVICE_DATA_MT                 = 65,
        NOVRAM_BD_SYSTEM_STATE_MT       = 66,
		TOUCH_TASK_Q                    = 67,
        SMCP_INTERFACE1_Q               = 68,
        SMCP_INTERFACE2_Q               = 69,
        SMCP_INTERFACE3_Q               = 70,
		TREND_EVENT_MT					= 71,
		VSET_SERVER_MT					= 72,
		GAINS_MANAGER_MT                = 73,
		CONTROLS_TUNING_MT				= 74,
		ADC_CHANNELS_MT					= 75,
MEMORY_HANDLE_MT				= 76,
BD_BATCH_COMMAND_LIST_MT		= 77,
 
        // Add your new initialized ID immediately *above* this line

        MAX_QID                         // MAX_QID must be last;
                                        // ie: total number of IPC Queues
};  

#endif  // IpcIds_HH
