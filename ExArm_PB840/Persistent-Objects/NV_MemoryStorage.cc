#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: NV_MemoryStorage - Nonvolatile memory storage
//---------------------------------------------------------------------
//@ Interface-Description
//  This class provides a a mechanism to store data structures that are
//  intended to be non-volatile (NV). In the original PB840, there were
//  battery backed up static RAM (NOVRAM) and Flash that were used to
//  store data during power off.
//
//  In the 880, there is no NOVRAM and the Flash is the WinCE file system.
//
//  All NV structures will be stored in the WinCE file system.
//
//  Objects of this class are instantiated in the NV_MemoryStorageThread's
//  Initialize method. This method is called during system initialization,
//  prior to starting the various threads. Once initialization is complete,
//  the NV_MemoryStorageThread provides execution context for these objects
//  to update
//---------------------------------------------------------------------
//@ Rationale
//  This class provides an interface to this subsystem, without unduly
//  exposing the internals and dependencies.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ Fault-Handling
//  If memory is not available for allocation, a fault will be thrown.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//

#include "DebugAssert.h"
#include "NV_MemoryStorage.hh"
#include "CrcUtilities.hh"
#include "Task.hh"
#include <fstream>

#if !defined(SIGMA_GUIPC_CPU)
// Vent based location
static const std::string NVRAM_FOLDER = "\\FlashFX Disk\\ProgramFiles\\";
static const std::string NVRAM_FOLDER_BACKUP  = "\\FlashFX Disk2\\ProgramFiles\\";
#else
// Don't worry about a subdirectory or backup partition for the PC based version.
static const std::string NVRAM_FOLDER = "Primary_";
static const std::string NVRAM_FOLDER_BACKUP  = "Backup_";
#endif

static const std::string DATA_FILE_EXTENSION = ".dat";
static const std::string CRC_FILE_EXTENSION = ".crc";

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  NV_MemoryStorage()
//
//@ Interface-Description
//  Class constructor, doesn't do much. Most of the init work is done
//  in Initialize().
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

NV_MemoryStorage::NV_MemoryStorage(void **addressOfAccessPointer, Uint32 memorySize, const std::string& fileName, bool dynamicUpdate)
{
    memorySize_ = memorySize;                       // Save how large a block we're dealing with
    dynamicUpdate_ = dynamicUpdate;                 // If true, update the when a change is detected.. TODO: FUTURE.

    // Allocate the memory that will be used in place of the old NV.
    buffer_ = new uint8_t[memorySize_];
    DEBUG_ASSERT(buffer_);

    // Pass the pointer back so it can be used by the main application.
    *addressOfAccessPointer = buffer_;

    // Allocate the memory that will be used to detect changes.
    bufferCopy_ = new uint8_t[memorySize_];
    DEBUG_ASSERT(bufferCopy_);

    // Construct the full file names (one for the raw data, the other for the CRC -- then duplicated for the backup drive)...
    CreateFileNames_(fileName);

    // If the file load failed, init memory... report an error??
    if (!Load_())
    {
		//There was no content stored on flash.
		//Initialize the memory
        memset(buffer_, 0x55, memorySize_);
    }

    // Save a copy as it currently exists. The bufferCopy_ version is used
    // to detect changes.
    memcpy(bufferCopy_, buffer_, memorySize_);
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ~NV_MemoryStorageThread()
//
//@ Interface-Description
//  If allocations have been made, clean them up.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

NV_MemoryStorage::~NV_MemoryStorage()
{
    delete [] buffer_;
	buffer_ = NULL;
    delete [] bufferCopy_;
	bufferCopy_ = NULL;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  Load_()
//
//@ Interface-Description
//  Called to load buffer_. 
//---------------------------------------------------------------------
//@ Implementation-Description
// Attempt to load from the primary data file first.
// If that fails, attempt from the backup. If that too fails, initialize
// memory and return.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

bool NV_MemoryStorage::Load_()
{
	//Attempt to load from the primary data file
	bool ret = LoadFromFile_(primaryDataFileName_, primaryCrcFileName_);
	if(!ret)
	{
		//Couldnt load from the primary data file. Attempt from the backup.
		ret = LoadFromFile_(backupDataFileName_, backupCrcFileName_);
		if(ret)
		{
			//Successfully loaded from the backup file. Now update the
			//primary data file. We are not altering the return status
			//based on this attempt to copy from backup to primary file,
			//because we are returning a good buffer anyway.
			StoreToFile_(primaryDataFileName_, primaryCrcFileName_);
		}
	}

	return ret;
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  LoadFromFile_()
//
//@ Interface-Description
// Utility API to load the NovRAM data from a data file, and verifying its
// integrity using a CRC file.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

bool NV_MemoryStorage::LoadFromFile_(const std::string& dataFileName, const std::string& crcFileName)
{
	bool ret = false;
	//First load the data
	if(LoadDataFromFile_(dataFileName, buffer_, memorySize_))
	{
		//Data loaded succesfully. Now read in CRC from CRC file
		CrcValue crcStored;
		if(LoadDataFromFile_(crcFileName, &crcStored, sizeof(CrcValue)))
		{
			//Loaded CRC as well. Compute the CRC of the data
			CrcValue crcComputed = CalculateCrc(buffer_, memorySize_);
			//Compare the stored and computed CRCs
			ret = (crcStored == crcComputed);
		}
	}
	return ret;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  Store_()
//
//@ Interface-Description
//  Store the internal buffer to its backing store. 
//---------------------------------------------------------------------
//@ Implementation-Description
// The API writes the internal buffer to both primary and secondry backing
// store
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

bool NV_MemoryStorage::Store_()
{
	//First store the buffer to the primary data file
	bool ret = StoreToFile_(primaryDataFileName_, primaryCrcFileName_);
	//Now, store the buffer to the secondary buffer as well
	bool ret1 = StoreToFile_(backupDataFileName_, backupCrcFileName_);
	UNUSED_SYMBOL(ret1);

	//The return status does not consider the status of storing the 
	//backup. It is done intentionally, so saving of the primary will
	//result in a succesful return.
	return ret;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  StoreMemoryToFile()
//
//@ Interface-Description
//  No Parameters. Accesses class variables only.
//
//---------------------------------------------------------------------
//@ Implementation-Description
//
// Updates the primary and backup storage with current data in RAM.
//
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

bool NV_MemoryStorage::StoreToFile_(const std::string& dataFileName, const std::string& crcFileName)
{
	bool ret = false;
	if(WriteDataToFile_(dataFileName, buffer_, memorySize_))
	{
		//Successfully wrote the data file. Now write the compute the checksum
		//and store it to the CRC file
		CrcValue crc = CalculateCrc(buffer_, memorySize_);
		ret = WriteDataToFile_(crcFileName, &crc, sizeof(CrcValue));
	}

	return ret;
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  CheckForStorageUpdate()
//
//@ Interface-Description
//
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void NV_MemoryStorage::CheckForStorageUpdate_()
{
    // Did something change in the memory array?
	// TODO: E600 It may be more efficient to create the checksum
	// of the current buffer and compare it to the previous value of
	// the checksum. It would reduce memory usage and avoid bringing in
	// two buffers into cache. For now, just sticking with the current
	// implementation of using two buffers and comparing them, because
	// there is nothing to suggest this is not optimum enough.
    if (memcmp(buffer_, bufferCopy_, memorySize_))
    {
        // Save a copy as it currently exists
        memcpy(bufferCopy_, buffer_, memorySize_);

		//TODO: E600 This implementation saves both primary and backup
		//data files every time there is a change. We may not want to
		//save the backup every time. It is a potential optimization, but
		//we dont know if we need it at the moment. So for now save both.
        Store_();       // Store the data as copied to temp area
    }
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  CreateFileNames()
//
//@ Interface-Description
//
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void NV_MemoryStorage::CreateFileNames_(const std::string& fName)
{
    // Put the directory names in place
    primaryDataFileName_ = NVRAM_FOLDER + fName + DATA_FILE_EXTENSION;
    primaryCrcFileName_ = NVRAM_FOLDER + fName + CRC_FILE_EXTENSION;

    backupDataFileName_ = NVRAM_FOLDER_BACKUP + fName + DATA_FILE_EXTENSION;
    backupCrcFileName_ = NVRAM_FOLDER_BACKUP + fName + CRC_FILE_EXTENSION;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  loadDataFromFile_()
//
//@ Interface-Description
// Opens the file specified by the name, and reads the specified amount of
// data from the file. No assumptions are made about the content, and no
// integrity checks are made.
//---------------------------------------------------------------------
//@ Implementation-Description
// Returns 'true' if it read all requested data, 'false' if it didnt get
// ALL the requested data.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
bool NV_MemoryStorage::LoadDataFromFile_(const std::string& fileName, void* buffer, Uint32 bufferSize)
{
	bool ret = false;
	std::ifstream file(fileName.c_str(), std::ios::binary);
	if(file)
	{
		//File opened succesfully. Read from the stream.
		file.read((char*)buffer, bufferSize);
		//Return status is true if we read all the requested bytes.
		ret = (file.gcount() == bufferSize);
	}
	return ret;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  writeDataToFile_()
//
//@ Interface-Description
// Opens the file specified by the name, and overwrites the file content
// with the data provided.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
bool NV_MemoryStorage::WriteDataToFile_(const std::string& fileName, const void* buffer, Uint32 bufferSize)
{
	bool ret = false;
	//Open the file stream in binary mode, and truncate it. All the file writes
	//we do here overwrites the entire file.
	std::ofstream file(fileName.c_str(), std::ios::binary|std::ios::trunc);
	if(file)
	{
		//File opened without error. Write the buffer into it.
		file.write((const char*)buffer, bufferSize);
		//Check if there was any error in stream during the write.
		ret = !file.bad();
	}
	return ret;
}
