#ifndef ScreenBrightnessHandler_HH
#define ScreenBrightnessHandler_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: ScreenBrightnessHandler - Handles setting of Screen Brightness 
// (initiated by the Screen Brightness off-screen key).
//---------------------------------------------------------------------
//@ Version
//@(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/ScreenBrightnessHandler.hhv   25.0.4.0   19 Nov 2013 14:08:20   pvcs  $
// 
//@ Modification-Log
//
//  Revision: 003  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 001  By:  hhd Data:   01-MAY-95   DR Number:
//  Project:  Sigma (R8027) 
//  Description:
//		Integration baseline.
//
//====================================================================

#include "AdjustPanelTarget.hh"
#include "KeyPanelTarget.hh"
#include "SettingObserver.hh"

//@ Usage-Classes
//@ End-Usage

class ScreenBrightnessHandler : public AdjustPanelTarget,
								public KeyPanelTarget,
								public SettingObserver
{
public:
	ScreenBrightnessHandler();
	~ScreenBrightnessHandler(void);

	// AdjustPanelTarget virtual methods
	virtual void adjustPanelKnobDeltaHappened(Int32 delta);
	virtual void adjustPanelAcceptPressHappened(void);
	virtual void adjustPanelClearPressHappened(void);
	virtual void adjustPanelLoseFocusHappened(void);

	// SettingObserver virtual method
    virtual void  valueUpdate(const Notification::ChangeQualifier qualifierId,
							  const SettingSubject*               pSubject);

	// Key Panel Target virtual methods
	virtual void keyPanelPressHappened(KeyPanel::KeyId key);
	virtual void keyPanelReleaseHappened(KeyPanel::KeyId key);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:

private:
	// these methods are purposely declared, but not implemented...
	ScreenBrightnessHandler(const ScreenBrightnessHandler&);// not implemented...
	void   operator=(const ScreenBrightnessHandler&);		// not implemented...

	//@ Constant: MIN_BRIGHTNESS_
	// The minimum brightness value that we should allow to be set by the
	// user.
	static const Uint16 MIN_BRIGHTNESS_;

	//@ Constant: MAX_BRIGHTNESS_
	// The maximum brightness value that we should allow to be set by the
	// user.
	static const Uint16 MAX_BRIGHTNESS_;
};


#endif // ScreenBrightnessHandler_HH
