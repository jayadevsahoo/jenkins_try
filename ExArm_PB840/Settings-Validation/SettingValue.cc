#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: SettingValue - Union of the possible values for a setting setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a union of the three types of values that a setting can
//  contain -- bounded, discrete and sequential.  This type provides a
//  means of storing each of the three possible setting values into a single
//  location/variable.  Instances of this type can be initialized based
//  on any of the three value types, and the value of that instance
//  can be set and queried based on which of the three values is stored.
//  There is no means by which the instances of this union can guarantee
//  that the client operates on the value type that is stored within this
//  instance; it is left as the responsibility of the client to ensure that
//  the value type of these instances are treated consistently.
//---------------------------------------------------------------------
//@ Rationale
//  This provides a generic setting storage type.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This type is the union of a bounded value, a discrete value and 
//  sequential value.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/SettingValue.ccv   25.0.4.0   19 Nov 2013 14:27:44   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//=====================================================================

#include "SettingValue.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SettingValue(void)  [Default Constructor]
//
//@ Interface-Description
//  Construct a default setting value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This is method is needed out-of-line for array initialization.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue::SettingValue(void)
{
	CALL_TRACE("SettingValue()");
}	// $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ~SettingValue()  [Destructor]
//
//@ Interface-Description
//  Destroy this setting value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This is method is needed out-of-line for array initialization.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue::~SettingValue(void)
{
	CALL_TRACE("~SettingValue()");
}

