
#ifndef VentObjectRefs_HH
#define VentObjectRefs_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Header: VentObjectRefs - All the external references of various Vent
//		   object references and its supporting objects.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/VentObjectRefs.hhv   25.0.4.0   19 Nov 2013 13:54:14   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 002  By:  syw    Date: 17-Apr-1997    DR Number: none
//       Project:  Sigma (R8027)
//       Description:
//             Rework per Vent object formal review.
//
//  Revision: 001  By:  syw    Date: 08-Nov-1996    DR Number: INITIAL RELEASE
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

class BinaryIndicator;
extern BinaryIndicator& RAirPressureSwitch;
extern BinaryIndicator& RO2PressureSwitch;
extern BinaryIndicator& RServiceSwitch;

#if 1 //E600_TO_CHECK
class Compressor;
extern Compressor& RCompressor;
#endif

class GasSupply;
extern GasSupply& RGasSupply;

class PowerSource;
extern PowerSource& RPowerSource;

class VentStatus;
extern VentStatus& RVentStatus;

class SystemBattery;
extern SystemBattery& RSystemBattery;

class ExternalBattery;
extern ExternalBattery& RExternalBattery;

class Fio2Monitor;
extern Fio2Monitor& RFio2Monitor;

// E600 BDIO class CompressorEeprom;
// E600 BDIO extern CompressorEeprom& RCompressorEeprom;

class CompressorTimer;
extern CompressorTimer& RCompressorTimer;

class DataKey;
extern DataKey& RDataKey;

#endif // VentObjectRefs_HH

