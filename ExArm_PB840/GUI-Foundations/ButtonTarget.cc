#include "stdafx.h"

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1995, Puritan-Bennett Corporation
//====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: ButtonTarget - An abstract class implementing callbacks from Button
//  objects.
//---------------------------------------------------------------------
//@ Interface-Description
// Any object that receives callbacks from a Button must have this class
// as one of its parent classes.  These callbacks notify the object of
// button selection, deselection, touch, and leave.
//
// Although multiple inheritance is not normally allowed, adding this
// class as an additional parent class is acceptable because its only
// use is to implement a callback interface to another class.
//---------------------------------------------------------------------
//@ Rationale
// This class was created to provide a framework for the callback mechanism for
// the Button class.  Classes derived from this base class will inherit methods
// allowing them to receive callbacks from a Button object.  Also, objects of a
// class derived from this one will be able to register with a Button for
// callback.
//---------------------------------------------------------------------
//@ Implementation-Description
// The member functions are all virtual functions but do nothing.
// The constructor is protected function so it is not possible to create
// an instance of this class.
//---------------------------------------------------------------------
//@ Fault-Handling
// None.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Foundations/vcssrc/ButtonTarget.ccv   25.0.4.0   19 Nov 2013 14:11:40   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By: sah     Date: 21-Sep-1998     DCS Number: 5098
//  Project:  Color
//  Description:
//      Added new method for supporting new bi-state buttons.
//
//  Revision: 002  By: gdc     Date: 16-Jun-1998     DCS Number:
//  Project:  Color
//  Description:
//      Initial release.
//
//  Revision: 001  By: yyy      Date: 01-Jan-1995  DR Number: NONE
//    Project:  Sigma (R8027)
//    Description:
//	Integration baseline.
//=====================================================================

#include "ButtonTarget.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...


//=====================================================================
//
// Public methods
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: buttonDownHappened [virtual]
//
//@ Interface-Description
// This is a virtual method that must be defined in a derived class if
// that class is to serve as a target for callbacks from an instance of the
// Button class.
//
// A Button executes this callback when the button is "selected".  This means
// that the button has been released when it was previously in the
// TOUCHED_WAS_UP state, so it is now in the DOWN state.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
ButtonTarget::buttonDownHappened(Button *pButton, Boolean isByOperatorAction)
{
	CALL_TRACE("ButtonTarget::buttonDownHappened(Button *pButton, Boolean isByOperatorAction)");
	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: buttonUpHappened [virtual]
//
//@ Interface-Description
// This is a virtual method that does nothing if that class is to serve as a
// target for callbacks from the Button class.
//
// A Button executes this callback when the button is "deselected".  This means
// that the button has been released when it was previously in the
// TOUCHED_WAS_DOWN state, so it is now in the UP state.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
ButtonTarget::buttonUpHappened(Button *pButton, Boolean isByOperatorAction)
{
	CALL_TRACE("ButtonTarget::buttonUpHappened(Button *pButton, Boolean isByOperatorAction)");
	// Do nothing
}


//=====================================================================
//
// Protected methods
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ButtonTarget  [Default Constructor]
//
//@ Interface-Description
// Construct and initialize a ButtonTarget object.
// This is an abstract base class, so this constructor does nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
// This is an abstract base class, so this constructor needs to do nothing.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ButtonTarget::ButtonTarget(void)
{
	CALL_TRACE("ButtonTarget::ButtonTarget(void)");
	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~ButtonTarget  [Destructor]
//
//@ Interface-Description
// Destroy object.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This destructor does nothing.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ButtonTarget::~ButtonTarget(void)
{
	CALL_TRACE("ButtonTarget::~ButtonTarget(void)");
	// Do nothing
}

