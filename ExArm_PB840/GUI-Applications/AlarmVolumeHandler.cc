#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: AlarmVolumeHandler - Handles setting of Alarm Volume
// (initiated by the Alarm Volume off-screen key).
//---------------------------------------------------------------------
//@ Interface-Description
// This is a class dedicated to the setting of the Alarm Volume.  The
// Alarm Volume is set by pressing down the Alarm Volume offscreen key and,
// while keeping it pressed, using the rotary knob to adjust.  An alarm
// volume tone is sounded while the key is pressed, giving the user
// immediate feedback on the result of volume adjustments.
//
// The keyPanelPressHappened() and keyPanelReleaseHappened() methods
// inform us of Alarm Volume key events.  We use the adjustPanel's methods
// to trap knob events and valueUpdate() tells us when the
// Alarm Volume setting changes so that we can communicate this event
// to the GUI-Foundation sound system.
//---------------------------------------------------------------------
//@ Rationale
// Something needs to handle the Alarm Volume key and this is it.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply register for Alarm Volume Key event callbacks and inform
// the Breath Delivery system via an ambassador object of any key press
// event.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
// Only one instance of this class should be created.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/AlarmVolumeHandler.ccv   25.0.4.0   19 Nov 2013 14:07:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 007   By: mnr    Date: 28-Dec-2009    SCR Number: 6437
//  Project:  NEO
//  Description:
//      Requirement tracing number added to last change.
//
//  Revision: 006   By: mnr    Date: 23-Nov-2009    SCR Number: 6437
//  Project:  NEO
//  Description:
//      Post Alarm Volume changed Trending Event.
//
//  Revision: 005   By: gdc   Date:  09-May-2007    SCR Number: 6330
//  Project:  Trend
//  Description:
//      Removed SUN prototype code.
//
//  Revision: 004   By: gdc   Date:  09-May-2007    SCR Number: 6237
//  Project:  Trend
//  Description:
//      Added getTargetType to identify feedback sensitive focus.
//
//  Revision: 003  By: sah    Date:  07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "AlarmVolumeHandler.hh"


//@ Usage-Classes
#include "AdjustPanel.hh"
#include "BoundStrs.hh"
#include "KeyPanel.hh"
#include "PromptArea.hh"
#include "Sound.hh"
#include "MiscStrs.hh"
#include "MessageArea.hh"
#include "SettingSubject.hh"
#include "BoundStatus.hh"
#include "TrendDataMgr.hh"
#include "TrendEvents.hh"
//@ End-Usage

//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: AlarmVolumeHandler()  [Default Constructor]
//
//@ Interface-Description
// Creates the Alarm Volume Handler object.
//---------------------------------------------------------------------
//@ Implementation-Description
// Register for Alarm Volume Key event callbacks and Alarm Volume setting
// changes.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

AlarmVolumeHandler::AlarmVolumeHandler(void)
{
	CALL_TRACE("AlarmVolumeHandler::AlarmVolumeHandler(void)");

	// Register for Alarm Volume key presses
	KeyPanel::SetCallback(KeyPanel::ALARM_VOLUME_KEY, this);

	// Register for Alarm Volume setting changes
	attachToSubject_(SettingId::ALARM_VOLUME, Notification::VALUE_CHANGED);
														// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~AlarmVolumeHandler  [Destructor]
//
//@ Interface-Description
// Destroys the Alarm Volume Handler object.  Does nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

AlarmVolumeHandler::~AlarmVolumeHandler(void)
{
	CALL_TRACE("AlarmVolumeHandler::~AlarmVolumeHandler(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: keyPanelPressHappened
//
//@ Interface-Description
// Called when the Alarm Volume key is pressed.  We begin the Alarm Volume
// adjustment process.
//---------------------------------------------------------------------
//@ Implementation-Description
// We grab the Adjust Panel focus (for knob events),  start the Alarm
// Volume sound, disable the knob rotate sound as its not needed for feedback
// in this instance  and post appropriate prompts.
//
// Note: pressing of these keys is invalid during service mode so just make
// the invalid entry sound in this case.
// $[01241] When the Alarm Volume key is pressed down an alarm sound ...
// $[01242] A prompt shall be displayed while the key is pressed down ...
// $[01243] The Alarm Volume sound will be made even if alarm silence active.
// $[01217] The alarm volume should be produced as long as the user ...
// $[01218] The alarm volume sound shall be distinct from the Hight, medium ...
// $[07034] During service mide, Alarm silence, alarm reset, help, ...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
AlarmVolumeHandler::keyPanelPressHappened(KeyPanel::KeyId)
{
	CALL_TRACE("AlarmVolumeHandler::keyPanelPressHappened(KeyPanel::KeyId)");

	// Only react to key during normal ventilation mode.
	if (GuiApp::GetGuiState() != STATE_SERVICE &&
		GuiApp::GetGuiState() != STATE_SST &&
		GuiApp::GetGuiState() != STATE_INIT)
	{													// $[TI1]
		// Grab the Adjust Panel focus
		// $[01350] The knob sound shall be produced each time knob ...
		AdjustPanel::TakePersistentFocus(this);

		// Sound the Alarm Volume tone
		Sound::Start(Sound::ALARM_VOLUME);

		// Disable knob rotate sound
		// $[01350] The knob sound shall be produced each time knob ...
		Sound::DisableKnobRotateSound();

		// Display a Primary and Secondary prompt, and clear Advisory prompt
		GuiApp::PPromptArea->setPrompt( PromptArea::PA_PRIMARY,
						PromptArea::PA_HIGH, PromptStrs::USE_KNOB_TO_ADJUST_P);
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
						PromptArea::PA_HIGH, PromptStrs::ALARM_VOLUME_CANCEL_S);
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_ADVISORY,
						PromptArea::PA_HIGH, PromptStrs::EMPTY_A);
	}
	else
	{													// $[TI2]
		// Illegal key press during service mode
		Sound::Start(Sound::INVALID_ENTRY);
	}

	// Display the offscreen key help message 
	GuiApp::PMessageArea->setMessage(
							MiscStrs::ALARM_VOLUME_HELP_MESSAGE,
									MessageArea::MA_HIGH);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: keyPanelReleaseHappened
//
//@ Interface-Description
// Called when the Alarm Volume key is released.  Stop the Alarm Volume
// adjustment.
//---------------------------------------------------------------------
//@ Implementation-Description
// Clear the adjust panel focus and turn off the offscreen help message.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
AlarmVolumeHandler::keyPanelReleaseHappened(KeyPanel::KeyId)
{
	CALL_TRACE("AlarmVolumeHandler::keyPanelReleaseHappened(KeyPanel::KeyId)");

	// Clear the Adjust Panel focus
	AdjustPanel::TakePersistentFocus(NULL);

	// Clear the off screen key message
	GuiApp::PMessageArea->clearMessage(MessageArea::MA_HIGH);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelKnobDeltaHappened
//
//@ Interface-Description
// This is a virtual method inherited from being an AdjustPanelTarget.  If this
// object has the Adjust Panel focus (as we do during the Alarm Volume
// adjustment) then we will be called when the operator turns the knob.  It is
// passed an integer corresponding to the amount the knob was turned.  We
// inform the Settings-Validation subsystem of the delta value and any
// hard bound violation is displayed in the Prompt Area.
//---------------------------------------------------------------------
//@ Implementation-Description
// Any deltas received by this button are sent immediately to the setting via
// the calcNewValue() method of alarmVolumeHandle_.  If the delta causes this
// setting to change then an update will be passed by the Settings-Validation
// subsystem back to us via the SettingObserver before the calcNewValue()
// method returns.  The return value of calcNewValue() indicates the current
// hard bound status of the Alarm Volume setting.  If there is a "hard bound
// violation" we will display an Advisory message in the Prompt Area.
//
// $[01240] The Alarm Volume key shall allow volume adjustment of ...
// $[01241] As the user turns the knob the volume shall change ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
AlarmVolumeHandler::adjustPanelKnobDeltaHappened(Int32 delta)
{
	CALL_TRACE("AlarmVolumeHandler::adjustPanelKnobDeltaHappened(Int32 delta)");

	SettingSubject*  pSubject = getSubjectPtr_(SettingId::ALARM_VOLUME);

	// Pass the knob delta on to the Alarm Volume setting
	const BoundStatus&  boundStatus = pSubject->calcNewValue(delta);

	// Get the string id appropriate to the hard bound id (if any)
	StringId hardBoundStr =
			BoundStrs::GetSettingBoundString(boundStatus.getBoundId());

	// Display the Advisory message corresponding to the hard bound id
	GuiApp::PPromptArea->setPrompt(PromptArea::PA_ADVISORY, PromptArea::PA_HIGH,
							hardBoundStr == NULL_STRING_ID ?
										PromptStrs::EMPTY_A : hardBoundStr);

	// If a hard bound was struck then make the Invalid Entry sound
	if (hardBoundStr != NULL_STRING_ID)
	{													// $[TI1]
		Sound::Start(Sound::INVALID_ENTRY);
	}													// $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelAcceptPressHappened
//
//@ Interface-Description
// This is a virtual method inherited from being an AdjustPanelTarget.  It is
// called when the operator presses down the Accept key when we have the
// Adjust Panel focus.  This is an invalid operation so sound the Invalid
// Entry sound.
//---------------------------------------------------------------------
//@ Implementation-Description
// Tell GUI-Foundation's Sound class to make the Invalid Entry sound.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
AlarmVolumeHandler::adjustPanelAcceptPressHappened(void)
{
	CALL_TRACE("AlarmVolumeHandler::adjustPanelAcceptPressHappened(void)");

	// Make the Invalid Entry sound
	Sound::Start(Sound::INVALID_ENTRY);					// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelClearPressHappened
//
//@ Interface-Description
// This is a virtual method inherited from being an AdjustPanelTarget.  It is
// called when the operator presses the Clear key.  This is an invalid
// operation so sound the Invalid Entry sound.
//---------------------------------------------------------------------
//@ Implementation-Description
// Tell GUI-Foundation's Sound class to make the Invalid Entry sound.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
AlarmVolumeHandler::adjustPanelClearPressHappened(void)
{
	CALL_TRACE("AlarmVolumeHandler::adjustPanelClearPressHappened(void)");

	// Make the Contact sound
	Sound::Start(Sound::INVALID_ENTRY);					// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelLoseFocusHappened
//
//@ Interface-Description
// This is a virtual method inherited from being an AdjustPanelTarget.
// It is called when the Alarm Volume Handler is about to lose the focus of
// the Adjust Panel.  This normally happens when some other button is
// pressed down or when the Alarm Volume key is pressed up.  We must
// terminate the Alarm Volume adjustment.
//---------------------------------------------------------------------
//@ Implementation-Description
// We tell GUI-Foundation's Sound class to stop sounding the Alarm Volume
// tone and remove any prompts we may have displayed.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
AlarmVolumeHandler::adjustPanelLoseFocusHappened(void)
{
	CALL_TRACE("AlarmVolumeHandler::adjustPanelLoseFocusHappened(void)");

	// Cancel the Alarm Volume tone
	Sound::CancelAlarmVolume();

	// Clear the Primary, Secondary and Advisory prompts
	GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY,
										PromptArea::PA_HIGH, NULL_STRING_ID);
	GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
										PromptArea::PA_HIGH, NULL_STRING_ID);
	GuiApp::PPromptArea->setPrompt(PromptArea::PA_ADVISORY,
										PromptArea::PA_HIGH, NULL_STRING_ID);
														// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getTargetType [virtual]
//
//@ Interface-Description
// Returns the AdjustPanelTarget::TargetType for this object. Virtual
// function of AdjustPanelTarget.
//---------------------------------------------------------------------
//@ Implementation-Description
// Returns TARGET_TYPE_SETTING for this alarm volume setting handler.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

AdjustPanelTarget::TargetType AlarmVolumeHandler::getTargetType(void) const
{
	CALL_TRACE("getTargetType(void)");

	return (AdjustPanelTarget::TARGET_TYPE_SETTING);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: valueUpdate
//
//@ Interface-Description
//  Called when setting values on this subscreen has changed.
// >Von
//  qualifierId	The context in which the change happened, either
//					ACCEPTED or ADJUSTED.
//  pSubject	The pointer to the Setting's Context subject.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Set the alarm volume to the new value.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
AlarmVolumeHandler::valueUpdate(const Notification::ChangeQualifier qualifierId,
							    const SettingSubject*               pSubject)
{
	CALL_TRACE("valueUpdate(qualifierId, pSubject)");

	AUX_CLASS_PRE_CONDITION((SettingId::ALARM_VOLUME == pSubject->getId()),
						    pSubject->getId());

	// Get the current value of the Alarm Volume setting
	const SequentialValue  ALARM_VOLUME = pSubject->getAcceptedValue();

	// [TR01179] Post related trending event 
	TrendDataMgr::PostEvent( TrendEvents::AUTO_EVENT_ALARM_VOLUME_CHANGE );

	// Pass it on to the Sound system
	Sound::SetAlarmVolume(Int32(ALARM_VOLUME));			// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
AlarmVolumeHandler::SoftFault(const SoftFaultID  softFaultID,
								    const Uint32       lineNumber,
								    const char*        pFileName,
								    const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, ALARMVOLUMEHANDLER,
							lineNumber, pFileName, pPredicate);
}
