#ifndef cktable_HH
#define cktable_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================ H E A D E R   D E S C R I P T I O N ====
//@ Filename: cktable - A "C" header file for the POST Checksum Table
//
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Kernel/vcssrc/cktable.hhv   25.0.4.0   19 Nov 2013 14:13:54   pvcs  $
//
//@ Modification-Log
//
//
//  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//=====================================================================


static const Int32 NUMBER_OF_CHECKSUM_BLOCKS = 16;

typedef struct
{
   Uint32   length;        	// length of checksum block
   Uint32   *pStartAddress;	// starting address
   Uint32   additiveChecksum; 	// additive the checksum
   Uint32   crc32Checksum;     	// crc checksum
} ChecksumStruct;


typedef struct
{
   Uint32		numChecksums;
   ChecksumStruct 	checksumBlock[NUMBER_OF_CHECKSUM_BLOCKS];
   Uint32  *endImageAddr;
   char magicCookie[30];
} EepromStruct;


//@ Begin-Free-Declarations

extern const EepromStruct *PEePromChecksums;

//@ End-Free-Declarations


#endif // cktable_HH
