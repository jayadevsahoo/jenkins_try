#include "stdafx.h"
//====================================================================
// This is a proprietary work to which Nellcor Puritan Bennett
// corporation of California claims exclusive right.  No part of this
// work may be used, disclosed, reproduced, sorted in an information
// retrieval system, or transmitted by any means, electronic,
// mechanical, photocopying, recording, or otherwise without the prior
// written permission of Nellcor Puritan Bennett Corporation.
//
//     Copyright (c) 1995, Nellcor Puritan Bennett Corporation
//=====================================================================


//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: Exception - Sigma Exception Handler Class
//---------------------------------------------------------------------
//@ Interface-Description
//  This class contains the methods related to processor exceptions
//  in the 840 system.  It processes power fail interrupts as well as
//  level 7 (NMI) interrupts and other exceptional conditions.  These
//  exception processing functions are called by the exception
//  "router" found in the "exceptions" module.  The exception router
//  is an assembly routine that initially decodes the exception and
//  calls the appropriate exception processing function in this class.
//
//  In addition to processing exceptions, this class contains methods
//  to generate character strings describing the interrupt used by
//  GUI-Applications to display the cause of the interrupt in the
//  System Diagnostic Log.
//
//---------------------------------------------------------------------
//@ Rationale
//  This class contains methods for processing exceptions.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Operating-System/vcssrc/Exception.ccv   25.0.4.0   19 Nov 2013 14:16:18   pvcs  $
//
//@ Modification-Log
//
//   Revision: 004   By: gdc    Date: 16-Aug-2010     SCR Number: 6663
//      Project: API/MAT
//      Description:
//          Corrected INTERRUPT_NAME table to include names for vectors
//          from second serial channel interrupts. Development only change.
//
//   Revision 003  By: Gary Cederquist  Date: 05-Oct-1999  DR Number: 5507
//      Project:   ATC
//      Description:
//			Toggled LED's during power fail interrupt as signal to
//			GUI for power-fail.
//
//   Revision 002  By: Gary Cederquist  Date: 19-NOV-1997  DR Number: 2394
//      Project:   Sigma   (R8027)
//      Description:
//         Capture the state of the power switch earlier in the power
//         fail exception handler to minimize the chance of reading a
//         "bounce" in the switch which can lead to a short power fail
//         recovery instead of a long (AC switch) power fail recovery.
//
//   Revision 001  By: Gary Cederquist  Date: 28-MAY-1997  DR Number: 2176
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version.
//
//=====================================================================

//=====================================================================
//
//      Includes...
//
//=====================================================================

#include <cpu.h>
#include <vrtxil.h>
#include <stdio.h>
#include <string.h>
#include "DiagnosticCode.hh"
#include "Exception.hh"
#include "FaultHandler.hh"
#include "InitiateReboot.hh"
#include "NmiSource.hh"
#include "Post.hh"
#include "Post_Library.hh"
#include "exceptions.hh"

//@ Usage-Classes
//@ End-Usage

//=====================================================================
//
//      Static Data...
//
//=====================================================================

#if defined( SIGMA_GUI_CPU ) || defined( SIGMA_DEVELOPMENT )
static const char * INTERRUPT_NAME[256] =
{
    "Reset Initial Interrupt Stack Pointer"          //  0
    , "Reset Initial Program Counter"                  //  1
    , "Bus Error / Access Fault"                       //  2
    , "Address Error"                                  //  3
    , "Illegal Instruction"                            //  4
    , "Integer Divide by Zero"                         //  5
    , "CHK, CHK2 Instructions"                         //  6
    , "FTRAPcc, TRAPcc, TRAPV Instructions"            //  7
    , "Privilege Violation"                            //  8
    , "Trace"                                          //  9
    , "Line 1010 Emulator (Unimplemented A-Line Opcode)"  //  10
    , "Line 1111 Emulator (Unimplemented F-Line Opcode)"  //  11
    , "(Unassigned, Reserved)"                         //  12
    , "(Unassigned, Reserved)"                         //  13
    , "Format Error"                                   //  14
    , "Uninitialized Interrupt"                        //  15
    , "(Unassigned, Reserved)"                         //  16
    , "(Unassigned, Reserved)"                         //  17
    , "(Unassigned, Reserved)"                         //  18
    , "(Unassigned, Reserved)"                         //  19
    , "(Unassigned, Reserved)"                         //  20
    , "(Unassigned, Reserved)"                         //  21
    , "(Unassigned, Reserved)"                         //  22
    , "(Unassigned, Reserved)"                         //  23
    , "Spurious Interrupt"                             //  24
    , "Level 1 Autovector"                             //  25
    , "Level 2 Autovector"                             //  26
    , "Level 3 Autovector / 82C54 Timer 0"             //  27
    , "Level 4 Autovector / 82C54 Timer 1"             //  28
    , "Level 5 Autovector / Ethernet "                 //  29
    , "Level 6 Autovector / 82C54 Timer 2"             //  30
    , "Level 7 Autovector / NMI"                       //  31
    , "Trap 0  Instruction Vector"                     //  32
    , "Trap 1  Instruction Vector"                     //  33
    , "Trap 2  Instruction Vector"                     //  34
    , "Trap 3  Instruction Vector"                     //  35
    , "Trap 4  Instruction Vector"                     //  36
    , "Trap 5  Instruction Vector"                     //  37
    , "Trap 6  Instruction Vector"                     //  38
    , "Trap 7  Instruction Vector"                     //  39
    , "Trap 8  Instruction Vector"                     //  40
    , "Trap 9  Instruction Vector"                     //  41
    , "Trap 10 Instruction Vector"                     //  42
    , "Trap 11 Instruction Vector"                     //  43
    , "Trap 12 Instruction Vector"                     //  44
    , "Trap 13 Instruction Vector"                     //  45
    , "Trap 14 Instruction Vector"                     //  46
    , "Trap 15 Instruction Vector"                     //  47
    , "FP Branch or Set on Unordered Condition"        //  48
    , "FP Inexact Result"                              //  49
    , "FP Divide by Zero"                              //  50
    , "FP Underflow"                                   //  51
    , "FP Operand Error"                               //  52
    , "FP Overflow"                                    //  53
    , "FP Signaling NAN"                               //  54
    , "FP Unimplemented Data Type"                     //  55
    , "(Unassigned, Reserved)"                         //  56
    , "(Unassigned, Reserved)"                         //  57
    , "(Unassigned, Reserved)"                         //  58
    , "(Unassigned, Reserved)"                         //  59
    , "(Unassigned, Reserved)"                         //  60
    , "(Unassigned, Reserved)"                         //  61
    , "(Unassigned, Reserved)"                         //  62
    , "(Unassigned, Reserved)"                         //  63
    , "MC68901-0 Rotary Encoder"                       //  64
    , "MC68901-1 Touch Screen"                         //  65
    , "MC68901-2"                                      //  66
    , "MC68901-3"                                      //  67
    , "MC68901-4  901 Timer D"                         //  68
    , "MC68901-5  901 Timer C"                         //  69
    , "MC68901-6"                                      //  70
    , "MC68901-7"                                      //  71
    , "MC68901-8  901 Timer B"                         //  72
    , "MC68901-9"                                      //  73
    , "MC68901-10"                                     //  74
    , "MC68901-11"                                     //  75
    , "MC68901-12"                                     //  76
    , "MC68901-13"                                     //  77
    , "MC68901-14"                                     //  78
    , "MC68901-15 82C54 Timer 3&4"                     //  79

    , "Z85230-1 Ch B Transmit Buffer Empty"              //  80
    , "Illegal Z85230-1 Ch B Interrupt"                  //  81
    , "Z85230-1 Ch B External/Status Change"             //  82
    , "Illegal Z85230-1 Ch B Interrupt"                  //  83
    , "Z85230-1 Ch B Receive Character Available"        //  84
    , "Illegal Z85230-1 Ch B Interrupt"                  //  85
    , "Z85230-1 Ch B Special Receive Condition"          //  86
    , "Illegal Z85230-1 Ch B Interrupt"                  //  87
    , "Z85230-1 Ch A Transmit Buffer Empty"              //  88
    , "Illegal Z85230-1 Ch A Interrupt"                  //  89
    , "Z85230-1 Ch A External/Status Change"             //  90
    , "Illegal Z85230-1 Ch A Interrupt"                  //  91
    , "Z85230-1 Ch A Receive Character Ready"            //  92
    , "Illegal Z85230-1 Ch A Interrupt"                  //  93
    , "Z85230-1 Ch A Special Receive Condition"          //  94
    , "Illegal Z85230-1 Ch A Interrupt"                  //  95

    , "Z85230-2 Ch B Transmit Buffer Empty"              //  96
    , "Illegal Z85230-2 Ch B Interrupt"                  //  97
    , "Z85230-2 Ch B External/Status Change"             //  98
    , "Illegal Z85230-2 Ch B Interrupt"                  //  99
    , "Z85230-2 Ch B Receive Character Available"        //  100
    , "Illegal Z85230-2 Ch B Interrupt"                  //  101
    , "Z85230-2 Ch B Special Receive Condition"          //  102
    , "Illegal Z85230-2 Ch B Interrupt"                  //  103
    , "Z85230-2 Ch A Transmit Buffer Empty"              //  104
    , "Illegal Z85230-2 Ch A Interrupt"                  //  105
    , "Z85230-2 Ch A External/Status Change"             //  106
    , "Illegal Z85230-2 Ch A Interrupt"                  //  107
    , "Z85230-2 Ch A Receive Character Ready"            //  108
    , "Illegal Z85230-2 Ch A Interrupt"                  //  109
    , "Z85230-2 Ch A Special Receive Condition"          //  110
    , "Illegal Z85230-2 Ch A Interrupt"                  //  111


    , NULL,NULL,NULL,NULL
    , NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL
    , NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL
    , NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL
    , NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL
    , NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL
    , NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL
    , NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL
    , NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL
    , NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL
    , NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL
    , NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL
    , NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL
    , NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL
    , NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL

};
#endif // SIGMA_DEVELOPMENT || SIGMA_GUI_CPU

#if defined( SIGMA_DEVELOPMENT )
static const Uint LEVEL7_LOG_ENTRY_SIZE = 128;  // approximate maximum
char Level7Log[4*LEVEL7_LOG_ENTRY_SIZE];

static const Uint LOG_ENTRY_SIZE = 1024;  // approximate maximum
char ExceptionLog[32*LOG_ENTRY_SIZE];
#endif // SIGMA_DEVELOPMENT


typedef void (*EntryPtr)(void);

#include "BdIoUtil.hh"

#if defined(SIGMA_UNIT_TEST)
extern  Uint16  SimAdcSystemStatusPort;
extern  Uint16  SimAdcErrorStatusPort;

    #define ADC_SYSTEM_STATUS_PORT  (&SimAdcSystemStatusPort)
    #define ADC_ERROR_STATUS_PORT   (&SimAdcErrorStatusPort)
#endif

struct  HandlerRec
{
    Uint8        vectorNumber;
    EntryPtr     entryPt;
};

static const HandlerRec  Handlers_[] =
{
    {  2, SigmaExceptionCatch}      // Access Fault
    ,{ 11, SigmaFlineCatch}      // F-Line Opcode
    ,{ 31, SigmaLevel7Catch}      // AV 7
    ,{ 33, SigmaTrapHandler}      // Sigma TRAP #1
};

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: InstallHandlers
//
//@ Interface-Description
//  This method installs the exception vectors for the Sigma exception
//  handlers.  By the time this method is called, VRTX has installed
//  its exception handlers for the run time library including the logio
//  library and floating point library exception handler for
//  unimplemented floating point instructions.  This method installs
//  the exception handlers for truly exceptional conditions such as an
//  illegal instruction or divide by zero.  If an exception vector
//  contains the same vector as the illegal instruction or spurious
//  interrupt handlers, then this function replaces it with the
//  standard Sigma exception handler SigmaExceptionCatch.  For other
//  exceptions, the Handlers_ struct contains the Sigma exception
//  handlers that replace the current handler.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
Exception::InstallHandlers(void)
{
    static  Boolean handlersInstalled = FALSE;

    if ( !handlersInstalled )
    {   // $[TI3.1]
        //  The EVT contains the handler addresses for each 68040
        //  exception.  The interrupt is "vectored" through the EVT
        //  to the appropriate handler by retrieving the handler
        //  address from the EVT entry corresponding to the interrupting
        //  vector number.

        //  Get the current contents of the vector base register (VBR)
        //  which points to the event vector table (EVT).

        EntryPtr * vbr = (EntryPtr*)cpu_get_physical_vbr();

        const Uint8 ILLEGAL_INSTRUCTION_VECTOR = 4;
        const Uint8 SPURIOUS_INTERRUPT_VECTOR  = 24;

        EntryPtr  vrtxExceptionCatch  = vbr[ILLEGAL_INSTRUCTION_VECTOR];
        EntryPtr  vrtxExceptionIgnore = vbr[SPURIOUS_INTERRUPT_VECTOR];

        //  Replace each exception handler in the EVT with our own,
        //  but first save the current exception handler address in
        //  the SigmaVirtualEvt.  SigmaVirtualEvt is used by the
        //  Sigma exception handler to call the standard VRTX exception
        //  handler after trapping the exception itself first.  This
        //  feature is used specifically by the Sigma F-Line opcode
        //  exception handler to trap an illegal F-Line opcode but pass
        //  on unimplemented floating-point operations to the VRTX
        //  run-time library.

        for (int i=0; i<256; i++)
#ifdef SIGMA_DEVELOPMENT
#define A_LINE_OPCODE_VECTOR  (10)
            //  don't catch breakpoints (A-Line Opcode) during development
            if (i != A_LINE_OPCODE_VECTOR)
#endif
            {
                for (int j=0; j<countof(Handlers_); j++)
                {
                    if (Handlers_[j].vectorNumber == i)
                    {
                        // $[TI1.1]
                        SigmaVirtualEvt[i] = vbr[i];
                        vbr[i] = Handlers_[j].entryPt;
                        break;
                    }
                    // $[TI1.2]  implied else
                }
                if ( vbr[i] == vrtxExceptionCatch || vbr[i] == vrtxExceptionIgnore )
                {
                    // $[TI2.1]
                    SigmaVirtualEvt[i] = vbr[i];
                    vbr[i] = SigmaExceptionCatch;
                }
                // $[TI2.2]  implied else
            }
        handlersInstalled = TRUE;
    }
    // $[TI3.2]  implied else
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: PowerFail
//
//@ Interface-Description
//  This method is called by the exception "router" upon receiving a
//  power fail interrupt.  Since a power failure can occur at any time
//  including prior to the Task Monitor clearing the POST rolling
//  thunder count, this handler clears this count automatically when a
//  power failure occurs.  This method is only defined and called on
//  the BD processor since only the BD contains the power-fail
//  interrupt circuitry.  If the power failure occurs prior to a
//  successful settings transaction to the BD as indicated by
//  Post::IsSysInitComplete(), the power down time is not updated since
//  we don't want to extend the downtime interval beyond 5 minutes due
//  to multiple power failures that could occur prior to receiving
//  settings and completing system initialization.  This method sets
//  the shutdown state to either POWERFAIL or ACSWITCH depending on the
//  state of the front panel switch.  If the switch is still on when
//  the power fail interrupt occurs, it sets the shutdown state to
//  POWERFAIL otherwise it sets it to ACSWITCH.  InitiateReboot() waits
//  for the power to decay until the power sentry resets the processor
//  or until the watchdog timer resets the processor after 250ms.  This
//  method does not return.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

#if defined( SIGMA_BD_CPU )
void
Exception::PowerFail(void)
{
    Boolean switchOn = ( *ADC_SYSTEM_STATUS_PORT & POWER_SWITCH_ON_MASK ) != 0;

    Post::ClearRollingThunderCount();

    // toggle the LEDs to the GUI for a power fail signal
    // the following loop has been scoped out at a 19.9kHz rate
    // with instruction and data cache on
    for (Uint i=0; i<10; i++)
    {
        SwitchLeds( Boolean(i&1) );
        for (Uint j=0; j<100; j++)
        {
            // do nothing - spin loop
        }
    }

    if ( Post::IsSysInitComplete() )
    {
        // $[TI1.1]
        Post::SetPowerDownTime_();
    }
    // $[TI1.2] implied else

    if ( switchOn )
    {
        // $[TI2.1]
        InitiateReboot(POWERFAIL);
    }
    else
    {
        // $[TI2.2]
        InitiateReboot(ACSWITCH);
    }
}
#endif // SIGMA_BD_CPU


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: DecodeAutovector7
//
//@ Interface-Description
//  This method is called by the exception "router" upon receiving a
//  non-powerfail level 7 (NMI) interrupt.  This method calls the
//  FaultHandler to log the interrupt in the System Diagnostic Log.
//  It then reboots the processor setting the shutdown state to
//  EXCEPTION which the POST Reset Umpire will handle as an unexpected
//  reset.  This method does not return.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
Exception::DecodeAutovector7(const Byte nmiSource)
{
    Uint16 errorCode = 0;

#if defined( SIGMA_BD_CPU )
    if (nmiSource & NMI_ANALOG_INTERFACE_ERROR)
    {
        // $[TI1.1]
        errorCode = *ADC_ERROR_STATUS_PORT;
    }
    // $[TI1.2]  implied else
#endif // SIGMA_BD_CPU


#if defined( SIGMA_DEVELOPMENT )
    static char *S = Level7Log;

    if (S > &Level7Log[sizeof(Level7Log)-LEVEL7_LOG_ENTRY_SIZE])
    {
        S = (char*)((Uint32)S + 15 & 0xfffffff0);
        sprintf(S, "**LOG OVERFLOW**");
    }
    else
    {
        S = (char*)((Uint32)S + 15 & 0xfffffff0);

        S += sprintf(S, "Level 7 Auto-Vector; NMI Source = %x", nmiSource );

        // Level 7 Autovector Interrupt always generates a Format $0 stack frame

#if defined( SIGMA_GUI_CPU )
        if (nmiSource & NMI_OVER_5VDC)
            S += sprintf(S, "; +5 VDC Overvoltage");
        if (nmiSource & NMI_OVER_12VDC)
            S += sprintf(S, "; +12 VDC Overvoltage");
        if (nmiSource & NMI_UNDER_12VDC)
            S += sprintf(S, "; +12 VDC Undervoltage");
        if (nmiSource & NMI_SAAS_FAILURE)
            S += sprintf(S, "; SAAS Failure");
#endif // SIGMA_GUI_CPU

#if defined( SIGMA_BD_CPU )
        if (nmiSource & NMI_ANALOG_INTERFACE_ERROR)
        {
            S += sprintf(S, "; Analog Interface Error");
            Uint16 error = *ADC_ERROR_STATUS_PORT;
            if (error & AIF_ADC_TIMING_FAULT)
                S += sprintf(S, "; ADC Timing Fault");
            if (error & AIF_ADC_CHANNEL_SEQUENCER_FAULT)
                S += sprintf(S, "; ADC Channel Sequencer Fault");
            if (error & AIF_HAMMING_DECODE_FAULT)
                S += sprintf(S, "; Hamming Decode Fault");
        }
        //  Power Fail is handled in Exception::PowerFail()
#endif // SIGMA_BD_CPU

        if (nmiSource & NMI_DRAM_PARITY_ERROR)
            S += sprintf(S, "; DRAM Parity Error");
        if (nmiSource & NMI_ETHERNET_PARITY_ERROR)
            S += sprintf(S, "; Ethernet Parity Error");
    }
#endif // SIGMA_DEVELOPMENT


    FaultHandler::LogNmiCode(nmiSource, errorCode);

#if defined( SIGMA_PRODUCTION )
    InitiateReboot( EXCEPTION );
#endif
    // $[TI1]
    //  for development, the exception returns so xtrace can
    //  process the exception
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Decode
//
//@ Interface-Description
//  This method is called by the exception "router" to process a
//  non level 7 (NMI) unexpected interrupt.  An unexpected interrupt
//  is a processor interrupt not assigned to a device driver or normal
//  exception handler such as an unimplemented floating-point
//  instruction exception.  Unexpected interrupts or exceptions include
//  most commonly memory access faults when software attempts to access
//  an unmapped memory location.  The exception router builds an
//  ExceptionStackFrame that includes all the CPU registers at the
//  time of the exception.  This method decodes the exception stack
//  frame and passes the decoded information to the FaultHandler which
//  writes it into the System Diagnostic Log.  After logging this
//  exception information, InitiateReboot() reboots the processor with
//  a shutdown state of EXCEPTION which the POST Reset Umpire handles
//  as an unexpected reset.  This method does not return.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
Exception::Decode(const ExceptionStackFrame * p)
{
    Byte * errorAddress = (Byte*)0;
    Uint16 vectorNumber = (p->formatAndVectorOffset & 0xfff)/4;
    Uint16 stackFrameFormat = p->formatAndVectorOffset >> 12;
    Byte * programCounter = (Byte *) ( (p->pcHigh << 16) | p->pcLow );
    Int    info[3];
    Int    error;

    sc_tinquiry(info, 0, &error);
    Uint8 threadId = info[0];

    switch (stackFrameFormat)
    {
    case 2:    // Six-Word Stack Frame
    case 3:    // MC68040 Floating-Point Post-Instruction Stack Frame
        {
            // $[TI1.1]
            errorAddress = (Byte*)p->frame.format2Or3.effectiveAddress;
            break;
        }

    case 7:    // MC68040 Access Error Stack Frame
        {
            // $[TI1.2]
            errorAddress = (Byte*)p->frame.format7.faultAddress;
            break;
        }

    default:
        // $[TI1.3]
        break;
    }

#if defined( SIGMA_DEVELOPMENT )
    static char *S = ExceptionLog;

    if (S > &ExceptionLog[sizeof(ExceptionLog)-LOG_ENTRY_SIZE])
    {
        S = (char*)((Uint32)S + 15 & 0xfffffff0);
        sprintf(S, "**LOG OVERFLOW**");
    }
    else
    {
        S = (char*)((Uint32)S + 15 & 0xfffffff0);

        if (vectorNumber == 11)
        {
            if (stackFrameFormat == 2)
                S += sprintf(S, "%s", "Unimplemented Floating-Point Instruction" );
            else
                S += sprintf(S, "%s", INTERRUPT_NAME[vectorNumber] );
        }
        else
        {
            S += sprintf(S, "%s", INTERRUPT_NAME[vectorNumber] );
        }

        Uint32  a7 = p->a[7];    // top of stack frame

        switch (stackFrameFormat)
        {
        case 0:    // Four-Word Stack Frame
        case 1:    // Throwaway Four-Word Stack Frame
            a7 += 6;
            break;

        case 2:    // Six-Word Stack Frame
        case 3:    // MC68040 Floating-Point Post-Instruction Stack Frame
            {
                Byte * effectiveAddress = (Byte*)p->frame.format2Or3.effectiveAddress;
                S += sprintf(S, "; Address = %x", effectiveAddress);
                a7 += sizeof(StackFrameFormat2and3) + 6;
                break;
            }

        case 7:    // MC68040 Access Error Stack Frame
            {
                Byte * faultAddress = (Byte*)p->frame.format7.faultAddress;
                S += sprintf(S, "; Address = %x", faultAddress);
                a7 += sizeof(StackFrameFormat7) + 6;
                break;
            }

        default:   // Unknown Stack Frame
            break;
        }

        S = (char*)((Uint32)S + 15 & 0xfffffff0);

        S += sprintf(S, "PC = %x; Thread id = %d"
                     , programCounter
                     , threadId );

        S = (char*)((Uint32)S + 15 & 0xfffffff0);

        S += sprintf(S, "   SR = %04.4x    ", p->sr);
        S += sprintf(S, "  USP = %08.8x", p->usp);
        S += sprintf(S, "  MSP = %08.8x", p->msp);
        S += sprintf(S, "  ISP = %08.8x", p->isp);
        S += sprintf(S, " ITT0 = %08.8x", p->itt0);
        S += sprintf(S, " ITT1 = %08.8x", p->itt1);
        S += sprintf(S, " DTT0 = %08.8x", p->dtt0);
        S += sprintf(S, " DTT1 = %08.8x", p->dtt1);
        S += sprintf(S, "  VBR = %08.8x", p->vbr);
        S += sprintf(S, "  SFC = %08.8x", p->sfc);
        S += sprintf(S, "  DFC = %08.8x", p->dfc);
        S += sprintf(S, " CACR = %08.8x", p->cacr);
        S += sprintf(S, "  URP = %08.8x", p->urp);
        S += sprintf(S, "  SRP = %08.8x", p->srp);
        S += sprintf(S, "   TC = %08.8x", p->tc);
        S += sprintf(S, "MMUSR = %08.8x", p->mmusr);

        for (int i=0; i<7; i++)
            S += sprintf(S, "   D%d = %08.8x", i, p->d[i]);

        for (i=0; i<6; i++)
            S += sprintf(S, "   A%d = %08.8x", i, p->a[i]);

        S += sprintf(S, "   A7 = %08.8x", a7);
    }
#endif // SIGMA_DEVELOPMENT

    FaultHandler::LogExceptionCode(  vectorNumber
                                     , programCounter
                                     , errorAddress
                                     , threadId );

#if defined( SIGMA_PRODUCTION )
    InitiateReboot( EXCEPTION );
#endif
    //  for development, the exception returns so xtrace can
    //  process the exception
}


#if defined ( SIGMA_GUI_CPU )
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetVectorName
//
//@ Interface-Description
//  This static method returns a character string containing the name
//  of vector assignment for the specified vector number.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

const char *
Exception::GetVectorName(const Uint vectorNumber)
{
    if (vectorNumber > 255)
    {
        // $[TI1.1]
        return "Vector Number Range Error";
    }
    else
    {   // $[TI1.2]
        if ( INTERRUPT_NAME[vectorNumber] )
        {
            // $[TI2.1]
            return INTERRUPT_NAME[vectorNumber];
        }
        else
        {   // $[TI2.2]
            return "Unassigned User Defined Vector";
        }
    }
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetNmiSourceName
//
//@ Interface-Description
//  This static method builds a two line message describing the source
//  of the NMI for the specified NMI source register, error code and
//  CPU.  GUI-Application calls this function to build the message it
//  displays for NMI's in the System Diagnostc Log.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
Exception::GetNmiMessage(  const Uint   nmiSource
                           , const Uint   errorCode
                           , const Uint   cpuFlag
                           , char *       pBuffer1
                           , const Uint   bufferLength1
                           , char *       pBuffer2
                           , const Uint   bufferLength2)
{
    static const Uint LOCAL_MESSAGE_LENGTH = 512;  // absolute maximum
    char message1[LOCAL_MESSAGE_LENGTH];
    char message2[LOCAL_MESSAGE_LENGTH];

    char * pS = message1;
    char * pT = message2;

    pS += sprintf(pS, "NMI");

    if (cpuFlag == DiagnosticCode::GUI_CPU_DIAG)
    {                                                          // $[TI1.1]
        if (nmiSource & NMI_OVER_5VDC)
        {                                                      // $[TI1.1.1]
            pS += sprintf(pS, ": +5 VDC Overvoltage");
        }                                                      // $[TI1.1.2]
        if (nmiSource & NMI_OVER_12VDC)
        {                                                      // $[TI1.2.1]
            pS += sprintf(pS, ": +12 VDC Overvoltage");
        }                                                      // $[TI1.2.2]
        if (nmiSource & NMI_UNDER_12VDC)
        {                                                      // $[TI1.3.1]
            pS += sprintf(pS, ": +12 VDC Undervoltage");
        }                                                      // $[TI1.3.2]
        if (nmiSource & NMI_SAAS_FAILURE)
        {                                                      // $[TI1.4.1]
            pS += sprintf(pS, ": SAAS Failure");
        }                                                      // $[TI1.4.2]
    }                                                          // $[TI1.2]

    if (cpuFlag == DiagnosticCode::BD_CPU_DIAG)
    {                                                          // $[TI2.1]
        if (nmiSource & NMI_ANALOG_INTERFACE_ERROR)
        {                                                      // $[TI2.1.1]
            pS += sprintf(pS, ": Analog Interface Error");
            if (errorCode & AIF_ADC_TIMING_FAULT)
            {                                                  // $[TI2.1.1.1]
                pT += sprintf(pT, ": ADC Timing Fault");
            }                                                  // $[TI2.1.1.2]
            if (errorCode & AIF_ADC_CHANNEL_SEQUENCER_FAULT)
            {                                                  // $[TI2.1.2.1]
                pT += sprintf(pT, ": ADC Channel Sequencer Fault");
            }                                                  // $[TI2.1.2.2]
            if (errorCode & AIF_HAMMING_DECODE_FAULT)
            {                                                  // $[TI2.1.3.1]
                pT += sprintf(pT, ": Hamming Decode Fault");
            }                                                  // $[TI2.1.3.2]
        }                                                      // $[TI2.1.2]
    }                                                          // $[TI2.2]

    if (nmiSource & NMI_DRAM_PARITY_ERROR)
    {                                                          // $[TI3.1]
        pS += sprintf(pS, ": DRAM Parity Error");
    }                                                          // $[TI3.2]
    if (nmiSource & NMI_ETHERNET_PARITY_ERROR)
    {                                                          // $[TI4.1]
        pS += sprintf(pS, ": Ethernet Parity Error");
    }                                                          // $[TI4.2]

    //  $[TI5.1]  $[TI5.2]
    Uint len = (pS-message1 < bufferLength1) ? (pS-message1) : (bufferLength1-1);
    memcpy(pBuffer1, message1, len);
    pBuffer1[len] = 0;

    //  $[TI6.1]  $[TI6.2]
    len = (pT-message2 < bufferLength2) ? (pT-message2) : (bufferLength2-1);
    memcpy(pBuffer2, message2, len);
    pBuffer2[len] = 0;

}

#endif //  SIGMA_GUI_CPU

//=====================================================================
//
//      Protected Methods...
//
//=====================================================================

//=====================================================================
//
//      Private Methods...
//
//=====================================================================

