#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================


// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: PressureController - Implements a pressure controller using a
//		PSOL for flow delivery.
//---------------------------------------------------------------------
//@ Interface-Description
//		This class controls the Psols so that the pressure in the
//		patient circuit is controlled to the desired pressure level during
//		the inspiration phase of a pressure based breath.  Methods are
//		implemented to set the proportional gain, to initialize integrator,
//		to set and get the weighting factor for the feedback pressure signal,
//		to get and determine the error gain, to get the flow command limit,
//		to initialize the controller, and to control the PSOL so that the
//		desired	pressure is achieved.
//---------------------------------------------------------------------
//@ Rationale
//		This class implements the algorithms to control pressure in the
//		patient circuit.
//---------------------------------------------------------------------
//@ Implementation-Description
//		This controller is invoked every BD cycle during the inspiration
//		phase of a pressure based breath.  The pressure controller is a
//		PI controller with the pressure error as an input.  The pressure
//		error is the difference between the desired pressure and the feedback
//		pressure.  The feedback pressure is given by
//			feedback pressure = Pin * wf + Pexh * (1 - wf)
//		where wf is a weighting factor ( 0 <= wf <= 1).  At the beginning of
//		every breath, wf = 0 unless changed by the setWf method.  The desired
//		flow (output of the PI controller) is
//		limited based on patient type.  Once the desired flow is known, the
//		flow controller is used to determine the PSOL current needed to
//		achieve that flow.
//---------------------------------------------------------------------
//@ Fault-Handling
//		N/A
//---------------------------------------------------------------------
//@ Restrictions
//		Only one instance of this class may be created
//---------------------------------------------------------------------
//@ Invariants
//		none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/PressureController.ccv   25.0.4.0   19 Nov 2013 14:00:04   pvcs  $
//
//@ Modification-Log
//
//  Revision: 011  By:  rhj    Date: 07-July-2008    SCR Number: 6435
//  Project:  LEAK
//  Description:
//       Leak Compensation project-related changes.
//       
//  Revision: 010  By: syw     Date:  09-Mar-2000    DR Number: 5684
//  Project:  NeoMode
//  Description:
//		Added call to FlowController::DeterminePsolLookupFlags() and added the
//		results of these flags as arguments to FlowController::updatePsol().
//
//  Revision: 009  By: syw     Date:  09-Feb-2000    DR Number: 5632
//  Project:  NeoMode
//  Description:
//		For neonatal circuit types, set errorGain_ = 1.0
//
//  Revision: 008  By: sah     Date:  22-Sep-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//      NeoMode-specific changes:
//      *  set default maximum flow to neonatal maximum
//      *  added neonatal-specific maximum flow value
//
//  Revision: 007  By:  yyy    Date:   14-May-1999    DR Number: DCS 5432
//       Project:  ATC
//       Description:
//           Update O2 mixture to accommadate cycle to cycle change.
//
//  Revision: 006  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 005  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 004 By: syw    Date: 03-Jul-1996   DR Number: 1073, 1074
//  	Project:  Sigma (R8027)
//		Description:
//			Initialize integrator_ to initIntegratorValue_.
//
//  Revision: 003 By: syw    Date: 26-Feb-1996   DR Number: 600
//  	Project:  Sigma (R8027)
//		Description:
//			Controls Rev 04 changes.
//
//  Revision: 002 By: syw    Date: 21-Feb-1996   DR Number: DCS 674
//  	Project:  Sigma (R8027)
//		Description:
//			Removed #ifndef around #include.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "PressureController.hh"

#include "BreathMiscRefs.hh"

//@ Usage-Classes

#include "MathUtilities.hh"
#include "BdDiscreteValues.hh"
#include "O2Mixture.hh"
#include "PressureSensor.hh"
#include "FlowController.hh"
#include "ControllersRefs.hh"
#include "PhasedInContextHandle.hh"
#include "BD_IO_Devices.hh"
#include "LeakCompEnabledValue.hh"
#include "LeakCompMgr.hh"

#include "MainSensorRefs.hh"
#include "ExhFlowSensor.h"
#include "NovRamManager.hh"

//@ End-Usage

//@ Constant: ADULT_MAX_FLOW
// maximum delivered flow for adults
extern const Real32 ADULT_MAX_FLOW ;

//@ Constant: PED_MAX_FLOW
// maximum delivered flow for pediatrics
extern const Real32	PED_MAX_FLOW ;

//@ Constant: NEO_MAX_FLOW
// maximum delivered flow for neonatals
extern const Real32	NEO_MAX_FLOW ;

//@ Constant: PED_MAX_FLOW_LEAK
// maximum delivered flow leak for pediatrics
extern const Real32	PED_MAX_FLOW_LEAK ;

//@ Constant: NEO_MAX_FLOW_LEAK
// maximum delivered flow leak for neonatals
extern const Real32	NEO_MAX_FLOW_LEAK ;


//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: PressureController()
//
//@ Interface-Description
//		Constructor.  This method has two PressureSensor& and two FlowController&
//		as arguments and has no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The data members are initialized with the arguments passed in.
//		Other data members are also initialized.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
PressureController::PressureController( const PressureSensor& inspPressure,
										const PressureSensor& expPressure,
          					            FlowController& o2FlCtrl,
          					            FlowController& airFlCtrl)
: rInspPressureSensor_(inspPressure),	  	// $[TI1.1]
  rExhPressureSensor_(expPressure),			// $[TI1.2]
  rO2FlowController_(o2FlCtrl),				// $[TI1.3]
  rAirFlowController_(airFlCtrl)			// $[TI1.4]
{
	CALL_TRACE("PressureController::PressureController( \
				const PressureSensor& inspPressure, \
				const PressureSensor& expPressure, FlowController& o2FlCtrl, \
       					            FlowController& airFlCtrl)") ;

	// $[TI2]
    kp_ = 0.0F ;
    ki_ = 1.0F ;
    integrator_ = 0.0F ;
    wf_ = 0.0F ;
	initIntegratorValue_ = 0.0F ;
	flowCommandLimit_ = NEO_MAX_FLOW ;

#if CONTROLS_TUNING	
	overrideKp_ = FALSE;
	overrideKi_ = FALSE;
#endif
	trajectoryAlpha_ = 1.0F;
	kpFinal_ = 0.0;
	kpInitial_ = 0.0;
	kiFinal_ = 1.0;
	kiInitial_ = 1.0;
	prevDesiredFlow_ = 0.0F;
	targetReached_ = FALSE;
    NovRamManager::GetExhResistance( exhResistance_) ;
	desPressFilter_ = 0.0F;
	desPressAlpha_ = 0.9F;
	prevKi_ = kiFinal_;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~PressureController()
//
//@ Interface-Description
//		Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//		none
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
PressureController::~PressureController( void)
{
	CALL_TRACE("PressureController::~PressureController( void)") ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newBreath()
//
//@ Interface-Description
//		This method has the proportional and integral gains as arguments and
//		has no return value.  This method is called at the beginning of every
//		pressure based inspiration to initialize the controller.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Initialize the gains of the controller with the arguments passed in.
//		Initialize other data members.  Initialize flow controllers with
//		their own newBreath() methods.
//---------------------------------------------------------------------
//@ PreCondition
//		ki > 0
//		patientType == PatientTypeValue::PEDIATRIC_PATIENT ||
//			patientType == PatientTypeValue::ADULT_PATIENT
// 		$[04070] $[04075] $[04076] $[04306]    		
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
void
PressureController::newBreath( const Real32 kp, const Real32 ki, const Real32 alpha) 
{
	CALL_TRACE("PressureController::newBreath( const Real32 kp, const Real32 ki)") ;
	 	
    CLASS_PRE_CONDITION( ki > 0.0F) ;
    
    wf_ = 0.0F ;		// use Pexp for feedback
	errorGain_ = 1.0F ;
   	crossingFlag_ = FALSE ;
	error_ = 0.0F ;
		    
    integrator_ = initIntegratorValue_ ;
    
#if CONTROLS_TUNING
	//Check override flag before updating gains
	if (!overrideKp_)
	{
		kp_ = kp;
	}

	if (!overrideKi_)
	{
		ki_ = ki;
	}
#else
	kp_ = kp;
	ki_ = ki;
#endif

	prevDesiredFlow_ = 1.0F;
	targetReached_ = FALSE;
	trajectoryAlpha_ = alpha;
	desPressFilter_ = rExhPressureSensor_.getValue() ;
	desPressAlpha_ = 0.7F;

	kiInitial_ = 0.5 * ki;
	kiFinal_ = 1.0 * ki;
	prevKi_ = kiInitial_;

    // determine flowCommandLimit_
    const DiscreteValue  CIRCUIT_TYPE =
        PhasedInContextHandle::GetDiscreteValue(SettingId::PATIENT_CCT_TYPE);

    switch (CIRCUIT_TYPE)
    {
    case PatientCctTypeValue::PEDIATRIC_CIRCUIT :

		// $[LC24004] -- When Leak Comp is ON, increase the maximum flow limits 
		// to PED_MAX_FLOW_LEAK (Pediatric). 
		if (RLeakCompMgr.isEnabled())
		{
			flowCommandLimit_ = PED_MAX_FLOW_LEAK;	
		}
		else
		{
			// $[TI1.3]
			flowCommandLimit_ = PED_MAX_FLOW ;
		}
        break;
    case PatientCctTypeValue::ADULT_CIRCUIT :
		// $[TI1.1]
		flowCommandLimit_ = ADULT_MAX_FLOW ;
        break;
    case PatientCctTypeValue::NEONATAL_CIRCUIT :

		// $[LC24004] -- When Leak Comp is ON, increase the maximum flow limits 
		// to NEO_MAX_FLOW_LEAK (Neonatal). 
		if (RLeakCompMgr.isEnabled())
		{
			flowCommandLimit_ = NEO_MAX_FLOW_LEAK;	
		}
		else
		{
			// $[TI1.3]
			flowCommandLimit_ = NEO_MAX_FLOW ;
		}


        break;
    default :
        AUX_CLASS_ASSERTION_FAILURE(CIRCUIT_TYPE);
        break;
    }

	rAirFlowController_.newBreath() ;
	rO2FlowController_.newBreath() ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updatePsol()
//
//@ Interface-Description
//		This method has desired pressure as an argument and has no return
//		value.  This method is called during every pressure based inspiration
//		BD cycle to output to the PSOLs such that the desired pressure
//		is achieved.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The feedback pressure is computed as
//			feedbackPressure = Pin * wf + Pexh * (1 - wf).
//		The pressure error is determined and is fed into the PI
//		controller to obtain the desiredFlow.  The desired air and O2
//		are determined by the o2Percent.  The flowController is then
//		called to control the PSOLs.
// 		$[04069] $[04072] $[04073] $[04074] $[04075] $[04077] 
//---------------------------------------------------------------------
//@ PreCondition
//		ki > 0
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
void
PressureController::updatePsol( const Real32 desiredPressure, const Real32 targetPressure, Int32 elapsedTime)
{
	CALL_TRACE("PressureController::updatePsol( const Real32 desiredPressure)") ;
		
	CLASS_PRE_CONDITION( ki_ > 0.0F) ;

    desiredPressure_ = desiredPressure ;

    Real32 	inspPressure = rInspPressureSensor_.getValue() ;
    Real32 	exhPressure = rExhPressureSensor_.getValue() ;
    Real32 exhFlow = RExhFlowSensor.getDryValue() ;
	exhPressure += exhResistance_.calculateDeltaP( exhFlow) ;

    Real32	feedbackPressure = inspPressure * wf_ + exhPressure * ( 1.0f - wf_) ;
	error_ = 1.0F * (desiredPressure - feedbackPressure) ;
    
	Real32 tgtError = targetPressure - feedbackPressure;
	if (ABS_VALUE(tgtError) <= MAX_VALUE(0.5, 0.025 * targetPressure) && targetReached_ == FALSE)
	{
		targetReached_ = TRUE;
	}

	if (targetReached_)
	{
		error_ = 0.5 * error_;
	}

	if (IsEquivalent( wf_, 1.0, HUNDREDTHS) && IsEquivalent( desiredPressure, 97.0, HUNDREDTHS))
	{
		error_ = 0.25 * error_;
	}

	if (!targetReached_ && error_ < 0.0)
	{
		error_ = 0.05 * error_;
	}

	integrator_ += error_ * CYCLE_TIME_MS ;	// rectangular integration

	ki_ = trajectoryAlpha_ * ki_ + (1.0 - trajectoryAlpha_) * kiFinal_;
	integrator_ = integrator_ * prevKi_ / ki_;
	prevKi_ = ki_;

	Real32 desiredFlow = kp_ * error_ + ki_ * integrator_ ;

    // limit desired flow and prevent windup conditions
    if (desiredFlow > flowCommandLimit_)
    {
		// $[TI1.1]
   		integrator_ = (flowCommandLimit_ - kp_ * error_) / ki_ ;
		desiredFlow = flowCommandLimit_ ;
    }
	else if (desiredFlow < 0.0F)
    {
		// $[TI1.2]
		integrator_ = - kp_ * error_ / ki_ ;
		desiredFlow = 0.0F ;
    }	// implied else $[TI1.3]

	prevDesiredFlow_ = desiredFlow;
    Real32 	desiredO2Flow = desiredFlow * RO2Mixture.calculateO2Fraction(desiredFlow) ;
    Real32 	desiredAirFlow = desiredFlow - desiredO2Flow ;

	Boolean useAirLookup = FALSE ;
	Boolean useO2Lookup = FALSE ;

	FlowController::DeterminePsolLookupFlags( desiredAirFlow, desiredO2Flow,
												useAirLookup, useO2Lookup) ;

    rAirFlowController_.updatePsol( desiredAirFlow, useAirLookup) ;
    rO2FlowController_.updatePsol( desiredO2Flow, useO2Lookup) ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setWf()
//
//@ Interface-Description
//		This method has the weighting factor as an argument and has no
//		return value.  This method is called to change the weighting factor
//		to the value passed in.  The weighting factor must be = 0 or 1
//---------------------------------------------------------------------
//@ Implementation-Description
//		wf_ is updated to the value passed in.  The integrator_ is adjusted
//		to avoid discontinuities.
//		$[04308]
//---------------------------------------------------------------------
//@ PreCondition
//		IsEquivalent( wf, 0.0, HUNDREDTHS) || IsEquivalent( wf, 1.0, HUNDREDTHS)
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
PressureController::setWf( const Real32 wf)
{
	CALL_TRACE("PressureController::setWf( const Real32 wf)") ;

	// $[TI1]
	CLASS_PRE_CONDITION( IsEquivalent( wf, 0.0, HUNDREDTHS)
						  || IsEquivalent( wf, 1.0, HUNDREDTHS)) ;
	
	wf_ = wf ;

	integrator_ += (kp_ * error_) / ki_ ;
	
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: determineErrorGain()
//
//@ Interface-Description
//		This method has a reference target and a target pressure as arguments
//		and has no return value.  This method is called to compute the
//		error gain of the pressure controller.  This method must be called
//		every CYCLE_TIME_MS by the PressureBasedPhase after the weighting
//		factor has been updated.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Set errorGain_ depending on which condition is met.  errorGain_
//		is initialized to 1.0 in newBreath().
// 		$[04306]
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
void
PressureController::determineErrorGain( const Real32 referenceTarget,
										const Real32 targetPressure)
{
	CALL_TRACE("PressureController::determineErrorGain( const Real32 referenceTarget, \
				const Real32 targetPressure)");
				
    Real32 	exhPressure = rExhPressureSensor_.getValue() ;
    Real32 	inspPressure = rInspPressureSensor_.getValue() ;

	if (IsEquivalent( wf_, 0.0, HUNDREDTHS) && exhPressure > referenceTarget && crossingFlag_)
	{
		// $[TI1.1]
		errorGain_ = 2.0F ;
	}
	else if (IsEquivalent( wf_, 1.0, HUNDREDTHS))
	{
		// $[TI1.2]
		errorGain_ = 0.25F * (inspPressure - targetPressure) ;

		if (errorGain_ > 1.0F)
		{
			// $[TI2.1]
			errorGain_ = 1.0F ;
		}
		else if (errorGain_ < 0.1F)
		{
			// $[TI2.2]
			errorGain_ = 0.1F ;
		}	// implied else $[TI2.3]
	}	// implied else $[TI1.3]

	const DiscreteValue  PATIENT_CCT_TYPE =
		PhasedInContextHandle::GetDiscreteValue(SettingId::PATIENT_CCT_TYPE);

	if (PATIENT_CCT_TYPE == PatientCctTypeValue::NEONATAL_CIRCUIT)
	{
		// $[TI4]
		errorGain_ = 1.0F ;
	}
	// $[TI5]

    if (exhPressure < referenceTarget)
    {
		// $[TI3.1]
    	crossingFlag_ = TRUE ;
	}	// implied else $[TI3.2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//      'lineNumber' are essential pieces of information.  The 'pFileName'
//      and 'pPredicate' strings may be defaulted in the macro to reduce
//      code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
 
void
PressureController::SoftFault( const SoftFaultID  softFaultID,
                			   const Uint32       lineNumber,
    			               const char*        pFileName,
                   			   const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)") ;
  	FaultHandler::SoftFault( softFaultID, BREATH_DELIVERY, PRESSURECONTROLLER, lineNumber,
  						     pFileName, pPredicate) ;
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//=====================================================================
//
//  Private Methods...
//
//=====================================================================













