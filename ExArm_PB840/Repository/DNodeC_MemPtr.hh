
#ifndef DNodeC_MemPtr_HH
#define DNodeC_MemPtr_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: DNodeC_MemPtr - Node for Doubly-Linked Lists of Generic Data
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Foundation/vcssrc/DLinkedNodeC.H_v   25.0.4.0   19 Nov 2013 14:03:44   pvcs  $
//
//@ Modification-Log
//
//   Revision: 003   By: sah   Date: 07-Jan-1999   DR Number: 5321
//   Project:  ATC
//   Description:
//	Changed 'SoftFault()' method to an non-inlined method.
//  
//   Revision: 002   By: sah   Date: 06-Feb-1996   DCS Number: 674
//   Project:  Sigma (R8027)
//   Description:
//	Removed 'ifndef' and 'endif' from around includes.
//
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//====================================================================

#include "Sigma.hh"
#include "FoundationClassId.hh"
#include "TemplateMacros.hh"
#include "MemPtr.hh"

//@ Usage-Classes
//@ End-Usage


class DNodeC_MemPtr
{
    //@ Friend:  DListC_MemPtr
    // 'DListC_MemPtr' needs access to the constructor, destructor,
    // and private members of this class.
    friend class DListC_MemPtr;

  public:
    inline const MemPtr&  getItem(void) const;
    inline MemPtr&        getItem(void);

    inline const DNodeC_MemPtr*  getNextPtr(void) const;
    inline const DNodeC_MemPtr*  getPrevPtr(void) const;

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName = NULL,
			   const char*       pBoolTest = NULL);

  private:
    // only 'DListC_MemPtr' can create/destroy this class...
    inline DNodeC_MemPtr(const DNodeC_MemPtr& dNode);
    inline DNodeC_MemPtr(const MemPtr& newItem);
    inline ~DNodeC_MemPtr(void);

    DNodeC_MemPtr(void);				// not implemented...
    void  operator=(const DNodeC_MemPtr&);		// not implemented...

    //@ Data-Member:  nodeItem_
    // This is the item, of type 'MemPtr', that is kept in the list.
    MemPtr  nodeItem_;

    //@ Data-Member:  pNext_
    // A pointer to the next node in this list.
    DNodeC_MemPtr*  pNext_;

    //@ Data-Member:  pPrev_
    // A pointer to the previous node in this list.
    DNodeC_MemPtr*  pPrev_;
};


// Inlined methods...
#include "DNodeC_MemPtr.in"


#endif  // DNodeC_MemPtr_HH
