/*****************************************************************************/
/*   Module Name:    dbapi.h                                            
/*   Purpose:      Database API header
/*    
/*   Modification History:
/*   
/*   Description of Change			        By			      Date 
/*   ---------------------					-----   		  ------
/*	 Selected Reports to preview or 
/* 	 delete  functions are added.			Lakshmi(CGS)   	  5 Dec,2000
/*                                                                           
/*   Requirements:   This module implements requirements detailed in         
/*          <75359-93>                                                       
/*                                                                           
/*   $Revision::   1.1      $                                                
/*                                                                          
/*****************************************************************************/

/*****************************************************************************
THIS IS A PROPRIETARY WORK TO WHICH NELLCOR PURITAN BENNETT CORPORATION CLAIMS
EXCLUSIVE RIGHT.  NO PART OF THIS WORK MAY BE USED, DISCLOSED, REPRODUCED,
STORED IN AN INFORMATION RETIEVAL SYSTEM, OR TRANSMITTED, IN ANY FORM OR BY ANY
MEANS ELECTRONIC, MECHANICAL, PHOTOCOPYING, RECORDING OR OTHERWISE WITHOUT THE
PRIOR WRITTEN PERMISSION OF NELLCOR PURITAN BENNETT CORPORATION.
 
Copyright (c) 1999, Mallinckrodt Nellcor Puritan Bennett
All rights reserved.
*******************************************************************************/

#ifndef _DBAPI
#define _DBAPI

#define DB_FAIL 0
#define DB_PASS 1
#define DB_SKIP 2
#define DB_ABORT 3

extern struct SSNdate SNDate;
typedef struct TEST_RUN_STRUCT
{
	int		TestIndex;
	int	Pass_Fail;
	char*	Fact;
	double	MinRdg;
	double	Rdg;
	double	MaxRdg;
	int		Type;
	int		set;
} test_run_struct;

int SetupDB_Jet(void);
int WriteSeqDB_Jet(char* User, int PFtext, int ID);
int SetupJetOATest(void);
int WriteOATestDB_Jet(int index, int PF);
int LoadTestInfo(int idx, char PF, char* fact, double min, double max, double rdg, int type);
int WriteTestDB_Jet(void);
int IsReportAvailable(void);
int WriteLogLine(char* line);
int ClearLog(void);
void CompactJetDB(void);
int CloseJetDB(void);
void OpenJetDB(void);
void CheckDBSize(void);
int ClearJetDB(void);
struct SSNdate SelectJetSeqSN(int RecId);
int DeleteSelectedSequence(int SeqID); 


#endif // #ifndef _DBAPI
