// DerivedStatusBar.cpp : implementation file
//

#include "stdafx.h"
#include "E600Util.h"
#include "DerivedStatusBar.h"


// CDerivedStatusBar

IMPLEMENT_DYNAMIC(CDerivedStatusBar, CStatusBar)

CDerivedStatusBar::CDerivedStatusBar()
{

}

CDerivedStatusBar::~CDerivedStatusBar()
{
}


BEGIN_MESSAGE_MAP(CDerivedStatusBar, CStatusBar)
	//{{AFX_MSG_MAP(CWzdStatusBar)
	ON_WM_CREATE()
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDerivedStatusBar message handlers

int CDerivedStatusBar::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CStatusBar::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	CRect rect(0,0,0,0);
	m_ProgressCtrl.Create(WS_CHILD|WS_VISIBLE,rect,this,IDC_STATUS_PROGRESS);

	return 0;
}

void CDerivedStatusBar::OnSize(UINT nType, int cx, int cy) 
{
	CStatusBar::OnSize(nType, cx, cy);

	UINT inx;
	CRect rect;

	inx=CommandToIndex(IDS_INDICATOR_PROGRESS);
	GetItemRect(inx,&rect);
	m_ProgressCtrl.MoveWindow(rect);

}

