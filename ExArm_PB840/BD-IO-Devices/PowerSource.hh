#ifndef PowerSource_HH
#define PowerSource_HH
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
// Class: PowerSource - implements the functionality required for 
//                      determining the current AC power quality.
//----------------------------------------------------------------------
// @ Version-Information
// @ (#) $Header::   /840/Baseline/BD-IO-Devices/vcssrc/PowerSource.hhv   10.7   08/17/07 09:30:40   pvcs  
//
// @ Modification-Log
//
//  Revision: 003  By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 002  By:  syw    Date:  08-Sep-1996    DR Number: DCS 2280, 2118
//       Project:  Sigma (R8027)
//       Description:
//       	Added compVoltageThresh_ and compRecoveryVoltageThresh_
//			data members.  Change AcQuality enums to either OKAY
//			or LOW only.  Eliminate isInsufficientAcPower().  Added
//			DetermineLowAcThresh() for Service-Mode.
//
//  Revision: 001  By:  by    Date:  11-Nov-1996    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "Sigma.hh"
#include "BD_IO_Devices.hh"

//@ Usage-Classes
//@ End-Usage

class PowerSource
{
    public:

        enum Source { AC_POWER, BATTERY_POWER };

        //@ Constant: VOLTSENSOR_VOLT_MIN
        //  Power source voltage sensor minimum voltage
        static const Real32 VOLTSENSOR_VOLT_MIN;

        //@ Constant: VOLTSENSOR_VOLT_MAX
        //  Power source voltage sensor maximum voltage
        static const Real32 VOLTSENSOR_VOLT_MAX;

        PowerSource( void );
        ~PowerSource( void );

        static void SoftFault( const SoftFaultID softfaultID,
							   const Uint32      lineNumber,
							   const char*       pFileName = NULL,
							   const char*       pPredicate = NULL );

        inline Source getSource( void ) const;
        inline void registerAlarmCallBack( PAlarmCallBack pAlarmCallBack );

        void initialize( void );
        void newCycle( void );  

        void initialStatusNotification( void );

        inline Boolean isExternalBatteryConnected( void ) const;
		
#ifdef SIGMA_DEVELOPMENT
void showPowerStatus( void );
#endif // SIGMA_DEVELOPMENT 

    protected:
    
    private:

        PowerSource( const PowerSource& );     // not implemented
        void operator=( const PowerSource& );  // not implemented

        //@ Data-Member:  powerSource_
        //  indicates the source of ventilator power.
        Source powerSource_;

        //@ Data-Member: pAlarmCallBack_
        //  pointer to the function called when AC power conditions
        //  that the Alarm-Analysis needs to know about are detected
        PAlarmCallBack pAlarmCallBack_;

        //@ Data-Member: lastPowerSourceSensorVolt_
        // last power source sensor voltage
        Real32 lastPowerSourceSensorVolt_;

        //@ Data-Member: externalBatteryConnected_
        // record if external battery is connected to BD board
        Boolean externalBatteryConnected_;
} ;

// Inlined methods...
#include "PowerSource.in"

#endif  //PowerSource_HH

