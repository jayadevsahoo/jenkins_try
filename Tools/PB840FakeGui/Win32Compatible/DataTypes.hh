#pragma once

//@ Type:  Byte
// An unsigned, 8-bit data type.
typedef unsigned char	Byte;

//@ Type:  Char
// A reserved type of characters.
typedef char	Char;

//@ Type:  Boolean
// A logical value (true or false) data type.
typedef unsigned char	Boolean;

//@ Type:  Uint8
// An unsigned, 8-bit integer data type.
typedef unsigned char	Uint8;

//@ Type:  Uint16
// An unsigned, 16-bit integer data type.
typedef unsigned short	Uint16;

//@ Type:  Uint32
// An unsigned, 32-bit integer data type.
typedef unsigned int	Uint32;

//@ Type:  Uint
// An unsigned, integer data type that is representative of a native long
// word.
typedef unsigned int	Uint;

//@ Type:  Int8
// A signed, 8-bit integer data type.
typedef char	Int8;

//@ Type:  Int16
// A signed, 16-bit integer data type.
typedef short	Int16;

//@ Type:  Int32
// A signed, 32-bit integer data type.
typedef int	Int32;

//@ Type:  Int
// A signed, integer data type that is representative of a native long word.
typedef int	Int;

//@ Type:  Real32
// A 32-bit, floating-point data type.
typedef float	Real32;

//@ Type:  MemPtr
// A generic pointer to memory.
typedef void*	MemPtr;

//@ Type:  DiscreteValue
// A discrete value type.
typedef Int16  DiscreteValue;

//@ Constant:  NO_SELECTION_
// A DiscreteValue used to indicate the operator has not selected a
// value for a DiscreteSetting.
static const DiscreteValue NO_SELECTION_ = -1;

//@ Type:  SigmaStatus
// Standard, system-wide status type.
enum SigmaStatus
{
	FAILURE,
	SUCCESS
};


//@ Type:  CommFaultID
// Communication fault identifier.
enum CommFaultID
{
	UNDEFINED_COMM_FAULT          =  -1,

	LOW_NETAPP_FAULT_VALUE        =   0, 
	NETWORKAPP_NULL_EVENT        =   LOW_NETAPP_FAULT_VALUE,
	NETWORKAPP_EPERM         =   1,
	NETWORKAPP_ENOENT            =   2,
	NETWORKAPP_ESRCH         =   3,
	NETWORKAPP_EINTR         =   4,
	NETWORKAPP_EIO           =   5,
	NETWORKAPP_ENXIO         =   6,
	NETWORKAPP_E2BIG         =   7,
	NETWORKAPP_ENOEXEC           =   8,
	NETWORKAPP_EBADF         =   9,
	NETWORKAPP_ECHILD            =  10,
	NETWORKAPP_EAGAIN            =  11,
	NETWORKAPP_ENOMEM            =  12,
	NETWORKAPP_EACCES            =  13,
	NETWORKAPP_EFAULT            =  14,
	NETWORKAPP_ENOTBLK           =  15,
	NETWORKAPP_EBUSY         =  16,
	NETWORKAPP_EEXIST            =  17,
	NETWORKAPP_EXDEV         =  18,
	NETWORKAPP_ENODEV            =  19,
	NETWORKAPP_ENOTDIR           =  20,
	NETWORKAPP_EISDIR            =  21,
	NETWORKAPP_EINVAL            =  22,
	NETWORKAPP_ENFILE            =  23,
	NETWORKAPP_EMFILE            =  24,
	NETWORKAPP_ENOTTY            =  25,
	NETWORKAPP_ETXTBSY           =  26,
	NETWORKAPP_EFBIG         =  27,
	NETWORKAPP_ENOSPC            =  28,
	NETWORKAPP_ESPIPE            =  29,
	NETWORKAPP_EROFS         =  30,
	NETWORKAPP_EMLINK            =  31,
	NETWORKAPP_EPIPE         =  32,
	NETWORKAPP_EDOM          =  33,
	NETWORKAPP_ERANGE            =  34,
	NETWORKAPP_EWOULDBLOCK       =  35,
	NETWORKAPP_EINPROGRESS       =  36,
	NETWORKAPP_EALREADY          =  37,
	NETWORKAPP_ENOTSOCK          =  38,
	NETWORKAPP_EDESTADDRREQ      =  39,
	NETWORKAPP_EMSGSIZE          =  40,
	NETWORKAPP_EPROTOTYPE        =  41,
	NETWORKAPP_ENOPROTOOPT       =  42,
	NETWORKAPP_EPROTONOSUPPORT       =  43,
	NETWORKAPP_ESOCKTNOSUPPORT       =  44,
	NETWORKAPP_EOPNOTSUPP        =  45,
	NETWORKAPP_EPFNOSUPPORT      =  46,
	NETWORKAPP_EAFNOSUPPORT      =  47,
	NETWORKAPP_EADDRINUSE        =  48,
	NETWORKAPP_EADDRNOTAVAIL     =  49,
	NETWORKAPP_ENETDOWN          =  50,
	NETWORKAPP_ENETUNREACH       =  51,
	NETWORKAPP_ENETRESET         =  52,
	NETWORKAPP_ECONNABORTED      =  53,
	NETWORKAPP_ECONNRESET        =  54,
	NETWORKAPP_ENOBUFS           =  55,
	NETWORKAPP_EISCONN           =  56,
	NETWORKAPP_ENOTCONN          =  57,
	NETWORKAPP_ESHUTDOWN         =  58,
	NETWORKAPP_ETOOMANYREFS      =  59,
	NETWORKAPP_ETIMEDOUT         =  60,
	NETWORKAPP_ECONNREFUSED      =  61,
	NETWORKAPP_ELOOP         =  62,
	NETWORKAPP_ENAMETOOLONG      =  63,
	NETWORKAPP_EHOSTDOWN         =  64,
	NETWORKAPP_EHOSTUNREACH      =  65,
	NETWORKAPP_INVALID_COMM_DATA     =  66,
	NETWORKAPP_DATA_TOO_LARGE        =  67,
	NETWORKAPP_INVALID_COMM_MSGID    =  68,
	NETWORKAPP_NO_ACK_MESSAGE        =  69,
	NETWORKAPP_MAX_ACTIVE_MSG_DELAY  =  70,
	NETWORKAPP_BLOCKIO_NO_ACCEPT_CALLBACK=  71,
	NETWORKAPP_BLOCKIO_SOCKET_ALREADY_OPENED=72,
	NETWORKAPP_WRONG_INPUT_COUNT     =  73,
	NETWORKAPP_OUT_OF_ACKBUFFER      =  74,
	NETWORKAPP_MSG_TRANSMISSION_FAILED   =  75,
	NETWORKAPP_CONNECT_TIMEOUT       =  76,
	NETWORKAPP_RECV_TIMEOUT      =  77,
	HIGH_NETAPP_FAULT_VALUE       = NETWORKAPP_RECV_TIMEOUT,
	NUM_NETAPP_FAULTS = (HIGH_NETAPP_FAULT_VALUE-LOW_NETAPP_FAULT_VALUE+1),

	LOW_STWARE_FAULT_VALUE        = 1000,
	STWARE_LOG_ERROR         = LOW_STWARE_FAULT_VALUE,
	STWARE_LOG_DIAGNOSTIC        = 1001,
	STWARE_IN_CONTROL_UNLINK_INIFADDR_IFP= 1002,   // diagnostic error 
	STWARE_IN_CONTROL_UNLINK_INIFADDR_LIST=1003,   // diagnostic error
	STWARE_IN_CKSUM_C_OUT_OF_DATA    = 1004,   // diagnostic error
	STWARE_XSRN_ADDROUTE_MASK        = 1005,   // diagnostic error
	STWARE_XSRN_DELETE_INCONSISTENT  = 1006,   // diagnostic error
	STWARE_XSRN_DELETE_NO_ANNOTATION = 1007,   // diagnostic error
	STWARE_XSRN_DELETE_NO_US     = 1008,   // diagnostic error
	STWARE_XSRN_DELETE_ORPHANED_MASK = 1009,   // diagnostic error
	STWARE_UDP_USRREQ_UNEXPECTED_CONTROL_DATA=1010,// diagnostic error
	STWARE_XS_STARTUP_NO_UNIT        = 1011,   // diagnostic error
	STWARE_NO_MEMORY_FOR_IFADDR      = 1012,   // Serious Stackware Error
	STWARE_LOOUTPUT_CANNOT_HANDLE_IF     = 1013,   // Serious Stackware Error
	STWARE_XSRN_INSERT_COMING_OUT    = 1014,   // Serious Stackware Error
	STWARE_MCOPYDATA_NEGATIVE_OFFSET = 1015,   // Serious Stackware Error
	STWARE_MCOPYDATA_NULL_DATA_PTR   = 1016,   // Serious Stackware Error
	STWARE_SBDROP_LEN_TOO_LARGE1     = 1017,   // Serious Stackware Error
	STWARE_SBDROP_LEN_TOO_LARGE2     = 1018,   // Serious Stackware Error
	STWARE_SBCOMPRESS_NO_EOR     = 1019,   // Serious Stackware Error
	STWARE_ARPINPUT_DUPLICATE_IP_ADDR    = 1020,   // Serious Stackware Error
	STWARE_XS_REGISTERIF_INVALID_TYPE    = 1021,   // Serious Stackware Error
	STWARE_XS_REGISTERIF_INVALID_FAMILY  = 1022,   // Serious Stackware Error
	STWARE_XS_DRIVIOCTL_INIT_FAILURE = 1023,   // Serious Stackware Error
	STWARE_XS_GOODETHERRARP_SHORT_RARP_PKT=1024,   // Serious Stackware Error
	STWARE_XS_GOODETHERRARP_WRONG_HWTYPE = 1025,   // Serious Stackware Error
	STWARE_XS_GOODETHERRARP_SHORT_RARP_PKT2=1026,  // Serious Stackware Error
	STWARE_XS_GOODETHERRARP_INVALID_PROTOCOL=1027, // Serious Stackware Error
	STWARE_M_COPYM_LEN_TOO_LARGE     = 1028,
	STWARE_M_COPYM_OFFSET_TOO_LARGE  = 1029,
	STWARE_M_COPYDATA_OUT_OF_MBUF    = 1030,
	STWARE_M_COPYDATA_LEN_TOO_LARGE  = 1031,
	STWARE_M_COPYDATA_OFFSET_NEGATIVE    = 1032,
	STWARE_M_COPYDATA_NULL_POINTER   = 1033,
	STWARE_MXS_MBGET_OUT_OF_MBUF     = 1034,   // Serious Stackware Error
	STWARE_M_CLALLOC_OUT_OF_CLICK    = 1035,   // Serious Stackware Error
	STWARE_MBUF_MULTI_MFREE      = 1036,   // Serious Stackware Error
	STWARE_MALLOC_FAILED         = 1037,
	STWARE_DUMMY_ERROR1          = 1038,
	STWARE_DUMMY_ERROR2          = 1039,
	STWARE_INFINITE_MBUF_LINK        = 1040,   // Serious Stackware Error
	HIGH_STWARE_FAULT_VALUE       = STWARE_INFINITE_MBUF_LINK,
	NUM_STWARE_FAULTS = (HIGH_STWARE_FAULT_VALUE-LOW_STWARE_FAULT_VALUE+1),

	LOW_DCI_FAULT_VALUE           = 2000,
	//default
	DCI_PARITY_ERROR         = LOW_DCI_FAULT_VALUE,
	DCI_INPUT_BUFFER_OVERFLOW_ERROR  = 2001,
	DCI_NON_SPECIFIC_ERROR       = 2002,
	DCI_UNKNOWN_ERROR            = 2003,
	///////////////////////////////////////////////////////////////////////////////////
	/////	Note that the following ennumerations (*_PORT1, *_PORT2, and *_PORT3) /////
	/////   are dependent on method error in DciReport.cc in subsystem DCI.       /////    
	///////////////////////////////////////////////////////////////////////////////////
	//Port 1
	DCI_PARITY_ERROR_PORT1       = DCI_PARITY_ERROR,
	DCI_INPUT_BUFFER_OVERFLOW_ERROR_PORT1= DCI_INPUT_BUFFER_OVERFLOW_ERROR,
	DCI_NON_SPECIFIC_ERROR_PORT1     = DCI_NON_SPECIFIC_ERROR,
	DCI_UNKNOWN_ERROR_PORT1      = DCI_UNKNOWN_ERROR,
	//Port 2
	DCI_PARITY_ERROR_PORT2       = 2010,
	DCI_INPUT_BUFFER_OVERFLOW_ERROR_PORT2= 2011,
	DCI_NON_SPECIFIC_ERROR_PORT2     = 2012,
	DCI_UNKNOWN_ERROR_PORT2      = 2013,
	//Port 3
	DCI_PARITY_ERROR_PORT3       = 2020,
	DCI_INPUT_BUFFER_OVERFLOW_ERROR_PORT3= 2021,
	DCI_NON_SPECIFIC_ERROR_PORT3     = 2022,
	DCI_UNKNOWN_ERROR_PORT3      = 2023,
	HIGH_DCI_FAULT_VALUE          = DCI_UNKNOWN_ERROR+8,  //8 additional unique error codes
	NUM_DCI_FAULTS = (HIGH_DCI_FAULT_VALUE-LOW_DCI_FAULT_VALUE+1),

	LOW_SAFETYNET_FAULT_VALUE		= 3000,
	SAFETYNET_CHECKSUM_ERROR		= 3000,
	SAFETYNET_CYCLE_TIME_ERROR		= 3001,
	SAFETYNET_INTERVAL_TIME_ERROR		= 3002,
	SAFETYNET_SYNC_ACQUIRED			= 3003,
	SAFETYNET_SYNC_LOST			= 3004,
	HIGH_SAFETYNET_FAULT_VALUE		= 3009,
	NUM_SAFETYNET_FAULTS = (HIGH_SAFETYNET_FAULT_VALUE-LOW_SAFETYNET_FAULT_VALUE+1),

	// this must be the last line!
	NUM_COMM_FAULT_IDS = (NUM_NETAPP_FAULTS+NUM_STWARE_FAULTS+NUM_DCI_FAULTS+NUM_SAFETYNET_FAULTS)
};


enum PortId
{
	ANY_PORT_ID			= 0,
	BD_BOARD_TCPPORT 		= 7000,
	LOWEST_PORT_ID			= BD_BOARD_TCPPORT,
	BLOCK_IO_TCPPORT 		= 7001,
	WAVEFORM_UDPPORT 		= 7002,
	PATIENT_DATA_UDPPORT 	= 7003,
	SETTINGS_UDPPORT 		= 7004,
	ALARMS_UDPPORT 			= 7005,
	BROADCAST_UDPPORT  		= 7006,
	SERVICE_DATA_UDPPORT    = 7007,
    BD_DEBUG_SRC_UDPPORT0   = 7008,
    BD_DEBUG_DEST_UDPPORT0  = 7009,
    BD_DEBUG_SRC_UDPPORT1   = 7010,
    BD_DEBUG_DEST_UDPPORT1  = 7011,
    BD_DEBUG_SRC_UDPPORT2   = 7012,
    BD_DEBUG_DEST_UDPPORT2  = 7013,
    BD_DEBUG_SRC_UDPPORT3   = 7014,
    BD_DEBUG_DEST_UDPPORT3  = 7015,
	BD_ADDR_BROADCAST_PORT  = 7016,
	GUI_ADDR_BROADCAST_PORT = 7017,
    HIGHEST_PORT_ID = GUI_ADDR_BROADCAST_PORT
};  


//@ Type:  SoftFaultID
// Software exception identifier.
enum SoftFaultID
{
  UNDEFINED_SOFT_FAULT,  // this MUST be zero...

  INVARIANT_ID,
  PRE_CONDITION_ID,
  POST_CONDITION_ID,
  ASSERTION_ID,

  NUM_SOFT_FAULT_IDS
};


//@ Type:  SubSystemID
// Identifications for each of the subsystems of the Sigma System.
enum SubSystemID
{
  ALARM_ANALYSIS	   = 0,
  BD_IO_DEVICES		   = 1,
  BREATH_DELIVERY	   = 2,
  DCI_DEVICE		   = 3,
  FOUNDATION		   = 4,
  GUI_APPLICATIONS	   = 5,
  GUI_FOUNDATIONS	   = 6,
  GUI_IO_DEVICES	   = 7,
  OS_FOUNDATION		   = 8,
  OS_PLATFORM		   = 9,
  NETWORK_APPLICATION  = 10,
  PATIENT_DATA		   = 11,
  PERSISTENT_OBJS	   = 12,
  POST			       = 13,
  SAFETY_NET		   = 14,
  SERVICE_DATA		   = 15,
  SERVICE_MODE		   = 16,
  SETTINGS_VALIDATION  = 17,
  STACK_WARE		   = 18,
  SYS_INIT	 	       = 19,
  VENTILATOR_SELF_TEST = 20,
  VENTILATOR_OBJECTS   = 21,
  VGA_GRAPHICS_DRIVERS = 22,
  UTILITIES		       = 23,
  DOWNLOAD		       = 24,
  GUI_SERIAL_INTERFACE = 25,
  TREND_DATABASE	   = 26

  // new subsystems to be added below...
};


enum BkEventName 
{
    BK_NO_EVENT = 0,
    BK_SV_SWITCHED_SIDE_OOR,
    BK_EXH_FLOW_OOR,
    BK_O2_PSOL_CURRENT_OOR,
    BK_AIR_PSOL_CURRENT_OOR,
    BK_EXH_MOTOR_CURR_OOR,
    BK_EXH_VLV_COIL_TEMP_OOR,
    BK_EXH_PRESS_OOR,
    BK_INSP_PRESS_OOR,
    BK_AIR_FLOW_OOR_HIGH,
    // 10    
    BK_AIR_FLOW_OOR_LOW,
    BK_AIR_FLOW_TEMP_OOR,
    BK_O2_FLOW_OOR_HIGH,
    BK_O2_FLOW_OOR_LOW,
    BK_O2_FLOW_TEMP_OOR,
    BK_EXH_FLOW_TEMP_OOR,
    BK_BD_10V_SUPPLY_MON_OOR,
    BK_BD_12V_FAIL_OOR,
    BK_BD_15V_FAIL_OOR,
    BK_NEG_15V_FAIL_OOR,
    // 20
    BK_GUI_12V_FAIL_OOR,
    BK_GUI_5V_FAIL_OOR,           
    BK_BD_5V_FAIL_OOR,
    BK_O2_PSOL_STUCK,
    BK_AIR_PSOL_STUCK,
    BK_AIR_PSOL_STUCK_OPEN,
    BK_O2_PSOL_STUCK_OPEN,
    BK_ATM_PRESS_OOR,
    BK_O2_SENSOR_OOR,
    BK_O2_SENSOR_OOR_RESET,
    // 30
    BK_SVO_CURRENT_OOR,
    BK_PI_STUCK,
    BK_PE_STUCK,
    BK_INSP_AUTOZERO_FAIL,
    BK_EXH_AUTOZERO_FAIL,
    BK_POWER_FAIL_CAP,        
    BK_ALARM_CABLE,
    BK_ADC_FAIL_HIGH,                        
    BK_ADC_FAIL_LOW,                
    BK_ADC_LOOPBACK_FAIL,            
    // 40
    BK_TOUCH_SCREEN_FAIL,            
    BK_TOUCH_SCREEN_BLOCKED,            
    BK_TOUCH_SCREEN_RESUME,
    BK_AC_SWITCH_STUCK,
    BK_BD_NOVRAM_CHECKSUM,
    BK_BD_TOD_FAIL,
    BK_GUI_TOD_FAIL,                         
    BK_GUI_NOVRAM_CHECKSUM,
    BK_BPS_VOLTAGE_OOR,                
    BK_BPS_CURRENT_OOR,            
    // 50
    BK_BPS_MODEL_OOR,                
    BK_EXH_HEATER_OOR,                
    BK_GUI_STUCK_KEY,
    BK_BD_EEPROM_CHECKSUM,
    BK_GUI_EEPROM_CHECKSUM,
    BK_GUI_SAAS_COMM_FAIL,            
    BK_COMPR_ELAPSED_TIMER,
	BK_COMPR_BAD_DATA,
    BK_LOSS_OF_GUI_COMM,                     
    BK_LOSS_OF_BD_COMM,
    // 60
    BK_RESUME_GUI_COMM,
    BK_RESUME_BD_COMM,
    BK_EST_REQUIRED,
    BK_GUI_SAAS_AUDIO_FAIL,
    BK_LV_REF_OOR,                
    BK_SV_CURRENT_OOR,                       
    BK_MON_ALARMS_FAIL,
    BK_MON_APNEA_ALARM_FAIL,
    BK_MON_APNEA_INT_FAIL,
    BK_MON_HIP_FAIL,
    // 70
    BK_MON_INSP_TIME_FAIL,
    BK_MON_NO_DATA,
    BK_MON_DATA_CORRUPTED,
    BK_MON_O2_MIXTURE_FAIL,                
    BK_MON_BREATH_TIME_FAIL,                
    BK_DATAKEY_UPDATE_FAIL,    
    BK_TASK_MONITOR_FAIL,    
    BK_GUI_WAVEFORM_DROP,
    BK_BD_WAVEFORM_DROP,
    BK_FORCED_VENTINOP,
    // 80
    BK_BD_MULT_MAIN_CYCLE_MSGS,
    BK_BD_MULT_SECOND_CYCLE_MSGS,
    BK_WATCHDOG_FAILURE,
    BK_INIT_RESUME_GUI_COMM,
    BK_INIT_RESUME_BD_COMM,
    BK_INIT_LOSS_GUI_COMM,
    BK_INIT_LOSS_BD_COMM,
    BK_COMPR_UPDATE_SN,
    BK_COMPR_UPDATE_PM_HRS,
    BK_DATAKEY_SIZE_ERROR,
    // 90
    BK_TOUCH_SCREEN_BLOCKED_INFO,
    BK_TOUCH_SCREEN_RESUME_INFO,
    BK_BPS_EVENT_INFO,
    BK_COMPACT_FLASH_ERROR_INFO,
    BK_PROX_ERROR_INFO,

    BK_MAX_EVENT                                  
};


class OsFoundation
{
  public:
    //@ Type:  OsClassId
    // Ids of all of the classes/files of this subsystem.
    // Used for Fault Handling.
    enum OsClassId 
    {
        MEMORY              = 0,
        MAILBOX             = 1,
        MSG_QUEUE           = 2,
        MSG_TIMER           = 3,
        MUTEX               = 4,
        TASK                = 5,
        TASK_INFO           = 6,
        TIMER_TBL           = 7,
        OSFOUNDATION_CLASS  = 8,
        TIMESTAMP           = 9,
        OSTIMESTAMP         = 10,
        IPC_QUEUE           = 11,
        BUFFERPOOL          = 12,
        HIRESTIMER          = 13,
		OSTREAM             = 14,
		SERIAL_PORT         = 15,
    
        NUM_OSFOUNDATION_CLASSES
    };

};


struct InterTaskMessage 
{
    Uint   msgId    : 8;
    Uint   msgData  : 24;
};

enum MessageIdRange
{
    FIRST_TASK_CONTROL_MSG    = 1,
    FIRST_TASK_MONITOR_MSG    = 11,
    FIRST_APPLICATION_MSG     = 101
};


//@ Type:  Precision
// The level of accuracy for a floating-point number.
enum Precision
{
  THOUSANDTHS = -3,	// accurate to 10^(-3)...
  HUNDREDTHS  = -2,	// accurate to 10^(-2)...
  TENTHS      = -1,	// accurate to 10^(-1)...
  ONES        =  0,	// accurate to 10^0...
  TENS        =  1,	// accurate to 10^1...
  HUNDREDS    =  2,	// accurate to 10^2...
  THOUSANDS   =  3	// accurate to 10^3...
};




struct TubeTypeValue
{
  //@ Type:  TubeTypeValueId 
  enum TubeTypeValueId
  {
    // $[TC02035] -- setting values...
    ET_TUBE_TYPE,
    TRACH_TUBE_TYPE,

    TOTAL_TUBE_TYPE_VALUES
  };
};

struct TriggerTypeValue
{
  //@ Type:  TriggerTypeValueId
  // All of the possible values of the Trigger Type Setting.
  enum TriggerTypeValueId
  {
    // $[02256] -- values of the range of Trigger Type Setting...
    FLOW_TRIGGER,
    PRESSURE_TRIGGER,
    
    TOTAL_TRIGGER_TYPES
  };
};

struct SupportTypeValue
{
  //@ Type:  SupportTypeValueId
  // All of the possible values of the Support Type Setting.
  enum SupportTypeValueId
  {
    // $[02248] -- values of the range of Support Type Setting...

    // standard values...
    OFF_SUPPORT_TYPE,
    PSV_SUPPORT_TYPE,

    // optional values...
    ATC_SUPPORT_TYPE,
    VSV_SUPPORT_TYPE,
    PAV_SUPPORT_TYPE,

    TOTAL_SUPPORT_TYPES
  };
};


struct MandTypeValue
{
  //@ Type:  MandTypeValueId
  // All of the possible values of the Mandatory Type Setting.
  enum MandTypeValueId
  {
    // $[02071] -- values of the range of Apnea Mandatory Type Setting...
    // $[02190] -- values of the range of Mandatory Type Setting...

    // standard values...
    PCV_MAND_TYPE,
    VCV_MAND_TYPE,

    // optional values...
    VCP_MAND_TYPE,

    TOTAL_MAND_TYPES
  };
};

struct ModeValue
{
  //@ Type:  ModeValueId 
  // All of the possible values of the Mode Setting.
  enum ModeValueId
  {
    // $[02200] -- values of the range of Mode Setting...

    // standard values...
    AC_MODE_VALUE,
    SIMV_MODE_VALUE,
    SPONT_MODE_VALUE,

    // optional values...
    BILEVEL_MODE_VALUE,
	CPAP_MODE_VALUE,

    TOTAL_MODE_VALUES
  };
};

struct VentTypeValue
{
  //@ Type:  VentTypeValueId 
  enum VentTypeValueId
  {
    // $[NI02007] -- setting values...
    INVASIVE_VENT_TYPE,
    NIV_VENT_TYPE,

    TOTAL_VENT_TYPE_VALUES
  };
};


struct PatientCctTypeValue
{
  //@ Type:  PatientCctTypeValueId
  // All of the possible values of the Patient Circuit Type Setting.
  enum PatientCctTypeValueId
  {
    // $[02213] -- values of the range of Patient Circuit Type Setting...

    // standard values...
    ADULT_CIRCUIT,
    PEDIATRIC_CIRCUIT,

    // optional values...
    NEONATAL_CIRCUIT,

    TOTAL_CIRCUIT_TYPES
  };
};

struct NominalLineVoltValue
{
  //@ Type:  NominalLineVoltValueId
  // All of the possible values for nominal line voltage  
  enum NominalLineVoltValueId
  {
    // $[02206] -- values of the range of Nominal Line Voltage Setting...
    VAC_100,
    VAC_120,
    VAC_220,
    VAC_230,
    VAC_240,

    TOTAL_NOMINAL_VOLT_VALUES 
  };
};

struct HumidTypeValue
{
  //@ Type:  HumidTypeValueId
  // All of the possible values of the Humidification Type Setting.
  enum HumidTypeValueId
  {
    // $[02153] -- values of the range of Humidifier Type Setting...
    NON_HEATED_TUBING_HUMIDIFIER,
    HEATED_TUBING_HUMIDIFIER,
    HME_HUMIDIFIER,

    TOTAL_HUMIDIFIER_TYPES  
  };
};

struct FlowPatternValue
{
  //@ Type:  FlowPatternValueId
  // All of the possible values of the Flow Pattern Setting.
  enum FlowPatternValueId
  {
    // $[02049] -- values of the range of Apnea Flow Pattern Setting...
    // $[02125] -- values of the range of Flow Pattern Setting...
    SQUARE_FLOW_PATTERN,
    RAMP_FLOW_PATTERN,

    TOTAL_FLOW_PATTERNS
  };
};

struct Fio2EnabledValue
{
  //@ Type:  Fio2EnabledValueId 
  // All of the possible values of the FIO2-Enabled Setting.
  enum Fio2EnabledValueId
  {
    // $[02274] -- values of the range of FIO2-Enabled Setting...
    NO_FIO2_ENABLED,
    YES_FIO2_ENABLED,
	CALIBRATE_FIO2,

    TOTAL_FIO2_ENABLED_VALUES
  };
};

struct LeakCompEnabledValue
{
  //@ Type:  LeakCompEnabledValueId 
  // All of the possible values of the Leak Compensation Enabled Setting.
  enum LeakCompEnabledValueId
  {
    // Values of the range of Leak Compensation Enabled Setting...
    LEAK_COMP_DISABLED,
    LEAK_COMP_ENABLED,

    TOTAL_LEAK_COMP_ENABLED_VALUES
  };
};

struct ProxEnabledValue
{
	//@ Type:  ProxEnabledValueId 
	// All of the possible values of the Prox Enabled Setting.
	enum ProxEnabledValueId
	{
		// Values of the range of Prox Enabled Setting...
		PROX_DISABLED,
		PROX_ENABLED,

		TOTAL_PROX_ENABLED_VALUES
	};
};

//@ Type: SigmaState
//  Processing states for the GUI or BD.  Not to exceed 1 byte.
enum SigmaState
{
     STATE_START     = 0x0001
    ,STATE_ONLINE    = 0x0002
    ,STATE_SERVICE   = 0x0004
    ,STATE_SST       = 0x0008
    ,STATE_INOP      = 0x0010
    ,STATE_TIMEOUT   = 0x0020
    ,STATE_INIT      = 0x0040
    ,STATE_UNKNOWN   = 0x0080
    ,STATE_FAILED    = 0x0100
};


// Enumeration indicating the length of the last downtime period.
enum DowntimeStatus
{
	DOWN_SHORT,
	DOWN_LONG
};


// Types of CPU resets
enum  ShutdownState
{
    UNKNOWN     = 1,
    EXCEPTION   = 2,
    ACSWITCH    = 3,
    POWERFAIL   = 4,
    WATCHDOG    = 5,
    INTENTIONAL = 6
};
 


// Criticality ranking of POST failures
// A post test return status is always one of these
enum  Criticality
{
    PASSED            = 0,
    MINOR             = 1,
    SERVICE_REQUIRED  = 2,
    MAJOR             = 3
};
 

enum FlashMemCons
{
	SERVICE_DATA_SPACE = 0x18000,		 // 96k bytes long..
	FLASH_CHECKSUM_SPACE = 0x400,          // 1024 bytes.
	GUI_FLASH_MEM_SIZE = 0x400000,			// 4 Mega bytes
	BD_FLASH_MEM_SIZE =   0x200000, 		// 2 Mega bytes
	SERIAL_NO_LENGTH = 3,   // 3 words long
	SERIAL_NO_INIT_PATTERN = 0x58585858,    // four 'X' chars
	SERIAL_NO_SIZE = 10,  // 10 digits
	BOOTPROM_SIZE = 0x80000,		 // 512k bytes.
	FLASH_WRITE_RETRIES = 3,
	CHECKSUM_BLOCK_SIZE = 0x40000,	// 256k bytes.
	MAX_FLASH_WRITE_TIME		= 2,	//  2 VRTX ticks, at least 10 ms.
	FLASH_FILL_CHAR = 0xFF
};