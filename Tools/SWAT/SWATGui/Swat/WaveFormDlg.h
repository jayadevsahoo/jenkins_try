#pragma once
#include "afxcmn.h"
#include "resource.h"
#include "PD_ComboListCtrl.h"


// CWaveFormDlg dialog

class CWaveFormDlg : public CDialog
{
	DECLARE_DYNAMIC(CWaveFormDlg)

public:
	CWaveFormDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CWaveFormDlg();

// Dialog Data
	enum { IDD = IDD_WAVEFORM_MANAGER };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	//CListCtrl m_List;
	CComboListCtrl m_List;
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	void PopulateInitialData();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedButtonClear();
	afx_msg void OnBnClickedButtonSave();
	float m_nFrequency;
	float m_nTriggerDeg;
	float m_nTriggerPhaseDeg;
	afx_msg void OnBnClickedButtonOpen();
};
