
#ifndef Applicability_HH
#define Applicability_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Filename:  Applicability - Setting applicability states.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/Applicability.hhv   25.0.4.0   19 Nov 2013 14:27:14   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001   By: sah   Date:  26-Jan-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//     Created to work with new applicability functionality.
//  
//====================================================================

//@ Usage-Classes
//@ End-Usage


struct Applicability
{
  //@ Type:  Id
  enum Id
  {
    CHANGEABLE,	  // user can change setting...
    VIEWABLE,	  // setting's value is valid, but can't be directly changed...
    INAPPLICABLE  // the validity of the setting's value is unknown...
  };
};


#endif // Applicability_HH 
