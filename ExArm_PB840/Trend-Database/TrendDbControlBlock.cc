#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================
//
// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: TrendDbControlBlock
//---------------------------------------------------------------------
//@ Interface-Description
//  The Control Block agent manages the persistent information that is needed
//  between power cycles.
//    - Current frame sequence number
//    - The "head" and "size" for each repository tables
//    - The current position in each of the write buffers
//	
//  Two blocks are used so that on any failure the agent can revert back to the
//  previous update and only the last update will be lost.
// 
//	The agent is initialized by the TrendDataMgr using the initialize()
//  call, this call will pick the control block which has the highest frame
//  number as the current control block.  If neither block is valid then
//  the control block will be initialized to empty.  The current control 
//  blocks data is loaded "shadowed" in this class.
//  The flush() method is used to flush out the shadow control block stored
//  in this class and updates the control block reference, so that the 
//  next flush will use the other block out on the compact flash.
// 
//---------------------------------------------------------------------
//@ Rationale
//  Encapsulates the logic of the store/retrieve of the persistent data
//  needed by the TrendDataMgr and its repository agents.  
//---------------------------------------------------------------------
//@ Implementation-Description
//  The control blocks are stored on the compact flash, each block is 
//  stored to one block on the compact flash in a back and forth manner.
//  Block A is written first then B and then back to A and so on.
//  
//---------------------------------------------------------------------
//@ Fault-Handling
//  SoftFault is used
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//@ Version
// @(#) $Header:   /840/Baseline/Trend-Database/vcssrc/TrendDbControlBlock.ccv   25.0.4.0   19 Nov 2013 14:34:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By:  mjfullm    Date:  3-Apr-2007    SCR Number: 6237
//  Project:  Trend Project
//  Description:
//          Initial version.
//
//
//====================================================================

#include <string.h>

//@ Usage-Classes
#include "TrendDbControlBlock.hh"
#include "TrendDataMgr.hh"


//@ End-Usage




//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TrendDbControlBlock [constructor]
//
//@ Interface-Description
// Constructor the control block, the control block is loaded
// by the initialize() call
//---------------------------------------------------------------------
//@ Implementation-Description
//  Initialize the interface used for persistent storage
//  Initialize the source block to "NO_SOURCE_BLOCK", initialize()
//  will update the source block 
//---------------------------------------------------------------------
//@ PreCondition
//	The control block schema must fit in a compact flash block
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
TrendDbControlBlock::TrendDbControlBlock( void )
	:   storeInterface_( CompactFlash::GetCompactFlash() ),
		sourceBlock_( NO_SOURCE_BLOCK )
{
	TREND_MGR_DBG1( 2, "Constructor Trend Control Block" );

	// verify that the schema will fit in 512 bytes
	AUX_CLASS_PRE_CONDITION( (sizeof(Schema) <= sizeof(CompactFlash::Block)),
							 (sizeof(Schema) << 16) | sizeof(CompactFlash::Block) );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TrendDbControlBlock [destructor]
//
//@ Interface-Description
//  Does no action  
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
TrendDbControlBlock::~TrendDbControlBlock()	
{
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: initialize
//
//@ Interface-Description
//  Load both control blocks, assign the source block
//  to the control block with the highest frame sequence number
//---------------------------------------------------------------------
//@ Implementation-Description
// Initialize the control block, load both the 'A' and 'B' blocks
// Evaluate the frame sequence number on each block and assign the current block
// to the block with the highest frame number.
// If both blocks are invalid then set the control block to empty
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDbControlBlock::initialize( void )
{
	TREND_MGR_DBG1( 2, "Initialize the control blocks" );

	CompactFlash::Block     blockA;
	CompactFlash::Block     blockB;
	Int32                   frameCountA = 0;
	Int32                   frameCountB = 0;

	// Load Block A 
	// Verify the block and get the frame count
	if (storeInterface_.read( TrendDataMgr::TREND_DATABASE_INFO_BLOCK_A, blockA, 1 ) == SUCCESS)
	{
		Schema * ptrSchema = (Schema *) blockA;
		Uint16 checksum = computeChecksum_( blockA );
		if (checksum == 0 && ptrSchema->version == TrendDataMgr::TREND_VERSION)
		{
			frameCountA = ptrSchema->currentFrame;
			TREND_MGR_DBG2( 2, "Control Block A with frame ", frameCountA );
		}
		else
		{
			TREND_MGR_DBG4( 1, "Control Block A invalid, checksum ", checksum,
							", version ", ptrSchema->version );
		}
	}
	else
	{
		TREND_MGR_DBG1( 1, "Error loading control Block A" );
	}

	// Load Block B
	// Verify the block and get the frame count
	if (storeInterface_.read( TrendDataMgr::TREND_DATABASE_INFO_BLOCK_B, blockB, 1 ) == SUCCESS)
	{
		Schema * ptrSchema = (Schema *) blockB;
		Uint16 checksum = computeChecksum_( blockB );
		if (checksum == 0 && ptrSchema->version == TrendDataMgr::TREND_VERSION)
		{
			frameCountB = ptrSchema->currentFrame;
			TREND_MGR_DBG2( 2, "Control Block B with frame ", frameCountB );
		}
		else
		{
			TREND_MGR_DBG4( 1, "Control Block B invalid, checksum ", checksum,
							", version ", ptrSchema->version );
		}
	}
	else
	{
		TREND_MGR_DBG1( 1, "Error loading control Block B" );
	}

	// Check to see if Block A is valid and "more current"
	if (frameCountA && frameCountA > frameCountB)
	{
		TREND_MGR_DBG1( 2, "Use control Block A" );
		sourceBlock_ = SOURCE_BLOCK_A;
		memcpy( controlBlockData_, blockA, sizeof(Schema) );
	}

	// else use control block B if valid
	else if (frameCountB)
	{
		TREND_MGR_DBG1( 2, "Use control block B" );
		sourceBlock_ = SOURCE_BLOCK_B;
		memcpy( controlBlockData_, blockB, sizeof(Schema) );

	}

	// both frame A and frame B are invalid
	else
	{
		TREND_MGR_DBG1( 1, "No valid control Blocks use default" );

		// Initialize to empty database
		memset( controlBlockData_, 0, sizeof(Schema) );

		// Set default version and control block to A
		Schema * pControlBlock = (Schema *) controlBlockData_;
		pControlBlock->version = TrendDataMgr::TREND_VERSION;
		sourceBlock_ = SOURCE_BLOCK_A;
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: flush
//
//@ Interface-Description
//  Flush the control block to the compact flash
//  and update the next source block
//---------------------------------------------------------------------
//@ Implementation-Description
//  Writes the ram copy of the control block, stored in this class, to the
//  compact flash.  If this is successful then update the source block
//  and return the SUCCESS status.  If there is an error don't update the
//  source block and return FAILURE.
//---------------------------------------------------------------------
//@ PreCondition
//	The source block must be initialized and be either block A or B
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
SigmaStatus TrendDbControlBlock::flush( void )
{
	TREND_MGR_DBG2( 4, "Flush control block ", sourceBlock_ );

	Uint32 blockNum = 0;
	SourceBlock nextSourceBlock = NO_SOURCE_BLOCK;

	// Set the block to either A or B control blocks
	if (sourceBlock_ == SOURCE_BLOCK_A)
	{
		blockNum = TrendDataMgr::TREND_DATABASE_INFO_BLOCK_A;
		nextSourceBlock = SOURCE_BLOCK_B;
	}
	else if (sourceBlock_ == SOURCE_BLOCK_B)
	{
		blockNum = TrendDataMgr::TREND_DATABASE_INFO_BLOCK_B;
		nextSourceBlock = SOURCE_BLOCK_A;
	}

	// Verify that the pre condition is meet
	AUX_CLASS_PRE_CONDITION( (blockNum != 0 && nextSourceBlock != NO_SOURCE_BLOCK),
							 sourceBlock_ );


	// Update the checksum value
	// Clear it first so that it's not included before computing the checksum
	Schema * pControl = (Schema *) controlBlockData_;
	pControl->checksum = 0; 
	pControl->checksum = computeChecksum_( (Uint16 *) pControl );

	// Save the control block to the compact flash
	SigmaStatus flushStatus = storeInterface_.write( blockNum, controlBlockData_, 1 );

	// If successful then update the source block
	if (flushStatus == SUCCESS)
	{
		TREND_MGR_DBG2( 16, "Control Block Flush successful, LAB block num ", blockNum );
		sourceBlock_ = nextSourceBlock;
	}

	return( flushStatus );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: computeChecksum_
//
//@ Interface-Description
//  Calculate a 16 bit checksum for the Schema memory
//---------------------------------------------------------------------
//@ Implementation-Description
//  Xor the words (16 bits) from the last in the Schema to the first
//  If the correct checksum is already stored the result returned will
//  be zero.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
Uint16 TrendDbControlBlock::computeChecksum_( Uint16 * pData )
{
	Uint16  checksum = 0;
	Uint16 idx = sizeof( Schema ) / sizeof( Uint16 );

	while (idx)
	{
		checksum ^= *(pData + (idx - 1));
		idx--;
	}

	return( checksum );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void TrendDbControlBlock::SoftFault(const SoftFaultID  softFaultID,
								   const Uint32       lineNumber,
								   const char*        pFileName,
								   const char*        pPredicate)  
{
	FaultHandler::SoftFault(softFaultID, TREND_DATABASE, TREND_DB_CONTROL_BLOCK,
							lineNumber, pFileName, pPredicate);
}

