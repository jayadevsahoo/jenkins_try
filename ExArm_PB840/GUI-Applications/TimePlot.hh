#ifndef TimePlot_HH
#define TimePlot_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: TimePlot - A drawable class that can plot a specified 
// stream of waveform data against time for use in the Waveforms subscreen.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/TimePlot.hhv   25.0.4.0   19 Nov 2013 14:08:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004   By: sah    Date:  11-Jul-2000    DCS Number: 5755
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      *  added support for shadow waveform traces
//      *  deleted 'lastY_' and 'currY_' as members, they are now stack
//         variables in the only method that they were being used
//
//  Revision: 003  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "WaveformPlot.hh"

//@ Usage-Classes
#include "BreathPhaseType.hh"
//@ End-Usage

class WaveformIntervalIter;

class TimePlot : public WaveformPlot
{
public:
	TimePlot(const Boolean isUpperPlot);
	~TimePlot(void);

	virtual void activate(void);
	virtual void update(WaveformIntervalIter& segmentIter);
	virtual void endPlot(void);
	virtual void erase(void);

	inline void setYUnits(PatientDataId::PatientItemId yUnits);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:

private:
	// these methods are purposely declared, but not implemented...
	TimePlot(const TimePlot&);		// not implemented...
	void   operator=(const TimePlot&);	// not implemented...

	//@ Data-Member: yUnits_
	// The Y units patient data id which specifies which patient data we
	// should plot.
	PatientDataId::PatientItemId yUnits_;

	//@ Data-Member: currX_
	// The current X pixel coordinate.
	Uint32 currX_;

	//@ Data-Member: lastX_
	// The last X pixel coordinate, i.e. the one previous to the current.
	Int32 lastX_;

	//@ Data-Member: sampleNum_
	// The current sample number counted from the first sample retrieved
	// when reading the complete waveform data for plotting.
	Uint32 sampleNum_;

	//@ Data-Member: lastSample_
	// The last sample value prior to the current sample.
	Real32 lastSample_;

	//@ Data-Member: lastAuxSample_
	// The last auxillary sample value prior to the current auxillary sample
	// (used for shadow traces).
	Real32 lastAuxSample_;

	//@ Data-Member: minEnvelopeSample_
	// The current minimum sample value in the "X-axis envelope".
	Real32 minEnvelopeSample_;

	//@ Data-Member: maxEnvelopeSample_
	// The current maximum sample value in the "X-axis envelope".
	Real32 maxEnvelopeSample_;

	//@ Data-Member: minAuxEnvelopeSample_
	// The current auxillary minimum sample value in the "X-axis envelope".
	Real32 minAuxEnvelopeSample_;

	//@ Data-Member: maxAuxEnvelopeSample_
	// The current auxillary maximum sample value in the "X-axis envelope".
	Real32 maxAuxEnvelopeSample_;

	//@ Data-Member: isLastPointSet_
	// Indicates if there is a valid X value stored in lastX_.
	Boolean isLastPointSet_;

	//@ Data-Member: isShadowDrawn_
	// Indicates whether any shadow trace is currently drawn for this plot.
	Boolean isShadowDrawn_;

	//@ Data-Member: envelopingSamples_
	// Indicates that data samples are being enveloped at the same X value.
	Boolean envelopingSamples_;

	//@ Data-Member: lineColor_
	// Indicates the current plot line color 
	Colors::ColorS lineColor_;

	//@ Data-Member: currPhaseType_
	// Indicates the breath phase type for the current sample
	BreathPhaseType::PhaseType currPhaseType_; 

	//@ Data-Member: prevPhaseType_
	// Indicates the breath phase type for the previous sample
	BreathPhaseType::PhaseType prevPhaseType_;
};

// Inlined methods
#include "TimePlot.in"

#endif // TimePlot_HH 
