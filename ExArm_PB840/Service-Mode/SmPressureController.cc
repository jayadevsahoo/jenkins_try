#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: SmPressureController - Pressure controller for Service Mode
//---------------------------------------------------------------------
//@ Interface-Description
//	This class is used to establish system pressure for the Service Mode
//	tests.  It is a derived class of the SmFlowController -- because of
//	this, an ASSERTION will result if the user attempts to use the
//	flow and pressure controllers at the the same time in order to
//	prevent two flow controllers from operating at the same time.
//---------------------------------------------------------------------
//@ Rationale
//	This class encapsulates the methods required for system pressurization
//	for Service Mode tests.
//---------------------------------------------------------------------
//@ Implementation-Description
//	The method establishPressure() is used to start the pressure algorithm,
//	stopFlowController() stops the algorithm, and newCycle() executes
//	the algorithm.
//	NOTE: as a Service Mode utility class, there is no direct SRS
//	traceability.
//---------------------------------------------------------------------
//@ Fault-Handling
//	n/a
//---------------------------------------------------------------------
//@ Restrictions
//	Only one instance of this class may be created.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/SmPressureController.ccv   25.0.4.0   19 Nov 2013 14:23:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004   By: syw   Date:  14-Sep-2000    DR Number: 5695
//  Project:  VTPC
//  Description:
//		Made getPressureTarget() inline
//
//  Revision: 003  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 002  By:  quf    Date: 23-May-1997    DR Number: DCS 2131
//       Project:  Sigma (840)
//       Description:
//            Fixed global reference names (i.e. "r" changed to "R").
//            Removed use of "this" pointer to determine flow control
//            owner, use an ID (initialized during construction) instead.
//
//  Revision: 001  By:  mad    Date:  2-Nov-1994    DR Number: none
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//=====================================================================

#include "SmPressureController.hh"

//@ Usage-Classes

#include "MainSensorRefs.hh"

#include "SmRefs.hh"
#include "SmGasSupply.hh"
#include "SmManager.hh"
#include "SmUtilities.hh"

#include "Task.hh"

#include "OsTimeStamp.hh"

#ifdef SIGMA_UNIT_TEST
# include "FakeIo.hh"
# define RSmGasSupply fakeSmGasSupply
# include "FakeSmGasSupply.hh"
extern FakeSmGasSupply fakeSmGasSupply;
#endif  // SIGMA_UNIT_TEST

#ifdef SIGMA_DEBUG
# include <stdio.h>
#endif  // SIGMA_DEBUG

//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SmPressureController()  [Default Constructor]
//
//@ Interface-Description
//	Constructor.
//	Initializes private data members.  No arguments.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Initialize private data members.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SmPressureController::SmPressureController(void)
  : SmFlowController(), 
  	requestForPressureControl_(FALSE),
    pressureControllerActive_(FALSE),
    pressureControllerDone_(FALSE),
    pressureTarget_(0.0),
    pPressureSensor_(&RInspPressureSensor)
{
// $[TI1]
	CALL_TRACE("SmPressureController::SmPressureController()");

	flowControlId_ = PRESSURE;

#ifdef SIGMA_DEBUG
	printf("flowControlId_ = %d\n", flowControlId_);
#endif  // SIGMA_DEBUG
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~SmPressureController()  [Destructor]
//
//@ Interface-Description
//	Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SmPressureController::~SmPressureController(void)
{
	CALL_TRACE("~SmPressureController()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: establishPressure()
//
//@ Interface-Description
//	This method is used to start the pressurization algorithm.
//	Four arguments: desired pressure, desired side to read pressure
//	(insp or exp), desired gas, and flow level to use.  No return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Initialize pressurization parameters to start building system pressure.
//	If gas type == EITHER, select gas using algorithm in SmUtilities.
//---------------------------------------------------------------------
//@ PreCondition
//	pressure control measurement side  == DELIVERY or EXHALATION.
//	gas type == AIR, O2, or EITHER.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SmPressureController::establishPressure( Real32          desiredPressure,
                                         MeasurementSide desiredSide,
                                         GasType         desiredGas,
                                         Real32 		 flowRate)
{
	CALL_TRACE("SmPressureController::establishPressure( \
										Real32          desiredPressure, \
                                        MeasurementSide desiredSide, \
                                        GasType         desiredGas, \
                                        Real32          flowRate)");

	CLASS_PRE_CONDITION ( desiredSide == DELIVERY ||
						  desiredSide == EXHALATION );

	CLASS_PRE_CONDITION ( desiredGas == AIR ||
						  desiredGas == O2 ||
						  desiredGas == EITHER );

	// If either gas can be used, select one
	if (desiredGas == EITHER)
  	{
	// $[TI1]
		desiredGas = RSmUtilities.selectGasType();
  	}
	// $[TI2]
	// implied else: a specific gas type was requested

	// use flow controller to build pressure
	establishFlow ( flowRate, DELIVERY, desiredGas );

	// Select the proper pressure sensor for pressure control
  	if (desiredSide == DELIVERY)
  	{
	// $[TI3]
    	pPressureSensor_ = &RInspPressureSensor;
  	}
  	else
  	{
	// $[TI4]
    	pPressureSensor_ = &RExhPressureSensor;
  	}
	//end else

	// Setup newCycle() parameters and signal newCycle()
	// to start controlling pressure
	pressureTarget_ = desiredPressure;
	pressureControllerDone_ = FALSE;
  	requestForPressureControl_ = TRUE;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: stopPressureController()
//
//@ Interface-Description
//  This method is used to stop the pressurization algorithm.
//	No arguments, no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Set controllers flags to deactivate pressure algorithm.
//	Wait for algorithm to deactivate via newCycle().
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SmPressureController::stopPressureController (void)
{
  CALL_TRACE("SmPressureController::stopPressureController(void)");

	// Signal pressure controller to stop
	requestForPressureControl_ = FALSE;
	pressureControllerDone_ = FALSE;

  	// Wait for the pressure controller to release control
  	while (pressureControllerActive_)
	{
	// $[TI1]
		Task::Delay(0, SM_CYCLE_TIME);

#ifdef SIGMA_UNIT_TEST
		// Since the service mode background task is not running,
		// we need to call newCycle() here to test this loop,
		// otherwise there's no way to exit the loop!
		newCycle();
#endif  // SIGMA_UNIT_TEST
	}

	// turn off the flow
	// NOTE: this sets FlowControlOwner_ to NONE
	stopFlowController();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newCycle(void)
//
//@ Interface-Description
//	This method implements the pressurization algorithm.
//	No arguments, no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	If pressure control is requested, set pressure controller active flag
//	TRUE and activate the flow controller.  Once pressure exceeds the
//	target pressure, stop the flow and signal that pressure controller
//	is done.
//
//	If pressure control is not requested and controller is active,
//	set the active flag FALSE and stop the flow controller.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SmPressureController::newCycle(void)
{

	CALL_TRACE("SmPressureController::newCycle(void)") ;
	
  	if (requestForPressureControl_)
  	{
	// $[TI1]
		// Foreground task requested pressure control

    	// Set the controller active indicator
    	pressureControllerActive_ = TRUE;

    	if (pPressureSensor_->getValue() > pressureTarget_)
    	{
		// $[TI1.1]
			// reached pressure target, so shut off flow and
			// signal that pressure controller is done
			requestForFlowControl_ = FALSE;
			pressureControllerDone_ = TRUE;
    	}
		// $[TI1.2]
		// implied else: didn't reach pressure target yet, so
		// do nothing for this iteration
	}
  	else
  	{
	// $[TI2]
		// Foreground task requested pressure control to stop

  		if (pressureControllerActive_)
  		{
		// $[TI2.1]
			// pressure control is active, so signal
			// inactive pressure control and shut
			// off flow
	    	pressureControllerActive_ = FALSE;
			requestForFlowControl_ = FALSE;
		}
		// $[TI2.2]
		// implied else: pressure controller is already inactive,
		// so do nothing
   	}

	// Perform a flow controller cycle.
	// NOTE: if flow controller is already active, then an assertion
	// is generated, otherwise there will be two flow controllers
	// fighting for control.
	SmFlowController::newCycle();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: waitForPressure()
//
//@ Interface-Description
//	This method is used to wait until the desired system pressure is
//	reached or until the defined timeout is reached.  Two arguments:
//	timeout in msec, and Boolean flag to indicate if pressure data
//	is to be continuously transmitted to the GUI during pressurization.
//	Returns TRUE if pressure was reached within the timeout, FALSE if not.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Wait until system pressure is reached or until timeout occurs.
//	While waiting and if 2nd argument is TRUE, send pressure data to
//	the GUI every few cycles.  Return the state of the pressure controller
//	done flag.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Boolean
SmPressureController::waitForPressure ( Int32 timeoutMS,
										Boolean displayData )
{
	CALL_TRACE("SmPressureController::waitForPressure (\
				Int32 timeoutMS, Boolean displayData)");

	Int32 timer = 0;
	Int32 dataSkipCounter = 0;
	do
	{
	// $[TI1]
#ifndef SIGMA_UNIT_TEST
		// Don't delay here during unit test for greater speed
		Task::Delay (0, SM_CYCLE_TIME);
#endif  // SIGMA_UNIT_TEST

		timer += SM_CYCLE_TIME;

		if (displayData)
		{
		// $[TI1.1]
			// send pressure bar graph data every 5th service mode cycle
			dataSkipCounter++;
			if (dataSkipCounter == 1)
			{
			// $[TI1.1.1]
				// send insp pressure value to gui
				RSmManager.sendMonitorPointToServiceData (
										RInspPressureSensor.getValue() );

#ifdef SIGMA_UNIT_TEST
				// back up the insp pressure sensor data index
				// for unit test to keep data in sync
				fakeInspPressureSensor.valueIndex--;
#endif  // SIGMA_UNIT_TEST

			}
			else if (dataSkipCounter == 5)
			{
			// $[TI1.1.2]
				// roll counter over
				dataSkipCounter = 0;
			}
			// $[TI1.1.3]
			// implied else
		}
		// $[TI1.2]
		// implied else: don't send pressure data to gui

#ifdef SIGMA_UNIT_TEST
		// Since the service mode background task is not running,
		// we need to call newCycle() here to test this loop,
		// otherwise there's no way to exit the loop!
		newCycle();
#endif  // SIGMA_UNIT_TEST

	}  while( !pressureControllerDone_ && timer < timeoutMS &&
			  !SmManager::CommDied() );

	return( pressureControllerDone_);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 
void
SmPressureController::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, SERVICE_MODE, SMPRESSURECONTROLLER,
                          lineNumber, pFileName, pPredicate);
}

//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//=====================================================================
//
//  Private Methods...
//
//=====================================================================
