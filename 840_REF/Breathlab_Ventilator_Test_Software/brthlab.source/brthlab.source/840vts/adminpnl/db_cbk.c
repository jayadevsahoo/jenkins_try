/***********************************************/
/****************** INCLUDES *******************/
/***********************************************/

#include <ansi_c.h>
#include <userint.h>
#include <utility.h>
#include "globals.h"
#include "cpvtmain.h"
#include "dbapi.h"
#include "ldb.h"
#include "mainpnls.h"
#include "MsgPopup.h"
#include "sysmenu.h"

/***********************************************/
/************ FUNCTION DECLARATION  ************/
/***********************************************/

void ClearPBfiles(void)
{
	FILE* hndl;
	DeleteFile("c:\\brthlab\\840vts\\logs\\72362.log");
	hndl = fopen("c:\\brthlab\\840vts\\logs\\72362.log", "w");
	fclose(hndl);
	DeleteFile("c:\\brthlab\\840vts\\logs\\72362.db1");
	hndl = fopen("c:\\brthlab\\840vts\\logs\\72362.db1", "w");
	fclose(hndl);
	DeleteFile("c:\\brthlab\\840vts\\logs\\72362.db2");
	hndl = fopen("c:\\brthlab\\840vts\\logs\\72362.db2", "w");
	fclose(hndl);

	return;
}


int CVICALLBACK DB_Conf_CB(int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	if(event == EVENT_COMMIT){
		switch(control){
			case DB_CONF_PACKDB:
				SetCtrlAttribute(gOPanelHandle[DB_CONF], DB_CONF_PACKDB, ATTR_DIMMED, TRUE);
				if(CPVTConfirmPopup(LDBf(Ldb_DB_Delete_title), LDBf(Ldb_DB_Sure_Delete), OK_CANCEL_DLG, 0)){
					SetSysPanelMessage(LDBf(Ldb_ClearingDB));				
					ClearJetDB();
					ClearPBfiles();
				}
				SetCtrlAttribute(gOPanelHandle[DB_CONF], DB_CONF_PACKDB, ATTR_DIMMED, FALSE);
				SetSysPanelMessage(NULL);				
				break;
		}
	}

	return 0;
}
