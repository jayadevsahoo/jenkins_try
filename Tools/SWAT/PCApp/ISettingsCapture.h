//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file ISettingsCapture.h
//----------------------------------------------------------------------------
#include "ILogger.h"

//-------------------------------------
// C L A S S  D E C L
//-------------------------------------

///@brief ISettingsCapture is the interface to SettingsCapture.
class ISettingsCapture{
    public:

    /// @brief addCallback allows user to add a callback to process 
    ///        incoming settings packet detected by pcap.
    /// 
    /// @param func Pointer to a function.
    /// @param obj  Pointer to an object.
    /// 
    /// @return  true on success.
    virtual bool addCallback(int (*func)(void *,char *, int), void *obj    ) = 0;


    /// @brief removeCallback removes callback with the function/obj signature.
    /// 
    /// @param func Pointer to function.
    /// @param obj  Pointer to object.
    /// 
    /// @return true on success.
    virtual bool removeCallback(int (*func)(void *,char *, int), void *obj ) = 0;

    /// @brief ~ISettingsCapture 
    virtual ~ISettingsCapture() = 0;

    //static ISettingsCapture *getInstance(char *dev, int (*func)(void *,char *, int)=0, void *obj=0 );
    
    /// @brief destroy
    virtual void destroy() = 0;

    /// @brief register_logger
    virtual bool register_logger(ILogger *pILogger) = 0;
};


typedef ISettingsCapture * (ISettingsCapture_F)(char *dev, int (*func)(void *, char *, int), void *obj);


/// @brief getSettingsCaptureInstance
/// 
/// @param dev  Internet address of outgoing device.
/// @param func function pointer to function to call when event occurs.
/// @param obj  Pointer to object to be passed into func when called.
///
/// @return Pointer to an ISettingsCapture object.
extern "C" __declspec(dllexport) ISettingsCapture * __cdecl 

    getSettingsCaptureInstance(char *dev, int (*func)(void *, char *, int), void *obj );

