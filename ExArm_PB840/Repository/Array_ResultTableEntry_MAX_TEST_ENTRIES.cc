#include "stdafx.h"

//====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: Array_ResultTableEntry_MAX_TEST_ENTRIES - Fixed Array with 'MAX_TEST_ENTRIES' Elements of Type 'ResultTableEntry'.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is an abstraction of an array of 'MAX_TEST_ENTRIES' elements, where
//  each element is of type 'ResultTableEntry'.  Safe element access is provided
//  with this class, along with access to the size of an array,
//  copying one array into another, setting all of the elements to
//  a particular value, and comparing two arrays.  This is the
//  derived class of 'Array_ResultTableEntry'.
//---------------------------------------------------------------------
//@ Rationale
//  This provides a "safe" array for use with both built-in types,
//  and user-defined types.  The client of this array, as opposed
//  to a 'C'-like array, will not have to deal with pointers, and
//  cannot step on memory that is out of the bounds of one of these
//  arrays.  Methods for copying and comparing are also provide with
//  this class.  The rationale for providing a templated class, as
//  opposed to writing specific array classes for each new array, is
//  that the same tested, fine-tuned code can be reused many times.
//---------------------------------------------------------------------
//@ Implementation-Description
//  A 'C' array of a constant length is used internally by this class.
//  This class initializes its base class ('Array_ResultTableEntry')
//  with its array and size, and then relies on its base class to
//  provide the functionality.  This division of implementation among
//  a base array, and the fixed derived array is done to reduce code
//  explosion -- many different sizes of the same type ('ResultTableEntry') can be
//  instantiated without having an immense growth in executable size.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  The one restriction on this class that limits its use as compared
//  to the built-in 'C'-like array is that a static initialization
//  list can't be used.  There are restrictions, or requirements, having
//  to do with the data type ('ResultTableEntry') used in this array.  The class
//  that is picked for 'ResultTableEntry' must have the following functionality:
//>Von
//              ResultTableEntry::ResultTableEntry(void);                          // default...
//              ResultTableEntry::operator=(const ResultTableEntry& aType);        // assignment...
//              ResultTableEntry::operator==(const ResultTableEntry& aType) const; // equivalence...
//              ResultTableEntry::operator!=(const ResultTableEntry& aType) const; // inequivalence...
//>Voff
//  Any class that lacks any of those methods will cause a compile-time
//  error.  (NOTE:  all built-in types already have all of this
//  functionality.)
//---------------------------------------------------------------------
//@ Invariants
//  anArray[0..MAX_TEST_ENTRIES] provides a legal instance of type 'ResultTableEntry', where
//  'anArray' is an instance of this class.
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Foundation/vcssrc/FixedArray.C_v   25.0.4.0   19 Nov 2013 14:03:46   pvcs  $
//
//@ Modification-Log
//
//   Revision: 003   By: sah   Date: 08-Feb-1999   DR Number: 5339
//   Project:  ATC
//   Description:
//	Removed 'SIGMA_DEVELOPMENT' flags from around 'SoftFault()'
//	method, to allow for mixed-mode builds.
//
//   Revision: 002   By: sah   Date: 07-Jan-1999   DR Number: 5321
//   Project:  ATC
//   Description:
//	Changed 'SoftFault()' method to an non-inlined method.
//	Also, as part of making all 'SoftFault()' methods non-inlined,
//	I'm also making those 'SoftFault()' methods that are NOT called
//	in PRODUCTION mode, DEVELOPMENT-only.
//
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//=====================================================================

#include "Array_ResultTableEntry_MAX_TEST_ENTRIES.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...


//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  Array_ResultTableEntry_MAX_TEST_ENTRIES(array)  [Copy Constructor]
//
//@ Interface-Description
//  Construct this new array using 'array' for initialization.  Each
//  element of this new array will be equivalent to the corresponding
//  element from 'array'.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Pass the address of this array's first element in the base
//  class's constructor, along with the size.  Use the base class's
//  assignment operator to copy the elements of 'array' into this
//  array.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  (*this == array)
//@ End-Method
//=====================================================================

Array_ResultTableEntry_MAX_TEST_ENTRIES::Array_ResultTableEntry_MAX_TEST_ENTRIES(
					const Array_ResultTableEntry_MAX_TEST_ENTRIES& array
					    )
					: Array_ResultTableEntry(pBlock_, MAX_TEST_ENTRIES)
{
  CALL_TRACE("Array_ResultTableEntry_MAX_TEST_ENTRIES(array)");

  Array_ResultTableEntry::operator=(array);  // forward to base class...
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  Array_ResultTableEntry_MAX_TEST_ENTRIES()  [Default Constructor]
//
//@ Interface-Description
//  Construct a new default array.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Pass the address of this array's first element in the base
//  class's constructor, along with the size.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Array_ResultTableEntry_MAX_TEST_ENTRIES::Array_ResultTableEntry_MAX_TEST_ENTRIES(void)
				    : Array_ResultTableEntry(pBlock_, MAX_TEST_ENTRIES)
{
  CALL_TRACE("Array_ResultTableEntry_MAX_TEST_ENTRIES()");
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ~Array_ResultTableEntry_MAX_TEST_ENTRIES()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this array, and its elements.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The compiler will automatically call the destructor on each
//  element in 'pBlock_', therefore nothing needs to be done
//  explicitly.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Array_ResultTableEntry_MAX_TEST_ENTRIES::~Array_ResultTableEntry_MAX_TEST_ENTRIES(void)
{
  CALL_TRACE("~Array_ResultTableEntry_MAX_TEST_ENTRIES()");
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                  [static]
//
//@ Interface-Description
//  Catch the synchronous faults generated by this class, and pass
//  them to 'FaultHandler'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

void
Array_ResultTableEntry_MAX_TEST_ENTRIES::SoftFault(const SoftFaultID softFaultID,
				 const Uint32      lineNumber,
				 const char*       pFileName,
				 const char*       pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, FOUNDATION, FIXED_ARRAY, lineNumber,
                          pFileName, pPredicate);
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

#if defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)

#include "Ostream.hh"

//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  print_(ostr)  [const, virtual]
//
// Interface-Description
//  This overrides the base class's pure virtual method, and dumps
//  an ASCII representation of this class to 'ostr'.
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreConditions
//  none
//---------------------------------------------------------------------
// PostConditions
//  none
// End-Method
//=====================================================================

Ostream&
Array_ResultTableEntry_MAX_TEST_ENTRIES::print_(Ostream& ostr) const
{
  CALL_TRACE("print_(ostr)");

  ostr << "Array_ResultTableEntry_MAX_TEST_ENTRIES {\n";

  for (Uint32 idx = 0; idx < MAX_TEST_ENTRIES; idx++)
  {
    ostr << "  [" << idx << "] = " << pBlock_[idx] << endl;
  }

  ostr << "}\n";
  ostr.flush();

  return(ostr);
}

#endif // defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)
