#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//================= C L A S S   D E S C R I P T I O N =================
//@ Class:  HighCctPressSetting - High Circuit Pressure Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, bounded setting that contains the upper limit of
//  circuit pressure (in cmH2O), above which the "high circuit pressure
//  alarm" shall be activated (this high pressure is not offset by PEEP).
//  This class inherits from 'BoundedSetting' and provides the specific
//  information needed for representation of high circuit pressure.  This
//  information includes the interval and range of this setting's values,
//  and this batch setting's new-patient value (see 'getNewPatientValue()').
//
//  This setting is an alarm limit setting, therefore this class overrides
//  the 'getMaxLimit()' and 'getMinLimit()' methods to provide access (for
//  the GUI) to this alarm's maximum and minimum limits.
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class has NO dependent settings, but 'updateConstraints_()' is
//  overridden to update this setting's dynamic bounds.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/HighCctPressSetting.ccv   25.0.4.0   19 Nov 2013 14:27:24   pvcs  $
//
//@ Modification-Log
//
//  Revision: 013   By: gdc   Date:  17-Feb-2005    DR Number: 6144
//  Project:  NIV1
//  Description:
//	    Additionally constrain lower bound of setting to low circuit
//      presssure limit
//
//  Revision: 012   By: sah   Date:  23-May-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//	VTPC project-related changes:
//      *  eliminated unneeded include of 'SupportTypeValue.hh'
//
//  Revision: 011   By: sah    Date: 29-Jun-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//      *  modified to use newly-modified 'BoundedRange' class to handle
//         range, via linked list of interval nodes
//      *  eliminated 'get{Max,Min}Limit()' method, now defined by base class
//      *  incorporated circuit-specific new-patient values and ranges
//
//  Revision: 010   By: sah  Date:  11-Jan-1999    DR Number:  5321
//  Project:  ATC
//  Description:
//	As part of making all 'SoftFault()' methods non-inlined, I'm
//	also making those 'SoftFault()' methods that are NOT called
//	in PRODUCTION mode, DEVELOPMENT-only.
//
//  Revision: 009   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'getApplicability()' method
//	*  modified 'updateConstraints_()' to factor in new BiLevel-specific
//	   settings (e.g., PEEP-low)
//	*  removed unnecessary 'isAcceptedValid()' method
//
//  Revision: 008   By: dosman    Date:  07-May-1998    DR Number: 
//  Project:  BILEVEL
//  Description:
//	add/fix TIs
//
//  Revision: 007   By: dosman    Date:  18-Feb-1998    DR Number: 
//  Project:  BILEVEL
//  Description:
//	when in bilevel we do not use the constraint 
//      HighCctPress > PEEP + PI + MIN_MAND_PRESS_BUFF
//	when in bilevel we do use the constraint 
//      HighCctPress > PEEP_HIGH + MIN_MAND_PRESS_BUFF
//
//  Revision: 006   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 005   By: dosman    Date:  24-Dec-1997    DR Number: 
//  Project:  BILEVEL
//  Description:
//	Initial BiLevel version.
//	Added symbolic constants.
//
//  Revision: 004   By: sah    Date:  28-May-1998    DR Number: 5079
//  Project:  COLOR
//  Description:
//	--- Merged Rev "Color" (1.23.1.0) into Rev "BiLevel" (1.23.2.0)
//	Change new-patient value from '60' to '40'.
//
//  Revision: 003   By: sah    Date:  11-Dec-1996    DR Number: 1623
//  Project: Sigma (R8027)
//  Description:
//	Changed minimum limit from "(PEEP + 6)" to "(PEEP + 7)".
//
//  Revision: 002   By: sah    Date:  08-Nov-1996    DR Number: 1562
//  Project: Sigma (R8027)
//  Description:
//	Fixed "warping" in 'updateConstraints_()' to be UP/DOWN, instead
//	of NEAREST; NEAREST causes problems due to incompatible resolutions.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//=====================================================================

#include "HighCctPressSetting.hh"
#include "MandTypeValue.hh"
#include "ModeValue.hh"
#include "SettingConstants.hh"
#include "PatientCctTypeValue.hh"

//@ Usage-Classes
#include "SettingsMgr.hh"
//@ End-Usage


//@ Code...

//====================================================================
//
//  Static Data Definitions...
//
//====================================================================

//  $[02133] -- The setting's range ...
//  $[02136] -- The setting's resolution ...
static const BoundedInterval  LAST_NODE_ =
  {
    0.0f,		// absolute minimum...
    0.0f,		// unused...
    ONES,		// unused...
    NULL
  };
static const BoundedInterval  INTERVAL_LIST_ =
  {
    100.0f,		// absolute maximum...
    1.0f,		// resolution...
    ONES,		// precision...
    &::LAST_NODE_
  };


//====================================================================
//
//  Public Methods...
//
//====================================================================

//============= M E T H O D   D E S C R I P T I O N ==================
//@ Method: HighCctPressSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This setting inherits from BoundedSetting.  The method passes to
//  BoundedSetting it's default information.  Also, this method initializes
//  the value interval range.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

HighCctPressSetting::HighCctPressSetting(void)
      : BatchBoundedSetting(SettingId::HIGH_CCT_PRESS,
			    Setting::NULL_DEPENDENT_ARRAY_,
			    &::INTERVAL_LIST_,
			    HIGH_CIRC_MAX_ID,		// maxConstraintId...
			    NULL_SETTING_BOUND_ID)	// minConstraintId...
{
  CALL_TRACE("HighCctPressSetting()");
}   // $[TI1]


//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  ~HighCctPressSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

HighCctPressSetting::~HighCctPressSetting(void)
{
  CALL_TRACE("~HighCctPressSetting()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This setting is ALWAYS changeable.
//
//  $[01081] -- breath settings applicability
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
HighCctPressSetting::getApplicability(const Notification::ChangeQualifier) const
{
  CALL_TRACE("getApplicability(qualifierId)");

  return(Applicability::CHANGEABLE);
}   // $[TI1]


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method: acceptTransition(settingId, newValue, currValue) [virtual]
//
//@ Interface-Description
//  This method is responsible for updating this setting based on the
//  the Main Control Setting identified by 'settingId' and the transition
//  of its value from 'currValue' to 'newValue'.  This is done for all
//  modes so input value of 'currValue' to 'newValue' are not used.
//
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//   None
//@ End-Method
//=====================================================================

void
	HighCctPressSetting::acceptTransition(const SettingId::SettingIdType settingId,
									   const DiscreteValue            newValue,
									   const DiscreteValue            currValue)
{
	CALL_TRACE("acceptTransition(settingId, newValue, currValue)");

	BoundedValue  highCctPress;
	BoundStatus  dummyStatus(getId());
	const Real32  HIGH_CCT_VALUE = BoundedValue(getAdjustedValue()).value;

	// calculate bound constraints...
	updateConstraints_();

	highCctPress.value = HIGH_CCT_VALUE;
	highCctPress.precision = ONES;

	getBoundedRange_().testValue(highCctPress, dummyStatus);

	// if the value changed
	if ( dummyStatus.getBoundState() != NO_BOUND_VIOLATION )
	{
		setAdjustedValue(highCctPress);

		// activation of transition rule must be italicized...
		setForcedChangeFlag();
	}
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [const, virtual]
//
//@ Interface-Description
//  Return the setting's new patient value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02134] -- new-patient value...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
HighCctPressSetting::getNewPatientValue(void) const
{
  CALL_TRACE("getNewPatientValue()");

  BoundedValue  newPatient;

  const Setting*  pPatientCctType =
		    SettingsMgr::GetSettingPtr(SettingId::PATIENT_CCT_TYPE);

  const DiscreteValue  PATIENT_CCT_TYPE_VALUE =
					pPatientCctType->getAdjustedValue();

  switch (PATIENT_CCT_TYPE_VALUE)
  {
  case PatientCctTypeValue::NEONATAL_CIRCUIT :		// $[TI1]
    newPatient.value = 30.0f;  // cmH2O...
    break;
  case PatientCctTypeValue::PEDIATRIC_CIRCUIT :
  case PatientCctTypeValue::ADULT_CIRCUIT :		// $[TI2]
    newPatient.value = 40.0f;  // cmH2O...
    break;
  default :
    // unexpected patient circuit type value...
    AUX_CLASS_ASSERTION_FAILURE(PATIENT_CCT_TYPE_VALUE);
    break;
  }
  
  newPatient.precision = ONES;

  return(newPatient);
}


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//	      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
HighCctPressSetting::SoftFault(const SoftFaultID  softFaultID,
			       const Uint32       lineNumber,
			       const char*        pFileName,
			       const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
			  HIGH_CIRCUIT_PRESS_SETTING, lineNumber, pFileName,
			  pPredicate);
}


//====================================================================
//
//  Protected Methods...
//
//====================================================================

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  updateConstraints_()  [virtual]
//
//@ Interface-Description
//  Update the dynamic bounds of this setting.  This method sets this
//  setting's maximum constraint to the most restrictive of this setting's
//  upper bounds, and, correspondingly, set this setting's minimum
//  constraint to its most restrictive lower bound.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02133] - The setting's range...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
HighCctPressSetting::updateConstraints_(void)
{
  CALL_TRACE("updateConstraints_()");

  const Setting*  pLowCirc = SettingsMgr::GetSettingPtr(SettingId::LOW_CCT_PRESS);
  const Setting*  pPeep = SettingsMgr::GetSettingPtr(SettingId::PEEP);
  const Setting*  pPeepHigh = SettingsMgr::GetSettingPtr(SettingId::PEEP_HIGH);
  const Setting*  pPeepLow = SettingsMgr::GetSettingPtr(SettingId::PEEP_LOW);
  const Setting*  pInspPress =
			  SettingsMgr::GetSettingPtr(SettingId::INSP_PRESS);
  const Setting*  pPressSupport =
		    SettingsMgr::GetSettingPtr(SettingId::PRESS_SUPP_LEVEL);

  Real32  peepValue;

  SettingBoundId  peepBasedMinId;
  SettingBoundId  psuppBasedMinId;

  if (pPeep->getApplicability(Notification::ADJUSTED) != 
						Applicability::INAPPLICABLE)
  {  // $[TI1] -- PEEP is applicable...
    peepValue       = BoundedValue(pPeep->getAdjustedValue()).value;
    peepBasedMinId  = HIGH_CIRC_MIN_BASED_PEEP_ID;
    psuppBasedMinId = HIGH_CIRC_MIN_BASED_PSUPP_PEEP_ID;
  }
  else
  {  // $[TI2] -- PEEP low must be applicable...
    SAFE_CLASS_ASSERTION(
	  (pPeepLow->getApplicability(Notification::ADJUSTED) != 
						Applicability::INAPPLICABLE));

    peepValue       = BoundedValue(pPeepLow->getAdjustedValue()).value;
    peepBasedMinId  = HIGH_CIRC_MIN_BASED_PEEPL_ID;
    psuppBasedMinId = HIGH_CIRC_MIN_BASED_PSUPP_PEEPL_ID;
  }

  const Real32  HIGH_CCT_VALUE = BoundedValue(getAdjustedValue()).value;

  BoundedRange::ConstraintInfo  minConstraintInfo;
  BoundStatus                   dummyStatus(getId());


  //-------------------------------------------------------------------
  // calculate minimum constraint based on the PEEP value..
  //-------------------------------------------------------------------

  BoundedValue  peepBasedMin;

  // reset this range's minimum constraint to allow for "warping" of a
  // new minimum value...
  getBoundedRange_().resetMinConstraint();

  peepBasedMin.value = (peepValue + SettingConstants::MIN_NON_MAND_PRESS_BUFF);

  // warp the minimum calculated value to an upper "click" boundary...
  getBoundedRange_().warpValue(peepBasedMin, BoundedRange::WARP_UP);

  // start off with the peep-based minimum value and id...
  minConstraintInfo.id    = peepBasedMinId;
  minConstraintInfo.value = peepBasedMin.value;


  //-------------------------------------------------------------------
  // calculate minimum constraint based on pressure support and the
  // PEEP value...
  //-------------------------------------------------------------------

  if (pPressSupport->getApplicability(Notification::ADJUSTED) !=
						  Applicability::INAPPLICABLE)
  {   // $[TI3] -- pressure support is applicable...
    const Real32  PRESS_SUPP_VALUE =
    			BoundedValue(pPressSupport->getAdjustedValue()).value;
 
    BoundedValue  psuppBasedMin;

    psuppBasedMin.value = (peepValue + PRESS_SUPP_VALUE +
				     SettingConstants::MIN_MAND_PRESS_BUFF);

    if (psuppBasedMin.value > minConstraintInfo.value)
    {   // $[TI3.1]
      // warp the minimum calculated value to an upper click" boundary...
      getBoundedRange_().warpValue(psuppBasedMin, BoundedRange::WARP_UP);

      // this PEEP-based minimum is more restrictive, therefore save its
      // value and id as the minimum bound value and id...
      minConstraintInfo.id    = psuppBasedMinId;
      minConstraintInfo.value = psuppBasedMin.value;
    }   // $[TI3.2] -- PSV-PEEP-based minimum is LESS restrictive...
  }   // $[TI4] -- pressure support is NOT applicable...


  //-------------------------------------------------------------------
  // calculate minimum constraint based on inspiratory pressure and the
  // PEEP value...
  //-------------------------------------------------------------------

  if (pInspPress->getApplicability(Notification::ADJUSTED) !=
						  Applicability::INAPPLICABLE)
  {   // $[TI5] -- inspiratory pressure is applicable...
    // inspiratory pressure is NEVER applicable when PEEP low is applicable...
    SAFE_CLASS_ASSERTION(
	  (pPeepLow->getApplicability(Notification::ADJUSTED) == 
						Applicability::INAPPLICABLE));

    const Real32  INSP_PRESS_VALUE =
    			BoundedValue(pInspPress->getAdjustedValue()).value;

    BoundedValue  inspPressBasedMin;

    inspPressBasedMin.value = (peepValue + INSP_PRESS_VALUE +
			       SettingConstants::MIN_MAND_PRESS_BUFF);

    if (inspPressBasedMin.value > minConstraintInfo.value)
    {   // $[TI5.1]
      // warp the calculated minimum value to an upper "click" boundary...
      getBoundedRange_().warpValue(inspPressBasedMin, BoundedRange::WARP_UP);

      // this PEEP-based minimum is more restrictive, therefore save its
      // value and id as the minimum bound value and id...
      minConstraintInfo.id    = HIGH_CIRC_MIN_BASED_INSP_PRESS_PEEP_ID;
      minConstraintInfo.value = inspPressBasedMin.value;
    }   // $[TI5.2]
  }   // $[TI6] -- inspiratory pressure is NOT applicable...


  //-------------------------------------------------------------------
  // calculate minimum constraint based on PEEP high...
  //-------------------------------------------------------------------

  if (pPeepHigh->getApplicability(Notification::ADJUSTED) !=
						  Applicability::INAPPLICABLE)
  {   // $[TI7] -- PEEP high is applicable...
    const Real32  PEEP_HIGH_VALUE =
    			BoundedValue(pPeepHigh->getAdjustedValue()).value;

    BoundedValue  peepHighBasedMin;

    peepHighBasedMin.value = (PEEP_HIGH_VALUE +
			      SettingConstants::MIN_MAND_PRESS_BUFF);

    if (peepHighBasedMin.value > minConstraintInfo.value)
    {   // $[TI7.1]
      // warp the calculated minimum value to an upper "click" boundary...
      getBoundedRange_().warpValue(peepHighBasedMin, BoundedRange::WARP_UP);

      // this PEEP-based minimum is more restrictive, therefore save its
      // value and id as the minimum bound value and id...
      minConstraintInfo.id    = HIGH_CIRC_MIN_BASED_PEEPH_ID;
      minConstraintInfo.value = peepHighBasedMin.value;
    }   // $[TI7.2]
  }   // $[TI8] -- PEEP high is NOT applicable...

  //-------------------------------------------------------------------
  // calculate minimum constraint based on Low Circ Pressure Setting
  //-------------------------------------------------------------------

  if (pLowCirc->getApplicability(Notification::ADJUSTED) !=
						  Applicability::INAPPLICABLE)
  {   // $[TI9.1] -- Low Circ is applicable...
    BoundedValue  lowCircBasedMin;

    // get the current value of low circ alarm setting; this setting
    // must always be higher that the current low circ setting...
    lowCircBasedMin.value = BoundedValue(pLowCirc->getAdjustedValue()).value;

    // NOTE:  a '>=' comparison is used instead of a '>' comparison because
    //        of the increment done to the value within the if clause, which
    //        equates an '>=' into an '>'...
    if (lowCircBasedMin.value >= minConstraintInfo.value)
    {   // $[TI10.1]
      // reset this range's minimum constraint to allow for "warping" of a
      // new minimum value...
      getBoundedRange_().resetMinConstraint();

      // warp the calculated value to a "click" boundary...
      getBoundedRange_().warpValue(lowCircBasedMin);

      // calculate the value one "click" above this current low minute volume
      // alarm value, by using high exh minute volume's bounded range
      // instance...
      getBoundedRange_().increment(1, lowCircBasedMin, dummyStatus);

      // this Low Circ-based minimum is more restrictive, therefore save its
      // value and id as the minimum bound value and id...
      minConstraintInfo.id    = HIGH_CIRC_MIN_BASED_LOW_ID;
      minConstraintInfo.value = lowCircBasedMin.value;
    }	// $[TI10.2]
  }   // $[TI9.2] -- Low Circ is NOT applicable...

  // this setting has no minimum soft bounds...
  minConstraintInfo.isSoft = FALSE;

  getBoundedRange_().updateMinConstraint(minConstraintInfo);
}
