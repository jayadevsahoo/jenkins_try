#ifndef IpcQueue_HH
#define IpcQueue_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: IpcQueue - A base class for passing Int32's between tasks.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/OS-Foundation/vcssrc/IpcQueue.hhv   25.0.4.0   19 Nov 2013 14:15:16   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003   By: sah   Date: 13-Jan-1999  DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 002    By: Gary Cederquist  Date: 15-APR-1997  DR Number: 1943
//    Project: Sigma (R8027)
//    Description:
//       Added revision headers.
//
//  Revision: 001    By: Greg Hamm        Date: 01-OCT-1996  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//====================================================================

#include "Sigma.hh"
#include "OsFoundation.hh"
#include "IpcTable.hh"
#include "IpcIds.hh"



typedef struct IpcQHandle
{
	void* rxQhandle;
	void* txQhandle;
}IPC_Q_HANDLE;

//@ Usage-Classes
//@ End-Usage

class IpcQueueList
{
public:
	static void     Create_(const IpcTableEntry* ipcEntry);
	static const IPC_Q_HANDLE* GetQueueById(const Int32 id);

    static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL,
			  const char*       pPredicate = NULL)
				{
								FaultHandler::SoftFault(softFaultID, OS_FOUNDATION, OsFoundation::IPC_QUEUE,
								lineNumber, pFileName, pPredicate);
				}
private:
	//@ Data-Member: IpcQueueList_
	// Holds handles of all created IPC queues
	static IPC_Q_HANDLE IpcQueueList_[];
};

class IpcQueue
{
  public:
	static const Uint32 IPC_MSG_SIZE = sizeof(Int32); //pre-defined IPC message size

    virtual ~IpcQueue(void);

    virtual Int32         putMsg(const Int32 msg) = 0;

    Int32                 pendForMsg( Int32 & rMsg,
                                      const Uint32 waitTime=Ipc::NO_TIMEOUT);
    Int32                 acceptMsg(Int32 & rMsg);

    Int32                 numMsgsAvail(void);
    void                  flush(void);

    inline Boolean        isMsgAvail(void);
    inline Boolean        isFull(void);

    inline Int32          getLength(void) const;
    inline Int32          getId(void) const;
    inline const char *   getName(void) const;
    inline Ipc::WaitOrder getWaitOrder(void) const;
	inline const IPC_Q_HANDLE*  getHandle(void) const { return pQueueHandle_; }

	static Ipc::ReturnCode mapOsIpcError(Int32 osError);

    void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL,
			  const char*       pPredicate = NULL);

  protected:
    IpcQueue(const Ipc::IpcType ipcType, const Int32 qId);

#ifdef	SIGMA_DEVELOPMENT
    static void           PrintError_(const Int32 errno);

    inline Boolean        isDebugOn_(void) const;
#endif // SIGMA_DEVELOPMENT

    //@ Data-Member: QID_
    // Holds the Id associated with this queue.
    const Int32 QID_;

    //@ Data-Member: error_
    // Holds the last VRTX error encountered by this object
    Int32 error_;

  private:
    // these methods are purposely declared, but not implemented...
    IpcQueue(void);			// not implemented...
    IpcQueue(const IpcQueue&);		// not implemented...
    void   operator=(const IpcQueue&);	// not implemented...

    //@ Data-Member: qLength_
    // Holds the Length of the associated VRTX queue.
    Int32 qLength_;

    //@ Data-Member: pQueueName_
    // Holds the Queue Name from the IpcTableDef file
    const char* pQueueName_;

    //@ Data-Member: ipcType_
    // Contains the enumerated type IpcType for this IpcQueue
    Ipc::IpcType  ipcType_;

    //@ Data-Member: waitOrder_
    // Contains the enumerated type WaitOrder for this IpcQueue
    Ipc::WaitOrder  waitOrder_;

	//@ Data-Member: qHandle_
	// A handle to the queue, returned by the OS when the queue is created
	const IPC_Q_HANDLE* pQueueHandle_;
};

#ifdef	SIGMA_DEVELOPMENT
// For debug print outs
class Ostream;
Ostream & operator << (Ostream &rOs,const IpcQueue &rQueue);
#endif // SIGMA_DEVELOPMENT

// Inlined methods
#include "IpcQueue.in"


#endif // IpcQueue_HH
