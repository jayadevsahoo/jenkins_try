
#ifndef PausePressTrigger_HH
#define PausePressTrigger_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: PausePressTrigger - Triggers if conditions for pressure 
//	triggering during inspiratory/expiratory pause are met.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/PausePressTrigger.hhv   25.0.4.0   19 Nov 2013 14:00:00   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By: yyy     Date:  29-Apr-1999    DR Number: 5367
//  Project:  ATC
//  Description:
//      Added readyToPrigger_ flag to handle minimum wait before pause
//		can be triggered.
//      Initial version for ATC
//      Class was renamed.  Use same pause trigger for both inspiration and
//		expiration pause handles.
//
//====================================================================

#  include "Sigma.hh"
#  include "Breath_Delivery.hh"

//@ Usage-Classes
#  include "BreathTrigger.hh"

//@ End-Usage

class PausePressTrigger : public BreathTrigger{
  public:
    PausePressTrigger(const Trigger::TriggerId triggerid);
    virtual ~PausePressTrigger(void);
    
    virtual void enable();

   static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL, 
			  const char*       pPredicate = NULL);

  protected:
    virtual Boolean triggerCondition_(void);

  private:
    PausePressTrigger(const PausePressTrigger&);		// not implemented...
    void   operator=(const PausePressTrigger&);	// not implemented...
    PausePressTrigger(void);		// not implemented...

    //@ Data member: readyToTrigger_
    // Patient triggers are not active until PATIENT_MAX_DELAY_MS ms after being enabled. 
    Boolean readyToTrigger_;

};


#endif // PausePressTrigger_HH 
