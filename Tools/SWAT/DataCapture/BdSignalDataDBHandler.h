//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file BdSignalDataDBHandler.h
//----------------------------------------------------------------------------


#ifndef BDSIGNALVIKING_H
#define BDSIGNALVIKING_H

#include "IEventLogger.h"

/// BdSignalDataDBHandler is a class contains the callbacks related to BdSignal data.
class BdSignalDataDBHandler
{
    public:

    /// @brief register_types registers all the BdSignalViking callbacks with
    ///        an instance of a IEventLogger.
    /// 
    /// @param pIEventLogger Pointer to an instance of IEventLogger.
    /// @param user  User data.
    /// 
    /// @return true if success.
    static bool register_types(IEventLogger *pIEventLogger, void *user);

    private:


    /// @brief BdSignalViking not used
    BdSignalDataDBHandler();


    /// @brief ~BdSignalViking not used
    ~BdSignalDataDBHandler();
};

#endif
