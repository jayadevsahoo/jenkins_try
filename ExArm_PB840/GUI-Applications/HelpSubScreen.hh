#ifndef HelpSubScreen_HH
#define HelpSubScreen_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: HelpSubScreen - A subscreen for displaying help information,
// activated by pressing the ? offscreen key.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/HelpSubScreen.hhv   25.0.4.0   19 Nov 2013 14:07:56   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005   By: gdc    Date:  29-Jun-1999    DCS Number: 5327
//  Project: 840 Neonatal
//  Description:
//	Optimized graphics library. Implemented Direct Draw mode for drawing
//  outside of BaseContainer refresh cycle.
//
//  Revision: 004  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 003  By:  gdc	   Date:  15-JUN-1998  DR Number: 5102 
//       Project:  ColorScreens (R8027)
//       Description:
//		 Updated breath timing diagram to new format.
//
//  Revision: 002  By:  clw    Date:  23-OCT-97    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Rework resulting from peer review.  Added 2 new constants
//             LOWER_SCREEN_DIAGRAM_LABEL_X_ and UPPER_SCREEN_DIAGRAM_LABEL_X_.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "Sigma.hh"

//@ Usage-Classes
#include "Bitmap.hh"
#include "Box.hh"
#include "HelpStrs.hh"
#include "Line.hh"
#include "LogTarget.hh"
class ScrollableLog;
#include "Scrollbar.hh"
#include "ScrollbarTarget.hh"
#include "SubScreen.hh"
#include "SubScreenTitleArea.hh"
#include "TextButton.hh"
#include "TextField.hh"
#include "NumericField.hh"

//@ End-Usage

class HelpSubScreen : public SubScreen, public LogTarget
{
public:
	HelpSubScreen(SubScreenArea *pSubScreenArea, HelpMenu* pMainMenu);
	~HelpSubScreen(void);

	virtual void activate(void);
	virtual void deactivate(void);

	// ButtonTarget virtual method
	virtual void buttonDownHappened(Button *pButton, Boolean byOperatorAction);

	// Inherited from LogTarget
	virtual void getLogEntryColumn(Uint16 entryNumber, Uint16 columnNumber,
						Boolean &rIsCheapText, StringId &rString1,
						StringId &rString2, StringId &rString3,
						StringId &rString1Message);

	virtual void currentLogEntryChangeHappened(
						Uint16 changedEntry, Boolean isSelected);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:

private:
	enum ScreenAreaId
	{
		MAIN_SETTING_AREA = 0,
		LOWER_SUB_SCREEN_AREA = 1,
		LOWER_SCREEN_SELECT_AREA = 2,
		MESSAGE_AREA = 3,
		PROMPT_AREA = 4,
		VITAL_PT_DATA_AREA = 5,
		ALARM_AND_STATUS_AREA = 6,
		UPPER_SUB_SCREEN_AREA = 7,
		UPPER_SCREEN_SELECT_AREA = 8,
		MAX_UPPER_LOWER_DISPLAY_AREAS = 9
	};
	enum DiagramLabelId
	{
		UPPER_SCREEN_LABEL,
		LOWER_SCREEN_LABEL,
		MAX_DIAGRAM_LABELS
	};

	enum
	{
		//@ Constant: MAX_NESTED_MENUS_
		// The maximum nesting level that the help menu system can handle.
		MAX_NESTED_MENUS_ = 10
	};

	// these methods are purposely declared, but not implemented...
	HelpSubScreen(void);					// not implemented...
	HelpSubScreen(const HelpSubScreen&);	// not implemented...
	void operator=(const HelpSubScreen&);	// not implemented...

	void setupCurrentMenu_(void);
	void setHelpTitle_(const wchar_t *P_HELP_TITLE);  
	void displayDiagram_(int diagramType);
	void setBoxAttributes_(ScreenAreaId areaId, Uint16 xVal, Uint16 yVal,
			Uint16 width, Uint16 height, StringId boxTitle, 
			Colors::ColorS fillColor, Colors::ColorS textColor);
	void hideDiagrams_();
	Boolean isDiagramVisible_();
	void setBreathTimingDiagram_();

	//@ Data-Member: pMainMenu_
	// A pointer to the main menu displayed when the Help subscreen is first
	// displayed.  This main menu internally can point to submenus and submenus
	// can point to other submenus, etc. etc.
	HelpMenu* pMainMenu_;

	//@ Data-Member: nestedMenus_
	// As submenus of the menus are chosen we need to keep account of what the
	// previously displayed menu was (if the user presses the Go Back button).
	// We store this nest of menus in this array.
	HelpMenu* nestedMenus_[MAX_NESTED_MENUS_];

	//@ Data-Member: firstMenuEntries_
	// For each nested menu in the nestedMenus_[] array we must keep account of
	// what the first displayed entry on the nested menu was so that we can
	// restore the display exact when the user goes back.
	Uint16 firstMenuEntries_[MAX_NESTED_MENUS_];

	//@ Data-Member: nestingLevel_
	// A number which keeps account of how deep in the menu tree we can go,
	// limited by MAX_NESTED_MENUS.
	Uint16 nestingLevel_;

	//@ Data-Member: currDiagram_
	// A number which keeps account of the current diagram.
	Uint16 currDiagram_;

	//@ Data-Member: changedEntry_
	// A number which keeps account of the current changed entry in the current menu.
	Uint16 changedEntry_;

	//@ Data-Member: titleArea_
	// The sub-screen's title at the top left of the screen.
	SubScreenTitleArea titleArea_;

	//@ Data-Member: pMenu_
	// A scrollable log which is used for displaying all menus/submenus.
	ScrollableLog *pMenu_;

	//@ Data-Member: numTopics_
	// The number of topics (menu entries) displayed on the current menu.
	Uint16 numTopics_;

	//@ Data-Member: goBackButton_
	// The button which allows the user to go back to a previous menu.
	TextButton goBackButton_;

	//@ Data-Member: helpTitle_
	// A title displayed on top of menus and help text areas which identifies
	// what is being read.
	TextField helpTitle_;

	//@ Data-Member: titleUnderline_
	// A line which underlines helpTitle_.
	Line titleUnderline_;

	//@ Data-Member: textArea_
	// The text area which displays help text when the user chooses a menu
	// option that actually displays help text, not a submenu.
	TextArea textArea_;

	//@ Data-Member: textAreaSeparator_
	// The text area is separated by a grey line from the area where the
	// help title is displayed.  This box is used to display that grey line.
	Box textAreaSeparator_;

	//@ Data-Member: nextPageButton_
	// The button which allows the user to view the next page of help text
	TextButton nextPageButton_;

	//@ Data-Member: prevPageButton_
	// The button which allows the user to view the previous page of help text
	TextButton prevPageButton_;

	//@ Data-Member: diagramButton_
	// The button, when pressed, displays any diagram on the current topic. 
	TextButton diagramButton_;

	//@ Data-Member: diagramBoxes_ 
	// An array of boxes making up the Help diagram 
	Box diagramBoxes_[MAX_UPPER_LOWER_DISPLAY_AREAS];

	//@ Data-Member: diagramText_ 
	// An array of text fields shown inside the boxes
	TextField diagramText_[MAX_UPPER_LOWER_DISPLAY_AREAS];	

	//@ Data-Member: inspiratoryTimeBox_
	// The colored box in which the inspiratory time value is normally displayed
	// and whose size width gives an indication of the relative size of the
	// inspiratory time.
	Box inspiratoryTimeBox_;

	//@ Data-Member: expiratoryTimeBox_
	// The colored box in which the expiratory time value is displayed
	// and whose size width gives an indication of the relative size of the
	// expiratory time.
	Box expiratoryTimeBox_;

	//@ Data-Member: ieRatioBox_
	// The colored box with a white border in which the I:E ratio value is
	// displayed and which is centered around the border between the
	// inspiratory and expiratory time boxes.
	Box ieRatioBox_;

	//@ Data-Member: ieRatioPointer_
	// The line which points from the I:E ratio box down to the X axis of the
	// bar.
	Box ieRatioPointer_;

	//@ Data-Member: tTotPointer_
	// The small tick mark which points from the T-tot value to the X axis.
	Box tTotPointer_;

	//@ Data-Member: inspiratoryTime_
	// The numeric field which displays the inspiratory time value, normally
	// displayed in the inspiratory time box.
	NumericField inspiratoryTime_;

	//@ Data-Member: expiratoryTime_
	// The numeric field which displays the expiratory time value,  displayed
	// in the expiratory time box.
	NumericField expiratoryTime_;

	//@ Data-Member: ieRatioText_
	// The text field which displays the I:E ratio value,  displayed
	// in the I:E Ratio box.
	TextField ieRatioText_;

	//@ Data-Member: unitsText_
	// The text field at the end of the timing diagram which displays
	// the units (seconds) for the timing graph.
	TextField unitsText_;

	//@ Data-Member: tTot_
	// The numeric field which displays T-tot (60 / Respiratory Rate).
	NumericField tTot_;

	//@ Data-Member: zero_
	// The numeric field which displays a zero for denoting the start of the
	// timing bar.
	NumericField zero_;

	//@ Data-Member: xAxis_
	// The horizontal line which forms the "X" axis of the bar.
	Box xAxis_;

    //@ Data-Member: inspiratoryXAxis_
    // The horizontal line which forms the inspiratory part of the "X" axis
    // bar.
    Box inspiratoryTimeXAxis_;
 
    //@ Data-Member: expiratoryXAxis_
    // The horizontal line which forms the expiratory part of the "X" axis
    // bar.
    Box expiratoryTimeXAxis_;
 
    //@ Data-Member: tTotOutsideBox_
    // The box outside the breath bar containing the T-tot value.
    Box tTotOutsideBox_;
 
	//@ Data-Member: leftMarker_
	// The black upright line which marks the beginning of the timing bar.
	Box leftMarker_;

	//@ Data-Member: rightMarker_
	// The blank line which marks the end of the timing bar.
	Box rightMarker_;

	//@ Data-Member: upperLabel_
	// The label for the upper screen.
	TextField upperLabel_;

	//@ Data-Member: lowerLabel_
	// The label for the upper screen.
	TextField lowerLabel_;

	//@ Data-Member: goBackBitmap_
	// The bitmap for the go back button
	Bitmap goBackBitmap_;

	//@ Data-Member: nextPageBitmap_
	// The bitmap for the next page button
	Bitmap nextPageBitmap_;

	//@ Data-Member: prevPageBitmap_
	// The bitmap for the previous page button
	Bitmap prevPageBitmap_;

};

#endif // HelpSubScreen_HH 
