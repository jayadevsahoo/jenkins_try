#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: TrendDbSampleNonAvg
//---------------------------------------------------------------------
//@ Interface-Description
//  Handles data which is last sampled over the timescale period.
//  The constructor takes the patient or settingId of the source data
//  and the trendSelectValueId of the trend data.  The 1Hr data is set via
//  the baseclass setValue(), values are reterived for the given timescales
//  via the baseclass getValue().
// 
//---------------------------------------------------------------------
//@ Rationale
//  Encaplsulates the handling of last sample trend data
// 
//---------------------------------------------------------------------
//@ Implementation-Description
//  The compressData() method just moves the last sample in the time period
//  to the value in the baseclass.
//  
//---------------------------------------------------------------------
//@ Fault-Handling
//  SoftFault is used
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Trend-Database/vcssrc/TrendDbSampleNonAvg.ccv   25.0.4.0   19 Nov 2013 14:34:40   pvcs  $
//
//@ Modification-Log
// 
//  Revision: 001  By:  ksg    Date:  17-June-2007    SCR Number: 6237
//       Project:  Trend
//       Description: Initial version
//
//=====================================================================

#include "Trend_Database.hh"
#include "TrendDbSampleNonAvg.hh"


//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TrendDbSampleNonAvg [constructor]
//
//@ Interface-Description
//  Constructs with the PatientDataId for the source data and the 
//  TrendSelectValueId for the trended value
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
TrendDbSampleNonAvg::TrendDbSampleNonAvg( PatientDataId::PatientItemId id,
										  TrendSelectValue::TrendSelectValueId trendId )
	:TrendDbSampleData( id, trendId )
{
	// do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TrendDbSampleNonAvg [constructor]
//
//@ Interface-Description
//  Construct with the SettingIdType for the source data and the
//  TrendSelectValueId for the trended value
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
TrendDbSampleNonAvg::TrendDbSampleNonAvg( SettingId::SettingIdType id,
										  TrendSelectValue::TrendSelectValueId trendId )
	:TrendDbSampleData( id, trendId )
{
	// do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TrendDbSampleAvg [destructor]
//
//@ Interface-Description
//  Does no action  
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
TrendDbSampleNonAvg::~TrendDbSampleNonAvg() 
{
	// do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: compressData
//
//@ Interface-Description
//  Compress the trend value for the given timescale
//---------------------------------------------------------------------
//@ Implementation-Description
//  Copies the last 1_hr sampled data to the valueArray for the provided
//  timescale.
// 
//  Satisfies requirements:
//  [[TR01114] [PRD 1512] For the 2, 4, 8, 12, 24, 48, and 72 hour
//  timescales, the system shall determine...
//---------------------------------------------------------------------
//@ PreCondition
//	Ensures that the provided timescale is valid
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDbSampleNonAvg::compressData( TrendTimeScaleValue::TrendTimeScaleValueId timescale )
{
	AUX_CLASS_PRE_CONDITION( (timescale < TrendTimeScaleValue::TOTAL_TREND_TIME_SCALE_VALUES), timescale );

	// For all but the ONE_HR timescale, copy the value from the
	// previous timescale
	if (timescale != TrendTimeScaleValue::TREND_TIME_SCALE_1HR)
	{
		valueArray_[timescale] = valueArray_[timescale-1];
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: clear_
//
//@ Interface-Description
//  None
//---------------------------------------------------------------------
//@ Implementation-Description
//  None - class has no derived data
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDbSampleNonAvg::clear_( void )
{
	// do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void TrendDbSampleNonAvg::SoftFault(const SoftFaultID  softFaultID,
									const Uint32       lineNumber,
									const char*        pFileName,
									const char*        pPredicate)  
{
	FaultHandler::SoftFault(softFaultID, TREND_DATABASE, TREND_DB_SAMPLE_NON_AVG,
							lineNumber, pFileName, pPredicate);
}





