#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: HighPressCompExpTrigger - Triggers if the pressure exceeds the high  
// 		pressure compensation limit for a duration greater than the IBW based
//		time limit.
//---------------------------------------------------------------------
//@ Interface-Description
//     This class is derived from BreathTrigger and is enabled by TcvPhase.
//     This class keeps track of the total time that the pressure exceeds the
//		high compensation limit.  When the time limit has reached, the trigger will
//     	"fire".  This trigger is kept on a list contained in the 
//     	BreathTriggerMediator.
//---------------------------------------------------------------------
//@ Rationale
//     It is necessary to end inspiration, and proceed directly to exhalation
//     when the total time TC high pressure compensation has exceeded.
//     This trigger provides a means of monitoring for that condition.
//---------------------------------------------------------------------
//@ Implementation-Description
//     This class has a member, count_, which increments when the pressure exceeds high
//     pressure compensation limit.  count_ is compared to member
//     maxCount_ which limits the amount of time the compliance can exceed 
//     limit.  maxCount_ is determined when trigger is enabled according to
//     IBW.
//---------------------------------------------------------------------
//@ Fault-Handling
//	n/a.
//---------------------------------------------------------------------
//@ Restrictions
//     Only one instance of this class may be created.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header::   /840/Baseline/Breath-Delivery/vcssrc/HighPressCompExpTrigger.ccv   10.7   08/17/07 09:37:24   pvcs  
//
//@ Modification-Log
//
//  Revision: 003   By: gdc    Date: 26-Jan-2009    SCR Number: 6407
//  Project:  840S
//  Description:
//      Modified to bring source into strict ARM compliance.
//
//  Revision: 002  By:  syw    Date:  23-Jun-1999    DR Number: DCS 5447
//       Project:  ATC
//       Description:
//			Add DCS 5322 to header
//
//  Revision: 001  By:  syw    Date:  14-Jan-1999    DR Number: DCS 5322
//       Project:  ATC
//       Description:
//             ATC initial version.
//
//=====================================================================

#include "HighPressCompExpTrigger.hh"

//@ Usage-Classes

#include "PhasedInContextHandle.hh"

//@ End-Usage

//@ Code...

static const Int32 PED_IBW_LIMIT = 25;

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: HighPressCompExpTrigger()  
//
//@ Interface-Description
//		Default Constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  	Calls base class constructor.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

HighPressCompExpTrigger::HighPressCompExpTrigger(void)
	:BreathTrigger(Trigger::HIGH_PRESS_COMP_EXP)

{
	CALL_TRACE("HighPressCompExpTrigger::HighPressCompExpTrigger(void)") ;
	
	// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~HighPressCompExpTrigger()  
//
//@ Interface-Description
//		Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  	none
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

HighPressCompExpTrigger::~HighPressCompExpTrigger(void)
{
	CALL_TRACE("HighPressCompExpTrigger::~HighPressCompExpTrigger(void)") ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: enable  
//
//@ Interface-Description
//      This method takes no argument and has no return value. This method
//      enables the trigger and is responsible for the trigger's
//      initialization.
//---------------------------------------------------------------------
//@ Implementation-Description
//      The state data member isEnabled_ is set to true, activating this
//      trigger.  The data members maxCount_ are initialized based on IBW.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
HighPressCompExpTrigger::enable(void)
{
	CALL_TRACE("HighPressCompExpTrigger::enable(void)") ;
		
	isEnabled_ = TRUE ;

	count_ = 0 ;

	if (PhasedInContextHandle::GetBoundedValue( SettingId::IBW).value < PED_IBW_LIMIT)
	{// $[TI1]
		maxCount_ = 60 ;	// 300 msec
	}
	else
	{// $[TI2]
		maxCount_ = 100 ;	// 500 msec
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setHighPressCompLimitMet  
//
//@ Interface-Description
//     This method takes a constant Boolean as an argument and returns
//     no value.  It is called by TcvPhase every new cycle.
//---------------------------------------------------------------------
//@ Implementation-Description
//     If met is TRUE, count_ is incremented, else decremented.
// $[TC04041]
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
HighPressCompExpTrigger::setHighPressCompLimitMet( const Boolean met)
{
	CALL_TRACE("HighPressCompExpTrigger::enable(void)") ;

	if (met)
	{// $[TI1]
		count_++ ;
	}
	else
	{// $[TI2]
		count_-- ;

		if (count_ < 0)
		{// $[TI2.1]
			count_ = 0 ;
		}// $[TI2.2]
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

#if defined(SIGMA_DEVELOPMENT)

void
HighPressCompExpTrigger::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, HIGHPRESSCOMPEXPTRIGGER,
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)

//=====================================================================
//
//  Protected Methods...
//
//=====================================================================
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: triggerCondition_
//
//@ Interface-Description
//    This method takes no parameters.  If the trigger condition has 
//    occured, the method returns true, otherwise, the method returns 
//    false.
//---------------------------------------------------------------------
//@ Implementation-Description
//    If count_ is greater than maxCount_ limit, the trigger condition 
//    returns TRUE, else returns FALSE.
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
Boolean
HighPressCompExpTrigger::triggerCondition_(void)
{
	CALL_TRACE("HighPressCompExpTrigger::triggerCondition_(void)") ;
	
	Boolean rtnValue = FALSE;

	if (count_ > maxCount_)
	{// $[TI1]
		rtnValue = TRUE ;
	}// $[TI2]

  	return( rtnValue) ;
}

//=====================================================================
//
//  Private Methods...
//
//=====================================================================








