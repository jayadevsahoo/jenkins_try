
#ifndef SstResultTable_HH
#define SstResultTable_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sotred in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class:  SstResultTable - SST Result Table.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Persistent-Objects/vcssrc/SstResultTable.hhv   25.0.4.0   19 Nov 2013 14:18:54   pvcs  $
//
//@ Modification-Table
//
//  Revision: 005  By:  sah    Date:  07-Jan-1999    DCS Number: 5321
//  Project:  Sigma (R8027)
//  Description:
//      Obsoleted this class's '.in' file because there are no longer
//	any inlined methods.
//
//  Revision: 004  By:  sah    Date:  05-May-1998    DCS Number: 5041
//  Project:  Sigma (R8027)
//  Description:
//      Changed 'SoftFault()' and 'verifySelf()' to non-inlined methods.
//
//  Revision: 003  By:  sah    Date:  18-Jul-1997    DCS Number: 2246
//  Project:  Sigma (R8027)
//  Description:
//	Removed 'overrideFailures()', which is now implemented in base
//	class.
//
//  Revision: 002  By:  sah    Date:  15-Jul-1997    DCS Number: 2175
//  Project:  Sigma (R8027)
//  Description:
//	Added one static method ('IntermediateResultLogged()') for handling
//	the forwarding of this request to the one instance of this class.
//
//  Revision: 001  By:  sah    Date:  15-Mar-1997    DCS Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//          Initial version (Integration baseline).
//
//====================================================================

//@ Usage-Classes
#include "TestResultTable.hh"
#include "ResultTableEntry.hh"

class  AbstractArray(ResultTableEntry);
//@ End-Usage


class SstResultTable : public TestResultTable
{
  public:
    SstResultTable(void);
    ~SstResultTable(void);

    NovRamStatus::InitializationId  initialize(void);
    NovRamStatus::VerificationId    verifySelf(void) const;

    void  repeatingTest(const Uint testNumber);

    void  getCurrEntries(AbstractArray(ResultTableEntry)& rArrayEntry) const;

    static void  IntermediateResultLogged(const DiagnosticCode& diagCode);

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL,
			   const char*       pPredicate = NULL);

  private:
    SstResultTable(const SstResultTable&);	// not implemented...
    void  operator=(const SstResultTable&);	// not implemented...

    //@ Data-Member:  arrEntries_
    // An array to contain each of this log's entries.
    ResultTableEntry  arrEntries_[::MAX_SST_RESULTS];
};


#endif // SstResultTable_HH 
