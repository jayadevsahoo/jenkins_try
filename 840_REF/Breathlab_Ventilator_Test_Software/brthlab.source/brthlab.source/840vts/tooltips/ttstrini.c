/*****************************************************************************/ 
/*   Module Name:    ttstringinit.c                                                
/*   Purpose:       Test Executive Source Code                                  
/*                                                                          	   
/*                                                                              
/*   Requirements:   This file contains the functions concerned     			   
/*  with writing test reports                                                   
/*
/*   Modification History:
/*   
/*   Description of Change			        By			      Date 
/*   ---------------------					-----   		  ------
/*   Removed Ground DC and AC volts         Rhomere Jimenez   25 Oct, 2005
/*   
/*	 Critical Observation during Phase1 
/* 	 is incorportated.						Kathirvel(CGS)    27 Sep,2000
/*
/*   Modified TTStringInsert to             Dave Richardson   23 Nov, 2002
/*   not allocate and abandon memory
/*
/*   $Log:   //taplea-ww01/swqa/Data/arc/840_CPVT_VTS/Program/tooltips/ttstrini.c-arc  $
 * 
 *    Rev 1.3   Aug 11 2006 12:47:50   Jimenez
 * Updated for Rev F.
 * 
 *    Rev 1.2   04 Dec 2002 15:37:12   Buchman
 * 840 VTS build done
 * 
 *    Rev 1.1   23 Nov 2002 12:27:10   Richardson
 * Updated per SCR # 31

/*****************************************************************************/ 
																				   
/*****************************************************************************  
THIS IS A PROPRIETARY WORK TO WHICH PURITAN-BENNETT CORPORATION CLAIMS  
EXCLUSIVE RIGHT.  NO PART OF THIS WORK MAY BE USED, DISCLOSED, REPRODUCED,	   
STORED IN AN INFORMATION RETIEVAL SYSTEM, OR TRANSMITTED, IN ANY FORM OR BY ANY 
MEANS ELECTRONIC, MECHANICAL, PHOTOCOPYING, RECORDING OR OTHERWISE WITHOUT THE  
PRIOR WRITTEN PERMISSION OF PURITAN-BENNETT CORPORATION.				   
 																			   
Copyright (c) 1999-2002, Puritan-Bennett						   
All rights reserved.															   
*******************************************************************************/
/* This file is created by ttgen.pl*/
/* Do not modify by hand*/

/***********************************************/
/****************** INCLUDES *******************/
/***********************************************/

#include <toolbox.h>
#include "ldb.h"
#include "mainpnls.h"
#include "tt.h"

/***********************************************/
/************ FUNCTION DECLARATION  ************/
/***********************************************/

/* TTstringInsert(void) populates the tiplist list with tooltip entries */
void TTStringInsert(void)
{
	TipListNode *NewNodePtr;

//	ListDispose(TipList);
	if(!TipList){
		TipList = ListCreate(sizeof(TipListNode));
	}
	else{
		ListClear(TipList);
	}

	NewNodePtr = (TipListNode *)calloc(1, sizeof(TipListNode)); // Modified the statement due to Critical Observation.

	//22:3:TESTPANEL_PAUSE_BTN:Ldb_tt14
	NewNodePtr->PanelID = TESTPANEL; // 22;
	NewNodePtr->CtrlID  = TESTPANEL_PAUSE_BTN;  // 3;
//	NewNodePtr->CtrlStr = StrDup(LDBf(Ldb_tt14));
	NewNodePtr->CtrlStr = LDBf(Ldb_tt14);
	ListInsertItem(TipList, NewNodePtr, END_OF_LIST);

	//22:6:TESTPANEL_LOOPTEST_DEV:Ldb_tt13
	NewNodePtr->PanelID = TESTPANEL; // 22;
	NewNodePtr->CtrlID  = TESTPANEL_LOOPTEST_DEV; // 6;
//	NewNodePtr->CtrlStr = StrDup(LDBf(Ldb_tt13));
	NewNodePtr->CtrlStr = LDBf(Ldb_tt13);
	ListInsertItem(TipList, NewNodePtr, END_OF_LIST);

	//22:5:TESTPANEL_RUNTEST_DEV:Ldb_tt12
	NewNodePtr->PanelID = TESTPANEL; // 22;
	NewNodePtr->CtrlID  = TESTPANEL_RUNTEST_DEV; // 5;
//	NewNodePtr->CtrlStr = StrDup(LDBf(Ldb_tt12));
	NewNodePtr->CtrlStr = LDBf(Ldb_tt12);
	ListInsertItem(TipList, NewNodePtr, END_OF_LIST);

	//22:2:TESTPANEL_TESTUUT_DEV:Ldb_tt11
	NewNodePtr->PanelID = TESTPANEL; // 22;
	NewNodePtr->CtrlID  = TESTPANEL_TESTUUT_DEV; // 2;
//	NewNodePtr->CtrlStr = StrDup(LDBf(Ldb_tt11));
	NewNodePtr->CtrlStr = LDBf(Ldb_tt11);
	ListInsertItem(TipList, NewNodePtr, END_OF_LIST);

	//11:3:LOGONPANEL_EXIT_BUTTON:Ldb_tt10
	NewNodePtr->PanelID = LOGONPANEL; // 11;
	NewNodePtr->CtrlID  = LOGONPANEL_EXIT_BUTTON; // 3;
//	NewNodePtr->CtrlStr = StrDup(LDBf(Ldb_tt10));
	NewNodePtr->CtrlStr = LDBf(Ldb_tt10);
	ListInsertItem(TipList, NewNodePtr, END_OF_LIST);

	//11:2:LOGONPANEL_OPERATORS:Ldb_tt9
	NewNodePtr->PanelID = LOGONPANEL; // 11;
	NewNodePtr->CtrlID  = LOGONPANEL_OPERATORS; // 2;
//	NewNodePtr->CtrlStr = StrDup(LDBf(Ldb_tt9));
	NewNodePtr->CtrlStr = LDBf(Ldb_tt9);
	ListInsertItem(TipList, NewNodePtr, END_OF_LIST);

	//19:3:SYSPANEL_CMD_ADMIN:Ldb_tt8
	NewNodePtr->PanelID = SYSPANEL; // 19;
	NewNodePtr->CtrlID  = SYSPANEL_CMD_ADMIN; // 3;
//	NewNodePtr->CtrlStr = StrDup(LDBf(Ldb_tt8));
	NewNodePtr->CtrlStr = LDBf(Ldb_tt8);
	ListInsertItem(TipList, NewNodePtr, END_OF_LIST);

	//22:4:TESTPANEL_STOP_BTN:Ldb_tt7
	NewNodePtr->PanelID = TESTPANEL; // 22;
	NewNodePtr->CtrlID  = TESTPANEL_STOP_BTN; // 4;
//	NewNodePtr->CtrlStr = StrDup(LDBf(Ldb_tt7));
	NewNodePtr->CtrlStr = LDBf(Ldb_tt7);
	ListInsertItem(TipList, NewNodePtr, END_OF_LIST);

	//19:8:SYSPANEL_CMD_PTS:Ldb_tt6
	NewNodePtr->PanelID = SYSPANEL; // 19;
	NewNodePtr->CtrlID  = SYSPANEL_CMD_PTS; // 8;
//	NewNodePtr->CtrlStr = StrDup(LDBf(Ldb_tt6));
	NewNodePtr->CtrlStr = LDBf(Ldb_tt6);
	ListInsertItem(TipList, NewNodePtr, END_OF_LIST);

	//19:2:SYSPANEL_CMD_HELP:Ldb_tt5
	NewNodePtr->PanelID = SYSPANEL; // 19;
	NewNodePtr->CtrlID  = SYSPANEL_CMD_HELP; // 2;
//	NewNodePtr->CtrlStr = StrDup(LDBf(Ldb_tt5));
	NewNodePtr->CtrlStr = LDBf(Ldb_tt5);
	ListInsertItem(TipList, NewNodePtr, END_OF_LIST);

	//19:6:SYSPANEL_CMD_TESTS:Ldb_tt4
	NewNodePtr->PanelID = SYSPANEL; // 19;
	NewNodePtr->CtrlID  = SYSPANEL_CMD_TESTS; // 6;
//	NewNodePtr->CtrlStr = StrDup(LDBf(Ldb_tt4));
	NewNodePtr->CtrlStr = LDBf(Ldb_tt4);
	ListInsertItem(TipList, NewNodePtr, END_OF_LIST);

	//19:5:SYSPANEL_CMD_EXIT:Ldb_tt3
	NewNodePtr->PanelID = SYSPANEL; // 19;
	NewNodePtr->CtrlID  = SYSPANEL_CMD_EXIT; // 5;
//	NewNodePtr->CtrlStr = StrDup(LDBf(Ldb_tt3));
	NewNodePtr->CtrlStr = LDBf(Ldb_tt3);
	ListInsertItem(TipList, NewNodePtr, END_OF_LIST);

	//19:4:SYSPANEL_CMD_REPORT:Ldb_tt2
	NewNodePtr->PanelID = SYSPANEL; // 19;
	NewNodePtr->CtrlID  = SYSPANEL_CMD_REPORT; // 4;
//	NewNodePtr->CtrlStr = StrDup(LDBf(Ldb_tt2));
	NewNodePtr->CtrlStr = LDBf(Ldb_tt2);
	ListInsertItem(TipList, NewNodePtr, END_OF_LIST);

	//19:7:SYSPANEL_CMD_840:Ldb_tt1
	NewNodePtr->PanelID = SYSPANEL; // 19;
	NewNodePtr->CtrlID  = SYSPANEL_CMD_840; // 7;
//	NewNodePtr->CtrlStr = StrDup(LDBf(Ldb_tt1));
	NewNodePtr->CtrlStr = LDBf(Ldb_tt1);
	ListInsertItem(TipList, NewNodePtr, END_OF_LIST);

	//11:6:LOGONPANEL_LOGIN_BUTTON:Ldb_tt15
	NewNodePtr->PanelID = LOGONPANEL; // 11;
	NewNodePtr->CtrlID  = LOGONPANEL_LOGIN_BUTTON; // 6;
//	NewNodePtr->CtrlStr = StrDup(LDBf(Ldb_tt15));
	NewNodePtr->CtrlStr = LDBf(Ldb_tt15);
	ListInsertItem(TipList, NewNodePtr, END_OF_LIST);

	//13:4:MANUALPNL_GROUNDRES:Ldb_mantest_tt1
	NewNodePtr->PanelID = MANUALPNL; // 13;
	NewNodePtr->CtrlID  = MANUALPNL_GROUNDRES; // 4;
//	NewNodePtr->CtrlStr = StrDup(LDBf(Ldb_mantest_tt1));
	NewNodePtr->CtrlStr = LDBf(Ldb_mantest_tt1);
	ListInsertItem(TipList, NewNodePtr, END_OF_LIST);

	//13:6:MANUALPNL_FRWDLEAK:Ldb_mantest_tt2
	NewNodePtr->PanelID = MANUALPNL; // 13;
	NewNodePtr->CtrlID  = MANUALPNL_FRWDLEAK; // 6;
//	NewNodePtr->CtrlStr = StrDup(LDBf(Ldb_mantest_tt2));
	NewNodePtr->CtrlStr = LDBf(Ldb_mantest_tt2);
	ListInsertItem(TipList, NewNodePtr, END_OF_LIST);

	//13:7:MANUALPNL_REVLEAK:Ldb_mantest_tt3
	NewNodePtr->PanelID = MANUALPNL; // 13;
	NewNodePtr->CtrlID  = MANUALPNL_REVLEAK; // 7;
//	NewNodePtr->CtrlStr = StrDup(LDBf(Ldb_mantest_tt3));
	NewNodePtr->CtrlStr = LDBf(Ldb_mantest_tt3);
	ListInsertItem(TipList, NewNodePtr, END_OF_LIST);

	//13:8:MANUALPNL_GRNDISOL:Ldb_mantest_tt4
	NewNodePtr->PanelID = MANUALPNL; // 13;
	NewNodePtr->CtrlID  = MANUALPNL_GRNDISOL; // 8;
//	NewNodePtr->CtrlStr = StrDup(LDBf(Ldb_mantest_tt4));
	NewNodePtr->CtrlStr = LDBf(Ldb_mantest_tt4);
	ListInsertItem(TipList, NewNodePtr, END_OF_LIST);


	//13:11:MANUALPNL_AIRREG:Ldb_mantest_tt7
	NewNodePtr->PanelID = MANUALPNL; // 13;
	NewNodePtr->CtrlID  = MANUALPNL_AIRREG; // 11;
//	NewNodePtr->CtrlStr = StrDup(LDBf(Ldb_mantest_tt7));
	NewNodePtr->CtrlStr = LDBf(Ldb_mantest_tt7);
	ListInsertItem(TipList, NewNodePtr, END_OF_LIST);

	//13:12:MANUALPNL_O2REG:Ldb_mantest_tt8
	NewNodePtr->PanelID = MANUALPNL; // 13;
	NewNodePtr->CtrlID  = MANUALPNL_O2REG; // 12;
//	NewNodePtr->CtrlStr = StrDup(LDBf(Ldb_mantest_tt8));
	NewNodePtr->CtrlStr = LDBf(Ldb_mantest_tt8);
	ListInsertItem(TipList, NewNodePtr, END_OF_LIST);

	//11:5:LOGONPANEL_VENT_CAL_BUTTON:Ldb_tt17
	NewNodePtr->PanelID = LOGONPANEL; // 11;
	NewNodePtr->CtrlID  = LOGONPANEL_VENT_CAL_BUTTON; // 5;
//	NewNodePtr->CtrlStr = StrDup(LDBf(Ldb_tt17));
	NewNodePtr->CtrlStr = LDBf(Ldb_tt17);
	ListInsertItem(TipList, NewNodePtr, END_OF_LIST);

	//11:4:LOGONPANEL_PTS_CAL_BUTTON:Ldb_tt16
	NewNodePtr->PanelID = LOGONPANEL; // 11;
	NewNodePtr->CtrlID  = LOGONPANEL_PTS_CAL_BUTTON; // 4;
//	NewNodePtr->CtrlStr = StrDup(LDBf(Ldb_tt16));
	NewNodePtr->CtrlStr = LDBf(Ldb_tt16);
	ListInsertItem(TipList, NewNodePtr, END_OF_LIST);

	free(NewNodePtr ); // Modified the statement due to Critical Observation.

	return;
}
