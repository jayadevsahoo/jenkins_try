#include "stdafx.h"
//=====================================================================
//   This is a proprietary work to which Puritan-Bennett corporation
//   of California claims exclusive right.  No part of this work may
//  be used, disclosed, reproduced, sorted in an information retrieval
//     system, or transmitted by any means, electronic, mechanical,
//   photocopying, recording, or otherwise without the prior written
//      permission of Puritan-Bennett Corporation of California.
//
//          Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: TaskAgent - Interface Agent to Application Tasks.
//---------------------------------------------------------------------
//@ Interface-Description
//  The TaskAgent class provides the interface to application tasks
//  managed by Sys-Init.  It contains static methods for sending and
//  receiving application task messages including Change-State/
//  Task-Ready message exchanges.  It messages the Task Control finite
//  state machine upon receiving a Task-Ready message.  A TaskAgent
//  object is instantiated for each task managed by Sys-Init.  The
//  TaskAgentManager class contains the TaskAgent objects used by 
//  Task Control.
//---------------------------------------------------------------------
//@ Rationale
//  The TaskAgent class encapsulates the interface and information for
//  a task managed by Sys-Init.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The TaskAgent contains the task name, its VRTX task id, its queue
//  id, and its state.  This information is used to start the task
//  in the create() method.  The send() copies and forwards
//  TaskControlMessages to the task's queue.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Sys-Init/vcssrc/TaskAgent.ccv   25.0.4.0   19 Nov 2013 14:33:56   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By:  gdc    Date:  12-Dec-2008    SCR Number: 6068
//  Project:  840S
//  Description:
//		Removed SUN prototype code.
//
//   Revision 002   By:   sah      Date: 01/13/99    DR Number: 5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to a non-inlined method.
//
//   Revision 001   By:   gdc      Date: 07/06/95    DR Number:   n/a  
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//=====================================================================

//=====================================================================
//
//      Includes...
//
//=====================================================================

#include "TaskAgent.hh"

#ifdef SIGMA_DEVELOPMENT
#  include "Ostream.hh"
#endif

//@ Usage-Classes
#include "ChangeStateMessage.hh"
#include "BdReadyMessage.hh"
#include "GuiReadyMessage.hh"
#include "CommDownMessage.hh"
#include "Task.hh"
#include "TaskControl.hh"
#include "TaskControlAgent.hh"
#include "TaskFlags.hh"
#include "TaskReadyMessage.hh"
//@ End-Usage

//=====================================================================
//
//      Static Data...
//
//=====================================================================

//@ Code...

//=====================================================================
//
//      Public Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TaskAgent
//
//@ Interface-Description
//  TaskAgent constructor.  Initializes internal data to NULL.
//  Provided as the default constructor for the static array of
//  TaskAgents in the TaskAgentManager.  set() initializes the 
//  TaskAgent to valid data from the Task class.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

TaskAgent::TaskAgent() : pTaskInfo_(NULL), sequenceCompleted_(0)
{
    CALL_TRACE("TaskAgent()");
    // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: set
//
//@ Interface-Description
//  Initializes the TaskAgent internal data from the TaskInfo object
//  for the specified task identifier.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses the taskId to retrieve the TaskInfo object that contains 
//  the task's name and queue identifier.
//	The calling task is not the target task (the task to set its TaskAgent info)
//	therefore, we provide the ID to GetTaskInfo to get us that task's info.
//	usingInternalId id defaulted to TRUE to indicate that the task is being
//	referenced by its internal ID which is an index in the task info array not the
//	OS thread ID.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
TaskAgent::set(const Int32 taskIdx)
{
    CALL_TRACE("set(taskIdx)");
	pTaskInfo_   = &TaskInfo::GetTaskInfo(taskIdx);
    // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~TaskAgent
//
//@ Interface-Description
//  Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

TaskAgent::~TaskAgent()
{
    CALL_TRACE("~TaskAgent()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: createTask
//
//@ Interface-Description
//  Creates (starts) a task thread defined for this TaskAgent.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses the TaskInfo::create method to create a task thread.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
TaskAgent::createTask(void)
{
    CALL_TRACE("createTask()");
 

    if (pTaskInfo_->GetState() != TASK_RUNNING) // $[TI1]
    {
        pTaskInfo_->Create();

#  ifdef SIGMA_DEVELOPMENT
    if (Sys_Init::GetDebugLevel(Sys_Init::ID_TASKAGENT) >= 1)
        cout << "CREATED TASK " << pTaskInfo_->getName() << endl;
#  endif
 
    }
    // else $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: send
//
//@ Interface-Description
//  Sends a copy of the specified BdReadyMessage to the task
//  associated with this TaskInfo object.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses the BdReadyMessage copy constructor to create a new message
//  Uses TaskControl::SendEvent to queue the message to the task's
//  queue.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
TaskAgent::send(const BdReadyMessage& rMessage) const
{
    CALL_TRACE("send(BdReadyMessage&)");

    //  check if task is flagged to receive task control messages
    if (   pTaskInfo_->IsFlagOn(F_TCMSG) )  // $[TI1]
    {
        // queue BdReady message to pTaskInfo_->getQueue()
        BdReadyMessage* pBdReady = new BdReadyMessage(rMessage);

        TaskControl::SendEvent(pTaskInfo_->GetQueue(), pBdReady);
    }
    // else $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: send
//
//@ Interface-Description
//  Sends a copy of the specified GuiReadyMessage to the task
//  associated with this TaskInfo object.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses the GuiReadyMessage copy constructor to create a new message
//  Uses TaskControl::SendEvent to queue the message to the task's
//  queue.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
TaskAgent::send(const GuiReadyMessage& rMessage) const
{
    CALL_TRACE("send(GuiReadyMessage&)");

    //  check if task is flagged to receive task control messages
    if (   pTaskInfo_->IsFlagOn(F_TCMSG) )  // $[TI3]
    {
        // queue GuiReady message to pTaskInfo_->getQueue()
        GuiReadyMessage* pGuiReady = new GuiReadyMessage(rMessage);

        TaskControl::SendEvent(pTaskInfo_->GetQueue(), pGuiReady);
    }
    // else $[TI4]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: send
//
//@ Interface-Description
//  Sends a copy of the specified CommDownMessage to the task
//  associated with this TaskInfo object.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses the CommDownMessage copy constructor to create a new message.
//  Uses TaskControl::SendEvent to queue the message to the task's
//  queue.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
TaskAgent::send(const CommDownMessage& rMessage) const
{
    CALL_TRACE("send(CommDownMessage&)");

    //  check if task is flagged to receive task control messages
    if (   pTaskInfo_->IsFlagOn(F_TCMSG) ) // $[TI5]
    {
        // queue CommDown message to pTaskInfo_->getQueue()
        CommDownMessage* pCommDown = new CommDownMessage(rMessage);

        TaskControl::SendEvent(pTaskInfo_->GetQueue(), pCommDown);
    }
    // else $[TI6]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: send
//
//@ Interface-Description
//  Sends a ChangeStateMessage to the task associated with this 
//  TaskAgent object.  TaskAgentManager calls this method for each
//  task to request a state transition to the application tasks.  Each
//  message task then responds with a TaskReadyMessage which are 
//  received by the TaskAgentManager and forwarded to the TaskAgent.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses TaskControl::SendEvent to queue the message to the task's
//  queue.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
TaskAgent::send(const ChangeStateMessage& rMessage) const
{
    CALL_TRACE("send(ChangeStateMessage&)");

    //  check if task is flagged to receive task control messages
    if (   pTaskInfo_->IsFlagOn(F_TCMSG) ) // $[TI7]
    {
        // queue ChangeState message to pTaskInfo_->getQueue()
        ChangeStateMessage* pChangeState
            = new ChangeStateMessage(  rMessage.getState() , this , rMessage.getSequenceNumber());

        TaskControl::SendEvent(pTaskInfo_->GetQueue(), pChangeState);
    }
    // else $[TI8]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: receive
//
//@ Interface-Description
//  Receives a TaskReadyMessage from the task associated with this 
//  TaskAgent.  This method is called whenever a TaskReadyMessage
//  for this TaskAgent is received by the TaskAgentManager.  It updates
//  the completed transition sequence number as received in the
//  TaskReadyMessage.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
TaskAgent::receive(const TaskReadyMessage& rMessage)
{
    CALL_TRACE("send(TaskReadyMessage&)");

    // if this is a newer transition item then update internal sequence
    if ( rMessage.getSequenceNumber() > sequenceCompleted_ ) // $[TI1]
    {
        sequenceCompleted_ = rMessage.getSequenceNumber();
    }
    // else $[TI2]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isTransitioned
//
//@ Interface-Description
//  Returns true if this TaskAgent is updated to the transition sequence
//  specified.  Accepts a sequence number parameter.  This method will
//  always return true for tasks that do not pariticipate in the 
//  state transition sequencing.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean
TaskAgent::isTransitioned(const Uint32 sequenceExpected) const
{
    CALL_TRACE("isTransitioned(Uint32)");

    //  check if task is flagged to receive task control messages
    if (   pTaskInfo_->IsFlagOn(F_TCMSG) )  // $[TI1]
    {
        return sequenceCompleted_ >= sequenceExpected;  // $[TI1.1] $[TI1.2]
    }
    else  //  $[TI2]
    {
        return TRUE;
    }
}


//=====================================================================
 

#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//
//@ Interface-Description
//   This method is called when a software fault is detected by the
//   fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//   'lineNumber' are essential pieces of information.  The 'pFileName'
//   and 'pPredicate' strings may be defaulted in the macro to reduce
//   code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This method receives the call for the SoftFault, adds it sub-system
//   and class name ID and sends the message to the non-member function
//   SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
void
TaskAgent::SoftFault(const SoftFaultID  softFaultID,
                     const Uint32       lineNumber,
                     const char*        pFileName,
                     const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
 
  FaultHandler::SoftFault(softFaultID, SYS_INIT,
                          Sys_Init::ID_TASKAGENT, 
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)

//=====================================================================
//
//      Protected Methods...
//
//=====================================================================

//=====================================================================
//
//      Private Methods...
//
//=====================================================================




