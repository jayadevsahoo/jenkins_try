#ifndef Semaphore_HH
#define Semaphore_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Nellcor/Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: Semaphore - Mutual exclusion mechanism to protect shared data.
//---------------------------------------------------------------------
//@ Version
//@(#) $Header:   /840/Baseline/Download/vcssrc/Semaphore.hhv   25.0.4.0   19 Nov 2013 14:02:50   pvcs  $ 
//
//@ Modification-Log
//
//  Revision: 002  By: Gary Cederquist  Date: 28-JUL-1997  DR Number: 2197
//       Project:  Sigma (R8027)
//       Description:
//             Changed to use SigmaTypes.hh
//
//  Revision: 001  By:  jhv    Date:  Jan-24-1996    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//====================================================================

#include "DownloadFault.hh"
#include <vrtxvisi.h>
#include <vrtxil.h>
#include "DownloadClassIds.hh"

enum WaitOrder
{
	PRIORITY_ORDER
};

//@ Constant: SEMAPHORE_LENGTH
// Length of a VRTX semaphore. 
enum SemaphoreCons
{
		NO_TIMEOUT = 0,
		SEMAPHORE_LENGTH = 1
};

class Semaphore
{
  public:
    Semaphore(void);
    ~Semaphore(void);

    SigmaStatus lock(const Uint32 timeout=NO_TIMEOUT);
    SigmaStatus release(void);
#ifdef SIGMA_UNIT_TEST
	void setSemaphoreId(void);
#endif // SIGMA_UNIT_TEST
    static inline void SoftFault(const FaultType softFaultID,
				 const Uint32      lineNumber,
				 const char*       pFileName  = NULL, 
				 const char*       pPredicate = NULL) {
  					DownloadFault::SoftFault(softFaultID, 
  					DOWNLOAD, SEMAPHORE_CLASS, lineNumber, pFileName,
						pPredicate); }


  protected:

  private:
    // these methods are purposely declared, but not implemented...
    Semaphore(const Semaphore&);		// not implemented...
    void   operator=(const Semaphore&);	// not implemented...

    //@ Data-Member: error_
    // Contains the last return code from VRTX32 for this Semaphore.
    Int32 error_;
 
    //@ Data-Member: semaphoreId_
    // VRTX32 semaphore ID returned for this Semaphore when created.
    Int32 semaphoreId_;
};

#endif // Semaphore_HH 
