
#ifndef TimerModeTrigger_HH
#define TimerModeTrigger_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: TimerModeTrigger -  Triggers when corresponding Interval 
//        Timer expires.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/TimerModeTrigger.hhv   25.0.4.0   19 Nov 2013 14:00:12   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 003  By:  sp    Date:  4-Mar-1996    DR Number:600
//       Project:  Sigma (R8027)
//       Description:
//             Remove unit test method.
//
//  Revision: 002  By:  sp    Date:  4-Mar-1996    DR Number:674
//       Project:  Sigma (R8027)
//       Description:
//             Remove #ifndef.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//====================================================================

#  include "Sigma.hh"

#  include "Breath_Delivery.hh"


//@ Usage-Classes
#  include "ModeTrigger.hh"

#  include "TimerTarget.hh"

#  include "IntervalTimer.hh"
//@ End-Usage

class TimerModeTrigger : public ModeTrigger, public TimerTarget {
  public:
    TimerModeTrigger(IntervalTimer& iTimer,const TriggerId id);
    virtual ~TimerModeTrigger(void);

    static void SoftFault(const SoftFaultID softFaultID,
  const Uint32      lineNumber,
  const char*       pFileName  = NULL, 
  const char*       pPredicate = NULL);

    inline void restartTimer(const Int32 interval);
    virtual void disable(void);
    virtual void timeUpHappened(const IntervalTimer& itimer);
 
  protected:
    virtual Boolean triggerCondition_(void);

  private:
    TimerModeTrigger(const TimerModeTrigger&);  // not implemented...
    void   operator=(const TimerModeTrigger&);  // not implemented...
    TimerModeTrigger(void);  // not implemented...
	

    //@ Data-Member:  rIntervalTimer_
    // The interval timer associated with this trigger
    IntervalTimer& rIntervalTimer_;
 
    //@ Data-Member: timeUp_
    // Set to true when the interval timer expires
    Boolean timeUp_;

};


// Inlined methods...
#include "TimerModeTrigger.in"


#endif // TimerModeTrigger_HH 
