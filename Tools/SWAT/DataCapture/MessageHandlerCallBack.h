//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file MessageHandlerCallBack.h
//----------------------------------------------------------------------------

#ifndef MESSAGEHANDLERCALLBACK_H
#define MESSAGEHANDLERCALLBACK_H

//-------------------------------------
// I N C L U D E S
//-------------------------------------
#include "Message.h"
#include <boost/shared_ptr.hpp>

//-------------------------------------
// C L A S S  D E C L
//-------------------------------------

class MessageHandlerCallBack
{
    public:
    MessageHandlerCallBack(bool (*function)(boost::shared_ptr<Message> msg, void *pUser), void *pUser);
    ~MessageHandlerCallBack();

    bool execute(boost::shared_ptr<Message> msg);

    void activate();
    void deactivate();

    inline void *getUserData()
    {
        return m_pUser;
    }

    private:
    MessageHandlerCallBack(const MessageHandlerCallBack&);  //Not implimented
    MessageHandlerCallBack operator=(const MessageHandlerCallBack&);               //Not implimented


    bool (*m_function)(boost::shared_ptr<Message> msg, void *pUser);
    void *m_pUser;
    bool  m_pActive;
};


#endif

