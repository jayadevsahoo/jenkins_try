
// E600UtilDlg.cpp : implementation file
//

#include "stdafx.h"
#include "E600Util.h"
#include "E600UtilDlg.h"
#include "UserMessages.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	//ID_INDICATOR_CAPS,
	//ID_INDICATOR_NUM,
	//ID_INDICATOR_SCRL,
	IDS_INDICATOR_PROGRESS,
	IDS_INDICATOR_MSG_COUNT,
	IDS_INDICATOR_2,
	IDS_INDICATOR_3
};


// CE600UtilDlg dialog


CE600UtilDlg::CE600UtilDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CE600UtilDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CE600UtilDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_E600UTIL_TAB1, m_tabDerivedTabCtrl);
}

BEGIN_MESSAGE_MAP(CE600UtilDlg, CDialog)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_CLOSE()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_MESSAGE_VOID(WM_KICKIDLE, OnKickIdle)
	ON_MESSAGE(WM_ETH_MSG_UPDATE_WINDOW, &OnEthMsgUpdateWindow)
	ON_UPDATE_COMMAND_UI(IDS_INDICATOR_MSG_COUNT, &OnUpdateMsgCount)

	ON_BN_CLICKED(ID_APP_EXIT, &CE600UtilDlg::OnBnClickedAppExit)
END_MESSAGE_MAP()


// App command to run the dialog
void CE600UtilDlg::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}

// CE600UtilDlg message handlers

BOOL CE600UtilDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	//ShowWindow(SW_MAXIMIZE);

	// TODO: Add extra initialization here
	m_tabDerivedTabCtrl.InsertItem(0, _T("Configurations"));
	m_tabDerivedTabCtrl.InsertItem(1, _T("   Waveforms  "));
	m_tabDerivedTabCtrl.InsertItem(2, _T("   Monitors   "));
	m_tabDerivedTabCtrl.InsertItem(3, _T("Cals/Self-Test"));
	m_tabDerivedTabCtrl.InsertItem(4, _T("Online Tuning "));
	m_tabDerivedTabCtrl.InsertItem(5, _T("   Settings   "));

	//m_tabDerivedTabCtrl.Init();
	//m_tabDerivedTabCtrl.SelectTab(0);
	//m_tabDerivedTabCtrl.pDlg = this;

	// 1 - Create the toolbar
	if (!m_wndToolBar.Create(this) ||
		!m_wndToolBar.LoadToolBar(IDR_E600UTIL_TOOLBAR))
	{
		TRACE0("Failed to create dialog toolbar\n");
		EndDialog( IDCANCEL );
	}

	// 2 - Figure out how big the control bar(s) are.
	CRect rcClientStart;
	CRect rcClientNow;
	GetClientRect(rcClientStart);
	RepositionBars(AFX_IDW_CONTROLBAR_FIRST, AFX_IDW_CONTROLBAR_LAST, 
				   0, reposQuery, rcClientNow);

	// 3 - Move all the controls so they are in the same
	// relative position within the remaining client area as
	// they would be with no control bar(s).
	CPoint ptOffset(rcClientNow.left - rcClientStart.left,
					rcClientNow.top - rcClientStart.top); 

	CRect  rcChild;					
	CWnd* pwndChild = GetWindow(GW_CHILD);
	while (pwndChild)
	{                               
		pwndChild->GetWindowRect(rcChild);
		ScreenToClient(rcChild);
		rcChild.OffsetRect(ptOffset);
		pwndChild->MoveWindow(rcChild, FALSE);
		pwndChild = pwndChild->GetNextWindow();
	}

	// 4 - Adjust the dialog window dimensions to make room
	// for the control bar(s)
	CRect rcWindow;
	GetWindowRect(rcWindow);
	rcWindow.right += rcClientStart.Width() - rcClientNow.Width();
	rcWindow.bottom += rcClientStart.Height() - rcClientNow.Height();
	MoveWindow(rcWindow, FALSE);
	
	// 5 - Position the control bar(s)
	RepositionBars(AFX_IDW_CONTROLBAR_FIRST, AFX_IDW_CONTROLBAR_LAST, 0);
		
	// 6 - Center the dialog on the screen
	// (this would not happen automatically because the
	// dialog window's size changed during the WM_INITDIALOG
	// processing)
	//CenterWindow();

	//////////////////////////////////////////////////////////////////
	if (!m_wndStatusBar.Create(this,WS_CHILD|WS_VISIBLE|WS_BORDER) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}

	// give first status bar pane a border
	UINT nID,nStyle;
	int nWidth;
	m_wndStatusBar.GetPaneInfo(0, nID, nStyle, nWidth) ;
	m_wndStatusBar.SetPaneInfo(0, nID, SBPS_STRETCH, nWidth) ;

	m_wndStatusBar.SetPaneInfo(1, IDS_INDICATOR_PROGRESS, SBPS_NORMAL, 400);
	m_wndStatusBar.SetPaneInfo(2, IDS_INDICATOR_MSG_COUNT, SBPS_NORMAL, 30);
	m_wndStatusBar.SetPaneInfo(3, IDS_INDICATOR_2, SBPS_NORMAL, 30);
	m_wndStatusBar.SetPaneInfo(4, IDS_INDICATOR_3, SBPS_NORMAL, 30);
	
	CRect StatusBarRect;
	GetDlgItem(IDC_STATIC_STATUSBAR)->GetWindowRect(&StatusBarRect);
	ScreenToClient(&StatusBarRect);
	m_wndStatusBar.MoveWindow(&StatusBarRect);

	ProgressCount = 0;
	//m_wndStatusBar.GetProgressCtrl().SetPos(ProgressCount);
	SetProgress(ProgressCount);
	//m_wndStatusBar.SetPaneText(0, L"Ready");
	SetStatusText(0, L"Ready");

	m_tabDerivedTabCtrl.pStatusBar = &m_wndStatusBar;
	m_tabDerivedTabCtrl.Init();
	m_tabDerivedTabCtrl.SelectTab(0);
	m_tabDerivedTabCtrl.pDlg = this;
	
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CE600UtilDlg::SetProgress(int Count)
{
	m_wndStatusBar.GetProgressCtrl().SetPos(Count);
}

void CE600UtilDlg::SetStatusText(int Pane, LPCTSTR Text)
{
	m_wndStatusBar.SetPaneText(0, Text);
}

void CE600UtilDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CE600UtilDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CE600UtilDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CE600UtilDlg::OnUpdateMsgCount(CCmdUI *pCCmdUI)
{
	static int Count = 0;
	CString MsgCountStr(L"");

	MsgCountStr.Format(L"%d", Count++);

	pCCmdUI->Enable(true);
	pCCmdUI->SetText((LPCTSTR)MsgCountStr);
}

LRESULT CE600UtilDlg::OnEthMsgUpdateWindow(WPARAM x, LPARAM y)
{
	ProgressCount += 1;
	if(ProgressCount > 100)
		ProgressCount = 0;
	m_wndStatusBar.GetProgressCtrl().SetPos(ProgressCount);

	return 0;
}


void CE600UtilDlg::OnKickIdle()
{
	UpdateDialogControls(this, FALSE);
	m_wndStatusBar.UpdateWindow();
}


void CE600UtilDlg::OnBnClickedAppExit()
{
	// TODO: Add your control notification handler code here
	//m_tabDerivedTabCtrl.SendMessage(WM_APP_SHUTDOWN);		// for child controls/dialogs to do cleaning up
    //EndDialog(IDOK);										// for modal dialog only?
	theApp.m_pMainWnd->DestroyWindow();
}

void CE600UtilDlg::PostNcDestroy()
{
	delete this;
}

// when user clicks X, do the same as for the EXIT button
void CE600UtilDlg::OnClose()
{
	theApp.m_pMainWnd->DestroyWindow();
}

void CE600UtilDlg::OnOK()
{
}

