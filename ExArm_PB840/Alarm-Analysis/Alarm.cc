#include "stdafx.h"
//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: Alarm - The central locus of alarm information. 
//---------------------------------------------------------------------
//@ Interface-Description
// As the Operating Parameter hierarchy is evaluated by the Operating Parameter
// Analysis cluster, if a group that causes or augments an alarm changes
// state, the causeUpdate_ or augmentUpdate_ field of the corresponding alarm
// is set accordingly.
//
// When the evaluation of the Operating Parameter hierarchy is complete, the
// Alarm Analysis cluster takes over and evaluates all Alarm objects whose
// causeUpdate_ or augmentUpdate_ attributes were set.  A list of 
// AlarmUpdateBuffer objects containing alarm information to be annunciated
// to the user is made available to the Alarm Annunciation cluster and an
// event is sent to the GUI task under which the Alarm Annunciation cluster 
// runs.
//---------------------------------------------------------------------
//@ Rationale
// The Alarm class is the main collection point for the alarm information
// contained in the Non-Technical and Technical Alarm Matrices (see Sections
// 2.5.6 and 2.5.7 of the Software Requirements Specification).
//---------------------------------------------------------------------
//@ Implementation-Description
// What follows is a description of how each column in the Alarm Matrices is
// modeled in class Alarm:
//
// ALARM/BASE MESSAGE
// The information contained in this column is stored for internal use in field
// name_ and identifies which alarm is represented in this instantiation of
// Alarm.  The base message, which is annunciated to the operator is contained
// as an enumerator in the baseMessage_ attribute.
// This column also identifies which alarms are Patient Data Alarms.  This
// information is stored in the boolean field patientDataAlarm_.
//
// CONDITION AUGMENTATION
// This column in the first matrix row for each alarm contains the detection
// criteria for the alarm and is defined in an OperatingGroup referenced by
// field rCausedBy_.  When this OperatingGroup undergoes a change in state it 
// updates its associated AlarmAugmentation and the causeUpdate_ field in the 
// Alarm referenced by the associated AlarmAugmentation.
//
// The Condition Augmentation objects contained in subsequent rows 
// are also defined in OperatingGroup objects.  When these undergo a change 
// in state, they update the AlarmAugmentation objects in their 
// AlarmAugmentationList and the field Alarm::augmentUpdate_ in
// the alarm referenced by the AlarmAugmentation.
//
// URGENCY/PRIORITY
// The urgency and priority for a matrix row are contained in the
// AlarmAnalysisInformation referenced by the AlarmAugmentation associated with
// the row.
//
// DEPENDENT AUGMENTATION
// The Dependent Augmentation information contained in matrix rows is modeled in
// DependentAugmentation objects which form relationships between two Alarm 
// objects.  When one of the Alarm objects undergoes a change, the associated 
// DependentAugmentation is accessed and the other Alarm object
// referenced by the DependentAugmentation is marked for update.  The
// DependentAugmentation objects are collected in Alarm::augmentations_.
//
// ANALYSIS MESSAGE
// The enumerated name of the analysis message associated with the first matrix
// row is contained in the AlarmAnalysisInformation referenced by the field
// Alarm::rInfo_.  The enumerated name of the analysis message associated with
// all other matrix rows is contained in the AlarmAnalysisInformation object
// referenced by the AlarmAugmentation associated with the matrix row.
//
// REMEDY MESSAGE
// The enumerated name of the remedy message associated with the first matrix
// row is contained in the AlarmAnalysisInformation referenced by the field
// Alarm::rInfo_.  The enumerated name of the remedy message associated with
// all other matrix rows is contained in the AlarmAnalysisInformation object
// referenced by the AlarmAugmentation associated with the matrix row.
//
// SYSTEM RESPONSE
// If there is a system response that must be processed when an augmentation
// changes state to TRUE, it is pointed to by the AlarmAugmentation associated
// with the matrix row.
//
// RESET CRITERIA
// If the autoreset criteria for the alarm is not the negation of the criteria
// specified in the Condition Augmentation column, it is specified in this
// column.  The OperatingGroup which defines the Condition Augmentation
// criteria is reevaluated using the autoreset constraint pointed to by the
// OperatingGroup::pAutoResetConstraint_.
//
// RESET SYSTEM RESPONSE
// If there is a system response that must be processed when an augmentation
// changes state to FALSE, it is pointed to by the AlarmAugmentation associated
// with the matrix row.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//      none
//---------------------------------------------------------------------
//@ Invariants
//      none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Alarm-Analysis/vcssrc/Alarm.ccv   25.0.4.0   19 Nov 2013 13:51:12   pvcs  $
//
//@ Modification-Log
//
//   Revision 001   By:   hct      Date: 01/01/95         DR Number:   n/a
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//  
//   Revision 002   By:   hct      Date: 01/05/96         DR Number:   644
//      Project:   Sigma   (R8027)
//      Description:
//         Assertions on counting semaphores testing for >= 1 are wrong.
//         If there is a loss of communications and the BD detects an alarm
//         which requires that one of the counting semaphores be incremented,
//         that information won't reach the GUI.  If the alarm autoresets after
//         communications has been re-established, the GUI will receive
//         notification that the counting semaphore (which was never
//         incremented) needs to be decremented.  This case could fail the
//         >= 1 test.  The assertion should verify the counting semaphore is
//         >= 0 and if it is zero, the decrement method should not decrement.
//  
//   Revision 003   By:   hct      Date: 01/18/96         DR Number:   659
//      Project:   Sigma   (R8027)
//      Description:
//         Use OsTimeStamp for interval timing instead of TimeStamp.
//  
//   Revision 004   By:   gbs      Date: 05/17/96         DR Number:   995
//      Project:   Sigma   (R8027)
//      Description:
//         Fix CALL_TRACE and add passed parameter comments.
//         These changes do not affect Unit Test.
//  
//   Revision 005   By:   hct      Date: 08/28/96         DR Number:  1274
//      Project:   Sigma   (R8027)
//      Description:
//         The code in method Alarm::updateRanking_ to determine 
//         currentPriority is inverted.
//  
//   Revision 006   By:   hct      Date: 12/03/96         DR Number:  1338
//      Project:   Sigma   (R8027)
//      Description:
//         Moved all alarm annunciation to GUI CPU.  BD_CPU forwards alarm
//         events to the GUI_CPU.  Don't need to send LOSS_PAT_DATA_MON_EN
//         event to the other CPU.
//  
//   Revision 007   By:   hct      Date: 12/06/96         DR Number:  1508, 1535
//      Project:   Sigma   (R8027)
//      Description:
//         Added check for the Alarm being inactive to the check of 
//         ConditionAugmenations in evaluateConditionAugs.
//  
//   Revision 008   By:   hct      Date: 02/14/97         DR Number:   N/A
//      Project:   Sigma   (R8027)
//      Description:
//         Formal review rework.
//  
//   Revision 009   By:   hct      Date: 24 APR 1997      DR Number:  1994
//      Project:   Sigma   (R8027)
//      Description:
//         Deleted requirement number 05147.
//  
//   Revision 010	By:   hhd	   Date: 08/05/97         DR Number:  	2310 
//      Project:   Sigma   (R8O27)
//      Description:
//		   Invoked InitAlarmTextInfo().
//
//   Revision 011   By:   sah      Date: 01/15/98       DR Number: 5004
//      Project:   Sigma   (R8O27)
//      Description:
//         Removed all reference to newly-obsoleted class ('AlarmMessages').
//
//   Revision 012	By:   hct	   Date: 01/12/98       DR Number: 5001
//      Project:   Sigma   (R8O27)
//      Description:
//		   Added ListActivePDResetAlarmConditions().
//
//   Revision 013	By:   sah	   Date: 01/15/98       DR Number: 5004
//      Project:   Sigma   (R8O27)
//      Description:
//		   Removed all referenced to newly-obsoleted class ('AlarmMessages').
//
//   Revision 014   By:   hct    Date: 07/01/98         DR Number:  5109
//      Project:   Sigma   (R8027)
//      Description:
//         Changed High Circuit Pressure alarm per DCS.
//  
//   Revision 015   By:   sah      Date: 01/12/99         DR Number:   5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to non-inlined method.
//  
//   Revision 016   By:   yyy      Date: 08/18/99         DR Number:   5513
//      Project:   ATC
//      Description:
//         Modified to handle alarm lock and break through.
//  
//   Revision 017   By:   gdc      Date: 01/04/00         DR Number:   5588
//      Project:   840 NeoMode
//      Description:
//         Using TimeStamp::GetRNullTimeStamp() instead of global 
//         reference.
//  
//   Revision 018   By:   rhj      Date: 01/16/09        SCR Number:  6452
//      Project:  840S
//      Description:
//         Added setRemedyMessage().
// 
//=====================================================================

#include "Sigma.hh"
#include "Alarm.hh"

//@ Usage-Classes
#include "AlarmAugmentation.hh"
#include "AlarmAnalysisInformation.hh"
#include "ConditionAugmentation.hh"
#include "DependentAugmentation.hh"
#include "OperandNameList.hh"
#include "OperatingComponent.hh"
#include "OperatingGroup.hh"
#include "OperatingParamEventName.hh"

//@ End-Usage

static Uint 
    pAlarmsMemory[(sizeof(AlarmList) + sizeof(Uint) - 1) / sizeof(Uint)];
AlarmList& Alarm::RAlarms_ = *((AlarmList*)::pAlarmsMemory);

Boolean Alarm::PatientDataReset_;    
Boolean Alarm::CancelScreenLockStatus_;    
Alarm::UpdateType Alarm::UpdateType_;
Int32 Alarm::NoPatientDataDisplayStatus_;
Int32 Alarm::NoGuiSettingChangesStatus_;
Int32 Alarm::VentInopLedOnStatus_;


//@ Code...

//=====================================================================
//
//      Public Methods
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Alarm 
//
//@ Interface-Description
//   Constructor.
//
// >Von
//   n                   Enumerated name of the Alarm.
//   rInfo               Reference to the object which holds the enumerators
//                       identifying the analysis message and remedy message
//                       associated with the detection criteria row in the 
//                       Alarm Matrices.
//   rCausedBy           Reference to the OperatingGroup which models the
//                       detection criteria for this alarm.
//   resetConditionName  Every Alarm can be thought of as being caused by a 
//                       core set of operating parameters modeled by 
//                       OperatingConditions.  There may be other
//                       OperatingCondition objects in the detection criteria, 
//                       but if the core set is not TRUE, the alarm will be 
//                       inactive.  For example, for the NO AIR SUPPLY alarm to 
//                       be active, the OperatingCondition modeling Wall
//                       Air Present must evaluate to FALSE.  If the Boolean 
//                       value of this OperatingCondition is negated, the NO AIR
//                       SUPPLY alarm will be reset.  The resetConditionName_ 
//                       is used to negate the main OperatingCondition during
//                       Manual Reset and Patient Data Reset.
//   patDataAlarm        Used to identify those Alarm objects marked as Patient 
//                       Data Alarms in the Alarm Matrices.  Patient Data
//                       Alarms are those alarms which depend on patient data 
//                       for their detection.  These alarms are not annunciated 
//                       while an alarm causing Loss of Patient Data Monitoring 
//                       is active and are reset when an alarm causing Loss of 
//                       Patient Data Monitoring auto resets.
//   base                This is the enumerator which identifies the text 
//                       associated with the Alarm Name/Base Message column of 
//                       the Alarm Matrices.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
// Constructs an Alarm object.
//---------------------------------------------------------------------
//@ PreCondition
//      ((ALARM_NAME_NULL < n) && (n < MAX_NUM_ALARMS))
//      ((OPERAND_NAME_NULL <= resetConditionName) &&
//       (resetConditionName < MAX_NUM_OPERANDS)
//      ((MESSAGE_NAME_NULL < base) && 
//       (base < MAX_NUM_ALARM_MESSAGES))
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Alarm::Alarm(const AlarmName n,
             AlarmAnalysisInformation& rInfo,
             OperatingGroup& rCausedBy,
             OperandName resetConditionName,
             Boolean patDataAlarm,
             MessageName base,
             Boolean breakThroughSilence):

             name_(n),
             rInfo_(rInfo),
             rCausedBy_(rCausedBy),
             resetConditionName_(resetConditionName),
             patientDataAlarm_(patDataAlarm),
             baseMessage_(base),
             breakThroughSilence_(breakThroughSilence)
{     
  CALL_TRACE("Alarm(n, rInfo, rCausedBy, resetConditionName, patDataAlarm, base)");

  CLASS_PRE_CONDITION((ALARM_NAME_NULL < n) &&
                      (n < MAX_NUM_ALARMS));
  CLASS_PRE_CONDITION((OPERAND_NAME_NULL <= resetConditionName) &&
                      (resetConditionName < MAX_NUM_OPERANDS));
  CLASS_PRE_CONDITION((MESSAGE_NAME_NULL < base) && 
                      (base < MAX_NUM_ALARM_MESSAGES));

  isActive_ = FALSE;
  whenOccurred_ = TimeStamp::GetRNullTimeStamp();
  intervalTime_.invalidate();
  whenAugmented_ = TimeStamp::GetRNullTimeStamp();
  currentUrgency_ = Alarm::NORMAL_URGENCY;
  previousUrgency_ = Alarm::NORMAL_URGENCY;
  currentPriority_ = 0;
  isDependent_ = FALSE;
  causeUpdate_ = FALSE;
  augmentUpdate_ = FALSE;
  analysisIndex_ = 0;
  remedyIndex_ = 0;
  updateType_ = Alarm::UPDATE_TYPE_NULL;
  resetConditionName2_ = OPERAND_NAME_NULL;

  for (Uint32 i=0; i < NUM_MESSAGE; i++)
  {
    currentAnalysisMessage_[i] = MESSAGE_NAME_NULL;
    currentRemedyMessage_[i] = MESSAGE_NAME_NULL;
  }
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Alarm 
//
//@ Interface-Description
//   Constructor.
//
// >Von
//   n                   Enumerated name of the Alarm.
//   rInfo               Reference to the object which holds the enumerators
//                       identifying the analysis message and remedy message
//                       associated with the detection criteria row in the 
//                       Alarm Matrices.
//   rCausedBy           Reference to the OperatingGroup which models the
//                       detection criteria for this alarm.
//   resetConditionName  Every Alarm can be thought of as being caused by a 
//                       core set of operating parameters modeled by 
//                       OperatingConditions.  There may be other
//                       OperatingCondition objects in the detection criteria, 
//                       but if the core set is not TRUE, the alarm will be 
//                       inactive.  For example, for the NO AIR SUPPLY alarm to 
//                       be active, the OperatingCondition modeling Wall
//                       Air Present must evaluate to FALSE.  If the Boolean 
//                       value of this OperatingCondition is negated, the NO AIR
//                       SUPPLY alarm will be reset.  The resetConditionName_ 
//                       is used to negate the main OperatingCondition during
//                       Manual Reset and Patient Data Reset.
//   resetConditionName2 Every Alarm can be thought of as being caused by a 
//                       core set of operating parameters modeled by 
//                       OperatingConditions.  There may be other
//                       OperatingCondition objects in the detection criteria, 
//                       but if the core set is not TRUE, the alarm will be 
//                       inactive.  For example, for the NO AIR SUPPLY alarm to 
//                       be active, the OperatingCondition modeling Wall
//                       Air Present must evaluate to FALSE.  If the Boolean 
//                       value of this OperatingCondition is negated, the NO AIR
//                       SUPPLY alarm will be reset.  The resetConditionName_ 
//                       is used to negate the main OperatingCondition during
//                       Manual Reset and Patient Data Reset.
//   patDataAlarm        Used to identify those Alarm objects marked as Patient 
//                       Data Alarms in the Alarm Matrices.  Patient Data
//                       Alarms are those alarms which depend on patient data 
//                       for their detection.  These alarms are not annunciated 
//                       while an alarm causing Loss of Patient Data Monitoring 
//                       is active and are reset when an alarm causing Loss of 
//                       Patient Data Monitoring auto resets.
//   base                This is the enumerator which identifies the text 
//                       associated with the Alarm Name/Base Message column of 
//                       the Alarm Matrices.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
// Constructs an Alarm object.
//---------------------------------------------------------------------
//@ PreCondition
//      ((ALARM_NAME_NULL < n) && (n < MAX_NUM_ALARMS))
//      ((OPERAND_NAME_NULL <= resetConditionName) &&
//       (resetConditionName < MAX_NUM_OPERANDS)
//      ((OPERAND_NAME_NULL <= resetConditionName2) &&
//       (resetConditionName2 < MAX_NUM_OPERANDS)
//      ((MESSAGE_NAME_NULL < base) && 
//       (base < MAX_NUM_ALARM_MESSAGES))
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Alarm::Alarm(const AlarmName n,
             AlarmAnalysisInformation& rInfo,
             OperatingGroup& rCausedBy,
             OperandName resetConditionName,
             OperandName resetConditionName2,
             Boolean patDataAlarm,
             MessageName base,
             Boolean breakThroughSilence):

             name_(n),
             rInfo_(rInfo),
             rCausedBy_(rCausedBy),
             resetConditionName_(resetConditionName),
             resetConditionName2_(resetConditionName2),
             patientDataAlarm_(patDataAlarm),
             baseMessage_(base),
             breakThroughSilence_(breakThroughSilence)
{     
  CALL_TRACE("Alarm(n, rInfo, rCausedBy, resetConditionName, patDataAlarm, base)");

  CLASS_PRE_CONDITION((ALARM_NAME_NULL < n) &&
                      (n < MAX_NUM_ALARMS));
  CLASS_PRE_CONDITION((OPERAND_NAME_NULL <= resetConditionName) &&
                      (resetConditionName < MAX_NUM_OPERANDS));
  CLASS_PRE_CONDITION((OPERAND_NAME_NULL <= resetConditionName2) &&
                      (resetConditionName2 < MAX_NUM_OPERANDS));
  CLASS_PRE_CONDITION((MESSAGE_NAME_NULL < base) && 
                      (base < MAX_NUM_ALARM_MESSAGES));

  isActive_ = FALSE;
  whenOccurred_ = TimeStamp::GetRNullTimeStamp();
  intervalTime_.invalidate();
  whenAugmented_ = TimeStamp::GetRNullTimeStamp();
  currentUrgency_ = Alarm::NORMAL_URGENCY;
  previousUrgency_ = Alarm::NORMAL_URGENCY;
  currentPriority_ = 0;
  isDependent_ = FALSE;
  causeUpdate_ = FALSE;
  augmentUpdate_ = FALSE;
  analysisIndex_ = 0;
  remedyIndex_ = 0;
  updateType_ = Alarm::UPDATE_TYPE_NULL;

  for (Uint32 i=0; i < NUM_MESSAGE; i++)
  {
    currentAnalysisMessage_[i] = MESSAGE_NAME_NULL;
    currentRemedyMessage_[i] = MESSAGE_NAME_NULL;
  }
  // $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~Alarm
//
//@ Interface-Description
//      Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Alarm::~Alarm(void)
{
  CALL_TRACE("~Alarm(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: operator==
//
//@ Interface-Description
//      Equality operator.
//
// >Von
//      rAlarm  Reference to the object to be used in the comparison.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//      Two alarm objects are equal if their whenOccurred_ timestamps are equal.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Boolean
Alarm::operator==(const Alarm& rAlarm) const
{
  CALL_TRACE("operator==(rAlarm)");

  return(whenOccurred_ == rAlarm.whenOccurred_); // $[TI1] $[TI2]
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: operator!=
//
//@ Interface-Description
//      Inequality operator.
//
// >Von
//      rAlarm  Reference to the object to be used in the comparison.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//      Two alarm objects are not equal if their whenOccurred_ timestamps are 
//      not equal.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Boolean
Alarm::operator!=(const Alarm& rAlarm) const
{     
  CALL_TRACE("operator!=(rAlarm)");

  return(!(*this == rAlarm)); // $[TI1]  $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: operator<
//
//@ Interface-Description
//      Less than operator which tests to see if this Alarm object is less than
//      the Alarm passed by reference, rAlarm.  If this Alarm occurred prior to
//      the referenced Alarm, rAlarm, then this method returns TRUE.  If this 
//      Alarm occurred after or at the same time as the referenced Alarm, 
//      return FALSE.
// >Von
//      rAlarm  Reference to the object to be used in the comparison.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Boolean
Alarm::operator<(const Alarm& rAlarm) const
{     
  CALL_TRACE("operator<(rAlarm)");

  return(whenOccurred_ < rAlarm.whenOccurred_); // $[TI1]  $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: operator> 
//
//@ Interface-Description
//      Greater than operator which tests to see if this Alarm object is
//      greater than the Alarm passed by reference, rAlarm.  If this Alarm 
//      occurred after the referenced Alarm, rAlarm, then this method returns 
//      TRUE.  If this Alarm occurred prior to or at the same time as the 
//      referenced Alarm, return FALSE.
// >Von
//      rAlarm  Reference to the object to be used in the comparison.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Boolean
Alarm::operator>(const Alarm& rAlarm) const
{     
  CALL_TRACE("operator>(rAlarm)");

  return(whenOccurred_ > rAlarm.whenOccurred_); // $[TI1]  $[TI2]
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Initialize
//
//@ Interface-Description
//      Method called during system initialization.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
Alarm::Initialize(void)
{ 
  CALL_TRACE("Initialize(void)");

  Alarm::InitMemory();
  Alarm::InitData();
  // $[TI1]
}




//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: InitMemory
//
//@ Interface-Description
//      Method called to initialize memory.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
Alarm::InitMemory(void)
{
  CALL_TRACE("InitMemory(void)");

  CLASS_ASSERTION(sizeof(::pAlarmsMemory) >= sizeof(AlarmList));
  new (pAlarmsMemory) AlarmList;
  // $[TI1]
}




//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: InitData
//
//@ Interface-Description
//      Method called to initialize static data.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
Alarm::InitData(void)
{
  CALL_TRACE("InitData(void)");

  Alarm::PatientDataReset_ = FALSE;
  Alarm::UpdateType_ = Alarm::UPDATE_TYPE_NULL;

  // Initialize the following counting semaphores to zero.
  Alarm::NoPatientDataDisplayStatus_ = 0;
  Alarm::NoGuiSettingChangesStatus_ = 0;
  Alarm::VentInopLedOnStatus_ = 0;

  // Initialize the following attribute to FALSE.
  Alarm::CancelScreenLockStatus_ = FALSE;
  // $[TI1]
}




//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: AddAlarm
//
//@ Interface-Description
//      Places a pointer to the Alarm object pointed to by passed parameter
//      pAlarm in the AlarmList array.
//
// >Von
//      pAlarm  Pointer to the object to be added.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//      The Alarm pointer is placed into the array using the enumerated
//      name of the Alarm object as the index.
//---------------------------------------------------------------------
//@ PreCondition
//      (pAlarm != NULL)
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void 
Alarm::AddAlarm(Alarm* pAlarm)
{
  CALL_TRACE("AddAlarm(pAlarm)");

  CLASS_PRE_CONDITION(pAlarm != NULL);

  Alarm::RAlarms_[(int)pAlarm->getName()] = pAlarm;
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ListManResetAlarmConditions
//
//@ Interface-Description
//      When the operator presses the Alarm Reset key, all active Non-Technical 
//      Alarms must be reset, including their detection algorithms.  In order to
//      accomplish this, each Alarm is examined.  For each active Alarm, the 
//      name of the OperatingConditions (which when negated cause their 
//      associated Alarm to be reset) contained in the resetConditionName 
//      attributes, are placed on the list of AlarmOperand objects to be
//      evaluated $[05016].
//      If the resetConditionName attributes are OPERAND_NAME_NULL, the alarm
//      is technical and should not be reset $[05020].
//
// >Von
//      rOperandNameList  Reference to the list the objects are to be appended
//                        to.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//      Those Alarm objects which are not manually resettable by the operator
//      have resetConditionName attributes set to OPERAND_NAME_NULL; therefore,
//      if resetConditionName does not equal OPERAND_NAME_NULL and the
//      Alarm is active, append the OperandName enumerators contained in
//      resetConditionName attributes on the list of names of AlarmOperand 
//      objects to be reset.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
Alarm::ListManResetAlarmConditions(OperandNameList& rOperandNameList) 
{
  CALL_TRACE("ListManResetAlarmConditions(rOperandNameList)");

  for (int ii = 0; ii < MAX_NUM_ALARMS; ii++)
  {
    CLASS_ASSERTION(Alarm::RAlarms_[ii] != NULL);

    if (Alarm::RAlarms_[ii]->isActive_ && 
        (Alarm::RAlarms_[ii]->resetConditionName_ != 
                OPERAND_NAME_NULL)) // $[TI1]
    {
      rOperandNameList.append(Alarm::RAlarms_[ii]->resetConditionName_);
      if (Alarm::RAlarms_[ii]->resetConditionName2_ != OPERAND_NAME_NULL) // $[TI1.1]
      {
        rOperandNameList.append(Alarm::RAlarms_[ii]->resetConditionName2_);
      } // else $[TI1.2]
    } // else $[TI2]
  }
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ListPDResetAlarmConditions
//
//@ Interface-Description
//      When an Alarm which causes Loss of Patient Data Monitoring autoresets,
//      all Patient Data Alarms must be reset, including their detection
//      algorithms.  In order to accomplish this, each Alarm is examined.  For
//      each Alarm in which attribute patientDataAlarm_ is TRUE, the name of
//      the OperatingCondition (which when negated causes its associated Alarm
//      to be reset) contained in attribute resetConditionName_ is placed on
//      the list of AlarmOperand objects to be evaluated $[05040].
//
// >Von
//      rOperandNameList  Reference to the list the objects are to be appended
//                        to.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
Alarm::ListPDResetAlarmConditions(OperandNameList& rOperandNameList) 
{
  CALL_TRACE("ListPDResetAlarmConditions(rOperandNameList)");

  for (int ii = 0; ii < MAX_NUM_ALARMS; ii++)
  {
    CLASS_ASSERTION(Alarm::RAlarms_[ii] != NULL);

    if (Alarm::RAlarms_[ii]->patientDataAlarm_) // $[TI1]
    {
      rOperandNameList.append(Alarm::RAlarms_[ii]->resetConditionName_);
      if (Alarm::RAlarms_[ii]->resetConditionName2_ != OPERAND_NAME_NULL) // $[TI1.1]
      {
        rOperandNameList.append(Alarm::RAlarms_[ii]->resetConditionName2_);
      } // else $[TI1.2]
    } // else $[TI2]
  }
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ListActivePDResetAlarmConditions
//
//@ Interface-Description
//      When an Alarm which causes Loss of Patient Data Monitoring is manually
//      reset, all active Patient Data Alarms must be reset, including their
//      detection algorithms.  In order to accomplish this, each Alarm is 
//      examined.  For each Alarm in which attribute patientDataAlarm_ is TRUE,
//      the name of the OperatingConditions (which when negated cause their 
//      associated Alarm to be reset) contained in the resetConditionName 
//      attributes are placed on the list of AlarmOperand objects to be
//      evaluated if the conditions they represent are active $[05040].
//
// >Von
//      rOperandNameList  Reference to the list the objects are to be appended
//                        to.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
Alarm::ListActivePDResetAlarmConditions(OperandNameList& rOperandNameList) 
{
  CALL_TRACE("ListActivePDResetAlarmConditions(rOperandNameList)");

  for (int ii = 0; ii < MAX_NUM_ALARMS; ii++)
  {
    CLASS_ASSERTION(Alarm::RAlarms_[ii] != NULL);

    // If the alarm is a Patient Data alarm and the reset OperatingCondition
    // is active, append the reset OperatingCondition name to the AlarmOperand
    // list
    if (Alarm::RAlarms_[ii]->patientDataAlarm_) // $[TI1]
    {
      if (AlarmOperand::GetOperand(Alarm::RAlarms_[ii]->resetConditionName_)
        ->getIsActive()) // $[TI1.1]
      {
        rOperandNameList.append(Alarm::RAlarms_[ii]->resetConditionName_);
      } // $[TI1.2]
      if ((Alarm::RAlarms_[ii]->resetConditionName2_ != OPERAND_NAME_NULL) && 
          AlarmOperand::GetOperand(Alarm::RAlarms_[ii]->resetConditionName2_)
          ->getIsActive()) // $[TI1.3]
      {
        rOperandNameList.append(Alarm::RAlarms_[ii]->resetConditionName2_);
      } // $[TI1.4]
    } // else $[TI2]
  }
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: evaluateUpdates
//
//@ Interface-Description
//      Method evaluateUpdates is called after the Operating Parameter
//      Hierarchy has been evaluated and the Alarm objects have been marked for
//      update.  evaluateUpdates checks this Alarm and determines whether or
//      not it needs to be updated, and returns a Boolean indicating its
//      findings $[05045].
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Boolean
Alarm::evaluateUpdates(void) 
{
  CALL_TRACE("evaluateUpdates(void)");

  // Create a temporary Boolean
  Boolean needsUpdate = FALSE;

  // If the OperatingGroup which causes this alarm has changed state,
  // causeUpdate_ will be TRUE and the alarm needs to be evaluated.
  if (causeUpdate_) // $[TI1]
  {
    needsUpdate = TRUE;
    isActive_ = rCausedBy_.getIsActive();

    // If the OperatingGroup which causes this alarm is active, it just
    // transitioned to that state so this alarm just became active.
    if (isActive_) // $[TI1.1]
    {
      // Set the CancelScreenLockStatus_ attribute so that GUI-Applications
      // will cancel the screen lock due to the occurrence of a new alarm.
      // $[01224]
      Alarm::SetCancelScreenLockStatus();

      // Run the Alarm Matrix System response (if there is one).
      rInfo_.runResponse();
      initializeAlarm_();

      // Put the analysis and remedy messages associated with the base alarm
      // into the message queues
      getInfo_(rInfo_, currentUrgency_);

      // Set the instance update type.
      setUpdateType_(Alarm::DETECTION_UT); // $[05045]
    }
    else // The OperatingGroup just transitioned to inactive $[TI1.2]
    {
      // Run the Alarm Matrix Reset System Response
      rInfo_.runResetResponse();
      initializeAlarm_();

      // Set the instance update type based on the static update type.
      switch(Alarm::UpdateType_)
      {
        case Alarm::MANUAL_RESET_UT: // $[TI1.2.1]
          setUpdateType_(Alarm::MANUAL_RESET_UT); // $[05045]
          break;
        case Alarm::PAT_DATA_RESET_UT: // $[TI1.2.2]
          setUpdateType_(Alarm::PAT_DATA_RESET_UT); // $[05045]
          break;
        case Alarm::UPDATE_TYPE_NULL: // $[TI1.2.3]
          setUpdateType_(Alarm::AUTO_RESET_UT); // $[05045]
          break;
        default:
          CLASS_ASSERTION(FALSE); // Alarm::UpdateType_ should never hit default
          break;
      };       
    } 
    // Clear the whenAugmented_ attribute.  
    whenAugmented_.invalidate();
  } 
  
  // If the causeUpdate_ attribute is set, the alarm will be evaluated so
  // the value of the augmentUpdate_ attribute doesn't matter; however, if
  // the causeUpdate_ attribute is FALSE, the augmentUpdate_ attribute must
  // be examined.  If augmentUpdate_ is TRUE and the Alarm is active, the
  // alarm must be evaluated but the initial processing done in this method
  // is slightly different than in the case of causeUpdate_.  
  else if (augmentUpdate_) // $[TI2]
  {
    if (isActive_) // $[TI2.1]
    {
      needsUpdate = TRUE;
      initializeAlarm_();
 
      // Put the analysis and remedy messages associated with the base alarm
      // into the message queues
      getInfo_(rInfo_, currentUrgency_);

      // Set the instance update type.
      setUpdateType_(Alarm::AUGMENTATION_UT); // $[05045]
    }
    else // $[TI2.2]
    {
      augmentUpdate_ = FALSE;
    }
  } // else $[TI3]
  return(needsUpdate);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: evaluateConditionAugs
//
//@ Interface-Description
//      This method examines each object referenced by the
//      Alarm::augmentations_ list in order to determine whether or not the
//      information referenced by the augmentation should be included in the
//      current annunciation for this alarm.
//---------------------------------------------------------------------
//@ Implementation-Description
//      If the OperatingGroup referenced 
//      by the ConditionAugmentation is active, the AlarmInformation referenced
//      by the augmentation is included in the lists of information to be 
//      annunciated to the operator.  If the ConditionAugmentation is inactive, 
//      the OperatingGroup just transitioned to active so the 
//      AlarmAnalysisInformation object pointed to by rInfo is messaged to run 
//      the system response and the ConditionAugmentation is set to active.
//      If the OperatingGroup is inactive and the ConditionAugmentation is 
//      active, the OperatingGroup just transitioned to inactive so the 
//      AlarmAnalysisInformation object pointed to by rInfo is messaged to run 
//      the reset system response and the ConditionAugmentation is set to 
//      inactive.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
Alarm::evaluateConditionAugs(void) 
{
  CALL_TRACE("evaluateConditionAugs(void)");

  SigmaStatus status = augmentations_.goFirst();
  while (status == SUCCESS) // $[TI1]
  {
    AlarmAugmentation& rAug = augmentations_.currentItem();

    // This method is only interested in ConditionAugmentation objects on
    // the list.
    if (rAug.getRole() == AlarmAugmentation::CONDITION_AUGMENTATION) // $[TI1.1]
    {
      // There are three participants in this relationship: the Alarm, the
      // ConditionAugmentation and the OperatingGroup.  Get a reference to
      // the OperatingGroup.
      const OperatingGroup& rGroup = 
          ((ConditionAugmentation&)rAug).getAugmentGroup();

      // Get a reference to the AlarmAnalysisInformation object which holds
      // the information to be annunciated to the operator.
      const AlarmAnalysisInformation& rInfo = rAug.getInfo();

      // If the alarm is inactive, it just transitioned to inactive.  Cancel
      // any active augmentations.
      if (!isActive_) // $[TI1.1.0]
      {
        if (rAug.getIsActive()) // $[TI1.1.0.1]
        {
          rInfo.runResetResponse();
          rAug.clearIsActive();
        } // $[TI1.1.0.2]
      }
      // If the OperatingGroup is active, the information for this
      // augmentation will be included in the current annunciation for this
      // Alarm.
      else if (rGroup.getIsActive()) // $[TI1.1.1]
      {
        getInfo_(rInfo, Alarm::NORMAL_URGENCY);
        
        // If the ConditionAugmentation is not active, but the OperatingGroup
        // is, the OperatingGroup just transitioned to active so the Alarm
        // Matrix System Response associated with the ConditionAugmentation
        // must be run and the ConditionAugmentation set to active.        
        if (!rAug.getIsActive()) // $[TI1.1.1.1]
        {
          rInfo.runResponse();
          rAug.setIsActive();
        } // else $[TI1.1.1.2]
      } 
      else // $[TI1.1.2]
      {
        // However, if the OperatingGroup is not active and the
        // ConditionAugmentation is, the OperatingGroup just transitioned to
        // inactive so the Alarm Matrix Reset System Response associated with
        // the ConditionAugmentation must be run and the ConditionAugmentation
        // set to inactive.
        if (rAug.getIsActive()) // $[TI1.1.2.1]
        {
          rInfo.runResetResponse();
          rAug.clearIsActive();
        } // else $[TI1.1.2.2]
      } 
    } // else $[TI1.2]
    status = augmentations_.goNext();
  } // No while statement executed  $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: evaluateDependencies
//
//@ Interface-Description
//      Check to see if any superior alarms are active, not dependent and
//      older.  If so, set isDependent_ for this Alarm.
//---------------------------------------------------------------------
//@ Implementation-Description
//      Examine this Alarm to determine if it should be annunciated as an
//      independent or a dependent Alarm.  In order to accomplish this, each
//      object on this Alarm object's augmentation_ list is examined.  If the
//      object is an active DependentAugmentation or this alarm just became
//      active on this cycle of alarm processing then if this Alarm is the
//      dependent alarm in any "superior Alarm / DependentAugmentation /
//      dependent Alarm" relationship and the superior Alarm is active and
//      occurred prior to this Alarm then set isDependent_ for this Alarm else
//      clear isDependent_ $[05033] $[05035] $[05036].
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
Alarm::evaluateDependencies(void) 
{
  CALL_TRACE("evaluateDependencies(void)");

  if (isActive_) // $[TI1]
  {
    isDependent_ = FALSE;

    SigmaStatus status = augmentations_.goFirst();
    // Continue processing while there are more augmentations and isDependent_
    // hasn't been set
    while ((status == SUCCESS) && !isDependent_) // $[TI1.1]
    {
      const AlarmAugmentation& rAug = augmentations_.currentItem();

      // If the augmentation is a DependentAugmentation...
      if (rAug.getRole() == AlarmAugmentation::DEPENDENT_AUGMENTATION) // $[TI1.1.1]
      {
        // If the DependentAugmentation is already active or the Alarm
        // has just become active on this cycle of alarm processing...
        if (rAug.getIsActive() || (updateType_ == DETECTION_UT)) // $[TI1.1.1.1]
        {
          Alarm& rSuperiorAlarm = rAug.getAlarm();

          // If this is the dependentAlarm and the superior Alarm is active
          // and not itself dependent and occurred prior to this Alarm...
          if ((&rSuperiorAlarm != this) && rSuperiorAlarm.getIsActive() &&
             (!rSuperiorAlarm.getIsDependent()) &&
             (rSuperiorAlarm.getIntervalTime() < intervalTime_)) // $[TI1.1.1.1.1]
          {
            isDependent_ = TRUE;
          } // else $[TI1.1.1.1.2]
        } // else $[TI1.1.1.2]
      } // else $[TI1.1.2]
      status = augmentations_.goNext();
    } // No while statement executed  $[TI1.2]
  } // else $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: evaluateDependentAugs
//
//@ Interface-Description
//      Build the annunciations for the Alarm objects which are dependent to
//      this alarm so they will be grouped together and begin with the phrase
//      "Note ". $[05041]
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
Alarm::evaluateDependentAugs(void) 
{
  CALL_TRACE("evaluateDependentAugs(void)");

  Uint32 tempIndex = 0;

  // Evaluate this alarms DependentAugmentation objects
  SigmaStatus status = augmentations_.goFirst();

  // If this Alarm does have dependent augmentations just in case any are 
  // active, append the enumerator for "Note " to the analysis message list.
  if (status == SUCCESS) // $[TI1]
  {
    // Append the "Note " enumerator.
    currentAnalysisMessage_[analysisIndex_++] = NOTE_MSG;
    // Store the analysisIndex_ in a local variable.
    tempIndex = analysisIndex_;
  } // else $[TI2]

  while (status == SUCCESS) // $[TI3]
  {
    DependentAugmentation& rDepAug = 
        (DependentAugmentation&)augmentations_.currentItem();

    // If the augmentation is a DependentAugmentation...
    if (rDepAug.getRole() == AlarmAugmentation::DEPENDENT_AUGMENTATION) // $[TI3.1]
    {
      // ... and this Alarm is the superior Alarm in the superior Alarm /
      // DependentAugmentation / dependent Alarm relationship...   
      if (&(rDepAug.getDependentAlarm()) != this) // $[TI3.1.1]
      {
        Alarm& rDependentAlarm = rDepAug.getDependentAlarm();

        const AlarmAnalysisInformation& rInfo = rDepAug.getInfo();

        // ... and this superior Alarm is active...
        if (isActive_) // $[TI3.1.1.1]
        {
          // ...and this superior alarm is independent...
          if (!isDependent_) // $[TI3.1.1.1.1]
          {
            // ...and the DependentAugmentation is already active...
            if (rDepAug.getIsActive()) // $[TI3.1.1.1.1.1]
            {
              // ... and the dependent Alarm is still active... $[05044]
              if (rDependentAlarm.getIsActive()) // $[TI3.1.1.1.1.1.1]
              {
                // ...add the dependent information to the superior alarm
                // $[05034]
                getInfo_(rInfo, rDependentAlarm.getCurrentUrgency());
              }
              // ... but the dependent Alarm is no longer active...
              else  // $[TI3.1.1.1.1.1.2]
              {
                // ...run the Alarm Matrix System Reset response and clear the
                // isActive_ attribute of the augmentation.
                rInfo.runResetResponse();
                rDepAug.clearIsActive();
              }
            }
            // ...but the DependentAugmentation is inactive...
            else // $[TI3.1.1.1.1.2]
            {
              // ...check to see if the potentially dependent Alarm should
              // actually be dependent to this Alarm.  This check is necessary 
              // because evaluateDependencies sets isDependent_ if ANY of the 
              // superior Alarm objects meets the criteria.  This alarm may not
              // have been the one for which isDependent_ was set.  If the
              // dependent alarm should be dependent to this superior
              // alarm (the dependent Alarm has just become active and its
              // isDependent_ attribute is set meaning it should be dependent
              // to at least one superior alarm and this Alarm occurred prior
              // to the dependent Alarm), add the dependent information to the
              // superior alarm (The currentUrgency_ attribute of the dependent
              // alarm reflects the the urgency of the condition augmentations 
              // only, as determined in Alarm method evaluateConditionAugs_
              // $[05038].) and run the Alarm Matrix System Response associated
              // with this DependentAugmentation and set the isActive attribute 
              // of the augmentation.
              
              if (rDependentAlarm.getIsActive() && 
                 rDependentAlarm.getUpdateType() == Alarm::DETECTION_UT &&
                 rDependentAlarm.getIsDependent() &&
                 (intervalTime_ < rDependentAlarm.getIntervalTime())) // $[TI3.1.1.1.1.2.1]
              {           
                getInfo_(rInfo, rDependentAlarm.getCurrentUrgency());
                rInfo.runResponse();
                rDepAug.setIsActive();
              } // else $[TI3.1.1.1.1.2.2]
            }
          } // else $[TI3.1.1.1.2]
        }
        // ...but this superior alarm is inactive...
        else // $[TI3.1.1.2]
        {
          // ...and the DependentAugmentation is active...
          if (rDepAug.getIsActive()) // $[TI3.1.1.2.1]
          {
            // ...meaning this superior alarm transitioned to inactive during
            // the current cycle of alarm reevaluation, so run the Reset System
            // Response and clear the isActive_ attribute for the augmentation.
            // If the isDependent_ attribute of the dependent Alarm is FALSE,
            // the dependent Alarm was dependent to the superior alarm ONLY
            // and now the superior Alarm is inactive so set the updateType_
            // of the dependent Alarm to Alarm::INDEPENDENT_UT.
          
            rInfo.runResetResponse();
            rDepAug.clearIsActive();
            if (!rDependentAlarm.getIsDependent()) // $[TI3.1.1.2.1.1]
            {
              rDependentAlarm.setUpdateType_(Alarm::INDEPENDENT_UT); // $[05045]
            } // else $[TI3.1.1.2.1.2]
          } // else $[TI3.1.1.2.2]
        }
      } // else $[TI3.1.2]
    } // else $[TI3.2]
    status = augmentations_.goNext();
  } // No while statement executed $[TI4]
  // Compare the current value of analysisIndex_ with the locally stored value.
  // If they are the same, remove the "Note " enumerator.
  if ((tempIndex != 0) && (tempIndex == analysisIndex_)) // $[TI5]
  {
    currentAnalysisMessage_[analysisIndex_--] = MESSAGE_NAME_NULL;
  } // else $[TI6]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: addAugmentation
//
//@ Interface-Description
//      Add the referenced AlarmAugmentation to this Alarm object's list of
//      augmentations.
//
// >Von
//      rAugmentation  Reference to the object to be added.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
Alarm::addAugmentation(AlarmAugmentation& rAugmentation) 
{
  CALL_TRACE("addAugmentation(rAugmentation)");

  augmentations_.append(rAugmentation);
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
// Method: setRemedyMessage
//
// Interface-Description
//   Sets the remedy message of an alarm based on the given 
//   remedyMessageName parameter.
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//   The input parameter remedyMessageName must be within the 
//   MessageName enumerator list.  
//---------------------------------------------------------------------
// PostCondition
//   none
// End-Method
//=====================================================================
void
	Alarm::setRemedyMessage(MessageName remedyMessageName) 
{
	CALL_TRACE("setRemedyMessage(MessageName remedyMessageName)");

	AUX_CLASS_PRE_CONDITION((MESSAGE_NAME_NULL <= remedyMessageName) &&
						    (remedyMessageName < MAX_NUM_ALARM_MESSAGES),
							remedyMessageName);

	rInfo_.setRemedyMessage(remedyMessageName);
}




//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SetNoPatientDataDisplayStatus
//
//@ Interface-Description
//      Increment the value of the class attribute NoPatientDataDisplayStatus_.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
Alarm::SetNoPatientDataDisplayStatus(void)
{
  CALL_TRACE("SetNoPatientDataDisplayStatus(void)");

  Alarm::NoPatientDataDisplayStatus_++;
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ClearNoPatientDataDisplayStatus      
//
//@ Interface-Description
//      Decrement the value of the class attribute NoPatientDataDisplayStatus_.
//      If the Alarm attribute NoPatientDisplayStatus_ has changed state from 
//      > 0 (There is at least one alarm which causes Loss of Patient Data
//      Monitoring active on this CPU) to 0 (There are no alarms which cause 
//      Loss of Patient Data Monitoring active on this CPU), message the static 
//      Alarm method, SetPatientDataReset, so that all Patient Data Alarm 
//      algorithms will be reset on this CPU.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
Alarm::ClearNoPatientDataDisplayStatus(void)
{
  CALL_TRACE("ClearNoPatientDataDisplayStatus(void)");

  CLASS_ASSERTION(Alarm::NoPatientDataDisplayStatus_ >= 0);

  if (Alarm::NoPatientDataDisplayStatus_ >= 1) // $[TI1]
  {
    Alarm::NoPatientDataDisplayStatus_--;

    // If Alarm::NoPatientDataDisplayStatus_ == 0 then message 
    // Alarm::SetPatientDataReset() so that Patient Data Alarm algorithms will
    // be reset.
    if (Alarm::NoPatientDataDisplayStatus_ == 0) // $[TI1.1]
    {
      // Set the PatientReset attribute so that Patient Data Alarm algorithms
      // will be reset.
      Alarm::SetPatientDataReset();
    }  // else $[TI1.2]
  } // else $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//           
//
//@ Interface-Description
//   This method is called when a software fault is detected by the
//   fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//   'lineNumber' are essential pieces of information.  The 'pFileName'
//   and 'pPredicate' strings may be defaulted in the macro to reduce
//   code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This method receives the call for the SoftFault, adds it sub-system
//   and class name ID and sends the message to the non-member function
//   SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition   
//   none
//@ End-Method
//=====================================================================
void
Alarm::SoftFault(const SoftFaultID  softFaultID,
                 const Uint32       lineNumber,
                 const char*        pFileName,
                 const char*        pPredicate)  
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, ALARM_ANALYSIS, Alarm_Analysis::ALARM, lineNumber,
                          pFileName, pPredicate);
}


//=====================================================================
//          
//      Protected Methods
//          
//=====================================================================

//=====================================================================
//
//      Private Methods
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getInfo_
//
//@ Interface-Description
//      Get the Alarm Matrix Analysis Message, Remedy Message and
//      Urgency/Priority from the rInfo object.  The Urgency/Priority
//      will depend on the dependentUrgency parameter.
//
// >Von
//      rInfo             Reference to the object which holds the enumerators 
//                        identifying the analysis message and remedy message 
//                        associated with the detection criteria row in the 
//                        Alarm Matrices.
//      dependentUrgency  Passed to method updateRanking_. 
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//      If there is an analysis message and/or a remedy message, add
//      it/them to this Alarm object's message enumerator arrays.
//      Message method updateRanking_ to determine the Urgency/Priority.
//---------------------------------------------------------------------
//@ PreCondition
//      ((Alarm::URGENCY_TYPE_NULL < dependentUrgency) &&
//       (dependentUrgency < Alarm::MAX_NUM_URGENCIES))
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
Alarm::getInfo_(const AlarmAnalysisInformation& rInfo, 
                const Alarm::Urgency dependentUrgency)
{
  CALL_TRACE("getInfo_(rInfo, dependentUrgency)");

  CLASS_PRE_CONDITION((Alarm::URGENCY_TYPE_NULL < dependentUrgency) &&
                      (dependentUrgency < Alarm::MAX_NUM_URGENCIES));

  MessageName tempName = rInfo.getAnalysisMessage();
  if (tempName != MESSAGE_NAME_NULL) // $[TI1]
  {
    currentAnalysisMessage_[analysisIndex_++] = tempName;
  } // else $[TI2]

  tempName = rInfo.getRemedyMessage();
  if (tempName != MESSAGE_NAME_NULL) // $[TI3]
  {
    currentRemedyMessage_[remedyIndex_++] = tempName;
  } // else $[TI4]
  
  updateRanking_(rInfo, dependentUrgency);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateRanking_
//
//@ Interface-Description
//      Update this Alarm object's currentUrgency_ and currentPriority_
//      attributes from the referenced rInfo based on the dependentUrgency
//      parameter.
//
// >Von
//      rInfo             Reference to the object which holds the enumerators 
//                        identifying the analysis message and remedy message 
//                        associated with the detection criteria row in the 
//                        Alarm Matrices.
//      dependentUrgency  Used to determine the urgency and priority for this
//                        Alarm object. 
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method is used to update the currentUrgency_ and currentPriority_
//      for this Alarm from both DependentInformation and AlarmInformation
//      objects through polymorphism.  Remember that an Alarm annunciation is
//      built from the augmentations referenced in its augmentations_ list and
//      that the urgency/priority is based on the maximum of all of the active
//      augmentations.  If the dependentUrgency parameter is equal to
//      NORMAL_URGENCY, the referenced rInfo is attached to a
//      ConditionAugmentation and the urgency is contained in the rInfo object.
//      If the dependentUrgency parameter is not equal to NORMAL_URGENCY, the
//      referenced rInfo is attached to a DependentAugmentation and the
//      dependentUrgency is the evaluated urgency of the dependent Alarm and is
//      considered to be the urgency associated with the augmentation.  A
//      temporary variable is set to whatever the urgency associated with the
//      augmentation is determined to be.
//
//      The priority is determined by comparing the currentUrgency_ of the
//      Alarm with the temporary urgency.  If they are equal, the
//      currentPriority_ of this alarm is set to the maximum of the
//      currentPriority_ and the priority contained in the referenced rInfo.
//      If the currentUrgency_ of the Alarm is less than the temporary
//      urgency, the currentUrgency_ is set to the temporary urgency and the
//      currentPriority_ is set to the priority contained in the referenced
//      rInfo $[05034].
//---------------------------------------------------------------------
//@ PreCondition
//      ((Alarm::URGENCY_TYPE_NULL < dependentUrgency) &&
//       (dependentUrgency < Alarm::MAX_NUM_URGENCIES))
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
Alarm::updateRanking_(const AlarmAnalysisInformation& rInfo,
                      const Alarm::Urgency dependentUrgency)
{
  CALL_TRACE("updateRanking_(rInfo, dependentUrgency)");

  CLASS_PRE_CONDITION((Alarm::URGENCY_TYPE_NULL < dependentUrgency) &&
                      (dependentUrgency < Alarm::MAX_NUM_URGENCIES));

  Alarm::Urgency tempUrgency;

  if (dependentUrgency == Alarm::NORMAL_URGENCY)  // Condition augmentation $[TI1]
  {
    tempUrgency = rInfo.getUrgency();
  }
  else                                     // Dependent augmentation $[TI2]
  {
    tempUrgency = dependentUrgency;
  }

  if (tempUrgency == currentUrgency_) // $[TI3]
  {
    Int16 tempPriority = rInfo.getPriority(tempUrgency);
    if (tempPriority < currentPriority_) // $[TI3.1]
    {
      currentPriority_ = tempPriority;
    } // else $[TI3.2]
  }
  
  else if (tempUrgency > currentUrgency_) // $[TI4]
  {
    currentUrgency_ = tempUrgency;
    currentPriority_ = rInfo.getPriority(tempUrgency);
  } // else $[TI5]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: initializeMessages_
//
//@ Interface-Description
//      Clear the analysis message and remedy message array cells and
//      initialize the array indices to 0.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
Alarm::initializeMessages_(void)       
{ 
  CALL_TRACE("initializeMessages_(void)");

  for (Uint32 i=0; i < analysisIndex_; i++)
  {
    currentAnalysisMessage_[i] = MESSAGE_NAME_NULL;
  }
  analysisIndex_ = 0;

  for (Uint32 j=0; j < remedyIndex_; j++)
  {
    currentRemedyMessage_[j] = MESSAGE_NAME_NULL;
  }
  remedyIndex_ = 0;
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: initializeAlarm_
//
//@ Interface-Description
//      Initialize urgencies, priority and analysis/remedy message arrays.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
Alarm::initializeAlarm_(void)       
{
  CALL_TRACE("initializeAlarm_(void)");

  previousUrgency_ = currentUrgency_;
  currentUrgency_ = Alarm::NORMAL_URGENCY;
  currentPriority_ = 0;
  initializeMessages_();
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setSuperiorAugmentUpdate_
//
//@ Interface-Description
//     This method is called to inform superior Alarm objects that a dependent
//     Alarm has undergone an augmentation change so the superior alarm
//     needs to have augmentUpdate_ set so that it will be reevaluated.
// >Von
//      rWhenAugmented  Timestamp reflecting when the OperatingGroup causing the
//                      augmentUpdate changed state.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//      When an OperatingGroup which augments an Alarm undergoes a change in
//      state, it calls that Alarm object's public setAugmentUpdate method
//      passing the time it changed state.  setAugmentUpdate sets
//      augmentUpdate_ for itself and calls this method for all the superior
//      alarms with which it is associated, passing the timestamp passed to it
//      $[05036].
//
//---------------------------------------------------------------------
//@ PreCondition
//      
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
Alarm::setSuperiorAugmentUpdate_(const TimeStamp& rWhenAugmented) 
{
  CALL_TRACE("setSuperiorAugmentUpdate_(rWhenAugmented)");

  SigmaStatus status = augmentations_.goFirst();
  // Examine each of this Alarm object's augmentations.  
  while (status == SUCCESS) // $[TI1]
  {
    const AlarmAugmentation& rAug = augmentations_.currentItem();

    // If the augmentation is a DependentAugmentation and this Alarm is the
    // dependent Alarm in the dependent Alarm / DependentAugmentation /
    // superior Alarm relationship and the DependentAugmentation is currently
    // active (meaning this Alarm is currently being annunciated dependently
    // as part of the superior Alarm object's annunciation), message the
    // superior Alarm to set it's augmentUpdate_ attribute.
    if ((rAug.getRole() == AlarmAugmentation::DEPENDENT_AUGMENTATION) &&
       (&rAug.getAlarm() != this) && rAug.getIsActive()) // $[TI1.1]
    {
      rAug.getAlarm().setAugmentUpdate_(rWhenAugmented);
    } // else $[TI1.2]
    status = augmentations_.goNext();
  } // No while statment executed $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setIndirectAugmentUpdate_
//
//@ Interface-Description
//      This method is called when an Alarm object's causeUpdate_ attribute is
//      set.  If a superior Alarm undergoes a change in state, it may be
//      transitioning to inactive in which case any active dependent Alarm
//      objects should become independent.  If a dependent Alarm undergoes a
//      change in state, it may affect it's superior Alarm object's
//      annunciation $[05044].  Therefore, this method examines all of this 
//      Alarm object's dependent augmentations and messages the 
//      setAugmentUpdate method for any affected superior/dependent Alarm 
//      objects.
// >Von
//      rWhenAugmented  Timestamp reflecting when the OperatingGroup causing the
//                      augmentUpdate changed state.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//      When an OperatingGroup that causes an Alarm undergoes a change in
//      state, it calls that Alarm object's setCauseUpdate method passing the
//      time it changed state and its current state.  setCauseUpdate sets
//      causeUpdate_ for itself and calls this method, passing the timestamp
//      and the state passed to it.
//
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
Alarm::setIndirectAugmentUpdate_(const TimeStamp& rWhenAugmented)
{
  CALL_TRACE("setIndirectAugmentUpdate_(rWhenAugmented)");

  SigmaStatus status = augmentations_.goFirst();

  // Examine each of this Alarm object's augmentations.
  while (status == SUCCESS) // $[TI1]
  {
    AlarmAugmentation& rAug = augmentations_.currentItem();

    // If the augmentation is a DependentAugmentation...
    if (rAug.getRole() == AlarmAugmentation::DEPENDENT_AUGMENTATION) // $[TI1.1]
    {
      // If this is the dependent Alarm, message the superior Alarm to set its
      // augmentUpdate_ attribute so it will be evaluated.
      if (&rAug.getAlarm() != this) // $[TI1.1.1]
      {
        rAug.getAlarm().setAugmentUpdate_(rWhenAugmented);
      }
      // Else this Alarm is the superior Alarm.  If the group causing this
      // superior Alarm has transitioned to inactive, so will this superior
      // Alarm transition to inactive.  Therefore, message the dependent Alarm 
      // to set its augmentUpdate_ attribute because it may become independent.
      else // $[TI1.1.2]
      {
        // If the augmentation is active, the group causing this alarm
        // has transitioned to inactive and so will this superior Alarm.        
        // Therefore, message the dependent Alarm to set its augmentUpdate_ 
        // attribute because it may become independent $[05035].
        if (rAug.getIsActive()) // $[TI1.1.2.1]
        {
          ((DependentAugmentation&)rAug).
              getDependentAlarm().setAugmentUpdate_(rWhenAugmented);
        } // else $[TI1.1.2.2]
      }
    } // else $[TI1.2]
    status = augmentations_.goNext();
  } // No while statement executed $[TI2]
}
