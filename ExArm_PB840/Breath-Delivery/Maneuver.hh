
#ifndef Maneuver_HH
#define Maneuver_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: Maneuver - Maneuver base class
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header::   /840/Baseline/Breath-Delivery/vcssrc/Maneuver.hhv   10.7   08/17/07 09:38:38   pvcs  
//
//@ Modification-Log
//
//  Revision: 007   By: gdc   Date:  29-Nov-2006   SCR Number: 6319
//  Project:  RESPM
//  Description:
//		Removed cancelManeuverRequested() method so derived classes
//      will use clearRequested_ protected data member more consistently.
//		
//  Revision: 006   By: gdc   Date:  22-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//      To accommodate RM maneuvers: Moved pressure stability buffer to 
//      its own class. Moved maneuver specific processing to derived classes.
//
//  Revision: 005  By: syw     Date:  18-Nov-1999    DR Number: 5584
//  Project:  ATC
//  Description:
//		Created TerminateManeuver() method to handle termination of pauses.
//
//  Revision: 002  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001  By:  iv    Date:  27-Feb-1997    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Initial version - per Breath Delivery formal code review 
//====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"
#include "EventData.hh"
#include "BreathPhase.hh"
#include "PauseTypes.hh"

//@ Usage-Classes

//@ End-Usage



class Maneuver
{
  public:

	//@ Type: ManeuverId
	// This enum represents the type of maneuver
	enum ManeuverId
	{
		NULL_ID
		, INSP_PAUSE
		, EXP_PAUSE
		, NIF_MANEUVER
		, P100_MANEUVER
		, VITAL_CAPACITY_MANEUVER
	};

	Maneuver(ManeuverId maneuverId);
	~Maneuver(void);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

	virtual void activate(void) = 0;
	virtual Boolean complete(void) = 0;
	virtual void clear(void) = 0;
	virtual void terminateManeuver(void) = 0;
	virtual void newCycle(void) = 0;
	virtual Boolean processUserEvent(UiEvent& rUiEvent) = 0;

	inline PauseTypes::ManeuverState getManeuverState(void) const;
	void setActiveManeuver(void);
	void resetActiveManeuver(void);

	static void Complete(void);
	static void CancelManeuver(void);
	static void TerminateManeuver( void) ;
	static void NewCycle(void);
	static Boolean ProcessUserEvent(UiEvent& rUiEvent, EventData::RequestedState request);

  protected:
	//@ Data-Member: breathPhaseType_
	// Previous breath phase type.
	static BreathPhaseType::PhaseType PreviousBreathPhaseType_;

	//@ Data-Member: clearRequested_
	// TRUE when user has requested to cancel the maneuver.
	Boolean clearRequested_;

	//@ Data-Member: maneuverState_
	// Current maneuver state
	PauseTypes::ManeuverState maneuverState_;

  private:
	Maneuver(void);	  // not implemented...
	Maneuver(const Maneuver&);		// not implemented...
	void   operator=(const Maneuver&);	// not implemented...

	//@ Data-Member: maneuverId_
	// Maneuver Id.
	const ManeuverId maneuverId_;

	//@ Data-Member: PActiveManeuver_
	// The pointer to the active Maneuver object.
	static Maneuver* PActiveManeuver_;

};


#include "Maneuver.in"

#endif // Maneuver_HH 
