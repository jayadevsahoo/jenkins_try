#if !defined(AFX_TESTRUNSSET_H__617D1251_54A5_11D3_9C97_004005519CCD__INCLUDED_)
#define AFX_TESTRUNSSET_H__617D1251_54A5_11D3_9C97_004005519CCD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TestrunsSet.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CTestrunsSet DAO recordset

class CTestrunsSet : public CDaoRecordset
{
public:
	CTestrunsSet(CDaoDatabase* pDatabase = NULL);
	DECLARE_DYNAMIC(CTestrunsSet)

// Field/Param Data
	//{{AFX_FIELD(CTestrunsSet, CDaoRecordset)
	long	m_ID;
	long	m_pf_flag;
	CString	m_Fact;
	double	m_MinRdg;
	double	m_Rdg;
	double	m_MaxRdg;
	short	m_TEST_ID;
	long	m_Type;
	//}}AFX_FIELD

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTestrunsSet)
	public:
	virtual CString GetDefaultDBName();		// Default database name
	virtual CString GetDefaultSQL();		// Default SQL for Recordset
	virtual void DoFieldExchange(CDaoFieldExchange* pFX);  // RFX support
	//}}AFX_VIRTUAL

// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TESTRUNSSET_H__617D1251_54A5_11D3_9C97_004005519CCD__INCLUDED_)
