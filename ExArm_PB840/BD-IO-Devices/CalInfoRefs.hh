#ifndef CalInfoRefs_HH
#define CalInfoRefs_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Header: CalInfoRefs - All the external references relating to calibration data
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/CalInfoRefs.hhv   25.0.4.0   19 Nov 2013 13:54:00   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By:  syw    Date:  03-Jul-97    DR Number: DCS 2279
//       Project:  Sigma (R8027)
//       Description:
//			Added extern FlowSensorOffset& RO2SideFlowSensorOffset
//			and extern FlowSensorOffset& RAirSideFlowSensorOffset declaration
//
//  Revision: 002  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 001  By:  syw    Date: 08-Nov-1996    DR Number: INITIAL RELEASE
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

class FlowSerialEeprom ;

extern FlowSerialEeprom& RAirFlowSerialEeprom ;
extern FlowSerialEeprom& RO2FlowSerialEeprom ;
extern FlowSerialEeprom& RExhFlowSerialEeprom ;


class CalInfoFlashBlock ;
extern CalInfoFlashBlock& RCalInfoFlashBlock ;
class FlowSensorOffset ;
extern FlowSensorOffset& RO2SideFlowSensorOffset ;
extern FlowSensorOffset& RAirSideFlowSensorOffset ;
#endif // CalInfoRefs_HH

