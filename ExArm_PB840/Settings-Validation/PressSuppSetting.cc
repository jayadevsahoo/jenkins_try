#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//================= C L A S S   D E S C R I P T I O N =================
//@ Class:  PressSuppSetting - Pressure Support Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, bounded setting that contains the amount of positive
//  pressure that is to be applied to the patient's airway during a
//  spontaneous breath.  This class inherits from 'BoundedSetting' and
//  provides the specific information needed for representation of pressure
//  support.  This information includes the interval and range of this
//  setting's values, this setting's response to a Main Control Transition
//  (see 'acceptTransition()'), and this batch setting's new-patient value
//  (see 'getNewPatientValue()').
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class has NO dependent settings, but it does override
//  'updateContraints_()' to update is dynamic bounds.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/PressSuppSetting.ccv   25.0.4.0   19 Nov 2013 14:27:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 010   By: gdc    Date: 24-Sep-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal non-invasive CPAP.
//
//  Revision: 009   By: sah    Date: 29-Jun-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  modified to use newly-modified 'BoundedRange' class to handle
//         range, via linked list of interval nodes
//
//  Revision: 008   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'getApplicability()' method
//	*  using new 'getApplicability()' method as part of 'acceptTransition()'
//	   implementation
//	*  removed unnecessary 'isAcceptedValid()' method
//
//  Revision: 007   By: sah   Date: 10-Nov-1998    DR Number:  5261
//  Project:  BILEVEL
//  Description:
//	Modified 'acceptTransition()' to use Psupp's new-patient value,
//	rather than current value.
//
//  Revision: 006   By: dosman    Date: 07-May-1998    DR Number: 
//  Project:  BILEVEL
//  Description:
//	add & fix TIs
//
//  Revision: 005   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 004   By: dosman    Date: 06 Jan 1998    DR Number: 
//  Project:  BILEVEL
//  Description:
//	Initial BiLevel version.
//	changed 90 to MAX_ALLOWED_PRESSURE_SETTING_VALUE
//	added symbolic constants
//	added BILEVEL_MODE_VALUE
//      Added requirement numbers for tracing to SRS. 
//      fixed bug so PS is limited by 90-PEEP during transition
//
//  Revision: 003   By: sah    Date: 12-Mar-1998    DR Number: 2752
//  Project:  COLOR
//  Description:
//	--- Merged Rev "Color" (1.26.1.0) into Rev "BiLevel" (1.25.1.0)
//	Added maximum limit of (90 - PEEP) to the Transition Rule.
//
//  Revision: 002   By: sah    Date: 10 Feb 1997    DR Number: 1747
//  Project: Sigma (R8027)
//  Description:
//	Added new maximum limit of (90 - PEEP).
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//=====================================================================

#include "PressSuppSetting.hh"
#include "SupportTypeValue.hh"
#include "ModeValue.hh"
#include "SettingConstants.hh"

//@ Usage-Classes
#include "SettingsMgr.hh"
//@ End-Usage


//@ Code...

//====================================================================
//
//  Static Data Definitions...
//
//====================================================================

//  $[02238] -- The setting's range ...
//  $[02240] -- The setting's resolution ...
static const BoundedInterval  LAST_NODE_ =
{
	0.0f,		// absolute minimum...
	0.0f,		// unused...
	ONES,		// unused...
	NULL
};
static const BoundedInterval  INTERVAL_LIST_ =
{
	70.0f,		// absolute maximum...
	1.0f,		// resolution...
	ONES,		// precision...
	&::LAST_NODE_
};


//====================================================================
//
//  Public Methods...
//
//====================================================================

//============= M E T H O D   D E S C R I P T I O N ==================
//@ Method: PressSuppSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This setting inherits from BoundedSetting.  The method passes to
//  BoundedSetting it's default information. Also, this method initializes
//  the value interval.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

PressSuppSetting::PressSuppSetting(void)
: BatchBoundedSetting(SettingId::PRESS_SUPP_LEVEL,
					  Setting::NULL_DEPENDENT_ARRAY_,
					  &::INTERVAL_LIST_,
					  PRESS_SUPP_MAX_ID, // maxConstraintId...
					  PRESS_SUPP_MIN_ID) // minConstraintId...
{
	CALL_TRACE("PressSuppSetting()");
}	// $[TI1]


//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  ~PressSuppSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

PressSuppSetting::~PressSuppSetting(void)
{
	CALL_TRACE("~PressSuppSetting()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[01071] -- main settings applicability
//  $[01081] -- breath settings applicability
//---------------------------------------------------------------------
//@ PreCondition
//  (qualifierId == Notification::ACCEPTED  ||
//   qualifierId == Notification::ADJUSTED)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
	PressSuppSetting::getApplicability(const Notification::ChangeQualifier qualifierId) const
{
	CALL_TRACE("getApplicability(qualifierId)");

	const Setting*  pSupportType =
		SettingsMgr::GetSettingPtr(SettingId::SUPPORT_TYPE);

	Applicability::Id  applicability;

	if ( pSupportType->getApplicability(qualifierId) != Applicability::INAPPLICABLE )
	{  // $[TI1] -- support type is applicable...
		DiscreteValue  supportTypeValue;

		switch ( qualifierId )
		{
			case Notification::ACCEPTED :		// $[TI1.1]
				supportTypeValue = pSupportType->getAcceptedValue();
				break;
			case Notification::ADJUSTED :		// $[TI1.2]
				supportTypeValue = pSupportType->getAdjustedValue();
				break;
			default :
				AUX_CLASS_ASSERTION_FAILURE(qualifierId);
				break;
		}

		applicability = (supportTypeValue == SupportTypeValue::PSV_SUPPORT_TYPE)
						? Applicability::CHANGEABLE	   // $[TI1.3]
						: Applicability::INAPPLICABLE; // $[TI1.4]
	}
	else
	{  // $[TI2] -- support type is NOT applicable...
		// pressure support can't be applicable if support type isn't...
		applicability = Applicability::INAPPLICABLE;
	}

	return(applicability);
}


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method: acceptTransition(settingId, newValue, currValue) [virtual]
//
//@ Interface-Description
//  This method is responsible for updating this setting based on the
//  the Main Control Setting identified by 'settingId' and the transition
//  of its value from 'currValue' to 'newValue'.
//
//  The setting is interested in the transition of mode to "SIMV" or "SPONT,
//  and of support type to "PSV".
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02025]\b\ -- transitioning to from A/C to PS-applicable
//  $[02027]    -- transitioning to PS...
//---------------------------------------------------------------------
//@ PreCondition
//  ((settingId == SettingId::MODE           &&
//    (newValue == ModeValue::SIMV_MODE_VALUE  ||
//     newValue == ModeValue::SPONT_MODE_VALUE ||
//     newValue == ModeValue::CPAP_MODE_VALUE ||
//     newValue == ModeValue::BILEVEL_MODE_VALUE)  ||
//   (settingId == SettingId:: SUPPORT_TYPE           &&
//    newValue  == SupportTypeValue::PSV_SUPPORT_TYPE)))
//---------------------------------------------------------------------
//@ PostCondition
//   None
//@ End-Method
//=====================================================================

void
	PressSuppSetting::acceptTransition(const SettingId::SettingIdType settingId,
									   const DiscreteValue            newValue,
									   const DiscreteValue)
{
	CALL_TRACE("acceptTransition(settingId, newValue, currValue)");

	if ( settingId == SettingId::MODE )
	{
		// $[TI1]
		AUX_CLASS_PRE_CONDITION((newValue == ModeValue::SIMV_MODE_VALUE  ||
								 newValue == ModeValue::SPONT_MODE_VALUE || 
								 newValue == ModeValue::CPAP_MODE_VALUE || 
								 newValue == ModeValue::BILEVEL_MODE_VALUE),
								newValue);
	}
	else if ( settingId == SettingId::SUPPORT_TYPE )
	{
		// $[TI2]
		AUX_CLASS_PRE_CONDITION(
							   (newValue == SupportTypeValue::PSV_SUPPORT_TYPE),
							   newValue
							   );
	}
	else
	{
		AUX_CLASS_ASSERTION_FAILURE(settingId);
	}

	if ( getApplicability(Notification::ADJUSTED) !=
		 Applicability::INAPPLICABLE )
	{	// $[TI3] -- pressure support is applicable...
		// use new-patient value...
		BoundedValue  pressSupp = getNewPatientValue();

		BoundStatus  dummyStatus(getId());

		// calculate bound constraints...
		updateConstraints_();

		// use the calculation of the maximum bound in 'updateConstraints_()'
		// to limit the current adjusted value to a valid range...
		getBoundedRange_().testValue(pressSupp, dummyStatus);

		// store the transition value into the adjusted context...
		setAdjustedValue(pressSupp);

		// activation of transition rule must be italicized...
		setForcedChangeFlag();
	} // $[TI4] -- pressure support is NOT applicable...
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [const, virtual]
//
//@ Interface-Description
//  Get the setting's new-patient value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns a constant for the new patient value.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
	PressSuppSetting::getNewPatientValue(void) const
{
	CALL_TRACE("getNewPatientValue()");

	BoundedValue  newPatient;

	// $[02239] The setting's new-patient value ...
	newPatient.value     = 0.0f;
	newPatient.precision = ONES;

	return(newPatient);	  // $[TI1]
}


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//	      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	PressSuppSetting::SoftFault(const SoftFaultID  softFaultID,
								const Uint32       lineNumber,
								const char*        pFileName,
								const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

	FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
							PRESSURE_SUPP_LEVEL_SETTING, lineNumber, pFileName,
							pPredicate);
}


//====================================================================
//
//  Protected Methods...
//
//====================================================================

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  updateConstraints_()  [virtual]
//
//@ Interface-Description
//  Update the dynamic bounds of this setting.  This method sets this
//  setting's maximum constraint to the most restrictive of this setting's
//  upper bounds, and, correspondingly, set this setting's minimum
//  constraint to its most restrictive lower bound.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Determine the minimum upper bound, and set the upper limit to it.
//
//  $[02238] -- pressure support's range
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	PressSuppSetting::updateConstraints_(void)
{
	CALL_TRACE("updateConstraints_()");

	const Setting*  pPeep = SettingsMgr::GetSettingPtr(SettingId::PEEP);
	const Setting*  pPeepLow = SettingsMgr::GetSettingPtr(SettingId::PEEP_LOW);
	const Setting*  pHighCctPress =
		SettingsMgr::GetSettingPtr(SettingId::HIGH_CCT_PRESS);

	Real32  peepValue;

	SettingBoundId  peepBasedMaxId;
	SettingBoundId  pcircBasedMaxId;

	if ( pPeep->getApplicability(Notification::ADJUSTED) != 
		 Applicability::INAPPLICABLE )
	{  // $[TI1] -- PEEP is applicable...
		peepValue       = BoundedValue(pPeep->getAdjustedValue()).value;
		peepBasedMaxId  = PRESS_SUPP_MAX_BASED_PEEP_ID;
		pcircBasedMaxId = PRESS_SUPP_MAX_BASED_PCIRC_PEEP_ID;
	}
	else
	{  // $[TI2] -- PEEP low must be applicable...
		SAFE_CLASS_ASSERTION(
							(pPeepLow->getApplicability(Notification::ADJUSTED) != 
							 Applicability::INAPPLICABLE));

		peepValue       = BoundedValue(pPeepLow->getAdjustedValue()).value;
		peepBasedMaxId  = PRESS_SUPP_MAX_BASED_PEEPL_ID;
		pcircBasedMaxId = PRESS_SUPP_MAX_BASED_PCIRC_PEEPL_ID;
	}

	BoundedRange::ConstraintInfo  maxConstraintInfo;

	BoundedValue  maxLimit;


	//===================================================================
	// start out with pressure support's absolute maximum bound...
	//===================================================================

	const Real32  ABSOLUTE_MAX = getAbsoluteMaxValue_();

	// set to absolute maximum bound...
	maxConstraintInfo.value = ABSOLUTE_MAX;
	maxConstraintInfo.id    = PRESS_SUPP_MAX_ID;


	//===================================================================
	// calculate pressure support's maximum bound that is based
	// on the PEEP value...
	//===================================================================

	maxLimit.value = (SettingConstants::MAX_ALLOWED_PRESSURE_SETTING_VALUE -
					  peepValue);

	if ( maxLimit.value < maxConstraintInfo.value )
	{  // $[TI3]
		// reset this range's maximum constraint to allow for "warping" of this
		// new maximum value...
		getBoundedRange_().resetMaxConstraint();

		// warp the calculated maximum value to a lower "click" boundary...
		getBoundedRange_().warpValue(maxLimit, BoundedRange::WARP_DOWN);

		// the PEEP-based maximum is more restrictive than
		// 'maxConstraintInfo.value', therefore save its value and id as the
		// maximum bound value and id...
		maxConstraintInfo.id    = peepBasedMaxId;
		maxConstraintInfo.value = maxLimit.value;
	}  // $[TI4]


	//===================================================================
	// calculate pressure support's maximum bound that is based
	// on high circuit pressure and the PEEP value...
	//===================================================================

	const Real32  HIGH_CCT_VALUE =
		BoundedValue(pHighCctPress->getAdjustedValue()).value;

	maxLimit.value = (HIGH_CCT_VALUE - peepValue -
					  SettingConstants::MIN_MAND_PRESS_BUFF);

	if ( maxLimit.value < maxConstraintInfo.value )
	{  // $[TI5]
		// reset this range's maximum constraint to allow for "warping" of this
		// new maximum value...
		getBoundedRange_().resetMaxConstraint();

		// warp the calculated maximum value to a lower "click" boundary...
		getBoundedRange_().warpValue(maxLimit, BoundedRange::WARP_DOWN);

		// the high circuit pressure-based maximum is more restrictive than
		// 'maxConstraintInfo.value', therefore save its value and id as the maximum
		// bound value and id...
		maxConstraintInfo.id    = pcircBasedMaxId;
		maxConstraintInfo.value = maxLimit.value;
	}  // $[TI6] -- 'maxConstraintInfo.value' is more restrictive...


	// this setting has no maximum soft bounds...
	maxConstraintInfo.isSoft = FALSE;

	// update the maximum bound to the most restrictive bound...
	getBoundedRange_().updateMaxConstraint(maxConstraintInfo);
}
