#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: DiscAutoResetTrigger - Monitors the system for the specific
//      conditions that cause auto reset of disconnect to be detected.
//---------------------------------------------------------------------
//@ Interface-Description
//    This class triggers a reset from the disconnect mode based on 
//    exhaled and delivered flow, and pressure measured from both 
//    the inspiratory and expiratory pressure transducers.  The trigger 
//    is enabled when disconnect is active.  When the conditions calculated 
//    by this trigger are true, the trigger is considered to have "fired".  
//    This trigger is kept on a list contained in any BreathPhaseScheduler
//    that may need it.
//---------------------------------------------------------------------
//@ Rationale
//    This class implements the algorithm for detecting a patient circuit 
//    reconnection, and triggers the transition from disconnect mode.
//---------------------------------------------------------------------
//@ Implementation-Description
//    This class contains a boolean state variable to keep track of whether 
//    this trigger is enabled or not.  If the trigger is not enabled, it 
//    will always return state of false. The trigger contains data 
//    members that must be initialized whenever the trigger is 
//    enabled.  Therefore, the enable() method in the base Trigger 
//    class has been redefined to include this initialization.  
//    The minPressureCount_, maxPressureCount_ and flowCount_ 
//    counters are used to track the length of time the flow and 
//    pressure conditions required for "reconnection sensed" have been met.
//    The occlusionCount_ counter is used to detect reconnection with 
//    an existing occlusion condition.
//    Once the criteria have been met for the specified amount of time, 
//    the trigger fires by notifying the active scheduler of its 
//    state.  This trigger is evaluated every BD cycle if it is enabled
//    and on the active trigger list.
//---------------------------------------------------------------------
//@ Fault-Handling
//    n/a.
//---------------------------------------------------------------------
//@ Restrictions
//    Only one instance of this class may be created.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/DiscAutoResetTrigger.ccv   25.0.4.0   19 Nov 2013 13:59:44   pvcs  $
//
//@ Modification-Log
//
//  Revision: 019  By: rhj     Date: 20-July-2010    SCR Number: 6608 
//  Project:  MAT
//  Description:
//      If the current circuit is Neonatal, FLOW_BAND is used instead
//      of FLOW_BAND *2.
// 
//  Revision: 018   By: rhj    Date: 07-Nov-2008   SCR Number: 6435
//  Project:  840S
//  Description:
//       Leak Compensation project-related changes.
//       Increase the flow band when leak comp is enabled.
// 
//  Revision: 017  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 016  By:  iv    Date:  18-Sep-1997     DR Number:DCS 1649
//       Project:  Sigma (R8027)
//       Description:
//             Deleted checks for Pe, Pi stuck flags.
//
//  Revision: 015  By:  iv    Date:  18-Jul-1997   DR Number: DCS 2298
//  	Project:  Sigma (R8027)
//		Description:
//             Implemented a running average of MAX_FLOW_SAMPLES
//             samples. 
//
//  Revision: 014  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 013  By:  sp    Date: 1-Apr-1997    DR Number: DCS NONE
//       Project:  Sigma (R8027)
//       Description:
//             Inspection rework.
//
//  Revision: 012  By:  syw    Date: 13-Mar-1997    DR Number: DCS 1780
//       Project:  Sigma (R8027)
//       Description:
//			Changed rExhDryFlowSensor.getValue() to rExhFlowSensor.getDryValue().
//
//  Revision: 011  By:  sp    Date: 24-Oct-1996    DR Number: 1476
//       Project:  Sigma (R8027)
//       Description:
//             Inspection rework.
//
//  Revision: 010  By:  sp    Date: 12-Oct-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Fixed test item number.
//
//  Revision: 009  By:  iv    Date: 12-Oct-1996    DR Number: DCS 1388, 1469
//       Project:  Sigma (R8027)
//       Description:
//             Fixed the triggerCondition to incorporate a check for 
//             PeStuck.
//
//  Revision: 008  By:  iv    Date: 05-Oct-1996    DR Number: DCS 1388, 1469
//       Project:  Sigma (R8027)
//       Description:
//             Fixed the triggerCondition to prevent a FALSE detection of
//             PiStuck during Occlusion.
//
//  Revision: 007  By:  sp    Date: 30-Sep-1996    DR Number:DCS 1388
//       Project:  Sigma (R8027)
//       Description:
//             Added the mechanism to force disconnect.
//
//  Revision: 006  By:  sp    Date:  29-Sep-1996    DR Number:600
//       Project:  Sigma (R8027)
//       Description: 
//             Remove unit test methods.
//
//  Revision: 005  By:  sp    Date:  19-Sep-1996    DR Number:600
//       Project:  Sigma (R8027)
//       Description: 
//             update const MAX_PRESSURE_THRESHOLD to 1.0.
//
//  Revision: 004  By:  sp    Date:  6-May-1996    DR Number:927
//       Project:  Sigma (R8027)
//       Description: 
//             update to the latest disconnect algorithm.
//
//  Revision: 003  By:  sp    Date:  4-Mar-1996    DR Number:674
//       Project:  Sigma (R8027)
//       Description:
//             Remove #ifndef.
//
//  Revision: 002  By:  iv    Date:  22-Jan-1996    DR Number:DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Changed the condition occluded circuit detection to comply
//             with the SRS: occlusionCount_ > OCC_COUNT_LIMIT 
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "DiscAutoResetTrigger.hh"

//@ Usage-Classes
#include "MathUtilities.hh"
#include "MainSensorRefs.hh"
#include "Sensor.hh"
#include "ExhFlowSensor.hh"
#include "ModeTriggerRefs.hh"
#include "SafetyNetRefs.hh"
#include "DisconnectPhase.hh"
#include "DisconnectTrigger.hh"
#include "PiStuckTest.hh"
#include "PeStuckTest.hh"
#include "ExhalationPhase.hh"
#include "PeepController.hh"
#include "PhaseRefs.hh"
#include "ControllersRefs.hh"
#include "LeakCompEnabledValue.hh"
#include "PhasedInContextHandle.hh"
#include "BreathMiscRefs.hh"
#include "LeakCompMgr.hh"
#include "PatientCctTypeValue.hh"

//@ End-Usage

//@ Code...

static const Real32 MIN_PRESSURE_THRESHOLD = -1.5F;
static const Real32 MAX_PRESSURE_THRESHOLD = 1.0F;
static const Real32 OCC_PRESSURE_THRESHOLD = 10.0F;
static const Int32 PRESSURE_COUNT_LIMIT = 100; //msec
static const Int32 FLOW_COUNT_LIMIT = 1000; //msec
static const Int32 OCC_COUNT_LIMIT = 100; //msec
static const Real32 FLOW_BAND = 5.0F; //lpm

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: DiscAutoResetTrigger 
//
//@ Interface-Description
//   Default constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//   The triggerId is set by the base class constructor.
//---------------------------------------------------------------------
//@ PreCondition
//   FLOW_BAND = 0.5 * IDLE_FLOW
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
DiscAutoResetTrigger::DiscAutoResetTrigger(void) 
: ModeTrigger(DISCONNECT_AUTORESET)
{
  CALL_TRACE("DiscAutoResetTrigger()");
  // $[TI1]

  CLASS_ASSERTION( IsEquivalent(FLOW_BAND, 0.5F * IDLE_FLOW, ::HUNDREDTHS) );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~DiscAutoResetTrigger 
//
//@ Interface-Description
//		Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
DiscAutoResetTrigger::~DiscAutoResetTrigger(void)
{
  CALL_TRACE("~DiscAutoResetTrigger()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: enable
//
//@ Interface-Description
//     This method takes no argument and has no return value.
//     By setting the isEnabled_ data member to true, this trigger becomes 
//     enabled, and is able to detect the trigger condition it is set up 
//     to monitor.  The object enabling this trigger must contain the 
//     algorithm for determining when it should be active.
//---------------------------------------------------------------------
//@ Implementation-Description
//     The state data member isEnabled_ is set to true, activating this 
//     trigger. The rest of the data members are reset to 0.
//---------------------------------------------------------------------
//@ PreCondition
//     none
//---------------------------------------------------------------------
//@ PostCondition
//     none
//@ End-Method
//=====================================================================
void
DiscAutoResetTrigger::enable(void)
{
  CALL_TRACE("enable(void)");

  // $[TI1]
  isEnabled_ = TRUE;
  flowCount_ = 0;
  occlusionCount_ = 0;
  minPressureCount_ = 0;
  maxPressureCount_ = 0;
  flowIndex_ = 0;
  idleFlowSum_ = 0.0F;
  exhFlowSum_ = 0.0F;
  for (Int8 ii = 0; ii < MAX_FLOW_SAMPLES; ii++)
  {
     idleFlowSample_[ii] = 0.0F;
     exhFlowSample_[ii] = 0.0F;
  }
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
 
void
DiscAutoResetTrigger::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
                   const char*        pFileName,
                   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, DISCAUTORESETTRIGGER,
                          lineNumber, pFileName, pPredicate);
}



//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: triggerCondition_
//
//@ Interface-Description
//    This method takes no parameters.  If the trigger condition has 
//    occured, the method returns true, otherwise, the method returns 
//    false.
//---------------------------------------------------------------------
//@ Implementation-Description
//    $[04148] 
//    The following conditions are check:
//    (1a) average expiratory flow is  within FLOW_BAND LPM of the average
//        idle flow for FLOW_COUNT_LIMIT duration
//    (1b) If Leak Compensation is enabled, average expiratory flow is  
//         within twice the FLOW_BAND LPM of the average idle flow for 
//         FLOW_COUNT_LIMIT duration
//    (2) both the inspiratory and expiratory pressure transducers reading 
//        less than MIN_PRESSURE_THRESHOLD cmH2O for PRESSURE_COUNT_LIMIT
//    (3) both the inspiratory and expiratory pressure transducers reading 
//        more than MAX_PRESSURE_THRESHOLD cmH2O for PRESSURE_COUNT_LIMIT
//    (4) inspiratory pressure is greater or equal to OCC_PRESSURE_THRESHOLD
//        for OCC_COUNT_LIMIT duration
//    If any of the above conditions is detected, this method returns true.
//    otherwise returns false.
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
Boolean
DiscAutoResetTrigger::triggerCondition_(void)
{
   CALL_TRACE("triggerCondition_(void)");

   Real32 exhPressure = ((Sensor&)RExhPressureSensor).getValue();		
   Real32 inspPressure = ((Sensor&)RInspPressureSensor).getValue();		
   Real32 exhFlow = RExhFlowSensor.getDryValue();
   Real32 idleFlow = ((Sensor&)RAirFlowSensor).getValue() + 
                          ((Sensor&)RO2FlowSensor).getValue();

   Boolean retVal = FALSE;

   idleFlowSum_ += (idleFlow - idleFlowSample_[flowIndex_]);
   exhFlowSum_ += (exhFlow - exhFlowSample_[flowIndex_]);
   
   idleFlowSample_[flowIndex_] = idleFlow;
   exhFlowSample_[flowIndex_] = exhFlow;

   Real32 idleFlowAvg = idleFlowSum_ / MAX_FLOW_SAMPLES;
   Real32 exhFlowAvg = exhFlowSum_ / MAX_FLOW_SAMPLES;

   flowIndex_ = (++flowIndex_) % MAX_FLOW_SAMPLES;

   Boolean condition1 = FALSE;

   // $[LC24040] Increase the flow band when leak compensation is enabled and 
   // the circuit type is not neonatal.
   const DiscreteValue  CIRCUIT_TYPE =
	   PhasedInContextHandle::GetDiscreteValue(SettingId::PATIENT_CCT_TYPE);

   if (RLeakCompMgr.isEnabled() && (CIRCUIT_TYPE != PatientCctTypeValue::NEONATAL_CIRCUIT))
   {
       condition1 = (ABS_VALUE(exhFlowAvg - idleFlowAvg) <= (FLOW_BAND *2.0f));
   }
   else
   {
	   condition1 = (ABS_VALUE(exhFlowAvg - idleFlowAvg) <= FLOW_BAND);
   }

   // condition 1
   if (condition1)
   {
      // $[TI1]
      flowCount_ += CYCLE_TIME_MS;
   }
   else  
   {
      // $[TI2]
      flowCount_ = 0;
   }

   // condition 2
   if ((exhPressure < MIN_PRESSURE_THRESHOLD) && (inspPressure < MIN_PRESSURE_THRESHOLD))
   {
      // $[TI3]
      minPressureCount_ += CYCLE_TIME_MS;
   }
   else
   {
      // $[TI4]
      minPressureCount_ = 0;
   }

   // condition 3
   if ((exhPressure > MAX_PRESSURE_THRESHOLD) && (inspPressure > MAX_PRESSURE_THRESHOLD))
   {
      // $[TI11]
      maxPressureCount_ += CYCLE_TIME_MS;
   }
   else
   {
      // $[TI12]
      maxPressureCount_ = 0;
   }

   // condition 4
   if (inspPressure > OCC_PRESSURE_THRESHOLD)
   {
      // $[TI9]
      occlusionCount_ += CYCLE_TIME_MS;
   }
   else
   {
      // $[TI10]
      occlusionCount_ = 0;
   }

   if ( (minPressureCount_ > PRESSURE_COUNT_LIMIT) || 
	(maxPressureCount_ > PRESSURE_COUNT_LIMIT) ||
	(flowCount_ >= FLOW_COUNT_LIMIT) ||
	(occlusionCount_ > OCC_COUNT_LIMIT) )
   {
      // $[TI5]
      retVal = TRUE;
   }
   // $[TI6]


   //FALSE: $[TI7]
   //TRUE: $[TI8]
   return (retVal);
}


//=====================================================================
//
//  Private Methods...
//
//=====================================================================

