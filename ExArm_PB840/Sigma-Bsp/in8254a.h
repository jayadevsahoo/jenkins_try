/***************************************************************************
*
*		Copyright (c) 1995 MICROTEC RESEARCH INCORPORATED.
*
*	All rights reserved. MICROTEC RESEARCH's source code is an unpublished
*	work and the use of a copyright notice does not imply otherwise.
*	This source code contains confidential, trade secret material of
*	MICROTEC RESEARCH. Any attempt or participation in deciphering,
*	decoding, reverse engineering or in any way altering the source code
*	is strictly prohibited, unless the prior written consent of
*	MICROTEC RESEARCH is obtained.
*
*
*	Module Name:		%M%
*
*	Identification:		%Z% %I% %M%
*
*	Date:			%G%  %U%
*
****************************************************************************
*/

#ifndef _in8254A_H_
#define _in8254A_H_

#include <logio.h>      /* int enable/disable */
#include <timer.h>      /* generic timer specific values */
#include <vbsp.h>       /* VBSP return codes and typedefs */

/* in8254 register bit defitions */
#define	PIT_MODE_0     	0x00		/* counter mode 0 */
#define	PIT_MODE_1     	0x02		/* counter mode 1 */
#define	PIT_MODE_2     	0x04		/* counter mode 2 */
#define	PIT_MODE_3     	0x06		/* counter mode 3 */
#define	PIT_MODE_4     	0x08		/* counter mode 4 */
#define	PIT_MODE_5     	0x0a	 	/* counter mode 5 */

#define	PIT_LATCH      	0x00		/* counter latch command   */
#define	PIT_RW_LAST    	0x10		/* r/w least signif. byte  */
#define	PIT_RW_MOST    	0x20		/* r/w most signf. byte    */
#define	PIT_RW_BOTH    	0x30		/* r/w lsb first, msb next */

#define	PIT_COUNTER_0  	0x00		/* select counter 0 */
#define	PIT_COUNTER_1  	0x40		/* select counter 1 */
#define	PIT_COUNTER_2  	0x80		/* select counter 2 */

#define	PIT_BCD       	0x01		/* BCD count        */

/* timer maximum values/size of timer */
#define TIMER_MAX	0xFFFF		/* 16 bits */

/* register addresses for the timer */
typedef struct {
   volatile unsigned char *in8254_cnt;  
   volatile unsigned char *in8254_cntrl;  
} in8254_reg_map_t;

/* register addresses for the timer */

typedef struct {
   unsigned char channel;
   unsigned char mode;
   unsigned char flag_int_enabled;
} in8254_specific_t;

/* timer device descriptor */

typedef struct {
   timer_config_t    base_config;
   extern_fops_t     externio;
   in8254_reg_map_t  regs;
   in8254_specific_t specific_cfg;
} in8254_config_t;

/* function prototypes */

vbsp_return_t in8254_install(in8254_config_t *, timer_dev_desc_t *);
vbsp_return_t in8254_init(struct timer_dev_desc_t *);
vbsp_return_t in8254_enable(timer_dev_desc_t *);
vbsp_return_t in8254_disable(timer_dev_desc_t *);
vbsp_return_t in8254_status(timer_dev_desc_t *, int *);
vbsp_return_t in8254_set_ns_per_count(timer_dev_desc_t *, unsigned long *);
vbsp_return_t in8254_get_ns_per_count(timer_dev_desc_t *, unsigned long *);
vbsp_return_t in8254_set_preload(timer_dev_desc_t *, unsigned long *);
vbsp_return_t in8254_get_preload(timer_dev_desc_t *, unsigned long *);
vbsp_return_t in8254_get_rollover(timer_dev_desc_t *, unsigned long *);
vbsp_return_t in8254_read_timer(timer_dev_desc_t *, unsigned long *);
vbsp_return_t in8254_set_timer(timer_dev_desc_t *, unsigned long *);
int in8254_test(logio_interrupt_entry_t *);
vbsp_return_t in8254_interrupt_status(timer_dev_desc_t *, int *);

#endif /* _in8254A_H_ */


