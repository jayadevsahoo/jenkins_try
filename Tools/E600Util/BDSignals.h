#pragma once

#define NUM_BD_SIGNALS				30
#define MAX_SIGNAL_NAME_LENGTH		20
#define MAX_SELECTED_SIGNALS		18
#define MAX_SIG_NAME_LENGTH			20
#define MAX_GROUP_NAME_LENGTH		20

typedef struct BDSignal
{
	wchar_t Name[MAX_SIG_NAME_LENGTH];
	bool Default;
} BD_SIGNAL;

extern const TCHAR *DEFAULT_SIGNALS[];
extern const TCHAR *DEFAULT_ENGINEERING_SIGNALS[];
extern CString GlobalHeaderString;

extern void ParseCSVHeader(CString HeaderStr, vector<CString> &SignalsArr);
extern void SwapSignalStrings(int FirstSig, vector<CString> &Signals);
extern void SetGlobalHeaderString(vector<CString>Signals);