#ifndef	TimeDay_HH
# define TimeDay_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// =========================== M O D U L E   D E S C R I P T I O N ====
//@ Filename: TimeDay.hh - Time of Day functionality header
//---------------------------------------------------------------------
//@ Interface-Description
//  Sigma Time of Day (TOD) OS or HW clock interface (whichever used in the platform).
//  Port this interface to the source of RTC used in the platform
//---------------------------------------------------------------------
//@ Rationale
//  Isolate low-level target-specific time-of-day hardware or OS from
//  application code. A higher level TimeStamp class interfaces between this
//  low-level interface and the application layer and forms an abstract
//  TimeStamp with necessary APIs
//---------------------------------------------------------------------
//@ Version-Information
// @ $Header:   /840/Baseline/Operating-System/vcssrc/TimeDay.hhv   10.0.5.0   06/22/06 11:40:54   pvcs  $
//
//@ Modification-Log
//
//   Revision 001   By: Gary Cederquist Date: 28-MAY-1997 DR Number: 2176
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version.
//
//=====================================================================

# include "Sigma.hh"
# include "MemoryMap.hh"

struct	TimeOfDay
{
	Uint8	year;			// 0..99
	Uint8	month;			// 1..12
	Uint8	date;			// 1..31
	Uint8	hour;			// 0..23, military-style
	Uint8	minute;			// 0..59
	Uint8	second;			// 0..59
	Uint16	milliseconds;	// 0..999
};

//@ Type: YearType
//  Used for setting and comparing years.
enum
{
  BASE_CENTURY = 2000,		//used as a base for the 2-digit year to get a full 4 digits
  START_EPOCH_YEAR = 00,	//using only 2 dec digits because TimeOfDay.year is Uint8
  MAX_EPOCH_YEAR = 99,		//using only 2 dec digits because TimeOfDay.year is Uint8
};

//@ Begin-Free-Function

void	SetTimeOfDay(const TimeOfDay * const time);
void	ReadTimeOfDay(TimeOfDay * const time);
Boolean WaitTicks(Uint16 nticks);

//@ End-Free-Function

#endif	// TimeDay_HH
