#ifndef  BOOTP_HH
#define	BOOTP_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1994, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
//@ Filename: BOOTP - provides BOOTP protocol to determine
//   BOOTP server IP address and BOOTP client IP Address.
//---------------------------------------------------------------------
//@ Version
//@(#) $Header:   /840/Baseline/Download/vcssrc/BOOTP.hhv   25.0.4.0   19 Nov 2013 14:02:46   pvcs  $ 
//@ Modification-Log
//
//  Revision: 001  By:  <jhv>    Date:  05-Jan-96    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//====================================================================

#include "DownloadCons.hh"

	enum BOOTPOpCodes
	{
		BOOTP_MIN_OP = 0x70,
		BOOTP_REQUEST = BOOTP_MIN_OP,
		BOOTP_REPLY,
		BOOTP_MAX_OP = BOOTP_REPLY
	};

	typedef struct BMsg
	{
		Uint32  opCode;
		TargetTypes targetType;
		Uint32  ClientIpAddr;
		Uint32  ServerIpAddr;
		Uint8   EtherAddr[8];
		char	bootFileName[MAX_FILE_NAME_LENGTH];
	} BOOTPMsg;

#endif // BOOTP_HH 
