#ifndef Compressor_HH
#define Compressor_HH
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: Compressor - implements the functionality required for
//                     interfacing with the compressor device.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/Compressor.hhv   25.0.4.0   19 Nov 2013 13:54:00   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005  By:  syw    Date:  28-Jul-1999    DR Number: 5416
//  Project:  840
//  Description:
//		Eliminate callbacks since EventFilter class called explicitly.
//
//  Revision: 004  By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 003  By:  syw    Date:  28-Sep-1998    DR Number: DCS 5181
//       Project:  BiLevel
//       Description:
//			Added service mode methods.
//
//  Revision: 002  By:  syw    Date:  28-Oct-1996    DR Number: DCS 2592
//       Project:  Sigma (R8027)
//       Description:
//			Added antiStallTimer_ and performAntiStallManeuver_ data members.
//
//  Revision: 001  By:  by    Date:  11-Nov-1996    DR Number:
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "Sigma.hh"
#include "BD_IO_Devices.hh"

//@ Usage-Classes
#include "BinaryIndicator.hh"
#include "BinaryCommand.hh"
// E600 BDIO #include "CompressorEeprom.hh"

//@ End-Usage

class Compressor
{

  public:

	// allow service mode to access private methods
	friend class SmGasSupply ;
	friend class SmLeakTest ;
	friend class SmFlowTest ;
// E600 BDIO     friend class SmCompressorEeprom ;
	friend class GasSupplyTest ;
	friend class FsCrossCheckTest ;
	friend class CompressorTest ;
	friend class CompressorLeakTest ;

    enum CompressorStatus {    OFF = 0, ENABLED, STANDBY, RUN };
    enum CompReadyStatus { INIT = 0, READY, NOT_READY };

    Compressor(void) {}
    ~Compressor(void) {}

    static void SoftFault( const SoftFaultID softFaultID,
						   const Uint32      lineNumber,
						   const char*       pFileName  = NULL,
						   const char*       pPredicate = NULL);

    Boolean checkOverTemperature( void )  {return FALSE;}
    Boolean isCompressedAirPresent( void ) const  {return FALSE;}
    Boolean isInstalled( void ) const  {return FALSE;}
    Boolean isCompressedAirUsable( void ) const  {return FALSE;}

    Compressor::CompressorStatus getCompressorStatus( void ) const  {return OFF;}

    void enableCompressor( void ) {}
    void disableCompressor( void ) {}

    void setCompressorMotorOn( void ) {}
    void setCompressorMotorReady( void ) {}

    Boolean checkCompressedAirPresence( void ) {return FALSE;}
    Boolean checkCompressorInstalled( void ) {return FALSE;}

    void initialize( void ){}
    void newCycle( void ){}

// E600 BDIO     static CompressorEeprom::errorStatus UpdateTotalElapsedTime( void );
    static Compressor::CompressorStatus UpdateTotalElapsedTime( void );
    void initialStatusNotification( void ){}

  protected:

  private:

    Compressor( const Compressor& );        // not implemented...
    void operator=( const Compressor& );    // not implemented...

    void verifyCompressorSerialNumber_( void ) {}
    Boolean smCheckCompressedAirPresence_( void)  {return FALSE;}
    Boolean smCheckCompressorInstalled_( void)  {return FALSE;}
    void smEnableCompressor_( void) {}
    void smSetCompressorMotorReady_( void) {}
    void smSetCompressorMotorOn_( void) {}
    void smDisableCompressor_( void) {}

// E600 BDIO
#if 0
    //@ Data-Member: operationStatus_
    // indicates the state of the compressor operation
    CompressorStatus operationStatus_;

    //@ Data-Member: compressorReadyStatus_
    // indicates the readiness of the compressor used to inform GUI of vent status
    CompReadyStatus compressorReadyStatus_;

    //@ Data-Member: isInstalled_
    // indicates whether or not compressor is installed
    Boolean isInstalled_;

    //@ Data-Member: isAirPresent_
    // indicates whether air is present
    Boolean isAirPresent_;

    //@ Data-Member: compInstalled_
    // provides the I/O interface to check if compressor is installed
    BinaryIndicator  compInstalled_;

    //@ Data-Member: compAirPresent_
    // provides the I/O interface to check if compressed air present
    BinaryIndicator  compAirPresent_;

    //@ Data-Member: compOverTemperature_
    // provides the I/O interface to check if the compressor is over temperature
    BinaryIndicator  compOverTemperature_;

    //@ Data-Member: compRunStby_
    // motor run = 1, motor standby = 0
    BinaryCommand  compRunStby_;

    //@ Data-Member: compOff_
    // compressor off = 0, compressor enabled = 1
    BinaryCommand  compOff_;

    //@ Data-Member: compressorOpMinAtPowerup_
    //  indicates the compressor operational minutes at power-up
    static Uint32 CompressorOpMinAtPowerup_;

    //@ Data-Member: antiStallTimer_
    // timer to disable the compressor in the anti-stall algorithm
    Uint32 antiStallTimer_ ;

	//@ Data-Member: performAntiStallManeuver_
	// set to perform the anti stall maneuver
	Boolean performAntiStallManeuver_ ;
// E600 BDIO
#endif

};


// Inlined methods...
// E600 BDIO #include "Compressor.in"

#endif // Compressor_HH
