#pragma once
#include "Resource.h"
#include "ClientSocket.h"
#include "TabTwo.h"
#include "TabThree.h"
#include "BDSignals.h"

#define BD_BROADCAST_MSG_ID					9925
#define BD_SIGNALS_RESPONSE_MSG_ID			9926
#define BD_RESPONSE_MSG_ID					9927
#define BD_PING_MSG_ID						9928
#define BD_CAL_STATUS_MSG_ID				9929
#define BD_ALL_GAINS_REPLY_MSG_ID			9930
#define BD_READ_DEBUG_REPLY_MSG_ID			9831

#define PC_DISABLE_BROADCAST_MSG_ID			9800
#define PC_ENABLE_BROADCAST_MSG_ID			9801
#define PC_SET_SIGNALS_MSG_ID				9802

#define PC_CALIBRATION_MSG_ID				9810
#define PC_CAL_CANCEL_MSG_ID				9811
#define PC_SEND_SETTINGS_MSG_ID				9812
#define PC_REQUEST_SETTINGS_MSG_ID			9813
#define PC_SEND_SETTINGS_DEBUG_MSG_ID		9814
#define PC_TEST_MSG_ID						9815

#define PC_REQUEST_SIGNALS_MSG_ID			9826
#define PC_SET_SAMPLE_TIME_MSG_ID			9827
#define PC_PING_MSG_ID						9828

#define PC_SET_PC_GAIN_MSG_ID				9850
#define PC_SET_FC_GAIN_MSG_ID				9851
#define PC_SET_EV_GAIN_MSG_ID				9852
#define PC_SET_PEEP_GAIN_MSG_ID				9853
#define PC_SET_PID_GAIN_MSG_ID				9854
#define PC_SET_PBP_GAIN_MSG_ID				9855
#define PC_SET_ALL_GAINS_MSG_ID				9856
#define PC_GET_PC_GAIN_MSG_ID				9857
#define PC_GET_FC_GAIN_MSG_ID				9858
#define PC_GET_EV_GAIN_MSG_ID				9859
#define PC_GET_PEEP_GAIN_MSG_ID				9860
#define PC_GET_PID_GAIN_MSG_ID				9861
#define PC_GET_PBP_GAIN_MSG_ID				9862
#define PC_GET_ALL_GAINS_MSG_ID				9863
#define PC_ENABLE_GAIN_MSG_ID				9864
#define PC_WRITE_DEBUG_MSG_ID				9865
#define PC_READ_DEBUG_MSG_ID				9866

#define PC_DEBUG_COMMAND_SIZE				2

#define NUMBER_PC_COMMANDS	7

typedef struct GroupData
{
	CString GroupName;
	vector <CString>Signals;
}GROUP_DATA_TYPE;

// CTabOne dialog

class CTabOne : public CDialog
{
	DECLARE_DYNAMIC(CTabOne)

public:
	CTabOne(CWnd* pParent);   // standard constructor
	virtual ~CTabOne();
	CClientSocket m_Client;
	CTabTwo *pTabTwo;
	CTabThree *pTabThree;
	CWnd* pParentWnd;

	CStatusBar *pStatusBar;

// Dialog Data
	enum { IDD = IDD_DIALOG_TAB1 };

	int CTabOne::SendPacketToBD(char *pBuff, int Size);

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	BOOL OnInitDialog();
	void ProcessDataPacket(int DataSize, char *pPacketData);

	bool ReadGroupNamesFromFile(bool bCloseFile);
	void ReadGroupSignals(GROUP_DATA_TYPE SigGroup);
	void CreateNewGroupFile(CString GroupName, const TCHAR *GroupSignals[]);
	bool ReadSelectedSignals();
	void PopulateSignalLists();
	bool FindGroupFromFileAndFillList(CString GroupName);
	bool FindGroupAndFillList(CString GroupName);
	bool IsSignalAvailable(const TCHAR *Signals, const vector<CString> SelectedSignals);
	CString ReadGroupSignals(vector<CString> &Signals);
	void ReadSelectableSignalsFromFile();
	void UpdateGlobalHeaderString();
	int LoadButtonBitmap(DWORD ButtonID, LPCWSTR Path, DWORD BitmapID);
	UINT16 GetSignalNumber(const CString& SigName);
	bool SetSelectedSignalNumbers();
	void PopulateAvailSignalsList();

	int SendCmdMessage(wchar_t *pBuff, int Size);

	CFile CaptureFile;
	CStdioFile SelectedSigsFile;
	CStdioFile GroupsFile;
	CStdioFile SelectableSigsFile;

	vector<CString>GroupNames;
	vector<CString>SelectableSignals;
	vector<CString>AvailSignals;
	vector<CString>SelectedSignals;
	vector<CString>EngineeringSignals;
	
	UINT16 SelectedSignalNumbers[MAX_SELECTED_SIGNALS];
	UINT16 BDSignalNumbers[MAX_SELECTED_SIGNALS];
	list<GROUP_DATA_TYPE>GroupList;

	int NumSelectableSignals;

	CString SelectableSignalsFileName;
	CString SignalsFileName;
	CString GroupsFileName;
	CString SelectedGroup;
	CString IPAddrStr;
	CString HeaderStr;
	
	BOOL bCapture;
	BOOL bMonitor;
	BOOL bConnect;
	BOOL bSelectedSignalsChanged;
	BOOL bGroupChanged;

	int ParamValue;

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnPaint();
	afx_msg void OnKickIdle();
	afx_msg void OnBnClickedButtonConnect();
	afx_msg void OnBnClickedButtonSend();
	afx_msg void OnBnClickedButtonCapture();
	afx_msg void OnBnClickedButtonMonitor();
	afx_msg LRESULT ReceiveUdpData(WPARAM x, LPARAM y);
	afx_msg LRESULT CheckAppButtons(WPARAM x, LPARAM y);
	afx_msg LRESULT OnShutdown(WPARAM x, LPARAM y);
	afx_msg LRESULT SendCalCommand(WPARAM x, LPARAM y);
	afx_msg LRESULT SendSettingCommand(WPARAM x, LPARAM y);
	afx_msg LRESULT SendGainCommand(WPARAM x, LPARAM y);

	afx_msg void OnUpdateButtonConnect(CCmdUI* pCmdUI);
	afx_msg void OnUpdateButtonCapture(CCmdUI* pCmdUI);
	afx_msg void OnBnClickedCheckTcp();
	afx_msg void OnBnClickedButtonAddSig();
	afx_msg void OnBnClickedButtonRemoveSig();
	afx_msg void OnBnClickedButtonMoveUp();
	afx_msg void OnBnClickedButtonMoveDown();
	afx_msg void OnBnClickedButtonCreateGroup();
	afx_msg void OnBnClickedButtonPopulate();
	afx_msg void OnBnClickedButtonModify();
	afx_msg void OnBnClickedButtonDelete();
	afx_msg void OnBnClickedButtonBrowse();
	afx_msg void OnBnClickedButtonDownloadFile();
	afx_msg void OnBnClickedButtonUpdateSignals();
	afx_msg void OnBnClickedButtonClearSignals();
	afx_msg void OnBnClickedButtonDloadBrowse();
	afx_msg void OnBnClickedCheckInEngineering();
	afx_msg void OnBnClickedCheckTabSepartated();
};
