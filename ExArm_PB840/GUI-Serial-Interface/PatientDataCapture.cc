#include "stdafx.h"
//====================================================================
// This is a proprietary work to which Nellcor Puritan Bennett
// corporation of California claims exclusive right.  No part of this
// work may be used, disclosed, reproduced, sorted in an information
// retrieval system, or transmitted by any means, electronic,
// mechanical, photocopying, recording, or otherwise without the prior
// written permission of Nellcor Puritan Bennett Corporation.
//
//     Copyright (c) 1997, Nellcor Puritan Bennett Corporation
//=====================================================================

// =========================== M O D U L E   D E S C R I P T I O N ====
//@ Filename: PatientDataCapture - Patient data area screen capture
//---------------------------------------------------------------------
//@ Interface-Description
//	This class is derived from ScreenCapture and provides for the
//	capture of pixel data from the patient data area.
//---------------------------------------------------------------------
//@ Rationale
//	Provides specific handling of pixel capture from the given screen area.
//---------------------------------------------------------------------
//@ Implementation-Description
//	The method GetPatientDataCapture() is provided to instantiate a
//	PatientDataCapture object and also to access this object.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness of 
//  parameterized data.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Serial-Interface/vcssrc/PatientDataCapture.ccv   25.0.4.0   19 Nov 2013 14:12:44   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002    By: quf         Date: 24-Sep-2001  DR Number: 5942
//  Project: GuiComms
//  Description:
//  Code/comment/header cleanup.
//
//  Revision: 001    By: gdc         Date: 23-Jan-2001  DR Number: 5493
//  Project: GuiComms
//  Description:
//      Add GuiComms functionality.
//=====================================================================

#include "Sigma.hh"
#include "PatientDataCapture.hh"

//@ Usage-Classes
#include "BaseContainer.hh"
#include "GuiFoundation.hh"
#include "DisplayContext.hh"
#include "Colors.hh"
//@ End-Usage

//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: PatientDataCapture [constructor]
//
//@ Interface-Description
//  Constructor for the PatientDataCapture class.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
 
PatientDataCapture::PatientDataCapture( Uint32* pRasterData, 
							  Uint32  sizeofRasterData,
							  DisplayContext& rDisplayContext,
							  Int32 x0, Int32 y0,
	                          Uint32 xWidth, Uint32 yHeight )
	: ScreenCapture(pRasterData,
		  sizeofRasterData,
		  rDisplayContext,
		  x0, y0,
		  xWidth, yHeight)
{
// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~PatientDataCapture [destructor]
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
 
PatientDataCapture::~PatientDataCapture()
{
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: filterPixel_
//
//@ Interface-Description
//	Convert the input screen pixel value to the appropriate printer
//	pixel value and return this value.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Byte
PatientDataCapture::filterPixel_(Byte pixel) const
{
// $[TI1]
  return    (pixel == Colors::WHITE)
	     || (pixel == Colors::WHITE_ON_EXTRA_DARK_GREY_BLINK);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetPatientDataCapture [static]
//
//@ Interface-Description
//  Defines and returns a reference to the patient data area capture
//  object.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
PatientDataCapture &
PatientDataCapture::GetPatientDataCapture(void)
{
// $[TI1]
  static const Uint32 AREA_HEIGHT_ = 63;
  static Uint32 RasterData_[(GuiFoundation::SCREEN_WIDTH_PIXELS+31)/32*AREA_HEIGHT_];
  static PatientDataCapture PatientDataCapture_(
	  RasterData_,
	  sizeof(RasterData_),
      GuiFoundation::GetUpperBaseContainer().getDisplayContext(),
	  0,		// x0
	  0,		// y0
	  GuiFoundation::SCREEN_WIDTH_PIXELS,
	  AREA_HEIGHT_);

  return PatientDataCapture_;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//@ Interface-Description
// Implicitly gain execution control when a software assertion macro
// detects and error.
//---------------------------------------------------------------------
//@ Implementation-Description
// Automatically invoked when CLASS_ASSERTION and CLASS_PRE_CONDITON
// macros (among others) detect a fault.  We just call FaultHandler::SoftFault
// to display pertinent information to the user.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
 
void    
PatientDataCapture::SoftFault(  const SoftFaultID softFaultID
                           , const Uint32      lineNumber
                           , const char *      pFileName
                           , const char *      pPredicate )
{
    FaultHandler::SoftFault(softFaultID, GUI_SERIAL_INTERFACE,
		GUI_Serial_Interface::PATIENT_DATA_CAPTURE_CLASS, 
		lineNumber, pFileName, pPredicate);
}
