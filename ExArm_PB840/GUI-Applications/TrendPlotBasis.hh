
#ifndef TrendPlotBasis_HH
#define TrendPlotBasis_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: TrendPlotBasis
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/TrendPlotBasis.hhv   25.0.4.0   19 Nov 2013 14:08:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By:  gdc	   Date:  25-Jan-2007    SCR Number: 6237
//  Project:  TREND
//  Description:
//		Trend project inital version.
//
//====================================================================

#include "LargeContainer.hh"
#include "Line.hh"
#include "TextUtil.hh"
#include "TrendDataSet.hh"


class TrendPlotBasis : public LargeContainer
{
public:
	TrendPlotBasis();
	virtual ~TrendPlotBasis(void);
	virtual void activate(void);
	virtual void erase(void);
	virtual void setGridArea(Uint32 x, Uint32 y, Uint32 width, Uint32 height);
	inline void  showXAxisValues(Boolean displayXValues = TRUE);
	inline void  setShowEvents(Boolean showEvents);
	void setY1Limits(Real32 upperY1Limit, Real32 lowerY1Limit);

	static void SoftFault(const SoftFaultID softFaultID,
							const Uint32      lineNumber,
							const char*       pFileName  = NULL, 
							const char*       pPredicate = NULL);


protected:
	inline Int32 y1ValueToPoint_(Real32 yValue);
	void autoScaleYAxis(const TrendDataSet& rTrendDataSet, Uint16 dataColumn);
	void updateXAxis(const TrendDataSet& rTrendDataSet);

	//@ Data-Member: plotContainer_
	// Plot container which contains the grid lines. The Trend plot
	// will be plotted here. Used to quickly clear this area.
	Container plotContainer_;

	//@ Data-Member: plotLine_
	// The line used to draw each plot segment in the plotContainer_
	Line plotLine_;

	//@ Data-Member: eventsContainer_
	// Container which contains the event markers. Used to quickly clear
	// this area.
	Container eventsContainer_;

	//@ Data-Member: eventMarker_
	// The line used to draw each event marker below the plotContainer_
	Line eventMarker_;

	//@ Data-Member: lastXPixel_
	// The last valid X pixel coordinate used when plotting the trend plot.
	Uint32 lastXPixel_;

	//@ Data-Member: lastYPixel_
	// The last valid Y pixel coordinate used when plotting the trend plot.
	Uint32 lastYPixel_;

	//@ Data-Member: isDataNull_
	// True when no data is in graph. Plot cannot be dynamically scaled.
	// No timestamps can be extracted.
    Boolean isDataNull_;

	//@ Data-Member: showEvents_
	// enables/disables display of event markers
	Boolean showEvents_;

private:
	// these methods are purposely declared, but not implemented...
	TrendPlotBasis(const TrendPlotBasis&);		// not implemented...
	void   operator=(const TrendPlotBasis&);	// not implemented...
	void calculateScaleFactors_(void);
	virtual void layoutGrid_(void);
	inline void gridChanged_(void);


	enum
	{
		MAX_GRID_LINES_ = 20
	};

	//@ Data-Member: displayXValues_
	// A flag which determines if TrendsPlot should label the X axis with
	// the X values.
	Boolean displayXValues_;

	//@ Data-Member: upperYLimit_
    // The upper Y limit, defining the last value on the Y axis at
    // the top left of the grid.
	Real32 upperY1Limit_;

	//@ Data-Member: lowerYLimit_
    // The lower Y limit, defining the first value on the Y axis at
    // the bottom left of the grid.
	Real32 lowerY1Limit_;

    //@ Data-Member: y1ScaleFactor_
	// The scaling factor used for translating Y values to pixel coordinates,
	Real32 y1ScaleFactor_;

	//@ Data-Member: xAxisLine_
	// The line representing the x axis is depicted in a different color
	Line xAxisLine_;

	//@ Data-Member: yAxisLine_
	// The line representing the y axis is depicted in a different color.
	Line yAxisLine_;

	//@ Data-Member: xGridLines_
	// The X grid lines, those lines parallel to the X axis which mark the
	// various divisions of the Y axis.
	Line xGridLines_[MAX_GRID_LINES_];

	//@ Data-Member: yGridLines_
	// The Y grid lines, those lines parallel to the Y axis which mark the
	// various divisions of the X axis.
	Line yGridLines_[MAX_GRID_LINES_];

	//@ Data-Member: timeLabels_
	// The time labels on the x-axis
	TextField timeLabels_[MAX_GRID_LINES_];

	//@ Data-Member: dateLabels_
	// The date labels on the x-axis
	TextField dateLabels_[MAX_GRID_LINES_];

	//@ Data-Member: yValues_
	// The Y values which label the various grid lines along the Y axis.
	TextField yValues_[MAX_GRID_LINES_];

	//@ Data-Member: dataNotAvailableMsg_
	// Displays the Data Not Available message.
	TextField dataNotAvailableMsg_;

	//@ Data-Member: isDataNotAvail_
	// True if data is not available in current installed option set.
    Boolean isDataNotAvail_;

    //@ Data-Member: trendSelectId_
	// Stores the current Trend Select ID for this plot
    TrendSelectValue::TrendSelectValueId trendSelectId_;

};

// Inlined methods
#include "TrendPlotBasis.in"

#endif // TrendPlotBasis_HH 
