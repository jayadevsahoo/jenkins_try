#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: NovRamSequenceNum - NOVRAM Sequence Number Value.
//---------------------------------------------------------------------
//@ Interface-Description
//  This class provides a means of assigning sequence numbers to events
//  over time.  This is used with the various diagnostic log and result
//  table to determine the sequence of events.
//---------------------------------------------------------------------
//@ Rationale
//  Since the user has the ability to modify the date/time of the ventilator,
//  this type of mechanism was the only simple means of keeping track of
//  the sequence of various events, over time.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class protects against multiple threads by using a multi-reader,
//  single-writer semaphore.  This has the effect of a mutual-exclusion
//  semaphore for "write" access, while allowing multiple "read" accesses
//  through when no "write" access is grabbeed.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Persistent-Objects/vcssrc/NovRamSequenceNum.ccv   25.0.4.0   19 Nov 2013 14:18:52   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By:  sah    Date:  01-Jan-1999    DCS Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' to a non-inlined method.
//
//  Revision: 003  By:  sah    Date:  30-Sep-1997    DCS Number: 2530
//  Project:  Sigma (R8027)
//  Description:
//	Fix incorrect handling of corrupt data, by using a 'DoubleBuffer'
//	instance within the class.  Furthermore, this new double-buffered
//	instance must have its own, unique semaphore to prevent being
//	blocked on a (additional) semaphore when retrieving a new
//	sequence number.
//
//  Revision: 002  By:  sah    Date:  03-Jul-1997    DCS Number: 2133
//  Project:  Sigma (R8027)
//  Description:
//       Modify to ensure NMI-safe integrity of sequence number.
//
//  Revision: 001  By:  sah    Date:  15-Mar-1997    DCS Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//          Initial version (Integration baseline).
//
//=====================================================================

#include "NovRamSequenceNum.hh"
#include "NovRamStatus.hh"

//@ Usage-Classes
#include "NovRamSemaphore.hh"
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  NovRamSequenceNum()  [Default Constructor]
//
//@ Interface-Description
//  Construct a default sequence number counter -- with a initial counter
//  value of '0'.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition 
//  (verifySelf() == NovRamStatus::ITEM_VALID)
//@ End-Method 
//===================================================================== 

NovRamSequenceNum::NovRamSequenceNum(void)
		: sequenceNumBuff_(NovRamSemaphore::SEQ_NUM_BUFFER_ACCESS)
{
  CALL_TRACE("NovRamSequenceNum()");

  // update to a zero value...
  sequenceNumBuff_.updateData(0u);

  // this should ALWAYS be true...
  SAFE_CLASS_POST_CONDITION((verifySelf() == NovRamStatus::ITEM_VALID),
  			    NovRamSequenceNum);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ~NovRamSequenceNum()  [Destructor]
//
//@ Interface-Description
//  Destroy this instance.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

NovRamSequenceNum::~NovRamSequenceNum(void)
{
  CALL_TRACE("~NovRamSequenceNum()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  assignNewSequenceNum()
//
//@ Interface-Description
//  Increment the current sequence number, and store the new one.  The
//  current, previously-unused sequence number is returned for use by
//  the caller.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (verifySelf() == NovRamStatus::ITEM_VALID)
//---------------------------------------------------------------------
//@ PostCondition 
//  (verifySelf() == NovRamStatus::ITEM_VALID)
//@ End-Method 
//===================================================================== 

Uint32
NovRamSequenceNum::assignNewSequenceNum(void)
{
  CALL_TRACE("assignNewSequenceNum()");
  SAFE_CLASS_PRE_CONDITION((verifySelf() == NovRamStatus::ITEM_VALID));

  Uint32  newSequenceNum;

  NovRamSemaphore::RequestWriteAccess(NovRamSemaphore::SEQUENCE_NUM_ACCESS);

  // get new sequence number...
  sequenceNumBuff_.getData(newSequenceNum);

  // store next sequence number...
  sequenceNumBuff_.updateData(newSequenceNum+1);

  NovRamSemaphore::ReleaseWriteAccess(NovRamSemaphore::SEQUENCE_NUM_ACCESS);

  SAFE_CLASS_ASSERTION((verifySelf() == NovRamStatus::ITEM_VALID));
  			
  return(newSequenceNum);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  initialize()  [const]
//
//@ Interface-Description
//  A reset sequence occurred, therefore verify the integrity of this
//  instance.  If this value is determined to be valid,
//  'NovRamStatus::ITEM_VERIFIED' is returned.  If a portion of this
//  instance is corrupted such that a valid value can be recovered,
//  the instance is "corrected" and 'NovRamStatus::ITEM_RECOVERED'
//  is returned.  However, if the value is corrupted to a point of no
//  recovery, the instance is initialized via its default constructor,
//  and 'NovRamStatus::ITEM_INITIALIZED' is returned.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition 
//  (verifySelf() == NovRamStatus::ITEM_VALID)
//@ End-Method 
//===================================================================== 

NovRamStatus::InitializationId
NovRamSequenceNum::initialize(void)
{
  CALL_TRACE("initialize()");

  const NovRamStatus::InitializationId  INIT_STATUS =
	  sequenceNumBuff_.initialize(NovRamSemaphore::SEQ_NUM_BUFFER_ACCESS);

  switch (INIT_STATUS)
  {
  case NovRamStatus::ITEM_INITIALIZED :		// $[TI1]
    // run this class's default constructor on this instance...
    new (this) NovRamSequenceNum();
    break;
  case NovRamStatus::ITEM_RECOVERED :
  case NovRamStatus::ITEM_VERIFIED :		// $[TI2]
    // do nothing; instance is left in a valid state...
    break;
  default :
    AUX_CLASS_ASSERTION_FAILURE(INIT_STATUS);
    break;
  };

  SAFE_CLASS_ASSERTION((verifySelf() == NovRamStatus::ITEM_VALID));

  return(INIT_STATUS);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  verifySelf()  [const]
//
//@ Interface-Description
//  Verify that this instance's value is a legal value.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//  To be in a legal state, 'currBufferIndex_' must be valid, as well
//  as both of the buffers, and the buffer's sequence values must be
//  equivalent.
//--------------------------------------------------------------------- 
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition 
//  none
//@ End-Method 
//===================================================================== 

NovRamStatus::VerificationId
NovRamSequenceNum::verifySelf(void)
{
  CALL_TRACE("verifySelf()");

  NovRamSemaphore::RequestReadAccess(NovRamSemaphore::SEQUENCE_NUM_ACCESS);

  const NovRamStatus::VerificationId  VERIF_STATUS =
	 sequenceNumBuff_.verifySelf(NovRamSemaphore::SEQ_NUM_BUFFER_ACCESS);

  NovRamSemaphore::ReleaseReadAccess(NovRamSemaphore::SEQUENCE_NUM_ACCESS);

  return(VERIF_STATUS);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//            [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//  and 'lineNumber' are essential pieces of information.  The
//  'pFileName' and 'pPredicate' strings may be defaulted in the macro
//  to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//  none 
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

void
NovRamSequenceNum::SoftFault(const SoftFaultID  softFaultID,
			 const Uint32       lineNumber,
			 const char*        pFileName,
			 const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, PERSISTENT_OBJS, NOV_RAM_SEQUENCE_NUM,
                          lineNumber, pFileName, pPredicate);
}
