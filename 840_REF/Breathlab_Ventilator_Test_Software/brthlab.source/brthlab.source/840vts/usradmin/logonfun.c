/*****************************************************************************/
/*   Module Name:    logonfun.c                                                
/*   Purpose:   Functions used to manage logon names/users                                            
/*                                                                           
/*   Requirements:   This module implements requirements detailed in         
/*          <75359-93>                                                       
/*
/*   Modification History:
/*   
/*   Description of Change			             By			      Date 
/*   ---------------------				    	-----   		  ------
/*   The SetPTSLabels(0) 					    Dwarkanath(CGS)   22 Jan,2001  
/*	 called in Logon_Callback function 
/*   are  removed.
/*
/*	 Critical Observation during Phase1
/* 	 is incorportated.						    Kathirvel(CGS)    27 Sep,2000
/*   	
/*   Set command button color to transparent    Dave Richardson   25 Nov, 2002
/*   to make square outline of rectangular 
/*   region around round buttons disappear
/*
/*   Redesign round command buttons so 
/*   background color more closely matches
/*   panel color
/*
/*   $Revision::   1.2      $                                                
/*
/*   $Log:   //pleasa1apo2/swqa/data/arc/840_CPVT_VTS/Program/usradmin/logonfun.c-arc  $
 * 
 *    Rev 1.2   04 Dec 2002 15:37:14   Buchman
 * 840 VTS build done
 * 
 *    Rev 1.1   25 Nov 2002 11:50:56   Richardson
 * Updated per SCR # 24

/*****************************************************************************/

/*****************************************************************************
THIS IS A PROPRIETARY WORK TO WHICH NELLCOR PURITAN BENNETT CORPORATION CLAIMS
EXCLUSIVE RIGHT.  NO PART OF THIS WORK MAY BE USED, DISCLOSED, REPRODUCED,
STORED IN AN INFORMATION RETIEVAL SYSTEM, OR TRANSMITTED, IN ANY FORM OR BY ANY
MEANS ELECTRONIC, MECHANICAL, PHOTOCOPYING, RECORDING OR OTHERWISE WITHOUT THE
PRIOR WRITTEN PERMISSION OF NELLCOR PURITAN BENNETT CORPORATION.
 
Copyright (c) 1999, Mallinckrodt Nellcor Puritan Bennett
All rights reserved.
*******************************************************************************/

/***********************************************/
/****************** INCLUDES *******************/
/***********************************************/

#include <windows.h>
#include <ansi_c.h>
#include <formatio.h>
#include <toolbox.h>
#include <userint.h>
#include <utility.h>

#include "globals.h"
#include "72500.h"
#include "840.h"
#include "840Cmds.h" // VENTDATA Vent
#include "cpvtmain.h"
#include "cvicpreh.h"
#include "dbapi.h"
#include "detfun.h"
#include "helpinit.h"
#include "ldb.h"
#include "logonfun.h"
#include "mainpnls.h"
#include "MsgPopup.h"
#include "pts.h"
#include "pwctrl.h"
#include "registry.h"
#include "regpwd.h"
#include "RT_Bmps.h"
#include "tt.h"
#include "usradmin.h"
#include "version.h"

/***********************************************/
/************** GLOBAL VARIABLES ***************/
/***********************************************/

static int LookingForPTS = 0;
static int LookingFor840 = 0;
static int DetectingIgnoreCommands = 0;

static void SetPTSLabels(int ptsdetected);
static void Set840Labels(int ventdetected);
       void login(int ventdetected, int ptsdetected);
static void redrawLogin(void);
static void SetupPTSWrapper(void);
static void Setup840Wrapper(void);
static void ValidateLogin(char* UserName);
static int  PopulateMenuFromRegistry(int Bar, int Menu);
static void UserMenu(int panel, int control);

/***********************************************/
/************ FUNCTION DECLARATION  ************/
/***********************************************/

extern int CVIFUNC_C GetUserBitmapFromText (char *imageText, int version, int *bitmapId); // needed if there are bitmaps

/*****************************************************************************/
/* Name:     SetPTSLabels                                                 
/*                                                                           
/* Purpose:  sets the state of the PTS information displayed on the login panel
/*                                                                           
/* Example Call:   void SetPTSLabels(int ptsdetected);                                    
/*                                                                           
/* Input:    int ptsdetected	1 if detected, 0 if not        
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
static void SetPTSLabels(int ptsdetected)
{
	SetCtrlVal(gPanelHandle[LOGONPANEL], LOGONPANEL_PTS2000_LBL_3, LDBf(Ldb_PTS_NoDetect));

	if(!ptsdetected){
		SetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_PTS_SERIAL_LBL,   ATTR_VISIBLE, FALSE);
		SetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_PTS_SERIAL_LBL_2, ATTR_VISIBLE, FALSE);
		SetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_SW_VERSION_LBL,   ATTR_VISIBLE, FALSE);
		SetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_SW_VERSION_LBL_2, ATTR_VISIBLE, FALSE);
		SetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_CAL_LAST_LBL,     ATTR_VISIBLE, FALSE);
		SetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_CAL_NEXT_LBL,     ATTR_VISIBLE, FALSE);
		SetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_CAL_LAST_LBL_2,   ATTR_VISIBLE, FALSE);
		SetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_CAL_LAST_LBL_3,   ATTR_VISIBLE, FALSE);

		SetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_PTS2000_LBL_3,    ATTR_VISIBLE, TRUE);
		SetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_PTS_CAL_BUTTON,   ATTR_VISIBLE, TRUE);
		Register_Logon_CB();
	}
	else{
		SetCtrlVal(gPanelHandle[LOGONPANEL], LOGONPANEL_PTS_SERIAL_LBL_2, szSerialNumber);
		SetCtrlVal(gPanelHandle[LOGONPANEL], LOGONPANEL_SW_VERSION_LBL_2, szVersionNumber);
		SetCtrlVal(gPanelHandle[LOGONPANEL], LOGONPANEL_CAL_LAST_LBL_2,   szCalDate);
		SetCtrlVal(gPanelHandle[LOGONPANEL], LOGONPANEL_CAL_LAST_LBL_3,   szNextCalDate);

		SetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_PTS_SERIAL_LBL,   ATTR_VISIBLE, TRUE);
		SetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_PTS_SERIAL_LBL_2, ATTR_VISIBLE, TRUE);
		SetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_SW_VERSION_LBL,   ATTR_VISIBLE, TRUE);
		SetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_SW_VERSION_LBL_2, ATTR_VISIBLE, TRUE);
		SetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_CAL_LAST_LBL,     ATTR_VISIBLE, TRUE);
		SetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_CAL_NEXT_LBL,     ATTR_VISIBLE, TRUE);
		SetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_CAL_LAST_LBL_2,   ATTR_VISIBLE, TRUE);
		SetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_CAL_LAST_LBL_3,   ATTR_VISIBLE, TRUE);

		SetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_PTS2000_LBL_3,    ATTR_VISIBLE, FALSE);
		SetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_PTS_CAL_BUTTON,   ATTR_VISIBLE, FALSE);
	}

	return; // Modified the statement due to Critical Observation.
}


/*****************************************************************************/
/* Name:     Set840Labels                                                     
/*                                                                           
/* Purpose:  sets the state of the 840 information displayed on the login panel
/*                                                                           
/* Example Call:   void Set840Labels(int ventdetected);                                    
/*                                                                           
/* Input:    int ventdetected	1 if detected, 0 if not       
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
static void Set840Labels(int ventdetected)
{
	char *BD;
	char *GUI;

	SetCtrlVal(gPanelHandle[LOGONPANEL], LOGONPANEL_840_LBL_4, LDBf(Ldb_840_NoDetect));

	if(!ventdetected){
		SetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_840_GUISERIAL_LBL, ATTR_VISIBLE, FALSE);
		SetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_840_GUISERIAL,     ATTR_VISIBLE, FALSE);
		SetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_840_BDSERIAL_LBL,  ATTR_VISIBLE, FALSE);
		SetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_840_BDSERIAL,      ATTR_VISIBLE, FALSE);

		SetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_840_LBL_4,         ATTR_VISIBLE, TRUE);
		SetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_VENT_CAL_BUTTON,   ATTR_VISIBLE, TRUE);
		Register_Logon_CB();
	}
	else{
		GUI = strchr(Vent.Gui.serial, ':');
		if(GUI){
			GUI++;
			SetCtrlVal(gPanelHandle[LOGONPANEL], LOGONPANEL_840_GUISERIAL, GUI);
        }
		else{
			SetCtrlVal(gPanelHandle[LOGONPANEL], LOGONPANEL_840_GUISERIAL, LDBf(Ldb_Not_Found));
		}

		BD = strchr(Vent.Bd.serial, ':');
		if(BD){
			BD++;
  			SetCtrlVal(gPanelHandle[LOGONPANEL], LOGONPANEL_840_BDSERIAL, BD);
        }
        else{
   			SetCtrlVal(gPanelHandle[LOGONPANEL], LOGONPANEL_840_BDSERIAL, LDBf(Ldb_Not_Found));
   		}

		SetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_840_GUISERIAL_LBL, ATTR_VISIBLE, TRUE);
		SetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_840_GUISERIAL,     ATTR_VISIBLE, TRUE);
		SetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_840_BDSERIAL_LBL,  ATTR_VISIBLE, TRUE);
		SetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_840_BDSERIAL,      ATTR_VISIBLE, TRUE);

		SetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_840_LBL_4,         ATTR_VISIBLE, FALSE);
		SetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_VENT_CAL_BUTTON,   ATTR_VISIBLE, FALSE);
	}

	return; // Modified the statement due to Critical Observation.
}


/*****************************************************************************/
/* Name:    login	                                                 
/*                                                                           
/* Purpose:  displays the login panel   
/*                                                                           
/* Example Call:   void login(int ventdetected, int ptsdetected);                                    
/*                                                                           
/* Input:    int ventdetected	1 if detected, 0 if not
/*		int ptsdetected	1 if detected, 0 if not    
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
void login(int ventdetected, int ptsdetected)
{	
	char VersionStr[64];
	int nCtrlTop;
	int nCtrlHeight;
	int nPanelWidth;

	static int bNextCalOverdueShown = FALSE;

	gPanelHandle[LOGONPANEL] = LoadPanel(0, LDBf(Ldb_MainpanelFile), LOGONPANEL);

	#ifndef BETA
	sprintf(VersionStr, "%s %s", LDBf(Ldb_Version_Str), VERSION);
	#else
	sprintf(VersionStr, "%s %s. %s", LDBf(Ldb_Version_Str), VERSION, BETA);
	#endif	

	SetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_VERSION_LBL, ATTR_CTRL_VAL, VersionStr);

	GetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_VERSION_LBL, ATTR_TOP,      &nCtrlTop);
	GetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_VERSION_LBL, ATTR_HEIGHT,   &nCtrlHeight);
	GetPanelAttribute(gPanelHandle[LOGONPANEL], ATTR_WIDTH, &nPanelWidth);
	CenterCtrl(gPanelHandle[LOGONPANEL], LOGONPANEL_VERSION_LBL, nCtrlTop, 0, nCtrlHeight, nPanelWidth);

	GetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_PICTURE, ATTR_TOP,      &nCtrlTop);
	GetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_PICTURE, ATTR_HEIGHT,   &nCtrlHeight);
	CenterCtrl(gPanelHandle[LOGONPANEL], LOGONPANEL_PICTURE, nCtrlTop, 0, nCtrlHeight, nPanelWidth);

	GetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_PICTURE_2, ATTR_TOP,      &nCtrlTop);
	GetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_PICTURE_2, ATTR_HEIGHT,   &nCtrlHeight);
	CenterCtrl(gPanelHandle[LOGONPANEL], LOGONPANEL_PICTURE_2, nCtrlTop, 0, nCtrlHeight, nPanelWidth);

	SetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_LOGIN_BUTTON,    ATTR_DIMMED,   TRUE);

	SetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_LOGIN_BUTTON,    ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT);
	SetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_EXIT_BUTTON,     ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT);
	SetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_PTS_CAL_BUTTON,  ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT);
	SetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_VENT_CAL_BUTTON, ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT);

	SetPTSLabels(ptsdetected);
	Set840Labels(ventdetected);

	DisplayPanel(gPanelHandle[LOGONPANEL]);

	InitTips(gPanelHandle[LOGONPANEL]);

	if(ptsdetected && IsNextCalOverdue(szNextCalDate)){
		if(!bNextCalOverdueShown){
			CPVTMessagePopup(LDBf(Ldb_Pts_cal_err), LDBf(Ldb_Pts_cal_expired), OK_ONLY_DLG, NO_HELP);
			bNextCalOverdueShown = TRUE;
		}
	}

	return;
}


/*****************************************************************************/
/* Name:     redrawLogin	                                                     
/*                                                                           
/* Purpose:  resets information on the login panel for language changes 
/*                                                                           
/* Example Call:  void redrawLogin();                                    
/*                                                                           
/* Input:    <None>        
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
static void redrawLogin()
{
	int ptsdetected;
	int ventdetected;
	HPNL hWasLogonPanel;
	char VersionStr[64];
	int nCtrlTop;
	int nCtrlHeight;
	int nPanelWidth;

	GetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_PTS_SERIAL_LBL,    ATTR_VISIBLE, &ptsdetected);
	GetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_840_GUISERIAL_LBL, ATTR_VISIBLE, &ventdetected);

	hWasLogonPanel = gPanelHandle[LOGONPANEL];

	gPanelHandle[LOGONPANEL] = LoadPanel(0, LDBf(Ldb_MainpanelFile), LOGONPANEL);

	#ifndef BETA
	sprintf(VersionStr, "%s %s", LDBf(Ldb_Version_Str), VERSION);
	#else
	sprintf(VersionStr, "%s %s. %s", LDBf(Ldb_Version_Str), VERSION, BETA);
	#endif	
	SetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_VERSION_LBL, ATTR_CTRL_VAL, VersionStr);

	GetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_VERSION_LBL, ATTR_TOP,      &nCtrlTop);
	GetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_VERSION_LBL, ATTR_HEIGHT,   &nCtrlHeight);
	GetPanelAttribute(gPanelHandle[LOGONPANEL], ATTR_WIDTH, &nPanelWidth);
	CenterCtrl(gPanelHandle[LOGONPANEL], LOGONPANEL_VERSION_LBL, nCtrlTop, 0, nCtrlHeight, nPanelWidth);

	GetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_PICTURE, ATTR_TOP,      &nCtrlTop);
	GetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_PICTURE, ATTR_HEIGHT,   &nCtrlHeight);
	CenterCtrl(gPanelHandle[LOGONPANEL], LOGONPANEL_PICTURE, nCtrlTop, 0, nCtrlHeight, nPanelWidth);

	GetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_PICTURE_2, ATTR_TOP,      &nCtrlTop);
	GetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_PICTURE_2, ATTR_HEIGHT,   &nCtrlHeight);
	CenterCtrl(gPanelHandle[LOGONPANEL], LOGONPANEL_PICTURE_2, nCtrlTop, 0, nCtrlHeight, nPanelWidth);

	SetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_OPERATORS,       ATTR_CTRL_VAL, gCurrOperator);

	SetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_LOGIN_BUTTON,    ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT);
	SetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_EXIT_BUTTON,     ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT);
	SetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_PTS_CAL_BUTTON,  ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT);
	SetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_VENT_CAL_BUTTON, ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT);

	SetPTSLabels(ptsdetected);
	Set840Labels(ventdetected);

	DisplayPanel(gPanelHandle[LOGONPANEL]);
	DiscardPanel(hWasLogonPanel);

	InitTips(gPanelHandle[LOGONPANEL]);

	return; // Modified the statement due to Critical Observation.
}


/*****************************************************************************/
/* Name:   SetupPTSWrapper	                                                    
/*                                                                           
/* Purpose: calls the autodetect function for the PTS  
/*                                                                           
/* Example Call:   void WINAPI SetupPTSWrapper(void);                                    
/*                                                                           
/* Input:    <None>          
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
static void SetupPTSWrapper(void)
{
    int bitmapId = 0; // needed if there are bitmaps 
    char *dataPtr = NULL; 

	LookingForPTS = 1;
	setButtonBMP(gPanelHandle[LOGONPANEL], LOGONPANEL_PTS_CAL_BUTTON, BMP_XCancel79);  
	ResetDetect();
	SetPTSLabels(!EstablishPTS(AUTO_DETECT));
	LookingForPTS = 0;
    dataPtr = 
        "!!!!!$!!%1IC!<!!!!>!!!<9!!!<!!!!!!!!!Q!Q!!9!!!!!0!``Q`!!6/!0600+6+3363;36DL8:T\""
        "D:`-DJ%5DJ%5D:`-D:`-DJ%5D:`-\?:\\%;6QT36;D+633/&@0060336;;Q7TD%G5T=GVTFE^/+<\\N';"
        "TF^;I-F7\?\\%63DL6+332/;060036;;\?:\\%LJ5N/'2\\NW'TFG^D%F53D6L++53/760006+3378T\";"
        "L5N2/%TNN'LJ=^;J%V3:TN+:DF#93=Z9+=Z9+5Z:+=#:3=+:DF3:TN;J%VLJ=^/'%TNW'TFG^D%F53D5"
        "D/5600063;D;D-=<TF'<TN'<TN';L=^;;%V7+DF1/(Z9+=Z9+5Z9+=Z9+=Z9+5Z9+5Z:+=+:DF;J%VLJ"
        "=^TNN'TNF'TNN'TJ=V8:T\"+6+3/&200603D7LL5GNTFW'TFW'TFG^;%FV#DEF/2'Z+=7#DF;;%V2/$T"
        "NF'TJF^D:`-+633/&000603D7TL9GVTFW'TFW'D-FV+LEF/&'Z+='Z+563336333633;6+3D6+3L6(3Y"
        "6(3Y7#+-7#+-1/'Z:+=+:LFDJ-V/'$TFW'D%F5+353/.600063DT;L9V<TF'<TF';;%V7#;F1/'Z:3=("
        "63Y3533)!))9!999!99J!JJJ!JJZ\"ZZ#6##/&$3363336D+36D+36L(36Y3DF9#;GF;%EV/$<TF';D%"
        "56+332/,060036DDLJ5NLJ=^LJ=^3:\\N/\"(Z3F=#3F=331;JJ%J/*!!!!!)))!999!9996###6DDD7"
        "LX='Z3=7#3=73\\N;TF^;L=^;L=^7;\\-2/+0600+63;DJ-=LJ=^LJ=^3:\\N#2/$3B=Z3F=#3F=#3B="
        "Z3B=Z3F=(36Y3313))%)/,!!!!!)))63336```6#;/)$=Z:3=3:\\N/'$L=F^;Q5T/*600078T\";L=^"
        ";L=^7;\\V6#;/*)=#:3=353;R\"RR/!-!!!!BB&BDD6DLX3/%=7#3=7#;=7;\\V;L=^;L=^;D-N63;D2"
        "/(060036;DDJ-NLJ=^DJ%V+:DF/&)#;F=(36Y3313))%)/-!!!!60006```6+D/*&=#:;=+:DF;J%VLJ"
        "=^LJ=^;:\\-/&(0060;\\G-L5G^D5F^3LFN#;FF#;FF#;E=/$7#;F2/$#:;=353;R\"RR/!-!!!!BB&B"
        ";D6;[^K<2C@>`X+!$55A/%7#;F73LN;D5^;L5^;D-V63;D2/&060036;DDJ-NLJ5^;:\\N/&*#;FF-;6"
        "V3313))%)/-!!!!60006```7+D=J2C>]`X!]`X!:T%\\2/%#:;F;:\\NDJ5^DJ5^;:\\-/&&0060;LG$"
        "D5G^D-FV+L3/)F7#DF7+D=633;!RRR1/-!!!!9\"99;6D;[:^<#:DF#;DFZT\"Z`TX!PRP)+:D5#:D=#"
        ":DF#:DF+:LFDJ-VDJ-^DJ%N+63;/&$0060+363;\\G9D-FV;\\EN/*7+DF73D%63336###!JJJ!BBB!9"
        "99!)))1/)!\"!!#6##T6TT3:TF+:DF+:DF#<D=9T=\?`SX!JBRZ#:;=/&$+DFF;\\GND-GVD-FV3D5T/$"
        "600063;D;D%N;D-V73TN2/'+:DF;:TNL:X=L6LLD6DDL6LL/&$``6`LL6L33633363##1#ZZ!ZJJ!J99"
        "!999!9!!!!99&93;7;\\*E@/$7+DF7#;=6CTB]`X!]`X!63DT7#D=7+DF7+DF73TN;D-V;D-V77T12/$"
        "060036DTDJ%V;J%V/&&+LGF\\-FV;LF$001011!1!!!!11%1/$!!!!!999!RRR6DDD6DDD6LLL6```K5"
        "5-6TTT6DDD633363336TTT7>_C7+LF7+LF7+DF7#D5&Z3`]),9]`X!I9AR7#;-7+D=7+LF7+LF;;%V;D"
        "%V7>_C6+3;6000600077T1;;%V7;\\N7+LF7+LF7D\\NK=N^`)&-K===6DDD!RRR!9991/+!!!!1!111"
        "!11J!JJR\"RR`6``L6LL;:L$/&%+LFF+DB=\\7F(IQ@(`X0!\?\?\"-Z36`#DF5+LFF+LFF3\\GN;%GV;%"
        "FN3;6L006000605VG7;%FV3\\FN3LFN3LKN55K5FFKFNNKNNNKN^^L^^'K^==F=##5#/%!!!!!RRR!JJ"
        "J1/%!!!!)!))Z\"ZZ0600363336;D3:N53:LN3:LN3:TN3:LF+:LF#9D5Z83`PTP)`RX!;6D;#:;-+:L"
        "F3:LF3:TN;J%V;J%N36DT060006005:V7;J%V/&$3TKN--K=--L=Z\"+Z55E-/$K5556TTT6TTT60006"
        "333K5554/$)_&-5J553633#6##3633363336DT3:N5/&(3TFN3LFF#DK5VV0J`X+!9A\"RZ36`+DF53T"
        "FF3TGN;%FV;\\FN+D6\\006000605VF7;\\3/$N73TN7>_C]/4D]`X!JJV;7D\\N;\\*@;\\*@K--=3/"
        "(5J553633363336;D3:H$3:TF/&+3TFN+LK=2=@%`X+!^^\"9Z+6T#DF-3TFF3TFN;\\FN;\\FN+D6\\"
        "006000605VF7;\\FN3TFN3TFN>_PC9=0\?`X0!`X0!HH&13TFN3TFN3\\FN3TFN3\\FN;\\FND\\GNI-G"
        "F\\*K@--F=[^F<3D6T3NF53\\FN3\\3/%N63T/*%N35\\/%FN3TGFT%@\\`X0!),\"9R+6L#DF%+LF=3"
        "TFN;\\FN;\\FN+D6\\006000603NE5/$73\\N]9=\?4/%`SX!2FC>3:TF3:TN3:\\N3:\\N;:\\N;:\\N"
        "3:\\N;:\\N3:\\N3:\\N;:\\N3:\\N;:\\N3:\\N;:\\N;:\\N3:\\N;:\\N;2/$\\FN3/'$\\N7;\\N"
        "73\\N7;\\9]`X!]`X!&R+D6#;\\7+L=2/$3:\\N+6D\\060006003:N53:TN;:\\N;:\\NLK155HLV`T"
        "X!`TX!/T4D9R=\?+:L53:TF/&9;\\FN3TPF`X0!`X\"!R+2DZ;6T+LF53\\FN3TFN3TFN+D6\\0060006"
        "03NF53TFN;\\FN;\\FV;\\FN3\\PN`X0!`X\"!Z32LZ+6L#;6\\+LF53\\FN;\\GN;%GV;%GV;%GN;%3"
        "/%V7;\\V;;%V7;\\V;;%N2/$;:\\V;J%V;J%V;:\\V;J%V;:\\V;J%V;:\\N;:\\V3<\\N`TX!`QX!R5"
        "+DZ67P+:D-5:YC3:TN3:TN-6;V06000600+:D-3:TN;:\\N;J%V;J%N5<YC`TX!`QX!Z5++J%Z3R6+D#"
        ":D%5:YC;J%N/'7;%GV_:P$`X0!/4\"DR+2DZ36L+DF-3TFF3TFN3LFF-;6V00600060+DF%+LFF;\\GN"
        ";%GV;%FN5YPC/40D`X&!IQ1(BR2#R#6;#DF%5YGC;%EN/5;;%VJ5LVJ5LVJJV;]`X!IVVJ&Z+L&Z3L7+"
        "D-73TF7+LF7+LF6+3D600060006-;V7+LF73TN;D%V;;%N7;\\NIJRZ]`X!INN9\"JZ+&R+D7#D%75YC"
        ";;%N2/'DK%V-PF'3^7;/0&\?\?U\?/$`37;`\?\?\?`\?\?\?`)&-;D%VJ5LV4/$`SX!2EC>Z53LZ63L+:D-+:L=+"
        ":LF+:D5+63;06000600+63D+:LF3:LNDJ-VDJ-V;J%N_L:$`TX!PQP)Z533Z6;T3:N5;J%NDE%/%GVD-"
        "GVL5K^Z^`/\?\?`\?)&[-VVEV/'KNNNO^'^`)&-KVVV6TTT;D-V;_:$4/%`SX!VAVJZ63L+:L-+:D=+:LF+"
        ":D%/&$0060++63+DF-+DGF;%GVD-GVD-GV;%PFHH01`X+!9A&R#D6\\5YGCD%3/%V;D-VL5V/`37;`37"
        ";KVVVKFFF3/%5K55=K==VKVV^['^VKVV=J==T6TT0600DJ-VDJ-V/0$`X0!77&1IQ2(Z;6T+DF-#DF=+"
        "DFF-;5V/%60006(3Y7#DF73TN;D-V;D5^;D%NJ2C>]`X!]PP)6;QT75YC;D-V;D-^;D-^;T=^P^/\?`\?\?"
        "\?`)&-K^^^KNNN3/$FKFFNKNNNKNN^['^VKVV=K==5K555J55T6TT0600DJ5^DL-V/T4DPSP)2E=%Z53L"
        "R5+;Z67P#:;-#:;=+:D5+63;/&%0060+36D#;F=+DGFD%GVL5G^D-FV;\\P9/40D`X0!77&1;\\G-D-G"
        "ND5G^L9KVFNKV^^K^FFKF55F-LL5L/$6;D;6++3!JJR6```KVVV3/%5J55T6TT0600DJ5^DK5V5FLV89"
        "T\"Z53LJ5#3J5#3Z5+LZ5+\\Z:;5#:3%/&&0060++63#3F%#;FF3TGNL5G^D5GV;%FF;Q@TPP0)`X+!J"
        "R'Z;%GFD5FV[^K<F=KNVNV'[^E</$7LX=7`CC7[^<7[^<!B9RKNNN3/$5K55VKVVVJVVT6TT0600LJ5^"
        "DJ5^;J%F+9L-R5+DJ%Z+J%Z+R5#DR5#\\Z:35+6+D/&'0060+36D#;F=#DGF;%GVL5G^D-FN5VF7CT@B"
        "PP+)JR&Z;\\G9D-FNL;;\\=%J^%LBN!#B=!DBV!DBV!;ANJ1E%3JF-TLF%TT;TNNKN55K5FFPF)&\\-^"
        "'K^==F=LL6L##7#L=G^L5G^D-FN7TB1Z32LR#2;R#1;JZ2DZ+F%#+5\\/)60006(3Y7#;=7+LF;D5^;L"
        "9V;;%F2/\"/5H`Z57PZ67P+:L-;J%F+73L%;LNNJ=^7:T1#:L%#:L%+;T-FO='NN5/L7LL=K===K==NL"
        "NN)_&-NJNN`6``D6DD#5##R\"RRLJ=^LJ=^DJ5V>:_C/&\"/H2`Z;2TR+2LR#2\\Z+F%#+5;/)60006+"
        "+37#;-7#3=73TN;L=^;D5N7;\\96#D\\&Z7P6#D\\7;\\92+ZD7`CCLZ^/4/$I`LN``\\X``\\XI^LNC"
        "6;R/+$==K=55F-TT6T3313RR\"RZ#6+;\\E9/$;L=^;D5N7>_C7#;-'R+)'Z+-6#+L2/+0600#5+DZ93"
        "5Z:3=3:\\NLJ=^DJ-N7:T1#6D\\#6D\\7:T1+&ZD`;CCNL=^7`'H\?^7P/0$\?/[H^NV7C;;R==K=55F5`"
        "`6`3;1;ZZ!ZZZ%Z/\"6/H`;L=^<TF';TF^;TF^;L=^;L=^73TF'Z+5'Z356#+\\2/-0600#5+LZ935Z:"
        "3=3:\\NLJ9VIJ-F;:\\97:T1;:\\9+&ZD`;CC^NN'/+&VFK^VNV'TT6T``6`LL6L##1#JJ&J+;7DL5EN"
        "/&<TF';T=^73\\N'Z3='Z3=7#3%6++32/.0600#5+LZ935Z:+=3:TFDJ5VDJ5NDJ-NDI5NR%93Z&1\\/"
        "!(R11D1!&R0010RR\"RZ+7+D%E5/'<TF';L=^73TN'Z3='Z3='Z+%6++32/00600#5+DZ9+-Z:+5+:DF"
        ";J%NTJF^TJF^/!*)!!1!!!)RR&R8TG\"T=E^/'<TF';D-V7+DF'Z+='Z+=6#+\\6++32/20600#5+;Z9"
        "+%Z9+=Z:3=3:TNLJ5^/'2TNW'D5F^3TBNZ3B=Z+B5Z+F5#+5T/560006++36#+T'Z+-'R+5'Z3=73TN;"
        "D-V:TF/.,'TNN'TNF'DJ-V39TNZ93=Z9+5Z9+5Z:+%#6+D/&80060#+6;#+2TZ#B-R#B5R+F5#;F=3TG"
        "N;%GVD5G^L=G^TFW'\\NW'TFW'L=G^D5G^;%FV3LFN#;B=R#B5R+B5R+B5Z+F%#+6L++53/;60006#+;"
        "6#+L'Z#%'R#-1/,R9#5R9#-Z9#%Z6+T#6+D+6+3/&@0060#+6;#+2DZ#5T/(&Z#\\&Z#T6#+L6#+D6++"
        "36/\"0500";
    GetUserBitmapFromText (dataPtr, 102, &bitmapId);
    if (bitmapId){
        SetCtrlBitmap (gPanelHandle[LOGONPANEL], LOGONPANEL_PTS_CAL_BUTTON, 0, bitmapId);
        DiscardBitmap (bitmapId);
        bitmapId = 0;
    }

	return;
}


/*****************************************************************************/
/* Name:     Setup840Wrapper		                                                   
/*                                                                           
/* Purpose:  calls the autodetect function for the vent
/*                                                                           
/* Example Call:   void Setup840Wrapper(void);                                    
/*                                                                           
/* Input:    <None>          
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
static void Setup840Wrapper(void)
{
    int bitmapId = 0; // needed if there are bitmaps 
    char *dataPtr = NULL; 

	LookingFor840 = 1;
	setButtonBMP(gPanelHandle[LOGONPANEL], LOGONPANEL_VENT_CAL_BUTTON, BMP_XCancel79);
	ResetDetect();
	Set840Labels(!Establish840(AUTO_DETECT));
	LookingFor840 = 0;
    dataPtr = 
        "!!!!!$!!%22C!<!!!!>!!!<9!!!<!!!!!!!!!Q!Q!!9!!!!!0!``Q`!!6/!0600+6+3363;76CP;:R)="
        ":Z)/''D%F5=ZF);Q6T3A6A+353/@6000633;6;QT;E+;;T=V;TF^2/+\\NN'TJF^LJ5F=:Z)76CP+633"
        "/&;00603;6;=ZG)L5EF/2<\\N';TF^;E+;67CP6++32/70600+633;6QTLJ5F/'%TNW'L=G^;%FV3TFN"
        "+DFF#;B=Z+B=Z+B5Z+F=#;F=+DFF3TGN;%GVL=E^/%<TN';TF^;D%563AA2/5060036AAEJ+;TNF'TNN"
        "'TNN'LJ=^;J%V+:DF/\"(Z+B=Z+B5Z+B=Z+B=Z+B5Z+B5Z+F=+DGF;%GVL=G^TNW'TFW'TNW'T=FV;RF"
        ")++53/2600067CP;L5V<TF'<TF';TF^;;%V7#DF1/2Z:+=#:DF;J%V/'$TFW'TFG^D%F5+353/060006"
        "7CP;L=V<TF'<TF';D-V7+LF1/6Z:+=+:LFDJ-V/'$TFW'D%F5+353/.600067CP;L=V<TF'<TF';;%V7"
        "#;F1/&Z93=Z:35363;3633363;+63D+63D+63\\+63\\#:3%#:3-#935Z5+/'B=Z3F=#;GF;%EV/$<TF"
        "';D%56+332/,060036AALJ5VLJ=^LJ=^3:\\N/\"(Z3F=+36\\3313))!)11!199!9BB!BJJ!JRR&R##"
        "6###6#33633363336;+36D+36D+36\\3LE1/$'Z3=73\\N;LF^;L=^;L=^7=Z)2/+0600+63;EJ+;LJ="
        "^LJ=^3:\\N/\"(Z3F=#3F5331;RR%R/+!!!!!999!999!RRR6;;;;\\%51/%Z:3=3:\\N/'$L=F^;Q5T"
        "/*60007;R);L=^;L=^7;\\V2/)#:;=+63\\3533)\"))/!-!!&!##6#TT6T3L3/&=7#;=7;\\V;L=^;L"
        "=^;D-N63;;2/(060036AADJ-NLJ=^DJ%V+:DF/&)#;F=331;RR%R/-!!!!!9996;;;;\\%52/'#:;=+:"
        "DF;J%VLJ=^LJ=^;:\\3/&(0060=ZG)L5G^D5F^3LFN#;FF#;FF#;E=/$7#;F7#;=7#;=6+;\\6333!))"
        ")1/-!\"!!#6##T6TT=<Z)PTP)4R7\?1:D9/&%#;FF3LGND5G^L5G^D-FV3A5A/&60006+;D;D-N;L5^7;"
        "\\N2/)#:;F#:;=353;R\"RR/!-!!!!99&9;;7;\\%F5;RP)PP0)`X0!\?C&51DE9/%7#;F7;\\N;D5^;D"
        "5^7=Z)2/&06003:L%DJ5^DJ-V+5L/)FF#DFF+;6\\3313))%)/-!!!!6###6TTT7;LF7#DF73L=]47\?]"
        "`X!]47\?7#;=2/$#:DF+:LFDJ-VDJ-^DJ%N+63;/&$0060+363;`G=D-FV;\\EN/*7+DF633;!RRR1/-!"
        "!!!1\"11;6;;\\J%5+:DF+:DF#:D=+<D-PTP)`RX!T6TT#:D=/&$+DFF;\\GND-GVD-FV3D5P/$60006"
        "+;D;D%N;D-V73TN2/(+:DF3:LF=:Z)36333633#5##R!RRJ!JJ9!999\"99/!(!!!!RR&RTT6TD\\FF+"
        "DFF+DFF#DF=#3K-BO@%`X0!++&9#;F5#DF=+DFF+DFF3TGND-GVD-FV;\\E3/$600063DP;D%V;;%V2/"
        "'+:LFPJ1R;:R)#6###6##;6;;T6TTL6LLT7TT5J55T6TTL6LL36333633#5##R!RRJ!JJ9!999\"9936"
        "33_J'-/&$+LFF#DF=#;F-#3@L`X0!`X&!3D6P#;F5+DFF+LFF+LGF;%GVD%FV;\\FF+36;00600060;R"
        "G);%FV;\\EN/$7+LF7D\\NK%5N6#++!9991/%!!!!1!11!!!!1!11!!!!R!RRR\"RR;6;;L6LL_J'-;6"
        ";;_J'-T6TTD6DDL6LLL5\\/%FF+LFF#DB5Z3@\\++09`X+!BO2%Z7F)#DF=+LFF+LFF3\\GN;%GV;%FN"
        "3;6P00600050/\"7/T5;;%V73\\N73LNK%5NP^'/^!)TKVVVK===6TTT6;;;!RRR1/%!!!!1!11!!!!9"
        "!991\"11/!%!!!!))&)3363DD6D7C5P/$73LN73TN73LF7+LF7#D5JBO%]`X!I^^9&Z+\\7#D57+LF73"
        "LN73TN;;%V;;%N63DP6000600073T=;;%V73TN73TNK555^!)T]`X!^!)TKNNNKNNN;_'-!JJJ1/$!!!"
        "!9\"99D6DDD5DDR!RR1!111!11)!))J\"JJ#6##3633363336;P3:L1/&'3TFN+LG=V+@A`X0!77\"1R"
        "+6L#;F-+LF=3TFN3TGN;%FV;\\EN/\"6/H`6000600073T=7;\\N73TN73TN]',Z]`X!]`X!]PH13/&5"
        "J55_J'-T7TTNKNNNKNN^K^^^K^^=J==T6TT3633363336AA3:D%3:TF/&*3TFN=ZP)`X0!`X\"!R#2DZ"
        "36\\+LF=3TFN3TFN;\\FN;\\EN/\"6/H`6000600073T=7;\\N73TN]',Z4/%`RX!^F1X3:TFD:\\FTJ"
        "%FTK%F-K5=-J5=/+$55G5_'F-33633;5;/\"6/H`73T=63\\/*%N35T/%FN3\\3/%N73TF]`X!]`X!&R"
        "#D&Z3T7#D573TF73\\N7;\\N7;\\N2/\"/6H`060006003:L=3:\\N3;\\N5GNLBFO%/0$`X0!47&\?+L"
        "F=3TEF/$73\\N7;\\N7;\\N7D\\N;D%N7L\\F73L17;\\N73\\N7;\\N73\\N7;\\N7;\\N73\\N7;\\"
        "N5;/$:\\N32/$\\FN;\\FN3TPF`X0!`X\"!R#2DZ36L+DF-3TFF;\\FN3\\FN3\\FN+;6\\006000603"
        "LF=3TFN;\\FN;\\FN3\\PN`X0!`X\"!Z36\\#;6L#;F%+LF=3/'&\\N5;/$:\\N32/$\\FF3T3/&F73\\"
        "N73\\N53/':\\N;<\\N`TX!`QX!R5+DR6+L#:D-3:TF;:\\N3:TN3:TN+6;\\060006003:L13:TN;:\\"
        "N;:\\N3<TF`TX!`QX!Z5+3J%Z3R6+D#:D-3:TF/&$;\\EN/*K===K555K===K===;_'-!99973TF73\\"
        "F7;\\N7;\\N;;%V7;\\N;E+;]`X!]PP)&R+D&R+L7#D%73TF7;\\N73TN73TN6+;\\600060007+D-73"
        "TN7;\\N;;%N73TF]47\?]`X!5LTR\"BR+&R#;7#;%73T=7;\\N;;%N7;\\NK^^^````4/(P`HX````P`H"
        "XP`HX`]``9\"99+:N/3:TF;:\\N;J%N;J%V;K%V5HNL`TX!4Q7\?R5+DR6+L#:D%3:TF;:\\N3:TN3:LF"
        "+6;T060006003:D%+:LF;:\\N;J%N3<\\F'T,Z`SX!EAPNJ%Z+R6#D#:D%3:T=;J%N;J%N3;\\FNLNN`"
        "^``>:(5+1/$BE%3BE%+BF%>(P551U;#9E%#9K%-!AK11&9#DE%/\"7/T573\\F;;%N;;%V;;%V]',Z]`"
        "X!JBO%&R+L&Z3L7#D%73\\F7;\\N7+LF7+LF6+3D600060006+;T7+LF73TN;;%V;;%NJ5FD]`X!]PP)"
        "&Z+3&Z3T7+N/7;\\N;;%V;;%N73\\FKNNN````3+B%\"R)\\#AT,#AT,#BN-\"R)\\3;R1`51;\"J!L\""
        "J!TK-!K!111&Z3T7+N/73\\F;Z7;;Z7;;D%V]PP)]`X!6TTT&Z3L&Z3L7+D-7;\\F73TN7+LF71D96+3"
        ";600060006+3D7+LF73LN;D%V;D-V7;\\F]\?C5]`X!IEPN6#;\\73T=;;%N;D-V;;%N73\\=KNNN````"
        "33B%#R9%'!LV'!LV');N#R9%3;R1`51;\"J!T\"J!TK-!K!111&Z3L7+D-73\\=;Y0&]`X!]47\?]`X!]"
        "47\?7#D%&Z3L&Z3L7+N/;;%F7+LF7+LF7+D-2/$0600+6+3+:D-+:DF;J%VDJ-V;K%N5HFD`TX!PRP);6"
        "QT3:\\=DJ-VDJ-VDJ%N3;\\=NLNN`^``3)B%R)9%!9LV!9LV)9;NR*9%;,J-P_HX-K!K9K)RNIF^1!11"
        "R6+D+:D-3:\\=YJ0&/0$`X+!5F6D#D2\\Z36L#;5T/\"7/T573\\F7+DF7+DF6+3T2/%0600+6;\\#:D"
        "F3:TNDJ-VDJ-N3<T=+T+9`TX!7R71;:\\3DJ%NDJ-VDJ-N;;`=NLNN`^``3)J-Z&)\\/!$BNA-Z)5\\3"
        "JP-````XX`XHH[HVVAV11\"1R+6D+DF-;`K=55E5/%]`X!JBO%&Z3L6#D\\73\\=73TF7+DF7#D=6+3;"
        "2/%0600+63D#:;=+:DFDJ%VDJ-V;J%F;8QTPTP)`TX!'R,Z;J%FDJ5VDJ-V;;`=NLNN`^``3)J-Z&)\\"
        "/!$NFA)Z)5\\3JP-XX[X^^F^LL6L##1#))\")Z36D+NF/;\\KF55P5`X0!`X0!47+\?BO6%3A2AZ36L+N"
        "F/3\\F=+DFF#DFF#;E%/&60006++37#3%7#;F73TN;L5^;D-N2/\"/:T5L8LDPSP)EBPN3:\\=DJ-NDJ"
        "-V;K%FNLNN`^``3)B%R)9%!9LV!9DN)9;NR*9%3,J-P^PPT5TT)!))1!9B9!BJZ6;L/&\"/TG5;%GFZ7"
        "K;5N6L=ZF)#;2TR+2DR#2;Z35L/\"7/T57+L=7#;=7#;=6+3D2/'0600+63D#:;=#:DF;J%VDJ5V;J%F"
        "+5F_Z53LZ6;L3:L%;J%FDJ-N;K%FNLNN`^``3)B%R)9%!9T^!9LV!9DNR*9%3,J-P^PPT5TT1!99J%Z+"
        "R6+D#6D\\3:\\=DJ-NDJ5V\?J-J+9N/Z53DJ5#+J6#3#6;L+:D-#:;5#:;=#:3%/&)0060#36\\#;F=+L"
        "GFD-GVD-FN;\\F3#D2\\Z36L#D6T;\\G3\?-GJ;%KFNNPN``U`3BA%N52`)3BF)3BF1+AFN55`3BP%PPW"
        "P_'A-19\"BR#63#;6T+NG/;%GFD5GVD5GV\?-FJ3LB%Z32DJ#23R#63#;2TZ3B%Z3F5#3F5++5D/)6000"
        "6++37#3-7#;=73TN;L5V;\?-J2/\"/:T5#6DT#6D\\/&\"/TG5;%GF;%KFNNPN``U`;RE1#93/$%3#B%3"
        "#9%3;R1`TPXK555!9BJ&Z3D6+F_7;`=;D-N;L=V;L=^;D-N7;\\36#DT&Z;L6#;T&Z3\\'R+)'Z356#+"
        "L2/+0600#6+D#935Z:3=3:\\NLJ=V\?J-J;:\\3+:N/;:`=\?J-J;K%FNLNNX_XXFJ=N/+&9)KRF=PNHH["
        "HNNAN11\"9Z+63#;6L;`G=D5GNL=G^L=G^L5GV\?-FJ;\\F3;\\F3#DB%R+B)Z+F5#+5\\/-60006#+L'"
        "Z35'Z3=73TN;L=V;D5N;\?-J;D-N;D5N;\?-JKVVV`51;6LLL2/&L6LDD6DDT7TTFJ=N_J'-_I'-1\"11;"
        ":`=DJ5NLJ=^TJF^LJ=^LJ=VDJ5N+9L=Z9+5Z:+5#:3%+6+3/&.0060#+2LZ3B5Z+F=3TGFL5GVL=G^L="
        "G^L=GVD-FN;;6;TT6TTT5T/(K===P^'/`XXX`HHH!9997;\\3;D-N;L=V;TF^<TF';L5^73TF'Z+5'Z3"
        "='Z+%6++32/00600#5+DZ9+-Z:+=+:DFDJ-VTNF'LJF^DI5NR5#39\"BJD6DD/+*55G5_'A-11%1/\"6"
        "/H`;E+;;L5V;TF^;D-V7+DF'Z+='Z+=6#+\\6++32/20600#5+DZ9+%Z9+=Z:3=3:TNDJ-VLJ=V;9`=R"
        "6#3/!-))&)+F6_;`G=;%FF+LBFZ3B=Z+B5Z+F5#+5T/560006++36#+T'Z+-'R+5'Z357+L=73T=7;\\"
        "36#DT&Z3D&R+/%(3R5#3R6+;#6;L#6;T#9;%Z9+-R9+5Z9+5Z:+%#6+D/&80060++6;#+2TR#B-R#A-N"
        "^B)Z+B%Z32\\Z32TZ32LZ/&$;L6#;L&Z3L&Z3D&R+D&J#D\"JZD\"BRL&R#\\#N^)'Z+%6#+L6++32/;"
        "0600+6+;#5+LR5#\\R5#\\J%ZTJ%ZTJ&RT/!(BR1LJR1TJZ1TJZ2TZ#2L^'6;++53/@60006##3&^';&"
        "Z#/%)LR%ZDR5#DZ6#3#6#+#6##/6!0010";
    GetUserBitmapFromText (dataPtr, 102, &bitmapId);
    if (bitmapId){
        SetCtrlBitmap (gPanelHandle[LOGONPANEL], LOGONPANEL_VENT_CAL_BUTTON, 0, bitmapId);
        DiscardBitmap (bitmapId);
        bitmapId = 0;
    }

	return;
}


/*****************************************************************************/
/* Name:     Logon_Callback                                                      
/*                                                                           
/* Purpose: Callback function for the login panel
/*                                                                           
/* Example Call:   int CVICALLBACK Logon_Callback(int panel, int control, int event,
/*		void *callbackData, int eventData1, int eventData2);                                    
/*                                                                           
/* Input:    standard callback parameters         
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   always 0                                                       
/*                                                                           
/*****************************************************************************/
int CVICALLBACK Logon_Callback(int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int BarHandle;
	int OpIndex;
	char OpLabel[50];

	if(event == EVENT_GOT_FOCUS || event == EVENT_LEFT_CLICK){
		switch(control){
			case LOGONPANEL_OPERATORS:
				SetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_LOGIN_BUTTON, ATTR_DIMMED, TRUE);
				UserMenu(panel, control);
				break;

			default:
				break;
		}
	}
	if(event == EVENT_COMMIT){
		switch(control){
			case LOGONPANEL_LOGIN_BUTTON:
				LoadPanels(APPSTART2);
				SwitchPanels(gPanelHandle[TESTPANEL]); 
				DiscardPanel(gPanelHandle[LOGONPANEL]);
				gPanelHandle[LOGONPANEL] = 0;
				DisplayPanel(gPanelHandle[SYSPANEL]);

				OpenJetDB();
				#ifdef CRPE_ENABLED
				OpenCrystalReportPrintEngine();	// Open Crystal Reports
				#endif
				break;
																						 
			case LOGONPANEL_PTS_CAL_BUTTON:
				if(LookingForPTS){
					SetCtrlAttribute(panel, LOGONPANEL_PTS_CAL_BUTTON, ATTR_DIMMED, TRUE);
					LookingForPTS = 0;
					StopPTSDetect();
					
				}
				else{
					SetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_OPERATORS,       ATTR_DIMMED, TRUE);
					SetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_VENT_CAL_BUTTON, ATTR_DIMMED, TRUE);
					SetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_EXIT_BUTTON,     ATTR_DIMMED, TRUE);
					SetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_LOGIN_BUTTON,    ATTR_DIMMED, TRUE);

				    SetupPTSWrapper();

					SetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_OPERATORS,       ATTR_DIMMED, FALSE);
					SetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_PTS_CAL_BUTTON,  ATTR_DIMMED, FALSE);
					SetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_VENT_CAL_BUTTON, ATTR_DIMMED, FALSE);
					SetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_EXIT_BUTTON,     ATTR_DIMMED, FALSE);

					GetCtrlVal(gPanelHandle[LOGONPANEL], LOGONPANEL_OPERATORS, OpLabel);
					if(strcmp(OpLabel, "") != 0){
						SetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_LOGIN_BUTTON, ATTR_DIMMED, FALSE);
					}
				}
				SetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_VENT_CAL_BUTTON, ATTR_DIMMED, FALSE);
				break;

			case LOGONPANEL_VENT_CAL_BUTTON:
				if(LookingFor840){
					SetCtrlAttribute(panel, LOGONPANEL_VENT_CAL_BUTTON, ATTR_DIMMED, TRUE);
					LookingFor840 = 0;
					Stop840Detect();
					
				}
				else{
					SetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_OPERATORS,      ATTR_DIMMED, TRUE);
					SetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_PTS_CAL_BUTTON, ATTR_DIMMED, TRUE);
					SetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_EXIT_BUTTON,    ATTR_DIMMED, TRUE);
					SetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_LOGIN_BUTTON,   ATTR_DIMMED, TRUE);

				    Setup840Wrapper();

					SetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_OPERATORS,       ATTR_DIMMED, FALSE);
					SetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_PTS_CAL_BUTTON,  ATTR_DIMMED, FALSE);
					SetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_VENT_CAL_BUTTON, ATTR_DIMMED, FALSE);
					SetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_EXIT_BUTTON,     ATTR_DIMMED, FALSE);

					GetCtrlVal(gPanelHandle[LOGONPANEL], LOGONPANEL_OPERATORS, OpLabel);
					if(strcmp(OpLabel, "") != 0){
						SetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_LOGIN_BUTTON, ATTR_DIMMED, FALSE);
					}
				}
				break;

			default:
				break;
		} // end switch(control)
	} // end if(event == EVENT_COMMIT)

	return 0;
}


/*****************************************************************************/
/* Name:    AddUser_Wrapper		                                                   
/*                                                                           
/* Purpose:  calls the function to add a user for the add user item in the user's list
/*                                                                           
/* Example Call:   void CVICALLBACK AddUser_Wrapper( int menuBarHandle,
/*         int menuItemID, void *callbackData, int panelHandle );                                    
/*                                                                           
/* Input:    standard menu callback parameters        
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
void CVICALLBACK AddUser_Wrapper(int menuBarHandle,
         int menuItemID, void *callbackData, int panelHandle )
{
	UIAddUser();

	return;	
}


/*****************************************************************************/
/* Name:     ValidateLogin		                                                 
/*                                                                           
/* Purpose:  prompts for user password
/*                                                                           
/* Example Call:   void ValidateLogin(char* UserName);                                    
/*                                                                           
/* Input:    UserName - to validate password for          
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>
/*                                                                           
/*****************************************************************************/
static void ValidateLogin(char* UserName)
{
	char PWbuff[MAX_PWD_LEN + 1] = {'\0'};
	char EnterPWStr[64]; // must be large enough for longest message

	GetPasswdForUser(UserName, PWbuff);
	if(strcmp(PWbuff, "") == 0){
		SetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_LOGIN_BUTTON, ATTR_DIMMED, FALSE);
		strcpy(gCurrOperator, UserName);
		LoadUserOptions(gCurrOperator);
		redrawLogin();
	 	return; 
	}
	gPanelHandle[PASSWDPNL] = LoadPanel(0, LDBf(Ldb_MainpanelFile), PASSWDPNL);

	strcpy(EnterPWStr, LDBf(Ldb_Enter_Pwd_For));
	if(strlen(UserName) > 8){
		strcat(EnterPWStr, "\n");
	}
	DefaultCtrl(gPanelHandle[PASSWDPNL], PASSWDPNL_PASSWORD);
	PasswordCtrl_SetAttribute(gPanelHandle[PASSWDPNL], PASSWDPNL_PASSWORD, ATTR_PASSWORD_VAL, "");

	SetCtrlAttribute(gPanelHandle[PASSWDPNL], PASSWDPNL_ERROR,    ATTR_VISIBLE,  FALSE);
	SetCtrlAttribute(gPanelHandle[PASSWDPNL], PASSWDPNL_USERNAME, ATTR_VISIBLE,  FALSE);
	SetCtrlVal(      gPanelHandle[PASSWDPNL], PASSWDPNL_USERNAME, UserName);
	SetCtrlAttribute(gPanelHandle[PASSWDPNL], PASSWDPNL_PASSWORD, ATTR_LABEL_TEXT, strcat(EnterPWStr, UserName));

	PasswordCtrl_ConvertFromString(gPanelHandle[PASSWDPNL], PASSWDPNL_PASSWORD);
	PasswordCtrl_SetAttribute(gPanelHandle[PASSWDPNL], PASSWDPNL_PASSWORD, ATTR_PASSWORD_MAX_LENGTH, MAX_PWD_LEN);

	SetPanelAttribute(gPanelHandle[LOGONPANEL], ATTR_FLOATING, VAL_FLOAT_NEVER);
	SetPanelAttribute(gPanelHandle[PASSWDPNL],  ATTR_FLOATING, VAL_FLOAT_ALWAYS);
	SetActivePanel(gPanelHandle[PASSWDPNL]);
	InstallPopup(gPanelHandle[PASSWDPNL]);

	return;
}	


/*****************************************************************************/
/* Name:     SelectUser_Wrapper                                                 
/*                                                                           
/* Purpose:  calls the function to validate a user selection (get password) from the logon user list
/*                                                                           
/* Example Call:   void CVICALLBACK SelectUser_Wrapper( int menuBarHandle,
/*         int menuItemID, void *callbackData, int panelHandle );                                    
/*                                                                           
/* Input:    standard menu callback parameters       
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
void CVICALLBACK SelectUser_Wrapper(int menuBarHandle,
         int menuItemID, void *callbackData, int panelHandle )
{
	char UserName[MAX_USERNAME_LEN + 1];
	int nStatus;

	nStatus = GetMenuBarAttribute(menuBarHandle, menuItemID, ATTR_ITEM_NAME, UserName);
	ValidateLogin(UserName);

	return;
}


/*****************************************************************************/
/* Name:     PopulateMenuFromRegistry	                                                      
/*                                                                           
/* Purpose:  Fills the user dropdown menu from the registry
/*                                          
/*                                                                           
/* Example Call:   void PopulateMenuFromRegistry(int Bar, int Menu);                                    
/*                                                                           
/* Input:    Bar - Menu bar handle to populate
/*			 Menu - Menu to populate
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
static int PopulateMenuFromRegistry(int Bar, int Menu)
{
	char* Userlist = NULL;
	char* Name     = NULL;
	int count;
	
	Userlist = GetUserList();
	count = 0;
	Name = strtok(Userlist, NameListDelimiter);
	do{
		if(Name != NULL){
			NewMenuItem(Bar, Menu, Name, -1, 0, SelectUser_Wrapper, Name);
			count++;
		}
	} while(Name = strtok(NULL, NameListDelimiter));

	free(Userlist);

	return count;
}


/*****************************************************************************/
/* Name:     UserMenu	                                                   
/*                                                                           
/* Purpose:  draws the user dropdown menu
/*                                                                           
/* Example Call:   void UserMenu(int panel, int control);                                    
/*                                                                           
/* Input:    panel - parent panel of new menu
/*			 control - control to position menu by
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
static void UserMenu(int panel, int control)
{
	int BarHandle, MenuHandle;
	int t, l, h, w;
	int nNumUsers;

	BarHandle = NewMenuBar(0);
	MenuHandle = NewMenu(BarHandle, "", -1);

	nNumUsers = PopulateMenuFromRegistry(BarHandle, MenuHandle);
	if(nNumUsers < MAX_USERS)
	{
		InsertSeparator(BarHandle, MenuHandle, -1);
		NewMenuItem(BarHandle, MenuHandle, LDBf(Ldb_Add_User), -1, 0, AddUser_Wrapper, NULL);
	}
	GetCtrlAttribute(panel, control, ATTR_TOP,    &t);
	GetCtrlAttribute(panel, control, ATTR_LEFT,   &l);
	GetCtrlAttribute(panel, control, ATTR_HEIGHT, &h);
	GetCtrlAttribute(panel, control, ATTR_WIDTH,  &w);

	RunPopupMenu(BarHandle, MenuHandle, panel, t+h, l, t, l, h, w);
	DiscardMenuBar(BarHandle);
	
	return;
}


/*****************************************************************************/
/* Name:     LogonPanel_Callback                                                      
/*                                                                           
/* Purpose:  panel callback to handle window exit 
/*                                                                           
/* Example Call:   int CVICALLBACK LogonPanel_Callback(int panel, int event, void *callbackData,
/*		int eventData1, int eventData2);                                    
/*                                                                           
/* Input:    standard callback parameters         
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   always 0                                                      
/*                                                                           
/*****************************************************************************/
int CVICALLBACK LogonPanel_Callback(int panel, int event, void *callbackData,
		int eventData1, int eventData2)
{
	switch(event){
		case EVENT_GOT_FOCUS:
			break;

		case EVENT_LOST_FOCUS:
			break;

		case EVENT_CLOSE:
			ExitWrapper();
			break;

		case EVENT_KEYPRESS:
			switch(eventData1){
				case VAL_UNDERLINE_MODIFIER | 'U':
				case VAL_UNDERLINE_MODIFIER | 'u':
					SetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_LOGIN_BUTTON, ATTR_DIMMED, TRUE);
					UserMenu(panel, LOGONPANEL_OPERATORS);
					break;

				case VAL_F1_VKEY:
				case VAL_F1_VKEY | VAL_SHIFT_MODIFIER:
					ShowHelp(panel, GetActiveCtrl(panel), eventData1);
					break;

				default:
					break;
			} // end switch(eventData1)
			break;
		// end case EVENT_KEYPRESS

		default:
			break;
	} // end switch(event)

	return 0;
}


/*****************************************************************************/
/* Name:    Passwd_Callback		                                                     
/*                                                                           
/* Purpose:  password prompt panel callback
/*                                                                           
/* Example Call:   int CVICALLBACK Passwd_Callback(int panel, int control, int event,
/*		void *callbackData, int eventData1, int eventData2);                                    
/*                                                                           
/* Input:    standard callback parameters       
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   always 0                                                         
/*                                                                           
/*****************************************************************************/
int CVICALLBACK Passwd_Callback(int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	char PassWord[MAX_PWD_LEN + 1];
	char UserName[MAX_USERNAME_LEN + 1];
	
	if(event == EVENT_COMMIT || (event == EVENT_KEYPRESS && eventData1 == VAL_ENTER_VKEY)){
		switch(control){
			case PASSWDPNL_PASSWORD:
				if(event == EVENT_COMMIT){
					break;
				}

				//OK, we have a keypress with VAL_ENTER_VKEY so fall through to the PASSWDPNL_OK_BUTTON code
			case PASSWDPNL_OK_BUTTON:
				GetCtrlVal(gPanelHandle[PASSWDPNL], PASSWDPNL_USERNAME, UserName);
				PasswordCtrl_GetAttribute(gPanelHandle[PASSWDPNL], PASSWDPNL_PASSWORD, ATTR_PASSWORD_VAL, PassWord);
				if(ValidatePassword(UserName, PassWord) == PWD_OK){
					SetCtrlAttribute(gPanelHandle[PASSWDPNL],  PASSWDPNL_ERROR,         ATTR_VISIBLE, FALSE);
					SetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_LOGIN_BUTTON, ATTR_DIMMED,  FALSE);

					strcpy(gCurrOperator, UserName);
					SetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_OPERATORS, ATTR_CTRL_VAL, gCurrOperator);

					LoadUserOptions(gCurrOperator);
					DiscardPanel(gPanelHandle[PASSWDPNL]);
					gPanelHandle[PASSWDPNL] = 0;

					redrawLogin();
				 	return 0; 
				}
				else{
					DefaultCtrl(gPanelHandle[PASSWDPNL], PASSWDPNL_PASSWORD);
					SetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_OPERATORS, ATTR_LABEL_TEXT, "");
					SetCtrlAttribute(gPanelHandle[PASSWDPNL],  PASSWDPNL_ERROR,      ATTR_VISIBLE,    TRUE);
				}	
				PasswordCtrl_SetAttribute(gPanelHandle[PASSWDPNL], PASSWDPNL_PASSWORD, ATTR_PASSWORD_VAL, "");
				break;

			case PASSWDPNL_CANCEL_BUTTON:
				SetCtrlAttribute(gPanelHandle[PASSWDPNL],  PASSWDPNL_ERROR,      ATTR_VISIBLE,    FALSE);
				SetCtrlAttribute(gPanelHandle[LOGONPANEL], LOGONPANEL_OPERATORS, ATTR_LABEL_TEXT, "");

				SetPanelAttribute(gPanelHandle[LOGONPANEL], ATTR_FLOATING,        VAL_FLOAT_ALWAYS);
				SetCtrlAttribute( gPanelHandle[LOGONPANEL], LOGONPANEL_OPERATORS, ATTR_CTRL_VAL, "");

				DiscardPanel(gPanelHandle[PASSWDPNL]);
				gPanelHandle[PASSWDPNL] = 0;

				return 0;
				//break;

			default:
				break;
		} // end switch(control)
	} // end if(event == EVENT_COMMIT || (event == EVENT_KEYPRESS && eventData1 == VAL_ENTER_VKEY))

	return 0;
}


/*****************************************************************************/
/* Name:    secondquit                                                    
/*                                                                           
/* Purpose:  Handles the logon panel exit button
/*                                          
/*                                                                           
/* Example Call:   int CVICALLBACK secondquit(int panel, int control, int event,
/*		void *callbackData, int eventData1, int eventData2);                                    
/*                                                                           
/* Input:    standard callback parameters  
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
int CVICALLBACK secondquit(int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch(event){
		case EVENT_COMMIT:
			   ExitWrapper();
			break;

		default:
			break;
	}

	return 0;
}

