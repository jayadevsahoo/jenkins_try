#ifndef BdSystemState_HH
#define BdSystemState_HH
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: BdSystemState - Keeps track on the state of the system for recovery purposes
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/BdSystemState.hhv   25.0.4.0   19 Nov 2013 13:59:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006  By:  gdc    Date:  12-Dec-2008    SCR Number: 5994
//  Project:  840S
//  Description:
//		Removed isSafetyPcvActive accessor as dead code.
//
//  Revision: 005  By: rpr     Date:  24-Oct-2008    SCR Number: 6435
//  Project:  S840
//  Description:
//      Added isCalibrateO2Requested_ to support +20 O2
//
//  Revision: 004  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 003  By:  iv    Date:  07-Feb-1997    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Rework per formal code review.
//
//  Revision: 002  By:  iv    Date:  15-Jan-1997    DR Number: DCS 1297
//       Project:  Sigma (R8027)
//       Description:
//             Deleted O2 interval timer from Novram.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#  include "Sigma.hh"
#  include "Breath_Delivery.hh"
#  include "SchedulerId.hh"

//@ Usage-Classes
class BreathPhase;
//@ End-Usage

class BdSystemState {
  public:
    BdSystemState(void);
    BdSystemState( const BdSystemState& rBdState);
    ~BdSystemState(void);

    static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL, 
			  const char*       pPredicate = NULL);
    void operator=( const BdSystemState& rBdState);
	inline void setSchedulerId(const SchedulerId::SchedulerIdValue id);
	inline void set100PercentO2Request(const Boolean is100PercentO2Requested);
	inline void setCalibrateO2Request(const Boolean isCalibrateO2Requested);
	inline void setApneaActiveStatus(const Boolean isApneaActive);
	inline void setSafetyPcvActiveStatus(const Boolean isSafetyPcvActive);
	inline SchedulerId::SchedulerIdValue getSchedulerId(void) const;
	inline Boolean is100PercentO2Requested(void) const;
	inline Boolean isCalibrateO2Requested(void) const;
	inline Boolean isApneaActive(void) const;

  protected:

  private:

	//@ Data-Member isApneaActive_
	// indicates whether the active breathing mode was apnea.
	Boolean isApneaActive_;

	//@ Data-Member isSafetyPcvActive_
	// indicates whether the active breathing mode was safetyPcv.
	Boolean isSafetyPcvActive_;

	//@ Data-Member schedulerId_
	// The id of the active scheduler.
	SchedulerId::SchedulerIdValue schedulerId_;

	//@ Data-Member is100PercentO2Requested_
	// Indicates if 100% O2 is active.
	Boolean is100PercentO2Requested_;

	//@ Data-Member isCalibrateO2Requested_
	// Indicates if Calibration of O2 is active.
	// during Neonatal circuit use
	Boolean isCalibrateO2Requested_;
};



// Inlined methods...
#include "BdSystemState.in"


#endif // BdSystemState_HH 
