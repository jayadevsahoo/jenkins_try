#ifndef PatientDataCascade_HH
#define PatientDataCascade_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1994, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: PatientDataCascade - A PatientDataTarget which stores a list
// of PatientDataTargets.  Allows multiple PatientDataTargets to
// register for changes to a single piece of patient data.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/PatientDataCascade.hhv   25.0.4.0   19 Nov 2013 14:08:12   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "PatientDataTarget.hh"

//@ Usage-Classes
#include "PatientDataId.hh"
#include "GuiAppClassIds.hh"
//@ End-Usage


class PatientDataCascade : public PatientDataTarget
{
public:
	PatientDataCascade(void);
	~PatientDataCascade(void);

	// Other PatientDataTarget's register with this method.
	void addTarget(PatientDataTarget *pTarget);

	// Patient Data change notices are communicated through here.
	void patientDataChangeHappened(
							PatientDataId::PatientItemId patientDataId);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:

private:
	// these methods are purposely declared, but not implemented...
	PatientDataCascade(const PatientDataCascade&);	// not implemented...
	void operator=(const PatientDataCascade&);		// not implemented...

	enum
	{
		//@ Constant: PDC_MAX_TARGETS
		// The maximum number of target pointers that can be stored by this
		// cascade
		PDC_MAX_TARGETS = 10
	};

    //@ Data-Member: targets_
    // The array which holds pointers to all of the targets.
    PatientDataTarget *targets_[PDC_MAX_TARGETS];

    //@ Data-Member: numTargets_
    // The number of targets stored in 'targets_[]'.
    Uint numTargets_;
};


#endif // PatientDataCascade_HH 
