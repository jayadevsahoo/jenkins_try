
#ifndef BatchBoundedSetting_HH
#define BatchBoundedSetting_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  BatchBoundedSetting - Batch Bounded Setting.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/BatchBoundedSetting.hhv   25.0.4.0   19 Nov 2013 14:27:14   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006   By: gdc    Date: 24-Sep-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support changeable IBW.
//
//  Revision: 005 By: srp    Date: 28-May-2002   DR Number: 5898
//  Project:  VCP
//	Description:
//		Incorporated Remediation code review issue(s) -- commentary changes only.
//
//  Revision: 004   By: sah  Date:  3-Jan-2000    DR Number: 5327
//  Project:  NeoMode
//  Description:
//  Updated for NeoMode
//
//  Revision: 003   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 002   By: sah    Date:  07-May-1997    DCS Number: 2034
//  Project: Sigma (R8027)
//  Description:
//	Fixed problem with new-patient setup producing "clipped" tidal
//	volumes, by supplying one new method for resetting the minimum
//	and maximum constraints for each batch, bounded setting (see
//	'resetBoundConstraints()').
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//====================================================================

#include "BasedOnCategory.hh"

//@ Usage-Classes
#include "BoundedSetting.hh"
#include "BoundedRange.hh"
//@ End-Usage


class BatchBoundedSetting : public BoundedSetting 
{
  public:
    virtual ~BatchBoundedSetting(void);

    virtual Boolean  isChanged(void) const;

    virtual SettingValue  getDefaultValue   (void) const;
    virtual SettingValue  getNewPatientValue(void) const = 0;

	// Setting virtual
	virtual void updateToNewIbwValue(void);

    virtual void  acceptTransition(const SettingId::SettingIdType settingId,
                                   const DiscreteValue            newValue,
                                   const DiscreteValue            currValue);

    static void   SoftFault(const SoftFaultID softFaultID,
			    const Uint32      lineNumber,
			    const char*       pFileName  = NULL, 
			    const char*       pPredicate = NULL);

  protected:
    BatchBoundedSetting(const SettingId::SettingIdType  settingId,
			const SettingId::SettingIdType* arrDependentSettingIds,
		        const BoundedInterval*          pIntervalList,
		        const SettingBoundId            maxConstraintId,
		        const SettingBoundId            minConstraintId,
		        const Boolean                   useEpsilonFactoring = TRUE);

    virtual Boolean  resolveDependentChange_(
			const SettingId::SettingIdType dependentId,
			const Boolean                  isValueIncreasing
					    );

    virtual BoundedValue  calcBasedOnSetting_(
			    const SettingId::SettingIdType basedOnSettingId,
			    const BoundedRange::WarpDir    warpDirection,
			    const BasedOnCategory          basedOnCategory
					     ) const;

#if defined(SIGMA_DEVELOPMENT)
    virtual Ostream&  print_(Ostream& ostr) const;
#endif  // defined(SIGMA_DEVELOPMENT)

  private:
    BatchBoundedSetting(const BatchBoundedSetting&);	// not implemented...
    BatchBoundedSetting(void);			// not implemented...
    void operator=(const BatchBoundedSetting&);	// not implemented...
};


#endif // BatchBoundedSetting_HH 
