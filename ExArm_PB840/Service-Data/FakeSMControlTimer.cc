#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
// Class: FakeSMControlTimer - Fakes out the interface between the
// Service Data subsystem and GUI.  Generates randomly changing
// pieces of Service Data.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Data/vcssrc/FakeSMControlTimer.ccv   25.0.4.0   19 Nov 2013 14:21:02   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By:  yyy    Date:  16-May-95   DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//=====================================================================


#if defined FAKE_SM
// This class is used only for test simulation.

#include "Sigma.hh"
# include <stdlib.h>
#include "FakeSMControlTimer.hh"

#ifdef SIGMA_TARGET		// FORNOW - for random() on the target
	extern "C" { int rand(); }
#	define random rand
#endif // SIGMA_TARGET

//@ Usage-Classes
#include "SDTimerId.hh"
#include "SDTimerRegistrar.hh"
#include "FakeServiceModeManager.hh"
//@ End-Usage

//@ Code...


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: FakeSMControlTimer()  [Default Constructor]
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

FakeSMControlTimer::FakeSMControlTimer(void)
{
	CALL_TRACE("FakeSMControlTimer::FakeSMControlTimer(void)");

	//
	// Register for timer event callbacks.
	//
	SDTimerRegistrar::RegisterTarget(SDTimerId::SERVICE_DATA_TIMER, this);

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~FakeSMControlTimer  [Destructor]
//
//@ Interface-Description
//  none
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

FakeSMControlTimer::~FakeSMControlTimer(void)
{
	CALL_TRACE("FakeSMControlTimer::~FakeSMControlTimer(void)");
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: timerEventHappened
//
//@ Interface-Description
//  none
//---------------------------------------------------------------------
//@ Implementation-Description
//  Callback for timer event, a concrete implementation of a pure
//  virtual function.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void 
FakeSMControlTimer::timerEventHappened(SDTimerId::SDTimerIdType)
{
	CALL_TRACE("FakeSMControlTimer::timerEventHappened(SDTimerId::SDTimerIdType)");

#if defined FORNOW	
printf("FakeSMControlTimer::timerEventHappened()\n");
#endif	// FORNOW

	SDTimerRegistrar::CancelTimer(SDTimerId::SERVICE_DATA_TIMER);

	// Proceed to the next Service Mode test procedure.
	FakeServiceModeManager::sMTimerEventHappened();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: startTimer
//
//@ Interface-Description
//  none
//---------------------------------------------------------------------
//@ Implementation-Description
//  Callback for timer event, a concrete implementation of a pure
//  virtual function.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void 
FakeSMControlTimer::startTimer(void)
{
	CALL_TRACE("FakeSMControlTimer::startTimer(void)");

#if defined FORNOW	
printf("FakeSMControlTimer::startTimer()\n");
#endif	// FORNOW

	SDTimerRegistrar::StartTimer(SDTimerId::SERVICE_DATA_TIMER, this);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: stopTimer
//
//@ Interface-Description
//  none
//---------------------------------------------------------------------
//@ Implementation-Description
//  Callback for timer event, a concrete implementation of a pure
//  virtual function.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void 
FakeSMControlTimer::stopTimer(void)
{
	CALL_TRACE("FakeSMControlTimer::stopTimer(void)");

#if defined FORNOW	
printf("FakeSMControlTimer::stopTimer()\n");
#endif	// FORNOW

	SDTimerRegistrar::CancelTimer(SDTimerId::SERVICE_DATA_TIMER);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
FakeSMControlTimer::SoftFault(const SoftFaultID  softFaultID,
		   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)  
{
	CALL_TRACE("FakeSMControlTimer::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, SERVICE_DATA,
					FAKE_SM_CONTROL_TIMER, lineNumber, pFileName, pPredicate);
}


#endif	// FAKE_SM
