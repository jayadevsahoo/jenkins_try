#include "stdafx.h"
//------------------------------------------------------------------------------
//                   Copyright (c) 2008 - 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
/// @file Allocator.cc
///
/// This file contains definition of Allocator class. This is a fixed block
/// allocator with macros to overload new and delete within each class. A 'new'
/// will pull data from the pool and 'delete' will save the block on an
///	internal free list stack for use later usage.
///
/// Using the macros, you can tell allocator how many instances to allow, or
/// alternatively setting a pool count of 0 means allocator can go to the
/// global 'new' to get a fresh block if one of the stack doesn't already exist.
///
/// Allocator is thread-safe. A mutex is locked during brief critical sections.
///
/// To use allocator, just place one macro within a class definition inside
//	a header file like this:
/// DECLARE_ALLOCATOR(MyClass);
///
/// Place another macro in the implementation file like this to define a
/// maximum number of instances allowed for the class:
/// IMPLEMENT_ALLOCATOR(MyClass, 10);	// allows 10 instances max of MyClass
///
/// Setting an object count up front declares a fixed pool to draw from.
//  Alternatively, set the count to 0 and allocator will go to the to heap
/// whenever the allocator free list stack cannot provide an existing object
/// storage location.
/// IMPLEMENT_ALLOCATOR(MyClass, 0);	// allows unlimited instances of MyClass
///
/// If you want an entire inheritance hierarchy to use the same fixed block
/// pool (useful if you have a lot of object, all with different sizes) define
/// a maximum pool size within the base class only. All derived classes will
/// automatically use the base class's overloaded new/delete and draw from the
/// same pool.
/// IMPLEMENT_ALLOCATOR_SETSIZE(MyBaseClass, 0, 128); // 128 byte max block size
//------------------------------------------------------------------------------
#include <new.h>
#include <Windows.h>
#include <tchar.h>
#include "allocator.hh"
// TODO e600 SWAT, enable or remove
//
//#include "NetworkLoggerMacros.hh"
//#include "tx_api.h"
//

#include "pset.hh"
#include "UtilityClassId.hh"

SOFT_FAULT_IMPL( Allocator, TOOLS, ALLOCATOR )
Boolean Allocator::m_mutexInitialized = FALSE;
static HANDLE _mutex;
const Uint Allocator::MIN_ONE_TIME_DELETE_BLOCK_SIZE = 32768;

//------------------------------------------------------------------------------
// Allocator_Init
// This function must be called exactly one time *before* the operating system
// scheduler starts. When the system is still single threaded at startup,
// the Allocator API does not need mutex protection.
//------------------------------------------------------------------------------
void Allocator_Init()
{
	Allocator::Init();
}

//------------------------------------------------------------------------------
// operator()
//------------------------------------------------------------------------------
bool AllocatorSizeCompare::operator ()(Allocator* a1, Allocator* a2)
{
	// Allow allocators with the same block size. If block matches, then just use
	// the allocator pointers for the comparison. Otherwise, just use the block size.
	if (a1->GetBlockSize() == a2->GetBlockSize())
		return (a1 < a2);
	else
		return (a1->GetBlockSize() < a2->GetBlockSize());
}

//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
Allocator::Allocator(size_t size, Uint16 objects, const Char* name) :
    m_blockSize(size < sizeof(long*) ? sizeof(long*):size),
    m_objectSize(size),
    m_maxObjects(objects),
    m_pHead(NULL),
    m_poolIndex(0),
    m_blockCnt(0),
    m_blocksInUse(0),
    m_allocations(0),
    m_deallocations(0),
    m_name(name)
{
	MutexLock();
	GetAllocators().insert(this);
	MutexUnlock();

    // if using a fixed memory pool then create it
    if (m_maxObjects)
        m_pPool = (Char*)new Char[m_blockSize * m_maxObjects];
}

//------------------------------------------------------------------------------
// Destructor
//------------------------------------------------------------------------------
Allocator::~Allocator()
{
	// Allocator destructor should never be called. Memory should never be
	// returned to the heap.
	CLASS_ASSERTION_FAILURE();

	// if using pool then destroy it, otherwise traverse
	// free-list and destroy each individual block
	if (m_maxObjects)
		delete [] m_pPool;
	else
	{
		while(m_pHead)
			delete [] (Char*)Pop();
	}
}

//------------------------------------------------------------------------------
// Init
//------------------------------------------------------------------------------
void Allocator::Init()
{
	MutexInit();
}


std::set<Allocator*, AllocatorSizeCompare>& Allocator::GetAllocators()
{
    
    // Normally we'd use a pset (the p version for "pool" to eliminate
    // any dynamic storage allocation deletes). However, pset uses
    // Allocator so there is a circular reference. We only insert into the
    // set, never erase, so freeing of memory will not occur.
    static std::set<Allocator*, AllocatorSizeCompare> allocators;

    return allocators;
}

//------------------------------------------------------------------------------
// OneTimeBlockDelete
//------------------------------------------------------------------------------
void Allocator::OneTimeBlockDelete()
{
	MutexLock();

	std::set<Allocator*, AllocatorSizeCompare>::iterator it;
	for (it = GetAllocators().begin(); it != GetAllocators().end(); it++)
	{
		// only delete blocks for allocators that have no blocks in use,
		// that have blocks created and the block size is above a certain level
		if ((*it)->GetBlocksInUse() > 0 ||
			(*it)->GetBlockCount() == 0U ||
			(*it)->GetBlockSize() < MIN_ONE_TIME_DELETE_BLOCK_SIZE)
			continue;

		// delete and return to the global heap all the blocks owned by this allocator
		(*it)->DeleteAllBlocks();
	}

	MutexUnlock();
}

//------------------------------------------------------------------------------
// DeleteAllBlocks
//------------------------------------------------------------------------------
void Allocator::DeleteAllBlocks()
{
	MutexLock();

	// if using pool then just return
	if (m_maxObjects)
		return;

	// traverse the free-list and destroy each individual block
	while(m_pHead)
		delete [] (Char*)Pop();

#ifdef WEBSERVICES
	m_blockCnt = 0;
	m_blocksInUse = 0;
	m_allocations = 0;
	m_deallocations = 0;
#endif

	MutexUnlock();
}

//------------------------------------------------------------------------------
/// Called exactly one time to create the Allocator mutex.
/// @pre        None
/// @post       None
//------------------------------------------------------------------------------
void Allocator::MutexInit()
{
    _mutex = CreateMutex(NULL, FALSE, _T("Allocator"));
    CLASS_ASSERTION(_mutex != NULL);
    m_mutexInitialized = TRUE;
}

//------------------------------------------------------------------------------
/// Call to lock the mutex.
/// @pre        None
/// @post       None
//------------------------------------------------------------------------------
void Allocator::MutexLock()
{
    // if mutex is not initialized we're still single threaded
    if (m_mutexInitialized == FALSE)
    	return;

    DWORD status = WaitForSingleObject(_mutex, INFINITE);
    
    CLASS_ASSERTION(status == WAIT_OBJECT_0);
}

//------------------------------------------------------------------------------
/// Call to unlock the mutex.
/// @pre        None
/// @post       None
//------------------------------------------------------------------------------
void Allocator::MutexUnlock()
{
    // if mutex is not initialized we're still single threaded
    if (m_mutexInitialized == FALSE)
    	return;

    BOOL status = ReleaseMutex(_mutex);
    CLASS_ASSERTION(status);
}

//------------------------------------------------------------------------------
// Allocate
//------------------------------------------------------------------------------
void* Allocator::Allocate(size_t size)
{
    CLASS_ASSERTION(size <= m_objectSize);

    // if can't obtain existing block then get a new one
    void* pBlock = Pop();
    if (!pBlock)
    {
        // if using pool method then get block from pool,
        // otherwise using dynamic so get block from heap
        if (m_maxObjects)
        {
        	MutexLock();

            // if we have not exceeded the pool maximum
            if(m_poolIndex < m_maxObjects)
            {
                pBlock = (void*)(m_pPool + (m_poolIndex++ * m_blockSize));
                MutexUnlock();
            }
            else
            {
            	MutexUnlock();

                // get the pointer to the new handler
                new_handler handler = set_new_handler(0);
                set_new_handler(handler);

                // if a new handler is defined, call it
                if (handler)
                    (*handler)();
                else
                    CLASS_ASSERTION_FAILURE();
            }
        }
        else
        {
#ifdef WEBSERVICES
        	m_blockCnt++;
#endif
            pBlock = (void*)new Char[m_blockSize];
        }
    }
#ifdef WEBSERVICES
    m_blocksInUse++;
    m_allocations++;
#endif
    return pBlock;
}

//------------------------------------------------------------------------------
// Deallocate
//------------------------------------------------------------------------------
void Allocator::Deallocate(void* pBlock)
{
    Push(pBlock);
#ifdef WEBSERVICES
	m_blocksInUse--;
	m_deallocations++;
#endif
}

//------------------------------------------------------------------------------
// Push
//------------------------------------------------------------------------------
void Allocator::Push(void* pMemory)
{
	MutexLock();

    Block* pBlock = (Block*)pMemory;
    pBlock->pNext = m_pHead;
    m_pHead = pBlock;

    MutexUnlock();
}

//------------------------------------------------------------------------------
// Pop
//------------------------------------------------------------------------------
void* Allocator::Pop()
{
    Block* pBlock = NULL;
	MutexLock();

    if (m_pHead)
    {
        pBlock = m_pHead;
        m_pHead = m_pHead->pNext;
    }

    MutexUnlock();
    return (void*)pBlock;
}

