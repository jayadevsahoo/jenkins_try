#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: TextArea - Implements an enclosed pageable area that allows
// viewing of a body of text.
//---------------------------------------------------------------------
//@ Interface-Description
// TextArea provides a simple method to view a large amount of text.  It
// is constructed with a fixed width and height.  A call to setLines()
// points TextArea to the array of text line information that it must
// display.  TextArea will display arrow(s) by the text title if there is
// more text to display than can be contained in its height.
//
// The scrollHappened() method is called whenever paging operation is
// detected and allows us to scroll the text display accordingly.
//---------------------------------------------------------------------
//@ Rationale
// Very useful anywhere a large amount of text needs to be displayed, e.g.
// on the help subscreens.
//---------------------------------------------------------------------
//@ Implementation-Description
// Whenever TextArea is given a new set of lines to display it first scans
// the lines (in processLines_()) and calculates and stores the relative
// Y position that each line should be displayed.  These Y positions can
// go far beyond the selected height of the TextArea and are subsequently
// used for determining where each line is displayed onscreen.
//
// The scrollHappened() method looks after displaying of all text.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/TextArea.ccv   25.0.4.0   19 Nov 2013 14:08:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003   By: hhd    Date:  16-Nov-1999    DCS Number: 5327
//  Project: Neo-Mode
//  Description:
// 		Modified as part of a general cleanup effort for Neo-Mode.
//
//  Revision: 002  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 001  By:  mpm    Date:  14-JUL-95    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

//
// Sigma includes.
//
#include "Sigma.hh"

//
// GUI-App includes.
//
#include "TextArea.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

const Int32 TextArea::LINE_SPACING_ = 3;

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TextArea()  [Constructor]
//
//@ Interface-Description
// Constructs a TextArea.  Passed two parameters, the width and height of
// the TextArea.
//---------------------------------------------------------------------
//@ Implementation-Description
// Initialize all data members.  Set the TextArea container's width and height
// to the input parameters.  Position and add the TextFields
// which display each line of text.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

TextArea::TextArea(Uint16 textWidth, Uint16 textHeight) :
		pTextLines_(NULL),
		numTextLines_(0),
		currentPage_(-1),
		numPages_(0),
		textHeight_(textHeight)
{
	CALL_TRACE("TextArea::TextArea(Uint16 width, Uint16 height)");

	// Size and position the sub-screen
	setWidth(textWidth);
	setHeight(textHeight);

	Uint16 i;
	for (i = 0; i < MAX_TEXT_LINE_FIELDS_; i++)
	{									// $[TI1]
		textLineFields_[i].setColor(Colors::WHITE);
		addDrawable(&textLineFields_[i]);
	}									// $[TI2]

	for (i = 0; i < MAX_PAGES_; i++)
	{									// $[TI3]
		pageInfoArray_[i].numberOfLines = 0;
		pageInfoArray_[i].firstLineIndex = 0;
	}									// $[TI4]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~TextArea  [Destructor]
//
//@ Interface-Description
// Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

TextArea::~TextArea(void)
{
	CALL_TRACE("TextArea::~TextArea(void)");

	// Do nothing.
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: scrollHappened
//
//@ Interface-Description
// Called whenever paging is detected.  We are passed 'direction' which
// indicates whether forward paging or backward paging is required.
// Also called whenever setLines() is called allowing for initialization 
// of the textual display.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
TextArea::scrollHappened(Uint16 direction)
{
	CALL_TRACE("TextArea::scrollHappened(Uint16 direction)");

	// If scroll is not possible, i.e, there is no more page to scroll 
	// simply return.
	switch(direction)
	{
		case SCROLL_BACKWARD:
		{										// $[TI1]
			if (prevPage())
			{									// $[TI1.1]
				--currentPage_;
			}
			else
			{									// $[TI1.2]
				return;
			}
			break;
		}
		case SCROLL_FORWARD:
		{										// $[TI2]
			if (nextPage())
			{									// $[TI2.1]
				++currentPage_;
			}
			else
			{									// $[TI2.2]
				return;
			}
			break;
		}
		default:								// $[TI3]
			break;
	}
#ifdef SIGMA_DEBUG
printf("In scrollHappened, currentPage_=%d, total lines=%d, firstLine=%d \n", currentPage_, pageInfoArray_[currentPage_].numberOfLines, pageInfoArray_[currentPage_].firstLineIndex
);
#endif

	Uint16 firstLineIndex = pageInfoArray_[currentPage_].firstLineIndex;

	Uint16 i;
	for (i = firstLineIndex;
				i < firstLineIndex+pageInfoArray_[currentPage_].numberOfLines; 
				i++)
	{											// $[TI4]
		// Prepare text fields for display
#ifdef SIGMA_DEBUG
		printf("textLineFields_[%d].setText: %s\n", i-firstLineIndex, pTextLines_[i].P_TEXT);
		printf("textLineFields_[%d].setY: %d\n", i-firstLineIndex, pTextLines_[i].yPosition);
#endif
		textLineFields_[i-firstLineIndex].setText(pTextLines_[i].P_TEXT);
		textLineFields_[i-firstLineIndex].setY(pTextLines_[i].yPosition);
		textLineFields_[i-firstLineIndex].setShow(TRUE);
	}											// $[TI5]
#ifdef SIGMA_DEBUG
printf("j=%d\n", i - firstLineIndex);
#endif
	// Hide any text fields that are not being used for display.
	for (int j = i - firstLineIndex; j < MAX_TEXT_LINE_FIELDS_; j++)
	{											// $[TI6]
		textLineFields_[j].setShow(FALSE);
	}											// $[TI7]

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setLines
//
//@ Interface-Description
// Sets the array of text lines that will be displayed by TextArea.  Passed
// a pointer to the array.  A pointer value of NULL will cause TextArea
// to display a blank screen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Store the input parameter, call processLines_() to scan the line data
// and call scrollHappened() to display the first page.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
TextArea::setLines(TextAreaLine *pTextLines)
{
	CALL_TRACE("TextArea::setLines(TextAreaLine *pTextLines)");

	pTextLines_ = pTextLines;		// Store parameter
	processLines_();				// Process lines
#ifdef SIGMA_DEBUG
printf("In TextArea::setLines, line %d\n", __LINE__);
#endif
	scrollHappened(SCROLL_FORWARD);		// Display first page
													// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processLines_		[Private]
//
//@ Interface-Description
// Does preprocessing of text lines before they are displayed.  The processing
// is needed to determine the Y positions of each line relative to the origin
// of the TextArea.
//---------------------------------------------------------------------
//@ Implementation-Description
// We first count the number of lines in the array (the last line of text
// in the array is followed by a NULL entry).  If the set of lines have not
// been scanned before, we process them (we can tell if they've been scanned
// before because the first line will have a non-zero Y position).
//
// Processing involves rendering each line to determine its absolute
// height and using this value to determine the Y position of the next line.
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
TextArea::processLines_(void)
{
	Uint16 i;
	// If it's non-null then go process lines for display
	if (pTextLines_ != NULL)
	{												// $[TI1]
		// Count lines
		for (numTextLines_ = 0;
				pTextLines_[numTextLines_].P_TEXT != NULL; numTextLines_++)
		{											// $[TI2]
			// Do nothing
		}											// $[TI3]

		// Check to see if text lines have previously been positioned or
		// not (the Y position of the first line should be non-zero if
		// previously positioned).

		// Update page information
		currentPage_ = -1;
		numPages_ = 0;

		if (0 == pTextLines_[0].yPosition)
		{											// $[TI4]

			// Not positioned before.  Let's do it.
			Uint16 currY = TOP_OF_HELP_PAGE_Y_;
			Uint16 yOffset = 0;

			// Go position each line on each page.
			for (i = 0; i < numTextLines_; i++)
			{										// $[TI5]
#ifdef SIGMA_DEBUG
printf("i=%d, numTextLines_=%d\n", i, numTextLines_);
#endif
				// Let's calculate what the height offset between the current line
				// and next one should be.  That will be 'current line's height +
				// line spacing'.
				// Use first element of textLineFields_[] to do current line
				// height calculation.
				textLineFields_[0].setText(pTextLines_[i].P_TEXT);
				yOffset = textLineFields_[0].getHeight() + LINE_SPACING_;

				if ((currY + yOffset) > textHeight_)
				{									// $[TI6]
					currY = pTextLines_[i].yPosition = TOP_OF_HELP_PAGE_Y_;

					// Current page is full.  So fill up the page information
					// on the current page then start a new page.
					pageInfoArray_[numPages_].numberOfLines = i - 
									pageInfoArray_[numPages_].firstLineIndex;

#ifdef SIGMA_DEBUG
printf("Current page=%d  firstLineIndex=%d, total lines=%d\n", numPages_, pageInfoArray_[numPages_].firstLineIndex, pageInfoArray_[numPages_].numberOfLines);
printf("END OF PAGE\n");
#endif
					pageInfoArray_[++numPages_].firstLineIndex = i;
				}									// $[TI7]
				pTextLines_[i].yPosition = currY;

				// Do next Y position calculation
				currY += yOffset;

#ifdef SIGMA_DEBUG
printf("pTextLines_[%d].text= %s\n", i, pTextLines_[i].P_TEXT);
printf("pTextLines_[%d].yPosition = %d\n", i, pTextLines_[i].yPosition);
#endif
			}  /* for */							// $[TI8]

			pageInfoArray_[numPages_].numberOfLines = i - 
								pageInfoArray_[numPages_].firstLineIndex;
#ifdef SIGMA_DEBUG
printf("NO MORE LINES, Current page=%d  firstLineIndex=%d, total lines=%d\n", numPages_, pageInfoArray_[numPages_].firstLineIndex, pageInfoArray_[numPages_].numberOfLines);
#endif
			numPages_++;


			// The last value of currY will essentially be the total height
			// of all the text in the area.  Store this value in the null
			// entry at the end of the pTextLines_ array.
			pTextLines_[numTextLines_].yPosition = currY;
#ifdef SIGMA_DEBUG
for (Uint16 j=0; j<numPages_;j++)
{
	printf("Page #%d: total lines: %d firstLineIndex: %d\n", j, 
							 pageInfoArray_[j].numberOfLines,
							 pageInfoArray_[j].firstLineIndex
							 );
}
#endif
		}											
		else // Text position has been calculated.  Need to fill pageInfo_ data
		{											// $[TI9]
			// Initialize pageInfoArray.
			for (i = 0; i < MAX_PAGES_; i++)
			{										// $[TI10]
				pageInfoArray_[i].numberOfLines = 0;
				pageInfoArray_[i].firstLineIndex = 0;
			}										// $[TI11]
			numPages_ = 0;

			for (i = 0; i < numTextLines_; i++)
			{										// $[TI12]
				if (pTextLines_[i].yPosition == TOP_OF_HELP_PAGE_Y_)
				{									// $[TI13]
					pageInfoArray_[numPages_].firstLineIndex = i;
					if (numPages_ > 0)
					{								// $[TI14]
						pageInfoArray_[numPages_-1].numberOfLines = 
							i - pageInfoArray_[numPages_-1].firstLineIndex;
					}								// $[TI15]
					numPages_++;
				}									// $[TI16]
			}										// $[TI17]
			// Last page data has not been processed since end of text is
			// reached.
			pageInfoArray_[numPages_-1].numberOfLines = 
					i - pageInfoArray_[numPages_-1].firstLineIndex;
#ifdef SIGMA_DEBUG
for (Uint16 j=0; j<numPages_;j++)
{
	printf("Page #%d: total lines: %d firstLineIndex: %d\n", j, 
							 pageInfoArray_[j].numberOfLines,
							 pageInfoArray_[j].firstLineIndex
							 );
}
#endif
		}
	}
	else
	{												// $[TI18]
		// No text lines.  Reset display.
		numTextLines_ = 0;
		numPages_ = 0;
		for (int i = 0; i < MAX_TEXT_LINE_FIELDS_; i++)
		{											// $[TI19]
			textLineFields_[i].setShow(FALSE);
		}											// $[TI20]
	}
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
TextArea::SoftFault(const SoftFaultID  softFaultID,
						 const Uint32       lineNumber,
						 const char*        pFileName,
						 const char*        pPredicate)  
{
	CALL_TRACE("TextArea::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, TEXTAREA,
							lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)
