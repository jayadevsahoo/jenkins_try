#ifndef	DciValueStatus_HH
#define	DciValueStatus_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ====================================================================
//@ Class: DciValueStatus - Store a validity tag (base class for DciNumber
// and DciString objects)
//---------------------------------------------------------------------
//@ Version-Information
// @ $Header:   /840/Baseline/DCI/vcssrc/DciValueStatus.hhv   25.0.4.0   19 Nov 2013 14:01:58   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002    By: sah     Date: 15-Jan-1999  DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' to a non-inlined method.
//
//  Revision  001	By: jdm	Date: 8/15/95	DR Number:
//	Project:    Sigma (R8027)
//	Description:
//		Initial version to coding standards (Integration baseline).
//=====================================================================

#include "DciClassIds.hh"

//@ Usage-Classes
#include "DciDebug.hh"
//@ End-Usage


class DciValueStatus
{
    public:
		// supported statuses (tags)
	enum DciValueStatusId
	{
		OK,			// value is good
		INVALID,		// value is unavailable or unreasonable
					// NOTE: just-instantiated and destroyed
					//	 objects are also INVALID
		NOT_SUPPORTED,		// value is not supported by Sigma
		NOT_IN_THIS_RELEASE,	// value is not supported by Sigma now
		TO_BE_DETERMINED	// value is an outstanding s/w issue
	};

		inline		DciValueStatus(void);
		inline		~DciValueStatus(void);

		static	void	SoftFault(const SoftFaultID	softFaultID,
								  const	Uint32		lineNumber,
								  const	char *		pFileName = NULL,
								  const	char *		pPredicate = NULL);

		inline	DciValueStatusId getStatus(void);

		inline	void	setOk(void);
		inline	void	setInvalid(void);
		inline	void	setNotSupported(void);
		inline	void	setNotInThisRelease(void);
		inline	void	setToBeDetermined(void);

		inline	Boolean	isOk(void);
		inline	Boolean	isInvalid(void);
		inline	Boolean	isNotSupported(void);
		inline	Boolean	isNotInThisRelease(void);
		inline	Boolean	isToBeDetermined(void);

    protected:

    private:
				// these are purposely not implemented...
			    // TODO E600 commented out to compile
				//operator=(DciValueStatus &);	// n. i.

				// the much-vaunted status (or tag)
			DciValueStatusId status_;
};


// inlined methods...
#include "DciValueStatus.in"

#endif	// DciValueStatus_HH
