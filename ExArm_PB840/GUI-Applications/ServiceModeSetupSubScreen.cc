#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: ServiceModeSetupSubScreen - The sub-screen in the Lower screen
// selected by pressing the OtherScreen tab button followed by the
// ServiceMode Setup button.  It displays the Nominal Line Voltage, System
// Test Baud Rate, Contrast Delta and Pressure Units settings buttons
// to allow the operator to adjust service settings.
//---------------------------------------------------------------------
//@ Interface-Description
// The subscreen contains a setting button for each of the above settings.
// If a setting is modified, the Accept key must be pressed in order to 
// accept and apply the settings changes.
//
// The activateHappened()/deactivateHappened() methods are called automatically on
// display/removal of this subscreen by our subscreen area owner.  The
// acceptHappened() method is called by the BatchSettingsSubScreen when
// the settings are being accepted.  buttonDownHappened() and
// buttonUpHappened() are called when the contrast delta button is
// pressed.  Timer events (specifically the subscreen setting change
// timeout event) are passed into timerEventHappened() and Adjust Panel
// events are communicated via the various Adjust Panel "happened"
// methods.  Settings events result in a call to
// valueUpdateHappened().
//
//---------------------------------------------------------------------
//@ Rationale
// Groups the components of the ServiceMode Setup subscreen in a single class.
//---------------------------------------------------------------------
//@ Implementation-Description
// The class creates setting buttons and most of its work is to inform the
// Setting subsystem of the setting changes for the selected setting button.
// To detect modification of the setting changes we use the ContextObserver
// to register for update notices when any of the settings change.  
// If the Accept key is pressed then the BatchSettingsSubScreen will call 
// the acceptHappened() method to accept the setting changes and deactivate 
// the subscreen.
// If no user activity on the GUI occurs within 3 minutes then the Setup
// timeouts, it abandons all settings adjustments and reverts the value to
// the original settings.
//---------------------------------------------------------------------
//@ Fault-Handling
// none
//---------------------------------------------------------------------
//@ Restrictions
// Only one instance of this class should be created (by LowerScreen).
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/ServiceModeSetupSubScreen.ccv   25.0.4.0   19 Nov 2013 14:08:26   pvcs  $
//
//@ Modification-Log
//
//  Revision: 014  By: gdc     Date: 26-May-2007     SCR Number: 6330
//  Project:  Trend
//	Description:
//  Removed SUN prototype code.
//
//  Revision: 013   By: gdc    Date:  20-Sep-2000    DCS Number: 5769
//  Project: GuiComms
//  Description:
// 		Added 38400 baud rate.
//
//  Revision: 012   By: sah    Date: 23-May-2000     DCS Number:  5755
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      *  re-designed interface to discrete setting value strings, whereby
//         the point-size and style are inserted at run-time
//
//  Revision: 008   By: hhd    Date:  16-Nov-1999    DCS Number: 5327
//  Project: Neo-Mode
//  Description:
// 		Modified as part of a general cleanup effort for Neo-Mode.
//
//  Revision: 007  By:  sah	   Date:  23-Feb-1999    DCS Number:  5314
//  Project:  ATC
//  Description:
//      Added integrated with new dynamic, setting applicability functionality.
//      This allows for looser coupling between settings and GUI, and the
//      need for significantly less "setting knowledge" in the GUI subsystem.
//
//  Revision: 006  By:  gdc    Date:  02-Sep-1998    DCS Number: 5153
//  Project:  Color
//  Description:
//      Added traceable ID's for Color Project requirements.
//
//  Revision: 005  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 004  By:  sah   Date:  18-Sep-1997    DCS Number:  2390
//  Project:  Sigma (R8027)
//  Description:
//      Changed 'deactivate()' method to call 'setDisplayContrast_()'
//		AFTER setting the 'IsSubScreenActive_' flag to FALSE, because
//		this will force the updating of the contrast to the ACCEPTED value.
//
//  Revision: 003  By:  sah   Date:  17-Jul-1997    DCS Number:  2270
//  Project:  Sigma (R8027)
//  Description:
//      Now using new static method of 'ScreenContrastHandler' for processing
//      all display contrast changes.  Also using new static data member
//      ('IsSubScreenActive_') to allow this new static method to determine
//      whether to use the ADJUSTED or ACCEPTED value of contrast delta.
//
//  Revision: 002  By:  sah   Date:  12-Jun-1997    DCS Number:  2208
//  Project:  Sigma (R8027)
//  Description:
//      Modified code to incorporate new VGA interface.
//
//  Revision: 001  By:  hhd Date:  13-MAY-95    DR Number:
//  Project:  Sigma (R8027)
//  Description:
//      Integration baseline.
//
//=====================================================================

#include "ServiceModeSetupSubScreen.hh"


//@ Usage-Classes
#include "AdjustPanel.hh"
#include "BaudRateValue.hh"
#include "GuiTimerId.hh"
#include "GuiTimerRegistrar.hh"
#include "LanguageValue.hh"
#include "MiscStrs.hh"
#include "NominalLineVoltValue.hh"
#include "PressUnitsValue.hh"
#include "PromptArea.hh"
#include "SettingConstants.hh"
#include "SettingContextHandle.hh"
#include "Sound.hh"
#include "SubScreenArea.hh"
#include "GuiApp.hh"
#include "VgaDevice.hh"
//@ End-Usage

//@ Code...

// Initialize static constants.
static const Int32 BUTTON_Y_      		= 55;
static const Int32 BUTTON_HEIGHT_		= 46;
static const Int32 BUTTON_WIDTH_ 		= 210;
static const Int32 BUTTON_BORDER_ 		= 3;
static const Int32 BUTTON_GAP_			= 10;
static const Int32 BUTTON_SPACING_Y_	= BUTTON_HEIGHT_ + BUTTON_GAP_;
static const Int32 BUTTON_X_      		= 
		( ::SUB_SCREEN_AREA_WIDTH - BUTTON_WIDTH_ ) / 2 - 1;

static const Int32 NUMERIC_VALUE_CENTER_X_ = 105;
static const Int32 NUMERIC_VALUE_CENTER_Y_ = 26;


static Uint  NlVoltageInfoMemory_[(sizeof(DiscreteSettingButton::ValueInfo) +
  (sizeof(StringId) * (NominalLineVoltValue::TOTAL_NOMINAL_VOLT_VALUES - 1)))];
static Uint  BaudRateInfoMemory_[(sizeof(DiscreteSettingButton::ValueInfo) +
		    (sizeof(StringId) * (BaudRateValue::TOTAL_BAUD_RATES - 1)))];
static Uint  PressUnitInfoMemory_[(sizeof(DiscreteSettingButton::ValueInfo) +
		    (sizeof(StringId) * (PressUnitsValue::TOTAL_UNIT_VALUES - 1)))];
static Uint  LanguageInfoMemory_[(sizeof(DiscreteSettingButton::ValueInfo) +
		    (sizeof(StringId) * (LanguageValue::TOTAL_LANGUAGE_VALUES - 1)))];

static DiscreteSettingButton::ValueInfo*  PNlVoltageInfo_ =
					(DiscreteSettingButton::ValueInfo*)::NlVoltageInfoMemory_;
static DiscreteSettingButton::ValueInfo*  PBaudRateInfo_ =
					(DiscreteSettingButton::ValueInfo*)::BaudRateInfoMemory_;
static DiscreteSettingButton::ValueInfo*  PPressUnitInfo_ =
					(DiscreteSettingButton::ValueInfo*)::PressUnitInfoMemory_;
static DiscreteSettingButton::ValueInfo*  PLanguageInfo_ =
					(DiscreteSettingButton::ValueInfo*)::LanguageInfoMemory_;


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ServiceModeSetupSubScreen()  
//
//@ Interface-Description
// No construction parameters needed.  Constructs all subscreens that
// will be displayed in the Lower Subscreen Area.
//---------------------------------------------------------------------
//@ Implementation-Description
// All subscreen members are initialized with a pointer to this
// subscreen area.  We then size, position and color this area.
// $[CL02003] The contrast delta setting is only available for grey-scale.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

ServiceModeSetupSubScreen::ServiceModeSetupSubScreen(SubScreenArea *pSubScreenArea)
: 
	BatchSettingsSubScreen(pSubScreenArea),
	nlVoltageButton_(BUTTON_X_,
				BUTTON_Y_, BUTTON_WIDTH_,
				BUTTON_HEIGHT_, Button::LIGHT, BUTTON_BORDER_,
				Button::SQUARE, Button::NO_BORDER,
				MiscStrs::NOMINAL_LINE_VOLTAGE_BUTTON_TITLE,
				SettingId::NOMINAL_VOLT, this,
				MiscStrs::NOMINAL_LINE_VOLTAGE_HELP_MESSAGE, 18),
	stBaudRateButton_(BUTTON_X_,
				BUTTON_Y_+1*BUTTON_SPACING_Y_, BUTTON_WIDTH_,
				BUTTON_HEIGHT_, Button::LIGHT, BUTTON_BORDER_,
				Button::SQUARE, Button::NO_BORDER,
				MiscStrs::SYSTEM_TEST_BAUD_RATE_BUTTON_TITLE,
				SettingId::SERVICE_BAUD_RATE, this,
				MiscStrs::SYSTEM_TEST_BAUD_RATE_HELP_MESSAGE, 18),
	contrastDeltaButton_(BUTTON_X_,
				BUTTON_Y_+4*BUTTON_SPACING_Y_, BUTTON_WIDTH_,
				BUTTON_HEIGHT_, Button::LIGHT, BUTTON_BORDER_,
				Button::SQUARE, Button::NO_BORDER,
				TextFont::EIGHTEEN, 
				NUMERIC_VALUE_CENTER_X_, NUMERIC_VALUE_CENTER_Y_,
				MiscStrs::SM_CONTRAST_DELTA_BUTTON_TITLE,
				SettingId::DISPLAY_CONTRAST_DELTA, this,
				MiscStrs::SM_CONTRAST_DELTA_HELP_MESSAGE),
	pressureUnitButton_(BUTTON_X_,
				BUTTON_Y_+2*BUTTON_SPACING_Y_, BUTTON_WIDTH_,
				BUTTON_HEIGHT_, Button::LIGHT, BUTTON_BORDER_,
				Button::SQUARE, Button::NO_BORDER,
				MiscStrs::PRESSURE_UNIT_BUTTON_TITLE,
				SettingId::PRESS_UNITS, this,
				MiscStrs::PRESSURE_UNIT_HELP_MESSAGE, 16),
	languageButton_(BUTTON_X_,
				BUTTON_Y_+3*BUTTON_SPACING_Y_, BUTTON_WIDTH_,
				BUTTON_HEIGHT_, Button::LIGHT, BUTTON_BORDER_,
				Button::SQUARE, Button::NO_BORDER,
				MiscStrs::LANGUAGE_BUTTON_TITLE,
				SettingId::LANGUAGE, this,
				MiscStrs::LANGUAGE_HELP_MESSAGE, 16),
	titleArea_(NULL_STRING_ID, SubScreenTitleArea::SSTA_CENTER)
{
	CALL_TRACE("ServiceModeSetupSubScreen::ServiceModeSetupSubScreen(void)");

	// Size and position the area
	setX(0);
	setY(0);
	setWidth(SERVICE_LOWER_SUB_SCREEN_AREA_WIDTH);
	setHeight(SERVICE_LOWER_SUB_SCREEN_AREA_HEIGHT);

	// Add the title area
	addDrawable(&titleArea_);


	// set up values for nominal line voltage....
	::PNlVoltageInfo_->numValues =
							   NominalLineVoltValue::TOTAL_NOMINAL_VOLT_VALUES;
	::PNlVoltageInfo_->arrValues[NominalLineVoltValue::VAC_100] =
										MiscStrs::NOMINAL_LINE_VAC_100_VALUE;
	::PNlVoltageInfo_->arrValues[NominalLineVoltValue::VAC_120] =
										MiscStrs::NOMINAL_LINE_VAC_120_VALUE;
	::PNlVoltageInfo_->arrValues[NominalLineVoltValue::VAC_220] =
										MiscStrs::NOMINAL_LINE_VAC_220_VALUE;
	::PNlVoltageInfo_->arrValues[NominalLineVoltValue::VAC_230] =
										MiscStrs::NOMINAL_LINE_VAC_230_VALUE;
	::PNlVoltageInfo_->arrValues[NominalLineVoltValue::VAC_240] =
										MiscStrs::NOMINAL_LINE_VAC_240_VALUE;
	nlVoltageButton_.setValueInfo(::PNlVoltageInfo_);


	// set up values for System Test baud rate....
	::PBaudRateInfo_->numValues = BaudRateValue::TOTAL_BAUD_RATES;
	::PBaudRateInfo_->arrValues[BaudRateValue::BAUD_1200_VALUE] =
												MiscStrs::BAUD_1200_VALUE_LABEL;
	::PBaudRateInfo_->arrValues[BaudRateValue::BAUD_2400_VALUE] =
												MiscStrs::BAUD_2400_VALUE_LABEL;
	::PBaudRateInfo_->arrValues[BaudRateValue::BAUD_4800_VALUE] =
												MiscStrs::BAUD_4800_VALUE_LABEL;
	::PBaudRateInfo_->arrValues[BaudRateValue::BAUD_9600_VALUE] =
												MiscStrs::BAUD_9600_VALUE_LABEL;
	::PBaudRateInfo_->arrValues[BaudRateValue::BAUD_19200_VALUE] =
												MiscStrs::BAUD_19200_VALUE_LABEL;
	stBaudRateButton_.setValueInfo(::PBaudRateInfo_);


	// set up values for pressure units....
	::PPressUnitInfo_->numValues = PressUnitsValue::TOTAL_UNIT_VALUES;
	::PPressUnitInfo_->arrValues[PressUnitsValue::CMH2O_UNIT_VALUE] =
											MiscStrs::PRESSURE_UNIT_CMH2O_VALUE;
	::PPressUnitInfo_->arrValues[PressUnitsValue::HPA_UNIT_VALUE] =
											MiscStrs::PRESSURE_UNIT_HPA_VALUE;
	pressureUnitButton_.setValueInfo(::PPressUnitInfo_);

	// set up values for language...
	::PLanguageInfo_->numValues = LanguageValue::TOTAL_LANGUAGE_VALUES;
	::PLanguageInfo_->arrValues[LanguageValue::ENGLISH] =
											MiscStrs::LANGUAGE_ENGLISH_VALUE;
	::PLanguageInfo_->arrValues[LanguageValue::CHINESE] =
											MiscStrs::LANGUAGE_CHINESE_VALUE;
	::PLanguageInfo_->arrValues[LanguageValue::FRENCH] =
											MiscStrs::LANGUAGE_FRENCH_VALUE;
	::PLanguageInfo_->arrValues[LanguageValue::GERMAN] =
											MiscStrs::LANGUAGE_GERMAN_VALUE;
	::PLanguageInfo_->arrValues[LanguageValue::ITALIAN] =
											MiscStrs::LANGUAGE_ITALIAN_VALUE;
	::PLanguageInfo_->arrValues[LanguageValue::JAPANESE] =
											MiscStrs::LANGUAGE_JAPANESE_VALUE;
	::PLanguageInfo_->arrValues[LanguageValue::POLISH] =
											MiscStrs::LANGUAGE_POLISH_VALUE;
	::PLanguageInfo_->arrValues[LanguageValue::PORTUGUESE] =
											MiscStrs::LANGUAGE_PORTUGUESE_VALUE;
	::PLanguageInfo_->arrValues[LanguageValue::RUSSIAN] =
											MiscStrs::LANGUAGE_RUSSIAN_VALUE;
	::PLanguageInfo_->arrValues[LanguageValue::SPANISH] =
											MiscStrs::LANGUAGE_SPANISH_VALUE;
	languageButton_.setValueInfo(::PLanguageInfo_);

	Uint  idx;

	idx = 0u;
	arrSettingButtonPtrs_[idx++] = &nlVoltageButton_;
	arrSettingButtonPtrs_[idx++] = &stBaudRateButton_;
	arrSettingButtonPtrs_[idx++] = &pressureUnitButton_;
	arrSettingButtonPtrs_[idx++] = &languageButton_;
	arrSettingButtonPtrs_[idx++] = &contrastDeltaButton_;
	arrSettingButtonPtrs_[idx]   = NULL;

	AUX_CLASS_ASSERTION((idx <= MAX_SETTING_BUTTONS_), idx);

	// Add the buttons to the subscreen
	for (idx = 0u; arrSettingButtonPtrs_[idx] != NULL; idx++ )
	{
		addDrawable(arrSettingButtonPtrs_[idx]);
	}

	// Register for callbacks 
	contrastDeltaButton_.setButtonCallback(this);
										// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~ServiceModeSetupSubScreen()  [Destructor]
//
//@ Interface-Description
// Destroys the Lower Subscreen Area.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

ServiceModeSetupSubScreen::~ServiceModeSetupSubScreen(void)
{
	CALL_TRACE("ServiceModeSetupSubScreen::~ServiceModeSetupSubScreen(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: buttonDownHappened
//
//@ Interface-Description
// Called when the contrast delta button is pressed down.
// Passed a pointer to the button as well as 'byOperatorAction', a flag 
// which indicates whether the operator pressed the button down or 
// whether the setToDown() method was called.
//---------------------------------------------------------------------
//@ Implementation-Description
// We simply display the appropriate prompts for the contrast delta
// button.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ServiceModeSetupSubScreen::buttonDownHappened(Button *pButton, Boolean)
{
	CALL_TRACE("ServiceModeSetupSubScreen::buttonDownHappened(Button *pButton, "
																"Boolean)");
	SAFE_CLASS_ASSERTION(pButton == &contrastDeltaButton_)
	// $[TI1]

	// Display the Advisory which prompts the operator to adjust
	// the upper screen contrast first if not already done so.
	GuiApp::PPromptArea->setPrompt(PromptArea::PA_ADVISORY,
					PromptArea::PA_LOW, 
					PromptStrs::ADJUST_SCREEN_CONTRAST_OFF_KEY_A);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: buttonUpHappened
//
//@ Interface-Description
// Called when the contrast delta button is pressed up. Passed a pointer to the
// button as well as 'byOperatorAction', a flag which indicates whether the
// operator pressed the button up or whether the setToUp() method was called.
//---------------------------------------------------------------------
//@ Implementation-Description
// We simply reset the appropriate prompts for the contrast delta 
// button.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ServiceModeSetupSubScreen::buttonUpHappened(Button *pButton, Boolean)
{
	CALL_TRACE("ServiceModeSetupSubScreen::buttonUpHappened(Button *pButton, "
																"Boolean)");

	SAFE_CLASS_ASSERTION(pButton == &contrastDeltaButton_)

	// $[TI1]
	// Display the Advisory which prompts the operator to adjust
	// the upper screen contrast first if not already done so.
	GuiApp::PPromptArea->setPrompt(PromptArea::PA_ADVISORY,
					PromptArea::PA_LOW, 
					NULL_STRING_ID);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: timerEventHappened
//
//@ Interface-Description
// Called when the Subscreen Setting Change timer that we started runs out.
// Passed the id of the timer.
//---------------------------------------------------------------------
//@ Implementation-Description
// We reset the subscreen by deactivating and reactivating it.  We also
// clear the Adjust Panel focus.  We only do this if the subscreen is
// visible and the operator has adjusted some settings.
//
// We only react to timers if this subscreen is visible.  There is a small
// chance that this timer event might arrive just as we are being deactivated,
// in which case we ignore it.
//---------------------------------------------------------------------
//@ PreCondition
// The timer id must be the subscreen setting change timer.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ServiceModeSetupSubScreen::timerEventHappened(GuiTimerId::GuiTimerIdType timerId)
{
	CALL_TRACE("ServiceModeSetupSubScreen::timerEventHappened("
										"GuiTimerId::GuiTimerIdType timerId)");

	CLASS_PRE_CONDITION(timerId ==
								GuiTimerId::SUBSCREEN_SETTING_CHANGE_TIMEOUT);

	if (isVisible() && !areSettingsCurrent_())
	{													// $[TI1]
		// Reactivate this sub-screen
		deactivate();
		activate();
	}													// $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: acceptHappened
//
//@ Interface-Description
// Called by the BatchSettinsSubScreen when the Accept key is pressed.
// Allows us to accept the ServiceMode settings and exit this subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// The operator has accepted the settings so we inform the Settings-Validation
// subsystem of such.  We then deactivate this subscreen.
// $[01118] User must press Accept key to accept settings...
// $[01257] The Accept key shall be the ultimate confirmation step for all...
// $[01258] The GUI shall accept all adjusted settings.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ServiceModeSetupSubScreen::acceptHappened(void)
{
	CALL_TRACE("ServiceModeSetupSubScreen::acceptHappened(void)");

	if ( areSettingsCurrent_() )
	{															// $[TI1.1]
		Sound::Start(Sound::INVALID_ENTRY);
	}
	else
	{															// $[TI1.2]
		// Clear the Adjust Panel focus, make the Accept sound, store the
		// current settings, accept the adjusted settings and deactivate
		// this subscreen.
		Sound::Start(Sound::ACCEPT);
		AdjustPanel::TakeNonPersistentFocus(NULL);

		// Accept Service Mode settings
		SettingContextHandle::AcceptServiceSetupBatch();
	
		// Deactivate this subscreen
		getSubScreenArea()->deactivateSubScreen();
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activateHappened_
//
//@ Interface-Description
// Called by activate() just before this subscreen is displayed
// allowing us to do any necessary display setup.  Takes no arguments.
//---------------------------------------------------------------------
//@ Implementation-Description
// Inform Settings subsystem of AdjustServiceSetupBatch() process.  Followed
// by calling a routine to activate all buttons then update title and prompts
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ServiceModeSetupSubScreen::activateHappened_(void)
{
	CALL_TRACE("activateHappened_()");

	// Begin adjusting settings
	SettingContextHandle::AdjustServiceSetupBatch();

	// Display the "current" title
	titleArea_.setTitle(MiscStrs::CURRENT_SERVICE_MODE_SETUP_SUBSCREEN_TITLE);

	// Activate all buttons
	operateOnButtons_(arrSettingButtonPtrs_, &SettingButton::activate);

	// Set Primary prompt to "Adjust settings."
	GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY,
				PromptArea::PA_LOW, PromptStrs::ADJUST_SETTINGS_P);

	// Set Secondary prompt to tell user to press OTHER SCREENS button to cancel
	GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
					PromptArea::PA_LOW, PromptStrs::OTHER_SCREENS_CANCEL_S);
	// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: deactivateHappened_
//
//@ Interface-Description
// Called by LowerSubScreenArea just after this subscreen is removed from the
// display allowing us to do any necessary cleanup.  Takes no arguments.
//---------------------------------------------------------------------
//@ Implementation-Description
// We perform any necessary cleanup.  We cancel the Service Inactivity timer
// and the Service countdown clock, clear the Prompt Area of any prompts
// that we may have set.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ServiceModeSetupSubScreen::deactivateHappened_(void)
{
	CALL_TRACE("deactivateHappened_()");

	// deactivate all setting buttons...
	operateOnButtons_(arrSettingButtonPtrs_, &SettingButton::deactivate);
}			// $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: valueUpdateHappened_
//
//@ Interface-Description
// Called when a setting has changed.  
// Passed the following parameters:
// >Von
//	settingId	The enum id of the setting which changed.
//	contextId	The context in which the change happened, either
//				ContextId::ACCEPTED_CONTEXT_ID or
//				ContextId::ADJUSTED_CONTEXT_ID.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
// We first check if we're visible.  We ignore all setting changes when this
// subscreen is not active.  If settings have changed from being current
// to proposed then change the screen title and activate the Accept key
// else if settings have reverted to current from being proposed then
// change back the screen title and deactivate the Accept key.
//
// $[01058] Accept key activated on first setting change...
// $[01118] As soon as settings have changed activate Accept key...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ServiceModeSetupSubScreen::valueUpdateHappened_(
					  const BatchSettingsSubScreen::TransitionId_ transitionId,
					  const Notification::ChangeQualifier,
					  const ContextSubject*
											   )
{
	CALL_TRACE("valueUpdateHappened_(transitionId, qualifierId, pSubject)");

	// If the setting has been changed and the screen was in the
	// current state then go to the proposed state.
	if (transitionId == BatchSettingsSubScreen::CURRENT_TO_PROPOSED)
	{													// $[TI1]
		// Display proposed title
		titleArea_.setTitle(MiscStrs::PROPOSED_SERVICE_MODE_SETUP_SUBSCREEN_TITLE);

		// Change the Secondary prompt to explain the use of the Accept Key
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
			PromptArea::PA_LOW, PromptStrs::OTHER_SCREENS_PROCEED_CANCEL_S);
	}
	// If the setting has been adjusted back to its original value and now
	// all settings are unchanged then reset the screen
	else if (transitionId == BatchSettingsSubScreen::PROPOSED_TO_CURRENT)
	{													// $[TI4]
		// Display current title
		titleArea_.setTitle(MiscStrs::CURRENT_SERVICE_MODE_SETUP_SUBSCREEN_TITLE);

		// Reset the Secondary prompt
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
			PromptArea::PA_LOW, PromptStrs::OTHER_SCREENS_CANCEL_S);
	}													// $[TI5]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
ServiceModeSetupSubScreen::SoftFault(const SoftFaultID  softFaultID,
								     const Uint32       lineNumber,
								     const char*        pFileName,
								     const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS,
							SERVICEMODESETUPSUBSCREEN, lineNumber,
							pFileName, pPredicate);
}
