#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//================= C L A S S   D E S C R I P T I O N =================
//@ Class:  PeepLowTimeSetting - PEEP Low Time Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, bounded setting that contains the time (in milliseconds)
//  an the low PEEP level is to be held during a BiLevel breath.  This class
//  inherits from 'BatchBoundedSetting' and provides the specific information
//  needed for representation of PEEP low time.  This information includes the
//  interval and range of this setting's values, this setting's response to a
//  change of its primary setting (see 'acceptPrimaryChange()'), this batch
//  setting's new-patient value (see 'getNewPatientValue()'), and the
//  contraints and dependent settings of this setting.
//
//  This setting also dynamically monitors the constant parm setting,
//  and updates its applicability accordingly.
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class defines an 'updateConstraints_()' method for updating
//  the constraints of this setting.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/PeepLowTimeSetting.ccv   25.0.4.0   19 Nov 2013 14:27:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004   By: gdc    Date: 16-Aug-2010     SCR Number: 6663
//  Project: API/MAT
//  Description:
//  Added support for VentSet API.
// 
//  Revision: 003   By: sah    Date: 22-Sep-1999    DR Number: 5424
//  Project:  NeoMode
//  Description:
//	Obsoleted 'SettingOptions' class, to use new global
//      'SoftwareOptions' class.
//
//  Revision: 002   By: sah   Date:  22-Jul-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  modified to use newly-modified 'BoundedRange' class to handle
//         range, via linked list of interval nodes
//
//  Revision: 001   By: sah   Date:  26-Jan-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//     Created to work with new applicability functionality.
//
//=====================================================================

#include "PeepLowTimeSetting.hh"
#include "ModeValue.hh"
#include "ConstantParmValue.hh"

//@ Usage-Classes
#include "SettingsMgr.hh"
#include "RespRateSetting.hh"
#include "ConstantParmSetting.hh"
#include "SoftwareOptions.hh"
//@ End-Usage


//@ Code...

//====================================================================
//
//  Static Data Definitions...
//
//====================================================================

// $[TC02025] -- this setting's dependent settings...
static const SettingId::SettingIdType  ARR_DEPENDENT_SETTING_IDS_[] =
  {
    SettingId::HL_RATIO,
    SettingId::PEEP_HIGH_TIME,

    SettingId::NULL_SETTING_ID
  };


// $[TC02022] The setting's range ...
// $[TC02024] The setting's resolution ..
static const BoundedInterval  LAST_NODE_ =
  {
    200.0f,		// absolute minimum...
    0.0f,		// unused...
    TENS,		// unused...
    NULL
  };
static const BoundedInterval  INTERVAL_LIST_ =
  {
    100000.0f,		// absolute maximum...
    10.0f,		// resolution...
    TENS,		// precision...
    &::LAST_NODE_
  };


//====================================================================
//
//  Public Methods...
//
//====================================================================

//============= M E T H O D   D E S C R I P T I O N ==================
//@ Method: PeepLowTimeSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This setting inherits from BatchBoundedSetting.  The method passes to
//  BatchBoundedSetting it's default information. Also, the method
//  initializes the value interval range.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

PeepLowTimeSetting::PeepLowTimeSetting(void)
	   : BatchBoundedSetting(SettingId::PEEP_LOW_TIME,
				 ::ARR_DEPENDENT_SETTING_IDS_,
				 &::INTERVAL_LIST_,
				 NULL_SETTING_BOUND_ID,	// maxConstraintId...
				 PEEP_LOW_TIME_MIN_ID)	// minConstraintId...
{
  CALL_TRACE("PeepLowTimeSetting()");
	isATimingSetting_ = TRUE;
}   // $[TI1]


//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  ~PeepLowTimeSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

PeepLowTimeSetting::~PeepLowTimeSetting(void)
{
  CALL_TRACE("~PeepLowTimeSetting()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[01071] -- main settings applicability
//  $[01081] -- breath settings applicability
//---------------------------------------------------------------------
//@ PreCondition
//  (qualifierId == Notification::ACCEPTED  ||
//   qualifierId == Notification::ADJUSTED)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
PeepLowTimeSetting::getApplicability(const Notification::ChangeQualifier qualifierId) const
{
  CALL_TRACE("getApplicability(qualifierId)");

  Applicability::Id  applicability;

  if (SoftwareOptions::IsOptionEnabled(SoftwareOptions::BILEVEL))
  {  // $[TI1] -- BILEVEL is an active option...
    const Setting*  pMode = SettingsMgr::GetSettingPtr(SettingId::MODE);
    const Setting*  pConstantParm =
			  SettingsMgr::GetSettingPtr(SettingId::CONSTANT_PARM);

    DiscreteValue  modeValue;
    DiscreteValue  constantParmValue;

    switch (qualifierId)
    {
    case Notification::ACCEPTED :		// $[TI1.1]
      modeValue         = pMode->getAcceptedValue();
      constantParmValue = pConstantParm->getAcceptedValue();
      break;
    case Notification::ADJUSTED :		// $[TI1.2]
      modeValue         = pMode->getAdjustedValue();
      constantParmValue = pConstantParm->getAdjustedValue();
      break;
    default :
      AUX_CLASS_ASSERTION_FAILURE(qualifierId);
      break;
    }

    if (modeValue == ModeValue::BILEVEL_MODE_VALUE)
    {  // $[TI1.3]
      applicability =
	    (constantParmValue == ConstantParmValue::PEEP_LOW_TIME_CONSTANT)
		       ? Applicability::CHANGEABLE	// $[TI1.3.1]
		       : Applicability::VIEWABLE;	// $[TI1.3.2]
    }
    else
    {  // $[TI1.4]
      applicability = Applicability::INAPPLICABLE;
    }
  }
  else
  {  // $[TI2] -- BILEVEL is not an active option...
    applicability = Applicability::INAPPLICABLE;
  }

  return(applicability);
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [const, virtual]
//
//@ Interface-Description
//  Return this setting's new-patient value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[TC02023] -- this setting's new patient value
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
PeepLowTimeSetting::getNewPatientValue(void) const
{
  CALL_TRACE("getNewPatientValue()");

  BoundedValue  newPatient;

  // the PEEP low time's new patient value is based on the new-patient
  // values of PEEP high time and respiratory rate...
  newPatient = calcBasedOnSetting_(SettingId::PEEP_HIGH_TIME,
  				   BoundedRange::WARP_NEAREST,
				   BASED_ON_NEW_PATIENT_VALUES);

  return(newPatient);
}   // $[TI1]


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method: acceptPrimaryChange(primaryId)
//
//@ Interface-Description
//  One of this setting's primary settings have changed, therefore update
//  this setting's value based on the new value of the primary setting
//  -- indicated by 'primaryId'.  If this setting's bound is violated by
//  the primary setting's newly "adjusted" value, a pointer to this
//  setting's bound status is returned, and this setting's value is "clipped"
//  to that bound.  Otherwise, a value of 'NULL' is returned to indicate no
//  dependent bound was violated.
//---------------------------------------------------------------------
//@ Implementation-Description
//  When respiratory rate is changing while holding H:L ratio constant,
//  AND when H:L ratio is the primary setting, this implementation is based
//  on PEEP high time being update BEFORE this method gets called; this
//  method depends on the new PEEP high time being in the Adjusted Context,
//  during this type of update.
//
//  $[02002] -- new dependent settings based on proposed primary settings...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

const BoundStatus*
PeepLowTimeSetting::acceptPrimaryChange(const SettingId::SettingIdType primaryId)
{
  CALL_TRACE("acceptPrimaryChange(primaryId)");

  // update dynamic constraints...
  updateConstraints_();

  BoundStatus&  rBoundStatus = getBoundStatus_();

  // initialize to holding this setting's id...
  rBoundStatus.setViolationId(getId());

  BoundedValue  newPeepLowTime;

  newPeepLowTime = calcBasedOnSetting_(primaryId, BoundedRange::WARP_NEAREST,
				       BASED_ON_ADJUSTED_VALUES);

  // test 'newPeepLowTime' against this setting's bounded range; the bound
  // violation state, if any, is returned in 'rBoundStatus', while
  // 'newPeepLowTime' is "clipped" if a bound is violated...
  getBoundedRange_().testValue(newPeepLowTime, rBoundStatus);

  // store the value...
  setAdjustedValue(newPeepLowTime);

  const BoundStatus*  pBoundStatus;

  // if there is a bound violation return a pointer to this setting's
  // bound status, otherwise return 'NULL'...
  pBoundStatus = (rBoundStatus.isInViolation()) ? &rBoundStatus	// $[TI1]
						: NULL;		// $[TI2]

  return(pBoundStatus);
}


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method: acceptTransition(settingId, newValue, currValue) [virtual]
//
//@ Interface-Description
//  This method is responsible for updating this setting based on the
//  the Main Control Setting identified by 'settingId' and the transition
//  of its value from 'currValue' to 'newValue'.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02025]\f\   -- transitioning from A/C to BiLevel
//  $[TC02000]\c\ -- transitioning from SIMV to BiLevel
//  $[02026]\e\   -- transitioning from SPONT to BiLevel
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
PeepLowTimeSetting::acceptTransition(const SettingId::SettingIdType,
				     const DiscreteValue,
				     const DiscreteValue)
{
  CALL_TRACE("acceptTransition(settingId, newValue, currValue)");

  BoundedValue  newPeepLowTime;

  newPeepLowTime = calcBasedOnSetting_(SettingId::PEEP_HIGH_TIME,
				       BoundedRange::WARP_NEAREST,
				       BASED_ON_ADJUSTED_VALUES);

  // store the transition value into the adjusted context...
  setAdjustedValue(newPeepLowTime);

  setForcedChangeFlag();
}  // $[TI1]


//==================== M E T H O D   D E S C R I P T I O N =============
//@ Method:  valueUpdate(qualifierId, pSubject)  [virtual]
//
//@ Interface-Description
//  Respond to subject's value changes, by, possibly, updating this setting's
//  applicability.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (pSubject->getId() == SettingId::CONSTANT_PARM)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
PeepLowTimeSetting::valueUpdate(const Notification::ChangeQualifier qualifierId,
			        const SettingSubject*               pSubject)
{
  CALL_TRACE("valueUpdate(qualifierId, pSubject)");
  SAFE_AUX_CLASS_PRE_CONDITION((pSubject->getId() == SettingId::CONSTANT_PARM),
			       pSubject->getId());

  if (SoftwareOptions::IsOptionEnabled(SoftwareOptions::BILEVEL))
  {  // $[TI1] -- BiLevel option is active...
    // update this instance's applicability...
    updateApplicability(qualifierId);
  }  // $[TI2] -- BiLevel option is NOT active...
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: doRetainAttachment(pSubject)  [virtual, const]
//
//@ Interface-Description
//  When task-level, state changes occur, all subjects are notified
//  to detach from all observers.  This is done because the GUI-Apps
//  observers may be overwritten by new class instances, without any
//  explicit destructor/detachment calls.
//
//  This method is defined here to indicate to any of this instance's
//  subjects, not to detach this observer.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean
PeepLowTimeSetting::doRetainAttachment(const SettingSubject*) const
{
  CALL_TRACE("doRetainAttachment(pSubject)");

  return(TRUE);
}  // $[TI1]


//==================== M E T H O D   D E S C R I P T I O N ============
//@ Method:  settingObserverInit()  [virtual]
//
//@ Interface-Description
//  This setting needs to monitor the value of Constant Parm setting,
//  therefore this virtual method is overridden to provide a point during
//  initialization to attach to that (subject) setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
PeepLowTimeSetting::settingObserverInit(void)
{
  CALL_TRACE("settingObserverInit()");

  // monitor changes in constant parm's value...
  attachToSubject_(SettingId::CONSTANT_PARM, Notification::VALUE_CHANGED);
}  // $[TI1]


#if defined(SIGMA_DEVELOPMENT)

//============== M E T H O D   D E S C R I P T I O N =================
// Method:  isAcceptedValid()  [const, virtual]
//
// Interface-Description
//  Is the accepted value of this setting valid as determined by its
//  dependencies?   Return "True" if the setting is valid, returns "False"
//  if the setting is not valid.
//---------------------------------------------------------------------
// Implementation-Description
//  The setting determines, and returns its validity.
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

Boolean
PeepLowTimeSetting::isAcceptedValid(void) const
{
  CALL_TRACE("isAcceptedValid()");

  Boolean  isValid = BoundedSetting::isAcceptedValid();

  if (isValid)
  {
    if (getApplicability(Notification::ACCEPTED) !=
					    Applicability::INAPPLICABLE)
    {
      // get the accepted value...
      const BoundedValue  ACCEPTED_VALUE = getAcceptedValue();

      const Real32  PEEP_LOW_TIME_VALUE =
      				BoundedValue(getAcceptedValue()).value;

      const DiscreteValue  CONSTANT_PARM_VALUE =
		    ConstantParmSetting::GetValue(BASED_ON_ACCEPTED_VALUES);

      SettingId::SettingIdType  basedOnSettingId;

      switch (CONSTANT_PARM_VALUE)
      {
      case ConstantParmValue::PEEP_LOW_TIME_CONSTANT :
      case ConstantParmValue::PEEP_HIGH_TIME_CONSTANT :
	basedOnSettingId = SettingId::PEEP_HIGH_TIME;
        break;
      case ConstantParmValue::HL_RATIO_CONSTANT :
	basedOnSettingId = SettingId::HL_RATIO;
        break;
      default : 
	AUX_CLASS_ASSERTION_FAILURE(CONSTANT_PARM_VALUE);
        break;
      };

      const BoundedValue  CALC_PEEP_LOW_TIME  =
			       calcBasedOnSetting_(basedOnSettingId,
						   BoundedRange::WARP_NEAREST,
						   BASED_ON_ACCEPTED_VALUES);

      isValid = ::IsEquivalent(PEEP_LOW_TIME_VALUE,
			       CALC_PEEP_LOW_TIME.value,
			       CALC_PEEP_LOW_TIME.precision);
    }
  }

  return(isValid);
}


//============== M E T H O D   D E S C R I P T I O N =================
// Method:  isAdjustedValid()  [const, virtual]
//
// Interface-Description
//  Is the adjusted value of this setting valid as determined by its
//  dependencies? Return "True" if the setting is valid, returns "False"
//  if the setting is not valid.
//---------------------------------------------------------------------
// Implementation-Description
//  The setting determines, and returns its validity.
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

Boolean
PeepLowTimeSetting::isAdjustedValid(void)
{
  CALL_TRACE("IsAdjustedValid()");

  Boolean  isValid = BoundedSetting::isAdjustedValid();

  if (isValid)
  {
    if (getApplicability(Notification::ADJUSTED) !=
					    Applicability::INAPPLICABLE)
    {
      // get the adjusted value...
      const BoundedValue  ADJUSTED_VALUE = getAdjustedValue();

      const Real32  PEEP_LOW_TIME_VALUE =
      				BoundedValue(getAdjustedValue()).value;

      const DiscreteValue  CONSTANT_PARM_VALUE =
		    ConstantParmSetting::GetValue(BASED_ON_ADJUSTED_VALUES);

      SettingId::SettingIdType  basedOnSettingId;

      switch (CONSTANT_PARM_VALUE)
      {
      case ConstantParmValue::PEEP_HIGH_TIME_CONSTANT :
      case ConstantParmValue::PEEP_LOW_TIME_CONSTANT :
	basedOnSettingId = SettingId::PEEP_HIGH_TIME;
        break;
      case ConstantParmValue::HL_RATIO_CONSTANT :
	basedOnSettingId = SettingId::HL_RATIO;
        break;
      default : 
	AUX_CLASS_ASSERTION_FAILURE(CONSTANT_PARM_VALUE);
        break;
      };

      const BoundedValue  CALC_PEEP_LOW_TIME  =
			       calcBasedOnSetting_(basedOnSettingId,
						   BoundedRange::WARP_NEAREST,
						   BASED_ON_ADJUSTED_VALUES);

      isValid = ::IsEquivalent(PEEP_LOW_TIME_VALUE,
			       CALC_PEEP_LOW_TIME.value,
			       CALC_PEEP_LOW_TIME.precision);

      if (!isValid)
      {
	const BoundedSetting*  pPeepHighTime =
		  SettingsMgr::GetBoundedSettingPtr(SettingId::PEEP_HIGH_TIME);
	const BoundedSetting*  pRespRate =
		    SettingsMgr::GetBoundedSettingPtr(SettingId::RESP_RATE);

	cout << "INVALID PEEP Low Time:\n";
	cout << "CALC = " << CALC_PEEP_LOW_TIME << endl;
	cout << *this << *pPeepHighTime << *pRespRate << endl;
      }
    }
  }

  return(isValid);
}

#endif // defined(SIGMA_DEVELOPMENT)


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//	      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
PeepLowTimeSetting::SoftFault(const SoftFaultID  softFaultID,
			      const Uint32       lineNumber,
			      const char*        pFileName,
			      const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
			  PEEP_LOW_TIME_SETTING, lineNumber, pFileName,
			  pPredicate);
}


//====================================================================
//
//  Protected Methods...
//
//====================================================================

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  updateConstraints_()  [virtual]
//
//@ Interface-Description
//  Update the dynamic bounds of this setting.  This method sets this
//  setting's maximum constraint to the most restrictive of this setting's
//  upper bounds, and, correspondingly, set this setting's minimum
//  constraint to its most restrictive lower bound.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Determine the minimum upper bound, and set the upper limit to it.
//
//  WARNING:  any new bounds that are added to this method may also affect
//            the 'calcDependentValues()' method of RespRateSetting.  Please
//            make sure that the latter method is checked.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
PeepLowTimeSetting::updateConstraints_(void)
{
  CALL_TRACE("updateConstraints_()");

  //===================================================================
  // determine PEEP low time's maximum limit...
  //===================================================================

  const Real32  BREATH_PERIOD_BASED_MAX =
    (RespRateSetting::GetBreathPeriod(BASED_ON_ADJUSTED_VALUES) - 100.0f);

  BoundedRange::ConstraintInfo  maxConstraintInfo;

  // NOTE:   though this constraint will never be hit in a quiescent mode,
  //         it is possible with a LARGE knob delta to jump to a PEEP low
  //         time value larger than this constraint, thereby messing
  //         up H:L ratio bound calculations; this prevents the screwed
  //         up H:L calculation scenario, and allows for H:L ratio to
  //         detect the bound violation, itself...
  maxConstraintInfo.id    = NULL_SETTING_BOUND_ID;
  maxConstraintInfo.value = BREATH_PERIOD_BASED_MAX;

  // this setting has no soft bounds...
  maxConstraintInfo.isSoft = FALSE;

  // update PEEP low time's maximum constraint info...
  getBoundedRange_().updateMaxConstraint(maxConstraintInfo);
}  // $[TI1]


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method: calcBasedOnSetting_(basedOnSettingId, warpDirection, basedOnCategory)  [const]
//
//@ Interface-Description
//  This method is responsible for calculating, and returning, a new
//  "adjusted" value for this setting, based on the "adjusted" value
//  of the dependent setting given by 'basedOnSettingId'.  This method will
//  calculate values for each of this setting's dependent settings,
//  based on values in the Adjusted Context.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (basedOnSettingId == SettingId::PEEP_HIGH_TIME  &&
//   (basedOnCategory == BASED_ON_ADJUSTED_VALUES  ||
//    basedOnCategory == BASED_ON_NEW_PATIENT_VALUES))
//			||
//  ((basedOnSettingId == SettingId::RESP_RATE  ||
//    basedOnSettingId == SettingId::HL_RATIO)  &&
//    basedOnCategory == BASED_ON_ADJUSTED_VALUES)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

BoundedValue
PeepLowTimeSetting::calcBasedOnSetting_(
				const SettingId::SettingIdType basedOnSettingId,
				const BoundedRange::WarpDir    warpDirection,
				const BasedOnCategory          basedOnCategory
				   ) const
{
  CALL_TRACE("calcBasedOnSetting_(basedOnSettingId, warpDirection, basedOnCategory)");

  SettingId::SettingIdType  finalBasedOnSettingId;

  if (basedOnSettingId == SettingId::RESP_RATE)
  {   // $[TI1] -- convert 'RESP_RATE' to "based-on" id...
    const DiscreteValue  CONSTANT_PARM_VALUE =
			       ConstantParmSetting::GetValue(basedOnCategory);

    switch (CONSTANT_PARM_VALUE)
    {
    case ConstantParmValue::PEEP_HIGH_TIME_CONSTANT :    // $[TI1.1]
      // respiratory rate is being changed while holding PEEP high time
      // constant, therefore calculate the new PEEP low time from the
      // current PEEP high time...
      finalBasedOnSettingId = SettingId::PEEP_HIGH_TIME;
      break;
    case ConstantParmValue::HL_RATIO_CONSTANT :    // $[TI1.2]
      // respiratory rate is being changed while holding H:L ratio
      // constant, therefore calculate the new PEEP low time from the
      // current H:L ratio...
      finalBasedOnSettingId = SettingId::HL_RATIO;
      break;
    case ConstantParmValue::PEEP_LOW_TIME_CONSTANT :
    case ConstantParmValue::TOTAL_CONSTANT_PARMS :
    default :
      // invalid value for 'CONSTANT_PARM_VALUE'...
      AUX_CLASS_ASSERTION_FAILURE(CONSTANT_PARM_VALUE);
      break;
    };
  }
  else
  {   // $[TI2]
    // use passed in parameter...
    finalBasedOnSettingId = basedOnSettingId;
  }

  // get the breath period from the respiratory rate...
  const Real32  BREATH_PERIOD_VALUE =
			    RespRateSetting::GetBreathPeriod(basedOnCategory);

  BoundedValue  peepLowLowTime;

  switch (finalBasedOnSettingId)
  {
  //-------------------------------------------------------------------
  // $[02014](2) -- new PEEP low time while changing PEEP high time...
  // $[02018](2) -- new PEEP low time while keeping PEEP high time constant...
  // $[02163]    -- formula for calculating this setting's new-patient value...
  //-------------------------------------------------------------------
  case SettingId::PEEP_HIGH_TIME :
    {   // $[TI3] -- base PEEP low time's calculation on PEEP high time...
      const BoundedSetting*  pPeepHighTime =
		SettingsMgr::GetBoundedSettingPtr(SettingId::PEEP_HIGH_TIME);

      Real32  peepHighTimeValue;

      switch (basedOnCategory)
      {
      case BASED_ON_NEW_PATIENT_VALUES :	// $[TI3.1]
	// get the new-patient values of PEEP high time...
	peepHighTimeValue =
		      BoundedValue(pPeepHighTime->getNewPatientValue()).value;
	break;
      case BASED_ON_ADJUSTED_VALUES :		// $[TI3.2]
	// get the "adjusted" values of PEEP high time...
	peepHighTimeValue =
			BoundedValue(pPeepHighTime->getAdjustedValue()).value;
	break;
      case BASED_ON_ACCEPTED_VALUES :
	// fall through to 'default:' when not in DEVELOPMENT mode...
#if defined(SIGMA_DEVELOPMENT)
	// get the "accepted" values of PEEP high time...
	peepHighTimeValue =
			BoundedValue(pPeepHighTime->getAcceptedValue()).value;
	break;
#endif // defined(SIGMA_DEVELOPMENT)
      default :
	// unexpected 'basedOnCategory' value..
	AUX_CLASS_ASSERTION_FAILURE(basedOnCategory);
	break;
      };

      // calculate PEEP low time...
      peepLowLowTime.value = (BREATH_PERIOD_VALUE - peepHighTimeValue);
    }
    break;

  //-------------------------------------------------------------------
  // $[02013](2) -- new PEEP low time while changing H:L ratio...
  // $[02017](2) -- new PEEP low time while keeping H:L ratio constant...
  //-------------------------------------------------------------------
  case SettingId::HL_RATIO :
    {   // $[TI4] -- base PEEP low time's calculation on H:L ratio...
      const Setting*  pHlRatio =
			      SettingsMgr::GetSettingPtr(SettingId::HL_RATIO);

      Real32  hlRatioValue;

      switch (basedOnCategory)
      {
      case BASED_ON_ADJUSTED_VALUES :
	// get the "adjusted" values of H:L ratio...
	hlRatioValue = BoundedValue(pHlRatio->getAdjustedValue()).value;
	break;
      case BASED_ON_ACCEPTED_VALUES :
	// fall through to 'default:' when not in DEVELOPMENT mode...
#if defined(SIGMA_DEVELOPMENT)
	// get the "accepted" values of H:L ratio...
	hlRatioValue = BoundedValue(pHlRatio->getAcceptedValue()).value;
	break;
#endif // defined(SIGMA_DEVELOPMENT)
      case BASED_ON_NEW_PATIENT_VALUES :
      default :
	// unexpected 'basedOnCategory' value..
	AUX_CLASS_ASSERTION_FAILURE(basedOnCategory);
	break;
      }

      // calculate PEEP low time from H:L ratio...
      Real32  lRatioValue;
      Real32  hRatioValue;

      if (hlRatioValue < 0.0f)
      {   // $[TI4.1]
	// the H:L ratio value is negative, therefore we have 1:xx with
	// "xx" the PEEP low portion of the ratio...
	hRatioValue = 1.0f;
	lRatioValue = -(hlRatioValue);
      }
      else
      {   // $[TI4.2]
	// the H:L ratio value is positive, therefore we have xx:1 with
	// "xx" the PEEP high portion of the ratio...
	hRatioValue = hlRatioValue;
	lRatioValue = 1.0f;
      }

      // calculate the PEEP low time from the H:L ratio...
      peepLowLowTime.value = (lRatioValue * BREATH_PERIOD_VALUE) /
			         (hRatioValue + lRatioValue);
    }
    break;

  default :
    // unexpected dependent setting id...
    AUX_CLASS_ASSERTION_FAILURE(finalBasedOnSettingId);
    break;
  };

  // place PEEP low on a "click" boundary...
  getBoundedRange().warpValue(peepLowLowTime, warpDirection, FALSE);

  return(peepLowLowTime);
}
