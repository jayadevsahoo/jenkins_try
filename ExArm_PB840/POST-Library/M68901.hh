#ifndef M68901_HH
#define M68901_HH


//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================ H E A D E R   D E S C R I P T I O N ====
//@ Filename: M68901.hh - Motorola 68901 Multi-Function Peripheral 
//                        Device Definitions.
//
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/POST-Library/vcssrc/M68901.hhv   25.0.4.0   19 Nov 2013 14:16:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//=====================================================================

#define  M68901_BASE   (0xFFBE8080)
#define  M68901_GPDR   (*(volatile Uint8 *)(M68901_BASE+0x00))
#define  M68901_AER    (*(volatile Uint8 *)(M68901_BASE+0x01))
#define  M68901_DDR    (*(volatile Uint8 *)(M68901_BASE+0x02))
#define  M68901_IERA   (*(volatile Uint8 *)(M68901_BASE+0x03))
#define  M68901_IERB   (*(volatile Uint8 *)(M68901_BASE+0x04))
#define  M68901_IPRA   (*(volatile Uint8 *)(M68901_BASE+0x05))
#define  M68901_IPRB   (*(volatile Uint8 *)(M68901_BASE+0x06))
#define  M68901_ISRA   (*(volatile Uint8 *)(M68901_BASE+0x07))
#define  M68901_ISRB   (*(volatile Uint8 *)(M68901_BASE+0x08))
#define  M68901_IMRA   (*(volatile Uint8 *)(M68901_BASE+0x09))
#define  M68901_IMRB   (*(volatile Uint8 *)(M68901_BASE+0x0A))
#define  M68901_VR     (*(volatile Uint8 *)(M68901_BASE+0x0B))
#define  M68901_TACR   (*(volatile Uint8 *)(M68901_BASE+0x0C))
#define  M68901_TBCR   (*(volatile Uint8 *)(M68901_BASE+0x0D))
#define  M68901_TCDCR  (*(volatile Uint8 *)(M68901_BASE+0x0E))
#define  M68901_TADR   (*(volatile Uint8 *)(M68901_BASE+0x0F))
#define  M68901_TBDR   (*(volatile Uint8 *)(M68901_BASE+0x10))
#define  M68901_TCDR   (*(volatile Uint8 *)(M68901_BASE+0x11))
#define  M68901_TDDR   (*(volatile Uint8 *)(M68901_BASE+0x12))
#define  M68901_SCR    (*(volatile Uint8 *)(M68901_BASE+0x13))
#define  M68901_UCR    (*(volatile Uint8 *)(M68901_BASE+0x14))
#define  M68901_RSR    (*(volatile Uint8 *)(M68901_BASE+0x15))
#define  M68901_TSR    (*(volatile Uint8 *)(M68901_BASE+0x16))
#define  M68901_UDR    (*(volatile Uint8 *)(M68901_BASE+0x17))


#endif // M68901_HH
