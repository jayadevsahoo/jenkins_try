#ifndef Drawable_HH
#define Drawable_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1995, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: Drawable - Represents an object that occupies an area on the screen.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Foundations/vcssrc/Drawable.hhv   25.0.4.0   19 Nov 2013 14:11:42   pvcs  $
//
//@ Modification-Log
//
//  Revision: 010   By: gdc    Date: 27-Apr-2009    SCR Number: 6489
//  Project:  840S
//  Description:
//      Modifications to provide for transitioning of tube I.D. to 
//		new patient value when spontaneous type changed to PAV.
//		Changed getColor() to const method.
//
//  Revision: 009   By: gdc    Date: 26-Jan-2009    SCR Number: 6407
//  Project:  840S
//  Description:
//      Modified to bring source into strict ARM compliance.
//
//  Revision: 008   By: gdc    Date:  25-Jan-2007    SCR Number: 6237
//  Project: TREND
//  Description:
//	Removed SUN target code. Added vertical positioning gravity and
//  alignment function. Added support for emulated overlay plane where
//  graphics are stored and retrieved from off-screen memory to
//  make it appear the graphics overlay the on-screen graphics. Used
//  to support trending cursor.
//
//  Revision: 007   By: gdc    Date:  29-Aug-2000    DCS Number: 5493
//  Project: GuiComms
//  Description:
//	Added Single Screen Compatibility.
//
//  Revision: 006   By: sah    Date:  20-Apr-2000    DCS Number: 5705
//  Project:  NeoMode
//  Description:
//      Added "GRAVITY_RIGHT" and "GRAVITY_LEFT" support in 'positionInContainer()'
//      as part of the adding of the "above PEEP" phrase at the right edge
//      of some setting buttons.
//
//  Revision: 006   By: gdc    Date:  29-Jun-1999    DCS Number: 5337
//  Project: 840 Neonatal
//  Description:
//      Optimized VGA library for Neonatal project to use BitBlt and Windows
//      Bitmap structures.
//
//  Revision: 005   By: sah    Date: 11-Jan-1999   DR Number: 5321
//  Project:  Sigma (R8027)
//  Description:
//      Changed 'SoftFault()' method to non-inline method.
//
//  Revision: 004  By: gdc     Date: 16-Jun-1998     DCS Number:
//  Project:  Color
//  Description:
//      Initial release.
//
//  Revision: 003  By: gdc      Date: 15-Feb-1996  DR Number: 674
//    Project:  Sigma (R8027)
//    Description:
//      Removed ifndef / endif around include directives
//
//  Revision: 002  By: mpm      Date: 02-Aug-1995  DCR Number: 486
//    Project:  Sigma (R8027)
//    Description:
//      Made destructor virtual as per coding standard requirements.
//
//  Revision: 001  By: yyy      Date: 01-Jan-1995  DR Number: NONE
//    Project:  Sigma (R8027)
//    Description:
//      Integration baseline.
//
//====================================================================

#include "Sigma.hh"
#include "GuiFoundation.hh"

//@ Usage-Classes
#include "Area.hh"
#include "Colors.hh"

enum Gravity
{
	GRAVITY_LEFT,
	GRAVITY_CENTER,
	GRAVITY_RIGHT,
	GRAVITY_TOP,
	GRAVITY_VERTICAL_CENTER,
	GRAVITY_BOTTOM
};

class Container;
//@ End-Usage

class Drawable
{
public:

	Drawable(void);
	virtual ~Drawable(void);

	virtual void setX(Int x);
	virtual void setY(Int y);

	Int getX(void) const;
	Int getY(void) const;
	
	Int getRealX(void) const;
	Int getRealY(void) const;

	virtual void setWidth(Int width);
	virtual void setHeight(Int height);

	Int getWidth(void) const;
	Int getHeight(void) const;

	Boolean overlapsRealArea(const Area& realArea) const;
	virtual const Drawable* findTouchedDrawable(const Area& touchArea,
												Int& numHits) const;

	virtual void touch(Int x, Int y);
	virtual void leave(Int x, Int y);
	virtual void release(void);

	virtual void activate(void);

	virtual void appendTo(Container *pNewParentContainer);
	virtual void removeFromParent(void);

	virtual void evaluateBase(void);

	virtual void setColor(Colors::ColorS color);

	virtual void setShow(Boolean isShown);
	virtual void evaluateVisible(void);

	virtual Colors::ColorS getColor(void) const;

	virtual void setHighlighted(Boolean var);
	virtual void setHighlightColor(Colors::ColorS color);

	virtual void positionInContainer(const Gravity gravity);

	void changing(void);

	virtual void draw(const Area& clipArea) = 0;
	void drawDirect(void);
	void drawOverlay(void);
	void removeOverlay(void);

	inline Area& getLastDrawnArea(void);

	inline const Area& getArea(void) const;
	inline Container *getParentContainer(void) const;
	inline BaseContainer *getBaseContainer(void) const;
	inline BaseContainer *getBaseWhereLastDrawn(void) const;

	inline Boolean getShow(void) const;
	inline Boolean isOpaque(void) const;
	inline Boolean isVisible(void) const;
	inline void clearChanged(void);

	inline Boolean isHighlighted(void) const;
	inline Colors::ColorS getHighlightColor(void) const;

	Boolean operator==(const Drawable& drawable) const;
	Boolean operator!=(const Drawable& drawable) const;

	static void SoftFault(const SoftFaultID  softFaultID,
						  const Uint32       lineNumber,
						  const char*        pFileName  = NULL,
						  const char*        pPredicate = NULL);


#if defined SIGMA_UNIT_TEST
	Boolean getIsSensitive_(void);
	void setIsSensitive_(Boolean isSensitive);
	Boolean getIsOpaque_(void);
	BaseContainer* getPBaseContainer_(void);
	Area* getArea_(void);
	Container* getPParentContainer_(void);
	Boolean getIsChanged_(void);
	Boolean getIsVisible_(void);
	Boolean getIsShown_(void);
	Area* getLastDrawnArea_(void);
	BaseContainer* getPBaseWhereLastDrawn_(void);
#endif	// SIGMA_UNIT_TEST
								 


	//@ Constant: MAX_TREE_DEPTH
	// This is the greatest number of levels allowed in the containment tree.
	static const Int16 MAX_TREE_DEPTH;



protected:
	inline void setVisible_(Boolean isVisible);

	//@ Data-Member: isSensitive_
	// Keeps track of whether or not this Drawable will respond to touch
	// events.  The default value is FALSE.  It is very important to note
	// that the search algorithm continues to search down the tree inside
	// insensitive Containers because they may have sensitive Drawables
	// inside them.
	Boolean isSensitive_;

	//@ Data-Member: isOpaque_
	// This attribute records whether or not the object is opaque.  It is
	// not publicly settable.  Instead, individual derived classes must
	// properly maintain this flag.  The default value is FALSE.
	Boolean isOpaque_;

	//@ Data-Member: color_
	// Stores the color for this object, as applicable.  Some derived
	// classes, like Containers, do not have an intrinsic color, so this
	// member may go unused.
	Colors::ColorS color_;

	//@ Data-Member: pBaseContainer_
	// Pointer to the topmost Container in the Container tree which
	// contains this Drawable.  It is NULL if this Drawable is not
	// connected to the screen.  Otherwise, the expected values are
	// GuiFoundation::GetUpperBaseContainer() and
	// GuiFoundation::GetLowerBaseContainer().
	BaseContainer* pBaseContainer_;

	//@ Data-Member: area_
	// This contained object holds the area that the drawable occupies.  The
	// coordinates are relative to the origin of the parent Container.
	Area area_;

	//@ Data-Member: isHighlighted_
	// Indicates whether or not the object is highlighted.
	Boolean isHighlighted_;

	//@ Data-Member: highlightColor_
	// Indicates the color of the field when highlighted
	Colors::ColorS highlightColor_;

private:
	Drawable(const Drawable&);            // Declared, but not implemented.
	void operator=(const Drawable&);      // Declared, but not implemented.

	//@ Data-Member: pParentContainer_
	// A pointer to the Container which directly contains this object.  It
	// must be updated whenever the parent of this object changes.
	Container* pParentContainer_;

	//@ Data-Member: isChanged_
	// This boolean is set to FALSE whenever the drawable is drawn.  It is
	// set to TRUE the first time it is changed.
	Boolean isChanged_;

	//@ Data-Member: isVisible_
	// Keeps track of whether or not this Drawable is actually on the
	// screen.  It is not publicly settable.  The public evaluateVisible()
	// function updates this value, while the protected setVisible_()
	// method allows derived classes to set the value.  Note that this
	// value represents a state depending on the parent Container as well
	// as other attributes of this object.  Be careful not to confuse this
	// with isShown_ which can be publicly set, but not changed internally.
	Boolean isVisible_;

	//@ Data-Member: isShown_
	// This Boolean can be set by the user of the class.  When FALSE the
	// object is not drawn on the next repainting of the screen.  Changing
	// this value always causes isVisible_ to be reevaluated.
	Boolean isShown_;

	//@ Data-Member: lastDrawnArea_
	// This area is the area that was occupied by this Drawable when it was
	// last drawn.  If it was not on the screen then the area will be all
	// zeros.  Note that this is a "real" area in absolute coordinates.
	// This is important because the old position and the new position of
	// the Drawable may be in different containers.
	Area lastDrawnArea_;

	//@ Data-Member: pBaseWhereLastDrawn_
	// This pointer is set non-null when changing() is called on a Drawable
	// that is currently visible (and therefore will need to be erased).
	// During a repaint, if it points to the BaseContainer being repainted,
	// this indicates that the lastDrawnArea_ for this Drawable is valid.
	// Setting it to the current pBaseContainer_ records which base it was
	// visible in, so the repaint() method can determine whether or not to add
	// the lastDrawnArea_ to its area list.  Note that it is valid only after
	// changing() is called - it is not updated by the draw() method.
	BaseContainer* pBaseWhereLastDrawn_;

	//@ Data-Member: pStoredImage
	// A pointer to the image stored in off-screen memory use when drawing
	// this object in the emulated overlay plane
	void *pStoredImage_;
	
};

// Inlined methods
#include "Drawable.in"

#endif // Drawable_HH 
