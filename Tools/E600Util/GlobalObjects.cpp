#include "StdAfx.h"
#include "GlobalObjects.h"
GlobalObjects* GlobalObjects::Instance_ = NULL;

GlobalObjects::GlobalObjects(void)
{
	initialize_();
}

GlobalObjects::~GlobalObjects(void)
{
}

GlobalObjects* GlobalObjects::getInstance()
{
	if ( Instance_ == NULL )
	{
		Instance_ = new	GlobalObjects;
	}

	return Instance_;
}

void GlobalObjects::initialize_()
{
	LOGFONT logFont;
	memset(&logFont, 0, sizeof(LOGFONT));
	wcscpy_s(logFont.lfFaceName, 32, _T("Arial"));
	logFont.lfHeight = 16;
	logFont.lfWeight = FW_BOLD;
	logFont.lfCharSet = ANSI_CHARSET;

	Arial_16_Bold_.CreateFontIndirect(&logFont);

	logFont.lfHeight = 32;
	Arial_32_Bold_.CreateFontIndirect(&logFont);

	logFont.lfHeight = 40;
	Arial_40_Bold_.CreateFontIndirect(&logFont);


	////////////////////////////////////////////////////////////////////////

	PenForCurve_.CreatePen(PS_SOLID, 1, RGB(0, 255, 0));
	PenForOldCurve_.CreatePen(PS_SOLID, 1, RGB(0, 127, 0));
	PenForWiper_.CreatePen(PS_SOLID, 2, RGB(0, 0, 0));
}

CFont* GlobalObjects::getArial_Bold(int fontHeight)
{
	switch(fontHeight)
	{
	case 32:
		return &Arial_32_Bold_;
	case 40:
		return &Arial_40_Bold_;
	default:
		return &Arial_16_Bold_;
	}
}

CPen* GlobalObjects::getPen( GlobalPenType penType )
{
	switch ( penType )
	{
	case Pen_Curve:
		return &PenForCurve_;
		break;
	case Pen_OldCurve:
		return &PenForOldCurve_;
		break;
	case Pen_Wiper:
		return &PenForWiper_;
		break;
	}
	return &PenForWiper_;
}


