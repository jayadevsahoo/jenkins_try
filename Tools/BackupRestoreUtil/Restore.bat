@Echo OFF
rem *************************************************************
rem : This batch file can Restore the files. It dynamically create the temp.txt file.
rem : This text files consist of ftp commands to transfer the files. It will Restore  
rem : the stored files form local computer GUI and BD folder to ProgramFiles folder.
rem *************************************************************

cls
@Echo OFF
rem ****** Check for GUI Folder *********************************
IF NOT EXIST .\GUI GOTO NOGUIDIR
rem ****** Create GUI temp text file for ftp commands  ***************
echo anonymous>>temp.txt
echo abc@covidien.com>>temp.txt
echo cd ProgramFiles>>temp.txt
echo lcd GUI>>temp.txt
echo mput *.dat>>temp.txt
echo mput *.crc>>temp.txt
echo mput *.tbl>>temp.txt
echo disconnect>>temp.txt
echo quit>>temp.txt

@Echo OFF
rem ******* Restore the data to GUI ************************
ftp -i -s:temp.txt 192.168.0.2

del temp.txt

GOTO BDSTART

:NOGUIDIR
echo "Restore Failed !!! GUI backup directory/Folder not found"


:BDSTART
rem ****** Check for GUI Folder *********************************
IF NOT EXIST .\BD GOTO NOBDDIR
rem ****** Create BD temp text file for ftp commands  ***************
echo anonymous>>temp.txt
echo abc@covidien.com>>temp.txt
echo cd ProgramFiles>>temp.txt
echo lcd BD>>temp.txt
echo mput *.dat>>temp.txt
echo mput *.crc>>temp.txt
echo mput *.tbl>>temp.txt
echo disconnect>>temp.txt
echo quit>>temp.txt

rem ****** Restore the data to BD*********************************
ftp -i -s:temp.txt 192.168.0.3

del temp.txt

GOTO EXIT

:NOBDDIR
echo "Restore Failed !!! BD backup directory/Folder not found"

:EXIT
rem *********** Script Ends *************************************