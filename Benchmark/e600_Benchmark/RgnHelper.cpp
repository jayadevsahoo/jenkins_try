#include "StdAfx.h"
#include "RgnHelper.h"

RgnHelper::RgnHelper(void)
{
}

RgnHelper::~RgnHelper(void)
{
}

void RgnHelper::appendRgn( CRgn* pTargetRgn, const LPRECT lpRect )
{
	ASSERT(NULL != pTargetRgn);

	CRgn rgnNew;
	rgnNew.CreateRectRgnIndirect( lpRect );

	appendRgn(pTargetRgn, &rgnNew);

	rgnNew.DeleteObject();
}

void RgnHelper::appendRgn( CRgn* pTargetRgn, CRgn* pRgn )
{
	ASSERT(NULL != pTargetRgn);
	ASSERT(NULL != pRgn);

	CRgn rgnCopy;
	rgnCopy.CreateRectRgn(0, 0, 0, 0);
	rgnCopy.CopyRgn( pTargetRgn );
	
	pTargetRgn->CombineRgn(&rgnCopy,  pRgn, RGN_OR);
	rgnCopy.DeleteObject();
}

void RgnHelper::setEmpty( CRgn* pRgn )
{
	pRgn->SetRectRgn(0, 0, 0, 0);
}

void RgnHelper::substractRgn( CRgn* pTargetRgn, const LPRECT lpRect )
{
	ASSERT(NULL != pTargetRgn);

	CRgn rgnNew;
	rgnNew.CreateRectRgnIndirect( lpRect );

	substractRgn(pTargetRgn, &rgnNew);

	rgnNew.DeleteObject();
}

void RgnHelper::substractRgn( CRgn* pTargetRgn, CRgn* pRgn )
{
	ASSERT(NULL != pTargetRgn);
	ASSERT(NULL != pRgn);
	CRgn rgnCopy;
	rgnCopy.CreateRectRgn(0, 0, 0, 0);
	rgnCopy.CopyRgn( pTargetRgn );
	pTargetRgn->CombineRgn(&rgnCopy,  pRgn, RGN_DIFF);
	rgnCopy.DeleteObject();
}

