#pragma once
#include <winsock2.h>

#define Uint8 unsigned char

#define RETAILMSG

class UdpCommunication
{
private:
	bool		recvInitialized_;		// recv socket initialized or not
	bool		sendInitialized_;		// send socket initialized or not

	SOCKET		recvSocket_;			// socket for receive
	SOCKET		sendSocket_;			// socket for send data
	SOCKADDR_IN sendtoSockaddr_;		// address to send to

public:

	UdpCommunication(void);
	~UdpCommunication(void);

	// send or receive
	enum UDPDirection
	{
		UDP_SEND	= 0x1,		// 0001b
		UDP_RECEIVE = 0x2,		// 0010b
		UDP_BOTH	= 0x3		// 0011b
	};

	// when the direction is UdpSend, set recvPort to 0 and specifiy sendIpAddr, sendPort
	bool setup(UDPDirection direction, unsigned short recvPort,
								const char* sendIpAddr = NULL, unsigned short sendPort = 0);

	// stop UDP connections
	bool stop();

	// a function to read char data from socket port
	bool readData(Uint8* DataBuffer, const DWORD NumberOfBytesToRead, DWORD* NumberOfBytesRead, 
								const DWORD MaxMsToWait = 0); 

	// a function to write char data from from a local buffer to serial port buffer
	bool writeData(Uint8* DataBuffer, const DWORD NumberOfBytesToWrite, DWORD* NumberOfBytesWritten);
};
