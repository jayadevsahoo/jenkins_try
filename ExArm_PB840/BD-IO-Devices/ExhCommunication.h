#ifndef EXH_COMMUNICATION_H

#define EXH_COMMUNICATION_H

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
//@ Class: ExhCommunication_t - Configure serial port used to communicate
//			with exhalation flow sensor.
//***********************************************************************

#include "SerialCommunication.h"

enum ExhSensorBoard_t
{
	EXH_SENSOR_BOARD_OLD,
	EXH_SENSOR_BOARD_NEW,
};

class ExhCommunication_t : public SerialCommunication_t
{     
    public:
        
        ExhCommunication_t();

        ~ExhCommunication_t(void);
            
		ExhSensorBoard_t GetSensorBoardType() { return exhSensorBoard; }

        virtual bool StartSerialCommunication(BYTE    ComPortNumber               =   2,           // default COM port is 2
                                              DWORD   BaudRate                    =   CBR_19200,
                                              BYTE    ByteSize                    =   8,
                                              BYTE    Parity                      =   NOPARITY,
                                              BYTE    StopBits                    =   ONESTOPBIT,
                                              DWORD   ReadIntervalTimeout         =   MAXDWORD,
                                              DWORD   ReadTotalTimeoutMultiplier  =   0,
                                              DWORD   ReadTotalTimeoutConstant    =   0,
                                              DWORD   WriteTotalTimeoutMultiplier =   0,
                                              DWORD   WriteTotalTimeoutConstant   =   5000);
    protected:

		bool DetectSensorBoard(DWORD baudRate, uint8_t command);
		void RetrieveSerialNumber();
		ExhSensorBoard_t exhSensorBoard;
};


#endif

