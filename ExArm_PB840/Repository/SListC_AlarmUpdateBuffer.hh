 
#ifndef SListC_AlarmUpdateBuffer_HH
#define SListC_AlarmUpdateBuffer_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: SListC_AlarmUpdateBuffer - Singly-Linked List of 'AlarmUpdateBuffer' Items.
//--------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Foundation/vcssrc/AbstractSListC.H_v   25.0.4.0   19 Nov 2013 14:03:42   pvcs  $
//
//@ Modification-Log
//
//   Revision: 003   By: sah   Date: 07-Jan-1999   DR Number: 5321
//   Project:  ATC
//   Description:
//	Changed 'SoftFault()' method to an non-inlined method.
//  
//   Revision: 002   By: sah   Date: 06-Feb-1996   DCS Number: 674
//   Project:  Sigma (R8027)
//   Description:
//	Removed 'ifndef' and 'endif' from around includes.
//  
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//====================================================================

#if defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)
class  Ostream;
#endif  // defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)

//@ Usage-Classes
#include "SNodeC_AlarmUpdateBuffer.hh"
#include "Heap.hh"
//@ End-Usage


class SListC_AlarmUpdateBuffer
{
#if defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)
    // Friend:  operator<<
    // This facilitates debug-only printing of this class's instances.
    friend inline Ostream&  operator<<(Ostream&                  ostr,
				       const SListC_AlarmUpdateBuffer& sList);
#endif  // defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)

    //@ Friend:  SIterC_AlarmUpdateBuffer
    // This list's corresponding iterator needs access to the protected
    // members of this list.
    friend class  SIterC_AlarmUpdateBuffer;

    //@ Friend:  SListR_AlarmUpdateBuffer
    // The singly-linked reference list uses a singly-linked list of
    // void*'s, and needs access to it's protected members.
    friend class  SListR_AlarmUpdateBuffer;

  public:
    virtual ~SListC_AlarmUpdateBuffer(void);

    inline Boolean  isEmpty(void) const;
    inline Boolean  isFull (void) const;

    inline Boolean  isAtFirst(void) const;
    inline Boolean  isAtLast (void) const;

    inline Uint32  getNumItems(void) const;
    inline Uint32  getMaxItems(void) const;

    inline SigmaStatus  goFirst(void);
    inline SigmaStatus  goNext (void);

    SigmaStatus  search(const AlarmUpdateBuffer&      item,
			const SearchFrom startFrom = FROM_FIRST);

    void  insert (const AlarmUpdateBuffer&          newItem,
		  const InsertionPlace insertPlace = AFTER_CURRENT);
    void  append (const AlarmUpdateBuffer& newItem);
    void  prepend(const AlarmUpdateBuffer& newItem);

    SigmaStatus  removeCurrent(void);
    SigmaStatus  removeItem(const AlarmUpdateBuffer& item);
    void         clearList(void);

    inline const AlarmUpdateBuffer&  currentItem(void) const;
    inline AlarmUpdateBuffer&        currentItem(void);

    void  operator=(const SListC_AlarmUpdateBuffer& sList);

    inline Boolean  operator==(const SListC_AlarmUpdateBuffer& sList) const;
    inline Boolean  operator!=(const SListC_AlarmUpdateBuffer& sList) const;

    static void  SoftFault(const SoftFaultID softFaultID,
                           const Uint32      lineNumber,
                           const char*       pFileName = NULL,
                           const char*       pBoolTest = NULL);

  protected:
    inline SListC_AlarmUpdateBuffer(Heap* pHeap);

    // return a pointer to the link that is equivalent to 'item', starting
    // the search from 'pStart'...
    const SNodeC_AlarmUpdateBuffer* findNode_(const AlarmUpdateBuffer&        item,
				 const SNodeC_AlarmUpdateBuffer* pStartNode) const;

    inline Boolean  isLegalNode_(const SNodeC_AlarmUpdateBuffer* pNode) const;

    inline const SNodeC_AlarmUpdateBuffer*  getFirst_  (void) const;
    inline const SNodeC_AlarmUpdateBuffer*  getCurrent_(void) const;

    inline SNodeC_AlarmUpdateBuffer*  getFirst_  (void);
    inline SNodeC_AlarmUpdateBuffer*  getCurrent_(void);

    inline void  setCurrent_(SNodeC_AlarmUpdateBuffer* pNewCurr);

    Boolean  isEquivTo_(const SListC_AlarmUpdateBuffer& sList) const;

#if defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)
    virtual Ostream&  print_(Ostream& ostr) const = 0;
#endif  // defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)

  private:
    SListC_AlarmUpdateBuffer(const SListC_AlarmUpdateBuffer&);	// not implemented...
    SListC_AlarmUpdateBuffer(void);				// not implemented...

    inline void  deallocNode_(SNodeC_AlarmUpdateBuffer*& pOldNode);

    SNodeC_AlarmUpdateBuffer* getPrevNode_(const SNodeC_AlarmUpdateBuffer* pStart) const;

    //@ Data-Member:  rHeapOfNodes_
    // A reference to this list's heap of nodes.
    Heap&  rHeapOfNodes_;

    //@ Data-Member:  pFirst_
    // A pointer to the first node in this list.
    SNodeC_AlarmUpdateBuffer*  pFirst_;

    //@ Data-Member:  pCurrent_
    // A pointer to the current node of this list.
    SNodeC_AlarmUpdateBuffer*  pCurrent_;
};


// Inlined methods...
#include "SListC_AlarmUpdateBuffer.in"


#endif  // SListC_AlarmUpdateBuffer_HH
