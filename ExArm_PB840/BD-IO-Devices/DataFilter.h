
//*****************************************************************************
//
//  PressureFilter.h - Header file for the PressureFilter_t class. This class
//      is used by the servo thread for filtering pressure data from Paw, Pint, etc.
//
//*****************************************************************************

#ifndef DATA_FILTER_H

#define DATA_FILTER_H

#include "Sigma.hh"

template <class T>
class DataFilter_t
{
    public:
		DataFilter_t() { FilteredValue = 0; }

		~DataFilter_t() { }

        T FilterValue(T UnfilteredValue, uint16_t TimeConst)
		{
			FilteredValue = ((FilteredValue * TimeConst + UnfilteredValue) - FilteredValue) / TimeConst;
			return FilteredValue;
		}

        T GetFilteredValue()
        {
            return FilteredValue;
        }


    protected:
        T FilteredValue;
};


#endif

