// TpSocket.cpp : implementation file
//

#include "stdafx.h"
#include "softd.h"
#include "TpSocket.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// TftpClientSocket

//////////////////////////////////////////////////////////////////////
TftpClientSocket::TftpClientSocket()
{
	bpServer = NULL;
	clientCallback = NULL;
	clientFilePostion = 0l;

	return;
}

//////////////////////////////////////////////////////////////////////
TftpClientSocket::~TftpClientSocket()
{
	bpServer = NULL;
	clientFile.Abort();
	Close();

	return;
}


//////////////////////////////////////////////////////////////////////
TftpClientSocket::TftpClientSocket(int id, BootPServer *server)
{
	clientFilePostion = 0l;
	bpServer = server;
	tftpClientId = id;
	clientCallback = NULL;
	CURRENT_STATE = ACK_WAITING; //bait the hook

	return;
}

// Do not edit the following lines, which are needed by ClassWizard.
#if 0
BEGIN_MESSAGE_MAP(TftpClientSocket, CSocket)
	//{{AFX_MSG_MAP(TftpClientSocket)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()
#endif	// 0

/////////////////////////////////////////////////////////////////////////////
// TftpClientSocket member functions

//////////////////////////////////////////////////////////////////////
void TftpClientSocket::SetClientCallback(void (*callback)(int, int, int))
{
	clientCallback = callback;

	return;
}

//////////////////////////////////////////////////////////////////////
void TftpClientSocket::OnReceive(int nErrorCode) 
{
	int nread;

	if(CURRENT_STATE == RRQ_RECEIVED){
		DoRRQ();
	}
	else{
		nread = Receive((char *) &incomingPacket, sizeof(tftpMsgHeader));
		if(nread == SOCKET_ERROR){
			CURRENT_STATE = CLIENT_ERROR;
			bpServer->ErrorControl(ERR_SOCKET_RECIEVE);
		}
		else{
			ProcessPacket();
		}
	}

	CSocket::OnReceive(nErrorCode);

	return;
}

//////////////////////////////////////////////////////////////////////
void TftpClientSocket::ProcessPacket()
{
	int errflag = 0;

	if(CURRENT_STATE != CLIENT_ERROR){

		incomingPacket.opCode   = htons(incomingPacket.opCode);
		incomingPacket.pktSize  = htons(incomingPacket.pktSize);
		incomingPacket.blockNum = htons(incomingPacket.blockNum);

		if(CURRENT_STATE == DATA_DONE){
			if(incomingPacket.opCode != OP_DOWNLOAD_SUCCESS){
				bpServer->ErrorControl(ERR_END_OF_FILE);  //past end of file
			}
		}

		switch(incomingPacket.opCode){

			case OP_RRQ:               CURRENT_STATE = RRQ_RECEIVED;
				                       break;

			case OP_DATA:	           bpServer->ErrorControl(ERR_SYSTEM_SEQ);
				                       break;

			case OP_DATA_ACK:          // VERIFY BLOCKNUM
				                       if(CURRENT_STATE != END_OF_FILE_REACHED){
					                       SendData(NEXT_BLOCK);
				                       }
									   else{
				                           CURRENT_STATE = DATA_DONE;
				                       }
				                       break;

			case OP_END_OF_FILE:       bpServer->ErrorControl(ERR_SYSTEM_SEQ);
				                       break;

			case OP_DOWNLOAD_SUCCESS:  CompleteRequest();
				                       break;

			case OP_DATA_REQUEST:      SendData(CURRENT_BLOCK);
				                       break;

			case OP_ERROR:             bpServer->ErrorControl(ERR_VENT_ERROR);
				                       break;

			default:                   bpServer->ErrorControl(ERR_SYSTEM_SEQ);
				                       break;
		} // endswitch(incomingPacket.opCode)
 
	} //end if CLIENT ERROR

	return;
}

//////////////////////////////////////////////////////////////////////
void TftpClientSocket::DoRRQ()
{
	TftpRRQMsg incomingMsg;
	int nerror;
	int ferror;
	int errflag = 0;
	char fileName[256];

	if(CURRENT_STATE != CLIENT_ERROR){
		nerror = Receive((char*)&incomingMsg, sizeof(incomingMsg));
		if(nerror == SOCKET_ERROR){
			errflag = ERR_SOCKET_RECIEVE;
		}

		if(!errflag){
			strcpy(fileName, bpServer->FilePath());
			strcat(fileName, incomingMsg.fileName);
			ferror = clientFile.Open(fileName, 
			                         ( CFile::modeRead 
									 | CFile::typeBinary 
									 | CFile::shareExclusive)
									 );
			if(ferror == 0){
				errflag = 10;
			}
			else{
				bpServer->setImageId(incomingMsg.fileName, &imageId);
			}
		}

		if(!errflag){
			clientFileSize = clientFile.GetLength();
			tblock = clientFileSize / DATA_BLOCK_SIZE;
			SendData(NEXT_BLOCK);
			CURRENT_STATE = ACK_WAITING;
		}

		if(errflag){
			CURRENT_STATE = CLIENT_ERROR;
			bpServer->ErrorControl(errflag);
		}
	} // end if(CURRENT_STATE != CLIENT_ERROR)

	return;
}

//////////////////////////////////////////////////////////////////////
void TftpClientSocket::CompleteRequest()
{
	clientFile.Close();

	clientCallback(imageId, -1, -1); // job done

	Close();  // close this socket from the stream connection;

	return;
}

//////////////////////////////////////////////////////////////////////
void TftpClientSocket::SendData(int block)
{
	unsigned char buff[DATA_BLOCK_SIZE + 1];
	int nread;
	int nerror;
	TftpMsgHeader outMsg;
	int errflag = 0;

	if(block == NEXT_BLOCK){
		clientFile.Seek(clientFilePostion, CFile::begin);
		nread = clientFile.Read(buff, (unsigned int)DATA_BLOCK_SIZE);
		clientFilePostion = (long)DATA_BLOCK_SIZE + clientFilePostion;
	}
	else if(block == CURRENT_BLOCK){
		clientFilePostion -= (long)(DATA_BLOCK_SIZE);
		clientFile.Seek(clientFilePostion, CFile::begin);
		nread = clientFile.Read(buff, (long)DATA_BLOCK_SIZE);
	}

	outMsg.opCode  = htons((unsigned short int)OP_DATA);
	outMsg.pktSize = htons((unsigned short int)DATA_BLOCK_SIZE);

	if(clientFilePostion == (long)DATA_BLOCK_SIZE){
		outMsg.blockNum = htons(1);
	}
	else{
		outMsg.blockNum = htons( ((unsigned short int)(clientFilePostion / (long)DATA_BLOCK_SIZE)) );
	}

	clientCallback(imageId, (int)(clientFilePostion / (long)DATA_BLOCK_SIZE), tblock);

	nerror = Send((char *)&outMsg, sizeof(outMsg)); // send header
	if(nerror == SOCKET_ERROR){
		errflag = ERR_SOCKET_TRANSMIT; // bad send
	}

	if(!errflag){
		nerror = Send((char *)buff, DATA_BLOCK_SIZE);
		if(nerror == SOCKET_ERROR){
			errflag = ERR_SOCKET_TRANSMIT; // bad send
		}
	}

	if(!errflag){
		CURRENT_STATE = ACK_WAITING;
	}

	if(nread < DATA_BLOCK_SIZE && !errflag){
		outMsg.opCode   = htons(OP_END_OF_FILE);
		outMsg.pktSize  = 0;
		outMsg.blockNum = 0;
		CURRENT_STATE = END_OF_FILE_REACHED;

		nerror = Send((char *)&outMsg, sizeof(outMsg));
		if(nerror == SOCKET_ERROR){
			errflag = ERR_SOCKET_TRANSMIT;
		}
	}

	if(errflag){
		CURRENT_STATE = CLIENT_ERROR;
		bpServer->ErrorControl(errflag);
	}

	return;
}

//////////////////////////////////////////////////////////////////////
int TftpClientSocket::ClientId()
{
	return(tftpClientId);
}

