#ifndef tod_HH
#define tod_HH


//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================ H E A D E R   D E S C R I P T I O N ====
//@ Filename: tod.hh
//
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Kernel/vcssrc/tod.hhv   25.0.4.0   19 Nov 2013 14:13:56   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003    By: Cederquist    Date: 27-Jan-2011  SCR Number: 6671
//  Project: XENA2
//  Description:
//      Cleaned up interface by maintaining the previous public interface
//      and hiding the separate TOD test internal to tod.cc.
//
//  Revision: 002    By: Mitesh Raval  Date: 29-Sep-2010  SCR Number: 6671
//  Project: XENA2
//  Description:
//      New methods added only for Xena2.
//
//  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//=====================================================================

#include "Sigma.hh"
#include "kpost.hh"


//@ Begin-Free-Declarations

Criticality TimeOfDayClockTest(void);
void        DisplayTOD(void);

//@ End-Free-Declarations

#endif  // tod_HH
