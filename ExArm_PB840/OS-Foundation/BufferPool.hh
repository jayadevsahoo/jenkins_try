#ifndef BufferPool_HH
#define BufferPool_HH

//====================================================================
// This is a proprietary work to which Nellcor Puritan Bennett
// corporation of California claims exclusive right.  No part of this
// work may be used, disclosed, reproduced, sorted in an information
// retrieval system, or transmitted by any means, electronic,
// mechanical, photocopying, recording, or otherwise without the prior
// written permission of Nellcor Puritan Bennett Corporation.
//
//     Copyright (c) 1995, Nellcor Puritan Bennett Corporation
//=====================================================================
 
//====================================================================
// Class:  BufferPool - Sys-Init subsystem class
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/OS-Foundation/vcssrc/BufferPool.hhv   25.0.4.0   19 Nov 2013 14:15:16   pvcs  $
//
//@ Modification-Log
// 
//  Revision: 002   By: sah   Date: 13-Jan-1999  DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001    By: Gary Cederquist  Date: 15-APR-1997  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//====================================================================
 

//@ Usage-Classes
#include "OsFoundation.hh"
//@ End-Usage

class BufferPool 
{
  public:
    BufferPool(Uint poolId, Uint * pBufferPool, Uint sizePool, Uint sizeBuffer);
    ~BufferPool();
 
    void *              allocate(size_t nbytes);
    void                freeThePool(void * pObject);
    
    Int32               getFreeBufferCount(void) const;
    Int32               getTotalBufferCount(void) const;

    static void         SoftFault(const SoftFaultID  softFaultID,
				  const Uint32       lineNumber,
				  const char*        pFileName = NULL,
				  const char*        pPredicate = NULL);

  private:
    BufferPool();                       // declared only
    BufferPool(const BufferPool&);      // declared only
    void operator=(const BufferPool&);  // declared only

    //@ Data-Member:    pBufferPool_
    //  Pointer to the buffer pool memory.
    Uint *  pBufferPool_;

    //@ Data-Member:    poolId_
    //  Buffer pool identifier passed to VRTX for partition operations.
    Uint    poolId_;

    //@ Data-Member:    sizePool_
    //  Size (in bytes) of the buffer pool.
    Uint    sizePool_;

    //@ Data-Member:    sizeBuffer_
    //  Size (in bytes) of each buffer in the buffer pool.
    Uint    sizeBuffer_;

};
 

#endif // BufferPool_HH
