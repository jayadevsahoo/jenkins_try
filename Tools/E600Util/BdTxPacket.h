#pragma once

#include "ClientSocket.h"

class BdTxPacket_t
{
public:	
	BdTxPacket_t();
	~BdTxPacket_t();

	void SendBdPacket(char* pPacket, UINT16 PacketSize);

	CClientSocket m_BdTxSocket;
	int Port;
	CString IpAddr;	
	
protected:

};

extern BdTxPacket_t BdTxPacket;


