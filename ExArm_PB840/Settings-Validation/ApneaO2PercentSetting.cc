#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//================= C L A S S   D E S C R I P T I O N =================
//@ Class:  ApneaO2PercentSetting - Apnea Oxygen Percentage Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, bounded setting that contains the percentage of oxygen
//  in the gas delivered to the patient during apnea ventilation.  This class
//  inherits from 'BatchBoundedSetting' and provides the specific information
//  needed for representation of apnea oxygen percentage.  This information
//  includes the interval and range of this setting's values, and this batch
//  setting's new-patient value (see 'getNewPatientValue()').
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class has no dependent settings, but it does have dynamic constraints,
//  therefore 'updateConstraints_()' is overridden.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/ApneaO2PercentSetting.ccv   25.0.4.0   19 Nov 2013 14:27:12   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005   By: sah    Date: 29-Jun-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  modified to use newly-modified 'BoundedRange' class to handle
//         range, via linked list of interval nodes
//	*  now using circuit-specific new-patient value
//
//  Revision: 004   By: sah  Date:  11-Jan-1999    DR Number:  5321
//  Project:  ATC
//  Description:
//	As part of making all 'SoftFault()' methods non-inlined, I'm
//	also making those 'SoftFault()' methods that are NOT called
//	in PRODUCTION mode, DEVELOPMENT-only.
//
//  Revision: 003   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'getApplicability()' method
//	*  added new 'correctSelf()' method to offload implementation
//	   detail from AdjustedContext to the specific setting
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//=====================================================================

#include "ApneaO2PercentSetting.hh"
#include "PatientCctTypeValue.hh"

//@ Usage-Classes
#include "ContextId.hh"
#include "SettingsMgr.hh"
#include "ApneaIntervalSetting.hh"
//@ End-Usage


//@ Code...

//====================================================================
//
//  Static Data Definitions...
//
//====================================================================

//  $[02077] -- The setting's range ...
//  $[02079] -- The setting's resolution ...
static const BoundedInterval  LAST_NODE_ =
  {
    21.0f,		// absolute minimum...
    0.0f,		// unused...
    ONES,		// unused...
    NULL
  };
static const BoundedInterval  INTERVAL_LIST_ =
  {
    100.0f,		// absolute maximum...
    1.0f,		// resolution...
    ONES,		// precision...
    &::LAST_NODE_
  };


//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============= M E T H O D   D E S C R I P T I O N ==================
//@ Method: ApneaO2PercentSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This setting inherits from BoundedSetting.  The method passes to
//  BoundedSetting it's default information. Also, the value interval
//  for this setting is initialized.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ApneaO2PercentSetting::ApneaO2PercentSetting(void)
	 : BatchBoundedSetting(SettingId::APNEA_O2_PERCENT,
			       Setting::NULL_DEPENDENT_ARRAY_,
			       &::INTERVAL_LIST_,
			       APNEA_O2_MAX_ID,	// maxConstraintId...
			       APNEA_O2_MIN_ID)	// minConstraintId...
{
  CALL_TRACE("ApneaO2PercentSetting()");
}   // $[TI1]


//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  ~ApneaO2PercentSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ApneaO2PercentSetting::~ApneaO2PercentSetting(void)
{
  CALL_TRACE("~ApneaO2PercentSetting()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Apnea oxygen percentage is ALWAYS changeable.
//
//  $[01096] -- apnea breath settings applicability
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
ApneaO2PercentSetting::getApplicability(const Notification::ChangeQualifier) const
{
  CALL_TRACE("getApplicability(qualifierId)");

  return(Applicability::CHANGEABLE);
}   // $[TI1]


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [const, virtual]
//
//@ Interface-Description
//  Get the setting's new-patient value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02078] -- new-patient value...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
ApneaO2PercentSetting::getNewPatientValue(void) const
{
  CALL_TRACE("getNewPatientValue()");

  BoundedValue  newPatient;

  const Setting*  pPatientCctType =
		    SettingsMgr::GetSettingPtr(SettingId::PATIENT_CCT_TYPE);

  const DiscreteValue  PATIENT_CCT_TYPE_VALUE =
					pPatientCctType->getAdjustedValue();

  switch (PATIENT_CCT_TYPE_VALUE)
  {
  case PatientCctTypeValue::NEONATAL_CIRCUIT :		// $[TI1]
    newPatient.value     = 40.0f;
    newPatient.precision = ONES;
    break;
  case PatientCctTypeValue::PEDIATRIC_CIRCUIT :
  case PatientCctTypeValue::ADULT_CIRCUIT :		// $[TI2]
    newPatient.value     = 100.0f;
    newPatient.precision = ONES;
    break;
  default :
    // unexpected patient circuit type value...
    AUX_CLASS_ASSERTION_FAILURE(PATIENT_CCT_TYPE_VALUE);
    break;
  }

  return(newPatient);
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  correctSelf()
//
//@ Interface-Description
//  Correct the value of this setting, with respect to its bound
//  contraints, and return a boolean as to whether a correction was
//  needed ('TRUE'), or not ('FALSE').
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02029]\b\ -- transition to a valid apnea value...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean
ApneaO2PercentSetting::correctSelf(void)
{
  CALL_TRACE("correctSelf()");

  BoundedValue  apneaO2Percent = getAdjustedValue();

  BoundStatus  dummyStatus(getId());

  // calculate bound constraints...
  updateConstraints_();

  // use the calculation of the bounds in 'updateConstraints_()'
  // to limit the current adjusted value to a valid range...
  getBoundedRange_().testValue(apneaO2Percent, dummyStatus);

  if (dummyStatus.isInViolation())
  {  // $[TI1] -- the current adjusted value required correction...
    // store the "clipped" value into the adjusted context...
    setAdjustedValue(apneaO2Percent);

    // force the change flag due to this correction...
    setForcedChangeFlag();
  }  // $[TI2] -- no correction was needed...

  return(dummyStatus.isInViolation());
}


#if defined(SIGMA_DEVELOPMENT)

//============== M E T H O D   D E S C R I P T I O N =================
// Method:  isAcceptedValid()  [const, virtual]
//
// Interface-Description
//  Is the accepted value of this setting valid as determined by its
//  dependencies?
//---------------------------------------------------------------------
// Implementation-Description
//  The setting determines, and returns its validity.
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

Boolean
ApneaO2PercentSetting::isAcceptedValid(void) const
{
  CALL_TRACE("isAcceptedValid()");

  Boolean  isValid = TRUE;

  if (ApneaIntervalSetting::IsApneaPossible(BASED_ON_ACCEPTED_VALUES))
  {
    // is the accepted apnea inspiratory time value within the min and max
    // values?...
    isValid = BoundedSetting::isAcceptedValid(); // forward to base class...
  }

  return(isValid);
}


//============== M E T H O D   D E S C R I P T I O N =================
// Method:  isAdjustedValid()  [virtual]
//
// Interface-Description
//  Is the adjusted value of this setting valid as determined by its
//  dependencies?
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

Boolean
ApneaO2PercentSetting::isAdjustedValid(void)
{
  CALL_TRACE("isAdjustedValid()");

  Boolean  isValid = TRUE;

  if (ApneaIntervalSetting::IsApneaPossible(BASED_ON_ADJUSTED_VALUES))
  {
    // is the adjusted apnea oxygen percentage value within the min and max
    // values?...
    isValid = BoundedSetting::isAdjustedValid(); // forward to base class...
  }

  return(isValid);
}

#endif // defined(SIGMA_DEVELOPMENT)


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//	      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
ApneaO2PercentSetting::SoftFault(const SoftFaultID  softFaultID,
				 const Uint32       lineNumber,
				 const char*        pFileName,
				 const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
			  APNEA_O2_PERCENT_SETTING, lineNumber, pFileName,
			  pPredicate);
}


//====================================================================
//
//  Protected Methods...
//
//====================================================================

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  updateConstraints_()  [virtual]
//
//@ Interface-Description
//  Update the dynamic bounds of this setting.  This method sets this
//  setting's maximum constraint to the most restrictive of this setting's
//  upper bounds, and, correspondingly, set this setting's minimum
//  constraint to its most restrictive lower bound.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Determine the maximum lower bound, and set the lower limit to it.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
ApneaO2PercentSetting::updateConstraints_(void)
{
  CALL_TRACE("updateConstraints_()");

  const BoundedSetting*  pOxygenPercent =
	       SettingsMgr::GetBoundedSettingPtr(SettingId::OXYGEN_PERCENT);

  BoundedRange::ConstraintInfo  minConstraintInfo;

  const Real32  ABSOLUTE_MIN = getAbsoluteMinValue_();
  const Real32  O2_PERCENT_BASED_MIN_VALUE =
		      BoundedValue(pOxygenPercent->getAdjustedValue()).value;

  if (O2_PERCENT_BASED_MIN_VALUE > ABSOLUTE_MIN)
  {   // $[TI1]
    // the oxygen-percentage-based minimum is more restricive, therefore set
    // it as the lower bound of apnea interval...
    minConstraintInfo.id    = APNEA_O2_MIN_BASED_O2_ID;
    minConstraintInfo.value = O2_PERCENT_BASED_MIN_VALUE;
  }
  else
  {   // $[TI2]
    // the absolute minimum is more restricive, therefore set it as the
    // lower bound of apnea interval...
    minConstraintInfo.id    = APNEA_O2_MIN_ID;
    minConstraintInfo.value = ABSOLUTE_MIN;
  }

  // this setting has no minimum soft bounds...
  minConstraintInfo.isSoft = FALSE;

  getBoundedRange_().updateMinConstraint(minConstraintInfo);
}
