/*****************************************************************************/
/*   Module Name:   testuir.h                                            
/*   Purpose:                                     
/*                                                                      
/*                                                                           
/*   Requirements:   This module implements requirements detailed in         
/*          <75359-93>                                                       
/*                     
/*   Modification History:
/*   
/*   Description of Change			        By			      Date 
/*   ---------------------					-----   		  ------
/*	 TestExecutive related code is
/* 	 removed and replaced					Hamsavally(CGS)   18 Oct,2000
/*   corresponding functionality.
/*
/*   $Revision::   1.1      $                                                
/*                                                                          
/*****************************************************************************/

/*****************************************************************************
THIS IS A PROPRIETARY WORK TO WHICH NELLCOR PURITAN BENNETT CORPORATION CLAIMS
EXCLUSIVE RIGHT.  NO PART OF THIS WORK MAY BE USED, DISCLOSED, REPRODUCED,
STORED IN AN INFORMATION RETIEVAL SYSTEM, OR TRANSMITTED, IN ANY FORM OR BY ANY
MEANS ELECTRONIC, MECHANICAL, PHOTOCOPYING, RECORDING OR OTHERWISE WITHOUT THE
PRIOR WRITTEN PERMISSION OF NELLCOR PURITAN BENNETT CORPORATION.
 
Copyright (c) 1999, Mallinckrodt Nellcor Puritan Bennett
All rights reserved.
*******************************************************************************/
#ifndef _TESTUIR_H_
#define _TESTUIR_H_

#include "mainpnls.h"
extern int ResArray[15];
extern int ChkXArray[15];
extern int ChkXDefArray[15];

//enum CHK_X_STATE{
//	CX_PASS=0,
//	CX_FAIL,
//	CX_CLEAR
//};

#define PASS_COLOR		VAL_OUR_GREEN
#define FAIL_COLOR		VAL_OUR_RED
#define RUNNING_COLOR	VAL_OUR_ORANGE
#define LOOPING_COLOR	VAL_OUR_ORANGE
#define SKIPPED_COLOR	VAL_OUR_YELLOW
#define ERROR_COLOR		VAL_OUR_RED

// These include color so update them with colors from common.h if they change:
#define PASS_FMT		"\033fg5de50b"
#define FAIL_FMT		"\033fge83030"
#define RUNNING_FMT		"\033fgFFCF60"  
#define LOOPING_FMT		"\033fgFFCF60"  
#define SKIPPED_FMT		"\033fgffff00"
#define ERROR_FMT		"\033fge83030"

#define FLEFT			""
#define FRIGHT			""
#define FCENTER			""


#endif

