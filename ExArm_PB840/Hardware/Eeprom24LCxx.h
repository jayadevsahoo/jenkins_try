

//#############################################################################
//
//  Eeprom24LCxx.h - Header file for Eeprom24LCxx_t class.
//
//
//#############################################################################

#ifndef EEPROM_24LCXX_H

#define EEPROM_24LCXX_H

#include "I2C.h"
#include "Sigma.hh"


class Eeprom24LCxx_t : public I2C
{
	public:
        Eeprom24LCxx_t(HANDLE FileHandle, uint8_t BusAddress, I2cSubAddressMode_t SubAddrMode, I2cBaudIndex_t BaudIndex, DWORD Timeout = 30);
        ~Eeprom24LCxx_t(){}

        bool Write(uint16_t EepromDestinationAddress, uint8_t *SourceAddress,      int NumberOfBytes);
        bool Read (uint16_t EepromSourceAddress,      uint8_t *DestinationAddress, int NumberOfBytes);
};

#endif
