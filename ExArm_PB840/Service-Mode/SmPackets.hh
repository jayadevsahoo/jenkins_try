#ifndef SmPackets_HH
#define SmPackets_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Struct:  SmPackets - Service Mode inter-task message formats
//---------------------------------------------------------------------
//@ Version-Identification
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/SmPackets.hhv   25.0.4.0   19 Nov 2013 14:23:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By:  gdc    Date: 23-Sep-1997    DR Number: DCS 2513
//       Project:  Sigma (R8027)
//       Description:
//            Modified to support test parameters.
//
//  Revision: 002  By:  quf    Date: 22-May-1997    DR Number: DCS 2131
//       Project:  Sigma (840)
//       Description:
//            Modification-Log fix only.
//
//  Revision: 001  By:  mad    Date:  01-Dec-95    DR Number: none
//	Project:  Sigma (R8027)
//	Description:
//		Initial version (Integration baseline).
//
//====================================================================

#include "SmTestId.hh"
#include "SmPromptId.hh"

//@ Type: SmPacketTypeId
// service mode test/function request packet type ID
enum SmPacketTypeId
{
	TEST_REQUEST,
	FUNCTION_REQUEST
};

//@ Type: SmTestRequest
// service mode test request data structure
struct SmTestRequest
{
	SmTestId::ServiceModeTestId testId;
	Boolean testRepeat;
};

//@ Type: SmMsgData
// message data union for test or function request data
union SmMsgData
{
	SmTestRequest testRequested;
	SmTestId::FunctionId functionId;
};

//@ Type: SmMsgPacket
// service mode test/function request message packet,
// used to send test/function requests from GUI to BD
struct SmMsgPacket
{
    enum SmPacketSize
    {
        PARAMETER_LENGTH = 120
    };
	SmPacketTypeId packetType;
	SmMsgData packetData;
    char packetParameters[SmMsgPacket::PARAMETER_LENGTH];
};

//@ Type: SmActionPacket
// service mode action ID packet,
// used to send operator keypress from GUI to BD
struct SmActionPacket
{
	SmPromptId::ActionId actionId;
};


#endif // SmPackets_HH 
