//-------------------------------------------------------------------------------
//                        Copyright (c) 2010 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//-------------------------------------------------------------------------------
/// @file VirtualSensor.h
///
/// @brief
/// Class deffinition for VirtualSensor and VirtualSensorBuffer.
///

#ifndef VIRTUALSENSOR_H
#define VIRTUALSENSOR_H

#include "SwatBatchUser.h"

#ifndef VIRTUALSENSOR_BUFFER
#define VIRTUALSENSOR_BUFFER 64
#endif

#ifndef VIRTUALRAWSENSOR_BUFFER
#define VIRTUALRAWSENSOR_BUFFER 64
#endif

namespace swat{

    class VirtualSensor
    {
        public:


            typedef union
            {
                float        f;
                unsigned int i;
                unsigned int ui;
            } SensorData;
            
            SensorData rValue;
            static SensorData g_rValue;

            VirtualSensor():bIsSet(false),bIsAdd(false),pValue(&rValue)
            {
                offset.f=0.0f;
            }

            SensorData value, offset;

            bool bIsSet;
            bool bIsAdd;

            SensorData *pValue;
    };



    class VirtualSensorBuffer
    {

        protected:
            static VirtualSensor m_data[VIRTUALSENSOR_BUFFER];

        public:


            static inline void set(unsigned int index, const float &rhs)
            {
                if(index < VIRTUALSENSOR_BUFFER)
                {
                    (&VirtualSensorBuffer::m_data[index].value)->f = rhs;
                    m_data[index].bIsSet=true;
                    m_data[index].bIsAdd=false;
                }
            }


            static inline void add(unsigned int index, const float &rhs)
            {
                if(index < VIRTUALSENSOR_BUFFER)
                {
                    (&VirtualSensorBuffer::m_data[index].offset)->f = rhs;
                    m_data[index].bIsAdd=true;
                    m_data[index].bIsSet=false;
                }
            }

            static inline void unset(unsigned int index)
            {
                if(index < VIRTUALSENSOR_BUFFER)
                {
                    m_data[index].bIsSet=false;
                    m_data[index].bIsAdd=false;
                }
            }


            static inline void reset()
            {
                for(int i=0;i<VIRTUALSENSOR_BUFFER;i++)
                {
                    m_data[i].bIsSet=false;
                    m_data[i].bIsAdd=false;
                }
            }


            static inline void update()
            {
                for(int i=0;i<VIRTUALSENSOR_BUFFER;i++)
                {
                    if(m_data[i].bIsSet == true)
                    {
                        (m_data[i].pValue)->f=(&m_data[i].value)->f;
                    }
                    else if(m_data[i].bIsAdd == true)
                    {
                        (m_data[i].pValue)->f+=(&m_data[i].offset)->f;
                    }
                }
            }

            static inline void update(unsigned int i)
            {
                if( i < VIRTUALSENSOR_BUFFER)

                {

                    if(m_data[i].bIsSet == true)
                    {
                        (m_data[i].pValue)->f=(&m_data[i].value)->f;
                    }
                    else if(m_data[i].bIsAdd == true)
                    {
                        (m_data[i].pValue)->f+=(&m_data[i].offset)->f;
                    }

                }
            }


    };

    class VirtualRawSensorBuffer
    {

        protected:
            static VirtualSensor m_data[VIRTUALRAWSENSOR_BUFFER];

        public:


            static inline void set(unsigned int index, const unsigned int &rhs)
            {
                if(index < VIRTUALRAWSENSOR_BUFFER)
                {
                    (&VirtualRawSensorBuffer::m_data[index].value)->ui = rhs;
                    m_data[index].bIsSet=true;
                    m_data[index].bIsAdd=false;
                }
            }


            static inline void add(unsigned int index, const int &rhs)
            {
                if(index < VIRTUALRAWSENSOR_BUFFER)
                {
                    (&VirtualRawSensorBuffer::m_data[index].offset)->i = rhs;
                    VirtualRawSensorBuffer::m_data[index].bIsAdd=true;
                    VirtualRawSensorBuffer::m_data[index].bIsSet=false;
                }
            }

            static inline void unset(unsigned int index)
            {
                if(index < VIRTUALRAWSENSOR_BUFFER)
                {
                    VirtualRawSensorBuffer::m_data[index].bIsSet=false;
                    VirtualRawSensorBuffer::m_data[index].bIsAdd=false;
                }
            }


            static inline void reset()
            {
                for(int i=0;i<VIRTUALRAWSENSOR_BUFFER;i++)
                {
                    VirtualRawSensorBuffer::m_data[i].bIsSet=false;
                    VirtualRawSensorBuffer::m_data[i].bIsAdd=false;
                }
            }


            static inline void update()
            {
                for(int i=0;i<VIRTUALRAWSENSOR_BUFFER;i++)
                {
                    if(VirtualRawSensorBuffer::m_data[i].bIsSet == true)
                    {
                        (VirtualRawSensorBuffer::m_data[i].pValue)->ui=
                            (&VirtualRawSensorBuffer::m_data[i].value)->ui;
                    }
                    else if(VirtualRawSensorBuffer::m_data[i].bIsAdd == true)
                    {
                        (VirtualRawSensorBuffer::m_data[i].pValue)->ui+=
                            (&VirtualRawSensorBuffer::m_data[i].offset)->i;
                    }
                }
            }

            static inline void update(unsigned int i)
            {
                if( i < VIRTUALRAWSENSOR_BUFFER)

                {
                    if(VirtualRawSensorBuffer::m_data[i].bIsSet == true)
                    {
                        (VirtualRawSensorBuffer::m_data[i].pValue)->ui=
                            (&VirtualRawSensorBuffer::m_data[i].value)->ui;
                    }
                    else if(VirtualRawSensorBuffer::m_data[i].bIsAdd == true)
                    {
                        (VirtualRawSensorBuffer::m_data[i].pValue)->ui+=
                            (&VirtualRawSensorBuffer::m_data[i].offset)->i;
                    }

                }
            }

            static inline void update(unsigned int *pData, unsigned int i)
            {

                if( i < VIRTUALRAWSENSOR_BUFFER)

                {
                    (VirtualRawSensorBuffer::m_data[i].pValue)=(VirtualSensor::SensorData *)pData;

                    if(VirtualRawSensorBuffer::m_data[i].bIsSet == true)
                    {
                        (VirtualRawSensorBuffer::m_data[i].pValue)->ui=
                            (&VirtualRawSensorBuffer::m_data[i].value)->ui;
                    }
                    else if(VirtualRawSensorBuffer::m_data[i].bIsAdd == true)
                    {
                        (VirtualRawSensorBuffer::m_data[i].pValue)->ui+=
                            (&VirtualRawSensorBuffer::m_data[i].offset)->i;
                    }
                }
            }



    };

}

#endif
