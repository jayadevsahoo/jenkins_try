#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: ProxTest -- Perform PROX system test
//---------------------------------------------------------------------
//@ Interface-Description
//
//  The purpose of this class is to provide the SST Tests for PROX board.
//	If a PROX Board is not installed, the test will exit immediately 
//  indicating a "not installed" status. If a PROX Board is installed, 
//  various tests will be performed in a proper sequence.
//---------------------------------------------------------------------
//@ Rationale
//  This test ensures that the PROX Board, if installed, is working properly.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//  The public method runTest() is provided for the ProxFlow test.
//---------------------------------------------------------------------
//@ Fault-Handling
//  n/a
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/ProxTest.ccv   25.0.4.0   19 Nov 2013 14:23:40   pvcs  $
//
//@ Modification-Log 
//
//  Revision: 026  By: rhj    Date: 12-Apr-2011     SCR Number: 6760
//  Project:  PROX
//  Description:
//      Added code in sstTest() method to handle clear key press
//      which indicates the user has skipped the prox test.
// 
//  Revision: 025  By: rhj    Date: 31-Mar-2011     SCR Number: 6436
//  Project:  PROX
//  Description:
//      Added code to ensure that PProxiInterface is not null.
// 
//  Revision: 024   By: rhj   Date: 31-Mar-2011    SCR Number: 6436
//  Project:  PROX
//  Description:
//      Added return FALSE when the user presses the clear button
//     in the doSensorTest_() method.
//  
//  Revision: 023   By: rhj   Date: 14-Mar-2011    SCR Number: 6754 
//  Project:  PROX
//  Description:
//      Added a task delay of two seconds to allow 
//      SmManager.sendOperatorExitToServiceData() complete before
//      exiting SST.
// 
//  Revision: 022   By: rhj   Date: 16-Feb-2011    SCR Number: 6745 
//  Project:  PROX
//  Description:
//      Added an atmPressure ratio calculation to the prox's flow 
//      under the waitForProxStableFlow_().
// 
//  Revision: 021   By: rhj   Date: 17-Jan-2011    SCR Number: 6735
//  Project:  PROX
//  Description:
//      Increased the time out in waitForProxStableFlow_ method 
//      from 500ms to 1500ms.
//  
//  Revision: 020   By: rhj   Date: 13-Sept-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//      Added flow results during leak test.  
//  
//  Revision: 019   By: rpr   Date: 09-Sept-2010  SCR Number: 6436
//  Project:  PROX
//  Description:
//		At the end of the autozero and leak test, the flow is checked to
//		be non zero.
//
//  Revision: 018   By: rhj   Date: 29-July-2010  SCR Number: 6611
//  Project:  PROX
//  Description:
//		Perform an autozero at the start of the Prox Flow Cross Check 
//
//  Revision: 017   By: rhj   Date: 08-July-2010  SCR Number: 6598
//  Project:  PROX
//  Description:
//		Changed the prox prompt from ACCEPT_YES_AND_CLEAR_NO_ONLY to
//      IF_CONNECTED_ACCEPT_YES_AND_CLEAR_NO_ONLY 
//
//  Revision: 016  By: mnr   Date: 15-Jun-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//      AutoZeroAndLeak test made more robust.
// 
//  Revision: 015  By: rhj   Date: 11-Jun-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//      Increase prox flow cross check tolerances and compressor bands 
//      when using a compressor as the source of air.
//  
//  Revision: 014  By: mnr   Date: 10-Jun-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//      Code review III action items related updates.
//  
//  Revision: 013  By: mnr   Date: 09-Jun-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//      IE transition delays reduced.	   
//    
//  Revision: 012  By: mnr   Date: 08-Jun-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//	    Updated doAutoZeroAndLeakTest_() to fix issues due to auto purge.
//
//  Revision: 011  By: mnr   Date: 07-Jun-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//	Delay increased at the end of doAutoZeroAndLeakTest_().
//
//  Revision: 010  By: mnr   Date: 03-Jun-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//		Updates acc. to code review action items.
//   
//  Revision: 009  By: mnr   Date: 26-May-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//		Purge suspend and resumes streamlined to avoid Auto Zero failures.
//   
//  Revision: 008  By: mnr   Date: 25-May-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//		SCR 93, 95 fixes.
//   
//  Revision: 007   By: mnr   Date: 04-May-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//		Updates to reuse data display enums to conserve NOVRAM.
//      AutoZeroAndLeak test and Pressure cross check test updated.
//
//  Revision: 006   By: mnr   Date: 03-May-2010    	SCR Number: 6436
//  Project:  PROX
//  Description:
//		AutozeroAndLeak and Pressure Cross check test related updates.
//	
//  Revision: 005  By:  mnr    Date:  28-Apr-2010   SCR Number: 6436
//  Project:  PROX
//  Description:
//        enums fixed, pressure units fixed and other minor updates.
//	
//  Revision: 004  By:  mnr    Date:  20-Apr-2010  SCR Number: 6436
//  Project:  PROX
//  Description:
//        doAutoZeroAndLeakTest_() stubbed back, delays reduced, removed.
//	
//  Revision: 003  By:  mnr    Date:  16-Apr-2010   SCR Number: 6436
//  Project:  PROX
//  Description:
//        doAutoZeroAndLeakTest_() first pass. 2 sec. delay for display
//		  of test result upper screen for each test and other updates.
//
//  Revision: 002  By:  mnr    Date:  13-Apr-2010   SCR Number: 6436
//  Project:  PROX
//  Description:
//        More functionality added.
//
//  Revision: 001  By:  mnr    Date:  24-Mar-2010   SCR Number: 6436
//  Project:  PROX
//  Description:
//        Initial check in.
//
//=====================================================================

#include "ProxTest.hh"

//@ Usage-Classes

#include "SmRefs.hh"
#include "MainSensorRefs.hh"
#include "SmManager.hh"
#include "SmUtilities.hh"
#include "SmPressureController.hh"
#include "SmExhValveController.hh"
#include "PressureSensor.hh"

#include "Task.hh"
#include "OsTimeStamp.hh"
#include "BitAccessGpio.hh"

#include "HumidTypeValue.hh"
#include "PhasedInContextHandle.hh"
#include "ProxiInterface.hh"
#include "MiscSensorRefs.hh"
#include "MathUtilities.hh"
#include "LinearSensor.hh"
#include "SmConstants.hh"
#include "SmGasSupply.hh"
#include "SmFlowController.hh"
#include "Compressor.hh"
#include "VentObjectRefs.hh"
#include "BD_IO_Devices.hh"
#include "NovRamManager.hh"
#include "ProxManager.hh"
#include "BreathMiscRefs.hh"
#include "VentObjectRefs.hh"
#include "BinaryIndicator.hh"
#include "RegisterRefs.hh"
#include "Barometer.hh"

//@ End-Usage

//@ Code...
extern ProxiInterface* PProxiInterface;

//Constants
#define PX_PRESSURE_TOLERANCE_PSI      1.0 
#define PX_PRESSURE_TOLERANCE_CMH20    1.0 

#define PX_LOW_TEST_PRESSURE 	10.0  //cmH2O
#define PX_HIGH_TEST_PRESSURE 	50.0  //cmH2O

// used to distinguish zero flow
static const Real32 PROX_MAX_INSP_FLOW_AT_PSOL_OFF = 0.15f;

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: ProxTest
//
//@ Interface-Description
//  	Constructor.
//-----------------------------------------------------------------------
//@ Implementation-Description
//-----------------------------------------------------------------------
//@ PreCondition
//-----------------------------------------------------------------------
//@ PostCondition
//@ End-Method
//=======================================================================
ProxTest::ProxTest( void) 
	: proxCurrentSubTestIndex_( PX_TEST_NOT_START_ID)
{ 
   
	Uint8 pxIndex = 0;

	proxPurgeTests_[pxIndex].pressSmDataId = SmDataId::PX_DATA_ITEM_2;
	proxPurgeTests_[pxIndex].minPressSmDataId = SmDataId::PX_DATA_ITEM_3;
	//ensure accumatorPressure is > 1.0
	proxPurgeTests_[pxIndex].accumatorPressure =  3.0f;
	proxPurgeTests_[pxIndex++].expectedMinPress =  1.5f;

	proxPurgeTests_[pxIndex].pressSmDataId = SmDataId::PX_DATA_ITEM_4;
	proxPurgeTests_[pxIndex].minPressSmDataId = SmDataId::PX_DATA_ITEM_5;
	proxPurgeTests_[pxIndex].accumatorPressure =  6.0f;
	proxPurgeTests_[pxIndex].expectedMinPress =  3.0f;

	pxIndex  = 0;
	proxFlowCrossTests_[pxIndex].ventflowSmDataId  =  SmDataId::PX_DATA_ITEM_1;
	proxFlowCrossTests_[pxIndex].proxflowSmDataId  =  SmDataId::PX_DATA_ITEM_2;
	proxFlowCrossTests_[pxIndex].tolerances  =  1.0f;   
	proxFlowCrossTests_[pxIndex++].flowValue = 20.0f;
	proxFlowCrossTests_[pxIndex].ventflowSmDataId  =  SmDataId::PX_DATA_ITEM_3;
	proxFlowCrossTests_[pxIndex].proxflowSmDataId  =  SmDataId::PX_DATA_ITEM_4;
	proxFlowCrossTests_[pxIndex].tolerances  =  0.50f;	
	proxFlowCrossTests_[pxIndex++].flowValue = 5.0f;
	proxFlowCrossTests_[pxIndex].ventflowSmDataId  =  SmDataId::PX_DATA_ITEM_5;
	proxFlowCrossTests_[pxIndex].proxflowSmDataId  =  SmDataId::PX_DATA_ITEM_6;
	proxFlowCrossTests_[pxIndex].tolerances  =  0.15f;
	proxFlowCrossTests_[pxIndex++].flowValue = 1.0f;

	isProxTestEnabled_ = FALSE;
	skipSensorTest_ = FALSE;
}

// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: ~ProxTest
//
//@ Interface-Description
//-----------------------------------------------------------------------
//@ Implementation-Description
//-----------------------------------------------------------------------
//@ PreCondition
//  none
//-----------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=======================================================================
ProxTest::~ProxTest( void)
{

}

// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: sstTest
//
//@ Interface-Description
//
//	This method performs the ProxFlow SST test. No arguments, no return value.
//-----------------------------------------------------------------------
//@ Implementation-Description
//	$[09188]
//	Phase 1: 
//-----------------------------------------------------------------------
//@ PreCondition
//  none
//-----------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=======================================================================
void
ProxTest::sstTest( void)
{
	static Boolean IsPressureTestFailed = FALSE;
	static Boolean IsFlowTestFailed = FALSE;


    if (PProxiInterface)
    {
        ProxiInterface& rProxiInterface = *PProxiInterface;
    
        // if not, send "NOT INSTALLED" test result to the GUI and exit test.
    	if( !rProxiInterface.isProxBoardInstalled() )
    	{
    		RSmManager.sendOptionNotInstalledToServiceData();
        }
    	else 
    	{
    		if (skipSensorTest_)
    		{
    			skipSensorTest_ = FALSE;
    		}
    		else
    		{
    			if (!doSensorTest_())
                {
                    // If true, the user has press the exit button
                    // otherwise the user has press the clear key
                    // which on the next statement exit with a condition
                    // 10 (Operator skip the prox test).
                    if (isProxTestEnabled_)
                    {
                        // Allow two seconds for
                        // RSmManager.sendOperatorExitToServiceData() 
                        // to complete before exiting SST.
                        Task::Delay(2);
                        return;
                    }
                }
    		}
    
    
    		if (!isProxTestEnabled_)
    		{
    			RSmManager.sendStatusToServiceData( SmStatusId::TEST_ALERT,
    												SmStatusId::CONDITION_10 );
    			Task::Delay(2);
    			return;
    		}
    		else if (IsPressureTestFailed)
    		{
    			IsPressureTestFailed = FALSE;
    
    			if (!RSmUtilities.messagePrompt(SmPromptId::ATTACH_CIRCUIT_PROMPT))
    			{
    				RSmManager.sendOperatorExitToServiceData();
    				Task::Delay(2);
    				return;
    			}
    
    		}
    		else if (IsFlowTestFailed)
    		{
                IsFlowTestFailed = FALSE;
    			if (!RSmUtilities.messagePrompt(SmPromptId::UNBLOCK_EXP_PORT_PROMPT))
    			{
    				RSmManager.sendOperatorExitToServiceData();
    				Task::Delay(2);
    				return;
    			}
    			else if (!RSmUtilities.messagePrompt(SmPromptId::CONCT_TUBE_BFR_EXH_FLTR_PROMPT))
    			{
    				RSmManager.sendOperatorExitToServiceData();
    				Task::Delay(2);
    				return;
    			}
    
    		}
    
    		// Basic communication test
    		if( !doCommTest_() )
    		{
    			RSmManager.sendStatusToServiceData( SmStatusId::TEST_ALERT,
    											    SmStatusId::CONDITION_1 );
    		}
    		// Test 1: Demo Mode Square Wave Test
    		else if (!doDemoModeSquareWaveTest_())
    		{
    			RSmManager.sendStatusToServiceData( SmStatusId::TEST_ALERT,
    												SmStatusId::CONDITION_3 );
    		}
    
    		// Test 2: Barometric Pressure Cross Check Test
    		else if( !doBarometricPressureTest_() )
    		{
                RSmManager.sendStatusToServiceData( SmStatusId::TEST_ALERT,
    												SmStatusId::CONDITION_4 );
    		}
    		// Test 3: Auto-Zero and Leak Test
    		else if( !doAutoZeroAndLeakTest_() )
    		{
                RSmManager.sendStatusToServiceData( SmStatusId::TEST_ALERT,
    												SmStatusId::CONDITION_5 );
    		}
    		// Test 4: Purge Test
    		else if( !doPurgeTest_() )
    		{
                RSmManager.sendStatusToServiceData( SmStatusId::TEST_ALERT,
    												SmStatusId::CONDITION_6 );
    		}
    		// Test 5: Pressure Cross Check Test
    		else if( !doPressureCrossCheckTest_() )
    		{
                RSmManager.sendStatusToServiceData( SmStatusId::TEST_ALERT,
    												SmStatusId::CONDITION_7 );
    			IsPressureTestFailed = TRUE;
    
    		}
    		// Test 6: Flow Sensor Cross Check Test
    		else if( !doFlowSensorCrossCheckTest_() )
    		{
                RSmManager.sendStatusToServiceData( SmStatusId::TEST_ALERT,
    												SmStatusId::CONDITION_8 );
    			IsFlowTestFailed = TRUE;
    		}
    		else if (!RSmUtilities.messagePrompt(SmPromptId::UNBLOCK_EXP_PORT_PROMPT))
    		{
    			RSmManager.sendOperatorExitToServiceData();
    		}
    		else if (!RSmUtilities.messagePrompt(SmPromptId::CONCT_TUBE_BFR_EXH_FLTR_PROMPT))
    		{
    			RSmManager.sendOperatorExitToServiceData();
    		}
    		else
    		{
    			// Completed SST - Update the prox firmware and serial number.
    			NovRamManager::UpdateLastSstProxFirmwareRev( RProxManager.getFirmwareRev() );
    			NovRamManager::UpdateLastSstProxSerialNum( rProxiInterface.getBoardSerialNumber() );
    		}
    
    		// Suspend a purge in case the user
    		// restart SST.
            rProxiInterface.suspendPurge();
    		// also set the purge interval back to default
    		const Uint32 DEFAULT_PURGE_INTERVAL = 180; //in seconds = 3mins.
    		rProxiInterface.updatePurgeInterval(DEFAULT_PURGE_INTERVAL);
    
    
    	} // end "NOT INSTALLED" else
    }

	// Delay to ensure the vent sent out the message before
	// sending the SST TEST end message.
	Task::Delay(2);

}


//=====================================================================
//
//  Private Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  doCommTest_
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
Boolean ProxTest::doCommTest_(void)
{
	ProxiInterface& rProxiInterface = *PProxiInterface;

    //check whether OEM Id is "COVIDIEN"
	return rProxiInterface.isCommunicationUp();
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  doDemoModeSquareWaveTest_
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
Boolean ProxTest::doDemoModeSquareWaveTest_(void)
{
	Boolean result = TRUE;
// E600 BDIO
#if 0
	const Uint16 MAX_WAIT_TIME = 5;

	ProxiInterface& rProxiInterface = *PProxiInterface;

	rProxiInterface.enableSquareWaveformDemoMode();
	Task::Delay(1);

	Uint8 numCycles = 0;

	// Check for demo mode status.  
	while ( ((Uint8) rProxiInterface.getFlowStatus() != ProxiInterface::DEMO_MODE_ACTIVE) && 
	        (numCycles < MAX_WAIT_TIME)  )
	{
			Task::Delay(1);		
   	    numCycles ++;
	}

	// If demo mode was not achieved with in MAX_WAIT_TIME,
	// generate an error.
	if (numCycles >= MAX_WAIT_TIME)
	{
		result = FALSE;   
	}
	else
	{
		for (numCycles = 0; numCycles < 3; numCycles++)
		{
			if ((IsEquivalent(rProxiInterface.getFlow(), 60.0, ONES) ||
				 IsEquivalent(rProxiInterface.getFlow(), -60.0, ONES)))
		{
				Task::Delay(1, 500);        
		}
		else
		{
			result = FALSE;
			break;
		}

		}
	}

	rProxiInterface.disableSquareWaveformDemoMode();
// E600 BDIO
#endif
	return result;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  doBarometricPressureTest_
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
Boolean ProxTest::doBarometricPressureTest_(void)
{
	Boolean result = TRUE;
    
	proxCurrentSubTestIndex_ =  ProxTest::PX_BAROMETRIC_PRESS_CC_TEST_ID;
    RSmManager.sendTestPointToServiceData(SmDataId::PX_CURRENT_SUB_TEST_ID,(Real32) proxCurrentSubTestIndex_ );

	ProxiInterface& rProxiInterface = *PProxiInterface;

	// Get 840 Barometric Pressure readings. mmHG
    Real32 atmPressure = RAtmosphericPressureSensor.getValue() / CMH2O_PER_MMHG;

	// Get Prox Barometric Pressure readings. mmHG
    Real32 proxAtmPressure = rProxiInterface.getBarometricPressure();
    
	// send readings to the GUI for initial display
	RSmManager.sendTestPointToServiceData(SmDataId::PX_DATA_ITEM_1, atmPressure);
	Task::Delay(1);
	RSmManager.sendTestPointToServiceData(SmDataId::PX_DATA_ITEM_2, proxAtmPressure);

	// Verify the difference of the two sensors
	if (ABS_VALUE(atmPressure - proxAtmPressure) > (atmPressure * 0.10))
	{
        result = FALSE;
	}

	//allow results to be displayed for at least 2 sec.
	Task::Delay(2);

	return result;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  doAutoZeroAndLeakTest_
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
Boolean ProxTest::doAutoZeroAndLeakTest_(void)
{
	Uint16 numCycles = 0;

	proxCurrentSubTestIndex_ = ProxTest::PX_AUTO_ZERO_LEAK_TEST_ID;
    RSmManager.sendTestPointToServiceData(SmDataId::PX_CURRENT_SUB_TEST_ID,(Real32) proxCurrentSubTestIndex_ );
	
	Boolean testPassed = TRUE;

    ProxiInterface& rProxiInterface = *PProxiInterface;

	const Uint32 MAX_PURGE_INTERVAL = 600; //in seconds = 10mins.
	rProxiInterface.updatePurgeInterval(MAX_PURGE_INTERVAL);

	// pair to the suspendPurge() call from LeakTest::sstTest()
	// NOTE: Purging, dumping the accum. and autozeroing all 
	// 	     require the "purge and autozero" to be active, 
	// 		 those cannot be executed in a suspended state
	rProxiInterface.resumePurge();
	Task::Delay(3);

	const Uint16 MAX_CYCLE_COUNT = 8; //allow 15 secs for worst case scenario

	// While the purge and autozero are suspended the MM still
	// keeps the timer rolling. So a purge or autozero can be due while 
	// they were suspended. Once the resumePurge() call above is executed
	// the MM might be in a state where purging and/or auto-zeroing is 
	// necessary.
	numCycles = 0; //keep this around in case the code block is moved
    while ( !rProxiInterface.isIdleModeActive() &&  numCycles < MAX_CYCLE_COUNT )
	{
		simIETransition_(200);
		Task::Delay(2);
		numCycles++;
	}

	if( numCycles >= MAX_CYCLE_COUNT )
	{
		return FALSE;
	}
    
	//301 is used to measure 3 secs when the Delay used in a 
	//loop is 10ms. e.g. numCycles < 301 
	const Uint16 MAX_CYCLES = 301;
	const Uint16 TWICE_MAX_CYCLES = 2 * MAX_CYCLES;

    	//dump accum pump off
	rProxiInterface.dumpAccumalator();
	Task::Delay(1);
	simIETransition_(200);
	Task::Delay(1);

	Real32 accumPressure = rProxiInterface.getAccumatorPressure();
	RSmManager.sendTestPointToServiceData(SmDataId::PX_DATA_ITEM_1, accumPressure);

	if (accumPressure > PX_PRESSURE_TOLERANCE_PSI) // psi
	{
		return FALSE;
	}

	// confirm idle status or zero waiting status before starting the autozero below
	numCycles = 0;
	while (    !rProxiInterface.isIdleModeActive() 
	        && !rProxiInterface.isZeroWaiting() 
	        &&  numCycles < TWICE_MAX_CYCLES 
	      )
	{
		Task::Delay(0,10);
		numCycles++;
	}

	if( !rProxiInterface.isZeroWaiting() && numCycles >= TWICE_MAX_CYCLES )
	{
		return FALSE;
	}

    //Auto-zero Prox pressure transducer
	if( !runAutoZeroTest_() )
	{
		return FALSE;
	}

	Real32 proxPressure = rProxiInterface.getPressure();
	RSmManager.sendTestPointToServiceData(SmDataId::PX_DATA_ITEM_2, proxPressure);

	if (proxPressure > PX_PRESSURE_TOLERANCE_CMH20)
	{
		return FALSE;
	}

	//this is also needed for the leak test below, so do it outside of above if statement
	simIETransition_(200);
	Task::Delay(1);

	//ref. [4-070139-00 section 37.3]:
    const Uint16 TARGET_P1_CYCLES = 100;
	const Uint16 TARGET_P2_CYCLES = 300;
	const Real32 TARGET_P1 = 3.0;
	const Real32 TARGET_P2 = 6.0;
	Boolean checkOnePassed = FALSE;
	Boolean checkTwoPassed = FALSE;

	rProxiInterface.enablePurgeTest(6.0);
	Task::Delay(5); //this is necessary
	numCycles = 0;
    while (rProxiInterface.isIdleModeActive() && numCycles < TWICE_MAX_CYCLES )
	{
		Task::Delay(0,10);
		numCycles++;
	}

	numCycles = 0;

	while (!rProxiInterface.isPurgeError() && numCycles < MAX_CYCLES)
	{
		Task::Delay(0,10);
		accumPressure = rProxiInterface.getAccumatorPressure();
	
		RSmManager.sendTestPointToServiceData(SmDataId::PX_DATA_ITEM_3, accumPressure);
	
		//ref. [4-070139-00 section 37.3]: should hit TARGET_P1 in 1 sec
		if ((accumPressure > TARGET_P1) && (numCycles < TARGET_P1_CYCLES))
		{
			checkOnePassed = TRUE;
		}
		//ref. [4-070139-00 section 37.3]: should hit TARGET_P2 in 3 sec
		if ((accumPressure > TARGET_P2) && (numCycles < TARGET_P2_CYCLES))
		{
			checkTwoPassed = TRUE;
			break;
		}

        if (   ( !checkOnePassed && numCycles == TARGET_P1_CYCLES )
			|| ( !checkTwoPassed && numCycles == TARGET_P2_CYCLES )
		   )
		{
			testPassed = FALSE;
		}

		if (!testPassed)
		{
			return FALSE;
		}

		numCycles++;
	}

	Task::Delay(1);
	accumPressure = rProxiInterface.getAccumatorPressure();
	RSmManager.sendTestPointToServiceData(SmDataId::PX_DATA_ITEM_4, accumPressure);

	Task::Delay(1);
	Real32 accumPressure2 = rProxiInterface.getAccumatorPressure();
	RSmManager.sendTestPointToServiceData(SmDataId::PX_DATA_ITEM_5, accumPressure);

	//ref. [4-070139-00 section 37.3]:
	if (accumPressure < 5.0 || accumPressure2 < 5.0)
	{
		return FALSE;
	}

	Task::Delay(3);
	accumPressure = rProxiInterface.getAccumatorPressure();
	RSmManager.sendTestPointToServiceData(SmDataId::PX_DATA_ITEM_6, accumPressure);

	//ref. [4-070139-00 section 37.3]:
	if (accumPressure < 4.0)
	{
		return FALSE;
	}

	numCycles = 0;
	while (!rProxiInterface.isWaitingForPurgeEnable() && numCycles < MAX_CYCLES)
	{
		Task::Delay(0,10);
		numCycles++;
	}

	if( numCycles >= MAX_CYCLES )
	{
		testPassed = FALSE;
	}

	if( testPassed )
	{
        Real32 proxFlow = ABS_VALUE( rProxiInterface.getFlow() );

        RSmManager.sendTestPointToServiceData(SmDataId::PX_DATA_ITEM_7, proxFlow);

        // no flow should be present, fail test if there is some.
        // if the accumulator is leaking it can effect the flow sensor such that
        // the flow sensor will have an offset. 
        // if flow is being measured we will have an offset problem
        if (proxFlow > PROX_MAX_INSP_FLOW_AT_PSOL_OFF)
        {
			testPassed = FALSE;
        }

        //send IE signal
        simIETransition_(200);

		//allow results to be displayed and also allow time
		//to read the idle status
		Task::Delay(4);

		if (!rProxiInterface.isIdleModeActive())
		{
			testPassed = FALSE;
		}
        else
        {
            // read prox flow 
            proxFlow = ABS_VALUE( rProxiInterface.getFlow() );
    
            RSmManager.sendTestPointToServiceData(SmDataId::PX_DATA_ITEM_7, proxFlow);
    
    
            // no flow should be present, fail test if there is some.
            // if the accumulator is leaking it can effect the flow sensor such that
            // the flow sensor will have an offset. 
            // if flow is being measured we will have an offset problem
            if (proxFlow > PROX_MAX_INSP_FLOW_AT_PSOL_OFF)
            {
    			testPassed = FALSE;
            }
            else
            {
                proxPressure = rProxiInterface.getPressure();
                RSmManager.sendTestPointToServiceData(SmDataId::PX_DATA_ITEM_8, proxPressure);

                if (proxPressure > PX_PRESSURE_TOLERANCE_CMH20)
                {
                    testPassed = FALSE;
                }
            }


        }
	}

    //allow results to be displayed for at least 2 sec.
    Task::Delay(2);

	return testPassed;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  doPurgeTest_
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
Boolean ProxTest::doPurgeTest_(void)
{
	proxCurrentSubTestIndex_ = ProxTest::PX_PURGE_TEST_ID;
	RSmManager.sendTestPointToServiceData(SmDataId::PX_CURRENT_SUB_TEST_ID,(Real32) proxCurrentSubTestIndex_ );

	const Uint8 MAX_NUM_TRIES = 2;
	Boolean result = TRUE;

	Uint8 numErrors = 0;

	for (Uint8 numTest = 0; numTest < MAX_PX_PURGE_TESTS_; numTest++)
	{
		for (Uint8 numTries = 0; numTries < MAX_NUM_TRIES; numTries ++)
		{
			// Execute purge test
			if (runPurgeTest_(numTest))
			{
		    	break;
			}
			else
			{
				numErrors ++;
			}
		}

		if (numErrors > 1)
		{
			result = FALSE;
			break;
		}
		else
		{
			numErrors = 0;
		}
	}

	//allow results to be displayed for at least 2 sec.
	Task::Delay(2);

	return result;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  doPressureCrossCheckTest_
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
Boolean ProxTest::doPressureCrossCheckTest_(void)
{
	Boolean testPassed = TRUE;
	proxCurrentSubTestIndex_ = ProxTest::PX_PRESSURE_CC_TEST_ID;
	RSmManager.sendTestPointToServiceData(SmDataId::PX_CURRENT_SUB_TEST_ID,(Real32) proxCurrentSubTestIndex_ );

	ProxiInterface& rProxiInterface = *PProxiInterface;

	// Suspend Purge
	rProxiInterface.suspendPurge();
    Task::Delay(2);

	// Prompt user to remove insp filter
	if (!RSmUtilities.messagePrompt(SmPromptId::PX_REMOVE_INSP_FILTER_PROMPT))
	{
		// graceful exit
		rProxiInterface.resumePurge();
		Task::Delay(2);
		//if operator pressed "Clear", exit SST
		RSmManager.sendOperatorExitToServiceData();
		return FALSE;
	}

	// open the safety valve, then wait 1 sec for pressure to decay
	RSmUtilities.openSafetyValve();
	Task::Delay(1);

	// Resume Purge
	rProxiInterface.resumePurge();
	Task::Delay(2);

	if( !runAutoZeroTest_() )
	{
		// graceful exit on error
		RSmUtilities.closeSafetyValve();
		return FALSE;
	}
	
	Task::Delay(1);
	//verify prox pressure reading
	Real32 proxPressure = rProxiInterface.getPressure();
	RSmManager.sendTestPointToServiceData(SmDataId::PX_DATA_ITEM_1, proxPressure );
	//check value against tolerance
	if (proxPressure > PX_PRESSURE_TOLERANCE_CMH20)
	{
		// graceful exit on error
		RSmUtilities.closeSafetyValve();
		return FALSE;
	}

	// Suspend Purge
	rProxiInterface.suspendPurge();
	Task::Delay(1);

	// close the safety valve
	RSmUtilities.closeSafetyValve();
    
	//close expiratory valve 
	RSmExhValveController.close();

	if (!pressurizeSystemTest_(PX_LOW_TEST_PRESSURE))
	{
        testPassed = FALSE;
	}
	else if (!pressureTest_(PX_LOW_TEST_PRESSURE))
	{
		testPassed = FALSE;
	}
	else if (!pressurizeSystemTest_(PX_HIGH_TEST_PRESSURE))
	{
		testPassed = FALSE;
	}
	else if (!pressureTest_(PX_HIGH_TEST_PRESSURE))
	{
		testPassed = FALSE;
	}

	// Resume Purge
	rProxiInterface.resumePurge();
    Task::Delay(2);


	RSmUtilities.closeSafetyValve();
	RSmExhValveController.open();

	rProxiInterface.dumpAccumalator(FALSE);
	Task::Delay(1);
	simIETransition_(200);
	Task::Delay(1);
	rProxiInterface.suspendPurge();
    
	//allow results to be displayed for at least 2 sec.
	Task::Delay(2);

	return testPassed;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  doFlowSensorCrossCheckTest_
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
Boolean ProxTest::doFlowSensorCrossCheckTest_(void)
{
	Boolean result = TRUE;
	proxCurrentSubTestIndex_ = ProxTest::PX_FLOW_CC_TEST_ID;
	RSmManager.sendTestPointToServiceData(SmDataId::PX_CURRENT_SUB_TEST_ID,(Real32) proxCurrentSubTestIndex_ );

	ProxiInterface& rProxiInterface = *PProxiInterface;
    const Real32 PROX_FLOW_TOLERANCE_FACTOR_WITH_COMPRESSOR = 3.0f;

	if (!RSmGasSupply.isAnyGasPresent())
	{
		// no gas connected
		RSmManager.sendStatusToServiceData(
					SmStatusId::TEST_FAILURE,
					SmStatusId::CONDITION_9);
		result = FALSE;
	}
	else
	{	
		GasType testGas = EITHER; 

		if (RSmGasSupply.isAnyAirPresent())
		{
            testGas = AIR;
			rProxiInterface.setGasParams(testGas);
			Task::Delay(1, 500);
		}
		else if (RSmGasSupply.isO2Present())
		{
            testGas = O2;
			rProxiInterface.setGasParams(testGas);
			Task::Delay(1, 500);

		}
		else
		{
			// Should not get here
			CLASS_ASSERTION_FAILURE();
		}


		if (!RSmUtilities.messagePrompt(SmPromptId::ATTACH_CIRCUIT_PROMPT))
		{
			//if operator pressed "Clear", exit SST
			RSmManager.sendOperatorExitToServiceData();
			result = FALSE;
		}	
		else if (!RSmUtilities.messagePrompt(SmPromptId::PX_UNBLOCK_SENSOR_OUTPUT_PROMPT))
		{
			//if operator pressed "Clear", exit SST
			RSmManager.sendOperatorExitToServiceData();
			result = FALSE;
		}
		else if (!RSmUtilities.messagePrompt(SmPromptId::DISCNT_TUBE_BFR_EXH_FLTR_PROMPT))
		{
			//if operator pressed "Clear", exit SST
			RSmManager.sendOperatorExitToServiceData();
			result = FALSE;
		}
		else if (!RSmUtilities.messagePrompt(SmPromptId::BLOCK_EXP_PORT_PROMPT))
		{
			//if operator pressed "Clear", exit SST
			RSmManager.sendOperatorExitToServiceData();
			result = FALSE;
		}		
		else
		{

			rProxiInterface.resumePurge();
			Task::Delay(3);

			//Auto-zero Prox pressure transducer
			if( !runAutoZeroTest_() )
			{
				result = FALSE;
			}
			else
			{
				rProxiInterface.suspendPurge();
				Task::Delay(3);
			
				// flush system with air for good exh flow sensor readings
				RSmUtilities.flushTubing( testGas, STATE_SST);
	
				// if using compressor air, wait for compressor to recharge
				if ( !RSmGasSupply.isWallAirPresent()
					 && RCompressor.checkCompressorInstalled() )
				{
					Task::Delay( 0, COMPRESSOR_FILL_TIME);
	
				}

	
				// perform the flow cross-checks and psol current checks
				for ( Uint8 testNum = 0;
					testNum < MAX_PX_FLOW_CROSS_TESTS_ && result;
					  testNum++ )
				{
						// Set test gas to the test flow
						RSmFlowController.establishFlow( proxFlowCrossTests_[testNum].flowValue,
														 DELIVERY, testGas);
						// remote testing setup delay
						Task::Delay(1, 500);
						RSmManager.signalTestPoint();
	
						// delay for alpha-filtering of flow values
						Task::Delay( 0, 300);
	
						// check if flow is stable
						if (!RSmFlowController.waitForStableFlow(500))
						{
							result = FALSE;
	
						}
						else if (!waitForProxStableFlow_(proxFlowCrossTests_[testNum].flowValue , 1500))
						{
							result = FALSE;
						}
						else
						{
	
							// Read 840's atm pressure in cmH20
							Real32 atmPressure = RAtmosphericPressureSensor.getValue();
		
							// Stable flow was reached 
							// read alpha-filtered insp flow
							 
							Real32 inspFlow = 0.0f;
							if (testGas == AIR)
							{
								inspFlow = RAirFlowSensor.getFilteredValue();
							}
							else if (testGas == O2)
							{
								inspFlow = RO2FlowSensor.getFilteredValue();
	
							}
							else
							{
								// Should not get here
								AUX_CLASS_ASSERTION_FAILURE(testGas);
							}
		
								// read prox flow 
							Real32 proxFlow = rProxiInterface.getFlow() * (atmPressure /STD_ATM_PRESSURE) ;
		
								// send data to the gui
							RSmManager.sendTestPointToServiceData( proxFlowCrossTests_[testNum].ventflowSmDataId,inspFlow );
							RSmManager.sendTestPointToServiceData( proxFlowCrossTests_[testNum].proxflowSmDataId,proxFlow );
	
	
							Real32 tolerances = proxFlowCrossTests_[testNum].tolerances;
	
							// If compressor is present, increase the tolerance by a factor of 
							if ((RSmGasSupply.isCompressorAirPresent()) && (!RSmGasSupply.isWallAirPresent()))
							{
								tolerances = tolerances * PROX_FLOW_TOLERANCE_FACTOR_WITH_COMPRESSOR;
							}
							
							if (ABS_VALUE(inspFlow - proxFlow) > tolerances)
							{
								 result = FALSE;
							}
	
	
						}
						// end else
				}
				// end for
	
				// turn off gas
				RSmFlowController.stopFlowController();

			}
			  // 0 lpm insp flow check
			if (result)				
			{
			// $[TI3.2]
				// ***** 0 lpm test point only *****

				// Allow time for flow to decay for accurate
				Task::Delay(2);

				// Read insp flow with psol off
				Real32 proxFlow = rProxiInterface.getFlow();

				RSmManager.sendTestPointToServiceData( SmDataId::PX_DATA_ITEM_7, proxFlow);

				if ( proxFlow > PROX_MAX_INSP_FLOW_AT_PSOL_OFF )
				{
				// $[TI3.2.1]
					// 0 lpm check failed
					result = FALSE;
				}
			}

			
		}
	}
	// end else

	//allow results to be displayed for at least 2 sec.
	Task::Delay(2);

	return result;
}

// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: pressurizeSystemTest_
//
//@ Interface-Description
//  This method tests if system can be pressurized within a specified
//	timeout period.  Target pressure is passed in as an argument.
//	Returns TRUE if system can pressurize, FALSE if not.
//-----------------------------------------------------------------------
//@ Implementation-Description
//	Assumption: exh valve and safety valve both closed.
//
//	Calculate the pressurization timeout.
//  Pressurize the system to the passed-in target pressure value.
//	If the system cannot pressurize within the calculated timeout, then
//	return FALSE, else return TRUE.
////-----------------------------------------------------------------------
//@ PreCondition
//  none
//-----------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=======================================================================
Boolean ProxTest::pressurizeSystemTest_ (Real32 targetPressure)
{
	Boolean testPassed;
	Uint32 timeoutMs;
	Real32 flowRate;
	ProxiInterface& rProxiInterface = *PProxiInterface;

    flowRate = 5.0;
	timeoutMs = RSmUtilities.pressureTimeout( STATE_SST, targetPressure,
											  flowRate );
	
	RSmPressureController.establishPressure(
							targetPressure, DELIVERY, EITHER, flowRate);

	if (RSmPressureController.waitForPressure(timeoutMs, FALSE))
	{
		testPassed = TRUE;
		// NOTE: if pressurization passes, 840 insp and prox pressure readings
		// are sent to the GUI in pressureTest_()
  	}
	else
	{
		testPassed = FALSE;
		// If pressurization fails, send the current 840 insp and prox pressures
		// to the GUI
		if (targetPressure == PX_LOW_TEST_PRESSURE)
		{
			RSmManager.sendTestPointToServiceData(SmDataId::PX_DATA_ITEM_2, 
											  RInspPressureSensor.getValue());
			RSmManager.sendTestPointToServiceData(SmDataId::PX_DATA_ITEM_3, 
												  rProxiInterface.getPressure());
		}
		else if (targetPressure == PX_HIGH_TEST_PRESSURE)
		{
			RSmManager.sendTestPointToServiceData(SmDataId::PX_DATA_ITEM_4, 
												  RInspPressureSensor.getValue());
			RSmManager.sendTestPointToServiceData(SmDataId::PX_DATA_ITEM_5, 
											  rProxiInterface.getPressure());
	}
	}

  	RSmPressureController.stopPressureController();

// keep safety and exh valves closed for pressureTest_()

	return( testPassed);
}

// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: pressureTest_
//
//@ Interface-Description
//	This method compares pressure readings of the insp and PROX pressure
//	sensors.  No arguments, returns TRUE if pressure difference is within
//	spec, else returns FALSE.
//-----------------------------------------------------------------------
//@ Implementation-Description
//	Assumption: system still pressurized from pressurizeSystemTest_().
//
//	Read insp and PROX pressure sensors.
//	If the difference is within spec, return TRUE, else return FALSE.
//-----------------------------------------------------------------------
//@ PreCondition
//  none
//-----------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=======================================================================
Boolean ProxTest::pressureTest_(Real32 targetPressure)
{
	Boolean testPassed;
	ProxiInterface& rProxiInterface = *PProxiInterface;

	// wait for pressure to settle
    Task::Delay (0, 200);
	
	Real32 inspPressure = RInspPressureSensor.getValue();
	Real32 proxPressure = rProxiInterface.getPressure();

	if (targetPressure == PX_LOW_TEST_PRESSURE)
	{
		RSmManager.sendTestPointToServiceData(SmDataId::PX_DATA_ITEM_2, 
											  RInspPressureSensor.getValue());
		RSmManager.sendTestPointToServiceData(SmDataId::PX_DATA_ITEM_3, 
											  rProxiInterface.getPressure());
	}
	else if (targetPressure == PX_HIGH_TEST_PRESSURE)
	{
		RSmManager.sendTestPointToServiceData(SmDataId::PX_DATA_ITEM_4, 
											  RInspPressureSensor.getValue());
		RSmManager.sendTestPointToServiceData(SmDataId::PX_DATA_ITEM_5, 
											  rProxiInterface.getPressure());
	}

	if ( ABS_VALUE(inspPressure - proxPressure) >
					(inspPressure + proxPressure) * REL_PRESSURE_ERROR +
												2 * ABS_PRESSURE_ERROR )
	{
		testPassed = FALSE;
	}
	else
	{
		testPassed = TRUE;
	}

	Task::Delay(1);
	return( testPassed);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  runAutoZeroTest_
//
//@ Interface-Description:
// This method can be called to perform Auto Zero. It is assumed that
// purge is NOT suspended.
//---------------------------------------------------------------------
//@ Implementation-Description
// Interact with the ProxiInterface object reference to request AutoZero
// and check the status to send I:E signals at appropriate intervals and 
// finally check for success or error.
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
Boolean ProxTest::runAutoZeroTest_(void)
{
	Boolean testPassed = TRUE;
	
	const Uint32  MAX_CYCLES = 1000;
	Uint32 numCycles = 0;
	
	ProxiInterface& rProxiInterface = *PProxiInterface;
	
	rProxiInterface.performZero();
	
	while( !rProxiInterface.isZeroWaiting() && numCycles < MAX_CYCLES )
	{
		Task::Delay(0,10);
		numCycles++;
	}
	
	if( numCycles >= MAX_CYCLES )
	{
		//exceeded time for reaching zero wait state
		testPassed = FALSE;
	}

	if( testPassed )
	{
		//send IE signal
		simIETransition_(200);
	
		numCycles = 0;
		while( ( rProxiInterface.isZeroActive() || rProxiInterface.isZeroWaiting() )
			   && numCycles < MAX_CYCLES )
		{
			Task::Delay(0,10);
			numCycles++;
		}
	
		if( numCycles >= MAX_CYCLES || !rProxiInterface.isIdleModeActive() )
		{
			testPassed = FALSE;
		}
	}
	
	return testPassed;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  runPurgeTest_
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

Boolean ProxTest::runPurgeTest_(Uint8 testNum)
{
	Boolean result = TRUE;

	AUX_CLASS_PRE_CONDITION( testNum < MAX_PX_PURGE_TESTS_, testNum)

	ProxiInterface& rProxiInterface = *PProxiInterface;
	rProxiInterface.dumpAccumalator(FALSE);


	const Uint16 MAX_WAIT_TIME = 400;
    Uint8 numCycles = 0;

	// Wait for a custom purge waiting bit enabled.
	// If the bit was not set within MAX_WAIT_TIME,
	// report an error.
	while ( (!rProxiInterface.isWaitingForPurgeEnable()) && 
			  (numCycles < MAX_WAIT_TIME) ) 
    {
		numCycles ++;
		Task::Delay(0,10);
    }

    if (numCycles >= MAX_WAIT_TIME)
	{
	    result = FALSE;
	}

    //send IE signal
    simIETransition_(200);


	if (result)
	{

        Real32 accumatorPress = rProxiInterface.getAccumatorPressure();

        RSmManager.sendTestPointToServiceData(SmDataId::PX_DATA_ITEM_1, accumatorPress);

        if (accumatorPress > PX_PRESSURE_TOLERANCE_PSI)
		{
			result = FALSE;
		}
		else
		{
			Real32 maxAccumatorPressure = 0.0f;
			Real32 minAccumatorPressure = 0.0f;
			Real32 accumatorPressure = 0.0f;
	
			//ensure accumatorPressure is > 1.0
			rProxiInterface.enablePurgeTest(proxPurgeTests_[testNum].accumatorPressure);
	
			Task::Delay(1);
// E600 BDIO		
			RBitAccessGpio.requestBitUpdate( DOUT_IE_PHASE_OUT,  BitAccessGpio::ON);

           	const Uint16 TWICE_MAX_WAIT_TIME = (MAX_WAIT_TIME * 2);

			numCycles = 0;
			//wait long enough to allow rProxiInterface to send out
			//the purge request in time i.e. MAX_WAIT_TIME * 2
			while ( (!rProxiInterface.isWaitingForPurgeEnable()) && 
					  (numCycles < TWICE_MAX_WAIT_TIME ) ) 
			{
				numCycles ++;
				accumatorPressure  = rProxiInterface.getAccumatorPressure();
				if (maxAccumatorPressure < accumatorPressure)
				{
					maxAccumatorPressure = accumatorPressure;
				}
				Task::Delay(0,10);
			}
	
			RSmManager.sendTestPointToServiceData(proxPurgeTests_[testNum].pressSmDataId , maxAccumatorPressure);
			minAccumatorPressure = maxAccumatorPressure;
	
			// SCR 96: The accumator pressure must be at least greater than one
			// and the waiting for purge enable should not exceed 
			// 4 secs
			if (numCycles >= MAX_WAIT_TIME || maxAccumatorPressure <= PX_PRESSURE_TOLERANCE_PSI)
			{
				result = FALSE;
			}
			else
			{
	
// E600 BDIO				
				RBitAccessGpio.requestBitUpdate( DOUT_IE_PHASE_OUT,  BitAccessGpio::OFF);
	
				numCycles = 0;
				while ( (!rProxiInterface.isIdleModeActive()) && 
						  (numCycles < MAX_WAIT_TIME ) ) 
				{
					numCycles ++;
					accumatorPressure  = rProxiInterface.getAccumatorPressure();
	
					if (accumatorPressure < minAccumatorPressure)
					{
						minAccumatorPressure = accumatorPressure;
					}
					Task::Delay(0,10);
				}
	
				RSmManager.sendTestPointToServiceData(proxPurgeTests_[testNum].minPressSmDataId , minAccumatorPressure);

				// Report an error if prox idle mode is not active
				// or if the minimum accumator pressure is greater than
				// the expected minimum pressure.
				if ((numCycles >= MAX_WAIT_TIME) || 
					(proxPurgeTests_[testNum].expectedMinPress  <= minAccumatorPressure))
				{
					result = FALSE;
				}
			}
		}

	}
	return result;

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  simIETransition_
//
//@ Interface-Description:
// This method can be called to simulate the I to E transition.
//---------------------------------------------------------------------
//@ Implementation-Description
// Go from E to I to E by updating the correct bit on RWriteIOPort.
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
void ProxTest::simIETransition_(Uint32 delayInMs)
{
	//simulate I to E transition
// E600 BDIO	
	RBitAccessGpio.requestBitUpdate( DOUT_IE_PHASE_OUT,  BitAccessGpio::OFF);
	Task::Delay(0,delayInMs);

// E600 BDIO	
	RBitAccessGpio.requestBitUpdate( DOUT_IE_PHASE_OUT,  BitAccessGpio::ON);
	Task::Delay(0,delayInMs);

// E600 BDIO	
	RBitAccessGpio.requestBitUpdate( DOUT_IE_PHASE_OUT,  BitAccessGpio::OFF);
	Task::Delay(0,delayInMs);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  doSensorTest_
//
//@ Interface-Description:
//  This method has no argument.  It returns TRUE the prox sensor is 
//  installed.  Otherwise it returns FALSE, and also skips the Prox
//  test.
//---------------------------------------------------------------------
//@ Implementation-Description
// 
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
Boolean ProxTest::doSensorTest_(void)
{

	Boolean result = TRUE;
	ProxiInterface& rProxiInterface = *PProxiInterface;

	// prompt operator 
	RSmManager.promptOperator(SmPromptId::CONNECT_PROX_SENSOR_PROMPT,
	SmPromptId::IF_CONNECTED_ACCEPT_YES_AND_CLEAR_NO_ONLY);

	// get operator keypress and process it
	SmPromptId::ActionId operatorAction =
	RSmManager.getOperatorAction();

	if (operatorAction == SmPromptId::KEY_ACCEPT)
	{

		Boolean clearKeyHasPressed = FALSE;

		//get PROX sensor type 
		Mercury::SensorType sensorType = rProxiInterface.getSensorType();

		//check for correct sensor type
		while( sensorType != Mercury::NEONATAL_FLOW_SENSOR )
		{
			// prompt operator 
			//prompt to install the sensor ONLY if it is unplugged
			RSmManager.promptOperator(SmPromptId::PROX_SENSOR_PROMPT,
			SmPromptId::IF_CONNECTED_ACCEPT_YES_AND_CLEAR_NO_ONLY);

			// get operator keypress and process it
			SmPromptId::ActionId operatorAction =
			RSmManager.getOperatorAction();

			if (operatorAction == SmPromptId::KEY_ACCEPT)
			{

				Task::Delay(2);
				//now update the sensor type
				sensorType = rProxiInterface.getSensorType();

			}
			else if (operatorAction == SmPromptId::USER_EXIT)
			{
				RSmManager.sendOperatorExitToServiceData();
				result = FALSE;
				break;
			}
			else
			{
				 // Clear key
				isProxTestEnabled_ = FALSE;
				clearKeyHasPressed = TRUE;
				break;
			}

		}

		if (result &&  !clearKeyHasPressed)
		{
			if (!RSmUtilities.messagePrompt(SmPromptId::PX_BLOCK_SENSOR_OUTPUT_PROMPT))
			{
				// EXIT button hit
				RSmManager.sendOperatorExitToServiceData();
				result = FALSE;
			}
			else
			{
				isProxTestEnabled_ = TRUE;
			}
		}


	}
	else if (operatorAction == SmPromptId::USER_EXIT)
	{
		// EXIT button hit
		RSmManager.sendOperatorExitToServiceData();
		result = FALSE;
	}
	else
	{
        // Disables Prox SST test
		// clear button has pressed.
		isProxTestEnabled_ = FALSE;
		result = FALSE;
	}

	return result;

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: waitForProxStableFlow_
//
//@ Interface-Description
//  This method has two arguments, flowTarget and timeoutMs. It returns 
//  TRUE if prox flow is  stable. Otherwise it returns FALSE when the
//  timeoutMs has occured before stability.
//---------------------------------------------------------------------
//@ Implementation-Description
//  
//---------------------------------------------------------------------
//@ PreCondition
//   none.
//---------------------------------------------------------------------
//@ PostCondition
//   none.
//@ End-Method
//=====================================================================
Boolean ProxTest::waitForProxStableFlow_(Real32 flowTarget, Uint32 timeoutMS)
{

	//  minimum absolute flow error for stability without compressor
    const Real32 FLOW_STABLE_BOUND_WITHOUT_COMPRESSOR_ABS = 0.5;  // lpm, absolute error

	// minimum absolute flow error for stability with compressor
	const Real32 FLOW_STABLE_BOUND_WITH_COMPRESSOR_ABS = 1.5;  // lpm, absolute error

	const Real32 PROX_FLOW_STABLE_BOUND_REL  = 0.10f;

	ProxiInterface& rProxiInterface = *PProxiInterface;
	Boolean flowStable = FALSE;
	Uint32 timer = 0;
	Int32 flowCount = 0;

	Real32 flowStableBoundAbs = 0.0f;
	if ((RSmGasSupply.isCompressorAirPresent()) && (!RSmGasSupply.isWallAirPresent()))
	{
		flowStableBoundAbs = FLOW_STABLE_BOUND_WITH_COMPRESSOR_ABS;
	}
	else
	{
		flowStableBoundAbs = FLOW_STABLE_BOUND_WITHOUT_COMPRESSOR_ABS;
	}

    // Read 840's atm pressure in cmH20
    Real32 atmPressure = RAtmosphericPressureSensor.getValue();

	do
	{
		// read flow and calculate flow error
		Real32 currentFlow = rProxiInterface.getFlow() * (atmPressure /STD_ATM_PRESSURE) ;
		Real32 error = ABS_VALUE(flowTarget - currentFlow);

		// check for flow stability
		if ( (error < PROX_FLOW_STABLE_BOUND_REL * flowTarget) ||
			 (error < flowStableBoundAbs) )
		{
			// flow is within error bounds this cycle
			if (++flowCount >= MIN_CYCLES_FOR_STABILITY)
			{
				// flow is within error bounds for the entire
				// stability check period
				flowStable = TRUE;
			}
			// implied else: flow is NOT stable yet
		}
		else
		{
			// flow error is too large, so reset stability counter
			flowCount = 0;
		}

		timer += SM_CYCLE_TIME;
        Task::Delay(0,SM_CYCLE_TIME);
	}  while( !flowStable && timer < timeoutMS  );


	return flowStable;
	
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: doSensorTest
//
//@ Interface-Description
//  This method has 
//---------------------------------------------------------------------
//@ Implementation-Description
//  
//---------------------------------------------------------------------
//@ PreCondition
//   none.
//---------------------------------------------------------------------
//@ PostCondition
//   none.
//@ End-Method
//=====================================================================
Boolean ProxTest::doSensorTest(void)
{
    skipSensorTest_ = TRUE;
	return doSensorTest_();
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isProxTestEnabled
//
//@ Interface-Description
//  This method has no argument and returns TRUE if a this test is 
//  skipped. Otherwise it returns FALSE.
//---------------------------------------------------------------------
//@ Implementation-Description
//   Simply returns isProxTestEnabled_.
//---------------------------------------------------------------------
//@ PreCondition
//   none.
//---------------------------------------------------------------------
//@ PostCondition
//   none.
//@ End-Method
//=====================================================================
Boolean ProxTest::isProxTestEnabled(void)
{
	return isProxTestEnabled_;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
ProxTest::SoftFault( const SoftFaultID  softFaultID,
                   		const Uint32       lineNumber,
		   				const char*        pFileName,
		   				const char*        pPredicate)
{
	CALL_TRACE("ProxTest::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  	FaultHandler::SoftFault( softFaultID, SERVICE_MODE, PROXTEST,
  							 lineNumber, pFileName, pPredicate) ;
}


