#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2006, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: P100Maneuver - P100 Maneuver Handler
//---------------------------------------------------------------------
//@ Interface-Description
// Handles the P100 maneuver events originated by the UI. This class is
// a derived Maneuver. The Maneuver base class provides data and
// methods common to all maneuvers. This class expands the base class
// functionality for processing the P100 Occlusion maneuver. The P100
// maneuver is a feature of the Respiratory Mechanics Option.
//---------------------------------------------------------------------
//@ Rationale
// This class implements the P100 maneuver functionality on the BD.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ Fault-Handling
// n/a
//---------------------------------------------------------------------
//@ Restrictions
// none
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/P100Maneuver.ccv   25.0.4.0   19 Nov 2013 13:59:58   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003   By: gdc   Date:  23-Feb-2007    SCR Number: 6307
//  Project:  RESPM
//  Description:
//		Added missing RM requirement numbers.
//
//  Revision: 002   By: gdc   Date:  22-Oct-2006    DR Number: 6308
//  Project:  RESPM
//  Description:
//		Changed to enabling patient triggers when pending P100
//      maneuver is canceled instead of using the DISARM maneuver
//      trigger to avoid undesireable PEEP recovery breath.
//
//  Revision: 001   By: gdc   Date:  22-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Initial version.
//
//=====================================================================

#include "P100Maneuver.hh"
#include "BreathMiscRefs.hh"

//@ Usage-Classes
#include "IntervalTimer.hh"
#include "Trigger.hh"
#include "TriggersRefs.hh"
#include "TimerBreathTrigger.hh"
#include "MsgQueue.hh"
#include "BdQueuesMsg.hh"
#include "BreathPhase.hh"
#include "BreathRecord.hh"
#include "BreathSet.hh"
#include "ApneaInterval.hh"
#include "ImmediateBreathTrigger.hh"
#include "P100Trigger.hh"
#include "PhaseRefs.hh"
#include "ExhalationPhase.hh"
#include "PressureInspTrigger.hh"
#include "PressureSetInspTrigger.hh"
#include "NetFlowInspTrigger.hh"
#include "PhasedInContextHandle.hh"
#include "TriggerTypeValue.hh"
#include "ManeuverTimes.hh"

//@ End-Usage

//@ Code...
//=====================================================================
//
//  //@ Data-Member: Methods..
//
//
//=====================================================================

//@ Constant MAX_MANEUVER_PENDING_TIME

// $[RM12095] An armed P100 maneuver shall be terminated if 7 seconds elapse...
const Int32 MAX_P100_MANEUVER_PENDING_TIME = 7000;

//@ Constant MAX_MANEUVER_ACTIVE_TIME
// $[RM12236] Active P100 timeout is 6 seconds
#ifdef E600_840_TEMP_REMOVED
extern const Int32 MAX_P100_MANEUVER_ACTIVE_TIME;
const Int32 MAX_P100_MANEUVER_ACTIVE_TIME = 6000;
#endif

//@ Constant PAUSE_INTERVAL_EXTENSION
extern const Int32 PAUSE_INTERVAL_EXTENSION ;

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: P100Maneuver
//
//@ Interface-Description
//  The method takes a ManeuverId and a reference to a IntervalTimer as
//  arguments. It calls the base class constructor and initializes its
//  data members.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

P100Maneuver::P100Maneuver(ManeuverId maneuverId, IntervalTimer& rPauseTimer) :
	Maneuver(maneuverId),
    TimerTarget(),
    rPauseTimer_(rPauseTimer),
	isTimedOut_(FALSE),
    pauseTime_(0),
	p100ElapsedTime_(0),
	isP100TimerStarted_(FALSE),
	p100Pressure_(0.0) ,
	referencePressure_(0.0),
	isExhalationPhaseReady_(FALSE)
{
	// $[TI1]
	CALL_TRACE("P100Maneuver::P100Maneuver(void)");

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~P100Maneuver()  [Destructor]
//
//@ Interface-Description
// Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

P100Maneuver::~P100Maneuver(void)
{
  CALL_TRACE("~P100Maneuver()");

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: timeUpHappened
//
//@ Interface-Description
// This method takes a reference to an IntervalTimer and has no return
// value. The P100 maneuver is a client of the timer server that
// notifies its client (by invoking this method) of the end of the P100
// maneuver interval.
//---------------------------------------------------------------------
//@ Implementation-Description
// The IntervalTimer reference, rTimer, is that of the interval timer
// server for the P100 maneuver.
//---------------------------------------------------------------------
//@ PreCondition
// &rTimer == &rPauseTimer_
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
void
P100Maneuver::timeUpHappened(const IntervalTimer& rTimer)
{
    CALL_TRACE("timeUpHappened(const IntervalTimer& rTimer)");

	// $[RM12095] An armed P100 maneuver shall be terminated if 7 seconds elapse without detecting a P100 trigger to activate the maneuver.
	// $[RM12236] Active P100 timeout is 6 seconds is max time with valves closed

    // make sure the caller is the proper server
    CLASS_PRE_CONDITION(&rTimer == &rPauseTimer_);

	// Stop the timer, but do not reset the currentTime_,
    rPauseTimer_.stop();
    rPauseTimer_.setCurrentTime(0);

	isTimedOut_ = TRUE;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: handleManeuver
//
//@ Interface-Description
// The method takes a Boolean (isStartRequest) as an argument and 
// returns an EventData::EventStatus. It is called to start the maneuver
// (isStartRequest is TRUE) or stop the maneuver (isStartRequest is
// FALSE). The return status indicates the state of the maneuver after
// processing the request. The client then communicates this state to
// the GUI.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

EventData::EventStatus
P100Maneuver::handleManeuver(const Boolean isStartRequest)
{
    CALL_TRACE("handleManeuver(const Boolean isStartRequest)");

	EventData::EventStatus rtnStatus = EventData::IDLE;

	if (isStartRequest)
	{
		// $[RM12087] A P100 maneuver shall be requested by selecting the Start button...
		// user pressed the START button
		if (maneuverState_ == PauseTypes::IDLE)
		{
			// maneuverState_ is IDLE so we allow the P100 maneuver to happen

			// $[RM12095] An armed P100 maneuver shall be terminated if 7 seconds elapse without detecting a P100 trigger to activate the maneuver.
		    pauseTime_ = MAX_P100_MANEUVER_PENDING_TIME;
		   	rPauseTimer_.setTargetTime(pauseTime_);

		    // start the timer to check the duration when the pause was requested
		   	rPauseTimer_.restart();
			isTimedOut_ = FALSE;

			setActiveManeuver();
	
			// activated via the scheduler
			maneuverState_ = PauseTypes::PENDING;

			// the exhalation flow must be shut down before the maneuver can be activated
			isExhalationPhaseReady_ = FALSE;

			DiscreteValue triggerType =
				PhasedInContextHandle::GetDiscreteValue( SettingId::TRIGGER_TYPE) ;

			// if currently in exhalation with pressure triggering, 
			// enable the P100 trigger immediately 
			// BreathPhase::NewBreath enables the P100 trigger on subsequent exhalation cycles
			// if using flow triggering or if in inspiratory phase
			if (BreathPhase::GetCurrentBreathPhase()->getPhaseType() == BreathPhaseType::EXHALATION &&
				triggerType == TriggerTypeValue::PRESSURE_TRIGGER)
			{
				// Arm the maneuver
                // $[RM12303] When the P100 maneuver is requested during exhalation with pressure triggering active, the maneuver shall be immediately armed.
				// $[RM12305] When the P100 maneuver becomes armed, the flow, pressure and backup pressure triggers shall be disabled.
				RPressureInspTrigger.disable() ;
				RPressureBackupInspTrigger.disable();
				// $[RM12090] When the P100 maneuver becomes armed, the P100 inspiratory trigger shall be enabled.
				RP100Trigger.enable();
			}

			// This return status shall update the ExpiratoryPauseEvent's
			// eventStatus_ through UiEvent.cc (UiEvent::setEventStatus)
			// This return status shall be sent to GUI to communicate to
			// the user through UiEvent::setEventStatus too.
			rtnStatus = EventData::PENDING;
		}
		else if (maneuverState_ == PauseTypes::PENDING ||
				 maneuverState_ == PauseTypes::ACTIVE)
		{
			// $[RM12030] If a P100 maneuver is pending or active, accept a duplicate P100 request with no action
			// An P100 maneuver was requested before.
			// Simply ignore this request - do nothing
			// an EventData::NO_ACTION_ACK is sending back to GUI.
			rtnStatus = EventData::NO_ACTION_ACK;
		}
		else
		{
			// Do nothing
			// Note: we do not perform any special operation for IDLE_PENDING
			// due to that this status will be reset by the current running
			// scheduler to IDLE in the same 5 ms cycle.
            AUX_CLASS_ASSERTION( (maneuverState_ == PauseTypes::IDLE_PENDING),
                             	 maneuverState_ );
		}
	}
	else
	{
		// user released the START button
		rtnStatus = EventData::NO_ACTION_ACK;
	}
	
	return (rtnStatus);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activate
//
//@ Interface-Description
//  This method takes no arguments and returns nothing.
//	This method is invoked by the P100PausePhase breath phase when 
//  taking control in the current scheduler. Calling this method changes
//  the maneuver state to ACTIVE.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
P100Maneuver::activate(void)
{
	CALL_TRACE("activate(void)");

	CLASS_ASSERTION(maneuverState_ = PauseTypes::PENDING);

	p100Pressure_ = 0.0;
	isP100TimerStarted_ = FALSE; 
	p100ElapsedTime_ = 0;
	activeElapsedTime_ = 0;

	// $[RM12236] Active P100 timeout is 6 seconds is max time with valves closed
	pauseTime_ = MAX_P100_MANEUVER_ACTIVE_TIME;
	rPauseTimer_.setTargetTime(pauseTime_);

	// start the timer to check the duration after maneuver activated
	rPauseTimer_.restart();
	isTimedOut_ = FALSE;

	// since we hold off inspiration while maneuver is active, extend apnea here
	// $[RM12092] Once a P100 maneuver is active, the apnea interval shall be extended by the maximum amount of time the maneuver can be active.
	RApneaInterval.extendInterval(MAX_P100_MANEUVER_ACTIVE_TIME + PAUSE_INTERVAL_EXTENSION);

	// Setup active maneuver state
	maneuverState_ = PauseTypes::ACTIVE;

	// notify the user that P100 maneuver is active:
	RP100ManeuverEvent.setEventStatus(EventData::ACTIVE);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: complete
//
//@ Interface-Description
// This method takes no arguments and returns a Boolean. It is 
// invoked when the P100 maneuver completes. It returns TRUE if there
// are no more pending P100 maneuvers.
//---------------------------------------------------------------------
//@ Implementation-Description
// Invokes clear() to complete the P100 maneuver and clean up.
// Always returns TRUE to indicate there are no more pending P100
// maneuvers since P100 maneuvers are not queued or stacked.
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
Boolean
P100Maneuver::complete(void)    
{
    CALL_TRACE("complete(void)");

	clear();

	return (TRUE);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: terminateManeuver
//
//@ Interface-Description
//  This method takes no arguments and returns nothing. Deactivates 
//  the maneuver on the next BD cycle.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Sets the clearRequested_ flag TRUE to cancel the maneuver on the 
//  next BD cycle in newCycle().
//---------------------------------------------------------------------
//@ PreCondition
//    none.
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

void
P100Maneuver::terminateManeuver(void)
{
	clearRequested_ = TRUE;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processUserEvent
//
//@ Interface-Description
// This method takes a reference to a UiEvent and returns a Boolean
// to indicate whether the UiEvent can be processed while this P100
// maneuver is active. Returns TRUE to continue processing of the 
// new UiEvent, otherwise returns FALSE.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none.
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

Boolean
P100Maneuver::processUserEvent(UiEvent &rUserEvent)
{
	Boolean continueNewEvent = TRUE;

	if (rUserEvent.getId() == EventData::P100_MANEUVER ||
		rUserEvent.getId() == EventData::MANUAL_INSPIRATION)
	{
		// $[RM12235] A manual inspiration request shall be accepted during a P100 maneuver
		continueNewEvent = TRUE;
	}
	else
	{
		// $[RM12033] All RM maneuver requests shall be rejected if any other pause or RM maneuver has already taken place during the same breath.
		rUserEvent.setEventStatus(EventData::REJECTED);
		continueNewEvent = FALSE;
	}

	return(continueNewEvent);
}




//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newCycle
//
//@ Interface-Description
// This method takes no arguments and returns nothing. It is 
// invoked every BD cycle. It implements a state machine that defines
// the P100 maneuver. It processes external events such as button
// press/release events and timeouts using internal variables that are    
// examined every BD cycle. Processing of these events are based on 
// the current state of the maneuver.
//---------------------------------------------------------------------
//@ Implementation-Description
// The actions of this method are dependent on the state of the 
// maneuver when this method is called. It uses triggers to signal 
// the scheduler and uses the P100ManeuverEvent.setEventStatus() method 
// to send maneuver status to the GUI. It processes external events 
// such as button press/release events and timeouts using Boolean flags 
// that are examined every BD cycle.
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

void
P100Maneuver::newCycle(void)    
{
	CALL_TRACE("newCycle(void)");

	if (maneuverState_ == PauseTypes::PENDING)
	{
		// $[RM12090] When the P100 maneuver becomes armed, the P100 inspiratory trigger shall be enabled. 
		// the maneuver is waiting for a P100Trigger in this state

		// record the airway pressure before the maneuver is activated
		referencePressure_ = BreathRecord::GetAirwayPressure();

		if (!isExhalationPhaseReady_)
		{
			BreathPhase* pBreathPhase = BreathPhase::GetCurrentBreathPhase();
			const  BreathPhaseType::PhaseType currentPhase = pBreathPhase->getPhaseType();

			if (currentPhase == BreathPhaseType::EXHALATION && RExhalationPhase.getFlowTarget() <= 1.0)
			{
				// $[RM12095] An armed P100 maneuver shall be terminated if 7 seconds elapse without detecting a P100 trigger to activate the maneuver.

				// exhalation flow has been reduced to 1.0 LPM in preparation
				// for the P100 inspiratory phase
				isExhalationPhaseReady_ = TRUE;

				// reset the timeout -- the patient must now trigger a breath 
				// within 7 seconds to activate the P100 maneuver and start
				// the P100 inspiratory phase.
				pauseTime_ = MAX_P100_MANEUVER_PENDING_TIME;
				rPauseTimer_.setTargetTime(pauseTime_);

				// start the timer to check the duration after exhalation phase
				// is ready
				rPauseTimer_.restart();
				isTimedOut_ = FALSE;
			}
		}

		if (clearRequested_ || isTimedOut_)
		{
			// $[RM12095] An armed P100 maneuver shall be terminated if 7 seconds elapse without detecting a P100 trigger to activate the maneuver.
			clear();
		}

	}
	else if (maneuverState_ == PauseTypes::ACTIVE)
	{
		// the P100PausePhase is active in this state

		if (clearRequested_ || isTimedOut_)
		{		
			// $[RM12236] Active P100 timeout is 6 seconds is max time with valves closed

			// trigger to stop the VcmPsvPhase
			RPauseCompletionTrigger.enable();

			// P100PausePhase will "complete" the maneuver upon receiving the trigger
			// we don't go to the IDLE_PENDING state here so clear() will send a CANCEL 
			// status to the GUI instead of COMPLETE status
			 
			// the scheduler restarts the breath timer when triggered out of P100PausePhase
		}
		else
		{
			Real32 currentPressure = BreathRecord::GetAirwayPressure();

			// $[RM12307] The valves shall remain closed for 100 msec after the pressure has dropped 0.5 cmH2O from the reference pressure measured when the valves are closed.

			// set reference pressure upon activating maneuver
			if (activeElapsedTime_ == 0)
			{
				// $[RM12307] 
				referencePressure_ = currentPressure;
			}

			if (isP100TimerStarted_)
			{
				// $[RM12307]
				p100ElapsedTime_ += CYCLE_TIME_MS;
			}
			else if (currentPressure <= referencePressure_ - 0.5)
			{
				// $[RM12307]
				isP100TimerStarted_ = TRUE;
			}

			if (p100ElapsedTime_ == 100)
			{
				// $[RM12307] 
				// $[RM12061] ...the value of Pwye measured at 100 msec... shall be displayed...
				// $[RM12309] ...the value of Pwye measured at 100 msec... shall be displayed...
				p100Pressure_ = currentPressure - (referencePressure_ - 0.5F);
				RP100CompletionTrigger.enable();
				maneuverState_ = PauseTypes::IDLE_PENDING;
			}

			activeElapsedTime_ += CYCLE_TIME_MS;
		}
	}
	else if ( maneuverState_ == PauseTypes::IDLE_PENDING )
	{
		// since P100PausePhase completes the maneuver, 
		// we should never get to this state in newCycle()
		CLASS_ASSERTION_FAILURE();
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: clear
//
//@ Interface-Description
// This method takes no arguments and returns nothing.  It is 
// invoked every time the P100 maneuver completes or is terminated.
// Its actions and processing are based on the state of the maneuver
// when called. It signals the scheduler, informs the GUI of the 
// maneuver's completion status and signals to send P100 data to the 
// GUI. It resets its internal data in preparation for the next P100
// request.
//---------------------------------------------------------------------
//@ Implementation-Description
// The actions of this method are dependent on the state of the 
// maneuver when this method was called. 
// It uses triggers to signal maneuver completion to the scheduler.
// It calls the P100ManeuverEvent.setEventStatus() method 
// to send the maneuver completion status to the GUI.
// It uses MsgQueue::PutMsg to signal the BreathData task to send
// the P100 data to the GUI.
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
void
P100Maneuver::clear(void)    
{
    CALL_TRACE("clear(void)");

	// $[RM12035] The GUI shall annunciate any RM request and distinguish between requests that are accepted and those that are rejected. The GUI shall also indicate the activation, cancellation, and termination of any RM maneuver.

	if (maneuverState_ == PauseTypes::PENDING)
	{
	    // $[RM12095] an armed maneuver shall be terminated if 7 seconds elapse...
		RP100Trigger.disable();

		// Set the state to IDLE so we can re-enable the normal patient triggers
		// for exhalation. Since the inspiratory patient triggers are unaffected
		// by the P100 maneuver, we only need to re-enable the exhalation triggers
		// if we're in exhalation. If we're in inspiration then the next transition
		// to exhalation will re-enable the normal exhalation triggers.
		maneuverState_ = PauseTypes::IDLE;
		if (BreathPhase::GetCurrentBreathPhase()->getPhaseType() == BreathPhaseType::EXHALATION)
		{
			BreathPhase::EnablePatientTriggers(BreathPhaseType::EXHALATION);
		}

		// $[RM12035] 
		RP100ManeuverEvent.setEventStatus(EventData::CANCEL);
	}
	else if (maneuverState_ == PauseTypes::ACTIVE)
	{
		// P100PausePhase::relinquishControl cancelled the maneuver due to an unexpected trigger

		// $[RM12035] 
		RP100ManeuverEvent.setEventStatus(EventData::CANCEL);
	}
	else if (maneuverState_ == PauseTypes::IDLE_PENDING)
	{		
		// $[RM12035]
		RP100ManeuverEvent.setEventStatus(EventData::COMPLETE);

		// $[RM12061] ...the value of Pwye measured at 100 msec... shall be displayed on the Waveforms screen.
		// $[RM12309] ...the value of Pwye measured at 100 msec... shall be displayed on the on the P100 subscreen.

		// signal to send P0.1 data to GUI
		MsgQueue::PutMsg(::BREATH_DATA_Q, BdQueuesMsg::SEND_END_P100_PAUSE_DATA);
	}
	else
	{
		AUX_CLASS_ASSERTION_FAILURE(maneuverState_);
	}

	maneuverState_ = PauseTypes::IDLE;
	isExhalationPhaseReady_ = FALSE;
	
	// Stop the timer, but do not reset the currentTime_,
    rPauseTimer_.stop();
    rPauseTimer_.setCurrentTime(0);
	isTimedOut_ = FALSE;
    RPauseCompletionTrigger.disable();
    RP100CompletionTrigger.disable();

   	resetActiveManeuver();
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
P100Maneuver::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, P100MANEUVER,
                          lineNumber, pFileName, pPredicate);
}

//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//=====================================================================
//
//  Private Methods...
//
//=====================================================================


