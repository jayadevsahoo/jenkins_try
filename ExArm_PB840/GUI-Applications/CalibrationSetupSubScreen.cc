#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: CalibrationSetupSubScreen - Activated by selecting Exhalation Valve 
// Calibration in the Service Lower Other Screen after user press/release the
// Lower Other screen tab button.
//---------------------------------------------------------------------
//@ Interface-Description
// This subscreen is activated by selecting Exhalation Valve Calibration
// button in the ServiceLowerOtherScreen. 
// Only one instance of this class is created by LowerSubScreenArea.  The
// interface is pretty generic, as a SubScreen it defines the activate()
// and deactivate() methods and then receives button events
// through via the usual means (i.e., the buttonDownHappened(),
// buttonUpHappened(), adjustPanelClearPressHappened and adjustPanelAcceptPressHappened()) 
//---------------------------------------------------------------------
//@ Rationale
// Groups the components of the Calibration Setup subscreen in a single class.
//---------------------------------------------------------------------
//@ Implementation-Description
// The main operation of this class is to wait for button down and up events,
// received via buttonUpHappened() ,buttonDownHappened(), adjustPanelClearPressHappened()
// and adjustPanelAcceptPressHappened().  It also waits for test result data and
// test commands received from Service-Data and dispatched to the virtual
// methods processTestPrompt(), processTestKeyAllowed(), 
// processTestResultStatus(), processTestResultCondition(), and 
// processTestData().  
// On the other hand, button events are caught by the button callback methods. 
// The display of this subscreen is organized into many different screen
// layouts corresponding to different stages of the test process.
// Pressing down the Start Test button causes the exhalation calibration process
// to begin.  The user's responses to the prompts displayed are what drives
// the process forward.
//
// The ServiceDataRegistrar is used to register for servicd data events.
// All other functionality is inherited from the SubScreenArea class.
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and
// pre-conditions to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
// Only one instance of this class should be created.  It should only be
// displayed in the LowerSubScreenArea.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/CalibrationSetupSubScreen.ccv   25.0.4.0   19 Nov 2013 14:07:38   pvcs  $ 
//
//@ Modification-Log
//
//  Revision: 010   By: srp   Date:  24-Apr-2002    DR Number: 5327 & 5908
//  Project:  NeoMode
//  Description:
//      Added Modification log. Updated for NeoMode  03/01/00
//
//  Revision: 009  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 008  By:  yyy    Date: 10-Oct-1997    DR Number: DCS 2452
//       Project:  Sigma (840)
//       Description:
//			Changed due to ExhValveCalibration modification.
//
//  Revision: 007  By:  yyy    Date:  25-SEP-1997    DR Number:   1861
//    Project:  Sigma (R8027)
//    Description:
//      Display more clear diagnostic information for all types of calibration
//		tests and vent inop test.
//
//  Revision: 006  By: gdc      Date: 23-Sep-1997  DR Number: 2513
//    Project:  Sigma (R8027)
//    Description:
//    Cleaned up Service-Mode interfaces to support remote test.
//
//  Revision: 005  By: yyy      Date: 19-Aug-1997  DR Number: 2338
//    Project:  Sigma (R8027)
//    Description:
//    Added error condition:FS_UNABLE_TO_PROGRAM_FLASH.
//
//  Revision: 004  By: yyy      Date: 19_Aug-1997  DR Number: 2340
//    Project:  Sigma (R8027)
//    Description:
//    Qualified adjustPanelClearPressHappened() to handle the CLEAR key
//	  only when running EXH_VALVE_CALIB_TEST_ID test.
//
//  Revision: 003  By: yyy      Date: 20-May-1997  DR Number: 2107
//    Project:  Sigma (R8027)
//    Description:
//      20-May-1997 Disabled the EST tab buttom being set to flat.
//      01-Aug-1997 Updated the new status information so NEVER RUN,
//		PASS, and IN PROGRESS can be processed correctly.
//
//  Revision: 002  By: yyy      Date: 07-May-1997  DR Number: 2035
//    Project:  Sigma (R8027)
//    Description:
//      Added new promot "Connect AC" to avoid of buss error/access fault error.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "CalibrationSetupSubScreen.hh"

#include "MiscStrs.hh"
#include "PromptStrs.hh"
#include "SmTestId.hh"
#include "ServiceLowerScreen.hh"
#include "ServiceUpperScreen.hh"
#include "ServiceLowerScreenSelectArea.hh"
#include "UpperSubScreenArea.hh"

#if defined FAKE_SM
#	include "FakeServiceModeManager.hh"
#   define SmManager FakeServiceModeManager
#else		
#	include "SmManager.hh"
#endif	// FAKE_SM

//@ Usage-Classes
#include "AdjustPanel.hh"
#include "PromptArea.hh"
#include "Sound.hh"
#include "Colors.hh"
#include "GuiAppClassIds.hh"

class SubScreenArea;
//@ End-Usage

//@ Code...

// Initialize static constants.
static const Uint16 BUTTON_X_ = 520;
static const Uint16 BUTTON_Y_ = 367;
static const Uint16 BUTTON_WIDTH_ = 110;
static const Uint16 BUTTON_HEIGHT_ = 34;
static const Uint8  BUTTON_BORDER_ = 3;
static const Int32 STATUS_LABEL_X_ = 379;
static const Int32 STATUS_LABEL_Y_ = 0;
static const Int32 STATUS_WIDTH_ = 255;
static const Int32 STATUS_HEIGHT_ = 31;
static const Int32 ERR_DISPLAY_AREA_X1_ = 115;
static const Int32 ERR_DISPLAY_AREA_Y1_ = 100;
static const Int32 ERR_DISPLAY_ROW_WIDTH_ = 20;
static const Int32 CAL_WARNING_LABEL_X_ = 120;
static const Int32 CAL_WARNING_LABEL_Y_ = 40;

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: CalibrationSetupSubScreen()  [Default Constructor]
//
//@ Interface-Description
// Creates the Exhalation Calibration Subscreen.  The given `pSubScreenArea' is 
// the SubScreenArea in which it will be displayed (in this case the upper 
// subscreen area).
//---------------------------------------------------------------------
//@ Implementation-Description
// Creates, positions, and initializes all the graphical components of this
// subscreen:  title and a number of static and dynamic textual items.
//  Also, register for callback for the Start Test button.
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

CalibrationSetupSubScreen::CalibrationSetupSubScreen(SubScreenArea *pSubScreenArea) :
		SubScreen(pSubScreenArea),
		calStatus_(STATUS_LABEL_X_, STATUS_LABEL_Y_,
						STATUS_WIDTH_, STATUS_HEIGHT_,
						MiscStrs::EXH_V_CAL_STATUS_LABEL,
						NULL_STRING_ID),
		calWarningMsg_(MiscStrs::EXH_V_CAL_WARNING_MSG_LABEL),
		errorHappened_(FALSE),
		errorIndex_(0),
		keyAllowedId_(-1),
		userKeyPressedId_(SmPromptId::NULL_ACTION_ID),
		startTestButton_(BUTTON_X_,				BUTTON_Y_,
						BUTTON_WIDTH_,			BUTTON_HEIGHT_,
						Button::DARK, 			BUTTON_BORDER_,
						Button::SQUARE,			Button::NO_BORDER,
						MiscStrs::START_TEST_TAB_BUTTON_LABEL),
		guiTestMgr_(this),
		titleArea_(MiscStrs::EXH_VALVE_CALIB_SUBSCREEN_TITLE)

{
	CALL_TRACE("CalibrationSetupSubScreen::CalibrationSetupSubScreen(pSubScreenArea)");

	// Size and position the sub-screen
	setX(0);
	setY(0);
	setWidth(SERVICE_LOWER_SUB_SCREEN_AREA_WIDTH);
	setHeight(SERVICE_LOWER_SUB_SCREEN_AREA_HEIGHT);

	for (int i=0; i<MAX_CONDITION_ID; i++)
	{
		errDisplayArea_[i].setColor(Colors::WHITE);
		errDisplayArea_[i].setY(ERR_DISPLAY_AREA_Y1_+(i*ERR_DISPLAY_ROW_WIDTH_));
		errDisplayArea_[i].setX(ERR_DISPLAY_AREA_X1_);
		addDrawable(&errDisplayArea_[i]);
	}

	// Register for callbacks for all buttons in which we are interested.
	startTestButton_.setButtonCallback(this);

	// Add the title area
	addDrawable(&titleArea_);

	// Add the buttons.
	addDrawable(&startTestButton_);

	// Add status line text objects to main container
	addDrawable(&calStatus_);

	// Setup menu stuff
	calWarningMsg_.setX(CAL_WARNING_LABEL_X_);
	calWarningMsg_.setY(CAL_WARNING_LABEL_Y_);
	calWarningMsg_.setColor(Colors::WHITE);

	// Add the warning message.
	addDrawable(&calWarningMsg_);

	// Initialize the promptName, promptId arrays and prompts after an error
	// condition has occured
	setPromptTable_();

	guiTestMgr_.setupTestResultDataArray();
	guiTestMgr_.setupTestCommandArray();
													// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~CalibrationSetupSubScreen  [Destructor]
//
//@ Interface-Description
//  Destroys the Enter Service subscreen.  Needs to do nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Empty 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

CalibrationSetupSubScreen::~CalibrationSetupSubScreen(void)
{
	CALL_TRACE("CalibrationSetupSubScreen::~CalibrationSetupSubScreen(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activate
//
//@ Interface-Description
//  Called by LowerSubScreenArea just before this subscreen is displayed
//  on the screen.  No parameters needed.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The first thing to do is to update the test status with result 
//  from the last Service run, if any.  Next, register the subscreen with
//  Gui Test manager for test events.  Then we call the generic screen layout
//  method to display the initial subscreen configuration.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
CalibrationSetupSubScreen::activate(void)
{
	CALL_TRACE("CalibrationSetupSubScreen::activate(void)");

	currentTestId_ = SmTestId::TEST_NOT_START_ID;
	
	if (GuiApp::IsExhValveCalSuccessful())
	{		// $[TI1]
		calStatus_.setStatus(MiscStrs::EXH_V_CAL_COMPLETE_MSG);

		// Show the calibration warning message Only when the
		// previous calibration is successful.
		calWarningMsg_.setShow(TRUE);
	}
	else
	{		// $[TI2]		
		calStatus_.setStatus(MiscStrs::EXH_V_CAL_FAILED_MSG);
	}

	//
	// Register all general possible test results for test events, but first
	// we have to initialize ServiceDataRegistrar.
	//
	guiTestMgr_.registerTestResultData();
	guiTestMgr_.registerTestCommands();

	// Let the service mode  manager knows the type of Service mode.
	SmManager::DoFunction(SmTestId::SET_TEST_TYPE_MISC_ID);

	layoutScreen_(CALIBRATION_SETUP);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: deactivate
//
//@ Interface-Description
//  Called by LowerSubScreenArea just after this subscreen has been
//  removed from the screen.  No parameters are needed.
//---------------------------------------------------------------------
//@ Implementation-Description
//  We perform any necessary cleanup.  First signal to Gui Test manager
//  about the end of test.  Release the AdjustPanel focus, then clear
//  the prompt area.  The Start Test button is shown in up position.
//  Finally, the LowerScreen Select area is shown.
// $[07008] When the test run finished or is stopped by the technician ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
CalibrationSetupSubScreen::deactivate(void)
{
	CALL_TRACE("CalibrationSetupSubScreen::deactivate(void)");

	// Make sure that nothing in this subscreen keeps focus.
	AdjustPanel::TakeNonPersistentFocus(NULL, TRUE);

	// Clear the Primary prompt
	guiTestMgr_.setTestPrompt(PromptArea::PA_PRIMARY,
		 PromptArea::PA_HIGH, NULL_STRING_ID);

	guiTestMgr_.setTestPrompt(PromptArea::PA_PRIMARY,
		 PromptArea::PA_LOW, NULL_STRING_ID);

	// Clear the Advisory prompts
	guiTestMgr_.setTestPrompt(PromptArea::PA_ADVISORY,
		 PromptArea::PA_HIGH, NULL_STRING_ID);

	// Clear the Advisory prompts
	guiTestMgr_.setTestPrompt(PromptArea::PA_ADVISORY,
		 PromptArea::PA_LOW, NULL_STRING_ID);

	// Clear the Secondary prompt
	guiTestMgr_.setTestPrompt(PromptArea::PA_SECONDARY,
		PromptArea::PA_HIGH, NULL_STRING_ID);

	// Clear the Secondary prompt
	guiTestMgr_.setTestPrompt(PromptArea::PA_SECONDARY,
		PromptArea::PA_LOW, NULL_STRING_ID);


	// Pop up the button.
	startTestButton_.setToUp();

	// Activate lower subscreen area.
	ServiceLowerScreen::RServiceLowerScreen.getServiceLowerScreenSelectArea()->setBlank(FALSE);
												// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: buttonDownHappened
//
//@ Interface-Description
// Called when start buttons in Exhalation Valve Calibration subscreen is
// pressed down (because this button registers a callback).
// Passed a pointer to the button as well as 'byOperatorAction', a flag which
// indicates whether the operator pressed the button down or whether the
// setToDown() method was called.
// $[07049] The Exhalation Valve calibration subscreen shall provide a button ...
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
CalibrationSetupSubScreen::buttonDownHappened(Button *pButton,
									Boolean byOperatorAction)
{
	CALL_TRACE("CalibrationSetupSubScreen::buttonDownHappened(Button *pButton, "
											"Boolean byOperatorAction)");
	if (!isVisible())
	{													// $[TI1]
		return;
	}													// $[TI2]

	if (pButton == &startTestButton_)
	{		// $[TI3]
		// Grab Adjust Panel focus
		AdjustPanel::TakeNonPersistentFocus(this, TRUE);

		// Disable the knob sound.
		AdjustPanel::DisableKnobRotateSound();

		//
		// Set all prompts 
		//
		guiTestMgr_.setTestPrompt(PromptArea::PA_PRIMARY, 
								PromptArea::PA_HIGH,
								PromptStrs::SM_KEY_ACCEPT_P);
		guiTestMgr_.setTestPrompt(PromptArea::PA_ADVISORY, 
								PromptArea::PA_HIGH,
								NULL_STRING_ID);
		guiTestMgr_.setTestPrompt(PromptArea::PA_ADVISORY, 
								PromptArea::PA_LOW,
								NULL_STRING_ID);
		guiTestMgr_.setTestPrompt(PromptArea::PA_SECONDARY,
								PromptArea::PA_HIGH,
								PromptStrs::OTHER_SCREENS_CANCEL_S);

		// Set the next test status.
		testStateId_ = START_BUTTON_PRESSED;
	}		// $[TI4]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: buttonUpHappened
//
//@ Interface-Description
// Called when any button in Exhalation Valve Calibration subscreen is 
// deselected.  Passed a pointer to the button as well as 'byOperatorAction', 
// which indicates whether the operator pressed the button up or whether the 
// setToUp() method was called.
//---------------------------------------------------------------------
//@ Implementation-Description
//  There is a possibility that this event occurs after this subscreen is
//  removed from the screen (when this subscreen is deactivated and the
//  Adjust Panel focus is lost).  In this case we do nothing, simply return.
//  Otherwise, we clear the Adjust Panel focus, dehighlight the prompt area
//  and change the display state id. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
CalibrationSetupSubScreen::buttonUpHappened(Button *pButton, 
													Boolean byOperatorAction)
{
	CALL_TRACE("CalibrationSetupSubScreen::buttonUpHappened(Button *pButton, "
										"Boolean byOperatorAction)");

	// Check if the event is operator-initiated
	if (byOperatorAction)
	{													// $[TI1]
		// Clear the all prompts
		// Clear the Adjust Panel focus
		AdjustPanel::TakeNonPersistentFocus(NULL, TRUE);

		// Make sure the prompt area's background is dehighlighted.
		guiTestMgr_.setTestPrompt(PromptArea::PA_PRIMARY,
				 PromptArea::PA_HIGH, NULL_STRING_ID);

		guiTestMgr_.setTestPrompt(PromptArea::PA_SECONDARY, 
				PromptArea::PA_HIGH, NULL_STRING_ID);

		// Set the next test status.
		testStateId_ = UNDEFINED_TEST_STATE;
	}													// $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelAcceptPressHappened
//
//@ Interface-Description
//  This is a virtual method inherited from being an AdjustPanelTarget.  It is
//  normally called when the operator presses down the Accept key to signal the
//  accepting of continuing the test process.
//---------------------------------------------------------------------
//@ Implementation-Description
//  First reset the Adjust Panel focus, then clear the prompt area.
//  Next, depending on the display state id, we layout the screen accordingly.
//  The state id is set to UNDEFINED_TEST_STATE.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
CalibrationSetupSubScreen::adjustPanelAcceptPressHappened(void)
{
	CALL_TRACE("CalibrationSetupSubScreen::adjustPanelAcceptPressHappened(void)");

	// Make sure that nothing in this subscreen keeps focus.
	AdjustPanel::TakeNonPersistentFocus(NULL, TRUE);

	// Make sure the prompt area's background is dehighlighted.
	guiTestMgr_.setTestPrompt(PromptArea::PA_PRIMARY,
				 PromptArea::PA_HIGH, NULL_STRING_ID);

	guiTestMgr_.setTestPrompt(PromptArea::PA_ADVISORY,
			 PromptArea::PA_HIGH, NULL_STRING_ID);

	guiTestMgr_.setTestPrompt(PromptArea::PA_SECONDARY,
			PromptArea::PA_HIGH, NULL_STRING_ID); 

	switch(testStateId_)
	{
	case START_BUTTON_PRESSED:		// $[TI1]

		layoutScreen_(CALIBRATION_START_ACCEPTED);

		break;

	case PROMPT_ACCEPT_BUTTON_PRESSED:		// $[TI2]
		// Set user key pressed id.
		userKeyPressedId_ = SmPromptId::KEY_ACCEPT;
		
		// ACCEPT key being pressed in response to the test PROMPT.
		layoutScreen_(CALIBRATION_PROMPT_ACCEPTED_CLEARED);
		break;

	default:								// $[TI3]
		// Make the Invalid Entry sound
		Sound::Start(Sound::INVALID_ENTRY);
		break;
	}

	testStateId_ = UNDEFINED_TEST_STATE;
	keyAllowedId_ = SmPromptId::NULL_PROMPT_ID;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelClearPressHappened
//
//@ Interface-Description
//  This is a virtual method inherited from being an AdjustPanelTarget.  It is
//  normally called when the operator presses down the Clear key to signal the
//  accepting of continuing the test process.
//---------------------------------------------------------------------
//@ Implementation-Description
//  First reset the Adjust Panel focus, then clear the prompt area.
//  Next, depending on the display state id, we layout the screen accordingly.
//  The state id is set to UNDEFINED_TEST_STATE.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
CalibrationSetupSubScreen::adjustPanelClearPressHappened(void)
{
	CALL_TRACE("CalibrationSetupSubScreen::adjustPanelClearPressHappened(void)");

	if (currentTestId_ == SmTestId::EXH_VALVE_CALIB_TEST_ID &&
		testStateId_ == PROMPT_ACCEPT_BUTTON_PRESSED &&
		keyAllowedId_ == SmPromptId::ACCEPT_AND_CANCEL_ONLY)
	{										// $[TI1]
		// Make sure that nothing in this subscreen keeps focus.
		AdjustPanel::TakeNonPersistentFocus(NULL, TRUE);

		// Make sure the prompt area's background is dehighlighted.
		guiTestMgr_.setTestPrompt(PromptArea::PA_PRIMARY,
				 PromptArea::PA_HIGH, NULL_STRING_ID);

		guiTestMgr_.setTestPrompt(PromptArea::PA_ADVISORY,
			 PromptArea::PA_HIGH, NULL_STRING_ID);

		guiTestMgr_.setTestPrompt(PromptArea::PA_SECONDARY,
			PromptArea::PA_HIGH, NULL_STRING_ID); 

		userKeyPressedId_ = SmPromptId::KEY_CLEAR;

		// CLEAR key being pressed in response to the test PROMPT.
		layoutScreen_(CALIBRATION_PROMPT_ACCEPTED_CLEARED);
		errorHappened_ = TRUE;
		testStateId_ = UNDEFINED_TEST_STATE;
		keyAllowedId_ = SmPromptId::NULL_PROMPT_ID;
	}
	else
	{										// $[TI2]
		// Make the Invalid Entry sound
		Sound::Start(Sound::INVALID_ENTRY);
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: layoutScreen_
//
//@ Interface-Description
// Sets the subscreen into one of three display configurations.
//---------------------------------------------------------------------
//@ Implementation-Description
//  If screen id is CALIBRATION_SETUP, we update the prompt area and 
//  show the Start Test button.
//  If screen id is CALIBRATION_START_ACCEPTED, we reset the error flag,
//  and error display area, update the prompt area, hide the start test
//  button and the lower select area.  Then we start up the test.
//  The status area is also updated.
//  If screen id is CALIBRATION_PROMPT_ACCEPTED_CLEARED, we update the prompt, status,
//  then communicate this state to Service Mode manager.
// $[07007] When an EST test, an Exhalation valve calibration or ...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
CalibrationSetupSubScreen::layoutScreen_(CalScreenId calScreenId)
{
	CALL_TRACE("CalibrationSetupSubScreen::layoutScreen_(CalScreenId calScreenId)");

	switch (calScreenId)
	{
	case CALIBRATION_SETUP:				// $[TI1]
		// Setup for transfer flash calibration information to GUI board.
		titleArea_.setTitle(MiscStrs::EXH_VALVE_CALIB_SUBSCREEN_TITLE);

		// Set all prompts
		guiTestMgr_.setTestPrompt(PromptArea::PA_PRIMARY,
			PromptArea::PA_LOW, PromptStrs::EXH_V_CAL_TOUCH_START_TEST_TO_BEGIN_P);
		guiTestMgr_.setTestPrompt(PromptArea::PA_ADVISORY,
			 PromptArea::PA_LOW, NULL_STRING_ID);
		guiTestMgr_.setTestPrompt(PromptArea::PA_SECONDARY,
			PromptArea::PA_LOW, PromptStrs::OTHER_SCREENS_CANCEL_S);

		// Show the startTest button
		startTestButton_.setShow(TRUE);

		break;

	case CALIBRATION_START_ACCEPTED:	// $[TI2]
	{
		// Hide the calibration warning message.
		calWarningMsg_.setShow(FALSE);

		// Set all prompts
		// Pop up the startTestButton_.
		startTestButton_.setToUp();

		// Hide the start button.
		startTestButton_.setShow(FALSE);

		// Hide the lower screen select area.
		ServiceLowerScreen::RServiceLowerScreen.getServiceLowerScreenSelectArea()->setBlank(TRUE);

		startTest_(SmTestId::EXH_VALVE_CALIB_TEST_ID);

		break;
	}	

	case CALIBRATION_PROMPT_ACCEPTED_CLEARED:	// $[TI3]

		// Set Primary prompt to "Please Wait...."
		guiTestMgr_.setTestPrompt(PromptArea::PA_PRIMARY,
			PromptArea::PA_LOW, PromptStrs::EXH_V_CAL_PLEASE_WAIT_P);

		guiTestMgr_.setTestPrompt(PromptArea::PA_ADVISORY,
				 PromptArea::PA_HIGH, NULL_STRING_ID);

		guiTestMgr_.setTestPrompt(PromptArea::PA_SECONDARY,
				PromptArea::PA_HIGH, NULL_STRING_ID);

		// $[07050] The Exhalation valve calibration shall provide ...
		// Display the overall testing status on the service status area
		calStatus_.setStatus(MiscStrs::EXH_V_CAL_RUNNING_MSG);

		// Signal the Service Test Manager to continue the test.
#ifndef FAKE_SM
		if (userKeyPressedId_ != SmPromptId::NULL_ACTION_ID)
#endif
		{								// $[TI4]
			SmManager::OperatorAction(SmTestId::EXH_VALVE_CALIB_TEST_ID,
						  userKeyPressedId_);
		}								// $[TI5]
		break;

	default:
		CLASS_ASSERTION_FAILURE();
		break;
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setPromptTable_
//
//@ Interface-Description
//  Initialize the promptId_, promptName_ and conditionText_ tables.  
//  The promptId_ array contains Service-Mode prompt ids.
//  The promptName_ table contains translation text of Service-Mode prompt code.
//  The conditionText_ table contains translation text of Service-Mode
//  error conditions.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Simply set the proper text and ids to the corresponding tables.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
CalibrationSetupSubScreen::setPromptTable_(void)
{
	Int testIndex = 0;

	CALL_TRACE("CalibrationSetupSubScreen::setPromptTable_(void)");

	// First clear each entry in the array.
	for (testIndex = 0; testIndex < MAX_CAL_PROMPTS; testIndex++)
	{
		promptId_[testIndex]	= SmPromptId::NULL_PROMPT_ID;
		promptName_[testIndex] 	= NULL_STRING_ID;
	}

	testIndex = 0;

	promptId_[testIndex] = SmPromptId::CONNECT_AC_PROMPT;
	promptName_[testIndex++] = PromptStrs::SM_CONNECT_AC_P;

	
	promptId_[testIndex] = SmPromptId::REMOVE_INSP_FILTER_PROMPT;
	promptName_[testIndex++] = PromptStrs::SM_REMOVE_INSP_FILTER_P;

	promptId_[testIndex] = SmPromptId::CONNECT_AIR_PROMPT;
	promptName_[testIndex++] = PromptStrs::SM_CONNECT_AIR_P;

	promptId_[testIndex] = SmPromptId::EV_CAL_PRESSURE_SENSOR_ALERT_PROMPT;
	promptName_[testIndex++] = PromptStrs::SM_EV_CAL_PRESSURE_SENSOR_ALERT_P;

#ifdef SIGMA_DEVELOPMENT

	// Make sure total test count is within the maximum allowed.
	SAFE_CLASS_ASSERTION(testIndex == MAX_CAL_PROMPTS);

	// Do some safe assertions that all entries in the array have been filled
	for (testIndex = 0; testIndex < MAX_CAL_PROMPTS; testIndex++)
	{
		SAFE_CLASS_ASSERTION(promptId_[testIndex] != SmPromptId::NULL_PROMPT_ID);
		SAFE_CLASS_ASSERTION(promptName_[testIndex] != NULL_STRING_ID);
	}
#endif // SIGMA_DEVELOPMENT
											// $[TI1]
}

//============================ m e t h o d   d e s c r i p t i o n ====
//@ Method: processTestPrompt_
//
//@ Interface-Description
//  Process the prompt command given by Service-Mode.
//---------------------------------------------------------------------
//@ Implementation-Description
//  First, we grab the Adjust Panel focus, then we update the prompt area
//  and the status area.  Next, we update the screen display id.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
CalibrationSetupSubScreen::processTestPrompt(Int command)
{
	CALL_TRACE("CalibrationSetupSubScreen::processTestPrompt(Int command)");

#if defined FORNOW
printf("alibrationSetupSubScreen::processTestPrompt() command=%d\n", command);
#endif	// FORNOW

	// Grab Adjust Panel focus
	AdjustPanel::TakeNonPersistentFocus(this, TRUE);

	// Disable the knob sound.
	AdjustPanel::DisableKnobRotateSound();

	// Error checking the prompt received 
	Int idx = 0;
	for (idx = 0;
			 idx < MAX_CAL_PROMPTS && promptId_[idx] != command;
			 idx++);
	SAFE_CLASS_ASSERTION(idx < MAX_CAL_PROMPTS);

	// Set primary prompt to the corresponding message.
	guiTestMgr_.setTestPrompt(PromptArea::PA_PRIMARY, 
								PromptArea::PA_HIGH,
								promptName_[idx]);

	guiTestMgr_.setTestPrompt(PromptArea::PA_ADVISORY,
								PromptArea::PA_HIGH,
								NULL_STRING_ID); 

	switch (keyAllowedId_)
	{
	case SmPromptId::ACCEPT_KEY_ONLY:				// $[TI1]
		guiTestMgr_.setTestPrompt(PromptArea::PA_SECONDARY, 
								PromptArea::PA_HIGH,
								PromptStrs::EXH_V_CAL_PROMPT_ACCEPT_S);
		break;

	case SmPromptId::ACCEPT_AND_CANCEL_ONLY:		 // $[TI2]
		guiTestMgr_.setTestPrompt(PromptArea::PA_SECONDARY, 
								PromptArea::PA_HIGH,
								PromptStrs::EXH_V_CAL_PROMPT_ACCEPT_CANCEL_S);
		break;
	}												// $[TI3]
	



	// Display the overall testing status on the service status area
	calStatus_.setStatus(MiscStrs::EXH_V_CAL_WAITING_MSG);

	// Set the next test status
	testStateId_ = PROMPT_ACCEPT_BUTTON_PRESSED;
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processTestResultStatus
//
//@ Interface-Description
//  Process the test result status given by Service Mode. 
//---------------------------------------------------------------------
//@ Implementation-Description
//  Dispatch the given command and make the corresponding layoutScreen_
//  call to update the screen display.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
CalibrationSetupSubScreen::processTestResultStatus(Int resultStatus, Int testResultId)
{
	CALL_TRACE("CalibrationSetupSubScreen::processTestResultStatus_"
								"(Int resultStatus Int testResultId)");

#if defined FORNOW
printf("alibrationSetupSubScreen::processTestResultStatus() resultStatus=%d\n", resultStatus);
#endif	// FORNOW

	switch (resultStatus)
	{
	case SmStatusId::TEST_END:			// $[TI1]
		nextTest_();
		break;

	case SmStatusId::TEST_START:		// $[TI2]
		break;

	case SmStatusId::TEST_ALERT:		// $[TI3]
	case SmStatusId::TEST_FAILURE:		// $[TI4]
		errorHappened_ = TRUE;
		break;

	case SmStatusId::TEST_OPERATOR_EXIT:// $[TI5]
		errorHappened_ = TRUE;
		break;

	default:
		CLASS_ASSERTION_FAILURE();
		break;
	}
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processTestResultCondition
//
//@ Interface-Description
//  Process test result condition passed by Service-Mode.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Display the text for the corresponding result condition on the screen
//  and update the prompt area.
//---------------------------------------------------------------------
//@ PreCondition
//  Result condition has to be in the range [0, MAX_CONDITION_ID]
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
CalibrationSetupSubScreen::processTestResultCondition(Int resultCondition)
{
	CALL_TRACE("CalibrationSetupSubScreen::processTestResultCondition(Int resultCondition)");

	SAFE_CLASS_ASSERTION((resultCondition >= 0) && (resultCondition <= MAX_CONDITION_ID));

#if defined FORNOW
printf("CalibrationSetupSubScreen::processTestResultCondition() resultCondition=%d\n", resultCondition);
#endif	// FORNOW

	if (resultCondition > 0)
	{						// $[TI1]
		errDisplayArea_[errorIndex_].setText(conditionText_[resultCondition-1]);
		errDisplayArea_[errorIndex_].setShow(TRUE);
		errorIndex_++;

		guiTestMgr_.setTestPrompt(PromptArea::PA_ADVISORY,
				PromptArea::PA_LOW, PromptStrs::EXH_V_CAL_SEE_OP_MANUAL_A);

		guiTestMgr_.setTestPrompt(PromptArea::PA_SECONDARY,
				PromptArea::PA_HIGH, NULL_STRING_ID);
	}						// $[TI2]
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processTestKeyAllowed
//
//@ Interface-Description
//  Process keyAllowed command given by Service-Mode.
//---------------------------------------------------------------------
//@ Implementation-Description
//	For exhalation calibration, this data is ignored.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
CalibrationSetupSubScreen::processTestKeyAllowed(Int command)
{
	CALL_TRACE("CalibrationSetupSubScreen::processTestKeyAllowed(Int command)");

#if defined FORNOW
printf("CalibrationSetupSubScreen::processTestKeyAllowed() command=%d\n", command);
#endif	// FORNOW

	// Remember the expected user key response ID
	keyAllowedId_ = command;
	// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelRestoreFocusHappened
//
//@ Interface-Description
// This is a virtual method inherited from being an AdjustPanelTarget.
// It is normally called when the operator release the off-screen
// keyboard key and the AdjustPanel needs to restore the default
// AdjustPanel target.  It is the duty of this inherited method to restore
// the previous test prompts in order to continue the Calibration
// process.  
//---------------------------------------------------------------------
//@ Implementation-Description
//  Call GuiManager's virtual method to do the job.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
CalibrationSetupSubScreen::adjustPanelRestoreFocusHappened(void)
{
	CALL_TRACE("CalibrationSetupSubScreen::adjustPanelRestoreFocusHappened(void)");

#if defined FORNOW
printf("adjustPanelRestoreFocusHappened at line  %d\n", __LINE__);
#endif	// FORNOW

	guiTestMgr_.restoreTestPrompt();
	// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processTestData
//
//@ Interface-Description
//  Process data given by Service-Mode
//---------------------------------------------------------------------
//@ Implementation-Description
//  Since this subscreen is designed not to display test data,
//  this remains an empty method
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
CalibrationSetupSubScreen::processTestData(Int dataIndex, TestResult *pResult)
{
	CALL_TRACE("CalibrationSetupSubScreen::processTestData(Int dataIndex, TestResult *pResult)");
	// Empty method
	// Exhalation Calibration SubScreen doesn't display test data
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setErrorTable_
//
//@ Interface-Description
// 	Sets the error text in the error table.  Any condition that arises
//	has a unique id which is used as an index into this table of condition
//	text.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Assign condition text (non-cheap text) to error table.  Different 
//	conditions will arise depending whether it's the Exhalation Calibration
//	or the Calibration Info Duplication process.  Therefore, different
//	sets of error conditions are used according to which test is in progress.
//	We also initialize the error display area's show flag.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
CalibrationSetupSubScreen::setErrorTable_(void)
{
	Int testIndex = 0;

	CALL_TRACE("CalibrationSetupSubScreen::setErrorTable_(void)");


#if defined FORNOW
printf("CalibrationSetupSubScreen::setErrorTable_() at line  %d, this = %d\n", __LINE__, this);
#endif	// FORNOW

	// Reset status flag
	errorHappened_ = 0;
	errorIndex_ = 0;


	if (currentTestId_ ==SmTestId::EXH_VALVE_CALIB_TEST_ID)
	{											// $[TI1]
		conditionText_[testIndex++] = MiscStrs::EXH_V_CAL_FLOW_SENSOR_INFO_INVALID;
		conditionText_[testIndex++] = MiscStrs::EXH_V_CAL_NO_AIR_CONNECTED;
		conditionText_[testIndex++] = MiscStrs::EXH_V_CAL_INSP_AUTOZERO_FAILED;
		conditionText_[testIndex++] = MiscStrs::EXH_V_CAL_EXH_AUTOZERO_FAILED;
		conditionText_[testIndex++] = MiscStrs::EXH_V_CAL_INSP_EXH_AUTOZERO_FAILED;
		conditionText_[testIndex++] = MiscStrs::EXH_V_CAL_FS_UNABLE_ESTABLISH_FLOW;
		conditionText_[testIndex++] = MiscStrs::EXH_V_CAL_BAD_FLOW_SENSOR;
		conditionText_[testIndex++] = MiscStrs::EXH_V_CAL_BAD_PRESSURE_SENSOR;
		conditionText_[testIndex++] = MiscStrs::EXH_V_CAL_PS_UNABLE_BUILD_PRESSURE;
		conditionText_[testIndex++] = MiscStrs::EXH_V_CAL_PS_ALERT;
		conditionText_[testIndex++] = MiscStrs::EXH_V_CAL_TEMP_OOR;
		conditionText_[testIndex++] = MiscStrs::EXH_V_CAL_UNABLE_ESTABLISH_FLOW;
		conditionText_[testIndex++] = MiscStrs::EXH_V_CAL_DIAG_UNDEFINED_ERROR;
		conditionText_[testIndex++] = MiscStrs::EXH_V_CAL_FAILED_GAIN_RESOLUTION;
		conditionText_[testIndex++] = MiscStrs::EXH_V_CAL_FAILED_CURR_LIMIT_EXCEEDED;
		conditionText_[testIndex++] = MiscStrs::EXH_V_CAL_BAD_LOOPBACK_CURRENT;
		conditionText_[testIndex++] = MiscStrs::EXH_V_CAL_UNABLE_ESTABLISH_PRESSURE;
		conditionText_[testIndex++] = MiscStrs::EXH_V_CAL_AC_POWER_NOT_CONNECTED;
		conditionText_[testIndex++] = MiscStrs::FS_UNABLE_TO_PROGRAM_FLASH;
		conditionText_[testIndex] = MiscStrs::EXH_V_CAL_DIAG_UNDEFINED_ERROR;
	}
	else if (currentTestId_ ==SmTestId::CAL_INFO_DUPLICATION_ID)
	{                                           // $[TI2]
		conditionText_[testIndex++] = MiscStrs::CAL_INFO_DUP_ERROR_RECEIVE_FLASH_DATA;
		conditionText_[testIndex]   = MiscStrs::CAL_INFO_DUP_ERROR_BURNING_FLASH;
	}
	else
	{
		CLASS_ASSERTION_FAILURE();
	}

	// Make sure total test count is within the maximum allowed.
	CLASS_ASSERTION(testIndex <= MAX_CONDITION_ID);

	for (int i=0; i < MAX_CONDITION_ID; i++)
	{	
		errDisplayArea_[i].setShow(FALSE);
	}	
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: startTest_
//
//@ Interface-Description
//	Preparation step for a test to start.	
//---------------------------------------------------------------------
//@ Implementation-Description
//	Initialize the condition text (error) table.  Display prompts and status.
//	Finally, tell Service Mode Manager to do the test.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
CalibrationSetupSubScreen::startTest_(SmTestId::ServiceModeTestId currentTestId)
{
	CALL_TRACE("CalibrationSetupSubScreen::startTest_(SmTestId::ServiceModeTestId currentTestId)");

#if defined FORNOW
printf("startTest_ at line  %d\n", __LINE__);
#endif	// FORNOW

	currentTestId_ = currentTestId;

	// Set the appropriated error message table.
	setErrorTable_();

	// Set all prompts
	guiTestMgr_.setTestPrompt(PromptArea::PA_PRIMARY,
			PromptArea::PA_LOW, PromptStrs::EXH_V_CAL_TESTING_P);

	guiTestMgr_.setTestPrompt(PromptArea::PA_SECONDARY,
			PromptArea::PA_LOW, NULL_STRING_ID); 

	// Display the overall testing status on the service status area
	calStatus_.setStatus(MiscStrs::EXH_V_CAL_RUNNING_MSG);

	// Start the test.
	SmManager::DoTest(currentTestId);
													// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: nextTest_
//
//@ Interface-Description
//	Handles the test and interface control at the end of a Calibration
//	or Calibration Info Dup test.
//---------------------------------------------------------------------
//@ Implementation-Description
//	The control differs depending what "test" was done prior to this
//	method.  If either the Exh Calibration or the Cal Info Dupl. test 
//	failed, we set it up for the users to restart the test.  If the
//	Calibration test succeeded, we set it up for the Cal Info Duplication 
//	test to begin.  If the Cal InfoDuplication test succeeded, we simply
//	inform the user as such.  The screen goes back to the initial
//	test's pre-start state.  There is a requirement that the EST tab
//	button stays dimmed (flat) as long as Exhalation Calibration test
//  doesn't pass.  It becomes selectable when the test succeeds.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
CalibrationSetupSubScreen::nextTest_(void)
{
	CALL_TRACE("CalibrationSetupSubScreen::nextTest_(void)");

#if defined FORNOW
printf("nextTest_ at line  %d\n", __LINE__);
#endif	// FORNOW

	if (currentTestId_ == SmTestId::EXH_VALVE_CALIB_TEST_ID)
	{											// $[TI1]
		ServiceLowerScreenSelectArea *pServiceLowerScreenSelectArea;
		pServiceLowerScreenSelectArea = ServiceLowerScreen::RServiceLowerScreen.getServiceLowerScreenSelectArea();

		if (errorHappened_)
		{										// $[TI2]
			setupForRestartCalTest_();

			// Update the ExhValveCalRequiredFlag.
			GuiApp::SetExhValveCalSuccessfulFlag(FALSE);
		}
		else
		{                                       // $[TI3]
			// Update the ExhValveCalRequiredFlag.
			GuiApp::SetExhValveCalSuccessfulFlag(TRUE);

#if defined SIGMA_DEVELOPMENT
			// Setup for transfer flash calibration information to GUI board.
			titleArea_.setTitle(MiscStrs::CAL_INFO_DUP_SUBSCREEN_TITLE);
#endif	// SIGMA_DEVELOPMENT

			startTest_(SmTestId::CAL_INFO_DUPLICATION_ID);
		}

		// Popup estTab button.
		(pServiceLowerScreenSelectArea->getEstTestTabButton())->setToUp();

		ServiceUpperScreen::RServiceUpperScreen.getUpperSubScreenArea()->updateVentTestSummaryScreen();
	}
	else if (currentTestId_ == SmTestId::CAL_INFO_DUPLICATION_ID)
	{                                           // $[TI4]
		setupForRestartCalTest_();

#if defined SIGMA_DEVELOPMENT
		// Setup for transfer flash calibration information to GUI board.
		titleArea_.setTitle(MiscStrs::EXH_VALVE_CALIB_SUBSCREEN_TITLE);
#endif	// SIGMA_DEVELOPMENT

		if (errorHappened_)
		{                                       // $[TI5]
			// Display the calibration failed status message.
			calStatus_.setStatus(MiscStrs::CAL_INFO_DUP_FAILED_MSG);

		}
		else
		{                                       // $[TI6]
			// Display the calibration completed status message.
			calStatus_.setStatus(MiscStrs::EXH_V_CAL_COMPLETE_MSG);
		}
	}
	else
	{
		CLASS_ASSERTION_FAILURE();
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setupForRestartCalTest_
//
//@ Interface-Description
//	Prepare the screen for users to restart the test.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Show the Lower Select area and START TEST button; Display the status
//	and prompts.
// $[07008] When the test run finished or is stopped by the technician ...
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
CalibrationSetupSubScreen::setupForRestartCalTest_(void)
{
	// Activate the lower screen 's select area.
	ServiceLowerScreen::RServiceLowerScreen.getServiceLowerScreenSelectArea()->setBlank(FALSE);

	// Display the calibration failed status message.
	calStatus_.setStatus(MiscStrs::EXH_V_CAL_FAILED_MSG);

	// Set primary prompt to start test.
	guiTestMgr_.setTestPrompt(PromptArea::PA_PRIMARY,
					PromptArea::PA_LOW, 
					PromptStrs::EXH_V_CAL_TOUCH_START_TEST_TO_BEGIN_P);

	// Erase the advisory prompt
	guiTestMgr_.setTestPrompt(PromptArea::PA_ADVISORY,
					PromptArea::PA_HIGH,
					NULL_STRING_ID);

	// Set the secondary prompt to To cancel touch other screen.
	guiTestMgr_.setTestPrompt(PromptArea::PA_SECONDARY,
					PromptArea::PA_LOW, 
					PromptStrs::OTHER_SCREENS_CANCEL_S);

	// Redisplay start button.
	startTestButton_.setShow(TRUE);

	currentTestId_ = SmTestId::TEST_NOT_START_ID;
													// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
CalibrationSetupSubScreen::SoftFault(const SoftFaultID  softFaultID,
							 const Uint32       lineNumber,
							 const char*        pFileName,
							 const char*        pPredicate)  
{
	CALL_TRACE("CalibrationSetupSubScreen::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS,
							CALIBRATIONSETUPSUBSCREEN,
							lineNumber, pFileName, pPredicate);
}


