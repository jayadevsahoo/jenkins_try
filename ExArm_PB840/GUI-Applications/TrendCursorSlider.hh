#ifndef TrendCursorSlider_HH
#define TrendCursorSlider_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: TrendCursorSlider - A slider for setting the trend cursor
// 							  position
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/TrendCursorSlider.hhv   25.0.4.0   19 Nov 2013 14:08:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002   By: gdc    Date: 26-Jan-2009    SCR Number: 6407
//  Project:  840S
//  Description:
//      Modified to bring source into strict ARM compliance.
//
//  Revision: 001  By:  gdc    Date: 26-Feb-2007   SCR Number: 6245
//  Project:  Trend
//  Description:
//  	Initial version.
//====================================================================

#include "Container.hh"

//@ Usage-Classes
#include "TextField.hh"
#include "CursorSettingButton.hh"
#include "ButtonTarget.hh"
#include "Line.hh"
#include "Box.hh"
#include "Triangle.hh"
#include "SettingObserver.hh"
#include "TrendEventTarget.hh"
//@ End-Usage

class TrendDataSetAgent;
class TrendDataSet;

class TrendCursorSlider
:   public Container, 
	public SettingObserver, 
	public ButtonTarget,
	public TrendEventTarget
{
public:

	TrendCursorSlider(SubScreen* pFocusSubScreen);
	~TrendCursorSlider(void);

	// Drawable virtual
	virtual void activate(void);
	virtual void deactivate(void);

	// SettingObserver virtual 
	virtual void  valueUpdate(const Notification::ChangeQualifier qualifierId,
							  const SettingSubject*               pSubject);

	// ButtonTarget virtual 
	virtual void buttonDownHappened(Button *pButton,
									Boolean byOperatorAction);

	virtual void buttonUpHappened(Button *pButton,
								  Boolean byOperatorAction);

	// TrendEventTarget virtual
	virtual void trendDataReady(TrendDataSet& rTrendDataSet);

	inline void setShowCursorLine(Boolean showCursorLine);

	inline Boolean isCursorActive(void) const;
	inline Boolean isCursorLineShown(void) const;

	inline void setCursorLineLength(Uint32 lineLength);

	void setShowAxis(Boolean showAxis);
	void disableCursor(void);
	void enableCursor(void);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:

	private:
	TrendCursorSlider(void);					 // not implemented...
	TrendCursorSlider(const TrendCursorSlider&); // not implemented...
	void operator=(const TrendCursorSlider&);	 // not implemented...

	void updateCursor_(void);

	void updateXAxis_(const TrendDataSet& rTrendDataSet);

	enum
	{
		MAX_TICK_MARKS_ = 20
	};

	//@ Data-Member: sliderBox_;
	// The horizontal "box" below the cursor button and pointer. In its
	// diminuative state, it's a line.
	Box sliderBox_;

	//@ Data-Member: cursorLine_;
	// The vertical line that overlays the graphs
	Line cursorLine_;

	//@ Data-Member: cursorButtonCntnr_
	// A container for the cursor setting button and pointer triangle
	Container cursorButtonCntnr_;

	//@ Data-Member: cursorSettingButton_
	// The cursor setting button.
	CursorSettingButton cursorSettingButton_;

	//@ Data-Member: cursorTriangle_
	// The triangle used to point to the cursor's current position.
	Triangle cursorTriangle_;

	//@ Data-Member: showCursorLine_
	// Flag to show/hide the cursor line that overlays the trend graphs
	Boolean showCursorLine_;

	//@ Data-Member: isCursorPressed_
	// TRUE when the cursor button is pressed
	Boolean isCursorPressed_;

	//@ Data-Member: isCursorLineShown
	// TRUE when the cursor line is displayed over the graphs
	Boolean isCursorLineShown_;

	//@ Data-Member: axisContainer_
	// Contains the drawables for the X axis (time axis)
	Container axisContainer_;

	//@ Data-Member: tickMark_
	// Tick marks along X axis to delineate time divisions
	Line tickMark_[MAX_TICK_MARKS_];

	//@ Data-Member: timeLabels_
	// The time labels on the x axis.
	TextField timeLabels_[MAX_TICK_MARKS_];

	//@ Data-Member: dateLabels_
	// The date labels on the x axis.
	TextField dateLabels_[MAX_TICK_MARKS_];

	//@ Data-Member: rTrendDataSetAgent_
	// The data set agent through which all trend data base requests are
	// mediated. Used by the slider to access the timestamp information
	// from the latest dataset.
	TrendDataSetAgent& rTrendDataSetAgent_;

	//@ Data-Member: CURSOR_BUTTON_WIDTH_
	// The width of the cursorSettingButton_ at construction.
	// This declaration must appear after the cursorSettingButton_
	// declaration to be properly initialized.
	const Int32 CURSOR_BUTTON_WIDTH_;

	//@ Data-Member: CURSOR_BUTTON_HEIGHT_
	// The height of the cursorSettingButton_ at construction
	// This declaration must appear after the cursorSettingButton_
	// declaration to be properly initialized.
	const Int32 CURSOR_BUTTON_HEIGHT_;

};

#include "TrendCursorSlider.in"

#endif // TrendCursorSlider_HH 
