//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file UILogger.h
//----------------------------------------------------------------------------

#ifndef UILOGGER_H
#define UILOGGER_H

#include "ILogger.h"

class UILogger:public ILogger
{
    public:
    UILogger();

    ~UILogger();

    
    virtual void logMessage(unsigned int line, int logtype, char *filename,
            char *str, ...);


    void disable();
    void disable(int);
    void enable();
    void enable(int);
    bool is_enabled(int);

    private:

    bool bLM_DEBUG;     
    bool bLM_INFO;      
    bool bLM_TRACE;     
    bool bLM_NOTICE;    
    bool bLM_WARNING;    
    bool bLM_ERROR;     
    bool bLM_CRITICAL;   
    bool bLM_ALERT;     
    bool bLM_GENERAL;

};


#endif

