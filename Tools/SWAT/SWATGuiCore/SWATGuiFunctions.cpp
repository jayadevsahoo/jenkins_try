#include "StdAfx.h"
#include <cstdlib>
#include "SWATGuiFunctions.h"
#include "GuiIOServer.h"

const char SWATGuiFunctions::className[] = "swat_guicore";

const Luna<SWATGuiFunctions>::RegType SWATGuiFunctions::Register[] = {
	{ "push_xy", &SWATGuiFunctions::simulate_touch },
	{ "bezel_key_press", &SWATGuiFunctions::simulate_bezel_key },
	{ "knob", &SWATGuiFunctions::simulate_knob },
	{ "screen_capture", &SWATGuiFunctions::capture_screen },
	{ "screen_capture_buffer_info", &SWATGuiFunctions::query_screen_capture_buffer_info },
	{ "clear_screens", &SWATGuiFunctions::clear_screens },
	{ "query_sc_ftp_addr", &SWATGuiFunctions::query_screenshot_path },
	{ "init", &SWATGuiFunctions::init },
	{ "connect", &SWATGuiFunctions::connect },
	{ "disconnect", &SWATGuiFunctions::disconnect },
	{ "sleep", &SWATGuiFunctions::sleep },
	{ 0 }
};


SWATGuiFunctions::SWATGuiFunctions(lua_State * L) : connected_( false )
{
	strcpy_s( ipaddr_, "" );
	port_ = 0;
}


SWATGuiFunctions::~SWATGuiFunctions(void)
{
}

bool SWATGuiFunctions::CheckConnection()
{
	if ( ! connected_ )
	{
		if ( GuiIOConnect( ipaddr_, port_ ) == GUIIO_SUCCESS )
		{
			connected_ = true;
		}
	}

	return connected_;
}

int SWATGuiFunctions::simulate_touch(lua_State * L)
{
	// get x, y from stack
	char x[16];
	strcpy_s(x, lua_tostring(L, -2));
	
	char y[16];
	strcpy_s(y, lua_tostring(L, -1));

	CheckConnection();

	GuiIOError error = GuiIOSimulateTouch( atoi(x), atoi(y) );

	lua_pushnumber(L, error);
	
	return 1;
}

int SWATGuiFunctions::simulate_bezel_key(lua_State * L)
{
	char keyId[16];
	strcpy_s(keyId, lua_tostring(L, -2));
	
	char onOff[16];
	strcpy_s(onOff, lua_tostring(L, -1));

	CheckConnection();

	GuiIOError error = GuiIOBezelKeyPress( atoi(keyId), (atoi(onOff) != 0) );

	lua_pushnumber(L, error);

	return 1;
}

int SWATGuiFunctions::simulate_knob(lua_State * L)
{
	char delta[32];
	strcpy_s(delta, lua_tostring(L, -1));

	CheckConnection();

	GuiIOError error = GuiIOKnob( atoi(delta) );

	lua_pushnumber(L, error);

	return 1;
}

int SWATGuiFunctions::capture_screen(lua_State * L)
{
	char fileName[260];
	strcpy_s( fileName, lua_tostring(L, -1) );
	
	CheckConnection();
	GuiIOError error = GuiIOScreenCapture( fileName );

	lua_pushnumber(L, error);

	return 1;
}

int SWATGuiFunctions::query_screen_capture_buffer_info(lua_State * L)
{
	GuiIOScreenCaptureBufferInfoResp resp;
	memset( &resp, 0, sizeof(resp) );

	CheckConnection();
	GuiIOError error = GuiIOGetScreenCaptureBufferInfo( &resp );
	lua_pushnumber(L, error);
	int nParams = 1;

	if ( error == GUIIO_SUCCESS )
	{
		lua_pushnumber( L, resp.bufferSize );
		
		nParams += 1;
	}

	return nParams;
}

int SWATGuiFunctions::clear_screens(lua_State * L)
{
	CheckConnection();
	GuiIOError error = GuiIOClearScreenshots();
	lua_pushnumber(L, error);

	return 1;
}

int SWATGuiFunctions::query_screenshot_path(lua_State * L)
{
	char index[32];
	strcpy_s( index, lua_tostring(L, -1) );

	GuiIOQueryScreenshotPathResp resp;
	memset( &resp, 0, sizeof(resp) );

	CheckConnection();
	GuiIOError error = GuiIOQueryScreenshotPath( atoi(index), &resp);

	lua_pushnumber(L, error);
	int nParams = 1;

	if ( error == GUIIO_SUCCESS )
	{
		lua_pushstring( L, resp.address );
		
		nParams += 1;
	}

	return nParams;
}


int SWATGuiFunctions::sleep(lua_State * L)
{
	char ms[32];
	strcpy_s(ms, lua_tostring(L, -1));

	Sleep( atoi(ms) );

	return 1;
}

int SWATGuiFunctions::init( lua_State* L )
{
	strcpy_s( ipaddr_, lua_tostring(L, -2) );

	char port[32];
	strcpy_s( port, lua_tostring(L, -1) );
	port_ = atoi( port );

	bool connected = CheckConnection();

	lua_pushboolean( L, connected );

	return 1;
}

int SWATGuiFunctions::connect(lua_State * L)
{
	bool connected = CheckConnection();

	lua_pushboolean( L, connected );

	return 1;
}

int SWATGuiFunctions::disconnect(lua_State * L)
{
	connected_ = false;

	GuiIOError error = GuiIODisconnect();

	lua_pushnumber(L, error);

	return 1;
}