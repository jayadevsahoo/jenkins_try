#ifndef BdSignal_HH
#define BdSignal_HH
//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sotred in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: BdSignal - Set of signals sent over the ethernet.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/BdSignal.hhv   25.0.4.0   19 Nov 2013 13:59:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By:  syw    Date:  18-Sep-2000    DR Number: 5695
//       Project:  VTPC
//       Description:
//             Initial version
//
//=====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"

//@ Usage-Classes

//@ End-Usage

static const Int32 MAX_SIZE = 10 ;
static const Int32 MAX_SIGNALS = 18 ;

typedef struct {
	Real32 signal[MAX_SIGNALS][MAX_SIZE] ;
} SmartPacket ;

class BdSignal
{
  public:
    static void Collect(void) ;
    static void Send(void) ;
    static void SoftFault( const SoftFaultID softFaultID,
						   const Uint32      lineNumber,
						   const char*       pFileName  = NULL, 
						   const char*       pPredicate = NULL) ;

  protected:

  private:
    static SmartPacket SData_[2] ; 
    static Int32 SDataCollectIndex_ ;
    static Int32 SDataSendIndex_ ;
    static Int32 Index_ ;
    static Boolean IsTransmitInProg_ ;
    
    BdSignal(void) ;					// Declared but not implemented
    ~BdSignal(void) ;					// Declared but not implemented
    BdSignal(const BdSignal&) ;         // Declared but not implemented
    void operator=(const BdSignal&) ;   // Declared but not implemented

};


#endif // BdSignal_HH 
