#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1994-1995, Puritan-Bennett Corporation
//=====================================================================  

// =========================== M O D U L E   D E S C R I P T I O N ====
//@ Class: VgaPoint - VGA point drawing routines.
//---------------------------------------------------------------------
//@ Interface-Description
//  This module contains the point drawing routines for the VGA graphics
//  library.
//---------------------------------------------------------------------
//@ Rationale
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ $Header:   /840/Baseline/VGA-Graphics-Driver/vcssrc/VgaPoint.ccv   25.0.4.0   19 Nov 2013 14:37:58   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003   By: gdc    Date:  28-Aug-2000    DCS Number: 5493
//  Project: GuiComms
//  Description:
//	Implemented single screen compatibility.
//
//  Revision: 002   By: gdc    Date:  27-Jan-2000    DCS Number: 5337
//  Project: 840 Neonatal
//  Description:
//	Optimized VGA library for Neonatal project to use BitBlt and Windows
//  Bitmap structures.
//
//  Revision: 001   By: sah   Date: 01-Aug-1997   DCS Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//      Initial version to coding standards (Integration baseline).
//
//=====================================================================  
#include "VgaPoint.hh" 

//@ Usage-Classes
#include "VgaDevice.hh" 
#include "DisplayContext.hh"
#include "GraphicsContext.hh"
//@ End-Usage

//=====================================================================  
//
//  Public Methods...
//
//=====================================================================  

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: DrawPoint
//
//@ Interface-Description
//  Draw a point at the (x,y) position in color "color", clipped to the
//  clipping rectangle.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses VGA Video Mode 79 (640x480, 256 color, packed pixels).
//  Writes directly to display memory.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================   

void
VgaPoint::DrawPoint(
	DisplayContext & rDc, GraphicsContext & rGc, Int x, Int y )
{

    Byte uColor = rGc.getForegroundColor();
	RgbQuad rgb;
	rDc.getColor(uColor,rgb);
	COLORREF crWndCol = RGB(rgb.rgbRed,rgb.rgbGreen,rgb.rgbBlue);
	rDc.getCDC()->SetPixel(x,y,crWndCol);
 
}

#if defined(SIGMA_UNIT_TEST)

//============================ M E T H O D   D E S C R I P T I O N ====
// Method: ReadPoint
//
// Interface-Description
//  This routine returns the color of a point (pixel) at the specified
//  location on the display screen.
//---------------------------------------------------------------------
// Implementation-Description
//  uses VGA read mode 0
//  sets up the VGA control registers for pixel bit plane read,
//  inputs bits from data of the 4 planes at pixel address,
//  isolate the "color" of the pixel bit and combine it into the color,
//  returns the "color" read
// ---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================   

Uint8
VgaPoint::ReadPoint(const DisplayContext & rDc, Int x, Int y)
{
  Byte * pDisplayByte = rDc.getDisplayFramePtr();
  const Uint BYTES_PER_ROW = rDc.getWidth();

  Uint8  color = 0;

  if (x >= 0 && x < rDc.getWidth() && y >= 0 && y < rDc.getHeight() )
  {
    pDisplayByte += y * BYTES_PER_ROW + x;
	rDc.accessDisplay();
	color = *pDisplayByte;
  }

  return color;
}

#endif // defined(SIGMA_UNIT_TEST)


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
VgaPoint::SoftFault(const SoftFaultID softFaultID,
		    const Uint32      lineNumber,
		    const char*       pFileName,
		    const char*       pPredicate)
{
  FaultHandler::SoftFault(softFaultID, VGA_GRAPHICS_DRIVERS,
			  VgaGraphicsDriver::VGA_POINT_ID, 
			  lineNumber, pFileName, pPredicate);
}
