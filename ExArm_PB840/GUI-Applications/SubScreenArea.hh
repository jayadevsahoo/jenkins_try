#ifndef SubScreenArea_HH
#define SubScreenArea_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: SubScreenArea - An area in which subscreens can be
// displayed.  Both the Upper and the Lower screens contain SubScreenArea's.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/SubScreenArea.hhv   25.0.4.0   19 Nov 2013 14:08:32   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005  By:  yyy	   Date:  29-Jun-1998    DCS Number: 5327
//  Project:  NeoMode
//  Description:
//		Initial version.
//
//  Revision: 004  By:  gdc    Date:  01-Oct-1998    DCS Number: 5192
//  Project:  BiLevel
//  Description:
//      Reverted to code used prior to Color version since the BiLevel
//      implementation for the maneuver sub-screen was incompatible with 
//      the Color implementation of this class. Added the method 
//      restorePreviousSubScreen to implement the functionality to restore 
//		the sub-screen that was active prior to activating a default sub-screen.
//
//  Revision: 003  By: yyy      Date: 05-Jan-1998  DR Number: 
//    Project:  BiLevel (R8027)
//    Description:
//     Initial release for BiLevel adding software option.
//     Merged /840/Baseline/GUI-Applications/vcssrc/SubScreenArea.hhv   1.4.1.0   07/30/98 10:22:14   gdc
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "Container.hh"
#include "KeyPanelTarget.hh"

//@ Usage-Classes
#include "GuiAppClassIds.hh"
//@ End-Usage

class SubScreen;
class SubScreenButton;

class SubScreenArea : public Container, public KeyPanelTarget
{
public:
	SubScreenArea();
	~SubScreenArea(void);

	virtual void activateSubScreen(SubScreen *pSubScreen,
									SubScreenButton *pButton,
									Boolean activatedByOperator = FALSE);

	virtual void deactivateSubScreen(void);

	virtual void refreshActiveSubScreen(void);

	void setDefaultSubScreen(SubScreen *pDefaultSubScreen,
								SubScreenButton *pDefaultButton = NULL);

	void activateDefaultSubScreen(void);

	void deactivateDefaultSubScreen(void);
	void restorePreviousSubScreen(void);
	void resetCurrentSubScreenButton(void);
									
	inline Boolean isCurrentSubScreen(SubScreen* pSubScreen) const;
	inline Boolean isSubScreenActive(void);
	inline Boolean isActivatedByOperator(void) const;
	inline const SubScreenButton *getCurrentSubScreenButton(void) const;
	
	static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL, 
			  const char*       pPredicate = NULL);

protected:

	// these methods are purposely declared, but not implemented...
	SubScreenArea(const SubScreenArea&);		// not implemented...
	void   operator=(const SubScreenArea&);	// not implemented...

	//@ Data-Member: pCurrentSubScreen_
	// This is a pointer to the subscreen which is currently active,
	// NULL if no subscreen is active.  If active, this subscreen
	// will be in this container's drawable list.
	SubScreen		*pCurrentSubScreen_;

	//@ Data-Member: pDefaultSubScreen_
	// This is the subscreen which is shown when there are no
	// other subscreens occupying the subscreen area.  It is normally
	// NULL but it is used for displaying the Apnea Ventilation
	// subscreen and Vent Setup subscreen (when in Resume mode).
	SubScreen		*pDefaultSubScreen_;

	//@ Data-Member: pCurrentButton_
	// The current button is the SubScreenButton which activated the
	// current subscreen.  It is stored in order to implement
	// exclusivity of subscreen buttons.  When a subscreen button
	// attempts to activate a subscreen we first look at pCurrentButton_
	// to see if it is set.  If it is we set it to up and then store the
	// new current button, thereby implementing exclusivity of subscreen
	// buttons.
	SubScreenButton *pCurrentButton_;

	//@ Data-Member: pDefaultButton_
	// This is the SubScreenButton associated with the Default
	// subscreen.  If it is non-NULL then it is pressed down
	// automatically whenever the default subscreen is activated
	// and pressed up when the default subscreen is deactivated.
	SubScreenButton *pDefaultButton_;

	//@ Data-Member: activatedByOperator_
	// Set TRUE by activateSubScreen() if the current subscreen was 
	// activated by operator action, otherwise FALSE.
	Boolean activatedByOperator_;

    //@ Data-Member: pPreviousSubScreen_
    // This is the subscreen which was active before the default
    // subscreen was activated. It returns as the active subscreen
    // when the default subscreen is deactivated if the default
    // subscreen is active.
    SubScreen       *pPreviousSubScreen_;

    //@ Data-Member: pPreviousButton_
    // This is the SubScreenButton associated with the active
    // subscreen before the default subscreen is activated. It
    // is pressed down again after the default subscreen is deactivated.
    SubScreenButton *pPreviousButton_;
 
    //@ Data-Member: previousActivatedByOperator_
    // Saves the state of activatedByOperator_ prior to activating
    // the default sub-screen. Used to restore state after deactivating
    // the default sub-screen.
    Boolean previousActivatedByOperator_;
 
private:

};

// Inlined methods
#include "SubScreenArea.in"

#endif // SubScreenArea_HH 
