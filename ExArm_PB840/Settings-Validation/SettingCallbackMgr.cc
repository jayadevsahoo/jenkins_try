#include "stdafx.h"

#if defined(SIGMA_BD_CPU)  ||  defined(SIGMA_COMMON)

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sotred in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//	      Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class:  SettingCallbackMgr - Manager of callbacks to external subsystems.
//---------------------------------------------------------------------
//@ Interface-Description
//  This class provides general callback functionality to allow other
//  subsystems to be notified of setting changes that occur withing the
//  Pending Context (BD CPU only).  The client can register for notifications
//  for these specific setting changes.  Each time a setting change callback
//  is activated, the setting id is passed with the call of the callback.
//  Therefore, clients of this subsystem can keep track of which setting
//  value changes within the Pending Context.
//
//  To manage the cases where multiple clients want to register for the
//  same setting event, each event can hold up to 'MAX_ALLOWABLE_CALLBACKS'
//  callback pointers.  Therefore, 'MAX_ALLOWABLE_CALLBACKS' defines the
//  maximum number of callbacks that can be registered for a particular
//  setting event.
//
//  Once a callback has been registered, this class provides no means of 
//  de-registering that callback.  However, it is valid to register a "null"
//  function -- it, obviously, will never be activated.
//---------------------------------------------------------------------
//@ Rationale
//  By providing this functionality, the Settings-Validation Subsystem
//  does not need to "know" who needs to be notified of settings changes;
//  all notification is done through pointers to functions.
//---------------------------------------------------------------------
//@ Implementation-Description
//  A multi-dimensional, static array is used to manage the callback
//  pointers.  The array, 'ArrCallbackPtrs_', contains the callback
//  pointers for the SETTING callbacks.  This array is of two dimensions:
//  one for each of the batch, BD settings, and one for the maximum number
//  of callbacks for each setting ('MAX_ALLOWABLE_CALLBACKS').
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//  There is no practical way for this class to ensure that all of the
//  function callback pointers passed to this class are indeed valid; it
//  is left as the client's responsibility to ensure the validity of
//  the callback pointers.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/SettingCallbackMgr.ccv   25.0.4.0   19 Nov 2013 14:27:42   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  obsoleted this class from use on the GUI; new observer/subject
//	   mechanism now used on the GUI
//	*  as part of the GUI obsolescence, removed unneeded methods and
//	   array dimensions
//
//  Revision: 003   By: sah  Date:  23-Nov-1998    DR Number: 5238
//  Project:  BILEVEL
//  Description:
//	Added method for clearing of all registered changes.
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//  
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//=====================================================================

#include "SettingCallbackMgr.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Static Member Definitions...
//
//=====================================================================

SettingCallbackMgr::SettingCallbackPtr
      SettingCallbackMgr::ArrCallbackPtrs_[::NUM_BD_SETTING_IDS]
			         [SettingCallbackMgr::MAX_ALLOWABLE_CALLBACKS];


//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Class-Method:  Initialize()  [static]
//
//@ Interface-Description
//  Initialize this class's callback pointers to 'NULL'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
SettingCallbackMgr::Initialize(void)
{
  CALL_TRACE("Initialize()");

  Uint  settingIdx, callbackIdx;

  // initialize ALL of the callback pointers to 'NULL'...
  for (settingIdx = 0u; settingIdx < ::NUM_BD_SETTING_IDS; settingIdx++)
  {
    for (callbackIdx = 0u;
	 callbackIdx < SettingCallbackMgr::MAX_ALLOWABLE_CALLBACKS;
	 callbackIdx++)
    {
      // initialize the SETTING CALLBACK pointer for the setting identified
      // by 'settingIdx'  and the callback identified by 'callbackIdx', to
      // 'NULL'...
      ArrCallbackPtrs_[settingIdx][callbackIdx] = NULL;
    }
  }
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Class-Method:  RegisterSettingCallback(settingId, pNewCallback)
//
//@ Interface-Description
//  Register 'pNewCallback' for activation upon changes to the setting
//  identified by 'setting' that is within the Pending Context.  If that
//  setting changes, the callback given by 'pNewCallback' is activated with
//  the id of the setting that was affected.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This iterates through the callback pointers of the setting identified
//  by 'settingId', searching for a "null" pointer.  When a null callback
//  pointer is found, 'pNewCallback' is stored in that location.
//
//  WARNING:  The implementation of 'ActivateSettingCallback()' is dependent
//	      on the implementation of this method.
//---------------------------------------------------------------------
//@ PreCondition
//  (SettingId::IsBatchBdId(settingId))
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
SettingCallbackMgr::RegisterSettingCallback(
			const SettingId::SettingIdType         settingId,
			SettingCallbackMgr::SettingCallbackPtr pNewCallback
		  			   )
{
  CALL_TRACE("RegisterSettingCallback(settingId, pNewCallback)");
  AUX_CLASS_PRE_CONDITION((SettingId::IsBdId(settingId)), settingId);

  Uint  callbackIdx;

  for (callbackIdx = 0u;
       callbackIdx < SettingCallbackMgr::MAX_ALLOWABLE_CALLBACKS;
       callbackIdx++)
  {   // $[TI1] -- this path ALWAYS executes at least once...
    if (ArrCallbackPtrs_[settingId][callbackIdx] == NULL)
    {   // $[TI1.1]
      // found an unitialized callback location, therefore, store the new
      // callback here, and break out of this loop...
      ArrCallbackPtrs_[settingId][callbackIdx] = pNewCallback;
      break;
    }   // $[TI1.2]
  }

  // assert that 'MAX_ALLOWABLE_CALLBACKS' is big enough to allow ALL needed
  // callbacks to be registered.
  CLASS_ASSERTION((callbackIdx < SettingCallbackMgr::MAX_ALLOWABLE_CALLBACKS));
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Class-Method:  ActivateSettingCallback(settingId)  [static]
//
//@ Interface-Description
//  Activate all callbacks, if any, that are registered for changes of the
//  setting identified by 'settingId'.  When activated, the id of the
//  setting that changed ('settingId') is passed to the registered callbacks.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Due to the implementation of the registration, as soon as a "null"
//  callback pointer is detected, no more callbacks will be found.
//  Therefore, the implementation of this method iterates until the first
//  "null" callback pointer -- or until no more callback pointers -- and
//  activates each callback along the way.
//---------------------------------------------------------------------
//@ PreCondition
//  (SettingId::IsBdId(settingId))
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
SettingCallbackMgr::ActivateSettingCallback(
				    const SettingId::SettingIdType settingId
				           )
{
  CALL_TRACE("ActivateSettingCallback(settingId)");
  AUX_CLASS_PRE_CONDITION((SettingId::IsBdId(settingId)), settingId);

  Uint  callbackIdx;

  for (callbackIdx = 0u;
       callbackIdx < SettingCallbackMgr::MAX_ALLOWABLE_CALLBACKS  &&
	   ArrCallbackPtrs_[settingId][callbackIdx] != NULL;
       callbackIdx++)
  {   // $[TI1] -- at least one registered callback...
    // activate the setting callback...
    (*(ArrCallbackPtrs_[settingId][callbackIdx]))(settingId);
  }   // $[TI2] -- no registered callbacks...
}


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
SettingCallbackMgr::SoftFault(const SoftFaultID  softFaultID,
			      const Uint32       lineNumber,
			      const char*        pFileName,
			      const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
			  SETTING_CALLBACK_MGR, lineNumber, pFileName,
			  pPredicate);
}


#endif // defined(SIGMA_BD_CPU)  ||  defined(SIGMA_COMMON)
