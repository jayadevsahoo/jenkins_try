 
#ifndef DaySetting_HH
#define DaySetting_HH

//====================================================================
//
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  DaySetting - Day Setting.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/DaySetting.hhv   25.0.4.0   19 Nov 2013 14:27:20   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005   By: sah  Date:  18-Jun-1999    DR Number:  5440
//  Project:  ATC
//  Description:
//      Eliminated 'isChanged()' method, and added 'resetForcedChangeFlag()',
//      to do nothing.
//
//  Revision: 004   By: sah  Date:  15-Jun-1999    DR Number:  5429
//  Project:  ATC
//  Description:
//	Now overridding 'isChanged()' to ALWAYS return 'TRUE', thereby
//      forcing highlight mode.
//
//  Revision: 003   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'getApplicability()' method
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Initial version.
//  
//====================================================================

//@ Usage-Classes
#include "BatchSequentialSetting.hh"
#include "SequentialRange.hh"
//@ End-Usage


class DaySetting : public BatchSequentialSetting 
{
  public:
    DaySetting(void);
    virtual ~DaySetting(void);

    virtual void  resetForcedChangeFlag(void);

    virtual Applicability::Id  getApplicability(
			    const Notification::ChangeQualifier qualifierId
					       ) const;

    virtual SettingValue  getNewPatientValue(void) const;

    virtual const BoundStatus*  acceptPrimaryChange(
				   const SettingId::SettingIdType primaryId
						   );

#if defined(SIGMA_DEVELOPMENT)
    virtual Boolean  isAcceptedValid(void) const;
#endif  // defined(SIGMA_DEVELOPMENT)

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL, 
			   const char*       pPredicate = NULL);

  protected:
    virtual void  updateConstraints_(void);

  private:
    DaySetting(const DaySetting&);   // not implemented...
    void  operator=(const DaySetting&);	     // not implemented...

    SequentialValue  getMaxDayValue_(void) const;

    //@ Data-Member:  sequentialRange_
    // Sequential range manager.
    SequentialRange  sequentialRange_;
};


#endif // DaySetting_HH 
