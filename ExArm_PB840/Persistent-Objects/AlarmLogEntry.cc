#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sotred in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//============================ M O D U L E   D E S C R I P T I O N ====
//@ Class: AlarmLogEntry - Entry for an Alarm History Log.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is an entry item for the Alarm History Log.  Instances of
//  this class contain alarm event information, and a time-stamp for
//  the time of the event.
//---------------------------------------------------------------------
//@ Rationale
//  This class provides an interface between the definition of an
//  alarm event ('AlarmAction'), and the requirements for an entry
//  of the Alarm History Log.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class is used only by the Alarm History Log, and has no
//  multi-tasking issues.
//
//  This class uses a CRC value to ensure the integrity of its
//  "non-cleared" instances.
//
//  $[00628] -- alarm diagnostic information...
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Persistent-Objects/vcssrc/AlarmLogEntry.ccv   25.0.4.0   19 Nov 2013 14:18:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By:  sah    Date:  01-Jan-1999    DCS Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' to a non-inlined method.
//
//  Revision: 001  By:  sah    Date:  15-Mar-1997    DCS Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//          Initial version (Integration baseline).
//
//=====================================================================

#include "AlarmLogEntry.hh"

//@ Usage-Classes...
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  AlarmLogEntry()  [Default Constructor]
//
//@ Interface-Description
//  Construct a default log entry.
//---------------------------------------------------------------------
//@ Implementation-Description
//  'paddingBlock_' is set to '0xff' to ensure that '0' won't be a valid
//  CRC value.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  (isClear())
//@ End-Method
//=====================================================================

AlarmLogEntry::AlarmLogEntry(void)
			     : paddingBlock_(0xff),
			       entryCrc_(0u)  // some illegal value...
{
  CALL_TRACE("AlarmLogEntry()");

  // make sure that there is no unexpected "padding" between the data
  // members of this class...
  SAFE_AUX_CLASS_ASSERTION((sizeof(AlarmLogEntry) ==
	(sizeof(alarmAction_) + sizeof(paddingBlock_) + sizeof(entryCrc_))),
			   sizeof(AlarmLogEntry));

  // clear this entry...
  clear();

  SAFE_CLASS_POST_CONDITION((isClear()), AlarmLogEntry);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ~AlarmLogEntry()  [Destructor]
//
//@ Interface-Description
//  Destroy this log entry.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

AlarmLogEntry::~AlarmLogEntry(void)
{
  CALL_TRACE("~AlarmLogEntry()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  correct()
//
//@ Interface-Description
//  Set this entry to a "corrected" state.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition 
//  (isCorrected())
//@ End-Method 
//===================================================================== 

void
AlarmLogEntry::correct(void)
{
  CALL_TRACE("correct()");

  AlarmAction  corruptedAlarmAction;

  corruptedAlarmAction.systemLevelId = AlarmAction::CORRECTED_ENTRY;

  logAction(corruptedAlarmAction);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  logAction(newAlarmAction)
//
//@ Interface-Description
//  Log the occurence of the alarm event indicated by 'newAlarmAction'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  (!isClear()  &&  isValid())
//@ End-Method
//=====================================================================

void
AlarmLogEntry::logAction(const AlarmAction& newAlarmAction)
{
  CALL_TRACE("logAction(newAlarmAction)");

  // initialize this alarm log entry with the given alarm event...
  alarmAction_ = newAlarmAction;  // store the alarm event...

  // calculate, and store, this entry's CRC value...
  entryCrc_ = ::CalculateCrc((const Byte*)this, getCrcOffset_());

  SAFE_AUX_CLASS_POST_CONDITION((!isClear()  &&  isValid()), 
  				!isClear(), AlarmLogEntry);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                          [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//  and 'lineNumber' are essential pieces of information.  The
//  'pFileName' and 'pPredicate' strings may be defaulted in the macro
//  to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
AlarmLogEntry::SoftFault(const SoftFaultID  softFaultID,
                         const Uint32       lineNumber,
                         const char*        pFileName,
                         const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, PERSISTENT_OBJS, ALARM_LOG_ENTRY,
                          lineNumber, pFileName, pPredicate);
}


//=====================================================================
//
//  Friend Functions...
//
//=====================================================================

#if defined(SIGMA_DEVELOPMENT)

#include "Ostream.hh"

//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
// Free-Function: operator<<(ostr, logEntry)  [friend]
//
// Interface-Description
//  Dump the contents of 'logEntry' out to 'ostr'.
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Free-Function
//=====================================================================

ostream&
operator<<(ostream& ostr, const AlarmLogEntry& logEntry)
{
  CALL_TRACE("operator<<(ostr, logEntry)");

  if (!logEntry.isClear())
  {
    if (logEntry.isCorrected())
    {
      ostr << "{CORRECTED}" << endl;
    }
    else
    {
      ostr << "{"   << logEntry.alarmAction_ << "}" << endl;
    }
  }
  else
  {
    ostr << "{CLEARED}";
  }

  return(ostr);
}

#endif  // defined(SIGMA_DEVELOPMENT)
