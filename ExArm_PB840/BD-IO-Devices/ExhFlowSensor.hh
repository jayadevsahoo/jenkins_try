#ifndef ExhFlowSensor_HH
#define ExhFlowSensor_HH
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: ExhFlowSensor - Implements the expiratory flow sensors.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/ExhFlowSensor.hhv   25.0.4.0   19 Nov 2013 13:54:04   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to non-inlined method.
//
//	Revision: 003  By:  syw    Date:  15-Aug-1997    DR Number: DCS 2386
//      Project:  Sigma (R8027)
//      Description:
//			Added unoffsettedFlow_ and unoffsettedProcessedValues_ data member
//			getUnoffsettedFilteredValue and getUnoffsettedValue() method.
//
//	Revision: 002  By:  syw    Date:  08-Apr-1997    DR Number: DCS 2282
//      Project:  Sigma (R8027)
//      Description:
//			Eliminate calTemp since it is now fixed.  Removed all spiro references.
//			Eliminate references to flowTemperature_.
//
//	Revision: 001  By:  iv    Date:  13-Mar-1997    DR Number: INITIAL RELEASE
//      Project:  Sigma (R8027)
//      Description:
//      	Initial version
//
//=====================================================================
// TODO E600_LL: This file is to be removed later

#include "Sigma.hh"
#include "BD_IO_Devices.hh"

//@ Usage-Classes

#include "FlowSensor.hh"

class TemperatureSensor;

//@ End-Usage

class ExhFlowSensor: public FlowSensor
{
  public:

#if defined (SIGMA_BD_CPU)

    ExhFlowSensor( // TODO E600 enable later BDIO SafetyNetSensorData *pData,
    			const AdcChannels::AdcChannelId adcId,
                TemperatureSensor& temperatureSensor,
                const Real32 alpha,
                FlowSensorCalInfo *pFlowSensorCalInfo,
                FlowSensorCalInfo *pSecondaryFlowSensorCalInfo) ;

#else

    ExhFlowSensor( TemperatureSensor& temperatureSensor,
                FlowSensorCalInfo *pFlowSensorCalInfo,
                FlowSensorCalInfo *pSecondaryFlowSensorCalInfo) ;

#endif // defined (SIGMA_BD_CPU)

    virtual ~ExhFlowSensor( void) ;

    static void SoftFault( const SoftFaultID softFaultID,
	   const Uint32        lineNumber,
	   const char*         pFileName  = NULL, 
	   const char*         pPredicate = NULL) ;

    inline void setO2Percent( const Real32 o2Percent) ;
 
#if defined (SIGMA_BD_CPU)

	inline void setPSecondaryFlowSensorCalInfo( FlowSensorCalInfo *pSecondaryFlowSensorCalInfo) ;
    inline void setDryFactor( const Real32 dryFactor) ;
    virtual void updateValue( void) ;
	inline Real32 getDryValue( void) const ;
    inline Real32 getDrySlope( void) const ;
    inline Real32 getFilteredSlopeDryValue( void) const ;
    inline Real32 getFilteredDryValue ( void) const ;
    inline void initDryValues( const Real32 filtered, const Real32 slope,
    						const Real32 filteredSlope, const Real32 prevValue) ;
	inline Real32 getUnoffsettedValue( void) const ;
	inline Real32 getUnoffsettedFilteredValue( void) const ;
	inline void setUnoffsettedProcessedValuesAlpha( const Real32 alpha) ;
			
#endif // defined (SIGMA_BD_CPU)

#if defined (SIGMA_GUI_CPU)

	Real32 updateValue( const AdcCounts flowCounts, const AdcCounts tempCounts) ;
	
#endif // defined (SIGMA_GUI_CPU)

  protected:

  private:
    // these methods are purposely declared, but not implemented...
    ExhFlowSensor( void) ;                    // default constructor
    ExhFlowSensor( const ExhFlowSensor&) ;       // not implemented
    void operator=( const ExhFlowSensor&) ;   // not implemented

	//@ Data-Member: Real32 o2Percent_
	// delivered O2 percent to compensate for exhalation flows
	Real32 o2Percent_ ;

    //@ Data-Member: pSecondaryFlowSensorCalInfo_
    // a pointer to secondary FlowSensorCalInfo instance
    FlowSensorCalInfo *pSecondaryFlowSensorCalInfo_ ;  

#if defined (SIGMA_BD_CPU)

    //@ Data-Member: dryFactor_   
    // Contains the humidification correction to dry gas,
    Real32 dryFactor_ ;

	//@ Data-Member: dryFlow_
	// exhalation flow compensated to dry
	Real32 dryFlow_ ;

    //@ Data-Member: dryProcessedValues_
    // processed values for dry flow
    ProcessedValues dryProcessedValues_ ;

	//@ Data-Member: unoffsettedFlow_
	// flow without flow offset compensation
	Real32 unoffsettedFlow_ ;

    //@ Data-Member: unoffsettedProcessedValues_
    // processed values for unoffsetted flow
    ProcessedValues unoffsettedProcessedValues_ ;

#endif // defined (SIGMA_BD_CPU)
} ;

// Inlined methods
#include "ExhFlowSensor.in"


#endif // ExhFlowSensor_HH 

