#ifndef SettingButton_HH
#define SettingButton_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-2007, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: SettingButton -  A setting button which interfaces to the
// Settings sub-system and allows a setting to be changed by the
// operator via the Adjust Panel.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/SettingButton.hhv   25.0.4.0   19 Nov 2013 14:08:28   pvcs  $
//
//@ Modification-Log
//
//  Revision: 014   By: gdc    Date: 27-Apr-2009    SCR Number: 6489
//  Project:  840S
//  Description:
//      Modifications to provide for transitioning of tube 
//		I.D. to new patient value when spontaneous type is changed to 
//		PAV with an incompatible tube I.D.. This includes changes to 
//		the user interface to verify transitioned value with flashing
//		verify arrow icon. Change better supports verification icon
//		used for tube type and I.D. as well as humidification type and
//		volume.
//
//  Revision: 013   By: gdc    Date: 11-Aug-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support changing IBW while ventilating.
//
//  Revision: 012   By: gdc    Date:  09-May-2007    SCR Number: 6237
//  Project: Trend
//  Description:
//      Modified to support single setting accept and getTargetType.
//
//  Revision: 011   By: sah    Date:  20-Apr-2000    DCS Number: 5705
//  Project:  NeoMode
//  Description:
//      Modified constructor to take arguments for auxillary title text
//      and position.  This functionality will be used to plug-in the
//      "above PEEP" phrases to the "Pi" and "Psupp" setting buttons.
//
//  Revision: 010  By:  hhd	   Date:  07-Feb-2000    DCS Number: 5504
//  Project:  NeoMode
//  Description:
//      Added batch change capability to the MainSettingsArea, whereby multiple
//      settings can be changed and accepted as a group.
//
//  Revision: 009  By:  hhd	   Date:  15-Nov-1999    DCS Number: 5412 
//  Project:  NeoMode
//  Description:
//      Modified to add showBitmap() method to show the attention icon.
//
//  Revision: 008  By:  yyy	   Date:  29-Jun-1998    DCS Number: 5327
//  Project:  NeoMode
//  Description:
//      Initial version.
//
//  Revision: 007  By: hhd    Date: 13-Aug-1999  DCS Number: 5411 
//  Project:  ATC
//  Description:
//     Implemented a mechanism to freeze the blinking of the attention icons
//	when the buttons which they are on are touched.
//
//  Revision: 006  By: sah    Date: 29-Apr-1999  DCS Number: 5365
//  Project:  ATC
//  Description:
//	Added ability to turn on and off new verify-needed icon, for use
//      with ultra-important settings.
//
//  Revision: 005  By:  sah	   Date:  23-Feb-1999    DCS Number:  5314
//  Project:  ATC
//  Description:
//      Added integrated with new dynamic, setting applicability functionality.
//      This allows for looser coupling between settings and GUI, and the
//      need for significantly less "setting knowledge" in the GUI subsystem.
//
//  Revision: 004  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 003  By: yyy      Date: 05-Jan-1998  DR Number: 
//    Project:  BiLevel (R8027)
//    Description:
//     Initial release for BiLevel adding software option.
//     Merged /840/Baseline/GUI-Applications/vcssrc/SettingButton.hhv   1.7.1.0   07/30/98 10:22:00   gdc
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "SettingBoundId.hh"

//@ Usage-Classes
#include "AdjustPanelTarget.hh"
#include "GuiTimerTarget.hh"
#include "SubScreenButton.hh"
#include "SettingObserver.hh"
#include "Bitmap.hh"
#include "Colors.hh"

class  SettingSubject;  // forward declaration...
//@ End-Usage


class SettingButton : public SubScreenButton, public AdjustPanelTarget,
						public GuiTimerTarget, public SettingObserver
{
public:
    //@ Type: ValueUpdateCallbackPtr
    // A pointer to a function used as a call back for user events.
    typedef void (*ValueUpdateCallbackPtr) (const Notification::ChangeQualifier qualifierId,
										  const SettingSubject *pSubject);

	//@ Type: UserEventId
	// This enum lists all of the offscreen keys that may be accessed
	// through this class.  Note: The last member of the enum is
	// NUMBER_OF_KEYS which should match the number of bits in the
	// keyboard register(s).
	enum UserEventId
	{
		DOWN,
		UP,
		KNOB,
		ACCEPT_KEY,
		CLEAR_KEY,
		TIMER,
		NUM_EVENT
	};

    //@ Type: UserEventCallbackPtr
    // A pointer to a function used as a call back for user events.
    typedef Boolean (*UserEventCallbackPtr) (const SettingId::SettingIdType settingId,
		    	const UserEventId userEventId, const Boolean byOperatorAction);

	SettingButton(Uint16 xOrg, Uint16 yOrg, Uint16 width, Uint16 height,
				ButtonType buttonType, Uint8 bevelSize, CornerType cornerType,
				BorderType borderType, StringId titleText,
				SettingId::SettingIdType settingId, SubScreen *pFocusSubScreen,
				StringId messageId, Boolean isMainSetting = FALSE,
				StringId auxTitleText = ::NULL_STRING_ID,
				Gravity  auxTitlePos  = ::GRAVITY_RIGHT);
    ~SettingButton(void);

	inline SettingId::SettingIdType  getSettingId(void) const;

	inline Boolean  isInVerifyState(void) const;
	inline void  setVerifyNeeded(Boolean isVerifyNeeded);

	inline void usePersistentFocus(void);
	inline void makeUnclearable(void);
	inline void setFocusSubScreen(SubScreen *pFocusSubScreen);
	inline void setMessageId(StringId messageId);
	inline void setBitmapColor(Colors::ColorS color);
	inline void showBitmap(Boolean);            
	inline void setSingleSettingAcceptEnabled(Boolean);
	inline void setShowViewable(Boolean);
	virtual void deactivate(void);
	
	// Drawable virtual method
	virtual void activate(void);

	// GuiTimerTarget virtual method
	virtual void timerEventHappened(GuiTimerId::GuiTimerIdType timerId);

	// AdjustPanelTarget virtual methods
	virtual void adjustPanelKnobDeltaHappened(Int32 delta);
	virtual void adjustPanelAcceptPressHappened(void);
	virtual void adjustPanelClearPressHappened(void);
	virtual void adjustPanelLoseFocusHappened(void);
	virtual AdjustPanelTarget::TargetType getTargetType(void) const;

	// SettingObserver virtual method
    virtual void  applicabilityUpdate(
                              const Notification::ChangeQualifier qualifierId,
                              const SettingSubject*               pSubject
                                     );
    virtual void  valueUpdate(const Notification::ChangeQualifier qualifierId,
                              const SettingSubject*               pSubject);

	void  overrideSoftBound(void);

	void registerValueUpdateCallbackPtr(ValueUpdateCallbackPtr pEventCallback);
	void registerUserEventCallbackPtr(UserEventCallbackPtr pEventCallback);

	void setBreathTimingDisplay(Boolean breathTimingDisplay);

    static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);
  
protected:
	enum ObserverActivity_
	{
		ACTIVATION_IN_PROGRESS,
		DEACTIVATION_IN_PROGRESS,
		NO_ACTIVITY
	};

	virtual void updateDisplay_(Notification::ChangeQualifier qualifierId,
                                const SettingSubject*         pSubject) = 0;

	// Button virtual methods
	virtual void downHappened_(Boolean byOperatorAction);
	virtual void upHappened_(Boolean byOperatorAction);

	inline Boolean  isThisMainSetting_(void) const;

    const SettingId::SettingIdType  SUBJECT_ID_;

	//@ Data-Member:  IS_MAIN_SETTING_
	// The activity state which indicates what triggers an
	// object's display state change, i.e, a subscreen
	// activation or users' pressing on the constant parm button. 
	ObserverActivity_  activityState_;

private:
    // these methods are purposely declared, but not implemented...
    SettingButton(const SettingButton&);		// not implemented...
    void   operator=(const SettingButton&);		// not implemented...

	//@ Data-Member:  IS_MAIN_SETTING_
	// Tells us if this setting button is a Main Setting, i.e. if it is one
	// of the buttons in the MainSettingsArea.
    const Boolean  IS_MAIN_SETTING_;

	//@ Data-Member:  IS_NON_BATCH_SETTING_
	// Tells us if this setting button is to control the value of a non-batch
	// setting.
    const Boolean  IS_NON_BATCH_SETTING_;

	//@ Data-Member:  isVerifyNeeded_
	// Tells us if this setting needs additional notification to the user
	// regarding verification.
    Boolean  isVerifyNeeded_;

	//@ Data-Member: verifyBitmap_
	// Bitmap for attention icons.
	Bitmap  verifyBitmap_;

	//@ Data-Member: pFocusSubScreen_
    // This is a pointer to the sub-screen that is associated with this
    // button (i.e. the sub-screen which either contains this button or, in
    // the case of a Main Setting button, is activated by this button).
    // All Accept Key events received by this button are sent directly to
    // this "focus" sub-screen.
	SubScreen		*pFocusSubScreen_;

    //@ Data-Member: messageId_
    // When a settings button is selected it must display a help message
    // detailing the full name of the setting being adjusted.  This
    // is the Id of that message which is sent to the LowerMessageArea
    // method setMessage().
    StringId		messageId_;

    //@ Data-Member: usePersisentFocus_
	// A flag indicating whether this button should grab the normal or
	// the persistent Adjust Panel focus.  Setting buttons in the lower
	// screen use the normal focus, upper screen setting buttons use the
	// persistent.  This is necessary to insulate focus clear events in
	// one screen from affecting the other.
    Boolean			usePersistentFocus_;

    //@ Data-Member: isClearable_
    // Most settings can be "cleared" by the user, i.e. reset to their
    // previous value that was active before the setting was adjusted.
    // Settings on the Upper screen cannot, they go into effect as soon as
    // they are adjusted.  We need this flag to tell us if we are allowed
    // to clear our setting or not.
    Boolean			isClearable_;

	//@ Data-Member: losingFocus_
	// Data member which tells us if the button is losing focus.
	// Needed to distinguish the reason for the upHappened_() method being 
	// called.
	Boolean			losingFocus_;

	//@ Data-Member: isPressedDown_
	// Data member which tells us if the button has been pressed down.
	// Used by upHappened_() to determine if the button had been 
	// pressed/set down prior to being pressed/set up.
	Boolean			isPressedDown_;

	//@ Data-Member: softBoundHappened_
	// Stores a boolean as to whether the last violated setting bound
	// was a "soft" bound, or not.
	Boolean			softBoundHappened_;

	//@ Data-Member: breathTimingDisplay_
	// Stores a boolean as to whether the last violated setting bound
	// was a "soft" bound, or not.
	Boolean			breathTimingDisplay_;

	//@ Data-Member: isSingleSettingAcceptEnabled_
	// TRUE if the accept key "accepts" the non-batch setting and pops up the button.
	// Used primarily for trend parameter setting buttons.
	Boolean			isSingleSettingAcceptEnabled_;

	//@ Data-Member: showViewable_
	// TRUE if the setting button should be shown with a VIEWABLE setting
	// FALSE (default) if the setting button is hidden with a VIEWABLE setting
	Boolean			showViewable_;

	//@ Data-Member: softBoundId_
	SettingBoundId  softBoundId_;
	
	//@ Data-Member: pValueUpdateCallBack_
	// A pointer to a function
	ValueUpdateCallbackPtr pValueUpdateCallBack_;

	//@ Data-Member: pUserEventCallBack_
	// A pointer to a function
	UserEventCallbackPtr pUserEventCallBack_;

};


// Inlined methods
#include "SettingButton.in"


#endif // SettingButton_HH
