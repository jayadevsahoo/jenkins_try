#ifndef SstFilterTest_HH
#define SstFilterTest_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class:  SstFilterTest - SST expiratory filter test
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/SstFilterTest.hhv   25.0.4.0   19 Nov 2013 14:23:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 008  By: syw    Date: 02-Feb-2000     DR Number: 5327
//  Project:  NeoMode
//  Description:
//      Delete unused extern declarations.
//
//  Revision: 007  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 006  By:  quf    Date: 23-Oct-1997    DR Number: DCS 2105
//       Project:  Sigma (840)
//       Description:
//			Updated filter low delta P Failure and Alert and high delta P
//			Alert criteria.
//
//  Revision: 005  By:  quf    Date: 05-Aug-1997    DR Number: DCS 2130
//       Project:  Sigma (840)
//       Description:
//            Moved total circuit resistance measurement from here to
//            the Circuit Resistance Test (InspResistanceTest.cc).
//
//  Revision: 004  By:  quf    Date: 14-Jul-1997    DR Number: DCS 2216
//       Project:  Sigma (840)
//       Description:
//            Added exp filter low delta-P Failure and Alert.
//
//  Revision: 003  By:  quf    Date: 24-Jun-1997    DR Number: DCS 2220
//       Project:  Sigma (840)
//       Description:
//            Changed circuit disconnect zero flow threshold to 1 lpm.
//
//  Revision: 002  By:  quf    Date: 22-May-1997    DR Number: DCS 2131
//       Project:  Sigma (840)
//       Description:
//            Modification-Log fix only.
//
//  Revision: 001  By:  mad    Date:  2-Nov-1994    DR Number: none
//	Project:  Sigma (R8027)
//	Description:
//		Initial version (Integration baseline).
//
//====================================================================

#include "ServiceMode.hh"

//@ Usage-Classes

//@ End-Usage


class SstFilterTest
{
  public:
	enum TestPhase
	{
		DISCONNECTED,
		CONNECTED
	};

    SstFilterTest( void) ;
    ~SstFilterTest( void) ;

    static void SoftFault( const SoftFaultID	softFaultID,
						   const Uint32		lineNumber,
						   const char		*pFileName  = NULL, 
						   const char		*pPredicate = NULL) ;
	void runTest( void) ;
	
  protected:

  private:
    SstFilterTest( const SstFilterTest&) ;		// not implemented...
    void operator=( const SstFilterTest&) ;		// not implemented...

	Boolean measureExhFilterResistance_ ( TestPhase testPhase,
										  Real32& inspPressure,
										  Real32& exhPressure,
										  Real32& exhFlow );

} ;


#endif // SstFilterTest_HH 
