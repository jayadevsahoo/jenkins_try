
//*****************************************************************************
//
//  ThreadPriorities.h - This contains the definitions of task's priorities.
//
//*****************************************************************************

#ifndef THREAD_PRIORITIES_H

#define THREAD_PRIORITIES_H

// Note:
// The Analog I/O thread is set as high as it is to guarrantee execution
// every 2ms on a tick boundary. We need the readings to be taken as
// evenly as possible.
// Servos and alarms run at a high priority but don't necessarily have to
// be as consitant. They just need to run every 2ms.

// General assignment of priorities in WinCE:
//      0   -  96 : High priority real time threads
//      97  - 152 : Default windows based drivers
//      153 - 247 : Low priority real time threads
//      248 - 255 : Non real time threads

#ifndef __SPLIT__GUI__PC__
#define ANALOG_INPUT_THREAD_PRIORITY			(0)		//UI for RealTimeTimerThread
#define GUI_TIMER_THREAD_PRIORITY				(1)		//UI for RealTimeTimerThread
#define REAL_TIME_TIMER_THREAD_PRIORITY			(10)
#define EXH_COMM_THREAD_PRIORITY				(89)
#define HIGH_PRIORITY_SHUTDOWN					(90)
#define SERVOS_THREAD_PRIORITY					(90)	
#define BUTTONS_THREAD_PRIORITY					(90)	//UI
#define I2C_THREAD_INITIALIZATION_PRIORITY		(91)
#define ALARMS_THREAD_PRIORITY					(91)	//UI
#define HIGH_MONITORS_THREAD_PRIORITY			(91)	/* Needs to be higher priority than the breath thread */
#define BREATH_THREAD_PRIORITY					(92)
#define COMMUNICATION_DATA_THREAD_PRIORITY		(93)	//UI
#define AUDIBLE_ALARM_THREAD_PRIORITY			(94)
#define COMMUNICATION_THREAD_PRIORITY			(180)
#define PULSE_OXIMETER_DRIVER_THREAD_PRIORITY	(190)
#define MONITORS_THREAD_PRIORITY				(100)	//UI
#define GUICOMM_THREAD_PRIORITY					(201)	//UI
#define SMARTIO_THREAD_PRIORITY					(249)
#define UI_THREAD_PRIORITY						(210)//(94) //(250)	//UI
#define NV_DATA_STORE_THREAD_PRIORITY			(254)	//UI
#define I2C_THREAD_PRIORITY						(254)
#define EVENTS_THREAD_PRIORITY					(255)	//UI
#define DEVICE_EVENTS_THREAD_PRIORITY			(255)
#define WAKEUP_EVENT_THREAD_PRIORITY			(255)
#define DEBUG_DATA_THREAD_PRIORITY				(255)
#define LOW_PRIORITY_SHUTDOWN					(255)
#else
#define HT70_PC_PRIORITY   THREAD_PRIORITY_NORMAL
#define SERIAL_DRIVER_IST_THREAD_PRIORITY       (HT70_PC_PRIORITY)    //(COMMUNICATION_DATA_THREAD_PRIORITY - 2)
#define SERIAL_DRIVER_RX_DMA_THREAD_PRIORITY    (HT70_PC_PRIORITY)    //(COMMUNICATION_DATA_THREAD_PRIORITY - 3)
#define ANALOG_INPUT_THREAD_PRIORITY    (HT70_PC_PRIORITY)
#define GUI_TIMER_THREAD_PRIORITY				(HT70_PC_PRIORITY)		//UI for RealTimeTimerThread
#define REAL_TIME_TIMER_THREAD_PRIORITY    (HT70_PC_PRIORITY)
#define HIGH_PRIORITY_SHUTDOWN          (HT70_PC_PRIORITY)
#define SERVOS_THREAD_PRIORITY          (HT70_PC_PRIORITY)
#define BUTTONS_THREAD_PRIORITY         (HT70_PC_PRIORITY)
#define COMMUNICATION_DATA_THREAD_PRIORITY       (HT70_PC_PRIORITY)
#define HIGH_MONITORS_THREAD_PRIORITY            (HT70_PC_PRIORITY)  /* Needs to be higher priority than the breath thread */
#define BREATH_THREAD_PRIORITY          (HT70_PC_PRIORITY)
#define COMMUNICATION_THREAD_PRIORITY            (HT70_PC_PRIORITY)
#define GUICOMM_THREAD_PRIORITY					 (HT70_PC_PRIORITY)
#define ALARMS_THREAD_PRIORITY                   (HT70_PC_PRIORITY)
#define AUDIBLE_ALARM_THREAD_PRIORITY            (HT70_PC_PRIORITY)
#define PULSE_OXIMETER_DRIVER_THREAD_PRIORITY    (HT70_PC_PRIORITY)
#define MONITORS_THREAD_PRIORITY        THREAD_PRIORITY_HIGHEST//(HT70_PC_PRIORITY);
#define SMARTIO_THREAD_PRIORITY         (HT70_PC_PRIORITY)
#define UI_THREAD_PRIORITY              (HT70_PC_PRIORITY)
#define NV_DATA_STORE_THREAD_PRIORITY   (HT70_PC_PRIORITY)
#define I2C_THREAD_PRIORITY             (HT70_PC_PRIORITY)
#define EVENTS_THREAD_PRIORITY                   (HT70_PC_PRIORITY)
#define DEVICE_EVENTS_THREAD_PRIORITY   (HT70_PC_PRIORITY)
#define WAKEUP_EVENT_THREAD_PRIORITY    (HT70_PC_PRIORITY)
#define DEBUG_DATA_THREAD_PRIORITY      (HT70_PC_PRIORITY)
#define LOW_PRIORITY_SHUTDOWN           (HT70_PC_PRIORITY)
#endif


#endif

