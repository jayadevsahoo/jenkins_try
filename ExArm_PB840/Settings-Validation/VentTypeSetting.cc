#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2004, Puritan-Bennett Corporation
//=====================================================================

//============== C L A S S   D E S C R I P T I O N ====================
//@ Class:  VentTypeSetting - Vent Type Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, discrete setting that indicates the type of vent
//  or patient interface used on the current patient. The available
//  settings are invasive or non-invasive.  This class inherits from
//  'BatchDiscreteSetting' and provides the specific information needed for
//  the representation of the vent type.  This information includes
//  the set of values that this setting can have (see 'VentTypeValue.hh'),
//  and this batch setting's new-patient value.
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class has no data members, and no dependent settings.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/VentTypeSetting.ccv   25.0.4.0   19 Nov 2013 14:27:50   pvcs  $
//
//@ Modification-Log
//
//  Revision: 009   By: rhj   Date: 28-July-2010    SCR Number: 6624
//  Project:  PROX
//  Description:
//      Moved the acceptTransition method for Prox to handle 
//      both conditions of vent type.
//
//  Revision: 008  By: rhj    Date: 17-Feb-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//	    Added ProxEnabledSetting acceptTransition method.		
// 
//  Revision: 007   By: gdc    Date: 16-Aug-2010     SCR Number: 6663
//  Project: API/MAT
//  Description:
//  Added support for VentSet API.
// 
//  Revision: 006  By: rhj    Date:  24-Apr-2009   SCR Number: 6502  
//  Project: 840S2
//  Description:
//	    Added LeakCompEnabledSetting acceptTransition method.
// 		
//  Revision: 005  By: gdc    Date: 24-Sep-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal non-invasive CPAP.
//
//  Revision: 004  By: gdc    Date: 11-Aug-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support changing IBW while ventilating.
//
//  Revision: 003  By: rhj    Date: 07-July-2008   SCR Number: 6435
//  Project:  LEAK
//  Description:
//       Leak Compensation project-related changes.
//       
//  Revision: 002  By: gdc    Date: 20-Feb-2005    SCR Number: 6144
//  Project:  NIV1
//  Description:
//      NIV mods. Added SRS requirement numbers.  
//
//  Revision: 001  By: gdc    Date: 20-Feb-2005    SCR Number: 6144
//  Project:  NIV1
//  Description:
//      Initial version.
//=====================================================================

#include "VentTypeSetting.hh"
#include "VentTypeValue.hh"
#include "ModeValue.hh"
#include "SupportTypeValue.hh"

//@ Usage-Classes
#include "AdjustedContext.hh"
#include "ContextMgr.hh"
#include "SettingsMgr.hh"
#include "SoftwareOptions.hh"
//@ End-Usage

//@ Code...
static const DiscreteParameterItem ParameterLookup_[] = 
{
	{"NIV", VentTypeValue::NIV_VENT_TYPE},
	{"INV", VentTypeValue::INVASIVE_VENT_TYPE},
	{NULL , NULL}
};

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  VentTypeSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

VentTypeSetting::VentTypeSetting(void)
: BatchDiscreteSetting(SettingId::VENT_TYPE,
					   Setting::NULL_DEPENDENT_ARRAY_,
					   VentTypeValue::TOTAL_VENT_TYPE_VALUES)
{
	setLookupTable(ParameterLookup_);
}  // $[TI1]


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method:  ~VentTypeSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

VentTypeSetting::~VentTypeSetting(void)
{
	CALL_TRACE("~VentTypeSetting(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[01080] -- normally changeable
//  $[LC01011] -- viewable when IBW is changing 
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id VentTypeSetting::getApplicability(const Notification::ChangeQualifier qualifierId) const
{
	CALL_TRACE("getApplicability(qualifierId)");

	Applicability::Id applicabilityId = Applicability::CHANGEABLE;

	if ( qualifierId == Notification::ADJUSTED && 
		 ContextMgr::GetAdjustedContext().getAllSettingValuesKnown() &&
		 SettingsMgr::GetSettingPtr(SettingId::IBW)->isChanged() )
	{
		applicabilityId = Applicability::VIEWABLE;
	}

	return applicabilityId;
}

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  calcTransition(toValue, fromValue)  [virtual]
//
//@ Interface-Description
//  The purpose of this method is to inform the settings effected
//  by a state transition of this setting. So they can adjust
//  their value(s) appropriately.
//
//  This method deals with state transitions where-as dependent settings
//  calculations typically deal with bounded settings and incremental
//  changes.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Inform the settings effected by a state transition of this setting.
//  So they can adjust their value(s) appropriately.
//
//  $[NI02007]   -- valid range...
//  $[NI02010]   -- transitioning from INVASIVE to NIV...
//  $[NI02011]   -- transitioning from NIV to INVASIVE...
//---------------------------------------------------------------------
//@ PreCondition
//  (toValue != fromValue)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void VentTypeSetting::calcTransition(
									const DiscreteValue toValue,
									const DiscreteValue fromValue)
{
	CALL_TRACE("calcTransition(toValue, fromValue)");

	AUX_CLASS_PRE_CONDITION(toValue != fromValue, fromValue);

	switch ( fromValue )
	{
		case VentTypeValue::INVASIVE_VENT_TYPE :
		case VentTypeValue::NIV_VENT_TYPE :
			SettingsMgr::GetSettingPtr(SettingId::LOW_CCT_PRESS)->
				acceptTransition(getId(), toValue, fromValue);
			SettingsMgr::GetSettingPtr(SettingId::LOW_EXH_MAND_TIDAL_VOL)->
				acceptTransition(getId(), toValue, fromValue);
			SettingsMgr::GetSettingPtr(SettingId::LOW_EXH_SPONT_TIDAL_VOL)->
				acceptTransition(getId(), toValue, fromValue);
			SettingsMgr::GetSettingPtr(SettingId::LOW_EXH_MINUTE_VOL)->
				acceptTransition(getId(), toValue, fromValue);
			SettingsMgr::GetSettingPtr(SettingId::LEAK_COMP_ENABLED)->
				acceptTransition(getId(), toValue, fromValue);
			SettingsMgr::GetSettingPtr(SettingId::DISCO_SENS)->
				acceptTransition(getId(), toValue, fromValue);
			SettingsMgr::GetSettingPtr(SettingId::PROX_ENABLED)->
				acceptTransition(getId(), toValue, fromValue);
			break;
		default:
			AUX_CLASS_ASSERTION_FAILURE(fromValue);
			break;
	}
}

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [const, virtual]
//
//@ Interface-Description
//  Get the setting's new-patient value.
//---------------------------------------------------------------------
//@ Implementation-Description
// $[NI02008] -- setting's new-patient value is INVASIVE
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue VentTypeSetting::getNewPatientValue(void) const
{
	CALL_TRACE("getNewPatientValue()");

	DiscreteValue  newPatientValue;

	newPatientValue = VentTypeValue::INVASIVE_VENT_TYPE;

	return(newPatientValue);
}  // $[TI1]

//==================== M E T H O D   D E S C R I P T I O N =============
//@ Method:  valueUpdate(qualifierId, pSubject)  [virtual]
//
//@ Interface-Description
//  Respond to IBW value changes by updating vent-type applicability.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void VentTypeSetting::valueUpdate(const Notification::ChangeQualifier qualifierId,
								  const SettingSubject*               pSubject)
{
	CALL_TRACE("valueUpdate(qualifierId, pSubject)");

	if ( qualifierId == Notification::ADJUSTED )
	{  // -- process IBW adjusted value change...
		const SettingId::SettingIdType SETTING_ID = pSubject->getId();

		switch ( SETTING_ID )
		{
			case SettingId::IBW:
			case SettingId::MODE:
				// updateApplicability required below
				break;
			default:
				AUX_CLASS_ASSERTION_FAILURE(SETTING_ID);
				break;
		}

		// update this instance's applicability...
		updateApplicability(qualifierId);
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: doRetainAttachment(pSubject)  [virtual, const]
//
//@ Interface-Description
//  When task-level, state changes occur, all subjects are notified
//  to detach from all observers.  This is done because the GUI-Apps
//  observers may be overwritten by new class instances, without any
//  explicit destructor/detachment calls.
//
//  This method is defined here to indicate to any of this instance's
//  subjects, not to detach this observer.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean VentTypeSetting::doRetainAttachment(const SettingSubject*) const
{
	CALL_TRACE("doRetainAttachment(pSubject)");

	return(TRUE);
}  // $[TI1]


//==================== M E T H O D   D E S C R I P T I O N ============
//@ Method:  settingObserverInit()  [virtual]
//
//@ Interface-Description
//  This method establishes the callback for monitoring changes to IBW.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void VentTypeSetting::settingObserverInit(void)
{
	CALL_TRACE("settingObserverInit()");

	attachToSubject_(SettingId::IBW, Notification::VALUE_CHANGED);
	//attachToSubject_(SettingId::MODE, Notification::VALUE_CHANGED);
}


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//			[static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void VentTypeSetting::SoftFault(const SoftFaultID  softFaultID,
								const Uint32       lineNumber,
								const char*        pFileName,
								const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

	FaultHandler::SoftFault(softFaultID, ::SETTINGS_VALIDATION,
							VENT_TYPE_SETTING, lineNumber, pFileName,
							pPredicate);
}
