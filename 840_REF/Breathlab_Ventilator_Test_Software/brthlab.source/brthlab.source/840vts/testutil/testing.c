/*****************************************************************************/
/*   Module Name:    TESTING.C	                                            
/*   Purpose:      	 Support functions for before, during and after the tests. 
/*
/*   Modification History:
/*   
/*   Description of Change			        By			      Date 
/*   ---------------------					-----   		  ------
/*	 TestExecutive related code is   
/* 	 removed and replaced corresponding		Hamsavally(CGS)    14 Oct,2000
/*   functionality.
/*
/*   Requirements:   This module implements requirements detailed in         
/*          <75359-93>                                                       
/*                                                                           
/*   $Revision::   1.1      $                                                
/*                                                                          
/*****************************************************************************/

/*****************************************************************************
THIS IS A PROPRIETARY WORK TO WHICH NELLCOR PURITAN BENNETT CORPORATION CLAIMS
EXCLUSIVE RIGHT.  NO PART OF THIS WORK MAY BE USED, DISCLOSED, REPRODUCED,
STORED IN AN INFORMATION RETIEVAL SYSTEM, OR TRANSMITTED, IN ANY FORM OR BY ANY
MEANS ELECTRONIC, MECHANICAL, PHOTOCOPYING, RECORDING OR OTHERWISE WITHOUT THE
PRIOR WRITTEN PERMISSION OF NELLCOR PURITAN BENNETT CORPORATION.
 
Copyright (c) 1999, Mallinckrodt Nellcor Puritan Bennett
All rights reserved.
*******************************************************************************/

/***********************************************/
/****************** INCLUDES *******************/
/***********************************************/

#include <ansi_c.h>
#include <formatio.h>
#include <toolbox.h>
#include <userint.h>
#include <utility.h>

#include "840cmds.h" // VENTDATA Vent
#include "cpvtmain.h"
#include "dbapi.h"
#include "ldb.h"
#include "mainpnls.h"
#include "MsgPopup.h"
#include "pb_util.h"
#include "sysmenu.h"
#include "testpnl.h"
#include "version.h"
#include "testing.h"


/***********************************************/
/************** GLOBAL VARIABLES ***************/
/***********************************************/

int SST = 0; //Flag which controls if the "Run SST" prompt is displayed
char *gDatabaseBuffer;

static char gDefaultRptFile[MAX_PATHNAME_LEN];

/***********************************************/
/************ FUNCTION DECLARATION  ************/
/***********************************************/
void GetReportFileName(void);

/*****************************************************************************/
/* Name:    RemoveRenameOldFiles                                                 
/*                                                                           
/* Purpose:  Renames the current DB files to .ol* extensions
/*                                          
/*                                                                           
/* Example Call:   int RemoveRenameOldFiles(void);                                    
/*                                                                           
/* Input:    <None>          
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   number of renamed files                                                        
/*                                                                           
/*****************************************************************************/
int RemoveRenameOldFiles(void)
{
	FILE* file_ptr;
	char* temp_ptr;
	char newname[MAX_PATHNAME_LEN];
	char extension[MAX_PATHNAME_LEN];
	int renfiles = 0;

	if(file_ptr = fopen(gTestResFile, "r")){
		fclose(file_ptr);
		strcpy(newname, gTestResFile);
		temp_ptr = strchr(newname + 1, '.');
		*temp_ptr = '\0';
		strcat(newname, ".ol1");
		rename(gTestResFile, newname);
		renfiles++;
	}
	if(file_ptr = fopen(gTestRunFile, "r")){
		fclose(file_ptr);
		strcpy(newname, gTestRunFile);
		temp_ptr = strchr(newname + 1, '.');
		*temp_ptr = '\0';
		strcat(newname, ".ol2");
		rename(gTestRunFile, newname);
		renfiles++;
	}
	if(file_ptr = fopen(gRunNumberFile, "r")){
		fclose(file_ptr);
		remove(gRunNumberFile);
	}

	return renfiles;
}


/*****************************************************************************/
/* Name:     CreateNewDBFiles                                                  
/*                                                                           
/* Purpose:  Creates New DB Files
/*                                          
/*                                                                           
/* Example Call:  void CreateNewDBFiles(void);                                    
/*                                                                           
/* Input:    <None>          
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
void CreateNewDBFiles(void)
{
	FILE* file_ptr;
	char message[256];
	int renfiles;

	if(renfiles = RemoveRenameOldFiles()){
		sprintf(message, LDBf(Ldb_DB_files_missing), renfiles);
		CPVTMessagePopup(LDBf(Ldb_Database_error), message, OK_ONLY_DLG, NO_HELP);
	}	

	file_ptr = fopen(gRunNumberFile, "w+");
	fprintf(file_ptr, "1");
	fclose(file_ptr);

	file_ptr = fopen(gTestResFile, "w+");
	fclose(file_ptr);

	file_ptr = fopen(gTestRunFile, "w+");
	fclose(file_ptr);

	return;
}


/*****************************************************************************/
/* Name:     SetupDB                                                         
/*                                                                           
/* Purpose:  Obtains the current run number and database   
/*           filenames.                
/*                                                                           
/* Example Call:  SetupDB();                                   
/*                                                                           
/* Input:   <None>            
/*                                                                           
/* Output:   TRUE if error, otherwise FALSE                                                        
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   TRUE=error, else FALSE
/*                                                                           
/*****************************************************************************/
int SetupDB(void)
{
	char *temp_ptr = NULL;
	FILE *file_ptr;
	long int next_run = 1;

	/* Ensure that filename exists */ 
	if(gSeqReportFile == NULL || strcmp(gSeqReportFile, "") == 0){
		return TRUE;
	}

	/* Copy file name over */
	strcpy(gTestResFile, gSeqReportFile);

	/* Find end of file */
	temp_ptr = strchr(gTestResFile, '\0');

	/* Move back before file extension */
	temp_ptr -= 5;

	/* Find the start of the file extension */
	temp_ptr = strchr(temp_ptr, '.');
	*(temp_ptr + 1) = '\0';

	/* Copy this string over to the run number file string */
	strcpy(gRunNumberFile, gTestResFile);
	strcpy(gTestRunFile, gTestResFile);

	/* Create the filenames for the database files */
	Fmt(gRunNumberFile, "%s[a]<%s", "run");
	Fmt(gTestResFile, "%s[a]<%s", "db1");
	Fmt(gTestRunFile, "%s[a]<%s", "db2");

	/* Open the file and get the run number */
	if(file_ptr = fopen(gRunNumberFile, "r")){
		/* Get run number */
		if(!fscanf(file_ptr, "%ld", &next_run)){
			return TRUE;
		}

		/* Close file */
		fclose(file_ptr);
	}
	else{
		//if the run file does not exist create it!
		CreateNewDBFiles();
//		file_ptr = fopen(gRunNumberFile, "w+");
//		fprintf(file_ptr, "0");
		next_run = 1;
//		fclose(file_ptr);
	}

	/* Delete the run number file */
//	if(remove(gRunNumberFile)){
//		return TRUE;
//	{

	/* Create the file again with new run number */
	if(file_ptr = fopen(gRunNumberFile, "w")){
		fprintf(file_ptr, "%ld\n", next_run + 1);
		fclose(file_ptr);
	}
	else{
		return TRUE;
	}

	gRunNumber = next_run;
	gJetSeqID = SetupDB_Jet();

	return FALSE;
}


/*****************************************************************************/
/* Name:    WriteRunDB                                               
/*                                                                           
/* Purpose:   Write test results to the run database file.
/*                                                                          
/* Example Call:   WriteRunDB(serialNum);                                 
/*                                                                           
/* Input:    serialNum - serial number of vent being tested (unused-now use Vent struct)        
/*                                                                           
/* Output:   TRUE if error, otherwise FALSE                                                             
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   TRUE=error, else FALSE                                                       
/*                                                                           
/*****************************************************************************/
int WriteRunDB(char *serialNum)
{
	FILE *file_ptr;
	char temp_str[250];
	char* tempBdString;
	char* tempBdSerial;

	/* Ensure that filename exists */
	if(gTestRunFile == NULL || strcmp(gTestRunFile, "") == 0){
		return TRUE;
	}

	/* Open the file or create it */
	if(file_ptr = fopen(gTestRunFile, "a+")){
		/* We need to strip the heading off the serial number first */
		tempBdString = malloc(sizeof(Vent.Bd.serial)); 
		strcpy(tempBdString, Vent.Bd.serial);
		strtok(tempBdString, ":");
		tempBdSerial = strtok(NULL, ":");

		/* Run #, Serial #, StationID, Prog. Rev, Operator, Date, Time */
		sprintf(temp_str, "%ld, %s, %d, %s, %s, %s, %s, ", gRunNumber, 
		        tempBdSerial, gStationNum, gProgramRev, gCurrOperator,
		        DateStr(), TimeStr());

		free(tempBdString);

		/* Append Pass/Fail flag to string */
		if(sAbortFlag){
			Fmt(temp_str, "%s[a]<%s", "A\n");
		}
		else{
			if(gOverallpass){
				Fmt(temp_str, "%s[a]<%s", "P\n");
			}
			else{
				Fmt(temp_str, "%s[a]<%s", "F\n");
			}
		}
		fprintf(file_ptr, "%s", temp_str);
		fclose(file_ptr);

		if(sAbortFlag){
			WriteSeqDB_Jet(gCurrOperator, DB_ABORT, gJetSeqID);
		}
		else{
			WriteSeqDB_Jet(gCurrOperator, gOverallpass, gJetSeqID);
		}
    }
    else{
        return TRUE;
	}

	return FALSE;
}


/*****************************************************************************/
/* Name:     WriteStepDB                                                    
/*                                                                           
/* Purpose:  Write test results to the step database file. 
/*                                                                           
/* Example Call:   WriteStepDB();                                      
/*                                                                           
/* Input:   <None>        
/*                                                                           
/* Output:   TRUE if error, otherwise FALSE                                                         
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   TRUE=error, else FALSE                                                        
/*                                                                           
/*****************************************************************************/
// currently only called by RunSequence
int WriteStepDB(void)
{
	FILE *file_ptr;
	char hold;
	char *temp_ptr;
	char *start_ptr;

	if(gTestResFile == NULL
	|| strcmp(gTestResFile, "") == 0
	|| gDatabaseBuffer == NULL){
		return TRUE;
	}

	/* Open the file and write to it */
	if(file_ptr = fopen(gTestResFile, "a+")){
		/* Save pointer to beginning of buffer */
		start_ptr = gDatabaseBuffer;

		/* Check to see if buffer is of size 511 or greater */
		while(strlen(gDatabaseBuffer) >= 511){
			/* Break buffer into blocks */
			temp_ptr = gDatabaseBuffer + 510;

			/* Save character where break will occur */
			hold = *temp_ptr;
			*temp_ptr = '\0';

			/* Write to the file */
			fprintf(file_ptr, "%s", gDatabaseBuffer);

			/* Move to next block */
			gDatabaseBuffer = temp_ptr;
			*gDatabaseBuffer = hold;
		} // end while(strlen(gDatabaseBuffer) >= 511)

		fprintf(file_ptr, "%s", gDatabaseBuffer);
		fclose(file_ptr);
	}
	else{
		free(gDatabaseBuffer);
		gDatabaseBuffer = NULL;
		return TRUE;
	}

	free(start_ptr);
	gDatabaseBuffer = NULL;

	return FALSE;
}


/*****************************************************************************/
/* Name:     GetReportFileName                                                      
/*                                                                           
/* Purpose:  get report file info from sequence.  if 
/*      report file name is not locked, allow user   
/*      the user to change it.        
/*                                                                           
/* Example Call:   void GetReportFileName();                                    
/*                                                                           
/* Input:    <None>          
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   None                                                        
/*                                                                           
/*****************************************************************************/
void GetReportFileName()
{
	int status;
	int selectStatus;
	
	char message[256];

    strcpy(gDefaultRptFile, LDBf(Ldb_RSE_Dir));
		/* report file name is locked, use default */
	strcpy(gSeqReportFile, gDefaultRptFile);
	return;
}

/*****************************************************************************/
/* Name:     InitDataRecBuffe                                                      
/*                                                                           
/* Purpose:  To initialise the datarec strucuture
/*         
/*             
/*                                                                           
/* Example Call:void  InitDataRecBuffer(void)                               
/*                                                                           
/* Input:    <None>          
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   None                                                        
/*                                                                           
/*****************************************************************************/


void InitDataRecBuffer(void)
{
	int i =0;
	for (i = 0;i<NUM_OF_TESTS;i++)
	{
		tDataRec[i].result    = PASS;
		tDataRec[i].outBuffer = NULL;
		tDataRec[i].outBuff_localized = NULL;
		tDataRec[i].sRunMode  = NULL; 
	}	
		

	return;
}

