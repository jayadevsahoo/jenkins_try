#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Nellcor Puritan-Bennett Corporation
//=====================================================================

// =========================== M O D U L E   D E S C R I P T I O N ====
//@ The download tftp Server main entry function.
//---------------------------------------------------------------------
//@ Interface-Description
//    The main function to parse the command arguments to find out
//    command options and kick off the download Server task.
//    Handles multi-user support options.
//---------------------------------------------------------------------
//@ Rationale
//    Every C or C++ program requires a main function that parses
//    and checks command arguments.
//---------------------------------------------------------------------
//@ Implementation-Description
//    UNIX style signal handling scheme is used to leave a consistent
//    locking state for a multiple user mode.
//    The download server recognizes only those targets that are
//    defined in the target table in this module.
//---------------------------------------------------------------------
//@ Fault-Handling
//   Errors detected by the software is generated to stdout.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
//@(#) $Header:   /840/Baseline/Download/vcssrc/DownloadMain.ccv   25.0.4.0   19 Nov 2013 14:02:48   pvcs  $ 
//
//@ Modification-Log
//
//  Revision: 005   By:   rhj    Date: 05-March-2007      DR Number: 6359
//  Project:  RESPM
//  Description:
//        Resolve merge conflict for RM to Baseline 
//
//  Revision: 004  By: gdc  Date: 22-Feb-2005  DR Number: 6148
//    Project:  NIV1
//    Description:
//       DCS 6148 - move to solaris
//       modified to instantiate static LockFile and get static instance of
//       Download objects instead of using placement new which is not 
//       inherently supported by the GNU compiler or linker
//
//  Revision: 003  By: Gary Cederquist  Date: 01-SEP-1999  DR Number: 5524
//       Project:  ATC
//       Description:
//             Corrected IP address used for MS01.
//
//  Revision: 002  By: Gary Cederquist  Date: 28-JUL-1997  DR Number: 2197
//       Project:  Sigma (R8027)
//       Description:
//             Changed to use SigmaTypes.hh
//
//  Revision: 001  By:  <who>    Date:  dd-mmm-yy    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//=====================================================================

//@ Code...

#include "Download.hh"
#include <sys/types.h>
#if	defined(SIGMA_SUN)
#include <sys/stat.h>
#include <signal.h>
#include <stdio.h>
int MaxCount = 0;
#endif//defined(SIGMA_SUN)
#include "LockFile.hh"

#include "TargetAddr.hh"

static LockFile *PLockfile;

// The following two are global variables, referenced by other modules.
Boolean UseServerIpAddr = FALSE;
TargetAddr *PTargetAddr = NULL;
TargetAddr TargetAddrTbl[] =
{
	{"xt00", "00a0612d9199", "00a0612d919a", 0x92d6914f, TRUE},
	{"xt90", "00a0612d9167", "00a0612d9168" ,0x92d69151, TRUE},
	{"xt91", "00a0612d916b", "00a0612d916c", 0x92d69153, TRUE},
	{"xt92", "00a0612d916f", "00a0612d9171", 0x92d69155, TRUE},
	{"xt93", "00a0612d9174", "00a0612d9175", 0x92d69157, TRUE},
	{"xt94", "00a0612d9178", "00a0612d9179", 0x92d69159, TRUE},
	{"xt95", "00a0612d917c", "00a0612d917d", 0x92d6915b, TRUE},
	{"xt96", "00a0612d9180", "00a0612d9181", 0x92d6915d, TRUE},
	{"xt97", "00a0612d9183", "00a0612d9185", 0x92d6915f, TRUE},
	{"xt98", "00a0612d9187", "00a0612d9189", 0x92d69161, TRUE},
	{"xt99", "00a061ff91c7", "00a061ff91c8", 0x92d69163, TRUE},
	{"ms00", "00a0612d91cd", "00a0612d91ce", 0x92d691cd, TRUE},
	{"ms01", "00a0612d91cf", "00a0612d91d0", 0x92d691cf, TRUE},
	{"ms02", "00a0612d91d7", "00a0612d91d8", 0x92d691d7, TRUE},
	{ "xtbd01", "00a061050001", NULL,  	0x92d6919d, FALSE}, // 146.214.145.157
	{ "xtgui01", NULL, "0a0061060001", 	0x92d6919e, FALSE}, // 146.214.145.158
	{ "bd01", "00a061050001", NULL,  	0x92d6919d, FALSE}, // 146.214.145.157
	{ "gui01", NULL,  "00a061060001",  	0x92d6919e, FALSE}, // 146.214.145.158
	{ "xtbd02", "00a061050002", NULL,  	0x92d691a1, FALSE}, // 146.214.145.161
	{ "xtgui02", NULL, "00a061060002",  	0x92d691a2, FALSE}, // 146.214.145.162
	{ "bd02", "00a061050002", NULL,  	0x92d691a1, FALSE}, // 146.214.145.161
	{ "gui02", NULL,  "00a061060002",  	0x92d691a2, FALSE}, // 146.214.145.162
	{ "xtbd03", "00a061050003", NULL,  	0x92d691a5, FALSE}, // 146.214.145.165
	{ "xtgui03", NULL,  "00a061060003",  	0x92d691a6, FALSE}, // 146.214.145.166
	{ "bd03", "00a061050003", NULL,  	0x92d691a5, FALSE}, // 146.214.145.165
	{ "gui03", NULL,  "00a061060003",  	0x92d691a6, FALSE}, // 146.214.145.166
	{ "xtbd04", "00a061050004", NULL,  	0x92d691a9, FALSE}, // 146.214.145.169
	{ "xtgui04", NULL,  "00a061060004",  	0x92d691aa, FALSE}, // 146.214.145.170
	{ "bd04", "00a061050004", NULL,  	0x92d691a9, FALSE}, // 146.214.145.169
	{ "gui04", NULL,  "00a061060004",  	0x92d691aa, FALSE}, // 146.214.145.170
	{ "xtbd05", "00a061050005", NULL,  	0x92d691ad, FALSE}, // 146.214.145.173
	{ "xtgui05", NULL,  "00a06160005",  	0x92d691ae, FALSE}, // 146.214.145.174
	{ "bd05", "00a061050005", NULL,  	0x92d691ad, FALSE}, // 146.214.145.173
	{ "gui05", NULL,  "00a061060005",  	0x92d691ae, FALSE}, // 146.214.145.174
	{ "xtbd06", "00a061050006", NULL,  	0x92d691b1, FALSE}, // 146.214.145.177
	{ "xtgui06", NULL,  "00a061060006",  	0x92d691b2, FALSE}, // 146.214.145.178
	{ "bd06", "00a061050006", NULL,  	0x92d691b1, FALSE}, // 146.214.145.177
	{ "gui06", NULL,  "00a061060006",  	0x92d691b2, FALSE}, // 146.214.145.178
	{ "xtbd07", "00a061050007", NULL,  	0x92d691b5, FALSE}, // 146.214.145.181
	{ "xtgui07", NULL,  "00a061060007",  	0x92d691b6, FALSE}, // 146.214.145.182
	{ "bd07", "00a061050007", NULL,  	0x92d691b5, FALSE}, // 146.214.145.181
	{ "gui07", NULL,  "00a061060007",  	0x92d691b6, FALSE}, // 146.214.145.182
	{ "xtbd08", "00a061050008", NULL,  	0x92d691b9, FALSE}, // 146.214.145.185
	{ "xtgui08", NULL,  "00a061060008",  	0x92d691ba, FALSE}, // 146.214.145.186
	{ "bd08", "00a061050008", NULL,  	0x92d691b9, FALSE}, // 146.214.145.185
	{ "gui08", NULL,  "00a061060008",  	0x92d691ba, FALSE}, // 146.214.145.186
	{ "xtbd09", "00a061050009", NULL,  	0x92d691bd, FALSE}, // 146.214.145.189
	{ "xtgui09", NULL,  "00a061060009",  	0x92d691be, FALSE}, // 146.214.145.190
	{ "bd09", "00a061050009", NULL,  	0x92d691bd, FALSE}, // 146.214.145.189
	{ "gui09", NULL,  "00a061060009",  	0x92d691be, FALSE}, // 146.214.145.190
	{ "xtbd10", "00a06105000a", NULL,  	0x92d691c1, FALSE}, // 146.214.145.193
	{ "xtgui10", NULL,  "00a06106000a",  	0x92d691c2, FALSE}, // 146.214.145.194
	{ "bd10", "00a06105000a", NULL,  	0x92d691c1, FALSE}, // 146.214.145.193
	{ "gui10", NULL,  "00a06106000a",  	0x92d691c2, FALSE}, // 146.214.145.194
	{ "xtbd11", "00a06105000b", NULL,  	0x92d691c5, FALSE}, // 146.214.145.197
	{ "xtgui11", NULL,  "00a06106000b",  	0x92d691c6, FALSE}, // 146.214.145.198
	{ "bd11", "00a06105000b", NULL,  	0x92d691c5, FALSE}, // 146.214.145.197
	{ "gui11", NULL,  "00a06106000b",  	0x92d691c6, FALSE}, // 146.214.145.198
	{ "xtbd12", "00a06105000c", NULL,  	0x92d69196, FALSE}, // 146.214.145.150
	{ "bd12", "00a06105000c", NULL,  	0x92d69196, FALSE}, // 146.214.145.150
	{ "xtgui12", NULL,  "00a06106000c",  	0x92d6918c, FALSE}, // 146.214.145.140
	{ "gui12", NULL,  "00a06106000c",  	0x92d6918c, FALSE}, // 146.214.145.140
	{ "xtbd13", "00a06105000d", NULL,  	0x92d69190, FALSE}, // 146.214.145.144
	{ "bd13", "00a06105000d", NULL,  	0x92d69190, FALSE}, // 146.214.145.144
	{ "xtgui13", NULL,  "00a06106000d",  	0x92d69191, FALSE}, // 146.214.145.145
	{ "gui13", NULL,  "00a06105000d",  	0x92d69191, FALSE}, // 146.214.145.145
	{ "gui14", NULL,  "00a06106000e",  	0x92d69195, FALSE}, // 146.214.145.149
	{ "xtgui14", NULL,  "00a06106000e",  	0x92d69195, FALSE}, // 146.214.145.149
	{NULL, NULL,NULL, 0, FALSE},
};

void
convertToHex(char *pAscii,char *pHex)
{
Uint j,k;
Uint8 number;
Uint8 nibble;

	for(j=0;j< ADDR_LENGTH; j++) 
	{
		pHex[j] = 0xFF;
	}
	if ( pAscii == NULL )
	{
		return;
	}
	for(j=0;j< ADDR_LENGTH; j++) 
	{
		pHex[j] = 0;
		for(k=0; k< 2; k++)
		{
			pHex[j] = pHex[j] << 4;
			nibble = pAscii[ j*2  + k];  
			if (( nibble <= '9' ) && ( nibble >= '0' ))
			{
				number = nibble & 0x0f;
			}
			else if ((( nibble <= 'f' ) && ( nibble >= 'a' ))
						|| (( nibble <= 'F' ) && ( nibble >= 'A' )))
			{
				number = (nibble & 0x0f) + 9;
			}
			pHex[j] += number;
		}
	}
#if defined(SIGMA_DEBUG)
printf("Target EtherAddr : %x:%x:%x:%x:%x:%x\n", 
			   pHex[0], pHex[1],pHex[2],pHex[3], pHex[4], pHex[5]);
#endif //defined(SIGMA_DEBUG)
}
	
void 
toLower(char *sName)
{
unsigned char c; 
static unsigned char caseDiff = 'a' -'A';

	while (c = *sName)
	{
		if ((c >= 'A') && (c <= 'Z'))
		{
			*sName = c + caseDiff;
		}
		sName++;
	}
}

TargetAddr *
getTargetAddr(char *targetName)
//*********************************************************************
//* DEBUG Function:   returns a TargetAddr pointer for a given target.
//  The function returns NULL pointer if a given target doesn't
//  exist in the Target table. If a target is found in the target
//  address table, it converts ascii string format ethernet address
//  to 6 Octet hex number.
//*********************************************************************/
{
Uint i;
TargetAddr *pTbl;

	toLower(targetName);
	for(i = 0; TargetAddrTbl[i].name; i++)
	{
		pTbl = &TargetAddrTbl[i];
		if ( !strcmp( targetName, pTbl->name ))
		{
			convertToHex(pTbl->pGui, pTbl->guiAddr);
			convertToHex(pTbl->pBd, pTbl->bdAddr);	
			return(pTbl);
		}
	}
	return(NULL);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Free Function:: term_sig_handler(Int )
//
//@ Interface-Description
//    Termination signal handler routine.
//    Gets rid of the lock file before it dies.
//---------------------------------------------------------------------
//@ Implementation-Description
//		If the pLockFile a non-zero positive number, the routine deletes
//      the file.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
term_sig_handler(Int )
{
	PLockfile->unLock();
	exit(0);
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Free Function:: main()
//
//@ Interface-Description
//	  The download server program entry point. It initializes
//    the download subsystem class. It sets up the Bootfile load
//	  paths from the LoadPath environment variable.
//    The '-d' option indicates that BD calibration data block should
//    not be erased.
//    The '-s' option indicates that a serial number should not
//    be intialized to an initial value during the download.
//    The '-k' option indicates that a client IP address should be 
//    based on a tftpServer Ip address instead of a pre-allocated 
//    IP addresses on SUN.
//---------------------------------------------------------------------
//@ Implementation-Description
//   Get the load image search path from the
//   environment variable. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

main(int argc, char **argv)
{
int i;
char  Usage[100];
char *pArg;
Boolean lockCheck = TRUE;
char targetName[100];
char pTargetEther = NULL;

	sprintf(Usage,"Usage: %s [-bd Bdfile]  [-gui Guifile] [-c Maxload ] [-s] [-d] [-n] [-k] [-t TargetId] \n",argv[0]);

	targetName[0] = '\0';
	static LockFile LockFile_;

	PLockfile = &LockFile_;

	for (i = 1; i < argc; i++ )
	{
		pArg = argv[i];
		switch(pArg[0])
		{
			case '-':
				switch(pArg[1])
				{
					case 'b':
						if ( pArg[2] == 'd')
						{
							if ( pArg[3] )
							{
								Download::SetBdBootFile(&pArg[3]);
							}
							else if ( argc > i )
							{
								Download::SetBdBootFile(argv[++i]);
							}
							else
							{
								printf(Usage);
								Download::ReportError("must to specify a bd image file after -bd option\n");
								return(-1);
							}
							continue;
						}
						printf(Usage);
						Download::ReportError("illegal option %s\n", pArg);
						return(-1);
					case 'c':
						if ( argc > i )
						{
							if ( sscanf(argv[++i], "%d", &MaxCount ) == 1)
								continue;
						}
						printf(Usage);
						Download::ReportError("a numeric number expected after -c option\n");
						return(-1);

					case 'n':
						lockCheck = FALSE;
						continue;;

					case 'k':
						UseServerIpAddr = TRUE;
						continue;

					case 'g':
						if ( strcmp(&pArg[1], "gui") == 0 )
						{
							if ( pArg[4] )
							{
								Download::SetGuiBootFile( &pArg[4]);
							}	
							else if ( argc > i )
							{
								Download::SetGuiBootFile( argv[++i]);
							}
							else
							{
								printf(Usage);
								Download::ReportError("must to specify a gui image file after -gui option");
								return(-1);
							}
							continue;
						}
						printf(Usage);
						Download::ReportError("illegal option %s\n", pArg);
						return(-1);
				   case 't':
				   	   	if ( pArg[2] )
					   	{
							strcpy(targetName, &pArg[2]);
					   	}
						else if ( argc > i )
					   	{
					   		strcpy(targetName, argv[++i]);
					   	}
						else
						{
							printf(Usage);
							Download::ReportError("must to specify a target name with  the -t option");
							return(-1);
						}
						continue;

				default:
					printf(Usage);
					Download::ReportError("unrecognized option %s \n", pArg);
					return(-1);
			}// switch(pArg[1])
			default:
				printf(Usage);
				Download::ReportError("unrecognized option %s \n", pArg);
				return(-1);
		}//switch(pArg[0])
	}// end of for loop  

	signal(SIGHUP, term_sig_handler);
	signal(SIGINT, term_sig_handler);
	signal(SIGTERM, term_sig_handler);
	signal(SIGQUIT, term_sig_handler);

	if ( targetName[0] )
	{
		if (!(PTargetAddr = getTargetAddr(targetName)))
		{
			Download::ReportError("Unsupported Target, %s", targetName); 
			return(-1);
		}
	}

	if ( lockCheck )
	{
		if ( PLockfile->setLock(targetName) < 0 )
		{
			return(-1);
		}
	}
	Download::Initialize();

	if ( BootFile::InitLoadPath() == FAILURE )
	{
		return(-1);	
	}
	Download::GetDownloader().serverTask();

#if	defined(SIGMA_SUN)
	term_sig_handler(SIGQUIT);
#endif //defined(SIGMA_SUN)
	return(-1);
}
