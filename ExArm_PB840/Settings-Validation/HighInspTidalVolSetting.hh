
#ifndef HighInspTidalVolSetting_HH
#define HighInspTidalVolSetting_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  HighInspTidalVolSetting - High Inspired Tidal Volume Limit
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/HighInspTidalVolSetting.hhv   25.0.4.0   19 Nov 2013 14:27:24   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By: sah     Date:  11-Sep-2000    DR Number: 5766
//  Project:  VTPC
//  Description:
//	Added new upper soft limit, which needs 'findSoftMinMavValue_()'
//      method.
//
//  Revision: 003   By: sah   Date:  22-Jul-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  modified to use newly-modified 'BoundedRange' class to handle
//         range, via linked list of interval nodes
//      *  eliminated 'get{Max,Min}Limit()' method, now defined by base class
//
//  Revision: 002   By: sah   Date:  19-Jan-1999    DR Number:  5314
//  Project:  ATC
//  Description:
//     Added new 'getApplicability()' method.
//
//  Revision: 001   By: sah   Date:  19-Jan-1999    DR Number:  5322
//  Project:  ATC
//  Description:
//	New ATC-specific setting.
//
//====================================================================

//@ Usage-Classes
#include "BatchBoundedSetting.hh"
//@ End-Usage


class HighInspTidalVolSetting : public BatchBoundedSetting 
{
  public:
    HighInspTidalVolSetting(void); 
    virtual ~HighInspTidalVolSetting(void);

    virtual Applicability::Id  getApplicability(
				 const Notification::ChangeQualifier qualifierId
					       ) const;

    virtual SettingValue  getNewPatientValue(void) const;

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL, 
			   const char*       pPredicate = NULL);
 
  protected:
    virtual void  updateConstraints_(void);

    virtual void  findSoftMinMaxValues_(Real32& rSoftMinValue,
					Real32& rSoftMaxValue) const;

  private:
    // not implemented...
    HighInspTidalVolSetting(const HighInspTidalVolSetting&);
    void  operator=(const HighInspTidalVolSetting&);
};


#endif // HighInspTidalVolSetting_HH 
