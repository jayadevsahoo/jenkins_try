//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file PatientDataUIEnumDef.h
//----------------------------------------------------------------------------

#ifndef ENUMDEF_H
#define ENUMDEF_H

//-------------------------------------
// I N C L U D E S
//-------------------------------------
#include"EpochTimeStamp.h"
#include"BreathDataPacket.hh"
#include"VtpcvStateDataPacket.hh"
#include"PavStartupDataPacket.hh"
#include"PavBreathDataPacket.hh"
#include"PavPlateauDataPacket.hh"

//-------------------------------------
// N A M E S P A C E  D E C L
//-------------------------------------
namespace Enum
{

    typedef struct tagEnumDef
    {
        char *name;
        int  index_value;
        int  def_value;
    } EnumDef;

    extern const int ENUM_BreathType_ID;
    extern const int ENUM_PhaseType_ID;
    extern const int ENUM_SupportType_ID;
    extern const int ENUM_MandType_ID;
    extern const int ENUM_Bool_ID;
    extern const int ENUM_Trigger_ID;
    extern const int ENUM_VentType_ID;
    extern const int ENUM_ManeuverType_ID;
    extern const int ENUM_ManeuverState_ID;
    extern const int ENUM_PedState_ID;
    extern const int ENUM_PpiState_ID;
    extern const int ENUM_ComplianceState_ID;
    extern const int ENUM_VtpcvState_ID;
    extern const int ENUM_PavState_ID;


    EnumDef *getEnumList(unsigned int in);

    EnumDef *getEnum(EnumDef *e,unsigned int in);


    int convertPhaseType(float x);

    float convert(float x);
    int convert(BreathType &x);
    int convert(BreathPhaseType::PhaseType &x);
    int convert(SupportTypeValue::SupportTypeValueId &x);
    int convert(MandTypeValue::MandTypeValueId &x);
    int convert(Trigger::TriggerId &x);
    int convert(VentTypeValue::VentTypeValueId &x);
    int convert(PauseTypes::ManeuverType &x);
    int convert(PauseTypes::ManeuverState &x);
    int convert(PauseTypes::PedState &x);
    int convert(PauseTypes::PpiState &x);
    int convert(PauseTypes::ComplianceState &x);
    int convert(VtpcvState::Id &x);
    int convert(PavState::Id &x);
    int convert(Boolean &x);

}
#endif
