#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: BreathData - This class stores all data for communication to
//  Patient-Data-Management subsystem that pertains to a single breath.
//---------------------------------------------------------------------
//@ Interface-Description
// The information pertaining to a single breath, stored in this class,
// is updated on a breath to breath basis, as it becomes available,
// through the method provided for breath updates.  The data is communicated
// to the GUI via PatientDataMgr's interface.
//---------------------------------------------------------------------
//@ Rationale
//   It is necessary to have the breath data required by subsystems other
//   than breath delivery isolated from the breath delivery subsystem.
//---------------------------------------------------------------------
//@ Implementation-Description
// This class contains private data members that store the information
// required by the Patient-Data-Management subsystem. The information stored
// here is from a mixture of the current and last breaths, as that is
// necessary for the user interface.  The following is data regarding the
// current breath:
//    breathType_
// The follwing is data regarding the last breath delivered:
//    previousBreathType_
//    breathDuration_
//    exhaledTidalVolume_
//    endInspiratoryPressure_
//    endExpiratoryPressure_
//    endExpiratoryPausePressure_
//    ieRatio_
//    peakCircuitPressure_
//---------------------------------------------------------------------
//@ Fault-Handling
//    n/a.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/BreathData.ccv   25.0.4.0   19 Nov 2013 13:59:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 043    By:  rhj    Date:  18-Feb-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//      PROX project-related changes.
// 
//  Revision: 042   By:   rhj    Date: 18-March-2009    SCR Number: 6492
//  Project:  840S2
//  Description:
//       Added  getInspiredLungVolume().
//
//  Revision: 041   By:   rhj    Date: 05-March-2009    SCR Number: 6454
//  Project:  840S
//  Description:
//       Added inspiredLungVolAlarm to endInspDataPacket.
//
//  Revision: 040   By: rhj    Date: 10-Nov-2008      SCR Number: 6435
//  Project:  840S
//  Description:
//       Leak Compensation project-related changes.
//
//  Revision: 039   By:   rhj    Date: 05-March-2007      DR Number: 6359
//  Project:  RESPM
//  Description:
//        Resolve merge conflict for RM to Baseline
//
//  Revision: 038   By: gdc      Date:  20-Feb-2007    SCR Number: 6345
//  Project:  RESPM
//  Description:
//		Removed C20/C related code.
//
//  Revision: 037   By: gdc      Date:  22-Oct-2006    SCR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Added processing of RM data items.
//
//  Revision: 036   By: gdc      Date:  10-Jun-2005    SCR Number: 6170
//  Project:  NIV
//  Description:
//		NIV related changes. Added breath trigger data to end of inspiration
//		data packet.
//
//  Revision: 035   By: gdc      Date:  10-Feb-2005    SCR Number: 6144
//  Project:  NIV
//  Description:
//		NIV related changes. Added vent-type to breath data.
//
//  Revision: 034   By: gfu   Date:  27-July-2004    DR Number: 6135
//  Project:  PAV
//  Description:
//      Modified module per SDCR #6135.  Based upon input from the Field Evaluations in Canada the
//      following PAV specific breath delivery change is needed:
//      Change the averaging algorithmn for exaled tidal volume due to the breath-to-breath variability
//      to be the single breath value if it falls outside 5.0 + 15% of the averaged value.
//
//  Revision: 033   By: yakovb   Date:  19-Mar-2002    DR Number: 5863
//  Project:  VCP
//  Description:
//      Handle VC+ mandatory messages.
//
//  Revision: 032   By: yakovb   Date:  12-Mar-2002    DR Number: 5860
//  Project:  VCP
//  Description:
//      Handle VC+/VS Startup messages.
//
//  Revision: 031   By: sah   Date:  09-Feb-2001    DR Number: 5857
//  Project:  PAV
//  Description:
//      Added new NEONATAL-specific rejection criteria for running average
//      of exhaled tidal volume.
//
//  Revision: 030   By: syw   Date:  02-Aug-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//		PAV project-related changes:
//		Handle SEND_PAV_DATA message.
//
//  Revision: 029  By: jja     Date:  17-May-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//		VTPC project-related changes:
//		Added spontTiTtotRatio_ calculation, spontInspTime_.
//
//  Revision: 028  By:  jja    Date:  20-Apr-00    DR Number: 5708
//	Project:  NeoMode
//	Description:
//		Revise Pmean to be an averaged value instead of
//              single breath.
//
//  Revision: 027  By: jja     Date:  06-Apr-00    DR Number: 5692
//  Project:  NeoMode
//  Description:
//		Move peakCircuitPressure update to end of insp.
//
//  Revision: 026  By: syw     Date:  18-Jul-1999    DR Number: 5471, 5481
//  Project:  ATC
//  Description:
//		Added breathTypeDisplay to manage the breath type bar on the GUI.
//
//  Revision: 025  By: syw     Date:  26-Apr-1999    DR Number: 5322
//  Project:  ATC
//  Description:
//		Added inspiredLungVolume_ and supportType_ support for TC.
//
//  Revision: 024  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 023  By:  iv    Date:  31-Jan-1998    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Bilevel initial version.
//             Added a check for INSPIRATORY_PAUSE phase in method update().
//
//  Revision: 012  By:  iv     Date: 20-Aug-1997    DR Number: DCS 2382
//       Project:  Sigma (R8027)
//       Description:
//             Added TI #s and optimized calculation time.
//
//  Revision: 011  By:  syw    Date: 14-Aug-1997    DR Number: DCS 2382
//       Project:  Sigma (R8027)
//       Description:
//             Added BUFFER_SIZE pt running average of exhaledTidalVolume data.
//
//  Revision: 010  By:  sp    Date: 12-Dec-1996    DR Number: DCS 1637
//       Project:  Sigma (R8027)
//       Description:
//             Removed delivered O2 percent from end breath data packet.
//
//  Revision: 009  By:  sp    Date: 12-Dec-1996    DR Number: DCS 1611
//       Project:  Sigma (R8027)
//       Description:
//             Added previous breath type to end exhalation data packet.
//
//  Revision: 008  By:  sp    Date: 24-Oct-1996    DR Number:
//       Project:  Sigma (R8027)
//       Description:
//             Inspection rework.
//
//  Revision: 007  By:  sp    Date:  16-Oct-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Removed unit test methods.
//
//  Revision: 006  By:  sp    Date:  1-Aug-1996    DR Number: DCS 967
//       Project:  Sigma (R8027)
//       Description:
//             Remove peepRecovery_ as part of end breath data.
//             Send peep revovery independently.
//
//  Revision: 005  By:  sp    Date:  24-Jun-1996    DR Number: DCS 967
//       Project:  Sigma (R8027)
//       Description:
//             Add peepRecovery_ as part of breath data to be sent.
//
//  Revision: 004  By:  sp    Date:  8-May-1996    DR Number:600
//       Project:  Sigma (R8027)
//       Description:
//             Rename SEND_END_EXH_PAUSE_DATA to SEND_BEGIN_EXP_PAUSE_DATA.
//             Update breath type at the end of exhlation.
//             Get the deliveredO2Percent from rFio2Monitor.
//
//  Revision: 003  By:  iv    Date:  4-Mar-1996    DR Number:674
//       Project:  Sigma (R8027)
//       Description:
//             Remove #ifndef.
//
//  Revision: 002  By:  sp    Date:  8-Feb-1996    DR Number:704
//       Project:  Sigma (R8027)
//       Description:
//             Add const SEND_END_INSP_DATA, SEND_END_EXH_DATA, SEND_END_EXH_PAUSE_DATA.
//             Add method sendPatientData.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number:
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "BreathData.hh"
#include "PatientCctTypeValue.hh"
#include "DiscreteValue.hh"

//@ Usage-Classes
#include "BreathRecord.hh"
#include "Btps.hh"
#include "MathUtilities.hh"
#include "PatientDataMgr.hh"
#include "Fio2Monitor.hh"
#include "VentObjectRefs.hh"
#include "BdQueuesMsg.hh"
#include "ManeuverRefs.hh"
#include "InspPauseManeuver.hh"
#include "ExpPauseManeuver.hh"
#include "NifManeuver.hh"
#include "P100Maneuver.hh"
#include "LungData.hh"
#include "PhasedInContextHandle.hh"
#include "PavManager.hh"
#include "BreathMiscRefs.hh"

#include "BreathDataPacket.hh"
#include "ComputedInspPauseDataPacket.hh"
#include "PavBreathDataPacket.hh"
#include "PavPlateauDataPacket.hh"
#include "PavStartupDataPacket.hh"

#include "VtpcvStateDataPacket.hh"
#include "VolumeTargetedManager.hh"
#include "VtpcvState.hh"

#include "ModeValue.hh"
#include "SupportTypeValue.hh"
#include "DynamicMechanics.hh"

#include "LeakCompMgr.hh"
#include "ProxEnabledValue.hh"
#include "ProxManager.hh"

#ifdef SIGMA_UNIT_TEST
#include <stdio.h>
#include "BreathData_UT.hh"
#endif // SIGMA_UNIT_TEST

#ifdef SIGMA_PAV_UNIT_TEST
extern Boolean SendPavData_UT ;
#endif //SIGMA_PAV_UNIT_TEST

//@ End-Usage

//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BreathData()
//
//@ Interface-Description
//		Default Constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
BreathData::BreathData(void)
{
    CALL_TRACE("BreathData(void)");

	for (Uint32 ii=0; ii < BUFFER_SIZE; ii++)
	{
		exhaledTvBuffer_[ii] = 0.0 ;
	}
	bufferIndex_ = 0 ;
	numPointsInBuffer_ = 0 ;
	averagedExhaledTv_ = 0.0 ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~BreathData
//
//@ Interface-Description
//		Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
BreathData::~BreathData()
{
    CALL_TRACE("~BreathData()");

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: update
//
//@ Interface-Description
// This method takes a reference to the BreathRecord to be used in
// updating the BreathData.  This method has no return value.
// It is called at the end of an inspiration or exhalation, or
// expiratory pause.  Depending upon the current PhaseType, the data
// of BreathRecord pertaining to that particular PhaseType is copied
// and sent to the GUI.
//---------------------------------------------------------------------
//@ Implementation-Description
//   All of the data members of this class must be updated when this
//   method is invoked.  The following may be copied directly from
//   the BreathRecord:
//   endInspiratoryPressure_
//   endExpiratoryPressure_
//   endExpiratoryPausePressure_
//   peakCircuitPressure_
//   breathDuration_
//   breathType_
//   by the BTPS correction factor exhaledTidalVolume_ must be compensated
//   for btps by multiplying by the BTPS correction factor ieRatio
//   must be calculated by inspiratoryTime_ / expiratoryTime_.
//   $[03007] $[03018] $[03022] $[03041] $[03045] $[03046]
//   $[03047] $[03050] $[03061]
//   $[BL03013]
// 	 $[TC03003]
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
void
BreathData::update(const BreathRecord& br)
{
    CALL_TRACE("update(BreathRecord& br)");



    // data to be updated at the end of inspiration
    if (BreathRecord::GetPhaseType() == BreathPhaseType::EXHALATION)
    {
        // $[TI1]
		endInspiratoryPressure_ = BreathRecord::GetEndInspPressurePatData();
        breathType_             = br.getBreathType();
        breathTypeDisplay_      = BreathRecord::GetBreathTypeDisplay();
        previousBreathType_     = BreathRecord::GetPreviousBreathType();
        breathDuration_         = br.getBreathDuration();
        peakCircuitPressure_    = BreathRecord::GetPeakAirwayPressure();

        inspiredLungVolume_     = RLungData.getInspLungBtpsVolume() ;

		supportType_            = (SupportTypeValue::SupportTypeValueId)PhasedInContextHandle::GetDiscreteValue(
										SettingId::SUPPORT_TYPE) ;
		mandType_               = (MandTypeValue::MandTypeValueId)PhasedInContextHandle::GetDiscreteValue(
										SettingId::MAND_TYPE) ;
		ventType_               = (VentTypeValue::VentTypeValueId)PhasedInContextHandle::GetDiscreteValue(
										SettingId::VENT_TYPE) ;
		breathTriggerId_        = BreathRecord::GetBreathTriggerId();
        // $[RM12017] PSF Rate of update: at the beginning of a spontaneous exhalation.
	    peakSpontInspFlow_      = RDynamicMechanics.getPeakSpontInspFlow();
        // $[RM12003] Cdyn Rate of update: At the start of exhalation...
		dynamicCompliance_      = RDynamicMechanics.getCompliance();
        // $[RM12006] Rdyn Rate of update: At the start of exhalation...
		dynamicResistance_      = RDynamicMechanics.getResistance();

	 	if (breathType_ == ::SPONT)
	 	{
	        // $[TI9]
			spontInspTime_ = breathDuration_ / 1000.0F ;
		}
        // $[TI10]
    }
    // data to be updated at the end of exhalation
    else if (BreathRecord::GetPhaseType() == BreathPhaseType::INSPIRATION)
    {
        // $[TI2]
		Real32 inspTime         = br.getInspTime() ;
        breathDuration_         = br.getBreathDuration();

        if (!IsEquivalent(breathDuration_ - inspTime, 0.0, TENTHS))
        {
	        // $[TI5]
	        ieRatio_ = inspTime / (breathDuration_ - inspTime);
		}	// implied else $[TI6]

        breathType_ = br.getBreathType();

        breathTypeDisplay_      = BreathRecord::GetBreathTypeDisplay();
        previousBreathType_     = BreathRecord::GetPreviousBreathType();
        endExpiratoryPressure_  = BreathRecord::GetEndExpPressurePatData();
		mandFraction_           = br.getMandatoryVolumeFraction() ;
        exhaledTidalVolume_     = br.getComplianceExhVolume();

		peakExpiratoryFlow_     = BreathRecord::GetPeakExpiratoryFlow();
        // $[RM12013] EEF Rate of update: at the beginning of inspiration.
		endExpiratoryFlow_      = BreathRecord::GetEndExpiratoryFlow();

		if (endExpiratoryFlow_ < 0.5 ||
			breathType_ == ::ASSIST ||
			breathType_ == ::SPONT)
		{
			// $[RM12410] If the measured exhalation phase is terminated by a patient trigger, then EEF shall be set to 0.0.
            // $[RM12411] If measured EEF is < 0.5, then EEF shall be set to 0.0.
			endExpiratoryFlow_   = 0.0;
		}

	 	if (previousBreathType_ == ::SPONT)
		{
	        // $[TI7]
			spontTiTtotRatio_ = inspTime / breathDuration_ ;
		}
        // $[TI8]
    }
    else if (BreathRecord::GetPhaseType() == BreathPhaseType::EXPIRATORY_PAUSE)
    {
        // $[TI3]
        expiratoryPausePressure_ = BreathRecord::GetEndExpPressurePatData();
    }
    else
    {
        CLASS_PRE_CONDITION(BreathRecord::GetPhaseType() != BreathPhaseType::NON_BREATHING);
    }


}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: sendPatientData
//
//@ Interface-Description
// This method takes an Int32 msg as an argument to determine which
// data packet to send to the GUI. This method has no return value.
// Depending upon the current msg, the data that is pertaining to
// that msg is copied to its data packet and sent to the GUI.
//---------------------------------------------------------------------
//@ Implementation-Description
// Depending upon the current msg, the data that is pertaining to
// that msg is copied to its data packet and sent to the GUI.
// $[03007] $[03022] $[03027]
//
//
//
// [03042] [P5.6.a][P5.6.d][P10.1.1.n][C7.6] [BL-MODIFIED][VC-MODIFIED] [PA-MODIFIED]
// Exhaled tidal volume (VTE) is calculated based on either
// a exhaled mandatory tidal volume (VTE MAND), if breath type = MANDATORY.
// b exhaled spontaneous tidal volume (VTE SPONT), if breath type = SPONT.
// d An exception exists during a mandatory exhalation when mode = BILEVEL and a spontaneous
//   breath has occurred (during the high PEEP phase.) In this case, VTE may be the sum of the
//   mandatory part and the spontaneous part of the exhaled volume.
// c VTE (n) shall be an average based on the last 5 full breaths or the number of breaths
//   since power up or`the number of breaths since the last time the average was reset such
//   that VTE(n) is the average unless the following condition resets the average:
//
//     ABS (tidal volume for current breath - VTE(n-1)) > 5 + 0.05 * VTE(n-1) ,
//        for ADULT circuits when spontaneous breath type is not PA
//     ABS (tidal volume for current breath - VTE(n-1)) > 5 + 0.15 * VTE(n-1) ,
//        for ADULT circuits when spontaneous breath type is PA
//     ABS (tidal volume for current breath - VTE(n-1)) > 5 + 0.05 * VTE(n-1) ,
//       for PEDIATRIC circuits
//     ABS (tidal volume for current breath - VTE(n-1)) > 1 + 0.10 * VTE(n-1) ,
//        for NEONATAL circuits
//
//     where:
//           VTE (n) is the displayed Exhaled tidal volume for the current breath
//           VTE (n-1) is the displayed Exhaled tidal volume for the previous breath or zero for the first breath
//           ABS is the absolute value
//---------------------------------------------------------------------
//@ PreCondition
//   BreathData::update has to be called first to update the data.
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
void
BreathData::sendPatientData(
	const Int32 msg
)
{
    CALL_TRACE("sendPatientData(const Int32 msg)");
    RealTimeDataPacket endInspDataPacket;
    BreathDataPacket endExhDataPacket;
    ComputedInspPauseDataPacket computedInspPauseDataPacket;
    BiLevelMandDataPacket mandDataPacket ;
    BiLevelMandExhDataPacket mandExhDataPacket ;



	switch (msg)
	{
	case BdQueuesMsg::SEND_END_INSP_DATA:
    {

		// $[TI1]

		Real32 inspiredLungVolTemp = inspiredLungVolume_;

        endInspDataPacket.breathType = breathType_;
        endInspDataPacket.breathTypeDisplay = breathTypeDisplay_ ;
        endInspDataPacket.endInspPressure = endInspiratoryPressure_;
        endInspDataPacket.phaseType = BreathPhaseType::EXHALATION ;

		// When PROX is enabled, subsitute the 840's inspired lung volume 
		// with PROX's inspired lung volume
        if (RProxManager.isAvailable())
        {
            inspiredLungVolTemp     = RProxManager.getInspiredVolume(); 
        }

        endInspDataPacket.inspiredlungVolume = inspiredLungVolTemp ;
        endInspDataPacket.supportType = supportType_ ;
        endInspDataPacket.mandType = mandType_ ;
        endInspDataPacket.peakCircuitPressure = peakCircuitPressure_;
        endInspDataPacket.spontInspTime = spontInspTime_;
        endInspDataPacket.ventType = ventType_;
        endInspDataPacket.breathTriggerId = breathTriggerId_;
		endInspDataPacket.dynamicCompliance = dynamicCompliance_;
		endInspDataPacket.dynamicResistance = dynamicResistance_;
		endInspDataPacket.peakSpontInspFlow = peakSpontInspFlow_;
		// A copy of inspiredLungVolume is required since this is used
		// to display Vti on the alarm setup subscreen.  The alarm setup
		// subcreen displays a different Vti label than the vital patient
		// area and the more data subscreen.
        endInspDataPacket.inspiredLungVolAlarm = inspiredLungVolTemp ;
        PatientDataMgr::NewRTBreathData(&endInspDataPacket,sizeof(endInspDataPacket));

#ifdef SIGMA_UNIT_TEST
cout << "Test Item 1: SEND_END_INSP_DATA\n";
#endif //SIGMA_UNIT_TEST
    }
	break;
	case BdQueuesMsg::SEND_END_EXH_DATA:
    {
    // $[TI2]
		const DiscreteValue  CIRCUIT_TYPE =
		   PhasedInContextHandle::GetDiscreteValue(SettingId::PATIENT_CCT_TYPE);

		Real32  rejectionLimit = 0;

		Real32 exhalationVolumeTemp = exhaledTidalVolume_;

		// When PROX is enabled, subsitute the 840's expired lung volume 
		// with PROX's expired lung volume
        if (RProxManager.isAvailable())
        {
			 exhalationVolumeTemp = RProxManager.getExpiredVolume();  // Confirm if btps is required.
        }



		// $[03042] -- reset criteria for running average...
		switch (CIRCUIT_TYPE)
		{
		case PatientCctTypeValue::ADULT_CIRCUIT :
 			if (PhasedInContextHandle::GetDiscreteValue(SettingId::MODE) ==
 					ModeValue::SPONT_MODE_VALUE &&
				PhasedInContextHandle::GetDiscreteValue(SettingId::SUPPORT_TYPE) ==
					SupportTypeValue::PAV_SUPPORT_TYPE)
			{
				rejectionLimit = (5.0F + 0.15F * averagedExhaledTv_);
			}
			else
			{
		  		rejectionLimit = (5.0F + 0.05F * averagedExhaledTv_);
			}
			break;

		case PatientCctTypeValue::PEDIATRIC_CIRCUIT :
			// $[TI2.7]
			rejectionLimit = (5.0F + 0.05F * averagedExhaledTv_);
			break;
		case PatientCctTypeValue::NEONATAL_CIRCUIT :
			// $[TI2.8]
			rejectionLimit = (1.0F + 0.10F * averagedExhaledTv_);
			break;
		default :
			// unexpected circuit type value...
			AUX_CLASS_ASSERTION_FAILURE(CIRCUIT_TYPE);
			break;
		}

    	if (ABS_VALUE( exhalationVolumeTemp - averagedExhaledTv_) > rejectionLimit)
    	{
        // $[TI2.1] -- reset averaging and use current (unaveraged) value...
    		bufferIndex_ = 0 ;
    		numPointsInBuffer_ = 0 ;
    	}
        // $[TI2.2]

    	exhaledTvBuffer_[bufferIndex_] = exhalationVolumeTemp ;

    	if (++bufferIndex_ >= BUFFER_SIZE)
    	{
        // $[TI2.3]
    		bufferIndex_ = 0 ;
    	}
        // $[TI2.4]

    	if (++numPointsInBuffer_ > BUFFER_SIZE)
    	{
        // $[TI2.5]
    		numPointsInBuffer_ = BUFFER_SIZE ;
    	}
        // $[TI2.6]

		averagedExhaledTv_ = 0.0 ;

    	for (Uint32 ii=0; ii < numPointsInBuffer_; ii++)
    	{
    		averagedExhaledTv_ += exhaledTvBuffer_[ii] ;
    	}
  		averagedExhaledTv_ /= numPointsInBuffer_ ;

        endExhDataPacket.time.now();
        endExhDataPacket.breathType = breathType_;
        endExhDataPacket.breathTypeDisplay = breathTypeDisplay_ ;
        endExhDataPacket.previousBreathType = previousBreathType_;
        endExhDataPacket.exhaledTidalVolume = averagedExhaledTv_ ;
        endExhDataPacket.breathDuration = breathDuration_;
        endExhDataPacket.endExpiratoryPressure = endExpiratoryPressure_;
        endExhDataPacket.ieRatio = ieRatio_;
        endExhDataPacket.spontInspTime = spontInspTime_;
        endExhDataPacket.spontTiTtotRatio = spontTiTtotRatio_;
        endExhDataPacket.phaseType = BreathPhaseType::INSPIRATION ;
		endExhDataPacket.mandFraction = mandFraction_ ;
		endExhDataPacket.peakExpiratoryFlow = peakExpiratoryFlow_;
		endExhDataPacket.endExpiratoryFlow = endExpiratoryFlow_;
		endExhDataPacket.percentLeak = RLeakCompMgr.getPercentLeak();
        endExhDataPacket.exhLeakRate  = RLeakCompMgr.getExhLeakRate();

	    // isLeakDetected is a Uint so it can be assigned to
	    // the DiscreteDataInfo data item IS_LEAK_DETECTED_ITEM
	    // without recasting/resizing in modifyData_()
        endExhDataPacket.isLeakDetected  = (Uint) RLeakCompMgr.isLeakDetected();
        endExhDataPacket.inspLeakVol  = RLeakCompMgr.getInspLeakVol();
        PatientDataMgr::NewEndBreathData(&endExhDataPacket,sizeof(endExhDataPacket));

#ifdef SIGMA_UNIT_TEST
cout << "Test Item 2: SEND_END_EXH_DATA\n";
#endif //SIGMA_UNIT_TEST
    }
	break;
	case BdQueuesMsg::SEND_BEGIN_EXP_PAUSE_DATA:
    {
        // $[TI3]
        PatientDataMgr::NewPeEndData(expiratoryPausePressure_);

#ifdef SIGMA_UNIT_TEST
cout << "Test Item 3: SEND_BEGIN_EXP_PAUSE_DATA\n";
#endif //SIGMA_UNIT_TEST
    }
	break;
	case BdQueuesMsg::SEND_BEGIN_INSP_PAUSE_DATA:
    {
        // $[TI5]
        Real32 data = 0.0 ;
        PatientDataMgr::NewPiEndData( data) ;
#ifdef SIGMA_UNIT_TEST
	BreathData_UT::SendBeginInspPauseData = TRUE ;
#endif //SIGMA_UNIT_TEST

    }
	break;
	case BdQueuesMsg::SEND_END_INSP_PAUSE_DATA:
    {
        // $[TI6]
        // Sending static compliance and resistance patient data
        computedInspPauseDataPacket.staticCompliance = RInspPauseManeuver.getComplianceValue();
        computedInspPauseDataPacket.staticComplianceValid = RInspPauseManeuver.getComplianceState();
        computedInspPauseDataPacket.staticResistance = RInspPauseManeuver.getResistanceValue();
        computedInspPauseDataPacket.staticResistanceValid = RInspPauseManeuver.getResistanceState();

        PatientDataMgr::NewComputedInspPauseData(&computedInspPauseDataPacket);

#ifdef SIGMA_UNIT_TEST
	BreathData_UT::SendEndInspPauseData = TRUE ;
#endif //SIGMA_UNIT_TEST
    }
	break;
	case BdQueuesMsg::SEND_END_EXP_PAUSE_DATA:
    {
        // $[TI9]
        // Sending end of exp pause pressure stability flag
        endExpPauseData_ = RExpPauseManeuver.getPressureStabilityState();
        PatientDataMgr::NewEndExpPauseData(endExpPauseData_);

#ifdef SIGMA_UNIT_TEST
	BreathData_UT::SendEndExpPauseData = TRUE ;
#endif //SIGMA_UNIT_TEST
    }
	break;
	case BdQueuesMsg::SEND_END_P100_PAUSE_DATA:
    {
	    // $[RM12309] P100 shall be displayed...
        PatientDataMgr::NewP100PressureData(RP100Maneuver.getP100Pressure()) ;
    }
	break;
	case BdQueuesMsg::SEND_END_NIF_PAUSE_DATA:
    {
        // Sending end of NIF pause pressure data
	    // $[RM12058] NIF result shall be displayed...
        PatientDataMgr::NewNifPressureData(RNifManeuver.getNifPressure()) ;
    }
	break;
	case BdQueuesMsg::SEND_VITAL_CAPACITY_DATA:
    {
		// $[RM12065] ...the Vte for the breath shall be displayed on the Waveforms screen as the VC.
        // $[RM12211] ...the Vte for the breath result shall be displayed on the VC maneuver subscreen.
        PatientDataMgr::NewVitalCapacityData(exhaledTidalVolume_);
    }
	break;
	case BdQueuesMsg::SEND_PEEP_RECV_DATA:
    {
        // $[TI4]
        PatientDataMgr::NewPeepRecovery();

#ifdef SIGMA_UNIT_TEST
cout << "Test Item 4: SEND_PEEP_RECV_DATA\n";
#endif //SIGMA_UNIT_TEST
    }
	break;
    case BdQueuesMsg::SEND_BL_MAND_DATA:
    {
        // $[TI7]
        mandDataPacket.breathType = breathType_;
        mandDataPacket.breathTypeDisplay = breathTypeDisplay_ ;
        mandDataPacket.endExpiratoryPressure = endExpiratoryPressure_;
        mandDataPacket.peakCircuitPressure = peakCircuitPressure_;
        mandDataPacket.phaseType = BreathPhaseType::INSPIRATION ;

		PatientDataMgr::NewBiLevelMandData( &mandDataPacket, sizeof(mandDataPacket)) ;

#ifdef SIGMA_UNIT_TEST
	BreathData_UT::SendBlMandData = TRUE ;
#endif //SIGMA_UNIT_TEST

    }
	break;
	case BdQueuesMsg::SEND_BL_MAND_EXH_DATA:
    {
        // $[TI8]
        mandExhDataPacket.breathType = breathType_;
        mandExhDataPacket.breathTypeDisplay = breathTypeDisplay_;
        mandExhDataPacket.phaseType = BreathPhaseType::EXHALATION ;
        mandExhDataPacket.peakCircuitPressure = peakCircuitPressure_;
		PatientDataMgr::NewBiLevelMandExhData( &mandExhDataPacket, sizeof(mandExhDataPacket)) ;

#ifdef SIGMA_UNIT_TEST
	BreathData_UT::SendBlMandExhData = TRUE ;
#endif //SIGMA_UNIT_TEST

    }
	break;
	case BdQueuesMsg::SEND_PAV_BREATH_DATA:
    {
        // $[TI10]
        PavBreathDataPacket  pavBreathDataPacket;

        pavBreathDataPacket.patientWorkOfBreathing = RPavManager.getPatientWob();
        pavBreathDataPacket.totalWorkOfBreathing = RPavManager.getTotalWob();
        pavBreathDataPacket.elastanceWob = RPavManager.getPatientElastanceWob();
        pavBreathDataPacket.resistiveWob = RPavManager.getPatientResistiveWob();
        pavBreathDataPacket.intrinsicPeep = RPavManager.getIntrinsicPeep() ;
        pavBreathDataPacket.pavState = RPavManager.getPavState() ;

        PatientDataMgr::NewPavBreathData(&pavBreathDataPacket);

#ifdef SIGMA_PAV_UNIT_TEST
        SendPavBreathData_UT = TRUE ;
#endif //SIGMA_PAV_UNIT_TEST
    }
	break;
	case BdQueuesMsg::SEND_PAV_PLATEAU_DATA:
    {
        // $[TI11]
    	PavPlateauDataPacket  pavPlateauDataPacket;

    	const Real32  ELASTANCE =
			RPavManager.getEPatient(PavState::CLOSED_LOOP);

        pavPlateauDataPacket.lungElastance  = ELASTANCE * 1000.0F;
        pavPlateauDataPacket.lungCompliance = 1.0F / ELASTANCE;

        pavPlateauDataPacket.patientResistance =
				    // cmH2O/lpm to cmH2O/lps
					RPavManager.getRPatient(PavState::CLOSED_LOOP) * 60.0F ;
        pavPlateauDataPacket.totalResistance =
				    // cmH2O/lpm to cmH2O/lps
					RPavManager.getRtotal() * 60.0F ;

        pavPlateauDataPacket.pavState = RPavManager.getPavState();

        PatientDataMgr::NewPavPlateauData(&pavPlateauDataPacket);

#ifdef SIGMA_PAV_UNIT_TEST
        SendPavPlateauData_UT = TRUE ;
#endif //SIGMA_PAV_UNIT_TEST
    }
	break;
	case BdQueuesMsg::SEND_PAV_STARTUP_DATA:
    {
        // $[TI11]
    	PavStartupDataPacket  pavStartupDataPacket;

        pavStartupDataPacket.pavState = RPavManager.getPavState();

        PatientDataMgr::NewPavStartupData(&pavStartupDataPacket);

#ifdef SIGMA_PAV_UNIT_TEST
        SendPavStartupData_UT = TRUE ;
#endif //SIGMA_PAV_UNIT_TEST
    }
	break;
	case BdQueuesMsg::SEND_VTPCV_STATE:
    {
	 VtpcvStateDataPacket  vtpcvStateDataPacket;
	 VolumeTargetedManager::TestBreathType vtpcvBreath =
	 	RVolumeTargetedManager.getTestBreathState();
	 if( vtpcvBreath == VolumeTargetedManager::VT_BASED ||
	     vtpcvBreath == VolumeTargetedManager::VS_BASED )
	 {
	 	vtpcvStateDataPacket.vtpcvState = VtpcvState::NORMAL_VTPCV;
	 }
	 else
	 {
	 	vtpcvStateDataPacket.vtpcvState = VtpcvState::STARTUP_VTPCV;
	 }
   	 PatientDataMgr::NewVtpcvStateData(&vtpcvStateDataPacket);
    }
	break;
	default:
        AUX_CLASS_ASSERTION_FAILURE(msg);
		break;
	}
}

#ifdef SIGMA_UNIT_TEST
void
BreathData::write( void )
{
    printf("===== Class BreathData =====\n");
    printf("breathDuration_:%f\n",breathDuration_);
    printf("exhaledTidalVolume_:%f\n",exhaledTidalVolume_);
    printf("endInspiratoryPressure_:%f\n",endInspiratoryPressure_);
    printf("endExpiratoryPressure_:%f\n",endExpiratoryPressure_);
    printf("ieRatio_:%f\n",ieRatio_);
    printf("peakCircuitPressure_:%f\n",peakCircuitPressure_);
    printf("breathType_: %d\n",(int) breathType_);
}

#endif // SIGMA_UNIT_TEST



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getExhaledTidalVolume()
//
//@ Interface-Description
// 		This method has no arguments.  It simply returns the
//      exhaledTidalVolume_.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Return exhaledTidalVolume_.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
Real32
BreathData::getExhaledTidalVolume( void)
{
	CALL_TRACE("BreathData::getExhaledTidalVolume( void)") ;

	return( exhaledTidalVolume_) ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getInspiredLungVolume()
//
//@ Interface-Description
// 		This method has no arguments.  It simply returns the
//      inspiredLungVolume_.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Return inspiredLungVolume_.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
Real32
BreathData::getInspiredLungVolume( void)
{
	CALL_TRACE("BreathData::getInspiredLungVolume( void)") ;

	return( inspiredLungVolume_) ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//      'lineNumber' are essential pieces of information.  The 'pFileName'
//      and 'pPredicate' strings may be defaulted in the macro to reduce
//      code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
BreathData::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
                   const char*        pFileName,
                   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, BREATHDATA,
			  lineNumber, pFileName, pPredicate);
}

