#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
//@Filename: BdIoUtil - BD IO Utilities 
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/POST-Library/vcssrc/BdIoUtil.ccv   25.0.4.0   19 Nov 2013 14:16:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002   By: Gary Cederquist  Date: 15-AUG-1997  DR Number: 1875
//    Project:   Sigma   (R8027)
//    Description:
//       Added testable item annotations.
//
//  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//=====================================================================
#include "BdIoUtil.hh"
#include "CpuDevice.hh"
#include <stdio.h>

//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Free-Function: ReadWord
//
//@ Interface-Description
//    This method takes Uint16 address pointer as an argument 
//    and returns a Uint16 data read from the argument.
//---------------------------------------------------------------------
//@ Implementation-Description
//    Read Uint16 data from the address.
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Free-Function
//=====================================================================
Uint16
ReadWord(volatile Uint16 * const address)
{
    // $[TI1]
    return *address;
}
 
//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Free-Function: ReadByte
//
//@ Interface-Description
//    This method takes Uint16 address pointer as an argument 
//    and returns a Uint8 data read from the argument.
//---------------------------------------------------------------------
//@ Implementation-Description
//    Read a byte data from the address. 
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Free-Function
//=====================================================================
Uint8
ReadByte(volatile Uint16 * const address)
{
    // $[TI1]
    return *(volatile Uint8*)address;
}

//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Free-Function: WriteByte
//
//@ Interface-Description
//    This method takes Uint16 address pointer and Uint8 data as arguments.
//    This method has no return type.
//---------------------------------------------------------------------
//@ Implementation-Description
//    Write a byte of data to the address.
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Free-Function
//=====================================================================
void
WriteByte(volatile Uint16 * const address, const Uint8 data)
{
    // $[TI1]
	*(volatile Uint8*)address = data;
}

//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Free-Function: WriteWord
//
//@ Interface-Description
//    This method takes Uint16 address pointer and Uint16 data as arguments.
//    This method has no return type.
//---------------------------------------------------------------------
//@ Implementation-Description
//    Write Uint16 data to the address.
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Free-Function
//=====================================================================
void
WriteWord(volatile Uint16 * const address, const Uint16 data)
{
    // $[TI1]
	*address = data;
}

//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Free-Function: ActivateVentInop
//
//@ Interface-Description
//    This method takes no argument and has no return value.
//    It places the ventilator in the VENT-INOP state.
//---------------------------------------------------------------------
//@ Implementation-Description
//    This method initiates a VENT-INOP state by activating VENT-INOP
//    through the hardware safety-net.  
//---------------------------------------------------------------------
//@ PreCondition
//    None
//---------------------------------------------------------------------
//@ PostCondition
//    None
//
//@ End-Free-Function
//=====================================================================
void 
ActivateVentInop(void)
{
   if ( CpuDevice::IsBdCpu() )
   {
       // $[TI1.1]
       //  the following commands to the redundant safety net 
       //  registers will activate safe state to open the safety 
       //  valve, open the exhalation valve and close the PSOLs
       //  In addition, setting VENT-INOP will disable software 
       //  control of the pneumatics.
       *SAFETY_NET_WRITE_REGA = FORCE_VENTINOP_A;    
       *SAFETY_NET_WRITE_REGB = FORCE_VENTINOP_B;    
    
       SwitchBdAudioAlarm( TRUE );
       SwitchBdLeds( TRUE );
   }
   // $[TI1.2]
}

//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Free-Function: ActivateSafeState
//
//@ Interface-Description
//    This method takes no argument and has no return value.
//    It places the ventilator in the Safe State.
//---------------------------------------------------------------------
//@ Implementation-Description
//    This method initiates a SAFE-STATE by activating SAFE-STATE
//    through the hardware safety-net.  It then turns on the audio
//    alarm and the SVO LED.
//---------------------------------------------------------------------
//@ PreCondition
//    None
//---------------------------------------------------------------------
//@ PostCondition
//    None
//@ End-Free-Function
//=====================================================================
void 
ActivateSafeState(void)
{
   if ( CpuDevice::IsBdCpu() )
   {
       // $[TI1.1]
       //  The following commands to the redundant safety net 
       //  registers will activate safe state to open the safety 
       //  valve, open the exhalation valve and close the PSOLs
       //  This command also has the side effect of deactivating 
       //  a software initiated VENT-INOP condition.
       *SAFETY_NET_WRITE_REGA = SAFE_STATE_A;    
       *SAFETY_NET_WRITE_REGB = SAFE_STATE_B;    

       SwitchBdAudioAlarm( TRUE );
       *VENT_CONTROL_STATUS = SVO_LED_ON;
   }
   // $[TI1.2]
}

//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Free-Function: SwitchBdAudioAlarm
//
//@ Interface-Description
//    This function activates or deactivates the BD audio alarm 
//    according to the boolean state parameter.   If state is TRUE,
//    the alarm is turned ON, otherwise it is turned OFF.
//
//    WARNING: This function has the side effect of forcing the
//    ventilator in the safe state with the safety valve open and
//    the heater off as well as turning the alarm on or off.
//
//---------------------------------------------------------------------
//@ Implementation-Description
//    See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//    None
//---------------------------------------------------------------------
//@ PostCondition
//    None
//
//@ End-Free-Function
//=====================================================================
void 
SwitchBdAudioAlarm(Boolean state)
{
    if ( CpuDevice::IsBdCpu() )
    {
        // $[TI1.1]
        if ( state )
        {
            // $[TI2.1]
            //  Safe state configuration includes turning on the BD alarm
            *WRITE_IO_PORT = SAFE_STATE_CMD;
        }
        else
        {
            // $[TI2.2]
            *WRITE_IO_PORT = SAFE_STATE_CMD | BD_AUDIO_ALARM_OFF;
        }
    }
    // $[TI1.2]
}


//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Free-Function: SwitchBdLeds
//
//@ Interface-Description
//    This function activates or deactivates the BD front panel LEDs 
//    for POST according to the boolean state parameter.  TRUE turns on
//    the SVO and VENT-INOP LEDs.  The Loss of User Interface LEDs are
//    left ON during POST so this function turns on the LOUI LEDs
//    regardless of the state parameter.
//
//---------------------------------------------------------------------
//@ Implementation-Description
//    See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//    None
//---------------------------------------------------------------------
//@ PostCondition
//    None
//
//@ End-Free-Function
//=====================================================================
void 
SwitchBdLeds(Boolean state)
{
    if ( CpuDevice::IsBdCpu() )
    {
        // $[TI1.1]
        if (state)
        {
           // $[TI2.1]
           //  Turn off SVO LED and VENT INOP LED
           //  Keep the Loss of User interface LEDs ON during POST
           *VENT_CONTROL_STATUS = SVO_LED_ON | VENT_INOP_LED_ON | LOUI_LED1_ON;
           *POWER_SUPPLY_WRITE_PORT |= LOUI_LED2_ON;
        }
        else
        {
           // $[TI2.2]
           //  Turn off SVO LED and VENT INOP LED
           //  Keep the Loss of User interface LEDs ON during POST
           *VENT_CONTROL_STATUS = LOUI_LED1_ON;
           *POWER_SUPPLY_WRITE_PORT |= LOUI_LED2_ON;
        }
    }
    // $[TI1.2]
}
