#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sotred in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================


// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: ModeTriggerMediator - Maintains a pointer to the active ModeTrigger list.
//---------------------------------------------------------------------
//@ Interface-Description
//     Every Bd cycle, a method in this class is invoked that causes the
//     mode triggers on the active list to be evaluated.  If one of the
//     triggers fires, causing the active BreathPhaseScheduler to be
//     changed, this class will be notified of the next list to set
//     as the active list.  The scheduler must request to set the enable
//     state of the mode triggers on its list before setting the list to
//     be active.
//---------------------------------------------------------------------
//@ Rationale
//     This class is necessary to provide a place to keep a pointer to
//     the mode trigger list that is active, and to provide a newCycle()
//     method as a means of evaluating the mode triggers every BD cycle.
//---------------------------------------------------------------------
//@ Implementation-Description
//     This class contains two data members that point to lists of 
//     pointers to mode triggers.  One pointer points to the currently
//     active list, and the other points to the next list to activate.  
//     The pointer to the next list is set whenever a ModeTrigger fires,
//     causing the active BreathPhaseScheduler to change.  There are
//     methods provided to traverse the list of ModeTriggers, and cause
//     them to be evaluated, and to change the pointer to the active
//     list.  Before the next list is activated, all triggers on the
//     current list are disabled.  The enabling of the triggers is
//     handled by other objects.
//---------------------------------------------------------------------
//@ Fault-Handling
//     n/a.
//---------------------------------------------------------------------
//@ Restrictions
//     The pointer to the active ModeTrigger list must be set before
//     the list is evaluated.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/ModeTriggerMediator.ccv   25.0.4.0   19 Nov 2013 13:59:54   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 002  By:  sp    Date:  4-Mar-1996    DR Number:674
//       Project:  Sigma (R8027)
//       Description:
//             Remove #ifndef.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "ModeTriggerMediator.hh"


//@ Usage-Classes
#include "ModeTrigger.hh"

#include "BreathPhaseScheduler.hh"

//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ModeTriggerMediator 
//
//@ Interface-Description
//		Constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
ModeTriggerMediator::ModeTriggerMediator(void)
{
	CALL_TRACE("ModeTriggerMediator(void)");

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~ModeTriggerMediator 
//
//@ Interface-Description
//		Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
ModeTriggerMediator::~ModeTriggerMediator()
{
   CALL_TRACE("~ModeTriggerMediator()");
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newCycle
//
//@ Interface-Description
//	This method takes no parameters, and returns no values.  It is invoked
//	every BD cycle to evaluate all triggers on the list.  If one of the
//	triggers has fired the enable status of the triggers on the active list
//	must be checked before the list pointer is changed to point to the new
//	list.  Once the list is changed, the triggers on the newly activated
//	list must then have their enable status evaluated.  The order in which
//	this is done is important.  During the processing of
//	checkActiveTriggers_(), whenever a mode trigger is evaluated to true, the
//	current scheduler is instructed to relinquish control, and the new
//	scheduler is instructed to take control. When relinquishing control, all
//	triggers on the scheduler list set their trigger isDisableRequested_ status 
//	to false. When taking control, all triggers on the acheduler list
//	set their trigger isEnableRequested_ status to true. The
//	setTriggerStatus_() method goes over the list of mode triggers of a given
//	scheduler and determine their enable status based on the isEnableRequested_
//	and isDisableRequested_ attributes. This method is called twice -
//	once for the list of triggers of the scheduler that is relinquishing
//	control and once for the list of triggers of the scheduler that is taking
//	control.
//---------------------------------------------------------------------
//@ Implementation-Description
//	The method checkActiveTriggers() is invoked to determine if any of the
//	triggers have fired. If they have, it is necessary to change the currently
//	active list to the next list desired.  The method changeListPtr() is called
//	to change the pointer to the currently active list, to the new desired
//	list. The method setTriggerStatus_() is called twice - once before the
//	pointer to the mode triggers has changed and once after the pointer to the
//	new list of mode triggers has been changed. This guarantees that all
//	triggers involved - the previously active triggers and the current triggers
//	- are processed.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
ModeTriggerMediator::newCycle(void)
{
    CALL_TRACE("newCycle(void)");

    Boolean isTrigger = checkActiveTriggers_(); 

    // if one of the triggers has fired causing a new Mode  
    // to be active, then change the active trigger list accordingly.
    if(isTrigger)
    {
	// $[TI1]
	//the following order of 3 statements is important	
	setTriggerStatus_();
        changeListPtr_();
	setTriggerStatus_();
    }
    // $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: resetTriggerList
//
//@ Interface-Description
//    This method takes no arguments, and has no return value.  It may be called by any
//    object desiring to reset all triggers on the active trigger list.
//---------------------------------------------------------------------
//@ Implementation-Description
//    This method uses a while loop to traverse the currently active 
//    list of ModeTriggers.  Each trigger isDisableRequested_ status is set to true.
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
void
ModeTriggerMediator::resetTriggerList(void)
{
	Int16 i = 0;
	while (pModeList_[i] != NULL)
	{
		// $[TI1]
		pModeList_[i]->setIsDisableRequested(TRUE);
		i++;
	}
	// $[TI2] not testable, the first element of the list can never be NULL
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
 
void
ModeTriggerMediator::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
                   const char*        pFileName,
                   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, MODETRIGGERMEDIATOR,
                          lineNumber, pFileName, pPredicate);
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//=====================================================================
//
//  Private Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: checkActiveTriggers_
//
//@ Interface-Description
//     This method is called every BD cycle to determine if any mode
//     transitions need to occur.  This method accepts no parameters. 
//     It returns True if any of the triggers has fired, otherwise, it
//     returns false.
//---------------------------------------------------------------------
//@ Implementation-Description
//     Each trigger on the active mode trigger list is evaluated until
//     either the end of the list is reached, or one of the triggers
//     returns TRUE.
// $[04117]
//---------------------------------------------------------------------
//@ PreCondition
//     pModeList != NULL
//---------------------------------------------------------------------
//@ PostCondition
//              none
//@ End-Method
//=====================================================================
Boolean
ModeTriggerMediator::checkActiveTriggers_(void) const
{
    CALL_TRACE("checkActiveTriggers_(void)");
 
    CLASS_PRE_CONDITION(pModeList_ != NULL);

    Int16 i = 0;
    Boolean terminate = FALSE;
    while (pModeList_[i] != NULL && !terminate)
    {
       // $[TI1]
       terminate = pModeList_[i]->determineState();
       i++;
    }
    // This test case can never be hit because of the class pre-condition $[TI2]

    // TRUE: $[TI3]
    // FALSE: $[TI4]
    return (terminate);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setTriggerStatus_
//
//@ Interface-Description
//	This method takes no argument and returns no value. The method traverses all
//	triggers on the active trigger list and checks their enable/disable request
//	status. These attributes have been set previously by the scheduler that
//	relinquishes control and by the scheduler that takes control. A scheduler
//	that relinquishes control sets the "request to disable" attribute of all
//	its triggers to TRUE. A scheduler that takes control sets the "request to
//	enable" attributes of all its triggers to TRUE.  Thus if both attributes
//	are set to TRUE - the trigger is neither enabled nor disabled - this allows
//	a mode trigger to be active (enabled) accross different modes. When the "request to
//	disable" attribute is TRUE and the "request to enable" attribute is FALSE -
//	the trigger is disabled since it was on the active list of the exiting
//	scheduler and it is not on the active list of the incoming scheduler. When
//	the "request to enable" attribute is TRUE and the "request to disable"
//	attribute is FALSE - the trigger is enabled since it was not on the active
//	list of the exiting scheduler but it is on the active list of the incoming
//	scheduler, thus it needs to be initialized.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method enable or disable the mode triggers in pModeList_
//	based on the conditions of isEnableRequested and isDisableRequested.
//	After the enable trigger attribute is determined, both "request to enable"
//	and "request to disable" attributes are set to FALSE. 
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
ModeTriggerMediator::setTriggerStatus_(void)
{
    CALL_TRACE("ModeTriggerMediator::setTriggerStatus_(void)");

    Int16 i = 0;
    Boolean disableRequested;
    Boolean enableRequested;

    // process triggers pointed to by the current list pointer
    while (pModeList_[i] != NULL)
    {
        enableRequested = pModeList_[i]->isEnableRequested();
        disableRequested = pModeList_[i]->isDisableRequested();

        if (disableRequested && enableRequested)
        {
	    // $[TI1]
            // do not change the trigger status
            // reset the enable/disable requests
            pModeList_[i]->setIsEnableRequested(FALSE);
            pModeList_[i]->setIsDisableRequested(FALSE);
        }
        else if (disableRequested && !enableRequested)
        {
	    // $[TI2]
            // disable the trigger:
            pModeList_[i]->disable();
            // reset the enable/disable requests
            pModeList_[i]->setIsEnableRequested(FALSE);
            pModeList_[i]->setIsDisableRequested(FALSE);
        }
        else if (!disableRequested && enableRequested)
        {
	    // $[TI3]
            // enable the trigger:
            pModeList_[i]->enable();
            // reset the enable/disable requests
            pModeList_[i]->setIsEnableRequested(FALSE);
            pModeList_[i]->setIsDisableRequested(FALSE);
        }
        else // no request is pending
        {
	    // $[TI4]
            CLASS_PRE_CONDITION(!disableRequested && !enableRequested);
        }
	i++;
    }
}
