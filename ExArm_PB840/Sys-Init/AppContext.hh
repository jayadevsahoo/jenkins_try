#ifndef AppContext_HH
#define AppContext_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//====================================================================
 
//====================================================================
// Class:  AppContext - Sys-Init subsystem class
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Sys-Init/vcssrc/AppContext.hhv   25.0.4.0   19 Nov 2013 14:33:48   pvcs  $
//
//@ Modification-Log
//
//   Revision 002   By:   sah      Date: 01/13/99    DR Number: 5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to a non-inlined method.
//
//   Revision 001   By:   gdc      Date: 05/16/95    DR Number:   n/a
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//====================================================================
 
#if 0  
// for bug in genDD
#endif

//@ Usage-Classes
#include "Sys_Init.hh"
#include "MsgQueue.hh"
//@ End-Usage
 
class CommDownMessage;
class CommUpMessage;
class ChangeStateMessage;
class BdReadyMessage;
class BdFailedMessage;
class GuiReadyMessage;
class StartupTimer;
class SyncClockMessage;
class SyncClockTimer;
class TaskReadyMessage;
class TransitionTimer;

class AppContext 
{
  public:
    typedef void (* CommDownCallbackPtr)(const CommDownMessage& rMessage);
    typedef void (* CommUpCallbackPtr)(const CommUpMessage& rMessage);
    typedef void (* ChangeStateCallbackPtr)(const ChangeStateMessage& rMessage);
    typedef void (* BdReadyCallbackPtr)(const BdReadyMessage& rMessage);
    typedef void (* GuiReadyCallbackPtr)(const GuiReadyMessage& rMessage);
    typedef void (* SyncClockCallbackPtr)(const SyncClockMessage& rMessage);
    typedef void (* StartupTimerCallbackPtr)(const StartupTimer& rMessage);
    typedef void (* SyncClockTimerCallbackPtr)(const SyncClockTimer& rMessage);
    typedef void (* TransitionTimerCallbackPtr)(const TransitionTimer& rMessage);
    typedef void (* TaskReadyCallbackPtr)(const TaskReadyMessage& rMessage);
    typedef void (* BdFailedCallbackPtr)(const BdFailedMessage& rMessage);

    AppContext(MsgQueue* pMsgQueue = NULL);
    ~AppContext();

    const char*          nameOf(void) const;
    Int32                nextEvent(void);
    Boolean              pending(void);
    void                 dispatchEvent(Int32 queueEvent);
    void                 mainLoop(void);

    void  setCallback(CommDownCallbackPtr pCallback);
    void  setCallback(CommUpCallbackPtr pCallback);
    void  setCallback(ChangeStateCallbackPtr pCallback);
    void  setCallback(BdReadyCallbackPtr pCallback);
    void  setCallback(GuiReadyCallbackPtr pCallback);
    void  setCallback(SyncClockCallbackPtr pCallback);
    void  setCallback(StartupTimerCallbackPtr pCallback);
    void  setCallback(SyncClockTimerCallbackPtr pCallback);
    void  setCallback(TransitionTimerCallbackPtr pCallback);
    void  setCallback(TaskReadyCallbackPtr pCallback);
    void  setCallback(BdFailedCallbackPtr pCallback);

    void dispatch(const CommDownMessage& rMessage) const;
    void dispatch(const CommUpMessage& rMessage) const;
    void dispatch(const ChangeStateMessage& rMessage) const;
    void dispatch(const BdReadyMessage& rMessage) const;
    void dispatch(const GuiReadyMessage& rMessage) const;
    void dispatch(const SyncClockMessage& rMessage) const;
    void dispatch(const StartupTimer& rMessage) const;
    void dispatch(const SyncClockTimer& rMessage) const;
    void dispatch(const TransitionTimer& rMessage) const;
    void dispatch(const TaskReadyMessage& rMessage) const;
    void dispatch(const BdFailedMessage& rMessage) const;

    static void          SoftFault(const SoftFaultID  softFaultID,
				   const Uint32       lineNumber,
				   const char*        pFileName = NULL,
				   const char*        pPredicate = NULL);

#ifdef SIGMA_UNIT_TEST
    friend class AppContext_UT;
#endif

  private:
    AppContext(const AppContext& rhs);          // declared only
    void operator=(const AppContext& rhs);      // declared only

    //@ Data-Member:    pMsgQueue_
    //  Pointer to the MsgQueue used to receive messages for nextEvent()
    //  and pending().
    MsgQueue*           pMsgQueue_;

    //@ Data-Member:    pCommDownCallback_
    //  Pointer to the applications's CommDownMessage callback routine.
    CommDownCallbackPtr    pCommDownCallback_;

    //@ Data-Member:    pCommUpCallback_
    //  Pointer to the applications's CommUpMessage callback routine.
    CommUpCallbackPtr      pCommUpCallback_;

    //@ Data-Member:    pChangeStateCallback_
    //  Pointer to the applications's ChangeStateMessage callback routine.
    ChangeStateCallbackPtr pChangeStateCallback_;

    //@ Data-Member:    pBdReadyCallback_
    //  Pointer to the applications's BdReadyMessage callback routine.
    BdReadyCallbackPtr   pBdReadyCallback_;

    //@ Data-Member:    pGuiReadyCallback_
    //  Pointer to the applications's GuiReadyMessage callback routine.
    GuiReadyCallbackPtr   pGuiReadyCallback_;

    //@ Data-Member:    pSyncClockCallback_
    //  Pointer to the applications's SyncClockMessage callback routine.
    SyncClockCallbackPtr   pSyncClockCallback_;

    //@ Data-Member:    pStartupTimerCallback_
    //  Pointer to the applications's StartupTimer callback routine.
    StartupTimerCallbackPtr   pStartupTimerCallback_;

    //@ Data-Member:    pSyncClockTimerCallback_
    //  Pointer to the applications's SyncClockTimer  callback routine.
    SyncClockTimerCallbackPtr   pSyncClockTimerCallback_;

    //@ Data-Member:    pTransitionTimerCallback_
    //  Pointer to the applications's TransitionTimer callback routine.
    TransitionTimerCallbackPtr  pTransitionTimerCallback_;

    //@ Data-Member:    pTaskReadyCallback_
    //  Pointer to the applications's TaskReady callback routine.
    TaskReadyCallbackPtr        pTaskReadyCallback_;

    //@ Data-Member:    pBdFailedCallback_
    //  Pointer to the applications's TaskReady callback routine.
    BdFailedCallbackPtr         pBdFailedCallback_;

};
 

#endif // AppContext_HH
