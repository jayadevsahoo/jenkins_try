#include "stdafx.h"
//====================================================================
// This is a proprietary work to which Covidien corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2010, Covidien, Inc.
//====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: VsetText - Manages text strings for VSET API
//---------------------------------------------------------------------
//@ Interface-Description
//  This class is designed to translate "simple text" that the GUI
//  uses to define strings for display on the GUI into strings that
//  can be used for status messages in the VSET API. This allows the 
//  VsetServer class to accept the same "simple text" strings for bounds 
//  violation messsages that the Settings-Validation sub-system uses and
//  translate them to a simple ASCII text string suitable as a status
//  message in the VSET STAT message.
//---------------------------------------------------------------------
//@ Rationale
//  Encaposulates the translation of "simple text" to VSET API status.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Serial-Interface/vcssrc/VsetText.ccv   25.0.4.0   19 Nov 2013 14:12:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001   By: gdc    Date: 16-Aug-2010     SCR Number: 6663
//  Project: API/MAT
//  Description:
//  Initial implementation to support VentSet API.
//=====================================================================

#include "VsetText.hh"
#include <string.h>
#include <ctype.h>

//@ Usage-Classes
#include <iostream>
using namespace std;

//@ End-Usage

//@ Macro:  TEXT_FIELD_ASSERTION_FAILURE()
// This macro provides a forced assertion identified for the VsetText class.
#define TEXT_FIELD_ASSERTION_FAILURE()	\
{	\
	dumpTextString_(); \
	CLASS_ASSERTION_FAILURE(); \
}

//@ Macro:  AUX_TEXT_FIELD_ASSERTION_FAILURE(auxErrorCode)
// This macro provides a forced assertion identified for the VsetText class.
#define AUX_TEXT_FIELD_ASSERTION_FAILURE(auxErrorCode)	\
{	\
	dumpTextString_(); \
	AUX_CLASS_ASSERTION_FAILURE(auxErrorCode); \
}

//@ Macro:  TEXT_FIELD_ASSERTION(boolExpr)
// This macro provides a test of the assertion identified by 'boolExpr' for
// the VsetText class.
#define TEXT_FIELD_ASSERTION(boolExpr)	\
{	\
	if (!(boolExpr))	\
	{	\
		dumpTextString_(); \
		CLASS_ASSERTION(boolExpr); \
	}	\
}

//@ Code...
//
// Initialization of static data members...
//

//=====================================================================
//
// Public methods
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: VsetText
//
//@ Interface-Description
// This constructor initializes the VsetText object.  The passed "pString"
// pointer must point to the text to be displayed, or it must be NULL.  The
// text to be displayed (if any) must be in the "Message String" format.  This
// text is copied into internal private storage.  The size of the VsetText is
// automatically set to just enclose the specified text.
//---------------------------------------------------------------------
//@ Implementation-Description
// The passed "pString" pointer is passed to the setSimpleText() method.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
VsetText::VsetText(const char *pString) :
simpleTextIndex_(0),
serialTextIndex_(0)
{
	setSimpleText(pString);
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: VsetText
//
//@ Interface-Description
// This is a default constructor with no argument passed.
//---------------------------------------------------------------------
//@ Implementation-Description
// It serves as the default constructor that initializes a NULL message string.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
VsetText::VsetText(void)
{
	setSimpleText(NULL);
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~VsetText() 
//
//@ Interface-Description
// Destructor. Destroys object.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This destructor does nothing.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

VsetText::~VsetText(void)
{
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setSimpleText
//
//@ Interface-Description
// Changes the text to be output to serial port.  The "pString" parameter
// points to the text that will be converted to a serial output string.
//---------------------------------------------------------------------
//@ Implementation-Description
// If the passed "pString" is NULL, sets the private simpleText_ array to a null
// string.  Otherwise, the text pointed to by pString is copied into the
// simpleText_ until a null character is encountered.  
//---------------------------------------------------------------------
//@ PreCondition
//	"pString" must be NULL, or it must point to a text string of length
// less than or equal to MAX_STRING_LENGTH.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void VsetText::setSimpleText(const char * pString)
{
	simpleText_ = pString;
	serialText_[0] = '\0';
	convert_();
}


//=====================================================================
//
// Private methods
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: convert_
//
//@ Interface-Description
// This method converts the simple text string to a string used by the 
// VsetServer to output to the external settings controller.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
VsetText::convert_(void)
{
	simpleTextIndex_ = 0;
	serialTextIndex_ = 0;

	if ( simpleText_ == NULL || simpleText_[0] == '\0' )
	{
		serialText_[0] = '\0';
	}
	else
	{
		Int len = strlen(simpleText_);
		if ( simpleText_[0] != '{' || simpleText_[len-1] != '}' )
		{
			// input string doesn't conform to standard - don't convert
			strncpy(serialText_, simpleText_, sizeof(serialText_));
		}
		else
		{
			Boolean isTextSubscripted = FALSE;
			renderSerialText_(isTextSubscripted);
		}
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: renderSerialText_
//
//@ Interface-Description
// Parse the "simple text" string and convert to a suitable ascii string
// equivalent for output to the the API/PC.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void 
VsetText::renderSerialText_(Boolean isTextSubscripted)
{
	simpleTextIndex_++;	// skip opening brace character

	// loop thru command portion of string (text preceding the ':'),
	//  extracting formatting data.  This consists of pairs of single-char
	//  commands followed by numbers.
	while ( simpleText_[simpleTextIndex_] != ':' )
	{
		TEXT_FIELD_ASSERTION(simpleText_[simpleTextIndex_] != '\0');

		char command = simpleText_[simpleTextIndex_];

		simpleTextIndex_++;

		TEXT_FIELD_ASSERTION(simpleText_[simpleTextIndex_] != '\0');

		// Now process command:
		switch ( command )
		{
			case ' ':
			case ',':
				break;

			case 'p' :	// "Point size": sets text height.
			case 'c' :	// Sets text color... ignore
			case 'x' :	// Sets absolute x text position within parent Container.
			case 'y' :	// sets absolute y text position within parent Container.
			case 'X' :	// sets relative x text position within parent Container.
			case 'Y' :	// sets relative y text position within parent Container.
			case 'o':	// Sets temporary y offset - reverts to previous settng
				getValue_();
				break;

			case 'S':	// Subscript drop... make all following text lower case
				isTextSubscripted = TRUE;
				break;

			case 'A':  //alternate characters such as Polish.  
				// ignore
				break;

				// Note: the following text styles are not combinable.
			case 'N':	// Sets "Normal" (bold) text style.
			case 'T':	// Sets "Thin" (not bold) text style.
			case 'I':	// Sets "Italic" text style.
			case 'J':	// Sets "Japanese" text style.
				// ignore
				break;

			default :
				// do nothing
				break;
		}	// end switch
	}	// end while not ':'

	simpleTextIndex_++;	  // skip the ':'
	TEXT_FIELD_ASSERTION(simpleText_[simpleTextIndex_] != '\0');

	// Process the string itself, now that the controls are done
	while ( simpleText_[simpleTextIndex_] != '}' )
	{
		// Extract substring to be rendered, up to the next open or close
		// brace, or to the next open bracket:
		while ( (simpleText_[simpleTextIndex_] != '}') &&
				(simpleText_[simpleTextIndex_] != '{') 
			  )
		{
			// If a backslash is encountered, grab the following character
			// regardless of what it is.  This removes any syntactical meaning
			// from the escaped character, allowing characters such as braces
			// and square brackets to be inserted in the message.
			// Note that the user must use TWO backslashes before the
			// character to be inserted, because the C++ pre-processor
			// will remove one of them.

			if ( simpleText_[simpleTextIndex_] == '\134' )	  // is a backslash character
			{
				simpleTextIndex_++;	// skip over the backslash
				TEXT_FIELD_ASSERTION(simpleText_[simpleTextIndex_] != '\0');
			}

			// TODO - E600 - Need to replace with special characters from viking font
			char ch = simpleText_[simpleTextIndex_++];
			switch ( ch )
			{
				case '\001':	  // is a V-Dot character
					serialTextIndex_ += sprintf(serialText_+serialTextIndex_,"Q");
					break;
				case '\003':	  // is DOWN ARROW
					serialTextIndex_ += sprintf(serialText_+serialTextIndex_,"Low ");
					break;
				case '\004':	  // is UP ARROW
					serialTextIndex_ += sprintf(serialText_+serialTextIndex_,"High ");
					break;
				case '\012':	  // is DOWN ARROW BAR
					serialTextIndex_ += sprintf(serialText_+serialTextIndex_,"Low Limit ");
					break;
				case '\013':	  // is UP ARROW BAR
					serialTextIndex_ += sprintf(serialText_+serialTextIndex_,"High Limit ");
					break;
				default:
					{
						// copy the ASCII character to the serialText_ string
						if ( isTextSubscripted )
						{
							serialText_[serialTextIndex_++] = tolower(ch);
						}
						else
						{
							serialText_[serialTextIndex_++] = ch;
						}
					}
			}
		}

		serialText_[serialTextIndex_] = '\0';	// Terminate the subString

		// ===================================================
		// Now that the substring has been rendered, check what caused the
		// substring to end - it might have encountered the start of
		// a nested substring.  This is handled by recursive
		// calls.  Or, it might have hit the end of the string ('}').

		if ( simpleText_[simpleTextIndex_] == '{' )		  // Check for nested substring:
		{
			// Process the nested string as a substring, using recursive call:
			renderSerialText_(isTextSubscripted);

			TEXT_FIELD_ASSERTION(simpleText_[simpleTextIndex_] == '}');

			simpleTextIndex_++;
			TEXT_FIELD_ASSERTION(simpleText_[simpleTextIndex_] != '\0');
		}
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getValue_
//
//@ Interface-Description
// This function extracts a base 10 number at the current location 
// in the simpleText_ buffer at simpleTextIndex_.
//---------------------------------------------------------------------
//@ Implementation-Description
// Extracts characters from simpleText_ one at a time, converts them to decimal
// digits, and accumulates the resulting value into numValue.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Int VsetText::getValue_(void)
{
	// if an equal sign is present, skip over it:
	if ( simpleText_[simpleTextIndex_] == '=' )
	{
		simpleTextIndex_ ++;
		TEXT_FIELD_ASSERTION(simpleText_[simpleTextIndex_] != '\0');
	}

	// now extract the number:
	Boolean isNegative = FALSE;

	if ( simpleText_[simpleTextIndex_] == '-' )	  // check for minus sign
	{
		isNegative = TRUE;
		simpleTextIndex_ ++;
		TEXT_FIELD_ASSERTION(simpleText_[simpleTextIndex_] != '\0');
	}

	// There must be at least one digit present:
	TEXT_FIELD_ASSERTION(isdigit(simpleText_[simpleTextIndex_]));

	Int numValue = 0;
	Int prevValue = -1;

	while ( isdigit(simpleText_[simpleTextIndex_]) )
	{
		numValue = numValue * 10 + (simpleText_[simpleTextIndex_] - '0');

		// Check for wrap-around:
		TEXT_FIELD_ASSERTION(numValue >= prevValue);

		simpleTextIndex_++;
		TEXT_FIELD_ASSERTION(simpleText_[simpleTextIndex_] != '\0');

		prevValue = numValue;
	}

	if ( isNegative )
	{
		numValue = -numValue;
	}

	//TEXT_FIELD_ASSERTION(numValue >= minValue && numValue <= maxValue);

	return(numValue);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ("CLASS_PRE_CONDITION()", etc.). The "softFaultID" and
//	"lineNumber" are essential pieces of information.  The "pFileName"
//	and "pPredicate" strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
VsetText::SoftFault(const SoftFaultID  softFaultID,
					const Uint32       lineNumber,
					const char*        pFileName,
					const char*        pPredicate)  
{
	FaultHandler::SoftFault(softFaultID, GUI_FOUNDATIONS, 
							GUI_Serial_Interface::VSET_TEXT_CLASS,
							lineNumber,	pFileName, pPredicate);

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: dumpTextString_
//
//@ Interface-Description
// This method is called by the TEXT_FIELD_ASSERTION macros to print the 
// this VsetText's text string to the console.
//---------------------------------------------------------------------
//@ Implementation-Description
// Uses cout to print the private simpleText_ to the console. Terminates 
// the private simpleText_ with a NULL to assure NULL termination before
// calling cout to print the string.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void VsetText::dumpTextString_(void) const
{
	cout << "invalid VsetText: " << simpleText_ << endl;
}

