// generated by genDefs.awk
#define CHOOSE_SETUP_P CHOOSE_SETUP_P[GuiApp::GetLanguageIndex()]
#define VENT_SETUP_CONTINUE_S VENT_SETUP_CONTINUE_S[GuiApp::GetLanguageIndex()]
#define VENT_SETUP_PROCEED_CANCEL_S VENT_SETUP_PROCEED_CANCEL_S[GuiApp::GetLanguageIndex()]
#define VENT_SETUP_CANCEL_S VENT_SETUP_CANCEL_S[GuiApp::GetLanguageIndex()]
#define ADJUST_SETTINGS_P ADJUST_SETTINGS_P[GuiApp::GetLanguageIndex()]
#define ADJUST_COM_SETTINGS_A ADJUST_COM_SETTINGS_A[GuiApp::GetLanguageIndex()]
#define ADJUST_SETTINGS_AS_NEEDED_P ADJUST_SETTINGS_AS_NEEDED_P[GuiApp::GetLanguageIndex()]
#define ADJUST_IBW_P ADJUST_IBW_P[GuiApp::GetLanguageIndex()]
#define VIEW_SETTINGS_P VIEW_SETTINGS_P[GuiApp::GetLanguageIndex()]
#define USE_KNOB_TO_ADJUST_P USE_KNOB_TO_ADJUST_P[GuiApp::GetLanguageIndex()]
#define VENT_SETUP_ALARMS_AND_APNEA_RESTORED_A VENT_SETUP_ALARMS_AND_APNEA_RESTORED_A[GuiApp::GetLanguageIndex()]
#define PRESS_CONTINUE_IF_OK_P PRESS_CONTINUE_IF_OK_P[GuiApp::GetLanguageIndex()]
#define PRESS_ACCEPT_IF_OK_P PRESS_ACCEPT_IF_OK_P[GuiApp::GetLanguageIndex()]
#define ADJUST_SCREEN_CONTRAST_OFF_KEY_A ADJUST_SCREEN_CONTRAST_OFF_KEY_A[GuiApp::GetLanguageIndex()]
#define OTHER_SCREENS_CANCEL_S OTHER_SCREENS_CANCEL_S[GuiApp::GetLanguageIndex()]
#define OTHER_SCREENS_PROCEED_CANCEL_S OTHER_SCREENS_PROCEED_CANCEL_S[GuiApp::GetLanguageIndex()]
#define APNEA_SETUP_CANCEL_S APNEA_SETUP_CANCEL_S[GuiApp::GetLanguageIndex()]
#define APNEA_SETUP_PROCEED_CANCEL_S APNEA_SETUP_PROCEED_CANCEL_S[GuiApp::GetLanguageIndex()]
#define TOUCH_A_BUTTON_P TOUCH_A_BUTTON_P[GuiApp::GetLanguageIndex()]
#define SELECT_BEFORE_ATTACHING_PATIENT_A SELECT_BEFORE_ATTACHING_PATIENT_A[GuiApp::GetLanguageIndex()]
#define RETAINS_PREVIOUS_SETTINGS_A RETAINS_PREVIOUS_SETTINGS_A[GuiApp::GetLanguageIndex()]
#define RELEASE_TEST_BUTTON_P RELEASE_TEST_BUTTON_P[GuiApp::GetLanguageIndex()]
#define TEST_BUTTON_STUCK_A TEST_BUTTON_STUCK_A[GuiApp::GetLanguageIndex()]
#define SST_CANCEL_S SST_CANCEL_S[GuiApp::GetLanguageIndex()]
#define ENSURE_NO_PATIENT_ATTACHED_P ENSURE_NO_PATIENT_ATTACHED_P[GuiApp::GetLanguageIndex()]
#define SST_WILL_BEGIN_A SST_WILL_BEGIN_A[GuiApp::GetLanguageIndex()]
#define SST_CONFIRM_CANCEL_S SST_CONFIRM_CANCEL_S[GuiApp::GetLanguageIndex()]
#define NEW_PATIENT_CONTINUE_RESTART_S NEW_PATIENT_CONTINUE_RESTART_S[GuiApp::GetLanguageIndex()]
#define NEW_PATIENT_PROCEED_CANCEL_S NEW_PATIENT_PROCEED_CANCEL_S[GuiApp::GetLanguageIndex()]
#define HB_ALARM_VOL_MIN_A HB_ALARM_VOL_MIN_A[GuiApp::GetLanguageIndex()]
#define HB_ALARM_VOL_MAX_A HB_ALARM_VOL_MAX_A[GuiApp::GetLanguageIndex()]
#define HB_APNEA_FLOW_PATT_BASED_IE_MAX_A HB_APNEA_FLOW_PATT_BASED_IE_MAX_A[GuiApp::GetLanguageIndex()]
#define HB_APNEA_FLOW_PATT_BASED_TI_MAX_A HB_APNEA_FLOW_PATT_BASED_TI_MAX_A[GuiApp::GetLanguageIndex()]
#define HB_APNEA_FLOW_PATT_BASED_TI_MIN_A HB_APNEA_FLOW_PATT_BASED_TI_MIN_A[GuiApp::GetLanguageIndex()]
#define HB_APNEA_IE_RATIO_MAX_A HB_APNEA_IE_RATIO_MAX_A[GuiApp::GetLanguageIndex()]
#define HB_APNEA_INSP_PRESS_MIN_A HB_APNEA_INSP_PRESS_MIN_A[GuiApp::GetLanguageIndex()]
#define HB_APNEA_INSP_PRESS_MAX_BASED_PEEP_A HB_APNEA_INSP_PRESS_MAX_BASED_PEEP_A[GuiApp::GetLanguageIndex()]
#define HB_APNEA_INSP_PRESS_MAX_BASED_PEEPL_A HB_APNEA_INSP_PRESS_MAX_BASED_PEEPL_A[GuiApp::GetLanguageIndex()]
#define HB_APNEA_INSP_PRESS_MAX_BASED_PCIRC_PEEP_A HB_APNEA_INSP_PRESS_MAX_BASED_PCIRC_PEEP_A[GuiApp::GetLanguageIndex()]
#define HB_APNEA_INSP_PRESS_MAX_BASED_PCIRC_PEEPL_A HB_APNEA_INSP_PRESS_MAX_BASED_PCIRC_PEEPL_A[GuiApp::GetLanguageIndex()]
#define HB_APNEA_INSP_PRESS_MAX_A HB_APNEA_INSP_PRESS_MAX_A[GuiApp::GetLanguageIndex()]
#define HB_APNEA_INSP_TIME_MAX_A HB_APNEA_INSP_TIME_MAX_A[GuiApp::GetLanguageIndex()]
#define HB_APNEA_INSP_TIME_MIN_A HB_APNEA_INSP_TIME_MIN_A[GuiApp::GetLanguageIndex()]
#define HB_APNEA_INTERVAL_MIN_A HB_APNEA_INTERVAL_MIN_A[GuiApp::GetLanguageIndex()]
#define HB_APNEA_INTERVAL_MIN_BASED_APNEA_RR_A HB_APNEA_INTERVAL_MIN_BASED_APNEA_RR_A[GuiApp::GetLanguageIndex()]
#define HB_APNEA_INTERVAL_MAX_A HB_APNEA_INTERVAL_MAX_A[GuiApp::GetLanguageIndex()]
#define HB_APNEA_O2_MIN_A HB_APNEA_O2_MIN_A[GuiApp::GetLanguageIndex()]
#define HB_APNEA_O2_MIN_BASED_O2_A HB_APNEA_O2_MIN_BASED_O2_A[GuiApp::GetLanguageIndex()]
#define HB_APNEA_O2_MAX_A HB_APNEA_O2_MAX_A[GuiApp::GetLanguageIndex()]
#define HB_APNEA_PEAK_FLOW_MIN_BASED_CCT_TYPE_A HB_APNEA_PEAK_FLOW_MIN_BASED_CCT_TYPE_A[GuiApp::GetLanguageIndex()]
#define HB_APNEA_PEAK_FLOW_MAX_BASED_CCT_TYPE_A HB_APNEA_PEAK_FLOW_MAX_BASED_CCT_TYPE_A[GuiApp::GetLanguageIndex()]
#define HB_APNEA_RESP_RATE_MIN_A HB_APNEA_RESP_RATE_MIN_A[GuiApp::GetLanguageIndex()]
#define HB_APNEA_RESP_RATE_MIN_BASED_INTERVAL_A HB_APNEA_RESP_RATE_MIN_BASED_INTERVAL_A[GuiApp::GetLanguageIndex()]
#define HB_APNEA_RESP_RATE_MAX_A HB_APNEA_RESP_RATE_MAX_A[GuiApp::GetLanguageIndex()]
#define HB_APNEA_TIDAL_VOL_MIN_A HB_APNEA_TIDAL_VOL_MIN_A[GuiApp::GetLanguageIndex()]
#define HB_APNEA_TIDAL_VOL_MIN_BASED_IBW_A HB_APNEA_TIDAL_VOL_MIN_BASED_IBW_A[GuiApp::GetLanguageIndex()]
#define HB_APNEA_TIDAL_VOL_SOFT_MIN_BASED_IBW_A HB_APNEA_TIDAL_VOL_SOFT_MIN_BASED_IBW_A[GuiApp::GetLanguageIndex()]
#define HB_APNEA_TIDAL_VOL_SOFT_MAX_BASED_IBW_A HB_APNEA_TIDAL_VOL_SOFT_MAX_BASED_IBW_A[GuiApp::GetLanguageIndex()]
#define HB_APNEA_TIDAL_VOL_MAX_BASED_IBW_A HB_APNEA_TIDAL_VOL_MAX_BASED_IBW_A[GuiApp::GetLanguageIndex()]
#define HB_APNEA_TIDAL_VOL_MAX_A HB_APNEA_TIDAL_VOL_MAX_A[GuiApp::GetLanguageIndex()]
#define HB_ATM_PRESSURE_MIN_A HB_ATM_PRESSURE_MIN_A[GuiApp::GetLanguageIndex()]
#define HB_ATM_PRESSURE_MAX_A HB_ATM_PRESSURE_MAX_A[GuiApp::GetLanguageIndex()]
#define HB_DCI_DATA_BITS_BASED_PARITY_A HB_DCI_DATA_BITS_BASED_PARITY_A[GuiApp::GetLanguageIndex()]
#define HB_DCI_PARITY_MODE_BASED_BITS_A HB_DCI_PARITY_MODE_BASED_BITS_A[GuiApp::GetLanguageIndex()]
#define HB_DISPLAY_BRIGHTNESS_MIN_A HB_DISPLAY_BRIGHTNESS_MIN_A[GuiApp::GetLanguageIndex()]
#define HB_DISPLAY_BRIGHTNESS_MAX_A HB_DISPLAY_BRIGHTNESS_MAX_A[GuiApp::GetLanguageIndex()]
#define HB_DISPLAY_CONTRAST_SCALE_MIN_A HB_DISPLAY_CONTRAST_SCALE_MIN_A[GuiApp::GetLanguageIndex()]
#define HB_DISPLAY_CONTRAST_SCALE_MAX_A HB_DISPLAY_CONTRAST_SCALE_MAX_A[GuiApp::GetLanguageIndex()]
#define HB_DISPLAY_CONTRAST_DELTA_MIN_A HB_DISPLAY_CONTRAST_DELTA_MIN_A[GuiApp::GetLanguageIndex()]
#define HB_DISPLAY_CONTRAST_DELTA_MAX_A HB_DISPLAY_CONTRAST_DELTA_MAX_A[GuiApp::GetLanguageIndex()]
#define HB_DISCO_SENS_MIN_A HB_DISCO_SENS_MIN_A[GuiApp::GetLanguageIndex()]
#define HB_DISCO_SENS_VENT_BASED_MAX_A HB_DISCO_SENS_VENT_BASED_MAX_A[GuiApp::GetLanguageIndex()]
#define HB_DISCO_SENS_MAX_A HB_DISCO_SENS_MAX_A[GuiApp::GetLanguageIndex()]
#define HB_DISCO_SENS_NEONATAL_SOFT_MAX_A HB_DISCO_SENS_NEONATAL_SOFT_MAX_A[GuiApp::GetLanguageIndex()]
#define HB_EXP_SENS_MIN_A HB_EXP_SENS_MIN_A[GuiApp::GetLanguageIndex()]
#define HB_EXP_SENS_MAX_A HB_EXP_SENS_MAX_A[GuiApp::GetLanguageIndex()]
#define HB_EXP_TIME_MIN_A HB_EXP_TIME_MIN_A[GuiApp::GetLanguageIndex()]
#define HB_FLOW_ACCEL_PERCENT_MIN_A HB_FLOW_ACCEL_PERCENT_MIN_A[GuiApp::GetLanguageIndex()]
#define HB_FLOW_ACCEL_PERCENT_MAX_A HB_FLOW_ACCEL_PERCENT_MAX_A[GuiApp::GetLanguageIndex()]
#define HB_FLOW_PATTERN_BASED_IE_MAX_A HB_FLOW_PATTERN_BASED_IE_MAX_A[GuiApp::GetLanguageIndex()]
#define HB_FLOW_PATTERN_BASED_TI_MAX_A HB_FLOW_PATTERN_BASED_TI_MAX_A[GuiApp::GetLanguageIndex()]
#define HB_FLOW_PATTERN_BASED_TI_MIN_A HB_FLOW_PATTERN_BASED_TI_MIN_A[GuiApp::GetLanguageIndex()]
#define HB_FLOW_PATTERN_BASED_TE_MIN_A HB_FLOW_PATTERN_BASED_TE_MIN_A[GuiApp::GetLanguageIndex()]
#define HB_FLOW_PLOT_SCALE_MIN_A HB_FLOW_PLOT_SCALE_MIN_A[GuiApp::GetLanguageIndex()]
#define HB_FLOW_PLOT_SCALE_MAX_A HB_FLOW_PLOT_SCALE_MAX_A[GuiApp::GetLanguageIndex()]
#define HB_FLOW_SENS_MIN_A HB_FLOW_SENS_MIN_A[GuiApp::GetLanguageIndex()]
#define HB_FLOW_SENS_MAX_A HB_FLOW_SENS_MAX_A[GuiApp::GetLanguageIndex()]
#define HB_HIGH_CIRC_MIN_BASED_LOW_A HB_HIGH_CIRC_MIN_BASED_LOW_A[GuiApp::GetLanguageIndex()]
#define HB_HIGH_CIRC_MIN_BASED_PEEP_A HB_HIGH_CIRC_MIN_BASED_PEEP_A[GuiApp::GetLanguageIndex()]
#define HB_HIGH_CIRC_MIN_BASED_PEEPL_A HB_HIGH_CIRC_MIN_BASED_PEEPL_A[GuiApp::GetLanguageIndex()]
#define HB_HIGH_CIRC_MIN_BASED_PSUPP_PEEP_A HB_HIGH_CIRC_MIN_BASED_PSUPP_PEEP_A[GuiApp::GetLanguageIndex()]
#define HB_HIGH_CIRC_MIN_BASED_PSUPP_PEEPL_A HB_HIGH_CIRC_MIN_BASED_PSUPP_PEEPL_A[GuiApp::GetLanguageIndex()]
#define HB_HIGH_CIRC_MIN_BASED_INSP_PRESS_PEEP_A HB_HIGH_CIRC_MIN_BASED_INSP_PRESS_PEEP_A[GuiApp::GetLanguageIndex()]
#define HB_HIGH_CIRC_MIN_BASED_PEEPH_A HB_HIGH_CIRC_MIN_BASED_PEEPH_A[GuiApp::GetLanguageIndex()]
#define HB_HIGH_CIRC_MAX_A HB_HIGH_CIRC_MAX_A[GuiApp::GetLanguageIndex()]
#define HB_LOW_CIRC_MIN_A HB_LOW_CIRC_MIN_A[GuiApp::GetLanguageIndex()]
#define HB_LOW_CIRC_MIN_BASED_PEEP_A HB_LOW_CIRC_MIN_BASED_PEEP_A[GuiApp::GetLanguageIndex()]
#define HB_LOW_CIRC_MAX_BASED_HIGH_A HB_LOW_CIRC_MAX_BASED_HIGH_A[GuiApp::GetLanguageIndex()]
#define HB_LOW_CIRC_MAX_A HB_LOW_CIRC_MAX_A[GuiApp::GetLanguageIndex()]
#define HB_HIGH_EXH_MIN_VOL_MIN_A HB_HIGH_EXH_MIN_VOL_MIN_A[GuiApp::GetLanguageIndex()]
#define HB_HIGH_EXH_MIN_VOL_MIN_BASED_LOW_A HB_HIGH_EXH_MIN_VOL_MIN_BASED_LOW_A[GuiApp::GetLanguageIndex()]
#define HB_HIGH_EXH_MIN_VOL_MAX_A HB_HIGH_EXH_MIN_VOL_MAX_A[GuiApp::GetLanguageIndex()]
#define HB_HIGH_EXH_TIDAL_VOL_MIN_A HB_HIGH_EXH_TIDAL_VOL_MIN_A[GuiApp::GetLanguageIndex()]
#define HB_HIGH_EXH_TIDAL_VOL_MIN_BASED_LOW_MAND_A HB_HIGH_EXH_TIDAL_VOL_MIN_BASED_LOW_MAND_A[GuiApp::GetLanguageIndex()]
#define HB_HIGH_EXH_TIDAL_VOL_MIN_BASED_LOW_SPONT_A HB_HIGH_EXH_TIDAL_VOL_MIN_BASED_LOW_SPONT_A[GuiApp::GetLanguageIndex()]
#define HB_HIGH_EXH_TIDAL_VOL_MAX_A HB_HIGH_EXH_TIDAL_VOL_MAX_A[GuiApp::GetLanguageIndex()]
#define HB_HIGH_INSP_TIDAL_VOL_MIN_MAND_A HB_HIGH_INSP_TIDAL_VOL_MIN_MAND_A[GuiApp::GetLanguageIndex()]
#define HB_HIGH_INSP_TIDAL_VOL_MIN_BASED_IBW_MAND_A HB_HIGH_INSP_TIDAL_VOL_MIN_BASED_IBW_MAND_A[GuiApp::GetLanguageIndex()]
#define HB_HIGH_INSP_TIDAL_VOL_MAX_BASED_IBW_MAND_A HB_HIGH_INSP_TIDAL_VOL_MAX_BASED_IBW_MAND_A[GuiApp::GetLanguageIndex()]
#define HB_HIGH_INSP_TIDAL_VOL_MAX_MAND_A HB_HIGH_INSP_TIDAL_VOL_MAX_MAND_A[GuiApp::GetLanguageIndex()]
#define HB_HIGH_INSP_TIDAL_VOL_MIN_A HB_HIGH_INSP_TIDAL_VOL_MIN_A[GuiApp::GetLanguageIndex()]
#define HB_HIGH_INSP_TIDAL_VOL_MIN_BASED_IBW_A HB_HIGH_INSP_TIDAL_VOL_MIN_BASED_IBW_A[GuiApp::GetLanguageIndex()]
#define HB_HIGH_INSP_TIDAL_VOL_SOFT_MAX_BASED_IBW_A HB_HIGH_INSP_TIDAL_VOL_SOFT_MAX_BASED_IBW_A[GuiApp::GetLanguageIndex()]
#define HB_HIGH_INSP_TIDAL_VOL_MAX_BASED_IBW_A HB_HIGH_INSP_TIDAL_VOL_MAX_BASED_IBW_A[GuiApp::GetLanguageIndex()]
#define HB_HIGH_INSP_TIDAL_VOL_MAX_A HB_HIGH_INSP_TIDAL_VOL_MAX_A[GuiApp::GetLanguageIndex()]
#define HB_HIGH_INSP_TIDAL_VOL_MIN_SPONT_A HB_HIGH_INSP_TIDAL_VOL_MIN_SPONT_A[GuiApp::GetLanguageIndex()]
#define HB_HIGH_INSP_TIDAL_VOL_MIN_BASED_IBW_SPONT_A HB_HIGH_INSP_TIDAL_VOL_MIN_BASED_IBW_SPONT_A[GuiApp::GetLanguageIndex()]
#define HB_HIGH_INSP_TIDAL_VOL_MAX_BASED_IBW_SPONT_A HB_HIGH_INSP_TIDAL_VOL_MAX_BASED_IBW_SPONT_A[GuiApp::GetLanguageIndex()]
#define HB_HIGH_INSP_TIDAL_VOL_MAX_SPONT_A HB_HIGH_INSP_TIDAL_VOL_MAX_SPONT_A[GuiApp::GetLanguageIndex()]
#define HB_HIGH_INSP_TIDAL_VOL_MIN_BASED_TIDAL_VOL HB_HIGH_INSP_TIDAL_VOL_MIN_BASED_TIDAL_VOL[GuiApp::GetLanguageIndex()]
#define HB_HIGH_INSP_TIDAL_VOL_MIN_BASED_TIDAL_VOL_MAND HB_HIGH_INSP_TIDAL_VOL_MIN_BASED_TIDAL_VOL_MAND[GuiApp::GetLanguageIndex()]
#define HB_HIGH_INSP_TIDAL_VOL_MIN_BASED_VOL_SUP_SPONT HB_HIGH_INSP_TIDAL_VOL_MIN_BASED_VOL_SUP_SPONT[GuiApp::GetLanguageIndex()]
#define HB_HIGH_RESP_RATE_MIN_A HB_HIGH_RESP_RATE_MIN_A[GuiApp::GetLanguageIndex()]
#define HB_HIGH_RESP_RATE_MAX_A HB_HIGH_RESP_RATE_MAX_A[GuiApp::GetLanguageIndex()]
#define HB_HL_RATIO_SOFT_MAX_A HB_HL_RATIO_SOFT_MAX_A[GuiApp::GetLanguageIndex()]
#define HB_HL_RATIO_MAX_A HB_HL_RATIO_MAX_A[GuiApp::GetLanguageIndex()]
#define HB_HUMID_VOLUME_MIN_A HB_HUMID_VOLUME_MIN_A[GuiApp::GetLanguageIndex()]
#define HB_HUMID_VOLUME_MAX_A HB_HUMID_VOLUME_MAX_A[GuiApp::GetLanguageIndex()]
#define HB_IBW_MIN_BASED_CCT_TYPE_A HB_IBW_MIN_BASED_CCT_TYPE_A[GuiApp::GetLanguageIndex()]
#define HB_IBW_SOFT_MIN_BASED_CCT_TYPE_A HB_IBW_SOFT_MIN_BASED_CCT_TYPE_A[GuiApp::GetLanguageIndex()]
#define HB_IBW_SOFT_MAX_BASED_CCT_TYPE_A HB_IBW_SOFT_MAX_BASED_CCT_TYPE_A[GuiApp::GetLanguageIndex()]
#define HB_IBW_MAX_BASED_CCT_TYPE_A HB_IBW_MAX_BASED_CCT_TYPE_A[GuiApp::GetLanguageIndex()]
#define HB_IE_RATIO_SOFT_MAX_A HB_IE_RATIO_SOFT_MAX_A[GuiApp::GetLanguageIndex()]
#define HB_IE_RATIO_MAX_A HB_IE_RATIO_MAX_A[GuiApp::GetLanguageIndex()]
#define HB_INSP_PRESS_MIN_A HB_INSP_PRESS_MIN_A[GuiApp::GetLanguageIndex()]
#define HB_INSP_PRESS_MAX_BASED_PEEP_A HB_INSP_PRESS_MAX_BASED_PEEP_A[GuiApp::GetLanguageIndex()]
#define HB_INSP_PRESS_MAX_BASED_PCIRC_PEEP_A HB_INSP_PRESS_MAX_BASED_PCIRC_PEEP_A[GuiApp::GetLanguageIndex()]
#define HB_INSP_PRESS_MAX_A HB_INSP_PRESS_MAX_A[GuiApp::GetLanguageIndex()]
#define HB_INSP_TIME_MAX_A HB_INSP_TIME_MAX_A[GuiApp::GetLanguageIndex()]
#define HB_INSP_TIME_MIN_BASED_PLAT_TIME_A HB_INSP_TIME_MIN_BASED_PLAT_TIME_A[GuiApp::GetLanguageIndex()]
#define HB_INSP_TIME_MIN_A HB_INSP_TIME_MIN_A[GuiApp::GetLanguageIndex()]
#define HB_LOW_EXH_MAND_TIDAL_MIN_A HB_LOW_EXH_MAND_TIDAL_MIN_A[GuiApp::GetLanguageIndex()]
#define HB_LOW_EXH_MAND_TIDAL_MAX_BASED_HIGH_A HB_LOW_EXH_MAND_TIDAL_MAX_BASED_HIGH_A[GuiApp::GetLanguageIndex()]
#define HB_LOW_EXH_MAND_TIDAL_MAX_A HB_LOW_EXH_MAND_TIDAL_MAX_A[GuiApp::GetLanguageIndex()]
#define HB_LOW_EXH_MIN_VOL_MIN_A HB_LOW_EXH_MIN_VOL_MIN_A[GuiApp::GetLanguageIndex()]
#define HB_LOW_EXH_MIN_VOL_SOFT_MIN_A HB_LOW_EXH_MIN_VOL_SOFT_MIN_A[GuiApp::GetLanguageIndex()]
#define HB_LOW_EXH_MIN_VOL_MAX_BASED_HIGH_A HB_LOW_EXH_MIN_VOL_MAX_BASED_HIGH_A[GuiApp::GetLanguageIndex()]
#define HB_LOW_EXH_MIN_VOL_MAX_A HB_LOW_EXH_MIN_VOL_MAX_A[GuiApp::GetLanguageIndex()]
#define HB_LOW_EXH_SPONT_TIDAL_MIN_A HB_LOW_EXH_SPONT_TIDAL_MIN_A[GuiApp::GetLanguageIndex()]
#define HB_LOW_EXH_SPONT_TIDAL_MAX_BASED_HIGH_A HB_LOW_EXH_SPONT_TIDAL_MAX_BASED_HIGH_A[GuiApp::GetLanguageIndex()]
#define HB_LOW_EXH_SPONT_TIDAL_MAX_A HB_LOW_EXH_SPONT_TIDAL_MAX_A[GuiApp::GetLanguageIndex()]
#define HB_O2_PERCENT_MIN_A HB_O2_PERCENT_MIN_A[GuiApp::GetLanguageIndex()]
#define HB_O2_PERCENT_MAX_A HB_O2_PERCENT_MAX_A[GuiApp::GetLanguageIndex()]
#define HB_PEAK_INSP_FLOW_MIN_BASED_CCT_TYPE_A HB_PEAK_INSP_FLOW_MIN_BASED_CCT_TYPE_A[GuiApp::GetLanguageIndex()]
#define HB_PEAK_INSP_FLOW_MAX_BASED_CCT_TYPE_A HB_PEAK_INSP_FLOW_MAX_BASED_CCT_TYPE_A[GuiApp::GetLanguageIndex()]
#define HB_PEAK_INSP_FLOW_MIN_BASED_VCP_A HB_PEAK_INSP_FLOW_MIN_BASED_VCP_A[GuiApp::GetLanguageIndex()]
#define HB_PEAK_INSP_FLOW_MAX_BASED_VCP_A HB_PEAK_INSP_FLOW_MAX_BASED_VCP_A[GuiApp::GetLanguageIndex()]
#define HB_PERCENT_SUPPORT_MIN_A HB_PERCENT_SUPPORT_MIN_A[GuiApp::GetLanguageIndex()]
#define HB_PERCENT_SUPPORT_SOFT_MAX_A HB_PERCENT_SUPPORT_SOFT_MAX_A[GuiApp::GetLanguageIndex()]
#define HB_PERCENT_SUPPORT_MAX_A HB_PERCENT_SUPPORT_MAX_A[GuiApp::GetLanguageIndex()]
#define HB_PLATEAU_MIN_A HB_PLATEAU_MIN_A[GuiApp::GetLanguageIndex()]
#define HB_PLATEAU_MAX_A HB_PLATEAU_MAX_A[GuiApp::GetLanguageIndex()]
#define HB_PEEP_MIN_A HB_PEEP_MIN_A[GuiApp::GetLanguageIndex()]
#define HB_PEEP_SOFT_DELTA_LIMIT_A HB_PEEP_SOFT_DELTA_LIMIT_A[GuiApp::GetLanguageIndex()]
#define HB_PEEP_MAX_BASED_PCIRC_A HB_PEEP_MAX_BASED_PCIRC_A[GuiApp::GetLanguageIndex()]
#define HB_PEEP_MAX_BASED_PI_A HB_PEEP_MAX_BASED_PI_A[GuiApp::GetLanguageIndex()]
#define HB_PEEP_MAX_BASED_PCIRC_PI_A HB_PEEP_MAX_BASED_PCIRC_PI_A[GuiApp::GetLanguageIndex()]
#define HB_PEEP_MAX_BASED_PSUPP_A HB_PEEP_MAX_BASED_PSUPP_A[GuiApp::GetLanguageIndex()]
#define HB_PEEP_MAX_BASED_PCIRC_PSUPP_A HB_PEEP_MAX_BASED_PCIRC_PSUPP_A[GuiApp::GetLanguageIndex()]
#define HB_PEEP_MAX_A HB_PEEP_MAX_A[GuiApp::GetLanguageIndex()]
#define HB_PEEP_HIGH_MIN_A HB_PEEP_HIGH_MIN_A[GuiApp::GetLanguageIndex()]
#define HB_PEEP_HIGH_MIN_BASED_PEEPL_A HB_PEEP_HIGH_MIN_BASED_PEEPL_A[GuiApp::GetLanguageIndex()]
#define HB_PEEP_HIGH_MAX_BASED_PCIRC_A HB_PEEP_HIGH_MAX_BASED_PCIRC_A[GuiApp::GetLanguageIndex()]
#define HB_PEEP_HIGH_MAX_A HB_PEEP_HIGH_MAX_A[GuiApp::GetLanguageIndex()]
#define HB_PEEP_HIGH_TIME_MAX_A HB_PEEP_HIGH_TIME_MAX_A[GuiApp::GetLanguageIndex()]
#define HB_PEEP_HIGH_TIME_MIN_A HB_PEEP_HIGH_TIME_MIN_A[GuiApp::GetLanguageIndex()]
#define HB_PEEP_LOW_MIN_A HB_PEEP_LOW_MIN_A[GuiApp::GetLanguageIndex()]
#define HB_PEEP_LOW_MAX_BASED_PCIRC_A HB_PEEP_LOW_MAX_BASED_PCIRC_A[GuiApp::GetLanguageIndex()]
#define HB_PEEP_LOW_MAX_BASED_PCIRC_PSUPP_A HB_PEEP_LOW_MAX_BASED_PCIRC_PSUPP_A[GuiApp::GetLanguageIndex()]
#define HB_PEEP_LOW_MAX_BASED_PSUPP_A HB_PEEP_LOW_MAX_BASED_PSUPP_A[GuiApp::GetLanguageIndex()]
#define HB_PEEP_LOW_MAX_BASED_PEEPH_A HB_PEEP_LOW_MAX_BASED_PEEPH_A[GuiApp::GetLanguageIndex()]
#define HB_PEEP_LOW_MAX_A HB_PEEP_LOW_MAX_A[GuiApp::GetLanguageIndex()]
#define HB_PEEP_LOW_TIME_MIN_A HB_PEEP_LOW_TIME_MIN_A[GuiApp::GetLanguageIndex()]
#define HB_PRESS_PLOT_SCALE_MIN_A HB_PRESS_PLOT_SCALE_MIN_A[GuiApp::GetLanguageIndex()]
#define HB_PRESS_PLOT_SCALE_MAX_A HB_PRESS_PLOT_SCALE_MAX_A[GuiApp::GetLanguageIndex()]
#define HB_PRESS_SENS_MIN_A HB_PRESS_SENS_MIN_A[GuiApp::GetLanguageIndex()]
#define HB_PRESS_SENS_MAX_A HB_PRESS_SENS_MAX_A[GuiApp::GetLanguageIndex()]
#define HB_PRESS_SUPP_MIN_A HB_PRESS_SUPP_MIN_A[GuiApp::GetLanguageIndex()]
#define HB_PRESS_SUPP_MAX_BASED_PEEP_A HB_PRESS_SUPP_MAX_BASED_PEEP_A[GuiApp::GetLanguageIndex()]
#define HB_PRESS_SUPP_MAX_BASED_PEEPL_A HB_PRESS_SUPP_MAX_BASED_PEEPL_A[GuiApp::GetLanguageIndex()]
#define HB_PRESS_SUPP_MAX_BASED_PCIRC_PEEP_A HB_PRESS_SUPP_MAX_BASED_PCIRC_PEEP_A[GuiApp::GetLanguageIndex()]
#define HB_PRESS_SUPP_MAX_BASED_PCIRC_PEEPL_A HB_PRESS_SUPP_MAX_BASED_PCIRC_PEEPL_A[GuiApp::GetLanguageIndex()]
#define HB_PRESS_SUPP_MAX_A HB_PRESS_SUPP_MAX_A[GuiApp::GetLanguageIndex()]
#define HB_PRESS_VOL_LOOP_BASE_MIN_A HB_PRESS_VOL_LOOP_BASE_MIN_A[GuiApp::GetLanguageIndex()]
#define HB_PRESS_VOL_LOOP_BASE_MAX_A HB_PRESS_VOL_LOOP_BASE_MAX_A[GuiApp::GetLanguageIndex()]
#define HB_RESP_RATE_MIN_A HB_RESP_RATE_MIN_A[GuiApp::GetLanguageIndex()]
#define HB_RESP_RATE_MAX_A HB_RESP_RATE_MAX_A[GuiApp::GetLanguageIndex()]
#define HB_TIDAL_VOL_MIN_A HB_TIDAL_VOL_MIN_A[GuiApp::GetLanguageIndex()]
#define HB_TIDAL_VOL_MIN_BASED_IBW_A HB_TIDAL_VOL_MIN_BASED_IBW_A[GuiApp::GetLanguageIndex()]
#define HB_TIDAL_VOL_SOFT_MIN_BASED_IBW_A HB_TIDAL_VOL_SOFT_MIN_BASED_IBW_A[GuiApp::GetLanguageIndex()]
#define HB_TIDAL_VOL_SOFT_MAX_BASED_IBW_A HB_TIDAL_VOL_SOFT_MAX_BASED_IBW_A[GuiApp::GetLanguageIndex()]
#define HB_TIDAL_VOL_MAX_BASED_IBW_A HB_TIDAL_VOL_MAX_BASED_IBW_A[GuiApp::GetLanguageIndex()]
#define HB_TIDAL_VOL_MAX_A HB_TIDAL_VOL_MAX_A[GuiApp::GetLanguageIndex()]
#define HB_TIDAL_VOL_MAX_BASED_HIGH_INSP_TIDAL_VOL HB_TIDAL_VOL_MAX_BASED_HIGH_INSP_TIDAL_VOL[GuiApp::GetLanguageIndex()]
#define HB_TIDAL_VOL_MAX_BASED_HIGH_INSP_TIDAL_VOL_MAND HB_TIDAL_VOL_MAX_BASED_HIGH_INSP_TIDAL_VOL_MAND[GuiApp::GetLanguageIndex()]
#define HB_VOL_SUP_MAX_BASED_HIGH_INSP_TIDAL_VOL HB_VOL_SUP_MAX_BASED_HIGH_INSP_TIDAL_VOL[GuiApp::GetLanguageIndex()]
#define HB_TIME_PLOT_SCALE_MIN_A HB_TIME_PLOT_SCALE_MIN_A[GuiApp::GetLanguageIndex()]
#define HB_TIME_PLOT_SCALE_MAX_A HB_TIME_PLOT_SCALE_MAX_A[GuiApp::GetLanguageIndex()]
#define HB_TUBE_ID_MIN_A HB_TUBE_ID_MIN_A[GuiApp::GetLanguageIndex()]
#define HB_TUBE_ID_SOFT_MIN_A HB_TUBE_ID_SOFT_MIN_A[GuiApp::GetLanguageIndex()]
#define HB_TUBE_ID_SOFT_MAX_A HB_TUBE_ID_SOFT_MAX_A[GuiApp::GetLanguageIndex()]
#define HB_TUBE_ID_MAX_A HB_TUBE_ID_MAX_A[GuiApp::GetLanguageIndex()]
#define HB_VOLUME_PLOT_SCALE_MIN_A HB_VOLUME_PLOT_SCALE_MIN_A[GuiApp::GetLanguageIndex()]
#define HB_VOLUME_PLOT_SCALE_MAX_A HB_VOLUME_PLOT_SCALE_MAX_A[GuiApp::GetLanguageIndex()]
#define HB_VOLUME_SUPP_MIN_A HB_VOLUME_SUPP_MIN_A[GuiApp::GetLanguageIndex()]
#define HB_VOLUME_SUPP_MIN_BASED_IBW_A HB_VOLUME_SUPP_MIN_BASED_IBW_A[GuiApp::GetLanguageIndex()]
#define HB_VOLUME_SUPP_SOFT_MIN_BASED_IBW_A HB_VOLUME_SUPP_SOFT_MIN_BASED_IBW_A[GuiApp::GetLanguageIndex()]
#define HB_VOLUME_SUPP_SOFT_MAX_BASED_IBW_A HB_VOLUME_SUPP_SOFT_MAX_BASED_IBW_A[GuiApp::GetLanguageIndex()]
#define HB_VOLUME_SUPP_MAX_BASED_IBW_A HB_VOLUME_SUPP_MAX_BASED_IBW_A[GuiApp::GetLanguageIndex()]
#define HB_VOLUME_SUPP_MAX_A HB_VOLUME_SUPP_MAX_A[GuiApp::GetLanguageIndex()]
#define HB_YEAR_MAX_A HB_YEAR_MAX_A[GuiApp::GetLanguageIndex()]
#define HB_YEAR_MIN_A HB_YEAR_MIN_A[GuiApp::GetLanguageIndex()]
#define HB_HIGH_SPONT_INSP_TIME_MIN_A HB_HIGH_SPONT_INSP_TIME_MIN_A[GuiApp::GetLanguageIndex()]
#define HB_HIGH_SPONT_INSP_TIME_MAX_BASED_IBW_A HB_HIGH_SPONT_INSP_TIME_MAX_BASED_IBW_A[GuiApp::GetLanguageIndex()]
#define ALARM_SETUP_CANCEL_S ALARM_SETUP_CANCEL_S[GuiApp::GetLanguageIndex()]
#define ALARM_SETUP_PROCEED_CANCEL_S ALARM_SETUP_PROCEED_CANCEL_S[GuiApp::GetLanguageIndex()]
#define TOUCH_A_BUTTON_D TOUCH_A_BUTTON_D[GuiApp::GetLanguageIndex()]
#define EMPTY_A EMPTY_A[GuiApp::GetLanguageIndex()]
#define SETTINGS_AUTO_SET_A SETTINGS_AUTO_SET_A[GuiApp::GetLanguageIndex()]
#define NP_APNEA_SETTINGS_AUTO_SET_A NP_APNEA_SETTINGS_AUTO_SET_A[GuiApp::GetLanguageIndex()]
#define SP_APNEA_SETTINGS_REVIEW_A SP_APNEA_SETTINGS_REVIEW_A[GuiApp::GetLanguageIndex()]
#define SETTINGS_LOCKED_OUT_A SETTINGS_LOCKED_OUT_A[GuiApp::GetLanguageIndex()]
#define MAIN_SETTING_CANCEL_S MAIN_SETTING_CANCEL_S[GuiApp::GetLanguageIndex()]
#define MAIN_SETTING_CANCEL_BUTTON_S MAIN_SETTING_CANCEL_BUTTON_S[GuiApp::GetLanguageIndex()]
#define APNEA_PI_CORRECTED_A APNEA_PI_CORRECTED_A[GuiApp::GetLanguageIndex()]
#define APNEA_O2_CORRECTED_A APNEA_O2_CORRECTED_A[GuiApp::GetLanguageIndex()]
#define APNEA_PI_O2_CORRECTED_A APNEA_PI_O2_CORRECTED_A[GuiApp::GetLanguageIndex()]
#define APNEA_SETUP_CONTINUE_CANCEL_S APNEA_SETUP_CONTINUE_CANCEL_S[GuiApp::GetLanguageIndex()]
#define SCREEN_BRIGHTNESS_CANCEL_S SCREEN_BRIGHTNESS_CANCEL_S[GuiApp::GetLanguageIndex()]
#define SCREEN_CONTRAST_CANCEL_S SCREEN_CONTRAST_CANCEL_S[GuiApp::GetLanguageIndex()]
#define SCREENLOCK_CONTROL_LOCKED SCREENLOCK_CONTROL_LOCKED[GuiApp::GetLanguageIndex()]
#define SCREENLOCK_CONTROL_LOCKED_P SCREENLOCK_CONTROL_LOCKED_P[GuiApp::GetLanguageIndex()]
#define SCREENLOCK_UNLOCK_S SCREENLOCK_UNLOCK_S[GuiApp::GetLanguageIndex()]
#define ALARM_VOLUME_CANCEL_S ALARM_VOLUME_CANCEL_S[GuiApp::GetLanguageIndex()]
#define SCREEN_CONTRAST_NOT_AVAILABLE_A SCREEN_CONTRAST_NOT_AVAILABLE_A[GuiApp::GetLanguageIndex()]
#define SCREEN_BRIGHTNESS_NOT_AVAILABLE_A SCREEN_BRIGHTNESS_NOT_AVAILABLE_A[GuiApp::GetLanguageIndex()]
#define SST_PRESS_ACCEPT_TO_CONFIRM_P SST_PRESS_ACCEPT_TO_CONFIRM_P[GuiApp::GetLanguageIndex()]
#define SST_AUTO_TEST_WILL_BEGIN_A SST_AUTO_TEST_WILL_BEGIN_A[GuiApp::GetLanguageIndex()]
#define SST_PLEASE_WAIT_P SST_PLEASE_WAIT_P[GuiApp::GetLanguageIndex()]
#define SST_USER_REQUESTING_EXIT_A SST_USER_REQUESTING_EXIT_A[GuiApp::GetLanguageIndex()]
#define SST_MATCH_ATTACHED_EQIP_A SST_MATCH_ATTACHED_EQIP_A[GuiApp::GetLanguageIndex()]
#define SST_PROCEED_EXIT_S SST_PROCEED_EXIT_S[GuiApp::GetLanguageIndex()]
#define SST_TEST_CANCEL_S SST_TEST_CANCEL_S[GuiApp::GetLanguageIndex()]
#define SST_DONE_P SST_DONE_P[GuiApp::GetLanguageIndex()]
#define SST_RESTART_OVERRIDE_S SST_RESTART_OVERRIDE_S[GuiApp::GetLanguageIndex()]
#define SST_EXIT_RESTART_S SST_EXIT_RESTART_S[GuiApp::GetLanguageIndex()]
#define SST_PROMPT_ACCEPT_CANCEL_S SST_PROMPT_ACCEPT_CANCEL_S[GuiApp::GetLanguageIndex()]
#define EST_TOUCH_BUTTONS_TO_SCROLL_S EST_TOUCH_BUTTONS_TO_SCROLL_S[GuiApp::GetLanguageIndex()]
#define EST_TOUCH_OVERRIDE_EXIT_S EST_TOUCH_OVERRIDE_EXIT_S[GuiApp::GetLanguageIndex()]
#define EST_SERVICE_REQUIRED_A EST_SERVICE_REQUIRED_A[GuiApp::GetLanguageIndex()]
#define EST_WAIT_TEST_TO_COMPLETE_P EST_WAIT_TEST_TO_COMPLETE_P[GuiApp::GetLanguageIndex()]
#define EST_TESTING_P EST_TESTING_P[GuiApp::GetLanguageIndex()]
#define EST_CANCEL_S EST_CANCEL_S[GuiApp::GetLanguageIndex()]
#define EST_PROMPT_ACCEPT_CANCEL_S EST_PROMPT_ACCEPT_CANCEL_S[GuiApp::GetLanguageIndex()]
#define EST_PROMPT_ACCEPT_CLEAR_CANCEL_S EST_PROMPT_ACCEPT_CLEAR_CANCEL_S[GuiApp::GetLanguageIndex()]
#define EST_PROMPT_ACCEPT_PASS_AND_CANCEL_FAIL_S EST_PROMPT_ACCEPT_PASS_AND_CANCEL_FAIL_S[GuiApp::GetLanguageIndex()]
#define SST_PROMPT_ACCEPT_YES_AND_CLEAR_NO_S SST_PROMPT_ACCEPT_YES_AND_CLEAR_NO_S[GuiApp::GetLanguageIndex()]
#define EST_USER_REQUESTING_EXIT_A EST_USER_REQUESTING_EXIT_A[GuiApp::GetLanguageIndex()]
#define EST_START_CANCEL_S EST_START_CANCEL_S[GuiApp::GetLanguageIndex()]
#define SM_REMOVE_INSP_FILTER_P SM_REMOVE_INSP_FILTER_P[GuiApp::GetLanguageIndex()]
#define SM_ATTACH_CIRCUIT_P SM_ATTACH_CIRCUIT_P[GuiApp::GetLanguageIndex()]
#define SM_UNBLOCK_WYE_P SM_UNBLOCK_WYE_P[GuiApp::GetLanguageIndex()]
#define SM_BLOCK_WYE_P SM_BLOCK_WYE_P[GuiApp::GetLanguageIndex()]
#define SM_IS_HUMIDIFIER_WET_P SM_IS_HUMIDIFIER_WET_P[GuiApp::GetLanguageIndex()]
#define SM_UNBLOCK_INLET_PORT_P SM_UNBLOCK_INLET_PORT_P[GuiApp::GetLanguageIndex()]
#define SM_BLOCK_INLET_PORT_P SM_BLOCK_INLET_PORT_P[GuiApp::GetLanguageIndex()]
#define SM_DISCONNECT_AIR_P SM_DISCONNECT_AIR_P[GuiApp::GetLanguageIndex()]
#define SM_DISCONNECT_O2_P SM_DISCONNECT_O2_P[GuiApp::GetLanguageIndex()]
#define SM_CONNECT_AIR_P SM_CONNECT_AIR_P[GuiApp::GetLanguageIndex()]
#define SM_CONNECT_AIR_IF_AVAIL_P SM_CONNECT_AIR_IF_AVAIL_P[GuiApp::GetLanguageIndex()]
#define SM_CONNECT_O2_P SM_CONNECT_O2_P[GuiApp::GetLanguageIndex()]
#define SM_DISCNT_TUBE_BFR_EXH_FLTR_P SM_DISCNT_TUBE_BFR_EXH_FLTR_P[GuiApp::GetLanguageIndex()]
#define SM_CONCT_TUBE_BFR_EXH_FLTR_P SM_CONCT_TUBE_BFR_EXH_FLTR_P[GuiApp::GetLanguageIndex()]
#define SM_ATTACH_GOLD_STD_TEST_TUBING_P SM_ATTACH_GOLD_STD_TEST_TUBING_P[GuiApp::GetLanguageIndex()]
#define SM_CONNECT_WALL_AIR_P SM_CONNECT_WALL_AIR_P[GuiApp::GetLanguageIndex()]
#define SM_KEY_ACCEPT_P SM_KEY_ACCEPT_P[GuiApp::GetLanguageIndex()]
#define SM_KEY_CLEAR_P SM_KEY_CLEAR_P[GuiApp::GetLanguageIndex()]
#define SM_KEY_INSP_PAUSE_P SM_KEY_INSP_PAUSE_P[GuiApp::GetLanguageIndex()]
#define SM_KEY_EXP_PAUSE_P SM_KEY_EXP_PAUSE_P[GuiApp::GetLanguageIndex()]
#define SM_KEY_MAN_INSP_P SM_KEY_MAN_INSP_P[GuiApp::GetLanguageIndex()]
#define SM_KEY_HUNDRED_PERCENT_O2_P SM_KEY_HUNDRED_PERCENT_O2_P[GuiApp::GetLanguageIndex()]
#define SM_KEY_INFO_P SM_KEY_INFO_P[GuiApp::GetLanguageIndex()]
#define SM_KEY_ALARM_RESET_P SM_KEY_ALARM_RESET_P[GuiApp::GetLanguageIndex()]
#define SM_KEY_ALARM_SILENCE_P SM_KEY_ALARM_SILENCE_P[GuiApp::GetLanguageIndex()]
#define SM_KEY_ALARM_VOLUME_P SM_KEY_ALARM_VOLUME_P[GuiApp::GetLanguageIndex()]
#define SM_KEY_SCREEN_BRIGHTNESS_P SM_KEY_SCREEN_BRIGHTNESS_P[GuiApp::GetLanguageIndex()]
#define SM_KEY_SCREEN_CONTRAST_P SM_KEY_SCREEN_CONTRAST_P[GuiApp::GetLanguageIndex()]
#define SM_KEY_SCREEN_LOCK_P SM_KEY_SCREEN_LOCK_P[GuiApp::GetLanguageIndex()]
#define SM_KEY_LEFT_SPARE_P SM_KEY_LEFT_SPARE_P[GuiApp::GetLanguageIndex()]
#define SM_TURN_KNOB_LEFT_P SM_TURN_KNOB_LEFT_P[GuiApp::GetLanguageIndex()]
#define SM_TURN_KNOB_RIGHT_P SM_TURN_KNOB_RIGHT_P[GuiApp::GetLanguageIndex()]
#define SM_NORMAL_P SM_NORMAL_P[GuiApp::GetLanguageIndex()]
#define SM_VENTILATOR_INOPERATIVE_P SM_VENTILATOR_INOPERATIVE_P[GuiApp::GetLanguageIndex()]
#define SM_SAFETY_VALVE_OPEN_P SM_SAFETY_VALVE_OPEN_P[GuiApp::GetLanguageIndex()]
#define SM_LOSS_OF_GUI_P SM_LOSS_OF_GUI_P[GuiApp::GetLanguageIndex()]
#define SM_BATTERY_BACKUP_READY_P SM_BATTERY_BACKUP_READY_P[GuiApp::GetLanguageIndex()]
#define SM_COMPRESSOR_READY_P SM_COMPRESSOR_READY_P[GuiApp::GetLanguageIndex()]
#define SM_COMPRESSOR_OPERATING_P SM_COMPRESSOR_OPERATING_P[GuiApp::GetLanguageIndex()]
#define SM_COMPRESSOR_MOTOR_ON_P SM_COMPRESSOR_MOTOR_ON_P[GuiApp::GetLanguageIndex()]
#define SM_COMPRESSOR_MOTOR_OFF_P SM_COMPRESSOR_MOTOR_OFF_P[GuiApp::GetLanguageIndex()]
#define SM_HUNDRED_PERCENT_O2_P SM_HUNDRED_PERCENT_O2_P[GuiApp::GetLanguageIndex()]
#define SM_ALARM_SILENCE_P SM_ALARM_SILENCE_P[GuiApp::GetLanguageIndex()]
#define SM_SCREEN_LOCK_P SM_SCREEN_LOCK_P[GuiApp::GetLanguageIndex()]
#define SM_HIGH_ALARM_P SM_HIGH_ALARM_P[GuiApp::GetLanguageIndex()]
#define SM_MEDIUM_ALARM_P SM_MEDIUM_ALARM_P[GuiApp::GetLanguageIndex()]
#define SM_LOW_ALARM_P SM_LOW_ALARM_P[GuiApp::GetLanguageIndex()]
#define SM_ON_BATT_PWR_P SM_ON_BATT_PWR_P[GuiApp::GetLanguageIndex()]
#define SM_SOUND_P SM_SOUND_P[GuiApp::GetLanguageIndex()]
#define SM_POWER_DOWN_P SM_POWER_DOWN_P[GuiApp::GetLanguageIndex()]
#define SM_GUI_SIDE_TEST_P SM_GUI_SIDE_TEST_P[GuiApp::GetLanguageIndex()]
#define SM_VENT_INOP_A_TEST_P SM_VENT_INOP_A_TEST_P[GuiApp::GetLanguageIndex()]
#define SM_VENT_INOP_B_TEST_P SM_VENT_INOP_B_TEST_P[GuiApp::GetLanguageIndex()]
#define SM_TEN_SEC_A_TEST_P SM_TEN_SEC_A_TEST_P[GuiApp::GetLanguageIndex()]
#define SM_TEN_SEC_B_TEST_P SM_TEN_SEC_B_TEST_P[GuiApp::GetLanguageIndex()]
#define SM_NURSE_CALL_TEST_P SM_NURSE_CALL_TEST_P[GuiApp::GetLanguageIndex()]
#define SM_NURSE_CALL_ON_P SM_NURSE_CALL_ON_P[GuiApp::GetLanguageIndex()]
#define SM_NURSE_CALL_OFF_P SM_NURSE_CALL_OFF_P[GuiApp::GetLanguageIndex()]
#define SM_EV_CAL_PRESSURE_SENSOR_ALERT_P SM_EV_CAL_PRESSURE_SENSOR_ALERT_P[GuiApp::GetLanguageIndex()]
#define SM_CONNECT_AC_P SM_CONNECT_AC_P[GuiApp::GetLanguageIndex()]
#define SM_CONNECT_AIR_AND_O2_PROMPT_P SM_CONNECT_AIR_AND_O2_PROMPT_P[GuiApp::GetLanguageIndex()]
#define SM_DISCONNECT_AIR_AND_O2_P SM_DISCONNECT_AIR_AND_O2_P[GuiApp::GetLanguageIndex()]
#define SM_CONNECT_HUMIDIFIER_P SM_CONNECT_HUMIDIFIER_P[GuiApp::GetLanguageIndex()]
#define SM_UNPLUG_AC_P SM_UNPLUG_AC_P[GuiApp::GetLanguageIndex()]
#define EXH_V_CAL_PROMPT_ACCEPT_S EXH_V_CAL_PROMPT_ACCEPT_S[GuiApp::GetLanguageIndex()]
#define EXH_V_CAL_PROMPT_ACCEPT_CANCEL_S EXH_V_CAL_PROMPT_ACCEPT_CANCEL_S[GuiApp::GetLanguageIndex()]
#define EXH_V_CAL_TOUCH_START_TEST_TO_BEGIN_P EXH_V_CAL_TOUCH_START_TEST_TO_BEGIN_P[GuiApp::GetLanguageIndex()]
#define EXH_V_CAL_TESTING_P EXH_V_CAL_TESTING_P[GuiApp::GetLanguageIndex()]
#define EXH_V_CAL_PLEASE_WAIT_P EXH_V_CAL_PLEASE_WAIT_P[GuiApp::GetLanguageIndex()]
#define EXH_V_CAL_SEE_OP_MANUAL_A EXH_V_CAL_SEE_OP_MANUAL_A[GuiApp::GetLanguageIndex()]
#define CONNECT_CABLE_TO_REMOTE_COMPUTER_P CONNECT_CABLE_TO_REMOTE_COMPUTER_P[GuiApp::GetLanguageIndex()]
#define FS_POWER_OFF_TO_RESTART_P FS_POWER_OFF_TO_RESTART_P[GuiApp::GetLanguageIndex()]
#define INSERT_DK_TO_BE_COPIED_FROM_P INSERT_DK_TO_BE_COPIED_FROM_P[GuiApp::GetLanguageIndex()]
#define INSERT_DK_TO_BE_COPIED_TO_P INSERT_DK_TO_BE_COPIED_TO_P[GuiApp::GetLanguageIndex()]
#define OP_HOUR_PROMPT_ACCEPT_CANCEL_S OP_HOUR_PROMPT_ACCEPT_CANCEL_S[GuiApp::GetLanguageIndex()]
#define CONNECT_CABLE_P CONNECT_CABLE_P[GuiApp::GetLanguageIndex()]
#define SM_PROX_SENSOR_P SM_PROX_SENSOR_P[GuiApp::GetLanguageIndex()]
#define EXIT_SERVICE_CANCEL_S EXIT_SERVICE_CANCEL_S[GuiApp::GetLanguageIndex()]
#define SN_SETTING_SERIAL_NUMBER_MSG_A SN_SETTING_SERIAL_NUMBER_MSG_A[GuiApp::GetLanguageIndex()]
#define DEVELOP_OPTIONS_SCREENS_CANCEL_S DEVELOP_OPTIONS_SCREENS_CANCEL_S[GuiApp::GetLanguageIndex()]
#define VENT_INOP_SEE_OP_MANUAL_S VENT_INOP_SEE_OP_MANUAL_S[GuiApp::GetLanguageIndex()]
#define INIT_PROMPT_ACCEPT_CLEAR_CANCEL_S INIT_PROMPT_ACCEPT_CLEAR_CANCEL_S[GuiApp::GetLanguageIndex()]
#define DATE_TIME_CANCEL_S DATE_TIME_CANCEL_S[GuiApp::GetLanguageIndex()]
#define DATE_TIME_PROCEED_CANCEL_S DATE_TIME_PROCEED_CANCEL_S[GuiApp::GetLanguageIndex()]
#define USE_KNOB_TO_SCROLL_P USE_KNOB_TO_SCROLL_P[GuiApp::GetLanguageIndex()]
#define TOUCH_TO_CONTINUE_CHANGE_P TOUCH_TO_CONTINUE_CHANGE_P[GuiApp::GetLanguageIndex()]
#define RM_NIF_START_ADVISORY_S RM_NIF_START_ADVISORY_S[GuiApp::GetLanguageIndex()]
#define RM_COMPLETE_ADVISORY_S RM_COMPLETE_ADVISORY_S[GuiApp::GetLanguageIndex()]
#define RM_P100_START_ADVISORY_P RM_P100_START_ADVISORY_P[GuiApp::GetLanguageIndex()]
#define RM_VC_START_ADVISORY_P RM_VC_START_ADVISORY_P[GuiApp::GetLanguageIndex()]
#define RM_LOWER_OTHER_SCREENS_P RM_LOWER_OTHER_SCREENS_P[GuiApp::GetLanguageIndex()]
#define HB_TREND_CURSOR_POSITION_MIN_ID HB_TREND_CURSOR_POSITION_MIN_ID[GuiApp::GetLanguageIndex()]
#define HB_TREND_CURSOR_POSITION_MAX_ID HB_TREND_CURSOR_POSITION_MAX_ID[GuiApp::GetLanguageIndex()]
#define TREND_EVENT_COMPLETE_ADVISORY_S TREND_EVENT_COMPLETE_ADVISORY_S[GuiApp::GetLanguageIndex()]
#define HB_IBW_MAX_BASED_ON_APNEA_TIDAL_VOL_ID HB_IBW_MAX_BASED_ON_APNEA_TIDAL_VOL_ID[GuiApp::GetLanguageIndex()]
#define HB_IBW_MIN_BASED_ON_APNEA_TIDAL_VOL_ID HB_IBW_MIN_BASED_ON_APNEA_TIDAL_VOL_ID[GuiApp::GetLanguageIndex()]
#define HB_IBW_MAX_BASED_ON_HIGH_INSP_TIDAL_VOL_MAND_ID HB_IBW_MAX_BASED_ON_HIGH_INSP_TIDAL_VOL_MAND_ID[GuiApp::GetLanguageIndex()]
#define HB_IBW_MIN_BASED_ON_HIGH_INSP_TIDAL_VOL_MAND_ID HB_IBW_MIN_BASED_ON_HIGH_INSP_TIDAL_VOL_MAND_ID[GuiApp::GetLanguageIndex()]
#define HB_IBW_MAX_BASED_ON_HIGH_INSP_TIDAL_VOL_ID HB_IBW_MAX_BASED_ON_HIGH_INSP_TIDAL_VOL_ID[GuiApp::GetLanguageIndex()]
#define HB_IBW_MIN_BASED_ON_HIGH_INSP_TIDAL_VOL_ID HB_IBW_MIN_BASED_ON_HIGH_INSP_TIDAL_VOL_ID[GuiApp::GetLanguageIndex()]
#define HB_IBW_MAX_BASED_ON_HIGH_INSP_TIDAL_VOL_SPONT_ID HB_IBW_MAX_BASED_ON_HIGH_INSP_TIDAL_VOL_SPONT_ID[GuiApp::GetLanguageIndex()]
#define HB_IBW_MIN_BASED_ON_HIGH_INSP_TIDAL_VOL_SPONT_ID HB_IBW_MIN_BASED_ON_HIGH_INSP_TIDAL_VOL_SPONT_ID[GuiApp::GetLanguageIndex()]
#define HB_IBW_MAX_BASED_ON_TIDAL_VOL_ID HB_IBW_MAX_BASED_ON_TIDAL_VOL_ID[GuiApp::GetLanguageIndex()]
#define HB_IBW_MIN_BASED_ON_TIDAL_VOL_ID HB_IBW_MIN_BASED_ON_TIDAL_VOL_ID[GuiApp::GetLanguageIndex()]
#define HB_IBW_MAX_BASED_ON_VOLUME_SUPP_ID HB_IBW_MAX_BASED_ON_VOLUME_SUPP_ID[GuiApp::GetLanguageIndex()]
#define HB_IBW_MIN_BASED_ON_VOLUME_SUPP_ID HB_IBW_MIN_BASED_ON_VOLUME_SUPP_ID[GuiApp::GetLanguageIndex()]
#define HB_IBW_MIN_BASED_ON_ATC_ID HB_IBW_MIN_BASED_ON_ATC_ID[GuiApp::GetLanguageIndex()]
#define HB_IBW_MIN_BASED_ON_PAV_ID HB_IBW_MIN_BASED_ON_PAV_ID[GuiApp::GetLanguageIndex()]
#define EST_SINGLE_TEST_CANCEL_S EST_SINGLE_TEST_CANCEL_S[GuiApp::GetLanguageIndex()]
#define EST_TOUCH_SINGLE_AND_START_TEST_TO_BEGIN_P EST_TOUCH_SINGLE_AND_START_TEST_TO_BEGIN_P[GuiApp::GetLanguageIndex()]
#define EST_SINGLE_TEST_WILL_EXIT_A EST_SINGLE_TEST_WILL_EXIT_A[GuiApp::GetLanguageIndex()]
#define EST_TOUCH_SINGLE_TEST_TO_BEGIN_P EST_TOUCH_SINGLE_TEST_TO_BEGIN_P[GuiApp::GetLanguageIndex()]
#define EST_TOUCH_SINGLE_TEST_TO_BEGIN_A EST_TOUCH_SINGLE_TEST_TO_BEGIN_A[GuiApp::GetLanguageIndex()]
#define EST_TOUCH_SINGLE_TEST_TO_BEGIN_S EST_TOUCH_SINGLE_TEST_TO_BEGIN_S[GuiApp::GetLanguageIndex()]
#define EST_SINGLE_TEST_WILL_BEGIN_A EST_SINGLE_TEST_WILL_BEGIN_A[GuiApp::GetLanguageIndex()]
#define EST_REPEAT_CURRENT_TEST_A EST_REPEAT_CURRENT_TEST_A[GuiApp::GetLanguageIndex()]
#define PROX_START_ADVISORY_P PROX_START_ADVISORY_P[GuiApp::GetLanguageIndex()]
#define SM_CONNECT_PROX_SENSOR_P SM_CONNECT_PROX_SENSOR_P[GuiApp::GetLanguageIndex()]
#define SM_BLOCK_EXP_PORT_P SM_BLOCK_EXP_PORT_P[GuiApp::GetLanguageIndex()]
#define SM_PX_REMOVE_INSP_FILTER_P SM_PX_REMOVE_INSP_FILTER_P[GuiApp::GetLanguageIndex()]
#define SM_REMOVE_EXP_LIMB_P SM_REMOVE_EXP_LIMB_P[GuiApp::GetLanguageIndex()]
#define SM_UNBLOCK_PROX_SENSOR_OUTPUT_P SM_UNBLOCK_PROX_SENSOR_OUTPUT_P[GuiApp::GetLanguageIndex()]
#define SM_BLOCK_PROX_SENSOR_OUTPUT_P SM_BLOCK_PROX_SENSOR_OUTPUT_P[GuiApp::GetLanguageIndex()]
#define SM_UNBLOCK_EXP_PORT_P SM_UNBLOCK_EXP_PORT_P[GuiApp::GetLanguageIndex()]
#define SST_PROMPT_IF_CONNECTED_ACCEPT_YES_AND_CLEAR_NO_S SST_PROMPT_IF_CONNECTED_ACCEPT_YES_AND_CLEAR_NO_S[GuiApp::GetLanguageIndex()]
