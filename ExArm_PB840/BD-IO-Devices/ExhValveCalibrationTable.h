#ifndef EXH_VALVE_CALIBRATION_H

#define EXH_VALVE_CALIBRATION_H

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
//@ Class: ExhValveCalibrationTable_t - Implements exhalation valve calibration.
//***********************************************************************

#include "CalibrationTable.h"

class ExhValveCalibrationTable_t : public CalibrationTable_t
{
public:
	ExhValveCalibrationTable_t(CalibrationID_t CalID);
	CalibrationStatus_t Calibrate(const ConvertedA2dReadings_t *pConvertedData);
	uint16_t GetDriveDAC(Pressure_t P2);
	uint16_t GetDriveDAC_l2h(Pressure_t P2);
	// result IDs for flow sensor cross-check
	enum FlowSensorTestResult
	{
		FLOW_SENSORS_OK,
	//	FLOW_NOT_ESTABLISHED,
		FS_CROSS_CHECK_FAILURE
	};

		// result IDs for pressure sensor cross-check
	enum PressureSensorTestResult
	{
		PRESSURE_SENSORS_OK,
	//	PRESSURE_NOT_ESTABLISHED,
		PS_CROSS_CHECK_FAILURE,
		PS_CROSS_CHECK_ALERT
	};

private:
	typedef enum PDRIVE_CAL_STATE 
	{
		INVALID_STATE = -1,
		PC_START_FLOW, PC_INIT, PC_ZERO_UP, PC_ZERO_DOWN, PC_MOVE, PC_WAIT, 
		PC_CHECK, PC_PRINT, PC_ABORT, PC_COMPLETE, PC_INIT_L2H, 
		PC_ZERO_UP_L2H, PC_ZERO_DOWN_L2H, PC_MOVE_L2H, PC_WAIT_L2H, PC_CHECK_L2H, 
		PC_PRINT_L2H, PC_ABORT_L2H, PC_COMPLETE_L2H
	} PC_CAL_STATE;

	bool DoCalibration(const ConvertedA2dReadings_t *pConvertedData);  //from low to high for pressure controller
	FlowSensorTestResult checkFlowSensors_ (const ConvertedA2dReadings_t *pConvertedData);
	PressureSensorTestResult checkPressureSensors_ (const ConvertedA2dReadings_t *pConvertedData);
	Boolean checkTimeout_( const Uint32 pressure, const Int32 timer);

	PC_CAL_STATE	CalState;
	bool			L2hCalCompleted;
	bool			CalCompleted;
	Pressure_t		PdrvSum;
	Pressure_t		PexhSum;
	Pressure_t		PexhAvg;
	Pressure_t		PdrvAvg;
	Int32			CalId;
};

#endif