
//====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: SortedIterR<TYPE> - Iterator for all 'AbstractSortR<TYPE>'.
//---------------------------------------------------------------------
//@ Interface-Description
//  This provides an iterator for a sorted list of data
//  items where each item is of type 'TYPE'.  Iterators are used to
//  "step" through the elements within a linked list.  The iterator is
//  strictly a constant reference to its list -- no changes to the
//  list can be made using an iterator.  Because of the referencing
//  to a list, it is very dangerous to change/destroy the list while
//  this iterator still exists, therefore iterators should be used in
//  very limited scopes.
//
//  This iterator can attach to abstract, sorted lists containing
//  references to data items of type 'TYPE'.  Any fixed, sorted list of
//  references to data items of type 'TYPE' can be used with this
//  iterator, no matter what 'SIZE' parameter is specified.
//---------------------------------------------------------------------
//@ Rationale
//  Iteration through a constant list is often needed; this provides
//  a means of viewing the items of a list without changing the list.
//---------------------------------------------------------------------
//@ Implementation-Description
//  A constant pointer to the list, and to a particular node in
//  the list are kept within this iterator.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  The list that this iterator is attached to should NOT be changed
//  while this iterator is attached.  If a change occurs to an
//  iterator's list, the iterator should be re-attached to the list before
//  using the iterator again.  (See 'AbstractSortR<TYPE>' for
//  more restrictions.)
//---------------------------------------------------------------------
//@ Invariants
//  (pIterList_ != NULL)
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Foundation/vcssrc/SortedIterR.C_v   25.0.4.0   19 Nov 2013 14:03:50   pvcs  $
//
//@ Modification-Log
//
//   Revision: 003   By: sah   Date: 15-Jun-1999   DR Number: 5405
//   Project:  ATC
//   Description:
//	Enhanced information from failed assertion by added auxillary
//      error codes.
//
//   Revision: 002   By: sah   Date: 07-Jan-1999   DR Number: 5321
//   Project:  ATC
//   Description:
//	Changed 'SoftFault()' method to an non-inlined method.
//
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//=====================================================================

#include "SortedIterR<TYPE>.hh"
#include "TYPE.hh"

//@ Usage-Classes
//@ End-Usage


//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  search(item, foundWhen)
//
//@ Interface-Description
//  Search the list for a node that matches 'item'.  If 'foundWhen'
//  is 'EQUIV_OBJECTS', then a match occurs when an equivalent item
//  is found in the list.  If 'foundWhen' is 'EQUIV_REFERENCES', then
//  a match occurs when a reference to 'item' is found in the list.
//  If a match occurs, then 'SUCCESS' is returned, and the current
//  node is set to the matching node.  If no match is found, then
//  'FAILURE' is returned and the list is left unchanged.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Near the end of this method a pointer to a constant node,
//  'pFoundNode', has its const-ness cast away.  This is due to the
//  fact that 'findNode_()' is a constant method (to allow its use
//  in constant public methods) that returns a pointer.  To ensure
//  consistent const-ness of 'findNode_()', the pointer that is
//  returned is a pointer to a CONSTANT node.  The const-ness of
//  the returned node is not necessary in the design of 'findNode_()',
//  but rather a consistency of design of that method.  Therefore,
//  when this method casts the const-ness of 'pFoundNode' away, it
//  is an acceptable case of working properly with a proper design.
//---------------------------------------------------------------------
//@ PreConditions
//  (foundWhen == EQUIV_OBJECTS  ||  foundWhen == EQUIV_REFERENCES)
//---------------------------------------------------------------------
//@ PostConditions
//  (status == SUCCESS) ==> (currentItem() == item)
//  (status == FAILURE) ==> (*this == OLD{*this})
//@ End-Method
//=====================================================================

SigmaStatus
SortedIterR<TYPE>::search(TYPE&                 item,
			  const EquivResolution foundWhen)
{
  CALL_TRACE("search(item, foundWhen)");

  SigmaStatus  status = FAILURE;

  if (!isEmpty())
  {   // $[TI1]
    // this iterator's list is not empty, therefore use the list to find
    // a node with an item that is equivalent to 'item'...
    const DLinkedNodeC<MemPtr>*  pFoundNode;

    // return a pointer to the link node that is equivalent to 'item'...
    pFoundNode = pIterList_->findNode_(item, foundWhen);

    switch (foundWhen)
    {
    case EQUIV_OBJECTS :	// $[TI1.1]
      if (*((const TYPE*)(pFoundNode->getItem())) == item)
      {   // $[TI1.1.1]
	// the matching OBJECT is found, therefore set this iterator's
	// current node to the found node...
	setCurrent_((DLinkedNodeC<MemPtr>*)pFoundNode); // "const cast away:"
	status = SUCCESS;
      }   // $[TI1.1.2] -- no matching OBJECT was found...

      break;

    case EQUIV_REFERENCES :	// $[TI1.2]
      if ((const TYPE*)(pFoundNode->getItem()) == &item)
      {   // $[TI1.2.1]
	// the matching REFERENCE is found, therefore set this iterator's
	// current node to the found node...
	setCurrent_((DLinkedNodeC<MemPtr>*)pFoundNode);// "const cast away"
	status = SUCCESS;
      }   // $[TI1.2.2] -- no matching REFERENCE was found...

      break;

    default :
      AUX_CLASS_ASSERTION_FAILURE(((foundWhen << 16) | sizeof(TYPE)));
      break;
    }   // end of switch...
  }   // $[TI2] -- this iterator's list is currently empty...

  return(status);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pBoolTest)
//                      [static]
//
//@ Interface-Description
//      Report the software fault that occured within the source code
//      of this class.  The fault, indicated by 'softFaultID', occured
//      at 'lineNumber' in 'pFileName'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//      none
//---------------------------------------------------------------------
//@ PostConditions
//      none
//@ End-Method
//=====================================================================

void
SortedIterR<TYPE>::SoftFault(const SoftFaultID softFaultID,
			     const Uint32      lineNumber,
			     const char*       pFileName,
			     const char*       pBoolTest)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pBoolTest)");
  FaultHandler::SoftFault(softFaultID, FOUNDATION, SORT_ITER_R,
                          lineNumber, pFileName, pBoolTest);
}


//=====================================================================
//
//  Friend Functions...
//
//=====================================================================

#if defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)

#include "Ostream.hh"

//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  operator<<(ostr, sortIter)  [Friend Function]
//
// Interface-Description
//  Dump out an ASCII representation of the current state of this
//  list to 'ostr'.
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreConditions
//  none
//---------------------------------------------------------------------
// PostConditions
//  none
// End-Method
//=====================================================================

Ostream&
operator<<(Ostream& ostr, const SortedIterR<TYPE>& sortIter)
{
  CALL_TRACE("::operator<<(ostr, sortIter)");

  ostr << "SortedIterR<TYPE> {\n";

  if (!sortIter.isEmpty())
  {
    const DLinkedNodeC<MemPtr>*  pNode = sortIter.getFirst_();

    do
    {
      ostr << ((pNode == sortIter.getCurrent_()) ? "->" : "  ")
	   << *((const TYPE*)(pNode->getItem())) << '\n';
    } while ((pNode = pNode->getNextPtr()) != sortIter.getFirst_());
  }

  ostr << "};\n" << endl;
  ostr.flush();

  return(ostr);
}

#endif  // defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)
