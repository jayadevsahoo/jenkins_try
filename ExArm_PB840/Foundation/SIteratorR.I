
//====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//	      Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: SIteratorR<TYPE> - Iterator for 'AbstractSListR<TYPE>'.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Foundation/vcssrc/SIteratorR.I_v   25.0.4.0   19 Nov 2013 14:03:50   pvcs  $
//
//@ Modification-Log
//
//   Revision: 002   By: sah   Date: 07-Jan-1999   DR Number: 5321
//   Project:  ATC
//   Description:
//	Changed 'SoftFault()' method to an non-inlined method.
//  
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//====================================================================

//====================================================================
//
//  Public Methods...
//
//====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  isEmpty()  [const]
//
//@ Interface-Description
//	Is this iterator's list empty?
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//	none
//---------------------------------------------------------------------
//@ PostConditions
//	none
//@ End-Method
//=====================================================================

inline Boolean
SIteratorR<TYPE>::isEmpty(void) const
{
  CALL_TRACE("isEmpty()");
  return(pIterList_->isEmpty());  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  isFull()  [const]
//
//@ Interface-Description
//	Is this iterator's list full?
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//	none
//---------------------------------------------------------------------
//@ PostConditions
//	none
//@ End-Method
//=====================================================================

inline Boolean
SIteratorR<TYPE>::isFull(void) const
{
  CALL_TRACE("isFull()");
  return(pIterList_->isFull());  // $[TI1]
}


//====================================================================
//
//  Protected Methods...
//
//====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getFirst_()  [const]
//
//@ Interface-Description
//	Return a constant pointer to the first node in this iterator's
//	list.  If the list is empty, then 'NULL' is returned.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//	none
//---------------------------------------------------------------------
//@ PostConditions
//	none
//@ End-Method
//=====================================================================

inline const SLinkedNodeC<MemPtr>*
SIteratorR<TYPE>::getFirst_(void) const
{
  CALL_TRACE("getFirst_()");
  return(pIterList_->getFirst_());  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getCurrent_()  [const]
//
//@ Interface-Description
//	Return a constant pointer to the current node in this iterator's
//	list.  If the list is empty, then 'NULL' is returned.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//	none
//---------------------------------------------------------------------
//@ PostConditions
//	none
//@ End-Method
//=====================================================================

inline const SLinkedNodeC<MemPtr>*
SIteratorR<TYPE>::getCurrent_(void) const
{
  CALL_TRACE("getCurrent_()");
  return(pIterCurrent_);  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ==== 
//@ Method:  isLegalNode_(pNode)  [const] 
// 
//@ Interface-Description 
//	Is 'pNode' a pointer to a legal, allocated singly-linked node? 
//---------------------------------------------------------------------
//@ Implementation-Description
//--------------------------------------------------------------------- 
//@ PreConditions 
//	none    
//--------------------------------------------------------------------- 
//@ PostConditions 
//	none 
//@ End-Method 
//===================================================================== 

inline Boolean 
SIteratorR<TYPE>::isLegalNode_(const SLinkedNodeC<MemPtr>* pNode) const  
{ 
  CALL_TRACE("isLegalNode_(pNode)");
  return(pIterList_->isLegalNode_(pNode));  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  setCurrent_(pNewCurr)
//
//@ Interface-Description
//	Set this iterator's current node to 'pNewCurr'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//	(isLegalNode_(pNewCurr))
//---------------------------------------------------------------------
//@ PostConditions
//	none
//@ End-Method
//=====================================================================

inline void
SIteratorR<TYPE>::setCurrent_(SLinkedNodeC<MemPtr>* pNewCurr)
{
  CALL_TRACE("setCurrent_(pNewCurr)");
  SAFE_CLASS_PRE_CONDITION((isLegalNode_(pNewCurr)));
  pIterCurrent_ = pNewCurr;
}  // $[TI1]


//====================================================================
//
//  Public Methods...
//
//====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  attachTo(sIter)
//
//@ Interface-Description
//	Attach this iterator to the list given by 'sIter'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//	none
//---------------------------------------------------------------------
//@ PostConditions
//	(*this == sIter  &&  (isEmpty()  ||
//			      currentItem() == sIter.currentItem()))
//@ End-Method
//=====================================================================

inline void
SIteratorR<TYPE>::attachTo(const SIteratorR<TYPE>& sIter)
{
  CALL_TRACE("attachTo(sIter)");
  pIterList_    = sIter.pIterList_;
  pIterCurrent_ = sIter.pIterCurrent_;
}  // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  attachTo(sList)
//
//@ Interface-Description
//	Attach this iterator to 'sList'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//	none
//---------------------------------------------------------------------
//@ PostConditions
//	(*this == sList  &&  (isEmpty()  ||
//			      currentItem() == sList.currentItem()))
//@ End-Method
//=====================================================================

inline void
SIteratorR<TYPE>::attachTo(const AbstractSListR<TYPE>& sList)
{
  CALL_TRACE("attachTo(sList)");
  pIterList_    = &sList;
  pIterCurrent_ = sList.getCurrent_();
}  // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SIteratorR<TYPE>(sIter)  [Copy Constructor]
//
//@ Interface-Description
//	Attach this iterator to the list of 'sIter'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//	none
//---------------------------------------------------------------------
//@ PostConditions
//	(*this == sIter  &&  (isEmpty()  ||
//			      currentItem() == sIter.currentItem()))
//	none
//@ End-Method
//=====================================================================

inline
SIteratorR<TYPE>::SIteratorR<TYPE>(const SIteratorR<TYPE>& sIter)
{
  CALL_TRACE("SIteratorR<TYPE>(sIter)");
  attachTo(sIter);
}  // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SIteratorR<TYPE>(sList)  [Conversion Constructor]
//
//@ Interface-Description
//	Attach this iterator to 'sList'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//	none
//---------------------------------------------------------------------
//@ PostConditions
//	(*this == sList  &&  (isEmpty()  ||
//			      currentItem() == sList.currentItem()))
//@ End-Method
//=====================================================================

inline
SIteratorR<TYPE>::SIteratorR<TYPE>(const AbstractSListR<TYPE>& sList)
{
  CALL_TRACE("SIteratorR<TYPE>(sList)");
  attachTo(sList);
}  // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ~SIteratorR<TYPE>()  [Destructor]
//
//@ Interface-Description
//	Destroy this iterator by detaching this iterator from its list.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//	none
//---------------------------------------------------------------------
//@ PostConditions
//	none
//@ End-Method
//=====================================================================

inline
SIteratorR<TYPE>::~SIteratorR<TYPE>(void)
{
  CALL_TRACE("~SIteratorR<TYPE>()");
}  // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  isAtFirst()  [const]
//
//@ Interface-Description
//	Is this iterator's list at the front of a non-empty list?
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//	none
//---------------------------------------------------------------------
//@ PostConditions
//	none
//@ End-Method
//=====================================================================

inline Boolean
SIteratorR<TYPE>::isAtFirst(void) const
{
  CALL_TRACE("isAtFirst()");
  return(!isEmpty()  &&  pIterCurrent_ == getFirst_());  // $[TI1] $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  isAtLast()  [const]
//
//@ Interface-Description
//	Is this iterator's list at the end of a non-empty list?
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//	none
//---------------------------------------------------------------------
//@ PostConditions
//	none
//@ End-Method
//=====================================================================

inline Boolean
SIteratorR<TYPE>::isAtLast(void) const
{
  CALL_TRACE("isAtLast()");
  return(!isEmpty()  &&
	 pIterCurrent_->getNextPtr() == NULL);  // $[TI1] (T) $[TI2] (F)
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getNumItems()
//
//@ Interface-Description
//	Return the current number of items in this iterator's list.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//	none
//---------------------------------------------------------------------
//@ PostConditions
//	none
//@ End-Method
//=====================================================================

inline Uint32 
SIteratorR<TYPE>::getNumItems(void) const
{ 
  CALL_TRACE("getNumItems()");
  return(pIterList_->getNumItems());  // $[TI1]
} 


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getMaxItems()
//
//@ Interface-Description
//	Return the maximum number of items allowed in this iterator's
//	list.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//	none
//---------------------------------------------------------------------
//@ PostConditions
//	none
//@ End-Method
//=====================================================================

inline Uint32 
SIteratorR<TYPE>::getMaxItems(void) const
{ 
  CALL_TRACE("getMaxItems()");
  return(pIterList_->getMaxItems());  // $[TI1]
} 


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  goFirst()
//
//@ Interface-Description
//	Move the current pointer to the first node in this iterator's
//	list, and return 'SUCCESS'.  If the list is empty, then 'FAILURE'
//	is returned, and the iterator is left unchanged.
//---------------------------------------------------------------------
//@ Implementation-Description
//	If this iterator's list is not empty, then make this iterator's
//	current node the first node in its list and return 'SUCCESS',
//	otherwise make no change to this iterator and return 'FAILURE'.
//---------------------------------------------------------------------
//@ PreConditions
//	none
//---------------------------------------------------------------------
//@ PostConditions
//	((goFirst() == SUCCESS) ? isAtFirst() : isEmpty())
//@ End-Method
//=====================================================================

inline SigmaStatus
SIteratorR<TYPE>::goFirst(void)
{
  CALL_TRACE("goFirst()");
  return((!isEmpty()) ? (pIterCurrent_ = getFirst_(), SUCCESS)	// $[TI1]
		      : FAILURE);				// $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  goNext()
//
//@ Interface-Description
//	Move the current pointer to the next node in this iterator's
//	list, and return 'SUCCESS'.  If the list is empty or the current
//	node is at the end of this iterator's list, then 'FAILURE' is
//	returned, and the iterator is left unchanged.
//---------------------------------------------------------------------
//@ Implementation-Description
//	If this iterator's list is not empty and this iterator's current
//	node is not the last node in its list, then make this iterator's
//	current node the next node in its list and return 'SUCCESS',
//	otherwise make no change to this iterator and return 'FAILURE'.
//---------------------------------------------------------------------
//@ PreConditions
//	none
//---------------------------------------------------------------------
//@ PostConditions
//	((goNext() == SUCCESS) ? !isAtFirst() : (isEmpty() || isAtLast())
//@ End-Method
//=====================================================================

inline SigmaStatus
SIteratorR<TYPE>::goNext(void)
{
  CALL_TRACE("goNext()");
  return((!isEmpty()  &&  pIterCurrent_->getNextPtr() != NULL)
	  ? (pIterCurrent_ = pIterCurrent_->getNextPtr(), SUCCESS)  // $[TI1]
	  : FAILURE);						    // $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  currentItem()  [const]
//
//@ Interface-Description
//	Return a constant reference to the current item in this iterator.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//	(!isEmpty())
//---------------------------------------------------------------------
//@ PostConditions
//	none
//@ End-Method
//=====================================================================

inline const TYPE&
SIteratorR<TYPE>::currentItem(void) const
{
  CALL_TRACE("currentItem()");
  CLASS_PRE_CONDITION((!isEmpty()));
  return(*((const TYPE*)(getCurrent_()->getItem())));  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  isEquivTo(sIter, equivWhen)  [const]
//
//@ Interface-Description
//	Is this iterator equivalent to 'sIter'?  Two iterators are
//	equivalent if their corresponding lists are equivalent.  When
//	'equivWhen' is 'EQUIV_OBJECTS', then equivalence is based on
//	the referenced objects of each node being equivalent, but when
//	'equivWhen' is 'EQUIV_REFERENCES', then equivalence is base on
//	the references of each node being equivalent.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//	none
//---------------------------------------------------------------------
//@ PostConditions
//	none
//@ End-Method
//=====================================================================

inline Boolean
SIteratorR<TYPE>::isEquivTo(const SIteratorR<TYPE>& sIter,
			    const EquivResolution   equivWhen) const
{
  CALL_TRACE("isEquivTo(sIter, equivWhen)");
  return(pIterList_->isEquivTo(*(sIter.pIterList_), equivWhen));  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  isEquivTo(sList, equivWhen)  [const]
//
//@ Interface-Description
//	Is this iterator equivalent to 'sList'?  When 'equivWhen' is
//	'EQUIV_OBJECTS', then equivalence is based on the referenced
//	objects of each node being equivalent, but when 'equivWhen'
//	is 'EQUIV_REFERENCES', then equivalence is base on the
//	references of each node being equivalent.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//	none
//---------------------------------------------------------------------
//@ PostConditions
//	none
//@ End-Method
//=====================================================================

inline Boolean
SIteratorR<TYPE>::isEquivTo(const AbstractSListR<TYPE>& sList,
			    const EquivResolution       equivWhen) const
{
  CALL_TRACE("isEquivTo(sList, equivWhen)");
  return(pIterList_->isEquivTo(sList, equivWhen));  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  operator==(sIter)  [const]
//
//@ Interface-Description
//	Is this iterator equivalent to 'sIter'?.  Two iterators are
//	equivalent if their corresponding lists are equivalent.
//	(Same as 'isEquivTo(sIter, EQUIV_OBJECTS)'.)
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//	none
//---------------------------------------------------------------------
//@ PostConditions
//	none
//@ End-Method
//=====================================================================

inline Boolean
SIteratorR<TYPE>::operator==(const SIteratorR<TYPE>& sIter) const
{
  CALL_TRACE("operator==(sIter)");
  return(*pIterList_ == *(sIter.pIterList_));  // $[TI1] (T) $[TI2] (F)
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  operator!=(sIter)  [const]
//
//@ Interface-Description
//	Is this iterator NOT equivalent to 'sIter'?.  Two iterators are
//	equivalent if their corresponding lists are equivalent.
//	(Same as '!isEquivTo(sIter, EQUIV_OBJECTS)'.)
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//	none
//---------------------------------------------------------------------
//@ PostConditions
//	none
//@ End-Method
//=====================================================================

inline Boolean
SIteratorR<TYPE>::operator!=(const SIteratorR<TYPE>& sIter) const
{
  CALL_TRACE("operator!=(sIter)");
  return(*pIterList_ != *(sIter.pIterList_));  // $[TI1] (T) $[TI2] (F)
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  operator==(sList)  [const]
//
//@ Interface-Description
//	Is this iterator equivalent to 'sList'?.
//	(Same as 'isEquivTo(sList, EQUIV_OBJECTS)'.)
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//	none
//---------------------------------------------------------------------
//@ PostConditions
//	none
//@ End-Method
//=====================================================================

inline Boolean
SIteratorR<TYPE>::operator==(const AbstractSListR<TYPE>& sList) const
{
  CALL_TRACE("operator==(sList)");
  return(*pIterList_ == sList);  // $[TI1] (T) $[TI2] (F)
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  operator!=(sList)  [const]
//
//@ Interface-Description
//	Is this iterator NOT equivalent to 'sList'?.
//	(Same as '!isEquivTo(sList, EQUIV_OBJECTS)'.)
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//	none
//---------------------------------------------------------------------
//@ PostConditions
//	none
//@ End-Method
//=====================================================================

inline Boolean
SIteratorR<TYPE>::operator!=(const AbstractSListR<TYPE>& sList) const
{
  CALL_TRACE("operator!=(sList)");
  return(*pIterList_ != sList);  // $[TI1] (T) $[TI2] (F)
}
