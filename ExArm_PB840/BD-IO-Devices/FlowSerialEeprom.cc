#include "stdafx.h"
//=======================================================================
// This is a proprietary work to which Puritan-Bennett Corporation
// claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or,
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or, otherwise with the prior written permission of
// Puritan-Bennett Corporation.
//
//		Copyright (c) 1995, Puritan-Bennett Corporation
//
//=======================================================================


// ============================= C L A S S  D E S C R I P T I O N =======
//@ Class:  FlowSerialEeprom - Interface to access the flow sensor calibration
//		data through the serial eeprom.
//-----------------------------------------------------------------------
//@ Interface-Description
//		This class is derived from the SerialEeprom class.  This class provides
//		methods to read all the data in the eeprom.  Methods to read only the
//		checksum and the serial number data are also provided.
//-----------------------------------------------------------------------
//@ Rationale
//		Abstraction of the flow serial eeprom.
//-----------------------------------------------------------------------
//@ Implementation-Description
//		The checksum data is stored in the first 2 bytes of the eeprom.  All
//		the data is read and a checksum is computed excluding the checksum
//		data.  The computed checksum is compared against the checksum data.
//		The values should be the same if there was no error.
//-----------------------------------------------------------------------
//@ Fault-Handling
//		n/a
//-----------------------------------------------------------------------
//@ Restrictions
//-----------------------------------------------------------------------
//@ Invariants
//-----------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/FlowSerialEeprom.ccv   26.0.1.0   04 Feb 2013 10:36:40   pvcs  $
//
//@ Modification-Log
//
//  Revision: 007 By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//	Description:
//		Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 006 By: syw    Date: 27-Juy-1998   DR Number: DCS 5126
//  	Project:  Sigma (R8027)
//		Description:
//			Read byte instead of read word when reading unused portion of eeprom.
//
//  Revision: 005 By: syw    Date: 06-Apr-1998   DR Number: DCS 5065
//  	Project:  Sigma (R8027)
//		Description:
//			Read whole serial eeprom in computation of checksum.  Added a
//			check to determine if a new revised flow sensor was read.  Read
//			serial number as a 10 digit serial number.  Use NEW_SENSOR
//			precompiler to support new sensor functionality.
//
//  Revision: 004 By: syw    Date: 06-Apr-1998   DR Number: DCS 5065
//  	Project:  Sigma (R8027)
//		Description:
//			Read whole serial eeprom in computation of checksum.  Added a
//			check to determine if a new revised flow sensor was read.  Read
//			serial number as a 10 digit serial number.  Use NEW_SENSOR
//			precompiler to support new sensor functionality.
//
//  Revision: 003 By: syw    Date: 01-Apr-1997   DR Number: DCS 1827 
//  	Project:  Sigma (R8027)
//		Description:
//			Remove call to set temperatureToCf_[] table since no longer exist.
//
//  Revision: 002  By: syw    Date:  19-Dec-1996    DR Number: DCS 1641
//  	Project:  Sigma (R8027)
//		Description:
//			Change constructor since we no longer have a rSerialReadWritePort but
//			we now have rSerialReadPort and rSerialWritePort.
//
//  Revision: 001  By:  syw    Date: 08-Nov-1996    DR Number: INITIAL RELEASE
//		Project:  Sigma (R8027)
//		Description:
//			Initial version (Integration baseline).
//
//=======================================================================
#include "FlowSerialEeprom.hh"
#include "BDIORefs.hh"
#include "AioDioDriver.h"

//@ Usage - Classes

#include "FlowSensorCalInfo.hh"

#ifdef SIGMA_DEBUG
#include <stdio.h>
#endif // SIGMA_DEBUG

//@ End - Usage

static const Uint32 TSI_AIR_MODEL		= 840201;
static const Uint32 TSI_O2_MODEL		= 840202;


#if WRITE_DEBUG_FILE

#define	ENA_TSI_DEBUG_FILE	1
#define AIR_TSI_RAM_FILE_NAME		_T("\\AirTsiCalTable.csv")
#define AIR_TSI_SD_FILE_NAME		_T("\\Storage_Card\\AirTsiCalTable.csv")
#define O2_TSI_RAM_FILE_NAME		_T("\\O2TsiCalTable.csv")
#define O2_TSI_SD_FILE_NAME			_T("\\Storage_Card\\O2TsiCalTable.csv")

#endif


#define SWAP_WORD(x)		(((x&0x00FF)<<8) | ((x&0xFF00)>>8))
#define SWAP_LONG(x)		(((x&0xFF000000)>>24) | ((x&0x00FF0000)>>8) | ((x&0x0000FF00)<<8) | ((x&0x000000FF)<<24))

typedef union IntAndFloat
{
	UINT32 IntVal;
	float FloatVal;
}INT_FLOAT_UNION;

/****************************************************************/
// Helper function to swap float value for endianess
float SwapFloat(float InFloat)
{
	INT_FLOAT_UNION SwapUnion;

	SwapUnion.FloatVal = InFloat;
	SwapUnion.IntVal = SWAP_LONG(SwapUnion.IntVal);
	return SwapUnion.FloatVal;
}

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method:  FlowSerialEeprom()
//
//@ Interface-Description
//  	Constructor.  This method has 6 arguments - dataInMask, dataOutMask,
//		chipSelectMask, chipClockMask, writeProtectMask, and two registers and
//		has no return value.
//-----------------------------------------------------------------------
//@ Implementation-Description
//		The arguments passed in are passed to the base class's constructor.
//-----------------------------------------------------------------------
//@ PreCondition
//		None
//-----------------------------------------------------------------------
//@ PostCondition
//		None
//@ End - Method
//=======================================================================
FlowSerialEeprom::FlowSerialEeprom( SetType setType)
{
  	CALL_TRACE("FlowSerialEeprom::FlowSerialEeprom( SetType setType)") ;

	bReadError_ = FALSE;
	bTableRead_ = FALSE;
	errorStatus_ = 0;
	tableType_ = setType;

	readFlowSensorTable_();
}

FlowSerialEeprom::FlowSerialEeprom( )
{
  	CALL_TRACE("FlowSerialEeprom::FlowSerialEeprom( )") ;

	bReadError_ = FALSE;
	bTableRead_ = FALSE;
	errorStatus_ = 0;
	tableType_ = INVALID_SET;
}

// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method:  ~FlowSerialEeprom(void)
//
//@ Interface-Description
//		Destructor.
//-----------------------------------------------------------------------
//@ Implementation-Description
//		None
//-----------------------------------------------------------------------
//@ PreCondition
//  	None
//-----------------------------------------------------------------------
//@ PostCondition
//  	None
//@ End - Method
//=======================================================================
FlowSerialEeprom::~FlowSerialEeprom(void)
{
	CALL_TRACE("FlowSerialEeprom::~FlowSerialEeprom(void)") ;
	
} 

// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method:  readFlowSensorEeprom
//
//@ Interface-Description
//		This method has FlowSensorCalInfo and a SetType as arguments.  This
//		method returns the error status of the read of the serial data.  A
//		non-zero value indicates that an error occurred.
//-----------------------------------------------------------------------
//@ Implementation-Description
//		The data is read and a checksum is computed.  The checksum is compared
//		with the checksum data stored in the eeprom.  The data is stored in
//		the FlowSensorCalInfo object passed in.
//-----------------------------------------------------------------------
//@ PreCondition
//  	None
//-----------------------------------------------------------------------
//@ PostCondition
//  	None
//@ End - Method
//=======================================================================
Uint32
FlowSerialEeprom::readFlowSensorEeprom( FlowSensorCalInfo *calInfo, const SetType setType)
{
	CALL_TRACE("FlowSerialEeprom::readFlowSensorEeprom( FlowSensorCalInfo *calInfo, \
				const SetType setType)") ;

	if(!bTableRead_)
	{
		tableType_ = setType;
		readFlowSensorTable_();
	}

	if(!bReadError_)
	{
		//=========================================================================
		// serial number and model number
		calInfo->setSerialNumber( tsiTable_.serial) ;

		//=========================================================================
		// model
		calInfo->setModel( tsiTable_.model) ;
		
		//=========================================================================
		// revision
		calInfo->setRevision( tsiTable_.rev) ;

		//=========================================================================
		// calibration date
		calInfo->setCalDate( tsiTable_.calDate) ;

		//=========================================================================
		// k factor
		for(int i=0; i<NUM_K_FACTORS; i++)
		{
			calInfo->setKFactor( tsiTable_.kFactor[i], i) ;
		}

		//=========================================================================
		// calibration temperature
		calInfo->setCalTemp(tsiTable_.tcal);

		//=========================================================================
		// span
		calInfo->setSpan(tsiTable_.span);
		
		//=========================================================================
		// zero
		calInfo->setZero(tsiTable_.zero);
		
		//=========================================================================
		// temperature correction factor
		calInfo->setTemperatureCorrection( tsiTable_.tempCorr) ;

		//=========================================================================
		FlowSensorCoeffEntry coeffSet[MAX_FLOW_SENSOR_COEFF_SETS] ;
		
		// coefficients
		for (Uint8 i=0; i < tsiTable_.noCoeff; i++)
		{
			coeffSet[i].setFlowSensorCoeffEntry( tsiTable_.coeff[i].vf, tsiTable_.coeff[i].a, tsiTable_.coeff[i].b, tsiTable_.coeff[i].c) ;
		}

		calInfo->setNumFlowCoeffSets( (Int32)tsiTable_.noCoeff) ;
		calInfo->setCoeffEntry( coeffSet) ;
	}

	return( errorStatus_) ;
}


// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method:	readChecksum
//
//@ Interface-Description
//		This method has no arguments and returns the value read from the
//		eeprom at the checksum address.
//-----------------------------------------------------------------------
//@ Implementation-Description
//		Read data at CHECKSUM_ADDRESS and return data.
//-----------------------------------------------------------------------
//@ PreCondition
//		None
//-----------------------------------------------------------------------
//@ PostCondition
//		None
//@ End - Method
//=======================================================================
Uint16
FlowSerialEeprom::readChecksum( void)
{
	CALL_TRACE("FlowSerialEeprom::readChecksum( void)") ;

	return( tsiTable_.crc) ;
}

// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method:	readSerialNumber
//
//@ Interface-Description
//		This method has no arguments and returns the value read from the
//		eeprom at the serial number address.
//-----------------------------------------------------------------------
//@ Implementation-Description
//		Read data at SERIAL_AND_MODEL_NUMBER_ADDRESS and return only the
//		serial number data.
//-----------------------------------------------------------------------
//@ PreCondition
//		None
//-----------------------------------------------------------------------
//@ PostCondition
//		None
//@ End - Method
//=======================================================================
Uint32
FlowSerialEeprom::readSerialNumber( void)
{
	CALL_TRACE("FlowSerialEeprom::readSerialNumber( void)") ;

	return( tsiTable_.serial) ;
}


// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: SoftFault
//
//@ Interface-Description
//  This method takes four arguments: softFaultID, lineNumber, pFileName,
//  and pPredicate and is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//-----------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//-----------------------------------------------------------------------
//@ PreCondition
//  none
//-----------------------------------------------------------------------
//@ PostCondition
//  none
//@ End - Method
//=======================================================================
void
FlowSerialEeprom::SoftFault(const SoftFaultID softFaultID,
			const Uint32      lineNumber,
			const char*       pFileName,
			const char*       pPredicate)
{
	CALL_TRACE("SoftFault( softFaultID, lineNumber, pFileName, pPredicate)") ;
  
  	FaultHandler::SoftFault( softFaultID, BD_IO_DEVICES, FLOWSERIALEEPROM,
    		                 lineNumber, pFileName, pPredicate) ;

}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//=====================================================================
//
//  Private Methods...
//
//=====================================================================

// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method:	readSerialNumber
//
//@ Interface-Description
//		This method has no arguments and returns the value read from the
//		eeprom at the serial number address.
//-----------------------------------------------------------------------
//@ Implementation-Description
//		Read data at SERIAL_AND_MODEL_NUMBER_ADDRESS and return only the
//		serial number data.
//-----------------------------------------------------------------------
//@ PreCondition
//		None
//-----------------------------------------------------------------------
//@ PostCondition
//		None
//@ End - Method
//=======================================================================
void FlowSerialEeprom::readFlowSensorTable_()
{
	Uint8 InBuffer[TSI_EEPROM_TAB_SIZE];
	Boolean success;
	Boolean checksumValidated;
	TSI_TABLE_STRUCT *pTsiTab = &tsiTable_;
	
	// assume read failure
	bReadError_ = TRUE;

	TsiFlowType_t tsiType;
	if(tableType_ == AIR_SET)
	{
		tsiType = TSI_AIR_FLOW;
	}
	else if(tableType_ == O2_SET)
	{
		tsiType = TSI_O2_FLOW;
	}
	else
	{
		return;
	}

	success = AioDioDriver.ReadTsiTable(tsiType, InBuffer);
	if(success)
	{	
		memcpy(pTsiTab, InBuffer, sizeof(Uint16));
		memcpy((Uint8 *)pTsiTab + 4, InBuffer + sizeof(Uint16), 14);
		memcpy((Uint8 *)pTsiTab + 20, InBuffer + 16, 240);

		checksumValidated = validateChecksum_(InBuffer, TSI_EEPROM_TAB_SIZE);
		if(checksumValidated)
		{
			bTableRead_ = TRUE;	// set flag so that don't have to read again
			bReadError_ = FALSE;

			// convert from Big Endian to Little Endian
			pTsiTab->crc = SWAP_WORD(pTsiTab->crc);			
			pTsiTab->serial = SWAP_LONG(pTsiTab->serial);
			pTsiTab->model = SWAP_LONG(pTsiTab->model);
			pTsiTab->calYear = SWAP_WORD(pTsiTab->calYear);
			for(int i=0; i<NUM_K_FACTORS; i++)
			{
				pTsiTab->kFactor[i] = SwapFloat(pTsiTab->kFactor[i]);
			}
			pTsiTab->tcal = SwapFloat(pTsiTab->tcal);
			pTsiTab->span = SwapFloat(pTsiTab->span);
			pTsiTab->zero = SwapFloat(pTsiTab->zero);
			pTsiTab->tempCorr = SwapFloat(pTsiTab->tempCorr);

			for(int i =0; i < MAX_FLOW_SENSOR_COEFF_SETS; i++)
			{
				pTsiTab->coeff[i].a = SwapFloat(pTsiTab->coeff[i].a);
				pTsiTab->coeff[i].b = SwapFloat(pTsiTab->coeff[i].b);
				pTsiTab->coeff[i].c = SwapFloat(pTsiTab->coeff[i].c);
				pTsiTab->coeff[i].vf = SwapFloat(pTsiTab->coeff[i].vf);
			}
		}
	}
}


// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method:	calcCRC_
//
//@ Interface-Description
//	Inputs:
//		pData	- pointer to data to be CRCed
//		size	- size of the data in bytes
//-----------------------------------------------------------------------
//@ Implementation-Description
//		Calculate the crc of the data using algorithm specified by TSI
//		flow sensor.
//-----------------------------------------------------------------------
//@ PreCondition
//		None
//-----------------------------------------------------------------------
//@ PostCondition
//		None
//@ End - Method
//=======================================================================
Uint16 FlowSerialEeprom::calcCRC_(Uint8 *pData, Uint32 size)
{
	Uint16 crc, bitCount;

	crc = 0x0000;
	for(Uint32 i=0; i<size; i++)
	{
		crc ^= pData[i];
		for(bitCount=8; bitCount; bitCount--)
		{
			if(crc&0x0001)
			{
				crc >>= 1;
				crc ^= 0xa001;
			}
			else
				crc >>= 1;
		}
	}
	return(crc);
}


// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method:	validateChecksum_
//
//@ Interface-Description
//	Inputs:
//		pTable	- pointer to the TSI flow table
//		size	- size of the flow table		
//-----------------------------------------------------------------------
//@ Implementation-Description
//		Validate the checksum of the TSI flow sensor.
//		Return TRUE if valid; else, return FALSE
//-----------------------------------------------------------------------
//@ PreCondition
//		None
//-----------------------------------------------------------------------
//@ PostCondition
//		None
//@ End - Method
//=======================================================================
Boolean FlowSerialEeprom::validateChecksum_(Uint8 *pTable, Uint32 size)
{
	Uint16 readChecksum = SWAP_WORD(*((Uint16 *)pTable));

	Uint16 computedChecksum = calcCRC_(pTable+sizeof(Uint16), size - sizeof(Uint16));

	if(computedChecksum == 0)
	{
		errorStatus_ |= CHECKSUM_RANGE ;
	}
	else if(computedChecksum == readChecksum)
	{
		errorStatus_ = 0 ;
		return TRUE;
	}
	else
	{
		errorStatus_ |= CHECKSUM_EQUAL ;
	}

	return FALSE;
}


// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method:	printTsiTable_
//
//@ Interface-Description
//-----------------------------------------------------------------------
//@ Implementation-Description
//		Helper function used for debug.
//-----------------------------------------------------------------------
//@ PreCondition
//		None
//-----------------------------------------------------------------------
//@ PostCondition
//		None
//@ End - Method
//=======================================================================
void FlowSerialEeprom::printTsiTable_(TSI_TABLE_STRUCT *pTsiTab, LPCTSTR lpRamFileName, LPCTSTR lpSdFileName)
{
#if ENA_TSI_DEBUG_FILE
	//TSI_TABLE_STRUCT *pTsiTab = &tsiTable_;
	float Buf[48];
	int i, j;

	HANDLE hFile = OpenDebugFile(lpRamFileName);
	if(!hFile)
	{
		return;
	}

	WriteDebugFileString(hFile, L"CRC,Serial,Model,Revision,CalYr,CalMo,CalDay,k0,k1,k2,k3,k4,TempCal,Span,Zero,TempCorr,NumCoeff\r\n");
	i = 0;
	Buf[i++] = (float)pTsiTab->crc;
	Buf[i++] = (float)pTsiTab->serial;
	Buf[i++] = (float)pTsiTab->model;
	Buf[i++] = (float)pTsiTab->rev;
	Buf[i++] = (float)pTsiTab->cal_yr;
	Buf[i++] = (float)pTsiTab->cal_mon;
	Buf[i++] = (float)pTsiTab->cal_date;
	Buf[i++] = pTsiTab->k0;
	Buf[i++] = pTsiTab->k1;
	Buf[i++] = pTsiTab->k2;
	Buf[i++] = pTsiTab->k3;
	Buf[i++] = pTsiTab->k4;
	Buf[i++] = pTsiTab->tcal;
	Buf[i++] = pTsiTab->span;
	Buf[i++] = pTsiTab->zero;
	Buf[i++] = pTsiTab->tcorr;
	Buf[i++] = pTsiTab->no_coeff;
	WriteDebugFile8Decimals(hFile, Buf, i);

	WriteDebugFileString(hFile, L"\r\nCalTable[12]\r\n");
	WriteDebugFileString(hFile, L" ,a,b,c,Vf\r\n ,");
	for(int i =0; i < 12; i++)
	{
		j = 0;
		Buf[j++] = pTsiTab->cal[i].a;
		Buf[j++] = pTsiTab->cal[i].b;
		Buf[j++] = pTsiTab->cal[i].c;
		Buf[j++] = pTsiTab->cal[i].vf;
		WriteDebugFile8Decimals(hFile, Buf, 4);
		WriteDebugFileString(hFile, L" ,");
	}
	CloseDebugFile(hFile);
	CopyFile(lpRamFileName, lpSdFileName, false);
#endif
}
