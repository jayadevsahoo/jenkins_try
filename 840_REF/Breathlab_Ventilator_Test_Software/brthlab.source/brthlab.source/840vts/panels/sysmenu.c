/*****************************************************************************/
/*   Module Name:    SYSMENU.c                                          
/*   Purpose:      Functions to support manipulation of the System Menu                                
/*                                                                          
/*                                                                           
/*   Requirements:   This module implements requirements detailed in         
/*          <75359-93>                                                       
/*                                                                           
/*   $Revision::   1.2      $                                                
/*                                                                          
/*****************************************************************************/

/*****************************************************************************
THIS IS A PROPRIETARY WORK TO WHICH NELLCOR PURITAN BENNETT CORPORATION CLAIMS
EXCLUSIVE RIGHT.  NO PART OF THIS WORK MAY BE USED, DISCLOSED, REPRODUCED,
STORED IN AN INFORMATION RETIEVAL SYSTEM, OR TRANSMITTED, IN ANY FORM OR BY ANY
MEANS ELECTRONIC, MECHANICAL, PHOTOCOPYING, RECORDING OR OTHERWISE WITHOUT THE
PRIOR WRITTEN PERMISSION OF NELLCOR PURITAN BENNETT CORPORATION.
 					  
Copyright (c) 1999, Mallinckrodt Nellcor Puritan Bennett
All rights reserved.
*******************************************************************************/

/***********************************************/
/****************** INCLUDES *******************/
/***********************************************/

#include <userint.h>
#include "840.h"
#include "cpvtmain.h"
#include "cvicpreh.h"
#include "dbapi.h"
#include "helpinit.h"
#include "ldb.h"
#include "logonfun.h"
#include "mainpnls.h"
#include "prmptpnl.h"
#include "pts.h"
#include "report.h"
#include "tt.h"
#include "version.h"
#include "sysmenu.h"

/***********************************************/
/************ FUNCTION DECLARATION  ************/
/***********************************************/

/*****************************************************************************/
/* Name:     SetSysMenuMode                                                    
/*                                                                           
/* Purpose:  Dimms and Undimms system panel controls based on the current state
/*                                          
/*                                                                           
/* Example Call:   void SetSysMenuMode(int Mode);                                    
/*                                                                           
/* Input:    int Mode   The state which the syspanel should be set to          
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
void SetSysMenuMode(int Mode)
{
	switch(Mode){
		case SYS_ALL_ON:
			SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_CMD_HELP,   ATTR_DIMMED, FALSE);
			SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_CMD_ADMIN,  ATTR_DIMMED, FALSE);
			SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_CMD_TESTS,  ATTR_DIMMED, FALSE);
			SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_CMD_PTS,    ATTR_DIMMED, FALSE);
			SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_CMD_840,    ATTR_DIMMED, FALSE);
			SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_CMD_REPORT, ATTR_DIMMED, FALSE);
			SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_CMD_EXIT,   ATTR_DIMMED, FALSE);
			break;
		case SYS_ALL_OFF:
			SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_CMD_HELP,   ATTR_DIMMED, TRUE);
			SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_CMD_ADMIN,  ATTR_DIMMED, TRUE);
			SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_CMD_TESTS,  ATTR_DIMMED, TRUE);
			SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_CMD_PTS,    ATTR_DIMMED, TRUE);
			SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_CMD_840,    ATTR_DIMMED, TRUE);
			SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_CMD_REPORT, ATTR_DIMMED, TRUE);
			SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_CMD_EXIT,   ATTR_DIMMED, TRUE);
			break;
		case SYS_HELP:
			SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_CMD_HELP,   ATTR_DIMMED, TRUE);
			SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_CMD_ADMIN,  ATTR_DIMMED, FALSE);
			SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_CMD_TESTS,  ATTR_DIMMED, FALSE);
			SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_CMD_PTS,    ATTR_DIMMED, FALSE);
			SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_CMD_840,    ATTR_DIMMED, FALSE);
			SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_CMD_REPORT, ATTR_DIMMED, FALSE);
			SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_CMD_EXIT,   ATTR_DIMMED, FALSE);
			break;
		case SYS_SETUP:
			SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_CMD_HELP,   ATTR_DIMMED, FALSE);
			SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_CMD_ADMIN,  ATTR_DIMMED, TRUE);
			SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_CMD_TESTS,  ATTR_DIMMED, FALSE);
			SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_CMD_PTS,    ATTR_DIMMED, FALSE);
			SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_CMD_840,    ATTR_DIMMED, FALSE);
			SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_CMD_REPORT, ATTR_DIMMED, FALSE);
			SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_CMD_EXIT,   ATTR_DIMMED, FALSE);
			break;
		case SYS_TEST:
			SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_CMD_HELP,   ATTR_DIMMED, FALSE);
			SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_CMD_ADMIN,  ATTR_DIMMED, FALSE);
			SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_CMD_TESTS,  ATTR_DIMMED, TRUE);
			SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_CMD_PTS,    ATTR_DIMMED, FALSE);
			SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_CMD_840,    ATTR_DIMMED, FALSE);
			SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_CMD_REPORT, ATTR_DIMMED, FALSE);
			SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_CMD_EXIT,   ATTR_DIMMED, FALSE);
			break;
		case SYS_PTS:
			SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_CMD_HELP,   ATTR_DIMMED, FALSE);
			SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_CMD_ADMIN,  ATTR_DIMMED, FALSE);
			SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_CMD_TESTS,  ATTR_DIMMED, FALSE);
			SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_CMD_PTS,    ATTR_DIMMED, TRUE);
			SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_CMD_840,    ATTR_DIMMED, FALSE);
			SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_CMD_REPORT, ATTR_DIMMED, FALSE);
			SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_CMD_EXIT,   ATTR_DIMMED, FALSE);
			break;
		case SYS_840:
			SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_CMD_HELP,   ATTR_DIMMED, FALSE);
			SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_CMD_ADMIN,  ATTR_DIMMED, FALSE);
			SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_CMD_TESTS,  ATTR_DIMMED, FALSE);
			SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_CMD_PTS,    ATTR_DIMMED, FALSE);
			SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_CMD_840,    ATTR_DIMMED, TRUE);
			SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_CMD_REPORT, ATTR_DIMMED, FALSE);
			SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_CMD_EXIT,   ATTR_DIMMED, FALSE);
			break;
		case SYS_REPORT:
			SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_CMD_HELP,   ATTR_DIMMED, FALSE);
			SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_CMD_ADMIN,  ATTR_DIMMED, FALSE);
			SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_CMD_TESTS,  ATTR_DIMMED, FALSE);
			SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_CMD_PTS,    ATTR_DIMMED, FALSE);
			SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_CMD_840,    ATTR_DIMMED, FALSE);
			SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_CMD_REPORT, ATTR_DIMMED, TRUE);
			SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_CMD_EXIT,   ATTR_DIMMED, FALSE);
			break;
		case SYS_EXIT:
			SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_CMD_HELP,   ATTR_DIMMED, FALSE);
			SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_CMD_ADMIN,  ATTR_DIMMED, FALSE);
			SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_CMD_TESTS,  ATTR_DIMMED, FALSE);
			SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_CMD_PTS,    ATTR_DIMMED, FALSE);
			SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_CMD_840,    ATTR_DIMMED, FALSE);
			SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_CMD_REPORT, ATTR_DIMMED, FALSE);
			SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_CMD_EXIT,   ATTR_DIMMED, TRUE);
			break;
		default:
			break;
	}

	return;
}


/*****************************************************************************/
/* Name:    SetTestButtonMode                                               
/*                                                                           
/* Purpose:  Dimms and Undimms test buttons based on the current state
/*                                          
/*                                                                           
/* Example Call:   void SetTestButtonMode(int state);                                    
/*                                                                           
/* Input:    int state   The state which the test buttons should be set to      
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
void SetTestButtonMode(int state)
{
	SetCtrlAttribute(gPanelHandle[TESTPANEL], TESTPANEL_SEQUENCEDISPLAY, ATTR_CTRL_MODE, VAL_INDICATOR);
	switch(state){
		case TB_NORMAL:
			SetSysPanelMessage("");
			SetCtrlAttribute(gPanelHandle[TESTPANEL], TESTPANEL_TESTUUT_DEV,  ATTR_DIMMED, FALSE);
			SetCtrlAttribute(gPanelHandle[TESTPANEL], TESTPANEL_RUNTEST_DEV,  ATTR_DIMMED, FALSE);
			SetCtrlAttribute(gPanelHandle[TESTPANEL], TESTPANEL_LOOPTEST_DEV, ATTR_DIMMED, FALSE);
			SetCtrlAttribute(gPanelHandle[TESTPANEL], TESTPANEL_STOP_BTN,     ATTR_DIMMED, TRUE);
			SetCtrlAttribute(gPanelHandle[TESTPANEL], TESTPANEL_PAUSE_BTN,    ATTR_DIMMED, TRUE);
			SetCtrlAttribute(gPanelHandle[TESTPANEL], TESTPANEL_SEQUENCEDISPLAY, ATTR_CTRL_MODE, VAL_HOT);
			break;
		case TB_RUNNING:
		case TB_RUNNING_SINGLE:
			SetCtrlAttribute(gPanelHandle[TESTPANEL], TESTPANEL_TESTUUT_DEV,  ATTR_DIMMED, TRUE);
			SetCtrlAttribute(gPanelHandle[TESTPANEL], TESTPANEL_RUNTEST_DEV,  ATTR_DIMMED, TRUE);
			SetCtrlAttribute(gPanelHandle[TESTPANEL], TESTPANEL_LOOPTEST_DEV, ATTR_DIMMED, TRUE);
			SetCtrlAttribute(gPanelHandle[TESTPANEL], TESTPANEL_STOP_BTN,     ATTR_DIMMED, FALSE);
			SetCtrlAttribute(gPanelHandle[TESTPANEL], TESTPANEL_PAUSE_BTN,    ATTR_DIMMED, FALSE);
			break;
		case TB_DEVICE_NOT_DETECTED:
			SetCtrlAttribute(gPanelHandle[TESTPANEL], TESTPANEL_TESTUUT_DEV,  ATTR_DIMMED, TRUE);
			SetCtrlAttribute(gPanelHandle[TESTPANEL], TESTPANEL_RUNTEST_DEV,  ATTR_DIMMED, TRUE);
			SetCtrlAttribute(gPanelHandle[TESTPANEL], TESTPANEL_LOOPTEST_DEV, ATTR_DIMMED, TRUE);
			SetCtrlAttribute(gPanelHandle[TESTPANEL], TESTPANEL_STOP_BTN,     ATTR_DIMMED, TRUE);
			SetCtrlAttribute(gPanelHandle[TESTPANEL], TESTPANEL_PAUSE_BTN,    ATTR_DIMMED, TRUE);
			break;
		case TB_PAUSED:
			SetSysPanelMessage(LDBf(Ldb_PauseMsg2));
			SetCtrlAttribute(gPanelHandle[TESTPANEL], TESTPANEL_TESTUUT_DEV,  ATTR_DIMMED, FALSE);
			SetCtrlAttribute(gPanelHandle[TESTPANEL], TESTPANEL_RUNTEST_DEV,  ATTR_DIMMED, TRUE);
			SetCtrlAttribute(gPanelHandle[TESTPANEL], TESTPANEL_LOOPTEST_DEV, ATTR_DIMMED, TRUE);
			SetCtrlAttribute(gPanelHandle[TESTPANEL], TESTPANEL_STOP_BTN,     ATTR_DIMMED, FALSE);
			SetCtrlAttribute(gPanelHandle[TESTPANEL], TESTPANEL_PAUSE_BTN,    ATTR_DIMMED, TRUE);
			break;
		case TB_STOPPED:
			SetSysPanelMessage(LDBf(Ldb_Stopped_Fnsh_tst));
			SetCtrlAttribute(gPanelHandle[TESTPANEL], TESTPANEL_TESTUUT_DEV,  ATTR_DIMMED, TRUE);
			SetCtrlAttribute(gPanelHandle[TESTPANEL], TESTPANEL_RUNTEST_DEV,  ATTR_DIMMED, TRUE);
			SetCtrlAttribute(gPanelHandle[TESTPANEL], TESTPANEL_LOOPTEST_DEV, ATTR_DIMMED, TRUE);
			SetCtrlAttribute(gPanelHandle[TESTPANEL], TESTPANEL_STOP_BTN,     ATTR_DIMMED, TRUE);
			SetCtrlAttribute(gPanelHandle[TESTPANEL], TESTPANEL_PAUSE_BTN,    ATTR_DIMMED, TRUE);
			break;
	}
	ProcessSystemEvents();
	ProcessDrawEvents();

	return;
}


/*****************************************************************************/
/* Name:     SetSysPanelMessage                                                
/*                                                                           
/* Purpose:  Displays a message on the System panel
/*                                          
/*                                                                           
/* Example Call:  void SetSysPanelMessage(char* cbuff);                                    
/*                                                                           
/* Input:    char* cbuff   The message to display         
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
void SetSysPanelMessage(char* cbuff)
{
	ResetTextBox(gPanelHandle[SYSPANEL], SYSPANEL_STATUS, "");
	if(cbuff != NULL){
		InsertTextBoxLine(gPanelHandle[SYSPANEL], SYSPANEL_STATUS, -1, cbuff);
	}
	else{
		InsertTextBoxLine(gPanelHandle[SYSPANEL], SYSPANEL_STATUS, -1, "");
	}
	ProcessDrawEvents();
	ProcessSystemEvents();

	return;

}


/*****************************************************************************/
/* Name:     CBFileMenuLogout                                                
/*                                                                           
/* Purpose:  Handler for the file menu logout item
/*                                          
/*                                                                           
/* Example Call:  Called by system                                   
/*                                                                           
/* Input:    various parameters supplied by system      
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
void CVICALLBACK CBFileMenuLogout(int menuBar, int menuItem, void *callbackData,
		int panel)
{
	// Just call the button handler directly
	SysPanelCallback(panel, SYSPANEL_CMD_EXIT, EVENT_COMMIT, NULL, 0, 0);

	return;	
}


/*****************************************************************************/
/* Name:     CBFileMenuExit                                                
/*                                                                           
/* Purpose:  Handler for the file menu exit item
/*                                          
/*                                                                           
/* Example Call:  Called by system                                   
/*                                                                           
/* Input:    various parameters supplied by system      
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
void CVICALLBACK CBFileMenuExit(int menuBar, int menuItem, void *callbackData,
		int panel)
{
	ExitWrapper();

	return;
}


/*****************************************************************************/
/* Name:     CBEditMenuCalibrate                                                
/*                                                                           
/* Purpose:  Handler for the edit menu calibrate item
/*                                          
/*                                                                           
/* Example Call:  Called by system                                   
/*                                                                           
/* Input:    various parameters supplied by system      
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
void CVICALLBACK CBEditMenuCalibrate(int menuBar, int menuItem, void *callbackData,
		int panel)
{
	// Just call the button handler directly
	CmdAutoZero(gPanelHandle[PTSPANEL], PTSPANEL_CALIBRATE, EVENT_COMMIT, NULL, 0, 0); // PTSPANEL_CALIBRATE

	return;
}


/*****************************************************************************/
/* Name:     CBViewMenuTest                                                
/*                                                                           
/* Purpose:  Handler for the view menu Test item
/*                                          
/*                                                                           
/* Example Call:  Called by system                                   
/*                                                                           
/* Input:    various parameters supplied by system      
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
void CVICALLBACK CBViewMenuTest(int menuBar, int menuItem, void *callbackData,
		int panel)
{
	// Just call the button handler directly
	SysPanelCallback(panel, SYSPANEL_CMD_TESTS, EVENT_COMMIT, NULL, 0, 0);

	return;	
}

/*****************************************************************************/
/* Name:     CBViewMenu840                                                
/*                                                                           
/* Purpose:  Handler for the view menu 840 item.
/*                                          
/*                                                                           
/* Example Call:  Called by system                                   
/*                                                                           
/* Input:    various parameters supplied by system      
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
void CVICALLBACK CBViewMenu840(int menuBar, int menuItem, void *callbackData,
		int panel)
{
	// Just call the button handler directly
	SysPanelCallback(panel, SYSPANEL_CMD_840, EVENT_COMMIT, NULL, 0, 0);

	return;	
}


/*****************************************************************************/
/* Name:     CBViewMenuPTS                                                
/*                                                                           
/* Purpose:  Handler for the view menu PTS-2000 item
/*                                          
/*                                                                           
/* Example Call:  Called by system                                   
/*                                                                           
/* Input:    various parameters supplied by system      
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
void CVICALLBACK CBViewMenuPTS(int menuBar, int menuItem, void *callbackData,
		int panel)
{
	// Just call the button handler directly
	SysPanelCallback(panel, SYSPANEL_CMD_PTS, EVENT_COMMIT, NULL, 0, 0);

	return;	
}


/*****************************************************************************/
/* Name:     CBMenuReport                                                
/*                                                                           
/* Purpose:  Handler for the report menu item
/*                                          
/*                                                                           
/* Example Call:  Called by system                                   
/*                                                                           
/* Input:    various parameters supplied by system      
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
void CVICALLBACK CBMenuReport(int menuBar, int menuItem, void *callbackData,
		int panel)
{
	// Just call the button handler directly
	SysPanelCallback(panel, SYSPANEL_CMD_REPORT, EVENT_COMMIT, NULL, 0, 0);

	return;	
}


/*****************************************************************************/
/* Name:     CBMenuSetup                                                
/*                                                                           
/* Purpose:  Handler for the setup menu item
/*                                          
/*                                                                           
/* Example Call:  Called by system                                   
/*                                                                           
/* Input:    various parameters supplied by system      
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
void CVICALLBACK CBMenuSetup(int menuBar, int menuItem, void *callbackData,
		int panel)
{
	// Just call the button handler directly
	SysPanelCallback(panel, SYSPANEL_CMD_ADMIN, EVENT_COMMIT, NULL, 0, 0);

	return;	
}


/*****************************************************************************/
/* Name:     CBMenuHelpContents                                                
/*                                                                           
/* Purpose:  Handler for the help menu contents item
/*                                          
/*                                                                           
/* Example Call:  Called by system                                   
/*                                                                           
/* Input:    various parameters supplied by system      
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
void CVICALLBACK CBMenuHelpContents(int menuBar, int menuItem, void *callbackData,
		int panel)
{
	// Just call the button handler directly
	SysPanelCallback(panel, SYSPANEL_CMD_HELP, EVENT_COMMIT, NULL, 0, 0);

	return;	
}


/*****************************************************************************/
/* Name:     CBMenuHelpAbout                                                
/*                                                                           
/* Purpose:  Handler for the help menu "about" item
/*                                          
/*                                                                           
/* Example Call:  Called by system                                   
/*                                                                           
/* Input:    various parameters supplied by system      
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
void CVICALLBACK CBMenuHelpAbout(int menuBar, int menuItem, void *callbackData,
		int panel)
{
	// Just call the button handler directly
	// Load splash panel here for awhile.....
	ShowHelpAbout(panel);

	return;	
}


/*****************************************************************************/
/* Name:      SysPanelCallback                                                    
/*                                                                           
/* Purpose:  Handle button presses on the system panel
/*                                          
/*                                                                           
/* Example Call:   int CVICALLBACK SysPanelCallback(int panel, int control, int event,
/*		void *callbackData, int eventData1, int eventData2);                                    
/*                                                                           
/* Input:    standard callback arguments   
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   always 0                                                        
/*                                                                           
/*****************************************************************************/
int CVICALLBACK SysPanelCallback(int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int attrib;

	if(event == EVENT_COMMIT){
		switch(control){
			case SYSPANEL_CMD_ADMIN:
				SwitchPanels(gPanelHandle[ADMINPANEL]);
				SetActivePanel(gPanelHandle[ADMINPANEL]);
				SetSysMenuMode(SYS_SETUP);
				break;
			case SYSPANEL_CMD_PTS:
				SwitchPanels(gPanelHandle[PTSPANEL]);
				SetActivePanel(gPanelHandle[PTSPANEL]);
				SetSysMenuMode(SYS_PTS);
				break;
			case SYSPANEL_CMD_840:
				SwitchPanels(gPanelHandle[_840PANEL]);
				SetActivePanel(gPanelHandle[_840PANEL]);
				SetSysMenuMode(SYS_840);
				break;
			case SYSPANEL_CMD_HELP:
				ShowHelp(panel, control, GENERAL_HELP);
				break;
			case SYSPANEL_CMD_TESTS:
				SwitchPanels(gPanelHandle[TESTPANEL]);
				SetActivePanel(gPanelHandle[TESTPANEL]);
				SetSysMenuMode(SYS_TEST);
				break;
			case SYSPANEL_CMD_REPORT:
				DisplayReport();
				break;
			case SYSPANEL_CMD_EXIT:
				#ifdef CRPE_ENABLED
				CloseCrystalReportPrintEngine();	// Close Crystal Reports
				#endif
				CloseJetDB(); //we have to make sure we close the DAO connection or a ASSERT will fail.

				TestPrompt(Discard);
				HidePanel(gPanelHandle[SYSPANEL]);
				LoadPanels(APPRESTART);
				login(Is840Connected(),IsPTSConnected());
				break;
		} // end switch(control)
	} // end if(event == EVENT_COMMIT)

	return 0;
}

