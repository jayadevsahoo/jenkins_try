#include "StdAfx.h"
#include "Plot.h"
#include "AntiAliasLine.h"

Plot::Plot(HWND hWnd, int width, int height, int timeLen, int dataPerSec)
{
	m_hWnd = hWnd;
	m_width = width;
	m_height = height;
	m_timeLen = timeLen;
	m_dataPerSec = dataPerSec;
	m_maxDataCount = timeLen * dataPerSec;

	m_maxData = 200;
	m_baseData = 100;
	m_minData = 0;

	m_topMargin = 15;
	m_rightMargin = 30;
	m_markLength = 5;

	m_Origin.x = 80;
	m_Origin.y = m_height - 20;

	m_dataDelta = static_cast<int>(m_maxData - m_minData);

	m_waveWidth = (m_width - m_Origin.x - m_rightMargin) / m_timeLen * m_timeLen;
	m_waveHeight = m_Origin.y - m_topMargin;

	m_waveRect.left = m_Origin.x;
	m_waveRect.top = m_topMargin;
	m_waveRect.right = m_waveRect.left + m_waveWidth;
	m_waveRect.bottom = m_Origin.y;

	m_dwWiperResetTime = 0;
	m_bBkgndInvalid = true;
	m_bWiperReset = false;
	m_pixelPerData = (double)m_waveWidth / (m_timeLen * dataPerSec);
	m_drawnCount = 0;

	// build memory bitmap and memory dc
	HDC hDC = GetDC(m_hWnd);
	m_pWndDC = CDC::FromHandle(hDC);

	// create memory buffer for entire plot
	m_dcMem.CreateCompatibleDC(m_pWndDC);
	m_bmpMem.CreateCompatibleBitmap(m_pWndDC, m_width, m_height);
	m_pOldMemBmp = m_dcMem.SelectObject(&m_bmpMem);

	// create memory bitmap buffer for background
	m_bmpBkgnd.CreateCompatibleBitmap(m_pWndDC, m_width, m_height);
	m_dcBkgnd.CreateCompatibleDC(m_pWndDC);
	m_pOldBkgndBmp = m_dcBkgnd.SelectObject(&m_bmpBkgnd);

	ReleaseDC(m_hWnd, hDC);
}

Plot::~Plot(void)
{
	m_dcMem.SelectObject(m_pOldMemBmp);
	m_dcMem.DeleteDC();	
	m_bmpMem.DeleteObject();

	m_dcBkgnd.SelectObject(m_pOldBkgndBmp);
	m_dcBkgnd.DeleteDC();
	m_bmpBkgnd.DeleteObject();
}

void Plot::draw(int x, int y, const deque<int>& data, const deque<int>& oldData )
{
	ASSERT(m_pWndDC != NULL);

	CPaintDC paintDC(CWnd::FromHandle(m_hWnd));

	// TimeSnapshot::getInstance()->bmpStart();
	if ( m_bBkgndInvalid )
	{
		// draw graphics to cache
		drawBackground_(&m_dcBkgnd);

		drawCoordinate_(&m_dcBkgnd);
	}

	// if we reset the wiper position, we need to copy entire background to canvas
	if ( m_bWiperReset || m_bBkgndInvalid )
	{
		m_dcMem.BitBlt(0, 0, m_width, m_height, &m_dcBkgnd, 0, 0, SRCCOPY);
	}
	
	// draw wave forms
	CRect rcUpdate = m_waveRect;	
	// SetRect(&rcUpdate, 0, 0, m_width, m_height);

	drawWaveForm_( rcUpdate );

	if ( m_bWiperReset )
	{
		rcUpdate = m_waveRect;
	}

	if ( m_bBkgndInvalid )
	{
		SetRect(&rcUpdate, 0, 0, m_width, m_height);
	}

	// TimeSnapshot::getInstance()->bmpEnd();
	
	// render memory dc to Screen dc
	// TimeSnapshot::getInstance()->bitbltStart();
	// paintDC.BitBlt(x, y, m_width, m_height, &m_dcMem, 0, 0, SRCCOPY);
	paintDC.BitBlt(x+rcUpdate.left, y+rcUpdate.top, rcUpdate.Width(), rcUpdate.Height(), &m_dcMem, rcUpdate.left, rcUpdate.top, SRCCOPY);
	// TimeSnapshot::getInstance()->bitbltEnd();

	m_bWiperReset = false;
	m_bBkgndInvalid = false;
}

void Plot::drawBackground_( CDC* pMemDC )
{
	
	CRect rcCanvas(0, 0, m_width, m_height);

	TRIVERTEX triVertex[2];

// 	// left top
// 	triVertex[0].x = rcCanvas.left;
// 	triVertex[0].y	= rcCanvas.top;
// 	triVertex[0].Red = 0x8000;
// 	triVertex[0].Green = 0x8000;
// 	triVertex[0].Blue = 0x8000;
// 	triVertex[0].Alpha = 0;
// 
// 	// right bottom
// 	triVertex[1].x = rcCanvas.right;
// 	triVertex[1].y = rcCanvas.bottom;
// 	triVertex[1].Red = 0;
// 	triVertex[1].Green = 0x0400;
// 	triVertex[1].Blue = 0x0800;
// 	triVertex[1].Alpha = 0;

	// left top
	triVertex[0].x = rcCanvas.left;
	triVertex[0].y	= rcCanvas.top;
	triVertex[0].Red = 0x0000;
	triVertex[0].Green = 0x0000;
	triVertex[0].Blue = 0x0000;
	triVertex[0].Alpha = 0;

	// right bottom
	triVertex[1].x = rcCanvas.right;
	triVertex[1].y = rcCanvas.bottom;
	triVertex[1].Red = 0;
	triVertex[1].Green = 0x0000;
	triVertex[1].Blue = 0x0000;
	triVertex[1].Alpha = 0;


	GRADIENT_RECT gradientRect;
	gradientRect.UpperLeft = 0;
	gradientRect.LowerRight = 1;

	GradientFill(pMemDC->GetSafeHdc(), triVertex, 2, &gradientRect, 1, GRADIENT_FILL_RECT_V);
}

void Plot::drawCoordinate_( CDC* pMemDC )
{

	CPoint xCordEnd, yCordEnd;
	xCordEnd.x = m_width - m_rightMargin;
	xCordEnd.y = m_Origin.y;
	yCordEnd.x = m_Origin.x;
	yCordEnd.y = m_topMargin;

	// prepare context
	CPen pen;
	CPen* pOldPen;
	pen.CreatePen(PS_SOLID, 2, RGB(255, 255, 255));
	pOldPen = pMemDC->SelectObject(&pen);

	// draw x,y coordinates
	pMemDC->MoveTo(xCordEnd);
	pMemDC->LineTo(m_Origin);
	pMemDC->LineTo(yCordEnd);
	
	// draw x coordinate mark
	int xLen = xCordEnd.x - m_Origin.x;
	int xScaling = xLen / m_timeLen;
	for (int i = 0; i <= m_timeLen; ++ i)
	{
		pMemDC->MoveTo(m_Origin.x + i * xScaling, m_Origin.y);
		pMemDC->LineTo(m_Origin.x + i * xScaling, m_Origin.y + m_markLength);
	}

	// draw y coordinate mark
	int yLen = m_Origin.y - yCordEnd.y;
	
	CPoint ptBase;
	ptBase.x = m_Origin.x;
	ASSERT(m_maxData - m_minData > 0);
	ptBase.y = m_Origin.y - static_cast<long>(((double)(m_baseData - m_minData) / (m_maxData - m_minData) ) * yLen);

	pMemDC->MoveTo(m_Origin);
	pMemDC->LineTo(m_Origin.x - m_markLength, m_Origin.y);

	pMemDC->MoveTo(ptBase);
	pMemDC->LineTo(ptBase.x - m_markLength, ptBase.y);

	pMemDC->MoveTo(yCordEnd);
	pMemDC->LineTo(yCordEnd.x - m_markLength, yCordEnd.y);

	// draw mark text
	CRect rcYMaxText, rcYMinText, rcYBaseText;
	getRectByPt_(m_Origin.x - 10, m_Origin.y, 100, 12, ExtLeft, rcYMinText);
	getRectByPt_(ptBase.x - 10, ptBase.y, 100, 12, ExtLeft, rcYBaseText);
	getRectByPt_(yCordEnd.x - 10, yCordEnd.y, 100, 12, ExtLeft, rcYMaxText);

	CRect destRect;
	if ( m_maxData - m_baseData > m_baseData - m_minData)
	{
		if ( destRect.IntersectRect(&rcYBaseText, &rcYMinText) )
		{
			rcYBaseText.OffsetRect(0, - destRect.Height() - 30);
			pMemDC->MoveTo(rcYBaseText.BottomRight());
			pMemDC->LineTo(ptBase.x - m_markLength, ptBase.y);
		}
	}
	else
	{
		if (destRect.IntersectRect(&rcYBaseText, &rcYMaxText) )
		{
			rcYBaseText.OffsetRect(0, destRect.Height() + 30 );
			pMemDC->MoveTo(rcYBaseText.right, rcYBaseText.top);
			pMemDC->LineTo(ptBase.x - m_markLength, ptBase.y);
		}
	}

	pMemDC->SetBkMode(TRANSPARENT);
	pMemDC->SetTextColor(RGB(255,255,255));

	CString strText;
	strText.Format(_T("%.2f"), m_maxData);
	pMemDC->DrawText(strText, &rcYMaxText, DT_RIGHT|DT_VCENTER);

	strText.Format(_T("%.2f"), m_baseData);
	pMemDC->DrawText(strText, &rcYBaseText, DT_RIGHT|DT_VCENTER);

	strText.Format(_T("%.2f"), m_minData);
	pMemDC->DrawText(strText, &rcYMinText, DT_RIGHT|DT_VCENTER);

	pMemDC->SelectObject(pOldPen);
	pen.DeleteObject();
}


void Plot::setY( double min, double base, double max )
{
	m_minData = min;
	m_baseData = base;
	m_maxData = max;
}

void Plot::getRectByPt_( const CPoint& ptBase, int width, int height, RectExtMethod extMethod, CRect& rectRtn)
{
	CPoint ptLeftTop = ptBase;

	switch(extMethod)
	{
	case ExtLeft:
		ptLeftTop.x = ptBase.x - width;
		ptLeftTop.y = ptBase.y - height / 2;
		break;
	case ExtDown:
		ptLeftTop.x = ptBase.x - width / 2;
		ptLeftTop.y = ptBase.y;
		break;
	default:
		break;
	}

	rectRtn.left = ptLeftTop.x;
	rectRtn.top = ptLeftTop.y;
	rectRtn.right = rectRtn.left + width;
	rectRtn.bottom = rectRtn.top + height;
}

void Plot::getRectByPt_( int x, int y, int width, int height, RectExtMethod extMethod, CRect& rectRtn )
{
	CPoint pt(x, y);
	getRectByPt_(pt, width, height, extMethod, rectRtn);
}

void Plot::drawWaveForm_( CRect& rcUpdate )
{
	int savedDC = m_dcMem.SaveDC();

	m_dcMem.SelectObject( GlobalObjects::getInstance()->getPen( Pen_OldCurve ));
	
	if ( m_bWiperReset && m_oldData.size() > 0)
	{
		// draw old data
#ifndef ANTIALIAS
		m_dcMem.MoveTo(m_Origin.x, getYPos_( *m_oldData.begin() ));
#endif
		CPoint startPt;
		startPt.x = m_Origin.x;
		startPt.y = getYPos_( *m_oldData.begin() );

		for ( UINT iOldDataIndex = 0; iOldDataIndex < m_oldData.size(); ++ iOldDataIndex)
		{
			int y = getYPos_( m_oldData[iOldDataIndex] );
			int x = getXPos_( iOldDataIndex );
#ifdef ANTIALIAS
			AntiAliasLine::Draw(&m_dcMem, startPt.x, startPt.y, x, y, RGB(0, 127, 0));
#else 
			m_dcMem.LineTo(x, y);
#endif

			startPt.x = x;
			startPt.y = y;
		}
	}
	
	
	m_dcMem.SelectObject( GlobalObjects::getInstance()->getPen( Pen_Curve ) );
	// we have at least 2 data to draw a line
	if ( m_data.size() >= 2 && m_data.size() > m_drawnCount )
	{
		int startDataIndex = m_drawnCount - 2;
		if (startDataIndex < 0) startDataIndex = 0;

		int repaintStartX = getXPos_(startDataIndex);
		int dataCountNeedDraw = m_data.size() - startDataIndex;
		int backgndWidth = static_cast<int>(dataCountNeedDraw * m_pixelPerData) + 1;
		
		m_dcMem.BitBlt(repaintStartX, 0, backgndWidth, m_height, &m_dcBkgnd, repaintStartX, 0, SRCCOPY);

		rcUpdate.left = static_cast<long>(repaintStartX);

		int firstY = getYPos_( m_data[startDataIndex] );

#ifndef ANTIALIAS
		m_dcMem.MoveTo(repaintStartX, firstY);
#endif

		CPoint startPt;
		startPt.x = repaintStartX;
		startPt.y = firstY;

		for ( UINT iDataIndex = startDataIndex + 1; iDataIndex < m_data.size(); ++ iDataIndex)
		{
			int y = getYPos_( m_data[iDataIndex] );
			int x = getXPos_( iDataIndex );


#ifdef ANTIALIAS
			AntiAliasLine::Draw(&m_dcMem, startPt.x, startPt.y, x, y, RGB(0, 255, 0));
#else
			m_dcMem.LineTo(x, y);
#endif

			startPt.x = x;
			startPt.y = y;
		}
		m_drawnCount = m_data.size();
	}
	
	//////////////////////////////////////////////////// draw wiper //////////////////////////////////////////////////////
	int x = getXPos_(m_data.size());
	m_dcMem.SelectObject( GlobalObjects::getInstance()->getPen( Pen_Wiper ) );
	m_dcMem.MoveTo(x, m_Origin.y - 1);
	m_dcMem.LineTo(x, m_topMargin);

	rcUpdate.right = x;

	// to calculate the real cycle time of the plot
	if (m_bWiperReset)
	{
		DWORD howLongPassed = ::GetTickCount() - m_dwWiperResetTime;

		m_strBuf.Format(_T("%d"), howLongPassed);
		
		m_dwWiperResetTime = GetTickCount();
	}

	m_dcMem.RestoreDC( savedDC );
}

void Plot::getWaveRect( CRect& rectWaveForm)
{
	rectWaveForm = m_waveRect;
}

int Plot::getMaxDataCount()
{
	return m_maxDataCount;
}

void Plot::resetWiper_()
{
	m_bWiperReset = true;
	m_drawnCount = 0;
}

int Plot::getYPos_( int data )
{
	double y = m_Origin.y - (double)(data - m_minData) / m_dataDelta * m_waveHeight;
	return static_cast<int>(y);
}

int Plot::getXPos_( int index )
{
	double x = m_Origin.x + (index) * m_pixelPerData;
	return static_cast<int>(x + 1);
}

void Plot::pushData( int data )
{
	if ( m_data.size() >= m_maxDataCount )
	{
		m_oldData.clear();
		std::copy(m_data.begin(), m_data.end(), std::back_inserter(m_oldData));
		m_data.clear();
		resetWiper_();
	}

	m_data.push_back( data );
}
