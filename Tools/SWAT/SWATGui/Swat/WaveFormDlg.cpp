// WaveFormDlg.cpp : implementation file
//

#include "stdafx.h"
#include "WaveFormDlg.h"
#include "IPatientDataSet.h"
#include "CLogger.h"

// CWaveFormDlg dialog

IMPLEMENT_DYNAMIC(CWaveFormDlg, CDialog)

CWaveFormDlg::CWaveFormDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CWaveFormDlg::IDD, pParent)
	, m_nFrequency(0)
	, m_nTriggerDeg(0)
	, m_nTriggerPhaseDeg(0)
{

}

CWaveFormDlg::~CWaveFormDlg()
{
}

void CWaveFormDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_CONTROL, m_List);
	DDX_Text(pDX, IDC_EDIT_FREQ, m_nFrequency);
	DDX_Text(pDX, IDC_EDIT_TRIGGER, m_nTriggerDeg);
	DDV_MinMaxInt(pDX, m_nTriggerDeg, 0, 360);
	DDX_Text(pDX, IDC_EDIT_PHASE, m_nTriggerPhaseDeg);
	DDV_MinMaxInt(pDX, m_nTriggerPhaseDeg, 0, 360);
}


BEGIN_MESSAGE_MAP(CWaveFormDlg, CDialog)

	ON_BN_CLICKED(IDOK, &CWaveFormDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_BUTTON_CLEAR, &CWaveFormDlg::OnBnClickedButtonClear)
	ON_BN_CLICKED(IDC_BUTTON_SAVE, &CWaveFormDlg::OnBnClickedButtonSave)
	ON_BN_CLICKED(IDC_BUTTON_OPEN, &CWaveFormDlg::OnBnClickedButtonOpen)
END_MESSAGE_MAP()


// CWaveFormDlg message handlers



BOOL CWaveFormDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	//initial style setting
	m_List.SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

	// Make the necessary initializations
	m_List.InsertColumn      (0, "Parameter", LVCFMT_LEFT, 150);
	m_List.SetReadOnlyColumns(0, true);

	m_List.InsertColumn      (1, "Min", LVCFMT_LEFT, 50);
	m_List.SetReadOnlyColumns(1, false);

	m_List.InsertColumn      (2, "Max", LVCFMT_LEFT, 50);
	m_List.SetReadOnlyColumns(2, false);

	m_List.InsertColumn      (3, "Phase(Deg)", LVCFMT_LEFT, 85);
	m_List.SetReadOnlyColumns(3, false);

	m_List.EnableVScroll(); 

	PopulateInitialData();
 

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}


BOOL CWaveFormDlg::PreTranslateMessage(MSG* pMsg)
{
	// Avoid closing the dialog box with enter key
	HWND hWnd;
	GetDlgItem(IDOK, &hWnd);
	
	if ((pMsg->message == WM_KEYDOWN) && (pMsg->wParam == VK_RETURN) && (pMsg->hwnd != hWnd))
	{
		return FALSE;
	}

	return CDialog::PreTranslateMessage(pMsg);
}

void CWaveFormDlg::PopulateInitialData()
{
	IPatientDataEl  *pEl;
	IPatientDataSet *pPd=getPatientDataSetInstance();    

	m_List.DeleteAllItems();

	for(int row_cnt=0;pEl=pPd->getWaveformElement(row_cnt);row_cnt++)
	{
		if(!pEl->isTrigger())
		{
			char *pName=pEl->getName();
			m_List.InsertItem(row_cnt, pName);      //Parameter name
		}
	}
}

void CWaveFormDlg::OnBnClickedOk()
{

	UpdateData(1);
	
	IPatientDataEl  *pEl;
	IPatientDataSet *pPd=getPatientDataSetInstance();     
	CString strNewValue;
	int nTotalRows = m_List.GetItemCount();
	
	//for each item in PD, check if it has been overwritten in the listbox
	for(int row_cnt=0;pEl=pPd->getWaveformElement(row_cnt);row_cnt++)
	{
		//set if it is NOT a trigger and has all three parameters filled in
		if(!pEl->isTrigger())
		{

			if(!m_List.GetItemText(row_cnt, 1).IsEmpty() &&
				!m_List.GetItemText(row_cnt, 2).IsEmpty() &&
				!m_List.GetItemText(row_cnt, 3).IsEmpty())
			{
				//setSinSignal    (double dFrequency,double dPhase,double dMin,double dMax)
				pEl->setSinSignal(	m_nFrequency, //double dFrequency
									atof(m_List.GetItemText(row_cnt, 3)), //double dPhase
									atof(m_List.GetItemText(row_cnt, 1)), //double dMin
									atof(m_List.GetItemText(row_cnt, 2))); //double dMax
			}
		} 
		else //in case of trigger element
		{
			//setTriggerSignal(double dFrequency,double dPhase,double dMin,double dMax, double trigger_high, double trigger_low) = 0;
			pEl->setTriggerSignal(	m_nFrequency, //double dFrequency
							m_nTriggerPhaseDeg, //double dPhase
							atof(m_List.GetItemText(row_cnt, 1)), //double dMin
							atof(m_List.GetItemText(row_cnt, 2)), //double dMax
							m_nTriggerDeg, //double trigger_high
							100); //double trigger_low
		}
	}
}



void CWaveFormDlg::OnBnClickedButtonClear()
{
	IPatientDataEl  *pEl;
	IPatientDataSet *pPd=getPatientDataSetInstance();     

	for(int row_cnt=0;pEl=pPd->getWaveformElement(row_cnt);row_cnt++)
	{
		m_List.SetItemText(row_cnt, 1, "");
		m_List.SetItemText(row_cnt, 2, "");
		m_List.SetItemText(row_cnt, 3, "");
		
		pEl->unsetValue();
	}

	GetDlgItem(IDC_EDIT_TRIGGER)->SetWindowTextA("0");
	GetDlgItem(IDC_EDIT_PHASE)->SetWindowTextA("0");
	GetDlgItem(IDC_EDIT_FREQ)->SetWindowTextA("0");

	PopulateInitialData();
}
void CWaveFormDlg::OnBnClickedButtonSave()
{
	CFileDialog dlg(FALSE, "wfs", "*.wfs",0, "WaveForm Setting File (wfs)|*.wfs|", NULL);
	if(dlg.DoModal()==IDCANCEL)
		return;

	UpdateData(1);

	CStdioFile fileOut;
	try
	{
		fileOut.Open( dlg.GetPathName(),
					CFile::modeCreate | CFile::modeWrite | CFile::typeText );
	}
	catch( CFileException e )
	{
		LOG_MESSAGE(LM_ERROR,"CWaveFormDlg::OnBnClickedButtonSave() File could not be opened to write ERROR:%d", e.m_cause );
	}
	catch(...)
	{
		LOG_MESSAGE(LM_ERROR,"CWaveFormDlg::OnBnClickedButtonSave() File could not be opened to write" );
	}

	try
	{
		fileOut.WriteString("Waveform setting file \r\n"); //line 1

		CString str;
		int nTotalRows = m_List.GetItemCount();
		for(int n = 0; n<nTotalRows; n++) // total 6 lines
		{
			str = m_List.GetItemText(n, 0) + " : " +
				m_List.GetItemText(n, 1) + "  :" +
				m_List.GetItemText(n, 2) + " : " +
				m_List.GetItemText(n, 3) + " \r\n" ;

			fileOut.WriteString(str);
		}

		fileOut.WriteString("Trigger Setting: \r\n");

		GetDlgItem(IDC_EDIT_TRIGGER)->GetWindowTextA(str);
		fileOut.WriteString("Trigger:" + str + "\r\n");

		GetDlgItem(IDC_EDIT_PHASE)->GetWindowTextA(str);
		fileOut.WriteString("TriggerPhase:" + str + "\r\n");

		GetDlgItem(IDC_EDIT_FREQ)->GetWindowTextA(str);
		fileOut.WriteString("TriggerFrequency:" + str + "\r\n");
	

		fileOut.Close();
	}
	catch(CFileException e )
	{
	}
	catch(...)
	{
	}

}

void CWaveFormDlg::OnBnClickedButtonOpen()
{

	CFileDialog dlg(TRUE, NULL,NULL,0, "WaveForm Setting File (wfs)|*.wfs|", NULL);
	if(dlg.DoModal()==IDCANCEL)
		return;

	CStdioFile fileIn;
	CString str;

	try
	{
		fileIn.Open( dlg.GetPathName(),
					CFile::modeRead | CFile::typeText );
	}
	catch( CFileException e )
	{
		LOG_MESSAGE(LM_ERROR,"CWaveFormDlg::OnBnClickedButtonOpen() File could not be opened to read ERROR:%d", e.m_cause );
	}
	catch(...)
	{
		LOG_MESSAGE(LM_ERROR,"CWaveFormDlg::OnBnClickedButtonOpen() File could not be opened to read" );
	}

	try
	{
		fileIn.ReadString(str); //line 1 :  Waveform setting file
	
		for(int nCount = 0 ; nCount <6; nCount++) //All six values
		{
			fileIn.ReadString(str);
			CString strParam, strMin, strMax, strPhase;
			int n = str.Find(":", 0);
			strParam = str.Left(n);

			str = str.Right(str.GetLength() - n - 1);
			n = str.Find(":", 0);
			strMin = str.Left(n);
	
			str = str.Right(str.GetLength() - n - 1);
			n = str.Find(":", 0);
			strMax = str.Left(n);

			str = str.Right(str.GetLength() - n - 1);
			strPhase = str.TrimRight();
			
			strMin.TrimLeft();
			strMin.TrimRight();
			strMax.TrimLeft();
			strMax.TrimRight();
			strPhase.TrimLeft();
			strPhase.TrimRight();

			m_List.SetItemText(nCount, 1, strMin);
			m_List.SetItemText(nCount, 2, strMax);
			m_List.SetItemText(nCount, 3, strPhase);

		}

		fileIn.ReadString(str); // Trigger Setting: line

		fileIn.ReadString(str);
		int n = str.ReverseFind(':');
		str = str.Right(str.GetLength()-n-1);
		str.TrimRight();
		GetDlgItem(IDC_EDIT_TRIGGER)->SetWindowTextA(str);

		fileIn.ReadString(str);
		n = str.ReverseFind(':');
		str = str.Right(str.GetLength()-n-1);
		str.TrimRight();
		GetDlgItem(IDC_EDIT_PHASE)->SetWindowTextA(str);


		fileIn.ReadString(str);
		n = str.ReverseFind(':');
		str = str.Right(str.GetLength()-n-1);
		str.TrimRight();
		GetDlgItem(IDC_EDIT_FREQ)->SetWindowTextA(str);
		
		fileIn.Close();

		}

	catch( CFileException e )
	{
		LOG_MESSAGE(LM_ERROR,"CWaveFormDlg::OnBnClickedButtonOpen() File could not be opened to read ERROR:%d", e.m_cause );
	}
	catch(...)
	{
		LOG_MESSAGE(LM_ERROR,"CWaveFormDlg::OnBnClickedButtonOpen() File could not be opened to read" );
	}






}
