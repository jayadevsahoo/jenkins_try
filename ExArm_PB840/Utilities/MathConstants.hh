
#ifndef MathConstants_HH
#define MathConstants_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
// Class:  MathConstants - Mathematical Constants.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Utilities/vcssrc/MathConstants.hhv   25.0.4.0   19 Nov 2013 14:35:14   pvcs  $
//
//@ Modifications
//
//   Revision: 002   By: sah   Date: 06-Feb-1996   DCS Number: 674
//   Project:  Sigma (R8027)
//   Description:
//	Removed 'ifndef' and 'endif' from include line.
//  
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//=====================================================================

#include "Sigma.hh"

//@ Usage-Classes
//@ End-Usage

#ifdef PI 
#  undef PI
#endif


struct MathConstants
{
  public:
    static const Real32  PI;
    static const Real32  E;
};


#endif  // MathConstants_HH
