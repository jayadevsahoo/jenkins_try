
#ifndef SIterR_AlarmOperand_HH
#define SIterR_AlarmOperand_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: SIterR_AlarmOperand - Iterator for 'SListR_AlarmOperand'.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Foundation/vcssrc/SIteratorR.H_v   25.0.4.0   19 Nov 2013 14:03:50   pvcs  $
//
//@ Modification-Log
//
//   Revision: 003   By: sah   Date: 07-Jan-1999   DR Number: 5321
//   Project:  ATC
//   Description:
//	Changed 'SoftFault()' method to an non-inlined method.
//  
//   Revision: 002   By: sah   Date: 06-Feb-1996   DCS Number: 674
//   Project:  Sigma (R8027)
//   Description:
//	Removed 'ifndef' and 'endif' from around includes.
//
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//====================================================================

//@ Usage-Classes
#include "SListR_AlarmOperand.hh"
//@ End-Usage


class SIterR_AlarmOperand
{
#if defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)
    friend Ostream&  operator<<(Ostream& ostr,
                                const SIterR_AlarmOperand& sIter);
#endif  // defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)

  public:
    inline SIterR_AlarmOperand(const SIterR_AlarmOperand& sIter);
    inline SIterR_AlarmOperand(const SListR_AlarmOperand& sList);
    inline ~SIterR_AlarmOperand(void);

    inline Boolean  isEmpty(void) const;
    inline Boolean  isFull (void) const;

    inline Boolean  isAtFirst(void) const;
    inline Boolean  isAtLast (void) const;

    inline Uint32  getNumItems(void) const;
    inline Uint32  getMaxItems(void) const;

    inline SigmaStatus  goFirst(void);
    inline SigmaStatus  goNext (void);

    SigmaStatus  search(AlarmOperand&                 item,
			const SearchFrom      startFrom = FROM_FIRST,
			const EquivResolution foundWhen = EQUIV_OBJECTS);

    inline const AlarmOperand&  currentItem(void) const;

    inline void  attachTo(const SIterR_AlarmOperand& sIter);
    inline void  attachTo(const SListR_AlarmOperand& sList);

    inline Boolean isEquivTo(const SIterR_AlarmOperand& sIter,
			     const EquivResolution   equivWhen) const;
    inline Boolean isEquivTo(const SListR_AlarmOperand& sList,
			     const EquivResolution     equivWhen) const;

    inline Boolean  operator==(const SIterR_AlarmOperand& sIter) const;
    inline Boolean  operator!=(const SIterR_AlarmOperand& sIter) const;

    inline Boolean  operator==(const SListR_AlarmOperand& sList) const;
    inline Boolean  operator!=(const SListR_AlarmOperand& sList) const;

    static void  SoftFault(const SoftFaultID softFaultID, 
			   const Uint32      lineNumber, 
			   const char*       pFileName = NULL, 
			   const char*       pBoolTest = NULL); 

  protected:
    inline const SNodeC_MemPtr*  getFirst_  (void) const;
    inline const SNodeC_MemPtr*  getCurrent_(void) const;

    inline Boolean  isLegalNode_(const SNodeC_MemPtr* pNode) const;

    inline void  setCurrent_(SNodeC_MemPtr* pNewCurr);

  private:
    SIterR_AlarmOperand(void);  	    		// not implemented...
    void  operator=(const SIterR_AlarmOperand&);	// not implemented...

    //@ Data-Member:  pIterList_
    // A pointer to this iterator's constant list.
    const SListR_AlarmOperand*  pIterList_;

    //@ Data-Member:  pIterCurrent_
    // A pointer to this iterator's current node.
    const SNodeC_MemPtr*  pIterCurrent_;
};


// Inlined methods...
#include "SIterR_AlarmOperand.in"


#endif  // SIterR_AlarmOperand_HH
