#ifndef FlowSensorCalibrationSubScreen_HH
#define FlowSensorCalibrationSubScreen_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: FlowSensorCalibrationSubScreen - Activated by selecting Flow Sensor
// Calibration button on the the Service lower subScreen.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/FlowSensorCalibrationSubScreen.hhv   25.0.4.0   19 Nov 2013 14:07:48   pvcs  $
//
//
//@ Modification-Log
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  yyy    Date:  29-AUG-19974    DR Number:   2279
//       Project:  Sigma (R8027)
//       Description:
//             Initial coding.
//====================================================================

#include "SubScreen.hh"
#include "GuiTimerTarget.hh"
#include "AdjustPanelTarget.hh"
#include "GuiTestManagerTarget.hh"

//@ Usage-Classes
#include "GuiTestManager.hh"
#include "ServiceStatusArea.hh"
#include "SmPromptId.hh"
#include "SubScreenTitleArea.hh"
#include "TextButton.hh"
#include "GuiAppClassIds.hh"
class TestResult;
//@ End-Usage

class FlowSensorCalibrationSubScreen : public SubScreen, public GuiTimerTarget,
							public AdjustPanelTarget,
							public GuiTestManagerTarget 
{
public:
	FlowSensorCalibrationSubScreen(SubScreenArea *pSubScreenArea);
	~FlowSensorCalibrationSubScreen(void);

	// Redefine SubScreen activation/deactivation methods
	virtual void activate(void);
	virtual void deactivate(void);

	// ButtonTarget virtual method
	virtual void buttonDownHappened(Button *pButton,
												Boolean byOperatorAction);
	virtual void buttonUpHappened(Button *pButton,
												Boolean byOperatorAction);

	// AdjustPanelTarget virtual methods
	virtual void adjustPanelAcceptPressHappened(void);
	virtual void adjustPanelRestoreFocusHappened(void);

	// GuiTestMangerTarget virtual methods
	virtual void processTestPrompt(Int command);
	virtual void processTestKeyAllowed(Int keyAllowed);
	virtual void processTestResultStatus(Int resultStatus, Int testResultId=-1);
	virtual void processTestResultCondition(Int resultCondition);
	virtual void processTestData(Int dataIndex, TestResult *pResult);



	static void SoftFault(const SoftFaultID softFaultID,
								 const Uint32      lineNumber,
								 const char*       pFileName  = NULL, 
								 const char*       pPredicate = NULL);

protected:

private:
	enum CalConditionId
	{
		MAX_CONDITION_ID = 7 
	};

	enum CalScreenId
	{
		CALIBRATION_END,
		CALIBRATION_FAILED,
		CALIBRATION_PROMPT_ACCEPTED_CLEARED,
		CALIBRATION_SETUP,
		CALIBRATION_START_ACCEPTED,
		MAX_CAL_SCREENS
	};

	enum CalPromptId
	{
		CAL_CONNECT_AC_PROMPT,
		CAL_ATTACH_CIRCUIT_PROMPT,
		CAL_CONNECT_AIR_PROMPT,
		CAL_CONNECT_O2_PROMPT,
		MAX_CAL_PROMPTS
	};

	enum TestStateId
	{
		PROMPT_ACCEPT_BUTTON_PRESSED,
		START_BUTTON_PRESSED,
		UNDEFINED_TEST_STATE,
		MAX_TEST_STATES
	};

	// these methods are purposely declared, but not implemented...
	FlowSensorCalibrationSubScreen(void);						// not implemented...
	FlowSensorCalibrationSubScreen(const FlowSensorCalibrationSubScreen&);	// not implemented...
	void operator=(const FlowSensorCalibrationSubScreen&);		// not implemented...

	void layoutScreen_(CalScreenId calScreenId);
	
	void setPromptTable_(void);
	void setErrorTable_(void);
	void startTest_(SmTestId::ServiceModeTestId currentTestId);
	void nextTest_(void);
	void setupForRestartCalTest_(void);

	//@ Data-Member: calStatus_
	// A container for displaying the current Flow Sensor Calibration status.
	ServiceStatusArea calStatus_;

	//@ Data-Member: calWarningMsg_
	// A TextField objects used to display calibration warning message.
	// lower screen
	TextField calWarningMsg_;

	//@ Data-Member: errDisplayArea_
	// An array of TextField objects used to display test conditions in the
	// lower screen
	TextField errDisplayArea_[MAX_CONDITION_ID];
	
	//@ Data-Member: conditionText_
	// Text for error condition prompts
	StringId conditionText_[MAX_CONDITION_ID];

	//@ Data-Member: errorHappened_
	// Flag that indicates status of test
	Int	errorHappened_;
	
	//@ Data-Member: errorIndex_
	// Keep tracks of index into TextFields in the error display area
	Int	errorIndex_;
	
	//@ Data-Member: keyAllowedId_
	Int keyAllowedId_;

	//@ Data-Member: userKeyPressedId_
	// Hold the operator pressed key Id.
	SmPromptId::ActionId userKeyPressedId_;

	//@ Data-Member: promptId_
	// The prompt id.
	SmPromptId::PromptId promptId_[MAX_CAL_PROMPTS];
	
	//@ Data-Member: promptName_
	// The prompts.
	StringId promptName_[MAX_CAL_PROMPTS];

	//@ Data-Member: startTestButton_
	// The StartTest button is used to start the Flow Sensor Calibration testing.
	TextButton startTestButton_;
	
	//@ Data-Member: testStateId_
	// The test status identifier.
	TestStateId testStateId_;
	
	//@ Data-Member: titleArea_
	// The sub-screen's title at the top left of the screen.
	SubScreenTitleArea titleArea_;

	//@ Data-Member: guiTestMgr_ 
	// Object instance of GuiTestManager
	GuiTestManager guiTestMgr_;

	//@ Data-Member: currentTestId_
	// Object instance of GuiTestManager
	SmTestId::ServiceModeTestId currentTestId_;
	
};

#endif // FlowSensorCalibrationSubScreen_HH 
