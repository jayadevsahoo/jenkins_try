#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: IbwSettingButton - A setting button used for modifying
// the Ideal Body Weight setting.  Needs to display the setting's value
// twice both in kilos and in pounds.
//---------------------------------------------------------------------
//@ Interface-Description
// The IBW Setting button consists of a title (IBW) along with two displays of
// the IBW setting value, one in kilos (Kg) and one in pounds (lb).  The pound
// value is displayed with surrounding parentheses (the IBW setting is actually
// stored as kilos).  If the IBW value has not been modified by the operator
// then the two values are displayed in normal text, otherwise display is in
// italic text.
//
// Once created, the IBW Setting Button operates without intervention except
// for needing an activate() call before being displayed (to sync its display
// with the current IBW setting value).
//
// Only one interface method is defined, updateDisplay_(), which is called
// (via SettingButton) when the button must update its displayed setting value.
//---------------------------------------------------------------------
//@ Rationale
// This class is really a specialization of NumericSettingButton but it
// is needed because two, rather than the normal one, floating point
// values are displayed in the button.
//---------------------------------------------------------------------
//@ Implementation-Description
// This setting button takes a lot less construction parameters than other
// setting button classes.  This is because this button's application is
// so specialized that many of its characteristics are fixed, and not
// programmable such as the general purpose NumericSettingButton class.
//
// After construction, most of the operation of this setting button is handled
// by the inherited SettingButton class.  All IbwSettingButton really needs to
// do is to react to changes of the IBW setting which it controls and update
// the numeric value displays accordingly.  The setting is held in kilos so a
// simple conversion gives the corresponding value in pounds.  Because this
// class registers as a target of the IBW setting, the updateDisplay_() method
// will be called by SettingButton::updateValue() when the setting
// changes.  This method handles the updating of the numeric value.
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and pre-conditions
// to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/IbwSettingButton.ccv   25.0.4.0   19 Nov 2013 14:07:56   pvcs  $
//
//@ Modification-Log
//
//  Revision: 012   By: gdc    Date: 11-Aug-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support changing IBW while ventilating.
//
//  Revision: 011   By: sah    Date: 25-Oct-2000     DCS Number:  5782
//  Project:  VTPC
//  Description:
//      Removed adding of '0.5' value to 'poundValue', because 'sprintf()'
//      now rounds, rather than just truncating.
//
//  Revision: 010  By: sah    Date: 16-Jul-1999   DCS Number:  5327
//  Project:  NeoMode
//  Description:
//      Increased precision of pounds (lbs.) value, when less than 10 lbs.
//
//  Revision: 009  By:  hhd	   Date:  24-May-1999    DR Number: 5369 
//    Project:  ATC
//    Description:
//		Removed references to Button::setButtonType() (empty) method.
//
//  Revision: 008  By:  sah	   Date:  23-Feb-1999    DCS Number:  5314
//  Project:  ATC
//  Description:
//      Added integrated with new dynamic, setting applicability functionality.
//      This allows for looser coupling between settings and GUI, and the
//      need for significantly less "setting knowledge" in the GUI subsystem.
//
//  Revision: 007  By:  sah	   Date:  11-Jan-1999    DCS Number: 5321
//  Project:  ATC
//  Description:
//		Added 'SIGMA_DEVELOPMENT' conditionals around this class (unused)
//		'SoftFault()' method.
//
//  Revision: 005  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 004  By:  sah	   Date:  04-Feb-98    DR Number: 2737
//  Project:  Sigma (R8027)
//  Description:
//		Corrected pounds-per-kilogram factor to 2.2046.
//
//  Revision: 003  By:  hhd	   Date:  06-Oct-97    DR Number: 2540 
//    Project:  Sigma (R8027)
//    Description:
//		Removed code that supports commas in floating point format.
//
//  Revision: 002  By:  hhd	   Date:  18-SEP-97		DR Number: 2512
//       Project:  Sigma (R8027)
//       Description:
//             Fixed repaint problem of the IBW's Numeric Field values displayed
//			 	in foreign languages which use the commas (',') format.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "IbwSettingButton.hh"

//@ Usage-Classes
#include "MiscStrs.hh"
#include <math.h>
#include "SettingSubject.hh"
//@ End-Usage

//@ Code...

// Initialize static constants.
static const Int32 BUTTON_WIDTH_ = 140;
static const Int32 BUTTON_HEIGHT_ = 46;
static const Int32 BUTTON_BORDER_ = 3;
static const Int32 POUND_VALUE_X_ = 107;
static const Int32 POUND_VALUE_Y_ = 1;
static const Int32 VALUE_WIDTH_ = 46;
static const Int32 VALUE_HEIGHT_ = 18; 
static const Int32 KILO_VALUE_X_ = 60;
static const Int32 KILO_VALUE_Y_ = 17;
static const Real32 POUNDS_PER_KILO_ = 2.2046;

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: IbwSettingButton()  [Constructor]
//
//@ Interface-Description
// Creates an IBW Setting Button. Passed the following arguments:
// >Von
//	xOrg			The X coordinate of the button.
//	yOrg			The Y coordinate of the button.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
// Passes parameters along to SettingButton and then sets up the
// drawables used to display the two weight values.  The kilo value
// can be displayed with a NumericField but the pound value is displayed
// using a TextField since this value is displayed with surrounding
// parentheses.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

IbwSettingButton::IbwSettingButton(Uint16 xOrg, Uint16 yOrg) :
SettingButton(  xOrg, yOrg, BUTTON_WIDTH_, BUTTON_HEIGHT_,
				Button::LIGHT, BUTTON_BORDER_, Button::ROUND,
				Button::NO_BORDER, MiscStrs::NEW_IBW_BUTTON_TITLE,
				SettingId::IBW, NULL,
				MiscStrs::IBW_HELP_MESSAGE),
kiloValue_(     TextFont::EIGHTEEN, 0, CENTER),
unitsText_(     MiscStrs::KG_UNITS_SMALL)
// Set number of digits to 0 above as we will
// be sizing the numeric field ourselves rather
// than letting GUI-Foundation setting it to
// a default value.
{
	CALL_TRACE("IbwSettingButton::IbwSettingButton(Uint16 xOrg, Uint16 yOrg)");

	setShowViewable(TRUE);

	// Position and size the two numeric value fields
	kiloValue_.setY(KILO_VALUE_Y_);
	kiloValue_.setWidth(VALUE_WIDTH_);
	kiloValue_.setHeight(VALUE_HEIGHT_);
	kiloValue_.setX(KILO_VALUE_X_ - kiloValue_.getWidth() / 2);

	poundText_.setX(POUND_VALUE_X_);
	poundText_.setY(POUND_VALUE_Y_);

	// Add the two fields to the label container of this button
	addLabel(&kiloValue_);
	addLabel(&unitsText_);
	addLabel(&poundText_);								// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~IbwSettingButton  [Destructor]
//
//@ Interface-Description
// Destroys an IbwSettingButton.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

IbwSettingButton::~IbwSettingButton(void)
{
	CALL_TRACE("IbwSettingButton::~IbwSettingButton(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateDisplay_ [Protected, virtual]
//
//@ Interface-Description
// Called when the display of this setting button needs to be updated with
// the latest setting value.  Passed a setting context id informing us the
// setting context from which we should retrieve the new setting value.
//---------------------------------------------------------------------
//@ Implementation-Description
// Retrieve IBW kilo value from Settings-Validation subsystem and display it.
// Convert the kilo value to pounds and display that also.
//---------------------------------------------------------------------
//@ PreCondition
// qualifierId == Notification::ADJUSTED
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
 
void
IbwSettingButton::updateDisplay_(Notification::ChangeQualifier qualifierId,
								 const SettingSubject*         pSubject)
{
	CALL_TRACE("updateDisplay_(qualifierId, pSubject)");

	AUX_CLASS_PRE_CONDITION((qualifierId == Notification::ADJUSTED),
							qualifierId);

	// Get the current IBW value (it's in kilos) setting and set
	// the kilo value display accordingly
	BoundedValue settingValue = pSubject->getAdjustedValue();

	kiloValue_.setValue(settingValue.value);
	kiloValue_.setPrecision(settingValue.precision);
	wchar_t formatChar;

	if ( pSubject->isChanged() )
	{
		kiloValue_.setItalic(TRUE);
		kiloValue_.setHighlighted(TRUE);
		poundText_.setHighlighted(TRUE);
		formatChar = L'I';
	}
	else
	{
		kiloValue_.setItalic(FALSE);
		kiloValue_.setHighlighted(FALSE);
		poundText_.setHighlighted(FALSE);
		formatChar = L'N';
	}

	Real32  poundValue = (settingValue.value * POUNDS_PER_KILO_);
	wchar_t poundString[32];

	if ( (poundValue + 0.5f) >= 10.0f )
	{
		swprintf(poundString, L"{p=10,y=10,%c:%.0f {T:lbs}}", formatChar, poundValue);
	}
	else
	{
		swprintf(poundString, L"{p=10,y=10,%c:%.1f {T:lbs}}", formatChar, poundValue);
	}

	// Display the pound value and center it
	poundText_.setText(poundString);
	poundText_.setX(POUND_VALUE_X_ - poundText_.getWidth() / 2);
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
IbwSettingButton::SoftFault(const SoftFaultID  softFaultID,
							const Uint32       lineNumber,
							const char*        pFileName,
							const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, IBWSETTINGBUTTON,
							lineNumber, pFileName, pPredicate);
}
