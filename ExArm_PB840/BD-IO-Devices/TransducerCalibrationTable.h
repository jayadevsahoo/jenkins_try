#ifndef TRANSDUCER_CALIBRATION_H

#define TRANSDUCER_CALIBRATION_H

#include "CalibrationTable.h"

enum TransducerCalStage_t
{
	CAL_TRANSDUCER_IDLE,
	CAL_TRANSDUCER_ZERO,
	CAL_TRANSDUCER_SPAN
};

#define Cal_t	uint16_t

class TransducerCalibrationTable_t : public CalibrationTable_t
{	
public:	
	TransducerCalibrationTable_t(CalibrationID_t CalID, Cal_t Zero, Cal_t Span);
	CalibrationStatus_t Calibrate(A2dWithStatus_t A2dData);
	Cal_t GetZeroCounts();
	Cal_t GetSpanCounts();
	Cal_t GetCountsPerUnit();
	float GetSlope() { return Slope; }
	float GetOffset() { return Offset; }


private:
	bool DoCalibration(A2dWithStatus_t A2dData);
	bool RetrieveCalibrationTable();
	bool SaveCalibrationTable();

	Cal_t					ZeroSetValue;
	Cal_t					SpanSetValue;
	Cal_t					ZeroCalValue;
	Cal_t					SpanCalValue;
	uint32_t				A2dDataSum;
	size_t					NumReadingsTaken;
	TransducerCalStage_t	CalStage;
	float     Slope;
    float     Offset;

};

#endif