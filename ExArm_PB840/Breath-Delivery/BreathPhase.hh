#ifndef BreathPhase_HH
#define BreathPhase_HH
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: BreathPhase - the base class for all possible breath phases.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/BreathPhase.hhv   25.0.4.0   19 Nov 2013 13:59:40   pvcs  $
//
//@ Modification-Log
//
//  Revision: 009   By:   rhj    Date: 05-March-2007      DR Number: 6359
//  Project:  RESPM
//  Description:
//        Resolve merge conflict for RM to Baseline 
//
//  Revision: 008  By: yyy     Date:  5-May-1999    DR Number: 5322
//  Project:  ATC
//  Description:
//      Added NewBreath() to update the tube information whenever a
//		newBreath occures.
//
//  Revision: 007  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 006  By: syw   Date:  12-Aug-1997    DR Number: 2356
//  	Project:  Sigma (840)
//		Description:
//			Added AdiabaticFactor_ data member.
//
//  Revision: 005  By: syw   Date: 16-Aug-1996   DR Number: DCS 1076
//  	Project:  Sigma (R8027)
//		Description:
//			Added updateFlowControllerFlags_() method.
//
//  Revision: 004 By: iv     Date: 24-Apr-1996   DR Number: DCS 600
//  	Project:  Sigma (R8027)
//		Description:
//			Added a method: inline static BreathTrigger* GetBreathTrigger( void) ; 
//
//  Revision: 003 By: syw    Date: 22-Feb-1996   DR Number: DCS 600
//  	Project:  Sigma (R8027)
//		Description:
//			Removed UpdateMixStatus() and setActiveControllers() methods
//			pertaining to Controls Rev 04 changes.  Functionality of both
//			methods are handled by the flow controller.
//
//  Revision: 002 By: syw    Date: 21-Feb-1996   DR Number: DCS 674
//  	Project:  Sigma (R8027)
//		Description:
//			Removed #ifndef around #include.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"
#include "Trigger.hh"

//@ Usage-Classes

#include "BreathPhaseType.hh"
#include "O2Mixture.hh"

class BreathTrigger ;

//@ End-Usage


class BreathPhase
{
  public:

    BreathPhase( const BreathPhaseType::PhaseType phase) ;
    virtual ~BreathPhase( void) ;

    static void SoftFault(const SoftFaultID softFaultID,
		   const Uint32      lineNumber,
		   const char*       pFileName  = NULL, 
		   const char*       pPredicate = NULL) ;
 
    static void NewBreath(void);
    inline static BreathPhase* GetCurrentBreathPhase( void) ; 			
    inline static const BreathTrigger* GetBreathTrigger( void) ; 
    inline static void SetCurrentBreathPhase( BreathPhase& phase,
                                       const BreathTrigger& rBreathTrigger) ;
    static void EnablePatientTriggers( const BreathPhaseType::PhaseType phase) ;

    virtual void newBreath(void) = 0;
    virtual void relinquishControl( const BreathTrigger& trigger) ;
    virtual void newCycle( void) = 0 ;
    inline Int32 getElapsedTimeMs( void) const ;
	inline BreathPhaseType::PhaseType getPhaseType( void) const ;
    virtual void setMixErrorSum( const Real32 mixErrorSum) ;
    virtual Real32 getMixErrorSum( void) const ;
	virtual void phaseInSettings( void) ;
    static inline Real32 GetAdiabaticFactor( void) ;

	virtual void  activate  (const BreathPhase* pPrevSpontPhase,
							 const BreathPhase* pLostSpontPhase);
	virtual void  deactivate(const Trigger::TriggerId triggerId);

#if defined(SIGMA_UNIT_TEST) || defined(SIGMA_DEVELOPMENT)
	char* getId( void) const ;
#endif // defined(SIGMA_UNIT_TEST) || defined(SIGMA_DEVELOPMENT)

  protected:

	virtual void updateFlowControllerFlags_( void) ;

    //@ Data-Member: Int32 elapsedTimeMs_
    // elapsed time of the active breath phase
    Int32 elapsedTimeMs_; 

    //@ Data-Member: BreathTrigger* PBreathTrigger_
    // pointer to the trigger that triggered the breath
    static const BreathTrigger* PBreathTrigger_;

	//@ Data-Member: Real32 AdiabaticFactor_
	// adiabatic factor used for compliance compensation
	static Real32 AdiabaticFactor_ ;

    //@ Data-Member:PhaseType phaseType_
    // The type of the phase being delivered
    BreathPhaseType::PhaseType phaseType_;

    //@ Data-Member: mixErrorSum_
    // sum of the past breath's mix errors
    Real32 mixErrorSum_ ;

  private:

    BreathPhase( void) ;					// not implemented
    BreathPhase( const BreathPhase&) ;    	// not implemented
    void operator=( const BreathPhase&) ;	// not implemented
    
    //@ Data-Member: PCurrentPhase_
    // stores the breath phase that is currently active
    static BreathPhase* PCurrentPhase_ ;
} ;

// Inlined methods
#include "BreathPhase.in"

#endif // BreathPhase_HH 
