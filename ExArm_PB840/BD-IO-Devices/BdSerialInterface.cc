#include "stdafx.h"
//====================================================================
// This is a proprietary work to which the Covidien corporation claims 
// exclusive right.  No part of this work may be used, disclosed, 
// reproduced, sorted in an information retrieval system, or 
// transmitted by any means, electronic, mechanical, photocopying, 
// recording, or otherwise without the prior written permission of 
// Covidien Corporation.
//
//              Copyright (c) 2008, Covidien Corporation
//=====================================================================


// =========================== C L A S S   D E S C R I P T I O N ====
//@ Class: BdSerialInterface - Serial Port Interface for the BD board
//---------------------------------------------------------------------
//@ Interface-Description
//  This class is the base class for the serial port driver
//  classes. It provides functions and data common to all the types of 
//  protocols. It also provides the entry point for the GUI-Serial-
//  Interface tasks and static methods for accessing the instantiated
//  protocol handlers.
//  
//  The class provides the interface to the GUI serial ports for the 
//  840 application subsystems. The BdSerialInterface::Task's are started
//  during system initialization. It task waits until the ventilator is 
//  transitioned to a normal (ventilating) state or service state to 
//  instantiate either the normal mode interfaces or the SmcpInterface for 
//  processing data to and from the serial port. The DciInterface 
//  provides the online Digital Communications Interface (DCI) whereas
//  the SmcpInterface provides the Service Mode Communications Protocol
//  interface. The PrinterInterface provides support to capture and print 
//  via the HP PCL printer language, the patient data, settings and waveforms.
//  External subsystems use the BdSerialInterface class static methods
//  to access the normal mode or SmcpInterface protocol handlers. 
//  The BdSerialInterface class provides read and write access to the serial
//  port protocol handlers as well as facilities to set the serial
//  port parameters such as baud rate. Once the protocol handler is
//  instantiated, the BdSerialInterface passes these requests along to
//  the instantiated handler. This mechanism provides a convenient and
//  safe interface for the application to the protocol handlers. The
//  details of the buffering mechanisms and protocols handling are hidden
//  from the external subsystems. In addition to the read and write
//  access to the serial port, the BdSerialInterface class provides
//  methods for enabling or disabling serial port loopback while in
//  Service-Mode.
//---------------------------------------------------------------------
//@ Rationale
//  Encapsulate the Sigma serial port access.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
// none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
//@(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/BdSerialInterface.ccv   25.0.4.0   19 Nov 2013 13:53:58   pvcs  $  
//
//=====================================================================
//@ Modification-Log
//
//  Revision: 002  By:  rhj    Date:  16-Sept-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//	   Removed rSerialPort_.isReceiveError()     
// 
//  Revision: 001  By:  gdc    Date:  25-APR-2008    SCR Number: 6436
//  Project:  PROX
//  Description:
//	   Initial version.
//=====================================================================


#include "TimeStamp.hh"

#include "BdSerialInterface.hh"
#include "Task.hh"

//@ Usage-Classes
#include "FaultHandler.hh"
#include "TaskControlAgent.hh"
#include "MsgQueue.hh"
#include "ProxiInterface.hh"
#include "ExhCommunicationInterface.h"
#include "BdSerialPort.hh"
#include "OsUtil.hh"

#if defined (_WIN32_WCE)
#include <winbase.h>
#include <tchar.h>
#endif
//@ End-Usage

//@ Code...

BdSerialInterface* BdSerialInterface::PBdSerialInterface = NULL;

static BdSerialInterface* 
	PSerialInterface_[BdSerialInterface::MAX_NUM_SERIAL_PORTS];

// global yuk...
ProxiInterface* PProxiInterface;

ExhCommunicationInterface* PExhCommunicationInterface;


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BdSerialInterface [constructor]
//
//@ Interface-Description
//  Base class constructor for the serial interface protocol handler.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

BdSerialInterface::BdSerialInterface( SerialPort & rSerialPort, 
									  BdSerialInterface::PortNum portNum ) 
	:  serialPortActivated_( FALSE )
	,rSerialPort_( rSerialPort )
	,portNum_( portNum )
{
	// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~BdSerialInterface [destructor]
//
//@ Interface-Description
//  Base class destructor for the serial interface protocol handler.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

BdSerialInterface::~BdSerialInterface()	
{
	// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Initialize
//
//@ Interface-Description
//  Initializes the GUI-Serial-Interface subsystem.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void BdSerialInterface::Initialize(void)
{
	//$[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: start
//
//@ Interface-Description
//  Set attribute serialPortActivated_ to TRUE
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void BdSerialInterface::start(void)
{
	// $[TI1]
	serialPortActivated_ = TRUE;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetAuxSerialPort
//
//@ Interface-Description
//  Returns a SerialPort reference to the static instantiation of the
//  SerialPort class specified by parameter portNum, but only for serial
//	ports 2 and 3. On first entry, it instantiates the SerialPort and
//	returns its reference.  Subsequent calls only return the reference
//	to the static instantiation.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Return a reference to the static instantiation of the serial port 
//  specified by parameter portNum.
//---------------------------------------------------------------------
//@ PreCondition
//  portNum is SERIAL_PORT_2 or SERIAL_PORT_3
//---------------------------------------------------------------------
//@ PostCondition
//  none
//---------------------------------------------------------------------
//@ End-Method
//=====================================================================
BdSerialPort & 
	BdSerialInterface::GetAuxSerialPort(BdSerialInterface::PortNum portNum)
{
	static BdSerialPort SerialPort2_(BdSerialInterface::SERIAL_PORT_2);

	BdSerialPort* pSerialPort = NULL;
	switch ( portNum )
	{
	case BdSerialInterface::SERIAL_PORT_2: // $[TI1.1]
		pSerialPort = &SerialPort2_;
		break;
	default:
		CLASS_ASSERTION_FAILURE();      
	}
	return(*pSerialPort);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetSerialPort
//
//@ Interface-Description
//  Returns a SerialPort reference to the static instantiation of the
//  SerialPort class specified by parameter portNum. On first 
//  entry, it instantiates the SerialPort and returns its reference. 
//  Subsequent calls only return the reference to the static instantiation.
//
//  Note that only the static instantiation of the SerialPort #1 object is
//	done directly in this method; for the SerialPort #2 and #3 objects this
//	is done via GetAuxSerialPort().
//---------------------------------------------------------------------
//@ Implementation-Description
//  Return a reference to the static instantiation of the serial port 
//  specified by parameter portNum.
//---------------------------------------------------------------------
//@ PreCondition
//  portNum is SERIAL_PORT_1, SERIAL_PORT_2 or SERIAL_PORT_3
//---------------------------------------------------------------------
//@ PostCondition
//  none
//---------------------------------------------------------------------
//@ End-Method
//=====================================================================
BdSerialPort & BdSerialInterface::GetSerialPort(BdSerialInterface::PortNum portNum)
{

	static BdSerialPort SerialPort1_(BdSerialInterface::SERIAL_PORT_1);
	BdSerialPort* pSerialPort = NULL;

	switch ( portNum )
	{
	case BdSerialInterface::SERIAL_PORT_1: // $[TI1.1]
		pSerialPort = &SerialPort1_;
		break;
	case BdSerialInterface::SERIAL_PORT_2: // $[TI1.2]
		pSerialPort = &BdSerialInterface::GetAuxSerialPort(portNum);
		break;
	default:
		CLASS_ASSERTION_FAILURE();      
	}
	return(*pSerialPort);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: driver_
//
//@ Interface-Description
//  Entry point for starting the protocol handler on the serial port. 
//  Sets the serial port parameters (baud rate etc.), flushes any
//  waiting data in the serial receive buffer, then enters the request
//  processing loop contained in processRequests_().
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void BdSerialInterface::driver_(void)
{

	processRequests_();

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Task
//
//@ Interface-Description
//  Called by the entry point for the GUI-Serial-Interface subsystem tasks.
//  Instantiates
//  the appropriate protocol handler based on vent operation mode and the
//  communications configuration setting value (Printer, DCI).
//  Passes control to the instantiated handler's driver. The 
//  BdSerialInterface::Task must run with supervisor privileges allowing
//  it to call the logio device functions directly.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void BdSerialInterface::Task(BdSerialInterface::PortNum portNum)
{
	//E600 moved the functinality in respective taskx
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Task1
//
//@ Interface-Description
//  Entry point for the GUI-Serial-Interface subsystem task 1.  
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void BdSerialInterface::Task1(void)
{
	
	// E600 moved functionality from Task	
#if 0
	//BdSerialInterface::Task(BdSerialInterface::SERIAL_PORT_1);

	BdSerialPort & rSerialPort = GetSerialPort(BdSerialInterface::SERIAL_PORT_1);

	ProxiInterface ProxiInterface(rSerialPort, BdSerialInterface::SERIAL_PORT_1);
	PProxiInterface = &ProxiInterface;

	while ( TaskControlAgent::GetMyState() != STATE_SERVICE && TaskControlAgent::GetMyState() != STATE_ONLINE )
	{
		Task::Delay(0,500);
	}
	
	PSerialInterface_[BdSerialInterface::SERIAL_PORT_1] = &ProxiInterface;
	

	PSerialInterface_[BdSerialInterface::SERIAL_PORT_1]->setSerialParameters_();
	PSerialInterface_[BdSerialInterface::SERIAL_PORT_1]->purgeline();
	// E600 TO DO 
	// PBdSerialInterface = PSerialInterface_[BdSerialInterface::SERIAL_PORT_1];

	for ( ;; )
	{
		PSerialInterface_[BdSerialInterface::SERIAL_PORT_1]->driver_();
		Task::Delay(0,15);	// huh???
	}
#endif
		// TODO E600 this is temp, remove when implemented
	for(;;)
	{
		RETAILMSG(0,(_T("BDSerailInterface::Task1 \r\n")) );
		Task::Delay(1);

	}

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Task1
//
//@ Interface-Description
//  Entry point for the GUI-Serial-Interface subsystem task 1.  
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void BdSerialInterface::Task2(void)
{
	// E600 created for Serail port 2	

	PExhCommunicationInterface = ExhCommunicationInterface::getInstance();
	
	PSerialInterface_[BdSerialInterface::SERIAL_PORT_2] = PExhCommunicationInterface;
	
	PBdSerialInterface = PSerialInterface_[BdSerialInterface::SERIAL_PORT_2];

	PSerialInterface_[BdSerialInterface::SERIAL_PORT_2]->driver_();

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SetupSerialPort
//
//@ Interface-Description
//  Inform serial interface protocol handler that there has been a 
//  change to the serial port's communication attributes. Called by 
//  Settings-Validation when a change occurs.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SigmaStatus BdSerialInterface::SetupSerialPort(void)
{
	//$[TI1]
	return(SUCCESS);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setSerialParameters_
//
//@ Interface-Description
//  Retrieves the DCI serial port settings from NOVRAM and sets the 
//  serial port accordingly.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls NovRamManager to retrieve the latest DCI serial port settings.
//  Translates these values to SerialPort values and calls 
//  SerialPort::setParameters() to initiate the change.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void BdSerialInterface::setSerialParameters_(Uint baudRate, Uint dataBits,
		                                     SerialPort::Parity parity, Uint stopBits)
{
	rSerialPort_.setParameters( baudRate, dataBits, parity, stopBits );
	// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Write
//
//@ Interface-Description
//  Invoke the protocol handler to write data from the callers buffer
//  to the serial interface. Transfers up to the specified number of
//  bytes or less depending on the protocol handler's buffering 
//  capability. Waits up to the specified timeout (in milliseconds) for
//  the transfer to occur.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Uint
	BdSerialInterface::Write( const void * pBuffer, Uint count, Uint milliseconds,
							  BdSerialInterface::PortNum portNum )
{
	SAFE_CLASS_ASSERTION( PSerialInterface_[portNum] != NULL );

	// $[TI1]
	return PSerialInterface_[portNum]->write(pBuffer, count, milliseconds);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Read
//
//@ Interface-Description
//  Invoke the protocol handler to transfer data from the 
//  serial interface into the caller's buffer up to the specified 
//  number of bytes. Waits up to the specified timeout (in milliseconds)
//  for data to become available before returning. Returns the number
//  of bytes actually written to the caller's buffer.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Uint BdSerialInterface::Read( void * pBuffer, Uint count, Uint milliseconds,
							 BdSerialInterface::PortNum portNum )
{
	AUX_CLASS_ASSERTION( PSerialInterface_[portNum] != NULL, portNum );

	// $[TI1]
	return PSerialInterface_[portNum]->read(pBuffer, count, milliseconds);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Start
//
//@ Interface-Description
//  Signal the protocol handler to start the interface. Initially, the
//  interface is disabled, this enables it.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
	BdSerialInterface::Start( void )
{
	for ( Uint32 portNum = BdSerialInterface::SERIAL_PORT_1; 
		portNum < BdSerialInterface::MAX_NUM_SERIAL_PORTS; portNum++ )
	{
		if ( PSerialInterface_[portNum] )
		{											// $[TI1.1]
			PSerialInterface_[portNum]->start();
		}											// $[TI1.2]
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: EnableLoopback
//
//@ Interface-Description
//  Calls the SmcpInterface handler to enable loopback mode.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	BdSerialInterface::EnableLoopback( BdSerialInterface::PortNum portNum )
{
	AUX_CLASS_ASSERTION( PSerialInterface_[portNum] != NULL, portNum );

	// $[TI1]
	PSerialInterface_[portNum]->enableLoopback();
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: DisableLoopback
//
//@ Interface-Description
//  Calls the SmcpInterface handler to disable loopback mode.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	BdSerialInterface::DisableLoopback( BdSerialInterface::PortNum portNum )
{
	AUX_CLASS_ASSERTION( PSerialInterface_[portNum] != NULL, portNum );

	// $[TI1]
	PSerialInterface_[portNum]->disableLoopback();
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: EnableExternalLoopback
//
//@ Interface-Description
//  Calls the SmcpInterface handler to enable external loopback mode.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	BdSerialInterface::EnableExternalLoopback( BdSerialInterface::PortNum portNum )
{
	AUX_CLASS_ASSERTION( PSerialInterface_[portNum] != NULL, portNum );

	// $[TI1]
	PSerialInterface_[portNum]->enableExternalLoopback();
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: DisableExternalLoopback
//
//@ Interface-Description
//  Calls the SmcpInterface handler to disable external loopback mode.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	BdSerialInterface::DisableExternalLoopback( BdSerialInterface::PortNum portNum )
{
	AUX_CLASS_ASSERTION( PSerialInterface_[portNum] != NULL, portNum );

	// $[TI1]
	PSerialInterface_[portNum]->disableExternalLoopback();
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: write
//
//@ Interface-Description
//  Writes the specified number of bytes from the buffer to the 
//  serial port. Waits up to the the specified timeout (in milliseconds)
//  for the data to enter the transmitter before returning. Returns
//  the number of bytes written to the serial port.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses the SerialPort class to implement the serial port write operation.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Uint BdSerialInterface::write( const void * pBuffer, Uint count, Uint milliseconds )
{
	// get the data into the logio output buffer
	Uint byteCount = rSerialPort_.write( pBuffer, count, milliseconds );

	// wait for it to enter transmitter
	Boolean waitFailed = rSerialPort_.wait( milliseconds );

	// $[TI1]
	return (waitFailed ? 0 : byteCount);

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: read
//
//@ Interface-Description
//  Reads the specified number of bytes in the buffer. Waits up to 
//  the specified timeout (in milliseconds) for the operation to 
//  complete before returning. Returns the number of bytes actually read.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses the SerialPort class to implement the serial port read operation.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Uint 
	BdSerialInterface::read( void * pBuffer, Uint count, Uint milliseconds )
{
	// $[TI1]
	return rSerialPort_.read( pBuffer, count, milliseconds );
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: bytesInReceiveBuffer
//
//@ Interface-Description
//  Reads the specified number of bytes in the buffer. 
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses the SerialPort class to implement the serial port 
//  bytesInReceiveBuffer operation.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Uint32 
	BdSerialInterface::bytesInReceiveBuffer( void )
{
	// $[TI1]
	return rSerialPort_.bytesInReceiveBuffer();
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SetCommunicationBaudRate
//
//@ Interface-Description
//  Sets desired baud rate 
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses the SerialPort class to implement the serial port 
//  SetCommunicationBaudRate operation.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Boolean 
	BdSerialInterface::SetCommunicationBaudRate(Uint32 BaudRate )
{
	// $[TI1]
	return rSerialPort_.SetCommunicationBaudRate(BaudRate);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: enableLoopback
//
//@ Interface-Description
//  This method is implemented in derived classes only. This method 
//  asserts when called for the base class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See interface description.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void 
	BdSerialInterface::enableLoopback(void)
{
	AUX_CLASS_ASSERTION_FAILURE(portNum_);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: disableLoopback
//
//@ Interface-Description
//  This method is implemented in derived classes only. This method 
//  asserts when called for the base class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See interface description.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void 
	BdSerialInterface::disableLoopback(void)
{
	AUX_CLASS_ASSERTION_FAILURE(portNum_);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: enableExternalLoopback
//
//@ Interface-Description
//  This method is implemented in derived classes only. This method 
//  asserts when called for the base class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See interface description.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void 
	BdSerialInterface::enableExternalLoopback(void)
{
	AUX_CLASS_ASSERTION_FAILURE(portNum_);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: disableExternalLoopback
//
//@ Interface-Description
//  This method is implemented in derived classes only. This method 
//  asserts when called for the base class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See interface description.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void 
	BdSerialInterface::disableExternalLoopback(void)
{
	AUX_CLASS_ASSERTION_FAILURE(portNum_);
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: purgeline
//
//@ Interface-Description
//    Flush the receive buffer.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
	BdSerialInterface::purgeline(void)
{
	// $[TI1]
	rSerialPort_.flushReceiveBuffer();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isSerialPortActivated_
//
//@ Interface-Description
//  Returns TRUE if the serial interface has been activated via the 
//  start() method.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns the value of private serialPortActivated_.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Boolean
	BdSerialInterface::isSerialPortActivated_(void) const
{
// $[TI1]
	return serialPortActivated_;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//@ Interface-Description
//  Implicitly gain execution control when a software assertion macro
//  detects and error.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Automatically invoked when CLASS_ASSERTION and CLASS_PRE_CONDITON
//  macros (among others) detect a fault.  We just call FaultHandler::SoftFault
//  to display pertinent information to the user.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

void    
	BdSerialInterface::SoftFault(  const SoftFaultID softFaultID
								   , const Uint32      lineNumber
								   , const char *      pFileName
								   , const char *      pPredicate )
{
	FaultHandler::SoftFault(softFaultID, BD_IO_DEVICES,BD_SERIAL_INTERFACE_CLASS,
							lineNumber, pFileName, pPredicate);
}

