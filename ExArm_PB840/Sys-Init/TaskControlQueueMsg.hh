#ifndef TaskControlQueueMsg_HH
#define TaskControlQueueMsg_HH
 
//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================
 
//====================================================================
// Class: TaskControlQueueMsg - Task-Control Inter-Task Message
//---------------------------------------------------------------------
//@ Version
// @(#) $Header  $
//
//@ Modification-Log
//
//   Revision 002   By:   sah      Date: 01/13/99    DR Number: 5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001  By:  gdc    Date:  15-May-95    DR Number:
//      Project:  Sigma (R8027)
//      Description:
//              Initial version (Integration baseline).
//
//====================================================================

#if 0  
// for bug in genDD
#endif

//@ Usage-Classes
#include "Sys_Init.hh"
#include "InterTaskMessage.hh"
//@ End-Usage
 
class TaskControlQueueMsg
{
  public:
    //@ Type: TCMEventType
    // This enum identifies the event type
    enum TCMEventType
    {
        TASK_CONTROL_NULL_MSG = 0,
        TASK_CONTROL_MSG      = FIRST_TASK_CONTROL_MSG,
        TASK_CONTROL_TIMER
    };

    TaskControlQueueMsg(Int32 objectId, Int32 objectData);
    TaskControlQueueMsg(Int32 queueData);
    TaskControlQueueMsg(const TaskControlQueueMsg& rEvent);
 
    ~TaskControlQueueMsg();

    void operator=(const TaskControlQueueMsg& rhs);
 
    inline Int32  getEventType(void) const;
    inline Int32  getEventData(void) const;
    inline Int32  getQueueData(void) const;
 
    static void     SoftFault(const SoftFaultID softFaultID,
			      const Uint32      lineNumber,
			      const char*       pFileName  = NULL,
			      const char*       pPredicate = NULL);
 
  private:

    TaskControlQueueMsg();       // declared only
    
    //@ Type: QueueMessage
    //  A 32-bit word accessors to an 8-bit eventType and 24-bit 
    //  eventData as well as access to the 32-bit queueData word.
    union QueueMessage
    {
        //  struct for all event types
        InterTaskMessage    event;
    
        // Use the following member to simply pass the 32 bits of
        // TaskControlQueueMsg data to message queues.
        Uint32 queueData;
    };

    //@ Data-Member:    msg_
    //  Contains the message in the QueueMessage union.
    QueueMessage    msg_;
};

#include "TaskControlQueueMsg.in"

#endif // TaskControlQueueMsg_HH
