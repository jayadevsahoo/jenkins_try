
// EBMDlg.cpp : implementation file
//

#include "stdafx.h"
#include "EBM.h"
#include "EBMDlg.h"
#include "BatteryInfo.hh"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

using namespace Gdiplus;

// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CEBMDlg dialog

UdpCommunication	CEBMDlg::UdpComm_;

CEBMDlg::CEBMDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CEBMDlg::IDD, pParent)
	, statusMsgId_(INITIALIZATING)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CEBMDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CEBMDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_MESSAGE(WM_WAIT_DATA, &CEBMDlg::OnWaitData )
	ON_MESSAGE(WM_BATTERY_INFO, &CEBMDlg::OnBatteryInfoReceived )
    ON_MESSAGE(WM_PARTIAL_DATA, &CEBMDlg::OnPartialDataReceived )
	
	//}}AFX_MSG_MAP
    ON_WM_ERASEBKGND()
    ON_BN_CLICKED(IDCANCEL, &CEBMDlg::OnBnClickedCancel)
END_MESSAGE_MAP()


// CEBMDlg message handlers

BOOL CEBMDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

    GetDlgItem(IDC_BUTTON_INDICATOR)->GetWindowRect( &indicatorRect_ );
    ScreenToClient( &indicatorRect_ );

    int halfHeight = indicatorRect_.Height() / 2;
    int halfWidth  = indicatorRect_.Width()  / 2;
   
    indicatorRectUp_ = indicatorRect_;
    indicatorRectUp_.bottom = indicatorRectUp_.top + halfHeight + 20;

    int lightSpace  = 5;
    int lightsWidth = (indicatorRectUp_.Height() - lightSpace * ( LIGHT_COUNT + 1 )) / LIGHT_COUNT;
    int lightsX     = indicatorRectUp_.left + halfWidth - lightsWidth / 2;

    for ( int i = 0; i < LIGHT_COUNT; ++ i )
    {
        lightsRect_[i].left  = lightsX;
        lightsRect_[i].right = lightsX + lightsWidth;
        lightsRect_[i].top   = indicatorRectUp_.top + lightSpace
            + (lightSpace + lightsWidth) * i;
        lightsRect_[i].bottom = lightsRect_[i].top + lightsWidth;
    }

    indicatorRectDown_ = indicatorRect_;
    indicatorRectDown_.top += indicatorRectUp_.Height() - 1;
    double w = (double)indicatorRectDown_.Width() / 5;
    indicatorRectDown_.left += static_cast<int>( w * 2 );
    indicatorRectDown_.right = indicatorRectDown_.left + static_cast<int>( w );

    indicatorColor_ = COLOR_YELLOW;

	setStatusMsg_( statusMsgId_, true );

	AfxBeginThread( CEBMDlg::BatteryInfoThreadProc, this );

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CEBMDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CEBMDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{

		CPaintDC dc(this);

        // Signal lights indicator is involved, because the PC is far away from 
        // EMC devices when testing.
        // For adding an indicator, its eaisier for tester to see the current 
        // communication status. 

        Graphics graphics(dc.m_hDC);
        graphics.SetSmoothingMode( SmoothingModeHighQuality );
        
        SolidBrush blackBrush( Color(255, 40, 11, 11));
        graphics.FillRectangle( &blackBrush, indicatorRectUp_.left, indicatorRectUp_.top, 
            indicatorRectUp_.Width(), indicatorRectUp_.Height() );

        graphics.FillRectangle( &blackBrush, indicatorRectDown_.left, indicatorRectDown_.top, 
            indicatorRectDown_.Width(), indicatorRectDown_.Height() );

        SolidBrush lightBrush( Color( 255, GetRValue( indicatorColor_ ), 
            GetGValue( indicatorColor_ ), GetBValue( indicatorColor_ ) ) );

        for ( int i = 0; i < LIGHT_COUNT; ++ i )
        {
            graphics.FillEllipse( &lightBrush, lightsRect_[i].left, lightsRect_[i].top,
                lightsRect_[i].Width(), lightsRect_[i].Height() );
        }
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CEBMDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

UINT CEBMDlg::BatteryInfoThreadProc(LPVOID lParam)
{
	CEBMDlg*	pGuiWnd	= static_cast<CEBMDlg*>( lParam );

	InitializeUdp_();
    static BatteryInfo	batteryInfo;

	memset( &batteryInfo, 0, sizeof(batteryInfo) );

	

	CString	strFileName(_T(".\\EBM_data.csv"));
	CFile	file;

	BOOL bWriteLog = file.Open(strFileName, CFile::modeWrite|CFile::modeCreate|CFile::modeNoTruncate);

	if ( bWriteLog )
	{
		file.SeekToEnd();

		if ( file.GetLength() == 0)
		{
			const char* header = "Time,StateOfCharge,RemainingCapacity,FullChargeCapacity,AverageCurrent,InstantaneousCurrent,Voltage,Temperature,SystemBatteryVoltage\r\n";
			file.Write(header, strlen(header));
		}
	}

    Uint8 recvBuf[ sizeof(batteryInfo) * 2 ];
    DWORD bytesInBuffer = 0;

    pGuiWnd->PostMessage(WM_WAIT_DATA);

    while ( !theApp.isClosing )
	{
        Sleep( 50 );

        DWORD bytesReaded = 0;
		bool readOk = UdpComm_.readData( recvBuf + bytesInBuffer,
                        sizeof(recvBuf) - bytesInBuffer, &bytesReaded, 1000 );
		
		if ( readOk )
		{
            if ( bytesInBuffer + bytesReaded < sizeof(batteryInfo) )
            {
                // Bytes in buffer is less than packet size, continue reading.
                bytesInBuffer += bytesReaded;
                continue;
            }
            else if ( bytesInBuffer + bytesReaded > sizeof(batteryInfo) )
            {
                // Stop the processing of current buffer, or the partial data
                // may mess up the following good packets
                bytesInBuffer = 0;
                pGuiWnd->PostMessage(WM_PARTIAL_DATA);
                continue;
            }

            // Now we got a full packet, copy to batteryInfo and reset the buffer
            memcpy( &batteryInfo, recvBuf, sizeof(batteryInfo) );
            bytesInBuffer = 0;

            // Convert network byte order to host
            batteryInfo.convNtoH();

            // Check the magic number for battery info
            if ( batteryInfo.magicNumber == BATTERYINFO_MAGIC_NUMBER )
            {
                ExternalBatteryInfo& ebi = batteryInfo.ebi;
                SystemBatteryInfo& sbi   = batteryInfo.sbi;
                if ( bWriteLog )
                {
                    CTime t = CTime::GetCurrentTime();

                    char log[256];

                    // Construct a csv format line
                    sprintf_s( log, "%04d-%02d-%02d %02d:%02d:%02d,%d,%d,%d,%d,%d,%d,%d,%f\r\n",
                        t.GetYear(), t.GetMonth(), t.GetDay(), t.GetHour(), t.GetMinute(), t.GetSecond(),
                        ebi.stateOfCharge, ebi.remainingCapacity, ebi.fullChargeCapacity,
                        ebi.averageCurrent, ebi.instantaneousCurrent, ebi.voltage,
                        ebi.temperature, sbi.voltage);

                    // Write a line to csv file
                    file.Write(log, strlen( log ));
                }
                // sendMessage to GUI
                pGuiWnd->SendMessage(WM_BATTERY_INFO, NULL, (LPARAM)&batteryInfo);

                continue;
            }
            else
            {
                // Unknow data, report as Partial Data
                pGuiWnd->PostMessage(WM_PARTIAL_DATA);
                continue;
            }
		}
		
		pGuiWnd->PostMessage(WM_WAIT_DATA);
	}

    // close file then notify dialog to close
    if ( bWriteLog )
    {
        file.Close();
    }

    pGuiWnd->PostMessage(WM_CLOSE);

	return 0;
}

void CEBMDlg::InitializeUdp_()
{
	UdpComm_.stop();
	UdpComm_.setup(UdpCommunication::UDP_RECEIVE, 7021);
}

LRESULT CEBMDlg::OnWaitData(WPARAM w, LPARAM l)
{
	setStatusMsg_( WAITING_FOR_NETWORK_DATA );
    setIndicatorColor_( COLOR_YELLOW );
	return 0;
}

void CEBMDlg::setStatusMsg_(StatusMsgId statusMsgId, bool forceUpdate)
{
	if ( statusMsgId_ != statusMsgId )
	{
		statusMsgId_ = statusMsgId;
		forceUpdate = true;
	}

	if ( forceUpdate )
	{
		const TCHAR* pMsg = getMsgText_( statusMsgId );
		GetDlgItem(IDC_STATIC_MSG)->SetWindowText( pMsg );
	}
}

const TCHAR * CEBMDlg::getMsgText_(StatusMsgId statusMsgId)
{
	const TCHAR* pMsg = _T("Undefined message.");

	switch( statusMsgId )
	{
	case INITIALIZATING:
		pMsg = _T("Initializating...");
		break;

	case WAITING_FOR_NETWORK_DATA:
		pMsg = _T("Waiting for network data...");
		break;

	case DATA_RECEVING:
		pMsg = _T("EBM data receiving...");
		break;
    
    case PARTIAL_DATA_RECEIVED:
        pMsg = _T("ERROR: partial data received.");
        break;
	}

	return pMsg;
}

LRESULT CEBMDlg::OnBatteryInfoReceived(WPARAM w, LPARAM l)
{
    ExternalBatteryInfo* pEBMInfo = &(((BatteryInfo*)l)->ebi);

	GetDlgItem(IDC_EDIT_INSTALLED)->SetWindowText(pEBMInfo->isInstalled ? _T("Yes") : _T("No"));
	GetDlgItem(IDC_EDIT_POWER_SOURCE)->SetWindowText(pEBMInfo->isPowerSource ? _T("Yes") : _T("No"));

	const TCHAR* pStrIsCharging = pEBMInfo->isCharging ? _T("Yes") : _T("No");
	GetDlgItem(IDC_EDIT_CHARGING)->SetWindowText(pEBMInfo->isInstalled ? pStrIsCharging : _T("N / A"));

	TCHAR text[256];
	_stprintf_s( text, _T("%d"), pEBMInfo->stateOfCharge );
	GetDlgItem(IDC_EDIT_STATE_OF_CHARGE)->SetWindowText( pEBMInfo->isInstalled ? text : _T("N / A"));

	_stprintf_s( text, _T("%d"), pEBMInfo->remainingCapacity );
	GetDlgItem(IDC_EDIT_REMAINING_CAPACITY)->SetWindowText( pEBMInfo->isInstalled ? text : _T("N / A") );

	_stprintf_s( text, _T("%d"), pEBMInfo->fullChargeCapacity );
	GetDlgItem(IDC_EDIT_FULL_CHARGE_CAPACITY)->SetWindowText( pEBMInfo->isInstalled ? text : _T("N / A") );

	_stprintf_s( text, _T("%d"), pEBMInfo->instantaneousCurrent );
	GetDlgItem(IDC_EDIT_INST_CURRENT)->SetWindowText( pEBMInfo->isInstalled ? text : _T("N / A") );

	_stprintf_s( text, _T("%d"), pEBMInfo->averageCurrent );
	GetDlgItem(IDC_EDIT_AVERAGE_CURRENT)->SetWindowText( pEBMInfo->isInstalled ? text : _T("N / A") );

	_stprintf_s( text, _T("%d"), pEBMInfo->voltage );
	GetDlgItem(IDC_EDIT_VOLTAGE)->SetWindowText( pEBMInfo->isInstalled ? text : _T("N / A") );

	// convert K to C
	double t = pEBMInfo->temperature;
	t /= 10;
	t -= 273.15;

	_stprintf_s( text, _T("%.2f"), t );
	GetDlgItem(IDC_EDIT_TEMPERATURE)->SetWindowText( pEBMInfo->isInstalled ? text : _T("N / A") );

	setStatusMsg_( DATA_RECEVING );
    setIndicatorColor_( pEBMInfo->isInstalled ? COLOR_GREEN : COLOR_RED );

    // Show system battery voltage 
    SystemBatteryInfo* pSystemBatteryInfo = &(((BatteryInfo*)l)->sbi);
    _stprintf_s( text, _T("%f"), pSystemBatteryInfo->voltage );
    GetDlgItem(IDC_EDIT_SYSBATT_VOLTAGE)->SetWindowText( text );
    
	return 0;
}

LRESULT CEBMDlg::OnPartialDataReceived(WPARAM w, LPARAM l)
{
    setStatusMsg_( PARTIAL_DATA_RECEIVED );
    setIndicatorColor_( COLOR_GRAY );
    return 0;
}


void CEBMDlg::setIndicatorColor_(COLORREF color)
{
    if ( indicatorColor_ != color )
    {
        indicatorColor_ = color;
        Invalidate( FALSE );
    }

}
BOOL CEBMDlg::OnEraseBkgnd(CDC* pDC)
{
    return CDialog::OnEraseBkgnd(pDC);
}

void CEBMDlg::OnBnClickedCancel()
{
    // Two times closing
    if ( !theApp.isClosing  )
    {
        theApp.isClosing = true;
    
        // Hide Window
        ShowWindow( SW_HIDE );
    }
    else
    {
        OnCancel();     
    }
}
