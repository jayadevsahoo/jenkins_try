 
#ifndef DciDataBitsSetting_HH
#define DciDataBitsSetting_HH

//====================================================================
//
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
//@ Class:  DciDataBitsSetting - DCI Data Bits Setting.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/DciDataBitsSetting.hhv   25.0.4.0   19 Nov 2013 14:27:20   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005   By: hct    Date: 03-DEC-1999    DR Number: 5493
//  Project:  GUIComms
//  Description:
//	Incorporated initial specifications for GUIComms Project.
//
//  Revision: 004   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'getApplicability()' method
//
//  Revision: 003   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 002   By: sah    Date:  25-Sep-1997    DR Number: 1825
//  Project: Sigma (R8027)
//  Description:
//	Added logic to this setting to disallow a value of 8-bits, when
//	parity is not "NONE".
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//		Initial version.
//  
//====================================================================

//@ Usage-Classes
#include "BatchDiscreteSetting.hh"
//@ End-Usage


class DciDataBitsSetting : public BatchDiscreteSetting
{
  public:
    DciDataBitsSetting(SettingId::SettingIdType settingId);
    virtual ~DciDataBitsSetting(void);

    virtual Applicability::Id  getApplicability(
			    const Notification::ChangeQualifier qualifierId
					       ) const;

    virtual SettingValue  getNewPatientValue(void) const;

#if defined(SIGMA_DEVELOPMENT)
    virtual Boolean  isAcceptedValid(void) const;
    virtual Boolean  isAdjustedValid(void);
#endif // defined (SIGMA_DEVELOPMENT)

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL, 
			   const char*       pPredicate = NULL);

  protected:
    virtual Boolean  calcDependentValues_(const Boolean isValueIncreasing);

  private:
    DciDataBitsSetting(void);
    DciDataBitsSetting(const DciDataBitsSetting&);	// not implemented...
    void  operator=(const DciDataBitsSetting&);	        // not implemented...

    //@ Data-Member:  settingId_
    // Setting Id for a comm port's data bits setting.
    SettingId::SettingIdType  settingId_;

};


#endif // DciDataBitsSetting_HH 
