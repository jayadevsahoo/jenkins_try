#ifndef	DciClassIds_HH
#define	DciClassIds_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ====================================================================
//@ Class: Dci - DCI SoftFault Class IDs
//---------------------------------------------------------------------
//@ Version-Information
// @ $Header:   /840/Baseline/DCI/vcssrc/DciClassIds.hhv   25.0.4.0   19 Nov 2013 14:01:58   pvcs  $
//
//@ Modification-Log
//
//  Revision  1.0	By: jdm	Date: 9/5/95	DR Number:
//	Project:    Sigma (R8027)
//	Description:
//		Initial version to coding standards (Integration baseline).
//=====================================================================

//@ Usage-Classes
//@ End-Usage


// class identification for all DCI-related FaultHandler::SoftFault()'s
enum DciClassIds {
	DCI_CLASS = 1,
	DCI_NUMBER_CLASS = 2,
	DCI_STRING_CLASS = 3,
	DCI_VALUE_STATUS_CLASS = 4
};

#endif	// DciClassIds_HH
