#pragma once

#include <ddraw.h>

class DDrawCanvas
{
private:
	LPDIRECTDRAW					PDD_;			// DirectDraw object
	LPDIRECTDRAWSURFACE				PDDSPrimary_;	// DirectDraw primary surface
	LPDIRECTDRAWCLIPPER				PWndClipper_;	// DirectDraw clipper for main window

private:
	bool init_(HWND hWnd);
	void release_();
	static const TCHAR* err_string_(HRESULT hr) ;
	

public:
	bool createOffscreenSurface(DWORD width, DWORD height, LPDIRECTDRAWSURFACE& offScrnSurface);
	LPDIRECTDRAWSURFACE getPrimarySurface();
	static bool failedCheck(HRESULT hr, const TCHAR* szMessage);

	DDrawCanvas(HWND hWnd);
	~DDrawCanvas(void);
};
