
#ifndef TemplateMacros_HH
#define TemplateMacros_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sotred in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Filename: TemplateMacros - Macros for the Template Names
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Foundation/vcssrc/TemplateMacros.hhv   25.0.4.0   19 Nov 2013 14:03:50   pvcs  $
//
//@ Modification-Log
//
//   Revision: 002   By: sah   Date: 15-Jan-1998   DR Number: 5004
//   Project:  Sigma (R8027)
//   Description:
//	Extended macros for the new 'HashTableC' class.
//
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//====================================================================

// only an ANSI-preprocessor will understand these macros...
#define  name2(a,b)			_name2_aux(a,b)
#define  _name2_aux(a,b)		a##b
#define  name4(a,b,c,d)			_name4_aux(a,b,c,d)
#define  _name4_aux(a,b,c,d)		a##b##c##d
#define  name6(a,b,c,d,e,f)		_name6_aux(a,b,c,d,e,f)
#define  _name6_aux(a,b,c,d,e,f)	a##b##c##d##e##f

//====================================================================
//  Array Templates...
//====================================================================

//@ Macro:  AbstractArray(TYPE)
// This provides a C++ template-like representation of the name of the
// abstract array template, where 'TYPE' is replaced with the specific
// type of the instantiated template.
#define  AbstractArray(TYPE)		name2(Array_,TYPE)

//@ Macro:  FixedArray(TYPE,SIZE)
// This provides a C++ template-like representation of the name of the
// fixed array template, where 'TYPE' is replaced with the specific
// type of the instantiated template and 'SIZE' is replaced with the
// specific size constant.
#define  FixedArray(TYPE,SIZE)		name4(Array_,TYPE,_,SIZE)


//====================================================================
//  Bit Array Templates...
//====================================================================

//@ Macro:  AbstractBitArray
// This provides a representation that is consistent with the other
// templates -- no arguments are taken.
#define  AbstractBitArray		BitArray

//@ Macro:  FixedBitArray(SIZE)
// This provides a C++ template-like representation of the name of the
// fixed bit array template, where 'SIZE' is replaced with the specific size
// constant.
#define  FixedBitArray(SIZE)		name2(BitArray_,SIZE)


//====================================================================
//  Heap Templates...
//====================================================================

//@ Macro:  AbstractHeap
// This provides a representation that is consistent with the other
// templates -- no arguments are taken.
#define  AbstractHeap		        Heap

//@ Macro:  FixedHeap(TYPE,SIZE)
// This provides a C++ template-like representation of the name of the
// fixed heap template, where 'TYPE' is replaced with the specific
// type of the instantiated template and 'SIZE' is replaced with the
// specific size constant.
#define  FixedHeap(TYPE,SIZE)		name4(Heap_,TYPE,_,SIZE)


//====================================================================
//  Doubly-Linked List Templates...
//====================================================================

//@ Macro:  DIteratorC(TYPE)
// This provides a C++ template-like representation of the name of the
// iterator for a doubly-linked list of copies, where 'TYPE' is replaced
// with the specific type of the instantiated template.
#define  DIteratorC(TYPE)	name2(DIterC_,TYPE)

//@ Macro:  DIteratorR(TYPE)
// This provides a C++ template-like representation of the name of the
// iterator for a doubly-linked list of references, where 'TYPE' is replaced
// with the specific type of the instantiated template.
#define  DIteratorR(TYPE)	name2(DIterR_,TYPE)

//@ Macro:  AbstractDListC(TYPE)
// This provides a C++ template-like representation of the name of the
// abstract doubly-linked list of copies, where 'TYPE' is replaced with
// the specific type of the instantiated template.
#define  AbstractDListC(TYPE)	name2(DListC_,TYPE)

//@ Macro:  FixedDListC(TYPE,SIZE)
// This provides a C++ template-like representation of the name of the
// fixed doubly-linked list of copies, where 'TYPE' is replaced with the
// specific type of the instantiated template and 'SIZE' is replaced with
// the specific size constant.
#define  FixedDListC(TYPE,SIZE)	name4(DListC_,TYPE,_,SIZE)

//@ Macro:  AbstractDListR(TYPE)
// This provides a C++ template-like representation of the name of the
// abstract doubly-linked list of references, where 'TYPE' is replaced with
// the specific type of the instantiated template.
#define  AbstractDListR(TYPE)	name2(DListR_,TYPE)

//@ Macro:  FixedDListR(TYPE,SIZE)
// This provides a C++ template-like representation of the name of the
// fixed doubly-linked list of references, where 'TYPE' is replaced with the
// specific type of the instantiated template and 'SIZE' is replaced with
// the specific size constant.
#define  FixedDListR(TYPE,SIZE)	name4(DListR_,TYPE,_,SIZE)


//====================================================================
//  Singly-Linked List Templates...
//====================================================================

//@ Macro:  SIteratorC(TYPE)
// This provides a C++ template-like representation of the name of the
// iterator for a singly-linked list of copies, where 'TYPE' is replaced
// with the specific type of the instantiated template.
#define  SIteratorC(TYPE)	name2(SIterC_,TYPE)

//@ Macro:  SIteratorR(TYPE)
// This provides a C++ template-like representation of the name of the
// iterator for a singly-linked list of references, where 'TYPE' is replaced
// with the specific type of the instantiated template.
#define  SIteratorR(TYPE)	name2(SIterR_,TYPE)

//@ Macro:  AbstractSListC(TYPE)
// This provides a C++ template-like representation of the name of the
// abstract singly-linked list of copies, where 'TYPE' is replaced with
// the specific type of the instantiated template.
#define  AbstractSListC(TYPE)	name2(SListC_,TYPE)

//@ Macro:  FixedSListC(TYPE,SIZE)
// This provides a C++ template-like representation of the name of the
// fixed singly-linked list of copies, where 'TYPE' is replaced with the
// specific type of the instantiated template and 'SIZE' is replaced with
// the specific size constant.
#define  FixedSListC(TYPE,SIZE)	name4(SListC_,TYPE,_,SIZE)

//@ Macro:  AbstractSListR(TYPE)
// This provides a C++ template-like representation of the name of the
// abstract singly-linked list of references, where 'TYPE' is replaced with
// the specific type of the instantiated template.
#define  AbstractSListR(TYPE)	name2(SListR_,TYPE)

//@ Macro:  FixedSListR(TYPE,SIZE)
// This provides a C++ template-like representation of the name of the
// fixed singly-linked list of references, where 'TYPE' is replaced with the
// specific type of the instantiated template and 'SIZE' is replaced with
// the specific size constant.
#define  FixedSListR(TYPE,SIZE)	name4(SListR_,TYPE,_,SIZE)


//====================================================================
//  Sorted List Templates...
//====================================================================

//@ Macro:  SortedIterC(TYPE)
// This provides a C++ template-like representation of the name of the
// iterator for a sorted list of copies, where 'TYPE' is replaced
// with the specific type of the instantiated template.
#define  SortedIterC(TYPE)	name2(SortIterC_,TYPE)

//@ Macro:  SortedIterR(TYPE)
// This provides a C++ template-like representation of the name of the
// iterator for a sorted list of references, where 'TYPE' is replaced
// with the specific type of the instantiated template.
#define  SortedIterR(TYPE)	name2(SortIterR_,TYPE)

//@ Macro:  AbstractSortC(TYPE)
// This provides a C++ template-like representation of the name of the
// abstract sorted list of copies, where 'TYPE' is replaced with
// the specific type of the instantiated template.
#define  AbstractSortC(TYPE)	name2(SortC_,TYPE)

//@ Macro:  FixedSortC(TYPE,SIZE)
// This provides a C++ template-like representation of the name of the
// fixed sorted list of copies, where 'TYPE' is replaced with the
// specific type of the instantiated template and 'SIZE' is replaced with
// the specific size constant.
#define  FixedSortC(TYPE,SIZE)	name4(SortC_,TYPE,_,SIZE)

//@ Macro:  FixedMutableSortC(TYPE,SIZE)
// This provides a C++ template-like representation of the name of the
// fixed, "mutable" sorted list of copies, where 'TYPE' is replaced
// with the specific type of the instantiated template and 'SIZE' is
// replaced with the specific size constant.
#define  FixedMutableSortC(TYPE,SIZE)	name4(MutableC_,TYPE,_,SIZE)

//@ Macro:  AbstractSortR(TYPE)
// This provides a C++ template-like representation of the name of the
// abstract sorted list of references, where 'TYPE' is replaced with
// the specific type of the instantiated template.
#define  AbstractSortR(TYPE)	name2(SortR_,TYPE)

//@ Macro:  FixedMutableSortR(TYPE,SIZE)
// This provides a C++ template-like representation of the name of the
// fixed, "mutable" sorted list of references, where 'TYPE' is replaced
// with the specific type of the instantiated template and 'SIZE' is
// replaced with the specific size constant.
#define  FixedMutableSortR(TYPE,SIZE)	name4(MutableR_,TYPE,_,SIZE)


//====================================================================
//  Hash Table Templates...
//====================================================================

//@ Macro:  HashTableC(TYPE,BUCKETS,SLOTS)
// This provides a C++ template-like representation of the name of the
// hash table, where 'TYPE' is replaced with the specific type of the
// to be stored, 'BUCKETS' identifies the number of hash "buckets", and
// 'SLOTS' identifies the number of "slots" in each bucket.
#define  HashTableC(TYPE,BUCKETS,SLOTS)	name6(HashC_,TYPE,_,BUCKETS,_,SLOTS)


#endif // TemplateMacros_HH 
