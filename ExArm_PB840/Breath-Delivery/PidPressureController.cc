#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: PidPressureController - Implements a pressure controller using
//      a modified PID controller.
//---------------------------------------------------------------------
//@ Interface-Description
//		This class controls the Psols so that the pressure in the
//		patient circuit is controlled to the desired pressure level during
//		the inspiration phase of TC inspiration or during any exhalation.  Methods are
//		implemented to get the flow command limit, to initialize the controller,
//		and to control the PSOL so that the desired	pressure is achieved.
//---------------------------------------------------------------------
//@ Rationale
//		This class implements the algorithms to control pressure in the
//		patient circuit.
//---------------------------------------------------------------------
//@ Implementation-Description 
//		This controller is invoked every new cycle of TC phase and during
//		each exhalation phase.  At the
//      beginning of each new breath, gains, flow command, compliance 
//      factor and pressure error are updated.  During TC based inspiration
//      and exhalation phase, the output to PSOLs are calculated using
//      updated values and o2 and air controllers are supplied with new
//      command values. 
//---------------------------------------------------------------------
//@ Fault-Handling
//	n/a.
//---------------------------------------------------------------------
//@ Restrictions
//		Only one instance of this class may be created
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header::   /840/Baseline/Breath-Delivery/vcssrc/PidPressureController.ccv   10.7   08/17/07 09:41:28   pvcs  
//
//@ Modification-Log
//
//  Revision: 007  By:  rhj    Date: 07-July-2008    SCR Number: 6435
//  Project:  LEAK
//  Description:
//       Leak Compensation project-related changes.
//       
//  Revision: 006  By: quf     Date:  28-Dec-2000    DR Number: 5832
//  Project:  Baseline
//  Description:
//		In newBreath() flowCommandLimit_ is now a function of patient
//		circuit type, same limits as in PressureController::newBreath().
//
//  Revision: 005  By: syw     Date:  31-Oct-2000    DR Number: 5793
//  Project:  Metabolics
//  Description:
//		Delete obsoleted requirement tracing.
//
//  Revision: 004  By: syw     Date:  09-Mar-2000    DR Number: 5684
//  Project:  NeoMode
//  Description:
//		Added call to FlowController::DeterminePsolLookupFlags() and added the
//		results of these flags as arguments to FlowController::updatePsol().
//
//  Revision: 003  By:  syw    Date:  23-Jun-1999    DR Number: DCS 5447
//       Project:  ATC
//       Description:
//			Add DCS 5322 to header
//
//  Revision: 002  By:  yyy    Date:   14-May-1999    DR Number: DCS 5432
//       Project:  ATC
//       Description:
//           Update O2 mixture to accommadate cycle to cycle change.
//
//  Revision: 001  By:  syw    Date:  14-Jan-1999    DR Number: DCS 5322
//       Project:  ATC
//       Description:
//             ATC initial version.
//
//=====================================================================

#include "PidPressureController.hh"
#include "O2Mixture.hh"
#include "PressureSensor.hh"
#include "FlowController.hh"
#include "BreathMiscRefs.hh"
#include "CircuitCompliance.hh"
#include "BdDiscreteValues.hh"
#include "PhasedInContextHandle.hh"
#include "Tube.hh"
#include "LeakCompEnabledValue.hh"
#include "LeakCompMgr.hh"
//@ Usage-Classes
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: PidPressureController()  
//
//@ Interface-Description
//		Default Constructor. This method has two FlowController&
//		as arguments and has no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The data members are initialized with the arguments passed in.
//		Other data members are also initialized.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

PidPressureController::PidPressureController(FlowController& o2FlCtrl,
											 FlowController& airFlCtrl)
: rO2FlowController_(o2FlCtrl),	   
  rAirFlowController_(airFlCtrl)   
{
	CALL_TRACE("PidPressureController::PidPressureController( \
                FlowController& o2FlCtrl, \
       			FlowController& airFlCtrl))") ;
	
	// $[TI2]

	// initialize private members
	ki_ = 1.0;
	kp_ = 0.5;
	kd_ = 3.0;
	complianceFactor_ = 1.0 ;
	
	flowCommandLimit_ = ADULT_MAX_FLOW ;

#if CONTROLS_TUNING
	overrideKp_ = FALSE;
	overrideKi_ = FALSE;
	overrideKd_ = FALSE;
#endif
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~PidPressureController()  
//
//@ Interface-Description
//		Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  	none
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

PidPressureController::~PidPressureController(void)
{
	CALL_TRACE("PidPressureController::~PidPressureController(void)") ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
PidPressureController::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, PIDPRESSURECONTROLLER,
                          lineNumber, pFileName, pPredicate);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newBreath()
//
//@ Interface-Description
//		This method has the proportional gain, integral gain, derivative gain,
//      lung flow, compliance factor and pressureError as arguments and  
//		has no return value.  This method is called at the beginning of every
//		pressure based inspiration to initialize the controller.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Initialize the gains of the controller with the arguments passed in.
//		Initialize other data members.  Initialize flow controllers with
//		their own newBreath() methods.
//---------------------------------------------------------------------
//@ PreCondition
// $[TC04024] flowCommand is initialized to lungFlow
// $[TC04018] gains are updated 		
// $[TC04022]
// $[TC04021]
// $[NC24003] limit flow based on circuit type
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
void
PidPressureController::newBreath(Real32 lungFlow,
								 Real32 kp, Real32 ki, Real32 kd,
								 Real32 complianceFactor,
								 Real32 pressureError)
{
	CALL_TRACE("PidPressureController::newBreath(Real32 lungFlow, \
											  Real32 kp, Real32 ki, Real32 kd, \
		 									  Real32 complianceFactor, \
                                              Real32 pressureError");
 
	// $[TI1]
	// update private members
	flowCommand_ = lungFlow;
	lastPressureError_ = pressureError;

#if CONTROLS_TUNING
    //Check override flag before updating gains
	if (!overrideKi_)
	{
		ki_ = ki;
	}

	if (!overrideKp_)
	{
		kp_ = kp;
	}

	if (!overrideKd_)
	{
		kd_ = kd;
	}
#else
	// update private members
	ki_ = ki;
	kp_ = kp;
	kd_ = kd;
#endif //#if defined(CONTROLS_TUNING)

	complianceFactor_ = complianceFactor ;

    // determine flowCommandLimit_
    const DiscreteValue  CIRCUIT_TYPE =
        PhasedInContextHandle::GetDiscreteValue(SettingId::PATIENT_CCT_TYPE);

    switch (CIRCUIT_TYPE)
    {
    case PatientCctTypeValue::PEDIATRIC_CIRCUIT :
		// $[TI1.2]

		// $[LC24004] -- When Leak Comp is ON, increase the maximum flow limits 
		// to PED_MAX_FLOW_LEAK LPM (Pediatric). 
		if (RLeakCompMgr.isEnabled())
		{
			flowCommandLimit_ = PED_MAX_FLOW_LEAK;	
		}
		else
		{
			// $[TI1.3]
			flowCommandLimit_ = PED_MAX_FLOW ;
		}

        break;
    case PatientCctTypeValue::ADULT_CIRCUIT :
		// $[TI1.1]
		flowCommandLimit_ = ADULT_MAX_FLOW ;
        break;
    case PatientCctTypeValue::NEONATAL_CIRCUIT :

		// $[LC24004] -- When Leak Comp is ON, increase the maximum flow limits 
		// to NEO_MAX_FLOW_LEAK LPM (Neonatal). 
		if (RLeakCompMgr.isEnabled())
		{
			flowCommandLimit_ = NEO_MAX_FLOW_LEAK;	
		}
		else
		{
			// $[TI1.3]
			flowCommandLimit_ = NEO_MAX_FLOW ;
		}

        break;
    default :
        AUX_CLASS_ASSERTION_FAILURE(CIRCUIT_TYPE);
        break;
    }

	rAirFlowController_.newBreath() ;
	rO2FlowController_.newBreath() ;

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updatePsol()
//
//@ Interface-Description
//		This method has pressure error and elapsed time as arguments and has no 
//		return value.  This method is called during every TC based 
//		inspiration and exhalation phase BD cycle to output to the PSOLs 
//      such that the desired pressure is achieved.
//---------------------------------------------------------------------
//@ Implementation-Description
//      The method calls calculateFlowCommand() to compute the flowCommand.
//      The desired air and O2 are determined by the o2Percent.
//	    The flowController is then called to control the PSOLs.
// $[TC04023] calculate O2Command and air command
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
void
PidPressureController::updatePsol( Real32 pressureError,
								   Uint32 time)
{
	// $[TI1]

	CALL_TRACE("PidPressureController::updatePsol(Real32 pressureError, \
								                  Uint32 time)");

	calculateFlowCommand(pressureError, time);

	Real32 o2Fraction = RO2Mixture.calculateO2Fraction(flowCommand_);
	
	Real32 o2Command = flowCommand_ * o2Fraction;
	Real32 airCommand = flowCommand_ - o2Command;

	Boolean useAirLookup = FALSE ;
	Boolean useO2Lookup = FALSE ;

	FlowController::DeterminePsolLookupFlags( airCommand, o2Command,
												useAirLookup, useO2Lookup) ;

    rAirFlowController_.updatePsol( airCommand, useAirLookup) ;
    rO2FlowController_.updatePsol( o2Command, useO2Lookup) ;

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: calculateFlowCommand()
//
//@ Interface-Description
//		This method has pressure error and elapsed time as arguments,  
//		computes the flowCommand, updates flowCommand_ and
//      returns the flowCommand value.  
//---------------------------------------------------------------------
//@ Implementation-Description
//      The flowCommand is computed with following equation:
//         flowCommand = kp_ * proportionalTerm + kd_ * derivativeTerm + 
//                       ki_ * estimationTerm
// $[TC04019] set proportional term equal to pressure error
// $[TC04020] calculate estimation term
// $[TC04021] calculate derivative term
// $[TC04022] calculate flow command
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
Real32
PidPressureController::calculateFlowCommand(Real32 pressureError,
											Uint32 time)
{
	CALL_TRACE("PidPressureController::calculateFlowCommand(Real32 pressureError, \
                                                         Uint32 time)");
	// $[TI1]
	// getComplianceVolume( ... CircuitCompliance::TUBE_COMPLIANCE) returns compliance
	// of one half of circuit tubing
	Real32 compliance = (RCircuitCompliance.getComplianceVolume(
								PRESSURE_TO_OBTAIN_COMPLIANCE,
								time, CircuitCompliance::TUBE_COMPLIANCE) * 2.0F)
							+ RCircuitCompliance.getBacteriaFilterCompliance() ;
	
   	Real32 estimationTerm = flowCommand_ + complianceFactor_ * pressureError * compliance / CYCLE_TIME_MS;
	Real32 derivativeTerm = (pressureError - lastPressureError_) / CYCLE_TIME_MS;
	Real32 proportionalTerm = pressureError;

	Real32 flowCommand = kp_ * proportionalTerm + kd_ * derivativeTerm + ki_ * estimationTerm;

	if (flowCommand < 0.0)
	{// $[TI1.1]
		flowCommand = 0.0;
	}
	// limit flowCommand
	else if (flowCommand > flowCommandLimit_)
	{// $[TI1.2]
		flowCommand = flowCommandLimit_;
	}// $[TI1.3]

	flowCommand_ = flowCommand;
	lastPressureError_ = pressureError;

	return(flowCommand_);

}

//=====================================================================
//
//  Protected Methods...
//
//=====================================================================
//=====================================================================
//
//  Private Methods...
//
//=====================================================================
