#ifndef BiLevelHighToLowExpTrigger_HH
#define BiLevelHighToLowExpTrigger_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: BiLevelHighToLowExpTrigger -  Triggers exhalation to implement the
//    transition from peep high to peep low in bilevel mode.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/BiLevelHighToLowExpTrigger.hhv   25.0.4.0   19 Nov 2013 13:59:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001  By:  yyy    Date:  11-Dec-1997    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#  include "Sigma.hh"
#  include "Breath_Delivery.hh"
#  include "TriggersRefs.hh"

//@ Usage-Classes
#  include "BreathTrigger.hh"
#  include "MathUtilities.hh"
//@ End-Usage

class BiLevelHighToLowExpTrigger : public BreathTrigger{
  public:
    BiLevelHighToLowExpTrigger(void);
    virtual ~BiLevelHighToLowExpTrigger(void);

    static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL, 
			  const char*       pPredicate = NULL);
  
    virtual void enable(void);
	inline void timeUpHappened(Boolean timeUp);

  protected:
    virtual Boolean triggerCondition_(void);

  private:
    BiLevelHighToLowExpTrigger(const BiLevelHighToLowExpTrigger&);		// not implemented...
    void   operator=(const BiLevelHighToLowExpTrigger&);	// not implemented...

    //@ Data-Member: timeUp_
    // Set to true when the interval timer expires
    Boolean timeUp_;

};


// Inlined methods...
#include "BiLevelHighToLowExpTrigger.in"


#endif // BiLevelHighToLowExpTrigger_HH 
