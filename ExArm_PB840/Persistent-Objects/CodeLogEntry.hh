
#ifndef CodeLogEntry_HH
#define CodeLogEntry_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sotred in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class:  CodeLogEntry - Entry for a Diagnostic Code Log.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Persistent-Objects/vcssrc/CodeLogEntry.hhv   25.0.4.0   19 Nov 2013 14:18:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By:  gdc    Date:  25-Jan-2007    SCR Number: 6237
//  Project:  TREND
//  Description:
//      Reduced number of EST log entries to 30.
//
//  Revision: 003  By:  sah    Date:  01-Jan-1999    DCS Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' to a non-inlined method.
//
//  Revision: 002  By:  sah    Date:  04-Sep-1997    DCS Number: 1923
//  Project:  Sigma (R8027)
//  Description:
//      Added new System Information Log (replacing the more specific
//	Communication Diagnostic Log), and modified the size of it and
//	the System Diagnostic Log.
//
//  Revision: 001  By:  sah    Date:  15-Mar-1997    DCS Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//          Initial version (Integration baseline).
//
//====================================================================

#include "PersistentObjsId.hh"

//@ Usage-Classes
#include "DiagnosticCode.hh"
#include "CrcUtilities.hh"
#include "TimeStamp.hh"

#if defined(SIGMA_DEVELOPMENT)
#include "Ostream.hh"
#endif  // defined(SIGMA_DEVELOPMENT)
//@ End-Usage


class CodeLogEntry
{
#if defined(SIGMA_DEVELOPMENT)
    friend Ostream&  operator<<(Ostream&            ostr,
				const CodeLogEntry& logEntry);
#endif  // defined(SIGMA_DEVELOPMENT)

  public:
    CodeLogEntry(void);
    ~CodeLogEntry(void);

    inline Boolean  isClear(void) const;
    inline void     clear  (void);

    Boolean  isCorrected(void) const;
    void     correct    (void);

    inline Boolean  isValid(void) const;

    inline Uint  getSequenceNumber(void) const;

    inline const TimeStamp&       getWhenOccurred  (void) const;
    inline const DiagnosticCode&  getDiagnosticCode(void) const;

    void  logDiagnosticCode(const DiagnosticCode& newDiagnosticCode);

    void  operator=(const CodeLogEntry& logEntry);

    Boolean  operator>(const CodeLogEntry& logEntry) const;

    Boolean         operator==(const CodeLogEntry& logEntry) const;
    inline Boolean  operator!=(const CodeLogEntry& logEntry) const;

    static void  SoftFault(const SoftFaultID softDiagID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL,
			   const char*       pPredicate = NULL);

  private:
#if defined(SIGMA_UNIT_TEST)
  public:  // allow access during unit tests...
#endif // defined(SIGMA_UNIT_TEST)
    CodeLogEntry(const CodeLogEntry&);	// not implemented...

    inline size_t  getCrcOffset_(void) const;

    //@ Data-Member:  sequenceNumber_
    // Used to determine the sequence of diagnostic events.
    Uint32  sequenceNumber_;

    //@ Data-Member:  whenOccurred_
    // The time and date of when this entry was added to the log.
    TimeStamp  whenOccurred_;

    //@ Data-Member:  diagnosticCode_
    // Event code.
    DiagnosticCode  diagnosticCode_;

    //@ Data-Member:  compilerPadding_
    // Since 'CrcValue' is a 16-bit type, the compiler adds 16 bits of
    // padding to this structure.  By defining this data member, the
    // calculation of the CRC value includes the padding.  This ensures
    // that any corruption within the diagnostic logs can be detected.
    Uint16  compilerPadding_;

    //@ Data-Member:  entryCrc_
    // CRC value for this log entry.
    CrcValue  entryCrc_;
};


// Inlined methods..
#include "CodeLogEntry.in"


//====================================================================
//
//  Global Constants...
//
//====================================================================

//@ Constant:  MAX_SYS_INFO_LOG_ENTRIES
// Constant that indicates the maximum number of diagnostics that are to be
// stored in the System Information Log.
//
//  $[00646] -- consists of a minimum of 50 entries...
enum { MAX_SYS_INFO_LOG_ENTRIES = 50 };


//@ Constant:  MAX_SYS_LOG_ENTRIES
// Constant that indicates the maximum number of diagnostics that are to be
// stored in the System Diagnostic Code Log.
//
//  $[00614] -- consists of a minimum of 30 entries...
enum { MAX_SYS_LOG_ENTRIES = 30 };


//@ Constant:  MAX_EST_LOG_ENTRIES
// Constant that indicates the maximum number of diagnostics that are to be
// stored in the EST Diagnostic Code Log.
//
//  $[00648] -- of which a minimum of 30 are from EST
enum { MAX_EST_LOG_ENTRIES = 30 };


//@ Constant:  MAX_SST_LOG_ENTRIES
// Constant that indicates the maximum number of diagnostics that are to be
// stored in the SST Diagnostic Code Log.
//
//  $[00648] -- of which a minimum of 20 are from SST...
enum { MAX_SST_LOG_ENTRIES = 20 };


//@ Constant:  MAX_TEST_ENTRIES
// Constant that indicates the size of the 'FixedArray(,)' to be used to
// retrieve the code log entries from any of the diagnostic logs.
// NOTE:  this MUST always be at least twice as big as the biggest above.
enum { MAX_CODE_LOG_ENTRIES = (::MAX_SYS_INFO_LOG_ENTRIES * 2) };


#endif // CodeLogEntry_HH 
