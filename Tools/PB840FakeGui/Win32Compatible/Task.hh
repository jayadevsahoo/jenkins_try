#pragma once

class Task
{
public:
	Task(void);
	~Task(void);

	static void Delay(const Uint32 seconds, const Uint32 mils);

	static Boolean IsTaskingOn();
};
