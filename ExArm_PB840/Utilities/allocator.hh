//------------------------------------------------------------------------------
//                   Copyright (c) 2008 - 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
/// @file Allocator.hh
///
/// This file contains definition of Allocator class.
//------------------------------------------------------------------------------
#ifndef ALLOCATOR_HH
#define ALLOCATOR_HH

#include <stddef.h>
#include <set>
#include "SigmaTypes.hh"
#include "SoftFaultId.hh"
#include "FaultHandlerMacros.hh"

// macro to provide header file interface
#define DECLARE_ALLOCATOR(class) \
    public: \
        void* operator new(size_t size) { \
            return _allocator.Allocate(size); \
        } \
        void operator delete(void* pObject) { \
            _allocator.Deallocate(pObject); \
        } \
    private: \
        static Allocator _allocator;

// macros to provide source file interface
#define IMPLEMENT_ALLOCATOR(class, objects) \
    Allocator class::_allocator(sizeof(class), objects, #class);
#define IMPLEMENT_ALLOCATOR_SETSIZE(class, objects, size) \
    Allocator class::_allocator(size, objects, #class);

class Allocator;

//------------------------------------------------------------------------------
// Functor to compare screen allocator block sizes
//------------------------------------------------------------------------------
class AllocatorSizeCompare
{
public:
    //------------------------------------------------------------------------------
    /// operator() function overload to compare allocator block sizes. Called by
	//	the STL library to sort Allocator objects by block size.
    /// @param[in]  a1 - allocator 1 to compare.
	/// @param[in]  a2 - allocator 2 to compare.
	//------------------------------------------------------------------------------
	bool operator ()(Allocator* a1, Allocator* a2);
};

void Allocator_Init();

//------------------------------------------------------------------------------
/// Allocator class provides fixed memory block new/delete services.
//------------------------------------------------------------------------------
class Allocator
{
public:
    //-------------------------------------------------------------------------
    /// This function must be called exactly one time *before* the operating system
	/// scheduler starts. When the system is still single threaded at startup,
	/// the Allocator API does not need mutex protection.
    /// @pre        Call before OS scheduler starts.
    /// @post       none
    //-------------------------------------------------------------------------
	static void Init();

    //-------------------------------------------------------------------------
    /// the initialization order of static variables is undefined according to
    /// c++ standard, this static method is used to ensure the `set` is 
    /// initialized before we insert Allocator instance into it. If this method
    /// is not defined, the inseration will cause a failure in constructor.
    //-------------------------------------------------------------------------
    static std::set<Allocator*, AllocatorSizeCompare>& GetAllocators();

    //-------------------------------------------------------------------------
    /// Constructor
    /// @param[in]  size - size of the fixed blocks
    /// @param[in]  objects - maximum number of object. If 0, new blocks are
	///		created off the heap as necessary.
	///	@param[in]	name - optional allocator name string.
    /// @pre        none
    /// @post       none
    //-------------------------------------------------------------------------
    Allocator(size_t size, Uint16 objects=0, const Char* name=NULL);

    //-------------------------------------------------------------------------
    /// Destructor
    /// @pre        none
    /// @post       none
    //-------------------------------------------------------------------------
    ~Allocator();

    //-------------------------------------------------------------------------
    /// Get a pointer to a memory block. This call is thread-safe and can be
    ///	called from multiple threads of control.
    /// @param[in]  size - size of the block to allocate
    /// @return     Returns pointer to the block. Otherwise NULL if unsuccessful.
    /// @pre        none
    /// @post       none
    //-------------------------------------------------------------------------
    void* Allocate(size_t size);

    //-------------------------------------------------------------------------
    /// Return a pointer to the memory pool. This call is thread-safe and can
    /// be called from multiple threads of control.
    /// @param[in]  pBlock - block of memory deallocate (i.e push onto free-list)
    /// @pre        none
    /// @post       none
    //-------------------------------------------------------------------------
    void Deallocate(void* pBlock);

    //-------------------------------------------------------------------------
    /// Get the allocator name string.
    /// @return		A pointer to the allocator name or NULL if none was assigned.
    /// @pre        none
    /// @post       none
    //-------------------------------------------------------------------------
    const Char* GetName() { return m_name; }

    //-------------------------------------------------------------------------
    /// Gets the fixed block memory size, in bytes, handled by the allocator.
    /// @return		The fixed block size in bytes.
    /// @pre        none
    /// @post       none
    //-------------------------------------------------------------------------
    size_t GetBlockSize() { return m_blockSize; }

    //-------------------------------------------------------------------------
    /// Gets the maximum number of blocks created by the allocator.
    /// @return		The number of fixed memory blocks created.
    /// @pre        none
    /// @post       none
    //-------------------------------------------------------------------------
    Uint GetBlockCount() { return m_blockCnt; }

    //-------------------------------------------------------------------------
    /// Gets the number of blocks in use.
    /// @return		The number of blocks in use by the application.
    /// @pre        none
    /// @post       none
    //-------------------------------------------------------------------------
    Uint GetBlocksInUse() { return m_blocksInUse; }

    //-------------------------------------------------------------------------
    /// Gets the total number of allocations for this allocator instance.
    /// @return		The total number of allocations.
    /// @pre        none
    /// @post       none
    //-------------------------------------------------------------------------
    Uint GetAllocations() { return m_allocations; }

    //-------------------------------------------------------------------------
    /// Gets the total number of deallocations for this allocator instance.
    /// @return		The total number of deallocations.
    /// @pre        none
    /// @post       none
    //-------------------------------------------------------------------------
    Uint GetDeallocations() { return m_deallocations; }

    //-------------------------------------------------------------------------
    /// Returns some previously allocated (but now unused) blocks to the global
    /// heap. At startup, Atlia creates a few very large memory blocks used
    /// as a scratch pad for decompressing images into video RAM. Once the the
    /// decompression is done, the memory blocks are not needed. Altia uses
    /// the fixed block allocator. Therefore, once Altia startup is complete
    ///	we will allow a one-time return of large memory blocks back to the
    ///	global heap. This method should be called only one time during startup.
    /// You do not have to call this method. If not, it just means we'll waste
    ///	some heap memory (about 10MB) from the large memory blocks Altia created.
    ///	Of note, even if the memory blocks for the allocator are deleted the
    /// allocator instance will	still work. There is nothing to prevent the
    /// allocator from reallocating	more memory at a later time.
    ///	flush.
    /// @pre        none
    /// @post       none
    //-------------------------------------------------------------------------
    static void OneTimeBlockDelete();

#ifdef WEBSERVICES
    //-------------------------------------------------------------------------
    /// Gets a std::set of all the allocator instances used within the system
    /// sorted by allocator fixed memory block size. Getting the internal list
    /// of allocators and iterating over them is not-thread safe. WebServices
    /// is not compiled into production versions of code, so its okay for
    ///	test builds only.
    /// @return		A multiset of all the Allocator instances.
    /// @pre        none
    /// @post       none
    //-------------------------------------------------------------------------
    static std::set<Allocator*, AllocatorSizeCompare>& GetAllocators() {
    	return m_allocators;
    }
#endif

private:
    //-------------------------------------------------------------------------
    /// Push a memory block onto head of free-list.
    /// @param[in]  pMemory - block of memory to push onto free-list
    /// @pre        none
    /// @post       none
    //-------------------------------------------------------------------------
    void Push(void* pMemory);

    //-------------------------------------------------------------------------
    /// Pop a memory block from head of free-list.
    /// @return     Returns pointer to the block. Otherwise NULL if unsuccessful.
    /// @pre        none
    /// @post       none
    //-------------------------------------------------------------------------
    void* Pop();

    //-------------------------------------------------------------------------
    /// Deletes all fixed blocks owned by the allocator and returns them to the
    ///	global heap.
    /// @pre        none
    /// @post       none
    //-------------------------------------------------------------------------
    void DeleteAllBlocks();

    //-------------------------------------------------------------------------
    /// Called exactly one time to create the Allocator mutex.
    /// @pre        none
    /// @post       none
    //-------------------------------------------------------------------------
    static void MutexInit();

    //-------------------------------------------------------------------------
    ///	Locks the mutex.
    /// @pre        none
    /// @post       none
    //-------------------------------------------------------------------------
    static void MutexLock();

    //-------------------------------------------------------------------------
    /// Unlocks the mutex.
    /// @pre        none
    /// @post       none
    //-------------------------------------------------------------------------
    static void MutexUnlock();

    //--------------------------------------------------------------------------
    // private structure
    //--------------------------------------------------------------------------
    struct Block
    {
        Block* pNext;
    };

    //--------------------------------------------------------------------------
    // normal member data
    //--------------------------------------------------------------------------
    const size_t m_blockSize;
    const size_t m_objectSize;
    const Uint16 m_maxObjects;
    Block* m_pHead;
    Char* m_pPool;
    Uint16 m_poolIndex;
    Uint m_blockCnt;
    Uint m_blocksInUse;
    Uint m_allocations;
    Uint m_deallocations;
    const Char* m_name;
    static Boolean m_mutexInitialized;
    static const Uint MIN_ONE_TIME_DELETE_BLOCK_SIZE;

    SOFT_FAULT_DEFN()
};

#endif //ALLOCATOR_HH



