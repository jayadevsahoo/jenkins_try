#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//================= C L A S S   D E S C R I P T I O N =================
//@ Class: HumidVolumeSetting - Humidifier Volume Setting
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, bounded setting that is used to adjust the empty
//  humidifier volume -- if applicable.  This class inherits from
//  'BatchBoundedSetting' and provides the specific information needed
//  for representation of humidifier volume.  This information includes
//  the interval and range of this setting's values, this batch setting's
//  new-patient value (see 'getNewPatientValue()'), and the contraints of
//  this setting.
//
//  This setting also dynamically monitors the humidifier type setting,
//  and updates its applicability accordingly.
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class has no dependent settings, and never needs to update its
//  bound values, therefore 'calcDependentValues_()' and 'updateConstraints_()'
//  are NOT overridden.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/HumidVolumeSetting.ccv   25.0.4.0   19 Nov 2013 14:27:26   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006   By: gdc    Date: 27-Apr-2009    SCR Number: 6489
//  Project:  840S
//  Description:
//      Modifications to provide for transitioning of tube 
//		I.D. to new patient value when spontaneous type is changed to 
//		PAV with an incompatible tube I.D.. This includes changes to 
//		the user interface to verify transitioned value with flashing
//		verify arrow icon. Change better supports verification icon
//		used for tube type and I.D. as well as humidification type and
//		volume.
//
//  Revision: 005   By: sah   Date:  23-May-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//	VTPC project-related changes:
//      *  eliminated unneeded include of 'SupportTypeValue.hh'
//
//  Revision: 004   By: sah   Date:  22-Jul-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  modified to use newly-modified 'BoundedRange' class to handle
//         range, via linked list of interval nodes
//
//  Revision: 003   By: sah   Date:  17-May-1999    DR Number:  5391
//  Project:  ATC
//  Description:
//     Changed applicability of this setting to no longer be dependent
//     on the TC option being activated.
//
//  Revision: 002   By: sah   Date:  06-Jan-1999    DR Number:  5314
//  Project:  ATC
//  Description:
//     Added new 'getApplicability()' method.
//
//  Revision: 001   By: sah   Date:  06-Jan-1999    DR Number:  5322
//  Project:  ATC
//  Description:
//	New ATC-specific setting.
//
//=====================================================================

#include "HumidVolumeSetting.hh"
#include "HumidTypeValue.hh"

//@ Usage-Classes
#include "SettingsMgr.hh"
#include "ContextMgr.hh"
#include "AcceptedContext.hh"
//@ End-Usage


//@ Code...

//====================================================================
//
//  Static Data Definitions...
//
//====================================================================

//  $[TC02002] -- The setting's range...
//  $[TC02004] -- The setting's resolution ...
static const BoundedInterval  LAST_NODE_ =
{
	100.0f,		// absolute minimum...
	0.0f,		// unused...
	TENS,		// unused...
	NULL
};
static const BoundedInterval  INTERVAL_LIST_ =
{
	1000.0f,		// absolute maximum...
	10.0f,		// resolution...
	TENS,		// precision...
	&::LAST_NODE_
};


//====================================================================
//
//  Public Methods...
//
//====================================================================

//============= M E T H O D   D E S C R I P T I O N ==================
//@ Method: HumidVolumeSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This setting inherits from BoundedSetting.  The method passes to
//  BoundedSetting it's default information. Also, this method initializes
//  the value interval range for the setting.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

HumidVolumeSetting::HumidVolumeSetting(void)
: BatchBoundedSetting(SettingId::HUMID_VOLUME,
					  Setting::NULL_DEPENDENT_ARRAY_,
					  &::INTERVAL_LIST_,
					  HUMID_VOLUME_MAX_ID,	 // maxConstraintId...
					  HUMID_VOLUME_MIN_ID)	 // minConstraintId...
{
	CALL_TRACE("HumidVolumeSetting()");
}  // $[TI1]


//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  ~HumidVolumeSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

HumidVolumeSetting::~HumidVolumeSetting(void)
{
	CALL_TRACE("~HumidVolumeSetting()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability(qualifierId)  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[01117]\d\ -- "more" settings applicability
//  $[01269]\c\ -- SST settings applicability
//---------------------------------------------------------------------
//@ PreCondition
//  (qualifierId == Notification::ACCEPTED  ||
//   qualifierId == Notification::ADJUSTED)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
HumidVolumeSetting::getApplicability(const Notification::ChangeQualifier qualifierId) const
{
	CALL_TRACE("getApplicability(qualifierId)");

	const Setting*  pHumidType =
	SettingsMgr::GetSettingPtr(SettingId::HUMID_TYPE);

	DiscreteValue  humidTypeValue;

	switch ( qualifierId )
	{
		case Notification::ACCEPTED :	  // $[TI1]
			humidTypeValue = pHumidType->getAcceptedValue();
			break;
		case Notification::ADJUSTED :	  // $[TI2]
			humidTypeValue = pHumidType->getAdjustedValue();
			break;
		default :
			AUX_CLASS_ASSERTION_FAILURE(qualifierId);
			break;
	}

	return((humidTypeValue == HumidTypeValue::NON_HEATED_TUBING_HUMIDIFIER  ||
			humidTypeValue == HumidTypeValue::HEATED_TUBING_HUMIDIFIER)
		   ? Applicability::CHANGEABLE		 // $[TI3]
		   : Applicability::INAPPLICABLE);	 // $[TI4]
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [const, virtual]
//
//@ Interface-Description
//  Return the setting's new patient value.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
HumidVolumeSetting::getNewPatientValue(void) const
{
	CALL_TRACE("getNewPatientValue()");

	const AcceptedContext&  rAcceptedContext = ContextMgr::GetAcceptedContext();

	// use the setting's value set during SST Setup...
	const BoundedValue  NEW_PATIENT =
	rAcceptedContext.getSettingValue(SettingId::HUMID_VOLUME);

	return(NEW_PATIENT);
}  // $[TI1]

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  isChanged()  [const, virtual]
//
//@ Interface-Description
//  Has this setting been changed? 
//---------------------------------------------------------------------
//@ Implementation-Description
//  This setting is changed when the base class indicates that it has
//  changed or when the humidification type has changed and volume 
//  applicability is changeable.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean	
HumidVolumeSetting::isChanged(void) const
{
	return BatchBoundedSetting::isChanged()
		   || (SettingsMgr::GetSettingPtr(SettingId::HUMID_TYPE)->isChanged()
			   && getApplicability(Notification::ADJUSTED) == Applicability::CHANGEABLE);
}


//==================== M E T H O D   D E S C R I P T I O N =============
//@ Method:  valueUpdate(qualifierId, pSubject)  [virtual]
//
//@ Interface-Description
//  Respond to subject's value changes, by, possibly, updating this setting's
//  applicability.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
HumidVolumeSetting::valueUpdate(const Notification::ChangeQualifier qualifierId,
								const SettingSubject* pSubject)
{
	CALL_TRACE("valueUpdate(qualifierId, pSubject)");

	// update this instance's applicability...
	updateApplicability(qualifierId);

}  // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: doRetainAttachment(pSubject)  [virtual, const]
//
//@ Interface-Description
//  When task-level, state changes occur, all subjects are notified
//  to detach from all observers.  This is done because the GUI-Apps
//  observers may be overwritten by new class instances, without any
//  explicit destructor/detachment calls.
//
//  This method is defined here to indicate to any of this instance's
//  subjects, not to detach this observer.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean
HumidVolumeSetting::doRetainAttachment(const SettingSubject*) const
{
	CALL_TRACE("doRetainAttachment(pSubject)");

	return(TRUE);
}  // $[TI1]


//==================== M E T H O D   D E S C R I P T I O N ============
//@ Method:  settingObserverInit()  [virtual]
//
//@ Interface-Description
//  This setting needs to monitor the value of Humidification Type setting,
//  therefore this virtual method is overridden to provide a point during
//  initialization to attach to that (subject) setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
HumidVolumeSetting::settingObserverInit(void)
{
	CALL_TRACE("settingObserverInit()");

	// monitor changes in humidifier type's value...
	attachToSubject_(SettingId::HUMID_TYPE, Notification::VALUE_CHANGED);
}  // $[TI1]


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//	      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
HumidVolumeSetting::SoftFault(const SoftFaultID  softFaultID,
							  const Uint32       lineNumber,
							  const char*        pFileName,
							  const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

	FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
							HUMID_VOLUME_SETTING, lineNumber, pFileName,
							pPredicate);
}
