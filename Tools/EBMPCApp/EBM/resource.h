//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by EBM.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_EBM_DIALOG                  102
#define IDR_MAINFRAME                   128
#define IDC_EDIT_INSTALLED              1000
#define IDC_EDIT_POWER_SOURCE           1001
#define IDC_EDIT_STATE_OF_CHARGE        1002
#define IDC_EDIT_REMAINING_CAPACITY     1003
#define IDC_EDIT_FULL_CHARGE_CAPACITY   1004
#define IDC_EDIT_AVERAGE_CURRENT        1005
#define IDC_EDIT_VOLTAGE                1006
#define IDC_EDIT_TEMPERATURE            1007
#define IDC_STATIC_MSG                  1008
#define IDC_EDIT_CHARGING               1009
#define IDC_EDIT_INST_CURRENT           1010
#define IDC_BUTTON_INDICATOR            1011
#define IDC_EDIT_SYSBATT_VOLTAGE        1012

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1012
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
