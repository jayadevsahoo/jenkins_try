#include "stdafx.h"
#if defined(SIGMA_BD_CPU)

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// =========================== C L A S S   D E S C R I P T I O N ====
//@ Class: BackgroundBdTests - Individual background tests for the BD.
//---------------------------------------------------------------------
//@ Interface-Description
//  This class consists of static methods that perform the cyclic background
//  tests.  These methods are not intended to be called directly, rather they
//  are intended to be constructed with each BackgroundTimer object that is
//  assigned to a test.
//---------------------------------------------------------------------
//@ Rationale
//  The class implements the test algorithms for the cyclic background
//  tests for BDU.
//---------------------------------------------------------------------
//@ Implementation-Description
//  When a BackgroundTimer object is created it is passed a pointer to one
//  of the static methods of this class.  The methods in this class organize
//  all the tests of the BD CPU in a single place for maintenance purposes.
//---------------------------------------------------------------------
//@ Fault-Handling
//  N/A
//---------------------------------------------------------------------
//@ Restrictions
//  None
//---------------------------------------------------------------------
//@ Invariants
//  None
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Safety-Net/vcssrc/BackgroundBdTests.ccv   25.0.4.0   19 Nov 2013 14:20:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 017  By: rpr   Date:  05-Nov-2010    SCR Number: 6690
//    Project:  PROX
//    Description:
//      Reverted revision 016.  The clock variance test has been removed so the 
//		test of the rtc has been brought back.  
//
//  Revision: 016  By: gdc   Date:  18-Aug-2009    SCR Number: 6147
//    Project:  XB
//    Description:
//      Removed background Time-of-Day clock test. Moved this function to
// 		the cycle-to-cycle and interval time variance tests in BdMonitoredData.
//
//  Revision: 015  By: iv    Date:  19-May-1999    DR Number: DCS 5397
//    Project:  ATC (840)
//    Description:
//      Restructure ADC test. The loopback stuck test is now reporting results
//      to the information log only, it does not cause a BTAT1. This test is
//      redundant to the ADC LOW and ADC HIGH tests.
//
//  Revision: 014  By: iv    Date:  03-Mar-1999    DR Number: 5344
//  Project:  ATC
//  Description:
//      Changed MAX_ADC_SUB_MUX_CYCLE_TIME to 80 from 40.
//
//  Revision: 013  By: sah    Date:  15-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 012    By: iv   Date: 11-Mar-1998    DR Number: DCS 5041, 5049, 5076
//    Project: Color
//    Description:
//       Set errorCode_ whenever relevant; improved the TOD test; require
//       2 bad samples to fail dac wrap test.
//
//
//  Revision: 011    By: iv   Date: 12-May-1998    DR Number: DCS 5076
//    Project: Sigma (840)
//    Description:
//       Require 2 bad samples to fail dac wrap test.
//
//  Revision: 010   By: pc   Date: 22-Oct-1997    DR Number: 2570
//    Project:  Signa (840)
//    Description:
//      Converted SIGMA_INTEGRATION compiler option to work with Sigma Integration Test (aka TestCommand Group)
//         Allows testing of requirements 06024; 06027; 06125.
//
//  Revision: 009   By: sp   Date: 9-July-1997    DR Number: 2387
//    Project:  Sigma (840)
//    Description:
//      Added integration test code.
//
//  Revision: 008  By: iv    Date:  29-May-1997    DR Number: 2018
//  	Project:  Sigma (840)
//		Description:
//			Remove obsolete file header Wait.hh.
//
//  Revision: 007  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 006   By: iv    Date: 06-May-1997   DR Number: DCS 2027, 2040 
//    Project:  Sigma (R8027)
//    Description:
//      Modified method AudibleAlarmDisconnectTest().
//
//  Revision: 005   By: iv    Date: 29-Apr-1997   DR Number: None 
//    Project:  Sigma (R8027)
//    Description:
//      Rework per unit test peer review.
//
//  Revision: 004   By: iv   Date: 28-Apr-1997    DR Number: 1996
//    Project:  Sigma (840)
//    Description:
//		Added tracing to SRS req. 00524, 00528
//
//  Revision: 003   By: by   Date: 14-Apr-1997    DR Number: 1921
//    Project:  Sigma (840)
//    Description:
//		Check that alarm is off for 28 seconds before reporting background
//		error in AudibleAlarmDisconnectTest().
//
//  Revision: 002   By: by   Date: 12-Feb-1997    DR Number: 1718
//    Project:  Sigma (840)
//    Description:
//      Added logic to register with VentStatus class for the alarm
//      activation flag during AudibleAlarmDisconnectTest().
//
//  Revision: 001   By: by   Date: 12-DEC-1995    DR Number: None
//    Project:  Sigma (840)
//    Description:
//      Initial version.
//
//=====================================================================

#include "Sigma.hh"
#include "BackgroundBdTests.hh"

//@ Usage-Classes
#include "MiscSensorRefs.hh"
#include "NovRamManager.hh"
#include "RegisterRefs.hh"
#include "SafetyNetSensorData.hh"
#include "Sensor.hh"
#include "TemperatureSensor.hh"
#include "Task.hh"
//#include "SubMux.hh"
#include "LinearSensor.hh"
#include "MainSensorRefs.hh"
#include "BdSystemStateHandler.hh"
#include "SystemBattery.hh"
#include "VentStatus.hh"        
#include "VentObjectRefs.hh"        

#ifdef SIGMA_INTEGRATION
#include "TestTrigger.hh"
#endif //SIGMA_INTEGRATION

#ifdef SIGMA_UNIT_TEST
# include "PhasedInContextHandle.hh"
#endif //SIGMA_UNIT_TEST

// Initialize all static private data members
Boolean BackgroundBdTests::AdcTestFailed_ = FALSE;
Boolean BackgroundBdTests::AcSwitchPositionTestFailed_ = FALSE;
Boolean BackgroundBdTests::NovRamChecksumTestFailed_ = FALSE;
Boolean BackgroundBdTests::TodTestFailed_ = FALSE;
Boolean BackgroundBdTests::TodTestInitialized_ = FALSE;
Int32 BackgroundBdTests::AdcTestIndex_ = 0;
Int32 BackgroundBdTests::AdcFaultCounter_ = 0;
Uint8 BackgroundBdTests::NumberAcSwitchCycles_ = 0;
TimeStamp BackgroundBdTests::PreviousTime_;
TimeStamp BackgroundBdTests::CurrentTime_;

// wait 80 MS of submux channels to be updated
static const Uint8 MAX_ADC_SUB_MUX_CYCLE_TIME = 80;

// AI PCBA Requirements Specification No. 70550-45 Rev. 6
// Read Word #2: A/C power switch Mask
static const Uint16 AI_PWR_SWITCH_POSITION_MASK = 0x0100;

// maximum time threshold for TOD test 1 minute in MS
static const Uint16 MAX_TIME_DIFFERENCE = 60000;

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  AudibleAlarmCapacitorTest
//
//@ Interface-Description
//  This method accepts Uint16 for error code and returns a BkEventName
//  enumeration.
//
//  Performs a range test of the Powerfail Capacitor voltage channel.
//  Returns BK_NO_EVENT if conditions have been met,
//  BK_POWER_FAIL_CAP otherwise.
//---------------------------------------------------------------------
//@ Implementation-Description
//  A BD-IO-Devices object is used to read the raw counts of the channel.
//  This same object contains the methods to range check the channel
//  using the raw counts read.  The criteria for consecutive readings is
//  implemented in getBackgndEventId. 
//
//  $[06054] $[06055]
//---------------------------------------------------------------------
//@ PreCondition
//  A SafetyNetSensorData object should exist, ie- should not be NULL.
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

BkEventName
BackgroundBdTests::AudibleAlarmCapacitorTest( Uint16 &errorCode )
{
    SafetyNetSensorData*   pSensorData;
    BkEventName            rtnValue;

    // $[TI1]
    // Set up the pointer to the SafetyNetSensorData object
    // for the Powerfail capacitor
    pSensorData = RPowerFailCapVoltage.getSafetyNetSensorDataAddr();

    CLASS_PRE_CONDITION( pSensorData != NULL );

    // Read the raw counts 
    AdcCounts count = RPowerFailCapVoltage.getCount();    
    errorCode = count;
    pSensorData->sensorReadingRangeCheck( count );

    // getBackgndEventId returns BK_NO_EVENT if there is no Background
    // event to report or if it was previously reported
    rtnValue = pSensorData->getBackgndEventId();

    return( rtnValue );
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  AudibleAlarmDisconnectTest
//
//@ Interface-Description
//  This method accepts Uint16 for error code and returns a BkEventName
//  enumeration.
//
//  Performs a range test of the Alarm Cable voltage channel.
//  Returns BK_NO_EVENT if conditions have been met, BK_ALARM_CABLE
//  otherwise.
//---------------------------------------------------------------------
//@ Implementation-Description
//  A BD-IO-Devices object is used to read the raw counts of the channel.
//  This same object contains the methods to range check the channel
//  using the raw counts read.  The criteria for consecutive readings is
//  implemented in getBackgndEventId. 
//
//  $[06159] $[06160]
//---------------------------------------------------------------------
//@ PreCondition
//  A SafetyNetSensorData object should exist, ie- should not be NULL.
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

BkEventName
BackgroundBdTests::AudibleAlarmDisconnectTest(  Uint16 &errorCode )
{
	const Uint32 CHARGE_TIME_MS = 60000 ;
	
    Boolean alarmActive = TRUE;
    SafetyNetSensorData*   pSensorData = NULL;
    BkEventName rtnValue = BK_NO_EVENT;

    // Set up the pointer to the SafetyNetSensorData object
    // for the alarm cable voltage
    pSensorData = RAlarmCableVoltage.getSafetyNetSensorDataAddr();
     CLASS_PRE_CONDITION( pSensorData != NULL );

     // Read the raw counts and send it to the range check test
    AdcCounts count = RAlarmCableVoltage.getCount();    
    pSensorData->sensorReadingRangeCheck( count );
    errorCode = count;

     // retrieve Background test status
    rtnValue = pSensorData->getBackgndEventId();
    alarmActive = RVentStatus.getBdAudioAlarmStatus();
    // no error should be reported if the BD audio alarm was active
    if ( TRUE == alarmActive ||
         RVentStatus.getAlarmOffTimeMs() < CHARGE_TIME_MS )
    {
    // $[TI1]
        pSensorData->resetNumCyclesOor();
        pSensorData->setBackgndCheckFailed( FALSE );
        pSensorData->setBackgndEventReported( FALSE );
        rtnValue = BK_NO_EVENT;
    }
    // $[TI2]
    return( rtnValue );
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  BatteryTest
//
//@ Interface-Description
//  This method accepts Uint16 for error code and returns a BkEventName
//  enumeration.
//
//  Performs a range test of the Battery Voltage, Current and Model
//  voltage channels.  Returns BK_NO_EVENT if conditions have been met, 
//  BK_BPS_VOLTAGE_OOR, BK_BPS_CURRENT_OOR, or BK_BPS_MODEL_OOR
//  otherwise.
//---------------------------------------------------------------------
//@ Implementation-Description
//  BD-IO-Devices objects are used to read the raw counts of the channels.
//  These same objects contains the methods to range check the channel
//  using the raw counts read.  The criteria for pass criteria is
//  implemented in getBackgndEventId. 
//
//  $[06136] $[06138] $[06139] $[06143]
//---------------------------------------------------------------------
//@ PreCondition
//  A SafetyNetSensorData object should exist, ie- should not be NULL.
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

BkEventName
BackgroundBdTests::BatteryTest(  Uint16 &errorCode )
{
    SafetyNetSensorData*     pSensorData;
    BkEventName              rtnValue = BK_NO_EVENT;
    SystemBattery::BpsStatus bpsStatus = RSystemBattery.getBpsStatus();

    // Set up the pointer to the SafetyNetSensorData object
    // for the Battery Model
    pSensorData = RSystemBatteryModel.getSafetyNetSensorDataAddr();

    CLASS_PRE_CONDITION( pSensorData != NULL );

    // checks the BPS model to be within range
    // Read the raw counts and send it to the range check test
    AdcCounts count = RSystemBatteryModel.getCount();    
    pSensorData->sensorReadingRangeCheck( count );
    errorCode = count;

    // getBackgndEventId returns BK_NO_EVENT if there is no Background
    // event to report or if it was previously reported
    rtnValue = pSensorData->getBackgndEventId();

    // Bypass this test if the battery option is not installed

    if ( BK_NO_EVENT == rtnValue && 
         bpsStatus != SystemBattery::NOT_INSTALLED )
    {
    // $[TI1]
        // Set up the pointer to the SafetyNetSensorData object
        // for the Battery Current
        pSensorData = RSystemBatteryCurrent.getSafetyNetSensorDataAddr();

        CLASS_PRE_CONDITION( pSensorData != NULL );

        // checks the BPS current to be within range
        // Read the raw counts and send it to the range check test
        AdcCounts count = RSystemBatteryCurrent.getCount();    
        pSensorData->sensorReadingRangeCheck( count );
        errorCode = count;

        // getBackgndEventId returns BK_NO_EVENT
        // if there is no Background
        // event to report or if it was previously reported
        rtnValue = pSensorData->getBackgndEventId();

        if ( BK_NO_EVENT == rtnValue )
        {
        // $[TI1.1]
            // Set up the pointer to the SafetyNetSensorData
            // object for the Battery Voltage
            pSensorData = RSystemBatteryVoltage.getSafetyNetSensorDataAddr();

            CLASS_PRE_CONDITION( pSensorData != NULL );

            // checks the BPS voltage to be within range
            // Read the raw counts and send it to the range check test
            AdcCounts count = RSystemBatteryVoltage.getCount();    
            pSensorData->sensorReadingRangeCheck( count );
            errorCode = count;

            // getBackgndEventId returns BK_NO_EVENT if there is no Background
            // event to report or if it was previously reported
            rtnValue = pSensorData->getBackgndEventId();
        }
        // $[TI1.2]
    }
    // $[TI2]

    return( rtnValue );
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  AdcTest
//
//@ Interface-Description
//  This method accepts Uint16 for error code and returns a BkEventName
//  enumeration. 
//  Performs a test of the ADC system by applying voltages to the DAC
//  and reading them back using the ADC.  Returns BK_NO_EVENT if
//  conditions have been satisfied, BK_ADC_FAIL_HIGH, or BK_ADC_FAIL_LOW
//  otherwise.
//  The purpose of the test is to check that ADC samples are not frozen, to
//  check the accuracy of the ADC measurements, to check the hamming code
//  generation in the system (FPGA), and to add diagnostics information in case
//  of a major failure.  Performs a test of the ADC system by applying voltages
//  to the DAC and reading them back using the ADC.  Record results in the
//  system information log for BK_LOOPBACK_FAIL.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Using the DacWrap and the DacWrapDacPort to write to the ADC and
//  read the test value and compare it to the high and low limits for
//  that test point.  Before a test value is written, the current
//  DacWrap register value is read and stored.  It then is compared
//  to the value read after the test value to written to the
//  DacWrapDacPort.  If the value are the same, it is determined that
//  the loop back mechanism in the ADC has failed.
//  Once a DACWrap reading is outside the limits, the test is repeated.
//  Four consecutive out of range readings will fail the test. 
//
//  $[06009] $[06010]
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================


BkEventName
BackgroundBdTests::AdcTest(  Uint16 &errorCode )
{

static const Uint8 NUM_ADC_TEST_POINTS = 3;
static const TestTableStruct adcTestTable[ NUM_ADC_TEST_POINTS ] =
{
    // Test Value      Min(V)      Max(V)
    // 0.5 Volts       0.4626V     0.5375V     
    // ADC Counts      Min Counts  Max Counts 
    // (ADC_TEST_POINT_1 + hamming code)
       ADC_TEST_POINT_1, 189,        220,

    // 5.0V            4.8920V     5.1095V      
    // (2047 + hamming code)
       51199,          2003,       2092,

    // 9.5V            9.3214V     9.6815V      
    // (3890 + hamming code)
       65330,          3817,       3965
};

    BkEventName rtnValue = BK_NO_EVENT;

    AdcCounts   count = 0;
    AdcCounts   newCount = 0;
    Uint16      testPoint = 0;
    Uint16      highLimit = 0;
    Uint16      lowLimit = 0;

#ifdef SIGMA_UNIT_TEST
Int32 UnitTestNum;
static Int32 testControl = 0;
Int32 apneaIntervalSetValue =
	 (Int32)(PhasedInContextHandle::GetBoundedValue(SettingId::APNEA_INTERVAL).value);
if (apneaIntervalSetValue == 50000)
	testControl = 1;
if (apneaIntervalSetValue == 51000 && testControl == 1)
	UnitTestNum = 1;
else if(apneaIntervalSetValue == 52000 && testControl == 1)
	UnitTestNum = 2;
else if(apneaIntervalSetValue == 53000 && testControl == 1)
	UnitTestNum = 3;
else if(apneaIntervalSetValue == 54000 && testControl == 1)
	UnitTestNum = 4;
else if(apneaIntervalSetValue == 55000 && testControl == 1)
	UnitTestNum = 5;
else if(apneaIntervalSetValue == 56000 && testControl == 1)
	UnitTestNum = 6;
else if(apneaIntervalSetValue == 57000 && testControl == 1)
	UnitTestNum = 7;
else if(apneaIntervalSetValue == 58000 && testControl == 1)
	UnitTestNum = 8;
else
	UnitTestNum = 0;
	
if ( UnitTestNum == 4)
{
    VentStatus::DirectVentStatusService(VentStatus::VENT_INOP_STATE, VentStatus::ACTIVATE);
}
#endif // SIGMA_UNIT_TEST

    if ( AdcTestFailed_ == FALSE )
    {
    // $[TI1]
        highLimit = adcTestTable[ AdcTestIndex_ ].highLimit;
        lowLimit = adcTestTable[ AdcTestIndex_ ].lowLimit;

        count = RDacWrap.getCount();

#ifdef SIGMA_UNIT_TEST
if ( AdcTestIndex_ == 0 && UnitTestNum == 1 )
{
   count = 188;
}
if ( AdcTestIndex_ == 2 && UnitTestNum == 2 )
{
   count = 3966;
}
// test 3 faults (should not fail):
if ( AdcTestIndex_ == 0 && UnitTestNum == 5 &&
       AdcFaultCounter_ >= 0 && AdcFaultCounter_ <= 2  )
{
   count = 188;
}
// test 4 faults (should fail):
if ( AdcTestIndex_ == 0 && UnitTestNum == 6 &&
       AdcFaultCounter_ >= 0 && AdcFaultCounter_ <= 3  )
{
   count = 188;
}
// test 3 faults (should not fail):
if ( AdcTestIndex_ == 2 && UnitTestNum == 7 &&
       AdcFaultCounter_ >= 0 && AdcFaultCounter_ <= 2  )
{
   count = 3966;
}
// test 4 faults (should fail):
if ( AdcTestIndex_ == 2 && UnitTestNum == 8 &&
       AdcFaultCounter_ >= 0 && AdcFaultCounter_ <= 3  )
{
   count = 3966;
}
#endif // SIGMA_UNIT_TEST

        errorCode = count | (AdcTestIndex_ << 12);

        if ( count < lowLimit )
        {
        // $[TI1.1]
            if ( ++AdcFaultCounter_ >= 4 )
            {
            // $[TI1.1.1]
                rtnValue = BK_ADC_FAIL_LOW;
            }// $[TI1.1.2]
        }
        else if ( count > highLimit )
        {
        // $[TI1.2]
            if ( ++AdcFaultCounter_ >= 4 )
            {
            // $[TI1.2.1]
                rtnValue = BK_ADC_FAIL_HIGH;
            } // $[TI1.2.2]
        }
        else
        {
        // $[TI1.3]
			AdcFaultCounter_ = 0;
			if (++AdcTestIndex_ == NUM_ADC_TEST_POINTS)
			{
            // $[TI1.3.1]
                AdcTestIndex_ = 0;
			} // $[TI1.3.2]
        }     

        // Write the test value to the DAC port
        testPoint = adcTestTable[ AdcTestIndex_ ].testValue; 
// E600 BDIO        RDacWrapDacPort.writeRegister( testPoint );

        // wait for SubMux channels to be updated
        Task::Delay( 0, MAX_ADC_SUB_MUX_CYCLE_TIME );
    
        // retrieve new DAC wrap value
        newCount = RDacWrap.getCount();

#ifdef SIGMA_UNIT_TEST
if ( UnitTestNum == 3)
{
    count = newCount;
}
#endif // SIGMA_UNIT_TEST

        if ( count == newCount ) 
        {
        // $[TI1.4]
            Background::LogDiagnosticCodeUtil(::BK_ADC_LOOPBACK_FAIL,
                errorCode);
        } // $[TI1.5]

        if ( rtnValue != BK_NO_EVENT )
        {
        // $[TI1.6]
            AdcTestFailed_ = TRUE;
        }
        // $[TI1.7]
    }
    return( rtnValue );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  AcSwitchPositionTest
//
//@ Interface-Description
//  This method accepts Uint16 for error code and returns a BkEventName
//  enumeration.
//
//  Tests the AC switch to determine if it is stuck closed.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This maintains an internal counter of the number of consectutive
//  AC switch cycles that indicate the switch is open (OFF).  If the
//  battery is present this switch should still be closed.  A single
//  event is generated subsequent detections are not reported.
//
//  $[06064]
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

BkEventName
BackgroundBdTests::AcSwitchPositionTest(  Uint16 &errorCode )
{
    BkEventName rtnStatus = BK_NO_EVENT;
    Uint16 acSwitchStatus = 0; // E600 BDIO RReadIOPort.getRegisterImage();

    if ( FALSE == AcSwitchPositionTestFailed_ )
    {
    // $[TI1]

        // check switch to see if it is open or close
        if ( acSwitchStatus & AI_PWR_SWITCH_POSITION_MASK )
        {
        // $[TI1.1]
            NumberAcSwitchCycles_ = 0;
        }
        else
        {
        // $[TI1.2]
            ++NumberAcSwitchCycles_;
        }

        // allow up to two reads of open before declaring fault
        if ( NumberAcSwitchCycles_ >= 2 )
        {
        // $[TI1.3]
            // prevent subsequent events
            AcSwitchPositionTestFailed_ = TRUE;
            rtnStatus = BK_AC_SWITCH_STUCK;
            errorCode = acSwitchStatus;
        }
        // $[TI1.4]
    }
    // $[TI2]

    return ( rtnStatus );
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  NovRamChecksumTest
//
//@ Interface-Description
//  This method accepts Uint16 for error code and returns a BkEventName
//  enumeration.
//
//  Determines if the NOVRAM checksums are valid.
//  This uses the NovRamManager utility to determine if checksums
//  are valid.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls NovRamManager class to perform a self-verification.
//  $[06027] $[00524] $[00528]  
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

BkEventName
BackgroundBdTests::NovRamChecksumTest(  Uint16 &errorCode )
{
    BkEventName rtnStatus = BK_NO_EVENT;

    if ( FALSE == NovRamChecksumTestFailed_ )
    {
    // $[TI1]
        if ( NovRamManager::VerifySelf( errorCode ) != NovRamStatus::ALL_ITEMS_VALID )
        {
        // $[TI1.1]
            NovRamChecksumTestFailed_ = TRUE;
            rtnStatus = BK_BD_NOVRAM_CHECKSUM;
        }
        // $[TI1.2]
    }
    // $[TI2]

#ifdef SIGMA_INTEGRATION
    if (TestTrigger::TriggerOnRequirementCheck( 6027))
        return BK_BD_NOVRAM_CHECKSUM; 
#endif //SIGMA_INTEGRATION


    return( rtnStatus );
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  TodTest
//
//@ Interface-Description
//  This method accepts Uint16 for error code and returns a BkEventName
//  enumeration.
//
//  Tests the wall clock time of day.  Returns BK_BD_TOD_FAIL if
//  clock fails, BK_NO_EVENT otherwise.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The time of day clock is tested by reading the current time and 
//  comparing it to a previously saved timestamp.  This test should
//  run in a 10 second cycle, the comparison limits are set large
//  to account for the low priority of this task.  The validity of a
//  timestamp is automatically checked in the TimeStamp class.
//
//  When the clocks are set by the operator or resynchronized
//  periodically this test is bypassed by if CheckIfTimeHasChanged
//  determines the clocks may be mismatched.
//
//  $[06125]
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

BkEventName
BackgroundBdTests::TodTest(  Uint16 &errorCode )
{
    BkEventName rtnValue = BK_NO_EVENT;

    if ( FALSE == TodTestFailed_ )
    {
    // $[TI1]

        if ( ( FALSE == TodTestInitialized_ ) ||
             ( TRUE == Background::CheckIfTimeHasChanged() ) )
        {
        // $[TI1.1]
            CurrentTime_.now();  // get the current time
            PreviousTime_.now();  // get the current time

            TodTestInitialized_ = TRUE;
        }
        else
        {
        // $[TI1.2]

            CurrentTime_.now();  // get the current time

            if ( ( ( (CurrentTime_ - PreviousTime_) > MAX_TIME_DIFFERENCE )
               ||  ( PreviousTime_ >= CurrentTime_ ) )
#ifdef SIGMA_INTEGRATION
               || (TestTrigger::TriggerOnRequirementCheck( 6125 ))
#endif //SIGMA_INTEGRATION
               )
            {
            // $[TI1.2.1]
                TodTestFailed_ = TRUE;
                rtnValue = BK_BD_TOD_FAIL;
                errorCode = (PreviousTime_ >= CurrentTime_) ? 1:2 ;
            }
            // $[TI1.2.2]

            // Unconditionally replace the previous time with new time
            PreviousTime_ = CurrentTime_;
        }
    }
    // $[TI2]

    return( rtnValue );
}

#endif // BackgroundBdTests_HH


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//  and 'lineNumber' are essential pieces of information.  The
//  'pFileName' and 'pPredicate' strings may be defaulted in the macro
//  to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//  None 
//---------------------------------------------------------------------
//@ PostCondition 
//  None 
//@ End-Method 
//===================================================================== 

void
BackgroundBdTests::SoftFault( const SoftFaultID  softFaultID,
                              const Uint32       lineNumber,
                              const char*        pFileName,
                              const char*        pPredicate )
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, SAFETY_NET, BACKGROUNDBDTESTS,
                          lineNumber, pFileName, pPredicate);
}
