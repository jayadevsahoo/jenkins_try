#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-2007, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: TimePlot - A drawable class that can plot a specified 
// stream of waveform data against time for use in the Waveforms subscreen.
//---------------------------------------------------------------------
//@ Interface-Description
// The purpose of TimePlot is to display a waveform of a specified patient data
// stream (such as pressure or flow) against time.  The plot is displayed on a
// grid whose X and Y axes are numbered with the appropriate units.  Successive
// data points are connected to the previous data point by a line thus forming
// a continuous waveform.  The waveform display is limited to the area of the
// grid with out-of-range plot points simply not being displayed.
//
// This class derives from WaveformPlot which handles the drawing of the grid
// (and the various labels of the grid) on which the waveform is plotted.
// TimePlot is devoted purely to the plotting of the actual waveform.
//
// Before display of a time plot the setYUnits() method must be called.  This
// method informs TimePlot which waveform data stream it should plot, e.g.
// pressure, volume or flow.
//
// Typically a time plot is plotted in segments via successive calls to
// the update() method.  endPlot() is called when the full waveform has
// been plotted allowing us to complete drawing of the waveform.  erase()
// erases the waveform and the activate() method should always be called
// before TimePlot is to be displayed.
//---------------------------------------------------------------------
//@ Rationale
// This class gathers all the functionality for displaying patient data
// against time in the one place.
//---------------------------------------------------------------------
//@ Implementation-Description
// The basic functionality of TimePlot is contained in the update() method
// which is responsible for almost all the waveform plotting.  See update()
// for more information.
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and pre-conditions
// which verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/TimePlot.ccv   25.0.4.0   19 Nov 2013 14:08:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 014   By: gdc    Date:  26-May-2007    SCR Number: 6330
//  Project:  Trend
//  Description:
//      Removed SUN prototype code.
//
//  Revision: 013   By: gdc    Date:  09-May-2007    SCR Number: 6237
//  Project:  Trend
//  Description:
//      Modified to show/hide direct draw graphics for modified handling
//      of visibility in Drawable class.
//
//  Revision: 012   By: erm    Date:  23-Apr-2002    DCS Number: 5848
//  Project:  VCP
//  Description:
//      Merge PAV into VCP
//
//  Revision: 011   By: sah    Date:  22-Jan-2001    DCS Number: 5835, 5805, 5848
//  Project:  PAV
//  Description:
//      *  no longer display Plung shadow trace during Apnea, Safety-PCV
//         and OSC
//      *  added enable/disable capability for the shadow traces
//
//  Revision: 010   By: sah    Date:  22-Nov-2000    DCS Number: 5798
//  Project:  PAV
//  Description:
//      No longer displaying lung volume and lung flow shadow traces.
//
//  Revision: 009   By: sah    Date:  25-Oct-2000    DCS Number: 5776
//  Project:  VTPC
//  Description:
//      Changed to not display shadow trace of lung volume in BILEVEL mode.
//
//  Revision: 008   By: sah    Date:  08-Sep-2000    DCS Number: 5313
//  Project:  PAV
//  Description:
//      PAV project-related changes:
//      *  added support for a shadow waveform trace of lung pressure
//         (PA only), and its label
//
//  Revision: 007   By: sah    Date:  11-Jul-2000    DCS Number: 5755
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      *  added support for shadow waveform traces of carinal pressure
//         (TC only), lung flow and lung volume, and their labels
//
//  Revision: 006   By: gdc    Date:  05-Aug-1999    DCS Number: 5467
//  Project: 840 Neonatal
//  Description:
//      endPlot() plotting removed - caused spurious plot lines and
//      is not needed with the current update() method.
//
//  Revision: 005   By: gdc    Date:  29-Jun-1999    DCS Number: 5327
//  Project: 840 Neonatal
//  Description:
//	Optimized graphics library. Implemented Direct Draw mode for drawing
//  outside of BaseContainer refresh cycle.
//
//  Revision: 004  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 003  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 002    By: sah    Date: 08-Oct-1997    DCS Number: 2264
//  Project:  Sigma (R8027)
//  Description:
//      Fixed incorrect handling of VGA access requests/releases, which
//      may have caused the problem with the errant lines outside of the
//      waveform grid.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//  Project:  Sigma (R8027)
//  Description:
//      Integration baseline.
//
//=====================================================================

#include "TimePlot.hh"
#include <math.h>
#include <string.h>
#include "MiscStrs.hh"
#include "ModeValue.hh"
#include "SupportTypeValue.hh"
#include "ShadowTraceEnableValue.hh"
#include "Plot1TypeValue.hh"

//@ Usage-Classes
#include "BreathPhaseType.hh"
#include "BreathType.hh"
#include "WaveformIntervalIter.hh"
#include "SettingContextHandle.hh"
#include "BdGuiEvent.hh"
//@ End-Usage

//@ Code...
static const Real32 X_SAMPLE_CONV_FACTOR = Real32(MSECS_PER_WAVEFORM_SAMPLE) / 1000;

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TimePlot  [Constructor]
//
//@ Interface-Description
// Default constructor for TimePlot.
//---------------------------------------------------------------------
//@ Implementation-Description
// Initializes all data members.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

TimePlot::TimePlot(const Boolean isUpperPlot) :
					WaveformPlot(isUpperPlot),
					yUnits_(PatientDataId::NULL_PATIENT_DATA_ITEM),
					currX_(0),
					lastX_(0),
					sampleNum_(0),
					lastSample_(0.0),
					lastAuxSample_(0.0),
					minEnvelopeSample_(0.0),
					maxEnvelopeSample_(0.0),
					isLastPointSet_(FALSE),
					isShadowDrawn_(FALSE),
					envelopingSamples_(FALSE),
					lineColor_(Colors::GREEN),
					currPhaseType_(BreathPhaseType::INSPIRATION),
					prevPhaseType_(BreathPhaseType::INSPIRATION)
{
	CALL_TRACE("TimePlot::TimePlot(void)");				// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~TimePlot  [Destructor]
//
//@ Interface-Description
// Destroys TimePlot.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

TimePlot::~TimePlot(void)
{
	CALL_TRACE("TimePlot::TimePlot(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activate
//
//@ Interface-Description
// Activates a TimePlot.  Should be called just before display of the plot.
//---------------------------------------------------------------------
//@ Implementation-Description
// Call the base class activate() method and call erase() to do some
// initializing.
//---------------------------------------------------------------------
//@ PreCondition
// The TimePlot should already be tied to a particular Y unit via a call to
// setYUnits().
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
TimePlot::activate(void)
{
	CLASS_PRE_CONDITION(PatientDataId::NULL_PATIENT_DATA_ITEM != yUnits_);

	WaveformPlot::activate();
	erase();											// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: update
//
//@ Interface-Description
// Draws successive segments of a waveform plot.  It is passed a reference to a
// WaveformIntervalIter.  This specifies the interval of waveform data that
// must be plotted.
//---------------------------------------------------------------------
//@ Implementation-Description
// If this method is called while this drawable is not visible then the call
// is ignored.
//
// The main difficulty in drawing the specified segment is the case where,
// due to a long time scale, there will be more waveform samples than there
// are pixels to plot them in.  In this case the algorithm below is designed
// to average successive samples that would be plotted at the same X
// coordinate and to plot these averaged Y values rather than plotting
// exact Y values as would be the situation if there are less samples than
// pixels to plot them in.
// 
// $[01152] Data shall be graphed as an XY plot with a positive X ....
// $[VC01001] When TC, carinal pressure shall be displayed, superimposed...
// $[VC01002] Lung flow shall be displayed, superimposed...
// $[VC01003] Lung volume shall be displayed, superimposed...
// $[PA01000] When PA, lung pressure shall be displayed, superimposed...
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
TimePlot::update(WaveformIntervalIter& segmentIter)
{
	CALL_TRACE("TimePlot::update(WaveformIntervalIter& segmentIter)");

	Boolean	displayLocked = FALSE;  // TRUE when we have the semaphore

	// Ignore call if not visible
	if (!isVisible())
	{													// $[TI1]
		return;
	}													// $[TI2]

	if (auxYLabelBox_.isVisible() != isShadowDrawn_)
	{													// $[TI30]
		auxYLabelBox_.setShow(isShadowDrawn_);
	}													// $[TI31]

	// Go through each sample in the segment
	for( SigmaStatus status = segmentIter.goFirst(); 
		  		SUCCESS == status;
	    status = segmentIter.goNext() )
	{													// $[TI6]
		// Get the breath phase of the current sample
		currPhaseType_ = (BreathPhaseType::PhaseType)segmentIter.
				getDiscreteValue(PatientDataId::BREATH_PHASE_ITEM);

		BreathType  currBreathType = (BreathType)segmentIter.
				getDiscreteValue(PatientDataId::BREATH_TYPE_ITEM);

		const DiscreteValue  MODE_VALUE =
			segmentIter.getDiscreteValue(PatientDataId::MODE_SETTING_ITEM);
		const DiscreteValue  SPONT_TYPE_VALUE =
			segmentIter.getDiscreteValue(PatientDataId::SUPPORT_TYPE_SETTING_ITEM);
		// a shadow trace is not allowed during apnea, an "error state" (e.g.,
		// OSC, disconnect, etc.) and during PEEP recovery...
		const Boolean  IS_SHADOW_ALLOWED =
		  (!segmentIter.getDiscreteValue(PatientDataId::IS_APNEA_ACTIVE_ITEM) &&
		   !segmentIter.getDiscreteValue(PatientDataId::IS_ERROR_STATE_ITEM)  &&
		   !segmentIter.getDiscreteValue(PatientDataId::IS_PEEP_RECOVERY_ITEM));

		Boolean  checkShadowState = FALSE;

		if ( prevPhaseType_ != currPhaseType_ )
		{
                        // this is used by Vital Patient Data Area for leaving the breath
			// bar blank during a PEEP-recovery breath, whereas the waveform,
			// using 'BREATH_TYPE_ITEM', will show a spontaneous breath...
			const Boolean  IS_PEEP_RECOVERY =
				segmentIter.
					getDiscreteValue(PatientDataId::IS_PEEP_RECOVERY_ITEM);

      										// $[TI6.1]
			// must use turn off DirectDraw feature...
			if ( displayLocked )
			{									// $[TI6.1.1]
				setDirectDraw(FALSE);
				displayLocked = FALSE;
			}									// $[TI6.1.2]

			switch ( currPhaseType_ )
			{
			case BreathPhaseType::EXHALATION:	// $[TI6.1.3]
				lineColor_ = Colors::YELLOW;

				// when starting off drawing the plot and not a PEEP recovery
				// breath, make sure shadow state is correctly set...
				checkShadowState = (!isLastPointSet_  &&  IS_SHADOW_ALLOWED);
				break;
			case BreathPhaseType::INSPIRATION:	// $[TI6.1.4]
				switch ( currBreathType )
				{
				case CONTROL:
				case ASSIST:					// $[TI6.1.4.1]
					lineColor_ = Colors::GREEN;
					break;
				case SPONT:
				default:						// $[TI6.1.4.2]
					lineColor_ = Colors::GOLDENROD;
					break;
				}

				if (!IS_SHADOW_ALLOWED)
				{								// $[TI6.1.4.3]
					// don't display any shadow traces...
					activeAuxYUnits_ = PatientDataId::NULL_PATIENT_DATA_ITEM;
				}
				else
				{								// $[TI6.1.4.4]
					// make sure any needed shadow traces are drawn...
					checkShadowState = TRUE;
				}
				break;
			case BreathPhaseType::EXPIRATORY_PAUSE:
			case BreathPhaseType::INSPIRATORY_PAUSE:
			case BreathPhaseType::BILEVEL_PAUSE_PHASE:
			case BreathPhaseType::PAV_INSPIRATORY_PAUSE:
			case BreathPhaseType::NON_BREATHING:
			default:							// $[TI6.1.5]
				lineColor_ = Colors::WHITE;

				if (currPhaseType_ == BreathPhaseType::NON_BREATHING  &&
					activeAuxYUnits_ != PatientDataId::NULL_PATIENT_DATA_ITEM)
				{								// $[TI6.1.5.1]
					// don't display any shadow traces during a non-
					// breathing phase...
					activeAuxYUnits_ =
								PatientDataId::NULL_PATIENT_DATA_ITEM;
				}								// $[TI6.1.5.2]
				break;
			}

			prevPhaseType_ = currPhaseType_;
		}
		else if (!isLastPointSet_)
		{										// $[TI6.2]
			// we're starting the drawing in the middle of a breath, therefore
			// make sure shadow data correctly matches plot's data...
			checkShadowState = TRUE;
		}

		const DiscreteValue  SHADOW_TRACE_ENABLE_VALUE =
	      SettingContextHandle::GetSettingValue(ContextId::ACCEPTED_CONTEXT_ID,
											    SettingId::SHADOW_TRACE_ENABLE);

		if (checkShadowState)
		{										// $[TI7]
		    if (IS_SHADOW_ALLOWED  &&
				SHADOW_TRACE_ENABLE_VALUE ==
										ShadowTraceEnableValue::ENABLE_SHADOW)
			{	// $[TI7.1] -- shadow trace is enabled...
				const DiscreteValue  PLOT1_TYPE_VALUE =
					   SettingContextHandle::GetSettingValue(
												 ContextId::ACCEPTED_CONTEXT_ID,
												 SettingId::PLOT1_TYPE
															);
				switch (yUnits_)
				{
				case PatientDataId::NET_FLOW_ITEM :
				case PatientDataId::NET_VOL_ITEM :			// $[TI7.1.1]
					// no shadow trace to be drawn...
					activeAuxYUnits_ = PatientDataId::NULL_PATIENT_DATA_ITEM;
					break;
				case PatientDataId::CIRCUIT_PRESS_ITEM :	// $[TI7.1.2]
					if (MODE_VALUE != ModeValue::AC_MODE_VALUE  &&
						PLOT1_TYPE_VALUE !=
										Plot1TypeValue::PRESSURE_VS_VOLUME  &&
						(SPONT_TYPE_VALUE ==
										SupportTypeValue::ATC_SUPPORT_TYPE  ||
						 SPONT_TYPE_VALUE ==
										SupportTypeValue::PAV_SUPPORT_TYPE))
					{									// $[TI7.1.2.1]
						// set to LUNG PRESSURE shadow trace...
						activeAuxYUnits_ = PatientDataId::LUNG_PRESS_ITEM;

						if (SPONT_TYPE_VALUE ==
									SupportTypeValue::ATC_SUPPORT_TYPE  &&
							0 != wcscmp(MiscStrs::CARINAL_PRESSURE_LABEL,
															auxYLabelText_))  
						{								// $[TI7.1.2.1.1]
							auxYLabelText_.setText(
											MiscStrs::CARINAL_PRESSURE_LABEL
												  );
							auxYLabelText_.setMessage(
											MiscStrs::CARINAL_PRESSURE_MSG
													 );
							auxYLabelText_.positionInContainer(
															GRAVITY_CENTER
															  );
						}
						else if (SPONT_TYPE_VALUE ==
									SupportTypeValue::PAV_SUPPORT_TYPE  &&
								 0 != wcscmp(MiscStrs::LUNG_PRESSURE_LABEL,
																auxYLabelText_))  
						{								// $[TI7.1.2.1.2]
							auxYLabelText_.setText(
											MiscStrs::LUNG_PRESSURE_LABEL
												  );
							auxYLabelText_.setMessage(
											MiscStrs::LUNG_PRESSURE_MSG
													 );
							auxYLabelText_.positionInContainer(
															GRAVITY_CENTER
															  );
						}								// $[TI7.1.2.1.3]

						isShadowDrawn_ = TRUE;
					}
					else
					{									// $[TI7.1.2.2]
						// no shadow trace to be drawn...
						activeAuxYUnits_ =
									PatientDataId::NULL_PATIENT_DATA_ITEM;
					}
					break;
				default :
					// no shadow trace to be drawn...
					activeAuxYUnits_ = PatientDataId::NULL_PATIENT_DATA_ITEM;
					break;
				}
			}
			else
			{	// $[TI7.2] -- shadow trace is disabled...
				// no shadow trace to be drawn...
				activeAuxYUnits_ = PatientDataId::NULL_PATIENT_DATA_ITEM;
			}
		}											// $[TI8]

		if (auxYLabelBox_.isVisible() != isShadowDrawn_)
		{											// $[TI9]
			auxYLabelBox_.setShow(isShadowDrawn_);
		}											// $[TI10]

		if ( currBreathType == NON_MEASURED )
		{											// $[TI11]
			lineColor_ = Colors::WHITE;
		}											// $[TI12]

		// Get the value of the current sample (it'll be a bounded value)
		Real32  currSampleValue = segmentIter.getBoundedValue(yUnits_);

		// must be set AFTER logic above that determines whether an "active"
		// auxillary shadow is appropriate, or not...
		Real32	currAuxSampleValue = segmentIter.getBoundedValue(activeAuxYUnits_);

		// Calculate the X pixel at which the current sample should be
		// plotted (the sample number and the fact that a sample is
		// measured every 20 milliseconds gives us the time that the sample
		// occurred at relative to the start of the waveform and we use
		// this value to calculate the X pixel).
        //   Since it is possible to loose waveform data, this scheme
        //   results in strange looking waveforms when data is missing.
        //   A better mechanism would use a TimeStamp mechanism to tag
        //   each sample to a particular time and plot it accordingly.
		currX_ = xValueToPoint_(sampleNum_ * X_SAMPLE_CONV_FACTOR);

		if (sampleNum_ == 0)
		{											// $[TI13]
			lastX_ = currX_;
		}											// $[TI14]

		// Check to see if we advanced to a new X coordinate (often successive
		// samples will end up with the same X coordinate when the time scale
		// is large, e.g. when plotting a 48 second or 2400 sample waveform
		// on the waveform grid).
        if (currX_ != lastX_ )
        {											// $[TI15]
		    // We advanced

			// Got a line segment so let's plot it.  Request access to the
			// the display if we haven't previously asked for it.

			if ( !displayLocked )
			{										// $[TI16]
				setDirectDraw(TRUE);
				displayLocked = TRUE;
				auxPlotLine_.setShow(TRUE);
				plotLine_.setShow(TRUE);
			}										// $[TI17]

			//=========================================================
			// NOTE:  it is important to remember that Y values increase
			//        as you progress DOWN the screen (i.e., 5 is closer
			//        to the top of the display than 105, and a minimum
			//        of two 'y' values results in a "higher" point).
			//=========================================================

			const Int32  ZERO_Y = yValueToPoint_(0.0f);

            //  If we've enveloped samples at a particular X value, we
            //  will plot that "histogram" first.
            if ( envelopingSamples_ )
            {										// $[TI18]
				if (activeAuxYUnits_ != PatientDataId::NULL_PATIENT_DATA_ITEM)
				{									// $[TI18.1]
					//-----------------------------------------------------
					// We're supposed to draw an auxillary histogram (column),
					// so first determine if, and where, I need a column with
					// respect to the X-axis line, then draw it...
					//-----------------------------------------------------

					// convert the auxillary "envelope" samples to pixel
					// locations...
					const Int32  MIN_AUX_Y =
										yValueToPoint_(minAuxEnvelopeSample_);
					const Int32  MAX_AUX_Y =
										yValueToPoint_(maxAuxEnvelopeSample_);

					// to ensure that no previously-drawn lines are overwritten
					// by the shadow trace, the "last X" can't be used...
					const Int32  COL_X = (lastX_ + 1);

					if (MIN_AUX_Y < ZERO_Y)
					{								// $[TI18.1.1]
						//-------------------------------------------------
						// At least part of the column is ABOVE the X-axis...
						//-------------------------------------------------
						auxPlotLine_.setVertices(COL_X, MIN_AUX_Y,
												 COL_X, (ZERO_Y-1));
						auxPlotLine_.drawDirect();
					}								// $[TI18.1.2]

					if (MAX_AUX_Y > ZERO_Y)
					{								// $[TI18.1.3]
						//-------------------------------------------------
						// At least part of the column is BELOW the X-axis...
						//-------------------------------------------------
						auxPlotLine_.setVertices(COL_X, (ZERO_Y+1),
												 COL_X, MAX_AUX_Y);
						auxPlotLine_.drawDirect();
					}								// $[TI18.1.4]
				}									// $[TI19]

				// convert the "envelope" samples to pixel locations...
                const Int32  MIN_Y = yValueToPoint_(minEnvelopeSample_);
                const Int32  MAX_Y = yValueToPoint_(maxEnvelopeSample_);

				plotLine_.setVertices( lastX_, MIN_Y, lastX_, MAX_Y );
				plotLine_.setColor( lineColor_ );
				plotLine_.drawDirect();
            }										// $[TI20]

            // Now we plot a diagonal line between the last X axis point
            // and the current one.
			Int32  currY;
			Int32  lastY;

			if (activeAuxYUnits_ != PatientDataId::NULL_PATIENT_DATA_ITEM)
			{									// $[T21]
				lastY = yValueToPoint_(lastAuxSample_);
				currY = yValueToPoint_(currAuxSampleValue);

				const Int32  DELTA_Y = (currY - lastY);

				// NOTE:  no lines are to be drawn on the column at 'lastX_'
				//        therefore there is no need for a "+1" operand in
				//        this calculation...
				const Int32  NUM_LINES_NEEDED = (currX_ - lastX_);

				// how much 'y' change per line...
				const Int32  LINE_DELTA = (DELTA_Y / NUM_LINES_NEEDED);

				const Boolean  IS_LAST_ABOVE = (lastY < ZERO_Y);
				const Boolean  IS_CURR_ABOVE = (currY < ZERO_Y);

				for (Uint lineNum = 0u; lineNum < NUM_LINES_NEEDED;
						lineNum++)
				{
					const Int32  COL_X = (currX_ - lineNum);
					const Int32  COL_Y = (currY - (lineNum * LINE_DELTA));

					if (COL_Y < ZERO_Y)
					{	// $[TI21.1] -- line is fully above X-axis...
						auxPlotLine_.setVertices(COL_X, COL_Y,
												 COL_X, (ZERO_Y-1));
						auxPlotLine_.drawDirect();
					}
					else if (COL_Y > ZERO_Y)
					{	// $[TI21.2] -- line is fully below X-axis...
						auxPlotLine_.setVertices(COL_X, (ZERO_Y+1),
												 COL_X, COL_Y);
						auxPlotLine_.drawDirect();
					}
					else  // (COL_Y == ZERO_Y)
					{	// $[TI21.3] -- line is fully on X-axis...
						if (IS_LAST_ABOVE != IS_CURR_ABOVE)
						{	// $[TI21.3.1] -- trace must cross X-axis...
							// this X is where we cross the X-axis,
							// therefore draw a single pixel ("dot")
							// above or below the X-axis so that this
							// column is visible...
							Int32  deltaAbove;
							Int32  deltaBelow;

							if (IS_LAST_ABOVE)
							{	// $[TI21.3.1.1]
								deltaAbove = (ZERO_Y - lastY);
								deltaBelow = (currY - ZERO_Y);
							}
							else  // (IS_CURR_ABOVE)...
							{	// $[TI21.3.1.2]
								deltaAbove = (ZERO_Y - currY);
								deltaBelow = (lastY - ZERO_Y);
							}

							// determine 'y' of "dot"...
							const Int32  DOT_Y =
											(deltaAbove > deltaBelow)
											 ? (ZERO_Y - 1)	// $[TI21.3.1.3]
											 : (ZERO_Y + 1);// $[TI21.3.1.4]

							auxPlotLine_.setVertices(COL_X, DOT_Y,
													 COL_X, DOT_Y);
							auxPlotLine_.drawDirect();
						}	// $[TI21.3.2]
					}
				}	// end of 'for (Uint lineNum = 0u; ...'
			}									// $[TI22]

			lastY = yValueToPoint_(lastSample_);
			currY = yValueToPoint_(currSampleValue);

			plotLine_.setVertices( lastX_, lastY, currX_, currY );
			plotLine_.setColor( lineColor_ );
			plotLine_.drawDirect();
    
			// if a higher priority task is waiting for the display,
			// unlock the display to yield to the higher priority task
			if ( isDisplayLockPending() )
			{ 	// $[TI23]
				auxPlotLine_.setShow(FALSE);
				plotLine_.setShow(FALSE);
				setDirectDraw(FALSE);
				displayLocked = FALSE;
			}  // $[TI24]

			// reset the envelope
			envelopingSamples_ = FALSE;
        }
        else if (sampleNum_ > 0)
        {												// $[TI25]
            // The X-axis point has not moved so "envelope" samples at
            // this point.
            envelopingSamples_ = TRUE;

            minEnvelopeSample_ = MIN_VALUE(currSampleValue, minEnvelopeSample_);
            maxEnvelopeSample_ = MAX_VALUE(currSampleValue, maxEnvelopeSample_);

            minAuxEnvelopeSample_ = MIN_VALUE(currAuxSampleValue,
											  minAuxEnvelopeSample_);
            maxAuxEnvelopeSample_ = MAX_VALUE(currAuxSampleValue,
											  maxAuxEnvelopeSample_);
        }

        //  Set the min/max envelope samples to the current sample if
        //  we're not already enveloping samples
        if ( !envelopingSamples_ )
        {												// $[TI26]
            minEnvelopeSample_ = currSampleValue;
            maxEnvelopeSample_ = currSampleValue;

            minAuxEnvelopeSample_ = currAuxSampleValue;
            maxAuxEnvelopeSample_ = currAuxSampleValue;
        }												// $[TI27]

        isLastPointSet_ = TRUE;
        lastX_          = currX_;
        lastSample_     = currSampleValue;
        lastAuxSample_  = currAuxSampleValue;
 
		// Increment our sample count
		sampleNum_++;
	}

	// If we have access to the display we must release it.
	if ( displayLocked )
	{													// $[TI28]
		auxPlotLine_.setShow(FALSE);
		plotLine_.setShow(FALSE);
		setDirectDraw(FALSE);
	}													// $[TI29]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: endPlot
//
//@ Interface-Description
// This is called at the end of drawing a complete waveform plot and it allows
// the derived plot us to do any tidy up needed to complete the waveform
// drawing.
//---------------------------------------------------------------------
//@ Implementation-Description
// Does nothing.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
TimePlot::endPlot(void)
{
	CALL_TRACE("TimePlot::endPlot(void)");
	// do nothing
	// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: erase
//
//@ Interface-Description
// Erases a time plot.
//---------------------------------------------------------------------
//@ Implementation-Description
// Call the base class's method first and then initialize data members to
// indicate that the plot has been erased and we'll be starting anew if
// update() is called next. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
TimePlot::erase(void)
{
	CALL_TRACE("TimePlot::erase(void)");

	WaveformPlot::erase();

	isLastPointSet_ = FALSE;
	isShadowDrawn_ = FALSE;
    envelopingSamples_ = FALSE;
	lastX_ = currX_ = xValueToPoint_(0.0);
	sampleNum_ = 0;										// $[TI1]
	auxYLabelBox_.setShow(FALSE);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
TimePlot::SoftFault(const SoftFaultID  softFaultID,
		   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, TIMEPLOT,
									lineNumber, pFileName, pPredicate);
}
