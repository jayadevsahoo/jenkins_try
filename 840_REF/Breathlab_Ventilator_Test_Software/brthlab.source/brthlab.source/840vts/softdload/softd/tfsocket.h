// TfSocket.h : header file
//
// tfsocket.h
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TFSOCKET_H__607BB36C_026F_11D1_9F84_00805FCAC1CC__INCLUDED_)
#define AFX_TFSOCKET_H__607BB36C_026F_11D1_9F84_00805FCAC1CC__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
#include "BPServer.h"

// forward declaration

class BootPServer;

// end declaration


/////////////////////////////////////////////////////////////////////////////
// TftpSocket command target

class TftpSocket : public CAsyncSocket
{
// Attributes
private:
	BootPServer * bpServer;

// Operations
public:
	TftpSocket();
	TftpSocket(BootPServer *server);
	virtual ~TftpSocket();

// Overrides
public:
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(TftpSocket)
	public:
	virtual void OnAccept(int nErrorCode);
	//}}AFX_VIRTUAL

	// Generated message map functions
	//{{AFX_MSG(TftpSocket)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

// Implementation
protected:
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TFSOCKET_H__607BB36C_026F_11D1_9F84_00805FCAC1CC__INCLUDED_)
