#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: ExhFlowSensor_t - Implements the expiratory flow sensors.
//---------------------------------------------------------------------
//@ Interface-Description
//      
//---------------------------------------------------------------------
//@ Rationale
//      
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ Fault-Handling
//		n/a
//---------------------------------------------------------------------
//@ Restrictions
//      None
//---------------------------------------------------------------------
//@ Invariants
//      None
//---------------------------------------------------------------------
//@ End-Preamble
//
//*****************************************************************************
//
//  ExhFLowSensor.cpp - Implementation of the ExhFLowSensor class.
//
//*****************************************************************************

#include "ExhFlowSensor.h"
#include "ExhCommunicationInterface.h"
#include "AioDioDriver.h"
#include "UtilityFunctions.h"
//TODO E600_LL: to review to determine how to get calibration table
#include "CalibrationTable.h"
#include "ExhFlowSensorCalibrationTable.h"
#include "BDIORefs.hh"
#include "CalInfoRefs.hh"
#include "CalInfoFlashBlock.hh"
#include "FlowSensorOffset.hh"

#define EXH_COMM_THREAD_DEBUG_OUTPUT (0)

#define MAX_FEX_ERR_COUNT			3

static Flow_t DefaultFexFlowTab_12bit[EXH_FLOW_SUBTABLE_SIZE] =
{ 0.00, 0.21, 0.41, 0.62, 0.82, 1.01, 1.23, 1.41, 1.63, 1.82,
2.02, 2.23, 2.44, 2.62, 2.83, 3.06, 3.25, 3.44, 3.64, 3.80,
4.03, 4.24, 4.46, 4.57, 4.89, 5.06, 5.21, 5.43, 5.60, 5.91,
6.09, 6.28, 6.47, 6.57, 6.86, 7.12, 7.31, 7.47, 7.66, 7.83,
8.03, 8.29, 8.45, 8.65, 8.82, 9.04, 9.25, 9.42, 9.75, 9.85,
10.13, 12.40, 14.18, 16.32, 18.11, 20.31, 22.18, 24.13, 26.42, 28.14,
30.35, 32.32, 34.52, 36.18, 38.29, 40.69, 42.61, 44.29, 46.46, 48.53,
50.64, 55.83, 60.33, 66.04, 70.15, 76.38, 80.88, 85.54, 90.93, 95.33,
100.73, 105.55, 112.07, 116.46, 121.24, 126.19, 130.09, 135.84, 139.99, 144.78,
150.11, 155.90, 159.63, 166.15, 171.87, 177.54, 181.74, 186.93, 191.67, 197.67,
201.56 };

static float DefaultFexDacTab_12bit[EXH_FLOW_SUBTABLE_SIZE] =
{ 235, 312, 385, 447, 492, 530, 571, 602, 638, 663,
688, 713, 737, 756, 779, 801, 819, 837, 855, 869,
886, 901, 917, 926, 949, 961, 971, 984, 996, 1013,
1022, 1034, 1044, 1051, 1065, 1080, 1090, 1098, 1108, 1115,
1125, 1137, 1146, 1156, 1164, 1173, 1182, 1191, 1204, 1209,
1221, 1306, 1365, 1430, 1485, 1544, 1592, 1639, 1691, 1729,
1769, 1817, 1860, 1891, 1928, 1968, 1996, 2022, 2054, 2086,
2116, 2185, 2243, 2296, 2340, 2400, 2440, 2480, 2525, 2562,
2604, 2639, 2686, 2719, 2751, 2783, 2809, 2845, 2869, 2898,
2926, 2954, 2982, 3020, 3046, 3072, 3093, 3117, 3142, 3171,
3190 };

static Flow_t DefaultFexFlowTab[EXH_FLOW_SUBTABLE_SIZE] =
{0.0, 0.11, 0.22, 0.32, 0.42, 0.50, 0.62, 0.72, 0.81, 0.92,
1.03, 1.12, 1.23, 1.32, 1.41, 1.54, 1.63, 1.73, 1.83, 1.93,
2.04, 2.14, 2.24, 2.31, 2.42, 2.52, 2.63, 2.73, 2.85, 2.92,
3.03, 3.14, 3.21, 3.33, 3.41, 3.53, 3.61, 3.74, 3.80, 3.91,
4.05, 4.13, 4.23, 4.31, 4.40, 4.53, 4.62, 4.75, 4.84, 4.94,
5.06, 5.15, 5.26, 5.30, 5.44, 5.49, 5.64, 5.73, 5.85, 5.98,
6.07, 6.18, 6.26, 6.39, 6.48, 6.51, 6.61, 6.77, 6.85, 6.92,
7.04, 7.19, 7.29, 7.42, 7.45, 7.56, 7.66, 7.72, 7.84, 7.98,
8.04, 8.14, 8.26, 8.33, 8.47, 8.55, 8.61, 8.68, 8.84, 8.94,
9.13, 9.16, 9.23, 9.36, 9.56, 9.58, 9.66, 9.83, 9.90, 10.04,
10.33, 12.30, 14.33, 16.16, 18.27, 20.22, 22.14, 24.42, 26.39, 28.22,
30.38, 32.13, 34.23, 36.37, 38.14, 40.19, 42.36, 44.12, 46.25, 48.38,
50.57, 54.85, 60.36, 65.69, 70.91, 76.19, 80.70, 85.31, 90.77, 95.47,
100.90, 105.89, 109.82, 116.60, 121.97, 125.71, 130.70, 135.90, 140.29, 145.63,
150.40, 153.86, 160.12, 165.64, 168.65, 176.20, 180.86, 186.81, 191.44, 195.46,
201.96 };

static float DefaultFexDacTab[EXH_FLOW_SUBTABLE_SIZE] =
{ 48, 81, 94, 106, 115, 124, 134, 141, 148, 154,
160, 166, 175, 179, 182, 187, 191, 195, 199, 203,
207, 211, 215, 217, 221, 224, 228, 232, 235, 237,
240, 244, 246, 249, 252, 255, 257, 260, 262, 264,
268, 270, 271, 273, 275, 278, 279, 282, 283, 285,
288, 290, 293, 293, 296, 296, 300, 300, 303, 305,
307, 309, 310, 313, 313, 316, 316, 318, 320, 322,
323, 326, 328, 329, 331, 331, 334, 334, 336, 338,
339, 341, 342, 343, 345, 346, 347, 349, 350, 351,
355, 355, 356, 357, 359, 360, 361, 362, 365, 365,
369, 392, 414, 431, 452, 466, 483, 499, 514, 524,
539, 549, 562, 573, 583, 594, 604, 614, 624, 634,
645, 663, 685, 699, 719, 735, 749, 761, 776, 787,
801, 812, 821, 836, 848, 856, 866, 876, 883, 893,
902, 911, 923, 932, 936, 948, 956, 965, 973, 979,
989};

//*****************************************************************************

ExhFlowSensor_t::ExhFlowSensor_t(const AdcChannels::AdcChannelId adcId)
	: Sensor(adcId)
{
	DisconnectCount = 0;
	HeatedWireBrokenCount = 0;
	CompWireBrokenCount = 0;
	ZeroOutCount = 0;
	BadSensorCount = 0;
	SensorStatus = STATUS_GOOD;
	StatusData = 0;
	RawData = 0;
	UnAdjustedFlow = 0.0;
	AdjustedFlow = 0.0;
	bError = false;
	bAutozero = false;
	bDataValid = false;
	FilteredFlow = 0;

	// Start the cal tables with default values
    if(ExhCommunicationInterface::getInstance()->GetSensorBoardType() == EXH_SENSOR_BOARD_NEW)
	{
		memcpy(ExhSensorTable.FlowTable,    DefaultFexFlowTab_12bit, sizeof(ExhSensorTable.FlowTable));
		memcpy(ExhSensorTable.DacTable,     DefaultFexDacTab_12bit,  sizeof(ExhSensorTable.DacTable));
	}
	else
	{
		memcpy(ExhSensorTable.FlowTable,    DefaultFexFlowTab, sizeof(ExhSensorTable.FlowTable));
		memcpy(ExhSensorTable.DacTable,     DefaultFexDacTab,  sizeof(ExhSensorTable.DacTable));
	}

    // Pointers to the data
    pDacTab = ExhSensorTable.DacTable;
    pFlowTab = ExhSensorTable.FlowTable;
}

//*****************************************************************************

ExhFlowSensor_t::~ExhFlowSensor_t()
{
}

//*****************************************************************************

bool ExhFlowSensor_t::IsError()
{
	return (SensorStatus != STATUS_GOOD);
}

//*****************************************************************************

bool ExhFlowSensor_t::IsDisconnected()
{
	return (SensorStatus & DIS_BITS) ? true : false;
}

//*****************************************************************************

bool ExhFlowSensor_t::GetFlow(float &Value)
{
	Value = AdjustedFlow;
	return bError;
}

//*****************************************************************************

Flow_t ExhFlowSensor_t::GetFlow2()
{
	return Flow2;
}
//*****************************************************************************

bool ExhFlowSensor_t::IsZeroOut()
{
	return ((StatusData & STATUS_ZERO_OUT) != 0);
}

//*****************************************************************************

void ExhFlowSensor_t::SetNoComm()
{
	if(bAutozero)
	{
		bAutozero = false;
	}
	bError = true;
}

//*****************************************************************************

bool ExhFlowSensor_t::Autozero()
{
	bool rc = false;

#ifdef SIGMA_BD_CPU
	if(!bAutozero)
	{
		bAutozero = true;

		rc = ExhCommunicationInterface::getInstance()->RequestAutozero();
	}
	else
	{
		RETAILMSG(1, (L"\r\nERROR: ExhSensor AZ Busy!!!\r\n{ "));
	}
#endif

	return rc;
}

//*****************************************************************************
//
//  Used by the calibration process to update the cal data
//
//*****************************************************************************

void ExhFlowSensor_t::UpdateCalData(ExhFlowSensorCalibrationTable_t *pCalTab)
{
	if(pCalTab)
	{
        // Get the pointer to the calibration data
        ExhSensorTable_t *pExhSensorTab = (ExhSensorTable_t *)pCalTab->GetTablePointer();

        // Transfer the new cal data for use by this class
        ExhSensorTable = *pExhSensorTab;

		ExhCalOffset = pDacTab[0];

        // Update in persistent storage
        RCalInfoFlashBlock.SaveExhFlowSensorData(ExhSensorTable);
	}
}

//*****************************************************************************
//
//  Used during initialization to set the cal data
//
//*****************************************************************************

void ExhFlowSensor_t::SetCalData(const ExhSensorTable_t & SensorTable)
{
    // Transfer the new cal data for use by this class
    ExhSensorTable = SensorTable;

    ExhCalOffset = pDacTab[0];
}

//*****************************************************************************
Flow_t ExhFlowSensor_t::ConvertFlow(uint16_t AdcValue)
{
#ifdef SIGMA_BD_CPU
	Flow_t Fex, FexO2;

	if(!pFlowTab || !pDacTab)
	{
		return 0;
	}

	// compute adc counts for air and o2
	Uint32 airAdc = (Uint32)((100.0F - o2Percent_) * AdcValue / 79.0) ;
	Uint32 o2Adc = AdcValue - airAdc ;

	// look up for air and o2 flows using adc counts
	LookUpFloat(&Fex, pFlowTab, (float)airAdc, pDacTab, EXH_FLOW_SUBTABLE_SIZE-1);
	LookUpFloat(&FexO2, pFlowTab, (float)o2Adc, pDacTab, EXH_FLOW_SUBTABLE_SIZE-1);

	return (Fex + FexO2);
#else
	return 0;
#endif
}

//*****************************************************************************
const int NUM_FLOW_ADJUST_FACTORS	= 4;
const struct {
	Flow_t flow;
	int factor;
} Factor[NUM_FLOW_ADJUST_FACTORS] = {{5, 17}, {10, 11}, {15, 9}, {20, 7}};

Flow_t ExhFlowSensor_t::FiO2_Correction(Flow_t Flow, FiO2_t FiO2)
{
	int i, FiO2Factor;
	float AdjustFactor;
	Flow_t AdjustedFlow=0;
	for(i = 0; i < NUM_FLOW_ADJUST_FACTORS; i++)
	{
		if(Flow < Factor[i].flow)
		{
			FiO2Factor = Factor[i].factor;
			break;
		}
	}
	if(i >= NUM_FLOW_ADJUST_FACTORS)
	 	FiO2Factor = Factor[NUM_FLOW_ADJUST_FACTORS - 1].factor;

	AdjustFactor = ((FiO2 - 0.21) * FiO2Factor) / 0.79;

	AdjustedFlow = (Flow * (100 - AdjustFactor)) / 100;

	return(AdjustedFlow);
}

//*****************************************************************************
void ExhFlowSensor_t::CalculateFlow(uint16_t AdcValue)
{
	Flow_t ExhFlow;

	if(SensorStatus == STATUS_GOOD)
	{
		if(ExhCommunicationInterface::getInstance()->GetSensorBoardType() == EXH_SENSOR_BOARD_NEW)
		{
			uint8_t HighByte = AdcValue >> 8;
			uint8_t LowByte = AdcValue & 0xFF;
			ExhFlow = (Flow_t)((HighByte * 256) + LowByte) / 100;
		}
		else
		{		
			if(AdcValue >= MAX_ADC_VALUE)
			{
				static bool bAdcError = false;
				if(!bAdcError)
				{
					DEBUGMSG(1, (_T("ERROR: Invalid Exh. Flow, AdcValue = %d\r\n"), AdcValue));
					bAdcError = true;
				}
				return;
			}

			ExhFlow = ConvertFlow(AdcValue);

#if 0	//TODO E600: Need to determine if FiO2 correction is nec.
			if(o2Percent_ > 21)
				ExhFlow = FiO2_Correction(ExhFlow, o2Percent_/100);
#endif
		}

		UnAdjustedFlow = ExhFlow;
		if(ExhFlow < 0)
		{
			AdjustedFlow = 0;
			UnAdjustedFlow = 0;
		}
        else
		{
			// dont do adjust during circuit check
			//TODO E600_LL: Need to do BTPS compensation later
			//AdjustedFlow = BTPS_Adjust(ExhFlow);
			AdjustedFlow = ExhFlow;
			
			if(AdjustedFlow < 0)
				AdjustedFlow = 0;
		}
		bError = FALSE;
	}
	else
	{
		bError = TRUE;
	}
}

//*****************************************************************************
Flow_t ExhFlowSensor_t::CalculateFlow2(uint16_t AdcValue, uint16_t AzValue)
{
	double FlowAdc = (double)(AdcValue - AzValue);
	if(FlowAdc < 0)
		FlowAdc = 0;
	double Term1 = (double)0.00000020675 * (FlowAdc * FlowAdc * FlowAdc);
	double Term2 = (double)0.00001203 * (FlowAdc * FlowAdc);
	double Term3 = (double)0.008563 * FlowAdc;

	Flow_t ExhFlow = (Flow_t)(Term1 + Term2 + Term3);
	return ExhFlow;
}


//*****************************************************************************
void ExhFlowSensor_t::NormalizeFexhTable(uint16_t Offset)
{
	float diff;

	RETAILMSG(1, (L"Exh Flow Sensor Offset: New=%d, Old=%d\r\n", Offset, (uint16_t)pDacTab[0]));

	diff = pDacTab[0] - (float)Offset;
	if(abs(diff) < 5)
	{
		ExhCalOffset = Offset;
		for(int i=0; i<EXH_FLOW_SUBTABLE_SIZE; i++)
			pDacTab[i] = pDacTab[i] - diff;
	}
	else
	{
		RETAILMSG(1, (L"ExhFlowSensor : Skip Normalizing\r\n"));
	}
}

//*****************************************************************************
void ExhFlowSensor_t::CheckStatus(uint8_t FexStatus)
{
	if(bAutozero)
	{
		uint8_t AzDoneMask;
		AzDoneMask = AUTOZERO_DONE_BIT;

		if(ExhCommunicationInterface::getInstance()->GetSensorBoardType() == EXH_SENSOR_BOARD_NEW)
		{
			AzDoneMask = AUTOZERO_DONE_BIT_NEW_SENSOR;
		}

		if((FexStatus & AzDoneMask) >> 1)
		{
			bAutozero = false;
			if(!(FexStatus & ZERO_OUT_BIT))
			{
				AzRawData = RawData;
				// TODO E600_LL: will determine if this call is necessary
				//NormalizeFexhTable(RawData);
			}

			if((FexStatus & ZERO_OUT_BIT) >> 3)
			{
				SensorStatus = STATUS_ZERO_OUT;
				// stop after a number of retries
				if(++ZeroOutCount >= MAX_FEX_ERR_COUNT)
				{
					ZeroOutCount = MAX_FEX_ERR_COUNT;
				}
				else
				{
					Autozero();
				}
			}
			else
			{
				ZeroOutCount = 0;
			}
		}
	}
	else if((FexStatus&DIS_BITS)==DIS_BITS)
	{
		if(++DisconnectCount>=MAX_FEX_ERR_COUNT)
		{
			DisconnectCount = MAX_FEX_ERR_COUNT;
			SensorStatus = STATUS_DISCONNECT;
		}
	}
	else
	{
	   	DisconnectCount = 0;
		if((FexStatus&HEATED_WIRE_BROKEN_BIT)!=0)
		{
			if(++HeatedWireBrokenCount>=MAX_FEX_ERR_COUNT)
			{
				HeatedWireBrokenCount = MAX_FEX_ERR_COUNT;
				SensorStatus = STATUS_WIRE_BROKEN;
			}
		}
		else
		{
			HeatedWireBrokenCount = 0;
			if((FexStatus&COMP_WIRE_BROKEN_BIT)!=0)
			{
				if(++CompWireBrokenCount>=MAX_FEX_ERR_COUNT)
				{
					CompWireBrokenCount = MAX_FEX_ERR_COUNT;
					SensorStatus = STATUS_COMP_WIRE_BROKEN;
				}
			}
			else
			{
				if((FexStatus&ZERO_OUT_BIT)!=0)
				{
					if(++ZeroOutCount>=MAX_FEX_ERR_COUNT)
					{
						ZeroOutCount = MAX_FEX_ERR_COUNT;
						SensorStatus = STATUS_ZERO_OUT;
					}
				}
				else
				{
					ZeroOutCount = 0;
					if((FexStatus&CLEANING_BIT)!=0)
						SensorStatus = STATUS_CLEANING;
					else
						SensorStatus = STATUS_GOOD;
				}
			}
		}
	}
}


//*****************************************************************************
void ExhFlowSensor_t::ProcessData(uint8_t FexStatus, uint16_t FexData, bool bData)
{
	StatusData = FexStatus;

	if(bData)
	{
		RawData = FexData;
		RawFilteredData = ExhDataFilter.FilterValue(RawData, 2);

	}
	CheckStatus(FexStatus);
	if(bData)
	{
		if(ExhCommunicationInterface::getInstance()->GetSensorBoardType() == EXH_SENSOR_BOARD_NEW)
			CalculateFlow(RawData);
		else
			AdjustedFlow = CalculateFlow2(RawData, 50);
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateValue()
//
//@ Interface-Description
//      This method take no arguments and converts the raw sample to
//		engineering units and update the processed values (slope, filter,
//		etc.).
//---------------------------------------------------------------------
//@ Implementation-Description
//      Convert the raw data into flow (LPM).  Correct for dry
//      gas (exhalation dry flow).  Update the filtered and slope values.
//		$[04326]
//---------------------------------------------------------------------
//@ PreCondition
//		None
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================

#if defined (SIGMA_BD_CPU)

void ExhFlowSensor_t::updateValue( void)
{
	CALL_TRACE("ExhFlowSensor::updateValue( void)") ;

	if(1)//ExhCommunicationThread_t::SensorBoard == EXH_SENSOR_BOARD_NEW)
	{
		Uint32 airFlow = (Uint32)((100.0F - o2Percent_) * AdjustedFlow / 79.0) ;
		Uint32 o2Flow = (Uint32)(AdjustedFlow - airFlow) ;
		Real32 airOffset = RAirSideFlowSensorOffset.getOffset( airFlow);
		Real32 o2Offset = RO2SideFlowSensorOffset.getOffset( o2Flow);
		
		Real32 mixFlow = AdjustedFlow - airOffset - o2Offset;

		engValue_ = mixFlow ;
		dryFlow_ = mixFlow;
	}
	else
	{
		engValue_ = AdjustedFlow ;
		dryFlow_ = AdjustedFlow;
	}

   	unoffsettedFlow_ = AdjustedFlow ;
	
    processedValues_.updateValues( engValue_) ;
    dryProcessedValues_.updateValues( dryFlow_) ;
    unoffsettedProcessedValues_.updateValues( unoffsettedFlow_) ;
}

#endif // defined (SIGMA_BD_CPU)

