#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2006, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: NifPausePhase -  NIF Pause Phase.
//---------------------------------------------------------------------
//@ Interface-Description
// This class is derived from the BreathPhase class. Virtual methods 
// defined in this class provide the implementation of the Negative 
// Inspiratory Force Pause (or Occlusion) phase. The NifPausePhase 
// provides the control necessary to occlude the circuit during the NIF
// maneuver so the NifManeuver class can measure the maximum negative 
// pressure that the patient can draw against a closed circuit and 
// report it to the GUI. Virtual methods are implemented to 
// initialize the NIF occlusion phase, to control the NIF occlusion 
// every BD cycle and to wrap up the phase and relinquish control at 
// the end of the phase.
//---------------------------------------------------------------------
//@ Rationale
// This class implements NIF Maneuver Pause phase.	
//---------------------------------------------------------------------
//@ Implementation-Description
// In order to create a closed circuit, the exhalation valve is closed 
// during this phase and the PSOLs are commanded closed. This class 
// uses the same type of control algorithm as the ExpPausePhase class
// to create a closed circuit.
//---------------------------------------------------------------------
//@ Fault-Handling
// N/A
//---------------------------------------------------------------------
//@ Restrictions
// none
//---------------------------------------------------------------------
//@ Invariants
// none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/NifPausePhase.ccv   25.0.4.0   19 Nov 2013 13:59:54   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001   By: gdc   Date:  22-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Initial version.
//
//=====================================================================

#include "NifPausePhase.hh"

#include "MainSensorRefs.hh"
#include "TriggersRefs.hh"
#include "ModeTriggerRefs.hh"
#include "ValveRefs.hh"
#include "BreathMiscRefs.hh"

//@ Usage-Classes

#include "PhasedInContextHandle.hh"
#include "ApneaInterval.hh"
#include "PressureSensor.hh"
#include "Trigger.hh"
#include "TimerBreathTrigger.hh"
#include "Psol.hh"
#include "ExhValveIController.hh"
#include "ControllersRefs.hh"
#include "BD_IO_Devices.hh"
#include "FlowController.hh"
#include "PausePressTrigger.hh"
#include "HighCircuitPressureInspTrigger.hh"
#include "NifManeuver.hh"
#include "ManeuverRefs.hh"

//@ End-Usage

//@ Constant: PRESSURE_TO_SEAL
// pressure above pat press for exhalation valve to seal
extern const Real32 PRESSURE_TO_SEAL ;

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: NifPausePhase()
//
//@ Interface-Description
// Constructor. This method has no arguments and returns nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
// The base class constructor is called with the phase type argument.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

NifPausePhase::NifPausePhase( void)
 : BreathPhase(BreathPhaseType::NIF_PAUSE) 	   	// $[TI1]
{
	CALL_TRACE("NifPausePhase::NifPausePhase( void)") ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~NifPausePhase()
//
//@ Interface-Description
// Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
// none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

NifPausePhase::~NifPausePhase(void)
{
	CALL_TRACE("NifPausePhase::~NifPausePhase(void)") ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: relinquishControl()
//
//@ Interface-Description
// This method has a BreathTrigger& as an argument and has no return 
// value.  This method is called at the end of the NIF pause
// phase to wrap up the phase.
//---------------------------------------------------------------------
//@ Implementation-Description
// Calls Maneuver::Complete() to inform the NifManeuver of the end
// of the NIF pause phase.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
void
NifPausePhase::relinquishControl( const BreathTrigger& )
{
   	CALL_TRACE("relinquishControl(BreathTrigger& trigger)");

	// $[RM24010] resume normal breath delivery...
	Maneuver::Complete();
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newCycle()
//
//@ Interface-Description
// This method has no arguments and no return value.  This method is
// called every BD cycle during an NIF pause phase to control the
// exhalation valve to maintain a closed circuit.
//---------------------------------------------------------------------
//@ Implementation-Description
// The Psols are closed.  The exhalation valve is commanded to patient
// pressure plus PRESSURE_TO_SEAL with a maximum of HCP.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
void
NifPausePhase::newCycle( void)
{
    CALL_TRACE("newCycle(void)");

	// $[RM24010] occlude the circuit...

	updateFlowControllerFlags_() ;
	
    RAirFlowController.updatePsol( 0.0F) ;
   	RO2FlowController.updatePsol( 0.0F) ;

	Real32	targetPressure = MIN_VALUE( 
				PRESSURE_TO_SEAL + RExhPressureSensor.getFilteredValue(),
				highCircuitPressure_) ;
				
   	RExhValveIController.updateExhalationValve( targetPressure, 0.0F) ;

	elapsedTimeMs_ += CYCLE_TIME_MS ;

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newBreath()
//
//@ Interface-Description
// This method has no arguments and has no return value.  This method is 
// called at the beginning of the NIF pause phase to initialize the
// phase.
//---------------------------------------------------------------------
//@ Implementation-Description
// The ExhValveIController and Psols are initalized via newBreath() method
// calls. Patient triggers are not enabled to force the patient to inspire
// against a closed circuit so that negative pressure can be measured and
// reported to the GUI. The NifManeuver maintains an interval timer 
// that terminates the phase if the operator keeps touching the NIF START
// button and does not stop the occlusion before the timer expires.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
void
NifPausePhase::newBreath(void)
{
    CALL_TRACE("newBreath(void)");
    // $[RM12107] ...shall transition to active upon detecting patient effort...

   	CLASS_ASSERTION(RNifManeuver.getManeuverState() == PauseTypes::PENDING);

	const BoundedValue& rHighCircuitPressure =
		PhasedInContextHandle::GetBoundedValue( SettingId::HIGH_CCT_PRESS) ;

	highCircuitPressure_ = rHighCircuitPressure.value ;
	
    RExhValveIController.newBreath() ;
    RAirFlowController.newBreath() ;
   	RO2FlowController.newBreath() ;
    
    RHighCircuitPressureInspTrigger.enable();

	elapsedTimeMs_ = 0 ;

	// Activate the exp. pause maneuver
	RNifManeuver.activate();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
NifPausePhase::SoftFault( const SoftFaultID  softFaultID,
                   				 const Uint32       lineNumber,
		   						 const char*        pFileName,
		  						 const char*        pPredicate)
{
  	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)") ;
  	FaultHandler::SoftFault( softFaultID, BREATH_DELIVERY, NIFPAUSEPHASE,
    	                     lineNumber, pFileName, pPredicate) ;
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateFlowControllerFlags_
//
//@ Interface-Description
// This method has no arguments and has no return value.  This method is
// called to set the controller shutdown flag of the flow controllers to
// TRUE.  This method overloads the base class method.
//---------------------------------------------------------------------
//@ Implementation-Description
// Calls setControllerShutdown for both flow controllers.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
void
NifPausePhase::updateFlowControllerFlags_( void)
{
	CALL_TRACE("NifPausePhase::updateFlowControllerFlags_( void)") ;

	// $[RM24010] \4a\ occlude the circuit...

	RO2FlowController.setControllerShutdown( TRUE) ;
	RAirFlowController.setControllerShutdown( TRUE) ;
}

//=====================================================================
//
//  Private Methods...
//
//=====================================================================
