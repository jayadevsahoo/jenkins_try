#ifndef SmDataId_HH
#define SmDataId_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: SmDataId - Defines the enums for test result data ID for each
// service mode test.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/SmDataId.hhv   25.0.4.0   19 Nov 2013 14:23:44   pvcs  $
//
//@ Modification-Log
//
//  Revision: 014   By: rhj   Date: 14-Sept-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//		Added PX_DATA_ITEM_8.
// 
//  Revision: 013   By: mnr   Date: 04-May-2010    	SCR Number: 6436
//  Project:  PROX
//  Description:
//		Updates to reuse data display enums to conserve NOVRAM.
//
//  Revision: 012   By: mnr   Date: 03-May-2010    	SCR Number: 6436
//  Project:  PROX
//  Description:
//		More enums added for Pressure Cross check test
//
//  Revision: 011  By: mnr    Date: 28-Apr-2010   SCR Number: 6436
//  Project:  PROX
//  Description:
//      More enums added for PX_ACCUMULATOR_PRESSURE readings.
//
//  Revision: 010  By: mnr    Date: 16-Apr-2010   SCR Number: 6436
//  Project:  PROX
//  Description:
//      PX_ACCUMULATOR_PRESSURE added.
//
//  Revision: 009  By: mnr    Date: 13-Apr-2010   SCR Number: 6436
//  Project:  PROX
//  Description:
//      PROX SST enums updated.
//
//  Revision: 008  By: mnr    Date: 24-Mar-2010     DR Number: 6436
//  Project:  PROX4
//  Description:
//      PROX SST related updates.
//
//  Revision: 007  By: syw    Date: 03-Feb-2000     DR Number: 5327
//  Project:  NeoMode
//  Description:
//		Added EST/SST flow sensor cross check at 20 lpm enum
//
//  Revision: 006  By:  quf    Date:  08-Oct-97    DR Number: DCS 2203
//    Project:  Sigma (840)
//    Description:
//      Updated data for new BD audio test procedure.
//
//  Revision: 005  By:  quf    Date:  01-Oct-97    DR Number: DCS 2204
//    Project:  Sigma (840)
//    Description:
//      Updated data for new battery test procedure.
//
//  Revision: 004  By:  quf    Date:  25-Sep-97    DR Number: DCS 2410
//    Project:  Sigma (840)
//    Description:
//      Added data for atm pressure xducer calibration.
//
//  Revision: 003  By:  yyy    Date:  30-JUL-97    DR Number: 2013
//    Project:  Sigma (R8027)
//    Description:
//      Updated the data item for Nurse Call test.
//
//  Revision: 002  By:  quf    Date:  30-Jul-97    DR Number: 2205
//       Project:  Sigma (R8027)
//       Description:
//             Added data ID for EV velocity xducer test.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number:
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "Sigma.hh"

//@ Usage-Classes
//@ End-Usage


class SmDataId
{
public:

	//@ Type: ItemizedTestDataId
	// This enum lists all of the Service mode tests data generated by the
	// service mode.
	enum ItemizedTestDataId
	{
		NULL_ITEMIZED_TEST_DATA_ITEM   = -1,

		TEST_DATA_START,


		// INITIALIZE_FLOW_SENSORS_ID
		INITIALIZE_FLOW_SENSORS_START = TEST_DATA_START,
		INITIALIZE_FLOW_SENSORS_END = INITIALIZE_FLOW_SENSORS_START,

		// GAS_SUPPLY_TEST_ID
		GAS_SUPPLY_TEST_START = TEST_DATA_START,
		// 1 LPM SV cracking pressure with inlet blocked (cmH2O)
		// Displayed on GUI
		GAS_SUPPLY_LOW_FLOW_CRACKING_PRESSURE = GAS_SUPPLY_TEST_START,
		// 1-100 LPM SV peak pressure with inlet blocked (cmH2O)
		// Displayed on GUI
		GAS_SUPPLY_HIGH_FLOW_CRACKING_PRESSURE,
		// Pressure rise with inlet blocked (cmH2O)
		// Displayed on GUI
		GAS_SUPPLY_AIR_PSOL_LEAK_PRESSURE,
		// Air flow sensor 0 offset (lpm)
		// Displayed on GUI
		GAS_SUPPLY_AIR_FLOW_SENSOR_0_OFFSET,
		// O2 flow sensor 0 offset (lpm)
		// Displayed on GUI
		GAS_SUPPLY_O2_FLOW_SENSOR_0_OFFSET,
		// Exh flow sensor 0 offset (lpm)
		// Displayed on GUI
		GAS_SUPPLY_EXH_FLOW_SENSOR_0_OFFSET,
		// Pressure rise with inlet blocked (cmH2O)
		// Displayed on GUI
		GAS_SUPPLY_O2_PSOL_LEAK_PRESSURE,
		GAS_SUPPLY_TEST_END = GAS_SUPPLY_O2_PSOL_LEAK_PRESSURE,

		// CIRCUIT_RESISTANCE_TEST_ID
		CIRCUIT_RESISTANCE_TEST_START = TEST_DATA_START,
		// Inspiratory limb resistance (cmH2O)
		// Displayed on GUI
		CIRCUIT_RESISTANCE_INSP_PRESSURE = CIRCUIT_RESISTANCE_TEST_START,
		// Expiratory limb resistance (cmH2O)
		// Displayed on GUI
		CIRCUIT_RESISTANCE_EXP_PRESSURE,
		CIRCUIT_RESISTANCE_TEST_END = CIRCUIT_RESISTANCE_EXP_PRESSURE,

		// SST_FILTER_TEST_ID
		SST_FILTER_TEST_START = TEST_DATA_START,
		// Inspiratory resistance (disconnected from exh filter) @ 60 LPM (cmH2O)
		SST_FILTER_INSPIRATORY_PRESSURE1 = SST_FILTER_TEST_START,
		// Inspiratory resistance (connected to exh filter) @ 60 LPM (cmH2O)
		SST_FILTER_INSPIRATORY_PRESSURE2,
		// Exhalation resistance (connected to exh filter) @ 60 LPM (cmH2O)
		SST_FILTER_EXHALATION_PRESSURE,
		// Filter delta @ 60 LPM (cmH2O)
		// Displayed on GUI
		SST_FILTER_DELTA_PRESSURE,
		SST_FILTER_TEST_END = SST_FILTER_DELTA_PRESSURE,

		// GUI_KEYBOARD_TEST_ID
		GUI_KEYBOARD_TEST_START = TEST_DATA_START,
		GUI_KEYBOARD_TEST_END = GUI_KEYBOARD_TEST_START,

		// GUI_KNOB_TEST_ID
		GUI_KNOB_TEST_START = TEST_DATA_START,
		GUI_KNOB_TEST_END = GUI_KNOB_TEST_START,

		// GUI_LAMP_TEST_ID
		GUI_LAMP_TEST_START = TEST_DATA_START,
		GUI_LAMP_TEST_END = GUI_LAMP_TEST_START,

		// BD_LAMP_TEST_ID
		BD_LAMP_TEST_START = TEST_DATA_START,
		BD_LAMP_TEST_END = BD_LAMP_TEST_START,

		// GUI_AUDIO_TEST_ID
		GUI_AUDIO_TEST_START = TEST_DATA_START,
		GUI_AUDIO_TEST_END = GUI_AUDIO_TEST_START,

		// GUI_NURSE_CALL_TEST_ID
		GUI_NURSE_CALL_TEST_START = TEST_DATA_START,
		GUI_NURSE_CALL_TEST_END = GUI_NURSE_CALL_TEST_START,

		// BD_AUDIO_TEST_ID
		BD_AUDIO_TEST_START = TEST_DATA_START,
		// Alarm Cable Voltage
		// Displayed on GUI
		BD_AUDIO_ALARM_CABLE_VOLTAGE = BD_AUDIO_TEST_START,
		// Initial power fail cap voltage (volts)
		// Displayed on GUI
		BD_AUDIO_POWER_FAIL_CAP_VOLTAGE1,
		// measured RC time constant (seconds)
		// Displayed on GUI
		BD_AUDIO_TIME_CONSTANT,
		BD_AUDIO_TEST_END = BD_AUDIO_TIME_CONSTANT,
		
		// FS_CROSS_CHECK_TEST_ID
		FS_CROSS_CHECK_TEST_START = TEST_DATA_START,
		// O2 Flow Sensor @ 120 LPM (LPM)
		// Displayed on GUI
		FS_CROSS_CHECK_O2_120_INSPIRATORY_FLOW = FS_CROSS_CHECK_TEST_START,
		// Exh Flow Sensor @ 120 LPM of O2 (LPM)
		// Displayed on GUI
		FS_CROSS_CHECK_O2_120_EXHALATION_FLOW,
		// PSOL current to open the O2 PSOL to 120 LPM (ma)
		// Displayed on GUI
		FS_CROSS_CHECK_O2_120_PSOL_CURRENT,
		// O2 Flow Sensor @ 60 LPM (LPM)
		// Displayed on GUI
		FS_CROSS_CHECK_O2_60_INSPIRATORY_FLOW,
		// Exh Flow Sensor @ 60 LPM of O2 (LPM)
		// Displayed on GUI
		FS_CROSS_CHECK_O2_60_EXHALATION_FLOW,
		// PSOL current to open the O2 PSOL to 60 LPM (ma)
		// Displayed on GUI
		FS_CROSS_CHECK_O2_60_PSOL_CURRENT,
		// O2 Flow Sensor @ 20 LPM (LPM)
		// Displayed on GUI
		FS_CROSS_CHECK_O2_20_INSPIRATORY_FLOW,
		// Exh Flow Sensor @ 20 LPM of O2 (LPM)
		// Displayed on GUI
		FS_CROSS_CHECK_O2_20_EXHALATION_FLOW,
		// PSOL current to open the O2 PSOL to 20 LPM (ma)
		// Displayed on GUI
		FS_CROSS_CHECK_O2_20_PSOL_CURRENT,
		// O2 Flow Sensor @ 5 LPM (LPM)
		// Displayed on GUI
		FS_CROSS_CHECK_O2_5_INSPIRATORY_FLOW,
		// Exh Flow Sensor @ 5 LPM of O2 (LPM)
		// Displayed on GUI
		FS_CROSS_CHECK_O2_5_EXHALATION_FLOW,
		// PSOL current to open the O2 PSOL to 5 LPM (ma)
		// Displayed on GUI
		FS_CROSS_CHECK_O2_5_PSOL_CURRENT,
		// O2 Flow Sensor @ 1 LPM (LPM)
		// Displayed on GUI
		FS_CROSS_CHECK_O2_1_INSPIRATORY_FLOW,
		// Exh Flow Sensor @ 1 LPM of O2 (LPM)
		// Displayed on GUI
		FS_CROSS_CHECK_O2_1_EXHALATION_FLOW,
		// PSOL current to open the O2 PSOL to 1 LPM (ma)
		// Displayed on GUI
		FS_CROSS_CHECK_O2_1_PSOL_CURRENT,
		// O2 Flow Sensor @ 0 LPM (LPM)
		// Displayed on GUI
		FS_CROSS_CHECK_O2_0_INSPIRATORY_FLOW,
		// Air Flow Sensor @ 120 LPM (LPM)
		// Displayed on GUI
		FS_CROSS_CHECK_AIR_120_INSPIRATORY_FLOW,
		// Exh Flow Sensor @ 120 LPM of Air (LPM)
		// Displayed on GUI
		FS_CROSS_CHECK_AIR_120_EXHALATION_FLOW,
		// PSOL current to open the Air PSOL to 120 LPM (ma)
		// Displayed on GUI
		FS_CROSS_CHECK_AIR_120_PSOL_CURRENT,
		// Air Flow Sensor @ 60 LPM (LPM)
		// Displayed on GUI
		FS_CROSS_CHECK_AIR_60_INSPIRATORY_FLOW,
		// Exh Flow Sensor @ 60 LPM of Air (LPM)
		// Displayed on GUI
		FS_CROSS_CHECK_AIR_60_EXHALATION_FLOW,
		// PSOL current to open the Air PSOL to 60 LPM (ma)
		// Displayed on GUI
		FS_CROSS_CHECK_AIR_60_PSOL_CURRENT,
		// Air Flow Sensor @ 20 LPM (LPM)
		// Displayed on GUI
		FS_CROSS_CHECK_AIR_20_INSPIRATORY_FLOW,
		// Exh Flow Sensor @ 20 LPM of Air (LPM)
		// Displayed on GUI
		FS_CROSS_CHECK_AIR_20_EXHALATION_FLOW,
		// PSOL current to open the Air PSOL to 20 LPM (ma)
		// Displayed on GUI
		FS_CROSS_CHECK_AIR_20_PSOL_CURRENT,
		// Air Flow Sensor @ 5 LPM (LPM)
		// Displayed on GUI
		FS_CROSS_CHECK_AIR_5_INSPIRATORY_FLOW,
		// Exh Flow Sensor @ 5 LPM of Air (LPM)
		// Displayed on GUI
		FS_CROSS_CHECK_AIR_5_EXHALATION_FLOW,
		// PSOL current to open the Air PSOL to 5 LPM (ma)
		// Displayed on GUI
		FS_CROSS_CHECK_AIR_5_PSOL_CURRENT,
		// Air Flow Sensor @ 1 LPM (LPM)
		// Displayed on GUI
		FS_CROSS_CHECK_AIR_1_INSPIRATORY_FLOW,
		// Exh Flow Sensor @ 1 LPM of Air (LPM)
		// Displayed on GUI
		FS_CROSS_CHECK_AIR_1_EXHALATION_FLOW,
		// PSOL current to open the Air PSOL to 1 LPM (ma)
		// Displayed on GUI
		FS_CROSS_CHECK_AIR_1_PSOL_CURRENT,
		// Air Flow Sensor @ 0 LPM (LPM)
		// Displayed on GUI
		FS_CROSS_CHECK_AIR_0_INSPIRATORY_FLOW,
		// Air PSOL liftoff command @ 0.3 LPM (counts)
		// Displayed on GUI
		FS_CROSS_CHECK_AIR_LIFTOFF_COMMAND,
		// O2 PSOL liftoff command @ 0.3 LPM (counts)
		// Displayed on GUI
		FS_CROSS_CHECK_O2_LIFTOFF_COMMAND,
		FS_CROSS_CHECK_TEST_END = FS_CROSS_CHECK_O2_LIFTOFF_COMMAND,

		// SST_FS_CC_TEST_ID
		SST_FS_CC_TEST_START = TEST_DATA_START,
		// O2 Flow Sensor @ 120 LPM (LPM)
		SST_FS_CC_O2_120_INSPIRATORY_FLOW = SST_FS_CC_TEST_START,
		// Exh Flow Sensor @ 120 LPM of O2 (LPM)
		SST_FS_CC_O2_120_EXHALATION_FLOW,
		// PSOL current to open the O2 PSOL to 120 LPM (ma)
		SST_FS_CC_O2_120_PSOL_CURRENT,
		// O2 Flow Sensor @ 60 LPM (LPM)
		SST_FS_CC_O2_60_INSPIRATORY_FLOW,
		// Exh Flow Sensor @ 60 LPM of O2 (LPM)
		SST_FS_CC_O2_60_EXHALATION_FLOW,
		// PSOL current to open the O2 PSOL to 60 LPM (ma)
		SST_FS_CC_O2_60_PSOL_CURRENT,
		// O2 Flow Sensor @ 20 LPM (LPM)
		SST_FS_CC_O2_20_INSPIRATORY_FLOW,
		// Exh Flow Sensor @ 20 LPM of O2 (LPM)
		SST_FS_CC_O2_20_EXHALATION_FLOW,
		// PSOL current to open the O2 PSOL to 20 LPM (ma)
		SST_FS_CC_O2_20_PSOL_CURRENT,
		// O2 Flow Sensor @ 5 LPM (LPM)
		SST_FS_CC_O2_5_INSPIRATORY_FLOW,
		// Exh Flow Sensor @ 5 LPM of O2 (LPM)
		SST_FS_CC_O2_5_EXHALATION_FLOW,
		// PSOL current to open the O2 PSOL to 5 LPM (ma)
		SST_FS_CC_O2_5_PSOL_CURRENT,
		// O2 Flow Sensor @ 1 LPM (LPM)
		SST_FS_CC_O2_1_INSPIRATORY_FLOW,
		// Exh Flow Sensor @ 1 LPM of O2 (LPM)
		SST_FS_CC_O2_1_EXHALATION_FLOW,
		// PSOL current to open the O2 PSOL to 1 LPM (ma)
		SST_FS_CC_O2_1_PSOL_CURRENT,
		// O2 Flow Sensor @ 0 LPM (LPM)
		SST_FS_CC_O2_0_INSPIRATORY_FLOW,
		// Air Flow Sensor @ 120 LPM (LPM)
		SST_FS_CC_AIR_120_INSPIRATORY_FLOW,
		// Exh Flow Sensor @ 120 LPM of Air (LPM)
		SST_FS_CC_AIR_120_EXHALATION_FLOW,
		// PSOL current to open the Air PSOL to 120 LPM (ma)
		SST_FS_CC_AIR_120_PSOL_CURRENT,
		// Air Flow Sensor @ 60 LPM (LPM)
		SST_FS_CC_AIR_60_INSPIRATORY_FLOW,
		// Exh Flow Sensor @ 60 LPM of Air (LPM)
		SST_FS_CC_AIR_60_EXHALATION_FLOW,
		// PSOL current to open the Air PSOL to 60 LPM (ma)
		SST_FS_CC_AIR_60_PSOL_CURRENT,
		// Air Flow Sensor @ 20 LPM (LPM)
		SST_FS_CC_AIR_20_INSPIRATORY_FLOW,
		// Exh Flow Sensor @ 20 LPM of Air (LPM)
		SST_FS_CC_AIR_20_EXHALATION_FLOW,
		// PSOL current to open the Air PSOL to 20 LPM (ma)
		SST_FS_CC_AIR_20_PSOL_CURRENT,
		// Air Flow Sensor @ 5 LPM (LPM)
		SST_FS_CC_AIR_5_INSPIRATORY_FLOW,
		// Exh Flow Sensor @ 5 LPM of Air (LPM)
		SST_FS_CC_AIR_5_EXHALATION_FLOW,
		// PSOL current to open the Air PSOL to 5 LPM (ma)
		SST_FS_CC_AIR_5_PSOL_CURRENT,
		// Air Flow Sensor @ 1 LPM (LPM)
		SST_FS_CC_AIR_1_INSPIRATORY_FLOW,
		// Exh Flow Sensor @ 1 LPM of Air (LPM)
		SST_FS_CC_AIR_1_EXHALATION_FLOW,
		// PSOL current to open the Air PSOL to 1 LPM (ma)
		SST_FS_CC_AIR_1_PSOL_CURRENT,
		// Air Flow Sensor @ 0 LPM (LPM)
		SST_FS_CC_AIR_0_INSPIRATORY_FLOW,
		// Air PSOL liftoff command @ 0.3 LPM (counts)
		SST_FS_CC_AIR_LIFTOFF_COMMAND,
		// O2 PSOL liftoff command @ 0.3 LPM (counts)
		SST_FS_CC_O2_LIFTOFF_COMMAND,
		SST_FS_CC_TEST_END = SST_FS_CC_O2_LIFTOFF_COMMAND,

		// LOW_FLOW_FS_CC_TEST_ID
		LOW_FLOW_FS_CC_TEST_START = TEST_DATA_START,
		// O2 Flow Sensor @ 5 LPM (LPM)
		LOW_FLOW_FS_CC_O2_5_INSPIRATORY_FLOW = LOW_FLOW_FS_CC_TEST_START,
		// Exh Flow Sensor @ 5 LPM of O2 (LPM)
		LOW_FLOW_FS_CC_O2_5_EXHALATION_FLOW,
		// PSOL current to open the O2 PSOL to 5 LPM (ma)
		LOW_FLOW_FS_CC_O2_5_PSOL_CURRENT,
		// O2 Flow Sensor @ 1 LPM (LPM)
		LOW_FLOW_FS_CC_O2_1_INSPIRATORY_FLOW,
		// Exh Flow Sensor @ 1 LPM of O2 (LPM)
		LOW_FLOW_FS_CC_O2_1_EXHALATION_FLOW,
		// PSOL current to open the O2 PSOL to 1 LPM (ma)
		LOW_FLOW_FS_CC_O2_1_PSOL_CURRENT,
		// O2 Flow Sensor @ 0 LPM (LPM)
		LOW_FLOW_FS_CC_O2_0_INSPIRATORY_FLOW,
		// Air Flow Sensor @ 5 LPM (LPM)
		LOW_FLOW_FS_CC_AIR_5_INSPIRATORY_FLOW,
		// Exh Flow Sensor @ 5 LPM of Air (LPM)
		LOW_FLOW_FS_CC_AIR_5_EXHALATION_FLOW,
		// PSOL current to open the Air PSOL to 5 LPM (ma)
		LOW_FLOW_FS_CC_AIR_5_PSOL_CURRENT,
		// Air Flow Sensor @ 1 LPM (LPM)
		LOW_FLOW_FS_CC_AIR_1_INSPIRATORY_FLOW,
		// Exh Flow Sensor @ 1 LPM of Air (LPM)
		LOW_FLOW_FS_CC_AIR_1_EXHALATION_FLOW,
		// PSOL current to open the Air PSOL to 1 LPM (ma)
		LOW_FLOW_FS_CC_AIR_1_PSOL_CURRENT,
		// Air Flow Sensor @ 0 LPM (LPM)
		LOW_FLOW_FS_CC_AIR_0_INSPIRATORY_FLOW,
		LOW_FLOW_FS_CC_TEST_END = LOW_FLOW_FS_CC_AIR_0_INSPIRATORY_FLOW,

		// HIGH_FLOW_FS_CC_TEST_ID
		HIGH_FLOW_FS_CC_TEST_START = TEST_DATA_START,
		// O2 Flow Sensor @ 120 LPM (LPM)
		HIGH_FLOW_FS_CC_O2_120_INSPIRATORY_FLOW = HIGH_FLOW_FS_CC_TEST_START,
		// Exh Flow Sensor @ 120 LPM of O2 (LPM)
		HIGH_FLOW_FS_CC_O2_120_EXHALATION_FLOW,
		// PSOL current to open the O2 PSOL to 120 LPM (ma)
		HIGH_FLOW_FS_CC_O2_120_PSOL_CURRENT,
		// O2 Flow Sensor @ 60 LPM (LPM)
		HIGH_FLOW_FS_CC_O2_60_INSPIRATORY_FLOW,
		// Exh Flow Sensor @ 60 LPM of O2 (LPM)
		HIGH_FLOW_FS_CC_O2_60_EXHALATION_FLOW,
		// PSOL current to open the O2 PSOL to 60 LPM (ma)
		HIGH_FLOW_FS_CC_O2_60_PSOL_CURRENT,
		// Air Flow Sensor @ 120 LPM (LPM)
		HIGH_FLOW_FS_CC_AIR_120_INSPIRATORY_FLOW,
		// Exh Flow Sensor @ 120 LPM of Air (LPM)
		HIGH_FLOW_FS_CC_AIR_120_EXHALATION_FLOW,
		// PSOL current to open the Air PSOL to 120 LPM (ma)
		HIGH_FLOW_FS_CC_AIR_120_PSOL_CURRENT,
		// Air Flow Sensor @ 60 LPM (LPM)
		HIGH_FLOW_FS_CC_AIR_60_INSPIRATORY_FLOW,
		// Exh Flow Sensor @ 60 LPM of Air (LPM)
		HIGH_FLOW_FS_CC_AIR_60_EXHALATION_FLOW,
		// PSOL current to open the Air PSOL to 60 LPM (ma)
		HIGH_FLOW_FS_CC_AIR_60_PSOL_CURRENT,
		HIGH_FLOW_FS_CC_TEST_END = HIGH_FLOW_FS_CC_AIR_60_PSOL_CURRENT,

		// CIRCUIT_PRESSURE_TEST_ID
		CIRCUIT_PRESSURE_TEST_START = TEST_DATA_START,
		// Inspiratory autozero (counts)
		// Displayed on GUI
		CIRCUIT_PRESSURE_INSP_ZERO_COUNTS = CIRCUIT_PRESSURE_TEST_START,
		// Exhalation autozero (counts)
		// Displayed on GUI
		CIRCUIT_PRESSURE_EXH_ZERO_COUNTS,
		// Inspiratory pressure @ 10 (cmH2O)
		// Displayed on GUI
		CIRCUIT_PRESSURE_INSP_PRESSURE_AT_10,
		// Exhalation pressure @ 10 (cmH2O)
		// Displayed on GUI
		CIRCUIT_PRESSURE_EXH_PRESSURE_AT_10,
		// Inspiratory autozero with pressurized tubing (cmH2O)
		// Displayed on GUI
		CIRCUIT_PRESSURE_INSP_AUTOZERO_PRESSURE,
		// Exhalation autozero with pressurized tubing (cmH2O)
		// Displayed on GUI
		CIRCUIT_PRESSURE_EXH_AUTOZERO_PRESSURE,
		// Inspiratory pressure @ 50 (cmH2O)
		// Displayed on GUI
		CIRCUIT_PRESSURE_INSP_PRESSURE_AT_50,
		// Exhalation pressure @ 50 (cmH2O)
		// Displayed on GUI
		CIRCUIT_PRESSURE_EXH_PRESSURE_AT_50,
		// Inspiratory pressure @ 100 (cmH2O)
		// Displayed on GUI
		CIRCUIT_PRESSURE_INSP_PRESSURE_AT_100,
		// Exhalation pressure @ 100 (cmH2O)
		// Displayed on GUI
		CIRCUIT_PRESSURE_EXH_PRESSURE_AT_100,
		CIRCUIT_PRESSURE_TEST_END = CIRCUIT_PRESSURE_EXH_PRESSURE_AT_100,

		// SM_SST_PROX_TEST_ID
		PROX_SST_TEST_START = TEST_DATA_START,
		PX_CURRENT_SUB_TEST_ID = PROX_SST_TEST_START,
		
		PX_DATA_ITEM_1,
		PX_DATA_ITEM_2,
		PX_DATA_ITEM_3,
		PX_DATA_ITEM_4,
		PX_DATA_ITEM_5,
		PX_DATA_ITEM_6,
		PX_DATA_ITEM_7,
        PX_DATA_ITEM_8,
        
		PROX_SST_TEST_END = PX_DATA_ITEM_8,

		// SAFETY_SYSTEM_TEST_ID
		SAFETY_SYSTEM_TEST_START = TEST_DATA_START,
		// Safety valve open back pressure @ 60 LPM (cmH2O)
		// Displayed on GUI
		SAFETY_SYSTEM_INSPIRATORY_PRESSURE1 = SAFETY_SYSTEM_TEST_START,
		// Valve open current (mA)
		// Displayed on GUI
		SAFETY_SYSTEM_SAFETY_VALVE_CURRENT1,
		// Valve close current @ 0.1 seconds (mA)
		// Displayed on GUI
		SAFETY_SYSTEM_SAFETY_VALVE_CURRENT2,
		// Valve close current @ 0.3 seconds (mA)
		// Displayed on GUI
		SAFETY_SYSTEM_SAFETY_VALVE_CURRENT3,
		// Valve close current @ 1.0 seconds (mA)
		// Displayed on GUI
		SAFETY_SYSTEM_SAFETY_VALVE_CURRENT4,
		// Reverse flow pressure drop from 85 to 5 cmH2O (milliseconds)
		// Displayed on GUI
		SAFETY_SYSTEM_ELAPSED_TIME,
		SAFETY_SYSTEM_TEST_END = SAFETY_SYSTEM_ELAPSED_TIME,

		// EXH_VALVE_SEAL_TEST_ID
		EXH_VALVE_SEAL_TEST_START = TEST_DATA_START,
		// Exhalation Valve Motor Temperature (deg C)
		// Displayed on GUI
		EXH_VALVE_SEAL_EXH_MOTOR_TEMP = EXH_VALVE_SEAL_TEST_START,
		// Exhalation pressure drop (cmH2O)
		// Displayed on GUI
		EXH_VALVE_SEAL_DELTA_PRESSURE,
		EXH_VALVE_SEAL_TEST_END = EXH_VALVE_SEAL_DELTA_PRESSURE,

		// EXH_VALVE_TEST_ID
		EXH_VALVE_TEST_START = TEST_DATA_START,
		// Exh pressure at 10 cal point (cmH2O)
		// Displayed on GUI
		EXH_VALVE_EXHALATION_PRESSURE_10 = EXH_VALVE_TEST_START,
		// Exh pressure at 45.0 cal point (cmH2O)
		// Displayed on GUI
		EXH_VALVE_EXHALATION_PRESSURE_45,
		// Exh pressure at 95.0 cal point (cmH2O)
		// Displayed on GUI
		EXH_VALVE_EXHALATION_PRESSURE_95,
		EXH_VALVE_TEST_END = EXH_VALVE_EXHALATION_PRESSURE_95,

		// EXH_VALVE_CALIB_TEST_ID
		EXH_VALVE_CALIB_TEST_START = TEST_DATA_START,
		// Exh pressure @ 1.0 cal point (cmH2O)
		EXH_VALVE_CALIB_VALVE_PRESSURE1 = EXH_VALVE_CALIB_TEST_START,
		// Exh valve command @ 1.0 cal point (count)
		EXH_VALVE_CALIB_VALVE_CURRENT1,
		// Exh pressure @ 5.0 cal point (cmH2O)
		EXH_VALVE_CALIB_VALVE_PRESSURE2,
		// Exh valve command @ 5.0 cal point (count)
		EXH_VALVE_CALIB_VALVE_CURRENT2,
		// Exh pressure @ 10.0 cal point (cmH2O)
		EXH_VALVE_CALIB_VALVE_PRESSURE3,
		// Exh valve command @ 10.0 cal point (count)
		EXH_VALVE_CALIB_VALVE_CURRENT3,
		// Exh pressure @ 15.0 cal point (cmH2O)
		EXH_VALVE_CALIB_VALVE_PRESSURE4,
		// Exh valve command @ 15.0 cal point (count)
		EXH_VALVE_CALIB_VALVE_CURRENT4,
		// Exh pressure @ 20.0 cal point (cmH2O)
		EXH_VALVE_CALIB_VALVE_PRESSURE5,
		// Exh valve command @ 20.0 cal point (count)
		EXH_VALVE_CALIB_VALVE_CURRENT5,
		// Exh pressure @ 25.0 cal point (cmH2O)
		EXH_VALVE_CALIB_VALVE_PRESSURE6,
		// Exh valve command @ 25.0 cal point (count)
		EXH_VALVE_CALIB_VALVE_CURRENT6,
		// Exh pressure @ 30.0 cal point (cmH2O)
		EXH_VALVE_CALIB_VALVE_PRESSURE7,
		// Exh valve command @ 30.0 cal point (count)
		EXH_VALVE_CALIB_VALVE_CURRENT7,
		// Exh pressure @ 35.0 cal point (cmH2O)
		EXH_VALVE_CALIB_VALVE_PRESSURE8,
		// Exh valve command @ 35.0 cal point (count)
		EXH_VALVE_CALIB_VALVE_CURRENT8,
		// Exh pressure @ 40.0 cal point (cmH2O)
		EXH_VALVE_CALIB_VALVE_PRESSURE9,
		// Exh valve command @ 40.0 cal point (count)
		EXH_VALVE_CALIB_VALVE_CURRENT9,
		// Exh pressure @ 45.0 cal point (cmH2O)
		EXH_VALVE_CALIB_VALVE_PRESSURE10,
		// Exh valve command @ 45.0 cal point (count)
		EXH_VALVE_CALIB_VALVE_CURRENT10,
		// Exh pressure @ 50.0 cal point (cmH2O)
		EXH_VALVE_CALIB_VALVE_PRESSURE11,
		// Exh valve command @ 50.0 cal point (count)
		EXH_VALVE_CALIB_VALVE_CURRENT11,
		// Exh pressure @ 55.0 cal point (cmH2O)
		EXH_VALVE_CALIB_VALVE_PRESSURE12,
		// Exh valve command @ 55.0 cal point (count)
		EXH_VALVE_CALIB_VALVE_CURRENT12,
		// Exh pressure @ 60.0 cal point (cmH2O)
		EXH_VALVE_CALIB_VALVE_PRESSURE13,
		// Exh valve command @ 60.0 cal point (count)
		EXH_VALVE_CALIB_VALVE_CURRENT13,
		// Exh pressure @ 65.0 cal point (cmH2O)
		EXH_VALVE_CALIB_VALVE_PRESSURE14,
		// Exh valve command @ 65.0 cal point (count)
		EXH_VALVE_CALIB_VALVE_CURRENT14,
		// Exh pressure @ 70.0 cal point (cmH2O)
		EXH_VALVE_CALIB_VALVE_PRESSURE15,
		// Exh valve command @ 70.0 cal point (count)
		EXH_VALVE_CALIB_VALVE_CURRENT15,
		// Exh pressure @ 75.0 cal point (cmH2O)
		EXH_VALVE_CALIB_VALVE_PRESSURE16,
		// Exh valve command @ 75.0 cal point (count)
		EXH_VALVE_CALIB_VALVE_CURRENT16,
		// Exh pressure @ 80.0 cal point (cmH2O)
		EXH_VALVE_CALIB_VALVE_PRESSURE17,
		// Exh valve command @ 80.0 cal point (count)
		EXH_VALVE_CALIB_VALVE_CURRENT17,
		// Exh pressure @ 85.0 cal point (cmH2O)
		EXH_VALVE_CALIB_VALVE_PRESSURE18,
		// Exh valve command @ 85.0 cal point (count)
		EXH_VALVE_CALIB_VALVE_CURRENT18,
		// Exh pressure @ 90.0 cal point (cmH2O)
		EXH_VALVE_CALIB_VALVE_PRESSURE19,
		// Exh valve command @ 90.0 cal point (count)
		EXH_VALVE_CALIB_VALVE_CURRENT19,
		// Exh pressure @ 95.0 cal point (cmH2O)
		EXH_VALVE_CALIB_VALVE_PRESSURE20,
		// Exh valve command @ 95.0 cal point (count)
		EXH_VALVE_CALIB_VALVE_CURRENT20,
		// Exh pressure @ 100.0 cal point (cmH2O)
		EXH_VALVE_CALIB_VALVE_PRESSURE21,
		// Exh valve command @ 100.0 cal point (count)
		EXH_VALVE_CALIB_VALVE_CURRENT21,
		// Exh pressure @ 105.0 cal point (cmH2O)
		EXH_VALVE_CALIB_VALVE_PRESSURE22,
		// Exh valve command @ 105.0 cal point (count)
		EXH_VALVE_CALIB_VALVE_CURRENT22,
		// Exh pressure @ 108.0 cal point (cmH2O)
		EXH_VALVE_CALIB_VALVE_PRESSURE23,
		// Exh valve command @ 108.0 cal point (count)
		EXH_VALVE_CALIB_VALVE_CURRENT23,
		EXH_VALVE_CALIB_TEST_END = EXH_VALVE_CALIB_VALVE_CURRENT23,

		// SM_LEAK_TEST_ID
		SM_LEAK_TEST_START = TEST_DATA_START,
		// Initial pressure (cmH2O)
		SM_LEAK_INSPIRATORY_PRESSURE1 = SM_LEAK_TEST_START,
		// The pressure monitored during the test (cmH2O)
		// Displayed on GUI (monitoring)
		SM_LEAK_MONITOR_PRESSURE,
		// Final pressure (cmH2O)
		SM_LEAK_INSPIRATORY_PRESSURE2,
		// Pressure drop over 10 seconds (cmH2O)
		// Displayed on GUI
		SM_LEAK_DELTA_PRESSURE,
		SM_LEAK_TEST_END = SM_LEAK_DELTA_PRESSURE,

		// SST_LEAK_TEST_ID
		SST_LEAK_TEST_START = TEST_DATA_START,
		// Initial pressure (cmH2O)
		SST_LEAK_INSPIRATORY_PRESSURE1 = SST_LEAK_TEST_START,
		// The pressure monitored during the test (cmH2O)
		// Displayed on GUI (monitoring)
		SST_LEAK_MONITOR_PRESSURE,
		// Final pressure (cmH2O)
		SST_LEAK_INSPIRATORY_PRESSURE2,
		// Pressure drop over 10 seconds (cmH2O)
		// Displayed on GUI
		SST_LEAK_DELTA_PRESSURE,
		SST_LEAK_TEST_END = SST_LEAK_DELTA_PRESSURE,

		// PRESSURE_SENSOR_TEST_ID
		PRESSURE_SENSOR_TEST_START = TEST_DATA_START,
		// Insp pressure @ 45 cmH2O (cmH2O)
		PRESSURE_SENSOR_INSPIRATORY_PRESSURE = PRESSURE_SENSOR_TEST_START,
		// Exh pressure @ 45 cmH2O (cmH2O)
		PRESSURE_SENSOR_EXHALATION_PRESSURE,
		PRESSURE_SENSOR_TEST_END = PRESSURE_SENSOR_EXHALATION_PRESSURE,

		// COMPLIANCE_CALIB_TEST_ID
		COMPLIANCE_CALIB_TEST_START = TEST_DATA_START,
		// High compliance at low flow (ml/cmH2O)
		COMPLIANCE_CALIB_HIGH_COMPLIANCE = COMPLIANCE_CALIB_TEST_START,
		// Low flow pressurize time (msec)
		COMPLIANCE_CALIB_LOW_FLOW_TIME,
		// Low compliance at high flow (ml/cmH2O)
		COMPLIANCE_CALIB_LOW_COMPLIANCE,
		// High flow pressurize time (msec)
		COMPLIANCE_CALIB_HIGH_FLOW_TIME,
		// Nominal compliance (ml/cmH2O)
		// Displayed on GUI
		COMPLIANCE_CALIB_NOMINAL_COMPLIANCE,
		COMPLIANCE_CALIB_TEST_END = COMPLIANCE_CALIB_NOMINAL_COMPLIANCE,

		// EXH_HEATER_TEST_ID
		EXH_HEATER_TEST_START = TEST_DATA_START,
		// Initial temperature (degrees C)
		// Displayed on GUI
		EXH_HEATER_EXHALATION_TEMPERATURE1 = EXH_HEATER_TEST_START,
		// Temperature after heater on (degrees C)
		// Displayed on GUI
		EXH_HEATER_EXHALATION_TEMPERATURE2,
		// Temperature after heater off (degrees C)
		// Displayed on GUI
		EXH_HEATER_EXHALATION_TEMPERATURE3,
		EXH_HEATER_TEST_END = EXH_HEATER_EXHALATION_TEMPERATURE3,

		// GENERAL_ELECTRONICS_TEST_ID
		GENERAL_ELECTRONICS_TEST_START = TEST_DATA_START,
		// InspPressureSensor (cmH2O)
		// Displayed on GUI
		GENERAL_ELECTRONICS_READING0 = GENERAL_ELECTRONICS_TEST_START,
		// ExhPressureSensor (cmH2O)
		// Displayed on GUI
		GENERAL_ELECTRONICS_READING1,
		// AtmosphericPressureSensor (PSIA)
		// Displayed on GUI
		GENERAL_ELECTRONICS_READING2,
		// O2TemperatureSensor (degrees C)
		// Displayed on GUI
		GENERAL_ELECTRONICS_READING3,
		// AirTemperatureSensor (degrees C)
		// Displayed on GUI
		GENERAL_ELECTRONICS_READING4,
		// ExhGasTemperatureSensor (degrees C)
		// Displayed on GUI
		GENERAL_ELECTRONICS_READING5,
		// ExhVoiceCoilTemperatureSensor (degrees C)
		// Displayed on GUI
		GENERAL_ELECTRONICS_READING6,
		// ExhHeaterTemperatureSensor (degrees C)
		// Displayed on GUI
		GENERAL_ELECTRONICS_READING7,
		// O2FlowSensor (LPM)
		// Displayed on GUI
		GENERAL_ELECTRONICS_READING8,
		// AirFlowSensor (LPM)
		// Displayed on GUI
		GENERAL_ELECTRONICS_READING9,
		// ExhFlowSensor (LPM)
		// Displayed on GUI
		GENERAL_ELECTRONICS_READING10,
		// Fio2Monitor (O2%)
		// Displayed on GUI
		GENERAL_ELECTRONICS_READING11,
		// O2PsolCurrent (ma)
		// Displayed on GUI
		GENERAL_ELECTRONICS_READING12,
		// AirPsolCurrent (ma)
		// Displayed on GUI
		GENERAL_ELECTRONICS_READING13,
		// ExhMotorCurrent (ma)
		// Displayed on GUI
		GENERAL_ELECTRONICS_READING14,
		// SafetyValveCurrent (ma)
		// Displayed on GUI
		GENERAL_ELECTRONICS_READING15,
		// AcLineVoltage (Volts)
		// Displayed on GUI
		GENERAL_ELECTRONICS_READING16,
		// PowerFailCapVoltage (Volts)
		// Displayed on GUI
		GENERAL_ELECTRONICS_READING17,
		// Dc10vSentry (Volts)
		// Displayed on GUI
		GENERAL_ELECTRONICS_READING18,
		// Dc15vSentry (Volts)
		// Displayed on GUI
		GENERAL_ELECTRONICS_READING19,
		// DcNeg15vSentry (Volts)
		// Displayed on GUI
		GENERAL_ELECTRONICS_READING20,
		// Dc5vGui (Volts)
		// Displayed on GUI
		GENERAL_ELECTRONICS_READING21,
		// Dc12vGui (Volts)
		// Displayed on GUI
		GENERAL_ELECTRONICS_READING22,
		// Dc5vVentHead (Volts)
		// Displayed on GUI
		GENERAL_ELECTRONICS_READING23,
		// Dc12vSentry (Volts)
		// Displayed on GUI
		GENERAL_ELECTRONICS_READING24,
		// SystemBatteryVoltage (Volts)
		// Displayed on GUI
		GENERAL_ELECTRONICS_READING25,
		// SystemBatteryCurrent (Amps)
		// Displayed on GUI
		GENERAL_ELECTRONICS_READING26,
		// SystemBatteryModel (Volts)
		// Displayed on GUI
		GENERAL_ELECTRONICS_READING27,
		// AlarmCableVoltage (Volts)
		// Displayed on GUI
		GENERAL_ELECTRONICS_READING28,
		// SafetyValveSwitchedSide (Volts)
		// Displayed on GUI
		GENERAL_ELECTRONICS_READING29,
		// DacWrap (Volts)
		// Displayed on GUI
		GENERAL_ELECTRONICS_READING30,
		// AiPcbaRevision (Volts)
		// Displayed on GUI
		GENERAL_ELECTRONICS_READING31,
		// LowVoltageReference (Volts)
		// Displayed on GUI
		GENERAL_ELECTRONICS_READING32,
		GENERAL_ELECTRONICS_TEST_END = GENERAL_ELECTRONICS_READING32,

		// GUI_TOUCH_TEST_ID
		GUI_TOUCH_TEST_START = TEST_DATA_START,
		GUI_TOUCH_TEST_END = GUI_TOUCH_TEST_START,

		// GUI_SERIAL_PORT_TEST_ID
		GUI_SERIAL_PORT_TEST_START = TEST_DATA_START,
		GUI_SERIAL_PORT_TEST_END = GUI_SERIAL_PORT_TEST_START,

		// BATTERY_TEST_ID
		BATTERY_TEST_START = TEST_DATA_START,
		// Initial battery voltage (Volts)
		// Displayed on GUI
		BATTERY_VOLTAGE1 = BATTERY_TEST_START,
		// Discharging battery voltage (Volts)
		// Displayed on GUI
		BATTERY_VOLTAGE2,
		// Discharging battery delta voltage (Volts)
		// Displayed on GUI
		BATTERY_DELTA_VOLTAGE,
		BATTERY_TEST_END = BATTERY_DELTA_VOLTAGE,

		// BD_INIT_SERIAL_NUMBER_ID
		BD_INIT_SERIAL_NUMBER_START = TEST_DATA_START,
		BD_INIT_SERIAL_NUMBER_END = BD_INIT_SERIAL_NUMBER_START,

		// GUI_INIT_SERIAL_NUMBER_ID
		GUI_INIT_SERIAL_NUMBER_START = TEST_DATA_START,
		GUI_INIT_SERIAL_NUMBER_END = GUI_INIT_SERIAL_NUMBER_START,

		// CAL_INFO_DUPLICATION_ID
		CAL_INFO_DUPLICATION_START = TEST_DATA_START,
		CAL_INFO_DUPLICATION_END = CAL_INFO_DUPLICATION_START,

		// BD_DOWNLOAD_ID
		BD_DOWNLOAD_START = TEST_DATA_START,
		BD_DOWNLOAD_END = BD_DOWNLOAD_START,

		// GUI_DOWNLOAD_ID
		GUI_DOWNLOAD_START = TEST_DATA_START,
		GUI_DOWNLOAD_END = GUI_DOWNLOAD_START,

		// ATMOS_PRESSURE_READ_ID
		ATMOS_PRESSURE_READ_START = TEST_DATA_START,
		// Atmospheric pressure transducer pressure (mmHg)
		ATM_XDUCER_PRESSURE = ATMOS_PRESSURE_READ_START,
		ATMOS_PRESSURE_READ_END = ATM_XDUCER_PRESSURE,

		NUM_ITEMIZED_TEST_DATA_ITEM = EXH_VALVE_CALIB_TEST_END + 1
	};

};

#endif // SmDataId_HH
