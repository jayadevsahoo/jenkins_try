#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: TrendPlotBasis - A trend plotting base class that handles drawing
// of the static elements of all plots (grid, x and y axis labels etc).
//---------------------------------------------------------------------
//@ Interface-Description
//  The purpose of TrendPlotBasis is to handle all the basics of a trend
//  plot excepting the drawing of the plot data itself.
// 
//  TrendPlotBasis is designed to be used as a base class.  Derived classes 
//  need to define the update() method.  The update() method is called to
//  inform a plot class to draw the specified trend data set on the plot. 
//
//  Other methods which can be redefined by derived classes are: activate()
//  which is called before a plot object is displayed, allowing the trend
//  grid and other drawables to be properly laid out in preparation for
//  display, erase() which erases the plot data(not the plot's grid)
//  and setGridArea() which defines the area (x, y, width, height) within the
//  plot's full area in which the grid itself is drawn.
//
//  The trend plot size is fixed. The Y-axis values are modifiable through the 
//  setY1Limits() method which gets called separately from the layoutGrid_() 
//  method when the axis is autoscaled. The X-axis values are extracted from
//  the TrendDataSet in the updateXAxis() method.
//
//---------------------------------------------------------------------
//@ Rationale
//  This class gathers all the functionality for displaying the basic
//  drawables of a trend plot grid in one place.
//---------------------------------------------------------------------
//@ Implementation-Description
//  An important method is layoutGrid_().  This method lays out the
//  plot and spaces out the grid.
//
//  TrendPlotBasis is a Container, which contains all the graphics make 
//  up the trend graphical display.  The most important graphic is
//  'plotContainer_', a PlotContainer object. All trend plotting is
//  performed within this container.  This is a protected member which is
//  accessible to derived classes so that they know where to draw the trend
//  data.
//---------------------------------------------------------------------
//@ Fault-Handling
//  No special fault handling apart from the usual assertions and pre-conditions
//  which verify the validity of arguments. SoftFault is used.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/TrendPlotBasis.ccv   25.0.4.0   19 Nov 2013 14:08:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: rhj      Date:  20-Jul-2007   SCR Number: 6384
//  Project:  Trend
//  Description:
//      Resolved the missing Trending data not available message.
//   
//  Revision: 001  By: gdc      Date:  15-Mar-2007   SCR Number: 6237
//  Project:  Trend
//  Description:
//  Initial Version
//   
//=====================================================================

#include <math.h>
#include "TrendPlotBasis.hh"
#include "BaseContainer.hh"
#include "MiscStrs.hh"
#include "TrendFormatData.hh"
#include "GuiAppClassIds.hh"
//@ Code...

static const Int32 X_VALUE_GAP_ = 5;
static const Int32 Y_VALUE_GAP_ = 4;
static const Int32 PLOT_LINE_THICKNESS_ = 1;
static const Int32 NUM_X_DIVISION_ = 6;
static const Int32 NUM_Y_DIVISION_ = 4;
static const Int32 Y_VALUE_OFFSET_ = 5;
static const Int32 EVENT_CONTAINER_HEIGHT_ = 4;

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TrendPlotBasis  [Default Constructor]
//
//@ Interface-Description
//  Default constructor for a TrendPlotBasis.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Initialize all data members.  Color and add all appropriate drawables.
//---------------------------------------------------------------------
//@ PreCondition
//	Ensures NUM_Y_DIVISION_ and NUM_X_DIVISION_ are greater than 1 and 
//  less than MAX_GRID_LINES_
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

TrendPlotBasis::TrendPlotBasis() 
:   displayXValues_(FALSE),
	upperY1Limit_(100.0f),
	lowerY1Limit_(0.0f),
	lastXPixel_(0),
	lastYPixel_(0),
	y1ScaleFactor_(0.0),
	isDataNull_(TRUE),
	showEvents_(FALSE),
	isDataNotAvail_(FALSE),
	trendSelectId_(TrendSelectValue::TREND_EMPTY)
{
	CALL_TRACE("TrendPlotBasis::TrendPlotBasis()");
	CLASS_PRE_CONDITION((NUM_X_DIVISION_ > 1) &&(NUM_X_DIVISION_ <= MAX_GRID_LINES_));
	CLASS_PRE_CONDITION((NUM_Y_DIVISION_ > 1) &&(NUM_Y_DIVISION_ <= MAX_GRID_LINES_));

	addDrawable(&plotContainer_);

	// set color for grid lines
	for (Uint32 i = 0; i < MAX_GRID_LINES_; i++)
	{
		xGridLines_[i].setColor(Colors::MEDIUM_BLUE);
		yGridLines_[i].setColor(Colors::MEDIUM_BLUE);
	}

	// set color for axis lines
	xAxisLine_.setColor(Colors::LIGHT_BLUE);
	yAxisLine_.setColor(Colors::LIGHT_BLUE);    

	plotLine_.setPenWidth( PLOT_LINE_THICKNESS_ );      
	// The plot line is not part of normal refresh
	plotLine_.setShow(FALSE);

	// set parameters for data not available message
	dataNotAvailableMsg_.setText(MiscStrs::TREND_DATA_NOT_AVAILABLE_MSG);
	dataNotAvailableMsg_.setColor(Colors::WHITE );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~TrendPlotBasis  [Destructor]
//
//@ Interface-Description
//  Destroys a TrendPlotBasis.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

TrendPlotBasis::~TrendPlotBasis(void)
{
	CALL_TRACE("TrendPlotBasis::TrendPlotBasis(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activate
//
//@ Interface-Description
//  Activates a TrendPlotBasis.  Should be called just before display of
//  the plot.  Derived classes which redefine this method should always
//  call this method first in the overridden method definition.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Simply call layoutGrid_() to setup the plot's grid.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void TrendPlotBasis::activate(void)
{
	CALL_TRACE("TrendPlotBasis::activate(void)");

	layoutGrid_();
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: erase
//
//@ Interface-Description
//  Erases a trend plot.
//---------------------------------------------------------------------
//@ Implementation-Description
// Because the line segments of a trend plot are not known to the
// GUI-Foundations drawable tree (they are drawn using PlotContainer's
// plotLine() method which bypasses the normal drawing scheme of
// GUI-Foundations) we must do the following in order to erase the plot:
// 1) Toggle the show flag of the plotContainer_ (this marks the
// plotContainer_ object as being changed)
// 2) Call the base container's repaint() method (this causes a full
// redraw of plotContainer_)
//
// This request is ignored if the object is not visible
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void TrendPlotBasis::erase(void)
{
	CALL_TRACE("TrendPlotBasis::erase(void)");

    if (isVisible())
	{				
		plotContainer_.setShow(FALSE);
		plotContainer_.setShow(TRUE);
		eventsContainer_.setShow(FALSE);
		eventsContainer_.setShow(showEvents_);

		const Uint numXGridLines = NUM_Y_DIVISION_ + 1;
		AUX_CLASS_ASSERTION((numXGridLines > 1) &&(numXGridLines <= MAX_GRID_LINES_), numXGridLines);

		Uint32 i = 0;
		for (i = 0; i < numXGridLines; i++)
		{
			yValues_[i].setShow(FALSE);
		}

		const Uint numYGridLines = NUM_X_DIVISION_ + 1; 
		AUX_CLASS_ASSERTION((numYGridLines > 1) &&(numYGridLines <= MAX_GRID_LINES_), numYGridLines);
		for (i = 0; i < numYGridLines; i++)
		{
			timeLabels_[i].setShow(FALSE);
			dateLabels_[i].setShow(FALSE);
		}

		getBaseContainer()->repaint();
	}	  
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setY1Limits
//
//@ Interface-Description
//  Sets the min and max limits of the Y-axis.  Passed the following
//  arguments:
//	upperY1Limit				The upper limit of the Y axis.
//	lowerY1Limit				The lower limit of the Y axis.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Store the passed in values and call gridChanged_() which will do what needs
//  to be done with the changes.
//---------------------------------------------------------------------
//@ PreCondition
//	Ensures the lower limit is less than the upper limit
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void TrendPlotBasis::setY1Limits(Real32 upperY1Limit, Real32 lowerY1Limit)
{
	CALL_TRACE("TrendPlotBasis::setYLimits(Int32 upperYLimit, Int32 lowerYLimit, "
			   "Uint32 yDivision, Boolean displayPositiveYValues)");

	CLASS_PRE_CONDITION((lowerY1Limit < upperY1Limit));

	// Store new values
	upperY1Limit_           = upperY1Limit;
	lowerY1Limit_           = lowerY1Limit;

	// Note that the grid has changed
	gridChanged_();	
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setGridArea
//
//@ Interface-Description
//  Sets the area occupied by the grid within the area of the TrendPlotBasis
//  object itself.  The area is passed via x, y, width and height values.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Store the new area and call gridChanged_() which will do what needs
//  to be done with the changes.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void TrendPlotBasis::setGridArea(Uint32 x, Uint32 y, Uint32 width, Uint32 height)
{
	CALL_TRACE("TrendPlotBasis::setGridArea(Uint32 x, Uint32 y, Uint32 width, "
			   "Uint32 height)");

	lastXPixel_ = width - 1;
	lastYPixel_ = height - 1;

	plotContainer_.setX(x);
	plotContainer_.setY(y);
	plotContainer_.setWidth(width);
	plotContainer_.setHeight(height);
	eventsContainer_.setX(x);
	eventsContainer_.setY(y - EVENT_CONTAINER_HEIGHT_);
	eventsContainer_.setWidth(width);
	eventsContainer_.setHeight(EVENT_CONTAINER_HEIGHT_);

	// Note that the grid has changed
	gridChanged_();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: layoutGrid_
//
//@ Interface-Description
//  Lays out the grid. Does not display any scale values. 
//---------------------------------------------------------------------
//@ Implementation-Description
//  Basically, lay out the grid and all appropriate labels according to
//  the values stored in various data members.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
 
void TrendPlotBasis::layoutGrid_(void)
{
	CALL_TRACE("TrendPlotBasis::layoutGrid_(void)");

	// Temporary buffer for stuffing numbers into
	wchar_t valueBuf[128];

	// Translate X and Y values from the grid's coordinate
	// system into on-screen pixel coordinates.
	calculateScaleFactors_();

	// Clear the plot of drawables before beginning the layout
	removeAllDrawables();
	plotContainer_.removeAllDrawables();
	addDrawable(&plotContainer_);
	eventsContainer_.removeAllDrawables();
	addDrawable(&eventsContainer_);

	// First thing we do is add and position the "Y grid lines", the
	// vertical lines parallel to the Y axis which mark the divisions
	// of the X axis.  Calculate how many we need.
	Uint numYGridLines = NUM_X_DIVISION_ + 1; 

	AUX_CLASS_ASSERTION((numYGridLines > 1) &&(numYGridLines <= MAX_GRID_LINES_), numYGridLines);

	Int32 i;
	for (i = 0; i < numYGridLines; i++)
	{
		// Calculate the X coordinate where the corresponding Y grid line will be displayed.
		Int32 xPos =  (i * ((lastXPixel_+1)/ NUM_X_DIVISION_)); 
		// Do we need to label the Y grid line with its X axis value?
		if (displayXValues_)
		{
			// Put the X value into cheap text and set and position the
			// string which displays it. Also, initialize the graph with 
			// no time stamp values.
			// use zeroes to size and center time label 
			swprintf(valueBuf, L"{p=8,y=8:00:00}");
			timeLabels_[i].setText(valueBuf);
			timeLabels_[i].setX(plotContainer_.getX() + xPos - timeLabels_[i].getWidth() / 2);
			timeLabels_[i].setY(plotContainer_.getY() + lastYPixel_ + X_VALUE_GAP_);
			swprintf(valueBuf, L"{p=8,y=8: }");
			timeLabels_[i].setText(valueBuf);
			timeLabels_[i].setShow(TRUE);

			swprintf(valueBuf, L"{p=8,y=8:00/00}");
			dateLabels_[i].setText(valueBuf);
			dateLabels_[i].setX(plotContainer_.getX() + xPos - dateLabels_[i].getWidth() / 2);
			dateLabels_[i].setY(plotContainer_.getY() + lastYPixel_ + X_VALUE_GAP_ + 10);
			swprintf(valueBuf, L"{p=8,y=8: }");
			dateLabels_[i].setText(valueBuf);
			dateLabels_[i].setShow(TRUE);

			if ((i == 0) || (i == (numYGridLines - 1)))
			{									 
				// The first and last X value label are displayed in white,
				timeLabels_[i].setColor(Colors::WHITE);
				dateLabels_[i].setColor(Colors::WHITE);
			}
			else
			{									 
				// otherwise color it medium grey.
				timeLabels_[i].setColor(Colors::LIGHT_BLUE);
				dateLabels_[i].setColor(Colors::LIGHT_BLUE);
			}

			// Display the label
			addDrawable(&timeLabels_[i]);
			addDrawable(&dateLabels_[i]);
		}										 

		// Position and add the Y grid line
		yGridLines_[i].setStartPoint(xPos, 0);
		yGridLines_[i].setEndPoint(xPos, lastYPixel_);
		yGridLines_[i].setColor(Colors::MEDIUM_BLUE);
		plotContainer_.addDrawable(&yGridLines_[i]);
	}

	// Position Y axis
	Int32 yAxisXPos =    0;	
	yAxisLine_.setStartPoint(yAxisXPos, 0);
	yAxisLine_.setEndPoint(yAxisXPos, lastYPixel_);

	// Add and position the "X grid lines", the horizontal lines parallel
	// to the X axis which mark the divisions of the Y axis.  Calculate
	// how many we need.
	Uint numXGridLines = NUM_Y_DIVISION_ + 1;
	AUX_CLASS_ASSERTION((numXGridLines > 1) && (numXGridLines <= MAX_GRID_LINES_), numXGridLines);

	static wchar_t tmpBuffer1[40];

	for (i = 0; i < numXGridLines; i++)
	{
		// Calculate the Y coordinate where the corresponding X grid line will be displayed 
		Int32 yPos = (lastYPixel_+1 ) -  (i * ((lastYPixel_+1 ) / NUM_Y_DIVISION_));

		Real32 y1Value = lowerY1Limit_ + (upperY1Limit_ - lowerY1Limit_) / NUM_Y_DIVISION_ * i;

		// Put the Y value into cheap text and set and position the string which displays it.
		if (isDataNull_)
		{
			swprintf(valueBuf, L"{p=8,y=8:--}");
		}
		else
		{
			TrendFormatData::FormatData(tmpBuffer1, trendSelectId_, y1Value);
			swprintf(valueBuf, L"{p=8,y=8:%s}", tmpBuffer1);
		}
		yValues_[i].setText(valueBuf);
		yValues_[i].setX(plotContainer_.getX() - yValues_[i].getWidth() - Y_VALUE_GAP_);
		yValues_[i].setShow(TRUE);

        if (i == 0)
		{
			// The first Y value label is displayed in white
			yValues_[i].setColor(Colors::WHITE);
			yValues_[i].setY(yPos + Y_VALUE_OFFSET_ - yValues_[i].getHeight() / 2 - 3);
		}
		else if (i == (numXGridLines - 1))
		{
			// The last Y value label is displayed in white, 
			yValues_[i].setColor(Colors::WHITE);
			yValues_[i].setY(yPos + Y_VALUE_OFFSET_ - yValues_[i].getHeight() / 2 + 3);
		}
		else
		{
			// otherwise color it light blue
			yValues_[i].setColor(Colors::LIGHT_BLUE);
			yValues_[i].setY(yPos + Y_VALUE_OFFSET_ - yValues_[i].getHeight() / 2);
			yValues_[i].setShow(FALSE);
		}

		// Display the label
		addDrawable(&yValues_[i]);

		// Position and add the X grid line
		xGridLines_[i].setStartPoint(0, yPos);
		xGridLines_[i].setEndPoint(lastXPixel_, yPos);
		plotContainer_.addDrawable(&xGridLines_[i]);
	}

	// Position the X-axis line
	Int32 xAxisYPos = lastYPixel_;
	xAxisLine_.setStartPoint(0, xAxisYPos);
	xAxisLine_.setEndPoint(lastXPixel_, xAxisYPos);

	// Add X and Y axis at the end to make sure they are on top of everything else
	plotContainer_.addDrawable(&xAxisLine_);
	plotContainer_.addDrawable(&yAxisLine_);

	// Add the plot line drawn using Direct Draw only
	plotContainer_.addDrawable(&plotLine_);

	addDrawable(&eventsContainer_);

	// Add the event marker that is drawn using Direct Draw only
	eventsContainer_.addDrawable(&eventMarker_);

	if (isDataNotAvail_)
	{
		dataNotAvailableMsg_.setX((plotContainer_.getWidth()/2 ) -
								  (dataNotAvailableMsg_.getWidth()/2 ) );

		dataNotAvailableMsg_.setY((plotContainer_.getHeight()/2 ) - 5);
		plotContainer_.addDrawable(&dataNotAvailableMsg_);
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: calculateScaleFactors_
//
//@ Interface-Description
//  Calculates the scaling factors which are used to convert X and Y 
//  data points into corresponding X and Y pixel coordinates within the
//  trend plot area (the grid).
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//  Ensures the lower limits are less than the upper limits
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void TrendPlotBasis::calculateScaleFactors_(void)
{
	CALL_TRACE("TrendPlotBasis::calculateScaleFactors_(void)");

	AUX_CLASS_PRE_CONDITION((lowerY1Limit_ < upperY1Limit_),lowerY1Limit_);
	y1ScaleFactor_ = Real32(lastYPixel_) / (upperY1Limit_ - lowerY1Limit_);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateXAxis
//
//@ Interface-Description
//  Labels the X axis with timestamps extracted from the trended data. 
//---------------------------------------------------------------------
//@ Implementation-Description
//  Loops through the trend data set. Associates the available data with
//  a y-axis grid line and extracts the timestamp from the record.
// 
//  Satisfies requirements:
//  [[TR01183] [PRD 1511, 1513]The timestamps shall be displayed below
//  the X-axis of the bottom trend graph...
//  [[TR01185] [PRD 1511]  The X-axis timestamps shall be displayed as
//  "HH:MM" with "MM/DD" ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
 
void TrendPlotBasis::updateXAxis(const TrendDataSet& rTrendDataSet)
{
	CALL_TRACE("TrendPlotBasis::updateXAxis( TrendDataSet & rTrendDataSet)");

	// Check if this plot can display X axis labels.
	if (displayXValues_)
	{
		wchar_t timeBuf[40];
		wchar_t dateBuf[40];

		// index of the X axis labels.
		Int32 index = NUM_X_DIVISION_;
		const Int32 TICK_INTERVAL_ = lastXPixel_ / NUM_X_DIVISION_;

		for (Int32 row = 0; row < TrendDataMgr::MAX_ROWS; row++)
		{
			if ((row == 0) || ((row + 1) % TICK_INTERVAL_ == 0))
			{
				if (rTrendDataSet.getTrendTimeStamp(row).isInvalid())
				{
					swprintf(timeBuf, L"{p=8,y=8:------}");
					swprintf(dateBuf, L"{p=8,y=8: }");
				}
				else
				{
					TextUtil::FormatTime(timeBuf, L"{p=8,y=8:%H:%M}", rTrendDataSet.getTrendTimeStamp(row));
                    TextUtil::FormatTime(dateBuf, L"{p=8,y=8:%p}", rTrendDataSet.getTrendTimeStamp(row));
				}
				timeLabels_[index].setText(timeBuf);
				timeLabels_[index].setShow(TRUE);
				dateLabels_[index].setText(dateBuf);
				dateLabels_[index].setShow(TRUE);
				index--;
			}
		}
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: autoScaleYAxis
//
//@ Interface-Description
//  Autoscales the Y Axis with the given Trend Data Set.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Loops through the data set and saves the highest value and the lowest
//  value for the autoscale factors. These values are expanded by 10% and
//  serve as the lower and upper bound on the autoscale.
//  
//  Satisfies requirements:
//  [[TR02179] [PRD 1510] Each graph shall display the minimum and
//   maximum range values (over the current timescale) ...
//  [[TR01186] [PRD 1510] The Trend Data minimum and maximum range
//  values shall be automatically calculated (autoscaled)...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void TrendPlotBasis::autoScaleYAxis(const TrendDataSet& rTrendDataSet, Uint16 dataColumn)
{
	CALL_TRACE("autoScaleYAxis(TrendDataSet&, Uint16)");

	const TrendDataValues& trendData = rTrendDataSet.getTrendDataValues(dataColumn);

	trendSelectId_ = trendData.getId();
	Uint16 numRows = rTrendDataSet.getDataSetSize();   

	isDataNull_ = FALSE;
	Uint16 firstValidRow = 0;

	isDataNotAvail_ = (trendSelectId_ == TrendSelectValue::TREND_DATA_NOT_AVAILABLE);

	if (isDataNotAvail_ || !trendData.isValid() )
	{
		// no data so the graph cannot be scaled
		isDataNull_ = TRUE;
		setY1Limits( 100, 0 );
	}
	else
	{
		// check if we have any valid (non-gap) data to scale for this parameter
		Boolean isAnyDataValid = FALSE;

		for (Uint32 row = 0; row < numRows && !isAnyDataValid; row++)
		{
			const TrendValue & trendValue = trendData.getValue( row );
			isAnyDataValid = trendValue.isValid;
			firstValidRow = row;
		}
		// All data is invalid for this parameter. Nothing to display
		if (!isAnyDataValid)
		{
			isDataNull_ = TRUE;
			setY1Limits( 100, 0 );
		}
	}

	// Valid data to display
	if (!isDataNull_)
	{
		const TrendValue & trendValue = trendData.getValue( firstValidRow );
		Real32 highValue = trendValue.data.realValue;
		Real32 lowValue  = trendValue.data.realValue;

		// loop through data to find floor and ceiling values for autoscale
		for (Uint32 row = firstValidRow; row < numRows; row++)
		{
			const TrendValue & trendValue = trendData.getValue( row );
			if (trendValue.isValid)
			{
				// Get the highest value of the Trend data and store it.
				if (highValue < trendValue.data.realValue)
				{
					highValue = trendValue.data.realValue; 
				}

				// Get the lowest value of the Trend data and store it.
				if (lowValue > trendValue.data.realValue)
				{
					lowValue = trendValue.data.realValue;
				}
			}
		}

		const Real32 SCALE_PERCENT = 0.10;
		Real32 dataRange = highValue - lowValue;

		// for small/zero ranges less than 1, default to a minimum range of 1
		if (dataRange < 1.0)
		{
			dataRange = 1.0;
		}

		// scalingBuffer is the amount to add/subtract from the highest/lowest 
		// values in the data range to represent the highest/lowest Y values
		// on the graph
		Real32 scalingBuffer = dataRange * SCALE_PERCENT;

		lowValue  = floor(lowValue - scalingBuffer);
		highValue = ceil(highValue + scalingBuffer);

		// Set the Y scale limits.
		setY1Limits( highValue, lowValue );
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void TrendPlotBasis::SoftFault(const SoftFaultID  softFaultID,
							   const Uint32       lineNumber,
							   const char*        pFileName,
							   const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, TRENDPLOTBASIS,
							lineNumber, pFileName, pPredicate);
}

