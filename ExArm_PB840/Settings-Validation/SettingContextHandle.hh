
#ifndef SettingContextHandle_HH
#define SettingContextHandle_HH

//====================================================================
// This is a  proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: SettingContextHandle - Handle to the Setting Contexts.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/SettingContextHandle.hhv   25.0.4.0   19 Nov 2013 14:27:42   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'GetSettingValue()' and 'GetSettingApplicability()'
//	   methods
//	*  removed all obsoleted callback manager implementation
//
//  Revision: 004   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//  
//  Revision: 003   By: sah    Date: 29-Sep-1997    DR Number: 2410
//  Project: Sigma (R8027)
//  Description:
//	Added new interface methods for the adjustment/acceptance of
//	the new Atmospheric Pressure Setting.
//  
//  Revision: 002   By: sah    Date: 07 Feb 1997    DR Number: 1730
//  Project: Sigma (R8027)
//  Description:
//	Provided method for Sys-Init to query whether a Vent-Startup
//	(i.e., New- or Same-Patient Setup) has completed.  The return
//	value of this new method will be used to help determine whether
//	the state of the BD settings is out-of-sync with the GUI settings.
//  
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//====================================================================

#include "BatchAdjustPhase.hh"
#include "ApneaCorrectionStatus.hh"
#include "Applicability.hh"
#include "ContextId.hh"
#include "SettingId.hh"

//@ Usage-Classes
#include "SettingValue.hh"
//@ End-Usage


class SettingContextHandle
{
  public:
    static SettingValue  GetSettingValue(
			        const ContextId::ContextIdType contextId,
				const SettingId::SettingIdType settingId
				        );

    static Applicability::Id  GetSettingApplicability(
				    const ContextId::ContextIdType contextId,
				    const SettingId::SettingIdType settingId
						     );

    //================================================================
    //  Methods provided for start-up functionality...
    //================================================================

    static Boolean  IsSamePatientValid     (void);
    static Boolean  HasVentStartupCompleted(void);

    static void  AdjustNewPatientBatch(const BatchAdjustPhase phase);

    static void  AcceptNewPatientBatch (void);
    static void  AcceptSamePatientBatch(void);


    //================================================================
    //  Methods provided for normal operation functionality...
    //================================================================

    static void                   AdjustVentSetupBatch(
    						const BatchAdjustPhase phase
						      );
    static ApneaCorrectionStatus  AcceptVentSetupBatch(void);

    static ApneaCorrectionStatus  AdjustApneaSetupBatch(
				     const BatchAdjustPhase phase,
				     const Boolean          isReEntry = FALSE
						       );
    static void                   AcceptApneaSetupBatch(void);

    static void                   AdjustBatch(void);
    static ApneaCorrectionStatus  AcceptBatch(void);

    static void  AdjustSstBatch(void);
    static void  AcceptSstBatch(void);

    static void  AdjustTimeDateBatch(void);
    static void  AcceptTimeDateBatch(void);

    static void  AdjustDciSetupBatch(void);
    static void  AcceptDciSetupBatch(void);

    static void  AdjustServiceSetupBatch(void);
    static void  AcceptServiceSetupBatch(void);

    static void  AdjustAtmPressureCal(const Real32 atmPressureValue);
    static void  AcceptAtmPressureCal(void);

    static Boolean  IsRecoverableBatchValid   (void);
    static void     RecoverStoredBatch        (void);
    static void     InvalidateRecoverableBatch(void);

    static Boolean  AreAnyBatchSettingsChanged(void);

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL, 
			   const char*       pPredicate = NULL);
  
  private:
    SettingContextHandle(const SettingContextHandle&);	// not implemented...
    SettingContextHandle(void);				// not implemented...
    ~SettingContextHandle(void);			// not implemented...
    void  operator=(const SettingContextHandle&);	// not implemented...
};


#endif // SettingContextHandle_HH 
