
#ifndef Sigma_HH
#define Sigma_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
// Filename:  Sigma - System-Wide Types, Macros, and Globals.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Utilities/vcssrc/Sigma.hhv   25.0.4.0   19 Nov 2013 14:35:14   pvcs  $
//
//@ Modifications
//
//   Revision: 004   By: gdc   Date: 07-APR-2008   SCR Number: 6407
//   Project:  TREND2
//   Description:
//		Added countof macro to replace multiple definitions in
//		multiple subsystems.
//
//   Revision: 003   By: gdc   Date: 16-Jun-1998   DCS Number: 5055
//   Project:  Color
//   Description:
//		Initial version.
//
//   Revision: 002   By: sah   Date: 06-Feb-1996   DCS Number: 674
//   Project:  Sigma (R8027)
//   Description:
//	Removed 'ifndef' and 'endif' from include line.
//
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//=====================================================================

#include <stddef.h>


//@ Usage-Class
//@ End-Usage

//=====================================================================
//
//  Sigma Standard Types...
//
//=====================================================================

#include "SigmaTypes.hh"


//=====================================================================
//
//  Sigma Standard Constants...
//
//=====================================================================

#include "SigmaConstants.hh"


//=====================================================================
//
//  Global Call-Trace Macros...
//
//=====================================================================

#include "CallTraceMacros.hh"


//=====================================================================
//
//  Global Fault-Handling Macros...
//
//=====================================================================

#include "FaultHandlerMacros.hh"


//=====================================================================
//
//  Global Utility Macros...
//
//=====================================================================

//@ Macro:  MAX_VALUE(val1, val2)
// Return the maximum value of the two values given by 'val1' and 'val2'.
#define MAX_VALUE(val1, val2)	(((val1) > (val2)) ? (val1) : (val2))

//@ Macro:  MIN_VALUE(val1, val2)
// Return the minimum value of the two values given by 'val1' and 'val2'.
#define MIN_VALUE(val1, val2)	(((val1) < (val2)) ? (val1) : (val2))

//@ Macro:  ABS_VALUE(val)
// Return the absolute value of the value given by 'val'.
#define ABS_VALUE(val)		(((val) < 0) ? -(val) : (val))

//@ Macro:  countof(array)
// Return the number of elements in an array
#define countof(array)		(sizeof(array) / sizeof(*array))


//=====================================================================
//
//  Global Placement New Operator...
//
//=====================================================================

#include "NewOperator.hh"


//=====================================================================
//
//  Overload stdio printf and sprintf...
//
//=====================================================================

#include "Stdio.hh"

#endif  // Sigma_HH
