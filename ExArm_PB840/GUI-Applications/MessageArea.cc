#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: MessageArea - The area at the bottom left of the Lower
// Screen currently reserved for displaying help messages explaining
// the symbols on setting buttons, patient data, alarm messages etc..
// This class is part of the GUI-Apps Framework cluster.
//---------------------------------------------------------------------
//@ Interface-Description
// There is only one instance of this class, stored in the LowerScreen class.
// It needs no construction parameters.
//
// Two public methods are provided, one to set a message (setMessage()) and one
// to clear a message (clearMessage()).  There are three priorities of messages
// provided, high, medium and low.  A high priority message, if set, is
// displayed.  Otherwise the next lowest priority message, if set, is
// displayed, etc. etc.  If no message is set the Message Area is blanked.
//---------------------------------------------------------------------
//@ Rationale
// This class was needed to manage the Message Area at the bottom left
// of the Lower screen.
//---------------------------------------------------------------------
//@ Implementation-Description
// The prioritized messages are stored in an array called stringIds_[], with
// one entry for each priority.  This array can be scanned from high priority
// to low priority, and the first non-null entry will be displayed.  The
// setMessage() and clearMessage() simply modify the content of the
// stringIds_[] array and then call the private updateArea_() function which
// determines which message, if any, to display.
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and pre-conditions
// to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
// Only one instance of this class should be created (by LowerScreen).
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/MessageArea.ccv   25.0.4.0   19 Nov 2013 14:08:06   pvcs  $
//
//@ Modification-Log
//
//  Revision: 008  By:  gdc    Date:  09-Aug-2010    SCR Number: 6663
//  Project:  API/MAT
//  Description:
//	Modified to allow for more low priority messages in message area.
//
//  Revision: 007  By:  gdc    Date:  09-Feb-2009    SCR Number: 6068
//  Project:  840S
//  Description:
//  Removed SUN prototype code.
//
//  Revision: 006  By:  gdc    Date:  10-Dec-2008    SCR Number: 6377
//  Project:  840S
//  Description:
//  Removed DELTA project single screen support.
//
//  Revision: 005   By: gdc    Date: 24-Sep-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal non-invasive CPAP.
//
//  Revision: 004  By: rhj    Date: 29-May-2007    SCR Number: 6237
//  Project:  Trend
//  Description:
//      Added SOFTWARE_SALES_LABEL as a default message.
// 
//  Revision: 003  By: gdc    Date: 28-Aug-2000   DCS Number:  5753
//  Project:  Delta
//  Description:
//      Implemented Single Screen option.
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "MessageArea.hh"
#include "BoundedValue.hh"
#include "DataKey.hh"
#include "Fio2EnabledValue.hh"
#include "GuiTimerRegistrar.hh"
#include "LowerScreen.hh"
#include "MiscStrs.hh"
// TODO E600 remove
//#include "OsUtil.hh"
#include "SettingConstants.hh"
#include "SettingContextHandle.hh"
#include "SoftwareOptions.hh"
#include "SoftwareRevNum.hh"
//@ Usage-Classes
//@ End-Usage

//@ Code...

// Initialize static constants.
static const Int32 CONTAINER_X_ = 0;
static const Int32 CONTAINER_Y_ = 456;
static const Int32 CONTAINER_WIDTH_ = 367;
static const Int32 CONTAINER_HEIGHT_ = 24;
static wchar_t  SalesLabel_[200];

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: MessageArea()  [Default Constructor]
//
//@ Interface-Description
// Constructs the Message Area. Only one instance should be constructed
// (by LowerScreen).
//---------------------------------------------------------------------
//@ Implementation-Description
// Sizes, positions and colors the area.  Initializes the stringIds_[]
// array to nulls.  Adds invisible message text to our container.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
 
MessageArea::MessageArea(void) : pSingleMsg_(NULL)
{
	CALL_TRACE("MessageArea::MessageArea(void)");

	// Size and position the area
	setX(CONTAINER_X_);
	setY(CONTAINER_Y_);
	setWidth(CONTAINER_WIDTH_);
	setHeight(CONTAINER_HEIGHT_);

	// Default background color is Extra Dark Grey
	setFillColor(Colors::EXTRA_DARK_BLUE);

	// Default text color is goldenrod
	setContentsColor(Colors::GOLDENROD);

	// Add invisible message text
	messageText_.setShow(FALSE);
	addDrawable(&messageText_);

	// Initialize the messages.
	for ( Uint16 i = 0; i < MA_NUMBER_OF_PRIORITIES; i++ )
	{
		stringIds_[i] = NULL_STRING_ID;
	}

	GuiTimerRegistrar::StartTimer(GuiTimerId::MESSAGE_AREA_TIMER, this);

	{
		Boolean  labelShowFlag = FALSE;

		const wchar_t*  pReleaseTypeStr = L"Development";

		switch ( SoftwareRelType::SOFTWARE_REL_TYPE )
		{
			case SoftwareRelType::DEVELOPMENT :             
				labelShowFlag = TRUE;
				pReleaseTypeStr = L"Development";
				break;
			case SoftwareRelType::PRE_PRODUCTION :          
				labelShowFlag = TRUE;
				pReleaseTypeStr = L"Pre-Release";
				break;
			case SoftwareRelType::PRODUCTION :              
				// this is an official release, therefore no need to show string for
				// this release...
				pReleaseTypeStr = L"Released";
				break;
		};

		const wchar_t*  pDataKeyTypeStr = L"Unformatted";

		switch ( SoftwareOptions::GetSoftwareOptions().getDataKeyType() )
		{
			case DataKey::PRODUCTION :
			case SoftwareOptions::STANDARD :                
				// this is an official datakey, therefore no need to show string for
				// this datakey...
				pDataKeyTypeStr = L"Customer";
				break;
			case SoftwareOptions::OPTION_FORMATTED :            
				labelShowFlag = TRUE;
				pDataKeyTypeStr = L"Unformatted";
				break;
			case SoftwareOptions::ENGINEERING :             
				labelShowFlag = TRUE;
				pDataKeyTypeStr = L"Engineering";
				break;
			case DataKey::DEMO :
			case SoftwareOptions::SALES :                   
				labelShowFlag = TRUE;
				pDataKeyTypeStr = L"Sales";
				break;
			case SoftwareOptions::MANUFACTURING :           
				labelShowFlag = TRUE;
				pDataKeyTypeStr = L"Manufacturing";
				break;
		};

		if ( labelShowFlag )
		{
			swprintf(SalesLabel_, MiscStrs::SOFTWARE_SALES_LABEL,
					pReleaseTypeStr, pDataKeyTypeStr);
			messageText_.setShow(TRUE);
			messageText_.setText(SalesLabel_);
		}
		else
		{
			SalesLabel_[0] = L'\0';
		}
	}

	updateArea_();
	// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~MessageArea  [Destructor]
//
//@ Interface-Description
// Destroys Message Area.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

MessageArea::~MessageArea(void)
{
	CALL_TRACE("MessageArea::~MessageArea(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setMessage
//
//@ Interface-Description
// Passed the StringId of a piece of text for display in the Message Area along
// with its priority (one of MessageArea::MA_HIGH, MessageArea::MA_LOW).
// Normally called by Setting buttons when they need to explain the meaning of
// their setting symbol or by patient data symbols in the Upper Screen when
// they are touched.  Persistent messages should use the low priority (such as
// the messages explaining a Setting button's symbol) and temporary messages
// should use the high priority (such as the patient data symbols which display
// a message only while the operator is touching the symbol).
//---------------------------------------------------------------------
//@ Implementation-Description
// Sets the appropriate entry in stringIds_[] to the string id, and
// then calls updateArea_() to reflect the change onscreen.
//
// $[01121] Message area must support three types of message ...
//---------------------------------------------------------------------
//@ PreCondition
// The priority must be valid and the string id must not be null.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
MessageArea::setMessage(StringId msgId, MessagePriority priority)
{
	CALL_TRACE("MessageArea::setMessage(StringId msgId, "
			   "MessagePriority priority)");
	CLASS_PRE_CONDITION((priority >= 0) &&
						(priority < MA_NUMBER_OF_PRIORITIES));
	CLASS_PRE_CONDITION(NULL_STRING_ID != msgId);

	// Store the string id in the appropriate entry in stringIds_ and
	// update the display.
	// already stored there)
	stringIds_[priority] = msgId;
	updateArea_();										// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: clearMessage
//
//@ Interface-Description
// Clears a message from the Message Area.  Normally called by Setting
// buttons if pressed up and they need to clear their help message.
// Needs one parameter, the priority of the message to be cleared.
//---------------------------------------------------------------------
//@ Implementation-Description
// Set the appropriate entry in stringIds_[] to null and then call
// updateArea_() to reflect the change onscreen.
//---------------------------------------------------------------------
//@ PreCondition
// The priority must be a valid one.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
MessageArea::clearMessage(MessagePriority priority)
{
	CALL_TRACE("MessageArea::clearMessage(MessagePriority priority)");

	CLASS_PRE_CONDITION((priority >= 0) &&
						(priority < MA_NUMBER_OF_PRIORITIES));

	// Clear the message (if not already cleared) and update the display.
	stringIds_[priority] = NULL_STRING_ID;
	updateArea_();										// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateArea_
//
//@ Interface-Description
// Needs no parameters.  Called when a message in the Message Area
// changes.  It updates the Message Area to reflect the current data.
//---------------------------------------------------------------------
//@ Implementation-Description
// Removes the message text object then scans down through the array of
// messages and displays the highest priority message found.
//
// $[01120] Message Area shall display no more than one message at a time.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
MessageArea::updateArea_(void)
{
	CALL_TRACE("MessageArea::updateArea_(void)");

	// By default hide the message
	messageText_.setShow(FALSE);

	// Scan down through each priority message
	for ( int i = 0; i < MA_NUMBER_OF_PRIORITIES; i++ )
	{
		// If this priority message is set then display it and we're done
		if ( stringIds_[i] != NULL_STRING_ID )
		{
			messageText_.setShow(TRUE);
			messageText_.setText(stringIds_[i]);
			break;
		}
	}

	messageText_.positionInContainer(GRAVITY_CENTER);

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: timerEventHappened
//
//@ Interface-Description
// Called when a timer that we started runs out, in this case the message
// area timer.  We are passed an enum id identifying the timer. Updates
// the scrolling low priority messages.
//---------------------------------------------------------------------
//@ Implementation-Description
// $[01323] If the user has set the O2 monitor setting to OFF, the message ...
// $[LC01005] if Apnea Interval is OFF, display a warning message...
//---------------------------------------------------------------------
//@ PreCondition
//	(timerId == GuiTimerId::MESSAGE_AREA_TIMER);
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
MessageArea::timerEventHappened(GuiTimerId::GuiTimerIdType timerId)
{
	CLASS_PRE_CONDITION(timerId == GuiTimerId::MESSAGE_AREA_TIMER);

	if ( !isVisible() )
	{
		return;
	}

	// toggle between all low priority messages 
	static Uint32 tripCount_ = 0;

	Int32 msgCount = 0;
	static const Int32 MAX_NUM_LOW_PRIORITY_MSGS = 4;
	StringId messages[MAX_NUM_LOW_PRIORITY_MSGS];

	for ( Int16 ix=0; ix<countof(messages); ix++ )
	{
		messages[ix] = NULL;
	}

	if ( SalesLabel_[0] != L'\0' )
	{
		messages[msgCount++] = SalesLabel_;
	}

	if ( GuiApp::GetGuiState() == STATE_ONLINE && !LowerScreen::RLowerScreen.isInPatientSetupMode() )
	{
		const BoundedValue  APNEA_INTERVAL =
		SettingContextHandle::GetSettingValue(ContextId::ACCEPTED_CONTEXT_ID,
											  SettingId::APNEA_INTERVAL);

		const DiscreteValue  FIO2_ENABLED =
		SettingContextHandle::GetSettingValue(ContextId::ACCEPTED_CONTEXT_ID,
											  SettingId::FIO2_ENABLED);

		if ( APNEA_INTERVAL == DEFINED_UPPER_ALARM_LIMIT_OFF )
		{
			messages[msgCount++] = MiscStrs::APNEA_DETECTION_DISABLED_MESSAGE;
		}
		if ( Fio2EnabledValue::NO_FIO2_ENABLED == FIO2_ENABLED )
		{
			messages[msgCount++] = MiscStrs::O2_SENSOR_DISABLED_MESSAGE;
		}
	}

#if defined(API_INVESTIGATIONAL_DEVICE)
	messages[msgCount++] = MiscStrs::INVESTIGATIONAL_DEVICE_MESSAGE;
#endif

	CLASS_ASSERTION(msgCount <= MAX_NUM_LOW_PRIORITY_MSGS);

	if ( msgCount == 0 )
	{
		clearMessage(MessageArea::MA_LOW);
		pSingleMsg_ = NULL;
	}
	else if (msgCount == 1 )
	{
		// to eliminate flashing when only a single message is displayed, 
		// set message only if it has changed
		if ( messages[ tripCount_ % msgCount ] != pSingleMsg_ )
		{
			pSingleMsg_ = messages[ tripCount_ % msgCount ];
			setMessage(pSingleMsg_, MessageArea::MA_LOW);
		}
	}
	else
	{
		setMessage(messages[ tripCount_ % msgCount ], MessageArea::MA_LOW);
		pSingleMsg_ = NULL;
	}

	++tripCount_;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
MessageArea::SoftFault(const SoftFaultID  softFaultID,
					   const Uint32       lineNumber,
					   const char*        pFileName,
					   const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, MESSAGEAREA,
							lineNumber, pFileName, pPredicate);
}

