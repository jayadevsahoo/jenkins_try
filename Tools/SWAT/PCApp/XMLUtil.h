//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file XMLUtil.h
//----------------------------------------------------------------------------


//-------------------------------------
// I N C L U D E S
//-------------------------------------
#include <xercesc/dom/DOMDocument.hpp>
#include <xercesc/dom/DOMNode.hpp>

/// A wraper around common XML processing.
namespace XMLUtil{

    using namespace XERCES_CPP_NAMESPACE;

	/// Load an XML file.
	/// 
	/// @param filename Filename.
	/// 
	/// @return A dom document containing a DOM repersentation of the XML file.
    DOMDocument *loadXMLDocument(char *filename);


	/// Save an XML tree to a file. 
	///
	/// @param filename Name of file to save the XML document to.
	/// @param doc Pointer to the XML document to save.
	/// 
	/// @return -1 on failure.
    int saveXMLDocument(char *filename, DOMNode *doc );


	/// A callback iterator used to aid in the processing of XML documents.
	///
	/// @param xpath XPATH of DOMNode's to process. 
	/// @param doc   XML Document.
	/// @param action Action to take for each node in the XML document described by the XPATH.
	/// @param action_object A pointer to the object in which the processing gets done.
    int forEachXpath(char *xpath, DOMDocument *doc, 
    int (*action)(DOMNode *ptr, void *t), void *action_object);

}


