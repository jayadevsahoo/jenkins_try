#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Nellcor Puritan Bennett Corporation of California.
//
//      Copyright (c) 1996, Nellcor Puritan Bennett Corporation
//=====================================================================

//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: Keyboard - GUI Keyboard Polling Handler
//---------------------------------------------------------------------
//@ Interface-Description
//  This class contains the method to poll the GUI keyboard during the
//  5ms cycle interrupt time.  It is called every 10ms or every other
//  5ms cycle to determine the state of the keyboard and report new
//  key events to the GUI-IO-Devices subsystem.
//---------------------------------------------------------------------
//@ Rationale
//  This class contains the polling method for the GUI keyboard.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Operating-System/vcssrc/Keyboard.ccv   25.0.4.0   19 Nov 2013 14:16:18   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002   By: sah    Date:  29-Sep-1997    DR Number: 1529
//  Project: Sigma (R8027)
//  Description:
//	Modified the key-polling mechanism to better filter the events
//	of a typical key press.  Changed to a two-stage "latching", where
//	transient event changes of less than 30ms are ignored.  Also,
//	added 'SoftFault()' method for handling assertions as "System"
//	assertions.  Finally, incorporated new module ID.
//
//   Revision 001   By: Gary Cederquist Date: 28-MAY-1997 DR Number: 2176
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version.
//
//=====================================================================
 
//=====================================================================
//
//      Includes...
//
//=====================================================================

#ifdef SIGMA_GUI_CPU

#include "Sigma.hh"
#include "Keyboard.hh"

// TODO E600
//#include <logio.h>
#undef const
#//include <vrtxil.h>
#include "IpcIds.hh"
#include "OsClassId.hh"

#ifdef SIGMA_UNIT_TEST
# define sc_qpost ut_qpost

  extern void   ut_qpost(int queue, VRTX_MSG msg, int * err);
  extern Uint8  SimKeyReg1;
  extern Uint8  SimKeyReg0;

  volatile Uint8 * const PKeyReg1 = &SimKeyReg1;
  volatile Uint8 * const PKeyReg0 = &SimKeyReg0;

#else
  volatile Uint8 * const PKeyReg1 = (Uint8 *)0xFFBEA030;
  volatile Uint8 * const PKeyReg0 = (Uint8 *)0xFFBEA031;
#endif

#define  KEY_CODES  ((*PKeyReg1 & 0x7f) << 7 | *PKeyReg0)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Poll [Static]
//
//@ Interface-Description
//  This method called every 10ms from the 5ms timer interrupt handler 
//  polls the GUI keyboard to determine if any new key events have 
//  occurred.  A new key state must remain in place for a minimum of
//  30ms before being considered an actual change.
//---------------------------------------------------------------------
//@ Implementation-Description
//  To address the problem from DCS #1529, a new "latching" scheme was
//  introduced whereas a state changes requires 30ms of consistency to
//  take affect.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void 
Keyboard::Poll()
{
    static Uint  InitKeyState_    = KEY_CODES;
    static Uint  LatchedKeyState_ = InitKeyState_;
    static Uint  NumInitHits_     = 0u;

    Uint  currKeyState = KEY_CODES;

    if ( currKeyState == InitKeyState_ ) 
    {                                                       // $[TI1.1]
        NumInitHits_++;
    }
    else
    {                                                       // $[TI1.2]
	InitKeyState_ = currKeyState;
        NumInitHits_  = 0u;
    }

    if ( NumInitHits_ == 3  &&  InitKeyState_ != LatchedKeyState_ )
    {                                                       // $[TI2.1]
	Int  status;

	// "latch" the new key state, because its been active for the
	// minimum de-bounce time...
	LatchedKeyState_ = InitKeyState_;

	// send the newly-latched key state to the key event handling
	// task...
        sc_qpost(KEYBOARD_POLL_Q, (VRTX_MSG)LatchedKeyState_, &status);
	AUX_CLASS_ASSERTION((status == RET_OK), status);
    }                                                       // $[TI2.2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SystemSoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
Keyboard::SoftFault(const SoftFaultID  softFaultID,
		    const Uint32       lineNumber,
		    const char*        pFileName,
		    const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, , )");
  FaultHandler::SystemSoftFault(softFaultID, OS_PLATFORM, KEYBOARD_ID,
  				lineNumber);
}



#endif // SIGMA_GUI_CPU
