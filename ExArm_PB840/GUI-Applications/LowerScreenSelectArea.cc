#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: LowerScreenSelectArea - The area in the Lower screen which
// contains the four subscreen select buttons for Vent Setup, Apnea
// Setup, Alarm Setup and Other Screens.
//---------------------------------------------------------------------
//@ Interface-Description
// Creates and displays the four "tab" buttons on the lower screen
// that select the Vent Setup, Apnea Setup, Alarm Setup and Other
// Screens subscreens.  Only one instance of this class should be
// created.
//
// After construction the initialize() method needs to be called in
// order to tie the subscreens to the tab buttons.
//
// One other method is provided, setBlank(), used for blanking this
// area (for the Same/New Patient Setup sequence).
//
// The guiEventHappened() method controls the display of this area and the
// associate subscreen.  Depending on the gui event, we either display, or
// hide the lower screen tab buttons.
//---------------------------------------------------------------------
//@ Rationale
// One class was needed to control the screen selection buttons for
// the Lower Screen.
//---------------------------------------------------------------------
//@ Implementation-Description
// The constructor simply creates the four tab buttons and formulates
// the display of the Lower Screen Select Area.  The initialize()
// method "connects" the tab buttons with their respective subscreens.
// Subscreen selection is automatically handled by the tab buttons.
//
// See the implementation description of UpperScreenSelectArea for
// more details.
//
// The ultimate control of the lower screen tab buttons display is based on
// the current gui event.  The gui event type, whether we are in patient setup
// mode, setting locked or settings transaction failed  controls the display of
// this area and the associate subscreen.
//---------------------------------------------------------------------
//@ Fault-Handling
//  none
//---------------------------------------------------------------------
//@ Restrictions
// Only one instance of this class should be created (by LowerScreen).
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/LowerScreenSelectArea.ccv   25.0.4.0   19 Nov 2013 14:08:06   pvcs  $
//
//@ Modification-Log
//
//  Revision: 010  By:  rhj    Date:  01-Apr-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//	Added "PF" symbol for proximal flow sensor.
//
//  Revision: 009  By:  gdc    Date:  10-Dec-2008    SCR Number: 6377
//  Project:  840S
//  Description:
//	Removed DELTA project single screen support.
//
//  Revision: 008   By: gdc    Date: 24-Sep-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal non-invasive CPAP.
//
//  Revision: 007   By:   rhj    Date: 07-July-2008   SCR Number: 6435
//  Project:  LEAK
//  Description:
//      Modified to support Leak Compensation.
//       
//  Revision: 006 By: RHJ    Date: 10-13-2006   DR Number: 
//  Project:  RESPM
//	Description:
//		Added an "RM" label on the Others Screen tab button.
//
//  Revision: 005 By: jja    Date: 03-May-2002   DR Number: 5908
//  Project:  VCP
//	Description:
//		Incorporated Remediation code review issue(s) -- commentary changes only.
//
//  Revision: 004   By: hhd    Date:  16-Nov-1999    DCS Number: 5327
//  Project: Neo-Mode
//  Description:
// 		Modified as part of a general cleanup effort for Neo-Mode.
//
//  Revision: 003   By: gdc    Date:  29-Jun-1999    DCS Number: 5327
//  Project: 840 Neonatal
//  Description:
//	Optimized graphics library. Implemented Direct Draw mode for drawing
//  outside of BaseContainer refresh cycle.
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "LowerScreenSelectArea.hh"

//@ Usage-Classes
#include "Image.hh"
#include "LowerSubScreenArea.hh"
#include "NovRamManager.hh"
#include "MiscStrs.hh"
#include "GuiEventRegistrar.hh"
#include "LowerScreen.hh"
#include "SIterR_Drawable.hh"
#include "VentSetupSubScreen.hh"
#include "ApneaSetupSubScreen.hh"
#include "AlarmSetupSubScreen.hh"
#include "VentStartSubScreen.hh"
#include "LowerOtherScreensSubScreen.hh"
#include "MoreSettingsSubScreen.hh"
#include "SoftwareOptions.hh"
#include "SettingContextHandle.hh"
#include "ContextMgr.hh"
#include "PatientCctTypeValue.hh"

//@ End-Usage

//@ Code...

// Initialize static constants.
static const Int32 CONTAINER_X_ = 0;
static const Int32 CONTAINER_Y_ = 407;
static const Int32 CONTAINER_WIDTH_ = 367;
static const Int32 CONTAINER_HEIGHT_ = 49;
static const Int32 BUTTON_WIDTH_ = 91;
static const Int32 BUTTON_HEIGHT_ = 49;
static const Int32 BUTTON_BORDER_ = 3;
static const Int32 ALARM_BITMAP_X_ = 18;
static const Int32 ALARM_BITMAP_Y_ = 3;
static const Int32 OTHER_SCREENS_BITMAP_X_ = 20;
static const Int32 OTHER_SCREENS_BITMAP_Y_ = 5;


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: LowerScreenSelectArea()  [Default Constructor]
//
//@ Interface-Description
// Constructs the Lower Screen Select Area.  Needs no parameters.  Created once
// as a member of LowerScreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// All four tab buttons are created and appended to this area's container.  A
// long white box is also displayed at the top of the area due to the fact that
// the top of the area sits on the white border of the Lower Subscreen Area
// The grey background of this area would trample on the white border except
// for the addition of this white box.
//
// Bitmaps are created for the Alarm Setup and Other Screens tab buttons since
// icons are used instead of text for these buttons.
//
// $[01064] The Lower Screen Select Area displays the following ...
// $[LC01012] the other screens icon shall indicate leak compensation option...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

LowerScreenSelectArea::LowerScreenSelectArea(void) :
	ventSetupTabButton_(0, 0, BUTTON_WIDTH_, BUTTON_HEIGHT_,
				Button::LIGHT_NON_LIT, BUTTON_BORDER_,
				Button::SQUARE, MiscStrs::VENT_SETUP_TAB_BUTTON_LABEL, NULL),
	apneaSetupTabButton_(BUTTON_WIDTH_, 0, BUTTON_WIDTH_,
				BUTTON_HEIGHT_, Button::LIGHT_NON_LIT,
				BUTTON_BORDER_, Button::SQUARE,
				MiscStrs::APNEA_SETUP_TAB_BUTTON_LABEL, NULL),
	alarmSetupTabButton_(2 * BUTTON_WIDTH_, 0, BUTTON_WIDTH_,
				BUTTON_HEIGHT_, Button::LIGHT_NON_LIT,
				BUTTON_BORDER_, Button::SQUARE, 
				MiscStrs::ALARM_SETUP_TAB_BUTTON_LABEL, NULL),
	lowerOtherScreensTabButton_(3 * BUTTON_WIDTH_, 0, BUTTON_WIDTH_,
				BUTTON_HEIGHT_, Button::LIGHT_NON_LIT,
				BUTTON_BORDER_, Button::SQUARE, NULL, NULL),
	alarmSetupBitmap_(Image::RMonoAlarmIcon),
	lowerOtherScreensBitmap_(Image::ROtherScreensIcon),
	topLineBox_(0, 0, CONTAINER_WIDTH_, BUTTON_BORDER_)
{
	CALL_TRACE("LowerScreenSelectArea::LowerScreenSelectArea(void)");

	// Size and position the area
	setX(CONTAINER_X_);
	setY(CONTAINER_Y_);
	setWidth(CONTAINER_WIDTH_);
	setHeight(CONTAINER_HEIGHT_);

	// Default background color is Extra Dark Grey
	setFillColor(Colors::EXTRA_DARK_BLUE);

	// Register for GUI event change callbacks.
	GuiEventRegistrar::RegisterTarget(this);

	// Add white line at top
	topLineBox_.setColor(Colors::FRAME_COLOR);
	topLineBox_.setFillColor(Colors::FRAME_COLOR);
	addDrawable(&topLineBox_);

	//
	// Position then add the bitmaps to the screen select buttons.
	//
	alarmSetupBitmap_.setX(ALARM_BITMAP_X_);
	alarmSetupBitmap_.setY(ALARM_BITMAP_Y_);
	//alarmSetupTabButton_.addLabel(&alarmSetupBitmap_);

    lowerOtherScreensTabButton_.addLabel(&lowerOtherScreensBitmap_);


	// Add screen select buttons to container
	addDrawable(&ventSetupTabButton_);
	addDrawable(&apneaSetupTabButton_);
	addDrawable(&alarmSetupTabButton_);
	redrawButtonLabels_(FALSE);
	addDrawable(&lowerOtherScreensTabButton_);			// $[TI1]


	// Register for PROX INSTALLED Event Data 
	BdEventRegistrar::RegisterTarget(EventData::PROX_INSTALLED, this);


}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~LowerScreenSelectArea  [Destructor]
//
//@ Interface-Description
// Destroys the Lower Screen Select Area.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

LowerScreenSelectArea::~LowerScreenSelectArea(void)
{
	CALL_TRACE("LowerScreenSelectArea::~LowerScreenSelectArea(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: initialize
//
//@ Interface-Description
// Called immediately after construction by LowerScreen.  Performs
// initialization of any members that need it.
//---------------------------------------------------------------------
//@ Implementation-Description
// Points the tab buttons to their relevant subscreens.  This could
// not be done on construction as it is questionable as to whether
// the subscreens are created before or after this area.
//
// $[01065] The buttons shall form a mutually exclusive group ...
// $[00503] During normal operating mode, all alarm limits must be accessible
// within 1 navigational step...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
LowerScreenSelectArea::initialize(void)
{
	CALL_TRACE("LowerScreenSelectArea::initialize(void)");

	// Connect each button to the subscreen which they select
	ventSetupTabButton_.setSubScreen(LowerSubScreenArea::GetVentSetupSubScreen());
	apneaSetupTabButton_.setSubScreen(LowerSubScreenArea::GetApneaSetupSubScreen());
	alarmSetupTabButton_.setSubScreen(LowerSubScreenArea::GetAlarmSetupSubScreen());
	lowerOtherScreensTabButton_.setSubScreen(LowerSubScreenArea::GetLowerOtherScreensSubScreen());
														// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setBlank
//
//@ Interface-Description
// Passed a boolean, 'blank', which tells Lower Screen Select Area to either
// blank its area (make all drawable contents invisible) or not.
// This is needed by LowerScreen when it is displaying the Same/New
// Patient Setup sequence.
//---------------------------------------------------------------------
//@ Implementation-Description
// Go through the members of this area's container and set each
// drawable's show flag to be the inverse of 'blank'.  However,
// topLineBox_ must always be shown.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
LowerScreenSelectArea::setBlank(Boolean blank)
{
	CALL_TRACE("LowerScreenSelectArea::setBlank(Boolean blank)");

	// Set the show flag on all drawable contents appropriately
	SIterR_Drawable children(*getChildren_());
	Drawable *pDraw;
  	for (SigmaStatus ok = children.goFirst();
			(SUCCESS == ok) && (pDraw= (Drawable *)&(children.currentItem()));
			ok = children.goNext())
	{
		pDraw->setShow(!blank);
	}

	// The Top Line box must be shown at all times
	topLineBox_.setShow(TRUE); 							// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: guiEventHappened
//
//@ Interface-Description
// Called when a change in the GUI App event happens.  Passed the id of the
// event which changed.  
//---------------------------------------------------------------------
//@ Implementation-Description
// If the eventId is GUI_ONLINE_READY, or SETTINGS_TRANSACTION_SUCCESS
// then we do the following process.
//     If we are in patient setup mode, then we activate the Ventilator
//     Start subscreen and blank the lower screen select tab buttons.
//     If we are not in patient setup mode, then we deactivate any subscreen
//        If the settings are locked or setting transaction failed then
//        we blank the lower screen select tab buttons, and display the
//        appropriated prompts.
//        Else, we show the lower screen select tab buttons and display
//        the default prompts.
// If the eventId is INOP, COMMUNICATIONS_DOWN or SETTINGS_LOCKOUT then we
// do the following process.
//     Blank the lower screen select tab buttons.
//     If we are in patient setup mode, then we activate the Ventilator Start
//     subscreen.
//     Else we deactivate any subscreen.
//     Finally, display the appropriated prompts.
// if the eventId is SERVICE_READY, then we assert.
//
// $[01011] Flatten Main Settings, hide Lower Screen Select Area and
//			display Vent Start subscreen.
// $[01230] When the ventilator enters its settings lockout mode, the gui ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
LowerScreenSelectArea::guiEventHappened(GuiApp::GuiAppEvent eventId)
{
	CALL_TRACE("LowerScreenSelectArea::guiEventHappened(GuiApp::GuiAppEvent eventId)");

	LowerSubScreenArea *pLowerSubScreenArea;

	pLowerSubScreenArea = LowerScreen::RLowerScreen.getLowerSubScreenArea();
				
	// Check for settings transaction event and vent startup being complete
	switch (eventId)
	{
	case GuiApp::GUI_ONLINE_READY:						// $[TI1]
	case GuiApp::SETTINGS_TRANSACTION_SUCCESS:
		if (LowerScreen::RLowerScreen.isInPatientSetupMode())
		{												// $[TI1.1]
			// Activate the Ventilator Start subscreen to display the 
			// appropriated messages.
			pLowerSubScreenArea->activateSubScreen(
				LowerSubScreenArea::GetVentStartSubScreen(), NULL);

			// Make sure Lower Screen Select Area is blank
			setBlank(TRUE);
		}
		else											// $[TI1.2]
		{	// Already have a patient settings.
			// Blank the lower subscreen area also since no subscreens are
			// now allowed to be accessed.
			pLowerSubScreenArea->deactivateSubScreen();

			// GUI is fine, but BD is not, say BD state is in STATE_INOP.
			if (GuiApp::IsSettingsLockedOut() || !GuiApp::IsSettingsTransactionSuccess())
			{											// $[TI1.2.1]
				// Make sure Lower Screen Select Area is blank
				setBlank(TRUE);

				// Set Primary prompt to nothing
				GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY,
							PromptArea::PA_LOW, NULL_STRING_ID);
				// Set Advisory prompt to "Settings have been locked out."
				GuiApp::PPromptArea->setPrompt(PromptArea::PA_ADVISORY,
							PromptArea::PA_LOW, PromptStrs::SETTINGS_LOCKED_OUT_A);

			}
			else										// $[TI1.2.2]
			{	// Both GUI and BD are function normally.
				// Make sure Lower Screen Select Area is visible
				setBlank(FALSE);

				// Set prompts to nothing
				GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY,
							PromptArea::PA_LOW, NULL_STRING_ID);
				GuiApp::PPromptArea->setPrompt(PromptArea::PA_ADVISORY,
							PromptArea::PA_LOW, NULL_STRING_ID);
				GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
							PromptArea::PA_LOW, NULL_STRING_ID);
			}
		}
		break;
		
	case GuiApp::INOP:								// $[TI2]
	case GuiApp::COMMUNICATIONS_DOWN:
	case GuiApp::SETTINGS_LOCKOUT:
		// Set screen select area to blank.
		setBlank(TRUE);

		if (LowerScreen::RLowerScreen.isInPatientSetupMode())
		{											// $[TI2.1]
			// Activate the Ventilator Start subscreen to display the 
			// appropriated messages.
			pLowerSubScreenArea->activateSubScreen(
					LowerSubScreenArea::GetVentStartSubScreen(), NULL);

		}
		else										// $[TI2.2]
		{	// Already have a patient settings.
			// Blank the lower subscreen area also since no subscreens are
			// now allowed to be accessed.
			pLowerSubScreenArea->deactivateSubScreen();
		}

		// Set Primary prompt to nothing
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY,
						PromptArea::PA_LOW, NULL_STRING_ID);
		// Set Advisory prompt to "Settings have been locked out."
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_ADVISORY,
						PromptArea::PA_LOW, PromptStrs::SETTINGS_LOCKED_OUT_A);
		break;
		
	case GuiApp::SERVICE_READY:						// $[TI4]
		AUX_CLASS_ASSERTION_FAILURE(eventId);
		break;
		
	default:										// $[TI5]
		break;
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setVentSetupTabTitleText
//
//@ Interface-Description
// Sets the title text in the VENT SETUP tab button to the specified
// string.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void 
LowerScreenSelectArea::setVentSetupTabTitleText(StringId title)
{
	// $[TI1]
	ventSetupTabButton_.setTitleText(title);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setApneaSetupTabTitleText
//
//@ Interface-Description
// Sets the title text in the APNEA SETUP tab button to the specified
// string.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void 
LowerScreenSelectArea::setApneaSetupTabTitleText(StringId title)
{
	// $[TI1]
	apneaSetupTabButton_.setTitleText(title);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setAlarmSetupTabTitleText
//
//@ Interface-Description
// Sets the title text in the ALARM SETUP tab button to the specified
// string.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void 
LowerScreenSelectArea::setAlarmSetupTabTitleText(StringId title)
{
	// $[TI1]
	alarmSetupTabButton_.setTitleText(title);
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: bdEventHappened
//
//@ Interface-Description
//  Called when Breath-Delivery notifies us of a PROX_INSTALLED event.
//---------------------------------------------------------------------
//@ Implementation-Description
//  When PROX_INSTALLED event is recieved redraw the button labels
//  to include a "PF" symbol.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void LowerScreenSelectArea::bdEventHappened(EventData::EventId eventId,
										EventData::EventStatus eventStatus,
										EventData::EventPrompt eventPrompt)
{
	CALL_TRACE("ProxSetupSubScreen::bdEventHappened(EventData::EventId "
			   ", EventData::EventStatus eventStatus)");

	if (EventData::PROX_INSTALLED == eventId)
	{
		if (eventStatus == EventData::ACTIVE)
		{
			redrawButtonLabels_(TRUE);
		}
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: redrawButtonLabels_
//
//@ Interface-Description
//  Redraws the LowerOtherScreensSubScreen button's symbols such as
//  "LC" and "RM" based on the option bits or based on the PROX INSTALLED
//   event.
//---------------------------------------------------------------------
//@ Implementation-Description
//  
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void LowerScreenSelectArea::redrawButtonLabels_(Boolean enableProx)
{


    // Display "LC" and "RM" when both RESPM_MECH and LEAK_COMP are
	// enabled.  If only LEAK_COMP is enabled, then "LC" shall be 
	// displayed. If only RESP_MECH is enabled, then "RM" shall be 
	// displayed.  Display only the other screen bitmap
	// when both options are disabled.  Also display "PF" when prox 
	// board is installed and the vent is set to a neonatal circuit 
	// type.
    const DiscreteValue  CIRCUIT_TYPE = 
            SettingContextHandle::GetSettingValue(
                                    ContextId::ACCEPTED_CONTEXT_ID,
                                    SettingId::PATIENT_CCT_TYPE);

	Boolean displayProx = (CIRCUIT_TYPE == PatientCctTypeValue::NEONATAL_CIRCUIT)  
	                        && enableProx;

	if (SoftwareOptions::IsOptionEnabled(SoftwareOptions::RESP_MECH) &&
		SoftwareOptions::IsOptionEnabled(SoftwareOptions::LEAK_COMP) )
    {
        lowerOtherScreensBitmap_.setX(OTHER_SCREENS_BITMAP_X_ - 7);

		if (displayProx)
		{
			lowerOtherScreensTabButton_.setTitleText(MiscStrs::LEAK_COMP_AND_RM_AND_PROX_SMALL_BUTTON_TITLE,
															GRAVITY_LEFT);

		}
		else
		{

		   lowerOtherScreensTabButton_.setTitleText(MiscStrs::LEAK_COMP_AND_RM_SMALL_BUTTON_TITLE,
											               GRAVITY_LEFT);
		}
    }
    else if (SoftwareOptions::IsOptionEnabled(SoftwareOptions::LEAK_COMP))
	{
    
	    lowerOtherScreensBitmap_.setX(OTHER_SCREENS_BITMAP_X_ - 7);

		if (displayProx)
		{
			lowerOtherScreensTabButton_.setTitleText(MiscStrs::LEAK_COMP_AND_PROX_SMALL_BUTTON_TITLE,
												  GRAVITY_LEFT);

		}
		else
		{

			lowerOtherScreensTabButton_.setTitleText(MiscStrs::LEAK_COMP_SMALL_BUTTON_TITLE,
												  GRAVITY_LEFT);
		}


    }
    else if (SoftwareOptions::IsOptionEnabled(SoftwareOptions::RESP_MECH) )
    {
        lowerOtherScreensBitmap_.setX(OTHER_SCREENS_BITMAP_X_ - 7);

		if (displayProx)
		{
			lowerOtherScreensTabButton_.setTitleText(MiscStrs::RM_AND_PROX_SMALL_BUTTON_TITLE,
												  GRAVITY_RIGHT);

		}
		else
		{

			lowerOtherScreensTabButton_.setTitleText(MiscStrs::RM_SMALL_BUTTON_TITLE,
															   GRAVITY_RIGHT);
		}

    }
    else
    {

		if (displayProx)
		{
			lowerOtherScreensBitmap_.setX(OTHER_SCREENS_BITMAP_X_ - 7);
			lowerOtherScreensTabButton_.setTitleText(MiscStrs::PROX_SMALL_BUTTON_TITLE,
															   GRAVITY_RIGHT);
			
		}
		else
		{
			lowerOtherScreensBitmap_.setX(OTHER_SCREENS_BITMAP_X_);
		}

    }


    lowerOtherScreensBitmap_.setY(OTHER_SCREENS_BITMAP_Y_);


}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
LowerScreenSelectArea::SoftFault(const SoftFaultID  softFaultID,
		   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, LOWERSCREENSELECTAREA,
									lineNumber, pFileName, pPredicate);
}
