#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: TrendingScrollbar -  Scrollbar for the trending log
//---------------------------------------------------------------------
//@ Interface-Description 
//  TrendingScrollbar is a Scrollbar specialized for use with the
//  trending log and the trend cursor position setting. The Scrollbar
//  base class handles most functions of a scrollbar including the
//  scrollbar's size and appearance. The TrendingScrollbar overrides the
//  Scrollbar::scroll method to directly change the trend cursor
//  position setting when the user uses the off-screen control knob to
//  scroll through the trending log.
//---------------------------------------------------------------------
//@ Rationale 
//	Encapsulates the unique behavior of the trending log's scrollbar.
//---------------------------------------------------------------------
//@ Implementation-Description 
//  Overrides the Scrollbar::scroll method to directly affect the trend
//  cursor position setting. With every lick of the knob, this method
//  changes the trend cursor position setting that in turn affects all
//  objects registered for changes to this setting.
//---------------------------------------------------------------------
//@ Fault-Handling
//  none
//---------------------------------------------------------------------
//@ Restrictions 
//	none 
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/TrendingScrollbar.ccv   25.0.4.0   19 Nov 2013 14:08:40   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001   By: gdc    Date:  09-Mar-2007    SCR Number: 6237
//  Project: Trend
//  Description:
//	Initial version.
//
//=====================================================================

#include "TrendingScrollbar.hh"

//@ Usage-Classes
#include "GuiApp.hh"
#include "ScrollbarTarget.hh"
#include "BoundedSetting.hh"
#include "BoundedValue.hh"
#include "SettingId.hh"
#include "SettingsMgr.hh"
//@ End-Usage

//@ Code...

// Initialize static constants.

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TrendingScrollbar()  [Constructor]
//
//@ Interface-Description
//  Constructs a TrendingScrollbar object. Passed the following
//  parameters:
// 
// >Von
//	height			Height of scrollbar in pixels (uses default width).
// 
//	numDisplayRows	The number of displayed rows in the scrolling area.
// 
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//	Initialize private data memebers. Pass along the scrollbar size
//	parameters to the Scrollbar base class. ScrollbarTarget pointer is
//	NULL since this class handles the scroll operation instead of the
//	base class.
//---------------------------------------------------------------------
//@ PreCondition
//	pTarget must be non-null.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

TrendingScrollbar::TrendingScrollbar(Uint16 height, Uint16 numDisplayRows)
:   Scrollbar(          height,numDisplayRows,NULL)
{
	CALL_TRACE("TrendingScrollbar::TrendingScrollbar(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~TrendingScrollbar()  [Destructor]
//
//@ Interface-Description
// TrendingScrollbar destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

TrendingScrollbar::~TrendingScrollbar(void)
{
	CALL_TRACE("TrendingScrollbar::~TrendingScrollbar(void)");

	// Do nothing
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: scroll			[public]
//
//@ Interface-Description
//  Subtracts the specified number of rows to scroll from the trend
//  cursor position. The rightmost (newest) position in the trend
//  timeline is cursor position 0 and decreases to a minimum value of
//  -359 at the leftmost (oldest) position in the table.
//---------------------------------------------------------------------
//@ Implementation-Description 
//  Retrieves the current trend cursor location from the accepted
//  setting value. Subtracts the specified number of rows to scroll.
//  Uses the settings warping and bounding functions to bound the new
//  data value. Then sets the new cusor position as its accepted value.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void TrendingScrollbar::scroll(Int32 numRowsToScroll)
{
	CALL_TRACE("scroll(Int32 numRowsToScroll)");

	// change the cursor position setting to initiate an update of all concerned objects
	BoundedSetting * pSetting = SettingsMgr::GetBoundedSettingPtr(SettingId::TREND_CURSOR_POSITION);
	BoundedValue trendCursorPosition = pSetting->getAcceptedValue();
	trendCursorPosition.value -= numRowsToScroll;

	// Warp to nearest value down from the computed new cursor position and set it.
	pSetting->warpToRange(trendCursorPosition, BoundedRange::WARP_DOWN);
	pSetting->setAcceptedValue(trendCursorPosition);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void TrendingScrollbar::SoftFault(const SoftFaultID  softFaultID,
								  const Uint32       lineNumber,
								  const char*        pFileName,
								  const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, TRENDINGSCROLLBAR,
							lineNumber, pFileName, pPredicate);
}
