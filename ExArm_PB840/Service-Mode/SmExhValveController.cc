#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: SmExhValveController - Exhalation Valve controller for
//  Service Mode
//---------------------------------------------------------------------
//@ Interface-Description
//	This class provides automatic exhalation valve control for the
//	Service Mode tests.  The control algorithm executes in a separate
//	task from the task which performs the Service Mode tests,
//	allowing each test to simply enable and disable control without
//	having to periodically update the exhalation valve command
//	within the test code.
//---------------------------------------------------------------------
//@ Rationale
//	This class encapsulates all methods required for automatic exhalation
//	valve control for the Service Mode tests.
//---------------------------------------------------------------------
//@ Implementation-Description
//	The method close() enables automatic EV control, and open()
//	disables control.  EV control is performed by the method newCycle(),
//	which, when control is active, periodically samples the expiratory
//	pressure and updates the EV command to prevent gas from escaping
//	the EV.
//	NOTE: as a Service Mode utility class, there is no direct SRS
//	traceability.
//---------------------------------------------------------------------
//@ Fault-Handling
//	n/a
//---------------------------------------------------------------------
//@ Restrictions
//	Only one instance of this class may be created.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/SmExhValveController.ccv   25.0.4.0   19 Nov 2013 14:23:44   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 002  By:  quf    Date: 22-May-1997    DR Number: DCS 2131
//       Project:  Sigma (840)
//       Description:
//            Modification-Log fix.
//            Fixed global reference names (i.e. "r" changed to "R").
//
//  Revision: 001  By:  mad    Date: 21-Aug-1995    DR Number: none
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//=====================================================================

#include "SmExhValveController.hh"

//@ Usage-Classes

#include "SmConstants.hh"

#include "Task.hh"
#include "NovRamManager.hh"

#include "MainSensorRefs.hh"
#include "ValveRefs.hh"
#include "RegisterRefs.hh"
#include "PressureSensor.hh"
#include "ExhalationValve.hh"
#include "ExhValveTable.hh"		// defines PRESSURE_RANGE
#include "DampingGainTable.hh"

#ifdef SIGMA_UNIT_TEST
# include "FakeIo.hh"
# include "SmExhValveController_UT.hh"
#endif  // SIGMA_UNIT_TEST

//@ End-Usage

//@ Code...

//@ Constant: HALF_COUNT
// half a DAC count
static const Real32 HALF_COUNT = 0.5;  // half a DAC count

//@ Constant: MAX_EV_COUNT_OFFSET
// max EV current offset in DAC counts
const Uint16 MAX_EV_COUNT_OFFSET = 320;  // DAC counts

//@ Constant: EV_CTRL_OFFSET_P
// offset to expiratory pressure, to keep EV closed
const Real32 EV_CTRL_OFFSET_P = 60.0;  // cmH2O

// from ExhValveCalibration.cc
extern const Real32 MAX_EV_SLOPE;  // mA/cmH2O


//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SmExhValveController() [Default Constructor]
//
//@ Interface-Description
//	Constructor.
//	Initializes private data members (i.e. state variables for
//	the controller).  No arguments.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Set controller inactive and not requested, max EV DAC count disabled.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SmExhValveController::SmExhValveController(void)
  : requestForExhValveControl_(FALSE),
    exhValveControllerActive_(FALSE),
    maxEvDacCountEnabled_(FALSE)
{
// $[TI1]
	CALL_TRACE("SmExhValveController()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~SmExhValveController()  [Destructor]
//
//@ Interface-Description
//	Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SmExhValveController::~SmExhValveController(void)
{
	CALL_TRACE("~SmExhValveController()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newCycle()
//
//@ Interface-Description
//	This method takes no arguments and return nothing.  It is used
//	to control the exhalation valve, i.e. to automatically provide
//	enough EV current to prevent gas from escaping.
//---------------------------------------------------------------------
//@ Implementation-Description
//	When EV control is requested, the EV is closed by reading the
//	the expiratory pressure Pexp, adding an offset to it (Pev = Pexp + offset),
//	limiting Pev to a minimum and maximum value, then using Pev to
//	command the EV.  The EV command is determined one of three ways:
//	1) using the EV calibration table, 2) using an equation, and
//	3) using the maximum EV DAC count.  Which method is used
//	depends on three factors: a) if the EV table is valid,
//	b) the value of Pev , and c) if "high EV DAC count" is enabled.
//	If the EV calib table is valid, it is used (except when "max EV DAC count"
//	is enabled and Pev = max value).  This is the "normal" case.
//	If the EV calib table is not valid, an equation is is used (except when
//	"high EV DAC count" is enabled and Pev = max value).
//	If "high EV DAC count" is enabled and Pev = max value, the EV is
//	is commanded to the maximum DAC count.  This case is used during
//	the SST compliance calibration to keep the EV shut as tightly
//	as possible once Pev exceeds the range of the table.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SmExhValveController::newCycle(void)
{
	CALL_TRACE("SmExhValveController::newCycle(void)");

	if(requestForExhValveControl_)
	{
	// $[TI1]
		// Set controller active flag TRUE
		exhValveControllerActive_ = TRUE;

		// Add a fixed offset to the current exh pressure reading
		// and use this sum to determine the EV current required
		// to close the EV (i.e. prevent gas from exiting the EV)
		Real32 targetPressure = RExhPressureSensor.getValue() +
							EV_CTRL_OFFSET_P;

		// If target pressure exceeds the EV cal table range and max
		// EV DAC count is enabled, set flag TRUE to force EV DAC
		// count to max.
		Boolean forceMaxEvDacCount;
		if ( targetPressure > PRESSURE_RANGE &&
			 maxEvDacCountEnabled_ )
		{
		// $[TI1.1]
			forceMaxEvDacCount = TRUE;
		}
		else
		{
		// $[TI1.2]
			forceMaxEvDacCount = FALSE;
		}

		// Bound the target pressure
		targetPressure = MAX_VALUE(targetPressure, 0.0F);
		targetPressure = MIN_VALUE(targetPressure, PRESSURE_RANGE);

		// Use the EV cal table if it's valid and if we're
		// not forcing max DAC count

		if ( !ServiceMode::IsExhValveCalRequired() &&
			 !forceMaxEvDacCount )
		{
		// $[TI1.3]
			RExhalationValve.updateValve(
						targetPressure, ExhalationValve::NORMAL);
		}
		else
		{
		// $[TI1.4]
			// Set the EV damping gain based on the target pressure
			// rounded to the nearest integer
// E600 BDIO			REVDampingGainDacPort.writeRegister(DampingGainTable[(Uint16) (targetPressure + 0.5)] );

			// Command the EV
			if (forceMaxEvDacCount)
			{
			// $[TI1.4.1]
				// Force EV DAC count to max
				RExhalationValve.commandValve(MAX_COUNT_VALUE);
			}
			else
			{
			// $[TI1.4.2]
				// Use equation to command DAC

				// calculate worst-case EV gain in counts/cmH2O
				Real32 maxGain = MAX_EV_SLOPE / EV_COUNTS_TO_MA;

				// calculate counts based on target pressure using
				// worst-case gain and offset, round to nearest integer
		    	Uint16 counts = (Uint16)(maxGain * targetPressure +
		    					MAX_EV_COUNT_OFFSET + HALF_COUNT);

// TODO E600- BDIO				RExhValveDacPort.writeRegister(counts);
			}
			// end else
		}
		// end else
	}
	else
	{
	// $[TI2]
		// Inactivate the EV controller if it is currently active
		if (exhValveControllerActive_)
		{
			// TODO E600 MS this is a hack needs to be fixed later.
			RExhalationValve.commandValve(MIN_DAC_COUNT);

			/*
			RExhalationValve.updateValve(
						MIN_DAC_COUNT, ExhalationValve::NORMAL);
			*/

		// $[TI2.1]
			// Open the exhalation valve
// E600 BDIO			REVDampingGainDacPort.writeRegister(MIN_DAMPING_GAIN);
// E600 BDIO			RExhValveDacPort.writeRegister(MIN_DAC_COUNT);

			// Set controller inactive
			exhValveControllerActive_ = FALSE;
		}
		// $[TI2.2]
		// implied else
	}
	// end else
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: open()
//
//@ Interface-Description
//	This method takes no arguments and returns nothing.  It is used
//	to stop active control and open the exhalation valve.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Set the EV controller request flag FALSE, delay to allow EV to open
//	via newCycle().
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SmExhValveController::open(void)
{
// $[TI1]
	CALL_TRACE("SmExhValveController::open(void)");

	// Set exhalation valve controller request flag to FALSE
	requestForExhValveControl_ = FALSE;

	// Allow time to open the valve
	Task::Delay (0, 200);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: close()
//
//@ Interface-Description
//	This method is used to activate the exhalation valve controller, i.e.
//	"close" the EV.  Two arguments: Boolean flag to enable EV high DAC count
//	(default = FALSE), and Boolean flag to enable EV closing delay
//	(default = TRUE).  No return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Set the max EV DAC count enabled flag equal to the
//	1st argument.  Set the EV control request flag TRUE.  If the
//	2nd argument = TRUE, allow time for the EV to close via newCycle().
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SmExhValveController::close( Boolean enableMaxEvDacCount,
							 Boolean enableDelay)
{
	CALL_TRACE("SmExhValveController::close( Boolean enableMaxEvDacCount \
												Boolean enableDelay)");

	// Set maxEvDacCountEnabled_ from 1st argument,
	// default = FALSE
	maxEvDacCountEnabled_ = enableMaxEvDacCount;

	// Set exhalation valve controller request flag to TRUE.
	// NOTE: do this AFTER setting all other state variables to avoid
	// a race with newCycle()
	requestForExhValveControl_ = TRUE;

	if (enableDelay)
	{
	// $[TI1]
		// Allow time for valve to close (default)
		Task::Delay (0, 200);

	#ifdef SIGMA_UNIT_TEST
		SmExhValveController_UT::EnableDelay = enableDelay;
	#endif  // SIGMA_UNIT_TEST
	}
	// $[TI2]
	// implied else
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
SmExhValveController::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, SERVICE_MODE, SMEXHVALVECONTROLLER,
                          lineNumber, pFileName, pPredicate);
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//=====================================================================
//
//  Private Methods...
//
//=====================================================================
