
#ifndef TriggersRefs_HH
#define TriggersRefs_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// File: TriggersRefs - extern definitions of breath triggers
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/TriggersRefs.hhv   25.0.4.0   19 Nov 2013 14:00:14   pvcs  $
//
//@ Modification-Log
//
//  Revision: 012   By: rhj   Date:  07-Nov-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//		Leak Compensation project-related changes:
//      Added RNetFlowBackupInspTrigger.
// 
//  Revision: 011   By: gdc   Date:  22-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Added RM maneuver triggers.
//
//  Revision: 008  By:  syw    Date:  14-Jan-1999    DR Number: 5367
//       Project:  ATC
//       Description:
//          Added RPausePressTrigger and HighPressCompExpTrigger, 
//          LungVolumeExpTrigger, and LungFlowExpTrigger classes
//		 	ATC initial version.
//
//  Revision: 007  By:  iv    Date:  31-Jan-1998    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Bilevel initial version.
//             Added RPeepReductionExpTrigger and RPausePressExpTrigger.
//
//  Revision: 006  By: iv    Date:  18-Jul-1997   DR Number: DCS 2298
//  	Project:  Sigma (R8027)
//		Description:
//          Added RPeepRecoveryMandInspTrigger.
//
//  Revision: 005  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 004  By:  iv    Date:  07-Feb-1997    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Rework per Breath Delivery formal code review.
//
//  Revision: 003  By:  sp    Date:  26-Sep-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Changed rPeepRecoveryTrigger to rPeepRecoveryInspTrigger.
//
//  Revision: 002  By:  iv    Date:  07-Sep-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Added rPeepRecoveryTrigger.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//====================================================================

// BreathTrigger references
class AsapInspTrigger;
class TimerBreathTrigger; 
class DeliveredFlowExpTrigger; 
class HighCircuitPressureExpTrigger; 
class HighCircuitPressureInspTrigger; 
class HighVentPressureExpTrigger;
class ImmediateBreathTrigger;
class NetFlowInspTrigger; 
class P100Trigger; 
class OperatorInspTrigger;
class PressureExpTrigger; 
class PausePressTrigger; 
class PressureInspTrigger; 
class PressureSetInspTrigger; 
class PressureSvcTrigger;
class OscTimeInspTrigger;
class SimvTimeInspTrigger;
class PeepRecoveryInspTrigger;
class BiLevelLowToHighInspTrigger;
class BiLevelHighToLowExpTrigger;
class HighPressCompExpTrigger ;
class LungVolumeExpTrigger;
class LungFlowExpTrigger;

extern AsapInspTrigger& RAsapInspTrigger;
extern TimerBreathTrigger& RBackupTimeExpTrigger;
extern SimvTimeInspTrigger& RSimvTimeInspTrigger;
extern BiLevelHighToLowExpTrigger& RBiLevelHighToLowExpTrigger;
extern BiLevelLowToHighInspTrigger& RBiLevelLowToHighInspTrigger;
extern TimerBreathTrigger& RTimeInspTrigger;
extern TimerBreathTrigger& RPauseTimeoutTrigger;
extern DeliveredFlowExpTrigger& RDeliveredFlowExpTrigger;
extern HighCircuitPressureExpTrigger& RHighCircuitPressureExpTrigger;
extern HighCircuitPressureInspTrigger& RHighCircuitPressureInspTrigger;
extern HighVentPressureExpTrigger& RHighVentPressureExpTrigger;
extern ImmediateBreathTrigger& RImmediateBreathTrigger;
extern ImmediateBreathTrigger& RImmediateExpTrigger;
extern ImmediateBreathTrigger& RPauseCompletionTrigger;
extern ImmediateBreathTrigger& RP100CompletionTrigger;
extern ImmediateBreathTrigger& RSvcCompleteTrigger;
extern ImmediateBreathTrigger& RPeepReductionExpTrigger;
extern ImmediateBreathTrigger& RArmManeuverTrigger;
extern ImmediateBreathTrigger& RDisarmManeuverTrigger;
extern NetFlowInspTrigger& RNetFlowInspTrigger;
extern NetFlowInspTrigger& RNetFlowBackupInspTrigger;
extern P100Trigger& RP100Trigger;
extern OperatorInspTrigger& ROperatorInspTrigger;
extern PressureExpTrigger& RPressureExpTrigger;
extern PausePressTrigger& RPausePressTrigger;
extern PressureSetInspTrigger& RPressureInspTrigger;
extern PressureInspTrigger& RPressureBackupInspTrigger;
extern OscTimeInspTrigger& ROscTimeInspTrigger;
extern PressureSvcTrigger& RPressureSvcTrigger;
extern TimerBreathTrigger& ROscTimeBackupInspTrigger;
extern TimerBreathTrigger& RSvcTimeTrigger;
extern PeepRecoveryInspTrigger& RPeepRecoveryInspTrigger;
extern PeepRecoveryInspTrigger& RPeepRecoveryMandInspTrigger;
extern HighPressCompExpTrigger& RHighPressCompExpTrigger;
extern LungVolumeExpTrigger& RLungVolumeExpTrigger;
extern LungFlowExpTrigger& RLungFlowExpTrigger;

//Trigger Mediator
class BreathTriggerMediator;
extern BreathTriggerMediator& RBreathTriggerMediator;

#endif // TriggersRefs_HH 
