// All rights reserved ADENEO EMBEDDED 2010
/*
===============================================================================
*             Texas Instruments OMAP(TM) Platform Software
* (c) Copyright Texas Instruments, Incorporated. All Rights Reserved.
*
* Use of this software is controlled by the terms and conditions found
* in the license agreement under which this software has been supplied.
*
===============================================================================
*/
//
//  File:  bsp_padcfg.c
//
#include "bsp.h"
#include "am387x_clocks.h"
#include "oal_padcfg.h"
#include "bsp_padcfg.h"

//------------------------------------------------------------------------------
//  Pad configuration
//------------------------------------------------------------------------------

static PAD_INFO g_allowedPadCfg[] = ALL_ALLOWED_PADS;

const PAD_INFO DummyPads[] = { END_OF_PAD_ARRAY };

PAD_INFO* BSPGetAllPadsInfo()
{    
    return g_allowedPadCfg;
}
const PAD_INFO* BSPGetDevicePadInfo(OMAP_DEVICE device)
{
    switch (device){
    	case AM_DEVICE_SDIO0:      
    	case AM_DEVICE_SDIO1:	   
    	case AM_DEVICE_UART1:     
#ifdef PRCM_DEBUG            
           RETAILMSG(1, (L"BSPGetDevicePadInfo() for device %d is not implemented!!!\r\n", device));
#endif
	    return DummyPads;
	}
    return NULL;
}
