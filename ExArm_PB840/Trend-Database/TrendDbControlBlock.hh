#ifndef TrendDbControlBlock_HH
#define TrendDbControlBlock_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class:  TrendDbControlBlock - Used for persistent storage of the database vars
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Trend-Database/vcssrc/TrendDbControlBlock.hhv   25.0.4.0   19 Nov 2013 14:34:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By:  mjfullm    Date:  3-Apr-2007    SCR Number: 6237
//  Project:  Trend Project
//  Description:
//          Initial version.
//
//
//====================================================================


//@ Usage-Classes
#include "Sigma.hh"				// Utilities
#include "CompactFlash.hh"		// GUI-IO-Devices
//@ End-Usage

#include "TrendDataMgr.hh"


class TrendDbControlBlock
{
public:

	TrendDbControlBlock( void );
	~TrendDbControlBlock(void);

	inline Uint32 getHeadBlockLba( TrendDataMgr::TableId tableId ) const;
	inline void setHeadBlockLba( TrendDataMgr::TableId tableId, Uint32 headLbaBlock ) const;
	inline Uint32 getSizeBlocks( TrendDataMgr::TableId tableId ) const;
	inline void setSizeBlocks( TrendDataMgr::TableId, Uint32 sizeBlocks ) const;
	inline Int16 getBufferPosition( TrendDataMgr::TableId tableId ) const;
	inline void setBufferPosition( TrendDataMgr::TableId, Int16 bufferPos ) const;
	inline Int32 getCurrentFrame( void ) const;
	inline void setCurrentFrame( Int32 frame ) const;

	void initialize(void);
	SigmaStatus flush( void );

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

private:
	TrendDbControlBlock(const TrendDbControlBlock&);	// not implemented...
	void  operator=(const TrendDbControlBlock&);	// not implemented...

	Uint16 computeChecksum_( Uint16 * pData );


	//@ Data-Member: storeInterface_
	// Interface to the compact flash used to store the persistent
	// database information, such as the current frame number
	CompactFlash & storeInterface_;

	//@ Type: Schema
	// Define the schema of the control block
	// The Schema must be less than 512 bytes
	struct Schema
	{
		Int32   version;
		Int32   currentFrame;
		Uint32  tableHeadLbaArray[ TrendDataMgr::TOTAL_TABLE_ID_VALUES ];
		Uint32  tableSizeBlocksArray[ TrendDataMgr::TOTAL_TABLE_ID_VALUES ];
		Int16   tableBufferPosArray[ TrendDataMgr::TOTAL_TABLE_ID_VALUES ];
		Uint16  checksum;
	};

	//@ Data-Member: SourceBlock
	// Enum used to define which control block is currently in use
	enum SourceBlock
	{
		NO_SOURCE_BLOCK,	// Not loaded
		SOURCE_BLOCK_A,		// data loaded from control Block A
		SOURCE_BLOCK_B		// data loaded from control Block B
	};
	SourceBlock sourceBlock_;

	//@ Data-Member: controlBlockData_
	// Ram copy of the control block
	// The copy can be loaded from either control block
	CompactFlash::Block controlBlockData_;
};


// Inlined methods
#include "TrendDbControlBlock.in"


#endif // TrendDbControlBlock_HH 
