#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//============== C L A S S   D E S C R I P T I O N ====================
//@ Class:  ConstantParmSetting - Constant Parameter Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, discrete setting that indicates which timing parameter
//  remains constant during a change of respiratory rate (i.e., either
//  inspiratory time, expiratory time or I:E ratio).  This class inherits
//  from 'BatchDiscreteSetting' and provides the specific information needed
//  for the representation of constant-during-rate-change.  This information
//  includes the set of values that this setting can have (see
//  'ConstantParmValue.hh'), this setting's role in Transition Rules
//  (see 'acceptTransition()'), and this batch setting's new-patient value.
//
//  This setting is changed differently than the other settings in that
//  knob deltas are NOT passed in via 'calcNewValue()', new values are
//  passed in.  This setting overrides 'calcNewValue()' to treat the
//  incoming delta parameter as the new value of this setting.  This is
//  needed because changing this setting via knob deltas is deemed
//  inadequate for the needs of GUI-Applications.
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class has no data members, and no dependent settings.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/ConstantParmSetting.ccv   25.0.4.0   19 Nov 2013 14:27:18   pvcs  $
//
//@ Modification-Log
//
//  Revision: 009   By: gdc    Date: 16-Aug-2010     SCR Number: 6663
//  Project: API/MAT
//  Description:
//  Added support for VentSet API.
// 
//  Revision: 008   By: gdc    Date: 24-Sep-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal non-invasive CPAP.
//
//  Revision: 007  By: sah     Date:  05-May-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//	VTPC project-related changes:
//      *  added determination of applicability based on 'VC+' value
//      *  added 'acceptTransition()' to support the "to-VC+" rule
//      *  added support for 'VC+' value to 'GetValue()'
//
//  Revision: 006   By: sah    Date: 29-Jun-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	Corrected incomplete comment.
//
//  Revision: 005   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'getApplicability()' method
//
//  Revision: 004   By: hhd  Date:  01-Oct-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Removed dead code in static method GetValue() which fell out during
//	unit testing.
//
//  Revision: 003   By: dosman    Date:  07-Apr-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Fixed incorrect default value caused by getNewPatientValue()
//	returning SettingValue: casted it to DiscreteValue
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Initial version.
//
//=====================================================================

#include "ConstantParmSetting.hh"
#include "ConstantParmValue.hh"
#include "MandTypeValue.hh"
#include "ModeValue.hh"

//@ Usage-Classes
#include "SettingsMgr.hh"
//@ End-Usage

//@ Code...
static const DiscreteParameterItem ParameterLookup_[] = 
{
	{"TI", ConstantParmValue::INSP_TIME_CONSTANT},
	{"TE", ConstantParmValue::EXP_TIME_CONSTANT},
	{"IE", ConstantParmValue::IE_RATIO_CONSTANT},
	{NULL , NULL}
};

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  ConstantParmSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02100] -- setting range
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ConstantParmSetting::ConstantParmSetting(void)
: BatchDiscreteSetting(SettingId::CONSTANT_PARM, 
					   Setting::NULL_DEPENDENT_ARRAY_,
					   ConstantParmValue::TOTAL_CONSTANT_PARMS)
{
	CALL_TRACE("ConstantParmSetting()");
	setLookupTable(ParameterLookup_);
}	// $[TI1]


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method:  ~ConstantParmSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Default destructor.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ConstantParmSetting::~ConstantParmSetting(void)
{
	CALL_TRACE("~ConstantParmSetting(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[01071] -- main settings applicability
//  $[01081] -- breath settings applicability
//---------------------------------------------------------------------
//@ PreCondition
//  (qualifierId == Notification::ACCEPTED  ||
//   qualifierId == Notification::ADJUSTED)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
	ConstantParmSetting::getApplicability(
										 const Notification::ChangeQualifier qualifierId
										 ) const
{
	CALL_TRACE("getApplicability(qualifierId)");

	const Setting*  pMode     = SettingsMgr::GetSettingPtr(SettingId::MODE);
	const Setting*  pMandType = SettingsMgr::GetSettingPtr(SettingId::MAND_TYPE);

	DiscreteValue  modeValue;
	DiscreteValue  mandTypeValue;

	switch ( qualifierId )
	{
		case Notification::ACCEPTED :	  // $[TI1]
			modeValue     = pMode->getAcceptedValue();
			mandTypeValue = pMandType->getAcceptedValue();
			break;
		case Notification::ADJUSTED :	  // $[TI2]
			modeValue     = pMode->getAdjustedValue();
			mandTypeValue = pMandType->getAdjustedValue();
			break;
		default :
			AUX_CLASS_ASSERTION_FAILURE(qualifierId);
			break;
	}

	Applicability::Id  applicability;

	switch ( mandTypeValue )
	{
		case MandTypeValue::PCV_MAND_TYPE :		  // $[TI3]
			switch ( modeValue )
			{
				case ModeValue::AC_MODE_VALUE :
				case ModeValue::SIMV_MODE_VALUE :
				case ModeValue::BILEVEL_MODE_VALUE :	// $[TI3.1]
					applicability = Applicability::CHANGEABLE;
					break;
				case ModeValue::SPONT_MODE_VALUE :		// $[TI3.2]
				case ModeValue::CPAP_MODE_VALUE :
					applicability = Applicability::INAPPLICABLE;
					break;
				default :
					// unexpected mode...
					AUX_CLASS_ASSERTION_FAILURE(modeValue);
					break;
			}
			break;

		case MandTypeValue::VCV_MAND_TYPE :		  // $[TI4]
			applicability = Applicability::INAPPLICABLE;
			break;

		case MandTypeValue::VCP_MAND_TYPE :		  // $[TI5]
			applicability = Applicability::CHANGEABLE;
			break;

		default :
			// unexpected mandatory type...
			AUX_CLASS_ASSERTION_FAILURE(mandTypeValue);
			break;
	}

	return(applicability);
}


//============== M E T H O D   D E S C R I P T I O N ================
//@ Method:  calcNewValue(knobDelta)  [virtual]
//
//@ Interface-Description
//  The purpose of this method is to override the base class method
//  and add the special behavior for this setting.  For this setting,
//  knob clicks are NOT used, the user interface passes instead of
//  knob clicks or knobDelta, the actual new value of the setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method performs the following:
//        (1) Make sure the new value is valid.
//        (2) Call a method which resets the setting's bound status and
//            sets the setting's adjusted value to the passed in value.
//---------------------------------------------------------------------
//@ PreCondition
//  (getApplicability(Notification::ADJUSTED) == Applicability::CHANGEABLE)
//  (knobDelta == ConstantParmValue::INSP_TIME_CONSTANT  ||
//   knobDelta == ConstantParmValue::EXP_TIME_CONSTANT   ||
//   knobDelta == ConstantParmValue::IE_RATIO_CONSTANT)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

const BoundStatus&
	ConstantParmSetting::calcNewValue(const Int16 knobDelta)
{
	CALL_TRACE("calcNewValue(knobDelta)");

	CLASS_PRE_CONDITION(
					   (getApplicability(Notification::ADJUSTED) == Applicability::CHANGEABLE)
					   );
	CLASS_PRE_CONDITION((knobDelta == ConstantParmValue::INSP_TIME_CONSTANT  ||
						 knobDelta == ConstantParmValue::EXP_TIME_CONSTANT   ||
						 knobDelta == ConstantParmValue::IE_RATIO_CONSTANT));

	// store the knob delta as the new value...
	setAdjustedValue(knobDelta);

	return(getBoundStatus());	// $[TI1]
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [const, virtual]
//
//@ Interface-Description
//  Return the setting's new-patient value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns a constant.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

SettingValue
	ConstantParmSetting::getNewPatientValue(void) const
{
	CALL_TRACE("getNewPatientValue()");

	// $[02101] The setting's new-patient value ...
	return(DiscreteValue(ConstantParmValue::INSP_TIME_CONSTANT)); // $[TI1]
}


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method: acceptTransition(settingId, toValue, fromValue) [virtual]
//
//@ Interface-Description
//  This method is responsible for updating this setting based on the
//  the Main Control Setting identified by 'settingId' and the transition
//  of its value from 'fromValue' to 'toValue'.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[VC02006]\l\ -- to-VC+ transition rules
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	ConstantParmSetting::acceptTransition(const SettingId::SettingIdType,
										  const DiscreteValue,
										  const DiscreteValue)
{
	CALL_TRACE("acceptTransition(settingId, toValue, fromValue)");

	//---------------------------------------------------------------------
	// mandatory type transition to VC+...
	//---------------------------------------------------------------------

	setAdjustedValue(ConstantParmValue::INSP_TIME_CONSTANT);
}	// $[TI1]


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  GetValue(basedOnCategory)  [static]
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

DiscreteValue
	ConstantParmSetting::GetValue(const BasedOnCategory basedOnCategory)
{
	CALL_TRACE("GetValue(basedOnCategory)");

	const Setting*  pMode = SettingsMgr::GetSettingPtr(SettingId::MODE);
	const Setting*  pMandType = SettingsMgr::GetSettingPtr(SettingId::MAND_TYPE);

	DiscreteValue  constantParmValue;

	switch ( basedOnCategory )
	{
		case BASED_ON_NEW_PATIENT_VALUES :	  // $[TI1]
			constantParmValue = ConstantParmValue::INSP_TIME_CONSTANT;
			break;

		case BASED_ON_ADJUSTED_VALUES :		  // $[TI2]
			{
				const DiscreteValue  MODE_VALUE =
					DiscreteValue(pMode->getAdjustedValue());
				const DiscreteValue  MAND_TYPE_VALUE =
					DiscreteValue(pMandType->getAdjustedValue());

				switch ( MODE_VALUE )
				{
					case ModeValue::AC_MODE_VALUE :
					case ModeValue::SIMV_MODE_VALUE :	  // $[TI2.1]
						switch ( MAND_TYPE_VALUE )
						{
							case MandTypeValue::VCP_MAND_TYPE :
							case MandTypeValue::PCV_MAND_TYPE :	// $[TI2.1.1]
								{
									const Setting*  pConstantParm =
										SettingsMgr::GetSettingPtr(SettingId::CONSTANT_PARM);

									// use current adjusted value...
									constantParmValue = pConstantParm->getAdjustedValue();
								}
								break;
							case MandTypeValue::VCV_MAND_TYPE :	// $[TI2.1.2]
								constantParmValue = ConstantParmValue::INSP_TIME_CONSTANT;
								break;
							default :
								// unexpected mand type...
								AUX_CLASS_ASSERTION_FAILURE(MODE_VALUE);
								break;
						};
						break;

					case ModeValue::BILEVEL_MODE_VALUE :  // $[TI2.2]
						{
							const Setting*  pConstantParm =
								SettingsMgr::GetSettingPtr(SettingId::CONSTANT_PARM);

							// use current adjusted value...
							constantParmValue = pConstantParm->getAdjustedValue();
						}
						break;

					case ModeValue::SPONT_MODE_VALUE :	  // $[TI2.3]
					case ModeValue::CPAP_MODE_VALUE : 
						constantParmValue = ConstantParmValue::INSP_TIME_CONSTANT;
						break;

					default :
						// unexpected mode...
						AUX_CLASS_ASSERTION_FAILURE(MODE_VALUE);
						break;
				}
			}
			break;

		case BASED_ON_ACCEPTED_VALUES :
			// fall through to the 'default' case, if not DEVELOPMENT...
#if defined(SIGMA_DEVELOPMENT)
			{
				const DiscreteValue  MODE_VALUE =
					DiscreteValue(pMode->getAcceptedValue());
				const DiscreteValue  MAND_TYPE_VALUE =
					DiscreteValue(pMandType->getAcceptedValue());

				switch ( MODE_VALUE )
				{
					case ModeValue::AC_MODE_VALUE :
					case ModeValue::SIMV_MODE_VALUE :
						switch ( MAND_TYPE_VALUE )
						{
							case MandTypeValue::VCP_MAND_TYPE :
							case MandTypeValue::PCV_MAND_TYPE :
								{
									const Setting*  pConstantParm =
										SettingsMgr::GetSettingPtr(SettingId::CONSTANT_PARM);

									// use current accepted value...
									constantParmValue = pConstantParm->getAcceptedValue();
								}
								break;
							case MandTypeValue::VCV_MAND_TYPE :
								constantParmValue = ConstantParmValue::INSP_TIME_CONSTANT;
								break;
							default :
								// unexpected mand type...
								AUX_CLASS_ASSERTION_FAILURE(MODE_VALUE);
								break;
						};
						break;

					case ModeValue::BILEVEL_MODE_VALUE :
						{
							const Setting*  pConstantParm =
								SettingsMgr::GetSettingPtr(SettingId::CONSTANT_PARM);

							// use current accepted value...
							constantParmValue = pConstantParm->getAcceptedValue();
						}
						break;

					case ModeValue::SPONT_MODE_VALUE :
					case ModeValue::CPAP_MODE_VALUE :
						constantParmValue = ConstantParmValue::INSP_TIME_CONSTANT;
						break;

					default :
						// unexpected mode...
						AUX_CLASS_ASSERTION_FAILURE(MODE_VALUE);
						break;
				}
			}
			break;
#endif // defined(SIGMA_DEVELOPMENT)
		default :
			// unexpected 'basedOnCategory' value...
			AUX_CLASS_ASSERTION_FAILURE(basedOnCategory);
			break;
	};

	return(constantParmValue);
}


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//			[static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
	ConstantParmSetting::SoftFault(const SoftFaultID  softFaultID,
								   const Uint32       lineNumber,
								   const char*        pFileName,
								   const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

	FaultHandler::SoftFault(softFaultID, ::SETTINGS_VALIDATION,
							CONSTANT_PARM_SETTING, lineNumber, pFileName,
							pPredicate);
}
