
//====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: DLinkedNodeC<TYPE> - Node for Doubly-Linked Lists of Generic Data
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Foundation/vcssrc/DLinkedNodeC.I_v   25.0.4.0   19 Nov 2013 14:03:46   pvcs  $
//
//@ Modification-Log
//
//   Revision: 002   By: sah   Date: 07-Jan-1999   DR Number: 5321
//   Project:  ATC
//   Description:
//	Changed 'SoftFault()' method to an non-inlined method.
//  
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//====================================================================

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getItem()  [const]
//
//@ Interface-Description
//	Return a constant reference to this node's item.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//	none
//---------------------------------------------------------------------
//@ PostConditions
//	none
//@ End-Method
//=====================================================================

inline const TYPE&
DLinkedNodeC<TYPE>::getItem(void) const
{
  CALL_TRACE("getItem() const");
  return(nodeItem_);  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getItem()
//
//@ Interface-Description
//	Return a non-constant reference to this node's item.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//	none
//---------------------------------------------------------------------
//@ PostConditions
//	none
//@ End-Method
//=====================================================================

inline TYPE&
DLinkedNodeC<TYPE>::getItem(void)
{
  CALL_TRACE("getItem()");
  return(nodeItem_);  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getNextPtr()  [const]
//
//@ Interface-Description
//	Return a constant pointer to the next node.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//	none
//---------------------------------------------------------------------
//@ PostConditions
//	none
//@ End-Method
//=====================================================================

inline const DLinkedNodeC<TYPE>*
DLinkedNodeC<TYPE>::getNextPtr(void) const
{
  CALL_TRACE("getNextPtr() const");
  return(pNext_);  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getPrevPtr()  [const]
//
//@ Interface-Description
//	Return a constant pointer to the previous list node.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//	none
//---------------------------------------------------------------------
//@ PostConditions
//	none
//@ End-Method
//=====================================================================

inline const DLinkedNodeC<TYPE>*
DLinkedNodeC<TYPE>::getPrevPtr(void) const
{
  CALL_TRACE("getPrevPtr() const");
  return(pPrev_);  // $[TI1]
}


//=====================================================================
//
//  Private Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  DLinkedNodeC<TYPE>(dNode)  [Copy Constructor]
//
//@ Interface-Description
//	Construct this node using 'dNode' to initialize this node's
//	item.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//	none
//---------------------------------------------------------------------
//@ PostConditions
//	(getItem() == dNode.getItem())
//	(getNextPtr() == NULL)
//	(getPrevPtr() == NULL)
//@ End-Method
//=====================================================================

inline
DLinkedNodeC<TYPE>::DLinkedNodeC<TYPE>(const DLinkedNodeC<TYPE>& dNode)
				       : nodeItem_(dNode.nodeItem_),
					 pNext_(NULL), pPrev_(NULL)
{
  CALL_TRACE("DLinkedNodeC<TYPE>(dNode)");
}  // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  DLinkedNodeC<TYPE>(newItem)  [Conversion Constructor]
//
//@ Interface-Description
//	Construct this node using 'newItem' to initialize this node's
//	item.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//	none
//---------------------------------------------------------------------
//@ PostConditions
//	(getItem() == newItem)
//	(getNextPtr() == NULL)
//	(getPrevPtr() == NULL)
//@ End-Method
//=====================================================================

inline
DLinkedNodeC<TYPE>::DLinkedNodeC<TYPE>(const TYPE& newItem)
				       : nodeItem_(newItem),
					 pNext_(NULL), pPrev_(NULL)
{
  CALL_TRACE("DLinkedNodeC<TYPE>(newItem)");
}  // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ~DLinkedNodeC<TYPE>(newItem)  [Destructor]
//
//@ Interface-Description
//	Destroy this list node, and the item it holds.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//	none
//---------------------------------------------------------------------
//@ PostConditions
//	none
//@ End-Method
//=====================================================================

inline
DLinkedNodeC<TYPE>::~DLinkedNodeC<TYPE>(void)
{
  CALL_TRACE("~DLinkedNodeC<TYPE>()");
}  // $[TI1]
