//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file Semaphore.hh
//----------------------------------------------------------------------------


#ifndef SEMAPHORE_HH
#define SEMAPHORE_HH

//-------------------------------------
// I N C L U D E S
//-------------------------------------
#define WIN32_LEAN_AND_MEAN
#include <windows.h>

//-------------------------------------
// C L A S S  D E C L
//-------------------------------------

///Class to manage a semaphore
class Semaphore
{
    public:

    /// @brief Semaphore constructor.
    Semaphore();


    /// @brief ~Semaphore destructor
    ~Semaphore();

    /// @brief post is used to increment semaphore.
    /// 
    /// @return true if was able to increment semaphore.
    bool post();
    

    /// @brief wait is used to decrement a semaphore.
    /// 
    /// @return true if was able to decrement semaphore.
    bool wait();


    /// @brief Decrement a semaphore, if possible. Do not wait.
    /// 
    /// @return true if was able to decrement semaphore.
    bool trywait();


    /// @brief 
    /// timedwait is used to wait no longer than a given amount
    /// of seconds to decrement a semaphore.
    /// 
    /// @param time Max number of seconds to wait.
    /// 
    /// @return true if was able to decrement semaphore.
    bool timedwait(unsigned int time);

    private:
    bool m_bActive;
    HANDLE m_Semaphore;
};
#endif 
