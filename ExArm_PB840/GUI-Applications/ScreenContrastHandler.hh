#ifndef ScreenContrastHandler_HH
#define ScreenContrastHandler_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: ScreenContrastHandler - Handles setting of Screen Contrast
// (initiated by the Screen Contrast off-screen key).
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/ScreenContrastHandler.hhv   25.0.4.0   19 Nov 2013 14:08:22   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 002  By:  sah   Date:  17-Jul-1997    DCS Number:  2270
//  Project:  Sigma (R8027)
//  Description:
//      Added a new static method for processing all display contrast
//      changes.
//
// Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
// Project:  Sigma (R8027)
// Description:
//             Integration baseline.
//====================================================================

#include "AdjustPanelTarget.hh"
#include "KeyPanelTarget.hh"
#include "SettingObserver.hh"

//@ Usage-Classes
//@ End-Usage

class ScreenContrastHandler : public AdjustPanelTarget,
								public KeyPanelTarget,
								public SettingObserver
{
public:
	ScreenContrastHandler();
	~ScreenContrastHandler(void);

	// AdjustPanelTarget virtual methods
	virtual void adjustPanelKnobDeltaHappened(Int32 delta);
	virtual void adjustPanelAcceptPressHappened(void);
	virtual void adjustPanelClearPressHappened(void);
	virtual void adjustPanelLoseFocusHappened(void);

	// SettingObserver virtual method
    virtual void  valueUpdate(const Notification::ChangeQualifier qualifierId,
							  const SettingSubject*               pSubject);

	// Key Panel Target virtual methods
	virtual void keyPanelPressHappened(KeyPanel::KeyId key);
	virtual void keyPanelReleaseHappened(KeyPanel::KeyId key);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

	//@ Constant: MIN_CONTRAST_
	// The minimum contrast value that we should allow to be set by the
	// user.
	static const Uint16 MIN_CONTRAST_;

	//@ Constant: MAX_CONTRAST_
	// The maximum contrast value that we should allow to be set by the
	// user.
	static const Uint16 MAX_CONTRAST_;

private:
	// these methods are purposely declared, but not implemented...
	ScreenContrastHandler(const ScreenContrastHandler&);// not implemented...
	void   operator=(const ScreenContrastHandler&);		// not implemented...
};


#endif // ScreenContrastHandler_HH
