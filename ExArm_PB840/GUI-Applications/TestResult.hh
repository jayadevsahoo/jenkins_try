#ifndef TestResult_HH
#define TestResult_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: TestResult - Generic service test result handling class which
// allows the controling of test result information.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/TestResult.hhv   25.0.4.0   19 Nov 2013 14:08:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "ServiceDataTarget.hh"

//@ Usage-Classes
#include "TestDataId.hh"
#include "GuiAppClassIds.hh"
class TestResultTarget;
//@ End-Usage

class TestResult : public ServiceDataTarget
{
public:
	// Constant: MAX_STRING_LENGTH
	// This is the longest string that you can use.
	enum {MAX_STRING_LENGTH = 20 };

	TestResult(TestResultTarget *pTarget);
	~TestResult(void);

	virtual void serviceDataChangeHappened(TestDataId::TestDataItemId testId);

    inline const Int& getCommandValue(void);
    inline const Int& getStatusValue(void);
    inline const Int& getConditionValue(void);
    inline wchar_t *getStringValue(void);   
    inline const Real32& getTestDataValue(void);
    inline void setPrecision(Int precision);
    
	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:

private:
	// these methods are purposely declared, but not implemented...
	TestResult(void);						// not implemented...
	TestResult(const TestResult&);			// not implemented...
	void operator=(const TestResult&);		// not implemented...

    //@ Data-Member: targets_
    // Holds pointer to the Result target.
    TestResultTarget *pTarget_;

	//@ Data-Member: command_
	// This field holds the enum test command identifier.
	Int command_;

	//@ Data-Member: status_
	// This field holds the enum test status identifier.
	Int status_;

	//@ Data-Member: condition_
	// This field holds the enum test condition identifier.
	Int condition_;

	//@ Data-Member: testDataValue_
	// This field holds the test data value.
	Real32 testDataValue_;

	//@ Data-Member: string_
	// Contains the string to be formated according to the test result.
	wchar_t string_[MAX_STRING_LENGTH+1];   

	//@ Data-Member: precision_
	// This is the precision number for the test data.
	Int precision_;

};

// Inlined methods
#include "TestResult.in"

#endif // TestResult_HH 
