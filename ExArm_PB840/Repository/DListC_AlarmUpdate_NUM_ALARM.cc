#include "stdafx.h"

//====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: DListC_AlarmUpdate_NUM_ALARM - Fixed Doubly-Linked List of 'AlarmUpdate' Items.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a derived class of an abstract, size-independent
//  doubly-linked list of 'AlarmUpdate' items ('DListC_AlarmUpdate').  This
//  provides an abstraction for a doubly-linked list of generic
//  data items each of type 'AlarmUpdate'.  This list can hold a maximum
//  of 'NUM_ALARM' data items.  The data items of the list are copies of the
//  data items that are added to the list.  There are non-constant,
//  public methods to iterate through the contents of the list, and
//  there's also an iterator class (see 'DIteratorC<AlarmUpdate>') to allow
//  iteration of a constant list.
//---------------------------------------------------------------------
//@ Rationale
//  This class provides a standard abstraction for storing data items
//  in a dynamic collection, where initial placement within the
//  collection is quick and easy.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Each list has an internal heap containing the doubly-linked
//  nodes ('DNodeC_AlarmUpdate').  The nodes are allocated from
//  the heap to contain copies of the inserted data.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  The restrictions have to do with the data type that is chosen for
//  this list.  The data type must provide the following mechanisms,
//  to be allowed to be used in this list:
//>Von
//      AlarmUpdate::AlarmUpdate(const AlarmUpdate& aType);            // copy constructor...
//      AlarmUpdate::operator=(const AlarmUpdate& aType);       // assignment operator...
//      AlarmUpdate::operator==(const AlarmUpdate& aType) const;// equivalence operator...
//      AlarmUpdate::operator!=(const AlarmUpdate& aType) const;// inequivalence operator...
//>Voff
//  For primitive (built-in) types this is already supplied, but for
//  complex (class) types, these must be explicitly supplied.
//
//      The assignment operator of any class used as a type of this list
//      must be implemented such that upon completion of an assignment
//      between two instances, the instances are equivalent (i.e.
//      'a = b; POST_CONDITION(a == b);').
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Foundation/vcssrc/FixedDListC.C_v   25.0.4.0   19 Nov 2013 14:03:46   pvcs  $
//
//@ Modification-Log
//
//   Revision: 002   By: sah   Date: 07-Jan-1999   DR Number: 5321
//   Project:  ATC
//   Description:
//	Changed 'SoftFault()' method to an non-inlined method.
//
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//=====================================================================

#include "DListC_AlarmUpdate_NUM_ALARM.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  DListC_AlarmUpdate_NUM_ALARM(dList)  [Copy Constructor]
//
//@ Interface-Description
//  Construct a new list using 'dList' for initialization.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The address of this list's heap of nodes is passed to the base
//  class's constructor.  The base class's constructor will ALWAYS
//  run before the constructor for 'heapOfNodes', therefore the
//  copying of the elements, MUST be initiated from this constructor,
//  and NOT from the base class.
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  (*this == dList  &&  (isEmpty()  ||
//                        currentItem() == dList.currentItem()))
//@ End-Method
//=====================================================================

DListC_AlarmUpdate_NUM_ALARM::DListC_AlarmUpdate_NUM_ALARM(
				       const DListC_AlarmUpdate_NUM_ALARM& dList
					      )
				       : DListC_AlarmUpdate(&heapOfNodes_),
					 heapOfNodes_()
{
  CALL_TRACE("DListC_AlarmUpdate_NUM_ALARM(dList)");

  operator=(dList);  // this MUST be done here, and not in base class...
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  DListC_AlarmUpdate_NUM_ALARM()  [Default Constructor]
//
//@ Interface-Description
//  Construct an empty list.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The address to this list's heap of nodes is passed to the base
//  class's constructor.
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  (isEmpty())
//@ End-Method
//=====================================================================

DListC_AlarmUpdate_NUM_ALARM::DListC_AlarmUpdate_NUM_ALARM(void)
			      : DListC_AlarmUpdate(&heapOfNodes_),
				heapOfNodes_()
{
  CALL_TRACE("DListC_AlarmUpdate_NUM_ALARM()");
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ~DListC_AlarmUpdate_NUM_ALARM()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this list and the items that is contains.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The list MUST be emptied out from this destructor, as opposed
//  to the base class's destructor, because the heap's destructor
//  will run BEFORE the base class's destructor.
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

DListC_AlarmUpdate_NUM_ALARM::~DListC_AlarmUpdate_NUM_ALARM(void)
{
  CALL_TRACE("~DListC_AlarmUpdate_NUM_ALARM()");

  clearList();          // the list MUST be cleared from here...
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pBoolTest)
//                      [static]
//
//@ Interface-Description
//      Report the software fault that occured within the source code
//      of this class.  The fault, indicated by 'softFaultID', occured
//      at 'lineNumber' in 'pFileName'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//      none
//---------------------------------------------------------------------
//@ PostConditions
//      none
//@ End-Method
//=====================================================================

void
DListC_AlarmUpdate_NUM_ALARM::SoftFault(const SoftFaultID softFaultID,
                                   const Uint32      lineNumber,
                                   const char*       pFileName,
                                   const char*       pBoolTest)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pBoolTest)");
  FaultHandler::SoftFault(softFaultID, FOUNDATION, FIXED_DLISTC,
                          lineNumber, pFileName, pBoolTest);
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

#if defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)

#include "Ostream.hh"

#if defined(SIGMA_DEBUG)
#  include "Foundation.hh"
#endif  // defined(SIGMA_DEBUG)

//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  print_(ostr, dList)  [const, virtual]
//
// Interface-Description
//  This method overrides the base class's pure virtual method, and
//  dumps the current state of this list out to 'ostr' in an
//  ASCII format.
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreConditions
//  none
//---------------------------------------------------------------------
// PostConditions
//  none
// End-Method
//=====================================================================

Ostream&
DListC_AlarmUpdate_NUM_ALARM::print_(Ostream& ostr) const
{
  CALL_TRACE("print_(ostr)");

  ostr << "DListC_AlarmUpdate_NUM_ALARM {\n";

  if (!isEmpty())
  {
    const DNodeC_AlarmUpdate*pNode = getFirst_();

    do
    {
      ostr << ((pNode == getCurrent_()) ? "->" : "  ")
	   << pNode->getItem() << '\n';
    } while ((pNode = pNode->getNextPtr()) != getFirst_());
  }

#if defined(SIGMA_DEBUG)
  if (Foundation::IsDebugOn(::ABSTRACT_HEAP) ||
      Foundation::IsDebugOn(::FIXED_HEAP))
  {
    // debugging the heap, therefore print it out...
    ostr << "------\n";
    ostr << "  heapOfNodes_ = " << heapOfNodes_;
  }
#endif  // defined(SIGMA_DEBUG)

  ostr << "};\n";
  ostr.flush();

  return(ostr);
}

#endif  // defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)
