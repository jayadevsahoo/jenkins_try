#ifndef GraphicsContext_HH
#define GraphicsContext_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: GraphicsContext -- Clipping for VGA library.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/VGA-Graphics-Driver/vcssrc/GraphicsContext.hhv   25.0.4.0   19 Nov 2013 14:37:28   pvcs  $
//
//@ Modification-Log
//  
//  Revision: 001   By: gdc    Date:  27-Jan-2000    DCS Number: 5337
//  Project: 840 Neonatal
//  Description:
//	Optimized VGA library for Neonatal project to use BitBlt and Windows
//  Bitmap structures.
//
//====================================================================

#include "VgaGraphicsDriver.hh"

//@ Usage-Classes
//@ End-Usage

class DisplayContext;

class GraphicsContext 
{
  public:
	GraphicsContext();
	~GraphicsContext();

    void     setClip(const DisplayContext & rDc, Int x0, Int y0, Int x1, Int y1);
    Boolean  clipLine(Int* pX0, Int* pY0, Int* pX1, Int* pY1) const;

    inline Int    getClipX0(void) const;
    inline Int    getClipY0(void) const;
    inline Int    getClipX1(void) const;
    inline Int    getClipY1(void) const;

    inline void   setForegroundColor(Uint color);
    inline Uint   getForegroundColor(void) const;	

   
    inline void   setLineWidth(Uint16 lineWidth);
    inline Uint16 getLineWidth(void) const;

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL, 
			   const char*       pPredicate = NULL);

  private:
    //@ Type:  ClippingCode_
    // Return code for the calculation of whether a line violates the
    // current clipping region.
    enum ClippingCode_
    {
      BELOW_MASK = (Int)(0x0001 << 0),
      ABOVE_MASK = (Int)(0x0001 << 1),
      RIGHT_MASK = (Int)(0x0001 << 2),
      LEFT_MASK  = (Int)(0x0001 << 3)
    };

    inline Int  calcClippingCode_(Int x, Int y) const;

    //@ Data-Member:  clipX0_
    // storage for the left-most vertical boundary.
    Uint16  clipX0_;

    //@ Data-Member:  clipY0_
    // storage for the upper-most horizontal boundary.
    Uint16  clipY0_;

    //@ Data-Member:  clipX1_
    // storage for the right-most vertical boundary.
    Uint16  clipX1_;

    //@ Data-Member:  clipX1_
    // storage for the lower-most horizontal boundary.
    Uint16  clipY1_;
	
	//@ Data-Member:  lineWidth_
	// line width
	Uint16  lineWidth_;
	
	//@ Data-Member:  foregroundColor_
	// foreground color
	Uint    foregroundColor_;
};

#include "GraphicsContext.in"

#endif // GraphicsContext_HH 
