
#ifndef NovRamUpdateManager_HH
#define NovRamUpdateManager_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmit by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class:  NovRamUpdateManager - Manager for the updates to NOVRAM.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Persistent-Objects/vcssrc/NovRamUpdateManager.hhv   25.0.4.0   19 Nov 2013 14:18:52   pvcs  $
//
//@ Modification-Log
//
//  Revision: 007  By:  gdc    Date:  12-Dec-2008    SCR Number: 6068
//  Project:  840S
//  Description:
//		Removed SUN prototype code.
//
//  Revision: 006  By:  sah    Date:  01-Jan-1999    DCS Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' to a non-inlined method.
//
//  Revision: 005  By:  sah    Date:  29-Sep-1997    DCS Number: 2525
//  Project:  Sigma (R8027)
//  Description:
//	No longer monitoring updates of the Vent-Inop Test Info.
//
//  Revision: 004  By:  sah    Date:  04-Sep-1997    DCS Number: 1923
//  Project:  Sigma (R8027)
//  Description:
//      Changed the name of the Communication Diagnostic Log to System
//	Information Log.
//
//  Revision: 003  By:  sah    Date:  04-Sep-1997    DCS Number: 2269
//  Project:  Sigma (R8027)
//  Description:
//      Added the registering of changes to the real-time clock.  Also,
//	added comments for clarity.
//
//  Revision: 002  By:  sah    Date:  06-Aug-1997    DCS Number: 1803
//  Project:  Sigma (R8027)
//  Description:
//      Added registering of changes to the Alarm History Log.
//
//  Revision: 001  By:  sah    Date:  15-Mar-1997    DCS Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//          Initial version (Integration baseline).
//
//====================================================================

#include "Sigma.hh"
#include "PersistentObjs.hh"
#include "IpcIds.hh"

//@ Usage-Classes
//@ End-Usage


class NovRamUpdateManager
{
  public:
    //@ Type:  UpdateTypeId
    // The type of data item that is being updated.
    enum UpdateTypeId
    {
      LOW_DIAG_LOG_VALUE,

      SYS_INFO_LOG_UPDATE = LOW_DIAG_LOG_VALUE,
      SYSTEM_LOG_UPDATE,
      EST_LOG_UPDATE,
      SST_LOG_UPDATE,

      HIGH_DIAG_LOG_VALUE = SST_LOG_UPDATE,
      LOW_RESULT_TABLE_VALUE,

      EST_RESULT_TABLE_UPDATE = LOW_RESULT_TABLE_VALUE,
      SST_RESULT_TABLE_UPDATE,

      HIGH_RESULT_TABLE_VALUE = SST_RESULT_TABLE_UPDATE,
      LOW_MISC_DATA_VALUE,

      SYSTEM_OPER_TIME_UPDATE = LOW_MISC_DATA_VALUE,
      COMPRESSOR_OPER_TIME_UPDATE,
      INSP_CCT_RESISTANCE_UPDATE,
      EXH_CCT_RESISTANCE_UPDATE,

      HIGH_MISC_DATA_VALUE = EXH_CCT_RESISTANCE_UPDATE,

      // this is set to the total number of BD data items that are monitored
      // by the GUI...
      NUM_BD_UPDATE_ITEMS,

      ALARM_LOG_UPDATE = NUM_BD_UPDATE_ITEMS,
      REAL_TIME_CLOCK_UPDATE,

      // this is set to the total number of monitored items/events...
      NUM_UPDATE_ITEMS
    };

    static void  Initialize(void);

#if defined(SIGMA_GUI_CPU)
    static void  RegisterForUpdates(const UpdateTypeId updateItem,
				    const IpcId        registerQueue,
				    const Int32        registerMsg);

#endif // defined(SIGMA_GUI_CPU)

    static void  UpdateHappened(const UpdateTypeId updateItem);

#if defined(SIGMA_BD_CPU)
    static inline void  UpdateCompleted(
			  const NovRamUpdateManager::UpdateTypeId updateId
				       );
#endif // defined(SIGMA_BD_CPU)

    static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL, 
			  const char*       pPredicate = NULL);

    //@ Type:  RegisterInfo
    // Structure to store the notification register information.
    struct RegisterInfo
    {
#if defined(SIGMA_GUI_CPU)
      IpcId  registeredQueue;
      Int32  registeredMsg;
#elif defined(SIGMA_BD_CPU)
      Boolean  hasChanged;
      Int32    unused;
#endif // defined(SIGMA_GUI_CPU)
    };

#if defined(SIGMA_GUI_CPU)
    //@ Type:  MAX_NUM_CALLBACKS
    // Maximum number of callbacks allowed for each update item.
    enum { MAX_NUM_CALLBACKS = 3 };
#endif // defined(SIGMA_GUI_CPU)
  
  private:
    NovRamUpdateManager(const NovRamUpdateManager&);	// not implemented...
    NovRamUpdateManager (void);				// not implemented...
    ~NovRamUpdateManager(void);				// not implemented...
    void  operator=(const NovRamUpdateManager&);	// not implemented...

    //@ Data-Member:  ArrRegisteredItems_
    // Registration information for ALL of the update items.
    static RegisterInfo  ArrRegisteredItems_[NUM_UPDATE_ITEMS]
#if defined(SIGMA_GUI_CPU)
    					    [MAX_NUM_CALLBACKS]
#endif // defined(SIGMA_GUI_CPU)
							;

};


// Inlined methods...
#include "NovRamUpdateManager.in"


#endif // NovRamUpdateManager_HH
