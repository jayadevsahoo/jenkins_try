#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2006, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: NifManeuver - Negative Inspiratory Force (NIF) Maneuver Handler
//---------------------------------------------------------------------
//@ Interface-Description
// Handles the NIF maneuver events originated by the UI. This class is 
// a derived Maneuver. The Maneuver base class provides data and
// methods common to all maneuvers. This class expands the base class
// functionality for processing the Negative Inspiratory Force
// maneuver. The Negative Inspiratory Force (NIF) maneuver is a 
// feature of the Respiratory Mechanics Option.
//---------------------------------------------------------------------
//@ Rationale
// This class implements the NIF maneuver functionality on the BD.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ Fault-Handling
// n/a
//---------------------------------------------------------------------
//@ Restrictions
// none
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/NifManeuver.ccv   25.0.4.0   19 Nov 2013 13:59:54   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001   By: gdc   Date:  22-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Initial version.
//
//=====================================================================

#include "NifManeuver.hh"
#include "BreathMiscRefs.hh"

//@ Usage-Classes
#include "IntervalTimer.hh"
#include "Trigger.hh"
#include "TriggersRefs.hh"
#include "MsgQueue.hh"
#include "BdQueuesMsg.hh"
#include "PhaseRefs.hh"
#include "BreathRecord.hh"
#include "BreathSet.hh"
#include "ApneaInterval.hh"
#include "ImmediateBreathTrigger.hh"

//@ End-Usage

//@ Code...
//=====================================================================
//
//  //@ Data-Member: Methods..
//
//
//=====================================================================

// $[RM12078] ...cancelled if 30 seconds elapse from the time the Start button is pressed...
// $[RM12112] an active NIF...completed if 30 seconds elapse from the time the Start button is pressed...
extern const Int32 MAX_NIF_MANEUVER_TIME;
const Int32 MAX_NIF_MANEUVER_TIME = 30000;

extern const Int32 PAUSE_INTERVAL_EXTENSION ;

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: NifManeuver
//
//@ Interface-Description
//  The method takes a ManeuverId and a reference to a IntervalTimer as
//  arguments. It calls the base class constructor and initializes its
//  data members.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

NifManeuver::NifManeuver(ManeuverId maneuverId, IntervalTimer& rPauseTimer) :
	Maneuver(maneuverId),
    TimerTarget(),
    rPauseTimer_(rPauseTimer),
    elapsedTime_(0),
    isActiveAckSent_(FALSE),
	isButtonPressed_(FALSE),
	isTimedOut_(FALSE)
{
	CALL_TRACE("NifManeuver::NifManeuver(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~NifManeuver()  [Destructor]
//
//@ Interface-Description
// Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

NifManeuver::~NifManeuver(void)
{
  CALL_TRACE("~NifManeuver()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: timeUpHappened
//
//@ Interface-Description
// This method takes a reference to an IntervalTimer and has no return
// value. The NIF maneuver is a client of the timer server that 
// notifies its client (by invoking this method) of the end of the NIF
// maneuver interval.
//---------------------------------------------------------------------
//@ Implementation-Description
// The IntervalTimer reference, rTimer, is that of the interval timer
// server for the NIF maneuver.  
//---------------------------------------------------------------------
//@ PreCondition
// &rTimer == &rPauseTimer_
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
void
NifManeuver::timeUpHappened(const IntervalTimer& rTimer)
{
    CALL_TRACE("NifManeuver::timeUpHappened(const IntervalTimer& rTimer)");

	// $[RM12078] ...shall be cancelled if 30 seconds elapse from the time the Start button is pressed without detecting patient inspiration effort to activate the maneuver.
	// $[RM12112] ...shall be successfully completed if 30 seconds elapse from the time the Start button is pressed.

    // make sure the caller is the proper server
    CLASS_PRE_CONDITION(&rTimer == &rPauseTimer_);

	// Stop the timer
    rPauseTimer_.stop();
    rPauseTimer_.setCurrentTime(0);

	isTimedOut_ = TRUE;

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: handleManeuver
//
//@ Interface-Description
// The method takes a Boolean (isStartRequest) as an argument and 
// returns an EventData::EventStatus. It is called to start the maneuver
// (isStartRequest is TRUE) or stop the maneuver (isStartRequest is
// FALSE). The return status indicates the state of the maneuver after
// processing the request. The client then communicates this state to
// the GUI.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

EventData::EventStatus
NifManeuver::handleManeuver(const Boolean isStartRequest)
{
    CALL_TRACE("NifManeuver::handleManeuver(const Boolean isStartRequest)");

	EventData::EventStatus rtnStatus = EventData::IDLE;

	if (isStartRequest)
	{
		isButtonPressed_ = TRUE;

		if (maneuverState_ == PauseTypes::IDLE)
		{
			// $[RM12070] ...started by pressing and holding the Start button...

			// User pressed the NIF Start button while IDLE so start the maneuver

		    elapsedTime_ = 0;

			// maneuverState_ is IDLE so we allow the NIF maneuver to happen
		    // adjust the target time
		   	rPauseTimer_.setTargetTime(MAX_NIF_MANEUVER_TIME);

		    // start the timer to check the duration when the pause was requested
		   	rPauseTimer_.restart();
			isTimedOut_ = FALSE;

			setActiveManeuver();

            // $[RM12075] ...apnea interval shall be extended by... 30 seconds.

			// since we hold off inspiration while maneuver is pending, extend apnea here
			RApneaInterval.extendInterval(MAX_NIF_MANEUVER_TIME + PAUSE_INTERVAL_EXTENSION);

			// arm the maneuver by signaling the scheduler
			RArmManeuverTrigger.enable();

			// armed and activated via the scheduler
			maneuverState_ = PauseTypes::PENDING;

			// This return status shall update the ExpiratoryPauseEvent's
			// eventStatus_ through UiEvent.cc (UiEvent::setEventStatus)
			// This return status shall be sent to GUI to communicate to
			// the user through UiEvent::setEventStatus too.
			rtnStatus = EventData::PENDING;
		}
		else if (maneuverState_ == PauseTypes::PENDING ||
				 maneuverState_ == PauseTypes::ACTIVE)
		{
			// If everything is working on the GUI, this branch will never be taken
			// but it's better to ignore a duplicate request than to assert.
			// A NIF maneuver was requested before. Just send an ack to the GUI.
			rtnStatus = EventData::NO_ACTION_ACK;
		}
		else
		{
			// Do nothing
			// Note: we do not perform any special operation for IDLE_PENDING
			// due to that this status will be reset by the current running
			// scheduler to IDLE in the same 5 ms cycle.
            AUX_CLASS_ASSERTION( (maneuverState_ == PauseTypes::IDLE_PENDING),
                             	 maneuverState_ );
		}
	}
	else
	{
		// $[RM12071] ...shall terminate when the operator releases the Start button.

		// User released the NIF Start Button

		// stop the maneuver in the next BD cycle by setting isButtonPressed_ FALSE
		isButtonPressed_ = FALSE;
		rtnStatus = EventData::NO_ACTION_ACK;
	}
	
	return (rtnStatus);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activate
//
//@ Interface-Description
//  This method takes no arguments and returns nothing.
//	This method is invoked by the NifPausePhase breath phase when 
//  taking control in the current scheduler. This method records the 
//  baseline (starting) pressure and notifies the GUI that the manuever 
//  is active.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
NifManeuver::activate(void)
{
	CALL_TRACE("NifManeuver::activate(void)");

    // $[RM12107] ...transition to active upon detecting patient effort...

	CLASS_ASSERTION(maneuverState_ = PauseTypes::PENDING);

	baselinePressure_ = BreathRecord::GetAirwayPressure();
	isActiveAckSent_ = FALSE;
	nifPressure_ = 130.0;

	// Setup active maneuver state
	maneuverState_ = PauseTypes::ACTIVE;

	// notify the user that NIF maneuver is active:
	RNifManeuverEvent.setEventStatus(EventData::ACTIVE);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: complete
//
//@ Interface-Description
// This method takes no arguments and returns a Boolean. It is 
// invoked when the NIF maneuver completes. It returns TRUE if there
// are no more pending NIF maneuvers.
//---------------------------------------------------------------------
//@ Implementation-Description
// Invokes clear() to complete the NIF maneuver and clean up.
// Always returns TRUE to indicate there are no more pending NIF
// maneuvers since NIF maneuvers are not queued or stacked.
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
Boolean
NifManeuver::complete(void)    
{
    CALL_TRACE("NifManeuver::complete(void)");

	clear();

	return (TRUE);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: terminateManeuver
//
//@ Interface-Description
//  This method takes no arguments and returns nothing. Deactivates 
//  the maneuver on the next BD cycle.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Sets the clearRequested_ flag TRUE to cancel the maneuver on the 
//  next BD cycle in newCycle().
//---------------------------------------------------------------------
//@ PreCondition
//    none.
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

void
NifManeuver::terminateManeuver(void)
{
	clearRequested_ = TRUE;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processUserEvent
//
//@ Interface-Description
// This method takes a reference to a UiEvent and returns a Boolean
// to indicate whether the UiEvent can be processed while this NIF
// maneuver is active. Returns TRUE to continue processing of the 
// new UiEvent, otherwise returns FALSE.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none.
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

Boolean
NifManeuver::processUserEvent(UiEvent &rUserEvent)
{
	Boolean continueNewEvent = TRUE;

	if (rUserEvent.getId() == EventData::NIF_MANEUVER)
	{
		continueNewEvent = TRUE;
	}
	else if (rUserEvent.getId() == EventData::MANUAL_INSPIRATION)
	{
        // $[RM12111] A pending NIF Maneuver shall be terminated if the operator issues a Manual Inspiration.

		continueNewEvent = TRUE;

		// a manual insp imust cancel the NIF maneuver because a 
		// manual insp breath restarts the breath timer and reenables
		// the patient triggers that must be disabled during the NIF maneuver
		clearRequested_ = TRUE;

		// if the maneuver is ACTIVE move to the IDLE_PENDING state
		// immediately since the scheduler picks up the manual insp
		// trigger and forces the maneuver to exit before 
		// NifManeuver::newCycle can run to move to the IDLE_PENDING state

		if (maneuverState_ == PauseTypes::ACTIVE)
		{
            // $[RM12110] An active NIF maneuver shall be successfully terminated upon detecting a Manual Inspiration request.
			maneuverState_ = PauseTypes::IDLE_PENDING;
		}
	}
	else
	{
		// $[RM12030] If another pause or RM maneuver is pending or active, a new RM maneuver request shall be rejected and the active or pending pause or RM maneuver shall be completed.
		// $[RM12033] All RM maneuver requests shall be rejected if any other pause or RM maneuver has already taken place during the same breath.
 
		// reject any other maneuver request
		rUserEvent.setEventStatus(EventData::REJECTED);
		continueNewEvent = FALSE;
	}

	return(continueNewEvent);
}




//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newCycle
//
//@ Interface-Description
// This method takes no arguments and returns nothing. It is 
// invoked every BD cycle. It implements a state machine that defines
// the NIF maneuver. It processes external events such as button
// press/release events and timeouts using internal variables that are    
// examined every BD cycle. Processing of these events are based on 
// the current state of the maneuver.
//---------------------------------------------------------------------
//@ Implementation-Description
// The actions of this method are dependent on the state of the 
// maneuver when this method is called. It uses triggers to signal 
// the scheduler and uses the NifManeuverEvent.setEventStatus() method 
// to send maneuver status to the GUI. It processes external events 
// such as button press/release events and timeouts using Boolean flags 
// that are examined every BD cycle.
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
void
NifManeuver::newCycle(void)    
{
	CALL_TRACE("NifManeuver::newCycle_(void)");

	elapsedTime_ += ::CYCLE_TIME_MS;

	if (maneuverState_ == PauseTypes::PENDING)
	{
		// $[RM12070] A NIF maneuver shall be started by pressing...

		if (isTimedOut_ || clearRequested_ || !isButtonPressed_)
		{
			// $[RM12071] ...shall terminate when the operator releases the Start button.
            // $[RM12078] ...shall be cancelled if 30 seconds elapse from the time the Start button is pressed without detecting patient inspiration effort to activate the maneuver.

			// disable the arm maneuver trigger in case we process a
			// start and a stop request in the same cycle
			// and signal scheduler to resume normal breath delivery
			RArmManeuverTrigger.disable();
			RDisarmManeuverTrigger.enable();
			clear();
		}

	}
	else if (maneuverState_ == PauseTypes::ACTIVE)
	{
		// $[RM12107] ...transition to active upon detecting patient effort...

		if (isTimedOut_ || clearRequested_ || !isButtonPressed_)
		{
			// $[RM12071] ...shall terminate when the operator releases the Start button.
			// $[RM12112] ...shall be successfully completed if 30 seconds elapse from the time the Start button is pressed.

			// trigger NifPausePhase to complete maneuver
			RPauseCompletionTrigger.enable();

			// set state to IDLE_PENDING so the maneuver completes normally
			// and returns a COMPLETE status to the GUI when NifPausePhase
			// terminates and completes the maneuver
			maneuverState_ = PauseTypes::IDLE_PENDING;
		}

		if (!isActiveAckSent_ && rPauseTimer_.getCurrentTime() >= 100)
		{
			// $[RM12035] ...shall also indicate the activation...

			// audible ack upon activation + 100 ms 
			isActiveAckSent_ = TRUE;
			RNifManeuverEvent.setEventStatus(EventData::AUDIO_ACK);
		}

        // $[RM12060] ...the minimum negative circuit pressure observed during the maneuver...
		// $[RM12108] Circuit pressure shall be monitored throughout the maneuver.

		// NIF is the maximum negative pressure from PEEP so we add 2.0 cmH2O to 
		// account for the pressure deflection required to activate the maneuver
		nifPressure_ = MIN_VALUE(nifPressure_, 
								 (BreathRecord::GetAirwayPressure() -
								  baselinePressure_ - 2.0F) );
	}
	else if ( maneuverState_ == PauseTypes::IDLE_PENDING )
	{
		CLASS_ASSERTION_FAILURE();
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: clear
//
//@ Interface-Description
// This method takes no arguments and returns nothing.  It is 
// invoked every time the NIF maneuver completes or is terminated.
// Its actions and processing are based on the state of the maneuver
// when called. It signals the scheduler, informs the GUI of the 
// maneuver's completion status and signals to send NIF data to the 
// GUI. It resets its internal data in preparation for the next NIF
// request.
//---------------------------------------------------------------------
//@ Implementation-Description
// The actions of this method are dependent on the state of the 
// maneuver when this method was called. 
// It uses triggers to signal maneuver completion to the scheduler.
// It calls the NifManeuverEvent.setEventStatus() method 
// to send the maneuver completion status to the GUI.
// It uses MsgQueue::PutMsg to signal the BreathData task to send
// the NIF data to the GUI.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
void
NifManeuver::clear(void)    
{
    CALL_TRACE("NifManeuver::clear(void)");

	if (maneuverState_ == PauseTypes::PENDING)
	{
		// $[RM12077] A NIF maneuver shall be canceled if...

		// disable the arm maneuver trigger in case we got here due to a
		// mode change (eg. disconnect) and the trigger hasn't been serviced yet
		RArmManeuverTrigger.disable();
		RDisarmManeuverTrigger.enable();

		// send status to GUI
		RNifManeuverEvent.setEventStatus(EventData::CANCEL);
	}
	else if (maneuverState_ == PauseTypes::ACTIVE)
	{
		// $[RM12077] A NIF maneuver shall be canceled if...

		// NifPausePhase terminated due to an unexpected trigger
		RNifManeuverEvent.setEventStatus(EventData::CANCEL);
	}
	else if (maneuverState_ == PauseTypes::IDLE_PENDING)
	{
        // $[RM12058] ...the calculated NIF result shall be displayed on the Waveforms screen...
        // $[RM12113] ..the calculated NIF result shall be displayed on the NIF subscreen...

		RNifManeuverEvent.setEventStatus(EventData::COMPLETE);
		// signal to send NIF pressure data to GUI
		MsgQueue::PutMsg(::BREATH_DATA_Q, BdQueuesMsg::SEND_END_NIF_PAUSE_DATA);
	}
	else
	{
		AUX_CLASS_ASSERTION_FAILURE(maneuverState_);
	}

	maneuverState_ = PauseTypes::IDLE;
	
    rPauseTimer_.stop();
    rPauseTimer_.setCurrentTime(0);
	isTimedOut_ = FALSE;
	isButtonPressed_ = FALSE;

    RPauseCompletionTrigger.disable();

   	resetActiveManeuver();
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
NifManeuver::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, NIFMANEUVER,
                          lineNumber, pFileName, pPredicate);
}

//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//=====================================================================
//
//  Private Methods...
//
//=====================================================================


