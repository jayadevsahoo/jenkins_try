#include "stdafx.h"

//#############################################################################
//
//  AioDioDriver.cpp - Implementation file for AioDioDriver_t class.
//
//#############################################################################

#include "Sigma.hh"
#include "AioDioDriver.h"
#include "AioDio.h"
#include "AnalogInputs.h"
#include "bsp_version.h"
#include "sdk_spi.h"
#include <gpio_ioctls.h>
#include "SpiFlash.h"
#include "DebugAssert.h"

extern SpiFlash_t SpiFlash;

//=====================================================================
//
//  Global Memory Allocation
//
//=====================================================================

// AioDioDriver should be instantiated before any objects that use it
AioDioDriver_t				AioDioDriver;

//#if defined(SIGMA_GUI_CPU) && !defined(SIGMA_GUIPC_CPU)
//TODO E600 need to investigate if this code is required 
//#include "SpiLoopback.h"
//extern SpiLoopback_t  SpiLoopbackSlot1;
//extern SpiLoopback_t  SpiLoopbackSlot3;
//#endif

enum DigitalPorts_t
{
	DIGITAL_OUTPUT_2=DIGITAL_PORT_0,
	DIGITAL_INPUT_1=DIGITAL_PORT_1,
	DIGITAL_INPUT_0=DIGITAL_PORT_2,
	DIGITAL_OUTPUT_0=DIGITAL_PORT_3,
	DIGITAL_OUTPUT_1=DIGITAL_PORT_4
};

struct DigitalSignalPhysicalLocation_t
{
	DigitalPorts_t      Port;
	uint8_t             BitPosition;                // Values are 0 - 7
};
union EEPROM_DATA_t
{
	struct 
	{
		BYTE Data;
		BYTE Address;
		BYTE Comand;
		BYTE Data0;
	} Detailed; 
	UINT32 DataWord;
};
#ifdef SIGMA_GUI_CPU
// MAX3107 - SPI to UART
//static const IOCTL_SPI_CONFIGURE_IN spi3_CH0_UART_config = {0, 0x000107E0};	//0000 0000 0000 0001 0000 0111 1110 0000
/*	TRM (13:12)		:	00		- TXRX
WL (11:7)		:	0xF	    - 16 bits
EPOL (6)		:	1		- SPIM_CSX held low during active state
CLKD (5:2)		:	8		- Divide Ref Clock of 48Mbps by 256 = 188 Kbps
POL PHA (1:0)	:	00		- clk active high and sampling on rising edge
*/
#endif
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------
#ifdef SIGMA_BD_CPU

static const IOCTL_SPI_CONFIGURE_IN          EEPROM_WRITE_ENABLE = {1, 0x000103D8};	//0000 0000 0000 0001   0000 0011 1101 1000
/*	TRM          :		0		- TXRX mode
WL(7:11)     :		0x7 	- 8 bits
EPOL (6)     :	    1		- SPIM_CSX held low during active state
CLKD (5:2) :		6		- Divide Ref Clock of 48Mbps by 64 = 0.75 Mbps
POL PHA(1:0) :	    00		- clk active high and sampling on rising edge
*/
//                                                                                                             111098 7654 3210
static const IOCTL_SPI_CONFIGURE_IN          EEPROM_READ_STATUS = {1, 0x000107D8};	//0000 0000 0000 0001   0000 0111 1101 1000
/*	TRM          :		0		- TXRX mode
WL(7:11)     :		0xF 	- 16 bits
EPOL (6)     :	    1		- SPIM_CSX held low during active state
CLKD (5:2) :		6		- Divide Ref Clock of 48Mbps by 32 = 1.5 Mbps
POL PHA(1:0) :	    00		- clk active high and sampling on rising edge
*/
//                                                                                                         111098 7654 3210
static const IOCTL_SPI_CONFIGURE_IN         EEPROM_config = {1, 0x00010BD8};	//0000 0000 0000 0001   0000 1011 1101 1000
/*	TRM          :		0		- TXRX mode
WL(7:11)     :		0x17	- 24 bits
EPOL (6)     :	    1		- SPIM_CSX held low during active state
CLKD (5:2) :		6		- Divide Ref Clock of 48Mbps by 32 = 1.5 Mbps
POL PHA(1:0) :	    00		- clk active high and sampling on rising edge
*/
// E600 DAC MAX5135, Max clock = 30 MHz, 24-bit word (8 control bits and 16 data bits; however, D0-D3 are don't-care bits)
// Data shifted into the DAC on the falling edge of SCLK                                                    111098 7654 3210
static const IOCTL_SPI_CONFIGURE_IN spi1_CH3_DAC_config = {3, 0x00010BC9};		// 0000 0000 0000 0001 0000 1011   1100 1001	
//                                                                                                          WL      EPOL(6)
/*	TRM :		00		- TXRX
WL(7:11) :  0x17	- 24 bits
EPOL (6):	  1		- SPIM_CSX held low during active state
CLKD :(5:2)   1		- Divide Ref Clock of 48Mbps by 4 = 12 Mbps (Spec 50)
POL PHA :	 01		- clk active high and sampling on falling edge
*/

// MAX1068 - Max clock is 200K samples per second, used to read ADC on the BD board
static const IOCTL_SPI_CONFIGURE_IN spi1_CH0_ADC_config = {0, 0x00010FD0};		//0000 0000 0000 0001 0000 1111 1101 0000
/*	TRM (13:12)		:	00		- TXRX
WL (11:7)		:	0x1F	- 32 bits
EPOL (6)		:	1		- SPIM_CSX held low during active state
CLKD (5:2)		:	4		- Divide Ref Clock of 48Mbps by 16 = 3Mbps
POL PHA (1:0)	:	00		- clk active high and sampling on rising edge
*/

//16 bits, CPOL=CPHA=0, 932 KHz on E360
static const IOCTL_SPI_CONFIGURE_IN AirSPIconfig = {3, 0x000103D4};	//0000 0000 0000 0001 0000 0011 1101 0100
																		/*	TRM (13:12)		:	00		- TXRX
																			WL (11:7)		:	0x7		- 8 bits
																			EPOL (6)		:	1		- SPIM_CSX held low during active state
																			CLKD (5:2)		:	6		- Divide Ref Clock of 48Mbps by 32 = 1.5MHz
																			POL PHA (1:0)	:	00		- clk active high and sampling on rising edge
																		*/
static const IOCTL_SPI_CONFIGURE_IN O2SPIconfig = {0, 0x000103D4};

#endif

static const DWORD MAX1068_WRITE_COMMAND	= 0x00000000;

static const DWORD DAC_WRITE_CMD			= 0x00100000; // Write individual channel

static const LPCTSTR lpSPI1Name = TEXT("SPI1:");
static const LPCTSTR lpSPI3Name = TEXT("SPI3:");
static const LPCTSTR lpSPI4Name = TEXT("SPI4:");
static const LPCTSTR lpGPIOName = TEXT("GIO1:");

#if defined(SIGMA_GUI_CPU)  && !defined(SIGMA_GUIPC_CPU)
#define ON_BOARD_GPIO_EXPANDER_ADDRESS (0x4E)
#define ON_BOARD_BACKLIGHT_ADDRESS (0x98)
#endif

//#############################################################################
//
//  AioDioDriver_t::AioDioDriver_t - Construct the class
//
//#############################################################################

AioDioDriver_t::AioDioDriver_t()
{
	AioDioInitDone = false; 
	bCalibrationRequest = false;
	InitializeCriticalSection(&AioDioCriticalSection);

	LPCTSTR lpAIOFileName = TEXT("AIO1:");
	hAioDioDriver = CreateFile( lpAIOFileName,  GENERIC_READ | GENERIC_WRITE, 0, 0, OPEN_EXISTING, 0, 0);
	if (!hAioDioDriver)
	{
		RETAILMSG(TRUE, (_T("AnalogInputThread: Can't open 'AIO1:'\r\n")));
		return;
	}

	BSPType = BSP_INVALID;
	DeviceIoControl(hAioDioDriver, IOCTL_AIODIO_GET_BSPTYPE, NULL, 0, &BSPType, sizeof(DWORD), NULL, NULL); 


	InitAioDio();                               // Get the hardware ready for the threads to use
}

//#############################################################################
//
//  AioDioDriver_t::~AioDioDriver_t -
//
//#############################################################################

AioDioDriver_t::~AioDioDriver_t()
{
	DeviceIoControl(hAioDioDriver, IOCTL_AIODIO_DEINIT, NULL, 0, NULL, 0, NULL, NULL);

	#if defined(SIGMA_GUI_CPU)  && !defined(SIGMA_GUIPC_CPU)
	delete PtrIOExpanderXRA1201Gui;
	delete PtrBackLight;
	CloseHandle(hI2c3File);
	#endif
}


void AioDioDriver_t::DeinitAioDio()
{
	if(hAioDioDriver)
	{
		DeviceIoControl(hAioDioDriver, IOCTL_AIODIO_DEINIT, NULL, 0, NULL, 0, NULL, NULL);
		CloseHandle(hAioDioDriver);
	}
}

//#############################################################################
//
//  void AioDioDriver_t::SetBackLight(BYTE DimLevel)
//  DimLevel is 0 (dark) throug 9
//
//#############################################################################

void AioDioDriver_t::SetBackLight(BYTE DimLevel)
{
	BYTE Brightness;
	switch (DimLevel)
	{
	case 0:
	default:
		Brightness = 180;
		break;
	case 1:
		Brightness = 160;
		break;
	case 2:
		Brightness = 140;
		break;
	case 3:
		Brightness = 120;
		break;
	case 4:
		Brightness = 100;
		break;
	case 5:
		Brightness = 80;
		break;
	case 6:
		Brightness = 60;
		break;
	case 7:
		Brightness = 40;
		break;
	case 8:
		Brightness = 20;
		break;
	case 9:
		Brightness = 0;
		break;

	}
#if defined(SIGMA_GUI_CPU)  && !defined(SIGMA_GUIPC_CPU)
	PtrBackLight->Write(&Brightness);
#endif
}


//#############################################################################
//
//  AioDioDriver_t::Write - This version of the write function updates one line
//      of the digital outputs.
//
//#############################################################################

void AioDioDriver_t::Write(ApplicationDigitalOutputs_t DigitalOutput, DigitalOutputStates_t State)
{
	DWORD	dwResult;
	DWORD	AIO_InBuf[WRITE_DIGITAL_IO_PARAMS];

	AIO_InBuf[0] = DigitalOutput;
	AIO_InBuf[1] = State;
	dwResult = DeviceIoControl(hAioDioDriver, IOCTL_AIODIO_WRITE_DIGITAL_IO, AIO_InBuf, sizeof(AIO_InBuf), NULL, 0, NULL, NULL);
}


//#############################################################################
//
//  AioDioDriver_t::Write - This version of the write function writes one GPIO,
//      of the dual direction GPIOs.
//
//#############################################################################
void AioDioDriver_t::Write(DigitalBiDirectional_t DigitalOutput, DigitalOutputStates_t State)
{
	DWORD	dwResult;
	DWORD	AIO_InBuf[WRITE_DIGITAL_IO_PARAMS];

	AIO_InBuf[0] = DigitalOutput;
	AIO_InBuf[1] = State;
	dwResult = DeviceIoControl(hAioDioDriver, IOCTL_AIODIO_WRITE_DIGITAL_IO, AIO_InBuf, sizeof(AIO_InBuf), NULL, 0, NULL, NULL);
}


//#############################################################################
//
//  AioDioDriver_t::Read - This version of the read function reads one GPIO.
//
//#############################################################################
uint8_t AioDioDriver_t::Read(DigitalBiDirectional_t DigitalInput)
{
	DWORD	dwResult;
	DWORD	DataIn[READ_DIGITAL_IO_PARAMS];
	DWORD	DataOut;

	DataIn[0] = DigitalInput;
	dwResult = DeviceIoControl(hAioDioDriver, IOCTL_AIODIO_READ_DIGITAL_IO, &DataIn[0], sizeof(DataIn), &DataOut, sizeof(DWORD), NULL, NULL);
	return((uint8_t)DataOut);
}

//#############################################################################
//
//  AioDioDriver_t::Read - This version of the read function reads one signal
//      of the digital inputs.
//
//      Return 1 if signal is high,
//             0 if signal is low
//
//#############################################################################

uint8_t AioDioDriver_t::Read(ApplicationDigitalInputs_t DigitalInput)
{
	DWORD	dwResult;
	DWORD	DataIn[READ_DIGITAL_INPUT_PARAMS];
	DWORD	DataOut;

	DataIn[0] = DigitalInput;
	dwResult = DeviceIoControl(hAioDioDriver, IOCTL_AIODIO_READ_DIGITAL_IO, &DataIn[0], sizeof(DataIn), &DataOut, sizeof(DWORD), NULL, NULL);
	return((uint8_t)DataOut);
}
//#############################################################################
//
//  AioDioDriver_t::SetDirection - Set a GPIO pin's direction.
//
//#############################################################################

void AioDioDriver_t::SetDirection(DigitalBiDirectional_t DigitalInputOutput, GpioDirection_t Direction)
{
	DWORD	dwResult;
	DWORD	AIO_InBuf[SET_DIGITAL_IO_DIR_PARAMS];

	AIO_InBuf[0] = DigitalInputOutput;
	AIO_InBuf[1] = Direction;
	dwResult = DeviceIoControl(hAioDioDriver, IOCTL_AIODIO_SET_DIGITAL_IO_DIR, AIO_InBuf, sizeof(AIO_InBuf), NULL, 0, NULL, NULL);
}

#ifdef SIGMA_BD_CPU
//#############################################################################
//
//  AioDioDriver_t::Read - Read the last loaded channel and at the same time
//          load the next channel to be read.
//
//#############################################################################

bool AioDioDriver_t::Read(AnalogInputs_t AnalogInput, uint16_t *Reading)
{
	bool	bResult = false;

#if USE_FILE_SYSTEM
	DWORD ReadResult;
	static AnalogInputs_t LastInput = ADC_FIRST_CHANNEL;

	// If input is on the manifold board
	if((AnalogInput > ADC_SIGNAL_TEST) && (LastInput <= ADC_SIGNAL_TEST))
	{
		hActiveSPI = hSPI3_CH0;
	}
	else if((AnalogInput <= ADC_SIGNAL_TEST) && (LastInput > ADC_SIGNAL_TEST))
	{
		hActiveSPI = hSPI1_CH0;
	}

	LastInput = AnalogInput;

	UINT32 WriteData;
	UINT32 ReadData;

	WriteData = ((AnalogInput % 8) << 29) | MAX1068_WRITE_COMMAND;
	ReadResult = DeviceIoControl(hActiveSPI, IOCTL_SPI_WRITEREAD, &ReadData, sizeof(DWORD), &WriteData, sizeof(DWORD), NULL, NULL);
	if(!ReadResult)
	{
		RETAILMSG(TRUE, (L"Failed reading ADC input %d\r\n", AnalogInput));
	}
	else
	{		
		*Reading = (uint16_t)((ReadData & 0xFFFF) >> 2);
		if(0)
			RETAILMSG(1, (L"Channel:\t%d\tReadData:\t%d\r\n", AnalogInput, *Reading));
		bResult = true;
	}
#else

	DWORD	DataIn[READ_ANALOG_CHANNEL_PARAMS];
	DWORD	DataOut;

	if(!hAioDioDriver)
		return false;

	DataIn[0] = AnalogInput;
	DataIn[1] = AnalogInput;

	if(DeviceIoControl(hAioDioDriver, IOCTL_AIODIO_READ_ANALOG_CHANNEL, DataIn, sizeof(DataIn), &DataOut, sizeof(DWORD), NULL, NULL))
	{
		*Reading = (uint16_t)DataOut;
		bResult = true;
	}
	else
	{
		DEBUGMSG(1, (_T("Read A2D Failed\r\n")));
		bResult = false;
	}
#endif
	return bResult;
}


bool AioDioDriver_t::Read(AnalogInputs_t FirstAnalogInput, AnalogInputs_t LastAnalogInput, uint32_t *Reading, uint32_t BufSize)
{
	bool	bResult = false;
	DWORD	DataIn[READ_MULTIPLE_ANALOG_CHANNELS_PARAMS];

	if(!hAioDioDriver)
		return false;

	DataIn[0] = FirstAnalogInput;
	DataIn[1] = LastAnalogInput;
	DataIn[2] = ADC_FIRST_MANIFOLD_BOARD_CHANNEL;

	if(DeviceIoControl(hAioDioDriver, IOCTL_AIODIO_READ_MULTIPLE_ANALOG_CHANNELS, DataIn, sizeof(DataIn), Reading, BufSize, NULL, NULL))
	{
		//for(int i = 0; i <= (LastAnalogInput - FirstAnalogInput); i++)
		//	Reading[i] = (uint16_t)DataOut[i];
		bResult = true;
	}
	else
	{
		DEBUGMSG(1, (_T("Read MultiChannel A2D Failed\r\n")));
		bResult = false;
	}
	return bResult;
}

//#############################################################################
//
//  AioDioDriver_t::WriteSpiEprom - Send array of char to SPI 
//
//#############################################################################

bool  AioDioDriver_t::WriteSpiEprom(BYTE SendValue[], BYTE Lenght, WORD StartAddress)
{
	bool bResult=true;
	EEPROM_DATA_t WriteData;
	EEPROM_DATA_t ReadData; 
	BYTE WriteDataByte = INSTRUCTION_WRITE_ENABLE;
	BYTE ReadDataByte;

	for(WORD i = 0; i < Lenght;  i++) 
	{
		// Configure SPI for single byte WE comand
		DeviceIoControl(hSPI3_CH1, IOCTL_SPI_CONFIGURE, (LPVOID)&EEPROM_WRITE_ENABLE, sizeof(IOCTL_SPI_CONFIGURE_IN), NULL, 0, NULL, NULL);
		Sleep(6);
		// Write WE
		WriteDataByte = INSTRUCTION_WRITE_ENABLE;
		DeviceIoControl(hSPI3_CH1, IOCTL_SPI_WRITEREAD, &ReadDataByte, sizeof(BYTE), &WriteDataByte, sizeof(BYTE), NULL, NULL);
		Sleep(6);
		// Configure SPI for 24 bit WRITE comand
		if(!DeviceIoControl(hSPI3_CH1, IOCTL_SPI_CONFIGURE, (LPVOID)&EEPROM_config, sizeof(IOCTL_SPI_CONFIGURE_IN), NULL, 0, NULL, NULL))
		{
			bResult=false;
		}
		WriteData.Detailed.Comand = INSTRUCTION_WRITE; // instruction 
		WriteData.Detailed.Address = (BYTE)(i+StartAddress); //  address
		WriteData.Detailed.Data = SendValue[i];
		WriteData.Detailed.Data0 = 0;
		// Write 24 bits
		if(!DeviceIoControl(hSPI3_CH1, IOCTL_SPI_WRITEREAD, &ReadData, sizeof(DWORD), &WriteData, sizeof(DWORD), NULL, NULL))
		{
			bResult=false;
		}
		Sleep(6);
		if(!bResult)
		{
			RETAILMSG(TRUE, (L"Failed writing Data to EEPROM \r\n"));
		}
		RETAILMSG(0, (TEXT("Write3[%d] 0x%0X  0x%0X\r\n"), i, ReadData, WriteData));
	} // end for() Lenght
	return bResult;
}
//#############################################################################
//
//  AioDioDriver_t::ReadSpiEprom - Send array of char to SPI 
//
//#############################################################################

bool  AioDioDriver_t::ReadSpiEprom(uint8_t *SendValue, BYTE Lenght, WORD StartAddress)
{
	bool bResult = true;
	EEPROM_DATA_t WriteData;
	EEPROM_DATA_t ReadData;
	WriteData.DataWord=0;
	ReadData.DataWord=0;

	for(WORD i = 0; i < Lenght;  i++)
	{
		WriteData.Detailed.Comand = INSTRUCTION_READ; // instruction  
		WriteData.Detailed.Address = (BYTE)(i+StartAddress); //  address
		ReadData.DataWord=0;
		Sleep(6);
		if(!DeviceIoControl(hSPI3_CH1, IOCTL_SPI_CONFIGURE, (LPVOID)&EEPROM_config, sizeof(IOCTL_SPI_CONFIGURE_IN), NULL, 0, NULL, NULL))
		{
			RETAILMSG(TRUE, (L"Can't configure 'SPI3:' Channel 1\r\n"));
			return false;
		}
		Sleep(6);
		if(!DeviceIoControl(hSPI3_CH1, IOCTL_SPI_WRITEREAD, &ReadData, sizeof(DWORD), &WriteData, sizeof(DWORD), NULL, NULL))
		{
			RETAILMSG(TRUE, (L"Failed reading Data \r\n"));
			return false;
		}
		RETAILMSG(0, (TEXT("Read1[%d] 0x%0X 0x%0X\r\n"), i, ReadData, WriteData));
		SendValue[i] = ReadData.Detailed.Data;
		RETAILMSG(0, (TEXT("Read2[%d] %X\r\n"), i, SendValue[i]));
	}

	return bResult;
}

//#############################################################################
//
//  AioDioDriver_t::Write - Write the data to the specified analog device.
//      When the );  is the device, we save the value, then write
//      both the );  and the DAC_EXHALATION_VALVE together to the
//      LTC1448.
//
//#############################################################################

void AioDioDriver_t::Write(AnalogOutputs_t AnalogOutput, DacCounts Value)
{
	DWORD dwResult;
	DWORD DataIn;
	DWORD Command;
	static const DWORD ANALOG_OUTPUT[4] = {0x00000000, 0x00020000, 0x00040000, 0x00060000};

	// 24-bit command, D21..D20: Write Command; D18..D17: Channel; D15..D4: Data; D3..D0: Don't care
	Command = DAC_WRITE_CMD | ANALOG_OUTPUT[(int)AnalogOutput] | (((DWORD)Value & 0x0FFF) << 4);
	dwResult = DeviceIoControl(hAioDioDriver, IOCTL_AIODIO_WRITE_DAC_MOTOR, &Command, sizeof(DWORD), &DataIn, sizeof(DWORD), NULL, NULL);
	if(dwResult)
		RETAILMSG(0, (L"IOCTL_SPI_WRITE: AnalogWrite OK, Command=0x%X\r\n", Command & 0x00FFFFFF)); 
	else
		RETAILMSG(TRUE, (L"Failed writing SPI DAC\n"));
}
#endif
//#############################################################################
//
//  AioDioDriver_t::GetEbootVersion()
//
//#############################################################################
bool AioDioDriver_t::GetEbootVersion(NMI_VERSION* pEbootVersion)
{
	if(DeviceIoControl(hAioDioDriver, IOCTL_AIODIO_GET_NMI_EBOOT_VER, NULL, 0, pEbootVersion, sizeof(NMI_VERSION), NULL, NULL))
		return true;
	else
		return false;
}

//#############################################################################
//
//  AioDioDriver_t::GetOSVersion()
//
//#############################################################################
bool AioDioDriver_t::GetOSVersion(NMI_VERSION* pOSVersion)
{
	if(DeviceIoControl(hAioDioDriver, IOCTL_AIODIO_GET_NMI_OS_VER, NULL, 0, pOSVersion, sizeof(NMI_VERSION), NULL, NULL))
		return true;
	else
		return false;
}

//#############################################################################
//
//  AioDioDriver_t::ReadKnob - This version of the read function reads Encoder Knob.
//
//#############################################################################

bool AioDioDriver_t::ReadKnob(uint8_t& KnobCount, uint8_t& KnobDir)
{
	bool	bResult;
	DWORD	DataOut[2];
#pragma warning(disable:4800)
	bResult = (bool)DeviceIoControl(hAioDioDriver, IOCTL_AIODIO_READ_ENCODER, NULL, 0, &DataOut, sizeof(DataOut), NULL, NULL);
#pragma warning(default:4800)
	KnobCount = (uint8_t)DataOut[0];
	KnobDir = (uint8_t)DataOut[1];	
	return(bResult);
}

//#############################################################################
//
// set speaker value
//
//#############################################################################
void AioDioDriver_t::SetVolume(uint8_t VolumeSetting)
{
	DWORD VolumeOut = VolumeSetting;
	int i;

	for (i=0; i<7; i++)
	{
		VolumeOut <<= 4;
		VolumeOut |= VolumeSetting;
	}


#if !defined(SIGMA_GUIPC_CPU)
	waveOutSetVolume(0, VolumeOut);
#endif
}

/****************************************************************
 *
 * NAME: void ReadTsiTab(IOCTL_SPI_CONFIGURE_IN SpiConfig)
 *
 * PURPOSE: Read calibration data from Air or O2 TSI flow sensor
 *		    EEPROM and store in working data structure.
 *
 * INPUT: calibration data in flow sensor EEPROM
 *
 * OUTPUT: flow sensor Calibration table
 *
 * OUTPUT: none
 *
 * DESCRIPTION:
 *    Read calibration data from flow sensor EEPROM. Verify the checksum. If the
 *    checksum is correct then store data into working data structure, otherwise
 *	  use default data and set error flag for front panel to display
 *	  error message.
 ****************************************************************/

#ifdef SIGMA_BD_CPU

bool AioDioDriver_t::ReadTsiTable(TsiFlowType_t FlowType, uint8_t *InBuffer)
{
	uint8_t SpiCommand[2] = {0x03, 0x00};
	uint8_t SpiData[2];
	DWORD result = 0;
	bool bError = false;
	uint16_t BytesRead = 0;
	IOCTL_SPI_CONFIGURE_IN Config;
	DWORD TimeOut = INFINITE;
			
	// don't care about output data
	uint8_t OutBuffer[TSI_EEPROM_TAB_SIZE];
	memset(OutBuffer, 0, TSI_EEPROM_TAB_SIZE);
	
	// clear input data 
	memset(InBuffer, 0, TSI_EEPROM_TAB_SIZE);

	HANDLE hSpi = CreateFile( lpSPI1Name,  GENERIC_READ | GENERIC_WRITE, 0, 0, OPEN_EXISTING, 0, 0);
	if (!hSpi)
	{
		bError = true;
		RETAILMSG(TRUE, (L"TSI: Can't open SPI1, CH %d for reading TSI\r\n"));
	}
	else
	{
		// enable insp. flow sensor SPI
		Write(GPIO_SPI1_ON_BD, SPI1_DISABLE);
		Write(GPIO_SPI1_OFF_BD, SPI1_ENABLE);

		if(FlowType == TSI_AIR_FLOW)
		{
			Config = AirSPIconfig;
		}
		else if(FlowType == TSI_O2_FLOW)
		{
			Config = O2SPIconfig;
		}
		else
		{
			bError = true;
		}
	}
		
	// must be no error to read
	if(hSpi && (OutBuffer != NULL) && (InBuffer != NULL) && !bError)
	{	
		// lock the Spi channel
		result = DeviceIoControl(hSpi, IOCTL_SPI_LOCK_CTRL, &TimeOut, sizeof(TimeOut), NULL, 0, NULL, NULL);
		
		Config.config |= MCSPI_CHCONF_FORCE;
		result = DeviceIoControl(hSpi, IOCTL_SPI_CONFIGURE, &Config, sizeof(IOCTL_SPI_CONFIGURE_IN), NULL, 0, NULL, NULL);
		
		if(result)
		{
			//send read command
			result = DeviceIoControl(hSpi, IOCTL_SPI_WRITEREAD, &SpiData, sizeof(SpiData),
										&SpiCommand, sizeof(SpiCommand), NULL, NULL);

			//read TSI table
			BytesRead = (uint16_t)(BytesRead + DeviceIoControl(hSpi, IOCTL_SPI_WRITEREAD, InBuffer, TSI_EEPROM_TAB_SIZE,
										OutBuffer, TSI_EEPROM_TAB_SIZE, NULL, NULL));
		}
		
		Config.config &= ~MCSPI_CHCONF_FORCE;
		result = DeviceIoControl(hSpi, IOCTL_SPI_CONFIGURE, &Config, sizeof(IOCTL_SPI_CONFIGURE_IN), NULL, 0, NULL, NULL);
		
		// unlock the Spi channel
		result = DeviceIoControl(hSpi, IOCTL_SPI_UNLOCK_CTRL, NULL, 0, NULL, 0, NULL, NULL);
		
		if(BytesRead != TSI_EEPROM_TAB_SIZE)
		{
			bError = true;	
		}

	}

	// disable insp. flow sensor SPI
	Write(GPIO_SPI1_ON_BD, SPI1_ENABLE);
	Write(GPIO_SPI1_OFF_BD, SPI1_DISABLE);
	
	CloseHandle(hSpi);
	return (!bError);
}

#endif

//#############################################################################
//
//  AioDioDriver_t::InitAioDio - Get the ADC powered up and ready to go.
//      Initialize the digital inputs and outputs. This is all done once
//      a thread is up and running.
//
//#############################################################################

void AioDioDriver_t::InitAioDio()
{
	/////////////////////////////////////////////////////////////////////////
	//    DWORD DataIn[2];
	//    DWORD Command = 0x10fff0;
	/////////////////////////////////////////////////////////////////////////

	static const BYTE SPI_NUMBER_1 = 1;
	static const BYTE SPI_NUMBER_4 = 4;
	if(!AioDioInitDone)
	{
		DeviceIoControl(hAioDioDriver, IOCTL_AIODIO_INIT_HARDWARE, NULL, 0, NULL, 0, NULL, NULL);       

//#if defined(SIGMA_GUI_CPU) && !defined(SIGMA_GUIPC_CPU)
//TODO E600 need to investigate if this code is required 
		// Init SPI driver
//		hSPI3_CH0 = CreateFile( lpSPI3Name,  GENERIC_READ | GENERIC_WRITE, 0, 0, OPEN_EXISTING, 0, 0);	
//		if(hSPI3_CH0 == INVALID_HANDLE_VALUE)
//		{
//			DWORD error=GetLastError();
//			RETAILMSG(TRUE, (L"CreateFile %s failed, error %d\n", lpSPI3Name, error));
//			return;
//		}
//		if(!DeviceIoControl(hSPI3_CH0, IOCTL_SPI_CONFIGURE, (LPVOID)&spi3_CH0_UART_config, sizeof(IOCTL_SPI_CONFIGURE_IN), NULL, 0, NULL, NULL))
//		{
//			DWORD error=GetLastError();
//			RETAILMSG(TRUE, (L"Can't configure 'SPI3:' Channel 0\r\n"));
//			return;
//		}
//		SpiLoopbackSlot1.OpenSpiLoopback(SPI_NUMBER_1);
//		SpiLoopbackSlot3.OpenSpiLoopback(SPI_NUMBER_4);
//		SpiFlash.OpenDevice();
//#endif

#if defined(SIGMA_GUI_CPU)  && !defined(SIGMA_GUIPC_CPU)
	hI2c3File = CreateFile(_T("I2C3:"), GENERIC_READ|GENERIC_WRITE, NULL, NULL, OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL, NULL);

    if (hI2c3File == INVALID_HANDLE_VALUE)
	{
        RETAILMSG(TRUE, (_T("Can't open 'I2C3:' driver: error %i\r\n"), GetLastError()));
		return;
	}

    PtrBackLight = new BackLight_t(hI2c3File, (uint8_t)ON_BOARD_BACKLIGHT_ADDRESS,I2C_SUBADDRESS_MODE_0, I2C_SLOWSPEED_MODE, 40);
	
	DEBUG_ASSERT(PtrBackLight);

	SetBackLight(BRIGHT_100_PER);

    PtrIOExpanderXRA1201Gui = new IOExpanderXRA1201_t(hI2c3File, (uint8_t)ON_BOARD_GPIO_EXPANDER_ADDRESS, (I2cSubAddressMode_t)I2C_SUBADDRESS_MODE_1, 
                                                      (I2cBaudIndex_t)I2C_SLOWSPEED_MODE, (DWORD)40);
	static const BYTE PORT0 = 0;
    static const BYTE PORT1 = 1;
    PtrIOExpanderXRA1201Gui->SetPinDirection(PORT0); // port 0
    PtrIOExpanderXRA1201Gui->SetPinDirection(PORT1); // port 1

#endif

#ifdef SIGMA_BD_CPU
		hSPI1_CH0 = CreateFile( lpSPI1Name,  GENERIC_READ | GENERIC_WRITE, 0, 0, OPEN_EXISTING, 0, 0);	
		if(hSPI1_CH0 == INVALID_HANDLE_VALUE)
		{
			DWORD error=GetLastError();
			RETAILMSG(TRUE, (L"CreateFile %s failed, error %d\n", lpSPI1Name, error));
			return;
		}
		if(!DeviceIoControl(hSPI1_CH0, IOCTL_SPI_CONFIGURE, (LPVOID)&spi1_CH0_ADC_config, sizeof(IOCTL_SPI_CONFIGURE_IN), NULL, 0, NULL, NULL))
		{
			DWORD error=GetLastError();
			RETAILMSG(TRUE, (L"Can't configure 'SPI1:' Channel 0\r\n"));
			return;
		}
		SpiFlash.OpenDevice();

		SetDirection((DigitalBiDirectional_t)GPIO_SPI1_ON_BD,   GPIO_OUTPUT);
		Write(GPIO_SPI1_ON_BD,  SPI1_ENABLE); // 111 low
		SetDirection((DigitalBiDirectional_t)GPIO_SPI1_OFF_BD,   GPIO_OUTPUT);
		Write(GPIO_SPI1_OFF_BD,  SPI1_DISABLE); // 141 high
		SetDirection((DigitalBiDirectional_t)GPIO_SPI1_ON_BD_ENABLED,   GPIO_OUTPUT);
		Write(GPIO_SPI1_ON_BD_ENABLED,  SPI1_DISABLE);  // Set 110 high for good

		SetDirection((DigitalBiDirectional_t)GPIO_17, GPIO_OUTPUT);
		Write(GPIO_17,  DOUT_LOW);

		SetDirection((DigitalBiDirectional_t)DOUT_P1_SOLENOID,   GPIO_OUTPUT);
		Write(DOUT_P1_SOLENOID,  DOUT_LOW);
		SetDirection((DigitalBiDirectional_t)DOUT_P2_SOLENOID,   GPIO_OUTPUT);
		Write(DOUT_P2_SOLENOID,  DOUT_LOW);
		SetDirection((DigitalBiDirectional_t)DOUT_SAFETY_SOLENOID,   GPIO_OUTPUT);
		Write(DOUT_SAFETY_SOLENOID,  DOUT_HIGH);
		SetDirection((DigitalBiDirectional_t)DOUT_CROSSOVER_SOLENOID,   GPIO_OUTPUT);
		Write(DOUT_CROSSOVER_SOLENOID,  DOUT_LOW);

		SetDirection((DigitalBiDirectional_t)DOUT_MNFLD_SEL,   GPIO_OUTPUT);
		Write(DOUT_MNFLD_SEL,  DOUT_LOW);
		uint16_t DummyValue;	// Dummy reading to sync ADC
		Read(ADC_FIRST_CHANNEL, &DummyValue);
		Read((AnalogInputs_t)(ADC_FIRST_CHANNEL + 8), &DummyValue);
		hSPI3_CH1 = CreateFile( lpSPI3Name,  GENERIC_READ | GENERIC_WRITE, 0, 0, OPEN_EXISTING, 0, 0);	
		SpiFlash.OpenDevice();
		//    	DeviceIoControl(hAioDioDriver, IOCTL_AIODIO_INIT_HARDWARE, NULL, 0, NULL, 0, NULL, NULL);

		SetDirection((DigitalBiDirectional_t)DOUT_FAN_ON, GPIO_OUTPUT);
		SetDirection((DigitalBiDirectional_t)DIN_FAN_ERR, GPIO_INPUT);
		SetDirection((DigitalBiDirectional_t)DIN_PB_OUT, GPIO_INPUT);
		SetDirection((DigitalBiDirectional_t)DOUT_KILL_PWR, GPIO_OUTPUT);
		SetDirection((DigitalBiDirectional_t)DOUT_LED5,   GPIO_OUTPUT);
		Write(DOUT_LED5,  DOUT_LOW);

		//E600 - set IE PHASE OUT signal as output
		SetDirection((DigitalBiDirectional_t)DOUT_IE_PHASE_OUT,   GPIO_OUTPUT);
		Write(DOUT_IE_PHASE_OUT,  DOUT_LOW);

		////////////////////////////////////////////////////////////////////////
		// TODO E600_LL: hack Service Mode button here
#define TPS_GPIO_2_PB1		2//(256+2)		// not working when using this pin
#define TPS_GPIO_15_PB2		15//(256+15)
		
		HANDLE hGpioTPS = NULL;
		hGpioTPS = CreateFile( L"GIO2:",  GENERIC_READ | GENERIC_WRITE, 0, 0, OPEN_EXISTING, 0, 0);
		if(!hGpioTPS)
		{
			DEBUGMSG(1, (L"Failed creating GIO2 handle\r\n"));
		}
		else
		{
			DWORD GpioState, dwResult;
			DWORD GpioPin = TPS_GPIO_15_PB2;		// working

			dwResult = DeviceIoControl(hGpioTPS, IOCTL_GPIO_GETBIT, &GpioPin, sizeof(DWORD),&GpioState, sizeof(DWORD), NULL, NULL);
			if(dwResult && (GpioState == 0))
			{
				bCalibrationRequest = true;
			}
			CloseHandle(hGpioTPS);
		}
		////////////////////////////////////////////////////////////////////////

#endif	// if SIGMA_BD_CPU
		AioDioInitDone = true; 
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//TODO E600 need to investigate if this code is required 
//#ifdef SIGMA_GUI_CPU
//
//		/*//while(true) 
//		{
//		WORD SendData = 0; 
//		WORD ReadData = 0;
//		BYTE READ_COMMAND = 0x00;
//		BYTE WRITE_COMMAND = 0x80;
//		WORD Address = (0x1700); 
//		for(int j =0; j < 32; j++)
//		{
//		SendData = (WRITE_COMMAND << 8) | Address  | 0xA5;
//		DeviceIoControl(hSPI3_CH0, IOCTL_SPI_WRITEREAD, &ReadData, sizeof(WORD), &SendData, sizeof(WORD), NULL, NULL);
//		RETAILMSG(1, (TEXT("ReadSpiToUART W %x R %x\r\n"), SendData, ReadData));
//		Sleep(20);
//		SendData = (READ_COMMAND << 8)| Address;
//		DeviceIoControl(hSPI3_CH0, IOCTL_SPI_WRITEREAD, &ReadData, sizeof(WORD), &SendData, sizeof(WORD), NULL, NULL);
//		RETAILMSG(1, (TEXT("ReadSpiToUART W %x R %x\r\n"), SendData, ReadData));
//
//		}
//		for(int j =0; j < 32; j++)
//		{
//		SendData = (READ_COMMAND << 8) | 0x5A | Address;
//		SendSpiToUART(&SendData, &ReadData);
//		Sleep(20);
//		}
//		}
//		*/
//#if !defined(SIGMA_GUIPC_CPU)        
//		InitSPI_UART();
//#endif
//		SetDirection((DigitalBiDirectional_t)DOUT_AUDIO_ENABLE,   GPIO_OUTPUT);
//		Write(DOUT_AUDIO_ENABLE,  AUDIO_AMPLIFIER_ENABLED);
//
//#endif	// if SIGMA_GUI_CPU
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		Sleep(20);
	}
}

//#############################################################################
//
//  AioDioDriver_t::GetAioDioDriverHandle - Return the global AioDioDriver_t
//      object reference. 
//
//#############################################################################

AioDioDriver_t& AioDioDriver_t::GetAioDioDriverHandle()
{
	return AioDioDriver;
}

#if defined(SIGMA_GUI_CPU)  && !defined(SIGMA_GUIPC_CPU)
//#############################################################################
//
//  AioDioDriver_t::GetIOExpanderXRA1201Handle - Return the IOExpanderXRA1201_t
//      object reference. 
//
//#############################################################################

IOExpanderXRA1201_t& AioDioDriver_t::GetIOExpanderXRA1201Handle()
{
	return *PtrIOExpanderXRA1201Gui;
}



//#############################################################################
//
//  ReadButtons - This function is periodicaly called to read membrane buttons
//
//#############################################################################

BYTE AioDioDriver_t::ReadMembraneButtons(void)
{ 
	BYTE Port=0;
	if(PtrIOExpanderXRA1201Gui->ReadPort0Status(&Port))
	{
		return Port;
	} 
	else
	{
		SAFE_CLASS_ASSERTION_FAILURE();
	}
}

#endif


