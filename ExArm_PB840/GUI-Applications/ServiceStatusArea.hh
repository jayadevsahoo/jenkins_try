#ifndef ServiceStatusArea_HH
#define ServiceStatusArea_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: ServiceStatusArea - The container which containing the test
// running status messages. This class is part of the GUI-Apps Framework
// cluster.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/ServiceStatusArea.hhv   25.0.4.0   19 Nov 2013 14:08:26   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "Container.hh"

//@ Usage-Classes
#include "TextField.hh"
#include "GuiAppClassIds.hh"
//@ End-Usage

class ServiceStatusArea : public Container
{
public:
	ServiceStatusArea(Int containerX, Int containerY,
					Int containerWidth, Int containerHeight,
					StringId serviceStatusLabelId,
					StringId serviceStatusValueId);
	~ServiceStatusArea(void);

	void setStatus(StringId stringId);
	
	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:

private:
	// these methods are purposely declared, but not implemented...
	ServiceStatusArea(const ServiceStatusArea&);		// not implemented...
	void   operator=(const ServiceStatusArea&);	// not implemented...

	void updateArea_(void);				// Formats UI of Prompt Area

	//@ Data-Member: serviceStatusLabel_
	// A text field for displaying the label of the current SERVICE status.
	TextField serviceStatusLabel_;

	//@ Data-Member: serviceStatusValue_
	// A text field for displaying the status of the current SERVICE status.
	TextField serviceStatusValue_;

	//@ Data-Member: serviceStatusLabelId_
	// A string ids of the prompt.
	StringId serviceStatusLabelId_;

	//@ Data-Member: serviceStatusValueId_
	// A string ids of the prompt.
	StringId serviceStatusValueId_;

	//@ Data-Member: containerX_
	// The X co-ordinate of the SERVICE Status Area within the Lower
	// Screen.
	Int containerX_;
	
	//@ Data-Member: containerY_
	// The Y co-ordinate of the SERVICE Status Area within the Lower
	// Screen.
	Int containerY_;
	
	//@ Data-Member: containerWidth_
	// The width of the SERVICE Status Area.
	Int containerWidth_;
	
	//@ Data-Member: containerHeight_
	// The height of the SERVICE Status Area.
	Int containerHeight_;
	
	//@ Data-Member: ServiceStatusLabelX_
	// The X coordinate of the SERVICE_STATUS_LABEL value field.
	Int ServiceStatusLabelX_;
	
	//@ Data-Member: ServiceStatusY_
	// The Y coordinate of the SERVICE_STATUS field.
	Int ServiceStatusY_;
	
	//@ Constant: SERVICE_STATUS_MSG_X_PAD_
	// The PAD pixel from the right edge of the status container area.
	static const Int SERVICE_STATUS_MSG_X_PAD_;
	
};


#endif // ServiceStatusArea_HH 
