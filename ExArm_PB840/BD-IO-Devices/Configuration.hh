#ifndef Configuration_HH
#define Configuration_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: Configuration - storage for ventilator specific information.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/Configuration.hhv   25.0.4.0   19 Nov 2013 13:54:02   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 001  By:  syw    Date: 08-Nov-1996    DR Number: INITIAL RELEASE
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//====================================================================

#include "Sigma.hh"
#include "BD_IO_Devices.hh"

//@ Usage-Classes

#include "CrcUtilities.hh"
#include "CalInfoFlashSignature.hh"
#include "BDIOFlashMap.hh"
#include "SerialNumber.hh"

//@ End-Usage

class Configuration {
  public:
    Configuration( BDIOFlashMap *pFlash) ;

    static void SoftFault( const SoftFaultID	softFaultID,
						   const Uint32		lineNumber,
						   const char		*pFileName  = NULL,
						   const char		*pPredicate = NULL) ;

  	inline const SerialNumber & getFlashSerialNumber( void) ;

	inline CalInfoFlashSignature::Signature getExhValveSignature( void)  ;
	inline CrcValue getExhValveCrc( void)  ;

	inline Uint32 getAirFlowSensorSN( void)  ;
	inline CrcValue getAirFlowSensorCrc( void)  ;

	inline Uint32 getO2FlowSensorSN( void)  ;
	inline CrcValue getO2FlowSensorCrc( void)  ;

	inline Uint32 getExhFlowSensorSN( void)  ;
	inline CrcValue getExhFlowSensorCrc( void)  ;

	inline Uint32 getExhO2FlowSensorSN( void)  ;
	inline CrcValue getExhO2FlowSensorCrc( void)  ;

#ifdef SIGMA_DEBUG

	void dumpDataMembers( void) ;

#endif // SIGMA_DEBUG

  protected:

  private:
    Configuration( void) ;						// not implemented...
    ~Configuration( void) ;						// not implemented...
    void operator=( const Configuration&) ;		// not implemented...
    Configuration( const Configuration&) ;		// not implemented...

	//@ Data-Member: *pFlash_
	// pointer to flash memory
	BDIOFlashMap *pFlash_ ;

    //@ Data-Member: flashSerialNumber_
    // serial number stored in flash
    SerialNumber flashSerialNumber_ ;

	//@ Data-Member:::Signature exhValveSignature_
	// exh valve signature
	CalInfoFlashSignature::Signature exhValveSignature_ ;

	//@ Data-Member: exhValveCrc_
	//exh valve Crc value
	CrcValue exhValveCrc_ ;

	//@ Data-Member: airFlowSensorSN_
	// air serial number
	Uint32 airFlowSensorSN_ ;

	//@ Data-Member: airFlowSensorCrc_
	// air flow sensor Crc value
	CrcValue airFlowSensorCrc_ ;

	//@ Data-Member: o2FlowSensorSN_
	// o2 serial number
	Uint32 o2FlowSensorSN_ ;

	//@ Data-Member: o2FlowSensorCrc_
	// o2 flow sensor Crc value
	CrcValue o2FlowSensorCrc_ ;

	//@ Data-Member: exhFlowSensorSN_
	// exh serial number
	Uint32 exhFlowSensorSN_ ;

	//@ Data-Member: exhFlowSensorCrc_
	// exh flow sensor Crc value
	CrcValue exhFlowSensorCrc_ ;


} ;

#include "Configuration.in"

#endif // Configuration_HH


