#ifndef Area_IN
#define Area_IN
//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1995, Puritan-Bennett Corporation
//====================================================================

//=====================================================================
// Class: Area - This class represents a rectangle.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Foundations/vcssrc/Area.inv   25.0.4.0   19 Nov 2013 14:11:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002   By: sah    Date: 11-Jan-1999   DR Number: 5321
//  Project:  Sigma (R8027)
//  Description:
//      Changed 'SoftFault()' method to non-inline method.
//
//  Revision: 001  By: yyy      Date: 01-Jan-1995  DR Number: NONE
//    Project:  Sigma (R8027)
//    Description:
//	Integration baseline.
//=====================================================================



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setX1
//
//@ Interface-Description
// Sets the x coordinate of the left edge of the area to the passed value.
// This method does not change the x position of the right edge; therefore, the
// width of the drawable may be changed.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply sets the private x1_ to the passed value.
//---------------------------------------------------------------------
//@ PreCondition
// The given "x1" value must be "to the left" of the current x2_ value.
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
inline void
Area::setX1(Int x1)
{
	CALL_TRACE("Area::setX1(Int)");
	CLASS_PRE_CONDITION(x1 <= x2_);
	//			$[TI1]
	x1_ = x1;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setY1
//
//@ Interface-Description
// Sets the y coordinate of the upper edge of the area to the passed value.
// This method does not change the y position of the lower edge; therefore, the
// height of the drawable may be changed.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply sets the private y1_ to the passed value.
//---------------------------------------------------------------------
//@ PreCondition
// The given "y1" value must be "above" the current y2_ value.
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
inline void
Area::setY1(Int y1)
{
	CALL_TRACE("Area::setY1(Int)");
	CLASS_PRE_CONDITION(y1 <= y2_);
	//			$[TI1]
	y1_ = y1;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setX2
//
//@ Interface-Description
// Sets the x coordinate of the right edge of the area to the passed value.
// This method does not change the x position of the left edge; therefore, the
// width of the drawable may be changed.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply sets the private x2_ to the passed value.
//---------------------------------------------------------------------
//@ PreCondition
// The given "x2" value must be "to the right" of the current x1_ value.
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
inline void
Area::setX2(Int x2)
{
	CALL_TRACE("Area::setX2(Int)");
	CLASS_PRE_CONDITION(x2 >= x1_);
	//			$[TI1]
	x2_ = x2;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setY2
//
//@ Interface-Description
// Sets the y coordinate of the lower edge of the area to the passed value.
// This method does not change the y position of the upper edge; therefore, the
// height of the drawable may be changed.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply sets the private y2_ to the passed value.
//---------------------------------------------------------------------
//@ PreCondition
// The given "y2" value must be "below" the current y1_ value.
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
inline void
Area::setY2(Int y2)
{
	CALL_TRACE("Area::setY2(Int)");
	CLASS_PRE_CONDITION(y2 >= y1_);
	//			$[TI1]
	y2_ = y2;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getX1
//
//@ Interface-Description
// Returns x coordinate of left edge of the area.
//---------------------------------------------------------------------
//@ Implementation-Description
// Returns the private x1_ coordinate.
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
inline Int
Area::getX1(void) const
{
	CALL_TRACE("Area::getX1(void)");
	//			$[TI1]
	return x1_;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getY1
//
//@ Interface-Description
// Returns y coordinate of upper edge of the area.
//---------------------------------------------------------------------
//@ Implementation-Description
// Returns the private y1_ coordinate.
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
inline Int
Area::getY1(void) const
{
	CALL_TRACE("Area::getY1(void)");
	//			$[TI1]
	return y1_;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getX2
//
//@ Interface-Description
// Returns x coordinate of right edge of the area.
//---------------------------------------------------------------------
//@ Implementation-Description
// Returns the private x2_ coordinate.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
inline Int
Area::getX2(void) const
{
	CALL_TRACE("Area::getX2(void)");
	//			$[TI1]
	return x2_;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getY2
//
//@ Interface-Description
// Returns y coordinate of lower edge of the area.
//---------------------------------------------------------------------
//@ Implementation-Description
// Returns the private y2_ coordinate.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
inline Int
Area::getY2(void) const
{
	CALL_TRACE("Area::getY2(void)");
	//			$[TI1]
	return y2_;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getWidth
//
//@ Interface-Description
// Returns the area width.  This is the number of pixel columns across.
//---------------------------------------------------------------------
//@ Implementation-Description
// The width is defined to be the difference between the right hand edge
// and the left hand edge, plus 1.  Note that this function depends on the
// invariant that x2 >= x1.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
inline Int
Area::getWidth(void) const
{
	CALL_TRACE("Area::getWidth(void)");
	SAFE_CLASS_ASSERTION(x2_ >= x1_);
	//			$[TI1]
	return x2_ - x1_ + 1;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getHeight
//
//@ Interface-Description
// Returns the area height. This is the number of rows of pixels.
//---------------------------------------------------------------------
//@ Implementation-Description
// The height is defined to be the difference between the upper edge
// and the lower edge, plus 1.  Note that this function depends on the
// invariant that y2 >= y1.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
inline Int
Area::getHeight(void) const
{
	CALL_TRACE("Area::getHeight(void)");
	SAFE_CLASS_ASSERTION(y2_ >= y1_);
	//			$[TI1]
	return y2_ - y1_ + 1;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setWidth
//
//@ Interface-Description
// Sets the width of the area to the given "width" by adjusting the
// right edge of the area, leaving the position of the left edge
// unchanged.
//---------------------------------------------------------------------
//@ Implementation-Description
// Change the value of x2_ to x1_ + the passed width - 1.
//---------------------------------------------------------------------
//@ PreCondition
// The given "width" must be > 0.
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
inline void
Area::setWidth(Int width)
{
	CALL_TRACE("Area::setWidth(Int)");
	CLASS_PRE_CONDITION(width > 0);
	//			$[TI1]
	x2_ = x1_ + width - 1;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setHeight
//
//@ Interface-Description
// Sets the height of the area to the given "Height" by adjusting the
// bottom edge of the area, leaving the position of the top edge
// unchanged.
//---------------------------------------------------------------------
//@ Implementation-Description
// Change the value of y2_ to y1_ + the passed height - 1.
//---------------------------------------------------------------------
//@ PreCondition
// The given "height" must be > 0.
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
inline void
Area::setHeight(Int height)
{
	CALL_TRACE("Area::setHeight(Int)");
	CLASS_PRE_CONDITION(height > 0);
	//			$[TI1]
	y2_ = y1_ + height - 1;
}


#endif // Area_IN 
