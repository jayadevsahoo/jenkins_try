
#ifndef SettingObserver_HH
#define SettingObserver_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  SettingObserver - Setting Observer Class.
//---------------------------------------------------------------------
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/SettingObserver.hhv   25.0.4.0   19 Nov 2013 14:27:42   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001   By: sah   Date:  26-Jan-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//     Created to work with new applicability functionality.
//
//====================================================================

#include "Notification.hh"

//@ Usage-Classes
#include "SettingsMgr.hh"
//@ End-Usage


class SettingObserver
{
  public:
    virtual ~SettingObserver(void);

    //-------------------------------------------------------------------
    // Dynamic Notification methods...
    //-------------------------------------------------------------------

    //@ Type: UpdateMethodPtr 
    // Pointer type to an update method of the SettingOberver class.
    typedef void (SettingObserver::* UpdateMethodPtr)(
					  const Notification::ChangeQualifier,
					  const SettingSubject*
						     );

    virtual void  applicabilityUpdate(
			      const Notification::ChangeQualifier qualifierId,
			      const SettingSubject*               pSubject
				     );
    virtual void  valueUpdate(const Notification::ChangeQualifier qualifierId,
			      const SettingSubject*               pSubject);

    virtual Boolean  doRetainAttachment(const SettingSubject* pSubject) const;


    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL, 
			   const char*       pPredicate = NULL);

  protected:
    SettingObserver(void);

    inline SettingSubject*  getSubjectPtr_(
				  const SettingId::SettingIdType subjectId
					  ) const;

    void  attachToSubject_  (const SettingId::SettingIdType      subjectId,
			     const Notification::SettingChangeId changeId);
    void  detachFromSubject_(const SettingId::SettingIdType      subjectId,
			     const Notification::SettingChangeId changeId) const;

    void activateSubject_(const SettingId::SettingIdType      subjectId,
			  const Notification::ChangeQualifier qualifierId);

  private:
    SettingObserver(const SettingObserver&);    // not implemented...
    void  operator=(const SettingObserver&);    // not implemented...
};


#include "SettingObserver.in"


#endif // SettingObserver_HH 
