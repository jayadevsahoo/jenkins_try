
#ifndef ContextSubject_HH
#define ContextSubject_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  ContextSubject - Context Subject Class.
//---------------------------------------------------------------------
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/ContextSubject.hhv   25.0.4.0   19 Nov 2013 14:27:20   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001   By: sah   Date:  26-Jan-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//     Created to work with new applicability functionality.
//
//====================================================================

#include "ContextId.hh"
#include "SettingId.hh"
#include "Notification.hh"

//@ Usage-Classes
#include "Subject.hh"
#include "SettingValue.hh"

class ContextObserver;  // forward declaration...
//@ End-Usage


class ContextSubject : public Subject
{
  public:
    virtual ~ContextSubject(void);

    //-------------------------------------------------------------------
    // Monitoring methods...
    //-------------------------------------------------------------------

    // attach/detach observer for monitoring the indicated type of change
    // for this context...
    void  attachObserver(ContextObserver*                    pObserver,
			 const Notification::ContextChangeId changeId);
    void  detachObserver(const ContextObserver*              pObserver,
			 const Notification::ContextChangeId changeId);

    // notify all of this context's observers who monitor this type of change
    // of a change...
    void  notifyAllObservers(
			  const Notification::ChangeQualifier qualifierId,
			  const Notification::ContextChangeId changeId,
			  const SettingId::SettingIdType      settingId
			    ) const;


    //-------------------------------------------------------------------
    // Query methods...
    //-------------------------------------------------------------------

    inline ContextId::ContextIdType  getId(void) const;

    virtual Boolean  isSettingChanged(
			      const SettingId::SettingIdType settingId
				     ) const = 0;

    virtual Boolean  areAnyBatchSettingsChanged(void) const = 0;

    virtual SettingValue  getSettingValue(
			      const SettingId::SettingIdType settingId
					 ) const = 0;


    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL, 
			   const char*       pPredicate = NULL);

  protected:
    ContextSubject(const ContextId::ContextIdType settingId);

    virtual void  detachFromAllObservers_(void);

  private:
    ContextSubject(const ContextSubject&);     // not implemented...
    ContextSubject(void);                      // not implemented...
    void  operator=(const ContextSubject&);    // not implemented...

    void  initObserverTable_(void);

    const ContextId::ContextIdType  CONTEXT_ID_;

    enum { MAX_OBSERVERS_ = 20 };

    ContextObserver*  observerTable_[Notification::NUM_CONTEXT_CHANGE_TYPES]
				    [ContextSubject::MAX_OBSERVERS_];
};


#include "ContextSubject.in"


#endif // ContextSubject_HH
 
