#pragma once
#include "afxcmn.h"
#include "ColorListBox.h"

// CTabStatusDlg dialog

class CTabStatusDlg : public CDialog
{
	DECLARE_DYNAMIC(CTabStatusDlg)

public:
	CTabStatusDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CTabStatusDlg();

// Dialog Data
	enum { IDD = IDD_TAB_STATUS_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:


	void UpdateTestStatus(CString &strStatus, int nMsgType);
	void ClearTestStatus ( );

	virtual BOOL OnInitDialog();
	bool m_bFreeze;
	afx_msg void OnBnClickedCheckFreeze();
	afx_msg void OnBnClickedExpand();
	CColorListBox m_lbColor;
	afx_msg void OnLbnDblclkListBox();


	afx_msg void OnBnClickedButtonCopy();
	afx_msg void OnStatusCopyselected();

	void OnContextMenu(CWnd* pWnd, CPoint pt);

	afx_msg void OnBnClickedButtonStatusClear();
	
	afx_msg void OnBnClickedButtonFind();
	


	afx_msg void OnEnChangeEditFind();
	afx_msg void OnBnClickedButtonFindUp();
	afx_msg void OnBnClickedButtonCpyID();
	afx_msg void OnBnClickedButtunOpen();
};
