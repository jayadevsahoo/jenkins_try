//-------------------------------------------------------------------------------
//                        Copyright (c) 2010 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//-------------------------------------------------------------------------------
#ifndef SWATMESSAGE_H
#define SWATMESSAGE_H

#include "uuid.h"

namespace swat
{

    class SwatMessage
    {
        public:
            static const uuid_t EXECUTESCRIPT;
            static const uuid_t LOADBUFFER;
            static const uuid_t ENABLESWAT;
            static const uuid_t DISABLESWAT;
    };

}
#endif

