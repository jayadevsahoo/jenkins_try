//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file CParameter.h
//----------------------------------------------------------------------------


//-------------------------------------
// I N C L U D E S
//-------------------------------------
#ifndef CPARAMETER_H
#define CPARAMETER_H

#include<xercesc/dom/DOMNode.hpp>
#include<map>
#include<string>
#include<memory>
#include <boost/shared_ptr.hpp>
#include<cstring>

//-------------------------------------
// C L A S S  D E C L
//-------------------------------------

/// This class represents a parameter value that can be changed. 
///
/// A parameter value contains a name value pair representation of data and
/// is used to associate a parameter with an id, integer representing a 
/// given parameter.
class CParameter{

    private:

    /// A std::map containing all the attributes that a parameter contains.
    std::map<std::string, std::string>  m_ParameterAttributeMap;

	/// A std::string containing the name of the parameter.
    std::string m_ParameterNameString;

	/// Unique id of parameter.
    unsigned int m_iId;

	/// Unique xmit id of parameter.
    unsigned int m_iXid;

	/// Used to indicate the type of data that this parameter contains.
	///
	/// Type of parameter: 
	/// - CParameter::FLOAT
	/// - CParameter::INT
	/// - CParameter::ENUM
	/// - CParameter::UNDEF
    unsigned int m_iType;

	/// Used to indicate if max is set.
    bool m_bMax;

	/// Used to indicate if min is set.
    bool m_bMin;

	/// Used to indicate if a value is set.
	/// 
	/// If a value is set it is of type CParameter::m_iType. 
	/// and is stored in one of the varables CParameter::m_fVal, 
	/// CParameter::m_iVal, or CParameter::m_iEnumVal. 
    bool m_bVal;

    union {
        float m_fVal;     ///< Float value, used if CParameter::FLOAT
        int   m_iVal;     ///< Integer value, used if CParameter::INT
        int   m_iEnumVal; ///< Enum value, also an index into CParameter::m_pEnumMap 
    } ;  

    union{
        float m_fMax;     ///< Float value if CParameter::m_iType is equal to CParameter::Float
        int   m_iMax;     ///< Integer value if CParameter::m_iType is equal to CParameter::Int
    };  

    union{
        float m_fMin;     ///< Float value if CParameter::m_iType is equal to CParameter::Float 
        int   m_iMin;     ///< Float value if CParameter::m_iType is equal to CParameter::Int
    };  


	/// A map containing all the name value pairs of 
    std::map<std::string, int> *m_pEnumMap;

    public:

	///@{
	/// Constant used to define that value in which CParameter::m_iType can be set to. 
    static const unsigned int FLOAT=0;
    static const unsigned int INT  =1;
    static const unsigned int ENUM =2;
    static const unsigned int UNDEF=3;
	///@}


    /// Constructor used to fill in data to CParameter.
	/// 
	/// @param ptr  A pointer to a DOMNode.
    CParameter(const XERCES_CPP_NAMESPACE::DOMNode *ptr);

	/// Default constructor.
    CParameter();

	/// Constructor.
	/// 
	/// @param pIn A pointer to a char array containing the name of the parameter.  This
	///            constructor is only used to find parameters in a set.
    CParameter(const char *pIn):m_ParameterNameString(pIn){}


	/// Destructor
    ~CParameter();


	/// Get the name of the parameter.
	///  
	/// @return A pointer to a character string containing the name of the parameter; 
	///         Otherwise return NULL if parameter does not have a valid name. 
    const char *getName() const;


	/// Set the name of the class.  
	/// 
	/// @param pszIn Pointer to a character string containing the name to set the class to true.
	/// 
	/// @return true on success, otherwise false.
    const bool setName(const char *pszIn);


	///@{
	/// Getters used to get the id of a type or the name. 
    const char *getTypeName() const;
    const int  getTypeId() const;
	///@}


	///@{
	/// Setters used to set CParameter::m_iType. 
	/// @return a bool true if able to resolve type and set it, otherwise false.
    const bool setType(const char *iIn);
    const bool setType(const unsigned int iIn);
	///@}

    /*Attribute*/

	/// Get the attribute of parameter by name.
	/// 
	/// @param name A string containing the name of a parameter.
	/// 
	/// @return A pointer to a string containing the attribute value if attribute is present, otherwise false.
    const char *getAttribute(const char *name) const;


	///@{
    /// Attributes can be added or updated.  
	/// 
	/// @param pszName The name of a attribute to set.
	/// @param pszValue The value to set the attribute to.
	/// 
	/// @return

    const bool addAttribute(const char *pszName, const char *pszValue);         ///< Return false if attribute is 
	                                                                            ///  already added, otherwise return true.

    const bool addOrUpdateAttribute(const char *pszName, const char *pszValue); ///< Always return true, action 
	                                                                            ///< should never fail.

    const bool updateAttribute(const char *pszName, const char *pszValue);      ///< Return true only if attribute already
	                                                                            ///< is present.

	///@}

    /*Enum*/

    /// Get the value of an enum by name.
	/// 
	/// @param[in] pszEnumName A character string containing the name of a 
	///                        defined enum constant. 
	///
	/// @param[out] piOut      A pointer to where the result of the getter
	///                        be stored.
	/// 
	/// @return true if able to get the integer value of an enum.

    bool getEnum(const char *pszEnumName, int *piOut) const;


	/// Get the name of an enum by value. 
	///
	/// @param iIn  Integer Value of an enum,
	/// 
	/// @return A char pointer containing the name of the enum constant if iIn is valid, 
	///         otherwise false is returned.
    const char * getEnumName(const int iIn) const;



	/// Add enum name value pair.
	/// 
	/// @param pszName A character string containing the name of the enum constant.
	/// @param pszValue A character string containing a string integer value.  
	/// 
	/// @return true if able to set the enum.
	///
    const bool addEnum(const char *pszName, const char *pszValue);



	/// Add an enum by DOMNode. 
	///
	/// @param ptr A pointer to a DOMNode  containing an enum.
	/// 
	/// @return true if able to add an enum.
    const bool addEnum(const XERCES_CPP_NAMESPACE::DOMNode *ptr);

	/// Get the id of the parameter.
	/// 
	/// @return An integer containing the id of the parameter.
    const int getId() const;

    const int getXid() const; 

	/// Get the max integer value that the parameter can be set to. 
	///
	/// @param piMaxInt A pointer to the result.
	/// 
	/// @return true if max int is set.
    const bool getMaxInt(int *piMaxInt) const;


	/// Get the min integer value that the parameter can be set to. 
	/// @param piMinInt A pointer to the result.
	/// 
	/// @return true if min int is set.
    const bool getMinInt(int *piMinInt) const;


    /// Get the of the parameter.
    /// 
    /// @param piDefaultInt A pointer to an int.
    /// 
    /// @return true iff there is a float value defined.
    ///
    const bool getDefaultInt(int *piDefaultInt) const;

    /// Get the max value that a parameter can have.
    /// 
    /// @param piMaxInt A pointer to a float.
    /// 
    /// @return true iff there is a float value defined.
    ///
    const bool getMaxFloat(float *piMaxInt) const;

    /// Get the min value that a parameter can have.
    /// 
    /// @param piMinInt A pointer to a float.
    /// 
    /// @return true iff there is a float value defined.
    ///
    const bool getMinFloat(float *piMinInt) const;

    /// Get value of the parameter.
    /// 
    /// @param piDefaultInt A pointer to a float.
    /// 
    /// @return true iff there is a float value defined.
    ///
    const bool getDefaultFloat(float *piDefaultInt) const;




	/// Less than operator, created for stl usage.
    bool operator <(CParameter &lhs){
        return strcmp(m_ParameterNameString.c_str(), lhs.m_ParameterNameString.c_str())<0;
    }

	/// Less than operator, created for stl usage.
    bool operator <(char *lhs){
        return strcmp(m_ParameterNameString.c_str(), lhs)<0;
    }


};

#endif
