#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: SubScreenTitleArea - Used for displaying the title of a
// subscreen with the title surrounded by a bounding box.
//---------------------------------------------------------------------
//@ Interface-Description
// This class (derived from GUI-Foundation's Container class) is used to
// display the title of a subscreen in a standard way.  Most subscreens have
// titles and they are usually displayed in the upper left-hand corner of the
// subscreen, in white, surrounded by a white border.  Some subscreens have
// similar titles displayed in the upper right-hand corner of the subscreen.
// Both formats are handled by SubScreenTitleArea.
//
// A SubScreenTitleArea object is constructed by passing it one or two
// arguments, the first specifying the string id of the title to display
// and the optional second argument specifying left or right justification
// (left is the default).
//
// Some screen titles need to change while the subscreen is being
// displayed so there is a setTitle() method provided for replacing
// the current title.  The border of the title expands or shrinks so as
// to encompass the new title.
//
// In the case of VentSetupSubScreen, this particular class needs more than
// just a title displayed in the title area, it needs to display a separate IBW
// value (while in its New Patient Setup mode).  Extra drawables can simply be
// added to the titleArea using the addDrawable() method and can subsequently
// all be removed in a single call to clear().
//---------------------------------------------------------------------
//@ Rationale
// Many subscreens have a title area that needs to have a resizable
// border that surrounds a possibly changing title.  This class was
// created to implement that functionality.
//---------------------------------------------------------------------
//@ Implementation-Description
// The class is simply a Container, containing one piece of text and two
// lines.  The text displays the title and the lines are used to border
// the text, one line goes below the title and the other to the side
// (the border of the SubScreenArea automatically forms the border of the
// title on the other two sides).
//
// Any change to the title area (either via a call to setTitle(), addDrawable()
// or clear() and also on construction) generates a call to updateArea_().
// This method looks after the resizing of the border so that it surrounds all
// the drawables in the title area by a certain amount.
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and pre-conditions
// to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
// Can only be used to display a title at the top left or top right of a
// subscreen.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/SubScreenTitleArea.ccv   25.0.4.0   19 Nov 2013 14:08:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By:  gdc	   Date:  28-May-2007    SCR Number:  6237
//  Project:  Trend
//  Description:
// 		Refined text format strings.
//
//  Revision: 003  By:  hhd	   Date:  27-Apr-1999    DCS Number:  5322
//  Project:  ATC
//  Description:
// 		Initial version.
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "SubScreenTitleArea.hh"

//@ Usage-Classes
#include "SIterR_Drawable.hh"
//@ End-Usage

//@ Code...

// Initialize static constants.
static const Int32 LINE_WIDTH_ = 1;
static const Int32 TITLE_AREA_HEIGHT_ = 31 + LINE_WIDTH_;   // best for 18 point font
static const Int32 TITLE_X_OFFSET_ = 6;


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SubScreenTitleArea()  [Constructor]
//
//@ Interface-Description
// Creates a SubScreenTitleArea object.  Passed the following parameters:
// >Von
//	title			The string id of the title to display
//	justification	The justification, either SSTA_LEFT or SSTA_RIGHT, which
//					determines on which side of the subscreen the title is
//					displayed. This parameter does not need to be passed.  If
//					it isn't then it defaults to SSTA_LEFT.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply adds the components of the title area (TextField & 2 Line's) to
// the container and calls updateArea_() to size the area correctly.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SubScreenTitleArea::SubScreenTitleArea(StringId title,
									   Justification justification) :
		titleText_(title),
		bottomLine_(0, 0, 0, 0, LINE_WIDTH_),
		justification_(justification)
{
	CALL_TRACE("SubScreenTitleArea::SubScreenTitleArea(StringId title, "
										"Justification justification)");

	// Color and add title and border lines
	setContentsColor(Colors::WHITE);
	setFillColor(Colors::EXTRA_DARK_BLUE);
	addDrawable(&titleText_);
	addDrawable(&bottomLine_);
	updateArea_();										// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~SubScreenTitleArea  [Destructor]
//
//@ Interface-Description
// Destroys a SubScreenTitleArea.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SubScreenTitleArea::~SubScreenTitleArea(void)
{
	CALL_TRACE("SubScreenTitleArea::~SubScreenTitleArea(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setTitle
//
//@ Interface-Description
// Changes the displayed title.  Passed the string id of the new title.
//---------------------------------------------------------------------
//@ Implementation-Description
// Called the setText() method of the title text and call updateArea_()
// to do any resizing of the border.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SubScreenTitleArea::setTitle(StringId title)
{
	CALL_TRACE("SubScreenTitleArea::setTitle(StringId title)");

	titleText_.setText(title);
	updateArea_();										// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: addDrawable
//
//@ Interface-Description
// Adds a new drawable to the title area.  Passed a pointer to the drawable
// object.
//---------------------------------------------------------------------
//@ Implementation-Description
// Call the corresponding Container::addDrawable() method to append the
// drawable and then call updateArea_() to do any resizing of the border.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SubScreenTitleArea::addDrawable(Drawable* pDrawable)
{
	CALL_TRACE("SubScreenTitleArea::addDrawable(Drawable* pDrawable)");

	Container::addDrawable(pDrawable);
	updateArea_();										// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: clear
//
//@ Interface-Description
// Empty the area of all drawables aside from the title and the
// bottom and side lines (the text of the title is cleared).
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply go through the list of items in the container and remove them
// all.  Then go back and re-add the title text (clear its contents) and
// two border lines. Call updateArea_() to do any resizing of the border.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SubScreenTitleArea::clear(void)
{
	CALL_TRACE("SubScreenTitleArea::clear(void)");

	// Empty the container of all objects.  We do this by continually
	// going to the first element in the list and removing it from the container
	// until the list is empty.
	SIterR_Drawable children(*getChildren_());
	Drawable *pDraw;
	SigmaStatus ok;
  	for (ok = children.goFirst();
  					ok && (pDraw= (Drawable *) &(children.currentItem()));
		  			ok = children.goFirst())
	{													// $[TI1]
		removeDrawable(pDraw);
	}													// $[TI2]

	// Re-add the necessary graphics and clear the title.
	addDrawable(&titleText_);
	addDrawable(&bottomLine_);
	setTitle(NULL_STRING_ID);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateArea_
//
//@ Interface-Description
// Called when any part of the title area changes.  Resizes the border
// to wrap around the contained drawables.
//---------------------------------------------------------------------
//@ Implementation-Description
// We first position and size the border lines so that they occupy the
// pixel at the top left of the title area.  This allows us to find the
// area occupied by the other drawables in the title area by calling the
// sizeToFit() method of Container.  We can then reposition and size the
// lines so that they form a border a certain distance from the title
// area contents.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SubScreenTitleArea::updateArea_(void)
{
	CALL_TRACE("SubScreenTitleArea::updateArea_(void)");

	// Set the lines to occupy pixel at top left of area so that they
	// will not affect rubber-banding.
	bottomLine_.setStartPoint(0, 0);
	bottomLine_.setEndPoint(0, 0);
	titleText_.setX(0);

	// reset height for consistency from screen to screen
	setHeight(TITLE_AREA_HEIGHT_);

	bottomLine_.setStartPoint(0, getHeight() - 1);
	bottomLine_.setEndPoint(SUB_SCREEN_AREA_WIDTH, getHeight() - 1);

	switch ( justification_ )
	{
	case SSTA_LEFT:   // $[TI1]
	{
		titleText_.setX(TITLE_X_OFFSET_);
		setWidth(SUB_SCREEN_AREA_WIDTH);
		break;
	}
	case SSTA_RIGHT:  // $[TI2]
	{
		titleText_.setX(SUB_SCREEN_AREA_WIDTH - titleText_.getWidth() - TITLE_X_OFFSET_ );
		setWidth(SUB_SCREEN_AREA_WIDTH);
		break;
	}
	case SSTA_CENTER: // $[TI3]
	{
		titleText_.setX( ( SUB_SCREEN_AREA_WIDTH - titleText_.getWidth() ) / 2
				 - 1 );
		setWidth(SUB_SCREEN_AREA_WIDTH);
		break;
	}
	default:
		AUX_CLASS_ASSERTION_FAILURE( justification_ );
	}

	titleText_.positionInContainer(::GRAVITY_VERTICAL_CENTER);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
SubScreenTitleArea::SoftFault(const SoftFaultID  softFaultID,
		   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, SUBSCREENTITLEAREA,
									lineNumber, pFileName, pPredicate);
}
