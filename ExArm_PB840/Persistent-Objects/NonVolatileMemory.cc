#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: NonVolatileMemory - NOVRAM Memory Block.
//---------------------------------------------------------------------
//@ Interface-Description
//  This class contains (as non-static data members) each of the different
//  data items that are to reside in NOVRAM.  The one instance of this
//  class is located within NOVRAM so that all of these data items are
//  persistent.  By containing the items in this class, there is no need
//  to manage the locating of each of the individual data items -- the
//  whole instance is located at once.
//
//  This class provides a static method for retrieving the starting address
//  (in NOVRAM) that this class's one instance is located at.  It also
//  contains some other public, static methods for forwarding actions
//  on to other data members.
//---------------------------------------------------------------------
//@ Rationale
//  This class provides a simple means of adding/deleting data items
//  into/from NOVRAM management.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class has an initialization method (see 'initialize_()') which
//  calls the initialization method of each of its data items, returning
//  the aggregate result.  Similarly, this class has a verification method
//  ('verifySelf_()'), which calls the verification method of each of its
//  data items.  When data items are added/deleted, these two methods need
//  to be modified to add/delete the processing of the corresponding data
//  item.
//
//  $[00613] -- system fault information shall persist across power fail...
//  $[00617] -- comm. fault information shall persist across power fail...
//  $[00645] -- EST/SST fault information shall persist across power fail...
//  $[00610] -- cct. compilance info. shall persist across power fail...
//  $[00611] -- FiO2 gain info. shall persist across power fail...
//  $[00612] -- current settings shall persist across power fail...
//  $[00618] -- EST/SST test results shall persist across power fail...
//  $[00621] -- GUI serial number shall persist across power fail...
//  $[00625] -- accepted settings persist across power fail...
//  $[00628] -- Alarm History Log entries persist across power fail...
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//  Data items that are stored in NOVRAM can NOT have any pointers
//  to data items that are outside of NOVRAM -- this includes virtual
//  function tables.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Persistent-Objects/vcssrc/NonVolatileMemory.ccv   25.0.4.0   19 Nov 2013 14:18:50   pvcs  $
//
//@ Modification-Log
//
//  Revision: 018   By: mnr   Date: 25-Feb-2010   SCR Number: 6436
//  Project:  PROX4
//  Description:
//      PROX Firmware Revision and serial number related updates.
//
//  Revision: 017  By:  gdc    Date:  12-Dec-2008    SCR Number: 6068
//  Project:  840S
//  Description:
//		Removed SUN prototype code.
//
//  Revision: 016  By:  gdc    Date:  10-Dec-2008    SCR Number: 6377
//  Project:  840S
//  Description:
//	Removed DELTA project single screen support.
//
//  Revision: 015   By: gdc  Date:  27-Mar-2007    SCR Number: 6237
//  Project:  Trend
//  Description:
//      Added compact flash test status for Trending option.
//
//  Revision: 014   By: sah  Date:  25-Aug-2000    DR Number:  5753
//  Project:  Delta
//  Description:
//      Delta project-specific changes:
//      *  added a screen-configuration flag, for determining whether this
//         is a single-screen configuration, or not
//
//  Revision: 013   By: sah  Date:  25-Jan-2000    DR Number:  5424
//  Project:  NeoMode
//  Description:
//      Added storing of user-controlled options, via the Development
//      Options subscreen.
//
//  Revision: 012   By: sah  Date:  11-Jan-1999    DR Number:  5321
//  Project:  ATC
//  Description:
//	As part of making all 'SoftFault()' methods non-inlined, I'm
//	also making those 'SoftFault()' methods that are NOT called
//	in PRODUCTION mode, DEVELOPMENT-only.
//
//  Revision: 011  By:  sah    Date:  08-May-1998    DCS Number: 5041
//  Project:  Sigma (R8027)
//  Description:
//	Changed 'SoftFault()' to a non-inlined method.
//
//  Revision: 010  By:  sah    Date:  29-Sep-1997    DCS Number: 2525
//  Project:  Sigma (R8027)
//  Description:
//	Removed 'exhCalInProgressState_' and 'ventInopTestInfo_' data
//	items.
//
//  Revision: 009  By:  sah    Date:  16-Sep-1997    DCS Number: 2385 & 1861
//  Project:  Sigma (R8027)
//  Description:
//	Added new Service-Mode data instance ('ServiceModeNovramData').
//	Also, add an instance of 'AtmPressOffset', which is available
//	on the BD CPU, only.
//
//  Revision: 008  By:  sah    Date:  04-Sep-1997    DCS Number: 1923
//  Project:  Sigma (R8027)
//  Description:
//	Replaced the Communication Diagnostic Log with the new System
//	Information Log.  Also, added 'SIGMA_UNIT_TEST' preprocessor
//	variable to the unit tests to detect subsystem-level corruption.
//
//  Revision: 007  By:  sah    Date:  17-Jul-1997    DCS Number: 2279
//  Project:  Sigma (R8027)
//  Description:
//	Added two new BD data items, 'airFlowSensorOffset_' and
//	o2FlowSensorOffset_'.
//
//  Revision: 006  By:  sah    Date:  15-Jul-1997    DCS Number: 2175
//  Project:  Sigma (R8027)
//  Description:
//	Removed four static methods whose only responsibility was to forward
//	the requests to the one instance of the corresponding class.  These
//	methods are now the responsibility of the corresponding classes.
//
//  Revision: 005  By:  sah    Date:  01-Jul-1997    DCS Number: 2066 & 2107
//  Project:  Sigma (R8027)
//  Description:
//	Moved 'lastSstPatientCctType_' from the GUI side to the BD side, and
//	replaced the 'exhCalInProgressFlag_' boolean data item with a
//	'exhCalInProgressState_' discrete data item.
//
//  Revision: 004  By:  sah    Date:  25-Jun-1997    DCS Number: 1908
//  Project:  Sigma (R8027)
//  Description:
//     For double-buffered items that are accessed by time-critical tasks,
//     use unique semaphores, otherwise use default double buffer item
//     semaphore.
//
//  Revision: 003  By:  sah    Date:  23-Jun-1997    DCS Number: 1902
//  Project:  Sigma (R8027)
//  Description:
//	Ignore initialization state when initializing a NOVRAM that has
//	not been initialized, yet.
//
//  Revision: 002  By:  sah    Date:  08-Apr-1997    DCS Number: 1901
//  Project:  Sigma (R8027)
//  Description:
//	Now using globals defined in 'MemoryMap.hh' for determining where,
//	in NOVRAM, to locate the application's persistent objects.
//
//  Revision: 001  By:  sah    Date:  15-Mar-1997    DCS Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//          Initial version (Integration baseline).
//
//=====================================================================

#include "NonVolatileMemory.hh"

//@ Usage-Classes
#include "MemoryMap.hh"
#include "Post.hh"
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Static Data Definitions...
//
//=====================================================================

#if defined(SIGMA_UNIT_TEST)
static Uint32  PStaticMemory_[(sizeof(NonVolatileMemory) + 3) / 4];
#endif // defined(SIGMA_UNIT_TEST)


//=====================================================================
//
//  Static Member Definitions...
//
//=====================================================================

Boolean  NonVolatileMemory::IsInitialized_ = FALSE;

NovRamStatus::InitializationId  NonVolatileMemory::InitializationStatusId_ =
						  NovRamStatus::ITEM_VERIFIED;
NovRamStatus::VerificationId  NonVolatileMemory::VerificationStatusId_ =
						  NovRamStatus::ITEM_VALID;

#if !defined(SIGMA_UNIT_TEST)

const Uint  NonVolatileMemory::MAX_BYTES_AVAILABLE_ = sizeof(NonVolatileMemory);

// Global pointer to the NonVolatileMemory area that will get
// saved by the NV_MemoryStorageThread.
NonVolatileMemory* NonVolatileMemory::P_INSTANCE_;



#else
// place ALL of the data items in DRAM if SUN or UNIT TEST...
const Uint  NonVolatileMemory::MAX_BYTES_AVAILABLE_ =
						  sizeof(NonVolatileMemory);
NonVolatileMemory* const  NonVolatileMemory::P_INSTANCE_ =
				       (NonVolatileMemory*)::PStaticMemory_;
#endif // !defined(SIGMA_UNIT_TEST)


//=====================================================================
//
//  Public Methods...
//
//=====================================================================

#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//            [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//  and 'lineNumber' are essential pieces of information.  The
//  'pFileName' and 'pPredicate' strings may be defaulted in the macro
//  to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
NonVolatileMemory::SoftFault(const SoftFaultID  softFaultID,
			     const Uint32       lineNumber,
			     const char*        pFileName,
			     const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, PERSISTENT_OBJS, NON_VOLATILE_MEMORY,
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)


//=====================================================================
//
//  Private Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  initialize_()  [static]
//
//@ Interface-Description
//  This block of NOVRAM is in one of three states:  UNITIALIZED (never
//  initialized); CORRUPT (initialized before, but invalid now); or
//  VALID (initialized before and still valid now).  This method handles
//  each of these states, and leaves this block of NOVRAM in an VALID state.
//  This involves verifying the validity of the data, and, if invalid, the
//  data is either corrected or re-constructed.  If any data item's
//  initialization detects some "corruption", this class's static data
//  member, 'InitializationStatusId_', stores a status other than
//  'NovRamStatus::ITEM_VERIFIED'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  (verifySelf_() == NovRamStatus::ITEM_VALID)
//  (NonVolatileMemory::IsInitialized_)
//@ End-Method
//=====================================================================

NovRamStatus::InitializationId
NonVolatileMemory::initialize_(void)
{
  CALL_TRACE("initialize_()");
  SAFE_CLASS_PRE_CONDITION((this == NonVolatileMemory::P_INSTANCE_));

  NovRamStatus::InitializationId  initStatusId;

  //===================================================================
  // Initialize ubiquitous NOVRAM data items...
  //===================================================================

  initStatusId = sequenceCount_.initialize();
  processInitStatusId_(initStatusId);
  SAFE_CLASS_ASSERTION((sequenceCount_.verifySelf() ==
  						NovRamStatus::ITEM_VALID));

  initStatusId = systemInfoLog_.initialize();
  processInitStatusId_(initStatusId);
  SAFE_CLASS_ASSERTION((systemInfoLog_.verifySelf() ==
  						NovRamStatus::ITEM_VALID));

  initStatusId = systemDiagLog_.initialize();
  processInitStatusId_(initStatusId);
  SAFE_CLASS_ASSERTION((systemDiagLog_.verifySelf() ==
  						NovRamStatus::ITEM_VALID));

  initStatusId = backgroundFailureFlag_.initialize();
  processInitStatusId_(initStatusId);
  SAFE_CLASS_ASSERTION((backgroundFailureFlag_.verifySelf() ==
  						NovRamStatus::ITEM_VALID));


  initStatusId = initStatusId = activeBtatEvents_.initialize();
  processInitStatusId_(initStatusId);
  SAFE_CLASS_ASSERTION((activeBtatEvents_.verifySelf() ==
  						NovRamStatus::ITEM_VALID));


  initStatusId = estDiagLog_.initialize();
  processInitStatusId_(initStatusId);
  SAFE_CLASS_ASSERTION((estDiagLog_.verifySelf() ==
  						NovRamStatus::ITEM_VALID));

  initStatusId = estResultTable_.initialize();
  processInitStatusId_(initStatusId);
  SAFE_CLASS_ASSERTION((estResultTable_.verifySelf() ==
  						NovRamStatus::ITEM_VALID));

  initStatusId = sstDiagLog_.initialize();
  processInitStatusId_(initStatusId);
  SAFE_CLASS_ASSERTION((sstDiagLog_.verifySelf() ==
  						NovRamStatus::ITEM_VALID));

  initStatusId = sstResultTable_.initialize();
  processInitStatusId_(initStatusId);
  SAFE_CLASS_ASSERTION((sstResultTable_.verifySelf() ==
  						NovRamStatus::ITEM_VALID));

  //===================================================================
  // Initialize the setting values and related flags...
  //===================================================================

  initStatusId = acceptedBatchValues_.initialize();
  processInitStatusId_(initStatusId);
  SAFE_CLASS_ASSERTION((acceptedBatchValues_.verifySelf() ==
  						NovRamStatus::ITEM_VALID));

  if (initStatusId == NovRamStatus::ITEM_INITIALIZED)
  {   // $[TI1]
    // a full initialization of the accepted setting values was done,
    // therefore "clear" the two Settings-Initialized Flags, thereby forcing
    // a New-Patient sequence...
    patientSettingsAvailFlag_.updateValue(FALSE);
    batchSettingsInitFlag_.updateValue(FALSE);
  }
  else
  {   // $[TI2] -- no full initialization done...
    initStatusId = patientSettingsAvailFlag_.initialize();
    processInitStatusId_(initStatusId);

    initStatusId = batchSettingsInitFlag_.initialize();
    processInitStatusId_(initStatusId);

    // verify the "synchronization" of the two flags...
    if (patientSettingsAvailFlag_.isTrue()  &&
      	batchSettingsInitFlag_.isFalse())
    {   // $[TI2.1]
      // this situation can only happen when the All-Batch-Settings-Init
      // Flag, itself, was corrupted; since the accepted batch values are
      // valid, set this flag to 'TRUE'...
      batchSettingsInitFlag_.updateValue(TRUE);
    }   // $[TI2.2] -- either both 'TRUE', or Patient-Avail is 'FALSE'...

    // if Patient-Settings-Avail 'TRUE', then the All-Settings-Init Flag MUST
    // be 'TRUE'; if Patient-Settings-Avail is 'FALSE', the All-Settings-Init
    // Flag can be either 'TRUE' or 'FALSE'...
    SAFE_CLASS_ASSERTION((patientSettingsAvailFlag_.isFalse()  ||
			  batchSettingsInitFlag_.isTrue()));
  }


  //===================================================================
  // Initialize the remaining "ubiquitous" items...
  //===================================================================

  initStatusId = unitSerialNum_.initialize();
  processInitStatusId_(initStatusId);
  SAFE_CLASS_ASSERTION((unitSerialNum_.verifySelf() ==
  						NovRamStatus::ITEM_VALID));

  initStatusId = userOptions_.initialize();
  processInitStatusId_(initStatusId);
  SAFE_CLASS_ASSERTION((userOptions_.verifySelf() ==
  						NovRamStatus::ITEM_VALID));

#if defined(SIGMA_GUI_CPU)

  //===================================================================
  //
  // Initialize GUI-only NOVRAM data items...
  //
  //===================================================================

  initStatusId = alarmHistoryLog_.initialize();
  processInitStatusId_(initStatusId);
  SAFE_CLASS_ASSERTION((alarmHistoryLog_.verifySelf() ==
  						NovRamStatus::ITEM_VALID));

  //===================================================================
  // Initialize the non-batch setting values and related flags...
  //===================================================================

  initStatusId = acceptedNonBatchValues_.initialize();
  processInitStatusId_(initStatusId);
  SAFE_CLASS_ASSERTION((acceptedNonBatchValues_.verifySelf() ==
  						NovRamStatus::ITEM_VALID));


  if (initStatusId == NovRamStatus::ITEM_INITIALIZED)
  {   // $[TI3]
    // a full initialization of the accepted non-batch values was done,
    // therefore "clear" the Non-Batch-Settings-Initialized Flags...
    nonBatchSettingsInitFlag_.updateValue(FALSE);
  }
  else
  {   // $[TI4] -- no full initialization done...
    // re-initialize all of the related flags, first...
    initStatusId = nonBatchSettingsInitFlag_.initialize();
    processInitStatusId_(initStatusId);
  }

  //===================================================================
  // Initialize the remaining "GUI" items...
  //===================================================================

  initStatusId = lastSstHumidifierType_.initialize();
  processInitStatusId_(initStatusId);
  SAFE_CLASS_ASSERTION((lastSstHumidifierType_.verifySelf() ==
  						NovRamStatus::ITEM_VALID));

  initStatusId = estTestResultData_.initialize();
  processInitStatusId_(initStatusId);
  SAFE_CLASS_ASSERTION((estTestResultData_.verifySelf() ==
  						NovRamStatus::ITEM_VALID));

  initStatusId = sstTestResultData_.initialize();
  processInitStatusId_(initStatusId);
  SAFE_CLASS_ASSERTION((sstTestResultData_.verifySelf() ==
  						NovRamStatus::ITEM_VALID));

  initStatusId = dataCorruptionInfo_.initialize(
			      NovRamSemaphore::DATA_CORRUPTION_INFO_ACCESS
					       );
  processInitStatusId_(initStatusId);
  SAFE_CLASS_ASSERTION((dataCorruptionInfo_.verifySelf(
			      NovRamSemaphore::DATA_CORRUPTION_INFO_ACCESS
						      ) ==
  						NovRamStatus::ITEM_VALID));

  initStatusId = noVentStartTime_.initialize();
  processInitStatusId_(initStatusId);
  SAFE_CLASS_ASSERTION((noVentStartTime_.verifySelf() ==
  						NovRamStatus::ITEM_VALID));

  initStatusId = compactFlashTestStatus_.initialize();
  processInitStatusId_(initStatusId);
  SAFE_CLASS_ASSERTION((compactFlashTestStatus_.verifySelf() ==
  						NovRamStatus::ITEM_VALID));

#endif // defined(SIGMA_GUI_CPU)

#if defined(SIGMA_BD_CPU)

  //===================================================================
  //
  // Initialize BD-only NOVRAM data items...
  //
  //===================================================================

  // since this double-buffered data item is accessed by a time-critical
  // task, it will be initialized with its own, unique semaphore...
  initStatusId = bdSystemState_.initialize(
				    NovRamSemaphore::BD_SYSTEM_STATE_ACCESS
					  );
  processInitStatusId_(initStatusId);
  SAFE_CLASS_ASSERTION(
  	(bdSystemState_.verifySelf(NovRamSemaphore::BD_SYSTEM_STATE_ACCESS) ==
  						NovRamStatus::ITEM_VALID));


  initStatusId = psolLiftoff_.initialize();
  processInitStatusId_(initStatusId);
  SAFE_CLASS_ASSERTION((psolLiftoff_.verifySelf() ==
  						NovRamStatus::ITEM_VALID));


  initStatusId = compressorOperationalMinutes_.initialize();
  processInitStatusId_(initStatusId);
  SAFE_CLASS_ASSERTION((compressorOperationalMinutes_.verifySelf() ==
  						NovRamStatus::ITEM_VALID));


  initStatusId = systemOperationalMinutes_.initialize();
  processInitStatusId_(initStatusId);
  SAFE_CLASS_ASSERTION((systemOperationalMinutes_.verifySelf() ==
  						NovRamStatus::ITEM_VALID));


  initStatusId = circuitComplianceTable_.initialize();
  processInitStatusId_(initStatusId);
  SAFE_CLASS_ASSERTION((circuitComplianceTable_.verifySelf() ==
  						NovRamStatus::ITEM_VALID));


  // since this double-buffered data item is accessed by a time-critical
  // task, it will be initialized with its own, unique semaphore...
  initStatusId = fio2CalInfo_.initialize(
				    NovRamSemaphore::FIO2_CAL_INFO_ACCESS
					);
  processInitStatusId_(initStatusId);
  SAFE_CLASS_ASSERTION(
  	(fio2CalInfo_.verifySelf(NovRamSemaphore::FIO2_CAL_INFO_ACCESS) ==
  						NovRamStatus::ITEM_VALID));


  initStatusId = compressorSerialNum_.initialize();
  processInitStatusId_(initStatusId);
  SAFE_CLASS_ASSERTION((compressorSerialNum_.verifySelf() ==
  						NovRamStatus::ITEM_VALID));


  initStatusId = inspCctResistance_.initialize();
  processInitStatusId_(initStatusId);
  SAFE_CLASS_ASSERTION((inspCctResistance_.verifySelf() ==
  						NovRamStatus::ITEM_VALID));


  initStatusId = exhCctResistance_.initialize();
  processInitStatusId_(initStatusId);
  SAFE_CLASS_ASSERTION((exhCctResistance_.verifySelf() ==
  						NovRamStatus::ITEM_VALID));

  initStatusId = airFlowSensorOffset_.initialize();
  processInitStatusId_(initStatusId);
  SAFE_CLASS_ASSERTION((airFlowSensorOffset_.verifySelf() ==
  						NovRamStatus::ITEM_VALID));

  initStatusId = o2FlowSensorOffset_.initialize();
  processInitStatusId_(initStatusId);
  SAFE_CLASS_ASSERTION((o2FlowSensorOffset_.verifySelf() ==
  						NovRamStatus::ITEM_VALID));

  initStatusId = lastSstPatientCctType_.initialize();
  processInitStatusId_(initStatusId);
  SAFE_CLASS_ASSERTION((lastSstPatientCctType_.verifySelf() ==
  						NovRamStatus::ITEM_VALID));

  initStatusId = serviceModeState_.initialize();
  processInitStatusId_(initStatusId);
  SAFE_CLASS_ASSERTION((serviceModeState_.verifySelf() ==
  						NovRamStatus::ITEM_VALID));

  initStatusId = atmPressOffset_.initialize();
  processInitStatusId_(initStatusId);
  SAFE_CLASS_ASSERTION((atmPressOffset_.verifySelf() ==
  						NovRamStatus::ITEM_VALID));

  initStatusId = proxFirmwareRevision_.initialize();
  processInitStatusId_(initStatusId);
  SAFE_CLASS_ASSERTION((proxFirmwareRevision_.verifySelf() ==
  						NovRamStatus::ITEM_VALID));

  initStatusId = proxSerialNumber_.initialize();
  processInitStatusId_(initStatusId);
  SAFE_CLASS_ASSERTION((proxSerialNumber_.verifySelf() ==
  						NovRamStatus::ITEM_VALID));

#endif // defined(SIGMA_BD_CPU)

  SAFE_CLASS_ASSERTION((verifySelf_() == NovRamStatus::ITEM_VALID));

  // indicate that this class is now ready for "interaction"...
  NonVolatileMemory::IsInitialized_ = TRUE;

  if (!Post::IsNovramInitialized())
  {   // $[TI5] -- ignore "corrupt" NOVRAM when NOVRAM is first initialized...
#if !defined(SIGMA_UNIT_TEST)
    NonVolatileMemory::InitializationStatusId_ = NovRamStatus::ITEM_VERIFIED;
#endif // !defined(SIGMA_UNIT_TEST)
  }   // $[TI6] -- NOVRAM is already initialized, therefore retain status...

  return(NonVolatileMemory::InitializationStatusId_);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  verifySelf_(rErrorCode)  [static]
//
//@ Interface-Description
//  Verify the current state of the NOVRAM Manager's data items.  If all
//  of the manager's data items are valid, 'NovRamStatus::ALL_ITEMS_VALID'
//  is returned, otherwise a 'NovRamStatus::ITEM_INVALID' is returned.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

NovRamStatus::VerificationId
NonVolatileMemory::verifySelf_(void)
{
  CALL_TRACE("verifySelf_()");

  // this must be called before proceeding...
  resetVerifStatusId_();

  NovRamStatus::VerificationId  verifStatusId;

  //===================================================================
  // Verify ubiquitous NOVRAM data items...
  //===================================================================

  verifStatusId = sequenceCount_.verifySelf();
  processVerifStatusId_(verifStatusId);

  verifStatusId = systemInfoLog_.verifySelf();
  processVerifStatusId_(verifStatusId);

  verifStatusId = systemDiagLog_.verifySelf();
  processVerifStatusId_(verifStatusId);

  verifStatusId = backgroundFailureFlag_.verifySelf();
  processVerifStatusId_(verifStatusId);

  verifStatusId = activeBtatEvents_.verifySelf();
  processVerifStatusId_(verifStatusId);

  verifStatusId = estDiagLog_.verifySelf();
  processVerifStatusId_(verifStatusId);

  verifStatusId = estResultTable_.verifySelf();
  processVerifStatusId_(verifStatusId);

  verifStatusId = sstDiagLog_.verifySelf();
  processVerifStatusId_(verifStatusId);

  verifStatusId = sstResultTable_.verifySelf();
  processVerifStatusId_(verifStatusId);

  verifStatusId = patientSettingsAvailFlag_.verifySelf();
  processVerifStatusId_(verifStatusId);

  verifStatusId = batchSettingsInitFlag_.verifySelf();
  processVerifStatusId_(verifStatusId);

  verifStatusId = acceptedBatchValues_.verifySelf();
  processVerifStatusId_(verifStatusId);

  verifStatusId = unitSerialNum_.verifySelf();
  processVerifStatusId_(verifStatusId);

#if defined(SIGMA_GUI_CPU)

  //===================================================================
  // Verify GUI-only NOVRAM data items...
  //===================================================================

  verifStatusId = alarmHistoryLog_.verifySelf();
  processVerifStatusId_(verifStatusId);

  verifStatusId = nonBatchSettingsInitFlag_.verifySelf();
  processVerifStatusId_(verifStatusId);

  verifStatusId = acceptedNonBatchValues_.verifySelf();
  processVerifStatusId_(verifStatusId);

  verifStatusId = lastSstHumidifierType_.verifySelf();
  processVerifStatusId_(verifStatusId);

  verifStatusId = estTestResultData_.verifySelf();
  processVerifStatusId_(verifStatusId);

  verifStatusId = sstTestResultData_.verifySelf();
  processVerifStatusId_(verifStatusId);

  verifStatusId = dataCorruptionInfo_.verifySelf(
			      NovRamSemaphore::DATA_CORRUPTION_INFO_ACCESS
  						);
  processVerifStatusId_(verifStatusId);

  verifStatusId = noVentStartTime_.verifySelf();
  processVerifStatusId_(verifStatusId);

#endif // defined(SIGMA_GUI_CPU)

#if defined(SIGMA_BD_CPU)

  verifStatusId = bdSystemState_.verifySelf(
				    NovRamSemaphore::BD_SYSTEM_STATE_ACCESS
  					   );
  processVerifStatusId_(verifStatusId);

  verifStatusId = psolLiftoff_.verifySelf();
  processVerifStatusId_(verifStatusId);

  verifStatusId = compressorOperationalMinutes_.verifySelf();
  processVerifStatusId_(verifStatusId);

  verifStatusId = systemOperationalMinutes_.verifySelf();
  processVerifStatusId_(verifStatusId);

  verifStatusId = circuitComplianceTable_.verifySelf();
  processVerifStatusId_(verifStatusId);

  verifStatusId = fio2CalInfo_.verifySelf(
				    NovRamSemaphore::FIO2_CAL_INFO_ACCESS
					 );
  processVerifStatusId_(verifStatusId);

  verifStatusId = compressorSerialNum_.verifySelf();
  processVerifStatusId_(verifStatusId);

  verifStatusId = inspCctResistance_.verifySelf();
  processVerifStatusId_(verifStatusId);

  verifStatusId = exhCctResistance_.verifySelf();
  processVerifStatusId_(verifStatusId);

  verifStatusId = airFlowSensorOffset_.verifySelf();
  processVerifStatusId_(verifStatusId);

  verifStatusId = o2FlowSensorOffset_.verifySelf();
  processVerifStatusId_(verifStatusId);

  verifStatusId = lastSstPatientCctType_.verifySelf();
  processVerifStatusId_(verifStatusId);

  verifStatusId = serviceModeState_.verifySelf();
  processVerifStatusId_(verifStatusId);

  verifStatusId = atmPressOffset_.verifySelf();
  processVerifStatusId_(verifStatusId);

  verifStatusId = proxFirmwareRevision_.verifySelf();
  processVerifStatusId_(verifStatusId);

  verifStatusId = proxSerialNumber_.verifySelf();
  processVerifStatusId_(verifStatusId);

#endif // defined(SIGMA_BD_CPU)

  return(NonVolatileMemory::VerificationStatusId_);
}   // $[TI1]
