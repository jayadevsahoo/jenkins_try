#pragma once

class UdpCommunication;
class UdpSocket
{
public:
	UdpSocket(void);
	~UdpSocket(void);

	bool Create();
	bool Connect(const CHAR* addr, UINT port);
	void Close();
	INT Send(const CHAR* sendbuf);
	INT Receive(CHAR* recvbuf, INT recvbuflen, INT maxMsToWait);
	bool ReceiveReady();

private:
    UdpCommunication*   PUdpComm_;
};
