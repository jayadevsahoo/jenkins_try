#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: PersistentObjs - The Persistent Objects Subsystem's Class.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is this subsystem's subsystem class.  This class is responsible
//  for the initialization of items within this subsystem.  It is
//  required that this subsystem be initialized before all of the other
//  application-level subsystems.
//---------------------------------------------------------------------
//@ Rationale
//  Each subsystem defines a subsystem class that is the point of
//  initialization for that subsystem.  This class is provides that
//  facility.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ Fault-Handling
//  none
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Persistent-Objects/vcssrc/PersistentObjs.ccv   25.0.4.0   19 Nov 2013 14:18:52   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006  By:  gdc    Date:  12-Dec-2008    SCR Number: 6068
//  Project:  840S
//  Description:
//		Removed SUN prototype code.
//
//  Revision: 005  By:  sah    Date:  01-Jan-1999    DCS Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' to a non-inlined method.
//
//  Revision: 004  By:  gdc    Date:  08-MAY-1998    DCS Number: 5084
//  Project:  Sigma (R8027)
//  Description:
//	Changed response to "SERVICE_REQUIRED" POST error. This error is
//  logged in the diagnostic log, but generates no system response.
//
//  Revision: 003  By:  sah    Date:  15-May-1997    DCS Number: 2112
//  Project:  Sigma (R8027)
//  Description:
//	Adding tracing of '[11023]', and '[11047]'.
//
//  Revision: 002  By:  sah    Date:  13-May-1997    DCS Number: 2085
//  Project:  Sigma (R8027)
//  Description:
//      Added the storing of auxillary information for POST diagnostics.
//
//  Revision: 001  By:  sah    Date:  15-Mar-1997    DCS Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//          Initial version (Integration baseline).
//
//=====================================================================

#include "PersistentObjs.hh"

//@ Usage-Classes
#include "NovRamSemaphore.hh"
#include "NovRamManager.hh"
#include "NovRamXaction.hh"
#include "NovRamUpdateManager.hh"
#include "NonVolatileMemory.hh"
#include "PostResultIterator.hh"
#include "AuxResult.hh"
#include "Post.hh"
#include "Background.hh"
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Static Member Initialization...
//
//=====================================================================

Uint  PersistentObjs::ArrDebugFlags_[];


//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  Initialize()  [static]
//
//@ Interface-Description
//  Initialize this subsystem.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[11023] -- POST alert shall be logged into System Diagnostic Log when
//              time-of-day clock is re-initialized.
//  $[11047] -- entries are made into the System Diagnostic Log for each
//              POST alert and failure detected prior to a successful POST
//              execution.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
PersistentObjs::Initialize(void)
{
  CALL_TRACE("Initialize()");

  // initialize the semaphore object that 'NovRamManager' uses to protect
  // its various data items...
  NovRamSemaphore::Initialize();

  // initialize the management of the application-level NOVRAM items; this
  // static method also verifies that each item is valid, and, if not, 
  // corrects those items...
  const NovRamStatus::InitializationId  INIT_STATUS_ID =
					      NovRamManager::Initialize();
  UNUSED_SYMBOL(INIT_STATUS_ID);

  //-------------------------------------------------------------------
  //  NOTE:  testable items 1, 1.1, 1.2, 1.3 and 1.4 can't be tested via
  //         unit tests, therefore extra diligence is needed during
  //         reviews...
  //-------------------------------------------------------------------

  PostResultIterator  postResultIterator;
  DiagnosticCode      postDiagCode;

  // log any POST results into the System Diagnostic Log...
  while (postResultIterator.next())
  {   // $[TI1] -- at least one iteration...
    const Criticality  CRITICALITY = postResultIterator.getTestResult();

    TestResultId  testResultId = NUM_TEST_RESULT_IDS;	//any way this case is not applicable for valid, EV_319 Warning Removal

    switch (CRITICALITY)
    {
    case MINOR :		// $[TI1.1]
      testResultId = ::MINOR_TEST_FAILURE_ID;

      // notify Safety-Net of minor POST failure...
      Background::SetPostMinorErrorOccurred();
      break;
    case SERVICE_REQUIRED :    // $[TI1.3]
      testResultId = ::MINOR_TEST_FAILURE_ID;
      break;
    case MAJOR :		// $[TI1.2]
      testResultId = ::MAJOR_TEST_FAILURE_ID;
      break;
    case PASSED :
    default :
      // invalid 'CRITICALITY'...
      AUX_CLASS_ASSERTION_FAILURE(CRITICALITY);
      break;
    };

    const AuxResult*  pAuxResult = postResultIterator.getPAuxResult();

    Uint32  programCounter;
    Uint8   vectorNum;
    Uint8   nmiSourceReg;
    Uint8   auxErrorCode;

    if (pAuxResult != NULL)
    {   // $[TI1.3] -- auxillary data available...
      programCounter = (Uint32)pAuxResult->getProgramCounter();
      vectorNum      = pAuxResult->getVectorNumber();
      nmiSourceReg   = pAuxResult->getNmiSource();
      auxErrorCode   = pAuxResult->getAuxCode();
    }
    else
    {   // $[TI1.4] -- no auxillary data available; set to default...
      programCounter = 0u;
      vectorNum      = 0u;
      nmiSourceReg   = 0u;
      auxErrorCode   = 0u;
    }

    postDiagCode.setStartupSelfTestCode(testResultId,
					postResultIterator.getTestNumber(),
					programCounter, vectorNum,
					nmiSourceReg, auxErrorCode);

    NovRamManager::LogSystemDiagnostic(postDiagCode);
  }   // $[TI2] -- no iterations...

  // clear out the newly logged entries...
  Post::ClearResultsLog();

  // initialize the NOVRAM transaction task info...
  NovRamXaction::Initialize();

  // initialize the update manager...
  NovRamUpdateManager::Initialize();
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  TurnAllDebugOn()  [static]
//
// Interface-Description
//  Turn all of this subsystem's debug flags on.
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  (PersistentObjs::IsDebugOn(...for all ids...))
// End-Method
//=====================================================================

void
PersistentObjs::TurnAllDebugOn(void)
{
  CALL_TRACE("TurnAllDebugOn()");

  for (Uint32 idx = 0; idx < PersistentObjs::NUM_WORDS_; idx++)
  {
    PersistentObjs::ArrDebugFlags_[idx] = ~(0u);
  }
}


//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  TurnAllDebugOff()  [static]
//
// Interface-Description
//  Turn all of this subsystem's debug flags off.
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  (!PersistentObjs::IsDebugOn(...for all ids...))
// End-Method
//=====================================================================

void
PersistentObjs::TurnAllDebugOff(void)
{
  CALL_TRACE("TurnAllDebugOff()");

  for (Uint32 idx = 0; idx < PersistentObjs::NUM_WORDS_; idx++)
  {
    PersistentObjs::ArrDebugFlags_[idx] = 0u;
  }
}

#endif  // defined(SIGMA_DEVELOPMENT)


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//            [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//  and 'lineNumber' are essential pieces of information.  The
//  'pFileName' and 'pPredicate' strings may be defaulted in the macro
//  to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//  none 
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

void
PersistentObjs::SoftFault(const SoftFaultID  softFaultID,
			  const Uint32       lineNumber,
			  const char*        pFileName,
			  const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, PERSISTENT_OBJS, PERSISTENT_OBJS_CLASS,
                          lineNumber, pFileName, pPredicate);
}
