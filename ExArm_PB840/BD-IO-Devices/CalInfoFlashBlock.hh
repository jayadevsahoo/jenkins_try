#ifndef CalInfoFlashBlock_HH
#define CalInfoFlashBlock_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: CalInfoFlashBlock - interface to flash memory
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/CalInfoFlashBlock.hhv   25.0.4.0   19 Nov 2013 13:54:00   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 002 By: syw   Date: 19-Dec-1996   DR Number: DCS 1603
//      Project:  Sigma (R8027)
//      Description:
//			Added airSerialNumberRead_, o2SerialNumberRead_, and
//			exhSerialNumberRead_ data members.
//
//  Revision: 001  By:  syw    Date: 08-Nov-1996    DR Number: INITIAL RELEASE
//		Project:  Sigma (R8027)
//		Description:
//			Initial version (Integration baseline).
//
//====================================================================

#include "Sigma.hh"
#include "BD_IO_Devices.hh"
#include "CalibrationTable.h"
#include "ExhValveTable.hh"

//@ Usage-Classes

#include "BDIOFlashMap.hh"

//@ End-Usage

class CalInfoFlashBlock {
  public:

    CalInfoFlashBlock( BDIOFlashMap * const pFlashAddress ) ;
    ~CalInfoFlashBlock( void) ;					// not implemented...

    static void SoftFault( const SoftFaultID	softFaultID,
						   const Uint32		lineNumber,
						   const char		*pFileName  = NULL,
						   const char		*pPredicate = NULL) ;

	inline BDIOFlashMap *getPFlashMap( void) const ;

	Boolean isFlashOkay( void) ;

	Uint32 programFlashBlock( void) ;
	void initialize( void) ;

	void buildFlowSensorData( void) ;

    //@ Data-Member: FlashImage
    // copy of what is in flash or to be programmed in flash
    BDIOFlashMap FlashImage ;

  protected:

  private:

    CalInfoFlashBlock( void) ;							// not implemented...
    CalInfoFlashBlock( const CalInfoFlashBlock&) ;		// not implemented...
    void operator=( const CalInfoFlashBlock&) ;			// not implemented...

	Boolean determineExhValveCalValid_(void) ;
	Boolean determineFlowSensorInfoValid_(void) ;
	Boolean determineExhFlowSensorCalValid_(void) ;

	void copyFlashToFlashImage_( void) ;

    //@ Data-Member: *pFlashMap_
    // pointer to flash where calibration data is stored
    BDIOFlashMap *pFlashMap_ ;

#if defined (SIGMA_BD_CPU)

  public:

    // TODO E600 VM: This needs review/discussion: The first 2 functions
    //  where from 840 and were just been moved from the .in file to here.
    //  - Put definition back in .in file
    //  - why isExhFlowSensorInfoValid_ was added? Isn't the existing isFlowSensorInfoValid_
    //    for exhal sensor?
    inline Boolean getExhValveCalValid( void) const
    {
        return( isExhValveCalValid_) ;
    }

    inline Boolean getFlowSensorInfoValid( void) const
    {
        return( isFlowSensorInfoValid_) ;
    }

    inline Boolean getExhFlowSensorInfoValid( void) const
    {
        return( isExhFlowSensorInfoValid_) ;
    }

	inline Boolean getExhFlowSensorCalValid( void) const
    {
        return( isExhFlowSensorCalValid_) ;
    }

    void SaveExhFlowSensorData(const ExhSensorTable_t & ExhSensorTable);
    void SaveExhValveData(const ExhValveTable::ExhValveCalTable & ValveTable);

    void InitFlowSensorData(const FlowSensorCalInfo &airInfo,
                            const FlowSensorCalInfo &o2Info,
							const ExhSensorTableStorage_t &exhInfo) ;

  private:

	//@ Data-Member: isExhValveCalValid_
	// set if exh valve cal table in flash is okay
	Boolean isExhValveCalValid_ ;

	//@ Data-Member: isFlowSensorInfoValid_
	// set if flow sensor data in flash is okay
	Boolean isFlowSensorInfoValid_ ;

    //@ Data-Member: isExhFlowSensorInfoValid_
	// set if exh flow sensor data (not including cal data) in flash is okay
	Boolean isExhFlowSensorInfoValid_;

	//@ Data-Member: isExhFlowSensorCalValid_
	// set if exh flow sensor cal data in flash is okay
	Boolean isExhFlowSensorCalValid_;

	//@ Data-Member: airSerialNumberRead_
	// serial number read from air eeprom
	Uint32 airSerialNumberRead_ ;

	//@ Data-Member: o2SerialNumberRead_
	// serial number read from o2 eeprom
	Uint32 o2SerialNumberRead_ ;

	//@ Data-Member: exhSerialNumberRead_
	// serial number read from exh eeprom
	Uint32 exhSerialNumberRead_ ;

#endif  // defined (SIGMA_BD_CPU)

} ;


#include "CalInfoFlashBlock.in"

#endif // CalInfoFlashBlock_HH
