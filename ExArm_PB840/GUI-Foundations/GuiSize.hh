#ifndef GuiSize_HH
#define GuiSize_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1995, Puritan-Bennett Corporation
//=====================================================================

//========================== C O N S T A N T    D E S C R I P T I O N ====
//@ Filename: GuiSize - Specifies sizes of linked list classes.
//---------------------------------------------------------------------
//@ Rationale
// This is the file containing the size constants used to instantiate the
// Link list and Iteration list used by the GuiFoundation.  Even though the
// GuiFoundation files do not use it directly, but the genGui script do use
// it to generate the appropriated include file to resolve the external
// variables.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Foundations/vcssrc/GuiSize.hhv   25.0.4.0   19 Nov 2013 14:11:42   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By: gdc      Date: 27-Jan-2007  SCR Number: 6237
//  Project:  Trend
//  Description:
//  Added multi-size Containers containing different sized Drawable lists.
//
//	Integration baseline.
//  Revision: 002  By: yyy      Date: 15-May-1995  DCR Number: 1948
//    Project:  Sigma (R8027)
//    Description:
//      Added a constant MAX_CHANGE_LIST_NUM for baseContainer to hold the changed
//      drawable list.  The size of MAX_AREA_LIST_NUM is according changed.  This will
//      avoid the waveform to overflow the baseContainer's changed list.
//
//  Revision: 001  By: yyy      Date: 01-Jan-1995  DR Number: NONE
//    Project:  Sigma (R8027)
//    Description:
//	Integration baseline.
//=====================================================================

//@ Constant: SM_DRAWABLE_LIST_NUM
// This constant is the size definition used by the Template macros for the
// drawable objects which can be allocated in the link list in a
// Container.
static const int SM_DRAWABLE_LIST_NUM = 30;

//@ Constant: LG_DRAWABLE_LIST_NUM
// This constant is the size definition used by the Template macros for the
// drawable objects which can be allocated in the link list in a
// LargeContainer.
static const int LG_DRAWABLE_LIST_NUM = 80;

//@ Constant: XL_DRAWABLE_LIST_NUM
// This constant is the size definition used by the Template macros for the
// drawable objects which can be allocated in the link list in a
// ExtraLargeContainer.
static const int XL_DRAWABLE_LIST_NUM = 160;

//@ Constant: MAX_CHANGE_LIST_NUM
// This constant is the size definition used by the Template macros for the
// changed drawable objects in the BaseContainer which can be allocated in
// the link list.
static const int MAX_CHANGE_LIST_NUM	= 300;


//@ Constant: MAX_AREA_LIST_NUM
// This constant is the size definition used by the Template macros for the
// changed area objects which can be allocated in the link list.
static const int MAX_AREA_LIST_NUM	=	400;

#endif // GuiFoundation_HH





