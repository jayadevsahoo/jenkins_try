#pragma once

#include <map>
using namespace std;

typedef LONG MEMDC_OBJ_ID;

typedef struct tagMEMDC_OBJ
{
	CDC* pMemDC;
	CBitmap* pBmp;
	CBitmap* pOldBmp;
	int width;
	int height;
} MEMDC_OBJ;

class GraphObjectManager
{
	GraphObjectManager(void);
	GraphObjectManager(const GraphObjectManager& ); //Not implemented.
	GraphObjectManager operator=(const GraphObjectManager& );// Not implemented.

	map <MEMDC_OBJ_ID, MEMDC_OBJ> Map_MemDC_Objs_;
	MEMDC_OBJ_ID MemDC_ID_Base_;
	static GraphObjectManager* Instance_;

public:
	static GraphObjectManager* getInstance();
	MEMDC_OBJ_ID createMemDCObj( CDC* pDC, int width, int height );
	void releaseMemDCObj( MEMDC_OBJ_ID objId );

	~GraphObjectManager(void);
};
