#ifndef SmTasks_HH
#define SmTasks_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: SmTasks - Service Mode tasks.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/SmTasks.hhv   25.0.4.0   19 Nov 2013 14:23:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 003  By:  gdc    Date: 02-Oct-1997    DR Number: DCS 2513
//       Project:  Sigma (840)
//       Description:
//            Added BD TaskControlMsgTask to receive GuiReadyMessages
//            that contain the GUI's serial number from GUI FLASH.
//
//  Revision: 002  By:  quf    Date: 22-May-1997    DR Number: DCS 2131
//       Project:  Sigma (840)
//       Description:
//            Modification-Log fix only.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: none
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "ServiceMode.hh"

//@ Usage-Classes
//@ End-Usage

const Uint16 MAX_SAMPLE_INDEX = 200;


class SmTasks {
  public:
    static void ServiceModeManagerTask( void) ;

#if defined (SIGMA_BD_CPU)
    static void ServiceModeBackgroundCycle( void) ;
#endif  // defined(SIGMA_BD_CPU)

	static void TaskControlMsgTask( void) ;

#ifdef SIGMA_SAFETY_NET
    static void SetEstablishPressure(const Boolean establishPressure );
	 
	static void SetDesirePressure( const Real32           desiredPressure,
		                           const MeasurementSide  desiredSide,
								   const GasType          desiredGas,
								   const Real32           flowRate );
#endif // SIGMA_SAFETY_NET

    static void SoftFault( const SoftFaultID softFaultID,
		  const Uint32      lineNumber,
		  const char*       pFileName  = NULL, 
		  const char*       pPredicate = NULL) ;
  
#if defined(SIGMA_DEBUG) && defined(SIGMA_BD_CPU)
	static Real32 InspPressure[MAX_SAMPLE_INDEX];
	static Real32 ExhPressure[MAX_SAMPLE_INDEX];
	static Real32 AirFlow[MAX_SAMPLE_INDEX];
	static Real32 O2Flow[MAX_SAMPLE_INDEX];
	static Real32 ExhAirFlow[MAX_SAMPLE_INDEX];
	static Real32 ExhO2Flow[MAX_SAMPLE_INDEX];
	static Uint16 SampleIndex;
	static Boolean CaptureData;
#endif  // defined(SIGMA_DEBUG) && defined(SIGMA_BD_CPU)

  protected:

  private:
    SmTasks( void) ;					// not implemented...
    ~SmTasks( void) ;					// not implemented...
    SmTasks( const SmTasks&) ;			// not implemented...
    void operator=( const SmTasks&) ;	// not implemented...

#ifdef SIGMA_BD_CPU
	static Boolean FirstBackgroundCycle_;
#endif // SIGMA_BD_CPU

#ifdef SIGMA_SAFETY_NET
static Boolean EstablishPressure_;
static Real32 DesiredPressure_;
static MeasurementSide DesiredSide_;
static GasType DesiredGas_;
static Real32 FlowRate_;
#endif // SIGMA_SAFETY_NET
  
} ;


#endif // SmTasks_HH 
