#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: Compressor - implements the functionality required for interfacing
// with the compressor device.
//---------------------------------------------------------------------
//@ Interface-Description
//  This class implements the interface to the compressor H/W functions.
//  It provides its client with the ability to interrogate whether or not
//  the compressor is installed, air is compressed, what is the operational
//  status of the compressor, and if the compressor is over temperature.
//
//  It provides its client with the ability to control the operational state of
//  the compressor (disable, enable, standby, and run).
//  Other services provided by this class are: interfacing to the compressor's
//  EEPROM in order to get/set the elapsed time, and the compressor
//  serial number.
//  The Compressor class is responsible for detecting changes in compressor
//  installed status, and in compressed air status, and notify the Alarm-
//  Analysis Sub-system and Vent Event Status about these changes.
//
//	A compressor anti-stalling algorithm is implemented where the compressor
//  is disabled for DISABLE_TIME_MS and then re-enabled.  The anti-stalling
//	algorithm is performed whenever no compressed air is detected.
//---------------------------------------------------------------------
//@ Rationale
//  The class provides access to the software controlled functions
//  of the compressor device.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Three private data members are implemented to keep track on the
//  compressor installed status, the compressed air status, and the
//  compressor's operational status.
//  Three BinaryIndicators are declared as private data members to
//  provide bit level I/O access to the compressor status information.
//  Two BinaryComands are declared as private data memebers to provide
//  bit level I/O access to control the compressor run/standby, and
//  off/enabled status.
//---------------------------------------------------------------------
//@ Fault-Handling
//  N/A
//---------------------------------------------------------------------
//@ Restrictions
//  None.
//---------------------------------------------------------------------
//@ Invariants
//  None.
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/Compressor.ccv   25.0.4.0   19 Nov 2013 13:54:00   pvcs  $
//
//@ Modification-Log
//
//  Revision: 015  By:  syw    Date:  28-Jul-1999    DR Number: 5416
//  Project:  840
//  Description:
//		Eliminate callbacks and call EventFilter class instead.
//
//  Revision: 014  By: hhd    Date:  18-Aug-1999    DR Number: DCS 5513
//    Project:  ATC (840)
//    Description:
//		Modified so the BK_COMPR_BAD_DATA event will be logged into NovRam instead of
//		sent to the BackgroundFaultHanlder Queue:
//			BK_COMPR_BAD_DATA
//
//  Revision: 013 By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//	Description:
//		Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 012  By:  syw    Date:  08-Sep-1998    DR Number: DCS 5151
//       Project:  Sigma (R8027)
//       Description:
//			BiLevel initial release.
//			Added second argument to pVentStatusCallback.
//			Use notInstalledSn instead of defaultSn in novram when there is
//			no compressor.
//
//  Revision: 011  By:  iv    Date:  30-Mar-1998    DR Number: DCS 5056
//       Project:  Sigma (R8027)
//       Description:
//			Changed method verifyCompressorSerialNumber_() to log a diagnostic
//          log information instead of a background fault.
//
//  Revision: 010  By:  syw    Date:  28-Oct-1996    DR Number: DCS 2592
//       Project:  Sigma (R8027)
//       Description:
//			Implement anti-stalling algorithm in newCycle(), initialize(), and
//			checkCompressedAirPresence() methods.
//
//  Revision: 009  By:  syw    Date:  08-Sep-1996    DR Number: DCS 2280
//       Project:  Sigma (R8027)
//       Description:
//			Eliminate call to RPowerSource.isInsufficientAcPower()
//
//  Revision: 008  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision:  007  By: iv   Date:  07-May-1997      DR Number: DCS 2004
//    Project:    Sigma (R8027)
//    Description:
//      Redesigned initialize method to properly initialize the eeprom.  The
//		CompressorOpMinAtPowerup_ data member is set to either novram or
//		the compressor eeprom elapsed time if certain conditions are met.

//  Revision: 006  By: syw    Date: 28-Apr-1997    DR Number: DCS 1996
//      Project:  Sigma (840)
//        Description:
//          Added requirement tracing.
//
//  Revision: 005  By: iv    Date:  23-Apr-1997    DR Number: DCS 1969
//      Project:  Sigma (840)
//        Description:
//          Added a call in disableCompressor() to invoke alarm for no air.
//
//  Revision: 004  By: by    Date:  10-Mar-1997    DR Number: DCS 1826
//      Project:  Sigma (840)
//        Description:
//          Added additional updates to compressorReadyStatus_ and
//          operationaStatus_ in checkCompressedAirPresence() so that
//          GUI LEDs are in synch with current status.
//
//  Revision: 003  By: by    Date:  11-Feb-1997    DR Number: DCS 1677
//      Project:  Sigma (840)
//        Description:
//          Changed the annuciation of compressor status to Alarm-
//          Analysis Subsystem and GUI vent status LEDs.
//
//  Revision: 002  By: syw    Date:  19-Dec-1996    DR Number: DCS 1641
//      Project:  Sigma (840)
//        Description:
//            Changed rCompressorReadWritePort to rCompressorReadPort or
//            rCompressorWritePort.  Change arguments for BinaryCommand.
//            Changed masks to bit location.
//
//  Revision: 001  By:  by    Date:  11-Nov-1996    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Initial version
//
//=====================================================================
// TODO E600_LL: This file along with the header file are to be deleted
#include "Compressor.hh"

// E600 BDIO #include "CompressorEeprom.hh"
// E600 BDIO #include "CompressorTimer.hh"
// E600 BDIO (This stuff is stubbed out in the header file)
#if 0
#include "NovRamManager.hh"
#include "GasSupply.hh"
#include "PowerSource.hh"
#include "RegisterRefs.hh"
#include "VentObjectRefs.hh"
#include "Background.hh"
#include "EventFilter.hh"
#include "BDIORefs.hh"

//@ Usage-Classes

//@ End-Usage

//@ Code...

//  Hardware Requirement Specifications are found in
//  Analog Interface PCBA No. 70550-45 Rev. 05

//@ Constant: COMPRESSOR_INSTALLED_MASK
// mask to read the compressor installed bit data
static const Uint16 COMPRESSOR_INSTALLED_MASK = 0x20;

//@ Constant: COMPRESSOR_AIR_PRESENT_MASK
// mask to read the compressed air present bit data
static const Uint16 COMPRESSOR_AIR_PRESENT_MASK = 0x08;

//@ Constant: COMPRESSOR_OVER_TEMPERATURE_MASK
// mask to read the compressor over temperature bit data
static const Uint16 COMPRESSOR_OVER_TEMPERATURE_MASK = 0x10;

//@ Constant: COMPRESSOR_RUN_STDBY_MASK
// mask to write to the compressor run/standby bit data
static const Uint16 COMPRESSOR_RUN_STDBY_MASK = 0x04;

//@ Constant: COMPRESSOR_OFF_MASK
// mask to write to the compressor off/enabled bit data
static const Uint16 COMPRESSOR_OFF_MASK = 0x02;

//@ Constant: NUM_OF_SECONDS_IN_ONE_MINUTE
// the number of seconds in a minute
static const Uint16 NUM_OF_SECONDS_IN_ONE_MINUTE = 60;

//@ Constant: NUM_OF_MINUTES_IN_ONE_HOUR
// the number of minutes in a hour
static const Uint16 NUM_OF_MINUTES_IN_ONE_HOUR = 60 ;

Uint32 Compressor::CompressorOpMinAtPowerup_ = 0;

#ifdef SIGMA_UNIT_TEST
extern CompressorEeprom::errorStatus UT_GetTotalElapsedTimeReturn ;
extern CompressorEeprom::errorStatus UT_UpdateTotalElapsedTimeReturn ;
extern Uint32 UT_currentCompressorOpHours ;
extern Uint32 UT_totalEepromElapsedHours ;
extern Uint32 UT_elapsedMinSincePowerup ;
#endif // SIGMA_UNIT_TEST

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Compressor()  [Default Contstructor]
//
//@ Interface-Description
// The constructor initializes all privately contained objects.
//---------------------------------------------------------------------
//@ Implementation-Description
// An initializer list is used to construct the objects.
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

Compressor::Compressor( void ) :
    compInstalled_( COMPRESSOR_INSTALLED_MASK, RCompressorReadPort ),
    compAirPresent_( COMPRESSOR_AIR_PRESENT_MASK, RCompressorReadPort ),
    compOverTemperature_( COMPRESSOR_OVER_TEMPERATURE_MASK, RCompressorReadPort ),
    compRunStby_( COMPRESSOR_RUN_STDBY_MASK, RCompressorWritePort),
    compOff_( COMPRESSOR_OFF_MASK, RCompressorWritePort)
{
    // $[TI1]
    CALL_TRACE("Compressor::Compressor(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~Compressor()  [Destructor]
//
//@ Interface-Description
// Destructor
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

Compressor::~Compressor(void)
{
    CALL_TRACE("Compressor::~Compressor(void)");
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: initialize
//
//@ Interface-Description
//
//  This method takes no arguments and has no return value. It sets up
//  the Compressor object to an initial known state.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//  The compressor operational status is unconditionally initialized to
//  OFF. The Boolean data members indicating compressor installed and
//  compressed air present are initialized using the appropriate
//  compressor methods.  If the compressor is installed, initialize the
//	compressor eeprom and timer.  Update the compressor serial number to
//	novram.  Determine CompressorOpMinAtPowerup_ by examining the novram
//	elapsed time or the compressor eeproms elapsed time.
// $[08006]
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=====================================================================

void
Compressor::initialize( void )
{
    CALL_TRACE("Compressor::initialize(void)");

    operationStatus_ = Compressor::OFF;
    compressorReadyStatus_ = Compressor::INIT;

    if( compInstalled_.getState() == BinaryIndicator::OFF )
    {
    // $[TI1]
        isInstalled_ = TRUE;

        if ( compAirPresent_.getState() == BinaryIndicator::ON )
        {
        // $[TI1.1]
            isAirPresent_ = TRUE;
        }
        else
        {
        // $[TI1.2]
            isAirPresent_ = FALSE;
        }
    }
    else
    {
    // $[TI2]
        isInstalled_ = FALSE;
        isAirPresent_ = FALSE;
    }

    if (isInstalled_ )
    {
    // $[TI3]
        RCompressorEeprom.initialize();
// E600 BDIO        RCompressorTimer.initialize();
        verifyCompressorSerialNumber_();

	    Uint32 novRamCompressorElapsedMins = NovRamManager::GetCompressorOperationalMinutes() ;
	    Uint32 compressorEepromElapsedHrs = 0;

#ifndef SIGMA_UNIT_TEST
    	Uint32 error = RCompressorEeprom.getTotalElapsedTime( &compressorEepromElapsedHrs) ;
#else
    	Uint32 error = UT_GetTotalElapsedTimeReturn ;
    	compressorEepromElapsedHrs = UT_totalEepromElapsedHours ;
#endif // SIGMA_UNIT_TEST

    	if (error != CompressorEeprom::DATA_OK)
    	{
    		// error in reading, use novram
		    // $[TI5]
            Background::LogDiagnosticCodeUtil( BK_COMPR_BAD_DATA );
   			CompressorOpMinAtPowerup_ = novRamCompressorElapsedMins ;
    	}
    	else
    	{
		    // $[TI6]
			if (novRamCompressorElapsedMins >= compressorEepromElapsedHrs * NUM_OF_MINUTES_IN_ONE_HOUR &&
					novRamCompressorElapsedMins < (compressorEepromElapsedHrs + 1) * NUM_OF_MINUTES_IN_ONE_HOUR)
			{
				// eeprom and novram agree within 1 hour, use novram since it has minute resolution
			    // $[TI7]
    			CompressorOpMinAtPowerup_ = novRamCompressorElapsedMins ;
    		}
	    	else
    		{
    			// use compressor eeprom and update novram
			    // $[TI8]
    			CompressorOpMinAtPowerup_ = compressorEepromElapsedHrs * NUM_OF_MINUTES_IN_ONE_HOUR ;
	        	NovRamManager::UpdateCompressorOperationalMinutes( CompressorOpMinAtPowerup_ );
	    	}
    	}
    }
    else
    {
    // $[TI4]
        CompressorOpMinAtPowerup_ = 0;
        SerialNumber notInstalledSn( "NOTINSTALL") ;
        NovRamManager::UpdateCompressorSerialNumber( notInstalledSn );
        NovRamManager::UpdateCompressorOperationalMinutes( CompressorOpMinAtPowerup_ );
    }
    performAntiStallManeuver_ = FALSE ;
    antiStallTimer_ = 0 ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newCycle
//
//@ Interface-Description
//  This method can be executed only after the new cycle method for the
//  compressor registers is called. This method takes no parameter and
//  returns no value. A check for a change in the compressor installed
//  status is performed.
//
//  If such a change is detected, the Alarm-Analysis sub-system is notified,
//  and the compressor is either disabled, enabled, or command to run.
//  A check for a change in the compressed air status is performed.
//
//  If such a malfunction is detected, the Alarm-Analysis sub-system is
//  notified.
//
//  If the compressor status has changed, the VentAndUserEventStatus class
//  is notified via the pVentStatusCallBack.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Check if the compressor needs to perform anti stall maneuver.  If it does,
//	then disable the compressor for 20 msec and return back to normal operation.
//
//  The compressor status is checked and compared to the previous status.
//  Any change is reported to the Alarm-Analysis sub-system.
//
//  The PowerSource class is interrogated as to the status of the A/C
//  voltage.  If the current A/C voltage is lost or low, the compressor
//  is shut-off immediately.
//
//  The GasSupply class is interrogated as to the current wall air
//  supply status.  If no wall air is detected, and the compressor
//  can be sustained, it is command to run continuously.
//
//  $[00436] $[05150] $[05151] $[05152] $[05153] $[05154] $[05155] $[05161]
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

void
Compressor::newCycle( void )
{
    CALL_TRACE("Compressor::newCycle( void )");

	const Uint32 DISABLE_TIME_MS = 20 ;

	if (performAntiStallManeuver_)
	{
    	// $[TI3]
        compOff_.setBit( BitAccessGpio::OFF);
		isAirPresent_ = FALSE;
        operationStatus_ = Compressor::OFF;
        compressorReadyStatus_ = Compressor::NOT_READY;
		antiStallTimer_ += CYCLE_TIME_MS ;

		if (antiStallTimer_ > DISABLE_TIME_MS)
		{
	    	// $[TI5]
			performAntiStallManeuver_ = FALSE ;
		}  	// implied else $[TI6]
	}
	else
	{
    	// $[TI4]

	    if ( TRUE == checkCompressorInstalled() &&
            PowerSource::AC_POWER == RPowerSource.getSource())
	    {
	    	// $[TI1]

        	if ( operationStatus_ == Compressor::OFF )
	        {
    		    // $[TI1.1]
        	    enableCompressor();
	        }
    	    else
        	{
		        // $[TI1.2]
    	        checkCompressedAirPresence();
        	}

	        if ( FALSE == RGasSupply.isAirPresent() )
    	    {
        		// $[TI1.3]
	            setCompressorMotorOn();
    	    }
        	else
	        {
    		    // $[TI1.4]
            	setCompressorMotorReady();
	        }
    	}
	    else
    	{
		    // $[TI2]
        	disableCompressor();
	    }
	}
}

// E600 BDIO
#endif

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:    UpdateTotalElapsedTime
//
//@ Interface-Description
//  This method accepts no argument and returns a
//  CompressorEeprom::errorStatus enumeration.
//
//  This method updates the compressor operational time to the
//  compressor EEPROM.  It reads the compressor timer to determine
//  the elapsed compressor operational time since power-up. It
//  calculates, in seconds, the elapsed time since the last update
//  to the EEPROM.  If more than an hour has elapsed since last update
//  to the EEPROM, it increments the total compressor operational
//  hour stored in the EEPROM by one and then updates the NOVRAM
//  with the residual minutes in second resolution.
//  An error status indicating the success of the EEPROM update is
//  returned to the client.
//---------------------------------------------------------------------
//@ Implementation-Description
//	$[08007]
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

// E600 BDIO CompressorEeprom::errorStatus
// E600 BDIO Compressor::UpdateTotalElapsedTime( void )
Compressor::CompressorStatus Compressor::UpdateTotalElapsedTime( void )
{
// E600 BDIO
#if 0
    Uint32 currentCompressorOpHours = 0;
    Uint32 elapsedMinSincePowerup = 0;
    Uint32 currentTotalCompressorOpMinutes = 0;
    Uint32 totalEepromElapsedHours = 0;
    CompressorEeprom::errorStatus error = CompressorEeprom::DATA_OK;

    // calculate elapsed time, in seconds, since last hourly update
#ifndef SIGMA_UNIT_TEST
    elapsedMinSincePowerup = RCompressorTimer.readActiveCounter()
                           / NUM_OF_SECONDS_IN_ONE_MINUTE;
#else
	elapsedMinSincePowerup = UT_elapsedMinSincePowerup ;
#endif // SIGMA_UNIT_TEST

    currentTotalCompressorOpMinutes = CompressorOpMinAtPowerup_
                                    + elapsedMinSincePowerup;

	if (NovRamManager::GetCompressorOperationalMinutes() != currentTotalCompressorOpMinutes)
	{
	    // $[TI3]
    	NovRamManager::UpdateCompressorOperationalMinutes( currentTotalCompressorOpMinutes );
    }
    // $[TI4]

    currentCompressorOpHours = currentTotalCompressorOpMinutes / NUM_OF_MINUTES_IN_ONE_HOUR;

#ifndef SIGMA_UNIT_TEST
    error = RCompressorEeprom.getTotalElapsedTime( &totalEepromElapsedHours );

#else
	error = UT_GetTotalElapsedTimeReturn ;
	currentCompressorOpHours = UT_currentCompressorOpHours ;
	totalEepromElapsedHours = UT_totalEepromElapsedHours ;
#endif // SIGMA_UNIT_TEST

    if ( CompressorEeprom::DATA_OK == error)
    {
    // $[TI1]
        if ( currentCompressorOpHours > totalEepromElapsedHours )
        {
        // $[TI1.1]
#ifndef SIGMA_UNIT_TEST
            error = RCompressorEeprom.updateTotalElapsedTime( currentCompressorOpHours );
#else
			error = UT_UpdateTotalElapsedTimeReturn ;
#endif // SIGMA_UNIT_TEST
        }
        // $[TI1.2]
    }
    // $[TI2]

    return( error) ;	// DATA_OK, BAD_CHECKSUM, UPDATE_FAILED
// E600 BDIO
#endif
    return OFF;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: checkCompressorInstalled()
//
//@ Interface-Description
//  This method takes no argument and returns a Boolean flag.
//  The method checks the hardware bit that indicates if the compressor
//  is installed.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The BinaryIndicator compInstalled_ is used to check the compressor
//  installed bit. If the current status is different from the previous,
//  the Alarm-Analysis Subsystem and Vent Status are notified.
//  A Boolean flag indicating compressor installed is returned.
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=====================================================================

// E600 BDIO
#if 0
Boolean
Compressor::checkCompressorInstalled( void )
{
    CALL_TRACE("Compressor::checkCompressorInstalled( void )");

    Boolean compressorInstalled = FALSE;

    if( compInstalled_.getState() == BinaryIndicator::OFF )
    {
    // $[TI1]
        compressorInstalled = TRUE;
    }
    // $[TI2]

    if( compressorInstalled != isInstalled_)
    {
    // $[TI3]

        if( FALSE == compressorInstalled )
        {
        // $[TI3.1]
            // send Compressor Not installed message to alarm analysis subsystem
            REventFilter.postAlarm( EventFilter::COMP_OPTION, BdAlarmId::BDALARM_NOT_COMP_OPTION_PRESENT, FALSE) ;
            REventFilter.postEventStatus( EventFilter::COMP_OPERATING, EventData::CANCEL, EventData::NULL_EVENT_PROMPT) ;
            REventFilter.postEventStatus( EventFilter::COMP_READY, EventData::CANCEL, EventData::NULL_EVENT_PROMPT) ;
        }
        else
        {
        // $[TI3.2]
            REventFilter.postAlarm( EventFilter::COMP_OPTION, BdAlarmId::BDALARM_COMP_OPTION_PRESENT, FALSE) ;
        }

        isInstalled_ = compressorInstalled;
    }
    // $[TI4]

    return ( compressorInstalled );
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: checkCompressedAirPresence()
//
//@ Interface-Description
//  This method takes no argument and returns a Boolean flag.
//  The method checks the hardware bits that indicate if the compressed
//  is installed and air is present.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The BinaryIndicator compInstalled_ is used to check the compressor
//  air present bit. If the current status is different from the previous,
//  the Alarm-Analysis Subsystem and Vent Status are notified.
//  A Boolean flag indicating compressor installed is returned.
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=====================================================================

Boolean
Compressor::checkCompressedAirPresence( void )
{
    CALL_TRACE("Compressor::checkCompressedAirPresence(void)");

    Boolean airPresent = FALSE;

    // Air is only present if the compressor option is installed
    if ( ( compInstalled_.getState() == BinaryIndicator::OFF ) &&
         ( compAirPresent_.getState() == BinaryIndicator::ON ) )
    {
    // $[TI1]
        airPresent = TRUE;
    }
    // $[TI2]

    if( airPresent != isAirPresent_ )
    {
    // $[TI3]

        // notify alarms on change in compressed air status
        if ( TRUE == airPresent )
        {
        // $[TI3.1]
        	antiStallTimer_ = 0 ;
            // update compressor status data members
            compressorReadyStatus_ = Compressor::READY;
            operationStatus_ = Compressor::ENABLED;

            REventFilter.postAlarm( EventFilter::COMP_AIR, BdAlarmId::BDALARM_COMP_AIR_PRESENT, FALSE) ;
            REventFilter.postEventStatus( EventFilter::COMP_READY, EventData::ACTIVE, EventData::NULL_EVENT_PROMPT) ;
        }
        else
        {
        // $[TI3.2]
            // update compressor status data members
            compressorReadyStatus_ = Compressor::NOT_READY;
            operationStatus_ = Compressor::OFF;

			if (antiStallTimer_ == 0)
			{
			    // $[TI5]
				performAntiStallManeuver_ = TRUE ;
			}	// implied else $[TI6]

            REventFilter.postAlarm( EventFilter::COMP_AIR, BdAlarmId::BDALARM_NOT_COMP_AIR_PRESENT, FALSE) ;
            REventFilter.postEventStatus( EventFilter::COMP_OPERATING, EventData::CANCEL, EventData::NULL_EVENT_PROMPT) ;
            REventFilter.postEventStatus( EventFilter::COMP_READY, EventData::CANCEL, EventData::NULL_EVENT_PROMPT) ;
        }

        isAirPresent_ = airPresent;
    }
    // $[TI4]

    return ( airPresent );
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setCompressorMotorOn()
//
//@ Interface-Description
//  This method takes no argument and has no return value.
//  If the compressor is not in a run state already, it is command to RUN.
//  It updates the data member operationStatus_ to RUN.
//  Alarm-Analysis Subsystem and Vent Status are notified of the change
//  in compressor status.
//---------------------------------------------------------------------
//@ Implementation-Description
//  It issues a command to set the compressor to the RUN state.
//  It changes the updates the data member operationStatus_ to
//  RUN.  Vent Status is notified of the change in compressor status.
//---------------------------------------------------------------------
//@ PreCondition
//  Compressor must be installed.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=====================================================================

void
Compressor::setCompressorMotorOn( void )
{
    CALL_TRACE("Compressor::setCompressorMotorOn(void)");

    CLASS_PRE_CONDITION(compInstalled_.getState() == BinaryIndicator::OFF);

    if ( ( operationStatus_ != Compressor::RUN ) && ( TRUE == isAirPresent_ ) )
    {
    // $[TI1]
        //turn compressor motor on
        compRunStby_.setBit( BitAccessGpio::ON);
        operationStatus_ = Compressor::RUN;
        REventFilter.postEventStatus( EventFilter::COMP_OPERATING, EventData::ACTIVE, EventData::NULL_EVENT_PROMPT) ;
    }
    // $[TI2]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setCompressorMotorReady()
//
//@ Interface-Description
//  This method takes no argument and has no return value.
//  It issues a command to set the compressor to the STANDBY state.
//  It changes the updates the data member operationStatus_ to
//  STANDBY. Alarm Subsystem is notified of the change in compressor status.
//---------------------------------------------------------------------
//@ Implementation-Description
//  It issues a command to set the compressor to the STANDBY state.
//  It changes the updates the data member operationStatus_ to
//  STANDBY.  Vent Status is notified of the change in compressor status.
//---------------------------------------------------------------------
//@ PreCondition
//  Compressor must be installed.
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

void
Compressor::setCompressorMotorReady( void )
{
    CALL_TRACE("Compressor::setCompressorMotorReady(void)");

    CLASS_PRE_CONDITION(compInstalled_.getState() == BinaryIndicator::OFF);

    if ( operationStatus_ != Compressor::STANDBY )
    {
    // $[TI1]
        //turn compressor motor ready state
        compRunStby_.setBit( BitAccessGpio::OFF);
        operationStatus_ = Compressor::STANDBY;
        REventFilter.postEventStatus( EventFilter::COMP_OPERATING, EventData::CANCEL, EventData::NULL_EVENT_PROMPT) ;
    }
    // $[TI2]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: enableCompressor()
//
//@ Interface-Description
//  This method takes no arguments and has no return value.
//  It issues a command to set the compressor to the ENABLED state.
//  It updates the data member operationStatus_ to ENABLED.
//  Alarm Subsystem is notified of the change in compressor status.
//---------------------------------------------------------------------
//@ Implementation-Description
//  It issues a command to set the compressor to the ENABLED state.
//  It changes the updates the private data member operationStatus_ to
//  ENABLED.  Vent Status is notified of the change in compressor status.
//---------------------------------------------------------------------
//@ PreCondition
//  Compressor must be installed.
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

void
Compressor::enableCompressor(void)
{
    CALL_TRACE("Compressor::enableCompressor(void)");

    CLASS_PRE_CONDITION(compInstalled_.getState() == BinaryIndicator::OFF);

    if ( operationStatus_ != Compressor::ENABLED )
    {
    // $[TI1]
        compOff_.setBit( BitAccessGpio::ON);
        operationStatus_ = Compressor::ENABLED;
        compressorReadyStatus_ = Compressor::READY;
    }
    // $[TI2]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: disableCompressor()
//
//@ Interface-Description
//  This method takes no arguments and has no return value. It issues a
//  command to set the compressor to the OFF state.
//  It changes the internal state of the Compressor object to indicate
//  compressor OFF.
//---------------------------------------------------------------------
//@ Implementation-Description
//  It issues a command to set the compressor to the OFF state.
//  It changes the updates the private data member operationStatus_ to
//  OFF.  Vent Status is notified of the change in compressor status.
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=====================================================================

void
Compressor::disableCompressor( void )
{
    CALL_TRACE("Compressor::disableCompressor(void)");

    if ( operationStatus_ != Compressor::OFF )
    {
    // $[TI1]

        compOff_.setBit( BitAccessGpio::OFF);
		isAirPresent_ = FALSE;
        operationStatus_ = Compressor::OFF;
        compressorReadyStatus_ = Compressor::NOT_READY;

        // send Compressor air Not present message to alarm analysis subsystem
        REventFilter.postAlarm( EventFilter::COMP_AIR, BdAlarmId::BDALARM_NOT_COMP_AIR_PRESENT, FALSE) ;
        REventFilter.postEventStatus( EventFilter::COMP_OPERATING, EventData::CANCEL, EventData::NULL_EVENT_PROMPT) ;
        REventFilter.postEventStatus( EventFilter::COMP_READY, EventData::CANCEL, EventData::NULL_EVENT_PROMPT) ;
    }
    // $[TI2]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: initialStatusNotification
//
//@ Interface-Description
//  This method takes no arguments and has no return value. It is invoked
//  after system initialization to notify Alarms and the GUI of initial
//  compressor status.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The local isInstalled_ and isAirPresent_ are used to determine
//  if the compressor is installed and if compressed air is available.
//  The Alarm-Analysis subsystem is then notified of the status.
//  $[05154] $[05155]
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=====================================================================

void
Compressor::initialStatusNotification(void)
{
    CALL_TRACE("Compressor::initialStatusNotification(void)");

    if( isInstalled_ )
    {
    // $[TI1]
        // send Compressor installed  message to alarm analysis subsystem
        REventFilter.postAlarm( EventFilter::COMP_OPTION, BdAlarmId::BDALARM_COMP_OPTION_PRESENT, TRUE) ;
    }
    else
    {
    // $[TI2]
        // send Compressor Not installed  message to alarm analysis subsystem
        REventFilter.postAlarm( EventFilter::COMP_OPTION, BdAlarmId::BDALARM_NOT_COMP_OPTION_PRESENT, TRUE) ;
    }

    if( isAirPresent_ )
    {
    // $[TI3]
        // send Compressor air present message to alarm analysis subsystem
        REventFilter.postAlarm( EventFilter::COMP_AIR, BdAlarmId::BDALARM_COMP_AIR_PRESENT, TRUE) ;
        REventFilter.postEventStatus( EventFilter::COMP_READY, EventData::ACTIVE, EventData::NULL_EVENT_PROMPT) ;
    }
    else
    {
    // $[TI4]
        // send Compressor air Not present message to alarm analysis subsystem
        REventFilter.postAlarm( EventFilter::COMP_AIR, BdAlarmId::BDALARM_NOT_COMP_AIR_PRESENT, TRUE) ;
        REventFilter.postEventStatus( EventFilter::COMP_READY, EventData::CANCEL, EventData::NULL_EVENT_PROMPT) ;
    }

}

#endif

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//  and 'lineNumber' are essential pieces of information.  The
//  'pFileName' and 'pPredicate' strings may be defaulted in the macro
//  to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=====================================================================

void
Compressor::SoftFault( const SoftFaultID  softFaultID,
                       const Uint32       lineNumber,
                       const char*        pFileName,
                       const char*        pPredicate )
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault( softFaultID, BD_IO_DEVICES, COMPRESSOR,
                           lineNumber, pFileName, pPredicate );
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//=====================================================================
//
//  Private Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: verifyCompressorSerialNumber_
//
//@ Interface-Description
//  This method takes no arguments and has no return value.  This method
//	is called to update the novram compressor serial number with the
//	serial numbered stored on the eeprom.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method examines the compressor serial number stored in the
//  compressor EEPROM and compares it with the one stored in the NOVRAM.
//  If they are different, the serial number stored in NOVRAM is updated
//	with the one stored in the compressor EEPROM and Background is notified.
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=====================================================================

// E600 BDIO
#if 0
void
Compressor::verifyCompressorSerialNumber_( void )
{
    SerialNumber compressorSerialNumber, novRamSerialNumber;

    compressorSerialNumber = RCompressorEeprom.getSerialNumber();
    NovRamManager::GetCompressorSerialNumber( novRamSerialNumber );

    // check for a different compressor installed
    if ( compressorSerialNumber != novRamSerialNumber )
    {
    // $[TI1]

        // store current compressor serial number in NOVRAM
        NovRamManager::UpdateCompressorSerialNumber( compressorSerialNumber );
        Background::LogDiagnosticCodeUtil( ::BK_COMPR_UPDATE_SN );
	}
    // $[TI2]
}

#endif


