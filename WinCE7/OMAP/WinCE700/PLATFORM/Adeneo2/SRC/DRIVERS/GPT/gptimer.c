//------------------------------------------------------------------------------
//
// Copyright (C) 2004-2010, Freescale Semiconductor, Inc. All Rights Reserved.
// THIS SOURCE CODE, AND ITS USE AND DISTRIBUTION, IS SUBJECT TO THE TERMS
// AND CONDITIONS OF THE APPLICABLE LICENSE AGREEMENT
//
//------------------------------------------------------------------------------
//
// File:  gptpdd.c
//
// Implementation of General Purpose Timer Driver
//
//-----------------------------------------------------------------------------

#pragma warning(push)
#pragma warning(disable: 4115 4201 4204 4214)
#include <windows.h>
#include <Devload.h>
#include <omap.h>
#include <oal_clock.h>
#include "omap_prcm_regs.h"
//#include "bsp_cfg.h"
#include <nkintr.h>
#pragma warning(pop)

#include "omap3530_clocks.h"
#include "gpt_priv.h"

#include "omap_gpio_regs.h"

#define DEBUG_TIMER_AUTORUN 0
#define DEUB_TIMER_PWM		0
#define DEBUG_TIMER_GPIO	0
#define DEBUG_TIMER_PRINT	0

//-----------------------------------------------------------------------------
// External Functions


//-----------------------------------------------------------------------------
// External Variables


//-----------------------------------------------------------------------------
// Defines
#define THREAD_PRIORITY				0

#define TIMER_POSTED_TIMEOUT		1000

#define DELTA_TIME					20         // In TICK

#define DEFAULT_PERIOD_MS			2
#define MAX_PERIOD_MS				0x7CFFFFF

#define GPT_FCLK					0x48005000     /* FCLK reg */
#define GPT_ICLK					0x48005010    /* ICLK reg */
#define GPT_CLK_SOURCE				0x48005040    /* CLK source */

//------------------------------------------------------------------------------
//
//  Define: MSEC / TICK conversions

//  Conversions for 32kHz clock
#define MSEC_TO_TICK(msec)  (((msec) << 12)/125 + 1)		// msec * 32.768
#define TICK_TO_MSEC(tick)  (((tick) * 1000) >> 15)			// tick / 32.768

//-----------------------------------------------------------------------------
// Types


//-----------------------------------------------------------------------------
// Global Variables

/*
 * Global:  dpCurSettings
 * Set debug zones names and initial setting for driver
 */
#ifdef DEBUG

DBGPARAM dpCurSettings = {
    L"Gptimer", {
        L"Errors",      L"Warnings",    L"Function",    L"Info",
        L"IST",         L"Undefined",   L"Undefined",   L"Undefined",
        L"Undefined",   L"Undefined",   L"Undefined",   L"Undefined",
        L"Undefined",   L"Undefined",   L"Undefined",   L"Undefined"
    },
    0x0005
};

#endif

//-----------------------------------------------------------------------------
// Local Variables

UINT32 timer_posted_pending_bit[23] = 
{
    0, 0, 0, 0, 0, 0, 0, 0, 0,
    TCLR_PENDING_BIT,
    TCRR_PENDING_BIT,
    TLDR_PENDING_BIT,
    TTGR_PENDING_BIT,
    0,
    TMAR_PENDING_BIT,
    0,
    0,
    0,
    TPIR_PENDING_BIT,
    TNIR_PENDING_BIT,
    TCVR_PENDING_BIT,
    TOCR_PENDING_BIT,
    TOWR_PENDING_BIT,
};

//-----------------------------------------------------------------------------
// Local Functions
static DWORD GptIntrThread(POMAP_GPT_STRUCT pDevice);
static DWORD GptISRLoop(POMAP_GPT_STRUCT pDevice, UINT32);
static void GptRegInit(POMAP_GPT_STRUCT pDevice);

DWORD GPT_Open(DWORD hDeviceContext, DWORD AccessCode, DWORD ShareMode);

//-----------------------------------------------------------------------------
// Local Variables
static DWORD AppEventRate = 2;

//------------------------------------------------------------------------------
//
//  Function: GptGetReg
//
//  General function to read Timer registers
//

__inline UINT32 GptGetReg(POMAP_GPT_STRUCT pDevice, REG32 *addr)
{
    UINT32 i=0;
	
    if(pDevice->Posted)
        while(INREG32(&pDevice->pGPT->TWPS) & timer_posted_pending_bit[((UINT32)addr&0xff)>>2]) 
		if(i++>TIMER_POSTED_TIMEOUT)
		{
		    RETAILMSG(1, (L"\r\n GptGetReg: wait timeout"));
		    break;
	       }
    
    return INREG32(addr);
    
}

//------------------------------------------------------------------------------
//
//  Function: GptSetReg
//
//  General function to Write Timer registers
//   addr: virtural addr of Timer register

__inline VOID GptSetReg(POMAP_GPT_STRUCT pDevice, REG32 *addr, UINT32 val)
{
    UINT32 i=0;

    if(pDevice->Posted)
        while(INREG32(&pDevice->pGPT->TWPS) & timer_posted_pending_bit[((UINT32)addr&0xff)>>2]) 
		if(i++>TIMER_POSTED_TIMEOUT) 
		{
		    RETAILMSG(1, (L"\r\n GptSetReg: wait timeout"));
		    break;
	       }
    OUTREG32(addr, val);
}

//------------------------------------------------------------------------------
//
//  Function:  OALTimerSetCompare
//	
__inline VOID GptTimerSetCompare(POMAP_GPT_STRUCT pDevice, UINT32 compare)
{

    GptSetReg(pDevice, &pDevice->pGPT->TMAR, compare);

	// We commented out the following line because it causes issues to the overall performance
	// of the system. As the tick timer is clocked at 32 kHz, the TMAR register takes some 
	// time to update and we loose this time waiting for it. The consequence is that we spend
	// more than 5% of the time in this loop where the CPU should actually be idle. Not waiting
	// should not have any consequence as we never actually read its value.
	//
    //while ((INREG32(&g_pTimerRegs->TWPS) & GPTIMER_TWPS_TMAR) != 0);

    // make sure we don't set next timer interrupt to the past
    //
    if (compare < GptGetReg(pDevice, &pDevice->pGPT->TCRR))
		GptUpdateTimerPeriod(pDevice, 1);
}

//------------------------------------------------------------------------------
//
//  Function: OALTimerStart
//
//  General function to start timer 1

VOID GptTimerStart(POMAP_GPT_STRUCT pDevice)
{
    UINT tcrrExit = 0;

    RETAILMSG(DEBUG_TIMER_PRINT,(L"GptTimerStart+\r\n"));

	if(pDevice->Mode == TIMER_MODE_PERIODIC)
	{
		//GptUpdateTimerPeriod(pDevice, pDevice->Period);
		GptUpdateTimerPeriod(pDevice, 1);
		// Enable overflow interrupt
		GptEnableTimerInterrupt(pDevice);
	}
	
    // Start the timer
	GptSetReg(pDevice, &pDevice->pGPT->TCLR, GptGetReg(pDevice, &pDevice->pGPT->TCLR) | GPTIMER_TCLR_ST);
    
	// get current TCRR value: workaround for errata 1.35
    GptGetReg(pDevice, &pDevice->pGPT->TCRR);
    tcrrExit = GptGetReg(pDevice, &pDevice->pGPT->TCRR);
    // ERRATA 1.31 workaround (ES 1.0 only)
    // wait for updated TCRR value
	while (tcrrExit == (GptGetReg(pDevice, &pDevice->pGPT->TCRR)));

	RETAILMSG(DEBUG_TIMER_PRINT,(L"GptTimerStart-\r\n"));
}

//------------------------------------------------------------------------------
//
//  Function: OALTimerStop
//
//  General function to stop timer 1

VOID GptTimerStop(POMAP_GPT_STRUCT pDevice)
{   
	UINT32 SetValue;
	
	if(pDevice->Mode == TIMER_MODE_PERIODIC)
		GptDisableTimerInterrupt(pDevice);

	SetValue = GptGetReg(pDevice, &pDevice->pGPT->TCLR) & ~GPTIMER_TCLR_ST;
	// stop the timer
    GptSetReg(pDevice, &pDevice->pGPT->TCLR, SetValue);
}

//-----------------------------------------------------------------------------
//
// Function: GptInitialize
//
// This function will call the PMU function to enable the timer source
// clock and create the timer irq event and create the IST thread.
//
// Parameters:
//      pContext
//          [in] Pointer to a string containing the registry path to the
//          active key for the stream interface driver.
//
// Returns:
//      TRUE - If success
//
//      FALSE - If failure
//
//-----------------------------------------------------------------------------
DWORD GptInitialize(LPCTSTR pContext)
{   
    LONG    regError;
	HKEY    hKey;
	DWORD   dwDataSize;
    PHYSICAL_ADDRESS phyAddr;
	POMAP_GPT_STRUCT pDevice;
    
    UNREFERENCED_PARAMETER(pContext);

	RETAILMSG(DEBUG_TIMER_PRINT,(L"GptInitialize+\r\n"));
	
	pDevice = (POMAP_GPT_STRUCT)LocalAlloc(LPTR, sizeof(OMAP_GPT_STRUCT));
    if(!pDevice)
    {
        DEBUGMSG(ZONE_ERROR,
            (TEXT("%s: LocalAlloc failed!\r\n"), __WFUNCTION__));
        goto Error;
    }

	// try to open active device registry key for this context
    hKey = OpenDeviceKey(pContext);
    if (hKey == NULL)
    {
        DEBUGMSG(ZONE_ERROR,
            (TEXT("%s: OpenDeviceKey failed!\r\n"), __WFUNCTION__));
        goto Error;
    }

    // try to load eCSPI index from registry data
    dwDataSize = sizeof(DWORD);
    regError = RegQueryValueEx(
        hKey,                       // handle to currently open key
        L"Index",                   // string containing value to query
        NULL,                       // reserved, set to NULL
        NULL,                       // type not required, set to NULL
        (LPBYTE)(&(pDevice->dwGptIndex)),      // pointer to buffer receiving value
        &dwDataSize);               // pointer to buffer size

    // close handle to open key
    RegCloseKey(hKey);

    // check for errors during RegQueryValueEx
    if (regError != ERROR_SUCCESS)
    {
        DEBUGMSG(ZONE_ERROR,
            (TEXT("%s: RegQueryValueEx failed!\r\n"), __WFUNCTION__));
        goto Error;
    }
	else
		RETAILMSG(DEBUG_TIMER_PRINT,(L"Timer Index = %d\r\n", pDevice->dwGptIndex));

#if DEBUG_TIMER_AUTORUN
	pDevice->TimerID = OMAP_DEVICE_GPTIMER10;
#else
	if(pDevice->dwGptIndex == 9)
		pDevice->TimerID = OMAP_DEVICE_GPTIMER10;
	else if(pDevice->dwGptIndex == 2)
		pDevice->TimerID = OMAP_DEVICE_GPTIMER3;
	else
		goto Error;
#endif
    
    phyAddr.QuadPart = GetAddressByDevice(pDevice->TimerID);
    if(!phyAddr.QuadPart)
    {
        DEBUGMSG(ZONE_ERROR,
            (TEXT("%s: GetAddressByDevice() failed\r\n"), __WFUNCTION__));
        goto Error;
    }
    
    pDevice->pGPT = (OMAP_GPTIMER_REGS *) MmMapIoSpace(phyAddr, sizeof(OMAP_GPTIMER_REGS), FALSE);

    // check if Map Virtual Address failed
    if (pDevice->pGPT == NULL)
    {
        DEBUGMSG(ZONE_ERROR,
            (TEXT("%s:  MmMapIoSpace failed!\r\n"), __WFUNCTION__));
        goto Error;
    }

    DEBUGMSG(ZONE_INFO, (TEXT("%s: CreateEvent\r\n"), __WFUNCTION__));
    pDevice->hGptIntrEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
    if (pDevice->hGptIntrEvent == NULL)
    {
        DEBUGMSG(ZONE_ERROR,
            (TEXT("%s: CreateEvent failed\r\n"), __WFUNCTION__));
        goto Error;
    }

    pDevice->dwGptIrq = GetIrqByDevice(pDevice->TimerID, NULL);
    if(!pDevice->dwGptIrq)
    {
        DEBUGMSG(ZONE_ERROR,
            (TEXT("%s: BSPGptGetIRQ() failed\r\n"), __WFUNCTION__));
        goto Error;
    }
  
    // Translate IRQ to SYSINTR
    if (!KernelIoControl(IOCTL_HAL_REQUEST_SYSINTR, &(pDevice->dwGptIrq), sizeof(pDevice->dwGptIrq),
        &(pDevice->dwGptIntr), sizeof(pDevice->dwGptIntr), NULL))
    {
        DEBUGMSG(ZONE_ERROR,
            (TEXT("%s: Request SYSINTR failed\r\n"), __WFUNCTION__));
        goto Error;
    }
    
    if(!InterruptInitialize(pDevice->dwGptIntr, pDevice->hGptIntrEvent, NULL, 0))
    {
        CloseHandle(pDevice->hGptIntrEvent);
        pDevice->hGptIntrEvent = NULL;
        DEBUGMSG(ZONE_ERROR,
            (TEXT("%s: Interrupt initialization failed! \r\n"), __WFUNCTION__));
        goto Error;
    }

    // Initialize critical section for GPT
    InitializeCriticalSection(&pDevice->hGptLock);

    if (NULL == (pDevice->hTimerThread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)GptIntrThread, pDevice, 0, NULL)))
    {
        DEBUGMSG(ZONE_ERROR,(TEXT("%s: CreateThread failed!\r\n"), __WFUNCTION__));
        return 0;
    }
    else
    {
        DEBUGMSG(ZONE_INFO, (TEXT("%s: create timer thread success\r\n"), __WFUNCTION__));

        // Set our interrupt thread's priority
        CeSetThreadPriority(pDevice->hTimerThread, THREAD_PRIORITY);
		CeSetThreadQuantum(pDevice->hTimerThread, 0);
    }

    // Set initial state for GPT registers
    pDevice->bCurrentlyOwned = FALSE;
	pDevice->hTimerEvent = NULL;
    pDevice->TimerEventString = NULL;
	pDevice->Mode = TIMER_MODE_PERIODIC;
	pDevice->Period = DEFAULT_PERIOD_MS;

	GptRegInit(pDevice);

	RETAILMSG(DEBUG_TIMER_PRINT,(L"GptInitialize-\r\n"));

    return (DWORD)pDevice;

Error:
    GptRelease(pDevice);
	RETAILMSG(DEBUG_TIMER_PRINT,(L"GptInitialize- with Error\r\n"));
    return 0;
}


//------------------------------------------------------------------------------
//
// Function: GptRelease
//
// This function will release all resources and terminate the IST thread.
//
// Parameters:
//      pDevice
//          [in] Pointer to GPT structure
//
// Returns:
//      None
//
//------------------------------------------------------------------------------
void GptRelease(POMAP_GPT_STRUCT pDevice)
{
    if(!pDevice)
		return;

	if (pDevice->pGPT)
    {
        GptDisableTimer(pDevice);
        GptDisableTimerInterrupt(pDevice);
        
        MmUnmapIoSpace(pDevice->pGPT,sizeof(OMAP_GPTIMER_REGS));
    }
    
    EnableDeviceClocks(pDevice->TimerID, FALSE);

    if (pDevice->bCurrentlyOwned)
    {
        pDevice->bCurrentlyOwned = FALSE;
    }
    
    // Release SYSINTR
    if ((pDevice->dwGptIntr != SYSINTR_UNDEFINED)&& pDevice->dwGptIrq)
    {
        KernelIoControl(IOCTL_HAL_RELEASE_SYSINTR, &(pDevice->dwGptIntr), sizeof(DWORD), NULL, 0, NULL);
        InterruptDisable(pDevice->dwGptIntr);
        pDevice->dwGptIntr = (DWORD) SYSINTR_UNDEFINED;
    }

    if (pDevice->hGptIntrEvent)
    {
        CloseHandle(pDevice->hGptIntrEvent);
        pDevice->hGptIntrEvent = NULL;
    }

    if (pDevice->hTimerEvent)
    {
        CloseHandle(pDevice->hTimerEvent);
        pDevice->hTimerEvent = NULL;
    }

    if (pDevice->hTimerThread)
    {
        CloseHandle(pDevice->hTimerThread);
        pDevice->hTimerThread = NULL;
    }

    // Delete GPT critical section
    DeleteCriticalSection(&pDevice->hGptLock);

    // Free the OMAP_GPT_STRUCT structure.
    LocalFree(pDevice);
}


//------------------------------------------------------------------------------
//
// Function: GptTimerCreateEvent
//
// This function is used to create a GPT timer event handle
// triggered in the GPT ISR.
//
// Parameters:
//      pDevice
//          [in] Pointer to GPT structure
//      eventString
//          [in] GPT event name
//
// Returns:
//      TRUE if success
//      FALSE if handle creation fails or if event
//      hasn't already been created by caller
//
//------------------------------------------------------------------------------
BOOL GptTimerCreateEvent(POMAP_GPT_STRUCT pDevice, LPTSTR eventString)
{
    RETAILMSG(DEBUG_TIMER_PRINT,(L"GptTimerCreateEvent+\r\n"));

    pDevice->hTimerEvent = CreateEvent(NULL, FALSE, FALSE, eventString);
    if ((pDevice->hTimerEvent == NULL) || (GetLastError() != ERROR_ALREADY_EXISTS))
    {
        DEBUGMSG(ZONE_ERROR, 
            (TEXT("%s: CreateEvent failed or event does not already exist\r\n"), __WFUNCTION__));
        return FALSE;
	}

    pDevice->TimerEventString = eventString;

    RETAILMSG(DEBUG_TIMER_PRINT,(L"GptTimerCreateEvent-\r\n"));
    return TRUE;
}


//------------------------------------------------------------------------------
//
// Function: GptTimerReleaseEvent
//
// This function is used to close the handle to the 
// GPT timer event.
//
// Parameters:
//      pDevice
//          [in] Pointer to GPT structure
//      eventString
//          [in] Event name
//
// Returns:
//      TRUE if success
//      FALSE if string name does not match
//
//------------------------------------------------------------------------------
BOOL GptTimerReleaseEvent(POMAP_GPT_STRUCT pDevice, LPTSTR eventString)
{
    if(pDevice->TimerEventString)
		return FALSE;

	if (pDevice->TimerEventString == NULL)  // This avoids warning C6387
		return FALSE;

	if (strcmp((const char *) pDevice->TimerEventString, (const char *) eventString) != 0)
    {
        DEBUGMSG(ZONE_ERROR, 
            (TEXT("%s: Cannot close GPT timer event handle: Wrong event string name.\r\n"), __WFUNCTION__));
        return FALSE;
    }
    
    CloseHandle(pDevice->hTimerEvent);
    
    return TRUE;
}


//------------------------------------------------------------------------------
//
// Function: GptEnableTimer
//
// This function is used to start the timer. It will set the enable 
// bit in the GPT control register. This function should be called to
// enable the timer after configuring the timer.
//
// Parameters:
//      pDevice
//          [in] Pointer to GPT structure
//
// Returns:
//      TRUE always.
//
//------------------------------------------------------------------------------
BOOL GptEnableTimer(POMAP_GPT_STRUCT pDevice)
{
    EnterCriticalSection(&pDevice->hGptLock);

    EnableDeviceClocks(pDevice->TimerID, TRUE);

    LeaveCriticalSection(&pDevice->hGptLock);

    return TRUE;
}


//------------------------------------------------------------------------------
//
// Function: GptDisableTimer
//
// This function is used to stop the timer. It will
// clear the enable bit in the GPT control register.
// After this, the counter will reset to 0x00000000.
//
// Parameters:
//      pDevice
//          [in] Pointer to GPT structure
//
// Returns:
//      None
//
//------------------------------------------------------------------------------
void GptDisableTimer(POMAP_GPT_STRUCT pDevice)
{
    EnterCriticalSection(&pDevice->hGptLock);

    EnableDeviceClocks(pDevice->TimerID, FALSE);

    LeaveCriticalSection(&pDevice->hGptLock);
}


//------------------------------------------------------------------------------
//
// Function: GptEnableTimerInterrupt
//
// This function is used to enable the timer interrupt.
//
// Parameters:
//      pDevice
//          [in] Pointer to GPT structure
//
// Returns:
//      None
//
//------------------------------------------------------------------------------
void GptEnableTimerInterrupt(POMAP_GPT_STRUCT pDevice)
{
    EnterCriticalSection(&pDevice->hGptLock);

    // Enable match interrupt
    if(pDevice->Mode == TIMER_MODE_PERIODIC)
		GptSetReg(pDevice, &pDevice->pGPT->TIER, GPTIMER_TIER_OVERFLOW);

    LeaveCriticalSection(&pDevice->hGptLock);
}


//------------------------------------------------------------------------------
//
// Function: GptDisableTimerInterrupt
//
// This function is used to disable the timer interrupt.
//
// Parameters:
//      pDevice
//          [in] Pointer to GPT structure
//
// Returns:
//      None
//
//------------------------------------------------------------------------------
void GptDisableTimerInterrupt(POMAP_GPT_STRUCT pDevice)
{
    EnterCriticalSection(&pDevice->hGptLock);

    // Enable match interrupt
    GptSetReg(pDevice, &pDevice->pGPT->TIER, 0);

    LeaveCriticalSection(&pDevice->hGptLock);
}

//------------------------------------------------------------------------------
//
// Function: GptClearInterruptStatus
//
// This function is used to clear the GPT interrupt status and signal to the
// kernel that interrupt processing is completed.
//
// Parameters:
//      pDevice
//          [in] Pointer to GPT structure
//
// Returns:
//      None
//
//------------------------------------------------------------------------------
void GptClearInterruptStatus(POMAP_GPT_STRUCT pDevice)
{
    EnterCriticalSection(&pDevice->hGptLock);

    // Clear interrupt
	GptSetReg(pDevice, &pDevice->pGPT->TISR, GPTIMER_TIER_OVERFLOW);

    LeaveCriticalSection(&pDevice->hGptLock);

    // Kernel call to unmask the interrupt so that it can be signalled again
    InterruptDone(pDevice->dwGptIntr);
}


//------------------------------------------------------------------------------
//
// Function: GptRegInit
//
// Set GPT registers to initial state. 
//
// Parameters:
//      pDevice
//          [in] Pointer to GPT structure
//
// Returns:
//      None
//
//------------------------------------------------------------------------------
static void GptRegInit(POMAP_GPT_STRUCT pDevice)
{
    EnterCriticalSection(&pDevice->hGptLock);

    // enable gptimer
    EnableDeviceClocks(pDevice->TimerID, TRUE);

	// stop timer
    GptSetReg(pDevice, &pDevice->pGPT->TCLR, 0);

    // Soft reset GPTIMER
    GptSetReg(pDevice, &pDevice->pGPT->TIOCP, SYSCONFIG_SOFTRESET);

    // While until done
    while ((GptGetReg(pDevice, &pDevice->pGPT->TISTAT) & GPTIMER_TISTAT_RESETDONE) == 0);

    // Set smart idle
    GptSetReg(pDevice,
			&pDevice->pGPT->TIOCP, 0x300 |
            SYSCONFIG_SMARTIDLE|SYSCONFIG_ENAWAKEUP|
            SYSCONFIG_AUTOIDLE
        );

    // Enable posted mode
    GptSetReg(pDevice, &pDevice->pGPT->TSICR, GPTIMER_TSICR_POSTED);
    pDevice->Posted =       1;

    // Set match register to avoid unwanted interrupt
    GptSetReg(pDevice, &pDevice->pGPT->TMAR, 0xFFFFFFFF);

    // Enable match interrupt
	GptEnableTimerInterrupt(pDevice);

    // Enable match wakeup
    GptSetReg(pDevice, &pDevice->pGPT->TWER, GPTIMER_TWER_MATCH);

    // Enable timer in auto-reload and compare mode
    //GptSetReg(pDevice, &pDevice->pGPT->TCLR, GPTIMER_TCLR_CE|GPTIMER_TCLR_AR|GPTIMER_TCLR_ST);
#if DEBUG_TIMER_PWM
	GptSetReg(pDevice, &pDevice->pGPT->TCLR, GPTIMER_TCLR_AR|GPTIMER_TCLR_TRG_OVERFLOW|GPTIMER_TCLR_PT| (1<<7));
#else
	GptSetReg(pDevice, &pDevice->pGPT->TCLR, GPTIMER_TCLR_AR);
#endif

	if(pDevice->Mode == TIMER_MODE_PERIODIC)
	{
		{
			GptSetReg(pDevice, &pDevice->pGPT->TPIR, 232000);
			GptSetReg(pDevice, &pDevice->pGPT->TNIR, (UINT32)-768000);
			GptSetReg(pDevice, &pDevice->pGPT->TLDR, 0xFFFFFFE0);
			GptSetReg(pDevice, &pDevice->pGPT->TCRR, 0xFFFFFFE0);
		}
		//GptUpdateTimerPeriod(pDevice, DEFAULT_PERIOD_MS);
	}

    // Wait until write is done
    while ((INREG32(&pDevice->pGPT->TWPS) & GPTIMER_TWPS_TCLR) != 0);	//++++ uncommented
    
    LeaveCriticalSection(&pDevice->hGptLock);
}

//------------------------------------------------------------------------------
//
// Function: GptIntrThread
//
// This function is the IST thread.
//
// Parameters:
//      pDevice
//          [in] Pointer to GPT structure
//
// Returns:
//      None
//
//------------------------------------------------------------------------------
DWORD GptIntrThread(POMAP_GPT_STRUCT pDevice)
{
    RETAILMSG(DEBUG_TIMER_PRINT,(L"GptIntrThread+\r\n"));
    
    return (GptISRLoop(pDevice, INFINITE));
}


//------------------------------------------------------------------------------
//
// Function: GptISRLoop
//
// This function is the interrupt handler for the GPT. 
// It waits for the GPT interrupt event, and signals
// the timer registered by the user of this timer.
//
// Parameters:
//      pDevice
//          [in] Pointer to GPT structure
//      timeout
//          [in] Timeout value while waiting for GPT timer interrupt.
//
// Returns:
//      None
//
//------------------------------------------------------------------------------
static DWORD GptISRLoop(POMAP_GPT_STRUCT pDevice, UINT32 timeout)
{
	DWORD rc = TRUE;
	BOOL Triggered = FALSE;
	static DWORD TriggerCount = 0;

#if DEBUG_TIMER_GPIO    
	static DWORD BitState = 0;
	static DWORD Bit = 95 % 32;
	PHYSICAL_ADDRESS pa;
	OMAP_GPIO_REGS* pGpioRegs;
	volatile UINT *p;
	
	pa.QuadPart = 0x49052000;	// OMAP_GPIO3_REGS_PA
	pGpioRegs = (OMAP_GPIO_REGS*)MmMapIoSpace(pa, sizeof(OMAP_GPIO_REGS), FALSE);
	p = &pGpioRegs->DATAOUT;
#endif

    RETAILMSG(DEBUG_TIMER_PRINT, (L"GptISRLoop+\r\n"));

    // loop here
    for (;;)
    {
		//Triggered = ~Triggered & 0x01;
		if(++TriggerCount >= AppEventRate)
		{
			TriggerCount = 0;
			Triggered = TRUE;
		}

		//DEBUGMSG (ZONE_INFO, (TEXT("%s: In the loop\r\n"), __WFUNCTION__));
        if ((rc = WaitForSingleObject(pDevice->hGptIntrEvent, timeout)) == WAIT_OBJECT_0)
        {
            //DEBUGMSG (ZONE_INFO, (TEXT("%s: Interrupt received\r\n"), __WFUNCTION__));
            if (pDevice->hTimerEvent && Triggered)
            {
                // Trigger timer event
                SetEvent(pDevice->hTimerEvent);
				Triggered = 0;
            }
        }
        else if (rc == WAIT_TIMEOUT)
        {
            // Timeout as requested
            DEBUGMSG (ZONE_INFO, (TEXT("%s: Time out\r\n"), __WFUNCTION__));
			//RETAILMSG(1,(L"Timer timeout\r\n"));
        }
        else
        {
            //Abnormal signal
            rc = FALSE;
			//RETAILMSG(1,(L"Timer abnormal\r\n"));
            break;
        }
        
		// Set new compare value for Compare mode
		//GptUpdateTimerPeriod(pDevice, pDevice->Period);

		// Clear interrupt status bits
		OUTREG32(&pDevice->pGPT->TISR, GPTIMER_TIER_OVERFLOW);
		// Kernel call to unmask the interrupt so that it can be signalled again
		InterruptDone(pDevice->dwGptIntr);

#if DEBUG_TIMER_GPIO
		if(Triggered)
		{
			BitState = ~BitState & 0x1;
			if(BitState == 0)
			{
				SETREG32(p, 1 << Bit);
			}
			else
			{
				CLRREG32(p, 1 << Bit);
			}
		}
#endif
    }

    return rc;
}

//------------------------------------------------------------------------------
//
// Function: GptResetTimer
//
// This function is used to reset GPT module
//
// Parameters:
//      pDevice
//          [in] Pointer to GPT structure
//
// Returns:
//      None
//
//------------------------------------------------------------------------------
BOOL GptResetTimer(POMAP_GPT_STRUCT pDevice)
{
    EnterCriticalSection(&pDevice->hGptLock);

    // Soft reset GPTIMER
    GptSetReg(pDevice, &pDevice->pGPT->TIOCP, SYSCONFIG_SOFTRESET);

    // While until done
    while ((GptGetReg(pDevice, &pDevice->pGPT->TISTAT) & GPTIMER_TISTAT_RESETDONE) == 0);

    LeaveCriticalSection(&pDevice->hGptLock);

    return TRUE;
}


//------------------------------------------------------------------------------
//
// Function: GptGetTimerCount
//
// This function is used to obtain the GPT mian counter value.
//
// Parameters:
//      pDevice
//          [in] Pointer to GPT structure
//      pGptCntValue
//          [out] 32 bit value of GPT main counter value
//
// Returns:
//      TRUE - Returns true always.
//
//
//------------------------------------------------------------------------------
BOOL GptGetTimerCount(POMAP_GPT_STRUCT pDevice, DWORD *pGptCntValue)
{
	*pGptCntValue = GptGetReg(pDevice, &pDevice->pGPT->TCRR);
	
    return TRUE;
}


//------------------------------------------------------------------------------
//
//  Function:  GptGetTickCount
//
//  This function returns number of 1 ms ticks which elapsed since system boot
//  or reset (absolute value isn't important). The counter can overflow but
//  overflow period should not be shorter then approx 30 seconds. Function
//  is used in  system boot so it must work before interrupt subsystem
//  is active.
//
DWORD GptGetMsTickCount(POMAP_GPT_STRUCT pDevice)
{
    UINT64 tickCount = GptGetReg(pDevice, &pDevice->pGPT->TCRR);
    //  Returns number of 1 msec ticks
    return (UINT32) TICK_TO_MSEC(tickCount);
}

//------------------------------------------------------------------------------
//
// Function: GptUpdateTimerPeriod
//
// This function is used to set the timer for specified period delay.
//
// Parameters:
//      pDevice
//          [in] Pointer to GPT structure
//      Period
//          [in] This parameter is the period in microseconds.
//
// Returns:
//      TRUE - returns TRUE always.
//
//
//------------------------------------------------------------------------------
BOOL GptUpdateTimerPeriod(POMAP_GPT_STRUCT pDevice, DWORD PeriodMSec)
{
    //UINT32 match;
    //INT32 delta;
	DWORD uSourceFreq = (32 * 1024);	//32,768 Hz
    DWORD tickCount;

    if(PeriodMSec == 1)
		return FALSE;

	EnterCriticalSection(&pDevice->hGptLock);    

#if 0    
	pDevice->period = PeriodMSec;
	GptGetTimerCount(pDevice, &tickCount);
	
	// This is compare value
    match = tickCount + MSEC_TO_TICK(PeriodMSec);

    delta = (INT32)(tickCount - match);

    // If we are behind, issue interrupt as soon as possible
    if (delta > 0)
    {
        match += MSEC_TO_TICK(1);
    }

    // Set timer match value
    GptTimerSetCompare(pDevice, match);
            
    DEBUGMSG (ZONE_INFO, (TEXT("%s: CurrCount: 0x%x, CompVal: 0x%x\r\n"), 
                          __WFUNCTION__, tickCount, match));
#endif

	tickCount = ((uSourceFreq * PeriodMSec) / 1000);
	tickCount = 0xffffffff - tickCount;
	GptSetReg(pDevice, &pDevice->pGPT->TLDR, tickCount);
	GptSetReg(pDevice, &pDevice->pGPT->TCRR, tickCount);

    LeaveCriticalSection(&pDevice->hGptLock);

    return TRUE;
}


//------------------------------------------------------------------------------
//
// Function: GPT_Init
//
// The Device Manager calls this function as a result of a call to the
// ActivateDevice() function.
//
// Parameters:
//      pContext
//          [in] Pointer to a string containing the registry path to the
//          active key for the stream interface driver.
//
// Returns:
//      Returns a handle to the device context created if successful. Returns
//      zero if not successful.
//
//------------------------------------------------------------------------------
DWORD GPT_Init(LPCTSTR pContext)
{
    DWORD dwRet = 0;
#if DEBUG_TIMER_AUTORUN	
	POMAP_GPT_STRUCT pTimerDevice;
#endif
    
    RETAILMSG(DEBUG_TIMER_PRINT,(L"GPT_Init+, %s\r\n", pContext));

    dwRet = GptInitialize(pContext);
    
#if DEBUG_TIMER_AUTORUN	
	if(!dwRet)
	{
		RETAILMSG(DEBUG_TIMER_PRINT,(L"GPT_Init+, %s\r\n", pContext));
	}
	else
	{
		GPT_Open((dwRet, 0, 0);
		pTimerDevice = (POMAP_GPT_STRUCT)dwRet;
		GptTimerStart(pTimerDevice);
	}
#endif

	RETAILMSG(DEBUG_TIMER_PRINT,(L"GPT_Init-\r\n"));

    return dwRet;

}


//------------------------------------------------------------------------------
//
// Function: GPT_Deinit
//
// This function uninitializes a device.
//
// Parameters:
//      hDeviceContext
//          [in] Handle to the device context.
//
// Returns:
//      TRUE indicates success. FALSE indicates failure.
//
//------------------------------------------------------------------------------
BOOL GPT_Deinit(DWORD hDeviceContext)
{
    GptRelease((POMAP_GPT_STRUCT)hDeviceContext);

    return TRUE;
}


//------------------------------------------------------------------------------
//
// Function: GPT_Open
//
// This function opens a device for reading, writing, or both.
//
// Parameters:
//      hDeviceContext
//          [in] Handle to the device context. The XXX_Init function creates
//          and returns this handle.
//
//      AccessCode
//          [in] Access code for the device. The access is a combination of
//          read and write access from CreateFile.
//
//      ShareMode
//          [in] File share mode of the device. The share mode is a
//          combination of read and write access sharing from CreateFile.
//
// Returns:
//      This function returns a handle that identifies the open context of
//      the device to the calling application.
//
//------------------------------------------------------------------------------
DWORD GPT_Open(DWORD hDeviceContext, DWORD AccessCode, DWORD ShareMode)
{
    RETAILMSG(DEBUG_TIMER_PRINT,(L"GPT_Open+\r\n"));
    // Remove-W4: Warning C4100 workaround
    UNREFERENCED_PARAMETER(AccessCode);
    UNREFERENCED_PARAMETER(ShareMode);

    if (((POMAP_GPT_STRUCT)hDeviceContext)->bCurrentlyOwned)
    {
        return 0;
    }
    else
    {
        ((POMAP_GPT_STRUCT)hDeviceContext)->bCurrentlyOwned = TRUE;
    }

	RETAILMSG(DEBUG_TIMER_PRINT,(L"GPT_Open-\r\n"));
    return hDeviceContext;
}


//------------------------------------------------------------------------------
//
// Function: GPT_Close
//
// This function closes the GPT for reading and writing.
//
// Parameters:
//      hOpenContext
//          [in] Handle returned by the XXX_Open function, used to identify
//          the open context of the device.
//
// Returns:
//      TRUE indicates success. FALSE indicates failure.
//
//------------------------------------------------------------------------------
BOOL GPT_Close(DWORD hDeviceContext)
{
    ((POMAP_GPT_STRUCT)hDeviceContext)->bCurrentlyOwned = FALSE;

    GptTimerStop((POMAP_GPT_STRUCT)hDeviceContext);
   
    return TRUE;
}


//------------------------------------------------------------------------------
//
// Function: GPT_IOControl
//
// This function sends a command to a device.
//
// Parameters:
//      hOpenContext
//          [in] Handle to the open context of the device. The XXX_Open
//          function creates and returns this identifier.
//
//      dwCode
//          [in] I/O control operation to perform. These codes are
//          device-specific and are usually exposed to developers through
//          a header file.
//
//      pBufIn
//          [in] Pointer to the buffer containing data to transfer to the
//          device.
//
//      dwLenIn
//          [in] Number of bytes of data in the buffer specified for pBufIn.
//
//      pBufOut
//          [out] Pointer to the buffer used to transfer the output data
//          from the device.
//
//      dwLenOut
//          [in] Maximum number of bytes in the buffer specified by pBufOut.
//
//      pdwActualOut
//          [out] Pointer to the DWORD buffer that this function uses to
//          return the actual number of bytes received from the device.
//
// Returns:
//      The new data pointer for the device indicates success. A value of -1
//      indicates failure.
//
//------------------------------------------------------------------------------
BOOL GPT_IOControl(DWORD hOpenContext, DWORD dwCode, PBYTE pBufIn,
                   DWORD dwLenIn, PBYTE pBufOut, DWORD dwLenOut,
                   PDWORD pdwActualOut)
{
    BOOL bRet = FALSE;
    LPTSTR pStr = NULL;
    PDWORD pGptPeriod;
    PDWORD pGptCount;
	PDWORD pTimerMode;
	PDWORD pEventRate;
	POMAP_GPT_STRUCT pTimerContext = (POMAP_GPT_STRUCT)hOpenContext;
  
    // Remove-W4: Warning C4100 workaround
    UNREFERENCED_PARAMETER(pdwActualOut);
    UNREFERENCED_PARAMETER(dwLenOut);
    UNREFERENCED_PARAMETER(pBufOut);

    switch(dwCode)
    {
    case GPT_IOCTL_TIMER_CREATE_EVENT:
        pStr = (LPTSTR)pBufIn;
        if(pStr)
			bRet = GptTimerCreateEvent(pTimerContext, pStr);
		else
			RETAILMSG(DEBUG_TIMER_PRINT, (TEXT("GPT_IOControl: GPT_IOCTL_TIMER_CREATE_EVENT failed\r\n")));
        DEBUGMSG(ZONE_IOCTL, (TEXT("GPT_IOControl: GPT_IOCTL_TIMER_CREATE_EVENT occurred\r\n")));
        break;

    case GPT_IOCTL_TIMER_RELEASE_EVENT:
        pStr = (LPTSTR)pBufIn;
        if(pStr)
		{
			bRet = GptTimerReleaseEvent(pTimerContext, pStr);
		}
		else
			RETAILMSG(DEBUG_TIMER_PRINT, (TEXT("GPT_IOControl: GPT_IOCTL_TIMER_RELEASE_EVENT failed\r\n")));
        DEBUGMSG(ZONE_IOCTL, (TEXT("GPT_IOControl: GPT_IOCTL_TIMER_CLOSE_HANDLE occurred\r\n")));
        break;

    case GPT_IOCTL_TIMER_START:
    {
        GptTimerStart(pTimerContext);              
        bRet = TRUE;
        DEBUGMSG(ZONE_IOCTL, (TEXT("GPT_IOControl: GPT_IOCTL_TIMER_START occurred\r\n")));
        break;
    }

    case GPT_IOCTL_TIMER_STOP:
    {
        GptTimerStop(pTimerContext);       
        bRet = TRUE;
        DEBUGMSG(ZONE_IOCTL, (TEXT("GPT_IOControl: GPT_IOCTL_TIMER_STOP occurred\r\n")));
        break;
    }           

    case GPT_IOCTL_TIMER_UPDATE_PERIOD:
    {
        if (dwLenIn != sizeof(DWORD))
            return FALSE;
        // access check input buffer
        pGptPeriod = (PDWORD)pBufIn;
        if(*pGptPeriod > MAX_PERIOD_MS)
			return FALSE;

		// update compare reg1 
        bRet = GptUpdateTimerPeriod(pTimerContext, *pGptPeriod);
        if(bRet)
			pTimerContext->Period = *pGptPeriod;
		DEBUGMSG(ZONE_IOCTL, (TEXT("GPT_IOControl: GPT_IOCTL_TIMER_UPDATE_PERIOD occurred\r\n")));
        break;
    }            

    case GPT_IOCTL_TIMER_SET_MODE:
    {
        if (dwLenIn != sizeof(DWORD))
            return FALSE;
        // access check input buffer
        pTimerMode = (PDWORD)pBufIn;
		pTimerContext->Mode = *pTimerMode;
        bRet = TRUE;
        DEBUGMSG(ZONE_IOCTL, (TEXT("GPT_IOControl: GPT_IOCTL_TIMER_SET_MODE occurred\r\n")));
        break;
    }

	case GPT_IOCTL_TIMER_GET_COUNT:
    {
        if (dwLenOut != sizeof(DWORD))
            return FALSE;
        // access check input buffer
        pGptCount = (PDWORD)pBufOut;
        // get GPT main counter value  
        bRet = GptGetTimerCount(pTimerContext, pGptCount);
        DEBUGMSG(ZONE_IOCTL, (TEXT("GPT_IOControl: GPT_IOCTL_TIMER_UPDATE_PERIOD occurred\r\n")));
        break;
    }

	case GPT_IOCTL_TIMER_SET_EVENT_RATE:
	{
		if (dwLenIn != sizeof(DWORD))
            return FALSE;
		// access check input buffer
        pEventRate = (PDWORD)pBufIn;
		AppEventRate = *pEventRate;
        bRet = TRUE;
        DEBUGMSG(ZONE_IOCTL, (TEXT("GPT_IOControl: GPT_IOCTL_TIMER_SET_EVENT_RATE occurred\r\n")));
        break;
    }

    default:
        DEBUGMSG(ZONE_WARN, (TEXT("GPT_IOControl: No matching IOCTL.\r\n")));
        break;
    }

    return bRet;
}

BOOL WINAPI DllEntry(HANDLE hInstDll, DWORD dwReason, LPVOID lpvReserved)
{
    // Remove-W4: Warning C4100 workaround
    UNREFERENCED_PARAMETER(lpvReserved);

    switch (dwReason)
    {
    case DLL_PROCESS_ATTACH:
        //Register Debug Zones
        DEBUGREGISTER((HINSTANCE) hInstDll);
        DEBUGMSG(ZONE_INFO, (TEXT("GPT_DllEntry: DLL_PROCESS_ATTACH lpvReserved(0x%x)\r\n"),lpvReserved));
        DisableThreadLibraryCalls((HMODULE) hInstDll);
        break;

    case DLL_PROCESS_DETACH:
        DEBUGMSG(ZONE_INFO, (TEXT("GPT_DllEntry: DLL_PROCESS_DETACH lpvReserved(0x%x)\r\n"),lpvReserved));
        break;
    }
    // Return TRUE for success
    return TRUE;
}
//------------------------------------------------------------------------------
