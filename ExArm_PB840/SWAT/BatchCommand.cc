#include "stdafx.h"
//-------------------------------------------------------------------------------
//                        Copyright (c) 2014 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//-------------------------------------------------------------------------------
#include "BatchCommandInfo.h"
#include "BatchCommand.h"
#include "SocketWrap.hh"

namespace swat
{
    ////////////////////////////////////////////////////////////////////////////
    /// BatchCommandInfo Implementation
    void BatchCommandInfo::convHtoN()
    {
        SocketWrap sockWrap;
        // UUID does not require bytes order convert
        iNCMD = sockWrap.HTONL(iNCMD);
    }

    void BatchCommandInfo::convNtoH()
    {
        SocketWrap sockWrap;
        // UUID does not require bytes order convert
        iNCMD = sockWrap.NTOHL(iNCMD);
    }

    ////////////////////////////////////////////////////////////////////////////
    /// BatchCommand Implementation

    void BatchCommand::convHtoN()
    {
        SocketWrap sockWrap;

        m_CommandId = sockWrap.HTONL(m_CommandId);
        int argArraySize = sizeof(m_Arg) / sizeof(m_Arg[0]);
        for ( int j = 0; j < argArraySize; ++ j )
        {
            m_Arg[j].ui = sockWrap.HTONL(m_Arg[j].ui);
        }
    }

    void BatchCommand::convNtoH()
    {
        SocketWrap sockWrap;

        m_CommandId = sockWrap.NTOHL(m_CommandId);
        int argArraySize = sizeof(m_Arg) / sizeof(m_Arg[0]);
        for ( int j = 0; j < argArraySize; ++ j )
        {
            m_Arg[j].ui = sockWrap.NTOHL(m_Arg[j].ui);
        }
    }

}