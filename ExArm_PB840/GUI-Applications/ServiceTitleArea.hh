#ifndef ServiceTitleArea_HH
#define ServiceTitleArea_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1994, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: ServiceTitleArea - A area at the top of the Upper screen used
// for displaying current system status, and date/time.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/ServiceTitleArea.hhv   25.0.4.0   19 Nov 2013 14:08:26   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003   By: srp   Date:  24-Apr-2002    DR Number: 5327 & 5908
//  Project:  NeoMode
//  Description:
//      Added Modification log. Updated for NeoMode  03/01/00
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  wks    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "Container.hh"
#include "GuiTimerTarget.hh"

//@ Usage-Classes
#include "Line.hh"
#include "TextField.hh"
#include "GuiAppClassIds.hh"
#include "NovRamEventTarget.hh"
#include "NovRamUpdateManager.hh"
#include "Notification.hh"
#include "ContextObserver.hh"
#include "ContextSubject.hh"
class TimeStamp;
//@ End-Usage


class ServiceTitleArea : public Container, public NovRamEventTarget,
						 public GuiTimerTarget,
						 public ContextObserver
{
public:
	ServiceTitleArea(void);
	~ServiceTitleArea(void);

	//@ Type: ServiceTitleMode
	// Identifies the current/new display mode for this area.
	enum ServiceTitleMode
	{
		SERVICE_NOT_START_MODE = -1,
		SST_MODE,
		SERVICE_MODE,
		SST_MODE_COMM_DOWN,
		SERVICE_MODE_COMM_DOWN,
		SERVICE_MAX_MODE
	};

	void setDisplayMode(ServiceTitleMode displayMode);

	virtual void timerEventHappened(GuiTimerId::GuiTimerIdType timerId);

	// Inherited from NovRamEventTarget
	virtual void novRamUpdateEventHappened(NovRamUpdateManager::UpdateTypeId
																	updateId);

	static void SoftFault(const SoftFaultID softFaultID,
								 const Uint32      lineNumber,
								 const char*       pFileName  = NULL, 
								 const char*       pPredicate = NULL);

	// ContextObserver virtual method...
	virtual void  batchSettingUpdate(
						 const Notification::ChangeQualifier qualifierId,
						 const ContextSubject*               pSubject,
						 const SettingId::SettingIdType      settingId
									);
protected:

private:
	// these methods are purposely declared, but not implemented...
	ServiceTitleArea(const ServiceTitleArea&);	// not implemented...
	void operator=(const ServiceTitleArea&);		// not implemented...

	void updateHumidTypeDisplay_();
	void updatePtCctTypeDisplay_();

	//@ Data-Member: displayText_
	// The label for currently display mode.
	TextField displayText_;

	//@ Data-Member: displayMode_
	// The currently display mode ID.
	ServiceTitleMode displayMode_;

	//@ Data-Member: dateTimeText_
	// Textfield to hold the current date/time.
	TextField dateTimeText_;

	//@ Data-Member: dividingLine_
	// Divide line to separate the displayMode text and dateTime text.
	Line dividingLine_;

	//@ Data-Member: circuitTypePrompt_
	// Text that says "Circuit Type:".
	TextField circuitTypePrompt_;

	//@ Data-Member: circuitTypeText_
	// Text that displays the current circuit type.
	TextField circuitTypeText_;

	//@ Data-Member: humidificationTypePrompt_
	// Text that says "Humidification Type:".
	TextField humidificationTypePrompt_;

	//@ Data-Member: humidificationTypeText_
	// Text that displays the current humidification type.
	TextField humidificationTypeText_;


};

#endif // ServiceTitleArea_HH 
