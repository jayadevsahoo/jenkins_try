#ifndef TrendTimeScaleSetting_HH
#define TrendTimeScaleSetting_HH

//====================================================================
//
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  TrendTimeScaleSetting - Trend Time Scale Setting
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/TrendTimeScaleSetting.hhv   25.0.4.0   19 Nov 2013 14:27:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001   By: gdc   Date:  10-Jan-2007    SCR Number: 6237
//  Project:  Trend
//  Description:
//      Initial version.
//====================================================================

//@ Usage-Classes
#include "NonBatchDiscreteSetting.hh"
//@ End-Usage


class TrendTimeScaleSetting : public NonBatchDiscreteSetting
{
public:
	TrendTimeScaleSetting(void);
	virtual ~TrendTimeScaleSetting(void);

	virtual Applicability::Id  getApplicability(const Notification::ChangeQualifier qualifierId) const;

	virtual SettingValue  getDefaultValue(void) const;

	static void  SoftFault(const SoftFaultID softFaultID,
						   const Uint32      lineNumber,
						   const char*       pFileName  = NULL, 
						   const char*       pPredicate = NULL);

private:
	TrendTimeScaleSetting(const TrendTimeScaleSetting&); // not implemented...
	void  operator=(const TrendTimeScaleSetting&);	// not implemented...
};


#endif // TrendTimeScaleSetting_HH 
