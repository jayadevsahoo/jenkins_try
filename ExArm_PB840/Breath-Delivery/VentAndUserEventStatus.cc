#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: VentAndUserEventStatus - sends ventilator and user event
//  status to the GUI via Network Apps
//---------------------------------------------------------------------
//@ Interface-Description
// Ventilator status includes things like entry to/exit from Apnea Ventilation,
// entry to /exit from SVO, compressor ready.  This class provides the
// call back mechanism for BD-IO-Devices to report status that it monitors
// (eg. EventFilter and Battery).  This class also provides the method
// that is called directly from Breath-Delivery to report status such as
// Apnea Ventilation active and User Event (such as Manual Inspiration
// and expiratory pause) status.  The method PostEventStatus() is invoked
// throughout breath delivery subsystem to post user and ventilator event status.
// This method posts the BD_EVENT_STATUS_EVENT to the BD_STATUS_TASK_Q.
// When the BdStatusTask runs, it pulls this event off the queue and
// invokes the method SendStatusToGui() which is responsible
// for building the bdToGuiEvent message and transmitting it to the GUI
// via NetworkApp::XmitMsg().
//
// The method SendStatusToGui() is not invoked directly from the BD main
// task to prevent that task from ever being blocked by the mutex used
// in NetworkApp.
//---------------------------------------------------------------------
//@ Rationale
// This class provides a common interface with the BdStatusTask and the
// GUI for reporting ventilator and user status.  This class is also
// responsible for synchronizing the user and ventilator status with the
// GUI when communcations is restored.
//---------------------------------------------------------------------
//@ Implementation-Description
// this class provide the following methods:
//   - PostEventStatus() provides the interface between BD and BD-IO-Devices 
//     for reporting ventilator status.  
//   - SendStatusToGui() is invoked from the BdStatusTask and builds the 
//     message and transmits it via NetworkApp.
//   - ResyncEventStatus() to resynch the event status between BD and
//     GUI when the communication is just established.
//   - Initialize() initializes data members and registers for call back.
//   - SetEventStatusCommState() set data member EventStatusCommState_.
//---------------------------------------------------------------------
//@ Fault-Handling
// n/a
//---------------------------------------------------------------------
//@ Restrictions
// This is a static class; it is not to be instantiated.
//---------------------------------------------------------------------
//@ Invariants
// None
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/VentAndUserEventStatus.ccv   25.0.4.0   19 Nov 2013 14:00:16   pvcs  $
//
//@ Modification-Log
//
//  Revision: 007   By: gdc   Date:  22-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Added RM event status.
//
//  Revision: 006  By:  syw    Date:  28-Jul-1999    DR Number: 5416
//       Project:  840
//       Description:
//          Eliminate callbacks to Compressor class since they
//			will interface with the EventFilter class directly.
//
//  Revision: 005  By: yyy     Date:  08-Jul-1999    DR Number: 5458
//  Project:  ATC
//  Description:
//      Reset the status for inspiratory or expiratory during comm down when
//		ResyncEventStatus() is called.
//
//  Revision: 004  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 003  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 002 By: sp   Date:   06-Mar-1996    DR Number: DCS NONE
//       Project:  Sigma (840)
//       Description:
//             Inspection rework.
//
//  Revision: 001  By:  kam    Date:  30-Oct-1995    DR Number: DCS NONE
//       Project:  Sigma (840)
//       Description:
//             Initial release.
//
//=====================================================================

#include "VentAndUserEventStatus.hh"

#include "BdQueuesMsg.hh"
#include "VentObjectRefs.hh"

//@ Usage-Classes
#include "MsgQueue.hh"
#include "IpcIds.hh"
#include "SystemBattery.hh"
#include "EventFilter.hh"
#include "BDIORefs.hh"
#include "VentStatus.hh"
#include "NetworkApp.hh"

#include <memory.h>

//@ End-Usage

//@ Code...
EventData::EventStatus VentAndUserEventStatus::EventStatus_[NUM_EVENT_IDS];
BdGuiCommSync::CommState VentAndUserEventStatus::EventStatusCommState_;

//=====================================================================
//
//  //@ Data-Member: Methods..
//
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Initialize
//
//@ Interface-Description
//	This method takes no argument and returns nothing.
//      This method registers call backs with the following objects for
//      Ventilator Status updates:  EventFilter, Battery and VentStatus.
//---------------------------------------------------------------------
//@ Implementation-Description
//      Calls are made to the registerVentStatusCallBack() methods of the
//      BD-IO-Devices objects that detect and notify of ventilator status.
//	EventStatus_ array is initialize to EventData::IDLE.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void	
VentAndUserEventStatus::Initialize(void)
{
    CALL_TRACE("VentAndUserEventStatus::Initialize(void)");

    VentAndUserEventStatus::EventStatusCommState_ = BdGuiCommSync::COMMS_DOWN;

    // Init the status array
    for (int ii = 0; ii < NUM_EVENT_IDS; ii++ )
    {
        // $[TI1]
        VentAndUserEventStatus::EventStatus_[ii] = EventData::IDLE;
    }

    // Register callbacks with the BD-IO-Devices objects that notify
    // this object of ventilator status to be sent to the GUI
    REventFilter.registerVentStatusCallBack(VentAndUserEventStatus::PostEventStatus);
    RVentStatus.registerVentStatusCallBack(VentAndUserEventStatus::PostEventStatus);
    RSystemBattery.registerVentStatusCallBack(VentAndUserEventStatus::PostEventStatus);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ResyncEventStatus
//
//@ Interface-Description
//    This method takes no input parameters and returns nothing.  It is
//    invoked from the BdStatusTask when communications are restored.
//    It looks through the static data member EventStatus_; all user event
//    status is sent to the GUI by calling NetworkApp::XmitMsg().  All
//    ventilator status that is not IDLE is sent to the GUI by
//    calling NetworkApp::XmitMsg().
//---------------------------------------------------------------------
//@ Implementation-Description
//    This method cycles through the EventStatus_ member array and
//    communicates the status of each user event to the GUI.  It also
//    sends each ventilator status that is not idle to the GUI.
//    It then sets the EventStatusCommState_ data member to COMMS_UP so
//    that subsequent event status is communicated to the GUI as it
//    occurs.
//---------------------------------------------------------------------
//@ PreCondition
//    None
//---------------------------------------------------------------------
//@ PostCondition
//    None
//@ End-Method
//=====================================================================
void
VentAndUserEventStatus::ResyncEventStatus(void)
{

    // $[TI1]
    
    CALL_TRACE("VentAndUserEventStatus::ResyncEventStatus(void)");
                
    // Since BD Main task communicates the ventilator and user event
    // status through the BD Status Task, the initialization performed
    // here will not be corrupted by the sending of new status.  Even
    // if the BD main task posts the new status, the BD status task
    // will complete this method before pulling the next status message
    // off of the queue

    EventData::EventStatus statusMsg[NUM_EVENT_IDS];

    VentAndUserEventStatus::EventStatus_[EventData::EXPIRATORY_PAUSE] = EventData::IDLE;
    VentAndUserEventStatus::EventStatus_[EventData::INSPIRATORY_PAUSE] = EventData::IDLE;
    VentAndUserEventStatus::EventStatus_[EventData::NIF_MANEUVER] = EventData::IDLE;
    VentAndUserEventStatus::EventStatus_[EventData::P100_MANEUVER] = EventData::IDLE;
    VentAndUserEventStatus::EventStatus_[EventData::VITAL_CAPACITY_MANEUVER] = EventData::IDLE;

    memcpy (statusMsg, EventStatus_, sizeof(EventStatus_));
	//This is an internal network xmit between BD and GUI. If the receiving
	//procesor has a diferent endianness than the sender, endianness compatibility
	//should be provided by converting statusMsg endianness to network. and the
	//receiver needs to convert it back to host
    NetworkApp::XmitMsg (ALL_EVENT_STATUS_MSG, statusMsg, sizeof(statusMsg));

    // Reset the indicator so that event status will be sent to the GUI as changes
    // occur
    VentAndUserEventStatus::EventStatusCommState_ = BdGuiCommSync::COMMS_UP;

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SendStatusToGui
//
//@ Interface-Description
//    This method takes an event id and status as input parameters and
//    returns nothing.  It builds the bdToGuiEvent message and transmits
//    it via Network Apps.  This method is invoked from the BdStatusTask
//    when the BD_EVENT_STATUS_EVENT is received on the BD_STATUS_TASK_Q.
//---------------------------------------------------------------------
//@ Implementation-Description
//    The status and event id are placed in a bdToGuiEvent messge.
//    NetworkApp::XmitMsg() is then invoked to transmit the message.
//---------------------------------------------------------------------
//@ PreCondition
//    The event ID is checked to be a valid user or vent status id
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

void
VentAndUserEventStatus::SendStatusToGui (const EventData::EventId id,
                                  		 const EventData::EventStatus status,
                                 		 const EventData::EventPrompt prompt)
{
    CALL_TRACE("VentAndUserEventStatus::SendStatusToGui(const EventData::EventId id, \
                const EventData::EventStatus status)");

    CLASS_PRE_CONDITION (id >= EventData::START_OF_EVENT_IDS &&
                         id <= EventData::END_OF_EVENT_IDS)


    // Update the status in the static array whether or not the message
    // is transmitted to the GUI.  This array is used to resync the vent
    // status when communications is restored.
    VentAndUserEventStatus::EventStatus_[id] = status;

    // Only send the Vent Status msg to the GUI if communications if up
    if (VentAndUserEventStatus::EventStatusCommState_ == BdGuiCommSync::COMMS_UP)
    {
        EventData msg;

        // $[TI1]
        msg.bdToGuiEvent.id = id;
        msg.bdToGuiEvent.status = status;
        msg.bdToGuiEvent.prompt = prompt;
		//This is an internal network xmit between BD and GUI. If the receiving
		//procesor has a diferent endianness than the sender, endianness compatibility
		//should be provided by converting msg endianness to network. and the
		//receiver needs to convert it back to host
        NetworkApp::XmitMsg (EVENT_STATUS_MSG, &msg, sizeof(msg));

    }
    // $[TI2]
       
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: PostEventStatus
//
//@ Interface-Description
//
//    This method takes an event id and status as input parameters and
//    returns nothing.  This method is invoked by the
//    BD main task from the BD-IO-Devices subsystem via the callback
//    mechanism and directly from the Breath-Delivery subsystem.
//
//---------------------------------------------------------------------
//@ Implementation-Description
//    This method builds a BD_EVENT_STATUS_EVENT message and puts the message
//    BD_STATUS_TASK_Q so that the BdStatusTask will send the event
//    status to the GUI.
//---------------------------------------------------------------------
//@ PreCondition
//    The event ID is a valid user event or ventilator event status id.
//    Resturn status of MsgQueue::PutMsg is Ipc::OK.
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

void
VentAndUserEventStatus::PostEventStatus (const EventData::EventId id,
                                  const EventData::EventStatus status,
                                  const EventData::EventPrompt prompt)
{

    BdQueuesMsg msg;
    
    // $[TI1]
    CALL_TRACE("VentAndUserEventStatus::PostEventStatus(const EventData::EventId id, \
                const EventData::EventStatus status, const EventData::EventPrompt prompt)");

    CLASS_PRE_CONDITION (id >= EventData::START_OF_EVENT_IDS &&
                         id <= EventData::END_OF_EVENT_IDS)

    // Post the vent status to the Bd Status Task, which is responsible for
    // communicating it to the GUI.
    msg.eventStatusEvent.eventType = BdQueuesMsg::BD_EVENT_STATUS_EVENT;
    msg.eventStatusEvent.id = id;
    msg.eventStatusEvent.status = status;
    msg.eventStatusEvent.prompt = prompt;

    // Put the event on the BD_STATUS_TASK_Q.  The BdStatusTask will take
    // it off of the queue and send it to the GUI via NetworkApps.        
    CLASS_ASSERTION (MsgQueue::PutMsg (BD_STATUS_TASK_Q, (Int32)msg.qWord) == Ipc::OK);
    
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
VentAndUserEventStatus::SoftFault(const SoftFaultID  softFaultID,
                           const Uint32       lineNumber,
                           const char*        pFileName,
                           const char*        pPredicate)
{
    CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
    FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, VENTANDUSEREVENTSTATUS,
                            lineNumber, pFileName, pPredicate);
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================



//=====================================================================
//
//  Private Methods...
//
//=====================================================================

