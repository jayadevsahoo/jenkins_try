#ifndef GeneralElectronicsTest_HH
#define GeneralElectronicsTest_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: GeneralElectronicsTest - Read and display the analog data channels
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/GeneralElectronicsTest.hhv   25.0.4.0   19 Nov 2013 14:23:40   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 002  By:  quf    Date: 22-May-1997    DR Number: DCS 2131
//       Project:  Sigma (840)
//       Description:
//            Modification-Log fix only.
//
//  Revision: 001  By:  mad    Date:  02-Nov-1994    DR Number: none
//	Project:  Sigma (R8027)
//	Description:
//		Initial version (Integration baseline).
//
//====================================================================

#include "ServiceMode.hh"

//@ Usage-Classes

#include "Sensor.hh"

//@ End-Usage

//@ Constant: NUM_CHANNELS
// number of analog data channels
const Int32 NUM_CHANNELS = 33;

class GeneralElectronicsTest
{
  public:
	GeneralElectronicsTest(void);
	~GeneralElectronicsTest(void);

	static void SoftFault(const SoftFaultID softFaultID,
		  const Uint32      lineNumber,
		  const char*       pFileName  = NULL, 
		  const char*       pPredicate = NULL);
  
	void runTest(void);

  protected:

  private:
    GeneralElectronicsTest(const GeneralElectronicsTest&);	// not implemented...
    void   operator=(const GeneralElectronicsTest&);		// not implemented...

    //@ Data-Member: pSensors_[]
    // Array of pointers to sensor objects
    Sensor *pSensors_[NUM_CHANNELS] ;
};


#endif // GeneralElectronicsTest_HH 
