// LungSetupDlg.cpp : implementation file
//

#include "stdafx.h"
#include "LungSetupDlg.h"


// CLungSetupDlg dialog

IMPLEMENT_DYNAMIC(CLungSetupDlg, CDialog)

CLungSetupDlg::CLungSetupDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CLungSetupDlg::IDD, pParent)
	, m_strLungsetup(_T(""))
{

}

CLungSetupDlg::~CLungSetupDlg()
{
}

void CLungSetupDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, m_strLungsetup);
}


BEGIN_MESSAGE_MAP(CLungSetupDlg, CDialog)
	ON_WM_CLOSE()
END_MESSAGE_MAP()


// CLungSetupDlg message handlers

BOOL CLungSetupDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  Add extra initialization here

	SetWindowPos(&CWnd::wndTopMost, 0, 0, 0, 0,
      SWP_NOMOVE|SWP_NOSIZE|SWP_NOACTIVATE);

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CLungSetupDlg::OnClose()
{
	// TODO: Add your message handler code here and/or call default
	
	CDialog::OnClose();
}
