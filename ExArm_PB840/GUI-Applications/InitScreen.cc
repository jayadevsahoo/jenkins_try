#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: InitScreen - Screen displayed on the Upper Screen
// when initializing the GUI, before online or service mode has been
// entered.
//---------------------------------------------------------------------
//@ Interface-Description
// This class is a simple container for a text message that indicates to
// the operator that the GUI is currently initializing.  An instance
// of this class is displayed in the Upper screen on initialization.
//
// Once initialized correctly the GUI goes to online or service mode and the
// InitScreen object is removed from the display.
//---------------------------------------------------------------------
//@ Rationale
// A message was needed to indicate that the GUI was initializing and
// this class provides that.
//---------------------------------------------------------------------
//@ Implementation-Description
// InitScreen is a container that contains a single TextField object which
// is used to display the initialization text message.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/InitScreen.ccv   25.0.4.0   19 Nov 2013 14:07:56   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 003  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 002  By: yyy      Date: 15-May-1997  DR Number: 2110
//    Project:  Sigma (R8027)
//    Description:
//      Added SRS 11032 mapping.
//
//  Revision: 001  By:  mpm    Date:  25-AUG-95    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "InitScreen.hh"

//@ Usage-Classes
#include "MiscStrs.hh"
//@ End-Usage

//@ Code...

// Initialize static constants.
static const Int32 CONTAINER_WIDTH_ = 640;
static const Int32 CONTAINER_HEIGHT_ = 480;
static const Int32 MESSAGE_Y_ = 200;

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: InitScreen()  [Default Constructor]
//
//@ Interface-Description
// Default constructor which formulates the display of InitScreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Size the InitScreen container and add the initialization message text to our
// container.
// $[11032] An indication that the system is initializing shall be displayed ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

InitScreen::InitScreen(void) :
				initializeText_(MiscStrs::INIT_SCREEN_MESSAGE)
{
	CALL_TRACE("InitScreen::InitScreen(void)");

	// Size container
	setWidth(CONTAINER_WIDTH_);
	setHeight(CONTAINER_HEIGHT_);
	setFillColor(Colors::MEDIUM_BLUE);

	// Color and add the initialize text message.
	initializeText_.setColor(Colors::WHITE);
	addDrawable(&initializeText_);				
	initializeText_.setY(MESSAGE_Y_);
	initializeText_.positionInContainer(GRAVITY_CENTER);		// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~InitScreen()  [Destructor]
//
//@ Interface-Description
// The destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

InitScreen::~InitScreen(void)
{
	CALL_TRACE("InitScreen::~InitScreen(void)");

	// Do nothing
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
InitScreen::SoftFault(const SoftFaultID  softFaultID,
		   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, INITSCREEN,
									lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)
