#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================ M O D U L E   D E S C R I P T I O N ====
//@ Class: Phase3.cc - Phase 3 POST Tests
//---------------------------------------------------------------------
//@ Interface-Description
//  This class contains the Phase 3 POST test definitions and accessor
//  method for the BD and GUI CPU.  The GetTestItems method returns the
//  list of tests for Phase 3 POST.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ Fault Handling
//  Any error during execution is logged to kernel NOVRAM and the execution
//  is suspended.
//---------------------------------------------------------------------
//@ Restrictions
//  None
//---------------------------------------------------------------------
//@ Invariants
//  None
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/POST/vcssrc/Phase3.ccv   25.0.4.0   19 Nov 2013 14:17:04   pvcs  $
//
//@ Modification-Log
//
//
//  Revision: 003    By: Gary Cederquist  Date: 15-AUG-1997  DR Number: 1875
//    Project: Sigma (R8027)
//    Description:
//       Added testable item annotations.
//
//  Revision: 002    By: Gary Cederquist  Date: 05-AUG-1997  DR Number: 2199
//    Project: Sigma (R8027)
//    Description:
//       Removed SAAS resonance test.  Resonance test replaced with
//       audible feedback test in Phase 2 POST.
//
//  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//=====================================================================

#include "Phase3.hh"

#include "Cpu.hh"
#include "PostErrors.hh"
#include "ResetUmpire.hh"


// -------------------------------------------------------------------------
// This is the POST executive test structure for both GUI and BD CPU.  Each test
// Each test is executed in the order they appear in this structure.
//
//
// Structure Members:
//
// testName:        Text of test in progress, used to send status information
//                  to a debug port
//
// testNumber:      Value displayed on diagnostic LED indicator.
//
// ptestProcedure:  Pointer to test procedure to execute
// -----------------------------------------------------------------------------

// Structure definition for unit testing

const PostTestItem Phase3::TestItems_[] =
{

// Test                       Test                                Test
// Name                       Number (error code)                 procedure

   {"FPU Test",		          POST_ERROR_FPU,			Cpu::FpuTest       } 
  ,{"MMU Test",		          POST_ERROR_MMU,			Cpu::MmuTest       } 
  ,{"Reset Umpire",           POST_ERROR_UNEXPECTED_RESET_UMPIRE, ResetUmpire::Test,      }
  ,{"END OF TESTS",	          POST_ERROR_UNUSED,		NULL               }

};

//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Method: GetTestItems
//
//@ Interface-Description
//   This routine returns the test items for POST phase 3.
//---------------------------------------------------------------------
//@ Implementation-Description
//    Returns a handle to TestItems_
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Free-Function
//=====================================================================
 
const PostTestItem *
Phase3::GetTestItems(void)
{
    // $[TI1]
    return (TestItems_);
}

