
#ifndef EXH_FLOW_SENSOR_H

#define EXH_FLOW_SENSOR_H

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
//@ Class: ExhFlowSensor_t - Implements the expiratory flow sensors.
//
//*****************************************************************************
//
//  ExhFlowSensor.h - Header file for the ExhFlowSensor_t class.
//
//*****************************************************************************

#include "Sigma.hh"
#include "FlowFilter.h"
#include "DataFilter.h"
#include "AioDioDriver.h"
#include "Sensor.hh"
#include "CalibrationTable.h"
#include "FlowSensor.hh"
#include "ProcessedValues.hh"

class ExhFlowSensorCalibrationTable_t;

enum ExhFlowSensorStatus_t
{
    STATUS_GOOD=0x00,       		/* good data */
    STATUS_CLEANING=0x01,	       	/* Wire Cleaning active */
    STATUS_WIRE_BROKEN=0x02,       	/* Heated wire broken */
    STATUS_COMP_WIRE_BROKEN=0x03,   /* Compensation wire broken */
    STATUS_ZERO_OUT=0x04,			/* Zero value of flow sensor out of range */
    STATUS_DISCONNECT=0x05			/* Sensor Disconnected */
};

#define NEW_DATA_BIT				0x80
#define CLEANING_BIT				0x40
#define DIS_BITS					0x30
#define HEATED_WIRE_BROKEN_BIT		0x20
#define COMP_WIRE_BROKEN_BIT		0x10
#define ZERO_OUT_BIT				0x08
#define BAD_POWER_OR_CIRCUIT_BIT	0x04
#define AUTOZERO_DONE_BIT			0x02

#define AUTOZERO_DONE_BIT_NEW_SENSOR	0x04


#define FEXTAB_SIZE			201
#define FEXTAB_LOW_SIZE		100

class CalibrationTable_t;

class ExhFlowSensor_t : public Sensor
{
	friend class ExhFlowSensorCalibrationTable_t;

public:
	ExhFlowSensor_t(const AdcChannels::AdcChannelId adcId);
	~ExhFlowSensor_t();
	void ProcessData(uint8_t FexStatus, uint16_t FexData, bool bDataValid);
	bool GetFlow(Flow_t &Value);
	void updateValue();

	Flow_t GetFlow2();
	bool IsError();
	bool IsDisconnected();
	bool Autozero();
	bool IsAutozeroInProgress()
	{
		return bAutozero;
	}
	bool IsZeroOut();
	void SetNoComm();

	uint16_t GetStatusData() { return StatusData; }
	ExhFlowSensorStatus_t GetSensorStatus() { return SensorStatus; }
	uint16_t GetRawData() { return RawData; }
	uint16_t GetRawFilteredData() { return RawFilteredData; }
	uint16_t GetZeroOffset() { return ExhCalOffset; }
	uint16_t GetAzRawData() { return AzRawData; }

    //  Used by the calibration process to update the cal data
    void UpdateCalData(ExhFlowSensorCalibrationTable_t *pCalTab);

    //  Used during initialization to set the cal data
    void SetCalData(const ExhSensorTable_t & SensorTable);

#if defined(SIGMA_BD_CPU)
	// E600 BDIO - to deal with existing code that wants this
	float getDrySlope() { return( dryProcessedValues_.getSlopeValue()) ; }
	float getDryValue() { return( dryFlow_) ; }

	Real32 getFilteredSlopeDryValue( void) const { return( dryProcessedValues_.getFilteredSlopeValue()) ; }
	Real32 getFilteredDryValue ( void) const { return( dryProcessedValues_.getFilteredValue()) ; }
#endif
	
	void  setO2Percent(float x) { o2Percent_ = x; }
    
#if defined(SIGMA_BD_CPU)
	Real32 getUnoffsettedValue( void) const { return( unoffsettedFlow_) ; }
    Real32 getUnoffsettedFilteredValue( void) const { return( unoffsettedProcessedValues_.getFilteredValue()) ; }
	void setUnoffsettedProcessedValuesAlpha( const Real32 alpha) { unoffsettedProcessedValues_.setAlpha( alpha) ; }
    
	Real32 getSlope( void) const { return( processedValues_.getSlopeValue()) ; }
	Real32 getFilteredSlopeValue( void) const { return( processedValues_.getFilteredSlopeValue()) ; }
	Real32 getFilteredValue ( void) const { return( processedValues_.getFilteredValue()) ; }
	void setAlpha( const Real32 alpha) { processedValues_.setAlpha( alpha) ; }

    void initValues( const Real32 filtered, const Real32 slope, const Real32 filteredSlope, const Real32 prevValue)
	{
		processedValues_.initValues( filtered, slope, filteredSlope, prevValue) ;
	}
#endif

protected:
	Flow_t  rawToEngValue_( const AdcCounts count) const 
	{
		UNUSED_SYMBOL(count);
		return AdjustedFlow; }

	void	CheckStatus(uint8_t FexStatus);
	Flow_t	FiO2_Correction(Flow_t ExhFlow, FiO2_t FiO2);
	Flow_t	ConvertFlow(uint16_t AdcValue);
	void	CalculateFlow(uint16_t AdcValue);
	void	NormalizeFexhTable(uint16_t Offset);
	float	CalcFactor(float Patm, float Tbt);
	Flow_t  CalculateFlow2(uint16_t AdcValue, uint16_t AzValue);

#if E600_TO_CHECK
	float	Standard_To_BTPS_Conversion(float Standard);
#endif

	uint8_t			DisconnectCount;
	uint8_t			HeatedWireBrokenCount;
	uint8_t			CompWireBrokenCount;
	uint8_t			ZeroOutCount;
	uint8_t			BadSensorCount;

	ExhFlowSensorStatus_t SensorStatus;
	uint16_t		StatusData;
	uint16_t		RawData;
	uint16_t		AzRawData;
	uint16_t		AzAdcSum;
	uint16_t		NumAzSamples;
	uint16_t		RawFilteredData;
	uint16_t		ExhCalOffset;
	Flow_t			UnAdjustedFlow;
	Flow_t			AdjustedFlow;
	Flow_t			FilteredFlow;
	Flow_t			Flow2;
	bool			bError;
	bool			bAutozero;
	bool			bAzCompleted;
	bool			bTakingAzADC;
	bool			bDataValid;
	DataFilter_t<uint16_t>	ExhDataFilter;

    ExhSensorTable_t ExhSensorTable;        // Actual stored data
    float           *pDacTab;               // Pointer into ExhSensorTable
    float           *pFlowTab;              // Pointer into ExhSensorTable

	//@ Data-Member: Real32 o2Percent_
	// delivered O2 percent to compensate for exhalation flows
	Real32 o2Percent_ ;

#if defined (SIGMA_BD_CPU)

    //@ Data-Member: processedValues_
    // a ProcessedValues instance
    ProcessedValues processedValues_;

	//@ Data-Member: dryFactor_   
    // Contains the humidification correction to dry gas,
    Real32 dryFactor_ ;

	//@ Data-Member: dryFlow_
	// exhalation flow compensated to dry
	Real32 dryFlow_ ;

    //@ Data-Member: dryProcessedValues_
    // processed values for dry flow
    ProcessedValues dryProcessedValues_ ;

	//@ Data-Member: unoffsettedFlow_
	// flow without flow offset compensation
	Real32 unoffsettedFlow_ ;

    //@ Data-Member: unoffsettedProcessedValues_
    // processed values for unoffsetted flow
    ProcessedValues unoffsettedProcessedValues_ ;

#endif // defined (SIGMA_BD_CPU)
};

#endif

