
#ifndef SNodeC_Int32_HH
#define SNodeC_Int32_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: SNodeC_Int32 - Node for Singly-Linked Lists of Generic Data
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Foundation/vcssrc/SLinkedNodeC.H_v   25.0.4.0   19 Nov 2013 14:03:50   pvcs  $
//
//@ Modification-Log
//
//   Revision: 003   By: sah   Date: 07-Jan-1999   DR Number: 5321
//   Project:  ATC
//   Description:
//	Changed 'SoftFault()' method to an non-inlined method.
//  
//   Revision: 002   By: sah   Date: 06-Feb-1996   DCS Number: 674
//   Project:  Sigma (R8027)
//   Description:
//	Removed 'ifndef' and 'endif' from around includes.
//
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//====================================================================

#include "Sigma.hh"
#include "FoundationClassId.hh"
#include "TemplateMacros.hh"
#include "Int32.hh"

//@ Usage-Classes
//@ End-Usage


class SNodeC_Int32
{
    //@ Friend:  SListC_Int32
    // 'SListC_Int32' needs access to the constructor, destructor,
    // and private members of this class.
    friend class SListC_Int32;

  public:
    inline const Int32&  getItem(void) const;
    inline Int32&        getItem(void);

    inline const SNodeC_Int32*  getNextPtr(void) const;

    static void  SoftFault(const SoftFaultID softFaultID, 
			   const Uint32      lineNumber, 
			   const char*       pFileName = NULL, 
			   const char*       pBoolTest = NULL); 

  private:
    // only 'SListC_Int32' can create/destroy this class...
    inline SNodeC_Int32(const SNodeC_Int32& sNodeC);
    inline SNodeC_Int32(const Int32& newItem);
    inline ~SNodeC_Int32(void);

    SNodeC_Int32(void);				// not implemented...
    void  operator=(const SNodeC_Int32&);		// not implemented...

    //@ Data-Member:  nodeItem_
    // The stored item for this list node.
    Int32  nodeItem_;

    //@ Data-Member:  pNext_
    // A pointer to the next node.
    SNodeC_Int32*  pNext_;
};


// Inline methods...
#include "SNodeC_Int32.in"


#endif  // SNodeC_Int32_HH
