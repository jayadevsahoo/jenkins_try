#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: CompactFlashTest - Performs the compact flash test
//---------------------------------------------------------------------
//@ Interface-Description
//	This class performs the Service Mode compact flash test. 
//---------------------------------------------------------------------
//@ Rationale
//	This class encapsulates the methods required to perform the compact
//	flash test.
//---------------------------------------------------------------------
//@ Implementation-Description
//	The method runTest() performs the compact flash test.
//---------------------------------------------------------------------
//@ Fault-Handling
//	n/a
//---------------------------------------------------------------------
//@ Restrictions
//	Only one instance of this class may be created.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/CompactFlashTest.ccv   25.0.4.0   19 Nov 2013 14:23:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By:  gdc    Date:  26-Mar-2007    SCR Number: 6237
//  Project:  Trend
//  Description:
//      Initial version.
//=====================================================================

#include "CompactFlashTest.hh"


//@ Usage-Classes

#include "CompactFlash.hh"
#include "NovRamManager.hh"
#include "SmManager.hh"
#include "SmRefs.hh"
#include "SmUtilities.hh"
#include "Task.hh"

//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: CompactFlashTest()  [Default Constructor]
//
//@ Interface-Description
//	Constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

CompactFlashTest::CompactFlashTest( void)
{
	CALL_TRACE("CompactFlashTest::CompactFlashTest( void)") ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~CompactFlashTest()  [Destructor]
//
//@ Interface-Description
//	Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//	none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

CompactFlashTest::~CompactFlashTest( void)
{
	CALL_TRACE("CompactFlashTest::~CompactFlashTest( void)") ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetCompactFlashTest
//
//@ Interface-Description
//	Instantiates a CompactFlashTest object and returns a reference to 
//  it.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
CompactFlashTest &
CompactFlashTest::GetCompactFlashTest( void)
{
	CALL_TRACE("GetCompactFlashTest(void)") ;

	static CompactFlashTest compactFlashTest;

	return compactFlashTest;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: runTest
//
//@ Interface-Description
//	This method performs the compact flash test.  No arguments,
//	no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[TR01129]  The system shall provide a method to test the compact 
//              flash functionality during service mode.
//  This method sets the novram status flag to "in progress". Then 
//  it performs a compact flash self test and retrieves the results
//  from the test.  If the results from the test is a STATUS_OK, 
//  it sets the novram status flag to "PASSED". Other results from 
//  the test shall set the novram status flag to "FAILED" and with
//  the status sent to service data.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
CompactFlashTest::runTest( void)
{
	CALL_TRACE("CompactFlashTest::runTest( void)") ;

	// Set novram status flag to "in progress"
	NovRamManager::UpdateCompactFlashTestStatus(ServiceMode::IN_PROGRESS);

	// delay for 1 ms
	Task::Delay(1);

	CompactFlash & compactFlash = CompactFlash::GetCompactFlash();

	// Verify if we can communicate to the compactflash
	// If so, perform a compactflash self test.
	if ( compactFlash.open() == SUCCESS )
	{
		// perform an extended self test
		compactFlash.selfTest( TRUE );
	}

	// Retrieve the compactflash status
	CompactFlash::CfStatus status = compactFlash.getStatus();

	// If the compactflash status returns ok 
    // then set the novram status flag to "passed" 
	if ( status == CompactFlash::STATUS_OK )
	{
		NovRamManager::UpdateCompactFlashTestStatus( ServiceMode::PASSED );
	}
	else if ( status == CompactFlash::NO_CARD_ERROR || status == CompactFlash::CARD_ERROR )
	{
		// The compactflash itself is reporting an error
		// Set the novram status flag to "Failed" and 
		// send a status update to Service data
		RSmManager.sendStatusToServiceData(SmStatusId::TEST_FAILURE, SmStatusId::CONDITION_2);
		NovRamManager::UpdateCompactFlashTestStatus( ServiceMode::FAILED );
	}
	else
	{
		// Cannot communicate to the compactflash interface
		// Set the novram status flag to "Failed" and 
		// send a status update to Service data
		RSmManager.sendStatusToServiceData(SmStatusId::TEST_FAILURE, SmStatusId::CONDITION_1);
		NovRamManager::UpdateCompactFlashTestStatus( ServiceMode::FAILED );
	}   
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//  and 'lineNumber' are essential pieces of information.  The
//  'pFileName' and 'pPredicate' strings may be defaulted in the macro
//  to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//  none 
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

void
CompactFlashTest::SoftFault( const SoftFaultID  softFaultID,
							 const Uint32 lineNumber,
							 const char *pFileName,
							 const char *pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)") ;
	FaultHandler::SoftFault( softFaultID, SERVICE_MODE, COMPACTFLASHTEST,
							 lineNumber, pFileName, pPredicate);
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//=====================================================================
//
//  Private Methods...
//
//=====================================================================
