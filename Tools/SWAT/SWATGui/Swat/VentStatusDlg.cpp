// VentStatusDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Swat.h"
#include "VentStatusDlg.h"



// CVentStatusDlg dialog

IMPLEMENT_DYNAMIC(CVentStatusDlg, CDialog)

CVentStatusDlg::CVentStatusDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CVentStatusDlg::IDD, pParent)
	, m_strPD1(_T(""))
	, m_strButtonID(_T(""))
	, m_strMainCentre(_T(""))
	, m_strBanner(_T(""))
	, m_strBanner_2(_T(""))
	, m_strBanner_3(_T(""))
	, m_strBanner_4(_T(""))
	, m_strBanner_5(_T(""))
	, m_strBanner_6(_T(""))
	, m_strPD2(_T(""))
	, m_strPD3(_T(""))
	, m_strPD4(_T(""))
	, m_strCtr_1(_T(""))
	, m_strCtr_2(_T(""))
	, m_strCtr_3(_T(""))
	, m_strCtr_4(_T(""))
	, m_strCtr_5(_T(""))
	, m_strCtr_6(_T(""))
	, m_strCtr_7(_T(""))
	, m_strMeta(_T(""))
{

}

CVentStatusDlg::~CVentStatusDlg()
{
}

void CVentStatusDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_BUTTONID, m_strButtonID);
	DDX_Text(pDX, IDC_STATIC_APNEA, m_strMainCentre);
	DDX_Text(pDX, IDC_STATIC_SAFETY_VENTILATION, m_strBanner);
	DDX_Text(pDX, IDC_STATIC_ID_BANNER_SAFETY_PCV_LINE_2, m_strBanner_2);
	DDX_Text(pDX, IDC_STATIC_ID_BANNER_SAFETY_PCV_LINE_3, m_strBanner_3);
	DDX_Text(pDX, IDC_STATIC_ID_BANNER_SAFETY_PCV_LINE_4, m_strBanner_4);
	DDX_Text(pDX, IDC_STATIC_ID_BANNER_SAFETY_PCV_LINE_5, m_strBanner_5);
	DDX_Text(pDX, IDC_STATIC_ID_BANNER_SAFETY_PCV_LINE_6, m_strBanner_6);
	DDX_Text(pDX, IDC_STATIC_PD_1, m_strPD1);
	DDX_Text(pDX, IDC_STATIC_PD_2, m_strPD2);
	DDX_Text(pDX, IDC_STATIC_PD_3, m_strPD3);
	DDX_Text(pDX, IDC_STATIC_PD_4, m_strPD4);
	DDX_Text(pDX, IDC_STATIC_CNTR_1, m_strCtr_1);
	DDX_Text(pDX, IDC_STATIC_CNTR_2, m_strCtr_2);
	DDX_Text(pDX, IDC_STATIC_CNTR_3, m_strCtr_3);
	DDX_Text(pDX, IDC_STATIC_CNTR_4, m_strCtr_4);
	DDX_Text(pDX, IDC_STATIC_CNTR_5, m_strCtr_5);
	DDX_Text(pDX, IDC_STATIC_CNTR_6, m_strCtr_6);
	DDX_Text(pDX, IDC_STATIC_CNTR_7, m_strCtr_7);
	DDX_Text(pDX, IDC_STATIC_META, m_strMeta);
}


BEGIN_MESSAGE_MAP(CVentStatusDlg, CDialog)
	ON_BN_CLICKED(IDC_BUTTON_REFRESH, &CVentStatusDlg::OnBnClickedButtonRefresh)
	ON_BN_CLICKED(IDC_BUTTON_GETTEXT, &CVentStatusDlg::OnBnClickedButtonGettext)
END_MESSAGE_MAP()


// CVentStatusDlg message handlers

void CVentStatusDlg::OnBnClickedButtonRefresh()
{


	
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	if(pApp->m_bRun == true)
	{
		if(AfxMessageBox("Script execution will be stopped. \n Continue?",MB_YESNO) == IDNO)
		{
			return;
		}
		else
		{
			while(pApp->m_bScriptRunning == true)
			{
				Sleep(500);
			}
		}
	}
	CWaitCursor wait;

	pApp->m_bRun = true;

	UpdateHeader();
	UpdatePD();
	UpdateCentre();
	UpdateMetaData();

	pApp->m_bRun = false;
	UpdateData(FALSE);

	
}

void CVentStatusDlg::OnBnClickedButtonGettext()
{
	CWaitCursor wait;
	UpdateData(TRUE);
	if(m_strButtonID.IsEmpty()) return;
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();

	CString str;
	pApp->m_bRun = 1;
	int nActive = pApp->GUI_IsActivated(m_strButtonID);
	if(pApp->GUI_IsActivated(m_strButtonID)<1)
	{
		str.Format("\n\nGetActivated returned : %d", nActive);
	}

	AfxMessageBox(pApp->GUI_IOGetText(m_strButtonID)+str);
	pApp->m_bRun = 0;
}

void CVentStatusDlg::UpdateHeader()
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();

	CString strID = "ID_BANNER_SAFETY_PCV_LINE_1";
	
	if(pApp->GUI_IsActivated(strID)==2)
	{
		m_strBanner = pApp->GUI_IOGetText(strID);
	}

	strID = "ID_BANNER_SAFETY_PCV_LINE_2";
	if(pApp->GUI_IsActivated(strID)==2)
	{
		m_strBanner_2 = pApp->GUI_IOGetText(strID);
	}
	
	strID = "ID_BANNER_SAFETY_PCV_LINE_3";
	if(pApp->GUI_IsActivated(strID)==2)
	{
		m_strBanner_3 = pApp->GUI_IOGetText(strID);
	}
	
	strID = "ID_BANNER_SAFETY_PCV_LINE_4";
	if(pApp->GUI_IsActivated(strID)==2)
	{
		m_strBanner_4 = pApp->GUI_IOGetText(strID);
	}
	
	strID = "ID_BANNER_TEXT_LINE_1";
	if(pApp->GUI_IsActivated(strID)==2)
	{
		m_strBanner_5 = pApp->GUI_IOGetText(strID);
	}
	
	strID = "ID_BANNER_TEXT_LINE_2";
	if(pApp->GUI_IsActivated(strID)==2)
	{
		m_strBanner_6 = pApp->GUI_IOGetText(strID);
	}
	UpdateData(FALSE);
}

void CVentStatusDlg::UpdatePD()
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	if(pApp->GUI_IsActivated("ID_VPDA_STATIC_SLOT_1")!=2)
	{ //nothing to update, return;
		return;
	}

	CString strLabels;
	CString strValues;
	CString strUnits;
	PDData pdData;

	pdData = GetPDData("ID_VPDA_STATIC_SLOT_1");
	m_strPD1 = pdData.strLabel + "\n" + pdData.strValue + "\n" + pdData.strUnit;

	pdData = GetPDData("ID_VPDA_STATIC_SLOT_2");
	m_strPD2 = pdData.strLabel + "\n" + pdData.strValue + "\n" + pdData.strUnit;

	pdData = GetPDData("ID_VPDA_STATIC_SLOT_3");
	m_strPD3 = pdData.strLabel + "\n" + pdData.strValue + "\n" + pdData.strUnit;

	pdData = GetPDData("ID_VPDA_STATIC_SLOT_4");
	m_strPD4 = pdData.strLabel + "\n" + pdData.strValue + "\n" + pdData.strUnit;

	UpdateData(FALSE);

	

}

PDData CVentStatusDlg::GetPDData(CString strButtonID)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	CString str = pApp->GUI_IOGetText(strButtonID);

	CString strValues;
	if(str.Find(",") != -1)
	{
		CString strItalic("false");
		CString strValue("false");
		CString strLabel("false");
		CString strUnits("false");
		
		strValue = str.Left(str.Find(",")); //strValue = "{I:45},"

		str = str.Right(str.GetLength() - str.Find(",")-1);

		strLabel = str.Left(str.Find(","));

		strUnits = str.Right(str.GetLength()-str.Find(",")-1);

		if(strValue.Find("}") != -1 ) // {I:24}
		{	strValue = strValue.Mid((strValue.Find("I:")+2), (strValue.GetLength()-4));
			strItalic = "True";
		}

		strValues.Format("Value=%s, Italic=%s, Label=%s, Unit=%s", strValue, strItalic, strLabel, strUnits); //Value and Italic
		PDData pd_data;
		pd_data.strLabel = strLabel;
		pd_data.strUnit = strUnits;
		pd_data.strValue = strValue;
		return pd_data;
	}
	else
	{
		CString strValue = str;
		CString strItalic = "na";
		//only one value recvd, no other info ->Its value of the button, check if italic
		if(strValue.Find("}") != -1 ) // {I:24}
		{	strValue = strValue.Mid((strValue.Find("I:")+2), (strValue.GetLength()-4));
			strItalic = "True";
		}
		else
		{
			strItalic = "False";
		}

		strValues.Format("Value=%s, Italic=%s, Label=%s, Unit=%s", str, strItalic, "na", "na");
		PDData pd_data;
		pd_data.strLabel = " ";
		pd_data.strUnit = " ";
		pd_data.strValue = strValue;
		return pd_data;
	}


}

void CVentStatusDlg::UpdateCentre()
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	if(pApp->GUI_IsActivated("ID_VENT_SETUP_DIALOG")>0)
		m_strCtr_1 = "Vent Setup";
	else
		m_strCtr_1.Empty();

	if (pApp->GUI_IsActivated("ID_MAIN_SETTINGS_DIALOG")>0)
		m_strCtr_2  = "Main settings dialog";
	else
		m_strCtr_2.Empty();

	if (pApp->GUI_IsActivated("ID_LAYOUT_DIALOG")>0)
		m_strCtr_3  = "Layout Dialog";
	else
		m_strCtr_3.Empty();

	if (pApp->GUI_IsActivated("ID_QUICK_LAYOUT_DIALOG")>0)
		m_strCtr_4  = "Quick Layout Dialog";
	else
		m_strCtr_4.Empty();

	if (pApp->GUI_IsActivated("ID_APNEA_VENTILATION_DIALOG")>0)
		m_strCtr_5  = "APNEA";
	else
		m_strCtr_5.Empty();

	if (pApp->GUI_IsActivated("ID_ELEVATEDO2_DIALOG")>0)
		m_strCtr_6  = "Elevated O2";
	else
		m_strCtr_6.Empty();

	if (pApp->GUI_IsActivated("ID_LARGE_FONT_DISPLAY_DIALOG")>0)
		m_strCtr_7  = "Large Font Dialog";
	else
		m_strCtr_7.Empty();






	

}

void CVentStatusDlg::UpdateMetaData()
{
	
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	m_strMeta = "Circ: "+ pApp->GUI_IOGetText("ID_META_AREA_CIRCUIT_TYPE")+"\n"+
		+"PBW :"+ pApp->GUI_IOGetText("ID_META_AREA_PBW")+"\n"+
		+"Mode:"+pApp->GUI_IOGetText("ID_META_AREA_MODE")+"\n"+
		+"Mand:"+pApp->GUI_IOGetText("ID_META_AREA_MAND_TYPE")+"\n"+
		+"Trig:"+pApp->GUI_IOGetText("ID_META_AREA_TRIG_TYPE");


}