/*****************************************************************************/
/*   Module Name:   Testreport.c                                             */
/*                                                                           */
/*   Purpose:       This file contains ASCII report functions                */
/*                                                                           */
/*                                                                           */
/*   Requirements:  This module implements requirements detailed in          */
/*                  <75359-93>                                               */
/*                                                                           */
/*   Modification History:                                                   */
/*                                                                           */
/*   Description of Change                  By                Date           */
/*   ---------------------                  -----             ------         */
/*   ReportStringToFile is modifed          Hamsavally.R      03 Jan,2000    */
/*   to append the 72362.log                                                 */
/*                                                                           */
/*   The Error codes in ErrorMessage        Dwarkanath(CGS)   12 Dec,2000    */
/*   functions are changed.                                                  */
/*                                                                           */
/*   Selected Reports to preview or         Lakshmi(CGS)      01 Dec,2000    */
/*   delete functions are added.                                             */
/*                                                                           */
/*   Removed 840vts dependent report        Dwarkanath(CGS)   17 Oct,2000    */
/*   functions and replaced with                                             */
/*   equivalent functions.                                                   */
/*                                                                           */
/*   Updated comment blocks                 Dave Richardson   20 Nov, 2002   */
/*   for file and function descriptions                                      */
/*                                                                           */
/*   $Revision:   1.2  $                                                              */
/*                                                                           */
/*   $Log:   //pleasa1apo2/swqa/data/arc/840_CPVT_VTS/Program/tests/testreport.c-arc  $
 * 
 *    Rev 1.2   04 Dec 2002 15:36:28   Buchman
 * 840 VTS build done
 * 
 *    Rev 1.1   20 Nov 2002 13:29:20   Richardson
 * Updated per SCRs # 32

                                                                             */
/*****************************************************************************/

/*****************************************************************************
THIS IS A PROPRIETARY WORK TO WHICH PURITAN-BENNETT CORPORATION CLAIMS
EXCLUSIVE RIGHT.  NO PART OF THIS WORK MAY BE USED, DISCLOSED, REPRODUCED,
STORED IN AN INFORMATION RETIEVAL SYSTEM, OR TRANSMITTED, IN ANY FORM OR BY ANY
MEANS ELECTRONIC, MECHANICAL, PHOTOCOPYING, RECORDING OR OTHERWISE WITHOUT THE
PRIOR WRITTEN PERMISSION OF PURITAN-BENNETT CORPORATION.
 
Copyright (c) 2001-2002, Puritan-Bennett
All rights reserved.
*******************************************************************************/

/***********************************************/
/****************** INCLUDES *******************/
/***********************************************/

#include <ansi_c.h>
#include <formatio.h>
#include <userint.h>
#include <utility.h>

#include "840cmds.h" // VENTDATA Vent
#include "cpvtmain.h"
#include "ldb.h"
#include "MsgPopup.h"
#include "pb_util.h"

/*************************************************************/
/*************************** defines *************************/
/*************************************************************/

#define REPORT_GROW_SIZE 2000
#define INDENT_STR "                    "
#define INDENT_SIZE (strlen(INDENT_STR))
#define FILE_APPEND 1

/*************************************************************/
/********************** Global Variables *********************/
/*************************************************************/

extern int gReportScreenOpt;
int gReportFileOpt = RPT_ALL;
int gPrintDestOpt = PRINT_TO_PRN;
int gReportType = FULL_REPORT;
extern int gOverallpass;

/*************************************************************/
/********************** Static Variables *********************/
/*************************************************************/

static char *gTestReport     = NULL;
static int   sTestReportSize = 0;
static char *gReport         = NULL;

/*************************************************************/
/**************** Static Function Prototypes *****************/
/*************************************************************/

static int IncreaseTestReport(char *newStr);
char *StrDuplicate(char *s);

/*****************************************************************************/
/* Name:     InitTestReport                                                  */
/*                                                                           */
/* Purpose:  Initialize Test Report                                          */
/*                                                                           */
/*                                                                           */
/* Example Call:   int InitTestReport(void);                                 */
/*                                                                           */
/* Input:    input1 - description                                            */
/*                                                                           */
/* Output:   <None>                                                          */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Globals Used:                                                             */
/*                                                                           */
/*                                                                           */
/* Return:   TRUE/FALSE                                                      */
/*                                                                           */
/*****************************************************************************/
int InitTestReport(void)
{
	if(gReport != NULL){
		free(gReport);
		gReport = NULL;
	}

	return TRUE;
}

/*****************************************************************************/
/* Name:     ReportStringToFile                                              */
/*                                                                           */
/* Purpose:  if user wants to save test report, save.                        */
/*                                                                           */
/*                                                                           */
/* Example Call:   int ReportStringToFile();                                 */
/*                                                                           */
/* Input:    input1 - description                                            */
/*                                                                           */
/* Output:   <None>                                                          */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Globals Used:                                                             */
/*                                                                           */
/*                                                                           */
/* Return:   TRUE/FALSE                                                      */
/*                                                                           */
/*****************************************************************************/
int ReportStringToFile(void)
{
	int hFile;
	int err;

	if(gSeqReportFile == NULL || strcmp(gSeqReportFile, "") == 0){
		return TRUE;
	}

	// Appends the ASCII report
	hFile = OpenFile(gSeqReportFile, VAL_WRITE_ONLY, FILE_APPEND, VAL_ASCII);

	if(hFile < 0){
		ErrorMessage(UNABLE_TO_OPEN_FILE_ERROR, gSeqReportFile);
		return TRUE;
	}
	err = FmtFile (hFile, "%s<%s", gReport);

	if(err < 0){
		CloseFile(hFile);
		ErrorMessage(WRITING_TO_FILE_ERROR, gSeqReportFile);
		return TRUE;
	}

	CloseFile(hFile);

	return TRUE;
}

/*****************************************************************************/
/* Name:      WriteTestString                                                */
/*                                                                           */
/* Purpose:  Add string to current test report                               */
/*                                                                           */
/*                                                                           */
/* Example Call:   int WriteTestString(char *str);                           */
/*                                                                           */
/* Input:    string to write                                                 */
/*                                                                           */
/* Output:   <None>                                                          */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Globals Used:                                                             */
/*                                                                           */
/*                                                                           */
/* Return:    TRUE/FALSE                                                     */
/*                                                                           */
/*****************************************************************************/
int WriteTestString(char *str)
{
	if(!IncreaseTestReport(str)){
		return FALSE;
	}
	Fmt(gTestReport, "%s[a]<%s", str);

	return TRUE;
}

/*****************************************************************************/
/* Name:      WriteSeqHeader                                                 */
/*                                                                           */
/* Purpose:   Write Test Report Sequence Header                              */
/*                                                                           */
/*                                                                           */
/* Example Call:   int WriteSeqHeader(short seqType,                         */
/*                                    char *serialNum,                       */
/*                                    char *uutRevNum);                      */
/*                                                                           */
/* Input:    Sequence type (single pass or UUT)                              */
/*                                                                           */
/* Output:   <None>                                                          */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Globals Used:                                                             */
/*                                                                           */
/*                                                                           */
/* Return:   TRUE/FALSE                                                      */                                                        
/*                                                                           */
/*****************************************************************************/
int WriteSeqHeader(char *serialNum, char *uutRevNum)
{
	if(gReportFileOpt == RPT_NONE){
		return TRUE;
	}

	if(!StatusReportString(1)){
		return FALSE;
	}

	if(!WriteTestString(LDBf(Ldb_rpt_title)))        {return FALSE;}
	if(!WriteTestString(LDBf(Ldb_rpt_test_station))) {return FALSE;}
	if(!WriteTestString(LDBf(Ldb_station_desc)))     {return FALSE;}
	if(!WriteTestString("\n"))                       {return FALSE;}

	if(!WriteTestString(LDBf(Ldb_rpt_stn_id)))       {return FALSE;}
	if(!WriteTestString(gStationID))                 {return FALSE;}
	if(!WriteTestString("\n"))                       {return FALSE;}

	if(!WriteTestString(LDBf(Ldb_rpt_pgm_rev)))      {return FALSE;}
	if(!WriteTestString(gProgramRev))                {return FALSE;}
	if(!WriteTestString("\n"))                       {return FALSE;}

	if(!WriteTestString(LDBf(Ldb_rpt_operator)))     {return FALSE;}
	if(!WriteTestString(gCurrOperator))              {return FALSE;}
	if(!WriteTestString("\n\n"))                     {return FALSE;}

	if(!WriteTestString(LDBf(Ldb_rpt_uut_pn)))       {return FALSE;}
	if(!WriteTestString(gUUTPN))                     {return FALSE;}
	if(!WriteTestString("\n"))                       {return FALSE;}

	if(!WriteTestString(LDBf(Ldb_rpt_uut_sn)))       {return FALSE;}
	if(!WriteTestString(Vent.Bd.serial))             {return FALSE;}
	if(!WriteTestString("\n"))                       {return FALSE;}

	if(!WriteTestString(LDBf(Ldb_rpt_uut_rev)))      {return FALSE;}
	if(!WriteTestString(Vent.Bd.post))               {return FALSE;}
	if(!WriteTestString("\n\n"))                     {return FALSE;}

	if(!WriteTestString(LDBf(Ldb_rpt_date)))         {return FALSE;}
	if(!WriteTestString(DateStr()))                  {return FALSE;}
	if(!WriteTestString(LDBf(Ldb_rpt_time)))         {return FALSE;}
	if(!WriteTestString(TimeStr()))                  {return FALSE;}
	if(!WriteTestString(LDBf(Ldb_rpt_test_results))) {return FALSE;}
	if(!WriteTestString("\n_________________________\n\n")) {return FALSE;}

	return TRUE;
}

/*****************************************************************************/
/* Name:     WriteStopReason                                                 */
/*                                                                           */
/* Purpose:  Write Reason Sequence stopped                                   */
/*                                                                           */
/*                                                                           */
/* Example Call:   int WriteStopReason(int abortFlag, int failStop);         */
/*                                                                           */
/* Input:    abort flag, stopped by fail flag                                */
/*                                                                           */
/* Output:   <None>                                                          */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Globals Used:                                                             */
/*                                                                           */
/*                                                                           */
/* Return:  TRUE/FALSE                                                       */
/*                                                                           */
/*****************************************************************************/
int WriteStopReason(int abortFlag, int failStop)
{
	if(gReportFileOpt == RPT_NONE){
		return TRUE;
	}
	if(abortFlag){
		if(!WriteTestString(LDBf(Ldb_rpt_tp_aborted))){
			return FALSE;
		}
	}
	if(failStop){
		if(!WriteTestString(LDBf(Ldb_rpt_rte_stopped))){
			return FALSE;
		}
	}

	return TRUE;
}

/*****************************************************************************/
/* Name:     WriteTestResult                                                 */
/*                                                                           */
/* Purpose:  Write results of test to Test Report                            */
/*                                                                           */
/*                                                                           */
/* Example Call:   int WriteTestResult(int testIndex);                       */
/*                                                                           */
/* Input:    test number                                                     */
/*                                                                           */
/* Output:   <None>                                                          */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Globals Used:                                                             */
/*                                                                           */
/*                                                                           */
/* Return:   TRUE/FALSE                                                      */
/*                                                                           */
/*****************************************************************************/
int WriteTestResult(int testIndex)
{
	char   *testInfo[8] = {0};
	char   *outputString, resultstring[30];
	int    i;
	short  limitSpec;
	double limitLow, limitHigh;
	int outputStrSize = 4096;
	char *tempBuf;
	int testPassed = FALSE;
	int testFailed = FALSE;
	#ifndef SKIP_IN_REPORT
	short runMode;
	#endif

	outputString = malloc(outputStrSize);
	outputString[0] = '\0';

	if(testIndex < 0){
		return FALSE;
	}

	#ifndef SKIP_IN_REPORT
	runMode = tDataRec[testIndex].sRunmode;
	if(runMode == RUN_MODE_SKIP){
		return TRUE;
	}
	#endif

	Fmt(outputString, "%s[w29]<%s", TestName[CurrLanguage][testIndex]);
	strcat(outputString, " ");
	if(gErrorFlag){
		Fmt(resultstring, "%s<%s\n", LDBf(Ldb_Error_Caps));
		Fmt(outputString, "%s[a]<%s", resultstring);
		testFailed = TRUE;
	}
	else if(tDataRec[testIndex].result == SKIP){
		Fmt(resultstring, "%s<%s\n", LDBf(Ldb_Skip_Caps));
		Fmt(outputString, "%s[a]<%s", resultstring);
	}
	else{
		if(limitSpec == SPEC_NONE || limitSpec == SPEC_LOG){
			Fmt(resultstring, "%s<%s\n", LDBf(Ldb_None_caps));
			Fmt(outputString, "%s[a]<%s", resultstring);
		}
		else{
			if(tDataRec[testIndex].result == PASS){
				Fmt(resultstring, "%s<%s\n", LDBf(Ldb_Pass_caps));
				testPassed = TRUE;
			}
			else{
				Fmt(resultstring, "%s<%s\n", LDBf(Ldb_Fail_caps));
				testFailed = TRUE;
			}
			Fmt(outputString, "%s[a]<%s", resultstring);
		}
	}
	if(tDataRec[testIndex].outBuffer != NULL){
		//                                                           NO_DB  LOCAL             NO_JET
		tempBuf = Tfmtdisplay(tDataRec[testIndex].outBuff_localized, FALSE, TRUE, NULL, NULL, FALSE, 0);
		if(tempBuf == NULL){
			return FALSE;
		}
		if(strlen(outputString) + strlen(tempBuf) > outputStrSize){
			outputString = realloc(outputString, outputStrSize + strlen(tempBuf) + 100);
		}
		Fmt(outputString, "%s[a]<%s\n", tempBuf);
		free(tempBuf);
	}
	else{
		/* Add return for easier reading */
		Fmt(outputString, "%s[a]<%s", "\n");
	}
	if(!IncreaseTestReport(outputString)){
		goto Error;
	}
	switch(gReportFileOpt){
		case RPT_ALL:
			Fmt(gTestReport, "%s[a]<%s", outputString);
			break;

		case RPT_PASSONLY:
			if(testPassed){
				Fmt(gTestReport, "%s[a]<%s", outputString);
			}
			break;

		case RPT_FAILONLY:
			if(testFailed){
				Fmt(gTestReport, "%s[a]<%s", outputString);
			}
			break;

		case RPT_NONE:
			break;
	} // end switch(gReportFileOpt)

	free(outputString);
	outputString = NULL;

	return TRUE;

Error:
	if(outputString != NULL){
		free(outputString);
	}

	return FALSE;
}

/*****************************************************************************/
/* Name:      StatusReportString                                             */
/*                                                                           */
/* Purpose:  Begin a new string for the report                               */
/*                                                                           */
/*                                                                           */
/* Example Call:   int StatusReportString();                                 */
/*                                                                           */
/* Input:    sts                                                             */
/*		if sts is 1, then begins a new report string.                        */
/*		if sts is 2, then update the test result.                            */
/*                                                                           */
/* Output:   <None>                                                          */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Globals Used:                                                             */
/*                                                                           */
/*                                                                           */
/* Return:   TRUE/FALSE                                                      */
/*                                                                           */
/*****************************************************************************/
int StatusReportString(int sts)
{
	char *ptr;
	int len;
	int i, j;

	switch(sts){
		case 1:
			if(gTestReport != NULL){
				free(gTestReport);
			}
			gTestReport = (char *)malloc(REPORT_GROW_SIZE);
			if(gTestReport == NULL){
				CPVTMessagePopup(LDBf(Ldb_MSG_OUT_OF_MEMORY), LDBf(Ldb_MSG_RPT_NO_MEM), 
				                 OK_ONLY_DLG, NO_HELP);
				return FALSE;
			}
			gTestReport[0] = '\0';
			sTestReportSize = REPORT_GROW_SIZE;
			break;

		case 2:
			gReport = gTestReport;
			gTestReport = NULL;

			if(gReport != NULL){
				for(j = 0; *(gReport+j); j++){
					if( *(gReport+j) == '~'){
						if(sAbortFlag){
							ptr = LDBf(Ldb_ABORTED);
						}
						else if(gOverallpass == PASS){
							ptr = LDBf(Ldb_PASSED);
						}
						else{
							ptr = LDBf(Ldb_FAILED);
						}
						len = strlen(ptr);
						for(i = 0; i < len; i++, ptr++){
							*(gReport+j+i) = *ptr;
							j++;
							*(gReport+j+i) = ' ';
						}
						while(*(gReport+j+i) == '~'){
							*(gReport+j+i) = ' ';
							j++;
						}
						break;
					} // end if( *(gReport+j) == '~')
				} // end for(j = 0; *(gReport+j); j++)
			} // end if(gReport != NULL)

			if(!ReportStringToFile()){
				return FALSE;
			}
			break;

		default:
			break;
	} // end switch(sts)

	return TRUE;
}

/*****************************************************************************/
/* Name:     ChangeReportScreenOption                                        */
/*                                                                           */
/* Purpose:  Change the report screen option                                 */
/*                                                                           */
/*                                                                           */
/* Example Call:   void ChangeReportScreenOption(int newVal);                */
/*                                                                           */
/* Input:    new screen option value                                         */
/*                                                                           */
/* Output:   <None>                                                          */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Globals Used:                                                             */
/*                                                                           */
/*                                                                           */
/* Return:   None                                                            */
/*                                                                           */
/*****************************************************************************/
void ChangeReportScreenOption(int newVal)
{
	gReportScreenOpt = newVal;

	return;
}

/*****************************************************************************/
/* Name:     ChangeReportFileOption                                          */
/*                                                                           */
/* Purpose:  Change the report screen option                                 */
/*                                                                           */
/*                                                                           */
/* Example Call:   void ChangeReportScreenOption(int newVal);                */
/*                                                                           */
/* Input:    new screen option value                                         */
/*                                                                           */
/* Output:   <None>                                                          */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Globals Used:                                                             */
/*                                                                           */
/*                                                                           */
/* Return:   None                                                            */
/*                                                                           */
/*****************************************************************************/
void ChangeReportFileOption(int newVal)
{
	gReportFileOpt = newVal;

	return;
}

/*****************************************************************************/
/* Name:    IncreaseTestReport                                               */
/*                                                                           */
/* Purpose:  Dynamically increase size of Test Report                        */
/*           if new string would exceed current size                         */
/*                                                                           */
/* Example Call:   static int GrowTestReportIfNeeded(char *newStr);          */
/*                                                                           */
/* Input:    char *newstr;                                                   */
/*                                                                           */
/* Output:   <None>                                                          */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Globals Used:                                                             */
/*                                                                           */
/*                                                                           */
/* Return:   TRUE/FALSE                                                      */                                                        
/*                                                                           */
/*****************************************************************************/
static int IncreaseTestReport(char *newStr)
{
	int growSize;

	if(gTestReport == NULL){
		if(strlen(newStr) > REPORT_GROW_SIZE){
			growSize = strlen(newStr) + REPORT_GROW_SIZE;
		}
		else{
			growSize = REPORT_GROW_SIZE;
		}
		gTestReport = (char*)calloc(1, growSize);
		sTestReportSize = growSize;
		return TRUE;
	}

	if(strlen(gTestReport) + strlen(newStr) + 2 >= sTestReportSize){
		if(strlen(newStr) > REPORT_GROW_SIZE){
			growSize = strlen(newStr) + REPORT_GROW_SIZE;
		}
		else{
			growSize = REPORT_GROW_SIZE;
		}
		gTestReport = realloc(gTestReport, sTestReportSize + growSize);

		if(gTestReport == NULL){
			CPVTMessagePopup(LDBf(Ldb_MSG_OUT_OF_MEMORY), LDBf(Ldb_MSG_RPT_NO_MORE_MEM), 
			                 OK_ONLY_DLG, NO_HELP);
			return FALSE;
		}
		else{
			sTestReportSize += growSize;
		}
	}

	return TRUE;
}

/*****************************************************************************/
/* Name:     StrDuplicate                                                    */
/*                                                                           */
/* Purpose: return a copy of the input string.                               */
/*                                                                           */
/* Example Call:   char *StrDuplicate(char *s);                              */
/*                                                                           */
/* Input:    string to copy                                                  */
/*                                                                           */
/* Output:   <None>                                                          */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Globals Used:                                                             */
/*                                                                           */
/*                                                                           */
/* Return:   copy of string                                                  */
/*                                                                           */
/*****************************************************************************/
char *StrDuplicate(char *s)
{
	char *temp;

	if(s == NULL){
		return NULL;
	}
	temp = (char *)malloc(strlen(s) + 1);
	strcpy(temp, s);

	return temp;
}
