#ifndef SstResultsSubScreen_HH
#define SstResultsSubScreen_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: SstResultsSubScreen - The screen selected by pressing the
// SST result tab button on the Upper Screens Select Area during Service
// Mode, or automatically displayed during SST Setup subscreen activation,
// or pressing the SST result log button on the Upper Other screen during
// normal ventilation mode.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/SstResultsSubScreen.hhv   25.0.4.0   19 Nov 2013 14:08:30   pvcs  $
//
//@ Modification-Log
//
//  Revision: 007  By:  hhd	   Date:  27-Apr-1999    DCS Number:  5322
//  Project:  ATC
//  Description:
//		Initial version.
//
//  Revision: 006  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 005  By:  hhd	   Date:  07-Oct-97    DR Number: 2540 
//    Project:  Sigma (R8027)
//    Description:
//		Removed code that supports commas in floating point format.
//
//  Revision: 004  By:  yyy    Date:  20-Aug-97    DR Number: 1988
//    Project:  Sigma (R8027)
//    Description:
//      Added handles for different pressure unit display.
//
//  Revision: 003  By:  hhd    Date:  18-AUG-97    DR Number: 2321
//       Project:  Sigma (R8027)
//       Description:
//	   Added decimalPointToComma_() method for all European languages, 
//		except for German.
//
//  Revision: 002  By:  yyy    Date:  02-JUL-97    DR Number: 2265
//    Project:  Sigma (R8027)
//    Description:
//      Removed hard coded strings for translation.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

//@ Usage-Classes
#include "Array_ResultTableEntry_MAX_TEST_ENTRIES.hh"
#include "LogTarget.hh"
#include "ServiceStatusArea.hh"
#include "SstTestResultData.hh"
#include "SubScreen.hh"
#include "SubScreenTitleArea.hh"
#include "TextField.hh"
#include "SettingObserver.hh"
class ScrollableLog;
//@ End-Usage


class SubScreenArea;

class SstResultsSubScreen : public SubScreen, public LogTarget,
							public SettingObserver
{
public:
	SstResultsSubScreen(SubScreenArea *pSubScreenArea);
	~SstResultsSubScreen(void);

	// Inherited from SubScreen
	virtual void activate(void);
	virtual void deactivate(void);

	// SettingObserver virtual method
    virtual void  valueUpdate(const Notification::ChangeQualifier qualifierId,
							  const SettingSubject*               pSubject);

	// Inherited from LogTarget
	virtual void getLogEntryColumn(Uint16 entryNumber, Uint16 colNumber,
								Boolean &rIsCheapText,
								StringId &rString1, StringId &rString2, 
								StringId &, StringId &);

	static void SoftFault(const SoftFaultID softFaultID,
								const Uint32      lineNumber,
								const char*       pFileName  = NULL, 
								const char*       pPredicate = NULL);

protected:

private:
	//@ Type: statusStringId_
	// Lists the possible test status states.
	enum statusStringId_
	{
		SST_NOT_APPLICABLE,
		SST_UNDEFINED,
		SST_INCOMPLETE,
		SST_NOT_INSTALLED,
		SST_PASSED,
		SST_OVERRIDDEN,
		SST_MINOR,
		SST_MAJOR
	};

	// these methods are purposely declared, but not implemented...
//	SstResultsSubScreen(const SstResultsSubScreen&);	// not implemented...
//	void operator=(const SstResultsSubScreen&);		// not implemented...
	void FormatStatusAndDate();

	// Establish references between log entries and SmTestIds.
	void setSstTestNameTable_();

	//@ Data-Member: pResultLog_ 
	// Test Result Log 
	ScrollableLog *pResultLog_;

	//@ Data-Member: sstDateTimeLabel_
	// Date and Time label of last SST test in the center of the screen
	TextField sstDateTimeLabel_;

	//@ Data-Member: sstDateTimeValue_
	// Date and Time value of last SST test in the center of the screen
	TextField sstDateTimeValue_;

	//@ Data-Member: sstStatus_
	// Status Area
	ServiceStatusArea sstStatus_;

	//@ Data-Member: sstTestData_
	// SST test data
	SstTestResultData	sstTestData_;

	//@ Constant: TestResultStrings_ 
	// Displayed text for test results
   	StringId TestResultStrings_[::NUM_TEST_RESULT_IDS];

	//@ Data-Member: titleArea_
	// The sub-screen's title at the top left of the screen
	SubScreenTitleArea titleArea_;

	//@ Data-Member: overallResult_
	// The overall result 
	TestResultId overallResult_;

	//@ Data-Member: sstTestId_
	// The SST Test Id table
	SmTestId::SstTestId sstTestId_[SmTestId::SST_TEST_MAX];

	//@ Data-Member: sstResTableEntries_
	// The result table for SST.
	FixedArray(ResultTableEntry, MAX_TEST_ENTRIES) sstResTableEntries_;

	//@ Data-Member: unitId_
	// The unit used based on settings.
	StringId unitId_;

};

#endif // SstResultsSubScreen_HH 
