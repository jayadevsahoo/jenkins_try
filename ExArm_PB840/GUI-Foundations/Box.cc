#include "stdafx.h"
//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1995, Puritan-Bennett Corporation
//====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: Box - This class is used to define a box object.
//---------------------------------------------------------------------
//@ Interface-Description
// The Box class implements a square or rectangular image.  Application code
// can position a box object anywhere within a Container, using the upper left
// corner of the Container as the origin.  The height, width, and border width
// are specified at construction time.  Note that changing the border width
// does not affect the overall size of the box; as the border width is
// increased, the border "grows" inwards.
//
// The Box class is derived from the Drawable class, and thus is "aware" of the
// screen management framework.  Border color (the color inherited from
// Drawable) is always initialized to black in Drawable's constructor.  Fill
// color is initialized to NO_COLOR in Box's constructor; this results in a
// "transparent" box.  Colors can be changed after construction using
// Box::setFillColor() and Drawable::setColor().
//---------------------------------------------------------------------
//@ Rationale
// This class is part of the GUI-Foundation and can have many uses.  It
// was deemed necessary to include this class in the GUI-Foundation
// library.
//---------------------------------------------------------------------
//@ Implementation-Description
// This class maintains internal variables that specify dimensions and
// other specifics about the Box, such as fill color and border width.
// Methods are provided to give access to this information.
//
// Box uses the VGA graphics library to draw itself.  It accepts calls
// to its draw() method from the screen management framework.  Changes
// to a Box will not result in changes on the screen until the
// application calls the BaseContainer::repaint() method, signaling the
// framework to update the screen.
//---------------------------------------------------------------------
//@ Fault-Handling
// This class uses the "contract programming" technique.  This technique
// utilizes the class assertion and class precondition macros provided by
// the Utilities subsystem to generate error messages in response to software
// faults.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Foundations/vcssrc/Box.ccv   25.0.4.0   19 Nov 2013 14:11:40   pvcs  $
//
//@ Modification-Log
//
//  Revision: 011   By: gdc    Date:  21-May-2007    SCR Number: 6330
//  Project: Trend
//  Description:
//	Removed SUN prototype code.
// 
//  Revision: 010   By: gdc    Date:  9-Oct-2000    DCS Number: 5493
//  Project: GuiComms
//  Description:
//	Added Single Screen Compatibility.
//
//  Revision: 009   By: gdc    Date:  29-Jun-1999    DCS Number: 5337
//  Project: 840 Neonatal
//  Description:
//	Optimized VGA library for Neonatal project to use BitBlt and Windows
//  Bitmap structures.
//
//  Revision: 008   By: sah    Date: 11-Jan-1999   DR Number: 5321
//  Project:  Sigma (R8027)
//  Description:
//      Changed 'SoftFault()' method to non-inline method.
//
//  Revision: 007  By: gdc     Date: 16-Jun-1998     DCS Number:
//  Project:  Color
//  Description:
//      Initial release.
//
//  Revision: 006  By: sah       Date: 06-Oct-1997    DCS Number: 2264
//  Project:  Sigma (R8027)
//  Description:
//      Fixed incorrect handling of VGA access requests/releases, which
//      may have caused the problem with the errant lines outside of the
//      waveform grid, by removing all semaphore requests/releases from
//      all of the 'draw()' methods.
//
//  Revision: 005  By: sah       Date: 12-Jun-1997    DCS Number: 2208
//    Project:  Sigma (R8027)
//    Description:
//      Incorporated new VGA interface methods, files and types.
//
//  Revision: 004  By: mpm       Date: 29-Mar-1996   DCR Number: 473
//    Project:  Sigma (R8027)
//    Description:
//      Added calls to GuiFoundation::RequestDisplayAccess() and ReleaseDisplayAccess()
//      in order to allow shared access to the display by separate drawing tasks.
//
//  Revision: 003  By: yyy       Date: 12-Jul-1995   DCR Number: 1949
//    Project:  Sigma (R8027)
//    Description:
//      Changed the Testable item number for the construction without
//      argument.
//
//  Revision: 002  By: yyy       Date: 16-May-1995   DCR Number: 1949
//    Project:  Sigma (R8027)
//    Description:
//      Added default constructor for Box class which takes no argument.
//
//  Revision: 001  By: yyy      Date: 01-Jan-1995  DR Number: NONE
//    Project:  Sigma (R8027)
//    Description:
//	Integration baseline.
//=====================================================================

#include "Box.hh"

//@ Usage-Classes
#include "BaseContainer.hh"
#include "Container.hh"
#include "DisplayContext.hh"
#include "GraphicsContext.hh"
//@ End-Usage

//@ Code...


//=====================================================================
//
// Public methods
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Box
//
//@ Interface-Description
// This is the constructor for the Box class.  The box origin (upper left
// corner) is given by the "xOrg" and "yOrg" parameters.  The "width" and
// "height" express the dimensions of the box.  The "penWidth" represents the
// border (frame) width.  The fill color is always initialized to NO_COLOR.
//---------------------------------------------------------------------
//@ Implementation-Description
// The inherited Drawable::area_ is initialized with the given position
// and dimension data.  The Box member variables are initialized here.
// There is no need to check the pen width against the current width nor the
// current height, due to the box draw() routine makes sure that the oversize
// penWidth will not cause undesirable drawing.
//---------------------------------------------------------------------
//@ PreCondition
// The given "width", "height", and "penWidth" must all be one or greater.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Box::Box(Int16 xOrg, Int16 yOrg, Int16 width, Int16 height, Int16 penWidth) :
	penWidth_(penWidth)
{
	CALL_TRACE("Box::Box(Int16 xOrg, Int16 yOrg, Int16 width, Int16 height, "
               " Int16 penWidth)");
	CLASS_PRE_CONDITION(width >= 1);
	CLASS_PRE_CONDITION(height >= 1);
	CLASS_PRE_CONDITION(penWidth >= 1);

	area_.setX(xOrg);
	area_.setY(yOrg);
	area_.setWidth(width);
	area_.setHeight(height);

	//				  								$[TI1]
	setFillColor(Colors::NO_COLOR);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Box [no arguments]
//
//@ Interface-Description
// Default constructor for the Box class.  Assigns default values to upper left
// corner of the box to (0,0), with width and height of 1 respectively.
// The pen width defaults to 1.  These coordinates are relative to
// the origin (upper left) of the parent container.
//---------------------------------------------------------------------
//@ Implementation-Description
// Initializes all data members to their default values then computes the
// bounding box using resetArea_().
//---------------------------------------------------------------------
//@ PreCondition
// none.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Box::Box(void) 
{
	CALL_TRACE("Box::Box(void)");
	
	// 							$[TI2]
	area_.setX(0);
	area_.setY(0);
	area_.setWidth(1);
	area_.setHeight(1);
	penWidth_ = 1;
	
	//
	setFillColor(Colors::NO_COLOR);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~Box
//
//@ Interface-Description
// Destroy object.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This destructor does nothing.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Box::~Box(void)
{
	CALL_TRACE("Box::~Box(void)");

	//				  								$[TI1]
	// do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setPenWidth 
//
//@ Interface-Description
// Sets the border width of the box to the given "penWidth" value.
// This does not affect the size of the box; the border "grows" inwards.
//---------------------------------------------------------------------
//@ Implementation-Description
// This method changes the object's state and thus causes a redraw. 
//---------------------------------------------------------------------
//@ PreCondition
// The given pen width must be one or greater.
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
Box::setPenWidth(Int16 penWidth)
{
	CALL_TRACE("Box::setPenWidth(Int16)");
	CLASS_PRE_CONDITION(penWidth >= 1);

	//				  								$[TI1]
	changing();

	penWidth_ = penWidth;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setFillColor
//
//@ Interface-Description
// Changes the interior color of this box to the given "fillColor".  If the
// fillColor is set to Colors::NO_COLOR, this prevents the fill area from
// being drawn, thus implementing a "transparent interior" rectangle.  
//---------------------------------------------------------------------
//@ Implementation-Description
// This method changes the object's state and causes a redraw.  If the
// fill color is a valid color, then mark this object as being "opaque".
// Otherwise, mark this object as being non-opaque (transparent).
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
Box::setFillColor(Colors::ColorS fillColor)
{
	CALL_TRACE("Box::setFillColor(Colors::ColorS)");
	CLASS_PRE_CONDITION((fillColor >= 0 &&
						 fillColor < Colors::NUM_COLORS
						)
						|| fillColor == Colors::NO_COLOR
					   );

	changing();
	
	fillColor_ = fillColor;
	
	if (fillColor_ == Colors::NO_COLOR)
	{	//													$[TI1]
		isOpaque_ = FALSE;
	}
	else
	{	//													$[TI2]
		isOpaque_ = TRUE;
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: draw [virtual]
//
//@ Interface-Description
// This draw routine renders the box on the VGA display.  It overrides the
// pure virtual method inherited from Drawable.  The passed parameter
// is ignored.
// The draw method shall only be called through the BaseContainer::repaint().
// The Container::draw() takes care of setting the clip area.
//---------------------------------------------------------------------
//@ Implementation-Description
// This method defines the VGA view for this object, based on the underlying
// Drawable::area_ region.  The relative coordinates for the Drawable::area_
// must be transformed into absolute (screen) coordinates.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
Box::draw(const Area&)
{
	CALL_TRACE("Box::draw(const Area&)");
	SAFE_CLASS_PRE_CONDITION(penWidth_ >= 1);

	DisplayContext & rDc = getBaseContainer()->getDisplayContext();
	GraphicsContext & rGc = getBaseContainer()->getGraphicsContext();

	Int x1 = getParentContainer()->convertToRealX(area_.getX1());
	Int y1 = getParentContainer()->convertToRealY(area_.getY1());
	Int x2 = getParentContainer()->convertToRealX(area_.getX2());
	Int y2 = getParentContainer()->convertToRealY(area_.getY2());

	Int minDimension = MIN_VALUE(area_.getWidth(), area_.getHeight() );

	// If Box is filled with same border and interior color, or
	// if border width is so wide that fill area does not appear:
	// just draw a solid rectangle of the border color.
	//
	Colors::ColorS color = getColor();
	
	rGc.setForegroundColor( color );
	rGc.setLineWidth( 1 );

	if ((fillColor_ == color) || (penWidth_ > (minDimension-1)/2))
	{	//													$[TI1]
		rDc.fillRectangle(rGc, x1, y1, x2, y2);
	}
	else
	{	//													$[TI2]
		//
		// Draw the Box borders as a series of lines.
		//
	  
		rGc.setLineWidth( penWidth_ );
		rDc.drawLine(rGc, x1, y1, x2, y1 );
		rDc.drawLine(rGc, x1, y2 - penWidth_ + 1, x2, y2 - penWidth_ + 1);
		rDc.drawLine(rGc, x1, y1 , x1, y2 );
		rDc.drawLine(rGc, x2 - penWidth_ + 1, y1, x2 - penWidth_+ 1, y2 );

		if (fillColor_ != Colors::NO_COLOR)
		{	//												$[TI2.1]
			//
			// Draw the interior of the Box.
			//
		    rGc.setForegroundColor( fillColor_ );

			rDc.fillRectangle(rGc,
								  x1 + penWidth_, y1 + penWidth_,
								  x2 - penWidth_, y2 - penWidth_  );
		}
		//													$[TI2.2]
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================
void
Box::SoftFault(const SoftFaultID  softFaultID,
			   const Uint32       lineNumber,
			   const char*        pFileName,
			   const char*        pPredicate)  
{
	CALL_TRACE("Box::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_FOUNDATIONS, BOX,
							lineNumber, pFileName, pPredicate);
}
