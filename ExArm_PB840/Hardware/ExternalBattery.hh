//====================================================================
// This is a proprietary work to which Covidien Plc claims exclusive 
// right. No part of this work may be used, disclosed, reproduced, 
// stored in an information retrieval system, or transmitted by any 
// means, electronic, mechanical, photocopying, recording, or otherwise 
// without the prior written permission of Covidien Plc.
//
//            Copyright (c) 2014-2025, Covidien Plc.
//====================================================================

#ifndef __External_Battery__
#define __External_Battery__

#include "Sigma.hh"

/// @class ExternalBattery
/// @detail Abstract class for External Battery Module, the class provides the 
/// default mechnisim for the following methods the isInstalled, isPowerSource,
/// readChargingStatus. Subclasses should override them if the mechnisim
/// is not the same.
class ExternalBattery
{
public:
    /// @detail Returns the single instance of the external battery. 
	/// The actual type depends on the hardware, but the callers do not need to 
	/// know it.
	/// @return The single, global instance of ExternalBattery. 
    static  ExternalBattery*    getInstance();

  	/// @detail Returns an unsigned short value of the prediccted remaining 
	/// battery with a range of 0 to 100%.
    /// @param socPercent - The percentage of the charging
    virtual bool readStateOfCharge( Uint16& socPercent ) = 0;

	/// @detail Returns the compensated battery capacity remaining. Units are
	/// 1 mAh per bit.
    /// @param remainingCapacity - The remaining capacity
	virtual bool readRemainingCapacity( Uint16& remainingCapacity ) = 0;

	/// @detail Returns the compensated capacity of the battery when fully 
	/// charged. Units are 1 mAh per bit
    /// @param fullChargeCapacity - The full charge capacity
	virtual bool readFullChargeCapacity( Uint32& fullChargeCapacity ) = 0;

	/// @detail Returns an unsigned short value of the measured voltage 
	/// in mV with a range of 0 V to 65535 mV.
    /// @param voltage - The voltage
	virtual bool readVoltage( Uint16& voltage ) = 0;

	/// @detail Returns a signed integer value that is the average current  
	/// flow. Units are 1 mA per bit
    /// @param averageCurrent - The average current
	virtual bool readAverageCurrent( Int32& averageCurrent ) = 0;

    /// @detail Returns the instantaneous current measured
    /// @param instCurrent - The instantaneous current
    virtual bool readInstantaneousCurrent( Int32& instCurrent ) = 0;

	/// @detail Returns an unsigned integer value of the temperature in 
	/// units of 0.1K with a range of 0 to 6553.5K.
    /// @param temperature - The temperature of the battery
	virtual bool readTemperature( Uint16& temperature ) = 0;

    /// @detail Check if the External Battery is installed
    /// @return true if battery is installed
    virtual bool isInstalled( void );

    /// @detail Check if current Battery is the Power Source of Ventilator
    /// @return true if battery is the power source
    virtual bool isPowerSource( void );

    /// @detail Check if current Battery is under charging
    /// @return true if battery is charging
    virtual bool isCharging( void );

protected:

    ExternalBattery(void);
    virtual ~ExternalBattery(void);

private:

    static ExternalBattery*    instance_;
};

#endif //__External_Battery__