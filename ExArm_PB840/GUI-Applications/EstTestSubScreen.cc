#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: EstTestSubScreen - The subscreen selected by the EST button in
// the Lower Other Screens subscreen.  Allows conducting EST test.
//---------------------------------------------------------------------
//@ Interface-Description
// This subscreen displays EST test information (status data) while
// EST is running, and provides a means of conveying operator prompts from 
// the Service-Mode subsystem to the operator.
// The activate()/deactivate() methods are called automatically on
// display/removal of this subscreen by our subscreen area owner.  The
// buttonDownHappened() and buttonUpHappened() are called when any button
// in the subscreen is pressed.  Timer events  are passed into timerEventHappened().
// Adjust Panel events are communicated via the various Adjust Panel
// "happened" methods.  NovRam events are passed into novRamUpdateEventHappened().
// Register all test status and prompt to Service-Data subsystem and process
// these data by various GuiTestManager methods.
//---------------------------------------------------------------------
//@ Rationale
// Implements the complete functionality of the EstTestSubScreen in
// a single class.
//---------------------------------------------------------------------
//@ Implementation-Description
// The processing of the EstTestSubscreen is based on a pre-defined internal
// state table.  All the possible state and events are defined in the header
// of this file.  Upon the activation of this subscreen, we set the state to
// INITIAL_STATE, and reset all the variables to the initial values.  The
// subsequent event can happen either by external input (timer event, keyboard
// and knob input, button input, test control input, etc) or internal control
// (auto event).
// We drop into the next state wenever a new event occures.  Appropriated
// processing (screen update, control of test flow, etc) will be executed to
// maintain the requirements for this state transition process.
//
// While running the EST test, the upper screen shall display the test data.
// We simply call setupTestResultOnUpperScreen_() to activate the Upper Screen
// Area, followed by calling setupUpperScreenLayout_ to setup the exact title
// and data format for the specific running test.
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and
// pre-conditions to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
// Only one instance of this class should be created.  It should only be
// displayed in the LowerSubScreenArea.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/EstTestSubScreen.ccv   25.0.4.0   19 Nov 2013 14:07:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 024   By: mnr   Date:  22-Mar-2010     SCR Number: 6559
//  Project:  NEO
//  Description:
//      Clear PA_LOW priority prompts to resolve left-over prompt issues.
//
//  Revision: 023   By: mnr   Date:  23-Oct-2009     SCR Number: 6528
//  Project:  S840BUILD3
//  Description:
//      Long EST prompt string broken up into 3 strings to make RUSSIAN
//	    and POLISH work.
//
//  Revision: 022   By: rhj   Date:  04-Apr-2009     SCR Number: 6495 
//  Project:  840S2
//  Description:
//      Added single test mode.
//
//  Revision: 021   By: gdc    Date:  04-Feb-2008    SCR Number: 6422
//  Project: Trend
//  Description:
//		Register for the setting change timeout to prevent the
//		TimeoutSubScreen from releasing focus during EST.
//
//  Revision: 020   By: gdc    Date:  26-May-2007    SCR Number: 6330
//  Project: Trend
//  Description:
//		Removed SUN prototype code.
//
//  Revision: 019   By: gdc    Date:  26-May-2007    SCR Number: 6237
//  Project: Trend
//  Description:
//		Trend related changes. getLogEntryColumn changed to set correct 
//      value for isCheapText. Date localization (rhj).
//
//  Revision: 018   By: gdc    Date:  29-Jun-1999    DCS Number: 5327
//  Project: 840 Neonatal
//  Description:
//	Optimized graphics library. Implemented Direct Draw mode for drawing
//  outside of BaseContainer refresh cycle.
//
//  Revision: 017  By:  gdc	   Date:  23-Nov-1998    DCS Number: 5173, 5178
//  Project:  BiLevel
//  Description:
//		Changed to deactivate scrollbar and data select buttons when 
//		the start test button has been pressed. This prevents the 
//		scrollbar and start test buttons from being pressed at the 
//		same time. (DCS 5173)
//		Changed "Ventilator requires service!" advisory to low
//		priority so it won't be cleared when the scrollbar prompt
//		appears. (DCS 5178)
//
//  Revision: 016  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 015  By:  yyy    Date: 16-Oct-1997    DR Number: DCS 2565
//  Project:  Sigma (840)
//  Description:
//		Mapped new SRS.
//
//  Revision: 014  By:  yyy    Date: 02-Oct-1997    DR Number: DCS 2214
//  Project:  Sigma (840)
//  Description:
//  	Added a overrideCounter_ to accumulate number of times after the user
//      requested to override the EST alert based on persistent objects.  When
//		the counter reaches to 2 we shall update the overall status for the
//		override operation.
//
//  Revision: 013  By:  yyy    Date: 01-Oct-1997    DR Number: DCS 2204
//  Project:  Sigma (840)
//  Description:
//       Added PromptId DISCONNECT_AIR_AND_O2_PROMPT for revised
//       EST Battery Test.
//
//  Revision: 012  By:  gdc	   Date:  24-SEP-1997    DCS Number: 2513 
//  Project:  Sigma (R8027)
//  Description:
//      Cleaned up Service-Mode interfaces in support of service/
//      manufacturing test.
//
//  Revision: 011  By:  hhd	   Date:  23-SEP-1997    DCS Number: 2265 
//  Project:  Sigma (R8027)
//  Description:
//      Fixed problem with scrollbar's width.
//
//  Revision: 010  By:  yyy    Date:  28-AUG-1997    DCS Number: 1803
//  Project:  Sigma (R8027)
//  Description:
//      Recover revision 6.
//
//  Revision: 009  By:  yyy    Date:  28-AUG-1997    DCS Number: 1803
//  Project:  Sigma (R8027)
//  Description:
//      undo revision 6 per "7C" release.
//
//  Revision: 008  By:  yyy    Date:  13-AUG-1997    DCS Number: 2365
//  Project:  Sigma (R8027)
//  Description:
//      Use the appropriated ResultEntryData method to query the test running
//		time.
//
//  Revision: 007  By:  yyy    Date:  12-AUG-1997    DCS Number: 2213
//  Project:  Sigma (R8027)
//  Description:
//      12-AUG-1997 Added test prompt handler for SmPromptId::USER_EXIT.
//		16-Sep-1997 Added EXECUTE_NO_ACT_PROMPT_STATE, NO_ACT_PAUSE_REQUEST_STATE
//		and NO_ACT_PAUSE_PROMPT_STATE to handle the knob and keyboard tests.
//		These two tests do not require ACCEPT key input to notify service
//		mode that a request had been serviced.
//
//  Revision: 006  By:  yyy    Date:  11-AUG-1997    DCS Number: 1803
//  Project:  Sigma (R8027)
//  Description:
//      Removed NovRamEventRegistrar::RegisterTarget() call from activate()
//		to constructor.
//
//  Revision: 005  By:  yyy    Date:  30-JUL-97    DR Number: 2205
//    Project:  Sigma (R8027)
//    Description:
//      Updated the error for exh valve velocity transducer test.
//
//  Revision: 004  By:  yyy    Date:  30-JUL-97    DR Number: 2013
//    Project:  Sigma (R8027)
//    Description:
//      Updated the error for Nurse Call test.
//
//  Revision: 003  By:  yyy    Date:  28-JUL-97    DR Number: 2265
//    Project:  Sigma (R8027)
//    Description:
//      Changed the error message from one to two lines.
//
//  Revision: 002  By: yyy      Date: 07-May-1997  DR Number: 2059
//    Project:  Sigma (R8027)
//    Description:
//      In execute_state_() when executing the EXIT_FALLBACK_STATE, if the
//		current state is already in DO_TEST_STATE then do nothing.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "EstTestSubScreen.hh"

#include "ServiceUpperScreen.hh"
#include "ServiceUpperScreenSelectArea.hh"
#include "LowerScreen.hh"
#include "ServiceLowerScreen.hh"
#include "ServiceLowerScreenSelectArea.hh"

#if defined FAKE_SM
#	include "FakeServiceModeManager.hh"
#   define SmManager FakeServiceModeManager
#else		
#	include "SmManager.hh"
#endif	// FAKE_SM

#include "ServiceDataMgr.hh"

//@ Usage-Classes
#include "GuiTimerId.hh"
#include "GuiTimerRegistrar.hh"
#include "AdjustPanel.hh"
#include "MiscStrs.hh"
#include "SubScreenArea.hh"
#include "Sound.hh"
#include "Colors.hh"
#include "TextUtil.hh"
#include "ResultEntryData.hh"
#include "NovRamEventRegistrar.hh"
#include "DiagnosticFault.hh"
//@ End-Usage

//@ Code...


// Initialize static constants.
static const Uint16 BUTTON_Y_ = 364;
static const Uint16 SMALL_BUTTON_WIDTH_ = 84;
static const Uint16 BIG_BUTTON_WIDTH_ = 110;
static const Uint16 BUTTON_HEIGHT_ = 34;
static const Uint8  BUTTON_BORDER_ = 3;
static const Uint16 START_TEST_BUTTON_X_ = 520;
static const Uint16 EXIT_BUTTON_X_ = 5;
static const Uint16 REPEAT_BUTTON_X_ = 456;
static const Uint16 NEXT_BUTTON_X_ = 546;
static const Uint32 LOG_Y_ = 33;
static const Int32 EST_STATUS_LABEL_X_ = 200;
static const Int32 EST_STATUS_Y_ = 0;
static const Int32 EST_STATUS_WIDTH_ = 300;
static const Int32 EST_STATUS_HEIGHT_ = 31;
static const Int16 EST_ROW_HEIGHT_ = 38;
static const Int16 EST_COL_1_WIDTH_ = 56;
static const Int16 EST_COL_2_WIDTH_ = 114;
static const Int16 EST_COL_3_WIDTH_ = 313;
static const Int16 EST_COL_4_WIDTH_ = 80;
static const Int16 EST_COL_5_WIDTH_ = 50;
static const Uint16 SINGLE_START_TEST_BUTTON_X_ = START_TEST_BUTTON_X_ - BIG_BUTTON_WIDTH_ - 15;

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: EstTestSubScreen()  [Default Constructor]
//
//@ Interface-Description
// Creates the EstTestSubscreen.  Passed a pointer to the subscreen
// area which creates it (Service's LowerSubScreenArea). 
//---------------------------------------------------------------------
//@ Implementation-Description
// Sizes, positions and colors the subscreen container.
// Creates the various buttons displayed in the subscreen and adds them
// to the container.  Registers for callbacks from each button.  Do
// necessary setups for buttons on the subscreen display.  Register for
// timer event callbacks.  Setup the test name, prompt and status tables.
// Most importantly, setup the state transition table.
// $[LC07001] The opening EST subscreen shall display ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

EstTestSubScreen::EstTestSubScreen(SubScreenArea *pSubScreenArea) :
		SubScreen(pSubScreenArea),
		titleArea_(MiscStrs::EXTENDED_SELF_TEST_SUBSCREEN_TITLE),
		startTestButton_(START_TEST_BUTTON_X_, 	BUTTON_Y_,
						BIG_BUTTON_WIDTH_,		BUTTON_HEIGHT_,
						Button::DARK, 			BUTTON_BORDER_,
						Button::SQUARE,			Button::NO_BORDER,
						MiscStrs::ALL_TEST_TAB_BUTTON_LABEL),
		exitButton_(	EXIT_BUTTON_X_,		 	BUTTON_Y_,
						SMALL_BUTTON_WIDTH_,	BUTTON_HEIGHT_,
						Button::DARK, 			BUTTON_BORDER_,
						Button::SQUARE,			Button::NO_BORDER,
						MiscStrs::EST_EXIT_MSG),
		repeatButton_(	REPEAT_BUTTON_X_,	 	BUTTON_Y_,
						SMALL_BUTTON_WIDTH_,		BUTTON_HEIGHT_,
						Button::DARK, 			BUTTON_BORDER_,
						Button::SQUARE,			Button::NO_BORDER,
						MiscStrs::EST_REPEAT_MSG),
		nextButton_(	NEXT_BUTTON_X_, 		BUTTON_Y_,
						SMALL_BUTTON_WIDTH_,		BUTTON_HEIGHT_,
						Button::DARK, 			BUTTON_BORDER_,
						Button::SQUARE,			Button::NO_BORDER,
						MiscStrs::EST_NEXT_MSG),
		overrideButton_(NEXT_BUTTON_X_,	 		BUTTON_Y_,
						SMALL_BUTTON_WIDTH_,	BUTTON_HEIGHT_,
						Button::DARK, 			BUTTON_BORDER_,
						Button::SQUARE,			Button::NO_BORDER,
						MiscStrs::EST_OVERRIDE_MSG),
		pCurrentButton_(NULL),
		estStatus_(EST_STATUS_LABEL_X_, EST_STATUS_Y_,
						EST_STATUS_WIDTH_, EST_STATUS_HEIGHT_,
						MiscStrs::EST_STATUS_TITLE,
						NULL_STRING_ID),
		log_(this, EST_DISPLAY_ROW_MAX, EST_DISPLAY_COL_MAX, EST_ROW_HEIGHT_),
		pEstResultSubScreen_(NULL),
		pUpperSubScreenArea_(NULL),
		testManager_(this),
		reviewMode_(TRUE),
		estCurrentTestIndex_(EST_NO_TEST_ID),
		maxServiceModeTest_(0),
		clearLogEntry_(FALSE),
		savedPromptName_(NULL),
		promptId_(SmPromptId::NULL_PROMPT_ID),
		keyAllowedId_(SmPromptId::NULL_KEYS_ALLOWED_ID),
		conditionId_(0),
		userKeyPressedId_(SmPromptId::NULL_ACTION_ID),
		isThisEstTestCompleted_(FALSE),
		mostSevereStatusId_(PASSED_TEST_ID),
		overallStatusId_(PASSED_TEST_ID),
		previousState_(INITIAL_STATE),
		nextState_(INITIAL_STATE),
		fallBackState_(INITIAL_STATE),
		overrideRequested_(FALSE),
		overrideCounter_(-1),
		repeatTestRequested_(FALSE),
		RecursionCount_(0),
		singleTestButton_(SINGLE_START_TEST_BUTTON_X_, 	BUTTON_Y_,
						BIG_BUTTON_WIDTH_,		BUTTON_HEIGHT_,
						Button::DARK, 			BUTTON_BORDER_,
						Button::SQUARE,			Button::NO_BORDER,
						MiscStrs::SINGLE_START_TEST_TAB_BUTTON_LABEL),
		newEvent_(-1)
{
	CALL_TRACE("EstTestSubScreen::EstTestSubScreen(pSubScreenArea)");

#if defined FORNOW
printf("EstTestSubScreen::() at line  %d, this = %d\n", __LINE__, this);
#endif	// FORNOW

	// Size and position the sub-screen
	setX(0);
	setY(0);
	setWidth(SERVICE_LOWER_SUB_SCREEN_AREA_WIDTH);
	setHeight(SERVICE_LOWER_SUB_SCREEN_AREA_HEIGHT);

	// Get the EST result subscreen address.
	pEstResultSubScreen_ = UpperSubScreenArea::GetEstResultSubScreen();

	// Get the EST service Upper subScreen area address.
	pUpperSubScreenArea_ = ServiceUpperScreen::RServiceUpperScreen.
							getUpperSubScreenArea();


	// Register for callbacks for all buttons in which we are interested.
	startTestButton_.setButtonCallback(this);
	exitButton_.setButtonCallback(this);
	repeatButton_.setButtonCallback(this);
	nextButton_.setButtonCallback(this);
	overrideButton_.setButtonCallback(this);
	singleTestButton_.setButtonCallback(this);

	// Setup the test name, test result title, and prompt table.
	setEstTestNameTable_();
	setEstTestPromptTable_();

	// Setup the display page 
	log_.setEntryInfo(EST_TEST_START_ID, maxServiceModeTest_);

	// Size and position the log
	log_.setY(LOG_Y_);

	// Setup the column titles
	log_.setColumnInfo(COL_1_TIME, MiscStrs::EST_LOG_COL_1_TITLE,
					EST_COL_1_WIDTH_, CENTER, 0, TextFont::NORMAL,
					TextFont::TEN, TextFont::TEN);
	log_.setColumnInfo(COL_2_TEST, MiscStrs::EST_LOG_COL_2_TITLE,
					EST_COL_2_WIDTH_, LEFT, 2, TextFont::NORMAL,
					TextFont::EIGHT, TextFont::EIGHT);
	log_.setColumnInfo(COL_3_DIAG_CODE, MiscStrs::EST_LOG_COL_3_TITLE,
					EST_COL_3_WIDTH_, LEFT, 2, TextFont::NORMAL,
					TextFont::TEN, TextFont::TEN);
	log_.setColumnInfo(COL_4_RESULT, MiscStrs::EST_LOG_COL_4_TITLE,
					EST_COL_4_WIDTH_, CENTER, 0, TextFont::NORMAL,
					TextFont::TEN, TextFont::TEN);
	log_.setColumnInfo(COL_5_DATA, MiscStrs::EST_LOG_COL_5_TITLE,
					EST_COL_5_WIDTH_, CENTER, 0, TextFont::NORMAL,
					TextFont::TEN, TextFont::TEN);

	// Setup the test logs to scrollable and selectable
	log_.setUserScrollable(TRUE);
	log_.setUserSelectable(TRUE);

	// Add log objects to main container
	addDrawable(&log_);

	// Add the title area
	addDrawable(&titleArea_);

	// Add the buttons.
	addDrawable(&startTestButton_);
    addDrawable(&singleTestButton_);

	// Hide the following drawables
	exitButton_.setShow(FALSE);
	repeatButton_.setShow(FALSE);
	nextButton_.setShow(FALSE);
	overrideButton_.setShow(FALSE);
    singleTestButton_.setShow(FALSE);
	
	// Add the following drawables
	addDrawable(&exitButton_);
	addDrawable(&repeatButton_);
	addDrawable(&nextButton_);
	addDrawable(&overrideButton_);

	// Add status line text objects to main container
	addDrawable(&estStatus_);

	//
	// Register for timer event callbacks.
	//
	GuiTimerRegistrar::RegisterTarget(GuiTimerId::SERVICE_MODE_TEST_PAUSE_TIMEOUT, this);

	// Register to the NovRam Register
	// We have to do registration at activation time to guarantee this
	// subscreen will get the callback notice.
	// 
	NovRamEventRegistrar::RegisterTarget(NovRamUpdateManager::EST_RESULT_TABLE_UPDATE,
							   this);
	NovRamEventRegistrar::RegisterTarget(NovRamUpdateManager::EST_LOG_UPDATE,
							   this);

	// Associate the testResults object with the pointer to the test result
	// array.
	testManager_.setupTestCommandArray();

	// Setup the status decoding table information
	estStatusDecodingTable_[SmStatusId::TEST_END] = PASSED_TEST_ID;
	estStatusDecodingTable_[SmStatusId::TEST_START] = UNDEFINED_TEST_RESULT_ID;
	estStatusDecodingTable_[SmStatusId::TEST_ALERT] = MINOR_TEST_FAILURE_ID;
	estStatusDecodingTable_[SmStatusId::TEST_FAILURE] = MAJOR_TEST_FAILURE_ID;
	estStatusDecodingTable_[SmStatusId::TEST_OPERATOR_EXIT] = INCOMPLETE_TEST_RESULT_ID;
	estStatusDecodingTable_[SmStatusId::NOT_INSTALLED] = NOT_INSTALLED_TEST_ID;

	// Initialize the estStateTable
	for (Int state = 0; state < EST_STATE_NUM; state++)
	{
		for (Int event = 0; event < EST_EVENT_NUM; event++)
		{
			estStateTable_[state][event] = UNDEFINED_NEXT_TEST;
		}
	}

	// Build up the estStateTable
	estStateTable_[INITIAL_STATE]			[START_EVENT]				= START_STATE;
    estStateTable_[INITIAL_STATE]           [SINGLE_EVENT]              = START_SINGLE_STATE;

	estStateTable_[START_STATE]				[START_EVENT]				= INITIAL_STATE;
	estStateTable_[START_STATE]				[ACCEPT_EVENT] 				= READY_TO_TEST_STATE;

	estStateTable_[START_SINGLE_STATE]      [ACCEPT_EVENT]              = READY_TO_TEST_STATE;
	estStateTable_[START_SINGLE_STATE]      [CLEAR_EVENT]               = START_SINGLE_STATE;
	estStateTable_[START_SINGLE_STATE]      [SINGLE_EVENT]              = START_SINGLE_STATE;

	estStateTable_[READY_TO_TEST_STATE]		[AUTO_EVENT] 				= DO_TEST_STATE;
    estStateTable_[READY_TO_TEST_STATE]     [USER_PROMPT_EVENT]         = BD_PROMPT_STATE;

	estStateTable_[BD_PROMPT_STATE]		    [ACCEPT_EVENT] 				= BD_PROMPT_STATE;
	estStateTable_[BD_PROMPT_STATE]		    [EXIT_EVENT] 				= EXIT_REQUEST_STATE;
	estStateTable_[BD_PROMPT_STATE]		    [AUTO_EVENT] 				= DO_TEST_STATE;

	estStateTable_[DO_TEST_STATE]			[EXIT_EVENT]			 	= PAUSE_REQUEST_STATE;
	estStateTable_[DO_TEST_STATE]			[USER_PROMPT_EVENT] 		= PROMPT_STATE;
	estStateTable_[DO_TEST_STATE]			[END_ALERT_EVENT] 			= END_ALERT_STATE;
	estStateTable_[DO_TEST_STATE]			[END_FAILURE_EVENT] 		= END_FAILURE_STATE;
	estStateTable_[DO_TEST_STATE]			[USER_END_EVENT] 			= TEST_PASS_STATE;

	estStateTable_[PROMPT_STATE]			[ACCEPT_EVENT] 				= EXECUTE_PROMPT_STATE;
	estStateTable_[PROMPT_STATE]			[CLEAR_EVENT] 				= EXECUTE_PROMPT_STATE;
	estStateTable_[PROMPT_STATE]			[NO_KEY_EVENT] 				= EXECUTE_NO_ACT_PROMPT_STATE;
	estStateTable_[PROMPT_STATE]			[EXIT_EVENT] 				= EXIT_REQUEST_STATE;

	estStateTable_[END_ALERT_STATE]			[EXIT_EVENT] 				= EXIT_REQUEST_STATE;
	estStateTable_[END_ALERT_STATE]			[REPEAT_EVENT] 				= REPEAT_STATE;
	estStateTable_[END_ALERT_STATE]			[NEXT_EVENT] 				= NEXT_STATE;

	estStateTable_[END_FAILURE_STATE]		[EXIT_EVENT] 				= EXIT_REQUEST_STATE;
	estStateTable_[END_FAILURE_STATE]		[REPEAT_EVENT] 				= REPEAT_STATE;
	estStateTable_[END_FAILURE_STATE]		[NEXT_EVENT] 				= NEXT_STATE;

	estStateTable_[TEST_PASS_STATE]			[EXIT_EVENT]				= EXIT_REQUEST_STATE;
	estStateTable_[TEST_PASS_STATE]			[USER_END_EVENT]			= TEST_PASS_STATE;
	estStateTable_[TEST_PASS_STATE]			[TIMEOUT_EVENT]				= DO_NEXT_STATE;

	estStateTable_[EXECUTE_PROMPT_STATE]	[AUTO_EVENT] 				= DO_TEST_STATE;

	estStateTable_[EXECUTE_NO_ACT_PROMPT_STATE]	[AUTO_EVENT] 			= DO_TEST_STATE;
	estStateTable_[EXECUTE_NO_ACT_PROMPT_STATE]	[EXIT_EVENT]			= NO_ACT_PAUSE_REQUEST_STATE;
	estStateTable_[EXECUTE_NO_ACT_PROMPT_STATE]	[USER_PROMPT_EVENT] 	= PROMPT_STATE;
	estStateTable_[EXECUTE_NO_ACT_PROMPT_STATE]	[END_ALERT_EVENT] 		= END_ALERT_STATE;
	estStateTable_[EXECUTE_NO_ACT_PROMPT_STATE]	[END_FAILURE_EVENT] 	= END_FAILURE_STATE;
	estStateTable_[EXECUTE_NO_ACT_PROMPT_STATE]	[USER_END_EVENT] 		= TEST_PASS_STATE;

	estStateTable_[EXIT_REQUEST_STATE]		[ACCEPT_EVENT] 				= EXECUTE_EXIT_EST_STATE;
	estStateTable_[EXIT_REQUEST_STATE]		[EXIT_EVENT] 				= EXIT_FALLBACK_STATE;
	estStateTable_[EXIT_REQUEST_STATE]		[TIMEOUT_EVENT]				= EXIT_REQUEST_STATE;
	estStateTable_[EXIT_REQUEST_STATE]		[REPEAT_EVENT] 				= REPEAT_STATE;
	estStateTable_[EXIT_REQUEST_STATE]		[NEXT_EVENT] 				= NEXT_STATE;
	estStateTable_[EXIT_REQUEST_STATE]		[OVERRIDE_EVENT]			= OVERRIDE_STATE;

	estStateTable_[REPEAT_STATE]			[ACCEPT_EVENT] 				= EXECUTE_REPEAT_STATE;
	estStateTable_[REPEAT_STATE]			[EXIT_EVENT] 				= EXIT_REQUEST_STATE;
	estStateTable_[REPEAT_STATE]			[REPEAT_EVENT]  			= EXIT_FALLBACK_STATE;
	estStateTable_[REPEAT_STATE]			[NEXT_EVENT]  				= NEXT_STATE;

	estStateTable_[NEXT_STATE]				[ACCEPT_EVENT] 				= DO_NEXT_STATE;
	estStateTable_[NEXT_STATE]				[EXIT_EVENT] 				= EXIT_REQUEST_STATE;
	estStateTable_[NEXT_STATE]				[REPEAT_EVENT]  			= REPEAT_STATE;
	estStateTable_[NEXT_STATE]				[NEXT_EVENT]  				= EXIT_FALLBACK_STATE;

	estStateTable_[DO_NEXT_STATE]			[AUTO_EVENT] 				= DO_TEST_STATE;
	estStateTable_[DO_NEXT_STATE]			[AUTO_END_OF_TEST_EVENT]	= END_OF_EST_STATE;

	estStateTable_[OVERRIDE_STATE]			[ACCEPT_EVENT]				= EXEC_OVERRIDE_STATE;
	estStateTable_[OVERRIDE_STATE]			[EXIT_EVENT]				= EXIT_REQUEST_STATE;
	estStateTable_[OVERRIDE_STATE]			[OVERRIDE_EVENT]			= EXIT_FALLBACK_STATE;

	estStateTable_[END_OF_EST_STATE]		[OVERRIDE_EVENT] 			= OVERRIDE_STATE;
	estStateTable_[END_OF_EST_STATE]		[EXIT_EVENT] 				= EXIT_REQUEST_STATE;
	estStateTable_[END_OF_EST_STATE]		[AUTO_EVENT] 				= RESTART_EST_STATE;

	estStateTable_[PAUSE_REQUEST_STATE]		[EXIT_EVENT] 				= EXIT_FALLBACK_STATE;
	estStateTable_[PAUSE_REQUEST_STATE]		[USER_PROMPT_EVENT] 		= EXIT_BEFORE_PROMPT_STATE;
	estStateTable_[PAUSE_REQUEST_STATE]		[END_ALERT_EVENT] 			= END_EXIT_ALERT_STATE;
	estStateTable_[PAUSE_REQUEST_STATE]		[END_FAILURE_EVENT] 		= END_EXIT_FAILURE_STATE;
	estStateTable_[PAUSE_REQUEST_STATE]		[USER_END_EVENT] 			= END_EXIT_TEST_STATE;

	estStateTable_[NO_ACT_PAUSE_REQUEST_STATE][EXIT_EVENT] 				= EXIT_FALLBACK_STATE;
	estStateTable_[NO_ACT_PAUSE_REQUEST_STATE][USER_PROMPT_EVENT] 		= NO_ACT_PAUSE_PROMPT_STATE;
	estStateTable_[NO_ACT_PAUSE_REQUEST_STATE][END_ALERT_EVENT] 		= END_EXIT_ALERT_STATE;
	estStateTable_[NO_ACT_PAUSE_REQUEST_STATE][END_FAILURE_EVENT] 		= END_EXIT_FAILURE_STATE;
	estStateTable_[NO_ACT_PAUSE_REQUEST_STATE][USER_END_EVENT] 			= END_EXIT_TEST_STATE;

	estStateTable_[NO_ACT_PAUSE_PROMPT_STATE][AUTO_EVENT] 				= NO_ACT_PAUSE_REQUEST_STATE;
	estStateTable_[NO_ACT_PAUSE_PROMPT_STATE][NO_KEY_EVENT] 			= NO_ACT_PAUSE_REQUEST_STATE;

	estStateTable_[EXIT_BEFORE_PROMPT_STATE][EXIT_EVENT]				= PROMPT_STATE;
	estStateTable_[EXIT_BEFORE_PROMPT_STATE][ACCEPT_EVENT] 				= EXECUTE_EXIT_EST_STATE;

	estStateTable_[END_EXIT_ALERT_STATE]	[AUTO_EVENT] 				= EXIT_REQUEST_STATE;
	estStateTable_[END_EXIT_FAILURE_STATE]	[AUTO_EVENT] 				= EXIT_REQUEST_STATE;
	estStateTable_[END_EXIT_TEST_STATE]		[AUTO_EVENT] 				= EXIT_REQUEST_STATE;

	estStateTable_[EXECUTE_REPEAT_STATE]	[AUTO_EVENT]				= DO_TEST_STATE;
	estStateTable_[EXECUTE_EXIT_EST_STATE]	[USER_END_EVENT] 			= EXIT_EST_STATE;

	estStateTable_[EXIT_EST_STATE]			[AUTO_EVENT] 				= UNDEFINED_NEXT_TEST;


											// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~EstTestSubScreen  [Destructor]
//
//@ Interface-Description
// Destroys EstTestSubScreen.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

EstTestSubScreen::~EstTestSubScreen(void)
{
	CALL_TRACE("EstTestSubScreen::~EstTestSubScreen(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activate
//
//@ Interface-Description
// Called by our subscreen area before this subscreen is to be displayed
// allowing us to do any necessary setup. Needs no parameters.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Initializes the subscreen display.  Registers all possible test commands
//	and results for test events.  Initializes all subscreen state variables.
//	Set test prompts.  Setup the test logs to be scrollable and selectable.
//  Registers for NovRam event callbacks.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
EstTestSubScreen::activate(void)
{
	CALL_TRACE("EstTestSubScreen::activate(void)");

#if defined FORNOW
printf("EstTestSubScreen::activate(void)\n");
#endif	// FORNOW

    showBdTestPrompt_ = FALSE;
    showAirO2ConnectedPrompt_ = TRUE;
    wasBdTestPromptShown_ = FALSE;

	RecursionCount_ = 0;

	// Always review the previous test result.
	reviewMode_ = TRUE;
	
	// Release Adjust Panel focus if exists.
	AdjustPanel::TakeNonPersistentFocus(NULL);

	// Disable the knob sound.
	AdjustPanel::DisableKnobRotateSound();

	// register for the setting change timeout to grab the callback 
	// away from the TimeoutSubScreen and prevent it from timing out
	// our EST prompts
	GuiTimerRegistrar::RegisterTarget(GuiTimerId::SUBSCREEN_SETTING_CHANGE_TIMEOUT, this);

	// Register all general possible test results for test events
	testManager_.registerTestCommands();

	// Setup the display page 
	log_.setEntryInfo(EST_TEST_START_ID, maxServiceModeTest_);

	// Setup the test logs to scrollable and selectable
	log_.setUserScrollable(TRUE);
	log_.setUserSelectable(TRUE);

	// Get Est test result table from NovRam
    NovRamManager::GetEstResultEntries(estResultTable_);

	// Activate the log
	log_.activate();

	nextState_ = INITIAL_STATE;
	layoutScreen_(EST_SETUP_SCREEN);

	isSingleTestMode_ = FALSE;
	titleArea_.setTitle(MiscStrs::EXTENDED_SELF_TEST_SUBSCREEN_TITLE);
											// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: deactivate
//
//@ Interface-Description
// Called by our subscreen area just after this subscreen is removed from
// the display allowing us to do any necessary cleanup. Needs no parameters.
//---------------------------------------------------------------------
//@ Implementation-Description
// Clears any prompts which may have been displayed and clears the Adjust Panel
// focus in case we had it.  Also clear the selected entry highlight from
// the scrollable log.  Deactive upper subscreen area and show the upper
// screen select area.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
EstTestSubScreen::deactivate(void)
{
	CALL_TRACE("EstTestSubScreen::deactivate(void)");

	// Make sure that nothing in this subscreen keeps focus.
	AdjustPanel::TakeNonPersistentFocus(NULL, TRUE);

	// Clear the Primary prompt
	testManager_.setTestPrompt(PromptArea::PA_PRIMARY,
		 PromptArea::PA_HIGH, NULL_STRING_ID);

	testManager_.setTestPrompt(PromptArea::PA_PRIMARY,
		 PromptArea::PA_LOW, NULL_STRING_ID);

	// Clear the Advisory prompts
	testManager_.setTestPrompt(PromptArea::PA_ADVISORY,
		 PromptArea::PA_HIGH, NULL_STRING_ID);

	// Clear the Advisory prompts
	testManager_.setTestPrompt(PromptArea::PA_ADVISORY,
		 PromptArea::PA_LOW, NULL_STRING_ID);

	// Clear the Secondary prompt
	testManager_.setTestPrompt(PromptArea::PA_SECONDARY,
		PromptArea::PA_HIGH, NULL_STRING_ID);

	// Clear the Secondary prompt
	testManager_.setTestPrompt(PromptArea::PA_SECONDARY,
		PromptArea::PA_LOW, NULL_STRING_ID);

	// Pop up the button.
	startTestButton_.setToUp();

    singleTestButton_.setToUp();

	SmManager::DoFunction(SmTestId::DISABLE_SINGLE_TEST_ID);	


	// Deactivate the scrollable log 
	log_.deactivate();

	// Deactive upper subscreen area.
	pEstResultSubScreen_->setResultTitle(NULL_STRING_ID);

	// Show the upper screen select area.
	ServiceUpperScreen::RServiceUpperScreen.getServiceUpperScreenSelectArea()->setBlank(FALSE);

	if (pUpperSubScreenArea_->isCurrentSubScreen(pEstResultSubScreen_))
	{													// $[TI1]
		ServiceUpperScreen::RServiceUpperScreen.getUpperSubScreenArea()->deactivateSubScreen();
	}													// $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getLogEntryColumn
//
//@ Interface-Description
//	Called by ScrollableLog when it needs to retrieve the text content of a
//	particular column of a log entry.  Passed an entry number and a column number.
//	Return a CheapText flag, four strings via parameter list.
//---------------------------------------------------------------------
//@ Implementation-Description
// 	If log entry is empty then a NULL string is passed back.  Otherwise,
//  fill up temporary buffer for the content of each log column.  This
//  temporary buffer is what will be passed back through the parameter
//  list.  The four columns are TIME, TEST NAME, DIAG_CODE and RESULT.
//  The "clearLogEntry_" flag dictates if the log cell shall be displayed
//  or not.  This is specially true when we just start a test but there
//  are previous time, diagnostic code and status information already.
//  So we have to hide the displaying of these fields until the new test
//  information is ready to be displayed.
//  Because the EST test is designed to display either reviewing status
//  for previous run or updating status of current run, so we have to
//  distinguish which status result to use when displaying the result.  When
//  we are in "reviewMode_",  we should use getResultId() to display the
//  previous test status.  When we are in running mode, we should use
//  getResultOfCurrRun() to display the current test status.
// $[07015] During testing, EST shall display the current test name, ....
// $[07028] The EST subscreen shall display a list detailing each ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
 
void
EstTestSubScreen::getLogEntryColumn(Uint16 entryNumber, Uint16 columnNumber,
						Boolean &rIsCheapText, StringId &rString1,
						StringId &rString2, StringId &rString3,
						StringId &rString1Message)
{
	CALL_TRACE("EstTestSubScreen::getLogEntryColumn(Uint16, Uint16, Boolean &, StringId &, StringId &)");

#if defined FORNOW
//printf("getLogEntryColumn() entryNumber=%d, columnNumber=%d, clearLogEntry=%d at line  %d\n",
//				entryNumber, columnNumber, (clearLogEntry_)?1:0, __LINE__);
#endif	// FORNOW

	SAFE_CLASS_ASSERTION(entryNumber < maxServiceModeTest_ && columnNumber < EST_DISPLAY_COL_MAX);

	wchar_t resultBuffer[256] = {0};
	static wchar_t tmpBuffer1[256] = {0};
	static wchar_t tmpBuffer2[256] = {0};
	ResultEntryData resultData;
	Int smTestId;

	rIsCheapText = FALSE;
	rString1 = NULL_STRING_ID;
	rString2 = NULL_STRING_ID;

	if (!clearLogEntry_)
	{							// $[TI1]
		tmpBuffer1[0] = L'\0';
		tmpBuffer2[0] = L'\0';

		// Convert the log entry number to corresponding EST result table index.
		smTestId = SmTestId::GetEstIdFromSmId(estTestId_[entryNumber]);

		// Get the corresponding Result Data for the entry number.
		estResultTable_[smTestId].getResultData(resultData);

#if defined FORNOW							
//printf("   	estResultTable_[%d].isClear= %d\n",
//		smTestId, estResultTable_[smTestId].isClear());
#endif	// FORNOW

		switch (columnNumber)
		{
		case COL_1_TIME:
		{						// $[TI2]
			// Check if there is anything to be displayed
			if (estResultTable_[smTestId].isClear())		 // $[TI3]
			{
					return;
			}					// $[TI4]

			// Format date and time
			TimeStamp timeStamp;
			
	        // Timestamp, let's format it
			if (reviewMode_)
			{ 		// $[TI4.1]
				timeStamp = resultData.getTimeOfResult();
			}
			else
			{	  // $[TI4.2]
				timeStamp = resultData.getTimeOfCurrResult();
			}

			// Formatting Time/Date string
			TextUtil::FormatTime(tmpBuffer1, L"{p=10,y=10:%T}", timeStamp);
			TextUtil::FormatTime(tmpBuffer2, L"{p=8,y=10:%P}", timeStamp);

			rString1 = &tmpBuffer1[0];
			rString2 = &tmpBuffer2[0];
			// rIsCheapText set false so LogCell centers the text
			rIsCheapText = FALSE;
			break;
		}

		case COL_2_TEST:		// $[TI5]	
			wcscpy(resultBuffer, estTestName_[entryNumber]);
			if (TextUtil::WordWrap(resultBuffer, tmpBuffer1, tmpBuffer2,
				TextFont::NORMAL, TextFont::EIGHT,
					EST_COL_2_WIDTH_, EST_ROW_HEIGHT_, LEFT, 0) == 2)
			{					// $[TI6]
				rString2 = &tmpBuffer2[0];
			}					// $[TI7]

			rString1 = &tmpBuffer1[0];
			rIsCheapText = TRUE;
			break;

		case COL_3_DIAG_CODE:	// $[TI8]
			// Check if there is anything to be displayed
			if (estResultTable_[smTestId].isClear())		 // $[TI9]
			{
				return;
			}					// $[TI10]

			TestResultId currentTestResultId;

			if (reviewMode_)
			{ 		// $[TI10.1]
				currentTestResultId = resultData.getResultId();
			}
			else
			{	  // $[TI10.2]
				currentTestResultId = resultData.getResultOfCurrRun();
			}
			
#if defined FORNOW							
//printf("   	currentTestResultId=%d, entryNumber=%d, smTestId = %d\n",
//				currentTestResultId, entryNumber, smTestId);
#endif	// FORNOW

			// If current test doesn't pass, show the diagnostic information
			if (currentTestResultId == MINOR_TEST_FAILURE_ID ||
				currentTestResultId == OVERRIDDEN_TEST_ID ||
				currentTestResultId == MAJOR_TEST_FAILURE_ID)
			{					// $[TI11]
				// Compose diagnostic display string(s)
				DiagnosticFault::ComposeDisplayDiagStrings(
						DiagnosticCode::EXTENDED_SELF_TEST_DIAG, smTestId, 
						tmpBuffer1, tmpBuffer2);

				rString1 = &tmpBuffer1[0];

				if (tmpBuffer2[0] != NULL)
				{				// $[TI12]
					rString2 = &tmpBuffer2[0];
				}				// $[TI13]

#if defined FORNOW							
//printf("   	tmpBuffer2=%s, $$, resultBuffer=%s, $$\n",
//						tmpBuffer1, tmpBuffer2);
//printf("    sString1=$s, $$, rString2=%s, $$\n", rString1, rString2);						
#endif	// FORNOW

			}
			break;

		case COL_4_RESULT:		// $[TI14]
			// Check if there is anything to be displayed
			if (estResultTable_[smTestId].isClear())		 // $[TI15]
			{
				return;
			}					// $[TI16]

			// Get the corresponding Result Data for the entry number.
			estResultTable_[smTestId].getResultData(resultData);

			formatStatusOutcome_(resultData, resultBuffer);

			if (wcslen(resultBuffer) != 0)					// $[TI17]
			{	// Not an empty strin parse the information.
				if (TextUtil::WordWrap(resultBuffer, tmpBuffer1, tmpBuffer2,
						TextFont::NORMAL, TextFont::TEN,
						EST_COL_4_WIDTH_, EST_ROW_HEIGHT_, CENTER, 0) == 2)
				{				// $[TI18]
					rString2 = &tmpBuffer2[0];
				}				// $[TI19]
			}					// $[TI20]

			rString1 = &tmpBuffer1[0];
			rIsCheapText = TRUE;
			break;

		case COL_5_DATA:		// $[TI21]
			break;

		default:
			CLASS_ASSERTION_FAILURE();
			break;
		}

#if defined FORNOW
//printf("rString1=%s\n", rString1);
//printf("rString2=%s\n", rString2);
#endif	// FORNOW

	}							// $[TI22]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: currentLogEntryChangeHappened
//
//@ Interface-Description
// Called when the user selects a new entry or deselects the current entry.
//---------------------------------------------------------------------
//@ Implementation-Description
// If Select button is pressed down, make sure to display the appropriate test
// result on the upper screen area. Also, trigger the Service-Data to send all data
// and download test data to GUI and display them.
// Else, if Select button is pressed up, we clear and deactivate the upper screen are. 
// $[07055] When the EST subscreen is selected but EST is not running, ...
// $[LC07003] If the technician touches a 'test selection' button 
// (in the Data column), when the button is active or present:
// \a\ The row for that test shall be highlighted
// \b\ The button allowing SINGLE TEST EST to be performed shall be activated and the button allowing FULL EST shall be inactivated.
// \c\ A prompt shall be displayed instructing the technician how to proceed
// \d\ The scroll bar for the EST subscreen is inactivated
// \e\ The screen select buttons on the Upper GUI are erased
// \f\ De-selecting the 'test selection' button shall restore the opening EST screen, reversing the actions above.
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
EstTestSubScreen::currentLogEntryChangeHappened(Uint16 changedEntry, Boolean isSelected)
{
	CALL_TRACE("EstTestSubScreen::currentLogEntryChangeHappened(Uint16, Boolean)");

#if defined FORNOW
	printf("Entry number %d was %s\n", changedEntry,
								isSelected ? "selected" : "deselected");
#endif	// FORNOW


	if (isSelected)				// $[TI1]		
	{	// User selects this item for detailed test information.
		if (!(ServiceUpperScreen::RServiceUpperScreen.getUpperSubScreenArea()->
					isCurrentSubScreen(pEstResultSubScreen_)))
		{						// $[TI2]	
			// Currently displayed upper screen is not EST result subscreen.
			// Setup the Upper Screen Area to display the appropriated test result.
			setupTestResultOnUpperScreen_();
		}						// $[TI3]

		// Grab Adjust Panel focus
		AdjustPanel::TakeNonPersistentFocus(this, TRUE);

		//
		// Set NULL to high priority prompts
		//
		testManager_.setTestPrompt(PromptArea::PA_PRIMARY,
							PromptArea::PA_HIGH, NULL_STRING_ID);
		testManager_.setTestPrompt(PromptArea::PA_ADVISORY,
							PromptArea::PA_HIGH, NULL_STRING_ID);
		testManager_.setTestPrompt(PromptArea::PA_SECONDARY,
							PromptArea::PA_HIGH, NULL_STRING_ID);

		// Setup the currently selected EST test index
		estCurrentTestIndex_ = changedEntry;

		// Setup the upper screen to display the test result data.
		setupUpperScreenLayout_(estCurrentTestIndex_);

		// Trigger the Servide-Data to send all data
		ServiceDataMgr::LoadNovRamTestData();

		// Download the test data to GUI and Display them.
		ServiceDataMgr::DownloadTestData(estTestId_[estCurrentTestIndex_]);

		// display the single test button.
		singleTestButton_.setShow(TRUE);
		singleTestButton_.setToUp();
		startTestButton_.setToFlat();

		// Release Adjust Panel focus
		AdjustPanel::TakeNonPersistentFocus(NULL, TRUE);

		log_.setUserScrollable(FALSE);


		// Set Primary prompt to "Touch SINGLE TEST to begin..."
		testManager_.setTestPrompt(PromptArea::PA_PRIMARY,
			PromptArea::PA_LOW, PromptStrs::EST_TOUCH_SINGLE_TEST_TO_BEGIN_P);

		testManager_.setTestPrompt(PromptArea::PA_ADVISORY,
			PromptArea::PA_LOW, PromptStrs::EST_TOUCH_SINGLE_TEST_TO_BEGIN_A);

		testManager_.setTestPrompt(PromptArea::PA_SECONDARY,
			PromptArea::PA_LOW, PromptStrs::EST_TOUCH_SINGLE_TEST_TO_BEGIN_S);

		// Hide the upper screen select area.
		ServiceUpperScreen::RServiceUpperScreen.getServiceUpperScreenSelectArea()->setBlank(TRUE);
		
	}
	else
	{							// $[TI4]	
		// User deselects this item.
		// Clear the upper screen area.
		pEstResultSubScreen_->clearScreen();

		// Hide the upper screen select area.
		ServiceUpperScreen::RServiceUpperScreen.getServiceUpperScreenSelectArea()->setBlank(FALSE);

		// Deactive upper subscreen area.
		ServiceUpperScreen::RServiceUpperScreen.getUpperSubScreenArea()->deactivateSubScreen();

		// Hide the single test button.
		singleTestButton_.setToFlat();
		startTestButton_.setToUp();	
		nextState_ = INITIAL_STATE;
		previousState_ = INITIAL_STATE;
		fallBackState_ = INITIAL_STATE;

		// Release Adjust Panel focus
		AdjustPanel::TakeNonPersistentFocus(NULL, TRUE);

		//
		// Set NULL to high priority prompts
		//
		testManager_.setTestPrompt(PromptArea::PA_PRIMARY,
							PromptArea::PA_HIGH, NULL_STRING_ID);
		testManager_.setTestPrompt(PromptArea::PA_ADVISORY,
							PromptArea::PA_HIGH, NULL_STRING_ID);
		testManager_.setTestPrompt(PromptArea::PA_SECONDARY,
							PromptArea::PA_HIGH, NULL_STRING_ID);

		//
		// Set NULL to low priority prompts
		//
		testManager_.setTestPrompt(PromptArea::PA_PRIMARY,
							PromptArea::PA_LOW, NULL_STRING_ID);
		testManager_.setTestPrompt(PromptArea::PA_ADVISORY,
							PromptArea::PA_LOW, NULL_STRING_ID);
		testManager_.setTestPrompt(PromptArea::PA_SECONDARY,
							PromptArea::PA_LOW, NULL_STRING_ID);

		layoutScreen_(EST_SETUP_SCREEN);
		
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: buttonDownHappened
//
//@ Interface-Description
// Called when either button on this subscreen is pressed down.  Passed a
// pointer to the button as well as 'byOperatorAction', a flag which indicates
// whether the operator pressed the button down or whether the setToDown()
// method was called (flag is ignored).  We display any appropriate prompts.
//---------------------------------------------------------------------
//@ Implementation-Description
// We must display some new prompts advising the operator on the use of the
// Proceed button.  Setup the "newEvent" based on the touch button. Then
// we figure out the "nextState_" and call executeState_to execute the needed
// functionality.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
EstTestSubScreen::buttonDownHappened(Button *pButton,
									Boolean byOperatorAction)
{
	CALL_TRACE("EstTestSubScreen::buttonDownHappened(Button *pButton, "
											"Boolean byOperatorAction)");

#if defined FORNOW
printf("buttonDownHappened at line  %d, pButton=%d, pCurrentButton=%d\n"
		, __LINE__, pButton, pCurrentButton_);
#endif	// FORNOW


	Int newEvent;

	if (!isVisible())
	{													// $[TI1]
		return;
	}													// $[TI2]

	// Check if the event is operator initiated
	if (byOperatorAction)
	{				// $[TI3]
		if (NULL != pCurrentButton_ && pCurrentButton_ != pButton)
		{			// $[TI4]	
			// User pressed two different buttons in consequence.
			// The previous button needs to be popped up.
			// The adjustPanel focus shall set to NULL.
			pCurrentButton_->setToUp();

			// Lose Adjust Panel focus
			AdjustPanel::TakeNonPersistentFocus(NULL, TRUE);

			// Clear the Primary prompts
			testManager_.setTestPrompt(PromptArea::PA_PRIMARY,
						 PromptArea::PA_HIGH, NULL_STRING_ID);
		}			// $[TI5]

		if (pButton == &startTestButton_)
		{			// $[TI6]
			newEvent = START_EVENT;
		}
		else if (pButton ==& repeatButton_)
		{			// $[TI7]
			newEvent = REPEAT_EVENT;
		}
		else if (pButton == &nextButton_)
		{			// $[TI8]
			newEvent = NEXT_EVENT;
		}
		else if (pButton == &exitButton_)
		{			// $[TI9]
			newEvent = EXIT_EVENT;
		}
		else if (pButton == &overrideButton_)
		{			// $[TI10]
			newEvent = OVERRIDE_EVENT;
		}
		else if (pButton == &singleTestButton_)
		{
            newEvent = SINGLE_EVENT;
		}
		else
		{
			CLASS_ASSERTION_FAILURE();
		}	

		pCurrentButton_ = pButton;
		// The call sequence of the next two statements is very important
		// in determining the fallbackState_.
		setupFallbackState_(newEvent);
		nextState_ = getNextState_(newEvent);

		executeState_();
	}			// $[TI11]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: buttonUpHappened
//
//@ Interface-Description
// Called when the Proceed button is pressed up.  Passed a pointer to the
// button as well as 'byOperatorAction', a flag which indicates whether the
// operator pressed the button up or whether the setToUp() method was called.
// We restore prompts appropriately.
//---------------------------------------------------------------------
//@ Implementation-Description
// We setup the "newEvent" based on the touch button. Then
// we figure out the "nextState_" and call executeState_to execute the needed
// functionality.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
EstTestSubScreen::buttonUpHappened(Button *pButton, Boolean byOperatorAction)
{
	CALL_TRACE("EstTestSubScreen::buttonUpHappened(Button *pButton, Boolean byOperatorAction)");

#if defined FORNOW
printf("buttonUpHappened at line  %d, pButton=%d, pCurrentButton=%d\n"
		, __LINE__, pButton, pCurrentButton_);
#endif	// FORNOW


	Int newEvent;

	// Check if the event is operator initiated
	if (byOperatorAction)
	{			// $[TI1]
		if (pButton == &startTestButton_)
		{		// $[TI2]
			newEvent = START_EVENT;
		}
		else if (pButton ==& repeatButton_)
		{		// $[TI3]
			newEvent = REPEAT_EVENT;
		}
		else if (pButton == &nextButton_)
		{		// $[TI4]
			newEvent = NEXT_EVENT;
		}
		else if (pButton == &exitButton_)
		{		// $[TI5]
			newEvent = EXIT_EVENT;
		}
		else if (pButton == &overrideButton_)
		{		// $[TI6]
			newEvent = OVERRIDE_EVENT;
		}
		else if (pButton == &singleTestButton_)
		{	
			newEvent = SINGLE_EVENT;
		}
		else
		{
			CLASS_ASSERTION_FAILURE();
		}

		pCurrentButton_ = NULL;
		nextState_ = getNextState_(newEvent);

		executeState_();
	}			// $[TI7]
}	


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelRestoreFocusHappened
//
//@ Interface-Description
// This is a virtual method inherited from being an AdjustPanelTarget.
// It is normally called when the operator release the off-screen
// keyboard key and the AdjustPanel needs to restore the default
// AdjustPanel target.  It is the duty of this inherited method to restore
// the previous test prompts in order to continue the EST process.  
//---------------------------------------------------------------------
//@ Implementation-Description
// Invoke the test manager's method to restore the prompts displayed before
// the focus is taken.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
EstTestSubScreen::adjustPanelRestoreFocusHappened(void)
{
	CALL_TRACE("EstTestSubScreen::adjustPanelRestoreFocusHappened(void)");

#if defined FORNOW
printf("adjustPanelRestoreFocusHappened at line  %d\n", __LINE__);
#endif	// FORNOW

	testManager_.restoreTestPrompt();
	// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelAcceptPressHappened
//
//@ Interface-Description
// This is a virtual method inherited from being an AdjustPanelTarget.  It is
// normally called when the operator presses down the Accept key to signal the
// accepting of continuing the EST process. 
//---------------------------------------------------------------------
//@ Implementation-Description
// Pop up the button that is currently down.  Clear the Adjust Panel focus.
// Clear high prioritied prompts.  Reset the keyAllowedId_ and userKeyPressedId_.
// Finally, execute the next state in the state machine.
// $[LC07007] The upper left corner of the EST subscreen shall identify 
//  which EST mode is active.
// $[LC07005] When a sub-test performed during SINGLE TEST EST ends...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
EstTestSubScreen::adjustPanelAcceptPressHappened(void)
{
	CALL_TRACE("EstTestSubScreen::adjustPanelAcceptPressHappened(void)");

#if defined FORNOW
printf("adjustPanelAcceptPressHappened at line  %d, pCurrentButton_ = %d, nextState=%d\n",
			__LINE__, pCurrentButton_, nextState_);
#endif	// FORNOW

	SAFE_CLASS_ASSERTION((pCurrentButton_ != NULL) ||
						(pCurrentButton_ == NULL && nextState_ == PROMPT_STATE));

	if (pCurrentButton_ != NULL)
	{		// $[TI1]
		// Pop up the button.
		pCurrentButton_->setToUp();

		if (pCurrentButton_ == &startTestButton_)
		{
			isSingleTestMode_ = FALSE;
			titleArea_.setTitle(MiscStrs::EXTENDED_SELF_TEST_SUBSCREEN_TITLE);

		}
		else if (pCurrentButton_ == &singleTestButton_)
		{
			isSingleTestMode_ = TRUE;
			titleArea_.setTitle(MiscStrs::SINGLE_TEST_EST_SUBSCREEN_TITLE);
		}
		

		pCurrentButton_ = NULL;
	}		// $[TI2]

	// Service mode had request either an ACCEPT or a CLEAR prompt action.
	// Make sure that nothing in this subscreen keeps focus.
	AdjustPanel::TakeNonPersistentFocus(NULL, TRUE);

	// Clear the Primary prompts
	testManager_.setTestPrompt(PromptArea::PA_PRIMARY,
				 PromptArea::PA_HIGH, NULL_STRING_ID);

	// Clear the Advisory prompts
	testManager_.setTestPrompt(PromptArea::PA_ADVISORY,
				 PromptArea::PA_HIGH, NULL_STRING_ID);

	// Clear the Secondary prompt
	testManager_.setTestPrompt(PromptArea::PA_SECONDARY,
				PromptArea::PA_HIGH, NULL_STRING_ID);


	// Reset the keyAllowedId_
	keyAllowedId_ = SmPromptId::NULL_KEYS_ALLOWED_ID;
	userKeyPressedId_ = SmPromptId::KEY_ACCEPT;

	nextState_ = getNextState_(ACCEPT_EVENT);
	executeState_();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelClearPressHappened
//
//@ Interface-Description
//  This is a virtual method inherited from being an AdjustPanelTarget.
//  It is called when the operator presses the Clear key. 
//---------------------------------------------------------------------
//@ Implementation-Description
//  Pop up the button that is currently down.  Clear the Adjust Panel focus.
//  Clear high prioritied prompts.  Reset the keyAllowedId_ and userKeyPressedId_.
//  Finally, execute the next state in the state machine.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
EstTestSubScreen::adjustPanelClearPressHappened(void)
{
	CALL_TRACE("EstTestSubScreen::adjustPanelClearPressHappened(void)");

#if defined FORNOW
printf("adjustPanelClearPressHappened at line  %d\n", __LINE__);
#endif	// FORNOW

	// Clear key is designed to be used only with prompts.  Exit request should 
	// ignore any Clear request
	if (nextState_ == EXIT_REQUEST_STATE || nextState_ == EXIT_BEFORE_PROMPT_STATE)
	{	//	$[TI1]
		return;
	}	//	$[TI2]	


	if (keyAllowedId_ == SmPromptId::ACCEPT_AND_CANCEL_ONLY ||
		keyAllowedId_ == SmPromptId::ACCEPT_PASS_AND_CANCEL_FAIL_ONLY ||
		keyAllowedId_ == SmPromptId::USER_EXIT_ONLY)
	{		// $[TI3]	
		// Service mode had request a CLEAR prompt action.
		SAFE_CLASS_ASSERTION((pCurrentButton_ != NULL) ||
							(pCurrentButton_ == NULL && nextState_ == PROMPT_STATE));

		if (pCurrentButton_ != NULL)
		{	// $[TI4]
			// Pop up the button.
			pCurrentButton_->setToUp();
			pCurrentButton_ = NULL;
		}	// $[TI5]

		// Reset the keyAllowedId_
		keyAllowedId_ = SmPromptId::NULL_KEYS_ALLOWED_ID;

		// Make sure that nothing in this subscreen keeps focus.
		AdjustPanel::TakeNonPersistentFocus(NULL, TRUE);

		// Clear the Primary prompts
		testManager_.setTestPrompt(PromptArea::PA_PRIMARY,
					 PromptArea::PA_HIGH, NULL_STRING_ID);

		// Clear the Advisory prompts
		testManager_.setTestPrompt(PromptArea::PA_ADVISORY,
					 PromptArea::PA_HIGH, NULL_STRING_ID);

		// Clear the Secondary prompt
		testManager_.setTestPrompt(PromptArea::PA_SECONDARY,
					PromptArea::PA_HIGH, NULL_STRING_ID);

		userKeyPressedId_ = SmPromptId::KEY_CLEAR;
		nextState_ = getNextState_(CLEAR_EVENT);
		executeState_();
	}		// $[TI6]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: novRamUpdateEventHappened
//
//@ Interface-Description
// Called by NovRam manager, this function updates the display with
// the latest, real-time data in the log.
//---------------------------------------------------------------------
//@ Implementation-Description
// Request the scrollable log to display the log information based on
// the type of novram.  For override Est alert, we had setup a counter
// to count the times the EST_RESULT_TABLE_UPDATE had happened.  We
// shall update the override status display only when two
// EST_RESULT_TABLE_UPDATE had occured.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
EstTestSubScreen::novRamUpdateEventHappened(NovRamUpdateManager::UpdateTypeId
											updateId)
{
	CALL_TRACE("EstTestSubScreen::novRamUpdateEventHappened"
				"(NovRamUpdateManager::UpdateTypeId	updateId)");

#if defined FORNOW	
printf("novRamUpdateEventHappened at line  %d, updateId =%d, isVisible = %d, estCurrentTestIndex_=%d\n",
				__LINE__, updateId, isVisible(), estCurrentTestIndex_);
#endif	// FORNOW

	if (!isVisible())
	{													// $[TI1]
		return;
	}													// $[TI2]

	// Get Est test result table from NovRam
	NovRamManager::GetEstResultEntries(estResultTable_);

	switch (updateId)
	{
	case NovRamUpdateManager::EST_RESULT_TABLE_UPDATE:	// $[TI3]
		if (overrideRequested_)
		{												// $[TI3.1]
			overrideCounter_++;

			if (overrideCounter_ == 2)
			{											// $[TI3.1.1]
				displayAndReviewEstResults_(maxServiceModeTest_ - EST_DISPLAY_ROW_MAX);

				// Show the lower screen select area.
				ServiceLowerScreen::RServiceLowerScreen.getServiceLowerScreenSelectArea()->setBlank(FALSE);
	
				// Show the upper screen select area.
//				ServiceUpperScreen::RServiceUpperScreen.getServiceUpperScreenSelectArea()->setBlank(FALSE);
			}											// $[TI3.1.2]
		}
		else
		{												// $[TI3.2]
			// Inform the scrollable log of the change in log entries.
			log_.updateEntryColumn(estCurrentTestIndex_, COL_1_TIME);
			log_.updateEntryColumn(estCurrentTestIndex_, COL_4_RESULT);
		}
		break;
	case NovRamUpdateManager::EST_LOG_UPDATE:		// $[TI4]
		// Inform the scrollable log of the change in log entries.
		log_.updateEntryColumn(estCurrentTestIndex_, COL_3_DIAG_CODE);
		break;
	default:
		CLASS_ASSERTION_FAILURE();
		break;
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: timerEventHappened
//
//@ Interface-Description
// Called when a countdown timer tick occurs.  The given `timerId'
// tells us which case occurred.
//---------------------------------------------------------------------
//@ Implementation-Description
// If the SERVICE_MODE_TEST_PAUSE_TIMEOUT countdown timer tick timeout
// then based on the "nextState_" to proceed to different state.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
EstTestSubScreen::timerEventHappened(GuiTimerId::GuiTimerIdType timerId)
{
	CALL_TRACE("EstTestSubScreen::timerEventHappened("
					"GuiTimerId::GuiTimerIdType timerId)");

	if (isVisible())
	{
		if (timerId == GuiTimerId::SERVICE_MODE_TEST_PAUSE_TIMEOUT)
		{
			GuiTimerRegistrar::CancelTimer(GuiTimerId::SERVICE_MODE_TEST_PAUSE_TIMEOUT);
			nextState_ = getNextState_(TIMEOUT_EVENT);
			executeState_();
		}
		else if (timerId == GuiTimerId::SUBSCREEN_SETTING_CHANGE_TIMEOUT)
		{
			// do nothing
		}
		else
		{
			AUX_CLASS_ASSERTION_FAILURE(timerId);
		}
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processTestPrompt
//
//@ Interface-Description
//  Called when we are informed of a change in test prompts.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Save the given command in private class storage.  Determine the next state in
//  the testing process then call the executeState() method to continue with the EST
//	 tests.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	 none
//@ End-Method
//=====================================================================

void
EstTestSubScreen::processTestPrompt(Int command)
{
	CALL_TRACE("EstTestSubScreen::processTestPrompt(Int command)");

#if defined FORNOW
printf("processTestPrompt at line  %d, command = %d\n", __LINE__, command);
printf("   before getNextState_(), nextState is %d\n", nextState_);
#endif	// FORNOW

	// Remember the most recent prompt ID
	promptId_ = command;

	nextState_ = getNextState_(USER_PROMPT_EVENT);
#if defined FORNOW	
printf("   after getNextState(), nextState is %d\n", nextState_);
#endif	// FORNOW

	executeState_();

#if defined FORNOW
printf("EstTestSubScreen::processTestPrompt() nextState is %d\n", nextState_);
#endif	// FORNOW
			// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processTestKeyAllowed
//
//@ Interface-Description
//	Called when we are informed of a change in Service-Mode's "key allowed"
//	command.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Copy the new command value to our own variable keyAllowedId_
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
EstTestSubScreen::processTestKeyAllowed(Int keyAllowed)
{
	CALL_TRACE("EstTestSubScreen::processTestKeyAllowed(Int keyAllowed)");

#if defined FORNOW
printf("processTestKeyAllowed at line  %d, command = %d\n", __LINE__, keyAllowed);
#endif	// FORNOW

	// Remember the expected user key response ID
	keyAllowedId_ = (SmPromptId::KeysAllowedId) keyAllowed;
			// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processTestResultStatus
//
//@ Interface-Description
//	Called when we are informed of a change in test result status.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Based on the result of the EST test, determine the next state and
//	event in the testing process.  If a EST test ends or if the user
//  presses exit, the method executeState() is then  called to execute
//  the EST tests. 
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
EstTestSubScreen::processTestResultStatus(Int resultStatus, Int testResultId)
{
	CALL_TRACE("EstTestSubScreen::processTestResultStatus(Int resultStatus, Int testResultId)");

	SAFE_CLASS_ASSERTION((resultStatus >= (Int) SmStatusId::TEST_END) &&
						 (resultStatus < (Int) SmStatusId::NUM_TEST_DATA_STATUS));
	Int newEvent=-1;

#if defined FORNOW
printf("processTestResultStatus_ at line  %d, resultStatus= %d, testResultId=%d, mostSevereStatusId_ = %d\n",
			__LINE__, resultStatus, testResultId, mostSevereStatusId_);
#endif	// FORNOW

	if (!isVisible())
	{													// $[TI1]
		return;
	}													// $[TI2]

	switch (resultStatus)
	{
	case SmStatusId::TEST_END:				// End of test status received
			// $[TI3]
		
		switch (mostSevereStatusId_)		// Check against the previous status
		{
		case PASSED_TEST_ID:				// Test end with an failure error
			// $[TI4]
			newEvent = USER_END_EVENT;
			break;
		case MINOR_TEST_FAILURE_ID:			// Test end with an alert error
			// $[TI5]
			newEvent = END_ALERT_EVENT;
			break;
		case MAJOR_TEST_FAILURE_ID:			// Test end with an failure error
			// $[TI6]
			newEvent = END_FAILURE_EVENT;
			break;
		default:
			CLASS_ASSERTION_FAILURE();
			break;
		}

		// Set isThisSstTestCompleted_ flag.
		isThisEstTestCompleted_ = TRUE;

		// Get Est test result table from NovRam
		NovRamManager::GetEstResultEntries(estResultTable_);

		// Inform the scrollable log of the change in log entries.
		log_.updateEntryColumn(estCurrentTestIndex_, COL_1_TIME);
		log_.updateEntryColumn(estCurrentTestIndex_, COL_3_DIAG_CODE);
		log_.updateEntryColumn(estCurrentTestIndex_, COL_4_RESULT);

		nextState_ = getNextState_(newEvent);
		executeState_();
		break;

	case SmStatusId::TEST_START:
			// $[TI7]
		// Do nothing.
		break;

	case SmStatusId::TEST_ALERT:
			// $[TI8]
	case SmStatusId::TEST_FAILURE:
			// $[TI9]
		if ((mostSevereStatusId_ == PASSED_TEST_ID) ||
			(mostSevereStatusId_ == MINOR_TEST_FAILURE_ID &&
			 resultStatus == SmStatusId::TEST_FAILURE))
		{	// Update mostSevereStatusId_ only when there are no previous error or
			// $[TI10]
			// there are previous minor error and we just get a major error.
			mostSevereStatusId_ = estStatusDecodingTable_[resultStatus];
		}
			// $[TI11]
		break;

	case SmStatusId::TEST_OPERATOR_EXIT:
			// $[TI12]
		CLASS_ASSERTION(nextState_ == EXECUTE_EXIT_EST_STATE);
		newEvent = USER_END_EVENT;
		nextState_ = getNextState_(newEvent);
		executeState_();
		break;

	case SmStatusId::NOT_INSTALLED:
			// $[TI13]
		mostSevereStatusId_ = PASSED_TEST_ID;
		break;

	default:
		CLASS_ASSERTION_FAILURE();
		break;
	}

#if defined FORNOW	
printf("$$$$$$$$testResultId=%d, conditionId_=%d\n",
				testResultId, conditionId_);
#endif	// FORNOW

}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processTestResultCondition
//
//@ Interface-Description
//	Called when we are informed of a change in Service-Mode's test
//	result condition.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Copy the new resultCondition to our copy of the same.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
EstTestSubScreen::processTestResultCondition(Int resultCondition)
{
	CALL_TRACE("EstTestSubScreen::processTestResultCondition(Int resultCondition)");

#if defined FORNOW
printf("EstTestSubScreen::processTestResultCondition_() condition=%d\n",
				resultCondition);
#endif 	// FORNOW

	// Remember the most recent condition ID
	conditionId_ = resultCondition;
			// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: layoutScreen_
//
//@ Interface-Description
// Sets the subscreen into one of the display configurations.
//---------------------------------------------------------------------
//@ Implementation-Description
//  If screen id is EST_SETUP_SCREEN, we call displayAndReviewEstResults_()
//  to display the previous test results.
//  If screen id is EST_START_SCREEN, we enforce the log_ starts at the
//  first log entry, Display test name, inform the service mode  manager
//  of the test type, and show exit button.
//  If screen id is EST_EXIT_SCREEN, we clear the advisory and secondary
//  prompts and deactivate this subscreen.  And show lower subscreen area,
//  and show upper subscreen select area.
// $[07007] When an EST test, an Exhalation valve calibration or ...
// $[07008] When the test run finished or is stopped by the technician ...
// $[07027] When the operator initiates EST, the EST results subscreen ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
EstTestSubScreen::layoutScreen_(estScreenId newScreenId)
{
	CALL_TRACE("EstTestSubScreen::layoutScreen_(estScreenId newScreenId)");

#if defined FORNOW
printf("layoutScreen_ at line  %d\n", __LINE__);
#endif	// FORNOW

	switch (newScreenId)
	{
	case EST_SETUP_SCREEN:		// $[TI1]
		displayAndReviewEstResults_(EST_TEST_START_ID);
		break;

	case EST_READY_TO_START_SCREEN:		// $[TI6]
		// Disable user scrollable function in the log.
		log_.setUserScrollable(FALSE);

		// Disable user selectable function in the log.  The DATA select
		// buttons shall disappear.
		log_.setUserSelectable(FALSE);

		if (!isSingleTestMode_)
		{
			// Clear the selected entry highlight whichever row it may be.
			log_.clearSelectedEntry();

			// Display the first log entry
			log_.setEntryInfo(EST_TEST_START_ID, maxServiceModeTest_);

			
			// Flatten the single button
			singleTestButton_.setToFlat();
		}


		break;

	case EST_START_SCREEN:		// $[TI2]
		if (!(ServiceUpperScreen::RServiceUpperScreen.getUpperSubScreenArea()->
					isCurrentSubScreen(pEstResultSubScreen_)))
		{	// Currently displayed upper screen is not EST result subscreen.
			// Setup the Upper Screen Area to display the appropriated test result.
								// $[TI3]
			setupTestResultOnUpperScreen_();
		}						// $[TI4]

		exitButton_.setShow(TRUE);

		if (!isSingleTestMode_)
		{
			// Show the following drawables

			// To enforce the log_ starts at the first log entry.
			// High light the test item
			estCurrentTestIndex_ = EST_TEST_START_ID;

			// Initialize the starting item test.
			log_.setSelectedEntry(estCurrentTestIndex_);
		}

		singleTestButton_.setShow(FALSE);
		

		// Hide the lower screen select area.
		ServiceLowerScreen::RServiceLowerScreen.getServiceLowerScreenSelectArea()->setBlank(TRUE);

		// Hide the following objects.
		startTestButton_.setShow(FALSE);


		// We are in testing mode.
		reviewMode_ = FALSE;
		
		// Signal to clear the log cell infos.
		clearLogEntry_ = TRUE;

		// Hide the data/time, diagnostic code and status columes.
		log_.updateColumn(COL_1_TIME);
		log_.updateColumn(COL_3_DIAG_CODE);
		log_.updateColumn(COL_4_RESULT);

		// Signal to generate the log cell infos.
		clearLogEntry_ = FALSE;

		// Signal not to override the alert.
		overrideRequested_ = FALSE;
		overrideCounter_ = -1;

		// Hide upper subscreen select area.
		ServiceUpperScreen::RServiceUpperScreen.getServiceUpperScreenSelectArea()->setBlank(TRUE);

		// Let the service mode  manager knows the type of Service mode.
		SmManager::DoFunction(SmTestId::SET_TEST_TYPE_EST_ID);

		break;

	case EST_EXIT_SCREEN:
			// $[TI5]
		// Clear the high priority prompts
		testManager_.setTestPrompt(PromptArea::PA_PRIMARY,
							PromptArea::PA_HIGH, NULL_STRING_ID);
		testManager_.setTestPrompt(PromptArea::PA_ADVISORY,
							PromptArea::PA_HIGH, NULL_STRING_ID);
		testManager_.setTestPrompt(PromptArea::PA_SECONDARY,
							PromptArea::PA_HIGH, NULL_STRING_ID);

		// Clear the low priority prompts
		testManager_.setTestPrompt(PromptArea::PA_PRIMARY,
							PromptArea::PA_LOW, NULL_STRING_ID);
		testManager_.setTestPrompt(PromptArea::PA_ADVISORY, PromptArea::PA_LOW,
							NULL_STRING_ID);
		testManager_.setTestPrompt(PromptArea::PA_SECONDARY,
							PromptArea::PA_LOW, NULL_STRING_ID);

		// Show lower subscreen area.
		ServiceLowerScreen::RServiceLowerScreen.getServiceLowerScreenSelectArea()->setBlank(FALSE);

		// Show upper subscreen select area.
		ServiceUpperScreen::RServiceUpperScreen.getServiceUpperScreenSelectArea()->setBlank(FALSE);

		// Make the Accept sound, clear the Adjust Panel focus, store the
		// current settings, accept the adjusted settings and deactivate
		// this subscreen.
		Sound::Start(Sound::ACCEPT);
		getSubScreenArea()->deactivateSubScreen();

		break;
	default:
		AUX_CLASS_ASSERTION_FAILURE(newScreenId);
		break;
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setEstTestNameTable_
//
//@ Interface-Description
//  Initialize the estTestName_, estTestId_, and estTestData_ tables.
//  The estTestId_ array contains Service-Mode test ids.
//  The estTestName_ table contains translation text of the test Name.
//  The estTestData_ table contains formated tested data result.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Simply set the proper text and ids to the corresponding tables.
// $[07069] During EST the following tests shall be in the order ...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
EstTestSubScreen::setEstTestNameTable_(void)
{
	CALL_TRACE("EstTestSubScreen::setEstTestNameTable_(void)");

	// First clear each entry in the array.
	for (Int32 i = 0; i < EST_TEST_MAX; i++)
	{
		estTestId_[i]	= SmTestId::TEST_NOT_START_ID;
		estTestResultTitle_[i] = NULL_STRING_ID;
		estTestName_[i] = NULL_STRING_ID;
	}

	Int testIndex = 0;

	estTestId_[testIndex]			= SmTestId::CIRCUIT_PRESSURE_TEST_ID;
	estTestResultTitle_[testIndex] 	= MiscStrs::EST_TEST_CIRCUIT_PRESSURE_RESULT_ID;
	estTestName_[testIndex++]		= MiscStrs::SM_CIRCUIT_PRESSURE_TEST_ID;

	estTestId_[testIndex]			= SmTestId::FS_CROSS_CHECK_TEST_ID;
	estTestResultTitle_[testIndex] 	= MiscStrs::EST_TEST_FS_CROSS_CHECK_RESULT_ID;
	estTestName_[testIndex++]		= MiscStrs::SM_FS_CROSS_CHECK_TEST_ID;

	estTestId_[testIndex]			= SmTestId::GAS_SUPPLY_TEST_ID;
	estTestResultTitle_[testIndex] 	= MiscStrs::EST_TEST_GAS_SUPPLY_RESULT_ID;
	estTestName_[testIndex++]		= MiscStrs::SM_GAS_SUPPLY_TEST_ID;

	estTestId_[testIndex]			= SmTestId::SM_LEAK_TEST_ID;
	estTestResultTitle_[testIndex] 	= MiscStrs::EST_TEST_SM_LEAK_RESULT_ID;
	estTestName_[testIndex++]		= MiscStrs::SM_SM_LEAK_TEST_ID;

	estTestId_[testIndex]			= SmTestId::GUI_KEYBOARD_TEST_ID;
	estTestResultTitle_[testIndex] 	= MiscStrs::EST_TEST_GUI_KEYBOARD_RESULT_ID;
	estTestName_[testIndex++]		= MiscStrs::SM_GUI_KEYBOARD_TEST_ID;
	estTestId_[testIndex]			= SmTestId::GUI_KNOB_TEST_ID;
	estTestResultTitle_[testIndex] 	= MiscStrs::EST_TEST_GUI_KNOB_RESULT_ID;
	estTestName_[testIndex++]		= MiscStrs::SM_GUI_KNOB_TEST_ID;

	estTestId_[testIndex]			= SmTestId::GUI_LAMP_TEST_ID;
	estTestResultTitle_[testIndex] 	= MiscStrs::EST_TEST_GUI_LAMP_RESULT_ID;
	estTestName_[testIndex++]		= MiscStrs::SM_GUI_LAMP_TEST_ID;

	estTestId_[testIndex]			= SmTestId::BD_LAMP_TEST_ID;
	estTestResultTitle_[testIndex] 	= MiscStrs::EST_TEST_BD_LAMP_RESULT_ID;
	estTestName_[testIndex++]		= MiscStrs::SM_BD_LAMP_TEST_ID;

	estTestId_[testIndex]			= SmTestId::GUI_AUDIO_TEST_ID;
	estTestResultTitle_[testIndex] 	= MiscStrs::EST_TEST_GUI_AUDIO_RESULT_ID;
	estTestName_[testIndex++]		= MiscStrs::SM_GUI_AUDIO_TEST_ID;

	estTestId_[testIndex]			= SmTestId::GUI_NURSE_CALL_TEST_ID;
	estTestResultTitle_[testIndex] 	= MiscStrs::EST_TEST_GUI_NURSE_CALL_RESULT_ID;
	estTestName_[testIndex++]		= MiscStrs::SM_GUI_NURSE_CALL_TEST_ID;

	estTestId_[testIndex]			= SmTestId::BD_AUDIO_TEST_ID;
	estTestResultTitle_[testIndex] 	= MiscStrs::EST_TEST_BD_AUDIO_RESULT_ID;
	estTestName_[testIndex++]		= MiscStrs::SM_BD_AUDIO_TEST_ID;

	estTestId_[testIndex]			= SmTestId::SAFETY_SYSTEM_TEST_ID;
	estTestResultTitle_[testIndex] 	= MiscStrs::EST_TEST_SAFETY_SYSTEM_RESULT_ID;
	estTestName_[testIndex++]		= MiscStrs::SM_SAFETY_SYSTEM_TEST_ID;

	estTestId_[testIndex]			= SmTestId::EXH_VALVE_SEAL_TEST_ID;
	estTestResultTitle_[testIndex] 	= MiscStrs::EST_TEST_EXH_VALVE_SEAL_RESULT_ID;
	estTestName_[testIndex++]		= MiscStrs::SM_EXH_VALVE_SEAL_TEST_ID;

	estTestId_[testIndex]			= SmTestId::EXH_VALVE_TEST_ID;
	estTestResultTitle_[testIndex] 	= MiscStrs::EST_TEST_EXH_VALVE_RESULT_ID;
	estTestName_[testIndex++]		= MiscStrs::SM_EXH_VALVE_TEST_ID;

	estTestId_[testIndex]			= SmTestId::EXH_HEATER_TEST_ID;
	estTestResultTitle_[testIndex] 	= MiscStrs::EST_TEST_EXH_HEATER_RESULT_ID;
	estTestName_[testIndex++]		= MiscStrs::SM_EXH_HEATER_TEST_ID;

	estTestId_[testIndex]			= SmTestId::GENERAL_ELECTRONICS_TEST_ID;
	estTestResultTitle_[testIndex] 	= MiscStrs::EST_TEST_GENERAL_ELECTRONICS_RESULT_ID;
	estTestName_[testIndex++]		= MiscStrs::SM_GENERAL_ELECTRONICS_TEST_ID;

	estTestId_[testIndex]			= SmTestId::GUI_TOUCH_TEST_ID;
	estTestResultTitle_[testIndex] 	= MiscStrs::EST_TEST_GUI_TOUCH_RESULT_ID;
	estTestName_[testIndex++]		= MiscStrs::SM_GUI_TOUCH_TEST_ID;

	estTestId_[testIndex]			= SmTestId::GUI_SERIAL_PORT_TEST_ID;
	estTestResultTitle_[testIndex] 	= MiscStrs::EST_TEST_GUI_SERIAL_PORT_RESULT_ID;
	estTestName_[testIndex++]		= MiscStrs::SM_GUI_SERIAL_PORT_TEST_ID;

	estTestId_[testIndex]			= SmTestId::BATTERY_TEST_ID;
	estTestResultTitle_[testIndex] 	= MiscStrs::EST_TEST_BATTERY_RESULT_ID;
	estTestName_[testIndex++]		= MiscStrs::SM_BATTERY_TEST_ID;

	maxServiceModeTest_ = testIndex;
			// $[TI1]

#ifdef SIGMA_DEVELOPMENT

	// Make sure total test count is within the maximum allowed.
	SAFE_CLASS_ASSERTION(maxServiceModeTest_ <= EST_TEST_MAX);

	// Do some safe assertions that all entries in the array have been filled
	for (i = 0; i < maxServiceModeTest_; i++)
	{
		SAFE_CLASS_ASSERTION(estTestId_[i]	!= SmTestId::TEST_NOT_START_ID);
		SAFE_CLASS_ASSERTION(estTestResultTitle_[i] != NULL_STRING_ID);
		SAFE_CLASS_ASSERTION(estTestName_[i] != NULL_STRING_ID);
	}
#endif // SIGMA_DEVELOPMENT
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setEstTestPromptTable_
//
//@ Interface-Description
//  Initialize the estTestPromptId_, and estTestPromptName_ tables.  
//  The estTestPromptId_ array contains Service-Mode prompt ids.
//  The estTestPromptName_ table contains translation text of Service-Mode
//	prompt code.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Simply set the proper text and ids to the corresponding tables.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
EstTestSubScreen::setEstTestPromptTable_(void)
{
	CALL_TRACE("EstTestSubScreen::setEstTestPromptTable_(void)");

	// First clear each entry in the array.
	for (Int32 i = 0; i < NUM_EST_TEST_PROMPT_MAX; i++)
	{
		estTestPromptId_[i]		= SmPromptId::NULL_PROMPT_ID;
		estTestPromptName_[i] 	= NULL_STRING_ID;
	}

	Int testIndex = 0;

	estTestPromptId_[testIndex]		= SmPromptId::REMOVE_INSP_FILTER_PROMPT;
	estTestPromptName_[testIndex++] = PromptStrs::SM_REMOVE_INSP_FILTER_P;

	estTestPromptId_[testIndex]		= SmPromptId::UNBLOCK_INLET_PORT_PROMPT;
	estTestPromptName_[testIndex++] = PromptStrs::SM_UNBLOCK_INLET_PORT_P;

	estTestPromptId_[testIndex]		= SmPromptId::BLOCK_INLET_PORT_PROMPT;
	estTestPromptName_[testIndex++] = PromptStrs::SM_BLOCK_INLET_PORT_P;

	estTestPromptId_[testIndex]		= SmPromptId::DISCONNECT_AIR_PROMPT;
	estTestPromptName_[testIndex++] = PromptStrs::SM_DISCONNECT_AIR_P;

	estTestPromptId_[testIndex]		= SmPromptId::DISCONNECT_O2_PROMPT;
	estTestPromptName_[testIndex++] = PromptStrs::SM_DISCONNECT_O2_P;

	estTestPromptId_[testIndex]		= SmPromptId::CONNECT_AIR_PROMPT;
	estTestPromptName_[testIndex++] = PromptStrs::SM_CONNECT_AIR_P;

	estTestPromptId_[testIndex]		= SmPromptId::CONNECT_AIR_IF_AVAIL_PROMPT;
	estTestPromptName_[testIndex++] = PromptStrs::SM_CONNECT_AIR_IF_AVAIL_P;

	estTestPromptId_[testIndex]		= SmPromptId::CONNECT_O2_PROMPT;
	estTestPromptName_[testIndex++] = PromptStrs::SM_CONNECT_O2_P;

	estTestPromptId_[testIndex]		= SmPromptId::DISCNT_TUBE_BFR_EXH_FLTR_PROMPT;
	estTestPromptName_[testIndex++] = PromptStrs::SM_DISCNT_TUBE_BFR_EXH_FLTR_P;

	estTestPromptId_[testIndex]		= SmPromptId::CONCT_TUBE_BFR_EXH_FLTR_PROMPT;
	estTestPromptName_[testIndex++] = PromptStrs::SM_CONCT_TUBE_BFR_EXH_FLTR_P;

	estTestPromptId_[testIndex]		= SmPromptId::ATTACH_GOLD_STD_TEST_TUBING_PROMPT;
	estTestPromptName_[testIndex++] = PromptStrs::SM_ATTACH_GOLD_STD_TEST_TUBING_P;

	estTestPromptId_[testIndex]		= SmPromptId::CONNECT_WALL_AIR_PROMPT;
	estTestPromptName_[testIndex++] = PromptStrs::SM_CONNECT_WALL_AIR_P;

	estTestPromptId_[testIndex]		= SmPromptId::KEY_ACCEPT_PROMPT;
	estTestPromptName_[testIndex++] = PromptStrs::SM_KEY_ACCEPT_P;

	estTestPromptId_[testIndex]		= SmPromptId::KEY_CLEAR_PROMPT;
	estTestPromptName_[testIndex++] = PromptStrs::SM_KEY_CLEAR_P;

	estTestPromptId_[testIndex]		= SmPromptId::KEY_INSP_PAUSE_PROMPT;
	estTestPromptName_[testIndex++] = PromptStrs::SM_KEY_INSP_PAUSE_P;

	estTestPromptId_[testIndex]		= SmPromptId::KEY_EXP_PAUSE_PROMPT;
	estTestPromptName_[testIndex++] = PromptStrs::SM_KEY_EXP_PAUSE_P;

	estTestPromptId_[testIndex]		= SmPromptId::KEY_MAN_INSP_PROMPT;
	estTestPromptName_[testIndex++] = PromptStrs::SM_KEY_MAN_INSP_P;

	estTestPromptId_[testIndex]		= SmPromptId::KEY_HUNDRED_PERCENT_O2_PROMPT;
	estTestPromptName_[testIndex++] = PromptStrs::SM_KEY_HUNDRED_PERCENT_O2_P;

	estTestPromptId_[testIndex]		= SmPromptId::KEY_INFO_PROMPT;
	estTestPromptName_[testIndex++] = PromptStrs::SM_KEY_INFO_P;

	estTestPromptId_[testIndex]		= SmPromptId::KEY_ALARM_RESET_PROMPT;
	estTestPromptName_[testIndex++] = PromptStrs::SM_KEY_ALARM_RESET_P;

	estTestPromptId_[testIndex]		= SmPromptId::KEY_ALARM_SILENCE_PROMPT;
	estTestPromptName_[testIndex++] = PromptStrs::SM_KEY_ALARM_SILENCE_P;

	estTestPromptId_[testIndex]		= SmPromptId::KEY_ALARM_VOLUME_PROMPT;
	estTestPromptName_[testIndex++] = PromptStrs::SM_KEY_ALARM_VOLUME_P;

	estTestPromptId_[testIndex]		= SmPromptId::KEY_SCREEN_BRIGHTNESS_PROMPT;
	estTestPromptName_[testIndex++] = PromptStrs::SM_KEY_SCREEN_BRIGHTNESS_P;

	estTestPromptId_[testIndex]		= SmPromptId::KEY_SCREEN_CONTRAST_PROMPT;
	estTestPromptName_[testIndex++] = PromptStrs::SM_KEY_SCREEN_CONTRAST_P;

	estTestPromptId_[testIndex]		= SmPromptId::KEY_SCREEN_LOCK_PROMPT;
	estTestPromptName_[testIndex++] = PromptStrs::SM_KEY_SCREEN_LOCK_P;

	estTestPromptId_[testIndex]		= SmPromptId::KEY_LEFT_SPARE_PROMPT;
	estTestPromptName_[testIndex++] = PromptStrs::SM_KEY_LEFT_SPARE_P;

	estTestPromptId_[testIndex]		= SmPromptId::TURN_KNOB_LEFT_PROMPT;
	estTestPromptName_[testIndex++] = PromptStrs::SM_TURN_KNOB_LEFT_P;

	estTestPromptId_[testIndex]		= SmPromptId::TURN_KNOB_RIGHT_PROMPT;
	estTestPromptName_[testIndex++] = PromptStrs::SM_TURN_KNOB_RIGHT_P;

	estTestPromptId_[testIndex]		= SmPromptId::NORMAL_PROMPT;
	estTestPromptName_[testIndex++] = PromptStrs::SM_NORMAL_P;

	estTestPromptId_[testIndex]		= SmPromptId::VENTILATOR_INOPERATIVE_PROMPT;
	estTestPromptName_[testIndex++] = PromptStrs::SM_VENTILATOR_INOPERATIVE_P;

	estTestPromptId_[testIndex]		= SmPromptId::SAFETY_VALVE_OPEN_PROMPT;
	estTestPromptName_[testIndex++] = PromptStrs::SM_SAFETY_VALVE_OPEN_P;

	estTestPromptId_[testIndex]		= SmPromptId::LOSS_OF_GUI_PROMPT;
	estTestPromptName_[testIndex++] = PromptStrs::SM_LOSS_OF_GUI_P;

	estTestPromptId_[testIndex]		= SmPromptId::BATTERY_BACKUP_READY_PROMPT;
	estTestPromptName_[testIndex++] = PromptStrs::SM_BATTERY_BACKUP_READY_P;

	estTestPromptId_[testIndex]		= SmPromptId::COMPRESSOR_READY_PROMPT;
	estTestPromptName_[testIndex++] = PromptStrs::SM_COMPRESSOR_READY_P;

	estTestPromptId_[testIndex]		= SmPromptId::COMPRESSOR_OPERATING_PROMPT;
	estTestPromptName_[testIndex++] = PromptStrs::SM_COMPRESSOR_OPERATING_P;

	estTestPromptId_[testIndex]		= SmPromptId::COMPRESSOR_MOTOR_ON_PROMPT;
	estTestPromptName_[testIndex++] = PromptStrs::SM_COMPRESSOR_MOTOR_ON_P;

	estTestPromptId_[testIndex]		= SmPromptId::COMPRESSOR_MOTOR_OFF_PROMPT;
	estTestPromptName_[testIndex++] = PromptStrs::SM_COMPRESSOR_MOTOR_OFF_P;

	estTestPromptId_[testIndex]		= SmPromptId::HUNDRED_PERCENT_O2_PROMPT;
	estTestPromptName_[testIndex++] = PromptStrs::SM_HUNDRED_PERCENT_O2_P;

	estTestPromptId_[testIndex]		= SmPromptId::ALARM_SILENCE_PROMPT;
	estTestPromptName_[testIndex++] = PromptStrs::SM_ALARM_SILENCE_P;

	estTestPromptId_[testIndex]		= SmPromptId::SCREEN_LOCK_PROMPT;
	estTestPromptName_[testIndex++] = PromptStrs::SM_SCREEN_LOCK_P;

	estTestPromptId_[testIndex]		= SmPromptId::HIGH_ALARM_PROMPT;
	estTestPromptName_[testIndex++] = PromptStrs::SM_HIGH_ALARM_P;

	estTestPromptId_[testIndex]		= SmPromptId::MEDIUM_ALARM_PROMPT;
	estTestPromptName_[testIndex++] = PromptStrs::SM_MEDIUM_ALARM_P;

	estTestPromptId_[testIndex]		= SmPromptId::LOW_ALARM_PROMPT;
	estTestPromptName_[testIndex++] = PromptStrs::SM_LOW_ALARM_P;

	estTestPromptId_[testIndex]		= SmPromptId::ON_BATTERY_POWER_PROMPT;
	estTestPromptName_[testIndex++] = PromptStrs::SM_ON_BATT_PWR_P;

	estTestPromptId_[testIndex]		= SmPromptId::SOUND_PROMPT;
	estTestPromptName_[testIndex++] = PromptStrs::SM_SOUND_P;

	estTestPromptId_[testIndex]		= SmPromptId::NURSE_CALL_TEST_PROMPT;
	estTestPromptName_[testIndex++] = PromptStrs::SM_NURSE_CALL_TEST_P;

	estTestPromptId_[testIndex]		= SmPromptId::NURSE_CALL_ON_PROMPT;
	estTestPromptName_[testIndex++] = PromptStrs::SM_NURSE_CALL_ON_P;

	estTestPromptId_[testIndex]		= SmPromptId::NURSE_CALL_OFF_PROMPT;
	estTestPromptName_[testIndex++] = PromptStrs::SM_NURSE_CALL_OFF_P;

	estTestPromptId_[testIndex]		= SmPromptId::CONNECT_AC_PROMPT;
	estTestPromptName_[testIndex++] = PromptStrs::SM_CONNECT_AC_P;

	estTestPromptId_[testIndex]		= SmPromptId::CONNECT_AIR_AND_O2_PROMPT;
	estTestPromptName_[testIndex++] = PromptStrs::SM_CONNECT_AIR_AND_O2_PROMPT_P;

	estTestPromptId_[testIndex]		= SmPromptId::DISCONNECT_AIR_AND_O2_PROMPT;
	estTestPromptName_[testIndex++] = PromptStrs::SM_DISCONNECT_AIR_AND_O2_P;

	estTestPromptId_[testIndex]		= SmPromptId::UNPLUG_AC_PROMPT;
	estTestPromptName_[testIndex++] = PromptStrs::SM_UNPLUG_AC_P;

#ifdef SIGMA_DEVELOPMENT

#if defined FORNOW
printf("setEstTestPromptTable_ at line  %d, testIndex= %d, NUM_EST_TEST_PROMPT_MAX = %d\n",
				 __LINE__, testIndex, NUM_EST_TEST_PROMPT_MAX);
#endif	// FORNOW

	// Make sure total test count is within the maximum allowed.
	SAFE_CLASS_ASSERTION(testIndex == NUM_EST_TEST_PROMPT_MAX);

	// Do some safe assertions that all entries in the array have been filled
	for (i = 0; i < NUM_EST_TEST_PROMPT_MAX; i++)
	{
		SAFE_CLASS_ASSERTION(estTestPromptId_[i] != SmPromptId::NULL_PROMPT_ID);
		SAFE_CLASS_ASSERTION(estTestPromptName_[i] != NULL_STRING_ID);
	}
#endif // SIGMA_DEVELOPMENT
			// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setEstTestStatusTable_
//
//@ Interface-Description
// 	Get the latest Est result table.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Simply update the latest Est result table from NovRam.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
EstTestSubScreen::setEstTestStatusTable_(void)
{
	CALL_TRACE("EstTestSubScreen::setEstTestStatusTable_(void)");

#if defined FORNOW
printf("setEstTestStatusTable_ at line  %d\n", __LINE__);
#endif	// FORNOW

	// Get Est test result table from NovRam
    NovRamManager::GetEstResultEntries(estResultTable_);

			// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: formatStatusOutcome_
//
//@ Interface-Description
//  Called when we need to update the status result.  We passed the
//  "resultData" from which we can retrieve the overall test result or
//  the result of current run.  The "tmpBuffer" is where we store the
//  formatted status result.  The "isOutcome" flag tells us whether we
//  are formatting a overall or a current status.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Based on the result of the SST test, and the "isOutcome" flag we 
//	format the test result string accordingly.
//  Because the EST test is designed to display either reviewing status
//  for previous run or updating status of current run, so we have to
//  distinguish which status result to use when displaying the result.  When
//  we are in "reviewMode_",  we should use getResultId() to display the
//  previous test status.  When we are in running mode, we should use
//  getResultOfCurrRun() to display the current test status.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
 
void
EstTestSubScreen::formatStatusOutcome_(ResultEntryData resultData, wchar_t *tmpBuffer)
{
	CALL_TRACE("formatStatusOutcome_(ResultEntryData resultData, char *tmpBuffer)");

#if defined FORNOW
printf("formatStatusOutcome_ at line  %d\n", __LINE__);
#endif	// FORNOW

	TestResultId currentTestResultId;

	if (reviewMode_)
	{								// $[TI0.1]
		currentTestResultId = resultData.getResultId();
	}
	else
	{		 						// $[TI0.2]
		currentTestResultId = resultData.getResultOfCurrRun();
	}
			
	// Test Result
	switch (currentTestResultId)
	{
	case PASSED_TEST_ID:		// $[TI1]
		wcscpy(tmpBuffer, MiscStrs::PASSED_STATUS_MSG);
		break;
		
	case OVERRIDDEN_TEST_ID:	// $[TI2]
	case MINOR_TEST_FAILURE_ID:	// $[TI3]
		wcscpy(tmpBuffer, MiscStrs::ALERT_STATUS_MSG);
		break;
		
	case MAJOR_TEST_FAILURE_ID:	// $[TI4]
		wcscpy(tmpBuffer, MiscStrs::FAILED_STATUS_MSG);
		break;
		
	case NOT_INSTALLED_TEST_ID:	// $[TI5]
		wcscpy(tmpBuffer, MiscStrs::NOT_INSTALLED_STATUS_MSG);
		break;
		
	case INCOMPLETE_TEST_RESULT_ID:		// $[TI6]
	case UNDEFINED_TEST_RESULT_ID:		// $[TI7]
		tmpBuffer[0] = NULL;
		break;
		
	case NOT_APPLICABLE_TEST_RESULT_ID:	// $[TI8]
	default:
		CLASS_ASSERTION_FAILURE();
		break;
	}

#if defined FORNOW
printf("   currentTestResultId = %d, tmpBuffer = %s\n",
				currentTestResultId, tmpBuffer);
#endif	// FORNOW

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: startTest_
//
//@ Interface-Description
//	Preparation step for a test to start.	
//---------------------------------------------------------------------
//@ Implementation-Description
//	Set various flags; Hide the date/time/ diagnostic code and status
//	log cells; Update the prompt area; Erase the test data result for
//	repeated test; Setup the upper screen layout to display the Leak
//	graphic; Set data format then do the test.
// $[07038] When the operator commands to rerun a test, the new outcome ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
EstTestSubScreen::startTest_(void)
{
	CALL_TRACE("EstTestSubScreen::startTest_(void)");

#if defined FORNOW
printf("startTest_ at line  %d\n", __LINE__);
#endif	// FORNOW

	// High light the test item
	log_.setSelectedEntry(estCurrentTestIndex_);

	// Hide the following drawables
	repeatButton_.setShow(FALSE);
	nextButton_.setShow(FALSE);

	// Signal to clear the log cell infos.
	clearLogEntry_ = TRUE;

	// Hide the data/time, diagnostic code and status cells
	log_.updateEntryColumn(estCurrentTestIndex_, COL_1_TIME);
	log_.updateEntryColumn(estCurrentTestIndex_, COL_3_DIAG_CODE);
	log_.updateEntryColumn(estCurrentTestIndex_, COL_4_RESULT);

	// Signal to display the log cell infos.
	clearLogEntry_ = FALSE;

	// Display the overall testing status on the service status area
	estStatus_.setStatus(MiscStrs::EST_RUNNING_MSG);

	// Reset the isThisEstTestCompleted_ for next test
	isThisEstTestCompleted_ = FALSE;

	// Reset the mostSevereStatusId_ for next test
	mostSevereStatusId_ = PASSED_TEST_ID;

	// Set Primary prompt to "Testing....."
	testManager_.setTestPrompt(PromptArea::PA_PRIMARY,
			PromptArea::PA_LOW, PromptStrs::EST_TESTING_P);

	// Set the Advisory prompt to Testing is in progress.
	testManager_.setTestPrompt(PromptArea::PA_ADVISORY,
		 PromptArea::PA_LOW, NULL_STRING_ID);

	// Clear the Secondary prompt
	testManager_.setTestPrompt(PromptArea::PA_SECONDARY,
		PromptArea::PA_LOW, NULL_STRING_ID);

	// Setup the Upper Screen Area to display the appropriated test result.
	setupUpperScreenLayout_(estCurrentTestIndex_);

#if defined FORNOW	
printf("   estCurrentTestIndex=%d, estTestId_[%d]=%d\n",
		estCurrentTestIndex_, estCurrentTestIndex_,	estTestId_[estCurrentTestIndex_]);
#endif	// FORNOW

	SmManager::DoFunction(SmTestId::DISABLE_SINGLE_TEST_ID);	

	// Start the test.
	SmManager::DoTest(estTestId_[estCurrentTestIndex_], repeatTestRequested_);

	repeatTestRequested_ = FALSE;
				// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: nextTest_
//
//@ Interface-Description
//	Handles the test and interface control at the end of each test.
//---------------------------------------------------------------------
//@ Implementation-Description
//	First, update the overallStatusId_ to reflect the most severe status
//	of all tests completed so far.  Then decide whether we have more tests
//	to do or it is at the end of the EST test and execute it.
//  
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
EstTestSubScreen::nextTest_(void)
{
	CALL_TRACE("EstTestSubScreen::nextTest_(void)");

#if defined FORNOW
printf("nextTest_ at line  %d, nextState_ = %d\n", __LINE__, nextState_);
#endif	// FORNOW

	// Stop possible running away timer
	GuiTimerRegistrar::CancelTimer(GuiTimerId::SERVICE_MODE_TEST_PAUSE_TIMEOUT);

	// Need to record the overall status id because the user had chosen
	// to ignore the current test errors and go ahead to the next test.
	switch (overallStatusId_)
	{	// Only in NEXT state the overall status id needs to be updated.
	case PASSED_TEST_ID:		// $[TI1]
		overallStatusId_ = mostSevereStatusId_;
		break;
	case MINOR_TEST_FAILURE_ID:	// $[TI2]
		if (mostSevereStatusId_ == MAJOR_TEST_FAILURE_ID)
		{
			overallStatusId_ = mostSevereStatusId_;
		}
		break;
	case MAJOR_TEST_FAILURE_ID:	// $[TI3]
		break;
	default:
		CLASS_ASSERTION_FAILURE();
		break;
	}

	// Set next test index
	estCurrentTestIndex_++;

	if (estCurrentTestIndex_ < maxServiceModeTest_)
	{	// More test to do.
		// This is purposely done to force a automatically generated
		// state to happen.
		// $[TI4]
		nextState_ = getNextState_(AUTO_EVENT);
	}	
	else
	{	// End of the test.
		// $[TI5]
		estCurrentTestIndex_--;
		nextState_ = getNextState_(AUTO_END_OF_TEST_EVENT);
	}

#if defined FORNOW
printf("nextTest_ at line  %d, nextState_ = %d\n", __LINE__, nextState_);
#endif	// FORNOW
	executeState_();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: repeatOrNextTest_
//
//@ Interface-Description
//	Provide the user an selection to either repeat the same test or proceed
//  to next test.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Set the appropriate prompt, status and repeat/next buttons.
// $[07012] At the end of EST test, if any test failure or alert were detected ...
//---------------------------------------------------------------------
//@ PreCondition
//	none 
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
EstTestSubScreen::repeatOrNextTest_(void)
{
	CALL_TRACE("EstTestSubScreen::epeatOrNextTest_(void)");

#if defined FORNOW
printf("repeatOrNextTest_ at line  %d\n", __LINE__);
#endif	// FORNOW

	if (keyAllowedId_ == SmPromptId::NO_KEYS || keyAllowedId_ == SmPromptId::USER_EXIT_ONLY)
	{		// $[TI1]
		// Service mode had request either an ACCEPT or a CLEAR prompt action.
		// Make sure that nothing in this subscreen keeps focus.
		AdjustPanel::TakeNonPersistentFocus(NULL, TRUE);

		// Clear the Primary prompts
		testManager_.setTestPrompt(PromptArea::PA_PRIMARY,
				 PromptArea::PA_HIGH, NULL_STRING_ID);
	}		// $[TI2]

	// Set Primary prompt to "Touch a button....."
	testManager_.setTestPrompt(PromptArea::PA_PRIMARY,
				PromptArea::PA_LOW, PromptStrs::TOUCH_A_BUTTON_P);

	// Display the overall testing status on the service status area
	estStatus_.setStatus(MiscStrs::EST_HALTED_MSG);

	// Show the following drawables
	repeatButton_.setShow(TRUE);
	nextButton_.setShow(TRUE);

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: estTestPause2Seconds_
//
//@ Interface-Description
//	Provide the user an 2 second pause for viewing the test data on the upper
//  screen before starting a new test.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Cancel running away timer, display the appropriate prompt messages.
//  Following by displaying the overall testing status on the service
//  status area then Start the appropriated countdown timer for each test.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
EstTestSubScreen::estTestPause2Seconds_(void)
{
	CALL_TRACE("EstTestSubScreen::estTestPause2Seconds_(void)");

#if defined FORNOW
printf("estTestPause2Seconds_ at line  %d\n", __LINE__);
#endif	// FORNOW

	// Cancel running away timer
	GuiTimerRegistrar::CancelTimer(GuiTimerId::SERVICE_MODE_TEST_PAUSE_TIMEOUT);

	if (keyAllowedId_ == SmPromptId::NO_KEYS || keyAllowedId_ == SmPromptId::USER_EXIT_ONLY)
	{		// $[TI1]
		// Service mode had request either an ACCEPT or a CLEAR prompt action.
		// Make sure that nothing in this subscreen keeps focus.
		AdjustPanel::TakeNonPersistentFocus(NULL, TRUE);

		// Clear the Primary prompts
		testManager_.setTestPrompt(PromptArea::PA_PRIMARY,
				 PromptArea::PA_HIGH, NULL_STRING_ID);
	}		// $[TI2]

	// Set Primary prompt to "Please Wait...."
	testManager_.setTestPrompt(PromptArea::PA_PRIMARY,
				PromptArea::PA_LOW, PromptStrs::SST_PLEASE_WAIT_P);

	// Clear the Advisory prompts
	testManager_.setTestPrompt(PromptArea::PA_ADVISORY,
			 PromptArea::PA_LOW, NULL_STRING_ID);

	// Clear the Secondary prompt.
	testManager_.setTestPrompt(PromptArea::PA_SECONDARY,
			PromptArea::PA_LOW, NULL_STRING_ID);

	// Display the overall testing status on the service status area
	estStatus_.setStatus(MiscStrs::EST_PAUSING_MSG);

	// $[07053] Following the completion of an EST test, a period of 2 second ...
	// Start the appropriated countdown timer for each test.
	GuiTimerRegistrar::StartTimer(GuiTimerId::SERVICE_MODE_TEST_PAUSE_TIMEOUT, this);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: displayPausingRequest_
//
//@ Interface-Description
//	Provide user the feedback when the EXIT key is pressed.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Grep the NonPersistentFocus, disable the knob sound, and set advisory/
//  secondry prompts.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
EstTestSubScreen::displayPausingRequest_(void)
{
	CALL_TRACE("EstTestSubScreen::displayPausingRequest_(void)");

#if defined FORNOW
printf("displayPausingRequest_ at line  %d\n", __LINE__);
#endif	// FORNOW

	// Grab Adjust Panel focus
	AdjustPanel::TakeNonPersistentFocus(this, TRUE);

	// Disable the knob sound.
	AdjustPanel::DisableKnobRotateSound();

	//
	// Set primary prompt to "Press ACCEPT to confirm."
	//
	testManager_.setTestPrompt(PromptArea::PA_PRIMARY, PromptArea::PA_HIGH,
						PromptStrs::SST_PRESS_ACCEPT_TO_CONFIRM_P);

	// Set advisory prompt to "Exit EST is requested"
	testManager_.setTestPrompt(PromptArea::PA_ADVISORY, PromptArea::PA_HIGH,
						PromptStrs::EST_USER_REQUESTING_EXIT_A);

	// Set secondry prompt to "To cancel: touch EXIT EST."
	testManager_.setTestPrompt(PromptArea::PA_SECONDARY,
						PromptArea::PA_HIGH, PromptStrs::EST_CANCEL_S);
						// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setupTestResultOnUpperScreen_
//
//@ Interface-Description
//	Activate the upper screen in preparing to display the currently running
//  test data.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Clear all upper screen display.  And activate the EstResultSubScreen.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
EstTestSubScreen::setupTestResultOnUpperScreen_(void)
{
	CALL_TRACE("EstTestSubScreen::setupTestResultOnUpperScreen_(void)");
#if defined FORNOW	
printf("setupTestResultOnUpperScreen_ at line  %d, pEstResultSubScreen_ = %d\n",
					__LINE__, pEstResultSubScreen_);
#endif	// FORNOW


	pEstResultSubScreen_->clearScreen();

	// Activate the EstResultSubScreen
	pUpperSubScreenArea_->activateSubScreen(
					UpperSubScreenArea::GetEstResultSubScreen(), NULL);
					// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setupUpperScreenLayout_
//
//@ Interface-Description
//	Setup the exact title and display format for the currently running test data.
//---------------------------------------------------------------------
//@ Implementation-Description
//  If it is a valid test then do the following steps.
//  Set the Title text on the Upper SubScreen EST result SubScreen.
//  Set the data display format for this test.
//  Else, erase the screen display.
// $[07024] This subscreen displays the results of a single EST test, ...
// $[07052] At the start fo each test in EST, the EST test Result subscreen ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
EstTestSubScreen::setupUpperScreenLayout_(Int estCurrentTestIndex)
{
	CALL_TRACE("EstTestSubScreen::setupUpperScreenLayout_(Int estCurrentTestIndex)");
#if defined FORNOW	
printf("setupUpperScreenLayout_ at line  %d\n", __LINE__);
#endif	// FORNOW


	if (estCurrentTestIndex != EST_NO_TEST_ID)
	{				// $[TI1]
#if defined FORNOW	
printf("$$$$estTestResultTitle_[%d]=%s, pEstResultSubScreen_ = %d\n", estCurrentTestIndex,
					estTestResultTitle_[estCurrentTestIndex], pEstResultSubScreen_);
#endif	// FORNOW

		// Set the Title text on the Upper SubScreen EST result SubScreen
		pEstResultSubScreen_->setResultTitle(estTestResultTitle_[estCurrentTestIndex]);

		pEstResultSubScreen_->setDataFormat((SmTestId::ServiceModeTestId)
							estTestId_[estCurrentTestIndex]);
	}
	else
	{				// $[TI2]
		pEstResultSubScreen_->clearScreen();
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: displayPrompt_
//
//@ Interface-Description
//	Provide user the feedback of displaying the prompt requested from
//  the Service-Mode subsystem.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Based on the "keyAllowedId", we grab the non-persistent focus, disable
//  the knob rotate sound, set the prompts, and display the overall testing
//  status on the service status area
// $[07015] During testing, EST shall display the current test name, ....
// $[07037] During testing, the EST GUI shall display message in the ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
EstTestSubScreen::displayPrompt_(void)
{
	CALL_TRACE("EstTestSubScreen::displayPrompt_(void)");

#if defined FORNOW
printf("displayPrompt_ at line  %d, keyAllowedId_ = %d\n", __LINE__, keyAllowedId_);
#endif	// FORNOW

	// Search the corresponding prompt string according to the promptId_
	Int idx;
	for (idx = 0;
			 idx < NUM_EST_TEST_PROMPT_MAX && estTestPromptId_[idx] != promptId_;
			 idx++);

	SAFE_CLASS_ASSERTION(idx < NUM_EST_TEST_PROMPT_MAX);

#if defined FORNOW
printf("EstTestSubScreen::displayPrompt_() clear advisory prompt\n");
#endif	// FORNOW

	// Clear advisory prompt
	testManager_.setTestPrompt(PromptArea::PA_ADVISORY, PromptArea::PA_HIGH,
						NULL_STRING_ID);

#if defined FORNOW
printf("EstTestSubScreen::displayPrompt_() display secondary prompt\n");
#endif	// FORNOW

	switch (keyAllowedId_)
	{
	case SmPromptId::ACCEPT_KEY_ONLY:
					// $[TI1]
		// Grab Adjust Panel focus
		AdjustPanel::TakeNonPersistentFocus(this, TRUE);

		// Disable the knob sound.
		AdjustPanel::DisableKnobRotateSound();

		// Set the Secondary prompt to "When done: press Accept."
		// "To cancel EST: touch EXIT EXT, then Accept."
		testManager_.setTestPrompt(PromptArea::PA_SECONDARY, PromptArea::PA_HIGH,
						PromptStrs::EST_PROMPT_ACCEPT_CANCEL_S);
		break;
	case SmPromptId::ACCEPT_AND_CANCEL_ONLY:
					// $[TI2]
		// Grab Adjust Panel focus
		AdjustPanel::TakeNonPersistentFocus(this, TRUE);

		// Disable the knob sound.
		AdjustPanel::DisableKnobRotateSound();

		// Set the Secondary prompt to "When done: press Accept, to skip test: press Clear."
		// "To cancel EST: touch EXIT EXT, then Accept."
		testManager_.setTestPrompt(PromptArea::PA_SECONDARY, PromptArea::PA_HIGH,
						PromptStrs::EST_PROMPT_ACCEPT_CLEAR_CANCEL_S);
		break;
	case SmPromptId::ACCEPT_PASS_AND_CANCEL_FAIL_ONLY:
					// $[TI3]
		// Grab Adjust Panel focus
		AdjustPanel::TakeNonPersistentFocus(this, TRUE);

		// Disable the knob sound.
		AdjustPanel::DisableKnobRotateSound();

		// Set the Secondary prompt to "If passed: press ACCEPT, if failed: press Clear."
		// "To cancel EST: touch EXIT EXT, then Accept."
		testManager_.setTestPrompt(PromptArea::PA_SECONDARY, PromptArea::PA_HIGH,
						PromptStrs::EST_PROMPT_ACCEPT_PASS_AND_CANCEL_FAIL_S);
		break;
	case SmPromptId::NO_KEYS:
					// $[TI4]
		// Clear primary prompt
		testManager_.setTestPrompt(PromptArea::PA_PRIMARY, PromptArea::PA_HIGH,
						NULL_STRING_ID);

		// Clear secondary prompt
		testManager_.setTestPrompt(PromptArea::PA_SECONDARY, PromptArea::PA_HIGH,
						NULL_STRING_ID);

		// Clear the Adjust Panel focus
		AdjustPanel::TakeNonPersistentFocus(NULL, TRUE);

		// Clear the off screen key message
		GuiApp::PMessageArea->clearMessage(MessageArea::MA_HIGH);
		break;
	case SmPromptId::USER_EXIT_ONLY:
					// $[TI5]
		// Clear primary prompt
		testManager_.setTestPrompt(PromptArea::PA_PRIMARY, PromptArea::PA_HIGH,
						NULL_STRING_ID);

		// Clear secondary prompt
		testManager_.setTestPrompt(PromptArea::PA_SECONDARY, PromptArea::PA_HIGH,
						NULL_STRING_ID);

		// Clear the Adjust Panel focus
		AdjustPanel::TakeNonPersistentFocus(NULL, TRUE);

		// Clear the off screen key message
		GuiApp::PMessageArea->clearMessage(MessageArea::MA_HIGH);
		break;
	default:
		CLASS_ASSERTION_FAILURE();
		break;
	}

	// The primary is set after the call to the
	// AdjustPanel::TakeNonPersistentFocus()
	// to remove any previous keyboard set adjustPanel focus.
	//
	// Set primary prompt according to the promptId_
	savedPromptName_ = estTestPromptName_[idx];
	testManager_.setTestPrompt(PromptArea::PA_PRIMARY, PromptArea::PA_HIGH,
							savedPromptName_);

	// Display the overall testing status on the service status area
	estStatus_.setStatus(MiscStrs::EST_WAITING_MSG);

	// This code forces the execution to continue when a prompt message
	// is displayed but the regular keyboard, knob, or touch control is
	// not enabled due to the takeover of the I/O interrupt handler by
	// Service mode managaer.
	if  (keyAllowedId_ == SmPromptId::NO_KEYS || keyAllowedId_ == SmPromptId::USER_EXIT_ONLY)
	{			// $[TI6]
		userKeyPressedId_ = SmPromptId::NULL_ACTION_ID;

		nextState_ = getNextState_(NO_KEY_EVENT);
		executeState_();
	}			// $[TI7]

#if defined FORNOW
printf("displayPrompt_ at line  %d, keyAllowedId_ = %d\n", __LINE__, keyAllowedId_);
#endif	// FORNOW

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: displayAndReviewEstResults_
//
//@ Interface-Description
//  Display the overall testing status on the service status area and
//  setup the display page starting from the "firstDisplayItem".
//---------------------------------------------------------------------
//@ Implementation-Description
//  Clear the upper screen area.  Set appropriate prompts.  Show the
//  startTest button.  Hide the exit, repeat, next, and override buttons.
//  Display the overall testing status on the service status area.
//  Setup the test logs to scrollable and selectable.  Clear the selected
//  entry highlight.  Finally, setup the display page.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
EstTestSubScreen::displayAndReviewEstResults_(int firstDisplayItem)
{
#if defined FORNOW
printf("displayAndReviewEstResults_ at line  %d\n", __LINE__);
#endif	// FORNOW

	// Clear the upper screen area.
	pEstResultSubScreen_->clearScreen();

	// Set Primary prompt to "Touch START TEST to begin."
	testManager_.setTestPrompt(PromptArea::PA_PRIMARY,
		PromptArea::PA_LOW, PromptStrs::EST_TOUCH_SINGLE_AND_START_TEST_TO_BEGIN_P);

	testManager_.setTestPrompt(PromptArea::PA_ADVISORY,
						PromptArea::PA_HIGH, NULL_STRING_ID);

	// Set Secondary prompt to "Touch buttons to scroll
	//							through previous test result."
	// "To ventilate: touch EXIT EST."
	testManager_.setTestPrompt(PromptArea::PA_SECONDARY,
		PromptArea::PA_LOW, PromptStrs::EST_TOUCH_BUTTONS_TO_SCROLL_S);

	// Show the following drawables
	startTestButton_.setShow(TRUE);
	startTestButton_.setToUp();

	// Hide the following drawables
	exitButton_.setShow(FALSE);
	repeatButton_.setShow(FALSE);
	nextButton_.setShow(FALSE);
	overrideButton_.setShow(FALSE);

	singleTestButton_.setShow(TRUE);
	singleTestButton_.setToFlat();
    
	// Initialize the next status
	nextState_ = INITIAL_STATE;
	previousState_ = INITIAL_STATE;
	fallBackState_ = INITIAL_STATE;
	overrideRequested_ = FALSE;
	overrideCounter_ = -1;
	repeatTestRequested_ = FALSE;

	// Get Est test result table from NovRam
    NovRamManager::GetEstResultEntries(estResultTable_);

	// Display the overall testing status on the service status area
	displayEstResultOutcome_();

	// $[07041] When the EST subscreen is selected but EST is not running ...
	// Setup the test logs to scrollable and selectable
	log_.setUserScrollable(TRUE);
	log_.setUserSelectable(TRUE);

	estCurrentTestIndex_ = EST_TEST_START_ID;

	// Clear the selected entry highlight.
	log_.clearSelectedEntry();

	// Setup the display page 
	if (!isSingleTestMode_)
	{
		log_.setEntryInfo(firstDisplayItem, maxServiceModeTest_);
	}

	// Let the service mode  manager knows the type of Service mode.
	SmManager::DoFunction(SmTestId::SET_TEST_TYPE_EST_ID);
					// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: displayEstFinishedScreen
//
//@ Interface-Description
//	Provide user the feedback of displaying the EST's completed screen.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Display appropriate prompts and buttons.  For MINOR FAILURE, we
//  will show the "Override" button.  Display the overall testing
//  status is also necessary.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
EstTestSubScreen::displayEstFinishedScreen(void)
{
	CALL_TRACE("EstTestSubScreen::displayEstFinishedScreen(void)");

#if defined FORNOW
printf("displayEstFinishedScreen at line  %d\n", __LINE__);
#endif	// FORNOW

	// Set Primary prompt to "Make a selection."
	testManager_.setTestPrompt(PromptArea::PA_PRIMARY,
			PromptArea::PA_LOW, PromptStrs::TOUCH_A_BUTTON_P);

	// Hide the following drawables
	repeatButton_.setShow(FALSE);
	nextButton_.setShow(FALSE);

	// Unhighlight the entry.
	log_.clearSelectedEntry();

	// Display the EST completion outcome.
	displayEstResultOutcome_();

	// Setup the test logs to scrollable and selectable
	log_.setUserScrollable(TRUE);
	log_.setUserSelectable(TRUE);

	if (overallStatusId_ == MINOR_TEST_FAILURE_ID)
	{					// $[TI1]
		// Set advisory prompt to "Ventilator inoperative!"
		testManager_.setTestPrompt(PromptArea::PA_ADVISORY, PromptArea::PA_LOW,
					PromptStrs::EST_SERVICE_REQUIRED_A);

		// Set Secondary prompt to "Touch OVERRIDE to ignore minor failures,
		//							touch EXIT EST to quit."
		testManager_.setTestPrompt(PromptArea::PA_SECONDARY,
					PromptArea::PA_LOW, PromptStrs::EST_TOUCH_OVERRIDE_EXIT_S);

		// Show the following drawables
		overrideButton_.setShow(TRUE);
		exitButton_.setShow(TRUE);
	}
	else
	{					// $[TI2]
		if (overallStatusId_ == MAJOR_TEST_FAILURE_ID)
		{				// $[TI2.1]
			// Set advisory prompt to "Ventilator inoperative!"
			testManager_.setTestPrompt(PromptArea::PA_ADVISORY, PromptArea::PA_LOW,
					PromptStrs::EST_SERVICE_REQUIRED_A);
		}
		else
		{				// $[TI2.2]
			// Clear the advisory prompt
			testManager_.setTestPrompt(PromptArea::PA_ADVISORY,
					PromptArea::PA_HIGH, NULL_STRING_ID);
		}

		// Clear the Secondary prompt.
		testManager_.setTestPrompt(PromptArea::PA_SECONDARY,
					PromptArea::PA_LOW, NULL_STRING_ID);

		// Hide the following drawables
		overrideButton_.setShow(FALSE);
		exitButton_.setShow(FALSE);

		// This is purposely done to force a automatically generated
		// state to happen.
		nextState_ = getNextState_(AUTO_EVENT);
		executeState_();
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: displayEstResultOutcome_
//
//@ Interface-Description
//	Provide user the feedback of displaying the EST's overall status result.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Show test completed time stamp and overall EST outcome.
//  Because the EST test is designed to display either reviewing status
//  for previous run or updating status of current run, so we have to
//  distinguish which status result to use when displaying the result.  When
//  we are in "reviewMode_",  we should use getResultId() to display the
//  previous test status.  When we are in running mode, we should use
//  getResultOfCurrRun() to display the current test status.
// $[07016] At the end of the EST run, Sigma shall display "PASSED" if ...
// $[01365] If a test is stopped prior to completion, its status shall be...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
EstTestSubScreen::displayEstResultOutcome_(void)
{
	CALL_TRACE("EstTestSubScreen::displayEstResultOutcome_(void)");

	ResultEntryData overallEstResultData;

	// Get the overall testing status
	NovRamManager::GetOverallEstResult(overallEstResultData);

	TestResultId currentTestResultId;

	if (reviewMode_)
	{	 // $[TI0.1]
		currentTestResultId = overallEstResultData.getResultId();
	}
	else
	{  // $[TI0.2]
		currentTestResultId = overallEstResultData.getResultOfCurrRun();
	}
			
#if defined FORNOW
printf("displayEstResultOutcome_ at line  %d, overallResult = %d\n",
					 __LINE__, currentTestResultId);
#endif	// FORNOW

	switch (currentTestResultId)
	{
	case PASSED_TEST_ID:
					// $[TI1]
		estStatus_.setStatus(MiscStrs::PASSING_OVERALL_MSG);
		break;
	case MINOR_TEST_FAILURE_ID:
					// $[TI2]
		estStatus_.setStatus(MiscStrs::ALERTING_OVERALL_MSG);
		break;
	case MAJOR_TEST_FAILURE_ID:
					// $[TI3]
		estStatus_.setStatus(MiscStrs::FAILURE_OVERALL_MSG);
		break;
	case OVERRIDDEN_TEST_ID:
					// $[TI4]
		estStatus_.setStatus(MiscStrs::OVERRIDEN_OVERALL_MSG);
		break;
	case INCOMPLETE_TEST_RESULT_ID:
					// $[TI5]
		estStatus_.setStatus(MiscStrs::IMCOMPLETE_OVERALL_MSG);
		break;
	case UNDEFINED_TEST_RESULT_ID:
					// $[TI6]
		estStatus_.setStatus(MiscStrs::UNDEFINED_OVERALL_MSG);
		break;
	case NOT_APPLICABLE_TEST_RESULT_ID:
					// $[TI7]
	default:
		CLASS_ASSERTION_FAILURE();
		break;
	}

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getNextState_
//
//@ Interface-Description
//	Handles the state transition based on the latest coming event.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Update newEvent_, previousState_ and nextState_.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Int
EstTestSubScreen::getNextState_(Int newEvent)
{
	CALL_TRACE("EstTestSubScreen::getNextState_(Int newEvent)");

#if defined FORNOW
printf("getNextState_ at line %d, state = %d, event = %d\n",
			 __LINE__, nextState_, newEvent);
#endif	// FORNOW

	if (!isVisible())
	{													// $[TI1]
		return(nextState_);
	}													// $[TI2]

	newEvent_ = newEvent;

	previousState_ = nextState_;
	return(estStateTable_[previousState_][newEvent]);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: executeState_
//
//@ Interface-Description
//	Handles the execution of state transition request.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Based on "nextState_", appropriated processing (screen update, control
//  of test flow, etc) will be executed to maintain the requirements for
//  this state transition process.
// $[07008] When the test run finished or is stopped by the technician ...
// $[LC07002] Touching the button for FULL EST, if active...
// $[LC07004] If the technician touches the SINGLE TEST button, if active...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
EstTestSubScreen::executeState_(void)
{
	CALL_TRACE("EstTestSubScreen::executeState_(void");

#if defined FORNOW
printf("   BEGIN executeState_ Recursion count=%d, nowState = %d, event=%d\n",
				RecursionCount_, nextState_, newEvent_);
#endif	// FORNOW

RecursionCount_++;

	if (!isVisible())
	{													// $[TI0.1]
		return;
	}													// $[TI0.2]

	switch (nextState_)
	{
	case INITIAL_STATE:			// $[TI1]
		// Release Adjust Panel focus
		AdjustPanel::TakeNonPersistentFocus(NULL, TRUE);
		//
		// Set NULL to high priority prompts
		//
		testManager_.setTestPrompt(PromptArea::PA_PRIMARY,
							PromptArea::PA_HIGH, NULL_STRING_ID);
		testManager_.setTestPrompt(PromptArea::PA_ADVISORY,
							PromptArea::PA_HIGH, NULL_STRING_ID);
		testManager_.setTestPrompt(PromptArea::PA_SECONDARY,
							PromptArea::PA_HIGH, NULL_STRING_ID);

		layoutScreen_(EST_SETUP_SCREEN);
		break;
	case START_SINGLE_STATE:
		if (singleTestButton_.getButtonState() == Button::DOWN)
		{
			// Grab Adjust Panel focus
			AdjustPanel::TakeNonPersistentFocus(this, TRUE);

			// Disable the knob sound.
			AdjustPanel::DisableKnobRotateSound();

			testManager_.setTestPrompt(PromptArea::PA_PRIMARY, PromptArea::PA_HIGH,
								PromptStrs::SST_PRESS_ACCEPT_TO_CONFIRM_P);
			//
			// Set primary prompt to "Press ACCEPT to confirm.", and set
			// advisory prompt to "automatic testing will begin."
			//
			testManager_.setTestPrompt(PromptArea::PA_ADVISORY, PromptArea::PA_HIGH,
								PromptStrs::EST_SINGLE_TEST_WILL_BEGIN_A);
			testManager_.setTestPrompt(PromptArea::PA_SECONDARY,
								PromptArea::PA_HIGH, PromptStrs::EST_SINGLE_TEST_CANCEL_S);

		}
		else if (singleTestButton_.getButtonState() == Button::UP)
		{
			// Release Adjust Panel focus
			AdjustPanel::TakeNonPersistentFocus(NULL, TRUE);

			//
			// Set NULL to high priority prompts
			//
			testManager_.setTestPrompt(PromptArea::PA_PRIMARY,
								PromptArea::PA_HIGH, NULL_STRING_ID);
			testManager_.setTestPrompt(PromptArea::PA_ADVISORY,
								PromptArea::PA_HIGH, NULL_STRING_ID);
			testManager_.setTestPrompt(PromptArea::PA_SECONDARY,
								PromptArea::PA_HIGH, NULL_STRING_ID);
            
		}
		break;
	case REPEAT_STATE:			// $[TI2]
	case START_STATE:			// $[TI3]
	case NEXT_STATE:			// $[TI4]
	case OVERRIDE_STATE:		// $[TI5]
		// Grab Adjust Panel focus
		AdjustPanel::TakeNonPersistentFocus(this, TRUE);

		// Disable the knob sound.
		AdjustPanel::DisableKnobRotateSound();

		testManager_.setTestPrompt(PromptArea::PA_PRIMARY, PromptArea::PA_HIGH,
							PromptStrs::SST_PRESS_ACCEPT_TO_CONFIRM_P);

		if (nextState_ == OVERRIDE_STATE)
		{						// $[TI6]
			//
			// Set primary prompt to "Press ACCEPT to confirm.", and set
			// secondary prompt to "To cancel: touch EXIT EST."
			//
			testManager_.setTestPrompt(PromptArea::PA_ADVISORY, PromptArea::PA_HIGH,
								NULL_STRING_ID);
			testManager_.setTestPrompt(PromptArea::PA_SECONDARY,
								PromptArea::PA_HIGH, PromptStrs::EST_CANCEL_S);

			overrideRequested_ = TRUE;
		}
		else
		{		
			// Changed the prompts based on the current state and if single test mode is
			// enabled.
			if (isSingleTestMode_ &&  (nextState_ == NEXT_STATE))
			{
				testManager_.setTestPrompt(PromptArea::PA_ADVISORY, PromptArea::PA_HIGH,
									PromptStrs::EST_SINGLE_TEST_WILL_EXIT_A);

			}
			else if (isSingleTestMode_ && (nextState_ == REPEAT_STATE))
			{
				testManager_.setTestPrompt(PromptArea::PA_ADVISORY, PromptArea::PA_HIGH,
									PromptStrs::EST_REPEAT_CURRENT_TEST_A);
			}
			else
			{
				//
				// Set primary prompt to "Press ACCEPT to confirm.", and set
				// advisory prompt to "automatic testing will begin."
				//
				testManager_.setTestPrompt(PromptArea::PA_ADVISORY, PromptArea::PA_HIGH,
									PromptStrs::SST_AUTO_TEST_WILL_BEGIN_A);
			}
			testManager_.setTestPrompt(PromptArea::PA_SECONDARY,
								PromptArea::PA_HIGH, NULL_STRING_ID);
		}

		if (nextState_ == START_STATE)
		{						// $[TI37.1]
			layoutScreen_(EST_READY_TO_START_SCREEN);
			testManager_.setTestPrompt(PromptArea::PA_SECONDARY,
								PromptArea::PA_HIGH, PromptStrs::EST_START_CANCEL_S);
		}						// $[TI37.2]
		break;

	case READY_TO_TEST_STATE:	// $[TI8]


		if (isSingleTestMode_)
		{
		    // Initialize the overall status.
		    overallStatusId_ = UNDEFINED_TEST_RESULT_ID;
			layoutScreen_(EST_READY_TO_START_SCREEN);
		}
		else
		{
		    // Initialize the overall status.
		    overallStatusId_ = PASSED_TEST_ID;
		}

		// The initial test screen will be displayed correctly.
		layoutScreen_(EST_START_SCREEN);

        // If the single test is a BD test,
		// display the "Remove insp filter and connect test tubing" 
		// and "Ensure air and O2 are connected" prompt.
		if (isSingleTestMode_)
		{
			
			switch (estCurrentTestIndex_)
			{
				case EST_TEST_FS_CROSS_CHECK_ID:
				case EST_TEST_SM_LEAK_ID:
			    case EST_TEST_GAS_SUPPLY_ID:
				case EST_TEST_SAFETY_SYSTEM_ID:
				case EST_TEST_EXH_VALVE_SEAL_ID:
				case EST_TEST_EXH_VALVE_ID:
				case EST_TEST_EXH_HEATER_ID:
				case EST_TEST_BATTERY_ID:

					showBdTestPrompt_ = TRUE;
					showAirO2ConnectedPrompt_ = TRUE;
                    wasBdTestPromptShown_ = TRUE;
					break;
			   default:
				    showBdTestPrompt_ = FALSE;
					wasBdTestPromptShown_ = FALSE;
					break;
			}
		}

		// If the "Remove insp filter and connect test tubing."
		// prompt is displayed otherwise continue to the DO_TEST_STATE.
		if (showBdTestPrompt_ && isSingleTestMode_)
		{
			
			nextState_ = getNextState_(USER_PROMPT_EVENT);
			executeState_();
			break;
		}
		else
		{		
			// This is purposely done to force a automatically generated
			// state to happen.
			nextState_ = getNextState_(AUTO_EVENT);
	
			// This two states have to be in this sequence. So that after
			// the setup of the initial testing screen, the first test
			// shall start automatically.
		}
	case DO_TEST_STATE:			// $[TI9]
		if (isSingleTestMode_)
		{
            startSingleTest_();
		}
		else
		{		
			startTest_();
		}
		break;
	case BD_PROMPT_STATE:
		if (isSingleTestMode_)
		{			
			// If the single test is a BD test,
			// display the "Remove insp filter and connect test tubing" 
			// and "Ensure air and O2 are connected" prompt.
			if (showBdTestPrompt_)
			{
				
				// Grab Adjust Panel focus
				AdjustPanel::TakeNonPersistentFocus(this, TRUE);
	
				// Disable the knob sound.
				AdjustPanel::DisableKnobRotateSound();
	
				// Clear advisory prompt
				testManager_.setTestPrompt(PromptArea::PA_ADVISORY, PromptArea::PA_HIGH,
										   NULL_STRING_ID);
				testManager_.setTestPrompt(PromptArea::PA_ADVISORY, PromptArea::PA_LOW,
										   NULL_STRING_ID);
	
				// Set the Secondary prompt to "When done: press Accept."
				// "To cancel EST: touch EXIT EXT, then Accept."
				testManager_.setTestPrompt(PromptArea::PA_SECONDARY, PromptArea::PA_HIGH,
										   PromptStrs::EST_PROMPT_ACCEPT_CANCEL_S);
	
				if (showAirO2ConnectedPrompt_)
				{
	
					// Set the Primary prompt to "Ensure air and O2 are connected"
					testManager_.setTestPrompt(PromptArea::PA_PRIMARY, PromptArea::PA_HIGH,
											   PromptStrs::SM_CONNECT_AIR_AND_O2_PROMPT_P);
					showAirO2ConnectedPrompt_ = FALSE;
	
				
				}
				else
				{
					// Set the Primary prompt to "Remove insp filter and connect test tubing."
					testManager_.setTestPrompt(PromptArea::PA_PRIMARY, PromptArea::PA_HIGH,
											   PromptStrs::SM_REMOVE_INSP_FILTER_P);
	
					showAirO2ConnectedPrompt_ = TRUE;
					showBdTestPrompt_= FALSE;
				}
			}
			else
			{
				wasBdTestPromptShown_ = FALSE;
				// Execute the test since the prompts has been displayed.
				nextState_ = getNextState_(AUTO_EVENT);
				executeState_();
	
			}
		}

		break;
	case DO_NEXT_STATE:			// $[TI10]
		if (isSingleTestMode_)
		{
			nextState_ = getNextState_(AUTO_END_OF_TEST_EVENT);
			executeState_();
		}
		else
		{		
		    nextTest_();
		}
		break;

	case PROMPT_STATE:			// $[TI11]
		displayPrompt_();
		break;

	case TEST_PASS_STATE:		// $[TI12]
		// Test is successful without user exiting.  Start 2 second
		// waiting before the next test starts.
		estTestPause2Seconds_();
		break;

	case EXIT_BEFORE_PROMPT_STATE:	// $[TI13]
		// Display the overall testing status on the service status area
		estStatus_.setStatus(MiscStrs::EST_PAUSING_MSG);
		displayPausingRequest_();
		setupFallbackState_(AUTO_EVENT);
		break;

	case EXECUTE_PROMPT_STATE:		// $[TI14]
		nextState_ = DO_TEST_STATE;
		promptId_ = SmPromptId::NULL_PROMPT_ID;

		// Signal the Service Test Manager to continue the test.
#ifndef FAKE_SM
		if (userKeyPressedId_ != SmPromptId::NULL_ACTION_ID)
#endif
		{							// $[TI15]
			SmManager::OperatorAction(estTestId_[estCurrentTestIndex_],
						  userKeyPressedId_);
		}							// $[TI16]

		// Display the overall testing status on the service status area
		estStatus_.setStatus(MiscStrs::EST_RUNNING_MSG);
		break;

	case END_ALERT_STATE:			// $[TI17]
	    repeatOrNextTest_();
		break;

	case END_FAILURE_STATE:			// $[TI18]
        repeatOrNextTest_();
		break;

	case END_EXIT_ALERT_STATE:		// $[TI19]
		// At the end of test with an ALERT error and EXIT request
		// In case the user press the EXIT button again, set the
		// fall back state.
		setupFallbackState_(AUTO_EVENT);
		nextState_ = getNextState_(AUTO_EVENT);
		executeState_();
		break;

	case END_EXIT_FAILURE_STATE:	// $[TI20]
		// At the end of test with an FAILURE error and EXIT request
		// In case the user press the EXIT button again, set the
		// fall back state.
		setupFallbackState_(AUTO_EVENT);
		nextState_ = getNextState_(AUTO_EVENT);
		executeState_();
		break;

	case END_EXIT_TEST_STATE:		// $[TI21]
		// At the end of test with EXIT requested
		// In case the user press the EXIT button again, set the
		// fall back state.
		setupFallbackState_(AUTO_EVENT);
		nextState_ = getNextState_(AUTO_EVENT);
		executeState_();
		break;

	case EXIT_REQUEST_STATE:		// $[TI22]
		// SM test has completed, confirm of the quitting
		displayPausingRequest_();
		break;

	case PAUSE_REQUEST_STATE:		// $[TI23]
		// Set Primary prompt to "Waiting for test to complete...."
		testManager_.setTestPrompt(PromptArea::PA_PRIMARY, PromptArea::PA_LOW, 
							PromptStrs::EST_WAIT_TEST_TO_COMPLETE_P);
	    setupFallbackState_(AUTO_EVENT);
		break;

	case EXIT_FALLBACK_STATE:		// $[TI24]
		// Make sure that nothing in this subscreen keeps focus.
		AdjustPanel::TakeNonPersistentFocus(NULL, TRUE);

		// Clear the high priority prompts
		testManager_.setTestPrompt(PromptArea::PA_PRIMARY,
							PromptArea::PA_HIGH, NULL_STRING_ID);
		testManager_.setTestPrompt(PromptArea::PA_ADVISORY, PromptArea::PA_HIGH,
							NULL_STRING_ID);
		testManager_.setTestPrompt(PromptArea::PA_SECONDARY,
							PromptArea::PA_HIGH, NULL_STRING_ID);

		// Reset the nextState_
		nextState_ = fallBackState_;

		if (nextState_ == DO_TEST_STATE)
		{							// $[TI24.1]
			// This is a normal test condition, user just pressed Exit buttom twice
			// And there was no prompt required user to process.  We are already in
			// the right state, so do nothing.

			// Set Primary prompt to "Testing....."
			testManager_.setTestPrompt(PromptArea::PA_PRIMARY,
					PromptArea::PA_LOW, PromptStrs::EST_TESTING_P);
		}
		else
		{							// $[TI24.2]
			// Force into the appropriate state when the next state is not in
			// DO_TEST_STATE or in any other state.
			executeState_();
		}
		break;

	case END_OF_EST_STATE:			// $[TI25]
		if (!isSingleTestMode_)
		{
			// Let the service mode  manager knows that completed status
			// has been issued.
			SmManager::DoFunction(SmTestId::EST_COMPLETE_ID);

		}

		displayEstFinishedScreen();
		SmManager::DoFunction(SmTestId::DISABLE_SINGLE_TEST_ID);	

		isSingleTestMode_ = FALSE;
		titleArea_.setTitle(MiscStrs::EXTENDED_SELF_TEST_SUBSCREEN_TITLE);
		break;

	case RESTART_EST_STATE:			// $[TI26]
		displayAndReviewEstResults_(maxServiceModeTest_ - EST_DISPLAY_ROW_MAX);		

		// Show the lower screen select area.
		ServiceLowerScreen::RServiceLowerScreen.getServiceLowerScreenSelectArea()->setBlank(FALSE);

		// Show the upper screen select area.
		ServiceUpperScreen::RServiceUpperScreen.getServiceUpperScreenSelectArea()->setBlank(FALSE);
		break;

	case EXEC_OVERRIDE_STATE:		// $[TI27]
		overallStatusId_ = OVERRIDDEN_TEST_ID;

		// Let the service mode  manager knows that overrid status
		// has been issued.
		SmManager::DoFunction(SmTestId::EST_OVERRIDE_ID);
		overrideCounter_ = 0;

		// Hide the following drawables
		overrideButton_.setShow(FALSE);
		exitButton_.setShow(FALSE);

		// Show the upper screen select area.
		ServiceUpperScreen::RServiceUpperScreen.getServiceUpperScreenSelectArea()->setBlank(FALSE);
		break;

	case EXECUTE_REPEAT_STATE:			// $[TI29]
		repeatTestRequested_ = TRUE;
		nextState_ = getNextState_(AUTO_EVENT);
		executeState_();
		break;

	case EXECUTE_EXIT_EST_STATE:		// $[TI30]
		// $[07014] The operator is allowed at any time to command testing to stop ...
		if (estCurrentTestIndex_ == EST_NO_TEST_ID || isThisEstTestCompleted_ || 
			(wasBdTestPromptShown_ && isSingleTestMode_))
		{								// $[TI31]
			nextState_ = getNextState_(USER_END_EVENT);
			executeState_();
		}
		else
		{								// $[TI32]
			// Signal the Service Test Manager to exit the EST test.
			SmManager::OperatorAction(estTestId_[estCurrentTestIndex_],
						  SmPromptId::USER_EXIT);
		}

		break;

	case EXIT_EST_STATE:				// $[TI33]
		// From this point the state machine is not active any more.
		nextState_ = UNDEFINED_NEXT_TEST;

		layoutScreen_(EST_EXIT_SCREEN);

		break;

	case EXECUTE_NO_ACT_PROMPT_STATE:	// $[TI34]
#if defined FAKE_SM
		// Signal the Fake Service Test Manager to continue the test.
		SmManager::OperatorAction(estTestId_[estCurrentTestIndex_],
							userKeyPressedId_);
#endif	// FAKE_SM

		// Set high Primary prompt to "Waiting for test to complete...."
		testManager_.setTestPrompt(PromptArea::PA_PRIMARY, PromptArea::PA_HIGH, 
							savedPromptName_);
		// Display the overall testing status on the service status area
		estStatus_.setStatus(MiscStrs::EST_RUNNING_MSG);
		break;

	case NO_ACT_PAUSE_PROMPT_STATE:		// $[TI35]
		displayPrompt_();
		nextState_ = NO_ACT_PAUSE_REQUEST_STATE;
		break;

	case NO_ACT_PAUSE_REQUEST_STATE:	// $[TI36]
#if defined FAKE_SM
		// Signal the Fake Service Test Manager to continue the test.
		SmManager::OperatorAction(estTestId_[estCurrentTestIndex_],
							userKeyPressedId_);
#endif	// FAKE_SM

		// Set high Primary prompt to "Waiting for test to complete...."
		testManager_.setTestPrompt(PromptArea::PA_PRIMARY, PromptArea::PA_HIGH, 
							savedPromptName_);
		// Set low Primary prompt to "Waiting for test to complete...."
		testManager_.setTestPrompt(PromptArea::PA_PRIMARY, PromptArea::PA_LOW, 
							PromptStrs::EST_WAIT_TEST_TO_COMPLETE_P);
		setupFallbackState_(AUTO_EVENT);
		break;

	case UNDEFINED_NEXT_TEST:			
	case EST_STATE_NUM:				
		AUX_CLASS_ASSERTION_FAILURE( (Uint32)nextState_);
		break;

	}


RecursionCount_--;

#if defined FORNOW
printf("   END executeState_ Recursion count=%d, nextState = %d, event=%d\n",
					RecursionCount_, nextState_, newEvent_);
#endif	// FORNOW

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setupFallbackState_
//
//@ Interface-Description
//	Handles the reversal of the state transition request.
//---------------------------------------------------------------------
//@ Implementation-Description
// 	Depending on the value of nextState_ variable set the "fallBackState_",
//  so that if the user decide to change his mind of certain key press,
//  we can backup to the previous state and continue on.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
EstTestSubScreen::setupFallbackState_(Int newEvent)
{
	CALL_TRACE("EstTestSubScreen::setupFallbackState_(Int newEvent)");

#if defined FORNOW
printf("setupFallbackState_ at line %d, nextState=%d, event = %d\n",
			 __LINE__, nextState_, newEvent);
#endif	// FORNOW

	if (nextState_ == END_EXIT_ALERT_STATE)
	{		// $[TI1]
		fallBackState_ = END_ALERT_STATE;
	}
	else if (nextState_ == END_EXIT_FAILURE_STATE)
	{		// $[TI2]
		fallBackState_ = END_FAILURE_STATE;
	}
	else if (nextState_ == END_EXIT_TEST_STATE)
	{		// $[TI3]
		fallBackState_ = DO_NEXT_STATE;
	}
	else if (nextState_ == PAUSE_REQUEST_STATE)
	{		// $[TI4]
		fallBackState_ = DO_TEST_STATE;
	}
	else if (nextState_ == EXIT_BEFORE_PROMPT_STATE)
	{		// $[TI5]
		fallBackState_ = PROMPT_STATE;
	}
	else if (nextState_ != EXIT_REQUEST_STATE &&
		nextState_ != REPEAT_STATE &&
		nextState_ != NEXT_STATE &&
		nextState_ != OVERRIDE_STATE &&
		nextState_ != START_STATE &&
		nextState_ != NO_ACT_PAUSE_REQUEST_STATE)
	{		// $[TI6]
		fallBackState_ = nextState_;
	}
	else if (nextState_ == EXIT_REQUEST_STATE  ||
		nextState_ == REPEAT_STATE  ||
		nextState_ == NEXT_STATE  ||
		nextState_ == OVERRIDE_STATE  ||
		nextState_ == START_STATE ||
		nextState_ == NO_ACT_PAUSE_REQUEST_STATE)
	{		// $[TI7]
		;
	}
	else
	{
		CLASS_ASSERTION_FAILURE();
	}
#if defined FORNOW
printf("setupFallbackState_ at line %d, fallBackState_=%d\n",
			 __LINE__, fallBackState_);
#endif	// FORNOW
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: clearHighlightedTestItem
//
//@ Interface-Description
// 
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
EstTestSubScreen::clearHighlightedTestItem(void)
{
	// Clear the selected entry highlight.
	log_.clearSelectedEntry();
								// $[TI1]
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: startSingleTest_
//
//@ Interface-Description
//	Preparation step for a single test to start.	
//---------------------------------------------------------------------
//@ Implementation-Description
//  
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
EstTestSubScreen::startSingleTest_(void)
{
	CALL_TRACE("EstTestSubScreen::startSingleTest_(void)");

#if defined FORNOW
printf("startSingleTest_ at line  %d\n", __LINE__);
#endif	// FORNOW


	// Hide the following drawables
	repeatButton_.setShow(FALSE);
	nextButton_.setShow(FALSE);
    singleTestButton_.setShow(FALSE);
	startTestButton_.setShow(FALSE);

	// Signal to clear the log cell infos.
	clearLogEntry_ = TRUE;

	// Hide the data/time, diagnostic code and status cells
	log_.updateEntryColumn(estCurrentTestIndex_, COL_1_TIME);
	log_.updateEntryColumn(estCurrentTestIndex_, COL_3_DIAG_CODE);
	log_.updateEntryColumn(estCurrentTestIndex_, COL_4_RESULT);

	// Signal to display the log cell infos.
	clearLogEntry_ = FALSE;

	// Display the overall testing status on the service status area
	estStatus_.setStatus(MiscStrs::EST_RUNNING_MSG);

	// Reset the isThisEstTestCompleted_ for next test
	isThisEstTestCompleted_ = FALSE;

	// Reset the mostSevereStatusId_ for next test
	mostSevereStatusId_ = PASSED_TEST_ID;

	// Release Adjust Panel focus
	AdjustPanel::TakeNonPersistentFocus(NULL, TRUE);

	// Set Primary prompt to "Testing....."
	testManager_.setTestPrompt(PromptArea::PA_PRIMARY,
			PromptArea::PA_LOW, PromptStrs::EST_TESTING_P);

	// Set the Advisory prompt to NULL
	testManager_.setTestPrompt(PromptArea::PA_ADVISORY,
		 PromptArea::PA_LOW, NULL_STRING_ID);

	// Clear the Secondary prompt
	testManager_.setTestPrompt(PromptArea::PA_SECONDARY,
		PromptArea::PA_LOW, NULL_STRING_ID);

	// Setup the Upper Screen Area to display the appropriated test result.
	setupUpperScreenLayout_(estCurrentTestIndex_);


	// Erase all EST results by setting it to undefined
	SmManager::DoFunction(SmTestId::FORCE_EST_UNDEFINED_ID);	

	// Enable single test mode in the BD
	SmManager::DoFunction(SmTestId::ENABLE_SINGLE_TEST_ID);	

	// Start the test.
	SmManager::DoTest(estTestId_[estCurrentTestIndex_], repeatTestRequested_);

	
	repeatTestRequested_ = FALSE;
			
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
EstTestSubScreen::SoftFault(const SoftFaultID  softFaultID,
							 const Uint32       lineNumber,
							 const char*        pFileName,
							 const char*        pPredicate)  
{
	CALL_TRACE("EstTestSubScreen::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS,
							ESTTESTSUBSCREEN,
							lineNumber, pFileName, pPredicate);
}

