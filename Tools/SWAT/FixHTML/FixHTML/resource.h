//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by FixHTML.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_FIXHTML_DIALOG              102
#define IDR_MAINFRAME                   128
#define IDC_BUTTON_START                1000
#define IDC_EDITBOX                     1001
#define IDC_BUTTON_START2               1002
#define IDC_BUTTON_STOP                 1002
#define IDC_LIST2                       1003
#define IDC_EDIT_LOG                    1005
#define IDC_EDIT_ROOT_PATH              1006
#define IDC_BUTTON_BROWSE_ROOT_PATH     1007

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1008
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
