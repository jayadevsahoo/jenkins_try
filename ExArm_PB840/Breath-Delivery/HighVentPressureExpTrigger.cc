#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: HighVentPressureExpTrigger - Triggers exhalation to prevent 
//      safety valve from cracking.
//---------------------------------------------------------------------
//@ Interface-Description
//    This class triggers exhalation once the measured inspiratory pressure
//    is greater or equal to HIGH_VENT_PRESS_LIMIT.
//    It is enabled only during volume controlled mandatory inspirations.
//    When the pressure condition calculated by this trigger is true,
//    the trigger is considered to have "fired".  This trigger is kept
//    on a list contained in the BreathTriggerMediator.
//---------------------------------------------------------------------
//@ Rationale
//    It is necessary to end inspiration, and proceed directly to exhalation 
//    when the inspiratory pressure exceeds or equals HIGH_VENT_PRESS_LIMIT.  
//    This trigger provides a means of monitoring for that condition.
//---------------------------------------------------------------------
//@ Implementation-Description
//    This class contains a boolean state variable to keep track of 
//    whether this trigger is enabled or not.  If the trigger is not 
//    enabled, it will always return a state of false.  If it is enabled,
//    and on the active list in the BreathTriggerMediator, the pressure
//    condition monitored by the trigger will be evaluated every BD cycle.
//---------------------------------------------------------------------
//@ Fault-Handling
//    n/a.
//---------------------------------------------------------------------
//@ Restrictions
//    Only one instance of this trigger may be created.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/HighVentPressureExpTrigger.ccv   25.0.4.0   19 Nov 2013 13:59:50   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 004  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 003  By:  sp    Date:  31-Oct-1996    DR Number:
//       Project:  Sigma (R8027)
//       Description:
//             Inspection rework.
//
//  Revision: 002  By:  sp    Date:  4-Mar-1996    DR Number:674
//       Project:  Sigma (R8027)
//       Description:
//             Remove #ifndef.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "HighVentPressureExpTrigger.hh"

//@ Usage-Classes
#  include "PressureSensor.hh"
 

#  include "MainSensorRefs.hh"

//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: HighVentPressureExpTrigger()  
//
//@ Interface-Description
//	Default Contstructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Initialize base class triggerId_ using BreathTrigger's constructor.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

HighVentPressureExpTrigger::HighVentPressureExpTrigger(
	void
) : BreathTrigger(Trigger::HIGH_VENT_PRESSURE_EXP)
{
  CALL_TRACE("HighVentPressureExpTrigger()");
  // $[TI1]

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~HighVentPressureExpTrigger()  
//
//@ Interface-Description
//	Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

HighVentPressureExpTrigger::~HighVentPressureExpTrigger(void)
{
  CALL_TRACE("~HighVentPressureExpTrigger()");

}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
HighVentPressureExpTrigger::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, HIGHVENTPRESSUREEXPTRIGGER,
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)



//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: triggerCondition_()
//
//@ Interface-Description
//    This method takes no parameters.  If the trigger condition has 
//    occured, the method returns true, otherwise, the method returns false.
//    If the inspiratory pressure measured exceeds or equals the pressure
//    limit, the trigger condition is met.
//---------------------------------------------------------------------
//@ Implementation-Description
//     This method checks the following condition:
//    1.    The pressure measured by the inspiratory pressure 
//          sensor goes higher or equal to HIGH_VENT_PRESS_LIMIT.
//    If the above condition is met, then this method returns true.
// $[04035]
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
const Real32 HIGH_VENT_PRESS_LIMIT = 100.0F; //cmH2O
Boolean
HighVentPressureExpTrigger::triggerCondition_(void)
{
    CALL_TRACE("triggerCondition_()");

    Boolean rtnValue = FALSE;
    if (RInspPressureSensor.getValue() >= HIGH_VENT_PRESS_LIMIT)
    {
	// $[TI2]
	rtnValue = TRUE;
    }
    // $[TI1]
    
    // TRUE: $[TI3]
    // FALSE: $[TI4]
    return (rtnValue); 
}


//=====================================================================
//
//  Private Methods...
//
//=====================================================================

