#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
// Class:  NovRamUpdateManager - Manager for the updates to NOVRAM.
//---------------------------------------------------------------------
//@ Interface-Description
//  This class is used to manage the registering and activating of updates
//  to various data items in NOVRAM.  There is a subset of the managed
//  data items on the BD CPU that must also be available to the GUI CPU.
//  This class is used to report changes to these items so that the
//  BD CPU can transmit them to the GUI, and the GUI can be notified
//  when they are received.
//---------------------------------------------------------------------
//@ Rationale
//  A central class to manage the notification of updates to certain
//  data items is needed.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ Fault-Handling
//  none
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Persistent-Objects/vcssrc/NovRamUpdateManager.ccv   25.0.4.0   19 Nov 2013 14:18:52   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By:  gdc    Date:  12-Dec-2008    SCR Number: 6068
//  Project:  840S
//  Description:
//		Removed SUN prototype code.
//
//  Revision: 002  By:  sah    Date:  01-Jan-1999    DCS Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' to a non-inlined method.
//
//  Revision: 001  By:  sah    Date:  15-Mar-1997    DCS Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//          Initial version (Integration baseline).
//
//=====================================================================

#include "NovRamUpdateManager.hh"

//@ Usage-Classes
#if defined(SIGMA_GUI_CPU)
#include "MsgQueue.hh"
#include "OsUtil.hh"
#elif defined(SIGMA_BD_CPU)
#include "NovRamXaction.hh"
#include "InterTaskMessage.hh"
#include "MsgQueue.hh"
#endif // defined(SIGMA_GUI_CPU)
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Static Member Definitions...
//
//=====================================================================

NovRamUpdateManager::RegisterInfo  NovRamUpdateManager::ArrRegisteredItems_[
					NovRamUpdateManager::NUM_UPDATE_ITEMS
									   ]
#if defined(SIGMA_GUI_CPU)
									   [
				       NovRamUpdateManager::MAX_NUM_CALLBACKS
									   ]
#endif // defined(SIGMA_GUI_CPU)
									;

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  Initialize()  [static]
//
//@ Interface-Description
//  Initialize this NOVRAM manager.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
NovRamUpdateManager::Initialize(void)
{
  CALL_TRACE("Initialize()");

#if defined(SIGMA_GUI_CPU)
  for (Uint callbackNum = 0u; callbackNum < MAX_NUM_CALLBACKS; callbackNum++)
  {
    for (Uint itemNum = 0u; itemNum < NUM_UPDATE_ITEMS; itemNum++)
    {
      ArrRegisteredItems_[itemNum][callbackNum].registeredQueue = NULL_ID;
    }
  }
#elif defined(SIGMA_BD_CPU)
  for (Uint itemNum = 0u; itemNum < NUM_UPDATE_ITEMS; itemNum++)
  {
    ArrRegisteredItems_[itemNum].hasChanged = FALSE;
  }
#endif // defined(SIGMA_GUI_CPU)
}   // $[TI1]


#if defined(SIGMA_GUI_CPU)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  RegisterForUpdates(updateItem, registerQueue, registerMsg)
//
//@ Interface-Description
//  Register for notification of updates to the "update items" of NOVRAM.
//  When an update to one of these items occurs, the message given by
//  'registerMsg' will be posted to the queue given by 'registerQueue'.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (updateItem >= 0  &&  updateItem < NUM_UPDATE_ITEMS)
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

void
NovRamUpdateManager::RegisterForUpdates(		
		    const NovRamUpdateManager::UpdateTypeId updateItem,
		    const IpcId                             registerQueue,
		    const Int32                             registerMsg	
					)
{
  CALL_TRACE("RegisterForUpdates(registerQueue, registerMsg)");
  CLASS_PRE_CONDITION((updateItem >= 0  &&  updateItem < NUM_UPDATE_ITEMS));

  for (Uint callbackNum = 0u; callbackNum < MAX_NUM_CALLBACKS; callbackNum++)
  {   // $[TI1]
    if (ArrRegisteredItems_[updateItem][callbackNum].registeredQueue ==
								 ::NULL_ID)
    {   // $[TI1.1] -- found a "free" slot...
      ArrRegisteredItems_[updateItem][callbackNum].registeredQueue =
							      registerQueue;
      ArrRegisteredItems_[updateItem][callbackNum].registeredMsg =
      								registerMsg;
      break;  // break out of loop...
    }   // $[TI1.2] -- this slot is taken, try the next one...
  }

  SAFE_AUX_CLASS_ASSERTION((callbackNum < MAX_NUM_CALLBACKS), callbackNum);
}


#endif // defined(SIGMA_GUI_CPU)


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  UpdateHappened(updateItem)  [static]
//
//@ Interface-Description
//  An update of the data item identified by 'updateItem' has occurred.
//  If a task is registered for this update, notify it using the register
//  information.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition 
//  none
//@ End-Method 
//===================================================================== 

void
NovRamUpdateManager::UpdateHappened(
			  const NovRamUpdateManager::UpdateTypeId updateItem
				   )
{
  CALL_TRACE("UpdateHappened(updateItem)");

#if defined(SIGMA_GUI_CPU)

  for (Uint callbackNum = 0u; callbackNum < MAX_NUM_CALLBACKS; callbackNum++)
  {   // $[TI1]
    //=================================================================== 
    // Response to an update on the GUI side...
    //=================================================================== 

    CLASS_PRE_CONDITION((updateItem >= 0  &&  updateItem < NUM_UPDATE_ITEMS));

    if (ArrRegisteredItems_[updateItem][callbackNum].registeredQueue != NULL_ID)
    {   // $[TI1.1]
      MsgQueue::PutMsg(
      		ArrRegisteredItems_[updateItem][callbackNum].registeredQueue,
	        ArrRegisteredItems_[updateItem][callbackNum].registeredMsg
		      );
    }   // $[TI1.2] -- no registered callback...
  }

#elif defined(SIGMA_BD_CPU)

  //=================================================================== 
  // Response to an update on the BD side...
  //=================================================================== 

  if (!ArrRegisteredItems_[updateItem].hasChanged)
  {   // $[TI2]
    ArrRegisteredItems_[updateItem].hasChanged = TRUE;

    MsgQueue  novRamXactionQ(NOVRAM_XACTION_Q);

#  if defined(SIGMA_UNIT_TEST)
    // don't want to require comm up during Unit Tests...
    if (!novRamXactionQ.isFull())
    {
#  endif // defined(SIGMA_UNIT_TEST)
    InterTaskMessage  updateMsg;

    updateMsg.msgId   = NovRamXaction::DATA_UPDATE;
    updateMsg.msgData = updateItem;

    // post the message to the NOVRAM transaction queue...
    novRamXactionQ.putMsg(*((Int32*)&updateMsg));
#  if defined(SIGMA_UNIT_TEST)
    }
#  endif // defined(SIGMA_UNIT_TEST)
  }   // $[TI3] -- this item's change has already been registered...
#endif // defined(SIGMA_BD_CPU)
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//            [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//  and 'lineNumber' are essential pieces of information.  The
//  'pFileName' and 'pPredicate' strings may be defaulted in the macro
//  to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//  none 
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

void
NovRamUpdateManager::SoftFault(const SoftFaultID  softFaultID,
			       const Uint32       lineNumber,
			       const char*        pFileName,
			       const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, PERSISTENT_OBJS,
  			  NOV_RAM_UPDATE_MANAGER, lineNumber, pFileName,
			  pPredicate);
}
