#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: GuiTestManagerTarget -  A target for using the ScrollableLog class.  Provides
// callbacks to ScrollableLog for retrieving log text retrieval and for
// communicating row selection/deselection events.
//---------------------------------------------------------------------
//@ Interface-Description
// Any class interested in using the ScrollableLog class must inherit
// from GuiTestManagerTarget.  GuiTestManagerTarget provides two callback routines,
// getLogEntryColumn() and currentLogEntryChangeHappened(), which are
// intrinsic to the operation of ScrollableLog.
//
// getLogEntryColumn() provides a method by which ScrollableLog can
// retrieve the textual contents of any entry in a log and
// currentLogEntryChangeHappened() is called by ScrollableLog whenever
// the user selects or deselects a row of the log.
//---------------------------------------------------------------------
//@ Rationale
// Needed to operate ScrollableLog correctly.
//---------------------------------------------------------------------
//@ Implementation-Description
// Both virtual callback methods are defined but do nothing.  It is
// up to the class which inherits from GuiTestManagerTarget to redefine these
// methods.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
// This class should not be instantiated directly.  This class has use only
// when inherited from.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/GuiTestManagerTarget.ccv   25.0.4.0   19 Nov 2013 14:07:54   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 001  By:  mpm    Date:  06-JUN-95    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "GuiTestManagerTarget.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processTestPrompt
//
//@ Interface-Description
// Called by ScrollableLog when it needs to retrieve the text content
// of a particular column of a log entry.  Passed the following parameters:
// If GuiTestManagerTarget desires to specify the contents of an entry column exactly
// then it sets rIsCheapText to TRUE and puts the cheap text into rString1.
// If it wants ScrollableLog to auto format the entry column then it can
// set rIsCheapText to FALSE and then set rString1 and/or rString2,
// depending on whether it wants a one or two-line auto formatted display.
// If rString1 and/or rString2 are not used then they should be set to
// NULL_STRING_ID.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
GuiTestManagerTarget::processTestPrompt(Int command)
{
        CALL_TRACE("GuiTestManagerTarget::processTestPrompt(Int command)");

        // Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processTestKeyAllowed
//
//@ Interface-Description
// Called by ScrollableLog when it needs to retrieve the text content
// of a particular column of a log entry.  Passed the following parameters:
// If GuiTestManagerTarget desires to specify the contents of an entry column exactly
// then it sets rIsCheapText to TRUE and puts the cheap text into rString1.
// If it wants ScrollableLog to auto format the entry column then it can
// set rIsCheapText to FALSE and then set rString1 and/or rString2,
// depending on whether it wants a one or two-line auto formatted display.
// If rString1 and/or rString2 are not used then they should be set to
// NULL_STRING_ID.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
GuiTestManagerTarget::processTestKeyAllowed(Int keyAllowed)
{
        CALL_TRACE("GuiTestManagerTarget::processTestKeyAllowed(Int keyAllowed)");

        // Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processTestResultStatus
//
//@ Interface-Description
// Called by ScrollableLog when it needs to retrieve the text content
// of a particular column of a log entry.  Passed the following parameters:
// If GuiTestManagerTarget desires to specify the contents of an entry column exactly
// then it sets rIsCheapText to TRUE and puts the cheap text into rString1.
// If it wants ScrollableLog to auto format the entry column then it can
// set rIsCheapText to FALSE and then set rString1 and/or rString2,
// depending on whether it wants a one or two-line auto formatted display.
// If rString1 and/or rString2 are not used then they should be set to
// NULL_STRING_ID.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
GuiTestManagerTarget::processTestResultStatus(Int resultStatus, Int testResultId)
{
        CALL_TRACE("GuiTestManagerTarget::processTestResultStatus(Int resultStatus, Int testResultId)");

        // Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processTestResultCondition
//
//@ Interface-Description
// Called by ScrollableLog when it needs to retrieve the text content
// of a particular column of a log entry.  Passed the following parameters:
// If GuiTestManagerTarget desires to specify the contents of an entry column exactly
// then it sets rIsCheapText to TRUE and puts the cheap text into rString1.
// If it wants ScrollableLog to auto format the entry column then it can
// set rIsCheapText to FALSE and then set rString1 and/or rString2,
// depending on whether it wants a one or two-line auto formatted display.
// If rString1 and/or rString2 are not used then they should be set to
// NULL_STRING_ID.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
GuiTestManagerTarget::processTestResultCondition(Int resultCondition)
{
        CALL_TRACE("GuiTestManagerTarget::processTestResultCondition(Int resultCondition)");

        // Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processTestData
//
//@ Interface-Description
// Called by ScrollableLog when it needs to retrieve the text content
// of a particular column of a log entry.  Passed the following parameters:
// If GuiTestManagerTarget desires to specify the contents of an entry column exactly
// then it sets rIsCheapText to TRUE and puts the cheap text into rString1.
// If it wants ScrollableLog to auto format the entry column then it can
// set rIsCheapText to FALSE and then set rString1 and/or rString2,
// depending on whether it wants a one or two-line auto formatted display.
// If rString1 and/or rString2 are not used then they should be set to
// NULL_STRING_ID.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
GuiTestManagerTarget::processTestData(Int dataIndex, TestResult *pResult)
{
        CALL_TRACE("GuiTestManagerTarget::processTestData(Int dataIndex, TestResult *pResult)");

        // Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GuiTestManagerTarget()  [Constructor, Protected]
//
//@ Interface-Description
// Constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

GuiTestManagerTarget::GuiTestManagerTarget(void)
{
        CALL_TRACE("GuiTestManagerTarget::GuiTestManagerTarget(void)");

        // Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~GuiTestManagerTarget()  [Destructor, Protected]
//
//@ Interface-Description
// Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

GuiTestManagerTarget::~GuiTestManagerTarget(void)
{
        CALL_TRACE("GuiTestManagerTarget::~GuiTestManagerTarget(void)");

        // Do nothing
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
GuiTestManagerTarget::SoftFault(const SoftFaultID  softFaultID,
		   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, GUITESTMANAGERTARGET,
				lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)
