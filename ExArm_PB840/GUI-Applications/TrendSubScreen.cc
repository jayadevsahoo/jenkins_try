#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: TrendSubScreen - Trend sub-screen base class
//---------------------------------------------------------------------
//@ Interface-Description 
//  The TrendSubScreen class is the base class for the two trend
//  sub-screens, TrendGraphsSubScreen and TrendTableSubScreen. It
//  provides functions common to both sub-screens and controls switching
//  between the two sub-screens.
// 
//  This class provides a public static interface RequestTrendData to
//  route trend data requests through the active TrendSubScreen so the
//  sub-screen can disable its user interface while the trend dataset
//  request is pending. The actual request is then routed through to the
//  TrendDataSetAgent and eventually on to the Trend-Database subsystem.
// 
//  This class declares four pure virtual functions that its derived
//  classes must define. The requestTrendData method is called to
//  request new trend data from the TrendDataSetAgent and Trend-Database
//  subsystems. The updateCursorPosition method is called to update the
//  position of the cursor on the display itself as a result of the
//  trend cursor position setting being changed. The disableScrolling
//  method is called to disable the user's ability to move through the
//  data with the trend cursor once a trend parameter has changed and
//  the displayed data no longer matches the displayed setting value.
//  The update method is called to update the screen (plots or table)
//  with the contents of the current trend dataset.
// 
//  This class is a GuiTimerTarget that inherits the timerEventHappened
//  method to process the touch screen release timeout to help limit
//  touch screen "drill-down". It also processes the trend screen
//  display retry timer that retries the screen update if the display is
//  busy and can't be updated immediately.
// 
//  This class is a SubScreen providing definitions for the activate and
//  deactivate pure virtual methods that are called to prepare the
//  sub-screen for display and removal.
// 
//  This class is a ButtonTarget via the SubScreen base class. It
//  inherits the buttonDownHappened and buttonUpHappened virtual
//  functions called when a button registered with this sub-screen is
//  pressed down or up.
// 
//  This class is a SettingObserver that inherits the valueUpdate method
//  to receive updates to the settings value of all trend parameters,
//  trend timescale and trend cursor position.
// 
//  This class is a PrintTarget and provides definitions for the
//  PrintTarget pure virtual methods to handle the print screen
//  requests.
// 
//  This base class also provides protected methods used by the derived
//  classes. The displayGraphs_ method switches the trend display to the
//  graph view. The displayTable_ switches to the table view of the
//  trend data. This methods are protected instead of public since the
//  only way to navigate between the two sub-screens is through the
//  navigation (GRAPHS/TABLE) button appearing on the trend sub-screens.
//  When switching sub-screens, this class will also change the
//  sub-screen pointed to by the trending tab button in the upper screen
//  select area. This way, the tab button activates the last trend
//  sub-screen selected by the operator.
// 
//---------------------------------------------------------------------
//@ Rationale 
//  This base class encapsulates methods common to all TrendSubScreens.
//---------------------------------------------------------------------
//@ Implementation-Description 
//  The TrendSubScreen class is a base class that is never directly
//  instantiated. It is instantiated through its protected constructor
//  as part of the derived class construction. The derived class passes
//  references to several of its contained Drawables so this class can
//  manage them similarly for all derived TrendSubScreens. This means
//  that every TrendSubScreen must define a set of common objects that
//  are passed to the base class constructor.
// 
//  The derived classes override several virtual functions inherited
//  and defined in this base class including valueUpdate, activate,
//  deactivate, buttonDownHappened and buttonUpHappened. The derived
//  class methods are called to provide specialized processing of these
//  events. In most cases, the derived class method will call the base
//  class to process the event as well. This allows the base class to
//  provide common processing of events for both derived
//  TrendSubScreens.
// 
//---------------------------------------------------------------------
//@ Fault-Handling
//  No special fault handling strategies apart from the usual assertions
//  and pre-conditions to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
//	none
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/TrendSubScreen.ccv   25.0.4.0   19 Nov 2013 14:08:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 035   By: mnr    Date: 07-Jan-2010    SCR Number: 6437
//  Project:  NEO
//  Description:
//      Using two different enums for Neo and Non-Neo presets.
//
//  Revision: 034   By: mnr    Date: 28-Dec-2009    SCR Number: 6437
//  Project:  NEO
//  Description:
//      Presets not based on option, SRS tracing numbers added.
//
//  Revision: 033   By: mnr    Date: 24-Nov-2009    SCR Number: 6437
//  Project:  NEO
//  Description:
//      updatePresets_() removed to resolve presets issue.
//
//  Revision: 032   By: mnr    Date: 23-Nov-2009    SCR Number: 6437
//  Project:  NEO
//  Description:
//      Conditionally using Presets for different modes.
//
//  Revision: 031   By: rpr    Date: 07-Nov-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support Leak Compensation and Work of Breathing
// 
//  Revision: 030   By: gdc     Date: 11-Aug-2008    SCR Number: 6439
//  Project:  840S
//  Description:
//      Removed touch screen "drill-down" work-around.
//
//  Revision: 003  By: rhj      Date: 16-Aug-2007    SCR Number: 6406 
//  Project:  Trend
//  Description:
//  Fixed a bug where the state of the PLOT SET, FREEZE, and 
//  PRINT/CANCEL buttons can enter a mixed-up state when 
//  dismissing and re-displaying this subscreen.
//
//  Revision: 002  By: rhj      Date:  20-Jul-2007   SCR Number: 6386
//  Project:  Trend
//  Description:
//      Fixed printing issues in SCR 6386.
//
//  Revision: 001  By: gdc      Date:  15-Mar-2007   SCR Number: 6237
//  Project:  Trend
//  Description:
//  Initial Version
//=====================================================================

#include "TrendSubScreen.hh"

#include "AcceptedContext.hh"
#include "AcceptedContextHandle.hh"
#include "AdjustedContext.hh"
#include "AdjustPanel.hh"
#include "ComPortConfigValue.hh"
#include "ContextMgr.hh"
#include "FocusButton.hh"
#include "GuiTimerId.hh"
#include "GuiTimerRegistrar.hh"
#include "MiscStrs.hh"
#include "PatientCctTypeValue.hh"
#include "SerialInterface.hh"
#include "SettingContextHandle.hh"
#include "Setting.hh"
#include "SettingId.hh"
#include "SettingsMgr.hh"
#include "SettingSubject.hh"
#include "Sound.hh"
#include "TabButton.hh"
#include "TextButton.hh"
#include "TrendDataSetAgent.hh"
#include "TrendGraphsSubScreen.hh"
#include "TrendPresetTypeValue.hh"
#include "TrendSelectValue.hh"
#include "TrendTableSubScreen.hh"
#include "UpperScreen.hh"
#include "UpperSubScreenArea.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

// Initialize static constants.

static const Int32 TREND_EVENT_SCROLL_NUM_ROWS_ = 5;
static const Int32 TREND_EVENT_SCROLL_NUM_COLUMNS_ = 2;
static const Int32 TREND_EVENT_SCROLL_ROW_HEIGHT_ = 21;
static const Boolean TREND_EVENT_SCROLL_DISPLAY_SCROLLBAR_ = TRUE;
static const Int32 TREND_EVENT_SCROLL_COL1_WIDTH_ = 20;
static const Int32 TREND_EVENT_SCROLL_COL2_WIDTH_ = 220;

// static data
static Uint PresetsInfoMemory_[sizeof(DiscreteSettingButton::ValueInfo) +
							   sizeof(StringId) * (MAX_PRESETS - 1)];

// static member data
TrendSubScreen* TrendSubScreen::PCurrentTrendSubScreen_;

DiscreteSettingButton::ValueInfo*  TrendSubScreen::PresetsTypeInfo_;



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TrendSubScreen()  [Constructor]
//
//@ Interface-Description 
//	Constructor. 
//---------------------------------------------------------------------
//@ Implementation-Description 
//  Initializes member data. Configures the presets menu data that is
//  common to all TrendSubScreen. Registers for all trend paramter
//  setting changes. Formats the event log. Sets the callback pointer
//  for the event table.
//---------------------------------------------------------------------
//@ PreCondition
//	SubScreenArea pointer parameter is not NULL.
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

TrendSubScreen::TrendSubScreen(SubScreenArea* pSubScreenArea, 
							   TextButton& screenNavButton,
							   TextButton& printButton,
							   TextField& printingText,
							   FocusButton& eventDetailButton,
							   DropDownSettingButton& presetsButton) 
:   rScreenNavButton_(  screenNavButton),
	rPrintButton_(      printButton),
	rPrintingText_(     printingText),
	rEventDetailButton_(eventDetailButton),
	rPresetsButton_(    presetsButton),
	rTrendDataSetAgent_(TrendDataSetAgent::GetTrendDataSetAgent()),
	eventDetailLog_(eventMenuItems_,
					TREND_EVENT_SCROLL_NUM_ROWS_, 
					TREND_EVENT_SCROLL_NUM_COLUMNS_, 
					TREND_EVENT_SCROLL_ROW_HEIGHT_,
					TREND_EVENT_SCROLL_DISPLAY_SCROLLBAR_),
	numEvents_(         0 ),
	SubScreen(          pSubScreenArea),
	isPresetChanging_(  FALSE),
	isEventButtonDown_( FALSE)
{
	CLASS_PRE_CONDITION(pSubScreenArea != NULL);

	// Configure Presets data
	PresetsTypeInfo_ = (DiscreteSettingButton::ValueInfo*)::PresetsInfoMemory_;

	PresetsTypeInfo_->numValues = MAX_PRESETS;

	const Setting*  pPatientCctType =
		SettingsMgr::GetSettingPtr(SettingId::PATIENT_CCT_TYPE);

	const DiscreteValue  PATIENT_CCT_TYPE_VALUE =
		pPatientCctType->getAdjustedValue();

    PresetsTypeInfo_->arrValues[TrendPresetTypeValue::TREND_PRESET_NONE] = 
		MiscStrs::TREND_PRESET_NONE_LABEL;

	if( PATIENT_CCT_TYPE_VALUE == PatientCctTypeValue::NEONATAL_CIRCUIT )
    { //[NV01207]
		PresetsTypeInfo_->arrValues[TrendPresetTypeValue::TREND_PRESET_NEO_VCV] = 
			MiscStrs::TREND_PRESET_NEO_VCV_LABEL;
		PresetsTypeInfo_->arrValues[TrendPresetTypeValue::TREND_PRESET_NEO_PCV] = 
			MiscStrs::TREND_PRESET_NEO_PCV_LABEL;
		PresetsTypeInfo_->arrValues[TrendPresetTypeValue::TREND_PRESET_NEO_BPD] = 
			MiscStrs::TREND_PRESET_NEO_BPD_LABEL;
		PresetsTypeInfo_->arrValues[TrendPresetTypeValue::TREND_PRESET_NEO_SURF] = 
			MiscStrs::TREND_PRESET_NEO_SURF_LABEL;
		PresetsTypeInfo_->arrValues[TrendPresetTypeValue::TREND_PRESET_NEO_WEANING] = 
			MiscStrs::TREND_PRESET_WEANING_LABEL;
		PresetsTypeInfo_->arrValues[TrendPresetTypeValue::TREND_PRESET_NEO_SIMV] = 
			MiscStrs::TREND_PRESET_NEO_SIMV_LABEL;
		PresetsTypeInfo_->arrValues[TrendPresetTypeValue::TREND_PRESET_NEO_SPONT] = 
			MiscStrs::TREND_PRESET_NEO_SPONT_LABEL;
	}
	else
	{ //[TR01048]
		PresetsTypeInfo_->arrValues[TrendPresetTypeValue::TREND_PRESET_WEANING] = 
			MiscStrs::TREND_PRESET_WEANING_LABEL;
		PresetsTypeInfo_->arrValues[TrendPresetTypeValue::TREND_PRESET_COPD] = 
			MiscStrs::TREND_PRESET_COPD_LABEL;
		PresetsTypeInfo_->arrValues[TrendPresetTypeValue::TREND_PRESET_ARDS] =
			MiscStrs::TREND_PRESET_ARDS_LABEL;
		PresetsTypeInfo_->arrValues[TrendPresetTypeValue::TREND_PRESET_VC_PLUS] = 
			MiscStrs::TREND_PRESET_VC_PLUS_LABEL;
		PresetsTypeInfo_->arrValues[TrendPresetTypeValue::TREND_PRESET_PAV] =
			MiscStrs::TREND_PRESET_PAV_PLUS_LABEL;
		PresetsTypeInfo_->arrValues[TrendPresetTypeValue::TREND_PRESET_BILEVEL] = 
			MiscStrs::TREND_PRESET_BILEVEL_LABEL;
		PresetsTypeInfo_->arrValues[TrendPresetTypeValue::TREND_PRESET_SIM] = 
			MiscStrs::TREND_PRESET_SIM_LABEL;
	}

	// the derived class must setValueInfo since the button is constructed
	// after this base class constructor is run 
	// presetsButton_.setValueInfo(::PresetsTypeInfo_);

	// begin monitoring these setting changes...
	attachToSubject_(SettingId::TREND_CURSOR_POSITION, Notification::VALUE_CHANGED);
	attachToSubject_(SettingId::TREND_TIME_SCALE, Notification::VALUE_CHANGED);
	attachToSubject_(SettingId::COM1_CONFIG, Notification::VALUE_CHANGED);
	attachToSubject_(SettingId::TREND_PRESET, Notification::VALUE_CHANGED);
	attachToSubject_(SettingId::TREND_SELECT_1, Notification::VALUE_CHANGED);
	attachToSubject_(SettingId::TREND_SELECT_2, Notification::VALUE_CHANGED);
	attachToSubject_(SettingId::TREND_SELECT_3, Notification::VALUE_CHANGED);
	attachToSubject_(SettingId::TREND_SELECT_4, Notification::VALUE_CHANGED);
	attachToSubject_(SettingId::TREND_SELECT_5, Notification::VALUE_CHANGED);
	attachToSubject_(SettingId::TREND_SELECT_6, Notification::VALUE_CHANGED);


	// set up the format of the event detail window
	eventDetailLog_.setColumnInfo(0, L"", TREND_EVENT_SCROLL_COL1_WIDTH_, 
								  CENTER, 0, TextFont::NORMAL, TextFont::SIX, TextFont::SIX);  

	eventDetailLog_.setColumnInfo(1, L"", TREND_EVENT_SCROLL_COL2_WIDTH_, 
								  LEFT, 0, TextFont::NORMAL, TextFont::SIX, TextFont::SIX);  

	// have focus button pass knob events to scrollable log
	rEventDetailButton_.setFocusButtonCallback(&eventDetailLog_ );

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~TrendSubScreen  [Destructor]
//
//@ Interface-Description
//	Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//	none
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

TrendSubScreen::~TrendSubScreen(void)   
{
	// do nothing
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activate() [virtual]
//
//@ Interface-Description
//	Prepares this subscreen for display.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Sets the static TrendSubScreen pointer to this sub-screen so trend
//  data requests get routed to the active trend sub-screen. Shows or
//  hides the PRINT button and informs GuiApp that this sub-screen is
//  the active PrintTarget. Configures the presets here instead of in
//  the constructor since the presetButton is constructed after
//  this class is constructed and its contsructor will undo any
//  initialization that this class does in its constructor.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void TrendSubScreen::activate(void)
{
	PCurrentTrendSubScreen_ = this;

	// Show the PRINT button if COM1 is configured for printing
	DiscreteValue com1DeviceSetting =
		AcceptedContextHandle::GetDiscreteValue(SettingId::COM1_CONFIG);
	rPrintButton_.setShow(com1DeviceSetting == ComPortConfigValue::PRINTER_VALUE);

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: deactivate() [virtual]
//
//@ Interface-Description
//	Prepares this subscreen for removal from the display.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Sets the current TrendSubScreen pointer NULL so trend database
//  requests will not be started without an active TrendSubScreen.
//  Cleans up the print process and deregisters as the active
//  PrintTarget.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void TrendSubScreen::deactivate(void)
{
	// make sure no button has the persistent (upper sub-screen) focus
	AdjustPanel::TakePersistentFocus(NULL);

	PCurrentTrendSubScreen_ = NULL;

	GuiTimerRegistrar::CancelTimer(GuiTimerId::TREND_DATA_SET_RETRY_TIMER);

	if (SerialInterface::IsPrintRequested())
	{
		cancelPrintRequest();
	}

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: RequestTrendData [static]
//
//@ Interface-Description 
//  This method is called by the TrendDataSetAgent to initiate a trend
//  database retrieval. There are no parameters. Returns the following
//  status:
// 
//  NOT_ACTIVE			The TrendSubScreen is not active (displayed)
// 
//  BUSY				The user interface is busy so the request
//                      cannot be accepted at this time. Try again
//                      later.
// 
//  REQUEST_ACCEPTED	The request was accepted.
// 
//---------------------------------------------------------------------
//@ Implementation-Description 
//  If there is an active TrendSubScreen, passes the request onto the
//  requestTrendData method.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

TrendSubScreen::Status TrendSubScreen::RequestTrendData(void)   
{
	if (!PCurrentTrendSubScreen_)
	{
		return(NOT_ACTIVE);
	}
	else 
	{
		return(PCurrentTrendSubScreen_->requestTrendData());
	}

}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: requestTrendData
//
//@ Interface-Description 
//  This method is called to initiate a trend database retrieval from
//  either the static interface RequestTrendData or from the derived
//  TrendSubScreens. There are no parameters. Returns the following
//  status:
// 
//  BUSY				The user interface is busy so the request
//                      cannot be accepted at this time. Try again
//                      later.
// 
//  REQUEST_ACCEPTED	The request was accepted.
// 
//---------------------------------------------------------------------
//@ Implementation-Description 
//  This method returns BUSY if the user interface is being used (a
//  button is down). We only start a database request when all of the
//  sub-screen's buttons are UP. The state of the user interface
//  buttons is checked using the isAnyButtonPressed_ pure virtual
//  method. The trend data request is forwarded to the active
//  TrendSubScreen via the requestTrendData_() pure virtual method.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

TrendSubScreen::Status TrendSubScreen::requestTrendData(void)   
{
	TrendSubScreen::Status status;

	if (isAnyButtonPressed_())
	{
		status = BUSY;
	}
	else
	{
		status = REQUEST_ACCEPTED;
		requestTrendData_();
	}

	return(status);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: cancelPrintRequest [virtual]
//
//@ Interface-Description
//  This PrintTarget virtual method is called to cancel a print request.
//---------------------------------------------------------------------
//@ Implementation-Description
//  If GUI serial task is in the process of printing then hide the
//  print button and the 'Printing' message, otherwise perform printDone()
//  to perform the final print events immediately.
// 
//  Inform the GUI serial interface subsystem to cancel the print.
// 
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void TrendSubScreen::cancelPrintRequest(void)
{
	if (SerialInterface::IsPrintInProgress())
	{
		rPrintButton_.setShow(FALSE);
		rPrintingText_.setShow(FALSE);
		rScreenNavButton_.setShow(TRUE);
		rPrintButton_.setTitleText(MiscStrs::PRINT_BUTTON_TITLE);
	}
	else
	{
		printDone();
	}

    SerialInterface::CancelPrint();


}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updatePrintButton
//
//@ Interface-Description
//  This PrintTarget virtual method is called by GuiApp when it receives
//  receives a PRINTER_ASSIGNMENT_EVENT indicating a printer interface
//  assignment.
//---------------------------------------------------------------------
//@ Implementation-Description 
//	Shows the PRINT button. 
//
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void TrendSubScreen::updatePrintButton(void)
{
	rPrintButton_.setShow(TRUE);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: transitionCaptureToPrint
//
//@ Interface-Description
//  This PrintTarget virtual method is called to transition the trending
//  subscreen from the capturing state to the printing state.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Change the text on the PRINT button to CANCEL so the user can
//  cancel the print operation in progress.
//
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void TrendSubScreen::transitionCaptureToPrint(void)
{
	if (SerialInterface::IsPrintRequested())
	{
		// Switch the print button to displaying 'CANCEL'.
		rPrintButton_.setShow(TRUE);
		rPrintButton_.setTitleText(MiscStrs::PRINT_BUTTON_CANCEL_PRINT_TITLE);
		rPrintButton_.setTitleColor(Colors::BLACK);
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: printDone
//
//@ Interface-Description
//  This PrintTarget virtual method is called at the end of the printing
//  process, either when printing has completed (via the
//  CAPTURE_DONE_EVENT from the serial task) or has been cancelled (via
//  the print cancel key or trend sub-screen dismissal).
//---------------------------------------------------------------------
//@ Implementation-Description
//  Show 'PRINT' on the print button.
// 
//  Show the print button if the PRINTER is configured on COM1,
//  otherwise hide it.
// 
//  Hide the "Printing" message.
// 
//  Show the screen navigation button again.
//
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void TrendSubScreen::printDone(void)
{
	DiscreteValue com1DeviceSetting =
		AcceptedContextHandle::GetDiscreteValue(SettingId::COM1_CONFIG);

	rPrintButton_.setTitleText(MiscStrs::PRINT_BUTTON_TITLE);
	rPrintButton_.setTitleColor(Colors::BLACK);
	rPrintButton_.setShow(com1DeviceSetting == ComPortConfigValue::PRINTER_VALUE);
	rPrintingText_.setShow(FALSE);
	rScreenNavButton_.setShow(TRUE);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: displayTable_ [protected]
//
//@ Implementation-Description
//  Changes the current trend view to the table view.
//---------------------------------------------------------------------
//@ Implementation-Description 
//  Gets a pointer to the TrendTableSubScreen from the
//  UpperSubScreenArea where it's instantiated. Changes the sub-screen
//  associated with the upper sub-screen "Trends" tab button to activate
//  the table sub-screen when pressed. Finally, activates the table
//  sub-screen via SubScreenArea::activateSubScreen that takes care of
//  deactivating current (graphs) sub-screen if necessary.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void TrendSubScreen::displayTable_(void)
{
	// assign the current trend sub-screen to the table
	PCurrentTrendSubScreen_ = (TrendSubScreen*)(UpperSubScreenArea::GetTrendTableSubScreen());
	CLASS_ASSERTION(PCurrentTrendSubScreen_ != NULL);

	// Fetch the Upper Other Screens tab button.
	TabButton*  pTabButton = UpperScreen::RUpperScreen.getUpperScreenSelectArea()->getTrendsTabButton();
	CLASS_ASSERTION(pTabButton != NULL);

	// associate tab button with trend table sub-screen
	pTabButton->setSubScreen(PCurrentTrendSubScreen_);

	// Activate the subscreen and set down the tab button
	getSubScreenArea()->activateSubScreen(PCurrentTrendSubScreen_, pTabButton);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: displayGraphs_ [protected]
//
//@ Interface-Description
//  Changes the current trend view to the graphs.
//---------------------------------------------------------------------
//@ Implementation-Description 
//  Gets a pointer to the TrendGraphsSubScreen from the
//  UpperSubScreenArea where it's instantiated. Changes the sub-screen
//  associated with the upper sub-screen "Trends" tab button to activate
//  the graphs sub-screen when pressed. Finally, activates the graphs
//  sub-screen via SubScreenArea::activateSubScreen that takes care of
//  deactivating current (table) sub-screen if necessary.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void TrendSubScreen::displayGraphs_(void)
{
	// assign the current trend sub-screen to the graphs
	PCurrentTrendSubScreen_ = (TrendSubScreen*)(UpperSubScreenArea::GetTrendGraphsSubScreen());
	CLASS_ASSERTION(PCurrentTrendSubScreen_ != NULL);

	// Fetch the Upper Other Screens tab button.
	TabButton*  pTabButton = UpperScreen::RUpperScreen.getUpperScreenSelectArea()->getTrendsTabButton();
	CLASS_ASSERTION(pTabButton != NULL);

	// associate tab button with trend graphs sub-screen
	pTabButton->setSubScreen(PCurrentTrendSubScreen_);

	// Activate the subscreen and set down the tab button
	getSubScreenArea()->activateSubScreen(PCurrentTrendSubScreen_, pTabButton);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: buttonDownHappened
//
//@ Interface-Description 
//  This ButtonTarget virtual method is called when a registered button
//  is pressed down. This method is overriden in the TrendSubScreen
//  derived classes and called from the derived class to process
//  button presses common to all TrendSubScreens. Specifically, this
//  method processes the Print button and Preset button callbacks.
//---------------------------------------------------------------------
//@ Implementation-Description 
//  Upon receiving a Print button callback, the method clears persistent
//  focus to dismiss any active menus and attempts to start the printing
//  process. Upon receiving the Preset button callback, this method
//  saves the current trend parameter setting values that can be
//  restored if the user cancels his change.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void TrendSubScreen::buttonDownHappened(Button *pButton, Boolean byOperatorAction)
{
	if (pButton == &rPrintButton_)
	{
        // sets the printing callback to this class.
        GuiApp::SetPrintTarget(this);

		// clear focus to dismiss any menus that may be up
		AdjustPanel::TakePersistentFocus(NULL);

		pButton->setToUp();

		// If a print request is in progress, cancel it.
		if (SerialInterface::IsPrintRequested())
		{
			cancelPrintRequest();
		}
		// else start printing unless SerialInterface indicates not to
		else if (!SerialInterface::StartPrint())
		{
			Sound::Start(Sound::SoundId::ACCEPT);
			rScreenNavButton_.setShow(FALSE);
			rPrintButton_.setShow(FALSE);
			rPrintingText_.setShow(TRUE);
		}
		// else if SerialInterface indicates a print is disallowed then
		// generate the invalid keypress sound
		else
		{
			Sound::Start(Sound::SoundId::INVALID_ENTRY);
		}
	}
	else if (pButton == &rPresetsButton_)
	{
		// save TREND_SELECT values when preset button is pressed
		AcceptedContext&  rAccContext = ContextMgr::GetAcceptedContext();

		savedSetting1_ = rAccContext.getNonBatchDiscreteValue(SettingId::TREND_SELECT_1);
		savedSetting2_ = rAccContext.getNonBatchDiscreteValue(SettingId::TREND_SELECT_2);
		savedSetting3_ = rAccContext.getNonBatchDiscreteValue(SettingId::TREND_SELECT_3);
		savedSetting4_ = rAccContext.getNonBatchDiscreteValue(SettingId::TREND_SELECT_4);
		savedSetting5_ = rAccContext.getNonBatchDiscreteValue(SettingId::TREND_SELECT_5);
		savedSetting6_ = rAccContext.getNonBatchDiscreteValue(SettingId::TREND_SELECT_6);
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: buttonUpHappened
//
//@ Interface-Description
//  This ButtonTarget virtual method is called when a registered button
//  is pressed up. This method is overriden in the TrendSubScreen
//  derived classes and called from the derived class to process
//  button presses common to all TrendSubScreens.
//---------------------------------------------------------------------
//@ Implementation-Description 
//  There are no callbacks of interest for button press up in the
//  TrendSubScreen.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void TrendSubScreen::buttonUpHappened(Button *pButton, Boolean byOperatorAction)
{
	// Do nothing.
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: valueUpdate [virtual]
//
//@ Interface-Description 
//  This SettingObserver virtual method is called when the setting
//  value of any registered setting is changed. This TrendSubScreen
//  method is overriden in the derived class and called by the derived
//  class to process settings changes common to all TrendSubScreens.
//
//  qualifierId		The context in which the change happened, either
//                  ACCEPTED or ADJUSTED.
// 
//  pSubject		The pointer to the Setting's Context subject.
// 
//---------------------------------------------------------------------
//@ Implementation-Description
//  Processes the PRESET setting change by disabling scrolling and
//  changing the trend parameter settings to pre-defined values for each
//  preset value.
// 
//  Processes any other trend parameter setting change by disabling
//  scrolling.
// 
//  Processes a change to the printer communication port by enabling or
//  disabling printing on this sub-screen.
// 
//  Scrolling is disabled by invoking the virtual function
//  disableScrolling defined in the derived TrendSubScreen.
// 
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void TrendSubScreen::valueUpdate(const Notification::ChangeQualifier qualifierId,
								 const SettingSubject* pSubject)
{
	if (!isVisible())
	{
		return;
	}

	const SettingId::SettingIdType  SETTING_ID = pSubject->getId();

	if (qualifierId == Notification::ACCEPTED)
	{
		if (SETTING_ID == SettingId::TREND_CURSOR_POSITION)
		{
			updateCursorPosition();
		}
		else if (SETTING_ID == SettingId::TREND_PRESET)
		{
			if (!isSettingChanged_)
			{
				isSettingChanged_ = TRUE;
				disableScrolling();
			}
            // Create an accepted context to change settings.
			AcceptedContext&  rAccContext = ContextMgr::GetAcceptedContext();

			DiscreteValue trendPresetSetting =
				SettingContextHandle::GetSettingValue(ContextId::ACCEPTED_CONTEXT_ID,
													  SettingId::TREND_PRESET);
		
			const Setting*  pPatientCctType =
				SettingsMgr::GetSettingPtr(SettingId::PATIENT_CCT_TYPE);

			const DiscreteValue  PATIENT_CCT_TYPE_VALUE =
				pPatientCctType->getAdjustedValue();

			isPresetChanging_ = TRUE;
		
			if ( PATIENT_CCT_TYPE_VALUE != PatientCctTypeValue::NEONATAL_CIRCUIT )
			{
				switch (trendPresetSetting)
				{
				case TrendPresetTypeValue::TREND_PRESET_NONE:
					// revert to saved values
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_1,savedSetting1_);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_2,savedSetting2_);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_3,savedSetting3_);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_4,savedSetting4_);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_5,savedSetting5_);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_6,savedSetting6_);         
					break;
				case TrendPresetTypeValue::TREND_PRESET_WEANING: //[TR01059]
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_1, 
														 TrendSelectValue::TREND_SPONTANEOUS_RAPID_SHALLOW_BREATHING_INDEX);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_2, 
														 TrendSelectValue::TREND_P100);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_3, 
														 TrendSelectValue::TREND_NEGATIVE_INSPIRATORY_FORCE);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_4, 
														 TrendSelectValue::TREND_SET_PRESSURE_SUPPORT_LEVEL);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_5, 
														 TrendSelectValue::TREND_SET_VOLUME_SUPPORT_LEVEL_VS);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_6, 
														 TrendSelectValue::TREND_STATIC_LUNG_COMPLIANCE);
					break;
				case TrendPresetTypeValue::TREND_PRESET_COPD: //[TR01060]
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_1, 
														 TrendSelectValue::TREND_DYNAMIC_RESISTANCE);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_2, 
														 TrendSelectValue::TREND_END_EXPIRATORY_FLOW);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_3, 
														 TrendSelectValue::TREND_TOTAL_RESPIRATORY_RATE);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_4, 
														 TrendSelectValue::TREND_EXHALED_SPONTANEOUS_TIDAL_VOLUME);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_5, 
														 TrendSelectValue::TREND_EXHALED_MINUTE_VOLUME);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_6, 
														 TrendSelectValue::TREND_STATIC_LUNG_COMPLIANCE);
					break;
				case TrendPresetTypeValue::TREND_PRESET_ARDS: //[TR01061]
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_1, 
														 TrendSelectValue::TREND_PLATEAU_PRESSURE);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_2, 
														 TrendSelectValue::TREND_SET_PEEP);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_3, 
														 TrendSelectValue::TREND_EXHALED_TIDAL_VOLUME);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_4, 
														 TrendSelectValue::TREND_STATIC_AIRWAY_RESISTANCE);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_5, 
														 TrendSelectValue::TREND_STATIC_LUNG_COMPLIANCE);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_6, 
														 TrendSelectValue::TREND_SET_TIDAL_VOLUME);
					break;
				case TrendPresetTypeValue::TREND_PRESET_VC_PLUS: //[TR01062]
			
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_1, 
														 TrendSelectValue::TREND_PEAK_CIRCUIT_PRESSURE);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_2, 
														 TrendSelectValue::TREND_EXHALED_TIDAL_VOLUME);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_3, 
														 TrendSelectValue::TREND_STATIC_LUNG_COMPLIANCE);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_4, 
														 TrendSelectValue::TREND_SET_TIDAL_VOLUME);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_5, 
														 TrendSelectValue::TREND_SET_INSPIRATORY_TIME);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_6, 
														 TrendSelectValue::TREND_STATIC_AIRWAY_RESISTANCE);
			
					break;
				case TrendPresetTypeValue::TREND_PRESET_PAV: //[TR01063]
			
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_1, 
														 TrendSelectValue::TREND_SET_PERCENT_SUPPORT);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_2, 
														 TrendSelectValue::TREND_PEAK_CIRCUIT_PRESSURE);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_3, 
														 TrendSelectValue::TREND_TOTAL_WORK_OF_BREATHING);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_4, 
														 TrendSelectValue::TREND_EXHALED_TIDAL_VOLUME);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_5, 
														 TrendSelectValue::TREND_PAV_COMPLIANCE);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_6, 
														 TrendSelectValue::TREND_PAV_RESISTANCE);
					break;
				case TrendPresetTypeValue::TREND_PRESET_BILEVEL: //[TR01064]
			
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_1, 
														 TrendSelectValue::TREND_STATIC_LUNG_COMPLIANCE);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_2, 
														 TrendSelectValue::TREND_SET_PEEP_HIGH);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_3, 
														 TrendSelectValue::TREND_SET_PEEP_LOW);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_4, 
														 TrendSelectValue::TREND_MEAN_CIRCUIT_PRESSURE);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_5, 
														 TrendSelectValue::TREND_INTRINSIC_PEEP);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_6, 
														 TrendSelectValue::TREND_TOTAL_RESPIRATORY_RATE);
					break;
				case TrendPresetTypeValue::TREND_PRESET_SIM: //[TR01212]
			
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_1, 
														 TrendSelectValue::TREND_PEAK_CIRCUIT_PRESSURE);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_2, 
														 TrendSelectValue::TREND_END_EXPIRATORY_PRESSURE);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_3, 
														 TrendSelectValue::TREND_DYNAMIC_COMPLIANCE);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_4, 
														 TrendSelectValue::TREND_EXHALED_TIDAL_VOLUME);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_5, 
														 TrendSelectValue::TREND_STATIC_LUNG_COMPLIANCE);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_6, 
														 TrendSelectValue::TREND_SET_INSPIRATORY_TIME);
					break;
				default:
					AUX_CLASS_ASSERTION_FAILURE((Uint32) trendPresetSetting);
					break;
				}
			}
			else {
				switch (trendPresetSetting)
				{
				case TrendPresetTypeValue::TREND_PRESET_NONE:
					// revert to saved values
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_1,savedSetting1_);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_2,savedSetting2_);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_3,savedSetting3_);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_4,savedSetting4_);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_5,savedSetting5_);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_6,savedSetting6_);         
					break;
				case TrendPresetTypeValue::TREND_PRESET_NEO_VCV: //[NV01200]
			
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_1, 
														 TrendSelectValue::TREND_PEAK_CIRCUIT_PRESSURE);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_2, 
														 TrendSelectValue::TREND_END_EXPIRATORY_PRESSURE);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_3, 
														 TrendSelectValue::TREND_SET_TIDAL_VOLUME);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_4, 
														 TrendSelectValue::TREND_MEAN_CIRCUIT_PRESSURE);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_5, 
														 TrendSelectValue::TREND_EXHALED_TIDAL_VOLUME);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_6, 
														 TrendSelectValue::TREND_OXYGEN_PERCENTAGE);
					break;
				case TrendPresetTypeValue::TREND_PRESET_NEO_PCV: //[NV01201]
			
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_1, 
														 TrendSelectValue::TREND_PEAK_CIRCUIT_PRESSURE);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_2, 
														 TrendSelectValue::TREND_EXHALED_TIDAL_VOLUME);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_3, 
														 TrendSelectValue::TREND_MEAN_CIRCUIT_PRESSURE);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_4, 
														 TrendSelectValue::TREND_SET_INSPIRATORY_TIME);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_5, 
														 TrendSelectValue::TREND_END_EXPIRATORY_PRESSURE);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_6, 
														 TrendSelectValue::TREND_OXYGEN_PERCENTAGE);
					break;
				case TrendPresetTypeValue::TREND_PRESET_NEO_BPD: //[NV01202]
			
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_1, 
														 TrendSelectValue::TREND_PEAK_CIRCUIT_PRESSURE);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_2, 
														 TrendSelectValue::TREND_DYNAMIC_COMPLIANCE);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_3, 
														 TrendSelectValue::TREND_MEAN_CIRCUIT_PRESSURE);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_4, 
														 TrendSelectValue::TREND_END_EXPIRATORY_PRESSURE);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_5, 
														 TrendSelectValue::TREND_DYNAMIC_RESISTANCE);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_6, 
														 TrendSelectValue::TREND_OXYGEN_PERCENTAGE);
					break;
				case TrendPresetTypeValue::TREND_PRESET_NEO_SURF: //[NV01203]
			
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_1, 
														 TrendSelectValue::TREND_EXHALED_TIDAL_VOLUME);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_2, 
														 TrendSelectValue::TREND_DYNAMIC_COMPLIANCE);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_3, 
														 TrendSelectValue::TREND_DYNAMIC_RESISTANCE);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_4, 
														 TrendSelectValue::TREND_MEAN_CIRCUIT_PRESSURE);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_5, 
														 TrendSelectValue::TREND_SET_INSPIRATORY_TIME);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_6, 
														 TrendSelectValue::TREND_OXYGEN_PERCENTAGE);
					break;
				case TrendPresetTypeValue::TREND_PRESET_NEO_WEANING: //[NV01204]
			
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_1, 
														 TrendSelectValue::TREND_TOTAL_RESPIRATORY_RATE);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_2, 
														 TrendSelectValue::TREND_EXHALED_TIDAL_VOLUME);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_3, 
														 TrendSelectValue::TREND_DYNAMIC_COMPLIANCE);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_4, 
														 TrendSelectValue::TREND_DYNAMIC_RESISTANCE);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_5, 
														 TrendSelectValue::TREND_SET_RESPIRATORY_RATE);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_6, 
														 TrendSelectValue::TREND_OXYGEN_PERCENTAGE);
					break;
				case TrendPresetTypeValue::TREND_PRESET_NEO_SIMV: //[NV01205]
			
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_1, 
														 TrendSelectValue::TREND_TOTAL_RESPIRATORY_RATE);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_2, 
														 TrendSelectValue::TREND_SET_RESPIRATORY_RATE);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_3, 
														 TrendSelectValue::TREND_SET_INSPIRATORY_PRESSURE);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_4, 
														 TrendSelectValue::TREND_END_EXPIRATORY_PRESSURE);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_5, 
														 TrendSelectValue::TREND_MEAN_CIRCUIT_PRESSURE);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_6, 
														 TrendSelectValue::TREND_OXYGEN_PERCENTAGE);
					break;
				case TrendPresetTypeValue::TREND_PRESET_NEO_SPONT: //[NV01206]
			
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_1, 
														 TrendSelectValue::TREND_TOTAL_RESPIRATORY_RATE);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_2, 
														 TrendSelectValue::TREND_END_EXPIRATORY_PRESSURE);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_3, 
														 TrendSelectValue::TREND_OXYGEN_PERCENTAGE);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_4, 
														 TrendSelectValue::TREND_I_E_RATIO);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_5, 
														 TrendSelectValue::TREND_PEAK_SPONTANEOUS_FLOW_RATE);
					rAccContext.setNonBatchDiscreteValue(SettingId::TREND_SELECT_6, 
														 TrendSelectValue::TREND_PERCENT_LEAK);
					break;
				default:
					AUX_CLASS_ASSERTION_FAILURE((Uint32) trendPresetSetting);
					break;
				}	// end of switch
			}

			isPresetChanging_ = FALSE;
		}
		else if ((SETTING_ID == SettingId::TREND_SELECT_1) ||
				 (SETTING_ID == SettingId::TREND_SELECT_2) ||
				 (SETTING_ID == SettingId::TREND_SELECT_3) ||
				 (SETTING_ID == SettingId::TREND_SELECT_4) ||
				 (SETTING_ID == SettingId::TREND_SELECT_5) ||
				 (SETTING_ID == SettingId::TREND_SELECT_6) ||
				 (SETTING_ID == SettingId::TREND_TIME_SCALE)
				)
		{
			if (!isPresetChanging_)
			{
				if (!isSettingChanged_)
				{
					isSettingChanged_ = TRUE;
					disableScrolling();
				}
			}
		}
		else if (SETTING_ID == SettingId::COM1_CONFIG)
		{
			// com1 device setting has changed -- if a print job is not in
			// progress then blank the PRINT button.
			// It is the responsibility of the GUI serial task to
			// subsequently send a PRINTER_ASSIGNMENT_EVENT message
			// if it is appropriate to display the PRINT button.
			// If a print job is in progress, make sure NOT to blank the
			// PRINT button -- actually a CANCEL button at this point --
			// since we want to be able to cancel the print job.

			DiscreteValue com1DeviceSetting =
				AcceptedContextHandle::GetDiscreteValue(SettingId::COM1_CONFIG);

			rPrintButton_.setShow(com1DeviceSetting == ComPortConfigValue::PRINTER_VALUE);
		}
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: timerEventHappened
//
//@ Interface-Description
//  This GuiTimerTarget virtual method is called when the touch screen
//  release timer or trend display retry timer fires.
//---------------------------------------------------------------------
//@ Implementation-Description 
// 
//  The TREND_DISPLAY_RETRY_TIMER fires to retry the screen update that
//  was deferred during the last call to virtual update() method.
// 
//---------------------------------------------------------------------
//@ PreCondition
//  The timer id must be the TREND_DISPLAY_RETRY_TIMER.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void TrendSubScreen::timerEventHappened(GuiTimerId::GuiTimerIdType timerId)
{
	if (isVisible())
	{
		switch (timerId)
		{
		case GuiTimerId::TREND_DISPLAY_RETRY_TIMER:        
			update();
			break;

		default:
			AUX_CLASS_ASSERTION_FAILURE(timerId);
			break;
		}
	}

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateEventDetailWindow_()
//
//@ Interface-Description
//  Update the event detail window based on the event data at the
//  current cursor position. 
//---------------------------------------------------------------------
//@ Implementation-Description 
//  Extracts the trend events from the current TrendDataSet in the
//  TrendDataSetAgent. Passes these event IDs to the EventMenuItems
//  class to build its contents based on those events. Formats the
//  ScrollableEventLog based on the number of events by calling its
//  setEntryInfo method.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void TrendSubScreen::updateEventDetailWindow_()
{
	const TrendDataSet& rTrendDataSet = rTrendDataSetAgent_.getTrendDataSet();
	Int32 row = rTrendDataSetAgent_.getCursorPosition();

	if (rTrendDataSet.getTrendEvents(row).isAnyEventSet())
	{

		const TrendEvents & rEvents = rTrendDataSet.getTrendEvents( row );

		numEvents_ = 0;

		// extract the event ids into the eventId array
		TrendEvents::EventId eventId[TrendEvents::TOTAL_EVENT_IDS]; 

		Int32 idx = 0;

		// extract the user events
		for (idx = TrendEvents::BEGIN_USER_EVENT_ID; 
			(idx <= TrendEvents::END_USER_EVENT_ID) && (numEvents_ < countof(eventId)); 
			idx++)
		{
			if (rEvents.isEventSet(TrendEvents::EventId(idx)))
			{
				eventId[numEvents_++] = TrendEvents::EventId(idx);
			}
		}
		// extract the auto-events
		for (idx = TrendEvents::BEGIN_AUTO_EVENT_ID; 
			(idx <= TrendEvents::END_AUTO_EVENT_ID) && (numEvents_ < countof(eventId)); 
			idx++)
		{
			if (rEvents.isEventSet(TrendEvents::EventId(idx)))
			{
				eventId[numEvents_++] = TrendEvents::EventId(idx);
			}
		}

		// build the event detail strings
		eventMenuItems_.updateEventDetailTable(eventId, numEvents_);

		// disable the knob sounds and hide the scrollbar if there's nothing to scroll
		if (numEvents_ <= TREND_EVENT_SCROLL_NUM_ROWS_)
		{
			AdjustPanel::DisableKnobRotateSound();
			eventDetailLog_.setShowScrollbar(FALSE);
		}
		else
		{
			eventDetailLog_.setShowScrollbar(TRUE);
		}

		eventDetailLog_.setEntryInfo(0, MAX_VALUE(numEvents_, TREND_EVENT_SCROLL_NUM_ROWS_));
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//        [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition   
//  none
//@ End-Method
//=====================================================================

void TrendSubScreen::SoftFault(const SoftFaultID  softFaultID,
							   const Uint32       lineNumber,
							   const char*        pFileName,
							   const char*        pPredicate)  
{
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, TRENDSUBSCREEN,
							lineNumber, pFileName, pPredicate);
}
