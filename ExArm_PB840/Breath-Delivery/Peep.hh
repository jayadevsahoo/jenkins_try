
#ifndef Peep_HH
#define Peep_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: Peep - Manages the value of Peep to be used in ventilation.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/Peep.hhv   25.0.4.0   19 Nov 2013 14:00:02   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 004  By:  syw   Date:  16-Jul-1998    DR Number: DCS 5120
//       Project:  Sigma (840)
//       Description:
//			Added peepChange_ flag which is set if the peep setting
//			differs from actualPeep_.
//
//  Revision: 003  By:  syw   Date:  09-Dec-1997    DR Number: none
//       Project:  Sigma (840)
//       Description:
//		 	BiLevel initial version.  Added updatePeep() for BiLevel.
//
//  Revision: 002  By:  iv    Date:  07-Feb-1997    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Rework per Breath Delivery formal code review.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//====================================================================

# include "Sigma.hh"
# include "Breath_Delivery.hh"

//@ Usage-Classes
# include "BreathPhaseType.hh"
//@ End-Usage


class Peep {
  public:
	enum PeepState { PEEP_LOW, PEEP_HIGH, PEEP_MAX } ;
	
    Peep(void);
    ~Peep(void);
    inline Real32 getActualPeep(void) const;
    inline void setActualPeep(const Real32 value);
    inline Boolean isPeepChange( void) const ;
    inline void setPeepChange( const Boolean value) ;
    void updatePeep(const BreathPhaseType::PhaseType phase);
	void updatePeep(const BreathPhaseType::PhaseType phase, const PeepState currPeepState,
					const PeepState newPeepState) ;

    static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL, 
			  const char*       pPredicate = NULL);
  
  protected:

  private:
    Peep(const Peep&);		// not implemented...
    void   operator=(const Peep&);	// not implemented...

    //@Data-Member:  actualPeep_
    //The actual value of Peep to be used during ventilation.
    Real32 actualPeep_;
    
	//@ Data-Member: peepChange_
	// set if the peep setting differs from actualPeep_
	Boolean peepChange_ ;

};


// Inlined methods...
#include "Peep.in"


#endif // Peep_HH 
