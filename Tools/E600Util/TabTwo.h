#pragma once
#include "ClientSocket.h"
#include "Plot.h"
#include "Resource.h"

// CTabTwo dialog

class CTabTwo : public CDialog
{
	DECLARE_DYNAMIC(CTabTwo)

public:
	CTabTwo(CWnd* pParent);   // standard constructor
	virtual ~CTabTwo();
	CClientSocket m_Client;

// Dialog Data
	enum { IDD = IDD_DIALOG_TAB2 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	BOOL OnInitDialog();

	void cacheInvalidateRgn_(const CRect& rect);
	void UpdateWaveform(int Index, float Data);
	void SetPlotScale(UINT MaxId, UINT MinId, int Index);
	void SetScale();
	void SetDataSpeed(int speed);

	int Signal1;
	int Signal2;
	int Signal3;

	BOOL bFreezed1;
	
	vector<Plot*> m_Plots;

	float MonitorData[NUM_SIGNALS];

	CRect Wave1Rect;
	CRect Wave2Rect;
	CRect Wave3Rect;
	CRect WaveRect[PLOT_COUNT];

	CRgn m_GlobalInvalidateRgn;

	afx_msg void OnPaint();
	afx_msg void OnTimer(UINT nTimerID);
	afx_msg LRESULT OnBDData(WPARAM x, LPARAM y);
	afx_msg LRESULT OnHeaderString(WPARAM x, LPARAM y);

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButtonSet1();
	afx_msg void OnBnClickedButtonSet2();
	afx_msg void OnBnClickedButtonSet3();
	afx_msg void OnBnClickedButtonPB880();
	afx_msg void OnCbnSelchangeComboWaveSelect1();
	afx_msg void OnCbnSelchangeComboWaveSelect2();
	afx_msg void OnCbnSelchangeComboWaveSelect3();
	afx_msg void OnBnClickedButtonFreeze1();
};
