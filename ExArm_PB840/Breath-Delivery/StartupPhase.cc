#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: StartupPhase - A placeholder for a breath, used during the powerup
//					sequence.
//---------------------------------------------------------------------
//@ Interface-Description
//		This class is derived from the base class BreathPhase.  This class
//		is implemented to provide the interface expected by any
// 		breath scheduler that follows the powerup scheduler.  It does not
//		implement any actual breathing.  Virtual methods are implemented to
//		initialize the StartupPhase, to perform StartupPhase activities each
//		BD cycle during StartupPhase, and to allow for internal clean up and
//		graceful termination of the breath phase.
//---------------------------------------------------------------------
//@ Rationale
// 		The use of this class during power up, simplifies the powerup
//		mechanism by simulating the same BreathScheduler-BreathPhase
//		collaboration that exist during normal breathing.    
//---------------------------------------------------------------------
//@ Implementation-Description
// 		Since this is just a placeholder, only the base class pure virtual
//		fuctions are redefined (with empty content) to allow for class
//		instanciation. 
//---------------------------------------------------------------------
//@ Fault-Handling
//		N/A
//---------------------------------------------------------------------
//@ Restrictions
//		none
//---------------------------------------------------------------------
//@ Invariants
//		none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/StartupPhase.ccv   25.0.4.0   19 Nov 2013 14:00:10   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 002 By: syw    Date: 21-Feb-1996   DR Number: DCS 674
//  	Project:  Sigma (R8027)
//		Description:
//			Removed #ifndef around #include.
//
//  Revision: 001  By:  iv    Date:  30-Nov-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//=====================================================================

#include "StartupPhase.hh"

//@ Usage-Classes

#include "Trigger.hh"

//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: StartupPhase()
//
//@ Interface-Description
//		Constructor.  This method has no arguments and returns nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The base class constructor is called with the phase type argument.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

StartupPhase::StartupPhase(void)
 : BreathPhase( BreathPhaseType::NON_BREATHING)	// $[TI1]
{
	CALL_TRACE("StartupPhase::StartupPhase(void)") ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~StartupPhase()
//
//@ Interface-Description
//		Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//		none
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

StartupPhase::~StartupPhase(void)
{
	CALL_TRACE("StartupPhase::~StartupPhase(void)") ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: relinquishControl()
//
//@ Interface-Description
//		This method has BreathTrigger& as an argument and has no return value.
//		This method is called at the beginning of the next phase to wrap up
//		start up phase in preparation for the next phase.
//---------------------------------------------------------------------
//@ Implementation-Description
//		check condition: triggerId == IMMEDIATE_BREATH
//---------------------------------------------------------------------
//@ PreCondition
//		triggerId == IMMEDIATE_BREATH
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
StartupPhase::relinquishControl( const BreathTrigger& trigger)
{
	CALL_TRACE("StartupPhase::relinquishControl( const BreathTrigger& trigger)") ;
	 	
	CLASS_PRE_CONDITION( Trigger::IMMEDIATE_BREATH == ((const Trigger&)trigger).getId() ) ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newCycle( void)
//
//@ Interface-Description
//		This method has no arguments and has no return value.  This method
//		should never be called.  If it is, a soft fault is generated.
//---------------------------------------------------------------------
//@ Implementation-Description
// 		none
//---------------------------------------------------------------------
//@ PreCondition
//		method never called
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
StartupPhase::newCycle( void)
{
	CALL_TRACE("StartupPhase::newCycle( void)") ;
	
	CLASS_PRE_CONDITION( FALSE) ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newBreath()
//
//@ Interface-Description
//		This method has no arguments and has no return value.  This method
//		is called at the beginning of the start up phase.
//---------------------------------------------------------------------
//@ Implementation-Description
// 		none
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
StartupPhase::newBreath( void)
{
	CALL_TRACE("StartupPhase::newBreath( void)") ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
StartupPhase::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
	CALL_TRACE("SoftFault( softFaultID, lineNumber, pFileName, pPredicate)") ;
  	FaultHandler::SoftFault( softFaultID, BREATH_DELIVERY, STARTUPPHASE,
    	                     lineNumber, pFileName, pPredicate) ;
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//=====================================================================
//
//  Private Methods...
//
//=====================================================================









