
#ifndef LinearSensor_HH
#define LinearSensor_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: LinearSensor - Basic sensor that only requires simple linear 
//      interpolation to determine the engineering units of the
//      measurement.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/LinearSensor.hhv   25.0.4.0   19 Nov 2013 13:54:08   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005  By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 004 By: syw    Date: 29-Sep-1997   DR Number: DCS 2410
//  	Project:  Sigma (R8027)
//		Description:
//			Added offset_ data member and set and get methods.
//
//  Revision: 003 By: syw    Date: 20-Feb-1996   DR Number: DCS 674
//  	Project:  Sigma (R8027)
//		Description:
//			Removed #ifndef around #include.
//
//  Revision: 002  By: syw    Date:  05-Dec-1995    DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Added SafetyNetSensorData * argument to constructor.
//
//	Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: INITIAL RELEASE
//      Project:  Sigma (R8027)
//      Description:
//      	Initial version
//
//=====================================================================

#include "Sigma.hh"
#include "BD_IO_Devices.hh"

//@ Usage-Classes

#include "Sensor.hh"

//@ End-Usage

class LinearSensor : public Sensor {
  public:
    LinearSensor( SafetyNetSensorData *pData,
    			  const AdcChannels::AdcChannelId AdcId,
                  const Real32 voltsMin, const Real32 voltsMax,
                  const Real32 engValueAtMin, const Real32 engValueAtMax) ;
    virtual ~LinearSensor( void) ;

    static void SoftFault( const SoftFaultID softFaultID,
						   const Uint32      lineNumber,
						   const char*       pFileName  = NULL, 
						   const char*       pPredicate = NULL) ;
                                  
	inline void setOffset( const Real32 offset) ;
	inline Real32 getOffset( void) const ;

#ifdef INTEGRATION_TEST_ENABLE
#if defined (SIGMA_BD_CPU)
        virtual void updateValue();
#endif // defined (SIGMA_BD_CPU)
#endif // INTEGRATION_TEST_ENABLE
  
  protected:

    virtual Real32 rawToEngValue_( const AdcCounts count) const ;

    //@ Data-Member:  countsMin_
    // The ADC counts at the lower conversion point
    Real32	countsMin_ ;

    //@ Data-Member:  engValueAtMin_
    // The engineering value corresponding to countsMin_
    Real32  engValueAtMin_ ;

    //@ Data-Member:  countToEngFactor_
    // The conversion factor from ADC counts to engineering value
    Real32  countToEngFactor_ ;

	//@ Data-Member: offset_
	// offset value in eng units due to miscalibration or drifting in sensor
	Real32 offset_ ;
	
  private:
  
    LinearSensor( const LinearSensor&) ;        // not implemented...
    void   operator=( const LinearSensor&) ;    // not implemented...
    LinearSensor( void) ;                       // not implemented...

} ;


// Inlined methods...
#include "LinearSensor.in"


#endif // LinearSensor_HH 
