#ifndef LungData_HH
#define LungData_HH
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: LungData - The class for central location to calculate the
//		related lung flow and volume data.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/LungData.hhv   25.0.4.0   19 Nov 2013 13:59:52   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004   By: gdc   Date:  08-Nov-2006    DR Number: 6309
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Implemented new inspiratory btps corrected lung flow method.
//
//  Revision: 003   By: syw   Date:  01-Jun-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//		PAV project-related changes:
//		Implemented new lung flow calculation.
//
//  Revision: 002  By:  syw    Date:  23-Jun-1999    DR Number: DCS 5447
//       Project:  ATC
//       Description:
//			Add DCS 5322 to header
//
//  Revision: 001  By:  yyy    Date:  07-Jan-1999    DR Number: DCS 5322
//       Project:  ATC (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"

//@ Usage-Classes

#include "Btps.hh"
#include "Tube.hh"
#include "BreathMiscRefs.hh"
#include "Resistance.hh"
#include "BreathPhase.hh"
#include "PressureXducerAutozero.hh"

class BreathTrigger ;

//@ End-Usage


class LungData
{
  public:

    LungData(void) ;
    virtual ~LungData( void) ;

	static void SoftFault(const SoftFaultID softFaultID,
							const Uint32      lineNumber,
							const char*       pFileName  = NULL, 
							const char*       pPredicate = NULL);

	//@ Type: TubeSide
	// This enum represents the type of Inspiratory pause.
  	enum TubeSide
  	{
  		INSP_SIDE,
  		EXP_SIDE
  	};
  
	inline Real32 getLungFlowLimit(void);
    inline Real32 getLungFlow(void) const;
    inline Real32 getLungPressure(void) const;
	inline Real32 getFilteredLungFlow( void) const;
	inline Real32 getPeakInspLungFlow(void);
	inline Real32 getPeakExpLungFlow(void);
    inline Real32 getLungAlpha(void) const;
    inline Real32 getInspLungVolume(void) const;
    inline Real32 getExhLungVolume(void) const;
    inline Real32 getInspLungBtpsFlow(void) const;
    inline Real32 getInspLungBtpsVolume(void) const;
    inline Real32 getExhLungBtpsVolume(void) const;
    inline void setTotalTrajLimitReached(Boolean totalTrajLimitReached);
	inline Real32 getFilteredLungFlowSlope(void);
    
    void newCycle( void);

  protected:

  private:
    LungData( const LungData&) ;    	// not implemented
    void operator=( const LungData&) ;	// not implemented

	void calculateLungFlow_(const Real32 inspFlow, const Real32 exhFlow,
			const Real32 phaseDurationMs, const BreathPhaseType::PhaseType phaseType);
	void updateInspLungAlpha_(Real32 inspTime, Real32 exhPress);
	void updateFilteredLungFlow_(void);
	Real32 computeLungPressure_( const Real32 lungFlow, const Real32 wyePressure) ;
	
	//@ Data-Member: centerPinsp_1_
	// previous center insp pressure estimate used in slope estimator
	Real32 centerPinsp_1_ ;

	//@ Data-Member: centerPinspSlope_1_
	// previous center insp pressure slope estimate used in slope estimator
	Real32 centerPinspSlope_1_ ;

	//@ Data-Member: centerPinspSlope_2_
	// 2 previous center insp pressure slope estimate used in slope estimator
	Real32 centerPinspSlope_2_ ;

	//@ Data-Member: centerPexh_1_
	// previous center exh pressure estimate used in slope estimator
	Real32 centerPexh_1_ ;

	//@ Data-Member: centerPexhSlope_1_
	// previous center exh pressure slope estimate used in slope estimator
	Real32 centerPexhSlope_1_ ;

	//@ Data-Member: centerPexhSlope_2_
	// 2 previous center exh pressure slope estimate used in slope estimator
	Real32 centerPexhSlope_2_ ;

	//@ Data-Member: peAlphaFiltered_1
	// previous filtered exh pressure estimate used in slope estimator
	Real32 peAlphaFiltered_1_ ;

	//@ Data-Member: pExhSlope_1_
	// previous filtered exh pressure slope estimate used in slope estimator
	Real32 pExhSlope_1_ ;

	//@ Data-Member: pExhSlope_2_
	// 2 previous filtered exh pressure slope estimate used in slope estimator
	Real32 pExhSlope_2_ ;

	//@ Data-Member: exhLungVolume_
	// expired lung volume
    Real32 exhLungVolume_;

	//@ Data-Member: inspLungVolume_
	// inspired lung volume.
    Real32 inspLungVolume_;

	//@ Data-Member: lungFlow_
	// lung flow.
	Real32 lungFlow_;

	//@ Data-Member: lungPressure_
	// lung pressure.
	Real32 lungPressure_;

	//@ Data-Member: filteredLungFlow_
	// filtered lung flow (alpha filter).
	Real32 filteredLungFlow_;

	//@ Data-Member: prevFilteredLungFlow_
	// previous filtered lung flow (alpha filter).
	Real32 prevFilteredLungFlow_;

	//@ Data-Member: peakInspLungFlow_
	// peak inspiratory lung flow
	Real32 peakInspLungFlow_;

	//@ Data-Member: peakExpLungFlow_
	// peak expiratory lung flow
	Real32 peakExpLungFlow_;

	//@ Data-Member: totalTrajLimitReached_
	// set to TRUE when tcv pressure target is greater than the total traj.
	// limit which is either highCircuitPressure (setting) or MAX_LUNG_TARGET.
	Boolean totalTrajLimitReached_;

	//@ Data-Member: lungAlpha_
	// lung alpha for filter the signal
	Real32 lungAlpha_;

	//@ Data-Member: exhResistance_
	// exh tubing resistance characteristics
	Resistance exhResistance_ ;

	//@ Data-Member: inspResistance_
	// insp tubing resistance characteristics
	Resistance inspResistance_ ;
	
	//@ Data-Member: piAlphaFiltered_
	// alpha filtered insp pressure
	Real32 piAlphaFiltered_ ;

	//@ Data-Member: peAlphaFiltered_
	// alpha filtered exh pressure
	Real32 peAlphaFiltered_ ;

	//@ Data-Member: lungFlowNegDuringExh_
	// set when lung flow is negative during exhalation
	Boolean lungFlowNegDuringExh_ ;

	//@ Data-Member: autozeroOccurred_
	// set if an autozero occurred
	Boolean autozeroOccurred_ ;

	//@ Data-Member:::Side autozeroSide_
	// autozero side
	PressureXducerAutozero::Side autozeroSide_ ;

} ;

// Inlined methods
#include "LungData.in"

#endif // LungData_HH 
