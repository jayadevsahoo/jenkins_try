#include "stdafx.h"

//#############################################################################
//
//  GlobalObjects.cpp - All global objects are defined here.
//
//   NOTE:  Due to the fact that Win32 critical sections can't be created in the
//          contructor of a static object, and one or more critical sections may
//          created in the threads (one is created for MailboxThread_t threads),
//          the threads must be created dynamically. See
//          WinCeApplicationApp_t::InitInstance().
//
//#############################################################################

#include "GlobalObjects.h"
#include "ExceptionHandler.h"


ExceptionHandler_t					ExceptionHandler;
