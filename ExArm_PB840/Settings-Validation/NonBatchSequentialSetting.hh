
#ifndef NonBatchSequentialSetting_HH
#define NonBatchSequentialSetting_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  NonBatchSequentialSetting - Non-Batch Sequential Setting.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/NonBatchSequentialSetting.hhv   25.0.4.0   19 Nov 2013 14:27:32   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 002   By: sah    Date:  28-May-1998    DR Number: 5079
//  Project:  COLOR
//  Description:
//	--- Merged Rev "Color" (1.4.1.0) into Rev "BiLevel" (1.4.2.0)
//	Added new constructor parameters that may be called by derived
//	class to disable bounds for this sequential setting.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Initial version.
//
//====================================================================

//@ Usage-Classes
#include "SequentialSetting.hh"
//@ End-Usage


class NonBatchSequentialSetting : public SequentialSetting 
{
  public:
    virtual ~NonBatchSequentialSetting(void);

    virtual Boolean  isChanged(void) const;

    virtual SettingValue  getDefaultValue   (void) const = 0;
    virtual SettingValue  getNewPatientValue(void) const;

    virtual SettingValue  getAdjustedValue(void) const;

    virtual void  setAdjustedValue(const SettingValue& newAdjustedValue);

    virtual void  updateToNewPatientValue(void);

    virtual void  acceptTransition(const SettingId::SettingIdType settingid,
                                   const DiscreteValue            newvalue,
                                   const DiscreteValue            currvalue);

#if defined(SIGMA_DEVELOPMENT)
    virtual Boolean  isAdjustedValid(void);
#endif // defined(SIGMA_DEVELOPMENT)

    static void   SoftFault(const SoftFaultID softFaultID,
			    const Uint32      lineNumber,
			    const char*       pFileName  = NULL, 
			    const char*       pPredicate = NULL);

  protected:
    NonBatchSequentialSetting(
		      const SettingId::SettingIdType  settingId,
		      const SettingId::SettingIdType* arrDependentSettingIds,
		      SequentialRange&  rSequentialRange,
		      const Boolean     useWrappingMode = FALSE
			     );
  private:
    // not implemented...
    NonBatchSequentialSetting(const NonBatchSequentialSetting&);
    NonBatchSequentialSetting(void);
    void operator=(const NonBatchSequentialSetting&);
};


#endif // NonBatchSequentialSetting_HH 
