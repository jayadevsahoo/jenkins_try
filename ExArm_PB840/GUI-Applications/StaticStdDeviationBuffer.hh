#ifndef StaticStdDeviationBuffer_HH
#define StaticStdDeviationBuffer_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: StaticStdDeviationBuffer - circular buffer for static standard
//		deriviation storage.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header::   /840/Baseline/GUI-Applications/vcssrc/StaticStdDeviationBuffer.hhv   10.7   08/17/07 10:09:02   pvcs  
//
//@ Modification-Log
//
//  Revision: 003  By:  sph	   Date:  28-Mar-2000    DCS Number:  5682
//  Project:  NeoMode
//  Description:
//      Changed buffer size from 20 to 250.
//
//  Revision: 002  By:  sph	   Date:  08-Feb-2000    DCS Number:  5504
//  Project:  NeoMode
//  Description:
//      Added recent-activity range to alarm bars whereby the standard deviation 
//      the last N consecutive breaths is displayed.
//
//  Revision: 001  By:  yyy	   Date:  29-Jun-1998    DCS Number: 5327
//  Project:  NeoMode
//  Description:
//		Initial version.
//
//====================================================================

#include "Sigma.hh"

//@ Usage-Classes

//@ End-Usage

const Uint32 STD_BUFFER_SIZE = 250 ;

class StaticStdDeviationBuffer {
  public:
    ~StaticStdDeviationBuffer( void) ;

    static void SoftFault( const SoftFaultID softFaultID,
				 const Uint32      lineNumber,
				 const char*       pFileName  = NULL, 
				 const char*       pPredicate = NULL) ;

	static Real32 *GetSummationBuffer( Int32 sliderIdx) ;
	static Real32 *GetDeviationBuffer( Int32 sliderIdx) ;
	static inline Uint32 GetBufferSize( void) ;
	
  protected:

  private:
    StaticStdDeviationBuffer( void) ;							// not implemented...
    StaticStdDeviationBuffer( const StaticStdDeviationBuffer&) ;// not implemented...
    void   operator=( const StaticStdDeviationBuffer&) ; 	    // not implemented...
	
} ;

#include "StaticStdDeviationBuffer.in"

#endif // StaticStdDeviationBuffer_HH 
