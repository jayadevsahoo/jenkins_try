
#ifndef TimePlotScaleSetting_HH
#define TimePlotScaleSetting_HH

//====================================================================
//
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
//@ Class:  TimePlotScaleSetting - Y Axis Scale for the Time Waveform.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/TimePlotScaleSetting.hhv   25.0.4.0   19 Nov 2013 14:27:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004   By: gdc   Date:  10-May-2007    SCR Number: 6375
//  Project:  Trend
//  Description:
//	Added support for 0 to 30 second waveform for NIF waveform.
//	
//  Revision: 003   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'getApplicability()' method
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Initial version.
//  
//====================================================================

//@ Usage-Classes
#include "NonBatchDiscreteSetting.hh"
//@ End-Usage


class TimePlotScaleSetting : public NonBatchDiscreteSetting
{
  public:
    TimePlotScaleSetting(void);
    virtual ~TimePlotScaleSetting(void);

    virtual Applicability::Id  getApplicability(
			    const Notification::ChangeQualifier qualifierId
					       ) const;

    virtual Boolean  isEnabledValue(const DiscreteValue value) const;

    virtual SettingValue  getDefaultValue(void) const;

	void set30SecondTimeScaleEnable(const Boolean);

    static void   SoftFault(const SoftFaultID softFaultID,
			    const Uint32 lineNumber,
			    const char* pFileName  = NULL, 
			    const char* pPredicate = NULL);

  private:
    TimePlotScaleSetting(const TimePlotScaleSetting&);// not implemented...
    void  operator=(const TimePlotScaleSetting&);    // not implemented...

	//@ Data-Member:  is30SecondTimeScaleEnabled_
	// TRUE when 30 second time scale setting is valid (allowed)
	Boolean is30SecondTimeScaleEnabled_;
};


#endif // TimePlotScaleSetting_HH 
