#ifndef HundredPercentO2Handler_HH
#define HundredPercentO2Handler_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: HundredPercentO2Handler - Handles control of Hundred Percent O2
// (initiated by the Hundred Percent O2 off-screen key).
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/HundredPercentO2Handler.hhv   25.0.4.0   19 Nov 2013 14:07:56   pvcs  $
//
//@ Modification-Log
//
//  Revision: 010  By:  rpr    Date:  18-Feb-2009    SCR Number: 6476
//  Project:  840S
//  Description:
//		Implemented NeoMode Update option.
//
//  Revision: 009   By: rpr    Date: 29-Jan-2009    SCR Number: 6463
//  Project:  840S
//  Description:
//      Removed GuiExceptionFlag_.  During CPU assertions cancel O2 manuever if
// 		present.
// 
//  Revision: 008   By: rpr    Date: 22-Jan-2009    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support code review comments.
// 
//  Revision: 007   By: rpr    Date: 10-Oct-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal Plus 20 O2.
// 
//  Revision: 006  By:  erm    Date:  13-Dec-2001    DCS Number: 5976
//  Project: GuiComms
//  Description:
//   Modified to use OsTimeStamp instead of TimeStamp
//
//  Revision: 005  By:  hhd	   Date:  08-Feb-2000    DCS Number: 5504
//  Project:  NeoMode
//  Descriptio:
//	Modified to provide additional off-screen key management, whereby 100% O2 can be cancelled.
//
//  Revision: 004  By:  yyy	   Date:  29-Jun-1998    DCS Number: 5327
//  Project:  NeoMode
//  Description:
//	Initial version.
//
//  Revision: 003  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 002  By:  yyy    Date:  01-Sep-1998    DR Number:
//       Project:  BiLevel (R8027)
//       Description:
//             Initial release.
//
//  Revision: 001  By:  hhd Date:  13-MAY-95	DR Number: 
//  Project:  Sigma (R8027)
//  Description:
//  	Integration baseline.
//====================================================================


#include "BdEventTarget.hh"
#include "KeyPanelTarget.hh"

//@ Usage-Classes
#include "GuiTimerTarget.hh"
#include "GuiAppClassIds.hh"
#include "OsTimeStamp.hh"
//@ End-Usage

class HundredPercentO2Handler :	public KeyPanelTarget, 
								public BdEventTarget,
								public GuiTimerTarget 
{
public:
	enum TimeBarStatus_
	{
	    ELAPSE_TIME_UNKNOWN = -1
	};
				
	HundredPercentO2Handler(void);
	~HundredPercentO2Handler(void);

	// GuiTimerTarget virtual method
	virtual void timerEventHappened(GuiTimerId::GuiTimerIdType timerId);

	// Key Panel Target virtual methods
	virtual void calibrateO2PressHappened(void);
	virtual void keyPanelPressHappened(KeyPanel::KeyId key);
	virtual void keyPanelReleaseHappened(KeyPanel::KeyId key);

	virtual void bdEventHappened(EventData::EventId evId,
				EventData::EventStatus evStatus,
				EventData::EventPrompt eventPrompt=EventData::NULL_EVENT_PROMPT);

	static void Cancel(void);

	static void ResetTimerRestartedFlag();
	
	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

	static Boolean IsCalibrateO2KeyPressed(void);

protected:

private:
	// these methods are purposely declared, but not implemented...
	HundredPercentO2Handler(const HundredPercentO2Handler&);// not implemented...
	void   operator=(const HundredPercentO2Handler&);		// not implemented...

	//@ Data-Member: isPlus20Enabled_
	// flag used to control code execution of plus 20 O2 feature
	// returns TRUE if option is enabled and neo ciruit is in use
	Boolean isPlus20Enabled_;

	//@ Data-Member: startTime_
	// OsTime stamp for the time when alarm silence is requested.
	OsTimeStamp startTime_;

	//@ Data-Member: hundredPercentO2KeyPressed_
	// flag used to indicate Off screen key for O2 suction was pressed.
	static Boolean hundredPercentO2KeyPressed_;

	//@ Data-Member: calibrateO2KeyPressed_
	// flag used to indicate on screen key for O2 calibration was pressed.
	static Boolean calibrateO2KeyPressed_;

	//@ Data-Member: pauseCanceled_
	// flag used to pause canceled occurred.
	static Boolean pauseCanceled_;

	//@ Data-Member: TimerRestartedFlag_
	// flag used to control code execution of Power failure processing case only
	// to be done once 
	static Boolean TimerRestartedFlag_;

	//@ Data-Member: TimerContFlag_
	// flag used to control code execution of Exception processing case only
	// to be done once 
	static Boolean TimerContFlag_;

};


#endif // HundredPercentO2Handler_HH
