#ifndef PatientDataTarget_HH
#define PatientDataTarget_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: PatientDataTarget - A target for changes to Patient Data
// values. Classes can specify this class as an additional parent class and
// register to be informed of changes to specific pieces of patient data.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/PatientDataTarget.hhv   25.0.4.0   19 Nov 2013 14:08:12   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

//@ Usage-Classes
#include "PatientDataId.hh"
#include "GuiAppClassIds.hh"
//@ End-Usage

class PatientDataTarget
{
public:
	// Derived classes must define this to receive patient data update notices
	virtual void patientDataChangeHappened(
						PatientDataId::PatientItemId	patientDataId);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:
	PatientDataTarget(void);
	virtual ~PatientDataTarget(void);

private:
	// these methods are purposely declared, but not implemented...
	PatientDataTarget(const PatientDataTarget&);	// not implemented...
	void operator=(const PatientDataTarget&);	// not implemented...
};


#endif // PatientDataTarget_HH
