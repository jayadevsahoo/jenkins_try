#pragma once

#include "afxwin.h"
#include "resource.h"

// CMessageBox dialog

class CMessageBox : public CDialog
{
	DECLARE_DYNAMIC(CMessageBox)

public:
	CMessageBox(CWnd* pParent = NULL);   // standard constructor
	virtual ~CMessageBox();

// Dialog Data
	enum { IDD = MESS_MESSAGE_DLG };

	CString m_strMessage;
	int m_nReturn;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	virtual BOOL OnInitDialog();
	
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButtonStopScript();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedButtonRepeat();
	afx_msg void OnBnClickedButtonContinue();
	int m_nBoxTypeRepeat;
};
