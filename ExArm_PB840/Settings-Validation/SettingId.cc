#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sotred in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class:  SettingId - Ids of all of the Settings.
//---------------------------------------------------------------------
//@ Interface-Description
//  This class encapsulates the enumerated ids for all of the settings
//  of this subsystem.  The ids are grouped into categories (e.g., Batch
//  Settings), and sub-categories (e.g., Bounded Batch Settings).  Within
//  each of the categories and sub-categories contain the ids with contiguous
//  values; that is, within a sub-category and spilling into the next 
//  sub-category/category, each of the enumerated ids contain values that
//  are in a sequential order.  It is crucial that the ids contain sequential
//  values from the first id all the way through the last id -- this property
//  is used by the "conversion" methods.
//
//  Along with the enumerated ids, this class contains a series of static
//  methods that provide querying and conversion utilities based on the
//  ids.  The querying methods provide a means of querying the validity
//  and/or category of a specified id, while a conversion method provides
//  a means of converting ids of various categories to contiguous array
//  indexing where the "lowest" id of that category returns a '0'.
//---------------------------------------------------------------------
//@ Rationale
//  This class provides a central repository for the setting ids, and the
//  routines that provide information and utilities based on those ids.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The order of the setting ID categories are intentional, in so far as,
//  the batch BD IDs MUST come first, followed by the rest of the batch IDS.
//  The ordering of the non-batch IDs is not, in and of itself, important.
//  However, one should take great care when changing the ordering of the IDs.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/SettingId.ccv   25.0.4.0   19 Nov 2013 14:27:42   pvcs  $
//
//@ Modification-Log
// 
//  Revision: 015   By: rhj    Date:  26-Jan-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//       Added Prox enabled setting
//       
//  Revision: 014   By: rhj    Date: 07-July-2008   SCR Number: 6435
//  Project:  840S
//  Description:
//       Leak Compensation project-related changes.
//       
//  Revision: 013   By: rhj   Date:  23-May-2007    SCR Number: 6237 
//  Project:  Trend
//  Description:
//      Trend related changes.
//
//  Revision: 012   By: gdc   Date:  18-Feb-2005    SCR Number: 6144 
//  Project:  NIV1
//  Description:
//      Changes to support NIV.
//
//  Revision: 011   By: erm   Date:  23-Apr-2002    DR Number: 5848
//  Project:  VCP
//  Description:
//	Added 'SHADOW_TRACE_ENABLE' id for supporting the enable/disable
//      of shadow traces.
//
//  Revision: 010   By: quf    Date: 17-Sep-2001    DR Number: 5493
//  Project:  GUIComms
//  Description:
//	Removed code relating to obsoleted print config setting.
//
//  Revision: 009  By: sah     Date:  05-May-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      *  DEVELOPMENT-ONLY code:  obsoleted apnea minute volume
//      *  DEVELOPMENT-ONLY code:  added volume support level, Vt-to-IBW
//         ratio and Vsupp-to-IBW ratio
//      *  DEVELOPMENT-ONLY code:  replaced assertions with text ouput
//
//  Revision: 008   By: hct    Date: 14-FEB-2000    DR Number: 5493
//  Project:  GUIComms
//  Description:
//	Incorporated initial specifications for GUIComms Project.
//
//  Revision: 007   By: sah    Date: 20-Jul-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//      NeoMode project-related changes:
//      *  obsoleted patient type setting
//
//  Revision: 006   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new BiLevel-specific settings
//
//  Revision: 005   By: sah   Date:  04-Mar-1999    DR Number: 5322
//  Project:  ATC
//  Description:
//	Project-related changes:
//	*  added new ATC-specific settings
//
//  Revision: 004   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 003   By: dosman Date:  16-Jan-1998    DR Number: 
//  Project:  BILEVEL
//  Description:
//	Initial BiLevel version.
//
//  Revision: 002   By: sah    Date:  22-Sep-1997    DR Number: 2410
//  Project: Sigma (R8027)
//  Description:
//	Added new Atmospheric Pressure Setting (changes to DEVELOPMENT
//	code, only).
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//=====================================================================

#include "SettingId.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  IsBoundedId(settingId)  [static]
//
//@ Interface-Description
//  Does 'settingId' represent a bounded setting?
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean
SettingId::IsBoundedId(const SettingId::SettingIdType settingId)
{
  CALL_TRACE("IsBoundedId(settingId)");

  return(SettingId::IsBatchBoundedId(settingId)   ||
	 SettingId::IsNonBatchBoundedId(settingId));
}   // $[TI1] (TRUE)  $[TI2] (FALSE)...


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  IsDiscreteId(settingId)  [static]
//
//@ Interface-Description
//  Does 'settingId' represent a discrete setting?
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean
SettingId::IsDiscreteId(const SettingId::SettingIdType settingId)
{
  CALL_TRACE("IsDiscreteId(settingId)");

  return(SettingId::IsBatchDiscreteId(settingId)  ||
	 SettingId::IsNonBatchDiscreteId(settingId));
}   // $[TI1] (TRUE)  $[TI2] (FALSE)...


//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Method:  IsSequentialId(settingId)  [static]
//
//@ Interface-Description
//  Does 'settingId' represent a sequential setting?
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean
SettingId::IsSequentialId(const SettingId::SettingIdType settingId)
{
  CALL_TRACE("IsSequentialId(settingId)");
  return(SettingId::IsBatchSequentialId(settingId)  ||
	 SettingId::IsNonBatchSequentialId(settingId));
}   // $[TI1] (TRUE)  $[TI2] (FALSE)


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  IsAlarmLimitId(alarmSettingId)  [static]
//
//@ Interface-Description
//  Does 'alarmSettingId' represent an alarm limit setting?
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean
SettingId::IsAlarmLimitId(const SettingId::SettingIdType alarmSettingId)
{
  CALL_TRACE("IsAlarmLimitId(alarmSettingId)");

  return(SettingId::IsBatchId(alarmSettingId)                 &&
  	 (alarmSettingId == SettingId::HIGH_CCT_PRESS          ||
	  alarmSettingId == SettingId::HIGH_EXH_TIDAL_VOL      ||
	  alarmSettingId == SettingId::HIGH_INSP_TIDAL_VOL     ||
	  alarmSettingId == SettingId::HIGH_EXH_MINUTE_VOL     ||
	  alarmSettingId == SettingId::HIGH_RESP_RATE          ||
	  alarmSettingId == SettingId::LOW_CCT_PRESS           ||
	  alarmSettingId == SettingId::LOW_EXH_MAND_TIDAL_VOL  ||
	  alarmSettingId == SettingId::LOW_EXH_MINUTE_VOL      ||
	  alarmSettingId == SettingId::LOW_EXH_SPONT_TIDAL_VOL));
}   // $[TI1] (TRUE)  $[TI2] (FALSE)


#if defined(SIGMA_DEVELOPMENT)

#include "Ostream.hh"

//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
// Free-Function:  GetSettingName(id)  [static]
//
// Interface-Description
//  Return a character string representing the name of the setting
//  given by 'id'.
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//  (SettingId::IsId(id))
//---------------------------------------------------------------------
// PostCondition
//  (SettingId::GetSettingName(id) != NULL)
// End-Free-Function
//=====================================================================

const char*
SettingId::GetSettingName(const SettingId::SettingIdType id)
{
  const char*  pSettingName = NULL;

  switch (id)
  {
  // bounded, batch settings (BD)...
  case SettingId::APNEA_FLOW_ACCEL_PERCENT:
    pSettingName = "Apnea Flow Acceleration Percent";
    break;
  case SettingId::APNEA_INSP_PRESS:
    pSettingName = "Apnea Inspiratory Pressure";
    break;
  case SettingId::APNEA_INSP_TIME:
    pSettingName = "Apnea Inspiratory Time";
    break;
  case SettingId::APNEA_INTERVAL:
    pSettingName = "Apnea Interval";
    break;
  case SettingId::APNEA_MIN_INSP_FLOW:
    pSettingName = "Apnea Minimim Inspiratory Flow";
    break;
  case SettingId::APNEA_O2_PERCENT:
    pSettingName = "Apnea Oxygen Percentage";
    break;
  case SettingId::APNEA_PEAK_INSP_FLOW:
    pSettingName = "Apnea Peak Inspiratory Flow";
    break;
  case SettingId::APNEA_PLATEAU_TIME:
    pSettingName = "Apnea Plateau Time";
    break;
  case SettingId::APNEA_RESP_RATE:
    pSettingName = "Apnea Respiratory Rate";
    break;
  case SettingId::APNEA_TIDAL_VOLUME:
    pSettingName = "Apnea Tidal Volume";
    break;
  case SettingId::DISCO_SENS:
    pSettingName = "Disconnection Sensitivity";
    break;
  case SettingId::EXP_SENS:
    pSettingName = "Expiratory Sensitivity";
    break;
  case SettingId::FLOW_ACCEL_PERCENT:
    pSettingName = "Flow Acceleration Percent";
    break;
  case SettingId::FLOW_SENS:
    pSettingName = "Flow Sensitivity";
    break;
  case SettingId::HIGH_CCT_PRESS:
    pSettingName = "High Circuit Pressure";
    break;
  case SettingId::HIGH_INSP_TIDAL_VOL:
    pSettingName = "High Inspired Tidal Volume";
    break;
  case SettingId::HIGH_EXH_TIDAL_VOL:
    pSettingName = "High Exhaled Tidal Volume";
    break;
  case SettingId::HL_RATIO:
    pSettingName = "H:L Ratio";
    break;
  case SettingId::HUMID_VOLUME:
    pSettingName = "Humidifier Volume";
    break;
  case SettingId::IBW:
    pSettingName = "Ideal Body Weight";
    break;
  case SettingId::INSP_PRESS:
    pSettingName = "Inspiratory Pressure";
    break;
  case SettingId::LOW_CCT_PRESS:
    pSettingName = "Low Circuit Pressure";
    break;
  case SettingId::INSP_TIME:
    pSettingName = "Inspiratory Time";
    break;
  case SettingId::MIN_INSP_FLOW:
    pSettingName = "Minimim Inspiratory Flow";
    break;
  case SettingId::OXYGEN_PERCENT:
    pSettingName = "Oxygen Percentage";
    break;
  case SettingId::PEAK_INSP_FLOW:
    pSettingName = "Peak Inspiratory Flow";
    break;
  case SettingId::PERCENT_SUPPORT:
    pSettingName = "Percent (Work) Support";
    break;
  case SettingId::PLATEAU_TIME:
    pSettingName = "Plateau Time";
    break;
  case SettingId::PEEP:
    pSettingName = "Positive End-Expiratory Pressure";
    break;
  case SettingId::PEEP_HIGH:
    pSettingName = "High Positive End-Expiratory Pressure";
    break;
  case SettingId::PEEP_HIGH_TIME:
    pSettingName = "High PEEP Time";
    break;
  case SettingId::PEEP_LOW:
    pSettingName = "Low Positive End-Expiratory Pressure";
    break;
  case SettingId::PEEP_LOW_TIME:
    pSettingName = "Low PEEP Time";
    break;
  case SettingId::PRESS_SENS:
    pSettingName = "Pressure Sensitivity";
    break;
  case SettingId::PRESS_SUPP_LEVEL:
    pSettingName = "Support Pressure";
    break;
  case SettingId::RESP_RATE:
    pSettingName = "Respiratory Rate";
    break;
  case SettingId::TIDAL_VOLUME:
    pSettingName = "Tidal Volume";
    break;
  case SettingId::TUBE_ID:
    pSettingName = "Tube Id";
    break;
  case SettingId::VOLUME_SUPPORT:
    pSettingName = "Target Support Volume";
    break;
  case SettingId::PROX_ENABLED:
	pSettingName = "Prox Enabled";
    break;
  case SettingId::LEAK_COMP_ENABLED:
    pSettingName = "Leak Comp Enabled";   
     break;
  // discrete, batch settings (BD)...
  case SettingId::APNEA_FLOW_PATTERN:
    pSettingName = "Apnea Flow Pattern";
    break;
  case SettingId::APNEA_MAND_TYPE:
    pSettingName = "Apnea Mandatory Type";
    break;
  case SettingId::FIO2_ENABLED:
    pSettingName = "FIO2 Sensor Enabled";
    break;
  case SettingId::FLOW_PATTERN:
    pSettingName = "Flow Pattern";
    break;
  case SettingId::HUMID_TYPE:
    pSettingName = "Humidification Type";
    break;
  case SettingId::MAND_TYPE:
    pSettingName = "Mandatory Type";
    break;
  case SettingId::MODE:
    pSettingName = "Mode";
    break;
  case SettingId::NOMINAL_VOLT:
    pSettingName = "Nominal Line Voltage";
    break;
  case SettingId::PATIENT_CCT_TYPE:
    pSettingName = "Patient Circuit Type";
    break;
  case SettingId::SUPPORT_TYPE:
    pSettingName = "Support Type";
    break;
  case SettingId::TRIGGER_TYPE:
    pSettingName = "Trigger Type";
    break;
  case SettingId::TUBE_TYPE:
    pSettingName = "Tube Type";
    break;
  case SettingId::VENT_TYPE:
    pSettingName = "Vent Type";
    break;

  // bounded, batch settings (non-BD)...
  case SettingId::APNEA_EXP_TIME:
    pSettingName = "Apnea Expiratory Time";
    break;
  case SettingId::APNEA_IE_RATIO:
    pSettingName = "Apnea I:E Ratio";
    break;
  case SettingId::ATM_PRESSURE:
    pSettingName = "Atmospheric Pressure";
    break;
  case SettingId::EXP_TIME:
    pSettingName = "Expiratory Time";
    break;
  case SettingId::HIGH_EXH_MINUTE_VOL:
    pSettingName = "High Exhaled Minute Volume";
    break;
  case SettingId::HIGH_RESP_RATE:
    pSettingName = "High Respiratory Rate";
    break;
  case SettingId::IE_RATIO:
    pSettingName = "I:E Ratio";
    break;
  case SettingId::LOW_EXH_MAND_TIDAL_VOL:
    pSettingName = "Low Exhaled Mandatory Tidal Volume";
    break;
  case SettingId::LOW_EXH_MINUTE_VOL:
    pSettingName = "Low Exhaled Minute Volume";
    break;
  case SettingId::LOW_EXH_SPONT_TIDAL_VOL:
    pSettingName = "Low Exhaled Spont. Tidal Volume";
    break;
  case SettingId::MINUTE_VOLUME:
    pSettingName = "Minute Volume";
    break;
  case SettingId::VSUPP_IBW_RATIO:
    pSettingName = "Vsupp-to-IBW Ratio";
    break;
  case SettingId::VT_IBW_RATIO:
    pSettingName = "Vt-to-IBW Ratio";
    break;

  // discrete, batch settings (non-BD)...
  case SettingId::APNEA_CONSTANT_PARM:
    pSettingName = "Apnea Constant-During-Rate-Change";
    break;
  case SettingId::CONSTANT_PARM:
    pSettingName = "Constant-During-Rate-Change";
    break;
  case SettingId::DCI_BAUD_RATE:
    pSettingName = "DCI Baud Rate";
    break;
  case SettingId::DCI_DATA_BITS:
    pSettingName = "DCI Data Bits";
    break;
  case SettingId::DCI_PARITY_MODE:
    pSettingName = "DCI Parity Mode";
    break;
  case SettingId::MONTH:
    pSettingName = "Month";
    break;
  case SettingId::PRESS_UNITS:
    pSettingName = "Pressure Units";
    break;
  case SettingId::LANGUAGE:
    pSettingName = "Language";
    break;
  case SettingId::SERVICE_BAUD_RATE:
    pSettingName = "Service Baud Rate";
    break;
  case SettingId::COM2_BAUD_RATE:
    pSettingName = "COM2 Baud Rate";
    break;
  case SettingId::COM2_DATA_BITS:
    pSettingName = "COM2 Data Bits";
    break;
  case SettingId::COM2_PARITY_MODE:
    pSettingName = "COM2 Parity Mode";
    break;
  case SettingId::COM3_BAUD_RATE:
    pSettingName = "COM3 Baud Rate";
    break;
  case SettingId::COM3_DATA_BITS:
    pSettingName = "COM3 Data Bits";
    break;
  case SettingId::COM3_PARITY_MODE:
    pSettingName = "COM3 Parity Mode";
    break;
  case SettingId::COM1_CONFIG : 
    pSettingName = "COM1 Configuration";
    break;
  case SettingId::COM2_CONFIG :
    pSettingName = "COM2 Configuration";
    break;
  case SettingId::COM3_CONFIG :
    pSettingName = "COM3 Configuration";
    break;
  case SettingId::DATE_FORMAT :
    pSettingName = "Date Format";
    break;

 

  // sequential, batch settings (non-BD)...
  case SettingId::DAY:
    pSettingName = "Day";
    break;
  case SettingId::DISPLAY_CONTRAST_DELTA:
    pSettingName = "Display Contrast Delta";
    break;
  case SettingId::HOUR:
    pSettingName = "Hour";
    break;
  case SettingId::MINUTE:
    pSettingName = "Minute";
    break;
  case SettingId::YEAR:
    pSettingName = "Year";
    break;

  //===================================================================
  //  non-batch settings...
  //===================================================================

  // bounded, non-batch settings...
  case SettingId::PRESS_VOL_LOOP_BASE:
    pSettingName = "P-V Loop Baseline";
    break;
  case SettingId::TREND_CURSOR_POSITION:
    pSettingName = "Trend Cursor Position";
    break;

  // discrete, non-batch settings...
  case SettingId::FLOW_PLOT_SCALE:
    pSettingName = "Flow Axis Scale";
    break;
  case SettingId::PLOT1_TYPE:
    pSettingName = "Plot 1 Waveform Type";
    break;
  case SettingId::PLOT2_TYPE:
    pSettingName = "Plot 2 Waveform Type";
    break;

       // trending
  case SettingId::TREND_SELECT_1 :
    pSettingName = "Trend 1";
    break;
  case SettingId::TREND_SELECT_2 :
    pSettingName = "Trend 2";
    break;
  case SettingId::TREND_SELECT_3 :
    pSettingName = "Trend 3";
    break;
  case SettingId::TREND_SELECT_4 :
    pSettingName = "Trend 4";
    break;
  case SettingId::TREND_SELECT_5 :
    pSettingName = "Trend 5";
    break;
  case SettingId::TREND_SELECT_6 :
    pSettingName = "Trend 6";
    break;
  case SettingId::TREND_TIME_SCALE :
    pSettingName = "Trend Time Scale";
    break;
  case SettingId::TREND_PRESET :
    pSettingName = "Trend Preset";
    break;
  case SettingId::SHADOW_TRACE_ENABLE:
    pSettingName = "Shadow Trace Enable/Disable";
    break;

  case SettingId::PRESSURE_PLOT_SCALE:
    pSettingName = "Pressure Axis Scale";
    break;
  case SettingId::TIME_PLOT_SCALE:
    pSettingName = "Time Axis Scale";
    break;
  case SettingId::VOLUME_PLOT_SCALE:
    pSettingName = "Volume Axis Scale";
    break;

  case SettingId::ALARM_VOLUME:
    pSettingName = "Alarm Volume";
    break;
  case SettingId::DISPLAY_BRIGHTNESS:
    pSettingName = "Display Brightness";
    break;
  case SettingId::DISPLAY_CONTRAST_SCALE:
    pSettingName = "Display Contrast Scale";
    break;

  default:
    cout << "SETTING NAME UNKNOWN (" << id << ")" << endl;
    break;
  };

  return(pSettingName);
}


//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
// Free-Function:  GetCategoryName(id)  [static]
//
// Interface-Description
//  Return a character string representing the category of the setting
//  given by 'id'.
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//  (SettingId::IsId(id))
//---------------------------------------------------------------------
// PostCondition
//  (SettingId::GetCategoryName(id) != NULL)
// End-Free-Function
//=====================================================================

const char*
SettingId::GetCategoryName(const SettingId::SettingIdType id)
{
  CLASS_PRE_CONDITION((SettingId::IsId(id)));

  const char*  pCategoryName = NULL;

  if (SettingId::IsBatchId(id))
  {
    pCategoryName = "Batch Setting";
  }
  else if (SettingId::IsNonBatchId(id))
  {
    pCategoryName = "Non-Batch Setting";
  }
  else
  {
    // 'id' is NOT of a legal category...
    cout << "CATEGORY NAME UNKNOWN (" << id << ")" << endl;
  }

  return(pCategoryName);
}


//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
// Free-Function:  GetValueTypeName(id)  [static]
//
// Interface-Description
//  Return a character string representing the value type (e.g. bounded
//  value) of the setting given by 'id'.
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//  (SettingId::IsId(id))
//---------------------------------------------------------------------
// PostCondition
//  (SettingId::GetValueTypeName(id) != NULL)
// End-Free-Function
//=====================================================================

const char*
SettingId::GetValueTypeName(const SettingId::SettingIdType id)
{
  CLASS_PRE_CONDITION((SettingId::IsId(id)));

  const char*  pValueTypeName = NULL;

  if (SettingId::IsBoundedId(id))
  {
    pValueTypeName = "Bounded Setting";
  }
  else if (SettingId::IsDiscreteId(id))
  {
    pValueTypeName = "Discrete Setting";
  }
  else if (SettingId::IsSequentialId(id))
  {
    pValueTypeName = "Sequential Setting";
  }
  else
  {
    // 'id' is NOT of a legal value type...
    cout << "TYPE NAME UNKNOWN (" << id << ")" << endl;
  }

  return(pValueTypeName);
}

#endif  // defined(SIGMA_DEVELOPMENT)


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
SettingId::SoftFault(const SoftFaultID  softFaultID,
		     const Uint32       lineNumber,
		     const char*        pFileName,
		     const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
  			  SETTING_ID_CLASS, lineNumber, pFileName,
			  pPredicate);
}
