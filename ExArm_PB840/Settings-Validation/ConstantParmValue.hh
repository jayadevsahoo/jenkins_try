
#ifndef ConstantParmValue_HH
#define ConstantParmValue_HH

//====================================================================
//
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  ConstantParmValue - Values of Constant Timing Parameter Setting.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/ConstantParmValue.hhv   25.0.4.0   19 Nov 2013 14:27:18   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  as part of this new functionality, the BiLevel and non-BiLevel
//	   timing settings needed separtion.  For readability purposes,
//	   aliased enumerators were added.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Initial version.
//  
//====================================================================

//@ Usage-Classes
//@ End-Usage

struct ConstantParmValue
{
  //@ Type:  ConstantParmValueId
  // All of the possible values of the Constant-During-Rate-Change Setting.
  enum ConstantParmValueId
  {
    // $[02040] -- values of the range of Apnea Constant Parm Setting...
    // $[02100] -- values of the range of Constant Parm Setting...
    INSP_TIME_CONSTANT, // this MUST be first; see 'ApneaConstantParmSetting'
    PEEP_HIGH_TIME_CONSTANT = INSP_TIME_CONSTANT,
    EXP_TIME_CONSTANT,
    PEEP_LOW_TIME_CONSTANT = EXP_TIME_CONSTANT,
    IE_RATIO_CONSTANT,
    HL_RATIO_CONSTANT = IE_RATIO_CONSTANT,
    
    TOTAL_CONSTANT_PARMS
  };
};


#endif // ConstantParmValue_HH 
