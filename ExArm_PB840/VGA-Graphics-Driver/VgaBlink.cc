#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1994-1995, Puritan-Bennett Corporation
//=====================================================================

// =========================== M O D U L E   D E S C R I P T I O N ====
//@ Class: VgaBlink - Interface class for VGA device functions.
//---------------------------------------------------------------------
//@ Interface-Description
//  This class provides a means for blinking display objects and LEDs.
//  This functionality provides application-level subsystems with two
//  different blink rates (not including no blink):  "fast" and "slow".
//  These rates can be used to bring attention to important information,
//  such as alarms.
//
//  There are two public methods for setting the blink blinkState of display
//  colors and LEDs.  For setting the blink blinkState of a color, the
//  'SetBlinkColor()' method takes the definition of an "off" and "on"
//  color, and the rate at which the color is to blink.  While for LEDs,
//  the 'SetLedStatus()' method sets the blink blinkState of an indicated
//  LED.
//
//  During subsystem initialization, the 'Initialize()' method is to be
//  called, which allows for initialization of this class's internal
//  blink table.
//
//  To provide the blinking functionality, this class defines a periodic
//  task driver, 'TaskDriver()'.
//---------------------------------------------------------------------
//@ Rationale
//  This provides a central interface, and implementation, of "blinking"
//  functionality.
//---------------------------------------------------------------------
//@ Implementation-Description
//  To provide the blinking, this class's task loop periodically toggles
//  the blinking display colors between their "on" and "off" values, and
//  the LEDs between their on and off states.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/VGA-Graphics-Driver/vcssrc/VgaBlink.ccv   25.0.4.0   19 Nov 2013 14:37:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003   By: gdc    Date:  28-Aug-2000    DCS Number: 5493
//  Project: GuiComms
//  Description:
//	Implemented single screen compatibility.
//
//  Revision: 003   By: gdc    Date:  27-Jan-2000    DCS Number: 5337
//  Project: 840 Neonatal
//  Description:
//	Optimized VGA library for Neonatal project to use BitBlt and Windows
//  Bitmap structures.
//
//  Revision: 002   By: gdc   Date: 28-Aug-1998   DCS Number:
//  Project:  Color
//  Description:
//      Increased duty cycle of low urgency blinking from 50% to 83%
//      to increase the "ON" duration of the blink.
//
//  Revision: 001   By: sah   Date: 01-Aug-1997   DCS Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//      Initial version to coding standards (Integration baseline).
//
//=====================================================================

#include "VgaBlink.hh"
#include "MemoryMap.hh"

//@ Usage-Classes
#include "Task.hh"
#include "TaskMonitor.hh"
#include "OsTimeStamp.hh"
#include "RgbQuad.hh"
#include "DisplayContext.hh"
#include "VgaGraphicsDriver.hh"
#include "AioDioDriver.h"
#if SIGMA_GUIPC_CPU
//For LED Simulation on GUI.  
#include "GuiHwSimulation.h"
#endif
//@ End-Usage


//=====================================================================
//
//  Static Declarations...
//
//=====================================================================

//@ Type:  ColorBlinkInfo_
// This structure is used to store the blink information for the colors
// of the screen.
struct ColorBlinkInfo_
{
  RgbQuad rgbOn;
  RgbQuad rgbOff;
  Uint8   blinkState;        // enum BlinkState
};

//@ Type:  LedBlinkInfo_
// This structure is used to store the blink information for the LEDs.
struct LedBlinkInfo_
{
  Uint8   blinkState;        // enum BlinkState
};

//@ Constant:  RGB_BLACK_
// Defines the RGB values for black (minimum intensity)
static const RgbQuad RGB_BLACK_ = {0,0,0};

//@ Constant:  RGB_WHITE_
// Defines the RGB values for white (maximum intensity)
static const RgbQuad RGB_WHITE_ = {255,255,255};

//=====================================================================
//
//  Static Definitions...
//
//=====================================================================

//@ Constant:  NUM_COLORS_
// Static constant that defines the maximum number of colors (currently)
// allowable.
enum { NUM_COLORS_ = 256 };


//@ Data-Variable:  ColorTable_
// Static table of info structures for tracking the state of each of the
// VGA colors.
static ColorBlinkInfo_  ColorTable_[::NUM_COLORS_];

//@ Data-Variable:  MaxBlinkColorIndex_
// Contains the maximum index in the color table containing a blinking
// color - this minimizes processing time during the blink task
static Uint MaxBlinkColorIndex_ = 0;

//@ Data-Variable:  LedTable_
// Static table of info structures for tracking the state of each of the
// LEDs ('::MAX_LED_COUNT' defined in GUI-IO-Devices/GuiIoLeds.hh).
static LedBlinkInfo_  LedTable_[::MAX_LED_COUNT];


//=====================================================================
//
//  Static Member Definitions...
//
//=====================================================================

// search for "ISO Standard Rates" for explanation of how this number
// is derived...
const Uint  VgaBlink::BLINK_PERIOD_ = 280 /*ms*/;


//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  Initialize
//
//@ Interface-Description
//  This method initializes this class's internal, static data items in
//  preparation for operation.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Initially, all display colors and LEDs are set up as non-blinking.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
VgaBlink::Initialize(void)
{

  // initialize the color table...

  Uint index = 0; 
  for (index = 0; index < ::NUM_COLORS_; index++)
  {
    VgaBlink::SetBlinkColor(index, BLINK_OFF, RGB_BLACK_, RGB_WHITE_);
  }

  // initialize the LED table...
  for (index = 0; index < ::MAX_LED_COUNT; index++)
  {
    VgaBlink::SetLedStatus(index, BLINK_OFF);
  }

  // Turn off all LEDs
  // TODO E600 currently turning off only ALARM LEDs.
	SetAlarmLedColor(ALARM_LED_COLOR_OFF);

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SetBlinkColor
//
//@ Interface-Description
//  Set the color indicated by 'index', with the blink blinkState indicated
//  by 'blinkState', and the "on" and "off" colors given.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Enter this information into this color's blink table location.
//---------------------------------------------------------------------
//@ PreCondition
//  (index < ::NUM_COLORS_)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
VgaBlink::SetBlinkColor(					
		const Uint8  index, const VgaBlink::BlinkStatus  blinkState,
		const RgbQuad & rgbOn, const RgbQuad & rgbOff )
{

  // $[TI1]
  AUX_CLASS_PRE_CONDITION((index < ::NUM_COLORS_), index);

  ColorBlinkInfo_&  rColorInfo = ::ColorTable_[index];
  MaxBlinkColorIndex_ = MAX_VALUE( MaxBlinkColorIndex_, index );

  rColorInfo.rgbOn   = rgbOn;
  rColorInfo.rgbOff  = rgbOff;
  rColorInfo.blinkState = blinkState;
  

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SetLedStatus
//
//@ Interface-Description
//  Set the blinking blinkState of the LED indicated by 'index', to the state
//  indicated by 'blinkState'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (index < ::MAX_LED_COUNT)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
VgaBlink::SetLedStatus(const Uint8 index, const Uint8 blinkState)
{

  // $[TI1]
  AUX_CLASS_PRE_CONDITION((index < ::MAX_LED_COUNT), index);

  ::LedTable_[index].blinkState = blinkState;

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetLedStatus
//
//@ Interface-Description
//  Returns the blinkState of the LED given by 'index'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (index < ::MAX_LED_COUNT)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

VgaBlink::BlinkStatus
VgaBlink::GetLedStatus(const Uint8 index)
{   
  // $[TI1]
  AUX_CLASS_PRE_CONDITION((index < ::MAX_LED_COUNT), index);

  return((VgaBlink::BlinkStatus)::LedTable_[index].blinkState);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  TaskDriver
//
//@ Interface-Description
//  Provide a task driver loop for this class to control the blinking
//  of display colors and LEDs.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This task is a inifinite loop which will do its blink processing
//  Then 'sleep' until the next blink
//
//  NOTE:  'tic' is stored in a 32-bit integer, and incremented indefinitely.
//         So long as the ventilator is not run continuously (without any
//         power cycles) for 34 consecutive years, 'tic' will not overflow.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
VgaBlink::TaskDriver(void)
{


  OsTimeStamp  startingTime;

  Uint     index;
  Boolean  fastOn  = TRUE;  // initial state is on
  Boolean  slowOn  = TRUE;  // initial state is on
  Uint32   tic     = 0u;    // state machine current state
  
  DisplayContext & rUpperDisplay = VgaGraphicsDriver::GetUpperDisplayContext();
  DisplayContext & rLowerDisplay = VgaGraphicsDriver::GetLowerDisplayContext();

  for( ;; )  // infinite loop...
  {
    // start the timer...
    startingTime.now();

    // let the task monitor know that we're alive, and well...
    TaskMonitor::Report();

    for (index = 0; index <= ::MaxBlinkColorIndex_; index++)
    {
      const ColorBlinkInfo_&  R_INFO = ::ColorTable_[index];

      switch (R_INFO.blinkState)
      {
      case BLINK_FAST :                                // $[TI1.1.1]
        if (fastOn) 
        {   // $[TI1.1.1.1]
		  rLowerDisplay.storeColor(index, R_INFO.rgbOn, FALSE);
		  rUpperDisplay.storeColor(index, R_INFO.rgbOn, FALSE);
        }
        else
        {   // $[TI1.1.1.2]
		  rLowerDisplay.storeColor(index, R_INFO.rgbOff, FALSE);
		  rUpperDisplay.storeColor(index, R_INFO.rgbOff, FALSE);
        }
        break;

      case BLINK_SLOW :                                // $[TI1.1.2]
        if (slowOn)
        {   // $[TI1.1.2.1]
		  rLowerDisplay.storeColor(index, R_INFO.rgbOn, FALSE);
		  rUpperDisplay.storeColor(index, R_INFO.rgbOn, FALSE);
        }
        else
        {   // $[TI1.1.2.2]
		  rLowerDisplay.storeColor(index, R_INFO.rgbOff, FALSE);
		  rUpperDisplay.storeColor(index, R_INFO.rgbOff, FALSE);
        }

      default :                                        // $[TI1.1.3]
        // do nothing...
        break;
      };
    }

    // now handle the flashing LEDS
	//TODO E600 code currently supports only Alarm Leds. 
	AlarmLedColors    CurrentLedColor;
	AlarmLedColors    LedColorOff;

    for (index = REDLEDSTRIP; index < ::MAX_LED_COUNT; index++)
    {
        switch (GuiLeds(index))
        {
            case REDLEDSTRIP:												//HIGH_ALARM_URGENCY
                CurrentLedColor = ALARM_LED_COLOR_RED;
				LedColorOff = ALARM_LED_COLOR_RED_OFF;
                break;
            case YELLOWLEDSTRIP:											//MEDIUM_ALARM_URGENCY, LOW_ALARM_URGENCY
                CurrentLedColor = ALARM_LED_COLOR_YELLOW;
				LedColorOff = ALARM_LED_COLOR_YELLOW_OFF;
                break;
			//TODO E600 add support to more Leds here
			default :                                        // $[TI1.2.4]
				CurrentLedColor = ALARM_LED_COLOR_OFF;
				break;
        }

      switch (::LedTable_[index].blinkState)
      {
      case BLINK_ON :                                // $[TI1.2.1]
        SetAlarmLedColor(CurrentLedColor); 
        break;
      case BLINK_FAST :                                // $[TI1.2.2]
        if (fastOn) 
        {   // $[TI1.2.2.1]
           SetAlarmLedColor(CurrentLedColor); 
        }   // $[TI1.2.2.2]
		else
		{
		   SetAlarmLedColor(LedColorOff);
		}
        break;
      case BLINK_SLOW :                                // $[TI1.2.3]
        if (slowOn) 
        {   // $[TI1.2.3.1]
           SetAlarmLedColor(CurrentLedColor); 
        }   // $[TI1.2.3.2]
		else
		{
			SetAlarmLedColor(LedColorOff);
		}
        break;
      case BLINK_OFF:                                // $[TI1.2.3]
			switch (GuiLeds(index))
			{
				case REDLEDSTRIP:
					SetAlarmLedColor(LedColorOff);
					break;
				case YELLOWLEDSTRIP:
					SetAlarmLedColor(LedColorOff);
					break;
				//TODO E600 add support to more Leds here
				default :                                        // $[TI1.2.4]
					// do nothing...
					break;
			}
		break;
      default :                                        // $[TI1.2.4]
        // do nothing...
        break;
      };
    }


    //-----------------------------------------------------------
    //  ISO Standard Rates (and requirements $[05005] & $[05008]):
    //
    //    For Medium Alarms ("slow" blink) --  0.4 Hz to 0.8 Hz
    //    Which means a target rate of 0.6 Hz (1.67 seconds).
	//    Medium urgency duty cycle is 80% on - 20% off.
    //
    //    For High Alarms ("fast" blink)   --  1.4 Hz to 2.8 Hz
    //    Which means a target rate of 2.1 Hz (0.476 seconds).
    //    To make things easier, however, we will use a rate that
    //    is three times the "slow" rate (1.8 Hz -- 0.556 seconds).
	//    High urgency duty cycle is 50% on - 50% off.
    //
    //-----------------------------------------------------------

    ++tic;  // increment tick...

    fastOn = !fastOn;      // toggle fast blink flag

	if (slowOn && (tic % 5) == 0)
	{
	  // $[TI1.3]
	  slowOn = FALSE;
	}
	else if (!slowOn)
	{
	  // $[TI1.4]
	  slowOn = TRUE;
	}

    const Uint32  ELAPSED_TIME = startingTime.getPassedTime();

    if (ELAPSED_TIME < VgaBlink::BLINK_PERIOD_)
    {   // $[TI1.5]
      // delay the amount of time that remains in the
      // period...
      Task::Delay(0, (VgaBlink::BLINK_PERIOD_ - ELAPSED_TIME));
    }   // $[TI1.6]
  }
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SetAlarmLedColor
//
//@ Interface-Description
//  Set the LED colour indicated by LedColor
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
VgaBlink::SetAlarmLedColor(AlarmLedColors LedColor)
{

	#ifdef SIGMA_GUIPC_CPU
	//For LED Simulation on GUI.  
	GuiHwSimulationView::SetAlarmLedColor(LedColor);
	#else
    switch (LedColor)
    {
        case ALARM_LED_COLOR_RED:
          AioDioDriver_t::GetAioDioDriverHandle().GetIOExpanderXRA1201Handle().SetRedLed(ALARM_LED_ON);
            break;

        case ALARM_LED_COLOR_YELLOW:
          AioDioDriver_t::GetAioDioDriverHandle().GetIOExpanderXRA1201Handle().SetAmberLed(ALARM_LED_ON);
            break;

		case ALARM_LED_COLOR_RED_OFF:
          AioDioDriver_t::GetAioDioDriverHandle().GetIOExpanderXRA1201Handle().SetRedLed(ALARM_LED_OFF);
            break;

		case ALARM_LED_COLOR_YELLOW_OFF:
          AioDioDriver_t::GetAioDioDriverHandle().GetIOExpanderXRA1201Handle().SetAmberLed(ALARM_LED_OFF);
            break;

		case ALARM_LED_COLOR_OFF:
          AioDioDriver_t::GetAioDioDriverHandle().GetIOExpanderXRA1201Handle().SetAmberLed(ALARM_LED_OFF);
		  AioDioDriver_t::GetAioDioDriverHandle().GetIOExpanderXRA1201Handle().SetRedLed(ALARM_LED_OFF);
            break;
    }
	#endif

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
VgaBlink::SoftFault(const SoftFaultID softFaultID,
		    const Uint32      lineNumber,
		    const char*       pFileName,
		    const char*       pPredicate)
{
  FaultHandler::SoftFault(softFaultID, VGA_GRAPHICS_DRIVERS,
			  VgaGraphicsDriver::VGA_BLINK_ID, 
			  lineNumber, pFileName, pPredicate);
}

#if defined(SIGMA_UNIT_TEST)
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetBlinkColor
//
//@ Interface-Description
//  Returns the BlinkStatus for the color indicated by 'index'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (index < ::NUM_COLORS_)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

VgaBlink::BlinkStatus
VgaBlink::GetBlinkStatus( const Uint8  index )
{
  AUX_CLASS_PRE_CONDITION((index < ::NUM_COLORS_), index);

  ColorBlinkInfo_&  rColorInfo = ::ColorTable_[index];
  return VgaBlink::BlinkStatus(rColorInfo.blinkState);
}

#endif // defined(SIGMA_UNIT_TEST)
