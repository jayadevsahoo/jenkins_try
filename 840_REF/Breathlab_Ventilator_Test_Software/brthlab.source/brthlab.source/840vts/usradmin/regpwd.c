/*****************************************************************************/
/*   Module Name:   REGPWD.c                                               
/*   Purpose:     High level API for managing passwords stored in the registry                                                 
/*                                                                           
/*   Requirements:   This module implements requirements detailed in         
/*          <75359-93>                                                       
/*
/*   Modification History:
/*   
/*   Description of Change			        By			      Date 
/*   ---------------------					-----   		  ------
/*	 Critical Observation during Phase1 
/* 	 is incorportated.						Kathirvel(CGS)    27 Sep,2000
/*
/*   $Revision::   1.1      $                                                
/*                                                                          
/*****************************************************************************/

/*****************************************************************************
THIS IS A PROPRIETARY WORK TO WHICH NELLCOR PURITAN BENNETT CORPORATION CLAIMS
EXCLUSIVE RIGHT.  NO PART OF THIS WORK MAY BE USED, DISCLOSED, REPRODUCED,
STORED IN AN INFORMATION RETIEVAL SYSTEM, OR TRANSMITTED, IN ANY FORM OR BY ANY
MEANS ELECTRONIC, MECHANICAL, PHOTOCOPYING, RECORDING OR OTHERWISE WITHOUT THE
PRIOR WRITTEN PERMISSION OF NELLCOR PURITAN BENNETT CORPORATION.
 
Copyright (c) 1999, Mallinckrodt Nellcor Puritan Bennett
All rights reserved.
*******************************************************************************/

/***************************************************************************/
/***	REGPWD.C	********************************************************/
/***************************************************************************/
/***	PURPOSE:														 ***/
/***	High level API for managing passwords stored in the registry	 ***/
/***																	 ***/
/***************************************************************************/
/***	HISTORY															 ***/
/***  	2-8-99  John E (PHD)	Create. 								 ***/
/***																	 ***/
/***************************************************************************/

/************/
/* INCLUDES */
/************/
#include <windows.h>
#include <ansi_c.h>
#include <stdio.h>

#include "registry.h"
#include "regpwd.h"

char NameListDelimiter[] = "|";

/*****************************************************************************/
/* Name:     CurrUserCount	                                                     
/*                                                                           
/* Purpose:  Returns the current number of users stored in the registry
/*                                          
/*                                                                           
/* Example Call:   int CurrUserCount(void);                                    
/*                                                                           
/* Input:    <none>          
/*                                                                           
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   Current User count from registry	                                                        
/*                                                                           
/*****************************************************************************/
int CurrUserCount(void)
{
	char* UserList;
	int count;

	UserList = GetUserList();
	count = 0;
	if(UserList != NULL){
		if(strtok(UserList, NameListDelimiter)){
			count++;
			while(strtok(NULL, NameListDelimiter)){
				count++;
			}
		}
		free(UserList);
	}

	return count;
}


/*****************************************************************************/
/* Name:     AddUserToRegList	                                                      
/*                                                                           
/* Purpose:  Adds a user name to the registry list
/*                                          
/*                                                                           
/* Example Call:  int AddUserToRegList(char* UserName);                                    
/*                                                                           
/* Input:    UserName - name of user to add to registry        
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   1  If User was added   									 ***/
/*		     0  If User not added   									 ***/
/*	        -1  If Unexpected error occured 	                                                        
/*                                                                           
/*****************************************************************************/
int AddUserToRegList(char* UserName)
{
	HKEY  CPVTkey;
	char  buffer[(MAX_USERNAME_LEN + 1) * MAX_USERS];
	DWORD retlen;
	DWORD disposition;
	long  error;
	int   retval;

	buffer[0] = '\0';
	retlen = (MAX_USERNAME_LEN + 1) * MAX_USERS;

	error = RegOpenKeyEx(PWD_HKEY, PWD_SUBKEY, RESERVED_ZERO, KEY_ALL_ACCESS, &CPVTkey);
	if(error == ERROR_FILE_NOT_FOUND){
		error = RegCreateKeyEx(PWD_HKEY, PWD_SUBKEY, RESERVED_ZERO, "KEY", REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &CPVTkey, &disposition);
		if(error != ERROR_SUCCESS){
			return -1;
		}
	}		

	error = RegQueryValueEx(CPVTkey, PWD_NAMELIST, RESERVED_NULL, NULL, (LPBYTE)buffer, &retlen);
	if(error != ERROR_SUCCESS){
		retlen = 1;
	}
	buffer[retlen -1] = '\0';  // just to make sure it's terminated.

	strcat(buffer, UserName);
	strcat(buffer, NameListDelimiter);

	error = RegSetValueEx(CPVTkey, PWD_NAMELIST, RESERVED_ZERO, REG_SZ, (LPBYTE)buffer, strlen(buffer)+1);
	if(error != ERROR_SUCCESS){
		retval = PWD_SET_ERROR;
	}
	else{
		retval = PWD_OK;
	}

	RegCloseKey(CPVTkey);

	return retval;
}


/*****************************************************************************/
/* Name:     UserInRegList	                                                     
/*                                                                           
/* Purpose:  Sees if a user name is in the registry list
/*                                          
/*                                                                           
/* Example Call:  int UserInRegList(char* UserName);                                    
/*                                                                           
/* Input:    UserName - User name to look up          
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:  1   If User was found   									 ***/
/*		0   If User not found   									 ***/
/*		-1  If Unexpected error occured                                                        
/*                                                                           
/*****************************************************************************/
int UserInRegList(char* UserName)
{
	char* Name;
	char* Userlist;
	int retval;

	retval = PWD_NOT_FOUND;
	Userlist = GetUserList();

	if(Userlist != NULL){
		Name = strtok(Userlist, NameListDelimiter);
		do{
			if(Name != NULL){
				if(strcmp(Name, UserName) == 0){
					retval = PWD_OK;
					break;
				}
			}
		} while(Name = strtok(NULL, NameListDelimiter));

		free(Userlist);
	}

	return retval;
}


/*****************************************************************************/
/* Name:    GetPasswdFromReg	                                                   
/*                                                                           
/* Purpose:  Retrieves a users password from the registry
/*                                          
/*                                                                           
/* Example Call:   void GetPasswdFromReg(char* UserName, char* PassWord);                                    
/*                                                                           
/* Input:    UserName - User to retrieve password for
/*			 PassWord - buffer to return password in (char PassWord [MAX_PWD_LEN])
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:  nothing		                                                       
/*                                                                           
/*****************************************************************************/
void GetPasswdFromReg(char* UserName, char* PassWord)
{
	HKEY CPVTkey;
	HKEY USERkey;
	char buffer[MAX_PWD_LEN + 1];
	DWORD retlen;
	long error;

	buffer[0] = '\0';
	retlen = MAX_PWD_LEN + 1;

	error = RegOpenKeyEx(   PWD_HKEY, PWD_SUBKEY, RESERVED_ZERO, KEY_QUERY_VALUE, &CPVTkey);
	error = RegOpenKeyEx(   CPVTkey,  UserName,   RESERVED_ZERO, KEY_QUERY_VALUE, &USERkey);
	error = RegQueryValueEx(USERkey,  PWD_FIELD,  RESERVED_ZERO, NULL,            (LPBYTE)buffer, &retlen);

	RegCloseKey(USERkey);
	RegCloseKey(CPVTkey);

	if(retlen > 0 && buffer[0] != '\0'){
		buffer[retlen-1] = '\0';
	}
	PassWord = strncpy(PassWord, buffer, retlen);

	return;
}


/*****************************************************************************/
/* Name:     GetPasswdForUser	                                                   
/*                                                                           
/* Purpose:  Retrieves a Users password from the registry after checking if the user exists
/*                                          
/*                                                                           
/* Example Call:   int GetPasswdForUser(char* UserName, char* PassWord);                                    
/*                                                                           
/* Input:    UserName - User to retrieve password for
/*			 PassWord - buffer to return password in (char PassWord [MAX_PWD_LEN])
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   1   If User was found   									 ***/
/*		0   If User not found   									 ***/
/*		-1  If Unexpected error occured                                                        
/*                                                                           
/*****************************************************************************/
int GetPasswdForUser(char* UserName, char* PassWord)
{
	int retval;

	if(UserInRegList(UserName) == PWD_OK){
		GetPasswdFromReg(UserName, PassWord);
		retval = PWD_OK;
	}
	else{
		retval = PWD_NOT_FOUND;
	}

	return retval;
}


/*****************************************************************************/
/* Name:     CreateUserKey_n_Passwd                                                        
/*                                                                           
/* Purpose:  Creates a new user entry and fills in the password
/*                                          
/*                                                                           
/* Example Call:   int CreateUserKey_n_Passwd(char* UserName, char* PassWord);                                    
/*                                                                           
/* Input:    UserName - The new user's name
/*			 PassWord - The new users password
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   1 If successfull				 							 ***/
/*		0 If failed	                                                        
/*                                                                           
/*****************************************************************************/
int CreateUserKey_n_Passwd(char* UserName, char* PassWord)
{
	HKEY CPVTkey;
	char* FullSubKey;
	DWORD disposition;
	long error;
	int retval;

	FullSubKey = (char *)malloc(sizeof(PWD_SUBKEY) + strlen(UserName) + 2); // Modified the statement due to Critical Observation.
	if(FullSubKey == NULL){
		retval = PWD_SET_ERROR;
	}
	else{
	sprintf(FullSubKey, "%s\\%s", PWD_SUBKEY, UserName);	

	error = RegCreateKeyEx(PWD_HKEY, FullSubKey, RESERVED_ZERO, "KEY", REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &CPVTkey, &disposition);
	free(FullSubKey);

	error = RegSetValueEx(CPVTkey, PWD_FIELD, RESERVED_ZERO, REG_SZ, (LPBYTE)PassWord, strlen(PassWord) + 1);
	if(error != ERROR_SUCCESS){
		retval = PWD_SET_ERROR;
	}
	else{
		retval = PWD_OK;
	}
	
	RegCloseKey(CPVTkey);
	}

	return retval;
}


/*****************************************************************************/
/* Name:    AddUser		                                                      
/*                                                                           
/* Purpose:  Creates a new user
/*                                          
/*                                                                           
/* Example Call:   int AddUser(char* UserName, char* PassWord);                                    
/*                                                                           
/* Input:    UserName - The new user's name
/*			 PassWord - the new user's password
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   2	If Max users reached									 ***/
/*		1   If User already exists 									 ***/
/*		0   If User was added   									 ***/
/*		-1  If Unexpected error occured 	                                                       
/*                                                                           
/*****************************************************************************/
int AddUser(char* UserName, char* PassWord)
{
	if(CurrUserCount() > MAX_USERS){
		return PWD_MAX_USERS;
	}
	if(UserInRegList(UserName) == PWD_OK){
		return PWD_USER_EXISTS;
	}

	AddUserToRegList(UserName);
	if(CreateUserKey_n_Passwd(UserName, PassWord) != PWD_OK){
		DeleteUser(UserName);
		return PWD_UNKNOWN_ERROR;
	}

	return PWD_OK;
}


/*****************************************************************************/
/* Name:     DeleteUserFromList	                                                      
/*                                                                           
/* Purpose:  Removes a user name from the registry list
/*                                          
/*                                                                           
/* Example Call:   int DeleteUserFromList(char* UserName);                                    
/*                                                                           
/* Input:    UserName - the user to delete          
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   1 if found and deleted	 									 ***/
/*		1 if not found and deleted 	                                                        
/*                                                                           
/*****************************************************************************/
int DeleteUserFromList(char* UserName)
{
	char* Userlist;
	char* Name;
	HKEY  CPVTkey;
	char  newUserlist[((MAX_USERNAME_LEN + 1) * MAX_USERS) + 1];
	long  error;
	int   retval = PWD_NOT_FOUND;

	if(UserInRegList(UserName) == PWD_OK){
		newUserlist[0] = '\0';
		Userlist = GetUserList();
		Name = strtok(Userlist, NameListDelimiter);
		do{
			if(Name != NULL){
				if(strcmp(Name, UserName) != 0){
					strcat(newUserlist, Name);
					strcat(newUserlist, NameListDelimiter);
				}
			}
		} while(Name = strtok(NULL, NameListDelimiter));

		free(Userlist);
		error = RegOpenKeyEx(   PWD_HKEY, PWD_SUBKEY,   RESERVED_ZERO, KEY_ALL_ACCESS, &CPVTkey);
		error = RegSetValueEx(  CPVTkey,  PWD_NAMELIST, RESERVED_ZERO, REG_SZ,         (LPBYTE)newUserlist, strlen(newUserlist) + 1);
		RegCloseKey(CPVTkey);
		if(error != ERROR_SUCCESS){
			retval = PWD_SET_ERROR;
		}
		else{
			retval = PWD_OK;
		}
	}

	return retval;
}


/*****************************************************************************/
/* Name:     DeleteUserKey                                                      
/*                                                                           
/* Purpose:  Removes a user's key from the registry
/*                                          
/*                                                                           
/* Example Call:   void DeleteUserKey(char* UserName);                                    
/*                                                                           
/* Input:    UserName - User whos key to delete         
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:  nothing	                                                       
/*                                                                           
/*****************************************************************************/
void DeleteUserKey(char* UserName)
{
	char* KeytoDel = (char *)malloc(sizeof(PWD_SUBKEY) + strlen(UserName) + 2); // Modified the statement due to Critical Observation.
	sprintf(KeytoDel, "%s\\%s", PWD_SUBKEY, UserName);
	RegDeleteKey(PWD_HKEY, KeytoDel);
	free(KeytoDel);
}


/*****************************************************************************/
/* Name:     DeleteUser	                                                      
/*                                                                           
/* Purpose:  Removes a user completely from the registry
/*                                          
/*                                                                           
/* Example Call:   void DeleteUser(char* UserName);                                    
/*                                                                           
/* Input:    UserName - User to remove      
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:  1   nothing	                                                       
/*                                                                           
/*****************************************************************************/
void DeleteUser(char* UserName)
{
	DeleteUserFromList(UserName);
	DeleteUserKey(UserName);
}


/*****************************************************************************/
/* Name:    ChangeUserPasswd	                                                     
/*                                                                           
/* Purpose:  Changes a users password
/*                                          
/*                                                                           
/* Example Call:   int ChangeUserPasswd(char* UserName, char* PassWord);                                    
/*                                                                           
/* Input:    UserName - User who's password to change     
/*			 PassWord - User's new password
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return: 0 Error														 ***/
/*	   1 No Error                                                      
/*                                                                           
/*****************************************************************************/
int ChangeUserPasswd(char* UserName, char* PassWord)
{
	HKEY CPVTkey;
	HKEY USERkey;
	long error;

	if(UserInRegList(UserName) != PWD_OK){
		return PWD_NOT_FOUND;
	}

	error = RegOpenKeyEx(PWD_HKEY, PWD_SUBKEY, RESERVED_ZERO, KEY_ALL_ACCESS, &CPVTkey);
	error = RegOpenKeyEx(CPVTkey,  UserName,   RESERVED_ZERO, KEY_ALL_ACCESS, &USERkey);
	error = RegSetValueEx(USERkey, PWD_FIELD,  RESERVED_ZERO, REG_SZ,         (LPBYTE)PassWord, strlen(PassWord)+1);

	RegCloseKey(USERkey);
	RegCloseKey(CPVTkey);

	if(error){
		return PWD_SET_ERROR;
	}

	return PWD_OK;
}


/*****************************************************************************/
/* Name:     GetUserList	                                               
/*                                                                           
/* Purpose:  Retrieves the user list from the registry.
/*                                          
/*                                                                           
/* Example Call:   extern char* GetUserList(void);                                    
/*                                                                           
/* Input:    <none>          
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   Pointer to the comma delimited string of user names			
/*     	     Or NULL.						                                                    
/*                                                                           
/*****************************************************************************/
char* GetUserList(void)
{
	HKEY hCPVTkey;
	char* buffer;
	DWORD retlen;
	long error;

	retlen = ((MAX_USERNAME_LEN + 1) * MAX_USERS) +1;
	buffer = (char *)malloc(retlen); // Modified the statement due to Critical Observation.
	buffer[0] = '\0';

	error = RegOpenKeyEx(   PWD_HKEY, PWD_SUBKEY,   RESERVED_ZERO, KEY_QUERY_VALUE, &hCPVTkey);
	error = RegQueryValueEx(hCPVTkey, PWD_NAMELIST, RESERVED_NULL, NULL,            (LPBYTE)buffer, &retlen);
	RegCloseKey(hCPVTkey);

	if(error != ERROR_SUCCESS){
		free(buffer);
		return NULL;
	}
	buffer[retlen - 1] = '\0'; // just to make sure it's terminated.

	if(retlen <= 0){
		free(buffer);
		return NULL;
	}
	else{
		return buffer;
	}
}


/*****************************************************************************/
/* Name:     ValidatePassword                                              
/*                                                                           
/* Purpose:  Sees if the supplied password for a user matches the one in the registry
/*                                          
/*                                                                           
/* Example Call:  int  ValidatePassword(char* UserName, char* PassWord);                                    
/*                                                                           
/* Input:    UserName - User who's password to check
/*			 PassWord - Supplied password to check
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   PWD_OK if password is correct for username					
/*		PWD_BAD_PASSWORD otherwise			                                                  
/*                                                                           
/*****************************************************************************/
int ValidatePassword(char* UserName, char* PassWord)
{
	char PwFromReg[MAX_PWD_LEN + 1];

	if(UserInRegList(UserName) != PWD_OK){
		return PWD_NOT_FOUND;
	}

	GetPasswdFromReg(UserName, PwFromReg);
	if(strcmp(PassWord, PwFromReg) == 0){
		return PWD_OK;
	}
	else{
		return PWD_BAD_PASSWORD;
	}
}
