
#ifndef NetworkMsgHeader_HH
#define NetworkMsgHeader_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: NetworkMsgHeader - Network Message header structure.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Network-Application/vcssrc/NetworkMsgHeader.hhv   25.0.4.0   19 Nov 2013 14:14:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By:  jhv    Date:  06-23-95    DR Number: 
//	Project:  Sigma (R8027)
//	Description:
//		Initial version (Integration baseline).
//
//====================================================================


	struct NetworkMsgHeader
	{
	  //@ Data-Member: msgId;
	  // msgId identifies the type of the network message of the following
	  // packet.
	  Uint16  msgId;

	  //@ Data-Member: pktSize;
	  // pktSize indicates size of the following network packet in bytes.
	  Uint16 pktSize;

	  //@ Data-Member seqNumber
	  //  Sequence number for the following network packet.
	  Uint32 seqNumber;
	};

#endif // NetworkMsgHeader_HH 
