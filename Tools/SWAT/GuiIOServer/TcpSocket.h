#pragma once
#include <winsock2.h>

class TcpSocket
{
public:
	TcpSocket(void);
	~TcpSocket(void);

	bool Create();
	bool Connect(const CHAR* addr, UINT port);
	void Close();
	INT Send(const CHAR* sendbuf);
	INT Receive(CHAR* recvbuf, INT recvbuflen);
	bool ReceiveReady();

private:
	SOCKET m_connectSocket;
	UINT m_port;
};
