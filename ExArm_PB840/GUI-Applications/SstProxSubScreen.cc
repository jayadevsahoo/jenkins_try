#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2010, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: SstProxSubScreen - The subscreen is automatically activated
// during PROX SST Test.
//---------------------------------------------------------------------
//@ Interface-Description
// This class is a container, derived from the GUI-Foundation Container
// class.  This class contains textual fields and containers.  This
// subscreen displays PROX SST test data information while running.
// The activate()/deactivate() methods are called automatically on
// display/removal of this subscreen by our subscreen area owner.
// Register all test data to Service-Data subsystem and process the data
// by various GuiTestManager methods throught processTestData().
//---------------------------------------------------------------------
//@ Rationale
// Implements the complete functionality of the Sst PROX Result subscreen in
// a single class.
//---------------------------------------------------------------------
//@ Implementation-Description
// The implementation is quite simple.  The ProxSstResult subscreen is activated.
// When each SST test is running, the test data shall
// be displayed on the upper screen.  Methods are also provided to set
// special data format for different PROX SST tests.
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and pre-conditions
// to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
// Only one instance of this class should be created.  It should only
// be displayed in UpperSubScreenArea.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/SstProxSubScreen.ccv   25.0.4.0   19 Nov 2013 14:08:30   pvcs  $
//
//@ Modification-Log
//
//  Revision: 010   By: rhj   Date: 22-Feb-2011    SCR Number: 6738
//  Project:  PROX
//  Description:
//       Modified UNIT_Y_, VALUE_Y_ and Y_OFFSET_ to fit the
//       Japanese text.
// 
//  Revision: 009   By: rhj   Date: 29-Sept-2010    SCR Number: 6682 
//  Project:  PROX
//  Description:
//      Fixed the wrong units for wye pressure under the Prox Auto
//      Zero and Leak Test.
//  
//  Revision: 008   By: rhj   Date: 13-Sept-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//      Added flow results during leak test.  
//  
//  Revision: 007   By: mnr   Date: 03-Jun-2010    	SCR Number: 6436
//  Project:  PROX
//  Description:
//		formatLabel_ removed, code review action items related changes.
//
//  Revision: 006   By: mnr   Date: 04-May-2010    	SCR Number: 6436
//  Project:  PROX
//  Description:
//		Updates to reuse data display enums to conserve NOVRAM.
//
//  Revision: 005   By: mnr   Date: 03-May-2010    	SCR Number: 6436
//  Project:  PROX
//  Description:
//		Updates for Pressure Cross Check test.
//	
//  Revision: 004  By:  mnr    Date:  28-APR-2010   SCR Number: 6436
//  Project:  PROX
//  Description:
//      Units fixed for Pressure, AutozeroLeak test display items added.
//
//  Revision: 003  By:  mnr    Date:  20-APR-2010   SCR Number: 6436
//  Project:  PROX
//  Description:
//      LeakGuage removed, setDataFormat() updated.
//
//  Revision: 002  By:  mnr    Date:  16-APR-2010   SCR Number: 6436
//  Project:  PROX
//  Description:
//        Auto Zero and Leak test updated, SetDataFormat() cleaned up.
//
//  Revision: 001  By:  mnr    Date:  13-APR-2010   SCR Number: 6436
//  Project:  PROX
//  Description:
//        Initial checkin.
//=====================================================================

#include <string.h>

#include "MiscStrs.hh"
#include "SstProxSubScreen.hh"
#include "ServiceLowerScreen.hh"
#include "SettingContextHandle.hh"

//@ Usage-Classes
#include "SubScreenArea.hh"
#include "SmDataId.hh"
#include "SmTestId.hh"
#include "DiscreteValue.hh"
#include "LanguageValue.hh"
//@ End-Usage

//@ Code...

// Initialize static constants.
static const Int16 RESULT_AREA_WIDTH_ = 634;
static const Int16 RESULT_AREA_HEIGHT_ = 273;
static const Int16 LABEL_X_ = 20;
static const Int16 LABEL_Y_ = 35;   
static const Int16 VALUE_X_ = 20;
static const Int16 VALUE_Y_ = 50;
static const Int16 UNIT_X_  = 45;
static const Int16 UNIT_Y_  = 50;
static const Int16 X_OFFSET_ = 200;
static const Int16 Y_OFFSET_ = 32;
static const Int16 DISPLAY_ITEMS_PER_COL_ = 7;

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SstProxSubScreen()  [Constructor]
//
//@ Interface-Description
// Constructor.  The 'pSubScreenArea' parameter is the pointer to the
// parent SubScreenArea which contains this subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Initializes all members; Sets the position, size and color of the subscreen
// area. Add the subscreen title object and the log to the 
// parent container for display.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SstProxSubScreen::SstProxSubScreen(SubScreenArea *pSubScreenArea) :
	SubScreen(pSubScreenArea),
	titleArea_(NULL, SubScreenTitleArea::SSTA_CENTER),
	testId_(ProxTest::PX_TEST_NOT_START_ID),
	testManager_(this),
	maxDataCount_(0),
	maxDisplayableDataCount_(0),
	pressureUnitId_(GuiApp::PressUnits() == PressUnitsValue::CMH2O_UNIT_VALUE ?
					MiscStrs::EST_TEST_RESULT_CM_H20_UNIT :
					MiscStrs::EST_TEST_RESULT_HPA_UNIT),
	proxCurrentTestIndex_(ProxTest::PX_TEST_NOT_START_ID)
{
	// Size and position the subscreen
	setX(0);
	setY(0);
	setWidth(SERVICE_LOWER_SUB_SCREEN_AREA_WIDTH);
	setHeight(SERVICE_UPPER_SUB_SCREEN_AREA_HEIGHT);

	// Associate the testResults object with the pointer to the test result
	// array.
	testManager_.setupTestResultDataArray();

	// Size and position the text displayable containers
	valueAreaContainer_.setX(0);
	valueAreaContainer_.setY(0);
	valueAreaContainer_.setWidth(RESULT_AREA_WIDTH_);
	valueAreaContainer_.setHeight(RESULT_AREA_HEIGHT_);
	labelAreaContainer_.setX(0);
	labelAreaContainer_.setY(0);
	labelAreaContainer_.setWidth(RESULT_AREA_WIDTH_);
	labelAreaContainer_.setHeight(RESULT_AREA_HEIGHT_);
	unitAreaContainer_.setX(0);
	unitAreaContainer_.setY(0);
	unitAreaContainer_.setWidth(RESULT_AREA_WIDTH_);
	unitAreaContainer_.setHeight(RESULT_AREA_HEIGHT_);

	// Hide the following drawables
	valueAreaContainer_.setShow(FALSE);
	labelAreaContainer_.setShow(FALSE);
	unitAreaContainer_.setShow(FALSE);

	// Add the following drawables
	addDrawable(&valueAreaContainer_);
	addDrawable(&labelAreaContainer_);
	addDrawable(&unitAreaContainer_);

	addDrawable(&titleArea_);


	for (Int idx = 0; idx < TestDataId::MAX_DATA_ENTRIES; idx++)
	{
		isTestResultDisplayable_[idx] = FALSE;

		// Hide the following drawables
		testResultValueArray_[idx].setShow(FALSE);
		testResultLabelArray_[idx].setShow(FALSE);
		testResultUnitArray_[idx].setShow(FALSE);

		// Add the following drawables
		valueAreaContainer_.addDrawable(&testResultValueArray_[idx]);
		labelAreaContainer_.addDrawable(&testResultLabelArray_[idx]);
		unitAreaContainer_.addDrawable(&testResultUnitArray_[idx]);

		testResultValueArray_[idx].setColor(Colors::WHITE);
		testResultLabelArray_[idx].setColor(Colors::WHITE);
		testResultUnitArray_[idx].setColor(Colors::WHITE);
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~SstProxSubScreen  [Destructor]
//
//@ Interface-Description
// Destroys EstResultSubScreen.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Empty
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SstProxSubScreen::~SstProxSubScreen(void)
{

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activate
//
//@ Interface-Description
//  Called by our subscreen area before this subscreen is to be displayed
//  allowing us to do any necessary setup. Needs no parameters.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Register to Service-Data subsystem for all possible test data events.
//  Show the value, label and unit drawables for each test.  Initialize
//  the isTestResultDisplayable_[], testResultValueArray_[],
//  testResultLabelArray_[], and testResultUnitArray_[] arrays to null.
//  Activate the leak gauge display.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SstProxSubScreen::activate(void)
{
	Int idx;

	// Register to Service-Data subsystem for all possible test data events
	testManager_.registerTestResultData();

	// Show the following drawables
	valueAreaContainer_.setShow(TRUE);
	labelAreaContainer_.setShow(TRUE);
	unitAreaContainer_.setShow(TRUE);

	// Initialize all the following items
	for (idx = 0; idx < TestDataId::MAX_DATA_ENTRIES; idx++)
	{
		isTestResultDisplayable_[idx] = FALSE;
		testResultValueArray_[idx].setShow(FALSE);
		testResultLabelArray_[idx].setShow(FALSE);
		testResultUnitArray_[idx].setShow(FALSE);
	}

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: deactivate
//
//@ Interface-Description
//  Called by our subscreen area just after this subscreen is removed from
//  the display allowing us to do any necessary cleanup. Needs no parameters.
//---------------------------------------------------------------------
//@ Implementation-Description
//  
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SstProxSubScreen::deactivate(void)
{
	proxCurrentTestIndex_ = ProxTest::PX_TEST_NOT_START_ID;
	clearScreen();

	// Hide the following drawables
	valueAreaContainer_.setShow(FALSE);
	labelAreaContainer_.setShow(FALSE);
	unitAreaContainer_.setShow(FALSE);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processTestData
//
//@ Interface-Description
//  This is a virtual method inherited from being an TestResutlTarget.  It is
//  called when the Service Data Manager detectes a new test data as indexed
//  by "dataIndex" and the test result is stored in pResult.
//---------------------------------------------------------------------
//@ Implementation-Description
//  If this piece of data is intended to be displayed on the upper screen
//  then we shall format the test data so the value, unit and label will
//  all be displayed at one time.
//---------------------------------------------------------------------
//@ PreCondition
//	dataIndex < TestDataId::LAST_DATA_ITEM && dataIndex >= 0
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SstProxSubScreen::processTestData(Int dataIndex, TestResult *pResult)
{

	SAFE_CLASS_ASSERTION(dataIndex < TestDataId::LAST_DATA_ITEM &&
						 dataIndex >= 0);

	//update the Upper Screen acc. to current sub test
	if ( dataIndex == SmDataId::PX_CURRENT_SUB_TEST_ID )
	{
		clearScreen();
		proxCurrentTestIndex_ = pResult->getTestDataValue();
		setDataFormat((ProxTest::ProxSstSubTestId)proxCurrentTestIndex_);
	}
	else if (isTestResultDisplayable_[dataIndex])
	{   
		// Display the data only when the Service mode says so.
		setText_(&testResultValueArray_[dataIndex], pResult->getStringValue());
		testResultValueArray_[dataIndex].setShow(TRUE);
		testResultLabelArray_[dataIndex].setShow(TRUE);
		testResultUnitArray_[dataIndex].setShow(TRUE);
	}

	return;
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setResultTitle
//
//@ Interface-Description
//  Initialize the titleArea_ with the passed parameter "title"
//---------------------------------------------------------------------
//@ Implementation-Description
//  Simply set the proper titleArea_ to the passed parameter "title"
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SstProxSubScreen::setResultTitle(StringId title)
{
	if (!isVisible())
	{                                                   
		return;
	}

	titleArea_.setShow(TRUE);
	titleArea_.setTitle(title);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setDataFormat
//
//@ Interface-Description
//  Prepare the data format for the current test's displayable result.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Initialize the isTestResultDisplayable_[], testResultValueArray_[],
//  testResultLabelArray_[], and testResultUnitArray_[] for the passed
//  parameter "testId".  
//  Based on the testId, call setupDisplayableTestResult_() to setup the
//  data precision, title string, and unit string for each displayable
//  test data.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SstProxSubScreen::setDataFormat(ProxTest::ProxSstSubTestId testId)
{
	Int16 idx = 0;

	if (!isVisible())
	{
		return;
	}

	testId_ = testId;
	for (idx = 0; idx < TestDataId::MAX_DATA_ENTRIES; idx++)
	{
		testManager_.setPrecision((SmDataId::ItemizedTestDataId) idx, 0);
		isTestResultDisplayable_[idx] = FALSE;
		testResultValueArray_[idx].setShow(FALSE);
		testResultLabelArray_[idx].setShow(FALSE);
		testResultUnitArray_[idx].setShow(FALSE);
	}

	maxDataCount_ = 0;
	maxDisplayableDataCount_ = 0;

	idx = 0;
	StringId unitId;

	switch (testId_)
	{
	case ProxTest::PX_BAROMETRIC_PRESS_CC_TEST_ID :         
		setResultTitle(MiscStrs::PX_BAROMETRIC_TEST_TITLE);
		testManager_.setPrecision(SmDataId::PX_DATA_ITEM_1, 2);
		testManager_.setPrecision(SmDataId::PX_DATA_ITEM_2, 2);
		unitId = MiscStrs::MMHG_UNIT_LABEL;
		setupDisplayableTestResult_(SmDataId::PX_DATA_ITEM_1, 2, &idx,
									MiscStrs::PX_DISTAL_BAROMETRIC_PRESSURE_TITLE,
									unitId);
		setupDisplayableTestResult_(SmDataId::PX_DATA_ITEM_2, 2, &idx,
									MiscStrs::PX_WYE_BAROMETRIC_PRESSURE_TITLE,
									unitId);

		maxDataCount_ = SmDataId::PROX_SST_TEST_END - SmDataId::PROX_SST_TEST_START + 1;

		break;

	case ProxTest::PX_AUTO_ZERO_LEAK_TEST_ID:   
		setResultTitle(MiscStrs::PX_AUTO_ZERO_LEAK_TEST_TITLE);

		testManager_.setPrecision(SmDataId::PX_DATA_ITEM_1, 2);
		testManager_.setPrecision(SmDataId::PX_DATA_ITEM_2, 2);
		testManager_.setPrecision(SmDataId::PX_DATA_ITEM_3, 2);
		testManager_.setPrecision(SmDataId::PX_DATA_ITEM_4, 2);
		testManager_.setPrecision(SmDataId::PX_DATA_ITEM_5, 2);
		testManager_.setPrecision(SmDataId::PX_DATA_ITEM_6, 2);
		testManager_.setPrecision(SmDataId::PX_DATA_ITEM_7, 2);
		testManager_.setPrecision(SmDataId::PX_DATA_ITEM_8, 2);

		unitId = MiscStrs::PSI_UNIT_LABEL;
		setupDisplayableTestResult_(SmDataId::PX_DATA_ITEM_1, 2, &idx,
									MiscStrs::PX_AUTO_ZERO_ACCUMULATOR_PRESSURE_TITLE1,
									unitId);

		setupDisplayableTestResult_(SmDataId::PX_DATA_ITEM_2, 2, &idx,
									MiscStrs::PX_AUTO_ZERO_WYE_PRESSURE_TITLE2,
									pressureUnitId_);

		setupDisplayableTestResult_(SmDataId::PX_DATA_ITEM_3, 2, &idx,
									MiscStrs::PX_AUTO_ZERO_ACCUMULATOR_PRESSURE_TITLE3,
									unitId);
		setupDisplayableTestResult_(SmDataId::PX_DATA_ITEM_4, 2, &idx,
									MiscStrs::PX_AUTO_ZERO_ACCUMULATOR_PRESSURE_TITLE4,
									unitId);
		setupDisplayableTestResult_(SmDataId::PX_DATA_ITEM_5, 2, &idx,
									MiscStrs::PX_AUTO_ZERO_ACCUMULATOR_PRESSURE_TITLE5,
									unitId);
		setupDisplayableTestResult_(SmDataId::PX_DATA_ITEM_6, 2, &idx,
									MiscStrs::PX_AUTO_ZERO_ACCUMULATOR_PRESSURE_TITLE6,
									unitId);
		setupDisplayableTestResult_(SmDataId::PX_DATA_ITEM_7, 2, &idx,
									MiscStrs::PX_AUTO_ZERO_FLOW_TITLE7,
									MiscStrs::LPM_UNIT_LABEL);
		setupDisplayableTestResult_(SmDataId::PX_DATA_ITEM_8, 2, &idx,
									MiscStrs::PX_AUTO_ZERO_WYE_PRESSURE_TITLE8,
									pressureUnitId_);

		maxDataCount_ = SmDataId::PROX_SST_TEST_END - SmDataId::PROX_SST_TEST_START + 1;

		break;

	case ProxTest::PX_PURGE_TEST_ID :
		setResultTitle(MiscStrs::PX_PURGE_TEST_TITLE);
		testManager_.setPrecision(SmDataId::PX_DATA_ITEM_1, 2);
		testManager_.setPrecision(SmDataId::PX_DATA_ITEM_2, 2);
		testManager_.setPrecision(SmDataId::PX_DATA_ITEM_3, 2);
		testManager_.setPrecision(SmDataId::PX_DATA_ITEM_4, 2);
		testManager_.setPrecision(SmDataId::PX_DATA_ITEM_5, 2);

		setupDisplayableTestResult_(SmDataId::PX_DATA_ITEM_1, 2, &idx,
									MiscStrs::PX_PURGE_TEST_ACCUMATOR_PRESSURE_AT_ZERO_TITLE,
									MiscStrs::PSI_UNIT_LABEL);
		setupDisplayableTestResult_(SmDataId::PX_DATA_ITEM_2, 2, &idx,
									MiscStrs::PX_PURGE_TEST_RESERVOIR_PRESSURE_AT_THREE_PSI_TITLE,
									MiscStrs::PSI_UNIT_LABEL);
		setupDisplayableTestResult_(SmDataId::PX_DATA_ITEM_3, 2, &idx,
									MiscStrs::PX_PURGE_TEST_MIN_RESERVOIR_PRESSURE_AT_THREE_PSI_TITLE,
									MiscStrs::PSI_UNIT_LABEL);
		setupDisplayableTestResult_(SmDataId::PX_DATA_ITEM_4, 2, &idx,
									MiscStrs::PX_PURGE_TEST_RESERVOIR_PRESSURE_AT_SIX_PSI_TITLE,
									MiscStrs::PSI_UNIT_LABEL);
		setupDisplayableTestResult_(SmDataId::PX_DATA_ITEM_5, 2, &idx,
									MiscStrs::PX_PURGE_TEST_MIN_RESERVOIR_PRESSURE_AT_SIX_PSI_TITLE,
									MiscStrs::PSI_UNIT_LABEL);

		maxDataCount_ = SmDataId::PROX_SST_TEST_END - SmDataId::PROX_SST_TEST_START + 1;

		break;

	case ProxTest::PX_PRESSURE_CC_TEST_ID :
		setResultTitle(MiscStrs::PX_PRESSURE_CC_TEST_TITLE);

		testManager_.setPrecision(SmDataId::PX_DATA_ITEM_1, 2);
		testManager_.setPrecision(SmDataId::PX_DATA_ITEM_2, 2);
		testManager_.setPrecision(SmDataId::PX_DATA_ITEM_3, 2);
		testManager_.setPrecision(SmDataId::PX_DATA_ITEM_4, 2);
		testManager_.setPrecision(SmDataId::PX_DATA_ITEM_5, 2);

		unitId = MiscStrs::EST_TEST_RESULT_CM_H20_UNIT;

		setupDisplayableTestResult_(SmDataId::PX_DATA_ITEM_1, 2, &idx,
									MiscStrs::PX_WYE_PRESSURE_TITLE,
									pressureUnitId_);
		setupDisplayableTestResult_(SmDataId::PX_DATA_ITEM_2, 2, &idx,
									MiscStrs::PX_DISTAL_INSP_PRESSURE_TITLE_AT_10,
									pressureUnitId_);
		setupDisplayableTestResult_(SmDataId::PX_DATA_ITEM_3, 2, &idx,
									MiscStrs::PX_WYE_PRESSURE_TITLE_AT_10,
									pressureUnitId_);
		setupDisplayableTestResult_(SmDataId::PX_DATA_ITEM_4, 2, &idx,
									MiscStrs::PX_DISTAL_INSP_PRESSURE_TITLE_AT_50,
									pressureUnitId_);

		setupDisplayableTestResult_(SmDataId::PX_DATA_ITEM_5, 2, &idx,
									MiscStrs::PX_WYE_PRESSURE_TITLE_AT_50,
									pressureUnitId_);

		maxDataCount_ = SmDataId::PROX_SST_TEST_END - SmDataId::PROX_SST_TEST_START + 1;

		break;

	case ProxTest::PX_FLOW_CC_TEST_ID :
		setResultTitle(MiscStrs::PX_FLOW_CC_TEST_TITLE);
		testManager_.setPrecision(SmDataId::PX_DATA_ITEM_1, 2);
		testManager_.setPrecision(SmDataId::PX_DATA_ITEM_2, 2);
		testManager_.setPrecision(SmDataId::PX_DATA_ITEM_3, 2);
		testManager_.setPrecision(SmDataId::PX_DATA_ITEM_4, 2);
		testManager_.setPrecision(SmDataId::PX_DATA_ITEM_5, 2);
		testManager_.setPrecision(SmDataId::PX_DATA_ITEM_6, 2);
		testManager_.setPrecision(SmDataId::PX_DATA_ITEM_7, 2);

		setupDisplayableTestResult_(SmDataId::PX_DATA_ITEM_1, 2, &idx,
									MiscStrs::PX_VENT_INSPIRATORY_FLOW_AT_TWENTY_LPM_TITLE,
									MiscStrs::LPM_UNIT_LABEL);

		setupDisplayableTestResult_(SmDataId::PX_DATA_ITEM_2, 2, &idx,
									MiscStrs::PX_PROX_INSPIRATORY_FLOW_AT_TWENTY_LPM_TITLE,
									MiscStrs::LPM_UNIT_LABEL);

		setupDisplayableTestResult_(SmDataId::PX_DATA_ITEM_3, 2, &idx,
									MiscStrs::PX_VENT_INSPIRATORY_FLOW_AT_FIVE_LPM_TITLE,
									MiscStrs::LPM_UNIT_LABEL);
		setupDisplayableTestResult_(SmDataId::PX_DATA_ITEM_4, 2, &idx,
									MiscStrs::PX_PROX_INSPIRATORY_FLOW_AT_FIVE_LPM_TITLE,
									MiscStrs::LPM_UNIT_LABEL);

		setupDisplayableTestResult_(SmDataId::PX_DATA_ITEM_5, 2, &idx,
									MiscStrs::PX_VENT_INSPIRATORY_FLOW_AT_ONE_LPM_TITLE,
									MiscStrs::LPM_UNIT_LABEL);
		setupDisplayableTestResult_(SmDataId::PX_DATA_ITEM_6, 2, &idx,
									MiscStrs::PX_PROX_INSPIRATORY_FLOW_AT_ONE_LPM_TITLE,
									MiscStrs::LPM_UNIT_LABEL);
		setupDisplayableTestResult_(SmDataId::PX_DATA_ITEM_7, 2, &idx,
									MiscStrs::PX_PROX_INSPIRATORY_FLOW_AT_ZERO_LPM_TITLE,
									MiscStrs::LPM_UNIT_LABEL);

		maxDataCount_ = SmDataId::PROX_SST_TEST_END - SmDataId::PROX_SST_TEST_START + 1;
		break;

	default:
		AUX_CLASS_ASSERTION_FAILURE(testId_);
		break;      
	}

	// Remember the maximum displayable data count
	maxDisplayableDataCount_ = idx;

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: clearScreen
//
//@ Interface-Description
//  Sets the subscreen into empty state.
//---------------------------------------------------------------------
//@ Implementation-Description
//  We hide the titleArea_, and all the test data related value, title,
//  and unit drables.  We also hide the leakGauge.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
SstProxSubScreen::clearScreen(void)
{
	Int idx;

	if (!isVisible())
	{                                                   
		return;
	}

	titleArea_.setShow(FALSE);
	maxDataCount_ = 0;

	// Hide the following drawables
	for (idx = maxDataCount_; idx < TestDataId::MAX_DATA_ENTRIES; idx++)
	{
		testResultValueArray_[idx].setShow(FALSE);
		testResultLabelArray_[idx].setShow(FALSE);
		testResultUnitArray_[idx].setShow(FALSE);
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setText_
//
//@ Interface-Description
//  Format then store the given resultString into a given text field.
//---------------------------------------------------------------------
//@ Implementation-Description
//  We first format the temporary string with the test result data.
//  Then call the textField method: setText() to set the text value.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
  
void
SstProxSubScreen::setText_(TextField *testResultField, wchar_t *resultString)
{
	wchar_t formatString[TextField::MAX_STRING_LENGTH+1];
	swprintf(formatString, L"{p=8,y=11:%s}", resultString);
	testResultField->setText(formatString);

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setupDisplayableTestResult_
//
//@ Interface-Description
//  This method is to provide an generic function for formating test data.
//---------------------------------------------------------------------
//@ Implementation-Description
//  We first inform the testManager of the precision of this data to be
//  displayed.  Then we set this data to be displayable.  Followed by
//  setting the unit and title for this data.  Calculate the display
//  position for the data value, unit and label.  Finally, we increment
//  the maximun displayable count for this EST test.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SstProxSubScreen::setupDisplayableTestResult_(SmDataId::ItemizedTestDataId testDataIdx,
											  Int16 dataPrecision, Int16 *currentDisplayableCount,
											  StringId labelString, StringId unitString)
{

	Int16  rowIncrement;
	Int16  colIncrement;
	Int16  rowPosition;
	Int16  colPosition;

	// Set the precision
	testManager_.setPrecision(testDataIdx, dataPrecision);
	isTestResultDisplayable_[testDataIdx] = TRUE;

	// Set the label and the unit strings
	testResultLabelArray_[testDataIdx].setText(labelString);
	testResultUnitArray_[testDataIdx].setText(unitString);

	// Calculate the row and col for the texts
	rowIncrement = *currentDisplayableCount % DISPLAY_ITEMS_PER_COL_;
	colIncrement = *currentDisplayableCount / DISPLAY_ITEMS_PER_COL_;

	// Calculate the row, and col pixel location for value string.
	rowPosition = VALUE_Y_ + Y_OFFSET_ * rowIncrement;
	colPosition = VALUE_X_ + X_OFFSET_ * colIncrement;

	testResultValueArray_[testDataIdx].setX(colPosition);
	testResultValueArray_[testDataIdx].setY(rowPosition);

	// Calculate the row, and col pixel location for labael string.
	rowPosition = LABEL_Y_ + Y_OFFSET_ * rowIncrement;
	colPosition = LABEL_X_ + X_OFFSET_ * colIncrement;

	testResultLabelArray_[testDataIdx].setX(colPosition);
	testResultLabelArray_[testDataIdx].setY(rowPosition);

	// Calculate the row, and col pixel location for unit string.
	rowPosition = UNIT_Y_ + Y_OFFSET_ * rowIncrement;
	colPosition = UNIT_X_ + X_OFFSET_ * colIncrement;

	testResultUnitArray_[testDataIdx].setX(colPosition);
	testResultUnitArray_[testDataIdx].setY(rowPosition);

	(*currentDisplayableCount)++;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
SstProxSubScreen::SoftFault(const SoftFaultID  softFaultID,
							const Uint32       lineNumber,
							const char*        pFileName,
							const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS,
							SSTPROXSUBSCREEN, lineNumber, pFileName, pPredicate);
}


