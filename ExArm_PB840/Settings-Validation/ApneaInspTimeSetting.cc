#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//================= C L A S S   D E S C R I P T I O N =================
//@ Class:  ApneaInspTimeSetting - Apnea Inspiratory Time Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, bounded setting that contains the time (in milliseconds)
//  an inspiration lasts during apnea ventilation.  This class inherits from
//  'BatchBoundedSetting' and provides the specific information needed for
//  representation of apnea inspiratory time.  This information includes
//  the interval and range of this setting's values, this setting's
//  response to a Main Control Setting's transition (see
//  'acceptTransition()'), this setting's response to a change of one of its
//  primary setting (see 'acceptPrimaryChange()'), this batch setting's
//  new-patient value (see 'getNewPatientValue()'), and the contraints and
//  dependent settings of this setting.
//
//  This class provides three static methods for calculating apnea
//  inspiratory time based on the value of other settings.  These
//  calculation methods provide a standard way for all settings (as needed)
//  to calculate an apnea inspiratory time value.
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class defines an 'updateConstraints_()' method for updating
//  the constraints of this setting.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/ApneaInspTimeSetting.ccv   25.0.4.0   19 Nov 2013 14:27:12   pvcs  $
//
//@ Modification-Log
//
//  Revision: 008   By: gdc    Date: 16-Aug-2010     SCR Number: 6663
//  Project: API/MAT
//  Description:
//  Added support for VentSet API.
// 
//  Revision: 007   By: mnr    Date: 20-Jul-2009    SCR Number: 6437
//  Project:  NEO
//  Description:
//		Renamed variable according to coding standards. 
//
//  Revision: 006   By: mnr    Date: 10-Jul-2009    SCR Number: 6437
//  Project:  NEO
//  Description:
//		Added code to enforce the lower bound for Apnea Insp Time. 
//
//  Revision: 005   By: sah    Date: 29-Jun-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  modified to use newly-modified 'BoundedRange' class to handle
//         range, via linked list of interval nodes
//      *  supporting use of 'RAMP' flow pattern during new-patient
//         calculation
//
//  Revision: 004   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'getApplicability()' method
//	*  modified 'acceptTransition()' method to remove explicit calculation
//	   of dependent settings -- now done by main control setting
//	*  cleaned up DEVELOPMENT-only 'is{Adjusted,Accepted}Valid()' methods
//
//  Revision: 003   By: dosman Date:  23-Sep-1998    DR Number: BILEVEL 144
//  Project:  BILEVEL
//  Description:
//	Added code to assert if acceptTransition cannot reach a stable setting value
//	when trying to resolve dependent values.
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//=====================================================================

#include "ApneaInspTimeSetting.hh"
#include "MandTypeValue.hh"
#include "FlowPatternValue.hh"

//@ Usage-Classes
#include "SettingsMgr.hh"
#include "ContextId.hh"
#include "ApneaRespRateSetting.hh"
//@ End-Usage


//@ Code...

//====================================================================
//
//  Static Data Definitions...
//
//====================================================================

// $[02066] -- this setting's dependent settings...
static const SettingId::SettingIdType  ARR_DEPENDENT_SETTING_IDS_[] =
  {
    SettingId::APNEA_EXP_TIME,
    SettingId::APNEA_IE_RATIO,

    SettingId::NULL_SETTING_ID
  };


// $[02064] The setting's resolution...
static const Real32  APNEA_PCV_RESOLUTION_ = 10.0f;
static const Real32  APNEA_VCV_RESOLUTION_ = 20.0f;


//  $[02062] -- The setting's range...
//  $[02064] -- The setting's resolution ...
static const BoundedInterval  LAST_NODE_ =
  {
    200.0f,		// absolute minimum...
    0.0f,		// unused...
    TENS,		// unused...
    NULL
  };
static BoundedInterval  IntervalList_ =
  {
    8000.0f,		// absolute maximum...
    20.0f,		// resolution...
    TENS,		// precision...
    &::LAST_NODE_
  };



//====================================================================
//
//  Public Methods...
//
//====================================================================

//============= M E T H O D   D E S C R I P T I O N ==================
//@ Method: ApneaInspTimeSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This setting inherits from BoundedSetting.  The method passes to
//  BoundedSetting it's default information.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ApneaInspTimeSetting::ApneaInspTimeSetting(void)
	  : BatchBoundedSetting(SettingId::APNEA_INSP_TIME,
			        ::ARR_DEPENDENT_SETTING_IDS_,
			        &::IntervalList_,
				APNEA_INSP_TIME_MAX_ID,	// maxConstraintId...
				APNEA_INSP_TIME_MIN_ID)	// minConstraintId...
{
  CALL_TRACE("ApneaInspTimeSetting()");
	isATimingSetting_ = TRUE;
}   // $[TI1]


//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  ~ApneaInspTimeSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ApneaInspTimeSetting::~ApneaInspTimeSetting(void)
{
  CALL_TRACE("~ApneaInspTimeSetting()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[01096] -- apnea breath settings applicability
//---------------------------------------------------------------------
//@ PreCondition
//  (qualifierId == Notification::ACCEPTED  ||
//   qualifierId == Notification::ADJUSTED)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
ApneaInspTimeSetting::getApplicability(
			      const Notification::ChangeQualifier qualifierId
				      ) const
{
  CALL_TRACE("getApplicability(qualifierId)");

  const Setting*  pApneaMandType =
		      SettingsMgr::GetSettingPtr(SettingId::APNEA_MAND_TYPE);

  DiscreteValue  apneaMandTypeValue;

  switch (qualifierId)
  {
  case Notification::ACCEPTED :		// $[TI1]
    apneaMandTypeValue = pApneaMandType->getAcceptedValue();
    break;
  case Notification::ADJUSTED :		// $[TI2]
    apneaMandTypeValue = pApneaMandType->getAdjustedValue();
    break;
  default :
    AUX_CLASS_ASSERTION_FAILURE(qualifierId);
    break;
  }

  //-------------------------------------------------------------------
  // NOTE:  this is dependent on Apnea Constant Parm being fixed at
  //        INSP_TIME_CONSTANT...
  //-------------------------------------------------------------------

  return((apneaMandTypeValue == MandTypeValue::PCV_MAND_TYPE)
	   ? Applicability::CHANGEABLE		// $[TI3]
	   : Applicability::VIEWABLE);		// $[TI4]
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [const, virtual]
//
//@ Interface-Description
//  Get the setting's new-patient value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02063] -- new-patient equation ...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
ApneaInspTimeSetting::getNewPatientValue(void) const
{
  CALL_TRACE("getNewPatientValue()");

  // force "VCV" resolution during new-patient calculation...
  ::IntervalList_.resolution = ::APNEA_VCV_RESOLUTION_;

  // calculate apnea inspiratory time's new-patient value using the apnea VCV
  // new-patient values (any apnea VCV setting id will do)...
  BoundedValue  newPatientValue = calcBasedOnSetting_(
  						SettingId::APNEA_TIDAL_VOLUME,
						BoundedRange::WARP_NEAREST,
						BASED_ON_NEW_PATIENT_VALUES
						       );

  // check the value to not fall below the lower bound
  const Real32  ABSOLUTE_MIN = getAbsoluteMinValue_();
  newPatientValue.value = MAX_VALUE( ABSOLUTE_MIN, newPatientValue.value );

  const Setting*  pApneaMandType =
			SettingsMgr::GetSettingPtr(SettingId::APNEA_MAND_TYPE);

  if (DiscreteValue(pApneaMandType->getAdjustedValue()) ==
						  MandTypeValue::PCV_MAND_TYPE)
  {  // $[TI1]
    // set to "PCV" resolution...
    ::IntervalList_.resolution = ::APNEA_PCV_RESOLUTION_;
  }  // $[TI2]

  return(newPatientValue);
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method: acceptPrimaryChange(primaryId)
//
//@ Interface-Description
//  One of this setting's primary settings have changed, therefore update
//  this setting's value based on the new value of the primary setting
//  -- indicated by 'primaryId'.  If this setting's bound is violated by
//  the primary setting's newly "adjusted" value, a pointer to this
//  setting's bound status is returned.  Otherwise, a value of 'NULL' is
//  returned to indicate no dependent bound was violated.
//
//  If apnea inspiratory time is being changed by an apnea VCV setting,
//  this method will automatically update apnea inspiratory time's dependent
//  settings (i.e., apnea expiratory time and apnea I:E ratio), this allows
//  the apnea VCV settings to notify and monitor apnea inspiratory time,
//  only.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02002] -- new dependent settings based on proposed primary settings...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

const BoundStatus*
ApneaInspTimeSetting::acceptPrimaryChange(
				const SettingId::SettingIdType primaryId
				         )
{
  CALL_TRACE("acceptPrimaryChange(primaryId)");

  // update dynamic constraints...
  updateConstraints_();

  BoundStatus&  rBoundStatus = getBoundStatus_();

  // initialize to holding this setting's id...
  rBoundStatus.setViolationId(getId());

  BoundedValue  newApneaInspTime;

  newApneaInspTime = calcBasedOnSetting_(primaryId, BoundedRange::WARP_NEAREST,
					 BASED_ON_ADJUSTED_VALUES);

  // test 'newApneaInspTime' against this setting's bounded range; the bound
  // violation state, if any, is returned in 'rBoundStatus', while
  // 'newApneaInspTime' is "clipped" if a bound is violated...
  getBoundedRange_().testValue(newApneaInspTime, rBoundStatus);

  // store the value...
  setAdjustedValue(newApneaInspTime);

  const BoundStatus*  pBoundStatus;

  // if there is a bound violation return a pointer to this setting's
  // bound status, otherwise return 'NULL'...
  pBoundStatus = (rBoundStatus.isInViolation()) ? &rBoundStatus	// $[TI1]
						: NULL;		// $[TI2]

  return(pBoundStatus);
}


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method: acceptTransition(settingId, newValue, currValue) [virtual]
//
//@ Interface-Description
//  This method is responsible for updating this setting based on the
//  the Main Control Setting identified by 'settingId' and the transition
//  of its value from 'currValue' to 'newValue'.
//
//  The only transitions that apnea inspiratory time is interested in is the
//  transitions of apnea mandatory type from "PCV" to "VCV ventilation, or vice
//  versa.  This method will conduct the appropriate transition, and -- unlike
//  other responses to a transition -- this method updates this setting's
//  dependent settings (i.e., apnea expiratory time and apnea I:E ratio).
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02024]\f\ -- apnea insp time's transition rule...
//---------------------------------------------------------------------
//@ PreCondition
//  (settingId == SettingId::APNEA_MAND_TYPE)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
ApneaInspTimeSetting::acceptTransition(
				  const SettingId::SettingIdType settingId,
				  const DiscreteValue            newValue,
				  const DiscreteValue
				      )
{
  CALL_TRACE("acceptTransition(settingId, newValue, currValue)");

  AUX_CLASS_PRE_CONDITION((settingId == SettingId::APNEA_MAND_TYPE),
		          settingId);

  // reset both constraints to ensure that "old" constraints won't interfere
  // with newly-calculated values...
  getBoundedRange_().resetMinConstraint();
  getBoundedRange_().resetMaxConstraint();

  switch (newValue)
  {
  case MandTypeValue::VCV_MAND_TYPE :		// $[TI1]
    {
      //====================================================================
      // apnea mandatory type transition from PCV to VCV...
      //====================================================================

      // use apnea VCV resolution...
      ::IntervalList_.resolution = ::APNEA_VCV_RESOLUTION_;

      BoundedValue  newApneaInspTime;

      // calculate apnea inspiratory time based on the apnea VCV values...
      newApneaInspTime = calcBasedOnSetting_(SettingId::APNEA_TIDAL_VOLUME,
					     BoundedRange::WARP_NEAREST,
					     BASED_ON_ADJUSTED_VALUES);

	  // check the value to not fall below the lower bound
	  const Real32  ABSOLUTE_MIN = getAbsoluteMinValue_();
	  newApneaInspTime.value = MAX_VALUE( ABSOLUTE_MIN, newApneaInspTime.value );

      // store the transition value into the adjusted context...
      setAdjustedValue(newApneaInspTime);
    }
    break;

  case MandTypeValue::PCV_MAND_TYPE :		// $[TI2]
    //====================================================================
    // apnea mandatory type transition from VCV to PCV...
    //====================================================================

    // use the apnea PCV resolution...
    ::IntervalList_.resolution = ::APNEA_PCV_RESOLUTION_;
    break;

  default :
    // unexpected apnea mandatory type...
    AUX_CLASS_ASSERTION_FAILURE(newValue);
    break;
  };

  setForcedChangeFlag();
}


#if defined(SIGMA_DEVELOPMENT)

//============== M E T H O D   D E S C R I P T I O N =================
// Method:  isAcceptedValid()  [const, virtual]
//
// Interface-Description
//  Is the accepted value of this setting valid as determined by its
//  dependencies?
//---------------------------------------------------------------------
// Implementation-Description
//  The setting determines, and returns its validity.
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

Boolean
ApneaInspTimeSetting::isAcceptedValid(void) const
{
  CALL_TRACE("isAdjustedValid()");

  Boolean  isValid = BoundedSetting::isAcceptedValid();

  if (isValid)
  {
    if (getApplicability(Notification::ACCEPTED) !=
					    Applicability::INAPPLICABLE)
    {
      Setting*  pApneaExpTime =
			  SettingsMgr::GetSettingPtr(SettingId::APNEA_EXP_TIME);
      Setting*  pApneaIeRatio =
			  SettingsMgr::GetSettingPtr(SettingId::APNEA_IE_RATIO);

      // forward on to apnea inspiratory time's dependent settings...
      isValid = (pApneaExpTime->isAcceptedValid()  &&
		 pApneaIeRatio->isAcceptedValid());

      if (!isValid)
      {
	cout << "INVALID Apnea Inspiratory Time:\n";
	cout << *this << *pApneaExpTime << *pApneaIeRatio << endl;
      }

      const Setting*  pApneaTidalVolume =
		SettingsMgr::GetSettingPtr(SettingId::APNEA_TIDAL_VOLUME);

      if (isValid   &&
	  pApneaTidalVolume->getApplicability(Notification::ACCEPTED) !=
						    Applicability::INAPPLICABLE)
      {
	// Is the the accepted inspiratory time value consistent with the
	// accepted VCV parameters?...
	const Real32  APNEA_INSP_TIME_VALUE =
				  BoundedValue(getAcceptedValue()).value;
	const Real32  CALC_APNEA_INSP_TIME_VALUE = calcBasedOnSetting_(
						SettingId::APNEA_TIDAL_VOLUME,
						BoundedRange::WARP_NEAREST,
						BASED_ON_ACCEPTED_VALUES).value;

	isValid = (APNEA_INSP_TIME_VALUE == CALC_APNEA_INSP_TIME_VALUE);

	if (!isValid)
	{
	  const Setting*  pApneaFlowPattern =
	      SettingsMgr::GetSettingPtr(SettingId::APNEA_FLOW_PATTERN);
	  const Setting*  pApneaMinInspFlow =
	      SettingsMgr::GetSettingPtr(SettingId::APNEA_MIN_INSP_FLOW);
	  const Setting*  pApneaPeakInspFlow =
	     SettingsMgr::GetSettingPtr(SettingId::APNEA_PEAK_INSP_FLOW);
	  const Setting*  pApneaPlateauTime =
	       SettingsMgr::GetSettingPtr(SettingId::APNEA_PLATEAU_TIME);

	  cout << "\nINVALID Apnea Inspiratory Time:\n";
	  cout << "CALC = " << CALC_APNEA_INSP_TIME_VALUE << endl;
	  cout << *this << *pApneaFlowPattern << *pApneaMinInspFlow
	       << *pApneaPeakInspFlow << *pApneaPlateauTime
	       << *pApneaTidalVolume << endl;
	}
      }
    }
  }

  return(isValid);
}


//============== M E T H O D   D E S C R I P T I O N =================
// Method:  isAdjustedValid()  [const, virtual]
//
// Interface-Description
//  Is the adjusted value of this setting valid as determined by its
//  dependencies?
//---------------------------------------------------------------------
// Implementation-Description
//  The setting determines, and returns its validity.
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

Boolean
ApneaInspTimeSetting::isAdjustedValid(void)
{
  CALL_TRACE("isAdjustedValid()");

  Boolean  isValid = BoundedSetting::isAdjustedValid();

  if (isValid)
  {
    if (getApplicability(Notification::ADJUSTED) !=
					    Applicability::INAPPLICABLE)
    {
      Setting*  pApneaExpTime =
		      SettingsMgr::GetSettingPtr(SettingId::APNEA_EXP_TIME);
      Setting*  pApneaIeRatio =
		      SettingsMgr::GetSettingPtr(SettingId::APNEA_IE_RATIO);

      // forward on to apnea inspiratory time's dependent settings...
      isValid = (pApneaExpTime->isAdjustedValid()  &&
		 pApneaIeRatio->isAdjustedValid());

      const Setting*  pApneaTidalVolume =
		SettingsMgr::GetSettingPtr(SettingId::APNEA_TIDAL_VOLUME);

      if (isValid   &&
	  pApneaTidalVolume->getApplicability(Notification::ADJUSTED) !=
						    Applicability::INAPPLICABLE)
      {
	// is the the adjusted apnea inspiratory time value consistent with the
	// adjusted apnea VCV parameters?...
	const Real32  APNEA_INSP_TIME_VALUE =
				  BoundedValue(getAdjustedValue()).value;
	const Real32  CALC_APNEA_INSP_TIME_VALUE = calcBasedOnSetting_(
						SettingId::APNEA_TIDAL_VOLUME,
						BoundedRange::WARP_NEAREST,
						BASED_ON_ADJUSTED_VALUES).value;

	isValid = (APNEA_INSP_TIME_VALUE == CALC_APNEA_INSP_TIME_VALUE);

	if (!isValid)
	{
	  Setting*  pApneaFlowPattern =
		      SettingsMgr::GetSettingPtr(SettingId::APNEA_FLOW_PATTERN);
	  Setting*  pApneaMinInspFlow =
		      SettingsMgr::GetSettingPtr(SettingId::APNEA_MIN_INSP_FLOW);
	  Setting*  pApneaPeakInspFlow =
		      SettingsMgr::GetSettingPtr(SettingId::APNEA_PEAK_INSP_FLOW);
	  Setting*  pApneaPlateauTime =
		      SettingsMgr::GetSettingPtr(SettingId::APNEA_PLATEAU_TIME);
	  Setting*  pApneaTidalVolume =
		      SettingsMgr::GetSettingPtr(SettingId::APNEA_TIDAL_VOLUME);

	  cout << "\nINVALID Apnea Inspiratory Time:\n";
	  cout << "CALC = " << CALC_APNEA_INSP_TIME_VALUE << endl;
	  cout << *this << *pApneaFlowPattern << *pApneaMinInspFlow
	       << *pApneaPeakInspFlow << *pApneaPlateauTime
	       << *pApneaTidalVolume << endl;
	}
      }
    }
  }

  return(isValid);
}

#endif // defined (SIGMA_DEVELOPMENT)


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//	      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ApneaInspTimeSetting::SoftFault(const SoftFaultID  softFaultID,
			        const Uint32       lineNumber,
			        const char*        pFileName,
			        const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
			  APNEA_INSP_TIME_SETTING, lineNumber, pFileName,
			  pPredicate);
} 


//====================================================================
//
//  Protected Methods...
//
//====================================================================

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  updateConstraints_()  [virtual]
//
//@ Interface-Description
//  Update the dynamic bounds of this setting.  This method sets this
//  setting's maximum constraint to the most restrictive of this setting's
//  upper bounds, and, correspondingly, set this setting's minimum
//  constraint to its most restrictive lower bound.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
ApneaInspTimeSetting::updateConstraints_(void)
{
  CALL_TRACE("updateConstraints_()");

  const Setting*  pApneaMandType =
		      SettingsMgr::GetSettingPtr(SettingId::APNEA_MAND_TYPE);

  const DiscreteValue  APNEA_MAND_TYPE_VALUE =
  					pApneaMandType->getAdjustedValue();

  if (APNEA_MAND_TYPE_VALUE == MandTypeValue::VCV_MAND_TYPE)
  {   // $[TI1]
    ::IntervalList_.resolution = ::APNEA_VCV_RESOLUTION_;
  }
  else if (APNEA_MAND_TYPE_VALUE == MandTypeValue::PCV_MAND_TYPE)
  {   // $[TI2]
    ::IntervalList_.resolution = ::APNEA_PCV_RESOLUTION_;
  }
  else
  {
    // invalid apnea mandatory type...
    AUX_CLASS_ASSERTION_FAILURE(APNEA_MAND_TYPE_VALUE);
  }

  //===================================================================
  // determine apnea inspiratory time's maximum bound...
  //===================================================================

  const Real32  BREATH_PERIOD_BASED_MAX =
    (ApneaRespRateSetting::GetApneaBreathPeriod(BASED_ON_ADJUSTED_VALUES) -
				    100.0f);

  BoundedRange::ConstraintInfo  maxConstraintInfo;

  maxConstraintInfo.id = APNEA_INSP_TIME_MAX_ID;

  if (::IntervalList_.value < BREATH_PERIOD_BASED_MAX)
  {   // $[TI3]
    maxConstraintInfo.value = ::IntervalList_.value;
  }
  else
  {   // $[TI4]
    // NOTE:   though this constraint will never be hit in a quiescent mode,
    //         it is possible with a LARGE knob delta to jump to an apnea
    //         insp time value larger than this constraint, thereby messing
    //         up apnea I:E ratio bound calculations; this prevents the
    //         screwed up I:E calculation scenario, and allows for apnea I:E
    //         ratio to detect the bound violation, itself...
    maxConstraintInfo.value = BREATH_PERIOD_BASED_MAX;
  }

  // this setting has no soft bounds...
  maxConstraintInfo.isSoft = FALSE;

  // update apnea inspiratory time's maximum constraint info...
  getBoundedRange_().updateMaxConstraint(maxConstraintInfo);
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method: calcBasedOnSetting_(basedOnSettingId, warpDirection, basedOnCategory)  [const]
//
//@ Interface-Description
//  This method is responsible for calculating, and returning, a new
//  "adjusted" value for this setting, based on the "adjusted" value
//  of the dependent setting given by 'basedOnSettingId'.  This method will
//  calculate values for each of this setting's dependent settings,
//  based on values in the Adjusted Context.
//---------------------------------------------------------------------
//@ Implementation-Description
//  All new-patient calculations are to be based on the apnea VCV setting
//  values.
//---------------------------------------------------------------------
//@ PreCondition
//  ((basedOnSettingId == SettingId::APNEA_TIDAL_VOLUME  ||
//    basedOnSettingId == SettingId::APNEA_PEAK_INSP_FLOW  ||
//    basedOnSettingId == SettingId::APNEA_PLATEAU_TIME  ||
//    basedOnSettingId == SettingId::APNEA_FLOW_PATTERN)  &&
//   (basedOnCategory == BASED_ON_ADJUSTED_VALUES  ||
//    basedOnCategory == BASED_ON_NEW_PATIENT_VALUES))
//			||
//  ((basedOnSettingId == SettingId::APNEA_IE_RATIO  ||
//    basedOnSettingId == SettingId::APNEA_EXP_TIME)  &&
//    basedOnCategory == BASED_ON_ADJUSTED_VALUES)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

BoundedValue
ApneaInspTimeSetting::calcBasedOnSetting_(
			  const SettingId::SettingIdType basedOnSettingId,
			  const BoundedRange::WarpDir    warpDirection,
			  const BasedOnCategory          basedOnCategory
					) const
{
  CALL_TRACE("calcBasedOnSetting_(basedOnSettingId, warpDirection, basedOnCategory)");

  BoundedValue  apneaInspTime;

  switch (basedOnSettingId)
  {
  //-------------------------------------------------------------------
  // $[02013](1) -- new apnea insp. time while changing apnea I:E ratio...
  //-------------------------------------------------------------------
  case SettingId::APNEA_IE_RATIO :
    {   // $[TI1] -- base apnea insp time's calculation on apnea I:E ratio...
      // apnea inspiratory time is to base its value on the "adjusted"
      // apnea I:E ratio, only...
      AUX_CLASS_ASSERTION((basedOnCategory == BASED_ON_ADJUSTED_VALUES),
      			  basedOnCategory);

      const Setting*  pApneaIeRatio =
		SettingsMgr::GetSettingPtr(SettingId::APNEA_IE_RATIO);

      // get the apnea I:E ratio value from the Adjusted Context...
      const Real32  APNEA_IE_RATIO_VALUE =
		      BoundedValue(pApneaIeRatio->getAdjustedValue()).value;

      // get the apnea breath period from the apnea respiratory rate...
      const Real32  APNEA_BREATH_PERIOD_VALUE =
		  ApneaRespRateSetting::GetApneaBreathPeriod(basedOnCategory);

      Real32  eRatioValue;
      Real32  iRatioValue;

      if (APNEA_IE_RATIO_VALUE < 0.0f)
      {   // $[TI1.1]
	// the apnea I:E ratio value is negative, therefore we have 1:xx with
	// "xx" the apnea expiratory portion of the ratio...
	iRatioValue = 1.0f;
	eRatioValue = -(APNEA_IE_RATIO_VALUE);
      }
      else
      {   // $[TI1.2]
	// the apnea I:E ratio value is positive, therefore we have xx:1 with
	// "xx" the apnea inspiratory portion of the ratio...
	iRatioValue = APNEA_IE_RATIO_VALUE;
	eRatioValue = 1.0f;
      }

      // calculate the apnea insp time from the apnea I:E ratio...
      apneaInspTime.value = (iRatioValue * APNEA_BREATH_PERIOD_VALUE) /
				   (iRatioValue + eRatioValue);

#if defined(SIGMA_DEVELOPMENT)
      if (Settings_Validation::IsDebugOn(::APNEA_INSP_TIME_SETTING))
      {
        cout << "AITS::cBOS BPV==" << APNEA_BREATH_PERIOD_VALUE
           << " AIERV==" << APNEA_IE_RATIO_VALUE
           << " aITv==" << apneaInspTime.value
           << endl;
      }
#endif // defined(SIGMA_DEVELOPMENT)
    }
    break;

  //-------------------------------------------------------------------
  // $[02012](2) -- new apnea insp. time while changing apnea exp. time...
  //-------------------------------------------------------------------
  case SettingId::APNEA_EXP_TIME :
    {   // $[TI2] -- base apnea insp time's calculation on apnea exp time...
      // apnea inspiratory time is to base its value on the "adjusted" apnea
      // expiratory time, only...
      AUX_CLASS_ASSERTION((basedOnCategory == BASED_ON_ADJUSTED_VALUES),
      			  basedOnCategory);

      const Setting*  pApneaExpTime =
		SettingsMgr::GetSettingPtr(SettingId::APNEA_EXP_TIME);

      // get the apnea expiratory time value from the Adjusted Context...
      const Real32  APNEA_EXP_TIME_VALUE =
			BoundedValue(pApneaExpTime->getAdjustedValue()).value;

      // get the apnea breath period from the apnea respiratory rate...
      const Real32  APNEA_BREATH_PERIOD_VALUE =
		  ApneaRespRateSetting::GetApneaBreathPeriod(basedOnCategory);

      // calculate apnea inspiratory time...
      apneaInspTime.value = APNEA_BREATH_PERIOD_VALUE - APNEA_EXP_TIME_VALUE;
    }
    break;

  //-------------------------------------------------------------------
  // $[02063]    -- new-patient equation ...
  // $[02019](2) -- new apnea insp. time while changing apnea VCV values...
  //-------------------------------------------------------------------
  case SettingId::APNEA_TIDAL_VOLUME :
  case SettingId::APNEA_PEAK_INSP_FLOW :
  case SettingId::APNEA_PLATEAU_TIME :
  case SettingId::APNEA_FLOW_PATTERN :
    {   // $[TI3] -- base apnea insp time's calculation on the apnea VCV
	//           settings...
      // get pointers to each of the apnea VCV parameters...
      const Setting*  pApneaFlowPattern =
		  SettingsMgr::GetSettingPtr(SettingId::APNEA_FLOW_PATTERN);
      const Setting*  pApneaPeakInspFlow =
		SettingsMgr::GetSettingPtr(SettingId::APNEA_PEAK_INSP_FLOW);
      const Setting*  pApneaPlateauTime =
		  SettingsMgr::GetSettingPtr(SettingId::APNEA_PLATEAU_TIME);
      const Setting*  pApneaTidalVolume =
		  SettingsMgr::GetSettingPtr(SettingId::APNEA_TIDAL_VOLUME);

      DiscreteValue  apneaFlowPatternValue;
      Real32         apneaPeakInspFlowValue;
      Real32         apneaPlateauTimeValue;
      Real32         apneaTidalVolumeValue;

      switch (basedOnCategory)
      {
      case BASED_ON_NEW_PATIENT_VALUES :	// $[TI3.1]
	// get the new-patient values of each of the apnea VCV parameters...
	apneaFlowPatternValue  = pApneaFlowPattern->getNewPatientValue();
	apneaPeakInspFlowValue =
		  BoundedValue(pApneaPeakInspFlow->getNewPatientValue()).value;
	apneaPlateauTimeValue  =
		  BoundedValue(pApneaPlateauTime->getNewPatientValue()).value;
	apneaTidalVolumeValue  =
		  BoundedValue(pApneaTidalVolume->getNewPatientValue()).value;
	break;
      case BASED_ON_ADJUSTED_VALUES :		// $[TI3.2]
	// get the "adjusted" values of each of the apnea VCV parameters...
	apneaFlowPatternValue  = pApneaFlowPattern->getAdjustedValue();
	apneaPeakInspFlowValue =
		  BoundedValue(pApneaPeakInspFlow->getAdjustedValue()).value;
	apneaPlateauTimeValue  =
		  BoundedValue(pApneaPlateauTime->getAdjustedValue()).value;
	apneaTidalVolumeValue  =
		  BoundedValue(pApneaTidalVolume->getAdjustedValue()).value;
	break;
      case BASED_ON_ACCEPTED_VALUES :
	// fall through to 'default:' when not in DEVELOPMENT mode...
#if defined(SIGMA_DEVELOPMENT)
	// get the "accepted" values of each of the apnea VCV parameters...
	apneaFlowPatternValue  = pApneaFlowPattern->getAcceptedValue();
	apneaPeakInspFlowValue =
		  BoundedValue(pApneaPeakInspFlow->getAcceptedValue()).value;
	apneaPlateauTimeValue  =
		  BoundedValue(pApneaPlateauTime->getAcceptedValue()).value;
	apneaTidalVolumeValue  =
		  BoundedValue(pApneaTidalVolume->getAcceptedValue()).value;
	break;
#endif // defined(SIGMA_DEVELOPMENT)
      default :
	// unexpected 'basedOnCategory' value..
	AUX_CLASS_ASSERTION_FAILURE(basedOnCategory);
	break;
      };

      switch (apneaFlowPatternValue)
      {
      case FlowPatternValue::SQUARE_FLOW_PATTERN : 
	{   // $[TI3.3] 
	  static const Real32  SQUARE_FACTOR_ = (60000.0f * 0.001f);

	  // $[02019]b -- formula for calculating apnea inspiratory time
	  //              from the apnea VCV parameters...
	  apneaInspTime.value = ((SQUARE_FACTOR_ * apneaTidalVolumeValue) /
					  apneaPeakInspFlowValue);

	}
	break;
      case FlowPatternValue::RAMP_FLOW_PATTERN :  
	{   // $[TI3.4] 
	  static const Real32  RAMP_FACTOR_ = (60000.0f * 0.001f * 2.0f);

	  const Setting*  pApneaMinInspFlow =
		  SettingsMgr::GetSettingPtr(SettingId::APNEA_MIN_INSP_FLOW);

	  Real32  apneaMinInspFlowValue;

	  switch (basedOnCategory)
	  {
	  case BASED_ON_NEW_PATIENT_VALUES :	// $[TI3.4.2]
	    // get the "new-patient" apnea minimum insp flow value...
	    apneaMinInspFlowValue =
		    BoundedValue(pApneaMinInspFlow->getNewPatientValue()).value;
	    break;
	  case BASED_ON_ADJUSTED_VALUES :	// $[TI3.4.1]
	    // get the "adjusted" apnea minimum insp flow value...
	    apneaMinInspFlowValue =
		    BoundedValue(pApneaMinInspFlow->getAdjustedValue()).value;
	    break;
	  case BASED_ON_ACCEPTED_VALUES :
	    // fall through to 'default:' when not in DEVELOPMENT mode...
#if defined(SIGMA_DEVELOPMENT)
	    // get the "accepted" apnea minimum insp flow value...
	    apneaMinInspFlowValue =
		    BoundedValue(pApneaMinInspFlow->getAcceptedValue()).value;
	    break;
#endif // defined(SIGMA_DEVELOPMENT)
	  default :
	    // unexpected 'basedOnCategory' value..
	    AUX_CLASS_ASSERTION_FAILURE(basedOnCategory);
	    break;
	  };

	  // $[02019]c -- formula for calculating apnea inspiratory time from
	  //              the apnea VCV parameters...
	  apneaInspTime.value = ((RAMP_FACTOR_ * apneaTidalVolumeValue) /
			     (apneaPeakInspFlowValue + apneaMinInspFlowValue));
	}
	break;
      default :
	AUX_CLASS_ASSERTION_FAILURE(apneaFlowPatternValue);
	break;
      };

      // add the apnea plateau time to the calculated apnea inspiratory
      // time -- irregardless of the apnea flow pattern...$[02019]b...
      apneaInspTime.value += apneaPlateauTimeValue;
    }
    break;

  default :
    // unexpected dependent setting id...
    AUX_CLASS_ASSERTION_FAILURE(basedOnSettingId);
    break;
  };

  // place apnea insp time on a "click" boundary...
  getBoundedRange().warpValue(apneaInspTime, warpDirection, FALSE);

#if defined(SIGMA_DEVELOPMENT)
  if (Settings_Validation::IsDebugOn(::APNEA_INSP_TIME_SETTING))
  {
    cout << "AITS::cBOS aft warp aITv==" << apneaInspTime.value << endl;
  }
#endif // defined(SIGMA_DEVELOPMENT)

  return(apneaInspTime);
}
