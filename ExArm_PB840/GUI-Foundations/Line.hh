#ifndef Line_HH
#define Line_HH
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1995, Puritan-Bennett Corporation
//=====================================================================
 
//====================================================================
// Class: Line - This class represents a line on the screen.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Foundations/vcssrc/Line.hhv   25.0.4.0   19 Nov 2013 14:11:44   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006   By: gdc    Date:  06-Feb-2007    SCR Number: 6237
//  Project: Trend
//  Description:
//	Trend project related changes.
//  
//  Revision: 005   By: gdc    Date:  29-Jun-1999    DCS Number: 5337
//  Project: 840 Neonatal
//  Description:
//	Optimized VGA library for Neonatal project to use BitBlt and Windows
//  Bitmap structures.
//
//  Revision: 004   By: sah    Date: 11-Jan-1999   DR Number: 5321
//  Project:  Sigma (R8027)
//  Description:
//      Changed 'SoftFault()' method to non-inline method.
//
//  Revision: 003  By: gdc      Date: 15-Feb-1996  DR Number: 674
//    Project:  Sigma (R8027)
//    Description:
//	Removed ifndef / endif around include directives
//
//  Revision: 002  By: mpm      Date: 02-Aug-1995  DCR Number: 486 
//    Project:  Sigma (R8027)
//    Description:
//      Made destructor virtual as per coding standard requirements.
//  
//  Revision: 001  By: yyy      Date: 01-Jan-1995  DR Number: NONE
//    Project:  Sigma (R8027)
//    Description:
//	Integration baseline.
//====================================================================

#include "Sigma.hh"
#include "GuiFoundation.hh"
#include "Drawable.hh"

//@ Usage-Classes
#include "Colors.hh"
//@ End-Usage

class Line : public Drawable
{
public:
	Line(void);
	Line(Int xStart, Int yStart, Int xEnd, Int yEnd,
			Int penWidth = 1);
	virtual ~Line(void);

	void setVertices(Int xStart, Int yStart, Int xEnd, Int yEnd);
	void setStartPoint(Int x1, Int y1);
	void setEndPoint(Int x2, Int y2);
	void setPenWidth(Int penWidth);

	virtual void setX(Int x);
	virtual void setY(Int y);
	virtual void setWidth(Int width);
	virtual void setHeight(Int height);

	// virtual in Drawable
	virtual void draw(const Area& clipArea);	

	static void SoftFault(const SoftFaultID  softFaultID,
						  const Uint32       lineNumber,
						  const char*        pFileName  = NULL,
						  const char*        pPredicate = NULL);

#if defined SIGMA_UNIT_TEST
	int getPenWidth_(void);
#endif	// SIGMA_UNIT_TEST
								 
protected:

private:
	Line(const Line&);				// declared, not implemented.
	void operator=(const Line&);	// declared, not implemented.

	static const Int MIN_PEN_WIDTH;
	static const Int MAX_PEN_WIDTH;
	
	void resetArea_(void);

	//@ Data-Member: x1_
	// X coordinate of the start point of the line.
	Int x1_;

	//@ Data-Member: y1_
	// Y coordinate of the start point of the line.
	Int y1_;

	//@ Data-Member: x2_
	// X coordinate of the end point of the line.
	Int x2_;

	//@ Data-Member: y2_
	// Y coordinate of the  point of the line.
	Int y2_;

	//@ Data-Member: penWidth_
	// Stores the width of the line.
	Int penWidth_;

	inline Int getHalfPenWidth_(void);

};

// Inlined methods
#include "Line.in"

#endif  // Line_HH

