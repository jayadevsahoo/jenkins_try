#ifndef BigSerialNumber_HH
#define BigSerialNumber_HH

//====================================================================
// This is a proprietary work to which Nellcor Puritan Bennett
// corporation of California claims exclusive right.  No part of this
// work may be used, disclosed, reproduced, sorted in an information
// retrieval system, or transmitted by any means, electronic,
// mechanical, photocopying, recording, or otherwise without the prior
// written permission of Nellcor Puritan Bennett Corporation.
//
//     Copyright (c) 2010, Covidien Ltd
//=====================================================================
 
//====================================================================
// Class: BigSerialNumber - a big string that contains serial number information.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/BigSerialNumber.hhv   25.0.4.0   19 Nov 2013 13:53:58   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By: mnr    Date: 25-Feb-2010   SCR Number: 6436
//  Project:  PROX4
//  Description:
//      Initial version checked in.
//
//====================================================================
 

#if 0  
// for bug in genDD
#endif

#include "Sigma.hh"
#include "BD_IO_Devices.hh"

//@ Usage-Classes

#include "ProxiInterface.hh"

//@ End-Usage

#ifdef SIGMA_DEVELOPMENT
#include "Ostream.hh"
#endif // SIGMA_DEVELOPMENT

class BigSerialNumber 
{
  public:
    BigSerialNumber();
    ~BigSerialNumber();
    BigSerialNumber(const BigSerialNumber& serial_number);
    BigSerialNumber(const char * pString);
    BigSerialNumber& operator=(const BigSerialNumber& serial_number);
    Boolean operator==(const BigSerialNumber& serial_number) const;
    Boolean operator!=(const BigSerialNumber& serial_number) const;

#ifdef SIGMA_DEVELOPMENT
    friend ostream& operator<< 
      (ostream& theStream, const BigSerialNumber& rNumber);
#endif // SIGMA_DEVELOPMENT

	const char * getString(void) const;
    char operator[](const Uint index) const;

    static void         SoftFault(const SoftFaultID  softFaultID,
								  const Uint32       lineNumber,
								  const char*        pFileName = NULL,
								  const char*        pPredicate = NULL);

  private:

    //@ Data-Member:    data_
    //  The serial number data itself.
    char   data_[ProxiInterface::PROX_MAX_REV_STR_SIZE];

};

#endif // BigSerialNumber_HH

