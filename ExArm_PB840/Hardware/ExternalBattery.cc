#include "stdafx.h"
//====================================================================
// This is a proprietary work to which Covidien Plc claims exclusive 
// right. No part of this work may be used, disclosed, reproduced, 
// stored in an information retrieval system, or transmitted by any 
// means, electronic, mechanical, photocopying, recording, or otherwise 
// without the prior written permission of Covidien Plc.
//
//            Copyright (c) 2014-2025, Covidien Plc.
//====================================================================

#include "ExternalBattery.hh"
#include "EBM_bq34z110.h"

// The boundary current to determine if the battery in charging or
// is the power source, the unit is mA.
static const Int32 BOUNDARY_CURRENT = 100;

ExternalBattery*    ExternalBattery::instance_  = NULL;

ExternalBattery*    ExternalBattery::getInstance()
{
    if ( instance_ == NULL )
    {
        // Pointer to the singleton instance of bq34z110
        instance_ = EBM_bq34z110::getInstance();
    }

    return instance_;
}

ExternalBattery::ExternalBattery(void)
{
}

ExternalBattery::~ExternalBattery(void)
{
}

// Consider as INSTALLED if able to read voltage data from battery
bool ExternalBattery::isInstalled()
{
    Uint16 votage = 0;
    return readVoltage( votage );
}

// If able to read instantaneous current correctly, and the value is less than 
// -BOUNDARY_CURRENT, consider as power souce. Otherwise, is not power source.
bool ExternalBattery::isPowerSource()
{
    Int32 instCurrent = 0;
    bool  isPowerSrc = false;

    if ( readInstantaneousCurrent( instCurrent ) )
    {
        // Only consider the current less than -BOUNDARY_CURRENT mA
        isPowerSrc = (instCurrent < (-BOUNDARY_CURRENT));
    }

    return isPowerSrc;
}

// If able to read instantaneous current correctly, and the value is great than 
// BOUNDARY_CURRENT, consider as CHARGING. Otherwise, is not charging
bool ExternalBattery::isCharging()
{
    Int32 instCurrent = 0;
    bool charging = false;

    if ( readInstantaneousCurrent( instCurrent ))
    {
        // Only consider the current great than 100 mA
        charging = (instCurrent > BOUNDARY_CURRENT);
    }

    return charging;
}
