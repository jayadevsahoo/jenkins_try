
#ifndef SchedulerId_HH
#define SchedulerId_HH

//====================================================================
//
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Struct:  SchedulerId - Enum Values for all schedulers.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header::   /840/Baseline/Breath-Delivery/vcssrc/SchedulerId.hhv   10.7   08/17/07 09:42:58   pvcs  
//
//@ Modification-Log
//
//  Revision: 004  By:  iv    Date:  18-Jan-1998    DR Number: None
//       Project:  BiLevel (R8027)
//       Description:
//             Bilevel initial version.
//
//  Revision: 003  By:  iv    Date:  07-Feb-1997    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Rework per formal code review.
//
//  Revision: 002   By: iv    Date: 05 June 1996    DR Number: DCS 600
//       Project: Sigma (R8027)
//       Description:
//		      Added enum FIRST_SCHEDULER.
//
//  Revision: 001   By: iv    Date: 13 October 1995    DR Number: None
//       Project: Sigma (R8027)
//       Description:
//		      Initial version.
//  
//====================================================================


struct SchedulerId
{
  //@ Type:  SchedulerIdValue
  // All of the possible values of Breath Phase Scheduler.
  enum SchedulerIdValue
  {
	FIRST_SCHEDULER = 0,
	FIRST_BREATHING_SCHEDULER = FIRST_SCHEDULER,
	APNEA = FIRST_BREATHING_SCHEDULER,
	AC,
	SPONT,
	SIMV,
	SAFETY_PCV,
	BILEVEL,
	LAST_BREATHING_SCHEDULER = BILEVEL,
	FIRST_NONBREATHING_SCHEDULER,
	DISCONNECT = FIRST_NONBREATHING_SCHEDULER,
	OCCLUSION,
	SVO,
	STANDBY,
	POWER_UP,
	LAST_NONBREATHING_SCHEDULER = POWER_UP,
    TOTAL_SCHEDULER_IDS
  };
};

#endif // SchedulerId_HH 



