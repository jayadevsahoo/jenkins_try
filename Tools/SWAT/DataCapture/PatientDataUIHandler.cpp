//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file PatientDataUIHandler.cpp
//----------------------------------------------------------------------------

//-------------------------------------
// I N C L U D E S
//-------------------------------------
#include "PatientDataUIEnumDef.h"
#include "IPatientDataSet.h"
#include "Message.h"

#include "NetworkMsgHeader.hh"
#include "IEventLogger.h"
#include "NetworkMsgId.hh"
#include "SocketClient.h"
#include "CLogger.h"

#include"PatientDataUISinSignal.h"
#include"PatientDataUITriggerSignal.h"
#include"PatientDataUIUserFloat.h"
#include"PatientDataUIUserInt.h"
#include"PatientDataUIUserEnum.h"

//-------------------------------------
// C L A S S  D E C L
//-------------------------------------


class PatientDataUIHandler:public IPatientDataSet
{
    public:
    IPatientDataEl * getPatientDataElement(unsigned int);
    IPatientDataEl * getWaveformElement(unsigned int iIndex);

    static bool register_types(IEventLogger *pIEventLogger, void *pData );

    private:

    static void *PatientDataUIHandler::check_header(
            boost::shared_ptr<Message> 
            pMessage, 
            const char *message_id, 
            const unsigned int expected_len
    );



    static bool end_of_breath_data       (boost::shared_ptr<Message> pMessage, void *pUser);
    static bool realtime_breath_data     (boost::shared_ptr<Message> pMessage, void *pUser);
    static bool averaged_breath_data     (boost::shared_ptr<Message> pMessage, void *pUser);
    static bool computed_insp_pause_data (boost::shared_ptr<Message> pMessage, void *pUser);
    static bool bilevel_mand_data        (boost::shared_ptr<Message> pMessage, void *pUser);
    static bool bilevel_mand_exh_data    (boost::shared_ptr<Message> pMessage, void *pUser);
    static bool pav_breath_data          (boost::shared_ptr<Message> pMessage, void *pUser);
    static bool pav_plateau_data         (boost::shared_ptr<Message> pMessage, void *pUser);
    static bool pav_startup_data         (boost::shared_ptr<Message> pMessage, void *pUser);
    static bool fio2_for_gui_msg         (boost::shared_ptr<Message> pMessage, void *pUser);
    static bool paused_peend_data        (boost::shared_ptr<Message> pMessage, void *pUser);
    static bool nif_pressure_data        (boost::shared_ptr<Message> pMessage, void *pUser);
    static bool p100_pressure_data       (boost::shared_ptr<Message> pMessage, void *pUser);
    static bool vital_capacity_data      (boost::shared_ptr<Message> pMessage, void *pUser);
    static bool paused_piend_data        (boost::shared_ptr<Message> pMessage, void *pUser);
    static bool end_exp_pause_pressure_state_data  (boost::shared_ptr<Message> pMessage, void *pUser);
    static bool vtpcv_state_data         (boost::shared_ptr<Message> pMessage, void *pUser);
    static bool peep_recovery_msg        (boost::shared_ptr<Message> pMessage, void *pUser);
    static bool waveform_breath_data     (boost::shared_ptr<Message> pMessage, void *pUser);
    static IPatientDataEl *m_el[];
    static IPatientDataEl *m_el2[];
    static int m_iSize;
};

// The following classes are only exposed through the interface IPatientDataEl and IPatientDataSet.












//-------------------------------------
// S T A T I C  D E C L
//-------------------------------------

// Construct PatientDataEl for each varable
// A PatientDataEl is a container for the patient data varable.  It knows what the allowable 
// values for the varable are.

static UserEnum breathTriggerId             ("breathTriggerId"             ,Enum::getEnumList(Enum::ENUM_Trigger_ID)        );
static UserEnum breathType                  ("breathType"                  ,Enum::getEnumList(Enum::ENUM_BreathType_ID)     );
static UserEnum breathTypeDisplay           ("breathTypeDisplay"           ,Enum::getEnumList(Enum::ENUM_BreathType_ID)     );
static UserEnum mandType                    ("mandType"                    ,Enum::getEnumList(Enum::ENUM_MandType_ID)       );
static UserEnum pavState                    ("pavState"                    ,Enum::getEnumList(Enum::ENUM_PavState_ID)       );
static UserEnum phaseType                   ("phaseType"                   ,Enum::getEnumList(Enum::ENUM_PhaseType_ID)      );
static UserEnum previousBreathType          ("previousBreathType"          ,Enum::getEnumList(Enum::ENUM_BreathType_ID)     );
static UserEnum supportType                 ("supportType"                 ,Enum::getEnumList(Enum::ENUM_SupportType_ID)    );
static UserEnum ventType                    ("ventType"                    ,Enum::getEnumList(Enum::ENUM_VentType_ID)       );
static UserEnum vtpcvState                  ("vtpcvState"                  ,Enum::getEnumList(Enum::ENUM_VtpcvState_ID)     );
static UserEnum end_exp_pause_pressure_state("end_exp_pause_pressure_state",Enum::getEnumList(Enum::ENUM_PpiState_ID)       );
static UserEnum staticComplianceValid       ("staticComplianceValid"       ,Enum::getEnumList(Enum::ENUM_ComplianceState_ID));
static UserEnum staticResistanceValid       ("staticResistanceValid"       ,Enum::getEnumList(Enum::ENUM_ComplianceState_ID));

static UserFloat breathDuration              ("breathDuration"        );
static UserFloat dynamicCompliance           ("dynamicCompliance"     );
static UserFloat dynamicResistance           ("dynamicResistance"     );
static UserFloat elastanceWob                ("elastanceWob"          );
static UserFloat endExpiratoryFlow           ("endExpiratoryFlow"     );
static UserFloat endExpiratoryPressure       ("endExpiratoryPressure" );
static UserFloat endInspPressure             ("endInspPressure"       );

static UserFloat exhaledMinuteVol            ("exhaledMinuteVol"      );
static UserFloat exhaledSpontMinuteVol       ("exhaledSpontMinuteVol" );
static UserFloat exhaledTidalVolume          ("exhaledTidalVolume"    );
static UserFloat fio2_data                   ("fio2"                  );
static UserFloat ieRatio                     ("ieRatio"               );
static UserFloat inspiredlungVolume          ("inspiredlungVolume"    );
static UserFloat intrinsicPeep               ("intrinsicPeep_PAV"     );
static UserFloat lungCompliance              ("lungCompliance"        );
static UserFloat lungElastance               ("lungElastance"         );
static UserFloat mandFraction                ("mandFraction"          );
static UserFloat meanCircuitPressure         ("meanCircuitPressure"   );
static UserFloat nif_pressure                ("nif_pressure"          );
static UserFloat p100_pressure               ("p100_pressure"         );
static UserFloat patientResistance           ("patientResistance"     );
static UserFloat patientWorkOfBreathing      ("patientWorkOfBreathing");
static UserFloat paused_peend                ("paused_peend"          );
static UserFloat paused_piend                ("paused_piend"          );
static UserFloat peakCircuitPressure         ("peakCircuitPressure"   );
static UserFloat peakExpiratoryFlow          ("peakExpiratoryFlow"    );
static UserFloat peakSpontInspFlow           ("peakSpontInspFlow"     );
static UserFloat percentLeak                 ("percentLeak"			  );
static UserFloat inspLeakVol                 ("inspLeakVol"			  );
static UserFloat exhLeakRate                 ("exhLeakRate"			  );
static UserFloat resistiveWob                ("resistiveWob"          );
static UserFloat spontFtotVtRatio            ("spontFtotVtRatio"      );
static UserFloat spontInspTime               ("spontInspTime"         );
static UserFloat spontTiTtotRatio            ("spontTiTtotRatio"      );
static UserFloat staticCompliance            ("staticCompliance"      );
static UserFloat staticResistance            ("staticResistance"      );
static UserFloat totalResistance             ("totalResistance"       );
static UserFloat totalRespRate               ("totalRespRate"         );
static UserFloat totalWorkOfBreathing        ("totalWorkOfBreathing"  );
static UserFloat vital_capacity              ("vital_capacity"        );
static UserFloat WaveformCircuitPressure1    ("WaveformCircuitPressure1");
static UserFloat c20ToCRatio				 ("c20ToCRatio"       );





unsigned int g_uiTime;

static SinSignal WaveformLungPressure        ("WaveformLungPressure"    ,g_uiTime);
static SinSignal WaveformNetFlow             ("WaveformNetFlow"         ,g_uiTime);
static SinSignal WaveformLungFlow            ("WaveformLungFlow"        ,g_uiTime);
static SinSignal WaveformCircuitPressure     ("WaveformCircuitPressure" ,g_uiTime);
static SinSignal WaveformNetVolume           ("WaveformNetVolume"       ,g_uiTime);
static SinSignal WaveformLungVolume          ("WaveformLungVolume"      ,g_uiTime);
static TriggerSignal WaveformBdPhase         ("WaveformBdPhase"         ,g_uiTime);
static UserEnum WaveformBdPhase2             ("WaveformBdPhase"        ,Enum::getEnumList(Enum::ENUM_PhaseType_ID ));
static UserInt WaveformIsAutozeroActive		 ("WaveformIsAutozeroActive" ,g_uiTime);












// Construct a static array of PatientDataEl.
IPatientDataEl *PatientDataUIHandler::m_el[]= 
{
    &breathTriggerId             ,
    &breathType                  ,
    &breathTypeDisplay           ,
    &mandType                    ,
    &pavState                    ,
    &phaseType                   ,
    &previousBreathType          ,
    &supportType                 ,
    &ventType                    ,
    &vtpcvState                  ,
    &end_exp_pause_pressure_state,
    &staticComplianceValid       ,
    &staticResistanceValid       ,
    &breathDuration              ,
    &dynamicCompliance           ,
    &dynamicResistance           ,
    &elastanceWob                ,
    &endExpiratoryFlow           ,
    &endExpiratoryPressure       ,
    &endInspPressure             ,
    &exhaledMinuteVol            ,
    &exhaledSpontMinuteVol       ,
    &exhaledTidalVolume          ,
    &fio2_data                   ,
    &ieRatio                     ,
    &inspiredlungVolume          ,
    &intrinsicPeep               ,
    &lungCompliance              ,
    &lungElastance               ,
    &mandFraction                ,
    &meanCircuitPressure         ,
    &nif_pressure                ,
    &p100_pressure               ,
    &patientResistance           ,
    &patientWorkOfBreathing      ,
    &paused_peend                ,
    &paused_piend                ,
    &peakCircuitPressure         ,
    &peakExpiratoryFlow          ,
    &peakSpontInspFlow           ,
    &resistiveWob                ,
    &spontFtotVtRatio            ,
    &spontInspTime               ,
    &spontTiTtotRatio            ,
    &staticCompliance            ,
    &staticResistance            ,
    &totalResistance             ,
    &totalRespRate               ,
    &totalWorkOfBreathing        ,
    &vital_capacity              ,
	&WaveformBdPhase2        ,
	&WaveformIsAutozeroActive,
	&percentLeak,
	&inspLeakVol,
	&exhLeakRate,
	&WaveformCircuitPressure1,
	&c20ToCRatio,
    NULL
};

IPatientDataEl *PatientDataUIHandler::m_el2[]= 
{
 &WaveformLungPressure      ,
 &WaveformNetFlow           ,
 &WaveformLungFlow          ,
 &WaveformCircuitPressure   ,
 &WaveformNetVolume         ,
 &WaveformLungVolume        ,
 &WaveformBdPhase        ,
  NULL
};


//-------------------------n
// PatientDataUIHandler::getElement    
//---------------------------
IPatientDataEl * PatientDataUIHandler::getPatientDataElement(unsigned int iIndex)
{
   
    static const unsigned int iLen=sizeof(PatientDataUIHandler::m_el)/sizeof(IPatientDataEl *);

    if(iIndex>=iLen)
    {
        return NULL;
    }
    return m_el[iIndex];
}



IPatientDataEl *PatientDataUIHandler::getWaveformElement(unsigned int iIndex)
{
    static const unsigned int iLen=sizeof(PatientDataUIHandler::m_el2)/sizeof(IPatientDataEl *);

    if(iIndex>=iLen)
    {
        return NULL;
    }
    return m_el2[iIndex];

}



// Create an instance of a PatinetDataSet.
PatientDataUIHandler clPatientDataUIHandlerInstance;

//---------------------------
// IPatientDataUIHandler::getInstance    
//---------------------------
IPatientDataSet *IPatientDataSet::getInstance()
{
    return &clPatientDataUIHandlerInstance;
}






//---------------------------
// PatientDataUIHandler::check_header    
//---------------------------
void *PatientDataUIHandler::check_header(boost::shared_ptr<Message> pMessage, const char *message_id, const unsigned int expected_len)
{
    unsigned int len = ntohs(((NetworkMsgHeader *)(( pMessage.get()->get() ).get()))->pktSize);

    if( ((pMessage.get()->getLength()-sizeof(NetworkMsgHeader)) != len) || 
            (len != expected_len )
    )
    {
	 LOG_MESSAGE(LM_ERROR, "PatientDataUIHandler::check_header() invalid message size, message_id=%s message_length=%i expected=%i"
                ,message_id, (pMessage.get()->getLength()-sizeof(NetworkMsgHeader)), expected_len);

        return NULL;
    }

    return (void *) ((char *)((NetworkMsgHeader *)
                    (( pMessage.get()->get() ).get()))+sizeof(NetworkMsgHeader));

}



//---------------------------
// PatientDataUIHandler::end_of_breath_data    
//---------------------------
bool PatientDataUIHandler::end_of_breath_data (boost::shared_ptr<Message> pMessage, void *pUser)
{


    BreathDataPacket *bdPktPtr = (BreathDataPacket *)
        check_header(pMessage, "END_OF_BREATH_DATA", sizeof(BreathDataPacket));

    if(bdPktPtr==NULL)
    {
        return false;
    }
    

    breathType           .updatePacket(&(bdPktPtr->breathType             ));
    exhaledTidalVolume   .updatePacket(&(bdPktPtr->exhaledTidalVolume     ));
    breathDuration       .updatePacket(&(bdPktPtr->breathDuration         ));
    endExpiratoryPressure.updatePacket(&(bdPktPtr->endExpiratoryPressure  ));
    ieRatio              .updatePacket(&(bdPktPtr->ieRatio                ));
    mandFraction         .updatePacket(&(bdPktPtr->mandFraction           ));
    dynamicCompliance    .updatePacket(&(bdPktPtr->dynamicCompliance      ));
    dynamicResistance    .updatePacket(&(bdPktPtr->dynamicResistance      ));
    phaseType            .updatePacket(&(bdPktPtr->phaseType              ));
    previousBreathType   .updatePacket(&(bdPktPtr->previousBreathType     ));
    breathTypeDisplay    .updatePacket(&(bdPktPtr->breathTypeDisplay      ));
    spontInspTime        .updatePacket(&(bdPktPtr->spontInspTime          ));
    spontTiTtotRatio     .updatePacket(&(bdPktPtr->spontTiTtotRatio       ));
    peakExpiratoryFlow   .updatePacket(&(bdPktPtr->peakExpiratoryFlow     ));
    endExpiratoryFlow    .updatePacket(&(bdPktPtr->endExpiratoryFlow      ));
	percentLeak    .updatePacket(&(bdPktPtr->percentLeak      ));
	inspLeakVol    .updatePacket(&(bdPktPtr->inspLeakVol      ));
	exhLeakRate    .updatePacket(&(bdPktPtr->exhLeakRate      ));
	endExpiratoryFlow    .updatePacket(&(bdPktPtr->endExpiratoryFlow      ));

    ((SocketClient *)pUser)->send( (char *)((pMessage.get()->get() ).get()), pMessage.get()->getLength());

    breathType           .updateValue(Enum::convert(bdPktPtr->breathType));
    exhaledTidalVolume   .updateValue(Enum::convert(bdPktPtr->exhaledTidalVolume));   
    breathDuration       .updateValue(Enum::convert(bdPktPtr->breathDuration));            
    endExpiratoryPressure.updateValue(Enum::convert(bdPktPtr->endExpiratoryPressure));     
    ieRatio              .updateValue(Enum::convert(bdPktPtr->ieRatio));                   
    mandFraction         .updateValue(Enum::convert(bdPktPtr->mandFraction));              
    dynamicCompliance    .updateValue(Enum::convert(bdPktPtr->dynamicCompliance));         
    dynamicResistance    .updateValue(Enum::convert(bdPktPtr->dynamicResistance));        
    phaseType            .updateValue(Enum::convert(bdPktPtr->phaseType));            
    previousBreathType   .updateValue(Enum::convert(bdPktPtr->previousBreathType));        
    breathTypeDisplay    .updateValue(Enum::convert(bdPktPtr->breathTypeDisplay));         
    spontInspTime        .updateValue(Enum::convert(bdPktPtr->spontInspTime));             
    spontTiTtotRatio     .updateValue(Enum::convert(bdPktPtr->spontTiTtotRatio));          
    peakExpiratoryFlow   .updateValue(Enum::convert(bdPktPtr->peakExpiratoryFlow));        
    endExpiratoryFlow    .updateValue(Enum::convert(bdPktPtr->endExpiratoryFlow));

	percentLeak    .updateValue(Enum::convert(bdPktPtr->percentLeak));
	inspLeakVol    .updateValue(Enum::convert(bdPktPtr->inspLeakVol));
	exhLeakRate    .updateValue(Enum::convert(bdPktPtr->exhLeakRate));
	endExpiratoryFlow    .updateValue(Enum::convert(bdPktPtr->endExpiratoryFlow));

    return true;
}

//---------------------------
// PatientDataUIHandler::realtime_breath_data    
//---------------------------
bool PatientDataUIHandler::realtime_breath_data (boost::shared_ptr<Message> pMessage, void *pUser)
{

    RealTimeDataPacket *bdPktPtr = (RealTimeDataPacket *)
        check_header(pMessage, "REALTIME_BREATH_DATA", sizeof(RealTimeDataPacket));

    if(bdPktPtr==NULL)
    {
        return false;
    }

    breathType         .updatePacket(&(bdPktPtr->breathType            ));
    endInspPressure    .updatePacket(&(bdPktPtr->endInspPressure       ));
    phaseType          .updatePacket(&(bdPktPtr->phaseType             ));
    inspiredlungVolume .updatePacket(&(bdPktPtr->inspiredlungVolume    ));
    supportType        .updatePacket(&(bdPktPtr->supportType           ));
    mandType           .updatePacket(&(bdPktPtr->mandType              ));
    breathTypeDisplay  .updatePacket(&(bdPktPtr->breathTypeDisplay     ));
    peakCircuitPressure.updatePacket(&(bdPktPtr->peakCircuitPressure   ));
    spontInspTime      .updatePacket(&(bdPktPtr->spontInspTime         ));
    ventType           .updatePacket(&(bdPktPtr->ventType              ));
    breathTriggerId    .updatePacket(&(bdPktPtr->breathTriggerId       ));
    dynamicCompliance  .updatePacket(&(bdPktPtr->dynamicCompliance     ));
    dynamicResistance  .updatePacket(&(bdPktPtr->dynamicResistance     ));
    peakSpontInspFlow  .updatePacket(&(bdPktPtr->peakSpontInspFlow     ));

//TODO e600	c20ToCRatio.updatePacket(&(bdPktPtr->c20ToCRatio     ));

    ((SocketClient *)pUser)->send( (char *)((pMessage.get()->get() ).get()), pMessage.get()->getLength());

    breathType         .updateValue(Enum::convert(bdPktPtr->breathType)); 
    endInspPressure    .updateValue(Enum::convert(bdPktPtr->endInspPressure)); 
    phaseType          .updateValue(Enum::convert(bdPktPtr->phaseType));
    inspiredlungVolume .updateValue(Enum::convert(bdPktPtr->inspiredlungVolume)); 
    supportType        .updateValue(Enum::convert(bdPktPtr->supportType));        
    mandType           .updateValue(Enum::convert(bdPktPtr->mandType));
    breathTypeDisplay  .updateValue(Enum::convert(bdPktPtr->breathTypeDisplay));   
    peakCircuitPressure.updateValue(Enum::convert(bdPktPtr->peakCircuitPressure));      
    spontInspTime      .updateValue(Enum::convert(bdPktPtr->spontInspTime));
    ventType           .updateValue(Enum::convert(bdPktPtr->ventType)); 
    breathTriggerId    .updateValue(Enum::convert(bdPktPtr->breathTriggerId)); 
    dynamicCompliance  .updateValue(Enum::convert(bdPktPtr->dynamicCompliance));        
    dynamicResistance  .updateValue(Enum::convert(bdPktPtr->dynamicResistance));
    peakSpontInspFlow  .updateValue(Enum::convert(bdPktPtr->peakSpontInspFlow));   

//TODO e600	c20ToCRatio  .updateValue(Enum::convert(bdPktPtr->c20ToCRatio));   

    return true;
}

//---------------------------
// PatientDataUIHandler::averaged_breath_data    
//---------------------------
bool PatientDataUIHandler::averaged_breath_data (boost::shared_ptr<Message> pMessage, void *pUser)
{

    AveragedBDPacket *bdPktPtr = (AveragedBDPacket *)
        check_header(pMessage, "AVERAGE_BREATH_DATA", sizeof(AveragedBDPacket));

    if(bdPktPtr==NULL)
    {
        return false;
    }

    exhaledMinuteVol       .updatePacket(&(bdPktPtr->exhaledMinuteVol         ));
    exhaledSpontMinuteVol  .updatePacket(&(bdPktPtr->exhaledSpontMinuteVol    ));
    totalRespRate          .updatePacket(&(bdPktPtr->totalRespRate            ));
    meanCircuitPressure    .updatePacket(&(bdPktPtr->meanCircuitPressure      ));
    spontFtotVtRatio       .updatePacket(&(bdPktPtr->spontFtotVtRatio         ));

    ((SocketClient *)pUser)->send( (char *)((pMessage.get()->get() ).get()), pMessage.get()->getLength());

    exhaledMinuteVol      .updateValue(Enum::convert(bdPktPtr->exhaledMinuteVol));
    exhaledSpontMinuteVol .updateValue(Enum::convert(bdPktPtr->exhaledSpontMinuteVol));
    totalRespRate         .updateValue(Enum::convert(bdPktPtr->totalRespRate));
    meanCircuitPressure   .updateValue(Enum::convert(bdPktPtr->meanCircuitPressure));
    spontFtotVtRatio      .updateValue(Enum::convert(bdPktPtr->spontFtotVtRatio));



    return true;
}

//---------------------------
// PatientDataUIHandler::computed_insp_pause_data    
//---------------------------
bool PatientDataUIHandler::computed_insp_pause_data (boost::shared_ptr<Message> pMessage, void *pUser)
{

    ComputedInspPauseDataPacket *bdPktPtr = (ComputedInspPauseDataPacket *)
        check_header(pMessage, "COMPUTED_INSP_PAUSE_DATA", sizeof(ComputedInspPauseDataPacket));

    if(bdPktPtr==NULL)
    {
        return false;
    }

   

    staticCompliance                 .updatePacket(&(bdPktPtr->staticCompliance         ));
    staticComplianceValid            .updatePacket(&(bdPktPtr->staticComplianceValid    ));
    staticResistance                 .updatePacket(&(bdPktPtr->staticResistance         ));
    staticResistanceValid            .updatePacket(&(bdPktPtr->staticResistanceValid    ));


    ((SocketClient *)pUser)->send( (char *)((pMessage.get()->get() ).get()), pMessage.get()->getLength());


    staticCompliance      .updateValue(Enum::convert(bdPktPtr->staticCompliance));
    staticComplianceValid .updateValue(Enum::convert(bdPktPtr->staticComplianceValid));  
    staticResistance      .updateValue(Enum::convert(bdPktPtr->staticResistance));
    staticResistanceValid .updateValue(Enum::convert(bdPktPtr->staticResistanceValid)); 


    return true; 
}

//---------------------------
// PatientDataUIHandler::bilevel_mand_data    
//---------------------------
bool PatientDataUIHandler::bilevel_mand_data                   (boost::shared_ptr<Message> pMessage, void *pUser)
{

    BiLevelMandDataPacket  *bdPktPtr = (BiLevelMandDataPacket *)check_header(pMessage, "BILEVEL_MAND_DATA", sizeof(BiLevelMandDataPacket));

    if(bdPktPtr==NULL)
    {
        return false;
    }


    breathType                       .updatePacket(&(bdPktPtr->breathType             ));
    endExpiratoryPressure            .updatePacket(&(bdPktPtr->endExpiratoryPressure  ));
    peakCircuitPressure              .updatePacket(&(bdPktPtr->peakCircuitPressure    ));
    phaseType                        .updatePacket(&(bdPktPtr->phaseType              ));
    breathTypeDisplay                .updatePacket(&(bdPktPtr->breathTypeDisplay      ));


    ((SocketClient *)pUser)->send( (char *)((pMessage.get()->get() ).get()), pMessage.get()->getLength());

    breathType            .updateValue(Enum::convert(bdPktPtr->breathType            ));
    endExpiratoryPressure .updateValue(Enum::convert(bdPktPtr->endExpiratoryPressure ));
    peakCircuitPressure   .updateValue(Enum::convert(bdPktPtr->peakCircuitPressure   ));
    phaseType             .updateValue(Enum::convert(bdPktPtr->phaseType             ));
    breathTypeDisplay     .updateValue(Enum::convert(bdPktPtr->breathTypeDisplay     ));



    return true;
} 

//---------------------------
// PatientDataUIHandler::bilevel_mand_exh_data    
//---------------------------
bool PatientDataUIHandler::bilevel_mand_exh_data               (boost::shared_ptr<Message> pMessage, void *pUser)
{

    BiLevelMandExhDataPacket  *bdPktPtr = (BiLevelMandExhDataPacket *)check_header(pMessage, "BILEVEL_MAND_EXH_DATA", sizeof(BiLevelMandExhDataPacket));

    if(bdPktPtr==NULL)
    {
        return false;
    }


    breathType                       .updatePacket(&(bdPktPtr->breathType           ));        
    phaseType                        .updatePacket(&(bdPktPtr->phaseType            ));
    breathTypeDisplay                .updatePacket(&(bdPktPtr->breathTypeDisplay    ));
    peakCircuitPressure              .updatePacket(&(bdPktPtr->peakCircuitPressure  ));


    ((SocketClient *)pUser)->send( (char *)((pMessage.get()->get() ).get()), pMessage.get()->getLength());

    breathType          .updateValue(Enum::convert(bdPktPtr->breathType         ));
    phaseType           .updateValue(Enum::convert(bdPktPtr->phaseType          ));
    breathTypeDisplay   .updateValue(Enum::convert(bdPktPtr->breathTypeDisplay  ));
    peakCircuitPressure .updateValue(Enum::convert(bdPktPtr->peakCircuitPressure));



    return true;
} 

//---------------------------
// PatientDataUIHandler::pav_breath_data    
//---------------------------
bool PatientDataUIHandler::pav_breath_data                     (boost::shared_ptr<Message> pMessage, void *pUser)
{

    PavBreathDataPacket  *bdPktPtr = (PavBreathDataPacket *)check_header(pMessage, "PAV_BREATH_DATA", sizeof(PavBreathDataPacket));

    if(bdPktPtr==NULL)
    {
        return false;
    }

    patientWorkOfBreathing            .updatePacket(&(bdPktPtr->patientWorkOfBreathing   ));  
    totalWorkOfBreathing              .updatePacket(&(bdPktPtr->totalWorkOfBreathing     ));
    elastanceWob                      .updatePacket(&(bdPktPtr->elastanceWob             ));
    resistiveWob                      .updatePacket(&(bdPktPtr->resistiveWob             ));
    intrinsicPeep                     .updatePacket(&(bdPktPtr->intrinsicPeep            ));
    pavState                          .updatePacket(&(bdPktPtr->pavState                 ));



    ((SocketClient *)pUser)->send( (char *)((pMessage.get()->get() ).get()), pMessage.get()->getLength());


    patientWorkOfBreathing     .updateValue(Enum::convert(bdPktPtr->patientWorkOfBreathing  ));  
    totalWorkOfBreathing       .updateValue(Enum::convert(bdPktPtr->totalWorkOfBreathing    ));  
    elastanceWob               .updateValue(Enum::convert(bdPktPtr->elastanceWob            ));  
    resistiveWob               .updateValue(Enum::convert(bdPktPtr->resistiveWob            )); 
    intrinsicPeep              .updateValue(Enum::convert(bdPktPtr->intrinsicPeep           ));
    pavState                   .updateValue(Enum::convert(bdPktPtr->pavState                ));
    

    return true;
} 

//---------------------------
// PatientDataUIHandler::pav_plateau_data    
//---------------------------
bool PatientDataUIHandler::pav_plateau_data                    (boost::shared_ptr<Message> pMessage, void *pUser)
{

    PavPlateauDataPacket  *bdPktPtr = (PavPlateauDataPacket *)check_header(pMessage, "PAV_PLATEAU_DATA", sizeof(PavPlateauDataPacket));

    if(bdPktPtr==NULL)
    {
        return false;
    }


    lungElastance                       .updatePacket(&(bdPktPtr->lungElastance      ));     
    lungCompliance                      .updatePacket(&(bdPktPtr->lungCompliance     ));
    patientResistance                   .updatePacket(&(bdPktPtr->patientResistance  ));
    totalResistance                     .updatePacket(&(bdPktPtr->totalResistance    ));
    pavState                            .updatePacket(&(bdPktPtr->pavState           ));


    ((SocketClient *)pUser)->send( (char *)((pMessage.get()->get() ).get()), pMessage.get()->getLength());


    lungElastance      .updateValue(Enum::convert(bdPktPtr->lungElastance     )); 
    lungCompliance     .updateValue(Enum::convert(bdPktPtr->lungCompliance    ));
    patientResistance  .updateValue(Enum::convert(bdPktPtr->patientResistance ));  
    totalResistance    .updateValue(Enum::convert(bdPktPtr->totalResistance   ));  
    pavState           .updateValue(Enum::convert(bdPktPtr->pavState          ));



    return true;
} 

//---------------------------
// PatientDataUIHandler::pav_startup_data    
//---------------------------
bool PatientDataUIHandler::pav_startup_data                    (boost::shared_ptr<Message> pMessage, void *pUser)
{

    PavStartupDataPacket  *bdPktPtr = (PavStartupDataPacket *)check_header(pMessage, "PAV_STARTUP_DATA", sizeof(PavStartupDataPacket));

    if(bdPktPtr==NULL)
    {
        return false;
    }


    pavState                         .updatePacket(&(bdPktPtr->pavState      ));     


    ((SocketClient *)pUser)->send( (char *)((pMessage.get()->get() ).get()), pMessage.get()->getLength());

    pavState.updateValue(Enum::convert(bdPktPtr->pavState      ));


    return true;

} 

//---------------------------
// PatientDataUIHandler::fio2_for_gui_msg    
//---------------------------
bool PatientDataUIHandler::fio2_for_gui_msg (boost::shared_ptr<Message> pMessage, void *pUser)
{

    float *bdPktPtr = (float *)
        check_header(pMessage, "FI02_FOR_GUI_MSG", sizeof(float));

    if(bdPktPtr==NULL)
    {
        return false;
    }

    fio2_data                        .updatePacket(&(*bdPktPtr));     

//TODO
#if 1
    ((SocketClient *)pUser)->send( (char *)((pMessage.get()->get() ).get()), pMessage.get()->getLength());
#endif
    fio2_data.updateValue(Enum::convert(*bdPktPtr));


    return true;
}

//---------------------------
// PatientDataUIHandler::paused_peend_data   
//---------------------------
bool PatientDataUIHandler::paused_peend_data                   (boost::shared_ptr<Message> pMessage, void *pUser)
{

    float *bdPktPtr = (float *)
        check_header(pMessage, "PAUSE_PEEND_DATA", sizeof(float));

    if(bdPktPtr==NULL)
    {
        return false;
    }

    paused_peend                     .updatePacket(&(*bdPktPtr));     

    ((SocketClient *)pUser)->send( (char *)((pMessage.get()->get() ).get()), pMessage.get()->getLength());

    paused_peend.updateValue(Enum::convert(*bdPktPtr));

    return true;
}

//---------------------------
// PatientDataUIHandler::paused_piend_data   
//---------------------------
bool PatientDataUIHandler::paused_piend_data (boost::shared_ptr<Message> pMessage, void *pUser)
{

    float *bdPktPtr = (float *)check_header(pMessage, "PAUSE_PIEND_DATA", sizeof(float));

    if(bdPktPtr==NULL)
    {
        return false;
    }

    paused_piend                     .updatePacket(&(*bdPktPtr));

    ((SocketClient *)pUser)->send( (char *)((pMessage.get()->get() ).get()), pMessage.get()->getLength());

    paused_piend.updateValue(Enum::convert(*bdPktPtr));
    
    return true;
}

//---------------------------
// PatientDataUIHandler::nif_pressure_data  
//---------------------------
bool PatientDataUIHandler::nif_pressure_data                   (boost::shared_ptr<Message> pMessage, void *pUser)
{
    std::string event_sequence_id;

    float *bdPktPtr = (float *)check_header(pMessage, "NIF_PRESSURE_DATA", sizeof(float));

    if(bdPktPtr==NULL)
    {
        return false;
    }

    nif_pressure                     .updatePacket(&(*bdPktPtr));


    ((SocketClient *)pUser)->send( (char *)((pMessage.get()->get() ).get()), pMessage.get()->getLength());

    nif_pressure.updateValue(Enum::convert(*bdPktPtr));
    
    return true;

} 

//---------------------------
// PatientDataUIHandler::p100_pressure_data 
//---------------------------
bool PatientDataUIHandler::p100_pressure_data                  (boost::shared_ptr<Message> pMessage, void *pUser)
{
    std::string event_sequence_id;

    float *bdPktPtr = (float *)check_header(pMessage, "P100_PRESSURE_DATA", sizeof(float));

    if(bdPktPtr==NULL)
    {
        return false;
    }

    p100_pressure                    .updatePacket(&(*bdPktPtr)); 

    ((SocketClient *)pUser)->send( (char *)((pMessage.get()->get() ).get()), pMessage.get()->getLength());

    p100_pressure.updateValue(Enum::convert(*bdPktPtr));
    
    return true;

} 

//---------------------------
// PatientDataUIHandler::vital_capacity_data 
//---------------------------
bool PatientDataUIHandler::vital_capacity_data                 (boost::shared_ptr<Message> pMessage, void *pUser)
{
    std::string event_sequence_id;

    float *bdPktPtr = (float *)check_header(pMessage, "VITAL_CAPACITY_DATA", sizeof(float));

    if(bdPktPtr==NULL)
    {
        return false;
    }
    vital_capacity                   .updatePacket(&(*bdPktPtr)); 


    ((SocketClient *)pUser)->send( (char *)((pMessage.get()->get() ).get()), pMessage.get()->getLength());

    vital_capacity.updateValue(Enum::convert(*bdPktPtr));
    
    return true;

} 

//---------------------------
// PatientDataUIHandler::vtpcv_state_data 
//---------------------------
bool PatientDataUIHandler::vtpcv_state_data                    (boost::shared_ptr<Message> pMessage, void *pUser)
{

    VtpcvStateDataPacket  *bdPktPtr = (VtpcvStateDataPacket *)check_header(pMessage, "VTPCV_STATE_DATA", sizeof(VtpcvStateDataPacket));

    if(bdPktPtr==NULL)
    {
        return false;
    }

    vtpcvState                       .updatePacket(&(bdPktPtr->vtpcvState      )); 

    ((SocketClient *)pUser)->send( (char *)((pMessage.get()->get() ).get()), pMessage.get()->getLength());

    vtpcvState.updateValue(Enum::convert(bdPktPtr->vtpcvState     ));

    return true;
} 

//---------------------------
// PatientDataUIHandler::end_exp_pause_pressure_state_data
//---------------------------
bool PatientDataUIHandler::end_exp_pause_pressure_state_data        (boost::shared_ptr<Message> pMessage, void *pUser)
{

    PauseTypes::PpiState  *bdPktPtr = (PauseTypes::PpiState  *)check_header(pMessage, "END_EXP_PAUSE_PRESSURE_STATE", sizeof(PauseTypes::PpiState));

    if(bdPktPtr==NULL)
    {
        return false;
    }


    //sizeof int32
    


    ((SocketClient *)pUser)->send( (char *)((pMessage.get()->get() ).get()), pMessage.get()->getLength());

    //TODO
    //convert(*bdPktPtr)

    return true;
} 

//---------------------------
// PatientDataUIHandler::peep_recovery_msg
//---------------------------
bool PatientDataUIHandler::peep_recovery_msg                   (boost::shared_ptr<Message> pMessage, void *pUser)
{
    //Used by gui, nothing to do
 
    ((SocketClient *)pUser)->send( (char *)((pMessage.get()->get() ).get()), pMessage.get()->getLength());


    return true;
}          



//---------------------------
// PatientDataUIHandler::waveform_breath_data
//---------------------------

bool PatientDataUIHandler::waveform_breath_data                   (boost::shared_ptr<Message> pMessage, void *pUser)
{
  static unsigned int nt=0;
  static int iVal = 0;

  unsigned int size = 
        min(ntohs(((NetworkMsgHeader *)(( pMessage.get()->get() ).get()))->pktSize), 
                pMessage.get()->getLength()-sizeof(NetworkMsgHeader));



  WaveformData *pWaveform = (WaveformData *)check_header(pMessage, "WAVEFORM_BREATH_DATA", size);

  if(pWaveform == NULL)
  {
    //return false;
  }

  Uint  nEntries = size/sizeof(WaveformData);





  for (Uint32 i = 0; i< nEntries; i++)
  {

    WaveformCircuitPressure1 .updatePacket(&(pWaveform[i].circuitPressure) ); 
    WaveformNetFlow         .updatePacket(&(pWaveform[i].netFlow        ) ); 
    WaveformNetVolume       .updatePacket(&(pWaveform[i].netVolume      ) ); 
    WaveformLungPressure    .updatePacket(&(pWaveform[i].lungPressure   ) ); 
    WaveformLungFlow        .updatePacket(&(pWaveform[i].lungFlow       ) ); 
    WaveformLungVolume      .updatePacket(&(pWaveform[i].lungVolume     ) ); 
    WaveformBdPhase         .updatePacket(&(pWaveform[i].bdPhase        ) ); 
    WaveformBdPhase2        .updatePacket(&(pWaveform[i].bdPhase        ) ); 

	WaveformIsAutozeroActive         .updatePacket(&(pWaveform[i].isAutozeroActive  ) ); 


    g_uiTime++;

  }
 

    ((SocketClient *)pUser)->send( (char *)((pMessage.get()->get() ).get()), pMessage.get()->getLength());


  for (Uint32 i = 0; i< nEntries; i++)
  {
    WaveformCircuitPressure1 .updateValue(Enum::convert(pWaveform[i].circuitPressure ));
    WaveformNetFlow         .updateValue(Enum::convert(pWaveform[i].netFlow         ));
    WaveformNetVolume       .updateValue(Enum::convert(pWaveform[i].netVolume       ));
    WaveformLungPressure    .updateValue(Enum::convert(pWaveform[i].lungPressure    ));
    WaveformLungFlow        .updateValue(Enum::convert(pWaveform[i].lungFlow        ));
    WaveformLungVolume      .updateValue(Enum::convert(pWaveform[i].lungVolume      ));
    WaveformBdPhase         .updateValue(Enum::convert(pWaveform[i].bdPhase         ));
    WaveformBdPhase2         .updateValue(Enum::convert(pWaveform[i].bdPhase         ));

	WaveformIsAutozeroActive         .updateValue(Enum::convert(pWaveform[i].isAutozeroActive         ));
  }

    return true;
}          




//---------------------------
// PatientDataUIHandler::register_types
//---------------------------
bool PatientDataUIHandler::register_types(IEventLogger *pIEventLogger, void *pCDatabase )
{

   pIEventLogger->addMessageHandler(REALTIME_BREATH_DATA               ,realtime_breath_data,        pCDatabase);

    pIEventLogger->addMessageHandler(AVERAGED_BREATH_DATA               ,averaged_breath_data,        pCDatabase);

    pIEventLogger->addMessageHandler(END_OF_BREATH_DATA                 ,end_of_breath_data,          pCDatabase);

    pIEventLogger->addMessageHandler(WAVEFORM_BREATH_DATA         ,waveform_breath_data,        pCDatabase);     

#if 1


    pIEventLogger->addMessageHandler(COMPUTED_INSP_PAUSE_DATA           ,computed_insp_pause_data,    pCDatabase);   

    pIEventLogger->addMessageHandler(BILEVEL_MAND_DATA                  ,bilevel_mand_data,           pCDatabase);   
    pIEventLogger->addMessageHandler(BILEVEL_MAND_EXH_DATA              ,bilevel_mand_exh_data,       pCDatabase);   

    pIEventLogger->addMessageHandler(PAV_BREATH_DATA                    ,pav_breath_data,             pCDatabase);   
    pIEventLogger->addMessageHandler(PAV_PLATEAU_DATA                   ,pav_plateau_data,            pCDatabase);   
    pIEventLogger->addMessageHandler(PAV_STARTUP_DATA                   ,pav_startup_data,            pCDatabase);   

    pIEventLogger->addMessageHandler(FIO2_FOR_GUI_MSG                   ,fio2_for_gui_msg,            pCDatabase);

    pIEventLogger->addMessageHandler(NIF_PRESSURE_DATA                  ,nif_pressure_data,           pCDatabase);   
    pIEventLogger->addMessageHandler(P100_PRESSURE_DATA                 ,p100_pressure_data,          pCDatabase);   
    pIEventLogger->addMessageHandler(VITAL_CAPACITY_DATA                ,vital_capacity_data,         pCDatabase);   
    pIEventLogger->addMessageHandler(PAUSED_PIEND_DATA                  ,paused_piend_data,           pCDatabase); 
    pIEventLogger->addMessageHandler(PAUSED_PEEND_DATA                  ,paused_peend_data,           pCDatabase);   


    pIEventLogger->addMessageHandler(END_EXP_PAUSE_PRESSURE_STATE       ,end_exp_pause_pressure_state_data
                                                                                                     ,pCDatabase);

    pIEventLogger->addMessageHandler(VTPCV_STATE_DATA                   ,vtpcv_state_data,            pCDatabase);   

    pIEventLogger->addMessageHandler(PEEP_RECOVERY_MSG                  ,peep_recovery_msg,           pCDatabase);   
#endif
    return true;

}

//---------------------------
// getPatientDataSetInstance    
//---------------------------
extern "C"{
    __declspec(dllexport) IPatientDataSet * __cdecl getPatientDataSetInstance()
    {
        return IPatientDataSet::getInstance();

    }
}

