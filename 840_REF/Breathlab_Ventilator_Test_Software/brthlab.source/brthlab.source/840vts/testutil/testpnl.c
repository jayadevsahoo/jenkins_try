/*****************************************************************************/
/*   Module Name:   testpnl.c	                                          
/*   Purpose:     Functions for controling and modifying the test panel	                                             
/*                                                                           
/*   Requirements:   This module implements requirements detailed in         
/*          <75359-93>                                                       
/*
/*   Modification History:
/*   
/*   Description of Change			        By			      Date 
/*   ---------------------					-----   		  ------
/*   Removed the Metabolic Port Test.       Rhomere Jimenez   10 Oct,2007
/* 
/*   TestExecutive related code is	 		Hamsavally(CGS)	  10 Oct,2000
/*   removed and replaced corresponding
/*   functionality.
/*
/*	 Critical Observation during Phase1 	Kathirvel(CGS)    27 Sep,2000 
/* 	 is incorportated.						
/*
/*   Set command button color to            Dave Richardson   21 Nov, 2002
/*    transparent to make square outline 
/*    of rectangular region around round
/*    buttons disappear
/*
/*   $Revision::   1.2      $                                                
/*                                                                          
/*   $Log:   //pleasa1apo2/swqa/data/arc/840_CPVT_VTS/Program/testutil/testpnl.c-arc  $
 * 
 *    Rev 1.2   04 Dec 2002 15:36:48   Buchman
 * 840 VTS build done
 * 
 *    Rev 1.1   21 Nov 2002 11:04:52   Richardson
 * Updated per SCR # 24

/*****************************************************************************/

/*****************************************************************************
THIS IS A PROPRIETARY WORK TO WHICH PURITAN-BENNETT CORPORATION CLAIMS
EXCLUSIVE RIGHT.  NO PART OF THIS WORK MAY BE USED, DISCLOSED, REPRODUCED,
STORED IN AN INFORMATION RETIEVAL SYSTEM, OR TRANSMITTED, IN ANY FORM OR BY ANY
MEANS ELECTRONIC, MECHANICAL, PHOTOCOPYING, RECORDING OR OTHERWISE WITHOUT THE
PRIOR WRITTEN PERMISSION OF PURITAN-BENNETT CORPORATION.
 
Copyright (c) 1999-2002, Puritan-Bennett
All rights reserved.
*******************************************************************************/

/***********************************************/
/****************** INCLUDES *******************/
/***********************************************/

#include <ansi_c.h>
#include <formatio.h>
#include <toolbox.h> // SetBOLE(int nBreakState)
#include <userint.h>
#include "common.h"
#include "cpvtmain.h"
#include "ldb.h"
#include "MsgPopup.h"
#include "RT_BMPs.h"
#include "sysmenu.h"
#include "testpnl.h"
#include "testuir.h"

int ResArray[15]={	TESTPANEL_RESULT_1,
					TESTPANEL_RESULT_2,
					TESTPANEL_RESULT_3,
					TESTPANEL_RESULT_4,
					TESTPANEL_RESULT_5,
					TESTPANEL_RESULT_6,
					TESTPANEL_RESULT_7,
					TESTPANEL_RESULT_8,
					TESTPANEL_RESULT_9,
					TESTPANEL_RESULT_10,
					TESTPANEL_RESULT_11,
					TESTPANEL_RESULT_12};

int ChkXArray[15]={	TESTPANEL_PICTURE_1,
					TESTPANEL_PICTURE_2,
					TESTPANEL_PICTURE_3,
					TESTPANEL_PICTURE_4,
					TESTPANEL_PICTURE_5,
					TESTPANEL_PICTURE_6,
					TESTPANEL_PICTURE_7,
					TESTPANEL_PICTURE_8,
					TESTPANEL_PICTURE_9,
					TESTPANEL_PICTURE_10,
					TESTPANEL_PICTURE_11,
					TESTPANEL_PICTURE_12};

int ChkXDefArray[15]={	TESTPANEL_PICTURE_1,
					TESTPANEL_PICTURE_2,
					TESTPANEL_PICTURE_3,
					TESTPANEL_PICTURE_4,
					TESTPANEL_PICTURE_5,
					TESTPANEL_PICTURE_6,
					TESTPANEL_PICTURE_7,
					TESTPANEL_PICTURE_8,
					TESTPANEL_PICTURE_9,
					TESTPANEL_PICTURE_10,
					TESTPANEL_PICTURE_11,
					TESTPANEL_PICTURE_12};


/***********************************************/
/************ FUNCTION DECLARATION  ************/
/***********************************************/

//void UpdateTestListing(int index,int status);
//void DisplayTestPanel(void);  

/*****************************************************************************/
/* Name:    DisplayTestPanel
/*
/* Purpose:  The function is used to display the test panel. It makes a DisplayPanel call,
/*			and then sets the active panel to the test panel so that hot keys for start
/*			test etc are recognized. This is required since the system panel might
/*			have the active focus and the hot keys will not be recognized.
/*                                          
/*                                                                           
/* Example Call:   void DisplayTestPanel(void);
/*
/* Input:    <None>
/*
/* Output:   <None>
/*
/* Globals Changed:
/*           <None>
/*
/* Globals Used:
/*
/*
/* Return:    <None>
/*
/*****************************************************************************/
void DisplayTestPanel(void)
{
	int nBreakOnLibErrsFlag; // flag to break/notbreak on library errors

	DisplayPanel(gPanelHandle[TESTPANEL]);

	nBreakOnLibErrsFlag = SetBOLE(FALSE); // do not break if another panel is modal
	SetActivePanel(gPanelHandle[TESTPANEL]);
	SetBOLE(nBreakOnLibErrsFlag); // restore previous nBreakOnLibErrsFlag

	AutoScrollResults();

	return;
}


/*****************************************************************************/
/* Name:      SetCheckXTest                                                      
/*                                                                           
/* Purpose:  Set the iconic pass/fail result for the
/*      specifed test index.   
/*                                                                           
/* Example Call:   void SetCheckXTest(int index, int state);                                    
/*                                                                           
/* Input:    index - index of the test to set the icon for
/*			 state - which state to set it to
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   None                                                        
/*                                                                           
/*****************************************************************************/
void SetCheckXTest(int index, int state)
{
	int numctrls;

	GetPanelAttribute(gPanelHandle[TESTPANEL], ATTR_NUM_CTRLS, &numctrls);
	switch(state){
		case CX_CLEAR:		 
			DeleteImage(gPanelHandle[TESTPANEL], ChkXArray[index]);
			break;
		case CX_PASS:
			setButtonBMP(gPanelHandle[TESTPANEL], ChkXArray[index], BMP_Check);
			break;
		case CX_FAIL:
			setButtonBMP(gPanelHandle[TESTPANEL], ChkXArray[index], BMP_X);
			break;
	}
	SetCtrlAttribute(gPanelHandle[TESTPANEL], ChkXArray[index], ATTR_FRAME_COLOR, VAL_TRANSPARENT);

	if(state != CX_CLEAR){
		SetCtrlAttribute(gPanelHandle[TESTPANEL], ChkXArray[index], ATTR_ZPLANE_POSITION, 0);
	}
	else{
		SetCtrlAttribute(gPanelHandle[TESTPANEL], ChkXArray[index], ATTR_ZPLANE_POSITION, numctrls - 1);
	}

	return;
}


/*****************************************************************************/
/* Name:     SystemWait                                                     
/*                                                                           
/* Purpose:  wait for an event on the specified control
/*                                          
/*                                                                           
/* Example Call:   void SystemWait(int panel, int control);                                    
/*                                                                           
/* Input:    panel - parent panel handle of control to wait on
/*			 control - id of control to wait on
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
void SystemWait(int panel, int control)
{
	int hitcontrol, hitpanel;

	while(1){
		GetUserEvent(1, &hitpanel, &hitcontrol);
		if(control == hitcontrol && panel == hitpanel){
			break;
		}
	}

	return;
}


/*****************************************************************************/
/* Name:    SetSystemStatus                                                      
/*                                                                           
/* Purpose:  To post status ofthe test system  
/*                                                                           
/* Example Call:   SetSystemStatus(PASSED);                                        
/*                                                                           
/* Input:    int value defined for the message           
/*                                                                           
/* Output:   none                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
static int SysStatusPanel;
void SetSystemStatus(int msg)
{
	switch(msg){
		case BLANK:
			SetCtrlAttribute(gPanelHandle[TESTPANEL],TESTPANEL_STATUS_ACK,   ATTR_VISIBLE,      FALSE);
			SetCtrlAttribute(gPanelHandle[TESTPANEL],TESTPANEL_SYSTEMSTATUS, ATTR_TEXT_BGCOLOR, VAL_DK_CYAN);
			SetCtrlAttribute(gPanelHandle[TESTPANEL],TESTPANEL_SYSTEMSTATUS, ATTR_TEXT_COLOR,   VAL_BLACK);
			InsertTextBoxLine(gPanelHandle[TESTPANEL], TESTPANEL_SYSTEMSTATUS, 0, "");
			DisplayTestPanel();
			break;

		case PASSED:
			SysStatusPanel = LoadPanel(0, LDBf(Ldb_MainpanelFile), SYSSTATPNL);
		//	SetPanelAttribute(SysStatusPanel, ATTR_TITLE_BACKCOLOR, VAL_GREEN);
			SetCtrlAttribute(SysStatusPanel, SYSSTATPNL_DONE,      ATTR_VISIBLE,    TRUE);
			SetCtrlAttribute(SysStatusPanel, SYSSTATPNL_DONE,      ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT);
			SetCtrlAttribute(SysStatusPanel, SYSSTATPNL_TEXT_BOX1, ATTR_VISIBLE,    TRUE);
			SetCtrlAttribute(SysStatusPanel, SYSSTATPNL_TEXT_BOX1, ATTR_TEXT_COLOR, VAL_DK_GREEN);
			ResetTextBox(SysStatusPanel, SYSSTATPNL_TEXT_BOX1, "");
			InsertTextBoxLine(SysStatusPanel, SYSSTATPNL_TEXT_BOX1, 0, LDBf(Ldb_PASSED));
			SetSysPanelMessage("");
			InstallPopup(SysStatusPanel);
			SystemWait(SysStatusPanel, SYSSTATPNL_DONE);
			RemovePopup(0);
			break;

		case PFAIL:
			SysStatusPanel = LoadPanel(0, LDBf(Ldb_MainpanelFile), SYSSTATPNL);
		//	SetPanelAttribute(SysStatusPanel, ATTR_TITLE_BACKCOLOR, VAL_RED);
			SetCtrlAttribute(SysStatusPanel, SYSSTATPNL_DONE,      ATTR_VISIBLE,    TRUE);
			SetCtrlAttribute(SysStatusPanel, SYSSTATPNL_DONE,      ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT);
			SetCtrlAttribute(SysStatusPanel, SYSSTATPNL_TEXT_BOX1, ATTR_VISIBLE,    TRUE);
			SetCtrlAttribute(SysStatusPanel, SYSSTATPNL_TEXT_BOX1, ATTR_TEXT_COLOR, VAL_DK_RED);
			ResetTextBox(SysStatusPanel, SYSSTATPNL_TEXT_BOX1, "");
			InsertTextBoxLine(SysStatusPanel, SYSSTATPNL_TEXT_BOX1, 0, LDBf(Ldb_FAILED));
			SetSysPanelMessage("");
			InstallPopup(SysStatusPanel);
			SystemWait(SysStatusPanel, SYSSTATPNL_DONE);
			RemovePopup(0);
			break;

		case PABORT:
			SysStatusPanel = LoadPanel(0, LDBf(Ldb_MainpanelFile), SYSSTATPNL);
		//	SetPanelAttribute(SYSSTATPNL, ATTR_TITLE_BACKCOLOR, VAL_RED);
			SetCtrlAttribute(SysStatusPanel, SYSSTATPNL_DONE,      ATTR_VISIBLE,    TRUE);
			SetCtrlAttribute(SysStatusPanel, SYSSTATPNL_DONE,      ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT);
			SetCtrlAttribute(SysStatusPanel, SYSSTATPNL_TEXT_BOX1, ATTR_VISIBLE,    TRUE);
			SetCtrlAttribute(SysStatusPanel, SYSSTATPNL_TEXT_BOX1, ATTR_TEXT_COLOR, VAL_DK_RED);
			ResetTextBox(SysStatusPanel, SYSSTATPNL_TEXT_BOX1, "");
			InsertTextBoxLine(SysStatusPanel, SYSSTATPNL_TEXT_BOX1, 0, LDBf(Ldb_ABORT));
			SetSysPanelMessage("");
			InstallPopup(SysStatusPanel);
			SystemWait(SysStatusPanel, SYSSTATPNL_DONE);
			RemovePopup(0);
			break;
	} /* end case */

	return;
}


/*****************************************************************************/
/* Name:    AutoScrollResults
/*
/* Purpose:  needs description
/*                                          
/*                                                                           
/* Example Call:   void AutoScrollResults(void);
/*
/* Input:    <None>
/*
/* Output:   <None>
/*
/* Globals Changed:
/*           <None>
/*
/* Globals Used:
/*
/*
/* Return:    <None>
/*
/*****************************************************************************/
void AutoScrollResults(void)
{
	int lines, vislines;

	GetCtrlAttribute(gPanelHandle[TESTPANEL], TESTPANEL_TESTDISPLAY, ATTR_TOTAL_LINES,   &lines);
	GetCtrlAttribute(gPanelHandle[TESTPANEL], TESTPANEL_TESTDISPLAY, ATTR_VISIBLE_LINES, &vislines);
	if(lines > vislines){
		SetCtrlAttribute(gPanelHandle[TESTPANEL], TESTPANEL_TESTDISPLAY, ATTR_FIRST_VISIBLE_LINE, lines - vislines - 1);
	}
	ProcessSystemEvents();

	return;
}


/*****************************************************************************/
/* Name:      ResultMenuClear                                                    
/*                                                                           
/* Purpose:  Callback which clears the results menu
/*                                          
/*                                                                           
/* Example Call:   void CVICALLBACK ResultMenuClear( int menuBarHandle,
/*     int menuItemID, void *callbackData, int panelHandle );                                    
/*                                                                           
/* Input:    standard menu callback parameters          
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
void CVICALLBACK ResultMenuClear( int menuBarHandle,
     int menuItemID, void *callbackData, int panelHandle )
{
	if(CPVTConfirmPopup(LDBf(Ldb_SureClearResults), LDBf(Ldb_SureClearResults), OK_CANCEL_DLG, NO_HELP)){
		ResetTextBox(gPanelHandle[TESTPANEL], TESTPANEL_TESTDISPLAY, "");
	}

	return;
}


/*****************************************************************************/
/* Name:      ResultMenuPrint                                                
/*                                                                           
/* Purpose:  Callback to clear results area
/*                                          
/*                                                                           
/* Example Call:   void CVICALLBACK ResultMenuPrint( int menuBarHandle,
/*    int menuItemID, void *callbackData, int panelHandle );                                    
/*                                                                           
/* Input:    standard menu callback parameters          
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
void CVICALLBACK ResultMenuPrint( int menuBarHandle,
     int menuItemID, void *callbackData, int panelHandle )
{
	char* buffer;
	int lines, linelen, i, buffsize, offset;

	buffsize = offset = 0;
	GetNumTextBoxLines(gPanelHandle[TESTPANEL], TESTPANEL_TESTDISPLAY, &lines);
	if(!lines){
		return;
	}
	for(i = 0; i < lines; i++){
		GetTextBoxLineLength(gPanelHandle[TESTPANEL], TESTPANEL_TESTDISPLAY, i, &linelen);
		buffsize += linelen + 1;
	}
	
	buffer = (char *)malloc(buffsize+lines); // Modified the statement due to Critical Observation.
	if(buffer == NULL){
 		CPVTMessagePopup(LDBf(Ldb_Error), LDBf(Ldb_Out_of_Memory), OK_ONLY_DLG, NO_HELP);
		return;
	}
	for(i = 0; i < lines; i++){
		GetTextBoxLine(      gPanelHandle[TESTPANEL], TESTPANEL_TESTDISPLAY, i, buffer + offset);
		GetTextBoxLineLength(gPanelHandle[TESTPANEL], TESTPANEL_TESTDISPLAY, i, &linelen);
		offset += (linelen + 1);
		if(offset > buffsize){
			CPVTMessagePopup(LDBf(Ldb_Error), LDBf(Ldb_Out_of_Memory), OK_ONLY_DLG, NO_HELP);
			free(buffer);
			return;
		}
		*(buffer + offset - 1) = '\n';
	}
	*(buffer + offset) = '\0';
	SetPrintAttribute(ATTR_PRINT_AREA_WIDTH,  19);
	SetPrintAttribute(ATTR_PRINT_AREA_HEIGHT, 25);
	SetPrintAttribute(ATTR_ORIENTATION, VAL_PORTRAIT);
	PrintTextBuffer(buffer, "");
	free(buffer);

	return;
}


/*****************************************************************************/
/* Name:    ResultMenuSave                                                    
/*                                                                           
/* Purpose:  callback to save results area
/*                                          
/*                                                                           
/* Example Call:   void CVICALLBACK ResultMenuSave( int menuBarHandle,
/*     int menuItemID, void *callbackData, int panelHandle );                                    
/*                                                                           
/* Input:    standard menu callback parameters        
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
void CVICALLBACK ResultMenuSave( int menuBarHandle,
     int menuItemID, void *callbackData, int panelHandle )
{
	int lines, i, length;
	char File[MAX_PATHNAME_LEN];
	int FileHandle;
	int SelecStat;
	char* buffer;

	GetNumTextBoxLines(gPanelHandle[TESTPANEL], TESTPANEL_TESTDISPLAY, &lines);
	if(lines){
		SelecStat = FileSelectPopup(".", "Results.txt", "", LDBf(Ldb_SaveAs), VAL_SAVE_BUTTON, 0, 0, 1, 1, File);
		switch(SelecStat){
			case(VAL_NO_FILE_SELECTED):
				return;
			case(VAL_EXISTING_FILE_SELECTED):
			case(VAL_NEW_FILE_SELECTED):
				FileHandle = OpenFile(File, VAL_WRITE_ONLY, VAL_TRUNCATE, VAL_ASCII);
				break;
		}
	}else{
		return;
	}

	for(i=0; i<lines; i++){
		GetTextBoxLineLength(gPanelHandle[TESTPANEL], TESTPANEL_TESTDISPLAY, i, &length);
		buffer = (char *)malloc(length + 1); // Modified the statement due to Critical Observation.
		GetTextBoxLine(      gPanelHandle[TESTPANEL], TESTPANEL_TESTDISPLAY, i, buffer);
		buffer[length] = '\n';
		WriteFile(FileHandle, buffer, length + 1);
	}
	CloseFile(FileHandle);

	return;
}


/*****************************************************************************/
/* Name:    DrawResultsOptMenu                                                  
/*                                                                           
/* Purpose:  Draws the results options menu. 
/*                                          
/*                                                                           
/* Example Call:   void DrawResultsOptMenu(void);                                    
/*                                                                           
/* Input:   <None>    
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
void DrawResultsOptMenu(void)
{
	int mouse_x, mouse_y;
	int BarHandle, MenuHandle;
	GetRelativeMouseState(gPanelHandle[TESTPANEL], 0, &mouse_x, &mouse_y, NULL, NULL, NULL);
	BarHandle = NewMenuBar(0);
	MenuHandle = NewMenu(BarHandle, "", -1);
	NewMenuItem(BarHandle, MenuHandle, LDBf(Ldb_Clear),  -1, 0, ResultMenuClear, 0);
	NewMenuItem(BarHandle, MenuHandle, LDBf(Ldb_SaveAs), -1, 0, ResultMenuSave,  0);
	NewMenuItem(BarHandle, MenuHandle, LDBf(Ldb_Print),  -1, 0, ResultMenuPrint, 0);
	RunPopupMenu(BarHandle, MenuHandle, gPanelHandle[TESTPANEL], mouse_y, mouse_x, 0, 0, 0, 0);
	DiscardMenuBar(BarHandle);

	return;
}

