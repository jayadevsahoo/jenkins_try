#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: NovRamManager - NOVRAM Manager.
//---------------------------------------------------------------------
//@ Interface-Description
//  This class provides a statis interface for all of the subsystems to
//  use.  All of the data items that are managed within NOVRAM have
//  access methods provided by this class.  This class also has methods
//  for initialization and verification.
//---------------------------------------------------------------------
//@ Rationale
//  This class provides an interface to this subsystem, without unduly
//  exposing the internals and dependencies.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class has no data items.
//---------------------------------------------------------------------
//@ Fault-Handling
//  none
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Persistent-Objects/vcssrc/NovRamManager.ccv   25.0.4.0   19 Nov 2013 14:18:50   pvcs  $
//
//@ Modification-Log
//
//  Revision: 026  By: mnr    Date: 04-Mar-2010   SCR Number: 6436
//  Project: PROX4
//  Description:
//      Novram max check enabled, printf removed.
//
//  Revision: 025  By: mnr    Date: 25-Feb-2010   SCR Number: 6436
//  Project: PROX4
//  Description:
//      PROX Firmware Revision and serial number related updates.
// 
//  Revision: 024  By:  rpr    Date:  27-Jan-2011    SCR Number: 6740 
//  Project:  XENA2
//  Description:
//		Only log clock syncs when the user intiates them. 
// 
//  Revision: 023  By:  rhj    Date:  04-Apr-2009    SCR Number: 6495 
//  Project:  840S2
//  Description:
//		Added setAllTestsUndefined() and added a new parameter for 
//      CalcOverallTestResult.
//
//  Revision: 022  By:  gdc    Date:  12-Dec-2008    SCR Number: 6068
//  Project:  840S
//  Description:
//		Removed SUN prototype code.
//
//  Revision: 021  By:  gdc    Date:  10-Dec-2008    SCR Number: 6377
//  Project:  840S
//  Description:
//	Removed DELTA project single screen support.
//
//  Revision: 020   By: gdc  Date:  27-Mar-2007    SCR Number: 6237
//  Project:  Trend
//  Description:
//      Added compact flash test status for Trending option.
//
//  Revision: 019   By: sah  Date:  25-Aug-2000    DR Number:  5753
//  Project:  Delta
//  Description:
//      Delta project-specific changes:
//      *  added a screen-configuration flag, for determining whether this
//         is a single-screen configuration, or not
//
//  Revision: 018   By: sah  Date:  07-Mar-2000    DR Number:  5677
//  Project:  NeoMode
//  Description:
//      Eliminated functionality tied to 'SIGMA_PSUEDO_OFFICIAL' build
//      flag.
//
//  Revision: 017   By: sah  Date:  25-Jan-2000    DR Number:  5424
//  Project:  NeoMode
//  Description:
//      Added access methods to the user-controlled options.
//
//  Revision: 016  By:  gdc    Date:  04-JAN-2000    DCS Number: 5588
//  Project:  NeoMode
//  Description:
//	Using TimeStamp::GetRNullTimeStamp() instead of global reference.
//
//  Revision: 015  By:  sah    Date:  29-Jul-1999    DCS Number: 5327
//  Project:  NeoMode
//  Description:
//	Added GUI development only code for locating static memory in
//      extended memory section, thereby allowing development downloads.
//
//  Revision: 014  By:  sah    Date:  11-Mar-1998    DCS Number: 5041
//  Project:  Sigma (R8027)
//  Description:
//	Added an error-code argument to 'VerifySelf()' so that more
//	information can be stored in the error diagnostics.  Also, added
//	new error reporting method, 'ReportAuxillaryInfo()', for
//	logging system information entries.
//
//  Revision: 013  By:  sah    Date:  10-Oct-1997    DCS Number: 2413
//  Project:  Sigma (R8027)
//  Description:
//	Fixed problem with 'OverrideAllSstTests()' not working following
//	a reset, by changing the way that the patient circuit type is
//	"overridden".  Instead of taking the "last-SST" circuit type
//	and storing it into the accepted setting values, I now do the
//	opposite.  Because every start-up, the GUI's accepted settings
//	are sent to the BD, the BD's "accepted" circuit type was being
//	overwritten by the GUI's.  Therefore, now I take the BD's
//	accepted circuit type, and I store it as the "last-SST" circuit
//	type.
//
//  Revision: 012  By:  sah    Date:  29-Sep-1997    DCS Number: 2525
//  Project:  Sigma (R8027)
//  Description:
//	Removed methods for accessing 'exhCalInProgressState_', and
//	Vent-Inop Test Info.
//
//  Revision: 011  By:  sah    Date:  24-Sep-1997    DCS Number: 2513
//  Project:  Sigma (R8027)
//  Description:
//	Eliminated pre-processor variables around the 'OverrideAll?stTests()'
//	for use by the Service Laptop.  Also, added new methods for clearing
//	out all of the diagnostic entries within the various diagnostic logs.
//
//  Revision: 010  By:  sah    Date:  16-Sep-1997    DCS Number: 2385 & 1861
//  Project:  Sigma (R8027)
//  Description:
//	Added new methods for accessing 'serviceModeState_',
//	and removed the methods for accessing 'exhCalInProgressState_'.
//	Also, added accessing methods for 'atmPressOffset_'.
//
//  Revision: 009  By:  sah    Date:  09-Sep-1997    DCS Number: 2339
//  Project:  Sigma (R8027)
//  Description:
//      Make the "Override SST" button work following a download (this
//	only affect DEVELOPMENT code).
//
//  Revision: 008  By:  sah    Date:  04-Sep-1997    DCS Number: 1923
//  Project:  Sigma (R8027)
//  Description:
//      Replacing the Communication Diagnostic Log with the new System
//	Information Log.
//
//  Revision: 007  By:  sah    Date:  04-Sep-1997    DCS Number: 2269
//  Project:  Sigma (R8027)
//  Description:
//      Added the activation of any callbacks registered for real-time
//	clock changes.
//
//  Revision: 006  By:  sah    Date:  09-Jul-1997    DCS Number: 2279
//  Project:  Sigma (R8027)
//  Description:
//      Added access to two new BD data items, via
//      'Get{Air,O2}FlowSensorOffset()' and
//      'Update{Air,O2}FlowSensorOffset()'.
//
//  Revision: 005  By:  sah    Date:  01-Jul-1997    DCS Number: 2066 & 2107
//  Project:  Sigma (R8027)
//  Description:
//	Moved '{Get,Update}LastSstPatientCctType()' from the GUI side to
//	the BD side, and replaced the 'IsExhCalInProgress()' and
//	'UpdateExhCalInProgressFlag()' with 'GetExhCalInProgressState()'
//	and 'UpdateExhCalInProgressState()'.
//
//  Revision: 004  By:  sah    Date:  23-Jun-1997    DCS Number: 2214
//  Project:  Sigma (R8027)
//  Description:
//	Fixed problem where overall result status logged in EST/SST
//	diagnostic log was not always the correct status, by changing
//	call to 'getResultOfCurrRun()' to 'getResultId()'.
//
//  Revision: 003  By:  sah    Date:  15-May-1997    DCS Number: 2112
//  Project:  Sigma (R8027)
//  Description:
//	Adding tracing of '[11076]'.
//
//  Revision: 002  By:  sah    Date:  03-Apr-1997    DCS Number: 1894
//  Project:  Sigma (R8027)
//  Description:
//      Add a mutual-exlusion semaphore to protect statically-allocated
//	arrays used by the 'Get*Entries()' methods.
//
//  Revision: 001  By:  sah    Date:  15-Mar-1997    DCS Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//	Initial version (Integration baseline).
//
//=====================================================================

#include "NovRamManager.hh"

#if defined(SIGMA_DEVELOPMENT)
#  include "Ostream.hh"
#endif // defined(SIGMA_DEVELOPMENT)
#include "MsgQueue.hh"

//@ Usage-Classes
#include "NovRamSemaphore.hh"
#include "NonVolatileMemory.hh"
#include "NovRamUpdateManager.hh"
#include "NovRamXaction.hh"
#include "Array_CodeLogEntry_MAX_CODE_LOG_ENTRIES.hh"
#include "Array_ResultTableEntry_MAX_TEST_ENTRIES.hh"
#include "Background.hh"
#include "UserOptions.hh"

#if defined(SIGMA_BD_CPU)
#	include "ProxManager.hh"
#	include "BreathMiscRefs.hh"
#endif

#if defined(SIGMA_GUI_CPU)
#  include "VolatileMemory.hh"
#  include "CriticalSection.hh"
#  include "IpcIds.hh"
#endif // defined(SIGMA_GUI_CPU)
#include "MemoryMap.hh"
#include "Post.hh"
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Static Data Definitions...
//
//=====================================================================

#if defined(SIGMA_GUI_CPU)

#  if defined(SIGMA_DEVELOPMENT)
     // for development only, locate this module's static data into
     // development unit's extended memory...
#    pragma options -NZstacks
#  endif //defined(SIGMA_DEVELOPMENT)

// NOTE:  these static data items are used by the various 'Get*LogEntries()'
//	  methods below, so as to use up less stack space (because these
//	  items are fairly large)...

static Uint  GuiCodeLogEntriesMemory_[
(sizeof(FixedArray(CodeLogEntry,MAX_CODE_LOG_ENTRIES)) + sizeof(Uint) - 1) /
  				sizeof(Uint)
				        ];

static FixedArray(CodeLogEntry,MAX_CODE_LOG_ENTRIES)&  RGuiCodeLogEntries_ =
*((FixedArray(CodeLogEntry,MAX_CODE_LOG_ENTRIES)*)::GuiCodeLogEntriesMemory_);

static Uint  BdCodeLogEntriesMemory_[
(sizeof(FixedArray(CodeLogEntry,MAX_CODE_LOG_ENTRIES)) + sizeof(Uint) - 1) /
  				sizeof(Uint)
				       ];

static FixedArray(CodeLogEntry,MAX_CODE_LOG_ENTRIES)&  RBdCodeLogEntries_ =
 *((FixedArray(CodeLogEntry,MAX_CODE_LOG_ENTRIES)*)::BdCodeLogEntriesMemory_);


// NOTE:  these static data items are used by the various 'Get*ResultEntries()'
//	  methods below, so as to use up less stack space (because these
//	  items are fairly large)...

static Uint  GuiResultEntriesMemory_[
(sizeof(FixedArray(ResultTableEntry,MAX_TEST_ENTRIES)) + sizeof(Uint) - 1) /
  				sizeof(Uint)
				        ];

static FixedArray(ResultTableEntry,MAX_TEST_ENTRIES)&  RGuiResultEntries_ =
*((FixedArray(ResultTableEntry,MAX_TEST_ENTRIES)*)::GuiResultEntriesMemory_);

static Uint  BdResultEntriesMemory_[
(sizeof(FixedArray(ResultTableEntry,MAX_TEST_ENTRIES)) + sizeof(Uint) - 1) /
  				sizeof(Uint)
				        ];

static FixedArray(ResultTableEntry,MAX_TEST_ENTRIES)&  RBdResultEntries_ =
*((FixedArray(ResultTableEntry,MAX_TEST_ENTRIES)*)::BdResultEntriesMemory_);

#endif // defined(SIGMA_GUI_CPU)


//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  IsNonVolatileAddr(addr)  [static]
//
//@ Interface-Description
//  Does the address given by 'addr' reside within the NovRamManager's
//  managed non-volatile memory?
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  none 
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

Boolean
NovRamManager::IsNonVolatileAddr(const void* addr)
{
  CALL_TRACE("IsNonVolatileAddr(addr)");

  // this MUST be a 'Byte' pointer due to the addition below...
  const Byte *const  P_NON_VOLATILE_MEMORY =
				(const Byte*)NonVolatileMemory::P_INSTANCE_;

  return(addr >= P_NON_VOLATILE_MEMORY  &&
  	 addr < (P_NON_VOLATILE_MEMORY + sizeof(NonVolatileMemory)));
}   // $[TI1] (TRUE)  $[TI2] (FALSE)


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  IsVolatileAddr(addr)  [static]
//
//@ Interface-Description
//  Does the address given by 'addr' reside within the NovRamManager's
//  managed volatile memory?
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  none 
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

Boolean
NovRamManager::IsVolatileAddr(const void* addr)
{
  CALL_TRACE("IsVolatileAddr(addr)");

#if defined(SIGMA_GUI_CPU)
  // this MUST be a 'Byte' pointer due to the addition below...
  const Byte *const  P_VOLATILE_MEMORY =
				(const Byte*)VolatileMemory::P_INSTANCE_;

  return(addr >= P_VOLATILE_MEMORY  &&
  	 addr < (P_VOLATILE_MEMORY + sizeof(VolatileMemory)));
#elif defined(SIGMA_BD_CPU)
  // no managed volatile memory on the BD...
  return(FALSE);
#endif // defined(SIGMA_GUI_CPU)
}   // $[TI1] (TRUE -- GUI only)  $[TI2] (FALSE)


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  Initialize()  [static]
//
//@ Interface-Description
//  Initialize this NOVRAM manager.  This initializes all of the data
//  items that reside in NOVRAM, and returns an initialization ID that
//  indicates whether any initialization failures occurred.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  (NonVolatileMemory::IsInitialized_)
//@ End-Method
//=====================================================================

NovRamStatus::InitializationId
NovRamManager::Initialize(void)
{
  CALL_TRACE("Initialize()");

  //===================================================================
  // Initialize non-volatile memory...
  //===================================================================

  AUX_CLASS_ASSERTION((sizeof(NonVolatileMemory) <=
  				NonVolatileMemory::MAX_BYTES_AVAILABLE_),
		      sizeof(NonVolatileMemory));

  NovRamStatus::InitializationId  initStatusId;

  // initialize the non-volatile memory block...
  initStatusId = NonVolatileMemory::P_INSTANCE_->initialize_();
  SAFE_CLASS_ASSERTION((NonVolatileMemory::IsInitialized_));

#if defined(SIGMA_GUI_CPU)
  //===================================================================
  // Initialize volatile memory...
  //===================================================================

  // initialize the volatile memory block...
  VolatileMemory::P_INSTANCE_->initialize_();
  SAFE_CLASS_ASSERTION((VolatileMemory::IsInitialized_));

  //===================================================================
  // Initialize static references...
  //===================================================================

  SAFE_CLASS_ASSERTION((sizeof(::GuiCodeLogEntriesMemory_) >=
		      sizeof(FixedArray(CodeLogEntry,MAX_CODE_LOG_ENTRIES))));
  new (&::RGuiCodeLogEntries_) FixedArray(CodeLogEntry,MAX_CODE_LOG_ENTRIES);

  SAFE_CLASS_ASSERTION((sizeof(::BdCodeLogEntriesMemory_) >=
		      sizeof(FixedArray(CodeLogEntry,MAX_CODE_LOG_ENTRIES))));
  new (&::RBdCodeLogEntries_)  FixedArray(CodeLogEntry,MAX_CODE_LOG_ENTRIES);


  SAFE_CLASS_ASSERTION((sizeof(::GuiResultEntriesMemory_) >=
		      sizeof(FixedArray(ResultTableEntry,MAX_TEST_ENTRIES))));
  new (&::RGuiResultEntries_) FixedArray(ResultTableEntry,MAX_TEST_ENTRIES);

  SAFE_CLASS_ASSERTION((sizeof(::BdResultEntriesMemory_) >=
		      sizeof(FixedArray(ResultTableEntry,MAX_TEST_ENTRIES))));
  new (&::RBdResultEntries_) FixedArray(ResultTableEntry,MAX_TEST_ENTRIES);

#endif // defined(SIGMA_GUI_CPU)
  
  return(initStatusId);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  VerifySelf(rErrorCode)  [static]
//
//@ Interface-Description
//  Verify the validity of all of the items under this class's control.
//  If all of the items are valid, 'NovRamStatus::ALL_ITEMS_VALID' is
//  returned.  If not an enumerator identifying the specific invalid
//  item is returned.
//
//  Also, if the NOVRAM memory is in an invalid state, a 16-bit error
//  code is returned in 'rErrorCode'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

NovRamStatus::VerificationId
NovRamManager::VerifySelf(Uint16& rErrorCode)
{
  CALL_TRACE("VerifySelf(rErrorCode)");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  // corruption information stored via other error codes (see
  // 'ReportAuxillaryInfo()'...
  rErrorCode = 0u;

  return(NonVolatileMemory::P_INSTANCE_->verifySelf_());
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SetEstTestToNotApplicable(estTestNum)  [static]
//
//@ Interface-Description
//  Set the status of test number 'estTestNum' to "NOT APPLICABLE",
//  because 'estTestNum' represents an EST test not done on this CPU.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

void
NovRamManager::SetEstTestToNotApplicable(const Uint estTestNum)
{
  CALL_TRACE("SetEstTestToNotApplicable(estTestNum)");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  NonVolatileMemory::P_INSTANCE_->estResultTable_.setToNotApplicable(
								estTestNum
								   );
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SetSstTestToNotApplicable(sstTestNum)  [static]
//
//@ Interface-Description
//  Set the status of test number 'sstTestNum' to "NOT APPLICABLE",
//  because 'sstTestNum' represents an SST test not done on this CPU.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

void
NovRamManager::SetSstTestToNotApplicable(const Uint sstTestNum)
{
  CALL_TRACE("SetSstTestToNotApplicable(sstTestNum)");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  NonVolatileMemory::P_INSTANCE_->sstResultTable_.setToNotApplicable(
								sstTestNum
								    );
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  RegisterPendingClockUpdate(isUserInitiated)  [static]
//
//@ Interface-Description
//  The real-time clock is about to be updated, therefore this method
//  will handle notifying the logs with the proper diagnostic event.
//
//  On the BD CPU, the 'isUserInitiated' parameter is used to determine
//  whether this clock change is due to the operator initiating this change,
//  or whether this is an automatic synchronization with the GUI's clock.
//
//  On the GUI CPU, the 'isUserInitiated' parameter is ignored.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//  Since this is ALWAYS supposed to be followed by a call to
//  'RegisterCompletedClockUpdate()', no update notification is needed
//  here.
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

void
NovRamManager::RegisterPendingClockUpdate(
#if defined(SIGMA_GUI_CPU)
					  const Boolean
#elif defined(SIGMA_BD_CPU)
					  const Boolean isUserInitiated
#endif // defined(SIGMA_GUI_CPU)
					 )
{
  CALL_TRACE("RegisterPendingClockUpdate(isUserInitiated)");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  DiagnosticCode::SystemEventId  systemEventId;

#if defined(SIGMA_BD_CPU)
  if (isUserInitiated)
  {   // $[TI1]
#endif // defined(SIGMA_BD_CPU)
    systemEventId = DiagnosticCode::REAL_TIME_CLOCK_UPDATE_PENDING;
#if defined(SIGMA_BD_CPU)
  }
  else
  {   // $[TI2]
    systemEventId = DiagnosticCode::BD_CLOCK_SYNC_PENDING;
  }
#endif // defined(SIGMA_BD_CPU)

  DiagnosticCode  clockUpdateDiag;

  clockUpdateDiag.setSystemEventCode(systemEventId);

#if defined(SIGMA_BD_CPU)
  if (isUserInitiated)
  {   // $[TI3] -- log this user-initiated change into these logs...
#endif // defined(SIGMA_BD_CPU)

	// log real-time clock events into System Info Log...
	NonVolatileMemory::P_INSTANCE_->systemInfoLog_.logDiagnostic(
							  clockUpdateDiag
								);
    NonVolatileMemory::P_INSTANCE_->systemDiagLog_.logDiagnostic(
							  clockUpdateDiag
								);
    NonVolatileMemory::P_INSTANCE_->estDiagLog_.logDiagnostic(
							  clockUpdateDiag
							     );
    NonVolatileMemory::P_INSTANCE_->sstDiagLog_.logDiagnostic(
							  clockUpdateDiag
							     );
#if defined(SIGMA_GUI_CPU)
    AlarmAction  clockUpdateAction;

    clockUpdateAction.systemLevelId = AlarmAction::CLOCK_UPDATE_PENDING;

    NonVolatileMemory::P_INSTANCE_->alarmHistoryLog_.logAlarmAction(
							  clockUpdateAction
								   );
#endif // defined(SIGMA_GUI_CPU)
#if defined(SIGMA_BD_CPU)
  }   // $[TI4] -- not a user-initiated change; 
#endif // defined(SIGMA_BD_CPU)
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  RegisterCompletedClockUpdate(isUserInitiated)  [static]
//
//@ Interface-Description
//  The real-time clock has just been updated, therefore this method
//  will handle notifying the logs with the proper diagnostic event.
//
//  On the BD CPU, the 'isUserInitiated' parameter is used to determine
//  whether this clock change is due to the operator initiating this change,
//  or whether this is an automatic synchronization with the GUI's clock.
//
//  On the GUI CPU, the 'isUserInitiated' parameter is ignored.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

void
NovRamManager::RegisterCompletedClockUpdate(
#if defined(SIGMA_GUI_CPU)
					    const Boolean
#elif defined(SIGMA_BD_CPU)
					    const Boolean isUserInitiated
#endif // defined(SIGMA_GUI_CPU)
					   )
{
  CALL_TRACE("RegisterCompletedClockUpdate(isUserInitiated)");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  DiagnosticCode::SystemEventId  systemEventId;

#if defined(SIGMA_BD_CPU)
  if (isUserInitiated)
  {   // $[TI1]
#endif // defined(SIGMA_BD_CPU)
    systemEventId = DiagnosticCode::REAL_TIME_CLOCK_UPDATE_DONE;
#if defined(SIGMA_BD_CPU)
  }
  else
  {   // $[TI2]
    systemEventId = DiagnosticCode::BD_CLOCK_SYNC_DONE;
  }
#endif // defined(SIGMA_BD_CPU)

  DiagnosticCode  clockUpdateDiag;

  clockUpdateDiag.setSystemEventCode(systemEventId);


#if defined(SIGMA_BD_CPU)
  if (isUserInitiated)
  {   // $[TI3] -- log this user-initiated change into these logs...
#endif // defined(SIGMA_BD_CPU)
	// log real-time clock events into System Info Log...
	NonVolatileMemory::P_INSTANCE_->systemInfoLog_.logDiagnostic(
							  clockUpdateDiag
								);
    NonVolatileMemory::P_INSTANCE_->systemDiagLog_.logDiagnostic(
							  clockUpdateDiag
								);
    NonVolatileMemory::P_INSTANCE_->estDiagLog_.logDiagnostic(
							  clockUpdateDiag
							     );
    NonVolatileMemory::P_INSTANCE_->sstDiagLog_.logDiagnostic(
							  clockUpdateDiag
							     );
#if defined(SIGMA_GUI_CPU)
    AlarmAction  clockUpdateAction;

    clockUpdateAction.systemLevelId = AlarmAction::CLOCK_UPDATE_DONE;

    NonVolatileMemory::P_INSTANCE_->alarmHistoryLog_.logAlarmAction(
							  clockUpdateAction
								   );

    // activate any callbacks that are registered for a change in the real-time
    // clock...
    NovRamUpdateManager::UpdateHappened(NovRamUpdateManager::REAL_TIME_CLOCK_UPDATE);
#endif // defined(SIGMA_GUI_CPU)
#if defined(SIGMA_BD_CPU)
  }   // $[TI4] -- not a user-initiated change; only log into System Info Log...
#endif // defined(SIGMA_BD_CPU)
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  LogSystemInformation(diagnosticCode)  [static]
//
//@ Interface-Description
//  Log the system information stored in the diagnostic code that is
//  given by 'diagnosticCode'.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

void
NovRamManager::LogSystemInformation(const DiagnosticCode& diagnosticCode)
{
  CALL_TRACE("LogSystemInformation(diagnosticCode)");

  NonVolatileMemory::P_INSTANCE_->systemInfoLog_.logDiagnostic(diagnosticCode);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  LogSystemDiagnostic(diagnosticCode)  [static]
//
//@ Interface-Description
//  Log the System diagnostic code that is identified by
//  'diagnosticCode'.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

void
NovRamManager::LogSystemDiagnostic(const DiagnosticCode& diagnosticCode)
{
  CALL_TRACE("LogSystemDiagnostic(diagnosticCode)");

  NonVolatileMemory::P_INSTANCE_->systemDiagLog_.logDiagnostic(
							  diagnosticCode
							      );
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  LogEstSstDiagnostic(diagnosticCode)  [static]
//
//@ Interface-Description
//  Log the EST/SST diagnostic code that is identified by 'diagnosticCode'.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

void
NovRamManager::LogEstSstDiagnostic(const DiagnosticCode& diagnosticCode)
{
  CALL_TRACE("LogEstSstDiagnostic(diagnosticCode)");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  if (diagnosticCode.getDiagCodeTypeId() ==
				   DiagnosticCode::EXTENDED_SELF_TEST_DIAG)
  {   // $[TI1]
    NonVolatileMemory::P_INSTANCE_->estDiagLog_.logDiagnostic(diagnosticCode);
  }
  else if (diagnosticCode.getDiagCodeTypeId() ==
				      DiagnosticCode::SHORT_SELF_TEST_DIAG)
  {   // $[TI2]
    NonVolatileMemory::P_INSTANCE_->sstDiagLog_.logDiagnostic(diagnosticCode);
  }
  else
  {
    // only EST/SST diagnostic codes to be logged via this method...
    AUX_CLASS_ASSERTION_FAILURE(diagnosticCode.getDiagCodeTypeId());
  }
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ClearSystemInformation()  [static]
//
//@ Interface-Description
//  Clear the system information log of all of its diagnostic entries.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

void
NovRamManager::ClearSystemInformation(void)
{
  CALL_TRACE("ClearSystemInformation()");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  NonVolatileMemory::P_INSTANCE_->systemInfoLog_.clearOutAllEntries();
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ClearSystemDiagnostic()  [static]
//
//@ Interface-Description
//  Clear the system diagnostic log of all of its diagnostic entries.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

void
NovRamManager::ClearSystemDiagnostic(void)
{
  CALL_TRACE("ClearSystemDiagnostic()");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  NonVolatileMemory::P_INSTANCE_->systemDiagLog_.clearOutAllEntries();
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ClearEstSstDiagnostic()  [static]
//
//@ Interface-Description
//  Clear the EST/SST diagnostic log of all of its diagnostic entries.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

void
NovRamManager::ClearEstSstDiagnostic(void)
{
  CALL_TRACE("ClearEstSstDiagnostic()");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  NonVolatileMemory::P_INSTANCE_->estDiagLog_.clearOutAllEntries();
  NonVolatileMemory::P_INSTANCE_->sstDiagLog_.clearOutAllEntries();
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetActiveBtatEvent(rBtatEvents)  [static]
//
//@ Interface-Description
//  Get the current list of active Background Events.  The current event
//  list is returned in 'rBtatEvents'.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

void
NovRamManager::GetActiveBtatEvents(BtatEventState& rBtatEvents)
{
  CALL_TRACE("GetActiveBtatEvents(rBtatEvents)");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  NonVolatileMemory::P_INSTANCE_->activeBtatEvents_.getData(rBtatEvents);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  UpdateActiveBtatEvents(newBtatEvents)  [static]
//
//@ Interface-Description
//  Store 'newBtatEvents' as the new state for the active Background Events.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

void
NovRamManager::UpdateActiveBtatEvents(const BtatEventState& newBtatEvents)
{
  CALL_TRACE("UpdateActiveBtatEvents(newBtatEvents)");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  NonVolatileMemory::P_INSTANCE_->activeBtatEvents_.updateData(newBtatEvents);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  HasBackgroundTestFailed()  [static]
//
//@ Interface-Description
//  Has a background test failed?
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

Boolean
NovRamManager::HasBackgroundTestFailed(void)
{
  CALL_TRACE("HasBackgroundTestFailed()");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  return(NonVolatileMemory::P_INSTANCE_->backgroundFailureFlag_.isTrue());
}   // $[TI1] (TRUE)  $[TI2] (FALSE)


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  UpdateBackgroundTestFailureFlag(newFlagValue)  [static]
//
//@ Interface-Description
//  Update the flag that indicates whether a background test has failed,
//  with the value given by 'newFlagValue'.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

void
NovRamManager::UpdateBackgroundTestFailureFlag(const Boolean newFlagValue)
{
  CALL_TRACE("UpdateBackgroundTestFailureFlag(newFlagValue)");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  NonVolatileMemory::P_INSTANCE_->backgroundFailureFlag_.updateValue(
  								newFlagValue
								   );
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  StartingEstTest(estTestNumber)  [static]
//
//@ Interface-Description
//  Process the starting of an individual EST test -- identified by
//  'estTestNumber'.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//  (estTestNumber <= ::MAX_EST_RESULTS)
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

void
NovRamManager::StartingEstTest(const Uint estTestNumber)
{
  CALL_TRACE("StartingEstTest(estTestNumber)");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));
  SAFE_AUX_CLASS_PRE_CONDITION((estTestNumber <= ::MAX_EST_RESULTS),
			       estTestNumber);

  NonVolatileMemory::P_INSTANCE_->estResultTable_.startingTest(estTestNumber);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  CompletedEstTest(estTestNumber)  [static]
//
//@ Interface-Description
//  Process the completion of EST.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//  (estTestNumber <= ::MAX_EST_RESULTS)
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

void
NovRamManager::CompletedEstTest(const Uint estTestNumber)
{
  CALL_TRACE("CompletedEstTest(estTestNumber)");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));
  SAFE_AUX_CLASS_PRE_CONDITION((estTestNumber <= ::MAX_EST_RESULTS),
			       estTestNumber);

  NonVolatileMemory::P_INSTANCE_->estResultTable_.completedTest(estTestNumber);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  RepeatingEstTest(estTestNumber)  [static]
//
//@ Interface-Description
//  Process the repeating of the EST test given by 'estTestNumber'.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//  (estTestNumber <= ::MAX_EST_RESULTS)
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

void
NovRamManager::RepeatingEstTest(const Uint estTestNumber)
{
  CALL_TRACE("RepeatingEstTest(estTestNumber)");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));
  SAFE_AUX_CLASS_PRE_CONDITION((estTestNumber <= ::MAX_EST_RESULTS),
			       estTestNumber);

  NonVolatileMemory::P_INSTANCE_->estResultTable_.repeatingTest(estTestNumber);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  CompletedEst()  [static]
//
//@ Interface-Description
//  Process the completion of EST.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//  $[11076] -- previously-recorded POST failures and uncorrected EST
//              failures are cleared by the successful completion of EST.
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

void
NovRamManager::CompletedEst(void)
{
  CALL_TRACE("CompletedEst()");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

#if defined(SIGMA_GUI_CPU)
  //=================================================================== 
  // Log the completion event into EST's diagnostic log...
  //=================================================================== 

  ResultEntryData  overallEstResultData;
  DiagnosticCode   estCompletionDiag;
  
  NovRamManager::GetOverallEstResult(overallEstResultData);

  estCompletionDiag.setExtendedSelfTestCode(overallEstResultData.getResultId(),
					    ::MAX_EST_RESULTS);

  NonVolatileMemory::P_INSTANCE_->estDiagLog_.logDiagnostic(
  							estCompletionDiag
							   );
#endif // defined(SIGMA_GUI_CPU)

  // clear the POST and Background failure flags, because we completed
  // EST...
  Post::SetPostFailed(FALSE);
  Post::SetBackgroundFailed(PASSED);
  Post::SetBdFailed(FALSE);
  Background::ResetActiveBtat();
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  EstOverridden()  [static]
//
//@ Interface-Description
//  Following the completion of EST, this method can be used to override
//  all of the minor failures that may have occurred in the last run.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

void
NovRamManager::EstOverridden(void)
{
  CALL_TRACE("EstOverridden()");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  NonVolatileMemory::P_INSTANCE_->estResultTable_.overrideFailures();
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetOverallEstResult(rOverallEstResult)  [static]
//
//@ Interface-Description
//  Return the overall result of the currently-running, or last EST.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

void
NovRamManager::GetOverallEstResult(ResultEntryData& rOverallEstResult)
{
  CALL_TRACE("GetOverallEstResult(rOverallEstResult)");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  FixedArray(ResultTableEntry,MAX_TEST_ENTRIES)  arrBdEstResults;

#if defined(SIGMA_GUI_CPU)
  CLASS_PRE_CONDITION((VolatileMemory::IsInitialized_));

  FixedArray(ResultTableEntry,MAX_TEST_ENTRIES)  arrGuiEstResults;

  NonVolatileMemory::P_INSTANCE_->estResultTable_.getCurrEntries(
							   arrGuiEstResults
							        );
  VolatileMemory::P_INSTANCE_->estResultTable_.getCurrEntries(
							   arrBdEstResults
							     );
#elif defined(SIGMA_BD_CPU)
  NonVolatileMemory::P_INSTANCE_->estResultTable_.getCurrEntries(
							   arrBdEstResults
							        );
#endif // defined(SIGMA_GUI_CPU)

  TestResultTable::CalcOverallTestResult(rOverallEstResult,
#if defined(SIGMA_GUI_CPU)
  					 arrGuiEstResults,
#endif // defined(SIGMA_GUI_CPU)
  					 arrBdEstResults,
					 ::MAX_EST_RESULTS,
				     NovRamUpdateManager::EST_RESULT_TABLE_UPDATE);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  StartingSstTest(sstTestNumber)  [static]
//
//@ Interface-Description
//  Process the starting of an individual SST test -- identified by
//  'sstTestNumber'.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//  (sstTestNumber <= ::MAX_SST_RESULTS)
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

void
NovRamManager::StartingSstTest(const Uint sstTestNumber)
{
  CALL_TRACE("StartingSstTest(sstTestNumber)");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));
  SAFE_AUX_CLASS_PRE_CONDITION((sstTestNumber <= ::MAX_SST_RESULTS),
			       sstTestNumber);

  NonVolatileMemory::P_INSTANCE_->sstResultTable_.startingTest(sstTestNumber);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  CompletedSstTest(sstTestNumber)  [static]
//
//@ Interface-Description
//  Process the completion of SST.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//  (sstTestNumber <= ::MAX_SST_RESULTS)
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

void
NovRamManager::CompletedSstTest(const Uint sstTestNumber)
{
  CALL_TRACE("CompletedSstTest(sstTestNumber)");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));
  SAFE_AUX_CLASS_PRE_CONDITION((sstTestNumber <= ::MAX_SST_RESULTS),
			       sstTestNumber);

  NonVolatileMemory::P_INSTANCE_->sstResultTable_.completedTest(	
							    sstTestNumber
							      );
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  RepeatingSstTest(sstTestNumber)  [static]
//
//@ Interface-Description
//  Process the repeating of the SST test given by 'sstTestNumber'.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//  (sstTestNumber <= ::MAX_SST_RESULTS)
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

void
NovRamManager::RepeatingSstTest(const Uint sstTestNumber)
{
  CALL_TRACE("RepeatingSstTest(sstTestNumber)");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));
  SAFE_AUX_CLASS_PRE_CONDITION((sstTestNumber <= ::MAX_SST_RESULTS),
			       sstTestNumber);

  NonVolatileMemory::P_INSTANCE_->sstResultTable_.repeatingTest(sstTestNumber);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  CompletedSst()  [static]
//
//@ Interface-Description
//  Process the completion of SST.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

void
NovRamManager::CompletedSst(void)
{
  CALL_TRACE("CompletedSst()");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

#if defined(SIGMA_GUI_CPU)
  //=================================================================== 
  // Log the completion event into SST's diagnostic log...
  //=================================================================== 

  ResultEntryData  overallSstResultData;
  DiagnosticCode   sstCompletionDiag;
  
  NovRamManager::GetOverallSstResult(overallSstResultData);

  sstCompletionDiag.setShortSelfTestCode(overallSstResultData.getResultId(),
					 ::MAX_SST_RESULTS);
 

  NonVolatileMemory::P_INSTANCE_->sstDiagLog_.logDiagnostic(
  							sstCompletionDiag
							   );
#endif // defined(SIGMA_GUI_CPU)
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SstOverridden()  [static]
//
//@ Interface-Description
//  Following the completion of SST, this method can be used to override
//  all of the minor failures that may have occurred in the last run.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

void
NovRamManager::SstOverridden(void)
{
  CALL_TRACE("SstOverridden()");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  NonVolatileMemory::P_INSTANCE_->sstResultTable_.overrideFailures();
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetOverallSstResult(rOverallSstResult)  [static]
//
//@ Interface-Description
//  Return the overall result of the currently-running, or last SST.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

void
NovRamManager::GetOverallSstResult(ResultEntryData& rOverallSstResult)
{
  CALL_TRACE("GetOverallSstResult(rOverallSstResult)");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  FixedArray(ResultTableEntry,MAX_TEST_ENTRIES)  arrBdSstResults;

#if defined(SIGMA_GUI_CPU)
  CLASS_PRE_CONDITION((VolatileMemory::IsInitialized_));

  FixedArray(ResultTableEntry,MAX_TEST_ENTRIES)  arrGuiSstResults;

  NonVolatileMemory::P_INSTANCE_->sstResultTable_.getCurrEntries(
							   arrGuiSstResults
							        );
  VolatileMemory::P_INSTANCE_->sstResultTable_.getCurrEntries(
							   arrBdSstResults
							     );
#elif defined(SIGMA_BD_CPU)
  NonVolatileMemory::P_INSTANCE_->sstResultTable_.getCurrEntries(
							   arrBdSstResults
							        );
#endif // defined(SIGMA_GUI_CPU)

  TestResultTable::CalcOverallTestResult(rOverallSstResult,
#if defined(SIGMA_GUI_CPU)
  					 arrGuiSstResults,
#endif // defined(SIGMA_GUI_CPU)
  					 arrBdSstResults,
					 ::MAX_SST_RESULTS,
					 NovRamUpdateManager::SST_RESULT_TABLE_UPDATE);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ArePatientSettingsAvailable()  [static]
//
//@ Interface-Description
//  Does the persistent store have patient settings that are available
//  for use?
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

Boolean
NovRamManager::ArePatientSettingsAvailable(void)
{
  CALL_TRACE("ArePatientSettingsAvailable()");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  return(NonVolatileMemory::P_INSTANCE_->patientSettingsAvailFlag_.isTrue());
}   // $[TI1] (TRUE)  $[TI2] (FALSE)...


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  UpdatePatientSettingsFlag(newFlagValue)  [static]
//
//@ Interface-Description
//  Update the flag that indicates whether the patient settings are
//  available for use, to the value given by 'newFlagValue'.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  (newFlagValue == NovRamManager::ArePatientSettingsAvailable())
//@ End-Method 
//===================================================================== 

void
NovRamManager::UpdatePatientSettingsFlag(const Boolean newFlagValue)
{
  CALL_TRACE("UpdatePatientSettingsFlag(newFlagValue)");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  NonVolatileMemory::P_INSTANCE_->patientSettingsAvailFlag_.updateValue(
								newFlagValue
								       );
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  AreBatchSettingsInitialized()  [static]
//
//@ Interface-Description
//  Does the persistent store have initialized batch setting values?
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

Boolean
NovRamManager::AreBatchSettingsInitialized(void)
{
  CALL_TRACE("AreBatchSettingsInitialized()");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  return(NonVolatileMemory::P_INSTANCE_->batchSettingsInitFlag_.isTrue());
}   // $[TI1] (TRUE)  $[TI2] (FALSE)...


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  UpdateBatchSettingsFlag(newFlagValue)  [static]
//
//@ Interface-Description
//  Update the flag that indicates whether the persistent store has
//  initialized batch setting values.  The flag is given the value indicated
//  by 'newFlagValue'.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  (newFlagValue == NovRamManager::AreBatchSettingsInitialized())
//@ End-Method 
//===================================================================== 

void
NovRamManager::UpdateBatchSettingsFlag(const Boolean newFlagValue)
{
  CALL_TRACE("UpdateBatchSettingsFlag(newFlagValue)");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  NonVolatileMemory::P_INSTANCE_->batchSettingsInitFlag_.updateValue(
							       newFlagValue
								    );
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetInspResistance(rInspCctResistance)  [static]
//
//@ Interface-Description
//  Return the inspiratory circuit's resistance calculated by the last SST.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (VolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none
//@ End-Method 
//===================================================================== 

void
NovRamManager::GetInspResistance(Resistance& rInspCctResistance)
{
  CALL_TRACE("GetInspResistance(rInspCctResistance)");

#if defined(SIGMA_GUI_CPU)
  CLASS_PRE_CONDITION((VolatileMemory::IsInitialized_));
  rInspCctResistance = VolatileMemory::P_INSTANCE_->inspCctResistance_;
#elif defined(SIGMA_BD_CPU)
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));
  NonVolatileMemory::P_INSTANCE_->inspCctResistance_.getData(
  							rInspCctResistance
							    );
#endif // defined(SIGMA_GUI_CPU)
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetExhResistance(rExhCctResistance)  [static]
//
//@ Interface-Description
//  Return the exhalation circuit's resistance calculated by the last SST.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (VolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none
//@ End-Method 
//===================================================================== 

void
NovRamManager::GetExhResistance(Resistance& rExhCctResistance)
{
  CALL_TRACE("GetExhResistance(rExhCctResistance)");

#if defined(SIGMA_GUI_CPU)
  CLASS_PRE_CONDITION((VolatileMemory::IsInitialized_));
  rExhCctResistance = VolatileMemory::P_INSTANCE_->exhCctResistance_;
#elif defined(SIGMA_BD_CPU)
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));
  NonVolatileMemory::P_INSTANCE_->exhCctResistance_.getData(
  							rExhCctResistance
							   );
#endif // defined(SIGMA_GUI_CPU)
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetUnitSerialNumber(rSerialNumber)  [static]
//
//@ Interface-Description
//  Return the serial number of this unit (BD or GUI), in 'rSerialNumber'.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (VolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none
//@ End-Method 
//===================================================================== 

void
NovRamManager::GetUnitSerialNumber(SerialNumber& rSerialNumber)
{
  CALL_TRACE("GetUnitSerialNumber(rSerialNumber)");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  NonVolatileMemory::P_INSTANCE_->unitSerialNum_.getData(rSerialNumber);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  UpdateUnitSerialNumber(serialNumber)  [static]
//
//@ Interface-Description
//  Update the stored serial number for the unit, to the value given
//  by 'serialNumber'.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none
//@ End-Method 
//===================================================================== 

void
NovRamManager::UpdateUnitSerialNumber(const SerialNumber& serialNumber)
{
  CALL_TRACE("UpdateUnitSerialNumber(serialNumber)");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  NonVolatileMemory::P_INSTANCE_->unitSerialNum_.updateData(serialNumber);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  OverrideAllEstTests()  [static]
//
//@ Interface-Description
//  Unconditionally, override the status of all of the EST tests, and
//  reset any flags that may force a service-required state.  Unlike the
//  override function provided at the end of EST, this will NOT result
//  in a diagnostic put into the EST/SST Diagnostic Log.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none
//@ End-Method 
//===================================================================== 

void
NovRamManager::OverrideAllEstTests(void)
{
  NonVolatileMemory::P_INSTANCE_->estResultTable_.overrideAllTests();

  // clear the POST and Background failure flags, because we completed
  // EST...
  Post::SetPostFailed(FALSE);
  Post::SetBackgroundFailed(PASSED);
  Post::SetBdFailed(FALSE);
  Background::ResetActiveBtat();
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  OverrideAllSstTests()  [static]
//
//@ Interface-Description
//  Unconditionally, override the status of all of the SST tests, and
//  reset any flags that may force an SST-required state.  Unlike the
//  override function provided at the end of SST, this will NOT result
//  in a diagnostic put into the EST/SST Diagnostic Log.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none
//@ End-Method 
//===================================================================== 

void 
NovRamManager::OverrideAllSstTests(void)
{
  NonVolatileMemory::P_INSTANCE_->sstResultTable_.overrideAllTests();

#  if defined(SIGMA_BD_CPU)
  BdSettingValues  acceptedSettingValues;

  NovRamManager::UpdateBatchSettingsFlag(TRUE);
  NovRamManager::GetAcceptedBatchSettings(acceptedSettingValues);

  // get the accepted patient circuit type value...
  const DiscreteValue  ACCEPTED_PATIENT_CCT_TYPE =
	  acceptedSettingValues.getDiscreteValue(SettingId::PATIENT_CCT_TYPE);

  // store as the "last-SST" value...
  NovRamManager::UpdateLastSstPatientCctType(ACCEPTED_PATIENT_CCT_TYPE);
#  endif // defined(SIGMA_BD_CPU)
}   // $[TI1]




//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  setAllTestsUndefined()  [static]
//
//@ Interface-Description
//  Sets all EST tests as undefined.  
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none
//@ End-Method 
//===================================================================== 

void
NovRamManager::SetAllTestsUndefined(void)
{
  CLASS_PRE_CONDITION(NonVolatileMemory::IsInitialized_);

  NonVolatileMemory::P_INSTANCE_->estResultTable_.setAllTestsUndefined();

}  


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ReportAuxillaryInfo(beginAddr, dataSize, statusId)  [static]
//
//@ Interface-Description
//  Store the passed in information into a log, for use in failure
//  analysis.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//  If this is the very first run, following a download, no errors are
//  logged.
//--------------------------------------------------------------------- 
//@ PreCondition
//  none 
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

void
NovRamManager::ReportAuxillaryInfo(const void *const  beginAddr,
				   const Uint         dataSize,
				   const Uint         statusId)
{
  CALL_TRACE("ReportAuxillaryInfo(beginAddr, dataSize, statusId)");
  UNUSED_SYMBOL(beginAddr);		

  if (Post::IsNovramInitialized())
  {   // $[TI1] -- NOVRAM is initialized, therefore report failure...
    // if initialization already completed, then this is a failure in the
    // SRAM part of NOVRAM, otherwise it's probably a failure of the EEPROM
    // part of NOVRAM...
    const DiagnosticCode::SystemEventId  EVENT_ID =
	  (NonVolatileMemory::IsInitialized_)
	   ? DiagnosticCode::APP_NOVRAM_ACCESS_FAILURE		// $[TI1.1]
	   : DiagnosticCode::APP_NOVRAM_RESTORE_FAILURE;	// $[TI1.2]

    AuxInfoCode_  auxInfoCode;

    // clear all fields...
    auxInfoCode.value = 0u;

    // store in values...
    auxInfoCode.layoutFields.statusId   = statusId;
    auxInfoCode.layoutFields.dataSize   = dataSize;
    auxInfoCode.layoutFields.addrOffset = 0;

    DiagnosticCode  diagCode;

    diagCode.setSystemEventCode(EVENT_ID, auxInfoCode.value);

    // log this diagnostic in the appropriate log...
	FaultHandler::LogDiagnosticCode(diagCode);
  }   // $[TI2] -- ignore "corrupt" NOVRAM when NOVRAM is first initialized...
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetUserOptions(rUserOptions)  [static]
//
//@ Interface-Description
//  Return -- via 'rUserOptions' -- the current state of the user-settable
//  software options.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none
//@ End-Method 
//===================================================================== 

void
NovRamManager::GetUserOptions(UserOptions& rUserOptions)
{
  CALL_TRACE("GetUserOptions(rUserOptions)");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  NonVolatileMemory::P_INSTANCE_->userOptions_.getData(rUserOptions);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  StoreUserOptions(userOptions)  [static]
//
//@ Interface-Description
//  Store 'userOptions' as the new state of the settable user software
//  option bits.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none
//@ End-Method 
//===================================================================== 

void
NovRamManager::StoreUserOptions(const UserOptions& userOptions)
{
  CALL_TRACE("StoreUserOptions(userOptions)");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  NonVolatileMemory::P_INSTANCE_->userOptions_.updateData(userOptions);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//            [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//  and 'lineNumber' are essential pieces of information.  The
//  'pFileName' and 'pPredicate' strings may be defaulted in the macro
//  to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//  none 
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

void
NovRamManager::SoftFault(const SoftFaultID  softFaultID,
			 const Uint32       lineNumber,
			 const char*        pFileName,
			 const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, PERSISTENT_OBJS, NOV_RAM_MANAGER,
                          lineNumber, pFileName, pPredicate);
}


#if defined(SIGMA_GUI_CPU)

//=====================================================================
//
//  GUI-only Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetSystemInfoLogEntries(rArrEntries)  [static]
//
//@ Interface-Description
//  Store into 'rArrEntries' the current entries from the System Information
//  Log.  The ordering of the entries within the returned array is such that
//  the first entry of the array is the entry that was entered last.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//  Using statically allocated memory for the "GUI" and "BD" entry arrays
//  because of the risk of stack overruns, if I use the stack.  The risk
//  is especially high due to the tremendous size of these arrays (about
//  3K each).
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_  &&  VolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

void
NovRamManager::GetSystemInfoEntries(
		  FixedArray(CodeLogEntry,MAX_CODE_LOG_ENTRIES)& rArrEntries
				   )
{
  CALL_TRACE("GetSystemInfoEntries(rArrEntries)");
  AUX_CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_  &&
			   VolatileMemory::IsInitialized_),
			  NonVolatileMemory::IsInitialized_);

  // protect usage of global code log entry arrays (used below)...
  CriticalSection  accessToGlobalArray(::NOVRAM_DIAG_LOG_ARRAY_MT);

  NonVolatileMemory::P_INSTANCE_->systemInfoLog_.getCurrEntries(
  						::RGuiCodeLogEntries_
							       );
  VolatileMemory::P_INSTANCE_->systemInfoLog_.getCurrEntries(
  						::RBdCodeLogEntries_
							    );

  DiagCodeLog::MergeCodeLogs(rArrEntries, ::RGuiCodeLogEntries_,
			     ::RBdCodeLogEntries_, ::MAX_SYS_INFO_LOG_ENTRIES);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetSystemLogEntries(rArrEntries)  [static]
//
//@ Interface-Description
//  Store into 'rArrEntries' the current entries from the System
//  Diagnostic Log.  The ordering of the entries within the returned
//  array is such that the first entry of the array is the entry that
//  was entered last.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//  Using statically allocated memory for the "GUI" and "BD" entry arrays
//  because of the risk of stack overruns, if I use the stack.  The risk
//  is especially high due to the tremendous size of these arrays (about
//  3K).
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_  &&  VolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

void
NovRamManager::GetSystemLogEntries(
		  FixedArray(CodeLogEntry,MAX_CODE_LOG_ENTRIES)& rArrEntries
				  )
{
  CALL_TRACE("GetSystemLogEntries()");
  AUX_CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_  &&
			   VolatileMemory::IsInitialized_),
			  NonVolatileMemory::IsInitialized_);

  // protect usage of global code log entry arrays (used below)...
  CriticalSection  accessToGlobalArray(::NOVRAM_DIAG_LOG_ARRAY_MT);

  NonVolatileMemory::P_INSTANCE_->systemDiagLog_.getCurrEntries(
						  ::RGuiCodeLogEntries_
							      );
  VolatileMemory::P_INSTANCE_->systemDiagLog_.getCurrEntries(
						  ::RBdCodeLogEntries_
						  	    );

  DiagCodeLog::MergeCodeLogs(rArrEntries, ::RGuiCodeLogEntries_,
			     ::RBdCodeLogEntries_, ::MAX_CODE_LOG_ENTRIES);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  HasGuiEstPassed()  [static]
//
//@ Interface-Description
//  Return a boolean indicating whether the EST tests performed on the
//  GUI have passed, or been overridden.  This provides a query of the
//  results of the GUI EST tests, such that this method can be called
//  before transitioning to the 'INIT' state of startup.  This is used
//  by the startup system to know if EST is required purely due to the
//  incompletion or failure of any of the EST tests that test items on
//  the GUI.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

Boolean
NovRamManager::HasGuiEstPassed(void)
{
  CALL_TRACE("HasGuiEstPassed()");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  FixedArray(ResultTableEntry,MAX_TEST_ENTRIES)  arrGuiEstResults;

  NonVolatileMemory::P_INSTANCE_->estResultTable_.getCurrEntries(
							arrGuiEstResults
							        );

  const Boolean  HAS_PASSED =
		    TestResultTable::HaveAllTestsPassed(arrGuiEstResults,
							::MAX_EST_RESULTS);

  return(HAS_PASSED);
}   // $[TI1] (TRUE)  $[TI2] (FALSE)


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetEstSstLogEntries()  [static]
//
//@ Interface-Description
//  Store into 'rArrEntries' the current entries from the EST/SST
//  Diagnostic Log.  The ordering of the entries within the returned
//  array is such that the first entry of the array is the entry that
//  was entered last.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//  Using statically allocated memory for the "GUI" and "BD" entry arrays,
//  and SST and EST entry arrays, because of the risk of stack overruns if
//  I use the stack.  The risk is especially high due to the tremendous size
//  of these arrays (about 2K).
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_  &&  VolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

void
NovRamManager::GetEstSstLogEntries(
		  FixedArray(CodeLogEntry,MAX_CODE_LOG_ENTRIES)& rArrEntries
		  		  )
{
  CALL_TRACE("GetEstSstLogEntries()");
  AUX_CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_  &&
			   VolatileMemory::IsInitialized_),
			  NonVolatileMemory::IsInitialized_);

  // protect usage of global code log entry arrays (used below)...
  CriticalSection  accessToGlobalArray(::NOVRAM_DIAG_LOG_ARRAY_MT);

  //=================================================================== 
  //  Initialize static arrays...
  //=================================================================== 

  static const Uint  MEMORY_SIZE_ =
(sizeof(FixedArray(CodeLogEntry,MAX_CODE_LOG_ENTRIES)) + sizeof(Uint) - 1) /
			      sizeof(Uint);

  static Uint  EstCodeLogEntriesMemory_[MEMORY_SIZE_];
  static FixedArray(CodeLogEntry,MAX_CODE_LOG_ENTRIES)&  REstCodeLogEntries_ =
 *((FixedArray(CodeLogEntry,MAX_CODE_LOG_ENTRIES)*)EstCodeLogEntriesMemory_);

  static Uint  SstCodeLogEntriesMemory_[MEMORY_SIZE_];
  static FixedArray(CodeLogEntry,MAX_CODE_LOG_ENTRIES)&  RSstCodeLogEntries_ =
 *((FixedArray(CodeLogEntry,MAX_CODE_LOG_ENTRIES)*)SstCodeLogEntriesMemory_);

  static Boolean  AreArraysInitialized_ = FALSE;

  if (!AreArraysInitialized_)
  {   // $[TI1] -- static arrays not initialized, yet...
    SAFE_CLASS_ASSERTION((sizeof(EstCodeLogEntriesMemory_) >=
		    sizeof(FixedArray(CodeLogEntry,MAX_CODE_LOG_ENTRIES))));
    new (EstCodeLogEntriesMemory_)
			    FixedArray(CodeLogEntry,MAX_CODE_LOG_ENTRIES)();

    SAFE_CLASS_ASSERTION((sizeof(SstCodeLogEntriesMemory_) >=
		    sizeof(FixedArray(CodeLogEntry,MAX_CODE_LOG_ENTRIES))));
    new (SstCodeLogEntriesMemory_)
			    FixedArray(CodeLogEntry,MAX_CODE_LOG_ENTRIES)();

    AreArraysInitialized_ = TRUE;
  }   // $[TI2] -- static arrays already initialized...

  //=================================================================== 
  //  Merge the "GUI" and "BD" EST entries into 'REstCodeLogEntries_'...
  //=================================================================== 

  NonVolatileMemory::P_INSTANCE_->estDiagLog_.getCurrEntries(
						      ::RGuiCodeLogEntries_
							   );
  VolatileMemory::P_INSTANCE_->estDiagLog_.getCurrEntries(
						      ::RBdCodeLogEntries_
						         );

  DiagCodeLog::MergeCodeLogs(REstCodeLogEntries_, ::RGuiCodeLogEntries_,
			     ::RBdCodeLogEntries_, ::MAX_CODE_LOG_ENTRIES);

  //=================================================================== 
  //  Merge the "GUI" and "BD" SST entries into 'RSstCodeLogEntries_'...
  //=================================================================== 

  NonVolatileMemory::P_INSTANCE_->sstDiagLog_.getCurrEntries(
  						::RGuiCodeLogEntries_
							   );
  VolatileMemory::P_INSTANCE_->sstDiagLog_.getCurrEntries(
  						::RBdCodeLogEntries_
							 );

  DiagCodeLog::MergeCodeLogs(RSstCodeLogEntries_, ::RGuiCodeLogEntries_,
			     ::RBdCodeLogEntries_, ::MAX_CODE_LOG_ENTRIES);

  //=================================================================== 
  //  Merge the EST and SST entries into 'rArrEntries'...
  //=================================================================== 

  DiagCodeLog::MergeCodeLogs(rArrEntries, REstCodeLogEntries_,
			     RSstCodeLogEntries_, ::MAX_CODE_LOG_ENTRIES);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetEstResultEntries(rArrEntries)  [static]
//
//@ Interface-Description
//  Store the current entries of the EST Result Table into 'rArrEntries'.
//  The ordering of the entries is based on the test number (i.e., the
//  entry at index '0' is the result of test '0').
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_  &&  VolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

void
NovRamManager::GetEstResultEntries(
		  FixedArray(ResultTableEntry,MAX_TEST_ENTRIES)& rArrEntries
				  )
{
  CALL_TRACE("GetEstResultEntries(rArrEntries)");
  AUX_CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_  &&
			   VolatileMemory::IsInitialized_),
			  NonVolatileMemory::IsInitialized_);

  // protect usage of global result table entry arrays (used below)...
  CriticalSection  accessToGlobalArray(::NOVRAM_RESULT_TABLE_ARRAY_MT);

  NonVolatileMemory::P_INSTANCE_->estResultTable_.getCurrEntries(
						      ::RGuiResultEntries_
							       );
  VolatileMemory::P_INSTANCE_->estResultTable_.getCurrEntries(
						      ::RBdResultEntries_
							     );

  TestResultTable::MergeResultTables(rArrEntries, ::RGuiResultEntries_,
  				     ::RBdResultEntries_, ::MAX_EST_RESULTS);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetSstResultEntries(rArrEntries)  [static]
//
//@ Interface-Description
//  Store the current entries of the SST Result Table into 'rArrEntries'.
//  The ordering of the entries is based on the test number (i.e., the
//  entry at index '0' is the result of test '0').
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_  &&  VolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

void
NovRamManager::GetSstResultEntries(
		  FixedArray(ResultTableEntry,MAX_TEST_ENTRIES)& rArrEntries
				  )
{
  CALL_TRACE("GetSstResultEntries(rArrEntries)");
  AUX_CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_  &&
			   VolatileMemory::IsInitialized_),
			  NonVolatileMemory::IsInitialized_);

  // protect usage of global result table entry arrays (used below)...
  CriticalSection  accessToGlobalArray(::NOVRAM_RESULT_TABLE_ARRAY_MT);

  NonVolatileMemory::P_INSTANCE_->sstResultTable_.getCurrEntries(
						      ::RGuiResultEntries_
							       );
  VolatileMemory::P_INSTANCE_->sstResultTable_.getCurrEntries(
						      ::RBdResultEntries_
							     );

  TestResultTable::MergeResultTables(rArrEntries, ::RGuiResultEntries_,
  				     ::RBdResultEntries_, ::MAX_SST_RESULTS);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetEstTestDiagnostics(estTestNumber, rArrEntries)  [static]
//
//@ Interface-Description
//  Get the diagnostic codes that were logged by the last run of the EST
//  test given by 'estTestNumber'.  The diagnostics are returned in
//  'rArrEntries', where the ordering of the entries within the returned
//  array is such that the first entry of the array is the entry that
//  was entered last.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//  This method first determines which EST Diagnostic Log ("BD" or
//  "GUI") to search for the diagnostics, then extracts from that log.
//
//  Using statically allocated memory for the "GUI" and "BD" entry arrays,
//  and SST and EST entry arrays, because of the risk of stack overruns if
//  I use the stack.  The risk is especially high due to the tremendous size
//  of these arrays (about 2K).
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

void
NovRamManager::GetEstTestDiagnostics(
		const Uint                                     estTestNumber,
		FixedArray(CodeLogEntry,MAX_CODE_LOG_ENTRIES)& rArrEntries
				    )
{
  CALL_TRACE("GetEstTestDiagnostics(estTestNumber, rArrEntries)");
  AUX_CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_  &&
			   VolatileMemory::IsInitialized_),
			  NonVolatileMemory::IsInitialized_);
  AUX_CLASS_PRE_CONDITION((estTestNumber < ::MAX_EST_RESULTS),
			  estTestNumber);

  // protect usage of global code log entry arrays (used below)...
  CriticalSection  accessToGlobalArray(::NOVRAM_DIAG_LOG_ARRAY_MT);

  // use statically allocated array, to reduce stack usage...
  FixedArray(CodeLogEntry,MAX_CODE_LOG_ENTRIES)&  rDiagLogEntries =
						      ::RGuiCodeLogEntries_;

  FixedArray(ResultTableEntry,MAX_TEST_ENTRIES)   bdTestResults;
  ResultEntryData                                 resultData;

  // get the "BD" EST results...
  VolatileMemory::P_INSTANCE_->estResultTable_.getCurrEntries(bdTestResults);

  if (!bdTestResults[estTestNumber].isClear())
  {   // $[TI1]
    // the diagnostics for test 'estTestNumber', if any, are in the "BD"
    // diagnostic log, therefore initialize 'resultData' and
    // 'rDiagLogEntries' with the result of the test and the entries from
    // the "BD" log, respectively...
    bdTestResults[estTestNumber].getResultData(resultData);

    VolatileMemory::P_INSTANCE_->estDiagLog_.getCurrEntries(rDiagLogEntries);
  }
  else
  {   // $[TI2]
    // the diagnostics for test 'estTestNumber', if any, are in the "GUI"
    // diagnostic log, therefore initialize 'resultData' and 'rDiagLogEntries'
    // with the result of the test and the entries from the "GUI" log,
    // respectively...
    FixedArray(ResultTableEntry,MAX_TEST_ENTRIES)  guiTestResults;

    // get the "GUI" EST results...
    NonVolatileMemory::P_INSTANCE_->estResultTable_.getCurrEntries(
							      guiTestResults
								 );

    guiTestResults[estTestNumber].getResultData(resultData);

    NonVolatileMemory::P_INSTANCE_->estDiagLog_.getCurrEntries(
							  rDiagLogEntries
							     );
  }

  Uint arrIdx = 0u;

  if (resultData.getResultOfCurrRun() > PASSED_TEST_ID)
  {  // $[TI3] -- there is a failure...
    for (Uint logIdx = 0u;
         logIdx < ::MAX_EST_LOG_ENTRIES        &&
	   !rDiagLogEntries[logIdx].isClear()  &&
	    rDiagLogEntries[logIdx].getSequenceNumber() >
				      resultData.getTestStartSeqNumber();
         logIdx++)
    {   // $[TI3.1] -- at least one loop...
      const DiagnosticCode&  R_DIAG_CODE =
			      rDiagLogEntries[logIdx].getDiagnosticCode();

      if (R_DIAG_CODE.getDiagCodeTypeId() ==
				 DiagnosticCode::EXTENDED_SELF_TEST_DIAG)
      {   // $[TI3.1.3]
	const DiagnosticCode::ExtendedSelfTestBits  EST_CODE =
				   R_DIAG_CODE.getExtendedSelfTestCode();

	if (EST_CODE.testNumber == estTestNumber)
	{   // $[TI3.1.3.1]
	  rArrEntries[arrIdx++] = rDiagLogEntries[logIdx];
	}   // $[TI3.1.3.2] -- diag. code doesn't represent this test...
      }   // $[TI3.1.4] -- diag. entry is NOT an EST entry...
    }   // $[TI3.2] -- no loops...
  }   // $[TI4] -- this test has no failures...

  for (; arrIdx < ::MAX_CODE_LOG_ENTRIES; arrIdx++)
  {   // $[TI5] -- at least one loop...
    rArrEntries[arrIdx].clear();
  }   // $[TI6] -- no loops...
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetSstTestDiagnostics(sstTestNumber, rArrEntries)  [static]
//
//@ Interface-Description
//  Get the diagnostic codes that were logged by the last run of the SST
//  test given by 'sstTestNumber'.  The diagnostics are returned in
//  'rArrEntries', where the ordering of the entries within the returned
//  array is such that the first entry of the array is the entry that
//  was entered last.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//  This method is first determines which SST Diagnostic Log ("BD" or
//  "GUI") to search for the diagnostics, then extracts from that log.
//
//  Using statically allocated memory for the "GUI" and "BD" entry arrays,
//  and SST and EST entry arrays, because of the risk of stack overruns if
//  I use the stack.  The risk is especially high due to the tremendous size
//  of these arrays (about 2K).
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

void
NovRamManager::GetSstTestDiagnostics(
		const Uint                                     sstTestNumber,
		FixedArray(CodeLogEntry,MAX_CODE_LOG_ENTRIES)& rArrEntries
				  )
{
  CALL_TRACE("GetSstTestDiagnostics(sstTestNumber, rArrEntries)");
  AUX_CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_  &&
			   VolatileMemory::IsInitialized_),
			  NonVolatileMemory::IsInitialized_);
  AUX_CLASS_PRE_CONDITION((sstTestNumber < ::MAX_SST_RESULTS),
  			  sstTestNumber);

  // protect usage of global code log entry arrays (used below)...
  CriticalSection  accessToGlobalArray(::NOVRAM_DIAG_LOG_ARRAY_MT);

  // use statically allocated array, to reduce stack usage...
  FixedArray(CodeLogEntry,MAX_CODE_LOG_ENTRIES)&  rDiagLogEntries =
						      ::RGuiCodeLogEntries_;

  FixedArray(ResultTableEntry,MAX_TEST_ENTRIES)   bdTestResults;
  ResultEntryData                                 resultData;

  // get the "BD" SST results...
  VolatileMemory::P_INSTANCE_->sstResultTable_.getCurrEntries(bdTestResults);

  if (!bdTestResults[sstTestNumber].isClear())
  {   // $[TI1]
    // the diagnostics for test 'sstTestNumber', if any, are in the "BD"
    // diagnostic log, therefore initialize 'resultData' and 'rDiagLogEntries'
    // with the result of the test and the entries from the "BD" log,
    // respectively...
    bdTestResults[sstTestNumber].getResultData(resultData);

    VolatileMemory::P_INSTANCE_->sstDiagLog_.getCurrEntries(rDiagLogEntries);
  }
  else
  {   // $[TI2]
    // the diagnostics for test 'sstTestNumber', if any, are in the "GUI"
    // diagnostic log, therefore initialize 'resultData' and 'rDiagLogEntries'
    // with the result of the test and the entries from the "GUI" log,
    // respectively...
    FixedArray(ResultTableEntry,MAX_TEST_ENTRIES)  guiTestResults;

    // get the "GUI" SST results...
    NonVolatileMemory::P_INSTANCE_->sstResultTable_.getCurrEntries(
							      guiTestResults
								 );

    guiTestResults[sstTestNumber].getResultData(resultData);

    NonVolatileMemory::P_INSTANCE_->sstDiagLog_.getCurrEntries(
							  rDiagLogEntries
							     );
  }

  Uint arrIdx = 0u;

  if (resultData.getResultOfCurrRun() > PASSED_TEST_ID)
  {  // $[TI3] -- there is a failure...
    for (Uint logIdx = 0u;
         logIdx < ::MAX_SST_LOG_ENTRIES        &&
	   !rDiagLogEntries[logIdx].isClear()  &&
	    rDiagLogEntries[logIdx].getSequenceNumber() >
					  resultData.getTestStartSeqNumber();
         logIdx++)
    {   // $[TI3.1] -- at least one loop...
      const DiagnosticCode&  R_DIAG_CODE =
      				rDiagLogEntries[logIdx].getDiagnosticCode();

      if (R_DIAG_CODE.getDiagCodeTypeId() ==
					DiagnosticCode::SHORT_SELF_TEST_DIAG)
      {   // $[TI3.1.1]
	const DiagnosticCode::ShortSelfTestBits  SST_CODE =
					  R_DIAG_CODE.getShortSelfTestCode();

	if (SST_CODE.testNumber == sstTestNumber)
	{   // $[TI3.1.1.1]
	  rArrEntries[arrIdx++] = rDiagLogEntries[logIdx];
	}   // $[TI3.1.1.2] -- diagnostic code doesn't represent this test...
      }   // $[TI3.1.2]
    }   // $[TI3.2] -- no loops...
  }   // $[TI4] -- this has no failures...

  for (; arrIdx < ::MAX_CODE_LOG_ENTRIES; arrIdx++)
  {   // $[TI5] -- at least one loop...
    rArrEntries[arrIdx].clear();
  }   // $[TI6] -- no loops...
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetAlarmLogEntries()  [static]
//
//@ Interface-Description
//  Store into 'rArrEntries' the current entries from the Alarm History
//  Log.  The ordering of the entries within the returned array is such
//  that the first entry of the array is the entry that was entered last.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

void
NovRamManager::GetAlarmLogEntries(
		  FixedArray(AlarmLogEntry,MAX_ALARM_ENTRIES)& rArrEntries
				 )
{
  CALL_TRACE("GetAlarmLogEntries(rArrEntries)");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  NonVolatileMemory::P_INSTANCE_->alarmHistoryLog_.getCurrEntries(
							      rArrEntries
								 );
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  LogAlarmAction(alarmAction)  [static]
//
//@ Interface-Description
//  Log the alarm action (event) given by 'alarmAction' into the Alarm
//  History Log.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

void
NovRamManager::LogAlarmAction(const AlarmAction& alarmAction)
{
  CALL_TRACE("LogAlarmAction(alarmAction)");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  NonVolatileMemory::P_INSTANCE_->alarmHistoryLog_.logAlarmAction(
							      alarmAction
								 );
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ClearAlarmLog()  [static]
//
//@ Interface-Description
//  Clear the entire Alarm History Log, such that the log is left in an
//  "empty" state.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

void
NovRamManager::ClearAlarmLog(void)
{
  CALL_TRACE("ClearAlarmLog()");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  NonVolatileMemory::P_INSTANCE_->alarmHistoryLog_.clearAlarmEntries();
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetAcceptedBatchSettings(rCurrAcceptedBatchValues)  [static]
//
//@ Interface-Description
//  Return, in 'rCurrAcceptedBatchValues', a copy of the currently accepted
//  values of the batch settings.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//  (NovRamManager::AreBatchSettingsInitialized())
//---------------------------------------------------------------------
//@ PostCondition 
//  none
//@ End-Method 
//===================================================================== 

void
NovRamManager::GetAcceptedBatchSettings(
				BatchSettingValues& rCurrAcceptedBatchValues
				       )
{
  CALL_TRACE("GetAcceptedBatchSettings(rCurrAcceptedBatchValues)");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));
  CLASS_PRE_CONDITION((NovRamManager::AreBatchSettingsInitialized()));

  NonVolatileMemory::P_INSTANCE_->acceptedBatchValues_.getData(
						    rCurrAcceptedBatchValues
							      );
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  UpdateAcceptedBatchSettings(newBatchValues)  [static]
//
//@ Interface-Description
//  Update the currently accepted values of the batch settings, to the
//  values given by 'newBatchValues'.  This is only to be called by
//  'AcceptedContext'.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  (newBatchValues == NovRamManager::GetAcceptedBatchSettings())
//@ End-Method 
//===================================================================== 

void
NovRamManager::UpdateAcceptedBatchSettings(
				    const BatchSettingValues& newBatchValues
					  )
{
  CALL_TRACE("UpdateAcceptedBatchSettings(newBatchValues)");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  NonVolatileMemory::P_INSTANCE_->acceptedBatchValues_.updateData(
							      newBatchValues
								 );
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  AreNonBatchSettingsInitialized()  [static]
//
//@ Interface-Description
//  Does the persistent store have initialized non-batch setting values?  The
//  values are considered initialized after the first initialization of the
//  Settings-Validation is completed.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

Boolean
NovRamManager::AreNonBatchSettingsInitialized(void)
{
  CALL_TRACE("AreNonBatchSettingsInitialized()");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  return(NonVolatileMemory::P_INSTANCE_->nonBatchSettingsInitFlag_.isTrue());
}   // $[TI1] (TRUE)  $[TI2] (FALSE)...


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  UpdateNonBatchSettingsFlag(newFlagValue)  [static]
//
//@ Interface-Description
//  Update the flag that indicates whether the persistent store has
//  initialized non-batch setting values.  The flag is given the value
//  indicated by 'newFlagValue'.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  (newFlagValue == NovRamManager::AreNonBatchSettingsInitialized())
//@ End-Method 
//===================================================================== 

void
NovRamManager::UpdateNonBatchSettingsFlag(const Boolean newFlagValue)
{
  CALL_TRACE("UpdateNonBatchSettingsFlag(newFlagValue)");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  NonVolatileMemory::P_INSTANCE_->nonBatchSettingsInitFlag_.updateValue(
  								 newFlagValue
								       );
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetAcceptedNonBatchSettings(rCurrAcceptedNonBatchValues) [static]
//
//@ Interface-Description
//  Return, in 'rCurrAcceptedNonBatchValues', a copy of the currently
//  accepted values of the non-batch settings.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none
//@ End-Method 
//===================================================================== 

void
NovRamManager::GetAcceptedNonBatchSettings(
			  NonBatchSettingValues& rCurrAcceptedNonBatchValues
			  		  )
{
  CALL_TRACE("GetAcceptedNonBatchSettings(rCurrAcceptedNonBatchValues)");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));
  CLASS_PRE_CONDITION((NovRamManager::AreNonBatchSettingsInitialized()));

  NonVolatileMemory::P_INSTANCE_->acceptedNonBatchValues_.getData(
						rCurrAcceptedNonBatchValues
								 );
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  UpdateAcceptedNonBatchSettings(newNonBatchValues)  [static]
//
//@ Interface-Description
//  Update the currently accepted values of the non-batch settings,
//  to the values given by 'newNonBatchValues'.  This is only to be called
//  by 'AcceptedContext'.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  (newNonBatchValues == NovRamManager::GetAcceptedNonBatchSettings())
//@ End-Method 
//===================================================================== 

void
NovRamManager::UpdateAcceptedNonBatchSettings(
			      const NonBatchSettingValues& newNonBatchValues
					     )
{
  CALL_TRACE("UpdateAcceptedNonBatchSettings(newNonBatchValues)");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  NonVolatileMemory::P_INSTANCE_->acceptedNonBatchValues_.updateData(
							   newNonBatchValues
								   );
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetLastSstHumidifierType()  [static]
//
//@ Interface-Description
//  Return a copy of the value of the Humidifier Type Setting for the
//  last SST run.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none
//@ End-Method 
//===================================================================== 

DiscreteValue
NovRamManager::GetLastSstHumidifierType(void)
{
  CALL_TRACE("GetLastSstHumidifierType()");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  return(NonVolatileMemory::P_INSTANCE_->lastSstHumidifierType_.getValue());
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  UpdateLastSstHumidifierType(newHumidifierType)  [static]
//
//@ Interface-Description
//  Update the value of the last Humidifier Type Setting value to
//  'newHumidifierType'.  This is to be called by SST only.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  (newHumidifierType == NovRamManager::GetLastSstHumidifierType())
//@ End-Method 
//===================================================================== 

void
NovRamManager::UpdateLastSstHumidifierType(
				    const DiscreteValue newHumidifierType
					  )
{
  CALL_TRACE("UpdateLastSstHumidifierType(newHumidifierType)");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  NonVolatileMemory::P_INSTANCE_->lastSstHumidifierType_.updateValue(
							  newHumidifierType
								   );
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetCompressorOperationalHours()  [static]
//
//@ Interface-Description
//  Return the stored elapsed hours of compressor usage.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (VolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none
//@ End-Method 
//===================================================================== 

Uint
NovRamManager::GetCompressorOperationalHours(void)
{
  CALL_TRACE("GetCompressorOperationalHours()");
  CLASS_PRE_CONDITION((VolatileMemory::IsInitialized_));

  return(VolatileMemory::P_INSTANCE_->compressorOperationalHours_);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetSystemOperationalHours()  [static]
//
//@ Interface-Description
//  Return the stored elapsed hours of system usage.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (VolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none
//@ End-Method 
//===================================================================== 

Uint
NovRamManager::GetSystemOperationalHours(void)
{
  CALL_TRACE("GetSystemOperationalHours()");
  CLASS_PRE_CONDITION((VolatileMemory::IsInitialized_));

  return(VolatileMemory::P_INSTANCE_->systemOperationalHours_);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetEstTestResultData(rEstTestResultData)  [static]
//
//@ Interface-Description
//  Get the stored EST Test Data, and return it in 'rEstTestResultData'.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

void
NovRamManager::GetEstTestResultData(EstTestResultData& rEstTestResultData)
{
  CALL_TRACE("GetEstTestResultData(rEstTestResultData)");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  NonVolatileMemory::P_INSTANCE_->estTestResultData_.getData(
  							rEstTestResultData
							   );
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  UpdateEstTestResultData(newEstTestResultData)  [static]
//
//@ Interface-Description
//  Store 'newEstTestResultData' as the new result data for EST.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

void
NovRamManager::UpdateEstTestResultData(
				const EstTestResultData& newEstTestResultData
				      )
{
  CALL_TRACE("UpdateEstTestResultData(newEstTestResultData)");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  NonVolatileMemory::P_INSTANCE_->estTestResultData_.updateData(
  							 newEstTestResultData
							      );
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetSstTestResultData(rSstTestResultData)  [static]
//
//@ Interface-Description
//  Get the stored SST Test Data, and return it in 'rSstTestResultData'.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

void
NovRamManager::GetSstTestResultData(SstTestResultData& rSstTestResultData)
{
  CALL_TRACE("GetSstTestResultData(rSstTestResultData)");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  NonVolatileMemory::P_INSTANCE_->sstTestResultData_.getData(
							rSstTestResultData
							   );
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  UpdateSstTestResultData(newSstTestResultData)  [static]
//
//@ Interface-Description
//  Store 'newSstTestResultData' as the new result data for SST.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

void
NovRamManager::UpdateSstTestResultData(
				const SstTestResultData& newSstTestResultData
				      )
{
  CALL_TRACE("UpdateSstTestResultData(newSstTestResultData)");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  NonVolatileMemory::P_INSTANCE_->sstTestResultData_.updateData(
  							 newSstTestResultData
							      );
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetDataCorruptionInfo(rCorruptionInfo)  [static]
//
//@ Interface-Description
//  Return -- via 'rCorruptionInfo' -- the data corruption inforomation
//  used by Safety-Net.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none
//@ End-Method 
//===================================================================== 

void
NovRamManager::GetDataCorruptionInfo(DataCorruptionInfo& rCorruptionInfo)
{
  CALL_TRACE("GetDataCorruptionInfo(rCorruptionInfo)");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  NonVolatileMemory::P_INSTANCE_->dataCorruptionInfo_.getData(rCorruptionInfo);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  UpdateDataCorruptionInfo(newCorruptionInfo)  [static]
//
//@ Interface-Description
//  Update the data corruption information used by Safety-Net.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none
//@ End-Method 
//===================================================================== 

void
NovRamManager::UpdateDataCorruptionInfo(
				const DataCorruptionInfo& newCorruptionInfo
				       )
{
  CALL_TRACE("UpdateDataCorruptionInfo(newCorruptionInfo)");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  NonVolatileMemory::P_INSTANCE_->dataCorruptionInfo_.updateData(
							    newCorruptionInfo
							       );
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetNoVentStartTime(rStartingTime)  [static]
//
//@ Interface-Description
//  Return -- via 'rStartingTime' -- a time stamp that indicates the
//  time of the beginning of a "No Ventilation: " state.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none
//@ End-Method 
//===================================================================== 

void
NovRamManager::GetNoVentStartTime(TimeStamp& rStartTime)
{
  CALL_TRACE("GetNoVentStartTime(rStartTime)");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  NonVolatileMemory::P_INSTANCE_->noVentStartTime_.getData(rStartTime);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SetNoVentStartTime()  [static]
//
//@ Interface-Description
//  Set the time stamp used to indicate the start time of the "No
//  Ventilation" state to "now".
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none
//@ End-Method 
//===================================================================== 

void
NovRamManager::SetNoVentStartTime(void)
{
  CALL_TRACE("SetNoVentStartTime()");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  TimeStamp  now;

  NonVolatileMemory::P_INSTANCE_->noVentStartTime_.updateData(now);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ClearNoVentStartTime()  [static]
//
//@ Interface-Description
//  Clear the time stamp used to indicate the start time of the "No
//  Ventilation" state to an invalid state.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none
//@ End-Method 
//===================================================================== 

void
NovRamManager::ClearNoVentStartTime(void)
{
  CALL_TRACE("ClearNoVentStartTime()");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  NonVolatileMemory::P_INSTANCE_->noVentStartTime_.updateData(
						  TimeStamp::GetRNullTimeStamp()
						  	     );
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetCompactFlashTestStatus()  [static]
//
//@ Interface-Description
//  Return a copy of the value of the compact flash test status.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none
//@ End-Method 
//===================================================================== 

DiscreteValue NovRamManager::GetCompactFlashTestStatus(void)
{
	CALL_TRACE("GetCompactFlashTestStatus()");
	CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

	return (NonVolatileMemory::P_INSTANCE_->compactFlashTestStatus_.getValue());
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  UpdateCompactFlashTestStatus(newStatus)  [static]
//
//@ Interface-Description
//  Update the value of the compact flash test status to newStatus.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//@ End-Method 
//===================================================================== 

void NovRamManager::UpdateCompactFlashTestStatus(const DiscreteValue newStatus)
{
	CALL_TRACE("UpdateCompactFlashTestStatus(newStatus)");
	CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

	NonVolatileMemory::P_INSTANCE_->compactFlashTestStatus_.updateValue( newStatus );
}

#endif // defined(SIGMA_GUI_CPU)


#if defined(SIGMA_BD_CPU)

//=====================================================================
//
//  BD-only Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetBdSystemState()  [static]
//
//@ Interface-Description
//  Return the BD's system state object.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

BdSystemState
NovRamManager::GetBdSystemState(void)
{
  CALL_TRACE("GetBdSystemState()");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  BdSystemState  bdSystemState;

  NonVolatileMemory::P_INSTANCE_->bdSystemState_.getData(bdSystemState);

  return(bdSystemState);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  UpdateBdSystemState(newBdSystemState)  [static]
//
//@ Interface-Description
//  Store the BD's system state object given by 'newBdSystemState'.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

void
NovRamManager::UpdateBdSystemState(const BdSystemState& newBdSystemState)
{
  CALL_TRACE("UpdateBdSystemState(newBdSystemState)");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  NonVolatileMemory::P_INSTANCE_->bdSystemState_.updateData(newBdSystemState);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetAcceptedBatchSettings(rCurrAcceptedBatchValues)  [static]
//
//@ Interface-Description
//  Return a copy of the currently accepted batch setting values.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//  (NovRamManager::AreBatchSettingsInitialized())
//---------------------------------------------------------------------
//@ PostCondition 
//  none
//@ End-Method 
//===================================================================== 

void
NovRamManager::GetAcceptedBatchSettings(
				  BdSettingValues& rCurrAcceptedBatchValues
				       )
{
  CALL_TRACE("GetAcceptedBatchSettings(rCurrAcceptedBatchValues)");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));
  CLASS_PRE_CONDITION((NovRamManager::AreBatchSettingsInitialized()));

  NonVolatileMemory::P_INSTANCE_->acceptedBatchValues_.getData(
						    rCurrAcceptedBatchValues
							      );
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  UpdateAcceptedBatchSettings(newBatchValues)  [static]
//
//@ Interface-Description
//  Update the current values of the Phased-In Settings, to the values
//  given by 'newBatchValues'.  This is only to be called by 'PhasedInContext'
//  only.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  (newBatchValues == NovRamManager::GetAcceptedBatchSettings())
//@ End-Method 
//===================================================================== 

void
NovRamManager::UpdateAcceptedBatchSettings(
				      const BdSettingValues& newBatchValues
					  )
{
  CALL_TRACE("UpdateAcceptedBatchSettings(newBatchValues)");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  NonVolatileMemory::P_INSTANCE_->acceptedBatchValues_.updateData(
							    newBatchValues
								 );
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetPsolLiftoffCurrents(rPsolLiftoff)  [static]
//
//@ Interface-Description
//  Return -- in 'rPsolLiftoff' -- the stored lift-off currents for the PSOL
//  valves.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none
//@ End-Method 
//===================================================================== 

void
NovRamManager::GetPsolLiftoffCurrents(PsolLiftoff& rPsolLiftoff)
{
  CALL_TRACE("GetPsolLiftoffCurrents(rPsolLiftoff)");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  NonVolatileMemory::P_INSTANCE_->psolLiftoff_.getData(rPsolLiftoff);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  UpdatePsolLiftoffCurrents(psolLiftoff)  [static]
//
//@ Interface-Description
//  Update the liftoff current values for the PSOL valves.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none
//@ End-Method 
//===================================================================== 

void
NovRamManager::UpdatePsolLiftoffCurrents(const PsolLiftoff& psolLiftoff)
{
  CALL_TRACE("UpdatePsolLiftoffCurrents(psolLiftoff)");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  NonVolatileMemory::P_INSTANCE_->psolLiftoff_.updateData(psolLiftoff);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetCompressorOperationalMinutes()  [static]
//
//@ Interface-Description
//  Return the compressor's elapsed time.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none
//@ End-Method 
//===================================================================== 

Uint32
NovRamManager::GetCompressorOperationalMinutes(void)
{
  CALL_TRACE("GetCompressorTime()");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  OperationalTime  compressorOperationalMinutes;

  NonVolatileMemory::P_INSTANCE_->compressorOperationalMinutes_.getData(
    						compressorOperationalMinutes
								      );

  return(compressorOperationalMinutes.getTimeCount());
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  UpdateCompressorOperationalMinutes(newMinutes)  [static]
//
//@ Interface-Description
//  Update the compressor's elapsed time.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none
//@ End-Method 
//===================================================================== 

void
NovRamManager::UpdateCompressorOperationalMinutes(const Uint32 newMinutes)
{
  CALL_TRACE("UpdateElapsedCompressorTime(newMinutes)");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  OperationalTime  compressorOperationalMinutes;

  compressorOperationalMinutes.setTimeCount(newMinutes);
  NonVolatileMemory::P_INSTANCE_->compressorOperationalMinutes_.updateData(
						  compressorOperationalMinutes
									  );

  NovRamUpdateManager::UpdateHappened(
			  NovRamUpdateManager::COMPRESSOR_OPER_TIME_UPDATE
				     );
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  UpdateFio2CalInfo(fio2CalInfo)  [static]
//
//@ Interface-Description
//  Update the calibration information of the FIO2 sensor.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none
//@ End-Method 
//===================================================================== 

void
NovRamManager::UpdateFio2CalInfo(const Fio2CalInfo& fio2CalInfo)
{
  CALL_TRACE("UpdateFio2CalInfo(fio2CalInfo)");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  NonVolatileMemory::P_INSTANCE_->fio2CalInfo_.updateData(fio2CalInfo);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetFio2CalInfo(rFio2CalInfo)  [static]
//
//@ Interface-Description
//  Return -- via 'rFio2CalInfo' -- the calibration information for the
//  FIO2 sensor.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none
//@ End-Method 
//===================================================================== 

void
NovRamManager::GetFio2CalInfo(Fio2CalInfo& rFio2CalInfo)
{
  CALL_TRACE("GetFio2CalInfo(rFio2CalInfo)");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  NonVolatileMemory::P_INSTANCE_->fio2CalInfo_.getData(rFio2CalInfo);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  UpdateCircuitComplianceTable(complianceTable)  [static]
//
//@ Interface-Description
//  Update the patient circuit's compliance table.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none
//@ End-Method 
//===================================================================== 

void
NovRamManager::UpdateCircuitComplianceTable(
				  const CircuitCompliance& complianceTable
					   )
{
  CALL_TRACE("UpdateCircuitComplianceTable(complianceTable)");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  NonVolatileMemory::P_INSTANCE_->circuitComplianceTable_.updateData(
							    complianceTable
								   );
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetCircuitComplianceTable(rComplianceTable)  [static]
//
//@ Interface-Description
//  Return -- via 'rComplianceTable' -- the patient circuit's compliance.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none
//@ End-Method 
//===================================================================== 

void
NovRamManager::GetCircuitComplianceTable(CircuitCompliance& rComplianceTable)
{
  CALL_TRACE("GetCircuitComplianceTable(rComplianceTable)");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  NonVolatileMemory::P_INSTANCE_->circuitComplianceTable_.getData(
							  rComplianceTable
								);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetSystemOperationalMinutes()  [static]
//
//@ Interface-Description
//  Return the stored elapsed minutes of system usage.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (VolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none
//@ End-Method 
//===================================================================== 

Uint32
NovRamManager::GetSystemOperationalMinutes(void)
{
  CALL_TRACE("GetSystemOperationalMinutes()");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  OperationalTime  systemOperationalMinutes;

  NonVolatileMemory::P_INSTANCE_->systemOperationalMinutes_.getData(
						    systemOperationalMinutes
								   );

  return(systemOperationalMinutes.getTimeCount());
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  UpdateSystemOperationalMinutes(newMinutes)  [static]
//
//@ Interface-Description
//  Update the stored elapsed hours of system usage.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none
//@ End-Method 
//===================================================================== 

void
NovRamManager::UpdateSystemOperationalMinutes(const Uint32 newMinutes)
{
  CALL_TRACE("UpdateSystemOperationalMinutes(newMinutes)");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  OperationalTime  systemOperationalMinutes;

  systemOperationalMinutes.setTimeCount(newMinutes);
  NonVolatileMemory::P_INSTANCE_->systemOperationalMinutes_.updateData(
						    systemOperationalMinutes
								      );

  NovRamUpdateManager::UpdateHappened(
			    NovRamUpdateManager::SYSTEM_OPER_TIME_UPDATE
				     );
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetCompressorSerialNumber(rSerialNumber)  [static]
//
//@ Interface-Description
//  Return the serial number of the compressor, in 'rSerialNumber'.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (VolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none
//@ End-Method 
//===================================================================== 

void
NovRamManager::GetCompressorSerialNumber(SerialNumber& rSerialNumber)
{
  CALL_TRACE("GetCompressorSerialNumber(rSerialNumber)");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  NonVolatileMemory::P_INSTANCE_->compressorSerialNum_.getData(rSerialNumber);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  UpdateCompressorSerialNumber(serialNumber)  [static]
//
//@ Interface-Description
//  Update the stored serial number for the compressor, to the value given
//  by 'serialNumber'.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none
//@ End-Method 
//===================================================================== 

void
NovRamManager::UpdateCompressorSerialNumber(
					const SerialNumber& serialNumber
					   )
{
  CALL_TRACE("UpdateCompressorSerialNumber(serialNumber)");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  NonVolatileMemory::P_INSTANCE_->compressorSerialNum_.updateData(
							      serialNumber
								 );
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  UpdateInspResistance(inspCctResistance)  [static]
//
//@ Interface-Description
//  Update the stored inspiratory circuit's resistance to the state given
//  by 'inspCctResistance'.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none
//@ End-Method 
//===================================================================== 

void
NovRamManager::UpdateInspResistance(const Resistance& inspCctResistance)
{
  CALL_TRACE("UpdateInspResistance(inspCctResistance)");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  NonVolatileMemory::P_INSTANCE_->inspCctResistance_.updateData(
							  inspCctResistance
							       );
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  UpdateExhResistance(exhCctResistance)  [static]
//
//@ Interface-Description
//  Update the stored exhalation circuit's resistance to the state given
//  by 'exhCctResistance'.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none
//@ End-Method 
//===================================================================== 

void
NovRamManager::UpdateExhResistance(const Resistance& exhCctResistance)
{
  CALL_TRACE("UpdateExhResistance(exhCctResistance)");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  NonVolatileMemory::P_INSTANCE_->exhCctResistance_.updateData(
							  exhCctResistance
							      );
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetAirFlowSensorOffset(rOffset)  [static]
//
//@ Interface-Description
//  Return the stored Air Flow Sensor's offset in the reference given by
//  'rOffset'.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none
//@ End-Method 
//===================================================================== 

void
NovRamManager::GetAirFlowSensorOffset(FlowSensorOffset& rOffset)
{
  CALL_TRACE("GetAirFlowSensorOffset(rOffset)");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  NonVolatileMemory::P_INSTANCE_->airFlowSensorOffset_.getData(rOffset);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetO2FlowSensorOffset(rOffset)  [static]
//
//@ Interface-Description
//  Return the stored O2 Flow Sensor's offset in the reference given by
//  'rOffset'.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none
//@ End-Method 
//===================================================================== 

void
NovRamManager::GetO2FlowSensorOffset(FlowSensorOffset& rOffset)
{
  CALL_TRACE("GetO2FlowSensorOffset(rOffset)");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  NonVolatileMemory::P_INSTANCE_->o2FlowSensorOffset_.getData(rOffset);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  UpdateAirFlowSensorOffset(offset)  [static]
//
//@ Interface-Description
//  Update the stored Air Flow Sensor's offset to the value given by
//  'offset'.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none
//@ End-Method 
//===================================================================== 

void
NovRamManager::UpdateAirFlowSensorOffset(const FlowSensorOffset& offset)
{
  CALL_TRACE("UpdateAirFlowSensorOffset(offset)");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  NonVolatileMemory::P_INSTANCE_->airFlowSensorOffset_.updateData(offset);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  UpdateO2FlowSensorOffset(offset)  [static]
//
//@ Interface-Description
//  Update the stored O2 Flow Sensor's offset to the value given by
//  'offset'.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none
//@ End-Method 
//===================================================================== 

void
NovRamManager::UpdateO2FlowSensorOffset(const FlowSensorOffset& offset)
{
  CALL_TRACE("UpdateO2FlowSensorOffset(offset)");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  NonVolatileMemory::P_INSTANCE_->o2FlowSensorOffset_.updateData(offset);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetLastSstPatientCctType()  [static]
//
//@ Interface-Description
//  Return a copy of the value of the Patient Circuit Type Setting for the
//  last SST run.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none
//@ End-Method 
//===================================================================== 

DiscreteValue
NovRamManager::GetLastSstPatientCctType(void)
{
  CALL_TRACE("GetLastSstPatientCctType()");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  return(NonVolatileMemory::P_INSTANCE_->lastSstPatientCctType_.getValue());
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  UpdateLastSstPatientCctType(newPatientCctType)  [static]
//
//@ Interface-Description
//  Update the value of the last Patient Circuit Type Setting value to
//  'newPatientCctType'.  This is to be called by SST only.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  (newPatientCctType == NovRamManager::GetLastSstPatientCctType())
//@ End-Method 
//===================================================================== 

void
NovRamManager::UpdateLastSstPatientCctType(
				      const DiscreteValue newPatientCctType
					  )
{
  CALL_TRACE("UpdateLastSstPatientCctType(newPatientCctType)");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  NonVolatileMemory::P_INSTANCE_->lastSstPatientCctType_.updateValue(
							  newPatientCctType
								   );
}   // $[TI1]

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetLastSstProxFirmwareRev()  [static]
//
//@ Interface-Description
//  Return a copy of the value of the PROX Firmware Rev for the
//  last SST run in 'proxFirmwareRev'.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none
//@ End-Method 
//===================================================================== 

void
NovRamManager::GetLastSstProxFirmwareRev(BigSerialNumber& proxFirmwareRev)
{
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  NonVolatileMemory::P_INSTANCE_->proxFirmwareRevision_.getData(proxFirmwareRev);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  UpdateLastSstProxFirmwareRev(newProxFirmwareRev)  [static]
//
//@ Interface-Description
//  Update the value of the last PROX Software Revision value to
//  'newProxFirmwareRevision'.  This is to be called by SST only.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  (newProxFirmwareRev == NovRamManager::GetLastSstProxFirmwareRev())
//@ End-Method 
//===================================================================== 

void
NovRamManager::UpdateLastSstProxFirmwareRev(
				      const BigSerialNumber& newProxFirmwareRevision
					  )
{
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  NonVolatileMemory::P_INSTANCE_->proxFirmwareRevision_.updateData(
							       newProxFirmwareRevision);
}   // $[TI1]

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetLastSstProxSerialNum()  [static]
//
//@ Interface-Description
//  Return a copy of the value of the PROX Serial Number for the
//  last SST run.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none
//@ End-Method 
//===================================================================== 

void
NovRamManager::GetLastSstProxSerialNum(Uint32& proxSerialNumber)
{
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  NonVolatileMemory::P_INSTANCE_->proxSerialNumber_.getData(proxSerialNumber);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  UpdateLastSstProxSerialNum(newProxSerialNum)  [static]
//
//@ Interface-Description
//  Update the value of the last PROX Serial Number value to
//  'newProxSerialNum'.  This is to be called by SST only.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  (newProxSerialNum == NovRamManager::GetLastSstProxSerialNum())
//@ End-Method 
//===================================================================== 

void
NovRamManager::UpdateLastSstProxSerialNum(
				      const Uint32& newProxSerialNum
					  )
{
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  NonVolatileMemory::P_INSTANCE_->proxSerialNumber_.updateData(
							       newProxSerialNum);
}   // $[TI1]

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetServiceModeState(rState)  [static]
//
//@ Interface-Description
//  Return, via 'rState', the current Service-Mode State.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none
//@ End-Method 
//===================================================================== 

void
NovRamManager::GetServiceModeState(ServiceModeNovramData& rState)
{
  CALL_TRACE("GetServiceModeState(rState)");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  NonVolatileMemory::P_INSTANCE_->serviceModeState_.getData(rState);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  UpdateServiceModeState(newState)  [static]
//
//@ Interface-Description
//  Update the Service-Mode State with 'newState'.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none
//@ End-Method 
//===================================================================== 

void
NovRamManager::UpdateServiceModeState(const ServiceModeNovramData& newState)
{
  CALL_TRACE("UpdateServiceModeState(newState)");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  NonVolatileMemory::P_INSTANCE_->serviceModeState_.updateData(newState);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetAtmPressOffset(rOffset)  [static]
//
//@ Interface-Description
//  Return, via 'rOffset', the current Atmospheric Pressure Offset.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none
//@ End-Method 
//===================================================================== 

void
NovRamManager::GetAtmPressOffset(AtmPressOffset& rOffset)
{
  CALL_TRACE("GetAtmPressOffset(rOffset)");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  NonVolatileMemory::P_INSTANCE_->atmPressOffset_.getData(rOffset);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  UpdateAtmPressOffset(newOffset)  [static]
//
//@ Interface-Description
//  Update the Atmospheric Pressure Offset with 'newOffset'.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition 
//  none
//@ End-Method 
//===================================================================== 

void
NovRamManager::UpdateAtmPressOffset(const AtmPressOffset& newOffset)
{
  CALL_TRACE("UpdateAtmPressOffset(newOffset)");
  CLASS_PRE_CONDITION((NonVolatileMemory::IsInitialized_));

  NonVolatileMemory::P_INSTANCE_->atmPressOffset_.updateData(newOffset);
}   // $[TI1]

#endif // defined(SIGMA_BD_CPU)


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetNewSequenceNumber()  [static]
//
//@ Interface-Description
//  Return a unique sequence number.  This is to be used by ALL "sequenced"
//  items within NOVRAM, to prevent problems that arrive due to time
//  changes.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (NonVolatileMemory::IsInitialized_)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Uint32
NovRamManager::GetNewSequenceNumber(void)
{
  CALL_TRACE("GetNewSequenceNumber()");

  if (NonVolatileMemory::P_INSTANCE_->sequenceCount_.verifySelf() != 
      						NovRamStatus::ITEM_VALID)
  {   // $[TI1]
    // a fault occurred BEFORE the NOVRAM has been initialized/verified,
    // therefore initialize/verify the sequence cound data item before
    // continuing...
    NovRamManager::Initialize();
  }   // $[TI2]

  const Uint32  NEW_SEQUENCE_NUM =
      NonVolatileMemory::P_INSTANCE_->sequenceCount_.assignNewSequenceNum();

  return(NEW_SEQUENCE_NUM);
}
