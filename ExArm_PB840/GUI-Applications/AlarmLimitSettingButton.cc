#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: AlarmLimitSettingButton - A setting button which displays
// an alarm limit containing an alarm bitmap and either a numeric value
// or the text "OFF".
//---------------------------------------------------------------------
//@ Interface-Description
// The Alarm Limit Setting Button is used in the AlarmSlider class as the
// setting button for adjusting Alarm Limits.  The Alarm Limit setting button
// is essentially identical to NumericSettingButton except for the fact that
// it sometimes needs to display the text "OFF" rather than always displaying a
// numeric value.
//
// Once created, the Alarm Limit Setting Button normally operates without
// intervention except for needing an activate() call before being displayed
// as well as possible positional changes received from AlarmSlider.
//
// The display of this button consists of an alarm bitmap in the upper half of
// the button and a changeable value in the lower half.  This value is either
// the numeric value of a specified alarm limit or else "OFF".  A unique value
// of the alarm limit (defined in the Settings-Validation subsystem) indicates
// that the alarm limit is off (will not be used).  This special value will
// trigger the display of "OFF".
//
// anytime our setting changes.  We update the setting display accordingly.
// anytime our setting changes.  We update the setting display accordingly.
//---------------------------------------------------------------------
//@ Rationale
// Contains all the functionality for adjusting an alarm limit setting in
// a single class.
//---------------------------------------------------------------------
//@ Implementation-Description
// Most of the features of the Alarm Limit setting button (size, color, title)
// are fixed and are not needed as construction parameters.  All that is
// required for construction is the position of the button, the id of the
// alarm limit setting which this button will adjust and the help message
// displayed when this button is pressed down.
//
// After construction, most of the operation of this setting button is handled
// by the parent SettingButton class.  All AlarmLimitSettingButton really
// needs to do is to react to changes of the setting which it controls and
// update the numeric/text value display accordingly.  Because it is registered
// as a target of this setting, the valueUpdate() method will be
// called when the setting changes.  This method handles the updating of the
// alarm limit value.
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and pre-conditions
// to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/AlarmLimitSettingButton.ccv   25.0.4.0   19 Nov 2013 14:07:32   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006  By:  hhd	   Date:  24-May-1999    DR Number: 5369 
//    Project:  Sigma (R8027)
//    Description:
//		Removed references to Button::setButtonType() (empty) method.
//
//  Revision: 005  By:  sah	   Date:  23-Feb-1999    DCS Number:  5314
//  Project:  ATC
//  Description:
//      Added integrated with new dynamic, setting applicability functionality.
//      This allows for looser coupling between settings and GUI, and the
//      need for significantly less "setting knowledge" in the GUI subsystem.
//
//  Revision: 004  By:  sah	   Date:  11-Jan-1999    DCS Number: 5321
//  Project:  ATC
//  Description:
//		Added 'SIGMA_DEVELOPMENT' conditionals around this class (unused)
//		'SoftFault()' method.
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "Sigma.hh"
#include "Colors.hh"
#include "AlarmLimitSettingButton.hh"
#include "Sigma.hh"
#include "MiscStrs.hh"

//@ Usage-Classes
#include "SettingConstants.hh"
#include "SettingSubject.hh"
//@ End-Usage

//@ Code...

// Initialize static constants.
const Int32 AlarmLimitSettingButton::BUTTON_WIDTH = 49;
const Int32 AlarmLimitSettingButton::BUTTON_HEIGHT = 37;

static const Int32 BUTTON_BORDER_ = 2;
static const Int32 NUMERIC_VALUE_X_ = 0; 
static const Int32 NUMERIC_VALUE_Y_ = 14; 
static const Int32 NUMERIC_VALUE_WIDTH_ =
				AlarmLimitSettingButton::BUTTON_WIDTH
				- 2 * (BUTTON_BORDER_ + 1);
					// Add 1 to the border above due to buttons having extra
					// 1 pixel black box surrounding the button border and
					// interior.
static const Int32 NUMERIC_VALUE_HEIGHT_ =
				AlarmLimitSettingButton::BUTTON_HEIGHT 
				- 2 * (BUTTON_BORDER_ + 1) - NUMERIC_VALUE_Y_ ;


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: AlarmLimitSettingButton()  [Constructor]
//
//@ Interface-Description
// Creates an Alarm Limit Setting Button. Passed the following arguments:
// >Von
//	xOrg			The X co-ordinate of the button.
//	yOrg			The Y co-ordinate of the button.
//	settingId		The setting whose value the button displays and modifies.
//	messageId		The help message displayed in the Message Area when this
//					button is pressed down.
//>Voff
//---------------------------------------------------------------------
//@ Implementation-Description
// Passes parameters along to SettingButton and then sets up the numeric
// value field.
//
// $[01047] Alarm limit buttons display an alarm symbol icon
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

AlarmLimitSettingButton::AlarmLimitSettingButton(Uint16 xOrg, Uint16 yOrg,
					SettingId::SettingIdType settingId, StringId messageId,
					SubScreen * pFocusSubScreen) :
			SettingButton(xOrg, yOrg, BUTTON_WIDTH, BUTTON_HEIGHT,
							Button::LIGHT, BUTTON_BORDER_,
							Button::ROUND, Button::NO_BORDER,
							MiscStrs::ALARM_LIMIT_SETTING_BUTTON_TITLE,
							settingId, pFocusSubScreen, messageId),
			numericValue_(TextFont::TWELVE, 0, CENTER)
							// Set number of digits to 0 above as we will
							// be sizing the numeric field ourselves rather
							// than letting GUI-Foundation setting it to
							// a default value.
{
	CALL_TRACE("AlarmLimitSettingButton::AlarmLimitSettingButton(Uint16 xOrg, "
					"Uint16 yOrg, SettingId::SettingIdType settingId, "
					"StringId messageId)");

	// Position and size the numeric value field
	numericValue_.setX(NUMERIC_VALUE_X_);
	numericValue_.setY(NUMERIC_VALUE_Y_);
	numericValue_.setWidth(NUMERIC_VALUE_WIDTH_);
	numericValue_.setHeight(NUMERIC_VALUE_HEIGHT_);		// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~AlarmLimitSettingButton  [Destructor]
//
//@ Interface-Description
// Destroys an AlarmLimitSettingButton.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

AlarmLimitSettingButton::~AlarmLimitSettingButton(void)
{
	CALL_TRACE("AlarmLimitSettingButton::~AlarmLimitSettingButton(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateDisplay_ [Protected, virtual]
//
//@ Interface-Description
// Called when the display of this setting button needs to be updated with
// the latest setting value.  Passed a setting context id informing us the
// setting context from which we should retrieve the new setting value.
//---------------------------------------------------------------------
//@ Implementation-Description
// If the setting's current value is LOWER_ALARM_LIMIT_OFF or
// UPPER_ALARM_LIMIT_OFF then this signifies that the alarm limit has been
// switched off and, in this case, we display the text "OFF" in the button.
// Otherwise we display the numeric value.
//
// $[01045] Alarm limit button displays the value of the alarm limit setting.
// $[01048] Alarm limit button displays OFF when ...
//---------------------------------------------------------------------
//@ PreCondition
// qualifierId must be a valid value.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
AlarmLimitSettingButton::updateDisplay_(Notification::ChangeQualifier qualifierId,
										const SettingSubject*         pSubject)
{
	CALL_TRACE("updateDisplay_(qualifierId, pSubject)");

	// Get the current value and precision from the setting
	BoundedValue settingValue = pSubject->getAdjustedValue();

	// Check whether the setting has been switched off
	if (settingValue.value == SettingConstants::LOWER_ALARM_LIMIT_OFF ||
		settingValue.value == SettingConstants::UPPER_ALARM_LIMIT_OFF)
	{													// $[TI1]
		// Alarm limit switched off so make sure numeric value is not
		// displayed and add the OFF string.
		removeLabel(&numericValue_);

		// Before adding decide whether to display OFF in italics or not
		// (depending on whether the setting has changed or not)
		if (pSubject->isChanged())
		{												// $[TI2]
			offValue_.setText(MiscStrs::ALARM_LIMIT_OFF_CHANGED);
			offValue_.setHighlighted(TRUE);
		}
		else
		{												// $[TI3]
			offValue_.setText(MiscStrs::ALARM_LIMIT_OFF);
			offValue_.setHighlighted(FALSE);
		}

		// Add OFF string
		addLabel(&offValue_);
	}
	else
	{													// $[TI4]
		// Alarm limit enabled so make sure OFF value is not displayed and add
		// the numeric value field.
		removeLabel(&offValue_);

		// Set the value and precision of the field
		numericValue_.setValue(settingValue.value);
		numericValue_.setPrecision(settingValue.precision);

		// Before adding decide whether to display the value in italics or not
		// (depending on whether the setting has changed or not)
		if (pSubject->isChanged())
		{												// $[TI5]
			numericValue_.setItalic(TRUE);
			numericValue_.setHighlighted(TRUE);
		}
		else
		{												// $[TI6]
			numericValue_.setItalic(FALSE);
			numericValue_.setHighlighted(FALSE);
		}

		// Add numeric value
		addLabel(&numericValue_);
	}
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
AlarmLimitSettingButton::SoftFault(const SoftFaultID  softFaultID,
		   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS,
				ALARMLIMITSETTINGBUTTON, lineNumber, pFileName, pPredicate);
}
