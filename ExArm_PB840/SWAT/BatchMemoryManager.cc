#include "stdafx.h"
//-------------------------------------------------------------------------------
//                        Copyright (c) 2010 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//-------------------------------------------------------------------------------
#include "BatchMemoryManager.h"
#include <cstring>

namespace swat{

    /////////////////////////////////////////////////////////////
    // BatchMemoryManager::BatchMemoryManager
    /////////////////////////////////////////////////////////////
    BatchMemoryManager::BatchMemoryManager(unsigned int iSlotLength,unsigned int iSlotMask,unsigned int iSlotMin):
        m_pSlotMemoryHeaderArray(0),
        m_iSlotLength(iSlotLength),
        m_iSlotMask(iSlotMask),
        m_iSlotMin(iSlotMin)
    {
        m_pSlotMemoryHeaderArray =
            (SlotMemoryHeader **)new char[sizeof(SlotMemoryHeader *)*iSlotLength];

        memset(m_pSlotMemoryHeaderArray,0,sizeof(SlotMemoryHeader *)*iSlotLength);
    }

    /////////////////////////////////////////////////////////////
    // BatchMemoryManager::~BatchMemoryManager
    /////////////////////////////////////////////////////////////
    BatchMemoryManager::~BatchMemoryManager()
    {
        for(int i=0;i<m_iSlotLength;i++)
        {
            if(m_pSlotMemoryHeaderArray[i]!=0)
            {
                delete [] m_pSlotMemoryHeaderArray[i];
            }
        }

        delete [] (m_pSlotMemoryHeaderArray);
    }

    /////////////////////////////////////////////////////////////
    // BatchMemoryManager::allocate
    /////////////////////////////////////////////////////////////
    void * BatchMemoryManager::allocate(unsigned int iLength)
    {
        bool bIsFreeSlot=false;
        unsigned int iFreeSlotIndex = 0 ;
        unsigned int iMaxSlotSize=0;

        //Align the length, increase mask to have greator chance of recycling.
        iLength=(((iLength==0)?1:iLength)+m_iSlotMask)&~m_iSlotMask;

        //Cycle through all the slots.
        for(int i=0;i<m_iSlotLength;i++)
        {
            //If slot element does not exsist, create it.
            if(m_pSlotMemoryHeaderArray[i]==0)
            {

                m_pSlotMemoryHeaderArray[i]=(SlotMemoryHeader *)new char[iLength+sizeof(SlotMemoryHeader)];
                m_pSlotMemoryHeaderArray[i]->m_iSlotId      = i;
                m_pSlotMemoryHeaderArray[i]->m_iBatchLength = iLength;
                m_pSlotMemoryHeaderArray[i]->m_bfreeSlot    = false;

                return ((char *)m_pSlotMemoryHeaderArray[i])+sizeof(SlotMemoryHeader);
            }

            //If slot is free, it is a candidate for reuse.
            else if(m_pSlotMemoryHeaderArray[i]->m_bfreeSlot == true )
            {

                //If free slot is large enough
                //recycle it.
                if(m_pSlotMemoryHeaderArray[i]->m_iBatchLength>=iLength)
                {
                    m_pSlotMemoryHeaderArray[i]->m_bfreeSlot    = false;

                    return ((char *)m_pSlotMemoryHeaderArray[i])+sizeof(SlotMemoryHeader);
                }

                //Otherwise find the biggest free slot candidate to reallocate
                //to larger size.
                else if(iMaxSlotSize < m_pSlotMemoryHeaderArray[i]->m_iBatchLength)
                {
                    iMaxSlotSize   = m_pSlotMemoryHeaderArray[i]->m_iBatchLength;
                    bIsFreeSlot    = true;
                    iFreeSlotIndex = i   ;
                }
            }
        }

        //If there is a free memory slot and was not able to reuse recycle memory,
        //forcebly reallocate the memory slot.
        if(bIsFreeSlot)
        {
            SlotMemoryHeader *pSlotMemoryHeader;
            pSlotMemoryHeader      = (SlotMemoryHeader *)new char[iLength+sizeof(SlotMemoryHeader)];

            if(pSlotMemoryHeader == 0)
            {
                return 0;
            }


            delete [] (m_pSlotMemoryHeaderArray[iFreeSlotIndex]);
            m_pSlotMemoryHeaderArray[iFreeSlotIndex]=0;

            m_pSlotMemoryHeaderArray[iFreeSlotIndex] = pSlotMemoryHeader;

            m_pSlotMemoryHeaderArray[iFreeSlotIndex]->m_iSlotId      = iFreeSlotIndex;
            m_pSlotMemoryHeaderArray[iFreeSlotIndex]->m_iBatchLength = iLength;
            m_pSlotMemoryHeaderArray[iFreeSlotIndex]->m_bfreeSlot    = false;
            return ((char *)m_pSlotMemoryHeaderArray[iFreeSlotIndex])+sizeof(SlotMemoryHeader);
        }


        return 0;
    }

    /////////////////////////////////////////////////////////////
    // BatchMemoryManager::free
    /////////////////////////////////////////////////////////////
    void BatchMemoryManager::free(void *pIn)
    {
        SlotMemoryHeader *pTemp=(SlotMemoryHeader *)(((char *)pIn)-sizeof(SlotMemoryHeader));
        pTemp->m_bfreeSlot=true;
    }



}







