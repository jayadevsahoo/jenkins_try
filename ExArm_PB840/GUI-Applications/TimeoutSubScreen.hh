
#ifndef TimeoutSubScreen_HH
#define TimeoutSubScreen_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: TimeoutSubScreen 
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/TimeoutSubScreen.hhv   25.0.4.0   19 Nov 2013 14:08:36   pvcs  $
//
//  @ Modification-Log
//
//  Revision: 001  By:  ksg    Date:  19-June-2007    SCR Number: 6237
//  Project:  Trend
//  Description: Initial version.
// 
//====================================================================


#include "BatchSettingsSubScreen.hh"
#include "SubScreenArea.hh"

class TimeoutSubScreen : public BatchSettingsSubScreen
{
public:
	TimeoutSubScreen(void);                    
	~TimeoutSubScreen(void);
	virtual void timerEventHappened(GuiTimerId::GuiTimerIdType timerId);
	virtual void acceptHappened(void);
	static void Initialize( void );
	static TimeoutSubScreen & GetTimeoutSubScreen( void );
	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:
	virtual void  activateHappened_  (void);
	virtual void  deactivateHappened_(void);
	virtual void  valueUpdateHappened_(const BatchSettingsSubScreen::TransitionId_ transitionId,
									   const Notification::ChangeQualifier         qualifierId,
									   const ContextSubject*                       pSubject);
private:

	// These methods are purposely declared, but not implemented...
	TimeoutSubScreen(SubScreenArea *pSubScreenArea); // not implemented...
	TimeoutSubScreen(const TimeoutSubScreen&);// not implemented...
	void   operator=(const TimeoutSubScreen&);	// not implemented...

	//@ Data-Member: nullSubScreenArea_
	// Null address provided to the base class.
	SubScreenArea nullSubScreenArea_;

};

#endif // TimeoutSubScreen_HH 

