;=====================================================================
; This is a proprietary work to which Puritan-Bennett corporation of
; California claims exclusive right.  No part of this work may be used,
; disclosed, reproduced, sorted in an information retrieval system, or
; transmitted by any means, electronic, mechanical, photocopying,
; recording, or otherwise without the prior written permission of
; Puritan-Bennett Corporation of California.
;
;            Copyright (c) 1995, Puritan-Bennett Corporation
;=====================================================================
;
;
;============================ M O D U L E   D E S C R I P T I O N ====
;@ Filename: InstallHandler - Install Interrupt Handler
;
;---------------------------------------------------------------------
;@ Interface-Description
;  This module contains the assembly language routine to install the 
;  interrupt handlers in the EVT
;
;  Contents:
;     InstallHandler         - Connects a handler to an interrupt vector
;     
;---------------------------------------------------------------------
;@ Implementation-Description
;  See Interface-Description.
;---------------------------------------------------------------------
;@ Restrictions
;  None
;---------------------------------------------------------------------
;@ End-Preamble
;
;@ Version
;
;@(#) $Header:   /840/Baseline/POST-Library/vcssrc/InstallHandler.s_v   25.0.4.0   19 Nov 2013 14:16:38   pvcs  $
;
;@ Modification-Log
;
;  Revision: 003   By: Gary Cederquist  Date: 15-AUG-1997  DR Number: 1875
;    Project:   Sigma   (R8027)
;    Description:
;       Added testable item annotations.
;
;  Revision: 002    By: Gary Cederquist  Date: 16-MAY-1997  DR Number: 2121
;    Project: Sigma (R8027)
;    Description:
;       Removed assembler options specified in source in favor of command
;       line options.
;
;  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997  DR Number:
;    Project: Sigma (R8027)
;    Description:
;       Initial version
;
;=====================================================================

   XDEF        _InstallHandler__FUiPFv_v

;============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
;@ Free-Function: InstallHandler - utility for connecting a handler to
;                                  an interrupt vector
;
;@ Interface-Description
;
;  This routine takes the vector number specified and places the passed in
;  pointer to an interrupt handler in to the appropriate place in the
;  vector table.  So when an interrupt with the specified number
;  occurs, the routine pointed at by the specified pointer will be called.
;                                      
;  Note: The routine pointed to MUST be an interrupt handler.
;
;  Note: This routine can only be used in supervisor mode.
;
;    Inputs:  C callable
;        InstallHandler( Uint8 vector_number, 
;                        void (*Handler)(void) handler)
;
;           vector_number - the number of the vector in the vector table
;           handler       - the interrupt handler pointer
;
;    Outputs: NONE
;
;    Returns: 
;        Pointer to the old exception handler 
;
;---------------------------------------------------------------------
;@ Implementation-Description
;
;  Retrieves the current handler address from the EVT and substitutes 
;  the new one.  Returns the old handler address in register D0, the 
;  standard function return register.
;
;  REGISTER USAGE: Modified: a0.l d0.l d1.l
;                      Used: a6.l
;
;---------------------------------------------------------------------
;@ PreCondition
;    None
;---------------------------------------------------------------------
;@ PostCondition
;    None
;
;@ End-Free-Function
;=====================================================================

      SECTION  code,,C     

; void   InstallHandler( Uint8 vector_number, void (*Handler)(void) handler) {
_InstallHandler__FUiPFv_v:
      link     a6,#0
      move.l   (8,a6),d1        ; get the vector number
      asl.l    #2,d1            ; offset = 4 * vector number
      movec.l  VBR,a0           ; vector base register
      move.l   (d1,a0),d0       ; the current vector
   
      move.l   (12,a6),(d1,a0)  ; install the new handler
      unlk     a6
      rts
; // $[TI1]
; }

      END


