#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: DisconnectScheduler - handles the ventilator state after
// disconnect detection, to allow for disconnect reset detection.
//---------------------------------------------------------------------
//@ Interface-Description
// The DisconnectScheduler is derived from an IdleModeScheduler.
// It sets up the next breath, determines the next scheduler
// when control is released, and validates the transition from the
// previous scheduler when it takes control.
//---------------------------------------------------------------------
//@ Rationale
// When a patient is disconnected from the ventilator, the disconnect
// scheduler is responsible to handle gas delivery so that reconnection
// can be detected.
//---------------------------------------------------------------------
//@ Implementation-Description
// The scheduler implements its functionality by defining all the protected
// pure virtual methods declared in its base class.  The class collaborates
// with the hadle PendingContextHandle to phase in new settings, and creates a
// new breath record using the object RBreathSet.
//---------------------------------------------------------------------
//@ Fault-Handling
// n/a
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
//
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/DisconnectScheduler.ccv   25.0.4.0   19 Nov 2013 13:59:46   pvcs  $
//
//@ Modification-Log
//
//   Revision 019   By: rhj    Date: 26-Jan-2009    SCR Number:  6452
//   Project:  840S
//   Description:
//      Added a condition to post BDALARM_DISCONNECT_WITH_COMPRESSOR 
//      when disconnect sensitivity is limited by the compressor.
// 
//  Revision: 018   By: gdc   Date:  22-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Added case to reject RM maneuver requests during disconnect.
//
//  Revision: 002  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 016  By:  iv    Date:  18-Jan-1998    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Bilevel initial version.
//             Added handling of inspiratory pause event.
//
//  Revision: 015  By: iv    Date:  22-Oct-1997    DR Number: DCS 1649
//  	Project:  Sigma (840)
//		Description:
//          Eliminate interaction with RDisconnectTrigger.
//
//  Revision: 014  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 013  By:  iv    Date:  03-Apr-1997    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Rework per formal code review for BD Schedulers.
//
//  Revision: 012  By:  iv    Date:  24-Jan-1997    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Rework per formal code review.
//
//  Revision: 011  By:  iv    Date:  04-Dec-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Cleanup for code inspection.
//
//  Revision: 010  By:  iv    Date:  15-Nov-1996    DR Number: DCS 1559
//       Project:  Sigma (R8027)
//       Description:
//             Replaced safe class assertion with class assertion in constructor.
//
//  Revision: 009  By:  iv    Date: 09-Oct-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             DetermineNextScheduler - Added a call:
//                      rDisconnectTrigger.setForcedDisconnect(FALSE)
//
//  Revision: 008 By:  iv   Date:   12-July-1996    DR Number: DCS 951
//       Project:  Sigma (R8027)
//       Description:
//             Eliminate the transition back to Standby.
//
//  Revision: 007 By:  iv   Date:   04-June-1996    DR Number: DCS 600 
//       Project:  Sigma (R8027)
//       Description:
//             Changed the handling of transitioning back to apnea, SafetyPcv
//             and Standby.
//             Eliminated the calls to the base class for takeControl() and
//             UpdateSchedulerId().
//             Eliminated the argument from the method validateScheduler_()
//
//  Revision: 006 By:  iv   Date:   15-Apr-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Eliminated O2_MONITOR_CALIBRATE case in reportEventStatus_().
//
//  Revision: 005 By:  iv   Date:   21-Feb-1996    DR Number: DCS 674, 672 
//       Project:  Sigma (R8027)
//       Description:
//             Removed #ifndef directives surrounding #include directives.
//             Allow transition back from Svo and short power interruption
//             to disconnect in validateSchedule_().
//
//  Revision: 004 By:  iv   Date:  13-Dec-1995    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Changed CheckIfApneaPossible_() to CheckIfApneaPossible()
//
//  Revision: 003 By:  kam   Date:  13-Dec-1995    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Modified interface for reporting ventilator status.  Vent
//             and user event status are posted to the BD Status task
//             via the method VentAndUserEventStatus::PostEventStatus().
//             The BD status task is then responsible for transmitting
//             the status information to the GUI through NetworkApps.
//
//             Also, updated SST_CONFIRMATION case of reportEventStatus_() and
//             added O2_MONITOR_CALIBRATE.  Added call to base class
//             takeControl() from validateScheduler_().
//
//  Revision: 002 By:  kam   Date:  25-Sep-1995    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Updated for interface with Alarm-Analysis subsystem
//             and for BD-GUI user event request and event status interface.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#  include "DisconnectScheduler.hh"
#  include "ModeTriggerRefs.hh"
#  include "BreathMiscRefs.hh"
#  include "TriggersRefs.hh"
#  include "SchedulerRefs.hh"
#  include "BdDiscreteValues.hh"

//@ Usage-Classes
#  include "BreathSet.hh"
#  include "PendingContextHandle.hh"
#  include "PhasedInContextHandle.hh"
#  include "PhaseInEvent.hh"
#  include "BreathTriggerMediator.hh"
#  include "BdAlarms.hh"
#  include "VentAndUserEventStatus.hh"
#  include "BdSystemStateHandler.hh"
#  include "LeakCompMgr.hh"

#ifdef INTEGRATION_TEST_ENABLE
#if defined (SIGMA_BD_CPU)
#include "EventManager.h"
#endif // SIGMA_BD_CPU
#endif // INTEGRATION_TEST_ENABLE

//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: DisconnectScheduler()  [Default Constructor]
//
//@ Interface-Description
// $[04119] $[04125] $[04130] $[04141]
// The constructor takes no arguments. It constructs the object with the proper id.
// The list of valid mode triggers, used by the mode trigger mediator is
// initialized as well. The triggers are ordered based on priority - the most urgent
// is first.
//---------------------------------------------------------------------
//@ Implementation-Description
// $[04215] The constructor takes no arguments. It invokes the base class
// constructor with the DISCONNECT id argument. It initializes the
// private data member pModeTriggerList_.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	The number of elements on the trigger list is asserted
//  to be MAX_NUM_DISCONNECT_TRIGGERS and MAX_NUM_DISCONNECT_TRIGGERS to be less than
//  MAX_MODE_TRIGGERS.
//@ End-Method
//=====================================================================
static Int32 const MAX_NUM_DISCONNECT_TRIGGERS = 3;
DisconnectScheduler::DisconnectScheduler(void):
					IdleModeScheduler(SchedulerId::DISCONNECT)
{
// $[TI1]
  CALL_TRACE("DisconnectScheduler()");
  	//$[04125] $[04141] $[04130] 

	Int32 ii = 0;
	pModeTriggerList_[ii++] = (ModeTrigger*)&RSvoTrigger;
	pModeTriggerList_[ii++] = (ModeTrigger*)&RDiscAutoResetTrigger;
	pModeTriggerList_[ii++] = (ModeTrigger*)&RManualResetTrigger;
	pModeTriggerList_[ii] = (ModeTrigger*)NULL;

	CLASS_ASSERTION(ii == MAX_NUM_DISCONNECT_TRIGGERS &&
						MAX_NUM_DISCONNECT_TRIGGERS < MAX_MODE_TRIGGERS);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~DisconnectScheduler()  [Destructor]
//
//@ Interface-Description
// Destructor
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

DisconnectScheduler::~DisconnectScheduler(void)
{
  CALL_TRACE("~DisconnectScheduler()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
DisconnectScheduler::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, DISCONNECTSCHEDULER,
                          lineNumber, pFileName, pPredicate);
}



//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setupBreath_
//
//@ Interface-Description
// The method takes no arguments and has no return value.
// It is invoked by the base class method determineBreathPhase().
// It creates a new breath record. 
//---------------------------------------------------------------------
//@ Implementation-Description
// The method uses the object RBreathSet to create a new breath
// record.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
DisconnectScheduler::setupBreath_(void)
{
// $[TI1]
	CALL_TRACE("DisconnectScheduler::setupBreath_(void)");

	//Update the breath record
 	RBreathSet.newBreathRecord(SchedulerId::DISCONNECT,
 						::NULL_MANDATORY_TYPE,    // no mandatory type is specified
 						NON_MEASURED,    // breath type
 						BreathPhaseType::NON_BREATHING
	);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: determineNextScheduler_
//
//@ Interface-Description
// This method is invoked by the base class method relinquishControl().  It
// accepts a TriggerId and a Boolean, signifying vent setup completion, as
// arguments and has no return value.  If new patient setup has been completed,
// the next scheduler may be either apnea or the set mode. Apnea may take
// control if it is possible and was active prior to the disconnection and no
// manual reset has been detected.  Return to the set mode takes place if
// manual reset has been detected. If new patient setup has not been completed,
// Sigma starts safety pcv mode of breathing. The disconnect scheduler may
// relinquish control to the Svo scheduler if Svo conditions are detected.  To
// immediately start a breath upon disconnect reset, an immediate breath
// trigger is enabled.  This method interfaces with BdAlarms to post the
// Disconnect reset condition, which is then passed to the Alarm-Analysis
// subsystem, if the disconnect condition has manually or automatically reset.
// The VentAndUserEventStatus class is also notified of the Disconnect status
// so that it can post the status to the BD Status Task. The forced disconnect
// flag is reset here.
//---------------------------------------------------------------------
//@ Implementation-Description
// The trigger id is checked to determine the next scheduler.  To determine the
// set mode, settings are phased-in using the handle PendingContextHandle.  The
// Boolean argument passed in is checked to determine whether vent setup has
// been completed. If the mode trigger id is DISCONNET_AUTORESET, apnea may
// start if current settings permit apnea detection and if the previous
// scheduler was apnea.  If vent setup has not been completed, the next
// scheduled mode is safety pcv.  The immediate breath trigger is enabled,
// using the method enable() in the instance variable rImmediateBreathTrigger.
// Once the next scheduler is determined, it is instructed to take control.
// rBdAlarms.postBdAlarm() is invoked to notify Alarms when the Disconnect
// condition has reset and VentAndUserEventStatus::PostEventStatus() is invoked
// to notify the BD Status Task of the exit from Disconnect.
//---------------------------------------------------------------------
//@ PreCondition
// the trigger id is checked to be in the following range:
// DISCONNECT_AUTORESET, MANUAL_RESET, SVO
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
DisconnectScheduler::determineNextScheduler_(const Trigger::TriggerId triggerId,
						const Boolean isVentSetupComplete) 
{
	CALL_TRACE("DisconnectScheduler::determineNextScheduler_(\
						const Trigger::TriggerId triggerId,\
						const Boolean isVentSetupComplete)");

	BreathPhaseScheduler* pNextScheduler = NULL;
	 
	// check if patient circuit reconnect:
	if( (triggerId == Trigger::DISCONNECT_AUTORESET) ||
		(triggerId == Trigger::MANUAL_RESET) )
	{
	// $[TI1]
		// $[04149], $[05165], $[05166] 
		// notify alarms on disconnect reset; both STARTUP_DISCONNECT and DISCONNECT
		// reset are posted
		RBdAlarms.postBdAlarm (BdAlarmId::BDALARM_NOT_DISCONNECT);
		RBdAlarms.postBdAlarm (BdAlarmId::BDALARM_NOT_STARTUP_DISCONNECT);

		// notify BD Status Task of patient connection
		VentAndUserEventStatus::PostEventStatus (EventData::PATIENT_CONNECT, EventData::ACTIVE);

		// recovery from disconnect is to the current set mode, or apnea, or safety pcv.
		if (isVentSetupComplete)
		{
		// $[TI1.1]
			// phase-in the new vent settings (for start of inspiration):
			PhasedInContextHandle::PhaseInPendingBatch(PhaseInEvent::START_OF_INSPIRATION);

#ifdef INTEGRATION_TEST_ENABLE
			// Signal Event for swat
			swat::EventManager::signalEvent(swat::EventManager::START_OF_INSP);
#endif // INTEGRATION_TEST_ENABLE

			pNextScheduler = &BreathPhaseScheduler::EvaluateSetScheduler();
			// if auto reset, check if should transfer control to apnea scheduler:
			if( triggerId == Trigger::DISCONNECT_AUTORESET &&
				BdSystemStateHandler::IsApneaActive() &&
				BreathPhaseScheduler::CheckIfApneaPossible() )
			{
			// $[TI1.1.1]
				pNextScheduler = (BreathPhaseScheduler*)&RApneaScheduler;
			}// $[TI1.1.2]
		}
		else // if vent setup not complete:
		{
		// $[TI1.2]
			pNextScheduler = (BreathPhaseScheduler*)&RSafetyPcvScheduler;
		}
	}
	else if(triggerId == Trigger::SVO)
	{
	// $[TI2]
		pNextScheduler = (BreathPhaseScheduler*)&RSvoScheduler;
	}
	else
	{
	// $[TI3]
		CLASS_PRE_CONDITION( (triggerId == Trigger::DISCONNECT_AUTORESET) ||
			(triggerId == Trigger::MANUAL_RESET) ||
			(triggerId == Trigger::SVO) );
	}
	// disable all triggers on the breath trigger list.
	RBreathTriggerMediator.resetTriggerList();
	// enable an immediate breath trigger.
	((Trigger&)RImmediateBreathTrigger).enable();
	
	pNextScheduler->takeControl(*this);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: validateScheduler_
//
//@ Interface-Description
// This method is invoked by the base class method takeControl().  It takes no
// arguments and has no return value. It checks to ensure the validity of the
// scheduler.  It also interfaces with BdAlarms to post the Disconnect
// condition, which is then passed to the Alarm-Analysis subsystem.  The BD
// Status Task is also notified of the Disconnect status through the
// VentAndUserEventStatus::PostEventStatus() method.
//---------------------------------------------------------------------
//@ Implementation-Description
// The id of the previous scheduler is checked for proper pre-conditions.
// rBdAlarms.postBdAlarm() is called to log the Disconnect condition and
// VentAndUserEventStatus::PostEventStatus() is invoked to notify the
// BD Status task of the start of Disconnect.
//---------------------------------------------------------------------
//@ PreCondition
//	The previous scheduler id is checked to be one of the following:
//	AC, SIMV, SPONT, APNEA, POWER_UP, SVO, SAFETY_PCV
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
DisconnectScheduler::validateScheduler_(void) 
{
// $[TI1]
	CALL_TRACE("DisconnectScheduler::validateScheduler_(void)");

    //  $[05165], $[05166] 
	// notify alarms on disconnect detection

	// When the disconnect sensitivity is limited due to the compressor,
	// post a disconnect alarm with an additional remedy message.
	// Otherwise use the original disconnect alarm message.
	if (RLeakCompMgr.isDiscoSensLimitByComp())
	{
		RBdAlarms.postBdAlarm (BdAlarmId::BDALARM_DISCONNECT_WITH_COMPRESSOR);
	}
	else
	{
		RBdAlarms.postBdAlarm (BdAlarmId::BDALARM_DISCONNECT);
	}

	// notify BD Status Task of loss of patient connection
	VentAndUserEventStatus::PostEventStatus (EventData::PATIENT_CONNECT, EventData::CANCEL);

    SchedulerId::SchedulerIdValue id = PPreviousScheduler_->getId();	
	// $[04141]
 	CLASS_PRE_CONDITION(
		(id == SchedulerId::AC) ||
		(id == SchedulerId::SIMV) ||
		(id == SchedulerId::SPONT) ||
		(id == SchedulerId::BILEVEL) ||
		(id == SchedulerId::APNEA) ||
		(id == SchedulerId::POWER_UP) ||
		(id == SchedulerId::SVO) ||
		(id == SchedulerId::SAFETY_PCV) );
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: reportEventStatus_
//
//@ Interface-Description
// The method accepts a EventId and a Boolean for the event status as
// arguments.  It returns a EventStatus. The method handles user events
// like manual inspiration, alarm reset, expiratory pause, etc.  The method
// allows for any user event to be either enabled or disabled.
//---------------------------------------------------------------------
//@ Implementation-Description
// $[01247] $[04215] $[01255] $[05016]
// The argument of type EventId indicates which user event is active.  The
// Boolean type argument, eventState, specifies whether user event is enabled
// (TRUE) or disabled (FALSE). A simple switch statement implements the
// response for the different user events. The manual inspiration event is rejected by
// invoking a call to the static method:
// BreathPhaseScheduler::RejectManualInspiration_(eventStatus).
// The alarm reset event is accepted by invoking a call to the static method:
// BreathPhaseScheduler::AcceptAlarmReset_(eventStatus).
// The expiratory and inspiratory pause events are rejected by invoking a call to
// the static methods: BreathPhaseScheduler::RejectExpiratoryPause_(eventStatus) and
// BreathPhaseScheduler::RejectInspiratoryPause_(eventStatus) .
//---------------------------------------------------------------------
//@ PreCondition
// The user event id is checked to be within a valid range.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

EventData::EventStatus
DisconnectScheduler::reportEventStatus_(const EventData::EventId id,
													 const Boolean eventStatus)
{
	CALL_TRACE("DisconnectScheduler::reportEventStatus_(const EventData::EventId id,\
													 const Boolean eventStatus)");

	EventData::EventStatus rtnStatus = EventData::IDLE;

	switch (id)
	{
		case EventData::MANUAL_INSPIRATION:
		// $[TI1]
			rtnStatus =
					BreathPhaseScheduler::RejectManualInspiration_(eventStatus);
			break;

		case EventData::ALARM_RESET:
		// $[TI2]
			rtnStatus =
					BreathPhaseScheduler::AcceptAlarmReset_(eventStatus);
			break;

		case EventData::EXPIRATORY_PAUSE:
		// $[TI3]
			rtnStatus =
					BreathPhaseScheduler::RejectExpiratoryPause_(eventStatus);
			break;

		case EventData::INSPIRATORY_PAUSE:
		// $[TI4]
		// inspiratory pause shall not be active
		// $[BL04007]
			rtnStatus =
					BreathPhaseScheduler::RejectInspiratoryPause_(eventStatus);
			break;

		case EventData::NIF_MANEUVER:
		case EventData::P100_MANEUVER:
		case EventData::VITAL_CAPACITY_MANEUVER:
			rtnStatus = EventData::REJECTED;
			break;

		default:
		// $[TI6]
            AUX_CLASS_ASSERTION_FAILURE(id);
	}	
	return (rtnStatus);
}


//=====================================================================
//
//  Private Methods...
//
//=====================================================================





