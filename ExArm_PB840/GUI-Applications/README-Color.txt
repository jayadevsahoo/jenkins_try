Date: 29-Jun-1998
Author: Gary Cederquist

Color version change summary:

1.  Used larger font sizes in most setup subscreens and the
    breath timing diagram.

2.  Changed to use tab button titles instead of ambiguous icons
    on lower screen.
 
3. S tandardized button appearance to convey same level of 
    importance for all settings.  (eg. "Humidification Type"
    setting is same size and level as "O2 Sensor")

4.  Used color as well as italics to indicate "changed" setting.

5.  Alarm messages indicate urgency using color.
	a. Red with flashing text - high urgency
	b. Yellow with flashing text - Medium urgency
	b. Yellow with non-flashing text - Low urgency

6.  Alarm setup sliders changed to indicate alarm urgency using color.

7.  Alarm limit violations are indicated by flashing the alarm symbol
    on the alarm limit pointer.

8.  Changed from grayscale to color.

9.  Scrollable logs now use the knob to scroll instead of the
    scrollbar arrows which have been removed.

10. Increased size of date field in logs where date field was
    getting cut off (in some foreign languages).

11. Many text and numeric fields are now centered automatically
    using the Drawable::positionInContainer() method. Cheap
    text that is automatically centered should use x=0 specification.

12. The "PROCEED" key has been removed from all subscreens.

13. Tri-state buttons have been replaced with activate on touch.
    This introduces the TOUCH_RELEASE_TIMEOUT to the VentSetupSubScreen
    and ApneaSetupSubScreen which use the timeout to wait until
    the touch screen is unblocked before displaying "navigation
    buttons". This prevents the user from accidentally activating
    another screen prior to lifting his finger.

14. When pressed, the padlocks on the Vent Setup SubScreen activate
    the associated setting button instead of requiring a two-step
    process of pressing the padlock and the setting button.

15. The waveforms uses color to indicate breath-type.
	a. Inspiration for SPONT breaths is colored GOLDENROD.
	b. Inspiration for CONTROL breaths is colored GREEN.
	c. Exhalation for either breath is colored YELLOW.
	d. Non-breathing is colored WHITE.
	e. PEEP recovery breath is colored GOLDENROD.

16. The breath bar uses color to indicate SPONT (GOLDENROD) or
    CONTROL (GREEN) breaths. The field is inverted between the
    inspiration and exhalation.

17. The breath timing diagram uses color to convey the inspiration
    time (GREEN) and the exhalation time (YELLOW).

18. Made SoftFault methods out-of-line (from inline) to save memory.

19. Most static const class members have been changed to module "static
    const" which saved about 8K.

20. Used color to tie in the prompt area to the activated setting
    button.
