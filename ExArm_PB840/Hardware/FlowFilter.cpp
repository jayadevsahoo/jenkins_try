#include "stdafx.h"

//*****************************************************************************
//
//  FlowFilter.cpp - Implementation of the FlowFilter_t class.
//
//*****************************************************************************

#include "FlowFilter.h"
#include "GlobalObjects.h"


//*****************************************************************************
//
//  FlowFilter_t::FlowFilter_t - Constructor
//
//*****************************************************************************

FlowFilter_t::FlowFilter_t()
{
	FullyFilteredValue = 0;
}



//*****************************************************************************
//
//  FlowFilter_t::~FlowFilter_t - Destructor
//
//*****************************************************************************

FlowFilter_t::~FlowFilter_t()
{
}


Flow_t FlowFilter_t::FilterValue(Flow_t UnfilteredValue, uint16_t TimeConst)
{
	FullyFilteredValue = ((FullyFilteredValue * TimeConst + UnfilteredValue) - FullyFilteredValue) / TimeConst;
	return FullyFilteredValue;
}
