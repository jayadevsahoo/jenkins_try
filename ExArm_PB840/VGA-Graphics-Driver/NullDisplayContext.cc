#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2001, Puritan-Bennett Corporation
//=====================================================================  

// =========================== M O D U L E   D E S C R I P T I O N ====
//@ Class: NullDisplayContext -- Display Context with no display.
//---------------------------------------------------------------------
//@ Interface-Description
//  This class provides the methods used to "draw" objects on a null
//  display. This class is used when drawing functions are called for
//  objects not present on an actual display. The methods contain
//  no-operation. This is required to handle the "fake" buttons on the
//  lower screen of a single screen GUI. The application uses the 
//  built-in geometry and touch detection mechanisms of GUI-Foundation 
//  which in turn uses a DisplayContext for drawing objects in a 
//  BaseContainer.
//---------------------------------------------------------------------
//@ Rationale
//  Contains context information and generic drawing interface for 
//  the null display device.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The NullDisplayContext is derived from DisplayContext. There is no
//  device specific code in either the base class or this class.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ $Header:   /840/Baseline/VGA-Graphics-Driver/vcssrc/NullDisplayContext.ccv   25.0.4.0   19 Nov 2013 14:37:30   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003   By: gdc    Date: 01-Feb-2007     SCR Number: 6237
//  Project: Trend
//  Description:
//  Trend project related changes. Moved the no-operation methods from
//  NullDisplayContext to the base class DisplayContext so this class 
//  won't have to be modified every time a method is added to 
//  DisplayContext.
//
//
//  Revision: 002   By: erm    Date:  15-Oct-2001    DCS Number: 5959
//  Project: GUIComms
//  Description:
//  Code CleanUp
//
//  Revision: 001   By: gdc    Date:  27-Sep-2001    DCS Number: 5493
//  Project: GuiComms
//  Description:
//	Initial Version.
//
//=====================================================================  

#include "NullDisplayContext.hh"
#include "GraphicsContext.hh"
#include <stdio.h>

//@ Usage-Classes
//@ End-Usage

//=====================================================================  
//
//  Public Methods...
//
//=====================================================================  

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: NullDisplayContext [constructor]
//
//@ Interface-Description
//  Constructor for the NullDisplayContext class.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
NullDisplayContext::NullDisplayContext(Uint32 width, Uint32 height, Uint32 bitsPerPixel)
  : DisplayContext(width, height, bitsPerPixel)
{  
  // $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~NullDisplayContext [destructor]
//
//@ Interface-Description
//  Destructor for the NullDisplayContext class.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
NullDisplayContext::~NullDisplayContext()
{
  // no code
}

//=====================================================================  
//
//  Private Methods...
//
//=====================================================================  


