//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: DiagnosticFault - The class that contains textual description
//	of diagnostic faults and diagnostic tests that are displayed by the
// Diagnostic Code logs and SST Results Logs.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/DiagnosticFault.hhv   25.0.4.0   19 Nov 2013 14:07:42   pvcs  $ 
//
//@ Modification-Log
//
//  Revision: 003  By: gdc    Date: 26-May-2007   DCS Number:  6330
//  Project:  Trend
//  Description:
//      Removed SUN prototype code.
//
//  Revision: 002  By: sah    Date:  07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//
//====================================================================

#ifndef DiagnosticFault_HH
#define DiagnosticFault_HH

#include "MiscStrs.hh"
#include "SmTestId.hh"
#include "GuiAppClassIds.hh"


// Structure for faults displayed in System Diagnostic and Communication Diagnostic logs
struct TestFaultStruct
{
	Uint16 		testFaultId;			// Diagnostic test or fault ID
	StringId	*testFaultName;			// Diagnostic test or fault name
	Uint8		UNUSED_BYTE;			// Unused byte of the long word
};


// Structure for faults displayed in EST/SST Diagnostic logs
struct EstSstTestFaultStruct
{
	Uint16		testNumber;				// Test ID
	Uint16		conditionNumber;		// also called Step number
	StringId 	*conditionName;			// Test condition 
//	Uint8		UNUSED_BYTE;			// Unused byte of the long word
};

struct EstSstTestNameStruct
{
	Uint16		testId;
	StringId	*testName;
};

class DiagnosticFault
{
public:
	static StringId GetBackgroundFaultStr(Uint16 faultId);	
	static StringId GetCommTestEventStr(Uint16 faultId);	
	static StringId GetSystemEventStr(Uint16 systemEventId);	
	static StringId GetStartupTestEventStr(Uint16 faultId);	
	static StringId GetTrapFaultStr(Uint16 faultId);	
	static StringId GetEstTestEventStr(
						Uint16 testNumber, Uint16 conditionNumber); 
	static StringId GetSstTestEventStr(
						Uint16 testNumber, Uint16 conditionNumber); 
 
	static void ComposeDisplayDiagStrings(Uint16 testType, Uint16 testId, 
						wchar_t *displayString, wchar_t *displayString2);

	static StringId GetEstTestNameStr(Uint16 testNumber);	
	static StringId GetSstTestNameStr(Uint16 testNumber);	

	static void SoftFault(const SoftFaultID  softFaultID,
						  const Uint32       lineNumber,
						  const char*        pFileName = NULL,
						  const char*        pPredicate = NULL);

private:
	DiagnosticFault(void);			// not implemented
	~DiagnosticFault(void);			// not implemented
 
	static void FormatString(Uint16 diagType, wchar_t *resultString, 
										wchar_t *fromString, 
										Uint16 testId, Uint8 stepNumber);
 
	static wchar_t *GetCode(wchar_t *theString, wchar_t *returnStr, int strSize);

	static StringId GetTestEventStr(Uint16 faultId, 
									TestFaultStruct pFaultStruct[],
									Uint16 arraySize);
 
	static Uint16 SearchDiagCodeString(Uint16 testType, Uint16 testId, 
						wchar_t *displayString, 
						Uint8 &firstStepNumber, Uint8 &secondStepNumber);

	static EstSstTestFaultStruct EstTestFaultStrs[];
	static EstSstTestFaultStruct SstTestFaultStrs[];

	// Hardware exception ids (Trap ids)
	enum
	{
		ACCESS_FAULT_ID = 2,
		ADDRESS_ERROR_ID = 3,
		ILLEGAL_INSTRUCTION_ID = 4,
		INTEGER_DIVIDE_BY_ZERO = 5,
		PRIVILEGE_VIOLATION_ID = 8,
		FORMAT_ERROR_ID = 14,
		UNINITIALIZED_INTERRUPT = 15,
		TRAP_MAX_FAULTS = 7
	};

	enum
	{
		STARTUP_MAX_FAULTS = 79	
	};

	// GUI data for EST test name table.
	static EstSstTestNameStruct EstTestNameStrs[];

	// GUI data for SST test name table.
	static EstSstTestNameStruct SstTestNameStrs[];

	// GUI data for COMMUNICATION faults 
	static TestFaultStruct CommFaultStrs[];

	// GUI data for STARTUP (POST and INIT) faults
	static TestFaultStruct StartupFaultStrs[]; 

	// GUI data for SYSTEM events
	static TestFaultStruct SystemEventStrs[]; 

	// GUI data for BACKGROUND faults
	static TestFaultStruct BackgroundFaultStrs[]; 

	// GUI data for processor's traps 
	static TestFaultStruct TrapFaultStrs[];
	
};


#endif // DiagnosticFault_HH
