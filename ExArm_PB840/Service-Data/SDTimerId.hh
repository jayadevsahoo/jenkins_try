#ifndef SDTimerId_HH
#define SDTimerId_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1994, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: SDTimerId - A list of enumerated ids for Service
//		  Data timer.
//---------------------------------------------------------------------
//@ Version
//
//  Revision: 001  By:  jhv    Date:  26-June-95    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

class SDTimerId
{
public:
	enum SDTimerIdType
	{
		//For Service Data subsystem
		SERVICE_DATA_TIMEOUT,
        SERVICE_DATA_TIMER,
		NUM_TIMER_IDS
	};
};

#endif // SDTimerId_HH 
