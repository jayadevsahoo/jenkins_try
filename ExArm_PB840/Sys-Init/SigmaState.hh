#ifndef SigmaState_HH
#define SigmaState_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//====================================================================
 
//====================================================================
// Filename:  SigmaState - Sigma system states.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Sys-Init/vcssrc/SigmaState.hhv   25.0.4.0   19 Nov 2013 14:33:54   pvcs  $
//
//@ Modification-Log
//
//   Revision 001   By:   gdc      Date: 05/16/95    DR Number:   n/a
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//====================================================================
 
#if 0  
// for bug in genDD
#endif

//@ Type: SigmaState
//  Processing states for the GUI or BD.  Not to exceed 1 byte.
enum SigmaState
{
     STATE_START     = 0x0001
    ,STATE_ONLINE    = 0x0002
    ,STATE_SERVICE   = 0x0004
    ,STATE_SST       = 0x0008
    ,STATE_INOP      = 0x0010
    ,STATE_TIMEOUT   = 0x0020
    ,STATE_INIT      = 0x0040
    ,STATE_UNKNOWN   = 0x0080
    ,STATE_FAILED    = 0x0100
};

#endif // SigmaState_HH
