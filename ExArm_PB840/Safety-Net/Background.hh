#ifndef Background_HH
#define Background_HH
//=============================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=============================================================================

//============================ H E A D E R   D E S C R I P T I O N ============
//  Class: Background - Background Fault Handling Utilities
//-----------------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Safety-Net/vcssrc/Background.hhv   25.0.4.0   19 Nov 2013 14:20:34   pvcs  $
//
//@ Modification-Log
// 
//  Revision: 013  By: rhj    Date: 19-Feb-2010   SCR Number: 6436
//  Project:  PROX
//  Description:
//      Added background event name for Prox errors.
// 
//  Revision: 012  By: gdc    Date:  20-Mar-2007    SCR Number: 6237
//  Project:  Trend
//  Description:
//      Added background event name for Compact Flash errors.
//
//  Revision: 011  By: iv     Date:  03-May-1999    DR Number: 5376
//  Project:  ATC
//  Description:
//      Added background event name for BPS events.
//
//  Revision: 010  By: sah    Date:  15-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 009  By: iv    Date:  29-Apr-1998    DR Number: DCS 5041
//      Project:  Sigma (840)
//      Description:
//        Added a Uint16 &errorCode argument to the TestNamePtr typedef.
//
//  Revision: 008  By: iv    Date:  07-Jan-1998    DR Number: DCS 5000
//      Project:  Sigma (840)
//      Description:
//        Added a Uint16 default argument to methods ReportBkEvent() and
//        LogDiagnosticCodeUtil().
//
//  Revision: 007  By: iv    Date:  06-Oct-1997    DR Number: DCS 2543
//      Project:  Sigma (840)
//      Description:
//        Added a new BkEventName: BK_TOUCH_SCREEN_RESUME_INFO.
//
//  Revision: 006  By: iv    Date:  26-Sep-1997    DR Number: DCS 1920
//      Project:  Sigma (840)
//      Description:
//        Added a new BkEventName: BK_TOUCH_SCREEN_BLOCKED_INFO.
//
//  Revision: 005  By: iv    Date:  06-Aug-1997    DR Number: DCS 2032
//      Project:  Sigma (840)
//      Description:
//        Added a constant BK_TEST_10SEC_DELAY.
//
//  Revision: 004   By: dosman    Date: 25-Jun-1997   DR Number: DCS 2233
//    Project:  Sigma (R8027)
//    Description:
//      Added BK_DATAKEY_SIZE_ERROR
// 
//  Revision: 003   By: syw    Date: 07-May-1997   DR Number: DCS 2004 
//    Project:  Sigma (R8027)
//    Description:
//      Changed BK_COMPR_CHECKSUM to BK_COMPR_BAD_DATA.  Added BK_COMPR_UPDATE_SN,
//		and BK_COMPR_UPDATE_PM_HRS.
//
//  Revision: 002   By: iv    Date: 06-May-1997   DR Number: DCS 2040 
//    Project:  Sigma (R8027)
//    Description:
//      Changed BK_TEST_28SEC_DELAY to BK_TEST_60SEC_DELAY.
//
//  Revision: 001    By: by Date: 10-DEC-1995    DR Number: none
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//=============================================================================

#include "Sigma.hh"
#include "SafetyNet.hh"
#include "TestResultId.hh"
#include "OperatingParamEventName.hh"

//////////////////////////////////////////////////////////////////////////////
//     Declare global time constant
//////////////////////////////////////////////////////////////////////////////
extern const Uint16 BACKGROUND_CYCLE_TIME;
extern const Int32 BK_TEST_FREQ_500MS;
extern const Int32 BK_TEST_FREQ_1SEC;
extern const Int32 BK_TEST_FREQ_10SEC;
extern const Int32 BK_TEST_NO_DELAY;
extern const Int32 BK_TEST_60SEC_DELAY;
extern const Int32 BK_TEST_10SEC_DELAY;

extern const Int16 NOT_FOUND;
    
/////////////////////////////////////////////////////////////////////////////
// This is the list of Background event names.
// Each of these IDs mapped, one to one, to BK_EVENT_ASSOCIATION_TABLE_,
// Background error definition table in BackgroundFaultHanlder class.
// The current design allows to have up to a total of BK_TOTAL_EVENTS_POSSIBLE
// background events.
//
// NOTE: It is important that this enumeration is in synch with the
// BK_EVENT_ASSOCIATION_TABLE_ in BackgroundFaultHanlder class.
// A modification to this enumeration will require the a modification
// to BK_EVENT_ASSOCIATION_TABLE_. 
/////////////////////////////////////////////////////////////////////////////

enum BkEventName 
{
    BK_NO_EVENT = 0,
    BK_SV_SWITCHED_SIDE_OOR,
    BK_EXH_FLOW_OOR,
    BK_O2_PSOL_CURRENT_OOR,
    BK_AIR_PSOL_CURRENT_OOR,
    BK_EXH_MOTOR_CURR_OOR,
    BK_EXH_VLV_COIL_TEMP_OOR,
    BK_EXH_PRESS_OOR,
    BK_INSP_PRESS_OOR,
    BK_AIR_FLOW_OOR_HIGH,
    // 10    
    BK_AIR_FLOW_OOR_LOW,
    BK_AIR_FLOW_TEMP_OOR,
    BK_O2_FLOW_OOR_HIGH,
    BK_O2_FLOW_OOR_LOW,
    BK_O2_FLOW_TEMP_OOR,
    BK_EXH_FLOW_TEMP_OOR,
    BK_BD_10V_SUPPLY_MON_OOR,
    BK_BD_12V_FAIL_OOR,
    BK_BD_15V_FAIL_OOR,
    BK_NEG_15V_FAIL_OOR,
    // 20
    BK_GUI_12V_FAIL_OOR,
    BK_GUI_5V_FAIL_OOR,           
    BK_BD_5V_FAIL_OOR,
    BK_O2_PSOL_STUCK,
    BK_AIR_PSOL_STUCK,
    BK_AIR_PSOL_STUCK_OPEN,
    BK_O2_PSOL_STUCK_OPEN,
    BK_ATM_PRESS_OOR,
    BK_O2_SENSOR_OOR,
    BK_O2_SENSOR_OOR_RESET,
    // 30
    BK_SVO_CURRENT_OOR,
    BK_PI_STUCK,
    BK_PE_STUCK,
    BK_INSP_AUTOZERO_FAIL,
    BK_EXH_AUTOZERO_FAIL,
    BK_POWER_FAIL_CAP,        
    BK_ALARM_CABLE,
    BK_ADC_FAIL_HIGH,                        
    BK_ADC_FAIL_LOW,                
    BK_ADC_LOOPBACK_FAIL,            
    // 40
    BK_TOUCH_SCREEN_FAIL,            
    BK_TOUCH_SCREEN_BLOCKED,            
    BK_TOUCH_SCREEN_RESUME,
    BK_AC_SWITCH_STUCK,
    BK_BD_NOVRAM_CHECKSUM,
    BK_BD_TOD_FAIL,
    BK_GUI_TOD_FAIL,                         
    BK_GUI_NOVRAM_CHECKSUM,
    BK_BPS_VOLTAGE_OOR,                
    BK_BPS_CURRENT_OOR,            
    // 50
    BK_BPS_MODEL_OOR,                
    BK_EXH_HEATER_OOR,                
    BK_GUI_STUCK_KEY,
    BK_BD_EEPROM_CHECKSUM,
    BK_GUI_EEPROM_CHECKSUM,
    BK_GUI_SAAS_COMM_FAIL,            
    BK_COMPR_ELAPSED_TIMER,
	BK_COMPR_BAD_DATA,
    BK_LOSS_OF_GUI_COMM,                     
    BK_LOSS_OF_BD_COMM,
    // 60
    BK_RESUME_GUI_COMM,
    BK_RESUME_BD_COMM,
    BK_EST_REQUIRED,
    BK_GUI_SAAS_AUDIO_FAIL,
    BK_LV_REF_OOR,                
    BK_SV_CURRENT_OOR,                       
    BK_MON_ALARMS_FAIL,
    BK_MON_APNEA_ALARM_FAIL,
    BK_MON_APNEA_INT_FAIL,
    BK_MON_HIP_FAIL,
    // 70
    BK_MON_INSP_TIME_FAIL,
    BK_MON_NO_DATA,
    BK_MON_DATA_CORRUPTED,
    BK_MON_O2_MIXTURE_FAIL,                
    BK_MON_BREATH_TIME_FAIL,                
    BK_DATAKEY_UPDATE_FAIL,    
    BK_TASK_MONITOR_FAIL,    
    BK_GUI_WAVEFORM_DROP,
    BK_BD_WAVEFORM_DROP,
    BK_FORCED_VENTINOP,
    // 80
    BK_BD_MULT_MAIN_CYCLE_MSGS,
    BK_BD_MULT_SECOND_CYCLE_MSGS,
    BK_WATCHDOG_FAILURE,
    BK_INIT_RESUME_GUI_COMM,
    BK_INIT_RESUME_BD_COMM,
    BK_INIT_LOSS_GUI_COMM,
    BK_INIT_LOSS_BD_COMM,
    BK_COMPR_UPDATE_SN,
    BK_COMPR_UPDATE_PM_HRS,
    BK_DATAKEY_SIZE_ERROR,
    // 90
    BK_TOUCH_SCREEN_BLOCKED_INFO,
    BK_TOUCH_SCREEN_RESUME_INFO,
    BK_BPS_EVENT_INFO,
    BK_COMPACT_FLASH_ERROR_INFO,
    BK_PROX_ERROR_INFO,

    BK_MAX_EVENT                                  
};

//////////////////////////////////////////////////////////////////////////////
// A Background test has the following common characteristics
//////////////////////////////////////////////////////////////////////////////
typedef  BkEventName (*TestNamePtr)  (Uint16 &errorCode);

class Background
{
    public:

        static void Initialize( void );

        static void ReportBkEvent(const BkEventName eventName,
                                 const Uint16 errorCode = 0u);

        static void ResetActiveBtat( void );

        static Boolean CheckIfTimeHasChanged( void );

        static void QueueAlarmEvent( OperatingParameterEventName alarmType );

        static void CheckAndProcessPostErrorStatus( void );

        static inline void TimeHasChanged( void );

        static inline void SetPostMinorErrorOccurred( void );

        static void LogDiagnosticCodeUtil(const BkEventName eventName,
                                          const Uint16 errorCode = 0u);

        static void SoftFault( const SoftFaultID softFaultID,
			       const Uint32      lineNumber,
			       const char*       pFileName  = NULL, 
			       const char*       pPredicate = NULL );

    protected:

    private:

        Background( void );  // Constructor, not implemented
        ~Background( void ); // Destructor, not implemented

        //@ Data-Member: TimeHasChangedFlag_ 
        // This flag is set when the wall clock has changed.
        // The method TimeHasChanged() sets this flag when
        // Sys-Init informs Background Test that the user
        // has changed the system clock, it is reset when
        // CheckIfTimeHasChanged() is called.   
        static Boolean TimeHasChangedFlag_;

        //@ Data-Member: PostMinorErrorOccurred_ 
        // This flag will be set by Persistent-Objects
        // to indicate that a POST minor error(s) occurred
        // This flag is cleared after 
        // CheckAndProcessPostErrorStatus() is called.
        static Boolean PostMinorErrorOccurred_; 

};

// Inlined methods
#include "Background.in"

#endif    // #ifndef Background_HH




