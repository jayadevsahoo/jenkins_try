#include "stdafx.h"

#if  defined(SIGMA_GUI_CPU) || defined(SIGMA_COMMON)

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, NellCor/Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: InspPauseDataInfo - Manages real time display of pressure
//                during inspiratory pause maneuver.
//---------------------------------------------------------------------
//@ Interface-Description
//  
//    Description:
//    ------------
//    This class updates the circuit pressure value every DELAY_TIME_ millisecond
//    during the inspiratory pause phase.
//
//    Class Features:
//    ---------------
//      1. Maintains a state that indicates whether the pause maneuver is active
//      2. Updates new circuit pressure values.
//    
//     Class Key methods:
//     ------------------
//     1. "updateData_"  - updates circuit pressure.
//     2. "startPause_"  - starts an insp Paused state and records the initial
//         End of inspiratory pressure at the beginning of a pause.
//     3. "stopPause_"  - stop an insp pause phase.
//
//     Class Interfaces:
//     -----------------
//       PatientDataMgr class interfaces with this class to keep
//       track of circuit pressure during inspiratory pause. PatientDataMgr class
//       starts and stops an insp pause state based on the data
//       packet type it receives from the Breath Delivery subsystem.
//       Actual circuit pressure data is maintained with breath item data 
//       processing agent PLATEAU_PRESSURE_ITEM type.
//       Other subsystems access the pressure data values through this agent.
//---------------------------------------------------------------------
//@  Rationale
//   Encapsulate all the data necessary to compute circuit pressure.
//---------------------------------------------------------------------
//@  Implementation-Description
//   When a PiEnd patient data packet arrives, Patient Data Manager
//   starts a breath pause phase. This class keeps track of last
//   pressure sample time stamp and it computes new pressure sample
//   every DELAY_TIME_ milliseconds.
//   Newly sampled circuit pressure is maintained within the coresponding
//   data processing agent type.
//---------------------------------------------------------------------
//@  Fault-Handling
//     Uses Standard Sigma CLASS_ASSERTION and CLASS_PRE_CONDITION
//     to handle software faults.
//---------------------------------------------------------------------
//@  Restrictions
//       The PatientDataMgr class should be initialized before
//       this class is used. OsTimeStamp class should be functional.
//>Voff
//---------------------------------------------------------------------
//@  Invariants
//---------------------------------------------------------------------
//@  End-Preamble
//
//---------------------------------------------------------------------
//@  Version
// @(#) $Header:   /840/Baseline/Patient-Data/vcssrc/InspPauseDataInfo.ccv   25.0.4.0   19 Nov 2013 14:17:58   pvcs  $
//
//@  Modification-Log
//
//  Revision: 006  By: sah   Date:  13-Jan-99    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//--Break in Revision numbers--
//
//  Revision: 001  By:  jhv    Date:  16-Feb-95   DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//  
//   Revision 002  By:  hct    Date: 04/23/97         DR Number:  1479
//      Project:   Sigma   (R8027)
//      Description:
//         Add requirements 03067, 03068, 03071 and 03072 to updateData_.  
//         These requirements were already added to the Unit Test Spec.
//
//   Revision 003  By:  hct    Date: 04/23/97         DR Number:  1479
//      Project:   Sigma   (R8027)
//      Description:
//         Add requirements 03069, 03070 and 03073 to updateData_.  
//         These requirements were already added to the Unit Test Spec.
//
//  Revision: 004  By:  hct    Date:  21-AUG-97    DR Number: 1479
//       Project:  Sigma (R8027)
//       Description:
//             Change 03067 to 03074 per review rework.  
//
//  Revision: 005  By:  iv     Date:  30-SEP-98    DR Number: None
//       Project:  BiLevel (R8027)
//       Description:
//             Initial BiLevel version.  
//
//=====================================================================
#include "InspPauseDataInfo.hh"
#include "PatientDataMgr.hh"
#include "BoundedDataInfo.hh"

//@  Code...
// Static member definition.
// $[BL03016] update rate
const Uint InspPauseDataInfo::DELAY_TIME_ = 200; // milliseconds

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@  Method: InspPauseDataInfo()  [Default Constructor]
//
//@  Interface-Description
//     The breath phase initial state is not paused.
//---------------------------------------------------------------------
//@  Implementation-Description
//---------------------------------------------------------------------
//@  PreCondition
//none
//---------------------------------------------------------------------
//@  PostCondition
//none
//@  End-Method
//=====================================================================

InspPauseDataInfo::InspPauseDataInfo(void)
{
    CALL_TRACE("InspPauseDataInfo()");
    paused_ = FALSE;
    updateTime_.invalidate();
    // $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@  Method: ~InspPauseDataInfo()  [Destructor]
//
//@  Interface-Description
//     Does nothing.
//---------------------------------------------------------------------
//@  Implementation-Description
//---------------------------------------------------------------------
//@  PreCondition
//none
//---------------------------------------------------------------------
//@  PostCondition
//none
//@  End-Method
//=====================================================================

InspPauseDataInfo::~InspPauseDataInfo(void)
{
    CALL_TRACE("~InspPauseDataInfo()");
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@  Method: updateData_( WaveformData *pWaveData )
//
//@  Interface-Description
//    Computes a pause pressure value based on the current circuit pressure
//    data of a new waveform data.
//    pause pressure value update is done once every DELAY_TIME_ milliseconds.
//---------------------------------------------------------------------
//@  Implementation-Description
//   The last update time is maintained in the updateTime_ member. 
//   The actual pause pressure data is kept within the Breath Item Data
//   Processing objects that Patient Data Manager maintains. These objects
//   are obtained from the PatientDataMgr Class through  the 'GetDataAgent'
//   method. Each pause pressure data is passed to its data type
//   processing object via 'modifyData_'.
//---------------------------------------------------------------------
//@  PreCondition
//   none
//---------------------------------------------------------------------
//@  PostCondition
// none
//@  End-Method
//=====================================================================
void
InspPauseDataInfo::updateData_(WaveformData *pWaveData)
{
BreathItemDataInfo *pDataManager;


    CALL_TRACE("updateData(pWaveData)");
    SAFE_CLASS_PRE_CONDITION(paused_);

    if ( updateTime_.getPassedTime() >= DELAY_TIME_) // $[TI1]
    {
	    // $[BL03017] pause pressure is Pcirc as measured
	    // during inspiratory pause maneuver
        pDataManager=PatientDataMgr::GetDataAgent(PatientDataId::PLATEAU_PRESSURE_ITEM);
        pDataManager->modifyData_((void *)&pWaveData->circuitPressure);
        updateTime_.now();
    }
    //else $[TI2]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  startPause_(const Real32  endInspPressure)
//
//@ Interface-Description
//   Start an inspiratory pause phase and record an initial circuit
//   pressure value. Record current time as a breath pause start time.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//   Get the End Of Inspiratory Pressure breath data agent object
//   from the patient data manager and let it process the data.
//--------------------------------------------------------------------- 
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 
void
InspPauseDataInfo::startPause_(const Real32 endInspPressure )
{
	CALL_TRACE("InspPauseDataInfo::startPause_(const Real32 endInspPressure )");

    BoundedDataInfo *pDataInfo;
    
    if ( paused_ )// $[TI0.1]
    {
           return;
    }
    //else  $[TI1]
    paused_ = TRUE;
    updateTime_.now();
    pDataInfo = (BoundedDataInfo *) PatientDataMgr::GetDataAgent
					    (PatientDataId::PLATEAU_PRESSURE_ITEM);
    initialPiEnd_ = pDataInfo->processData_(endInspPressure);
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//. Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//. Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//. Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//. PreCondition
//      none 
//---------------------------------------------------------------------
//. PostCondition 
//      none 
//. End-Method 
//===================================================================== 

void
InspPauseDataInfo::SoftFault(const SoftFaultID  softFaultID,
							 const Uint32       lineNumber,
							 const char*        pFileName,
							 const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, PATIENT_DATA, INSP_PAUSE_DATA_INFO,
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)

#endif//defined(SIGMA_GUI_CPU) || defined(SIGMA_COMMON)
