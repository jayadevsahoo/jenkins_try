#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: ExhCommunicationThread_t - Active object which communicates with
//			exhalation flow sensor to command autozero and accquire data.
//---------------------------------------------------------------------
//@ Interface-Description
//      
//---------------------------------------------------------------------
//@ Rationale
//      
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ Fault-Handling
//		n/a
//---------------------------------------------------------------------
//@ Restrictions
//      None
//---------------------------------------------------------------------
//@ Invariants
//      None
//---------------------------------------------------------------------
//@ End-Preamble
//
//*****************************************************************************
//
//  ExhCommunicationThread.cpp - Implementation of the ExhCommunicationThread_t class.
//
//*****************************************************************************
#include "ExhCommunicationThread.h"
#include "GlobalObjects.h"
#include "ExceptionHandler.h"
#include "CalibrationIDs.h"
#include "ExhFlowSensor.h"
#include "BDIORefs.hh"
#include "MainSensorRefs.hh"

static const uint16_t INTIAL_EXH_AZ_DAC_VALUE	= 1000;
static const uint16_t FINAL_EXH_AZ_DAC_VALUE	= 3000;
static const uint16_t EXH_AZ_DAC_INCREMENT		= 50;

#define EXH_COMM_THREAD_DEBUG_OUTPUT (0)

#define EXH_THREAD_EVENT_NAME	TEXT("EXH_THREAD_EVENT")

HANDLE hExhCommThreadEvent = NULL;
bool bFlowDataRequested = false;


ExhCommunication_t      ExhCommunicationThread_t::ExhCommunication;
ExhCommState_t          ExhCommunicationThread_t::ExhCommunicationState;
ExhAutozeroState_t      ExhCommunicationThread_t::AutozeroRequestState;
uint16_t                ExhCommunicationThread_t::FexRawData;
uint8_t                 ExhCommunicationThread_t::FexStatus;
bool                    ExhCommunicationThread_t::bFexDataValid;
bool                    ExhCommunicationThread_t::bAutozeroRequested;
ExhSensorBoard_t		ExhCommunicationThread_t::SensorBoard;
uint8_t                 ExhCommunicationThread_t::BytesToReceive = 2;

//*****************************************************************************
//
//  ExhCommunicationThread_t::Initialize the thread
//
//*****************************************************************************
void ExhCommunicationThread_t::Initialize()
{
    ExhCommunicationState = STATE_FEX_STATUS;
	AutozeroRequestState = STATE_AUTOZERO_COMPLETED;
	bFexDataValid = false;
	ExhCommunicationThread_t::SensorBoard = ExhCommunication.GetSensorBoardType();

	hExhCommThreadEvent = CreateEvent(NULL, FALSE, FALSE, EXH_THREAD_EVENT_NAME);
	if(!hExhCommThreadEvent)
	{
		RETAILMSG(TRUE, (_T("ExhCommunicationThread: Creating hExhCommThreadEvent failed\r\n")));
	}
	else
	{
		DWORD err = GetLastError();
		if(err != ERROR_ALREADY_EXISTS)
		{
			RETAILMSG(TRUE, (L"ExhCommunicationThread: hExhCommThreadEvent has not been created\r\n"));
		}
	}
}


//*****************************************************************************
//
//  ExhCommunicationThread_t::RequestAutozero
//
//*****************************************************************************
bool ExhCommunicationThread_t::RequestAutozero()
{
	if(!bAutozeroRequested)
	{
		bAutozeroRequested = true;
		return true;
	}
	else
		return false;
}

//*****************************************************************************
//
//  ExhCommunicationThread_t::ThreadProcedure -
//
//*****************************************************************************

uint32_t ExhCommunicationThread_t::ThreadProcedure(void *ProcedureArg)
{
	uint8_t Data;
	DWORD NumberOfBytesRead = 0;
	DWORD NumberOfBytesWritten;
	bool bCommStatus = false;
	bool bProcessData = false;
	static DWORD AZPendingCount;
	static uint16_t FlowDataPendingCount = 0;
	static DWORD InitialCount;
	static uint16_t DacValue;
	DWORD ElapsedTickCount;
	static DWORD TimeToWait;
    bool  WriteCommandWasIssued = false;
	bool forever = true;
	UNUSED_SYMBOL(ProcedureArg);	
	
	// used for the new sensor board
	static DWORD delayInitialCount = 0;
	static DWORD delayTimeToWait = 100;
	bool delayInit = false;
	bool bExtraResponseReceived = false;
	static bool bNoComm = false;

	while (forever)
	{
	  if(WriteCommandWasIssued)
      {
		while (forever)	
		{
			Data = 0;
			bCommStatus = ExhCommunication.ReadData(&Data, 1, &NumberOfBytesRead, 0);
			if(!bCommStatus || (NumberOfBytesRead != 1))	// if nothing to read, get out of the loop
			{
				break;
			}

			switch(ExhCommunicationThread_t::ExhCommunicationState)
			{
			case STATE_FEX_STATUS:
				FexStatus = Data;
				if(BytesToReceive > 0)
				{
					ExhCommunicationState = STATE_FEX_MSB;
				}
				else
				{
					RETAILMSG(1, (L"STATUS BYTE = 0x%x\r\n", FexStatus));
				}
				break;

			case STATE_FEX_STATUS_BYTE1:
				FexStatus = Data;
				RETAILMSG(1, (L"STATUS BYTE1 AFTER AZ = 0x%x\r\n", FexStatus));
				bExtraResponseReceived = true;
				ExhCommunicationState = STATE_FEX_STATUS;
				break;

			case STATE_FEX_MSB:
				if(BytesToReceive > 1)
				{
					FexRawData = (uint16_t)Data << 8;			
					ExhCommunicationState = STATE_FEX_LSB;
				}
				else
				{
					ExhCommunicationState = STATE_FEX_STATUS;
				}
				break;

			case STATE_FEX_LSB:
				ExhCommunicationThread_t::FexRawData |= Data;
				bFlowDataRequested = false;
				ExhCommunicationState = STATE_FEX_STATUS;
				bProcessData = true;
				bFexDataValid = true;
				break;

			case STATE_AUTOZERO_STATUS:
				{
				FexStatus = Data;
				if(SensorBoard == EXH_SENSOR_BOARD_NEW)
				{
					//There are two new bytes following AZ status.. They indicate.
					//the zero offset value. Read them out and pass to the upper layer
					uint8_t offsetMSB = 0;
					uint8_t offsetLSB = 0;
					ExhCommunication.ReadData(&offsetMSB, 1, &NumberOfBytesRead, 0);
					ExhCommunication.ReadData(&offsetLSB, 1, &NumberOfBytesRead, 0);
					FexRawData = ((offsetMSB & 0x3f) << 6) | (offsetLSB & 0x3f);
				}
				bProcessData = true;
				bFexDataValid = true;
				RETAILMSG(1, (L"AZ Request Done,Status=0x%x, FexRawData=%d\r\n", FexStatus, FexRawData));
				break;
				}

			default:
				break;
			}//end of: switch(ExhCommunicationState)
		}
      } // end if


	  if(bFlowDataRequested)
	  {
		FlowDataPendingCount++;		// Track # of ticks since the flow data request was sent
	  }

	  switch(AutozeroRequestState)
	  {
	    case STATE_AUTOZERO_INIT:
			InitialCount = GetTickCount();
			// TODO E600_LL: need to check for ventilation state later
			if(forever)
			{
				DacValue = INTIAL_EXH_AZ_DAC_VALUE;
#if defined(SIGMA_BD_CPU)
				AioDioDriver.Write(DAC_EXHALATION_VALVE, DacValue);
#endif
				TimeToWait = EXH_VALVE_CLOSE_TIME;
				AutozeroRequestState = STATE_AUTOZERO_CLOSE_VALVE;
			}
			else
			{
				TimeToWait = 500;
				AutozeroRequestState = STATE_AUTOZERO_WAIT;
			}
			break;
		case STATE_AUTOZERO_CLOSE_VALVE:
			DacValue += EXH_AZ_DAC_INCREMENT;
#if defined(SIGMA_BD_CPU)
			AioDioDriver.Write(DAC_EXHALATION_VALVE, DacValue);
#endif
			if(DacValue >= FINAL_EXH_AZ_DAC_VALUE)
			{
				AutozeroRequestState = STATE_AUTOZERO_WAIT;
			}
			break;
		case STATE_AUTOZERO_WAIT:
			if((GetTickCount() - InitialCount) > TimeToWait)
			{
				AutozeroRequestState = STATE_AUTOZERO_REQUEST;
			}
			break;
	    case STATE_AUTOZERO_REQUEST:
			RETAILMSG(1, (L"ExhComm: Sending AZ Request\r\n"));
			Data = 0x40;
			ExhCommunication.WriteData(&Data, 1, &NumberOfBytesWritten);
			AutozeroRequestState = STATE_AUTOZERO_PENDING;		// Stay in this state until autozero status received
			AZPendingCount = GetTickCount();
			ExhCommunicationState = STATE_AUTOZERO_STATUS;		// Signal this thread that autozero request has been sent
            WriteCommandWasIssued = true;
			bNoComm = false;
			break;

		case STATE_AUTOZERO_PENDING:
			ElapsedTickCount = GetTickCount() - AZPendingCount;
			uint8_t AzDoneMask;
			AzDoneMask = AUTOZERO_DONE_BIT;

			if(SensorBoard == EXH_SENSOR_BOARD_NEW)
			{
				AzDoneMask = AUTOZERO_DONE_BIT_NEW_SENSOR;
			}

			if(bProcessData && (ExhCommunicationThread_t::FexStatus & AzDoneMask))
			{
				AutozeroRequestState = STATE_AUTOZERO_COMPLETED;
				if(SensorBoard == EXH_SENSOR_BOARD_NEW)
				{
					AutozeroRequestState = STATE_AUTOZERO_DELAY;
				}

				// TODO E600_LL: need to check for ventilation state later
				if(forever)
				{
#if defined(SIGMA_BD_CPU)
					if(SensorBoard == EXH_SENSOR_BOARD_NEW)
					{
						AioDioDriver.Write(DAC_EXHALATION_VALVE, 0);
					}
					else
					{
						AutozeroRequestState = STATE_AUTOZERO_OFFSET;
						Data = 0x01;
						BytesToReceive = 2;
						ExhCommunication.WriteData(&Data, 1, &NumberOfBytesWritten);
						bExtraResponseReceived = false;
						ExhCommunicationState = STATE_FEX_STATUS;
					}
#endif
				}
				RETAILMSG(1, (L"ExhComm: AZ Request Completed\r\n"));
			}
			// if taking too long, terminate
			else if(ElapsedTickCount > 1000)
			{
				RETAILMSG(1, (L"ExhComm: Sensor Not Responding\r\n"));
				
				if(SensorBoard == EXH_SENSOR_BOARD_NEW)
				{	
					AutozeroRequestState = STATE_AUTOZERO_DELAY;
				}

#if defined(SIGMA_BD_CPU)
				AioDioDriver.Write(DAC_EXHALATION_VALVE, 0);
				RExhFlowSensor.SetNoComm();
				bNoComm = true;
#endif
			}
			break;

		case STATE_AUTOZERO_DELAY:
			if(!delayInit)
			{
				//RETAILMSG(1, (L"ExhComm: Waiting for some time\r\n"));
				delayInitialCount = GetTickCount();
				delayTimeToWait = 100;
				delayInit = true;
			}
			else
			{
				if((GetTickCount() - delayInitialCount) > delayTimeToWait)
				{
					//AutozeroRequestState = STATE_AUTOZERO_COMPLETED;
					delayInit = false;

					if(bNoComm)
					{
						AutozeroRequestState = STATE_AUTOZERO_COMPLETED;
					}
					else
					{
						AutozeroRequestState = STATE_AUTOZERO_EXTRA_CMD;
						
						Data = 0x04;
						ExhCommunicationState = STATE_FEX_STATUS_BYTE1;
						BytesToReceive = 0;

						ExhCommunication.WriteData(&Data, 1, &NumberOfBytesWritten);
						bExtraResponseReceived = false;
					}
				}
			}
			break;

		// this state is added to get an extra status byte1 on the new sensor board
		case STATE_AUTOZERO_EXTRA_CMD:
			if(bExtraResponseReceived)
			{
				AutozeroRequestState = STATE_AUTOZERO_COMPLETED;
			}
			break;

		// this state is added to be able to get the AZ value for old board and old sensor only
		case STATE_AUTOZERO_OFFSET:
			AioDioDriver.Write(DAC_EXHALATION_VALVE, 0);
			AutozeroRequestState = STATE_AUTOZERO_COMPLETED;
			RETAILMSG(1, (L"STATE_AUTOZERO_OFFSET: Status=0x%x, FexRawData=%d\r\n", FexStatus, FexRawData));
			break;

		case STATE_AUTOZERO_COMPLETED:	// not requested for autozero, send request for sensor status and flow data
			if(bAutozeroRequested)
			{
				AutozeroRequestState = STATE_AUTOZERO_INIT;
				bAutozeroRequested = false;
				RETAILMSG(1, (L"ExhComm: AZ Request Received\r\n"));
			}
			else
			{
				if(!bFlowDataRequested || (FlowDataPendingCount > 2)) 
				{
					Data = 0x01;
					bFlowDataRequested = true;
					FlowDataPendingCount = 0;
					BytesToReceive = 2;
					
					if(SensorBoard == EXH_SENSOR_BOARD_NEW)
					{
						Data = 0x03;
					}

					ExhCommunication.WriteData(&Data, 1, &NumberOfBytesWritten);
					ExhCommunicationState = STATE_FEX_STATUS;
					WriteCommandWasIssued = true;
				}
			}
			break;

		default: break;
	  } // end switch

	  if(bProcessData)	// if there is something for the ExhFlowSensor to do, call it
	  {
#ifdef SIGMA_BD_CPU			
		    RExhFlowSensor.ProcessData(FexStatus, FexRawData, bFexDataValid);

#endif
			bProcessData = false;
			bFexDataValid = false;
			FexRawData = 0;
	  }

	  WaitForSingleObject(hExhCommThreadEvent, INFINITE);
	}//end of: while (true)

    return 0;
}

//*****************************************************************************
//
//  Thread - This is the thread entry point
//
//*****************************************************************************

void ExhCommunicationThread_t::Thread()
{

    uint32_t ThreadResult = 0;

    __try
    {
		ThreadResult = ExhCommunicationThread_t::ThreadProcedure(0);
    }
    __except(ExceptionHandler.EvaluateException(GetExceptionInformation()))
    {
        // Get exception info
        int32_t Code = _exception_code();
        // Call exception handler
        ExceptionHandler.VentilateWithUi(Code, "ExhCommmunicationThread");
    }


}


