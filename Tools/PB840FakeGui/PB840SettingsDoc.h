
// PB840SettingsDoc.h : interface of the CPB840SettingsDoc class
//


#pragma once

#include "SettingXmitData.hh"


class CPB840SettingsDoc : public CDocument
{
protected: // create from serialization only
	CPB840SettingsDoc();
	DECLARE_DYNCREATE(CPB840SettingsDoc)

// Attributes
public:

// Operations
public:
	BOOL GetSettingElement( Uint32 idx, SettingXmitData& data );
	void SetSettingElement( Uint32 idx, const SettingXmitData& data );
	static CPB840SettingsDoc* GetDocInstance();

// Overrides
public:
	virtual BOOL OnNewDocument();
	
	virtual BOOL OnOpenDocument(LPCTSTR lpszPathName);

	virtual BOOL OnSaveDocument(LPCTSTR lpszPathName);
	/*
	virtual void OnCloseDocument();
	*/
	virtual void Serialize(CArchive& ar);

// Implementation
public:
	virtual ~CPB840SettingsDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

	SettingXmitData  arrXmitElems[::NUM_BD_SETTING_IDS];


// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()
public:
	
};


