#ifndef GuiTimerId_HH
#define GuiTimerId_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1994=2007, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: GuiTimerId - A list of enumerated ids for GUI timer.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/GuiTimerId.hhv   25.0.4.0   19 Nov 2013 14:07:54   pvcs  $
//
//@ Modification-Log
//
//  Revision: 011  By:  rhj    Date:  19-Mar-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//	Added PROX_STATUS_TIMEOUT
// 
//  Revision: 010  By:  rhj    Date:  18-Feb-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//	Added PROX_MANUAL_PURGE_TIMEOUT and PROX_MANUAL_PURGE_PROGRESS_TIMER_TICK
//
//  Revision: 009  By:  gdc    Date:  10-Dec-2008    SCR Number: 6377
//  Project:  840S
//  Description:
//	Removed DELTA project single screen support.
//
//  Revision: 008   By: gdc    Date: 24-Sep-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal non-invasive CPAP.
//
//  Revision: 007   By: rhj    Date: 07-July-2008   SCR Number: 6435
//  Project:  LEAK
//  Description:
//      Modified to support Leak Compensation.
//       
//  Revision: 006  By:  rhj	   Date:  08-Jan-2007    SCR Number: 6237
//  Project:  Trend
//  Description:
//		Added trending timers.
//
//  Revision: 005  By:  gdc	   Date:  30-May-2005    SCR Number: 6170
//  Project:  NIV
//  Description:
//		Added 30 second timeout for High Spontaneous Inspiratory Alert.
//
//  Revision: 004  By:  yyy	   Date:  29-Jun-1998    DCS Number: 5327
//  Project:  NeoMode
//  Description:
//		Initial version.
//
//  Revision: 003  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 002  By:  yyy    Date:  10-Sep-97    DR Number: 1796
//    Project:  Sigma (R8027)
//    Description:
//      Removed all references to PREVIOUS SETUP RECOVERY.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

class GuiTimerId
{
public:
	enum GuiTimerIdType
	{
		NO_VENT_CLOCK = 0,
		WALL_CLOCK,
		AUTO_REPEAT_CLOCK,
		LAPTOP_COUNTDOWN_CLOCK,
		MAIN_SETTING_CHANGE_TIMEOUT,
		SUBSCREEN_SETTING_CHANGE_TIMEOUT,
		SST_INACTIVITY_TIMEOUT,
		ALARM_SILENCE_TIMEOUT,
		MORE_ALARMS_TIMEOUT,
		SERVICE_MODE_TEST_PAUSE_TIMEOUT,
		TOUCH_RELEASE_TIMEOUT,
		DELAYED_PAUSE_TIMEOUT,
		ALARM_PROGRESS_TIMER_TICK,
		O2_PROGRESS_TIMER_TICK,
		HIGH_SPONT_INSP_TIME_ALERT_TIMER,
        TREND_DATA_SET_TIMER_TICK,
        RESPM_NIF_MANEUVER_TIMEOUT,
        RESPM_P100_MANEUVER_TIMEOUT,
        RESPM_VC_MANEUVER_TIMEOUT,
        TREND_TABLE_TIMER,
        TREND_DATA_SET_RETRY_TIMER,
        TREND_DISPLAY_RETRY_TIMER,
		LEAK_COMP_LOWER_DISPLAY_TIMER,
		MESSAGE_AREA_TIMER,
		PROX_MANUAL_PURGE_PROGRESS_TIMER_TICK,
		PROX_MANUAL_PURGE_TIMEOUT,
		PROX_STATUS_TIMEOUT,
		NUM_TIMER_IDS
	};
};

#endif // GuiTimerId_HH 
