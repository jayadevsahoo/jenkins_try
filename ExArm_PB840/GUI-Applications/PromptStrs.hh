
#ifndef PromptStrs_HH
#define PromptStrs_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: PromptStrs - Language-independent repository for
// strings displayed in the Prompt Area of the Lower Screen.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/PromptStrs.hhv   25.0.4.0   19 Nov 2013 14:08:16   pvcs  $
//
//@ Modification-Log  
//
//  Revision: 037  By: rhj   Date: 13-July-2010  SCR Number: 6581 
//  Project:  XENA2
//  Description:
//       Added HB_DISCO_SENS_NEONATAL_SOFT_MAX_A.
//
//  Revision: 036  By: mnr   Date: 04-May-2010   SCR Number: 6436
//  Project:  PROX
//  Description:
//      Added SST prompt string for PROX.
//
//  Revision: 035  By: rhj   Date:  26-Jan-2010  SCR Number: 6436
//  Project:  PROX
//  Description:
//       PROX prompt changes.
//      
//  Revision: 034  By:  mnr    Date: 03-Mar-2010     SCR Number: 6556
//  Project: NEO
//  Description:
//  	Added advisory prompt for Philips IntelliBridge COM setting.
// 
//  Revision: 033  By:  mnr    Date:  23-Feb-2010    SCR Number: 6555 
//  Project:  NEO
//  Description:
//		Added SCREENLOCK_CONTROL_LOCKED.
//
//  Revision: 032   By: mnr   Date:  23-Oct-2009     SCR Number: 6528
//  Project:  S840BUILD3
//  Description:
//      Long EST prompt string broken up into 3 strings to make RUSSIAN
//	    and POLISH work.
//
//  Revision: 031  By:  rhj    Date:  04-Apr-2009    SCR Number: 6495 
//  Project:  840S2
//  Description:
//		Added single test strings for EST. 
//
//  Revision: 030   By: gdc    Date: 11-Aug-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support changing IBW while ventilating.
//
//  Revision: 029  By:  rhj	   Date:  11-Apr-2007    SCR Number: 6237
//  Project:  Trend
//  Description:     
//      Added Trend Cursor Position Limits and the Event Subscreen 
//      prompt message
//
//  Revision: 028  By:  gdc	   Date:  27-May-2005    SCR Number: 6170
//  Project:  NIV2
//  Description:     
//      Removed Insp Too Long alarm for NIV. Added High Ti spont alert.
// 
//  Revision: 027  By:  gdc	   Date:  15-Feb-2005    SCR Number: 6144
//  Project:  NIV1
//  Description:     
//      Added new bounds strings for NIV
// 
//  Revision: 026  By:  gfu	   Date:  11-Aug-2004    SCR Number: 6132
//  Project:  PAV3
//  Description:     
//      Modified code per SDCR #6132.  Also added keyword "Log" to 
//      file header as requested during code review.
//
//  Revision: 025  By: scottp  Date:  28-Mar-2002    DR Number: 5790
//  Project:  VCP
//  Description:
//      Added VT/Vti/Vsupp setting interactions/limitations for VC+ and VS.
//
//  Revision: 024   By: quf    Date: 23-Jan-2002    DR Number: 5984
//  Project:  GUIComms
//  Description:
//	Added new strings for Serial Loopback Test.
//
//  Revision: 023  By: hlg    Date: 04-SEP-2001   DCS Number:  5931
//  Project:  GuiComms
//  Description:
//      Removed unused functionality for settings bounds for comm
//      port configuaration.
//
//  Revision: 022  By: hct    Date: 09-OCT-2000   DCS Number:  5493
//  Project:  GuiComms
//  Description:
//      Added message for new ComPortConfigValue bound.
//
//  Revision: 021  By: sah     Date:  11-Sep-2000    DR Number: 5766
//  Project:  VTPC
//  Description:
//      Added prompt for Vti-spont's new soft limit.
//
//  Revision: 020  By: sah     Date:  05-May-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      *  added new VC+-specific prompts for peak insp flow
//      *  added prompts for new Vt-supp setting
//      *  removed 'TC_NOT_ALLOWED_*' messages, because they're not needed
//         with new drop-down menu feature
//
//  Revision: 019  By: sah    Date: 27-Apr-2000   DCS Number:  5713
//  Project:  NeoMode
//  Description:
//      Added message for new PEEP soft-bound.
//
//  Revision: 018  By: sah    Date: 30-Jun-1999   DCS Number:  5327
//  Project:  NeoMode
//  Description:
//      Initial NeoMode Project implementation added.
//
//  Revision: 017  By:  sah	   Date:  23-Feb-1999    DCS Number:  5314
//  Project:  ATC
//  Description:
//      Added integrated with new dynamic, setting applicability functionality.
//      This allows for looser coupling between settings and GUI, and the
//      need for significantly less "setting knowledge" in the GUI subsystem.
//
//  Revision: 016  By: sah  Date: 08-Feb-1999  DR Number: 5314
//  Project:  ATC (R8027)
//  Description:
//      Added new BiLevel-specific settings' bound ids, and reforrmatted
//	for better readability.
//
//  Revision: 015   By: sah   Date: 06-Jan-1999  DCS Number:  5322
//  Project:  ATC
//  Description:
//		Added bound ids for new ATC settings.
//
//  Revision: 014  By: dosman   Date: 23-Nov-1998  DCS Number: 5285
//    Project:  BiLevel (R8027)
//    Description:
//      Put back string HB_INSP_TIME_MIN_BASED_PLAT_TIME_A, which is used
//
//  Revision: 014  By: dosman   Date: 23-Nov-1998  DCS Number: 5285
//    Project:  BiLevel (R8027)
//    Description:
//      Put back string HB_INSP_TIME_MIN_BASED_PLAT_TIME_A, which is used
//
//  Revision: 013  By:  gdc	   Date:  23-Nov-1998    DCS Number: 5173
//  Project:  BiLevel
//  Description:
//		Added EST_START_CANCEL_S prompt.
//
//  Revision: 012  By: btray   Date: 04-Nov-1998  DCS Number: 5251
//    Project:  BiLevel (R8027)
//    Description:
//	Removed unused prompt HB_INSP_TIME_MIN_BASED_PLAT_TIME_A
//
//  Revision: 011  By: dosman   Date: 30-Sep-1998  DR Number: 
//    Project:  BiLevel (R8027)
//    Description:
//	removed extraneous prompt id
//
//  Revision: 010  By: yyy      Date: 05-Jan-1998  DR Number: 
//    Project:  BiLevel (R8027)
//    Description:
//     Initial release for BiLevel adding software option.
//     Merged /840/Baseline/GUI-Applications/vcssrc/PromptStrs.hhv   1.69.1.0   07/30/98 10:19:22   gdc
//
//  Revision: 009  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 011  By:  dosman    Date:  29-Apr-1998    DR Number: 34
//    Project:  Sigma (R8027)
//    Description:
//	added strings for TH, TL, TH:TL for bilevel
//
//  Revision: 010  By:  dosman    Date:  23-Feb-1998    DR Number: None
//    Project:  Sigma (R8027)
//    Description:
//      Added prompt for min highcctpress based on peephigh
//
//  Revision: 009  By:  iv    Date:  18-Jan-1998    DR Number: None
//    Project:  Sigma (R8027)
//    Description:
//      Bilevel initial version.
//      Added prompts for inspiratory pause.
// 
//  Revision: 008  By:  yyy    Date: 08-Oct-1997    DR Number: DCS 2326, 2443
//  Project:  Sigma (840)
//  Description:
//		Added PromptId CONNECT_HUMIDIFIER_PROMPT.
//
//  Revision: 007  By:  yyy    Date: 01-Oct-1997    DR Number: DCS 2204
//  Project:  Sigma (840)
//  Description:
//      Added PromptId DISCONNECT_AIR_AND_O2_PROMPT for revised
//      EST Battery Test.
//		21-Oct-1997 Added SM_UNPLUG_AC
//
//  Revision: 006  By: sah    Date: 26-Sep-1997  DR Number: 2410
//  Project:  Sigma (R8027)
//  Description:
//      Added HB_ATM_PRESSURE_{MIN,MAX}_A bound messages.
//
//  Revision: 005  By: sah    Date: 25-Sep-1997  DR Number: 1825
//  Project:  Sigma (R8027)
//  Description:
//      Added HB_DCI_DATA_BITS_BASED_PARITY_A and
//      HB_DCI_PARITY_MODE_BASED_BITS_A bound messages.
//
//  Revision: 004  By: yyy      Date: 04-Aug-1997  DR Number: 2312
//    Project:  Sigma (R8027)
//    Description:
//      Added INIT_PROMPT_ACCEPT_CLEAR_CANCEL_S; prompt.
//
//  Revision: 003  By: yyy      Date: 04-Aug-1997  DR Number: 2314
//    Project:  Sigma (R8027)
//    Description:
//      Added DATE_TIME_CANCEL_S, DATE_TIME_PROCEED_CANCEL_S prompts.
//
//  Revision: 002  By: yyy      Date: 07-May-1997  DR Number: 2059
//    Project:  Sigma (R8027)
//    Description:
//      Removed unused prompts.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "GuiAppClassIds.hh"

class PromptStrs
{
	public:
#include "PromptStrsDecl.hh"

	private:
		// these methods are purposely declared, but not implemented...
		PromptStrs(void);												// not implemented...
		~PromptStrs(void);												// not implemented...
		PromptStrs(const PromptStrs&);					// not implemented...
		void operator=(const PromptStrs&);				// not implemented...
};

#ifndef DEFINE_STRS
#include "GuiApp.hh"
#include "PromptStrsDef.hh"
#endif


#endif // PromptStrs_HH 


