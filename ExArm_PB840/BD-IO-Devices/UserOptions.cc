#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: UserOptions - Storage class for User-Configured Software Options
//---------------------------------------------------------------------
//@ Interface-Description
//  This class is used to store the user-set option bits into NOVRAM. As
//  a class that is to be stored in NOVRAM, it defines a default constructor,
//  and assignment operator.  And, as a class that is used by 'SoftwareOptions',
//  it defines query methods for its state and value.
//---------------------------------------------------------------------
//@ Rationale
//  'NovRamManager' needs a simple class for storing data in NOVRAM,
//  therefore this acts as a go-between with 'SoftwareOptions' and
//  'NovRamManager'.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Simple implementation of assignment operator, and various query
//  methods.
//---------------------------------------------------------------------
//@ Fault-Handling
//  n/a
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/UserOptions.ccv   25.0.4.0   19 Nov 2013 13:54:14   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By:  sah    Date: 09-Sep-1999    DR Number: 5424
//  Project:  NeoMode
//  Description:
//      Created to support changing of software option bits, via the
//      Development Options Subscreen.
//
//=====================================================================

#include "UserOptions.hh"
#include "BD_IO_Devices.hh"

//@ Usage-Classes
//@ End-Usage


//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: UserOptions()
//
//@ Interface-Description
//  Default Constructor.
//--------------------------------------------------------------------- 
//@ Implementation-Description
//--------------------------------------------------------------------- 
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition 
//      None 
//@ End-Method 
//===================================================================== 

UserOptions::UserOptions(void)
						: initializationFlag_(FALSE),
						  optionBits_(0u)
{
	CALL_TRACE("UserOptions()");
}  // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~UserOptions()
//
//@ Interface-Description
//  Destructor.
//--------------------------------------------------------------------- 
//@ Implementation-Description
//--------------------------------------------------------------------- 
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition 
//  None 
//@ End-Method 
//===================================================================== 

UserOptions::~UserOptions(void)
{
	CALL_TRACE("~UserOptions()");	
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: operator=
//
//@ Interface-Description
//  Assign 'userOptions' into this instance.
//--------------------------------------------------------------------- 
//@ Implementation-Description
//--------------------------------------------------------------------- 
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

void
UserOptions::operator=(const UserOptions& userOptions)
{
	CALL_TRACE("operator=(userOptions)");

	if (this != &userOptions)
	{  // $[TI1]
		initializationFlag_ = userOptions.initializationFlag_;
		optionBits_         = userOptions.optionBits_;
	}  // $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//  and 'lineNumber' are essential pieces of information.  The
//  'pFileName' and 'pPredicate' strings may be defaulted in the macro
//  to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//  none 
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

void
UserOptions::SoftFault( const SoftFaultID  softFaultID,
                   		const Uint32       lineNumber,
		   				const char*        pFileName,
		   				const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  	FaultHandler::SoftFault( softFaultID, BD_IO_DEVICES, USEROPTIONS,
  							 lineNumber, pFileName, pPredicate) ;
}
