#ifndef BoundedSetting_HH
#define BoundedSetting_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  BoundedSetting - Bounded Setting.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/BoundedSetting.hhv   25.0.4.0   19 Nov 2013 14:27:16   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006   By: gdc    Date: 16-Aug-2010     SCR Number: 6663
//  Project: API/MAT
//  Description:
//  Added support for VentSet API.
// 
//  Revision: 005   By: sah    Date: 29-Jun-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  added new virtual method, 'resetConstraints()' for resetting
//         all dynamic constraints to their absolute limit counterpart,
//         to ensure correct calculations during new-patient initialization
//	*  re-designed much of the settings framework, moving many methods
//         up to this level, further simplifying, and genericizing, the
//         design
//	*  added new 'warpToRange()' method
//	*  added new 'getAbsolute{Min,Max}Value_()' methods
//
//  Revision: 004   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  moved 'get{Max,Min}Limit()' methods to 'Setting' base class
//
//  Revision: 003   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 002   By: sah    Date:  07-May-1997    DCS Number: 2034
//  Project: Sigma (R8027)
//  Description:
//	Part of the fix to the problem 'RESTART' of New-patient setup
//	producing "clipped" tidal volumes.  This class now allows
//	derived classes non-constant access to its private 'rBoundedRange_'
//	member, via a "protected" method, 'getBoundedRange_()'.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//====================================================================

//@ Usage-Classes
#include "Setting.hh"
#include "BoundedRange.hh"
//@ End-Usage


class BoundedSetting : public Setting 
{
  public:
    virtual ~BoundedSetting(void);

    inline const BoundedRange&  getBoundedRange(void) const;

    virtual void  resetConstraints(void);

    virtual Real32  getMaxLimit(void);
    virtual Real32  getMinLimit(void);

    virtual const BoundStatus&  calcNewValue(const Int16 knobDelta);

    void  warpToRange(BoundedValue&               rCurrValue,
		      const BoundedRange::WarpDir warpDirection =
					       BoundedRange::WARP_NEAREST,
		      const Boolean               clipToRange = TRUE);

#if defined(SIGMA_DEVELOPMENT)
    virtual Boolean  isAcceptedValid(void) const;
    virtual Boolean  isAdjustedValid(void);
#endif  // defined(SIGMA_DEVELOPMENT)

	// change requested through GUI serial interface
	virtual SigmaStatus ventset(const char* requestedValue);

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL, 
			   const char*       pPredicate = NULL);

  protected:
    BoundedSetting(const SettingId::SettingIdType  settingId,
		   const SettingId::SettingIdType* arrDependentSettingIds,
		   const BoundedInterval*          pIntervalList,
		   const SettingBoundId            maxConstraintId,
		   const SettingBoundId            minConstraintId,
		   const Boolean                   useEpsilonFactoring);

    inline BoundedRange&  getBoundedRange_(void);

    virtual Real32  getAbsoluteMaxValue_(void) const;
    virtual Real32  getAbsoluteMinValue_(void) const;

	//@ Data-Member:  isATimingSetting_;
	// Set TRUE if this setting is a time-related setting stored in
	// milliseconds yet presented on the GUI in seconds resolution.
	Boolean  isATimingSetting_;

#if defined(SIGMA_DEVELOPMENT)
    virtual Ostream&  print_(Ostream& ostr) const;
#endif  // defined(SIGMA_DEVELOPMENT)

  private:
    BoundedSetting(const BoundedSetting&);	// not implemented...
    BoundedSetting(void);			// not implemented...
    void operator=(const BoundedSetting&);	// not implemented...

    //@ Type:  ChangeState_
    // States used by 'calcNewValue()', for managing value placement
    // following a bound violation.
    enum ChangeState_
    {
      NO_CHANGE,
      CHANGE__NO_VIOLATION,
      CHANGE__VIOLATION,

      NUM_CHANGE_STATES
    };

    //@ Data-Member:  valueInterval_
    // Instance to manage list of intervals, plus constraint info.
    BoundedRange  valueInterval_;
};


// Inlined methods...
#include "BoundedSetting.in"


#endif // BoundedSetting_HH 
