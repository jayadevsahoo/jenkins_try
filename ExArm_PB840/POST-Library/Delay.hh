#ifndef DELAY_HH
#define DELAY_HH
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================ H E A D E R   D E S C R I P T I O N ====
//@ Class: Delay - class to wait for transitions of external clock 
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/POST-Library/vcssrc/Delay.hhv   25.0.4.0   19 Nov 2013 14:16:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003    By: Gary Cederquist  Date: 27-Jan-2011  SCR Number: 6671
//  Project: XENA2
//  Description:
//      Moved device struct declarations to their own headers DS1286.hh and
//      STM48T58.hh. 
//
//  Revision: 002    By: Mitesh Raval   Date: 14-Oct-2010  SCR Number: 6671
//  Project: XENA2
//  Description:
//       Class DS1286 renamed to Delay.
//       New struct BQ4822YRegMap added for Xena2 version RTC.
//
//  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//=====================================================================

#include "Sigma.hh"

class Delay 
{
public:
    static Boolean WaitForTicks(Uint32 numberTicks);

private:
    Delay(void);
    ~Delay(void);

    static Boolean Xena2WaitForTicks_(Uint32 numberTicks);
    static Boolean DS1286WaitForTicks_(Uint32 numberTicks);
};

#endif // DELAY_HH
