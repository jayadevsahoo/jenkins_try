#ifndef SstProxSubScreen_HH
#define SstProxSubScreen_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: SstProxSubScreen - The subscreen is automatically activated
// during SST Test SubScreen.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/SstProxSubScreen.hhv   25.0.4.0   19 Nov 2013 14:08:30   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003   By: mnr   Date: 03-Jun-2010    	SCR Number: 6436
//  Project:  PROX
//  Description:
//		formatLabel_ removed, code review action items related changes.
//
//  Revision: 002  By:  mnr    Date:  20-APR-2010    	SCR Number: 6436
//  Project:  PROX
//  Description:
//      LeakGuage removed.
//
//  Revision: 001  By:  mnr    Date:  13-APR-2010    	SCR Number: 6436
//  Project:  PROX
//  Description:
//      Initial checkin.
//====================================================================

#include "SubScreen.hh"
#include "GuiTestManagerTarget.hh"

//@ Usage-Classes
#include "LargeContainer.hh"
#include "GuiTestManager.hh"
#include "SmTestId.hh"
#include "SmDataId.hh"
#include "SubScreenTitleArea.hh"
#include "TestDataId.hh"
#include "TextField.hh"
#include "GuiAppClassIds.hh"
#include "ProxTest.hh"

class TestResult;
//@ End-Usage

class SstProxSubScreen : public SubScreen,
						 public GuiTestManagerTarget
{
public:
	SstProxSubScreen(SubScreenArea *pSubScreenArea);
	~SstProxSubScreen(void);

	// Overload SubScreen methods
	virtual void activate(void);
	virtual void deactivate(void);

	// GuiTestManagerTarget virtual method
	virtual void processTestData(Int dataIndex, TestResult *pResult);

	// Allow the outside world to set the title area on this subScreen.
	void setResultTitle(StringId title);
	void setDataFormat(ProxTest::ProxSstSubTestId testId);
	void clearScreen(void);

    static void SoftFault(const SoftFaultID softFaultID,
				  		  const Uint32      lineNumber,
				  		  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:

private:
	// these methods are purposely declared, but not implemented...
	SstProxSubScreen(void);							// not implemented..
	SstProxSubScreen(const SstProxSubScreen&);	// not implemented..
	void operator=(const SstProxSubScreen&);			// not implemented..

	void setText_(TextField *testResultField, wchar_t * resultString);   
	void setupDisplayableTestResult_(SmDataId::ItemizedTestDataId testDataIdx,
									Int16 dataPrecision, Int16 *currentDisplayableCount,
									StringId labelString, StringId unitString);
    
	//@ Data-Member: titleArea_
	// The subscreen's title at the top left of the screen.
	SubScreenTitleArea titleArea_;
	
	//@ Data-Member: valueAreaContainer_
	// A container which holds all the test value displayable texts.
	LargeContainer  valueAreaContainer_;

	//@ Data-Member: labelAreaContainer_
	// A container which holds all the test label displayable texts.
	LargeContainer  labelAreaContainer_;

	//@ Data-Member: unitAreaContainer_
	// A container which holds all the test unit displayable texts.
	LargeContainer  unitAreaContainer_;

	//@ Data-Member: testResultValueArray_
	// The text field for displaying the test result value.
	TextField testResultValueArray_[TestDataId::MAX_DATA_ENTRIES];

	//@ Data-Member: testResultLabelArray_
	// The text field for displaying the test result label.
	TextField testResultLabelArray_[TestDataId::MAX_DATA_ENTRIES];

	//@ Data-Member: testResultUnitArray_
	// The text field for displaying the test result unit.
	TextField testResultUnitArray_[TestDataId::MAX_DATA_ENTRIES];

	//@ Data-Member: isTestResultDisplayable_
	// The boolean array indicates if the corresponding test result shall
	// be displayed on GUI screen.
	Boolean isTestResultDisplayable_[TestDataId::MAX_DATA_ENTRIES];

	//@ Data-Member: testId_
	// The current service test Id in testing.
	ProxTest::ProxSstSubTestId testId_;
    
	//@ Data-Member: testManager_
	// An object which look after rendering the service mode test.
	GuiTestManager testManager_;
	
	//@ Data-Member: maxDataCount_
	// The maximun number of data expected for each test in testing.
	Int maxDataCount_;
	
	//@ Data-Member: maxDisplayableDataCount_
	// The maximun number of data expected for each test in testing.
	Int maxDisplayableDataCount_;

	//@ Data-Member: proxCurrentTestIndex_
	// The current PROX sub-test id.
	Int proxCurrentTestIndex_;

	//@ Data-Member: pressureUnitId_
	// The unit used based on settings.
	StringId pressureUnitId_;

};

#endif // SstProxSubScreen_HH 

