
#ifndef NonBatchValue_HH
#define NonBatchValue_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: NonBatchValue - Union of the possible values for a non-batch setting.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/NonBatchValue.hhv   25.0.4.0   19 Nov 2013 14:27:32   pvcs  $
//
//@ Modification-Log
//  
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//		Integration baseline.
//
//====================================================================

#include "SettingClassId.hh"
#include "SettingId.hh"

//@ Usage-Classes
#include "BoundedValue.hh"
#include "DiscreteValue.hh"
#include "SequentialValue.hh"
//@ End-Usage


class NonBatchValue
{
  public:
    NonBatchValue(void);
    ~NonBatchValue(void);

    inline const BoundedValue& getBoundedValue(void) const;
    inline void                setBoundedValue(const BoundedValue& newValue);

    inline DiscreteValue  getDiscreteValue(void) const;
    inline void           setDiscreteValue(const DiscreteValue newValue);

    inline SequentialValue  getSequentialValue(void) const;
    inline void           setSequentialValue(const SequentialValue newValue);

  private:
    NonBatchValue(const NonBatchValue&);	// not implemented...

    union
    {
      //@ Data-Member:  boundedValue_
      // A bounded, non-batch setting value.
      BoundedValue  boundedValue_;

      //@ Data-Member:  discreteValue_
      // A discrete, non-batch setting value.
      DiscreteValue  discreteValue_;

      //@ Data-Member:  sequentialValue_
      // A sequential, non-batch setting value.
      SequentialValue  sequentialValue_;
    };
};


// Inlined methods
#include "NonBatchValue.in"


#endif // NonBatchValue_HH 
