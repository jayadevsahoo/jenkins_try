#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: SettingValueMgr - Manager of the Setting Values.
//---------------------------------------------------------------------
//@ Interface-Description
//  This class provides a common base class for all of the setting-values
//  managers (e.g., 'BatchSettingValues' and 'BdSettingValues').  This
//  class provides a callback facility for all of the derived classes to
//  use for the notification of value changes.  There is a public method
//  for clients of the derived classes to register their callback functions
//  with, and a protected method for the derived classes to activate their
//  callback, if any.
//---------------------------------------------------------------------
//@ Rationale
//  This provides a general callback mechanism for all of the setting-values
//  managers.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The pointer to the callback is kept as a private member of this class.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/SettingValueMgr.ccv   25.0.4.0   19 Nov 2013 14:27:44   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003   By: sah  Date:  11-Jan-1999    DR Number:  5321
//  Project:  ATC
//  Description:
//	As part of making all 'SoftFault()' methods non-inlined, I'm
//	also making those 'SoftFault()' methods that are NOT called
//	in PRODUCTION mode, DEVELOPMENT-only.
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//=====================================================================

#include "SettingValueMgr.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~SettingValueMgr(void)  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this setting values instance.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValueMgr::~SettingValueMgr(void)
{
  CALL_TRACE("~SettingValueMgr()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  registerCallback(pCallback)  [virtual]
//
//@ Interface-Description
//  Register the function pointed to by 'pCallback' as a callback
//  function for this instance.  Any changes to any of the values of
//  this instance will activate this callback.  The previous callback
//  pointer is returned from this method.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValueMgr::ChangeCallbackPtr
SettingValueMgr::registerCallback(
			    SettingValueMgr::ChangeCallbackPtr pNewCallback
				 )
{
  CALL_TRACE("registerCallback(pNewCallback)");

  // store the current callback pointer...
  ChangeCallbackPtr  pOldCallback = pCallback_;

  // copy over the new callback pointer...
  pCallback_ = pNewCallback;

  // return the old callback pointer...
  return(pOldCallback);
}    // $[TI1]


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//	    [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
SettingValueMgr::SoftFault(const SoftFaultID  softFaultID,
			   const Uint32       lineNumber,
			   const char*        pFileName,
			   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
			  SETTING_VALUE_MGR, lineNumber, pFileName,
			  pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SettingValueMgr(pInitCallBack)  [Constructor]
//
//@ Interface-Description
//  Construct a setting value's instance, using 'pInitCallback'
//  as the pointer to the value-change callback function.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValueMgr::SettingValueMgr(
			    SettingValueMgr::ChangeCallbackPtr pInitCallback
			        )
			    : pCallback_(pInitCallback)
{
  CALL_TRACE("SettingValueMgr(pInitCallback)");
}   // $[TI1]
