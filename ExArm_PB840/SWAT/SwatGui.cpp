#include "StdAfx.h"
#include <new.h>
#include "SwatGui.h"
#include "UdpCommunication.h"
#include "PortIds.hh"

namespace swat
{
	SwatGui*	SwatGui::PInstance_		=	NULL;
	
	static Uint UdpCommMem[(sizeof(UdpCommunication)+sizeof(Uint)-1)/sizeof(Uint)];

	UdpCommunication& SwatGui::RUdpComm_ = *((UdpCommunication*) UdpCommMem);

	const char SWAT_GUI_IP_ADDR[] = "192.168.0.2";
	#define	   MAX_MSG_LENGTH	    (1024)

	SwatGui::SwatGui(void)
	{
		new (&RUdpComm_) UdpCommunication();

		RUdpComm_.setup( UdpCommunication::UDP_SEND, 0, SWAT_GUI_IP_ADDR, SWAT_GUI_COMMAND_UDP_PORT);
	}

	SwatGui::~SwatGui(void)
	{
		RUdpComm_.stop();
	}

	SwatGui*	SwatGui::GetInstance()
	{
		if ( NULL == PInstance_ )
		{
			PInstance_	= new SwatGui();
		}

		return PInstance_;
	}

	void SwatGui::simulateTouch(int x, int y)
	{
		int seconds = 0;
		int microseconds = 0;
		int transactionId = rand();

		char msg[MAX_MSG_LENGTH];
		sprintf_s(msg, MAX_MSG_LENGTH,
			"<m>"
				"<h t='%d' s='%d' m='%d' />"
				"<push_xy>"
					"<p1>%d</p1><p2>%d</p2>"
				"</push_xy>"
			"</m>",
			transactionId, seconds, microseconds, x, y );

		DWORD bytesWritten = 0;

		RUdpComm_.writeData( (Uint8*)msg, strlen(msg), &bytesWritten );
	}

	void SwatGui::simulateBezelKey(int keyId, int onOff)
	{
		int seconds = 0;
		int microseconds = 0;
		int transactionId = rand();

		char msg[MAX_MSG_LENGTH];
		sprintf_s(msg, MAX_MSG_LENGTH,
			"<m>"
				"<h t='%d' s='%d' m='%d' />"
				"<bezel_key_press>"
					"<on_off>%d</on_off>"
					"<key_id>%d</key_id>"
				"</bezel_key_press>"
			"</m>",
			transactionId, seconds, microseconds, onOff, keyId );

		DWORD bytesWritten = 0;

		RUdpComm_.writeData( (Uint8*)msg, strlen(msg), &bytesWritten );
	}
}