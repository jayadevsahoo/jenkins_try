
#ifndef ResultEntryData_HH
#define ResultEntryData_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sotred in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class:  ResultEntryData - Data for a result entry.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Persistent-Objects/vcssrc/ResultEntryData.hhv   25.0.4.0   19 Nov 2013 14:18:54   pvcs  $
//
//@ Modification-Table
//
//  Revision: 002  By:  sah    Date:  01-Jan-1999    DCS Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' to a non-inlined method.
//
//  Revision: 001  By:  sah    Date:  15-Mar-1997    DCS Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//          Initial version (Integration baseline).
//
//====================================================================

#include "PersistentObjsId.hh"
#include "TestResultId.hh"

//@ Usage-Classes
#include "TimeStamp.hh"

#if defined(SIGMA_DEVELOPMENT)
#include "Ostream.hh"
#endif  // defined(SIGMA_DEVELOPMENT)
//@ End-Usage


class ResultEntryData
{
#if defined(SIGMA_DEVELOPMENT)
    friend Ostream&  operator<<(Ostream&               ostr,
				const ResultEntryData& entryData);
#endif  // defined(SIGMA_DEVELOPMENT)

  public:
    ResultEntryData(const ResultEntryData& entryData);
    ResultEntryData(void);
    ~ResultEntryData(void);

    inline TestResultId  getResultId    (void) const;
    inline TimeStamp     getTimeOfResult(void) const;

    inline Boolean  isTestCompleted(void) const;
    inline void     setCompletionFlag(const Boolean newCompletionValue);

    inline Uint32  getTestStartSeqNumber(void) const;
    inline void    setTestStartSeqNumber(const Uint32 newSequenceNumber);

    inline TestResultId  getResultOfPrevRun(void) const;
    inline void          setResultOfPrevRun(
				      const TestResultId newResultOfPrevRun
					   );

    inline TestResultId  getResultOfCurrRun(void) const;
    inline void          setResultOfCurrRun(
    					const TestResultId newResultOfCurrRun
					   );

    inline TimeStamp  getTimeOfCurrResult  (void) const;
    inline void       resetTimeOfCurrResult(void);
    inline void       setTimeOfCurrResult  (const TimeStamp& newTime);

    void  saveCurrValues(void);

    void  operator=(const ResultEntryData& entryData);

    static void  SoftFault(const SoftFaultID softDiagID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL,
			   const char*       pPredicate = NULL);

  private:
    inline Boolean  usePrevResult_(void) const;

    //@ Data-Member:  isCompleted
    Boolean  isCompleted_;

    //@ Data-Member:  testStartSequenceNumber
    Uint32  testStartSequenceNumber_;

    //@ Data-Member:  resultOfPrevRun
    TestResultId  resultOfPrevRun_;

    //@ Data-Member:  timeOfPrevResult
    TimeStamp  timeOfPrevResult_;

    //@ Data-Member:  resultOfCurrRun
    TestResultId  resultOfCurrRun_;

    //@ Data-Member:  timeOfCurrResult
    TimeStamp  timeOfCurrResult_;
};


// Inlined methods..
#include "ResultEntryData.in"


#endif // ResultEntryData_HH 
