#ifndef BitmapInfoHeader_HH
#define BitmapInfoHeader_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: BitmapInfoHeader -  struct containing information for
//                            Windows standard bitmaps
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/VGA-Graphics-Driver/vcssrc/BitmapInfoHeader.hhv   25.0.4.0   19 Nov 2013 14:37:28   pvcs  $
//
//@ Modification-Log
//  
//  Revision: 001   By: gdc    Date:  27-Jan-2000    DCS Number: 5337
//  Project: 840 Neonatal
//  Description:
//	Optimized VGA library for Neonatal project to use BitBlt and Windows
//  Bitmap structures.
//
//====================================================================

#include "Sigma.hh"

//@ Usage-Classes
//@ End-Usage

struct BitmapInfoHeader
{
  Uint32    biSize;         // Size of the BitmapInfoHeader structure in bytes
  Uint32    biWidth;        // Width of the bitmap in pixels
  Uint32    biHeight;       // Height of the bitmap in pixels
  Uint16    biPlanes;       // Set to 1
  Uint16    biBitCount;     // Color bits per pixel (1, 4, 8, or 24)
  Uint32    biCompression;  // Compression scheme (0 for none)
  Uint32    biSizeImage;    // Size of bitmap bits in bytes
  Uint32    biXPelsPerMeter;// Horizontal resolution in pixels per meter
  Uint32    biYPelsPerMeter;// Vertical resolution in pixels per meter
  Uint32    biClrUsed;      // Number of colors used in image
  Uint32    biClrImportant; // Number of important colors in image
};
 
#endif // BitmapInfoHeader_HH 
