#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: VtpcvPhase - Implements volume targeted pressure control
//					   ventilation phase.
//---------------------------------------------------------------------
//@ Interface-Description
//		This class is derived from PcvPhase class.  No public methods are
//		defined by this class since base class methods are used.  Protected
//		virtual methods are implemented to support the base class.
//---------------------------------------------------------------------
//@ Rationale
// 		This class implements the algorithms for volume targeted pressure
//		controlled inspirations.
//---------------------------------------------------------------------
//@ Implementation-Description
// 		The data members values from the base class are defined	in the pure
//		virtual methods.
//---------------------------------------------------------------------
//@ Fault-Handling
//		N/A
//---------------------------------------------------------------------
//@ Restrictions
//		none
//---------------------------------------------------------------------
//@ Invariants
//		none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/VtpcvPhase.ccv   25.0.4.0   19 Nov 2013 14:00:18   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By:  syw    Date:  03-Sep-2000    DR Number: 5755
//       Project:  VTPC
//       Description:
//             Initial version
//
//=====================================================================

#include "VtpcvPhase.hh"

#include "ControllersRefs.hh"

//@ Usage-Classes

#include "VolumeTargetedController.hh"
#include "PhasedInContextHandle.hh"

//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: VtpcvPhase()
//
//@ Interface-Description
//		Constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Base class constructor is called and the tital volume id are stored.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

VtpcvPhase::VtpcvPhase( const SettingId::SettingIdType tidalVolumeId,
    	      	const SettingId::SettingIdType inspTimeId,
    	      	const SettingId::SettingIdType flowAccelPercentId)
 : PcvPhase( SettingId::INSP_PRESS, inspTimeId, flowAccelPercentId)
{
	CALL_TRACE("VtpcvPhase::VtpcvPhase( \
				const SettingId::SettingIdType tidalVolumeId, \
    	      	const SettingId::SettingIdType inspTimeId, \
    	      	const SettingId::SettingIdType flowAccelPercentId)");

  	// $[TI1]
	tidalVolumeId_ = tidalVolumeId ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~VtpcvPhase()
//
//@ Interface-Description
//		Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//		none
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

VtpcvPhase::~VtpcvPhase(void)
{
	CALL_TRACE("VtpcvPhase::~VtpcvPhase(void)") ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
VtpcvPhase::SoftFault( const SoftFaultID  softFaultID,
                     const Uint32       lineNumber,
		   			 const char*        pFileName,
		   			 const char*        pPredicate)
{
	CALL_TRACE("SoftFault( softFaultID, lineNumber, pFileName, pPredicate)") ;
  	FaultHandler::SoftFault( softFaultID, BREATH_DELIVERY, VTPCVPHASE,
                          	 lineNumber, pFileName, pPredicate) ;
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: determineEffectivePressureAndBiasOffset_
//
//@ Interface-Description
//		This method has no arguments and returns nothing.  This method is
//      called by the base class, PressureBasePhase, to determine the
//		effective pressure and the bias offset. 
//---------------------------------------------------------------------
//@ Implementation-Description
//		The effectivePressure_ is computed by the output of the
//		VolumeTargetedController.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
VtpcvPhase::determineEffectivePressureAndBiasOffset_( void)
{
	CALL_TRACE("VtpcvPhase::determineEffectivePressureAndBiasOffset_( void)") ;

  	// $[TI1]

	// $[VC24006]
	// $[VC24008]
	// $[VC24010]
	static const Real32 ALPHA = 0.5 ;
	static const Real32 KP = 0.7 ;
	static const Real32 LOWER_LIMIT = 5.0 ;
	
	biasOffset_ = 0.0F ;

	effectivePressure_ = RVolumeTargetedController.getPressureTrajectory(
					PhasedInContextHandle::GetBoundedValue(tidalVolumeId_).value,
					ALPHA, KP, LOWER_LIMIT) ;
}

//=====================================================================
//
//  Private Methods...
//
//=====================================================================













