#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: BitAccessGpioMediator - To manage the write operations of
//			all the bit access registers.
//---------------------------------------------------------------------
//@ Interface-Description
//      This class is used to moderate the update of all the
//		BitAccessGpio instances to the hardware I/O address.  A method
//		is provided for this update and it should be called each BD cycle.
//---------------------------------------------------------------------
//@ Rationale
//		Used to update all the BitAccessGpio instances.
//---------------------------------------------------------------------
//@ Implementation-Description
//		A list of all the pointers of BitAccessGpio are stored and
//		is used to traverse the list to update each BitAccessGpio.
//---------------------------------------------------------------------
//@ Fault-Handling
//		n/a
//---------------------------------------------------------------------
//@ Restrictions
//      none
//---------------------------------------------------------------------
//@ Invariants
//      none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/BitAccessGpioMediator.ccv   25.0.4.0   19 Nov 2013 13:53:58   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003 By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//	Description:
//		Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 002  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 001  By:  syw    Date: 08-Nov-1996    DR Number: INITIAL RELEASE
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "BitAccessGpioMediator.hh"

//@ Usage-Classes

//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BitAccessGpioMediator()
//
//@ Interface-Description
//      Constructor.  This method has no arguments and returns nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The list is initialized with all the BitAccessGpio instances.
//		The BD alarm bit, and the loss of gui led bits are enabled for
//		on/off logic.
//---------------------------------------------------------------------
//@ PreCondition
//		ii == MAX_REGISTERS
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

BitAccessGpioMediator::BitAccessGpioMediator( BitAccessGpio& rBitAccessGpio)
{
	CALL_TRACE("BitAccessGpioMediator::BitAccessGpioMediator( void)") ;

	pBitAccessGpio_ = &rBitAccessGpio;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~BitAccessGpioMediator()
//
//@ Interface-Description
//      Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//      none
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

BitAccessGpioMediator::~BitAccessGpioMediator( void)
{
	CALL_TRACE("BitAccessGpioMediator::~BitAccessGpioMediator( void)") ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newCycle
//
//@ Interface-Description
//		This method has no arguments and returns nothing.  This method
//		is called to update the hardware registers to the values in the
//		BitAccessGpio instances.
//---------------------------------------------------------------------
//@ Implementation-Description
//   	The list is traversed to call the writeNewCycle() method.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
BitAccessGpioMediator::newCycle( void)
{
	CALL_TRACE("BitAccessGpioMediator::newCycle( void)") ;
    pBitAccessGpio_->NewCycle();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault()
//
//@ Interface-Description
//      This method takes four arguments: softFaultID, lineNumber, pFileName,
//      and pPredicate and is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
BitAccessGpioMediator::SoftFault( const SoftFaultID  softFaultID,
                     const Uint32       lineNumber,
                     const char*        pFileName,
                     const char*        pPredicate)
{
    CALL_TRACE("SoftFault( softFaultID, lineNumber, pFileName, pPredicate)") ;

    FaultHandler::SoftFault( softFaultID, BD_IO_DEVICES, BITACCESSGPIOMEDIATOR,
                             lineNumber, pFileName, pPredicate) ;
}

//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//=====================================================================
//
//  Private Methods...
//
//=====================================================================
