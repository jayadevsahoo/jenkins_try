#pragma once

class CheckLoop
{
private:
	int m_ncount;
	unsigned int m_ntime;

public:
	CheckLoop()
	{
		m_ncount = 0;
		m_ntime = (int)time (NULL);
	}

	void AddCount()
	{
		m_ncount++;
	}

	bool TestLoop(int nTotalCount, int nTimeDiff)
	{
		if(m_ncount >= nTotalCount)
		{
			if((time(NULL) - m_ntime) > nTimeDiff)
			{
				m_ncount = 0;
				m_ntime = (int)time(NULL);
				return false;
/*
				#ifdef DATA_CAPTURE
					assert(0);
				#endif

				#ifdef SWATGUI
					ASSERT(0);
				#endif
*/
			}
			m_ncount = 0;
			m_ntime = (int)time(NULL);
		}
		return true;

	}

}
;