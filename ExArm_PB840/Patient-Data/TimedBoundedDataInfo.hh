#ifndef TimedBoundedDataInfo_HH
#define TimedBoundedDataInfo_HH

#if		defined(SIGMA_GUI_CPU) || defined(SIGMA_COMMON)

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, NellCor/Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// TimedBoundedDataInfo: Data processing agent class for bounded breath 
//   data types with additional timeout constraints.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Patient-Data/vcssrc/TimedBoundedDataInfo.hhv   25.0.4.0   19 Nov 2013 14:18:00   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004   By: gdc    Date: 26-Jan-2009    SCR Number: 6407
//  Project:  840S
//  Description:
//      Modified to bring source into strict ARM compliance.
//
//  Revision: 003  By:  gdc    Date:  12-Dec-2008    SCR Number: 6068
//  Project:  840S
//  Description:
//		Removed SUN prototype code.
//
//  Revision: 002  By: sah   Date:  13-Jan-99    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001  By:  jhv    Date:  23-Jan-1995   DR Number: 
//	Project:  Sigma (R8027)
//	Description:
//		Initial version (Integration baseline).
//
//====================================================================

#include "BoundedDataInfo.hh"
#include  "PDTimerTarget.hh"
#include  "PatientDataId.hh"
#include  "PatientDataClassIds.hh"
#include  "MutEx.hh"
#include "OsTimeStamp.hh"

class  TimedBoundedDataInfo : public  BoundedDataInfo, virtual public PDTimerTarget
{
  public:
    // PDTimerTarget virtual function.
    virtual void timerEventHappened(PDTimerId::PDTimerIdType timerId);
    TimedBoundedDataInfo( const PatientDataId::PatientItemId id,
    const Real32 min, const Real32 max, const Real32 changePoint,
	const Precision lowerP, const Precision higherP,
	PDTimerId::PDTimerIdType timerId);
    ~TimedBoundedDataInfo(void); 
	static void Initialize();
	virtual void setDataTimeout(void);

    static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

  
  protected:
    virtual void modifyData_(void *pValue );
  private:
    TimedBoundedDataInfo(const TimedBoundedDataInfo&);	// not implemented...
    void   operator=(const TimedBoundedDataInfo&);	// not implemented...

    //@Data-Member: RTimeLock_
    // RTimeLock_ is used to seriaize updates to a patient data item.
    static MutEx	&RTimeLock_;

    //@Data-Member: TIMER_ID_
    // A unique Timer id for a TimedBoundedDataInfo agent object.
    const PDTimerId::PDTimerIdType TIMER_ID_;

    //@Data-Membe: updateTime_;
    // A time stamp that records time that the data was update last.
    OsTimeStamp updateTime_;
};


#endif  //SIGMA_GUI_CPU || SIGMA_COMMON

#endif // TimedBoundedDataInfo_HH 
