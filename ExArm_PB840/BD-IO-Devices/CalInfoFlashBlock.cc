#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: CalInfoFlashBlock - interface to flash memory
//---------------------------------------------------------------------
//@ Interface-Description
//		This class provides the interface to the calibration data that is
//		to reside in flash memory.  Methods are provided to determine
//		whether the flow sensor data is valid, whether the exhalation
//		valve calibration table is valid, to initialize the class, to verify
//		if the flash data is okay and to program the flash.
//
//		This class provides support for the BD_CPU and the GUI_CPU.
//---------------------------------------------------------------------
//@ Rationale
//		Abstraction of the calibration flash block.
//---------------------------------------------------------------------
//@ Implementation-Description
//		In order to program flash, all data in the flash block must be
//		erased.  A flash image exists and mirrors only the valid data that
//		resides	in flash memory so that the valid data can be retained.
//		Updating of the data is made via the flash image which overwrites the
//		mirrored flash data.  The data in the flash image will then be
//		programmed to flash memory.  While programming, the
//		ServiceModeBackgroundCycle is disabled to prevent the reading of the
//		flash data while the new data is being programmed.  NetworkApp
//		"keep alive" messages along with task monitor reporting are disabled
//		since programming flash will require disabling of interrupts.
//---------------------------------------------------------------------
//@ Fault-Handling
//		n/a
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/CalInfoFlashBlock.ccv   25.0.4.0   19 Nov 2013 13:54:00   pvcs  $
//
//@ Modification-Log
//
//  Revision: 007  By: rhj    Date:  07-Aug-2006    DR Number: 6192
//  Project:  XENA
//  Description:
//      Added support for BD XENA Spansion Flash memory.
//
//  Revision: 006  By: rhj    Date:  11-April-2006    DR Number: 6192
//  Project:  XENA
//  Description:
//      Added support for the XENA Spansion Flash memory.
//      These changes only supports the new XENA GUI board not
//      the older GUI boards.
//
//  Revision: 005 By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//	Description:
//		Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 004  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 003 By: sp   Date: 28-Jan-1997   DR Number: NONE
//      Project:  Sigma (R8027)
//      Description:
//			Changed interface to BdTasks::SetServiceModeBackgroundEnabled.
//			Inspection rework.
//
//  Revision: 002 By: syw   Date: 19-Dec-1996   DR Number: DCS 1603
//      Project:  Sigma (R8027)
//      Description:
//			Call rAirFlowSerialEeprom.readSerialNumber(),
//			rO2FlowSerialEeprom.readSerialNumber(), and
//			rExhFlowSerialEeprom.readSerialNumber() only in the constructor
//			to prevent future accesses to the serial register.  The read data
//			is stored in airSerialNumberRead_, o2SerialNumberRead_, and
//			exhSerialNumberRead_.
//
//  Revision: 001  By:  syw    Date: 08-Nov-1996    DR Number: INITIAL RELEASE
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//=====================================================================

#include "BD_IO_Devices.hh"
#include "CalInfoFlashBlock.hh"
//@ Usage-Classes
#include "NetworkApp.hh"
#include "Watchdog.hh"
#include "FlashMemory.hh"
#include "Task.hh"
#include "Post.hh"

#include <memory.h>

#if defined (SIGMA_BD_CPU)

#include "FlowSensorCalInfo.hh"
#include "FlowSerialEeprom.hh"
#include "ExhValveTable.hh"
#include "CalInfoRefs.hh"
#include "CalInfoFlashSignature.hh"
#include "FlowSensorOffset.hh"
#include "ServiceMode.hh"
#include "CalibrationTable.h"
#include "NovRamManager.hh"
#include "SmRefs.hh"
#include "SmManager.hh"
#include "FlowSensor.hh"
#include "MainSensorRefs.hh"
#include "FlowSerialEeprom.hh"
#include "BackgroundMaintApp.hh"
#include "NovRamManager.hh"
#include "Task.hh"
#include "ExhCommunicationInterface.h"

#else

#include "ServiceMode.hh"

#endif  // defined (SIGMA_BD_CPU)

//@ End-Usage

#if defined (SIGMA_BD_CPU)

class BdTasks { public: static Boolean IsServiceModeBackgroundEnabled( void );
						static void SetServiceModeBackgroundEnabled( const Boolean enableState ); };

#endif  // defined (SIGMA_BD_CPU)

//@ Code...


//=====================================================================
//
//  Public Methods...
//
//=====================================================================

// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method:	CalInfoFlashBlock()
//
//@ Interface - Description
//  	Constructor.  This method has a pointer to BDIOFlashMap as an argument
//		and returns nothing.  This method will collaborate with the serial
//		register to read in the flow sensor serial numbers in the eeprom.
//-----------------------------------------------------------------------
//@ Implementation - Description
//		Initialize the data member with the argument passed in.
//
//		BD_CPU implementaion:
//		Read in the air, o2 and exh serial numbers stored in the eeprom.
//		Determine if flow sensor and exhalation valve data is valid.
//-----------------------------------------------------------------------
//@ PreCondition
//		none
//-----------------------------------------------------------------------
//@ PostCondition
//		none
//@ End - Method
//=======================================================================

CalInfoFlashBlock::CalInfoFlashBlock( BDIOFlashMap * const pFlashMap)
: pFlashMap_( pFlashMap)													// $[TI1]
{
	CALL_TRACE("CalInfoFlashBlock::CalInfoFlashBlock( BDIOFlashMap * const pFlashMap)") ;

#if defined (SIGMA_BD_CPU)

	// $[TI2]

	airSerialNumberRead_ = RAirFlowSerialEeprom.readSerialNumber() ;
	o2SerialNumberRead_  = RO2FlowSerialEeprom.readSerialNumber() ;
	ExhCommunicationInterface::getInstance()->readSerialNumber(exhSerialNumberRead_);


    // Load exhalation flow sensor cal data from persistent memory, this will either
    // be a valid cal, or if the CRC is bad, an invalid cal
    FlashImage.exhCalInfo = pFlashMap_->exhCalInfo;
    FlashImage.exhCalInfoCrc = pFlashMap_->exhCalInfoCrc ;

    // Load exhalation valve cal data from persistent memory, this will either
    // be a valid cal, or if the CRC or signature are bad, an invalid cal.
    FlashImage.exhValveCalInfoSignature = pFlashMap_->exhValveCalInfoSignature;
    FlashImage.exhValveCalInfo          = pFlashMap_->exhValveCalInfo;
    FlashImage.exhValveCalInfoCrc       = pFlashMap_->exhValveCalInfoCrc;


	isExhValveCalValid_ = determineExhValveCalValid_() ;
	isFlowSensorInfoValid_ = determineFlowSensorInfoValid_() ;

#endif  // defined (SIGMA_BD_CPU)

}


// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: ~CalInfoFlashBlock()
//
//@ Interface - Description
//		Destructor.
//-----------------------------------------------------------------------
//@ Implementation - Description
//		none
//-----------------------------------------------------------------------
//@ PreCondition
//  	none
//-----------------------------------------------------------------------
//@ PostCondition
//  	none
//@ End - Method
//=======================================================================
CalInfoFlashBlock::~CalInfoFlashBlock( void)
{
	CALL_TRACE("CalInfoFlashBlock::~CalInfoFlashBlock( void)") ;
}

// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method:  programFlashBlock
//
//@ Interface - Description
//		This method has no arguments and has no return value.  This method is
//      called to transfer the local member variable image of the BDIOFlashMap
//      to the persistent storage memory.
//-----------------------------------------------------------------------
//@ Implementation - Description
//      Copy the data.
//-----------------------------------------------------------------------
//@ PreCondition
//		none
//-----------------------------------------------------------------------
//@ PostCondition
//		none
//@ End - Method
//=======================================================================
Uint32
CalInfoFlashBlock::programFlashBlock( void)
{
    memcpy( pFlashMap_, &FlashImage, sizeof( BDIOFlashMap)) ;

    return ( 0) ;

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: buildFlowSensorData()
//
//@ Interface-Description
//	This method is used to copy flow sensor data from the serial EEPROMs
//	to FLASH.  No arguments, no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Read each flow sensor serial EEPROM and copy its data to FLASH,
//	generate FAILURE(s) if data can't be read or written.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
CalInfoFlashBlock::buildFlowSensorData( void)
{
    CALL_TRACE("CalInfoFlashBlock::buildFlowSensorData( void)");

#if defined (SIGMA_BD_CPU)
	FlowSensorOffset defaultOffset ;

	// zero out offsets in NovRam
    NovRamManager::UpdateAirFlowSensorOffset( defaultOffset) ;
    NovRamManager::UpdateO2FlowSensorOffset( defaultOffset) ;

	// Setup the flow sensor data from the serial EEPROMs
	FlowSensorCalInfo airInfo ;
	FlowSensorCalInfo o2Info ;
	//TODO E600:
	ExhSensorTableStorage_t exhInfo = {0};			

	// Read in the air cal data from the air flow sensor
	Uint32 airError = RAirFlowSerialEeprom.readFlowSensorEeprom( &airInfo, FlowSerialEeprom::AIR_SET) ;

	if (airError == TRUE)
	{
	// $[TI1]
		// Unable to read the air flow sensor data
		RSmManager.sendStatusToServiceData(
			SmStatusId::TEST_FAILURE, SmStatusId::CONDITION_1);
	}
	// $[TI2]
	// implied else: able to read air flow sensor cal data

	// Read in the O2 cal data from the O2 flow sensor
	Uint32 o2Error = RO2FlowSerialEeprom.readFlowSensorEeprom( &o2Info, FlowSerialEeprom::O2_SET) ;

	if (o2Error == TRUE)
	{
	// $[TI3]
		// Unable to read the O2 flow sensor data
		RSmManager.sendStatusToServiceData(
			SmStatusId::TEST_FAILURE, SmStatusId::CONDITION_2);
	}
	// $[TI4]
	// implied else: able to read O2 flow sensor cal data

	// Read in the serial number from the exh flow sensor
	Uint32 exhError = ExhCommunicationInterface::getInstance()->readSerialNumber(exhInfo.serialNumber_);

	if (exhError == TRUE)
	{
	// $[TI5]
		// Unable to read the exhalation flow sensor air data
		RSmManager.sendStatusToServiceData(
			SmStatusId::TEST_FAILURE, SmStatusId::CONDITION_3);
	}
	// $[TI6]
	// implied else: able to read exh air flow sensor cal data


	// done reading data, resume task
	BackgroundMaintApp::IssueNewSemphCommand( BackgroundMaintApp::TASK_RESUME) ;

	if (airError == NEW_REVISED_SENSOR || o2Error == NEW_REVISED_SENSOR ||
		exhError == NEW_REVISED_SENSOR)
	{
		// $[TI12]
		// detected new flow sensor revision, issue upgrade software message
		RSmManager.sendStatusToServiceData(
			SmStatusId::TEST_FAILURE, SmStatusId::CONDITION_6);
	}	// implied else $[TI13]

	// copy flow sensor data to FLASH image
    FlashImage.airCalInfo = airInfo ;
    FlashImage.o2CalInfo = o2Info ;

//  So, load the flash block from the stored image
	//TODO E600 save the cal data from flash incase of old sensor
	if(ExhCommunicationInterface::getInstance()->GetSensorBoardType() == EXH_SENSOR_BOARD_OLD)
		FlashImage.exhCalInfo = pFlashMap_->exhCalInfo ;
	else
		FlashImage.exhCalInfo = exhInfo ; 

	// Update the CRCs
    FlashImage.airCalInfoCrc = CalculateCrc((Byte *) &(FlashImage.airCalInfo), sizeof (FlowSensorCalInfo));

    FlashImage.o2CalInfoCrc = CalculateCrc((Byte *) &(FlashImage.o2CalInfo), sizeof (FlowSensorCalInfo));

	if(ExhCommunicationInterface::getInstance()->GetSensorBoardType() == EXH_SENSOR_BOARD_NEW)
		FlashImage.exhCalInfoCrc = CalculateCrc((Byte *) &(FlashImage.exhCalInfo), sizeof (FlashImage.exhCalInfo));
	else
		//TODO E600 save the CRC from the flash data incase of old sensor as CRC on the CAL is computed elsewhere
		// We don't want the CRC for exh flow calculated based on the stored data. It may not be a good cal.
		// The CRC should only be calculated when the calibration is performed.
		FlashImage.exhCalInfoCrc = getPFlashMap()->exhCalInfoCrc ;


	// copy flow sensor data to FLASH only if all serial EEPROM
	// reads were successful
	if (!o2Error  &&  !airError  &&  !exhError)
	{
	// $[TI9]
		// Temporarily prevent flow sensor reads while writing
		// new flow sensor data to FLASH
		ServiceMode::SetFlowSensorInfoValid( FALSE);

        programFlashBlock() ;

        ServiceMode::SetFlowSensorInfoValid( TRUE);
	}
	// $[TI10]
	// implied else
#endif
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: buildFlowSensorData()
//
//@ Interface-Description
//  This method is used to copy flow sensor data
//  to the member image of the persistent memory.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Copy its data to member variable.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
#if defined (SIGMA_BD_CPU)
void CalInfoFlashBlock::InitFlowSensorData(const FlowSensorCalInfo &airInfo,
                                           const FlowSensorCalInfo &o2Info,
										   const ExhSensorTableStorage_t &exhInfo)
{
    CALL_TRACE("CalInfoFlashBlock::buildFlowSensorData( void)");

	// copy flow sensor data to FLASH image
    FlashImage.airCalInfo = airInfo ;
    FlashImage.o2CalInfo = o2Info ;
	FlashImage.exhCalInfo = exhInfo ;


	// Update the CRCs
    FlashImage.airCalInfoCrc = CalculateCrc((Byte *) &(FlashImage.airCalInfo), sizeof (FlowSensorCalInfo));

    FlashImage.o2CalInfoCrc = CalculateCrc((Byte *) &(FlashImage.o2CalInfo), sizeof (FlowSensorCalInfo));

	FlashImage.exhCalInfoCrc = CalculateCrc((Byte *) &(FlashImage.exhCalInfo), sizeof (ExhSensorTableStorage_t));

}
#endif


// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method:  SaveExhFlowSensorData
//
//@ Interface - Description
//      This method has a new sensor cal table as a reference input.
//-----------------------------------------------------------------------
//@ Implementation - Description
//      Saves the table in the local image, updates the CRC and then
//      updates the persistent memory area
//-----------------------------------------------------------------------
//@ PreCondition
//		none
//-----------------------------------------------------------------------
//@ PostCondition
//		none
//@ End - Method
//=======================================================================
#if defined (SIGMA_BD_CPU)
void CalInfoFlashBlock::SaveExhFlowSensorData(const ExhSensorTable_t & SensorTable)
{
    // Save the sensor cal data in the local image
    FlashImage.exhCalInfo.ExhSensorTable = SensorTable;

    // Calc the CRC for the new data
    FlashImage.exhCalInfoCrc = CalculateCrc((Byte *) &(FlashImage.exhCalInfo), sizeof (FlashImage.exhCalInfo));

    // Finished updating the local image, update the persistent memory
    programFlashBlock();
}
#endif  // defined (SIGMA_BD_CPU)


// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method:  SaveExhValveData
//
//@ Interface - Description
//      This method has a new valve cal table as a reference input.
//-----------------------------------------------------------------------
//@ Implementation - Description
//      Saves the table in the local image, updates the CRC and then
//      updates the persistent memory area
//-----------------------------------------------------------------------
//@ PreCondition
//		none
//-----------------------------------------------------------------------
//@ PostCondition
//		none
//@ End - Method
//=======================================================================

#if defined (SIGMA_BD_CPU)
void CalInfoFlashBlock::SaveExhValveData(const ExhValveTable::ExhValveCalTable & ValveTable)
{
	ExhValveTable exhValveTable ( ValveTable );

    // Save the sensor cal data in the local image
    FlashImage.exhValveCalInfo = exhValveTable;

    // Calc the CRC for the new data
    FlashImage.exhValveCalInfoCrc = CalculateCrc((Byte *) &(FlashImage.exhValveCalInfo), sizeof (FlashImage.exhValveCalInfo));

	RETAILMSG( TRUE, (L"Calculated exh valve CRC: 0x%x\r\n"),  FlashImage.exhValveCalInfoCrc );

    // Set the signaure
    RCalInfoFlashBlock.FlashImage.exhValveCalInfoSignature = CalInfoFlashSignature::EXH_VALVE_SIGNATURE;

    // Finished updating the local image, update the persistent memory
    programFlashBlock();
}
#endif



// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method:  isFlashOkay
//
//@ Interface - Description
//		This method has no arguments and returns whether the data
//		stored in flash is okay.  This method provides services to the
//		background task.
//-----------------------------------------------------------------------
//@ Implementation - Description
//		Calls determineFlowSensorInfoValid_() and determineExhValveCalValid_().
//-----------------------------------------------------------------------
//@ PreCondition
//		none
//-----------------------------------------------------------------------
//@ PostCondition
//		none
//@ End - Method
//=======================================================================

Boolean
CalInfoFlashBlock::isFlashOkay( void)
{
	CALL_TRACE("CalInfoFlashBlock::isFlashOkay( void)") ;

	// $[TI1.1] return TRUE
	// $[TI1.2] return FALSE

	return( determineFlowSensorInfoValid_() && determineExhValveCalValid_()) ;
}

// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method:  initialize
//
//@ Interface - Description
//		This method has no arguments and has no return value.  This method is
//		called to initialize the FlashImage.  This method can only be called anytime
//		after rSensorMediator.newCycle() is called in BD_IO_Devices::Initialize()
//		since this method will overwrite the default flow calibration data.
//-----------------------------------------------------------------------
//@ Implementation - Description
//		Calls copyFlashToFlashImage_().
//-----------------------------------------------------------------------
//@ PreCondition
//		none
//-----------------------------------------------------------------------
//@ PostCondition
//		none
//@ End - Method
//=======================================================================

void
CalInfoFlashBlock::initialize( void)
{
	CALL_TRACE("CalInfoFlashBlock::initialize( void)") ;

	// $[TI1]
	copyFlashToFlashImage_() ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
CalInfoFlashBlock::SoftFault( const SoftFaultID  softFaultID,
							  const Uint32       lineNumber,
							  const char*        pFileName,
							  const char*        pPredicate)
{
	CALL_TRACE("CalInfoFlashBlock::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)") ;
  	FaultHandler::SoftFault( softFaultID, BD_IO_DEVICES, CALINFOFLASHBLOCK,
  							 lineNumber, pFileName, pPredicate) ;
}

//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//=====================================================================
//
//  Private Methods...
//
//=====================================================================

// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method:  determineFlowSensorInfoValid_
//
//@ Interface - Description
//		This method has no arguments and returns whether the flow sensor data
//		stored in flash is okay.
//-----------------------------------------------------------------------
//@ Implementation - Description
//		The BD_CPU retrieves the flow sensor serial	number from the serial
//		number data member.  The GUI_CPU gets the data from ServiceMode which
//		is sent to the ServiceMode class when GUI communication is established.
//		The serial number stored in flash is compared to what is stored on
//		the serial eeprom on the flow sensor.  A Crc is computed and
//		compared to what is stored in flash.  If both agree then true is
//		returned, else false.
//-----------------------------------------------------------------------
//@ PreCondition
//		none
//-----------------------------------------------------------------------
//@ PostCondition
//		none
//@ End - Method
//=======================================================================
#include "ExhCommunicationInterface.h"

Boolean
CalInfoFlashBlock::determineFlowSensorInfoValid_( void)
{
	CALL_TRACE("CalInfoFlashBlock::determineFlowSensorInfoValid_( void)") ;

#if defined (SIGMA_BD_CPU)
    isExhFlowSensorInfoValid_ = TRUE;

	if(ExhCommunicationInterface::getInstance()->GetSensorBoardType() == EXH_SENSOR_BOARD_OLD)
	{
		//TODO E600_LL: will uncomment/review the line below when new exh. flow sensor is ready
		//For E600, the calibration data can be invalid initially, so calibration table should not be checked for validity here
		//Uint32 esn = exhSerialNumberRead_ ;
		//if ((pFlashMap_->exhCalInfo).getSerialNumber() != esn ||
		//	!VerifyCrc ((Byte *) &(pFlashMap_->exhCalInfo), sizeof(pFlashMap_->exhCalInfo), pFlashMap_->exhCalInfoCrc))
		//{
		//	isExhFlowSensorInfoValid_ = FALSE;
		//}
		isExhFlowSensorInfoValid_ = TRUE;
	}
	else // (ExhCommunicationThread_t::SensorBoard == EXH_SENSOR_BOARD_NEW)
	{
		Uint32 esn = exhSerialNumberRead_ ;
		if ((pFlashMap_->exhCalInfo).getSerialNumber() != esn ||
			!VerifyCrc ((Byte *) &(pFlashMap_->exhCalInfo), sizeof(pFlashMap_->exhCalInfo), pFlashMap_->exhCalInfoCrc))
		{
			isExhFlowSensorInfoValid_ = FALSE;
		}
	}

#endif

	Boolean flowSensorInfoValid = TRUE;
#if defined (SIGMA_BD_CPU)
	isFlowSensorInfoValid_ = TRUE;

	Uint32 asn = airSerialNumberRead_ ;
	Uint32 osn = o2SerialNumberRead_ ;

	if ((pFlashMap_->airCalInfo).getSerialNumber() != asn ||

		!VerifyCrc ((Byte *) &(pFlashMap_->airCalInfo),
		sizeof (FlowSensorCalInfo),
		pFlashMap_->airCalInfoCrc) ||

		(pFlashMap_->o2CalInfo).getSerialNumber() != osn ||

		!VerifyCrc ((Byte *) &(pFlashMap_->o2CalInfo),
		sizeof (FlowSensorCalInfo),
		pFlashMap_->o2CalInfoCrc))
	{
		isFlowSensorInfoValid_ = false;
	}

	if(!isExhFlowSensorInfoValid_ || !isFlowSensorInfoValid_)
	{
		// $[TI1.1]
		flowSensorInfoValid = FALSE ;
	}	// implied else $[TI1.2]

#else

	Uint32 asn = ServiceMode::GetAirFlowSensorSN() ;
	Uint32 osn = ServiceMode::GetO2FlowSensorSN() ;
	Uint32 esn = ServiceMode::GetExhFlowSensorSN() ;

#endif  // defined (SIGMA_BD_CPU)

	return (flowSensorInfoValid);
}



// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method:  determineExhValveCalValid_
//
//@ Interface - Description
//		This method has no arguments and returns whether the exhalation
//		valve data stored in flash is okay.
//-----------------------------------------------------------------------
//@ Implementation - Description
//		A signature is compared to the one in flash.  A Crc is computed and
//		compared to what is stored in flash.  If both agree then true is
//		returned, else false.
//-----------------------------------------------------------------------
//@ PreCondition
//		none
//-----------------------------------------------------------------------
//@ PostCondition
//		none
//@ End - Method
//=======================================================================
Boolean
CalInfoFlashBlock::determineExhValveCalValid_( void)
{
	CALL_TRACE("CalInfoFlashBlock::determineExhValveCalValid_( void)") ;

	Boolean exhValveCalValid = TRUE;

	if (pFlashMap_->exhValveCalInfoSignature !=
		CalInfoFlashSignature::EXH_VALVE_SIGNATURE ||

		!VerifyCrc ((Byte *) &(pFlashMap_->exhValveCalInfo),
		sizeof (ExhValveTable),
		pFlashMap_->exhValveCalInfoCrc))
	{
		// $[TI1.1]
		exhValveCalValid = FALSE ;
	}	// implied else $[TI1.2]

	return (exhValveCalValid);
}

// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method:  determineExhFlowSensorCalValid_
//
//@ Interface - Description
//		This method has no arguments and returns whether the exhalation
//		valve data stored in flash is okay.
//-----------------------------------------------------------------------
//@ Implementation - Description
//		A signature is compared to the one in flash.  A Crc is computed and
//		compared to what is stored in flash.  If both agree then true is
//		returned, else false.
//-----------------------------------------------------------------------
//@ PreCondition
//		none
//-----------------------------------------------------------------------
//@ PostCondition
//		none
//@ End - Method
//=======================================================================

Boolean
CalInfoFlashBlock::determineExhFlowSensorCalValid_( void)
{
	CALL_TRACE("CalInfoFlashBlock::determineExhValveCalValid_( void)") ;

	Boolean exhFlowSensorCalValid = TRUE;

	if(!VerifyCrc ((Byte *) &(pFlashMap_->exhCalInfo), sizeof(pFlashMap_->exhCalInfo), pFlashMap_->exhCalInfoCrc))
	{
		exhFlowSensorCalValid = FALSE ;
	}
	return exhFlowSensorCalValid;
}

// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method:  copyFlashToFlashImage_
//
//@ Interface - Description
//		This method has no arguments and has no return value.  This method is
//      called to copy the data that is in persistent memory to the
//      local (member variable) image.
//-----------------------------------------------------------------------
//@ Implementation - Description
//		byte to byte memory copy.
//-----------------------------------------------------------------------
//@ PreCondition
//		none
//-----------------------------------------------------------------------
//@ PostCondition
//		none
//@ End - Method
//=======================================================================

void
CalInfoFlashBlock::copyFlashToFlashImage_( void)
{
	CALL_TRACE("CalInfoFlashBlock::copyFlashToFlashImage_( void)") ;

	// $[TI1]
	memcpy( &FlashImage, pFlashMap_, sizeof( BDIOFlashMap)) ;
}
