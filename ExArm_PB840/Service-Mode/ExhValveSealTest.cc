#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class:  ExhValveSealTest - Test the exhalation valve seal
//---------------------------------------------------------------------
//@ Interface-Description
//	This class is used to test the exhalation valve seal.
//
//	NOTE: the 840 Ventilator Self-Tests Spec refers to this test as the
//	"Expiratory Valve Pressure Accuracy Test", and the GUI display refers
//	to this test as the "Exp Valve Seal Test".
//---------------------------------------------------------------------
//@ Rationale
//	This class encapsulates the exhalation valve seal test.
//---------------------------------------------------------------------
//@ Implementation-Description
//	The method runTest() performs the exhalation valve seal test.
//---------------------------------------------------------------------
//@ Fault-Handling
//	n/a
//---------------------------------------------------------------------
//@ Restrictions
//	Only one instance of this class may be created.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/ExhValveSealTest.ccv   25.0.4.0   19 Nov 2013 14:23:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005  By: dosman    Date: 03-May-1999     DR Number: 5322
//  Project:  ATC
//  Description:
//	ATC initial release. 
//	Removed some debugging code that was causing compiler error.
//
//  Revision: 004  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 003  By:  quf    Date: 10-Oct-1997    DR Number: DCS 2262
//       Project:  Sigma (840)
//       Description:
//			Added comment to class-level Interface-Description indicating
//			alternate names for this test.
//
//  Revision: 002  By:  quf    Date: 23-May-1997    DR Number: DCS 2131
//       Project:  Sigma (840)
//       Description:
//            Fixed global reference names (i.e. "r" changed to "R").
//
//  Revision: 001  By:  mad    Date:  2-Nov-1994    DR Number: none
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//=====================================================================

#include "ExhValveSealTest.hh"

//@ Usage-Classes

#include "SmRefs.hh"
#include "SmFlowController.hh"
#include "SmExhValveController.hh"
#include "SmManager.hh"
#include "SmUtilities.hh"
#include "SmConstants.hh"

#include "Task.hh"

#include "ValveRefs.hh"
#include "MainSensorRefs.hh"
#include "PressureSensor.hh"
#include "ExhalationValve.hh"
#include "Psol.hh"
#include "ExhFlowSensor.h"
#include "TemperatureSensor.hh"

#ifdef SIGMA_DEVELOPMENT
#include <stdio.h>
#endif  // SIGMA_DEVELOPMENT

#ifdef SIGMA_DEBUG
// need this to read captured flow data
# include "SmTasks.hh"
#endif  // SIGMA_DEBUG

#ifdef SIGMA_UNIT_TEST
# include "FakeIo.hh"
# include "FakeControllers.hh"
#endif

//@ End-Usage

//@ Code...

//@ Constant: EV_SEAL_TEST_HIGH_FLOW
// starting (high) flow for EV seal test
const Real32 EV_SEAL_TEST_HIGH_FLOW = 5.0;  // lpm

//@ Constant: EV_SEAL_TEST_LOW_FLOW
// final (low) flow for EV seal test
const Real32 EV_SEAL_TEST_LOW_FLOW = 0.3;  // lpm

//@ Constant: EV_SEAL_TEST_PRESSURE
// Pressure used to command the EV via the calib table
const Real32 EV_SEAL_TEST_PRESSURE = 43.6;  // cmH2O

//@ Constant: EV_SEAL_TEST_ALERT_DELTA_P
// EV seal test ALERT criteria
const Real32 EV_SEAL_TEST_ALERT_DELTA_P = 2.6;  // cmH2O


//=====================================================================
//
//  Public Methods...
//
//=====================================================================

// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: ExhValveSealTest()  [Default Constructor]
//
//@ Interface - Description
//	Constructor.
//-----------------------------------------------------------------------
//@ Implementation - Description
//-----------------------------------------------------------------------
//@ PreCondition
//	none
//-----------------------------------------------------------------------
//@ PostCondition
//	none
//@ End - Method
//=======================================================================
ExhValveSealTest::ExhValveSealTest( void)
{
	CALL_TRACE("ExhValveSealTest::ExhValveSealTest( void)") ;
}


// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: ~ExhValveSealTest()  [Destructor]
//
//@ Interface - Description
//	Destructor.
//-----------------------------------------------------------------------
//@ Implementation - Description
//-----------------------------------------------------------------------
//@ PreCondition
//  none
//-----------------------------------------------------------------------
//@ PostCondition
//  none
//@ End - Method
//=======================================================================
ExhValveSealTest::~ExhValveSealTest( void)
{
	CALL_TRACE("ExhValveSealTest::~ExhValveSealTest( void)") ;
}


// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: runTest
//
//@ Interface - Description
//	This method performs the exhalation valve seal test.
//	No arguments and no return value.
//-----------------------------------------------------------------------
//@ Implementation - Description
//	$[09189]  $[09147]
//	Exit immediately with a FAILURE if EV calibration is required.
//	Check if EV motor thermistor temp is in the acceptable range.
//	Command EV and EV damping gain to the required levels.
//	Establish the initial flow, delay then read the alpha-filtered
//	exh pressure = P0.
//	Ramp flow down to the required low flow level, delay then read the
//	alpha-filtered exh pressure = P1.
//	Calculate deltaP = P0 - P1 and check if its in the acceptable range.
//	Turn off the PSOL and open the EV.
//-----------------------------------------------------------------------
//@ PreCondition
//	none
//-----------------------------------------------------------------------
//@ PostCondition
//	none
//@ End - Method
//=======================================================================
void
ExhValveSealTest::runTest( void)
{
	CALL_TRACE("ExhValveSealTest::runTest( void)") ;

	if (ServiceMode::IsExhValveCalRequired())
	{	
	// $[TI1]
		// EV calibration table is invalid
		RSmManager.sendStatusToServiceData(
			SmStatusId::TEST_FAILURE,
			SmStatusId::CONDITION_4);
	}
	else
	{
	// $[TI2]
		// first check the EV motor thermistor
		checkEvMotorThermistor_();

		RSmUtilities.autozeroPressureXducers();

		const Int16 DAMPING_GAIN_COUNT = 180;  // = 0.44 V

		// calculate the failure criteria as a function
		// of the alert criteria
		const Real32 EV_SEAL_TEST_FAILURE_DELTA_P =
			EV_SEAL_TEST_ALERT_DELTA_P +
			EV_SEAL_TEST_PRESSURE * REL_PRESSURE_ERROR +
			ABS_PRESSURE_ERROR + EV_P5_REPEATABILITY;

#ifdef SIGMA_DEBUG
		printf("ALERT deltaP = %7.3f cmH2O\n", EV_SEAL_TEST_ALERT_DELTA_P);
		printf("FAILURE deltaP = %7.3f cmH2O\n", EV_SEAL_TEST_FAILURE_DELTA_P);
#endif  // SIGMA_DEBUG

		// set exh valve command and damping gain
		RExhalationValve.updateValve( EV_SEAL_TEST_PRESSURE,
										ExhalationValve::NORMAL);
//TODO : E600 BDIO		const Int16 DAMPING_GAIN_COUNT = 180; //= 0.44 V	

// E600 BDIO		REVDampingGainDacPort.writeRegister(DAMPING_GAIN_COUNT);

#ifdef SIGMA_DEBUG
		//RSmFlowController.resetFlowStabilityParams();
		SmTasks::CaptureData = TRUE;
#endif  // SIGMA_DEBUG

		RSmFlowController.establishFlow( EV_SEAL_TEST_HIGH_FLOW,
											EXHALATION, AIR);

		if (!RSmFlowController.waitForStableFlow())
		{
		// $[TI2.1]

			// Can' establish exh flow
			RSmManager.sendStatusToServiceData(
				SmStatusId::TEST_FAILURE,
				SmStatusId::CONDITION_3);

			RSmFlowController.stopFlowController();
		}
		else
		{
		// $[TI2.2]

			// delay then get initial Pexh_filtered
			Task::Delay(0, 500);
			Real32 initialExhPressure =
							RExhPressureSensor.getFilteredValue();

			// stop flow controller but keep PSOL at its last command
			RSmFlowController.stopFlowController(TRUE);

			// get last flow controller psol command so air psol
			// can be commanded directly
			Uint16 airPsolCommand = RAirPsol.getPsolCommand();

#ifdef SIGMA_DEBUG
			printf("Seal test: air psol count before ramp = %d\n",
				airPsolCommand);
			printf("Seal test: air flow before ramp (raw) = %f lpm\n",
				RAirFlowSensor.getValue());
			printf("Seal test: air flow before ramp (filtered) = %f lpm\n",
				RAirFlowSensor.getFilteredValue());
			printf("Seal test: exh air flow before ramp (raw) = %f lpm\n",
				RExhFlowSensor.getValue());
			printf("Seal test: exh air flow before ramp (filtered) = %f lpm\n",
				RExhFlowSensor.getFilteredValue());
#endif  // SIGMA_DEBUG

			// ramp air flow down to the low flow level
			while (RExhFlowSensor.getValue() > EV_SEAL_TEST_LOW_FLOW)
			{
			// $[TI2.2.1]
				RAirPsol.updatePsol(--airPsolCommand);
				Task::Delay(0, 50);
			}
			// end while
				
#ifdef SIGMA_DEBUG
			printf("Seal test: air psol count after ramp = %d\n",
				airPsolCommand);
			printf("Seal test: air flow after ramp (raw) = %f lpm\n",
				RAirFlowSensor.getValue());
			printf("Seal test: exh air flow after ramp (raw) = %f lpm\n",
				RExhFlowSensor.getValue());
#endif  // SIGMA_DEBUG

			// delay then get final Pexh_filtered
		  	Task::Delay(5);
			Real32 finalExhPressure = RExhPressureSensor.getFilteredValue();

			// calculate delta pressure
			Real32 deltaPressure = initialExhPressure - finalExhPressure;

			RSmManager.sendTestPointToServiceData(deltaPressure);

#ifdef SIGMA_DEBUG
			printf("Seal test: initial pressure = %f cm H2O\n", initialExhPressure);
			printf("Seal test: final pressure = %f cm H2O\n", finalExhPressure);
			printf("Seal test: delta pressure = %f cm H2O\n", deltaPressure);
			printf("Seal test: final air flow (raw) = %f lpm\n",
				RAirFlowSensor.getValue());
			printf("Seal test: final air flow (filtered) = %f lpm\n",
				RAirFlowSensor.getFilteredValue());
			printf("Seal test: final exh air flow (raw) = %f lpm\n",
				RExhFlowSensor.getValue());
			printf("Seal test: final exh air flow (filtered) = %f lpm\n",
				RExhFlowSensor.getFilteredValue());
#endif  // SIGMA_DEBUG

		  	if (deltaPressure > EV_SEAL_TEST_FAILURE_DELTA_P)
		  	{	
			// $[TI2.2.2]
				// Exhalation Valve Seal FAILURE
				RSmManager.sendStatusToServiceData(
					SmStatusId::TEST_FAILURE,
					SmStatusId::CONDITION_1);
	  		}
		  	else if (deltaPressure > EV_SEAL_TEST_ALERT_DELTA_P)
		  	{	
			// $[TI2.2.3]
				// Exhalation Valve Seal ALERT
				RSmManager.sendStatusToServiceData(
					SmStatusId::TEST_ALERT,
					SmStatusId::CONDITION_5);
	  		}
			// $[TI2.2.4]
			// implied else: EV seal test passed

			RAirPsol.updatePsol(0);
		}
		// end else

		RSmUtilities.openExhValve();
	}
	// end else
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
ExhValveSealTest::SoftFault( const SoftFaultID  softFaultID,
                   		const Uint32       lineNumber,
		   				const char*        pFileName,
		   				const char*        pPredicate)
{
	CALL_TRACE("ExhValveSealTest::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  	FaultHandler::SoftFault( softFaultID, SERVICE_MODE, EXHVALVESEALTEST,
  							 lineNumber, pFileName, pPredicate) ;
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//=====================================================================
//
//  Private Methods...
//
//=====================================================================

// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: checkEvMotorThermistor_
//
//@ Interface - Description
//	This method checks if the exh valve motor thermistor is working
//	correctly.  No argument, no return value.
//-----------------------------------------------------------------------
//@ Implementation - Description
//	Read EV motor thermistor and check if temp is in acceptable range.
//-----------------------------------------------------------------------
//@ PreCondition
//	none
//-----------------------------------------------------------------------
//@ PostCondition
//	none
//@ End - Method
//=======================================================================
void
ExhValveSealTest::checkEvMotorThermistor_( void)
{
	CALL_TRACE("ExhValveSealTest::checkEvMotorThermistor_( void)") ;

	Real32 evCoilTemp = 0; // E600 BDIO RExhVoiceCoilTemperatureSensor.getValue();
	RSmManager.sendTestPointToServiceData(evCoilTemp);

	if ( evCoilTemp < MIN_EV_MAGNET_TEMP ||
		 evCoilTemp > MAX_EV_MAGNET_TEMP )
	{
	// $[TI1]
		// exh valve temp out of range
		RSmManager.sendStatusToServiceData(
			SmStatusId::TEST_FAILURE,
			SmStatusId::CONDITION_2);
	}
	// $[TI2]
	// implied else
}
