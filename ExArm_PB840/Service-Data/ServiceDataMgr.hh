#ifndef ServiceDataMgr_HH
#define ServiceDataMgr_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1994, Puritan-Bennett Corporation
//====================================================================

//=====================================================================
//@ Class:  ServiceDataMgr -  Manager of all the service data items.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Data/vcssrc/ServiceDataMgr.hhv   25.0.4.0   19 Nov 2013 14:21:04   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By: sah     Date:  15-Jan-99   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' to a non-inlined method.
//
//  Revision: 002  By: yyy      Date: 07-May-1997  DR Number: 2037
//    Project:  Sigma (R8027)
//    Description:
//      Implemented double buffering scheme guarded by a mutex to handle
//		the overlapped statuses sent by Service-Mode.
//
//  Revision: 001  By:  yyy    Date:  16-May-95   DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//====================================================================

#include "Sigma.hh"
#include "ServiceDataClassIds.hh"
#include "SmTestId.hh"
#include "SmStatusId.hh"
#include "TestDataId.hh"
#include "TestDataPacket.hh"
#include "TestDataInfo.hh"
#include "TestCommandInfo.hh"
#include "TestDatum.hh"
#include  "NetworkApp.hh"
#include  "EstTestResultData.hh"
#include  "SstTestResultData.hh"
#include  "DiagnosticCode.hh"
#include  "NovRamManager.hh"
#include  "NovRamUpdateManager.hh"

#if defined FAKE_SM
#	include "FakeServiceModeManager.hh"	
#endif	// FAKE_SM

class ServiceDataMgr
{
  public:
	typedef void (*TestItemCallbackPtr) (const TestDataId::TestDataItemId dataId);

    static void Initialize(void);

    // These methods should be resided on the BD and GUI CPU.
    // Method to send new test data to NetApp.
    static inline void NewTestData(TestDataPacket *pData, Uint size);
    // Method to send new test prompt to NetApp.
    static inline void NewPromptData(TestPromptPacket *pPrompt, Uint size);
    // Method to send new test status to NetApp.
    static inline void NewStatusData(TestStatusPacket *pStatus, Uint size);

    // Method to set test type: EST, SST, ...
    static void SetTestType(SmTestId::ServiceModeTestTypeId smType);
    // Method to signal test Id to start test.
    static void SetTestIdToStart(SmTestId::ServiceModeTestId currentTestItem);
    // Method to signal test Id to end test.
    static void SetTestIdToEnd(SmTestId::ServiceModeTestId currentTestItem);

#if defined (SIGMA_GUI_CPU) || defined (SIGMA_COMMON)    
    // Service-Data task method
	static void SDTask(void);
    // Method to receive status updated message from Persistent Objects.
	static void SDStatusEventHappened(NovRamUpdateManager::UpdateTypeId updateId);
    // Method to receive prompt updated message from Persistent Objects.
	static void SMPromptEventHappened(NovRamUpdateManager::UpdateTypeId updateId);
    // Method to receive data updated message from Persistent Objects.
	static void SMDataEventHappened(NovRamUpdateManager::UpdateTypeId updateId);
    // Method to receive new test data from NetApp.
    static void RecvTestData(XmitDataMsgId, void* pData, Uint size);
    // Method to receive new test prompt from NetApp.
    static void RecvPromptData(XmitDataMsgId, void *pData, Uint  size);
    // Method to receive new test status from NetApp.
    static void RecvStatusData(XmitDataMsgId, void *pData, Uint  size);
    // Method to download local test data to NovRam
    static void LoadNovRamTestData(void);
    // Method to download NovRam data to local test data.
    static void DownloadTestData(SmTestId::ServiceModeTestId currentTestItem);

    // Method to query the validity of test ID.
    static inline Boolean IsTestItemId(const TestDataId::TestDataItemId itemId);
    // Method to call back routines that are registered to receive 
	// individual Test Item change notification.
    static inline void MakeItemCalls(const TestDataId::TestDataItemId dataId);
    // Method to query the process agent.
	static inline TestInfo *GetDataAgent( const TestDataId::TestDataItemId itemId);
    // Method to register callback method pointer.
    static inline void RegisterTestItemCallback(TestItemCallbackPtr pCallback);


#endif  // SIGMA_GUI_CPU) || defined (SIGMA_COMMON)

    static void  SoftFault(const SoftFaultID softFaultID,
                                  const Uint32      lineNumber,
                                  const char*       pFileName  = NULL,
                                  const char*       pPredicate = NULL);

    private:				  
      ServiceDataMgr(void); 				  // not implemented
      ~ServiceDataMgr(void); 				  // not implemented
      ServiceDataMgr(const ServiceDataMgr &); // not implemented
      void operator=(const ServiceDataMgr&);  // not implemented
      
	static void SetNovRamStatus_(TestStatusPacket *pStatus);
	static void LogDiagnosticCode_(TestResultId testResultId,
									SmStatusId::TestConditionId condition);
    
#if   defined(SIGMA_GUI_CPU) || defined(SIGMA_COMMON)

    //@Data-Member: PTestInfo_[];
    // The following static array contains Test Data Processing objects
    // that has intelligence to process each specific type of Test Data Item.
    static TestInfo *PTestInfo_[TestDataId::NUM_TEST_DATA_ITEM];
    
    //@Data-Member: TestItemCallbacks_  [static]
    // The following static array contains addresses of callback routines
    // that are called whenever a new service data item is available. 
    static TestItemCallbackPtr TestItemCallbacks_[TestDataId::NUM_TEST_DATA_ITEM];

    //@Data-Member: TotalTestItemCalls_ [static]
    // Total number of callback routines that are registered to be called
    // whenever a new service data item is available.
    static Uint TotalTestItemCalls_;

    //@Data-Member: PTestResultData_
    // The object contains the pointer to the test result data.
	static TestResultData *PTestResultData_;
	
    //@Data-Member: REstTestResultData_
    // The object contains the EST Test result data.
	static EstTestResultData& REstTestResultData_;
	
    //@Data-Member: RSstTestResultData_
    // The object contains the SST Test result data.
	static SstTestResultData& RSstTestResultData_;
	
#endif //defined(SIGMA_GUI_CPU) || defined(SIGMA_COMMON)    

    //@Data-Member: CurrentTestItem_
    // The variable contains the current Test item identifier.
    static SmTestId::ServiceModeTestId CurrentTestItem_;

    //@Data-Member: SmTestId_
    // The variable contains the translated EST or SST Test Id.
	static Int SmTestId_;

    //@Data-Member: SmType_
    // The variable contains the current Test type identifier.
	static SmTestId::ServiceModeTestTypeId SmType_;
	
	//@ Data-Member:  RDiagnosticCode_
	// The SST/EST diagnostic log object used to update/manage the SST/EST
	// log and test result table.
	static DiagnosticCode &RDiagnosticCode_;

	//@ Data-Member:  StatusProcessed_
	// The flag signifies whether to the the oldest status has been processed.
	static Boolean StatusProcessed_;

	//@ Data-Member:  QueuedStatus1_
	// The overlapped status value to be processd by Service-Data queue.
	static SmStatusId::TestStatusId QueuedStatus1_;

	//@ Data-Member:  QueuedCondition1_
	// The overlapped condition value to be processd by Service-Data queue.
	static SmStatusId::TestConditionId QueuedCondition1_;

	//@ Data-Member:  QueuedStatus_
	// The status value to be processd by Service-Data queue.
	static SmStatusId::TestStatusId QueuedStatus_;

	//@ Data-Member:  QueuedCondition_
	// The condition value to be processd by Service-Data queue.
	static SmStatusId::TestConditionId QueuedCondition_;

	//@ Data-Member:  QueuedPrompt_
	// The condition value to be processd by Service-Data queue.
	static SmPromptId::PromptId QueuedPrompt_;

	//@ Data-Member:  QueuedKeysAllowed_
	// The condition value to be processd by Service-Data queue.
	static SmPromptId::KeysAllowedId QueuedKeysAllowed_;

	//@ Data-Member:  TestDataPacket_
	// The condition value to be processd by Service-Data queue.
	static TestDataPacket QueuedTestDataPacket_;

	//@ Data-Member:  ExecuteLog_
	// The flag signifies whether to log the status to NOVRam.
	static Boolean ExecuteLog_;

	//@ Data-Member:  OperatorExitRequested_
	// The flag signifies whether the operator has requested to exit the test. 
	static Boolean OperatorExitRequested_;

#if defined SIGMA_UNIT_TEST
	//@ Data-Member:  QueuedRepeatTestRequested_
	// The status value to be processd by Service-Data queue.
	static Boolean QueuedRepeatTestRequested_;
#endif	// SIGMA_UNIT_TEST
};


#include "ServiceDataMgr.in"


#endif // ServiceDataMgr_HH 
