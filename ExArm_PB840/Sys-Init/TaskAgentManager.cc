#include "stdafx.h"
//=====================================================================
//   This is a proprietary work to which Puritan-Bennett corporation
//   of California claims exclusive right.  No part of this work may
//  be used, disclosed, reproduced, sorted in an information retrieval
//     system, or transmitted by any means, electronic, mechanical,
//   photocopying, recording, or otherwise without the prior written
//      permission of Puritan-Bennett Corporation of California.
//
//          Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: TaskAgentManager - Manager of TaskAgent Objects.
//---------------------------------------------------------------------
//@ Interface-Description
//  The TaskAgentManager class manages the TaskAgent objects.  It
//  creates the TaskAgent objects and provides the interface 
//  to these objects.  It iterates through its list of TaskAgents 
//  initialized from the TaskTable to create the application tasks.
//  It also sends ChangeStateMessages to application tasks whenever
//  this processor changes state.  It forwards a BdReadyMessage/
//  GuiReadyMessage to applications tasks upon receiving the message
//  from the reporting processor.
//---------------------------------------------------------------------
//@ Rationale
//  The TaskAgentManager class manages and provides a single interface
//  to the TaskAgents.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The private method TaskAgents_ contains the static instantiations
//  of all the TaskAgent objects.  They are initialized on the first
//  pass of control through the TaskAgents_ method.  The Initialize()
//  method initializes the TaskAgents to the settings contained in 
//  the TaskTable.  The Create function, called during Task-Control
//  initialization, creates the task threads for each application task
//  defined in the TaskTable.  A collection of static functions provide 
//  Task-Control its access to the tasks for notifying the application
//  tasks of a completed state transition or for forwarding a 
//  BdReadyMessage/GuiReadyMessage when received from it peer processor.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Sys-Init/vcssrc/TaskAgentManager.ccv   25.0.4.0   19 Nov 2013 14:33:56   pvcs  $
//
//@ Modification-Log
//
//   Revision 002   By:   sah      Date: 01/13/99    DR Number: 5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to a non-inlined method.
//
//   Revision 001   By:   gdc      Date: 07/06/95    DR Number:   n/a  
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//=====================================================================

//=====================================================================
//
//      Includes...
//
//=====================================================================

#include "TaskAgentManager.hh"
#include "OsFoundation.hh"
#ifdef SIGMA_DEVELOPMENT 
# include "Ostream.hh"
#endif

//@ Usage-Classes
#include "ChangeStateMessage.hh"
#include "BdReadyMessage.hh"
#include "FsmState.hh"
#include "GuiReadyMessage.hh"
#include "InitConditions.hh"
#include "TaskAgent.hh"
#include "TaskControl.hh"
#include "TaskControlAgent.hh"
#include "Task.hh"
#include "TaskReadyMessage.hh"
//@ End-Usage

//=====================================================================
//
//      Static Data...
//
//=====================================================================

//  Set to the maximum valid task index (internally used ID) for tasks 
//  controlled by the TaskAgentManager.  Implies the number of TaskAgents
//  initialized from the TaskTable.
static Uint32  MaxTaskIndex_;

static Uint32  TransitionSequence_ = 1;

//@ Code...

//=====================================================================
//
//      Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Initialize [static]
//
//@ Interface-Description
//  This static method instantiates the 
//  TaskAgent objects which provide task control's interface to tasks
//  and task information.  Each TaskAgent object is created and set to
//  information contained in the TaskTable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Creates the static TaskAgent objects by calling TaskAgents_ the
//  first time.  Messages each TaskAgent to set its contents to the
//  task's TaskTable entry.
//---------------------------------------------------------------------
//@ PreCondition
//  Task::GetTaskCount() <= Task::MAX_NUM_TASKS
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
TaskAgentManager::Initialize(void)
{
    CALL_TRACE("Initialize(void)");
    CLASS_PRE_CONDITION(TaskInfo::GetTaskCount() <= TaskInfo::MAX_NUM_TASKS);
    
#ifdef SIGMA_DEVELOPMENT
    if (Sys_Init::GetDebugLevel(Sys_Init::ID_TASKAGENTMANAGER) >= 1)
        cout << "# Task Entries: " << Task::GetTaskCount() << endl;
#endif

    // controlled initialization of statics
    // Task ids start at 1 as a task ID of 0 has special meaning to vrtx
    // First entry in Task Table is init so it is ignored by 
    // most methods in TaskAgentManager.  However, a TaskAgent object 
    // for init is initialized here for consistency with the TaskTable.
  
    MaxTaskIndex_ = TaskInfo::GetTaskCount();

    for (Uint32 taskIdx=1; taskIdx <= MaxTaskIndex_; taskIdx++)
    {
        TaskAgents_(taskIdx).set(taskIdx);
    }
    // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: CreateTasks
//
//@ Interface-Description
//  This method iterates through the
//  TaskAgent objects, messaging each agent to create the associated
//  VRTX task.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Starts indexing the TaskAgents_ with taskIdx 2 since the init task
//  is taskIdx 1.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void TaskAgentManager::CreateTasks(void)
{
    CALL_TRACE("CreateTasks(void)");

    Int32 myPriority = Task::GetPriority();
	if(myPriority < 0)
	{
		//in case GetPriority failed, we have to restore to something..
		myPriority = TaskInfo::MIDRANGE_PRIORITY;
	}
 
    // Boost my priority (me, this thread) to system high so that none of
	// the other tasks runs while I (this thread) am creating them. Tasks
	// should start running when all of them are created.
	Task::SetPriority(TaskInfo::HIGHEST_PRIORITY);

    int   taskCount=0;
    for (Uint32 taskIdx=2; taskIdx<=MaxTaskIndex_; taskIdx++)
    {
        TaskAgents_(taskIdx).createTask();
        taskCount++;

#if defined( SIGMA_GUIPC_CPU )
		// Some lower priority task depends on high priority task, since
		// Win32 is not a real-time operating system, it cannot provides
		// protection of initialization order of tasks.
		// So we delay some time for WIN32, to make sure high priority 
		// tasks are initialized first.

        // TODO E600: This is not a strong guarantee, remove after task 
        //       initialization order is protected by other mechnisim.
		Task::Delay( 0, 200 );
#endif

    }

    //  Restore base priority
	Task::SetPriority(myPriority);

    // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ReportNodeReady
//
//@ Interface-Description
//  This method iterates through the TaskAgent objects, messaging each
//  TaskAgent to send a GuiReady/BdReady message to the associated task.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
TaskAgentManager::ReportNodeReady(void)
{
    CALL_TRACE("ReportNodeReady(void)");
#ifdef SIGMA_BD_CPU
    BdReadyMessage rReadyMessage(  TaskControlAgent::GetBdState()
                                 , InitConditions::IsServiceRequired());
#endif
#ifdef SIGMA_GUI_CPU
    GuiReadyMessage rReadyMessage(  TaskControlAgent::GetGuiState()
                                  , InitConditions::IsServiceRequired());
#endif

    for (Uint32 taskIdx=2; taskIdx <= MaxTaskIndex_; taskIdx++)
    {
        TaskAgents_(taskIdx).send(rReadyMessage);
    }
    // $[TI1]
}
 

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ChangeState
//
//@ Interface-Description
//  This method iterates through the TaskAgent objects, messaging each
//  TaskAgent to change state to the target state. 
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
TaskAgentManager::ChangeState(void)
{
    CALL_TRACE("ChangeState(void)");

#ifdef SIGMA_GUI_CPU
	TRACE1("TaskAgentManager::ChangeState. Target State: 0x%x\n", 
		FsmState::GetTargetState().getSigmaState() );
#endif
    ChangeStateMessage rChangeState(  
                           FsmState::GetTargetState().getSigmaState()
                         , NULL
                         , ++TransitionSequence_ );

    for (Uint32 taskIdx=2; taskIdx <= MaxTaskIndex_; taskIdx++)
    {
        TaskAgents_(taskIdx).send(rChangeState);
    }
    // $[TI1]
}
 

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Send
//
//@ Interface-Description
//  This method iterates through the TaskAgent objects, messaging each
//  TaskAgent to forward a copy of the referenced BdReadyMessage to the
//  managed task.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Starts with  taskIdx 2 since taskIdx 1 is the init task.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
TaskAgentManager::Send(const BdReadyMessage& rMessage)
{
    CALL_TRACE("Send(const BdReadyMessage&)");
    for (Uint32 taskIdx=2; taskIdx <= MaxTaskIndex_; taskIdx++)
    {
        TaskAgents_(taskIdx).send(rMessage);
    }
    // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Send
//
//@ Interface-Description
//  This method iterates through 
//  the TaskAgent objects, messaging each TaskAgent to forward a copy of 
//  the referenced GuiReadyMessage to the managed task.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Starts with  taskIdx 2 since taskIdx 1 is the init task.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
TaskAgentManager::Send(const GuiReadyMessage& rMessage)
{
    CALL_TRACE("Send(const GuiReadyMessage&)");
    for (Uint32 taskIdx=2; taskIdx <= MaxTaskIndex_; taskIdx++)
    {
        TaskAgents_(taskIdx).send(rMessage);
    }
    // $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Send
//
//@ Interface-Description
//  This method iterates through 
//  the TaskAgent objects, messaging each TaskAgent to forward a copy of 
//  the referenced CommDownMessage to the managed task.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Starts with  taskIdx 2 since taskIdx 1 is the init task.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
TaskAgentManager::Send(const CommDownMessage& rMessage)
{
    CALL_TRACE("Send(const CommDownMessage&)");
    for (Uint32 taskIdx=2; taskIdx <= MaxTaskIndex_; taskIdx++)
    {
        TaskAgents_(taskIdx).send(rMessage);
    }
    // $[TI3]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Receive
//
//@ Interface-Description
//  This method receives a TaskReadyMessage from a "managed" task.  The
//  task responds to a ChangeStateMessage by returning a TaskReadyMessage
//  when it completes the requested state transition.  This method 
//  forwards the message to the TaskAgent of the responding task 
//  which updates the transition status of the task.  If all tasks
//  have completed the latest transition sequence this method will
//  message the finite state machine with the final TaskReadyMessage
//  to complete the transition sequence.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
TaskAgentManager::Receive(const TaskReadyMessage& rMessage)
{
    CALL_TRACE("Receive(const TaskReadyMessage&)");

    TaskAgent * const pTaskAgent = (TaskAgent*)rMessage.getPTaskAgent();

    pTaskAgent->receive(rMessage);

	Uint32 taskIdx;
    for (taskIdx=2; taskIdx <= MaxTaskIndex_; taskIdx++)
    {
       if (!TaskAgents_(taskIdx).isTransitioned(TransitionSequence_))
       {            // $[TI1]
           break;
       }            // $[TI2]
    }
    
    //  if all tasks have transitioned, this is the final TaskReadyMessage
    //  so we send it on to the finite state machine for processing
    if (taskIdx > MaxTaskIndex_)   // $[TI3]
    {
        FsmState::GetCurrentState().receiveTaskReady(rMessage);
    }
    // $[TI4]

}

 

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//
//@ Interface-Description
//   This method is called when a software fault is detected by the
//   fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//   'lineNumber' are essential pieces of information.  The 'pFileName'
//   and 'pPredicate' strings may be defaulted in the macro to reduce
//   code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This method receives the call for the SoftFault, adds it sub-system
//   and class name ID and sends the message to the non-member function
//   SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
void
TaskAgentManager::SoftFault(const SoftFaultID  softFaultID,
                            const Uint32       lineNumber,
                            const char*        pFileName,
                            const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
 
  FaultHandler::SoftFault(softFaultID, SYS_INIT,
                          Sys_Init::ID_TASKAGENTMANAGER, 
                          lineNumber, pFileName, pPredicate);
}


//=====================================================================
//
//      Protected Methods...
//
//=====================================================================

//=====================================================================
//
//      Private Methods...
//
//=====================================================================



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TaskAgents_
//
//@ Interface-Description
//  TaskAgents_ contains the function static for all TaskAgentManager
//  TaskAgent instantiations.  On first call this function initializes
//  all the static TaskAgents and returns a reference to the requested
//  TaskAgent object.  On subsequent calls, this method only returns
//  a reference to the TaskAgent requested by the taskIdx argument.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns the TaskAgent reference indexed by the taskIdx argument.
//  Index 0 in the agents array is taskIdx 1 since taskIdx 0 has special
//  meaning to VRTX.
//---------------------------------------------------------------------
//@ PreCondition
//  0 < taskIdx && taskIdx <= MaxTaskIndex_
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================


TaskAgent&
TaskAgentManager::TaskAgents_(Int32 taskIdx)
{
    CALL_TRACE("TaskAgents_(Int32)");

    //  After the first call which
    //  initializes the static table, a TaskAgent reference is 
    //  returned based on the index argument.  The TaskAgent reference
    //  can then be used as an L-value or R-value.

    static TaskAgent agents[TaskInfo::MAX_NUM_TASKS];

    CLASS_PRE_CONDITION(0 < taskIdx && taskIdx <= MaxTaskIndex_);

    //  index 0 == taskIdx 1
    return agents[taskIdx-1];
    // $[TI1]
}

#ifdef SIGMA_UNIT_TEST

//============================ M E T H O D   D E S C R I P T I O N ====
//. Method: GetMaxTaskId - UNIT TEST ONLY
//
//. Interface-Description
//  Returns value of private MaxTaskIndex_ for unit test purposes.
//---------------------------------------------------------------------
//. Implementation-Description
//---------------------------------------------------------------------
//. PreCondition
//  none
//---------------------------------------------------------------------
//. PostCondition
//  none
//. End-Method
//=====================================================================

Uint32
TaskAgentManager::GetMaxTaskId(void)
{
    CALL_TRACE("GetMaxTaskId(void)");

    return MaxTaskIndex_;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//. Method: GetTransitionSequence - UNIT TEST ONLY
//
//. Interface-Description
//  Returns value of private TransitionSequence_ for unit test purposes.
//---------------------------------------------------------------------
//. Implementation-Description
//---------------------------------------------------------------------
//. PreCondition
//  none
//---------------------------------------------------------------------
//. PostCondition
//  none
//. End-Method
//=====================================================================

Uint32
TaskAgentManager::GetTransitionSequence(void)
{
    CALL_TRACE("GetTransitionSequence(void)");

    return TransitionSequence_;
}

#endif  // SIGMA_UNIT_TEST
