#ifndef FocusButton_HH
#define FocusButton_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
//@ Class: FocusButton - A button which displays a piece of text in its
// interior (in the button's Label container) and takes the current 
// persistant or non-persistent focus.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/FocusButton.hhv   25.0.4.0   19 Nov 2013 14:07:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By: rhj    Date: 06-Jun-2007    SCR Number: 6237
//  Project:  Trend
//  Description:
//      Initial version.
//====================================================================

#include "TextButton.hh"
#include "AdjustPanel.hh"
#include "AdjustPanelTarget.hh"

//@ Usage-Classes
#include "GuiAppClassIds.hh"
#include "SubScreen.hh"
#include "FocusButtonTarget.hh"
//@ End-Usage

class FocusButton : public TextButton, public AdjustPanelTarget
{
public:
	FocusButton(Uint16 xOrg, Uint16 yOrg, Uint16 width, Uint16 height,
				ButtonType buttonType, Uint8 bevelSize, CornerType cornerType,
				BorderType borderType, StringId title,
				StringId auxTitleText = ::NULL_STRING_ID,
				Gravity  auxTitlePos  = ::GRAVITY_RIGHT,
				Button::PushButtonType pushButtonType = Button::PB_TOGGLE);
	~FocusButton(void);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

	// AdjustPanelTarget virtual methods
	virtual void adjustPanelLoseFocusHappened(void);
	virtual void adjustPanelKnobDeltaHappened(Int32 delta );
	virtual void adjustPanelAcceptPressHappened(void);
	virtual void adjustPanelClearPressHappened(void);

	inline void usePersistentFocus(void);

	inline void setFocusButtonCallback(FocusButtonTarget* pFocusButtonTarget);

protected:

	// Button virtual methods
	virtual void downHappened_(Boolean byOperatorAction);
	virtual void upHappened_(Boolean byOperatorAction);


private:
	// these methods are purposely declared, but not implemented...
	FocusButton(const FocusButton&);		// not implemented...
	void   operator=(const FocusButton&);	// not implemented...

	//@ Data-Member: losingFocus_
	// Data member which tells us if the button is losing focus.
	// Needed to distinguish the reason for the upHappened_() method being 
	// called.
	Boolean         losingFocus_;

	//@ Data-Member: isPressedDown_
	// Data member which tells us if the button has been pressed down.
	// Used by upHappened_() to determine if the button had been 
	// pressed/set down prior to being pressed/set up.
	Boolean         isPressedDown_;

	//@ Data-Member: usePersisentFocus_
	// A flag indicating whether this button should grab the normal or
	// the persistent Adjust Panel focus.  Focus buttons in the lower
	// screen use the normal focus, upper screen focus buttons use the
	// persistent.  This is necessary to insulate focus clear events in
	// one screen from affecting the other.
	Boolean         usePersistentFocus_;

	//@ Data-Member: pFocusButtonTarget_
	// Callback object which is notified when this button's
	// knob changes.
	FocusButtonTarget* pFocusButtonTarget_;

};

// Inlined methods
#include "FocusButton.in"

#endif // FocusButton_HH 
