#ifndef TemperatureSensor_HH
#define TemperatureSensor_HH
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: TemperatureSensor - Defines the implementation for all
// 		temperature sensors.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/TemperatureSensor.hhv   25.0.4.0   19 Nov 2013 13:54:14   pvcs  $
//
//@ Modification-Log
//
//  Revision: 011   By:   rhj    Date: 05-March-2007      DR Number: 6359
//  Project:  RESPM
//  Description:
//        Resolve merge conflict for RM to Baseline 
//
//  Revision: 010  By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to non-inlined method.
//
//	Revision: 009  By:  syw    Date:  28-Jul-1997    DR Number: DCS 2327
//      Project:  Sigma (R8027)
//      Description:
//			Added Alpha_ data member and set method.
//
//  Revision: 008 By: syw    Date: 07-Jul-1997   DR Number: 2283
//  	Project:  Sigma (R8027)
//			Define updateValue() on BD_CPU to process inverse alpha filters,
//			tau_ and predictedTemperature_ for inverse alpha calculations.
//			Added tempOffset_ data member and methods.
//
//  Revision: 007 By: syw    Date: 03-Mar-1997   DR Number: DCS 1780, 1827
//  	Project:  Sigma (R8027)
//			Eliminate modified temperature.  Delete updateValue() on BD_CPU.
//
//  Revision: 006 By: syw    Date: 25-Feb-1997   DR Number: DCS 1827
//  	Project:  Sigma (R8027)
//			Temperature sensors are	computed as before except for that the
//			exhalation flow temperature sensors which uses a fixed
//			temperature that is obtained near the end of exhalation.
//
//  Revision: 005 By: syw    Date: 02-May-1996   DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Added BD_CPU macros.  Added constructor and getValue() method
//			for GUI CPU.
//
//  Revision: 004 By: syw    Date: 02-May-1996   DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Added updateValue() with counts as and argument for TUV support.
//
//  Revision: 003  By: syw    Date: 20-Feb-1996   DR Number: DCS 674
//  	Project:  Sigma (R8027)
//		Description:
//			Removed #ifndef around #include.
//
//  Revision: 002  By: syw    Date: 05-Dec-1995   DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Added interface in constructor for SafetyNetSensorData *.
//
//	Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: INITIAL RELEASE
//      Project:  Sigma (R8027)
//      Description:
//      	Initial version
//
//=====================================================================
// TODO E600_LL:  TemperatureSensor is not currently used; will need to be reviewed
#include "Sigma.hh"
#include "BD_IO_Devices.hh"

//@ Usage-Classes

#include "Sensor.hh"

//@ End-Usage


//@ Constant: NOMINAL_ALPHA
// nominal alpha to use in low pass filter
static const Real32 NOMINAL_ALPHA = 0.86 ;

//@ Constant: TRIGGER_ALPHA
// alpha to use when cleaner temp signals are desired
static const Real32 TRIGGER_ALPHA = 0.96 ;


//@ Data-Member : FlowSensorThermistorTable[]
// A fixed table shared by all flow temperature sensors
extern const Real32 FlowSensorThermistorTable[] ;
    
//@ Data-Member : ExhHeaterCoilThermistorTable[]
// A fixed table shared by exhalation valve and exhalation heater temperature sensors
extern const Real32 ExhHeaterCoilThermistorTable[] ;

class TemperatureSensor: public Sensor 
{
  public:

#if defined (SIGMA_BD_CPU)

    TemperatureSensor( SafetyNetSensorData *pData,
    				   const AdcChannels::AdcChannelId id,
    				   const Real32* pThermistorTable) ;
#else

    TemperatureSensor( const Real32* pThermistorTable) ;
    				   
#endif // defined (SIGMA_BD_CPU)
    				   
    virtual ~TemperatureSensor( void) ;
    static void SoftFault( const SoftFaultID softFaultID,
						   const Uint32      lineNumber,
						   const char*       pFileName  = NULL, 
						   const char*       pPredicate = NULL) ;

	inline void setTempOffset( const Real32 offset) ;

#if defined (SIGMA_GUI_CPU)

    inline Real32 getValue( void) const ;
	void updateValue( const AdcCounts count) ;

#else

	virtual void updateValue( void) ;
	inline Real32 getPredictedTemperature( void) const ;
	inline static void SetAlpha( const Real32 alpha) ;
	void calculateTau( const Real32 flow) ;
	void setTauCoeffs( const Real32 a, const Real32 b, const Real32 c, const Real32 d) ;
	inline void enableFilter( void)	;
    inline void disableFilter( void) ;

#endif // defined (SIGMA_GUI_CPU)
	
  protected:

    virtual Real32 rawToEngValue_( const AdcCounts count) const ;

  private:
    // these methods are purposely declared, but not implemented...
    TemperatureSensor( void) ;    					// default Constructor
    TemperatureSensor( const TemperatureSensor&) ; 	// not implemented
    void operator=( const TemperatureSensor&) ;    	// not implemented

    //@ Data-Member : pVoltTemperatureTable_
    // Pointer to VoltTemperatureTable1 or VoltTemperatureTable2
    const Real32* pVoltTemperatureTable_ ;

#if defined (SIGMA_BD_CPU)

	//@ Data-Member: tau_
	// the time response of the temperature sensor for a specified flow
	Real32 tau_ ;

	//@ Data-Member: predictedTemperature_
	// the output of the inverse alpha filter and alpha filter
	Real32 predictedTemperature_ ;

	//@ Data-Member: Real32 Alpha_
	// alpha to aplly to the output of the inverse alpha filter
	static Real32 Alpha_ ;

	//@ Data-Member: a_
	// a coefficient in tau curve fit
	Real32 a_ ;

	//@ Data-Member: b_
	// b coefficient in tau curve fit
	Real32 b_ ;

	//@ Data-Member: c_
	// c coefficient in tau curve fit
	Real32 c_ ;

	//@ Data-Member: d_
	// d coefficient in tau curve fit
	Real32 d_ ;

	//@ Data-Member: filterEnabled_
    // if FALSE, filtering turned off
    Boolean filterEnabled_ ;
			
#endif // defined (SIGMA_BD_CPU)

	//@ Data-Member: tempOffset_
	// temperature offset added to temperature signal
	Real32 tempOffset_ ;
} ;

// Inlined methods
#include "TemperatureSensor.in"

#endif // TemperatureSensor_HH
 
