#if (defined IMAGE_CFG_COMMENT)
// All rights reserved ADENEO EMBEDDED 2010
/*
================================================================================
*             Texas Instruments OMAP(TM) Platform Software
* (c) Copyright Texas Instruments, Incorporated. All Rights Reserved.
*
* Use of this software is controlled by the terms and conditions found
* in the license agreement under which this software has been supplied.
*
================================================================================
*/
//
//  File:  image_cfg.h
//
// This file is also included in the config.bib. It's thereof very important that 
// the preprocessor statements are limited to those understood by romimage.exe
// For example don't use #ifndef  #endif but use #ifdef #else #endif, and keep the #define on 1 line only

// IMAGE_CFG_COMMENT is just a dummy flag to make config.bib and platform.reg happy.  Do not expect this flag to be
// ever defined

#endif

#if (defined IMAGE_CFG_COMMENT)
//------------------------------------------------------------------------------
//  EVM board 128 MB of SDRAM located physically at 0x80000000
//
#endif
#define DEVICE_RAM_PA                   0x80000000
#define DEVICE_RAM_CA                   0x80000000

#if (defined BSP_SDRAM_BANK1_ENABLE)
#define DEVICE_RAM_SIZE                 0x10000000
#else
#define DEVICE_RAM_SIZE                 0x08000000
#endif

#if (defined IMAGE_CFG_COMMENT)
//------------------------------------------------------------------------------
//
//  Define: IMAGE_SHARE_ARGS
//
//  Following constants define location and maximal size of arguments shared
//  between loader and kernel. For actual structure see args.h file.
//  
#endif
#define IMAGE_SHARE_ARGS_CA             0x80000000
#define IMAGE_SHARE_ARGS_SIZE           0x00001000

#if (defined IMAGE_CFG_COMMENT)
//------------------------------------------------------------------------------
//
//  Define: CPU_INFO_ADDR
//
//  Following constants define location and maximal size of arguments shared
//  between loader and kernel. For actual structure see args.h file.
//  
#endif
#define CPU_INFO_ADDR_PA                0x80001000
#define CPU_INFO_ADDR_CA                0x80001000
#define CPU_INFO_ADDR_SIZE              0x00001000

#if (defined IMAGE_CFG_COMMENT)
//------------------------------------------------------------------------------
//
//  Define: IMAGE_WINCE_CODE
//
//  Following constants define Windows CE OS image layout.
//
#endif
#if (defined BSP_SDRAM_BANK1_ENABLE)
#define IMAGE_WINCE_CODE_CA             0x88000000   //
#define IMAGE_WINCE_CODE_SIZE           0x047FF000   // CODE     72 MB

#define IMAGE_WINCE_RAM_CA              0x8C7FE000   //
#define IMAGE_WINCE_RAM_SIZE            0x03801000   // RAM      56 MB

#define HEX_VALUE(a)      0x##a
#define IMAGE_WINCE_RAM_DISK_CA_HEX     80002000   //
#define IMAGE_WINCE_RAM_DISK_SIZE_HEX   27FE000   // RAMDISK 40 MB

#define IMAGE_WINCE_RAM_DISK_CA         HEX_VALUE(IMAGE_WINCE_RAM_DISK_CA_HEX)   //
#define IMAGE_WINCE_RAM_DISK_SIZE       HEX_VALUE(IMAGE_WINCE_RAM_DISK_SIZE_HEX)   // RAMDISK 48 MB

#define IMAGE_WINCE_DISPLAY_CA          0x82800000   //
#define IMAGE_WINCE_DISPLAY_SIZE        0x01000000   // Display    16 MB

#define IMAGE_WINCE_DISPLAY_720p_CA     0x83800000   //
#define IMAGE_WINCE_DISPLAY_720p_SIZE   0x01000000   // Display    16 MB - extra needed for 720p video playback; should be contiguous with IMAGE_WINCE_DISPLAY_CA

#define IMAGE_CMEM_CA					0x84800000   //CMEM                      ; this is always needed if DVSDK is enabled
#define IMAGE_CMEM_SIZE				    0x01000000	//16 MB

#define IMAGE_DSP_720P_CA				0x85800000   //DSP (2)
#define IMAGE_DSP_720P_SIZE			    0x01A00000	//26MB                      ; extra DSP region if using 720p codecs and SDRAM bank1

#define IMAGE_DSP_CA					0x87200000   //DSP		
#define IMAGE_DSP_SIZE					0x00E00000	// 14MB                     ; this is always used if DVSDK is enabled


#else
/* for this config you will have to disable DSP 720p support and RAMDisk. 
    Also make sure your NK is smaller than 60MB, else you will have to disable DSP+CMEM too.
*/    
#define IMAGE_WINCE_CODE_CA             0x80002000   //
#define IMAGE_WINCE_CODE_SIZE           0x01FFE000   // CODE     32 MB

#define IMAGE_WINCE_RAM_CA              0x82000000   //
#define IMAGE_WINCE_RAM_SIZE            0x02800000   // RAM      40 MB

#define IMAGE_WINCE_RAM_BANK1_CA        0x88000000   //
#define IMAGE_WINCE_RAM_BANK1_SIZE      0x08000000   // RAM (2nd BANK) 128 MB

#define IMAGE_CMEM_CA					0x84800000   //CMEM                      ; this is always needed if DVSDK is enabled
#define IMAGE_CMEM_SIZE				    0x01000000	//16 MB

#define IMAGE_WINCE_DISPLAY_CA          0x85800000   //
#define IMAGE_WINCE_DISPLAY_SIZE        0x01A00000   // VIDEO    16 MB

#define IMAGE_DSP_CA					0x87200000   //DSP		
#define IMAGE_DSP_SIZE					0x00E00000	// 14MB                     ; this is always used if DVSDK is enabled

#endif


#define IMAGE_WINCE_NOR_OFFSET         0x00060000
#define NAND_ROMOFFSET                 0x40000000
#define NOR_ROMOFFSET                  0x60000000

#if (defined IMAGE_CFG_COMMENT)
//------------------------------------------------------------------------------
//
//  Define: IMAGE_XLDR_xxx
//
//  Following constants define image layout for X-Loader. used to download/program the XLDR
//  XLDR executes from SRAM. (MMU is not used in XLDR, so the address are physical here)
// NOTE : This is for flash XLDR. (XLDR SD is another matter)
#endif
#define IMAGE_XLDR_CODE_PA              0x40200000
#define IMAGE_XLDR_CODE_SIZE            0x0000C000

#define IMAGE_XLDR_NOR_OFFSET           (0x00000000)

#if (defined IMAGE_CFG_COMMENT)
//------------------------------------------------------------------------------
//
//  Define: IMAGE_EBOOT_xxx
//
//  Following constants define EBOOT image layout. 
//
#endif
#define IMAGE_EBOOT_CODE_CA             0x87E00000
#define IMAGE_EBOOT_CODE_SIZE           0x00040000

#define IMAGE_EBOOT_DATA_CA             0x87E80000
#define IMAGE_EBOOT_DATA_SIZE           0x00050000

#define IMAGE_EBOOT_NOR_OFFSET          0x00020000
#define IMAGE_EBOOT_CFG_NOR_OFFSET      0x00008000

#if (defined IMAGE_CFG_COMMENT)
//------------------------------------------------------------------------------
//
//  Define: IMAGE_STARTUP_xxx
//
//  Jump address XLDR uses to bring-up the device.
//
#endif
#define IMAGE_STARTUP_IMAGE_PA         (IMAGE_EBOOT_CODE_CA - DEVICE_RAM_CA + DEVICE_RAM_PA)
#define IMAGE_STARTUP_IMAGE_CA         (IMAGE_EBOOT_CODE_CA)
#define IMAGE_STARTUP_IMAGE_SIZE       (IMAGE_EBOOT_CODE_SIZE)


#if (defined IMAGE_CFG_COMMENT)
//------------------------------------------------------------------------------
//
//  Define: IMAGE_BOOTLOADER_xxx
//
//  Following constants define bootloader information
//
#endif
#define IMAGE_XLDR_BOOTSEC_NAND_SIZE		(4 * 128 * 1024)        // Needs to be equal to four NAND flash blocks due to boot ROM requirements
#define IMAGE_EBOOT_BOOTSEC_NAND_SIZE		IMAGE_EBOOT_CODE_SIZE   // Needs to be a multiple of flash block size

#define IMAGE_BOOTLOADER_BITMAP_SIZE		0x00100000                  // Needs to be a multiple of 128k, and minimum 480x640x3 (VGA)  

#define IMAGE_BOOTLOADER_NAND_SIZE			(IMAGE_XLDR_BOOTSEC_NAND_SIZE + IMAGE_EBOOT_BOOTSEC_NAND_SIZE + IMAGE_BOOTLOADER_BITMAP_SIZE)

#if (defined IMAGE_CFG_COMMENT)
//Allocate 60M for NK.bin in NAND
#endif
#define IMAGE_NK_NAND_SIZE                            (0x5000000)

