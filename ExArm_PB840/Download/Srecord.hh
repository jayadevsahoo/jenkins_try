#ifndef  Srecord_HH
#define	Srecord_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Nellcor/Puritan-Bennett Corporation of California.
//
//    Copyright (c) 1996-1997, Nellcor/Puritan-Bennett Corporation
//=====================================================================

//====================================================================
//@ Filename: Srecord - Class definition of the Motorola S record 
//  object format file parser.
//---------------------------------------------------------------------
//@ Version
//@(#) $Header:   /840/Baseline/Download/vcssrc/Srecord.hhv   25.0.4.0   19 Nov 2013 14:02:50   pvcs  $ 
//@ Modification-Log
//
//  Revision: 003  By: sah    Date: 15-Jan-99    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 002  By: Gary Cederquist  Date: 28-JUL-1997  DR Number: 2197
//       Project:  Sigma (R8027)
//       Description:
//             Changed to use SigmaTypes.hh
//
//  Revision: 001  By:  <jhv>    Date:  27-Feb-96    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//====================================================================

#include "DownloadTypes.hh"
#include "DownloadError.hh"
#include "DownloadCons.hh"
#include "DownloadClassIds.hh"
#include "DownloadFault.hh"

enum SrecordCons
{
	MAX_SRECORD_DATA_LENGTH = 120,
	SRECORD_ADDR_LEN = 4,
	SRECORD_BYTECOUNT_LEN= 1,
	SRECORD_CHECKSUM_LEN = 1,
	MAX_SRECORD_SIZE = 250
};

enum SrecordState
{
	S_SYNC,		// waiting for the 'S' in 'S-Record'
	S_GET_TYPE,	// determine what type of record this is
	S_GET_COUNT_HI,	// Get the record byte count
	S_GET_COUNT_LO,
	S_GET_ADDR,	// Get the destination address
	S_GET_DATA,	// Get the record data
	S_GET_CKSUM,	// Check the checksum
	S_EXIT,		// return an exit indication to caller
	S_ERROR		// This is the catch-all error state
};

enum SrecordType
{
	SREC1 = 1,
	SREC2 = 2,
	SREC3 = 3,
	SREC4 = 4,
	SREC5 = 5,
	SREC6 = 6,
	SREC7 = 7
};

class BuildFlash;

class Srecord 
{
  public:
    Srecord();
    ~Srecord(void);
	void Initialize(void);
	void initStates();
	inline Boolean endOfSfile();
	inline void setBuilder(BuildFlash *pBuilder);
	Int buildEofRecord(Byte *pRecord);
	Int	 buildDataRecord(Byte *pTargetAddr, Byte *pData,
			Byte *pRecord, Int dataSize);
	SigmaStatus parse(Byte c);
    static void SoftFault(const FaultType softFaultID,
		const Uint32      lineNumber,
		const char*       pFileName  = NULL, 
		const char*       pPredicate = NULL);

  protected:

  private:
    Srecord(const Srecord&);			// not implemented...
    void   operator=(const Srecord&);	// not implemented...

	//@Data-Member: AsciiConvTbl_[16]
	// a hexa-decimal number to an ascii conversion.
		static Byte  AsciiConvTbl_[16];

	//@Data-Member: pImageBuilder_
	// Flash memory image builder object.
		BuildFlash *pImageBuilder_;

	//@Data-Member: exitSrecord_
	// Indicates whether the current S record has encountered
	// an of S record file record.
		Boolean exitSrecord_;

	//@Data-Member: state_
	// the present state of the Srecord parser state machine.
		Int state_;

	//@Data-Member: type_
	// record type of the current Srecord
	Int type_;		

	//@Data-Member: count_
	// number of bytes in the current Srecord
	Int count_;		

	// checksum in progress for the current Srecord.
	//@Data-Member: cksum_
	Uint cksum_;	

	//@Data-Member: addlen_
	// 4, 6, or 8 hex digit address size
	Int addlen_;		

	//@Data-Member: address_
	// destination address of the current Srecord data
	Uint address_;	

	//@Data-Member: data_
	// accumulated data of the current Srecord
	Int data_;		

	//@Data-Member: addrlen_
	// keeps track of remaining size of the address bytes
	Int addrlen_;		

	//@Data-Member: flop_
	// flip-flop byte toggle
	Int flop_;		

	//@Data-Member: isNumber_
	//  indicator to determine whether the current Srecord
	//  data is a numeric number or not
	Boolean isNumber_;		
};

// Inlined methods...
#include "Srecord.in"

#endif // Srecord_HH 
