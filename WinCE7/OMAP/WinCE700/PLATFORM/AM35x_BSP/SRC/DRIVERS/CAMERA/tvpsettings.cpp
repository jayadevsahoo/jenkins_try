
#include <windows.h>
#include "tvp5146.h"

TVP_SETTINGS tvpSettings[]={\
        {REG_INPUT_SEL, 0x00},//Composite path
        {REG_AFE_GAIN_CTRL, 0x0F},//default
        {REG_VIDEO_STD, 0x00},//default: auto mode
        {REG_OPERATION_MODE, 0x00},//normal mode
        {REG_AUTOSWITCH_MASK, 0x3F},//autoswitch mask: enable all
        {REG_COLOR_KILLER, 0x10},//default
        {REG_LUMA_CONTROL1, 0x00},//default
        {REG_LUMA_CONTROL2, 0x00},//default
        {REG_LUMA_CONTROL3, 0x00},//optimize for NTSC/ PAL 
        {REG_BRIGHTNESS, 0x80},//default
        {REG_CONTRAST, 0x80},//default
        {REG_SATURATION, 0x80},//default
        {REG_HUE, 0x00},//default
        {REG_CHROMA_CONTROL1, 0x00},//default
        {REG_CHROMA_CONTROL2, 0x04},//default
        {REG_COMP_PR_SATURATION, 0x80},//default
        {REG_COMP_Y_CONTRAST, 0x80},//default
        {REG_COMP_PB_SATURATION, 0x80},//default
        {REG_COMP_Y_BRIGHTNESS, 0x80},//default             
		{REG_OUTPUT_FORMATTER1, 0x00},//ITU601 coding range, 10bit 4:2:2 embedded sync
        {REG_OUTPUT_FORMATTER2, 0x11},//Enable clk & y[9:0],c[9:0] active, DATACLK OUT "rising edge "
        {REG_OUTPUT_FORMATTER3, 0x02},//Set FID as FID output
        {REG_OUTPUT_FORMATTER4, 0xAF},//default: HS, VS is output, C1~C0 is input 
        {REG_OUTPUT_FORMATTER5, 0xFF},//default: C5~C2 is input 
        {REG_OUTPUT_FORMATTER6, 0xFF},//default: C9~C6 is input                
};

UINT num_tvp_settings = (sizeof(tvpSettings)/sizeof(TVP_SETTINGS));