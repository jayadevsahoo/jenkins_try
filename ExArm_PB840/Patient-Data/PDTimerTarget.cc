#include "stdafx.h"
#if	defined(SIGMA_GUI_CPU) || defined(SIGMA_DEVELOPMENT)
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: PDTimerTarget - A target object to handle Patient Data Timer
//  events. Other classes can easily add a timer handling capability
//  through the mulple inheritance feature.
//---------------------------------------------------------------------
//@ Interface-Description
// Any classes which need to be informed of timer events (either one-shot or
// periodic events) need to multiply inherit from this class.  Then, 
// register itself with the PDTimerRegistar class to be informed of
// events via the timerEventHappened() method.
//---------------------------------------------------------------------
//@ Rationale
//   This class adds timer event handling capability to the Patient
//   Data processing agent classes.
//---------------------------------------------------------------------
//@ Implementation-Description
// If classes need to be informed of timer events they must do 3
// things: a) multiply inherit from this class, b) define the
// timerEventHappened() method of this class and c) register for
// change notices via the PDTimerRegistrar class.  They will then
// be informed of timer events via the timerEventHappened() method.
//---------------------------------------------------------------------
//@ Fault-Handling
//    SoftFault is generated if class preconditions or assertions
//    are not met.
//---------------------------------------------------------------------
//@ Restrictions
// This class should not be instantiated directly.  This class has use only
// when inherited from.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Patient-Data/vcssrc/PDTimerTarget.ccv   25.0.4.0   19 Nov 2013 14:17:58   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah   Date:  13-Jan-99    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001  By:  jhv    Date:  26-June-95    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "Sigma.hh"
#include "PDTimerTarget.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: PDTimerTarget()  [Default Constructor, Protected]
//
//@ Interface-Description
// The constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

PDTimerTarget::PDTimerTarget(void)
{
	CALL_TRACE("PDTimerTarget::PDTimerTarget(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~PDTimerTarget  [Destructor, Protected]
//
//@ Interface-Description
// Nothing to destruct!
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

PDTimerTarget::~PDTimerTarget(void)
{
	CALL_TRACE("PDTimerTarget::~PDTimerTarget(void)");

	// Do nothing
}
#endif	//defined(SIGMA_GUI_CPU) || defined(SIGMA_DEVELOPMENT)


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
PDTimerTarget::SoftFault(const SoftFaultID  softFaultID,
						 const Uint32       lineNumber,
						 const char*        pFileName,
						 const char*        pPredicate)  
{
	CALL_TRACE("PDTimerTarget::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, PATIENT_DATA,
			PATIENT_DATA_TIMER_TARGET, lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)
