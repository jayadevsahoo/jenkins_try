#pragma once
// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the GUIIOSERVER_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// GUIIOSERVER_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
#ifdef GUIIOSERVER_EXPORTS
#define GUIIOSERVER_API __declspec(dllexport)
#else
#define GUIIOSERVER_API __declspec(dllimport)
#endif

#include "Windows.h"

// If anything related to the GUI IO Server DLL client interface changes (constants change
// or API's), then bump this revision number. This way, the SWAT system can verify the 
// correct DLL is being used. 
#define GUIIO_DLL_VERSION  11

 typedef GUIIOSERVER_API enum 
{
    SWAT_SUCCESS				=	0x0000,		// SUCCESS
	SWAT_ERROR					=	0x0001,		// COMMON ERROR
	SWAT_OBJECT_ID_NOT_FOUND	=	0x0002,		// OBJECT ID NOT FOUND
	SWAT_HANDLER_UNDEFINED		=	0x0008,		// SWAT Handler is not defined
    SWAT_COMMAND_SYNTAX_ERROR   =   0x0009,     // SWAT command syntax error
    SWAT_COMMAND_EXECUTE_ERROR  =   0x000A,
    SWAT_SERVER_NOT_IMPLEMENTED =   0x000B,
    SWAT_CAPTURE_SCREEN_FAILED  =   0x000C,
    SWAT_SS_INDEX_OUT_OF_RANGE  =   0x000D,
    SWAT_SS_ALREADY_EXISTS      =   0x000E,
    SWAT_SS_INIT_ERROR          =   0x000F,
    SWAT_SS_CLEAR_FAILED        =   0x0010,

	GUIIO_SUCCESS               =   0x1000,
	GUIIO_ERROR                 =   0x1001,
	GUIIO_TIMEOUT               =   0x1002,
	GUIIO_OBJECT_ID_NOT_FOUND   =   0x1003,
	GUIIO_INVALID_TRANSACTION_ID            =   0x1004,
	GUIIO_WRONG_MESSAGE_RECEIVED            =   0x1005,
	GUIIO_TABLE_ROW_OR_COL_OUTSIDE_BOUNDS   =   0x1006,
	GUIIO_SCREEN_LOCKED                     =   0x1007,
	GUIIO_UNDEFINED                         =   0x1008  // The command requested was not defined on target
} GuiIOError;

 typedef GUIIOSERVER_API enum
{
    GUI_IO_MAX_ACTION_PARAMS = 4,
    GUI_IO_MAX_STR_LEN = 256,
	GUI_IO_LONG_MAX_STR_LEN = 1024
} GuiIOParamConstants;

typedef GUIIOSERVER_API enum
{
	CONTROL_DOWN = 0,
	CONTROL_CLICKED = 1,
	CONTROL_UP = 2,
	KNOB = 3,
	KEY = 4
} GuiIOOutputType;

typedef enum
{
    NO_ALT_LANGUAGE = -1,
    BULGARIAN,
    CHINESE,
    CROATIAN,
    CZECH,
    DANISH,
    DUTCH,
    FINNISH,
    FRENCH,
    GERMAN,
    GREEK,
    HUNGARIAN,
    ITALIAN,
    JAPANESE,
    KOREAN,
    NORWEGIAN,
    POLISH,
    PORTUGUESE,
    ROMANIAN,
    RUSSIAN,
    SERBIAN,
    SLOVAK,
    SLOVENIAN,
    SPANISH,
    SWEDISH,
    TURKISH,
    MAX_ALT_LANGUAGE,
} ALT_LANGUAGE;

//------------------------------------------------------------------------------
/// Defines the ACK/NAK response XML element members.
//------------------------------------------------------------------------------
typedef GUIIOSERVER_API struct
{
	INT error;
} GuiIOAckNakResp;

//------------------------------------------------------------------------------
/// Defines integer parameters XML element member.
//------------------------------------------------------------------------------
typedef GUIIOSERVER_API struct
{
    INT					params[GUI_IO_MAX_ACTION_PARAMS];
} GuiIOIntParams;

//------------------------------------------------------------------------------
/// Defines the query XY response XML element members.
//------------------------------------------------------------------------------
typedef GUIIOSERVER_API struct
{
	CHAR objectId[GUI_IO_MAX_STR_LEN];
	INT x;
	INT y;
} GuiIOQueryXYResp;

//------------------------------------------------------------------------------
/// Defines the query WH response XML element members.
//------------------------------------------------------------------------------
typedef GUIIOSERVER_API struct
{
	CHAR objectId[GUI_IO_MAX_STR_LEN];
	INT w;
	INT h;
} GuiIOQueryWHResp;

//------------------------------------------------------------------------------
/// Defines the get properties response XML element members.
//------------------------------------------------------------------------------
typedef GUIIOSERVER_API struct
{
	CHAR objectId[GUI_IO_MAX_STR_LEN];
	BOOL enabled;
	BOOL viewable;
	BOOL activated;
	BOOL adornment;
	BOOL blink;
	BOOL canFocus;
	INT state;
	INT row;
	INT col;
} GuiIOGetPropertiesResp;

//------------------------------------------------------------------------------
/// Defines the get object value response XML element members.
//------------------------------------------------------------------------------
typedef GUIIOSERVER_API struct
{
	CHAR objectId[GUI_IO_MAX_STR_LEN];
	CHAR value[GUI_IO_LONG_MAX_STR_LEN];
	INT row;
	INT col;
} GuiIOGetObjectValueResp;

//------------------------------------------------------------------------------
/// Defines the get focus response XML element members.
//------------------------------------------------------------------------------
typedef GUIIOSERVER_API struct
{
	CHAR objectId[GUI_IO_MAX_STR_LEN];
} GuiIOGetFocusResp;

//------------------------------------------------------------------------------
/// Defines the UI output response XML element members.
//------------------------------------------------------------------------------
typedef GUIIOSERVER_API struct
{
	GuiIOOutputType outputType;
	CHAR objectId[GUI_IO_MAX_STR_LEN];
	CHAR value[GUI_IO_MAX_STR_LEN];
	INT state;
	INT state2;
} GuiIOUIOutputResp;

//------------------------------------------------------------------------------
/// Defines the screen capture buffer response XML element members.
//------------------------------------------------------------------------------
typedef GUIIOSERVER_API struct
{
	INT bufferSize;
} GuiIOScreenCaptureBufferInfoResp;

//------------------------------------------------------------------------------
/// Defines the vent information response XML element members.
//------------------------------------------------------------------------------
typedef GUIIOSERVER_API struct
{
	CHAR guiSoftwareVer[GUI_IO_MAX_STR_LEN];
	CHAR bdSoftwareVer[GUI_IO_MAX_STR_LEN];
	CHAR serialNum[GUI_IO_MAX_STR_LEN];
	ALT_LANGUAGE language;
} GuiIOVentInfoResp;

//------------------------------------------------------------------------------
/// Defines the altia poll event response XML element members.
//------------------------------------------------------------------------------
typedef GUIIOSERVER_API struct
{
	CHAR eventName[GUI_IO_MAX_STR_LEN];
	INT eventValue;
} GuiIOAltiaPollEventResp;

//------------------------------------------------------------------------------
/// Defines the e600 QueryScreenshotPath response XML element members.
//------------------------------------------------------------------------------
typedef GUIIOSERVER_API struct
{
	INT index;
	CHAR address[GUI_IO_MAX_STR_LEN];
} GuiIOQueryScreenshotPathResp;

//------------------------------------------------------------------------------
/// Exported DLL functions.
//------------------------------------------------------------------------------
GUIIOSERVER_API GuiIOError GuiIOGetDllVersion(INT* ver);
GUIIOSERVER_API GuiIOError GuiIOConnect(const CHAR* addr, UINT port);
GUIIOSERVER_API GuiIOError GuiIODisconnect();
GUIIOSERVER_API GuiIOError GuiIOReceiveUIOutput(int timeout, GuiIOUIOutputResp* out);
GUIIOSERVER_API GuiIOError GuiIOCancelReceiveUIOutput();
GUIIOSERVER_API GuiIOError GuiIOMousePos(int x, int y);
GUIIOSERVER_API GuiIOError GuiIOQueryXY(const CHAR* objectId, GuiIOQueryXYResp* out);
GUIIOSERVER_API GuiIOError GuiIOQueryWH(const CHAR* objectId, GuiIOQueryWHResp* out);
GUIIOSERVER_API GuiIOError GuiIOGetProperties(const CHAR* objectId, GuiIOGetPropertiesResp* out);
GUIIOSERVER_API GuiIOError GuiIOGetTableProperties(const CHAR* objectId, int row, int col, GuiIOGetPropertiesResp* out);
GUIIOSERVER_API GuiIOError GuiIOGetObjectValue(const CHAR* objectId, GuiIOGetObjectValueResp* out);
GUIIOSERVER_API GuiIOError GuiIOGetTableCellObjectValue(const CHAR* objectId, int row, int col, GuiIOGetObjectValueResp* out);
GUIIOSERVER_API GuiIOError GuiIOGetTableRowObjectValue(const CHAR* objectId, int row, int totalRowCols, GuiIOGetObjectValueResp out[]);
GUIIOSERVER_API GuiIOError GuiIOGetFocus(GuiIOGetFocusResp* out);
GUIIOSERVER_API GuiIOError GuiIOUIOutputOnOff(bool onOff);
GUIIOSERVER_API GuiIOError GuiIOCursor(bool onOff);
GUIIOSERVER_API GuiIOError GuiIOScreenCapture(const CHAR* fileName);
GUIIOSERVER_API GuiIOError GuiIOGetScreenCaptureBufferInfo(GuiIOScreenCaptureBufferInfoResp* out);
GUIIOSERVER_API GuiIOError GuiIOSimulateBDAlarm(const CHAR* alarmId, const CHAR* condAugOptions, const CHAR* depenAugOptions);
GUIIOSERVER_API GuiIOError GuiIOGetVentInfo(GuiIOVentInfoResp* out);
GUIIOSERVER_API GuiIOError GuiIOBezelKeyPress(int keyId, bool onOff);
GUIIOSERVER_API GuiIOError GuiIOKnob(int delta);
GUIIOSERVER_API GuiIOError GuiIOAltiaSendEvent(const CHAR* eventName, int eventValue);
GUIIOSERVER_API GuiIOError GuiIOAltiaPollEvent(const CHAR* eventName, GuiIOAltiaPollEventResp* out);
GUIIOSERVER_API GuiIOError GuiIOLButtonDown(int x, int y);
GUIIOSERVER_API GuiIOError GuiIOLButtonUp(int x, int y);
GUIIOSERVER_API GuiIOError GuiIOButtonClick(int x, int y);
GUIIOSERVER_API GuiIOError GuiIOBye();

GUIIOSERVER_API GuiIOError GuiIOSimulateTouch(int x, int y);
GUIIOSERVER_API GuiIOError GuiIOClearScreenshots();
GUIIOSERVER_API GuiIOError GuiIOQueryScreenshotPath(int index, GuiIOQueryScreenshotPathResp* out);
