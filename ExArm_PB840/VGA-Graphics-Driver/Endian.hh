#ifndef Endian_HH
#define Endian_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: Endian - Endian Conversion Functions
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/VGA-Graphics-Driver/vcssrc/Endian.hhv   25.0.4.0   19 Nov 2013 14:37:28   pvcs  $
//
//@ Modification-Log
//  
//  Revision: 002   By: gdc    Date:  07-Mar-2008    SCR Number: 6430
//  Project: TREND2
//  Description:
//	Added endian conversion for 16-bit entities.
// 
//  Revision: 001   By: gdc    Date:  27-Jan-2000    DCS Number: 5337
//  Project: 840 Neonatal
//  Description:
//	Optimized VGA library for Neonatal project to use BitBlt and Windows
//  Bitmap structures.
//
//====================================================================

#include "VgaGraphicsDriver.hh"

//@ Usage-Classes
//@ End-Usage

class Endian 
{
  public:
	static inline Uint32 Convert( Uint32 i );
	static inline Uint16 Convert( Uint16 i );

  private:
	Endian();     // not implemented
	~Endian();    // not implemented

	static inline Uint32 SwapW_( Uint32 i );
};

#include "Endian.in"

#endif // Endian_HH 
