
#ifndef Array_MessageName_HH
#define Array_MessageName_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  Array_MessageName - Abstract Array of 'MessageName' Elements.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Foundation/vcssrc/AbstractArray.H_v   25.0.4.0   19 Nov 2013 14:03:40   pvcs  $
//
//@ Modification-Log
//
//   Revision: 003   By: sah   Date: 07-Jan-1999   DR Number: 5321
//   Project:  ATC
//   Description:
//	Changed 'SoftFault()' method to an non-inlined method.
//  
//   Revision: 002   By: sah   Date: 06-Feb-1996   DCS Number: 674
//   Project:  Sigma (R8027)
//   Description:
//	Removed 'ifndef' and 'endif' from around includes.
//  
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//====================================================================

#include "Sigma.hh"
#include "FoundationClassId.hh"
#include "TemplateMacros.hh"

#if defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)
class Ostream;
#endif  // defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)

//@ Usage-Classes
#include "MessageName.hh"
//@ End-Usage


class Array_MessageName
{
#if defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)
    // Friend:  operator<<
    // This facilitates debug-only printing of this class's instances.
    friend inline Ostream&  operator<<(Ostream&           ostr,
				       const Array_MessageName& array);
#endif  // defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)

  public:
    virtual ~Array_MessageName(void);

    inline Uint32  getNumElems(void) const;

    inline const MessageName&  operator[](const Uint32 index) const;
    inline MessageName&        operator[](const Uint32 index);

    void  cleanAllElems(const MessageName& newElemValue);

    void  operator=(const Array_MessageName& array);

    inline Boolean  operator==(const Array_MessageName& array) const;
    inline Boolean  operator!=(const Array_MessageName& array) const;

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL,
			   const char*       pPredicate = NULL);

  protected:
    inline Array_MessageName(MessageName* const  pInitElems,
			       const Uint32 initNumElems);

    Boolean  isEquivTo_(const Array_MessageName& array) const;

#if defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)
    virtual Ostream&  print_(Ostream& ostr) const = 0;
#endif  // defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)

  private:
    Array_MessageName(const Array_MessageName&);	// not implemented...
    Array_MessageName(void);				// not implemented...

    //@ Data-Member:  pElems_
    // An array of elements.
    MessageName* const  pElems_;

    //@ Constant:  NUM_ELEMS_
    // The number of elements within this array.
    const Uint32  NUM_ELEMS_;
};


// Inlined methods...
#include "Array_MessageName.in"


#endif  // Array_MessageName_HH
