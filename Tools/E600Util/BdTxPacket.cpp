#include "stdafx.h"
#include "BdTxPacket.h"

//BdTxPacket_t BdTxPacket;

BdTxPacket_t::BdTxPacket_t()
{
	if(!m_BdTxSocket.Socket(SOCK_DGRAM))
	{
		TRACE(L"BdTxPacket_t: Failed to Create Socket");
		return;
	}
	BOOL OptValue = TRUE; // 
	m_BdTxSocket.SetSockOpt(SO_BROADCAST, &OptValue, sizeof(OptValue), SOL_SOCKET);
	m_BdTxSocket.Bind(TOOL_ETHERNET_PORT_UDP);

	IpAddr.Format(L"192.168.0.3");
}

BdTxPacket_t::~BdTxPacket_t()
{
	//m_BdTxSocket.Close();
}

void BdTxPacket_t::SendBdPacket(char* pPacket, UINT16 PacketSize)
{
	int SizeSent = m_BdTxSocket.SendToEx(pPacket, PacketSize, Port, (LPCTSTR)IpAddr);

	if(SizeSent != PacketSize)
	{
		 UINT uErr = GetLastError();
		 if (uErr != WSAEWOULDBLOCK) 
		 {
			TCHAR szError[256];
			wsprintf(szError, L"Server Socket failed to send: %d", uErr);
			AfxMessageBox(szError);
		 }
	}
}