#include "stdafx.h"
//=====================================================================
//   This is a proprietary work to which Puritan-Bennett corporation
//   of California claims exclusive right.  No part of this work may
//  be used, disclosed, reproduced, sorted in an information retrieval
//     system, or transmitted by any means, electronic, mechanical,
//   photocopying, recording, or otherwise without the prior written
//      permission of Puritan-Bennett Corporation of California.
//
//          Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: TransitionTimer - Timer for GUI Transition and transition to 
//                        TIMEOUT state.
//---------------------------------------------------------------------
//@ Interface-Description
//  The TransitionTimer is a TaskControlTimer created by GUI task control
//  and used as a timeout for all state transitions. 
//---------------------------------------------------------------------
//@ Rationale
//  The TransitionTimer is the general purpose state transition timer.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The TransitionTimer is derived from the TaskControlTimer class which
//  provides the timing mechanism.  The GetTimer() function initializes
//  a static TransitionTimer and returns its reference to the caller.  
//  The constructor and destructor are private methods allowing object
//  construction only from the static member function GetTimer().  The
//  dispatch() method uses the AppContext to call the task's 
//  TransitionTimer callback function.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Sys-Init/vcssrc/TransitionTimer.ccv   25.0.4.0   19 Nov 2013 14:33:58   pvcs  $
//
//@ Modification-Log
//
//   Revision 004   By:   sah      Date: 01/13/99    DR Number: 5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to a non-inlined method.
//
//   Revision 003   By:   gdc      Date: 17-JUL-1997 DR Number: 2292
//      Project:   Sigma   (R8027)
//      Description:
//         Changed timeout to 5 seconds from 10 so Task Control can
//         respond to Task Monitor within monitoring period.
//
//   Revision 002   By:   gdc      Date: 11-JUN-1997 DR Number: 1902
//      Project:   Sigma   (R8027)
//      Description:
//         Modified to provide queue number to TaskControlTimer
//         base class constructor.
//
//   Revision 001   By:   gdc      Date: 07/06/95    DR Number:   n/a  
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//=====================================================================

//=====================================================================
//
//      Includes...
//
//=====================================================================

#include "TransitionTimer.hh"
#include <string.h>
#include "IpcIds.hh"

//@ Usage-Classes
#include "AppContext.hh"
#include "MsgTimer.hh"
#include "TaskControlQueueMsg.hh"
//@ End-Usage

//=====================================================================
//
//      Static Data...
//
//=====================================================================

const Uint TRANSITION_TIMER_MS = 5 * 1000;

//@ Code...

//=====================================================================
//
//      Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetTimer
//
//@ Interface-Description
//  This method initializes a static TransitionTimer and returns its
//  reference.  It initializes the static TransitionTimer on the first 
//  pass through this method.  On subsequent calls, it returns a
//  reference to the initialized TransitionTimer.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
TransitionTimer&
TransitionTimer::GetTimer(void)
{
    CALL_TRACE("GetTimer(void)");

    static TransitionTimer startupTimer(TRANSITION_TIMER_MS);
    return startupTimer;
    // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TransitionTimer [constructor]
//
//@ Interface-Description
//  This private constructor creates a TransitionTimer with the 
//  specified timeout value (in milliseconds).
//---------------------------------------------------------------------
//@ Implementation-Description 
//  Calls the base class TaskControlTimer constructor with this class
//  identifier and specified timeout value.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

TransitionTimer::TransitionTimer(const Int32 timeout)
    : TaskControlTimer(  Sys_Init::ID_TRANSITIONTIMER
                       , timeout
                       , TASK_CONTROL_TRANS_Q)
{
    CALL_TRACE("TransitionTimer(void)");
    // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~TransitionTimer [destructor]
//
//@ Interface-Description
//  TransitionTimer private destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

TransitionTimer::~TransitionTimer()
{
    CALL_TRACE("~TransitionTimer()");
}

//============================ M E T H O D   D E S C R I P T I O N ====
// Method: nameOf
//
// Interface-Description
//  Returns the name of the class.
//---------------------------------------------------------------------
// Implementation-Description
//  Returns a pointer to the const string containing the class name.
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================
 
const char*
TransitionTimer::nameOf(void) const
{
    CALL_TRACE("nameOf(void)");
    return "TransitionTimer";
    // $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: dispatch
//
//@ Interface-Description
//  This method dispatches this TransitionTimer to the specified 
//  AppContext.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls the AppContext dispatch method with a reference to this
//  TransitionTimer.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
void
TransitionTimer::dispatch(const AppContext& context) const
{
    CALL_TRACE("dispatch(void)");
    context.dispatch(*this);
    // $[TI1]
}

 

#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//
//@ Interface-Description
//   This method is called when a software fault is detected by the
//   fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//   'lineNumber' are essential pieces of information.  The 'pFileName'
//   and 'pPredicate' strings may be defaulted in the macro to reduce
//   code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This method receives the call for the SoftFault, adds it sub-system
//   and class name ID and sends the message to the non-member function
//   SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
void
TransitionTimer::SoftFault(const SoftFaultID  softFaultID,
                            const Uint32       lineNumber,
                            const char*        pFileName,
                            const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
 
  FaultHandler::SoftFault(softFaultID, SYS_INIT,
                          Sys_Init::ID_TRANSITIONTIMER, 
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)

//=====================================================================
//
//      Protected Methods...
//
//=====================================================================

//=====================================================================
//
//      Private Methods...
//
//=====================================================================
