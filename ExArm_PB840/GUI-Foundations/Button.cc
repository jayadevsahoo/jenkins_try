#include "stdafx.h"

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1995, Puritan-Bennett Corporation
//====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: Button - This is an all purpose general button.
//---------------------------------------------------------------------
//@ Interface-Description
// This object controls the rendering and states of a button.  The
// Button class is derived from the Container class, and thus is "aware"
// of the screen management framework.
//
// To use a Button in application code, the application programmer must
// create the button, specify what text or label will appear in the
// button, and set up some way for button actions to trigger application
// code.  Note that activation of the button via touch events is handled
// automatically, once the Button is attached to a BaseContainer.
//
// Creation of a Button object is controlled by the choice of parameters
// passed to its constructor.  These determine the initial position,
// size, border width, and type of the button.  The type determines the
// color scheme for each state of the button; the color attribute inherited
// from the Drawable class is not used.
//
// The text or label appearing in a button is specified by placing it
// in the button's label Container.  This Container is a private member
// of the Button class.  The label Container may hold an arbitrary number of
// screen objects, provided that all are derived from the Drawable class.
//
// There are two ways to trigger application code from a Button.  One
// way is to place the application code within a class derived from
// Button.  Such a class can redefine the protected downHappened_() and
// upHappened_() methods of Button; these are called when the button
// transitions to the selected (DOWN) or non-selected (UP) state.  Such
// application code becomes part of the Button.
//
// The second, more generic way to attach application code to a Button
// is via a callback mechanism.  This is done by making the ButtonTarget
// class a parent to the target application class; the application class
// thus inherits several methods (see below) which are called by Button
// when button events occur.  Since the ButtonTarget class adds interface
// capability only, multiple inheritance is permitted; this means the
// application class can have another parent class.  The target application
// class may redefine any of the following methods:
// >Von
//   buttonDownHappened()	Button touched, go to selected state (DOWN)
//   buttonUpHappened()		Button touched, go to non-selected state (UP)
// >Voff
// To attach a ButtonTarget to a Button, the application code must pass
// the chosen target's pointer to the Button's setButtonCallback()
// method.
//
// Public methods are supplied to allow the application to set the
// Button's state directly.  This is done using the Button's setToUp(),
// setToDown(), or setToFlat() methods.  Calls to these methods also
// trigger callbacks; however, a parameter is passed which allows the
// ButtonTarget to distinguish between these callbacks and those
// triggered by the operator.
//
// The public (virtual) draw() method inherited from Container is
// redefined to draw the button in a way appropriate to its state, and
// to draw the label container.  The Button class accepts calls to its
// draw() method from the screen management framework.  Changes to a
// Button will not result in changes on the screen until the
// application calls the BaseContainer::repaint() method, signaling the
// framework to update the screen.
//---------------------------------------------------------------------
//@ Rationale
// The Button class encapsulates the design details of a general-purpose
// 3-D touch-operated on-screen button, which appears to pop in or out
// when touched.  The shading of the bevels of the button is designed to
// create the illusion that the button is raised or recessed.  Several
// color schemes are provided.  It is not necessary for the application
// to derive classes from this class in order to use it; the button
// contents are customizable by attaching contents to the label
// container, and button events can trigger application code via
// callbacks.  However, an application class must be derived from Button
// if the look or other function of the button must be changed.
//---------------------------------------------------------------------
//@ Implementation-Description
// The three possible states of a button are UP, DOWN, 
// and FLAT.  The terms "up" and "down" refer to the
// 3-D rendering technique used.  A button that appears to be raised is
// "up", while a button that looks depressed is "down".  Button action
// models a real button which reverses its raised or depressed condition
// when pressed.  Touching a depressed button causes it to pop
// out, while touching a raised button makes it pop in.
//
// Note that the "selected" condition is not a state maintained by a Button.
// It is a concept used extensively at the application level, however.  A
// button is considered to be "selected" if it is in the DOWN 
// state; otherwise it is considered "non-selected".
// Transition from the selected to the non-selected condition only occurs on
// the touch.
//
// The FLAT state is imposed when a button is non-functional, but for some
// reason it must remain visible.  While in this state, touching the button has
// no effect.
//
// The touch() function is inherited from the
// Drawable class.  A call to this method causes a transition between 
// button states.  Re-defining this method allows Button to control
// how it reacts to events on the touch screen.  Calls to this method
// is dispatched from the Touch object, which receives notification of
// touch-related events from the GUI-Applications subsystem.
//
// The image of the button is made up of two basic parts, referred to as
// the foreground and the background.  The background is the basic image
// of the button -- a button with nothing on it.  The foreground
// consists of all the labeling and other information displayed on the
// button.  This information is contained in a Container called
// labelContainer_ which is a private member of the Button class.  This
// container may hold any set of Drawables that the GUI is capable of
// producing.
//
// Note that this labelContainer_ is both a member of the Button class,
// AND is contained by the Button object.  This is possible because
// Button is derived from the Container class, and thus inherits the
// ability to contain Containers.
//
// The technique used to produce a three-dimensional effect requires the
// foreground to shift downward and to the right whenever the button is
// to appear depressed.  Therefore, labelContainer_ has two positions,
// one for when the Button is UP, and the another for all other states.
//
// The rendering of the contents of labelContainer_ is affected by the
// button state and type.  When the button is rendered by a call to
// drawButton_(), if the button background central area is white or
// light gray, the contents are drawn in black; otherwise, the central
// area is medium or dark gray, and the labelContainer_ is rendered in white.
//---------------------------------------------------------------------
//@ Fault-Handling
// This class uses the "contract programming" technique.  This technique
// utilizes the class assertion and class precondition macros provided by
// the Utilities subsystem to generate error messages in response to software
// faults.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
//@(#) $Header:   /840/Baseline/GUI-Foundations/vcssrc/Button.ccv   25.0.4.0   19 Nov 2013 14:11:40   pvcs  $
//
//@ Modification-Log
//
//  Revision: 016  By:  gdc    Date:  10-Dec-2008    SCR Number: 6377
//  Project:  840S
//  Description:
//	Removed DELTA project single screen support.
//
//  Revision: 015   By: gdc    Date:  21-May-2007    SCR Number: 6330
//  Project: Trend
//  Description:
//	Removed SUN prototype code.
// 
//  Revision: 014   By: gdc    Date:  21-May-2007    SCR Number: 6237
//  Project: Trend
//  Description:
//	Made accessors getButtonState() and getButtonType() const.
//	Added momentary push button type.
// 
//  Revision: 012   By: gdc    Date:  29-Aug-2000    DCS Number: 5493
//  Project: GuiComms
//  Description:
//	Added Single Screen Compatibility.
//
//  Revision: 011   By: gdc    Date:  29-Jun-1999    DCS Number: 5337
//  Project: 840 Neonatal
//  Description:
//      Optimized VGA library for Neonatal project to use BitBlt and Windows
//      Bitmap structures.
//
//  Revision: 010  By:  hhd	   Date:  24-May-1999    DR Number: 5369 
//  Project:  Sigma (R8027)
//  Description:
//		Removed method setButtonType().  This was essentially an empty method since
//		all of its code was commented out.
//
//  Revision: 009   By: sah    Date: 11-Jan-1999   DR Number: 5321
//  Project:  Sigma (R8027)
//  Description:
//      Changed 'SoftFault()' method to non-inline method.
//
//  Revision: 008  By: gdc     Date: 16-Jun-1998     DCS Number:
//  Project:  Color
//  Description:
//      Initial release.
//
//  Revision: 007  By: sah       Date: 06-Oct-1997    DCS Number: 2264
//  Project:  Sigma (R8027)
//  Description:
//      Fixed incorrect handling of VGA access requests/releases, which
//      may have caused the problem with the errant lines outside of the
//      waveform grid, by removing all semaphore requests/releases from
//      all of the 'draw()' methods.
//
//  Revision: 006  By:  yyy    Date:  10-Sep-97    DR Number: 2169
//    Project:  Sigma (R8027)
//    Description:
//      Removed obsolete SRS references.
//
//  Revision: 005  By: sah       Date: 12-Jun-1997    DCS Number: 2208
//    Project:  Sigma (R8027)
//    Description:
//      Incorporated new VGA interface methods, files and types.
//
//  Revision: 004  By: yyy      Date: 29-Mar-1996  DCR Number: 473
//    Project:  Sigma (R8027)
//    Description:
//      Added calls to GuiFoundation::RequestDisplayAccess() and ReleaseDisplayAccess()
//      in order to allow shared access to the display by separate drawing tasks.
//
//  Revision: 003  By: clw      Date: 20-Mar-1996  DCR Number: 1955
//    Project:  Sigma (R8027)
//    Description:
//      Deleted CONTACT sound generation which was being done when 
//      the button was initially touched and when the user's
//      finger was dragged out of the button.  The CONTACT sound
//      is now generated only when the button is released, either
//      to select it or to deselect it.
//
//  Revision: 002  By: gdc      Date: 15-Feb-1996  DCR Number: 981
//    Project:  Sigma (R8027)
//    Description:
//      Corrected minor syntax errors detected by new compiler.
//  
//  Revision: 001  By: yyy      Date: 01-Jan-1995  DR Number: NONE
//    Project:  Sigma (R8027)
//    Description:
//	Integration baseline.
//=====================================================================

#include "Sigma.hh"
#include "Colors.hh"

#include "Button.hh"

#include "Sound.hh"

//@ Usage-Classes
#include "ButtonTarget.hh"
#include "DisplayContext.hh"
#include "GraphicsContext.hh"
#include "BaseContainer.hh"
//@ End-Usage

//@ Code...

//=====================================================================
//
// Public methods
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Button
//
//@ Interface-Description
// Constructor which defines the position and graphical appearance of this
// button.  The "xOrg" and "yOrg" define the position of the upper left corner.
// The "width" and "height" specify the width and height of this
// button.  The "buttonType" specifies the coloring scheme used for this button
// and the "bevelWidth" specifies the width of the 3-D bevels which appear at
// the edges of the button.  The "cornerType" specifies whether or not to draw
// rounded corners and the "borderType" whether or not the button has a
// "surrounding box" border.  Finally, "buttonState" specifies the initial
// state of the Button.
//
// Note that the width and height specified includes the optional surrounding
// border; therefore, if the borderType is BORDER, the effective size of the
// button will be smaller than if the borderType is NO_BORDER. The width of
// the optional border is automatically set equal to the width of the bevel.
//---------------------------------------------------------------------
//@ Implementation-Description
// Initializes all member variables, including inherited member variables from
// the Drawable base class.  Sizes and positions the button based on the given
// parameter values.  Initializes the label Container and appends it to this
// Container.  Finally, calls the setState_() method to initialize the button
// state.
//
// $[01021] Button must have the ability to display 3-D look.
// $[01022] Button shall have the ability to provide up to three appearances.
//
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Button::Button(Int16      xOrg, 
			   Int16      yOrg, 
			   Int16      width, 
			   Int16      height, 
			   ButtonType  buttonType,
			   Int16       bevelWidth, 
			   CornerType  cornerType,
			   BorderType  borderType, 
			   ButtonState buttonState,
			   PushButtonType pushButtonType) :
	buttonState_(UP),			// overwritten below
	pButtonTarget_(NULL),
	xUpOffset_(0),
	yUpOffset_(0),
	xDownOffset_(0),
	yDownOffset_(0),
	borderWidth_(borderType == BORDER ? bevelWidth : 0),
	bevelWidth_(bevelWidth),    
	cornerType_(cornerType),  
	buttonType_(buttonType),
	INIT_BUTTON_TYPE_(buttonType),
	upperEdgeColor_(Colors::NO_COLOR),
	lowerEdgeColor_(Colors::NO_COLOR),
	centerColor_(Colors::NO_COLOR),
	borderColor_(Colors::NO_COLOR),
	pushButtonType_(pushButtonType)
{
	CALL_TRACE("Button::Button(lotsa parameters...)");

	//
	// Size and position the button.
	//
	setWidth(width);
	setHeight(height);
	setX(xOrg);
	setY(yOrg);

	//
	// Initialize some Drawable base class members.
	//
	isSensitive_ = TRUE;
	
	if (borderType == BORDER)
	{
		isOpaque_ = FALSE;									// $[TI1]
	}
	else
	{
		isOpaque_ = TRUE;									// $[TI2]
	}

	//
	// Initialize the 2 possible positions of the labelContainer_ within the
	// button:
	//
	xUpOffset_   = 1 + bevelWidth_ + borderWidth_;
	yUpOffset_   = 1 + bevelWidth_ + borderWidth_;
	xDownOffset_ = 1 + bevelWidth_ + borderWidth_ + 2;
	yDownOffset_ = 1 + bevelWidth_ + borderWidth_ + 2;

	//
	// Initialize label Container and add it to this Container.
	//
	labelContainer_.setX(getX() + bevelWidth_ + borderWidth_ + 1);
	labelContainer_.setY(getY() + bevelWidth_ + borderWidth_ + 1);
	labelContainer_.setWidth(width - 2*bevelWidth_ - 2*borderWidth_ - 2);
	labelContainer_.setHeight(height - 2*bevelWidth_ - 2*borderWidth_ - 2);
	labelContainer_.appendTo(this);
	if (buttonType_ == DARK)
	{
		labelContainer_.setContentsColor(Colors::WHITE);	//	$[TI3]
	}
	// 															$[TI4]

	//
	// Initialize the button to the user-specified button state.
	//
	setState_(buttonState);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~Button [virtual]
//
//@ Interface-Description
// Destroy object.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This destructor does nothing.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Button::~Button(void)
{
	CALL_TRACE("Button::~Button(void)");

	// do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: touch [virtual]
//
//@ Interface-Description
// This method is called by the Touch class when the screen is touched within
// the area of this button.  This method takes 2 parameters, which are the X
// and Y coordinates of the touch location; these are not used.  
//
// Based on the initial button state, this method causes the button to
// transition to a new state, and triggers the appropriate actions for those
// transitions.  If initially in the UP state, the button goes to the
// DOWN state; if initially in the DOWN state, the button goes to the
// UP state.  If initially in the FLAT state, nothing happens.
//
// Transition actions: In the FLAT state, no transition action occurs.
// When the button is previously in the DOWN state, the buttonUpHappened()
// callback is invoked. When the button is previously in the UP state, the
// buttonDownHappened() callback is invoked.
//---------------------------------------------------------------------
//@ Implementation-Description
// This method redefines the virtual Drawable::touch() method.  It performs the
// transition from the UP or DOWN state to the DOWN or UP states, respectively.
// It calls setState_() to handle the state transition properly.
//
// Callbacks are performed to notify the associated button target, if any,
// that the particular state transition occurred.
//
// If this button is in an illegal state (i.e., a state where it should
// not be possible to generate a touch() call), then a fault is
// triggered.
//
// $[01023] When a popped out button is touched, the GUI shall provide the
//			illusion of a physical button being pressed by changing in the
//			appearance of the button.
// $[01024] When a user touches a select button, the button must toggle its
//          select/deselect state.
// $[01032] A button which is not in the flat state must be touch sensitive.
// $[01349] The contact sound shall be produced when any select button ...
//
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
Button::touch(Int, Int) 
{
	CALL_TRACE("Button::touch(Int, Int)");

	//
	// Perform a valid state transition to a "touched" state.
	//
	switch (buttonState_)
	{												// $[TI1]
	case FLAT:										// $[TI1.1]
		//
		// Do nothing.  There's no reaction to touching a flat button.
		//
		break;
	case UP:								// $[TI1.2]
		Sound::Start(Sound::CONTACT);	// Make the CONTACT sound on touch

		setState_(DOWN);
		downHappened_(TRUE);		// TRUE indicates operator action

		//
		// Notify associated target, if any, that this button was
		// "selected".  TRUE param indicates operator action.
		//
		if (pButtonTarget_ != NULL) 					// $[TI1.2.1]
			pButtonTarget_->buttonDownHappened(this, TRUE);
														// $[TI1.2.2]
		break;
	case DOWN:								// $[TI1.3]
		Sound::Start(Sound::CONTACT);	// Make the CONTACT sound on touch

		setState_(UP);
		upHappened_(TRUE);			// TRUE indicates operator action

		//
		// Notify associated target, if any, that this button was
		// "deselected".  TRUE param indicates operator action.
		//
		if (pButtonTarget_ != NULL)						// $[TI1.3.1]
			pButtonTarget_->buttonUpHappened(this, TRUE);
														// $[TI1.3.2]
		break;
	default:
		SAFE_CLASS_ASSERTION_FAILURE();
		break;
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: leave [public, virtual]
//
//@ Interface-Description
// Called when the user's touching finger leaves the display area of 
// this object. 
//---------------------------------------------------------------------
//@ Implementation-Description
// This leave action has the same effect as releasing the button
// so just call the release() method.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
Button::leave(Int, Int)
{
	CALL_TRACE("Button::leave(Int, Int)");

	release();
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: release
//
//@ Interface-Description
// Called when the user removes their finger from this button.  We
// pop up the button if this a momentary push-button.
//---------------------------------------------------------------------
//@ Implementation-Description
// Call Button::setToUp() with byOperatorAction set TRUE since this 
// button was released by the operator and should force callback 
// actions.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
Button::release(void)
{
	CALL_TRACE("Button::release(void)");

	if (pushButtonType_ == PB_MOMENTARY && buttonState_ != UP )
	{
		setState_(UP);
		upHappened_(TRUE);	// TRUE = done due to user action.

		//
		// Notify associated target, if any, that this button is now "up". The
		//  TRUE parameter indicates this is being done due to user action.
		//
		if (pButtonTarget_ != NULL)
		{
			pButtonTarget_->buttonUpHappened(this, TRUE);
		}
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setToUp 
//
//@ Interface-Description
// Forces button state to UP (deselected) state.  This corresponds to what
// normally happens when the user presses a button that was previously down,
// causing it to pop up and become deselected.  Callback notification is
// carried out as if the user deselected the button except that the
// notified ButtonTarget is informed that this deselection was not commanded by
// the user, by setting the second parameter of the buttonUpHappened() target
// method to FALSE.  Similarly, the internal protected method upHappened_() is
// called, but its param is set to FALSE.
//---------------------------------------------------------------------
//@ Implementation-Description
// Calls setState_() to handle state transition properly.  Calls
// upHappened_() to perform any processing associated with state
// transition.  Notifies button target, if any, that button transitioned
// to UP state.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
Button::setToUp(void)
{
	CALL_TRACE("Button::setToUp(void)");

	if ( buttonState_ != UP )
	{														// $[TI3.1]
		setState_(UP);
		upHappened_(FALSE);	// FALSE = not being done due to user action.

		//
		// Notify associated target, if any, that this button is now "up". The
		//  FALSE parameter indicates this is not being done due to user action.
		//
		if (pButtonTarget_ != NULL)
		{													// $[TI1]
			pButtonTarget_->buttonUpHappened(this, FALSE);
		}													// $[TI2]	
	}														// $[TI3.2]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setToDown 
//
//@ Interface-Description
// Forces button state to DOWN (selected) state.  This corresponds to what
// normally happens when the user presses a button that was previously up,
// causing it to pop in and become selected.  Callback notification is
// carried out as if the user pressed the button except that the
// notified ButtonTarget is informed that this selection was not commanded by
// the user, by setting the second parameter of the buttonDownHappened() target
// method to FALSE.  Similarly, the internal protected method downHappened_() is
// called, but its param is set to FALSE.
//---------------------------------------------------------------------
//@ Implementation-Description
// Calls setState_() to handle state transition properly.  Calls
// downHappened_() to perform any processing associated with state
// transition.  Notifies button target, if any, that button transitioned
// to DOWN state.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
Button::setToDown(void)
{
	CALL_TRACE("Button::setToDown(void)");

	if ( buttonState_ != DOWN )
	{														// $[TI3.1]
		setState_(DOWN);
		downHappened_(FALSE);	// FALSE = not being done due to user action.

		//
		// Notify associated target, if any, that this button is now "down". The
		//  FALSE parameter indicates this is not being done due to user action.
		//
		if (pButtonTarget_ != NULL)
		{													// $[TI1]
			pButtonTarget_->buttonDownHappened(this, FALSE);
		} 												   	// $[TI2]	
	}														// $[TI3.2]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setToFlat 
//
//@ Interface-Description
// Forces button state to FLAT.
//---------------------------------------------------------------------
//@ Implementation-Description
// Calls setState_() to handle state transition properly.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
Button::setToFlat(void)
{
	CALL_TRACE("Button::setToFlat(void)");

	if ( buttonState_ != FLAT )
	{ 														// $[TI1]
		setState_(FLAT);
	}														// $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setButtonType
//
//@ Interface-Description
// Changes the button type to that specified.
//---------------------------------------------------------------------
//@ Implementation-Description
// Updates the buttonType_ data member.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
Button::setButtonType(Button::ButtonType buttonType)
{
	CALL_TRACE("Button::setButtonType(ButtonType)");

	// This method is called in the current version but takes no action.
	// To affect a change in button type, uncomment the following two
	// lines.
	changing();
	buttonType_ = buttonType;
	// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getButtonState
//
//@ Interface-Description
// Gets the button state.
//---------------------------------------------------------------------
//@ Implementation-Description
// Returns the buttonState_ data member.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Button::ButtonState Button::getButtonState(void) const
{
	CALL_TRACE("Button::getButtonState(void)");

	return(buttonState_);
	// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getButtonType
//
//@ Interface-Description
// Gets the button type.
//---------------------------------------------------------------------
//@ Implementation-Description
// Returns the buttonType_ data member.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Button::ButtonType Button::getButtonType(void) const
{
	CALL_TRACE("Button::getButtonType(void)");

	return(buttonType_);
	// $[TI1]
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setBorderColor
//
//@ Interface-Description
// Changes the "surrounding box" border color to the given "borderColor".
//---------------------------------------------------------------------
//@ Implementation-Description
// Updates the "opaque" flag.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
Button::setBorderColor(Colors::ColorS borderColor)
{
	CALL_TRACE("Button::setBorderColor(Colors::ColorS)");

	changing();
	borderColor_ = borderColor;
	if (borderColor_ == Colors::NO_COLOR)
	{	//										$[TI1]
		isOpaque_ = FALSE;
	}
	else
	{	//										$[TI2]
		isOpaque_ = TRUE;
	}

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: draw [virtual]
//
//@ Interface-Description
// Draws this button on the VGA display.  The passed "clipArea" is not used,
// except to pass it to the draw() method of parent class Container.
//---------------------------------------------------------------------
//@ Implementation-Description
// Calls drawButton_() to draw the button image, other than the label.  The way
// the button is drawn depends on the button type and current state.  Depending
// on the state and type, certain pieces of the button (frame, bevels,
// background, etc.)  may have different colors; depending on the
// state, the button may be drawn with a raised or depressed appearance.
//
// Calls Container::draw() on itself (it being a Container) to handle the label
// portion of the button (labelContainer_), which is a contained Container.
//---------------------------------------------------------------------
//@ PreCondition
//  The object must be visible.
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
Button::draw(const Area& clipArea)
{
	CALL_TRACE("Button::draw(const Area&)");
	CLASS_PRE_CONDITION(isVisible());

	//
	// First, draw the generic portions of the button.  While this call does
	// not draw the labelContainer_, it does set the color of labelContainer_'s
	// contents.
	// Note: this results in a call to this object's changing() method
	// thus affecting the repaint.
	//
	//										$[TI1]
	drawButton_();

	//
	// Second, draw the label container portion of the button.  Note that the
	// labelContainer_ is contained by this button which is itself a Container;
	// hence, this call to the draw() method inherited from Container simply
	// passes a draw() call to all contained Drawables; and the labelContainer_
	// is the ONLY contained Drawable.  The other graphical elements of a Button
	// are NOT treated as contained Drawables.
	//
	// It is assumed that the color scheme of the labelContainer_'s contents
	// has already been set up by a call to the determineColors_() method;
	// this is called within the drawButton_() method called above.
	//
	Container::draw(clipArea);
}


//=====================================================================
//
// Protected Methods:
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: upHappened_ [virtual, protected]
//
//@ Interface-Description
// This protected method handles notification of button state change to the UP
// state.  The Boolean "isByOperatorAction" parameter is unused; this method
// does nothing.  It is provided so that classes derived from Button can
// redefine it to receive notification of button deselection without having to
// use the callback mechanism.  Note that the transition into the deselected
// (UP) state happens when the user RELEASES the button.
//---------------------------------------------------------------------
//@ Implementation-Description
// Empty implementation of a virtual function.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
Button::upHappened_(Boolean)
{
	CALL_TRACE("Button::upHappened_(Boolean)");

	//										$[TI1]
	// do nothing
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: downHappened_ [virtual, protected]
//
//@ Interface-Description
// This protected method handles notification of button state change to the
// DOWN state.  The Boolean "isByOperatorAction" parameter is unused; this
// method does nothing.  It is provided so that classes derived from Button can
// redefine it to receive notification of button selection without having to
// use the callback mechanism.  Note that the transition into the selected
// (DOWN) state happens when the user RELEASES the button.
//---------------------------------------------------------------------
//@ Implementation-Description
// Empty implementation of a virtual function.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
Button::downHappened_(Boolean)
{
	CALL_TRACE("Button::downHappened_(Boolean)");

	//										$[TI1]
	// do nothing
}


//=====================================================================
//
// Private Methods:
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setState_
//
//@ Interface-Description
// Changes the button's state to the given "newState".  Ensures that the label
// position stays in sync with the new button state.  This is needed because
// the label container is required to shift down and to the right when the
// button goes to the depressed state to enhance the 3-D effect.
//
// This method does not handle any other actions related to button state
// changes, such as executing callbacks, or generating sound effects.
//---------------------------------------------------------------------
//@ Implementation-Description
// Marks this button as needing redraw, updates the internal buttonState_, then
// sets the origin of the label Container based on whether the button is "up"
// or "down".
//
// This is the only place where the value of buttonState_ should be changed.
// When the button is actually drawn later, the state change will result in the
// needed changes to the way the button looks.
//
// $[01022] Button shall have the ability to provide up to three appearances.
//---------------------------------------------------------------------
//@ PreCondition
//  Button is beyond the legal state.
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
Button::setState_(ButtonState newState)
{
	CALL_TRACE("Button::setState_(ButtonState newState)");

	changing();
	buttonState_ = newState;

	switch (buttonState_) 
	{												// $[TI1]
	case UP:										// $[TI1.1]
	case FLAT:										// $[TI1.2]
		labelContainer_.setX(xUpOffset_);
		labelContainer_.setY(yUpOffset_);
		break;
	case DOWN:										// $[TI1.5]
		labelContainer_.setX(xDownOffset_);
		labelContainer_.setY(yDownOffset_);
		break;
	default:
		CLASS_ASSERTION_FAILURE();
		break;
	}

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: drawButton_
//
//@ Interface-Description
// Draws the basic button image, not including the application-specific label
// portion of the button managed by the button's label Container.
// Assumes that the Button is visible.
//---------------------------------------------------------------------
//@ Implementation-Description
// First, calls determineColors_() to set up the colors of the various button
// components and the labelContainer_ based on the button state and type.
// Then, depending upon the button state, calls either drawButtonUp_() or
// drawButtonDown_().  One of these methods then draws the button image.
// Note that this method does *not* draw the label.
//
// $[01022] Button shall have the ability to provide up to three appearances.
//
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void 
Button::drawButton_(void)
{
	CALL_TRACE("Button::drawButton_(void)");

	//
	// Setup color scheme for all applicable button components and the
	// contents of the labelContainer_.
	// Note: this results in a call to this object's changing() method
	// thus affecting the repaint.
	//
	determineColors_();

	switch (buttonState_) 
	{											// $[TI1]
	case UP:   									// $[TI1.1]
		drawButtonUp_();
		break;
	case DOWN:									// $[TI1.2]
	case FLAT:									// $[TI1.3]
		drawButtonDown_();
		break;
	default:
		CLASS_ASSERTION_FAILURE();
		break;
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: drawButtonUp_
//
//@ Interface-Description
// This method draws the image of the button (not including the button label)
// in its up (raised) position. Assumes that the Button is visible.
//
// Prior to calling this method, a call to determineColors_() must be made to
// set up the button's color scheme based on the button state and the
// buttonType_.
//---------------------------------------------------------------------
//@ Implementation-Description
// This method draws a "bare" raised button, using the color scheme set up by
// the previous call to determineColors_().  The way the button is drawn (other
// than color) is determined by the cornerType_, the bevelWidth_, the
// borderType_ (which determines the borderWidth_), and the buttonType_.
//
// This is the place where all the low-level calls into the VGA Driver
// subsystem are made for drawing a raised button.
//
// $[01021] Button must have the ability to display 3-D look.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
Button::drawButtonUp_(void)
{
	CALL_TRACE("Button::drawButtonUp_(void)")

	CLASS_ASSERTION(getParentContainer() != NULL);

	DisplayContext & rDc = getBaseContainer()->getDisplayContext();
	GraphicsContext & rGc = getBaseContainer()->getGraphicsContext();

	//
	// Determine the upper left and lower right corners of the surrounding box
	// border (i.e., the "exterior" of the button).  These are in real
	// (absolute) coordinates.
	//
	Int outer_x1 = getParentContainer()->convertToRealX(area_.getX1());
	Int outer_y1 = getParentContainer()->convertToRealY(area_.getY1());
	Int outer_x2 = getParentContainer()->convertToRealX(area_.getX2());
	Int outer_y2 = getParentContainer()->convertToRealY(area_.getY2());

	//
	// Determine the upper left and lower right corners of the interior of the
	// button, which is just inside the single pixel frame border.  The
	// borderWidth_ is 0 if the borderType is NO_BORDER; its equal to the bevel
	// width if borderType is BORDER.  These are also in real (absolute)
	// coordinates.
	//
	Int inner_x1 = outer_x1 + borderWidth_ + 1;
	Int inner_y1 = outer_y1 + borderWidth_ + 1;
	Int inner_x2 = outer_x2 - borderWidth_ - 1;
	Int inner_y2 = outer_y2 - borderWidth_ - 1;

	//
	// Draw the surrounding box border, if any.
	//
	if ((borderWidth_ != 0) && (borderColor_ != Colors::NO_COLOR)) 
	{	//										$[TI1]
		rGc.setForegroundColor( borderColor_ );
		rDc.fillRectangle(rGc, outer_x1, outer_y1, outer_x2, outer_y2);
	}
	//											$[TI2]
	
	//
	// Draw the top and side portions of the upper edge.
	//
	rGc.setForegroundColor( upperEdgeColor_ );
	rDc.fillRectangle(rGc, inner_x1, inner_y1, 
						  inner_x2, inner_y1 + bevelWidth_);
	rDc.fillRectangle(rGc, inner_x1, inner_y1 + bevelWidth_,
						  inner_x1 + bevelWidth_, inner_y2);

	//
	// Draw the side and bottom portions of the lower edge.
	//
	rGc.setForegroundColor( lowerEdgeColor_ );
	rDc.fillRectangle(rGc, inner_x2 - bevelWidth_, inner_y1 + bevelWidth_,
						  inner_x2, inner_y2);

	rDc.fillRectangle(rGc, inner_x1 + bevelWidth_, inner_y2 - bevelWidth_,
						  inner_x2 - bevelWidth_, inner_y2);

	if (buttonType_ == DARK) 
	{	//										$[TI3]
		//
		// The highlight polygon is needed since this button uses a DARK
		// color scheme.  This is the small white "line" in the upper left
		// corner of the button, where the bevels meet.
		//
		rGc.setForegroundColor( Colors::WHITE );
		rDc.fillTriangle(rGc,
								  inner_x1, inner_y1,
								  inner_x1 + (bevelWidth_ >> 1), inner_y1,
								  inner_x1, inner_y1 + (bevelWidth_ >> 1) );
						
		rDc.fillTriangle(rGc, 
								  inner_x1 + bevelWidth_, inner_y1 + bevelWidth_,
								  inner_x1 + (bevelWidth_ >> 1), inner_y1,
								  inner_x1, inner_y1 + (bevelWidth_ >> 1) );
	}
	//											$[TI4]
	//
	// Draw the triangles to complete the lower left and upper right
	// corners of the lower edge.
	//
	rGc.setForegroundColor( lowerEdgeColor_ );
	rDc.fillTriangle(rGc,
							  inner_x1, inner_y2,
							  inner_x1 + bevelWidth_, inner_y2 - bevelWidth_, 
							  inner_x1 + bevelWidth_, inner_y2 );
	rDc.fillTriangle(rGc,
							  inner_x2, inner_y1,
							  inner_x2 - bevelWidth_, inner_y1 + bevelWidth_, 
							  inner_x2, inner_y1 + bevelWidth_ );

	//
	// Draw the center rect.
	//
	rGc.setForegroundColor( centerColor_ );
	rDc.fillRectangle(rGc,
						  inner_x1 + bevelWidth_, inner_y1 + bevelWidth_,
						  inner_x2 - bevelWidth_, inner_y2 - bevelWidth_ );

	//
	// Draw the frame around the button.
	//
	Int radius = (cornerType_ == ROUND) ? 1 : 0;

	rGc.setForegroundColor( Colors::BLACK );
	rGc.setLineWidth( 1 );

	rDc.drawLine(rGc,
					  outer_x1 + borderWidth_ + radius,
					  outer_y1 + borderWidth_,
					  outer_x2 - borderWidth_ - radius,
					  outer_y1 + borderWidth_);
	rDc.drawLine(rGc,
					  outer_x1 + borderWidth_ + radius, 
					  outer_y2 - borderWidth_,
					  outer_x2 - borderWidth_ - radius,
					  outer_y2 - borderWidth_);
	rDc.drawLine(rGc,
					  outer_x1 + borderWidth_, 
					  outer_y1 + borderWidth_ + radius,
					  outer_x1 + borderWidth_, 
					  outer_y2 - borderWidth_ - radius);
	rDc.drawLine(rGc,
					  outer_x2 - borderWidth_, 
					  outer_y1 + borderWidth_ + radius,
					  outer_x2 - borderWidth_, 
					  outer_y2 - borderWidth_ - radius);

	if (cornerType_ == ROUND)
	{	//										$[TI5]
		//
		// Draw points at upper left and lower right of center rect to
		// "round" off the corners of the center rect.
		//
		rGc.setForegroundColor( upperEdgeColor_ );

		rDc.drawPoint(rGc, inner_x1 + bevelWidth_, inner_y1 + bevelWidth_);
					
		rGc.setForegroundColor( lowerEdgeColor_ );

		rDc.drawPoint(rGc, inner_x2 - bevelWidth_, inner_y2 - bevelWidth_);
	}
	//											$[TI6]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: drawButtonDown_ 
//
//@ Interface-Description
// This method draws the image of the button (not including the button label)
// in its down (depressed) position.  Assumes that the Button is visible.
//
// Prior to calling this method, a call to determineColors_() must be made to
// set up the button's color scheme based on the button state and the
// buttonType_.
//---------------------------------------------------------------------
//@ Implementation-Description
// This method draws a "bare" depressed button, using the color scheme set up by
// the previous call to determineColors_().  The way the button is drawn (other
// than color) is determined by the cornerType_, the bevelWidth_, and the
// borderType_ (which determines the borderWidth_).
//
// This is the place where all the low-level calls into the VGA Driver
// subsystem are made for drawing a depressed button.
//
// $[01021] Button must have the ability to display 3-D look.
// $[01023] When a popped out button is touched, the GUI shall provide the
//			illusion of a physical button being pressed by changing in the
//			appearance of the button.
//
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void Button::drawButtonDown_(void)
{
	CALL_TRACE("Button::drawButtonDown_(void)");

	CLASS_ASSERTION(getParentContainer() != NULL);

	DisplayContext & rDc = getBaseContainer()->getDisplayContext();
	GraphicsContext & rGc = getBaseContainer()->getGraphicsContext();

	//
	// Determine the upper left and lower right corners of the surrounding box
	// border (i.e., the "exterior" of the button).  These are in real
	// (absolute) coordinates.
	//
	Int outer_x1 = getParentContainer()->convertToRealX(area_.getX1());
	Int outer_y1 = getParentContainer()->convertToRealY(area_.getY1());
	Int outer_x2 = getParentContainer()->convertToRealX(area_.getX2());
	Int outer_y2 = getParentContainer()->convertToRealY(area_.getY2());

	//
	// Determine the upper left and lower right corners of the interior of the
	// button, which is just inside the single pixel frame border.  The
	// borderWidth_ is 0 if the borderType is NO_BORDER; it's equal to the bevel
	// width if borderType is BORDER.  These are also in real (absolute)
	// coordinates.
	//
	Int inner_x1 = outer_x1 + borderWidth_ + 1;
	Int inner_y1 = outer_y1 + borderWidth_ + 1;
	Int inner_x2 = outer_x2 - borderWidth_ - 1;
	Int inner_y2 = outer_y2 - borderWidth_ - 1;

	//
	// Draw the surrounding box border, if any.
	//
	if ((borderWidth_ != 0) && (borderColor_ != Colors::NO_COLOR)) 
	{	//										$[TI1]
		rGc.setForegroundColor( borderColor_ );
		rDc.fillRectangle(rGc, outer_x1, outer_y1, outer_x2, outer_y2);
	}
	//											$[TI2]
	
	//
	// Draw the top and side portions of the upper edge.
	//
	rGc.setForegroundColor( upperEdgeColor_ );

	rDc.fillRectangle(rGc, 
						  inner_x1, inner_y1, 
						  inner_x2, inner_y1 + bevelWidth_);
	rDc.fillRectangle(rGc, 
					   inner_x1, inner_y1 + bevelWidth_,
						  inner_x1 + bevelWidth_, inner_y2);

	//
	// Draw the side and bottom portions of the lower edge.
	//
	rGc.setForegroundColor( lowerEdgeColor_ );

	rDc.fillRectangle(rGc, 
						  inner_x2 - bevelWidth_, inner_y1 + bevelWidth_,
						  inner_x2, inner_y2);

	rDc.fillRectangle(rGc, 
						  inner_x1 + bevelWidth_, inner_y2 - bevelWidth_,
						  inner_x2 - bevelWidth_, inner_y2);

	//
	// Draw the triangles to complete the lower left and upper right
	// corners of the lower edge.
	//
	rDc.fillTriangle(rGc, 
							  inner_x1, inner_y2,
							  inner_x1 + bevelWidth_, inner_y2 - bevelWidth_, 
							  inner_x1 + bevelWidth_, inner_y2);
	rDc.fillTriangle(rGc, 
							  inner_x2, inner_y1,
							  inner_x2 - bevelWidth_, inner_y1 + bevelWidth_, 
							  inner_x2, inner_y1 + bevelWidth_);

	//
	// Draw the center rect.
	//
	rGc.setForegroundColor( centerColor_ );

	rDc.fillRectangle(rGc, 
						  inner_x1 + bevelWidth_, inner_y1 + bevelWidth_,
						  inner_x2 - bevelWidth_, inner_y2 - bevelWidth_);

	//
	// Draw the frame around the button.
	//
	Int radius = (cornerType_ == ROUND) ? 1 : 0;

	rGc.setForegroundColor( Colors::BLACK );
	rGc.setLineWidth( 1 );

	rDc.drawLine(rGc, 
					  outer_x1 + borderWidth_ + radius,
					  outer_y1 + borderWidth_,
					  outer_x2 - borderWidth_ - radius,
					  outer_y1 + borderWidth_);
	rDc.drawLine(rGc, 
					  outer_x1 + borderWidth_ + radius, 
					  outer_y2 - borderWidth_,
					  outer_x2 - borderWidth_ - radius,
					  outer_y2 - borderWidth_);
	rDc.drawLine(rGc, 
					  outer_x1 + borderWidth_, 
					  outer_y1 + borderWidth_ + radius,
					  outer_x1 + borderWidth_, 
					  outer_y2 - borderWidth_ - radius);
	rDc.drawLine(rGc, 
					  outer_x2 - borderWidth_, 
					  outer_y1 + borderWidth_ + radius,
					  outer_x2 - borderWidth_, 
					  outer_y2 - borderWidth_ - radius);

	if (cornerType_ == ROUND)
	{	//										$[TI5]
		//
		// Draw points at upper left and lower right of center rect to
		// "round" off the corners of the center rect.
		//
		rGc.setForegroundColor( upperEdgeColor_ );

		rDc.drawPoint(rGc, inner_x1 + bevelWidth_, inner_y1 + bevelWidth_);
					
		rGc.setForegroundColor( lowerEdgeColor_ );

		rDc.drawPoint(rGc, inner_x2 - bevelWidth_, inner_y2 - bevelWidth_);
	}
	//											$[TI6]

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: determineColors_ 
//
//@ Interface-Description
// Determines the proper color scheme for this button as a function
// of button's type and state.  Sets up these colors in preparation
// for rendering by the draw() method.  This method also sets up the
// color for the button's label (contents of the label Container).
//---------------------------------------------------------------------
//@ Implementation-Description
// Sets the upper edge color, lower edge color, button center (interior) color.
// The contents color for the label container is also set, unless the button
// type is LIGHT; for LIGHT buttons, the label background is always light, so
// it is not necessary to reverse the label contents color to white.
// Note: the setContentColor() will triggle a call to this object's changing()
// method thus affecting the repaint.
//
// $[01021] Button must have the ability to display 3-D look.
// $[01031] All types of buttons must be settable to a "flat" state in 2D
//			rectangle with no 3D shading.
//---------------------------------------------------------------------
//@ PreCondition
//  buttonType_ and buttonState_ should be in valid state.
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void 
Button::determineColors_(void) 
{
	CALL_TRACE("Button::determineColors_(void)");

	switch (buttonType_) 
	{												// $[TI1]
	case LIGHT_NON_LIT: 							// $[TI1.1]
		switch (buttonState_)
		{											// $[TI1.1.1]
		case UP:   									// $[TI1.1.1.1]
			upperEdgeColor_ = Colors::WHITE;
			centerColor_    = Colors::LIGHT_BLUE;
			lowerEdgeColor_ = Colors::DARK_BLUE;
			labelContainer_.setContentsColor(Colors::BLACK);
			break;
		case DOWN: 									// $[TI1.1.1.4]
			upperEdgeColor_ = Colors::MEDIUM_BLUE;
			centerColor_    = Colors::MEDIUM_BLUE;
			lowerEdgeColor_ = Colors::MEDIUM_BLUE;
			labelContainer_.setContentsColor(Colors::WHITE);
			break;
		case FLAT: 									// $[TI1.1.1.5]
			upperEdgeColor_ = Colors::LIGHT_BLUE;
			centerColor_    = Colors::LIGHT_BLUE;
			lowerEdgeColor_ = Colors::LIGHT_BLUE;
			labelContainer_.setContentsColor(Colors::BLACK);
			break;
		default:
			CLASS_ASSERTION_FAILURE();
			break;
		}
		break;
	case DARK: 									// $[TI1.2]  
		switch (buttonState_) 
		{										// $[TI1.2.1]
		case UP:   								// $[TI1.2.1.1]
			upperEdgeColor_ = Colors::LIGHT_BLUE;
			centerColor_    = Colors::DARK_BLUE;
			lowerEdgeColor_ = Colors::EXTRA_DARK_BLUE;
			labelContainer_.setContentsColor(Colors::WHITE);
			break;
		case DOWN: 								// $[TI1.2.1.4]
			labelContainer_.setContentsColor(Colors::BLACK);
			upperEdgeColor_ = Colors::EXTRA_DARK_BLUE;
			lowerEdgeColor_ = Colors::LIGHT_BLUE;
//
// 			$[01039] A selected (pushed in) button shall display a light
//					background to indicate that it is the currently selected
//					button.
//
			centerColor_    = Colors::GOLDENROD;
			break;
		case FLAT: 								// $[TI1.2.1.5]
			upperEdgeColor_ = Colors::DARK_BLUE;
			lowerEdgeColor_ = Colors::DARK_BLUE;
			centerColor_    = Colors::DARK_BLUE;
			labelContainer_.setContentsColor(Colors::WHITE);
			break;
		default:
			CLASS_ASSERTION_FAILURE();
			break;
		}
		break;
	case LIGHT: 								// $[TI1.3]
		switch (buttonState_) 
		{										// $[TI1.3.1]
		case UP:   								// $[TI1.3.1.1]
			upperEdgeColor_ = Colors::WHITE;
			centerColor_    = Colors::LIGHT_BLUE;
			lowerEdgeColor_ = Colors::DARK_BLUE;
			break;
		case DOWN: 								// $[TI1.3.1.4]

			if (INIT_BUTTON_TYPE_ == Button::DARK)
			{	// $[TI1.3.1.4.1]
				// use dark-style edge colors...
				upperEdgeColor_ = Colors::EXTRA_DARK_BLUE;
				lowerEdgeColor_ = Colors::LIGHT_BLUE;
			}
			else
			{	// $[TI1.3.1.4.2]
				// use light-style edge colors...
				upperEdgeColor_ = Colors::DARK_BLUE;
				lowerEdgeColor_ = Colors::WHITE;
			}

//
// 			$[01039] A selected (pushed in) button shall display a light
//					background to indicate that it is the currently selected
//					button.
//
			centerColor_ = Colors::GOLDENROD;
			break;
		case FLAT: 								// $[TI1.3.1.5]
			upperEdgeColor_ = Colors::LIGHT_BLUE;
			lowerEdgeColor_ = Colors::LIGHT_BLUE;
			centerColor_    = Colors::LIGHT_BLUE;
			break;
		default:
			CLASS_ASSERTION_FAILURE();
			break;
		}
		break;
	default:
		CLASS_ASSERTION_FAILURE();
		break;
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
Button::SoftFault(const SoftFaultID  softFaultID,
				  const Uint32       lineNumber,
				  const char*        pFileName,
				  const char*        pPredicate)  
{
	CALL_TRACE("Button::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_FOUNDATIONS, BUTTON,
							lineNumber, pFileName, pPredicate);
}
