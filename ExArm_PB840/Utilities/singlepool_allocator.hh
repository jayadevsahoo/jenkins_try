//------------------------------------------------------------------------------
//                   Copyright (c) 2009 - 2010 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
/// @file singlepool_allocator.h
///
/// This file contains a single memory pool version of the STL-compatible
/// fixed memory block allocator. The size of the memory block returned is
/// sizeof T (the template class argument). Each specific type created with this
/// class gets its own Allocator instance dedicated to providing memory blocks
/// the exact size of T.
///
/// This class is useful for providing an alternate STL allocator for
/// STL containers like list, map, and multimap.
//------------------------------------------------------------------------------
#ifndef SINGLEPOOL_ALLOCATOR_H
#define SINGLEPOOL_ALLOCATOR_H

//------------------------------------------------------------------------------
// I N C L U D E S
//------------------------------------------------------------------------------
#include "pool_allocator.hh"

//-----------------------------------------------------------------------------
/// singlepool_allocator with a void type.
//-----------------------------------------------------------------------------
template <typename T> class singlepool_allocator;
template <> class singlepool_allocator<void> : public pool_allocator<void>
{
public:
	//-------------------------------------------------------------------------
    /// Convert an singlepool_allocator<void> to an singlepool_allocator<U>.
    /// @pre        none
    /// @post       none
    //-------------------------------------------------------------------------
    template <class U>
        struct rebind { typedef singlepool_allocator<U> other; };
};

//-----------------------------------------------------------------------------
/// singlepool_allocator with a T type. T is the type of object being allocated.
//-----------------------------------------------------------------------------
template <typename T>
class singlepool_allocator : public pool_allocator<T>
{
public:
	//-------------------------------------------------------------------------
    /// Convert an singlepool_allocator<void> to an singlepool_allocator<U>.
    /// @pre        none
    /// @post       none
    //-------------------------------------------------------------------------
    template <class U>
        struct rebind { typedef singlepool_allocator<U> other; };

	//-------------------------------------------------------------------------
    /// Get an instance of the memory allocator.
	/// @return		An instance of the allocator used to obtain memory blocks.
	/// @param[in]	n - size of memory
    /// @pre        none
    /// @post       none
    //-------------------------------------------------------------------------
	virtual Allocator* get_allocator(size_t n)
	{
		if (!m_allocator)
			m_allocator = new Allocator(sizeof(T), 0, "singlepool_allocator");
		return m_allocator;
	}

	//-------------------------------------------------------------------------
    /// Constructor
    /// @pre        none
    /// @post       none
    //-------------------------------------------------------------------------
	singlepool_allocator(){}

	//-------------------------------------------------------------------------
    /// Copy constructor
    /// @pre        none
    /// @post       none
    //-------------------------------------------------------------------------
	template <class U> singlepool_allocator(const singlepool_allocator<U>&){}

private:
	// all singlepool_allocator instances of a particular type use the same
	// fixed block allocator
	static Allocator* m_allocator;
};

template <typename T> Allocator* singlepool_allocator<T>::m_allocator = NULL;

#endif // SINGLEPOOL_ALLOCATOR_H
