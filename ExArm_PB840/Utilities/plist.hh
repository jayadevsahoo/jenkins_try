//------------------------------------------------------------------------------
//                   Copyright (c) 2008 - 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
/// @file plist.h
///
/// This file contains an fixed block allocator used with the std::list
/// types so that any dynamic storage requested by the STL library is obtained
/// from a fixed memory pool and not the global heap. The class plist
/// is used in place of std::list. Use plist exactly the same as
/// std::list except that the maximum string size is limited to the pool
///	size limit.
///
/// Fixed block allocators are used to eliminate dynamic storage allocation
/// heap fragmentation issues.
///
/// The plist has a preset maximum number of elements that can be added.
/// This can help prevent memory leaks by catching early code that is adding
/// too many elements. Otherwise, the list would add element until the
/// heap is exhausted. The limit can be overridden using the 2nd template
/// argument should you need more than the preset limit (e.g. plist<int, 1234>).
///
/// Example usage:
///
///	plist<pstring> testlist;
///	testlist.push_back("test string");
//------------------------------------------------------------------------------
#ifndef PLIST_H
#define PLIST_H

#include "singlepool_allocator.hh"
#include <list>

template<class _Ty,
	size_t _MaxElements = 200,
	class _Ax = singlepool_allocator<_Ty> >
	class plist
		: public std::list<_Ty, _Ax>
	{
	public:
		void push_front(const _Ty& _Val)
		{
			std::list<_Ty, _Ax>::push_front(_Val);

			// If this assertion fails, it means you have exceeded a preset element limit
			// imposed on all plist instances. The limit is arbitrary and helps catch
			// software bugs that add too many elements. Without this check, the software
			// will continue to add elements until the heap is exhausted. You can override
			// the preset limit using the 2nd template argument (e.g. plist<int, 1234>).
			CLASS_ASSERTION(this->size() <= _MaxElements);
		}

		void push_back(const _Ty& _Val)
		{
			std::list<_Ty, _Ax>::push_back(_Val);

			// If this assertion fails, it means you have exceeded a preset element limit
			// imposed on all plist instances. The limit is arbitrary and helps catch
			// software bugs that add too many elements. Without this check, the software
			// will continue to add elements until the heap is exhausted. You can override
			// the preset limit using the 2nd template argument (e.g. plist<int, 1234>).
			CLASS_ASSERTION(this->size() <= _MaxElements);
		}

	private:
		///  This method is called when a software fault is detected by the
		///  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
		///  and 'lineNumber' are essential pieces of information.  The
		/// 'pFileName' and 'pPredicate' strings may be defaulted in the macro
		/// to reduce code space.
		///
		/// @param[in] SoftFaultID The ID of the softFault.
		/// @param[in] lineNumber The line number of the file.
		/// @param[in] pFileName The filename string.
		/// @param[in] pPredicate The boolean expression causing the fault
		/// @param[out] none.
		/// @return     none.
		///
		/// @pre  none.
		/// @post A software fault is reported.
		static void  SoftFault(const SoftFaultID softFaultID,
							   const Uint32      lineNumber,
							   const Char*       pFileName  = NULL,
							   const Char*       pPredicate = NULL)
		{
			FaultHandler::SoftFault(softFaultID, TOOLS,
					PLIST, lineNumber, pFileName, pPredicate);
		}
	};

#endif // PLIST_H

