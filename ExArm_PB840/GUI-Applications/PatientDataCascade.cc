#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: PatientDataCascade - A PatientDataTarget which stores a list
// of PatientDataTargets.  Allows multiple PatientDataTargets to
// register for changes to a single piece of patient data.
//---------------------------------------------------------------------
//@ Interface-Description
// PatientDataCascade inherits directly from PatientDataTarget so it can
// be a target for patient data changes.  The idea is that multiple objects
// may want to be informed when one particular patient datum changes so
// this class accomplishes that by storing an array of PatientDataTargets.
// Targets can be registered via the addTarget() method.
//
// The cascade can then be registered with the particular patient datum and,
// when it receives notification of a patient data change via its
// patientDataChangeHappened() method, it then calls the
// patientDataChangeHappened() method of each object that has registered with
// it.  Hence, the change is cascaded to all interested objects.
//
// The PatientDataCascade is intended for use by the PatientDataRegistrar
// class.
//---------------------------------------------------------------------
//@ Rationale
// Needed because multiple objects need notification when particular
// patient data change.
//---------------------------------------------------------------------
//@ Implementation-Description
// The cascade stores a list of PatientDataTarget pointers.  As objects
// register with the cascade (via addTarget()) pointers to the objects
// are stored in the array (called 'targets_[]').  If the cascade is
// registered as a target of a patient datum then it will receive update
// notices via the patientDataChangeHappened() method.  This method
// just loops through the pointers in the targets_[] array and calls
// their corresponding patientDataChangeHappened() methods.
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and pre-conditions
// to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
// There is a limit to the number of targets stored in a cascade.  This number
// can be increased if the need arises.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/PatientDataCascade.ccv   25.0.4.0   19 Nov 2013 14:08:12   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "PatientDataCascade.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: PatientDataCascade()  [Default Constructor]
//
//@ Interface-Description
// Constructs the PatientDataCascade.  Needs no arguments.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply initializes the targets_[] array.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

PatientDataCascade::PatientDataCascade(void) :
						numTargets_(0)
{
	CALL_TRACE("PatientDataCascade::PatientDataCascade(void)");

	// Initialise target pointers to NULL
	for (Uint16 i = 0; i < PDC_MAX_TARGETS; i++)
	{
		targets_[i] = NULL;
	}
														// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~PatientDataCascade  [Destructor]
//
//@ Interface-Description
// Destroys the PatientDataCascade.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

PatientDataCascade::~PatientDataCascade(void)
{
	CALL_TRACE("PatientDataCascade::~PatientDataCascade(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: addTarget [public]
//
//@ Interface-Description
// Adds a Patient Data target to this cascade.  Passed 'pTarget', a
// pointer to the target object.
//---------------------------------------------------------------------
//@ Implementation-Description
// Adds another target pointer to the 'targets_[]' array.
//---------------------------------------------------------------------
//@ PreCondition
// The target pointer must be non-null and the 'targets_[]' array must
// not be full.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
PatientDataCascade::addTarget(PatientDataTarget *pTarget)
{
	CALL_TRACE("PatientDataCascade::addTarget(PatientDataTarget *pTarget)");

	CLASS_PRE_CONDITION(pTarget != NULL);
	CLASS_PRE_CONDITION(numTargets_ < PDC_MAX_TARGETS);

	// Add target to the next empty element of the array.
	targets_[numTargets_] = pTarget;
	numTargets_++;										// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: patientDataChangeHappened  [public, virtual]
//
//@ Interface-Description
// Called when the patient datum of which this cascade is a target changes.  We
// simply call the corresponding method of all registered targets with the same
// parameter, 'patientDataId', the id of the patient datum that changed.
//---------------------------------------------------------------------
//@ Implementation-Description
// Walks the list of targets and calls their patientDataChangeHappened()
// methods in turn.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
PatientDataCascade::patientDataChangeHappened(
							PatientDataId::PatientItemId patientDataId)
{
	CALL_TRACE("PatientDataCascade::patientDataChangeHappened("
						"PatientDataId::PatientItemId patientDataId)");

	// Walk the 'targets_[]' array and call the patientDataChangeHappened()
	// method on each target.
	for (Uint16 i = 0; i < numTargets_; i++)
	{													// $[TI1]
		SAFE_CLASS_ASSERTION(targets_[i]);
		targets_[i]->patientDataChangeHappened(patientDataId);
	}													// $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
PatientDataCascade::SoftFault(const SoftFaultID  softFaultID,
		   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, PATIENTDATACASCADE,
									lineNumber, pFileName, pPredicate);
}
