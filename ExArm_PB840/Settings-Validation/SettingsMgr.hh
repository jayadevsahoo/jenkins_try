
#ifndef SettingsMgr_HH
#define SettingsMgr_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: SettingsMgr - Manager of the Setting Instances.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/SettingsMgr.hhv   25.0.4.0   19 Nov 2013 14:27:44   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'GetSettingSubjectPtr()' method to provide 'SettingSubject'
//	   pointers
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//  
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//====================================================================

#include "Settings_Validation.hh"
#include "SettingId.hh"
#include "SettingPtr.hh"

//@ Usage-Classes
class  Setting;			// forward declaration...
class  BoundedSetting;		// forward declaration...
class  DiscreteSetting;		// forward declaration...
class  SequentialSetting;	// forward declaration...
class  SettingSubject;		// forward declaration...
//@ End-Usage


class SettingsMgr
{
  public:
    static void  Initialize(void);

    static inline Setting*            GetSettingPtr(
				  const SettingId::SettingIdType settingId
						   );
    static inline BoundedSetting*     GetBoundedSettingPtr(
				  const SettingId::SettingIdType boundedId
						          );
    static inline DiscreteSetting*    GetDiscreteSettingPtr(
				  const SettingId::SettingIdType discreteId
							   );
    static inline SequentialSetting*  GetSequentialSettingPtr(
				  const SettingId::SettingIdType sequentialId
							     );

    static inline SettingSubject*      GetSettingSubjectPtr(
				  const SettingId::SettingIdType settingId
							   );

    static inline const SettingPtr*  GetArrayOfSettingPtrs(void);

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32 lineNumber,
			   const char*  pFileName  = NULL, 
			   const char*  pPredicate = NULL);

  private:
    SettingsMgr(const SettingsMgr&);		// not implemented...
    SettingsMgr(void);				// not implemented...
    ~SettingsMgr(void);				// not implemented...
    void  operator=(const SettingsMgr&);	// not implemented...

    //@ Data-Member:  ArrSettingPtrs_
    // A static array containing pointers to all of the settings.
    static SettingPtr  ArrSettingPtrs_[SettingId::TOTAL_SETTING_IDS];
};


// Inlined Methods...
#include "SettingsMgr.in"


#endif // SettingsMgr_HH 
