#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1994, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: UpperSubScreenArea - The area on the Upper screen
// in which subscreens are displayed.
//---------------------------------------------------------------------
//@ Interface-Description
// This is a Container class, derived from SubScreenArea, which manages the
// display of the Upper Subscreen Area.  Most of the basic subscreen switching
// functionality is provided by the base class.
//
// This class does provide specialized functionality that is specific to the
// upper screen.  The activateSafetyVentilationSubScreen() and
// deactivateSafetyVentilationSubScreen() methods handle the GUI-controlled
// Safety PCV subscreen.  The keyPanelPressHappened() method is the event
// handler for the info (help) key.
//
// This class is also the repository for all subscreen objects that are
// displayed in the Upper Subscreen Area, and this class provides a public
// access method for each subscreen member, except the Safety Ventilation
// Subscreen (which is accessed via the public activate/deactivate methods
// shown above).
//---------------------------------------------------------------------
//@ Rationale
// This class is both the manager of and the repository for all subscreens
// which are displayed on the Upper Subscreen Area.
//---------------------------------------------------------------------
//@ Implementation-Description
// For the most part, this class is simply the repository for all subscreen
// objects which appear on the Upper Subscreen Area.  The core screen switching
// functionality is inherited from the SubScreenArea base class.  However, this
// class does provide specialized functionality specific to the Upper Subscreen
// Area -- i.e., handling the Safety PCV subscreen, and handling off-screen key
// events that affect this subscreen area
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and pre-conditions
// to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/UpperSubScreenArea.ccv   25.0.4.0   19 Nov 2013 14:08:42   pvcs  $
//
//@ Modification-Log
//
//  Revision: 013  By: mnr     Date: 13-APR-2010     SCR Number: 6436
//  Project:  PROX
//  Description:
//      SstProxSubScreen added.
//
//  Revision: 012  By:  gdc    Date:  10-Dec-2008    SCR Number: 6377
//  Project:  840S
//  Description:
//	Removed DELTA project single screen support.
//
//  Revision: 011  By:  rhj	  Date:  21-Oct-2008    SCR Number: 6435
//  Project:  S840
//  Description:     
//      Added Software Options Subscreen.
// 
//  Revision: 010  By:  rhj	  Date:  03-Apr-2007    SCR Number: 6237
//  Project:  Trend
//  Description:     
//      Added TrendGraphSubScreen, TrendTableSubScreen and EventSubScreen.
//
//  Revision: 009   By: rhj   Date:  20-Sep-2006    SCR Number: 6236
//  Project:  RESPM
//  Description:
//      Added the RM Data Subscreen 
//
//  Revision: 008 By: jja    Date: 03-Jun-2002   DR Number: 5908
//  Project:  VCP
//	Description:
//		Incorporated Remediation code review issue(s) -- commentary changes only.
//
//  Revision: 007  By:  cep  Date:  17-Apr-2002    DCS Number: 5915 
//  Project: VCP 
//  Description:
//		Removed 2nd if clause (==STATE_SST) multiple places.
//
//  Revision: 006  By:  gdc	 	   Date:  08-Jan-2001    DCS Number: 5373 
//  Project: Delta
//  Description:
//		Switch to upper screen when INFO key pressed.
//
//  Revision: 005  By:  btray  Date:  21-Jul-1999    DCS Number: 5424 
//  Project: Neonatal 
//  Description:
//		Initial Neonatal version. Removed development option button
//		from normal mode.
//
//  Revision: 004  By:  gdc    Date:  01-Oct-1998    DCS Number: 5192
//  Project:  BiLevel
//  Description:
//      Reverted to code used prior to Color version since the BiLevel
//      implementation for the maneuver sub-screen was incompatible with 
//      the Color implementation of the SubScreenArea class. Added call to
//      restorePreviousSubScreen() to to restore the sub-screen that was 
//      active prior to activating a default sub-screen.
//
//  Revision: 003  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 002  By:  yyy   Date:  04-Aug-1997    DCS Number:  2315
//  Project:  Sigma (R8027)
//  Description:
//      Removed the clearDisplayForVentInop() method.
//
//  Revision: 001  By:  wks    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "UpperSubScreenArea.hh"

//@ Usage-Classes
#include "MessageArea.hh"
#include "MiscStrs.hh"
#include "KeyPanel.hh"
#include "Sound.hh"
#include "AlarmLogSubScreen.hh"
#include "ApneaVentilationSubScreen.hh"
#include "DiagCodeLogSubScreen.hh"
#include "DiagLogMenuSubScreen.hh"
#include "GuiFoundation.hh"
#include "HelpSubScreen.hh"
#include "MoreAlarmsSubScreen.hh"
#include "MoreDataSubScreen.hh"
#include "OperationalTimeSubScreen.hh"
#include "SafetyVentilationSubScreen.hh"
#include "SstResultsSubScreen.hh"
#include "UpperOtherScreensSubScreen.hh"
#include "VentTestSummarySubScreen.hh"
#include "VentConfigSubScreen.hh"
#include "WaveformsSubScreen.hh"
#include "AlarmLogTabButton.hh"
#include "EstResultSubScreen.hh"

#include "DevelopmentOptionsSubScreen.hh"
#include "SstLeakSubScreen.hh"
#include "SstProxSubScreen.hh"
#include "ServiceUpperScreen.hh"
#include "ServiceUpperScreenSelectArea.hh"
#include "EstResultSubScreen.hh"
#include "RmDataSubScreen.hh"
#include "SoftwareOptionsSubScreen.hh"

#include "TrendGraphsSubScreen.hh"
#include "TrendTableSubScreen.hh"
#include "EventSubScreen.hh"
//@ End-Usage

//@ Code...

// Initialize static constants.
const Int32 UpperSubScreenArea::CONTAINER_X = 3;
const Int32 UpperSubScreenArea::CONTAINER_Y = 155;
const Int32 UpperSubScreenArea::CONTAINER_WIDTH = 634;
const Int32 UpperSubScreenArea::CONTAINER_HEIGHT = 276;

// The following struct is established here holding data which is previously the member
// of class LowerSubScreenArea for one reason:  To reduce header file dependency.  
// Consequently, less memory is used for the final executable image.
//
struct USAContents
{
	USAContents(SubScreenArea *pSubScreenArea);

	// Subscreen to display the scrollable alarm log.
	AlarmLogSubScreen alarmLogSubScreen;

    // Subscreen which displays Apnea PCV settings during safety ventilation.
    ApneaVentilationSubScreen apneaVentilationSubScreen;

	// Subscreen to display the system diagnostic codes and POST error codes.
	DiagCodeLogSubScreen diagCodeLogSubScreen;

    // Subscreen to display the buttons for diagnostic logs.
    DiagLogMenuSubScreen diagLogMenuSubScreen;

	// Help Subscreen for displaying helpful information.
	HelpSubScreen helpSubScreen;

	// Subscreen to display the current alarm overflow.
	MoreAlarmsSubScreen moreAlarmsSubScreen;

	// Subscreen to display supplemental patient data.
	MoreDataSubScreen moreDataSubScreen;

	// Subscreen to display the operational times of the major system modules.
	OperationalTimeSubScreen operationalTimeSubScreen;

	// Subscreen which displays Safety PCV settings during safety ventilation.
	SafetyVentilationSubScreen safetyVentilationSubScreen;

	// Subscreen to display the SST/EST test results.
	SstResultsSubScreen sstResultsSubScreen;

	// Subscreen which contains additional subscreens.
	UpperOtherScreensSubScreen upperOtherScreensSubScreen;

	// Subscreen which displays the Ventilator Configuration subscreen.
	VentConfigSubScreen ventConfigSubScreen;

	// Subscreen which displays the Ventilator Test Summary subscreen.
	VentTestSummarySubScreen ventTestSummarySubScreen;

	// Subscreen to handle waveform setup/display.
	WaveformsSubScreen waveformsSubScreen;

    // Subscreen to display RM data
    RmDataSubScreen   rmDataSubScreen;

    // Subscreen to display trending graphs
    TrendGraphsSubScreen   trendGraphsSubScreen;

    // Subscreen to display trending table
    TrendTableSubScreen trendTableSubScreen;

    // Subscreen to display the Event Subscreen.
	EventSubScreen    eventSubScreen;



};

USAContents::USAContents(SubScreenArea *pSubScreenArea) :
	alarmLogSubScreen(pSubScreenArea),
	apneaVentilationSubScreen(pSubScreenArea),
	diagCodeLogSubScreen(pSubScreenArea),
	diagLogMenuSubScreen(pSubScreenArea),
	helpSubScreen(pSubScreenArea, &::NormalVentHelpMenu),
	moreAlarmsSubScreen(pSubScreenArea),
	moreDataSubScreen(pSubScreenArea),
	operationalTimeSubScreen(pSubScreenArea),
	safetyVentilationSubScreen(pSubScreenArea),
	sstResultsSubScreen(pSubScreenArea),
	upperOtherScreensSubScreen(pSubScreenArea),
	ventConfigSubScreen(pSubScreenArea),
	ventTestSummarySubScreen(pSubScreenArea),
	waveformsSubScreen(pSubScreenArea),
    rmDataSubScreen(pSubScreenArea),
    trendTableSubScreen(pSubScreenArea),
    trendGraphsSubScreen(pSubScreenArea),
    eventSubScreen(pSubScreenArea)

{}


struct ServiceUSAContents
{
	ServiceUSAContents(SubScreenArea *pSubScreenArea);

	// Subscreen which displays the SST Result subscreen
	SstResultsSubScreen sstResultsSubScreen;

	// Subscreen which displays the EST Result subscreen
	EstResultSubScreen estResultSubScreen;

	// Subscreen to display the logs for viewing diagnostic codes. 
	DiagCodeLogSubScreen diagCodeLogSubScreen;

	// Subscreen which displays the DiagLogMenu subscreen
	DiagLogMenuSubScreen diagLogMenuSubScreen;

	// Subscreen which displays the alarmLog subscreen
	AlarmLogSubScreen alarmLogSubScreen;

	// Subscreen which displays the Ventilator Configuration subscreen.
	VentConfigSubScreen ventConfigSubScreen;

	// Subscreen to display the operational times of the major system modules.
	OperationalTimeSubScreen operationalTimeSubScreen;

	// Subscreen which displays the Ventilator Test Summary subscreen.
	VentTestSummarySubScreen ventTestSummarySubScreen;

	// Subscreen which displays the Sst Leak test graph.
	SstLeakSubScreen sstLeakSubScreen;

	// Subscreen which displays the Sst PROX test data.
	SstProxSubScreen sstProxSubScreen;

	// Subscreen which displays the Help screen.
	HelpSubScreen helpSubScreen;

	// Subscreen which displays the Development Options screen.
	DevelopmentOptionsSubScreen developmentOptionsSubScreen;

	// Subscreen to display Software Options Subscreen
    SoftwareOptionsSubScreen   softwareOptionsSubScreen;

};

ServiceUSAContents::ServiceUSAContents(SubScreenArea *pSubScreenArea) :
			sstResultsSubScreen(pSubScreenArea),
			estResultSubScreen(pSubScreenArea),
			diagCodeLogSubScreen(pSubScreenArea),
			diagLogMenuSubScreen(pSubScreenArea),
			alarmLogSubScreen(pSubScreenArea),
			ventConfigSubScreen(pSubScreenArea),
			operationalTimeSubScreen(pSubScreenArea),
			ventTestSummarySubScreen(pSubScreenArea),
			sstLeakSubScreen(pSubScreenArea),
			sstProxSubScreen(pSubScreenArea),
			helpSubScreen(pSubScreenArea, &::NormalVentHelpMenu),
			developmentOptionsSubScreen(pSubScreenArea),
            softwareOptionsSubScreen(pSubScreenArea)

{}


#define  SIZEOF_CONTENTS  ((sizeof(USAContents) > sizeof(ServiceUSAContents)) \
							? sizeof(USAContents) : sizeof(ServiceUSAContents))

static Uint PContentsMemory_[(SIZEOF_CONTENTS + 
									sizeof(Uint) - 1) / sizeof(Uint)];

static USAContents *const PUSAContents_ = 
							(USAContents *)PContentsMemory_;

static ServiceUSAContents *const PServiceUSAContents_ = 
							(ServiceUSAContents *)PContentsMemory_;


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: UpperSubScreenArea()  [Default Constructor]
//
//@ Interface-Description
// Default constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
// Initializes colors; sizes, and position of this Container object.
// Registers for off-screen key event callbacks for keys which affect
// this subscreen area.
// $[01192] The user shall have access to the diagnostic code log ...
// $[01205] The user shall have access to the SST result log ...
// $[01328] On startup following a power fail recovery, the upper screen ...
// $[01346] The help subscreen displays quick reference information for ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

UpperSubScreenArea::UpperSubScreenArea(void) 
{
	CALL_TRACE("UpperSubScreenArea::UpperSubScreenArea(void)");

    // Size and position the area
    setX(CONTAINER_X);
    setY(CONTAINER_Y);
    setWidth(CONTAINER_WIDTH);
    setHeight(CONTAINER_HEIGHT);

	const SigmaState  GUI_STATE = GuiApp::GetGuiState();

	switch (GUI_STATE)
	{
	case ::STATE_INOP :
	case ::STATE_TIMEOUT :
	case ::STATE_ONLINE : // $[TI1.1]
		new (::PUSAContents_) USAContents(this);
		break;

	case ::STATE_SST :
	case ::STATE_SERVICE : // $[TI1.2]
		new (::PServiceUSAContents_) ServiceUSAContents(this);
		break;

	case ::STATE_START :
	case ::STATE_INIT :
	case ::STATE_UNKNOWN :
	case ::STATE_FAILED :
	default : // $[TI1.3]
		// unexpected state...
		AUX_CLASS_ASSERTION_FAILURE(GUI_STATE);
		break;
	}

    // Default background color is medium grey
    setFillColor(Colors::MEDIUM_BLUE);

	// Register ourselves as the target for INFO key events.
	KeyPanel::SetCallback(KeyPanel::INFO_KEY, this);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~UpperSubScreenArea  [Destructor]
//
//@ Interface-Description
// Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  n/a
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

UpperSubScreenArea::~UpperSubScreenArea(void)
{
	CALL_TRACE("UpperSubScreenArea::~UpperSubScreenArea(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: keyPanelPressHappened [public, virtual]
//
//@ Interface-Description
// Key press event handler for the Info (help) key.  Activates/deactivates
// the Help subscreen depending on whether it's displayed or not.
//---------------------------------------------------------------------
//@ Implementation-Description
// If the Help subscreen is not displayed, then display it.  Otherwise dismiss
// it.
//
// $[01068] Info key is part of Upper Screen Select Area mutually-excl group
// $[01210] If Help screen is not displayed display it, if it is displayed ...
//---------------------------------------------------------------------
//@ PreCondition
//  The keyId must be INFO_KEY.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
UpperSubScreenArea::keyPanelPressHappened(KeyPanel::KeyId keyId)
{
	CALL_TRACE("UpperSubScreenArea::keyPanelPressHappened(keyId)");

	CLASS_ASSERTION(keyId == KeyPanel::INFO_KEY);

	// Display the offscreen key help message 
	GuiApp::PMessageArea->setMessage(
								MiscStrs::HELP_SCREEN_HELP_MESSAGE,
										MessageArea::MA_HIGH);
	
	if (GuiApp::GetGuiState() == STATE_ONLINE)
	{														// $[TI6]
		if (isCurrentSubScreen(&(PUSAContents_->helpSubScreen)))
		{													// $[TI1]
			deactivateSubScreen();
		}
		else
		{													// $[TI2]
			activateSubScreen(&(PUSAContents_->helpSubScreen), NULL);		// $[01068]
		}
	}
	else if (GuiApp::GetGuiState() == STATE_SST)
 	{
		if (isCurrentSubScreen(&(PServiceUSAContents_->helpSubScreen)))
		{													// $[TI3]
			activateSubScreen(&(PServiceUSAContents_->sstResultsSubScreen), NULL);
		}
		else
		{													// $[TI4]
			if (isCurrentSubScreen(&(PServiceUSAContents_->sstResultsSubScreen)))
			{
				activateSubScreen(&(PServiceUSAContents_->helpSubScreen), NULL);
			}
		}
	}
	else 
 	{														// $[TI5]
		// Illegal key press during service mode
		Sound::Start(Sound::INVALID_ENTRY);
 	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: keyPanelReleaseHappened
//
//@ Interface-Description
// Called when the Info key is released.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Simply turn off the offscreen help message.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
UpperSubScreenArea::keyPanelReleaseHappened(KeyPanel::KeyId keyId)
{
	CALL_TRACE("UpperSubScreenArea::keyPanelReleaseHappened(KeyPanel::KeyId)");

	CLASS_ASSERTION(keyId == KeyPanel::INFO_KEY);
	//								 $[TI1]
	// Clear the off screen key message
	GuiApp::PMessageArea->clearMessage(MessageArea::MA_HIGH);

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activateSafetyVentilationSubScreen
//
//@ Interface-Description
// Displays the SafetyVentilationSubScreen as the "default" subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Set the 'safetyVentilationSubScreen' as the default subscreen for
// the subscreen area, then display it.  The subscreen area
// automatically displays the "default" subscreen when no other
// user-selected subscreen is being displayed.
//
// $[01148] Safety PCV screen must be displayed automatically if ...
// $[01225] When the vent goes to Safety PCV the GUI shall ...
// $[01226] The applicable GUI functions shall still be available ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
UpperSubScreenArea::activateSafetyVentilationSubScreen(void)
{
	CALL_TRACE("activateSafetyVentilationSubScreen(void)");

	setDefaultSubScreen(&(PUSAContents_->safetyVentilationSubScreen));
	activateDefaultSubScreen();
	// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: deactivateSafetyVentilationSubScreen
//
//@ Interface-Description
// Hide the SafetyVentilationSubScreen, if it is currently displayed.
//---------------------------------------------------------------------
//@ Implementation-Description
// Nullify the default subscreen for the subscreen area.  If the
// SafetyVentilationSubScreen (the only default subscreen) is currently
// displayed, then dismiss it.
//
// $[01227] The GUI shall remove the Safety PCV subscreen as soon as ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
UpperSubScreenArea::deactivateSafetyVentilationSubScreen(void)
{
	CALL_TRACE("deactivateSafetyVentilationSubScreen(void)");

    // Clear it from the default
    setDefaultSubScreen(NULL);
 
    // Deactivate only if the safety subscreen is currently displayed.
    if (isCurrentSubScreen(&(PUSAContents_->safetyVentilationSubScreen)))
    {                                                   // $[TI1]
        deactivateSubScreen();
		restorePreviousSubScreen();
    }                                                   // $[TI2]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activateApneaVentilationSubScreen
//
//@ Interface-Description
// Displays the ApneaVentilationSubScreen as the "default" subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Set the 'apneaVentilationSubScreen' as the default subscreen for
// the subscreen area, then display it.  The subscreen area
// automatically displays the "default" subscreen when no other
// user-selected subscreen is being displayed.
// $[01264] If the apnea in progress subscreen is displaced by ...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
UpperSubScreenArea::activateApneaVentilationSubScreen(void)
{
	CALL_TRACE("activateApneaVentilationSubScreen(void)");

    // Make Apnea Vent the new default sub-screen and activate it
    setDefaultSubScreen(&(PUSAContents_->apneaVentilationSubScreen));
 
    activateDefaultSubScreen();                         // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: deactivateApneaVentilationSubScreen
//
//@ Interface-Description
// Hide the ApneaVentilationSubScreen, if it is currently displayed.
//---------------------------------------------------------------------
//@ Implementation-Description
// Nullify the default subscreen for the subscreen area.  If the
// ApneaVentilationSubScreen (the only default subscreen) is currently
// displayed, then dismiss it.
//
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
void
UpperSubScreenArea::deactivateApneaVentilationSubScreen(void)
{
    CALL_TRACE("deactivateApneaVentilationSubScreen(void)");
 
    // Clear it from the default
    setDefaultSubScreen(NULL);
 
    // Deactivate only if the apnea subscreen is currently displayed.
    if (isCurrentSubScreen(&(PUSAContents_->apneaVentilationSubScreen)))
    {                                                   // $[TI1]
        deactivateSubScreen();
		restorePreviousSubScreen();
    }                                                   // $[TI2]
}                                                                         

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetAlarmLogSubScreen
//
//@ Interface-Description
// Returns a pointer to the Alarm Log subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Returns the address of alarmLogSubScreen.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

 AlarmLogSubScreen* 
UpperSubScreenArea::GetAlarmLogSubScreen(void)
{
	CALL_TRACE("UpperSubScreenArea::GetAlarmLogSubScreen(void)");

	if (GuiApp::GetGuiState() == STATE_SERVICE )
	{								  	// $[TI1]
	  	return (&(PServiceUSAContents_->alarmLogSubScreen));		
	}
	else
	{									// $[TI2]
		return (&(PUSAContents_->alarmLogSubScreen));
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetDiagCodeLogSubScreen
//
//@ Interface-Description
// Returns a pointer to the Diagnostics Code Log subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Returns the address of diagCodeLogSubScreen.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

 DiagCodeLogSubScreen* 
UpperSubScreenArea::GetDiagCodeLogSubScreen(void)
{
	CALL_TRACE("UpperSubScreenArea::GetDiagCodeLogSubScreen(void)");

	if (GuiApp::GetGuiState() == STATE_SERVICE)
	{									  // $[TI1]
		return (&(PServiceUSAContents_->diagCodeLogSubScreen));	
	}
	else								  // $[TI2]
	{
		return (&(PUSAContents_->diagCodeLogSubScreen));
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetDiagLogMenuSubScreen
//
//@ Interface-Description
// Returns a pointer to the Diagnostics Log Menu subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Returns the address of diagLogMenuSubScreen.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
 DiagLogMenuSubScreen* 
UpperSubScreenArea::GetDiagLogMenuSubScreen(void)
{
	CALL_TRACE("UpperSubScreenArea::GetDiagLogMenuSubScreen(void)");

	if (GuiApp::GetGuiState() == STATE_SERVICE)
	{		     // $[TI1]
		return (&(PServiceUSAContents_->diagLogMenuSubScreen));  
	}
	else 
	{			// $[TI2]
		return (&(PUSAContents_->diagLogMenuSubScreen));               
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetHelpSubScreen
//
//@ Interface-Description
// Returns a pointer to the Help subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Returns the address of helpSubScreen.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

 HelpSubScreen* 
UpperSubScreenArea::GetHelpSubScreen(void)
{
	CALL_TRACE("UpperSubScreenArea::GetHelpSubScreen(void)");

	if (GuiApp::GetGuiState() == STATE_SERVICE  || GuiApp::GetGuiState() == STATE_SST)
	{		    // $[TI1]

		return (&(PServiceUSAContents_->helpSubScreen));	
	}
	else
	{		    // $[TI2]
		return (&(PUSAContents_->helpSubScreen));							// $[TI1]
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetMoreAlarmsSubScreen
//
//@ Interface-Description
// Returns a pointer to the More Alarms subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Returns the address of moreAlarmsSubScreen.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

 MoreAlarmsSubScreen* 
UpperSubScreenArea::GetMoreAlarmsSubScreen(void)
{
	CALL_TRACE("UpperSubScreenArea::GetMoreAlarmsSubScreen(void)");

	// More Alarms is a Normal screen
	SAFE_CLASS_ASSERTION(GuiApp::GetGuiState() == STATE_ONLINE);

	return (&(PUSAContents_->moreAlarmsSubScreen));						// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetMoreDataSubScreen
//
//@ Interface-Description
// Returns a pointer to the More Data subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Returns the address of moreDataSubScreen.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

 MoreDataSubScreen* 
UpperSubScreenArea::GetMoreDataSubScreen(void)
{
	CALL_TRACE("UpperSubScreenArea::GetMoreDataSubScreen(void)");

	SAFE_CLASS_ASSERTION(GuiApp::GetGuiState() == STATE_ONLINE);

	return (&(PUSAContents_->moreDataSubScreen));						// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetOperationalTimeSubScreen
//
//@ Interface-Description
// Returns a pointer to the Operational Time subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Returns the address of operationalTimeSubScreen.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

 OperationalTimeSubScreen* 
UpperSubScreenArea::GetOperationalTimeSubScreen(void)
{
	CALL_TRACE("UpperSubScreenArea::GetOperationalTimeSubScreen(void)");

	if (GuiApp::GetGuiState() == STATE_SERVICE)
	{		// $[TI1]
		return (&(PServiceUSAContents_->operationalTimeSubScreen));	
	}
	else
	{	    // $[TI2]
		return (&(PUSAContents_->operationalTimeSubScreen));			
	}			
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetSstResultsSubScreen
//
//@ Interface-Description
// Returns a pointer to the Test Result subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Returns the address of sstResultsSubScreen.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

 SstResultsSubScreen* 
UpperSubScreenArea::GetSstResultsSubScreen(void)
{
	CALL_TRACE("UpperSubScreenArea::GetSstResultsSubScreen(void)");

	if (GuiApp::GetGuiState() == STATE_SERVICE  || GuiApp::GetGuiState() == STATE_SST)
	{		// $[TI1]
		return (&(PServiceUSAContents_->sstResultsSubScreen));		
	}
	else
	{	   // $[TI2]
	 	return (&(PUSAContents_->sstResultsSubScreen));
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetUpperOtherScreensSubScreen
//
//@ Interface-Description
// Returns a pointer to the Upper Other Screens subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Returns the address of upperOtherScreenSubScreen.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

 UpperOtherScreensSubScreen * 
UpperSubScreenArea::GetUpperOtherScreensSubScreen(void)
{
	CALL_TRACE("UpperSubScreenArea::GetUpperOtherScreensSubScreen(void)");

	SAFE_CLASS_ASSERTION(GuiApp::GetGuiState() == STATE_ONLINE);

	return (&(PUSAContents_->upperOtherScreensSubScreen));				// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetVentConfigSubScreen
//
//@ Interface-Description
// Returns a pointer to the Ventilator Configuration subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Returns the address of ventConfigSubScreen.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

 VentConfigSubScreen* 
UpperSubScreenArea::GetVentConfigSubScreen(void)
{
	CALL_TRACE("UpperSubScreenArea::GetVentConfigSubScreen(void)");

	if (GuiApp::GetGuiState() == STATE_SERVICE)
	{		// $[TI1]
		return (&(PServiceUSAContents_->ventConfigSubScreen));	
	}
	else
	{	    // $[TI2]
		return (&(PUSAContents_->ventConfigSubScreen));			
	}	
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetVentTestSummarySubScreen
//
//@ Interface-Description
// Returns a pointer to the Ventilator Test Summary subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Returns the address of ventTestSummarySubScreen.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

 VentTestSummarySubScreen* 
UpperSubScreenArea::GetVentTestSummarySubScreen(void)
{
	CALL_TRACE("UpperSubScreenArea::GetVentTestSummarySubScreen(void)");

	if (GuiApp::GetGuiState() == STATE_SERVICE)
	{	// $[TI1]

		return (&(PServiceUSAContents_->ventTestSummarySubScreen));	
	}
	else
	{   // $[TI2]
		return (&(PUSAContents_->ventTestSummarySubScreen));
	}			
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetWaveformsSubScreen
//
//@ Interface-Description
// Returns a pointer to the Waveforms subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Returns the address of waveformsSubScreen.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

 WaveformsSubScreen*
UpperSubScreenArea::GetWaveformsSubScreen(void)
{
	CALL_TRACE("UpperSubScreenArea::GetWaveformsSubScreen(void)");

	SAFE_CLASS_ASSERTION(GuiApp::GetGuiState() == STATE_ONLINE);

	return (&(PUSAContents_->waveformsSubScreen));						// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetTrendGraphsSubScreen
//
//@ Interface-Description
// Returns a pointer to the Trends subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Returns the address of  TrendGraphsSubScreen.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

 TrendGraphsSubScreen*
UpperSubScreenArea::GetTrendGraphsSubScreen(void)
{
	CALL_TRACE("UpperSubScreenArea::GetTrendSubScreen(void)");

	SAFE_CLASS_ASSERTION(GuiApp::GetGuiState() == STATE_ONLINE);

	return (&(PUSAContents_->trendGraphsSubScreen));						// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetTrendTableSubScreen
//
//@ Interface-Description
// Returns a pointer to the Trend Table subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Returns the address of Trend Table subscreen.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

 TrendTableSubScreen*
UpperSubScreenArea::GetTrendTableSubScreen(void)
{
	CALL_TRACE("UpperSubScreenArea::GetTrendsSubScreen(void)");

	SAFE_CLASS_ASSERTION(GuiApp::GetGuiState() == STATE_ONLINE);

	return (&(PUSAContents_->trendTableSubScreen));						// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetEventSubScreen
//
//@ Interface-Description
// Returns a pointer to the Event subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Returns the address of eventSubScreen.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

EventSubScreen*
UpperSubScreenArea::GetEventSubScreen(void)
{
	CALL_TRACE("UpperSubScreenArea::GetEventSubScreen(void)");

	SAFE_CLASS_ASSERTION(GuiApp::GetGuiState() == STATE_ONLINE);

	return (&(PUSAContents_->eventSubScreen));						// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetApneaVentilationSubScreen
//
//@ Interface-Description
// Returns a pointer to the ApneaVentilation subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Returns the address of apneaVentilationSubScreen.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

 ApneaVentilationSubScreen*
UpperSubScreenArea::GetApneaVentilationSubScreen(void)
{
	CALL_TRACE("UpperSubScreenArea::GetApneaVentilationSubScreen(void)");

	SAFE_CLASS_ASSERTION(GuiApp::GetGuiState() == STATE_ONLINE);

	return (&(PUSAContents_->apneaVentilationSubScreen));				// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetSafetyVentilationSubScreen
//
//@ Interface-Description
// Returns a pointer to the SafetyVentilation subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Returns the address of safetyVentilationSubScreen.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

 SafetyVentilationSubScreen*
UpperSubScreenArea::GetSafetyVentilationSubScreen(void)
{
	CALL_TRACE("UpperSubScreenArea::GetSafetyVentilationSubScreen(void)");

	SAFE_CLASS_ASSERTION(GuiApp::GetGuiState() == STATE_ONLINE);

	return (&(PUSAContents_->safetyVentilationSubScreen));						// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateVentTestSummaryScreen
//
//@ Interface-Description
//  Disable the access to VentTestSummarySubScreen.
//---------------------------------------------------------------------
//@ Implementation-Description
//  If the currently displayed subScreen is VentTestSummarySubScreen
//  then will need to refresh the display
//  accordingly.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
UpperSubScreenArea::updateVentTestSummaryScreen(void)
{
	CALL_TRACE("UpperSubScreenArea::updateVentTestSummaryScreen(void)");

	SAFE_CLASS_ASSERTION(GuiApp::GetGuiState() == STATE_SERVICE);

	if (isCurrentSubScreen(&(PServiceUSAContents_->ventTestSummarySubScreen)))
	{													// $[TI1]
		TabButton *pTabButton;

		pTabButton = ServiceUpperScreen::RServiceUpperScreen.getServiceUpperScreenSelectArea()->
											getVentTestSummaryTabButton();
		
		// Deactivate/activate the date/time screen.
		deactivateSubScreen();

		// Activate the vent test subscreen and force the tab button down.
		activateSubScreen(&(PServiceUSAContents_->ventTestSummarySubScreen), pTabButton);
	}													// $[TI2]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetEstResultSubScreen
//
//@ Interface-Description
// Returns a pointer to the Lower Subscreen Area's EstResult subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply returns the address of alarmSetupSubScreen
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

EstResultSubScreen  *
UpperSubScreenArea::GetEstResultSubScreen(void)
{
	CALL_TRACE("UpperSubScreenArea::GetEstResultSubScreen(void)");

	SAFE_CLASS_ASSERTION(GuiApp::GetGuiState() == STATE_SERVICE);

	return (&(PServiceUSAContents_->estResultSubScreen));						// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetSstLeakSubScreen
//
//@ Interface-Description
// Returns a pointer to the SstLeak subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Returns the address of ventTestSummarySubScreen.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SstLeakSubScreen* 
UpperSubScreenArea::GetSstLeakSubScreen(void)
{
	CALL_TRACE("UpperSubScreenArea::GetSstLeakSubScreen(void)");

	SAFE_CLASS_ASSERTION(GuiApp::GetGuiState() == STATE_SERVICE);

	return (&(PServiceUSAContents_->sstLeakSubScreen));				// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetSstProxSubScreen
//
//@ Interface-Description
// Returns a pointer to the SstProx subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Returns the address of SstProxSubScreen.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SstProxSubScreen* 
UpperSubScreenArea::GetSstProxSubScreen(void)
{
	CALL_TRACE("UpperSubScreenArea::GetSstProxSubScreen(void)");

	SAFE_CLASS_ASSERTION(GuiApp::GetGuiState() == STATE_SERVICE);

	return (&(PServiceUSAContents_->sstProxSubScreen));				// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetDevelopmentOptionsSubScreen
//
//@ Interface-Description
// Returns a pointer to the Development Options subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Returns the address of developmentOptionsSubScreen.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

DevelopmentOptionsSubScreen* 
UpperSubScreenArea::GetDevelopmentOptionsSubScreen(void)
{
	CALL_TRACE("UpperSubScreenArea::GetDevelopmentOptionsSubScreen(void)");

	SAFE_CLASS_ASSERTION(GuiApp::GetGuiState() == STATE_SERVICE);

	return (&(PServiceUSAContents_->developmentOptionsSubScreen));							// $[TI1]
}




//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetRmDataSubScreen
//
//@ Interface-Description
// Returns a pointer to the RM Data subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Returns the address of RM Data subscreen.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
RmDataSubScreen*
UpperSubScreenArea::GetRmDataSubScreen(void)
{
    CALL_TRACE("UpperSubScreenArea::GetRmDataSubScreen(void)");

    SAFE_CLASS_ASSERTION(GuiApp::GetGuiState() == STATE_ONLINE);

    return (&(PUSAContents_->rmDataSubScreen));                     // $[TI1]
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetSoftwareOptionsSubScreen
//
//@ Interface-Description
// Returns a pointer to the Software Options Subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Returns the address of Software Options Subscreen.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
SoftwareOptionsSubScreen*
UpperSubScreenArea::GetSoftwareOptionsSubScreen(void)
{
    CALL_TRACE("UpperSubScreenArea::GetSoftwareOptionsSubScreen(void)");

    SAFE_CLASS_ASSERTION(GuiApp::GetGuiState() == STATE_SERVICE);

    return (&(PServiceUSAContents_->softwareOptionsSubScreen));                     // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
UpperSubScreenArea::SoftFault(const SoftFaultID  softFaultID,
							  const Uint32       lineNumber,
							  const char*        pFileName,
							  const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, UPPERSUBSCREENAREA,
							lineNumber, pFileName, pPredicate);
}

