// GuiIOServer.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "GuiIOServer.h"
#include "WinDef.h"
#include "Socket.h"
#include "CommandResponse.h"
#include "Parser.h"

static Socket m_socket;

GUIIOSERVER_API GuiIOError GuiIOGetDllVersion(INT* ver)
{
	*ver = GUIIO_DLL_VERSION;
	return GUIIO_SUCCESS;
}

GUIIOSERVER_API GuiIOError GuiIOConnect(const CHAR* addr, UINT port)
{
	if (!m_socket.Connect(addr, port))
		return GUIIO_ERROR;
	return GUIIO_SUCCESS;
}

GUIIOSERVER_API GuiIOError GuiIODisconnect()
{
	Parser parser;
	parser.BuildBye();
	m_socket.Send(parser.GetBuiltMsg());
	m_socket.Close();
	return GUIIO_SUCCESS;
}

GUIIOSERVER_API GuiIOError GuiIOReceiveUIOutput(int timeout, GuiIOUIOutputResp* out)
{
	CommandResponse cr(&m_socket);

	CHAR inBuf[Parser::MAX_MSG_LENGTH];
	int bytes = cr.Receive(inBuf, Parser::MAX_MSG_LENGTH, timeout);
	if (bytes <= 0)
		return GUIIO_TIMEOUT;

	// parse the XML response
	QGuiIOActionRespEvent action;
	Parser parser;
	if (parser.ParseCmd(inBuf, Parser::MAX_MSG_LENGTH, 0, action) == -1)
		return GUIIO_ERROR;	

	// copy the parsed result into the caller's return structure
	*out = action.msg.uiOutputResp;

	return GUIIO_SUCCESS;
}

GUIIOSERVER_API GuiIOError GuiIOCancelReceiveUIOutput()
{
	CommandResponse::CancelReceive();
	return GUIIO_SUCCESS;
}

GUIIOSERVER_API GuiIOError GuiIOMousePos(int x, int y)
{
	CommandResponse cr(&m_socket);
	Parser parser;
	parser.BuildMousePos(x, y);
	if (!cr.Send(parser.GetBuiltMsg()))
		return GUIIO_ERROR;
	return GUIIO_SUCCESS;
}

GUIIOSERVER_API GuiIOError GuiIOQueryXY(const CHAR* objectId, GuiIOQueryXYResp* out)
{
	if (out == NULL)
		return GUIIO_ERROR;

	// create a new command/response instance using the current socket
	CommandResponse cr(&m_socket);

	// create XML command message
	Parser parser;
	int transactionId = parser.BuildQueryXY(objectId);

	// send the message and receive a response
	CHAR inBuf[Parser::MAX_MSG_LENGTH];
	if (cr.SendReceive(parser.GetBuiltMsg(), inBuf, Parser::MAX_MSG_LENGTH) == -1)
		return GUIIO_ERROR;

	// parse the XML response
	QGuiIOActionRespEvent action;
	if (parser.ParseCmd(inBuf, Parser::MAX_MSG_LENGTH, 0, action) == -1)
		return GUIIO_ERROR;	

	// error check response
	if (action.header.transactionId != transactionId)
		return GUIIO_INVALID_TRANSACTION_ID;
	else if (action.responseType == QGuiIOActionRespEvent::ACK_NAK_RESP)
		return static_cast<GuiIOError>(action.msg.ackNakResp.error);
	else if (action.responseType != QGuiIOActionRespEvent::QUERY_XY_RESP)
		return GUIIO_WRONG_MESSAGE_RECEIVED;

	// copy the parsed result into the caller's return structure
	*out = action.msg.queryXYResp;
	return GUIIO_SUCCESS;
}

GUIIOSERVER_API GuiIOError GuiIOQueryWH(const CHAR* objectId, GuiIOQueryWHResp* out)
{
	if (out == NULL)
		return GUIIO_ERROR;

	// create a new command/response instance using the current socket
	CommandResponse cr(&m_socket);

	// create XML command message
	Parser parser;
	INT transactionId = parser.BuildQueryWH(objectId);

	// send the message and receive a response
	CHAR inBuf[Parser::MAX_MSG_LENGTH];
	if (cr.SendReceive(parser.GetBuiltMsg(), inBuf, Parser::MAX_MSG_LENGTH) == -1)
		return GUIIO_ERROR;

	// parse the XML response
	QGuiIOActionRespEvent action;
	if (parser.ParseCmd(inBuf, Parser::MAX_MSG_LENGTH, 0, action) == -1)
		return GUIIO_ERROR;	

	// error check response
	if (action.header.transactionId != transactionId)
		return GUIIO_INVALID_TRANSACTION_ID;
	else if (action.responseType == QGuiIOActionRespEvent::ACK_NAK_RESP)
		return static_cast<GuiIOError>(action.msg.ackNakResp.error);
	else if (action.responseType != QGuiIOActionRespEvent::QUERY_WH_RESP)
		return GUIIO_WRONG_MESSAGE_RECEIVED;

	// copy the parsed result into the caller's return structure
	*out = action.msg.queryWHResp;
	return GUIIO_SUCCESS;
}

GUIIOSERVER_API GuiIOError GuiIOGetProperties(const CHAR* objectId, GuiIOGetPropertiesResp* out)
{
	return GuiIOGetTableProperties(objectId, -1, -1, out);
}

GUIIOSERVER_API GuiIOError GuiIOGetTableProperties(const CHAR* objectId, int row, int col, GuiIOGetPropertiesResp* out)
{
	if (out == NULL)
		return GUIIO_ERROR;

	// create a new command/response instance using the current socket
	CommandResponse cr(&m_socket);

	// create XML command message
	Parser parser;
	INT transactionId = parser.BuildGetProperties(objectId, row, col);

	// send the message and receive a response
	CHAR inBuf[Parser::MAX_MSG_LENGTH];
	if (cr.SendReceive(parser.GetBuiltMsg(), inBuf, Parser::MAX_MSG_LENGTH) == -1)
		return GUIIO_ERROR;

	// parse the XML response
	QGuiIOActionRespEvent action;
	if (parser.ParseCmd(inBuf, Parser::MAX_MSG_LENGTH, 0, action) == -1)
		return GUIIO_ERROR;	

	// error check response
	if (action.header.transactionId != transactionId)
		return GUIIO_INVALID_TRANSACTION_ID;
	else if (action.responseType == QGuiIOActionRespEvent::ACK_NAK_RESP)
		return static_cast<GuiIOError>(action.msg.ackNakResp.error);
	else if (action.responseType != QGuiIOActionRespEvent::GET_PROPERTIES_RESP)
		return GUIIO_WRONG_MESSAGE_RECEIVED;

	// copy the parsed result into the caller's return structure
	*out = action.msg.getPropertiesResp;
	return GUIIO_SUCCESS;
}

GUIIOSERVER_API GuiIOError GuiIOGetObjectValue(const CHAR* objectId, GuiIOGetObjectValueResp* out)
{
	return GuiIOGetTableCellObjectValue(objectId, -1, -1, out);
}

GUIIOSERVER_API GuiIOError GuiIOGetTableCellObjectValue(const CHAR* objectId, int row, int col, GuiIOGetObjectValueResp* out)
{
	if (out == NULL)
		return GUIIO_ERROR;

	// create a new command/response instance using the current socket
	CommandResponse cr(&m_socket);

	// create XML command message
	Parser parser;
	INT transactionId = parser.BuildGetObjectValue(objectId, row, col);

	// send the message and receive a response
	CHAR inBuf[Parser::MAX_MSG_LENGTH];
	if (cr.SendReceive(parser.GetBuiltMsg(), inBuf, Parser::MAX_MSG_LENGTH) == -1)
		return GUIIO_ERROR;

	// parse the XML response
	QGuiIOActionRespEvent action;
	if (parser.ParseCmd(inBuf, Parser::MAX_MSG_LENGTH, 0, action) == -1)
		return GUIIO_ERROR;	

	// error check response
	if (action.header.transactionId != transactionId)
		return GUIIO_INVALID_TRANSACTION_ID;
	else if (action.responseType == QGuiIOActionRespEvent::ACK_NAK_RESP)
		return static_cast<GuiIOError>(action.msg.ackNakResp.error);
	else if (action.responseType != QGuiIOActionRespEvent::GET_OBJECT_VALUE_RESP)
		return GUIIO_WRONG_MESSAGE_RECEIVED;

	// copy the parsed result into the caller's return structure
	*out = action.msg.getObjectValueResp;
	return GUIIO_SUCCESS;
}

GUIIOSERVER_API GuiIOError GuiIOGetTableRowObjectValue(const CHAR* objectId, int row, int totalRowCols, GuiIOGetObjectValueResp out[])
{
	if (out == NULL)
		return GUIIO_ERROR;

	// create a new command/response instance using the current socket
	CommandResponse cr(&m_socket);

	// create XML command message. A col of -1 means get all col widgets in the entire row.
	Parser parser;
	INT transactionId = parser.BuildGetObjectValue(objectId, row, -1);
	INT bufIdx = 0;
	INT bytesReceived=0;

	for (int col = 0; col<totalRowCols; col++)
	{
		CHAR inBuf[Parser::MAX_MSG_LENGTH];
		if (col == 0)
		{
			// send the message and receive response. The response will include all 
			// column widget object values.
			bytesReceived = cr.SendReceive(parser.GetBuiltMsg(), inBuf, Parser::MAX_MSG_LENGTH);
			if (bytesReceived == -1)
				return GUIIO_ERROR;
		}

		// parse the XML response
		QGuiIOActionRespEvent action;
		INT bytesParsed = parser.ParseCmd(&inBuf[bufIdx], Parser::MAX_MSG_LENGTH, 0, action);
		if (bytesParsed == -1)
		{
			// the entire message may come in more than one packet, if so lets try to get more bytes
			// if the parser fails and append to incoming buffer
			INT bytes = cr.Receive(&inBuf[bytesReceived], Parser::MAX_MSG_LENGTH - bytesReceived, 500);
			if (bytes==-1)
				return GUIIO_ERROR;	

			bytesReceived += bytes;

			// try parsing the XML response one more time with the additional bytes
			bytesParsed = parser.ParseCmd(&inBuf[bufIdx], Parser::MAX_MSG_LENGTH, 0, action);
			if (bytesParsed == -1)
				return GUIIO_ERROR;
		}

		// error check response
		if (action.header.transactionId != transactionId)
			return GUIIO_INVALID_TRANSACTION_ID;
		else if (action.responseType == QGuiIOActionRespEvent::ACK_NAK_RESP)
			return static_cast<GuiIOError>(action.msg.ackNakResp.error);
		else if (action.responseType != QGuiIOActionRespEvent::GET_OBJECT_VALUE_RESP)
			return GUIIO_WRONG_MESSAGE_RECEIVED;

		// copy the parsed result into the caller's return structure
		out[col] = action.msg.getObjectValueResp;

		// increment the buffer index to parse the other col object value responses
		bufIdx += bytesParsed;
	}

	return GUIIO_SUCCESS;
}

GUIIOSERVER_API GuiIOError GuiIOGetFocus(GuiIOGetFocusResp* out)
{
	if (out == NULL)
		return GUIIO_ERROR;

	// create a new command/response instance using the current socket
	CommandResponse cr(&m_socket);

	// create XML command message
	Parser parser;
	INT transactionId = parser.BuildGetFocus();

	// send the message and receive a response
	CHAR inBuf[Parser::MAX_MSG_LENGTH];
	if (cr.SendReceive(parser.GetBuiltMsg(), inBuf, Parser::MAX_MSG_LENGTH) == -1)
		return GUIIO_ERROR;

	// parse the XML response
	QGuiIOActionRespEvent action;
	if (parser.ParseCmd(inBuf, Parser::MAX_MSG_LENGTH, 0, action) == -1)
		return GUIIO_ERROR;	

	// error check response
	if (action.header.transactionId != transactionId)
		return GUIIO_INVALID_TRANSACTION_ID;
	else if (action.responseType == QGuiIOActionRespEvent::ACK_NAK_RESP)
		return static_cast<GuiIOError>(action.msg.ackNakResp.error);
	else if (action.responseType != QGuiIOActionRespEvent::GET_FOCUS_RESP)
		return GUIIO_WRONG_MESSAGE_RECEIVED;

	// copy the parsed result into the caller's return structure
	*out = action.msg.getFocusResp;
	return GUIIO_SUCCESS;
}

GUIIOSERVER_API GuiIOError GuiIOUIOutputOnOff(bool onOff)
{
	// create a new command/response instance using the current socket
	CommandResponse cr(&m_socket);

	// create XML command message
	Parser parser;
	INT transactionId = parser.BuildUIOutputOnOff(onOff);

	// send the message and receive a response
	CHAR inBuf[Parser::MAX_MSG_LENGTH];
	if (cr.SendReceive(parser.GetBuiltMsg(), inBuf, Parser::MAX_MSG_LENGTH) == -1)
		return GUIIO_ERROR;

	// parse the XML response
	QGuiIOActionRespEvent action;
	if (parser.ParseCmd(inBuf, Parser::MAX_MSG_LENGTH, 0, action) == -1)
		return GUIIO_ERROR;

	// error check response
	if (action.header.transactionId != transactionId)
		return GUIIO_INVALID_TRANSACTION_ID;
	else if (action.responseType != QGuiIOActionRespEvent::ACK_NAK_RESP)
		return GUIIO_WRONG_MESSAGE_RECEIVED;

	return GUIIO_SUCCESS;
}

GUIIOSERVER_API GuiIOError GuiIOCursor(bool onOff)
{
	// create a new command/response instance using the current socket
	CommandResponse cr(&m_socket);

	// create XML command message
	Parser parser;
	INT transactionId = parser.BuildCursor(onOff);

	// send the message and receive a response
	CHAR inBuf[Parser::MAX_MSG_LENGTH];
	if (cr.SendReceive(parser.GetBuiltMsg(), inBuf, Parser::MAX_MSG_LENGTH) == -1)
		return GUIIO_ERROR;

	// parse the XML response
	QGuiIOActionRespEvent action;
	if (parser.ParseCmd(inBuf, Parser::MAX_MSG_LENGTH, 0, action) == -1)
		return GUIIO_ERROR;

	// error check response
	if (action.header.transactionId != transactionId)
		return GUIIO_INVALID_TRANSACTION_ID;
	else if (action.responseType != QGuiIOActionRespEvent::ACK_NAK_RESP)
		return GUIIO_WRONG_MESSAGE_RECEIVED;

	return GUIIO_SUCCESS;
}

GUIIOSERVER_API GuiIOError GuiIOScreenCapture(const CHAR* fileName)
{
	// create a new command/response instance using the current socket
	CommandResponse cr(&m_socket);

	// create XML command message
	Parser parser;
	INT transactionId = parser.BuildScreenCapture(fileName);

	// send the message and receive a response
	CHAR inBuf[Parser::MAX_MSG_LENGTH];
	if (cr.SendReceive(parser.GetBuiltMsg(), inBuf, Parser::MAX_MSG_LENGTH) == -1)
		return GUIIO_ERROR;

	// parse the XML response
	QGuiIOActionRespEvent action;
	if (parser.ParseCmd(inBuf, Parser::MAX_MSG_LENGTH, 0, action) == -1)
		return GUIIO_ERROR;	

	// error check response
	if (action.header.transactionId != transactionId)
		return GUIIO_INVALID_TRANSACTION_ID;
	else if (action.responseType != QGuiIOActionRespEvent::ACK_NAK_RESP)
		return GUIIO_WRONG_MESSAGE_RECEIVED;

	return static_cast<GuiIOError>(action.msg.ackNakResp.error);
}

GUIIOSERVER_API GuiIOError GuiIOGetScreenCaptureBufferInfo(GuiIOScreenCaptureBufferInfoResp* out)
{
	// create a new command/response instance using the current socket
	CommandResponse cr(&m_socket);

	// create XML command message
	Parser parser;
	INT transactionId = parser.BuildScreenCaptureBufferInfo();

	// send the message and receive a response
	CHAR inBuf[Parser::MAX_MSG_LENGTH];
	if (cr.SendReceive(parser.GetBuiltMsg(), inBuf, Parser::MAX_MSG_LENGTH) == -1)
		return GUIIO_ERROR;

	// parse the XML response
	QGuiIOActionRespEvent action;
	if (parser.ParseCmd(inBuf, Parser::MAX_MSG_LENGTH, 0, action) == -1)
		return GUIIO_ERROR;

	// error check response
	if (action.header.transactionId != transactionId)
		return GUIIO_INVALID_TRANSACTION_ID;
    else if (action.responseType == QGuiIOActionRespEvent::ACK_NAK_RESP)
        return static_cast<GuiIOError>(action.msg.ackNakResp.error);
	else if (action.responseType != QGuiIOActionRespEvent::SCREEN_CAPTURE_BUFFER_INFO_RESP)
		return GUIIO_WRONG_MESSAGE_RECEIVED;

	*out = action.msg.screenCaptureBufferInfoResp;
	return GUIIO_SUCCESS;
}

GUIIOSERVER_API GuiIOError GuiIOSimulateBDAlarm(const CHAR* alarmId, const CHAR* condAugOptions, const CHAR* depenAugOptions)
{
	// create a new command/response instance using the current socket
	CommandResponse cr(&m_socket);

	// create XML command message
	Parser parser;
	INT transactionId = parser.BuildSimulateBDAlarm(alarmId, condAugOptions, depenAugOptions);

	// send the message and receive a response
	CHAR inBuf[Parser::MAX_MSG_LENGTH];
	if (cr.SendReceive(parser.GetBuiltMsg(), inBuf, Parser::MAX_MSG_LENGTH) == -1)
		return GUIIO_ERROR;

	// parse the XML response
	QGuiIOActionRespEvent action;
	if (parser.ParseCmd(inBuf, Parser::MAX_MSG_LENGTH, 0, action) == -1)
		return GUIIO_ERROR;	

	// error check response
	if (action.header.transactionId != transactionId)
		return GUIIO_INVALID_TRANSACTION_ID;
	else if (action.responseType != QGuiIOActionRespEvent::ACK_NAK_RESP)
		return GUIIO_WRONG_MESSAGE_RECEIVED;

	return static_cast<GuiIOError>(action.msg.ackNakResp.error);
}

GUIIOSERVER_API GuiIOError GuiIOGetVentInfo(GuiIOVentInfoResp* out)
{
	if (out == NULL)
		return GUIIO_ERROR;

	// create a new command/response instance using the current socket
	CommandResponse cr(&m_socket);

	// create XML command message
	Parser parser;
	INT transactionId = parser.BuildVentInfo();

	// send the message and receive a response
	CHAR inBuf[Parser::MAX_MSG_LENGTH];
	if (cr.SendReceive(parser.GetBuiltMsg(), inBuf, Parser::MAX_MSG_LENGTH) == -1)
		return GUIIO_ERROR;

	// parse the XML response
	QGuiIOActionRespEvent action;
	if (parser.ParseCmd(inBuf, Parser::MAX_MSG_LENGTH, 0, action) == -1)
		return GUIIO_ERROR;	

	// error check response
	if (action.header.transactionId != transactionId)
		return GUIIO_INVALID_TRANSACTION_ID;
	else if (action.responseType != QGuiIOActionRespEvent::VENT_INFO_RESP)
		return GUIIO_WRONG_MESSAGE_RECEIVED;

	// copy the parsed result into the caller's return structure
	*out = action.msg.ventInfoResp;
	return GUIIO_SUCCESS;
}

GUIIOSERVER_API GuiIOError GuiIOBezelKeyPress(int keyId, bool onOff)
{
	// create a new command/response instance using the current socket
	CommandResponse cr(&m_socket);

	// create XML command message
	Parser parser;
	INT transactionId = parser.BuildBezelKeyPress(keyId, onOff);

	// send the message and receive a response
	CHAR inBuf[Parser::MAX_MSG_LENGTH];
	if (cr.SendReceive(parser.GetBuiltMsg(), inBuf, Parser::MAX_MSG_LENGTH) == -1)
		return GUIIO_ERROR;

	// parse the XML response
	QGuiIOActionRespEvent action;
	if (parser.ParseCmd(inBuf, Parser::MAX_MSG_LENGTH, 0, action) == -1)
		return GUIIO_ERROR;

	// error check response
	if (action.header.transactionId != transactionId)
		return GUIIO_INVALID_TRANSACTION_ID;
	else if (action.responseType != QGuiIOActionRespEvent::ACK_NAK_RESP)
		return GUIIO_WRONG_MESSAGE_RECEIVED;

	return GUIIO_SUCCESS;
}

GUIIOSERVER_API GuiIOError GuiIOKnob(int delta)
{
	// create a new command/response instance using the current socket
	CommandResponse cr(&m_socket);

	// create XML command message
	Parser parser;
	INT transactionId = parser.BuildKnob(delta);

	// send the message and receive a response
	CHAR inBuf[Parser::MAX_MSG_LENGTH];
	if (cr.SendReceive(parser.GetBuiltMsg(), inBuf, Parser::MAX_MSG_LENGTH) == -1)
		return GUIIO_ERROR;

	// parse the XML response
	QGuiIOActionRespEvent action;
	if (parser.ParseCmd(inBuf, Parser::MAX_MSG_LENGTH, 0, action) == -1)
		return GUIIO_ERROR;

	// error check response
	if (action.header.transactionId != transactionId)
		return GUIIO_INVALID_TRANSACTION_ID;
	else if (action.responseType != QGuiIOActionRespEvent::ACK_NAK_RESP)
		return GUIIO_WRONG_MESSAGE_RECEIVED;

	return static_cast<GuiIOError>(action.msg.ackNakResp.error);
}

GUIIOSERVER_API GuiIOError GuiIOAltiaSendEvent(const CHAR* eventName, int eventValue)
{
	// create a new command/response instance using the current socket
	CommandResponse cr(&m_socket);

	// create XML command message
	Parser parser;
	INT transactionId = parser.BuildAltiaSendEvent(eventName, eventValue);

	// send the message and receive a response
	CHAR inBuf[Parser::MAX_MSG_LENGTH];
	if (cr.SendReceive(parser.GetBuiltMsg(), inBuf, Parser::MAX_MSG_LENGTH) == -1)
		return GUIIO_ERROR;

	// parse the XML response
	QGuiIOActionRespEvent action;
	if (parser.ParseCmd(inBuf, Parser::MAX_MSG_LENGTH, 0, action) == -1)
		return GUIIO_ERROR;

	// error check response
	if (action.header.transactionId != transactionId)
		return GUIIO_INVALID_TRANSACTION_ID;
	else if (action.responseType != QGuiIOActionRespEvent::ACK_NAK_RESP)
		return GUIIO_WRONG_MESSAGE_RECEIVED;

	return static_cast<GuiIOError>(action.msg.ackNakResp.error);
}

GUIIOSERVER_API GuiIOError GuiIOAltiaPollEvent(const CHAR* eventName, GuiIOAltiaPollEventResp* out)
{
	// create a new command/response instance using the current socket
	CommandResponse cr(&m_socket);

	// create XML command message
	Parser parser;
	INT transactionId = parser.BuildAltiaPollEvent(eventName);

	// send the message and receive a response
	CHAR inBuf[Parser::MAX_MSG_LENGTH];
	if (cr.SendReceive(parser.GetBuiltMsg(), inBuf, Parser::MAX_MSG_LENGTH) == -1)
		return GUIIO_ERROR;

	// parse the XML response
	QGuiIOActionRespEvent action;
	if (parser.ParseCmd(inBuf, Parser::MAX_MSG_LENGTH, 0, action) == -1)
		return GUIIO_ERROR;

	// error check response
	if (action.header.transactionId != transactionId)
		return GUIIO_INVALID_TRANSACTION_ID;
	else if (action.responseType != QGuiIOActionRespEvent::ALTIA_POLL_EVENT_RESP)
		return GUIIO_WRONG_MESSAGE_RECEIVED;

	// copy the parsed result into the caller's return structure
	*out = action.msg.altiaPollEventResp;

	return GUIIO_SUCCESS;
}

GUIIOSERVER_API GuiIOError GuiIOLButtonDown(int x, int y)
{
	CommandResponse cr(&m_socket);

	Parser parser;
	INT transactionId = parser.BuildLButtonDown(x, y);

	// send the message and receive a response
	CHAR inBuf[Parser::MAX_MSG_LENGTH];
	if (cr.SendReceive(parser.GetBuiltMsg(), inBuf, Parser::MAX_MSG_LENGTH) == -1)
		return GUIIO_ERROR;

	// parse the XML response
	QGuiIOActionRespEvent action;
	if (parser.ParseCmd(inBuf, Parser::MAX_MSG_LENGTH, 0, action) == -1)
		return GUIIO_ERROR;

	// error check response
	if (action.header.transactionId != transactionId)
		return GUIIO_INVALID_TRANSACTION_ID;
	else if (action.responseType != QGuiIOActionRespEvent::ACK_NAK_RESP)
		return GUIIO_WRONG_MESSAGE_RECEIVED;

	return static_cast<GuiIOError>(action.msg.ackNakResp.error);
}

GUIIOSERVER_API GuiIOError GuiIOLButtonUp(int x, int y)
{
	CommandResponse cr(&m_socket);

	Parser parser;
	INT transactionId = parser.BuildLButtonUp(x, y);

	// send the message and receive a response
	CHAR inBuf[Parser::MAX_MSG_LENGTH];
	if (cr.SendReceive(parser.GetBuiltMsg(), inBuf, Parser::MAX_MSG_LENGTH) == -1)
		return GUIIO_ERROR;

	// parse the XML response
	QGuiIOActionRespEvent action;
	if (parser.ParseCmd(inBuf, Parser::MAX_MSG_LENGTH, 0, action) == -1)
		return GUIIO_ERROR;

	// error check response
	if (action.header.transactionId != transactionId)
		return GUIIO_INVALID_TRANSACTION_ID;
	else if (action.responseType != QGuiIOActionRespEvent::ACK_NAK_RESP)
		return GUIIO_WRONG_MESSAGE_RECEIVED;

	return static_cast<GuiIOError>(action.msg.ackNakResp.error);
}

GUIIOSERVER_API GuiIOError GuiIOButtonClick(int x, int y)
{
	CommandResponse cr(&m_socket);

	Parser parser;
	INT transactionId = parser.BuildButtonClick(x, y);

	// send the message and receive a response
	CHAR inBuf[Parser::MAX_MSG_LENGTH];
	if (cr.SendReceive(parser.GetBuiltMsg(), inBuf, Parser::MAX_MSG_LENGTH) == -1)
		return GUIIO_ERROR;

	// parse the XML response
	QGuiIOActionRespEvent action;
	if (parser.ParseCmd(inBuf, Parser::MAX_MSG_LENGTH, 0, action) == -1)
		return GUIIO_ERROR;

	// error check response
	if (action.header.transactionId != transactionId)
		return GUIIO_INVALID_TRANSACTION_ID;
	else if (action.responseType != QGuiIOActionRespEvent::ACK_NAK_RESP)
		return GUIIO_WRONG_MESSAGE_RECEIVED;

	return static_cast<GuiIOError>(action.msg.ackNakResp.error);
}

GUIIOSERVER_API GuiIOError GuiIOBye()
{
	CommandResponse cr(&m_socket);
	Parser parser;
	parser.BuildBye();
	if (!cr.Send(parser.GetBuiltMsg()))
		return GUIIO_ERROR;
	return GUIIO_SUCCESS;
}

GUIIOSERVER_API GuiIOError GuiIOSimulateTouch(int x, int y)
{
	CommandResponse cr(&m_socket);

	Parser parser;
	INT transactionId = parser.BuildSimulateTouch(x, y);

	// send the message and receive a response
	CHAR inBuf[Parser::MAX_MSG_LENGTH];
	if (cr.SendReceive(parser.GetBuiltMsg(), inBuf, Parser::MAX_MSG_LENGTH) == -1)
		return GUIIO_ERROR;

	// parse the XML response
	QGuiIOActionRespEvent action;
	if (parser.ParseCmd(inBuf, Parser::MAX_MSG_LENGTH, 0, action) == -1)
		return GUIIO_ERROR;

	// error check response
	if (action.header.transactionId != transactionId)
		return GUIIO_INVALID_TRANSACTION_ID;
	else if (action.responseType != QGuiIOActionRespEvent::ACK_NAK_RESP)
		return GUIIO_WRONG_MESSAGE_RECEIVED;

	return static_cast<GuiIOError>(action.msg.ackNakResp.error);
}

GUIIOSERVER_API GuiIOError GuiIOClearScreenshots()
{
	CommandResponse cr(&m_socket);

	Parser parser;
	INT transactionId = parser.BuildClearScreenshots();

	// send the message and receive a response
	CHAR inBuf[Parser::MAX_MSG_LENGTH];
	if (cr.SendReceive(parser.GetBuiltMsg(), inBuf, Parser::MAX_MSG_LENGTH) == -1)
		return GUIIO_ERROR;

	// parse the XML response
	QGuiIOActionRespEvent action;
	if (parser.ParseCmd(inBuf, Parser::MAX_MSG_LENGTH, 0, action) == -1)
		return GUIIO_ERROR;

	// error check response
	if (action.header.transactionId != transactionId)
		return GUIIO_INVALID_TRANSACTION_ID;
	else if (action.responseType != QGuiIOActionRespEvent::ACK_NAK_RESP)
		return GUIIO_WRONG_MESSAGE_RECEIVED;

	return static_cast<GuiIOError>(action.msg.ackNakResp.error);
}

GUIIOSERVER_API GuiIOError GuiIOQueryScreenshotPath(int index, GuiIOQueryScreenshotPathResp* out)
{
	// create a new command/response instance using the current socket
	CommandResponse cr(&m_socket);

	// create XML command message
	Parser parser;
	INT transactionId = parser.BuildQueryScreenshotPath(index);

	// send the message and receive a response
	CHAR inBuf[Parser::MAX_MSG_LENGTH];
	if (cr.SendReceive(parser.GetBuiltMsg(), inBuf, Parser::MAX_MSG_LENGTH) == -1)
		return GUIIO_ERROR;

	// parse the XML response
	QGuiIOActionRespEvent action;
	if (parser.ParseCmd(inBuf, Parser::MAX_MSG_LENGTH, 0, action) == -1)
		return GUIIO_ERROR;

	// error check response
	if (action.header.transactionId != transactionId)
		return GUIIO_INVALID_TRANSACTION_ID;
    else if (action.responseType == QGuiIOActionRespEvent::ACK_NAK_RESP)
        return static_cast<GuiIOError>(action.msg.ackNakResp.error);
	else if (action.responseType != QGuiIOActionRespEvent::QUERY_SCREENSHOT_PATH_RESP)
		return GUIIO_WRONG_MESSAGE_RECEIVED;

	// copy the parsed result into the caller's return structure
	*out = action.msg.queryScreenshotPathResp;

	return GUIIO_SUCCESS;
}
