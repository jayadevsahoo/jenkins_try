#pragma once
// CAskUserDlg dialog

#include "afxwin.h"
#include "resource.h"

class CAskUserDlg : public CDialog
{
	DECLARE_DYNAMIC(CAskUserDlg)

public:
	CAskUserDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CAskUserDlg();

	int m_nReturn;

	CString m_strMessage;
	CString m_strButton1;
	CString m_strButton2;
	CString m_strButton3;
	CString m_strButton4;


// Dialog Data
	enum { IDD = IDD_ASK_USER_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedButtonStopScript();
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton2();
	afx_msg void OnBnClickedButton3();
	afx_msg void OnBnClickedButton4();
protected:
	virtual void OnCancel();
};
