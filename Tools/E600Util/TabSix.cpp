// TabSix.cpp : implementation file
//

#include "stdafx.h"
#include "E600Util.h"
#include "TabSix.h"
#include "GlobalObjects.h"
#include "UserMessages.h"
#include <stdlib.h>

static CWnd *pEditFloat[NUM_BOUNDED_SETTING_IDS];
static CWnd *pEditDebugFloat[NUM_BOUNDED_SETTING_DEBUG_IDS];
	
UINT SettingIdMap[NUM_TEST_SETTING_IDS] =
{
	MAND_TYPE,
	MODE,
	VENT_TYPE,
	TRIGGER_TYPE,
	FLOW_PATTERN,
	SUPPORT_TYPE,
	RESP_RATE,
	TIDAL_VOLUME,
	PEAK_INSP_FLOW,
	PLATEAU_TIME,
	FLOW_SENS,
	PRESS_SENS,
	OXYGEN_PERCENT,
	PEEP,
	INSP_PRESS,
	INSP_TIME,
	FLOW_ACCEL_PERCENT,
	PRESS_SUPP_LEVEL,
	EXP_SENS,
	IBW,
	HIGH_CCT_PRESS,
	/////////////
	MIN_INSP_FLOW,
	/////////////
};

// TabFive dialog

IMPLEMENT_DYNAMIC(CTabSix, CDialog)

CTabSix::CTabSix(CWnd* pParent /*=NULL*/)
	: CDialog(CTabSix::IDD, pParent)
{
}

CTabSix::~CTabSix()
{
}

void CTabSix::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_COMBO_MAND_TYPE, ComboBoxInt[MAND_TYPE_ID]);
	DDX_Control(pDX, IDC_COMBO_MODE, ComboBoxInt[MODE_ID]);
	DDX_Control(pDX, IDC_COMBO_VENT_TYPE, ComboBoxInt[VENT_TYPE_ID]);
	DDX_Control(pDX, IDC_COMBO_TRIG_TYPE, ComboBoxInt[TRIGGER_TYPE_ID]);
	DDX_Control(pDX, IDC_COMBO_FLOW_PATTERN, ComboBoxInt[FLOW_PATTERN_ID]);
	DDX_Control(pDX, IDC_COMBO_SUPPORT_TYPE, ComboBoxInt[SUPPORT_TYPE_ID]);
}


BEGIN_MESSAGE_MAP(CTabSix, CDialog)
	ON_WM_PAINT()
	ON_BN_CLICKED(IDC_BUTTON_SEND_SETTINGS, &CTabSix::OnBnClickedButtonXmitSettings)
	ON_BN_CLICKED(IDC_BUTTON_LOAD_SETTINGS, &CTabSix::OnBnClickedButtonLoadSettings)

	ON_MESSAGE(WM_SETTINGS_REPLY, &OnSettingsReply)
	ON_BN_CLICKED(IDC_BUTTON_XMIT_DEBUG_SETTINGS, &CTabSix::OnBnClickedButtonXmitDebugSettings)
END_MESSAGE_MAP()


void CTabSix::OnPaint()
{
	CDialog::OnPaint();
}

void CTabSix::GetEditControlPointers()
{
	pEditFloat[RESP_RATE_ID] = GetDlgItem(IDC_EDIT_RATE);
	pEditFloat[TIDAL_VOLUME_ID] = GetDlgItem(IDC_EDIT_Vt);
	pEditFloat[PEAK_INSP_FLOW_ID] = GetDlgItem(IDC_EDIT_Fp);
	pEditFloat[PLATEAU_TIME_ID] = GetDlgItem(IDC_EDIT_Tpl);
	pEditFloat[FLOW_SENS_ID] = GetDlgItem(IDC_EDIT_FLOW_SENSE);
	pEditFloat[PRESS_SENS_ID] = GetDlgItem(IDC_EDIT_PRESSURE_SENSE);
	pEditFloat[OXYGEN_PERCENT_ID] = GetDlgItem(IDC_EDIT_FIO2);
	pEditFloat[PEEP_ID] = GetDlgItem(IDC_EDIT_PEEP);
	pEditFloat[INSP_PRESS_ID] = GetDlgItem(IDC_EDIT_Pi);
	pEditFloat[INSP_TIME_ID] = GetDlgItem(IDC_EDIT_Ti);
	pEditFloat[FLOW_ACCEL_PERCENT_ID] = GetDlgItem(IDC_EDIT_RISE_TIME);
	pEditFloat[PRESS_SUPP_LEVEL_ID] = GetDlgItem(IDC_EDIT_Psupp);
	pEditFloat[EXP_SENS_ID] = GetDlgItem(IDC_EDIT_Esense);
	pEditFloat[IBW_ID] = GetDlgItem(IDC_EDIT_IBW);
	pEditFloat[HIGH_CCT_PRESS_ID] = GetDlgItem(IDC_EDIT_HIGH_CCT_PRESS);
}

// TabThree message handlers
BOOL CTabSix::OnInitDialog()
{
	CDialog::OnInitDialog();

	ComboBoxInt[MAND_TYPE_ID].AddString(L"PC");
	ComboBoxInt[MAND_TYPE_ID].AddString(L"VC");
	ComboBoxInt[MAND_TYPE_ID].AddString(L"VC+");
	ComboCurSel[MAND_TYPE_ID] = ComboBoxInt[MAND_TYPE_ID].SelectString(-1, L"PC");

	ComboBoxInt[MODE_ID].AddString(L"AC");
	ComboBoxInt[MODE_ID].AddString(L"SIMV");
	ComboBoxInt[MODE_ID].AddString(L"SPONT");
	ComboBoxInt[MODE_ID].AddString(L"BILEVEL");
	ComboCurSel[MODE_ID] = ComboBoxInt[MODE_ID].SelectString(-1, L"AC");

	ComboBoxInt[VENT_TYPE_ID].AddString(L"INVASIVE");
	ComboBoxInt[VENT_TYPE_ID].AddString(L"NIV");
	ComboCurSel[VENT_TYPE_ID] = ComboBoxInt[VENT_TYPE_ID].SelectString(-1, L"INVASIVE");

	ComboBoxInt[TRIGGER_TYPE_ID].AddString(L"FLOW");
	ComboBoxInt[TRIGGER_TYPE_ID].AddString(L"PRESSURE");
	ComboCurSel[TRIGGER_TYPE_ID] = ComboBoxInt[TRIGGER_TYPE_ID].SelectString(-1, L"PRESSURE");

	ComboBoxInt[FLOW_PATTERN_ID].AddString(L"SQUARE");
	ComboBoxInt[FLOW_PATTERN_ID].AddString(L"RAMP");
	ComboCurSel[FLOW_PATTERN_ID] = ComboBoxInt[FLOW_PATTERN_ID].SelectString(-1, L"SQUARE");

	ComboBoxInt[SUPPORT_TYPE_ID].AddString(L"NONE");
	ComboBoxInt[SUPPORT_TYPE_ID].AddString(L"PS");
	ComboBoxInt[SUPPORT_TYPE_ID].AddString(L"TC");
	ComboBoxInt[SUPPORT_TYPE_ID].AddString(L"VS");
	ComboBoxInt[SUPPORT_TYPE_ID].AddString(L"PA");
	ComboCurSel[SUPPORT_TYPE_ID] = ComboBoxInt[SUPPORT_TYPE_ID].SelectString(-1, L"PS");

	pEditFloat[RESP_RATE_ID] = GetDlgItem(IDC_EDIT_RATE);
	pEditFloat[RESP_RATE_ID]->SetWindowTextW(L"15");
	
	pEditFloat[TIDAL_VOLUME_ID] = GetDlgItem(IDC_EDIT_Vt);
	pEditFloat[TIDAL_VOLUME_ID]->SetWindowTextW(L"500");

	pEditFloat[PEAK_INSP_FLOW_ID] = GetDlgItem(IDC_EDIT_Fp);
	pEditFloat[PEAK_INSP_FLOW_ID]->SetWindowTextW(L"30");

	pEditFloat[PLATEAU_TIME_ID] = GetDlgItem(IDC_EDIT_Tpl);
	pEditFloat[PLATEAU_TIME_ID]->SetWindowTextW(L"0");

	pEditFloat[FLOW_SENS_ID] = GetDlgItem(IDC_EDIT_FLOW_SENSE);
	pEditFloat[FLOW_SENS_ID]->SetWindowTextW(L"2");

	pEditFloat[PRESS_SENS_ID] = GetDlgItem(IDC_EDIT_PRESSURE_SENSE);
	pEditFloat[PRESS_SENS_ID]->SetWindowTextW(L"2");

	pEditFloat[OXYGEN_PERCENT_ID] = reinterpret_cast<CWnd *>(GetDlgItem(IDC_EDIT_FIO2));
	pEditFloat[OXYGEN_PERCENT_ID]->SetWindowTextW(L"21");

	pEditFloat[PEEP_ID] = GetDlgItem(IDC_EDIT_PEEP);
	pEditFloat[PEEP_ID]->SetWindowTextW(L"3");

	pEditFloat[INSP_PRESS_ID] = GetDlgItem(IDC_EDIT_Pi);
	pEditFloat[INSP_PRESS_ID]->SetWindowTextW(L"18");

	pEditFloat[INSP_TIME_ID] = GetDlgItem(IDC_EDIT_Ti);
	pEditFloat[INSP_TIME_ID]->SetWindowTextW(L"1000");

	pEditFloat[FLOW_ACCEL_PERCENT_ID] = GetDlgItem(IDC_EDIT_RISE_TIME);
	pEditFloat[FLOW_ACCEL_PERCENT_ID]->SetWindowTextW(L"50");

	pEditFloat[PRESS_SUPP_LEVEL_ID] = GetDlgItem(IDC_EDIT_Psupp);
	pEditFloat[PRESS_SUPP_LEVEL_ID]->SetWindowTextW(L"18");

	pEditFloat[EXP_SENS_ID] = GetDlgItem(IDC_EDIT_Esense);
	pEditFloat[EXP_SENS_ID]->SetWindowTextW(L"25");

	pEditFloat[IBW_ID] = GetDlgItem(IDC_EDIT_IBW);
	pEditFloat[IBW_ID]->SetWindowTextW(L"50");

	pEditFloat[HIGH_CCT_PRESS_ID] = GetDlgItem(IDC_EDIT_HIGH_CCT_PRESS);
	pEditFloat[HIGH_CCT_PRESS_ID]->SetWindowTextW(L"50");

	pEditDebugFloat[COMPLIANCE_ID] = GetDlgItem(IDC_EDIT_COMPLIANCE);
	pEditDebugFloat[COMPLIANCE_ID]->SetWindowTextW(L"1.0");

	return FALSE;
}


//**********************************************************************************************//
void CTabSix::SendPacketToBD(char *pBuff, int Size)
{
	pTabOne->SendMessage(WM_SETTING_CMD, Size, (LPARAM)pBuff);
	//pTabOne->SendPacketToBD(pBuff, Size);

	//BdTxPacket.SendBdPacket(pBuff, Size);
}

//**********************************************************************************************//
void CTabSix::SendCompleteSettingPacket(SettingData_t Data[])
{
	SettingsPacket_t Packet;
		
	Packet.MsgId = PC_SEND_SETTINGS_MSG_ID;
	Packet.NumOfSettings = NUM_TEST_SETTING_IDS;

	for(int i=FIRST_SETTING_ID; i<=LAST_DISCRETE_SETTING_ID; i++)
	{
		Packet.Data[i].settingId = SettingIdMap[i];
		Packet.Data[i].discreteValue = Data[i].discreteValue;
	}

	for(int i=FIRST_BOUNDED_SETTING_ID; i<=LAST_BOUNDED_SETTING_ID; i++)
	{
		Packet.Data[i].settingId = SettingIdMap[i];
		Packet.Data[i].boundedValue = Data[i].boundedValue;
		Packet.Data[i].boundedPrec = Data[i].boundedPrec;
	}

	Packet.Data[MIN_INSP_FLOW_ID].settingId = SettingIdMap[MIN_INSP_FLOW_ID];
	Packet.Data[MIN_INSP_FLOW_ID].boundedValue = Data[MIN_INSP_FLOW_ID].boundedValue;
	Packet.Data[MIN_INSP_FLOW_ID].boundedPrec = Data[MIN_INSP_FLOW_ID].boundedPrec;

	//pTabOne->SendMessage(WM_SETTING_CMD, sizeof(SettingsPacket_t), (LPARAM)&Packet);
	SendPacketToBD((char *)&Packet, sizeof(SettingsPacket_t));
}

//**********************************************************************************************//
void CTabSix::SendUpdateSettingPacket(SettingData_t Data[])
{
	UINT16 Index = 0;
	SettingsPacket_t Packet;

	Packet.MsgId = PC_SEND_SETTINGS_MSG_ID;

	for(int i=FIRST_SETTING_ID; i<=LAST_DISCRETE_SETTING_ID; i++)
	{
		if(SettingData[i].discreteValue != Data[i].discreteValue)
		{
			Packet.Data[Index].settingId = SettingIdMap[i];
			Packet.Data[Index++].discreteValue = Data[i].discreteValue;
			SettingData[i].discreteValue = Data[i].discreteValue;
		}
	}

	for(int i=FIRST_BOUNDED_SETTING_ID; i<=LAST_BOUNDED_SETTING_ID; i++)
	{
		if(SettingData[i].boundedValue != Data[i].boundedValue)
		{
			Packet.Data[Index].settingId = SettingIdMap[i];
			Packet.Data[Index].boundedValue = Data[i].boundedValue;
			Packet.Data[Index++].boundedPrec = Data[i].boundedPrec;
			SettingData[i].boundedValue = Data[i].boundedValue;
		}
	}

	
	if(Index > 0)
	{
		Packet.NumOfSettings = Index;
		//pTabOne->SendMessage(WM_SETTING_CMD, sizeof(SettingsPacket_t), (LPARAM)&Packet);
		SendPacketToBD((char *)&Packet, sizeof(SettingsPacket_t));
	}
}

//**********************************************************************************************//
static const float SQUARE_FLOW_FACTOR = (60000.0f * 0.001f);
static const float RAMP_FLOW_FACTOR = (60000.0f * 0.001f * 2.0f);

void CTabSix::ReadSettings()
{
	GetEditControlPointers();

	for(int i=FIRST_SETTING_ID; i<=LAST_DISCRETE_SETTING_ID; i++)
	{
		SettingDataNew[i].discreteValue = ComboBoxInt[i].GetCurSel();	
	}

	for(int i=FIRST_BOUNDED_SETTING_ID,j=0; i<=LAST_BOUNDED_SETTING_ID; i++,j++)
	{
		CString Str;
		pEditFloat[j]->GetWindowTextW(Str);
		SettingDataNew[i].boundedValue = (float)_wtof(Str);
		SettingDataNew[i].boundedPrec = 0;
		if(i == TIDAL_VOLUME_ID)
			SettingDataNew[i].boundedPrec = 3;
	}

	// Min insp flow
	float MinInspFlow = max(0.8f, SettingDataNew[PEAK_INSP_FLOW_ID].boundedValue * 0.1f);
	SettingDataNew[MIN_INSP_FLOW_ID].boundedValue = MinInspFlow;
	SettingDataNew[MIN_INSP_FLOW_ID].boundedPrec = 0;

	if(SettingDataNew[MAND_TYPE_ID].discreteValue == 1)	// VCV
	{
		float Itime;
		if(SettingDataNew[FLOW_PATTERN_ID].discreteValue == 0)	// square
		{
			Itime = (SQUARE_FLOW_FACTOR * SettingDataNew[TIDAL_VOLUME_ID].boundedValue) /
					SettingDataNew[PEAK_INSP_FLOW_ID].boundedValue;
		}
		else // ramp
		{
			Itime = (RAMP_FLOW_FACTOR * SettingDataNew[TIDAL_VOLUME_ID].boundedValue) /
					(SettingDataNew[PEAK_INSP_FLOW_ID].boundedValue + MinInspFlow);
		}
		
		SettingDataNew[INSP_TIME_ID].boundedValue = Itime + SettingDataNew[PLATEAU_TIME_ID].boundedValue;
	}
}

//**********************************************************************************************//
void CTabSix::OnBnClickedButtonXmitSettings()
{
	static bool bFirstTime = true;
		
	ReadSettings();
	if(bFirstTime)
	{
		memcpy(SettingData, SettingDataNew, NUM_TEST_SETTING_IDS * sizeof(SettingData_t));
		SendCompleteSettingPacket(SettingDataNew);
		//bFirstTime = false;
	}
	else
	{
		SendUpdateSettingPacket(SettingDataNew);
		memcpy(SettingData, SettingDataNew, NUM_TEST_SETTING_IDS * sizeof(SettingData_t));
	}
	pStatusBar->SetPaneText(0, L"Sending settings...");
}


void CTabSix::OnBnClickedButtonLoadSettings()
{
	SettingsRequestPacket_t Packet;

	Packet.MsgId = PC_REQUEST_SETTINGS_MSG_ID;
	Packet.NumOfSettings = NUM_TEST_SETTING_IDS;

	SendPacketToBD((char *)&Packet, sizeof(SettingsRequestPacket_t));
	pStatusBar->SetPaneText(0, L"Loading settings...");
}

LRESULT CTabSix::OnSettingsReply(WPARAM x, LPARAM y)
{
	SettingsReplyPacket_t *pPacket = (SettingsReplyPacket_t *)y;

	if(x == sizeof(SettingsReplyPacket_t))
	{
		for(int i=0; i<NUM_DISCRETE_SETTING_IDS; i++)
		{
			ComboBoxInt[i].SetCurSel(pPacket->Data[i].discreteValue);
		}

		GetEditControlPointers();
		for(int i=FIRST_BOUNDED_SETTING_ID,j=0; i<=LAST_BOUNDED_SETTING_ID; i++,j++)
		{
			CString Str;
			Str.Format(L"%f", pPacket->Data[i].boundedValue);
			pEditFloat[j]->SetWindowTextW(Str);
		}	
	}
	return 0;
}

void CTabSix::OnBnClickedButtonXmitDebugSettings()
{
	pEditDebugFloat[COMPLIANCE_ID] = GetDlgItem(IDC_EDIT_COMPLIANCE);

	SettingsDebugPacket_t Packet;
	Packet.MsgId = PC_SEND_SETTINGS_DEBUG_MSG_ID;
	Packet.NumOfSettings = 1;

	CString Str;
	pEditDebugFloat[COMPLIANCE_ID]->GetWindowTextW(Str);
	Packet.Data[COMPLIANCE_ID].boundedValue = (float)_wtof(Str);

	SendPacketToBD((char *)&Packet, sizeof(SettingsDebugPacket_t));
}
