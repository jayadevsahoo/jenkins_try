
#ifndef HighExhMinVolSetting_HH
#define HighExhMinVolSetting_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  HighExhMinVolSetting - High exhaled minute volume setting 
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/HighExhMinVolSetting.hhv   25.0.4.0   19 Nov 2013 14:27:24   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006   By: gdc    Date: 24-Sep-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal non-invasive CPAP.
//
//  Revision: 005   By: sah    Date: 29-Jun-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  modified to use newly-modified 'BoundedRange' class to handle
//         range, via linked list of interval nodes
//      *  eliminated 'get{Max,Min}Limit()' method, now defined by base class
//
//  Revision: 004   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'getApplicability()' method
//	*  removed unnecessary 'isAcceptedValid()' method
//
//  Revision: 003   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 002   By: sah    Date:  28-May-1998    DR Number: 5058
//  Project:  COLOR
//  Description:
//	--- Merged Rev "Color" (1.10.1.0) into Rev "BiLevel" (1.10.2.0)
//	Modified range/resolution for improving the user's ability to
//	accurately change this setting's value.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Initial version.
//
//====================================================================

//@ Usage-Classes
#include "BatchBoundedSetting.hh"
//@ End-Usage


class HighExhMinVolSetting : public BatchBoundedSetting
{
  public:
    HighExhMinVolSetting(void); 
    virtual ~HighExhMinVolSetting(void);

    virtual Applicability::Id  getApplicability(
			    const Notification::ChangeQualifier qualifierId
					       ) const;

    virtual SettingValue  getNewPatientValue(void) const;

	virtual void  acceptTransition(const SettingId::SettingIdType settingId,
								   const DiscreteValue            newValue,
								   const DiscreteValue            currValue);

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL, 
			   const char*       pPredicate = NULL);

  protected:
    virtual void  updateConstraints_(void);

  private:
    HighExhMinVolSetting(const HighExhMinVolSetting&);	// not implemented...
    void  operator=(const HighExhMinVolSetting&);	// not implemented...
};


#endif // HighExhMinVolSetting_HH 
