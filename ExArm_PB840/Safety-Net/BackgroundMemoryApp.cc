#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//============================ C L A S S   D E S C R I P T I O N ======
//@  Class: BackgroundMemoryApp - Background memory testing application
//---------------------------------------------------------------------
//@ Interface-Description
//
//  This class contains the functionality for performing the Safety-Net
//  test that monitors the Flash memory of the system.  Contained in this
//  class is the application Task(), a standard do-forever task that 
//  is spawned during System Initialization.  The task runs continuously
//  at the lowest priority of the system.  In effect this is the idle
//  task of the system.
//
//  A BackgroundMemoryApp exists on both the GUI & BD platforms.  
//---------------------------------------------------------------------
//@ Rationale
//  The class provides the facility to monitor the flash memory of the 
//  each CPU on a continuous basis.
//---------------------------------------------------------------------
//@ Implementation-Description
//  
//  This task is in a forever loop that continuously perform 32-bit
//  checksum test on the flash memory when the system state is online
//  for that CPU (BD/GUI).
//---------------------------------------------------------------------
//@ Fault-Handling
//  N/A
//---------------------------------------------------------------------
//@ Restrictions
//  None
//---------------------------------------------------------------------
//@ Invariants
//  None
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Safety-Net/vcssrc/BackgroundMemoryApp.ccv   25.0.4.0   19 Nov 2013 14:20:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 016  By:  gdc    Date:  24-Aug-2010    SCR Number: 6663
//  Project:  API/MAT
//  Description:
//		Reduced block size to test before task yield to 1024 bytes
//		based on performance requirement of same priority serial tasks.
//
//  Revision: 015  By:  gdc    Date:  12-Dec-2008    SCR Number: 6068
//  Project:  840S
//  Description:
//		Removed SUN prototype code.
//
//  Revision: 014  By: sah    Date:  15-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 013 By: iv     Date: 11-Mar-1998   DR Number: DCS 5041
//  	Project:  Sigma (R8027)
//		Description:
//		   Added error code for memory test.
//
//  Revision: 012   By: pc   Date: 22-Oct-1997    DR Number: 2570
//    Project:  Sigma (840)
//    Description:
//      Converted SIGMA_INTEGRATION compiler option to work with Sigma Integration Test (aka TestCommand Group)
//         Allows testing of requirements 06024; 06027; 06125.
//
//  Revision: 011   By: iv   Date: 18-Aug-1997    DR Number: 2396
//    Project:  Sigma (840)  
//    Description:
//     FlashMemoryTest_() was called twice in Task().
//     Added testable items.
//
//  Revision: 010   By: gdc  Date: 30-JUL-1997    DR Number: 2197
//    Project:  Sigma (840)  
//    Description:
//      Changed to use FLASH_CHECKSUMM_BASE defined in MemoryMap.hh
//
//  Revision: 009   By: sp   Date: 9-July-1997    DR Number: 2387
//    Project:  Sigma (840)  
//    Description:
//      Added integration test code.
//
//  Revision: 008  By: gdc   Date: 01-Jul-1997    DR Number: 2232
//  	Project:  Sigma (840)
//		Description:
//			Yield CPU to other lowest priority tasks periodically.
//
//  Revision: 007  By: sp    Date:  1-Jul-1997    DR Number: 1848
//      Project:  Sigma (840)
//      Description:
//          Add task monitoring.
//
//  Revision: 006  By: sp    Date:  6-Jun-1997    DR Number: 2196
//  	Project:  Sigma (840)
//		Description:
//			Need to report error if flash memory check fails.
//
//  Revision: 005  By: iv    Date:  29-May-1997    DR Number: 2018
//  	Project:  Sigma (840)
//		Description:
//			Remove obsolete file header Wait.hh.
//
//  Revision: 004  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 003   By: iv    Date: 29-Apr-1997   DR Number: None 
//    Project:  Sigma (R8027)
//    Description:
//      Rework per unit test peer review.
//
//  Revision: 002   By: iv   Date: 28-Apr-1997    DR Number: 1996
//    Project:  Sigma (840)
//    Description:
//		Added tracing to SRS req. 00528, 06024, 06130
//
//  Revision: 001    By: by   Date: 12-Dec-1996   DR Number: None
//
//    Project:  Sigma (840)
//    Description:
//        Initial version.
//
//=====================================================================

#include "BackgroundMemoryApp.hh"

//@ Usage-Classes
#include "MsgTimer.hh"
//TODO E600 #include "OsUtil.hh"
#include "Task.hh"
#include "TaskMonitor.hh"
#include "CalInfoFlashBlock.hh"
#include "CalInfoRefs.hh"
#include "TaskControlAgent.hh"
//TODO E600 #include "FlashMemCons.hh"
//TODO E600 added this include
#include "MemoryMap.hh"

#include "OsTimeStamp.hh"

//TODO remove if not needed.. this is only to enable RETAILMSG macro
#if defined(_WIN32_WCE)
#include <winbase.h>
#include <tchar.h>
#endif

#ifdef SIGMA_INTEGRATION
#include "TestTrigger.hh"
#endif //SIGMA_INTEGRATION


////////////////////////////////////////////////////////////////////////////////
//         Local    Constant      Declarations
////////////////////////////////////////////////////////////////////////////////
#ifdef E600_840_TEMP_REMOVED
static const
EepromStruct* PFLASH_CHECKSUM_TABLE = (EepromStruct*) FLASH_CHECKSUM_BASE;
#endif

///////////////////////////////////////////////////////////////////////////////
//  Initialize static data member(s)
///////////////////////////////////////////////////////////////////////////////
Boolean BackgroundMemoryApp::HasFlashMemoryTestFailed_ = FALSE;

#ifdef SIGMA_UNIT_TEST
Boolean BackgroundMemoryApp::FailChecksum_ = FALSE;
#endif // ifdef SIGMA_UNIT_TEST

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  Task
//
//@ Interface-Description
//  This method accepts no parameter and does not return.
//
//  This task is in a forever loop that continuously performs a 32-bit
//  checksum test on the system flash memory.
//  Priority assigned to this task must be the absolute lowest.
//  Otherwise, task(s) with lower priority will not run.
//---------------------------------------------------------------------
//@ Implementation-Description
//  When system state is online, this Task will invoke the 32-bit
//  flash memory test on a continuous basis.
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================


void 
BackgroundMemoryApp::Task( void )
{
    const Int32 BK_TASK_REPORT_TIME_MS = 60000;
    OsTimeStamp lastReportTime;

#ifndef SIGMA_UNIT_TEST
    while( TRUE )
    {
#endif // SIGMA_UNIT_TEST

        if ( STATE_ONLINE == TaskControlAgent::GetMyState() )
        {
#if defined( SIGMA_PRODUCTION ) || defined( SIGMA_UNIT_TEST )
            // $[TI1]
            Uint16 errorCode = 0U;
            BkEventName testStatus = FlashMemoryTest_( errorCode );
            if ( testStatus != BK_NO_EVENT )
            {
                // $[TI1.1]
                // Report the failure if needed
                Background::ReportBkEvent( testStatus, errorCode );
            }
            // $[TI1.2]
#else
            Task::Delay(1);
#endif  // SIGMA_PRODUCTION || SIGMA_UNIT_TEST
        }
        else
        {
            // $[TI2]
            //  no processing if not online, but
            //  suspend so other same priority task can process
            Task::Delay(1);
        }

        if (lastReportTime.getPassedTime() >= BK_TASK_REPORT_TIME_MS)
        {
            // $[TI3.1]
            lastReportTime.now();
            TaskMonitor::Report();
        }
        // $[TI3.2]

#ifndef SIGMA_UNIT_TEST
   }
#endif // SIGMA_UNIT_TEST

}

//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//=====================================================================
//
//  Private Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  FlashMemoryTest_
//
//@ Interface-Description
//  This method accepts a Uint16 errorCode and returns a BkEventName enumeration.
//  This method tests the Flash memory on board.  This test is performed
//  on both the BD and GUI CPUs.  In addition, a test to insure the
//  integrity of the CalInfoFlashBlock is invoked.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[00528] $[06024] $[06130] 
//  If this test has not already failed, the method invokes a 32-bit
//  checksum test on the flash memory on board.  In addition, it invokes
//  CalInfoFlashBlock class to perform a data integrity check on its
//  Service-Data checksums.
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

BkEventName
BackgroundMemoryApp::FlashMemoryTest_( Uint16 &errorCode )
{
#ifdef E600_840_TEMP_REMOVED

    BkEventName rtnValue = BK_NO_EVENT;

    if ( FALSE == HasFlashMemoryTestFailed_ )
    {
    // $[TI1]
        if ( ( VerifyAppsChecksum32_( PFLASH_CHECKSUM_TABLE, errorCode ) != SUCCESS )
#ifndef SIGMA_UNIT_TEST
                // validate Service-Mode block data
             || ( RCalInfoFlashBlock.isFlashOkay() != TRUE )
#endif // SIGMA_UNIT_TEST
           )
        {
        // $[TI1.1]
            HasFlashMemoryTestFailed_ = TRUE;

#if defined( SIGMA_BD_CPU )
            rtnValue = BK_BD_EEPROM_CHECKSUM;
#elif defined( SIGMA_GUI_CPU )
            rtnValue = BK_GUI_EEPROM_CHECKSUM;
#endif // elif defined( SIGMA_GUI_CPU )

        }
        // $[TI1.2]
    }
    // $[TI2]

    return( rtnValue );
#endif
	return BK_NO_EVENT;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: VerifyAppsChecksum32_ 
//
//@ Interface-Description
//
//  This method accepts a const pointer to a EepromStruct type and 
//  a Uint16 for error code and returns a SigmaStatus (SUCCESS/FAILURE).
//
//  This method calculates a 32-bit checksum on the memory locations
//  defined by Flash memory map embedded into a specified location
//  of Flash memory when an image is burned-in.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//  This method verifies the data image in the flash memory by
//  performing a 32-bit checksums.
//  The calculated checksum is compared with the expected checksum
//  in the checksum table.  If the calculated and expected checksums
//  match, a SUCCESS status is returned.  Otherwise, a FAILURE status
//  is returned.
//---------------------------------------------------------------------
//@ PreCondition
//  CLASS_PRE_CONDITION( pFlashMemory != NULL )
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

SigmaStatus  
BackgroundMemoryApp::VerifyAppsChecksum32_( const EepromStruct* pFlashMemory, Uint16 &errorCode )
{
    CLASS_PRE_CONDITION( pFlashMemory != NULL );

    SigmaStatus testStatus = SUCCESS;
    Int32 flashBlockNdx = 0;
    register Uint32 flashAddrNdx = 0;
    register Uint32 additiveChecksum = 0;
    Uint32 flashBlockLength = 0;
    Uint32* pStartingAddr = NULL;

    for ( flashBlockNdx = 0; flashBlockNdx < pFlashMemory->numChecksums &&
                             SUCCESS == testStatus; ++flashBlockNdx )
    {
        pStartingAddr 
            = pFlashMemory->checksumBlock[ flashBlockNdx ].pStartAddress;

        flashBlockLength = pFlashMemory->checksumBlock[ flashBlockNdx ].length;

        // convert the block size from byte to long word
        flashBlockLength /= sizeof( Int32 );

        // initialize checksum to 0
        additiveChecksum = 0;

#ifdef SIGMA_INTEGRATION
#ifdef SIGMA_BD_CPU
        if ( TestTrigger::TriggerOnRequirementCheck( 6024 ) )
        {
           // corrupt checksum to 1
           additiveChecksum = 1;
        }
#endif // SIGMA_BD_CPU
#ifdef SIGMA_GUI_CPU
        if ( TestTrigger::TriggerOnRequirementCheck( 6130 ) )
        {
           // corrupt checksum to 1
           additiveChecksum = 1;
        }
#endif // SIGMA_GUI_CPU
#endif // SIGMA_INTEGRATION

#ifdef SIGMA_UNIT_TEST
        if ( TRUE == FailChecksum_ ) { additiveChecksum = 1; }
#endif // ifdef SIGMA_UNIT_TEST

        register Uint32* pLoc = pStartingAddr;
#if E600_TEMP_REMOVED
        for( flashAddrNdx = 0; flashAddrNdx < flashBlockLength; flashAddrNdx += 16 )
        {
            //additiveChecksum += *( pStartingAddr + flashAddrNdx );            
            //  perform 16 additions to optimize performance through
            //  instruction cache
            additiveChecksum += *pLoc++;
            additiveChecksum += *pLoc++;
            additiveChecksum += *pLoc++;
            additiveChecksum += *pLoc++;
            additiveChecksum += *pLoc++;
            additiveChecksum += *pLoc++;
            additiveChecksum += *pLoc++;
            additiveChecksum += *pLoc++;
            additiveChecksum += *pLoc++;
            additiveChecksum += *pLoc++;
            additiveChecksum += *pLoc++;
            additiveChecksum += *pLoc++;
            additiveChecksum += *pLoc++;
            additiveChecksum += *pLoc++;
            additiveChecksum += *pLoc++;
            additiveChecksum += *pLoc++;

            //  suspend after processing 1KB data so same priority
            //  tasks can process
            if ( !(flashAddrNdx % 256) ) 
            {
            // $[TI3]
                Task::Yield();
            }
            // $[TI4]
        }
#endif

        if ( additiveChecksum !=
             pFlashMemory->checksumBlock[ flashBlockNdx ].additiveChecksum )
        {
        // $[TI1]
            errorCode = (Uint16)flashBlockNdx;
            testStatus = FAILURE;
        }
        // $[TI2]
    }

    return( testStatus );
}

#if defined( SIGMA_UNIT_TEST )

////////////////////////////////////////////////////////////////////////////////

void
BackgroundMemoryApp::SetupMemoryBlock_( void )
{
    const Uint16   MAX_CHKSUM_SPACE = 1024;
    Uint32         chksumSpace[ MAX_CHKSUM_SPACE ];
 
    EepromStruct flashMemoryLayout;
 
    for ( Uint16 tmpCtr = 0; tmpCtr < MAX_CHKSUM_SPACE; ++tmpCtr )
    {
         chksumSpace[ tmpCtr ] = 0;
    }
 
    chksumSpace[ 0 ] = 1;
 
    flashMemoryLayout.numChecksums = 8;
 
    for ( tmpCtr = 0; tmpCtr < flashMemoryLayout.numChecksums; ++tmpCtr )
    {
        flashMemoryLayout.checksumBlock[ tmpCtr ].length = MAX_CHKSUM_SPACE;
        flashMemoryLayout.checksumBlock[ tmpCtr ].pStartAddress = chksumSpace;
        flashMemoryLayout.checksumBlock[ tmpCtr ].additiveChecksum = 1;
        flashMemoryLayout.checksumBlock[ tmpCtr ].crc32Checksum = 0;
    }
 
    PFLASH_CHECKSUM_TABLE = &flashMemoryLayout;
} 
////////////////////////////////////////////////////////////////////////////////

#endif // if defined( SIGMA_UNIT_TEST )
 

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//  and 'lineNumber' are essential pieces of information.  The
//  'pFileName' and 'pPredicate' strings may be defaulted in the macro
//  to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//  None 
//---------------------------------------------------------------------
//@ PostCondition 
//  None 
//@ End-Method 
//===================================================================== 

void
BackgroundMemoryApp::SoftFault( const SoftFaultID  softFaultID,
                                const Uint32       lineNumber,
                                const char*        pFileName,
                                const char*        pPredicate )
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, SAFETY_NET, BACKGROUNDMEMORYAPP,
                          lineNumber, pFileName, pPredicate);
}
