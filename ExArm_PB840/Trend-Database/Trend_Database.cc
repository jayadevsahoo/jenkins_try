#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

// =========================== M O D U L E   D E S C R I P T I O N ====
//@ Filename: Trend_Database.cc - Initialize TrendDataMgr
//---------------------------------------------------------------------
//@ Interface-Description
// Called from System Initialization prior to the GUI Application
// running, the functions in this file will setup and initialize the
// TrendDataMgr.
//---------------------------------------------------------------------
//@ Module-Decomposition
//	TrendDatabaseInitialize
//---------------------------------------------------------------------
//@ Rationale
// initialization code
//---------------------------------------------------------------------
//@ Implementation-Description
// The function in this file performs the initialization of TrendDataMgr
//---------------------------------------------------------------------
//@ Fault-Handling
//  none
//---------------------------------------------------------------------
//@ Restrictions
//  Must be called before TrendDataMgr::Task() execution
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Trend-Database/vcssrc/Trend_Database.ccv   25.0.4.0   19 Nov 2013 14:34:42   pvcs  $
//
//@ Modification-Log
//
//
//  Revision: 001  By:  MJF    Date:  27-Feb-2007    SCR Number: 6237
//       Project:  Trend
//       Description:
//             Initialize TrendDataMgr
//=====================================================================

#include "Trend_Database.hh"
#include "TrendDataMgr.hh"




//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function:  TrendDatabaseInitialize
//
//@ Interface-Description
//  Initialization to support the TrendDataMgr
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls the static TrendDataMgr::Initialize() method
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Free-Function
//=====================================================================
void TrendDatabaseInitialize(void)
{
	TrendDataMgr::Initialize();
}




