/***************************************************************************
*   Source File Part Number:  4-075472-00
*   Source File Name:  softdlic.h
*   Purpose: Function prototypes and declared identifiers for the agreements panel
*
*   Modification History:
*
*    Source Code Revision  Date of Revision  Author of Modification  Specific reasons and comments for the change
*    --------------------  ----------------  ----------------------  --------------------------------------------
*		 Rev B                 14 Jun 1999       Dave G. Richardson      New agreements panel
*
**************************************************************************/

#ifndef _AGREE_INCLUDED
#define _AGREE_INCLUDED

#include <userint.h>
#include "globals.h"

#ifdef __cplusplus
    extern "C" {
#endif

//-------------------------------------------------------------------------
//  Function Prototypes
//-------------------------------------------------------------------------

HPNL  MakeAgreePanel(HPNL hParentPanel);

//-------------------------------------------------------------------------
// Prototype callbacks for Panel: AGREE
// ------------------------------------------------------------------------


//-------------------------------------------------------------------------------------------------
// Declare identifiers for Panel:  AGREE
// ------------------------------------------------------------------------------------------------

extern int AGREE_LICENSE_TEXT     ; // control identifier
extern int AGREE_CMD_AGREE_YES    ; // control identifier
extern int AGREE_CMD_AGREE_NO     ; // control identifier
extern int AGREE_TXT_LINE_1       ; // control identifier

#ifdef __cplusplus
    }
#endif

#endif // _AGREE_INCLUDED
