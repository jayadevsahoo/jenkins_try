// TestResultSet.h : Implementation of the CTestResultSet class



// CTestResultSet implementation

// code generated on Tuesday, September 22, 2009, 12:00 PM

#include "stdafx.h"
#include "TestResultSet.h"
IMPLEMENT_DYNAMIC(CTestResultSet, CRecordset)

CTestResultSet::CTestResultSet(CDatabase* pdb)
	: CRecordset(pdb)
{
	m_test_result_id = 0;
	m_test_session_id = "";
	m_test_data = "";
	m_test_start_time;
	m_test_stop_time;
	m_test_data_type = "";
	m_test_data_units = "";
	m_test_data_stroage_type = "";
	m_test_status = "";
	m_filename = "";
	m_test_data_min = 0.0;
	m_test_data_max = 0.0;
	m_test_data_derived_from = "";

	m_script_start_time;
	m_script_stop_time;

	m_nFields = 16;
	m_nDefaultType = dynaset;

	m_test_comment = "";
}

CString CTestResultSet::GetDefaultConnect()
{
	return _T("DSN=PostgreSQL30;DATABASE=viking_integration_test;SERVER=127.0.0.1;PORT=5432;UID=postgres;PWD=its4me@1234;SSLmode=disable;ReadOnly=0;Protocol=7.4;FakeOidIndex=0;ShowOidColumn=0;RowVersioning=0;ShowSystemTables=0;ConnSettings=;Fetch=100;Socket=4096;UnknownSizes=0;MaxVarcharSize=255;MaxLongVarcharSize=8190;Debug=0;CommLog=0;Optimizer=0;Ksqo=1;UseDeclareFetch=0;TextAsLongVarchar=1;UnknownsAsLongVarchar=0;BoolsAsChar=1;Parse=0;CancelAsFreeStmt=0;ExtraSysTablePrefixes=dd_;;LFConversion=1;UpdatableCursors=1;DisallowPremature=0;TrueIsMinus1=0;BI=0;ByteaAsLongVarBinary=0;UseServerSidePrepare=0;LowerCaseIdentifier=0;XaOpt=1");
//	return _T("DSN=PostgreSQL30;DATABASE=viking_integration_test;SERVER=127.0.0.1;PORT=5432;UID=michael.ekaireb;PWD=its4me;SSLmode=disable;ReadOnly=0;Protocol=7.4;FakeOidIndex=0;ShowOidColumn=0;RowVersioning=0;ShowSystemTables=0;ConnSettings=;Fetch=100;Socket=4096;UnknownSizes=0;MaxVarcharSize=255;MaxLongVarcharSize=8190;Debug=0;CommLog=0;Optimizer=0;Ksqo=1;UseDeclareFetch=0;TextAsLongVarchar=1;UnknownsAsLongVarchar=0;BoolsAsChar=1;Parse=0;CancelAsFreeStmt=0;ExtraSysTablePrefixes=dd_;;LFConversion=1;UpdatableCursors=1;DisallowPremature=0;TrueIsMinus1=0;BI=0;ByteaAsLongVarBinary=0;UseServerSidePrepare=0;LowerCaseIdentifier=0;XaOpt=1");
}

CString CTestResultSet::GetDefaultSQL()
{
	return _T("[TestResults].[test_result]");
}

void CTestResultSet::DoFieldExchange(CFieldExchange* pFX)
{
	pFX->SetFieldType(CFieldExchange::outputColumn);
// Macros such as RFX_Text() and RFX_Int() are dependent on the
// type of the member variable, not the type of the field in the database.
// ODBC will try to automatically convert the column value to the requested type
	RFX_BigInt(pFX, _T("[test_result_id]"), m_test_result_id);
	RFX_Text(pFX, _T("[test_session_id]"), m_test_session_id);
	RFX_Text(pFX, _T("[test_data]"), m_test_data);
	RFX_Date(pFX, _T("[test_start_time]"), m_test_start_time);
	RFX_Date(pFX, _T("[test_stop_time]"), m_test_stop_time);
	RFX_Text(pFX, _T("[test_data_type]"), m_test_data_type);
	RFX_Text(pFX, _T("[test_data_units]"), m_test_data_units);
	RFX_Text(pFX, _T("[test_data_stroage_type]"), m_test_data_stroage_type);
	RFX_Text(pFX, _T("[test_status]"), m_test_status);
	RFX_Text(pFX, _T("[filename]"), m_filename);
	RFX_Single(pFX, _T("[test_data_min]"), m_test_data_min);
	RFX_Single(pFX, _T("[test_data_max]"), m_test_data_max);
	RFX_Text(pFX, _T("[test_data_derived_from]"), m_test_data_derived_from);
	RFX_Date(pFX, _T("[script_start_time]"), m_script_start_time);
	RFX_Date(pFX, _T("[script_stop_time]"), m_script_stop_time);
	RFX_Text(pFX, _T("[test_comment]"), m_test_comment);
}
/////////////////////////////////////////////////////////////////////////////
// CTestResultSet diagnostics

#ifdef _DEBUG
void CTestResultSet::AssertValid() const
{
	CRecordset::AssertValid();
}

void CTestResultSet::Dump(CDumpContext& dc) const
{
	CRecordset::Dump(dc);
}
#endif //_DEBUG


