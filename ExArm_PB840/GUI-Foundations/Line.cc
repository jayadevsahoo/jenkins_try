#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: Line - This class implements a line on the screen.
//---------------------------------------------------------------------
//@ Interface-Description
// The Line class manages line drawing on the screen.  The application
// specifies the position of a line object within its Container relative
// to the Container's upper left corner.  The start point, end point,
// and pen width are set by the application at construction time, or
// individually later using the public methods provided.  Color is set
// using the setColor() method inherited from parent class Drawable.
//
// The Line class is derived from the Drawable class, and thus is
// "aware" of the screen management framework.
//---------------------------------------------------------------------
//@ Rationale
// It was deemed necessary to include this class in the GUI foundations
// library.
//---------------------------------------------------------------------
//@ Implementation-Description
// This class maintains internal variables that specify the position and
// pen width attributes of a Line object.  It uses the VGA graphics
// library to draw the line.  It accepts calls to its draw() method from
// the screen management framework.  Changes to a line will not result
// in changes on the screen until the application calls the
// BaseContainer::repaint() method, signaling the framework to update
// the screen.
//
// Although this class inherits the Drawable::area_ attribute,
// Drawable::area_ is not used to specify the line position.  An Area
// cannot be used to represent all possible lines, because it is
// constrained to have a positive area (area_.x1 <= area_.x2, area_.y1
// <= area_.y2).  This means that a line starting at (x1,y1) and going
// to (x2,y2) would always have a negative slope.  Therefore, this class
// adds its own separate x1_, y1_, x2_, y2_ coordinates, and sets the
// Drawable::area_ attribute based on these.
//
// The Drawable::area_ is also "swelled" as needed to reflect the pen
// width being used.  Note that as the pen width is changed, the
// specified line position stays in the middle of the line -- the line
// "swells" around the defined position.  This can cause the
// Drawable::area_ to increase, which is required for correct handling
// of screen repaint.
//---------------------------------------------------------------------
//@ Fault-Handling
// This class uses the "contract programming" technique.  This technique
// utilizes the class assertion and class precondition macros provided by
// the Utilities subsystem to generate error messages in response to software
// faults.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Foundations/vcssrc/Line.ccv   25.0.4.0   19 Nov 2013 14:11:44   pvcs  $
//
//@ Modification-Log
//
//  Revision: 011   By: gdc    Date:  21-May-2007    SCR Number: 6330
//  Project: Trend
//  Description:
//	Removed SUN prototype code.
// 
//  Revision: 010   By: gdc    Date:  09-Oct-2000    DCS Number: 5753
//  Project: Delta
//  Description:
//	Implemented Single Screen option.
//
//  Revision: 009   By: gdc    Date:  29-Jun-1999    DCS Number: 5337
//  Project: 840 Neonatal
//  Description:
//	Optimized VGA library for Neonatal project to use BitBlt and Windows
//  Bitmap structures.
//
//  Revision: 008   By: sah    Date: 11-Jan-1999   DR Number: 5321
//  Project:  Sigma (R8027)
//  Description:
//      Changed 'SoftFault()' method to non-inline method.
//
//  Revision: 007  By: gdc     Date: 16-Jun-1998     DCS Number:
//  Project:  Color
//  Description:
//      Initial release.
//
//  Revision: 006  By: sah       Date: 06-Oct-1997    DCS Number: 2264
//  Project:  Sigma (R8027)
//  Description:
//      Fixed incorrect handling of VGA access requests/releases, which
//      may have caused the problem with the errant lines outside of the
//      waveform grid, by removing all semaphore requests/releases from
//      all of the 'draw()' methods.
//
//  Revision: 005  By: sah       Date: 12-Jun-1997    DCS Number: 2208
//    Project:  Sigma (R8027)
//    Description:
//      Incorporated new VGA interface methods, files and types.
//
//  Revision: 004  By: clw      Date: 03-Sep-1996   DCR Number: 10046
//    Project:  Sigma (R8027)
//    Description:
//      Changed ::draw() to use new fat line routines from VGA-Graphics-Driver
//
//  Revision: 003  By: gdc      Date: 09-May-1996   DCR Number: 981
//    Project:  Sigma (R8027)
//    Description:
//      Corrected minor syntax errors detected by new compiler.
//
//  Revision: 002  By: mpm      Date: 29-Mar-1996   DCR Number: 473
//    Project:  Sigma (R8027)
//    Description:
//      Added calls to GuiFoundation::RequestDisplayAccess() and ReleaseDisplayAccess()
//      in order to allow shared access to the display by separate drawing tasks.
//
//  Revision: 001  By: yyy      Date: 01-Jan-1995  DR Number: NONE
//    Project:  Sigma (R8027)
//    Description:
//	Integration baseline.
//=====================================================================

#include "Line.hh"

//@ Usage-Classes
#include "BaseContainer.hh"
#include "Container.hh"
#include "GraphicsContext.hh"
#include "DisplayContext.hh"
//@ End-Usage

//@ Code...


// Initialize static data members
const Int Line::MIN_PEN_WIDTH=1;
const Int Line::MAX_PEN_WIDTH=3;


//=====================================================================
//
// Public methods
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Line [no arguments]
//
//@ Interface-Description
// Default constructor for the Line class.  Assigns default values to the start
// and end points: start point is at (0,0), end is at (1,1).  The pen width
// (which is the line width) defaults to 1.  These coordinates are relative to
// the origin (upper left) of the parent container.
//---------------------------------------------------------------------
//@ Implementation-Description
// Initializes all data members to their default values then computes the
// bounding box using resetArea_().
//---------------------------------------------------------------------
//@ PreCondition
// none.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Line::Line() 
{
	CALL_TRACE("Line::Line(void)");
													// $[TI1]
	
	x1_ = 0;
	y1_ = 0;
	x2_ = 1;
	y2_ = 1;
	penWidth_ = 1;

	//
	// Update Drawable::area_ based on the default endpoints and pen width.
	//
	resetArea_();

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Line [overloaded constructor - 5 arguments]
//
//@ Interface-Description
// Line takes 5 arguments in this constructor.  The start point is given by
// "xStart" and "yStart". The end point is given by "xEnd" and "yEnd".  These
// coordinates are relative to the origin (upper left) of the parent container.
// The "penWidth" represents the line width, which defaults to 1 (single pixel
// line).
//---------------------------------------------------------------------
//@ Implementation-Description
// Initializes all data members to the given values then computes the
// bounding box using resetArea_().
//---------------------------------------------------------------------
//@ PreCondition
// Checks that the given "penWidth" is in the legal range.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Line::Line(Int xStart, Int yStart, Int xEnd, Int yEnd, Int penWidth) : 
	x1_(xStart),
	y1_(yStart),
	x2_(xEnd),
	y2_(yEnd),
	penWidth_(penWidth)
{
	CALL_TRACE(
			"Line::Line(Int xStart, Int yStart, Int xEnd, Int yEnd, Int pen)");

	CLASS_PRE_CONDITION(penWidth_ >= MIN_PEN_WIDTH &&
						penWidth_ <= MAX_PEN_WIDTH);

	//
	// Update Drawable::area_ based on the endpoints and pen width.
	//
	resetArea_();									// $[TI2]

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~Line  
//
//@ Interface-Description
// Destroy object.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This destructor does nothing.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Line::~Line(void)
{
	CALL_TRACE("Line::~Line(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setStartPoint	[virtual]
//
//@ Interface-Description
// Sets the start point of this Line to the passed "x1","y1" coordinates.
//---------------------------------------------------------------------
//@ Implementation-Description
// The start point represents the pixel where the line is to begin,
// specified relative to the upper left corner of the parent Container
// object which contains the line.  The resetArea_() method updates the
// Drawable::area_.  This method changes the object's state and causes a
// redraw.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
Line::setStartPoint(Int x1, Int y1)
{
	CALL_TRACE("Line::setStartPoint(Int x1, Int y1)");

	x1_ = x1;										// $[TI1]
	y1_ = y1;
	resetArea_();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setEndPoint 	[virtual]
//
//@ Interface-Description
// Sets the end point of this Line to the given "x1","y1" coordinate.
//---------------------------------------------------------------------
//@ Implementation-Description
// The end point represents the pixel where the line is to end,
// specified relative to the upper left corner of the parent Container
// object which contains the line.  The resetArea_() method updates the
// Drawable::area_.  This method changes the object's state and causes a
// redraw.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
Line::setEndPoint(Int x2, Int y2)
{
	CALL_TRACE("Line::setEndPoint(Int x2, Int y2)");

	x2_ = x2;										// $[TI1]
	y2_ = y2;
	resetArea_();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setVertices 	[virtual]
//
//@ Interface-Description
// Sets new vertices for the line.
//---------------------------------------------------------------------
//@ Implementation-Description
// The vertices are
// specified relative to the upper left corner of the parent Container
// object which contains the line.  The resetArea_() method updates the
// Drawable::area_.  This method changes the object's state and causes a
// redraw.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
Line::setVertices(Int x1, Int y1, Int x2, Int y2)
{
	x1_ = x1;
	y1_ = y1;
	x2_ = x2;										// $[TI1]
	y2_ = y2;
	resetArea_();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setPenWidth  	[virtual]
//
//@ Interface-Description
// Sets the line width to the given "penWidth".  The specified width must
// fall within the range defined by class constants MIN_PEN_WIDTH and
// MAX_PEN_WIDTH.
//---------------------------------------------------------------------
//@ Implementation-Description
// Updates the penWidth_ data member.  This method changes the object's
// state and causes a redraw.  Note that as the pen width is changed, the
// specified line position stays in the middle of the line -- the line
// "swells" around the defined position.  This can cause the
// Drawable::area_ to increase, which is required for correct handling
// of screen repaint.  This change to the area of the line is implemented
// by the call to resetArea_().
//---------------------------------------------------------------------
//@ PreCondition
// Checks that the given "penWidth" is in the legal range.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
Line::setPenWidth(Int penWidth)
{
	CALL_TRACE("Line::setPenWidth(Int penWidth)");

	CLASS_PRE_CONDITION(penWidth >= MIN_PEN_WIDTH && penWidth <= MAX_PEN_WIDTH);

	penWidth_ = penWidth;								// $[TI1]
	
	//
	// Changing the pen width can affect the screen area occupied by the
	// line -- therefore, resetArea_() must be called.
	//
	resetArea_();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setX 	[virtual]
//
//@ Interface-Description
// Redefines the inherited Drawable::setX() method move the line's
// vertices and its bounding area to a new X location.
//---------------------------------------------------------------------
//@ Implementation-Description
// Computes the delta between the old X and the new X position and 
// applies this delta to each of the line's vertices.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
Line::setX(Int newX)
{
	CALL_TRACE("setX(Int)");
	// $[TI1]

	changing();
	Int deltaX = newX - getX();
	x1_ += deltaX;
	x2_ += deltaX;
	area_.setX(newX);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setY 	[virtual]
//
//@ Interface-Description
// Redefines the inherited Drawable::setY() method reset the line's
// vertices and its bounding area to a new Y location.
//---------------------------------------------------------------------
//@ Implementation-Description
// Computes the delta between the old Y and the new Y position and 
// applies this delta to each of the line's vertices.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
Line::setY(Int newY)
{
	CALL_TRACE("setY(Int)");
	// $[TI1]

	changing();
	Int deltaY = newY - getY();
	y1_ += deltaY;
	y2_ += deltaY;
	area_.setY(newY);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setWidth 	[virtual]
//
//@ Interface-Description
// Redefines the virtual Drawable::setWidth() method to generate an error.
//---------------------------------------------------------------------
//@ Implementation-Description
// This redefinition of Drawable::setWidth() effectively prevents the
// application from changing the width of the inherited area_.  Use the
// setStartPoint() or setEndPoint() method instead.
//
// Since the area occupied by a Line is fully determined by the line's
// start/end points and pen width, this class has taken over management of the
// inherited area_ attribute.  If the application was allowed to change it, it
// could be changed into an illegal state.
//
// If called, this method will generate an error.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
Line::setWidth(Int)
{
	CALL_TRACE("Line::setWidth(Int)");

	CLASS_ASSERTION_FAILURE();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setHeight 	[virtual]
//
//@ Interface-Description
// Redefines the virtual Drawable::setHeight() method to generate an error.
//---------------------------------------------------------------------
//@ Implementation-Description
// This redefinition of Drawable::setHeight() effectively prevents the
// application from changing the height of the inherited area_.  Use the
// setStartPoint() or setEndPoint() method instead.
//
// Since the area occupied by a Line is fully determined by the line's
// start/end points and pen width, this class has taken over management of the
// inherited area_ attribute.  If the application was allowed to change it, it
// could be changed into an illegal state.
//
// If called, this method will generate an error.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
Line::setHeight(Int)
{
	CALL_TRACE("Line::setHeight(Int)");

	CLASS_ASSERTION_FAILURE();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: draw 	[virtual]
//
//@ Interface-Description
// This method draws the line on the VGA display.  The passed clipArea
// parameter is ignored.
//---------------------------------------------------------------------
//@ Implementation-Description
// This method converts its Container-relative coordinates to real screen
// coordinates, then draws the line using functions from the
// VGA-Graphics-Driver subsystem.  There are three line-drawing functions
// available: one for vertical lines, one for horizontal lines, and one
// (slower) for arbitrary lines.
//
// Note that when the pen width is > 1, the specified line position
// falls in the center of the "fat" line.  If the line width is an even
// number, the specified line position can't be in the center, but is
// adjacent to the center, on the side closer to the origin.
//
// In the case of sloped lines of pen width > 1, a different set of
// single-width lines is drawn for each pen width; the VGA-Graphics-Driver
// subsystem only supports single-pixel-width line drawing.  The start and end
// points of these lines form a pattern surrounding the specified endpoints,
// that is designed to produce a fairly uniform line width at a variety of line
// slopes.  This pattern is different for each line width.  For line width 2,
// it consists of a "square" of 4 adjacent pixels; for line width 3, it
// contains 5 pixels arranged like a plus sign.
//
// Note: the result of the different handling for sloped lines versus
// horizontal and vertical lines, the "look" of the ends are different.
// Sloped lines have a "rounded" look, while horizontal and vertical lines
// have square ends.
//
// Line widths greater than MAX_PEN_WIDTH are not supported.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
Line::draw(const Area&)
{
	CALL_TRACE("Line::draw(const Area&)");

    DisplayContext & rDc = getBaseContainer()->getDisplayContext();
    GraphicsContext & rGc = getBaseContainer()->getGraphicsContext();

	//
	// Compute start and end point in real (absolute) coordinates.
	//
	Int x1 = getParentContainer()->convertToRealX(x1_);
	Int y1 = getParentContainer()->convertToRealY(y1_);
	Int x2 = getParentContainer()->convertToRealX(x2_);
	Int y2 = getParentContainer()->convertToRealY(y2_);

	rGc.setForegroundColor( getColor() );
	rGc.setLineWidth( penWidth_ );

	rDc.drawLine(rGc, x1, y1, x2, y2);
	
}



//=====================================================================
//
// Private methods
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: resetArea_ 
//
//@ Interface-Description
// Calculates the area occupied by the line object and updates the
// inherited Drawable::area_ data member.
//---------------------------------------------------------------------
//@ Implementation-Description
// Uses the start and end points to calculate the area.  This area must
// be "swelled" to reflect the pen width, to ensure that the correct
// area is redrawn when the repaint() method is called.  This method
// changes the object's state and causes a redraw.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
Line::resetArea_(void)
{
	CALL_TRACE("Line::resetArea_(void)");

	changing();
													// $[TI1]
	//
	// The area_ must be "swelled" on all sides by half the pen width.
	//

	// This formula implements the chosen strategy for how a line's area is
	// supposed to "swell" as the line width is increased.  It ensures that for
	// odd line widths, the area swells equally on all sides, while for even
	// line widths, it swells 1 pixel more on the right and lower edges than on
	// the left and upper edges.  If the line width is 1, there is no swelling.
	Int halfPenWidth = getHalfPenWidth_();

	area_.setX(MIN_VALUE(x1_, x2_) - halfPenWidth); 
	area_.setY(MIN_VALUE(y1_, y2_) - halfPenWidth); 
	area_.setWidth(ABS_VALUE(x1_ - x2_) + penWidth_); 
	area_.setHeight(ABS_VALUE(y1_ - y2_) + penWidth_); 
}


#if defined SIGMA_UNIT_TEST

//=====================================================================
//
// Public methods
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getPenWidth_    [static]
//
//@ Interface-Description
// This function returns the value of the penWidth_.
//
//---------------------------------------------------------------------
//@ Implementation-Description
// The value of penWidth_ is returned.
//
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

Int
Line::getPenWidth_(void)
{
	CALL_TRACE("Line::getPenWidth_(void");

	return(penWidth_);
}


#endif	// SIGMA_UNIT_TEST


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================
void
Line::SoftFault(const SoftFaultID  softFaultID,
				const Uint32       lineNumber,
				const char*        pFileName,
				const char*        pPredicate)  
{
	CALL_TRACE("Line::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_FOUNDATIONS, LINE,
							lineNumber, pFileName, pPredicate);
}
