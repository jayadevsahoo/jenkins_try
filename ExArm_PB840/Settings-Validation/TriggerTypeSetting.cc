#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//============== C L A S S   D E S C R I P T I O N ====================
//@ Class:  TriggerTypeSetting - Trigger Type Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, discrete setting that indicates whether inspirations
//  are triggered on the basis of flow or pressure.  This setting is also
//  a Main Control Setting, and, therefore, activates the Transition Rules
//  of the settings that are dependent on this setting's transitions.  This
//  class inherits from 'DiscreteSetting' and provides the specific
//  information needed for the representation of trigger type.  This
//  information includes the set of values that this setting can have (see
//  'TriggerTypeValue.hh'), this setting's activation of Transition Rules
//  (see 'calcTransition()'), and this batch setting's new-patient value.
//
//  Because this is a Main Control Setting, the changing of this setting
//  is done differently than the other settings.  The direct changing of a
//  Main Control Setting does not update the settings that are dependent on
//  its value -- unlike those settings that have dependent settings and
//  override 'calcDependentValues_()'.  The updating of the "dependent"
//  settings of Main Control Settings is done by the Adjusted Context AFTER
//  all of the Main Control Settings have been changed and approved by the
//  operator.  This update is done via the Transition Rules defined for this
//  setting (see 'calcTransition()').
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class has no data members, and no dependent settings.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/TriggerTypeSetting.ccv   25.0.4.0   19 Nov 2013 14:27:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 011   By: gdc    Date: 16-Aug-2010     SCR Number: 6663
//  Project: API/MAT
//  Description:
//  Added support for VentSet API.
// 
//  Revision: 010   By: gdc    Date: 31-Dec-2008    SCR Number: 6177
//  Project:  840S
//  Description:
//      Restructured class to correctly revert to the user-selected value 
// 		when an "observed" setting has forced a change to the value of
// 		this setting. Changed getNewPatient method to provide default value 
// 		for	this setting based on the settings it depends upon. Implemented
// 		derived method setAdjustedValue to save the user-selected value for
// 		so it can be used by valueUpdate in handling changes to higher level
// 		settings such as vent-type.
//
//  Revision: 009  By: gdc    Date: 20-Feb-2005    SCR Number: 6144
//  Project:  NIV1
//  Description:
//      NIV mods. Added SRS requirement numbers.  
//
//  Revision: 008  By: gdc    Date: 18-Feb-2005    SCR Number: 6144
//  Project:  NIV1
//  Description:
//      Changes to support NIV.
//
//  Revision: 007   By: sah    Date: 19-Apr-2000    DR Number: 5687
//  Project:  NeoMode
//  Description:
//      Pressure triggering is no longer allowed for NEONATAL circuit
//      types.  To ensure this, the applicability of this setting will
//      now be "VIEWABLE", which results in its button being flat
//      (unchangeable).
//
//  Revision: 006   By: sah    Date: 29-Jun-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	Incorporated initial specifications for NeoMode Project.
//
//  Revision: 005   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'getApplicability()' method
//
//  Revision: 004   By: sah  Date:  11-Jan-1999    DR Number:  5321
//  Project:  ATC
//  Description:
//	As part of making all 'SoftFault()' methods non-inlined, I'm
//	also making those 'SoftFault()' methods that are NOT called
//	in PRODUCTION mode, DEVELOPMENT-only.
//
//  Revision: 003   By: dosman    Date:  07-Apr-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Fixed incorrect default value caused by getNewPatientValue()
//	returning SettingValue: casted it to DiscreteValue
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Initial version.
//
//=====================================================================

#include "TriggerTypeSetting.hh"
#include "TriggerTypeValue.hh"
#include "AdjustedContext.hh"
#include "ContextMgr.hh"
#include "PatientCctTypeValue.hh"
#include "VentTypeValue.hh"

//@ Usage-Classes
#include "SettingsMgr.hh"
//@ End-Usage

//@ Code...
static const DiscreteParameterItem ParameterLookup_[] = 
{
	{"FLOW", TriggerTypeValue::FLOW_TRIGGER},
	{"PRES", TriggerTypeValue::PRESSURE_TRIGGER},
	{NULL , NULL}
};

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  TriggerTypeSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02256] -- setting range
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

TriggerTypeSetting::TriggerTypeSetting(void)
: BatchDiscreteSetting(SettingId::TRIGGER_TYPE,
					   Setting::NULL_DEPENDENT_ARRAY_,
					   TriggerTypeValue::TOTAL_TRIGGER_TYPES),
	isChangeForced_(FALSE),
	operatorSelectedValue_(NO_SELECTION_)
{
	setLookupTable(ParameterLookup_);
}	// $[TI1]


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method:  ~TriggerTypeSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

TriggerTypeSetting::~TriggerTypeSetting(void)
{
	CALL_TRACE("~TriggerTypeSetting(void)");
}

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  setAdjustedValue(newAdjustedValue)  [virtual]
//
//@ Interface-Description
//  This virtual method is called to set the adjusted value of this 
//  setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls the Setting base class method to actually store the new value
//  in the adjusted context. 
//---------------------------------------------------------------------
//@ PreCondition
//  isEnabledValue(DiscreteValue(newValue))
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void TriggerTypeSetting::setAdjustedValue(const SettingValue& newValue)
{
	AUX_CLASS_ASSERTION(isEnabledValue(DiscreteValue(newValue)), DiscreteValue(newValue));

	// call the base class to set the value into the adjusted context
	Setting::setAdjustedValue(newValue);

	if (!isChangeForced_)
	{
		operatorSelectedValue_ = newValue;
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Trigger type is ALWAYS changeable.
//
//  $[01080] -- main control setting applicability
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id TriggerTypeSetting::getApplicability(const Notification::ChangeQualifier qualifierId) const
{
	CALL_TRACE("getApplicability(qualifierId)");

	const Setting*  pCctType =
		SettingsMgr::GetSettingPtr(SettingId::PATIENT_CCT_TYPE);
	const Setting*  pVentType =
		SettingsMgr::GetSettingPtr(SettingId::VENT_TYPE);

	DiscreteValue  cctTypeValue;
	DiscreteValue  ventTypeValue;

	switch ( qualifierId )
	{
		case Notification::ACCEPTED :	// $[TI1]
			cctTypeValue = pCctType->getAcceptedValue();
			ventTypeValue = pVentType->getAcceptedValue();
			break;
		case Notification::ADJUSTED :	// $[TI2]
			cctTypeValue = pCctType->getAdjustedValue();
			ventTypeValue = pVentType->getAdjustedValue();
			break;
		default :
			AUX_CLASS_ASSERTION_FAILURE(qualifierId);
			break;
	}

	Applicability::Id  applicability;

	switch ( cctTypeValue )
	{
		case PatientCctTypeValue::ADULT_CIRCUIT :
		case PatientCctTypeValue::PEDIATRIC_CIRCUIT : // $[TI3]
			switch ( ventTypeValue )
			{
				case VentTypeValue::INVASIVE_VENT_TYPE :  // $[TI3.1]
					applicability = Applicability::CHANGEABLE;
					break;
					// since P-trig is not allowed with an NIV circuit type, and the
					// new-patient value is V-trig, just ensure that this setting can't be
					// changed...
				case VentTypeValue::NIV_VENT_TYPE:	// $[TI3.2]
					applicability = Applicability::VIEWABLE;
					break;
				default:
					AUX_CLASS_ASSERTION_FAILURE(ventTypeValue);
			}
			break;
		case PatientCctTypeValue::NEONATAL_CIRCUIT :  // $[TI4]
			// since P-trig is not allowed with a NEONATAL circuit type, and the
			// new-patient value is V-trig, just ensure that this setting can't be
			// changed...
			applicability = Applicability::VIEWABLE;
			break;
		default :
			AUX_CLASS_ASSERTION_FAILURE(cctTypeValue);
			break;
	}

	return(applicability);
}

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  isEnabledValue()  [const, virtual]
//
//@ Interface-Description
//  Is this setting's value given by 'value', a currently-enabled value?
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[01080] -- 'P-Trig' restricted to invasive non-neonatal ventilation
//---------------------------------------------------------------------
//@ PreCondition
//  none 
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean TriggerTypeSetting::isEnabledValue(const DiscreteValue value) const
{
	Boolean  isEnabled;

	switch ( value )
	{
		case TriggerTypeValue::FLOW_TRIGGER :		  // $[TI1.1] 
			{
				// this standard, unrestricted values is ALWAYS enabled...
				isEnabled = TRUE;
				break;
			}

		case TriggerTypeValue::PRESSURE_TRIGGER :	  // $[TI1.2]
			{
				// -- restricted to invasive, non-neonatal circuit
				const DiscreteValue  VENT_TYPE =
					SettingsMgr::GetSettingPtr(SettingId::VENT_TYPE)->getAdjustedValue();
				const DiscreteValue  CCT_TYPE =
					SettingsMgr::GetSettingPtr(SettingId::PATIENT_CCT_TYPE)->getAdjustedValue();

				// $[2.1] $[TI2.2]
				isEnabled = (VENT_TYPE == VentTypeValue::INVASIVE_VENT_TYPE) &&
							(CCT_TYPE != PatientCctTypeValue::NEONATAL_CIRCUIT);
				break;
			}

		default :
			// unexpected mandatory type value...
			AUX_CLASS_ASSERTION_FAILURE(value);
			break;
	};

	return(isEnabled);
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  calcTransition(toValue, fromValue)  [virtual]
//
//@ Interface-Description
//  The purpose of this method is to inform the settings effected
//  by a state transition of this setting. So they can adjust
//  their value(s) appropriately.
//---------------------------------------------------------------------
//@ Implementation-Description
//  There are no Transition Rules for the Trigger Type Setting, yet.  This
//  method is provided, and is called, so that the fact that there is no
//  transition needed, is only "known" by this setting.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void TriggerTypeSetting::calcTransition(const DiscreteValue,
										const DiscreteValue)
{
	CALL_TRACE("calcTransition(toValue, fromValue)");

	// do nothing...
}

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  calcNewValue(knobDelta)  [virtual]
//
//@ Interface-Description
//  Calculate, and initialize to, the new discrete value.  The value of
//  'knobDelta' is only used in so far as the "direction" of change; the
//  value, itself, is ignored a delta of '1' is applied in the direction
//  of change.
//
//  This does some specific work for this class, then forwards on to base
//  class's implementation.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

const BoundStatus& TriggerTypeSetting::calcNewValue(const Int16 knobDelta)
{
	CALL_TRACE("calcNewValue(knobDelta)");

	// forward to base class...
	return(DiscreteSetting::calcNewValue(knobDelta));
}

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [const, virtual]
//
//@ Interface-Description
//  Returns this setting's new-patient (default) value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns a default setting value for trigger type. The default is 
//  always flow triggering.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

SettingValue TriggerTypeSetting::getNewPatientValue(void) const
{
	CALL_TRACE("getNewPatientValue()");

	// $[02257] The setting's new-patient value ...
	return(DiscreteValue(TriggerTypeValue::FLOW_TRIGGER));	// $[TI1]
}

//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method:  resetState()  [virtual]
//
//@ Interface-Description
//  Reset the state of this setting, preparing it for changes.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void TriggerTypeSetting::resetState(void)
{
	CALL_TRACE("resetState()");

	// forward up to base class...
	Setting::resetState();

	operatorSelectedValue_ = getAcceptedValue();
}

//==================== M E T H O D   D E S C R I P T I O N =============
//@ Method:  valueUpdate(qualifierId, pSubject)  [virtual]
//
//@ Interface-Description
//  Respond to vent type value changes, by, possibly, updating mode
//  applicability and value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[NI02010]\d\ -- from Invasive to NIV, if pressure change to flow trigger
//---------------------------------------------------------------------
//@ PreCondition
//  (pSubject->getId() == SettingId::VENT_TYPE)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void TriggerTypeSetting::valueUpdate(const Notification::ChangeQualifier qualifierId,
									 const SettingSubject*               pSubject)
{
	AUX_CLASS_PRE_CONDITION(
		(pSubject->getId() == SettingId::VENT_TYPE), pSubject->getId());

	if ( qualifierId == Notification::ADJUSTED )
	{
		const DiscreteValue CURRENT_VALUE = getAdjustedValue();

		isChangeForced_ = TRUE;
		// attempt to revert to the operator selection first if enabled
		if ( operatorSelectedValue_ != NO_SELECTION_ && isEnabledValue(operatorSelectedValue_) )
		{
			if ( CURRENT_VALUE != operatorSelectedValue_ )
			{
				setAdjustedValue(operatorSelectedValue_);
			}
		}
		else
		{
			setAdjustedValue(getNewPatientValue());
		}
		// else - no change required
		isChangeForced_ = FALSE;

		updateApplicability(qualifierId);
	}  // -- don't care about accepted value changes...
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: doRetainAttachment(pSubject)  [virtual, const]
//
//@ Interface-Description
//  When task-level, state changes occur, all subjects are notified
//  to detach from all observers.  This is done because the GUI-Apps
//  observers may be overwritten by new class instances, without any
//  explicit destructor/detachment calls.
//
//  This method is defined here to indicate to any of this instance's
//  subjects, not to detach this observer.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean TriggerTypeSetting::doRetainAttachment(const SettingSubject*) const
{
	CALL_TRACE("doRetainAttachment(pSubject)");

	return(TRUE);
}  // $[TI1]


//==================== M E T H O D   D E S C R I P T I O N ============
//@ Method:  settingObserverInit()  [virtual]
//
//@ Interface-Description
//  This setting needs to monitor the value of Vent Type setting, therefore
//  this virtual method is overridden to provide a point during
//  initialization to attach to that (subject) setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void TriggerTypeSetting::settingObserverInit(void)
{
	CALL_TRACE("settingObserverInit()");

	// monitor changes in vent type value...
	attachToSubject_(SettingId::VENT_TYPE, Notification::VALUE_CHANGED);
}  // $[TI1]



//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//			[static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void TriggerTypeSetting::SoftFault(const SoftFaultID  softFaultID,
								   const Uint32       lineNumber,
								   const char*        pFileName,
								   const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

	FaultHandler::SoftFault(softFaultID, ::SETTINGS_VALIDATION,
							TRIGGER_TYPE_SETTING, lineNumber, pFileName,
							pPredicate);
}
