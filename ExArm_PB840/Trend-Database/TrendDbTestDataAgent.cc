#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: TrendDbTestDataAgent
//---------------------------------------------------------------------
//@ Interface-Description
//  Responsible for simulated test data.
//  Provides a static interface to add simulated waveform data
//  to the database. via the "LoadSimulatedData()" call.
// 
//  Provides a serial command interface for the TDG tool
//  By passing in the command string to processTdgCmd() call.
//  More information can be found in the TDG Manual documentation
//  (resp_mechanics/Non-DHF/Software/TrendDataMgr_TDG.doc)
//---------------------------------------------------------------------
//@ Rationale
//  Need to get direct access to the database for integration level
//  testing, provides
//---------------------------------------------------------------------
//@ Implementation-Description
//  Provides a back door into the database.  This class is a friend of the
//  manager and interacts with its private data to load data into the
//  database.
//---------------------------------------------------------------------
//@ Fault-Handling
//  SoftFault is used
//---------------------------------------------------------------------
//@ Restrictions
//  None
//---------------------------------------------------------------------
//@ Invariants
// None
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Trend-Database/vcssrc/TrendDbTestDataAgent.ccv   25.0.4.0   19 Nov 2013 14:34:42   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By:  mjf    Date:  22-Aug-2006    SCR Number: 6237
//       Project:  Trend
//       Description:
//             Initial version.
//=====================================================================


#include "CompactFlash.hh"			// GUI-IO-Devices
#include "OsTimeStamp.hh"           // OS-Foundation
#include "Task.hh"        			// OS-Foundation

#include "Trend_Database.hh"
#include "TrendDbTestDataAgent.hh"
#include "TrendDataMgr.hh"
#include "TrendEvents.hh"
#include "TrendDbPatientRepository.hh"
#include "TrendDbSettingRepository.hh"
#include "TrendDbControlBlock.hh"	


//@ Code...



//=====================================================================
//
//  Static simulation data
//
//=====================================================================

// TimeStamp Steps in Min/Sec for each timescale
const Uint8
	TrendDbTestDataAgent::SimTimeStepSeconds_[TrendSelectValue::TOTAL_TREND_SELECT_VALUES] = 
{ 10, 20, 40, 20, 0, 0, 0, 0};
const Uint8
	TrendDbTestDataAgent::SimTimeStepMinutes_[TrendSelectValue::TOTAL_TREND_SELECT_VALUES] =
{ 0, 0, 0, 1, 2, 4, 8, 12};

const Real32
	TrendDbTestDataAgent::SimBoundedStep_[TrendSelectValue::TOTAL_TREND_SELECT_VALUES] =
{ 0.3, 0.6, 1.2, 2.1, 2.2, 2.7, 3.3, 3.5};

const Real32 TrendDbTestDataAgent::SimMaxValue_ = 200.0; 
const Real32 TrendDbTestDataAgent::SimMinValue_ = -20.0;

const char TrendDbTestDataAgent::FIELD_SEPARATOR = ',';

const Int16  TrendDbTestDataAgent::NUMBER_BUFFER_LEN = 11;
static char NumberBuffer[ TrendDbTestDataAgent::NUMBER_BUFFER_LEN + 1 ];




//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: NextSimulatedBoundedData [static]
//
//@ Interface-Description
//  Take the oldvalue and apply the step value
//  return the new value
//---------------------------------------------------------------------
//@ Implementation-Description
//  Bounds the value between the SimMinValue and SimMaxValue
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
const TrendValue & TrendDbTestDataAgent::NextSimulatedBoundedData( Int16 scale,
																   const TrendValue & oldValue )
{
	static TrendValue   newValue;

	newValue.data.realValue = oldValue.data.realValue + SimBoundedStep_[ scale ];
	newValue.isValid = TRUE;
	if (newValue.data.realValue > SimMaxValue_)
		newValue.data.realValue = SimMinValue_;

	return( newValue );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: NextSimulatedTimeStamp [static]
//
//@ Interface-Description
//  Take the oldvalue and apply the step value
//  return the new timestamp
//---------------------------------------------------------------------
//@ Implementation-Description
//  simplifies each month to 21 days
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
const TimeStamp & TrendDbTestDataAgent::NextSimulatedTimeStamp( Int16 scale,
																const TimeStamp oldTime )
{
	static TimeStamp newValue;

	// Update the data/time forward
	Int16 initSec = oldTime.getSeconds() + SimTimeStepSeconds_[ scale ];
	Int16 initMin = oldTime.getMinutes() + SimTimeStepMinutes_[ scale ];
	Int16 initHour = oldTime.getHour();
	Int16 initDay = oldTime.getDayOfMonth();
	Int16 initMonth = oldTime.getMonth();
	Int16 initYear = oldTime.getYear();

	// Check the update Time of Day, adjust as needed
	if (initSec > 59)
	{
		initMin++;
		initSec = 0;
	}
	if (initMin > 59)
	{
		initHour++;
		initMin = 0;
	}
	if (initHour > 23)
	{
		initDay++;
		initHour = 0;
	}
	if (initDay > 21)
	{
		initDay = 1;   // all months have are three weeks
	}

	// Assign this new timestamp to the data set
	newValue = TimeStamp(initYear, initMonth, initDay,
						 initHour, initMin, initSec );
	return( newValue );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: FillSimulatedData [static]
//
//@ Interface-Description
//  Fills the database with simulated waveform data
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses the sampleSimulatedData() method in the repositories
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDbTestDataAgent::FillSimulatedData( Uint32 frames,
											  const TimeStamp & startTime )
{
	TrendDataMgr & trendMgr = TrendDataMgr::GetTrendDataMgr(); 
	TrendDbPatientRepository & patientRep = trendMgr.getPatientRepository_();
	TrendDbSettingRepository & settingRep = trendMgr.getSettingRepository_();
	Uint32 simSeqPosNumber = trendMgr.getCurrentSequenceCount_();
	TimeStamp simTimeStamp = startTime;

	TrendValue simTrendValue;
	simTrendValue.data.realValue = 0;
	simTrendValue.isValid = TRUE;


	TREND_MGR_DBG4( 1, "Fill ", (frames / 360),
					" hours of data, start at seq/Frame # ",
					simSeqPosNumber );

	for (Int16 idx = 0; idx < frames; idx++)
	{
		// Setup the array of simulated data
		// skip the first two columns in the patient DB, as these are events
		TrendValue simTrendPatientArray[ TrendDataMgr::TREND_PATIENT_MAX_SLOTS ];
		for (Int16 pdx = 0; pdx < TrendDataMgr::TREND_PATIENT_MAX_SLOTS; pdx ++)
		{
			if (pdx < 2)
			{
				simTrendPatientArray[ pdx ].data.discreteValue = 0;
				simTrendPatientArray[ pdx ].isValid = TRUE;
			}
			else
			{
				simTrendPatientArray[ pdx ] = simTrendValue;
			}
		}
		patientRep.sampleSimulatedData( simSeqPosNumber, simTimeStamp, simTrendPatientArray );

		// Take a setting change once per minute (or every 6th entry)
		if (!(idx % 6))
		{
			TrendValue simTrendSettingArray[ TrendDataMgr::TREND_SETTING_MAX_SLOTS ];
			for (Int16 sdx = 0; sdx < TrendDataMgr::TREND_SETTING_MAX_SLOTS; sdx++)
			{
				simTrendSettingArray[sdx] = simTrendValue;
			}
			settingRep.sampleSimulatedData( simSeqPosNumber, simTimeStamp, simTrendSettingArray );
		}

		// Update sequence count
		trendMgr.setNextSequenceCount_();
		simSeqPosNumber = trendMgr.getCurrentSequenceCount_();
		simTimeStamp = 
			NextSimulatedTimeStamp(TrendTimeScaleValue::TREND_TIME_SCALE_1HR, simTimeStamp );
		simTrendValue = 
			NextSimulatedBoundedData(TrendTimeScaleValue::TREND_TIME_SCALE_1HR, simTrendValue );

		// Output a message on each hour of simulated data
		if (!(idx % 360))
		{
			TREND_MGR_DBG4( 1, "Hour ", (idx / 360),
							", loaded, seq/frame # ", simSeqPosNumber );
		}
	}
	TREND_MGR_DBG1( 1, "Simulation load done\n" );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: LoadSimulatedData [static]
//
//@ Interface-Description
//  Fills the database with simulated data, the number of frames is defaulted
//  to the Max 72 Hours.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Gets the starting frame sequence number and timestamp and 
//  fills the database with simulated waveform data and
//  flush the control block
//  
//---------------------------------------------------------------------
//@ PreCondition
//	Manager must be in the INITIALIZED_IDLE state
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDbTestDataAgent::LoadSimulatedData( Uint32 numFrames )
{
	TrendDataMgr & trendMgr = TrendDataMgr::GetTrendDataMgr();

	// Verify that the manager is in the "Idle" state before
	// filling it with all this simulated data
	if (trendMgr.isState_(TrendDataMgr::INITIALIZED_IDLE))
	{
		TimeStamp startDateTime;
		FillSimulatedData( numFrames, startDateTime );

		// Flush
		TrendDbControlBlock & controlBlock = trendMgr.getControlBlock_();
		controlBlock.flush();
	}
	else
	{
		TREND_MGR_DBG2( 1, "Can't fill database with simulated data while in state ",
						trendMgr.initState_ );
	}
}


//=====================================================================
//
// Public methods - Trend Test Data Agent
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TrendDbTestDataAgaent [constructor]
//
//@ Interface-Description
//  Setup the TrendDbTestDataAgent, this is for integration testing only
//  There should only be one of these agents in the system
//---------------------------------------------------------------------
//@ Implementation-Description
//  friend to the TrendDbManager
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
TrendDbTestDataAgent::TrendDbTestDataAgent( void )
{
	TREND_MGR_DBG1( 2, "Construct Test Data Agent" );

	// Request that the trend database initialize its self
	// if not already initialized
	if (!TrendDataMgr::IsInitialized())
	{
		// Set initial flag to initial the repositories
		TrendDataMgr & trendMgr = TrendDataMgr::GetTrendDataMgr();
		trendMgr.isInitRequestDatabase_ = TRUE;
	}
	else
	{
		TREND_MGR_DBG1( 2, "Trend Database already initialized" );
	}

	// Initialize the test data to zero/gaped data
	memset( testPatientDataArray_, 0, sizeof(testPatientDataArray_) );
	memset( testSettingDataArray_, 0, sizeof(testSettingDataArray_) );
	memset( testEvents_, 0, sizeof(testEvents_) );
	isTestSettingDataChange_ = 0;

	// default time stamp to current date/time
	testTimestamp_.now();    
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TrendDbTestDataAgent [destructor]
//
//@ Interface-Description
//  Does no action  
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
TrendDbTestDataAgent::~TrendDbTestDataAgent( void )
{
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processTdgCmd
//
//@ Interface-Description
//  Process the TDG command string 
//  See the TDG Manual for details on the commands
//---------------------------------------------------------------------
//@ Implementation-Description
//  Pulls the command token off of the string, uses this to find
//  the correct method to call and passes the rest of the string
//  to the method (if it takes arguments)
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDbTestDataAgent::processTdgCmd( const char * pCmdString )
{
	TREND_MGR_DBG2( 4, "Process TDG command ", pCmdString );

	// Strip off the command token
	static const Int16 TOKEN_MAX_SIZE = 11;
	Int16 idx = 0;
	char cmdToken[ TOKEN_MAX_SIZE + 1 ];
	idx = getFieldStr_( pCmdString, cmdToken, TOKEN_MAX_SIZE );
	TREND_MGR_DBG4( 4, "Command token <", cmdToken, "> at index ", idx );

	// Pass the rest of the string to the specific command function
	if (!strcmp( cmdToken, "set" ))
	{
		processTdgSetCmd_( pCmdString + idx + 1 );
	}
	else if (!strcmp( cmdToken, "sample"))
	{
		// Check for optional argument
		if (pCmdString[idx] == FIELD_SEPARATOR)
		{
			processTdgSampleCmd_( pCmdString + idx + 1 );
		}
		else
		{
			// no args
			processTdgSampleCmd_( 0 );
		}
	}
	else if (!strcmp( cmdToken, "timestamp" ))
	{
		processTdgTimestampCmd_( pCmdString + idx + 1 );
	}
	else if (!strcmp( cmdToken, "event" ))
	{
		processTdgEventCmd_( pCmdString + idx + 1 );
	}
	else if (!strcmp( cmdToken, "disable" ))
	{
		processTdgDisableCmd_();
	}
	else if (!strcmp( cmdToken, "corrupt" ))
	{
		processTdgCorruptCmd_( pCmdString + idx + 1 );
	}
	else if (!strcmp( cmdToken, "reset" ))
	{
		processTdgResetCmd_();
	}
	else if (!strcmp( cmdToken, "sim" ))
	{
		processTdgSimCmd_( pCmdString + idx + 1 );
	}
	else
	{
		TREND_MGR_DBG4( 1, "Unknown TDG command <", cmdToken, "> source line ", pCmdString );
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processTdgSetCmd_
//
//@ Interface-Description
//  Process the TDG Set command
//---------------------------------------------------------------------
//@ Implementation-Description
//   The Args are
//      repository     -    which repository to update patient/setting
//      column number  -    which column in the database to update
//      new value      -    new value to assign in the database/column
//      new valid flag -    new is valid flag value to store
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDbTestDataAgent::processTdgSetCmd_( const char * cmdArrgs )
{
	TREND_MGR_DBG2( 4, "Process set command Arrgs ", cmdArrgs );

	// Parse the arguments - Repository name
	Int16 idx = 0;
	static const Int16 REPOSITORY_NAME_LEN = 7;
	char repositoryName[ REPOSITORY_NAME_LEN + 1 ];
	idx = getFieldStr_( cmdArrgs, repositoryName, REPOSITORY_NAME_LEN );
	TREND_MGR_DBG4( 4, "Repository name <", repositoryName, "> at index ", idx );

	/// Parse the number data
	Int32 columnNumber = -1;
	Real32 newValue = 0.0;
	Int32 newValidFlag = 0;

	cmdArrgs += idx + 1;
	idx = getFieldStr_( cmdArrgs, NumberBuffer, NUMBER_BUFFER_LEN );
	columnNumber = getNumber_( NumberBuffer );
	TREND_MGR_DBG6( 4, "Number buffer <", NumberBuffer,
					"> Column number ", columnNumber,
					" at index ", idx );

	cmdArrgs += idx + 1;
	idx = getFieldStr_( cmdArrgs, NumberBuffer, NUMBER_BUFFER_LEN );
	newValue = getNumber_( NumberBuffer );
	TREND_MGR_DBG6( 4, "Number buffer <", NumberBuffer,
					"> new value ", newValue,
					" at index ", idx );

	cmdArrgs += idx + 1;
	idx = getFieldStr_( cmdArrgs, NumberBuffer, NUMBER_BUFFER_LEN );
	newValidFlag = getNumber_( NumberBuffer );
	TREND_MGR_DBG6( 4, "Number buffer <", NumberBuffer,
					"> new valid flag ", newValidFlag,
					" at index ", idx );

	// Update the value if valid
	if (!strcmp( repositoryName, "patient" ))
	{
		// Patient value update
		if (columnNumber >= 0 && columnNumber < TrendDataMgr::TREND_PATIENT_MAX_SLOTS)
		{
			testPatientDataArray_[ columnNumber ].data.realValue = newValue;
			testPatientDataArray_[ columnNumber ].isValid = newValidFlag;

			// echo back the command    
			printf( ">set patient column %d, value %f, valid %d\n",
					columnNumber,
					testPatientDataArray_[ columnNumber ].data.realValue,
					testPatientDataArray_[ columnNumber ].isValid );
		}
		else
		{
			TREND_MGR_DBG2( 1, "Invalid patient column number ", columnNumber );
		}
	}
	else if (!strcmp( repositoryName, "setting" ))
	{
		// Setting value update
		if (columnNumber >= 0 && columnNumber < TrendDataMgr::TREND_SETTING_MAX_SLOTS)
		{
			testSettingDataArray_[ columnNumber ].data.realValue = newValue;
			testSettingDataArray_[ columnNumber ].isValid = newValidFlag;

			// echo back the command    
			printf( ">set setting column %d, value %f, valid %d\n",
					columnNumber,
					testSettingDataArray_[ columnNumber ].data.realValue,
					testSettingDataArray_[ columnNumber ].isValid );

			// Set the setting changed happen flag so that the sample command will
			// pick it up and save simulated settings
			isTestSettingDataChange_ = TRUE;
		}
		else
		{
			TREND_MGR_DBG2( 1, "Invalid setting column number ", columnNumber );
		}
	}
	else
	{
		TREND_MGR_DBG2( 1, "Invalid repository name ", repositoryName );
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processTdgSampleCmd_
//
//@ Interface-Description
//  Process the TDG Sample command
//---------------------------------------------------------------------
//@ Implementation-Description
//  optional argument - number of samples to insert
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDbTestDataAgent::processTdgSampleCmd_( const char * arrgs )
{
	Int32 sampleCount = 1;
	TrendDataMgr & trendMgr = TrendDataMgr::GetTrendDataMgr();

	// Verify that the manager is in the "Idle" state before
	// processing this TDG command 
	if (!trendMgr.isState_(TrendDataMgr::INITIALIZED_IDLE))
	{
		TREND_MGR_DBG2( 1, "TDG set command, invalid state ", trendMgr.initState_ );
		return;
	}

	if (arrgs)
	{
		TREND_MGR_DBG2( 4, "Process Sample command ", arrgs );
		Int16 idx = getFieldStr_( arrgs, NumberBuffer, NUMBER_BUFFER_LEN );
		sampleCount = getNumber_( NumberBuffer );
		TREND_MGR_DBG6( 4, "Number buffer <", NumberBuffer,
						"> loop number ", sampleCount,
						" at index ", idx );        
	}
	else
	{
		TREND_MGR_DBG1( 4, "Process Sample command 1" );
		sampleCount = 1;
	}

	TrendDbPatientRepository & patientRep = trendMgr.getPatientRepository_();
	TrendDbSettingRepository & settingRep = trendMgr.getSettingRepository_();
	Uint32 simSeqPosNumber = trendMgr.getCurrentSequenceCount_();

	// Copy the events into the first two slots of the patient database
	// This is dependent on the events being in slot 0 & 1
	for (Int16 idx = 0; idx < (TrendEvents::TOTAL_EVENT_IDS / BITS_PER_LWORD); idx++)
	{
		testPatientDataArray_[idx].data.discreteValue = testEvents_[idx];
		testPatientDataArray_[idx].isValid = TRUE;
		TREND_MGR_DBG6( 4, "Assigned event byte ", idx, ", value ",
						hex, testPatientDataArray_[idx].data.discreteValue, dec );
	}

	for (Int16 loopCount = 0; loopCount < sampleCount; loopCount++)
	{
		// Pass the database to the repositories
		patientRep.sampleSimulatedData( simSeqPosNumber,
										testTimestamp_,
										testPatientDataArray_ );
		TREND_MGR_DBG1( 4, "Added test data to Patient Repository" );

		if (isTestSettingDataChange_)
		{
			// Pass the test data to the setting repository
			settingRep.sampleSimulatedData( simSeqPosNumber, 
											testTimestamp_,
											testSettingDataArray_ );
			isTestSettingDataChange_ = FALSE;

			TREND_MGR_DBG1( 4, "Added test data to Setting Repository" );
		}

		// Update sequence count and timestmp for next sample
		trendMgr.setNextSequenceCount_();
		simSeqPosNumber = trendMgr.getCurrentSequenceCount_();
		testTimestamp_ = NextSimulatedTimeStamp( TrendTimeScaleValue::TREND_TIME_SCALE_1HR,
												 testTimestamp_ );

		// Clear out the first two slots in the patient database (these are the events)

		Int16 idx = 0;
		for (idx = 0; idx < (TrendEvents::TOTAL_EVENT_IDS / BITS_PER_LWORD); idx++)
		{
			testPatientDataArray_[idx].data.discreteValue = 0;
			testPatientDataArray_[idx].isValid = FALSE;
			testEvents_[idx] = 0;
		}
		TREND_MGR_DBG4( 4, "Cleared event bytes value ",
						hex, testPatientDataArray_[idx].data.discreteValue, dec );

	} // loop on each sample

	// Flush out the control block data
	TrendDbControlBlock & controlBlock = trendMgr.getControlBlock_();
	controlBlock.flush();

	printf( ">sample last frame %d date/time %02d/%02d/%04d %02d:%02d:%02d\n", 
			simSeqPosNumber,
			testTimestamp_.getMonth(), testTimestamp_.getDayOfMonth(), testTimestamp_.getYear(),
			testTimestamp_.getHour(), testTimestamp_.getMinutes(), testTimestamp_.getSeconds() );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processTdgTimestampCmd_
//
//@ Interface-Description
//  Process the TDG Timestamp command
//---------------------------------------------------------------------
//@ Implementation-Description
//   The Args are
//      yyyy,mm,dd,hh,mm,ss
//      
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDbTestDataAgent::processTdgTimestampCmd_( const char * cmdArrgs )
{
	TREND_MGR_DBG2( 4, "Process timestamp command Arrgs ", cmdArrgs );

	// Parse the arguments - Repository name
	Int16 idx = 0;
	Int32 year = 2007;
	Int32 month = 1;
	Int32 day = 1;
	Int32 hour = 0;
	Int32 minute = 0;
	Int32 second = 0;

	idx = getFieldStr_( cmdArrgs, NumberBuffer, NUMBER_BUFFER_LEN );
	year = getNumber_( NumberBuffer );
	TREND_MGR_DBG6( 4, "Number buffer <", NumberBuffer,
					"> year ", year,
					" at index ", idx );

	cmdArrgs += idx + 1;
	idx = getFieldStr_( cmdArrgs, NumberBuffer, NUMBER_BUFFER_LEN );
	month = getNumber_( NumberBuffer );
	TREND_MGR_DBG6( 4, "Number buffer <", NumberBuffer,
					"> month ", month,
					" at index ", idx );

	cmdArrgs += idx + 1;
	idx = getFieldStr_( cmdArrgs, NumberBuffer, NUMBER_BUFFER_LEN );
	day = getNumber_( NumberBuffer );
	TREND_MGR_DBG6( 4, "Number buffer <", NumberBuffer,
					"> day of month ", day,
					" at index ", idx );

	cmdArrgs += idx + 1;
	idx = getFieldStr_( cmdArrgs, NumberBuffer, NUMBER_BUFFER_LEN );
	hour = getNumber_( NumberBuffer );
	TREND_MGR_DBG6( 4, "Number buffer <", NumberBuffer,
					"> hour ", hour,
					" at index ", idx );

	cmdArrgs += idx + 1;
	idx = getFieldStr_( cmdArrgs, NumberBuffer, NUMBER_BUFFER_LEN );
	minute = getNumber_( NumberBuffer );
	TREND_MGR_DBG6( 4, "Number buffer <", NumberBuffer,
					"> minute ", minute,
					" at index ", idx );
	cmdArrgs += idx + 1;
	idx = getFieldStr_( cmdArrgs, NumberBuffer, NUMBER_BUFFER_LEN );
	second = getNumber_( NumberBuffer );
	TREND_MGR_DBG6( 4, "Number buffer <", NumberBuffer,
					"> seconds ", second,
					" at index ", idx );

	// Assign to test timestmp, echo command
	testTimestamp_ = TimeStamp( year, month, day, hour, minute, second );

	printf( ">timestamp %02d/%02d/%04d %02d:%02d:%02d\n",
			testTimestamp_.getMonth(), testTimestamp_.getDayOfMonth(), testTimestamp_.getYear(),
			testTimestamp_.getHour(), testTimestamp_.getMinutes(), testTimestamp_.getSeconds() );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processTdgeEventCmd_
//
//@ Interface-Description
//  Process the TDG Event command
//---------------------------------------------------------------------
//@ Implementation-Description
//   The Args are
//      event - the event bit number 0 - 63
//      
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDbTestDataAgent::processTdgEventCmd_( const char * cmdArrgs )
{
	TREND_MGR_DBG2( 4, "Process event command Arrgs ", cmdArrgs );

	// Parse the arguments - event bit number 0 - 63
	Uint32  event = 0;

	Int16 idx = getFieldStr_( cmdArrgs, NumberBuffer, NUMBER_BUFFER_LEN );
	event = getNumber_( NumberBuffer );
	TREND_MGR_DBG6( 4, "Number buffer <", NumberBuffer,
					"> event ", event,
					" at index ", idx );

	// Post the event
	SetBit( testEvents_, event );

	// echo the command back
	printf( ">event %2d, byte 0 0x%x, byte 1 0x%x\n", event, testEvents_[0], testEvents_[1] );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processDisableCmd_
//
//@ Interface-Description
//  Process the TDG Disable command
//  Sends a disable request to the TrendDataMgr
//---------------------------------------------------------------------
//@ Implementation-Description
//   No Args
//      
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDbTestDataAgent::processTdgDisableCmd_( void )
{
	TREND_MGR_DBG1( 4, "Process disable command" );
	TrendDataMgr::Disable();

	printf( ">disable\n" );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processTdgCorruptCmd_
//
//@ Interface-Description
//  Process the TDG Corrupt a LBA block command
//  Corrupts a block on the Compact Flash
//---------------------------------------------------------------------
//@ Implementation-Description
//   The Args are
//      LAB - Block number to corrupt
//      
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDbTestDataAgent::processTdgCorruptCmd_( const char * cmdArrgs )
{
	// Parse the arguments - hours
	Uint32  lba = 0;

	Int16 idx = getFieldStr_( cmdArrgs, NumberBuffer, NUMBER_BUFFER_LEN );
	lba = getNumber_( NumberBuffer );
	TREND_MGR_DBG6( 4, "Number buffer <", NumberBuffer,
					"> LBA ", lba,
					" at index ", idx );

	// Load a corrupted block into the compact flash
	static CompactFlash::Block  block;
	memset( block, 0xA5A5, sizeof( block ) );
	CompactFlash & compactFlash = CompactFlash::GetCompactFlash();
	if (compactFlash.write( lba, block, 1 ) != SUCCESS)
	{
		TREND_MGR_DBG2( 1, "Failed to write bad block ", lba );
	}

	printf( ">corrupt LBA %d\n", lba );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processTdgRestCmd_
//
//@ Interface-Description
//  Process the TDG Reset command
//  Sends a reset request to the TrendDataMgr, only works if the manager
//  is in INITIALIZED_IDLE
//---------------------------------------------------------------------
//@ Implementation-Description
//   No Args
//      
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDbTestDataAgent::processTdgResetCmd_( void )
{
	TrendDataMgr & trendMgr = TrendDataMgr::GetTrendDataMgr();

	// Verify that the manager is in the "Idle" state before
	// processing the TDG reset command 
	if (trendMgr.isState_(TrendDataMgr::INITIALIZED_IDLE))
	{
		// Unlock the database then make the reset request
		Boolean lockDb = trendMgr.IsLockDatabase;
		trendMgr.IsLockDatabase = FALSE;

		TREND_MGR_DBG2( 2, "Reset Trend Database, orignal lock ", lockDb );
		trendMgr.isResetRequestDatabase_ = TRUE;

		// Wait for the reset to complete
		// Then reset the lock to its orignal state
		while (trendMgr.isResetRequestDatabase_)
		{
			Task::Delay( 1, 0 );
		}
		trendMgr.IsLockDatabase = lockDb;
	}
	else
	{
		TREND_MGR_DBG2( 1, "Can't perform Sample TDG command while in state ",
						trendMgr.initState_ );
	}

	printf( ">reset\n" );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processTdgSimCmd_
//
//@ Interface-Description
//  Process the TDG SIM command
//  Loads simulated data into the database
//---------------------------------------------------------------------
//@ Implementation-Description
//   The Args are
//      hours - the number of hours to load
//      
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDbTestDataAgent::processTdgSimCmd_( const char * cmdArrgs )
{
	// Parse the arguments - hours
	Uint32  hours = 0;

	Int16 idx = getFieldStr_( cmdArrgs, NumberBuffer, NUMBER_BUFFER_LEN );
	hours = getNumber_( NumberBuffer );
	TREND_MGR_DBG6( 4, "Number buffer <", NumberBuffer,
					"> hours ", hours,
					" at index ", idx );

	// Call the LoadSimulatedData() converting hours into frames
	// at 60min * 6frames/pre min
	LoadSimulatedData( hours * 360 );

	printf( ">sim hours %d\n", hours );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getFieldStr_
//
//@ Interface-Description
//  Copy the field data from the command string to pFieldStr
//---------------------------------------------------------------------
//@ Implementation-Description
//  "," is used to separate fields
//  returns number of char copied
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
Int16 TrendDbTestDataAgent::getFieldStr_( const char * pCmdString,
										  char * pFieldStr, 
										  Int16 maxSize )
{
	Int16 idx = 0;
	while (pCmdString[idx] != FIELD_SEPARATOR && 
		   pCmdString[idx] != '\0' &&
		   idx < maxSize)
	{
		pFieldStr[idx] = pCmdString[idx];
		idx++;
	}
	pFieldStr[idx] = '\0';

	return( idx );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getNumber_
//
//@ Interface-Description
//  Convert the string into a real number
//---------------------------------------------------------------------
//@ Implementation-Description
//  If the number begins with "0x" then used an Int hex format
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
Real32 TrendDbTestDataAgent::getNumber_( const char * pNumString )
{
	Real32 rtn;

	// Check to see if the string is in hex format
	if (!strncmp( pNumString, "0x", 2 ))
	{
		Int32 iNumber = 0;
		sscanf( pNumString, "0x%x", &iNumber );
		rtn = iNumber;
	}
	else	// real number
	{
		sscanf( pNumString, "%f", &rtn );
	}

	return( rtn );
}




