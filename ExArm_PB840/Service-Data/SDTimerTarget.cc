#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: SDTimerTarget - A target to handle Service Data Timer events.
// Classes can specify this class as an additional parent class register
// to be informed of timer events.
//---------------------------------------------------------------------
//@ Interface-Description
// Any classes which need to be informed of timer events (either one-shot or
// periodic events) need to multiply inherit from this class.  Then, when they
// register for timer events with the SDTimerRegistrar class, they will be
// informed of events via the timerEventHappened() method.
//---------------------------------------------------------------------
//@ Rationale
// Used to implement the callback mechanism needed to communicate
// timer events to objects.
//---------------------------------------------------------------------
//@ Implementation-Description
// If classes need to be informed of timer events they must do 3
// things: a) multiply inherit from this class, b) define the
// timerEventHappened() method of this class and c) register for
// change notices via the SDTimerRegistrar class.  They will then
// be informed of timer events via the timerEventHappened() method.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
// This class should not be instantiated directly.  This class has use only
// when inherited from.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Data/vcssrc/SDTimerTarget.ccv   25.0.4.0   19 Nov 2013 14:21:04   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah     Date:  15-Jan-99   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' to a non-inlined method.
//
//  Revision: 001  By:  yyy    Date:  16-May-95   DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//=====================================================================

#if defined FAKE_SM
// This class is used only for test simulation.

#include "Sigma.hh"
#include "SDTimerTarget.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: timerEventHappened
//
//@ Interface-Description
// This method should be redefined in derived classes.  It is via this method
// that timer events are communicated.  It is passed one parameter, 'timerId',
// which identifies which timer fired.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SDTimerTarget::timerEventHappened(SDTimerId::SDTimerIdType timerId)
{
	CALL_TRACE("SDTimerTarget::timerEventHappened(SDTimerId::SDTimerIdType"
															"timerId)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SDTimerTarget()  [Default Constructor, Protected]
//
//@ Interface-Description
// The constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SDTimerTarget::SDTimerTarget(void)
{
	CALL_TRACE("SDTimerTarget::SDTimerTarget(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~SDTimerTarget  [Destructor, Protected]
//
//@ Interface-Description
// Nothing to destruct!
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SDTimerTarget::~SDTimerTarget(void)
{
	CALL_TRACE("SDTimerTarget::~SDTimerTarget(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
SDTimerTarget::SoftFault(const SoftFaultID  softFaultID,
		   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)  
{
	CALL_TRACE("SDTimerTarget::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, SERVICE_DATA,
			SERVICE_DATA_TIMER_TARGET, lineNumber, pFileName, pPredicate);
}


#endif	// FAKE_SM
