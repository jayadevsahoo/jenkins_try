#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: FlowSensorCalInfo - Stores calibration and other specific 
//      information about the flow sensors, as received from the serial EPROM.
//---------------------------------------------------------------------
//@ Interface-Description
//      This class contains the flow sensor eprom calibration data
//      and methods for accessing the calibration data, for converting
//      voltage to flow using the calibration data, and for building
//      conversion tables.
//---------------------------------------------------------------------
//@ Rationale
//      Abstraction of flow sensor calibration information.
//      The calibration information is read from the flow sensor
//      EPROM and placed in the Persistent Object class.
//---------------------------------------------------------------------
//@ Implementation-Description
//      Access method are provided for service mode to read calibration
//      data from the flow sensor EPROM.  Service mode is responsible to 
//      set all calibration information in this class.  The class provides a
//      method to set all the information.  A method is provided to convert
//      voltage into flow using the flow sensor calibration data.
//---------------------------------------------------------------------
//@ Fault-Handling
//		n/a
//---------------------------------------------------------------------
//@ Restrictions
//      none
//---------------------------------------------------------------------
//@ Invariants
//      None
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/FlowSensorCalInfo.ccv   26.0.1.0   04 Feb 2013 10:36:40   pvcs  $
//
//@ Modification-Log
//
//  Revision: 012 By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//	Description:
//		Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 011 By: syw    Date: 07-Apr-1998   DR Number: DCS 5065
//  	Project:  Sigma (840)
//		 	BiLevel initial version.  Added code to support NEW_SENSOR
//			(conditionally compiled).  Eliminate modelNumber_.
//
//  Revision: 010 By: syw    Date: 27-Aug-1997   DR Number: DCS 2426 
//  	Project:  Sigma (R8027)
//		Description:
//			Changed integer data types to Uint32.
//
//  Revision: 009 By: syw    Date: 03-Jul-1997   DR Number: DCS 1908 
//  	Project:  Sigma (R8027)
//		Description:
//			Added back temperatureToCf_[] and methods associated to improve
//			thruput by avoiding sqrt calculation on the fly.
//
//  Revision: 008 By: syw    Date: 01-Apr-1997   DR Number: DCS 1827 
//  	Project:  Sigma (R8027)
//		Description:
//			Remove temperatureToCf_[] and methods associated.
//
//  Revision: 007 By: syw    Date: 27-Feb-1997   DR Number: DCS 1827
//  	Project:  Sigma (R8027)
//		Description:
//			Changed cT to 32.0 for model = 1 only
//
//  Revision: 006 By: syw    Date: 10-Sep-1996   DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Remove setTemperatureCorrection_().  Call public method
//			setTemperatureCorrection( kFactor) instead.
//
//  Revision: 005 By: syw    Date: 18-Mar-1996   DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Remove setFlowSensorCalInfoTable().  Initialize flow sensor data
//			to prevent having garbage in the table.  Eventually, the data will
//			store the actual data.
//
//  Revision: 003  By: syw    Date:  25-Jan-1996    DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Added SIGMA_DEBUG printCalInfoTable() method.
//
//  Revision: 002  By: syw    Date:  05-Dec-1995    DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Removed copy constructor since it is declared as private.
//			Changed definition of cT to 35.0 regardless of modelNumber to
//			reflect latest update in flow sensors.  Added the following
//			methods for service mode usage:
//				- setTemperatureToCfTable()
//				- setCoeffEntry()
//
//	Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: INITIAL RELEASE
//      Project:  Sigma (R8027)
//      Description:
//      	Initial version
//
//=====================================================================
#include "FlowSensorCalInfo.hh"
#include <math.h>
#include <string.h>

//@ Usage-Classes
//@ End-Usage

#ifdef SIGMA_DEBUG 
#include "Ostream.hh"
#include <stdio.h>
#include "PrintQueue.hh"
#endif // SIGMA_DEBUG

static const Real32 TempVoltageTable[TEMPTAB_SIZE] = { 
	3.891, 3.844, 3.796, 3.747, 3.697, 3.645, 3.593, 3.540, 3.486, 3.431,	/* 0 - 9 */
	3.376, 3.320, 3.263, 3.206, 3.148, 3.090, 3.031, 2.972, 2.913, 2.854,	/* 10 - 19 */
	2.795, 2.736, 2.676, 2.617,	2.559, 2.500, 2.442, 2.384,	2.327, 2.270,	/* 20 - 29 */
	2.213, 2.158, 2.102, 2.048, 1.994, 1.942, 1.889, 1.838, 1.788, 1.738,	/* 30 - 39 */
	1.690};

static const Uint16 ADC_ADJUSTED_FACTOR = 1;

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: FlowSensorCalInfo()
//
//@ Interface-Description
//      Constructor.  Initialize the flow data to arbitrary reasonable data.
//---------------------------------------------------------------------
//@ Implementation-Description
//      Initialize the flow data to arbitrary reasonable data.  The real data
//		will be read from the flow sensor or FLASH.
//---------------------------------------------------------------------
//@ PreCondition
//      None
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================

FlowSensorCalInfo::FlowSensorCalInfo( void)
{
	CALL_TRACE("FlowSensorCalInfo::FlowSensorCalInfo( void)") ;

    // $[TI1]

	const Uint32 NUM_COEFFS = 12 ;
	
static Real32 voltage[NUM_COEFFS] = { 0.331458f, 0.625030f, 0.960310f, 1.296682f, 1.749697f, 2.122120f, 2.742360f, 3.232111f, 3.744826f, 0, 0, 0} ;

static Real32 coeffA[NUM_COEFFS] = { -0.291012f, -0.582271f, -1.209300f, -3.636020f, -11.579100f, -28.479400f, -21.293100f, -46.525500f, -46.525500f, 0, 0, 0} ;

static Real32 coeffB[NUM_COEFFS] = { 2.522600f, 3.790740f, 4.920690f, 7.049210f, 10.613100f, 16.583700f, 14.563300f, 16.356200f, 16.356200f, 0, 0, 0} ;
								     
static Real32 coeffC[NUM_COEFFS] = { 3.464250f, 1.324040f, 0.815889f, 0.501591f, 0.320630f, 0.088561f, 0.140192f, 0.158628f, 0.158628f, 0, 0, 0} ;

	checksum_ = 24174;	
	serialNumber_ = 2010233001 ;
	kFactor_[0] = 0.9396423740f ;
	kFactor_[1] = 0.002103325f ;
	kFactor_[2] = 0.002433017f ;
	kFactor_[3] = 0.000009284f ;
	kFactor_[4] = 0.000000137f ;
	numFlowCoeffSets_ = 9 ;

	revision_ = 0 ;
	calDate_ = 18 ;
	calMon_ = 7;
	calYear_ = 2002;
	calTemp_ = 21.11f ;
	span_ = 1.702430471f ;
	zero_ = 1.243781095f ;
	temperatureCorrection_ = -1.0F ;
	
	for (Uint32 ii=0; ii < NUM_COEFFS; ii++)
	{
	    coeffSet_[ii].calVoltage_ = voltage[ii] ;
    	coeffSet_[ii].coeffA_ = coeffA[ii] ;
	    coeffSet_[ii].coeffB_ = coeffB[ii] ;
    	coeffSet_[ii].coeffC_ = coeffC[ii] ;
    }

	for(int i = 0; i < TEMPTAB_SIZE; i++)
	{
		tempADTable_[i] = (Uint16)((TempVoltageTable[i] * 4095) / 5.0f);
	}

	genTempCompFactorTab_();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~FlowSensorCalInfo()
//
//@ Interface-Description
//      Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//      None
//---------------------------------------------------------------------
//@ PreCondition
//      None
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================

FlowSensorCalInfo::~FlowSensorCalInfo( void)
{
	CALL_TRACE("FlowSensorCalInfo::~FlowSensorCalInfo( void)") ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: operator=()
//
//@ Interface-Description
//      operator=.
//---------------------------------------------------------------------
//@ Implementation-Description
//     Sets all elements equal to the object passed in.
//---------------------------------------------------------------------
//@ PreCondition
//      0 < numFlowCoeffSets_ <= MAX_FLOW_SENSOR_COEFF_SETS
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================
void
FlowSensorCalInfo::operator=(const FlowSensorCalInfo& calInfo)
{
	CALL_TRACE("FlowSensorCalInfo::operator=(const FlowSensorCalInfo& calInfo)") ;

	if (this != &calInfo)
	{
	    checksum_ = calInfo.checksum_ ;
		// $[TI1.1]
    	serialNumber_ = calInfo.serialNumber_ ;
		revision_ = calInfo.revision_ ;
		calDate_ = calInfo.calDate_ ;
		calMon_ = calInfo.calMon_ ;
		calYear_ = calInfo.calYear_ ;
		model_ = calInfo.model_;

	    temperatureCorrection_ = calInfo.temperatureCorrection_ ;
		memmove(kFactor_, calInfo.kFactor_, sizeof(kFactor_));

    	numFlowCoeffSets_ = calInfo.numFlowCoeffSets_ ;

		span_ = calInfo.span_ ;
		zero_ = calInfo.zero_ ;
		calTemp_ = calInfo.calTemp_ ;

    	AUX_CLASS_PRE_CONDITION( numFlowCoeffSets_ <= MAX_FLOW_SENSOR_COEFF_SETS
    						 && numFlowCoeffSets_ >= 0, numFlowCoeffSets_) ;
    
		memmove(coeffSet_,  calInfo.coeffSet_, sizeof(coeffSet_));
		memmove(tempCompFactorTab_, calInfo.tempCompFactorTab_, sizeof(tempCompFactorTab_));
		memmove(tempADTable_, calInfo.tempADTable_, sizeof(tempADTable_));
	}	// implied else $[TI1.2]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getFlow()
//
//@ Interface-Description
//      This method takes one argument, correctedVoltage and returns the
//      flow (lpm) calculated from the value passed in.
//---------------------------------------------------------------------
//@ Implementation-Description
//      The coefficients, corresponding to the correctedVoltage, are
//      used to convert the voltage to flow (LPM).
// 		$[00401]
//---------------------------------------------------------------------
//@ PreCondition
//      None
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

Real32
FlowSensorCalInfo::getFlow( const AdcCounts flowCounts, const AdcCounts tempCounts) const
{
	CALL_TRACE("FlowSensorCalInfo::getFlow( const Uint16 flowCount, const Uint16 tempCount)") ;

    Real32 correctedVoltage = tempCompensateFlow_(flowCounts*ADC_ADJUSTED_FACTOR, tempCounts*ADC_ADJUSTED_FACTOR);
	Real32 flow = calculateFlow_(correctedVoltage);

    // It is possible to compute a negative flow when CoeffA_ is
    // negative, but this is not allowed.
    if (flow < 0.0)
    {
	    // $[TI1.1]
        flow = 0.0 ;
    }	// implied else$[TI1.2]

    return( flow) ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setCoeffEntry
//
//@ Interface-Description
//      This method takes a NUM_DATA_VALUES by MAX_FLOW_SENSOR_COEFF_SETS
//		array as arguments and returns nothing.  The FlowSensorCoeffEntry
//		array is filled with the array passed in.  The first element of the
//		array passed in is the calVoltage, then coeffA, coeffB, coeffC.
//---------------------------------------------------------------------
//@ Implementation-Description
//      Step through the array and initialize the data members.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================
void
FlowSensorCalInfo::setCoeffEntry( FlowSensorCoeffEntry coeffSet[MAX_FLOW_SENSOR_COEFF_SETS])
{
	CALL_TRACE("FlowSensorCalInfo::setCoeffEntry( FlowSensorCoeffEntry coeffSet[MAX_FLOW_SENSOR_COEFF_SETS])");

    // $[TI1]

	for (Uint32 ii=0; ii < MAX_FLOW_SENSOR_COEFF_SETS; ii++)
	{
	   	coeffSet_[ii] = coeffSet[ii] ;
	}

#ifdef SIGMA_DEBUG
	printCalInfoTable();
#endif
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: convertTemperature
//
//@ Interface-Description
//		This method has the temperature counts as an argument and returns
//		the temperature in Celcius.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Convert raw count to voltage, then using a curve fit to determine
//		the temperature.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================
Real32
FlowSensorCalInfo::convertTemperature( const Uint32 counts)
{
	CALL_TRACE("FlowSensorCalInfo::convertTemperature( const Uint32 counts)") ;

    Real32 temp;
	Uint32 adjustedCounts = counts * ADC_ADJUSTED_FACTOR;
	int i;

	//Determine the gas temperature using the Temp. ADC count
	for(i=0; i<TEMPTAB_SIZE; i++)
	{
		if(adjustedCounts > tempADTable_[i])
			break;
	}
	if(i >= TEMPTAB_SIZE)
	{
		i = TEMPTAB_SIZE - 1;
	}

	if(i==0)
	{
		temp = 0;
	}
	else
	{
		temp = (Real32)i + ((Real32)(tempADTable_[i-1]-adjustedCounts)/(Real32)(tempADTable_[i-1]-tempADTable_[i]));
	}
	
	return( temp) ;
}



#ifdef SIGMA_DEBUG
void
FlowSensorCalInfo::printCalInfoTable( void)
{
	sprintf( string, "checksum_ = %u\n", checksum_) ;
	PrintQueue::SendString( string) ;
	printf( "checksum = %d\n", checksum_) ;
	
	sprintf( string, "serialNumber_ = %u\n", serialNumber_) ;
	PrintQueue::SendString( string) ;
	printf( "serialNumber = %u\n", serialNumber_) ;
	
	sprintf( string, "model_ = %u\n", model_) ;
	PrintQueue::SendString( string) ;
	printf( "model = %u\n", model_) ;

	sprintf( string, "revision_ = %u\n", revision_) ;
	PrintQueue::SendString( string) ;
	printf( "revision = %u\n", revision_) ;
	
	sprintf( string, "calDate_ = %u\n", calDate_) ;
	PrintQueue::SendString( string) ;
	printf( "calData = %u\n", calDate_) ;
	
	for(int i=0; i<5; i++)
	{
		sprintf( string, "kFactor_[%d] = %e\n", i, kFactor_[i]) ;
		PrintQueue::SendString( string) ;
		printf( "kFactor[%u] = %u\n", kFactor_[i]) ;
	}

	sprintf( string, "calTemp_ = %e\n", calTemp_) ;
	PrintQueue::SendString( string) ;
	printf( "calTemp = %f\n", calTemp_) ;

	sprintf( string, "span_ = %e\n", span_) ;
	PrintQueue::SendString( string) ;
	printf( "span = %f\n", span_) ;

	sprintf( string, "zero_ = %e\n", zero_) ;
	PrintQueue::SendString( string) ;
	printf( "zero = %f\n", zero_) ;

	sprintf( string, "temperatureCorrection_ = %e\n", temperatureCorrection_) ;
	PrintQueue::SendString( string) ;
	printf( "tempCorr = %f\n", temperatureCorrection_) ;
	
	sprintf( string, "numFlowCoeffSets_ = %u\n", numFlowCoeffSets_) ;
	PrintQueue::SendString( string) ;
	printf( "numCoeff = %d\n", numFlowCoeffSets_) ;
			
	for (Uint32 ii=0; ii < numFlowCoeffSets_; ii++)
	{
		coeffSet_[ii].write() ;
	}
}
#endif  // SIGMA_DEBUG



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault()
//
//@ Interface-Description
//      This method takes four arguments: softFaultID, lineNumber, pFileName,
//      and pPredicate and is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//      'lineNumber' are essential pieces of information.  The 'pFileName'
//      and 'pPredicate' strings may be defaulted in the macro to reduce
//      code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      None
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================
void
FlowSensorCalInfo::SoftFault( const SoftFaultID  softFaultID,
                              const Uint32       lineNumber,
                              const char*        pFileName,
                              const char*        pPredicate)
{
    CALL_TRACE("SoftFault( softFaultID, lineNumber, pFileName, pPredicate)") ;
    
    FaultHandler::SoftFault( softFaultID, BD_IO_DEVICES, FLOWSENSORCALINFO, lineNumber,
                          	 pFileName, pPredicate) ;
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//=====================================================================
//
//  Private Methods...
//
//=====================================================================

/**********************************************************************************
* NAME:   void genTempCompFactorTab_()
*
* PURPOSE: To generate the temperature compensation correction factor.
*
* DESCRIPTION: This function uses the Real32ing point computation to generate the  
*              temperature compensation correction factor (tcomp) table for tempearature
*			   from 0 to 40 oC.
*
*			   T = T + Tcorr
*
*			   tcomp = k0+k2*T+k3*T*T+k4*T*T*T
*
*			   k1*Vb is skipped because Vb is realtime data.
*
*			   This fucntion should be executed at power up.
**********************************************************************************/
void FlowSensorCalInfo::genTempCompFactorTab_()
{
	Real32 t;
	Uint16 i;			// index used as temperature

	for(i=0; i<TEMPTAB_SIZE; i++)
	{
		t = (Real32)i + temperatureCorrection_;
		tempCompFactorTab_[i] = kFactor_[0] + (kFactor_[2]*t) + (kFactor_[3]*t*t) + (kFactor_[4]*t*t*t);
	}
}

/**********************************************************************************
* NAME:   Real32 tempCompensateFlow_(const AdcCounts flowCounts, const AdcCounts tempCounts) const
*
* PURPOSE: To calculate Vfstd .
*
* INPUT:	flowCounts  - Flow ADC counts
*			tempCounts	- Temperature ADC counts
*
* OUTPUT:	Vfstd		- Temperature compensated voltage representing flow
*
* DESCRIPTION:
*
*			   Calculate Vf based on input signal from sensor.
*			   Calculate Vb
*			   Vb = (Vf+Z)/S	(where:Z=zero value and S=span value)
*			   Look up temperature value based on A/D value 
*			   Add k1*Vb to TempCompFactor
*
*			   calculate Vfstd:
*				 Vfstd = (Vf+Z)*TempCompFactor - Z
*
**********************************************************************************/
Real32 FlowSensorCalInfo::tempCompensateFlow_(const AdcCounts flowCounts, const AdcCounts tempCounts) const
{
	Real32 TempCompFactor;
	Real32 Vf, Vb, Vfstd;	// flow voltage, bridge voltage, and standard flow voltage respectively
	Uint16 i;

	Vf = (((Real32)flowCounts * 5.0f) / 4095);		// convert flow 14-bit ADC with 4.096-volt reference to voltage

	Vb = (Vf + zero_) / span_;	// calculate bridge voltage
	
	//Determine the gas temperature using the Temp. ADC count
	for(i=0; i<TEMPTAB_SIZE; i++)
	{
		if(tempCounts > (AdcCounts)tempADTable_[i])
			break;
	}
	if(i >= TEMPTAB_SIZE)
	{
		i = TEMPTAB_SIZE - 1;
	}
	
	// Compute temperature compensation factor
	TempCompFactor = kFactor_[1] * Vb;
	if(i==0)
	{
		TempCompFactor += tempCompFactorTab_[i];
	}
	else
	{
		TempCompFactor += tempCompFactorTab_[i-1];
	}
	
	//Calculate standard flow Vfstd which is Vf would be if gas temperature were 21.11 oC
	Vfstd = (TempCompFactor * (Vf + zero_)) - zero_;

	return(Vfstd);
}


/**********************************************************************************
* NAME:   Real32 calculateFlow_(Real32 Vfstd) const
*
* PURPOSE: convert voltage to flow using the equation specified by the manufacturer.
*
* INPUT: Vfstd - temperature compensated flow voltage
*
* OUTPUT: Standard flow
*
* DESCRIPTION:
*
**********************************************************************************/
Real32 FlowSensorCalInfo::calculateFlow_(Real32 Vfstd) const
{
	Uint16 i;
	Real32 Q, a, b, c;
	Real32 V2fstd;	// square of Vfstd
	Real32 V5fstd;	// Vfstd ^5
	Q = a = b = c = 0;
	
	for(i=0; i<numFlowCoeffSets_; i++)
	{
		if(coeffSet_[i].calVoltage_ > Vfstd)
		{
			if(i!=0)
			{
				a = coeffSet_[i-1].coeffA_;
				b = coeffSet_[i-1].coeffB_;
				c = coeffSet_[i-1].coeffC_;
			}
			else
			{
				a = coeffSet_[i].coeffA_;
				b = coeffSet_[i].coeffB_;
				c = coeffSet_[i].coeffC_;
			}
			break;
		}
	}

    if(i>=numFlowCoeffSets_)	// Max index check         
	{
		a = coeffSet_[i-1].coeffA_;
		b = coeffSet_[i-1].coeffB_;
		c = coeffSet_[i-1].coeffC_;
	}

	V2fstd = Vfstd * Vfstd;
	V5fstd = V2fstd * V2fstd * Vfstd;

	Q = a + (b * V2fstd) + (c * V5fstd);

    return((Q<0)? 0 : Q);
}










