// All rights reserved ADENEO EMBEDDED 2010
//
//  File:  bsp_version.h
//
#ifndef __BSP_VERSION_H
#define __BSP_VERSION_H

#define BSP_VERSION_MAJOR       2
#define BSP_VERSION_MINOR       30
#define BSP_VERSION_QFES		0
#define BSP_VERSION_INCREMENTAL 1

#define BSP_VERSION_STRING      L"BSP_WINCE_ARM_A8 2.30.00.01"

#endif
