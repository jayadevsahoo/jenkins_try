
#ifndef OperationalTime_HH
#define OperationalTime_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class:  OperationalTime - Storage class for the operational time.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Persistent-Objects/vcssrc/OperationalTime.hhv   25.0.4.0   19 Nov 2013 14:18:52   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By:  sah    Date:  01-Jan-1999    DCS Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' to a non-inlined method.
//
//  Revision: 001  By:  sah    Date:  15-Mar-1997    DCS Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//          Initial version (Integration baseline).
//
//====================================================================

#include "Sigma.hh"
#include "PersistentObjsId.hh"

//@ Usage-Classes
//@ End-Usage


class OperationalTime
{
  public:
    inline OperationalTime (void);
    inline ~OperationalTime(void);

    inline Uint32  getTimeCount(void) const;
    inline void    setTimeCount(const Uint32 newTimeCount);

    inline void  operator=(const OperationalTime& operTime);

    static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL, 
			  const char*       pPredicate = NULL);

  private:
    OperationalTime(const OperationalTime&);  // not implemented...

    //@ Data-Member:  timeCount_
    // The count of the number of time units that have accumulated.
    Uint32  timeCount_;
};


// Inlined methods...
#include "OperationalTime.in"


#endif // OperationalTime_HH 
