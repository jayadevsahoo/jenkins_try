#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: Padlock -  A "padlock" is used for adjusting the Constant During
// Rate Change setting on the Vent Setup subscreen.  Switches between a "locked
// padlock" bitmap and an "unlocked padlock" button.  This is displayed only
// while Respiratory Rate is selected in PCV mode within the Vent Setup
// subscreen.
//---------------------------------------------------------------------
//@ Interface-Description
// A Padlock is displayed in two states, locked or unlocked.  In the locked
// state, a locked padlock bitmap is displayed indicating that the associated
// breath timing setting is now "constant during a rate change".  In the
// unlocked state, a button containing an unlocked padlock is displayed
// indicating that the associated timing setting is not constant during a rate
// change.
//
// Padlocks are always displayed in sets of three, one locked and two
// unlocked.  The current "constant during a rate change" setting is
// represented by the state of this set.
//
// A timing setting is associated with a padlock explicitly via a construction
// parameter and implicitly by displaying the padlock above the setting which
// adjusts the timing setting in question.
//
// Once created (creation defines the position of the padlock and the
// associated timing setting) no other interaction with this object need be
// performed apart from calling its activate() method before display.
//
// The padlock registers for changes to the Constant During Rate Change
// setting, hence the need for the valueUpdate() method.  It also
// registers for the button events which occur on the unlocked padlock button,
// hence the definition of the buttonDownHappened() methods.
//---------------------------------------------------------------------
//@ Rationale
// Encapsulates the complete functionality of a padlock as needed for
// changing the Constant During Rate Change setting.
//---------------------------------------------------------------------
//@ Implementation-Description
// In the unlocked state, Padlock displays a small button containing an
// unlocked bitmap.
//
// Padlock registers itself for this button's events.  Whenever this
// button is pressed down we set the value of the Constant During
// Rate Change setting to the value passed to us on construction.  This
// results in the valueUpdate() method being called and it
// is here that we switch to displaying the locked bitmap, with the unlocked
// button being removed from the display. 
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and pre-conditions
// to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
// Padlocks should be created in a set of three, one each for the
// Inspiratory Time, Expiratory Time and I:E Ratio buttons.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/Padlock.ccv   25.0.4.0   19 Nov 2013 14:08:12   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005   By: gdc    Date:  29-Jun-1999    DCS Number: 5327
//  Project: 840 Neonatal
//  Description:
//	Optimized graphics library. Implemented Direct Draw mode for drawing
//  outside of BaseContainer refresh cycle.
//
//  Revision: 004  By:  sah	   Date:  11-Jan-1999    DCS Number: 5321
//  Project:  ATC
//  Description:
//		Added 'SIGMA_DEVELOPMENT' conditionals around this class (unused)
//		'SoftFault()' method.
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "Padlock.hh"

//@ Usage-Classes
#include "SettingSubject.hh"
#include "Colors.hh"
#include "BoundStatus.hh"
#include "GuiAppClassIds.hh"
#include "Image.hh"
//@ End-Usage

//@ Code...

// Initialize static constants.
static const Int32 BUTTON_WIDTH_ = 27;
static const Int32 BUTTON_HEIGHT_ = 20;
static const Int32 BUTTON_BORDER_ = 2;


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Padlock()  [Constructor]
//
//@ Interface-Description
// Creates a Padlock.  Passed the following parameters:
// >Von
//	centerX			The X coordinate around which the padlock will be centered.
//	yOrg			The Y coordinate at which the padlock is positioned.
//	timingValue		One of the three discrete values, INSP_TIME, EXP_TIME and
//					IE_RATIO (all of enum type ConstantParmValueId), which
//					will be the value that the Constant During Rate Change
//					setting will be set to if the padlock button is pressed.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
// We set the Y coordinate of the padlock container, add the unlocked bitmap
// to the center of the button and register for button callback and Constant
// During Rate Change setting events.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Padlock::Padlock(Int16 centerX, Int16 yOrg,
					ConstantParmValue::ConstantParmValueId timingValue) :
			centerX_(centerX),
			timingValue_(timingValue),
			unlockedButton_(0, 0, BUTTON_WIDTH_, BUTTON_HEIGHT_,
							Button::LIGHT, BUTTON_BORDER_, Button::ROUND,
							Button::NO_BORDER),
			unlockedBitmap_(Image::RUnlockedPadlockIcon),
			lockedBitmap_(Image::RLockedPadlockIcon)
{
	CALL_TRACE("Padlock(centerX, yOrg, timingValue)");

	// Set the Y coordinate 
	setY(yOrg);

	// Locked bitmap is displayed in inverse video
	lockedBitmap_.setColor(Colors::WHITE);

	// Add unlocked bitmap to the label container of the unlocked button.
	unlockedButton_.addLabel(&unlockedBitmap_);

	// Register for button events.
	unlockedButton_.setButtonCallback(this);
														// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~Padlock()  [Destructor]
//
//@ Interface-Description
// Padlock destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Padlock::~Padlock(void)
{
	CALL_TRACE("Padlock::~Padlock(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: valueUpdate
//
//@ Interface-Description
// Called when the Constant During Rate Change setting has changed.
// If this padlock is visible it will update its display according to the
// current setting value.  Passed the following parameters:
// >Von
//	settingId	The setting id which changed (PCV_CONSTANT_PARM_ID).
//	contextId	The context in which the change happened, either
//				ACCEPTED or ADJUSTABLE.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
// We ignore the event if we're invisible otherwise we simply call
// updateDisplay_().
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
Padlock::valueUpdate(const Notification::ChangeQualifier,
					 const SettingSubject* pSubject)
{
	CALL_TRACE("valueUpdate(qualifierId, pSubject)");

	// Ignore event if we're invisible.
	if (isVisible())
	{													// $[TI1]
		// Update our display
		updateDisplay_(pSubject);
	}													// $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: buttonDownHappened
//
//@ Interface-Description
// Called when the unlocked padlock button is pressed down.  We must set the
// Constant During Rate Change setting appropriately.
//---------------------------------------------------------------------
//@ Implementation-Description
// We bounce the button back into the up position as the button should
// never stay down.  We then set the Constant During Rate Change setting
// to the value timingValue_.  This will cause the valueUpdate()
// methods of all three padlocks to be called and hence the display will
// update correctly.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
Padlock::buttonDownHappened(Button *, Boolean)
{
	CALL_TRACE("buttonDownHappened(Button *, Boolean)");

	// Bounce the button
	unlockedButton_.setToUp();

	// Change the Constant During Rate Change setting
	getSubjectPtr_(SettingId::CONSTANT_PARM)->calcNewValue(timingValue_);
														// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activate
//
//@ Interface-Description
// Called when the subscreen containing this padlock is about to display us
// (this padlock).  Informs us to update our display according to the current
// Constant During Rate Change setting value.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply call the updateDisplay_() method.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
Padlock::activate(void)
{
	CALL_TRACE("activate()");

	// monitor value changes to constant parm setting...
	attachToSubject_(SettingId::CONSTANT_PARM, Notification::VALUE_CHANGED);

	// Tell ourselves to update
	updateDisplay_(getSubjectPtr_(SettingId::CONSTANT_PARM));
}									// $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: deactivate
//
//@ Interface-Description
// Called when the subscreen containing this padlock is being dismissed.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Simply detach itself from the Settings Subject.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
Padlock::deactivate(void)
{
	CALL_TRACE("deactivate()");

	// stop monitoring value changes to constant parm setting...
	detachFromSubject_(SettingId::CONSTANT_PARM, Notification::VALUE_CHANGED);
}									// $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateDisplay_ [Private]
//
//@ Interface-Description
// Called when the display of the Padlock needs to be updated, normally
// due to a change in the Contsant During Rate Change setting.
//---------------------------------------------------------------------
//@ Implementation-Description
// If the current value of the Constant During Rate Change value is
// timingValue_ then we display the locked bitmap else we display the unlocked
// button.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
Padlock::updateDisplay_(const SettingSubject* pSubject)
{
	CALL_TRACE("updateDisplay_(pSubject)");

	// Decide which display to use, locked or unlocked
	if (DiscreteValue(pSubject->getAdjustedValue()) == timingValue_)
	{													// $[TI1]
		// Do the locked bitmap stuff

		// Remove the unlocked button first
		removeDrawable(&unlockedButton_);

		// Resize and position the container
		setX(centerX_ - lockedBitmap_.getWidth() / 2);
		setWidth(lockedBitmap_.getWidth());
		setHeight(lockedBitmap_.getHeight());
		setFillColor(Colors::BLACK);

		// Add the locked bitmap
		addDrawable(&lockedBitmap_);
	}
	else
	{													// $[TI2]
		// Do the unlocked button stuff

		// Remove the locked bitmap
		removeDrawable(&lockedBitmap_);

		// Resize and position the container
		setX(centerX_ - BUTTON_WIDTH_ / 2);
		setWidth(BUTTON_WIDTH_);
		setHeight(BUTTON_HEIGHT_);
		setFillColor(Colors::NO_COLOR);

		// Add the unlocked button
		addDrawable(&unlockedButton_);
	}
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
Padlock::SoftFault(const SoftFaultID  softFaultID,
				   const Uint32       lineNumber,
				   const char*        pFileName,
				   const char*        pPredicate)  
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, PADLOCK,
  				lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)
