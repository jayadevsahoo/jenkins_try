
#ifndef StandbyScheduler_HH
#define StandbyScheduler_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: StandbyScheduler - handles the ventilator state on powerup
// when the patient circuit is disconnected.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/StandbyScheduler.hhv   25.0.4.0   19 Nov 2013 14:00:10   pvcs  $
//
//@ Modification-Log
//
//  Revision: 007  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 006  By:  iv    Date:  07-Feb-1997    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Rework per Breath Delivery formal code review.
//
//  Revision: 005  By:  iv    Date:  04-Dec-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Cleanup for code inspections.
//
//  Revision: 004 By:  iv   Date:   04-June-1996    DR Number: DCS 600 
//       Project:  Sigma (R8027)
//       Description:
//             Changed method validateScheduler_() to have no arguments.
//
//  Revision: 003 By:  iv   Date:   21-Feb-1996    DR Number: DCS 674 
//       Project:  Sigma (R8027)
//       Description:
//             Removed #ifndef directives surrounding #include directives.
//
//  Revision: 002  By:  kam   Date:  27-Sep-1995    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Updated for BD-GUI user event request and event status interface.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//====================================================================

#  include "Sigma.hh"
#  include "Breath_Delivery.hh"
#  include "IdleModeScheduler.hh"

//@ Usage-Classes
class Trigger;
//@ End-Usage

class StandbyScheduler: public IdleModeScheduler
{
  public:
    StandbyScheduler(void);
    virtual ~StandbyScheduler(void);

    static void SoftFault(const SoftFaultID softFaultID,
  const Uint32      lineNumber,
  const char*       pFileName  = NULL, 
  const char*       pPredicate = NULL);

  protected:
	virtual void setupBreath_(void);
	virtual void determineNextScheduler_(const Trigger::TriggerId triggerId,
						const Boolean isVentSetupComplete);
	virtual void validateScheduler_(void);
	virtual EventData::EventStatus reportEventStatus_(const EventData::EventId id,
													 const Boolean eventStatus);

  private:
    StandbyScheduler(const StandbyScheduler&);		// not implemented...
    void   operator=(const StandbyScheduler&);	// not implemented...

};


#endif // StandbyScheduler_HH 
