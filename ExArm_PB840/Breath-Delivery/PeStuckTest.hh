#ifndef PeStuckTest_HH
#define PeStuckTest_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
//  Class: PeStuckTest - BD Safety Net Test for the detection of
//                       expiratory pressure transducer reading stuck
//                       due to a malfunctioning solenoid
//---------------------------------------------------------------------
//@ Version
// @(#) $Header::   /840/Baseline/Breath-Delivery/vcssrc/PeStuckTest.hhv   10.7   08/17/07 09:40:56   pvcs  
//
//@ Modification-Log
//
//  Revision: 004  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 003  By:  iv    Date: 17-Oct-1997     DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//           Reworked per formal code review.
//
//  Revision: 002  By:  iv    Date: 15-Oct-1997     DR Number:DCS 1649
//       Project:  Sigma (R8027)
//       Description:
//           Revise detection algorithm to eliminate dependency on disconnect.
//
//  Revision: 001  By:  by    Date:  20-Sep-1996    DR Number: None
//    Project:  Sigma (840)
//    Description:
//        Initial version.
//
//====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"
#include "SafetyNetTest.hh"
#include "BreathPhaseType.hh"

//@ Usage-Classes
#include "Background.hh"
//@ End-Usage

class PeStuckTest : public SafetyNetTest
{
    public:

        PeStuckTest( void );
        ~PeStuckTest( void );

        static void SoftFault( const SoftFaultID softFaultID,
			       const Uint32      lineNumber,
			       const char*       pFileName  = NULL,
			       const char*       pPredicate = NULL );

        virtual BkEventName checkCriteria( void );
  
    protected:

    private:

        // these methods are purposely declared, but not implemented...
        PeStuckTest( const PeStuckTest& );        // not implemented...
        void operator=( const PeStuckTest& );    // not implemented...

        void resetCriteria_( void );
        
        //@ Data-Member: peStuckFlag_ 
	    // Indicates whether Pe stuck condition has been detected
        Boolean peStuckFlag_;

        //@ Data-Member: bkEventReported_ 
	    // Indicates whether Pe stuck condition has been reported 
        Boolean bkEventReported_;

        //@ Data-Member: phaseTime_ 
	    // Counts the time in ms during the current phase
        Int32 phaseTime_;

        //@ Data-Member: detectionDisqualified_ 
	    // Used to disqualify detection of Pe stuck
        Boolean detectionDisqualified_;

        //@ Data-Member: previousPeStuckStatus_ 
	    // Indicates if Pe stuck was detected in the previous breath phase
        Boolean previousPeStuckStatus_;

        //@ Data-Member: phaseType_ 
	    // The current breath phase type, e.g. - inspiration, exhalation - is stored
        BreathPhaseType::PhaseType phaseType_;
};


#endif // PeStuckTest_HH 
