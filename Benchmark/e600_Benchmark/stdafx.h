// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently,
// but are changed infrequently

#pragma once

#pragma comment(linker, "/nodefaultlib:libc.lib")
#pragma comment(linker, "/nodefaultlib:libcd.lib")

// NOTE - this is value is not strongly correlated to the Windows CE OS version being targeted
#define WINVER _WIN32_WCE

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers
#endif

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS	// some CString constructors will be explicit
#ifdef _CE_DCOM
#define _ATL_APARTMENT_THREADED
#endif

// turns off MFC's hiding of some common and often safely ignored warning messages
#define _AFX_ALL_WARNINGS

#include <ceconfig.h>
#if defined(WIN32_PLATFORM_PSPC) || defined(WIN32_PLATFORM_WFSP)
#define SHELL_AYGSHELL
#endif

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions

#ifndef _AFX_NO_OLE_SUPPORT
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#endif



#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT



#if defined(WIN32_PLATFORM_PSPC) || defined(WIN32_PLATFORM_WFSP)
#ifndef _DEVICE_RESOLUTION_AWARE
#define _DEVICE_RESOLUTION_AWARE
#endif
#endif

#ifdef _DEVICE_RESOLUTION_AWARE
#include "DeviceResolutionAware.h"
#endif


#if (_WIN32_WCE < 0x500) && ( defined(WIN32_PLATFORM_PSPC) || defined(WIN32_PLATFORM_WFSP) )
	#pragma comment(lib, "ccrtrtti.lib")
	#ifdef _X86_	
		#if defined(_DEBUG)
			#pragma comment(lib, "libcmtx86d.lib")
		#else
			#pragma comment(lib, "libcmtx86.lib")
		#endif
	#endif
#endif

#include <altcecrt.h>


#include <deque>
#include <vector>
#include <map>

using namespace std;

#define	PLOT_COUNT			5

const int PLOT_WIDTH		= 810;		// 540
const int PLOT_HEIGHT		= 270;		// 180

const int GRAPH_COLUMNS		= 1;

const int DATA_SPEED		= 20;				// 20ms per data, that means 50 data per second
const int MM_TIMER_PERIOD	= 20;				// multi-media timer period in ms

const int USE_DATA_SAMPLE	= 1;
const int SHOW_MONITORS		= 1;

const int DRAW_CYCLE		= 66;				// draw cycle 66 ms

#define MonitorBkgndColor	RGB(132, 132, 132)
#define MonitorGridColor	RGB(152, 152, 152)
#define MonitorTextColor	RGB(222, 222, 222)

// #define DIRECT_DRAW							// Option for direct draw

#include "TimeSnapshot.h"
#include "GlobalObjects.h"
#include <Windows.h>
