
//*****************************************************************************
//
//  PhysicalMembraneButtons.h - Header file containing membrane button defs.
//      See MembraneButtons.h for logocal membrane buttons
//
//*****************************************************************************

#ifndef PHYSICAL_MEMBRANE_BUTTONS_H

#define PHYSICAL_MEMBRANE_BUTTONS_H

// The enum order MUST correspond to the bit order of the buttons input to the 74ALS251 hardware register
enum PhysicalMembraneButtons_t
{
    PHYSICAL_MEMBRANE_BUTTON_ON_OFF,
    PHYSICAL_MEMBRANE_BUTTON_UP,
    PHYSICAL_MEMBRANE_BUTTON_DIMNESS,
    PHYSICAL_MEMBRANE_BUTTON_ACCEPT,
    PHYSICAL_MEMBRANE_BUTTON_MAN_BREATH,
    PHYSICAL_MEMBRANE_BUTTON_SILENCE_RESET,
    PHYSICAL_MEMBRANE_BUTTON_CANCEL,
    PHYSICAL_MEMBRANE_BUTTON_DOWN,
    NUMBER_OF_PHYSICAL_MEMBRANE_BUTTONS
};


#endif

