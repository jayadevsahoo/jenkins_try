#ifndef	PrinterInterface_HH
# define PrinterInterface_HH

//====================================================================
// This is a proprietary work to which Nellcor Puritan Bennett
// corporation of California claims exclusive right.  No part of this
// work may be used, disclosed, reproduced, sorted in an information
// retrieval system, or transmitted by any means, electronic,
// mechanical, photocopying, recording, or otherwise without the prior
// written permission of Nellcor Puritan Bennett Corporation.
//
//     Copyright (c) 1997, Nellcor Puritan Bennett Corporation
//=====================================================================

//=====================================================================
//@ Class:  PrinterInterface - HP-PCL Printer Driver
//---------------------------------------------------------------------
//@ Version-Information
// @ $Header:   /840/Baseline/GUI-Serial-Interface/vcssrc/PrinterInterface.hhv   25.0.4.0   19 Nov 2013 14:12:44   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002    By: quf         Date: 26-Sep-2001  DR Number: 5942
//  Project: GuiComms
//  Description:
//  Code/comment/header cleanup.
//
//  Revision: 001    By: gdc         Date: 29-Jan-2001  DR Number: 5493
//  Project: GuiComms
//  Description:
//      Initial version
//
//=====================================================================

#include "GUI_Serial_Interface.hh"
#include "SerialInterface.hh"
#include "ScreenCapture.hh"

//@ Usage-Classes
//@ End-Usage

class SerialPort;

class PrinterInterface : public SerialInterface
{
  public:

	PrinterInterface( SerialPort & rSerialPort,
            SerialInterface::PortNum portNum );
	~PrinterInterface();

	static void  SoftFault(const SoftFaultID softFaultID,
					       const Uint32	lineNumber,
					       const char *	pFileName = NULL,
					       const char *	pPredicate = NULL);

  protected:

  private:

	virtual void processRequests_(void);

	void printRasterData_(ScreenCapture& rScreenCapture);
	void printDivider_(void);
	void printWhiteSpace_(void);
	void setupPrinter_(void);
	void finishPrinting_(void);
};

#endif	// ! PrinterInterface_HH
