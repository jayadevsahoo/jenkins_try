#include "stdafx.h"

//====================================================================
// This is a proprietary work to which Nellcor Puritan Bennett
// corporation of California claims exclusive right.  No part of this
// work may be used, disclosed, reproduced, sorted in an information
// retrieval system, or transmitted by any means, electronic,
// mechanical, photocopying, recording, or otherwise without the prior
// written permission of Nellcor Puritan Bennett Corporation.
//
//     Copyright (c) 1997, Nellcor Puritan Bennett Corporation
//=====================================================================

// =========================== C L A S S   D E S C R I P T I O N ====
//@ Class: DciValueStatus - Store a validity tag (base class for DciNumber
// and DciString objects)
//---------------------------------------------------------------------
//@ Interface-Description
//---------------------------------------------------------------------
//@ Rationale
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/DCI/vcssrc/DciValueStatus.ccv   25.0.4.0   19 Nov 2013 14:01:58   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001    By: sah     Date: 15-Jan-1999  DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' to a non-inlined method.  Created via this
//	DCS.
//
//=====================================================================

#include "Sigma.hh"
#include "DciValueStatus.hh"

//@ Usage-Classes
//@ End-Usage


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//@ Interface-Description
// Implicitly gain execution control when a software assertion macro
// detects and error.
//---------------------------------------------------------------------
//@ Implementation-Description
// Automatically invoked when CLASS_ASSERTION and CLASS_PRE_CONDITON
// macros (among others) detect a fault.  We just call FaultHandler::SoftFault
// to display pertinent information to the user.
//---------------------------------------------------------------------
//@ PreCondition
// A fault handling assertion has been taken.
//---------------------------------------------------------------------
//@ PostCondition
// There is no return from this function.
//@ End-Method
//=====================================================================

void
DciValueStatus::SoftFault(const SoftFaultID softFaultID,
						  const Uint32	lineNumber,
						  const char *	pFileName,
						  const char *	pPredicate)
{
	FaultHandler::SoftFault(softFaultID, DCI_DEVICE, DCI_VALUE_STATUS_CLASS,
		lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)
