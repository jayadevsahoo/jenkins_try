//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file PatientDataUIValue.h
//----------------------------------------------------------------------------
#ifndef VALUE_H
#define VALUE_H

//-------------------------------------
// I N C L U D E S
//-------------------------------------
#include"IPatientDataSet.h"
#include"Winsock2.h"

//-------------------------------------
// C L A S S  D E C L
//-------------------------------------


class Value:public IPatientDataEl
{
    public:

    Value(char *name);
    char *getName() ; 

    bool invalidate();
    bool unsetValue();

    bool isValid();
    bool isEnum();
    bool isFloat();
    bool isInt();
    bool isWaveform();
    bool isSinSignal();
    bool isTrigger();


    bool setFloat(float fValue);
    bool setInt  (int   iValue);
    bool setEnum (unsigned int eValue);
    bool setSinSignal(double dFrequency,double dPhase,double dMin,double dMax);
    bool setTriggerSignal(double dFrequency,double dPhase,double dMin,double dMax, double trigger_high, double trigger_low);
    bool setIsSetFlag();


    char  * getEnumValue();
    float   getFloatValue();
    int     getIntValue();
    char  * getEnum(int cnt);
    double  getFrequency();
    double  getPhase();
    double  getMax();
    double  getMin();
    double  getTriggerHigh();
    double  getTriggerLow ();



    protected:
    bool m_bIsDesired;
    bool m_bIsSet;
    char * m_pszName;
};
#endif

