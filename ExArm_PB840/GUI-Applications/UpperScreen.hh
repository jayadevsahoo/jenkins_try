#ifndef UpperScreen_HH
#define UpperScreen_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//            Copyright (c) 1993-1994, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: UpperScreen - The base container for all upper screen containers.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/UpperScreen.hhv   25.0.4.0   19 Nov 2013 14:08:40   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003   By: hhd    Date:  16-Nov-1999    DCS Number: 5327
//  Project: Neo-Mode
//  Description:
// 	Modified as part of a general cleanup effort for Neo-Mode.
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  wks    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "Container.hh"

//@ Usage-Classes
#include "AlarmAndStatusArea.hh"
#include "VitalPatientDataArea.hh"
#include "UpperScreenSelectArea.hh"
#include "UpperSubScreenArea.hh"
#include "GuiAppClassIds.hh"
//@ End-Usage

class UpperScreen : public Container
{
public:
	UpperScreen(void);
	~UpperScreen(void);

	inline AlarmAndStatusArea*    getAlarmAndStatusArea(void);
	inline VitalPatientDataArea*  getVitalPatientDataArea(void);
	inline UpperSubScreenArea*	  getUpperSubScreenArea(void);
	inline UpperScreenSelectArea* getUpperScreenSelectArea(void);

	void patientSetupComplete(void);
	static inline void Initialize(void);

	static void SoftFault(const SoftFaultID softFaultID,
							     const Uint32      lineNumber,
							     const char*       pFileName  = NULL, 
							     const char*       pPredicate = NULL);

	// Reference to LowerScreen, used by many to access public methods of this class
  	static UpperScreen&  RUpperScreen;

protected:

private:
	// these methods are purposely declared, but not implemented...
	UpperScreen(const UpperScreen&);		// not implemented...
	void operator=(const UpperScreen&);		// not implemented...

	//@ Data-Member: alarmAndStatusArea_
	// The AlarmAndStatusArea container.
	AlarmAndStatusArea alarmAndStatusArea_;

	//@ Data-Member: vitalPatientDataArea_
	// The VitalPatientDataArea container.
	VitalPatientDataArea vitalPatientDataArea_;

	//@ Data-Member: upperSubScreenArea_
	// The UpperSubScreenArea container.
	UpperSubScreenArea upperSubScreenArea_;

	//@ Data-Member: upperScreenSelectArea_
	// The UpperScreenSelectArea container.
	UpperScreenSelectArea upperScreenSelectArea_;
};

// Inlined methods
#include "UpperScreen.in"

#endif // UpperScreen_HH 
