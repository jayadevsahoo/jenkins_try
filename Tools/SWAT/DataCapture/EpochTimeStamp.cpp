//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file EpochTimeStamp.cpp
//----------------------------------------------------------------------------

//-------------------------------------
// I N C L U D E S
//-------------------------------------
#include"EpochTimeStamp.h"
#include"gettimeofday.h"

//-------------------------------------
// C O D E
//-------------------------------------

//---------------------------
// EpochTimeStamp::EpochTimeStamp()
//---------------------------
EpochTimeStamp::EpochTimeStamp():m_tv()
{
    gettimeofday(&m_tv, NULL);
}

//---------------------------
// EpochTimeStamp::~EpochTimeStamp()
//---------------------------
EpochTimeStamp::~EpochTimeStamp()
{
}

//---------------------------
// EpochTimeStamp::EpochTimeStamp(EpochTimeStamp &rts)
//---------------------------
EpochTimeStamp::EpochTimeStamp(EpochTimeStamp &rts):m_tv(rts.m_tv) 
{
}

//---------------------------
// EpochTimeStamp::getEpochTimeAsDouble()
//---------------------------
double EpochTimeStamp::getEpochTimeAsDouble()
{
    return timeval2epoch(&m_tv);
}

//---------------------------
// EpochTimeStamp::set(const EpochTimeStamp &ts)
//---------------------------
void EpochTimeStamp::set(const EpochTimeStamp &ts)
{
    m_tv.tv_sec  = ts.m_tv.tv_sec; 
    m_tv.tv_usec = ts.m_tv.tv_usec; 
}

//---------------------------
// EpochTimeStamp::set()
//---------------------------
void EpochTimeStamp::set()
{
    gettimeofday(&m_tv, NULL);
}


