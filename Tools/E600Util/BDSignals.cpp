#include "stdafx.h"
#include "BDSignals.h"

/* E600 Default Signals */
/*
const TCHAR *DEFAULT_SIGNALS[MAX_SELECTED_SIGNALS] = 
{
	{L"Qo2"},
	{L"Qair"},
	{L"Qexh"},
	{L"Pinsp"},
	{L"Pexp"},
	{L"Pdrv"},
	{L"Qaird"},
	{L"Qo2d"},
	{L"Pinsd"},
	{L"FIO2"},
	{L"Pbar"},
	{L"DAC_Air"},
	{L"DAC_O2"},
	{L"DAC_Exh"},
	{L"ADC_Tair"},
	{L"ADC_Qexh"},
	{L"Pawy"},
	{L"Phase"}
};*/

/* PB880 Default Signals */
const TCHAR *DEFAULT_SIGNALS[MAX_SELECTED_SIGNALS] = 
{
	{L"Qo2"},
	{L"Qair"},
	{L"Qexh"},
	{L"Pinsp"},
	{L"Pexp"},
	{L"DAC_Exh"},
	{L"Qaird"},
	{L"Qo2d"},
	{L"Pinsd"},
	{L"ADC_Qo2"},
	{L"ADC_Qair"},
	{L"DAC_Air"},
	{L"DAC_O2"},
	{L"Pdrv"},
	{L"ADC_To2"},
	{L"ADC_Tair"},
	{L"ADC_Qexh"},
	{L"Phase"}
};

const TCHAR *DEFAULT_ENGINEERING_SIGNALS[MAX_SELECTED_SIGNALS] = 
{
	{L"Qo2"},
	{L"Qair"},
	{L"Qexh"},
	{L"Pinsp"},
	{L"Pexp"},
	{L"Pdrv"},
	{L"Pair"},
	{L"Po2"},
	{L"Pawy"},
	{L"ADC_Qo2"},
	{L"Pbar"},
	{L"DAC_Air"},
	{L"DAC_O2"},
	{L"DAC_Exh"},
	{L"ADC_Tair"},
	{L"ADC_Qexh"},
	{L"ADC_Qair"},
	{L"ADC_TO2"}
};


CString GlobalHeaderString;


void ParseCSVHeader(CString HeaderStr, vector<CString> &SignalsArr)
{
	CString ParseStr = HeaderStr;

	while(true)
	{
		int Stop = ParseStr.Find(L",", 0);
		if(Stop >= 0)
		{
			CString MonLabel = ParseStr.Left(Stop);
			SignalsArr.push_back(MonLabel);
			ParseStr.Delete(0,Stop + 1);
		}
		else
		{
			if(!ParseStr.IsEmpty())
				SignalsArr.push_back(ParseStr);
			break;
		}
	}
}


void SwapSignalStrings(int FirstSig, vector<CString> &Signals)
{
	CString TempStr = Signals[FirstSig];
	Signals[FirstSig] = Signals[FirstSig + 1];
	Signals[FirstSig + 1] = TempStr;
}


void SetGlobalHeaderString(vector<CString>Signals)
{
	GlobalHeaderString = L"";
	for(int i=0; i<(int)Signals.size(); i++)
	{
		GlobalHeaderString += Signals[i];
		if(i != (int)Signals.size() - 1)
			GlobalHeaderString += L",";
	}
}

	

