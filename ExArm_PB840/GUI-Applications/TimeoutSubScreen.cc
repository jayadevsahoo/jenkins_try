#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: TimeoutSubScreen - This class handles the timeouts for 
//  settings changes when no visible BatchSettingSubScreen is active
//---------------------------------------------------------------------
//@ Interface-Description 
//  This is a virtual subscreen that is never displayed. Its purpose is
//  to be a GuiTimerTarget for the SUBSCREEN_SETTING_CHANGE_TIMEOUT timer.
// 
//---------------------------------------------------------------------
//@ Rationale
//  To provide a target for the SUBSCREEN_SETTING_CHANGE_TIMEOUT timer
//  when target subscreens are not displayed.
//---------------------------------------------------------------------
//@ Implementation-Description
//  A subscreen area is required for derived class of
//  BatchSettingsSubScreen to provide an address for the drawable objects.
//  Since this subscreen has no drawable objects, we provide a NULL
//  subscreen area to the base class. Subscreen areas are allocated in the 
//  lowerSubScreenArea and upperSubScreenArea classes. We could not use
//  this interface since these areas are managed to have only one
//  subscreen in a subscreen area at a time. TimeoutSubscreen is
//  required to be active at the same time as other drawable subscreens.
// 
//  This subscreen acts as a NULL GuiTimerTarget. When another 
//  BatchSettingsSubscreen is not activated, this object receives the 
//  timer event and deactivates and reactivates itself through the
//  BatchSettingsSubscreen base class. This sequence grabs the focus 
//  from whomever has the focus resulting in the 3-minute timeout
//  behavior on the upper and lower screen.
// 
//  Prior to this class being implemented, a BatchSettingsSubScreen 
//  was required to be activated in order for timeouts on 
//  non-BatchSettingsSubscreens to work.
//---------------------------------------------------------------------
//@ Fault-Handling
//  SoftFault is used
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/TimeoutSubScreen.ccv   25.0.4.0   19 Nov 2013 14:08:36   pvcs  $
//
// @ Modification-Log
//
//  Revision: 001  By:  ksg    Date:  19-June-2007    SCR Number: 6237
//  Project:  Trend
//  Description: Initial version.
//
//=====================================================================
#include "GuiAppClassIds.hh"
#include "TimeoutSubScreen.hh"
#include "MiscStrs.hh"
#include "PromptArea.hh"
#include "Sound.hh"
#include "ContextSubject.hh"
#include "GuiTimerRegistrar.hh"


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TimeoutSubScreen()  [Constructor]
//
//@ Interface-Description
//  Constructs a TimeoutSubscreen. Forwards to base class,
//  BatchSettingsSubScreen.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

TimeoutSubScreen::TimeoutSubScreen(void):
	nullSubScreenArea_(),
	BatchSettingsSubScreen(&nullSubScreenArea_)
{
	// do nothing
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Initialize [static]
//
//@ Interface-Description
//  Intialize the object, called by GuiApp.cc
//---------------------------------------------------------------------
//@ Implementation-Description
//  Initializes object by generating the first call to 
//  GetTimeoutSubScreen()
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TimeoutSubScreen::Initialize( void )
{
	// First call to the static constructor of TimeoutSubScreen creates
	// the object
	TimeoutSubScreen & timeoutSubScreen = GetTimeoutSubScreen();
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~TimeoutSubScreen  [Destructor]
//
//@ Interface-Description
//  Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

TimeoutSubScreen::~TimeoutSubScreen(void)	
{
	CALL_TRACE("~TimeoutSubScreen(void)");
	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activateHappened_
//
//@ Interface-Description
//  Overloaded pure virtual function from BatchSettingsSubScreen base
//  class.  
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void TimeoutSubScreen::activateHappened_(void)
{
	CALL_TRACE("activateHappened_()");
	// do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: deactivateHappened_
//
//@ Interface-Description
//  Overloaded pure virtual function from BatchSettingsSubScreen base
//  class.  
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void TimeoutSubScreen::deactivateHappened_(void)
{
	CALL_TRACE("deactivateHappened_()");
	// do nothing
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: acceptHappened
//
//@ Interface-Description
//  Called by the BatchSettingsSubScreen when the Accept key is pressed.
//  Since there are no settings to accept in this subscreen, just 
//  produces the invalid entry sound
//---------------------------------------------------------------------
//@ Implementation-Description
// 
//  Satisfies requirements:
//  $[01215] The Accept sound shall be produced whenever the accept ...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void TimeoutSubScreen::acceptHappened(void)
{
	CALL_TRACE("acceptHappened()");
	Sound::Start(Sound::INVALID_ENTRY);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: timerEventHappened
//
//@ Interface-Description
//  Called when the SUBSCREEN_SETTING_CHANGE_TIMEOUT timer is triggered.  
//  GuiTimerTarget virtual method
// ---------------------------------------------------------------------
//@ Implementation-Description
//  Calls the base class deactivate() and activate() functions to release
//  and then grab focus, thus timing out the setting change timer from
//  whomever currently has focus.
//
//  Satisfies requirements:
//  $[01087] After 3 minutes of user inactivity ...
//---------------------------------------------------------------------
//@ PreCondition
//  The timer id must be the Subscreen Setting Change timer id.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void TimeoutSubScreen::timerEventHappened(GuiTimerId::GuiTimerIdType timerId)
{
	CALL_TRACE("timerEventHappened(timerId)");

	if (timerId == GuiTimerId::SUBSCREEN_SETTING_CHANGE_TIMEOUT)
	{
		deactivate();
		activate();
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetTimeoutSubScreen
//
//@ Interface-Description
//  Return a static instance of the TimeoutSubScreen
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
TimeoutSubScreen & TimeoutSubScreen::GetTimeoutSubScreen( void )
{
	static TimeoutSubScreen TimeoutSubScreen_;
	return( TimeoutSubScreen_ );
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: valueUpdateHappened_
//
//@ Interface-Description
//  This function is overloading a pure virtual function from
//  BatchSettingsSubScreen
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void TimeoutSubScreen::valueUpdateHappened_(
										   const BatchSettingsSubScreen::TransitionId_ transitionId,
										   const Notification::ChangeQualifier         qualifierId,
										   const ContextSubject*                       pSubject
										   )
{
	CALL_TRACE("valueUpdateHappened_(transitionId, qualifierId, pSubject)");
	// do nothing
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void TimeoutSubScreen::SoftFault(const SoftFaultID  softFaultID,
								 const Uint32       lineNumber,
								 const char*        pFileName,
								 const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, TIMEOUTSUBSCREEN,
							lineNumber, pFileName, pPredicate);
}

