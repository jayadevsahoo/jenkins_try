
#ifndef BtreeVars_HH
#define BtreeVars_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1994, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: BtreeVars -- Used for BTree-specific variables/triggers.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Utilities/vcssrc/BtreeVars.hhv   25.0.4.0   19 Nov 2013 14:35:12   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By:  sah    Date:  14-Aug-97    DCS Number: 2320
//  Project:  Sigma (R8027)
//  Description:
//       Integration baseline.
//
//====================================================================

#include "Sigma.hh"

//@ Usage-Classes
//@ End-Usage


struct BtreeVars
{
  //@ Data-Member:  CurrAlarmUrgency
  // Storage for current alarm urgency.
  static Byte  CurrAlarmUrgency;
};


#endif // BtreeVars_HH 
