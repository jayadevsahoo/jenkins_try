#pragma once
#include "PressureSensor.hh"


// Forward Declarations
class TransducerCalibrationTable_t;

class GasInletPressureSensor : public PressureSensor
{
public:


    GasInletPressureSensor( SafetyNetSensorData *pData,
				const AdcChannels::AdcChannelId AdcId,
				const Real32 alpha,
				TransducerCalibrationTable_t * transducerCalTable) ;

    static void SoftFault( const SoftFaultID softFaultID,
					   const Uint32      lineNumber,
					   const char*       pFileName  = NULL,
					   const char*       pPredicate = NULL ) ;

protected:

	virtual Real32 rawToEngValue_( const AdcCounts count) const ;


private:
	~GasInletPressureSensor(void);

	// Not implemented
	GasInletPressureSensor& operator=( const GasInletPressureSensor& );
	GasInletPressureSensor( const GasInletPressureSensor& );
};
