#ifndef GuiEventTarget_HH
#define GuiEventTarget_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: GuiEventTarget - A target for changes in the GUI state.
// Classes can specify this class as an additional parent class and register to
// be informed of state changes such as vent inoperative.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/GuiEventTarget.hhv   25.0.4.0   19 Nov 2013 14:07:52   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003   By: hhd    Date:  16-Nov-1999    DCS Number: 5327
//  Project: Neo-Mode
//  Description:
// 	Modified as part of a general cleanup effort for Neo-Mode.
//
//  Revision: 002  By: sah    Date:  07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 001  By:  mpm    Date:  29-AUG-95    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

//@ Usage-Classes
#include "GuiApp.hh"
//@ End-Usage

class GuiEventTarget
{
public:
	// Inheriting classes will be informed of GUI state changes through this
	// method.
	virtual void guiEventHappened(GuiApp::GuiAppEvent eventId);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:
	GuiEventTarget(void);
	virtual ~GuiEventTarget(void);


private:
	// these methods are purposely declared, but not implemented...
	GuiEventTarget(const GuiEventTarget&);	// not implemented...
	void operator=(const GuiEventTarget&);	// not implemented...
};


#endif // GuiEventTarget_HH 
