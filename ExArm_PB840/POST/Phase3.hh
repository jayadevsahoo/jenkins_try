#ifndef Phase3_HH
#define Phase3_HH
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================ H E A D E R   D E S C R I P T I O N ====
// Class: Phase3.hh
//
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/POST/vcssrc/Phase3.hhv   25.0.4.0   19 Nov 2013 14:17:04   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001    By: Gary Cederquist Date: 04-MAR-1997 DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//=====================================================================

#include "Post.hh"

class Phase3 : public Post
{
  public:
    static const PostTestItem * GetTestItems(void);

  private:
    Phase3();         // not implemented
    ~Phase3();        // not implemented

    static const PostTestItem TestItems_[];
};


#endif  // Phase3_HH
