#ifndef SyncClockMessage_HH
#define SyncClockMessage_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//====================================================================
 
//====================================================================
// Class:  SyncClockMessage - Sys-Init subsystem class
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Sys-Init/vcssrc/SyncClockMessage.hhv   25.0.4.0   19 Nov 2013 14:33:56   pvcs  $
//
//@ Modification-Log
//
//   Revision 002   By:   sah      Date: 01/13/99    DR Number: 5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to a non-inlined method.
//
//   Revision 001   By:   gdc      Date: 05/16/95    DR Number:   n/a
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//====================================================================
 
#if 0  
// for bug in genDD
#endif

//@ Usage-Classes
#include "Sys_Init.hh"
#include "TaskControlMessage.hh"
#include "TimeStamp.hh"
//@ End-Usage
 
class SyncClockMessage : public TaskControlMessage {
  public:
 
    SyncClockMessage(Boolean userInitiated = FALSE);
    SyncClockMessage(const SyncClockMessage& rSyncClocks);
    ~SyncClockMessage();

    const char* nameOf(void) const;
    void dispatch(const AppContext& context) const;

    inline const TimeStamp&     getTimeConstructed(void) const;
    inline Boolean              isUserInitiated(void) const;


    static void          SoftFault(const SoftFaultID  softFaultID,
				   const Uint32       lineNumber,
				   const char*        pFileName = NULL,
				   const char*        pPredicate = NULL);
 
  private:
    void operator=(const SyncClockMessage&);   // declared only

    //@ Data-Member:    timeConstructed_
    //  Time stamp for when the message was constructed.
    TimeStamp             timeConstructed_;
 
    //@ Data-Member:    userInitiated_
    //  Indicates if the user changed the time or if this message
    //  is simply a clock synchronization
    Boolean             userInitiated_;
 
};
 
#include "SyncClockMessage.in"

#endif // SyncClockMessage_HH
