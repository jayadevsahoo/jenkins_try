#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: BreathSet - Set of breath records.
//---------------------------------------------------------------------
//@ Interface-Description
// This class maintains a set of breath records.  Methods are
// provided for the following functions:
//   -retrieving the record in use for the current breath
//   -retrieving the record of the last breath delivered.  If the method
//    returns a NULL value, then there is no breath record for any breath
//    delivered previous to the current one.
//   -performing initialization of a new record when a new breath
//    is initiated.  The currentRecord_ index and the count of records
//    in the set are updated by this method.  
//   -initialization for the set to be empty.
//   -extracting BreathSet's indexes to be send to spiro task.
// Any object desiring to search through the list of records must use a
// BreathSetIterator object.
//---------------------------------------------------------------------
//@ Rationale
//   It is required to store information regarding each breath delivered (see
//   BreathRecord) for up to the last BREATH_HISTORY_TABLE_SIZE breaths.  
//   The number BREATH_HISTORY_TABLE_SIZE was selected to provide 
//   a large enough number for a proper average breath data calculations.
//   This information is used in calculating spirometric averages, apnea
//   auto-reset, and for other various breath scheduler and controller
//   algorithms.  If the data stored in the set represents less than 1 minute
//   of breaths delivered, then the calculations based on the data in this set
//   must be normalized.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This set of BreathRecords is implemented using circular array.
//   The const NUM_PREV_BREATH_RECORDS is the maximum number of breath
//   records you can traverse backward to prevent the breath record that is
//   being read from over written.
//   The data member numRecords_ keeps a count of the number of records 
//   stored in the set.  The data member currentRecord_ keeps an 
//   index to the record that contains information about the current breath.
//   The data member numRespRateRecords_ keeps track of number of breath 
//   records that may be used in calculating the respiratory rate.
//   The data member numTidalVolumeRecords_ keeps track of number of breath 
//   records that may be used in minute volume calculations.
//   The data members tidalVolume_ and respRate_ are duplicate copies 
//   of the current valid settings.  These are required 
//   for tracking of the number of breath records that
//   may be accessed for the averaged breath data calculations.  The
//   setting values and record counts are updated when a new breath record
//   is initialized. 
//---------------------------------------------------------------------
//@ Fault-Handling
//   n/a.
//---------------------------------------------------------------------
//@ Restrictions
//   Any object desiring to search through the set of records is required 
//   to use a BreathSetIterator.
//   Only one instance of this class may be created. 
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/BreathSet.ccv   25.0.4.0   19 Nov 2013 13:59:40   pvcs  $
//
//@ Modification-Log
//
//  Revision: 014  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 013  By:  syw    Date:  07-Jul-1998    DR Number: DCS 132
//       Project:  Sigma (840)
//       Description:
//			Added handle for BILEVEL_PAUSE_PHASE.
//
//  Revision: 012  By:  iv    Date:  18-Jan-1998    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//          Bilevel initial version.
//
//  Revision: 011  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 010  By:  sp    Date: 10-Apr-1997    DR Number: DCS 1867
//       Project:  Sigma (R8027)
//       Description:
//             Fixed Spont min volume calculation.
//
//  Revision: 009  By:  sp    Date: 13-Feb-1997    DR Number: DCS 1726
//       Project:  Sigma (R8027)
//       Description:
//             Do not start the 10 seconds timer if the current breath is peep recovery.
//
//  Revision: 008  By:  sp    Date: 13-Feb-1997    DR Number: NONE
//       Project:  Sigma (R8027)
//       Description:
//             Add unit test item (unit test review).
//
//  Revision: 007  By:  sp    Date: 25-Oct-1996    DR Number: NONE
//       Project:  Sigma (R8027)
//       Description:
//             Inspection rework.
//
//  Revision: 006  By:  sp    Date:  9-Oct-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Removed unit test methods.
//
//  Revision: 005  By:  sp    Date:  27-Aug-1996    DR Number:DCS 967
//       Project:  Sigma (R8027)
//       Description:
//             Do not calculate spirometry and start the 10 secs timer
//             if the previous breath is a peep recovery breath.
//
//  Revision: 004  By:  sp    Date:  24-Jun-1996    DR Number:DCS 967 
//       Project:  Sigma (R8027)
//       Description:
//             Add default argument to newBreathRecord.
//
//  Revision: 003  By:  sp    Date:  5-Jun-1996    DR Number:DCS 1022, 997
//       Project:  Sigma (R8027)
//       Description:
//             Fixed bug regarding rAvgBreathDataTimer not being restart
//             on every new breath. 
//
//  Revision: 002  By:  sp    Date:  4-Mar-1996    DR Number:DCS 674
//       Project:  Sigma (R8027)
//       Description:
//             Remove #ifndef.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "BreathSet.hh"

//@ Usage-Classes
#include "PhasedInContextHandle.hh"
#include "BreathPhase.hh"
#include "MathUtilities.hh"
#include "PhaseRefs.hh"
#include "IntervalTimer.hh"
#include "TimersRefs.hh"
#include "MsgQueue.hh"
#include "IpcIds.hh"
#include "ProxManager.hh"
//@ End-Usage

#ifdef SIGMA_UNIT_TEST
#include "Ostream.hh"
#endif //SIGMA_UNIT_TEST

//@ Code...
// instantiate and initialize static data
const Int32 NUM_PREV_BREATH_RECORDS = BREATH_HISTORY_TABLE_SIZE - 10;
const Int32 MAX_SPIRO_INTERVAL = 10000;

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BreathSet()  
//
//@ Interface-Description
//	Default Contstructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

BreathSet::BreathSet(void)
{
    CALL_TRACE("BreathSet()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~BreathSet()  
//
//@ Interface-Description
//	Destructor
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

BreathSet::~BreathSet(void)
{
    CALL_TRACE("~BreathSet()");

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: initialize
//
//@ Interface-Description
//   This method takes no parameters, and has no return value.  It is
//   called whenever it is necessary to reset the BreathSet set to empty.
//---------------------------------------------------------------------
//@ Implementation-Description
//   The numRecords_, numRespRateRecords_, numTidalVolumeRecords_ and 
//   currentRecord_ data members are reset to 0.  This effectively
//   makes all BreathRecords stored in the set unaccessible.
//   Data members tidalVolume_ and respRate_ are initialize to 
//   start up setting value.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
BreathSet::initialize(void)
{
    CALL_TRACE("initialize(void)");
 
    // $[TI1]
    numRecords_ = 0;
    currentRecord_ = 0;
    numRespRateRecords_ = 0;
    numTidalVolumeRecords_ = 0;

    tidalVolume_ = 0.0F; 
    respRate_ = 0.0F; 
    isUsingProxFlow_ = FALSE;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getQueMsg
//
//@ Interface-Description
//   This method takes no parameters.  It returns a Int32 msg that
//   contains BreathSet's indexes.
//---------------------------------------------------------------------
//@ Implementation-Description
//   The message has the following fields: 
//       (1) numRecords_ is embedded in the first order 8 bits(0x000000FF)
//       (2) currentRecord_ is embedded in the second order 8 bits(0x0000FF00)
//       (3) numTidalVolumeRecords_ is embedded in the third order 8 bits(0x00FF0000)
//       (4) numRespRateRecords_ is embedded in the fourth order 6 bits(0x3F000000)
//       (5) Phase type is embedded in the first 2 most significant bits(0xC0000000)
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
Int32
BreathSet::getQueMsg(void)
{
    CALL_TRACE("getQueMsg(void)");
 
    // $[TI1]
    Int32 msg1 = numRecords_;
    Int32 msg2 = currentRecord_;
    Int32 msg3 = numTidalVolumeRecords_;
    Int32 msg4 = numRespRateRecords_;
    Int32 msg5 = (Int32) BreathRecord::GetPhaseType();
    Int32 msg = ((msg5 << 30) | (msg4 << 24) | (msg3 << 16) | (msg2 << 8) | msg1);
 
    return (msg);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newBreathRecord
//
//@ Interface-Description
//  This method takes parameters of schedulerId, mandatoryType, BreathType
//  phaseType , peepRecovery and isBiLevelLowToHigh to be stored in breath record. 
//  The default argument for peepRecovery is FALSE. This method has no return
//  value.  It is called on transition to a new inspiration or exhalation
//  which is only possible in SPONT.
//---------------------------------------------------------------------
//@ Implementation-Description
//   The currentRecord_ index is incremented.  If this index exceeds the 
//   size of the set, then it must be reset to 0, as the set is implemented 
//   as a circular array.  The numRecords_ data member is incremented, 
//   but must be limited to the maximum number of records stored in the set.
//   A message containing numRecords_, currentRecord_, numTidalVolumeRecords_,
//   and numRespRateRecords_ is posted to SPIROMETRY_TASK_Q. 
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
BreathSet::newBreathRecord(
    const SchedulerId::SchedulerIdValue schedulerId,
    const DiscreteValue mandatoryType,
    const BreathType breathType,
    const BreathPhaseType::PhaseType phaseType,
    const Boolean peepRecovery,
   	const Boolean isMandBreath)
{
    CALL_TRACE("newBreathRecord(\
	const SchedulerId::SchedulerIdValue schedulerId,\
    const DiscreteValue mandatoryType,\
    const BreathType breathType,\
    const BreathPhaseType::PhaseType phaseType,\
    const Boolean peepRecovery, \
  	const Boolean isMandBreath)");

    checkSettingChange_(schedulerId);

    currentRecord_++;
    if ( currentRecord_ >  BREATH_HISTORY_TABLE_SIZE - 1)
    {
        //$[TI1]
        currentRecord_ = 0;
    }
    //$[TI2]
 
    numRecords_++;
    if (numRecords_ > BREATH_HISTORY_TABLE_SIZE)
    {
        //$[TI3]
        numRecords_ = BREATH_HISTORY_TABLE_SIZE;
    }
    //$[TI4]

#ifdef SIGMA_TARGET_DEBUG
BreathRecord::write2();
breathRecord_[currentRecord_-1].write1();
#endif //SIGMA_TARGET_DEBUG
   
    breathRecord_[currentRecord_].newInspiration(
	schedulerId, mandatoryType, breathType, phaseType, peepRecovery, isMandBreath, isUsingProxFlow_);

    // notify the spirometry task to start
    if (schedulerId >= SchedulerId::FIRST_BREATHING_SCHEDULER &&
        schedulerId <= SchedulerId::LAST_BREATHING_SCHEDULER)
    {
       // $[TI5]
       	if(phaseType != BreathPhaseType::NULL_INSPIRATION)
       	{
	       // $[TI7]
	       	RAvgBreathDataTimer.setTargetTime(MAX_SPIRO_INTERVAL);
    		RAvgBreathDataTimer.restart();

#ifndef SIGMA_UNIT_TEST       
			MsgQueue::PutMsg(::SPIROMETRY_TASK_Q, getQueMsg());
#endif // SIGMA_UNIT_TEST       
		}	// implied else $[TI8]
    }
    else
    {
       // $[TI6]
       RAvgBreathDataTimer.stop();
    }
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getLastBreathRecord
//
//@ Interface-Description
//   This method takes no parameters, and returns a pointer to a 
//   BreathRecord for the last breath delivered.  If there is no last
//   breath, then a NULL pointer is returned.
//---------------------------------------------------------------------
//@ Implementation-Description
//   If there was a breath delivered previous to the current one, a pointer 
//   to its BreathRecord is returned.  Otherwise a NULL pointer is returned.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
BreathRecord*
BreathSet::getLastBreathRecord(void)
{
    CALL_TRACE("getLastBreathRecord(void)");
 
    BreathRecord* returnValue = NULL;
    Int32 index = currentRecord_ - 1;
 
    //If there is not more than one breath recorded, then return a NULL value
    if (numRecords_ >= 2)
    {
        //$[TI1]
        if (index < 0)
        {
             //$[TI3]
             returnValue = &breathRecord_[BREATH_HISTORY_TABLE_SIZE - 1];
        }
        else
        {
             //$[TI4]
             returnValue = &breathRecord_[index];
        }
    }
    //$[TI2]
 
    return (returnValue);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
BreathSet::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, BREATHSET,
                          lineNumber, pFileName, pPredicate);
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//=====================================================================
//
//  Private Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: checkSettingChange_
//
//@ Interface-Description
//  This method takes schedulerId argument and has no return value.
//  It maintains number of breath records that are available after
//  setting changed in tidal volume and respiratory rate.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Both numRespRateRecords_ and numTidalVolumeRecords_ are incremented.
//  and has upper limit of NUM_PREV_BREATH_RECORDS. numTidalVolumeRecords_ and 
//  numRespRateRecords_ can be reset depending on schedulerId, 
//  current breath phase and change in respiratory rate or tidal volume setting.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
BreathSet::checkSettingChange_(
	const SchedulerId::SchedulerIdValue schedulerId
)
{
    CALL_TRACE("checkSettingChange_(SchedulerId::SchedulerIdValue schedulerId)");

    Real32 tidalVolume = 0;
    Real32 respRate= 0;

    // determine which tidal volume and respiratory rate are being used currently.
    if ( (schedulerId == SchedulerId::AC) || 
         (schedulerId == SchedulerId::BILEVEL) || 
         (schedulerId == SchedulerId::SIMV) || 
         (schedulerId == SchedulerId::SAFETY_PCV) )
    {
        // $[TI5]
        tidalVolume = 
            PhasedInContextHandle::GetBoundedValue(SettingId::TIDAL_VOLUME).value;
        respRate = 
            PhasedInContextHandle::GetBoundedValue(SettingId::RESP_RATE).value;
    }
    else if (schedulerId == SchedulerId::APNEA)
    {
        // $[TI6]
    	tidalVolume = 
            PhasedInContextHandle::GetBoundedValue(SettingId::APNEA_TIDAL_VOLUME).value;
        respRate = 
            PhasedInContextHandle::GetBoundedValue(SettingId::APNEA_RESP_RATE).value;
    }
    // $[TI13]

    //check for setting change only where exhale minute volume is required.
    if ( schedulerId >= SchedulerId::FIRST_BREATHING_SCHEDULER &&
         schedulerId <= SchedulerId::LAST_BREATHING_SCHEDULER &&
         schedulerId != SchedulerId::SPONT )
    {

        //$[TI7]
        if (!IsEquivalent(respRate, respRate_, TENTHS))
        {
            //$[TI8]
            numRespRateRecords_ = 0;
            numTidalVolumeRecords_ = 0;
            respRate_ = respRate;
        }
	    //$[TI9]

        //If prox availability changes either way, reset the moving average window
        //every time there is a change and start a new one because we cannot combine
        //a prox and non-prox Vte in one exh minute volume calculation
        Boolean isUsingProxFlow = RProxManager.isAvailable();
        if (isUsingProxFlow_ != isUsingProxFlow)
        {
            numRespRateRecords_ = 0;
            numTidalVolumeRecords_ = 0;
            isUsingProxFlow_ = isUsingProxFlow;
        }
        if ( !IsEquivalent(tidalVolume, tidalVolume_, TENTHS) &&
             ( (BreathPhase::GetCurrentBreathPhase() == (BreathPhase*)&RVcvPhase) ||
               (BreathPhase::GetCurrentBreathPhase() == (BreathPhase*)&RApneaVcvPhase) ) )
        {
	    //$[TI10]
            numTidalVolumeRecords_ = 0;
            tidalVolume_ = tidalVolume; 
        }
        // $[TI11]
    }
    // $[TI12]

    numRespRateRecords_++;
    if (numRespRateRecords_ > NUM_PREV_BREATH_RECORDS)
    {
	//$[TI1]
        numRespRateRecords_ = NUM_PREV_BREATH_RECORDS;
    }
    // $[TI2]

    numTidalVolumeRecords_++;
    if (numTidalVolumeRecords_ > NUM_PREV_BREATH_RECORDS)
    {
	//$[TI3]
        numTidalVolumeRecords_ = NUM_PREV_BREATH_RECORDS;
    }
    // $[TI4]

}

#ifdef SIGMA_UNIT_TEST
void
BreathSet::write(void)
{
    CALL_TRACE("write(void)");
 
    Int32 ii = numRecords_;
    Int32 current = currentRecord_;

    if (ii > 100)
	ii = 100;
    cout << "===== Class BreathSet =====" << endl;
    cout << "numRecords_:" << numRecords_ << endl;
    cout << "currentRecord_:" << currentRecord_ << endl;
    cout << "numRespRateRecords_:" << numRespRateRecords_ << endl;
    cout << "numTidalVolumeRecords_:" << numTidalVolumeRecords_ << endl;
    cout << "tidalVolume_:" << tidalVolume_ << endl;
    cout << "respRate_:" << respRate_ << endl;

    if (numRecords_)
		BreathRecord::write2();

    if (numRecords_ != 100)
    {
        while (ii)
        {
            breathRecord_[current].write1();
	    cout << endl;
            current --;
            ii--;
            if ( current <  0)
                current = BREATH_HISTORY_TABLE_SIZE - 1;
        }
    }
 
}

#endif //SIGMA_UNIT_TEST

