#ifndef GuiFsm_HH
#define GuiFsm_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//====================================================================
 
//====================================================================
// Class:  GuiFsm - Sys-Init subsystem class
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Sys-Init/vcssrc/GuiFsm.hhv   25.0.4.0   19 Nov 2013 14:33:52   pvcs  $
//
//@ Modification-Log
//
//   Revision 002   By:   sah      Date: 01/13/99    DR Number: 5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to a non-inlined method.
//
//   Revision 001   By:   gdc      Date: 05/16/95    DR Number:   n/a
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//====================================================================
 
#if 0  
// for bug in genDD
#endif

//@ Usage-Classes
#include "Sys_Init.hh"
//@ End-Usage

class GuiStart;
class GuiOnline;
class GuiInop;
class GuiService;
class GuiSst;
class GuiTimeout;
class GuiOnlineInit;
class GuiServiceInit;
class GuiSstInit;
class FsmState;

class GuiFsm 
{
  public:
    
    static void  Initialize(void);

    static const GuiStart&                 GetGuiStartState(void);
    static const GuiOnline&                GetGuiOnlineState(void);
    static const GuiInop&                  GetGuiInopState(void);
    static const GuiService&               GetGuiServiceState(void);
    static const GuiSst&                   GetGuiSstState(void);
    static const GuiTimeout&               GetGuiTimeoutState(void);
    static const GuiOnlineInit&            GetGuiOnlineInitState(void);
    static const GuiServiceInit&           GetGuiServiceInitState(void);
    static const GuiSstInit&               GetGuiSstInitState(void);
 
    static void          SoftFault(const SoftFaultID  softFaultID,
				   const Uint32       lineNumber,
				   const char*        pFileName = NULL,
				   const char*        pPredicate = NULL);

  private:
    GuiFsm();                             // declared only
    ~GuiFsm();                            // declared only

    GuiFsm(const GuiFsm&);                // declared only
    void operator=(const GuiFsm&);        // declared only
 
};
 
 
#endif // GuiFsm_HH
