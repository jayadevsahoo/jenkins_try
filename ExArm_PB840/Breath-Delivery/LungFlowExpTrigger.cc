#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: LungFlowExpTrigger - Triggers exhalation based on  
//      lung flow. 
//---------------------------------------------------------------------
//@ Interface-Description - 
//      This trigger class is derived from BreathTrigger.  When trigger
//      conditions are met, the trigger is considered "fired".
//---------------------------------------------------------------------
//@ Rationale
//      It is necessary to end inspiration, and proceed to exhalation when
//      trigger condition(s) are met.
//---------------------------------------------------------------------
//@ Implementation-Description
//      Trigger condition is dependent on peak and desired flow, the greater of 
//      lung flow limit or flow threshold, and inspiration time.  
//      If the trigger is not enabled, it will always return
//      state of false.  If it is enabled and on the active list
//      in the BreathTriggerMediator, the condition monitored 
//      by the trigger will be evaluated every BD cycle.  
//      
//---------------------------------------------------------------------
//@ Fault-Handling
//    n/a.
//---------------------------------------------------------------------
//@ Restrictions
//    Only one instance of this class may be created.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/LungFlowExpTrigger.ccv   25.0.4.0   19 Nov 2013 13:59:52   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004   By:   rhj    Date: 05-March-2007      DR Number: 6359
//  Project:  RESPM
//  Description:
//        Resolve merge conflict for RM to Baseline 
//
//  Revision: 003   By: syw   Date:  20-Jul-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//		PAV project-related changes:
//		Added esens = 10 if isUsePavEsens_.
//
//  Revision: 002  By:  healey    Date:  13-Jan-1999    DR Number: 5322
//       Project:  ATC
//       Description:
//           Added enable and triggerCondition_
//           
//  Revision: 001  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//		ATC initial release.
//      Changed 'SoftFault()' method to a non-inlined method.
//
//
//=====================================================================

#include "LungFlowExpTrigger.hh"
#include "LungData.hh"
#include "BreathRecord.hh"
#include "BdDiscreteValues.hh"
#include "PhasedInContextHandle.hh"

//@ Usage-Classes

//@ End-Usage

//@ Constant
static const Real32 MAX_LEAK_LIMIT = 21.5f;
static const Real32 MIN_LEAK_LIMIT = 0.5f;

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: LungFlowExpTrigger()  
//
//@ Interface-Description
//	Default Constructor
//---------------------------------------------------------------------
//@ Implementation-Description
//		Base class constrctor is called.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

LungFlowExpTrigger::LungFlowExpTrigger(void)
 : BreathTrigger(Trigger::LUNG_FLOW_EXP)
{
	CALL_TRACE("LungFlowExpTrigger::LungFlowExpTrigger(void)") ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~LungFlowExpTrigger() 
//
//@ Interface-Description 
//	Destructor
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

LungFlowExpTrigger::~LungFlowExpTrigger(void)
{
	CALL_TRACE("LungFlowExpTrigger::~LungFlowExpTrigger(void)") ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: enable  
//
//@ Interface-Description
//      This method takes no argument and has no return value. This method
//      enables the trigger and is responsible for the trigger's
//      initialization.
//---------------------------------------------------------------------
//@ Implementation-Description
//      The state data member isEnabled_ is set to true, activating this
//      trigger.  The data members desiredFlow_ and peakDesiredFlow_
//      are initialized.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
LungFlowExpTrigger::enable(void)
{
	CALL_TRACE("LungFlowExpTrigger::enable(void)") ;
	
	isEnabled_ = TRUE ;
	
	desiredFlow_ = 200.0 ;
	peakDesiredFlow_ = 0.0 ;
	isUsePavEsens_ = FALSE ;
    isUseLpmEsens_ = FALSE ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
LungFlowExpTrigger::SoftFault(const SoftFaultID  softFaultID,
							  const Uint32       lineNumber,
							  const char*        pFileName,
							  const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, LUNGFLOWEXPTRIGGER,
                          lineNumber, pFileName, pPredicate);
}

//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: triggerCondition_
//
//@ Interface-Description
//    This method takes no parameters.  If the trigger condition has 
//    occured, the method returns true, otherwise, the method returns 
//    false.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The following condition causes the trigger to happen:
//    1. if peakDesiredFlow_ * 0.45 >= desiredFlow_ and
//    2. lung flow < MAX_VALUE(lungFlowLimit, flowThreshold and
//    3. inspiratory time is equal to or exceeds 100 mSec
// $[TC04031] declare exhalation
// $[TC04033] Calculate flowThrehold
//
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Boolean
LungFlowExpTrigger::triggerCondition_(void)
{
	CALL_TRACE("LungFlowExpTrigger::triggerCondition_(void)") ;
	
	Boolean rtnValue = FALSE ;

	Real32 exhSensitivity = PhasedInContextHandle::GetBoundedValue(SettingId::EXP_SENS).value;

	// $[PA24030] a
	// $[PA24031]

	if (isUsePavEsens_)
	{
		// $[TI9]
		const Real32 PAV_STARTUP_ESEN = 3.0 ;
		exhSensitivity = PAV_STARTUP_ESEN ;
	}
	// $[TI10]
	
	Real32 peakLungFlow = RLungData.getPeakInspLungFlow();
	Real32 lungFlowLimit = RLungData.getLungFlowLimit();
	
	Real32 flowThreshold = peakLungFlow * exhSensitivity / 100.0F;

	if (isUseLpmEsens_)
	{
        flowThreshold = exhSensitivity ;
	}

	if (flowThreshold > MAX_LEAK_LIMIT)
	{   // $[TI3]		
		flowThreshold = MAX_LEAK_LIMIT;
	}
	else if (flowThreshold < MIN_LEAK_LIMIT)
	{   // $[TI7]		
		flowThreshold = MIN_LEAK_LIMIT;
	}	// $[TI8]
	
	if (peakDesiredFlow_ * 0.45 >= desiredFlow_ &&
		RLungData.getLungFlow() < MAX_VALUE(lungFlowLimit, flowThreshold) &&
		BreathRecord::GetInspiratoryTime() >= 100)
	{		// $[TI5]
		rtnValue = TRUE;

	}

	// $[TI6] FALSE
	return( rtnValue) ;
}

//=====================================================================
//
//  Private Methods...
//
//=====================================================================
