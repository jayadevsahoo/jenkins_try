#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: ServiceStatusArea - The container which containing the test
// running status messages. This class is part of the GUI-Apps Framework
// cluster.
//---------------------------------------------------------------------
//@ Interface-Description
// This class is a container, derived from the GUI-Foundation Container class.
// It contains textual fieldss.  It displays the current test status.
// The setStatus() method stores the current test status and requests
// the status display update via updateArea_().
//---------------------------------------------------------------------
//@ Rationale
// For the Service GUI, this class is dedicated to managing the display
// of the test status.
//---------------------------------------------------------------------
//@ Implementation-Description
// The ServiceStatusArea has two textfields: serviceStatusLabel_ and
// serviceStatusValue_.  The serviceStatusLabel_ is set at construction
// time, but the serviceStatusValue_ can be set either at construction
// time or at any point when we need to update the status.
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and pre-conditions
// to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
// Only one instance of this class needs to be created (by LowerScreen).
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/ServiceStatusArea.ccv   25.0.4.0   19 Nov 2013 14:08:26   pvcs  $
//
//@ Modification-Log
//    
//  Revision: 004  By:  gdc	  Date:  03-Jun-2007  SCR Number:  6237
//  Project:  Trend
//  Description:
//      Added positioning gravity for serviceStatusValue_ and 
//      serviceStatusLabel_ as part of automated text positioning change.
//
//  Revision: 003  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 002  By:  yyy    Date:  22-JUL-97    DR Number: 2265
//    Project:  Sigma (R8027)
//    Description:
//		22-Jul-97 Changed the status value x position.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "ServiceStatusArea.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

// Initialize static constants.
const Int ServiceStatusArea::SERVICE_STATUS_MSG_X_PAD_ = 3;


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ServiceStatusArea [Default Constructor]
//
//@ Interface-Description
// Default constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
// Initializes all members and initializes the colors, sizes, and
// positions of all graphical objects.  Adds all graphical objects to
// the parent container for display.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

ServiceStatusArea::ServiceStatusArea(Int containerX, Int containerY,
					Int containerWidth, Int containerHeight,
					StringId serviceStatusLabelId,
					StringId serviceStatusValueId) :
	serviceStatusLabelId_(serviceStatusLabelId),
	serviceStatusValueId_(serviceStatusValueId),
	containerX_(containerX),
	containerY_(containerY),
	containerWidth_(containerWidth),
	containerHeight_(containerHeight)
{
	CALL_TRACE("ServiceStatusArea::ServiceStatusArea(void)");

	// Size and position the area
	setX(containerX_);
	setY(containerY_);
	setWidth(containerWidth_);
	setHeight(containerHeight_);

	// Default background color is black (this is the background color
	// of the Status Area line, the button container will overlay the rest
	// of the Main Settings Area with an Extra Dark Grey background).
	// All text within should be colored white.
	setFillColor(Colors::BLACK);			// Background black
	setContentsColor(Colors::WHITE);		// Contents white

	//
	// Initialize the text items
	//
	serviceStatusLabel_.setText(serviceStatusLabelId_);
	serviceStatusValue_.setText(serviceStatusValueId_);

	// Add status line text objects to main container
	addDrawable(&serviceStatusLabel_);
	addDrawable(&serviceStatusValue_);

	// vertically center the label
	serviceStatusLabel_.positionInContainer(::GRAVITY_VERTICAL_CENTER);

	//
	// Update Status Area
	// Auto-size X coordinate for serviceStatusValue_ based on the bounding
	// values of the ServiceStatusArea.
	//
	updateArea_();
							// $[TI1]	
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~ServiceStatusArea  [Destructor]
//
//@ Interface-Description
// Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  N/A
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

ServiceStatusArea::~ServiceStatusArea(void)
{
	CALL_TRACE("ServiceStatusArea::~ServiceStatusArea(void)");

	// Do nothing.
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setStatus
//
//@ Interface-Description
// Sets the status to be displayed for a given stringId.
//---------------------------------------------------------------------
//@ Implementation-Description
// The string is simply stored in serviceStatusValueId_.  Then we update
// the display to reflect the new status by calling updateArea_().
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ServiceStatusArea::setStatus(StringId stringId)
{
	CALL_TRACE("ServiceStatusArea::setStatus(StringId stringId)");

	serviceStatusValueId_ = stringId;

	// Update Status Area
	updateArea_();
							// $[TI1]
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateArea_
//
//@ Interface-Description
// Needs no parameters.  Called when test status changes.  It updates the
// status Area to reflect the current status.
//---------------------------------------------------------------------
//@ Implementation-Description
// Displays serviceStatusValueId_ if it is not the default value.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ServiceStatusArea::updateArea_(void)
{
	CALL_TRACE("ServiceStatusArea::updateArea_(void)");

	if (serviceStatusValueId_ != NULL_STRING_ID)
	{													// $[TI1]
		//
		// Auto-size X coordinate for serviceStatusValue_ based on the bounding
		// values of the ServiceStatusArea.
		//
		serviceStatusValue_.setText(serviceStatusValueId_);
		serviceStatusValue_.setX(SERVICE_STATUS_MSG_X_PAD_ * 2 + 
								serviceStatusLabel_.getWidth());
		serviceStatusValue_.positionInContainer(::GRAVITY_VERTICAL_CENTER);
		serviceStatusValue_.setShow(TRUE);
	}
	else
	{													// $[TI2]
		serviceStatusValue_.setShow(FALSE);
	}
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
ServiceStatusArea::SoftFault(const SoftFaultID  softFaultID,
		   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS,
									SERVICESTATUSAREA,
									lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)
