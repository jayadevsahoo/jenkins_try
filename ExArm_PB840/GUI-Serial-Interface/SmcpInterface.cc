#include "stdafx.h"
//====================================================================
// This is a proprietary work to which Nellcor Puritan Bennett
// corporation of California claims exclusive right.  No part of this
// work may be used, disclosed, reproduced, sorted in an information
// retrieval system, or transmitted by any means, electronic,
// mechanical, photocopying, recording, or otherwise without the prior
// written permission of Nellcor Puritan Bennett Corporation.
//
//     Copyright (c) 1997, Nellcor Puritan Bennett Corporation
//=====================================================================

// =========================== M O D U L E   D E S C R I P T I O N ====
//@ Filename: SmcpInterface - Protocol Driver for the serial Service Mode
//			      Communication Protocol (SMCP).
//---------------------------------------------------------------------
//@ Interface-Description
//  Provides the Service Mode Communications Protocol (SMCP) driver for
//  the 840 ventilator. This class contains the methods that receive
//  and decode incoming commands from the host over the serial port. It
//  also contains the interface for outputing data to the SMCP host
//  through the serial port. Once activated by calling the driver_()
//  method, this class receives and processes SMCP commands from the
//  serial port and receives and sends responses to the serial port
//  indefinately. The interface to the application task is provided
//  through the SerialInterface::Read and SerialInterface::Write
//  methods which invoke the SmcpInterface class methods read() and
//  write().  The setSerialParameters_() method sets the serial port
//  parameters to the current settings in NOVRAM. Commands received
//  from the host over the serial port are queued in the
//  SmcpRequestBuffer and an intertask message sent to wake up the
//  pending application.  Outgoing messages from the application task
//  are queued in the SmcpResponseBuffer and an intertask message sent 
//  to the GUI-Serial-Interface task.
//---------------------------------------------------------------------
//@ Rationale
//  Implements the Service-Mode Communication Protocol.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The SerialInterface::Task instantiates this class when the
//  ventilator transitions to the SERVICE state. It then calls the 
//  driver_() method to initiate the SMCP protocol on the serial port.
//  The driver alternately looks at its task queue and the serial port
//  processing incoming commands and transmitting outgoing data to the
//  host.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness of
//  parameterized data. Incoming commands are verified for correctness
//  and a negative acknowledgement is returned for commands in error.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
// none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Serial-Interface/vcssrc/SmcpInterface.ccv   25.0.4.0   19 Nov 2013 14:12:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 008    By: gdc        Date: 13-Jan-2009  SCR Number: 5944
//  Project: 840S
//  Description:
//  	Modified to allow changes to service mode baud rate without
//  	requiring power cycle.
//
//  Revision: 007    By: erm           Date: 23-Jan-2002  DR Number: 5984
//  Project: GuiComms
//  Description:
//   Added External Loopback functionality
//
//  Revision: 006    By: hct           Date: 22-OCT-1999  DR Number: 5493
//  Project: GuiComms
//  Description:
//      Add GuiComms functionality.
//
//  Revision: 005    By: Gary Cederquist  Date: 11-MAR-1998  DR Number: 2750
//    Project: Sigma (R8027)
//    Description:
//       Changed to boost task priority during loopback test to 
//       eliminate interference from GUI screen updates which caused
//       I/O timeouts on the serial port. Reduced the loopback I/O
//       timeouts to 10-20ms.
//
//  Revision: 004    By: Gary Cederquist  Date: 11-DEC-1997  DR Number: 2677
//    Project: Sigma (R8027)
//    Description:
//       Flush serial port prior to loopback test. Extend transmit
//       receive loopback timeouts to help alleviate serial port test
//       failures.
//
//  Revision: 003    By: Gary Cederquist  Date: 24-Nov-1997  DR Number: 2605
//    Project: Sigma (R8027)
//    Description:
//       Complete restructuring of GUI-Serial-Interface to fix
//       several problems.
//
//  Revision: 002  By: gdc   Date: 01-Jul-1997    DR Number: 2232
//      Project:  Sigma (840)
//      Description:
//          Yield CPU to other lowest priority tasks periodically
//          to cooperatively multitask.
//
//  Revision: 1.X	By: jdm	Date: X/XX/96	DR Number: 
//	Project:  Sigma (R8027)
//	Description:
//		Initial version to coding standards (Integration baseline).
//=====================================================================

#include "Sigma.hh"
#include "SmcpInterface.hh"
#include "SerialPort.hh"

//@ Usage-Classes
#include "BatchSettingValues.hh"
#include "BaudRateValue.hh"
#include "CrcUtilities.hh"
#include "UserAnnunciationMsg.hh"
#include "LaptopEventId.hh"
#include "MailBox.hh"
#include "MsgQueue.hh"
#include "NovRamManager.hh"
#include "SettingsMgr.hh"
#include "Setting.hh"
#include "Task.hh"
#include "SmcpPacket.hh"
#include "SmcpRequestBuffer.hh"
#include "SmcpResponseBuffer.hh"

//@ End-Usage

//@ Code...

// byte isolation macros
#define HI_BYTE(word)  ((word >> 8) & 0xFF)
#define LO_BYTE(word)  (word & 0xFF)

// standard ASCII acknowledgement characters
static const Byte ACK = 0x06;		// positive
static const Byte NAK = 0x15;		// negative

static const Uint MAX_SMCP_RETRIES        = 7;
static const Uint MIN_SMCP_REQUEST_SIZE   = 3;
static const Uint MAX_SMCP_REQUEST_SIZE   = 128;
static const Uint STACKWARE_PRIORITY_     = 10;


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SmcpInterface [constructor]
//
//@ Interface-Description
//  Construct the SmcpInterface and associated classes including the 
//  SmcpRequestBuffer and SmcpResponseBuffer as well as MsgQueues for
//  the normal-mode and loopback-mode applications.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Instantiate the serial input/output SerialBuffers. The SmcpRequest
//  Buffer is a SerialBuffer for transferring data to the application
//  received on the serial port. The SmcpResponseBuffer is a SerialBuffer
//  for transferring data from the application to the SerialInterface::Task
//  destined for output to the host via the serial port.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SmcpInterface::SmcpInterface( SerialPort & rSerialPort,
							  SerialInterface::PortNum portNum,
							  IpcId smcpQueueId) 
:   SerialInterface( rSerialPort, portNum )
, rApplicationQueue_( USER_ANNUNCIATION_Q )
, rLoopbackQueue_( SERVICE_MODE_GUIIO_Q )
, rSerialInterfaceQueue_( smcpQueueId )
, rSmcpRequestBuffer_()
, rSmcpResponseBuffer_()
, pConsumerQueue_( &rApplicationQueue_ )
, loopbackEnabled_( FALSE )
{
	// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~SmcpInterface [destructor]
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SmcpInterface::~SmcpInterface()
{
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: driver_
//
//@ Interface-Description
//  Sets the serial port parameters and then the process loop
//  alternately processes messages from the task queue and serial port.
//  Data is not processed (in this process loop) from the serial port 
//  while in loopback mode.
//---------------------------------------------------------------------
//@ Implementation-Description
//  
//---------------------------------------------------------------------
//@ PreCondition
// Vent must be in Service Mode
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void SmcpInterface::driver_(void)
{
	areSettingsChanged_ = FALSE;
	attachToSubjects_();
	setSerialParameters_();
	purgeline();

	while ( !areSettingsChanged_ )
	{
		//$[TI1]
		processQueue_();

		if ( !loopbackEnabled_ )
		{
			//$[TI2.1]
			processRequests_();
		}
		//$[TI2.2]

		Task::Yield();    
	}
	detachFromSubjects_();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processQueue_
//
//@ Interface-Description
//  While a message is available on the serial interface task mailbox,
//  dequeue it and process it accordingly. The intertask message 
//  indicates data is available for output to the serial port.
//  If loopback is enabled, the loopBack_() method processes the 
//  outgoing data, otherwise the outputSerialData_() method formats
//  and sends the data to the host. Returns when no more messages are
//  queued.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void SmcpInterface::processQueue_(void)
{
	Int32 msg;

	while ( rSerialInterfaceQueue_.isMsgAvail() )
	{
		// $[TI1.1]
		rSerialInterfaceQueue_.acceptMsg( msg );

		switch ( msg )
		{
			case SERIAL_OUTPUT_AVAILABLE:  // $[TI2.1]
				if ( !loopbackEnabled_ )
				{
					// $[TI3.1]
					outputSerialData_();
				}
				else
				{
					// $[TI3.2]
					loopBack_();
				}
				break;

			default:
				AUX_CLASS_ASSERTION_FAILURE( msg );
		}
	}
	// $[TI1.2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processRequests_
//
//@ Interface-Description
//  Polls the serial port for an SMCP packet.  If the port is not
//  empty, read the packet and verify its length and CRC. If the packet
//  is OK then send an acknowledgement (ACK) to the host, otherwise
//  flush the input buffer and send a negative acknowledgement (NAK) to
//  the host.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The packet format is:
//
//  +---------------------------------------------------+
//  | Length     |   Data                   | CRC       |
//  | (2 bytes)  |   (Length-2 bytes)       | (2 bytes) |
//  +---------------------------------------------------+
//
//  The CRC is a 16-bit additive checksum of the length and data fields.
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void SmcpInterface::processRequests_(void)
{
	Uint16   packetLength = 0;

	//  read the first byte of the packet length (if available)
	Uint bytesRead = rSerialPort_.read( &packetLength, 1 );

	if ( isSerialPortActivated_ && bytesRead )
	{
		//$[TI1.1]
		SmcpPacket  pkt;
		Int32       msgLen;
		Boolean     goodPacket = FALSE;
		Uint16      crc;

		//  read the second byte of the packet length 
		bytesRead = rSerialPort_.read( ((Byte*)&packetLength)+1, 1, 100 );

		if ( bytesRead
			 && packetLength >= MIN_SMCP_REQUEST_SIZE
			 && packetLength <= MAX_SMCP_REQUEST_SIZE )
		{
			//$[TI2.1]
			pkt.length = packetLength;

			//  fetch remaining message bytes EXCEPT for appended CRC
			//  which is read separately
			msgLen = pkt.length - sizeof(crc);

			if ( rSerialPort_.read( &pkt.smcpMessage.number, msgLen, 100 )
				 == msgLen )
			{
				//$[TI3.1]
				// read and verify the CRC

				if ( (   rSerialPort_.read(  &crc, sizeof(crc), 100 )
						 == sizeof(crc) )

					 && (   VerifyCrc( (const Byte *) &pkt.length
									   , (size_t)pkt.length
									   , (CrcValue) crc ) 
							== SUCCESS) )
				{	//$[TI4.1]
					goodPacket = TRUE;
				}
				else
				{	//$[TI4.2]
					// bad or incomplete CRC
				}
			}
			else
			{
				//$[TI3.2]
				// incomplete message
			}
		}
		else
		{
			//$[TI2.2]
			// bad or incomplete packet length
		}

		if ( goodPacket )
		{
			// $[TI5.1]	
			// input was good, so ACK and pass it up
			ackPacket_();
			rSmcpRequestBuffer_.write( &pkt.smcpMessage, msgLen );

			// inform Service-GUI of new request
			postConsumer( UserAnnunciationMsg::SERIAL_REQUEST_EVENT );
		}
		else
		{
			// $[TI5.2]
			//  something wrong, so NAK it
			//  flush remaining data on serial port and NAK packet
			purgeline();
			nakPacket_();
		}
	}
	//$[TI1.2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ackPacket_
//
//@ Interface-Description
//  Acknowledge receipt of a good SMCP packet.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Sends the ACK character to the serial port transmitter and waits
//  up to one second for the the character to be transmitted. No action
//  is taken if the ACK cannot be transmitted since the interface will
//  be resynched on the next sucessful transmission.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void SmcpInterface::ackPacket_(void)
{
	rSerialPort_.write( &ACK, sizeof(ACK), 1000 ); 
	rSerialPort_.wait( 1000 );
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: nakPacket_
//
//@ Interface-Description
//  Send a negative acknowledgement of a bad SMCP packet to the host
//  and wait for a return NAK. Attempt to send the NAK up to three times
//  without acknowledgement before giving up and returning to the caller.
//  Waits up to one second on each read waiting for acknowledgment.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void SmcpInterface::nakPacket_(void)
{
	for ( Uint i=0; i<3; i++ )
	{
		//$[TI1.1]
		sendNak_();

		// wait for a return NAK
		char ch = 0;
		rSerialPort_.read( &ch, sizeof(ch), 1000 );

		if ( ch == NAK )
		{
			//$[TI2.1]
			break;
		}
		//$[TI2.2]
	}                   
	//$[TI1.2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: sendNak_
//
//@ Interface-Description
//  Send a NAK to the host. Waits up to one seconds for the transmission
//  to occur. Takes no corrective action if NAK cannot be transmitted
//  since the interface is resyched elsewhere.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
	SmcpInterface::sendNak_(void)
{
	// $[TI1]
	rSerialPort_.write( &NAK, sizeof(NAK), 1000 ); 
	rSerialPort_.wait( 1000 );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: outputSerialData_
//
//@ Interface-Description
//  Formats SMCP messages that were generated by GUI-Applications and
//  sends them to the host. Reads an SMCP message from the
//  SmcpResponseBuffer prepends a packet length, generates and appends
//  a CRC, and sends the packet to the host via the serial port. Waits
//  for acknowledgement from the host and retransmits the packet
//  MAX_SMCP_RETRIES times before giving up, reporting a transmission
//  error and returning. The time between retries is one second based
//  on the timeout waiting for packet acknowledgement. Returns when
//  there are no more messages in the SmcpResponseBuffer.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
	SmcpInterface::outputSerialData_(void)
{
	Uint16      crc;
	Boolean     failed = FALSE;
	SmcpPacket  smcpPacket;

	Uint8       failureId;

	for ( ; ; )
	{
		// $[TI1.1]
		Uint32 length = rSmcpResponseBuffer_.read( &smcpPacket.smcpMessage
												   , sizeof(SmcpMessage) );

		if ( !length )
		{
			// $[TI2.1]
			//  no more output - stop reading response buffer
			break;
		}
		// $[TI2.2]

		smcpPacket.length   = length + sizeof(crc);

		// and compute the CRC on the packet and append it
		crc = CalculateCrc((Byte *)&smcpPacket, (size_t)smcpPacket.length);

		// stuff it into the packet
		smcpPacket.smcpMessage.data[smcpPacket.length-4] = HI_BYTE(crc);
		smcpPacket.smcpMessage.data[smcpPacket.length-4+1] = LO_BYTE(crc);

		// output packet size is the packet length plus the size of the
		// specified packet length (ie: storage for packet length is not
		// included in the packet length)
		length = smcpPacket.length + sizeof(smcpPacket.length);

		for ( Int32 ii = 0; ii < MAX_SMCP_RETRIES; ii++ )
		{
			// $[TI3.1]
			Uint bytesOut = rSerialPort_.write( &smcpPacket, length, 1000 );  
			Boolean timedOut = rSerialPort_.wait( 1000 );

			if ( bytesOut != length || timedOut )
			{
				// $[TI4.1]
				failureId = SERIAL_FAILURE_TOUT;
				failed = TRUE;
				break;
			}
			// $[TI4.2]

			char ch = 0;
			rSerialPort_.read( &ch, 1, 1000 );	// wait for acknowledgement

			if ( ch == ACK )
			{
				// $[TI5.1]
				failed = FALSE;
				break;
			}
			// $[TI5.2]

			if ( ch == NAK )
			{
				// $[TI6.1]
				failed = TRUE;
				failureId = SERIAL_FAILURE_NAK;

				// acknowledge any NAKs we see and then resend
				sendNak_();
			}
			else
			{
				// $[TI6.2]
				// - the host is probably attempting to send us a message
				// - the interface is out of sync but we'll resend
				//   the message anyway in an attempt to resynchronize
			}
		}
		// $[TI3.2]
	}
	// $[TI1.2]

	//  inform GUI-Application of problems only once instead of flooding queue
	if ( failed )
	{
		// $[TI7.1]
		// inform GUI-Application of interface failure
		// and why:
		//	if at least 1 NAK was seen,
		//		assume the laptop is connected
		//		else they might not be there at all
		postConsumer( UserAnnunciationMsg::SERIAL_FAILURE_EVENT, failureId );
	}
	// $[TI7.2]

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: loopBack_
//
//@ Interface-Description
//  Reads one message which was generated by the Service-Mode subsystem
//  from the SmcpResponseBuffer. Sends this data over the serial port
//  which has been previously enabled in loopback mode and reads each
//  loopback byte from the serial port into a buffer in the 
//  SmcpRequestBuffer. Upon reading or timing out on the loopback read,
//  posts an intertask message to the Service-Mode task to indicate
//  data is available in the SmcpRequestBuffer.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
	SmcpInterface::loopBack_(void)
{
	Uint8   outputData[MAX_SMCP_RESPONSE_SIZE];
	Uint8   inputData[MAX_SMCP_RESPONSE_SIZE];

	purgeline();

	// fetch the output...
	Uint byteCount 
		= rSmcpResponseBuffer_.read( outputData, sizeof(outputData) );

	Uint totalBytesRead = 0;

	Int32 basePriority = Task::GetTaskBaseInfo().GetBasePriority();

	// increase tasking priority to maintain CPU control and assure I/O timing
	Task::SetPriority(STACKWARE_PRIORITY_ + 1);

	for ( Uint i=0; i<byteCount; i++ )
	{
		// $[TI1.1]
		rSerialPort_.write( &outputData[i], 1, 20 );
		rSerialPort_.wait( 20 );

		Uint bytesRead = rSerialPort_.read( &inputData[i], 1, 20/*ms*/ );

		totalBytesRead += bytesRead;

		if ( !bytesRead )
		{
			// $[TI2.1]
			break;
		}
		// $[TI2.2]
	}
	// $[TI1.2]

	// restore base priority
	Task::SetPriority(basePriority);

	rSmcpRequestBuffer_.write( inputData, totalBytesRead );

	postConsumer( UserAnnunciationMsg::SERIAL_REQUEST_EVENT );
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: attachToSubjects_ [protected, virtual]
//
//@ Interface-Description
//  Setup the Settings-Validation observer/subject system so the serial port
//  can update their communications parameters (ie. baud rate) when a user 
//  makes an associated setting change.
//---------------------------------------------------------------------
//@ Implementation-Description
//  For the SMCP interface, only the service mode buad rate setting is
//  configurable.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void SmcpInterface::attachToSubjects_(void)
{
	attachToSubject_(SettingId::SERVICE_BAUD_RATE, Notification::VALUE_CHANGED);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: detachFromSubjects_
//
//@ Interface-Description
//  Cancel the observer/subject relationship for the service mode 
//  serial port baud rate setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calling the Settings-Validation method detachFromSubject_ cancels the
//  observer/subject relationship set up in the attachToSubjects_ call.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void SmcpInterface::detachFromSubjects_(void)
{
	detachFromSubject_(SettingId::SERVICE_BAUD_RATE, Notification::VALUE_CHANGED);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setSerialParameters_ [protected, virtual]
//
//@ Interface-Description
//  Retrieves the service mode baud rate setting from NOVRAM and sets
//  the serial port parameters accordingly. The number of data bits
//  is fixed at 8 bits, the number of stop bits is fixed at 1.
//
//  $[07047] The service baud rate shall be used in communicating with
//  the external computer.
//  $[07048] On entry to the External Control mode, the Service Mode GUI
//  software shall change the baud rate of the GUI serial port to the
//  service baud rate. The other communications setup parameters shall
//  be set to the following values:
//    \a\ Parity shall be set to "none".
//    \b\ Data Bits shall be set to 8.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void SmcpInterface::setSerialParameters_(void)
{
	static const Uint   SMCP_DATA_BITS = 8;
	static const Uint   SMCP_STOP_BITS = 1;
	Uint                baudRate;

	const Setting*  pSetting = SettingsMgr::GetSettingPtr(SettingId::SERVICE_BAUD_RATE);
	const DiscreteValue  baudSetting = pSetting->getAcceptedValue();

	switch ( baudSetting )
	{
		case BaudRateValue::BAUD_19200_VALUE: // $[TI1.1]
			baudRate = 19200;
			break;
		case BaudRateValue::BAUD_9600_VALUE:  // $[TI1.3]
			baudRate = 9600;
			break;
		case BaudRateValue::BAUD_4800_VALUE:  // $[TI1.4]
			baudRate = 4800;
			break;
		case BaudRateValue::BAUD_2400_VALUE:  // $[TI1.5]
			baudRate = 2400;
			break;
		case BaudRateValue::BAUD_1200_VALUE:  // $[TI1.6]
			baudRate = 1200;
			break;
		default:
			AUX_CLASS_ASSERTION_FAILURE( baudSetting );
	}

	rSerialPort_.setParameters(  baudRate
								 , SMCP_DATA_BITS
								 , SerialPort::NONE
								 , SMCP_STOP_BITS );
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: enableLoopback
//
//@ Interface-Description
//  Enables loopback mode for the SMCP interface. Redirects the 
//  consumer queue to the loopback queue and enables loopback mode on
//  the serial port hardware.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void SmcpInterface::enableLoopback()
{
	loopbackEnabled_ = TRUE;

	pConsumerQueue_ = &rLoopbackQueue_;

	rSerialPort_.enableLoopback();

	// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: disableLoopback
//
//@ Interface-Description
//  Disables loopback mode for the SMCP interface. Redirects the 
//  consumer queue to the normal application queue and disables loopback 
//  mode on the serial port hardware.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void SmcpInterface::disableLoopback()
{
	loopbackEnabled_ = FALSE;

	rSerialPort_.reset();

	pConsumerQueue_ = &rApplicationQueue_;

	rSerialPort_.disableLoopback();

	// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: enableExternalLoopback
//
//@ Interface-Description
//  Enables external loopback mode for the SMCP interface.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void SmcpInterface::enableExternalLoopback()
{
	loopbackEnabled_ = TRUE;
	// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: disableExternalLoopback
//
//@ Interface-Description
//  Disables external loopback mode for the SMCP interface.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void SmcpInterface::disableExternalLoopback()
{
	loopbackEnabled_ = FALSE;

	rSerialPort_.reset();
	// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: read
//
//@ Interface-Description
//  Transfers the contents of the first message in the SmcpRequestBuffer 
//  FIFO to the callers local buffer up to the number of bytes specified.
//  Returns the actual number of bytes transferred.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
Uint SmcpInterface::read( void * pBuffer, Uint count, Uint milliseconds )
{
	// $[TI1]
	return rSmcpRequestBuffer_.read( pBuffer, count, milliseconds );
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: write
//
//@ Interface-Description
//  Transfers the contents of the caller's buffer to a message in the 
//  SmcpResponseBuffer. Posts the GUI-Serial-Interface task to indicate
//  a message is available for output to the serial port.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
Uint SmcpInterface::write( const void * pBuffer, Uint count, Uint milliseconds )
{
	//  post a message first which works because SerialInterface task
	//  is always a lower priority task than the supplier task

	rSerialInterfaceQueue_.putMsg( SERIAL_OUTPUT_AVAILABLE );

	// $[TI1]
	return rSmcpResponseBuffer_.write( pBuffer, count, milliseconds );
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: postConsumer
//
//@ Interface-Description
//  Sends an intertask message to the current consumer task for 
//  SMCP Request Messages.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void SmcpInterface::postConsumer(  UserAnnunciationMsg::UAMEventType event
								   , Uint8 failure ) const
{
	CLASS_ASSERTION( pConsumerQueue_ != NULL );

	UserAnnunciationMsg  message;

	message.qWord = 0;
	message.serialRequestParts.eventType = event;
	message.serialRequestParts.failureId = failure;
	message.serialRequestParts.portNum = portNum_;

	pConsumerQueue_->putMsg( message.qWord );

	// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//@ Interface-Description
//  Implicitly gain execution control when a software assertion macro
//  detects and error.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Automatically invoked when CLASS_ASSERTION and CLASS_PRE_CONDITON
//  macros (among others) detect a fault.  We just call FaultHandler::SoftFault
//  to display pertinent information to the user.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

void    SmcpInterface::SoftFault(  const SoftFaultID softFaultID
								   , const Uint32      lineNumber
								   , const char *      pFileName
								   , const char *      pPredicate )
{
	FaultHandler::SoftFault(softFaultID, GUI_SERIAL_INTERFACE,
							GUI_Serial_Interface::SMCP_INTERFACE_CLASS, 
							lineNumber, pFileName, pPredicate);
}
