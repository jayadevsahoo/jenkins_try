#ifndef Task_HH
#define Task_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: Task - Task creation and control
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/OS-Foundation/vcssrc/Task.hhv   25.0.4.0   19 Nov 2013 14:15:18   pvcs  $
//
//@ Modification-Log
//
//  Revision: 008   By:   rhj   Date: 23-Feb-2010   SCR Number: 6436
//  Project:  PROX
//  Description:
//      Added ProxiInterface class.
//
//  Revision: 007    By: hct              Date: 17-JUN-1999  DR Number: 5493
//    Project: GUIComms
//    Description:
//       GUIComms baseline.
//
//  Revision: 006    By: Gary Cederquist  Date: 17-JUN-1999  DR Number: 5422
//    Project: Sigma (R8027)
//    Description:
//       Added UserClock class as friend class since it must change its
//       tasking priority.
//
//  Revision: 005   By: sah   Date: 13-Jan-1999  DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//
//  Revision: 004    By: Gary Cederquist  Date: 11-MAR-1998  DR Number: 2750
//    Project: Sigma (R8027)
//    Description:
//       Added SmcpInterface class as friend class since it must change its
//       tasking priority.
//
//  Revision: 003    By: Gary Cederquist  Date: 30-Sep-1997  DR Number: 2451
//    Project: Sigma (R8027)
//    Description:
//       Added ServiceMode class as friend class since it must change its
//       tasking priority.
//
//  Revision: 002    By: Gary Cederquist  Date: 15-APR-1997  DR Number: 1943
//    Project: Sigma (R8027)
//    Description:
//       Added revision headers.
//
//  Revision: 001    By: Greg Hamm        Date: 01-OCT-1996  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//====================================================================

#include "Sigma.hh"

#ifdef SIGMA_DEVELOPMENT
#  include "Ostream.hh"
#endif // SIGMA_DEVELOPMENT

//@ Usage-Classes
#include "TaskInfo.hh"
//@ End-Usage


// The following is needed for the PC build.
// Without it, the old Yield() function/macro used in old windows
// programs will conflict the member function below.
#if defined(WIN32)
#undef Yield
#endif

class Task
{
  public:

#ifdef SIGMA_DEVELOPMENT
    static void					Print(void);
#endif // SIGMA_DEVELOPMENT

	//wrapper APIs for OS thread calls. The contents get ported to the used OS
    static Uint32				GetId(void);
    static Int32				GetPriority(void);
    static void					SetPriority(const Int32 priority);
	static void					Exit(const TaskState exitState=::TASK_EXITED);
    static void					Delay(const Uint32 sec, const Uint32 mils=0);
    static void					Yield(void);
    
	//general APIs, not thread function wrappers
	static inline Boolean		IsTaskingOn(void);
	static inline const TaskInfo&  GetTaskBaseInfo(void);

    static void SoftFault(const SoftFaultID softFaultID,
                                 const Uint32      lineNumber,
                                 const char*       pFileName  = NULL,
                                 const char*       pPredicate = NULL);

  protected:

  private:
    Task(void);                         // not implemented...
    ~Task(void);                        // not implemented...

};

#include "Task.in"

#endif // Task_HH
