//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @DownloadSWDlg.cpp 
//----------------------------------------------------------------------------


#include "stdafx.h"
#include "Swat.h"
#include "DownloadSWDlg.h"
#include "Pipe.h"

#define SW_DOWNLOAD_PROGRESS_TIMER_ID		1111
#define BD		1
#define GUI		0


// CDownloadSWDlg dialog

IMPLEMENT_DYNAMIC(CDownloadSWDlg, CDialog)

CDownloadSWDlg::CDownloadSWDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CDownloadSWDlg::IDD, pParent)
	, m_strBD_IP(_T(""))
	, m_strGUI_IP(_T(""))
	, m_strBD_log(_T(""))
	, m_strGUI_log(_T(""))
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	m_strBD_IP = pApp->m_strBD_JTAG_IPAddr;
	m_strGUI_IP = pApp->m_strGUI_JTAG_IPAddr;

}

CDownloadSWDlg::~CDownloadSWDlg()
{
}

void CDownloadSWDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PROGRESS_DWLD_BD, m_Progress_bar_BD);
	DDX_Text(pDX, IDC_EDIT_BD_IP, m_strBD_IP);
	DDX_Text(pDX, IDC_EDIT_GUI_IP, m_strGUI_IP);
	DDX_Control(pDX, IDC_PROGRESS_DWLD_GUI, m_Progress_bar_GUI);
	DDX_Text(pDX, IDC_EDIT_BD_LOG, m_strBD_log);
	DDX_Text(pDX, IDC_EDIT_GUI_LOG, m_strGUI_log);
}


BEGIN_MESSAGE_MAP(CDownloadSWDlg, CDialog)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON_BROWSE_BD, &CDownloadSWDlg::OnBnClickedButtonBrowseBd)
	ON_BN_CLICKED(IDC_BUTTON_BROWSE_GUI, &CDownloadSWDlg::OnBnClickedButtonBrowseGui)
	ON_BN_CLICKED(IDC_BUTTON_DWN_SW, &CDownloadSWDlg::OnBnClickedButtonDwnSw)
	ON_BN_CLICKED(IDC_DWN_BD, &CDownloadSWDlg::OnBnClickedDwnBd)
	ON_BN_CLICKED(IDC_DWN_GUI, &CDownloadSWDlg::OnBnClickedDwnGui)
	ON_BN_CLICKED(IDC_BD_SHELL, &CDownloadSWDlg::OnBnClickedBdShell)
	ON_BN_CLICKED(IDC_BUTTON_DWN_SW1, &CDownloadSWDlg::OnBnClickedButtonDwnSw1)
	ON_BN_CLICKED(IDC_LOAD_LAST_BD, &CDownloadSWDlg::OnBnClickedLoadLastBd)
	ON_BN_CLICKED(IDC_LOAD_LAST_GUI, &CDownloadSWDlg::OnBnClickedLoadLastGui)
	ON_BN_CLICKED(IDCANCEL, &CDownloadSWDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_BUTTON_NOVRAM, &CDownloadSWDlg::OnBnClickedButtonNovram)
	ON_BN_CLICKED(IDC_BUTTON_BURN_BD, &CDownloadSWDlg::OnBnClickedButtonBurnBd)
	ON_BN_CLICKED(IDC_BUTTON_BURN_GUI, &CDownloadSWDlg::OnBnClickedButtonBurnGui)
END_MESSAGE_MAP()









bool CDownloadSWDlg::CreatePowerPCFile(CString strTempFile, CString strLoadFile, CString strIPAdd)
{
	strLoadFile.Replace("\\", "\\\\");

	CString strTemp;
	
	CStdioFile file( strTempFile, CFile::modeCreate | CFile::modeWrite | CFile::typeText );
	

		
	strTemp.Format("target remote %s \r\n", strIPAdd);
	file.WriteString(strTemp);

	file.WriteString("mon reset \r\n");

	strTemp.Format("file %s \r\n", strLoadFile);
	file.WriteString(strTemp);

	strTemp.Format("load \r\n");
	file.WriteString(strTemp);

	strTemp.Format("C \r\n");
	file.WriteString(strTemp);

//	strTemp.Format("q \r\n");
//	file.WriteString(strTemp);

	file.Close();
	return true;
}



void CDownloadSWDlg::OnBnClickedButtonBrowseBd()
{
	UpdateData(1);
	CFileDialog dlg(TRUE, NULL,NULL,0, "BD Load (elf)|*.elf|", NULL);
	if(dlg.DoModal()==IDCANCEL)		
		return;

	if(dlg.GetFileName().Find("Bd") == -1)
	{
		AfxMessageBox("Doesn't look BD elf file. Continue at your own risk !");
	}
		
	GetDlgItem(IDC_STATIC_BD_PATH)->SetWindowTextA(dlg.GetPathName());
	//GetDlgItem(IDC_BUTTON_DWN_BD)->EnableWindow(0);
	
	m_Progress_bar_BD.SetRange(0, 500);
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	pApp->m_nBD_SW_DownloadStatus = 0;
	
}

void CDownloadSWDlg::OnBnClickedButtonBrowseGui()
{

	UpdateData(1);
	CFileDialog dlg(TRUE, NULL,NULL,0, "GUI Load (elf)|*.elf|", NULL);
	if(dlg.DoModal()==IDCANCEL)		
		return;

	if(dlg.GetFileName().Find("Gui") == -1)
	{
		AfxMessageBox("Doesn't look GUI elf file. Continue at your own risk !");
	}

	if(dlg.GetFileName().Find("Viking") == -1)
	{
		AfxMessageBox("NOT a VIKING gui image.");
	}

	GetDlgItem(IDC_STATIC_GUI_PATH)->SetWindowTextA(dlg.GetPathName());
	//GetDlgItem(IDC_BUTTON_DWN_BD)->EnableWindow(0);
	

	m_Progress_bar_GUI.SetRange(0, 600);
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	pApp->m_nGUI_SW_DownloadStatus = 0;
	

}

void CDownloadSWDlg::OnBnClickedButtonDwnSw()
{

	CString strGUIPath, strBDPath;
	GetDlgItem(IDC_STATIC_GUI_PATH)->GetWindowTextA(strGUIPath);
	GetDlgItem(IDC_STATIC_BD_PATH)->GetWindowTextA(strBDPath);
	if(strBDPath.IsEmpty())
	{
		AfxMessageBox("BD elf file path required");
		return;
	}
	if(strGUIPath.IsEmpty())
	{
		AfxMessageBox("GUI elf file path required");
		return;
	}


	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	pApp->m_nBD_SW_DownloadStatus =0;
	pApp->m_nGUI_SW_DownloadStatus =0;


	CString strCopyCmd;
	strCopyCmd.Format("copy %s %s",strBDPath, TEMP_BD_ELF_FILE_PATH_NAME);
	system(strCopyCmd);

	
	AfxBeginThread((AFX_THREADPROC)DownloadThread, (LPVOID)BD); //start only BD

}

void CDownloadSWDlg::OnTimer(UINT_PTR nIDEvent)
{

	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	m_Progress_bar_BD.SetPos(pApp->m_nBD_SW_DownloadStatus);
	m_Progress_bar_GUI.SetPos(pApp->m_nGUI_SW_DownloadStatus);

	if(m_strBD_log != pApp->m_str_BD_SW_Dwnld_log)
	{
		m_strBD_log = pApp->m_str_BD_SW_Dwnld_log;
		UpdateData(0);
		RefreshStatus();
	}

	if(m_strGUI_log != pApp->m_str_GUI_SW_Dwnld_log)
	{
		m_strGUI_log = pApp->m_str_GUI_SW_Dwnld_log;
		UpdateData(0);
		RefreshStatus();
	}


	if(pApp->m_nDownloading == 0)
		KillTimer(SW_DOWNLOAD_PROGRESS_TIMER_ID);

	CDialog::OnTimer(nIDEvent);
}

void CDownloadSWDlg::RefreshStatus()
{
	int nLines = ((CEdit*)GetDlgItem(IDC_EDIT_BD_LOG))->GetLineCount();
	((CEdit*)GetDlgItem(IDC_EDIT_BD_LOG))->LineScroll(nLines,0); //make sure edit has WS_HSCROLL style added.

	nLines = ((CEdit*)GetDlgItem(IDC_EDIT_GUI_LOG))->GetLineCount();
	((CEdit*)GetDlgItem(IDC_EDIT_GUI_LOG))->LineScroll(nLines,0); //make sure edit has WS_HSCROLL style added.

}

UINT CDownloadSWDlg::DownloadThread(LPVOID lp)
{
	CPipe pipe(lp?true:false);
	pipe.Run();

	if(pipe.m_bError == true)
	{
		CString str;
		str.Format("Error downloading SW on %i",pipe.m_bBD);
		AfxMessageBox(str);
	}

	return 0;
}
void CDownloadSWDlg::OnBnClickedDwnBd()
{
	CWaitCursor wait;
	CString strBDPath;
	GetDlgItem(IDC_STATIC_BD_PATH)->GetWindowTextA(strBDPath);
	if(strBDPath.IsEmpty())
	{
		AfxMessageBox("BD elf file path required");
		return;
	}
	
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	pApp->m_nBD_SW_DownloadStatus =0;


	
	CString strDelCmd;
	strDelCmd.Format("del %s", TEMP_BD_ELF_FILE_PATH_NAME);
	int nRet = system(strDelCmd);
/*
	strCopyCmd.Format("copy %s %s /Y",strBDPath, TEMP_BD_ELF_FILE_PATH_NAME);
	nRet = system(strCopyCmd);
	if(nRet != NULL) 
	{
		AfxMessageBox("Error accessing the BD elf file, download already in progress?");
		return;
	}
*/
	if(!CopyFile( strBDPath, TEMP_BD_ELF_FILE_PATH_NAME, true))
	{
		AfxMessageBox("Can not copy BD elf file, download already in progress?");
		return;
	}


	//create temp command file for BD
	CreateTempFile(BD);
	ShellExecute(0, "open", "powerpc-elf-gdb", "--command=c:\\bd_cmd.txt", 0, SW_SHOW);


	pApp->SaveString2LocalTempFile(TEMP_LAST_BDELF_PATH, strBDPath);

	
}

void CDownloadSWDlg::OnBnClickedDwnGui()
{
	CWaitCursor wait;
	CString strGUIPath;
	GetDlgItem(IDC_STATIC_GUI_PATH)->GetWindowTextA(strGUIPath);
	if(strGUIPath.IsEmpty())
	{
		AfxMessageBox("GUI elf file path required");
		return;
	}
	if(strGUIPath.Find("VikingServiceGui") != -1)
	{
		if(AfxMessageBox("Reminder: Clear NOVRAM before running Service Mode. \nContinue loading GUI?", MB_YESNO)==IDNO)
			return;
	}
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	pApp->m_nGUI_SW_DownloadStatus =0;
	
	CString strDelCmd;
	strDelCmd.Format("del %s", TEMP_GUI_ELF_FILE_PATH_NAME);
	int nRet = system(strDelCmd);
/*
	strCopyCmd.Format("copy %s %s /Y",strGUIPath, TEMP_GUI_ELF_FILE_PATH_NAME);
	nRet = system(strCopyCmd);
	if(nRet != NULL) 
	{
		AfxMessageBox("Error accessing the GUI elf file, download already in progress?");
		return;
	}
*/
	if(!CopyFile( strGUIPath, TEMP_GUI_ELF_FILE_PATH_NAME, true))
	{
		AfxMessageBox("Can not copy GUI elf file, download already in progress?");
		return;
	}

	pApp->GUI_IODisconnect();
	//create temp command file for GUI
	CreateTempFile(GUI);
	ShellExecute(0, "open", "powerpc-elf-gdb", "--command=c:\\gui_cmd.txt", 0, SW_SHOW);


	pApp->SaveString2LocalTempFile(TEMP_LAST_GUIELF_PATH, strGUIPath);

	
}

void CDownloadSWDlg::OnBnClickedBdShell()
{

}

void CDownloadSWDlg::OnBnClickedButtonDwnSw1()
{
	

}

void CDownloadSWDlg::OnBnClickedLoadLastBd()
{
	CreateTempFile(BD);
	ShellExecute(0, "open", "powerpc-elf-gdb", "--command=c:\\bd_cmd.txt", 0, SW_SHOW);
}

void CDownloadSWDlg::OnBnClickedLoadLastGui()
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	pApp->GUI_IODisconnect();
	CreateTempFile(GUI);
	ShellExecute(0, "open", "powerpc-elf-gdb", "--command=c:\\gui_cmd.txt", 0, SW_SHOW);
}

void CDownloadSWDlg::OnBnClickedCancel()
{
	// TODO: Add your control notification handler code here
	OnCancel();
}


void CDownloadSWDlg::CreateTempFile(bool bBD)
{
	CWaitCursor wait;
	UpdateData(1);
	CString strFile;
	if(bBD)
	{
		strFile = "c:\\bd_cmd.txt";
	}
	else
	{
		strFile = "c:\\gui_cmd.txt";
	}

	CStdioFile file;
	try
	{
		file.Open(strFile, CFile::modeCreate | CFile::modeWrite | CFile::typeText );
	}
	catch( CFileException e )
	{
		LOG_MESSAGE(LM_ERROR,"CDownloadSWDlg::CreateTempFile() File could not be opened to write ERROR:%d", e.m_cause );
		return;
	}
	catch(...)
	{
		LOG_MESSAGE(LM_ERROR,"CDownloadSWDlg::CreateTempFile() File could not be opened to write" );
		return;
	}


	try
	{

		CString strTemp;
		if(bBD)
			strTemp = m_strBD_IP;
		else
			strTemp = m_strGUI_IP;

		CString str;
		str.Format("target remote %s\n", strTemp);
		file.WriteString(str);

		str.Format("mon reset \n");
		file.WriteString(str);

		if(bBD)
			strTemp = TEMP_BD_ELF_FILE_PATH_NAME;
		else
			strTemp = TEMP_GUI_ELF_FILE_PATH_NAME;


		strTemp.Replace("\\", "\\\\");
		str.Format("file %s \n", strTemp);
		file.WriteString(str);

		str.Format("load");
		file.WriteString(str);

		file.Close();

	}
	catch(CFileException e )
	{
		LOG_MESSAGE(LM_ERROR,"CDownloadSWDlg::CreateTempFiles() Could not write to the file" );
		return;

	}
	catch(...)
	{
		LOG_MESSAGE(LM_ERROR,"CDownloadSWDlg::CreateTempFiles() Could not write to the file" );
		return;

	}

	

}


BOOL CDownloadSWDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	
	if(GetFileAttributes(TEMP_LAST_BDELF_PATH)!= -1)
	{
		GetDlgItem(IDC_STATIC_BD_PATH)->SetWindowTextA(pApp->ReadLocalTempFile(TEMP_LAST_BDELF_PATH));
	}

	if(GetFileAttributes(TEMP_LAST_GUIELF_PATH)!= -1)
	{
		GetDlgItem(IDC_STATIC_GUI_PATH)->SetWindowTextA(pApp->ReadLocalTempFile(TEMP_LAST_GUIELF_PATH));
	}
	GetDlgItem(IDC_STATIC_MY_PC_IP)->SetWindowTextA("My PC IP:" + pApp->m_strMyPC_IPAddr);

	UpdateData(0);

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CDownloadSWDlg::OnBnClickedButtonNovram()
{
	if(AfxMessageBox("Are you sure? You will need to run calibrations again. \nContinue?", MB_YESNO|MB_ICONSTOP)==IDNO)
	{
		return;
	}

	CreateNovRamFiles();
	
	CString strBatch("C:\\eraseNOVRAM.bat");

	HINSTANCE n = ShellExecute(this->m_hWnd, NULL, strBatch, NULL, NULL, SW_HIDE ); 
	if((int)n <32)
	{
		char ch[30];
		_itoa((int)n, ch, 10);
		CString str;
		str.Format("error %s",ch);
		AfxMessageBox(str);
	}


}
void CDownloadSWDlg::CreateNovRamFiles()
{
	{
		CStdioFile file( "C:\\Program Files\\TTERMPRO\\eraseBD.txt", CFile::modeCreate | CFile::modeWrite | CFile::typeText );
		CString strTemp;

		strTemp.Format("messagebox \"Erasing BD flash/novram. Disconnect/Close window when done.\" \"BD\" \r\n");
		file.WriteString(strTemp);

		strTemp.Format("connect '%s' \r\n", m_strBD_IP.Left(m_strBD_IP.Find(":")));
		file.WriteString(strTemp);

		strTemp.Format("wait '>' \r\n");
		file.WriteString(strTemp);

		strTemp.Format("sendln 'e 0xfd820000' \r\n");
		file.WriteString(strTemp);

		strTemp.Format("wait '>' \r\n");
		file.WriteString(strTemp);

		strTemp.Format("sendln 'e 0xfd830000' \r\n");
		file.WriteString(strTemp);

		strTemp.Format("wait '>' \r\n");
		file.WriteString(strTemp);

		strTemp.Format("sendln 'mm 0xfb000000 0xffffffff 0x4000'\r\n");
		file.WriteString(strTemp);

		strTemp.Format("sendln 'mm 0xfb000000 0xffffffff 0x8000'\r\n");
		file.WriteString(strTemp);

		strTemp.Format("wait '>' \r\n");
		file.WriteString(strTemp);

//		strTemp.Format("sendln 'q' \r\n");
//		file.WriteString(strTemp);

		file.Close();
	}

	//GUI
	{
		CStdioFile file( "C:\\Program Files\\TTERMPRO\\eraseGUI.txt", CFile::modeCreate | CFile::modeWrite | CFile::typeText );
		CString strTemp;

		strTemp.Format("messagebox \"Erase GUI flash/novram? Disconnect/Close window when done.\" \"GUI\" \r\n");
		file.WriteString(strTemp);

		strTemp.Format("connect '%s' \r\n", m_strGUI_IP.Left(m_strGUI_IP.Find(":")));
		file.WriteString(strTemp);

		strTemp.Format("wait '>' \r\n");
		file.WriteString(strTemp);

		strTemp.Format("sendln 'e 0xfde20000' \r\n");
		file.WriteString(strTemp);

		strTemp.Format("wait '>' \r\n");
		file.WriteString(strTemp);

		strTemp.Format("sendln 'e 0xfde30000' \r\n");
		file.WriteString(strTemp);

		strTemp.Format("wait '>' \r\n");
		file.WriteString(strTemp);

		strTemp.Format("sendln 'e 0xfdd00000' \r\n");
		file.WriteString(strTemp);

		strTemp.Format("wait '>' \r\n");
		file.WriteString(strTemp);

		strTemp.Format("sendln 'e 0xfdd30000' \r\n");
		file.WriteString(strTemp);

		strTemp.Format("wait '>' \r\n");
		file.WriteString(strTemp);

		strTemp.Format("sendln 'mm 0xfb000000 0xffffffff 0x4000'\r\n");
		file.WriteString(strTemp);

		strTemp.Format("sendln 'mm 0xfb000000 0xffffffff 0x8000'\r\n");
		file.WriteString(strTemp);

		strTemp.Format("wait '>' \r\n");
		file.WriteString(strTemp);


//		strTemp.Format("sendln 'q' \r\n");
//		file.WriteString(strTemp);
		file.Close();

	}
	//batch
	{
		CStdioFile file( "C:\\eraseNOVRAM.bat", CFile::modeCreate | CFile::modeWrite | CFile::typeText );
		CString strTemp;

		strTemp.Format("@ECHO OFF \r\n");
		file.WriteString(strTemp);
 
		strTemp.Format("\"C:\\Program Files\\TTERMPRO\\ttpmacro.exe\" \"eraseBD.txt\" \r\n");
		file.WriteString(strTemp);

		strTemp.Format("\"C:\\Program Files\\TTERMPRO\\ttpmacro.exe\" \"eraseGUI.txt\" \r\n");
		file.WriteString(strTemp);

		file.Close();

	}
}


void CDownloadSWDlg::OnBnClickedButtonBurnBd()
{
	CWaitCursor wait;
	//run TFTP server
//	ShellExecute(0, "open", "C:\\Program Files\\Tftpd32\\tftpd32.exe", 0 , 0, SW_SHOW);

	UpdateData(1);
	CFileDialog dlg(TRUE, NULL,NULL,0, "BD Load (bin)|*.bin|", NULL);
	if(dlg.DoModal()==IDCANCEL)		
		return;

	if(dlg.GetFileName().Find("Bd") == -1)
	{
		if(AfxMessageBox(dlg.GetFileName()+": Doesn't look BD bin file. Do you want to continue?", MB_ICONQUESTION|MB_YESNO)==IDNO)
			return;
	}

	CString strCmd;
	strCmd.Format("del \"C:\\Program Files\\Tftpd32\\%s\" ",dlg.GetFileName());
	system(strCmd);

	//following function will fail if older bin file is not deleted by now
	if(!CopyFile( dlg.GetPathName(), "C:\\Program Files\\Tftpd32\\"+dlg.GetFileName(), true))
	{
		AfxMessageBox("Old BD elf file @ C:\\Program Files\\Tftpd32\\ was not deleted, exiting.");
		return;
	}

	CreateBurnFileBD(dlg.GetFileName());
	CreateBurnFilesOther();

	CString strBatch("C:\\burn_BD_n.bat");
	HINSTANCE n = ShellExecute(this->m_hWnd, NULL, strBatch, NULL, NULL, SW_HIDE ); 
	if((int)n <32)
	{
		char ch[30];
		_itoa((int)n, ch, 10);
		CString str;
		str.Format("error %s",ch);
		AfxMessageBox(str);
	}
}
void CDownloadSWDlg::CreateBurnFileBD(CString strBIN)
{
	//BD
	{
		CStdioFile file( "C:\\Program Files\\TTERMPRO\\burn_BD_n.txt", CFile::modeCreate | CFile::modeWrite | CFile::typeText );
		CString strTemp;

		strTemp.Format("messagebox \"Burn BD image?\" \"BD\" \r\n");
		file.WriteString(strTemp);

		strTemp.Format("connect '%s' \r\n", m_strBD_IP.Left(m_strBD_IP.Find(":")));
		file.WriteString(strTemp);

		strTemp.Format("wait '>' \r\n");
		file.WriteString(strTemp);

		strTemp.Format("sendln 'host %s'  \r\n", ((CSwatApp*)AfxGetApp())->m_strMyPC_IPAddr);
		file.WriteString(strTemp);

		strTemp.Format("wait '>' \r\n");
		file.WriteString(strTemp);

		strTemp.Format("sendln 'e 0xfd100000 0x10000 60'  \r\n");
		file.WriteString(strTemp);

		strTemp.Format("wait '>' \r\n");
		file.WriteString(strTemp);

		strTemp.Format("sendln 'p 0xfd100000 %s bin' \r\n", strBIN);
		file.WriteString(strTemp);

		strTemp.Format("wait '>' \r\n");
		file.WriteString(strTemp);

		strTemp.Format("messagebox \"Done!\" \"BD\" \r\n");
		file.WriteString(strTemp);

		file.Close();
	}
}
void CDownloadSWDlg::CreateBurnFileGUI(CString strBIN)
{	//GUI
	{
		CStdioFile file( "C:\\Program Files\\TTERMPRO\\burn_GUI_n.txt", CFile::modeCreate | CFile::modeWrite | CFile::typeText );
		CString strTemp;

		strTemp.Format("messagebox \"Burn image on GUI?\" \"GUI\" \r\n");
		file.WriteString(strTemp);

		strTemp.Format("connect '%s' \r\n", m_strGUI_IP.Left(m_strGUI_IP.Find(":")));
		file.WriteString(strTemp);

		strTemp.Format("wait '>' \r\n");
		file.WriteString(strTemp);

		strTemp.Format("sendln 'host %s'  \r\n", ((CSwatApp*)AfxGetApp())->m_strMyPC_IPAddr);
		file.WriteString(strTemp);

		strTemp.Format("wait '>' \r\n");
		file.WriteString(strTemp);

		strTemp.Format("sendln 'e 0xfd100000 0x10000 112'  \r\n");
		file.WriteString(strTemp);

		strTemp.Format("wait '>' \r\n");
		file.WriteString(strTemp);

		strTemp.Format("sendln 'e 0xfd800000 0x2000 8'  \r\n");
		file.WriteString(strTemp);

		strTemp.Format("wait '>' \r\n");
		file.WriteString(strTemp);

		strTemp.Format("sendln 'e 0xfd810000 0x10000 42'  \r\n");
		file.WriteString(strTemp);

		strTemp.Format("wait '>' \r\n");
		file.WriteString(strTemp);

		strTemp.Format("sendln 'p 0xfd100000 %s bin'  \r\n", strBIN);
		file.WriteString(strTemp);

		strTemp.Format("wait '>' \r\n");
		file.WriteString(strTemp);

		strTemp.Format("messagebox \"Done\" \"GUI\" \r\n");
		file.WriteString(strTemp);

	}
}

void CDownloadSWDlg::CreateBurnFilesOther()
{
	//batch file BD
	{
		CStdioFile file( "C:\\burn_BD_n.bat", CFile::modeCreate | CFile::modeWrite | CFile::typeText );
		CString strTemp;

		strTemp.Format("@ECHO OFF \r\n");
		file.WriteString(strTemp);
 
		//"C:\Program Files\TTERMPRO\ttpmacro.exe" "burn_BD.txt" 

		strTemp.Format("\"C:\\Program Files\\TTERMPRO\\ttpmacro.exe\" \"burn_BD_n.txt\"  \r\n");
		file.WriteString(strTemp);

		file.Close();

	}
	//batch file GUI
	{
		CStdioFile file( "C:\\burn_GUI_n.bat", CFile::modeCreate | CFile::modeWrite | CFile::typeText );
		CString strTemp;

		strTemp.Format("@ECHO OFF \r\n");
		file.WriteString(strTemp);
 
		strTemp.Format("\"C:\\Program Files\\TTERMPRO\\ttpmacro.exe\" \"burn_GUI_n.txt\"  \r\n");
		file.WriteString(strTemp);

		file.Close();

	}

}
void CDownloadSWDlg::OnBnClickedButtonBurnGui()
{
	UpdateData(1);
	CFileDialog dlg(TRUE, NULL,NULL,0, "GUI Load (gz)|*.gz|GUI Load (bin)|*.bin|", NULL);
	if(dlg.DoModal()==IDCANCEL)		
		return;

	if(dlg.GetFileName().Find("gz") == -1)
	{
		if(AfxMessageBox(dlg.GetFileName()+": Doesn't look gz file. Do you want to continue?", MB_ICONQUESTION|MB_YESNO)==IDNO)
		{
			return;
		}
	}
	if(dlg.GetFileName().Find("VikingServiceGui") != -1)
	{
		if(AfxMessageBox("Reminder: Clear NOVRAM before running Service Mode. \nContinue burning GUI?", MB_YESNO)==IDNO)
		{
			return;
		}
	}
	CString strCmd;
	strCmd.Format("del \"C:\\Program Files\\Tftpd32\\%s\" ",dlg.GetFileName());
	system(strCmd);

	if(!CopyFile( dlg.GetPathName(), "C:\\Program Files\\Tftpd32\\"+dlg.GetFileName(), true))
	{
		AfxMessageBox("Old GUI gz file @ C:\\Program Files\\Tftpd32\\ was not deleted, exiting...");
		return;
	}

	CreateBurnFileGUI(dlg.GetFileName());
	CreateBurnFilesOther();

	CString strBatch("C:\\burn_GUI_n.bat");
	HINSTANCE n = ShellExecute(this->m_hWnd, NULL, strBatch, NULL, NULL, SW_HIDE ); 
	if((int)n <32)
	{
		char ch[30];
		_itoa((int)n, ch, 10);
		CString str;
		str.Format("error %s",ch);
		AfxMessageBox(str);
	}
}

