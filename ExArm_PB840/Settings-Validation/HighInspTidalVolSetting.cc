#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//================= C L A S S   D E S C R I P T I O N =================
//@ Class: HighInspTidalVolSetting - High Inspired Tidal Volume Limit
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, bounded setting that is used to limit the inspired
//  tidal volume, during spontaneous, tube-compensated breaths.  This class
//  inherits from 'BatchBoundedSetting' and provides the specific information
//  needed for representation of this limit.  This information includes the
//  interval and range of this setting's values, this batch setting's
//  new-patient value (see 'getNewPatientValue()'), and the constraints of
//  this setting.
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class has no dependent settings, therefore 'calcDependentValues_()'
//  is NOT overridden.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/HighInspTidalVolSetting.ccv   25.0.4.0   19 Nov 2013 14:27:24   pvcs  $
//
//@ Modification-Log
//
//  Revision: 011   By: mnr    Date: 10-Jul-2009    SCR Number: 6437
//  Project:  NEO
//  Description:
//		Enforced lower bound on calculated new patient value.
//
//  Revision: 010   By: gdc    Date: 11-Aug-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//  Modified to support changing IBW while ventilating. Added 
//  setting constants for scaling factors.
//
//  Revision: 009 By: jja     Date:  21-Mar-2002    DR Number: 5790
//  Project:  VTPC
//  Description:
//	Added Vt/Vti/Vsupp setting interaction/limitations for VC+ and VS
//
//  Revision: 008  By: erm     Date:  05-Mar-2002    DR Number: 5790
//  Project:  VTPC
//  Description:
//	Add Applicability for  VC+
//
//  Revision: 007  By: sah     Date:  11-Sep-2000    DR Number: 5766
//  Project:  VTPC
//  Description:
//	Added new upper soft limit.
//
//  Revision: 006  By: sah     Date:  02-Aug-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//	VTPC project-related changes:
//      *  added support for new 'VS' spontaneous type
//
//  Revision: 005   By: sah   Date:  22-Jul-1999    DR Number: 5313
//  Project:  PAV
//  Description:
//	PAV project-related changes:
//      *  added support for new 'PA' spontaneous type
//
//  Revision: 004   By: sah    Date: 22-Sep-1999    DR Number: 5424
//  Project:  NeoMode
//  Description:
//	Obsoleted 'SettingOptions' class, to use new global
//      'SoftwareOptions' class.
//
//  Revision: 003   By: sah    Date: 29-Jun-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related issues:
//      *  using new 'getAbsolute{Min,Max}Value_()' methods
//	*  modified to use newly-modified 'BoundedRange' class to handle
//         range, via linked list of interval nodes
//      *  eliminated 'get{Max,Min}Limit()' method, now defined by base class
//
//  Revision: 002   By: sah   Date:  19-Jan-1999    DR Number:  5314
//  Project:  ATC
//  Description:
//     Added new 'getApplicability()' method.
//
//  Revision: 001   By: sah   Date:  19-Jan-1999    DR Number:  5322
//  Project:  ATC
//  Description:
//	New ATC-specific setting.
//
//=====================================================================

#include "HighInspTidalVolSetting.hh"
#include "SettingConstants.hh"
#include "SupportTypeValue.hh"
#include "MandTypeValue.hh"
#include "ModeValue.hh"

//@ Usage-Classes
#include "SettingsMgr.hh"
#include "SoftwareOptions.hh"
#include  "ContextMgr.hh"
#include "AdjustedContext.hh"
//@ End-Usage

//@ Code...

//====================================================================
//
//  Static Data Definitions...
//
//====================================================================

//  $[TC02038] -- The setting's range...
//  $[TC02040] -- The setting's resolution ...
static const BoundedInterval  LAST_NODE_ =
{
	6.0f,		// absolute minimum...
	0.0f,		// unused...
	TENTHS,		// unused...
	NULL
};
static const BoundedInterval  NODE2_ =
{
	100.0f,		// change-point...
	1.0f,		// resolution...
	ONES,		// precision...
	&::LAST_NODE_
};
static const BoundedInterval  NODE1_ =
{
	400.0f,		// change-point...
	5.0f,		// resolution...
	ONES,		// precision...
	&::NODE2_
};
static const BoundedInterval  INTERVAL_LIST_ =
{
	6000.0f,		// absolute maximum...
	10.0f,		// resolution...
	TENS,		// precision...
	&::NODE1_
};


//====================================================================
//
//  Public Methods...
//
//====================================================================

//============= M E T H O D   D E S C R I P T I O N ==================
//@ Method: HighInspTidalVolSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This setting inherits from BoundedSetting.  The method passes to
//  BoundedSetting it's default information. Also, this method initializes
//  the value interval range for the setting.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

HighInspTidalVolSetting::HighInspTidalVolSetting(void)
: BatchBoundedSetting(SettingId::HIGH_INSP_TIDAL_VOL,
					  Setting::NULL_DEPENDENT_ARRAY_,
					  &::INTERVAL_LIST_,
					  HIGH_INSP_TIDAL_VOL_MAX_ID,	 // maxConstraintId...
					  HIGH_INSP_TIDAL_VOL_MIN_ID)	 // minConstraintId...
{
	CALL_TRACE("HighInspTidalVolSetting()");

	// register this setting's soft bound ids...
	registerSoftBound_(::HIGH_INSP_TIDAL_VOL_SOFT_MAX_BASED_IBW_ID, Setting::UPPER);
}  // $[TI1]


//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  ~HighInspTidalVolSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

HighInspTidalVolSetting::~HighInspTidalVolSetting(void)
{
	CALL_TRACE("~HighInspTidalVolSetting()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability(qualifierId)  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[01081] -- breath settings applicability
//  $[01108] -- alarm settings applicability
//---------------------------------------------------------------------
//@ PreCondition
//  (qualifierId == Notification::ACCEPTED  ||
//   qualifierId == Notification::ADJUSTED)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
	HighInspTidalVolSetting::getApplicability(
											 const Notification::ChangeQualifier qualifierId
											 ) const
{
	CALL_TRACE("getApplicability(qualifierId)");

	Applicability::Id  applicability;
	DiscreteValue      mandTypeValue;

	if ( SoftwareOptions::IsOptionEnabled(SoftwareOptions::ATC)  ||
		 SoftwareOptions::IsOptionEnabled(SoftwareOptions::PAV) ||
		 SoftwareOptions::IsOptionEnabled(SoftwareOptions::VTPC) )
	{  // $[TI1] -- TC,VC+ and/or PA is an active option...
		const Setting*  pSupportType =
			SettingsMgr::GetSettingPtr(SettingId::SUPPORT_TYPE);

		const Setting*  pMandType =
			SettingsMgr::GetSettingPtr(SettingId::MAND_TYPE);


		if ( pSupportType->getApplicability(qualifierId) !=
			 Applicability::INAPPLICABLE )
		{  // $[TI1.1] -- support type is applicable...
			DiscreteValue  supportTypeValue;

			switch ( qualifierId )
			{
				case Notification::ACCEPTED :	  // $[TI1.1.1]
					supportTypeValue = pSupportType->getAcceptedValue();
					mandTypeValue    = pMandType->getAcceptedValue(); 
					break;
				case Notification::ADJUSTED :	  // $[TI1.1.2]
					supportTypeValue = pSupportType->getAdjustedValue();
					mandTypeValue    = pMandType->getAdjustedValue(); 
					break;
				default :
					AUX_CLASS_ASSERTION_FAILURE(qualifierId);
					break;
			}

			switch ( supportTypeValue )
			{
				case SupportTypeValue::PSV_SUPPORT_TYPE :
				case SupportTypeValue::OFF_SUPPORT_TYPE :
					applicability = Applicability::INAPPLICABLE;
					break;
				case SupportTypeValue::VSV_SUPPORT_TYPE :
				case SupportTypeValue::ATC_SUPPORT_TYPE :
				case SupportTypeValue::PAV_SUPPORT_TYPE : // $[TI1.1.3]
					applicability = Applicability::CHANGEABLE;
					break;
				default :
					// unexpected 'supportTypeValue'...
					AUX_CLASS_ASSERTION_FAILURE(supportTypeValue);
					break;
			};

			// For SIMV VC+ with support types of NONE and PS
			if ( mandTypeValue ==  MandTypeValue::VCP_MAND_TYPE )
			{	
				applicability = Applicability::CHANGEABLE;
			}

		}
		else
		{
			switch ( qualifierId )
			{
				case Notification::ACCEPTED :		 // $[TI1.1.1]
					mandTypeValue    = pMandType->getAcceptedValue(); 
					break;
				case Notification::ADJUSTED :		 // $[TI1.1.2]
					mandTypeValue    = pMandType->getAdjustedValue(); 
					break;
				default :
					AUX_CLASS_ASSERTION_FAILURE(qualifierId);
					break;

			};
			if ( mandTypeValue ==  MandTypeValue::VCP_MAND_TYPE )
			{	
				applicability = Applicability::CHANGEABLE;
			}
			else
			{	
				applicability = Applicability::INAPPLICABLE;
			}

		}
	}
	else
	{  // $[TI2] -- TC ,PA  or VC+ are not an active options...
		applicability = Applicability::INAPPLICABLE;
	}

	return(applicability);
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [const, virtual]
//
//@ Interface-Description
//  Return the setting's new patient value.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
	HighInspTidalVolSetting::getNewPatientValue(void) const
{
	CALL_TRACE("getNewPatientValue()");

	// Adjusted Context contains the new-patient value of IBW)...
	const Setting*  pIbw = SettingsMgr::GetSettingPtr(SettingId::IBW);

	const Real32  IBW_VALUE = BoundedValue(pIbw->getAdjustedValue()).value;

	const Real32  ABSOLUTE_MIN = getAbsoluteMinValue_();

	BoundedValue  newPatient;

	// $[TC02039] -- this setting's new-patient calculation...
	// calculate the IBW-based new-patient value...
	newPatient.value = MAX_VALUE( ABSOLUTE_MIN, 
								  (IBW_VALUE * SettingConstants::NP_HIGH_INSP_TIDAL_VOL_IBW_SCALE) );
	
	// warp the calculated value to a "click" boundary...
	getBoundedRange().warpValue(newPatient);

	return(newPatient);	// $[TI1]
}


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//	      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//  This setting may be subscripted by MAND, SPONT, or nothing depending
//  on the applicable mand and spont types.  Thus, the bound IDs are set
//  based on checking for combinations of VC+, VS, TC, and PA.
//  The softbound message is generic Vti with no subscript since the
//  softbound routines will not permit multiple message IDs.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	HighInspTidalVolSetting::SoftFault(const SoftFaultID  softFaultID,
									   const Uint32       lineNumber,
									   const char*        pFileName,
									   const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

	FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
							HIGH_INSP_TIDAL_VOL_SETTING, lineNumber, pFileName,
							pPredicate);
}



//====================================================================
//
//  Protected Methods...
//
//====================================================================

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  updateConstraints_()  [virtual]
//
//@ Interface-Description
//  Update the dynamic bounds of this setting.  This method sets this
//  setting's maximum constraint to the most restrictive of this setting's
//  upper bounds, and, correspondingly, set this setting's minimum
//  constraint to its most restrictive lower bound.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[TC02038] -- high insp tidal volume limit's range...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	HighInspTidalVolSetting::updateConstraints_(void)
{
	CALL_TRACE("updateConstraints_()");

	Setting*  pSetting = SettingsMgr::GetSettingPtr(SettingId::IBW);

	const Real32  IBW_VALUE = BoundedValue(pSetting->getAdjustedValue()).value;

	pSetting = SettingsMgr::GetSettingPtr(SettingId::MAND_TYPE);

	const DiscreteValue  MAND_TYPE_VALUE = pSetting->getAdjustedValue();

	pSetting = SettingsMgr::GetSettingPtr(SettingId::MODE);

	const DiscreteValue  MODE_VALUE = pSetting->getAdjustedValue();

	pSetting = SettingsMgr::GetSettingPtr(SettingId::SUPPORT_TYPE);

	const DiscreteValue  SPONT_TYPE_VALUE = pSetting->getAdjustedValue();

	Boolean tVtiMandType = FALSE;
	Boolean tVtiSpontType= FALSE;



	//===================================================================
	// calculate high insp tidal vol limit's most restrictive MINIMUM bound...
	//===================================================================
	{
		const Real32  ABSOLUTE_MIN = getAbsoluteMinValue_();

		BoundedRange::ConstraintInfo  minConstraintInfo;

		BoundedValue  ibwBasedMin;

		BoundedValue  vtBasedMin;
		vtBasedMin.value = ABSOLUTE_MIN - 1.0f;	 // Ensure a low value to start

		BoundStatus dummyStatus(getId());


		if ( (MODE_VALUE != ModeValue::AC_MODE_VALUE)  && 
			 ((SPONT_TYPE_VALUE == SupportTypeValue::VSV_SUPPORT_TYPE) ||
			  (SPONT_TYPE_VALUE == SupportTypeValue::PAV_SUPPORT_TYPE) ||
			  (SPONT_TYPE_VALUE == SupportTypeValue::ATC_SUPPORT_TYPE)) )
		{
			tVtiSpontType = TRUE;
		}

		if ( MAND_TYPE_VALUE == MandTypeValue::VCP_MAND_TYPE )
		{
			pSetting = SettingsMgr::GetSettingPtr(SettingId::TIDAL_VOLUME);
			vtBasedMin.value = BoundedValue(pSetting->getAdjustedValue()).value;
			tVtiMandType = TRUE;
		}
		else if ( MODE_VALUE == ModeValue::SPONT_MODE_VALUE && 
				  SPONT_TYPE_VALUE == SupportTypeValue::VSV_SUPPORT_TYPE )
		{
			pSetting = SettingsMgr::GetSettingPtr(SettingId::VOLUME_SUPPORT);
			vtBasedMin.value = BoundedValue(pSetting->getAdjustedValue()).value;
		}

		// Assume the absolute minimum will be more restrictive, and
		//  make it the lower bound value...

		minConstraintInfo.id = HIGH_INSP_TIDAL_VOL_MIN_SPONT_ID;   //default -- spont assumed
		if ( tVtiMandType )
		{
			if ( tVtiSpontType )
			{
				minConstraintInfo.id = HIGH_INSP_TIDAL_VOL_MIN_ID;
			}
			else
			{
				minConstraintInfo.id = HIGH_INSP_TIDAL_VOL_MIN_MAND_ID;
			}
		}

		minConstraintInfo.value = ABSOLUTE_MIN;


		// calculate and test the IBW-based minimum constraint...
		ibwBasedMin.value = (IBW_VALUE * SettingConstants::MIN_HIGH_INSP_TIDAL_VOL_IBW_SCALE);

		if ( ibwBasedMin.value > ABSOLUTE_MIN )
		{	// $[TI1]
			// reset this range's minimum constraint to allow for "warping" of a
			// new minimum value...
			getBoundedRange_().resetMinConstraint();

			// warp the minimum value to an upper "click" boundary...
			getBoundedRange_().warpValue(ibwBasedMin, BoundedRange::WARP_UP);

			// the IBW-based minimum is more restrictive, therefore make it the
			// lower bound value...
			minConstraintInfo.id    = HIGH_INSP_TIDAL_VOL_MIN_BASED_IBW_SPONT_ID;  //Default -- assume spont
			if ( tVtiMandType )
			{
				if ( tVtiSpontType )
				{
					minConstraintInfo.id = HIGH_INSP_TIDAL_VOL_MIN_BASED_IBW_ID;
				}
				else
				{
					minConstraintInfo.id = HIGH_INSP_TIDAL_VOL_MIN_BASED_IBW_MAND_ID;
				}
			}

			minConstraintInfo.value = ibwBasedMin.value;
		}

		// Now test for the volume based limitation (but only after all settings are initialized)
		AdjustedContext&  rAdjContext = ContextMgr::GetAdjustedContext();
		if ( (vtBasedMin.value >= minConstraintInfo.value) && rAdjContext.getAllSettingValuesKnown() )
		{
			// the Volume-based minimum is more restrictive, therefore make it the
			// lower bound value...

			// reset this range's minimum constraint to allow for "warping" of a
			// new minimum value...
			getBoundedRange_().resetMinConstraint();
			minConstraintInfo.id    = HIGH_INSP_TIDAL_VOL_MIN_BASED_VOL_SUP_SPONT_ID;  //default -- assume spont
			if ( tVtiMandType )
			{
				if ( tVtiSpontType )
				{
					minConstraintInfo.id = HIGH_INSP_TIDAL_VOL_MIN_BASED_TIDAL_VOL_ID;
				}
				else
				{
					minConstraintInfo.id = HIGH_INSP_TIDAL_VOL_MIN_BASED_TIDAL_VOL_MAND_ID;
				}
			}

			getBoundedRange_().increment( 1, vtBasedMin, dummyStatus);
			minConstraintInfo.value = vtBasedMin.value;
		}

		// this setting has no minimum soft bounds...
		minConstraintInfo.isSoft = FALSE;

		getBoundedRange_().updateMinConstraint(minConstraintInfo);
	}


	//===================================================================
	// calculate high insp tidal vol limit's most restrictive MAXIMUM bound...
	//===================================================================
	{
		const Real32  ABSOLUTE_MAX = getAbsoluteMaxValue_();

		Real32  dummySoftMinValue, softMaxValue;

		findSoftMinMaxValues_(dummySoftMinValue, softMaxValue);

		BoundedRange::ConstraintInfo  maxConstraintInfo;

		BoundedValue  ibwBasedMax;


		if ( isSoftBoundActive_(HIGH_INSP_TIDAL_VOL_SOFT_MAX_BASED_IBW_ID) )
		{
			// High insp tidal volume's soft maximum bound is
			// currently active...
			// initialize to soft bound maximum bound id...
			ibwBasedMax.value = softMaxValue;

			//  NOTE:  registerSoftBound only permits a single ID, so we
			//         have to use a generic soft bound id instead of the specific
			//         Vtispont, Vtimand ids used elsewhere
			maxConstraintInfo.id = HIGH_INSP_TIDAL_VOL_SOFT_MAX_BASED_IBW_ID;
		}
		else
		{
			// High insp tidal volume's soft bound is NOT active...
			// initialize to absolute ibw based maximum bound id...
			ibwBasedMax.value = (IBW_VALUE *
								 SettingConstants::MAX_TIDAL_VOL_IBW_SCALE);

			maxConstraintInfo.id = HIGH_INSP_TIDAL_VOL_MAX_BASED_IBW_SPONT_ID;	// default -- assume spont
			if ( tVtiMandType )
			{
				if ( tVtiSpontType )
				{
					maxConstraintInfo.id = HIGH_INSP_TIDAL_VOL_MAX_BASED_IBW_ID;
				}
				else
				{
					maxConstraintInfo.id = HIGH_INSP_TIDAL_VOL_MAX_BASED_IBW_MAND_ID;
				}
			}
		}


		// This path is taken by an active soft bound
		if ( ibwBasedMax.value < ABSOLUTE_MAX )
		{	// $[TI3]
			// reset this range's maximum constraint to allow for "warping" of a
			// new maximum value...
			getBoundedRange_().resetMaxConstraint();

			// warp the maximum value to an lower "click" boundary...
			getBoundedRange_().warpValue(ibwBasedMax, BoundedRange::WARP_DOWN);

			// the IBW-based maximum is more restrictive, therefore make it the
			// lower bound value...
			maxConstraintInfo.value = ibwBasedMax.value;
		}
		else
		{
			// make the absolute maximum the upper bound value...
			maxConstraintInfo.id = HIGH_INSP_TIDAL_VOL_MAX_SPONT_ID;  //default -- assume spont
			if ( tVtiMandType )
			{
				if ( TRUE == tVtiSpontType )
				{
					maxConstraintInfo.id = HIGH_INSP_TIDAL_VOL_MAX_ID;
				}
				else
				{
					maxConstraintInfo.id = HIGH_INSP_TIDAL_VOL_MAX_MAND_ID;
				}
			}
			maxConstraintInfo.value = ABSOLUTE_MAX;
		}

		// this setting has one maximum soft bound...
		maxConstraintInfo.isSoft =
			(maxConstraintInfo.id == HIGH_INSP_TIDAL_VOL_SOFT_MAX_BASED_IBW_ID ); 

		getBoundedRange_().updateMaxConstraint(maxConstraintInfo);
	}
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  findSoftMinMaxValues_(rSoftMinValue, rSoftMaxValue)  [const]
//
//@ Interface-Description
//  Return, via 'rSoftMinValue' and 'rSoftMaxValue', the soft bound lower
//  and upper limit values, respectively.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[VC02013] -- high insp tidal volume's soft range...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	HighInspTidalVolSetting::findSoftMinMaxValues_(Real32& rSoftMinValue,
												   Real32& rSoftMaxValue) const
{
	CALL_TRACE("findSoftMinMaxValues_(rSoftMinValue, rSoftMaxValue)");

	const Setting*  pIbw = SettingsMgr::GetSettingPtr(SettingId::IBW);

	const Real32  IBW_VALUE = BoundedValue(pIbw->getAdjustedValue()).value;

	BoundedValue  boundedValue;

	//-------------------------------------------------------------------
	// disable lower soft bound limit...
	//-------------------------------------------------------------------

	rSoftMinValue = ::LAST_NODE_.value;

	//-------------------------------------------------------------------
	// determine upper soft bound limit value...
	//-------------------------------------------------------------------

	// calculate upper soft bound limit...
	boundedValue.value = (IBW_VALUE * 20.0f);

	// warp the calculated value to the lower "click", with no clipping...
	getBoundedRange().warpValue(boundedValue, BoundedRange::WARP_DOWN, FALSE);

	rSoftMaxValue = boundedValue.value;
}  // $[TI1]
