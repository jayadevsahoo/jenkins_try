#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: TrendDbConfig
//---------------------------------------------------------------------
//@ Interface-Description
//  The TrendDbConfig maps the TrendDbSampleData objects to slots in the
//  repository.  A slot in the repository is a column in each of the tables
//  that is mapped into the sample data object.  So for example the slot 0
//  of the patient repository is User Events for all of the patient data
//  tables.  The TrendDbConfig is constructed with a max size in slots
//  for this repository, and then each slot is configured with configSlot().
// 
//  Clients then can find the sample data for a given slot via the method
//  getSampleData().  Or the client can get the slot number given a trend Id
//  via the getSlotNumber() method.
// 
//---------------------------------------------------------------------
//@ Rationale
//  Encapsulates the mapping between slots in the repository and the sample
//  data objects that collect and compress the trended values.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The TrendDbConfig uses two tables (that hard coded sizes) to map from
//  slot to SampleData pointer and trendId to slot number.  It is assumed
//  that each sample data object has a unique trend Id.
//  The constructor initialize all of the sample data pointers to zero in
//  the sampleDataMap_ and all of the slot numbers to -1 in the trendIdMap_
//  The configSlot() method fills in an entry in both tables and the static
//  get... methods return entries from these tables.
//---------------------------------------------------------------------
//@ Fault-Handling
//  SoftFault is used
//---------------------------------------------------------------------
//@ Restrictions
//  The maximum size supported is 100 slots.
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Trend-Database/vcssrc/TrendDbConfig.ccv   25.0.4.0   19 Nov 2013 14:34:36   pvcs  $
//
//@ Modification-Log
// 
//  Revision: 001  By:  mjf    Date:  18-Jun-2007    SCR Number: 6237
//       Project:  Trend
//       Description:
//             Initial version
// 
//
//=====================================================================

#include <string.h>

#include "Trend_Database.hh"
#include "TrendDbConfig.hh"
#include "TrendDataMgr.hh"
#include "TrendDbSampleData.hh"

//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TrendDbConfig [constructor]
//
//@ Interface-Description
//  Initialize the values
//---------------------------------------------------------------------
//@ Implementation-Description
//  Sets the sampleDataMap_ to all null "zero" pointers and
//  the trendIdMap to all -1 "no slot".
//---------------------------------------------------------------------
//@ PreCondition
//	maxSlotNumber must be less than MAX_CONFIG_SIZE
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
TrendDbConfig::TrendDbConfig( Uint8 maxSlotNumber )
{
	AUX_CLASS_PRE_CONDITION( (maxSlotNumber <= MAX_CONFIG_SIZE),
							 ((maxSlotNumber << 16) | MAX_CONFIG_SIZE) );
	maxSlotNumber_ = maxSlotNumber;

	// Clear the maps array's
	Int16 i;
	for (i = 0; i < maxSlotNumber_; i++)
	{
		sampleDataMap_[ i ] = 0;
	}
	for (i = 0; i < TrendDataMgr::MAX_TREND_ID; i++)
	{
		trendIdMap_[i] = -1;
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TrendPatientDataDBConfig [destructor]
//
//@ Interface-Description
//  Does no action  
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
TrendDbConfig::~TrendDbConfig()	
{
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: configSlot
//
//@ Interface-Description
//  Defines a Sample object to a slot number
//---------------------------------------------------------------------
//@ Implementation-Description
//  Adds the sample data object to the sampleDatamap_ and also
//  updates the trendIdMap_ with the slot number
//---------------------------------------------------------------------
//@ PreCondition
//	valid slot number and non zero sample data pointer
//  The trend Id of the sample data object must be valid
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDbConfig::configSlot( TrendDbSampleData * ptrSampleData, Uint8 slotNumber )
{
	AUX_CLASS_PRE_CONDITION( (slotNumber < maxSlotNumber_),
							 ((slotNumber << 16) | maxSlotNumber_) );
	AUX_CLASS_PRE_CONDITION( ptrSampleData, (Uint32) ptrSampleData );

	Int16 trendId = ptrSampleData->getTrendId();
	AUX_CLASS_PRE_CONDITION( (trendId < TrendDataMgr::MAX_TREND_ID),
							 ((trendId << 16) | TrendDataMgr::MAX_TREND_ID ) );


	// map the sample data and the trendId
	sampleDataMap_[ slotNumber ] = ptrSampleData;
	trendIdMap_[ trendId ] = slotNumber;

	TREND_MGR_DBG4( 32, "TrendDbConfig Id ", trendId,
					" -> slot ", slotNumber );
	TREND_MGR_DBG4( 32, "    Sample Data ", hex, (Uint32) ptrSampleData, dec );

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================
void TrendDbConfig::SoftFault(const SoftFaultID  softFaultID,
							  const Uint32       lineNumber,
							  const char*        pFileName,
							  const char*        pPredicate)  
{
	FaultHandler::SoftFault(softFaultID, TREND_DATABASE, TREND_DB_CONFIG,
							lineNumber, pFileName, pPredicate);
}




