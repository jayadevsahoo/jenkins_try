#include "stdafx.h"
//=====================================================================
//   This is a proprietary work to which Puritan-Bennett corporation
//   of California claims exclusive right.  No part of this work may
//  be used, disclosed, reproduced, sorted in an information retrieval
//     system, or transmitted by any means, electronic, mechanical,
//   photocopying, recording, or otherwise without the prior written
//      permission of Puritan-Bennett Corporation of California.
//
//          Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: BdStart - BD State: Start.
//---------------------------------------------------------------------
//@ Interface-Description
//  The BdStart class is a FsmState used by the BdFsm finite state 
//  machine.  It is a transitory state in which the application tasks
//  are created before entering the BdOnline, BdService or BdInop states.
//  This class determines the initial FsmState based on the conditions
//  returned by the InitConditions class.  When service mode is 
//  requested, a transition to the BdService state occurs.  If service
//  mode is not requested yet service is required, a transition to 
//  the BdInop state occurs.  If neither condition is true, the
//  vent is transitioned to the BdOnline state.
//---------------------------------------------------------------------
//@ Rationale
//  This class represents the BD Start state.  It contains methods 
//  to start the BD finite state machine and transition to an initial
//  operational state.
//---------------------------------------------------------------------
//@ Implementation-Description
//  BdStart is derived from the FsmState class.  It provides the 
//  activate method called when the BdFsm is activated.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Sys-Init/vcssrc/BdStart.ccv   25.0.4.0   19 Nov 2013 14:33:50   pvcs  $
//
//@ Modification-Log
//
//   Revision 005   By:   sah      Date: 01/13/99    DR Number: 5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to a non-inlined method.
//
//   Revision 004   By:   gdc      Date: 12-Oct-1998 DCS Number: 5164
//      Project:   BiLevel
//      Description:
//         Replaced call to InitConditions::IsServiceModeRequested with
//		   call to Post::IsServiceModeRequested.
//
//   Revision 003   By:   gdc      Date: 17-JUN-1997 DR Number:  1902
//      Project:   Sigma   (R8027)
//      Description:
//         Removed receiveGuiReady() as "dead" code since BdStart
//         is a transitory state only.
//
//   Revision 002   By: Gary Cederquist Date: 25-APR-1997 DR Number: 1996
//      Project:   Sigma   (R8027)
//      Description:
//         Added missing requirement number.
//
//   Revision 001   By:   gdc      Date: 07/06/95    DR Number:   n/a  
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//=====================================================================

//=====================================================================
//
//      Includes...
//
//=====================================================================

#include "BdStart.hh"
#include "SigmaState.hh"
#include "TaskFlags.hh"

//@ Usage-Classes
#include "BdFsm.hh"
#include "BdInop.hh"
#include "BdOnline.hh"
#include "BdService.hh"
#include "InitConditions.hh"
#include "Post.hh"
#include "ServiceMode.hh"
#include "TaskAgentManager.hh"
#include "TaskControlAgent.hh"
// TODO E600 remove
//#include "Ostream.hh"
//@ End-Usage

//=====================================================================
//
//      Static Data...
//
//=====================================================================

//@ Code...

//=====================================================================
//
//      Public Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BdStart [default constructor]
//
//@ Interface-Description
//  BdStart class default constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

BdStart::BdStart()
{
    CALL_TRACE("BdStart()");
    // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~BdStart [destructor]
//
//@ Interface-Description
//  BdStart class destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

BdStart::~BdStart()
{
    CALL_TRACE("~BdStart()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
// Method: nameOf
//
// Interface-Description
//  Returns the name of the class.
//---------------------------------------------------------------------
// Implementation-Description
//  Returns a pointer to the const string containing the class name.
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================
 
const char*
BdStart::nameOf(void) const
{
    CALL_TRACE("nameOf(void)");
    return "BdStart";
    // $[TI1]
}
 

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getSigmaState
//
//@ Interface-Description
//  The getSigmaState method returns the SigmaState associated with 
//  this BdStart state.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns a SigmaState enumeration for the START state.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
SigmaState
BdStart::getSigmaState(void) const
{
    CALL_TRACE("getSigmaState(void)");
    return STATE_START;
    // $[TI1]
}
 

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activate
//
//@ Interface-Description
//  The activate method determines the target state for BD task control
//  upon starting the finite state machine.  BdFsm::Start invokes this
//  method after completing initialization.  It derives the initial
//  BD state from the service mode requested and BD service required
//  conditions.  If service mode is requested, this method transitions
//  the finite state machine to the SERVICE state.  If service mode is 
//  not requested and yet service is required, the method transitions
//  to INOP state and latches the vent-inop hardware.  If service (EST)
//  or SST is required, this method activates the audio alarm on the
//  BD.  A transition to the ONLINE state occurs only when 
//  service mode is not requested and service is not required.  After
//  the appropriate state transition, the application task are created
//  and informed of the current state.  A BdReady message for the 
//  current state is then sent to GUI task control. 
//  $[11075]
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method uses the InitConditions container class for the states
//  of service mode required. It uses Post for the service mode request
//  state. For each 
//  transition, the state is retrieved from the finite state machine
//  using the BdFsm::Get<FsmState> method and the Fsm messaged
//  to change the finite state machine's state.  The TaskAgentManager 
//  is used to create the application tasks and queue a 
//  ChangeStateMessage to each managed task.  TaskControlAgent::
//  ReportNodeReady is messaged to send a BdReadyMessage to GUI 
//  task control.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
void
BdStart::activate(void) const
{
    CALL_TRACE("activate(void)");

    if (Post::IsServiceModeRequested()) // $[TI1]
    {
        BdFsm::GetBdServiceState().startTransition();
    }
    else if (InitConditions::IsServiceRequired()) // $[TI2]
    {
        BdFsm::GetBdInopState().startTransition();
    } 
    else // $[TI3]
    {
        BdFsm::GetBdOnlineState().startTransition();
    }   
    //  start tasks after establishing target state for processor
    //  since tasks use target state during their startup processing
    TaskAgentManager::CreateTasks();
}

 

#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//
//@ Interface-Description
//   This method is called when a software fault is detected by the
//   fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//   'lineNumber' are essential pieces of information.  The 'pFileName'
//   and 'pPredicate' strings may be defaulted in the macro to reduce
//   code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This method receives the call for the SoftFault, adds it sub-system
//   and class name ID and sends the message to the non-member function
//   SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
void
BdStart::SoftFault(const SoftFaultID  softFaultID,
                                  const Uint32       lineNumber,
                                  const char*        pFileName,
                                  const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
 
  FaultHandler::SoftFault(softFaultID, SYS_INIT,
                          Sys_Init::ID_BDSTART,
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)

//=====================================================================
//
//      Protected Methods...
//
//=====================================================================

//=====================================================================
//
//      Private Methods...
//
//=====================================================================




