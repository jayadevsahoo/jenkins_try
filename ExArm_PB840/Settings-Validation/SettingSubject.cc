#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//================= C L A S S   D E S C R I P T I O N ================
//@ Class:  SettingSubject - SettingSubject Class.
//---------------------------------------------------------------------
//@ Interface-Description
//  This class -- which is derived off of 'Subject' -- is to be a base
//  class for all classes that can be monitored by 'SettingObserver'
//  instances.  This class defines three methods used within this dynamic
//  monitoring mechanism:  'attachObserver()' and 'detachObserver()' which
//  provide a means for an observer to manage its monitoring of its
//  subjects; and 'notifyAllObservers()' which is called anytime the state
//  of this subject changes, and -- via the observer's "update" methods --
//  notifies all attached observers of the change.
//
//  This class also overrides the protected 'detachFromAllObservers_()'
//  method, which is called when the system is transitioning to and from
//  SST.
//---------------------------------------------------------------------
//@ Rationale
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class has a two-dimensional array of SettingObserver pointers
//  that is used to keep track of which observers are currently monitoring
//  each subject instance.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/SettingSubject.ccv   25.0.4.0   19 Nov 2013 14:27:44   pvcs  $
//
//@ Modification-Log
//
//  Revision: 008   By: gdc    Date: 21-Jan-2009    SCR Number: 6458
//  Project:  840S
//  Description:
//      Modified to use isNewlyApplicable() method to determine if
//		setting's applicability has changed from non-applicable to
//		an applicable state. Resolves problem of setting getting
//		into a changed state and not being able to get out of it when
//		even when the applicability reverts to non-applicable.
//		Corrected updateApplicability() to not set "changed-forced"
//		and instead let isChanged() and isNewlyApplicable() methods
//		handle identifying new applicable settings.
//
//  Revision: 007  By: gdc    Date:  24-Feb-2005    DR Number: 6144
//  Project:  NIV1
//  Description:
//   Changed to support NIV. updateApplicability() changed to set
//   change flag only when previous applicability is NOT_APPLICABLE
//   so settings are not highlighted when applicability changes
//   but setting value does not with the setting button remaining
//   visible.
//
//  Revision: 006  By: sah    Date:  30-Aug-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//	VTPC project-related changes:
//      *  added 'isEnabledValue()' to provide support for the new
//         drop-down menus
//      *  removed unneeded call to 'resetForcedChangeFlag_()' in
//         'updateApplicability()'
//
//  Revision: 005   By: sah   Date:  23-May-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//      PAV project-related changes:
//
//  Revision: 004   By: sah    Date: 19-Jan-2000    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  corrected comments
//
//  Revision: 003   By: sah  Date:  18-Jun-1999    DR Number:  5440
//  Project:  ATC
//  Description:
//      Changed 'resetForcedChangeFlag()' from an inlined method to a
//      virtual method, to allow derived settings to override and change
//      its behavior.
//
//  Revision: 002   By: sah   Date:  09-Jun-1999    DR Number: 5368 & 5388
//  Project:  ATC
//  Description:
//     Eliminated storage of calculated applicability, and corresponding
//     processing with that stored applicability, to ensure proper state
//     of forced-change flag.
//
//  Revision: 001   By: sah   Date:  26-Jan-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//     Created to work with new applicability functionality.
//
//===================================================================

#include "SettingSubject.hh"

//@ Usage-Classes
#include "SafetyPcvSettingValues.hh"
#include "SettingObserver.hh"
#include "ContextMgr.hh"
#include "AdjustedContext.hh"
//@ End-Usage

//@ Code...

//====================================================================
//
//  Public Methods...
//
//====================================================================

//==================== M E T H O D   D E S C R I P T I O N =============
//@ Method: ~SettingSubject()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this SettingSubject.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingSubject::~SettingSubject(void)
{
	CALL_TRACE("~SettingSubject()");
}


//==================== M E T H O D   D E S C R I P T I O N =============
//@ Method:  attachObserver(pObserver, changeId)
//
//@ Interface-Description
//  Attach (register) an observer to monitor the change type, given by
//  'changeId', of this setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (changeId < Notification::NUM_SETTING_CHANGE_TYPES)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	SettingSubject::attachObserver(SettingObserver*                    pObserver,
								   const Notification::SettingChangeId changeId)
{
	CALL_TRACE("attachObserver(pObserver, changeId)");
	AUX_CLASS_PRE_CONDITION((changeId < Notification::NUM_SETTING_CHANGE_TYPES),
							changeId);

	SettingObserver**  arrObserverPtrs = observerTable_[changeId];

	Uint oIdx;
	for ( oIdx = 0u; oIdx < SettingSubject::MAX_OBSERVERS_; oIdx++ )
	{  // $[TI1] -- this path is ALWAYS taken...
		if ( arrObserverPtrs[oIdx] == NULL )
		{  // $[TI1.1] -- found an unused slot...
			observerTable_[changeId][oIdx] = pObserver;
			break;
		}
		else if ( arrObserverPtrs[oIdx] == pObserver )
		{  // $[TI1.2] -- the observer is already attached...
			break;
		}  // $[TI1.3] -- keep looking for an unused slot...
	}

	// make sure an empty slot was found; if not, maybe 'MAX_OBSERVERS_' needs
	// to be increased...
	AUX_CLASS_ASSERTION((oIdx < SettingSubject::MAX_OBSERVERS_),
						((getId() << 16) | oIdx));
}


//==================== M E T H O D   D E S C R I P T I O N =============
//@ Method:  detachObserver(pObserver, changeId)
//
//@ Interface-Description
//  Detach (unregister) an observer that is monitoring a change type,
//  given by 'changeId', of this setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (changeId < Notification::NUM_SETTING_CHANGE_TYPES)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	SettingSubject::detachObserver(const SettingObserver*              pObserver,
								   const Notification::SettingChangeId changeId)
{
	CALL_TRACE("attachObserver(pObserver, changeId)");
	AUX_CLASS_PRE_CONDITION((changeId < Notification::NUM_SETTING_CHANGE_TYPES),
							changeId);

	SettingObserver**  arrObserverPtrs = observerTable_[changeId];

	for ( Uint oIdx = 0u; oIdx < SettingSubject::MAX_OBSERVERS_; oIdx++ )
	{  // $[TI1] -- this path is ALWAYS taken...
		if ( arrObserverPtrs[oIdx] == pObserver )
		{  // $[TI1.1] -- found the 'pObserver' slot...
			observerTable_[changeId][oIdx] = NULL;
			break;
		}  // $[TI1.2] -- keep looking for the 'pObserver' slot...
	}
}


//==================== M E T H O D   D E S C R I P T I O N =============
//@ Method:  notifyAllObservers(qualifierId, changeId)  [const, virtual]
//
//@ Interface-Description
//  Notify all observers that are monitoring this setting's change type
//  that is identified by 'changeId'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (changeId == Notification::VALUE_CHANGED  ||
//   changeId == Notification::APPLICABILITY_CHANGED)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	SettingSubject::notifyAllObservers(
									  const Notification::ChangeQualifier qualifierId,
									  const Notification::SettingChangeId changeId
									  ) const
{
	CALL_TRACE("notifyAllObservers(qualifierId, changeId)");

	SettingObserver::UpdateMethodPtr  updateMethodPtr;

	// get a pointer to the observer method that is to receive the
	// notification...
	switch ( changeId )
	{
		case Notification::VALUE_CHANGED :		  // $[TI1]
			updateMethodPtr = &SettingObserver::valueUpdate;
			break;
		case Notification::APPLICABILITY_CHANGED :	  // $[TI2]
			updateMethodPtr = &SettingObserver::applicabilityUpdate;
			break;
		case Notification::NUM_SETTING_CHANGE_TYPES :
		default :
			AUX_CLASS_ASSERTION_FAILURE((getId() << 16) | changeId);
			break;
	}

	SettingObserver *const *  arrObserverPtrs = observerTable_[changeId];

	for ( Uint oIdx = 0u; oIdx < SettingSubject::MAX_OBSERVERS_; oIdx++ )
	{  // $[TI3] -- this path is ALWAYS taken...
		if ( arrObserverPtrs[oIdx] != NULL )
		{  // $[TI3.1] -- found an observer...
			(arrObserverPtrs[oIdx]->*updateMethodPtr)(qualifierId, this);
		}  // $[TI3.2]
	}
}


//================ M E T H O D   D E S C R I P T I O N ================
//@ Method:  isChanged()  [const, pure virtual]
//
//@ Interface-Description
//  This is a pure virtual method that is to be overridden by derived classes
//  to return a boolean indicating whether this setting has changed from
//  its currently-accepted value.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

//@ Not-Implemented


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  resetForcedChangeFlag()  [virtual]
//
//@ Interface-Description
//  This method RESETS (to 'FALSE') this setting's internal flag that
//  indicates whether a forced change is being initiated (e.g., New-Patient
//  Setup or Transition Rules).
//
//  This is a virtual method to allow those settings whose change-state
//  should NEVER be reset (e.g., time/date settings), to override this
//  method and change its response.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void SettingSubject::resetForcedChangeFlag(void)
{
	CALL_TRACE("resetForcedChangeFlag()");

	forcedChangeFlag_ = FALSE;
}	// $[TI1]


//================ M E T H O D   D E S C R I P T I O N ================
//@ Method:  getApplicability(qualifierId)  [const, pure virtual]
//
//@ Interface-Description
//  This is a pure virtual method that is to be overridden by derived classes
//  to return a boolean indicating whether this setting is currently applicable,
//  based on 'qualifierId'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

//@ Not-Implemented


//================ M E T H O D   D E S C R I P T I O N ================
//@ Method:  getAcceptedValue()  [virtual, const]
//
//@ Interface-Description
//  This is a pure virtual method that is to be overridden by derived classes
//  to return this setting's "accepted" value.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

//@ Not-Implemented


//================ M E T H O D   D E S C R I P T I O N ================
//@ Method:  getAdjustedValue()  [virtual, const]
//
//@ Interface-Description
//  This is a pure virtual method that is to be overridden by derived classes
//  to return this setting's "adjusted" value.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

//@ Not-Implemented


//================ M E T H O D   D E S C R I P T I O N ================
//@ Method:  getSafetyPcvValue()  [const]
//
//@ Interface-Description
//  This method returns the Safety-PCV value of this setting.  This is
//  only applicable to batch settings.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue SettingSubject::getSafetyPcvValue(void) const
{
	CALL_TRACE("getSafetyPcvValue()");
	AUX_CLASS_PRE_CONDITION((SettingId::IsBatchId(getId())), getId());

	return(SafetyPcvSettingValues::GetValue(getId()));
}	// $[TI1]


//================ M E T H O D   D E S C R I P T I O N ================
//@ Method:  getMaxLimit()  [virtual]
//
//@ Interface-Description
//  This is a pure virtual method that is to be overridden by derived classes
//  to return this alarm setting's absolute maximum value.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

//@ Not-Implemented


//================ M E T H O D   D E S C R I P T I O N ================
//@ Method:  getMinLimit()  [virtual]
//
//@ Interface-Description
//  This is a pure virtual method that is to be overridden by derived classes
//  to return this alarm setting's absolute minimum value.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

//@ Not-Implemented


//================ M E T H O D   D E S C R I P T I O N ================
//@ Method:  isEnabledValue(value)  [virtual]
//
//@ Interface-Description
//  This is a pure virtual method that is to be overridden by derived classes
//  to return a boolean as to whether 'value' is a currently-enabled value.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (isDiscreteSetting())
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

//@ Not-Implemented


//================ M E T H O D   D E S C R I P T I O N ================
//@ Method:  activationHappened()  [pure virtual]
//
//@ Interface-Description
//  This is a pure virtual method that is to be overridden by derived classes
//  to respond to the initial activation of this setting's button.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

//@ Not-Implemented


//================ M E T H O D   D E S C R I P T I O N ================
//@ Method:  updateApplicability(qualifierId)  [const]
//
//@ Interface-Description
//  Update this subject's internal applicability state, and issue
//  notifications if there is a change.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (qualifierId < Notification::NUM_CHANGE_QUALIFIERS)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void SettingSubject::updateApplicability( const Notification::ChangeQualifier qualifierId )
{
	CALL_TRACE("updateApplicability(qualifierId)");

	if ( qualifierId == Notification::ACCEPTED )
	{
		// qualifier is ACCEPTED, therefore reset the "forced-change" flag...
		// not sure this is needed
		resetForcedChangeFlag();
	}
	else if ( qualifierId == Notification::ADJUSTED )
	{
		// do nothing -- we used to set the "forcedr-change" flag here for
		// a newly applicable setting, but this flag couldn't be reset when 
		// applicability reverted to the "accepted" applicability without
		// affecting other logic that sets this "forced-change" flag as well.
		// The isChanged() method now compares applicability and returns
		// TRUE for a newly applicable setting instead.
	}
	else
	{
		// unexpected qualifier id...
		AUX_CLASS_ASSERTION_FAILURE(((getId() << 16) | qualifierId));
	}

	// regardless of whether the applicability is changing, notify all
	// observers to process current applicability...
	notifyAllObservers(qualifierId, Notification::APPLICABILITY_CHANGED);
}


//================ M E T H O D   D E S C R I P T I O N ================
//@ Method:  calcNewValue()  [pure virtual]
//
//@ Interface-Description
//  This is a pure virtual method that is to be overridden by derived
//  classes to respond to knob deltas from the operator.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

//@ Not-Implemented


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  overrideSoftBound(boundId)
//
//@ Interface-Description
//  This is a pure virtual method that is to be overridden by derived
//  classes that define soft-bounds.  This method is used to notify this
//  setting to override (disable) the indicated soft bound.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

//@ Not-Implemented


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  resetToStoredValue()
//
//@ Interface-Description
//  This is a pure virtual method that is to be overridden by derived
//  classes to reset this setting's value to what it was upon activation.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

//@ Not-Implemented


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	SettingSubject::SoftFault(const SoftFaultID  softFaultID,
							  const Uint32       lineNumber,
							  const char*        pFileName,
							  const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

	FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION, SETTING_SUBJECT,
							lineNumber, pFileName, pPredicate);
}


//====================================================================
//
//  Protected Methods...
//
//====================================================================

//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method:  SettingSubject(settingId)  [Constructor]
//
//@ Interface-Description
//  Create a default setting subject.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (SettingId::IsId(settingId))
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingSubject::SettingSubject(const SettingId::SettingIdType settingId)
: SETTING_ID_(settingId),
	forcedChangeFlag_(FALSE)
{
	CALL_TRACE("SettingSubject(settingId)");
	AUX_CLASS_PRE_CONDITION((SettingId::IsId(settingId)), settingId);

	initObserverTable_();
}	// $[TI1]


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method:  detachFromAllObservers_()  [virtual, const]
//
//@ Interface-Description
//  Detach this subject from all observers.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	SettingSubject::detachFromAllObservers_(void)
{
	CALL_TRACE("detachFromAllObservers_()");

	for ( Uint cIdx = 0u; cIdx < Notification::NUM_SETTING_CHANGE_TYPES; cIdx++ )
	{  // $[TI1] -- this path is ALWAYS taken...
		for ( Uint oIdx = 0u; oIdx < MAX_OBSERVERS_; oIdx++ )
		{  // $[TI1.1] -- this path is ALWAYS taken...
			SettingObserver*  pObserver = observerTable_[cIdx][oIdx];

			if ( pObserver != NULL  &&  !pObserver->doRetainAttachment(this) )
			{  // $[TI1.1.1] -- found a non-NULL pointer, NOT to be retained...
				observerTable_[cIdx][oIdx] = NULL;
			}  // $[TI1.1.2] -- found a NULL pointer, or one to be retained...
		}
	}
}


//====================================================================
//
//  Private Methods...
//
//====================================================================

//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method:  initObserverTable_()  [virtual, const]
//
//@ Interface-Description
//  Initialize this instance's observer table to all 'NULL' pointers.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	SettingSubject::initObserverTable_(void)
{
	CALL_TRACE("initObserverTable_()");

	for ( Uint cIdx = 0u; cIdx < Notification::NUM_SETTING_CHANGE_TYPES; cIdx++ )
	{
		for ( Uint oIdx = 0u; oIdx < MAX_OBSERVERS_; oIdx++ )
		{
			observerTable_[cIdx][oIdx] = NULL;
		}
	}
}	// $[TI1]
