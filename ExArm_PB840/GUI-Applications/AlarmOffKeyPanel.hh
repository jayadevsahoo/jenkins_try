#ifndef AlarmOffKeyPanel_HH
#define AlarmOffKeyPanel_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: AlarmOffKeyPanel - Alarm offkey panel class which allows
// the viewing of alarm silence progress status.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/AlarmOffKeyPanel.hhv   25.0.4.0   19 Nov 2013 14:07:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah	Date: 10-Apr-2000   DCS Number:  5683
//  Project:  NeoMode
//  Description:
//      As part of this DCS, removed unneeded parameters from 'activatePanel()',
//      to go along with the removal of the corresponding unneeded strings.
//
//  Revision: 001  By:  yyy    Date:  12-Jul-99    DR Number: 5327
//       Project:  NeoMode (R8027)
//       Description:
//             Integration baseline.
//	(hhd) 29-Nov-99	Modified to fix bugs.
//====================================================================

#include "OffKeyPanel.hh"

//@ Usage-Classes
#include "Box.hh"
//@ End-Usage

class AlarmOffKeyPanel : public OffKeyPanel
{
public:
	AlarmOffKeyPanel(Uint32 xOrg, Uint32 yOrg, Uint32 width, Uint32 height,
				Uint16 numButtons, StringId panelTitle,
				SubScreen *pFocusSubScreen, EventData::EventId eventId,
				UserEventCallbackPtr pEventCallback=NULL);
    ~AlarmOffKeyPanel(void);

	void activatePanel(StringId msg, Boolean displayCancelButton,
						Int32 elapseTime, Boolean mustRedraw);
	virtual void deactivatePanel(void);

	void showPanel(Boolean redraw);

    static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL, 
			  const char*       pPredicate = NULL);
  
protected:

private:
    // these methods are purposely declared, but not implemented...
    AlarmOffKeyPanel(const AlarmOffKeyPanel&);				// not implemented...
    void   operator=(const AlarmOffKeyPanel&);		// not implemented...

	//@ Data-Member: timeProgressBox_
	// The colored box for which the width of the box represents the passed
	// time period of alarm silence.
	Box timeProgressBox_;

	//@ Data-Member: timeBox_
	// The colored box for which the width of the box represents the duration of
	// alarm silence.
	Box timeBox_;

	//@ Data-Member: timeStatus_
	// The text field which informs users of the unusual time status.
	TextField timeStatusMsg_;
};


#endif // AlarmOffKeyPanel_HH
