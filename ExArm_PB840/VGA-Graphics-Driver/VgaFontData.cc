#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1994-1995, Puritan-Bennett Corporation
//=====================================================================  

// =========================== M O D U L E   D E S C R I P T I O N ====
//@ Class: VgaFontData - Manager of the VGA font tables.
//
//@ Interface-Description
//  This class manages the table of font entries. Each font in included
//  and initialized in the font table and off-screen memory.
//---------------------------------------------------------------------
//@ Rationale
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[00456] -- the font set shall be a "Sans Serif" type font
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ $Header:   /840/Baseline/VGA-Graphics-Driver/vcssrc/VgaFontData.ccv   25.0.4.0   19 Nov 2013 14:37:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 012   By: mnr      Date:  20-Aug-2007    SCR Number: 6434 
//  Project: Trend4
//  Description:
//	Updates to allow 4th (D) set of Japanese font files. 
//
//  Revision: 011   By: mnr      Date:  16-July-2007    SCR Number: 6434 
//  Project: Trend4
//  Description:
//	Added missing declaration to fix Japanese build compilation issue.
//
//  Revision: 010   By: gdc      Date:  03-Jun-2007    SCR Number: 6237 
//  Project: Trend
//  Description:
//	Provided font metrics through FontMetrics struct for improved text 
//  positioning.
//
//  Revision: 009   By: Boris Kuznetsov    Date:  03-May-2000    SCR Number:6036 
//  Project: Russian
//  Description:
//	    Added Alternate character matrix for Russian characters
//
//  Revision: 008   By: S. Peters    Date:  16-Oct-2000    DCS Number:6092 
//  Project: Polish
//  Description:
//	    Added Alternate character matrix for Polish characters
//
//  Revision: 007   By: gdc    Date:  28-Aug-2000    DCS Number: 5493
//  Project: GuiComms
//  Description:
//	Implemented single screen compatibility.
//
//  Revision: 006   By: gdc    Date:  27-Jan-2000    DCS Number: 5337
//  Project: 840 Neonatal
//  Description:
//	Optimized VGA library for Neonatal project to use BitBlt and Windows
//  Bitmap structures.
//
//  Revision: 005   By: clw    Date:  28-May-1998    DCS Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//      Added testable item markers. Added conditional compilation to avoid unneeded
//      memory use when language is not japanese.
//
//  Revision: 004   By: clw    Date:  13-May-1998    DCS Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//      Extended support for Japanese to handle 3 font sets for each point size of Japanese.
//
//  Revision: 003   By: clw    Date:  14-Mar-1998    DCS Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//      Corrected bug in setup of FontTable.
//
//  Revision: 002   By: clw    Date:  06-Mar-1998    DCS Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//      Added prelinary support for Japanese.  The FontTable is now set up in
//      the Initialize method instead of in an initializer.  The indexes are now
//      from the FontIndex enum instead of based on position.  The FontTable is
//      checked to ensure that no entries are skipped.
//
//  Revision: 001   By: sah    Date:  01-Aug-1997    DCS Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//      Initial version to coding standards (Integration baseline).
//
//=====================================================================  
#include "VgaFontData.hh"
#include "VgaRectangle.hh"
#include "OsUtil.hh"      
// TODO E600 removed
//#include "Ostream.hh"

//@ Usage-Classes
#include "DisplayContext.hh"


#if defined (SIGMA_POLISH) || defined (SIGMA_RUSSIAN)
	#define VGA_ALTERNATE_FONT 1   /* allow different languages to use the Alternate labels
                                 and the 'A' format specifier in the LA files. 
                                 if flag is not set and a 'A' specifier is used 
                                 the program will assert     */
#endif


//@ End-Usage


//=====================================================================
//
//  Static Member Definitions...
//
//=====================================================================
CFont*  VgaFontData::FontTable[FONT_COUNT];
CDC* VgaFontData::m_screenCDC = NULL;
VgaFontData::FontMetricsEntry  VgaFontData::FontMetrics[FONT_COUNT];

// This indicates how many sets of 256-character fonts are allowed for a
// given point size of Japanese characters.
const Uint16 VgaFontData::MAX_JAPANESE_FONTS_PER_PT_SIZE = 4;

#define CREATE_FONT(type, size) \
	do { \
		int index = type##size##_INDEX; \
		CFont* font = new CFont(); \
		ConfigureFont(*font, size, type); \
		SetFontMetrics(index);\
		FontTable[index] = font; \
	} while(false)

#define CREATE_ALL_FONTS(size) \
	do { \
		CREATE_FONT(NORMAL_, size); \
		CREATE_FONT(THIN_, size); \
		CREATE_FONT(ITALIC_, size); \
	}while(false)

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  Initialize
//
//@ Interface-Description
//  This method is to be called at every startup, and ensures the
//  integrity of the font table, and its fonts.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Explicitly set up the FontTable array, using indexes from the FontIndex
//  enum.
//  Skipping over the default font (index zero), make sure that every
//  character, in every font, is located in the proper position.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void VgaFontData::Initialize(void)
{
#if defined(SIGMA_GUI_CPU)
	//Load the font.. THis si required only on target build
	AddFontResource(_T("\\FlashFX Disk\\ProgramFiles\\Viking.ttf"));
#endif
	// First, initialize the FontTable to all NULL.  This will allow
	// us to verify below that all entries (except #0) were set up.
	Uint32 fontIdx = 0;
	for (fontIdx = 0; fontIdx < VgaFontData::FIRST_JAPANESE_FONT_INDEX; fontIdx++)
	{
		FontTable[fontIdx] = NULL;
	}

	//Initialize the screen CDC.. Note that this is a "copy" of the 
	//actual screen context. Making any changes on this wont impact what
	//is displayed on screen.
	m_screenCDC = new CDC;
	if(m_screenCDC->CreateCompatibleDC(NULL) == 0)
	{
		//error creating CDC.. This API doesnt return any
		//status (legacy behavior). For now, assert.
		//TODO: E600 Revisit and introduce a return value.
		CLASS_ASSERTION(false);
	}


	// Set up the FontTable, using indexes from enum FontIndex:
	//    Slot #0 is currently NULL.  Reserved for default font, if needed...

	CREATE_ALL_FONTS(6);
	CREATE_ALL_FONTS(8);
	CREATE_ALL_FONTS(10);
	CREATE_ALL_FONTS(12);
	CREATE_ALL_FONTS(14);
	CREATE_ALL_FONTS(16);
	CREATE_ALL_FONTS(18);
	CREATE_ALL_FONTS(24);
	CREATE_ALL_FONTS(32);
	CREATE_ALL_FONTS(60);

	//TODO: BVP - E600 Need to figure out if we need to do something special
	//for Japanese. At the minimum we have to fill in the  Fonts array for 
	//Japanese fonts. For now, accessing Japanese font will result in crash.

}




//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getAlternateFontId(fontId)
//      [static]
//
//@ Interface-Description
//  This method is called when useing the alternate font set.
//  When passed in an 'regular' Font Id it will return the corresponding
//  alternat font id.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Add an offset to the font id to calculate the alternate font id.
//  The assumpting this routing make is that the alternate font set id's
//  follow the regular id in the enumeration declaration.
//---------------------------------------------------------------------
//@ PreCondition
//  font id must be in the regular fonr id range.
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
Int16 VgaFontData::getAlternateFontId(Int16 fontId)
{
#if defined  VGA_ALTERNATE_FONT



	CLASS_ASSERTION(FIRST_ALTERNATE_FONT_INDEX > fontId);

	// Program Note: Fonts 32 and 60 where not defined for initial release of Polish
	CLASS_ASSERTION((ALTERNATE_ITALIC_24_INDEX + 1) > (fontId + (FIRST_ALTERNATE_FONT_INDEX - 1)))


	return(fontId + (FIRST_ALTERNATE_FONT_INDEX - 1) );
#else
	// This function should not have been called unless VGA_ALTERNATE_Font
	// has been defined.
	CLASS_ASSERTION(FALSE);
	return fontId;

#endif

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TextRect
//---------------------------------------------------------------------
//@ Interface-Description
//  Determine the bounding rectangle of a string as it will be displayed on
//  the screen, in screen coordinates.  No negative values produced.  Using
//  the passed starting text position ('xPos' and 'yPos'), font ID ('fontId'),
//  and character string ('pString'), this method will return (in 'pRect')
//  the smallest rectangle that will contain the entire string.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Does not consider clipping parameters.
//  Clipping can be done by sending the rectangle through the 
//  VgaRectangle::FillBox() routine.
//---------------------------------------------------------------------
//@ PreCondition
//  (fontId > 0  &&  fontId < VgaFontData::FIRST_JAPANESE_FONT_INDEX)
//  NOTE: FIRST_JAPANESE_FONT_INDEX is equivalent to the number of non-Japanese fonts.
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
void
VgaFontData::TextRect(Int xPos, Int yPos, Uint fontId,
		  const wchar_t* pString, VgaRect *pRect)
{
	AUX_CLASS_PRE_CONDITION((fontId > 0  &&
			   fontId < VgaFontData::FIRST_JAPANESE_FONT_INDEX), fontId);
	//Set the requested font on the CDC before estimating the
	//size required.
	CFont* font = FontTable[fontId];
	m_screenCDC->SelectObject(font);
	//Check the size required to display the text
	pRect->x0 = xPos;
	//TODO E600 - Formula for y0 is derived by considering yPos as Baseline; hence ascent is subtracted from it, 
	//as ascent part is above the baseline.
	pRect->y0 = yPos-FontMetrics[fontId].ascent; 
	CSize size = m_screenCDC->GetTextExtent(pString, wcslen(pString));
	pRect->x1 = xPos+size.cx;
	//TODO E600 - Formula for y1 is derived by considering yPos as Baseline; hence descent is added from it, 
	//as descent part is below the baseline
	pRect->y1 = yPos+FontMetrics[fontId].descent;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void VgaFontData::SoftFault(const SoftFaultID softFaultID,
							const Uint32      lineNumber,
							const char*       pFileName,
							const char*       pPredicate)
{
	FaultHandler::SoftFault(softFaultID, VGA_GRAPHICS_DRIVERS,
							VgaGraphicsDriver::VGA_FONT_DATA_ID, 
							lineNumber, pFileName, pPredicate);
}



//=====================================================================
//
//  Private Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ConfigureFont [private]
//
//@ Interface-Description
//  This method called during VgaFontData initialization. It configures
// the input CFont with the specified attributes.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses the CFont APIs to configure the font with the specified 
// attributes. This is just a utility method solely aimed at reducing  
// code replication.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void VgaFontData::ConfigureFont(CFont& font, int size, FontInternalType_ type)
{
	LOGFONT lf;

	// clear out structure.
	memset(&lf, 0, sizeof(LOGFONT));

	//Set the font size.. it is in tenths of a point (i.e. 12-size font should
	//set this parameter to 120)
	lf.lfHeight = size*10;

	//Enable anti-aliasing
	lf.lfQuality = ANTIALIASED_QUALITY;

#if defined(SIGMA_GUI_CPU)
	//TODO E600 - The Viking font seems to have what is called SHIFTJIS_CHARSET.
	//According to MSDN doc, it specifies the Japanese character set. Not sure why
	//it is set so, but until we figure out we need to set the character set selected
	//that way to use Viking font on the vent
	lf.lfCharSet = SHIFTJIS_CHARSET;
#endif

	// request a face name "Viking".
	wcsncpy(lf.lfFaceName, L"viking", wcslen(L"viking"));

	switch(type)
	{
	case NORMAL_:
		lf.lfWeight = FW_NORMAL;
		lf.lfItalic = FALSE;
		break;
	case THIN_:
		lf.lfWeight = FW_THIN;
		lf.lfItalic = FALSE;
		break;
	case ITALIC_:
		//For italic, use regular (not thin) weight
		lf.lfWeight = FW_NORMAL;
		lf.lfItalic = TRUE;
		break;
	default:
		//use the NORMAL font
		lf.lfWeight = FW_NORMAL;
		lf.lfItalic = FALSE;
		break;
	};


	//Initialize the fontthe font
	font.CreatePointFontIndirect(&lf, m_screenCDC);   

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SetFontMetrics [private]
//
//@ Interface-Description
//  This method is called during VgaFontData initialization. It reads the 
//  Windows font metrices and store the required attributes.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses the CDC APIs to get Windows font metrices with the specified 
//  attributes. 
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void VgaFontData::SetFontMetrics(int fontIdx)
{
	TEXTMETRIC lpMetrics;
	//Selecting the font for which Metrics is to be obtained.
	CFont* font = FontTable[fontIdx];
	m_screenCDC->SelectObject(font);
	m_screenCDC->GetTextMetrics(&lpMetrics);

	
	Int32 maxAscent = 0;
	Int32 maxDescent = 0;
	Int32 hotspotY = 0;
	
	//TODO E600 - "hotspotY" is 840 perameter used for the calucation for dropdown lists.
	//In 840 "hotspotY" was read from the old Font files in 840 s/w system. Now, these font files do not exist.
	//On Windows font metrics do not have equivalent parameter for it.
	//As of now by implementing below, DropdownSetting buttons are functioning properly on the ventilator GUI as well as on GUIPC
#ifdef SIGMA_GUIPC_CPU
	hotspotY = 2;
#else
	hotspotY = (lpMetrics.tmExternalLeading+lpMetrics.tmInternalLeading)+3;
#endif

	maxAscent = MAX_VALUE(maxAscent,lpMetrics.tmHeight +hotspotY);
	maxDescent = MAX_VALUE(maxDescent,-hotspotY);

	// initialize the font metrics
	FontMetrics[fontIdx].ascent = lpMetrics.tmAscent;
	FontMetrics[fontIdx].descent = lpMetrics.tmDescent;
	FontMetrics[fontIdx].width = lpMetrics.tmWeight;
	FontMetrics[fontIdx].height = lpMetrics.tmHeight;
	FontMetrics[fontIdx].maxAscent = maxAscent;
	FontMetrics[fontIdx].maxDescent = maxDescent;

}
