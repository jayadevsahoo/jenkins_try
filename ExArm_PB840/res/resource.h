//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by ExArm.rc
//
#define IDS_NEW                         65000
#define IDS_FILE                        65001
#define IDS_HELP                        65002
#define IDS_SAVE                        65003
#define IDS_CUT                         65004
#define IDS_COPY                        65005
#define IDS_PASTE                       65006
#define IDS_ABOUT                       65007



#define IDR_HIGH_ALARM                  65008
#define IDR_MEDIUM_ALARM                65009
#define IDR_LOW_ALARM                   65010


#define IDR_PRESS_WAVE                         65011
#define IDR_REJECT_WAVE                        65012
#define IDR_RELEASE_WAVE                       65013
#define IDR_REMIND_WAVE                        65014
#define IDR_ROTARY_WAVE                        65015
#define IDR_ACCEPT_WAVE                        65016
#define IDR_VOLUME_ALARM                       65017

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        133
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
