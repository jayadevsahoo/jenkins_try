#if defined FAKE_SM
// This class is used only for test simulation.

#ifndef SDTimerRegistrar_HH
#define SDTimerRegistrar_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1994, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: SDTimerRegistrar - The central registration, control, and
// dispatch point for all Service Data timer events.  The timer events are
// received from the OS-Foundation subsystem via the GUI task's User
// Annunciation queue.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Data/vcssrc/SDTimerRegistrar.hhv   25.0.4.0   19 Nov 2013 14:21:02   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By:  gdc    Date:  12-Dec-2008    SCR Number: 6068
//  Project:  840S
//  Description:
//		Removed SUN prototype code.
//
//  Revision: 002  By: sah     Date:  15-Jan-99   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' to a non-inlined method.
//
//  Revision: 001  By:  yyy    Date:  16-May-95   DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//====================================================================

#include "Sigma.hh"
#include "ServiceDataClassIds.hh"
#include "SDTimerRegistrar.hh"
#include "UserAnnunciationMsg.hh"
#include "SDTimerId.hh"
#include "MsgTimer.hh"

class SDTimerTarget;


class SDTimerRegistrar
{
public:
	//
	// SDTimerRecord -- private structure which defines elements
	// of TimerArray_[]
	//
	struct SDTimerRecord_
	{
		SDTimerRecord_();
		MsgTimer 		msgTimer;	// timer object provided by Operating-Systems	
		SDTimerTarget*	pTarget;	// ptr to callback object, if any
		Boolean			isActive;	// TRUE when this is an active timer
	};

	static void TimerEventHappened(UserAnnunciationMsg &eventMsg);
	static void RegisterTarget(SDTimerId::SDTimerIdType timerId,
							   SDTimerTarget* pTarget);
	static void StartTimer(SDTimerId::SDTimerIdType timerId);
	static void StartTimer(SDTimerId::SDTimerIdType timerId,
						   SDTimerTarget* pTarget);
	static void CancelTimer(SDTimerId::SDTimerIdType timerId);
	static void Initialize(void);

	static void SoftFault(const SoftFaultID softFaultID,
								 const Uint32      lineNumber,
								 const char*       pFileName  = NULL, 
								 const char*       pPredicate = NULL);

protected:

private:
	// these methods are purposely declared, but not implemented...
	SDTimerRegistrar(void);						// not implemented...
	SDTimerRegistrar(const SDTimerRegistrar&);	// not implemented...
	~SDTimerRegistrar(void);						// not implemented...
	void operator=(const SDTimerRegistrar&);		// not implemented...

	//@ Data-Member: TimerArray_
	// A static array of SDTimerRecord_'s, indexed by the
	// SDTimerId::SDTimerIdType enum.  Each array element contains a MsgTimer
	// object, a pointer to a registered callback object and a flag showing
	// whether the timer is currently active.
	static SDTimerRecord_* TimerArray_;
};

typedef SDTimerRegistrar::SDTimerRecord_ SDTimerRecordInRegistrar;


#endif // SDTimerRegistrar_HH 


#endif	// FAKE_SM
