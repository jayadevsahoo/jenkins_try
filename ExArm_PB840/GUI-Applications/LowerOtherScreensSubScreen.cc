#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: LowerOtherScreensSubScreen - The subscreen selected by the
// Other Screens button on the Lower screen.  Allows access to the
// Communications Setup, Date/Time Settings and More Settings subscreens.
//---------------------------------------------------------------------
//@ Interface-Description
// All Lower Screen subscreens during normal ventilation excluding Vent Setup,
// Apnea Setup and Alarm Setup are accessed via this subscreen.  The subscreen
// displays navigation buttons, one for each destination subscreen.  Currently
// there are three buttons, one for each of the Communications Setup, Date/Time
// Settings and More Settings subscreens.
//
// This class collaborates with the LowerSubScreenArea to activate the
// subscreens associated with the screen select buttons contained in this
// subscreen.
//
// The activate()/deactivate() methods are called automatically on
// display/removal of this subscreen by our subscreen area owner.
// The buttonDownHappened() method is called when any button in
// the subscreen is pressed down allowing us to activate its associated
// subscreen.
//---------------------------------------------------------------------
//@ Rationale
// Used to allow access to lower screen subscreens which do not have a
// dedicated subscreen button in the lower screen select area.  Allows
// addition of an arbitrary number of lower subscreens for future expansion.
//---------------------------------------------------------------------
//@ Implementation-Description
// In the constructor we create and display one button for every
// subscreen for which access is required.  At this time we register
// for button press event callbacks.  When any button is pressed down
// our buttonPressDownHappened() method is called and this function
// then determines which subscreen should be activated (depending on
// which button was pressed down).  Activation of the new subscreen
// will automatically deactivate this subscreen but our screen select button
// will remain selected.
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and pre-conditions
// to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
// Only one instance of this class should be created (in LowerSubScreenArea).
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/LowerOtherScreensSubScreen.ccv   25.0.4.0   19 Nov 2013 14:08:04   pvcs  $
//
//@ Modification-Log
//
//  Revision: 009   By: gdc   Date: 07-Feb-2011   SCR Number: 6733
//  Project:  PROX
//  Description:
//      Added VCO2 study code for development reference.
// 
//  Revision: 008   By: rhj   Date: 02-Apr-2010   SCR Number: 6436
//  Project:  PROX
//  Description:
//      Modified to display PROX setup subscreen when the prox 
//      board is installed and neonatal is the current vent's circuit type.
// 
//  Revision: 007   By: rhj   Date: 17-Feb-2010   SCR Number: 6436
//  Project:  PROX
//  Description:
//      Added a prox setup subscreen.    
//   
//  Revision: 006   By:   rhj    Date: 07-July-2008   SCR Number: 6435
//  Project:  LEAK
//  Description:
//      Modified to support Leak Compensation.
//       
//  Revision: 005   By: rhj    Date:  19-Sept-2006    DCS Number: 
//  Project: RESPM
//  Description:
//      Add the ability to disable or enable RESPM.
//
//  Revision: 004   By: hhd    Date:  16-Nov-1999    DCS Number: 5327
//  Project: Neo-Mode
//  Description:
// 		Modified as part of a general cleanup effort for Neo-Mode.
//
//  Revision: 003  By: yyy      Date: 05-Jan-1998  DR Number: 
//    Project:  BiLevel (R8027)
//    Description:
//     Initial release for BiLevel adding software option.
//     Merged /840/Baseline/GUI-Applications/vcssrc/LowerOtherScreensSubScreen.ccv   1.10.1.0   07/30/98 10:14:38   gdc
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "LowerOtherScreensSubScreen.hh"

//@ Usage-Classes
#include "MiscStrs.hh"
#include "AdjustPanel.hh"
#include "BaseContainer.hh"
#include "LowerScreen.hh"
#include "MoreSettingsSubScreen.hh"
#include "DateTimeSettingsSubScreen.hh"
#include "CommSetupSubScreen.hh"
#include "NifSubScreen.hh"
#include "P100SubScreen.hh"
#include "VitalCapacitySubScreen.hh"
#include "BdGuiEvent.hh"
#include "EventData.hh"
#include "SoftwareOptions.hh"
#include "Sound.hh"
#include "SettingContextHandle.hh"
#include "PatientCctTypeValue.hh"
#include "VentTypeValue.hh"
#include "ProxSetupSubScreen.hh"
//@ End-Usage

//@ Code...

// Initialize static constants.
static const Int32 BUTTON_WIDTH_ = 140;
static const Int32 BUTTON_HEIGHT_ = 46;
static const Int32 BUTTON_BORDER_ = 3;
static const Int32 BUTTON_X2_ = ( ::SUB_SCREEN_AREA_WIDTH - BUTTON_WIDTH_ ) / 2 - 1;
static const Int32 BUTTON_X1_ = BUTTON_X2_ - BUTTON_WIDTH_ - 10;
static const Int32 BUTTON_X3_ = BUTTON_X2_ + BUTTON_WIDTH_ + 10;
static const Int32 BUTTON_Y1_ = 50;
static const Int32 RESP_MECH_BOX_WIDTH_ = 490;
static const Int32 RESP_MECH_BOX_HEIGHT_ = 108;
static const Int32 RESP_MECH_BOX_X_ = ( ::SUB_SCREEN_AREA_WIDTH - RESP_MECH_BOX_WIDTH_ ) / 2 - 1;
static const Int32 RESP_MECH_BOX_Y_ = 120;
static const Int32 RESP_MECH_TITLE_Y_ = RESP_MECH_BOX_Y_ + 2;
static const Int32 BUTTON_Y2_ = RESP_MECH_BOX_Y_ + 30;
static const Int32 RESP_MECH_NOT_AVAIL_MSG_Y_ = BUTTON_Y2_ + BUTTON_HEIGHT_ + 5;

static const Int32 BUTTON_ROW1_X1_ = 20;
static const Int32 BUTTON_ROW1_X2_ = BUTTON_ROW1_X1_ + BUTTON_WIDTH_ + 10;
static const Int32 BUTTON_ROW1_X3_ = BUTTON_ROW1_X2_ + BUTTON_WIDTH_ + 10;

static const Int32 BUTTON_ROW1_X4_ = BUTTON_ROW1_X3_ + BUTTON_WIDTH_ + 10 ;

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: LowerOtherScreensSubScreen()  [Constructor]
//
//@ Interface-Description
// Constructs this subscreen.  Sizes the subscreen and adds the
// required graphics to the displayable container.  No parameters
// needed.
//---------------------------------------------------------------------
//@ Implementation-Description
// Create the moreSettingsButton_ and titleArea_, size the container
// and add the two drawables.  Also, register for callbacks from the
// more settings button.
//
// $[01115] This subscreen provides access to More Settings subscreen.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

LowerOtherScreensSubScreen::LowerOtherScreensSubScreen(SubScreenArea *pSubScreenArea) :
	SubScreen(pSubScreenArea),
	commSetupButton_(BUTTON_X1_, BUTTON_Y1_, BUTTON_WIDTH_,
				BUTTON_HEIGHT_, Button::LIGHT, BUTTON_BORDER_,
				Button::SQUARE, Button::NO_BORDER,
				MiscStrs::COMM_SETUP_BUTTON_TITLE),
	dateTimeSettingsButton_(BUTTON_X2_, BUTTON_Y1_, BUTTON_WIDTH_,
				BUTTON_HEIGHT_, Button::LIGHT, BUTTON_BORDER_,
				Button::SQUARE, Button::NO_BORDER,
				MiscStrs::DATE_TIME_SETTINGS_BUTTON_TITLE),
	moreSettingsButton_(BUTTON_X3_, BUTTON_Y1_, BUTTON_WIDTH_,
				BUTTON_HEIGHT_, Button::LIGHT, BUTTON_BORDER_,
				Button::SQUARE, Button::NO_BORDER,
				MiscStrs::MORE_SETTINGS_BUTTON_TITLE),
	titleArea_(MiscStrs::OTHER_SCREENS_TITLE, SubScreenTitleArea::SSTA_CENTER),

    respMechTitle_(MiscStrs::RESP_MECH_TITLE),
    respMechBox_(RESP_MECH_BOX_X_, RESP_MECH_BOX_Y_, RESP_MECH_BOX_WIDTH_, RESP_MECH_BOX_HEIGHT_ ),
    respMechNotAvailMsg_(MiscStrs::RM_NOT_AVAILABLE_SMALL_MSG),
    nifManeuverButton_(BUTTON_X1_, BUTTON_Y2_, BUTTON_WIDTH_,
                BUTTON_HEIGHT_, Button::LIGHT, BUTTON_BORDER_,
                Button::SQUARE, Button::NO_BORDER,
                MiscStrs::NIF_MANEUVER_BUTTON_TITLE),
    p100ManeuverButton_(BUTTON_X2_, BUTTON_Y2_, BUTTON_WIDTH_,
                BUTTON_HEIGHT_, Button::LIGHT, BUTTON_BORDER_,
                Button::SQUARE, Button::NO_BORDER,
                MiscStrs::P100_MANEUVER_BUTTON_TITLE),
    svcManeuverButton_(BUTTON_X3_, BUTTON_Y2_, BUTTON_WIDTH_,
                BUTTON_HEIGHT_, Button::LIGHT, BUTTON_BORDER_,
                Button::SQUARE, Button::NO_BORDER,
                MiscStrs::VITAL_CAPACITY_MANEUVER_BUTTON_TITLE),
    proxSetupButton_(BUTTON_ROW1_X4_, BUTTON_Y1_, BUTTON_WIDTH_,
                BUTTON_HEIGHT_, Button::LIGHT, BUTTON_BORDER_,
                Button::SQUARE, Button::NO_BORDER,
                MiscStrs::PROX_SETUP_BUTTON_TITLE)
{
	CALL_TRACE("LowerOtherScreensSubScreen::LowerOtherScreensSubScreen(SubScreenArea "
													"*pSubScreenArea)");

	// Size and position the sub-screen
	setX(0);
	setY(0);
	setWidth(LOWER_SUB_SCREEN_AREA_WIDTH);
	setHeight(LOWER_SUB_SCREEN_AREA_HEIGHT);

	// Register for callbacks to More Settings button
	commSetupButton_.setButtonCallback(this);
	dateTimeSettingsButton_.setButtonCallback(this);
	moreSettingsButton_.setButtonCallback(this);
    nifManeuverButton_.setButtonCallback(this);
    p100ManeuverButton_.setButtonCallback(this);
    svcManeuverButton_.setButtonCallback(this);
    proxSetupButton_.setButtonCallback(this); 

	// Add the title area and button
	addDrawable(&titleArea_);
	addDrawable(&commSetupButton_);
	addDrawable(&dateTimeSettingsButton_);
	addDrawable(&moreSettingsButton_);

    respMechTitle_.setX( ( ::SUB_SCREEN_AREA_WIDTH - respMechTitle_.getWidth() ) / 2 - 1 );
    respMechTitle_.setY( RESP_MECH_TITLE_Y_ );
    respMechTitle_.setColor(Colors::WHITE);
    respMechBox_.setColor(Colors::WHITE);

    respMechNotAvailMsg_.setColor(Colors::WHITE);
    respMechNotAvailMsg_.setX( ( ::SUB_SCREEN_AREA_WIDTH - respMechNotAvailMsg_.getWidth() ) / 2 - 1 );
    respMechNotAvailMsg_.setY( RESP_MECH_NOT_AVAIL_MSG_Y_ );

	// Display "L" with the more setting button when leak compensation 
	// option is enabled. $[LC01012]
	if(SoftwareOptions::IsOptionEnabled(SoftwareOptions::LEAK_COMP))
	{
		moreSettingsButton_.setTitleText(MiscStrs::MORE_SETTINGS_BUTTON_L_TITLE, GRAVITY_LEFT);
	}
	// $[TI1]

	// Register for PROX INSTALLED Event Data 
	BdEventRegistrar::RegisterTarget(EventData::PROX_INSTALLED, this);

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~LowerOtherScreensSubScreen  [Destructor]
//
//@ Interface-Description
// Destroys the LowerOtherScreensSubScreen.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

LowerOtherScreensSubScreen::~LowerOtherScreensSubScreen(void)
{
	CALL_TRACE("LowerOtherScreensSubScreen::~LowerOtherScreensSubScreen(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activate [Virtual]
//
//@ Interface-Description
// Called by the LowerSubScreenArea just before this subscreen is
// displayed on-screen. 
//---------------------------------------------------------------------
//@ Implementation-Description
//  Checks the vent type, mode, circuit type and RM option to determine
//  if RM is available.
//  $[RM12101] $[RM12200] $[RM12300] 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
LowerOtherScreensSubScreen::activate(void)
{
	CALL_TRACE("LowerOtherScreensSubScreen::activate(void)");

    // Retrieve Vent Type, mode and circuit type
    const DiscreteValue  MODE = 
            SettingContextHandle::GetSettingValue(
                                    ContextId::ACCEPTED_CONTEXT_ID,
                                    SettingId::MODE);
    const DiscreteValue  VENT_TYPE = 
            SettingContextHandle::GetSettingValue(
                                    ContextId::ACCEPTED_CONTEXT_ID,
                                    SettingId::VENT_TYPE);

    const DiscreteValue  CIRCUIT_TYPE = 
            SettingContextHandle::GetSettingValue(
                                    ContextId::ACCEPTED_CONTEXT_ID,
                                    SettingId::PATIENT_CCT_TYPE);

    // If the RESPM Option is disabled, we do not display 
    // RESPM.  If RESPM is enabled and and the vent is not 
    // in NIV, BILEVEL nor NEONATAL, we display RESPM.
    // If RESPM is enabled but the vent is either in NIV,
    // BILEVEL, or NEONATAL, then we make the RESPM Buttons Flat,
    // and display a "Not available message" on the bottom of the RESPM Buttons.
    // $[RM01001] 
    if (SoftwareOptions::IsOptionEnabled(SoftwareOptions::RESP_MECH) )
    {
        addDrawable(&respMechBox_);
        addDrawable(&nifManeuverButton_);
        addDrawable(&p100ManeuverButton_);
        addDrawable(&svcManeuverButton_);
        addDrawable(&respMechBox_);
        addDrawable(&respMechTitle_);

        if( (MODE       == ModeValue::BILEVEL_MODE_VALUE)          ||
            (CIRCUIT_TYPE == PatientCctTypeValue::NEONATAL_CIRCUIT) ||
            (VENT_TYPE    == VentTypeValue::NIV_VENT_TYPE) )
        {
             nifManeuverButton_.setToFlat();
             p100ManeuverButton_.setToFlat();
             svcManeuverButton_.setToFlat();
             addDrawable(&respMechNotAvailMsg_);
        }
        else
        {
            nifManeuverButton_.setToUp();
            p100ManeuverButton_.setToUp();
            svcManeuverButton_.setToUp();
            removeDrawable(&respMechNotAvailMsg_);
            // Set Primary prompt to "To make a selection touch a button."
            GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY,
                        PromptArea::PA_LOW, PromptStrs::RM_LOWER_OTHER_SCREENS_P);

            // Set Secondary prompt to tell user to press OTHER SCREENS button
            // to cancel
            GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
                        PromptArea::PA_LOW, PromptStrs::OTHER_SCREENS_CANCEL_S);


        }

    }
    else
    {
        removeDrawable(&respMechBox_);
        removeDrawable(&nifManeuverButton_);
        removeDrawable(&p100ManeuverButton_);
        removeDrawable(&svcManeuverButton_);
        removeDrawable(&respMechBox_);
        removeDrawable(&respMechTitle_);
        removeDrawable(&respMechNotAvailMsg_);

    }


}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: deactivate
//
//@ Interface-Description
// Called by the LowerSubScreenArea when this subscreen is being removed from
// the screen. 
//---------------------------------------------------------------------
//@ Implementation-Description
// Clears any prompts which may have been displayed.  
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
LowerOtherScreensSubScreen::deactivate(void)
{
	CALL_TRACE("LowerOtherScreensSubScreen::deactivate(void)");

    // Clear the Primary, Advisory and Secondary prompts
    GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY, PromptArea::PA_LOW,
                                   NULL_STRING_ID);
    GuiApp::PPromptArea->setPrompt(PromptArea::PA_ADVISORY, PromptArea::PA_LOW,
                                   NULL_STRING_ID);
    GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
                                   PromptArea::PA_LOW, NULL_STRING_ID);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: buttonDownHappened
//
//@ Interface-Description
// Called when any button in LowerOtherScreensSubScreen is pressed down.  We
// must display the screen corresponding to the pressed button while still
// keeping the screen select button for the LowerOtherScreensSubScreen pressed
// down.  Passed a pointer to the button as well as 'byOperatorAction', a flag
// which indicates whether the operator pressed the button down or whether the
// setToDown() method was called.
//---------------------------------------------------------------------
//@ Implementation-Description
// Check the button pressed and then activate the corresponding subscreen.
// We must also make sure the tab button for Other Screens subscreen stays 
// pressed down (it pops up when this subscreen is deactivated as a result of 
// the new Settings subscreens being activated).
//
// $[01063] The Other Screens screen select button shall remain selected ...
//---------------------------------------------------------------------
//@ PreCondition
// Callback must have been generated by an operator action.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
LowerOtherScreensSubScreen::buttonDownHappened(Button *pButton,
													Boolean byOperatorAction)
{
	CALL_TRACE("LowerOtherScreensSubScreen::buttonDownHappened(Button *pButton, "
												"Boolean byOperatorAction)");
	CLASS_PRE_CONDITION(byOperatorAction);

	// Activate the corresponding sub-screen and force the Lower
	// Other Screens tab button down with it. $[01063]
	if (pButton == &moreSettingsButton_)
	{													// $[TI1]
		getSubScreenArea()->activateSubScreen(
			LowerSubScreenArea::GetMoreSettingsSubScreen(),
			LowerScreen::RLowerScreen.getLowerScreenSelectArea()->
						getLowerOtherScreensTabButton());
	}
	else if (pButton == &dateTimeSettingsButton_)
	{													// $[TI2]
		getSubScreenArea()->activateSubScreen(
			LowerSubScreenArea::GetDateTimeSettingsSubScreen(),
			LowerScreen::RLowerScreen.getLowerScreenSelectArea()->
						getLowerOtherScreensTabButton());
	}
	else if (pButton == &commSetupButton_)
	{													// $[TI3]
		getSubScreenArea()->activateSubScreen(
			LowerSubScreenArea::GetCommSetupSubScreen(),
			LowerScreen::RLowerScreen.getLowerScreenSelectArea()->
						getLowerOtherScreensTabButton());
	}
	else if (pButton == &nifManeuverButton_)
	{
		getSubScreenArea()->activateSubScreen(
			LowerSubScreenArea::GetNifSubScreen(),
			LowerScreen::RLowerScreen.getLowerScreenSelectArea()->
						getLowerOtherScreensTabButton());
	}
	else if (pButton == &p100ManeuverButton_)
	{
		getSubScreenArea()->activateSubScreen(
			LowerSubScreenArea::GetP100SubScreen(),
			LowerScreen::RLowerScreen.getLowerScreenSelectArea()->
						getLowerOtherScreensTabButton());
	}
	else if (pButton == &svcManeuverButton_)
	{
		getSubScreenArea()->activateSubScreen(
			LowerSubScreenArea::GetVitalCapacitySubScreen(),
			LowerScreen::RLowerScreen.getLowerScreenSelectArea()->
						getLowerOtherScreensTabButton());
	}
    else if(pButton == &proxSetupButton_) 
    {
		getSubScreenArea()->activateSubScreen(
			LowerSubScreenArea::GetProxSetupSubScreen(),
			LowerScreen::RLowerScreen.getLowerScreenSelectArea()->
						getLowerOtherScreensTabButton());    
    }
	else
	{
		CLASS_ASSERTION(FALSE);                            
	}

	// Bounce the screen selection button
	pButton->setToUp();
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: bdEventHappened
//
//@ Interface-Description
//  Called when Breath-Delivery notifies us of a PROX_INSTALLED event.
//---------------------------------------------------------------------
//@ Implementation-Description
//  When PROX_INSTALLED event is recieved ProxSetupSubscreen button shall
//  be shown.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void LowerOtherScreensSubScreen::bdEventHappened(EventData::EventId eventId,
										EventData::EventStatus eventStatus,
										EventData::EventPrompt eventPrompt)
{
	CALL_TRACE("ProxSetupSubScreen::bdEventHappened(EventData::EventId "
			   ", EventData::EventStatus eventStatus)");

	if (EventData::PROX_INSTALLED == eventId)
	{

		if (eventStatus == EventData::ACTIVE)
		{
			const DiscreteValue  CIRCUIT_TYPE = 
					SettingContextHandle::GetSettingValue(
											ContextId::ACCEPTED_CONTEXT_ID,
											SettingId::PATIENT_CCT_TYPE);
	
			// Enable Prox setup button if patient type is 
			// a neonatal circuit and board is installed.
#if !defined(VCO2_STUDY)
			if (CIRCUIT_TYPE == PatientCctTypeValue::NEONATAL_CIRCUIT)
#endif
			{
	
				commSetupButton_.setX(BUTTON_ROW1_X1_);
				dateTimeSettingsButton_.setX(BUTTON_ROW1_X2_);
				moreSettingsButton_.setX(BUTTON_ROW1_X3_);
				addDrawable(&proxSetupButton_);		
			}
		}

	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
LowerOtherScreensSubScreen::SoftFault(const SoftFaultID  softFaultID,
		   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS,
			LOWEROTHERSCREENSSUBSCREEN, lineNumber, pFileName, pPredicate);
}
