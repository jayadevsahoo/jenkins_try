
#ifndef PersistentObjsId_HH
#define PersistentObjsId_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  PersistentObjsId - Id of all of the modules of this subsystem.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Persistent-Objects/vcssrc/PersistentObjsId.hhv   25.0.4.0   19 Nov 2013 14:18:52   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By:  sah    Date:  04-Sep-1997    DCS Number: 1923
//  Project:  Sigma (R8027)
//  Description:
//      Changed the name of the Communication Diagnostic Log to System
//	Information Log.
//
//  Revision: 001  By:  sah    Date:  15-Mar-1997    DCS Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//          Initial version (Integration baseline).
//  
//====================================================================

//@ Usage-Classes
//@ End-Usage


//@ Type:  PersistentObjsId
// Id for all of this subsystem's classes.
enum PersistentObjsId
{
  NOV_RAM_MANAGER		=  0,
  NOV_RAM_UPDATE_MANAGER	=  1,
  NOV_RAM_XACTION		=  2,

  NON_VOLATILE_MEMORY		=  3,
  VOLATILE_MEMORY		=  4,

  ALARM_LOG_ENTRY		=  5,
  CODE_LOG_ENTRY		=  6,
  RESULT_TABLE_ENTRY		=  7,

  RESULT_ENTRY_DATA		=  8,
  DIAGNOSTIC_CODE		=  9,

  DIAG_CODE_LOG			= 10,
  EST_DIAG_LOG			= 12,
  SST_DIAG_LOG			= 13,
  SYSTEM_INFO_LOG		= 11,
  SYSTEM_DIAG_LOG		= 14,

  ALARM_HISTORY_LOG		= 15,

  TEST_RESULT_TABLE		= 16,
  EST_RESULT_TABLE		= 17,
  SST_RESULT_TABLE		= 18,

  NOV_RAM_BOOLEAN		= 19,

  NOV_RAM_DISCRETE		= 20,

  NOV_RAM_SEMAPHORE		= 21,
  NOV_RAM_SEQUENCE_NUM		= 22,

  DOUBLE_BUFFER			= 23,
  DOUBLE_BUFFER_DATA		= 24,

  OPERATIONAL_TIME		= 25,

  PERSISTENT_OBJS_CLASS		= 26,

  NUM_PERS_OBJS_CLASSES		= 27
};


#endif // PersistentObjsId_HH 
