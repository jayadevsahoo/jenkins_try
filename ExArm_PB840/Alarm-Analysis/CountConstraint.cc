#include "stdafx.h"
//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: CountConstraint - Type "2 of last 4 breaths meet detection criteria".
//---------------------------------------------------------------------
//@ Interface-Description
// The ViolationHistoryManager derived object attached to the child of the
// OperatingGroup associated with the CountConstraint provides the number of
// violations which is compared against the minimum and maximum values
// according to the Boolean operators specified in the CountConstraint.
// A method which does the Boolean comparisons is provided by the pure virtual
// base class Constraint.
//---------------------------------------------------------------------
//@ Rationale
// CountConstraint is used to model condition augmentations from the Technical
// and Non-Technical alarm matrices of the type "Detection criteria is met for
// 2 of the last 4 breaths" or "Detection criteria is met for 2 breaths in the
// last 60 seconds".
//---------------------------------------------------------------------
//@ Implementation-Description
// CountConstraint defines base class Constraint's pure virtual method,
// evaluate.  This method takes an OperatingGroup as a parameter.  This
// OperatingGroup object is the only child of the OperatingGroup object to
// which the CountConstraint is attached.  The child OperatingGroup contains
// the detection criteria for an Alarm or AlarmAugmentation and has an object
// derived from ViolationHistoryManager attached to it.  The
// ViolationHistoryManager derived object keeps track of the number of
// violations which have occurred.  When it is time to evaluate the
// OperatingGroup object attached to the CountConstraint, the evaluate method
// is called.  The number of violations is accessed from the
// ViolationHistoryManager derived object attached to the child group and
// compared against the minimum and maximum derived from the Constraint base
// class.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Alarm-Analysis/vcssrc/CountConstraint.ccv   25.0.4.0   19 Nov 2013 13:51:22   pvcs  $
//
//@ Modification-Log

//  
//   Revision 001   By:   hct      Date: 01/01/95         DR Number:   n/a
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//  
//   Revision 002   By:   sah      Date: 01/12/99         DR Number:   5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to non-inlined method.
//  
//=====================================================================

#include "Sigma.hh"
#include "Alarm_Analysis.hh"
#include "CountConstraint.hh"

//@ Usage-Classes
#include "OperatingGroup.hh"

// class <OtherUsageClass>;
//@ End-Usage

//@ Code...

//=====================================================================
//
//      Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: CountConstraint
//
//@ Interface-Description
//   Constructor.
//
// >Von
//   minBooleanOp      Enumerator representing the Boolean operator used for 
//                     evaluating the Constraint minimum.
//   maxBooleanOp      Enumerator representing the Boolean operator used for 
//                     evaluating the Constraint maximum.
//   pMinCalcFunction  Pointer to a function which returns the minimum value.
//   pMaxCalcFunction  Pointer to a function which returns the maximum value.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================

CountConstraint::CountConstraint(Constraint::BooleanOperator minBooleanOp, 
                             Constraint::BooleanOperator maxBooleanOp,
                             Constraint::CalcFunctionPtrType pMinCalcFunction,
                             Constraint::CalcFunctionPtrType pMaxCalcFunction):
                             Constraint(minBooleanOp,
                                        maxBooleanOp,
                                        pMinCalcFunction,
                                        pMaxCalcFunction)

{
  CALL_TRACE("CountConstraint(minBooleanOp, maxBooleanOp, pMinCalcFunction, pMaxCalcFunction)");
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~CountConstraint()
//
//@ Interface-Description
//   Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================

CountConstraint::~CountConstraint(void)
{
  CALL_TRACE("~CountConstraint()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: evaluate
//
//@ Interface-Description
//   This method is called when an OperatingGroup has an object derived from
//   Constraint associated with it.  When the OperatingGroup is messaged to
//   evaluate itself, it messages its children to evaluate themselves.  After
//   control returns to the OperatingGroup, it messages this method, passing a
//   reference to its child OperatingGroup which will have an object derived 
//   from ViolationHistoryManager attached to it.  The child OperatingGroup is 
//   queried as to the current number of violations its ViolationHistoryManager 
//   is tracking.  This number of violations is compared against the 
//   CountConstraint minimum and/or the CountConstraint maximum via the 
//   respective Boolean operators.  If the current number of violations falls
//   within the ranges specified by the CountConstraint minimum and maximum as
//   compared using the respective Boolean operators, this method returns TRUE
//   else FALSE.
//
// >Von
//   rChildGroup  Reference to the OperatingGroup which refers to the 
//                ViolationHistoryManager which tracks violations for this
//                CountConstraint.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//@ End-Method
//=====================================================================
Boolean
CountConstraint::evaluate(OperatingGroup& rChildGroup) const
{
  CALL_TRACE("evaluate(rChildGroup)");

  Boolean state = TRUE;
  Int32 numViolations = rChildGroup.getNumViolations();

  if (numViolations > 0) // $[TI1]
  {
    if (pMinCalcFunction_ != NULL) // $[TI1.1]
    {
      state = evaluateBoolean_(numViolations, (*pMinCalcFunction_)(),
          minBooleanOp_);
    } // else $[TI1.2]
    if (state) // $[TI1.3]
    {
      if (pMaxCalcFunction_ != NULL) // $[TI1.3.1]
      {
        state = evaluateBoolean_(numViolations, (*pMaxCalcFunction_)(), 
             maxBooleanOp_);
      } // else $[TI1.3.2]
    } // else $[TI1.4]
  }
  else // $[TI2]
    state = FALSE;

  return(state);
}



#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//@ Interface-Description
//   This method is called when a software fault is detected by the
//   fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//   'lineNumber' are essential pieces of information.  The 'pFileName'
//   and 'pPredicate' strings may be defaulted in the macro to reduce
//   code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This method receives the call for the SoftFault, adds it sub-system
//   and class name ID and sends the message to the non-member function
//   SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition   
//   none
//@ End-Method
//=====================================================================

void
CountConstraint::SoftFault(const SoftFaultID  softFaultID,
                           const Uint32       lineNumber,
                           const char*        pFileName,
                           const char*        pPredicate)  
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, ALARM_ANALYSIS, 
                          Alarm_Analysis::COUNTCONSTRAINT, lineNumber,
                          pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)
