#ifndef PavPausePhase_HH
#define PavPausePhase_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: PavPausePhase -  Implements a PAV pause phase.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/PavPausePhase.hhv   25.0.4.0   19 Nov 2013 14:00:00   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001   By: syw   Date:  14-Jun-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//		PAV initial release
//
//=====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"

//@ Usage-Classes

#include "BreathPhase.hh"

//@ End-Usage


class PavPausePhase : public BreathPhase {
  public:
    PavPausePhase(BreathPhaseType::PhaseType phaseType) ;
	virtual ~PavPausePhase( void) ;

    virtual void newBreath( void) ;
    virtual void relinquishControl( const BreathTrigger& trigger) ;
    virtual void newCycle( void) ;

    static void SoftFault( const SoftFaultID softFaultID,
							   const Uint32      lineNumber,
							   const char*       pFileName  = NULL, 
							   const char*       pPredicate = NULL) ;

  protected:
	virtual void updateFlowControllerFlags_( void) ;

  private:
    PavPausePhase( void) ;							// not implemented...
    PavPausePhase( const PavPausePhase&) ;			// not implemented...
    void   operator=( const PavPausePhase&) ;		// not implemented...

};


#endif // PavPausePhase_HH 
