#pragma once


// CGetUserInputDlg dialog

class CGetUserInputDlg : public CDialog
{
	DECLARE_DYNAMIC(CGetUserInputDlg)

public:
	CGetUserInputDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CGetUserInputDlg();

	BOOL OnInitDialog();


// Dialog Data
	enum { IDD = IDD_DIALOG_GET_USER_INPUT };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	CString m_strUserInput, m_strTitle;
	afx_msg void OnBnClickedOk();
};
