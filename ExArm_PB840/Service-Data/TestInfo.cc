#include "stdafx.h"

#if		defined(SIGMA_GUI_CPU) || defined(SIGMA_COMMON)

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class:  TestInfo - Service Data Item processing agent class.
//---------------------------------------------------------------------
//@ Interface-Description
//
//>Von
//   Description:
//   ------------
//   TestInfo serves as a base class for all the service data
//   processing agent classes. It handles several common aspects of
//   all service data processing, maintains double buffer to coordinate
//   data access and data update amongst multiple tasks, notifies
//   other interested tasks when there is a service item change, and
//   provides service data access methods.
//
//   Class Features:
//   --------------------
//    1. Maintains a double service data buffer, one to hold an active data
//       that is already to be accessed by other subsystems and the other to
//       receive a new data from the network.
//    2. Inform interested subsystems about a service item data change
//       and data timeout.
//    3. Provides various service data access methods.
//
//   Class Key methods:
//   -----------------
//   1. modifyData_   -- modifies service item data of this data agent
//						 and sends out a service data change notification.
//   2. getDataValue  -- gets the active data value of this
//						    data service type agent.
//   3. getCommandValue -- gets the active data value of this
//       			        command service type data agent.
//   4. isData  --   determines whether an service data is available.
//   5. isCommand  --   determines whether an service command is available.
//---------------------------------------------------------------------
//@ Rationale
//     To capture all the common functionality of service item
//     data processing within one class.
//---------------------------------------------------------------------
//@ Implementation-Description
//    Data integrity of each service item type is maintained by using double
//    data buffer. Initially buffer index value is set to indicate that
//    there is no active data in the buffer, both network and active indexes
//    are set to zero until network starts delivering service data from the
//    Breath Delivery board.
//    The 'network' indexes into the buffer array that is either receiving
//    data or waiting to be filled. The 'active' indexes into a
//    buffer where can be read by other tasks. As soon as, a new data
//    is completely stored into a network buffer, the index_.active
//    will be updated to a newer data and index_.network will be updated
//    to next buffer.
//
//---------------------------------------------------------------------
//@ Fault-Handling
//    Follows the Sigma Standard contract programming model,
//    class precoditions and class assertions are used to raise
//    a fatal software conditions.
//>Voff
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Data/vcssrc/TestInfo.ccv   25.0.4.0   19 Nov 2013 14:21:04   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah     Date:  15-Jan-99   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' to a non-inlined method.
//
//  Revision: 001  By:  yyy    Date:  16-May-95   DR Number:
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//=====================================================================

#include "TestInfo.hh"
#include "ServiceDataMgr.hh"

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  TestInfo
//
//@ Interface-Description
//     The TestInfo class constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//     The network and active index of the buffer index is initially
//      set to zero. When these two indexes are same there is no active
//      data for this data type.
//---------------------------------------------------------------------
//@ PreCondition
//  ( ServiceDataMgr::IsTestItemId(id) )
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

TestInfo::TestInfo(TestDataId::TestDataItemId dataId) :
		dataId_(dataId)
{
	CALL_TRACE("TestInfo(TestDataId::TestDataItemId dataId)");
	CLASS_PRE_CONDITION( ServiceDataMgr::IsTestItemId(dataId) );

  												// $[TI1]
	index_.active = index_.network = 0;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~TestInfo()  [Destructor]
//
//@ Interface-Description
//    Destructor - does nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

TestInfo::~TestInfo(void)
{
	CALL_TRACE("~TestInfo()");
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: modifyData_(void *pValue )
//
//@ Interface-Description
//     TestInfo::modifyData_() updates the active data to
//     be the data just received from the network.
//     Sends out service data change notification for this specific
//     service item data type.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This is a virtual function that should be overriden by its
//      derived classes for actual modification of the data. After
//      newly received data is completely processed into a network buffer
//      by a subclass 'modifyData' method, this method is called to update
//      the buffer index.
//      This method is executed on the Service-Data task only
//      and it has higher execution priority than other tasks
//      that reads service data. It is critical that this method doesn't
//      get interruped by these tasks.
//---------------------------------------------------------------------
//@ PreCondition
//  ( index_.network >=0 && index_.network < 2 )
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
TestInfo::modifyData_(void *)
{

	CALL_TRACE("TestInfo::modifyData_()");
	SAFE_CLASS_PRE_CONDITION( index_.network >=0 && index_.network < 2 )

#if defined (FORNOW)
	cout << "TestInfo::modifyData_()::TI1" << endl;
#endif	// (FORNOW)

												// $[TI1]

#if defined (FORNOW)
printf("TestInfo::modifyData_()\n");
#endif	// (FORNOW)

	index_.active = index_.network;
	index_.network =  (index_.network +1 ) %2;

	ServiceDataMgr::MakeItemCalls(dataId_);
} 


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
TestInfo::SoftFault(const SoftFaultID  softFaultID,
		const Uint32       lineNumber,
		const char*        pFileName,
		const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

	FaultHandler::SoftFault(softFaultID, SERVICE_DATA,
			TEST_INFO, lineNumber, pFileName, pPredicate);
}



#endif		//defined(SIGMA_GUI_CPU) || defined(SIGMA_COMMON)
