#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============= C L A S S   D E S C R I P T I O N ====================
//@ Class:  DiscreteSetting - Discrete Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This class provides the settings' framework for the Discrete Settings.
//  All discrete settings ultimately are derived from this setting.  This
//  class overrides some of the virtual methods of 'Setting' including
//  those to:  update to a batch discrete setting's new-patient value,
//  respond to an activation by the operator, calculate a new discrete value
//  based on a knob delta, and to reset a discrete setting's value to its
//  value upon activation.  This class also defines new virtual methods,
//  with default behavior, that can be overridden by derived classes, if
//  need be.  These new virtual methods include methods to:  get this
//  discrete setting's default and new-patient value, get this discrete
//  setting's "accepted" and "adjusted" value, get the total number of
//  POSSIBLE values, accept a new value from a primary setting, activating
//  the Transition Rules associated with this setting (Main Control Settings
//  only), get the current index of the current value, get the number of
//  ALLOWABLE values, calculate (update) this settings dependent settings,
//  and store a new value for this setting into the Adjusted Context.
//  These methods are overridden, if needed, by the derived discrete settings
//  to provide the specific behavior needed.
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to establish the setting's discrete framework
//  for the rest of the settings to build upon.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class is initialized with a pointer to the first element of an
//  array of possible values, and a constant that indicates the total
//  number of possible values.  These members are used in the processing
//  of knob deltas when determing "new" values for this discrete setting.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/DiscreteSetting.ccv   25.0.4.0   19 Nov 2013 14:27:20   pvcs  $
//
//@ Modification-Log
//
//  Revision: 012   By: gdc    Date: 16-Aug-2010     SCR Number: 6663
//  Project: API/MAT
//  Description:
//  Added support for VentSet API.
// 
//  Revision: 011   By: sah   Date:  23-May-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      *  changed the private method, 'isEnabledValue_()', to the public
//         method 'isEnabledValue()', thereby allowing its exportation
//         out of this subsystem, for use with the drop-down menus
//
//  Revision: 010   By: sah    Date: 29-Jun-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  re-wrote 'calcNewValue()' to better handle bound violations
//	*  incorporated initial specifications for NeoMode Project. Also,
//         eliminated use of unneeded 'INVALID_DISCRETE_VALUE' bound state,
//         to allow for more consistent use of dependent soft-bounds.
//
//  Revision: 009   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new pre-condition to 'calcNewValue()', to ensure that
//	   only CHANGEABLE settings are directly changed.
//
//  Revision: 008   By: dosman    Date:  15-Jan-1999    DR Number: 5187
//  Project:  ATC
//  Description:
//	Added functionality to store the bounds hit by dependent settings
//	(both the bound ids and the bound values of the dependents).
//
//  Revision: 007   By: sah    Date:  19-Oct-1998    DR Number:
//  Project:  BILEVEL
//  Description:
//	* Fixed dependent bound mechanism in 'calcDependentValues_()' to
//	call 'resolveDependentChange_()' for every dependent setting, rather
//	than breaking the loop following first violation.
//	* Moved 'isEnabledValue_()' to an area with the other private methods.
//	* Added more debug code.
//
//  Revision: 006   By: dosman Date:  30-Apr-1998    DCS Number: 4
//  Project:  BILEVEL
//  Description:
//	remove totalNumAccessibleValues_
//	because removing the whole mechanism of storing the number of
//	accessible values...that was being used to be able to control
//	to the total number of accessible values so that the last
//	value(s) in the enumeration could be made nonaccessible
//	just by decreasing the totalNumAccessibleValues_....
//	instead we are moving to a different implementation that
//	allows determining if a given value is enabled at the time
//	that it is needed, using polymorphism.
//
//  Revision: 005   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 004   By: dosman Date:  05-Feb-1998    DCS Number: 
//  Project:  BILEVEL
//  Description:
//	initial bilevel version.
//	instead of using the total
//	possible number of setting values for this particular setting,
//	TOTAL_NUM_VALUES_,
//	when choosing which values to loop through 
//	when the knob is turned,
//	we instead use the total number of accessible values
//	totalNumAccessibleValues_,
//	and we make sure the total number of accessible values
//	is initialized to the total possible number of values
//	and can be changed later as needed
//
//  Revision: 003   By: sah    Date:  28-May-1998    DR Number: 5092
//  Project:  COLOR
//  Description:
//	--- Merged Rev "Color" (1.25.1.0) into Rev "BiLevel" (1.25.2.0)
//	Added ability of discrete settings to activate, via constructor
//	parameters, bounds for this setting.  Modified this class's
//	constructor, and 'calcNewValue()' to facilitate this new feature.
//
//  Revision: 002   By: sah    Date:  04-Jun-1997    DCS Number: 2188
//  Project: Sigma (R8027)
//  Description:
//	Fixed problem where discrete setting bound violations were never
//	being cleared.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Initial version.
//
//=====================================================================

#include "DiscreteSetting.hh"

//@ Usage-Classes
#include "AdjustedContext.hh"
#include "AcceptedContext.hh"
#include "SettingsMgr.hh"
#include "VsetServer.hh"
#include <string.h>
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method: ~DiscreteSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this discrete setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

DiscreteSetting::~DiscreteSetting(void)
{
  CALL_TRACE("~DiscreteSetting()");
}


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method: getDefaultValue()  [const, pure virtual]
//
//@ Interface-Description
//  This is a pure virtual method that is to be overridden by ALL non-batch
//  settings and defined with appropriate behavior.  This virtual method is
//  is used to retrieve a setting's default value.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

//@ Not-Implemented


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method: getNewPatientValue()  [const, pure virtual]
//
//@ Interface-Description
//  This is a pure virtual method that is to be overridden by ALL batch
//  settings and defined with appropriate behavior.  This virtual method is
//  is used to retrieve a setting's new-patient value.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

//@ Not-Implemented


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method: getAcceptedValue()  [const, pure virtual]
//
//@ Interface-Description
//  This is a pure virtual method that is to be overridden by derived
//  classes to return this setting's "accepted" value.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

//@ Not-Implemented


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method: getAdjustedValue()  [const, pure virtual]
//
//@ Interface-Description
//  This is a pure virtual method that is to be overridden by derived
//  classes to return this setting's "adjusted" value.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

//@ Not-Implemented


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  setAdjustedValue(newAdjustedValue)  [pure virtual]
//
//@ Interface-Description
//  This is a pure virtual method that is to be overridden by all derived
//  classes to store 'newAdjustedValue' as this setting's new "adjusted"
//  value.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

//@ Not-Implemented


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  calcNewValue(knobDelta)  [virtual]
//
//@ Interface-Description
//  Calculate, and initialize to, the new discrete value.  The value of
//  'knobDelta' is only used in so far as the "direction" of change; the
//  value, itself, is ignored a delta of '1' is applied in the direction
//  of change.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02001] -- The validation of a proposed setting value...
//  $[BL04079]
//---------------------------------------------------------------------
//@ PreCondition
//  (getApplicability() == Applicability::CHANGEABLE)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

const BoundStatus&
DiscreteSetting::calcNewValue(const Int16 knobDelta)
{
  CALL_TRACE("calcNewValue(knobDelta)");

#if !defined(SIGMA_UNIT_TEST)
  // checked when NOT unit testing...
  {
    // batch settings can only be modified in the ADJUSTED context, while
    // non-batch settings can only be modified in the ACCEPTED context...
    const Notification::ChangeQualifier  QUALIFIER_ID =
      (SettingId::IsBatchId(getId())) ? Notification::ADJUSTED	// $[TI4]
				      : Notification::ACCEPTED;	// $[TI5]
    AUX_CLASS_PRE_CONDITION(
		    (getApplicability(QUALIFIER_ID) == Applicability::CHANGEABLE),
			    getId());
  }
#endif // !defined(SIGMA_UNIT_TEST)

  BoundStatus&  rBoundStatus = getBoundStatus_();
  BoundStatus   savedBoundStatus(getId());

  const DiscreteValue  STARTING_VALUE = getAdjustedValue();

  // algorithm depends on this being initialized to current value...
  DiscreteValue  newValue = STARTING_VALUE;

  // reset the bound status before processing 'knobDelta', because the
  // last time we modified this setting we may have left the setting
  // in a violation-state (DCS #2188)...
  rBoundStatus.reset();

  do
  {   
    // $[TI1] -- this path ALWAYS executes...
    savedBoundStatus.setBoundStatus(rBoundStatus.getBoundState(),
    				    rBoundStatus.getBoundId());
    savedBoundStatus.setViolationId(rBoundStatus.getViolatedSettingId());

    if (knobDelta < 0)
    {   
      // $[TI1.1]

      do 
      {
        // $[TI1.1.1] -- this path ALWAYS executes
        // this code is always executed at least once

        // attempt to apply a decrement of '1' to the current discrete value...

        if (IS_IN_NON_WRAPPING_MODE_)
        {   
          // $[TI1.1.1.1]  -- don't wrap-around...
          if (newValue != 0)
	  {   
            // $[TI1.1.1.1.1]
	    // apply a decrement of '1' to the current discrete value...
	    newValue--;
	  }
	  else
	  {   
            // $[TI1.1.1.1.2]
	    savedBoundStatus.setBoundStatus(::LOWER_HARD_BOUND,
					    LOWER_LIMIT_BOUND_ID_);
	    savedBoundStatus.setViolationId(getId());
	  }
        }
        else
        {   // $[TI1.1.1.2]
	  // apply a "wrapping" decrement of '1' to the current discrete value...
	  newValue = (newValue != 0) ? (newValue - 1)		// $[TI1.1.1.2.1]
				   : (TOTAL_NUM_VALUES_ - 1);	// $[TI1.1.1.2.2]
        }

      // as long as their is a conflict in the value,
      // ie. value is not enabled,
      // keep decrementing

      } while ( !isEnabledValue(newValue) );

    } // endif (knobDelta < 0)
    else
    {   
      // $[TI1.2] -- (knobDelta > 0)

      do
      {
        // $[TI1.2.1] -- this path ALWAYS executes
        // this code is always executed at least once

        // attempt to apply an increment of '1' to the current discrete value...

        if (IS_IN_NON_WRAPPING_MODE_)
        {   
          // $[TI1.2.1.1]  -- don't wrap-around...
          if (newValue != (TOTAL_NUM_VALUES_ - 1))
	  {   
            // $[TI1.2.1.1.1]
	    // apply an increment of '1' to the current discrete value...
	    newValue++;
	  }
	  else
	  {  
            // $[TI1.2.1.1.2]
	    savedBoundStatus.setBoundStatus(::UPPER_HARD_BOUND,
					    UPPER_LIMIT_BOUND_ID_);
	    savedBoundStatus.setViolationId(getId());
	  }
        }
        else
        {   
          // $[TI1.2.1.2]
	  // apply a "wrapping" increment of '1' to the current discrete value...
	  newValue = (newValue + 1) % TOTAL_NUM_VALUES_;
        }

        // as long as their is a conflict in the value,
        // ie. value is not enabled,
        // keep incrementing

      } while ( !isEnabledValue(newValue) );
    } // endelse (knobDelta > 0)

    const DiscreteValue  CURRENT_VALUE = getAdjustedValue();

    if (CURRENT_VALUE != newValue)
    {  // $[TI1.3]
      // store the new value...
      setAdjustedValue(newValue);

      // reset the setting's bound status, before calculating dependent bounds
      // (NOTE:  this IS needed despite the 'reset()' call before the loop,
      //         because if a bound violation were to occur, the loop will be
      //         iterated through more than once)...
      rBoundStatus.reset();

      // check this new discrete value against this setting's dependent
      // settings, if any...
      calcDependentValues_();
    }  // $[TI1.4]

    // while a dependent bound was violated and we haven't circled back
    // to this discrete setting's original value...
  } while (rBoundStatus.isInViolation()  &&  newValue != STARTING_VALUE);

  // make sure that this setting's adjusted value is still valid...
  SAFE_AUX_CLASS_ASSERTION((isAdjustedValid()), getId());

  if (savedBoundStatus.isInViolation())
  {   // $[TI2] -- store the bound violation info...
    rBoundStatus.setBoundStatus(savedBoundStatus.getBoundState(),
				savedBoundStatus.getBoundId());
    rBoundStatus.setViolationId(savedBoundStatus.getViolatedSettingId());
  }   // $[TI3] -- no violation occurred...

  return(rBoundStatus);
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  calcTransition(newValue, currValue)  [pure virtual]
//
//@ Interface-Description
//  This is a pure virtual method that is to be overridden by all derived
//  Main Control Setting classes, to provide appropriate behavior regarding
//  the transition of this Main Control Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

//@ Not-Implemented


#if defined(SIGMA_DEVELOPMENT)

//============== M E T H O D   D E S C R I P T I O N =================
// Method:  isAcceptedValid()  [const, virtual]
//
// Interface-Description
//  Is the accepted value of this setting valid as determined by its
//  dependencies?
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

Boolean
DiscreteSetting::isAcceptedValid(void) const
{
  CALL_TRACE("isAcceptedValid()");

  const DiscreteValue  ACCEPTED_VALUE = getAcceptedValue();

  return(ACCEPTED_VALUE >= 0  &&
         ACCEPTED_VALUE < TOTAL_NUM_VALUES_  &&
         isEnabledValue(ACCEPTED_VALUE));
}


//============== M E T H O D   D E S C R I P T I O N =================
// Method:  isAdjustedValid()  [virtual]
//
// Interface-Description
//  Is the adjusted value of this setting valid as determined by its
//  dependencies?
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

Boolean
DiscreteSetting::isAdjustedValid(void)
{
  CALL_TRACE("isAdjustedValid()");

  const DiscreteValue  ADJUSTED_VALUE = getAdjustedValue();

  return(ADJUSTED_VALUE >= 0  &&
  	 ADJUSTED_VALUE < TOTAL_NUM_VALUES_  &&
         isEnabledValue(ADJUSTED_VALUE));
}

#endif  // defined(SIGMA_DEVELOPMENT)


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//        [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
DiscreteSetting::SoftFault(const SoftFaultID  softFaultID,
                           const Uint32       lineNumber,
                           const char*        pFileName,
                           const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION, DISCRETE_SETTING,
                          lineNumber,pFileName, pPredicate);
}


//=====================================================================
//
//  Protected Methods..
//
//=====================================================================

//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method: DiscreteSetting(...)
//
//@ Interface-Description
//  Construct a discrete setting using 'discreteId' for its ID, and
//  'totalNumValues' as the total number of values.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (SettingId::IsDiscreteId(discreteId))
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

DiscreteSetting::DiscreteSetting(
		       const SettingId::SettingIdType  discreteId,
		       const SettingId::SettingIdType* arrDependentSettingIds,
                                 const Uint              totalNumValues,
				 const Boolean           useNonWrappingMode,
				 const SettingBoundId       upperLimitBoundId,
				 const SettingBoundId       lowerLimitBoundId)
				 : Setting(discreteId, arrDependentSettingIds),
				   TOTAL_NUM_VALUES_(totalNumValues),
				   IS_IN_NON_WRAPPING_MODE_(useNonWrappingMode),
				   UPPER_LIMIT_BOUND_ID_(upperLimitBoundId),
LOWER_LIMIT_BOUND_ID_(lowerLimitBoundId),
pLookupTable_(NULL)
{
  CALL_TRACE("DiscreteSetting(...)");
  AUX_CLASS_PRE_CONDITION((SettingId::IsDiscreteId(discreteId)),
  			  discreteId);

  const SettingId::SettingIdType*  pDependentId;

  Uint  idx;

  // count the total number of this setting's dependent settings...
  for (pDependentId = ARR_DEPENDENT_SETTING_IDS_, idx = 0u;
       *pDependentId != SettingId::NULL_SETTING_ID; pDependentId++, idx++)
  {
    // do nothing...
  }

  CLASS_ASSERTION((idx < MAX_DEPENDENT_VALUES_));
}   // $[TI1]


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method: calcDependentValues_(isValueIncreasing)
//
//@ Interface-Description
//  Notify this setting's dependent settings of an update to this
//  primary setting.  If a dependent bound is violated, this setting's
//  bound status is updated with the dependent bound information.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean
DiscreteSetting::calcDependentValues_(const Boolean)
{
  CALL_TRACE("calcDependentValues_(isValueIncreasing)");

  if (*ARR_DEPENDENT_SETTING_IDS_ != SettingId::NULL_SETTING_ID)
  {   // $[TI1] -- this setting has at least one dependent setting...
    static SettingValue  ArrInitialDependentValues_[MAX_DEPENDENT_VALUES_];

    const SettingId::SettingIdType*  pDependentId;
    SettingValue*                    pInitialDependentValue;

    Uint  numDependentIds = 0u;

    // save all of the dependent setting's current values into
    // 'ArrInitialDependentValues_'...
    for (pDependentId = ARR_DEPENDENT_SETTING_IDS_,
	   pInitialDependentValue = ArrInitialDependentValues_;
         *pDependentId != SettingId::NULL_SETTING_ID;
	 pDependentId++, pInitialDependentValue++, numDependentIds++)
    {
      // get a pointer to the dependent setting identified by '*pDependentId',
      // then store that setting's value into '*pInitialDependentValue'...
      *pInitialDependentValue =
		SettingsMgr::GetSettingPtr(*pDependentId)->getAdjustedValue();
    }

    // ensure that 'ArrInitialDependentValues_' is big enough...
    AUX_CLASS_ASSERTION((numDependentIds <= MAX_DEPENDENT_VALUES_),
		        numDependentIds);

 
    SettingBoundId*  pDependentBoundId;
    SettingValue* pDependentValue;
 
    // initialize all of the dependent setting's bound violation Ids and
    // dependent setting values
    for (pDependentId = ARR_DEPENDENT_SETTING_IDS_,
                pDependentBoundId = arrDependentBoundIds_,
                pDependentValue = arrDependentValues_;
         *pDependentId != SettingId::NULL_SETTING_ID;
         pDependentId++,
         pDependentBoundId++,
         pDependentValue++)
    {
      // initialize the dependent bound violation Ids to NULL at this point.
      // The dependent values will be initialized to their current values,
      // even though these variables are not currently accessed until after
      // the dependent setting accepts the change of the primary setting.
 
      *pDependentBoundId = NULL_SETTING_BOUND_ID;
      *pDependentValue =
                SettingsMgr::GetSettingPtr(*pDependentId)->getAdjustedValue();
    }            
 
#if defined(SIGMA_DEVELOPMENT)
    if (Settings_Validation::IsDebugOn(::ADJUSTED_CONTEXT))
    {
      cout << "CALC-DEPENDENT STEP #1:  settingId = " << getId() << endl;
    }
#endif // defined(SIGMA_DEVELOPMENT)

    Boolean  didViolationOccur = FALSE;

    for (pDependentId = ARR_DEPENDENT_SETTING_IDS_;
         *pDependentId != SettingId::NULL_SETTING_ID; pDependentId++)
    {
      // resolve the changing of this primary setting's dependent setting,
      // based on the change of this primary setting...
      didViolationOccur = (resolveDependentChange_(*pDependentId) ||
      					didViolationOccur);
    }

#if defined(SIGMA_DEVELOPMENT)
    if (Settings_Validation::IsDebugOn(::ADJUSTED_CONTEXT))
    {
      cout << "CALC-DEPENDENT STEP #2:  didViolationOccur = "
	   << didViolationOccur << endl;
    }
#endif // defined(SIGMA_DEVELOPMENT)

    if (didViolationOccur)
    {  // $[TI1.1] -- this primary setting's new value violates a
       //             dependent setting's bound...
      // restore all of the dependent setting values to their original,
      // before-processing values...
      for (pDependentId = ARR_DEPENDENT_SETTING_IDS_,
				     pInitialDependentValue = ArrInitialDependentValues_;
	   *pDependentId != SettingId::NULL_SETTING_ID;
	   pDependentId++, pInitialDependentValue++)
      {
	// get a pointer to the dependent setting identified by '*pDependentId',
	// then store that setting's value into '*pInitialDependentValue'...
	SettingsMgr::GetSettingPtr(*pDependentId)->setAdjustedValue(
							    *pInitialDependentValue
							    	   );
      }

      // allow this discrete setting to process its dependent setting's
      // bound violation...
      processBoundViolation_();

      // NOTE:  by returning this setting's bound status in a violated state,
      //        the 'calcNewValue()' method will take care of attempting
      //        the "next" discrete value...
    }  // $[TI1.2] -- no bound violation occurred...
  }   // $[TI2] -- this setting has no dependent settings...

  // For discrete settings, we are always able to settle on a satisfactory value
  return(TRUE); 
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  resolveDependentChange_(dependentId, isValueIncreasing)
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean
DiscreteSetting::resolveDependentChange_(
			      const SettingId::SettingIdType dependentId,
			      const Boolean
				        )
{
  CALL_TRACE("resolveDependentChange_(dependentId, isValueIncreasing)");

#if defined(SIGMA_DEVELOPMENT)
  if (Settings_Validation::IsDebugOn(::ADJUSTED_CONTEXT))
  {
    cout << "RESOLVE-CHANGE STEP #1:  dependentId = "
	 << dependentId << endl;
  }
#endif // defined(SIGMA_DEVELOPMENT)

  Setting*  pDependentSetting = SettingsMgr::GetSettingPtr(dependentId);

  const BoundStatus*  pBoundStatus;

  // notify the dependent setting of a change to this primary setting...
  pBoundStatus = pDependentSetting->acceptPrimaryChange(getId());

  // save the resulting dependent value
  const SettingId::SettingIdType* pDependentId = ARR_DEPENDENT_SETTING_IDS_;
  SettingBoundId* pDependentBoundId = arrDependentBoundIds_;
  SettingValue* pDependentValue = arrDependentValues_;
 
  Boolean foundDependentSettingId = FALSE;

  while ( (*pDependentId != SettingId::NULL_SETTING_ID) &&
          (foundDependentSettingId == FALSE) )
  {
    // $[TI3] -- this branch ALWAYS executes at least once
    // because we are resolving a dependent setting, 
    // so there must be at least one dependent setting,
    // and we are looping through the dependent settings
    if (*pDependentId == dependentId)
    {
      // $[TI3.1]
      // if the dependent Id being processed
      // matches the the dependent Id within the static array,
      // then store the bound Id & changed dependent setting value
      // at the element in their respective arrays corresponding
      // to the element in ARR_DEPENDENT_SETTING_IDS_
 
      if (pBoundStatus == NULL)
      {
        // $[TI3.1.1]
        *pDependentBoundId = NULL_SETTING_BOUND_ID;
      }
      else
      {
        // $[TI3.1.2]
        *pDependentBoundId = pBoundStatus->getBoundId();
      }
 
      *pDependentValue = pDependentSetting->getAdjustedValue();
 
      foundDependentSettingId = TRUE;
      // there is no reason to continue looping if we already found what we were looking for
    } // end if  
    // $[TI3.2] -- implied else
 
    pDependentId++;
    pDependentBoundId++;
    pDependentValue++;
  } // end while 

  // make sure we did not fall out of the loop simply because
  // we never matched the dependent setting being processed with one in the array....
  CLASS_ASSERTION((foundDependentSettingId == TRUE));

  // get a reference to this primary setting's bound status...
  BoundStatus&  rBoundStatus = getBoundStatus_();

  Boolean  didViolationOccur = (pBoundStatus != NULL);

  if (didViolationOccur)
  {   // $[TI1]
    // set this primary setting's bound status with the dependent bound
    // violation information...
    rBoundStatus.setBoundStatus(pBoundStatus->getBoundState(),
				pBoundStatus->getBoundId());
    rBoundStatus.setViolationId(pBoundStatus->getViolatedSettingId());

#if defined(SIGMA_DEVELOPMENT)
    if (Settings_Validation::IsDebugOn(::ADJUSTED_CONTEXT))
    {
      cout << "RESOLVE-CHANGE STEP #2:  boundId = "
           << pBoundStatus->getBoundId() << endl;
    }
#endif // defined(SIGMA_DEVELOPMENT)
  }   // $[TI2]

  return(didViolationOccur);
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  processBoundViolation_()  [virtual]
//
//@ Interface-Description
//  This is a virtual method that is to be overridden by all discrete
//  settings that have dependent settings with constraints.  This method
//  is called by 'calcDependentValues_()' to allow this discrete setting
//  to convert the dependent setting's bound-violation id to a bound-violation
//  id for this setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (FALSE)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
DiscreteSetting::processBoundViolation_(void)
{
  CALL_TRACE("processBoundViolation_()");
  AUX_CLASS_ASSERTION_FAILURE(getId());
}

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  setLookupTable
//
//@ Interface-Description
//  This method is called by DiscreteSetting classes with a pointer to
//  the DiscreteParameterLookup table used by the ventset() method to
//  look up discrete value strings passed in through the VentSet 
//  command protocol.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Sets the local lookup table pointer to the pointer passed in by the
//  caller.
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//@ End-Method
//=====================================================================

void 
DiscreteSetting::setLookupTable(const DiscreteParameterItem* pLookupTable)
{
	pLookupTable_ = pLookupTable;
}

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  lookupValue
//
//@ Interface-Description
//  This method searches for the specified string in the parameter
//  lookup table and sets the corresponding DiscreteValue parameter
//  if it finds the string. Returns SigmaStatus of SUCCESS if it
//  finds the parameter string, otherwise return FAILURE.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Iterates through the DiscreteParameterItem lookup table pointed to
//  by pLookupTable_.
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//@ End-Method
//=====================================================================

SigmaStatus
DiscreteSetting::lookupValue_(const char* pName, DiscreteValue& value)
{
	const DiscreteParameterItem* pLookupItem = pLookupTable_;
	while ( pLookupItem && pLookupItem->name != NULL )
	{
		if ( !strncmp(pLookupItem->name, pName, strlen(pLookupItem->name)) )
		{
			value = pLookupItem->value;
			return SUCCESS;
		}
		++pLookupItem;
	}
	return FAILURE;
}

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  lookupName
//
//@ Interface-Description
//  This method searches for the specified DiscreteValue in the 
//  parameter lookup table and returns a pointer to the parameter name
//  string if found. Returns a NULL pointer if it fails to find the
//  value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Iterates through the DiscreteParameterItem lookup table pointed to
//  by pLookupTable_.
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//@ End-Method
//=====================================================================

const char*
DiscreteSetting::lookupName_(DiscreteValue lookupValue)
{
	const DiscreteParameterItem* pLookupItem = pLookupTable_;
	while ( pLookupItem && pLookupItem->name != NULL )
	{
		if ( lookupValue == pLookupItem->value )
		{
			return pLookupItem->name;
		}
		++pLookupItem;
	}
	return NULL;
}

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  ventset
//
//@ Interface-Description
//  This method attempts to set the DiscreteSetting to the value
//  specified by the string parameter. 
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses lookupValue_ to search the parameter lookup table for the
//  parameter string. Uses lookupName_ to provide the parsed parameter
//  name to the VentSet::Report method that provides the status of the
//  setting change requested through the serial port. Uses 
//  setAdjustedValue() to set this DiscreteSetting to the 
//  DiscreteValue found in the lookup table.
//---------------------------------------------------------------------
//@ PreCondition
//  pLookupTable_ != NULL
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SigmaStatus
DiscreteSetting::ventset(const char* pValueString)
{
	Boolean isValid = FALSE;
	DiscreteValue newValue;
	const char* errorString = "";

    AUX_CLASS_PRE_CONDITION( pLookupTable_ != NULL, getId());

	if ( lookupValue_(pValueString, newValue) != SUCCESS )
	{
		errorString = "Invalid value.";
	}
	else if ( !isEnabledValue(newValue) )
	{
		errorString = "Value not enabled.";
	}
	else
	{
		errorString = "OK";
		isValid = TRUE;
		setAdjustedValue(newValue);
	}
	VsetServer::Report(lookupName_(getAdjustedValue()), !isValid, errorString);

	return isValid ? SUCCESS : FAILURE;
}

#if defined(SIGMA_DEVELOPMENT)

#include "Ostream.hh"

//============== M E T H O D   D E S C R I P T I O N =================
// Method:  print_(ostr)  [const]
//
// Interface-Description
//  Print the contents of this discrete setting into 'ostr'.
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

Ostream&
DiscreteSetting::print_(Ostream& ostr) const
{
  CALL_TRACE("print_(ostr)");

  ostr << "  Name:            " << SettingId::GetSettingName(getId())
       << endl;
  ostr << "  Accepted Value:  " << (DiscreteValue)getAcceptedValue() << endl;

  return(ostr);
}

#endif  // defined(SIGMA_DEVELOPMENT)
