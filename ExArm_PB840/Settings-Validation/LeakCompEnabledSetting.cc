#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2008, Covidien
//=====================================================================

//============== C L A S S   D E S C R I P T I O N ====================
//@ Class:  LeakCompEnabledSetting - Leak Compensation Enabled Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, discrete setting that indicates the whether the optional
//  Leak Compensation is enabled.  This class inherits from 'BatchDiscreteSetting'
//  and provides the specific information needed for the representation of
//  the Leak Compensation's activation.  This information includes the set of values
//  that this setting can have (see 'LeakCompEnabledValue.hh'), and this batch
//  setting's new-patient value.
//  $[LC24002]
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class has no data members, and no dependent settings.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/LeakCompEnabledSetting.ccv   25.0.4.0   19 Nov 2013 14:27:28   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006   By: gdc    Date: 16-Aug-2010     SCR Number: 6663
//  Project: API/MAT
//  Description:
//  Added support for VentSet API.
// 
//  Revision: 005   By: rhj    Date:  12-May-2009    SCR Number: 6516
//  Project: 840S2
//  Description:
//	    Modified getApplicability() method to fix an incorrect 
//      leak compensation button state.
// 
//  Revision: 004   By: rhj    Date:  24-Apr-2009    SCR Number: 6474
//  Project: 840S2
//  Description:
//	    Added functionality in getApplicability method to resolve
//      SCR 6474.
// 
//  Revision: 003   By: rhj    Date:  24-Apr-2009    SCR Number: 6502  
//  Project: 840S2
//  Description:
//	    Added additional checks to handle vent type changes.
// 
//  Revision: 002   By: rhj    Date:  06-Feb-2009    SCR Number: 6467 
//  Project: 840S
//  Description:
//		Fixed SCR number 6467 by comparing the adjusted setting of 
//      mandatory type instead of the accepted setting of mandatory type.
// 
//  Revision: 001   By: rhj    Date:  05-Aug-2008    SCR Number: 6435
//  Project: LEAK
//  Description:
//		Initial version.
//=====================================================================

#include "LeakCompEnabledSetting.hh"
#include "LeakCompEnabledValue.hh"
#include "SettingsMgr.hh"
#include "MandTypeValue.hh"
#include "VentTypeValue.hh"
//@ Usage-Classes
#include "ContextMgr.hh"
#include "AcceptedContext.hh"
#include "SupportTypeValue.hh"
#include "SettingContextHandle.hh"
#include "ModeValue.hh"
#include "SoftwareOptions.hh"
#include "PatientCctTypeValue.hh"
#include "AcceptedContextHandle.hh"
//@ End-Usage

//@ Code...


//====================================================================
//
//  Static Data Definitions...
//
//====================================================================

// This array is set up dynamically via this class's 'calcDependentValues_()'
static SettingId::SettingIdType  ArrDependentSettingIds_[] =
{
	SettingId::DISCO_SENS,
	SettingId::NULL_SETTING_ID
};

static const DiscreteParameterItem ParameterLookup_[] = 
{
	{"ENA", LeakCompEnabledValue::LEAK_COMP_ENABLED},
	{"DIS", LeakCompEnabledValue::LEAK_COMP_DISABLED},
	{NULL , NULL}
};

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  LeakCompEnabledSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[LC02009] -- setting range
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

LeakCompEnabledSetting::LeakCompEnabledSetting(void)
	: BatchDiscreteSetting(SettingId::LEAK_COMP_ENABLED,
						   ::ArrDependentSettingIds_,
						   LeakCompEnabledValue::TOTAL_LEAK_COMP_ENABLED_VALUES)
{
	CALL_TRACE("LeakCompEnabledSetting()");
	setLookupTable(ParameterLookup_);
}  // $[TI1]


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method:  ~LeakCompEnabledSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

LeakCompEnabledSetting::~LeakCompEnabledSetting(void)	
{
	CALL_TRACE("~LeakCompEnabledSetting(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[LC02009] Leak Compensation is Disabled when Mandatory Breath 
//             Type is VC+ or Spontaneous Breath Type is TC, PA or VS..
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
	LeakCompEnabledSetting::getApplicability(const Notification::ChangeQualifier qualifierId) const
{
	CALL_TRACE("getApplicability(qualifierId)");

	Applicability::Id  applicabilityId = Applicability::CHANGEABLE; 

	const DiscreteValue VENT_TYPE_VALUE = 
		AcceptedContextHandle::GetDiscreteValue(SettingId::VENT_TYPE);

	const DiscreteValue MAND_TYPE_VALUE = 
		AcceptedContextHandle::GetDiscreteValue(SettingId::MAND_TYPE);


	// When Leak Comp software option is enabled, determine whether
	// to display Leak Comp or not based on the current user settings.  
	// If the Leak Comp Software option is disabled,
	// Leak Comp is not applicable.
	if (SoftwareOptions::IsOptionEnabled(SoftwareOptions::LEAK_COMP))
	{

		if (MAND_TYPE_VALUE == MandTypeValue::VCP_MAND_TYPE)
		{
			applicabilityId = Applicability::VIEWABLE;
		}

		Applicability::Id  SUPPORT_TYPE_APPLIC =
			SettingContextHandle::GetSettingApplicability(
														 ContextId::ACCEPTED_CONTEXT_ID,
														 SettingId::SUPPORT_TYPE
														 );

		if (SUPPORT_TYPE_APPLIC != Applicability::INAPPLICABLE)
		{				
			const DiscreteValue  SUPPORT_TYPE_VALUE =
				AcceptedContextHandle::GetDiscreteValue(SettingId::SUPPORT_TYPE);

			switch (SUPPORT_TYPE_VALUE)
			{
			case SupportTypeValue::PSV_SUPPORT_TYPE:        
			case SupportTypeValue::OFF_SUPPORT_TYPE:    
				// do nothing
				break;
	
			// Disable Leak Compensation
			case SupportTypeValue::ATC_SUPPORT_TYPE:            
			case SupportTypeValue::VSV_SUPPORT_TYPE:	
			case SupportTypeValue::PAV_SUPPORT_TYPE:        
	
				applicabilityId = Applicability::VIEWABLE;
				break;
	
			default:
				AUX_CLASS_ASSERTION_FAILURE(SUPPORT_TYPE_VALUE);
				break;
			}
		}
	}
	else
	{
		applicabilityId = Applicability::INAPPLICABLE;
	}



	return(applicabilityId);
}	// $[TI1]


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [const, virtual]
//
//@ Interface-Description
//  Get the setting's new-patient value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[LC02010] The setting's new-patient value ...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
	LeakCompEnabledSetting::getNewPatientValue(void) const
{
	CALL_TRACE("getNewPatientValue()");

	DiscreteValue  newPatientValue = LeakCompEnabledValue::LEAK_COMP_ENABLED;

	const DiscreteValue VENT_TYPE = 
		SettingsMgr::GetSettingPtr(SettingId::VENT_TYPE)->getAdjustedValue();

	const DiscreteValue  CIRCUIT_TYPE =
		SettingsMgr::GetSettingPtr(SettingId::PATIENT_CCT_TYPE)->getAcceptedValue();

	const DiscreteValue MAND_TYPE = 
		SettingsMgr::GetSettingPtr(SettingId::MAND_TYPE)->getAdjustedValue();

	const DiscreteValue SUPPORT_TYPE = 
		SettingsMgr::GetSettingPtr(SettingId::SUPPORT_TYPE)->getAdjustedValue();

	if ((MAND_TYPE == MandTypeValue::VCP_MAND_TYPE) || 
		(SUPPORT_TYPE == SupportTypeValue::ATC_SUPPORT_TYPE) ||
		(SUPPORT_TYPE == SupportTypeValue::VSV_SUPPORT_TYPE) ||
		(SUPPORT_TYPE == SupportTypeValue::PAV_SUPPORT_TYPE) ||
		(!SoftwareOptions::IsOptionEnabled(SoftwareOptions::LEAK_COMP)) ||
		(VENT_TYPE == VentTypeValue::INVASIVE_VENT_TYPE && 
		    (CIRCUIT_TYPE == PatientCctTypeValue::ADULT_CIRCUIT ||
		     CIRCUIT_TYPE == PatientCctTypeValue::PEDIATRIC_CIRCUIT))
		
	   )
	{
		newPatientValue = LeakCompEnabledValue::LEAK_COMP_DISABLED;
	}


	return(newPatientValue);
}  // $[TI1]



//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method: acceptTransition(settingId, newValue, currValue) [virtual]
//
//@ Interface-Description
//  This method is responsible for updating this setting based on 
//  mode, mand type and support type setting.  
//---------------------------------------------------------------------
//@ Implementation-Description
//  Leak Compensation shall be disabled under VC+, PAV, TC and VS. 
//  Otherwise Leak Compensation shall be enabled in other setting 
//  configuration.  If the proposed Leak Compensation setting changes,
//  then the DiscoSensSetting class will default the disconnect 
//  sensitivity setting. $[LC02009]
//  When Vent Type is changed to NIV the following transition rules ... 
//    $[NI02010.f]  Dsens will be set to its New Patient value EXCEPT
//    $[NI02010.g] Dsens will be unchanged if Leak Compensation = Enabled...
//    $[NI02010.h]  If Circuit Type = Adult or Pediatric, ...
//  When Vent Type is changed to Invasive the following transition rules... 
//    $[NI02011.f]  Dsens will be set to its New Patient value EXCEPT
//    $[NI02011.g] Dsens will be unchanged if Leak Compensation = Enabled.. 
//    $[NI02011.h]  If Circuit Type = Adult or Pediatric, ...
//---------------------------------------------------------------------
//@ PreCondition
//  (settingId == SettingId::SUPPORT_TYPE) ||
//  (settingId == SettingId::MAND_TYPE) ||
//  (settingId == SettingId::MODE)  ||
//  (settingId == SettingId::VENT_TYPE)
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================

void
	LeakCompEnabledSetting::acceptTransition(
											const SettingId::SettingIdType settingId,
											const DiscreteValue            newValue,
											const DiscreteValue        currValue)
{
	CALL_TRACE("acceptTransition(settingId, newValue, currValue)");

	AUX_CLASS_PRE_CONDITION((settingId == SettingId::SUPPORT_TYPE) ||
							(settingId == SettingId::MAND_TYPE) ||
							(settingId == SettingId::MODE) ||
							(settingId == SettingId::VENT_TYPE), 
							settingId);

	// get an alias to the Accepted Context...
	const AcceptedContext&  rAccContext = ContextMgr::GetAcceptedContext();

	const DiscreteValue NEW_MAND_TYPE_VALUE = 
		SettingsMgr::GetSettingPtr(SettingId::MAND_TYPE)->getAdjustedValue();

	const DiscreteValue NEW_SUPPORT_TYPE_VALUE = 
		SettingsMgr::GetSettingPtr(SettingId::SUPPORT_TYPE)->getAdjustedValue();

	const DiscreteValue CURR_MAND_TYPE_VALUE = 
		SettingsMgr::GetSettingPtr(SettingId::MAND_TYPE)->getAcceptedValue();

	const DiscreteValue CURR_SUPPORT_TYPE_VALUE = 
		SettingsMgr::GetSettingPtr(SettingId::SUPPORT_TYPE)->getAcceptedValue();

	const DiscreteValue  CIRCUIT_TYPE =
		SettingsMgr::GetSettingPtr(SettingId::PATIENT_CCT_TYPE)->getAcceptedValue();

	Applicability::Id  SUPPORT_TYPE_APPLIC =
		SettingContextHandle::GetSettingApplicability(
													 ContextId::ADJUSTED_CONTEXT_ID,
													 SettingId::SUPPORT_TYPE
													 );
	const DiscreteValue NEW_VENT_TYPE_VALUE = 
		SettingsMgr::GetSettingPtr(SettingId::VENT_TYPE)->getAdjustedValue();

	DiscreteValue  newPatientValue;

	// Do not update leak compensation when leak comp option is disabled.
	if (SoftwareOptions::IsOptionEnabled(SoftwareOptions::LEAK_COMP))
	{

		// Handles vent type changes
		if (settingId == SettingId::VENT_TYPE)
		{
		    if(CIRCUIT_TYPE == PatientCctTypeValue::ADULT_CIRCUIT ||
		       CIRCUIT_TYPE == PatientCctTypeValue::PEDIATRIC_CIRCUIT)
            {
                if (newValue == VentTypeValue::INVASIVE_VENT_TYPE)
				{
					newPatientValue = LeakCompEnabledValue::LEAK_COMP_DISABLED;
					setAdjustedValue( (SettingValue) newPatientValue );   

				}
				else
				{
					newPatientValue = LeakCompEnabledValue::LEAK_COMP_ENABLED;
					setAdjustedValue( (SettingValue) newPatientValue );   

				}
			}

		}
		// Handles mode changes.
		else if (settingId == SettingId::MODE)
		{

			// If the new value is AC and it is not under VC+, TC, VS and PA,
			// enable leak compensation.
			if (newValue == ModeValue::AC_MODE_VALUE)
			{
				if ((NEW_MAND_TYPE_VALUE != MandTypeValue::VCP_MAND_TYPE) &&
					((CURR_SUPPORT_TYPE_VALUE == SupportTypeValue::ATC_SUPPORT_TYPE) ||
					 (CURR_SUPPORT_TYPE_VALUE == SupportTypeValue::VSV_SUPPORT_TYPE) ||
					 (CURR_SUPPORT_TYPE_VALUE == SupportTypeValue::PAV_SUPPORT_TYPE)) 
				   )
				{
					if(CIRCUIT_TYPE == PatientCctTypeValue::ADULT_CIRCUIT ||
					   CIRCUIT_TYPE == PatientCctTypeValue::PEDIATRIC_CIRCUIT)
					{
						if (NEW_VENT_TYPE_VALUE == VentTypeValue::INVASIVE_VENT_TYPE)
						{
							newPatientValue = LeakCompEnabledValue::LEAK_COMP_DISABLED;
							setAdjustedValue( (SettingValue) newPatientValue );   

						}
						else
						{
							newPatientValue = LeakCompEnabledValue::LEAK_COMP_ENABLED;
							setAdjustedValue( (SettingValue) newPatientValue );   

						}
					}
				}
			}
			else
			{

				// If the new value is other modes, check if the current changes
				// are not in VC+, TC, VS and PA then enable leak compensation. 
				// Otherwise disable leak compensation if VC+, TC, VS, or PA 
				// is enabled.
				if ((NEW_MAND_TYPE_VALUE != MandTypeValue::VCP_MAND_TYPE) &&
					((CURR_SUPPORT_TYPE_VALUE == SupportTypeValue::ATC_SUPPORT_TYPE) ||
					 (CURR_SUPPORT_TYPE_VALUE == SupportTypeValue::VSV_SUPPORT_TYPE) ||
					 (CURR_SUPPORT_TYPE_VALUE == SupportTypeValue::PAV_SUPPORT_TYPE)) &&
					((NEW_SUPPORT_TYPE_VALUE == SupportTypeValue::OFF_SUPPORT_TYPE) ||
					 (NEW_SUPPORT_TYPE_VALUE == SupportTypeValue::PSV_SUPPORT_TYPE)) 
				   )
				{
					if(CIRCUIT_TYPE == PatientCctTypeValue::ADULT_CIRCUIT ||
					   CIRCUIT_TYPE == PatientCctTypeValue::PEDIATRIC_CIRCUIT)
					{
						if (NEW_VENT_TYPE_VALUE == VentTypeValue::INVASIVE_VENT_TYPE)
						{
							newPatientValue = LeakCompEnabledValue::LEAK_COMP_DISABLED;
							setAdjustedValue( (SettingValue) newPatientValue );   

						}
						else
						{
							newPatientValue = LeakCompEnabledValue::LEAK_COMP_ENABLED;
							setAdjustedValue( (SettingValue) newPatientValue );   

						}
					}
				}
				else if ((NEW_SUPPORT_TYPE_VALUE == SupportTypeValue::ATC_SUPPORT_TYPE) ||
						 (NEW_SUPPORT_TYPE_VALUE == SupportTypeValue::VSV_SUPPORT_TYPE) ||
						 (NEW_SUPPORT_TYPE_VALUE == SupportTypeValue::PAV_SUPPORT_TYPE) 
						)
				{
					newPatientValue = LeakCompEnabledValue::LEAK_COMP_DISABLED;
					setAdjustedValue( (SettingValue) newPatientValue );   

				}


			}

		}
		// Handles Mandatory type changes
		else if (settingId == SettingId::MAND_TYPE)
		{

			// If the current value is VC+ and it is changing to other mandatory types,
			// enable leak compensation if the new support type are not TC, VS and PA.
			// Otherwise disable leak compensation if TC, VS, or PA is enabled.
			if (currValue == MandTypeValue::VCP_MAND_TYPE)
			{
				if ((SUPPORT_TYPE_APPLIC != Applicability::INAPPLICABLE) &&
					((NEW_SUPPORT_TYPE_VALUE == SupportTypeValue::ATC_SUPPORT_TYPE) ||
					 (NEW_SUPPORT_TYPE_VALUE == SupportTypeValue::VSV_SUPPORT_TYPE) ||
					 (NEW_SUPPORT_TYPE_VALUE == SupportTypeValue::PAV_SUPPORT_TYPE))
				   )
				{
					newPatientValue = LeakCompEnabledValue::LEAK_COMP_DISABLED;
					setAdjustedValue( (SettingValue) newPatientValue );   

				}
				else
				{
					if(CIRCUIT_TYPE == PatientCctTypeValue::ADULT_CIRCUIT ||
					   CIRCUIT_TYPE == PatientCctTypeValue::PEDIATRIC_CIRCUIT)
					{
						if (NEW_VENT_TYPE_VALUE == VentTypeValue::INVASIVE_VENT_TYPE)
						{
							newPatientValue = LeakCompEnabledValue::LEAK_COMP_DISABLED;
							setAdjustedValue( (SettingValue) newPatientValue );   

						}
						else
						{
							newPatientValue = LeakCompEnabledValue::LEAK_COMP_ENABLED;
							setAdjustedValue( (SettingValue) newPatientValue );   

						}
					}

				}
			}
			// If the new value is VC+, disable leak compensation.
			else if (newValue == MandTypeValue::VCP_MAND_TYPE)
			{
				newPatientValue = LeakCompEnabledValue::LEAK_COMP_DISABLED;
				setAdjustedValue( (SettingValue) newPatientValue );
			}
		}
		// Handles Support Type changes
		else if ((settingId == SettingId::SUPPORT_TYPE) &&
				 (NEW_MAND_TYPE_VALUE != MandTypeValue::VCP_MAND_TYPE)
				)
		{

			// If the current support type are not TC, VS and PA then 
			// enable leak compensation. Otherwise disable leak compensation 
			// if TC, VS, or PA is enabled.
			if (((currValue == SupportTypeValue::ATC_SUPPORT_TYPE) ||
				 (currValue == SupportTypeValue::VSV_SUPPORT_TYPE) ||
				 (currValue == SupportTypeValue::PAV_SUPPORT_TYPE)) &&
				((newValue == SupportTypeValue::OFF_SUPPORT_TYPE) ||
				 (newValue == SupportTypeValue::PSV_SUPPORT_TYPE)) 
			   )
			{
				if(CIRCUIT_TYPE == PatientCctTypeValue::ADULT_CIRCUIT ||
				   CIRCUIT_TYPE == PatientCctTypeValue::PEDIATRIC_CIRCUIT)
				{
					if (NEW_VENT_TYPE_VALUE == VentTypeValue::INVASIVE_VENT_TYPE)
					{
						newPatientValue = LeakCompEnabledValue::LEAK_COMP_DISABLED;
						setAdjustedValue( (SettingValue) newPatientValue );   

					}
					else
					{
						newPatientValue = LeakCompEnabledValue::LEAK_COMP_ENABLED;
						setAdjustedValue( (SettingValue) newPatientValue );   

					}
				}
			}
			else if ((newValue == SupportTypeValue::ATC_SUPPORT_TYPE) ||
					 (newValue == SupportTypeValue::VSV_SUPPORT_TYPE) ||
					 (newValue == SupportTypeValue::PAV_SUPPORT_TYPE) 
					)
			{
				newPatientValue = LeakCompEnabledValue::LEAK_COMP_DISABLED;
				setAdjustedValue( (SettingValue) newPatientValue );   

			}

		}


	}

	// activation of Transition Rule will be italicized if value has changed
	setForcedChangeFlag();

}




//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//			[static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	LeakCompEnabledSetting::SoftFault(const SoftFaultID  softFaultID,
									  const Uint32       lineNumber,
									  const char*        pFileName,
									  const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

	FaultHandler::SoftFault(softFaultID, ::SETTINGS_VALIDATION,
							LEAK_COMP_ENABLED_SETTING, lineNumber, pFileName,
							pPredicate);
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  calcDependentValues_(isValueIncreasing)  [virtual]
//
//@ Interface-Description
//  Notify this setting's dependent settings of an update to this
//  primary setting.  If a dependent bound is violated, this setting's
//  bound status is updated with the dependent bound information.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean
	LeakCompEnabledSetting::calcDependentValues_(const Boolean isValueIncreasing)
{
	CALL_TRACE("calcDependentValues_()");

	// forward on to base class...
	return(DiscreteSetting::calcDependentValues_(isValueIncreasing));

}


