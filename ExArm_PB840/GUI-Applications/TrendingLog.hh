#ifndef TrendingLog_HH
#define TrendingLog_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: TrendingLog - Generic scrollable log class which allows
// the viewing of information in a tabular form.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/TrendingLog.hhv   25.0.4.0   19 Nov 2013 14:08:40   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By:  gdc	   Date:  06-Mar-2007    SCR Number: 6237
//  Project:  Trend
//  Description:
//		Initial version
//
//====================================================================

#include "GuiAppClassIds.hh"
#include "GuiTable.H"
#include "TrendingScrollbar.hh"

//@ Usage-Classes
//@ End-Usage

static const int NUM_LOG_ROWS = 7;
static const int NUM_LOG_COLS = 8;

class LogTarget;

class TrendingLog 
:	public GuiTable<NUM_LOG_ROWS, NUM_LOG_COLS>
{
public:
	TrendingLog(LogTarget &logTarget, 
				Uint16 numDisplayRows, 
				Uint16 numDisplayCols, 
				Uint16 rowHeight);

	~TrendingLog(void);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

private:
	// these methods are purposely declared, but not implemented...
	TrendingLog(const TrendingLog&);			 // not implemented...
	void   operator=(const TrendingLog&);		 // not implemented...

	virtual void refreshCell_(Uint16 rowNumber, Uint16 columnNumber);

	//@ Data-Member: scrollbar_
	// The specialized trending log scrollbar to the right of the table.
	// It's specialized to scroll through the trending table and update the
	// trend cursor location setting.
	TrendingScrollbar scrollbar_;

	//@ Data-Member: rLogTarget_
	// Reference to a LogTarget that provides textual context for each cell
	// in the log.
	LogTarget& rLogTarget_;
};

#endif // TrendingLog_HH
