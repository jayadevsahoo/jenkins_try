#ifndef Ostream_HH
#define Ostream_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: Ostream - Stream debug output facility for multitasking
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/OS-Foundation/vcssrc/Ostream.hhv   25.0.4.0   19 Nov 2013 14:15:18   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By:  gdc    Date:  12-Dec-2008    SCR Number: 6068
//  Project:  840S
//  Description:
//		Removed SUN prototype code.
// 
//  Revision: 003   By: rhj    Date:  03-Aug-2007    SCR Number: 6397
//  Project: Trend
//  Description:
//      Removed #define SIGMA_OFFICIAL and added comments.
//
//  Revision: 002   By: sah   Date: 13-Jan-1999  DR Number: 5321
//  Project:  ATC
//  Description:
//      Obsoleted the empty '.in' file.
//
//  Revision: 001    By: Gary Cederquist  Date: 15-APR-1997  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//====================================================================

#include "Sigma.hh"
#include "OsFoundation.hh"

class Ostream;
typedef Ostream& (*OmanipFunc)(Ostream&);
typedef void (*VOmanipFunc)(Ostream&);

class Ostream
{
public:

    // The maximum number of flush marks and buf size. 
	enum
	{
		FLUSH_MARK=100,
		BUF_SIZE=128
	};


	Ostream(void);
	~Ostream(void);

	Ostream& operator<<(const char *s);
	Ostream& operator<<(const void *p);
	Ostream& operator<<(const char c);
	Ostream& operator<<(const int i);
	Ostream& operator<<(const unsigned int u);
	Ostream& operator<<(const float f);
	Ostream& operator<<(const double d);

	void     flush(void);
	void     dec(void);
	void     hex(void);
 	void     init(void);
	
    Ostream& operator<<(OmanipFunc func);
    void     operator<<(VOmanipFunc func);

	static void SoftFault(const SoftFaultID softFaultID,
					  	  const Uint32      lineNumber,
					  	  const char*       pFileName  = NULL, 
					  	  const char*       pPredicate = NULL);

private:

	// these methods are purposely declared, but not implemented...
	Ostream(const Ostream&);		// not implemented...
	void   operator=(const Ostream&);	// not implemented...

	void         checkOverFlow_(void);
	Boolean      bufferFull_(void);

    //@ Data-Member: hexFlag_
	// Sets the output format in hex
	Boolean      hexFlag_;

	//@ Data-Member: buf_
	// A buffer which stores data
	char         buf_[Ostream::BUF_SIZE];

	//@ Data-Member: placePtr_
	// A pointer to the buffer.
	char *       placePtr_;

	//@ Data-Member: charCount_
	// Counts the number of characters.
	Int32        charCount_;

	//@ Data-Member: taskName_
	// Stores the current task name
	const char * taskName_;
};


void endl(Ostream& os);
void flush(Ostream& os);
Ostream& hex(Ostream& os);
Ostream& dec(Ostream& os);


class OstreamTbl
{
public:
	static Ostream & GetOstream(void);
    static Ostream & getOstream(void);   
	static void Initialize(void);
};


#define cout OstreamTbl::GetOstream()
#define cerr OstreamTbl::GetOstream()
#define ostream Ostream

#endif // Ostream_HH 

