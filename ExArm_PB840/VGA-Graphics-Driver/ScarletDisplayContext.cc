#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2001, Puritan-Bennett Corporation
//=====================================================================  

// =========================== M O D U L E   D E S C R I P T I O N ====
//@ Class: ScarletDisplayContext -- Display Context for VGA Library Functions
//---------------------------------------------------------------------
//@ Interface-Description
//  This class provides an implementation for the pseudo-interface 
//  DisplayContext. This implementation is specific to the Fujitsu
//  Scarlet graphics chip which is only available on Xena II config
//  of the hardware. 
//---------------------------------------------------------------------
//@ Rationale
//  Contains a context information and drawing interface for the VGA
//  display device.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The ScarletDisplayContext uses the VgaScarlet class to implement most of the
//  hardware specific control of the Fujitsu Scarlet chip.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ $Header:   /840/Baseline/VGA-Graphics-Driver/vcssrc/ScarletDisplayContext.ccv   25.0.4.0   19 Nov 2013 14:37:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003   By: rpr     Date: 11-Mar-2011     SCR Number: 6752
//  Project: XENA2
//      ExpandFont and ExpandIcon passes the original witdth of the font/bitmap
//		so clipping can be accounted for properly.
// 
//  Revision: 002   By: rpr     Date: 11-Mar-2011     SCR Number: 6753
//  Project: XENA2
//      fillTriangle modified to call drawTriangle to fill in edges.
// 
//  Revision: 001   By: mnr     Date: 24-Sep-2010     SCR Number: 6672
//  Project: XENA2
//      Initial check-in.
// 
//=====================================================================  

#include "ScarletDisplayContext.hh"
#include "GraphicsContext.hh"
#include "VgaDib.hh"
#include "VgaFont.hh"
#include "VgaPoint.hh"
#include "VgaLine.hh"
#include "VgaRectangle.hh"
#include "VgaTriangle.hh"
#include <string.h>

//@ Usage-Classes
//@ End-Usage

 
//=====================================================================  
//
//  Public Methods...
//
//=====================================================================  

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ScarletDisplayContext [constructor]
//
//@ Interface-Description
//  Contructor for the ScarletDisplayContext class. 
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
ScarletDisplayContext::ScarletDisplayContext( VgaDisplayId displayId, Int frame )
  : DisplayContext( 640, 480, 8 )
  , rVgaScarlet_( VgaScarlet::GetVgaScarlet( displayId ) )
{  
    pDisplayMemory_ = ((Byte*)(rVgaScarlet_.getPDisplayMemory() ));
    pDisplayFrame_ = pDisplayMemory_ + frame * (width_ * height_);
    memorySize_ = rVgaScarlet_.getMemorySize();
    lockPending_ = FALSE;
    pOffScreenMemory_ = (OffScreenMemory*)(pDisplayMemory_ + rVgaScarlet_.getFrameCount() * width_ * height_);
    pOsmStackTop_ = pOffScreenMemory_->fontData;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~ScarletDisplayContext [destructor]
//
//@ Interface-Description
//  Destructor for the ScarletDisplayContext class.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
ScarletDisplayContext::~ScarletDisplayContext()
{
  // no code
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: accessDisplay
//
//@ Interface-Description
//  Assures the display and display memory is ready to be accessed.
//  For the Scarlet chip, this assures that all the graphics engines 
//  (BitBlt, geometry, pixel) are idle.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
void
ScarletDisplayContext::accessDisplay() const
{
  rVgaScarlet_.accessDisplay();
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: lock
//
//@ Interface-Description
//  Locks the display for accessing display memory or display registers.
//  The client enters its critical section.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
void
ScarletDisplayContext::lock(void)
{
  // $[TI1]
  rVgaScarlet_.lock();
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: unlock
//
//@ Interface-Description
//  Unlocks the display for after accessing display memory or display 
//  registers. The client leaves its critical section.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
void
ScarletDisplayContext::unlock()
{
  // $[TI1]
  rVgaScarlet_.unlock();
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: moveBlock
//
//@ Interface-Description
//  Moves a rectangular block from one display memory location to 
//  another using the BitBlt engine of the VGA controller.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
void 
ScarletDisplayContext::moveBlock(Int xSrc, Int ySrc, Int xDest, Int yDest, 
                          Int xWidth, Int yHeight)
{
  SAFE_AUX_CLASS_PRE_CONDITION(xSrc >= 0, xSrc);
  SAFE_AUX_CLASS_PRE_CONDITION(ySrc >= 0, ySrc);
  SAFE_AUX_CLASS_PRE_CONDITION(xDest >= 0, xDest);
  SAFE_AUX_CLASS_PRE_CONDITION(yDest >= 0, yDest);
  SAFE_AUX_CLASS_PRE_CONDITION(0 <= xWidth && xWidth <= width_, xWidth);
  SAFE_AUX_CLASS_PRE_CONDITION(0 <= yHeight && yHeight <= height_, yHeight);
  SAFE_AUX_CLASS_PRE_CONDITION( yDest * width_ + xDest + (xWidth * yHeight) 
      <= width_ * height_, xWidth * yHeight);

  rVgaScarlet_.accessDisplay();

  rVgaScarlet_.bltCopy( (Uint32)xSrc, (Uint32)ySrc, (Uint32)xDest, (Uint32)yDest, 
                        (Uint32)xWidth, (Uint32)yHeight );
  rVgaScarlet_.flushDispList();
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: allocateImage
//
//@ Interface-Description
// Allocates the off-screen image memory of the size specified.
// Returns NULL if the request cannot be satisfied.
//---------------------------------------------------------------------
//@ Implementation-Description
// In the current impelementation, there is only one off-screen memory
// image available to the application so it just returns a pointer to 
// the beginning of this area.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
void * 
ScarletDisplayContext::allocateImage(Uint32 size)
{
    void * pImage = NULL;

    if (!isOffScreenImageAllocated_ && size <= sizeof(pOffScreenMemory_->offScreenImage))
    {                          
        pImage = pOffScreenMemory_->offScreenImage;
        isOffScreenImageAllocated_ = TRUE;
    }

    return pImage;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: freeImage
//
//@ Interface-Description
// Frees the image in off-screen memory specified by the memory pointer
// parameter.
//---------------------------------------------------------------------
//@ Implementation-Description
// There is a single block of off-screen memory shared by all clients.
// If the address passed to this method is the address of the off-screen
// memory block then reset isOffScreenImageAllocated_. If the address
// is invalid then assert.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
void
ScarletDisplayContext::freeImage(void * pImage)
{
    AUX_CLASS_ASSERTION(pImage == pOffScreenMemory_->offScreenImage, Uint32(pImage));
    CLASS_ASSERTION(isOffScreenImageAllocated_);

    isOffScreenImageAllocated_ = FALSE;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: storeImage
//
//@ Interface-Description
//  Copies from on-screen memory to an allocated off-screen memory
//  block. Client specifies the block's x and y origin, width and 
//  height, and destination memory location returned by allocateImage.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses the BitBLT engine to move the data inside VGA memory and 
//  efficiently compress the data into the offscreen area.
//---------------------------------------------------------------------
//@ PreCondition
// (xSrc >= 0 && xSrc + xWidth <= width_, xSrc)
// && (ySrc >= 0 && ySrc + yHeight <= height_, ySrc)
// && (xWidth * yHeight <= sizeof(pOffScreenMemory_->offScreenImage), xWidth * yHeight)
// && (isInDisplayMemory_(pDest), Uint32(pDest))
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
void 
ScarletDisplayContext::storeImage(void * pDest, Int xSrc, Int ySrc, Int xWidth, Int yHeight)
{
  AUX_CLASS_PRE_CONDITION(xSrc >= 0 && xSrc + xWidth <= width_, xSrc);
  AUX_CLASS_PRE_CONDITION(ySrc >= 0 && ySrc + yHeight <= height_, ySrc);
  AUX_CLASS_PRE_CONDITION(xWidth * yHeight <= sizeof(pOffScreenMemory_->offScreenImage), xWidth * yHeight);
  AUX_CLASS_PRE_CONDITION(isInDisplayMemory_(pDest), Uint32(pDest));
 
  // op is left-to-right and top-to-bottom - not important
  unsigned long destReg  = (unsigned long)(pDest);

  rVgaScarlet_.accessDisplay();

  rVgaScarlet_.bltCopyAlt( (unsigned long)pDisplayFrame_,xSrc,ySrc,destReg, 
                           0, 0, xWidth, yHeight );
  rVgaScarlet_.flushDispList();

}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: recoverImage
//
//@ Interface-Description
//  Recreates the block stored in off-screen memory by storeImage back
//  onto the screen. Client specifies the block's x and y destination, 
//  width and height, and source off-screen memory location.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses the BitBLT engine to move the data inside VGA memory and 
//  expand the data to on-screen memory.
//---------------------------------------------------------------------
//@ PreCondition
// (xDest >= 0 && xDest + xWidth <= width_, xDest)
// && (yDest >= 0 && yDest + yHeight <= height_, yDest)
// && (xWidth * yHeight <= sizeof(pOffScreenMemory_->offScreenImage), xWidth * yHeight)
// && (isInDisplayMemory_(pSrc), Uint32(pSrc))
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
void 
ScarletDisplayContext::recoverImage(Int xDest, Int yDest, const void * pSrc, 
                                Int xWidth, Int yHeight)
{
  AUX_CLASS_PRE_CONDITION(xDest >= 0 && xDest + xWidth <= width_, xDest);
  AUX_CLASS_PRE_CONDITION(yDest >= 0 && yDest + yHeight <= height_, yDest);
  AUX_CLASS_PRE_CONDITION(xWidth * yHeight <= sizeof(pOffScreenMemory_->offScreenImage), xWidth * yHeight);
  AUX_CLASS_PRE_CONDITION(isInDisplayMemory_(pSrc), Uint32(pSrc));

  unsigned long srcReg  = (unsigned long)(pSrc);

  rVgaScarlet_.accessDisplay();

  rVgaScarlet_.bltCopyAlt( srcReg,0,0,(unsigned long)pDisplayFrame_, 
                           xDest, yDest, xWidth, yHeight );
  rVgaScarlet_.flushDispList();

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: fillSolid
//
//@ Interface-Description
//  Performs a "solid fill" to the area specified using the BitBlt
//  engine of the VGA controller.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
void 
ScarletDisplayContext::fillSolid(Int xDest, Int yDest, Int xWidth, Int yHeight, Int color)
{
  SAFE_AUX_CLASS_PRE_CONDITION(xDest >= 0, xDest);
  SAFE_AUX_CLASS_PRE_CONDITION(yDest >= 0, yDest);
  SAFE_AUX_CLASS_PRE_CONDITION(0 <= xWidth && xWidth <= width_, xWidth);
  SAFE_AUX_CLASS_PRE_CONDITION(0 <= yHeight && yHeight <= height_, yHeight);
  SAFE_AUX_CLASS_PRE_CONDITION( yDest * width_ + xDest + (xWidth * yHeight) 
      <= width_ * height_, xWidth * yHeight);

  rVgaScarlet_.accessDisplay();
  rVgaScarlet_.setColor( (unsigned long) color );
  rVgaScarlet_.fillSolid( xDest, yDest, xWidth, yHeight );
  rVgaScarlet_.flushDispList();

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: expandFont
//
//@ Interface-Description
//  Performs a font expansion to display memory from the font data
//  specified using the BitBlt engine of the VGA controller.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
void 
ScarletDisplayContext::expandFont(Int xDest, Int yDest, Int xWidth, Int yHeight,
                           const Byte * pFontData, Uint32 fontOffset,
                           Int color, Int orignalWidth)
{
    if ( xWidth > 0 && yHeight > 0 && fontOffset > 0 )
    {       
        Int fontDimension = fontOffset * yHeight;
        // number of longwords to copy plus one for non- longword alignment
        const unsigned long UINT32_TO_COPY = (fontDimension-1)/sizeof(Uint32)+2;
        
        if ( !isInDisplayMemory_( pFontData ) )
        {                                   
            // copy data 4 bytes at a time to scratchpad memory
            Uint32 * pStartFont    = (Uint32*)pOffScreenMemory_->scratchPad;
            Uint32 * pOffScreenMem = pStartFont;
            Uint32 * pSystemMem    = (Uint32*)(Uint32(pFontData) & ~3);
            
            if (fontDimension > sizeof(pOffScreenMemory_->scratchPad))
            {                                   
                fontDimension = sizeof(pOffScreenMemory_->scratchPad);
            }                                   
            
            rVgaScarlet_.accessDisplay();
      
            // copy font bits to off-screen memory scratchpad area
            for (Int m=0; m < UINT32_TO_COPY; m++)
            {
                *pOffScreenMem++ = *pSystemMem++;
            }
      
            pFontData = (Byte*)pStartFont + ( Uint32(pFontData) & 3 );
        }                                           

        rVgaScarlet_.accessDisplay();
        rVgaScarlet_.drawFont( (Uint16)xDest, (Uint16)yDest, (Uint16)xWidth, 
                               (Uint16)yHeight, pFontData, (unsigned long)color, orignalWidth);
        rVgaScarlet_.flushDispList(); 

    }                                               
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: expandIcon
//
//@ Interface-Description
//  Performs an icon expansion to display memory from the icon data
//  specified using the BitBlt engine of the VGA controller.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
void 
ScarletDisplayContext::expandIcon(Int xDest, Int yDest, Int xWidth, Int yHeight,
                           const Byte * pFontData, Uint32 fontOffset,
                           Int color, Int orignalWidth)
{
    if ( xWidth > 0 && yHeight > 0 && fontOffset > 0 )
    {       
        Int fontDimension = fontOffset * yHeight;
        // number of longwords to copy plus one for non- longword alignment
        const unsigned long UINT32_TO_COPY = (fontDimension-1)/sizeof(Uint32)+2;
        
        if ( !isInDisplayMemory_( pFontData ) )
        {                                   
            // copy data 4 bytes at a time to scratchpad memory
            Uint32 * pStartFont    = (Uint32*)pOffScreenMemory_->scratchPad;
            Uint32 * pOffScreenMem = pStartFont;
            Uint32 * pSystemMem    = (Uint32*)(Uint32(pFontData) & ~3);
            
            if (fontDimension > sizeof(pOffScreenMemory_->scratchPad))
            {                                   
                fontDimension = sizeof(pOffScreenMemory_->scratchPad);
            }                                   
            
            rVgaScarlet_.accessDisplay();
      
            // copy font bits to off-screen memory scratchpad area
            for (Int m=0; m < UINT32_TO_COPY; m++)
            {
                *pOffScreenMem++ = *pSystemMem++;
            }
      
            pFontData = (Byte*)pStartFont + ( Uint32(pFontData) & 3 );
        }                                           

        rVgaScarlet_.accessDisplay();
        rVgaScarlet_.drawIcon( (Uint16)xDest, (Uint16)yDest, (Uint16)xWidth, 
                               (Uint16)yHeight, (unsigned long*)pFontData, (unsigned long)color, orignalWidth); 
        rVgaScarlet_.flushDispList(); 
    }                                               
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: allocateColor
//
//@ Interface-Description
//  Allocates a colormap entry for the specified RgbQuad.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses Vga::allocateColor() method to allocate the entry in the VGA
//  colormap.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
void
ScarletDisplayContext::allocateColor(Byte & index, const RgbQuad & rgb)
{
    rVgaScarlet_.allocateColor(index, rgb);
}
 
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: storeColor
//
//@ Interface-Description
//  Sets the specified colormap entry with the colors specified. 
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses VgaScarlet::storeColor() method to store the entry in the VGA
//  colormap.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
void 
ScarletDisplayContext::storeColor(Byte index, const RgbQuad & rgb, Boolean readOnly)
{
   rVgaScarlet_.storeColor(index, rgb, readOnly);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getColor
//
//@ Interface-Description
//  Gets the specified colormap entry for specified index and stores it
//  in the specified RgbQuad.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses Vga::getColor() method to get the RgbQuad entry from the VGA
//  colormap.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
void 
ScarletDisplayContext::getColor(Byte index, RgbQuad & rgb)
{
  // $[TI1]
  rVgaScarlet_.getColor(index, rgb);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: showFrame
//
//@ Interface-Description
//  Just a placeholder to adhere to parent class inheritance.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Nothing to be done.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
void
ScarletDisplayContext::showFrame(void) 
{
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: drawPoint
//
//@ Interface-Description
//  Renders a point on the VGA display at the specified coordinates.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses VgaScarlet::drawPoint() to draw the point on the VGA display.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
void  
ScarletDisplayContext::drawPoint(
                    GraphicsContext & rGc, 
                    Int x, Int y)
{
  Byte color = rGc.getForegroundColor();

  if (x >= rGc.getClipX0()  &&  x <= rGc.getClipX1()  &&
      y >= rGc.getClipY0()  &&  y <= rGc.getClipY1())
  { 
     rVgaScarlet_.setColor(color);
     rVgaScarlet_.drawPoint( (float)x, (float)y );
     rVgaScarlet_.flushDispList();
  }  
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: drawLine
//
//@ Interface-Description
//  Renders a line to the VGA display at the specified coordinates.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses VgaScarlet::drawLine() to draw the line on the VGA display.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
void  
ScarletDisplayContext::drawLine(
                    GraphicsContext & rGc, 
                    Int x0, Int y0, Int x1, Int y1)
{
    if (x0 == x1)
    {                                                   // $[TI1.1] vertical line
        VgaRectangle::FillBox( *this, rGc, x0, y0, x0+rGc.getLineWidth()-1, y1 );
    }
    else if (y0 == y1)
    {                                                   // $[TI1.2] horizontal line
        VgaRectangle::FillBox( *this, rGc, x0, y0, x1, y0+rGc.getLineWidth()-1 );
    }
    else
    {   
        if (rGc.clipLine(&x0, &y0, &x1, &y1))
        {
            Byte color = rGc.getForegroundColor();
            rVgaScarlet_.setColor(color);
            rVgaScarlet_.drawLine( (float)x0, (float)y0, (float)x1, (float)y1,
                                   rGc.getLineWidth() );
            rVgaScarlet_.flushDispList();
        }
    }
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: fillRectangle
//
//@ Interface-Description
//  Renders a filled rectangle to the VGA display at the specified
//  coordinates.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses VgaRectangle::FillBox() to draw the rectangle on the VGA display.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
void  
ScarletDisplayContext::fillRectangle( 
                    GraphicsContext & rGc, 
                    Int x0, Int y0, Int x1, Int y1 )
{
  VgaRectangle::FillBox(*this, rGc, x0, y0, x1, y1);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: drawTriangle
//
//@ Interface-Description
//  Renders a non-filled triangle to the VGA display at the specified
//  coordinates.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses three calls to drawLine() to draw the triangle on the VGA display.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
void  
ScarletDisplayContext::drawTriangle( 
                    GraphicsContext & rGc,
                    Int x0, Int y0,
                    Int x1, Int y1,
                    Int x2, Int y2 )
{
  drawLine(rGc, x0, y0, x1, y1);
  drawLine(rGc, x0, y0, x2, y2);
  drawLine(rGc, x1, y1, x2, y2);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: fillTriangle
//
//@ Interface-Description
//  Renders a filled triangle to the VGA display at the specified
//  coordinates.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses VgaScarlet::fillTriangle() to draw the filled triangle on the
//  VGA display.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
void  
ScarletDisplayContext::fillTriangle( 
                    GraphicsContext & rGc,
                    Int x0, Int y0,
                    Int x1, Int y1,
                    Int x2, Int y2 )
{
	rVgaScarlet_.accessDisplay();
    rVgaScarlet_.setColor( rGc.getForegroundColor() );
    drawTriangle(rGc, x0, y0, x1, y1, x2, y2); 
    rVgaScarlet_.fillTriangle( (float)x0, (float)y0, (float)x1, (float)y1,
                               (float)x2, (float)y2 );
    rVgaScarlet_.flushDispList();
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: drawBitmap
//
//@ Interface-Description
//  Renders the specified bitmap to the VGA display.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses VgaDib::DisplayBitmap() to draw the bitmap on the VGA display.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
void  
ScarletDisplayContext::drawBitmap( 
                    GraphicsContext & rGc,
                    DibInfo & rDibInfo,
                    Int xPos, Int yPos )
{
 
  // TODO E600 commented out due to link issues
  VgaDib::DisplayBitmap(*this, rGc, rDibInfo, xPos, yPos);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: drawText
//
//@ Interface-Description
//  Writes the specified character string to the VGA display.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses VgaFont::TextOut() to render the character string to the display.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
void  
ScarletDisplayContext::drawText(
                    GraphicsContext & rGc,
                    Int xPos, Int yPos, Uint fontId, const char* pString)
{
  // $[TI1]
  //VgaFont::TextOut(*this, rGc, xPos, yPos, fontId, pString);  //TODO E600 port -- need to check while implementing fonts
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: drawJapaneseText
//
//@ Interface-Description
//  Writes the specified Japanese character string to the VGA display.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses VgaFont::TextOutJ() to render the character string to the display.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
void  
ScarletDisplayContext::drawJapaneseText(
                    GraphicsContext & rGc,
                    Int xPos, Int yPos, Uint fontId, const char* pString)
{
  // $[TI1]
  //VgaFont::TextOutJ(*this, rGc, xPos, yPos, fontId, pString);   //TODO E600 port -- need to check while implementing fonts
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: loadFont
//
//@ Interface-Description
//  Writes font data into the off screen font memory of the VGA. Returns
//  the offset into the VGA where the font data is stored. Returns
//  NULL if there is no more available off screen memory.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses accessDisplay() to assure that the frame buffer is not busy
//  prior to accessing it.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 

Uint32
ScarletDisplayContext::loadFont( const VgaFontData::CharHeader & rFontHeader )
{
  Uint32 returnOffset = NULL;
  Uint32 fontOffset = (rFontHeader.width - 1) / 8 + 1;
  Uint32 fontDimension = fontOffset * rFontHeader.height;  // in bytes

  // check for space available in off-screen memory
  if ( pOsmStackTop_ + fontDimension < pDisplayMemory_ + memorySize_ )
  {                                             // $[TI2.1]
    rVgaScarlet_.accessDisplay();
    memcpy( pOsmStackTop_, rFontHeader.pBits, fontDimension );

    returnOffset = pOsmStackTop_ - pDisplayMemory_;

    // increment off-screen memory pointer
    pOsmStackTop_ += fontDimension;
  }                                             // $[TI2.2]

  return returnOffset;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
ScarletDisplayContext::SoftFault(const SoftFaultID softFaultID,
           const Uint32 lineNumber,
           const char *pFileName,
           const char *pPredicate)
{
  // $[TI1]
  FaultHandler::SoftFault(softFaultID, VGA_GRAPHICS_DRIVERS,
              VgaGraphicsDriver::SCARLET_DISPLAY_CONTEXT, 
              lineNumber, pFileName, pPredicate);
}

//=====================================================================  
//
//  Private Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isInDisplayMemory_
//
//@ Interface-Description
//  Returns TRUE if the address specified is in display memory.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
Boolean
ScarletDisplayContext::isInDisplayMemory_( const void * pMemory ) const
{
  // $[TI1.1] $[TI1.2]
  return(   Uint32(pMemory) >= Uint32(pDisplayMemory_)
         && Uint32(pMemory) < Uint32(pDisplayMemory_ + memorySize_) ) ;
}

