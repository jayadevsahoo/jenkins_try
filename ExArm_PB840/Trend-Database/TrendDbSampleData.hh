#ifndef TrendDbSampleData_HH
#define TrendDbSampleData_HH


//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
// Class: TrendDbSampleData
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Trend-Database/vcssrc/TrendDbSampleData.hhv   25.0.4.0   19 Nov 2013 14:34:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By:  mjf    Date:  28-May-2007    SCR Number: 6237
//       Project:  Trend
//       Description:
//             Initial version
//
// 
//=====================================================================

#include "TrendDataValues.hh"
#include "TrendDataMgr.hh"

#include "TrendSelectValue.hh"    // Setting-Validation



class TrendDbSampleData
{
public:

	void clear( void );

	inline const TrendValue & 
		getValue( TrendTimeScaleValue::TrendTimeScaleValueId timescale = 
				  TrendTimeScaleValue::TREND_TIME_SCALE_1HR );
	virtual void               
		setValue( const TrendValue & newValue,
				  TrendTimeScaleValue::TrendTimeScaleValueId timescale = 
				  TrendTimeScaleValue::TREND_TIME_SCALE_1HR );

	//@ Type: TrendEnumId
	// Union of the PatientItemId and SettingIdType
	union TrendEnumId
	{
		PatientDataId::PatientItemId    patient;
		SettingId::SettingIdType        setting;
		Uint16                          rawValue;
	};

	inline TrendEnumId getId( void );
	inline TrendSelectValue::TrendSelectValueId getTrendId( void );
	virtual void compressData( TrendTimeScaleValue::TrendTimeScaleValueId timescale ) = 0;
	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);


protected:

	TrendDbSampleData( PatientDataId::PatientItemId id,
					   TrendSelectValue::TrendSelectValueId trendId );
	TrendDbSampleData( SettingId::SettingIdType id,
					   TrendSelectValue::TrendSelectValueId trendId );
	virtual ~ TrendDbSampleData();
	virtual void clear_() = 0;

	//@ Data-Member: valueArray_
	// Array of values, one for each timescales    
	TrendValue valueArray_[ TrendTimeScaleValue::TOTAL_TREND_TIME_SCALE_VALUES ];

	//@ Data-Member: id_
	// The patient Id that this value is tied to or SettingIdType
	TrendEnumId id_;

	//@ Data-Member: trendId_
	// Aux method to identify a trend parameter via the TrendSelectValueId enum
	TrendSelectValue::TrendSelectValueId trendId_;

private:
	// no copy or assignment constructor
	TrendDbSampleData( const TrendDbSampleData& );	// no implemented
	void operator=( const TrendDbSampleData& );		// no implemented

};

// Inlined methods
#include "TrendDbSampleData.in"

#endif // TrendDbSampleData_HH
