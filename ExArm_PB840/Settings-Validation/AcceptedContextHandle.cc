#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: AcceptedContextHandle -  Handle to the Accepted Settings.
//---------------------------------------------------------------------
//@ Interface-Description
//  This class provides an external interface to the Settings-Validation
//  Subsystem, for the functionality relating to the "accepted" settings.
//  This class provides static methods for reading the values of the
//  currently accepted settings.
//
//  The purpose of this class is to provide an interface for all of the
//  other subsystems that need to interact with the accepted settings.
//  This class provides a clear demarcation line between the functionality
//  of this subsystem's internal classes, and the functionality needed by
//  other subsystems.
//
//  This class is only available on the GUI CPU.
//---------------------------------------------------------------------
//@ Rationale
//  This class provides a clear border between the Settings-Validation
//  Subsystem and the subsystems that need to get values of the currently
//  accepted settings.
//---------------------------------------------------------------------
//@ Implementation-Description
//  All of the interface methods are set up as static, non-inline methods.
//  The fact that the methods are static, allows the other subsystems to
//  have access to the needed functionality, without needing access to an
//  instance of the class; direct calls to these methods could be made.
//  By making all of the methods non-inline, the client's dependency on
//  the classes and types needed for only the implementation is eliminated.
//  In other words, the non-inline methods allow for the header files needed
//  for the implementation to be included in the source file and NOT in the
//  header file.
//
//  None of these methods are set up, explicitly, for multiple thread
//  protection.  This class READS data from the Accepted Context, and it is
//  the responsibility of the Accepted Context to protect itself during an
//  update.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//  None
//---------------------------------------------------------------------
//@ Invariants
//  None
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/AcceptedContextHandle.ccv   25.0.4.0   19 Nov 2013 14:27:08   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004   By: sah   Date:  04-Mar-1999    DR Number: 5322
//  Project:  ATC
//  Description:
//	Project-related changes:
//	*  using SafetyPcvSettingValue's new, generic interface
//	*  removed obsolete block of "special" code
//
//  Revision: 003   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//  
//  Revision: 002   By: sah    Date:  13-Oct-1997    DR Number: 2360
//  Project: Sigma (R8027)
//  Description:
//	Added the "warping" of expiratory time and I:E ratio, and their
//	apnea counterparts, because now these four values are allowed
//	to "float" between resolution values.
//  
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//=====================================================================

#include "AcceptedContextHandle.hh"

//@ Usage-Classes
#include "ContextMgr.hh"
#include "AcceptedContext.hh"
#include "SafetyPcvSettingValues.hh"
#include "BoundedSetting.hh"
#include "SettingsMgr.hh"
#include "BoundedRange.hh"
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetBoundedValue(boundedId)  [static]
//
//@ Interface-Description
//  Return a copy of the value of the bounded setting indicated by
//  'boundedId'.
//---------------------------------------------------------------------
//@ Implementation-Description
//  When the BD is in a state where Safety-PCV is the only ventilation
//  mode that is allowed (indicated by the 'isBdInSafetyPcvState()' method
//  of the AcceptedContext), the Safety-PCV value is to be returned, instead
//  of the currently-accepted value.
//---------------------------------------------------------------------
//@ PreCondition
//  (SettingId::IsBoundedId(boundedId))
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

BoundedValue
AcceptedContextHandle::GetBoundedValue(
				const SettingId::SettingIdType boundedId
				      )
{
  CALL_TRACE("GetBoundedValue(boundedId)");
  CLASS_PRE_CONDITION((SettingId::IsBoundedId(boundedId)));

  const AcceptedContext&  rAccContext = ContextMgr::GetAcceptedContext();

  BoundedValue  boundedValue;

  if (rAccContext.isBdInSafetyPcvState())
  {   // $[TI1] -- BD in Safety-PCV state; use Safety-PCV values...
    boundedValue = BoundedValue(SafetyPcvSettingValues::GetValue(boundedId));
  }
  else
  {   // $[TI2] -- BD NOT in Safety-PCV; use currently-accepted values...
    boundedValue = rAccContext.getBoundedValue(boundedId);
  }

  return(boundedValue);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetDiscreteValue(discreteId)  [static]
//
//@ Interface-Description
//  Return a copy of the value of the discrete setting indicated by
//  'discreteId'.
//---------------------------------------------------------------------
//@ Implementation-Description
//  When the BD is in a state where Safety-PCV is the only ventilation
//  mode that is allowed (indicated by the 'isBdInSafetyPcvState()' method
//  of the AcceptedContext), the Safety-PCV value is to be returned, instead
//  of the currently-accepted value.
//---------------------------------------------------------------------
//@ PreCondition
//  (SettingId::IsDiscreteId(discreteId))
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

DiscreteValue
AcceptedContextHandle::GetDiscreteValue(
				  const SettingId::SettingIdType discreteId
				       )
{
  CALL_TRACE("GetDiscreteValue(discreteId)");
  CLASS_PRE_CONDITION((SettingId::IsDiscreteId(discreteId)));

  const AcceptedContext&  rAccContext = ContextMgr::GetAcceptedContext();

  DiscreteValue  discreteValue;

  if (rAccContext.isBdInSafetyPcvState())
  {   // $[TI1] -- BD in Safety-PCV state; use Safety-PCV values...
    discreteValue = DiscreteValue(SafetyPcvSettingValues::GetValue(discreteId));
  }
  else
  {   // $[TI2] -- BD NOT in Safety-PCV; use currently-accepted values...
    discreteValue = rAccContext.getDiscreteValue(discreteId);
  }

  return(discreteValue);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetSequentialValue(sequentialId)  [static]
//
//@ Interface-Description
//  Return a copy of the value of the sequential setting indicated by
//  'sequentialId'.
//---------------------------------------------------------------------
//@ Implementation-Description
//  When the BD is in a state where Safety-PCV is the only ventilation
//  mode that is allowed (indicated by the 'isBdInSafetyPcvState()' method
//  of the AcceptedContext), the Safety-PCV value is to be returned, instead
//  of the currently-accepted value.
//---------------------------------------------------------------------
//@ PreCondition
//  (SettingId::IsSequentialId(sequentialId))
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SequentialValue
AcceptedContextHandle::GetSequentialValue(
				const SettingId::SettingIdType sequentialId
					 )
{
  CALL_TRACE("GetSequentialValue(sequentialId)");
  CLASS_PRE_CONDITION((SettingId::IsSequentialId(sequentialId)));

  const AcceptedContext&  rAccContext = ContextMgr::GetAcceptedContext();

  SequentialValue  sequentialValue;

  if (rAccContext.isBdInSafetyPcvState())
  {   // $[TI1] -- BD in Safety-PCV state; use Safety-PCV values...
    sequentialValue =
	  SequentialValue(SafetyPcvSettingValues::GetValue(sequentialId));
  }
  else
  {   // $[TI2] -- BD NOT in Safety-PCV; use currently-accepted values...
    sequentialValue = rAccContext.getSequentialValue(sequentialId);
  }

  return(sequentialValue);
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  SetBoundedValue(boundedId, boundedValue)  [static]
//
// Interface-Description
//  Set the bounded setting identified by 'boundedId' to the value
//  given by 'boundedValue'.
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//  (SettingId::IsBoundedId(boundedId))
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

void
AcceptedContextHandle::SetBoundedValue(
			    const SettingId::SettingIdType boundedId,
			    const BoundedValue&            boundedValue
				      )
{
  CALL_TRACE("SetBoundedValue(boundedId, boundedValue)");
  CLASS_PRE_CONDITION((SettingId::IsBoundedId(boundedId)));

  AcceptedContext&  rAccContext = ContextMgr::GetAcceptedContext();

  if (SettingId::IsBatchId(boundedId))
  {
    rAccContext.setBatchBoundedValue(boundedId, boundedValue);
  }
  else
  {
    rAccContext.setNonBatchBoundedValue(boundedId, boundedValue);
  }
}


//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  SetDiscreteValue(discreteId, discreteValue)  [static]
//
// Interface-Description
//  Set the discrete setting identified by 'discreteId' to the value
//  given by 'discreteValue'.
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//  (SettingId::IsDiscreteId(discreteId))
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

void
AcceptedContextHandle::SetDiscreteValue(
			    const SettingId::SettingIdType discreteId,
			    const DiscreteValue             discreteValue
				      )
{
  CALL_TRACE("SetDiscreteValue(discreteId, discreteValue)");
  CLASS_PRE_CONDITION((SettingId::IsDiscreteId(discreteId)));

  AcceptedContext&  rAccContext = ContextMgr::GetAcceptedContext();

  if (SettingId::IsBatchId(discreteId))
  {
    rAccContext.setBatchDiscreteValue(discreteId, discreteValue);
  }
  else
  {
    rAccContext.setNonBatchDiscreteValue(discreteId, discreteValue);
  }
}


//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  SetSequentialValue(sequentialId, sequentialValue)  [static]
//
// Interface-Description
//  Set the sequential setting identified by 'sequentialId' to the value
//  given by 'sequentialValue'.
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//  (SettingId::IsSequentialId(sequentialId))
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

void
AcceptedContextHandle::SetSequentialValue(
			    const SettingId::SettingIdType sequentialId,
			    const SequentialValue          sequentialValue
					 )
{
  CALL_TRACE("SetSequentialValue(sequentialId, sequentialValue)");
  CLASS_PRE_CONDITION((SettingId::IsSequentialId(sequentialId)));

  AcceptedContext&  rAccContext = ContextMgr::GetAcceptedContext();

  if (SettingId::IsBatchId(sequentialId))
  {
    rAccContext.setBatchSequentialValue(sequentialId, sequentialValue);
  }
  else
  {
    rAccContext.setNonBatchSequentialValue(sequentialId, sequentialValue);
  }
}

#endif // defined(SIGMA_DEVELOPMENT)


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//            [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//  and 'lineNumber' are essential pieces of information.  The
//  'pFileName' and 'pPredicate' strings may be defaulted in the macro
//  to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
AcceptedContextHandle::SoftFault(const SoftFaultID  softFaultID,
			         const Uint32       lineNumber,
			         const char*        pFileName,
			         const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
			  ACCEPTED_CONTEXT_HANDLE, lineNumber, pFileName,
			  pPredicate);
}
