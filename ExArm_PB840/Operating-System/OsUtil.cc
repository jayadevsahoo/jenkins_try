#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// =========================== M O D U L E   D E S C R I P T I O N ====
//@ Filename: OsUtil.cc - Low-level OS Utilities.
//---------------------------------------------------------------------
//@ Interface-Description
//---------------------------------------------------------------------
//@ Module-Decomposition
//      SetProcessorIPL
//      IsGuiCpu
//      IsFlashEnabled
//---------------------------------------------------------------------
//@ Rationale
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Operating-System/vcssrc/OsUtil.ccv   25.0.4.0   19 Nov 2013 14:16:20   pvcs  $
//
//@ Modification-Log
//
//   Revision  008  By: gdc         Date: 27-Jan-2011   SCR Number: 6706
//   Project:   XENA2
//   Description:
//         Moved IsXena2Config to CpuDevice class in POST-Library
//         where Kernel POST can link to it properly.
//
//   Revision  007  By: mnr         Date: 02-Dec-2010   SCR Number: 6671,6672
//   Project:   XENA2
//   Description:
//         Code review action item related updates.
//         Added IsXena2Config() function.
//
//   Revision  005  By: gdc         Date: 24-Aug-2009   SCR Number: 6147
//      Project:   XB
//      Description:
//         Removed IsVentInopActive() function as dead code.
//
//   Revision  004  By: Rhomere Jimenez  Date: 22-June-2006  DR Number: 6192
//      Project:   Xena
//      Description:
//         Added IsXenaConfig() function.
//
//   Revision: 003    By: gdc       Date: 07-Feb-2001   DCS Number: 5493
//      Project: GuiComms
//      Description:
//               Added IsGuiCommsConfig() function.
//
//   Revision 002   By: Gary Cederquist Date: 18-FEB-1998 DR Number: XXXX
//      Project:   Sigma   (R8027)
//      Description:
//         Added IsUpperDisplayColor() and IsLowerDisplayColor() functions.
//
//   Revision 001   By: Gary Cederquist Date: 28-MAY-1997 DR Number: 2176
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version.
//
//=====================================================================

#include "Sigma.hh"
#include "OsUtil.hh"
#include "MemoryMap.hh"
#include "NmiSource.hh"
#include "CpuDevice.hh"
#include "postnv.hh"
#include "BdIoUtil.hh"

#ifdef SIGMA_UNIT_TEST
extern  Uint8  SimIoRegister1;
extern  Uint8  SimIoRegister2;
extern  Uint8  SimNmiRegister;
extern  Uint8  SimDiagLedRegister;

#define IO_REGISTER_1  (&SimIoRegister1)
#define IO_REGISTER_2  (&SimIoRegister2)
#define NMI_REGISTER   (&SimNmiRegister)
#define DIAG_LED_REG   (&SimDiagLedRegister)
#endif

static  Uint8 IoConfig1_;

//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function:  BoardInit
//
//@ Interface-Description
//  Called by application initialization to disable the watchdog
//  strobing interrupt in POST and enable to VRTX 5ms timer interrupt
//  and Ethernet (LAN) interrupt.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The autovector mask register is used to mask the appropriate
//  autovector interrupts.
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Free-Function
//=====================================================================
void
BoardInit(void)
{
	// TODO E600 port
	/*
    // enable LAN and VRTX timer interrupts
    CpuDevice::EnableAutovector( ENABLE_AUTOVEC_5 | ENABLE_AUTOVEC_6 );

    IoConfig1_ = PKernelNovram->ioReg1Cache;
	*/
}

//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function:  SetProcessorIPL
//
//@ Interface-Description
// Set the processor's Interrupt Priority Level (IPL) to the passed value.
// Valid IPLs are 0 through 7, with interrupts at levels less than or equal
// to the IPL being masked off.  Thus, 0 masks nothing while 7 masks all.
// Notice that exceptions and traps cannot be masked off, only interrupts.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses the SigmaTrapHandler to call SetProcIpl which modifies the
//  SR on the exception stack.
//
//  Prior to calling this function, SigmaTrapHandler is installed 
//  during initialization to handle TRAP #1.  The POST unexpected 
//  exception handler will properly handle the TRAP #1 as an error 
//  if this function is called prior to installing the SigmaTrapHandler.
//
//  Stack just before TRAP
//      31               0
//      +----------------+
//      | return address |    +0  <- sp (A7)
//      +----------------+
//      |  Uint8 ipl arg |    +4
//      +----------------+
//
//  D1 holds new IPL prior to trap, old IPL after trap
//
//  NOTE: MRI C++ compiler assumes functions destroy D0, D1, A0, A1.
//
//  WARNING: we pass information in D1 since stacks may be switched
//           on us (if we're moving from user to supervisor mode)
//
//---------------------------------------------------------------------
//@ PreConditions
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Free-Function
//=====================================================================

Uint8
SetProcessorIPL(Uint8 ipl)
{
	// TODO E600 port
	/*
    asm(" clr.l   d1             ");  // clear argument register
    asm(" move.b  `ipl`,d1       ");  // pass the ipl argument
    asm(" move.l  #SETPROCIPL,d0 ");  // SetProcIpl function code
    return asm(" trap #SIGMATRAP ");  // SetProcIpl sets return value
	*/
    // $[TI1]

	return 0;
}


//=====================================================================
//
// *** IO_REGISTER_1 READ DEFINITIONS ***
//
//=====================================================================
static const Uint8 IO_1_RD_BD_CPU        = 0x80;  // BD (1)/GUI (0)
static const Uint8 IO_1_RD_NVRAM_WENABLE = 0x08;  // on (1)/off (0)

#ifdef SIGMA_GUI_CPU
static const Uint8 IO_1_RD_REMOTE_ALARM  = 0x40;  // off (1)/on (0)
static const Uint8 IO_1_RD_UPPER_COLOR   = 0x20;  // no (1)/yes (0)
static const Uint8 IO_1_RD_LOWER_COLOR   = 0x10;  // no (1)/yes (0)
static const Uint8 IO_1_RD_LAN_BKOFF     = 0x02;  // yes (1)/no (0)
#endif // SIGMA_GUI_CPU

#ifdef SIGMA_BD_CPU
static const Uint8 IO_1_RD_FAN_ALARM     = 0x10;  // yes (1)/no (0)
static const Uint8 IO_1_RD_WATCHDOG_BITE = 0x04;  // yes(1)/no (0)
static const Uint8 IO_1_RD_LAN_BKOFF     = 0x02;  // no (1)/yes (0)
#endif // SIGMA_GUI_CPU

static const Uint8 IO_1_RD_FLASH_WRITE   = 0x01;  // on (1)/off (0)

//=====================================================================
//
// *** IO_REGISTER_1 WRITE DEFINITIONS ***
//
//=====================================================================
#if defined(SIGMA_GUI_CPU) || defined(SIGMA_COMMON)

// TODO E600 port


//asm("IO_1_WR_VENT_INOP_BIT      EQU  7");
//asm("IO_1_WR_REMOTE_ALARM_BIT   EQU  6");
//asm("IO_1_WR_AUDIO_RESET_BIT    EQU  5");
#endif // SIGMA_GUI_CPU

/*
asm("IO_1_WR_LAN_RESET_BIT      EQU  4");
asm("IO_1_WR_NVRAM_WENABLE_BIT  EQU  3");
asm("IO_1_WR_FLASH_WRITE_BIT2   EQU  2");
asm("IO_1_WR_FLASH_WRITE_BIT1   EQU  1");
asm("IO_1_WR_FLASH_WRITE_BIT0   EQU  0");
*/

//=====================================================================
//
// ***  IO_REGISTER_2 -- only on GUI  ***
//
//=====================================================================
#ifdef SIGMA_GUI_CPU  
static const Uint8 IO_2_RD_AUDIO_ACK     = 0x80;  // no (1)/yes (0)
static const Uint8 IO_2_RD_AUDIO_ATTN    = 0x20;  // no (1)/yes (0)
static const Uint8 IO_2_RD_WATCHDOG_BITE = 0x08;  // yes (1)/no (0)
static const Uint8 IO_2_RD_LED_ACK       = 0x04;  // no (1)/yes (0)
static const Uint8 IO_2_RD_KEYBD_ACK     = 0x02;  // no (1)/yes (0)
static const Uint8 IO_2_RD_AUDIO_BUSY    = 0x01;  // no (1)/yes (0)
# endif // SIGMA_GUI_CPU


//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function:  ReadIoConfig1_
//
//@ Interface-Description
//  Returns the contents of hardware I/O register 1.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Free-Function
//=====================================================================
static inline Uint8
ReadIoConfig1_(void)
{
    // $[TI1]
	// TODO E600 port
    //return *IO_REGISTER_1;

	return 0;
}

//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function:  WriteIoConfig1_
//
//@ Interface-Description
//  Writes the specified value to hardware I/O register 1.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Free-Function
//=====================================================================
static inline void
WriteIoConfig1_(Uint8 value)
{
	// TODO E600 port
    // write-only register, so need to keep track of all changes
    //*IO_REGISTER_1 = value;
    // $[TI1]
}

//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function:  ReadIoConfig2_  [GUI ONLY]
//
//@ Interface-Description
//  Returns the contents of hardware I/O register 2.  Available on the
//  GUI CPU only.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Free-Function
//=====================================================================
# ifdef SIGMA_GUI_CPU
static inline Uint8
ReadIoConfig2_(void)
{
    // $[TI1]
	// TODO E600 port
    //return *IO_REGISTER_2;

	return 0;
}

# endif // SIGMA_GUI_CPU


//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function:  IsGuiCpu
//
//@ Interface-Description
// Return a Boolean indicating if the target is a GUI (TRUE) or BD (FALSE).
//---------------------------------------------------------------------
//@ Implementation-Description
//      Read processor bit in I/O Register 1.  It is set for BD and clear
//      for GUI.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Free-Function
//=====================================================================

Boolean
IsGuiCpu(void)
{
    // $[TI1]  $[TI2]
    return  (ReadIoConfig1_() & IO_1_RD_BD_CPU) == 0;
}

#ifdef SIGMA_GUI_CPU

//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function:  IsAudioAttnOn  [GUI ONLY]
//
//@ Interface-Description
//  Returns TRUE if SAAS ATTN is asserted, otherwise returns FALSE.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Read the SAAS ATTN status bit in I/O Register 2. 
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Free-Function
//=====================================================================

Boolean
IsAudioAttnOn(void)
{
    // active low logic
    // $[TI1]  $[TI2]
    return  (ReadIoConfig2_() & IO_2_RD_AUDIO_ATTN) == 0;
}


//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function:  IsAudioAckOn  [GUI ONLY]
//
//@ Interface-Description
//  Returns TRUE if SAAS ACK is asserted, otherwise FALSE.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Read the SAAS ACK status bit in I/O Register 2.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Free-Function
//=====================================================================

Boolean
IsAudioAckOn(void)
{
    // active low logic
    // $[TI1]  $[TI2]
    return (ReadIoConfig2_() & IO_2_RD_AUDIO_ACK) == 0;
}


//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function:  IsAudioBusyOn  [GUI ONLY]
//
//@ Interface-Description
//  Returns TRUE is SAAS BUSY is asserted, otherwise returns FALSE.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Read the SAAS BUSY status bit in I/O Register 2. 
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Free-Function
//=====================================================================

Boolean
IsAudioBusyOn(void)
{
    // active low logic
    // $[TI1]  $[TI2]
    return (ReadIoConfig2_() & IO_2_RD_AUDIO_BUSY) == 0;
}

//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function:  SetVentInop  [GUI ONLY]
//
//@ Interface-Description
//  Activates VENT-INOP from the GUI.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Sets the VENT-INOP bit in I/O register 1.
//  Uses 'bset' for thread safe operation.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Free-Function
//=====================================================================

void
SetVentInop(void)
{
	// TODO E600 port
    // NB   active high logic.
    // setting the bit activates vent inop from GUI
    //asm(" bset  #IO_1_WR_VENT_INOP_BIT,`IoConfig1_` ");
    //WriteIoConfig1_(IoConfig1_);
    // $[TI1]
}

//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function:  ClearRemoteAlarm  [GUI ONLY]
//
//@ Interface-Description
//  Deactivates the GUI remote alarm.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Sets the REMOTE-ALARM bit in I/O register 1.
//  Uses 'bset' for thread safe operation.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Free-Function
//=====================================================================

void
ClearRemoteAlarm(void)
{
	// TODO E600 port
    // NB   active low logic.
    // setting the bit disables the remote alarm
    //asm(" bset  #IO_1_WR_REMOTE_ALARM_BIT,`IoConfig1_` ");
   // WriteIoConfig1_(IoConfig1_);
    // $[TI1]
}

//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function:  SetRemoteAlarm  [GUI ONLY]
//
//@ Interface-Description
//  Activates the GUI remote alarm.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Clears the REMOTE-ALARM bit in I/O register 1.
//  Uses 'bclr' for thread safe operation.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Free-Function
//=====================================================================

void
SetRemoteAlarm(void)
{
	// TODO E600 port
    // NB   active low logic.
    // clearing the bit enables the remote alarm
    //asm(" bclr  #IO_1_WR_REMOTE_ALARM_BIT,`IoConfig1_` ");
    //WriteIoConfig1_(IoConfig1_);
    // $[TI1]
}

//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function:  ResetSaas
//
//@ Interface-Description
//  Toggles the reset line to the SAAS audio device.  This starts the
//  reset logic contained in the SAAS.  
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Free-Function
//=====================================================================
void
ResetSaas()
{
	// TODO E600 port
    // NB   active low reset line.
    // assert the SAAS reset line low for min 1-2us and release
    //static const Uint SAAS_RESET_COUNT = 4000;
   // asm(" bclr  #IO_1_WR_AUDIO_RESET_BIT,`IoConfig1_` ");
    //WriteIoConfig1_(IoConfig1_);
    //for (Uint i=SAAS_RESET_COUNT; i; i--)
    //{
        // SAAS RST requires 1-2us hold time
   // };
    //asm(" bset  #IO_1_WR_AUDIO_RESET_BIT,`IoConfig1_` ");
    //WriteIoConfig1_(IoConfig1_);
    // $[TI1]
}

//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function:  IsUpperDisplayColor  [GUI ONLY]
//
//@ Interface-Description
//  Returns TRUE is upper display is color, otherwise returns FALSE.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Read the display state in in I/O Register 1. 
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Free-Function
//=====================================================================

Boolean
IsUpperDisplayColor(void)
{
	// TODO E600 port
    // active low logic
    // $[TI1]  $[TI2]
    //return (ReadIoConfig1_() & IO_1_RD_UPPER_COLOR) == 0;

	return TRUE;
}

//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function:  IsLowerDisplayColor  [GUI ONLY]
//
//@ Interface-Description
//  Returns TRUE is lower display is color, otherwise returns FALSE.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Read the display state in in I/O Register 1. 
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Free-Function
//=====================================================================

Boolean
IsLowerDisplayColor(void)
{
    // active low logic
    // $[TI1]  $[TI2]

	// TODO E600 port
    //return (ReadIoConfig1_() & IO_1_RD_LOWER_COLOR) == 0;

	return TRUE;
}

//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function:  IsGuiCommsConfig  [GUI ONLY]
//
//@ Interface-Description
//  Returns TRUE if the CPU board is in a GUI Comms configuration as
//  determined by the state of the "spare" key on the keyboard.
//---------------------------------------------------------------------
//@ Implementation-Description
//  If the "spare" key bit is negated return TRUE else FALSE.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Free-Function
//=====================================================================

Boolean
IsGuiCommsConfig(void)
{
    // $[TI1]  $[TI2]
	// TODO E600 port
    //return (*KEYS_1 & 0x1) == 0;

	return TRUE;
}

#endif // SIGMA_GUI_CPU


//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function:  ClearFlashWriteEnable
//
//@ Interface-Description
//  Disables the programming voltage (Vpp) to FLASH memory.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Negates the three FLASH write enable bits in I/O register 1.
//  Uses 'bclr' for thread safe operation.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Free-Function
//=====================================================================

void
ClearFlashWriteEnable(void)
{
	// TODO E600 port
	/*
    asm(" bclr #IO_1_WR_FLASH_WRITE_BIT0,`IoConfig1_` ");
    asm(" bclr #IO_1_WR_FLASH_WRITE_BIT1,`IoConfig1_` ");
    asm(" bclr #IO_1_WR_FLASH_WRITE_BIT2,`IoConfig1_` ");
    WriteIoConfig1_(IoConfig1_);
	*/
    // $[TI1]
}

//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function:  SetFlashWriteEnable
//
//@ Interface-Description
//  Enables the programming voltage (Vpp) to FLASH memory.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Asserts the three FLASH write enable bits in I/O register 1.
//  Uses 'bset' for thread safe operation.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Free-Function
//=====================================================================

void
SetFlashWriteEnable(void)
{
	// TODO E600 port
	/*
    asm(" bset #IO_1_WR_FLASH_WRITE_BIT0,`IoConfig1_` ");
    asm(" bset #IO_1_WR_FLASH_WRITE_BIT1,`IoConfig1_` ");
    asm(" bset #IO_1_WR_FLASH_WRITE_BIT2,`IoConfig1_` ");
    WriteIoConfig1_(IoConfig1_);
	*/
    // $[TI1]
}


//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function:  WriteDiagLeds
//
//@ Interface-Description
//  Displays the specified value in the CPU LED.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Writes the inverted value specified to the CPU diagnostic LED
//  register.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//
//@ End-Free-Function
//=====================================================================

void
WriteDiagLeds(Uint8 value)
{
	// TODO E600 port
    //*DIAG_LED_REG = ~value;
    // $[TI1]
}


//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function:  IsXenaConfig 
//
//@ Interface-Description
//  Returns TRUE if the CPU board is a XENA configuration as
//  determined by specific registers.
//---------------------------------------------------------------------
//@ Implementation-Description
//  If the register returns an expected bit return TRUE else FALSE.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Free-Function
//=====================================================================

Boolean
IsXenaConfig(void)
{
    Boolean isXena = FALSE;
   
	// TODO E600 port
	/*
    Uint8 revControlBits = *NMI_REGISTER & NMI_REV_MASK;

    if(revControlBits == NMI_REV_MASK)
    {       
        // "Pre-Cost Reduction detected!
        isXena  = FALSE;
    }
    else
    {
        // "Cost Reduction / Xena detected! (0x00)
        if( (*REV_CTRL_REG == XENA_MICRON_FLASH) || 
            (*REV_CTRL_REG == XENA_SPANSION_FLASH) ||
            (*REV_CTRL_REG == XENA_II) )
        {
            isXena = TRUE;
        }
        else
        {
            // "Cost Reduced detected!"
            isXena = FALSE;
        }

    }

	*/
    return isXena;

}

#ifdef SIGMA_COMMON
//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function:  StopAudibleAlarm
//
//@ Interface-Description
//  Stops the audible alarm for the download process.  Silences the
//  vent alarm when called on the BD CPU.  Silences the remote alarm
//  when called on the GUI CPU.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//
//@ End-Free-Function
//=====================================================================

void
StopAudibleAlarm(void)
{
	// TODO E600 port
	/*
    if ( IsGuiCpu() )
    {                                                         // $[TI1.1]
        // setting the bit disables the remote alarm
        asm(" bset #IO_1_WR_REMOTE_ALARM_BIT,`IoConfig1_` ");
        WriteIoConfig1_(IoConfig1_);
    }
    else
    {                                                         // $[TI1.2]
        *WRITE_IO_PORT = BD_AUDIO_ALARM_OFF;
    }
	*/
}
#endif // SIGMA_COMMON 
