
#ifndef TimerBreathTrigger_HH
#define TimerBreathTrigger_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: TimerBreathTrigger -  Triggers when corresponding Interval
// Timer expired.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/TimerBreathTrigger.hhv   25.0.4.0   19 Nov 2013 14:00:12   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006   By: gdc   Date:  22-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Added restartTimer(void) method to restart the timer with the 
//      current interval setting.
//
//  Revision: 005  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 004  By:  healey    Date:  19-Oct-1998    DR Number: none
//       Project:  Bilevel
//       Description:
//		 	   BiLevel initial version.
//
//  Revision: 003  By:  sp    Date:  30-Sept-1996    DR Number:600
//       Project:  Sigma (R8027)
//       Description:
//             Removed unit test methods.
//
//  Revision: 002  By:  sp    Date:  4-Mar-1996    DR Number:674
//       Project:  Sigma (R8027)
//       Description:
//             Remove #ifndef.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//====================================================================

#  include "Sigma.hh"

#  include "Breath_Delivery.hh"


//@ Usage-Classes
#  include "BreathTrigger.hh"

#  include "TimerTarget.hh"

#  include "IntervalTimer.hh"
//@ End-Usage

class TimerBreathTrigger : public BreathTrigger, public TimerTarget {
  public:
    TimerBreathTrigger(IntervalTimer& iTimer,const TriggerId id);
    virtual ~TimerBreathTrigger(void);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

    inline void restartTimer(void);
    inline void restartTimer(const Int32 interval);
    inline void continueTimer(void);
    inline void setTargetTime(const Int32 interval);
    inline void extendInterval(const Int32 intervalMs);
    virtual void timeUpHappened(const IntervalTimer& itimer);
    virtual void disable(void);
 
  protected:
    virtual Boolean triggerCondition_(void);

    //@ Data-Member:  rIntervalTimer_
    // The interval timer associated with this trigger
    IntervalTimer& rIntervalTimer_;
 
    //@ Data-Member: timeUp_
    // Set to true when the interval timer expires
    Boolean timeUp_;

  private:
    TimerBreathTrigger(const TimerBreathTrigger&);  // not implemented...
    void   operator=(const TimerBreathTrigger&);  // not implemented...
    TimerBreathTrigger(void);  // not implemented...
	
};


// Inlined methods...
#include "TimerBreathTrigger.in"


#endif // TimerBreathTrigger_HH 
