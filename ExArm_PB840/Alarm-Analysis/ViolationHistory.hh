#ifndef ViolationHistory_HH
#define ViolationHistory_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: ViolationHistory - 
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Alarm-Analysis/vcssrc/ViolationHistory.hhv   25.0.4.0   19 Nov 2013 13:51:26   pvcs  $
//
//@ Modification-Log

//  
//   Revision 001   By:   hct      Date: 01/01/95         DR Number:   n/a
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//  
//   Revision 002   By:   sah      Date: 01/12/99         DR Number:   5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to non-inlined method.
//  
//====================================================================

#include "Sigma.hh"
#include "Alarm_Analysis.hh"

//@ Usage-Classes
#include "MsgTimer.hh"
#include "TimeStamp.hh"
#include "ValidVHList.hh"
class OperatingGroup;
//@ End-Usage


class ViolationHistory 
{
  public:

    ViolationHistory(OperatingGroup*  pGroup);

    ViolationHistory(const ViolationHistory& rHist);
    ~ViolationHistory(void);

    void   operator=  (const ViolationHistory& rHist);
    void   createTimer(void);


    void            setActive(Boolean eventDetected = FALSE);
    Boolean         getIsActive(void) const;
    void            setTimerMsec(Int32 time);
    void            clearEventDetected(void);
    Boolean         getEventDetected(void)  const;
    void            reset           (void);
    OperatingGroup* getGroup        (void)  const;

    inline void    setEventDetected(void);
    inline Uint8   getOrdinalValue (void)  const;
    inline void    incOrdinalValue (void);
    inline Boolean operator==(const ViolationHistory& rHist) const;
    inline Boolean operator!=(const ViolationHistory& rHist) const;

    static void Initialize(void);
    static void CheckIsValid(ViolationHistory* pViolation);

    static  void SoftFault(const SoftFaultID softFaultID,
						   const Uint32      lineNumber,
						   const char*       pFileName  = NULL, 
						   const char*       pPredicate = NULL);
#ifdef SIGMA_UNIT_TEST
    inline MsgTimer* getTimer(void) const;
#endif
  
  protected:

  private:
    ViolationHistory();  // Declared only

    //@ Constant:  ValidReferences
    // To verify that a pointer to an instance of ViolationHistory is a valid
    // pointer, method CheckIsValid() is messaged.  It compares the passed
    // pointer to the valid references on this list.
    static ValidVHList&  RValidReferences_;

    //@ Data-Member:    ordinalValue_
    // The breath or event number associated with this object.  An ordinal
    // value of one would correspond to the most recent violation breath (or
    // event) represented by this object.
    Uint8 ordinalValue_;

    //@ Data-Member:    eventDetected_
    // Attribute eventDetected_ is used during the processing of alarm events
    // detected during a breath.
    Boolean eventDetected_;

    //@ Data-Member:    pGroup_    
    // Attribute pGroup_ points to the OperatingGroup representing the
    // violation criteria tracked by this object.
    OperatingGroup* pGroup_;    

    //@ Data-Member:    pTimer_
    // Attribute pTimer_ points to the callback timer cell associated with this
    // object.  ViolationHistory objects are reset when their timer cell 
    // counts down to zero.  If the OperatingGroup pointed to by pGroup_ is 
    // reset, the timer cell pointed to by pTimer_ is cancelled.
    MsgTimer* pTimer_;
};


// Inlined methods
#include "ViolationHistory.in"


#endif // ViolationHistory_HH 
