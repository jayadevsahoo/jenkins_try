#ifndef DS1286_HH
#define DS1286_HH


//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================ H E A D E R   D E S C R I P T I O N ====
//@ Class: DS1286 - Interface class for Dallas Semiconductor 1286
//
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/POST-Library/vcssrc/DS1286.hhv   25.0.4.0   19 Nov 2013 14:16:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004    By:  gdc    Date:  27-Jan-2011    SCR Number: 6671
//  Project:  XENA2
//  Description:
//		Moved Delay functionality to Delay class. Maintained Dallas
//		1286 timekeeper register struct in this file.
//
//  Revision: 003    By: Mitesh Raval   Date: 14-Oct-2010  SCR Number: 6671
//  Project: XENA2
//  Description:
//       Class DS1286 renamed to Delay.
//
//  Revision: 002    By: Mitesh Raval   Date: 24-Sep-2010  SCR Number: 6671
//  Project: XENA2
//  Description:
//       New struct BQ4822YRegMap added for Xena2 version RTC.
//
//  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//=====================================================================

#include "Sigma.hh"

// *************************************************************************
// *****************  Dallas Semiconductor DS1286 Real Time Clock  *********
// *************************************************************************

struct DS1286RegMap
{
      Uint second_tenths:4;
      Uint second_hundreths:4;

      Uint second_unused:1;
      Uint second_tens:3;
      Uint second_ones:4;
      
      Uint minute_unused:1;
      Uint minute_tens:3;
      Uint minute_ones:4;
      
      Uint alarm_minute_mask:1;
      Uint alarm_minute_tens:3;
      Uint alarm_minute_ones:4;
     
      Uint unused:1;
      Uint time_12_hour:1;
      Uint hour_tens_msb:1;
      Uint hour_tens_lsb:1;
      Uint hour_ones:4;
      
      Uint alarm_hour_mask:1;
      Uint alarm_time_12_hour:1;
      Uint alarm_hour_tens_msb:1;
      Uint alarm_hour_tens_lsb:1;
      Uint alarm_hour_ones:4;

      Uint day_of_week_unused:5;
      Uint day_of_week:3;

      Uint alarm_day_of_week_mask:1;
      Uint alarm_day_of_week_unused:4;
      Uint alarm_day_of_week:3;

      Uint date_unused:2;
      Uint date_tens:2;
      Uint date_ones:4;

      Uint disable_real_time_oscillator:1;    // set to one by manufacturer
                                              // must be set to 0 to run the
                                              // the real time clock
      

      Uint disable_squarewave_output:1;       // set this bit to 0 to get
                                              // a 1024 Hertz squarewave out
      Uint month_unused:1;
      Uint month_tens:1;
      Uint month_ones:4;



      Uint year_tens:4;
      Uint year_ones:4;

      Uint          enable_update:1;   // 0 => registers freeze for read
      Uint         interrupt_mode:3;
      Uint    mask_watchdog_alarm:1;   // 1 => mask watchdog alarm int
      Uint mask_time_of_day_alarm:1;   // 1 => mask time of day alarm int
      Uint         watchdog_alarm:1;   // 1 => watchdog alarm has occured
      Uint      time_of_day_alarm:1;   // 1 => time of day alarm has happened

      Uint watchdog_tenths:4;
      Uint watchdog_hundreths:4;

      Uint watchdog_tens:4;
      Uint watchdog_ones:4;

      char non_volatile_memory[48];
};


#endif // DS1286_HH
