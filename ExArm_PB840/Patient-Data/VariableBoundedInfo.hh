
#ifndef VariableBoundedInfo_HH
#define VariableBoundedInfo_HH

#if        defined(SIGMA_GUI_CPU) || defined(SIGMA_COMMON)
//====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//       Copyright (c) 1995, NellCor/Puritan-Bennett Corporation
//====================================================================

//====================================================================
//@ Class: VariableBoundedInfo -  Breath Data Processing agent class 
//      for those patient data items that have absolute upper and lower
//      values, but with some variable constraints, as well.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Patient-Data/vcssrc/VariableBoundedInfo.hhv   25.0.4.0   19 Nov 2013 14:18:00   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By: sah   Date:  09-Jan-2001   DR Number: 5779
//  Project:  PAV
//  Description:
//      Needed for IBW-based resistance ranges.
//
//====================================================================


#include  "BoundedDataInfo.hh"


class VariableBoundedInfo : public BoundedDataInfo 
{
  public:

    VariableBoundedInfo(const PatientDataId::PatientItemId id,
						const Real32 min,
						const Real32 max,
						const Real32 changePoint,
						const Precision lowerP,
						const Precision higherP);
    virtual ~VariableBoundedInfo(void); 

	inline void  setLowerVariableValue(const Real32 lowerVariableValue);
	inline void  setUpperVariableValue(const Real32 upperVariableValue);

    static void  SoftFault(const SoftFaultID softFaultID, 
						   const Uint32      lineNumber, 
						   const char*       pFileName = NULL, 
						   const char*       pBoolTest = NULL); 
  protected:
    virtual void  modifyData_(void *pValue);

  private:
    VariableBoundedInfo(void);                            // not implemented ...
    VariableBoundedInfo(const VariableBoundedInfo& range); // not implemented ...
    void  operator=(const VariableBoundedInfo&);       // not implemented...

	void  processLimitValue_(const Real32 rawLimitValue,
							 Real32& rLimitValue, Precision& rPrecision);

    //@ Constant:    lowerVariableValue_
    // This is the lower variable value allowed for this data type. 
    Real32     lowerVariableValue_;
    Precision  lowerVariablePrecision_;

    //@ Constant:    upperVariableValue_
    // This is the upper variable value allowed for this data type. 
    Real32     upperVariableValue_;
    Precision  upperVariablePrecision_;
};


#include "VariableBoundedInfo.in"


#endif        //defined(SIGMA_GUI_CPU) || defined(SIGMA_COMMON)

#endif  // VariableBoundedInfo_HH
