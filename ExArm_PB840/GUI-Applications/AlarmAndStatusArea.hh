#ifndef AlarmAndStatusArea_HH
#define AlarmAndStatusArea_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1994, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: AlarmAndStatusArea - A container for displaying alarms and status messages.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/AlarmAndStatusArea.hhv   25.0.4.0   19 Nov 2013 14:07:32   pvcs  $
//
//@ Modification-Log
//
//  Revision: 012  By: gdc   Date:  10-June-2005    DR Number: 6170
//  Project:  NIV2
//  Description:
//      DCS 6170 - NIV2
//      Process high insp time alert based on breath trigger from BD.
// 
//  Revision: 011  By: gdc   Date:  01-June-2005    DR Number: 6170
//  Project:  NIV2
//  Description:
//      DCS 6170 - NIV2
//      Changed High Ti,spont alert timeout to 15 seconds.
//
//  Revision: 010   By: gdc   Date:  30-May-2005    DR Number: 6170
//  Project:  NIV2
//  Description:
//      DCS 6170 - NIV2
//      Added 30 second timeout for High Spontaneous Inspiratory Alert.
//
//  Revision: 009   By: gdc   Date:  27-May-2005    DR Number: 6170
//  Project:  NIV2
//  Description:
//      DCS 6170 - NIV2
//      Removed Insp Too Long alarm for NIV. Added High Ti spont alert.
//
//  Revision: 008  By: gdc    Date: 15-Feb-2005  DCS Number:  6144
//  Project:  NIV1
//  Description:
//      DCS 6144 - NIV1 - added "NIV" text to status area
//	
//  Revision: 007  By: hhd    Date: 4-June-1999  DCS Number:  5418
//  Project:  ATC
//  Description:
//  	Modified the parameter list of updateTCTubeDisplay_(), per DCS #5418.
//	
//  Revision: 006  By: hhd    Date: 21-May-1999  DCS Number:  5391
//  Project:  ATC
//  Description:
//	Removed string STATUS_CCT_CAL_HUMID_CHANGED_WARNING.
//
//  Revision: 005  By:  sah	   Date:  23-Feb-1999    DCS Number:  5314
//  Project:  ATC
//  Description:
//      Added integrated with new dynamic, setting applicability functionality.
//      This allows for looser coupling between settings and GUI, and the
//      need for significantly less "setting knowledge" in the GUI subsystem.
//
//  Revision: 004  By:  gdc	   Date:  27-Jan-1998    DCS Number: 
//  Project:  ATC
//  Description:
//		Initial version.
//
//  Revision: 003  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 002  By:  yyy    Date:  04-AUG-1997    DCS Number: 1803
//  Project:  Sigma (R8027)
//  Description:
//      Registered for DATE/TIME settings to capture the setting change
//		events and update the date/time accordingly.
//
//  Revision: 001  By:  wks    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "Container.hh"
#include "GuiTimerTarget.hh"
#include "NovRamEventTarget.hh"
#include "NovRamUpdateManager.hh"
#include "PatientDataTarget.hh"

//@ Usage-Classes
#include "AlarmMsg.hh"
#include "BreathDatumHandle.hh"
#include "BreathType.hh"
#include "Box.hh"
#include "TextField.hh"
#include "GuiAppClassIds.hh"
#include "Notification.hh"
#include "OsTimeStamp.hh"
#include "ContextObserver.hh"
#include "ContextSubject.hh"

class TimeStamp;
//@ End-Usage


class AlarmAndStatusArea : public Container, 
				public GuiTimerTarget,
				public NovRamEventTarget,
				public PatientDataTarget,
				public ContextObserver
{
public:
	AlarmAndStatusArea(void);
	~AlarmAndStatusArea(void);

	//@ Type: DisplayMode
	// Identifies the current/new display mode.
	enum DisplayMode
	{
		AASA_MODE_NORMAL,
		AASA_MODE_MORE_ALARMS
	};

	void setDisplayMode(DisplayMode displayMode);
	void updateAlarmMsgDisplay(void);

	virtual void timerEventHappened(GuiTimerId::GuiTimerIdType timerId);

	// Inherited from NovRamEventTarget
	virtual void novRamUpdateEventHappened(NovRamUpdateManager::UpdateTypeId
								updateId);

	// inherited from PatientDataTarget
	virtual void patientDataChangeHappened(
							PatientDataId::PatientItemId patientDataId);

	// ContextObserver virtual method...
	virtual void  batchSettingUpdate(
						 const Notification::ChangeQualifier qualifierId,
						 const ContextSubject*               pSubject,
						 const SettingId::SettingIdType      settingId
									);

	static void SoftFault(const SoftFaultID softFaultID,
								 const Uint32      lineNumber,
								 const char*       pFileName  = NULL, 
								 const char*       pPredicate = NULL);

protected:

private:
	// these methods are purposely declared, but not implemented...
	AlarmAndStatusArea(const AlarmAndStatusArea&);	// not implemented...
	void operator=(const AlarmAndStatusArea&);		// not implemented...

	void showAlarmMsg1_(void);
	void hideAlarmMsg1_(void);
	void showAlarmMsg2_(void);
	void hideAlarmMsg2_(void);
	void updateTCTubeDisplay_(Boolean isVentStartUp=FALSE);
	void updateNivLabel_(Boolean isVentStartUp=FALSE);
	void updateHighSpontInspTimeAlert_();
	void updateHumidTypeDisplay_();
	void updatePtCctTypeDisplay_();


	//@ Data-Member: displayMode_
	// The current display mode for the AlarmAndStatusArea.
	DisplayMode displayMode_;

	//@ Data-Member: fixedBorder_
	// The fixed border between the AlarmAndStatusArea and the
	// UpperSubScreen area.  Always visible, but set to white when
	// in "normal" display mode and set to black when in "more alarms"
	// display mode.
	Box fixedBorder_;

	//@ Data-Member: alarmMsg1_;
	// First alarm message container.  Normally hidden unless there
	// is at least one active alarm.
	AlarmMsg alarmMsg1_;

	//@ Data-Member: alarmMsg2_;
	// Second alarm message container.  Normally hidden unless there
	// are at least two active alarms.
	AlarmMsg alarmMsg2_;

	//@ Data-Member: alarmTopBorder_
	// Top border for the AlarmAndStatusArea, visible only when in 
	// "more alarms" display mode.
	Box alarmTopBorder_;

	//@ Data-Member: alarmLeftBorder_
	// Left border for the AlarmAndStatusArea, visible only when in 
	// "more alarms" display mode.
	Box alarmLeftBorder_;

	//@ Data-Member: alarmRightBorder_
	// Right border for the AlarmAndStatusArea, visible only when in 
	// "more alarms" display mode.
	Box alarmRightBorder_;

	//@ Data-Member: statusArea_
	// A container that holds all status information.  To remove the current
	// status information we simply need to hide this container.
	Container statusArea_;

	//@ Data-Member: dateTimeText_
	// Text for the current date/time.
	TextField dateTimeText_;

	//@ Data-Member: circuitTypePrompt_
	// Text that says "Circuit Type:".
	TextField circuitTypePrompt_;

	//@ Data-Member: circuitTypeText_
	// Text that displays the current circuit type.
	TextField circuitTypeText_;

	//@ Data-Member: humidificationTypePrompt_
	// Text that says "Humidification Type:".
	TextField humidificationTypePrompt_;

	//@ Data-Member: humidificationTypeText_
	// Text that displays the current humidification type.
	TextField humidificationTypeText_;

	//@ Data-Member: ventTypeLabel_
	// Text that contains "NIV"
	TouchableText ventTypeLabel_;

	//@ Data-Member: highSpontInspTimeAlert_
	// High spontaneous inspiratory time indicator (NIV only)
	TouchableText highSpontInspTimeAlert_;

	//@ Data-Member: tubeTypePrompt_
	// Text that says "Tube Type:".
	TextField tubeTypePrompt_;

	//@ Data-Member: tubeTypeText_
	// Text that displays the current tube type.
	TextField tubeTypeText_;

	//@ Data-Member: tubeIdPrompt_
	// Text that says "Tube Id:".
	TextField tubeIdPrompt_;

	//@ Data-Member: tubeIdText_
	// Text that displays the current tube id
	TextField tubeIdText_;

	//@ Data-Member: isNivInspTooLong_
	// set TRUE when patient-data received indicating NIV spont breath 
	// was truncated for exceeding the High Ti spont limit setting
	Boolean isNivInspTooLong_;

	//@ Data-Member: isSpontInspTimeLimitActive_
	// TRUE when the NIV High Spontaneous Time Limit is applicable
	Boolean isSpontInspTimeLimitActive_;

	//@ Data-Member: alertTimer_
	// Time stamp when high spontaneous inspiratory time alert was activated
	OsTimeStamp alertTimer_;
};

#endif // AlarmAndStatusArea_HH 
