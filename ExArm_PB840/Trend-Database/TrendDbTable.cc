#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: TrendDbTable
//---------------------------------------------------------------------
//@ Interface-Description
//  Each table in the Trend Database is represented by a TrendDbTable
//  object.  The TrendDbTable does all of the management of the table storage
//  on the compact flash and provides a simple interface to the clients.
// 	The table is made up of sample records "rows" where each record has 
//  some number of slots "columns", one for each parameter stored in the table.
//  The interface is made of a set of methods to add records and data to 
//  the table (nextRecord, appendData, and closeRecord) and a method to
//  read data from the table (loadRecordSet).  There are also methods to
//  return properties of table such as Capacity and Size.  
// 
// 	A Table is constructed with a unique Id, a record reference used to parse
//  the data and a reference to the compact flash interface for the read/write
//  operations.
// 
//  To add data to the table first nextRecord() is called and the client must
//  confirm the success of the call.  Then appendData() can be called to load 
//  trend values into the sample record "row" of the table.  Once the client
//  is done loading the values, the closeRecord() method is called to compute
//  the checksum and write out the data to the compact flash.
// 
//  To read data from the table the client calls loadRecordSet() the client
//  must provide a buffer to hold the data.  The client gives the position
//  in sample records (0 is the most current sample) and the number of 
//  sample records to load. 
// 
//---------------------------------------------------------------------
//@ Rationale
//  Encapsulates the processing/management of the table on the compact flash
//  and provides the clients with a simple flat view of the table.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Implementes a wrap around table on the compact flash but provides the
//  clients an interface which seems contiguous and flat.  Each table is 
//  required to have an unique Id, a reference to a sample record used 
//  to parse the trend data and a reference to the compact flash interface
//  used in the read and write operations.
//---------------------------------------------------------------------
//@ Fault-Handling
//  SoftFault is used
//---------------------------------------------------------------------
//@ Restrictions
//	Only 1 block for the write buffer is supported in the code
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Trend-Database/vcssrc/TrendDbTable.ccv   25.0.4.0   19 Nov 2013 14:34:40   pvcs  $
//
//@ Modification-Log
// 
//  Revision: 001  By:  mjf    Date:  18-Jun-2007    SCR Number: 6237
//       Project:  Trend
//       Description:
//             Initial version
// 
//
//=====================================================================

//@ Usage-Classes
//@ End-Usage

#include "Task.hh"                      // OS-Foundation

#include "Trend_Database.hh"
#include "TrendDbTable.hh"
#include "TrendDbControlBlock.hh"



//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TrendDbTable [constructor]
//
//@ Interface-Description
//  Initializes the table with the unique table Id, a record reference that 
//  is used to parse the sample record data and a compact flash interface
//  reference used to do the read/writes
//---------------------------------------------------------------------
//@ Implementation-Description
//  Only 1 block write buffer is currently supported in the code
//---------------------------------------------------------------------
//@ PreCondition
//	write buffer must be 1 block only
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
TrendDbTable::TrendDbTable( TrendDataMgr::TableId tableId,
							TrendDbRecord & tableRecord,
							CompactFlash & tableInterface )
	:   tableId_( tableId ),
		tableRecord_( tableRecord ),
		tableInterface_( tableInterface ),
		writeBuffer_( writeMemory_,
					  TrendDataMgr::TREND_WRITE_BUFFER_SIZE,
					  sizeof(CompactFlash::Block),
					  tableRecord.size() )
{
	AUX_CLASS_PRE_CONDITION( (writeBuffer_.getBlockCount() == 1),
							 writeBuffer_.getBlockCount() );

	TREND_MGR_DBG2( 8, "TrendDbTable Id ", tableId_ );
	TREND_MGR_DBG6( 8, "  write block ", hex, writeBuffer_.getBlockMemory(0),
					", record size ", dec, tableRecord_.size() );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TrendDbTable [destructor]
//
//@ Interface-Description
//  Does no action  
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
TrendDbTable::~TrendDbTable()	
{
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: config
//
//@ Interface-Description
//  Initializes the compact flash storage for the table and
//  initializes the table variables from the control block
//---------------------------------------------------------------------
//@ Implementation-Description
//  Validates the table variables and if invalid the variables are
//  reset to empty table configuration
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDbTable::config( Uint32 tableStartBlock, Uint32 tableCapacity )
{
	// Convert the tableCapacity into LBA's, round up
	Uint32 capacityLba = (tableCapacity / writeBuffer_.getRecordsPerBlock());
	if (tableCapacity % writeBuffer_.getRecordsPerBlock())
	{
		capacityLba++;
	}

	// Add capacity to hold the current head
	// which is saved to the compact flash
	capacityLba += TrendDataMgr::TREND_WRITE_BUFFER_SIZE;

	// Configure the table variables
	// Initialize the table values from the control block
	const TrendDbControlBlock & controlBlock = TrendDataMgr::GetControlBlock();
	Uint32 headBlockLba = controlBlock.getHeadBlockLba( tableId_ );
	Uint32 sizeBlocks   = controlBlock.getSizeBlocks( tableId_ );
	Boolean isWriteBufferLoaded = FALSE;

	TREND_MGR_DBG6( 8, "Config table ", tableId_,
					", start block ", tableStartBlock,
					", capacity ", capacityLba );

	TREND_MGR_DBG6( 8, "Control Block data for Table id ", tableId_,
					", head LBA ", headBlockLba,
					", size blocks ", sizeBlocks );

	// Validate that the table head and size are correct
	// Size must be less than capacity
	// If size has not rolled then it must equal the head
	// Head must be more then or equal to start block
	// head must be less than the ending block (start + capacity)
	if (sizeBlocks < capacityLba &&
		(sizeBlocks == (capacityLba - 1) || sizeBlocks == (headBlockLba - tableStartBlock) ) && 
		headBlockLba >= tableStartBlock &&
		headBlockLba < (tableStartBlock + capacityLba))
	{
		tableVars_.config( tableStartBlock, capacityLba, headBlockLba, sizeBlocks );
		TREND_MGR_DBG4( 32, "Use control block data, head ", tableVars_.getHeadLba(),
						", size ", tableVars_.getSizeLba() );

		// Load the write buffer from compact flash
		CompactFlash::BlockPtr pBlockData = (CompactFlash::BlockPtr) writeBuffer_.getBlockMemory(0);
		if (tableInterface_.read( tableVars_.getHeadLba(), pBlockData ) == SUCCESS)
		{
			isWriteBufferLoaded = TRUE;
		}

		TREND_MGR_DBG6( 32, "Loaded write buffer for table ", tableId_,
						", block LBA ", tableVars_.getHeadLba(),
						", status ", isWriteBufferLoaded );
	}
	else
	{
		// use initial default values
		tableVars_.config( tableStartBlock, capacityLba, tableStartBlock, 0 );

		// Initialize control block
		controlBlock.setHeadBlockLba( tableId_, tableVars_.getHeadLba() );
		controlBlock.setSizeBlocks( tableId_, tableVars_.getSizeLba() ); 

		TREND_MGR_DBG4( 32, "Use default table Vars - head ", tableVars_.getHeadLba(),
						", size ", tableVars_.getSizeLba() );
	}

	// If write buffer loaded from compact flash then set the record position
	if (isWriteBufferLoaded)
	{
		TREND_MGR_DBG4( 32, "Write buffer loaded for table ", tableId_,
						", set record position ",
						controlBlock.getBufferPosition(tableId_) );

		writeBuffer_.setRecordPosition( controlBlock.getBufferPosition(tableId_) );
	}
	else
	{
		// write buffer memory not loaded so clear and reset
		TREND_MGR_DBG4( 32, "Write Buffer not loaded, table ", tableId_,
						", block LBA ", tableVars_.getHeadLba() ); 

		writeBuffer_.clear();
		controlBlock.setBufferPosition( tableId_,
										writeBuffer_.getRecordPosition() );
	}

	TREND_MGR_DBG6( 8, "Buffer record position for table ", tableId_,
					", buffer's position ", writeBuffer_.getRecordPosition(),
					", control block's position ",
					controlBlock.getBufferPosition(tableId_) );

	// Set the table capacity in sample records "rows"
	// The capacity will be adjusted to a block boundaries 
	tableRecordCapacity_ = tableVars_.getCapacityLba() *
						   writeBuffer_.getRecordsPerBlock();

	TREND_MGR_DBG6( 8, "Table start LBA ", tableVars_.getStartLba(),
					" capacity LBA ", tableVars_.getCapacityLba(),
					" record capacity ", tableRecordCapacity_ );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: clear
//
//@ Interface-Description
//  Reset the table to default empty configuration
//---------------------------------------------------------------------
//@ Implementation-Description
//  Also clears the write buffer and updates the control block
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDbTable::clear( void )
{
	TREND_MGR_DBG2( 8, "Clear table ", tableId_ );

	// Clear the table buffer
	writeBuffer_.clear();

	// Config the table vars to default
	tableVars_.clear();

	// Update the control block
	const TrendDbControlBlock & controlBlock = TrendDataMgr::GetControlBlock();
	controlBlock.setHeadBlockLba( tableId_, tableVars_.getHeadLba() );
	controlBlock.setSizeBlocks( tableId_, tableVars_.getSizeLba() );
	controlBlock.setBufferPosition( tableId_, writeBuffer_.getRecordPosition() );
}



// ====================================================================
// Write methods - Appending data to the table
// ====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: nextRecord
//
//@ Interface-Description
//  Sets up the next record in the database with the seq number and timestamp
//  returns the success of the operation (a FAILURE indicates an error)
//---------------------------------------------------------------------
//@ Implementation-Description
//  Does the following steps
//  1. Verify room in the buffer, flush if needed
//  2. Update the buffer positions
//  3. Store seqNumber and time stamp in write buffer
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
SigmaStatus TrendDbTable::setNextRecord( Uint32 seqNumber, const TimeStamp & timeStamp )
{
	SigmaStatus bufferOk = SUCCESS;

	// 1. Flush write buffer if needed
	// Should only need to flush if the closeRecord() method
	// ran into a problem, this provides the second chance, reset the 
	// compact interface, to clear any intermediate error status
	// If the second chance fails it will be pickup below, because the
	// buffer will have no room
	if (writeBuffer_.atEnd())
	{
		tableInterface_.open();
		closeRecord();
		TREND_MGR_DBG4( 1, "Table ", tableId_,
						" second write, status ", writeBuffer_.atStart() );
	}

	// 2. Update the record position in the write buffer
	if (writeBuffer_.atStart())
	{
		writeBuffer_.setFirstRecord( tableRecord_ );
	}
	else
	{
		// Will return error no room in the buffer (ie atEnd)
		bufferOk = writeBuffer_.gotoNextRecord( tableRecord_, TRUE );
	}

	TREND_MGR_DBG6( 32, "Table next record, table Id ", tableId_,
					", buffer pos ", writeBuffer_.getRecordPosition(),
					" buffer ok ", bufferOk ); 

	// 3. Store the seq number and timestamp in the current record
	if (bufferOk == SUCCESS)
	{
		tableRecord_.setSeqNumber( seqNumber );
		tableRecord_.setTimeOfDay( timeStamp.getTimeOfDay() );

		TREND_MGR_DBG4( 32, "Table next record seqNum ", seqNumber,
						" buf entry ", writeBuffer_.getRecordPosition() );
	}

	return( bufferOk );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: appendData
//
//@ Interface-Description
//  Append trend data to the record at the given slot number
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDbTable::appendData( Int8 slotNumber, const TrendValue & newValue )
{
	tableRecord_.setTrendValue( slotNumber, newValue );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: closeRecord
//
//@ Interface-Description
//  Close the record, computes the checksum and writes the data to the
//  compact flash.
//---------------------------------------------------------------------
//@ Implementation-Description
//  If the buffer is full then update the table variables to the next 
//  block on the compact flash and clear/reset the buffer
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDbTable::closeRecord( void )
{
	const TrendDbControlBlock & controlBlock = TrendDataMgr::GetControlBlock();

	// Compute and save the checksum
	tableRecord_.setChecksum();

	// If at the end of the buffer and the flush is successful
	// then update the table vars and clear the write buffer
	Boolean isAtEnd = writeBuffer_.atEnd();
	Boolean isNoError = (flushWriteBuffer_() == SUCCESS ? TRUE : FALSE );

	TREND_MGR_DBG6( 8, "Trend table close record, Id ", tableId_,
					", at end ", isAtEnd,
					", flush status ", isNoError );

	if (isAtEnd && isNoError)
	{
		tableVars_.incLba();

		// Update the control block
		controlBlock.setHeadBlockLba(tableId_, tableVars_.getHeadLba() );
		controlBlock.setSizeBlocks( tableId_, tableVars_.getSizeLba() );

		// clear the write buffer
		writeBuffer_.clear();
	}

	// Set the buffer record position, in the control block
	controlBlock.setBufferPosition( tableId_,
									writeBuffer_.getRecordPosition() );

	TREND_MGR_DBG6( 32, "Table Id ", tableId_,
					", DB Blocks head ", tableVars_.getHeadLba(),
					", size ", tableVars_.getSizeLba() );
	TREND_MGR_DBG6( 32, "Control block Vars - head ", controlBlock.getHeadBlockLba(tableId_),
					", size blocks ", controlBlock.getSizeBlocks(tableId_),
					", buffer position ", controlBlock.getBufferPosition(tableId_));
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: loadRecordSet
//
//@ Interface-Description
//  Load data from the compact flash into the supplied buffer
//  given the staring position (in sample record) in the table
//  and the number of sample records to load.
//  Returns the success of the operation, this method does not
//  attempt to retry the read from the compact flash, it is the 
//  responsibility of the caller to provide the second retry.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The load is done in reverse order on the compact flash
//  So the start position is at the end of the transfer from
//  the compact flash 
//---------------------------------------------------------------------
//@ PreCondition
//	The recordSet buffer (which will be loaded with data) must
//  match in recordSize and BlockSize of the write buffer.
//  The request transfer size must fix in the supplied buffer
//  The supplied buffer must be larger than the write buffer
//  The supplied request position and size must be valid
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
SigmaStatus TrendDbTable::loadRecordSet( TrendDbTableBuffer & recordSet,
								 Uint32 position, Int16 dataSize )
{
	// Get and verify the record and block size match the buffer
	// used to write the data
	Int16 recordSize = recordSet.getRecordSize();
	Int16 blockSize = recordSet.getBlockSize();
	Int16 recordSetSize = recordSet.getRecordCapacity();
	Int16 tableSize = getRecordCount();

	TREND_MGR_DBG4( 16, "Table load record size ", recordSize, 
					" block size ", blockSize );
	TREND_MGR_DBG4( 16, "  position ", position, " record count ", dataSize );
	TREND_MGR_DBG4( 16, "  buffer size ", recordSetSize, " table size ", tableSize );

	TREND_MGR_DBG4( 16, "  DB head ", tableVars_.getHeadLba(),
					" tail ", tableVars_.getTailLba() );


	// Verify the layout of the database sample records and blocks
	AUX_CLASS_PRE_CONDITION( (recordSize == writeBuffer_.getRecordSize() ),
							 ((recordSize << 16) | writeBuffer_.getRecordSize() ) );
	AUX_CLASS_PRE_CONDITION( (blockSize == writeBuffer_.getBlockSize() ),
							 ((blockSize << 16) | writeBuffer_.getBlockSize() ) );

	// Datasize must fix in the recordSet buffer
	AUX_CLASS_PRE_CONDITION( (dataSize <= recordSetSize), ((dataSize << 16) | recordSetSize ) );

	// The read buffer must be bigger then the write buffers
	AUX_CLASS_PRE_CONDITION( (writeBuffer_.getRecordCapacity() <= recordSetSize),
							 ((writeBuffer_.getRecordCapacity() << 16) | recordSetSize) );

	// Verify that the position is valid
	AUX_CLASS_PRE_CONDITION( (position < tableSize), ((position << 16) | tableSize) );

	// Verify that the size is valid
	AUX_CLASS_PRE_CONDITION( ((position + dataSize) <= tableSize),
							 (((position + dataSize) << 16) | tableSize ) );


	// Compute the transfer in blocks
	Int32 xferSizeBlocks = ((dataSize-1) / recordSet.getRecordsPerBlock()) + 1;

	// Check to see if data needs to be pulled from RAM (position is in RAM)
	Boolean isLoadRam       = FALSE;
	Int16 ramBlocks         = writeBuffer_.getBlockCount();
	Int16 ramEntries        = writeBuffer_.getRecordPosition() + 1;
	Uint32 positionOffset   = 0;
	if (position < ramEntries)
	{
		// Need to load from RAM, adjust the compact flash size
		isLoadRam = TRUE;
		xferSizeBlocks -= ramBlocks;
		if (xferSizeBlocks < 0)
		{
			xferSizeBlocks = 0;
		}

		// Set position to the last block in the compact flash
		positionOffset = ramBlocks;
	}
	else
	{
		// Adjust the position to take the RAM in account
		positionOffset =
			((position - ramEntries) / recordSet.getRecordsPerBlock()) + 1;
	}

	TREND_MGR_DBG6( 16, "LoadRecordSet, block count ", xferSizeBlocks,
					", position offset ", positionOffset,
					", load from Ram ", isLoadRam );

	// clear the read buffer and get starting memory pointer
	recordSet.clear();
	CompactFlash::BlockPtr pMemory = (CompactFlash::BlockPtr)
									 recordSet.getBlockMemory(0);


	// Transfer from the compact flash
	SigmaStatus readOk = SUCCESS;
	if (xferSizeBlocks > 0)
	{
		// Compute the start of the transfer
		Int32 xferEndLba = tableVars_.getPositionLba( positionOffset );
		Int32 xferStartLba = tableVars_.getPositionLba( positionOffset + (xferSizeBlocks - 1) );
		TREND_MGR_DBG6( 32, "  Xfer start ", xferStartLba,
						", blocks  ", xferSizeBlocks,
						",  end ", xferEndLba );

		// Verify the transfer (begin and end), return error if the transfer is invalid
		AUX_CLASS_PRE_CONDITION( (xferStartLba != -1 && xferEndLba != -1),
								 ((xferStartLba << 16) | xferEndLba) );

		// Then check to see if the transfer spans the wrap around
		// if the data is wrapped then read in two passes
		if (xferStartLba <= xferEndLba)
		{
			// One read
			CompactFlash::BlockPtr pMemory = (CompactFlash::BlockPtr) recordSet.getBlockMemory(0);
			readOk = tableInterface_.read( xferStartLba, pMemory, xferSizeBlocks );

			TREND_MGR_DBG6( 32, "Table read (1of1) status ", readOk,
							"  addr ", hex, pMemory, dec );
		}
		else
		{
			// Split the read into two segments
			// Span the wrap around
			//      segment #1 (xferStart -> End Block of table)
			//      segment #2 (Start Block of table -> xferEndLba)

			// Compute the size of the segments
			Uint32 seg1SizeLba = tableVars_.getEndLba() - xferStartLba + 1;
			Uint32 seg2SizeLba = xferEndLba - tableVars_.getStartLba() + 1;

			// Verify the transfer sizes
			AUX_CLASS_PRE_CONDITION( ((seg1SizeLba + seg2SizeLba) <= xferSizeBlocks),
									 ((seg1SizeLba << 16) | seg2SizeLba ) );

			// Read the first segment
			CompactFlash::BlockPtr pMemory = (CompactFlash::BlockPtr) recordSet.getBlockMemory(0);
			readOk = tableInterface_.read( xferStartLba, pMemory, seg1SizeLba );

			TREND_MGR_DBG4( 32, "Table read (1of2) status ", readOk,
							" size ", seg1SizeLba );
			TREND_MGR_DBG4( 32, "  memory addr ", hex, pMemory, dec );

			// Read the second segment, the memory pointer needs to updated
			if (readOk)
			{
				pMemory = (CompactFlash::BlockPtr) recordSet.getBlockMemory(seg1SizeLba);
				Uint32 seg2Start = tableVars_.getStartLba();
				readOk = tableInterface_.read( seg2Start, pMemory, seg2SizeLba );

				TREND_MGR_DBG4( 32, "Table read (2of2) status ", readOk,
								" size ", seg2SizeLba );
				TREND_MGR_DBG4( 32, "  memory addr ", hex, pMemory, dec );
			}
		}
	}
	else
	{
		TREND_MGR_DBG1( 16, "Nothing to load from CF" );
	}

	// Load the data from RAM last so when read in reverse order it is first
	if (isLoadRam)
	{
		CompactFlash::BlockPtr pMemory =
			(CompactFlash::BlockPtr) recordSet.getBlockMemory(xferSizeBlocks);

		memcpy( pMemory,
				writeBuffer_.getBlockMemory(0),
				ramBlocks * sizeof( CompactFlash::Block ) );

		TREND_MGR_DBG6( 32, "Table copy RAM buffer ", hex, writeBuffer_.getBlockMemory(0),
						" to ", hex, pMemory );
		TREND_MGR_DBG3( 32, "  size ", dec, (ramBlocks * sizeof( CompactFlash::Block )) );
	}


	// Set the last position in the recordSet
	Int16 lastPosition = (xferSizeBlocks * recordSet.getRecordsPerBlock()) - 1;
	if (isLoadRam)
	{
		lastPosition += ramEntries;
	}
	recordSet.setLastPosition( lastPosition );
	TREND_MGR_DBG2( 16, "Set last position ", lastPosition ); 

	return( readOk );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: flushWriteBuffer_
//
//@ Interface-Description
//  Stores the write buffer to the "head" of the table on the compact flash
//  Returns the success of the operation 
//---------------------------------------------------------------------
//@ Implementation-Description
//  Only supports write buffer of 1 block
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
SigmaStatus TrendDbTable::flushWriteBuffer_( void )
{
	SigmaStatus writeOk = SUCCESS;

	// Transfer one block from RAM on to the compact flash
	Uint32 xferStartLba = tableVars_.getHeadLba();
	CompactFlash::BlockPtr pMemory = (CompactFlash::BlockPtr) writeBuffer_.getBlockMemory(0);
	writeOk = tableInterface_.write( xferStartLba, pMemory );

	TREND_MGR_DBG6( 32, "Table flushed - tableId ", tableId_,
					", write block ", xferStartLba,
					", status ", writeOk );

	return( writeOk  );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void TrendDbTable::SoftFault(const SoftFaultID  softFaultID,
							const Uint32       lineNumber,
							const char*        pFileName,
							const char*        pPredicate)  
{
	FaultHandler::SoftFault(softFaultID, TREND_DATABASE, TREND_DB_TABLE,
							lineNumber, pFileName, pPredicate);
}



