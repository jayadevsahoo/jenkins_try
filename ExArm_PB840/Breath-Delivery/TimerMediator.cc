#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: TimerMediator - Updates all the timers that are on its list.
//---------------------------------------------------------------------
//@ Interface-Description
//    This mediator maintains predefined list of timers. The list of active 
//    timers is evaluated every BD cycle.  Any other timer operations
//    such as start and stop must be performed by other objects. 
//---------------------------------------------------------------------
//@ Rationale
//     This class provides the method to update all the timers every BD cycle.
//---------------------------------------------------------------------
//@ Implementation-Description
//    Timers are maintained on a list that is implemented as an array.
//    All active timers will be instructed to update their elapsed time
//    every BD cycle. 
//---------------------------------------------------------------------
//@ Fault-Handling
//    n/a.
//---------------------------------------------------------------------
//@ Restrictions
//    Only one instance of this class is to be allowed.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/TimerMediator.ccv   25.0.4.0   19 Nov 2013 14:00:12   pvcs  $
//
//@ Modification-Log
//
//  Revision: 011   By: rpr    Date: 22-Jan-2009    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support code review comments.
// 
//  Revision: 010  By:  rpr   Date:  17-Oct-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//  	Added RO2MixTimer Timer ref in support of +20
// 
//  Revision: 009   By: gdc   Date:  13-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Added RM maneuver timers.
//
//  Revision: 008  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 007  By:  yyy    Date:  12-Jan-1998    DR Number:
//       Project:  BiLevel (R8027)
//       Description:
//          BiLevel Baseline.  Added RBiLevelCycleTimer, RInspPauseManeuverTimer,
//			and RExpPauseManeuverTimer to pTimerList_.
//
//  Revision: 006  By:  iv    Date:  22-Aug-1997    DR Number: 2409
//       Project:  Sigma (R8027)
//       Description:
//           Added rev 005.
//
//  Revision: 005  By: syw   Date:   23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 004  By:  sp    Date:  29-Oct-1996    DR Number:
//       Project:  Sigma (R8027)
//       Description:
//             Inspection Rework.
//
//  Revision: 003  By:  sp    Date:  4-Mar-1996    DR Number:674
//       Project:  Sigma (R8027)
//       Description:
//             Remove #ifndef.
//
//  Revision: 002  By:  kam   Date:  06-Nov-1995    DR Number:600 
//       Project:  Sigma (R8027)
//       Description:
//             Added SST Confirmation Timer
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "TimerMediator.hh"

//@ Usage-Classes
#include "TimersRefs.hh"

#include "IntervalTimer.hh"
//@ End-Usage

//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TimerMediator()  
//
//@ Interface-Description
//	Default Contstructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Each element of the array is initialize individually to point to
//	a specific interval timer.
//
// Note !!!
// ====
// When a timer reference is added to the list, update the const
//  NUM_TIMERS in the header file.	
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
TimerMediator::TimerMediator(void)
{   
    CALL_TRACE("TimerMediator(void)");

	Int32 ii = 0;
    // $[TI1]
    pTimerList_[ii++] = &RApneaTimer;
    pTimerList_[ii++] = &RBackupTimeExpTimer;
    pTimerList_[ii++] = &RSimvCycleTimer;
    pTimerList_[ii++] = &RSimvInspTimer;
    pTimerList_[ii++] = &RInspTimer;
    pTimerList_[ii++] = &RPauseTimeoutTimer;
    pTimerList_[ii++] = &RO2Timer;
    pTimerList_[ii++] = &RO2MixTimer;
    pTimerList_[ii++] = &RPressureXducerAutozeroTimer;
    pTimerList_[ii++] = &ROscInspTimer;
    pTimerList_[ii++] = &RSvcTimer;
    pTimerList_[ii++] = &ROscBackupInspTimer;
    pTimerList_[ii++] = &RAvgBreathDataTimer;
    pTimerList_[ii++] = &RSmSwitchConfirmationTimer;
    pTimerList_[ii++] = &RBiLevelCycleTimer;
    pTimerList_[ii++] = &RInspPauseManeuverTimer;
    pTimerList_[ii++] = &RExpPauseManeuverTimer;
    pTimerList_[ii++] = &RNifManeuverTimer;
    pTimerList_[ii++] = &RP100ManeuverTimer;
    pTimerList_[ii++] = &RVitalCapacityManeuverTimer;

    CLASS_PRE_CONDITION(NUM_TIMERS == ii);
    
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~TimerMediator 
//
//@ Interface-Description
//	Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
TimerMediator::~TimerMediator()
{   
    CALL_TRACE("~TimerMediator()");

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newCycle
//
//@ Interface-Description
//  This method takes no parameters and has no return value.
//  It is to be called in the main breath delivery task every BD cycle.
//  This method instructs every timer on its list to update for a new
//  BD cycle. 
//---------------------------------------------------------------------
//@ Implementation-Description
//  Each timer is updated every BD cycle by calling IntervalTimer::updateTimer. 
//---------------------------------------------------------------------
//@ PreCondition
//  CLASS_PRE_CONDITION(pTimerList_ != NULL)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
TimerMediator::newCycle(void) 
{   
    CALL_TRACE("newCycle()");

    CLASS_PRE_CONDITION(pTimerList_ != NULL);

    for( Int32 ii=0; ii<NUM_TIMERS; ii++)
    {
	// $[TI1]
        pTimerList_[ii]->updateTimer();
    }
    // This test item is not reachable $[TI2] 
}

#ifdef SIGMA_UNIT_TEST
IntervalTimer*
TimerMediator::getTimerList1(void)
{
	return (pTimerList_[0]);
}
#endif //SIGMA_UNIT_TEST



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
 
void
TimerMediator::SoftFault(const SoftFaultID  softFaultID,
					     const Uint32       lineNumber,
					     const char*        pFileName,
					     const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, TIMERMEDIATOR,
                          lineNumber, pFileName, pPredicate);
}

