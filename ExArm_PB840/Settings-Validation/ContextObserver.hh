
#ifndef ContextObserver_HH
#define ContextObserver_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  ContextObserver - Context Observer Class.
//---------------------------------------------------------------------
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/ContextObserver.hhv   25.0.4.0   19 Nov 2013 14:27:18   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001   By: sah   Date:  26-Jan-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//     Created to work with new applicability functionality.
//
//====================================================================

#include "Notification.hh"
#include "ContextId.hh"
#include "SettingId.hh"

//@ Usage-Classes
class  ContextSubject;
//@ End-Usage


class ContextObserver
{
  public:
    virtual ~ContextObserver(void);

    //-------------------------------------------------------------------
    // Dynamic Notification methods...
    //-------------------------------------------------------------------

    //@ Type: UpdateMethodPtr 
    // Pointer type to an update method of the ContextOberver class.
    typedef void (ContextObserver::* UpdateMethodPtr)(
					  const Notification::ChangeQualifier,
					  const ContextSubject*,
					  const SettingId::SettingIdType
						     );

    virtual void  batchSettingUpdate(
			  const Notification::ChangeQualifier qualifierId,
			  const ContextSubject*               pContextSubject,
			  const SettingId::SettingIdType      settingId
				    );
    virtual void  nonBatchSettingUpdate(
			  const Notification::ChangeQualifier qualifierId,
			  const ContextSubject*               pContextSubject,
			  const SettingId::SettingIdType      settingId
				       );

    virtual Boolean  doRetainAttachment(const ContextSubject* pSubject) const;


    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL, 
			   const char*       pPredicate = NULL);

  protected:
    ContextObserver(void);

    inline ContextSubject*  getSubjectPtr_(
				    const ContextId::ContextIdType subjectId
					  ) const;

    void  attachToSubject_  (const ContextId::ContextIdType      subjectId,
			     const Notification::ContextChangeId changeId);
    void  detachFromSubject_(const ContextId::ContextIdType      subjectId,
			     const Notification::ContextChangeId changeId) const;

  private:
    ContextObserver(const ContextObserver&);    // not implemented...
    void  operator=(const ContextObserver&);    // not implemented...

    ContextSubject*  arrSubjectPtrs_[ContextId::TOTAL_CONTEXT_IDS-1];
};


#include "ContextObserver.in"


#endif // ContextObserver_HH 
