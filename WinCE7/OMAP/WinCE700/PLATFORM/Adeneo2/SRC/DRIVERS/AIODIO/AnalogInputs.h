
#ifndef ANALOG_INPUTS_H

#define ANALOG_INPUTS_H



enum AnalogInputs_t
{
    ADC_FIRST_CHANNEL,
    ADC_AIRWAY_PRESSURE_TRANSDUCER = ADC_FIRST_CHANNEL,
    ADC_INTERNAL_PRESSURE_TRANSDUCER,
    ADC_INTERNAL_CASE_TEMPERATURE,
    ADC_FLOW_HIGH_RES_DIFFERENTIAL_TRANSDUCER,
    ADC_SIGNAL_TEST,
    ADC_FIO2_SENSOR,
    ADC_WATCHDOG_SIGNAL,
    ADC_FLOW_DIFFERENTIAL_TRANSDUCER,
    ADC_LAST_CHANNEL = ADC_FLOW_DIFFERENTIAL_TRANSDUCER,
    ADC_NUMBER_OF_CHANNELS
};

#endif

