#ifndef VentStartSubScreen_HH
#define VentStartSubScreen_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: VentStartSubScreen - Subscreen displayed in the Lower Screen
// on startup of normal ventilation.  Allows operator to choose Same Patient,
// New Patient or Short Self Test.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/VentStartSubScreen.hhv   25.0.4.0   19 Nov 2013 14:08:42   pvcs  $
//
// @Modification-Log
//
//  Revision: 006  By: hhd    Date: 07-June-1999  DCS Number: 5423
//  Project:  ATC
//  Description:
//	Removed iconBackgroundBox_.
//
//  Revision: 005  By: hhd    Date: 05-May-1999  DCS Number: 5365
//  Project:  ATC
//  Description:
//	Added background for flashing icons.
//
//  Revision: 004  By: sah    Date: 29-Apr-1999  DCS Number: 5365
//  Project:  ATC
//  Description:
//	Supporting new verification-needed mechanism, whereby certain
//      setting buttons that are deemed "ultra"-important will flash an
//      icon, and this subscreen will flash the same icon along with a
//      message.
//
//  Revision: 003  By: yyy      Date: 05-Jan-1998  DR Number: 
//    Project:  BiLevel (R8027)
//    Description:
//     Initial release for BiLevel adding software option.
//     Merged /840/Baseline/GUI-Applications/vcssrc/VentStartSubScreen.hhv   1.9.1.0   07/30/98 10:22:40   gdc
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "Sigma.hh"
#include "BdEventTarget.hh"
#include "GuiEventTarget.hh"
#include "GuiTimerTarget.hh"
#include "SubScreen.hh"

//@ Usage-Classes
#include "AdjustPanelTarget.hh"
#include "Box.hh"
#include "Line.hh"
#include "SubScreenTitleArea.hh"
#include "TextButton.hh"
#include "Bitmap.hh"
//@ End-Usage

class VentStartSubScreen : public SubScreen, public GuiTimerTarget,
							public AdjustPanelTarget, public GuiEventTarget,
							public BdEventTarget
{
public:
	VentStartSubScreen(SubScreenArea *pSubScreenArea);
	~VentStartSubScreen(void);

	// Overload SubScreen activation/deactivation methods
	virtual void activate(void);
	virtual void deactivate(void);

	// ButtonTarget virtual method
	virtual void buttonDownHappened(Button *pButton,
												Boolean byOperatorAction);
	virtual void buttonUpHappened(Button *pButton,
												Boolean byOperatorAction);

	// GuiTimerTarget virtual method
	virtual void timerEventHappened(GuiTimerId::GuiTimerIdType timerId);

	// AdjustPanelTarget virtual methods
	virtual void adjustPanelAcceptPressHappened(void);
	virtual void adjustPanelClearPressHappened(void);
	virtual void adjustPanelLoseFocusHappened(void);

	// GuiEventTarget virtual method
	virtual void guiEventHappened(GuiApp::GuiAppEvent eventId);

	// BdEventTarget virtual method
	virtual void bdEventHappened(EventData::EventId eventId,
				                EventData::EventStatus eventStatus,
								EventData::EventPrompt eventPrompt=EventData::NULL_EVENT_PROMPT);

	static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL, 
			  const char*       pPredicate = NULL);

protected:

private:
	// these methods are purposely declared, but not implemented...
	VentStartSubScreen(const VentStartSubScreen&);		// not implemented...
	void   operator=(const VentStartSubScreen&);	// not implemented...

	void layoutScreen_(void);

	// TubeType display text query method.
	StringId getTubeTypeText_(DiscreteValue tubeType);

	//@ Type: SetupOptions_
	// Indicates which option is currently active (i.e. which option
	// button is currently pressed down), if any.  Can be SAME_PATIENT,
	// NEW_PATIENT, SHORT_SELF_TEST, O2_MONITOR_CAL or NO_OPTION.
	enum SetupOptions_
	{
		SAME_PATIENT_,
		NEW_PATIENT_,
		SHORT_SELF_TEST_,
		O2_MONITOR_CAL_,
		NO_OPTION_
	};

	//@ Data-Member: samePatientButton_
	// Text button which displays the Same Patient option.
	TextButton samePatientButton_;

	//@ Data-Member: newPatientButton_
	// Text button which displays the New Patient option.
	TextButton newPatientButton_;

	//@ Data-Member: sstButton_
	// Text button which displays the Short Self Test option.
	TextButton sstButton_;

	//@ Data-Member: continueButton_
	// The Continue button which confirms the choice of the New Patient or SST
	// options.
	TextButton continueButton_;

	//@ Data-Member: samePatientMessage_
	// Message explaining how the Same Patient option will affect
	// settings and patient data.
	TextField samePatientMessage_;

	//@ Data-Member: samePatientVerifyMessage_
	// Message explaining the need for verification of newly-applicable
	// settings.
	TextField samePatientVerifyMessage_;

	//@ Data-Member: samePatientVerifyIcon_
	// Icon explaining the need for verification of newly-applicable
	// settings.
	Bitmap samePatientVerifyIcon_;

	//@ Data-Member: newPatientMessage_
	// Message explaining how the New Patient option will affect
	// settings and patient data.
	TextField newPatientMessage_;

	//@ Data-Member: sstMessage_
	// Message explaining what the Short Self Test option does.
	TextField sstMessage_;

	//@ Data-Member: titleArea_
	// The title area containing the title of this subscreen.
	SubScreenTitleArea titleArea_;

	//@ Data-Member: dividingLine_
	// The line which divides the upper part of the subscreen containing the
	// New/Same Patient buttons from the lower part which contains the
	// SST button.
	Line dividingLine_;

	//@ Data-Member: currentOption_
	// Flag which indicates which option is currently active
	SetupOptions_ currentOption_;

};

#endif // VentStartSubScreen_HH 
