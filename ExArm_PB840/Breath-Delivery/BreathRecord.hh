
#ifndef BreathRecord_HH
#define BreathRecord_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: BreathRecord - Records vital information about delivered breaths.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/BreathRecord.hhv   25.0.4.0   19 Nov 2013 13:59:40   pvcs  $
//
//@ Modification-Log
// 
//  Revision: 043  By:  rhj    Date:  05-Oct-2010    SCR Number: 6627
//  Project:  PROX
//  Description:
//      Added WyePressEstPrev_, WyePressEst_, GetWyePressEst(),
//      GetWyePressEstPrev() and ResetWyePressEst()
// 
//  Revision: 042  By:  rhj    Date:  18-Feb-2010    SCR Number: 6627
//  Project:  PROX
//  Description:
//      PROX project-related changes.
//      Added resetProxSubPress(), GetProxPressSub(), GetProxPressSubPrev()     
//
//  Revision: 041   By:   rhj    Date: 16-Mar-2009     SCR Number: 6492 
//  Project:  840S
//  Description:
//      Added EndDryExhFlow_ data member to keep track of the 
//      dry exhalation flow at the end of exhalation.
//       
//  Revision: 040   By:   rhj    Date: 07-Nov-2008     SCR Number: 6435
//  Project:  840S
//  Description:
//       Leak Compensation project-related changes.
//       
//  Revision: 039   By:   rhj    Date: 05-March-2007      DR Number: 6359
//  Project:  RESPM
//  Description:
//        Resolve merge conflict for RM to Baseline 
//
//  Revision: 038   By: gdc   Date:  22-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Added RM data items.
//
//  Revision: 037   By: gdc   Date:  10-June-2005    DR Number: 6170
//  Project:  NIV2
//  Description:
//      DCS 6170 - NIV2
//      Added breath trigger id for patient-data packet.
//   
//  Revision: 036  By: jja     Date:  05-Sep-2001    DR Number: 5748
//  Project:  GUIComms
//  Description:
//		Added trueExhTime_ data member to keep track of actual exh time
//		for the IsPhaseRestricted logic.  The NULL_INSPIRATION during
//		Bilevel was continuing the exh time from high peep to low peep,
//		thus setting IsPhaseRestricted to false causing a stacked breath.
//
//  Revision: 035   By: syw   Date:  19-Jun-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//		PAV project-related changes:
//		Handle for PAV inspiratory pause.
//
//  Revision: 034   By: syw   Date:  19-Jun-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//		Added filtering to lung pressure
//
//  Revision: 033  By: syw     Date:  07-Sep-2000    DR Number: 5748
//  Project:  Delta
//  Description:
//		Added trueExhTime_ data member to keep track of actual exh time
//		for the IsPhaseRestricted logic.  The NULL_INSPIRATION during
//		Bilevel was continuing the exh time from high peep to low peep,
//		thus setting IsPhaseRestricted to false causing a stacked breath.
//
//  Revision: 032  By: jja     Date:  20-Apr-2000    DR Number: 5708
//  Project:  NeoMode
//  Description:
//		Revise Pmean to be averaged value instead of breath to breath.  
//              Delete static MeanAirwayPressure_ data member and replace with 
//              meanAirwayPressure_ data member.
//		Delete static GetMeanAirwayPressure method and replace
//              with getMeanAirwayPressure() method.
//
//  Revision: 031  By: syw     Date:  18-Jul-1999    DR Number: 5471, 5481
//  Project:  ATC
//  Description:
//		Added BreathTypeDisplay_ data member and access method.
//
//  Revision: 030  By: iv     Date:  18-May-1999    DR Number: 5322
//       Project:  ATC
//  Description:
//          Added members for Pwye estimates based on Pi and Pe.
//
//  Revision: 029  By: sah     Date:  12-Jan-1999    DR Number: 5321
//       Project:  ATC
//  Description:
//          Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 028  By:  syw   Date:  16-Jul-1998    DR Number: DCS 5120
//       Project:  Sigma (840)
//       Description:
//			Added IsPeepTransition_ data member and access methods.
//
//  Revision: 027  By:  syw    Date:  07-Jul-1998    DR Number: DCS 5157
//       Project:  Sigma (840)
//       Description:
//			Added handle for BILEVEL_PAUSE_PHASE.
//
//  Revision: 026  By:  iv    Date:   28-May-1998    DR Number: 5100
//       Project:  Sigma (840)
//       Description:
//			Added a data member PeakPrevNetFlow1_ .
//
//  Revision: 025  By: syw     Date:  14-May-1998    DR Number: DCS 5095
//  	Project:  Sigma (840)
//		Description:
//			Added maxExpiredVolume_ data member and access method.
//
//  Revision: 024  By:  iv    Date:  18-Jan-1998    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//          Bilevel initial version.
//
//  Revision: 023  By: iv     Date:  21-Oct-1997    DR Number: DCS 2572
//  	Project:  Sigma (840)
//		Description:
//			Added a static const Real32 PCV_TIME_FACTOR.
//
//  Revision: 022  By: syw    Date:  25-Sep-1997    DR Number: DCS 1135
//  	Project:  Sigma (840)
//		Description:
//			Created FirstCycleOfInspiration_, DisplayAlpha_, and DisplayValue_,
//			data members for filtering.  Added FirstCycleOfPlateau_ data
//			member and set methods.
//
//	Revision: 021  By:  syw    Date:  12-Aug-1997    DR Number: DCS 2357
//      Project:  Sigma (R8027)
//      Description:
//			Use only insp time in call to calculateComplianceBtpsVol() ;
//
//	Revision: 020  By:  syw    Date:  17-Jul-1997    DR Number: DCS 2309
//      Project:  Sigma (R8027)
//      Description:
//			Added counter to determine when to terminate spiro condition.
//
//  Revision: 019  By:  sp    Date: 10-Apr-1997    DR Number: DCS 1867
//       Project:  Sigma (R8027)
//       Description:
//             Fixed exh spont min vol calculation.
//
//  Revision: 018  By:  sp    Date: 1-Apr-1997    DR Number: DCS NONE
//       Project:  Sigma (R8027)
//       Description:
//             Inspection rework.
//
//  Revision: 017  By:  sp    Date: 27-Mar-1997    DR Number: DCS 1780 
//       Project:  Sigma (R8027)
//       Description:
//             Added SpiroTime_.
//
//  Revision: 016  By:  sp    Date: 12-Dec-1996    DR Number: DCS 1780
//       Project:  Sigma (R8027)
//       Description:
//             Removed NetFlowCounter_
//
//  Revision: 015  By:  sp    Date: 12-Dec-1996    DR Number: DCS 1611
//       Project:  Sigma (R8027)
//       Description:
//             Add previous breath type.
//
//  Revision: 014  By:  sp    Date: 22-Oct-1996    DR Number: NONE
//       Project:  Sigma (R8027)
//       Description:
//             Inspection rework.
//
//  Revision: 013  By:  sp    Date:  9-Oct-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Added data member AirwayPressure_ and its access method
//             GetAirwayPressure.
//
//  Revision: 012  By:  sp    Date:  9-Oct-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Added data member RInspResistance_ and RExhResistance_.
//             Removed unit test methods.
//
//  Revision: 011  By:  sp    Date:  23-Sept-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Added data member EndInspPressure_  and method GetEndInspPressure.
//
//  Revision: 010  By:  sp    Date:  19-Sept-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Changed LastEndExpPressurePatData_ to LastEndExpPressure_
//             and GetLastEndExpPressurePatData to GetLastEndExpPressure.
//
//  Revision: 009  By:  sp    Date:  13-Sept-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Add unit test method.
//             Change GetIsFlowResumed to IsFlowResumed.
//
//  Revision: 008  By:  sp    Date:  5-Sept-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Remove method GetPeakExpiratoryPressure and 
//             data member PeakExpiratoryPressure_.
//
//  Revision: 007  By:  sp    Date:  3-Sept-1996    DR Number: DCS 1293
//       Project:  Sigma (R8027)
//       Description:
//             Add data members LastEndExpPressurePatData_, IsFlowResumed_,
//             PrevNetFlow_ and NetFlowCounter_.
//             Add access methods GetLastEndExpPressurePatData, GetFlowTarget,
//             and GetIsFlowResumed.
//             Delete data member DiscDeliveredVolume_ and its access methods.
//
//  Revision: 006  By:  sp    Date:  24-Jun-1996    DR Number: DCS 967
//       Project:  Sigma (R8027)
//       Description:
//             Add argument peepRecovery to newInspiration and
//             access method IsPeepRecovery.
//             Add static data member IsPeepRecovery_.
//
//  Revision: 005  By:  sp    Date:  8-May-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Add unit test method setMandType. 
//
//  Revision: 004  By:  sp    Date:  30-Apr-1996    DR Number: DCS 967
//       Project:  Sigma (R8027)
//       Description:
//             Add new data member DiscDeliveredVolume_, new methods 
//             SetDiscDeliveredVolume and GetDiscDeliveredVolume.
//
//  Revision: 003  By:  sp    Date:  4-Mar-1996    DR Number: DCS 674
//       Project:  Sigma (R8027)
//       Description:
//             Remove #ifndef.
//
//  Revision: 002  By:  sp   Date:  15-Dec-1995    DR Number: DCS 624, 600
//       Project:  Sigma (R8027)
//       Description:
//             Add new data member inspSideMinVol_ to store both air and o2 volume
//             accumulated throughout a breath (624).
//             Change BreathPhaseScheduler to SchedulerId (600).
//             Add unit test methods (600).
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#  include "Sigma.hh"
#include "Breath_Delivery.hh"

//@ Usage-Classes
#include "SchedulerId.hh"
#include "DiscreteValue.hh"
#include "BreathType.hh"
#include "BreathPhaseType.hh"
#include "BreathMiscRefs.hh"
#include "BreathData.hh"
#include "BD_IO_Devices.hh"
#include "MsgQueue.hh"
#include "IpcIds.hh"
#include "Resistance.hh"
#include "BdQueuesMsg.hh"
#include "Trigger.hh"
//@ End-Usage

extern const Int32 MIN_EXH_TIME;
static const Real32 PCV_TIME_FACTOR = 4.0F;  

class BreathRecord {
  public:
  	enum BiLevelPhases { LOW_TO_HIGH_INSP,					// low to high mand inspiration
						 LOW_TO_HIGH_EXH,					// low to high mand exhalation
						 FIRST_HIGH_SPONT_INSP,				// first spont during peep high
						 NORMAL_INSP_EXH,					// normal insp or exh
						 HIGH_SPONT_EXH_TO_LOW_EXH } ;		// high peep spont exh to low peep exh

	enum BtpsTypes { INSP_BTPS, EXP_BTPS} ;

    BreathRecord(void);
    ~BreathRecord(void);

    void newInspiration(
        const SchedulerId::SchedulerIdValue schedulerId,
        const DiscreteValue mandatoryType,
        const BreathType breathType,
        const BreathPhaseType::PhaseType phaseType,
        const Boolean peepRecovery,
	    const Boolean isMandBreath = FALSE,
        const Boolean isUsingProxFlow = FALSE);
	

    Real32 calculateComplianceBtpsVol(
			const Real32 vol,
			const Real32 endInspPress,
			const Real32 endExpPress,
			const Int32 inspTime,
			const BtpsTypes type = EXP_BTPS) ;

    void newExhalation(void);
    void newCycle(void);
    inline void startExpiratoryPause(void);
    inline void startInspiratoryPause(void);
    inline void startPavInspiratoryPause(void);
    inline void startBilevelPausePhase(void);
    inline void startNifPause(void);
    inline void startP100Pause(void);
    inline void startVitalCapacityPause(void);
	inline Boolean getIsProxFlowBreath(void) const; 
	inline Real32 getExpiredVolume(void) const;
    inline Real32 getProxExpiredVolume(void) const;
    inline Real32 getMaxExpiredVolume(void) const;
    inline Real32 getComplianceExhVolume(void) const;
    inline Real32 getInspiredVolume(void) const;
    inline Real32 getInspSideMinVol(void) const;
    inline Real32 getBreathDuration(void) const;
    inline Real32 getInspTime(void) const;
    inline Real32 getMeanAirwayPressure(void) const;
    inline DiscreteValue getMandatoryType(void) const;
    inline SchedulerId::SchedulerIdValue getSchedulerId(void) const;
    inline BreathType getBreathType(void) const;
    inline Boolean isNoInspiration(void) const;
    inline Real32 getMandatoryVolumeFraction(void) const;

	// static methods
    static inline BreathType GetBreathTypeDisplay(void) ;
    static inline BreathType GetPreviousBreathType(void);
    static inline Real32 GetPeakAirwayPressure(void);
    static inline Real32 GetAirwayPressure(void);
    static inline Real32 GetPeakExpiratoryFlow(void);
    static inline Real32 GetEndInspPressure(void);
    static inline Real32 GetEndInspPressurePatData(void);
    static inline Real32 GetEndInspPressureComp(void);
    static inline Real32 GetEndExpPressurePatData(void);
    static inline Real32 GetLastEndExpPressure(void);
    static inline Real32 GetEndExpPressureComp(void);
    static inline Real32 GetInspiratoryTime(void);
    static inline Real32 GetFlowTarget(void);
    static inline Real32 GetPressWyeInspEst(void);
    static inline Real32 GetPressWyeExpEst(void);
    static inline Boolean IsExhFlowFinished(void);
    static inline Boolean IsPhaseRestricted(void);
    static inline Boolean IsPeepRecovery(void);
    static inline Boolean GetIsMandBreath( void) ;
    static inline Real32 GetBreathPeriod(void);
    static inline Int32 GetIntervalNumber(void);
    static inline Int32 GetExhTime(void);
    static inline BreathPhaseType::PhaseType GetPhaseType(void);
    static inline Trigger::TriggerId GetBreathTriggerId(void);

	static inline void SetFirstCycleOfPlateauFlag( void) ;
	
    static inline void SetBreathPeriod(const Real32 breathPeriod);
    static inline void SetFlowTarget(const Real32 flowTarget);

    static inline Real32 CalculateAirVolume(void);
    static inline Real32 CalculateO2Volume(void);

	static inline void SetBiLevelPhase( const BiLevelPhases newPhase) ;
	static inline void SetBiLevelFirstBreathType( const BreathType newBreathType) ;
	static inline void SetIsHighPeep( const Boolean isHighPeep) ;
	static inline BreathType GetBiLevelFirstBreathType( void) ;

    static inline Boolean GetIsPeepTransition( void) ;
    static inline void SetIsPeepTransition( const Boolean value) ;
    static inline void SetBreathTriggerId(const Trigger::TriggerId value);
		
    static inline Real32 GetPeakInspiratoryFlow(void);
    static inline Real32 GetEndExpiratoryFlow(void);

    static inline Int32   GetTrueExhTime(void);	
    static inline Boolean IsHighPeep(void);
    static inline Real32  GetEndDryExhFlow( void);
	static inline Real32 GetDistalPressure(void);
    static inline void ResetProxSubPress(void);

	static inline Real32 GetProxPressSub(void);
	static inline Real32 GetProxPressSubPrev(void);
	
	static inline Real32 GetWyePressEst(void);
	static inline Real32 GetWyePressEstPrev(void);
    static inline void ResetWyePressEst(void);

    static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL, 
			  const char*       pPredicate = NULL);

  
  protected:

  private:
    BreathRecord(const BreathRecord&);		// not implemented...
    void operator=(const BreathRecord&);    // not implemented...

    //@ Data-Member:  mandType_
    // data is available from the first cycle of inspiration.
    DiscreteValue mandType_;

    //@ Data-Member:  complianceExhVolume_
    // It is calculated at the end of exhalation.
    // The volume calculation is compliance and btps compensated.
    Real32  complianceExhVolume_;	

    //@ Data-Member:  expiredVolume_
    // data is available every cycle from beginning of exhalation,
    // and is not compensated.
    Real32  expiredVolume_;	
    //@ Data-Member:  proxExpiredVolume_
    // data is available (calculated) every cycle from ProxManager at beginning
    //  of inspiration (exhaled volume of last breath as calculated by prox manager).
    Real32  proxExpiredVolume_;

    //@ Data-Member:  isProxFlowBreath_
    // flag to stamp the breath's source of flow: from prox-flow or system flow
    // callers can pole this flag to determine if they are using a breath with prox flow or not
    Boolean isProxFlowBreath_;

    //@ Data-Member:  inspiredVolume_
    // data is available every cycle from beginning of inpiration,
    // but the calculation is completed at the end of inpiration.
    Real32  inspiredVolume_;

    //@ Data-Member:  inspSideMinVol_
    // data is available every cycle from beginning of inpiration,
    // but the calculation is completed at the end of exhalation.
    // This data memeber stores the air and o2 volume throughout a breath. 
    Real32  inspSideMinVol_;

    //@ Data-Member:  breathDuration_
    // data is available every cycle but completed at the last cycle of exhalation.
    Real32  breathDuration_;

    //@ Data-Member:  inspTime_
    // data is available every cycle but completed at the last cycle of exhalation.
    Real32  inspTime_;

    //@ Data-Member:  TrueExhTime_
    // data is available every cycle but completed at the last cycle of exhalation.
    static Uint32  TrueExhTime_;

    //@ Data-Member:  schedulerId_
    // data is available from the first cycle of inspiration.
    // it is require for apnea reset
    SchedulerId::SchedulerIdValue schedulerId_;

    //@ Data-Member:  breathType_
    // data is available from the beginning of inspiration.
    BreathType  breathType_;

    //@ Data-Member:  noInspiration_
    // flag indicating initiation of exhalation with no previous inspiration
    Boolean  noInspiration_;

	// STATIC DATA

	//@ Data-Member: BiLevelEndExhVolume_
	// stores end exh volume offset for volume - time plot
	static Real32 BiLevelEndExhVolume_ ;

	//@ Data-Member: Uint32 BiLevelMandTime_
	// time of the last mand delivery before the first spont during high peep.
	static Uint32 BiLevelMandTime_ ;

	//@ Data-Member: Real32 BiLevelEndMandPressure_ ;
	// pressure for compliance compensation of the mand breath before the
	// first spont during high peep.
	static Real32 BiLevelEndMandPressure_ ;	

    //@ Data-Member:  mandatoryVolumeFraction_
    // indicates how much of the exhaled volume can be attributed to
    // a mandatory breath.
    Real32 mandatoryVolumeFraction_;

    //@ Data-Member:  PreviousBreathType_
    // data is available from the beginning of inspiration.
    static BreathType  PreviousBreathType_;

    //@ Data-Member:  RInspResistance_
    // inspiration side tubing resistance
    static Resistance  RInspResistance_;

    //@ Data-Member:  RExhResistance_
    // exhalation side tubing resistance
    static Resistance  RExhResistance_;

    //@ Data-Member:  PhaseType_
    // data is available from the beginning of inspiration.
    static BreathPhaseType::PhaseType PhaseType_;

    //@ Data-Member:  PeakAirwayPressure_
    // data is available at the last cycle of exhalation.
    static Real32  PeakAirwayPressure_;

    //@ Data-Member:  meanAirwayPressure_
    // data is available at the last cycle of exhalation.
    Real32  meanAirwayPressure_;

    //@ Data-Member:  AirwayPressure_
    // data is available every cycle.
    static Real32  AirwayPressure_;

    //@ Data-Member:  AirwayPressureAccum_
    // used to calculate meanAirwayPressure_.
    static Real32  AirwayPressureAccum_;

    //@ Data-Member:  PressWyeInspEst_
    // data is available everyCycle.
    static Real32  PressWyeInspEst_;

    //@ Data-Member:  PressWyeExpEst_
    // data is available everyCycle.
    static Real32  PressWyeExpEst_;

    //@ Data-Member:  IntervalNumber_
    // used to calculate averages
    static Int32  IntervalNumber_;

    //@ Data-Member:  PeakExpiratoryFlow_
    // data is available at the last cycle of exhalation.
    static Real32  PeakExpiratoryFlow_;

    //@ Data-Member:  PeakInspiratoryFlow_
    // data is only available at the last cycle of inspiration.
    static Real32  PeakInspiratoryFlow_;

    //@ Data-Member:  EndInspPressure_
    // data is available after the transition to exhalation.
    // use for disconnect algorithm.
    static Real32  EndInspPressure_;

    //@ Data-Member:  EndInspPressureComp_
    // data is available after the transition to exhalation.
    // use for compliance volume calculation.
    static Real32  EndInspPressureComp_;

    //@ Data-Member:  EndInspPressurePatData_
    // data is available after the transition to exhalation.
    // for patient data.
    static Real32  EndInspPressurePatData_;

    //@ Data-Member:  EndExpPressureComp_
    // data is available after exhalation
    // use for compliance volume calculation
    static Real32  EndExpPressureComp_;

    //@ Data-Member:  EndExpPressurePatData_
    // EndExpPressurePatData_ is stored.
    static Real32  EndExpPressurePatData_;

    //@ Data-Member:  LastEndExpPressure_
    // last end expiratory pressure is stored.
    // use for disconnect.
    static Real32  LastEndExpPressure_;

    //@ Data-Member:  InspiratoryTime_
    // data is completed at the end of inspiration.
    static Real32  InspiratoryTime_;

    //@ Data-Member:  IsPhaseRestricted_
    // data is available anytime 
    static Boolean IsPhaseRestricted_;

    //@ Data-Member:  BreathPeriod_
    // get and set access method are provided
    static Real32 BreathPeriod_;

    //@ Data-Member:  FlowTarget_
    // this data member is used for exhaled volume calculation.
    static Real32 FlowTarget_;

    //@ Data-Member:  AirFlowSum_
    // this data member is only used for O2Mix object
    static Real32 AirFlowSum_;

    //@ Data-Member: O2FlowSum_ 
    // this data member is only used for O2Mix object
    static Real32  O2FlowSum_;

    //@ Data-Member: IsExhFlowFinished_ 
    // It is used by DiconnectTrigger
    static Boolean  IsExhFlowFinished_;

    //@ Data-Member: ExhTime_ 
    // Exhalation elapsed time without including expiratory pause
    static Int32  ExhTime_;

    //@ Data-Member: IsPeepRecovery_ 
    // peep recovery is default to false.
    static Boolean  IsPeepRecovery_;

    //@ Data-Member: IsMandBreath_ 
    // set if biLevel low to high transition, default to false.
    static Boolean  IsMandBreath_ ;

    //@ Data-Member: PrevNetFlow1_ 
    // previous net flow alpha filter 1
    static Real32  PrevNetFlow1_;

    //@ Data-Member: PrevNetFlow2_ 
    // previous net flow alpha filter 2
    static Real32  PrevNetFlow2_;

    //@ Data-Member: PeakPrevNetFlow1_ 
    // peak value for previous net flow alpha filter 1
    static Real32  PeakPrevNetFlow1_;

	//@ Data-Member: spiroTerminateCount_
	// counter to determine when spiro terminate condition is met
	static Uint32 SpiroTerminateCount_ ;

	//@ Data-Member: firstCycleOfExhalation_
	// true if first cycle of exhalation
	static Boolean FirstCycleOfExhalation_ ;

	//@ Data-Member: FirstCycleOfInspiration_
	// true if first cycle of exhalation
	static Boolean FirstCycleOfInspiration_ ;

	//@ Data-Member: Boolean FirstCycleOfPlateau_
	// true if first cycle of plateau
	static Boolean FirstCycleOfPlateau_ ;

	//@ Data-Member: Real32 DisplayAlpha_
	// alpha for pressure display
	static Real32 DisplayAlpha_ ;

	//@ Data-Member: Real32 DisplayValue_
	// patient wye pressure filtered value for display
	static Real32 DisplayValue_ ;

	//@ Data-Member: Real32 DisplayLungPressure_
	// lung pressure filtered value for display
	static Real32 DisplayLungPressure_ ;

	//@ Data-Member: maxExpiredVolume_
	// maximum expired volume for breath
	Real32 maxExpiredVolume_ ;
	
	//@ Data-Member: BiLevelPhases biLevelPhase_
	// use to determine which biLevel phase the breath is in.
	static BiLevelPhases BiLevelPhase_ ;

	//@ Data-Member: BiLevelPhases PreviousBiLevelPhase_
	// use to determine the previous biLevel phase the breath is in.
	static BiLevelPhases PreviousBiLevelPhase_ ;

    //@ Data-Member: BreathType BiLevelFirstBreathType_
    // stores the breath type of the low to high mand breath
    static BreathType BiLevelFirstBreathType_ ;

	//@ Data-Member: Boolean IsHighPeep_
	// set if in high peep during BiLevel
	static Boolean IsHighPeep_ ;

	//@ Data-Member: Boolean IsPeepTransition_
	// set if currrent breath is phasing in a peep change
	static Boolean IsPeepTransition_ ;

    //@ Data-Member:  BreathTypeDisplay_
    // data is available from the beginning of inspiration.
    static BreathType  BreathTypeDisplay_;
	
    //@ Data-Member:  BreathTriggerId_
    // set to the current breath trigger
    static Trigger::TriggerId  BreathTriggerId_;
	
    //@ Data-Member:  EndExpiratoryFlow_
    // End flow rate observed during exhalation before inspiration
    static Real32  EndExpiratoryFlow_;


    //@ Data-Member:  EndDryExhFlow_
    // Dry exhalation flow at the end of exhalation.
	static Real32 EndDryExhFlow_;

	//@ Data-Member: StartProxInspCycleCount_
	// Flag which indicates to start counting the prox 
	// cycle delay for sending inspiratory patient data 
	// packets.
    static Boolean StartProxInspCycleCount_;
    
	//@ Data-Member: ProxInspCycleCount_
	// The number of prox inspiratory delay cycle count
	static Uint8 ProxInspCycleCount_;

	//@ Data-Member: StartProxExpCycleCount_
	// Flag which indicates to start counting the prox 
	// cycle delay for sending expiratory patient data 
	// packets.
    static Boolean StartProxExpCycleCount_;
    
	//@ Data-Member: ProxExpCycleCount_
	// The number of prox expiratory delay cycle count
	static Uint8 ProxExpCycleCount_;

	//@ Data-Member: DistalPressure_
	// The current pressure from exp side
	static Real32 DistalPressure_;

	//@ Data-Member: ProxPressSub_
	// Prox substitution pressure.
	static Real32 ProxPressSub_;

	//@ Data-Member: ProxPressSubPrev_
	// Previous Prox substitution pressure.
    static Real32 ProxPressSubPrev_;

	//@ Data-Member: IsProxSubPressEnabled_
	// Stores whether prox substitution pressure
	// is enabled or disabled.
	static Boolean IsProxSubPressEnabled_;

	//@ Data-Member: ProxPrevPressure_
	// Stores the prox's previous pressure.
	static Real32 ProxPrevPressure_;

	//@ Data-Member: WaveformPrevPressure_
	// Stores the waveform previous pressure.
	static Real32 WaveformPrevPressure_;

    //@ Data-Member: WyePressEst_
	// Estimated Wye Pressure.
	static Real32 WyePressEst_;

	//@ Data-Member: WyePressEstPrev_
	// Estimated Wye pressure previous.
    static Real32 WyePressEstPrev_;

#if defined(SIGMA_UNIT_TEST) || defined(SIGMA_DEBUG)
public:
    void write1(void);
    static void write2(void);
#endif //SIGMA_UNIT_TEST || defined(SIGMA_DEBUG)
};


// Inlined methods...
#include "BreathRecord.in"


#endif // BreathRecord_HH 
