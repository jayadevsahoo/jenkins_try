#include "stdafx.h"

#if        defined(SIGMA_GUI_CPU) || defined(SIGMA_COMMON)

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, NellCor/Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: VariableBoundedInfo -  Breath Data Processing agent class 
//      for those patient data items that have absolute upper and lower
//      values, but with some variable constraints, as well.
//---------------------------------------------------------------------
//@ Interface-Description
//
//>Von

// TBD....

//  Description:
//  ------------
//  The 'VariableBoundedInfo' class processes bounded Patient data types.
//  A 'BoundedDataInfo' class object  maintains upper and lower boundry
//  values, data precision information for a specific bounded breath
//  item type.  A new data value is clipped to fit within upper and lower
//  boundry values of the data type.
//  This object buffers the latest data within its double buffer
//  to maintain data integrity between a reader and a writer of the data.
//
//  Calss Key Methods:
//  -----------------
//     modifyData_  -- the method clips the raw breath data value, 
//             determines which precision to use for a data value,
//             and buffers the data. The method also sends out a Breath
//             Item change notification.
//@ Rationale
//     The purpose of this class is to put all the procssing
//     smarts of a bounded breath item type into one object to
//     avoid these smarts spreading all over the patient data manager.
//       
//---------------------------------------------------------------------
//@ Implementation-Description
//     The class is a subclass of the "BreathItemDataInfo" class.
//     The parent class handles most of the double buffer indexing,
//     provides data access methods to outside and data update notice.
//     This class  maintains minimum and maximum allowed value range,
//     upper and lower precisions that can be used for a data value.
//     The PreciseValue routine computes a data value which fits to
//     a given precision, this computation process is called warping.
//     This method saves a warped data value in its double buffer.
//---------------------------------------------------------------------
//@ Fault-Handling
//    Follows the standard Sigma contract programming model, Class
//    precodition and Class assertion are used to raise a fatal software
//    fault.
//>Voff
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Patient-Data/vcssrc/VariableBoundedInfo.ccv   25.0.4.0   19 Nov 2013 14:18:00   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By: sah   Date:  09-Jan-2001    DR Number: 5779
//  Project:  PAV
//  Description:
//      Needed for IBW-based resistance ranges.
//
//=====================================================================

#include "VariableBoundedInfo.hh"
#include "MathUtilities.hh"

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: VariableBoundedInfo( const PatientDataId::PatientItemId id,
//    const Real32 min, const Real32 mid,const Real32 max,
//    const Real32 changePoint, const Precision lowerP, const Precision higherP)
//    Constructor
//
//@ Interface-Description
//    Initializes  all the private and inherited member variables.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

VariableBoundedInfo::VariableBoundedInfo(
    const PatientDataId::PatientItemId id, const Real32 min,
	const Real32 max, const Real32 changePoint, const Precision lowerP,
    const Precision higherP)
        : BoundedDataInfo(id, min, max, changePoint, lowerP, higherP),
		  lowerVariableValue_(min),
		  lowerVariablePrecision_(lowerP),
		  upperVariableValue_(max),
		  upperVariablePrecision_(higherP)
{
  CALL_TRACE("VariableBoundedInfo()");
} // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~VariableBoundedInfo()  [Destructor]
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

VariableBoundedInfo::~VariableBoundedInfo(void)
{
  CALL_TRACE("~VariableBoundedInfo()");
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: modifyData_(void *pValue) [virtual]
//
//@ Interface-Description

// TBD...

//     Processes a raw breath data value and maintains the modified data
//     in its buffer. Each data processing agent maintains upper and lower
//     boundry values. The method clips a raw value to a boundary limit 
//     value if a data value is outside of these two boundries.
//     It chooses a precision to use based on the data value.
//     Initially, modified data is always considered fresh( not timed out).
//     The method informs other subsystems of a new data change.
//---------------------------------------------------------------------
//@ Implementation-Description
//      The modifyData_ method is called whenever a  new breath data 
//      is received from the Breath Delivery System.
//      Processed data is put into a network buffer to avoid over writing
//      an active data that might have been in the middle of being
//      accessed by another task before the NetworkApp task interrupted.
//
//      The parent class's BreathItemDataInfo::modifyData_ is called
//      to send data change notification and make the newly received data
//      data to be an active one.
//      A precision choice is made once more after a data is warped
//      since warping a number using a precision can round up a number
//      to a next precision level.
//
//     $[03001], $[03013], $[03014], $[03016], $[03017],
//     $[03020], $[03021], $[03028], $[03029], $[03032], $[03033],
//     $[03039], $[03040], $[03048], $[03049], $[03059], $[03060],
//     $[03063], $[03064], $[XXX001], $[XXX002], $[YYY001], $[YYY002]
//     $[03025], $[03026], $[03036], $[03037]
//     $[TC03001], $[TC03002]
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
VariableBoundedInfo::modifyData_(void *pValue)
{
  CLASS_PRE_CONDITION(pValue);

  // forward to base class, allowing it to verify against the absolute
  // constraints, setting the clipping info accordingly...
  BoundedDataInfo::modifyData_(pValue);

  const Real32   COOKED_VALUE = data_[index_.active].bValue.data.value;

  if (COOKED_VALUE <= lowerVariableValue_  ||
	  COOKED_VALUE >= upperVariableValue_)
  {                                // $[TI1]
	if ( COOKED_VALUE <= lowerVariableValue_ )
	{                                // $[TI1.1]
	  data_[index_.network].bValue.data.value     = lowerVariableValue_;   
	  data_[index_.network].bValue.data.precision = lowerVariablePrecision_;
	}                                                        
	else
	{                                // $[TI1.2]
	  data_[index_.network].bValue.data.value     = upperVariableValue_;
	  data_[index_.network].bValue.data.precision = upperVariablePrecision_;
	}

	data_[index_.network].bValue.clippingState = BoundedValue::VARIABLE_CLIP;

	// call up to base class's base class, to transition these "network" values
	// to "active" values...
	BreathItemDataInfo::modifyData_(pValue);
  }									// $[TI4]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
VariableBoundedInfo::SoftFault(const SoftFaultID  softFaultID,
						   const Uint32       lineNumber,
						   const char*        pFileName,
						   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, PATIENT_DATA,
						  BOUNDED_DATA_INFO, lineNumber, pFileName, pPredicate);
}


//=====================================================================
//
//  Private Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processLimitValue_(rawLimitValue, rLimitValue, rPrecision)
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
VariableBoundedInfo::processLimitValue_(const Real32  rawLimitValue,
										Real32&       rLimitValue,
										Precision&    rPrecision)
{
  CALL_TRACE("processLimitValue_(rawLimitValue, rLimitValue, rPrecision)");

  Precision  precision;

  if (rawLimitValue < MID_CHANGE_POINT_)
  {     //    $[TI1]
	precision = LOWER_PRECISION_;
  }
  else
  {    //    $[TI2]
	precision = UPPER_PRECISION_;
  }

  Real32  cookedLimitValue = ::PreciseValue(rawLimitValue, precision);

  // need to check precision again since the warping might have pushed 
  // the number to next range...
  if (cookedLimitValue >= MID_CHANGE_POINT_  ||
	  ::IsEquivalent(cookedLimitValue, MID_CHANGE_POINT_, LOWER_PRECISION_))
  {    //    $[TI3]
	  precision        = UPPER_PRECISION_;
	  cookedLimitValue = ::PreciseValue(cookedLimitValue, precision);
  }    //    $[TI4]

  // set the final ("cooked") value and precision...
  rLimitValue = cookedLimitValue;
  rPrecision  = precision;
}

#endif        //defined(SIGMA_GUI_CPU) || defined(SIGMA_COMMON)
