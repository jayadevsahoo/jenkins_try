#ifndef  IoRegisterCons_HH
#define	IoRegisterCons_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1994, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
//@ Filename: IoRegisterCons.hh
//      Constant enum definitions for the IO control register one.
//---------------------------------------------------------------------
//@ Version
//@(#) $Header:   /840/Baseline/Download/vcssrc/IoRegisterCons.hhv   25.0.4.0   19 Nov 2013 14:02:50   pvcs  $ 
//@ Modification-Log
//
//  Revision: 001  By:  <jhv>    Date:  23-March-96    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//====================================================================

enum IoControlReg
{
	 IO_1_WR_FLASH_WRITE= 0x07,     // Flash Voltage  Write on (7)/ off 0
	 GUI_IO_1_DEFAULT =   0x70, 	// do not reset remote alarm, audio and the lan
	 BD_IO_1_DEFAULT =	0x10,  		// do not reset the lan.
	 IO_1_BD_BOARD = 0x80
};

#endif // IoRegisterCons_HH 
