#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: FocusButton - A button which displays a piece of text in its
// interior (in the button's Label container) and takes the current 
// persistant focus.
//---------------------------------------------------------------------
//@ Interface-Description
// This is a button derived directly from the TextButton
// class which provides functionality for displaying a button with
// a text.  The text, specified in the constructor, is positioned at 
// 0, 0 in the button's "label container". The button's action is 
// controlled by the subscreen.
//---------------------------------------------------------------------
//@ Rationale
// Focus buttons can be used in numerous screens so it made sense to 
// create a generic FocusButton class with a text label and has the 
// ability to take persistent focus.
//---------------------------------------------------------------------
//@ Implementation-Description
// This class simply inherits from TextButton class and on a buttonDown 
// event, it takes persistent focus.  Also on a buttonUp 
// event it either releases persistent focus.
//---------------------------------------------------------------------
//@ Fault-Handling
// none
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/FocusButton.ccv   25.0.4.0   19 Nov 2013 14:07:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By: rhj    Date: 06-Jun-2007    SCR Number: 6237
//  Project:  Trend
//  Description:
//      Initial version.
//=====================================================================

#include "FocusButton.hh"
#include "GuiApp.hh"
#include "PromptArea.hh"
#include "MessageArea.hh"
//@ Usage-Classes     
//@ End-Usage

// TODO E600 remove
//#include "Ostream.hh"
//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: FocusButton  [Constructor]
//
//@ Interface-Description
// Creates a FocusButton.  Passed the normal Button parameters along
// with a StringId for specifying the text of the button.  The
// parameter list is as follows:
// >Von
//  xOrg            The X co-ordinate of the button.
//  yOrg            The Y co-ordinate of the button.
//  width           Width of the button.
//  height          Height of the button.
//  buttonType      Type of the button, either LIGHT, LIGHT_NON_LIT or DARK.
//  bevelSize       Width of the button's internal border.
//  cornerType      Tells whether its corners are rounded or not, can be
//                  ROUND or SQUARE.
//  borderType      The border type (whether it has an external border or not).
//  title           The string displayed as the button's title (text).
//  auxTitleText    The button's auxillary text.
//  auxTitlePos     The positioning info for the button's auxillary text.
//  pushButtonType  The push type of the button.
//>Voff
//---------------------------------------------------------------------
//@ Implementation-Description
// Pass most parameters on to the parent class, Button, initialize
// title text and add the text field, titleText_, to the label
// container.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

FocusButton::FocusButton(Uint16 xOrg, 
						 Uint16 yOrg, 
						 Uint16 width, 
						 Uint16 height,
						 Button::ButtonType buttonType, 
						 Uint8 bevelSize,
						 Button::CornerType cornerType, 
						 Button::BorderType borderType,
						 StringId title, 
						 StringId auxTitleText, 
						 Gravity auxTitlePos,
						 Button::PushButtonType pushButtonType) :
TextButton(xOrg, 
		   yOrg, 
		   width, 
		   height,
		   buttonType, 
		   bevelSize,
		   cornerType, 
		   borderType,
		   title, 
		   auxTitleText, 
		   auxTitlePos,
		   pushButtonType), 
losingFocus_(FALSE),
isPressedDown_(FALSE),
usePersistentFocus_(FALSE)
{

	CALL_TRACE("FocusButton::FocusButton()");

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~FocusButton  [Destructor]
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

FocusButton::~FocusButton(void)
{
	CALL_TRACE("FocusButton::~FocusButton(void)");

	// Do nothing
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: downHappened_
//
//@ Interface-Description
// This is a virtual method inherited from being a Button.  It is called
// when this button is sent into the Down state, either by the operator
// pressing us down (byOperatorAction = TRUE) or by the application
// calling the method setToDown() (byOperatorAction = FALSE).
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class uses AdjustPanel::TakePersistentFocus() to take the
//  persistent focus.
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
FocusButton::downHappened_(Boolean byOperatorAction)
{
	CALL_TRACE("FocusButton::downHappened_(Boolean byOperatorAction)");

	// Grab the Adjust Panel focus for the button.
	if (usePersistentFocus_)
	{
		AdjustPanel::TakePersistentFocus(this);
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
									   PromptArea::PA_HIGH, PromptStrs::EMPTY_A);
	}
	else
	{
		// For future implementation of Non-PersistentFocus,
		CLASS_ASSERTION_FAILURE();
	}

	isPressedDown_ = TRUE;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelKnobDeltaHappened
//
//@ Interface-Description
// This is a virtual method inherited from being an AdjustPanelTarget.
// If this object has the Adjust Panel focus then it will be called when
// the operator turns the knob.  It is passed an integer corresponding
// to the amount the knob was turned.  
//---------------------------------------------------------------------
//@ Implementation-Description
// The knob event is passed on to the FocusButtonTarget whose pointer is
// stored in private data pFocusButtonTarget_.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
FocusButton::adjustPanelKnobDeltaHappened(Int32 delta)
{

	CALL_TRACE("adjustPanelKnobDeltaHappened(Int32 delta)");

	if (pFocusButtonTarget_ != NULL)
	{
		// Pass the delta to the focus button target object.
		pFocusButtonTarget_->adjustKnobHappened(delta);
	}

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: upHappened_
//
//@ Interface-Description
// This is a virtual method inherited from being a Button.  It is called
// when this button is sent into the Up state, either by the operator
// pressing us down (byOperatorAction = TRUE) or by the application
// calling the method setToUp() (byOperatorAction = FALSE).
//---------------------------------------------------------------------
//@ Implementation-Description
// If the button was pressed up by the operator then we release
// the Adjust Panel focus.  We do not release focus if we were pressed 
// up due to another event which cause this button to lose the focus.  
//
// If this button had an associated message then we must clear the
// Message Area.  The Prompt Area Confirmation prompt must be cleared
// also, if one was displayed.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
FocusButton::upHappened_(Boolean byOperatorAction)
{
	CALL_TRACE("FocusButton::upHappened_(Boolean byOperatorAction)");

	// Only clear the prompt area and message areas if we have 
	// been pressed down previously. If we're set up but do not
	// have focus, then don't mess with the message and prompt areas.
	if ( isPressedDown_)
	{
		// Clear the Message Area
		GuiApp::PMessageArea->clearMessage(MessageArea::MA_MEDIUM);

		// Clear Primary, Advisory and Secondary prompts
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY, 
									   PromptArea::PA_HIGH, NULL_STRING_ID);
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_ADVISORY,
									   PromptArea::PA_HIGH, NULL_STRING_ID);
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
									   PromptArea::PA_HIGH, NULL_STRING_ID);
		isPressedDown_ = FALSE;
	}


	// If we're invisible, do nothing else
	if (!isVisible())
	{
		return;
	}

	// Check to see if we're not in the process of losing Adjust Panel focus
	if (!losingFocus_ && byOperatorAction)
	{
		// Release the Adjust Panel focus if the operator pressed us up
		if (usePersistentFocus_)
		{
			AdjustPanel::TakePersistentFocus(NULL);
		}
		else
		{
	   	    // For future implementation of Non-PersistentFocus,
			CLASS_ASSERTION_FAILURE();
		}
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelLoseFocusHappened
//
//@ Interface-Description
// This is a virtual method inherited from being an AdjustPanelTarget.
// It is called when this button is about to lose the focus of
// the Adjust Panel.  This normally happens when some other button is
// pressed down or when this button is pressed up.  We must make sure
// this button pops up.
//---------------------------------------------------------------------
//@ Implementation-Description
// When we lose the focus we simply set this button to the Up state,
// if not already up.
//
// We use the private losingFocus_ to indicate why this button is
// being set up.  The setToUp() method ends up calling the
// upHappened_() method below.  upHappened_() will be called
// with the parameter byOperatorAction set to FALSE. If byOperatorAction
// is FALSE, the adjust panel focus is not cleared.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
FocusButton::adjustPanelLoseFocusHappened(void)
{
	CALL_TRACE("FocusButton::adjustPanelLoseFocusHappened(void)");

	losingFocus_ = TRUE;

	setToUp();
	losingFocus_ = FALSE;

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelAcceptPressHappened
//
//@ Interface-Description
// This is a virtual method inherited from being an AdjustPanelTarget.  It is
// called when the operator presses down the Accept key.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
FocusButton::adjustPanelAcceptPressHappened(void)
{
	CALL_TRACE("FocusButton::adjustPanelAcceptPressHappened(void)");

	setToUp();

	// the upHappened_ callback does not release focus so do it here
	CLASS_ASSERTION(usePersistentFocus_);
	AdjustPanel::TakePersistentFocus(NULL);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelClearPressHappened
//
//@ Interface-Description
// This is a virtual method inherited from being an AdjustPanelTarget.  It is
// called when the operator presses the Clear key.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
FocusButton::adjustPanelClearPressHappened(void)
{
	CALL_TRACE("FocusButton::adjustPanelClearPressHappened(void)");

	setToUp();

	// the upHappened_ callback does not release focus so do it here
	CLASS_ASSERTION(usePersistentFocus_);
	AdjustPanel::TakePersistentFocus(NULL);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//        [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition   
//  none
//@ End-Method
//=====================================================================

void
FocusButton::SoftFault(const SoftFaultID  softFaultID,
					   const Uint32       lineNumber,
					   const char*        pFileName,
					   const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, FOCUSBUTTON,
							lineNumber, pFileName, pPredicate);
}



