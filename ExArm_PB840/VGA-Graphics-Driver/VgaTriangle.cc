#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1994-1995, Puritan-Bennett Corporation
//=====================================================================  

// =========================== M O D U L E   D E S C R I P T I O N ====
//@ Class: VgaTriangle - triangle drawing routines
//--------------------------------------------------------------------
//@ Interface-Description
//  This module consists of the triangle drawing routines for the VGA
//  graphics library.
//---------------------------------------------------------------------
//@ Rationale
//  Isolate triangle routines.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Draw a filled triangle algorithm found on page 373 of
//  Advanced Programmer's Guide to the EGA/VGA, written by George Sutty 
//  and Steve Blair.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ $Header:   /840/Baseline/VGA-Graphics-Driver/vcssrc/VgaTriangle.ccv   25.0.4.0   19 Nov 2013 14:38:22   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003   By: gdc    Date:  27-Jan-2000    DCS Number: 5337
//  Project: 840 Neonatal
//  Description:
//	Optimized VGA library for Neonatal project to use BitBlt and Windows
//  Bitmap structures.
//
//  Revision: 002   By: gdc   Date: 21-Jun-1999   DCS Number: XXXX
//  Project:  Sigma Neonate (R8027)
//  Description:
//      Initial version.
//
//  Revision: 001   By: sah   Date: 01-Aug-1997   DCS Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//      Initial version to coding standards (Integration baseline).
//
//=====================================================================  

#include "VgaTriangle.hh"

//@ Usage-Classes
#include "DisplayContext.hh"
#include "GraphicsContext.hh"
//@ End-Usage


//=====================================================================  
//
//  Public Methods...
//
//=====================================================================  

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Free-Function:  DrawTriangle
//
//@ Interface-Description
//  This routine draws a simple outline, or unfilled, triangle on the 
//  display screen.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses VgaLine::DrawLine to draw the three sides of the triangle.
//  If clipping is needed it is done in the line drawing routine.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Free-Function
//=====================================================================  

void
VgaTriangle::DrawTriangle(DisplayContext & rDc, GraphicsContext & rGc,
			  Int x0, Int y0,
			  Int x1, Int y1,
			  Int x2, Int y2 )
{
  Int origLineWidth = rGc.getLineWidth();
  rGc.setLineWidth(1);
  VgaLine::DrawLine( rDc, rGc, x0, y0, x1, y1 );
  VgaLine::DrawLine( rDc, rGc, x0, y0, x2, y2 );
  VgaLine::DrawLine( rDc, rGc, x1, y1, x2, y2 );
  rGc.setLineWidth(origLineWidth);
  // $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  FillTriangle
//
//@ Interface-Description
//  This routine draws a filled or solid triangle on the display screen.
// ---------------------------------------------------------------------
//@ Implementation-Description
//  Draw a filled triangle. Algorithm found on page 373 of
//    Advanced Programmer's Guide to the EGA/VGA,
//    written by George Sutty and Steve Blair
//  Order the corners of the triange so that 'a' has the lowest y-coordinate
//  value, and 'b' has the largest y-coordinate value.
//  Handle special cases, i.e. one side of the triangle is a horizontal line.
//  Extend a horizontal line from 'c' to its intersection with line ab.
//  Use this horizontal line to form two triangles with horizontal bases,
//  Fill these newly formed upper and lower triangles by drawing horizontal
//  lines with the color.
//        a
//        | \
//        |   \
//        |upper\
//        --------c
//        |lower/
//        |   /
//        | /
//        b
//  Note:  All clipping will be done by the line drawing routines.
//       
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================  

void
VgaTriangle::FillTriangle(DisplayContext & rDc, GraphicsContext & rGc,
			  Int x0, Int y0,
			  Int x1, Int y1,
			  Int x2, Int y2 )
{
  Int origLineWidth = rGc.getLineWidth();
  rGc.setLineWidth(1);

  Int x[4];                // arrays to hold the x and y coordinates   
  Int y[4];                // of the triangle and the x and y          
		             // coordinates of the horizontal line that   
		             // divides the triangle into two triangles  
  Int a;                   // indexes into the arrays to order points  
  Int b;
  Int c;
  Int temp;

  // set up the arrays with input values   
  x[0] = x0;
  x[1] = x1;
  x[2] = x2;
  y[0] = y0;
  y[1] = y1;
  y[2] = y2;

  // determine the relative position of the 3 points along the y axis    
  // with 'a' the lesser index of the y coordinate and 'b' the greatest 
  // index of the y coordinate if a horizontal side is found, order 
  // the x coordinates    

  a = 0;  // set 'a' to the smallest index of the y coordinate   

  if (y[a] > y[1])
  {   // $[TI1]
    a = 1;        
  }   // $[TI2]

  if (y[a] > y[2])
  {   // $[TI3]
    a = 2;
  }   // $[TI4]

  b = (a + 1) % 3;
  c = (b + 1) % 3;

  if (y[a] == y[b]  &&  x[a] > x[b])
  {   // $[TI5]
    a = b;
  }   // $[TI6]

  if (y[a] == y[c]  &&  x[a] > x[c])
  {   // $[TI7]
    a = c;
  }   // $[TI8]

  b = (a + 1) % 3;
  c = (b + 1) % 3;

  if (y[b] < y[c]  ||  (y[b] == y[c]  &&  x[b] > x[c]))
  {   // $[TI9] --  set 'b' to the largest index of the y coordinate    
    temp = b;
    b    = c;
    c    = temp;
  }   // $[TI10]

  if (y[a] == y[b])
  {   // $[TI11] -- only happens if all 3 points on a horizontal line   
    VgaLine::DrawLine(rDc, rGc, x[a], y[b], x[b], y[b]);
    VgaLine::DrawLine(rDc, rGc, x[a], y[c], x[c], y[c]);
  }
  else if (y[a] == y[c])
  {   // $[TI12] -- top side of triangle is horizontal    
    VgaTriangle::FillDown_(rDc, rGc, x[a], y[a], x[b], y[b], x[c], y[c], y[c]);
  }
  else if (y[b] == y[c])
  {   // $[TI13] -- bottom side of triangle is horizontal   
    VgaTriangle::FillUp_(rDc, rGc, x[a], y[a], x[b], y[b], x[c], y[c], y[c]);
  }
  else
  {   // $[TI14] -- general case, 'a' top, 'b' bottom, 'c' middle     
    // split triangle by a horizontal line through 'c' to line ab   
    y[3] = y[c];   // one up one down   
    x[3] = x[a] + (((y[3] - y[a]) * (x[b] - x[a])) / (y[b] - y[a]));

    if (x[3] < x[c])
    {   // $[TI14.1]
      VgaTriangle::FillUp_  (rDc, rGc, x[a], y[a], x[b], y[b], x[c], y[c], y[c]);
      VgaTriangle::FillDown_(rDc, rGc, x[a], y[a], x[b], y[b], x[c], y[c], y[c]);
    }
    else
    {   // $[TI14.2]
      VgaTriangle::FillUp_  (rDc, rGc, x[a], y[a], x[c], y[c], x[b], y[b], y[c]);
      VgaTriangle::FillDown_(rDc, rGc, x[c], y[c], x[b], y[b], x[a], y[a], y[c]);
    }
  }

  // Draw the sides of the triangle in the same color
  // This may help eliminate some of the jagged edges on the sides
  VgaLine::DrawLine(rDc, rGc, x0, y0, x1, y1);
  VgaLine::DrawLine(rDc, rGc, x0, y0, x2, y2);
  VgaLine::DrawLine(rDc, rGc, x1, y1, x2, y2);

  rGc.setLineWidth(origLineWidth);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
VgaTriangle::SoftFault(const SoftFaultID softFaultID,
		       const Uint32      lineNumber,
		       const char*       pFileName,
		       const char*       pPredicate)
{
  FaultHandler::SoftFault(softFaultID, VGA_GRAPHICS_DRIVERS,
			  VgaGraphicsDriver::VGA_TRIANGLE_ID, 
			  lineNumber, pFileName, pPredicate);
}


//=====================================================================  
//
//  Private Methods...
//
//=====================================================================  

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: FillUp_
//
//@ Interface-Description
//  This routine is only called by VgaTriangle::FillTriangle.  It fills
//  the upper area of the triangle by drawing horizontal lines from one
//  side of the triangle to the other side of the triangle. 
//
//  There are no 'cx' and 'cy', because they are the same values as 'ax'
//  and 'ay', respectively.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Fill with horizontal lines drawn from one side of the triangle to
//  the other side of the triangle.
//
//  This is a general purpose fill routine that fills an area bounded
//  by two straight lines with horizontal lines.  The first 8 parameters
//  are the end points of the two defining lines, for a triangle the 
//  lines meet at the upper apex.   y0 is the y location of the base of
//  the triangle and color is the fill color.
//
//        |\
//        | \
//        |  \
//        |   \
//        |    \
//        |upper\
//        |______\
//
//---------------------------------------------------------------------
//@ PreCondition
//  (ay != dy  &&  by != ay)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================  

void
VgaTriangle::FillUp_( DisplayContext & rDc, GraphicsContext & rGc,
			Int ax, Int ay,
		    Int bx, Int by,
		    Int dx, Int dy,
		    Int y0 )
{
  Int  dyab;                      // computational variable   
  Int  dycd;                      // computational variable   

  dycd = dy - ay;
  dyab = by - ay;

  // "SAFE", because client logic prevents this...
  SAFE_AUX_CLASS_PRE_CONDITION((dycd != 0  &&  dyab != 0), dycd);

  for (Int y = ay; y <= y0; y++)
  {   // $[TI1] -- 'ay <= y0'...
    const Int  Y_FACTOR = (y - ay);

    // compute the distance in x direction from 'x[a]' for each y   
    const Int  DIST1 = (Y_FACTOR * (bx - ax)) / dyab;
    const Int  DIST2 = (Y_FACTOR * (dx - ax)) / dycd;

    VgaLine::DrawLine(rDc, rGc, (ax + DIST1), y, (ax + DIST2), y);
  }   // $[TI2] -- 'ay > y0'...
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: FillDown_
//
//@ Interface-Description
//  This routine is only called by VGAFillTriangle.  It fills the lower
//  area of the triangle by drawing horizontal lines from one side of the
//  triangle to the other side of the triangle. The horizontal top of
//  the lower triangular area is located at the corner of
//  the triangle that is bounded in the y direction by the remaining 
//  corners of the triangle. 
//
//  There are no 'dx' and 'dy', because they are the same values as 'bx'
//  and 'by', respectively.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Fill with horizontal lines drawn from one side of the triangle to
//  the other side of the triangle.
//
//  This is a general purpose fill routine that fills an area bounded
//  by two straight lines with horizontal lines.  The first 8 parameters
//  are the end points of the two defining lines, for this triangle the 
//  lines meet at the lower apex.   y0 is the y location of the base of
//  the triangle and color is the fill color.
//
//        ________
//        |      /
//        |lower/
//        |    /
//        |   /
//        |  /
//        | /
//        |/
//
//---------------------------------------------------------------------
//@ PreCondition
//  (by != cy  &&  by != ay)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================  

void
VgaTriangle::FillDown_(DisplayContext & rDc, GraphicsContext & rGc,
			  Int ax, Int ay,
		      Int bx, Int by,
		      Int cx, Int cy,
		      Int y0 )
{
  Int  dyab;                   // computation variable     
  Int  dycd;                   // computation variable     

  dycd = by - cy;
  dyab = by - ay;

  // "SAFE", because client logic prevents this...
  SAFE_AUX_CLASS_PRE_CONDITION((dycd != 0  &&  dyab != 0), dycd);

  for (Int y = by; y >= y0; y--)
  {   // $[TI1] -- 'by >= y0'...
    // compute the distance in x direction from 'x[a]' for each y   
    const Int  DIST1 = ((y - ay) * (bx - ax)) / dyab;
    const Int  DIST2 = ((y - cy) * (bx - cx)) / dycd;

    VgaLine::DrawLine(rDc, rGc, (ax + DIST1), y, (cx + DIST2), y);
  }   // $[TI2] -- 'by < y0'...
}
