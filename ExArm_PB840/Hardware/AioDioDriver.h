#pragma once

#ifndef AIO_DIO_DRIVER_H
#define AIO_DIO_DRIVER_H
//#############################################################################
//
//  AioDioDriver.h - Header file for AioDioDriver_t class.
//
//#############################################################################

// TODO E600 remove unnecessary and dead code later

#include "BD_IO_Devices.hh"
#include "bsp_version.h"
#include "AnalogInputs.h"
#include "Backlight.h"

//#include "Thread.h"
#include "AioDio.h"
#if defined(SIGMA_GUI_CPU)  && !defined(SIGMA_GUIPC_CPU)
#include "IOExpanderXRA1201.h"
#endif

enum DigitalOutputStates_t
{
    DOUT_LOW,
    DOUT_HIGH
};

enum DigitalInputStates_t
{
    DIN_LOW=0,
    DIN_HIGH=1
};

enum BrightnessLevel_t
{
	BRIGHT_70_PER = 6,
	BRIGHT_100_PER = 9
};

//static const DISPLAY_AND_STORAGE_DATA_TYPE       MIN_EXHALATION_VALVE_DAC = 0;
//static const DISPLAY_AND_STORAGE_DATA_TYPE       MAX_EXHALATION_VALVE_DAC = 4095;
//static const DISPLAY_AND_STORAGE_DATA_TYPE       EXHALATION_VALVE_OPEN   = MIN_EXHALATION_VALVE_DAC;
//static const DISPLAY_AND_STORAGE_DATA_TYPE       EXHALATION_VALVE_CLOSED = MAX_EXHALATION_VALVE_DAC;
//static const DWORD                               NO_BUTTON_PRESSED_DATA_BYTE_VALUE = 0xFF;

static const DigitalOutputStates_t EXHALATION_SOLENOID_ON_OFF_OPEN      = DOUT_LOW;
static const DigitalOutputStates_t EXHALATION_SOLENOID_ON_OFF_CLOSED    = DOUT_HIGH;

static const DigitalOutputStates_t AUTOZERO_SOLENOID_MANIFOLD           = DOUT_LOW;
static const DigitalOutputStates_t AUTOZERO_SOLENOID_AMBIENT            = DOUT_HIGH;

static const DigitalOutputStates_t AUDIO_AMPLIFIER_ENABLED              = DOUT_HIGH;
static const DigitalOutputStates_t AUDIO_AMPLIFIER_DISABLED             = DOUT_LOW;

static const DigitalOutputStates_t LED_ON                               = DOUT_HIGH;
static const DigitalOutputStates_t LED_OFF                              = DOUT_LOW;

static const DigitalOutputStates_t REMOTE_ALARM_SIGNAL_ACTIVE           = DOUT_LOW;
static const DigitalOutputStates_t REMOTE_ALARM_SIGNAL_INACTIVE         = DOUT_HIGH;

static const DigitalOutputStates_t BATTERY_COMM_RESET                   = DOUT_HIGH;
static const DigitalOutputStates_t BATTERY_COMM_ENABLED                 = DOUT_LOW;

static const DigitalOutputStates_t POWER_CONTROL_OFF                    = DOUT_HIGH;
static const DigitalOutputStates_t POWER_CONTROL_ON                     = DOUT_LOW;

static const DigitalOutputStates_t RELIEF_VALVE_OPEN                    = DOUT_LOW;
static const DigitalOutputStates_t RELIEF_VALVE_CLOSED                  = DOUT_HIGH;

static const DigitalOutputStates_t USER_INPUT_BEEP_ON                   = DOUT_LOW;
static const DigitalOutputStates_t USER_INPUT_BEEP_OFF                  = DOUT_HIGH;

static const DigitalOutputStates_t SET_REMOTE_ALARM_HARDWARE_INACTIVE   = DOUT_LOW;
static const DigitalOutputStates_t SET_REMOTE_ALARM_HARDWARE_ACTIVE     = DOUT_HIGH;

static const DigitalOutputStates_t I2C_MUX_DISABLE                      = DOUT_LOW;
static const DigitalOutputStates_t I2C_MUX_ENABLE                       = DOUT_HIGH;

static const DigitalOutputStates_t FLOW_SENSOR_COMM_DISABLE             = DOUT_LOW;
static const DigitalOutputStates_t FLOW_SENSOR_COMM_ENABLE              = DOUT_HIGH;

enum GpioDirection_t
{
    GPIO_OUTPUT,
    GPIO_INPUT
};
typedef enum
{
	DAC_AIR_VALVE,
	DAC_O2_VALVE,
    DAC_EXHALATION_VALVE,
    DAC_DESIRED_FLOW
} AnalogOutputs_t;


#ifdef SIGMA_BD_CPU

typedef enum
{
	TSI_AIR_FLOW,
	TSI_O2_FLOW
}TsiFlowType_t;

// These are the application related outputs for BD REV P3
enum ApplicationDigitalInputs_t
{
    DIN_NOAFS_ID        = 13,  // ETK_CTL       Turns on debug LED on PCB6009A
    DIN_ENET_INT        = 19,  // ETK_D5        Ethernet interrupt (active low)
    DIN_ENET_PME        = 22,  // ETK_D8        Ethernet Power Management Event, Steve marked as output.
    DIN_UI_ACK          = 24,  // ETK_D10       Software defined
    DIN_UI_OK           = 25,  // ETK_D11       UI SETS HIGH WHEN UI IS OK (active high)
    DIN_PB_OUT          = 57,  // GPMC_nCS6     ON/OFF SWITCH IS DEPRESSED WHILE LOW (active low)
    DIN_ACDC_ON         = 58,  // This is BD power source pin.
    DIN_FAN_ERR         = 95,  // CAM_VS        Active low
    DIN_PWRGD_3V3       = 97,  // CAM_PCLK
    DIN_DIN_PWRGD_5V0   = 98,  // CAM_FLD
    DIN_PWRGD_6V0       = 99,  // CAM_D0
    DIN_DY1             = 108, // fake
   	DIN_FLOW_SENSOR_CONNECTED, // fake

    DIN_Device_Alert    = 118, // McBSP2_DR     Device Alert is Active (active low)
    DIN_ID3             = 162, // McBSP1_CLKX   CAM_STROBE BOARD BOOT UP ID PIN
    DIN_ID1             = 167  // CAM_WEN BOARD BOOT UP ID PIN
};
enum ApplicationDigitalOutputs_t
{
    DOUT_BD_ACK          = 12,  // ETK_CLK      This was intended to allow an "I'm alive" toggle to the GUI
    DOUT_DUMMY_840_OUT   = 12,  // Duplicates the above because it isn't currently used. This gives unsued 840
                                // Digital outputs a valid ApplicationDigitalOutputs_t type to use.
	DOUT_IE_PHASE_OUT	 = 13,  // E600 assigened for IE PHASE out
    DOUT_LED4            = 14,  // ETK_D0
    DOUT_CRYPTO_PD       = 15,  // ETK_D1       Not currently used
    DOUT_AMB_LED         = 16,  // ETK_D2       SETS HIGH FOR CLINICAL ALARM REQUIRING AMB LED�S ACTIVE. SHOULD BE FLASHED
                                //              AT SAME RATE AS E360. FLASHING IS SOFTWARE CONTROLLED. (active high)
    DOUT_RED_LED         = 17,  // ETK_D3       SETS HIGH FOR CLINICAL ALARM REQUIRING RED LED�S ACTIVE. SHOULD BE FLASHED
                                //              AT SAME RATE AS E360. FLASHING IS SOFTWARE CONTROLLED. (active high)
    DOUT_BUZZER          = 18,  // ETK_D4       ACTIVATES DEVICE PIEZO (active low)
    DOUT_LED5            = 20,  // ETK_D6       TURNS ON DEBUG LED ON PCB6009A (active high)
    DOUT_LED6            = 21,  // ETK_D7       TURNS ON DEBUG LED ON PCB6009A (active high)
    DOUT_DIS_PB_LED      = 26,  // ETK_D12      TURN ON PUSH BUTTON LED WHEN UNIT IS TURNED ON (active high)
    DOUT_NURSECALL       = 27,  // ETK_D13      DRIVE LOW TO ACTIVATE NURSE CALL (active low)
    DOUT_KILL_PWR        = 54,  // GPMC_nCS3    SET LOW TO REMOVE POWER FROM VENT (active low)
    DOUT_ENET_CS         = 56,   // GPMC_NCS5    ETHERNET CHIP SELECT
    DOUT_EN_HEATER       = 61,  // GPMC_nBE1    TURN ON HEATER (active high)
	DOUT_FAN_ON          = 94,  // CAM_HS Active high
    DOUT_P2_SOLENOID     = 102, // P2 autozero solenoid
	DOUT_CROSSOVER_SOLENOID = 103, // CAM_D4
    DOUT_SAFETY_SOLENOID = 104, // This is for backward compatibility
    GPIO_SPI1_ON_BD_ENABLED  = 110, //
    GPIO_SPI1_ON_BD      = 111, // CAM_XCLKB    LOW WHEN INTERFACING TO ONBOARD DAC OR ADC (active low)
    DOUT_WATCH_DOG		 = 116, // McBSP2_FXS   SHOULD CHANGE STATE AT LEAST EVERY 10ms
    DOUT_WD_TIMER_SET    = 117, // McBSP2_CLKX  LOW DURING BOOTUP BY DEFAULT. SET HIGH BY APPLICATION SOFTWARE
    DOUT_MNFLD_SEL       = 136, // MMC2_DAT4    Manifold select, Active low
    GPIO_SPI1_OFF_BD     = 141, // McBSP3_DR    LOW WHEN INTERFACING TO AIR OR O2 TSI FLOW SENSORS VIA SPI1, HIGH OTHERWISE (active low)
    DOUT_P1_SOLENOID     = 170, // P1 autozero solenoid
};
// These are the application related outputs for UI REV P1
static const DigitalOutputStates_t SPI1_ENABLE					= DOUT_LOW;
static const DigitalOutputStates_t SPI1_DISABLE					= DOUT_HIGH;
static const DigitalOutputStates_t DAC_LDAC_ENABLE				= DOUT_LOW;
static const DigitalOutputStates_t DAC_LDAC_DISABLE				= DOUT_HIGH;

static const uint16_t TSI_EEPROM_TAB_SIZE = 256;

enum DigitalBiDirectional_t
{
    GPIO_17 = 17,
    GPIO_19 = 19,
	GPIO_94 = 94,
    GPIO_95 = 95,
    GPIO_96 = 96,
    GPIO_97 = 97,
	GPIO_102 = 102,
	GPIO_103 = 103,
	GPIO_104 = 104,
	GPIO_105 = 105,
	GPIO_110 = 110,
	GPIO_111 = 111,
	GPIO_141 = 141,
	GPIO_170 = 170,
};
// End application related outputs for BD REV P3

#endif

#if defined(SIGMA_GUI_CPU)
// These are the application related outputs.

enum ApplicationDigitalInputs_t
{
    DIN_INT1        =   0, // SYS_nIRQ
    DIN_ENET        =  22,  // LAN9221 PME (PWakeup Indicator)
    DIN_ENC_PHA         = 58,  // GPMC_nCS7
    DIN_ACDC_ON         = 58,  // This is BD power source pin. GUI may need it to request from BD
    DIN_LSCNSL_IRQ  =  55, // Console I2C Expander IRQ
    DIN_MOD1_DET    =  94, // Mod1 detection
    DIN_MOD2_DET    =  99, // Mod2 detection
    DIN_MOD3_DET    = 100, // Mod3 detection
    DIN_DX0         = 105, // Impedence tomography graphics input
    DIN_DY0         = 106, // Impedence tomography graphics input
    DIN_DX1         = 107, // Impedence tomography graphics input
    DIN_DY1         = 108, // Impedence tomography graphics input
    DIN_MOD4_DET    = 111  // Mod4 detection
};

enum ApplicationDigitalExtensionInputs_t
{
    ACCEPT_BUTTON_MASK	  =	0x01,
    CANCEL_BUTTON_MASK    =	0x02,
    HOME_BUTTON_MASK	  =	0x04,
    MANUAL_BUTTON_MASK	  =	0x08,
    RESERVED1_BUTTON_MASK =	0x10,
    RESERVED2_BUTTON_MASK =	0x20,
    RESET_BUTTON_MASK	  =	0x40,
    SILENCE_BUTTON_MASK	  =	0x80

};

enum ApplicationDigitalExtensionOutputs_t
{
    DOUT_MINIMUM = 8,
    DOUT_RED_ALARM_LED = DOUT_MINIMUM,
    DOUT_YELLOW_ALARM_LED,

    DOUT_SILENCE_LED,

};

enum ApplicationDigitalOutputs_t
{
    DOUT_ID2            =  10,
    DOUT_LED4           =  14,
    DOUT_DVIMC_PUP      =  16, // Main video power enable
    DOUT_DVIRC_PUP      =  17, // Remote video power enable
    DOUT_ENC_PHB        =  18, // Encoder phase B
    DOUT_LED5           =  20,
    DOUT_LED6           =  21,
//    DOUT_SPKR_ENA       =  54,
    DOUT_AUDIO_ENABLE = 54,
    DOUT_ENET_CS        =  56,
    DOUT_UI_ACK         =  57,  // This was intended to allow an "I'm alive" toggle to the GUI
    DOUT_DUMMY_840_OUT   = 57,  // Duplicates the above because it isn't currently used. This gives unsued 840
                                // Digital outputs a valid ApplicationDigitalOutputs_t type to use.
    DOUT_MOD2_MOD3_CS   = 138, // Mod2 & Mod3 CS arbitration
    DOUT_MOD1_MOD4_CS   = 139, // Mod1 & Mod4 CS arbitration
    DOUT_ENET_RST       = 157, // LAN9221 reset
    DOUT_ID3            = 162,
    DOUT_ID1            = 167,
    DOUT_OK_LED         = 170  // Processor OK LED
};

enum DigitalBiDirectional_t
{
    MOD1_GPIO2     =   2,
    MOD2_GPIO3     =   3,
    MOD3_GPIO5     =   5,
    MOD4_GPIO6     =   6,
    TP_310         =   7,
    TP_311         =   8,
    MOD4_RST       =  12, // Mod4 reset
    MOD3_GPIO_13   =  13,
    TP_401         =  15,
	BD_LED_ALERT   =  18,
    TP_308         =  59,
    MOD1_GPIO95    =  95,
    MOD1_RST       =  96, // Mod1 reset
    TP_403         =  97,
    MOD2_GPIO98    =  98,
    MOD2_RST       = 101, // Mod2 reset
    MOD2_GPIO102   = 102,
    MOD3_GPIO103   = 103,
    MOD3_RST       = 104, // Mod3 reset
    MOD4_GPIO109   = 109,
    DOUT_GPIO_110  = 110, // Not used
	BD_AUDIO_ALERT_PIN = 118,
    MOD1_GPIO126   = 126,
    MOD4_GPIO186   = 186

};


#endif

    static const BYTE INSTRUCTION_WRITE_STATUS = 0x01;
    static const BYTE INSTRUCTION_READ = 0x03;
    static const BYTE INSTRUCTION_WRITE = 0x02;
    static const BYTE INSTRUCTION_WRITE_DISABLE = 0x04;
    static const BYTE INSTRUCTION_READ_STATUS = 0x05;
    static const BYTE INSTRUCTION_WRITE_ENABLE = 0x06;

//#define USE_FILE_SYSTEM	1
class AioDioDriver_t
{
    // Only high priority operations should have direct access to the driver
    friend class AnalogInputThread_t;
    friend class ServosThread_t;
    friend class BreathThread_t;
	friend class ButtonsThread_t;
    friend class AlarmsThread_t;
    //friend class NoExternalPowerAlarm_t;
    //friend class BackupBatteryFailureAlarm_t;
    //friend class ExternalBatteryFailureAlarm_t;
    //friend class RunningOnBackupBatteryAlarm_t;
    //friend class SwitchToBackupBatteryAlarm_t;
    //friend class BatteryLevelMonitor_t;
    friend class Autozero_t;
    friend class ExhalationValveServo_t;
    friend class CryptoMemDriver_t;
	friend class DpAutozero_t;
	friend class ExhFlowSensor_t;

	public:
        AioDioDriver_t(void);
        ~AioDioDriver_t(void);

		static AioDioDriver_t& GetAioDioDriverHandle(void);

        // set up the AD7923 ADC and the digital I/O
        void            InitAioDio(void);
		void			DeinitAioDio();

        bool            AioDioInitComplete(void)
        {
            return AioDioInitDone;
        }

		bool			IsServiceRequest() { return bCalibrationRequest; }

        void            SetVolume(uint8_t VolumeSetting);
		bool		    GetEbootVersion(NMI_VERSION *pEbootVersion);
		bool			GetOSVersion(NMI_VERSION *pOSVersion);

        // Read the specified digital input
        uint8_t         Read(ApplicationDigitalInputs_t DigitalInput);
        // Read, write and set direction of bi-directional GPIOs
        uint8_t         Read(DigitalBiDirectional_t DigitalInput);
		void            Write(DigitalBiDirectional_t DigitalOutput, DigitalOutputStates_t Value);
		void            SetDirection(DigitalBiDirectional_t DigitalInputOutput, GpioDirection_t Direction);
        void            SetBackLight(BYTE DimLevel);
        void            Write(ApplicationDigitalOutputs_t DigitalOutput, DigitalOutputStates_t Value);
        bool            WriteSpiEprom(BYTE SendValue[], BYTE Lenght, WORD StartAddress);
        bool            ReadSpiEprom(uint8_t *SendValue, BYTE Lenght, WORD StartAddress);
//+++protected:
//		bool			ReadPowerOnButtons(bool *ButtonPressedArray,DWORD *ButtonDataByte);

        // Read the specified A/D signal.  Returns true
        // if everything when ok. False if it fails. Failure would be if the requested
        // address doesn't match what was set as the "NextAnalogInput" on the previous call.
        bool            Read(AnalogInputs_t AnalogInput, uint16_t *Reading);
		bool			Read(AnalogInputs_t FirstAnalogInput, AnalogInputs_t LastAnalogInput, uint32_t *Reading, uint32_t BufSize);

#if defined(SIGMA_GUI_CPU)  && !defined(SIGMA_GUIPC_CPU)
		BYTE AioDioDriver_t::ReadMembraneButtons(void);
		IOExpanderXRA1201_t& GetIOExpanderXRA1201Handle(void);
#endif

#ifdef SIGMA_BD_CPU
        // Write to the DACs.
        // The first version assumes the motor DAC is written first, followed by the Exhl valve
        void            Write(AnalogOutputs_t AnalogOutput, DacCounts Value);
		bool			ReadTsiTable(TsiFlowType_t FlowType, uint8_t *InBuffer);
#endif

        bool			ReadKnob(uint8_t& KnobCount, uint8_t& KnobDir);
//        bool            SendReceiveGuiSpi(BYTE SpiNumber, BYTE Len, BYTE SendValue[], BYTE ReceiveValue[]);
bool WriteSpiToUART(BYTE* WriteValue, BYTE *Lenght);
bool ReadSpiFromUART(BYTE* ReaddValue, BYTE *Lenght);
bool SendSpiToUART(WORD *SendValue, WORD *ReceivedValue);
BYTE GetUART_FIFO(void);

void InitSPI_UART(void);

protected:
		bool            AioDioInitDone;
		bool			bCalibrationRequest;
		HANDLE			hAioDioDriver;
		uint32_t		BSPType;
		CRITICAL_SECTION AioDioCriticalSection;     // make sure the digital I/O don't get stepped on by threads

		HANDLE			hSPI1_CH3;
		HANDLE			hActiveSPI;
		HANDLE			hSPI3_CH0;
		HANDLE			hSPI3_CH1;

#if defined(SIGMA_GUI_CPU)  && !defined(SIGMA_GUIPC_CPU)
		BackLight_t          *PtrBackLight;
		IOExpanderXRA1201_t  *PtrIOExpanderXRA1201Gui;
		HANDLE      hI2c3File;
		//End
#endif

#ifdef SIGMA_BD_CPU
        HANDLE	hSPI1_CH0;
		HANDLE	hSPI4_CH0;
#endif
};


#endif

