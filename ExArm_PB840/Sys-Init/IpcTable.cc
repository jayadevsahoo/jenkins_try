#include "stdafx.h"
//=====================================================================
//   This is a proprietary work to which Puritan-Bennett corporation
//   of California claims exclusive right.  No part of this work may
//  be used, disclosed, reproduced, sorted in an information retrieval
//     system, or transmitted by any means, electronic, mechanical,
//   photocopying, recording, or otherwise without the prior written
//      permission of Puritan-Bennett Corporation of California.
//
//          Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: IpcTable - Inter-Process Communications Table.
//---------------------------------------------------------------------
//@ Interface-Description
//  The IpcTable class contains information for initialization of VRTX
//  interprocess communication structures.  It provides access to an
//  IpcTableEntry by IpcId or index.  Each IpcTableEntry contains its
//  enumerated ID (defined in IpcIds.hh), IPC type, length and name.
//  MsgQueue, MailBox, and MutEx initialization methods use IpcTable
//  to retrieve this information.
//---------------------------------------------------------------------
//@ Rationale
//  IpcTable is the repository for interprocess communication 
//  initialization information.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The inter-process communication configuration is defined in the 
//  private Table_ defined in IpcTableDef.cc.  Initialize sets the 
//  number of entries found in the table.  Accessor methods return an
//  IpcTableEntry reference retrieved by IpcId (defined in IpcIds.hh)
//  or table index.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Sys-Init/vcssrc/IpcTable.ccv   25.0.4.0   19 Nov 2013 14:33:54   pvcs  $
//
//@ Modification-Log
//
//   Revision 002   By:   sah      Date: 01/13/99    DR Number: 5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to a non-inlined method.
//
//   Revision 001   By:   gdc      Date: 07/06/95    DR Number:   n/a  
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//=====================================================================

//=====================================================================
//
//      Includes...
//
//=====================================================================

#include "IpcTable.hh"
#include "IpcIds.hh"

#ifdef SIGMA_DEVELOPMENT
#  include <stdio.h>
#endif
#ifdef SIGMA_DEVELOPMENT
#  include "Ostream.hh"
#endif

//@ Usage-Classes
#include "MailBox.hh"
#include "MsgQueue.hh"
#include "MutEx.hh"
//@ End-Usage

//=====================================================================
//
//      Static Data...
//
//=====================================================================

//  IpcTable::Table_ is defined in IpcTableDef.cc

static const Int MAX_IPC_ID = 100;

Boolean              IpcTable::IsInitialized_ = FALSE;
const IpcTableEntry *IpcTable::PTableById_[MAX_IPC_ID+1];


//@ Code...

//=====================================================================
//
//      Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Initialize
//
//@ Interface-Description
//  Sets the numbers of entries found in the private table.  Initializes
//  the table to retrieve entries by id.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Scans the table for the first null entry.  Sets the number of
//  entries.  Initializes PTableById_ entries by indexing into the 
//  table by id and storing the IPC's reference there.
//---------------------------------------------------------------------
//@ PreCondition
//              none
//---------------------------------------------------------------------
//@ PostCondition
//              none
//@ End-Method
//=====================================================================
void
IpcTable::Initialize(void)
{
    CALL_TRACE("IpcTable::Initialize(void)");

    for (Uint i=0; i < IpcTable::NumEntries_; i++)
    {
        const IpcTableEntry & ipcEntry = IpcTable::GetIpcTableEntry(i);
        AUX_CLASS_ASSERTION(  0 < ipcEntry.id && ipcEntry.id <= MAX_IPC_ID
                            , ipcEntry.id);
        PTableById_[ipcEntry.id] = &ipcEntry;
    }

    IpcTable::IsInitialized_ = TRUE;
    // $[TI1]
}

 
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetIpcTableEntry
//
//@ Interface-Description
//  Returns the reference to IpcTableEntry corresponding at index
//  specified.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (0 <= index && index < IpcTable::NumEntries_)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
const IpcTableEntry &
IpcTable::GetIpcTableEntry(const Uint index)
{
    CALL_TRACE("GetIpcTable(void)");
 
    CLASS_PRE_CONDITION(0 <= index && index < IpcTable::NumEntries_);
    return IpcTable::Table_[index];
    // $[TI1]
}
 
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetEntryById(const Int32 id)
//
//@ Interface-Description
//  Returns the IpcTableEntry that matches the id argument.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  ( 0 < id && id <= MAX_IPC_ID )
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
const IpcTableEntry &
IpcTable::GetEntryById(const Int32 id)
{
    CALL_TRACE("GetEntryById(Int32)");
 
    AUX_CLASS_PRE_CONDITION( 0 < id && id <= MAX_IPC_ID, id );
    AUX_CLASS_ASSERTION( PTableById_[id] != NULL, id );
 
    return *PTableById_[id];
    // $[TI1]
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetNumEntries(coid)
//
//@ Interface-Description
//  Returns the number of entries in the IpcTable.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
Int32
IpcTable::GetNumEntries(void)
{
    CALL_TRACE("GetNumEntries(void)");
    return IpcTable::NumEntries_;
    // $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: IsInitialized
//
//@ Interface-Description
//  Returns state of IpcTable initialization.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns value of private static data IsInitialized_.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
Boolean
IpcTable::IsInitialized(void)
{
    CALL_TRACE("IsInitialized(void)");
 
    return IpcTable::IsInitialized_;
    // $[TI1]
}
#ifdef SIGMA_DEVELOPMENT

//============================ M E T H O D   D E S C R I P T I O N ====
// Method: Print()
//
// Interface-Description
//  Prints out information about all the queues that were created from
//  the file IpcTable.cc
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================
void 
IpcTable::Print(void)
{
    cout << "IPC Table:" << endl;
    for (int i = 0; i < IpcTable::NumEntries_; i++)
    {
        const IpcTableEntry & entry = IpcTable::GetIpcTableEntry(i);
        cout << entry.name 
             << "(" << entry.id << ")," 
             << entry.length << endl;
    }
    printf("\n");
}

#endif // SIGMA_DEVELOPMENT
 

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//@ Interface-Description
//   This method is called when a software fault is detected by the
//   fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//   'lineNumber' are essential pieces of information.  The 'pFileName'
//   and 'pPredicate' strings may be defaulted in the macro to reduce
//   code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This method receives the call for the SoftFault, adds it sub-system
//   and class name ID and sends the message to the non-member function
//   SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
 
void
IpcTable::SoftFault(const SoftFaultID  softFaultID,
                    const Uint32       lineNumber,
                    const char*        pFileName,
                    const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
 
  FaultHandler::SoftFault(softFaultID, SYS_INIT,
                          Sys_Init::ID_IPCTABLE, 
                          lineNumber, pFileName, pPredicate);
}


//=====================================================================
//
//      Protected Methods...
//
//=====================================================================

//=====================================================================
//
//      Private Methods...
//
//=====================================================================



