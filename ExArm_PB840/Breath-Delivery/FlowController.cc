#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================


// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: FlowController - Implements a flow controller that is using a 
// 		PSOL as a valve.
//---------------------------------------------------------------------
//@ Interface-Description
//		Methods are implemented to update the PSOL such that the desired
//		flow is achieved, to initialize the controller, to set the controller
//		shutdown flags, and to get the input into the controller.
//---------------------------------------------------------------------
//@ Rationale
//		This class implements the algorithms to deliver flow to the
//		patient. 
//---------------------------------------------------------------------
//@ Implementation-Description
//		This controller is invoked every BD cycle if active to deliver the
//		desired flow to the patient.  The controller is a feedforward
//		plus integral type of controller.  On the very first cycle of
//		each breath, the integrator is disabled.
//---------------------------------------------------------------------
//@ Fault-Handling
//		N/A
//---------------------------------------------------------------------
//@ Restrictions
//		none
//---------------------------------------------------------------------
//@ Invariants
//		none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/FlowController.ccv   25.0.4.0   19 Nov 2013 13:59:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 010  By: jja    Date: 23-Dec-2000   DR Number: 5832
//  Project:  Baseline
//  Description:
//      Add code to use Psol Lookup tables only for Neo patients
//	Add upper limit on ki_
//	Cast PsolCalibration_.lookupPsolCommand values to Real32
//
//  Revision: 009  By: syw     Date:  09-Mar-2000    DR Number: 5684
//  Project:  NeoMode
//  Description:
//		Added PsolSide to constructor arguments.
//		Use computeFeedForward_() method to determine ff component of controller.
//		Added Boolean argument to updatePsol() and computeKi_().
//		Define DeterminePsolLookupFlags().
//
//  Revision: 008  By: yyy     Date:  29-Apr-1999    DR Number: 5322
//  Project:  ATC
//  Description:
//      Generate FlowControllerIntegralGainExpTabl
//
//  Revision: 007  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 006 By: syw    Date: 21-Aug-1996   DR Number: DCS 600
//  	Project:  Sigma (R8027)
//		Description:
//			Added psolStuckEventOccurred_ code to avoid multiple declarations.
//
//  Revision: 005 By: syw    Date: 16-Jul-1996   DR Number: DCS 1076
//  	Project:  Sigma (R8027)
//		Description:
//			Added controllerShutdown_ to shutdown controller when desired flow
//			is zero.  Eliminate integrator initialization each newBreath().
//			Remove startupGain_.
//
//  Revision: 004 By: syw    Date: 26-Feb-1996   DR Number: DCS 600
//  	Project:  Sigma (R8027)
//		Description:
//			Implemented SafetyNetPsolStuckOpen test.
//
//  Revision: 003 By: syw    Date: 26-Feb-1996   DR Number: DCS 600
//  	Project:  Sigma (R8027)
//		Description:
//			Controls Rev 04 changes:
//			- removed isControllerActive related code since controller will
//			  explicitly command zero psol command if flow <= ZERO FLOW.
//			- closeLevel is no longer needed.
//
//  Revision: 002 By: syw    Date: 21-Feb-1996   DR Number: DCS 674
//  	Project:  Sigma (R8027)
//		Description:
//			Removed #ifndef around #include.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "FlowController.hh"

#include "MainSensorRefs.hh"

//@ Usage-Classes

#include <math.h>
#include "Psol.hh"
#include "MathUtilities.hh"
#include "FlowSensor.hh"
#include "BD_IO_Devices.hh"
#include "NovRamManager.hh"
#include "PhasedInContextHandle.hh"
#include "BdDiscreteValues.hh"

//@ End-Usage

const Real32 INTEGRAL_GAIN = 0.310F ;		 // in counts/lpm-msec
const Real32 MAX_INTEGRAL_GAIN = 1.0;
const Real32 ZERO_FLOW = 0.001F ;
const Real32 INITIAL_DESIRED_FLOW = 0.1;
const Real32 STEP_VALUE = 0.1;
const Int32 MAX_GAIN_TABLE_SIZE = 230;

static const Real32 A_FACTOR = 51.271329;
static const Real32 B_FACTOR = 44.080624;
static const Real32 C_FACTOR = 1.5812379;
static const Real32 D_FACTOR = -0.55841703;

Real32 FlowController::aFactor_ = A_FACTOR;
Real32 FlowController::bFactor_ = B_FACTOR;
Real32 FlowController::cFactor_ = C_FACTOR;
Real32 FlowController::dFactor_ = D_FACTOR;
Boolean FlowController::factorChanged_ = FALSE;

//@ Code...

PsolLiftoff FlowController::PsolCalibration_ ;

#if defined SIGMA_UNIT_TEST
	extern Real32 FlowControllerIntegralGainExpTable[MAX_GAIN_TABLE_SIZE];
	Real32 FlowControllerIntegralGainExpTable[MAX_GAIN_TABLE_SIZE];
#else
	static Real32 FlowControllerIntegralGainExpTable[MAX_GAIN_TABLE_SIZE];
#endif

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: FlowController()
//
//@ Interface-Description
//      Constructor.  A Psol&, a Flowsensor& and PsolLiftoff::PsolSide
//		are passed as arguments for initialization.  This method returns nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//	 	The data members are initialized with the arguments passed in and
//		other data members are also initialized.  A private lookup table
//		is generated to substitute integral gain calculation.
//		Since the integral gain calculation involves both power and log
//		functions which are very expensive timingwise, a lookup table is
//		generated.  A finite size of table is chosen based on the range
//		requirement for the integral gain values which are bounded within
//		0.310 (INTEGRAL_GAIN) and 1.0 (MAX_INTEGRAL_GAIN).
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//	 	none
//@ End-Method
//=====================================================================
FlowController::FlowController( Psol& psol,
                                const FlowSensor& flowSensor,
                                const PsolLiftoff::PsolSide side)
: rPsol_(psol),   				// $[TI1.1]
  rFlowSensor_(flowSensor),   	// $[TI1.2]
  psolSide_( side)
{
	CALL_TRACE("FlowController::FlowController( Psol& psol, \
				const FlowSensor& flowSensor, \
				const PsolLiftoff::PsolSide side)") ;

	NovRamManager::GetPsolLiftoffCurrents( PsolCalibration_);
	
  	integrator_ = 0.0F ;
  	controllerShutdown_ = FALSE ;
	liftoff_ = rPsol_.getLiftoff() ;

	static Boolean tableInitialized = FALSE;

	if (!tableInitialized)
	{							// $[TI1.1]
		tableInitialized = TRUE;
		calculateGainTable();
	}							// $[TI1.2]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~FlowController()
//
//@ Interface-Description
//      Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//      none
//---------------------------------------------------------------------
//@ PreCondition
// 		none
//---------------------------------------------------------------------
//@ PostCondition
// 		none
//@ End-Method
//=====================================================================
FlowController::~FlowController( void)
{
	CALL_TRACE("FlowController::~FlowController( void)") ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: calculateGainTable()
//
//@ Interface-Description
//      No parameters, no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//      Calculates the gain table. Uses the a, b, c, d factors. The
//      gain table is calculated once per change but impacts the Insp
//      flow controllers.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
FlowController::calculateGainTable()
{
	Boolean expandTableSize = FALSE;
		
	FlowControllerIntegralGainExpTable[0] = INTEGRAL_GAIN;
	Real32 desiredFlow = INITIAL_DESIRED_FLOW;
		
	for (Int32 index = 1; index < MAX_GAIN_TABLE_SIZE; index++)
	{
		//	$[TC04036] algorithm for adjust the integral gain.
		Real32 k1 = - cFactor_ * (Real32) (pow(desiredFlow, dFactor_));
		Real32 k2 = aFactor_ - bFactor_ * (Real32) (exp(k1));
		Real32 ki = k2 * INTEGRAL_GAIN / 18.0F;
	
		desiredFlow += STEP_VALUE;
	
		if (ki < INTEGRAL_GAIN)
		{						// $[TI1.1.1]
			expandTableSize = TRUE;
			ki = INTEGRAL_GAIN;
		}
		else if (ki > MAX_INTEGRAL_GAIN)
		{						// $[TI1.1.2]
			ki = MAX_INTEGRAL_GAIN;
		}						// $[TI1.1.3]

		FlowControllerIntegralGainExpTable[index] = ki;
	}

	CLASS_ASSERTION(expandTableSize);

#if defined SIGMA_UNIT_TEST
	for (index = 1; index < MAX_GAIN_TABLE_SIZE; index+=10)
	{
		printf("%.3f, %.3f, %.3f, %.3f, %.3f, %.3f, %.3f, %.3f, %.3f, %.3f\n",
				FlowControllerIntegralGainExpTable[index],
				FlowControllerIntegralGainExpTable[index+1],
				FlowControllerIntegralGainExpTable[index+2],
				FlowControllerIntegralGainExpTable[index+3],
				FlowControllerIntegralGainExpTable[index+4],
				FlowControllerIntegralGainExpTable[index+5],
				FlowControllerIntegralGainExpTable[index+6],
				FlowControllerIntegralGainExpTable[index+7],
				FlowControllerIntegralGainExpTable[index+8],
				FlowControllerIntegralGainExpTable[index+9]);
	}		
#endif
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newBreath()
//
//@ Interface-Description
//      This method has no arguments and has no return value.  This method is
//      called at the beginning of every breath to initialize the flow
//      controller.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The data members are initialized.
// 		$[04184]
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
void
FlowController::newBreath( void) 
{   
	CALL_TRACE("FlowController::newBreath( void)") ;
	
   	// $[TI1]
  	ki_ = INTEGRAL_GAIN ;
	prevGain_ = INTEGRAL_GAIN ;

	kff_ = 12.0F ;

#if CONTROLS_TUNING	
	if( factorChanged_ == TRUE )
	{
		calculateGainTable();
		factorChanged_ = FALSE;
	}
#endif
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updatePsol()
//
//@ Interface-Description
//      This method has desired flow and usePsolLookup flag as arguments
//		and has no return value.  This method is called every BD cycle to
//		control the PSOL so the	desiredFlow is delivered out of the PSOL.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The controllerShutdown_ flag is used to command the psol to zero command
//		when the desired flow is zero.  Otherwise, the contoller is active and
//		the psol command is given by
//			psolCommand = Kff_ * desiredFlow + ki_ * integrator_
//					  + liftoff_
//		Anti-windup algorithms are implemented if the psol command exceeds
//		its limits.
// 		$[04065] $[04067] $[04068] $[04279] $[04302] 
//---------------------------------------------------------------------
//@ PreCondition
//      ki_ > 0.0
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
FlowController::updatePsol( const Real32 desiredFlow, const Boolean usePsolLookup) 
{
	CALL_TRACE("FlowController::updatePsol( const Real32 desiredFlow, \
											const Boolean usePsolLookup)") ;
	
	CLASS_PRE_CONDITION( ki_ > 0.0F) ;

	Int32 psolCommand ;
	
	desiredFlow_ = desiredFlow ;	// store command input

	if (desiredFlow_ < ZERO_FLOW && controllerShutdown_)
	{
	   	// $[TI1.1]
		//	$[04302] whenever desiredFlow = 0 during exhalation ...
		psolCommand = 0 ;
		integrator_ = 0.0F ; 
	}
	else
	{
	   	// $[TI1.2]
	   	computeKi_( usePsolLookup);
	   	integrator_ += (desiredFlow_ - rFlowSensor_.getValue()) * 
		            		CYCLE_TIME_MS ;

		Real32 ff = computeFeedForward_( usePsolLookup) ;
		
		psolCommand = (Int32)(ki_ * integrator_	+ ff) ;
		if (psolCommand > MAX_COUNT_VALUE)
  		{
		   	// $[TI2.1]
			integrator_ = (MAX_COUNT_VALUE - ff)
							/ ki_ ;
	    	psolCommand = MAX_COUNT_VALUE ;
		}
  		else if (psolCommand < 0)
	  	{
		   	// $[TI2.2]
    		integrator_ = -ff / ki_ ;
		    psolCommand = 0 ;
		}	// implied else	$[TI2.3]
	}

  	rPsol_.updatePsol( (Int16)psolCommand) ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: DeterminePsolLookupFlags()
//
//@ Interface-Description
//      This method has air and o2 flows and the corresponding psol lookup
//		flag references as arguments.  This method returns nothing.  This method is
//		called to determine if the psol lookup algorithm should be invoked.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The actual mix is computed based on the air and o2 flows passed in.
//		If 21 < actual mix <= 30.0, useO2Lookup = TRUE else
//		If 90.0 <=  actual mix < 100.0, useAirLookup = TRUE else
//		useO2Lookup = FALSE, useAirLookup = FALSE
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
FlowController::DeterminePsolLookupFlags( const Real32 airFlow, const Real32 o2Flow,
										  Boolean& useAirLookup, Boolean& useO2Lookup)
{
	CALL_TRACE("FlowController::DeterminePsolLookupFlags( const Real32 airFlow, \
						const Real32 o2Flow, Boolean& useAirLookup, Boolean& useO2Lookup)") ;

	const DiscreteValue  CIRCUIT_TYPE =
		PhasedInContextHandle::GetDiscreteValue(SettingId::PATIENT_CCT_TYPE);

	// $[NC24001]
	if ( (CIRCUIT_TYPE == PatientCctTypeValue::NEONATAL_CIRCUIT) && (airFlow + o2Flow > 0.0) )
	{
	 	Real32 actualMix = (airFlow * 21 + 100 * o2Flow) / (airFlow + o2Flow) ;
						
		useAirLookup = (airFlow >= 0.0 && airFlow <= MAX_PSOL_LOOKUP_FLOW &&
				actualMix >= 90.0 && !IsEquivalent( actualMix, 100.0, HUNDREDTHS)) ;
		// $[TI1] TRUE
		// $[TI2] FALSE

		useO2Lookup = (o2Flow >= 0.0 && o2Flow <= MAX_PSOL_LOOKUP_FLOW &&
				actualMix <= 30.0 && !IsEquivalent( actualMix, 21.0, HUNDREDTHS)) ;
		// $[TI3] TRUE
		// $[TI4] FALSE
	}
	else
	{
		// $[TI5]
		useAirLookup = FALSE ;
		useO2Lookup = FALSE ;
	}
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]  
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//      'lineNumber' are essential pieces of information.  The 'pFileName'
//      and 'pPredicate' strings may be defaulted in the macro to reduce
//      code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition      
//      none
//@ End-Method
//=====================================================================
 
void
FlowController::SoftFault( const SoftFaultID  softFaultID,
                   		   const Uint32       lineNumber,
	                       const char*        pFileName,
	                       const char*        pPredicate)
{
	CALL_TRACE("SoftFault( softFaultID, lineNumber, pFileName, pPredicate)") ;
	FaultHandler::SoftFault( softFaultID, BREATH_DELIVERY, FLOWCONTROLLER,
							 lineNumber, pFileName, pPredicate) ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setAFactor()
//
//@ Interface-Description
//      Takes a floating point input.
//---------------------------------------------------------------------
//@ Implementation-Description
//      Sets the factor, and indicates that the gains table needs
//      to be recalculated.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
FlowController::setAFactor( Real32 factor)
{
    aFactor_ = factor;
#if CONTROLS_TUNING
    factorChanged_ = TRUE;
#endif
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getAFactor()
//
//@ Interface-Description
//      No parameters, returns a floating point number
//---------------------------------------------------------------------
//@ Implementation-Description
//      Accessor method.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Real32
FlowController::getAFactor()
{
    return (aFactor_);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setBFactor()
//
//@ Interface-Description
//      Takes a floating point input.
//---------------------------------------------------------------------
//@ Implementation-Description
//      Sets the factor, and indicates that the gains table needs
//      to be recalculated.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
FlowController::setBFactor( Real32 factor)
{
    bFactor_ = factor;
#if CONTROLS_TUNING
    factorChanged_ = TRUE;
#endif
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getBFactor()
//
//@ Interface-Description
//      No parameters, returns a floating point number
//---------------------------------------------------------------------
//@ Implementation-Description
//      Accessor method.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Real32
FlowController::getBFactor()
{
    return (bFactor_);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setCFactor()
//
//@ Interface-Description
//      Takes a floating point input.
//---------------------------------------------------------------------
//@ Implementation-Description
//      Sets the factor, and indicates that the gains table needs
//      to be recalculated.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
FlowController::setCFactor( Real32 factor)
{
    cFactor_ = factor;
#if CONTROLS_TUNING
    factorChanged_ = TRUE;
#endif
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getCFactor()
//
//@ Interface-Description
//      No parameters, returns a floating point number
//---------------------------------------------------------------------
//@ Implementation-Description
//      Accessor method.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Real32
FlowController::getCFactor()
{
    return (cFactor_);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setDFactor()
//
//@ Interface-Description
//      Takes a floating point input.
//---------------------------------------------------------------------
//@ Implementation-Description
//      Sets the factor, and indicates that the gains table needs
//      to be recalculated.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
FlowController::setDFactor( Real32 factor)
{
    dFactor_ = factor;
#if CONTROLS_TUNING
    factorChanged_ = TRUE;
#endif
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getDFactor()
//
//@ Interface-Description
//      No parameters, returns a floating point number
//---------------------------------------------------------------------
//@ Implementation-Description
//      Accessor method.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Real32
FlowController::getDFactor()
{
    return (dFactor_);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updatePsolCalibration()
//
//@ Interface-Description
//      No parameters, returns nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//      called to update PSOL calibration when new calibration is
//		available (example - after SST in Normal Mode)
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
FlowController::updatePsolCalibration(void)
{
	NovRamManager::GetPsolLiftoffCurrents(PsolCalibration_);
}

//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//=====================================================================
//
//  Private Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: computeKi_()
//
//@ Interface-Description
//      This method has a Boolean as an argument and has no return value.
//		This method is called by updatePsol() to get the integral gain value.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Based on the desiredFlow_, calculate the index into the
//		integral gain table to lookup the ki_ value.  The integrator_
//		and prevGain_ are calculated based on ki_.  The integral gain
//		is overridden if usePsolLookup is TRUE and
//		0 <= desiredFlow <= MAX_PSOL_LOOKUP_FLOW.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
void
FlowController::computeKi_( const Boolean usePsolLookup) 
{   
	CALL_TRACE("FlowController::computeKi_( const Boolean usePsolLookup)") ;

	if (usePsolLookup)
	{
		// $[TI2]
		const Real32 GAIN_RATIO = 0.01 ;	// use 1% of psol gain for integral gain
	
		if (desiredFlow_ > 2 * FLOW_INCREMENT)
		{
			// $[TI3]
			ki_ = ((Real32)PsolCalibration_.lookupPsolCommand( desiredFlow_, psolSide_)
				- (Real32)PsolCalibration_.lookupPsolCommand( desiredFlow_ - FLOW_INCREMENT, psolSide_))
					/ FLOW_INCREMENT * GAIN_RATIO ;
		}
		else
		{
			// $[TI4]
			ki_ = ((Real32)PsolCalibration_.lookupPsolCommand( 2 * FLOW_INCREMENT, psolSide_)
				- (Real32)PsolCalibration_.lookupPsolCommand( FLOW_INCREMENT, psolSide_))
					/ FLOW_INCREMENT * GAIN_RATIO ;
		}

		// [$NC24002]
		if (ki_ < INTEGRAL_GAIN)
		{
			// $[TI5]
			ki_ = INTEGRAL_GAIN ;
		}
		if (ki_ > MAX_INTEGRAL_GAIN)
		{
			// $[TI8]
			ki_ = MAX_INTEGRAL_GAIN ;
		}
		// $[TI6]
	}
	else
	{
		// $[TI7]
		if (desiredFlow_ >= 0.0)
		{				  	// $[TI1.1]
			Int32 index = (Int32)(desiredFlow_ / STEP_VALUE + 0.5F);

			if (index >= MAX_GAIN_TABLE_SIZE)
			{ 		  		// $[TI1.1.1]
				index = MAX_GAIN_TABLE_SIZE - 1;
			}			   	// $[TI1.1.2]
			
			ki_ = FlowControllerIntegralGainExpTable[index];
		}
		else
		{				   	// $[TI1.2]
			ki_ = INTEGRAL_GAIN;
		}
	}

	//	$[TC04037] adjust the intergal gain every time it change
	integrator_ *= prevGain_ / ki_;
	prevGain_ = ki_;
}

