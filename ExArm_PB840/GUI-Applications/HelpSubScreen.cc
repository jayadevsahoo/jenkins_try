#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: HelpSubScreen - A subscreen for displaying help information,
// activated by pressing the ? offscreen key.
//---------------------------------------------------------------------
//@ Interface-Description
// HelpSubScreen displays a help system consisting of nested levels of help
// menus leading to pages of help text.  This class is designed
// to be "data-driven", i.e. the code is written so that the content of
// the help menus and help text can be specified external to this class.
// HelpSubScreen is constructed with a parameter which points to a structure
// containing information for the highest level help menu.  This structure
// in turn points to sub-structures of other menus and help text.  The
// content of the structures is user-defined.  All HelpSubScreen needs is
// a pointer to the top-level menu to begin the work of displaying the
// help system.  The description for the menu structures can be found in
// the file HelpStrs.hh.
//
// Each menu entry has a button allowing the user to select that entry.
// Selecting the button will bring the user either directly to the help
// text for that option or to a sub-menu for that option.  Once the user
// has navigated off the main help menu a "go back" button appears at
// the top right of the subscreen.  This button allows the user to go
// back to the previously displayed menu.  Forward and backward paging is
// done with the arrow buttons next to the "go back" button.  Either or both of 
// these butttons are displayed if there is remaining text in that direction.
//
// Besides the normal subscreen title displayed at the top left of the
// subscreen, there is also an underlined help title which is centered above
// each menu and help text area, identifying the particular item being viewed.
//
// The activate()/deactivate() methods are called automatically on
// display/removal of this subscreen by our subscreen area owner.  The main
// menu is always displayed on activation (as opposed to displaying the menu or
// help text that was visible when this subscreen was last visible).
// buttonDownHappened() is called when the user presses the "go back" button,
// or any of the paging buttons.
// For interacting with the ScrollableLog class that's used for menu display
// the getLogEntryColumn() is called when the ScrollableLog needs to update
// the contents of one of its cells.  ScrollableLog calls
// currentLogEntryChangeHappened() when an entry in a menu is selected.
//---------------------------------------------------------------------
//@ Rationale
// Implements a generic handler that can handle help systems for normal
// ventilation mode as well as service mode.
//---------------------------------------------------------------------
//@ Implementation-Description
// A ScrollableLog object is used for displaying all menus.  A TextArea
// object is used for displaying help text.  An array of pointers to
// HelpMenu structures helps keep track of the stack of menus created as
// the user navigates forward and backward through the menu hierarchy.
//
// Most of the action occurs in the buttonDownHappened() and
// currentLogEntryChangeHappened() methods.  If called when the "go back" button
// is selected buttonDownHappened() is responsible for displaying the
// previously displayed menu (and removing the help text area if necessary).
// If called when the paging button(s) is selected, buttonDownHappened() is
// responsible for displaying the next/previous page of text whichever is appropriate.
// When a menu option is selected currentLogEntryChangeHappened() decides
// whether the user has selected a submenu or a help text option.  If a
// submenu was chosen then the current menu items are replaced with the
// new menu items.  If a help text option was chosen then the current
// menu is removed and the TextArea object containing the help text is
// displayed.
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and pre-conditions
// to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
// Currently only 10 levels of menu nesting is supported.  This can be
// increased in the unlikely event that more is needed.  The toplevel of
// the menu system can only be specified at construction time.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/HelpSubScreen.ccv   25.0.4.0   19 Nov 2013 14:07:54   pvcs  $
//
//@ Modification-Log
//
//  Revision: 010   By: hhd    Date:  16-Nov-1999    DCS Number: 5327
//  Project: Neo-Mode
//  Description:
// 		Modified as part of a general cleanup effort for Neo-Mode.
//
//  Revision: 009   By: gdc    Date:  29-Jun-1999    DCS Number: 5327
//  Project: 840 Neonatal
//  Description:
//	Optimized graphics library. Implemented Direct Draw mode for drawing
//  outside of BaseContainer refresh cycle.
//
//  Revision: 008  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 007  By:  gdc	   Date:  15-JUN-1998  DR Number: 5102 
//       Project:  ColorScreens (R8027)
//       Description:
//		 Updated breath timing diagram to new format.
//
//  Revision: 006  By:  hhd	   Date:  25-NOV-97    DR Number: 2265 
//       Project:  Sigma (R8027)
//       Description:
//		 Changed y-position for Help text's title.
//
//  Revision: 005  By:  yyy    Date:  30-OCT-97    DR Number: 2591
//       Project:  Sigma (R8027)
//       Description:
//             When Diagram button was pressed and we are in top level of
//			   help, the go back button will be displayed rather than
//			   disabled.
//
//  Revision: 004  By:  hhd    Date:  30-OCT-97    DR Number: 2597
//       Project:  Sigma (R8027)
//       Description:
//       		Repositioned the DIAGRAM and PREVIOUS buttons.
//     
//  Revision: 003  By:  clw    Date:  23-OCT-97    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Rework resulting from peer review.  Added 2 new constants
//             LOWER_SCREEN_DIAGRAM_LABEL_X_ and UPPER_SCREEN_DIAGRAM_LABEL_X_.
//
//  Revision: 002  By:  clw    Date:  22-OCT-97    DR Number: 2265 
//       Project:  Sigma (R8027)
//       Description:
//             The definition of the pHelpTextTitle member of the HelpText struct
//             (declared in HeplStrs.hh) has changed - it is now a pointer to a
//             StringId instead of a StringId.  This was done to get around a
//             SUN compiler limitation.  This file required two modifications to
//             accomodate this change.
//
//  Revision: 001  By:  wks    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "Sigma.hh"
#include "HelpSubScreen.hh"
#include "MiscStrs.hh"

//@ Usage-Classes
#include "Bitmap.hh"
#include "Image.hh"
#include "ScrollableLog.hh"
//@ End-Usage

//@ Code...

// Initialize static constants

static const Int MAIN_SETTING_AREA_X_ = 217;
static const Int MAIN_SETTING_AREA_Y_ = 160;
static const Int MAIN_SETTING_AREA_WIDTH_ = 198;
static const Int MAIN_SETTING_AREA_HEIGHT_ = 35;

static const Int LOWER_SUB_SCREEN_AREA_Y_ = 195;
static const Int LOWER_SUB_SCREEN_AREA_HEIGHT_ = 55;
static const Int LOWER_SCREEN_SELECT_AREA_Y_ = 246;
static const Int LOWER_SCREEN_SELECT_AREA_WIDTH_ = 114;	
static const Int LOWER_SCREEN_SELECT_AREA_HEIGHT_ = 12;
static const Int MESSAGE_AREA_Y_ = 258;
static const Int MESSAGE_AREA_HEIGHT_ = 12;
static const Int PROMPT_AREA_X_ = 331;
static const Int PROMPT_AREA_WIDTH_ = 84;
static const Int PROMPT_AREA_HEIGHT_ = 24;
static const Int VITAL_PATIENT_DATA_AREA_HEIGHT_ = 20;
static const Int UPPER_SUB_SCREEN_AREA_Y_ = 81;
static const Int UPPER_SUB_SCREEN_AREA_HEIGHT_ = 55;
static const Int UPPER_SCREEN_SELECT_AREA_Y_ = 136;
static const Int UPPER_SCREEN_SETTING_AREA_X_ = 217;
static const Int UPPER_SCREEN_SETTING_AREA_Y_ = 41;
static const Int LOWER_SCREEN_DIAGRAM_LABEL_X_ = 82;
static const Int UPPER_SCREEN_DIAGRAM_LABEL_X_ = 82;

// Breath Timing Diagram specification
static const Int BOX_HEIGHT_ = 20;
static const Int TICK_MARK_HEIGHT_ = 2;
static const Int TEXT_OFFSET_Y_ = 4;

static const Int XAXIS_WIDTH_ = 360;
static const Int XAXIS_HEIGHT_ = 6;
static const Int XAXIS_X_ = 320 - XAXIS_WIDTH_ / 2;
static const Int XAXIS_Y_ = 140;

static const Int LEFT_MARKER_WIDTH_ = 2;
static const Int LEFT_MARKER_HEIGHT_ = BOX_HEIGHT_ + XAXIS_HEIGHT_ + TICK_MARK_HEIGHT_;
static const Int LEFT_MARKER_X_ = XAXIS_X_;
static const Int LEFT_MARKER_Y_ = XAXIS_Y_ - BOX_HEIGHT_;

static const Int INSP_TIME_BOX_WIDTH_ = 72;
static const Int INSP_TIME_BOX_HEIGHT_ = BOX_HEIGHT_;
static const Int INSP_TIME_BOX_X_ = XAXIS_X_;
static const Int INSP_TIME_BOX_Y_ = XAXIS_Y_ - INSP_TIME_BOX_HEIGHT_;

static const Int INSP_TIME_XAXIS_WIDTH_ = INSP_TIME_BOX_WIDTH_;
static const Int INSP_TIME_XAXIS_HEIGHT_ = XAXIS_HEIGHT_;
static const Int INSP_TIME_XAXIS_X_ = XAXIS_X_ + 1;
static const Int INSP_TIME_XAXIS_Y_ = XAXIS_Y_;

static const Int INSP_TIME_HEIGHT_ = 12;
static const Int INSP_TIME_WIDTH_ = 50;
static const Int INSP_TIME_X_ = INSP_TIME_BOX_X_ + INSP_TIME_BOX_WIDTH_ / 2 - INSP_TIME_WIDTH_ / 2;
static const Int INSP_TIME_Y_ = XAXIS_Y_ - INSP_TIME_HEIGHT_ - TEXT_OFFSET_Y_;

static const Int EXP_TIME_BOX_WIDTH_ = 198;
static const Int EXP_TIME_BOX_HEIGHT_ = BOX_HEIGHT_;
static const Int EXP_TIME_BOX_X_ = XAXIS_X_ + INSP_TIME_BOX_WIDTH_ + 1;
static const Int EXP_TIME_BOX_Y_ = XAXIS_Y_ - EXP_TIME_BOX_HEIGHT_;

static const Int EXP_TIME_XAXIS_WIDTH_ = EXP_TIME_BOX_WIDTH_;
static const Int EXP_TIME_XAXIS_HEIGHT_ = XAXIS_HEIGHT_;
static const Int EXP_TIME_XAXIS_X_ = EXP_TIME_BOX_X_;
static const Int EXP_TIME_XAXIS_Y_ = XAXIS_Y_;

static const Int EXP_TIME_HEIGHT_ = 12;
static const Int EXP_TIME_WIDTH_ = 50;
static const Int EXP_TIME_X_ = EXP_TIME_BOX_X_ + EXP_TIME_BOX_WIDTH_ / 2 - EXP_TIME_WIDTH_ / 2;
static const Int EXP_TIME_Y_ = XAXIS_Y_ - EXP_TIME_HEIGHT_ - TEXT_OFFSET_Y_;

static const Int IE_RATIO_POINTER_WIDTH_ = 2;
static const Int IE_RATIO_POINTER_HEIGHT_ = 50;
static const Int IE_RATIO_POINTER_X_ = XAXIS_X_ + INSP_TIME_BOX_WIDTH_;
static const Int IE_RATIO_POINTER_Y_ = XAXIS_Y_ - BOX_HEIGHT_;

static const Int IE_RATIO_BOX_WIDTH_ = 60;
static const Int IE_RATIO_BOX_HEIGHT_ = BOX_HEIGHT_;
static const Int IE_RATIO_BOX_X_ = IE_RATIO_POINTER_X_ - IE_RATIO_BOX_WIDTH_ / 2;
static const Int IE_RATIO_BOX_Y_ = IE_RATIO_POINTER_Y_ + IE_RATIO_POINTER_HEIGHT_;

static const Int IE_RATIO_TEXT_X_ = IE_RATIO_BOX_X_ + 4;
static const Int IE_RATIO_TEXT_Y_ = IE_RATIO_BOX_Y_ + TEXT_OFFSET_Y_;

static const Int T_TOT_POINTER_HEIGHT_ = BOX_HEIGHT_ + XAXIS_HEIGHT_ + TICK_MARK_HEIGHT_;
static const Int T_TOT_POINTER_WIDTH_ = 2;
static const Int T_TOT_POINTER_X_ = XAXIS_X_ + INSP_TIME_BOX_WIDTH_ + EXP_TIME_BOX_WIDTH_;
static const Int T_TOT_POINTER_Y_ = XAXIS_Y_ - BOX_HEIGHT_;

static const Int T_TOT_BOX_HEIGHT_ = BOX_HEIGHT_;
static const Int T_TOT_BOX_WIDTH_ = 46;
static const Int T_TOT_BOX_X_ = T_TOT_POINTER_X_ - T_TOT_BOX_WIDTH_ / 2;
static const Int T_TOT_BOX_Y_ = T_TOT_POINTER_Y_ + T_TOT_POINTER_HEIGHT_;

static const Int T_TOT_HEIGHT_ = 12;
static const Int T_TOT_WIDTH_ = 36;
static const Int T_TOT_X_ = T_TOT_BOX_X_ + 5;
static const Int T_TOT_Y_ = T_TOT_BOX_Y_ + TEXT_OFFSET_Y_;

static const Int ZERO_WIDTH_ = 10;
static const Int ZERO_HEIGHT_ = 12;
static const Int ZERO_X_ = XAXIS_X_ - 4;
static const Int ZERO_Y_ = XAXIS_Y_ + XAXIS_HEIGHT_ + TICK_MARK_HEIGHT_ + TEXT_OFFSET_Y_;

static const Int RIGHT_MARKER_WIDTH_ = 2;
static const Int RIGHT_MARKER_HEIGHT_ = XAXIS_HEIGHT_ + TICK_MARK_HEIGHT_;
static const Int RIGHT_MARKER_X_ = XAXIS_X_ + XAXIS_WIDTH_ - RIGHT_MARKER_WIDTH_;
static const Int RIGHT_MARKER_Y_ = XAXIS_Y_;

static const Int UNITS_TEXT_X_ = RIGHT_MARKER_X_ - 4;
static const Int UNITS_TEXT_Y_ = RIGHT_MARKER_Y_ + RIGHT_MARKER_HEIGHT_ + TEXT_OFFSET_Y_;

static const Int NUMBER_OF_DIGITS_ = 3;

static const Real32 ZERO_VALUE_ = 0.0;
static const Real32 T_TOT_VALUE_ = 3.75;
static const Real32 INSPIRATORY_TIME_IN_SECS = 1.0;
static const Real32 EXPIRATORY_TIME_IN_SECS = 2.75;


static const Int32 MENU_X_ = 10;
static const Int32 MENU_Y_ = 36;
static const Int32 MENU_ROW_HEIGHT_ = 26;
static const Int32 MENU_NUM_ROWS_ = 8;
static const Int32 MENU_NUM_COLUMNS_ = 2;
static const Int32 MENU_COLUMN1_WIDTH_ = 558;
static const Int32 MENU_COLUMN2_WIDTH_ = 52;

static const Int32 GO_BACK_BUTTON_X_ = 550;
static const Int32 GO_BACK_BUTTON_Y_ = 3;
static const Int32 GO_BACK_BUTTON_WIDTH_ = 80;
static const Int32 GO_BACK_BUTTON_HEIGHT_ = 25;
static const Int32 GO_BACK_BUTTON_BORDER_ = 2;

static const Int32 NEXT_PAGE_BUTTON_X_ = 498;
static const Int32 NEXT_PAGE_BUTTON_WIDTH_ = 30;
static const Int32 PREV_PAGE_BUTTON_X_ = 461;
static const Int32 DIAGRAM_BUTTON_X_ = 80;
static const Int32 DIAGRAM_BUTTON_WIDTH_ = 74;

static const Int32 HELP_TITLE_CENTER_X_ = 318;
static const Int32 HELP_TITLE_Y_ = 4;

static const Int32 TEXT_AREA_X_ = 0;
static const Int32 TEXT_AREA_Y_ = 44;
static const Int32 TEXT_AREA_WIDTH_ = 634;
static const Int32 TEXT_AREA_HEIGHT_ = 225;
static const Int32 TEXT_AREA_SEPARATOR_HEIGHT_ = 2;
static const Int32 TEXT_AREA_SEPARATOR_Y_ =
					TEXT_AREA_Y_ - TEXT_AREA_SEPARATOR_HEIGHT_;


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: HelpSubScreen()  [Constructor]
//
//@ Interface-Description
// Constructs a Help subscreen.  It is passed two pointers, the first a
// pointer to the SubScreenArea that displays this subscreen and the
// second a pointer to the HelpMenu which is the main menu first displayed
// by this subscreen, and the root menu of the help system.
//---------------------------------------------------------------------
//@ Implementation-Description
// First construct all data members.  Then size the Help subscreen.
// Position and color any drawable members that need it.  Add any permanently
// displayed drawables to our container.  Register button callbacks with the 
// buttons.  Set precision to timing bar variables.  Lastly, position, color
// and set text to the diagram graphics components then hide them.
// No manual entry for HelpSubScreen.cc.
// $[01211] Help subscreen shall provide instant action topic buttons.
//---------------------------------------------------------------------
//@ PreCondition
// The subscreen area parameter must be non-null.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

HelpSubScreen::HelpSubScreen(SubScreenArea *pSubScreenArea,
											HelpMenu* pMainMenu) :
	SubScreen(pSubScreenArea),
	pMainMenu_(pMainMenu),
	nestingLevel_(0),
	currDiagram_(0),
	changedEntry_(0),
	titleArea_(MiscStrs::HELP_SUBSCREEN_TITLE),
	pMenu_(NULL),
	numTopics_(0),
	goBackButton_(GO_BACK_BUTTON_X_, GO_BACK_BUTTON_Y_,
				GO_BACK_BUTTON_WIDTH_, GO_BACK_BUTTON_HEIGHT_,
				Button::LIGHT, GO_BACK_BUTTON_BORDER_,
				Button::SQUARE, Button::NO_BORDER,
				MiscStrs::GO_BACK_BUTTON_TITLE),
	nextPageButton_(NEXT_PAGE_BUTTON_X_, GO_BACK_BUTTON_Y_,
				NEXT_PAGE_BUTTON_WIDTH_, GO_BACK_BUTTON_HEIGHT_,
				Button::LIGHT, GO_BACK_BUTTON_BORDER_,
				Button::SQUARE, Button::NO_BORDER,
				MiscStrs::NEXT_PAGE_BUTTON_TITLE),
	prevPageButton_(PREV_PAGE_BUTTON_X_, GO_BACK_BUTTON_Y_,
				NEXT_PAGE_BUTTON_WIDTH_, GO_BACK_BUTTON_HEIGHT_,
				Button::LIGHT, GO_BACK_BUTTON_BORDER_,
				Button::SQUARE, Button::NO_BORDER,
				MiscStrs::PREV_PAGE_BUTTON_TITLE),
	diagramButton_(DIAGRAM_BUTTON_X_, GO_BACK_BUTTON_Y_,
				DIAGRAM_BUTTON_WIDTH_, GO_BACK_BUTTON_HEIGHT_,
				Button::LIGHT, GO_BACK_BUTTON_BORDER_,
				Button::SQUARE, Button::NO_BORDER,
				MiscStrs::HELP_DIAGRAM_BUTTON_TITLE),
	helpTitle_(NULL_STRING_ID),
	titleUnderline_(0, 0, 0, 0),
	textArea_(TEXT_AREA_WIDTH_, TEXT_AREA_HEIGHT_),
	textAreaSeparator_(TEXT_AREA_X_, TEXT_AREA_SEPARATOR_Y_,
						TEXT_AREA_WIDTH_, TEXT_AREA_SEPARATOR_HEIGHT_),
	inspiratoryTimeBox_(INSP_TIME_BOX_X_, INSP_TIME_BOX_Y_, 
						INSP_TIME_BOX_WIDTH_, INSP_TIME_BOX_HEIGHT_),
	expiratoryTimeBox_(EXP_TIME_BOX_X_, EXP_TIME_BOX_Y_,
						EXP_TIME_BOX_WIDTH_, EXP_TIME_BOX_HEIGHT_),
	ieRatioBox_(IE_RATIO_BOX_X_, IE_RATIO_BOX_Y_,
						IE_RATIO_BOX_WIDTH_, IE_RATIO_BOX_HEIGHT_),
	ieRatioPointer_(IE_RATIO_POINTER_X_, IE_RATIO_POINTER_Y_,
						IE_RATIO_POINTER_WIDTH_, IE_RATIO_POINTER_HEIGHT_),
	tTotPointer_(T_TOT_POINTER_X_, T_TOT_POINTER_Y_, 
						T_TOT_POINTER_WIDTH_, T_TOT_POINTER_HEIGHT_),
	leftMarker_(LEFT_MARKER_X_, LEFT_MARKER_Y_, 
						LEFT_MARKER_WIDTH_, LEFT_MARKER_HEIGHT_),
	inspiratoryTime_(TextFont::TWELVE, NUMBER_OF_DIGITS_, CENTER),
	expiratoryTime_(TextFont::TWELVE, NUMBER_OF_DIGITS_, CENTER),
	tTot_(TextFont::TWELVE, NUMBER_OF_DIGITS_, CENTER),
	zero_(TextFont::TWELVE, NUMBER_OF_DIGITS_, CENTER),
	xAxis_(XAXIS_X_, XAXIS_Y_, XAXIS_WIDTH_, XAXIS_HEIGHT_),
	inspiratoryTimeXAxis_(INSP_TIME_XAXIS_X_, INSP_TIME_XAXIS_Y_,
						INSP_TIME_XAXIS_WIDTH_, INSP_TIME_XAXIS_HEIGHT_),
	expiratoryTimeXAxis_(EXP_TIME_XAXIS_X_, EXP_TIME_XAXIS_Y_, 
						EXP_TIME_XAXIS_WIDTH_, EXP_TIME_XAXIS_HEIGHT_),
	tTotOutsideBox_(T_TOT_BOX_X_, T_TOT_BOX_Y_, T_TOT_BOX_WIDTH_, T_TOT_BOX_HEIGHT_ ),
	rightMarker_(RIGHT_MARKER_X_, RIGHT_MARKER_Y_, 
				RIGHT_MARKER_WIDTH_, RIGHT_MARKER_HEIGHT_),
	unitsText_(MiscStrs::HELP_BREATH_TIMING_UNIT),
	goBackBitmap_( Image::RLeftArrowIcon ),
	nextPageBitmap_( Image::RRightArrowIcon ),
	prevPageBitmap_( Image::RLeftArrowIcon )
{
	CALL_TRACE("HelpSubScreen::HelpSubScreen(pSubScreenArea)");
	SAFE_CLASS_PRE_CONDITION(pSubScreenArea != NULL);

	// Size and position the sub-screen
	setX(0);
	setY(0);
	setWidth(UPPER_SUB_SCREEN_AREA_WIDTH);
	setHeight(UPPER_SUB_SCREEN_AREA_HEIGHT);

	// Set subscreen background color
	setFillColor(Colors::MEDIUM_BLUE);

	// Set text area 
	textArea_.setX(TEXT_AREA_X_);
	textArea_.setY(TEXT_AREA_Y_);
	//textAreaSeparator_.setColor(Colors::LIGHT_BLUE);

	// Set up title area
	helpTitle_.setY(HELP_TITLE_Y_);
	helpTitle_.setColor(Colors::WHITE);
	titleUnderline_.setColor(Colors::WHITE);

	// Set up navigation button bitmaps
	goBackBitmap_.setX(3);
	goBackBitmap_.setY(2);
	goBackButton_.addLabel( &goBackBitmap_ );

	nextPageBitmap_.setX(3);
	nextPageBitmap_.setY(2);
	nextPageButton_.addLabel( &nextPageBitmap_ );

	prevPageBitmap_.setX(3);
	prevPageBitmap_.setY(2);
	prevPageButton_.addLabel( &prevPageBitmap_ );

	// Register for callbacks from "go back" button
	goBackButton_.setButtonCallback(this);
	nextPageButton_.setButtonCallback(this);
	prevPageButton_.setButtonCallback(this);
	diagramButton_.setButtonCallback(this);

	for (int i = 0; i < MAX_UPPER_LOWER_DISPLAY_AREAS; i++)
	{												// $[TI1]
		addDrawable(&diagramBoxes_[i]);
		diagramBoxes_[i].setColor(Colors::WHITE);
		addDrawable(&diagramText_[i]);
	}												// $[TI2]

	// Add drawables to drawable list
	addDrawable(&titleArea_);						
	addDrawable(&helpTitle_);
	addDrawable(&titleUnderline_);
	addDrawable(&goBackButton_);
	addDrawable(&nextPageButton_);
	addDrawable(&prevPageButton_);
	addDrawable(&diagramButton_);

	addDrawable(&inspiratoryTimeBox_);
	addDrawable(&expiratoryTimeBox_);
	addDrawable(&ieRatioBox_);
	addDrawable(&ieRatioPointer_);
	addDrawable(&tTotPointer_);
	addDrawable(&leftMarker_);
	addDrawable(&inspiratoryTime_);
	addDrawable(&expiratoryTime_);
	addDrawable(&ieRatioText_);
	addDrawable(&tTotOutsideBox_);
	addDrawable(&tTot_);
	addDrawable(&unitsText_);
	addDrawable(&zero_);
	addDrawable(&xAxis_);
	addDrawable(&inspiratoryTimeXAxis_);
	addDrawable(&expiratoryTimeXAxis_);
	addDrawable(&rightMarker_);
	addDrawable(&lowerLabel_);
	addDrawable(&upperLabel_);

	//  Lower Screen :	Main Settings Area
	setBoxAttributes_(MAIN_SETTING_AREA, 
					MAIN_SETTING_AREA_X_, MAIN_SETTING_AREA_Y_, 
					MAIN_SETTING_AREA_WIDTH_, MAIN_SETTING_AREA_HEIGHT_, 
					MiscStrs::HELP_MAIN_SETTING_TITLE,
					Colors::BLACK, Colors::WHITE);

	//	Lower Screen : Lower Subscreen Area
	setBoxAttributes_(LOWER_SUB_SCREEN_AREA,
					MAIN_SETTING_AREA_X_,
					LOWER_SUB_SCREEN_AREA_Y_,
					MAIN_SETTING_AREA_WIDTH_, 
					LOWER_SUB_SCREEN_AREA_HEIGHT_, 
					MiscStrs::HELP_LOWER_SUBSCREEN_TITLE,
					Colors::MEDIUM_BLUE, Colors::WHITE);

	//	Lower Screen : Lower Screen Select Area
	setBoxAttributes_(LOWER_SCREEN_SELECT_AREA,
					MAIN_SETTING_AREA_X_,
					LOWER_SCREEN_SELECT_AREA_Y_,
					LOWER_SCREEN_SELECT_AREA_WIDTH_,
					LOWER_SCREEN_SELECT_AREA_HEIGHT_, 
					MiscStrs::HELP_LOWER_SELECT_AREA_TITLE,
					Colors::LIGHT_BLUE, Colors::BLACK);

	//	Lower Screen : Message Area
	setBoxAttributes_(MESSAGE_AREA,
					MAIN_SETTING_AREA_X_,
					MESSAGE_AREA_Y_,
					LOWER_SCREEN_SELECT_AREA_WIDTH_,
					MESSAGE_AREA_HEIGHT_,
					MiscStrs::HELP_MESSAGE_AREA_TITLE,
					Colors::EXTRA_DARK_BLUE, Colors::WHITE);

	//  Lower Screen : Prompt Area
	setBoxAttributes_(PROMPT_AREA,
					PROMPT_AREA_X_,
					LOWER_SCREEN_SELECT_AREA_Y_,
					PROMPT_AREA_WIDTH_,
					 PROMPT_AREA_HEIGHT_, 
					MiscStrs::HELP_PROMPT_AREA_TITLE,
					Colors::LIGHT_BLUE, Colors::BLACK);

	// Upper Screen: Vital Patient Data Area
	setBoxAttributes_(VITAL_PT_DATA_AREA, 
					UPPER_SCREEN_SETTING_AREA_X_, UPPER_SCREEN_SETTING_AREA_Y_, 
					MAIN_SETTING_AREA_WIDTH_, VITAL_PATIENT_DATA_AREA_HEIGHT_,
					MiscStrs::HELP_VPDA_TITLE,
					Colors::BLACK, Colors::WHITE);

	// Upper Screen: Alarm and Status Area
	setBoxAttributes_(ALARM_AND_STATUS_AREA, 
					UPPER_SCREEN_SETTING_AREA_X_, 
					UPPER_SCREEN_SETTING_AREA_Y_+ VITAL_PATIENT_DATA_AREA_HEIGHT_,
					MAIN_SETTING_AREA_WIDTH_, 
					VITAL_PATIENT_DATA_AREA_HEIGHT_, 
					MiscStrs::HELP_ALARM_STATUS_AREA_TITLE,
					Colors::BLACK, Colors::WHITE);

	//	Upper Screen : Upper SubScreen Area
	setBoxAttributes_(UPPER_SUB_SCREEN_AREA,
					UPPER_SCREEN_SETTING_AREA_X_,
					UPPER_SUB_SCREEN_AREA_Y_,
					MAIN_SETTING_AREA_WIDTH_, 
					UPPER_SUB_SCREEN_AREA_HEIGHT_, 
					MiscStrs::HELP_UPPER_SUBSCREEN_AREA_TITLE,
					Colors::MEDIUM_BLUE, Colors::WHITE);

	//	Upper SubScreen Select Area 
	setBoxAttributes_(UPPER_SCREEN_SELECT_AREA,
					MAIN_SETTING_AREA_X_,
					UPPER_SCREEN_SELECT_AREA_Y_,
					MAIN_SETTING_AREA_WIDTH_,
					LOWER_SCREEN_SELECT_AREA_HEIGHT_, 
					MiscStrs::HELP_UPPER_SCREEN_SELECT_AREA_TITLE,
					Colors::LIGHT_BLUE, Colors::BLACK);

	// Label areas
	lowerLabel_.setColor(Colors::WHITE);
	lowerLabel_.setX(LOWER_SCREEN_DIAGRAM_LABEL_X_);
	lowerLabel_.setY(MAIN_SETTING_AREA_Y_);
	lowerLabel_.setText(MiscStrs::HELP_LOWER_SCREEN_DIAGRAM_LABEL);

	upperLabel_.setColor(Colors::WHITE);
	upperLabel_.setX(UPPER_SCREEN_DIAGRAM_LABEL_X_);
	upperLabel_.setY(UPPER_SCREEN_SETTING_AREA_Y_);
	upperLabel_.setText(MiscStrs::HELP_UPPER_SCREEN_DIAGRAM_LABEL);

	setBreathTimingDiagram_();
	hideDiagrams_();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~HelpSubScreen  [Destructor]
//
//@ Interface-Description
//  Destroys HelpSubScreen.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Empty 
//---------------------------------------------------------------------
//@ PreCondition
//	Empty	
//---------------------------------------------------------------------
//@ PostCondition
//  Empty	
//@ End-Method
//=====================================================================

HelpSubScreen::~HelpSubScreen(void)
{
	CALL_TRACE("HelpSubScreen::~HelpSubScreen(void)");
	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activate
//
//@ Interface-Description
// Called by our SubScreenArea just before this subscreen is displayed
// allowing us to do any necessary display setup.  Takes no arguments.
// Simply display the highest level menu.
//---------------------------------------------------------------------
//@ Implementation-Description
// Create a new ScrollableLog object to store our new menu's data then position 
// and initialize it.
// Setup the columns of the ScrollableLog which display
// all menus.  Reset the nested menu arrays back to the start
// and make sure the text area drawables, the diagrams and all the buttons
// are not displayed.  Then display the menu.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
HelpSubScreen::activate(void)
{
	CALL_TRACE("HelpSubScreen::activate(void)");

	// Create the scrollable log used for the menu
	pMenu_ = ScrollableLog::New(this, MENU_NUM_ROWS_, MENU_NUM_COLUMNS_,
								MENU_ROW_HEIGHT_, FALSE);

	// Setup menu stuff
	pMenu_->setX(MENU_X_);
	pMenu_->setY(MENU_Y_);

	pMenu_->setUserSelectable(TRUE);

	// Setup column info for the menu's ScrollableLog
	pMenu_->setColumnInfo(1, MiscStrs::HELP_TOPIC_COLUMN_TITLE,
						MENU_COLUMN1_WIDTH_, LEFT, 10,
						TextFont::NORMAL, TextFont::FOURTEEN);
	pMenu_->setColumnInfo(0, MiscStrs::HELP_SELECT_COLUMN_TITLE,
						MENU_COLUMN2_WIDTH_);

	// Display the main menu
	addDrawable(pMenu_);

	// We are at the highest level.  Set the nestingLevel_ to 0 and point
	// the top entry to the main menu.
	nestingLevel_		 = 0;
	nestedMenus_[0]		 = pMainMenu_;
	firstMenuEntries_[0] = 0;

	// Make sure all buttons are not shown because it's not possible.
	// to go back any farther.
	goBackButton_.setShow(FALSE);
	prevPageButton_.setShow(FALSE);
	nextPageButton_.setShow(FALSE);
	diagramButton_.setShow(FALSE);

	// Don't show text area.
	removeDrawable(&textArea_);
	//removeDrawable(&textAreaSeparator_);

	// Hide any help graph
	hideDiagrams_();

	// Setup the main menu
	setupCurrentMenu_();

	// Activate the menu
	pMenu_->activate();									// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: deactivate
//
//@ Interface-Description
// Called by our SubScreenArea just after this subscreen is removed from the
// display allowing us to do any necessary cleanup.  Takes no arguments.
//---------------------------------------------------------------------
//@ Implementation-Description
// Remove the scrollable log from our container.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
HelpSubScreen::deactivate(void)
{
	CALL_TRACE("HelpSubScreen::deactivate(void)");

	// Remove the scrollable log
	removeDrawable(pMenu_);								// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: buttonDownHappened
//
//@ Interface-Description
// Called when any button on the help screen is pressed down.  
// If it's the "go back" button, the last menu that was displayed is reshown.
// If it's the diagram button, the proper diagram for the current help text
// is shown.  If it's the forward/backward arrow button, the next/previous 
// page of text is shown.
//---------------------------------------------------------------------
//@ Implementation-Description
// If "diagram" button is pressed down, set this button to the "up" state
// (its next state) before hiding it.  Also hide other buttons.  Remove any
// text.  Display the diagram.
//
// If "go back" button is pressed down, first, pop the "go back" button back to
// up (it is an instant action button).  Then, if already displaying a menu,
// just overwrite the contents with the previous menu in the menu stack.  Else
// if help diagram is on display, redisplay the appropriate buttons, the text
// area (the help diagram is hidden) and its help title.  Else if help text is
// being display, return display to the previously displayed menu.  If "paging"
// button is pressed, turn to the appropriate page as requested.
// 
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
HelpSubScreen::buttonDownHappened(Button* pButton, Boolean )
{
	CALL_TRACE("HelpSubScreen::buttonDownHappened(Button* , Boolean )");


	if (pButton == &diagramButton_)
	{											//$[TI1]
		diagramButton_.setToUp();
		diagramButton_.setShow(FALSE);
		prevPageButton_.setShow(FALSE);
		nextPageButton_.setShow(FALSE);

		// Display screen diagram 
		removeDrawable(&textArea_);
		//removeDrawable(&textAreaSeparator_);
		displayDiagram_(currDiagram_);
	}
	else if (pButton == &goBackButton_)
	{											// $[TI2]
		Boolean wasDiagram = FALSE;

		// Bounce the "go back" button
		goBackButton_.setToUp();

		// Display the Help screen topics.
		addDrawable(&titleUnderline_);

		// If currently displaying a menu then go back up a menu level.
		if (pMenu_->isVisible())
		{										// $[TI3]
			SAFE_CLASS_ASSERTION(nestingLevel_ > 0);  // Can't be at top level
	
			nestingLevel_--;				// Back up one level
			setupCurrentMenu_();			// Setup new current menu
		}
		else	// Help diagram or text is on current display
		{										// $[TI4]
			// If diagram is on display, hide diagram and return to
			// the previous text screen.
			if (isDiagramVisible_())
			{									// $[TI5]
				wasDiagram = TRUE;

				// Show the diagram button
				diagramButton_.setShow(TRUE);
				nextPageButton_.setShow(textArea_.nextPage());
				prevPageButton_.setShow(textArea_.prevPage());

				// Hide any diagram
				hideDiagrams_();

				// Return to the previous text screen
				addDrawable(&textArea_);
				//addDrawable(&textAreaSeparator_);

				// Set the help title to be the current help text title
				setHelpTitle_(*((nestedMenus_[nestingLevel_]->pHelpTopics[changedEntry_].pHelpText)->pHelpTextTitle));
			}
			else	// Text is being displayed, so return to the
					// previous menu
			{									// $[TI6]
				// Hide text
				removeDrawable(&textArea_);
				//removeDrawable(&textAreaSeparator_);
				prevPageButton_.setShow(FALSE);
				nextPageButton_.setShow(FALSE);
				diagramButton_.setShow(FALSE);

				// Redisplay last menu.
				addDrawable(pMenu_);
		
				// Set the help title to be the current menu's title
				setHelpTitle_(*(nestedMenus_[nestingLevel_]->P_TITLE));
			}
		}

		// If the menu being displayed is a top level menu and it was not
		// the result of returning from the diagram then remove "go back"
		// button as we can't go back any further.  Whenever we are returning
		// from the diagram, the "go back" button should be available to
		// allow user back to the top menu.
		if (0 == nestingLevel_ && !wasDiagram)
		{										// $[TI7]
			goBackButton_.setShow(FALSE);
		}										// $[TI8]
	}
	else if (pButton == &nextPageButton_)
	{											// $[TI9]
		// Bounce the "next page" button
		nextPageButton_.setToUp();

		// Page down
		textArea_.scrollHappened(TextArea::SCROLL_FORWARD);

		// If there is no more text to scroll, hide this button.
		nextPageButton_.setShow(textArea_.nextPage());
		prevPageButton_.setShow(textArea_.prevPage());
	}
	else if (pButton == &prevPageButton_)
	{											// $[TI10]
		// Bounce the "previous page" button
 		prevPageButton_.setToUp();

		// Page up
		textArea_.scrollHappened(TextArea::SCROLL_BACKWARD);

		// If there is no more text to scroll, hide this button.
		nextPageButton_.setShow(textArea_.nextPage());
		prevPageButton_.setShow(textArea_.prevPage());
	}											// $[TI11]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getLogEntryColumn
//
//@ Interface-Description
// Called by ScrollableLog when it needs to retrieve the text content
// of a particular column of a help menu entry.  Passed the following
// parameters:
// >Von
//	entryNumber			The index of the log entry
//	columnNumber		The column number of the log entry
//	rIsCheapText		Output parameter.  A reference to a flag which
//						determines whether the text content of the log
//						entry is specified as cheap text or not.
//	rString1			String id of the first string. Can be
//						set to NULL_STRING_ID to indicate an empty entry
//						column.
//	rString2			String id of the second string.
//	rString3			String id of the third string.  Not used if
//						rIsCheapText is FALSE.
//	rString1Message		A help message that will be displayed in the
//						Message Area when rString1 is touched.  This only
//						works when rIsCheapText is TRUE.
// >Voff
// We retrieve information for the appropriate column, make sure it's
// formatted correctly and return it to ScrollableLog.
//---------------------------------------------------------------------
//@ Implementation-Description
// Depending on which column is requested we return the textual content
// of the specified menu entry.  Column 0 is the topic column, column 1
// is the select column (contains no text, just select buttons).  The
// automatic text formatting feature of ScrollableLog is used for all columns.
// ScrollableLog is used for all columns.
//---------------------------------------------------------------------
//@ PreCondition
// The entry and column number parameters must be within range.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
HelpSubScreen::getLogEntryColumn(Uint16 entry, Uint16 column,
				Boolean &rIsCheapText, StringId &rString1, StringId &rString2,
				StringId &, StringId &)
{
	CALL_TRACE("HelpSubScreen::getLogEntryColumn(Uint16 entry, Uint16 column, "
				"Boolean &rIsCheapText, StringId &rString1, "
				"StringId &rString2, StringId &rString3, StringId &rString1Message)");

	CLASS_PRE_CONDITION((entry < numTopics_) && (column < MENU_NUM_COLUMNS_));

	// No cheap text is used for any menu information
	rIsCheapText = TRUE;

	// Check which column is requested
	switch (column)
	{
	case 1:												// $[TI1]
		// Column 1, the Topic column.  Only 1 line of text is used so
		// set rString1 to the correct help topic.
		rString1 =
				*(nestedMenus_[nestingLevel_]->pHelpTopics[entry].P_TOPIC);
		rString2 = NULL;
		break;

	case 0:												// $[TI2]
		// Column 0, the Select column.  Contains only selection buttons so
		// no text is displayed.  Set both rString1 and rString2 to null.
		rString1 = NULL_STRING_ID;
		rString2 = NULL_STRING_ID;
		break;

	default:											// $[TI3]
		SAFE_CLASS_ASSERTION(FALSE);			// should not be reached.
		break;
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: currentLogEntryChangeHappened
//
//@ Interface-Description
// When the users selects or deselects a menu entry of the log then
// ScrollableLog calls this method.  The first parameter specifies the entry in
// question and the second is a flag, 'isSelected', which will be TRUE if the
// entry was selected, FALSE if the entry was deselected.
//---------------------------------------------------------------------
//@ Implementation-Description
// We clear the selected entry as soon as we detect the selection event
// (selection is an instant action event).  We check whether the current option
// points to a submenu or to help text and change the display appropriately.
// The diagram button is shown only if there is a diagram contained in the 
// current topics.
// 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
HelpSubScreen::currentLogEntryChangeHappened(Uint16 changedEntry, Boolean)
{
	CALL_TRACE("HelpSubScreen::currentLogEntryChangeHappened("
									"Uint16 changedEntry, Boolean isSelected)");

	// User selected a menu topic.  First debounce the selection
	pMenu_->clearSelectedEntry();

	// Let's see if the selection brings us to help text, graph or a submenu.
	HelpMenuTopic *pHelpTopics = nestedMenus_[nestingLevel_]->pHelpTopics;
				
	if (pHelpTopics[changedEntry].pHelpText != NULL)
	{												// $[TI1]
		// Help text!
		// Remove menu
		removeDrawable(pMenu_);

		// Display help text in text area
		textArea_.setLines((pHelpTopics[changedEntry].pHelpText)->pHelpTextLines);

		addDrawable(&textArea_);
		//addDrawable(&textAreaSeparator_);

		// Set the help title to be the current help text title
		setHelpTitle_(*((pHelpTopics[changedEntry].pHelpText)->pHelpTextTitle));

		goBackButton_.setShow(TRUE);

		// Display next or previous page buttons only if there is more text
		// to scroll.
		nextPageButton_.setShow(textArea_.nextPage());
		prevPageButton_.setShow(FALSE);	// No prev page on first page display

		// Diagram button is only displayed if there is a diagram associated
		// with the current topics.
		if (pHelpTopics[changedEntry].pHelpText->helpDiagram != HELP_NO_DIAGRAM)
		{											// $[TI2]
			diagramButton_.setShow(TRUE);
			currDiagram_ = pHelpTopics[changedEntry].pHelpText->helpDiagram;
		}											// $[TI3]
		changedEntry_ = changedEntry;
	}
	else
	{												// $[TI4]
		// Submenu!
		CLASS_ASSERTION(pHelpTopics[changedEntry].pSubMenu != NULL);

		// Store the first displayed entry of the current menu so that we
		// can go back to that position in this menu when this menu is popped
		// back to by the user.
		firstMenuEntries_[nestingLevel_] = pMenu_->getFirstDisplayedEntry();

		// Nested menu, go down a nesting level
		nestingLevel_++;
		CLASS_ASSERTION(nestingLevel_ < MAX_NESTED_MENUS_);

		// Add the new submenu to the nestedMenus_[] array.
		nestedMenus_[nestingLevel_] =
			nestedMenus_[nestingLevel_ - 1]->pHelpTopics[changedEntry].pSubMenu;
		firstMenuEntries_[nestingLevel_] = 0;

// The order of the following 2 methods have been reversed.
		// Setup the display for the new menu.
		setupCurrentMenu_();

		// Setup the display for the select buttons.
		pMenu_->setUserSelectable(TRUE);

		// Make sure "go back" button is displayed
		goBackButton_.setShow(TRUE);

		// Undisplay all but "go back" button.
		nextPageButton_.setShow(FALSE);
		prevPageButton_.setShow(FALSE);
		diagramButton_.setShow(FALSE);
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setupCurrentMenu_		[Private]
//
//@ Interface-Description
// Formulates the display for a new current menu.
//---------------------------------------------------------------------
//@ Implementation-Description
// Go through the current menu array structure and count the number of entries
// in it.  Then pass that information on to pMenu_, our ScrollableLog.
// Change the help title to the new menu's title.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
HelpSubScreen::setupCurrentMenu_(void)
{
	CALL_TRACE("HelpSubScreen::setupCurrentMenu_(void)");

	// Count the number of topics in the current menu

	for (numTopics_ = 0;
				(nestedMenus_[nestingLevel_]->
							pHelpTopics[numTopics_].P_TOPIC) != NULL;
				numTopics_++)
	{													// $[TI1]
		// Do nothing
	}													// $[TI2]
	

	// Inform menu_ of the number of entries in the menu (this action
	// refreshes the menu if it's currently displayed.
	pMenu_->setEntryInfo(firstMenuEntries_[nestingLevel_], numTopics_);

	// Set the help title to be the current menu's title
	setHelpTitle_(*(nestedMenus_[nestingLevel_]->P_TITLE));
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setHelpTitle_
//
//@ Interface-Description
// Looks after formatting the help title text and its underlining.
//---------------------------------------------------------------------
//@ Implementation-Description
// Build a cheap text string out of the title, display it in helpTitle_,
// center it and position and size the underline line appropriately.
//---------------------------------------------------------------------
//@ PreCondition
// The help title string must be non-null.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
 
void
HelpSubScreen::setHelpTitle_(const wchar_t *P_HELP_TITLE)
{
	CALL_TRACE("HelpSubScreen::setHelpTitle_(const char *P_HELP_TITLE)");

	CLASS_ASSERTION(P_HELP_TITLE != NULL);

	wchar_t tmpBuffer[256];
	swprintf(tmpBuffer, L"{p=12,y=16:%s}", P_HELP_TITLE);
	helpTitle_.setText(tmpBuffer);
	helpTitle_.setX(HELP_TITLE_CENTER_X_ - helpTitle_.getWidth() / 2);
	titleUnderline_.setStartPoint(helpTitle_.getX(),
				helpTitle_.getY() + helpTitle_.getHeight() + 1);	
	titleUnderline_.setEndPoint(helpTitle_.getX() + helpTitle_.getWidth() - 1,
				helpTitle_.getY() + helpTitle_.getHeight() + 1);
														// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: displayDiagram_ [private] 
//
//@ Interface-Description
//	Show the help diagram.
//---------------------------------------------------------------------
//@ Implementation-Description
//	We set the diagram title then show the requested diagram.
//---------------------------------------------------------------------
//@ PreCondition
//	Empty 
//---------------------------------------------------------------------
//@ PostCondition
//	Empty	
//@ End-Method
//=====================================================================

void HelpSubScreen::displayDiagram_(int whichDiagram)
{
	CALL_TRACE("HelpSubScreen::displayDiagram_");
	
	switch(whichDiagram)
	{
		// Lower Subscreen	
		case	HELP_DIAGRAM1:	
		{													// $[TI1]
			// Display the title for the lower subscreen.
			setHelpTitle_(MiscStrs::HELP_SCREEN_DIAGRAM_TITLE);

			// Set visibility of all diagram graphics.
			for (int i = 0; i < MAX_UPPER_LOWER_DISPLAY_AREAS; i++)
			{												// $[TI2]
				diagramBoxes_[i].setShow(TRUE);
				diagramText_[i].setShow(TRUE);
			}												// $[TI3]
			lowerLabel_.setShow(TRUE);
			upperLabel_.setShow(TRUE);
			break;
		}
		case	HELP_DIAGRAM2:
															// $[TI4]
			// Display the title for the upper subscreen.

			setHelpTitle_(MiscStrs::HELP_BREATH_TIMING_DIAGRAM_TITLE);
			inspiratoryTimeBox_.setShow(TRUE);
			leftMarker_.setShow(TRUE);
			inspiratoryTime_.setShow(TRUE);
			expiratoryTimeBox_.setShow(TRUE);
			expiratoryTime_.setShow(TRUE);
			ieRatioPointer_.setShow(TRUE);
			ieRatioText_.setShow(TRUE);
			ieRatioBox_.setShow(TRUE);
			tTotPointer_.setShow(TRUE);
			tTotOutsideBox_.setShow(TRUE);
			tTot_.setShow(TRUE);
			unitsText_.setShow(TRUE);
			xAxis_.setShow(TRUE);
			inspiratoryTimeXAxis_.setShow(TRUE);
			expiratoryTimeXAxis_.setShow(TRUE);
			rightMarker_.setShow(TRUE);
			zero_.setShow(TRUE);
			break;
		default:											// $[TI5]
			break;
	}	
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setBoxAttributes_ [private] 
//
//@ Interface-Description
//  Assign data to the display box's attributes.
//---------------------------------------------------------------------
//@ Implementation-Description
// 	Set the diagram coordinates, sizes and color.  Then set  
//  the text to be displayed inside the box.  
//---------------------------------------------------------------------
//@ PreCondition
//	Width > 0, height > 0 and  boxTitle non NULL.
//---------------------------------------------------------------------
//@ PostCondition
//	Empty	
//@ End-Method
//=====================================================================

void HelpSubScreen::setBoxAttributes_(ScreenAreaId areaId, 
			Uint16 xVal, Uint16 yVal,
			Uint16 width, Uint16 height, StringId boxTitle,
			Colors::ColorS fillColor, Colors::ColorS textColor)
{
	CALL_TRACE("HelpSubScreen::setBoxAttributes_(ScreenAreaId areaId," 
	            "Uint16 xVal, Uint16 yVal,"
	            "Uint16 width, Uint16 height, StringId boxTitle,"
	            "Colors::ColorS fillColor, Colors::ColorS textColor");

	CLASS_ASSERTION((width > 0 ) && (height > 0) && (boxTitle != NULL));

	// Set box attribute values.
	diagramBoxes_[areaId].setX(xVal);
	diagramBoxes_[areaId].setY(yVal);
	diagramBoxes_[areaId].setWidth(width);				
	diagramBoxes_[areaId].setHeight(height);
	diagramBoxes_[areaId].setFillColor(fillColor);

	// Set text attribute values.
	diagramText_[areaId].setX(xVal);
	diagramText_[areaId].setY(yVal);
	diagramText_[areaId].setColor(textColor);
	diagramText_[areaId].setText(boxTitle);
																// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: hideDiagrams_ [private] 
//
//@ Interface-Description
//  Called when we want to hide all diagrams from display.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Set the Show flags of all diagram elements to FALSE.
//---------------------------------------------------------------------
//@ PreCondition
//	Empty	
//---------------------------------------------------------------------
//@ PostCondition
//	Empty	
//@ End-Method
//=====================================================================

void
HelpSubScreen::hideDiagrams_()
{
	// Hide screen diagram.
	for (int i = 0; i < MAX_UPPER_LOWER_DISPLAY_AREAS; i++)
	{														// $[TI1]
		diagramBoxes_[i].setShow(FALSE);
		diagramText_[i].setShow(FALSE);
	}														// $[TI2]
	lowerLabel_.setShow(FALSE);
	upperLabel_.setShow(FALSE);

	// Hide the breath timing diagram.
	inspiratoryTimeBox_.setShow(FALSE);
	expiratoryTimeBox_.setShow(FALSE);
	ieRatioBox_.setShow(FALSE);
	ieRatioPointer_.setShow(FALSE);
	tTotPointer_.setShow(FALSE);
	tTotOutsideBox_.setShow(FALSE);
	leftMarker_.setShow(FALSE);
	inspiratoryTime_.setShow(FALSE);
	expiratoryTime_.setShow(FALSE);
	ieRatioText_.setShow(FALSE);
	tTot_.setShow(FALSE);
	unitsText_.setShow(FALSE);
	zero_.setShow(FALSE);
	xAxis_.setShow(FALSE);
	inspiratoryTimeXAxis_.setShow(FALSE);
	expiratoryTimeXAxis_.setShow(FALSE);
	rightMarker_.setShow(FALSE);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isDiagramVisible_ [private] 
//
//@ Interface-Description
//  Called when we want to check the visibility of the current diagram. 
//---------------------------------------------------------------------
//@ Implementation-Description
//	Check the visibility flags of the current diagram.   Return TRUE
//  if all flags are set to TRUE.  Return FALSE otherwise.
//---------------------------------------------------------------------
//@ PreCondition
//	Empty	
//---------------------------------------------------------------------
//@ PostCondition
//	Empty	
//@ End-Method
//=====================================================================

Boolean
HelpSubScreen::isDiagramVisible_()
{
	switch(currDiagram_)
	{
		case HELP_DIAGRAM1:
		{														// $[TI1]
			for (int i = 0; i < MAX_UPPER_LOWER_DISPLAY_AREAS; i++)
			{													// $[TI2]
				if ( !(diagramBoxes_[i].isVisible()) ||
				     !(diagramText_[i].isVisible()) 
				   )
				{												// $[TI3]
					return(FALSE);
				}												// $[TI4]
			}													// $[TI5]
			return(TRUE);
		}
		case HELP_DIAGRAM2:
		{														// $[TI6]
			return(inspiratoryTimeBox_.isVisible());
		}
		default:												// $[TI7]
			break;
	}
	return(FALSE);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setBreathTimingDiagram [private] 
//
//@ Interface-Description
//  Called when we want to initialize HelpScreen's Breath Timing diagram.
//---------------------------------------------------------------------
//@ Implementation-Description
//	We set the value, color, position and dimension for all the
//	graphics contained in this diagram.
//---------------------------------------------------------------------
//@ PreCondition
//	Empty	
//---------------------------------------------------------------------
//@ PostCondition
//	Empty	
//@ End-Method
//=====================================================================

void
HelpSubScreen::setBreathTimingDiagram_()
{
	// Set box attribute values.
	inspiratoryTimeBox_.setColor(Colors::LIGHT_BLUE);
	inspiratoryTimeBox_.setFillColor(Colors::LIGHT_BLUE);

	inspiratoryTime_.setX(INSP_TIME_X_);
	inspiratoryTime_.setY(INSP_TIME_Y_);
	inspiratoryTime_.setHeight(INSP_TIME_HEIGHT_);
	inspiratoryTime_.setValue(INSPIRATORY_TIME_IN_SECS);
	inspiratoryTime_.setColor(Colors::BLACK);
	inspiratoryTime_.setPrecision(HUNDREDTHS);

	// Expiratory Time
	expiratoryTimeBox_.setColor(Colors::LIGHT_BLUE);
	expiratoryTimeBox_.setFillColor(Colors::LIGHT_BLUE);

	expiratoryTime_.setX(EXP_TIME_X_);
	expiratoryTime_.setY(EXP_TIME_Y_);
	expiratoryTime_.setHeight(EXP_TIME_HEIGHT_);
	expiratoryTime_.setColor(Colors::BLACK);
	expiratoryTime_.setHighlighted(TRUE);
	expiratoryTime_.setItalic(TRUE);
	expiratoryTime_.setValue(EXPIRATORY_TIME_IN_SECS);
	expiratoryTime_.setPrecision(HUNDREDTHS);

	ieRatioPointer_.setX(IE_RATIO_POINTER_X_);

	// Formulate the cheap text for the 1:num display
	wchar_t * ieRatioString_ = L"{p=12,y=12,I:1 : 2.75}";   
	ieRatioText_.setColor(Colors::BLACK);
	ieRatioText_.setText(ieRatioString_);
	ieRatioText_.setHighlighted(TRUE);
	ieRatioText_.setX(IE_RATIO_TEXT_X_);
	ieRatioText_.setY(IE_RATIO_TEXT_Y_);

	// Wrap box around I:E Ratio text
	ieRatioBox_.setColor(Colors::WHITE);
	ieRatioBox_.setFillColor(Colors::LIGHT_BLUE);

	// T-tot
	tTotPointer_.setX(T_TOT_POINTER_X_);
	tTotPointer_.setY(T_TOT_POINTER_Y_);
	tTotPointer_.setWidth(T_TOT_POINTER_WIDTH_);
	tTotPointer_.setHeight(T_TOT_POINTER_HEIGHT_);

	tTot_.setX(T_TOT_X_);
	tTot_.setY(T_TOT_Y_);
	tTot_.setValue(T_TOT_VALUE_);
	tTot_.setItalic(TRUE);
	tTot_.setHighlighted(TRUE);
	tTot_.setColor(Colors::WHITE);
	tTot_.setWidth(T_TOT_WIDTH_);
	tTot_.setHeight(T_TOT_HEIGHT_);
	tTot_.setPrecision(HUNDREDTHS);

	tTotOutsideBox_.setColor(Colors::WHITE);
	tTotOutsideBox_.setFillColor(Colors::GOLDENROD);

	unitsText_.setColor(Colors::WHITE);
	unitsText_.setX(UNITS_TEXT_X_);
	unitsText_.setY(UNITS_TEXT_Y_);

	xAxis_.setColor(Colors::BLACK);
	xAxis_.setFillColor(Colors::BLACK);

	rightMarker_.setColor(Colors::BLACK);
	rightMarker_.setFillColor(Colors::BLACK);

	zero_.setColor(Colors::WHITE);
	zero_.setX(ZERO_X_);
	zero_.setY(ZERO_Y_);
	zero_.setWidth(ZERO_WIDTH_);
	zero_.setHeight(ZERO_HEIGHT_);
	zero_.setValue(ZERO_VALUE_);
	zero_.setPrecision(ONES);

	inspiratoryTimeXAxis_.setColor(Colors::BLACK);
	inspiratoryTimeXAxis_.setFillColor(Colors::GREEN);

	expiratoryTimeXAxis_.setColor(Colors::BLACK);
	expiratoryTimeXAxis_.setFillColor(Colors::YELLOW);
														// $[TI1]
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
HelpSubScreen::SoftFault(const SoftFaultID  softFaultID,
						 const Uint32       lineNumber,
						 const char*        pFileName,
						 const char*        pPredicate)  
{
	CALL_TRACE("HelpSubScreen::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, HELPSUBSCREEN,
							lineNumber, pFileName, pPredicate);
}
