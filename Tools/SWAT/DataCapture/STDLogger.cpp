//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file STDLogger.cpp
//----------------------------------------------------------------------------


//-------------------------------------
// I N C L U D E S
//-------------------------------------
#include "STDLogger.h"
#include < stdio.h    >
#include "gettimeofday.h"


//-------------------------------------
// C O D E
//-------------------------------------

//---------------------------
// STDLogger
//---------------------------
STDLogger::STDLogger(FILE *pfp):m_fp(pfp),m_bCreated(false){
    logMessage(0, LM_LOGINFO, "","LOGGING STARTED");
}


//---------------------------
// STDLogger
//---------------------------
STDLogger::STDLogger(char *pName):m_fp(stderr),m_bCreated(false){
    FILE *fp=fopen(pName,"a");

    if(fp){
        m_fp       = fp;
        m_bCreated = true;

        logMessage(0, LM_LOGINFO, "","LOGGING STARTED");
    }
}


//---------------------------
// ~STDLogger
//---------------------------
STDLogger::~STDLogger(){

    logMessage(0, LM_LOGINFO, "","LOGGING STOPED");

    fflush(m_fp);

    if(m_bCreated){
        fclose(m_fp);
    }
}





//---------------------------
// logMessage
//---------------------------
void STDLogger::logMessage(unsigned int line, int logtype, char *filename,
            char *str, ...){

    struct timeval tv;
    struct tm *tm;
    va_list args;
    time_t now;
    
    char log_date[300];
    char *tagname;

    gettimeofday(&tv, NULL);

    now = tv.tv_sec;
    tm = localtime(&now);

    sprintf(log_date, "%04d.%02d.%02d@%02d:%02d:%02d "
            , tm->tm_year + 1900
            , tm->tm_mon + 1
            , tm->tm_mday
            , tm->tm_hour
            , tm->tm_min
            , tm->tm_sec);


    switch(logtype)
    {
        case LM_DEBUG:     tagname="LM_DEBUG";break; 
        case LM_INFO:      tagname="LM_INFO";break; 
        case LM_TRACE:     tagname="LM_TRACE";break; 
        case LM_NOTICE:    tagname="LM_NOTICE";break; 
        case LM_WARNING:   tagname="LM_WARNING";break; 
        case LM_ERROR:     tagname="LM_ERROR";break; 
        case LM_CRITICAL:  tagname="LM_CRITICAL";break; 
        case LM_ALERT:     tagname="LM_ALERT";break; 
        default:
            tagname="LM_GENERAL";
    }

    char buffer[4000];
    va_start(args, str);
    vsnprintf(buffer, sizeof(buffer)-1, str , args);
    va_end(args);

    //Print out data
    if(logtype!=LM_LOGINFO){
        fprintf(m_fp,"%s %s %s@%i: %s\n",log_date,tagname,filename,line,buffer);
    }
    else{
        fprintf(m_fp,"%s %s: %s\n",log_date,"LM_LOGINFO",buffer);
    }
}

