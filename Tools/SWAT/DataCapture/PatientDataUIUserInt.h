//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file PatientDataUIUserInt.h
//----------------------------------------------------------------------------

#ifndef USERINT_H
#define USERINT_H

//-------------------------------------
// I N C L U D E S
//-------------------------------------
#include"PatientDataUIValue.h"


//-------------------------------------
// C L A S S  D E C L
//-------------------------------------

class UserInt:public Value
{
    public:
    UserInt(char *pName,int i=0);
    
    void updatePacket(void *address);
    bool updateValue(int iIn);
    int getIntValue();
    bool isInt();
    bool setInt(int iValue);

    protected:
    int m_iDesiredValue;
    int m_iValue;
};

#endif
