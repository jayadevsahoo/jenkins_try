#ifndef PidPressureController_HH
#define PidPressureController_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: PidPressureController - Implements a pressure controller using
//      a modified PID controller.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header::   /840/Baseline/Breath-Delivery/vcssrc/PidPressureController.hhv   10.7   08/17/07 09:41:30   pvcs  
//
//@ Modification-Log
//
//  Revision: 002  By:  syw    Date:  23-Jun-1999    DR Number: DCS 5447
//       Project:  ATC
//       Description:
//			Add DCS 5322 to header
//
//  Revision: 001  By:  syw    Date:  14-Jan-1999    DR Number: DCS 5322
//       Project:  ATC
//       Description:
//             ATC initial version.
//
//====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"

//@ Usage-Classes

class FlowController ;

//@ End-Usage

class PidPressureController {
  public:
    PidPressureController( FlowController& o2FlCtrl,
						   FlowController& airFlCtrl) ;
    ~PidPressureController( void) ;

    static void SoftFault(  const SoftFaultID softFaultID,
							const Uint32      lineNumber,
							const char*       pFileName  = NULL, 
			 				const char*       pPredicate = NULL) ;

	void newBreath( Real32 lungFlow, 
					Real32 kp, Real32 ki, Real32 kd,
					Real32 complianceFactor,
					Real32 pressureError);

    void updatePsol( Real32 pressureError,
					 Uint32 time) ;

	Real32 calculateFlowCommand( Real32 pressureError,
								 Uint32 time) ;

    inline Real32 getFlowCommandLimit(void) const;

	//These getters/setters are currently used only by the
    //GainsManager for runtime gains tuning

    //Returns the Kp gain
    Real32 getKp() { return kp_; }
    //Sets the Kp gain.
    inline void setKp( Real32 gain );

    //Returns the Ki gain
    Real32 getKi() { return ki_; }
    //Sets the Ki gain.
    inline void setKi( Real32 ki );

    //Returns the Kd gain
    Real32 getKd() { return kd_; }
    //Sets the Kd gain.
    inline void setKd( Real32 gain );

  protected:

  private:
    PidPressureController( const PidPressureController&) ;  			// not implemented...
    void   operator=( const PidPressureController&) ; 	// not implemented...

	//@ Data-Member: flowCommand_
	// stores current flow command
	Real32 flowCommand_ ;

	//@ Data-Member: flowCommandLimit_ 
    // maximum flow command in lpm
	Real32 flowCommandLimit_;

	//@ Data-Member: lastPressureError_
	// stores previous pressure error
	Real32 lastPressureError_ ;

    //@ Data-Member: ki_
	// integral gain in lpm/cmH2O-msec
	Real32 ki_;

	//@ Data-Member: kp_
    // proportional gain in lpm/cmH2O
	Real32 kp_;

	//@ Data-Member: kd_
    // derivative gain in lpm-msec/cmH2O
	Real32 kd_;

	//@ Data-Member: complianceFactor_
    // stores current compliance factor
	Real32 complianceFactor_ ;

	//@ Data-Member: rAirFlowController_
	// reference to air flow controller
	FlowController& rAirFlowController_ ;

    //@ Data-Member: rO2FlowController_
	// reference to O2  flow controller
	FlowController& rO2FlowController_ ;

#if CONTROLS_TUNING	
	//These parameters are currently used only by the
    //GainsManager for runtime gains tuning
    Boolean overrideKp_;
	Boolean overrideKi_;
	Boolean overrideKd_;
#endif
} ;

#include "PidPressureController.in"

#endif // PidPressureController_HH 









