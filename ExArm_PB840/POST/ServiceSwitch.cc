#include "stdafx.h"
#ifdef SIGMA_BD_CPU  
// This module is defined only for BD CPU

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================  M O D U L E   D E S C R I P T I O N ====
//@ Class: ServiceSwitch  -  Service Switch Power On Self Test 
//---------------------------------------------------------------------
//@ Interface-Description
//  This class contains methods to test and access the service switch
//  during POST.
//
//  In order to run EST, the service switch must be depressed by the
//  user during Phase 2 POST. In POST, after the Kernel tests complete, the
//  switch and associated latch is examined. They must both be open to
//  start.  If they are both open (not stuck), then the service
//  mechanism is armed by writing 1 to SERV_ACKIN.  If the operator
//  presses the switch anytime after the switch is armed the hardware
//  latches the result in a SRVLCK_OUT hardware bit.  The routine
//  WasPressed reads this bit to determine if the arming/pressing
//  sequence was performed.  At the end of POST the switch is unarmed.
//
//  If the switch is pressed during short POST, a MINOR failure is
//  issued since the ventilator must not enter Service-Mode during
//  powerfail recovery.  
//---------------------------------------------------------------------
//@ Rationale
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ Fault-Handling 
//    Faults during POST cause the test in progress to fail.
//---------------------------------------------------------------------
//@ Restrictions 
//    None
//---------------------------------------------------------------------
//@ Invariants 
//    None
//---------------------------------------------------------------------
//@ End-Preamble 
// 
//@ Version 
// @(#) $Header:   /840/Baseline/POST/vcssrc/ServiceSwitch.ccv   25.0.4.0   19 Nov 2013 14:17:06   pvcs  $ // 
//@ Modification-Log 
// 
//  Revision: 004    By: Yakov Blyakhman  Date: 16-APR-2002  DR Number: 5896
//    Project: 840 VCP
//    Description:
//       Added parentheses for better readability in two places (expression was correct).
// 
//  Revision: 003    By: Gary Cederquist  Date: 05-MAY-1999  DR Number: 5373
//    Project: 840 ATC
//    Description:
//       Deconflicted LONG enumerator by defining a "classed" enumerator
//		 for PostType.
// 
//  Revision: 002    By: Gary Cederquist  Date: 15-AUG-1997  DR Number: 1875
//    Project: Sigma (R8027)
//    Description:
//       Added testable item annotations.
//
//  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//=====================================================================

#include "ServiceSwitch.hh"
#include "BdIoUtil.hh"
#include "PostNovram.hh"
#include "AioDioDriver.h"


//=====================================================================
//
//  Public Methods
//
//=====================================================================

//============== M E T H O D  D E S C R I P T I O N ===================
//@ Method: TestAndArm(void)
//
//@ Interface-Description
//  Arm service switch if it is not stuck.  This method examines both
//  the switch and the latch, both must not be set.  If either are set,
//  it returns a MINOR failure.
//---------------------------------------------------------------------
//@ Implementation-Description
//  If the service switch is stuck then return MINOR else the service 
//  switch is armed.
//---------------------------------------------------------------------
//@ PreCondition 
//    none
//---------------------------------------------------------------------
//@ PostCondition 
//    none 
//@ End-Method
//=====================================================================
static const Uint8 SERVICE_SWITCH_PRESSED_BIT = 0x01;
static const Uint8 SERVICE_SWITCH_LATCHED_BIT = 0x04;
static const Uint8 ARM_SERVICE_SWITCH_MASK    = 0x02; 
static const Uint8 DISARM_SERVICE_SWITCH_MASK = 0x00;

static Boolean ServiceSwitchStuck_;

Criticality 
ServiceSwitch::TestAndArm(void)
{
    Boolean  switchClosed = ServiceSwitch::IsClosed_();
    Boolean  switchLatched = ServiceSwitch::IsLatched_();

UNIT_TEST_POINT(POSTUT_27_1)
    //  latch state will not be true until switch is armed so 
    //  we check if the switch is pressed or if the latch is set
    //  either condition is an error at this point
    if ( switchClosed || switchLatched )
    {
        // $[TI1.1]
        ServiceSwitchStuck_ = TRUE;
        return MINOR;
    }
    else
    {
        // $[TI1.2]
        ServiceSwitchStuck_ = FALSE;
        *SAFETY_NET_WRITE_REGA = ARM_SERVICE_SWITCH_MASK;
        return PASSED;
    }
}

//============== M E T H O D  D E S C R I P T I O N ===================
//@ Method: Disarm(void)
//
//@ Interface-Description
//  Disables the service mode switch latch.  If the latch was pressed
//  during the short POST preceding this call then this method returns
//  a MINOR failure.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition 
//    none
//---------------------------------------------------------------------
//@ PostCondition 
//    none 
//@ End-Method
//=====================================================================

Criticality
ServiceSwitch::Disarm(void)
{
    *SAFETY_NET_WRITE_REGA = DISARM_SERVICE_SWITCH_MASK;
    Boolean  switchLatched = ServiceSwitch::IsLatched_();
    Boolean  shortPost     = (Post::GetPostType() == Post::POST_SHORT);

UNIT_TEST_POINT(POSTUT_27_2)

    if ( switchLatched && shortPost )
    {
        // $[TI1.1]
        return MINOR;
    }
    else
    {
        // $[TI1.2]
        return PASSED;
    }
}

//============== M E T H O D  D E S C R I P T I O N ===================
//@ Method: IsServiceModeRequested(void)
//
//@ Interface-Description
//  Returns the state of the service mode request.  If the switch
//  was not latched prior to arming but is now latched, then return
//  TRUE else returns FALSE except during a short powerfail condition
//  which always returns FALSE.  This prevents entry into service mode
//  during a short powerfail.  $[11092] $[11073] 
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition 
//    none
//---------------------------------------------------------------------
//@ PostCondition 
//    none 
//@ End-Method
//=====================================================================

Boolean
ServiceSwitch::IsServiceModeRequested(void)
{
	extern AioDioDriver_t				AioDioDriver;

	Boolean isServiceModeRequeseted = AioDioDriver.IsServiceRequest();
	
	DEBUG_MSG( "isServiceModeRequeseted = " << ( (TRUE == isServiceModeRequeseted )? "YES" : "NO" ) );
	
	return isServiceModeRequeseted;
}

//=====================================================================
//
//  Private Methods
//
//=====================================================================

//============== M E T H O D  D E S C R I P T I O N ===================
//@ Method: IsClosed_(void)
//
//@ Interface-Description
//  Returns state of service mode switch.  It directly reads the 
//  hardware register containing the state of the service switch.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition 
//    none
//---------------------------------------------------------------------
//@ PostCondition 
//    none 
//@ End-Method
//=====================================================================
Boolean
ServiceSwitch::IsClosed_(void)
{
    // $[TI1.1]  $[TI1.2]
    return *SAFETY_NET_READ_REG & SERVICE_SWITCH_PRESSED_BIT;
}

//============== M E T H O D  D E S C R I P T I O N ===================
//@ Method: IsLatched_(void)
//
//@ Interface-Description
//  This method returns state of service switch latch.  It directly
//  reads the hardware register containing the state of the service
//  switch latch.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition 
//    none
//---------------------------------------------------------------------
//@ PostCondition 
//    none 
//@ End-Method
//=====================================================================
Boolean
ServiceSwitch::IsLatched_(void)
{
    // $[TI1.1]  $[TI1.2]
    return *SAFETY_NET_READ_REG & SERVICE_SWITCH_LATCHED_BIT;
}

#endif   // ifdef SIGMA_BD_CPU
