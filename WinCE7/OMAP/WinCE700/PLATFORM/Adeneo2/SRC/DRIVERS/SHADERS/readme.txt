==================================================================
         TI notes for Static Shaders 
==================================================================

The section after this one has the readme.txt for shaders.dll from Microsoft. There are a few issues with 
the instructions and the provided files that result in unsuccessful loading of static shaders. 

Most files in this directory were copied from C:\WINCE700\public\COMMON\oak\XamlRenderPlugin\OpenGL\shaders
and have the necessary changes and have been verified on DM3730 platform. 
The binaries are suitable for SGX530 Rev 125 (DM3730) and match DDK 1.7.17.xxxx and will not work
with Rev121 (Omap3530, AM3517) or with any other DDK version.

The files are checked in as reference in case a customer wants to make static shaders. By default 
the parent dirs directory will not include shaders.

To make static shaders for Rev121 or for other versions of DDK, 

1. Find the glslcompiler from the SDK release associated with the appropraite DDK release. 
2. Compile the .glsl files from a cmd prompt (example given below for SGX530 Rev 125). 
This will give the bins that are needed for shaders.dll to be built. 
glslcompiler_SGX530_125.exe defaulteffect.fs.glsl DefaultEffect.fs.bin -f
glslcompiler_SGX530_125.exe defaulteffect.vs.glsl DefaultEffect.vs.bin -v
3. Make sure the parents "dirs" has shaders enabled (by default it is disabled). When the BSP
is compiled shaders.dll will be built with the .bins generated above. 
4. Make sure that platform.bib has an explicit entry for shaders.dll. The common.bib entry 
with defaultshaders.dll is not enough to load the static shaders.

To verify that the static shaders are being loaded, generate a debug version of xrrendererplugin.

Edit C:\WINCE700\public\COMMON\oak\XamlRenderPlugin\OpenGL\debug.cpp and enable all
the debug zones. 
ZONE_ID_ERROR | ZONE_ID_WARNING | ZONE_ID_INFO | ZONE_ID_FUNCTION | ZONE_ID_SURFACE | ZONE_ID_RENDER | ZONE_ID_MEMORY | ZONE_ID_SHADERS | ZONE_ID_SERIALIZE | ZONE_ID_CONFIGS | ZONE_ID_VERBOSE

Open a build window from the project and go to 
C:\WINCE700\public\COMMON\oak\XamlRenderPlugin\OpenGL

set wincerel=1
set wincedebug=debug
build -c
This will copy the debug version of XRRendererOpenGL.dll into the FLAT release folder.
make runtime image from pb.

Load the BSP and run a silverlight application.
You should not see debug messge "Hardware supports runtime shader compilation". If you
see this message then it means that the static shader loading has failed and the code is 
attempting to do runtime compilation of the shaders.


==================================================================
Readme.txt from Microsoft (C:\WINCE700\public\COMMON\oak\XamlRenderPlugin\OpenGL\shaders)
==================================================================
         Sample source code for OpenGL ES SHADERS.DLL
==================================================================

This folder must be copied to the BSP and recompiled to 
support the platform's GPU.  This is sample source code only.


Embedded XamlRuntime may use OpenGL ES for graphics acceleration.

In addition to OpenGL ES 2.0 drivers, the BSP must provide a 
simple vertex and fragment shader.  These will draw textured 
rectangles on the screen (XamlRuntime UIElements where 
CacheMode="BitmapCache").

The shaders must be compiled for the platform's GPU, and linked 
into a file named Shaders.dll.  Windows Embedded Compact does not 
include a shader compiler.  Consult your hardware provider for 
instructions on compiling shaders for the target GPU.


Here is an example SOURCES file for Shaders.dll:

TARGETNAME  = Shaders
TARGETTYPE  = DYNLINK
RELEASETYPE = PLATFORM

NOEXPORTS=1
NOLIBC=1
NOCRTDLL=1
NODLLENTRY=1
DEFFILE=

INCLUDES = $(INCLUDES);\
           $(_PUBLICROOT)\common\oak\XAMLRenderPlugin\OpenGL

SOURCES =    \
    shaders.rc



Here is an example SHADERS.RC:

#include "shaders.h"
DefaultEffect.vs GL_VERTEX_SHADER_RT   "DefaultEffect.vs.bin"
DefaultEffect.fs GL_FRAGMENT_SHADER_RT "DefaultEffect.fs.bin"


NOTE: the BSP must provide shader binaries. Consult your hardware 
provider for instructions on compiling shader source to binary.  
Windows Embedded Compact does not include a shader compiler.



Here is an example vertex shader:

precision highp    float;
attribute vec4     inPosition;
attribute vec4     inDiffuse;
attribute vec2     inTextureCoordinate;

uniform   mat4     matViewProjection;
varying   vec2     textureCoordinate;
varying   vec4     diffuse;

void main()
{
   // Transform the vertex into screen space
   // XamlRuntime arbitrarily sets Z = 0.5
   // which is in front of the screen/near clip plane for OpenGL
   // Zero it.
   gl_Position = matViewProjection * vec4(inPosition.xy, 0.0, 1.0);

   // Pass to fragment shader
   diffuse              = inDiffuse;
   textureCoordinate    = inTextureCoordinate;
}



Here is an example fragment shader:

precision highp float;
uniform    sampler2D   inTexture1;
varying    vec2        textureCoordinate;
varying    vec4        diffuse;
uniform    bool        colorConversion;

void main(void)
{
   // XamlRuntime uses the diffuse color's Alpha channel to 
   // alter the opacity of textures
   vec4 color = diffuse * texture2D(inTexture1, textureCoordinate);

   // OpenGL drivers that don't support BGRA textures require
   // a per-pixel color conversion.
   // XamlRuntime sets colorConversion=1 when this is needed.
   // You should remove this test and code if you know your driver 
   // supports BGRA.
   if (colorConversion)
   {
       // Do BGRA -> RGBA color conversion
       float temp  = color.r;
       color.r     = color.b;
       color.b     = temp;
   }

   gl_FragColor = color;
}


If Shaders.dll is not found, or the shader binaries don't match 
the platform's GPU, Windows Embedded Compact will try to compile 
default shaders at runtime.  Note, however, that many OpenGL 
drivers will not support run-time compilation.


