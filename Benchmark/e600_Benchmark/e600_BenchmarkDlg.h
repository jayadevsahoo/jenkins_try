// e600_BenchmarkDlg.h : header file
//

#pragma once
#include "DataGen.h"
#include "MultiMediaTimer.h"

#ifdef DIRECT_DRAW
#include "DDrawCanvas.h"
#endif


#define  WM_WHEEL_UPDATE		WM_APP + 1
#define  WM_DATAGEN_TIMER		WM_APP + 2
#define  WM_MONITOR_REAL_TIMER	WM_APP + 3
#define  WM_MYINVALIDATE_TIMER	WM_APP + 4

// thread proc for detect knob event every 25ms
UINT KnobEventDetectThreadProc(LPVOID pParam);

// multi-media timer call back for generate data
void DataGenTimerCallback(UINT uTimerID, UINT uMsg, DWORD_PTR dwUser, DWORD_PTR dw1, DWORD_PTR dw2);

// multi-media timer call back for refresh monitor area
void MonitorRealTimerCallback(UINT uTimerID, UINT uMsg, DWORD_PTR dwUser, DWORD_PTR dw1, DWORD_PTR dw2);

// multi-media timer call back for control FPS
void InvalidateRealTimerCallback(UINT uTimerID, UINT uMsg, DWORD_PTR dwUser, DWORD_PTR dw1, DWORD_PTR dw2);

class Plot;

// Ce600_BenchmarkDlg dialog
class Ce600_BenchmarkDlg : public CDialog
{
	// Construction
public:
	Ce600_BenchmarkDlg(CWnd* pParent = NULL);	// standard constructor
	~Ce600_BenchmarkDlg();

	// Dialog Data
	enum { IDD = IDD_E600_BENCHMARK_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support

	// Implementation

protected:
	void cacheInvalidateRgn_(LPRECT lpRect);
	void drawMonitorData_( );
	void initMonitorsRect_( );
	void layoutButtons_();
	void prepareMonitorCanvas_( );

protected:

	HICON			m_hIcon;
	
	HIMAGELIST		m_hImageList;

	CRect			m_rcDesktop;

	// for cache the monitor area background
	CDC				m_dcMemMonitorBkgnd;
	CBitmap			m_bmpMonitorBkgnd;

	CDC				m_dcMemMonitorArea;		// device context for monitor area
	CBitmap			m_bmpMonitorArea;		// used to buffering the monitor area

	CDC				m_dcMemHeaderArea;		// device context for header area
	CBitmap			m_bmpHeaderArea;		// used to buffering the header area

	CBrush			m_bkGndBrush;			// brush for render background
	CPen			m_gridVerticalPen;		// pen for grid vertical lines
	CPen			m_gridHorizontalPen;	// pen for grid horizontal lines

	CRgn			m_GlobalInvalidateRgn;	// this region is used to cache the invalidated area
	CRgn			m_MonitorDataRgn;
	CButton*		m_pCurrentCheckBtn;		// current selected check button.

	int				m_monitorRows;			// monitors cell rows
	int				m_monitorColumns;		// monitors cell columns

	int				m_cellWidth;			// cell width in pixels
	int				m_cellHeight;			// cell height in pixels

	int				m_monitorsAreaHeight;	// the height pixels of monitors area

	int				m_baseX;				// monitors base X pos based on screen
	int				m_baseY;				// monitors base Y pos based on screen

	DWORD			m_dwTickCount1ST;		// used to record the first OnPaint time to caculate the FPS
	int				m_paintCount;			// how many times the OnPaint method is invoked

	bool			m_bIsLButtonDown;
	CPoint			m_ptDragOffset;
	CRect			m_rcCurrentDragIcon;

	DataGen			m_dataGen;				// for generate graph data
	MultiMediaTimer m_MMTimer;
	
	// vectors for cache the monitor cell title/data rectangles
	vector<CRect>	m_vMonitorCellTitles;
	vector<CRect>	m_vMonitorCellDatas;
	vector<Plot*>	m_Plots;

	deque<int>		m_data[PLOT_COUNT];
	deque<int>		m_oldData[PLOT_COUNT];

#ifdef DIRECT_DRAW
	// direct draw canvas
	DDrawCanvas*	m_pDDCanvas;
	LPDIRECTDRAWSURFACE	m_pMonitorSurface;
	LPDIRECTDRAWSURFACE m_pPrimarySurface;
#endif

	bool			m_bMonitorRedraw;
	DWORD			m_dwLastMonitorTime;
	CString			m_strMonitorDrawPeriod;


	// Generated message map functions
	virtual BOOL OnInitDialog();

#if defined(_DEVICE_RESOLUTION_AWARE) && !defined(WIN32_PLATFORM_WFSP)
	afx_msg void OnSize(UINT /*nType*/, int /*cx*/, int /*cy*/);
#endif

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnPaint();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);										// we just don't want windows erase background for us, we will manage the bkground draw manually
	afx_msg LRESULT OnVentWheelUpdate(WPARAM WheelCount, LPARAM WheelDir);		// handler for knob event from hardware
	afx_msg LRESULT OnDataGenTimer(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnMonitorRealTimer(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnMyInvalidateTimer(WPARAM wParam, LPARAM lParam);
	afx_msg void OnCheckButtons(UINT nId);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);						// support drag and drop operation
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
};
