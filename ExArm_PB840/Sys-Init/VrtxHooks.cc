#include "stdafx.h"
//=====================================================================
//   This is a proprietary work to which Puritan-Bennett corporation
//   of California claims exclusive right.  No part of this work may
//  be used, disclosed, reproduced, sorted in an information retrieval
//     system, or transmitted by any means, electronic, mechanical,
//   photocopying, recording, or otherwise without the prior written
//      permission of Puritan-Bennett Corporation of California.
//
//          Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: VrtxHooks - Intercept Routines to VRTX Tasking.
//---------------------------------------------------------------------
//@ Interface-Description
//  The VrtxHooks class provides application functions that VRTX 
//  calls when a task is created, deleted or when a task switch occurs.  
//  The class Initialize method registers these functions with the 
//  VRTX operating system.  When called by VRTX during task creation,
//  the TCreateHook_ method establishes a new stack area for the task
//  and changes the VRTX Task Control Block (TCB) stack pointer to the
//  new stack area.  This method uses the StackCheck class to gather 
//  statistics on stack usage.  The task delete and task switch hooks, 
//  though specified to VRTX at initialization, contain no functionality.  
//---------------------------------------------------------------------
//@ Rationale
//  VrtxHooks contains the application functions that VRTX calls 
//  during task creation, deletion and switching.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The class is implemented as a collection of static member functions
//  that are registered with VRTX as task hooks using the
//  sys_insert_hooks library call.  Once registered with VRTX, these
//  functions are called by VRTX during task creation, deletion, and
//  when a task switch occurs.  The task creation hook TCreateHook_ is
//  the only hook implemented for Sigma.  For tasks found in TaskTable,
//  the TCreateHook_ sets up the stack pointers in the task's Task
//  Control Block (TCB) to point to the assigned stack area designated
//  in the TaskTable.  StackCheck called by TCreateHook_ initializes the
//  stack area and maintains stack statistics for each task created.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  VRTX services may not be called from any VRTX task hook function.
//  This includes vconsole I/O, which is why call trace is disabled 
//  during their execution.
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Sys-Init/vcssrc/VrtxHooks.ccv   25.0.4.0   19 Nov 2013 14:34:00   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By:  gdc    Date:  12-Dec-2008    SCR Number: 6068
//  Project:  840S
//  Description:
//		Removed SUN prototype code.
//
//   Revision 003   By:   gdc      Date: 03/07/08    SCR Number: 6430
//      Project:   TREND2
//      Description:
//         Reduced number of "unmanaged" tasks to reduce memory usage.
//         Unmanaged tasks are not used except during development so
//         no need to take up more memory than required for minimal
//         development and debugging activities.
//
//   Revision 002   By:   sah      Date: 01/13/99    DR Number: 5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to a non-inlined method.
//
//   Revision 001   By:   gdc      Date: 07/06/95    DR Number:   n/a  
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//=====================================================================

//=====================================================================
//
//      Includes...
//
//=====================================================================

#include "VrtxHooks.hh"
#include <string.h>

// vrtxvisi does not declare a function prototype
extern "C" void sys_insert_hooks(void * c, void * d, void * s);


//@ Usage-Classes
#include "StackCheck.hh"
#include "Task.hh"
//@ End-Usage

//=====================================================================
//
//      Static Data...
//
//=====================================================================

//  local constants 

const Uint MAX_UNMANAGED_TASKS = 2;
const Uint DEFAULT_STACK_SIZE =  8*1024;
const Uint DEFAULT_SYSTEM_STACK_SIZE = 1*1024;


//  local typedefs

typedef struct 
{
    unsigned char    data[28];
} StackFrame;


typedef struct
{
    unsigned char    data[DEFAULT_STACK_SIZE];
} CombinedStack;


typedef struct
{
    TCB *            pTcb;
    unsigned char    id;
    unsigned char *  pSsp;
    unsigned char *  pUsp;
    unsigned char *  pStk;
} TcbPointers;


//@ Code...

//=====================================================================
//
//      Public Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TCreateHook_
//
//@ Interface-Description
//  The TCreateHook_ static method is called by VRTX when a task is 
//  created.  This method assigns a stack area for the new task and 
//  initializes its contents from the temporary stack provided by VRTX.
//  It invokes the StackCheck to track the task's stack utilization.
//
//  Each VRTX sc_tcreate call invokes the TCreateHook_ method.  This
//  includes calls to create Sigma and non-Sigma tasks.  All Sigma 
//  tasks are specified in the TaskTable and all have an associated 
//  task id and stack area.  TCreateHook_ references the TaskTable 
//  information through the TaskInfo class 
//  to obtain a stack area for these "managed" tasks.  For "unmanaged"
//  tasks such as debuggers, this method reserves one of the stacks
//  in the combinedStack memory.  Each task is either a user mode or
//  supervisory mode task as indicated by the supervisor bit in the task's
//  status register.  For supervisor mode tasks, the entire stack area
//  is reserved for the supervisor stack.  For user mode tasks, the 
//  stack area is split into a user stack and a small supervisor stack.
//  Upon completing this allocation, this method copies the contents
//  of the temporary stack provided by VRTX to the new stack area.  It
//  then adjusts the task's TCB pointers to reference the new stack 
//  area(s) and returns.
//---------------------------------------------------------------------
//@ Implementation-Description
//  References the supervisor bit in the task's status register (SR) 
//  located on the temporary stack to determine if a task is a 
//  supervisor or user mode task.  If the TCB stack base pointer is
//  not NULL, VRTX has already allocated a stack and this method exits.
//  If the task id indicates this task was started from an entry in 
//  the task table, the method uses the stack area and size contained
//  in the TaskInfo object for this task id.  For unmanaged tasks, the
//  next stack in the combinedStack area is allocated and assigned to
//  the task.  Splits the stack area into separate user and supervisor 
//  stacks for user mode tasks.  Maintains a single supervisor stack
//  for supervisor tasks.  Invokes the StackCheck class to initialize
//  the stack area and maintain stack usage statistics for the task.
//  Copies the temporary stack frame created by VRTX to the new 
//  supervisor stack and updates the stack pointers in the TCB.
//  VRTX services may not be called from any VRTX task hook function.
//  This includes vconsole I/O, which is why call trace is disabled
//  by TCreateHook_.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void 
VrtxHooks::TCreateHook_(TCB * pCreateeTcb, TCB * )
{
    static CombinedStack     combinedStack[MAX_UNMANAGED_TASKS];
    static CombinedStack *   pCombinedStack = combinedStack;

#ifdef TRACE_ON
    Boolean traceState = Trace::IsTraceOn();  // save trace state
    Trace::SetTraceOff();
#endif

    unsigned char * pStackTop;
    size_t          stackSize;

    StackFrame *  pOldSystemStackFrame = (StackFrame *)(pCreateeTcb->tbssp);

    //  Determine supervisor/user state of task at start-up
    Boolean supervisorModeTask = pOldSystemStackFrame->data[20] & 0x20;

    //  Allocate stack only if the OS has not allocated one already
    if (pCreateeTcb->tbstack == NULL) // $[TI1]
    {
    
        unsigned char  taskId  = pCreateeTcb->tbid;

        if (0 < taskId && taskId <= Task::GetTaskCount()) // $[TI1.1]
        {
            //  managed task - get info from TaskInfo
            pStackTop = Task::GetTaskInfo(taskId).getStack();
            stackSize = Task::GetTaskInfo(taskId).getStackSize();
            CLASS_ASSERTION(pStackTop != NULL);
            CLASS_ASSERTION(stackSize > 0);
            pCreateeTcb->tbssp = pStackTop + stackSize;
        }
        else // $[TI1.2]
        {
            //  unmanaged task
            pStackTop = (unsigned char *)pCombinedStack;
            stackSize = sizeof(CombinedStack);
            CLASS_ASSERTION( (unsigned char*)pCombinedStack 
                    < (unsigned char*)combinedStack + sizeof(combinedStack))
            pCreateeTcb->tbssp = (unsigned char *)(++pCombinedStack);
        }

        if (!supervisorModeTask) // $[TI1.3]
        {
	    // Allocate the last DEFAULT_SYSTEM_STACK_SIZE bytes of the
	    // combined stack for the system stack.  The remainder of the
	    // combined stack is reserved for the user stack.

            pCreateeTcb->tbusp = pCreateeTcb->tbssp - DEFAULT_SYSTEM_STACK_SIZE;

            //  Register the stack area with StackCheck to later 
            //  determine the stack usage/overflow conditions.
            StackCheck::Register(taskId, pStackTop, 
                                 stackSize-DEFAULT_SYSTEM_STACK_SIZE,
                                 pCreateeTcb->tbusp, 
                                 DEFAULT_SYSTEM_STACK_SIZE);
        
        }
        else // $[TI1.4]
        {
            //  For supervisor tasks, the entire stack area is 
            //  reserved for the supervisor stack.

            //  Register the stack area with StackCheck to later 
            //  determine the stack usage/overflow conditions.
            StackCheck::Register(taskId, NULL, 0,
                                 pStackTop,
                                 stackSize);
        }

        //  copy stack frame contents from the temporary stack to the new stack
        StackFrame *  pNewSystemStackFrame = (StackFrame *)(pCreateeTcb->tbssp);
        memcpy( --pNewSystemStackFrame, pOldSystemStackFrame, sizeof(StackFrame) );
        pCreateeTcb->tbssp = (unsigned char *)pNewSystemStackFrame;
    }
    // $[TI2]

#ifdef SIGMA_DEBUG
    // debugging information
    static TcbPointers     TcbData[30];
    static TcbPointers *   pTcbData = TcbData;

    pTcbData->pTcb   = pCreateeTcb;
    pTcbData->id     = pCreateeTcb->tbid;
    pTcbData->pSsp   = pCreateeTcb->tbssp;
    pTcbData->pUsp   = pCreateeTcb->tbusp;
    pTcbData->pStk   = pCreateeTcb->tbstack;
    ++pTcbData;
#endif

#ifdef TRACE_ON
    traceState ? Trace::SetTraceOn() : Trace::SetTraceOff();
#endif

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TDeleteHook_
//
//@ Interface-Description
//  TDeleteHook_ is called from VRTX when a task switch occurs.  This 
//  method completes the specification to the VRTX sys_insert_hooks 
//  call, but provides no run-time functionality.  It simply returns 
//  when called.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
   

void 
VrtxHooks::TDeleteHook_(TCB *, TCB *)
{
    // no code
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TSwitchHook_
//
//@ Interface-Description
//  TDeleteHook_ is called from VRTX when a task switch occurs.  This 
//  method completes the specification to the VRTX sys_insert_hooks 
//  call, but provides no run-time functionality.  It simply returns 
//  when called.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void 
VrtxHooks::TSwitchHook_(TCB *, TCB *)
{
    // no code
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Initialize
//
//@ Interface-Description
//  VrtxHooks Initialize method called by Sys_Init::Create prior to
//  VRTX tasking calls sys_insert_hooks to insert the VrtxHooks methods
//  called by VRTX during task create, delete, and switch operations.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls the sys_insert_hooks library function to add TCreateHook_,
//  TDeleteHook_, and TSwitchHook_ to the call list for these operations.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
VrtxHooks::Initialize(void)
{
    // This method is called from Sys_Init prior to tasking

    (void)    sys_insert_hooks( (void *)VrtxHooks::TCreateHook_, 
                                (void *)VrtxHooks::TDeleteHook_, 
                                (void *)VrtxHooks::TSwitchHook_);
    // $[TI1]
}

 

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//@ Interface-Description
//   This method is called when a software fault is detected by the
//   fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//   'lineNumber' are essential pieces of information.  The 'pFileName'
//   and 'pPredicate' strings may be defaulted in the macro to reduce
//   code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This method receives the call for the SoftFault, adds it sub-system
//   and class name ID and sends the message to the non-member function
//   SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
 
void
VrtxHooks::SoftFault(const SoftFaultID  softFaultID,
                         const Uint32       lineNumber,
                         const char*,
                         const char*)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
 
  asm(" ILLEGAL ");  // remove after appropriate fault handler developed

  FaultHandler::SystemSoftFault(softFaultID, SYS_INIT,
                                Sys_Init::ID_VRTXHOOKS, 
                                lineNumber);
}


//=====================================================================
//
//      Protected Methods...
//
//=====================================================================

//=====================================================================
//
//      Private Methods...
//
//=====================================================================

