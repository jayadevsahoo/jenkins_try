#ifndef SmTestId_HH
#define SmTestId_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: SmTestId - Defines master and subset test IDs and ID
//                   remapping functions for all Service Mode tests
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/SmTestId.hhv   25.0.4.0   19 Nov 2013 14:23:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 021  By: mnr    Date: 24-Mar-2010     DR Number: 6436
//  Project:  PROX4
//  Description:
//      PROX SST related updates.
//
//  Revision: 020  By: mnr    Date: 28-Dec-2009     SCR Number: 6437
//  Project:  NEO
//  Description:
//      Added support for Advanced and Lockout options.
//
//  Revision: 019  By:  rhj    Date:  04-Apr-2009    SCR Number: 6495 
//  Project:  840S2
//  Description:
//		Added FORCE_EST_UNDEFINED_ID, ENABLE_SINGLE_TEST_ID and
//      DISABLE_SINGLE_TEST_ID.
//
//  Revision: 018  By:  gdc    Date:  18-Feb-2009    SCR Number: 6480
//  Project:  840S
//  Description:
//		Implemented "set vent head operational hours" test.
//
//  Revision: 017  By: rhj    Date: 20-Oct-2008     SCR Number: 6435
//  Project: 840S
//  Description:
//  Added ENABLE_LEAK_COMP_OPTION and DISABLE_LEAK_COMP_OPTION enum.
// 
//  Revision: 016  By: gdc    Date: 26-Mar-2007     SCR Number: 6237 
//  Project: Trend
//  Description:
//    Added compact flash test.
//
//  Revision: 015  By: erm    Date: 19-Dec-2002     DR Number: 6028 
//  Project: GuiComms
//  Description:
//    Added MFG_PROD_KEY_CONVERT_ID to be used to convert
//    Mfg Datakey to Production Datakeys.
//
//  Revision: 014  By: erm    Date: 23-Jan-2002     DR Number: 5984
//  Project: GuiComms
//  Description:
//    Added SERIAL_LOOBACK_TEST_ID to be used during External loopback
//    test.
//
//  Revision: 013  By: gdc    Date: 28-Aug-2000     DR Number: 5753
//  Project:  Delta
//  Description:
//      Implemented Single Screen option.
//
//  Revision: 012  By: sah    Date: 17-Jul-2000     DR Number: 5755
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      *  added ENABLE/DISABLE directive for VTPC option
//
//  Revision: 011   By: sah   Date:  23-May-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//      PAV project-related changes:
//      *  added ENABLE/DISABLE directive for PAV option
//
//  Revision: 010  By: sah    Date: 06-Jul-1999     DR Number: 5424
//  Project:  NeoMode
//  Description:
//      Added capability for circuit type-specific override of SST.
//      Added ability to disable/enable individual software options.
//
//  Revision: 009  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 008  By:  syw    Date: 27-Aug-1998    DR Number:
//       Project:  BiLevel
//       Description:
//          BiLevel Initial Release.  Added DK_COPY_HOURS_ID and
//			REMOTE_TEST_DK_COPY_HOURS_ID.
//
//  Revision: 007  By:  gdc    Date: 30-Sep-1997    DR Number: DCS 1985
//       Project:  Sigma (840)
//       Description:
//            Assigned specific values to enumerators.
//
//  Revision: 006  By:  gdc    Date: 23-Sep-1997    DR Number: DCS 2513
//       Project:  Sigma (840)
//       Description:
//            Added remote tests to support service/manufacturing tests.
//
//  Revision: 005  By:  quf    Date: 29-Aug-1997    DR Number: DCS 2435
//       Project:  Sigma (840)
//       Description:
//            Added DEFAULT_FLASH_ID used for creating a default exh
//            valve calibration table.
//
//  Revision: 004  By:  quf    Date: 28-Jul-1997    DR Number: DCS 2205, 2013
//       Project:  Sigma (840)
//       Description:
//			Add ID's for EST nurse call and EST/remote EV velocity xducer tests.
//
//  Revision: 003  By:  syw    Date: 23-Jul-1997    DR Number: DCS 2279
//       Project:  Sigma (840)
//       Description:
//			Add FlowSensorCalibration test.
//
//  Revision: 002  By:  quf    Date: 22-May-1997    DR Number: DCS 2131
//       Project:  Sigma (840)
//       Description:
//            Modification-Log fix.
//            Removed SM_TEST-compiled master test IDs EV_COMMAND_TEST_ID
//            and MY_TEST_ID.
//
//  Revision: 001  By:  mad    Date:  08-Nov-95    DR Number: none
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//
//====================================================================

#ifndef SIGMA_LAPTOP
	#include "ServiceMode.hh"
#endif


class SmTestId
{
	public:
		//@ Type: ServiceModeTestId
		// List of all Service Mode tests
		enum ServiceModeTestId
		{
			TEST_NOT_START_ID = -1
			, INITIALIZE_FLOW_SENSORS_ID = 0
			, GAS_SUPPLY_TEST_ID = 1
			, CIRCUIT_RESISTANCE_TEST_ID = 2
			, SST_FILTER_TEST_ID = 3
			, GUI_KEYBOARD_TEST_ID = 4
			, GUI_KNOB_TEST_ID = 5
			, GUI_LAMP_TEST_ID = 6
			, BD_LAMP_TEST_ID = 7
			, GUI_AUDIO_TEST_ID = 8
			, BD_AUDIO_TEST_ID = 9
			, FS_CROSS_CHECK_TEST_ID = 11
			, SST_FS_CC_TEST_ID = 12
			, LOW_FLOW_FS_CC_TEST_ID = 13
			, HIGH_FLOW_FS_CC_TEST_ID = 14
			, CIRCUIT_PRESSURE_TEST_ID = 15
			, SAFETY_SYSTEM_TEST_ID = 16
			, EXH_VALVE_SEAL_TEST_ID = 18
			, EXH_VALVE_TEST_ID = 19
			, EXH_VALVE_CALIB_TEST_ID = 20
			, SM_LEAK_TEST_ID = 21
			, SST_LEAK_TEST_ID = 22
			, COMPLIANCE_CALIB_TEST_ID = 23
			, EXH_HEATER_TEST_ID = 24
			, GENERAL_ELECTRONICS_TEST_ID = 28
			, GUI_TOUCH_TEST_ID = 29
			, GUI_SERIAL_PORT_TEST_ID = 30
			, BATTERY_TEST_ID = 31
			, BD_INIT_SERIAL_NUMBER_ID = 32
			, GUI_INIT_SERIAL_NUMBER_ID = 33
			, CAL_INFO_DUPLICATION_ID = 34
			, BD_VENT_INOP_ID = 35
			, GUI_VENT_INOP_ID = 36
			, RESTART_BD_ID = 37
			, RESTART_GUI_ID = 38
			, BD_INIT_DEFAULT_SERIAL_NUMBER_ID = 39
			, GUI_INIT_DEFAULT_SERIAL_NUMBER_ID = 40
			, FS_CALIBRATION_ID = 42
			, GUI_NURSE_CALL_TEST_ID = 43
			, ATMOS_PRESSURE_READ_ID = 44
			, SET_COMPRESSOR_SN_ID = 45
			, SET_COMPRESSOR_TIME_ID = 46
			, SET_DATAKEY_TO_VENT_DATA_ID = 47
			, SET_AIR_FLOW_ID = 48
			, SET_O2_FLOW_ID = 49
			, PURGE_WITH_AIR_ID = 50
			, PURGE_WITH_O2_ID = 51
			, ATMOS_PRESSURE_CAL_ID = 52
			, DK_COPY_HOURS_ID = 53
			, SERIAL_LOOPBACK_TEST_ID = 54
			, COMPACT_FLASH_TEST_ID = 55
			, SET_VENT_HEAD_TIME_ID = 56
			, SM_SST_PROX_TEST_ID = 57
			, NUM_SERVICE_TEST_ID
		};

		//@ Type: ServiceModeTestTypeId
		// List of the Service Mode test types
		enum ServiceModeTestTypeId
		{
			SM_TYPE_NOT_SET = -1
			, SM_EST_TYPE = 0
			, SM_SST_TYPE = 1
			, SM_EXTERNAL_TYPE = 2
			, SM_MISC_TYPE = 3

			, NUM_SM_TEST_TYPE
		};

		//@ Type: EstTestId
		// List of EST tests
		enum EstTestId
		{
			EST_NO_TEST_ID = -1
			, EST_TEST_START_ID = 0
			, EST_TEST_CIRCUIT_PRESSURE_ID = EST_TEST_START_ID
			, EST_TEST_FS_CROSS_CHECK_ID = 1
			, EST_TEST_GAS_SUPPLY_ID = 2
			, EST_TEST_SM_LEAK_ID = 3
			, EST_TEST_GUI_KEYBOARD_ID = 4
			, EST_TEST_GUI_KNOB_ID = 5
			, EST_TEST_GUI_LAMP_ID = 6
			, EST_TEST_BD_LAMP_ID = 7
			//To do E600 tests needs to be revisted for sound class
			, EST_TEST_GUI_AUDIO_ID = 8
			, EST_TEST_BD_AUDIO_ID = 9
			, EST_TEST_SAFETY_SYSTEM_ID = 11
			, EST_TEST_EXH_VALVE_SEAL_ID = 13
			, EST_TEST_EXH_VALVE_ID = 14
			, EST_TEST_EXH_HEATER_ID = 15
			, EST_TEST_GENERAL_ELECTRONICS_ID = 18
			, EST_TEST_GUI_TOUCH_ID = 19
			, EST_TEST_GUI_SERIAL_PORT_ID = 20
			, EST_TEST_BATTERY_ID = 21
			, EST_TEST_GUI_NURSE_CALL_ID = 23
			/*E600
			We may want to keep the numbers intact,  
			as these numbers are possibly used in some external tools (like VTS). 
			Do not worried about the extra memory we use for those tests that got removed, 
			but we have to deal with those "empty slots" in the test arrays properly.
			Just dont use EST_TEST_MAX to count the number of tests.
			*/
			, EST_TEST_MAX
		};

		//@ Type: SstTestId
		// List of SST tests
		enum SstTestId
		{
			SST_NO_TEST_ID = -1
			, SST_TEST_START_ID = 0
			, SST_TEST_FLOW_SENSORS_ID = SST_TEST_START_ID
			, SST_TEST_CIRCUIT_PRESSURE_ID = 1
			, SST_TEST_CIRCUIT_LEAK_ID = 2
			, SST_TEST_EXPIRATORY_FILTER_ID = 3
			, SST_TEST_CIRCUIT_RESISTANCE_ID = 4
			, SST_TEST_COMPLIANCE_CALIBRATION_ID = 5
			, SST_TEST_PROX_ID = 6

			, SST_TEST_MAX
		};

		//@ Type: RemoteTestId
		// List of remote tests
		enum RemoteTestId
		{
			REMOTE_TEST_NO_START_ID = -1
			, REMOTE_TEST_GAS_SUPPLY_ID = 0
			, REMOTE_TEST_GUI_KEYBOARD_ID = 1
			, REMOTE_TEST_GUI_KNOB_ID = 2
			, REMOTE_TEST_GUI_LAMP_ID = 3
			, REMOTE_TEST_BD_LAMP_ID = 4
			, REMOTE_TEST_GUI_AUDIO_ID = 5
			, REMOTE_TEST_BD_AUDIO_ID = 6
			, REMOTE_TEST_LOW_FLOW_FS_CC_ID = 8
			, REMOTE_TEST_HIGH_FLOW_FS_CC_ID = 9
			, REMOTE_TEST_CIRCUIT_PRESSURE_ID = 10
			, REMOTE_TEST_SAFETY_SYSTEM_ID = 11
			, REMOTE_TEST_EXH_VALVE_SEAL_ID = 13
			, REMOTE_TEST_EXH_VALVE_ID = 14
			, REMOTE_TEST_EXH_VALVE_CALIB_ID = 15
			, REMOTE_TEST_SM_LEAK_ID = 16
			, REMOTE_TEST_EXH_HEATER_ID = 17
			, REMOTE_TEST_GENERAL_ELECTRONICS_ID = 20
			, REMOTE_TEST_GUI_TOUCH_ID = 21
			, REMOTE_TEST_GUI_SERIAL_PORT_ID = 22
			, REMOTE_TEST_BATTERY_ID = 23
			, REMOTE_TEST_FS_CALIBRATION_ID = 24
			, REMOTE_TEST_GUI_NURSE_CALL_ID = 26
			, REMOTE_TEST_ATMOS_PRESSURE_READ_ID = 27
			, REMOTE_TEST_SET_COMPRESSOR_SN_ID = 28
			, REMOTE_TEST_SET_COMPRESSOR_TIME_ID = 29
			, REMOTE_TEST_SET_DATAKEY_TO_VENT_DATA_ID = 30
			, REMOTE_TEST_SET_AIR_FLOW_ID = 31
			, REMOTE_TEST_SET_O2_FLOW_ID = 32
			, REMOTE_TEST_PURGE_WITH_AIR_ID = 33
			, REMOTE_TEST_PURGE_WITH_O2_ID = 34
			, REMOTE_TEST_DK_COPY_HOURS_ID = 35
			, REMOTE_TEST_SET_VENT_HEAD_TIME_ID = 36
			/*E600
			We may want to keep the numbers intact,  
			as these numbers are possibly used in some external tools (like VTS). 
			Do not worried about the extra memory we use for those tests that got removed, 
			but we have to deal with those "empty slots" in the test arrays properly.
			Just dont use REMOTE_TEST_MAX to count the number of tests.
			*/
			, REMOTE_TEST_MAX
		};

		//@ Type: FuntionId
		// List of Service Mode functions
		enum FunctionId
		{
			FUNCTION_ID_START = -1
			, EST_PASSED_ID = 0
			, SET_TEST_TYPE_EST_ID = 1
			, SET_TEST_TYPE_SST_ID = 2
			, SET_TEST_TYPE_REMOTE_ID = 3
			, SET_TEST_TYPE_MISC_ID = 4
			, EST_OVERRIDE_ID = 5
			, SST_OVERRIDE_ID = 6
			, EST_COMPLETE_ID = 7
			, SST_COMPLETE_ID = 8
			, BD_DOWNLOAD_ID = 9
			, GUI_DOWNLOAD_ID = 10
			, FORCE_EST_OVERIDE_ID = 11
			, FORCE_SST_PED_OVERIDE_ID = 19
			, FORCE_SST_ADULT_OVERIDE_ID = 12
			, FORCE_SST_NEO_OVERIDE_ID = 20
			, ENABLE_BILEVEL_OPTION = 13
			, ENABLE_ATC_OPTION = 14
			, ENABLE_NEO_MODE_OPTION = 21
			, ENABLE_VTPC_OPTION = 25
			, ENABLE_PAV_OPTION = 26
			, DISABLE_BILEVEL_OPTION = 22
			, DISABLE_ATC_OPTION = 23
			, DISABLE_NEO_MODE_OPTION = 24
			, DISABLE_VTPC_OPTION = 27
			, DISABLE_PAV_OPTION = 28
			, DEFAULT_FLASH_ID = 15
			, DELETE_SYSTEM_DIAGNOSTIC_LOG_ID = 16
			, DELETE_SYSTEM_INFORMATION_LOG_ID = 17
			, DELETE_SELFTEST_LOG_ID = 18
			, ENABLE_RESERVED1_OPTION = 29
			, DISABLE_RESERVED1_OPTION = 30
			, RESET_SOFTWARE_OPTIONS = 31
			, MFG_PROD_KEY_CONVERT_ID = 32
			, ENABLE_RESP_MECH_OPTION = 33
			, ENABLE_TRENDING_OPTION = 34
			, DISABLE_RESP_MECH_OPTION = 35
			, DISABLE_TRENDING_OPTION = 36
			, ENABLE_LEAK_COMP_OPTION  = 37
			, DISABLE_LEAK_COMP_OPTION = 38
			, ENABLE_NEO_MODE_UPDATE_OPTION  = 39
			, DISABLE_NEO_MODE_UPDATE_OPTION = 40
			, ENABLE_NEO_MODE_ADVANCED_OPTION  = 41
			, DISABLE_NEO_MODE_ADVANCED_OPTION = 42
			, ENABLE_NEO_MODE_LOCKOUT_OPTION  = 43
			, DISABLE_NEO_MODE_LOCKOUT_OPTION = 44
			, FORCE_EST_UNDEFINED_ID = 45
			, ENABLE_SINGLE_TEST_ID = 46
			, DISABLE_SINGLE_TEST_ID = 47
			, FUNCTION_ID_MAX = 48
		};


#ifndef SIGMA_LAPTOP
		static void Initialize(void);

		static EstTestId GetEstIdFromSmId(ServiceModeTestId smId);
		static SstTestId GetSstIdFromSmId(ServiceModeTestId smId);
		static RemoteTestId GetRemoteIdFromSmId(ServiceModeTestId smId);

		static ServiceModeTestId GetSmIdFromEstId(EstTestId estId);
		static ServiceModeTestId GetSmIdFromSstId(SstTestId sstId);
		static ServiceModeTestId GetSmIdFromRemoteId(RemoteTestId sstId);

		static Boolean IsGuiTest(ServiceModeTestId smId);

		static void  SoftFault(const SoftFaultID softFaultID,
							   const Uint32      lineNumber,
							   const char*       pFileName  = NULL,
							   const char*       pPredicate = NULL);

	private:                
		SmTestId( void);					// not implemented...
		~SmTestId( void);					// not implemented...
		SmTestId( const SmTestId&);		// not implemented...
		void operator=( const SmTestId&);	// not implemented...

		//@ Data-Member: EstTestId_
		// Mapping array for EST tests
		static SmTestId::ServiceModeTestId EstTestId_[EST_TEST_MAX];

		//@ Data-Member: SstTestId_
		// Mapping array for SST tests
		static SmTestId::ServiceModeTestId SstTestId_[SST_TEST_MAX];

		//@ Data-Member: RemoteTestId_
// Mapping array for remote tests
		static SmTestId::ServiceModeTestId RemoteTestId_[REMOTE_TEST_MAX];
#endif // !SIGMA_LAPTOP
};


#endif // SmTestId_HH 
