#ifndef TrendDataValues_HH
#define TrendDataValues_HH


//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation 
//=====================================================================

//=====================================================================
// Class: TrendDataValues.hh - Encapsulation of an arrray of Trend Values
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Trend-Database/vcssrc/TrendDataValues.hhv   25.0.4.0   19 Nov 2013 14:34:36   pvcs  $
//
//@ Modification-Log
// 
//  Revision: 002   By: gdc    Date: 26-Jan-2009    SCR Number: 6407
//  Project:  840S
//  Description:
//      Modified to bring source into strict ARM compliance.
//
//  Revision: 001  By:  mjf    Date:  22-Aug-2006    SCR Number: 6237
//       Project:  Trend
//       Description:
//             Initial version
// 
//=====================================================================
//@ Usage-Classes
//@ End-Usage

#include "PatientDataId.hh"       // Patient-Data
#include "SettingId.hh"           // Setting-Validation
#include "TrendSelectValue.hh"    // Setting-Validation


#include "TrendDataMgr.hh"
#include "TrendValue.hh"


class TrendDataValues
{
public:    

	// Constructor/destructor
	TrendDataValues( void );
	~TrendDataValues( void );

	inline TrendSelectValue::TrendSelectValueId getId( void ) const;
	inline const TrendValue & getValue( Uint16 row ) const;
	inline Boolean isValid( void ) const;


	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

private:

	// Give special "friend class" access to the Data Manager and Repositories, so
	// that they can load the trend data into the set.
	friend class TrendDataMgr;
	friend class TrendDbPatientRepository;
	friend class TrendDbSettingRepository;

	void setId_( TrendSelectValue::TrendSelectValueId trendId );
	void setValue_( Uint16 row, const TrendValue & trendValue );


	// no copy or assignment constructor
	TrendDataValues( const TrendDataValues& );	// no implemented
	void operator=( const TrendDataValues& );		// no implemented

	//@ Data-member: trendId_
	// The Trend selct Id for this column od trend values
	TrendSelectValue::TrendSelectValueId	trendId_;

	//@ Data-Member: valueArray_
	// The array of trend values fopr this column
	TrendValue          valueArray_[ TrendDataMgr::MAX_ROWS ];
};


// Inlined methods
#include "TrendDataValues.in"

#endif // TrendDataValues_HH
