#ifndef IpcTable_HH
#define IpcTable_HH
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1994, Puritan-Bennett Corporation
//=====================================================================
 
// =========================== M O D U L E   D E S C R I P T I O N ====
//@ Class: IpcTable - Sys-Init subsystem class
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Sys-Init/vcssrc/IpcTable.hhv   25.0.4.0   19 Nov 2013 14:33:54   pvcs  $
//
//@ Modification-Log
//
//   Revision 002   By:   sah      Date: 01/13/99    DR Number: 5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001  By:  gdc      Date:  08-JUN-95    DR Number:
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//=====================================================================
 
#if 0  
// for bug in genDD
#endif

//@ Usage-Classes
#include "Sys_Init.hh"
//@ End-Usage
 
//  struct for an entry in the IpcTable
struct IpcTableEntry
{
    Int32 id;
    Int32 type;
    Int32 length;
    const char * name;
};
 
 
class IpcTable
{
  public:
 
	static const Uint32 MAX_Q_NAME_STRING_LENGTH = 50;

    static void                     Initialize(void);
    static void                     Print(void);

    static const IpcTableEntry &    GetIpcTableEntry(const Uint32 index);
    static const IpcTableEntry &    GetEntryById(const Int32 id);
    static Int32                    GetNumEntries(void);
    static Boolean                  IsInitialized(void);
 
    static void     SoftFault(const SoftFaultID softFaultID,
			      const Uint32      lineNumber,
			      const char*       pFileName  = NULL,
			      const char*       pPredicate = NULL);
 
  private:
    IpcTable();      // declared only
    ~IpcTable();     // declared only

    IpcTable(const IpcTable&);        // declared only
    void operator=(const IpcTable&);  // declared only

    //@ Data-Member:    NumEntries_
    //  Number of entries in the Table.
    static const Uint               NumEntries_;

    //@ Data-Member:    Table_
    //  Array of IpcTableEntry's.
    static const IpcTableEntry  Table_[];

    //@ Data-Member:    PTableById_
    //  Array containing pointers to IpcTableEntry indexed by 
    //  the IPC's identifier.
    static const IpcTableEntry *PTableById_[];

    //@ Data-Member:    IsInitialized_
    //  Is the IpcTable class initialized.
    static Boolean              IsInitialized_;
 
};
 

#endif   // IpcTable_HH
