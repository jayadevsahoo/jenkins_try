#ifndef FlowSensorOffset_HH
#define FlowSensorOffset_HH
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
// Class: FlowSensorOffset - This class is the interface between the
//		FlowSensor class and the NovRamManager for retrieval and updates
//		of the flow offsets.
//---------------------------------------------------------------------
// @ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/FlowSensorOffset.hhv   26.0.1.0   04 Feb 2013 10:36:40   pvcs  $
//
// @ Modification-Log
//
//  Revision: 002  By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 001  By:  syw    Date:  27-Aug-97    DR Number: 2279
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//=====================================================================

#include "Sigma.hh"
#include "BD_IO_Devices.hh"

const Uint32 MAX_FLOW_CALIBRATION = 200 + 1 ;	// store 0 to 200 inclusive

class FlowSensorOffset
{
    public:

        FlowSensorOffset( void );
        ~FlowSensorOffset( void );

        FlowSensorOffset( const FlowSensorOffset& rFlowSensorOffset );
        void operator=( const FlowSensorOffset& rFlowSensorOffset );        

        static void SoftFault( const SoftFaultID softFaultID,
							   const Uint32      lineNumber,
							   const char*       pFileName  = NULL,
							   const char*       pPredicate = NULL ) ;

		inline void setOffset( const Uint32 index, const Real32 offset) ;
		Real32 getOffset( const Uint32 index) ;
		
    protected:

    private:

	//@ Data-Member: offset[MAX_FLOW_CALIBRATION]
	// offset in exhalation flow
	Real32 offset_[MAX_FLOW_CALIBRATION] ;
} ;

// Inlined methods...
#include "FlowSensorOffset.in"

#endif  //FlowSensorOffset_HH
