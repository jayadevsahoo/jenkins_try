#include "stdafx.h"

//======================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//======================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: AdjustedContext - Context for the Adjusted Batch Settings.
//---------------------------------------------------------------------
//@ Interface-Description
//  A context is a storage pool for different kinds of setting values.  The
//  settings, themselves, do not contain their values, the contexts do.  This
//  context is used for batch setting changes only.  Only batch setting
//  values are stored in this context, and the methods provided by this
//  context are for the adjustment of those settings.  This context contains
//  setting values that the user is adjusting, but hasn't yet accepted.
//
//  This context provides methods for readying the settings system for the
//  adjustment of various batch settings.  When an adjustment of any, or
//  all, of the batch settings is initiated, the setting values from the
//  Accepted Context that are different from the current values of this
//  context are copied into this context.  Those that are copied will 
//  activate any callbacks that are registered for changes of this context.
//
//  Various batch acceptances cause the storage of the previous batch of
//  accepted setting into the Recoverable Context.  The Recoverable Context
//  provides a means of recovering a previous batch of settings.  When a
//  recovery is initiated, this context gets a copy all of the values from
//  the Recoverable Context that are different from the current values of
//  this context.  Again, those values that are copied will activate any
//  callbacks that are registered for changes of this context.
//
//  Other functional responsibilities of this context include the
//  "correction" of the apnea setting values, and the recovery of the
//  setting that is currently being adjusted.
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for the adjusted context.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Privately, this context contains an instance of 'BatchSettingValues'
//  to store all of the values managed by this context.  This values
//  manager has a callback mechanism that allows for the registering of
//  callbacks that are activated upon changes to itself.  This callback
//  mechanism allows the Adjusted Context to monitor the activity of all
//  of its settings easily, despite the fact that there are numerous 
//  means by which a change of a setting value can be initiated via this
//  context.  By using the callback mechanism of the 'BatchSettingValues'
//  instance, the Adjusted Context can "piggy-back" its callback activation
//  to the activation of its internal values manager; that is, callbacks
//  that are registered against the Adjusted Context are activated via the
//  callback of the internal instance of the 'BatchSettingsValues'.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/AdjustedContext.ccv   25.0.4.0   19 Nov 2013 14:27:10   pvcs  $
//
//@ Modification-Log
//
//  Revision: 022   By: mnr    Date: 28-Dec-2009     SCR Number: 6545
//  Project:  NEO
//  Description:
//      Comparing IBW more precisely using isSettingChanged().
//
//  Revision: 021   By: mnr    Date: 23-Nov-2009     SCR Number: 6545
//  Project:  NEO
//  Description:
//      IBW comparison bug fixed that was introduced during CIBW project.
//
//  Revision: 020   By: gdc    Date: 27-Jan-2009    SCR Number: 6459
//  Project:  840S
//  Description:
//      Fixed order of initialization of main control settings. 
//      Vent-type must come first since it's a primary setting for 
//      mode.
//   
//  Revision: 019   By: rhj    Date: 10-Nov-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support Leak Compensation related changes.
//   
//  Revision: 018   By: gdc    Date: 11-Aug-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support changing IBW while ventilating.
//   
//  Revision: 017   By: gdc    Date: 17-Feb-2005    DR Number: 6144
//  Project:  NIV1
//  Description:
//      Added vent-type (NIV/INVASIVE) as a "main control" phase setting
//
//  Revision: 016   By: jja    Date: 05-Apr-2002    DR Number: 5790
//  Project:  VTPC
//  Description:
//      Added setting new 'allSettingValuesKnown_' data member to correct
//      initialization problem in VC+/VS.
//
//  Revision: 015   By: sah    Date: 05-May-2000    DR Number: 5718
//  Project:  NeoMode
//  Description:
//      Added initialization and setting of new 'batchState_' data
//      member.
//
//  Revision: 014   By: sah    Date: 05-Aug-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  added call to new setting method, 'resetConstraints()' for
//         resetting all dynamic constraints to their absolute limit
//         counterpart, to ensure correct calculations during new-patient
//         initialization
//      *  removed unneeded (as of TC release) code for setting mand type
//         to PC for New-Patient BiLevel Setup
//      *  incorporated new 'warpToRange()' method
//
//  Revision: 013   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  now derived off of 'ContextSubject' base class
//	*  old 'SettingCallbackMgr' callback mechanism obsoleted, with
//	   new observer/subject mechanism incorporated
//	*  added new 'newPatientSetupFlag_' flag to monitor when in
//	   New-Patient Setup mode
//	*  removed special handling of apnea mand type from
//	   'adjustNewPatientBatch()' (now handled by the setting)
//	*  modified special handling of mand type in
//	   'adjustNewPatientBatch()' to be more readable
//
//  Revision: 012   By: sah   Date:  19-Jan-1999    DR Number: 5322
//  Project:  ATC
//  Description:
//	Project-related changes:
//	*  cleaned up requirements referencing
//	*  now many access methods are available off of the 'Setting'
//	   base class, therefore I cleaned up many of those operations
//	   to use generic, 'Setting' pointers rather than specific
//	   derived classes
//	*  cleaned up 'correctApneaSettings()' method to use new
//	   'correctSelf()' setting methods
//	*  added new 'isSettingChanged()' method
//
//  Revision: 011   By: sah    Date: 12-Nov-1998    DR Number: 5257
//  Project:  BILEVEL
//  Description:
//	While testing the changes for this DCS, noticed obsolete
//	DEVELOPMENT-only code.
//
//  Revision: 010   By: dosman    Date: 31-Aug-1998    DR Number: 143
//  Project:  BILEVEL
//  Description:
//	Fixed so that apnea mandatory type gets set to PCV when 
//      we enter BILEVEL at new patient time.
//
//  Revision: 009   By: dosman    Date: 07-May-1998    DR Number: 
//  Project:  BILEVEL
//  Description:
//	code cleanup, add/fix TIs, again on 10-Jul-1998
//
//  Revision: 008   By: dosman    Date: 19-Apr-1998    DR Number: 14
//  Project:  BILEVEL
//  Description:
//	Fixed so that mandatory type gets set to PCV when 
//      we enter BILEVEL at new patient time.
//
//  Revision: 007   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 006   By: dosman    Date: 24-Dec-1997    DR Number: 
//  Project:  BILEVEL
//  Description:
//	initial BiLevel version
//	added symbolic constants
//	added new mode bilevel
//	Changed adjustAcceptedBatch_ so that the absolute max/min
//	values of all batch settings are updated whenever 
//	an adjustment of batch settings begins.
//      Went back and removed unnecessary call to setForcedChangeFlag
//      Added requirement numbers for tracing to SRS.
//
//  Revision: 005   By: sah    Date: 29-Sep-1997    DR Number: 2410
//  Project: Sigma (R8027)
//  Description:
//	Added new interface method for the adjustment of the new
//	Atmospheric Pressure Setting.
//
//  Revision: 004   By: sah    Date:  18-Sep-1997    DR Number: 2360
//  Project: Sigma (R8027)
//  Description:
//	The 'warpValue()' method no longer defaults its warp direction
//	parameter, therefore add 'WARP_NEAREST' (in 'correctApneaSettings()').
//
//  Revision: 003   By: sah    Date:  07-May-1997    DCS Number: 2034
//  Project: Sigma (R8027)
//  Description:
//	Fixed problem with new-patient setup producing "clipped" tidal
//	volumes, by resetting all bound constraints BEFORE calculating
//	any of the new-patient values (see 'adjustNewPatientBatch()').
//
//  Revision: 002   By: sah    Date:  10-Dec-1996    DR Number: 1606
//  Project: Sigma (R8027)
//  Description:
//	Added '(90 - PEEP)' constraint to 'correctApneaSettings()'.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//=====================================================================

#include "AdjustedContext.hh"

#if defined(SIGMA_DEVELOPMENT)
	#include "Ostream.hh"
	#include "Settings_Validation.hh"
#endif  // defined(SIGMA_DEVELOPMENT)

#include "ModeValue.hh"
#include "MandTypeValue.hh"
#include "SettingConstants.hh"

//@ Usage-Classes
#include "ContextMgr.hh"
#include "SettingsMgr.hh"
#include "DiscreteSetting.hh"
#include "BoundedRange.hh"
#include "AcceptedContext.hh"
#include "RecoverableContext.hh"
#include "BoundedSetting.hh"
#include "TimeStamp.hh"
#include "ApneaInspPressSetting.hh"
#include "ApneaO2PercentSetting.hh"
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  AdjustedContext()  [Default Constructor]
//
//@ Interface-Description
//  Construct a default adjusted settings context.
//
//  If there is a previous patient's setting values available, the Accepted
//  Context will initialized itself based on those values.  Therefore, if
//  this occurs the Adjusted Context (this context) will initialize itself
//  based on the Accepted Context's batch values (no callbacks are activated
//  due to these updates).  Thereby, "synchronizing" the two contexts.
//
//  If there is NO previous patient values, the setting values of this
//  context inititialized to their, respective, default values.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Each of the "setting values" instances are registered with this context's
//  callback handler ('RegisterSettingChange_()'), so that the changes of
//  the setting values can be monitored by this context.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

AdjustedContext::AdjustedContext(void)
: ContextSubject(ContextId::ADJUSTED_CONTEXT_ID),
proposedBatchValues_(&AdjustedContext::RegisterSettingChange_),
batchState_(AdjustedContext::NEW_PATIENT_BATCH),
newPatientSetupFlag_(FALSE)
{
	// fully initialize this context with the values of the Accepted Context;
	// no callbacks are activated...
	proposedBatchValues_ =
	ContextMgr::GetAcceptedContext().getAcceptedBatchValues();
}	// $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~AdjustedContext(void)  [Destructor]
//
//@ Interface-Description
//  Destroy this adjusted context.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

AdjustedContext::~AdjustedContext(void)
{
	CALL_TRACE("~AdjustedContext()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  adjustNewPatientBatch(phase)
//
//@ Interface-Description
//  Prepare the Adjusted Context for phase number 'phase' of the
//  New-Patient Setup.  This method allows the Adjusted Context to
//  initialize its settings based on the phase of the New-Patient
//  Setup that is being entered.  When 'phase' is 'IBW_ADJUST_PHASE',
//  only IBW is initialized and allowed to be adjusted.  When 'phase' is
//  'MAIN_CONTROL_ADJUST_PHASE', only the Main Control Settings (i.e.,
//  Mode, Mandatory Type, Trigger Type and Support Type) are initialized
//  and allowed to be adjusted.  Finally, when 'phase' is set to
//  'BREATH_SETTINGS_ADJUST_PHASE', all other batch settings are initialized
//  (often, based on IBW and the Main Control Settings) and all non-apnea
//  batch settings are allowed to be adjusted.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method contains part of the implementation that assures
//  that we are using PCV mandatory type when in bilevel mode.
//  $[02012] -- new TI & I:E ratio while changing exp. time
//  $[02013] -- new TI & TE while changing I:E ratio...
//  $[02014] -- new TE & I:E while changing TI...
//  $[02016] -- new TI & I:E while keeping exp. time constant...
//  $[02017] -- new TI & TE while keeping I:E ratio constant...
//  $[02018] -- new TE & I:E while keeping TI constant...
//  $[02162] -- when in bilevel, I:E max is 149, else I:E max is 4
//  $[02172] -- when in bilevel, TI max is 30, else TI max is 8
//---------------------------------------------------------------------
//@ PreCondition
//  (phase == IBW_ADJUST_PHASE           ||
//   phase == MAIN_CONTROL_ADJUST_PHASE  ||
//   phase == BREATH_SETTINGS_ADJUST_PHASE)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
AdjustedContext::adjustNewPatientBatch(const BatchAdjustPhase phase)
{
	CALL_TRACE("adjustNewPatientBatch(phase)");

	batchState_ = NEW_PATIENT_BATCH;

	// set the flag indicating that we're currently in a New-Patient Setup
	// mode of operation (cleared by Accepted Context)...
	newPatientSetupFlag_ = TRUE;

	// get a local copy of the array of setting pointers...
	const SettingPtr*  arrSettingPtrs = SettingsMgr::GetArrayOfSettingPtrs();

	Uint32  idx;

	for ( idx = SettingId::LOW_BATCH_ID_VALUE;
		idx <= SettingId::HIGH_BATCH_ID_VALUE; idx++ )
	{
		// before initializing to new-patient values, ensure that no
		// residual, previous patient constraints are in place...
		(arrSettingPtrs[idx])->resetConstraints();
	}

	switch ( phase )
	{
		case ::IBW_ADJUST_PHASE :		  // $[TI1]
			{
				//
				// $[02006] New-patient step 1 processing ...
				//

				//-----------------------------------------------------------------
				// first, reset the "state" for all of the batch settings...
				//-----------------------------------------------------------------

				for ( idx = SettingId::LOW_BATCH_ID_VALUE;
					idx <= SettingId::HIGH_BATCH_ID_VALUE; idx++ )
				{
					(arrSettingPtrs[idx])->resetState();
				}
				allSettingValuesKnown_ = FALSE;

				//-----------------------------------------------------------------
				// finally, calculate the new-patient value for IBW...
				//-----------------------------------------------------------------

				(arrSettingPtrs[SettingId::IBW])->updateToNewPatientValue();
			}
			break;

		case ::MAIN_CONTROL_ADJUST_PHASE :		  // $[TI2]
			//
			// $[02007] New-patient step 2 processing ...
			//
			// calculate the new-patient values for the Main Control Settings...
			(arrSettingPtrs[SettingId::VENT_TYPE])->updateToNewPatientValue();
			(arrSettingPtrs[SettingId::MODE])->updateToNewPatientValue();
			(arrSettingPtrs[SettingId::MAND_TYPE])->updateToNewPatientValue();
			(arrSettingPtrs[SettingId::SUPPORT_TYPE])->updateToNewPatientValue();
			(arrSettingPtrs[SettingId::TRIGGER_TYPE])->updateToNewPatientValue();
			break;

		case ::BREATH_SETTINGS_ADJUST_PHASE :	  // $[TI3]
			{
				//
				// $[02008] New-patient step 3 processing ...
				//
				// calculate the new-patient values for all of the batch settings,
				// except those initialized in the previous phases...
				for ( idx = SettingId::LOW_BATCH_ID_VALUE;
					idx <= SettingId::HIGH_BATCH_ID_VALUE; idx++ )
				{	// $[TI3.3] -- this path is ALWAYS executed...
					switch ( idx )
					{
						case SettingId::IBW :
						case SettingId::VENT_TYPE :
						case SettingId::MODE :
						case SettingId::MAND_TYPE :
						case SettingId::SUPPORT_TYPE :
						case SettingId::TRIGGER_TYPE :
							// do nothing; this setting has already been initialized...
							break;
						default :				// $[TI3.3.2]
							// initialize this setting to it's new-patient value...
							(arrSettingPtrs[idx])->updateToNewPatientValue();
							break;
					}
				}  // end of for loop...

				// Reinitialize Disconnect Sensitivity due the change of Leak Comp settings.
				(arrSettingPtrs[SettingId::DISCO_SENS])->updateToNewPatientValue();

				// Set flag indicating all new patient values initialized 
				// for setting constraint checks
				allSettingValuesKnown_ = TRUE;

				// ALL settings should be valid, at this point...
				SAFE_CLASS_ASSERTION((areAllValuesValid()));
			}
			break;

		case ::UNDEF_ADJUSTMENT_PHASE :
		default:        
			AUX_CLASS_ASSERTION_FAILURE(phase);
			break;
	};
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  adjustVentSetupBatch(phase)
//
//@ Interface-Description
//  Prepare the Adjusted Context for phase number 'phase' of Vent Setup.
//  When 'phase' is 'MAIN_CONTROL_ADJUST_PHASE', only the Main Control
//  Settings (i.e., Mode, Mandatory Type, Trigger Type and Support Type)
//  are allowed to be adjusted.  This involves copying, from the Accepted
//  Context, all of the batch values that are different from the values
//  stored in this context, and activating a callback for each of the values
//  copied.  When 'phase' is set to 'BREATH_SETTINGS_ADJUST_PHASE', all
//  other non-apnea, batch settings allowed to be adjusted.  Given changes
//  of the Main Control Settings, the specified Transition Rules will run
//  upon entry into the 'BREATH_SETTINGS_ADJUST_PHASE' phase.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (phase == MAIN_CONTROL_ADJUST_PHASE  ||
//   phase == BREATH_SETTINGS_ADJUST_PHASE)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
AdjustedContext::adjustVentSetupBatch(const BatchAdjustPhase phase)
{
	CALL_TRACE("adjustVentSetupBatch(phase)");

	batchState_ = VENT_SETUP_BATCH;

	if ( phase == MAIN_CONTROL_ADJUST_PHASE )
	{	// $[TI1]
		// copy in all of the batch setting values from the Accepted Context
		// into this context...
		adjustAcceptedBatch_();
	}
	else if ( phase == BREATH_SETTINGS_ADJUST_PHASE )
	{
		// get an alias to the Accepted Context...
		const AcceptedContext&  rAccContext = ContextMgr::GetAcceptedContext();

		// SCR 6545 : Illegal Vti mand after changing IBW and Mandatory Type
		if( isSettingChanged(SettingId::IBW) )
		{
			const SettingPtr*  arrSettingPtrs = SettingsMgr::GetArrayOfSettingPtrs();

			for ( Uint32 idx = SettingId::LOW_BATCH_ID_VALUE;
				idx <= SettingId::HIGH_BATCH_ID_VALUE; idx++ )
			{
				(arrSettingPtrs[idx])->updateToNewIbwValue();
			}

			// ALL settings should be valid, at this point...
			SAFE_CLASS_ASSERTION((areAllValuesValid()));
		}

		// get the ACCEPTED and ADJUSTED Vent Type Setting values...
		const DiscreteValue  ACCEPTED_VENT_TYPE_VALUE = rAccContext.getBatchDiscreteValue(SettingId::VENT_TYPE);
		const DiscreteValue  ADJUSTED_VENT_TYPE_VALUE = getDiscreteValue(SettingId::VENT_TYPE);

		if ( ADJUSTED_VENT_TYPE_VALUE != ACCEPTED_VENT_TYPE_VALUE )
		{	// $[TI2.9]
			// the adjusted value for Vent Type is different than what is accepted,
			// therefore activate any Transition Rules that apply to changes
			// of Vent Type...
			DiscreteSetting*  pVentTypeSetting = SettingsMgr::GetDiscreteSettingPtr(SettingId::VENT_TYPE);

			pVentTypeSetting->calcTransition(ADJUSTED_VENT_TYPE_VALUE, ACCEPTED_VENT_TYPE_VALUE);
		}	// $[TI2.10] -- no transition occurred...

		// get the ACCEPTED and ADJUSTED Mode Setting values...
		const DiscreteValue  ACCEPTED_MODE_VALUE = rAccContext.getBatchDiscreteValue(SettingId::MODE);
		const DiscreteValue  ADJUSTED_MODE_VALUE = getDiscreteValue(SettingId::MODE);

		if ( ADJUSTED_MODE_VALUE != ACCEPTED_MODE_VALUE )
		{	// $[TI2.1]
			// the adjusted value for Mode is different than what is accepted,
			// therefore activate any Transition Rules that apply to changes
			// of Mode...
			DiscreteSetting*  pModeSetting = SettingsMgr::GetDiscreteSettingPtr(SettingId::MODE);

			pModeSetting->calcTransition(ADJUSTED_MODE_VALUE, ACCEPTED_MODE_VALUE);
		}	// $[TI2.2] -- no transition occurred...

		// get the ACCEPTED and ADJUSTED Mandatory Type Setting values...
		const DiscreteValue  ACCEPTED_MAND_TYPE_VALUE = rAccContext.getBatchDiscreteValue(SettingId::MAND_TYPE);
		const DiscreteValue  ADJUSTED_MAND_TYPE_VALUE = getDiscreteValue(SettingId::MAND_TYPE);

		if ( ADJUSTED_MAND_TYPE_VALUE != ACCEPTED_MAND_TYPE_VALUE )
		{	// $[TI2.3]
			// the adjusted value for Mandatory Type is different than what is
			// accepted, therefore activate any Transition Rules that apply to
			// changes of Mandatory Type...
			DiscreteSetting*  pMandTypeSetting = SettingsMgr::GetDiscreteSettingPtr(SettingId::MAND_TYPE);

			pMandTypeSetting->calcTransition(ADJUSTED_MAND_TYPE_VALUE, ACCEPTED_MAND_TYPE_VALUE);
		}	// $[TI2.4] -- no transition occurred...

		// get the ACCEPTED and ADJUSTED Trigger Type Setting values...
		const DiscreteValue  ACCEPTED_TRIGGER_TYPE_VALUE = rAccContext.getBatchDiscreteValue(SettingId::TRIGGER_TYPE);
		const DiscreteValue  ADJUSTED_TRIGGER_TYPE_VALUE = getDiscreteValue(SettingId::TRIGGER_TYPE);

		if ( ADJUSTED_TRIGGER_TYPE_VALUE != ACCEPTED_TRIGGER_TYPE_VALUE )
		{	// $[TI2.5]
			// the adjusted value for Trigger Type is different than what is
			// accepted, therefore activate any Transition Rules that apply to
			// changes of Trigger Type...
			DiscreteSetting*  pTriggerTypeSetting = SettingsMgr::GetDiscreteSettingPtr(SettingId::TRIGGER_TYPE);

			pTriggerTypeSetting->calcTransition(ADJUSTED_TRIGGER_TYPE_VALUE, ACCEPTED_TRIGGER_TYPE_VALUE);
		}	// $[TI2.6] -- no transition occurred...

		// get the ACCEPTED and ADJUSTED Support Type Setting values...
		const DiscreteValue  ACCEPTED_SUPPORT_TYPE_VALUE = rAccContext.getBatchDiscreteValue(SettingId::SUPPORT_TYPE);
		const DiscreteValue  ADJUSTED_SUPPORT_TYPE_VALUE = getDiscreteValue(SettingId::SUPPORT_TYPE);

		if ( ADJUSTED_SUPPORT_TYPE_VALUE != ACCEPTED_SUPPORT_TYPE_VALUE )
		{	// $[TI2.7]
			// the adjusted value for Support Type is different than what is
			// accepted, therefore activate any Transition Rules that apply to
			// changes of Support Type...
			DiscreteSetting*  pSupportTypeSetting = SettingsMgr::GetDiscreteSettingPtr(SettingId::SUPPORT_TYPE);

			pSupportTypeSetting->calcTransition(ADJUSTED_SUPPORT_TYPE_VALUE, ACCEPTED_SUPPORT_TYPE_VALUE);
		}	// $[TI2.8] -- no transition occurred...
	}
	else
	{
		// 'phase' is an illegal value...
		AUX_CLASS_ASSERTION_FAILURE(phase);
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  adjustApneaSetupBatch(phase, isReEntry)
//
//@ Interface-Description
//  Prepare the Adjusted Context for phase number 'phase' of Apnea Setup.
//  When 'phase' is 'BREATH_SETTINGS_ADJUST_PHASE', all of the Apnea Breath
//  Settings are allowed to be adjusted.  This involves copying, from the
//  Accepted Context, ALL of the batch values that are different from the
//  values stored in this context, and activating a callback for each of the
//  values copied.  When 'phase' is set to 'MAIN_CONTROL_ADJUST_PHASE', only
//  the Apnea Main Control Setting (i.e., Apnea Mandatory Type) is allowed to
//  be adjusted.  A change of the Apnea Main Control Setting will activate
//  the  specified Transition Rule upon re-entry into the
//  'BREATH_SETTINGS_ADJUST_PHASE' phase.
//
//  If 'phase' indicates an entry into a "breath settings" phase, 'isReEntry'
//  must indicate whether this is an initial entry or re-entry.
//  
//  Since all apnea settings may be visible to the operator during Apnea
//  Setup, a "correction" of the invalid apnea settings is initiated --
//  whether apnea is possible, or not -- upon entry into the
//  'BREATH_SETTINGS_ADJUST_PHASE' phase of Apnea Setup. A status is
//  returned that indicates which, if any, apnea settings were "corrected".
//---------------------------------------------------------------------
//@ Implementation-Description
//  For Apnea Setup, the sequence of events is a little different than the
//  other multi-phase setups.  First of all, the FIRST phase that's entered
//  is the "breath settings" phase -- not the "main control" phase.  This
//  means that the accepted setting values need to be copied into this
//  context upon entry into this phase.  However, there is one hitch:  the
//  operator can go to the "main control" phase -- provided no changes have
//  been made, yet -- and alter the Apnea Main Control Setting, then, the
//  operator can RE-ENTER into the "breath settings" phase.  This means that
//  this method needs to keep track of whether entry into the "breath
//  settings" phase is a normal entry or a re-entry.  If it is an initial
//  entry, the accepted setting values are copied into this context and the
//  apnea settings are "corrected".  If this is a re-entry, the transition
//  rule may be applied.
//
//  The "correction" of the apnea settings must be done BEFORE the Transition
//  Rule is activated.
//
//  $[02005]\a\ -- "correct" apnea when Apnea Setup is entered... 
//---------------------------------------------------------------------
//@ PreCondition
//  (phase == MAIN_CONTROL_ADJUST_PHASE  ||
//   phase == BREATH_SETTINGS_ADJUST_PHASE)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ApneaCorrectionStatus
AdjustedContext::adjustApneaSetupBatch(const BatchAdjustPhase phase,
									   const Boolean          isReEntry)
{
	CALL_TRACE("adjustApneaSetupBatch(phase)");
	CLASS_PRE_CONDITION((phase == ::MAIN_CONTROL_ADJUST_PHASE  ||
						 phase == ::BREATH_SETTINGS_ADJUST_PHASE));

	batchState_ = APNEA_BATCH;

	ApneaCorrectionStatus  correctionStatus = ::NONE_CORRECTED;

	if ( phase == ::BREATH_SETTINGS_ADJUST_PHASE )
	{	// $[TI1]
		if ( !isReEntry )
		{	// $[TI1.1]
			// this is the first time into this "breath settings" phase, therefore
			// copy in ALL of the batch setting values from the Accepted Context
			// into this context...
			adjustAcceptedBatch_();

			// ...and correct any apnea settings that may be invalid...
			correctionStatus = correctApneaSettings();
		}
		else
		{	// $[TI1.2]
			// this is a RE-ENTRY into this "breath settings" phase, therefore
			// activate any needed Transition Rules...

			// get an alias to the Accepted Context...
			const AcceptedContext&  rAccContext = ContextMgr::GetAcceptedContext();

			// get the ACCEPTED and ADJUSTED Apnea Mandatory Type Setting values...
			const DiscreteValue  ACCEPTED_APNEA_MAND_TYPE_VALUE =
			rAccContext.getBatchDiscreteValue(SettingId::APNEA_MAND_TYPE);
			const DiscreteValue  ADJUSTED_APNEA_MAND_TYPE_VALUE =
			getDiscreteValue(SettingId::APNEA_MAND_TYPE);

			if ( ADJUSTED_APNEA_MAND_TYPE_VALUE != ACCEPTED_APNEA_MAND_TYPE_VALUE )
			{	// $[TI1.2.1]
				// the adjusted value for Apnea Mandatory Type is different than what
				// is accepted, therefore activate any Transition Rules that apply to
				// changes of Apnea Mandatory Type...
				DiscreteSetting*  pApneaMandTypeSetting = 
				SettingsMgr::GetDiscreteSettingPtr(SettingId::APNEA_MAND_TYPE);

				pApneaMandTypeSetting->calcTransition(ADJUSTED_APNEA_MAND_TYPE_VALUE,
													  ACCEPTED_APNEA_MAND_TYPE_VALUE);
			}	// $[TI1.2.2] -- no transition occurred...

			// all of the ADJUSTED values should be valid, at this point...
			SAFE_CLASS_ASSERTION((areAllValuesValid()));
		}	// end of else ([TI1.2])...
	}	// $[TI2] -- 'phase' indicates "main control" phase; do nothing...

	return(correctionStatus);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  adjustTimeDateBatch()
//
//@ Interface-Description
//  Prepare the Adjusted Context for the adjustment of a Time/Date Batch.
//  (i.e., the Day, Month, Year, Hour and Minute Settings).
//---------------------------------------------------------------------
//@ Implementation-Description
//  The Time/Date settings are NEVER stored (as set) in NOVRAM or the
//  Accepted Context, therefore there is no need to "get" them from
//  the Accepted Context -- they are initialized to the current time.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
AdjustedContext::adjustTimeDateBatch(void)
{
	CALL_TRACE("adjustTimeDateBatch()");

	batchState_ = TIME_DATE_BATCH;

	// initialize all of the change-flags of all of the batch settings...
	adjustAcceptedBatch_();

	//===================================================================
	// Store the current date and time into this context...
	//===================================================================

	TimeStamp  timeStamp;	//initialized to "NOW"...

	proposedBatchValues_.setSequentialValue(SettingId::DAY,
											timeStamp.getDayOfMonth());
	proposedBatchValues_.setDiscreteValue(SettingId::MONTH,
										  (timeStamp.getMonth() - 1u));
	proposedBatchValues_.setSequentialValue(SettingId::YEAR,
											timeStamp.getYear());

	proposedBatchValues_.setSequentialValue(SettingId::HOUR,
											timeStamp.getHour());
	proposedBatchValues_.setSequentialValue(SettingId::MINUTE,
											timeStamp.getMinutes());

	//===================================================================
	// Set the "forced-change" flag of all of the time/date settings...
	//===================================================================

	SettingsMgr::GetSettingPtr(SettingId::DAY)->setForcedChangeFlag();
	SettingsMgr::GetSettingPtr(SettingId::MONTH)->setForcedChangeFlag();
	SettingsMgr::GetSettingPtr(SettingId::YEAR)->setForcedChangeFlag();
	SettingsMgr::GetSettingPtr(SettingId::HOUR)->setForcedChangeFlag();
	SettingsMgr::GetSettingPtr(SettingId::MINUTE)->setForcedChangeFlag();
}	// $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  adjustAtmPressureCal(atmPressureValue)
//
//@ Interface-Description
//  Prepare the Adjusted Context for the adjustment of an Atmospheric
//  Pressure Setting Calibration.  Use 'atmPressureValue' to initialize the
//  setting to the sensor's current value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The adjusted value must start off set to the current internal reading
//  (sensor plus offset) of the atmospheric pressure.
//
//  $[07072] -- displays the current barometric pressure...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
AdjustedContext::adjustAtmPressureCal(const Real32 atmPressureValue)
{
	CALL_TRACE("adjustAtmPressureCal(atmPressureValue)");

	batchState_ = ATM_PRESS_CAL;

	// initialize all of the change-flags of all of the batch settings...
	adjustAcceptedBatch_();

	BoundedSetting*  pAtmPressure;
	BoundedValue     atmPressure;

	pAtmPressure = SettingsMgr::GetBoundedSettingPtr(SettingId::ATM_PRESSURE);

	// store the passed-in 'atmPressureValue' into a boundedValue structure...
	atmPressure.value = atmPressureValue;

	// set the 'precision' field of 'atmPressure', and ensure that the
	// value resides on a resolution boundary...
	pAtmPressure->warpToRange(atmPressure, BoundedRange::WARP_NEAREST);

	proposedBatchValues_.setBoundedValue(SettingId::ATM_PRESSURE, atmPressure);

	pAtmPressure->setForcedChangeFlag();
}	// $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  recoverStoredBatch()
//
//@ Interface-Description
//  Recover the stored accepted settings from the Recoverable Context.
//  This involves copying, from the Recoverable Context, all of the values
//  that are different from the values stored in this context, and activating
//  a callback for each of the values copied.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (ContextMgr::GetRecoverableContext().isValid())
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
AdjustedContext::recoverStoredBatch(void)
{
	CALL_TRACE("recoverStoredBatch()");

	RecoverableContext&  rRecoverableContext =
	ContextMgr::GetRecoverableContext();

	SAFE_CLASS_PRE_CONDITION((rRecoverableContext.isValid()));

	// copy all of the batch setting values that are different from the
	// values stored in this context; callbacks ARE activated...
	proposedBatchValues_.copyFrom(rRecoverableContext.getStoredValues());

	//=================================================================
	// reset the "state" of all of the batch settings...
	//=================================================================

	// get a local copy of the array of setting pointers...
	const SettingPtr*  arrSettingPtrs = SettingsMgr::GetArrayOfSettingPtrs();

	Uint  idx;

	for ( idx = SettingId::LOW_BATCH_ID_VALUE;
		idx <= SettingId::HIGH_BATCH_ID_VALUE; idx++ )
	{
		(arrSettingPtrs[idx])->resetState();
	}

	// make sure all of the values in this context are currently valid...
	SAFE_CLASS_ASSERTION((areAllValuesValid()));
}	// $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  correctApneaSettings()
//
//@ Interface-Description
//  This "corrects" any apnea settings that are deemed invalid, and returns
//  a status indicating which, if any, settings were corrected.  There are
//  two apnea settings, apnea inspiratory pressure and apnea oxygen
//  percentage, whose valid values are based on non-apnea counterparts.
//  Therefore, if either of these two settings are invalid based on the
//  current value(s) of those counterparts, they must be "clipped" back
//  to a valid value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02029] -- calculations for correcting the apnea invalid condition...
//  $[02081] -- correct apnea oxygen percent...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ApneaCorrectionStatus
AdjustedContext::correctApneaSettings(void)
{
	CALL_TRACE("correctApneaSettings()");

	//===================================================================
	// Correction of Apnea Inspiratory Pressure...
	//===================================================================

	ApneaInspPressSetting*  pApneaInspPress =
	(ApneaInspPressSetting*)SettingsMgr::GetSettingPtr( SettingId::APNEA_INSP_PRESS );

	const Boolean  IS_APNEA_PI_CORRECTED = pApneaInspPress->correctSelf();


	//===================================================================
	// Correction of Apnea Oxygen Percentage...
	//===================================================================

	ApneaO2PercentSetting*  pApneaO2Percent =
	(ApneaO2PercentSetting*)SettingsMgr::GetSettingPtr(
													  SettingId::APNEA_O2_PERCENT
													  );

	const Boolean  IS_APNEA_O2_CORRECTED = pApneaO2Percent->correctSelf();


	//===================================================================
	// Determine overall correction status...
	//===================================================================

	ApneaCorrectionStatus  correctionStatus;

	if ( IS_APNEA_PI_CORRECTED  &&  IS_APNEA_O2_CORRECTED )
	{  // $[TI1] -- both were corrected...
		correctionStatus = ::BOTH_CORRECTED;
	}
	else if ( IS_APNEA_PI_CORRECTED )
	{  // $[TI2] -- only apnea inspiratory pressure was corrected...
		correctionStatus = ::APNEA_PI_CORRECTED;
	}
	else if ( IS_APNEA_O2_CORRECTED )
	{  // $[TI3] -- only apnea O2 percent was corrected...
		correctionStatus = ::APNEA_O2_CORRECTED;
	}
	else
	{  // $[TI4] -- neither were corrected...
		correctionStatus = ::NONE_CORRECTED;
	}

	return(correctionStatus);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  isSettingChanged(settingId)  [const, virtual]
//
//@ Interface-Description
//  Redefined from ContextSubject base class, to return the change status
//  of the indicated setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean
AdjustedContext::isSettingChanged( const SettingId::SettingIdType settingId ) const
{
	CALL_TRACE("isSettingChanged(settingId)");
	SAFE_AUX_CLASS_PRE_CONDITION((SettingId::IsBatchId(settingId)), settingId);

	return(SettingsMgr::GetSettingPtr(settingId)->isChanged());
}	 // $[TI1] (TRUE)  $[TI2] (FALSE)


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  areAnyBatchSettingsChanged()  [const, virtual]
//
//@ Interface-Description
//  Redefined from ContextSubject base class, to return the change status
//  of this context's batch of settings.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean
AdjustedContext::areAnyBatchSettingsChanged(void) const
{
	CALL_TRACE("areAnyBatchSettingsChanged()");

	const SettingPtr*  arrSettingPtrs = SettingsMgr::GetArrayOfSettingPtrs();

	Boolean  areAnyChanged = FALSE;

	for ( Uint idx = SettingId::LOW_BATCH_ID_VALUE;
		!areAnyChanged  &&  idx <= SettingId::HIGH_BATCH_ID_VALUE; idx++ )
	{
		// until a changed setting is detected...
		areAnyChanged = (arrSettingPtrs[idx])->isChanged();
	}

	return(areAnyChanged);
}	// $[TI1] (TRUE)  $[TI2] (FALSE)


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getSettingValue(settingId)  [const, virtual]
//
//@ Interface-Description
//  Redefined from ContextSubject base class. This query will return the
//  "adjusted" value of the setting indicated by 'settingId'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (SettingId::IsId(settingId))
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
AdjustedContext::getSettingValue(const SettingId::SettingIdType settingId) const
{
	CALL_TRACE("getSettingValue(settingId)");
	SAFE_AUX_CLASS_PRE_CONDITION((SettingId::IsBatchId(settingId)), settingId);

	return(proposedBatchValues_.getSettingValue(settingId));  // $[TI1]
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  areAllValuesValid()  [const]
//
// Interface-Description
//  Are all of the setting values within this context valid?
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

Boolean
AdjustedContext::areAllValuesValid(void) const
{
	CALL_TRACE("areAllValuesValid()");

	const SettingPtr*  arrSettingPtrs = SettingsMgr::GetArrayOfSettingPtrs();

	Boolean  areAllValid = TRUE;
	Uint32   idx;

	for ( idx  = SettingId::LOW_BATCH_ID_VALUE;
		idx <= SettingId::HIGH_BATCH_ID_VALUE; idx++ )
	{
		if ( !(arrSettingPtrs[idx])->isAdjustedValid() )
		{
			cout << "Invalid ADJUSTED Setting:" << endl;
			cout << *(arrSettingPtrs[idx]) << endl;

			areAllValid = FALSE;
		}
	}

	return(areAllValid);
}

#endif // defined(SIGMA_DEVELOPMENT)


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
AdjustedContext::SoftFault(const SoftFaultID softFaultID,
						   const Uint32      lineNumber,
						   const char*       pFileName,
						   const char*       pPredicate)
{

	FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
							ADJUSTED_CONTEXT, lineNumber, pFileName,
							pPredicate);
} 


//=====================================================================
//
//  Private Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  RegisterSettingChange_(batchId)  [static]
//
//@ Interface-Description
//  Notify the callback manager that a value in this context has changed.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (SettingId::IsBatchId(batchId))
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
AdjustedContext::RegisterSettingChange_( const SettingId::SettingIdType batchId )
{
	SAFE_AUX_CLASS_PRE_CONDITION((SettingId::IsBatchId(batchId)), batchId);

#if defined(SIGMA_DEVELOPMENT)
	if ( Settings_Validation::IsDebugOn(::ADJUSTED_CONTEXT) )
	{
		cout << "Changed ADJUSTED Setting:" << endl;
		cout << *(SettingsMgr::GetSettingPtr(batchId));
	}
#endif  // defined(SIGMA_DEVELOPMENT)

	SettingSubject*  pSettingSubject = SettingsMgr::GetSettingSubjectPtr(batchId);

	// notify the setting's observers of its value change...
	pSettingSubject->notifyAllObservers(Notification::ADJUSTED,
										Notification::VALUE_CHANGED);


	// notify this context's observers of a setting value change...
	ContextMgr::GetAdjustedContext().notifyAllObservers(Notification::ADJUSTED,
														Notification::BATCH_CHANGED,
														batchId);
}	// $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  adjustAcceptedBatch_()
//
//@ Interface-Description
//  Ready this context to adjust the currently accepted batch settings.
//  This involves copying, from the Accepted Context, all of the values
//  that are different from the values stored in this context, and activating
//  a callback for each of the values copied.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method contains part of the implementation to ensure that
//  IeRatioSetting and InspTimeSetting have the correct maximum value
//  based on mode.
//  $[02162] -- when in bilevel, I:E max is 149, else I:E max is 4
//  $[02172] -- when in bilevel, TI max is 30, else TI max is 8
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
AdjustedContext::adjustAcceptedBatch_(void)
{
	CALL_TRACE("adjustAcceptedBatch_()");

	// set allSettingValuesKnown_ flag for case of SamePatient
	if ( !allSettingValuesKnown_ )
	{
		allSettingValuesKnown_ = TRUE;
	}

	// copy all of the batch values from the Accepted Context to this
	// context; 'RegisterSettingChange_()' is activated for every value
	// copied...
	proposedBatchValues_.copyFrom( ContextMgr::GetAcceptedContext().getAcceptedBatchValues() );

	//=================================================================
	// reset the "state" all of the batch settings...
	//=================================================================

	// get a local copy of the array of setting pointers...
	const SettingPtr*  arrSettingPtrs = SettingsMgr::GetArrayOfSettingPtrs();

	Uint  idx;

	for ( idx = SettingId::LOW_BATCH_ID_VALUE;
		idx <= SettingId::HIGH_BATCH_ID_VALUE; idx++ )
	{
		(arrSettingPtrs[idx])->resetState();
	}

#if defined(SIGMA_DEVELOPMENT)  &&  !defined(SIGMA_COMMON)
	// only do this for DEVELOPMENT builds that are NOT build for 'COMMON'
	// CPU, because the 'Contexts_ut' unit test will fail here due to steps
	// taken that a user cannot take...
	SAFE_CLASS_ASSERTION((areAllValuesValid()));
#endif // defined(SIGMA_DEVELOPMENT)  &&  !defined(SIGMA_COMMON)
}	// $[TI1]
