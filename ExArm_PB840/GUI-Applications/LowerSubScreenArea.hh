#ifndef LowerSubScreenArea_HH
#define LowerSubScreenArea_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: LowerSubScreenArea - This subscreen area is the area on the Lower screen
// in which Lower subscreens are displayed (normally selected by Main Settings
// buttons or buttons in the Lower Screen Select Area).  Part of the GUI-App
// Framework cluster.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/LowerSubScreenArea.hhv   25.0.4.0   19 Nov 2013 14:08:06   pvcs  $
//
//@ Modification-Log
//
//  Revision: 010  By:  rhj   Date:  19-Feb-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//	Added ProxSetupSubScreen.
//
//  Revision: 009  By:  gdc	  Date:  22-AUG-2008    SCR Number: 6435
//  Project:  840S
//  Description:     
//      Provided for changing IBW while ventilating. Eliminated the
//      IBW setup sub-screen (aka New Patient Sub-Screen).
//
//  Revision: 008  By:  gdc	   Date:  23-Mar-2007    SCR Number: 6237
//  Project:  Trend
//  Description:
//	Added CompactFlashTestSubScreen.
//
//  Revision: 007  By:  rhj	   Date:  02-Oct-2006    SCR Number: 6236
//  Project:  RESPM
//  Description:
//  Added NifSubScreen, P100SubScreen, VitalCapacitySubScreen to LSAContents.
//
//  Revision: 006  By:  quf	   Date:  23-Jan-2002    DCS Number: 5984
//  Project:  GUIComms
//  Description:
//	Added SerialLoopbackTestSubScreen to ServiceLSAContents.
//
//  Revision: 005  By:  yyy	   Date:  09-Nov-1999    DCS Number: 5327
//  Project:  NeoMode
//  Description:
//		(hhd 16-Nov-1999) - Reorganize this class to reduce header file dependency. 
//		(yyy) Initial version.
//
//  Revision: 004  By: yyy      Date: 05-Jan-1998  DR Number: 
//    Project:  BiLevel (R8027)
//    Description:
//     Initial release for BiLevel adding software option.
//     Merged /840/Baseline/GUI-Applications/vcssrc/LowerSubScreenArea.hhv   1.10.1.0   07/30/98 10:15:04   gdc
//
//  Revision: 003  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 002  By:  yyy   Date:  04-Aug-1997    DCS Number:  1846
//  Project:  Sigma (R8027)
//  Description:
//      Reactivate the DateTime subScreen if the VitalPatientDataArea is changes
//      mode.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "SubScreenArea.hh"

//@ Usage-Classes
class AlarmSetupSubScreen;
class ApneaSetupSubScreen;
class CommSetupSubScreen;
class BreathTimingSubScreen;
class LowerOtherScreensSubScreen;
class MoreSettingsSubScreen;
class VentSetupSubScreen;
class VentStartSubScreen;
class OffKeysSubScreen;
class NifSubScreen;
class P100SubScreen;
class VitalCapacitySubScreen;
class CalibrationSetupSubScreen;
class DateTimeSettingsSubScreen;
class EstTestSubScreen;
class ExitServiceModeSubScreen;
class ExternalControlSubScreen;
class ServiceLowerOtherScreensSubScreen;
class ServiceModeSetupSubScreen;
class SstSetupSubScreen;
class SstTestSubScreen;
class ServiceInitializationSubScreen;
class SerialNumSetupSubScreen;
class FlowSensorCalibrationSubScreen;
class PressureXducerCalibrationSubScreen;
class OperationHourCopySubScreen;
class SerialLoopbackTestSubScreen;
class CompactFlashTestSubScreen;
class ProxSetupSubScreen;

#include "GuiAppClassIds.hh"
//@ End-Usage

class LowerSubScreenArea : public SubScreenArea
{
public:
	LowerSubScreenArea(void);
	~LowerSubScreenArea(void);

	void updateDateTimeSettingScreen(void);

	// Normal Mode Screen queries
	static AlarmSetupSubScreen *GetAlarmSetupSubScreen(void);
	static ApneaSetupSubScreen *GetApneaSetupSubScreen(void);
	static BreathTimingSubScreen *GetBreathTimingSubScreen(void);
	static CommSetupSubScreen *GetCommSetupSubScreen(void);
	static LowerOtherScreensSubScreen *GetLowerOtherScreensSubScreen(void);
	static MoreSettingsSubScreen *GetMoreSettingsSubScreen(void);
	static VentSetupSubScreen *GetVentSetupSubScreen(void);
	static VentStartSubScreen *GetVentStartSubScreen(void);
	static OffKeysSubScreen *GetOffKeysSubScreen(void);
	static NifSubScreen *GetNifSubScreen(void);
	static P100SubScreen *GetP100SubScreen(void);
	static VitalCapacitySubScreen *GetVitalCapacitySubScreen(void);
    static ProxSetupSubScreen * GetProxSetupSubScreen(void);

	// Service Mode Screen queries
	static SstSetupSubScreen *GetSstSetupSubScreen(void);
	static SstTestSubScreen	 *GetSstTestSubScreen(void);
	static EstTestSubScreen	 *GetEstTestSubScreen(void);
	static DateTimeSettingsSubScreen  *GetDateTimeSettingsSubScreen(void);
	static ExitServiceModeSubScreen   *GetExitServiceModeSubScreen(void);
	static ServiceLowerOtherScreensSubScreen *GetServiceLowerOtherScreensSubScreen(void);
	static CalibrationSetupSubScreen *GetCalibrationSetupSubScreen(void);
    	static ServiceModeSetupSubScreen	*GetServiceModeSetupSubScreen(void);
	static ExternalControlSubScreen		*GetExternalControlSubScreen(void);
	static ServiceInitializationSubScreen	*GetServiceInitializationSubScreen(void);
	static SerialNumSetupSubScreen	*GetSerialNumSetupSubScreen(void);
	static FlowSensorCalibrationSubScreen	*GetFlowSensorCalibrationSubScreen(void);
	static PressureXducerCalibrationSubScreen	*GetPressureXducerCalibrationSubScreen(void);
	static OperationHourCopySubScreen	*GetOperationHourCopySubScreen(void);
	static SerialLoopbackTestSubScreen *GetSerialLoopbackTestSubScreen(void);
	static CompactFlashTestSubScreen *GetCompactFlashTestSubScreen(void);

	void activateOffKeysSubScreen(Boolean delayedDisplay=FALSE);
	void deactivateOffKeysSubScreen(void);

	static void SoftFault(const SoftFaultID softFaultID,
								 const Uint32      lineNumber,
								 const char*       pFileName  = NULL, 
								 const char*       pPredicate = NULL);

protected:

private:
	// these methods are purposely declared, but not implemented...
	LowerSubScreenArea(const LowerSubScreenArea&);		// not implemented...
	void   operator=(const LowerSubScreenArea&);	// not implemented...
};


#endif // LowerSubScreenArea_HH 
