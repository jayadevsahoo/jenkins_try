
//----------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------
//
//  "softdload.h"
//
//  Declaration and include file for UI build functions
//

//  Date: 03-28-2002
//

//----------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------

#ifndef _SOFTDLOAD_INCLUDED
#define _SOFTDLOAD_INCLUDED

#ifdef __cplusplus
    extern "C" {            /* Assume C declarations for C++ */
#endif  /* __cplusplus */

//----------------------------------------------------------------------------------------------------
//  Include required headers
//----------------------------------------------------------------------------------------------------

#include <cvidef.h> // definition of CVICALLBACK

//----------------------------------------------------------------------------------------------------
//  Prototype UI build functions.
//----------------------------------------------------------------------------------------------------

int BuildBOOT_PANEL (int hParentPanel);
int BuildIMAG_PANEL (int hParentPanel);
int BuildINS_PANEL (int hParentPanel);
int BuildSTATEPANEL (int hParentPanel);
int BuildTFTP_PANEL (int hParentPanel);

//-------------------------------------------------------------------------------------------------
// Prototype callbacks for Panel: BOOT_PANEL
// ------------------------------------------------------------------------------------------------

// Control: BOOT_PANEL_BOOTP_TIMEOUT
int CVICALLBACK calcTimeout (int panelHandle, int control, int event, void *callbackData, int eventData1, int eventData2);

//-------------------------------------------------------------------------------------------------
// Prototype callbacks for Panel: IMAG_PANEL
// ------------------------------------------------------------------------------------------------
int CVICALLBACK ImagePanelCallback(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);

//-------------------------------------------------------------------------------------------------
// Prototype callbacks for Panel: INS_PANEL
// ------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
// Prototype callbacks for Panel: STATEPANEL
// ------------------------------------------------------------------------------------------------
int CVICALLBACK DnloadStateCB(int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2);

//-------------------------------------------------------------------------------------------------
// Prototype callbacks for Panel: TFTP_PANEL
// ------------------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------------------
// Declare identifiers for Panel:  BOOT_PANEL
// ------------------------------------------------------------------------------------------------

extern int BOOT_PANEL_BOOTP_CANCEL                  ;     // control identifier
extern int BOOT_PANEL_BOOTP_TEXT                    ;     // control identifier
extern int BOOT_PANEL_DECORATION                    ;     // control identifier
extern int BOOT_PANEL_BOOTP_TIMEOUT                 ;     // control identifier

//-------------------------------------------------------------------------------------------------
// Declare identifiers for Panel:  IMAG_PANEL
// ------------------------------------------------------------------------------------------------

extern int IMAG_PANEL_VENTSFTWR_BOX                 ;     // control identifier
extern int IMAG_PANEL_VENTSFTWR_BOX_LBL             ;     // control identifier
extern int IMAG_PANEL_840_GUISOFTWARE_LBL           ;     // control identifier
extern int IMAG_PANEL_840_GUISOFTWARE               ;     // control identifier
extern int IMAG_PANEL_840_BDSOFTWARE_LBL            ;     // control identifier
extern int IMAG_PANEL_840_BDSOFTWARE                ;     // control identifier
extern int IMAG_PANEL_840_AUTODET_STATUS            ;     // control identifier
extern int IMAG_PANEL_VENT_CAL_BUTTON               ;     // control identifier
extern int IMAG_PANEL_ADAPTER_LIST                  ;     // control identifier
extern int IMAG_PANEL_IMAG_LIST                     ;     // control identifier
extern int IMAG_PANEL_LANG_DONE                     ;     // control identifier
extern int IMAG_PANEL_LANG_CANCEL                   ;     // control identifier
extern int IMAG_PANEL_TEXTMSG                       ;     // control identifier

//-------------------------------------------------------------------------------------------------
// Declare identifiers for Panel:  INS_PANEL
// ------------------------------------------------------------------------------------------------

extern int INS_PANEL_INS_DONE                       ;     // control identifier
extern int INS_PANEL_INS_CANCEL                     ;     // control identifier
extern int INS_PANEL_INSTRUCTIONS                   ;     // control identifier
extern int INS_PANEL_DECORATION                     ;     // control identifier
extern int INS_PANEL_TEXTMSG                        ;     // control identifier

//-------------------------------------------------------------------------------------------------
// Declare identifiers for Panel:  STATEPANEL
// ------------------------------------------------------------------------------------------------

extern int DloadType;

extern int STATEPANEL_GROUP_BOX                     ;     // control identifier
extern int STATEPANEL_INITIAL_DLOAD                 ;     // control identifier
extern int STATEPANEL_REPLACE_BOTH_DLOAD            ;     // control identifier
extern int STATEPANEL_STATE_DONE                    ;     // control identifier
extern int STATEPANEL_STATE_CANCEL                  ;     // control identifier

//-------------------------------------------------------------------------------------------------
// Declare identifiers for Panel:  TFTP_PANEL
// ------------------------------------------------------------------------------------------------

extern int TFTP_PANEL_GUI_LED                       ;     // control identifier
extern int TFTP_PANEL_BD_LED                        ;     // control identifier
extern int TFTP_PANEL_BD_BLOCK_SENT                 ;     // control identifier
extern int TFTP_PANEL_BD_FILE                       ;     // control identifier
extern int TFTP_PANEL_BD_PERCENT                    ;     // control identifier
extern int TFTP_PANEL_BD_TOTAL                      ;     // control identifier
extern int TFTP_PANEL_GUI_FILE                      ;     // control identifier
extern int TFTP_PANEL_GUI_PERCENT                   ;     // control identifier
extern int TFTP_PANEL_GUI_BLOCK_SENT                ;     // control identifier
extern int TFTP_PANEL_GUI_TOTAL                     ;     // control identifier
extern int TFTP_PANEL_DECORATION                    ;     // control identifier
extern int TFTP_PANEL_DECORATION_2                  ;     // control identifier
extern int TFTP_PANEL_GUI_PROGRAM                   ;     // control identifier
extern int TFTP_PANEL_GUI_MSG                       ;     // control identifier
extern int TFTP_PANEL_BD_PROGRAM                    ;     // control identifier
extern int TFTP_PANEL_BD_MSG                        ;     // control identifier
extern int TFTP_PANEL_BD_COMPLETE                   ;     // control identifier
extern int TFTP_PANEL_GUI_COMPLETE                  ;     // control identifier

#ifdef __cplusplus
    }
#endif

#endif // _SOFTDLOAD_INCLUDED
