
#ifndef ResultTableEntry_HH
#define ResultTableEntry_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sotred in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class:  ResultTableEntry - Entry for a Result Table.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Persistent-Objects/vcssrc/ResultTableEntry.hhv   25.0.4.0   19 Nov 2013 14:18:54   pvcs  $
//
//@ Modification-Table
//
//  Revision: 002  By:  sah    Date:  01-Jan-1999    DCS Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' to a non-inlined method.
//
//  Revision: 001  By:  sah    Date:  15-Mar-1997    DCS Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//          Initial version (Integration baseline).
//
//====================================================================

//@ Usage-Classes
#include "CrcUtilities.hh"
#include "NovRamBoolean.hh"
#include "DblBuff_ResultEntryData.hh"
//@ End-Usage


class ResultTableEntry
{
#if defined(SIGMA_DEVELOPMENT)
    friend Ostream&  operator<<(Ostream&                ostr,
				const ResultTableEntry& tableEntry);
#endif  // defined(SIGMA_DEVELOPMENT)

  public:
    ResultTableEntry(void);
    ~ResultTableEntry(void);

    NovRamStatus::InitializationId  initialize(void);

    Boolean  isClear(void) const;
    void     clear  (void);

    inline Boolean  isValid(void);

    void  startingTest(void);
    void  intermediateResultLogged(
			   const TestResultId resultOfIntermediateStep
			       	  );
    void  completedTest(void);
    void  override(void);

    inline void  getResultData(ResultEntryData& rResultData) const;

    inline void  updateResultData(const ResultEntryData& newResultData);

    void  setToNotApplicable(void);

    void  operator=(const ResultTableEntry& tableEntry);

    inline Boolean  operator==(const ResultTableEntry& tableEntry) const;
    inline Boolean  operator!=(const ResultTableEntry& tableEntry) const;

    static void  SoftFault(const SoftFaultID softDiagID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL,
			   const char*       pPredicate = NULL);

  private:
    //@ Data-Member:  resultData_
    //  Double-buffered instance of 'ResultEntryData' information.
    DoubleBuff(ResultEntryData)  resultData_;
};


// Inlined methods..
#include "ResultTableEntry.in"


//====================================================================
//
//  Global Constants...
//
//====================================================================

#include "SmTestId.hh"

//@ Constant:  MAX_EST_RESULTS 
// Constant that indicates how many EST results -- and, therefore, tests --
// that are to be stored in the EST Result Table.
enum { MAX_EST_RESULTS = SmTestId::EST_TEST_MAX };


//@ Constant:  MAX_SST_RESULTS 
// Constant that indicates how many SST results -- and, therefore, tests --
// that are to be stored in the SST Result Table.
enum { MAX_SST_RESULTS =  SmTestId::SST_TEST_MAX };


//@ Constant:  MAX_TEST_ENTRIES
// Constant that indicates the size of the 'FixedArray(,)' to be used to
// retrieve the results from either the EST or SST Result Table.  NOTE:  this
// MUST always be at least as big as the biggest, above.
enum { MAX_TEST_ENTRIES = ::MAX_EST_RESULTS };


#endif // ResultTableEntry_HH 
