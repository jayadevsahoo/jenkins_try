#ifndef EventFilter_HH
#define EventFilter_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: EventFilter - implements a filter for reporting events
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header::   /840/Baseline/BD-IO-Devices/vcssrc/EventFilter.hhv   10.7   08/17/07 09:29:12   pvcs  
//
//@ Modification-Log
//
//  Revision: 002  By:  syw    Date:  16-Aug-1999    DR Number: 5511
//       Project:  840
//       Description:
//			Added alarmInit_[] and ventStatusInit_[] to keep track whether
//			the coreesponding Debouncer objects needs initialization.
//
//  Revision: 001  By:  syw    Date:  28-Jul-1999    DR Number: 5416
//       Project:  840
//       Description:
//			Initial revision
//
//====================================================================

#include "Sigma.hh"
#include "BD_IO_Devices.hh"
#include "Debouncer.hh"

//@ Usage-Classes

//@ End-Usage

class EventFilter {
  public:
  	enum AlarmFilterType { WALL_AIR = 0, WALL_O2, COMP_OPTION, COMP_AIR, NUM_ALARM_FILTERS } ;
  	enum VentStatusFilterType { SVO = 0, COMP_OPERATING, COMP_READY, NUM_VENT_STATUS } ;
  	
    EventFilter( void) ;
    ~EventFilter( void) ;

    inline void registerAlarmCallBack( PAlarmCallBack pAlarmCallBack );
    inline void registerVentStatusCallBack( PVentStatusCallBack pVentStatusCallBack );

    static void SoftFault(  const SoftFaultID softFaultID,
							const Uint32      lineNumber,
							const char*       pFileName  = NULL, 
			 				const char*       pPredicate = NULL) ;

	void postAlarm( const EventFilter::AlarmFilterType type,
					const BdAlarmId::BdAlarmIdType bdAlarmId,
			   		const Boolean initInProg) ;

	void postEventStatus( const EventFilter::VentStatusFilterType type,
                          const EventData::EventStatus status,
                          const EventData::EventPrompt prompt = EventData::NULL_EVENT_PROMPT) ;

	void newSecondaryCycle( void) ;
                          
  protected:

  private:
    EventFilter( const EventFilter&) ;  			// not implemented...
    void   operator=( const EventFilter&) ; 	// not implemented...

	//@ Data-Member: wallAirDebouncer_
	// debouncer for wall air alarms
	Debouncer wallAirDebouncer_ ;

	//@ Data-Member: wallO2Debouncer_
	// debouncer for wall o2 alarms
	Debouncer wallO2Debouncer_ ;

	//@ Data-Member: compOptionDebouncer_
	// debouncer for compressor installed
	Debouncer compOptionDebouncer_ ;

	//@ Data-Member: compAirDebouncer_
	// debouncer for compressor air
	Debouncer compAirDebouncer_ ;

	//@ Data-Member: svoDebouncer_
	// debouncer for svo event
	Debouncer svoDebouncer_ ;

	//@ Data-Member: compOperatingDebouncer_
	// debouncer for compressor operating event
	Debouncer compOperatingDebouncer_ ;

	//@ Data-Member: compReadyDebouncer_
	// debouncer for compressor ready event
	Debouncer compReadyDebouncer_ ;

	//@ Data-Member: *pAlarmDebouncer_[ NUM_ALARM_FILTERS]
	// pointer for a list of alarm debouncers
	Debouncer *pAlarmDebouncer_[ NUM_ALARM_FILTERS] ;

	//@ Data-Member: *pVentStatusDebouncer_[ NUM_VENT_STATUS]
	// pointer for a list of vent status debouncers
	Debouncer *pVentStatusDebouncer_[ NUM_VENT_STATUS] ;

	//@ Data-Member: alarmInit_[NUM_ALARM_FILTERS]
	// is initialization required
	Boolean alarmInit_[NUM_ALARM_FILTERS] ;

	//@ Data-Member: ventStatusInit_[NUM_VENT_STATUS]
	// is initialization required
	Boolean ventStatusInit_[NUM_VENT_STATUS] ;

    //@ Data-Member: pAlarmCallBack_
    // pointer to function called for posting alarm messages.
    PAlarmCallBack pAlarmCallBack_ ;

    //@ Data-Member: pVentStatusCallBack_
    // pointer to function called for posting vent status messages.
    PVentStatusCallBack pVentStatusCallBack_;
} ;

#include "EventFilter.in"

#endif // EventFilter_HH 
