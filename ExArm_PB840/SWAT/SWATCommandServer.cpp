#include "stdafx.h"
////////////////Copyright (c) 2013 Covidien/////////////////////////////////////
/*******************************************************************************
Name:		SWATCommandServer.cpp

Purpose:
	Implementation file for SWATCommandServer class.

REVISION HISTORY:

REVISION: 001	BY: SF		DATE: 30-JUL-2013
PROJECT: e600 vent
DR NUMBER: N / A
DESCRIPTION:
	Initialize version.

END OF REVISIONS

*******************************************************************************/
#include "Sigma.hh"
#include "SWATCommandServer.h"
// for execute command
#include "SWATAgent.h"
#include "HandlerFactory.h"
#include "Handlers.h"

#include "ScreenCaptureMgr.h"
#include "UdpCommunication.h"
#include "PortIds.hh"

static Uint CommandServerMem[(sizeof(SWATCommandServer)+sizeof(Uint) -1)/sizeof(Uint)];
static Uint UdpCommMem[(sizeof(UdpCommunication)+sizeof(Uint) -1)/sizeof(Uint)];
SWATCommandServer& SWATCommandServer::RSWATCommandServer_ = *((SWATCommandServer*) CommandServerMem);
UdpCommunication& SWATCommandServer::RUdpComm_ = *((UdpCommunication*) UdpCommMem);

// implementations of class SWATCommandServer
SWATCommandServer::SWATCommandServer()
{
	 new (&RUdpComm_) UdpCommunication();

     RUdpComm_.setup( UdpCommunication::UDP_BOTH, SWAT_GUI_COMMAND_UDP_PORT,
         NULL, SWAT_PC_UDP_PORT );
}

bool SWATCommandServer::serverMain()
{
    bool    forever = true;
    char    recvbuf[SWAT_MAX_MSG_LENGTH];
    DWORD   bytesRead = 0;
    DWORD   bytesWritten = 0;

    while ( forever )
    {
        memset( recvbuf, 0, SWAT_MAX_MSG_LENGTH );
        bool dataIsReady = RUdpComm_.readData( (Uint8*)recvbuf, SWAT_MAX_MSG_LENGTH, &bytesRead, 1000 );

        if ( dataIsReady )
        {
            // parse and then execute the command msg
            SWATStatus result = ParseExecCmd( recvbuf );

            // command handler was not created successfully
            if ( result != SWAT_SUCCESS )
            {
                sprintf(m_msg,	"<m>"
                    "<h t='%d' s='%d' m='%d'/>"
                    "<error>%d</error>"
                    "</m>"
                    , header.transactionId, header.seconds, header.microseconds, result);
            }

            RUdpComm_.writeData( (Uint8*)m_msg, strlen(m_msg), &bytesWritten );
        }
    }

	return true;
}

SWATStatus SWATCommandServer::ParseExecCmd(const char * buffer)
{
	SWATStatus parseErr = SWAT_ERROR;
	do
	{
		if (buffer == NULL)
			break;

		const CHAR* pStart = &buffer[0];
		const CHAR* pEnd = &buffer[0];

		// search for '<m>'
		pStart = strstr(pStart, "<m>");
		if (pStart == NULL)
			break;

		// search for '</m>' from where we found '<m>'
		pEnd = strstr(pStart, "</m>");
		if (pEnd == NULL)
    		break;

		// sanity check
		int msg_len = (pEnd - pStart + 4);
		if (msg_len > SWAT_MAX_MSG_LENGTH)
		{
			// valid token, but too long for us to process
			break;
		}

		// copy message into local buffer for parsing
		memset(m_msg, 0, SWAT_MAX_MSG_LENGTH);
		memcpy(m_msg, pStart, msg_len);

		// give message to XML parser
		m_doc.Clear();
		m_doc.Parse(m_msg);

		// get the XML fragment root element
		TiXmlElement* ele = m_doc.RootElement();
		if (!ele)
			break;

		// element m must start each XML message
		if (strcmp(ele->Value(), MESSAGE_ROOT) != 0)
			break;

		// parse the message header
		ele = ParseHeader(ele, header);
		if (!ele)
			break;

		// get the next element after the header
		ele = ele->NextSiblingElement();
		if (!ele)
			break;

		CommandHandlerBase* handler = CommandHandlerFactory::QueryHandler(ele->Value());

		// no handler for current command
		if (handler == NULL)
		{
			parseErr = SWAT_HANDLER_UNDEFINED;
			break;
		}

		handler->ProcessCommandBody(ele);

		// build response text
		sprintf(m_msg, "<?xml version='1.0' encoding='utf-8'?><m><h t='%d' s='%d' m='%d'/>%s</m>", header.transactionId,
			header.seconds, header.microseconds, handler->GetResponse());

		parseErr = SWAT_SUCCESS;
	} while ( false );
	
	return parseErr;
}

TiXmlElement * SWATCommandServer::ParseHeader(TiXmlElement * ele, CommandMsgHeader & s)
{
	// find the h (header) element
	ele = ele->FirstChildElement(HEADER);
	if (!ele)
	{
		m_parseErr = true;
		return NULL;
	}

	// extract header attributes
	if (ele->QueryIntAttribute( "t", &s.transactionId ) != TIXML_SUCCESS)
		m_parseErr = true;
	if (ele->QueryIntAttribute( "s", &s.seconds ) != TIXML_SUCCESS)
		m_parseErr = true;
	if (ele->QueryIntAttribute( "m", &s.microseconds ) != TIXML_SUCCESS)
		m_parseErr = true;

	return ele;
}


void SWATCommandServer::Initialize()
{
	// placement new SWAT command server
	new (&RSWATCommandServer_) SWATCommandServer();
    SWATAgent::Initialize();
}

void SWATCommandServer::Task()
{
	RSWATCommandServer_.serverMain();
}
