#ifndef Breath_Delivery_HH
#define Breath_Delivery_HH
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sotred in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: Breath_Delivery - SubSystem definitions
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/Breath_Delivery.hhv   25.0.4.0   19 Nov 2013 13:59:42   pvcs  $
//
//@ Modification-Log
//  Revision: 023   By: erm   Date: 07-July-2008    SCR Number: 6436
//  Project:  PROX
//  Description:
//      Added ProxManager to class id
//
//  Revision: 022   By: rhj   Date: 07-July-2008    SCR Number: 6435
//  Project:  LEAK
//  Description:
//       Leak Compensation project-related changes:
//        Added PED_MAX_FLOW_LEAK and NEO_MAX_FLOW_LEAK.
//   
//  Revision: 021   By: gdc   Date:  22-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Added RM objects and initialization.
//
//  Revision: 020  By: cep     Date:  17-Apr-2002    DR Number: 5899
//  Project:  VCP
//  Description:
//		Renamed obsoleted classes as UNUSED_RESERVED_n.
//
//  Revision: 019   By: syw   Date:  24-May-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//		PAV project-related changes:
//		* added PAVPHASE, PAVPAUSEPHASE, PAVMANAGER, COMPENSATIONBASEDPHASE,
//		  PAVFILTERS
//
//  Revision: 019  By: jja     Date:  17-May-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//		VTPC project-related changes:
//		Added VTPCVPHASE, VSVPHASE, VOLUMETARGETEDMANAGER,
//			VOLUMETARGETEDCONTROLLER.
//
//  Revision: 018  By: syw     Date:  21-Mar-2000    DR Number: 5611
//  Project:  NeoMode
//  Description:
//		Added comment to note obsoleted classes.
//
//  Revision: 017  By: sah     Date:  22-Sep-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//      NeoMode-specific changes:
//      *  added neonatal-specific maximum flow value
//
//  Revision: 016  By: yyy     Date:  5-May-1999    DR Number: 5322
//  Project:  ATC
//  Description:
//      Added TC required triggers and objects enums.
//
//  Revision: 015  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//		ATC initial release.
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 014  By:  syw    Date:  08-Dec-1997    DR Number: none
//       Project:  BiLevel (840)
//       Description:
//		 	BiLevel initial version.  Added BILEVELSCHEDULER,
//			BILEVELTIMEINSPTRIGGER, INSPIRATORYPAUSEPHASE, LOWTOHIGHPEEPPHASE,
//			HILEVELPSVPHASE.
//
//  Revision: 013  By:  iv    Date:  09-May-1997    DR Number: DCS 675
//       Project:  Sigma (840)
//       Description:
//             Hard coded module ID numbers.
//
//  Revision: 012  By:  iv    Date:  07-Feb-1997    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Rework per formal code review.
//
//  Revision: 011  By:  iv    Date:  23-Jan-1997    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Rework per formal code review.
//
//  Revision: 010  By:  sp    Date:  9-Jan-1997    DR Number: DCS 1339
//       Project:  Sigma (R8027)
//       Description:
//             Add HighCircuitPressureExpTrigger.
//
//  Revision: 009  By:  sp    Date:  31-Oct-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Changed PEEPRECOVERYTRIGGER to PEEPRECOVERYINSPTRIGGER.
//
//  Revision: 008 By: sp   Date:   10-Oct-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//			Added SPIROWFTASK.
//
//  Revision: 007 By: iv   Date:   07-Sep-1996    DR Number: DCS 10035
//       Project:  Sigma (R8027)
//       Description:
//			Added PEEPRECOVERYTRIGGER.
//
//  Revision: 006 By: syw   Date:  06-May-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//			Added PEEPRECOVERYPHASE.
//
//  Revision: 005 By: syw    Date: 10-Apr-1996   DR Number: DCS 669
//  	Project:  Sigma (R8027)
//		Description:
//			Removed RELIEF_FLOW_TARGET... no longer used.
//
//  Revision: 004  By: syw    Date:  29-Feb-1996    DR Number: DCS 600 
//       Project:  Sigma (R8027)
//       Description:
//             Added ARTS classId.
//
//  Revision: 003  By:  iv    Date:  28-Feb-1996    DR Number: DCS 675 
//       Project:  Sigma (R8027)
//       Description:
//             Sorted and enumerated Breath_DeliveryClassID's.
//
//  Revision: 002  By:  iv    Date:  12-Feb-1996    DR Number: DCS 600, 674 
//       Project:  Sigma (R8027)
//       Description:
//             Deleted PIPEDRIFTTEST, added USEREVENTBUFFER.
//             Removed #ifndef directives surrounding #include directives.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "Sigma.hh"
#  include "DiscreteValue.hh"

//@ Usage-Classes

//@ End-Usage

// Note: Add new classes at the end of the list
enum Breath_DeliveryClassID {
    ACSCHEDULER                      = 0, 
    AIRFLOWSENSORTEST                = 1, 
    AIRPSOLSTUCKTEST                 = 2, 
    AIRPSOLSTUCKOPENTEST             = 3, 
    APNEAAUTORESETTRIGGER            = 4, 
    APNEAINTERVAL                    = 5, 
    APNEASCHEDULER                   = 6, 
    ARTS                             = 7, 
    ASAPINSPTRIGGER                  = 8, 
    UNUSED_RESERVED_1                = 9, 		// place holder
    AVERAGEDBREATHDATA               = 10, 
    BDALARMS                         = 11, 
    BDGUICOMMSYNC                    = 12, 
    BDGUIEVENT                       = 13, 
    BDSYSTEMSTATE                    = 14, 
    BDSYSTEMSTATEHANDLER             = 15, 
    BDTASKS                          = 16, 
    BREATHDATA                       = 17, 
    BREATHDELIVERY                   = 18, 
    BREATHPHASE                      = 19, 
    BREATHPHASESCHEDULER             = 20, 
    BREATHRECORD                     = 21, 
    BREATHSET                        = 22, 
    BREATHSETITERATOR                = 23, 
    BREATHTRIGGER                    = 24, 
    BREATHTRIGGERMEDIATOR            = 25, 
    BTPS                             = 26, 
    CIRCUITCOMPLIANCE                = 27, 
    CYCLETIMER                       = 28, 
    DELIVEREDFLOWEXPTRIGGER          = 29, 
    DISCAUTORESETTRIGGER             = 30, 
    DISCONNECTPHASE                  = 31, 
    UNUSED_RESERVED_2                = 32, 		// place holder
    DISCONNECTSCHEDULER              = 33, 
    DISCONNECTTRIGGER                = 34, 
    EXHALATIONPHASE                  = 35, 
    EXHFLOWSENSORFLOWTEST            = 36, 
    EXHHEATERCONTROLLER              = 37, 
    EXHPRESSURESENSORTEST            = 38, 
    UNUSED_RESERVED_3                = 39, 		// place holder
    EXHVALVEICONTROLLER              = 40, 
    EXPIRATORYPAUSEPHASE             = 41, 
    FLOWCONTROLLER                   = 42, 
    HIGHCIRCUITPRESSUREEXPTRIGGER    = 43, 
    HIGHCIRCUITPRESSUREINSPTRIGGER   = 44, 
    HIGHVENTPRESSUREEXPTRIGGER       = 45, 
    IDLEMODESCHEDULER                = 46, 
    IMMEDIATEBREATHTRIGGER           = 47, 
    IMMEDIATEMODETRIGGER             = 48, 
    INSPPRESSURESENSORTEST           = 49, 
    INTERVALTIMER                    = 50, 
    MODETRIGGER                      = 51, 
    MODETRIGGERMEDIATOR              = 52, 
    NETFLOWINSPTRIGGER               = 53, 
    O2FLOWSENSORTEST                 = 54, 
    O2MIXTURE                        = 55, 
    O2PSOLSTUCKTEST                  = 56, 
    O2PSOLSTUCKOPENTEST              = 57, 
    UNUSED_RESERVED_4                = 58, 		// place holder
    UNUSED_RESERVED_5                = 59,		// place holder
    OCCLUSIONTRIGGER                 = 60, 
    OPERATORINSPTRIGGER              = 61, 
    OSCEXHPHASE                      = 62, 
    OSCPCVPHASE                      = 63, 
    OSCSCHEDULER                     = 64, 
    OSCTIMEINSPTRIGGER               = 65, 
    PATIENTTRIGGER                   = 66, 
    PCVPHASE                         = 67, 
    PEEP                             = 68, 
    PEEPRECOVERYPHASE                = 69, 
    PEEPRECOVERYINSPTRIGGER          = 70, 
    PESTUCKTEST                      = 71, 
    PISTUCKTEST                      = 72, 
    POWERUPSCHEDULER                 = 73, 
    PRESSUREBASEDPHASE               = 74, 
    PRESSURECONTROLLER               = 75, 
    PRESSUREEXPTRIGGER               = 76, 
    PRESSUREINSPTRIGGER              = 77, 
    PRESSURESVCTRIGGER               = 78, 
    PRESSUREXDUCERAUTOZERO           = 79, 
    PSVPHASE                         = 80, 
    SAFESTATEPHASE                   = 81, 
    SAFETYNETRANGETEST               = 82, 
    SAFETYNETTEST                    = 83, 
    SAFETYNETTESTMEDIATOR            = 84, 
    SAFETYPCVSCHEDULER               = 85, 
    SAFETYVALVECLOSEPHASE            = 86, 
    SIMVSCHEDULER                    = 87, 
    SIMVTIMEINSPTRIGGER              = 88, 
    SMSWITCHCONFIRMATION             = 89, 
    SPONTSCHEDULER                   = 90, 
    SPIROWFTASK                      = 91, 
    STANDBYSCHEDULER                 = 92, 
    STARTUPPHASE                     = 93, 
    SVCLOSEDLOOPBACKTEST             = 94, 
    SVORESETTRIGGER                  = 95, 
    SVOSCHEDULER                     = 96, 
    SVOPENEDLOOPBACKTEST             = 97, 
    SVOTRIGGER                       = 98, 
    UNUSED_RESERVED_6                = 99, 		// place holder
    TIMERBREATHTRIGGER               = 100, 
    TIMERMEDIATOR                    = 101, 
    TIMERMODETRIGGER                 = 102, 
    TIMERTARGET                      = 103, 
    TRIGGER                          = 104, 
    UIEVENT                          = 105, 
    UIEVENTBUFFER                    = 106, 
    VCVPHASE                         = 107, 
    VENTANDUSEREVENTSTATUS           = 108, 
    VENTSETUPCOMPLETETRIGGER         = 109, 
    VOLUMECONTROLLER                 = 110, 
    WAVEFORM                         = 111, 
    WAVEFORMRECORD                   = 112, 
    WAVEFORMSET                      = 113, 
    UNUSED_RESERVED_7                = 114,		// place holder
    BILEVELSCHEDULER				 = 115,
    BILEVELLOWTOHIGHINSPTRIGGER		 = 116,
    BILEVELHIGHTOLOWEXPTRIGGER		 = 117,
    INSPIRATORYPAUSEPHASE			 = 118,
    LOWTOHIGHPEEPPHASE				 = 119,
    HILEVELPSVPHASE					 = 120,
    MANEUVER						 = 121,
    INSPPAUSEMANEUVER				 = 122,
    EXPPAUSEMANEUVER				 = 123,
    SUMMATIONBUFFER					 = 124,
    LINEARREGRESSION				 = 125,
    STATICMECHANICSBUFFER			 = 126,
    OSCBILEVELPHASE					 = 127,
    UNUSED_RESERVED_8                = 128,		// place holder
    AVERAGEDSTABILITYBUFFER          = 129,
    LUNGDATA						 = 130,
    TUBE							 = 131,
    TCVPHASE						 = 132,
    LUNGFLOWEXPTRIGGER				 = 133,
    LUNGVOLUMEEXPTRIGGER			 = 134,
    PIDPRESSURECONTROLLER			 = 135,
    PEEPCONTROLLER					 = 136,
    PAUSEPRESSTRIGGER       	     = 137,
    HIGHPRESSCOMPEXPTRIGGER			 = 138,
    VSVPHASE						 = 139,
    VTPCVPHASE						 = 140,
    VOLUMETARGETEDMANAGER			 = 141,
    VOLUMETARGETEDCONTROLLER		 = 142,
    BDSIGNAL						 = 143,
    PAVPHASE						 = 144,
    PAVPAUSEPHASE					 = 145,
    PAVMANAGER						 = 146,
	COMPENSATIONBASEDPHASE       	 = 147,
	PAVFILTERS             			 = 148,
	NIFMANEUVER        				 = 149,
	P100MANEUVER        			 = 150,
	VITALCAPACITYMANEUVER        	 = 151,
    PRESSURESETINSPTRIGGER           = 152,
	VCMPSVPHASE						 = 153,
	P100PAUSEPHASE                   = 154,
	NIFPAUSEPHASE                    = 155,
	DYNAMICMECHANICS                 = 156,
	LEAKCOMPMGR                      = 157,
	PROXMANAGER                      = 158
};


//@ Constant: BASE_FLOW_MIN
//minimum base flow
extern const Real32 BASE_FLOW_MIN;

//@ Constant: ADULT_MAX_FLOW
// max flow for adult circuit
extern const Real32 ADULT_MAX_FLOW;

//@ Constant: PED_MAX_FLOW
// max flow for pediatric circuit
extern const Real32 PED_MAX_FLOW;

//@ Constant: NEO_MAX_FLOW
// max flow for neonatal circuit
extern const Real32 NEO_MAX_FLOW;

//@ Constant: PED_MAX_FLOW_LEAK
// max flow leak for pediatric circuit
extern const Real32 PED_MAX_FLOW_LEAK;

//@ Constant: NEO_MAX_FLOW_LEAK
// max flow leak for neonatal circuit
extern const Real32 NEO_MAX_FLOW_LEAK;

//@ Constant: PRESSURE_TO_SEAL
//pressure above Pe, required to seal the exhalation valve 
extern const Real32 PRESSURE_TO_SEAL;

//@ Constant: PRESSURE_ABOVE_HCP
//pressure threshold above HCP
extern const Real32 PRESSURE_ABOVE_HCP;

//@ Constant: BACKUP_PRESSURE_SENS_VALUE
// backup pressure trigger level during flow triggering
extern const Real32 BACKUP_PRESSURE_SENS_VALUE ;

//@ Constant: SV_CLOSE_TIME_MS
// time for safety valve to close
extern const Int32 SV_CLOSE_TIME_MS ;

//@ Constant: MAX_INSP_TIME_MS
// longest inspiration possible
extern const Int32 MAX_INSP_TIME_MS ;

//@ Constant: NULL_MANDATORY_TYPE
// A value to be associated with an inspiration of no mandatory type 
extern const DiscreteValue NULL_MANDATORY_TYPE; 

//@ Constant: PURGE_FLOW
// flow during exhalation with pressure triggering
extern const Real32 PURGE_FLOW;

class Breath_Delivery
{
  public:
    static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL, 
			  const char*       pPredicate = NULL);

    static void Initialize(void);

  private:
    // these methods are purposely declared, but not implemented...
    Breath_Delivery(const Breath_Delivery&);	// not implemented...
    void   operator=(const Breath_Delivery&);	// not implemented...
 
    // Breath_Delivery is never constructed or destructed
    Breath_Delivery(void);   // not implemented
    ~Breath_Delivery(void);  // not implemented

};


#endif // Breath_Delivery_HH
