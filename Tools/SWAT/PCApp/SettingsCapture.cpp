//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file SettingsCapture.cpp
//----------------------------------------------------------------------------


//-------------------------------------
// I N C L U D E S
//-------------------------------------
#define _CRT_SECURE_NO_WARNINGS

#include "ISettingsCapture.h"
#include "CConfiguration.h"
#include "CLogger.h"
#include <stdio.h>

#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <errno.h>
#include <sys/types.h>
#include <winsock2.h>
#include "Thread.h"

#include "SettingXmitData.h"
#include "PcapHelper.h"

#include<list>




//-------------------------------------
// C L A S S  D E C L
//-------------------------------------

class SettingsCapture:public ISettingsCapture{
public:
    SettingsCapture(char *dev, int (*func)(void *,char *, int)=0, void *obj=0 );
    ~SettingsCapture();

    void destroy(){
        delete this;
    }

    bool addCallback(int (*func)(void *,char *, int), void *obj );
    bool removeCallback(int (*func)(void *,char *, int), void *obj );

    bool register_logger(ILogger *pILogger);

private:
    void ThreadEntranceProc();
    static void *ThreadEntranceProc   (void *pVoid);

    SettingsCapture(const SettingsCapture&);
    SettingsCapture operator=(const SettingsCapture&);
    SettingsCapture();

    bool          m_bRunning;
    pcap_t       *m_pcapHandle;
    Thread       *m_pThread;

    struct {
        struct NetworkMsgHeader header;
        char buffer[
            sizeof(struct SettingXactionBlock)
            +sizeof(struct SettingXmitData)*256
         ];
        } m_data;

    unsigned int m_data_len;

    struct bpf_program m_fp; /* compiled filter program (expression) */
    bpf_u_int32 m_mask;           /* subnet mask */
    bpf_u_int32 m_net;            /* ip */


    class Callback{
        public:
        Callback(int (*func)(void *,char *, int),void *obj):m_func(func),m_obj(obj){}
        int (*m_func)(void *,char *, int);
        void *m_obj;
    };

    std::list<Callback> m_callbackList;

    int (*m_func)(void *,char *, int);
    void *m_obj;

};

#if 0
ISettingsCapture * ISettingsCapture::getInstance(char *dev, int (*func)(void *,char *, int), void *obj ){
    ISettingsCapture *sc = NULL;
    try{
        sc = new SettingsCapture(dev,func,obj);
    }
    catch(...){}

    return sc;
}
#endif
 
//-------------------------------------
// C O D E
//-------------------------------------

//---------------------------
// SettingsCapture 
//---------------------------
SettingsCapture::SettingsCapture(char *dev, int (*func)(void *, char *, int), void *obj ):
	m_bRunning(false),
    m_pcapHandle(NULL),
    m_func(func),
    m_obj(obj)
{
    static char filter_exp[] = 
        "tcp dst port 7001 and (((ip[2:2] - ((ip[0]&0xf)<<2)) - ((tcp[12]&0xf0)>>2)) != 0)"; 

    char errbuf[PCAP_ERRBUF_SIZE];      /* error buffer */

    LOG_MESSAGE(LM_INFO,"SettingsCapture(%X):SettingsCapture() constructor called.",this); 

    dev = PcapHelper::getDeviceList(dev);

    if(dev == NULL) 
    {
        LOG_MESSAGE(LM_CRITICAL,"SettingsCapture(%X):SettingsCapture() could not find capture device.",this); 
        throw -1;
        //TODO[ME]:Throw an error
        //dev = pcap_lookupdev(errbuf);
    }
    
    /* get network number and mask associated with capture device */
    if (pcap_lookupnet(dev, &m_net, &m_mask, errbuf) == -1) 
    {
        m_net = 0, m_mask = 0;

        LOG_MESSAGE(LM_CRITICAL,"SettingsCapture(%X):SettingsCapture() could not find network of device.",this); 
        throw -1;
        //TODO[ME]:Throw an error
    }


    /* open capture device */
    m_pcapHandle = pcap_open_live(dev, SNAP_LEN, 1, 250, errbuf);
    if (m_pcapHandle == NULL) 
    {
        LOG_MESSAGE(LM_CRITICAL,"SettingsCapture(%X):SettingsCapture() could open device.",this); 
        throw -1;
        //TODO[ME]:Throw an error
    }


    if (pcap_datalink(m_pcapHandle) != DLT_EN10MB) 
    {
        LOG_MESSAGE(LM_CRITICAL,"SettingsCapture(%X):SettingsCapture() device is not ethernet.",this); 
        throw -1;
        //TODO[ME]:Throw error.
    }

    /* compile the filter expression */
    if (pcap_compile(m_pcapHandle, &m_fp, filter_exp, 0, m_net) == -1) 
    {
        LOG_MESSAGE(LM_CRITICAL,"SettingsCapture(%X):SettingsCapture() could not create filter.",this); 
        pcap_close(m_pcapHandle);
        throw -1;
        //TODO[ME]:Throw error.
    }



    // apply the compiled filter 
    if (pcap_setfilter(m_pcapHandle, &m_fp) == -1) 
    {
        pcap_freecode(&m_fp);
        pcap_close(m_pcapHandle);

        LOG_MESSAGE(LM_CRITICAL,"SettingsCapture(%X):SettingsCapture() could not apply filter.",this); 
        throw -1;

        //TODO[ME]:Throw error.
    }

    m_pThread = new Thread(ThreadEntranceProc, (void *)this);
}

//---------------------------
// ~ISettingsCapture 
//---------------------------
ISettingsCapture::~ISettingsCapture() {
    LOG_MESSAGE(LM_INFO,"ISettingsCapture(%X):ISettingsCapture() destructor called.",this); 
}

//---------------------------
// ~SettingsCapture 
//---------------------------
SettingsCapture::~SettingsCapture()
{
	m_bRunning = false;

    LOG_MESSAGE(LM_INFO,"SettingsCapture(%X):SettingsCapture() destructor called.",this); 

    PcapHelper::disable_pcap();

	m_pThread->join();
    delete m_pThread;
    pcap_freecode(&m_fp);
    pcap_close(m_pcapHandle);

    PcapHelper::reset_pcap();
}

//---------------------------
// addCallback 
//---------------------------
bool SettingsCapture::addCallback(int (*func)(void *,char *, int), void *obj )
{
    m_callbackList.push_back(Callback(func,obj));

    return true;
}

//---------------------------
// removeCallback 
//---------------------------
bool SettingsCapture::removeCallback(int (*func)(void *,char *, int), void *obj )
{
    std::list<Callback>::iterator it = m_callbackList.begin();

    for(;it!=m_callbackList.end();it++){
        if( (it->m_func == func ) && (it->m_obj == obj)  )
        {
            m_callbackList.erase(it);
            return true;
        }
    }

    return false;
}

//---------------------------
// ThreadEntranceProc
//---------------------------
void *SettingsCapture::ThreadEntranceProc    (void *pVoid)
{
	((SettingsCapture* )pVoid)->ThreadEntranceProc ();	

    Thread::exit();

	return NULL;
}



//---------------------------
// ThreadEntranceProc
//---------------------------
void SettingsCapture::ThreadEntranceProc()
{
    m_bRunning = true;

    LOG_MESSAGE(LM_INFO,"SettingsCapture(%X)::ThreadEntranceProc() is started.");

    while(m_bRunning){

        int rlen;
        int dlen;

        try {
            rlen = PcapHelper::readblock (m_pcapHandle,
                    (char *)&m_data.header, sizeof(m_data.header));
        }
        catch(...){
            break;
        }


        //Make sure there is enough to read for the packet header
        if(rlen != sizeof(m_data.header))
        {
            continue;
        }


        // Make sure that there is more data to read
        if(((int )htons(m_data.header.pktSize))){
            unsigned int nsettings;
            unsigned int nbytes;

            //Read the remainder of the message
            try {
            dlen=  PcapHelper::readblock (m_pcapHandle, (char *)&m_data.buffer, 
                    min(((int )htons(m_data.header.pktSize)),sizeof(m_data.buffer)));
            }
            catch(...){
                break;
            }

            //Make sure that we are reading correct packet size and type.
            if( 
                dlen!= ((int )htons(m_data.header.pktSize)) || 
                htons(m_data.header.msgId)!=9906            ||
                dlen < sizeof(SettingXactionBlock)
            ) 
            {
                LOG_MESSAGE(LM_NOTICE,"SettingsCapture(%X):::ThreadEntranceProc()"
                        " Invalid message header."
                        ,this); 

                continue;
            }
            
            nsettings = (unsigned int)
                htons( ((struct SettingXactionBlock *)m_data.buffer)->numXmitElems);

            nbytes    = 
                sizeof(struct SettingXactionBlock)
                +nsettings*sizeof(struct SettingXmitData);



            //Make sure that data is sane.
            if( nbytes != dlen ){
                LOG_MESSAGE(LM_NOTICE,"SettingsCapture(%X):::ThreadEntranceProc()"
                        " Invalid message header."
                        ,this); 
                continue;
            }

            if( nsettings ){

            
                m_data_len = dlen+rlen;
#if 1
                std::list<Callback>::iterator it = m_callbackList.begin();

                for(;it!=m_callbackList.end();it++){
                    try{
                        it->m_func(it->m_obj,(char *)&m_data,m_data_len);
                    }
                    catch(...){

                        LOG_MESSAGE(LM_NOTICE,"SettingsCapture(%X):::ThreadEntranceProc()"
                                " Exception caught."
                                ,this); 

                        //Error
                    }
                }
#endif

                if(m_func){
                    try{
                        m_func(m_obj,(char *)&m_data,m_data_len);
                    }
                    catch(...){

                        LOG_MESSAGE(LM_NOTICE,"SettingsCapture(%X):::ThreadEntranceProc()"
                                " Exception caught."
                                ,this); 

                        //Error
                    }
                }

            }
        }
    }

    LOG_MESSAGE(LM_INFO,"SettingsCapture(%X)::ThreadEntranceProc() is stopped.");
}



bool SettingsCapture::register_logger(ILogger *pILogger)
{
    LOG_MESSAGE(LM_DEBUG,"Registering of logger.");
    g_GlobalLogger.set(pILogger);
    return true; 
}



#ifdef __cplusplus    // If used by C++ code, 
extern "C" {          // we need to export the C interface
#endif

//---------------------------
// getSettingsCaptureInstance 
//---------------------------
__declspec(dllexport) ISettingsCapture * __cdecl 
    getSettingsCaptureInstance(char *dev, int (*func)(void *, char *, int), void *obj )
{

    ISettingsCapture *sc = NULL;
    try{
        sc = new SettingsCapture(dev,func,obj);
    }
    catch(...){
        LOG_MESSAGE(LM_NOTICE,"SettingsCapture():::ThreadEntranceProc()"
                " Exception caught."
                ); 
    
    }

    return sc;

}
 
#ifdef __cplusplus
}
#endif




