#pragma once



class AioDioDriver_t
{
protected:
	HANDLE			hAioDioDriver;
	CRITICAL_SECTION AioDioCriticalSection;
	unsigned char		BSPType;

	HANDLE			hSPI1_CH0;
	HANDLE			hSPI1_CH3;
	HANDLE			hSPI4_CH0;
	HANDLE			hActiveSPI;
	HANDLE			hSPI3_CH0;
	HANDLE			hSPI3_CH1;

public:
	AioDioDriver_t(void);
	~AioDioDriver_t(void);

	
	// set up the AD7923 ADC and the digital I/O
	void            InitAioDio(void);
	bool			ReadKnob(unsigned char *KnobCount, unsigned char *KnobDir);
};
