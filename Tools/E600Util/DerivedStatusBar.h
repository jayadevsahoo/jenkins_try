#pragma once


// CDerivedStatusBar

class CDerivedStatusBar : public CStatusBar
{
	DECLARE_DYNAMIC(CDerivedStatusBar)

public:
	CDerivedStatusBar();
	virtual ~CDerivedStatusBar();

	CProgressCtrl &GetProgressCtrl(){return m_ProgressCtrl;};

protected:
	//{{AFX_MSG(CWzdStatusBar)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	CProgressCtrl m_ProgressCtrl;
};


