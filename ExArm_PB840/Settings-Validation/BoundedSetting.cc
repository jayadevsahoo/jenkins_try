#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//================= C L A S S   D E S C R I P T I O N =================
//@ Class:  BoundedSetting - Bounded Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This class provides the setting's framework for the Bounded Settings.
//  All bounded settings ultimately are derived from this setting.  This
//  class overrides some of the virtual methods of 'Setting' including
//  those to:  respond to an activation by the operator, calculate a new
//  bounded value based on a knob delta, and to reset a bounded setting's
//  value to its value upon activation.  This class also defines new virtual
//  methods are can be overridden by derived classes.  These new virtual
//  methods include methods to:  get this bounded setting's default and
//  new-patient value, get this bounded setting's "accepted" and "adjusted"
//  value, and get the maximum and minimum limit values of a bounded alarm
//  setting.  These methods are overridden, if needed, by the derived
//  bounded settings to provide the specific behavior needed.
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to establish the bounded setting's framework
//  for the rest of the bounded settings to build upon.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class is initializes its hard bound instances with the IDs and
//  values of each of the corresponding bounds (i.e., upper and lower).
//
//  This class also defines a "protected" framework of one virtual method
//  that accepts a new value to be stored in the appropriate context.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/BoundedSetting.ccv   25.0.4.0   19 Nov 2013 14:27:16   pvcs  $
//
//@ Modification-Log
//
//  Revision: 010   By: gdc    Date: 16-Aug-2010     SCR Number: 6663
//  Project: API/MAT
//  Description:
//  Added support for VentSet API.
// 
//  Revision: 009   By: sah    Date: 31-Jan-2000    DR Number: 5616
//  Project:  NeoMode
//  Description:
//      Raised maximum loop count to '30', based on analysis of case
//      documented in this DR.
//
//  Revision: 008   By: sah    Date: 29-Jun-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  added new virtual method, 'resetConstraints()' for resetting
//         all dynamic constraints to their absolute limit counterpart,
//         to ensure correct calculations during new-patient initialization
//	*  re-designed much of the settings framework, moving many methods
//         up to this level, further simplifying, and genericizing, the
//         design
//	*  re-wrote 'calcNewValue()' to better handle the ensuring of
//         proper placement, following a bound violation
//	*  added new 'warpToRange()' method
//	*  added new 'getAbsolute{Min,Max}Value_()' methods
//
//  Revision: 007   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new pre-condition to 'calcNewValue()', to ensure that
//	   only CHANGEABLE settings are directly changed.
//	*  moved 'get{Max,Min}Limit()' methods to 'Setting' base class
//	*  cleaned up DEVELOPMENT-only 'is{Adjusted,Accepted}Valid()' methods
//
//  Revision: 006   By: dosman Date:  16-Sep-1998    DCS Number: BILEVEL 144
//  Project:  BILEVEL
//  Description:
//	Changed calcNewValue() to deal with the special case when the attempt
//	to resolve dependent settings through the call to calcDependentValues_
//	results in the primary setting going beyond a bound and being unable
//	to get back, and unable to settle on a satisfactory value.  When
//	this special case occurs, we calculate a new value by using a knob delta
//	of one in the direction opposite to the direction we originally tried
//	to change this primary setting, which forces the primary setting 
//	value back into a legal value.  It is assumed that when calcDependentValues_
//	is unable to settle on a satisfactory primary setting value, then this
//	setting value is only one click beyond a satisfactory value.  This 
//	assumption was verified through an extensive unit test.
//
//  Revision: 005   By: dosman Date:  08-Sep-1998    DCS Number: BILEVEL 130
//  Project:  BILEVEL
//  Description:
//	Renamed the method to updateAllSoftBoundStates_
// 	to clarify that all soft bounds of this setting are updated through this method.
//
//  Revision: 004   By: dosman Date:  29-Jul-1998    DCS Number: BILEVEL 142
//  Project:  BILEVEL
//  Description:
//	Changed calcNewValue() because in certain cases, when we turn the knob 
//	quickly, and the setting is limited by a dependent setting's bound,
//	the limit calculated based on the dependent setting's bound is more 
//	restrictive than expected (due to rounding and the difference in coarseness
//	between different settings).  Therefore, if we hit a bound while resolving
//	the new value of the primary setting, then we will attempt to force the setting
//	value one more time in the same direction that it was attempting to go, by one click.
//
//  Revision: 003   By: dosman Date:  16-Jun-1998    DCS Number: 116
//  Project:  BILEVEL
//  Description:
//	Changed value passed into updateSoftBoundState_() to handle the case where value
//	is not changing.
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 002   By: sah    Date:  20-Oct-1997    DCS Number: 2563
//  Project: Sigma (R8027)
//  Description:
//	Added a call to 'calcDependentValues()' to the "special" handling of
//	bound violations causing changes in the opposite direction.  Part of
//	the problem documented in this DCS, was that plateau time's value
//	reverted back to the starting value without updating its dependent
//	settings.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//=====================================================================

#include "BoundedSetting.hh"
#include "SettingConstants.hh"
#include "SettingBoundTypes.hh"

#if defined(SIGMA_DEVELOPMENT)
#  include "Settings_Validation.hh"
#endif // defined(SIGMA_DEVELOPMENT)

//@ Usage-Classes
#include "BoundedRange.hh"
#include "VsetServer.hh"
#include "StringConverter.hh"  
//@ End-Usage

//@ Code...

//===================================================================
//
//  Public Method...
//
//===================================================================

//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  ~BoundedSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

BoundedSetting::~BoundedSetting(void)
{
  CALL_TRACE("~BoundedSetting()");
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  resetConstraints()  [virtual]
//
//@ Interface-Description
//  Reset this setting's minimum and maximum constraints, if any, to their
//  absolute min/max values, to ensure proper new-patient initialization.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Because during New-Patient Setup, their could be "old" patient setting
//  values, and constraints, we need to reset these constraints to ensure
//  proper initialization.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
BoundedSetting::resetConstraints(void)
{
  CALL_TRACE("resetConstraints()");

  // reset min/max constraints...
  valueInterval_.resetMinConstraint();
  valueInterval_.resetMaxConstraint();
}   // $[TI1]


//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  getMaxLimit()  [virtual]
//
//@ Interface-Description
//  Return the maximum limit of this alarm setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Use virtual 'getAbsoluteMaxValue_()' method.
//---------------------------------------------------------------------
//@ PreCondition
//  (SettingId::IsAlarmLimitId(getId()))
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Real32
BoundedSetting::getMaxLimit(void)
{
  CALL_TRACE("getMaxLimit()");
  // "safe" because there is no ill effects if assertion is not true...
  SAFE_AUX_CLASS_PRE_CONDITION((SettingId::IsAlarmLimitId(getId())), getId());

  // update constraints, before getting limit values...
  updateConstraints_();

  Real32  maxLimitValue = getAbsoluteMaxValue_();

  if (maxLimitValue == SettingConstants::UPPER_ALARM_LIMIT_OFF)
  {  // $[TI1] -- the absolute maximum is the "OFF" value...
    // get the 2nd ("next") node value...
    maxLimitValue = valueInterval_.getIntervalList()->pNext->value;
  }  // $[TI2] -- the absolute maximum is appropriate...

  return(maxLimitValue);
}


//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  getMinLimit()  [virtual]
//
//@ Interface-Description
//  Return the minimum limit of this alarm setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Use virtual 'getAbsoluteMinValue_()' method.
//---------------------------------------------------------------------
//@ PreCondition
//  (SettingId::IsAlarmLimitId(getId()))
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Real32
BoundedSetting::getMinLimit(void)
{
  CALL_TRACE("getMinLimit()");
  // "safe" because there is no ill effects if assertion is not true...
  SAFE_AUX_CLASS_PRE_CONDITION((SettingId::IsAlarmLimitId(getId())), getId());

  // update constraints, before getting limit values...
  updateConstraints_();

  Real32  minLimitValue = getAbsoluteMinValue_();

  if (minLimitValue == SettingConstants::LOWER_ALARM_LIMIT_OFF)
  {  // $[TI1] -- the absolute minimum is the "OFF" value...
    // find the 2nd-to-last node...
	const BoundedInterval* pNode;
    for (pNode = valueInterval_.getIntervalList();
	 pNode->pNext->pNext != NULL; pNode = pNode->pNext)
    {
      // do nothing...
    }

    // return "previous" value...
    minLimitValue = pNode->value;
  }  // $[TI2] -- the absolute minimum is appropriate...

  return(minLimitValue);
}


//================ M E T H O D   D E S C R I P T I O N ================
//@ Method:  getDefaultValue()  [const, pure virtual]
//
//@ Interface-Description
//  This is a pure virtual method that is to be overridden by the derived
//  class and defined with appropriate behavior.  This virtual method is 
//  is used to retrieve a setting's default value.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

//@ Not-Implemented


//================ M E T H O D   D E S C R I P T I O N ================
//@ Method:  getNewPatientValue()  [const, pure virtual]
//
//@ Interface-Description
//  This is a pure virtual method that is to be overridden by the derived
//  class and defined with appropriate behavior.  This virtual method is 
//  is used to retrieve a setting's new-patient value.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

//@ Not-Implemented


//================ M E T H O D   D E S C R I P T I O N ================
//@ Method:  getAcceptedValue()  [const, pure virtual]
//
//@ Interface-Description
//  This is a pure virtual method that is to be overridden by derived
//  classes to return this setting's "accepted" value.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

//@ Not-Implemented...


//================ M E T H O D   D E S C R I P T I O N ================
//@ Method:  getAdjustedValue()  [const, pure virtual]
//
//@ Interface-Description
//  This is a pure virtual method that is to be overridden by derived
//  classes to return this setting's "adjusted" value.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

//@ Not-Implemented...


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  setAdjustedValue(newAdjustedValue)  [pure virtual]
//
//@ Interface-Description
//  This is a pure virtual method that is to be overridden by the
//  derived classes to store 'newAdjustedValue' as this setting's new
//  "adjusted" value.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

//@ Not-Implemented


//============== M E T H O D   D E S C R I P T I O N ================
//@ Method:  calcNewValue(knobDelta)  [virtual]
//
//@ Interface-Description
//  This method determines the new setting value from the knob delta.
//  It also checks the new value against its bounds, and updates any
//  dependent settings.  A constant reference to this setting's bound
//  status is returned; the bound status will contain bound information
//  about this setting's violated bound, if any.
//---------------------------------------------------------------------
//@ Implementation-Description
//  (NOTE:  a dependent setting should only cause a primary setting to go
//  AWAY from its bound, not TOWARDS it. When adjusting the primary setting,
//  values for dependent settings are calculated.  The new values are then
//  sent to the dependent settings, where they are checked against their
//  bounds.  If a dependent setting bound is reached, the dependent setting
//  value is clipped to the bound value.  The clipped dependent value is
//  returned to the primary setting.  A new value for the primary
//  setting is then calculated based on the clipped dependent setting.
//  This new value, if the user is increasing the primary setting,
//  will not be less than the original value, or, if the user is
//  decreasing the primary setting, will not be more than the original
//  value. This is why we don't loop through calculating new dependent
//  values.)
// 
//  Notes:
//
//  The flag leaveSettingUnchanged is used to keep track of whether
//  the primary setting ends up not changing, either because the primary
//  setting's bound will not allow the change, or because a dependent 
//  bound will not allow the change:
//  soft bound activation/deactivation depends on whether
//  the setting value changed or did not change.
//
//  The flag continueLooping is set when it is discovered 
//  on the first iteration that there is a bound violation and 
//  either (1) the knobDelta is not one click, or (2) the attempt
//  by calcDependentValues() to resolve the primary setting's change
//  with its dependents could not settle on a satisfactory primary 
//  value.  When the flag is set, it means we will loop through 
//  one more iteration to ensure that the resulting primary setting
//  value is correct.  The flag is reset after we perform 
//  the second iteration.
//
//  After completing the last iteration of the main loop, and
//  before returning the bound status, we check if we had performed
//  a second iteration of the loop, and if so, we restore the bound
//  status that resulted from the first iteration of changing the primary
//  setting.  The bound status that resulted from the first iteration
//  is always the one we want.  For the special case of a primary setting
//  being back-limited too far back by a dependent setting's bound,
//  the second iteration would result in no violation, and we want to
//  report the bound violation.  For the normal case, the bound violation
//  after the first and second iterations will be the same.
//
//  $[02001] -- The validation of a proposed setting value...
//---------------------------------------------------------------------
//@ PreCondition
//  (getApplicability() == Applicability::CHANGEABLE)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

const BoundStatus&
BoundedSetting::calcNewValue(const Int16 knobDelta)
{
  CALL_TRACE("calcNewValue(knobDelta)");

#if !defined(SIGMA_UNIT_TEST)
  // checked when NOT unit testing...
  {
    // batch settings can only be modified in the ADJUSTED context, while
    // non-batch settings can only be modified in the ACCEPTED context...
    const Notification::ChangeQualifier  QUALIFIER_ID =
      (SettingId::IsBatchId(getId())) ? Notification::ADJUSTED	// $[TI17]
				      : Notification::ACCEPTED;	// $[TI18]
    AUX_CLASS_PRE_CONDITION(
		    (getApplicability(QUALIFIER_ID) == Applicability::CHANGEABLE),
			    getId());
  }
#endif // !defined(SIGMA_UNIT_TEST)

#if defined(SIGMA_DEVELOPMENT)
  const Boolean  IS_ADJUSTED_CONTEXT_DEBUG_ON =
			    (Settings_Validation::IsDebugOn(ADJUSTED_CONTEXT));

  if (Settings_Validation::IsDebugOn(BOUNDED_SETTING))
  {
    Settings_Validation::TurnDebugOn(ADJUSTED_CONTEXT);
    cout << "CALC-NEW-00:"
	 << "  id = " << SettingId::GetSettingName(getId())
	 << ", delta = " << knobDelta
	 << endl;
  }
#endif // defined(SIGMA_DEVELOPMENT)

  // use the actual knobDelta initially, but we may need to use
  // a different knob delta below to "fix" the special case
  // when an initial, big delta leads to a backing off of the
  // primary setting that goes slightly back too far because of rounding.
  Int16  knobDeltaToUse = knobDelta;

  // store whether the (variable) knob delta will cause 
  // an increase to this setting's value...
  Boolean  knobDeltaToUseIsIncreasing;

  // only loop through under certain circumstances
  // the purpose of this flag is to tell us when to break out 
  // of the iteration loop
  Boolean continueLooping = FALSE;

  BoundStatus&  rBoundStatus = getBoundStatus_();

  rBoundStatus.setViolationId(getId());

  BoundState      saveBoundStatusState = ::NO_BOUND_VIOLATION;
  SettingBoundId  saveBoundStatusId = ::NULL_SETTING_BOUND_ID;

  // Keep track of how many times we have gone through this loop.
  // The main purpose of this counter is to signal when we have
  // gone through the loop the maximum number of times allowed.
  Uint loopCount = 0u;

  BoundedSetting::ChangeState_  currentChangeState;
  BoundedSetting::ChangeState_  initialChangeState;

  Boolean  currentlySettledOnValidValue = FALSE;
  Boolean  initiallySettledOnValidValue = FALSE;

    // store the original value...
    BoundedValue  originalAdjustedValue = getAdjustedValue();
  do 
  {
    // More than one iteration occurs whenever there is a bound violation in
    // the first iteration.  Subsequent iterations are to ensure that the value
    // "advances" (in the direction of 'knobDelta') far enough to be at the most
    // advanced value that does NOT cause a violation, and still allows the
    // dependents to "settle" in a valid state.  Through initial "paper"
    // analysis it was determined that no more than 4 loops would be needed.
    // However, a real-world example, reported in DCS #5616, found a case where
    // peak inspiratory flow needed as many as 10 loops.  Therefore, to allow
    // for enough loops that may be needed to approach the ideal value, without
    // introducing the risk of a runaway loop, the maximum loop count is as
    // follows: If a runaway loop is detected, then revert this setting back to 
    // its original value to get back to a known valid set of settings.
    if(loopCount >=30)
    {
        setAdjustedValue( originalAdjustedValue );
        calcDependentValues_(knobDeltaToUseIsIncreasing);
        break;
    }

    knobDeltaToUseIsIncreasing = (knobDeltaToUse > 0);

    // store the starting value...
    const BoundedValue  STARTING_VALUE = getAdjustedValue();

    BoundedValue  proposedValue = getAdjustedValue();

    if (loopCount > 0)
    {  // $[TI15]
      // the proposed value may not be a legal value because it was determined
      // by resolving dependents and never warped properly, so warp it if
      // necessary, and make sure it is clipped if it is outside a legal
      // interval...
      if (!(valueInterval_.isLegalValue(proposedValue))) 
      {  // $[TI15.1]
        valueInterval_.warpValue(proposedValue, BoundedRange::WARP_NEAREST, 
				 TRUE);
      }  // $[TI15.2] -- implied else
    }  // $[TI16]

    if (!knobDeltaToUseIsIncreasing)
    {   // $[TI1]
      // decrement the value; the decremented value is returned in
      // 'proposedValue'; if a bound is violated due to the decrementing,
      // 'proposedValue' is "clipped" to the minimum bound value, and
      // 'rBoundStatus' is set up with the bound violation information...
      valueInterval_.decrement(-(knobDeltaToUse), proposedValue, rBoundStatus);
    }
    else
    {   // $[TI2]
      // increment the value; the incremented value is returned in
      // 'proposedValue'; if a bound is violated due to the incrementing,
      // 'proposedValue' is "clipped" to the maximum bound value, and
      // 'rBoundStatus' is set up with the bound violation information...
      valueInterval_.increment(knobDeltaToUse, proposedValue, rBoundStatus);
    }

    SAFE_AUX_CLASS_POST_CONDITION((valueInterval_.isLegalValue(proposedValue)),
			        getId(), BoundedRange);

#if defined(SIGMA_DEVELOPMENT)
    if (Settings_Validation::IsDebugOn(BOUNDED_SETTING))
    {
      cout << "CALC-NEW-01 (" << loopCount << "):"
	   << "  delta = " << knobDeltaToUse
	   << ", START = " << BoundedValue(getAdjustedValue())
	   << ", PROPOSED = " << proposedValue
	   << endl;
    }
#endif // defined(SIGMA_DEVELOPMENT)

    if (proposedValue != STARTING_VALUE)
    {  // $[TI3]
      // the new value is valid against this setting's bounds and different
      // from what is currently stored, therefore store it to allow access to
      // this new value during the dependent setting updates...
      setAdjustedValue(proposedValue);

      // update the dependent settings...
      currentlySettledOnValidValue =
			    calcDependentValues_(knobDeltaToUseIsIncreasing);

      // get the value that results following the dependent calculations...
      proposedValue = getAdjustedValue();

      if ((proposedValue < STARTING_VALUE  &&  knobDeltaToUseIsIncreasing)  ||
	  (proposedValue > STARTING_VALUE  &&  !knobDeltaToUseIsIncreasing))
      {  // $[TI3.1]
	// the resulting value is in the opposite direction of the intended
	// change, therefore revert batck to the starting point...
	setAdjustedValue(STARTING_VALUE);

	// update the dependent settings...
	currentlySettledOnValidValue =
			    calcDependentValues_(knobDeltaToUseIsIncreasing);

	proposedValue = STARTING_VALUE;
      }  // $[TI3.2]

      if (rBoundStatus.isInViolation())
      {  // $[TI3.3] -- bound violation occurred...
	currentChangeState = BoundedSetting::CHANGE__VIOLATION;
      }
      else
      {  // $[TI3.4] -- no bound violation occurred here...
	currentChangeState = BoundedSetting::CHANGE__NO_VIOLATION;
      }

      if (knobDeltaToUseIsIncreasing)
      {  // $[TI3.5]
	updateAllSoftBoundStates_(Setting::IS_INCREASING);
      }
      else
      {  // $[TI3.6]
	updateAllSoftBoundStates_(Setting::IS_DECREASING);
      }
    }
    else
    {  // $[TI4] -- implied else, the value has not changed...
      currentChangeState = BoundedSetting::NO_CHANGE;

      updateAllSoftBoundStates_(Setting::NON_CHANGING);
    }

    if (loopCount == 0)
    {  // $[TI5]
      initialChangeState           = currentChangeState;
      initiallySettledOnValidValue = currentlySettledOnValidValue;

      // Only save the bound status if this is the first iteration of the loop.
      // We do not want to override the bound status from the first iteration
      // with that of later iterations which are merely meant to solve the
      // problem caused by the first iteration.
      saveBoundStatusState = rBoundStatus.getBoundState();
      saveBoundStatusId    = rBoundStatus.getBoundId();
    }  // $[TI6]

#if defined(SIGMA_DEVELOPMENT)
    if (Settings_Validation::IsDebugOn(BOUNDED_SETTING))
    {
      cout << "CALC-NEW-02 (" << loopCount << "):"
	   << "  initCS = " << initialChangeState
	   << ", initSVV = " << initiallySettledOnValidValue
	   << ", currCS = " << currentChangeState
	   << ", currSVV = " << currentlySettledOnValidValue
	   << endl;
    }
#endif // defined(SIGMA_DEVELOPMENT)

    switch (initialChangeState)
    {
    case BoundedSetting::NO_CHANGE :
    case BoundedSetting::CHANGE__NO_VIOLATION :		// $[TI7]
      // initially, either no change, or no violation, therefore stop looping...
      continueLooping = FALSE;
      break;
    case BoundedSetting::CHANGE__VIOLATION :		// $[TI8]
      // initially, this setting changed, with a violation, therefore need at
      // least one more loop to ensure proper final placement of this setting's
      // value, relative to the violated limit...
      continueLooping = TRUE;

      if (loopCount == 0)
      {  // $[TI8.1] -- initial loop...
	// continue looping, but the direction of change is based on whether a
	// valid value is reached, relative to this setting's dependent
	// setting...
	if (!initiallySettledOnValidValue)
	{  // $[TI8.1.1]
	  // a valid value for this setting wasn't reached, with respect to
	  // this setting's dependent settings, therefore back off one click...
	  knobDeltaToUse = (knobDeltaToUseIsIncreasing) ? -1	// $[TI8.1.1.1]
						        : +1;	// $[TI8.1.1.2]
	} 
	else 
	{  // $[TI8.1.2]
	  // a valid value for this setting was reached, with respect to
	  // this setting's dependent settings, therefore ensure that the
	  // resulting value wasn't backed off too far, by advancing one
	  // more click...
	  knobDeltaToUse = (knobDeltaToUseIsIncreasing) ? +1	// $[TI8.1.2.1]
							: -1;	// $[TI8.1.2.2]
	}
      }
      else
      {  // $[TI8.2] -- secondary loop, key off secondary state...
	switch (currentChangeState)
	{
	case BoundedSetting::NO_CHANGE :		// $[TI8.2.1]
	  // at furthest valid point, therefore stop looping...
	  continueLooping = FALSE;
	  break;
	case BoundedSetting::CHANGE__NO_VIOLATION :	// $[TI8.2.2]
	  // changed with no violation, determine whether to stop...
	  if (!initiallySettledOnValidValue)
	  {  // $[TI8.2.2.1]
	    // only continue looping if this setting's value is still NOT on
	    // a valid value, with respect to its dependents...
	    // $[TI8.2.2.1.1] (TRUE)  $[TI8.2.2.1.2] (FALSE)
	    continueLooping = (!currentlySettledOnValidValue);
	  }
	  else
	  {  // $[TI8.2.2.2]
	    // continue looping, because this setting's value is trying to
	    // ensure that its at the most advanced point, with respect to the
	    // violated setting limit; won't know most advanced point until
	    // a violation occurs...
	    continueLooping = TRUE;
	  }
	  break;
	case BoundedSetting::CHANGE__VIOLATION :	// $[TI8.2.3]
	  // changed with violation, determine whether to stop, only continue
	  // looping if this setting's value is still NOT on a valid value,
	  // with respect to its dependents...
	  // $[TI8.2.3.1] (TRUE)  $[TI8.2.3.2] (FALSE)
	  continueLooping = (!currentlySettledOnValidValue);
	  break;
	default :
	  // unexpected 'initialChangeState' value...
	  AUX_CLASS_ASSERTION_FAILURE(initialChangeState);
	  break;
	};
      }
      break;
    default :
      // unexpected 'initialChangeState' value...
      AUX_CLASS_ASSERTION_FAILURE(initialChangeState);
      break;
    };

    loopCount++;
  } while (continueLooping);

  // at this point this setting shall be valid with its bounds, and the
  // values of its dependent settings...
  SAFE_AUX_CLASS_ASSERTION((isAdjustedValid()), getId());

  if (loopCount > 1)
  {  // $[TI9]
    // store the saved bound status information...
    rBoundStatus.setBoundStatus(saveBoundStatusState, saveBoundStatusId);
  }  // $[TI10]

  // these calls are to ensure that those settings that have multiple high or
  // multiple low soft bounds (e.g., H:L ratio) have their soft bound states
  // updated following a knob delta change; this makes sure if, for example,
  // H:L's 1.00:1 soft limit were overridden, and then the user moves past it,
  // that H:L's 4.00:1 soft limit would be activated...
  updateAllSoftBoundStates_(Setting::NON_CHANGING);
  updateAllDependentsSoftBoundStates_(FALSE);

#if defined(SIGMA_DEVELOPMENT)
  if (Settings_Validation::IsDebugOn(BOUNDED_SETTING))
  {
    if (!IS_ADJUSTED_CONTEXT_DEBUG_ON)
    {
      Settings_Validation::TurnDebugOff(ADJUSTED_CONTEXT);
    }
    cout << "CALC-NEW-03:"
	 << "  final = " << BoundedValue(getAdjustedValue())
	 << ", boundId = " << rBoundStatus.getBoundId()
	 << ", whoViol = "
	     << SettingId::GetSettingName(rBoundStatus.getViolatedSettingId())
	 << endl;
  }
#endif // defined(SIGMA_DEVELOPMENT)

  return(rBoundStatus);
}


//============== M E T H O D   D E S C R I P T I O N ================
//@ Method:  warpToRange(rCurrValue, warpDirection, clipToRange)
//
//@ Interface-Description
//  This method returns in 'rCurrValue' a "warped" value that is based
//  on 'warpDirection' and 'clipToRange'.  If 'clipToRange' is 'TRUE',
//  then the resulting value will be clipped to the minimum and maximum
//  values.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
BoundedSetting::warpToRange(BoundedValue&               rCurrValue,
			    const BoundedRange::WarpDir warpDirection,
			    const Boolean               clipToRange)
{
  CALL_TRACE("warpToRange(rCurrValue, warpDirection, clipToRange)");

  // first reset the min/max constraints, to ensure accurate warping
  // throughout the full range of this setting...
  valueInterval_.resetMinConstraint();
  valueInterval_.resetMaxConstraint();

  // ...then, use this setting's bounded range instance to warp according
  // to the passed-in parameters...
  valueInterval_.warpValue(rCurrValue, warpDirection, clipToRange);
}


#if defined(SIGMA_DEVELOPMENT)

//============== M E T H O D   D E S C R I P T I O N =================
// Method:  isAcceptedValid()  [const, virtual]
//
// Interface-Description
//  Is the accepted value of this setting valid?
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

Boolean
BoundedSetting::isAcceptedValid(void) const
{
  CALL_TRACE("isAcceptedValid()");

  Boolean  isValid;

  if (getApplicability(Notification::ACCEPTED) !=
					      Applicability::INAPPLICABLE)
  {
    BoundedSetting* const  pNonConstThis = (BoundedSetting*)this;
    pNonConstThis->valueInterval_.resetMaxConstraint();
    pNonConstThis->valueInterval_.resetMinConstraint();

    // get the accepted value...
    const BoundedValue  ACCEPTED_VALUE = getAcceptedValue();

    // is the accepted value within the absolute min and max range?...
    isValid = valueInterval_.isLegalValue(ACCEPTED_VALUE);

    if (!isValid)
    {
      cout << "value = " << ACCEPTED_VALUE << "\n"
	   << valueInterval_ << endl;
    }
  }
  else
  {
    // inapplicability implies validity...
    isValid = TRUE;
  }

  return(isValid);
}


//============== M E T H O D   D E S C R I P T I O N =================
// Method:  isAdjustedValid()  [virtual]
//
// Interface-Description
//  Is the adjusted value of this setting valid?
//---------------------------------------------------------------------
// Implementation-Description
//  The setting determines, and returns its validity.
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

Boolean
BoundedSetting::isAdjustedValid(void)
{
  CALL_TRACE("isAdjustedValid()");

  Boolean  isValid;

  if (getApplicability(Notification::ADJUSTED) !=
					      Applicability::INAPPLICABLE)
  {
    // get the adjusted value...
    BoundedValue  adjustedValue = getAdjustedValue();

    // is the adjusted value within the absolute min and max range?...
    isValid = valueInterval_.isLegalValue(adjustedValue);

    if (isValid)
    {
      // check against setting's current bounds...
      BoundStatus  dummyStatus(getId());

      // calculate bound constraints...
      updateConstraints_();

      // use the calculation of the maximum bound in 'updateConstraints_()'
      // to test the current adjusted value against a valid range...
      valueInterval_.testValue(adjustedValue, dummyStatus);

      isValid = !dummyStatus.isInViolation();

      if (!isValid)
      {
	cout << "value = " << adjustedValue
	     << ", boundId = " << dummyStatus.getBoundId()
	     << "\n" << valueInterval_ << endl;
      }
    }
    else
    {
      cout << "value = " << adjustedValue << "\n"
	   << valueInterval_ << endl;
    }
  }
  else
  {
    // inapplicability implies validity...
    isValid = TRUE;
  }

  return(isValid);
}

#endif // defined(SIGMA_DEVELOPMENT)


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//	      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
BoundedSetting::SoftFault(const SoftFaultID  softFaultID,
			  const Uint32       lineNumber,
			  const char*        pFileName,
			  const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION, BOUNDED_SETTING,
                          lineNumber,pFileName, pPredicate);
}


//======================================================================
//
//  Protected Methods...
//
//======================================================================

//============= M E T H O D   D E S C R I P T I O N ==================
//@ Method:  BoundedSetting(settingId, ...)  [Constructor]
//
//@ Interface-Description
//  Construct a bounded setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (SettingId::IsBoundedId(settingId))
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

BoundedSetting::BoundedSetting(
			const SettingId::SettingIdType  settingId,
		        const SettingId::SettingIdType* arrDependentSettingIds,
		        const BoundedInterval*          pIntervalList,
		        const SettingBoundId            maxConstraintId,
		        const SettingBoundId            minConstraintId,
		        const Boolean                   useEpsilonFactoring
							  ) : 
Setting(            settingId, arrDependentSettingIds),
			  valueInterval_(pIntervalList,
					 maxConstraintId,
					 minConstraintId,
					useEpsilonFactoring),
isATimingSetting_(  FALSE)
{
  CALL_TRACE("BoundedSetting(settingId, ...)");
  AUX_CLASS_ASSERTION((SettingId::IsBoundedId(settingId)),
  		      settingId);
}   // $[TI1]


//================ M E T H O D   D E S C R I P T I O N ================
//@ Method:  getAbsoluteMaxValue_()  [const, virtual]
//
//@ Interface-Description
//  Use this to return the absolute maximum value that is currently available
//  from this setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  In general, the absolute maximum value for BoundedSetting's is found
//  in their BoundedRange instances.  Those settings with special cases
//  can override this definition.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Real32
BoundedSetting::getAbsoluteMaxValue_(void) const
{
  CALL_TRACE("getAbsoluteMaxValue_()");

  return(valueInterval_.getMaxValue());
}  // $[TI1]


//================ M E T H O D   D E S C R I P T I O N ================
//@ Method:  getAbsoluteMinValue_()  [const, virtual]
//
//@ Interface-Description
//  Use this to return the absolute minimum value that is currently available
//  from this setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  In general, the absolute minimum value for BoundedSetting's is found
//  in their BoundedRange instances.  Those settings with special cases
//  can override this definition.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Real32
BoundedSetting::getAbsoluteMinValue_(void) const
{
  CALL_TRACE("getAbsoluteMinValue_()");

  return(valueInterval_.getMinValue());
}  // $[TI1]


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  ventset() [virtual]
//
//@ Interface-Description
//  Changes the setting value to the value specified by the string
//  parameter.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
#include <stdlib.h>
#include <string.h>
#include "BoundStrs.hh"

SigmaStatus
BoundedSetting::ventset(const char* requestedValue)
{
	BoundedValue newValue;
	newValue.value = atof(requestedValue);

	if ( isATimingSetting_ )
	{
		// convert to milliseconds
		newValue.value *= 1000;
	}

	// call activationHappened to update constraints
	activationHappened();

	warpToRange(newValue,BoundedRange::WARP_NEAREST,FALSE);

	BoundedValue adjustedValue = getAdjustedValue();
	const BoundStatus& rBoundStatus = getBoundStatus_();

	if ( !IsEquivalent(newValue.value,adjustedValue.value,adjustedValue.precision ) )
	{
		if ( newValue.value > adjustedValue.value )
		{
			while ( newValue.value > adjustedValue.value )
			{
				calcNewValue(1);
				adjustedValue = getAdjustedValue();
				if ( rBoundStatus.isInViolation() )
				{
					break;
				}
			}
		}
		else if ( newValue.value < adjustedValue.value )
		{
			while ( newValue.value < adjustedValue.value )
			{
				calcNewValue(-1);
				adjustedValue = getAdjustedValue();
				if ( rBoundStatus.isInViolation() )
				{
					break;
				}
			}
		}
	}

	const SettingBoundId  SETTING_BOUND_ID = rBoundStatus.getBoundId();
	const Boolean isBoundViolated = rBoundStatus.isInViolation();
	StringId  boundMsg = L"OK";   

	if ( SETTING_BOUND_ID != ::NULL_SETTING_BOUND_ID )
	{
		boundMsg = BoundStrs::GetSettingBoundString(SETTING_BOUND_ID);
	}

	BoundedValue settingValue = getAdjustedValue();

	if ( isATimingSetting_ )
	{
		// convert back to seconds with scaled timing resolution
		settingValue.value /= 1000;
		switch ( settingValue.precision )
		{
			case THOUSANDS:
				settingValue.precision = ONES;
				break;
			case HUNDREDS:
				settingValue.precision = TENTHS;
				break;
			case TENS:
				settingValue.precision = HUNDREDTHS;
				break;
			default:
				settingValue.precision = THOUSANDTHS;
				break;
		}
	}

	// TODO E600 - Need to revisit for non-English language setting.
	// As boundMsg contains language specific prompt strings.
	char pTemp[256];
	StringConverter::ToString( pTemp, 256, boundMsg );
	VsetServer::Report(settingValue, pTemp);  

	if ( isBoundViolated )
	{
		resetToStoredValue();
	}

	return isBoundViolated ? FAILURE : SUCCESS;
}

#if defined(SIGMA_DEVELOPMENT)

//============== M E T H O D   D E S C R I P T I O N =================
// Method:  print_(ostr)  [const]
//
// Interface-Description
//  Print the contents of this bounded setting into 'ostr'.
//---------------------------------------------------------------------
// Implementation-Description
//  Build message with setting contents inserted in it.
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

Ostream&
BoundedSetting::print_(Ostream& ostr) const
{
  CALL_TRACE("print_(ostr)");

  ostr << "  Name:            " << SettingId::GetSettingName(getId())
       << endl;

  const BoundedValue  ACCEPTED_VALUE = getAcceptedValue();

  ostr << "  Accepted Value:  " << ACCEPTED_VALUE << endl;

  return(ostr);
}

#endif  // defined(SIGMA_DEVELOPMENT)
