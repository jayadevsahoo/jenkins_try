#ifndef CpuDevice_IN
#define CpuDevice_IN
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//=====================================================================
// Class: CpuDevice - POST CPU Utilities
//
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/POST-Library/vcssrc/CpuDevice.inv   25.0.4.0   19 Nov 2013 14:16:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By:  gdc    Date:  12-Dec-2008    SCR Number: 6068
//  Project:  840S
//  Description:
//		Removed SUN prototype code.
//
//  Revision: 003   By: Gary Cederquist  Date: 15-AUG-1997  DR Number: 1875
//    Project:   Sigma   (R8027)
//    Description:
//       Added testable item annotations.
//
//  Revision: 002    By: Gary Cederquist  Date: 25-APR-1997  DR Number: 1996
//    Project: Sigma (R8027)
//    Description:
//       Added missing requirement number.
//
//  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//=====================================================================


//@ Usage-Classes
#include "MemoryMap.hh"
//@ End-Usage


//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method: IsBdCpu
//
//@ Interface-Description
//  >Von
//  Returns TRUE is the CPU is a BD CPU.
//
//  INPUTS:  NONE
//  OUTPUTS: TRUE if BD, FALSE otherwise
//
//  A specific bit of a hardware port is set to indicate the type
//  of CPU installed.  If it is grounded a GUI CPU is installed,
//  if it is high (VCC) a BD CPU is installed.
//  >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//    See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

Boolean
CpuDevice::IsBdCpu(void)
{
#ifdef E600_840_TEMP_REMOVED
    // $[TI1.1]  $[TI1.2]
    return (*IO_REGISTER_1 & BD_CPU_BIT);
#endif
return false;
}

//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method: IsGuiCpu
//
//@ Interface-Description
//  >Von
//  Returns TRUE is the CPU is a GUI CPU.
//
//  INPUTS:  NONE
//  OUTPUTS: TRUE if GUI, FALSE otherwise
//
//  A specific bit of a hardware port is set to indicate the type
//  of CPU installed.  If it is grounded a GUI CPU is installed,
//  if it is high (VCC) a BD CPU is installed.
//  >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//    See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

Boolean
CpuDevice::IsGuiCpu(void)
{
#ifdef E600_840_TEMP_REMOVED
    // $[TI1.1]  $[TI1.2]
    return !(*IO_REGISTER_1 & BD_CPU_BIT);
#endif
return false;
}

//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method: LightDiagnosticLED - display pattern in CPU LED
//
//@ Interface-Description
//  >Von
//    Outputs a binary value to the bank of diagnostic LEDS.
//    Any pattern except 255 (the power on pattern) is valid.
//
//    INPUTS:  pattern  0-254
//    OUTPUTS: NONE
//
//  >Voff
//  $[11046]
//---------------------------------------------------------------------
//@ Implementation-Description
//    See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

void
CpuDevice::LightDiagnosticLED(Uint8 pattern)
{
#ifdef E600_840_TEMP_REMOVED
  //  DO NOT ISSUE PRINT STATEMENTS IN THIS METHOD
  //  USED BY NMI HANDLERS AND PRIOR TO PRINT INITIALIZATION

   *DIAG_LED_REG = ~pattern;   // leds are inverted.

   // $[TI1]
#endif
}

//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method: InvalidateBothCaches
//
//@ Interface-Description
//    Invalidates the data and instruction caches required before
//    MMU initialization in VRTX.
//---------------------------------------------------------------------
//@ Implementation-Description
//    See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

void
CpuDevice::InvalidateBothCaches(void)
{
	//TODO E600 remove asm
    //asm(" moveq  #0,d0 "," movec d0,cacr "," cinva bc ");
    // $[TI1]
}


//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method: EnableAutovector
//
//@ Interface-Description
//    Uses the hardware autovector mask register to enable/disable
//    autovector interrupts using mask parameter.
//---------------------------------------------------------------------
//@ Implementation-Description
//    See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

void
CpuDevice::EnableAutovector(Uint8 mask)
{
#ifdef E600_840_TEMP_REMOVED
    *AUTOVECTOR_ENABLE_REG = mask;
    // $[TI1]
#endif
}

//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method: StrobeWatchdog
//
//@ Interface-Description
//    Strobes the watchdog restarting its internal timeout for reseting
//    the processor which occurs within 60-250 ms.
//---------------------------------------------------------------------
//@ Implementation-Description
//    See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

void
CpuDevice::StrobeWatchdog(void)
{
#ifdef E600_840_TEMP_REMOVED
    *WATCHDOG_STROBE_REG = 0;
    // $[TI1]
#endif
}

//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method: ClearWatchdogBite
//
//@ Interface-Description
//    Clears the watchdog bite condition that indicates the watchdog
//    has fired.
//---------------------------------------------------------------------
//@ Implementation-Description
//    See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

void
CpuDevice::ClearWatchdogBite(void)
{
#ifdef E600_840_TEMP_REMOVED
    *WATCHDOG_BITE_CLEAR_REG = 0;
    // $[TI1]
#endif
}

//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method: IsWatchdogBiteOn
//
//@ Interface-Description
//    Returns true if the watchdog "bite" is set.  This indicates the
//    processor was reset by the watchdog timer.
//---------------------------------------------------------------------
//@ Implementation-Description
//    See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

Boolean
CpuDevice::IsWatchdogBiteOn(void)
{
#ifdef E600_840_TEMP_REMOVED
    if ( CpuDevice::IsBdCpu() )
    {
        // $[TI1.1]
        return ( *IO_REGISTER_1 & 0x04 );
    }
    else
    {
        // $[TI1.2]
        return ( *IO_REGISTER_2 & 0x08 );
    }
#endif
return false;
}

//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method: ResetAutovector3
//
//@ Interface-Description
//    Unlatches the interrupt line causing a autovector level 3.
//---------------------------------------------------------------------
//@ Implementation-Description
//    See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

void
CpuDevice::ResetAutovector3(void)
{
#ifdef E600_840_TEMP_REMOVED
    *AUTOVECTOR_3_RESET_REG = 0;
    // $[TI1]
#endif
}

//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method: ResetAutovector4
//
//@ Interface-Description
//    Unlatches the interrupt line causing a autovector level 4.
//---------------------------------------------------------------------
//@ Implementation-Description
//    See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

void
CpuDevice::ResetAutovector4(void)
{
#ifdef E600_840_TEMP_REMOVED
    *AUTOVECTOR_4_RESET_REG = 0;
    // $[TI1]
#endif
}

//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method: ResetAutovector5
//
//@ Interface-Description
//    Unlatches the interrupt line causing a autovector level 5.
//---------------------------------------------------------------------
//@ Implementation-Description
//    See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

void
CpuDevice::ResetAutovector5(void)
{
#ifdef E600_840_TEMP_REMOVED
    *AUTOVECTOR_5_RESET_REG = 0;
    // $[TI1]
#endif
}

//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method: ResetAutovector6
//
//@ Interface-Description
//    Unlatches the interrupt line causing a autovector level 6.
//---------------------------------------------------------------------
//@ Implementation-Description
//    See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

void
CpuDevice::ResetAutovector6(void)
{
#ifdef E600_840_TEMP_REMOVED
    *AUTOVECTOR_6_RESET_REG = 0;
    // $[TI1]
#endif
}

//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method: ResetAutovectors
//
//@ Interface-Description
//    Unlatches (resets) all autovectored interrupts.
//---------------------------------------------------------------------
//@ Implementation-Description
//    See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

void
CpuDevice::ResetAutovectors(void)
{
#ifdef E600_840_TEMP_REMOVED
    *AUTOVECTOR_3_RESET_REG = 0;
    *AUTOVECTOR_4_RESET_REG = 0;
    *AUTOVECTOR_5_RESET_REG = 0;
    *AUTOVECTOR_6_RESET_REG = 0;
    // $[TI1]
#endif
}

//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method: IsAudioBusyAsserted
//
//@ Interface-Description
//    Returns the state of the SAAS busy signal.  Returns TRUE if the
//    line is asserted.
//---------------------------------------------------------------------
//@ Implementation-Description
//    See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

Boolean
CpuDevice::IsAudioBusyAsserted(void)
{
#ifdef E600_840_TEMP_REMOVED
    // $[TI1.1]  $[TI1.2]
    return !(*IO_REGISTER_2 & AUDIO_BUSY_BIT);
#endif
return false;
}

//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method: IsAudioAckAsserted
//
//@ Interface-Description
//    Returns the state of the SAAS ack signal.  Returns TRUE if the
//    line is asserted.
//---------------------------------------------------------------------
//@ Implementation-Description
//    See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

Boolean
CpuDevice::IsAudioAckAsserted(void)
{
#ifdef E600_840_TEMP_REMOVED
    // $[TI1.1]  $[TI1.2]
    return !(*IO_REGISTER_2 & AUDIO_ACK_BIT);
#endif
return false;
}

//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method: IsAudioAttnAsserted
//
//@ Interface-Description
//    Returns the state of the SAAS attn signal.  Returns TRUE if the
//    line is asserted.
//---------------------------------------------------------------------
//@ Implementation-Description
//    See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

Boolean
CpuDevice::IsAudioAttnAsserted(void)
{
#ifdef E600_840_TEMP_REMOVED
    // $[TI1.1]  $[TI1.2]
    return !(*IO_REGISTER_2 & AUDIO_ATTN_BIT);
#endif
return false;
}

#endif // CpuDevice_IN
