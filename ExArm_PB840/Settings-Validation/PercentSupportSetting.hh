
#ifndef PercentSupportSetting_HH
#define PercentSupportSetting_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  PercentSupportSetting - Percent Support Setting
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/PercentSupportSetting.hhv   25.0.4.0   19 Nov 2013 14:27:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006   By: gdc    Date: 26-Jan-2009    SCR Number: 6407
//  Project:  840S
//  Description:
//      Modified to bring source into strict ARM compliance.
//
//  Revision: 005   By: gfu   Date:  11-Aug-2004    DR Number: 6132
//  Project:  PAV3
//  Description:
//	   Modified code per SDCR #6132.  Also added keyword "Log" to 
//     the file header as requested during code review.
//
//  Revision: 004   By: sah   Date:  23-May-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//	PAV project-related changes:
//      *  added 'updateContraints_()', and 'getAbsolute{Min,Max}Value_()'
//         methods for supporting new dynamic range
//
//  Revision: 003   By: sah   Date:  22-Jul-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  modified to use newly-modified 'BoundedRange' class to handle
//         range, via linked list of interval nodes
//
//  Revision: 002   By: sah   Date:  06-Jan-1999    DR Number:  5314
//  Project:  ATC
//  Description:
//     Added new 'getApplicability()' method.
//
//  Revision: 001   By: sah   Date:  06-Jan-1999    DR Number:  5322
//  Project:  ATC
//  Description:
//	New ATC-specific setting.
//
//====================================================================

//@ Usage-Classes
#include "BatchBoundedSetting.hh"
//@ End-Usage


class PercentSupportSetting : public BatchBoundedSetting 
{
  public:
    PercentSupportSetting(void); 
    virtual ~PercentSupportSetting(void);

    virtual Applicability::Id  getApplicability(
				const Notification::ChangeQualifier qualifierId
					       ) const;

    virtual void  acceptTransition(const SettingId::SettingIdType settingId,
				   const DiscreteValue            newValue,
				   const DiscreteValue            currValue);

    virtual SettingValue  getNewPatientValue(void) const;

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL, 
			   const char*       pPredicate = NULL);

    virtual void findSoftMinMaxValues_(Real32& rSoftMinValue, Real32& rSoftMaxValue) const;
    
  protected:
    virtual void  updateConstraints_(void);

    virtual Real32  getAbsoluteMaxValue_(void) const;
    virtual Real32  getAbsoluteMinValue_(void) const;

  private:
    // not implemented...
    PercentSupportSetting(const PercentSupportSetting&);
    void  operator=(const PercentSupportSetting&);
};


#endif // PercentSupportSetting_HH 
