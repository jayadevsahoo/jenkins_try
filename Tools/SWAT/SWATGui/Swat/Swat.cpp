// SWAT.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "Swat.h"
#include "MainFrm.h"
#include "SwatDoc.h"
#include "SwatView.h"
#include "DataCaptureIf.h"
#include "DownloadSWDlg.h"
#include "CaptureSettingsDlg.h"
#include "TestResultsDlg.h"
#include "PatientDataDlg.h"
#include "TestResultsDlg.h"
#include "DeleteDataDlg.h"
#include "IPatientDataSet.h"
#include "CLogger.h"
#include "UILogger.h"
#include "MessageTimerDlg.h"
#include "BatchBuilder.h"
#include "BatchBuilderLua.h"
#include "CheckLoop.h"
#include "TestResultLogSet.h"
#include "SWATSettingsDlg.h"
#include "PowerResetDlg.h"
#include "GetUserInputDlg.h"

#include "Winerror.h"

#include "GuiIOServer.h"

#include "PowerResetDlg.h"
#include "LungSetupDlg.h"
#include "Hyperlink.h"

#include "ping.h"

#include <WinSock2.h>
#include <windows.h>
#include <stdio.h>

#include "afxinet.h"

#include "FileOperations.h"



#ifdef ENABLE_DOWNLOADER
	#include "..\\..\\..\\Downloader\\SoftwareDownload\\SoftwareDownload\\VentConsoleDriver\\VikingConsoleDriver.h"
#endif


//#include "AdcChannelId.hh"

typedef DataCaptureIf * (__cdecl *MYPROC)(); 

DataCaptureIf *g_DataCapture = NULL;
UILogger  *g_clUILogger;
CLogger   g_clCLogger;

#include "LuaFunctions.cpp"
#include "hyperlink.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif





// CSwatApp

BEGIN_MESSAGE_MAP(CSwatApp, CWinApp)
	ON_COMMAND(ID_APP_ABOUT, &CSwatApp::OnAppAbout)
	// Standard file based document commands
	ON_COMMAND(ID_FILE_NEW, &CWinApp::OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, &CWinApp::OnFileOpen)
	ON_COMMAND(ID_FILE_NEW, &CSwatApp::OnFileNew)
	ON_COMMAND(ID_SWAT_SW_DWNLOAD, &CSwatApp::OnSwatSwDwnload)
	ON_COMMAND(ID_SWAT_CAPTURE_SETTINGS, &CSwatApp::OnSwatCaptureSettings)
	ON_COMMAND(ID_SWAT_PATIENTDATA, &CSwatApp::OnSwatPatientSettings)
	ON_COMMAND(ID_SWAT_CLEANDATABASE, &CSwatApp::OnSwatCleanDatabase)
	ON_COMMAND(ID_GUICONNECTION_CONNECTGUI, &CSwatApp::OnGuiConnectGUI)
	ON_COMMAND(ID_GUICONNECTION_DISCONNECTGUI, &CSwatApp::OnGuiDisconnectGUI)



	ON_UPDATE_COMMAND_UI(ID_GUICONNECTION_CONNECTGUI, &CSwatApp::OnUpdateGuiConnectGUI)
	ON_UPDATE_COMMAND_UI(ID_GUICONNECTION_DISCONNECTGUI, &CSwatApp::OnUpdateGuiDisconnectGUI)
	ON_COMMAND(ID_HELP_TEST, &CSwatApp::OnHelpTest)
	ON_COMMAND(ID_SWAT_SETTINGS, &CSwatApp::OnSwatSettings)
	ON_COMMAND(ID_SWAT_STOP_BDSIMULATIONS, &CSwatApp::OnSwatStopBdsimulations)
	ON_UPDATE_COMMAND_UI(ID_SWAT_STOP_BDSIMULATIONS, &CSwatApp::OnUpdateSwatStopBdsimulations)
	ON_COMMAND(ID_SOFTWAREDOWNLOAD_STARTDOWNLOAD, &CSwatApp::OnSoftwaredownloadStartdownload)
	ON_COMMAND(ID_SOFTWAREDOWNLOAD_CANCELDOO, &CSwatApp::OnSoftwaredownloadCanceldoo)
	ON_COMMAND(ID_SWAT_TAKEVENTSCREENSHOT, &CSwatApp::OnSwatTakeventscreenshot)
	ON_UPDATE_COMMAND_UI(ID_SWAT_TAKEVENTSCREENSHOT, &CSwatApp::OnUpdateSwatTakeventscreenshot)
END_MESSAGE_MAP()


// CSwatApp construction

CSwatApp::CSwatApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance

	m_hinstLib = NULL;
	m_ShowMsgContinueDlg = NULL;
	m_nGUI_SW_DownloadStatus = 0;
	m_nBD_SW_DownloadStatus = 0;
	m_bNewSettingsRcvd = 0;
	m_pDownloadSWDlg = NULL;
	m_pVentStatusDlg = NULL;
	m_bPopErrorMsg = 1;//show pop up message in case of errors 
	m_strSWATLogStatus = ""; 



	strSWATVersion = "22.7";
	m_nGuiIOServerVerexpected = 11;




	m_bRecording = false;
	m_bRecord = false;

	m_strSettings0 = ""; 
	m_strSettings1 = ""; 
	m_strSettings2 = ""; 
	m_strSettings3 = ""; 
	m_strSettings4 = "";
	m_bVent_Batch_enabled = false;
	m_bScriptRunning = false;

	m_bTwoImagesForComparison = false;  //If doing language verification, it creates report file with English png on it to compare with current language


}


// The one and only CSwatApp object

CSwatApp theApp;


// CSwatApp initialization
BOOL CSwatApp::InitInstance()
{

//TODO: call AfxInitRichEdit2() to initialize richedit2 library.
	// InitCommonControlsEx() is required on Windows XP if an application
	// manifest specifies use of ComCtl32.dll version 6 or later to enable
	// visual styles.  Otherwise, any window creation will fail.


//RohitVij
//Only one instance of SWAT allowed
	 HANDLE hMutex = CreateMutex(NULL,FALSE,"Global\\SWATCovidien");
	 if(GetLastError()==ERROR_ALREADY_EXISTS)
	 {
			MessageBox(NULL,"An instance of SWAT is already running. Only one instance of SWAT allowed on one PC. \n\nIf you just closed SWAT, please wait 5 seconds before running it again.","SWAT Error",MB_ICONHAND);
			ShowWindow( FindWindow(NULL, "SWAT - SoftWare Automation Tester"),SW_SHOWNORMAL|SW_RESTORE | SW_SHOW);
			PostQuitMessage(0);
			return TRUE;
	 }
 	if(GetFileAttributes(SWAT_SETTING_FILE)== INVALID_FILE_ATTRIBUTES)
	{
		AfxMessageBox("C:\\SWAT\\Lua\\SWATinit.lua file missing. File required to run SWAT.",MB_ICONERROR);
		PostQuitMessage(0);
		return TRUE;
	}

	if(m_lpCmdLine[0] == _T('\0'))
	{
		if(GetSetLungSetup()==false)
		{	
			PostQuitMessage(0);
			return false;
		}
	}
	else
	{
		CString str(m_lpCmdLine);



	}


	{//Get logged in user name
		TCHAR szUserName[UNLEN + 1];
		DWORD dwNameLen = UNLEN + 1; 
		if( !GetUserName( szUserName, &dwNameLen ) )
		{
			AfxMessageBox("Error getting user name.\nApplication can not be run");
			PostQuitMessage(0);
			return TRUE;
		}
		m_strUserName = CString(szUserName);
	}

	ReadLuaInitFile();//get BD and GUI IP addresses from the init_lua file and saved in App variables
	//set decoda, scite and teraterm paths
	SetWindowsPath();

	if(GetFileAttributes(SWAT_EMAIL_PL)== INVALID_FILE_ATTRIBUTES)
	{
		m_strGUI_IPAddr;
		CPing p2;
		CPingReply pr2;
		CString str;
		
		if (p2.Ping2("RACARL-RD01", pr2, 10, 1000))
		{
			CString strPerlPath = "\\\\racarl-rd01\\Viking_Software\\work\\rohit.vij\\SWAT_Installation\\Archive\\swat_pickups\\SendEmail.pl";
			CopyFile(strPerlPath, SWAT_EMAIL_PL, TRUE);
		}
		else
		{
			//don't worry about it, you are not on covidien network, can't send mail anyway
		}
	}
	{//copy dingling.wav
		m_strGUI_IPAddr;
		CPing p2;
		CPingReply pr2;
		CString str;
		
		if (p2.Ping2("RACARL-RD01", pr2, 10, 1000))
		{
			CString strWavPath = "\\\\racarl-rd01\\Viking_Software\\work\\rohit.vij\\SWAT_Installation\\Archive\\swat_pickups\\swat.wav";
			CopyFile(strWavPath, SWAT_SOUND, FALSE);
		}
		else
		{
			//don't worry about it, you are not on covidien network, can't send mail anyway
		}
	}
	

	//Open logging file
	CFileException e;
	if(!m_SWAT_log_file.Open( SWAT_INTERNAL_LOG_FILE, CFile::shareDenyWrite | CFile::modeCreate | CFile::modeWrite | CFile::typeText, &e ))
	{
		char szCause[255];
		e.GetErrorMessage(szCause, 255);
		CString strError;
		strError.Format("Error %s, writing log file %s", szCause, SWAT_INTERNAL_LOG_FILE);
		AfxMessageBox(strError);
		PostQuitMessage(0);
		return TRUE;
	}
	if(!m_SWAT_test_log_file.Open( SWAT_TEST_LOG_FILE, CFile::shareDenyWrite | CFile::modeCreate | CFile::modeWrite | CFile::typeText, &e ))
	{
		char szCause[255];
		e.GetErrorMessage(szCause, 255);
		CString strError;
		strError.Format("Error %s, writing log file %s", szCause, SWAT_TEST_LOG_FILE);
		AfxMessageBox(strError);
		PostQuitMessage(0);
		return TRUE;
	}
	m_SWAT_test_log_file.WriteString("Mime-Version: 1.0; \r\n");
	m_SWAT_test_log_file.WriteString("Content-Type: text/html; charset=\"ISO-8859-1\";  \r\n");
	m_SWAT_test_log_file.WriteString("Content-Transfer-Encoding: 7bit; \r\n\r\n");

	m_SWAT_test_log_file.WriteString("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\"    \r\n");
	m_SWAT_test_log_file.WriteString("\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">      \r\n");
	m_SWAT_test_log_file.WriteString("<html xmlns=\"http://www.w3.org/1999/xhtml\"> \r\n\r\n");


	m_SWAT_test_log_file.WriteString("<span style=\"font-family: Lucida Console;\"> \r\n <font size = \"2\">");


 //ReadErrorPopupValue(); //Do we want to show error pop up or only in the log window
	m_bPopErrorMsg = FALSE;//show errors in only log window, not messagebox
 
	CreateRegistryKey();
	ReadAllRegistrySettings();


//	ReadVentNameInfo();
//	GetSetLungSetup();

	

//Generated by Visual Studio
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// Set this to include all the common control classes you want to use
	// in your application.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	AfxInitRichEdit2();

	CWinApp::InitInstance();

	// Initialize OLE libraries
	if (!AfxOleInit())
	{
		AfxMessageBox(IDP_OLE_INIT_FAILED);
		return FALSE;
	}
	AfxEnableControlContainer();
	// Standard initialization
	// If you are not using these features and wish to reduce the size
	// of your final executable, you should remove from the following
	// the specific initialization routines you do not need
	// Change the registry key under which our settings are stored
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization
	SetRegistryKey(_T("Local AppWizard-Generated Applications"));
	LoadStdProfileSettings(4);  // Load standard INI file options (including MRU)
	// Register the application's document templates.  Document templates
	//  serve as the connection between documents, frame windows and views
	CSingleDocTemplate* pDocTemplate;
	pDocTemplate = new CSingleDocTemplate(
		IDR_MAINFRAME,
		RUNTIME_CLASS(CSwatDoc),
		RUNTIME_CLASS(CMainFrame),       // main SDI frame window
		RUNTIME_CLASS(CSwatView));
	if (!pDocTemplate)
		return FALSE;
	AddDocTemplate(pDocTemplate);


	// Enable DDE Execute open
	EnableShellOpen();
	RegisterShellFileTypes(FALSE);

	// Parse command line for standard shell commands, DDE, file open
	CCommandLineInfo cmdInfo;
	ParseCommandLine(cmdInfo);


	// Dispatch commands specified on the command line.  Will return FALSE if
	// app was launched with /RegServer, /Register, /Unregserver or /Unregister.
	if (!ProcessShellCommand(cmdInfo))
		return FALSE;

	// The one and only window has been initialized, so show and update it
	m_pMainWnd->ShowWindow(SW_SHOW);
	m_pMainWnd->UpdateWindow();
	// call DragAcceptFiles only if there's a suffix
	//  In an SDI app, this should occur after ProcessShellCommand
	// Enable drag/drop open
	m_pMainWnd->DragAcceptFiles();

//---------------Generated by VS 2008 over---------------//


	m_pMainWnd = AfxGetMainWnd( );



	if (!InitializeCriticalSectionAndSpinCount(&m_CriticalSection, 
        0x80000400) ) 
	{
		AfxMessageBox("Error initializing critical section");
	}

	if(	OpenDB() == true)//open database
	{
		//load dll and start data capturing
		MYPROC ProcAdd; 
		BOOL fRunTimeLinkSuccess = FALSE; 
	 	// Get a handle to the DLL module.
		 m_hinstLib = LoadLibrary(TEXT("DataCapture.dll")); 
	 	// If the handle is valid, try to get the function address.
		 if (m_hinstLib != NULL) 
		{ 
			ProcAdd = (MYPROC) GetProcAddress(m_hinstLib, "DataCapture"); 
	 
			// If the function address is valid, call the function.
			 if (NULL != ProcAdd) 
			{
				fRunTimeLinkSuccess = TRUE;
				g_DataCapture=(ProcAdd) ();
				bool b ;

				g_clUILogger=new UILogger();
				g_clCLogger.set(g_clUILogger);
				
				b= g_DataCapture->register_logger(g_clUILogger);
				b= g_DataCapture->start();
				
		
			}
		 }
		 else {
			 AfxMessageBox("Error loading Datacapture DLL.");
		 }
	}

	
	ReadLuaInitFile();//get BD IP address from the init_lua file
	m_strRecordSWinReport = ReadRegistrySetting(SWAT_REG_PARAM_RECORD_SW_VER);
	if(m_strRecordSWinReport != "true" && m_strRecordSWinReport != "false") 
	{
		SaveRegistrySetting(SWAT_REG_PARAM_RECORD_SW_VER, "true");
		m_strRecordSWinReport = ReadRegistrySetting(SWAT_REG_PARAM_RECORD_SW_VER);
	}

	m_strLockPassReports = ReadRegistrySetting(SWAT_SET_REPORT_READ_ONLY);
	if(m_strLockPassReports != "true" && m_strLockPassReports != "false") 
	{
		SaveRegistrySetting(SWAT_SET_REPORT_READ_ONLY, "true");
		m_strLockPassReports = ReadRegistrySetting(SWAT_SET_REPORT_READ_ONLY);
	}






	m_bSendMail = atoi(ReadRegistrySetting(SWAT_SEND_EMAIL)) ? true : false;//returns 0 if not found in registry
	m_bAppendTimeInStatusWindow = atoi(ReadRegistrySetting(SWAT_REG_PARAM_APPEND_TIME)) ? true : false;//returns 0 if not found in registry

	CaptureSettings(); //start capturing settings

	EnableBDSendSettings();

#ifdef	ENABLE_DOWNLOADER
	m_pVikingConsoleDriver = ::VikingConsoleDriver_init();           //This function must be called on start.
#endif


	AfxBeginThread((AFX_THREADPROC)Thread_BreathDisplay, (LPVOID)this);
	AfxBeginThread((AFX_THREADPROC)Thread_WaveformData, (LPVOID)this);

	return TRUE;
}

void CSwatApp::CreateRegistryKey()
{
	HKEY hKey;
	DWORD dwDisp = 0;
	LPDWORD lpdwDisp = &dwDisp;
	LONG iSuccess = RegCreateKeyEx( HKEY_CURRENT_USER, SWAT_REG_KEY, 0L,NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &hKey,lpdwDisp);
	if(iSuccess == ERROR_SUCCESS)
	{
		RegCloseKey(hKey);
	}


}

void CSwatApp::SaveRegistrySetting(CString strParam, CString strVal)
{
	HKEY hKey;
	DWORD dwDisp = 0;
	LPDWORD lpdwDisp = &dwDisp;
	LONG iSuccess = RegCreateKeyEx( HKEY_CURRENT_USER, SWAT_REG_KEY, 0L,NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &hKey,lpdwDisp);
	if(iSuccess == ERROR_SUCCESS)
	{
		RegSetValueEx(hKey, strParam, 0, REG_SZ, (LPBYTE) strVal.GetBuffer(strVal.GetLength()), strVal.GetLength()+ 1);
		RegCloseKey(hKey);
	}
}
void CSwatApp::SaveString2LocalTempFile(CString strTempFilename, CString str)
{
	TRY
	{
		CStdioFile file( strTempFilename, CFile::modeCreate | CFile::modeWrite | CFile::typeText );
		file.WriteString(str);
		file.Close();
	}
	CATCH( CFileException, e )
	{
		char szCause[255];
		e->GetErrorMessage(szCause, 255);
		LOG_MESSAGE(LM_ERROR, "Error %s, writing temp file %s", szCause, strTempFilename);
	}
	END_CATCH
 
}
CString CSwatApp::ReadLocalTempFile(CString strTempFilename)
{
	if(GetFileAttributes(strTempFilename)== INVALID_FILE_ATTRIBUTES)
	{
		LOG_MESSAGE(LM_ERROR, "%s file missing", strTempFilename);
		return "error";
	}

	CString str,strFinal;

	TRY
	{
		CStdioFile file( strTempFilename, CFile::modeRead | CFile::typeText );
		while(file.ReadString(str)== TRUE)
		{
			if(!strFinal.IsEmpty())
			{
				strFinal = strFinal + _T(" ");
			}
			strFinal = strFinal + str;
		}

		file.Close();
	}
	CATCH( CFileException, e )
	{
		char szCause[255];
		e->GetErrorMessage(szCause, 255);
		LOG_MESSAGE(LM_ERROR, "Error '%s', reading temp file %s", szCause, strTempFilename);
		return "error";
	}
	END_CATCH

	return strFinal;
 
}
CString CSwatApp::ReadRegistrySetting(CString strParam)
{
	DWORD dwType;
	char  szVersion[500];
	DWORD dwDataSize=500;
	memset(szVersion,0,500);
	HKEY hKey;
	long lResult = RegOpenKeyEx(HKEY_CURRENT_USER, SWAT_REG_KEY, 0, KEY_READ, &hKey);
	if(ERROR_SUCCESS == lResult)
	{
	   lResult = RegQueryValueEx(hKey,strParam, NULL, &dwType, (BYTE*)szVersion, &dwDataSize);
	   if(ERROR_SUCCESS == lResult)
	   {
		   RegCloseKey(hKey);
		   return szVersion;
	   }
	   RegCloseKey(hKey);
	}
	else
	{
		LOG_MESSAGE(LM_ERROR, "Could not read registry setting");
	}
	return "ERROR";

}

void CSwatApp::SetWindowsPath()
{
	//Scite path
	if(GetFileAttributes("C:\\Program Files\\Lua\\5.1\\SciTE\\scite.exe")!= INVALID_FILE_ATTRIBUTES)
	{
		m_strSWAT_SCITE_PATH = "C:\\Program Files\\Lua\\5.1\\SciTE\\scite.exe";
	}
	else if(GetFileAttributes("C:\\Program Files (x86)\\Lua\\5.1\\SciTE\\scite.exe")!= INVALID_FILE_ATTRIBUTES)
	{
		m_strSWAT_SCITE_PATH = "C:\\Program Files (x86)\\Lua\\5.1\\SciTE\\scite.exe";
	}
	else
	{
		LOG_MESSAGE(LM_INFO, "Scite Path not found.");
	}

	//Decoda path
	if(GetFileAttributes("C:\\Program Files\\Decoda\\Decoda.exe")!= INVALID_FILE_ATTRIBUTES)
	{
		m_strSWAT_DECODA_PATH = "C:\\Program Files\\Decoda\\Decoda.exe";
	}
	else if(GetFileAttributes("C:\\Program Files (x86)\\Decoda\\Decoda.exe")!= INVALID_FILE_ATTRIBUTES)
	{
		m_strSWAT_DECODA_PATH = "C:\\Program Files (x86)\\Decoda\\Decoda.exe";
	}
	else
	{
		LOG_MESSAGE(LM_INFO, "Decoda Path not found.");
	}


	//Teraterm path
	if(GetFileAttributes("C:\\Program Files\\TTERMPRO\\ttpmacro.exe")!= INVALID_FILE_ATTRIBUTES)
	{
		m_strTTP_MACRO_EXE = "C:\\Program Files\\TTERMPRO\\ttpmacro.exe";
	}
	else if(GetFileAttributes("C:\\Program Files (x86)\\TTERMPRO\\ttpmacro.exe")!= INVALID_FILE_ATTRIBUTES)
	{
		m_strTTP_MACRO_EXE = "C:\\Program Files (x86)\\TTERMPRO\\ttpmacro.exe";
	}
	else
	{
		LOG_MESSAGE(LM_INFO, "TeraTerm Path not found.");
	}



}
void CSwatApp::ReadAllRegistrySettings()
{
	m_strTestScriptPath = ReadRegistrySetting(SWAT_REG_PARAM_TREE_PATH);
	if(m_strTestScriptPath.CompareNoCase("ERROR") == 0)
	{
		m_strTestScriptPath = "C:\\SWAT\\TestScripts";
	}

	m_strEditorPath = ReadRegistrySetting(SWAT_REG_PARAM_LUA_EDITOR);
	m_strEditorPath.Trim();
	if(m_strEditorPath.IsEmpty() || m_strEditorPath.CompareNoCase("ERROR") == 0)
	{
		m_strEditorPath = m_strSWAT_SCITE_PATH;
	}

	nSWAT_XOpen_Position = atoi(ReadRegistrySetting(SWAT_REG_X_POS));
	if(nSWAT_XOpen_Position == 0)
	{
		nSWAT_XOpen_Position = 100;
	}
}
//runs appName with cmdLine parameter and blocks execution till cmd line finishes
int CSwatApp::ExecuteBlocking(const char *appName, const char *cmdLine)
{	

	int nTimeOut = m_strArrFileNames.GetCount()+ 5;
	char sz[260];	
	sprintf(sz, "%s %s", appName, cmdLine);	
	STARTUPINFO siStartupInfo;
	
	
	PROCESS_INFORMATION piProcessInfo;	

	memset(&siStartupInfo, 0, sizeof(siStartupInfo));	
	memset(&piProcessInfo, 0, sizeof(piProcessInfo));	
	siStartupInfo.cb = sizeof(siStartupInfo);	
	siStartupInfo.wShowWindow = SW_HIDE;
	

	//	BOOL b = CreateProcess(0, sz, 0, 0, 0, CREATE_DEFAULT_ERROR_MODE|CREATE_NO_WINDOW, 0, 0, &siStartupInfo, &piProcessInfo);	
	BOOL b = CreateProcess(0, sz, 0, 0, 0, CREATE_NO_WINDOW, 0, 0, &siStartupInfo, &piProcessInfo);	

	if (b == 0)
	{	
		LOG_MESSAGE(LM_ERROR, "CreateProcess() failed");
		return -1;	
	}
	DWORD dw = WaitForSingleObject(piProcessInfo.hProcess, nTimeOut*1000);

	CloseHandle(piProcessInfo.hProcess);	
	CloseHandle(piProcessInfo.hThread);	
	
	if (dw == WAIT_TIMEOUT)
	{
		LOG_MESSAGE(LM_INFO, sz);
		LOG_MESSAGE(LM_ERROR, "WaitForSingleObject() returned timeout");
		return -1;
	}
	else if (dw == WAIT_FAILED)
	{
		LOG_MESSAGE(LM_ERROR, "WaitForSingleObject() returned wait failed");
		return -1;
	}

	return 0;
}

//Add the loaded file names to get versions later
CString CSwatApp::AddFileToRecordVersion(CString strFilePathName)
{
	m_strArrFileNames.Add(strFilePathName);
	return strFilePathName;
}
//Remove loaded file names 
void CSwatApp::RemoveFileToRecordVersion()
{
	m_strArrFileNames.RemoveAll();
}

//gets the clearcase file version for the report
CString CSwatApp::GetFileVersions( )
{
	if(!m_bRun) return "Stop triggered...";
	if(m_strRecordSWinReport=="false") return "not requested per settings";

	LogStatus("======================================================================", REPORT_RESULT_FOOTER);

	LogStatus("Clearcase versions of scripts run:", REPORT_MESSAGE);
	CString strFilePathName;

	int nCount = m_strArrFileNames.GetCount();
	for (int m = 0; m<nCount; m++)
	{
		strFilePathName = strFilePathName + " \"" + m_strArrFileNames.GetAt(m) + "\" ";
	}
	
	static int n=0; CString strCount; strCount.Format("%d",n++);//creating temp files for each time
	//delete if older files are there
	if(0 == DeleteFile(TEMP_GET_FILE_VERSION_BAT+strCount+".bat"))
	{
		DWORD dw = GetLastError(); if( ERROR_FILE_NOT_FOUND != dw) {LOG_MESSAGE(LM_ERROR,"Could not delete temp file getFileVer.bat", __FUNCTION__);}	
	}
	if(0 == DeleteFile(TEMP_GET_FILE_VERSION_1+strCount))
	{
		DWORD dw = GetLastError(); if( ERROR_FILE_NOT_FOUND != dw) {LOG_MESSAGE(LM_ERROR,"Could not delete temp file fileVer1", __FUNCTION__);}	
	}
	if(0 == DeleteFile(TEMP_GET_FILE_VERSION_2+strCount))
	{
		DWORD dw = GetLastError(); if( ERROR_FILE_NOT_FOUND != dw) {LOG_MESSAGE(LM_ERROR,"Could not delete temp file fileVer2", __FUNCTION__);}
	}
	////////Delete temp files over/////////////////////////////////////////////////////



	CString strCmd;
	strCmd.Format("cleartool.exe ls %s > %s 2> %s", strFilePathName, TEMP_GET_FILE_VERSION_1+strCount, TEMP_GET_FILE_VERSION_2+strCount);
	SaveString2LocalTempFile(TEMP_GET_FILE_VERSION_BAT+strCount+".bat", strCmd);
	DWORD dw = ExecuteBlocking(TEMP_GET_FILE_VERSION_BAT+strCount+".bat", NULL);
	if(dw == -1)
	{
		LOG_MESSAGE(LM_ERROR,"ExecuteBlocking() failed in %s", __FUNCTION__);
		return "ERROR";
	}
	
	
	//LOG_MESSAGE(LM_INFO, strCmd.GetBuffer());
	//strCmd.ReleaseBuffer();
/*
	CString str = ReadLocalTempFile(TEMP_GET_FILE_VERSION_1+strCount);
	str += ReadLocalTempFile(TEMP_GET_FILE_VERSION_2+strCount);
	if(str.Find("special") + str.Find("Error") + str.Find("CHECKEDOUT") + str.Find("hijacked") > -4)
	{
		LogStatus("Following version in not valid for FORMAL VERIFICATION: ", REPORT_WARNING);
	}
*/


	//////////////////////
	CString str;
	TRY
	{
		CStdioFile file( TEMP_GET_FILE_VERSION_1+strCount, CFile::modeRead | CFile::typeText );
		while(file.ReadString(str)== TRUE)
		{
			if((str.Find("special") + str.Find("Error") + str.Find("CHECKEDOUT") + str.Find("hijacked") + str.Find("LATEST") > -5) || (str.Find("Rule:")==-1) )
			{
				LogStatus("Following version in not valid for FORMAL VERIFICATION: ", REPORT_WARNING);
			}
			LogStatus(str, REPORT_MESSAGE);
		}
		file.Close();

		CStdioFile file2( TEMP_GET_FILE_VERSION_2+strCount, CFile::modeRead | CFile::typeText );
		while(file2.ReadString(str)== TRUE)
		{
			LogStatus(str, REPORT_WARNING);
		}
		file2.Close();

	}
	CATCH( CFileException, e )
	{
		char szCause[255];
		e->GetErrorMessage(szCause, 255);
		LOG_MESSAGE(LM_ERROR, "Error '%s', reading temp files to get file versions", szCause);
		return "error";
	}
	END_CATCH
	////////////////////////////

	LogStatus("======================================================================", REPORT_RESULT_FOOTER);


	////////Delete temp files/////////////////////////////////////////////////////
	if(0 == DeleteFile(TEMP_GET_FILE_VERSION_BAT+strCount+".bat"))
	{
		DWORD dw = GetLastError();
		LOG_MESSAGE(LM_INFO,"Could not delete temp file getFileVer.bat", __FUNCTION__);
	}
	if(0 == DeleteFile(TEMP_GET_FILE_VERSION_1+strCount))
	{
		DWORD dw = GetLastError();
		LogStatus("Could not delete temp file filever1.txt", REPORT_RED);
	}
	if(0 == DeleteFile(TEMP_GET_FILE_VERSION_2+strCount))
	{
		DWORD dw = GetLastError();
		LogStatus("Could not delete temp file filever2.txt", REPORT_RED);
	}
	////////Delete temp files over/////////////////////////////////////////////////////

	LOG_MESSAGE(LM_INFO, "Returning from GetFileVersion()");

	return str;

}


void CSwatApp::EnableBDSendSettings()
{

 try
	{
        m_pBDSendSettings = new CBDSendSettings();
    }
    catch(...)
	{
        LOG_MESSAGE(LM_ERROR, "Could not start PCApp dll to push BD settings through dll");
    }
}       

void CSwatApp::CaptureSettings(void)
{
	

   try
	{
        m_pGUISettingsCapture = new CGUISettingsCapture();

    }
    catch(...)
	{
        LOG_MESSAGE(LM_ERROR, "Could not start PCApp dll for capturing settings");
    }
        
}




int CSwatApp::ExitInstance()
{
	
	m_bVentDownloaderRun = false;
	//Sleep(1000);

#ifdef ENABLE_DOWNLOADER
	::VikingConsoleDriver_finish(m_pVikingConsoleDriver);
#endif

	SaveRegistrySetting( SWAT_REG_PARAM_LAST_FILE, m_strCurrentOpenScriptListFile);



	if(nSWAT_XClose_Position<0) nSWAT_XClose_Position = 10;
	char ch[12];
	SaveRegistrySetting(SWAT_REG_X_POS, _itoa(nSWAT_XClose_Position, ch, 10));




	if(m_bGUIConnected)
	{
		GuiIOError err;
		err = GuiIODisconnect();
	}

	BOOL bFreeResult;
	if(NULL != g_DataCapture)
	{
		g_DataCapture->stop();
	}
	bFreeResult  = FreeLibrary(m_hinstLib);


	if(m_SWAT_log_file.m_pStream)
	{
		m_SWAT_log_file.Close();
	}


	CloseDB();

	WSACleanup(); //close sockets etc.

	delete m_pGUISettingsCapture;
	return CWinApp::ExitInstance();
}



// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
public:
	CString m_strHelp;
	afx_msg void OnBnClickedOk();
	virtual BOOL OnInitDialog();
	CHyperLink m_hyper;
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
, m_strHelp(_T(""))
{

	m_strHelp = "Supported commands thru Lua: \r\n"
		"log_messageEx(\"str\") \r\n"
		"show_message(\"str\") \r\n"\
		"repeat_message(\"str\") \r\n"\
		"show_and_continue_message(\"str\") \r\n"
		"clear_message() \r\n"
		"stop_script() (script uses it to check if stop clicked or error occured)\r\n"
		"error_occuredEx( ) (triggers stop from the script) \r\n"
//		"gui_setvalue(\"button ID\", \"value\") \r\n"

		"gui_LButtonDown(\"x\", \"y\")   \r\n"
		"gui_LButtonUp(\"x\", \"y\")   \r\n"
		"gui_MousePosition(\"x\", \"y\")   \r\n"
		

		"gui_IOClickXY(X, Y, 1 (longpress))         \r\n"
		"gui_IOClickXYDirect(int, int)   \r\n"

		"gui_IOGetValue(\"RESID\")    \r\n"
		"gui_IOGetText(\"RESID\")    \r\n"
		"gui_IOKnob(\"-12\") \r\n"
		"gui_IODisconnect( )  \r\n"
		"gui_is_activated(\"RESID\")    \r\n"

		"get_breath_phase( )  \r\n"
		"1: Inspiration, 2: Exhalation  \r\n"

		"\r\n\r\n"

		"BDPushSettings(\"O2=21 RESP_RATE=12 ...\", \"IP\", \"Port\") \r\n"
		
		"\r\n\r\n"

		"enable_db_logging( )  \r\n"
		"disable_db_logging( )    \r\n"

		"bezel_key_click(\"RESID\" ) \r\n"
		"get_focus_button( ) - returns the resID which has current focus otherwise ERROR \r\n"

		"over_write_pd_valueEx(\"Parameter\", \"Value\" ) -overwrite a PD value \r\n"

		"get_raw_pd_valueEx(\"param\") - get raw value of PD sent from BD to GUI   \r\n"


		"reset_pd_valuesEx( )  -Reset all PD overwritten values  \r\n"

		"VerifyManually_Ex( strParam, strExpectedBehaviour ) - shows pass/fail and skips for timeout \r\n"

		"setting_capture_get_valueEx(strSetting)   \r\n"
		"power_on_off_vent(bool)   \r\n"
	
		
		"sound( ) - plays wave file c:\\swat\\swat.wav \r\n"

		"gui_capture_screen(strFileName) - saves captured image in the script path \r\n"

		"ping_vent( ) - pings BD and GUI, returns false if both or either of them fails \r\n"
		"gui_simulate_alarm(\"A380\", \"G38001,G38006\", \"A235,A260,A265\") -  \r\n"
		"turn_on_hit_s( )                  \r\n"

		"watermarkonoff(0 or 1) \r\n"
		"mouseonoff(0 or 1)   \r\n"
		"setwidgetproperties(strWidgetName, n) \r\n"

		"set_report_for_language_comparison(0 or 1)    \r\n"

		"get_count_png_to_copy()          \r\n"


		"get_vent_language( )                \r\n"
		


		"\r\n\r\n"

		"Supported settings examples thru init.Lua: \r\n"
		"bd_host=\"192.168.0.1\"			\r\n"
		"gui_host=\"192.168.0.2\"			\r\n"
		
		"take_screen_shot_for_failure=true  \r\n"
//		"bd_jtag_ip=\"192.168.0.26:2001\"   \r\n"
//		"gui_jtag_ip=\"192.168.0.25:2001\"  \r\n"
		"power_switch_ip=\"192.168.0.50\"   \r\n"
		"port_vent_switch=\"1\"				\r\n"
		"port_service_mode=\"4\"			\r\n"
		"port_power=\"3\"					\r\n"
		"my_pc_ip=\"192.168.0.201\"			\r\n"
		"bd_com_port=\"1\"					\r\n"
		"VERBOSE_LOG = 0					\r\n"
		"TRACE = 0							\r\n"
		
		

		""
		""
		;
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_COMMANDS, m_strHelp);
	DDX_Control(pDX, IDC_STATIC_SWAT_BUILDS, m_hyper);
}

BOOL CAboutDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	CString str;
	str.Format("SWAT Build date/time : %s - %s",__DATE__, __TIME__);
	GetDlgItem(IDC_STATIC_DATE_TIME)->SetWindowTextA(str);
	GetDlgItem(IDC_STATIC_SWAT_VERSION)->SetWindowTextA("SWAT Version: "+((CSwatApp*)AfxGetApp())->strSWATVersion);

	m_hyper.SetURL("\\\\racarl-rd01\\Viking_Software\\work\\rohit.vij\\SWAT_Installation");



	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CAboutDlg::OnBnClickedOk)
END_MESSAGE_MAP()

// App command to run the dialog
void CSwatApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}


// CSwatApp message handlers


void CSwatApp::OnFileNew()
{
	// TODO: Add your command handler code here
}

void CSwatApp::LogStatus(CString str, int n)
{

	//updating the time
	m_ScriptResults.m_timeStop = CTime::GetCurrentTime();

/*	lua_Debug ar;
	lua_getinfo(m_L, "nsl", &ar);
	int line = ar.currentline;   
*/

//	stack traceback: s:\TestScripts\tempFiles\rohit\debug__.lua:2: in function 'erroroccured' s:\TestScripts\tempFiles\rohit\debug__.lua:16: in function 'high' s:\TestScripts\tempFiles\rohit\debug__.lua:12: in function 'mid' s:\TestScripts\tempFiles\rohit\debug__.lua:8: in function 'low' s:\TestScripts\tempFiles\rohit\debug__.lua:21: in main chunk


	if(str.Left(15).CompareNoCase("stack traceback") ==0)
	{
		LOG_MESSAGE(LM_INFO, str.GetBuffer());
		str.ReleaseBuffer();

		CString strPost = "Stack traceback follows:";
//		CString* p = new CString(strPost);
//		SendMessage(AfxGetMainWnd()->m_hWnd, WM_NEW_LOG_RCVD, 0, reinterpret_cast<LPARAM>(p));

		CString str2parse(str);
		str2parse = str2parse + "\n"; 
		CString strLog;
		int n = str2parse.Find("\n");

		int nIndex = 0;

		while(n>-1)
		{
			int m = str2parse.Find("\n", n+1);
			strLog = str2parse.Mid(n+2, m-n-2);
			if(strLog.IsEmpty()) break;
			
			//PostLog
			if(nIndex > 0) {
			strPost = ">>>"+strLog+"<<<";
			CString* p = new CString(strPost);
			SendMessage(AfxGetMainWnd()->m_hWnd, WM_NEW_LOG_RCVD, 0, reinterpret_cast<LPARAM>(p));
			}//

			int nlen = str2parse.GetLength();
			str2parse = str2parse.Right( nlen - m);
			n = str2parse.Find("\n");
			
			nIndex++;
		}

	}
	else
	{
		CString strLog(str);


		if(m_bAppendTimeInStatusWindow)
		{
			SYSTEMTIME systime; GetLocalTime(&systime); CString strTime;
			strTime.Format("%d:%d:%d:%d ",systime.wHour,systime.wMinute, systime.wSecond, systime.wMilliseconds);
			strLog = strTime+strLog;
		}

		if(n == REPORT_TEST_PASS)
		{	
			strLog = "PASS - " + str;
			m_ScriptResults.m_nPass++;
			m_ScriptResults.m_nTotal++;

			m_SessionResults.m_nPass++;
			m_SessionResults.m_nTotal++;
		}
		else if(n == REPORT_TEST_FAIL)
		{	
			strLog = "FAIL - " + str;
			m_ScriptResults.m_nFail++;
			m_ScriptResults.m_nTotal++;

			m_SessionResults.m_nFail++;
			m_SessionResults.m_nTotal++;

		}	
		else if(n == REPORT_WARNING)
		{	
			m_ScriptResults.m_nWarnings++;
			m_SessionResults.m_nWarning++ ;
		}
		else if(n == REPORT_ERROR)
		{	
			m_ScriptResults.m_nErrors++;
			m_SessionResults.m_nError++ ;
		}
		else if( n == REPORT_TEST_OBJECTIVE)
		{
			strLog.Replace(REPORT_OBJ_SEPERATOR, "  ");
			m_ScriptResults.m_nObjective++;
		}

		
		if( n == REPORT_IMAGE)
		{	strLog = "Capturing image : "+strLog;
		}
		CString* p = new CString(strLog);
		SendMessage(AfxGetMainWnd()->m_hWnd, WM_NEW_LOG_RCVD, n, reinterpret_cast<LPARAM>(p));
		


		//update the results in the list box if we get any pass/fail from the script
		if((n == REPORT_TEST_PASS) || (n == REPORT_TEST_FAIL)
			|| (n==REPORT_WARNING) || (n == REPORT_ERROR))
		{
			GetScriptResult(m_ScriptResults.m_strFileName);
		}
	}

/*	if(n == REPORT_WARNING)
	{
		m_SessionResults.m_nWarning++ ;
		m_ScriptResults.m_nWarnings ++;
		
	}else
	if(n == REPORT_ERROR)
	{
		m_SessionResults.m_nError++ ;
	}
*/
	if(n > 0) 
	{
		LogReportMessage(str, n);
	}
	
}

void CSwatApp::InitLua()
{
	//if already in session, close that and open new
	if(m_L) 
	{
		CloseLua();
	}

	/* initialize Lua */
	m_L = lua_open();



	/* load various Lua libraries */
	luaL_openlibs(m_L);

	//Register the functions:
	lua_register(m_L, "log_messageEx", log_messageEx);
	lua_register(m_L, "show_message", show_message);
	lua_register(m_L, "repeat_message", repeat_message);
	lua_register(m_L, "ask_user", ask_user);

	lua_register(m_L, "resetsettings", resetsettings); //remove
	lua_register(m_L, "sendsettings", sendsettings);  //remove
	lua_register(m_L, "show_and_continue_message", show_and_continue_message);
	lua_register(m_L, "clear_message", clear_message);

	lua_register(m_L, "gui_setvalue", gui_setvalue); //remove
	lua_register(m_L, "gui_LButtonDown", gui_LButtonDown);
	lua_register(m_L, "gui_LButtonUp", gui_LButtonUp);
	lua_register(m_L, "gui_MousePosition", gui_MousePosition); //remove?
	lua_register(m_L, "gui_IOGetXYWH", gui_IOGetXYWH);

	lua_register(m_L, "gui_IOClickXY", gui_IOClickXY);

	lua_register(m_L, "gui_IOClickXYDirect", gui_IOClickXYDirect);

	lua_register(m_L, "gui_IOGetValue", gui_IOGetValue); //remove
	lua_register(m_L, "gui_IOGetText", gui_IOGetText);
	lua_register(m_L, "gui_IOKnob", gui_IOKnob);
	lua_register(m_L, "gui_is_activated", gui_is_activated);
	lua_register(m_L, "gui_is_selected", gui_is_selected);

	lua_register(m_L, "gui_is_blinking", gui_is_blinking);
	lua_register(m_L, "gui_is_adornment", gui_is_adornment);


	lua_register(m_L, "stop_script", stop_script);
	lua_register(m_L, "error_occuredEx", error_occuredEx);

	lua_register(m_L, "BDPushSettings", BDPushSettings);     //remove
	lua_register(m_L, "gui_IODisconnect", gui_IODisconnect);  
	lua_register(m_L, "gui_IOConnect", gui_IOConnect);         

	lua_register(m_L, "enable_db_logging", enable_db_logging);   //for debug only, not for IUV 
	lua_register(m_L, "disable_db_logging", disable_db_logging);  //for debug only, not for IUV 

	lua_register(m_L, "get_breath_phase", get_breath_phase);
	lua_register(m_L, "get_breath_type", get_breath_type);
	lua_register(m_L, "get_breath_duration", get_breath_duration);
	lua_register(m_L, "get_breath_ie", get_breath_ie);

	lua_register(m_L, "is_autozero_active", is_autozero_active);

	lua_register(m_L, "get_focus_button", get_focus_button);

	lua_register(m_L, "bezel_key_click", bezel_key_click);
	lua_register(m_L, "include", include);

	lua_register(m_L, "over_write_pd_valueEx", over_write_pd_valueEx);
	
	lua_register(m_L, "get_raw_pd_valueEx", get_raw_pd_valueEx);

	lua_register(m_L, "reset_pd_valuesEx", reset_pd_valuesEx);

	lua_register(m_L, "verifyManually_Ex", verifyManually_Ex); 

	lua_register(m_L, "SWATGetScriptPath", SWATGetScriptPath); 
	lua_register(m_L, "pause", pause);                             //for debug only, not for IUV 
	
	lua_register(m_L, "power_on_off_vent", power_on_off_vent); 

	lua_register(m_L, "setting_capture_get_valueEx", setting_capture_get_valueEx);

	lua_register(m_L, "get_sent_settingEx", get_sent_settingEx);    //remove

	lua_register(m_L, "gui_capture_screen", gui_capture_screen);    

	lua_register(m_L, "gui_get_screen_capture_status", gui_get_screen_capture_status);

	lua_register(m_L, "gui_simulate_alarm", gui_simulate_alarm);

	lua_register(m_L, "sound", sound);    //play sound

	lua_register(m_L, "getuserinput", getuserinput);    //

	//get consolidated row from a table
	lua_register(m_L, "gui_IOGetRowText", gui_IOGetRowText);
	lua_register(m_L, "gui_IOGetCellText", gui_IOGetCellText);

	lua_register(m_L, "ping_vent", ping_vent);
	lua_register(m_L, "turn_on_hit_s", turn_on_hit_s);

	lua_register(m_L, "testLua", testLua);    //for test only

	lua_register(m_L, "watermarkonoff", watermarkonoff);    
	lua_register(m_L, "mouseonoff", mouseonoff);   
	lua_register(m_L, "set_report_for_language_comparison", set_report_for_language_comparison);   

	lua_register(m_L, "get_count_png_to_copy", get_count_png_to_copy);   
	lua_register(m_L, "get_vent_language", get_vent_language);   

	lua_register(m_L, "setwidgetproperties", setwidgetproperties);   

	Luna<Batch>::Register(m_L);





	LOG_MESSAGE(LM_INFO, "Lua initialized.");
}

void CSwatApp::CloseLua()
{
	
	Reset_PD_ValuesEx( );
	if(m_L)
	{
		lua_close(m_L);
		m_L = NULL;
		LOG_MESSAGE(LM_INFO, "Lua closed.");
	}

	//Return if report file not valid, nothing to do then 
	if(m_ReportFile.m_pStream == NULL)
	{
		return;
	}

	//otherwise check if need to make read only
	m_ReportFile.Close();

	//If all passed then set it read only
	if(m_bSetCurrentReportReadOnly == TRUE)
	{
		CFileStatus status; 
		m_ReportFile.GetStatus(status); 
		status.m_attribute = CFile::readOnly; 
		m_ReportFile.SetStatus(m_ReportFile.GetFilePath(),status); 

		CString str;
		str.Format("ALL PASS REPORT %s MARKED READ ONLY!", m_ReportFile.GetFileName());
		LogStatus(str, SHOW_STATUS_DONT_LOG);
	}
	m_ReportFile.m_pStream = NULL;

}


void CSwatApp::EmailUser(CString strStringOrFilename, CString strSubject, BOOL bIsFile)
{
	
	if(!m_bSendMail) return;

	CString strCmd;

	if(bIsFile)
	{//perl SendEmail.pl "FILE:./tmp_email.html" "rohit.vij" "My Subject Line2"

		strCmd.Format("perl %s \"FILE:%s\" \"%s\" \"%s\"  2>c:\\swat\\temp\\log.log", SWAT_EMAIL_PL, strStringOrFilename, m_strUserName, strSubject );


	}
	else
	{//perl SendEmail.pl "Some small string for mail-body" "rohit.vij" "My Subject Line"
		strCmd.Format("perl %s \"%s\" \"%s\" \"%s\"  2>c:\\swat\\temp\\log.log", SWAT_EMAIL_PL, strStringOrFilename, m_strUserName, strSubject);
		
	}




	SaveString2LocalTempFile(SWAT_EMAIL_BAT, strCmd);

	HINSTANCE n = ShellExecute(m_pMainWnd->m_hWnd, NULL, SWAT_EMAIL_BAT, NULL, NULL, SW_HIDE ); 
	if((int)n <32)
	{
		char ch[30];
		_itoa((int)n, ch, 10);
		CString str;
		str.Format("error emailing log file. shellexecute returned: %s",ch);
		LogStatus(str, REPORT_RED);
	}
	
//	DeleteFile(SWAT_EMAIL_BAT);

}

void CSwatApp::CreateReportFile(CString& strSessionID)
{
	
}

bool CSwatApp::OpenDB()
{
	LOG_MESSAGE(LM_INFO,"Enter %s", __FUNCTION__);
	CTime time1 = CTime::GetCurrentTime();

	try{
	
		if(!m_SessionSet.IsOpen())
		{
			if(m_SessionSet.Open(CRecordset::dynaset)==0)
			{
				AfxMessageBox("Session recordset open failed");
				return 0;
			}
		}

		if(!m_TestResultSet.IsOpen())
		{
			if(m_TestResultSet.Open(CRecordset::dynaset)==0)
			{
				AfxMessageBox("Test Results recordset open failed");
				return 0;
			}
		}

		if(!m_TestGraphset.IsOpen())
		{
			if(m_TestGraphset.Open(CRecordset::dynaset)==0)
			{
				AfxMessageBox("Event Data recordset open failed");
				return 0;
			}
		}
	}
	
	catch(CDBException db)
	{
		AfxMessageBox(db.m_strError);
		return 0;
	}
	catch(CMemoryException mem)
	{
		mem.ReportError();
		return 0;
	}
	catch(...)
	{
		AfxMessageBox("Error opening database. Check that database is installed and properly configured.\nAll data logging disabled.",MB_ICONSTOP);
		return 0;
		
	}

	CTime time2 = CTime::GetCurrentTime();

	CTimeSpan timeDelay = time2-time1;

	//If it took long time to connect to the database, it will delay the test execution as well
	if(timeDelay.GetTotalSeconds() > 5)
	{
		AfxMessageBox("Large number of database entries will make test execution sluggish. \nPlease clean database for faster execution");
	}

	LOG_MESSAGE(LM_INFO,"Exit %s", __FUNCTION__);
	return true;

}


bool CSwatApp::CloseDB()
{
	
	try{
	
		if(m_SessionSet.IsOpen())
		{
			m_SessionSet.Close();
		}

		if(m_TestResultSet.IsOpen())
		{
			m_TestResultSet.Close();
		}

		if(m_TestGraphset.IsOpen())
		{
			m_TestGraphset.Close( );
		}
	}
	
	catch(CDBException db)
	{
		AfxMessageBox(db.m_strError);
		return 0;
	}
	catch(CMemoryException mem)
	{
		mem.ReportError();
		return 0;
	}
	catch(...)
	{
		AfxMessageBox("Error closing database.",MB_ICONSTOP);
		return 0;
		
	}
	
	LOG_MESSAGE(LM_INFO,"DB closed");
	return true;

}

void CSwatApp::ShowContinueMessage(CString strMessage)
{
//	if(m_ShowMsgContinueDlg == NULL)
	{
		m_ShowMsgContinueDlg = new CShowMsgContinueDlg();
		m_ShowMsgContinueDlg->Create(IDD_SHOW_CONT_DLG);
	}
	m_ShowMsgContinueDlg->ShowWindow(SW_SHOW);
	m_ShowMsgContinueDlg->ShowString(strMessage);


}
void CSwatApp::ClearMessage()
{
	if(m_ShowMsgContinueDlg != NULL)
		m_ShowMsgContinueDlg->ShowWindow(SW_HIDE);

}
void CSwatApp::CreateSessionID()
{
	LOG_MESSAGE(LM_INFO,"Enter %s", __FUNCTION__);


	m_timeSessionStart = CTime::GetCurrentTime();
	m_strCurrentSessionID.Format("%s_%s",m_timeSessionStart.Format("%Y%m%d_%H%M%S"),m_strUserName);

	if(!m_SessionSet.CanAppend())
	{

		AfxMessageBox("Can NOT append m_SessionSet. Please restart SWAT.");
		return;

	}
	try
	{
		m_SessionSet.AddNew();

	}
	catch(...)
	{
		AfxMessageBox("Error AddNew m_SessionSet. Please restart SWAT.");
		LOG_MESSAGE(LM_ERROR,"CSwatApp::CreateSessionID() ExecuteSQL() threw unknown exception.");
		return;
	}

	m_SessionSet.m_user_name = m_strUserName;
	m_SessionSet.m_test_session_id = m_strCurrentSessionID;

	try
	{
		m_SessionSet.Update();
	}
	catch(CDBException db)
	{
		LOG_MESSAGE(LM_ERROR, db.m_strError.GetBuffer());
		db.m_strError.ReleaseBuffer();
		AfxMessageBox(db.m_strError);
		return;
	}
	catch(CMemoryException mem)
	{
		mem.ReportError();
		AfxMessageBox("Error updating m_SessionSet. Out of Memory. Please restart SWAT.");
		return;
	}
	catch(...)
	{
		AfxMessageBox("Error Update() m_SessionSet. Please restart SWAT.");
		LOG_MESSAGE(LM_ERROR,"CSwatApp::CreateSessionID() ExecuteSQL() threw unknown exception.");
		return;
	}

	LOG_MESSAGE(LM_INFO,"Exit %s", __FUNCTION__);

}


//Get pass/fail/info/total for the strFile for current session ID
void CSwatApp::GetScriptResult(CString strFile)
{
	LOG_MESSAGE(LM_INFO,"Enter %s", __FUNCTION__);


	CMDIFrameWnd *pFrame = (CMDIFrameWnd*)AfxGetApp()->GetMainWnd();
	if(!pFrame) return;

	CMDIChildWnd *pChild = (CMDIChildWnd*)pFrame->GetActiveFrame();
	if(!pChild) return;

	CSwatView *pView = (CSwatView*)pChild->GetActiveView();
	if(pView) pView->UpdateScriptResult(strFile);

	LOG_MESSAGE(LM_INFO,"Exit %s", __FUNCTION__);


}
//Get pass/fail/info/total for the strFile for current session ID
CString CSwatApp::GetScriptNameToRun( )
{
	LOG_MESSAGE(LM_INFO,"Enter %s", __FUNCTION__);


	CMDIFrameWnd *pFrame = (CMDIFrameWnd*)AfxGetApp()->GetMainWnd();
	if(!pFrame) return "";

	CMDIChildWnd *pChild = (CMDIChildWnd*)pFrame->GetActiveFrame();
	if(!pChild) return "";

	CSwatView *pView = (CSwatView*)pChild->GetActiveView();
	if(pView) 
	{
		return pView->GetScriptNameToRun();
	}

	LOG_MESSAGE(LM_INFO,"Exit %s", __FUNCTION__);

	return "";


}

void CSwatApp::Create_Lua_Init_Exit_Files(CString strScriptFile)
{



	//Create Lua Init file to pass on global variable values for each script
	try
	{
		CString strTemp;
		strScriptFile.Replace("\\", "\\\\");

		CStdioFile file( TEMP_SCRIPT_INIT, CFile::modeCreate | CFile::modeWrite | CFile::typeText );
		strTemp.Format("fileName = [[%s]] \r\n", strScriptFile);
		file.WriteString(strTemp);
		strTemp.Format("SessionID = \"%s\" \r\n", m_strCurrentSessionID);
		file.WriteString(strTemp);

		file.WriteString("GLOBAL_lock(_G)\r\n");


		
		file.Close();
	}
	catch( CFileException* e )
	{
		CString str;
		str.Format("Temp_script_init could not be opened or created, cause = %d\n", e->m_cause);
		AfxMessageBox(str);
		e->Delete();
	}

	//Create session Exit file
	try
	{
		CString strTemp;
		CStdioFile file( TEMP_SESSION_EXIT, CFile::modeCreate | CFile::modeWrite | CFile::typeText );
		file.WriteString("--not used--");
		file.Close();
	}
	catch( CFileException* e )
	{
		CString str;
		str.Format("Temp_script_exit could not be opened or created, cause = %d\n", e->m_cause);
		AfxMessageBox(str);
		e->Delete();
	}

	//Create Lua script Exit file
	try
	{
		CString strTemp;
		CStdioFile file( TEMP_SCRIPT_EXIT, CFile::modeCreate | CFile::modeWrite | CFile::typeText );

		//file.WriteString("swat.log_message(\"closing Lua DB connection.. \") \r\n");

		
		file.WriteString("if(false == con:close()) then swat.log_message(\"connection closing failed \") end \r\n");

		//file.WriteString("swat.log_message(\"Done \") \r\n");

		//file.WriteString("swat.log_message(\"closing Lua ODBC connection.. \") \r\n");

		
		file.WriteString("if(false == odbc:close() ) then swat.log_message(\"odbc closing failed \") end \r\n");
		//file.WriteString("swat.log_message(\"Done \") \r\n");

		file.Close();
	}
	catch( CFileException* e )
	{
		CString str;
		str.Format("Temp_session_exit could not be opened or created, cause = %d\n", e->m_cause);
		AfxMessageBox(str);
		e->Delete();
	}

}

//called whenever new set of setting is intercepted from GUI to BD
void CSwatApp::GetNewSettings(CString str)
{

	//captured settings, 0 is latest
	m_strSettings0, m_strSettings1, m_strSettings2, m_strSettings3, m_strSettings4; 

	m_strSettings4 = m_strSettings3;
	m_strSettings3 = m_strSettings2;
	m_strSettings2 = m_strSettings1;
	m_strSettings1 = m_strSettings0;
	m_strSettings0 = str;


	m_capturedSettingsList.RemoveAll();
	//load newest values in a list for exposing to Lua
	CString strFromSettingCapture = str; //Latest settings
	for(int nIndex = 1; nIndex < 55; nIndex++)
	{
		int nNewLine = strFromSettingCapture.Find('\n');
		if( nNewLine == -1) break;

		CString strLine = strFromSettingCapture.Left(nNewLine);//grab one line and parse to get data/parameter/id etc
		CString strID, strParam, strType, strValue;
		int n = strLine.Find(',');
		strID = strLine.Left(n);

		strLine = strLine.Right(strLine.GetLength()-n-1);
		n = strLine.Find(',');
		strParam = strLine.Left(n);

		strLine = strLine.Right(strLine.GetLength()-n-1);
		n = strLine.Find(',');
		strType = strLine.Left(n);
		strValue = strLine.Right(strLine.GetLength()-n-1);

		//add parsed parm name and setting to a struct
		CapturedSettings newSetting;
		newSetting.strSettingName = strParam;
		newSetting.strSettingValue = strValue;

		//add the struct to the end of the list. this list is exposed to Lua get the values
		m_capturedSettingsList.AddTail(newSetting);

		strFromSettingCapture = strFromSettingCapture.Right(strFromSettingCapture.GetLength()-nNewLine -1);// -1 for new line
	}


}

void CSwatApp::GenerateReport(CString strScriptName, CString strSessionID)
{
	if(strScriptName.IsEmpty())
	{
		return;
	}
	strScriptName.Replace("\\", "\\\\");
	CTestResultsDlg resultDlg;
	resultDlg.SetResultParams( strSessionID , strScriptName);
	resultDlg.DoModal();

}

void CSwatApp::OnSwatSwDwnload()
{
	if(m_strVentType.IsEmpty())
	{
		AfxMessageBox("Please set vent_type in SWATinit.lua");
		return;
	}
	if(m_strVentType != "2.0")
	{
		AfxMessageBox("Only intended for P2.0 systems. Double check 'vent_type' in SWATinit.lua");
		return;
	}

	if(m_pDownloadSWDlg == NULL)
	{
		m_pDownloadSWDlg = new CDownloadSWDlg;
		if(!m_pDownloadSWDlg->Create(IDD_DOWNLOAD_SW_DLG))
		{
			delete m_pDownloadSWDlg;
			m_pDownloadSWDlg = NULL;
			return;
		}
		m_pDownloadSWDlg->ShowWindow(SW_SHOW);
	}
	else
	{
		m_pDownloadSWDlg->ShowWindow(SW_SHOW);
	}

}

void CSwatApp::OnSwatCaptureSettings()
{

	CCaptureSettingsDlg dlg;
	dlg.DoModal();
}



bool CSwatApp::GetSetLungSetup()
{
	if(GetFileAttributes(TEMP_LUNG_SETUP_DETAILS)== INVALID_FILE_ATTRIBUTES)
	{
		m_strLungName = "Please enter details.";
	}
	else{
		m_strLungName = ReadLocalTempFile(TEMP_LUNG_SETUP_DETAILS);
	}
	
	CLungSetupDlg dlg;
	dlg.m_strLungsetup = m_strLungName;
	if(dlg.DoModal()==IDOK)
	{
		m_strLungName = dlg.m_strLungsetup;
		SaveString2LocalTempFile(TEMP_LUNG_SETUP_DETAILS, m_strLungName);
		return true;
	}else
	{
		PostQuitMessage(0);
		return false;
	}

	
}


void CSwatApp::MapDrive()
{

	CStdioFile file( SWAT_SETTING_FILE, CFile::modeRead | CFile::typeText );
	CString str; BOOL bRead;
	do
	{
		bRead = file.ReadString(str);
		if(str.Find("drive_path_cmd") > -1)
		{
			if(str.Left(2).Compare("--") != 0)
				break;
		}

	}
	while (bRead);

	if(bRead == NULL) 
	{
		AfxMessageBox("drive_pat_cmd not found in the init file");
		return;
	}
	int n = str.Find("=");
	CString strCmd = str.Right(str.GetLength()-n-2);

	strCmd = strCmd.Left(strCmd.GetLength()-1);

	LOG_MESSAGE(LM_INFO,"Drive command for test scripts : %s ", strCmd);
	file.Close();
	
	system(strCmd);
}

void CSwatApp::ReadLuaInitFile()
{
	CStdioFile file( SWAT_SETTING_FILE, CFile::modeRead | CFile::typeText );
	CString str; BOOL bRead;

	do
	{
		bRead = file.ReadString(str);
		str.Trim();
		if(str.Find("bd_host") > -1)
		{
			if(str.Left(2).Compare("--") != 0)
			{	int n = str.Find("=");
				m_strBD_IPAddr = str.Right(str.GetLength()-n-2);
				m_strBD_IPAddr = m_strBD_IPAddr.Left(m_strBD_IPAddr.GetLength()-1);
			}
		} else
		if(str.Find("gui_host") > -1)
		{
			if(str.Left(2).Compare("--") != 0)
			{	int n = str.Find("=");
				m_strGUI_IPAddr = str.Right(str.GetLength()-n-2);
				m_strGUI_IPAddr = m_strGUI_IPAddr.Left(m_strGUI_IPAddr.GetLength()-1);
			}
		}else
		if(str.Find("bd_jtag_ip") > -1)
		{
			if(str.Left(2).Compare("--") != 0)
			{	int n = str.Find("=");
				m_strBD_JTAG_IPAddr = str.Right(str.GetLength()-n-2);
				m_strBD_JTAG_IPAddr = m_strBD_JTAG_IPAddr.Left(m_strBD_JTAG_IPAddr.GetLength()-1);
			}
		}else
		if(str.Find("gui_jtag_ip") > -1)
		{
			if(str.Left(2).Compare("--") != 0)
			{	int n = str.Find("=");
				m_strGUI_JTAG_IPAddr = str.Right(str.GetLength()-n-2);
				m_strGUI_JTAG_IPAddr = m_strGUI_JTAG_IPAddr.Left(m_strGUI_JTAG_IPAddr.GetLength()-1);
			}
		}else
		if(str.Find("power_switch_ip") > -1)
		{
			if(str.Left(2).Compare("--") != 0)
			{	int n = str.Find("=");
				m_strPowerSwitch_IPAddr = str.Right(str.GetLength()-n-2);
				m_strPowerSwitch_IPAddr = m_strPowerSwitch_IPAddr.Left(m_strPowerSwitch_IPAddr.GetLength()-1);
			}
		}else
		if(str.Find("my_pc_ip") > -1)
		{
			if(str.Left(2).Compare("--") != 0)
			{	int n = str.Find("=");
				m_strMyPC_IPAddr = str.Right(str.GetLength()-n-2);
				m_strMyPC_IPAddr = m_strMyPC_IPAddr.Left(m_strMyPC_IPAddr.GetLength()-1);
			}
		}else
		if(str.Find("port_vent_switch") > -1)
		{
			if(str.Left(2).Compare("--") != 0)
			{	int n = str.Find("=");
				m_strPortVentSwitch = str.Right(str.GetLength()-n-2);
				m_strPortVentSwitch = m_strPortVentSwitch.Left(m_strPortVentSwitch.GetLength()-1);
			}
		}else
		if(str.Find("port_service_mode") > -1)
		{
			if(str.Left(2).Compare("--") != 0)
			{	int n = str.Find("=");
				m_strPortServiceMode = str.Right(str.GetLength()-n-2);
				m_strPortServiceMode = m_strPortServiceMode.Left(m_strPortServiceMode.GetLength()-1);
			}
		}else
		if(str.Find("port_power") > -1)
		{
			if(str.Left(2).Compare("--") != 0)
			{	int n = str.Find("=");
				m_strPortPower = str.Right(str.GetLength()-n-2);
				m_strPortPower = m_strPortPower.Left(m_strPortPower.GetLength()-1);
			}
		}else
		if(str.Find("bd_com_port") > -1)
		{
			if(str.Left(2).Compare("--") != 0)
			{	int n = str.Find("=");
				m_strBDComPort = str.Right(str.GetLength()-n-2);
				m_strBDComPort = m_strBDComPort.Left(m_strBDComPort.GetLength()-1);
			}
		}
		else
		if(str.Find("vent_type") > -1)
		{
			if(str.Left(2).Compare("--") != 0)
			{	int n = str.Find("=");
				m_strVentType = str.Right(str.GetLength()-n-2);
				m_strVentType = m_strVentType.Left(m_strVentType.GetLength()-1);
			}
		}
	}
	while (bRead);
	file.Close();



}
void CSwatApp::ReadErrorPopupValue()
{
	
	CStdioFile file( SWAT_SETTING_FILE, CFile::modeRead | CFile::typeText );
	CString str; BOOL bRead;
	
	do
	{
		bRead = file.ReadString(str);
		if(str.Find("error_popup") > -1)
		{
			if(str.Left(2).Compare("--") != 0)
				break;
		}
	}
	while (bRead);

	if(bRead == NULL)
	{
		m_bPopErrorMsg = TRUE;
		return;
	}

	int n = str.Find("=");
	CString strBool;
	strBool = str.Right(str.GetLength()-n-2);

	strBool = strBool.Left(strBool.GetLength()-1);

	if(strBool.CompareNoCase("true") == 0)
	{
		m_bPopErrorMsg = TRUE;
	}
	else
	{
		m_bPopErrorMsg = FALSE;
	}
	file.Close();

}

void CSwatApp::OnSwatPatientSettings()
{

	CPatientDataDlg dlg;
	dlg.DoModal();

}

CString CSwatApp::GUI_IOGetError(int nEnum)
{
	CString strError;
		switch (nEnum) {
		case 0 : 
			strError = "GUIIO_SUCCESS";
			break;
		case 1 : 
			strError = "GUIIO_ERROR";
			break;
		case 2 : 
			strError = "GUIIO_TIMEOUT";
			break;
		case 3 : 
			strError = "GUIIO_OBJECT_ID_NOT_FOUND";
			break;
		case 4 : 
			strError = "GUIIO_INVALID_TRANSACTION_ID";
			break;
		case 5 : 
			strError = "GUIIO_WRONG_MESSAGE_RECEIVED";
			break;
		case 6 : 
			strError = "GUIIO_TABLE_ROW_OR_COL_OUTSIDE_BOUNDS";
			break;
		case 7 : 
			strError = "GUIIO_SCREEN_LOCKED";
			break;

		default	: 
			strError.Format("Invalid Error code: %d",nEnum);		
		};

		return strError;
}
int CSwatApp::GUI_IOConnect()
{
/* 880 commented out
	LOG_MESSAGE(LM_INFO,"Enter %s", __FUNCTION__);


	if(m_bGUIConnected == true) 
	{
		LOG_MESSAGE(LM_INFO,"%s : GUI already connected ", __FUNCTION__);
		return SWAT_SUCCESS;
	}

	if(!PingVent(FALSE))
	{
		LOG_MESSAGE(LM_ERROR,"Failed to ping vent. Try 'arp -d' if vent is connected and turned ON.");
		return SWAT_FAIL;
	}

	GuiIOError err;
	INT nVer;
	err = GuiIOGetDllVersion(&nVer);
	if(err == GUIIO_ERROR) 
	{
		//LogStatus("ERROR : Not able to get guiIOserver.dll version number, GuiIOConnect( )failed ", REPORT_RED);
		LOG_MESSAGE(LM_ERROR,"ERROR : Not able to get guiIOserver.dll version number, %s( )failed", __FUNCTION__);
		m_bRun = 0;
		return SWAT_FAIL;
	}
	if(nVer != m_nGuiIOServerVerexpected)
	{
		LOG_MESSAGE(LM_ERROR,"ERROR : guiIOserver.dll version mismatch. Version expected %i, installed %i.", m_nGuiIOServerVerexpected, nVer);
		LOG_MESSAGE(LM_ERROR,"ERROR : GUI not connected.");
		m_bRun = 0;
		return SWAT_FAIL;
	}


	CString strErr;
	err = GuiIODisconnect();//disconnect if already connected previously.
	LOG_MESSAGE(LM_INFO,"GuiIODisconnect returned %s", GUI_IOGetError(err));

	LogStatus("Connecting to vent GUI...");
	err = GuiIOConnect(m_strGUI_IPAddr, 32274);
	if(err != GUIIO_SUCCESS) 
	{
		CString str;
		str.Format("ERROR : Not able to connect to GUI, GuiIOConnect( )failed, error : %s ", GUI_IOGetError(err));
		LogStatus(str, REPORT_RED);
		LOG_MESSAGE(LM_ERROR,"Exit %s failed", __FUNCTION__);
		return SWAT_FAIL;
	}
	m_bGUIConnected = true;
	
	LOG_MESSAGE(LM_INFO,"GUI Connected", __FUNCTION__);
	LOG_MESSAGE(LM_INFO,"Exit %s pass", __FUNCTION__);
*/ // 880 over
	return SWAT_SUCCESS;
}

int CSwatApp::Over_Write_PD_ValueEx(CString strParam, CString strNewValue)
{
	LOG_MESSAGE(LM_INFO,"Entering CSwatApp::OverRightPDValue() ");

	IPatientDataEl  *pEl;
	IPatientDataSet *pPd=getPatientDataSetInstance();     
	
	
	//for each item in PD, check if it matches the given parameter
	for(int row_cnt=0;pEl=pPd->getPatientDataElement(row_cnt);row_cnt++)
	{
		//if it is same item as parameter, overwrite it
		if(strParam.CompareNoCase(pEl->getName())==0)
		{
			if(pEl->isFloat())
			{
				if(pEl->isValid())
				{
					float f = (float)atof(strNewValue);
					pEl->setFloat(f);
				}
			}
			else if(pEl->isEnum())
			{
				if(pEl->isValid())
				{
					char *t=NULL;
					for(int cnt=0;t=pEl->getEnum(cnt);cnt++)
					{
						if(strNewValue.CompareNoCase(t)==0)
						{
							pEl->setEnum(cnt);
						}
					}
				}
			}
			else if(pEl->isInt())
			{
				if(pEl->isValid())
				{
					int i = atoi(strNewValue);
					pEl->setInt(i);
				}
			}
			
		}
	}
	LOG_MESSAGE(LM_INFO,"Exiting CSwatApp::OverRightPDValue()  ");

	return 1;

}

int CSwatApp::Reset_PD_ValuesEx( )
{
	IPatientDataEl  *pEl;
	IPatientDataSet *pPd=getPatientDataSetInstance();     

	for(int row_cnt=0;pEl=pPd->getPatientDataElement(row_cnt);row_cnt++)
	{
		pEl->unsetValue(); //unset the value sent to GUI
	}

	for(int row_cnt=0;pEl=pPd->getWaveformElement(row_cnt);row_cnt++)
	{
		pEl->unsetValue();
	}

	return 1;
}

CString CSwatApp::Get_Raw_PD_ValueEx(CString strParam)
{
	LOG_MESSAGE(LM_INFO,"Entering CSwatApp::Get_PD_ValueEx() ");

	IPatientDataEl  *pEl;
	IPatientDataSet *pPd=getPatientDataSetInstance();     
	
	CString strOut("ERROR");
	
	//for each item in PD, check if it matches the given parameter
	for(int row_cnt=0;pEl=pPd->getPatientDataElement(row_cnt);row_cnt++)
	{
		//if it is same item as parameter, overwrite it
		if(strParam.CompareNoCase(pEl->getName())==0)
		{
			if(pEl->isFloat())
			{
				if(pEl->isValid())
				{
					strOut.Format("%f", pEl->getFloatValue());
				}
			}
			else if(pEl->isEnum())
			{
				if(pEl->isValid())
				{
					strOut.Format("%s", pEl->getEnumValue());
				}
			}
			else if(pEl->isInt())
			{
				if(pEl->isValid())
				{
					strOut.Format("%d", pEl->getIntValue());
				}
			}	
		}
	}

	LOG_MESSAGE(LM_INFO,"Entering CSwatApp::Get_PD_ValueEx() ");

	return strOut;

}


int CSwatApp::GUI_IODisconnect()
{

	if(m_bGUIConnected == false)
	{
		LOG_MESSAGE(LM_INFO,"%s : GUI already disconnected ", __FUNCTION__);
		return SWAT_SUCCESS;
	}

	GuiIOError err;
	err = GuiIODisconnect();
	if(err == GUIIO_ERROR)
	{
		m_bGUIConnected = false;
		LogStatus("GuiIODisconnect( ) failed...", REPORT_RED);
		LOG_MESSAGE(LM_ERROR,"%s : GuiIODisconnect( ) failed ", __FUNCTION__);
		return SWAT_FAIL;
	}
	else if(err == GUIIO_SUCCESS)
	{
		m_bGUIConnected = false;
		LOG_MESSAGE(LM_INFO, "GUI IO disconnected.");
		return SWAT_SUCCESS;
	}


	return SWAT_SUCCESS;	
}



void CSwatApp::RefreshSWATLogs(CString &strLog)
{

	m_strSWATLogStatus = strLog;

	CMDIFrameWnd *pFrame = (CMDIFrameWnd*)AfxGetApp()->m_pMainWnd;                    
	if(!pFrame) return;
    CMDIChildWnd *pChild = (CMDIChildWnd *) pFrame->GetActiveFrame();                  
	if(!pChild) return;
	m_pView = (CSwatView*)pChild->GetActiveView();    
	if(!m_pView) return;


	m_pView->RefreshSWATLogWindow(strLog);

}

int CSwatApp::GUI_IOKnob(int nClicks)
{
	if(m_bRun == 0) return SWAT_FAIL;

	CheckGUIConnection();
	LOG_MESSAGE(LM_INFO,"Enter : %s", __FUNCTION__);

	GuiIOError err;
	CString strErr;
	err = GuiIOKnob(nClicks);
	Sleep(30);
	if(err == GUIIO_ERROR) 
	{
		strErr.Format("ERROR : Not able to rotate knob, GuiIOKnob( ) failed", REPORT_RED);
		LogStatus(strErr);
		return SWAT_FAIL;
	}

	LOG_MESSAGE(LM_INFO,"Exit : %s", __FUNCTION__);
	
	return SWAT_SUCCESS;
}

int CSwatApp::GUI_IOGetVentSWVersion(GuiIOVentInfoResp *out)
{
	if(m_bRun == 0) return SWAT_FAIL;

	CheckGUIConnection();
	GuiIOError err;
	err = GuiIOGetVentInfo(out);

	static int nErrourCount(0);
	if(err != GUIIO_SUCCESS) 
	{
		strErr.Format("connection error %s, retrying...",GUI_IOGetError(err));
		LogStatus(strErr, REPORT_RED);
		nErrourCount++;

		//if 3 times error, just forget trying
		if(nErrourCount >3) 
		{
			m_bRun = 0;
			return SWAT_FAIL;
		}

		//Try reconnecting
		GUI_IODisconnect();
		Sleep(1000);
		GUI_IOConnect();

		GUI_IOGetVentSWVersion(out);
		nErrourCount = 0;
		return SWAT_FAIL;
	}
	
	return SWAT_SUCCESS;
}


/*
[if Variable == 0 ] simulate normal user touch and release
[if Variable > 0  ] Variable is time in seconds to touch and hold
[if Variable == -1] touch down only, don't release
[if Variable == -2] release only, without touching
[if Variable == -3] double click gesture
*/
int CSwatApp::GUI_IOClickXY(int nX, int nY, float nVariable)
{
	if(m_bRun == 0) return SWAT_FAIL;

	if(nVariable == -3)
	{
		return GUI_IOClickXY_DBL(nX, nY);
	}

	int nSleepTime;
	if(nVariable == 0)
	{// normal touch hard coded for 100 ms
		nSleepTime = 100;
	}
	else if(nVariable > 0)
	{//changing seconds to ms
		nSleepTime = (int)nVariable*1000;
	}


	CheckGUIConnection();
	LOG_MESSAGE(LM_INFO,"Enter CSwatApp::GUI_IOClickXY( )%s", __FUNCTION__);

	GuiIOError err;
	CString str, strErr;

	err = GuiIOMousePos(nX , nY);
	if(err != GUIIO_SUCCESS) 
	{
		strErr.Format("ERROR : Not able to MousePos on %d, %d., GuiIOMousePos( ) failed with error %s",nX,nY, GUI_IOGetError(err) );
		LogStatus(strErr, REPORT_RED);
		return SWAT_FAIL;
	}

//	Sleep(GUI_IO_SLEEP);

	if(nVariable >= -1)
	{
		err = GuiIOLButtonDown(nX , nY);
		if(err != GUIIO_SUCCESS) 
		{
			err = GuiIOLButtonDown(nX , nY);//retry
			if(err != GUIIO_SUCCESS) 
			{
				strErr.Format("ERROR : Not able to L Button down on %d, %d, GuiIOLButtonDown( ) failed TWICE with error %d: %s",nX, nY, err, GUI_IOGetError(err) );
				LogStatus(strErr, REPORT_RED);
				return SWAT_FAIL;
			}
		}
	
	}
	if(nVariable >= 0)
	{
		if(nSleepTime < 1000)
		{
			Sleep(nSleepTime);
		}
		else
		{
			while(nSleepTime>0)
			{
				Sleep(1000);
				nSleepTime = nSleepTime - 1000;
				if(nSleepTime > 0 && nSleepTime < 1000) 
				{
					Sleep(nSleepTime);
					nSleepTime = 0;
				}
        		if(!m_bRun) break;
			}
		}
	}
	if(nVariable != -1)
	{
		err = GuiIOLButtonUp(nX , nY);
		if(err != GUIIO_SUCCESS) 
		{
			err = GuiIOLButtonUp(nX , nY);//retry once
			if(err != GUIIO_SUCCESS) 
			{
				strErr.Format("ERROR : Not able to L Button Up on %d, %d., GuiIOLButtonUp( ) failed TWICE with error %s.",nX, nY, GUI_IOGetError(err) );
				LogStatus(strErr, REPORT_RED);
				return SWAT_FAIL;
			}
		}
	}

	LOG_MESSAGE(LM_INFO,"Exit : %s", __FUNCTION__);

	

	return SWAT_SUCCESS;

}

int CSwatApp::GUI_IOClickXYDirect(int nX, int nY)
{
	if(m_bRun == 0) return SWAT_FAIL;
	CheckGUIConnection();
	LOG_MESSAGE(LM_INFO,"Enter CSwatApp::GUI_IOClickXYDirect( )%s", __FUNCTION__);

	GuiIOError err;
	CString str, strErr;

	err = GuiIOButtonClick(nX , nY);
	if(err != GUIIO_SUCCESS) 
	{
		strErr.Format("ERROR : Not able to click direct on %d, %d., GuiIOButtonClick( ) failed with error %s.",nX, nY, GUI_IOGetError(err) );
		LogStatus(strErr, REPORT_RED);
		return SWAT_FAIL;
	}

	LOG_MESSAGE(LM_INFO,"Exit : %s", __FUNCTION__);
	
	return SWAT_SUCCESS;
}

int CSwatApp::GUI_IOClickXY_DBL(int nX, int nY)
{

	CheckGUIConnection();
	LOG_MESSAGE(LM_INFO,"Enter CSwatApp::GUI_IOClickXY( )%s", __FUNCTION__);

	GuiIOError err;
	CString str, strErr;

	err = GuiIOMousePos(nX , nY);
	if(err == GUIIO_ERROR) 
	{
		strErr.Format("ERROR : Not able to MousePos on %d, %d., GuiIOMousePos( ) failed",nX,nY );
		LogStatus(strErr, REPORT_RED);
		return SWAT_FAIL;
	}

	Sleep(GUI_IO_SLEEP);


	{//first click
		err = GuiIOLButtonDown(nX , nY);
		if(err != GUIIO_SUCCESS) 
		{
			strErr.Format("ERROR : Not able to L Button down on %d, %d., GuiIOLButtonDown( ) failed with error %s",nX, nY, GUI_IOGetError(err) );
			LogStatus(strErr, REPORT_RED);
			return SWAT_FAIL;
		}
		Sleep(100);
		err = GuiIOLButtonUp(nX , nY);
		if(err != GUIIO_SUCCESS) 
		{
			strErr.Format("ERROR : Not able to L Button Up on %d, %d., GuiIOLButtonUp( ) failed with error %s.",nX, nY, GUI_IOGetError(err) );
			LogStatus(strErr, REPORT_RED);
			return SWAT_FAIL;
		}
	}

	Sleep(100);

	{//second click
		err = GuiIOLButtonDown(nX , nY);
		if(err != GUIIO_SUCCESS) 
		{
			strErr.Format("ERROR : Not able to L Button down on %d, %d., GuiIOLButtonDown( ) failed with error %s.",nX, nY, GUI_IOGetError(err)  );
			LogStatus(strErr, REPORT_RED);
			return SWAT_FAIL;
		}
		Sleep(100);
		err = GuiIOLButtonUp(nX , nY);
		if(err != GUIIO_SUCCESS) 
		{
			strErr.Format("ERROR : Not able to L Button Up on %d, %d., GuiIOLButtonUp( ) failed with error %s.",nX, nY, GUI_IOGetError(err) );
			LogStatus(strErr, REPORT_RED);
			return SWAT_FAIL;
		}
	}



	LOG_MESSAGE(LM_INFO,"Exit : %s", __FUNCTION__);

	

	return SWAT_SUCCESS;

}

CString CSwatApp::GUI_IOGetValue(CString strResID)
{
	if(m_bRun == 0) return "stopped";

	CheckGUIConnection();
	GuiIOGetObjectValueResp valueResp;
	GuiIOError err;
	CString strErr;

	if(GUI_IsActivated(strResID)==0)
	{
		//LogStatus(strResID+" Not available on the screen");
		LOG_MESSAGE(LM_ERROR, "ERROR reading %s : IsActivated returned 0",strResID);
		return "ERROR reading "+strResID;
	}

	Sleep(GUI_IO_SLEEP);
	err = GuiIOGetObjectValue(strResID, &valueResp);
	if(err == GUIIO_ERROR) 
	{
		strErr.Format("%s - GUI_IOGetValue->GuiIOGetObjectValue( ) failed ", strResID);
		LogStatus(strErr, REPORT_RED);
		return "ERROR";
	}
	CString str(valueResp.value); //"{I:45},PPEAK,cmH2O,"

	/*log it*/	CString strLog; 
	/*log it*/	strLog.Format("GuiIOGetObjectValue returned: %s", str);
	if(strLog.Find("%")== -1){ //don't log if it has %
	/*log it*/	LOG_MESSAGE(LM_INFO,strLog.GetBuffer());
	/*log it*/	strLog.ReleaseBuffer();}

	ConvertSpecialChar(str);
	CString strValues;
	if(str.Find(",") != -1)
	{
		CString strItalic("false");
		CString strValue("false");
		CString strLabel("false");
		CString strUnits("false");
		
		strValue = str.Left(str.Find(",")); //strValue = "{I:45},"

		str = str.Right(str.GetLength() - str.Find(",")-1);

		strLabel = str.Left(str.Find(","));

		strUnits = str.Right(str.GetLength()-str.Find(",")-1);

		if(strValue.Find("}") != -1 ) // {I:24}
		{	strValue = strValue.Mid((strValue.Find("I:")+2), (strValue.GetLength()-4));
			strItalic = "True";
		}

		strValues.Format("Value=%s, Italic=%s, Label=%s, Unit=%s", strValue, strItalic, strLabel, strUnits); //Value and Italic
	}
	else
	{
		CString strValue = str;
		CString strItalic = "na";
		//only one value recvd, no other info ->Its value of the button, check if italic
		if(strValue.Find("}") != -1 ) // {I:24}
		{	strValue = strValue.Mid((strValue.Find("I:")+2), (strValue.GetLength()-4));
			strItalic = "True";
		}
		else
		{
			strItalic = "False";
		}

		strValues.Format("Value=%s, Italic=%s, Label=%s, Unit=%s", str, strItalic, "na", "na");
	}


	strLog.Format("%s : %s button : %s", __FUNCTION__, strResID, strValues);
	strLog.Replace("%", "'%'");

	LOG_MESSAGE(LM_INFO,strLog.GetBuffer());
	strLog.ReleaseBuffer();

	return strValues; //returns the string value on the button


}

CString CSwatApp::GUI_IOGetText(CString strResID)
{
	if(m_bRun == 0) return "stopped";

	CheckGUIConnection();
	GuiIOGetObjectValueResp valueResp;
	GuiIOError err;
	CString strErr;

	if(GUI_IsActivated(strResID)==0)
	{
		//LogStatus(strResID+" Not available on the screen");
		LOG_MESSAGE(LM_ERROR, "ERROR reading %s : IsActivated returned 0",strResID);
		return "ERROR reading "+strResID;
	}

	Sleep(GUI_IO_SLEEP);
	err = GuiIOGetObjectValue(strResID, &valueResp);
	if(err == GUIIO_ERROR) 
	{
		strErr.Format("%s - GUI_IOGetText->GuiIOGetObjectValue( ) failed ", strResID);
		LogStatus(strErr, REPORT_RED);
		return "ERROR reading "+strResID+" : not on screen";
	}
	CString str(valueResp.value); //"{I:45},PPEAK,cmH2O,"

	/*log it*/	CString strLog; 
	/*log it*/	strLog.Format("GuiIOGetObjectValue returned: %s", str);
	if(strLog.Find("%")== -1){ //don't log if it has %
	/*log it*/	LOG_MESSAGE(LM_INFO,strLog.GetBuffer());
	/*log it*/	str.ReleaseBuffer();}

	ConvertSpecialChar(str);
		
	return str; //returns the string value on the button

}

CString CSwatApp::GUI_IOGetRowText(CString strResID, int nRow)
{
	if(m_bRun == 0) return "stopped";
	CheckGUIConnection();
	GuiIOGetObjectValueResp Response;

	GuiIOError err;
	CString strErr;
	
	if(GUI_IsActivated(strResID)==0)
	{
		//LogStatus(strResID+" Not available on the screen");
		LOG_MESSAGE(LM_ERROR, "ERROR reading %s : IsActivated returned 0",strResID);
		return "ERROR reading "+strResID;
	}

	Sleep(GUI_IO_SLEEP);
	err = GuiIOGetObjectValue (strResID, &Response);
	if(err == GUIIO_ERROR) 
	{
		strErr.Format("%s - GUI_IOGetText->GuiIOGetObjectValue( ) failed ", strResID);
		LOG_MESSAGE(LM_ERROR, "ERROR reading %s ",strResID);
		return "ERROR reading "+ strResID;
	}

	if(Response.objectId != strResID) 
	{
		LOG_MESSAGE(LM_ERROR, "ERROR ID mismatch for %s",strResID);
		return "ERROR reading "+strResID;
	}

	CString strRC(Response.value);
	int n = strRC.Find(',',0);
	int m = strRC.GetLength()-n-1;
	CString strC = strRC.Right(m); // 5,

	strC  = strC.Left(strC.GetLength()-1);

	int nColCount = atoi(strC);

	//CString strlog;
	//strlog.Format("Table %s has %d columns.",strResID, nColCount);
	//LogStatus(strlog);
	LOG_MESSAGE(LM_INFO, "Table %s has %d columns.",strResID, nColCount);

	if(nColCount>20){
		LOG_MESSAGE(LM_ERROR, "Table %s has more than 20 columns. %d found.",strResID, nColCount);
		return "ERROR reading "+strResID;
	}

	LOG_MESSAGE(LM_INFO, "Table %s has %d columns.",strResID, nColCount);

	GuiIOGetObjectValueResp Values[20];

	//strErr.Format("Calling GuiIOGetTableRowObjectValue(%s, %d, %d, val[20])", strResID, nRow, nColCount);
	//LogStatus(strErr);
	LOG_MESSAGE(LM_INFO, "Calling GuiIOGetTableRowObjectValue(%s, %d, %d, val[20])", strResID, nRow, nColCount);

	if(strResID== "ID_PATIENT_DATA_LOG_TABLE") 
	{
		nColCount = 14;
	}

	err = GuiIOGetTableRowObjectValue(strResID, nRow, nColCount, Values);
	if(err == GUIIO_ERROR) 
	{
		strErr.Format("GuiIOGetTableRowObjectValue(%s, %d, %d, val[20]) failed ", strResID, nRow, nColCount);
		LogStatus(strErr);
		LOG_MESSAGE(LM_ERROR, "ERROR reading %s ",strResID);
		return "ERROR reading "+ strResID;
	}

//	out[] ?a totalRowCols array of responses. The array must be sized to hold totalRowCols number of responses. Array location 0 is column 0 object value, location 1 is column 1, etc?The response structure contains the exact row/col as well.
//	GuiIOGetTableRowObjectValue(const CHAR* objectId, 
//		int row, int totalRowCols, GuiIOGetObjectValueResp out[]);


	CString strFinal, strDelimiter;
	for(int n =0; n< nColCount; n++)
	{
		if(m_bRun == 0) return "stopped";

		CString strTemp(Values[n].value);
		ConvertSpecialChar(strTemp);

		CString strLog; 	strLog.Format("GuiIOGetTableObjectValue returned: %s", strTemp);
		
		if(strLog.Find("%")== -1){ //don't log if it has %
			LOG_MESSAGE(LM_INFO,strLog.GetBuffer());
			strTemp.ReleaseBuffer();
		}

		if(!strFinal.IsEmpty())
			strDelimiter = "||";

		strFinal = strFinal + strDelimiter + strTemp;
	}
		
	return strFinal; //return the consolidated string from the table row

}



CString CSwatApp::GUI_IOGetCellText(CString strResID, int nRow, int nCol)
{
	if(m_bRun == 0) return "stopped";
	CheckGUIConnection();

	GuiIOError err;
	CString strErr;
	
	if(GUI_IsActivated(strResID)==0)
	{
		//LogStatus(strResID+" Not available on the screen");
		LOG_MESSAGE(LM_ERROR, "ERROR reading %s : IsActivated returned 0",strResID);
		return "ERROR reading "+strResID;
	}

/*	err = GuiIOGetObjectValue(strResID, &valueResp);
	if(err == GUIIO_ERROR) 
	{
		strErr.Format("%s - GUI_IOGetText->GuiIOGetObjectValue( ) failed ", strResID);
		LOG_MESSAGE(LM_ERROR, "ERROR reading %s ",strResID);
		return "ERROR reading "+ strResID;
	}

	if(valueResp.objectId != strResID) 
	{
		LOG_MESSAGE(LM_ERROR, "ERROR ID mismatch for %s",strResID);
		return "ERROR reading "+strResID;
	}
*/	

	GuiIOGetObjectValueResp Value;

	Sleep(GUI_IO_SLEEP);

	err = GuiIOGetTableCellObjectValue(strResID, nRow, nCol, &Value);
	if(err == GUIIO_ERROR) 
	{
		strErr.Format("%s - GUI_IOGetText->GuiIOGetObjectValue( ) failed ", strResID);
		LOG_MESSAGE(LM_ERROR, "ERROR reading %s, check if row/col is valid(%d,%d) ",strResID, nRow,nCol);
		return "ERROR reading "+ strResID;
	}
	

	CString strTemp(Value.value);

	ConvertSpecialChar(strTemp);

	//log it
	CString strLog; strLog.Format("GuiIOGetTableObjectValue returned: %s", strTemp);
	if(strLog.Find("%")== -1){ //don't log if it has %
		LOG_MESSAGE(LM_INFO,strLog.GetBuffer());
		strTemp.ReleaseBuffer();}

			
	return strTemp; //return the consolidated string from the table row

}

CString CSwatApp::GUI_IOGetXYWH(CString strResID)
{
	if(m_bRun == 0) return "stopped";

	CheckGUIConnection();
	GuiIOQueryXYResp queryXYResp;
	GuiIOQueryWHResp queryWHResp;
	GuiIOError err;
	CString strErr;

	err = GuiIOQueryXY(strResID, &queryXYResp);
	if(err == GUIIO_ERROR) 
	{
		strErr.Format("ERROR : Not able to query X,Y for %s, GUI_IOGetXYWH()->GuiIOQueryXY() failed", strResID);
		LogStatus(strErr);
		return "ERROR";
	}

	Sleep(GUI_IO_SLEEP);
	err = GuiIOQueryWH(strResID, &queryWHResp);
	if(err == GUIIO_ERROR) 
	{
		strErr.Format("ERROR : Not able to query H, W for %s, GuiIOQueryWH( ) failed", strResID);
		LogStatus(strErr);
		return "ERROR";
	}

	CString str;
	str.Format("X=%d, Y=%d, H=%d, W=%d", queryXYResp.x, queryXYResp.y, queryWHResp.h, queryWHResp.w);


	/*LOG_MESSAGE(LM_INFO, str.GetBuffer());
	str.ReleaseBuffer();*/
	
	return str; //return the string with co-ordinates

}





void CAboutDlg::OnBnClickedOk()
{
	OnOK();
}



void CSwatApp::OnSwatCleanDatabase()
{

	int nMinutes;

	CDeleteDataDlg dlg;
	if(dlg.DoModal() == IDCANCEL)
	{
		return;
	}
	nMinutes = dlg.m_nTimeMinutes;
	LOG_MESSAGE(LM_INFO,"Enter CSwatApp::OnSwatCleandatabase keep %d minutes ", dlg.m_nTimeMinutes);
	CWaitCursor wait;

	g_DataCapture->clean_database(nMinutes);
	LOG_MESSAGE(LM_INFO,"Exit CSwatApp::OnSwatCleandatabase kept %d minutes ", 15);

}
int CSwatApp::BDPushSettingsToHost(CString strSettings, CString strHost, CString strPort)
{
	
	char *ch[100];	
	CString str;  
	int n(0);

	CString resToken; 
	int curPos(0);
	
	str = strSettings;
	resToken= str.Tokenize(" ",curPos);
	while (resToken != _T(""))
	{
		ch[n++] = _strdup(resToken.GetBuffer());
		resToken = str.Tokenize(" ",curPos);
	}
	ch[n] = NULL;


//Call PCApp dll 

	char chHost[100];
	char chPort[100];

	strcpy(chHost, strHost.GetBuffer());
	strcpy(chPort, strPort.GetBuffer());

	int nReturn = m_pBDSendSettings->BDSendSettings(ch, chHost, chPort);
	
	for(int i =0; ch[i] != NULL; i++)
	{
		free(ch[i]);
	}
	if(nReturn == -1)
	{
		LOG_MESSAGE(LM_CRITICAL, "Sending settings failed");
		return -1;
	}
	return 0;

}


void CSwatApp::OnGuiConnectGUI()
{
	CWaitCursor wait;
	if(GUI_IOConnect() == SWAT_SUCCESS)
	{
		LogStatus("GUI connected");
	}

}
void CSwatApp::OnGuiDisconnectGUI()
{
	CWaitCursor wait;
	if(GUI_IODisconnect() == SWAT_SUCCESS)
	{
		LogStatus("GUI disconnected");
	}

}


bool CSwatApp::Enable_DB_Logging()
{
	return g_DataCapture->enable_logging();
}

bool CSwatApp::Disable_DB_Logging()
{
	return g_DataCapture->disable_logging();
}


int CSwatApp::GUI_Bezel_Key_Down(CString &strResID)
{
	GuiIOError err;
	err = GuiIOBezelKeyPress(atoi(strResID), true);
	if(err == GUIIO_ERROR) 
	{
		strErr.Format("GuiIOBezelKeyPress(%s) failed ", strResID);
		LOG_MESSAGE(LM_ERROR, "CSwatApp::GuiIOBezelKeyPress(%s, 1) failed ", strResID);
		return 0;
	}
	return 1;
}

int CSwatApp::GUI_Bezel_Key_Up(CString &strResID)
{
	err = GuiIOBezelKeyPress(atoi(strResID), false);
	if(err == GUIIO_ERROR) 
	{
		strErr.Format("GuiIOBezelKeyPress(%s) failed ", strResID);
		LOG_MESSAGE(LM_ERROR, "CSwatApp::GuiIOBezelKeyPress(%s, 0) failed ", strResID);
		return 0;
	}
	return 1;
}

int CSwatApp::GUI_Bezel_Key_Click(CString strResID, int nVariable)
{
	/*CheckGUIConnection();
	GUI_Bezel_Key_Down(strResID);
	Sleep(nVariable);
	return GUI_Bezel_Key_Up(strResID);*/

	//to support insp pause on bezel
	if(nVariable == 0) nVariable = 100;

	CheckGUIConnection();
	
	if(nVariable > 0)
	{
		GUI_Bezel_Key_Down(strResID);
		Sleep(nVariable);
		return GUI_Bezel_Key_Up(strResID);
	}
	else if(nVariable == -1)
	{
		return GUI_Bezel_Key_Down(strResID);
	}
	else if(nVariable == -2)
	{
		return GUI_Bezel_Key_Up(strResID);
	}

	LOG_MESSAGE(LM_ERROR, "CSwatApp::GUI_Bezel_Key_Click(%s, %d) incorrect variable", strResID, nVariable);
	return 0;
}




int CSwatApp::GUI_IsBlinking(CString strResID)
{
	if(m_bRun == 0) return SWAT_FAIL;

	CheckGUIConnection();
	GuiIOGetPropertiesResp propResp;
	GuiIOError err;
	CString strErr;
	Sleep(GUI_IO_SLEEP);
	err = GuiIOGetProperties(strResID, &propResp);
	if(err == GUIIO_ERROR) 
	{
		strErr.Format("GUI_IsBlinking->GuiIOGetProperties(%s) failed ", strResID);
		LOG_MESSAGE(LM_ERROR, "CSwatApp::GUI_IsBlinking(%s) failed ", strResID);
		return 0;
	}
	else if (GUIIO_OBJECT_ID_NOT_FOUND == err)
	{
		LOG_MESSAGE(LM_ERROR, "GUI_IsBlinking() %s OBJECT ID Not found, returning 0", strResID);
		return 0;
	}
	else 
	{
		LOG_MESSAGE(LM_INFO, "%s blink = %i", strResID, propResp.blink);
		return propResp.blink;
	}

}


int CSwatApp::GUI_IsAdornment(CString strResID)
{
	if(m_bRun == 0) return SWAT_FAIL;

	CheckGUIConnection();
	GuiIOGetPropertiesResp propResp;
	GuiIOError err;
	CString strErr;
	Sleep(GUI_IO_SLEEP);
	err = GuiIOGetProperties(strResID, &propResp);
	if(err == GUIIO_ERROR) 
	{
		strErr.Format("GUI_IsAdornment->GuiIOGetProperties(%s) failed ", strResID);
		LOG_MESSAGE(LM_ERROR, "CSwatApp::GUI_IsAdornment(%s) failed ", strResID);
		return 0;
	}
	else if (GUIIO_OBJECT_ID_NOT_FOUND == err)
	{
		LOG_MESSAGE(LM_ERROR, "GUI_IsBlinking() %s OBJECT ID Not found, returning 0", strResID);
		return 0;
	}
	else 
	{
		LOG_MESSAGE(LM_INFO, "%s adornment = %i", strResID, propResp.adornment);
		return propResp.adornment;
	}

}
void CSwatApp::ResetImageCounter( )
{
	m_nImageCounter = 0;
}
CString CSwatApp::FormatImageName(CString strFileName)
{
	
	m_nImageCounter ++;

	//strFileName.Format("%s_%d", strFileName, m_nImageCounter);

	//fix the file name for extension
	if(strFileName.Find(".png") ==-1)       
	{
		strFileName = strFileName+".png";
	}

	strFileName.Replace('\\','_');
	strFileName.Replace(' ','_');

	return strFileName;

}



int CSwatApp::GUI_CaptureScreen(CString strFileName)
{
	LOG_MESSAGE(LM_INFO,"%s Entering", __FUNCTION__);
	GuiIOError err;
	CheckGUIConnection();

	//don't take image capture if stopped.
	if(m_bRun == 0) return -1;

	//error if the vent buffer is already full
	if(GUI_CaptureScreenStatus() == 4)
	{
		LogStatus("Vent image capture buffer is full. Aborting image capture for now.");
		return -1;
	}


	CString strScriptPathName = m_ScriptResults.m_strFileName;
	CString strScriptName = strScriptPathName.Right(strScriptPathName.GetLength()-strScriptPathName.Find('\\')-1);

	strFileName = FormatImageName(strFileName);

	//path on internal USB to capture, used now
	CString strIntUSBPath;
	strIntUSBPath.Format("%s/%s", SWAT_USB_ROOT, strFileName);

	//ftp is just a file name
	CString strFTPPath = strFileName;
	
	//path on the local drive to save when session complete
	strScriptPathName = m_ScriptResults.m_strFileName;
	CString strDstPath;
	strDstPath = strScriptPathName.Left(strScriptPathName.ReverseFind('\\'))+'\\'+strFileName;
	strDstPath.Format("%s\\%s",strScriptPathName.Left(strScriptPathName.ReverseFind('\\')), strFileName);

	//for sending data to transfer over FTP, to use later in a different thread
	ImagesCaptured imageCaptured;
	imageCaptured.m_FileName = strFTPPath;
	imageCaptured.m_strDestinationFilePath = strDstPath;
	imageCaptured.m_bCopied2PC = false;

	//now capture image
	err = GuiIOScreenCapture(strIntUSBPath);
	LogStatus("Calling vent screen capture with: "+strIntUSBPath);

	if(err != GUIIO_SUCCESS) 
	{
		m_ScriptResults.m_nErrors++;
		strErr.Format("GuiIOScreenCapture(%s) failed, error code %d ", strIntUSBPath, err);
		LogStatus(strErr, REPORT_RED);
		LOG_MESSAGE(LM_INFO, "GuiIOScreenCapture(%s) failed , error code %d", strIntUSBPath, err);
		return -1;
	}

	//put the HTML tag in the report
	if(m_bTwoImagesForComparison)
		LogStatus(strFileName,REPORT_IMAGE_LANGUAGE);
	else
		LogStatus(strFileName,REPORT_IMAGE);



	//add to the list for Thread_CopyImageOverFTP to copy from vent to test folder

	POSITION pos = m_listCapturedImages.GetHeadPosition();
	for (int i=0;i < m_listCapturedImages.GetCount();i++)
	{
	   ImagesCaptured tmp;
	   tmp = m_listCapturedImages.GetNext(pos);
	   if(tmp.m_FileName == strFTPPath)
	   {
		   CString str; str.Format("File name repeated in script: %s, overwriting older image file...", strFileName);
		   LogStatus(str, REPORT_RED);
	   }
	}

	m_listCapturedImages.AddTail(imageCaptured);

	LOG_MESSAGE(LM_INFO,"%s Exiting", __FUNCTION__);
	return 0;
}

int CSwatApp::GUI_CaptureScreenStatus( )
{
	//LOG_MESSAGE(LM_INFO,"%s Entering", __FUNCTION__);

	CheckGUIConnection();

	GuiIOScreenCaptureBufferInfoResp out;
	GuiIOGetScreenCaptureBufferInfo(&out);

	LOG_MESSAGE(LM_INFO,"%s Exiting, video buffer at %d", __FUNCTION__, out.bufferSize);
	return out.bufferSize;
}

UINT CSwatApp::CopyImageFromVent(CString strFTPFilename, CString strHardDrive)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	//give some time for vent to start storing image to USB
	Sleep(1000);
			
	CInternetSession internetSession;
	CFtpConnection *ftp;
	CString strDir;
	CString str;
	TRY
	{
		ftp = internetSession.GetFtpConnection("192.168.0.2", 0, 0, 3021);
		if(ftp->GetCurrentDirectory(strDir)==0)
		{
			DWORD dw = GetLastError();
			str.Format("Copying %s to %s failed in GetCurrentDirectory(). Error code from GetLastError = %d", strFTPFilename, strHardDrive, dw);
			pApp->LogStatus(str, REPORT_WARNING);

			ftp->Close();
			delete ftp;
			internetSession.Close();
			return 0;

		}
		if(!ftp->SetCurrentDirectory(strDir + "SWAT"))
		{
			DWORD dw;
			char strErr[800];
			DWORD len;
			InternetGetLastResponseInfo(&dw,  strErr,  &len);
			pApp->LogStatus(strErr);

			dw = GetLastError();
			str.Format("Copying %s to %s failed in SetCurrentDirectory(). Error code from GetLastError = %d", strFTPFilename, strHardDrive, dw);
			pApp->LogStatus(str, REPORT_WARNING);
			
			ftp->Close();
			delete ftp;
			internetSession.Close();
			return 0;
		}

		// wait for the file to be completely saved on the vent
		CFtpFileFind finder(ftp);
		if (finder.FindFile(strFTPFilename) == FALSE)    
		{ 
			str.Format("File not found on ftp server: %s ", strFTPFilename);
			pApp->LogStatus(str, REPORT_WARNING);
			
			ftp->Close();
			delete ftp;
			internetSession.Close();
			return 0;

		}	        
		//We have to use a FindNextFile to get info on first file, why ?. Becuase FindNextFile doesn't get the    
		//next file on its first invocation. It gets the first file! Makes sense doesn't it? Pure Genius...    
		finder.FindNextFile();   
		CString source = strFTPFilename;

		//Empty the CString    
		CString temp_ftp_name = finder.GetFilePath();

		//Get the actual file name in case wildcards were used    
		if (temp_ftp_name.IsEmpty()) temp_ftp_name = source;

		//Make sure we got something. If not use source    
		ULONGLONG file_size_start(0), file_size_stop(1);   
		int nSeconds = 0;//for timeout 60 seconds

		while(file_size_start != file_size_stop)
		{
			finder.FindFile(strFTPFilename);
			finder.FindNextFile();   
			finder.GetFilePath();

			file_size_start = finder.GetLength();
			//str.Format("size of %s = %d",strFTPFilename, file_size_start);
			//pApp->LogStatus(str, REPORT_RED);

			Sleep(50);//changed from 8K to 50, assuming file is already copied since buffer is 0
			
			finder.FindFile(strFTPFilename);
			finder.FindNextFile();   
			finder.GetFilePath();
			
			file_size_stop = finder.GetLength();
			nSeconds = nSeconds+10;

			if(nSeconds>60) 
			{
				CString str;
				str.Format("Image copy Time Out occured for %s", strFTPFilename);
				pApp->LogStatus(str, REPORT_WARNING);
				break;
			}
			
		}

		finder.Close();
		Sleep(500);

	
		if(!ftp->GetFile(strFTPFilename, strHardDrive,0, FILE_ATTRIBUTE_NORMAL, FTP_TRANSFER_TYPE_BINARY|INTERNET_FLAG_RELOAD ))
		{
			DWORD dw = GetLastError();
			str.Format("GetFile( %s) failed. Error code from GetLastError = %d", strFTPFilename, dw);
			pApp->LogStatus(str, REPORT_RED);
			ftp->Close();
			delete ftp;
			internetSession.Close();
			return 0;
		}
		else
		{
			str.Format("Copied %s to report", strFTPFilename);
			pApp->LogStatus(str);
		}

		//remove file from the ftp server
		if(!ftp->Remove(strFTPFilename))
		{
			//try one more time
			Sleep(5000);
			if(!ftp->GetFile(strFTPFilename, strHardDrive,0, FILE_ATTRIBUTE_NORMAL, FTP_TRANSFER_TYPE_BINARY|INTERNET_FLAG_RELOAD ))
			{
				DWORD dw;
				char strErr[800];
				DWORD len;
				InternetGetLastResponseInfo(&dw,  strErr,  &len);
				
				dw = GetLastError();
				str.Format("Cppying %s from vent FTP server failed. Error : %s", strFTPFilename, strErr);
				pApp->LogStatus(str, REPORT_RED);
				if(!ftp->Remove(strFTPFilename))
				{
					str.Format("Deleting %s from vent FTP server failed. Error : %s", strFTPFilename, strErr);
					pApp->LogStatus(str, REPORT_RED);
				}
			}
	
			ftp->Close();
			delete ftp;
			internetSession.Close();
			return 0;
		}

	}
	CATCH(CInternetException, ex)
	{
		str.Format("ERROR: CInternetException caught in copy over FTP for %s :", strFTPFilename);
		pApp->LogStatus(str, REPORT_RED);
		char szCause[255];
		ex->GetErrorMessage(szCause, 255);
		pApp->LogStatus(CString(szCause), REPORT_ERROR);
		
		return 0;
	}
	END_CATCH

	LOG_MESSAGE(LM_INFO,"Copied to SWAT report : %s", strFTPFilename);


	ftp->Close();
	delete ftp;
	internetSession.Close();
	return 1;

}


void CSwatApp::CopyAllCapturedImages()
{
	//if nothing captured, just return
	if(m_listCapturedImages.GetCount()== 0)
	{
		return;
	}

	int nBuffer = GUI_CaptureScreenStatus();
	while(nBuffer > 0)
	{
		CString str;
		str.Format("Waiting for video buffer to go down to zero, might take upto %d minutes", nBuffer);
		LogStatus(str);
		Sleep(4000);
		nBuffer = GUI_CaptureScreenStatus();

		if(nBuffer>10) 
		{
			LogStatus("Error reading video buffer, will start copying in 30 seconds.");
			Sleep(30000);
			break;
		}
		
	}


	AfxBeginThread((AFX_THREADPROC)Thread_CopyImageOverFTP, (LPVOID)this);
}


//this thread copies all iamges at the end of the script
UINT CSwatApp::Thread_CopyImageOverFTP(LPVOID lp)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();

	while(pApp->m_listCapturedImages.GetCount()!=0)
	{
		POSITION pos = pApp->m_listCapturedImages.GetHeadPosition();
		ImagesCaptured imageCaptured = pApp->m_listCapturedImages.GetAt(pos);
		int n = CopyImageFromVent(imageCaptured.m_FileName, imageCaptured.m_strDestinationFilePath);
	
		pApp->m_listCapturedImages.RemoveAt(pos);//remove form list
	}
	return 0;

}

int CSwatApp::PlayWaveFile(CString strFileName)
{
	CString strFullPath = m_strTestScriptPath+"\\00_libs\\Sounds\\"+strFileName;
	
	if(GetFileAttributes(strFullPath)== INVALID_FILE_ATTRIBUTES)
	{
		PlaySound("C:\\SWAT\\swat.WAV", NULL, SND_ASYNC);
	}
	else
	{
		PlaySound(strFullPath, NULL, SND_ASYNC);
	}

	return 0;
}


int CSwatApp::GUI_IsActivated(CString strResID)
{
	if(m_bRun == 0) return SWAT_FAIL;
	static int nButtonErrorCount(0); //to take care of getting invalid button state sometimes because of timing issue

	CheckGUIConnection();
	GuiIOGetPropertiesResp propResp;
	GuiIOError err;
	CString strErr;

	Sleep(GUI_IO_SLEEP);
	err = GuiIOGetProperties(strResID, &propResp);
	if(err == GUIIO_ERROR) 
	{
		m_ScriptResults.m_nErrors++;
		strErr.Format("GUI_IsActivated->GuiIOGetProperties(%s) failed ", strResID);
		LOG_MESSAGE(LM_ERROR, "CSwatApp::GuiIOGetProperties(%s) failed , error code %i > GUIIO_ERROR", strResID, err);
		nButtonErrorCount = 0;
		return 0;
	}
	else if (GUIIO_OBJECT_ID_NOT_FOUND == err)
	{
		CString strErr;
		strErr.Format("%s OBJECT ID  NOT  found, returning 0", strResID);
		LOG_MESSAGE(LM_INFO, strErr.GetBuffer());
		strErr.ReleaseBuffer();

		nButtonErrorCount = 0;
		return 0;
	}
	else if(!propResp.activated)
	{
		//NotActivated
		LOG_MESSAGE(LM_INFO, "%s Not Activated - activated %i, enabled %i, viewable %i", strResID, propResp.activated , propResp.enabled , propResp.viewable);
		nButtonErrorCount = 0;
		return 0;
	}

	else if (propResp.activated && propResp.enabled && !propResp.viewable && !propResp.canFocus && (propResp.state == -1)) 
	{	//for the text on the screen, which is not button hence can not focus and state is -1
		LOG_MESSAGE(LM_INFO, "%s - activated, enabled, !viewable, !canFocus, state = -1 -> returning 1", strResID);
		nButtonErrorCount = 0;
		return 1;
	}
	else if (propResp.activated && propResp.enabled && !propResp.viewable && !propResp.canFocus && (propResp.state != -1)) 
	{	//buttons like accept/cancel, A/C SPONT etc.
		LOG_MESSAGE(LM_INFO, "%s - activated, enabled, !viewable, !canFocus, state != -1 -> returning 2", strResID);
		nButtonErrorCount = 0;
		return 2;
	}
	else if (propResp.activated && propResp.enabled && !propResp.viewable  && propResp.canFocus) 
	{	//Control is visible and ready for user interaction. Note, however, this does not mean the control is 
		//accessible to the user. A popup dialog could still be obscuring the control preventing the user from 
		//interacting with the control.
		LOG_MESSAGE(LM_INFO, "%s - activated, enabled, !viewable, canFocus -> returning 2", strResID);
		nButtonErrorCount = 0;
		return 2;
	}
	else if (propResp.activated && !propResp.enabled && !propResp.viewable) 
	{	//Control may be semi-transparent (but visible) but can not be changed by the user, or it may be completely 
		//invisible depending on the control type. A button graphic is grayed out (but visible) and all button text 
		//is completely invisible. A simple label control, on the other hand, is completely invisible. 
		LOG_MESSAGE(LM_INFO, "%s - activated, !enabled, !viewable -> returning 0", strResID);
		nButtonErrorCount = 0;
		return 0;
	}
	else if (propResp.activated && !propResp.enabled && propResp.viewable) 
	{	//Control may be semi-transparent (but visible) but can not be changed by the user, or it may be completely 
		//invisible depending on the control type. A button graphic is grayed out (but visible) and all button text 
		//is completely invisible. A simple label control, on the other hand, is completely invisible. 
		LOG_MESSAGE(LM_INFO, "%s - activated, !enabled, viewable -> returning 1", strResID);
		nButtonErrorCount = 0;
		return 1;
	}
	else if (propResp.enabled && propResp.viewable) 
	{	//Invalid state; should never occur. 
		LOG_MESSAGE(LM_INFO, "%s - enabled AND viewable; should never happen, retry %d", strResID, nButtonErrorCount);
		nButtonErrorCount++;

		CString str; str.Format("ERROR...enabled AND viewable button indicated(%s), retrying in 1 second...", strResID);
		LogStatus(str, 20);
		{ 
			GUI_IODisconnect();
			Sleep(1000);
			GUI_IOConnect();
		}
		if(nButtonErrorCount > 2)
		{
			LOG_MESSAGE(LM_ERROR, "%s - enabled and viewable; should never happen -> Tried 3 times. Returning 0", strResID);
			return SWAT_FAIL;
		}
		else
		{
			return GUI_IsActivated(strResID);
		}
	}
	else
	{
		CString strErr;
		strErr.Format("Error in %s button state: activated %i, enabled %i, viewable %i", strResID, propResp.activated , propResp.enabled , propResp.viewable);
		LogStatus(strErr);
		LOG_MESSAGE(LM_ERROR, "Error in %s button state",strResID);
		nButtonErrorCount = 0;
		return 0;
	}


	return SWAT_FAIL;
}





int CSwatApp::GUI_SimulateBDAlarm(CString strAlarmId, CString strCondAugOptions, CString strDepenAugOptions)
{
	if(m_bRun == 0) return SWAT_FAIL;
	CheckGUIConnection();
	Sleep(GUI_IO_SLEEP);

	//ensure there is no space in any param
	strAlarmId.Replace(" ", "");
	strCondAugOptions.Replace(" ", "");
	strDepenAugOptions.Replace(" ", "");

	GuiIOError err;
	CString str, strErr;

	err = GuiIOSimulateBDAlarm(strAlarmId, strCondAugOptions, strDepenAugOptions);
	if(err != GUIIO_SUCCESS) 
	{
		strErr.Format("ERROR : GUI_SimulateBDAlarm() returned %d", err);
		LogStatus(strErr, REPORT_RED);
		return SWAT_FAIL;
	}
	return SWAT_SUCCESS;
}




int CSwatApp::GUI_IsSelected(CString strResID)
{
	if(m_bRun == 0) return SWAT_FAIL;

	CheckGUIConnection();
	GuiIOGetPropertiesResp propResp;
	GuiIOError err;
	CString strErr;

	Sleep(GUI_IO_SLEEP);
	err = GuiIOGetProperties(strResID, &propResp);
	if(err == GUIIO_ERROR) 
	{
		strErr.Format("GUI_IsActivated->GuiIOGetProperties(%s) failed ", strResID);
		LOG_MESSAGE(LM_ERROR, "CSwatApp::GuiIOGetProperties(%s) failed ", err);
		return 0;
	}
	if (propResp.state == 1)
	{
		//is selected button
		LOG_MESSAGE(LM_INFO, "%s - GuiIOGetPropertiesResp.state = 1", strResID);
		return 1;
	}
	else 
		return 0;

}

void CSwatApp::OnUpdateGuiConnectGUI(CCmdUI *pCmdUI)
{
	pCmdUI->Enable(!m_bGUIConnected);
}

void CSwatApp::OnUpdateGuiDisconnectGUI(CCmdUI *pCmdUI)
{
	pCmdUI->Enable(m_bGUIConnected);
}

void CSwatApp::CheckGUIConnection()
{
	if(!m_bGUIConnected)
	{
		if(m_bRun == false) //user has clicked 'stop' don't worry about connecting
			return;

		if(SWAT_FAIL == GUI_IOConnect())
		{
			//Messagebox blocks further execution of scripts, logStatus doesn't
			//AfxMessageBox("Failed to connect GUI. Please check connection and try again.\nReboot the vent if connections are ok.");
			
			LogStatus("Failed to connect GUI. Please check connection and try again.", REPORT_ERROR);
			LogStatus("Reboot the vent if connections are ok.", REPORT_ERROR);

			m_bRun = 0;//stop script
		}
	}
}

int CSwatApp::GetFocusButtonID( char *ch)
{
	GuiIOGetFocusResp out;
	GuiIOError err;
	err = GuiIOGetFocus(&out);

	if(err == GUIIO_ERROR)
	{
		LOG_MESSAGE(LM_ERROR, "GuiIOGetFocus() returned GuiIO_ERROR");
		return 0;
	}
	else
	{
		strcpy(ch, out.objectId);
		return 1;
	}

}

void CSwatApp::ConvertSpecialChar(CString &str)
{
/*
#define _VDOT                       L"\uE001"
#define _SQUARE_WAVE                L"\uE002"
#define _RAMP_WAVE                  L"\uE003"
#define _RISE_TIME                  L"\uE005"
#define _UP_ARROW                   L"\uE010"
#define _UP_ARROW_LIMIT             L"\uE011"
#define _DOWN_ARROW                 L"\uE012"
#define _DOWN_ARROW_LIMIT           L"\uE013"*/

	int nLen = str.GetLength();
	CString toAppend;
	CString newStr;
	for(int n(0); n < nLen; n++)
	{
		switch (str[n]) {
		case '\x1' : 
			toAppend = "VDOT";
			break;
		case '\x2' : 
			toAppend = "SQUARE";
			break;
		case '\x3' : 
			toAppend = "RAMP";
			break;
		case '\x5' : 
			toAppend = "RISETIME";
			break;
		case '\x10' : 
			toAppend = "UA";
			break;
		case '\x11' : 
			toAppend = "UA_LIMIT";
			break;
		case '\x12' : 
			toAppend = "DA";
			break;
		case '\x13' : 
			toAppend = "DA_LIMIT";
			break;

		case '\x1E' :  //1E = 30 in Dec 
			toAppend = ">=";
			break;

		case '\x1F' :   //1F = 31 in Dec
			toAppend = "<=";
			break;

		default	: 
			toAppend = str[n];		
		}
		newStr = newStr+toAppend;
	}

	str = newStr;
}



UINT CSwatApp::Thread_SWDownloader(LPVOID lp)
{
#ifdef ENABLE_DOWNLOADER
	CSwatApp* pApp = (CSwatApp*)lp;

	if(pApp->m_bVentDownloaderInProgress) return 0; //don't start again if already running

	pApp->m_bVentDownloaderInProgress = true;


	::VikingConsoleDriver_reset(pApp->m_pVikingConsoleDriver);

	::VikingConsoleDriver_startDownloadServer(pApp->m_pVikingConsoleDriver);

	::VikingConsoleDriver_enableTrigger(pApp->m_pVikingConsoleDriver);

	pApp->LogStatus("Download Started v2");


	while(1)
	{
		if(!pApp->m_bVentDownloaderRun)
		{
			break;
		}
		::VikingConsoleToken *token;

		if ((token = ::VikingConsoleDriver_getNextToken(pApp->m_pVikingConsoleDriver )) !=  0)
        {
            int nElements = VikingConsoleDriver_getLength(token);


            if (nElements == 0) continue;

			std::string str = VikingConsoleDriver_getValue(token, 0);

            try
            {
                if (strcmp(VikingConsoleDriver_getValue(token,0),"REMOTEFILEIO")==0)
                {
                    if ((strcmp(VikingConsoleDriver_getValue(token,2),"ERROR")==0) && nElements >3)
                    {
                      //  this.BeginInvoke(new SimpleDelegate2(LogMessage), token.getValue(3));
                    }
                }

                else if (strcmp(VikingConsoleDriver_getValue(token,0),"GUI")==0)
                {
                    if ((strcmp(VikingConsoleDriver_getValue(token,1),"SET")==0) && nElements > 3)
                    {
                        if (strcmp(VikingConsoleDriver_getValue(token,2),"PROGRESS")==0)
                        {
                            //this.downloadProgressGUI.UpdateProgress(Int32.Parse(token.getValue(3)));
                        }
                        else if (strcmp(VikingConsoleDriver_getValue(token,2),"LABEL_2")==0)
                        {
                           // this.downloadProgressGUI.UpdateLabel1(token.getValue(3));
                        }
                        else if (strcmp(VikingConsoleDriver_getValue(token,2),"LABEL_1")==0)
                        {
                           // this.downloadProgressGUI.UpdateLabel2(token.getValue(3));
                        }
                    }
                    else if (strcmp(VikingConsoleDriver_getValue(token,1),"APPEND")==0 && strcmp(VikingConsoleDriver_getValue(token,2),"TEXTBOX_1")==0 && nElements > 3)
                    {
						char buffer[1000];
						sprintf(buffer,"<GUI> %s",VikingConsoleDriver_getValue(token,3));
						pApp->LogStatus(buffer);

                        ///this.BeginInvoke(new SimpleDelegate2(LogMessage), "<GUI> "+token.getValue(3));
                    }
                    else if (strcmp(VikingConsoleDriver_getValue(token,1),"FINISH")==0)
                    {
						char buffer[1000];
						sprintf(buffer,"<SYS> %s","Download Finished");
						pApp->LogStatus(buffer);
						::VikingConsoleDriver_disableTrigger(pApp->m_pVikingConsoleDriver);
						break;
                    }
                    else if (strcmp(VikingConsoleDriver_getValue(token,1),"START")==0)
                    {
						//disable trigger
                        //OnStart("Download Started");
						char buffer[1000];
						sprintf(buffer,"<GUI> %s","Download Started");
						pApp->LogStatus(buffer);
						::VikingConsoleDriver_disableTrigger(pApp->m_pVikingConsoleDriver);
                    }

                }

                else if (strcmp(VikingConsoleDriver_getValue(token,0),"BD")==0)
                {
                    if (strcmp(VikingConsoleDriver_getValue(token, 1),"SET")==0 && nElements > 3)
                    {

                        if (strcmp(VikingConsoleDriver_getValue(token,2),"PROGRESS")==0)
                        {
                           // this.downloadProgressBD.UpdateProgress(Int32.Parse(token.getValue(3)));
                        }
                        else if (strcmp(VikingConsoleDriver_getValue(token, 2),"LABEL_2")==0)
                        {
                           // this.downloadProgressBD.UpdateLabel1(token.getValue(3));
                        }
                        else if (strcmp(VikingConsoleDriver_getValue(token, 2),"LABEL_1")==0)
                        {
                           // this.downloadProgressBD.UpdateLabel2(token.getValue(3));
                        }
                    }
                    else if ((strcmp(VikingConsoleDriver_getValue(token,1),"APPEND")==0) && (strcmp(VikingConsoleDriver_getValue(token,2),"TEXTBOX_1")==0) && nElements > 3)
                    {
                        char buffer[1000];
						sprintf(buffer,"<BD > %s",VikingConsoleDriver_getValue(token,3));
						pApp->LogStatus(buffer);

                    }					
		
                    else if (strcmp(VikingConsoleDriver_getValue(token,1),"FINISH")==0)
                    {
						char buffer[1000];
						sprintf(buffer,"<SYS> %s","Download Finished");
						pApp->LogStatus(buffer);
						::VikingConsoleDriver_disableTrigger(pApp->m_pVikingConsoleDriver);
						break;
                    }
                    else if (strcmp(VikingConsoleDriver_getValue(token, 1),"START")==0)
                    {
						char buffer[1000];
						sprintf(buffer,"<BD > %s","Download Started");
						pApp->LogStatus(buffer);
						::VikingConsoleDriver_disableTrigger(pApp->m_pVikingConsoleDriver);
                    }
                }
            }
			catch(...)
            {
                // Do Nothing
            }
        }
        else
        {
           // pApp->m_pVikingConsoleDriver.reset();
        }
    }
  

	::VikingConsoleDriver_stopDownloadServer(pApp->m_pVikingConsoleDriver);


	pApp->m_bVentDownloaderInProgress = false;
	pApp->LogStatus("Exiting Downloader");

#endif

	return 0;
}
UINT CSwatApp::Thread_BreathDisplay(LPVOID lp)
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	char szValueOld[300];
	szValueOld[0]='\0';

	char szValueNew[300];
	szValueNew[0]='\0';

	char szValueNewBrType[300];
	char szValueOldBrType[300];

	
	
	
//	return 0;





//	CheckLoop check;

	while(1)
	{
//		check.AddCount();
//		ASSERT(check.TestLoop(1000, 1));


		Sleep(5);
		IPatientDataEl  *pEl;
		IPatientDataSet *pPd=getPatientDataSetInstance();     

	   for(int row_cnt=0;pEl=pPd->getPatientDataElement(row_cnt);row_cnt++)
	   {
			char *pName=pEl->getName();
			bool bEnumDef=false;
			if(strcmp (pName, "WaveformBdPhase") == 0)
			{
				if(pEl->isEnum() && pEl->isValid())
				{
					char *phaseType = pEl->getEnumValue();
					if(phaseType)
					{ 
						sprintf(szValueNew, "%s",phaseType);
						if(strcmp(szValueNew, szValueOld) != 0)
						{
							sprintf(pApp->m_CurrentBreathData.m_chBreathPhase,"%s", szValueNew);
							CString* p = new CString(szValueNew);
							if(pApp->m_pMainWnd && pApp->m_pMainWnd->m_hWnd)
							{
								::PostMessage(pApp->m_pMainWnd->m_hWnd,WM_BREATH_PHASE_RCVD, 0, reinterpret_cast<LPARAM>(p));
							}
							else
							{
								delete p;
							}
							strcpy(szValueOld, szValueNew);
						}
					}
					
				 }
			} else 	if(strcmp (pName, "endExpiratoryPressure") == 0)
			{
				    if(pEl->isValid())
					{
						pApp->m_CurrentBreathData.m_fPEEP = pEl->getFloatValue();
					}
			}
			else 	if(strcmp (pName, "breathType") == 0)
			{
				    if(pEl->isEnum() && pEl->isValid())
					{
						char *breathType = pEl->getEnumValue();
						if(breathType)
						{ 
							sprintf(szValueNewBrType, "%s",breathType);
							if(strcmp(szValueNewBrType, szValueOldBrType) != 0)
							{
								sprintf(pApp->m_CurrentBreathData.m_chBreathType,"%s", szValueNewBrType);
								strcpy(szValueOldBrType, szValueNewBrType);
							}
				
						}
			

					}
			}
			else if(strcmp (pName, "breathDuration") == 0)
			{
				if(pEl->isValid())
				{
					pApp->m_CurrentBreathData.m_fBreathDuration = pEl->getFloatValue();
				}
			}
			else if(strcmp (pName, "ieRatio") == 0)
			{
				if(pEl->isValid())
				{
				pApp->m_CurrentBreathData.m_fIERatio = pEl->getFloatValue();
				}
			}
			else if(strcmp (pName, "WaveformIsAutozeroActive") == 0)
			{
				if(pEl->isValid())
				{
					int nActive = pEl->getIntValue();
					if(pApp->m_CurrentWaveformData.n_isAutozeroActive != nActive)
					{
						pApp->m_CurrentWaveformData.n_isAutozeroActive = nActive;
						//LOG_MESSAGE(LM_INFO, "Autozero state %i", nActive);
					}
					
				}
			} 




		}
	}
   return 0;
}


UINT CSwatApp::Thread_WaveformData(LPVOID lp)
{
	
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	char szValueOld[300];
	szValueOld[0]='\0';

	char szValueNew[300];
	szValueNew[0]='\0';

	//char szValueNewBrType[300];
	//char szValueOldBrType[300];

	return 0;

	while(1)
	{
		Sleep(5);
		IPatientDataEl  *pEl;
		IPatientDataSet *pPd=getPatientDataSetInstance();     

	   for(int row_cnt=0;pEl=pPd->getWaveformElement(row_cnt);row_cnt++)
	   {
			char *pName=pEl->getName();
			bool bEnumDef=false;
			if(strcmp (pName, "WaveformBdPhase") == 0)
			{
				if(pEl->isValid())
				{
					char *phaseType = pEl->getEnumValue();
					if(phaseType)
					{ 
						sprintf(szValueNew, "%s",phaseType);
						if(strcmp(szValueNew, szValueOld) != 0)
						{
							sprintf(pApp->m_CurrentBreathData.m_chBreathPhase,"%s", szValueNew);
							CString* p = new CString(szValueNew);
							if(pApp->m_pMainWnd && pApp->m_pMainWnd->m_hWnd)
							{
								::PostMessage(pApp->m_pMainWnd->m_hWnd,WM_BREATH_PHASE_RCVD, 0, reinterpret_cast<LPARAM>(p));
							}
							else
							{
								delete p;
							}
							strcpy(szValueOld, szValueNew);
						}
					}
					
				 }
			}
			else 
			if(strcmp (pName, "WaveformIsAutozeroActive") == 0)
			{
				if(pEl->isValid())
				{
					int nActive = pEl->getIntValue();
					if(pApp->m_CurrentWaveformData.n_isAutozeroActive != nActive)
					{
						pApp->m_CurrentWaveformData.n_isAutozeroActive = nActive;
						LOG_MESSAGE(LM_INFO, "Autozero state %i", nActive);
					}
					
				}
			} 
		}
	}
 

	return 0;
}


CString CSwatApp::VerifyManually_Ex( CString strParam, CString strExpected)
{
	CMessageTimerDlg dlg;
	dlg.m_strParam = strParam;
	dlg.m_strMessage = strExpected;
	dlg.DoModal();

	CString strResult;
	strResult.Format("%d:%s", dlg.m_nTestResult, dlg.m_strActual);

	return strResult;

/*	Pass = 1
	fail = 2
*/
}


void CSwatApp::LogReportMessage(CString& str, int n)
{
	if(m_ReportFile.m_pStream == NULL) return;
	CTime time = CTime::GetCurrentTime();

	CString strTab(" &nbsp; &nbsp;&nbsp;");
	CString strHTML;

	//create and write to HTML report
	if(n == REPORT_TEST_FAIL || n==REPORT_ERROR)
	{
		//strHTML = "<font size=\"2\">" + time.Format("%H:%M:%S") +"</font>"+ " <font size=\"2\" color=\"#ff0000\"> &nbsp; FAIL" + strTab +str+ "</font><br /> \r\n";
		m_ReportFile.WriteString("<br /> \r\n");
		strHTML = "<font size=\"2\">" + time.Format("%H:%M:%S") +"</font>"+ " <font size=\"2\" color=\"#ff0000\"> &nbsp; FAIL" + strTab +str+ "</font><br /> \r\n";
		m_ReportFile.WriteString(strHTML);

	}
	else if(n == REPORT_WARNING)
	{
		strHTML = "<font size=\"2\">" + time.Format("%H:%M:%S") +"</font>"+ " <font size=\"2\" color=\"#ff0000\"> &nbsp; &nbsp;" + strTab +str+ "</font><br /> \r\n";
		m_ReportFile.WriteString(strHTML);

	}
	else if ( n == REPORT_TEST_SKIP )
	{
		strHTML = "<font size=\"2\" color=\"#ff0000\">" + time.Format("%H:%M:%S") + "&nbsp; SKIP" + strTab +str+ "</font><br /> \r\n";
		m_ReportFile.WriteString(strHTML);
	}
	else if ( n == REPORT_TITLE)
	{
		strHTML = "<font size=\"3\" color=\"#ff0000\">" + str + "</font><br /> \r\n";
		m_ReportFile.WriteString(strHTML);
	}
	else if ( n == REPORT_TEST_PASS )
	{
//		m_ReportFile.WriteString("<br /> \r\n");
		strHTML = "<font size=\"2\">" + time.Format("%H:%M:%S") +"</font>"+ " <font size=\"2\" color=\"##009900\"> &nbsp; PASS" + strTab +str+ "</font><br /> \r\n";
		m_ReportFile.WriteString(strHTML);
	}
	else if ( n == REPORT_PASS_FAIL_INFO) //actual and expected
	{
		strHTML = "<font size=\"2\" color=\"##000000\">" + strTab + strTab+strTab + "&nbsp;"+ strTab + str + "</font><br /> \r\n";
		m_ReportFile.WriteString(strHTML);
		if(str.Find("Expected") == 0)
		{
			m_ReportFile.WriteString("<br /> \r\n");//new line after expected results
		}
	}
	else if( n == REPORT_RESULT_FOOTER ) //Footer
	{
		strHTML = "<font size=\"2\" color=\"##000000\">" + str + "</font><br /> \r\n";
		m_ReportFile.WriteString(strHTML);
	}
	else if( n == REPORT_TEST_OBJECTIVE )
	{

		int nFound = str.Find(REPORT_OBJ_SEPERATOR);
		CString strTestID, strReq, strObjective;
		strTestID = str.Left(nFound);
		str = str.Right(str.GetLength() - nFound - 5);
		nFound = str.Find(REPORT_OBJ_SEPERATOR);
		strReq = str.Left(nFound);
		strObjective = str.Right(str.GetLength() - nFound - 5);
	


		strHTML = " <br/> <font size=\"2\" color=\"##000000\"> Testcase ID : &nbsp;"+ strTab + strTestID + "</font><br /> \r\n";
		m_ReportFile.WriteString(strHTML);
		
		strHTML = " <font size=\"2\" color=\"##000000\"> Requirement : &nbsp;"+ strTab + strReq + "</font><br /> \r\n";
		m_ReportFile.WriteString(strHTML);

		strHTML = " <font size=\"2\" color=\"##000000\"> Objective &nbsp; : &nbsp;"+ strTab + strObjective + "</font><br /> \r\n";
		m_ReportFile.WriteString(strHTML);

		m_strTestObjective = strTestID + ": " + strObjective;
	}
	else if( n == REPORT_IMAGE)//create tag with given image name
	{	
	
		strHTML.Format("<a href=\"%s\"> <img src=\"%s\" alt=\"%s\" width=\"600\" height=\"500\" /> </a>  \r\n", str,str,str);
		m_ReportFile.WriteString(strHTML);
		//<a href="1.png"> <img src="1.png" alt="1.png" width="304" height="228" /> </a> 

		strHTML.Format("<br/> %s%s%s%s%s %s <br />  \r\n", strTab,strTab,strTab,strTab,strTab,  str);
		m_ReportFile.WriteString(strHTML);
	}

	else if( n == REPORT_IMAGE_LANGUAGE)//generate html for two images, one in this folder and another corresponding from English folder
	{	
/*PRINTS:
<table border="0">  <tr>
	<td> <font size="2"> <a href="PNG.png"><img src="PNG.png" alt="PNG.png" width="510" height="425" /> </a>  </font></td> 
 	<td> <font size="2"> <a href="..\English\PNG.png"> <img src="..\English\PNG.png"  width="510" height="425" /> </a>  </font></td>	
</tr> </table> 
*/
		strHTML = "<table border=\"0\">  <tr>\r\n 	<td> <font size=\"2\"> <a href=\"";
		m_ReportFile.WriteString(strHTML);
		m_ReportFile.WriteString(str);
		strHTML = "\"> <img src=\"";
		m_ReportFile.WriteString(strHTML);
		m_ReportFile.WriteString(str);
		strHTML = "\" alt=\"";
		m_ReportFile.WriteString(strHTML);
		m_ReportFile.WriteString(str);
		strHTML = "\" width=\"510\" height=\"425\" /> </a>  </font></td> \r\n 	<td> <font size=\"2\"> <a href=\"..\\English\\" ;
		m_ReportFile.WriteString(strHTML);
		m_ReportFile.WriteString(str);
		strHTML = "\"> <img src=\"..\\English\\";
		m_ReportFile.WriteString(strHTML);
		m_ReportFile.WriteString(str);
		strHTML = "\"  width=\"510\" height=\"425\" /> </a>  </font></td>	\r\n </tr> </table> \r\n";
		m_ReportFile.WriteString(strHTML);


	}
	else	// all other messages are NOT tabbed in
	{
		//strHTML = "<font size=\"2\" color=\"##000000\">" + strTab + strTab+strTab + "&nbsp;"+ strTab + str + "</font><br /> \r\n";
		strHTML = "<font size=\"2\" color=\"##000000\">" + str + "</font><br /> \r\n";
		m_ReportFile.WriteString(strHTML);
	}
 
}

int CSwatApp::LogReportHeader(CString& strScriptName)
{
	m_strTestObjective.Empty();
	CString strTemp;

	//Create/open Report File for LogReportMessage( )
	CString strReportFileName = GetReportFileName(strScriptName);
	m_ReportFile.Open( strReportFileName, CFile::modeCreate | CFile::modeWrite | CFile::typeText | CFile::shareDenyNone);
	if(m_ReportFile.m_pStream == NULL)
	{
		CString str;
		str.Format("Could NOT create/open report file. \n%s", strReportFileName);
		LogStatus(str, SHOW_STATUS_DONT_LOG);
		return 0;
	}

	strTemp = "<HTML> \r\n<HEAD> \r\n<META HTTP-EQUIV=\"Content-Type\" CONTENT=\"text/html; charset=UTF-8\"> \r\n";
	m_ReportFile.WriteString(strTemp);

	strTemp.Format("<TITLE>Report for %s</TITLE> \r\n", strScriptName);
	m_ReportFile.WriteString(strTemp);

	m_ReportFile.WriteString("</HEAD><BODY> \r\n\r\n");



	m_ReportFile.WriteString("<span style=\"font-family: Lucida Console;\"> \r\n<font size = \"2\">   \r\n");

	strTemp.Format("Report for %s", strScriptName);
	m_ReportFile.WriteString(strTemp);

	m_ReportFile.WriteString("<table border=\"1\">   \r\n");

	CTime time = CTime::GetCurrentTime();

	m_ReportFile.WriteString("<tr> <td> <font size=\"2\"> TEST PROCEDURE 			</font></td> <td> <font size=\"2\"> "+ strScriptName + "</font></td>		</tr> \r\n");

	strTemp.Format("<tr> <td> <font size=\"2\"> LOGGED-IN USER 		</font></td> <td><font size=\"2\">				%s			</font></td></tr> \r\n", m_strUserName);
	m_ReportFile.WriteString(strTemp);
	
	strTemp.Format("<tr> <td><font size=\"2\">START DATE / TIME </font></td> <td><font size=\"2\"> %s &nbsp; %s </font></td>", time.Format("%B %d, %Y"), time.Format("%H:%M:%S"));
	m_ReportFile.WriteString(strTemp);

	strTemp.Format("<tr> <td> <font size=\"2\"> VENT SERIAL NUMBER 				</font></td> <td><font size=\"2\">		%s		</font></td></tr> \r\n", m_strVentSerialNumber);
	m_ReportFile.WriteString(strTemp);

	strTemp.Format("<tr> <td> <font size=\"2\"> VENT LANGUAGE 				</font></td> <td><font size=\"2\">		%s		</font></td></tr> \r\n", m_strVentLanguage);
	m_ReportFile.WriteString(strTemp);

	strTemp.Format("<tr> <td> <font size=\"2\"> TEST LUNG EQUIPMENT 				</font></td> <td><font size=\"2\">		%s		</font></td></tr> \r\n", m_strLungName);
	m_ReportFile.WriteString(strTemp);
	
	strTemp.Format("<tr> <td> <font size=\"2\"> VENT GUI SW VERSION 		</font></td> <td><font size=\"2\">				%s			</font></td></tr> \r\n", m_strVentGUISWVersion );
	m_ReportFile.WriteString(strTemp);
	
	strTemp.Format("<tr> <td> <font size=\"2\"> VENT BD SW VERSION 		</font></td> <td><font size=\"2\">		%s			</font></td></tr> \r\n", m_strVentBDSWVersion );
	m_ReportFile.WriteString(strTemp);
	
	strTemp.Format("<tr> <td> <font size=\"2\"> SWAT VERSION 			</font></td> <td><font size=\"2\">		%s				</font></td></tr> \r\n", strSWATVersion );
	m_ReportFile.WriteString(strTemp);
	

	//LogStatus("------------------------------------------------------------------------------------", REPORT_RESULT_HEADER);
//	m_ReportFile.WriteString("\r\n");
	m_ReportFile.WriteString("</tr> </table>    \r\n");

	m_ReportFile.WriteString( " </font> \r\n" );


	/* Meta Area Button
      DECL_ENUM_ELEMENT(ID_META_AREA_BUTTON),
      DECL_ENUM_ELEMENT(ID_META_AREA_CIRCUIT_TYPE),
      DECL_ENUM_ELEMENT(ID_META_AREA_PBW),
      DECL_ENUM_ELEMENT(ID_META_AREA_PBW_UNITS),
      DECL_ENUM_ELEMENT(ID_META_AREA_VT_PBW_RATIO),
      DECL_ENUM_ELEMENT(ID_META_AREA_VT_PBW_RATIO_UNITS),
      DECL_ENUM_ELEMENT(ID_META_AREA_MODE),
      DECL_ENUM_ELEMENT(ID_META_AREA_MAND_TYPE),
      DECL_ENUM_ELEMENT(ID_META_AREA_TRIG_TYPE),
      DECL_ENUM_ELEMENT(ID_META_AREA_TUBE_TYPE),
      DECL_ENUM_ELEMENT(ID_META_AREA_HUMIDIFIER_VOLUME), */
	return 1;
}


void CSwatApp::LogReportFooter(CString& strScriptName, bool bFailed)
{

	CTime time = CTime::GetCurrentTime();
	
	//LogStatus("------------------------------------------------------------------------------------", REPORT_RESULT_FOOTER);
	LogStatus(" ", REPORT_RESULT_FOOTER);

	if(m_ScriptResults.m_nObjective == 0)
	{
		LogStatus("SWAT: No objective defined in the script", REPORT_TEST_FAIL );
	}

	GetFileVersions();
	RemoveFileToRecordVersion();//clean up file name for next run

	if(m_bRun && !bFailed)
		LogStatus("==== PROCEDURE COMPLETED : "+ strScriptName, REPORT_RESULT_FOOTER);
	else if(m_bRun && bFailed)
		LogStatus("==== PROCEDURE ERROR : "+ strScriptName, REPORT_RESULT_FOOTER);
	else if(!m_bRun)
		LogStatus("==== PROCEDURE INTERRUPTED : "+ strScriptName, REPORT_RESULT_FOOTER);


	CString strResults;
	strResults.Format("==== TOTAL TEST CASES : %d  |  PASS : %d  |  FAIL : %d ",  
		m_ScriptResults.m_nTotal, m_ScriptResults.m_nPass, m_ScriptResults.m_nFail);
	LogStatus(strResults, REPORT_RESULT_FOOTER);

	if(m_ScriptResults.m_nWarnings)
	{
		strResults.Format("==== WARNINGS : %d  ",  m_ScriptResults.m_nWarnings);
		LogStatus(strResults, REPORT_RESULT_FOOTER);
	}

	CString strTime;
	strTime.Format("==== END DATE /TIME : %s   %s", time.Format("%B %d, %Y"),time.Format("%H:%M:%S"));
	LogStatus( strTime, REPORT_RESULT_FOOTER );


	CTimeSpan timeSpan = m_ScriptResults.m_timeStop - m_ScriptResults.m_timeStart;
	strTime.Format("==== SCRIPT RUN TIME : %s", timeSpan.Format("%H:%M:%S"));
	LogStatus( strTime, REPORT_RESULT_FOOTER );

	LogStatus("======================================================================", REPORT_RESULT_FOOTER);

		
	//Close BODY tag
	m_ReportFile.WriteString("\r\n</BODY>     \r\n");


	if(	m_bRun && 
		!bFailed &&
		m_ScriptResults.m_nTotal == m_ScriptResults.m_nPass &&
		m_ScriptResults.m_nPass != 0 &&
		m_ScriptResults.m_nWarnings == 0 
		)
	{
		if(m_ReportFile.GetFilePath().Left(2) == "O:" || m_strLockPassReports == "true")
			m_bSetCurrentReportReadOnly = TRUE;
	}
	else
	{
		m_bSetCurrentReportReadOnly = FALSE;
	}

}


CString CSwatApp::GetReportFileName(CString strScriptFileName)
{
	CString strFolder = strScriptFileName.Left(strScriptFileName.ReverseFind('\\'));
	CString filename = "rep_" + strScriptFileName.Right(strScriptFileName.GetLength()- strScriptFileName.ReverseFind('\\')-1)+".htm";
	return strFolder + "\\"+ filename;
}


//if caller is "script" then this function won't run if script is stopped(m_bRun ==0)
//otherwsie, if called from the dialogbox tab, you can still call it.
BOOL CSwatApp::Power_on_off_vent(CString strCaller, int nPortNum, bool bToSwitchOnOROff)
{
	LOG_MESSAGE(LM_INFO,"entering Power_on_off_vent");

	if(nPortNum<0 || nPortNum>5)
	{
		LOG_MESSAGE(LM_ERROR,"Invalid port: %d", nPortNum);
		return FALSE;
	}

	CString strSwitchIP = m_strPowerSwitch_IPAddr;//it's read from the init.lua
	if(m_strPowerSwitch_IPAddr.IsEmpty())
	{
		LOG_MESSAGE(LM_ERROR,"power_switch_ip not defined in SWATinit.lua");
		return FALSE;
	}

	CString strCmd;

	if(strCaller=="script" && !m_bRun) 
	{
		LOG_MESSAGE(LM_INFO,"leaving"); 
		return FALSE;
	}
	
	if(strSwitchIP == "0") 
	{
		LOG_MESSAGE(LM_ERROR,"power_switch_ip not defined in SWATinit.lua");
		return FALSE;
	}

	static int nTries = 0;

	TRY{
		CWaitCursor wait;
		CString str, strURL;
		CInternetSession IE;
		CHttpFile* file = NULL; 
		//sample to turn on first port: "http://admin:12345678@192.168.0.50/Set.cmd?CMD=SetPower+P60=1";

		

		strCmd.Format("http://admin:12345678@%s/Set.cmd?CMD=SetPower+P6",strSwitchIP);

		str.Format("%d=%d", nPortNum-1, bToSwitchOnOROff);
		strCmd = strCmd+str;
		

		file = (CHttpFile *)IE.OpenURL(strCmd);

		if (NULL != file)
		{
			file->ReadString(str);
			LOG_MESSAGE(LM_INFO, str.GetBuffer());
			str.ReleaseBuffer();
			LOG_MESSAGE(LM_INFO, "IE.OpenURL() returned NULL");
			//Clean up the file here to avoid a memory leak!!!!!!!
			file->Close(); 
			delete file; 
		}
		IE.Close();
	}
	CATCH(CInternetException, ex)
	{
		LOG_MESSAGE(LM_INFO,"%s", strCmd);

		LogStatus("ERROR: CInternetException caught in talking to IP switch, will retry if called by script");
		char szCause[255];
		ex->GetErrorMessage(szCause, 255);
		LogStatus(CString(szCause), REPORT_RED);

		
		//don't retry if manually called form SWAT dialog box
		if(strCaller!="script") return FALSE;

		nTries = nTries+1;
		if(nTries >= 3)
		{
			LOG_MESSAGE(LM_ERROR,"ERROR: Tried reset 3 times, may be Power Switch is not connected. check IP %s",strSwitchIP);
			nTries = 0; //for next time
			m_bRun = false;//stop current script
			return FALSE;
		}
	
		LogStatus("Calling Power_on_off_vent() again");
		Sleep(3000);
		Power_on_off_vent(strSwitchIP, nPortNum, bToSwitchOnOROff);
	}
	
	END_CATCH



	return TRUE;

}

void CSwatApp::OnSwatSettings()
{
	CSWATSettingsDlg dlg;
	if(dlg.DoModal() == IDOK)
	{
	}
}


CString CSwatApp::Get_Sent_Setting(CString str)
{
	int nCount = m_capturedSettingsList.GetCount();
	CapturedSettings setting;

	POSITION pos = m_capturedSettingsList.GetHeadPosition();
	for (int i = 0; i < nCount; i++)
	{
		if(m_capturedSettingsList.GetAt(pos).strSettingName.CompareNoCase(str) == 0)
		{
			return m_capturedSettingsList.GetAt(pos).strSettingValue;
		}
		m_capturedSettingsList.GetNext(pos);
	}    

	LOG_MESSAGE(LM_ERROR,"Setting '%s' not found for %s", str, __FUNCTION__);
	return "SettingNotFound:"+str;
}

void CSwatApp::OnHelpTest() //called by SWAT menu item -- for script use " int CSwatApp::Test()"
{

	
	

	/*
	CSwatApp* pApp = this;
	CString strFTPFilename = "TestScripts_some_with_space.lua_my_image_1_1.png";
	CString strHardDrive("C:\\temp.png");

		CInternetSession internetSession;
		CFtpConnection *ftp;
		CString strDir;
		CString str;
		TRY
		{
			ftp = internetSession.GetFtpConnection("192.168.0.2", 0, 0, 3021);
			if(ftp->GetCurrentDirectory(strDir)==0)
			{
				DWORD dw = GetLastError();
				str.Format("Copying %s to %s failed in GetCurrentDirectory(). Error code from GetLastError = %d", strFTPFilename, strHardDrive, dw);
				pApp->LogStatus(str);
				
				pApp->m_nImagesYetToCopyOverFTP --;

				ftp->Close();
				delete ftp;
				internetSession.Close();
				LeaveCriticalSection(&pApp->m_CriticalSectionFTP);
				return ;

			}
			if(!ftp->SetCurrentDirectory(strDir + "SWAT"))
			{
				DWORD dw;
				char strErr[800];
				DWORD len;
				InternetGetLastResponseInfo(&dw,  strErr,  &len);
				pApp->LogStatus(strErr);

				dw = GetLastError();
				str.Format("Copying %s to %s failed in SetCurrentDirectory(). Error code from GetLastError = %d", strFTPFilename, strHardDrive, dw);
				pApp->LogStatus(str);
				pApp->m_nImagesYetToCopyOverFTP --;

				ftp->Close();
				delete ftp;
				internetSession.Close();
				LeaveCriticalSection(&pApp->m_CriticalSectionFTP);
				return ;
			}

			// wait for the file to be completely saved on the vent
			CFtpFileFind finder(ftp);
			int startLen(1), newLen(2);
////////////////////////////////////////////////////////////////////////////////////////////////
			if (finder.FindFile(strFTPFilename) == FALSE)    
			{ 
				str.Format("File not found on ftp server: %s ", strFTPFilename);
				pApp->LogStatus(str, REPORT_RED);
				
				pApp->m_nImagesYetToCopyOverFTP --;
				ftp->Close();
				delete ftp;
				internetSession.Close();
				LeaveCriticalSection(&pApp->m_CriticalSectionFTP);
				return ;

				
			}	        
			//We have to use a FindNextFile to get info on first file, why ?. Becuase FindNextFile doesn't get the    
			//next file on its first invocation. It gets the first file! Makes sense doesn't it? Pure Genius...    
			finder.FindNextFile();   


CString source = strFTPFilename;

//Empty the CString    
CString temp_ftp_name = finder.GetFilePath();

//Get the actual file name in case wildcards were used    
if (temp_ftp_name.IsEmpty()) temp_ftp_name = source;

//Make sure we got something. If not use source    
int file_size = 0;   
file_size = finder.GetLength();    


		}
			CATCH(CInternetException, ex)
		{
			pApp->LogStatus("ERROR: CInternetException caught in copy over FTP", REPORT_ERROR);
			char szCause[255];
			ex->GetErrorMessage(szCause, 255);
			pApp->LogStatus(CString(szCause), REPORT_ERROR);
			
			pApp->m_nImagesYetToCopyOverFTP --;
			
			ftp->Close();
			delete ftp;
			internetSession.Close();
			LeaveCriticalSection(&pApp->m_CriticalSectionFTP);
			return ;
		}
		END_CATCH

return ;




*/



	
/*
	CMDIFrameWnd *pFrame = (CMDIFrameWnd*)AfxGetApp()->m_pMainWnd;                    
	if(!pFrame) return ;
    CMDIChildWnd *pChild = (CMDIChildWnd *) pFrame->GetActiveFrame();                  
	if(!pChild) return ;
	m_pView = (CSwatView*)pChild->GetActiveView();    
	if(!m_pView) return ;

	m_pView->m_pPowerDlg->GetStatus();
*/
	
/*

	//this is to store binary data(reports/log files/images) in the database tables
	//http://www.codeproject.com/KB/database/usingblob.aspx

	CTestResultLogSet m_TestLogSet;

	try
	{
		m_TestLogSet.Open();
		m_TestLogSet.AddNew();

		m_TestLogSet.m_test_user_name = "rohit";

		CFile	fileReport;
		CFileStatus	fileStatus;

		fileReport.Open("c:\\Book1.xls", CFile::modeRead);
		fileReport.GetStatus(fileStatus);

		m_TestLogSet.m_test_script_name = fileReport.GetFileTitle();
		m_TestLogSet.m_test_report.m_dwDataLength = (SQLUINTEGER)fileStatus.m_size;

		HGLOBAL hGlobal	= GlobalAlloc(GPTR, (SIZE_T)fileStatus.m_size);
		m_TestLogSet.m_test_report.m_hData = GlobalLock(hGlobal);

		fileReport.Read(m_TestLogSet.m_test_report.m_hData,(UINT)fileStatus.m_size);

		m_TestLogSet.SetFieldDirty(&m_TestLogSet.m_test_report);
		m_TestLogSet.SetFieldNull(&m_TestLogSet.m_test_report,FALSE);
		m_TestLogSet.Update();

		GlobalUnlock(hGlobal);

		m_TestLogSet.Close();

	//    pList->InsertItem(0,fileReport.GetFileTitle());
	}
catch(CException* pE)
	{
		pE->ReportError();
		pE->Delete();
		return;
	}




	// To restore image from db table

//	CTestResultLogSet m_TestLogSet;

	m_TestLogSet.m_strFilter.Format("test_user_name = '%s'","rohit");
	try
	{
		m_TestLogSet.Open();
		if  (m_TestLogSet.IsEOF())
			AfxMessageBox("Unable to get file from db");
		else
		{
			char    tmpPath[_MAX_PATH+1];
			GetTempPath(_MAX_PATH,tmpPath);

			CString strFileName("c:\\swat\\temp\\Book1.xls");
	        
			CFile	outFile(strFileName,CFile::modeCreate|CFile::modeWrite);
			LPSTR	buffer = (LPSTR)GlobalLock(m_TestLogSet.m_test_report.m_hData);
			outFile.Write(buffer,m_TestLogSet.m_test_report.m_dwDataLength);
			GlobalUnlock(m_TestLogSet.m_test_report.m_hData);
			outFile.Close();
		}

		m_TestLogSet.Close();

	}
	catch(CException* pE)
	{
		pE->ReportError();
		pE->Delete();
		return;
	}
*/


}


void CSwatApp::VentStatus()
{
	if(m_bRun == true)
	{
		AfxMessageBox("Script execution detected.");
		return;
	}

	if(m_pVentStatusDlg == NULL)
	{
		m_pVentStatusDlg = new CVentStatusDlg;
		if(!m_pVentStatusDlg->Create(IDD_VENT_STATUS_DLG))
		{
			delete m_pVentStatusDlg;
			m_pVentStatusDlg = NULL;
			return;
		}
		m_pVentStatusDlg->ShowWindow(SW_SHOW);
	}
	else
	{
		m_pVentStatusDlg->ShowWindow(SW_SHOW);
	}
}




void CSwatApp::OnSwatStopBdsimulations()
{
	CWaitCursor wait;
	
	{	
		BatchBuilder* m_batch = new BatchBuilder;
		int nMODULE_DATA_IDS_UNSIGNED_MAX = 400;

		m_batch->resetall();
	
	
		struct sockaddr_in si_other;
		int s,  slen=sizeof(si_other);

		s=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
		memset((char *) &si_other, 0, sizeof(si_other));
		si_other.sin_family = AF_INET;
		si_other.sin_port = htons(SEND_BATCH_PORT);
		si_other.sin_addr.s_addr =inet_addr("192.168.0.1");

		int i=sendto(s, m_batch->getBatch(), m_batch->getLength(), 0, (sockaddr *)&si_other, slen);
		LOG_MESSAGE(LM_INFO, "Batch file size : %d", m_batch->getLength());

		if(i != m_batch->getLength())
		{
			int n = WSAGetLastError( );
			//AfxMessageBox("sending batch to vent failed with windows socket error code: %d",n);
			LOG_MESSAGE(LM_ERROR, "sending batch to vent failed with windows socket error code: %d",n);
			//return; don't return w/o closing socket
		}
		LOG_MESSAGE(LM_INFO, "Command 0 : batch resetall send returned %i",i);

		closesocket(s);
		delete m_batch;
	}
	Sleep(1000);
	{	
		BatchBuilder* m_batch = new BatchBuilder;
		int nMODULE_DATA_IDS_UNSIGNED_MAX = 400;

		for (int i=0; i<nMODULE_DATA_IDS_UNSIGNED_MAX; i++) 
		{
			m_batch->unsetsensor(i);
		}
	
		struct sockaddr_in si_other;
		int s,  slen=sizeof(si_other);

		s=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
		memset((char *) &si_other, 0, sizeof(si_other));
		si_other.sin_family = AF_INET;
		si_other.sin_port = htons(SEND_BATCH_PORT);
		si_other.sin_addr.s_addr =inet_addr("192.168.0.1");

		int i=sendto(s, m_batch->getBatch(), m_batch->getLength(), 0, (sockaddr *)&si_other, slen);
		LOG_MESSAGE(LM_INFO, "Batch file size : %d", m_batch->getLength());

		if(i != m_batch->getLength())
		{
			int n = WSAGetLastError( );
			//AfxMessageBox("sending batch to vent failed with windows socket error code: %d",n);
			LOG_MESSAGE(LM_ERROR, "sending batch to vent failed with windows socket error code: %d",n);
			//return; don't return w/o closing socket
		}
		LOG_MESSAGE(LM_INFO, "Command 0 : batch unsetsensor returned %i",i);

		closesocket(s);
		delete m_batch;
	}
	Sleep(1000);
	{	
		BatchBuilder* m_batch = new BatchBuilder;
		int nMODULE_DATA_IDS_UNSIGNED_MAX = 400;

		for (int i=0; i<nMODULE_DATA_IDS_UNSIGNED_MAX; i++) 
		{
			m_batch->unsetrawsensor(i);
		}
	
		struct sockaddr_in si_other;
		int s,  slen=sizeof(si_other);

		s=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
		memset((char *) &si_other, 0, sizeof(si_other));
		si_other.sin_family = AF_INET;
		si_other.sin_port = htons(SEND_BATCH_PORT);
		si_other.sin_addr.s_addr =inet_addr("192.168.0.1");

		int i=sendto(s, m_batch->getBatch(), m_batch->getLength(), 0, (sockaddr *)&si_other, slen);
		LOG_MESSAGE(LM_INFO, "Batch file size : %d", m_batch->getLength());

		if(i != m_batch->getLength())
		{
			int n = WSAGetLastError( );
			//AfxMessageBox("sending batch to vent failed with windows socket error code: %d",n);
			LOG_MESSAGE(LM_ERROR, "sending batch to vent failed with windows socket error code: %d",n);
			//return; don't return w/o closing socket
		}
		LOG_MESSAGE(LM_INFO, "Command 0 : batch unsetrawsensor returned %i",i);

		closesocket(s);
		delete m_batch;
	}

	return;
}

void CSwatApp::OnUpdateSwatStopBdsimulations(CCmdUI *pCmdUI)
{
	pCmdUI->Enable(!m_bScriptRunning && !m_bRecording);
}



void CSwatApp::AddSessionTestResultsToFile()
{
	CMDIFrameWnd *pFrame = (CMDIFrameWnd*)AfxGetApp()->GetMainWnd();
	if(!pFrame) return;

	CMDIChildWnd *pChild = (CMDIChildWnd*)pFrame->GetActiveFrame();
	if(!pChild) return;

	CSwatView *pView = (CSwatView*)pChild->GetActiveView();
	if(pView) pView->AddSessionTestResultsToFile();


}
void CSwatApp::ReadSessionTestResultsFromFile(CStdioFile *file)
{
	CMDIFrameWnd *pFrame = (CMDIFrameWnd*)AfxGetApp()->GetMainWnd();
	if(!pFrame) return;

	CMDIChildWnd *pChild = (CMDIChildWnd*)pFrame->GetActiveFrame();
	if(!pChild) return;

	CSwatView *pView = (CSwatView*)pChild->GetActiveView();
	if(pView) pView->ReadSessionTestResultsFromFile(file);


}

CString CSwatApp::GetUserInput(CString strTitle)
{
	CGetUserInputDlg dlg;

	dlg.m_strTitle = strTitle;

	dlg.DoModal();

	CString strLog;

	strLog.Format("User entered '%s' for prompt '%s'", dlg.m_strUserInput, dlg.m_strTitle);

	LogStatus(strLog, REPORT_MESSAGE);

	return dlg.m_strUserInput;
}

void CSwatApp::WaterMarkOnOff(int n) 
{
	GuiIOError err;
	CheckGUIConnection();
	CString str;

	if(n==1)
	{	
		err = GuiIOAltiaSendEvent("121564_card_visibility", 7);
		LOG_MESSAGE(LM_INFO, "Water mark turned ON");
		return;

	}
	else if(n==0)
	{
		err = GuiIOAltiaSendEvent("121564_card_visibility", 0);
		LOG_MESSAGE(LM_INFO, "Water mark turned OFF");

//		err = GuiIOAltiaSendEvent("43910_card", -1);
//		LOG_MESSAGE(LM_INFO, "neo/ped/Adult image turned OFF");

		return;
	}
	LOG_MESSAGE(LM_INFO, "WaterMarkOnOff invalid param");
}

//to set Altia widget properties
void CSwatApp::SetWidgetProperties(CString str, int n) 
{
	GuiIOError err;
	CheckGUIConnection();
	err = GuiIOAltiaSendEvent(str, n);
	LOG_MESSAGE(LM_INFO, "Exiting SetWidgetProperties( )");
	return;

}
void CSwatApp::MouseOnOff(int n) 
{
	GuiIOError err;
	CheckGUIConnection();

	if(n==1)
	{	
		err = GuiIOCursor(true);
		LOG_MESSAGE(LM_INFO, "Mouse turned ON");
		return;

	}
	else if(n==0)
	{
		err = GuiIOCursor(false);
		LOG_MESSAGE(LM_INFO, "Mouse turned OFF");
		return;
	}
	LOG_MESSAGE(LM_INFO, "Mouse on/off invalid param");
}

CString CSwatApp::GetLanguageInString(ALT_LANGUAGE lang)
{
	CString strLanguage;

//880
if(1)
{
	return "Test mode for 880";
}

//880 over
	switch(lang)
	{
	 case NO_ALT_LANGUAGE:
			strLanguage = "ENGLISH";
			break;

	 case BULGARIAN:
			strLanguage = "BULGARIAN";
			break;
	 case CHINESE:
			strLanguage = "CHINESE";
			break;
	case CROATIAN:
			strLanguage = "CROATIAN";
			break;
	case CZECH:
			strLanguage = "CZECH";
			break;
	case DANISH:
			strLanguage = "DANISH";
			break;
	case DUTCH:
			strLanguage = "DUTCH";
			break;
	case FINNISH:
			strLanguage = "FINNISH";
			break;
	case FRENCH:
			strLanguage = "FRENCH";
			break;
	case GERMAN:
			strLanguage = "GERMAN";
			break;
	case GREEK:
			strLanguage = "GREEK";
			break;
	case HUNGARIAN:
			strLanguage = "HUNGARIAN";
			break;
	case ITALIAN:
			strLanguage = "ITALIAN";
			break;
	case JAPANESE:
			strLanguage = "JAPANESE";
			break;
	case KOREAN:
			strLanguage = "KOREAN";
			break;
	case NORWEGIAN:
			strLanguage = "NORWEGIAN";
			break;
	case POLISH:
			strLanguage = "POLISH";
			break;
	case PORTUGUESE:
			strLanguage = "PORTUGUESE";
			break;
	case ROMANIAN:
			strLanguage = "ROMANIAN";
			break;
	case RUSSIAN:
			strLanguage = "RUSSIAN";
			break;
	case SERBIAN:
			strLanguage = "SERBIAN";
			break;
	case SLOVAK:
			strLanguage = "SLOVAK";
			break;
	case SLOVENIAN:
			strLanguage = "SLOVENIAN";
			break;
	case SPANISH:
			strLanguage = "SPANISH";
			break;
	case SWEDISH:
			strLanguage = "SWEDISH";
			break;
	case TURKISH:
			strLanguage = "TURKISH";
			break;
	
	 default :
		 strLanguage.Format("Warning : Failed getting vent language, API returned %d", lang);
		 LogStatus(strLanguage);
		 LOG_MESSAGE(LM_ERROR, "Language name not found");
		 strLanguage.Format("Error, API returned %d", lang);
		break;
 } 

	return strLanguage;

}
int CSwatApp::TestLua(CString str1, CString str2) //its called from Lua, use OnHelpTest( )
{

GuiIOError err;
GuiIOAltiaPollEventResp altiaPollEventResp;
GuiIOVentInfoResp ventInfoResp;
CString str;

if(str1=="1"){
	//Reading an Altia animation value directly.
	err = GuiIOAltiaPollEvent("121564_card_visibility", &altiaPollEventResp);

	LogStatus(altiaPollEventResp.eventName);
	str.Format("poll event : %d",altiaPollEventResp.eventValue);
	LogStatus(str);

}
else if(str1=="2")
	//Write an Altia animation value directly. This example shows turning the non-production key watermark off (0% visibility).
	err = GuiIOAltiaSendEvent("121564_card_visibility", 0);

else if(str1=="3")
	err = GuiIOAltiaSendEvent("121564_card_visibility", 7);

else if(str1=="4")
{	//Get the ventilator info which now includes the master serial number and language properties. 
	err = GuiIOGetVentInfo(&ventInfoResp);

	//ventInfoResp.language;

	LogStatus(CString(ventInfoResp.serialNum));
	LogStatus(CString("Don't know which language vent is running"));
	LogStatus(CString(ventInfoResp.bdSoftwareVer));
	LogStatus(CString(ventInfoResp.guiSoftwareVer));
}

else if(str1=="5")
	//Click a button at the on-screen location specified.  
	err = GuiIOButtonClick(484, atoi(str2));


else if(str1=="6")
	//Turn the arrow cursor on or off.
	err = GuiIOCursor(true);

else if(str1=="7")
	//Turn the arrow cursor on or off.
	err = GuiIOCursor(false);















return 0;
/*	CStdioFile file( "C:\\Program Files\\TTERMPRO\\go2serviceMode.txt", CFile::modeCreate | CFile::modeWrite | CFile::typeText );
	CString strTemp;

	strTemp.Format("connect 192.168.0.1 \r\n");
	file.WriteString(strTemp);

	strTemp.Format("wait 'Press \'s\' to enter service mode.' \r\n");
	file.WriteString(strTemp);

	strTemp.Format("sendln 's' \r\n");
	file.WriteString(strTemp);

	strTemp.Format("wait '>' \r\n");
	file.WriteString(strTemp);

	strTemp.Format("disconnect \r\n");
	file.WriteString(strTemp);

	file.Close();

	return;
*/	
}

CString CSwatApp::GetBDComPort()
{
	return m_strBDComPort;
}

BOOL CSwatApp::TurnOnHitS(bool bScriptCall, bool bTurnON)
{
	//return if script called and it shas been stopped
	if(bScriptCall == 1 && m_bRun==0) return 0;

	//kill all open ttermo windows
	system( "taskkill /IM ttermpro.exe /F" ); //ttermpro 
	system( "taskkill /IM ttpmacro.exe /F" ); //ttp macro

	//create macro
	CString strMacroFile("C:\\Program Files\\TTERMPRO\\gotoservicemode.txt");
	CString strTemp;

	CStdioFile tmp;
	tmp.Open(strMacroFile, CFile::shareDenyWrite | CFile::modeCreate | CFile::modeWrite | CFile::typeText );
	strTemp.Format("connect '/C=%s' \r\n", GetBDComPort());
	tmp.WriteString(strTemp);
	tmp.WriteString("wait \"Press 's' to enter service mode.\" \r\n");
	tmp.WriteString("sendln 's' \r\n");
	tmp.WriteString("wait '>' \r\n");
	tmp.WriteString("disconnect ");
	tmp.Close();
	
	strTemp.Format("\"%s\" \"gotoservicemode.txt\"  \r\n", m_strTTP_MACRO_EXE);
	SaveString2LocalTempFile(SERVICE_MODE_BATCH, strTemp);

	//ExecuteBlocking(TTP_MACRO_EXE, NULL);

	if((bScriptCall == 1) && (m_bRun==0)) return 0;

	//turn on the vent
	if(bTurnON) {
		Power_on_off_vent("script", atoi(m_strPortVentSwitch), 1); }

	//run macro
	HINSTANCE n = ShellExecute(NULL, NULL, SERVICE_MODE_BATCH, NULL, NULL, SW_SHOWMINIMIZED ); 
	if((int)n <32)
	{
		char ch[30];
		_itoa((int)n, ch, 10);
		CString str;
		str.Format("error %s",ch);
		AfxMessageBox(str);
	}
	
	return 0;
}


BOOL CSwatApp::PingVent(BOOL bPrintError)
{

// for 880
	if(1)
	{
		return TRUE;
	}
/* comment out for 880
	//Bypass for test and debugging
	if(m_strVentType=="PC")
	{ 
		return TRUE;
	}

	m_strGUI_IPAddr;
	CPing p2;
	CPingReply pr2;
	CString str;

	if(bPrintError) LogStatus("Pinging GUI...");
	if (p2.Ping2(m_strGUI_IPAddr, pr2, 10, 2000))
	{

	}
	else
	{
		str.Format("GUI ping failed with error code %d. Try running 'arp -d' on cmd prompt.", GetLastError());
		if(bPrintError) LogStatus(str,REPORT_ERROR);

		return FALSE;
	}

	if(bPrintError)("Pinging BD...");
	if (p2.Ping2(m_strBD_IPAddr, pr2, 10, 2000))
	{
	}
	else
	{
		str.Format("BD ping failed, try running 'arp -d' on cmd prompt, GetLastError returned: %d", GetLastError());
		if(bPrintError) LogStatus(str,REPORT_ERROR);

		return FALSE;
	}
*/ //880 over

	return TRUE;
}
void CSwatApp::OnSoftwaredownloadStartdownload()
{
	if(m_bVentDownloaderInProgress)
	{
		AfxMessageBox("Download in progress. Please cancel download to restart");
		return;
	}
	CFileDialog dlg(TRUE, NULL,NULL,0, "BD Load (bin)|*.bin|", NULL);
	if(dlg.DoModal()==IDCANCEL)		
		return;


	CString str(dlg.GetPathName());
		
	str = str.Left(str.ReverseFind('\\'));
	str = str.Left(str.ReverseFind('\\'));

	if(AfxMessageBox(str, MB_YESNO) == IDNO)
	{
		return;
	}
	


	BeginWaitCursor();

	CString m_sSource = str;
	CString m_sDest = "C:\\";


	CFileOperation fo;
	fo.SetAskIfReadOnly(0);
	fo.SetOverwriteMode(1);
	if (!fo.Copy(m_sSource, m_sDest)) fo.ShowError();
	EndWaitCursor();

	m_bVentDownloaderRun = true;

	AfxBeginThread((AFX_THREADPROC)Thread_SWDownloader, (LPVOID)this);
}

void CSwatApp::OnSoftwaredownloadCanceldoo()
{
	m_bVentDownloaderRun = false;
	LogStatus("Cancel Download triggered");
}

void CSwatApp::OnSwatTakeventscreenshot()
{

	CString strFileName, strErr;
	CTime time = CTime::GetCurrentTime();
	strFileName.Format("%s_%s.png", m_strUserName, time.Format("%Y%m%d_%H%M%S"));

	if(GUI_IOConnect() == SWAT_FAIL) return;
		
	int nBufferState = GUI_CaptureScreenStatus();

	if(nBufferState>5)//more than 5 means junk from vent, reset connection and ask again
	{
		GUI_IODisconnect();
		Sleep(500);
		if(GUI_IOConnect()==SWAT_FAIL) return;
		nBufferState = GUI_CaptureScreenStatus();
	}
	

	CString str; str.Format("Vent image capture buffer count = %d", nBufferState);
	LogStatus(str);
	
	if(nBufferState > 3) 
	{
		LogStatus("Buffer full, please try later.", REPORT_RED);
		Sleep(500);
		return;
	}
	
	//now capture image
	str.Format("Capturing @ ftp://192.168.0.2:3021/SWAT w filename: %s. DblClik to open.", strFileName);
	LogStatus(str);

	err = GuiIOScreenCapture("/usbfs/SWAT/"+strFileName);
	
	if(err != GUIIO_SUCCESS) 
	{
		strErr.Format("GuiIOScreenCapture(%s) failed, error code:%d, %s", strFileName, err, GUI_IOGetError(err));
		LogStatus(strErr, REPORT_RED);
		return;
	}
	CWaitCursor();
	
	Sleep(1000);
	

//	if(PingVent())
	{
		
	}
}

void CSwatApp::OnUpdateSwatTakeventscreenshot(CCmdUI *pCmdUI)
{
	pCmdUI->Enable(!m_bScriptRunning && !m_bRecording);
}

CString CSwatApp::GetVentLanguage()
{
	if(m_bRun == 0) return "Script Stopped";

	CheckGUIConnection();

	GuiIOVentInfoResp out;
	GuiIOGetVentInfo(&out);
	return GetLanguageInString(out.language);

}

