
#ifndef Array_AlarmPtr_MAX_NUM_ALARMS_HH
#define Array_AlarmPtr_MAX_NUM_ALARMS_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  Array_AlarmPtr_MAX_NUM_ALARMS - Fixed Array of 'MAX_NUM_ALARMS' Elements of Type 'AlarmPtr'.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Foundation/vcssrc/FixedArray.H_v   25.0.4.0   19 Nov 2013 14:03:46   pvcs  $
//
//@ Modification-Log
//
//   Revision: 003   By: sah   Date: 07-Jan-1999   DR Number: 5321
//   Project:  ATC
//   Description:
//	Changed 'SoftFault()' method to an non-inlined method.
//  
//   Revision: 002   By: sah   Date: 06-Feb-1996   DCS Number: 674
//   Project:  Sigma (R8027)
//   Description:
//	Removed 'ifndef' and 'endif' from around includes.
//  
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//====================================================================

#include "AlarmName.hh"
#include "AlarmName.hh"

//@ Usage-Classes
#include "Array_AlarmPtr.hh"
//@ End-Usage


class Array_AlarmPtr_MAX_NUM_ALARMS : public Array_AlarmPtr
{
  public:
    Array_AlarmPtr_MAX_NUM_ALARMS(const Array_AlarmPtr_MAX_NUM_ALARMS& array);
    Array_AlarmPtr_MAX_NUM_ALARMS(void);
    virtual ~Array_AlarmPtr_MAX_NUM_ALARMS(void);

    inline void  operator=(const Array_AlarmPtr_MAX_NUM_ALARMS& array);

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL,
			   const char*       pPredicate = NULL);

  protected:
#if defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)
    virtual Ostream&  print_(Ostream& ostr) const;
#endif  // defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)

  private:
    //@ Data-Member:  pBlock_
    // A fixed array of 'MAX_NUM_ALARMS' elements.
    AlarmPtr  pBlock_[MAX_NUM_ALARMS];
};


// Inlined methods...
#include "Array_AlarmPtr_MAX_NUM_ALARMS.in"


#endif  // Array_AlarmPtr_MAX_NUM_ALARMS_HH
