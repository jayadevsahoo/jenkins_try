#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================ M O D U L E   D E S C R I P T I O N ====
//@ Filename: RollThun.cc - Rolling Thunder POST Test
//
//---------------------------------------------------------------------
//@ Interface-Description
//  >Von
//    This test determines if POST has been entered 3 or more times
//    without completion.  A power fail condition is NOT counted in
//    this determination.  In normal operation, POST is entered one
//    time on power-up and a second time due to a watchdog reset from
//    the watchdog timer test.  If it is entered a third time, a spurious
//    reset must have occurred so this is declared a MAJOR failure.
//
//    This test only runs on the BD CPU since it is the only CPU with
//    a power fail interrupt.
//
//  Free-Functions:
//    RollingThunderTest 
//
//  >Voff
//---------------------------------------------------------------------
//@ Rationale
//    This is a safety net design that catches endless POST restarts.
//
//---------------------------------------------------------------------
//@ Implementation-Description
//  >Von
//    The rolling thunder test is designed to enunciate a failure if BD-CPU
//    POST has been unexpectedly interrupted 3 times before completion. 
//    This is not to be confused with the unexpected reset umpire test that
//    fails with 3 counts.  Rolling thunder has no time basis, and does not
//    count power interruptions.
//    
//    The count is maintained in the POST NOVRAM variable RollingThunderCount.
//    This is set to zero during the power down sequence in the NMI interrupt
//    handler.  It is incremented and checked in this test.  After 3 counts the
//    test fails.  At the end of POST this is set back to zero.
//
//    Note: Rolling thunder is not tested on the GUI.  If the GUI is experiencing
//    multiple resets, the BD will detect a loss of communication.
//  >Voff
//---------------------------------------------------------------------
//@ Fault-Handling
//    Kernel test failures POST stop testing.  No error code is logged as
//    the integrity to write a value is in question.
//---------------------------------------------------------------------
//@ Restrictions
//    None
//---------------------------------------------------------------------
//@ Invariants
//    None
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Kernel/vcssrc/rollthun.ccv   25.0.4.0   19 Nov 2013 14:13:56   pvcs  $
//
//@ Modification-Log
//
//
//  Revision: 002    By: Gary Cederquist  Date: 15-AUG-1997  DR Number: 1875
//    Project: Sigma (R8027)
//    Description:
//       Added testable item annotations.
//
//  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//=====================================================================

#include "kpost.hh"
#include "rollthun.hh"
#include "postnv.hh"
#include "CpuDevice.hh"


//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Free-Function: RollingThunderTest 
//
//@ Interface-Description
//  >Von
//  Counts the number of POST restarts that are not AC power related.
//  For the GUI, this test is bypassed.  Declares a MAJOR POST failure
//  if it has been entered 3 or more times.  $[11016]
//
//  INPUTS:    NONE
//  OUTPUTS:   PASS, MAJOR
//
//  >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//    Determine the CPU type from a post utility.  If it is GUI, then
//    bypass this test.
//
//    Read the NOVRAM variable RollingThunderCount.  Increment the count, if
//    it is 3 or more then fail the test.    
//
//    The rolling thunder strike counter is reset (set to zero) when a 
//    power cycle interruption occurs or if POST completes successfully.
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//
//@ End-Free-Function
//=====================================================================

Criticality RollingThunderTest (void)
{

#if defined(SIGMA_UNIT_TEST)
   if ( !PKernelNovram->unitTestRollingThunder )
      return PASSED;
#endif

UNIT_TEST_POINT(POSTUT_36)
   if ( CpuDevice::IsBdCpu() )
   {
      // $[TI1.1]
#ifdef E600_840_TEMP_REMOVED
      if (++PKernelNovram->rollingThunderCount >= 3)
      {
         // $[TI2.1]
         return MAJOR;
      }
      else                      
      {
         // $[TI2.2]
         return PASSED;
      }
#endif
	  return PASSED;
   }
   else
   {
      // $[TI1.2]
      return PASSED;
   }

}


