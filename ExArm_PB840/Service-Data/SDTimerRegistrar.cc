#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: SDTimerRegistrar - The central registration, control, and
// dispatch point for all Fake Service Data generation timer events.
//  The timer events are received from the OS-Foundation subsystem 
//  via the unit test task's FakeServiceData queue.
//---------------------------------------------------------------------
//@ Interface-Description
// Note: only one object at a time can be registered for any one timer.
//---------------------------------------------------------------------
//@ Rationale
// This class is necessary to register, set, and centrally dispatch timer
// events which are received from the Operating-System subsystem.
//---------------------------------------------------------------------
//@ Implementation-Description
// Only one instance of this class is created.  The class is implemented
// with the TimerArray_[] as the centerpiece.  This is an array of
// private SDTimerRecord_ records -- one for each unique timer id.  The 
// SDTimerId acts as the index into the TimerArray_[].
// The RegisterTarget() method is used to initialize the pointer
// slots of the TimerArray_[] -- this is done on-the-fly as needed.
//
// Timer events are communicated to the registrar via the
// TimerEventHappened() method.  This then calls the
// timerEventHappened() of the object which is registered as a target
// of the particular timer event.
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and pre-conditions
// to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
// Unlike GuiSettingRegistrar and ServiceDataRegistrar, SDTimerRegistrar
// allows only one object at a time to be registered for a particular
// timer.  "Un-registering" is not allowed.  If you register for a timer
// you stay registered for it until another object registers for the same
// timer.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Data/vcssrc/SDTimerRegistrar.ccv   25.0.4.0   19 Nov 2013 14:21:02   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By:  gdc    Date:  12-Dec-2008    SCR Number: 6068
//  Project:  840S
//  Description:
//		Removed SUN prototype code.
//
//  Revision: 002  By: sah     Date:  15-Jan-99   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' to a non-inlined method.
//
//  Revision: 001  By:  yyy    Date:  16-May-95   DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//=====================================================================

#if defined FAKE_SM
// This class is used only for test simulation.


#include "Sigma.hh"
#include <stdio.h>
#include "SDTimerRegistrar.hh"

//@ Usage-Classes
#include "SDTimerId.hh"
#include "SDTimerTarget.hh"
#include "IpcIds.hh"
#include "MsgTimer.hh"
//@ End-Usage

//@ Code...

//
// Word-aligned memory pools for static member data.
//
static Uint TimerArrayMemory_ [
		((sizeof(SDTimerRegistrar::SDTimerRecord_)  + sizeof(Uint) - 1)
		 / sizeof(Uint)) * SDTimerId::NUM_TIMER_IDS];

//
// Initializations of static member data.
//
SDTimerRegistrar::SDTimerRecord_ *SDTimerRegistrar::TimerArray_ =
					(SDTimerRegistrar::SDTimerRecord_ *) ::TimerArrayMemory_;


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SDTimerRecord_()  [Default Constructor]
//
//@ Interface-Description
// Constructs and initializes the private SDTimerRecord_ structure within
// SDTimerRegistrar.  Needs no parameters.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SDTimerRecordInRegistrar::SDTimerRecord_(void) :
	msgTimer(0, 0, 0)
{
	CALL_TRACE("SDTimerRecordInRegistrar::SDTimerRecord_(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TimerEventHappened
//
//@ Interface-Description
// Called due to a timer event message from the operating system (comes via a
// timer task).  It is passed the event message sent by the timer task.  We
// decode the timer id from this message.  The object associated with
// this timer is then informed of the event by calling the object's
// corresponding timerEventHappened() method.
//---------------------------------------------------------------------
//@ Implementation-Description
// This is the central dispatch point for all Service Data timer events.
// Since a timer event has just been received, execute the callback
// to the target object, as long as the timer is marked as active (there
// is a small chance that the timer was deactivated just as the timer
// expired and that we receive an expired event for this deactivated
// timer, we ignore this event).
//
// timerId is used as an index into TimerArray_[] to find the target
// object.
//---------------------------------------------------------------------
//@ PreCondition
// The target object for the given 'timerId' must exist.  timerId
// must be a valid timer id.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SDTimerRegistrar::TimerEventHappened(UserAnnunciationMsg &eventMsg)
{
	CALL_TRACE("SDTimerRegistrar::TimerEventHappened(timerId)");

	SDTimerId::SDTimerIdType timerId =
					(SDTimerId::SDTimerIdType)eventMsg.timerParts.timerId;

#if defined FORNOW
printf("SDTimerRegistrar::TimerEventHappened(timerId=%d)\n", timerId);
#endif	// FORNOW

	CLASS_PRE_CONDITION(timerId >= 0 && timerId < SDTimerId::NUM_TIMER_IDS);
	CLASS_PRE_CONDITION(TimerArray_[timerId].pTarget != NULL);

	//
	// Execute the callback to the registered target.
	//
	if (TimerArray_[timerId].isActive)
	{
		TimerArray_[timerId].pTarget->timerEventHappened(timerId);
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: RegisterTarget [public, static]
//
//@ Interface-Description
// Objects needing to know when timer events occur use this method to
// register themselves for timer notices.  The parameters are as follows:
// >Von
//	timerId		The enum id of the timer associated with the target object.
//	pTarget		ptr to the object to be notified when the timer event given by
//				'timerId' occurs.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
// Registers the given target with the associated with the given 'timerId' by
// storing the given pointer into the appropriate slot of the TimerArray_[].
//---------------------------------------------------------------------
//@ PreCondition
// timerId must be valid and pTarget must be non-null.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SDTimerRegistrar::RegisterTarget(SDTimerId::SDTimerIdType timerId,
								  SDTimerTarget* pTarget)
{
	CALL_TRACE("SDTimerRegistrar::RegisterTarget("
									"SDTimerId::SDTimerIdType timerId, "
									"SDTimerTarget* pTarget)");

#if defined FORNOW
printf("SDTimerRegistrar::RegisterTarget(), timerId is %d, pTarget is %d\n",
		timerId, pTarget);
#endif	// FORNOW

	CLASS_PRE_CONDITION(timerId >= 0 && timerId < SDTimerId::NUM_TIMER_IDS);
	CLASS_PRE_CONDITION(pTarget != NULL);

	// Cancel an existing timer, if any.
	CancelTimer(timerId);
	SAFE_CLASS_ASSERTION(FALSE == TimerArray_[timerId].isActive);

	// Register the new target.
	TimerArray_[timerId].pTarget = pTarget;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: StartTimer [public, static]
//
//@ Interface-Description
// Starts the specified timer and simultaneously registers an object as the
// target of the timer.  This is just a handy shortcut for RegisterTarget()
// followed by the (overloaded) simpler StartTimer() call.  The parameters are
// as follows:
// >Von
//	timerId		The enum id of the timer associated with the target object.
//	pTarget		ptr to the object to be notified when the timer event given by
//				'timerId' occurs.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
// Registers the given target with the timer associated with the given
// 'timerId' by storing the given pointer into the appropriate slot of the
// TimerArray_[].  Then, starts the corresponding timer.
//---------------------------------------------------------------------
//@ PreCondition
// timerId must be valid and pTarget must be non-null.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SDTimerRegistrar::StartTimer(SDTimerId::SDTimerIdType timerId,
								SDTimerTarget *pTarget)
{
	CALL_TRACE("SDTimerRegistrar::StartTimer("
			"SDTimerId::SDTimerIdType timerId, SDTimerTarget *pTarget)");

#if defined FORNOW
printf("SDTimerRegistrar::StartTimer(), timerId is %d, pTarget is %d\n",
		timerId, pTarget);
#endif	// FORNOW

	CLASS_PRE_CONDITION(timerId >= 0 && timerId < SDTimerId::NUM_TIMER_IDS);
	CLASS_PRE_CONDITION(pTarget != NULL);

	StartTimer(timerId);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: StartTimer [public, static]
//
//@ Interface-Description
// Starts the specified timer.  It is passed 'timerId', the enum id of
// the timer associated with the target object.
//---------------------------------------------------------------------
//@ Implementation-Description
// Starts the corresponding timer as long as something is registered
// for it.
//---------------------------------------------------------------------
//@ PreCondition
//	The timerId must be valid.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SDTimerRegistrar::StartTimer(SDTimerId::SDTimerIdType timerId)
{
	CALL_TRACE("SDTimerRegistrar::StartTimer("
									"SDTimerId::SDTimerIdType timerId)");
#if defined FORNOW
printf("SDTimerRegistrar::StartTimer(), timerId is %d\n", timerId);
#endif	// FORNOW

	CLASS_PRE_CONDITION(timerId >= 0 && timerId < SDTimerId::NUM_TIMER_IDS);

	// Start timer only if we have a registered target for it
	if (TimerArray_[timerId].pTarget != NULL)
	{
		// Set the timer going
		TimerArray_[timerId].msgTimer.set();
		TimerArray_[timerId].isActive = TRUE;
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: CancelTimer [public, static]
//
//@ Interface-Description
//  Cancels the specified timer.  It is passed 'timerId', the enum id of
//  the timer.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Cancels the MsgTimer pointed to by timerId and marks the timer as
//  inactive.
//---------------------------------------------------------------------
//@ PreCondition
//	timerId must be a valid timer.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SDTimerRegistrar::CancelTimer(SDTimerId::SDTimerIdType timerId)
{
	CALL_TRACE("SDTimerRegistrar::CancelTimer("
									"SDTimerId::SDTimerIdType timerId)");

	CLASS_PRE_CONDITION(timerId >= 0 && timerId < SDTimerId::NUM_TIMER_IDS);

	if (TimerArray_[timerId].pTarget != NULL)
	{
		TimerArray_[timerId].msgTimer.cancel();
		TimerArray_[timerId].isActive = FALSE;
	}

	SAFE_CLASS_ASSERTION(FALSE == TimerArray_[timerId].isActive);
	SAFE_CLASS_ASSERTION(FALSE == TimerArray_[timerId].msgTimer.isSet());
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Initialize [public, static]
//
//@ Interface-Description
// Called at startup by the Service Data subsystem in order to initialize
// static member data.  No parameters needed.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Performs one-time initialization of static member data.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SDTimerRegistrar::Initialize(void)
{
	CALL_TRACE("SDTimerRegistrar::Initialize(void)");

	SAFE_CLASS_ASSERTION(sizeof(::TimerArrayMemory_) >=
				(sizeof(SDTimerRegistrar::SDTimerRecord_)
											* SDTimerId::NUM_TIMER_IDS));

	UserAnnunciationMsg timerEventMsg;		// 32 bits of data that each
											// MsgTimer will put on our input
											// queue to identify timeout event

	timerEventMsg.timerParts.eventType = UserAnnunciationMsg::TIMER_EVENT;

	// First, clear all flags and target pointers.
	for (Int32 i = 0; i < SDTimerId::NUM_TIMER_IDS; i++)
	{
		TimerArray_[i].pTarget = NULL;
		TimerArray_[i].isActive = FALSE;
	}

#	define TIMER_Q_ID	SERVICE_MODE_TASK_Q

	// Create a timer for the Service Data Management subsystem's service
	// data time out, 2 minutes.
	timerEventMsg.timerParts.timerId = (GuiTimerId::GuiTimerIdType) SDTimerId::SERVICE_DATA_TIMEOUT;
	new (&(TimerArray_[SDTimerId::SERVICE_DATA_TIMEOUT].msgTimer))
		MsgTimer(TIMER_Q_ID, timerEventMsg.qWord, 120000, MsgTimer::ONE_SHOT);

	timerEventMsg.timerParts.timerId = (GuiTimerId::GuiTimerIdType)SDTimerId::SERVICE_DATA_TIMER;

#if defined(FAKE_SM)
	// For the Sun, the first argument passed to MsgTimer's constructor is
	// a function pointer rather than a queue id!
	new (&(TimerArray_[SDTimerId::SERVICE_DATA_TIMER].msgTimer)) 
		MsgTimer(TIMER_Q_ID, timerEventMsg.qWord, 1000,
					MsgTimer::ONE_SHOT);
#else
	new (&(TimerArray_[SDTimerId::SERVICE_DATA_TIMER].msgTimer)) 
		MsgTimer(TIMER_Q_ID, timerEventMsg.qWord, 200,
					MsgTimer::ONE_SHOT);
#endif // defined FAKE_SM
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
SDTimerRegistrar::SoftFault(const SoftFaultID  softFaultID,
						     const Uint32       lineNumber,
						     const char*        pFileName,
						     const char*        pPredicate)  
{
	CALL_TRACE("SDTimerRegistrar::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, SERVICE_DATA, 
				SERVICE_DATA_TIMER_REGISTRAR, lineNumber,
										pFileName, pPredicate);
}


#endif	// FAKE_SM
