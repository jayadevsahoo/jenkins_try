#include "stdafx.h"
//=====================================================================
//   This is a proprietary work to which Puritan-Bennett corporation
//   of California claims exclusive right.  No part of this work may
//  be used, disclosed, reproduced, sorted in an information retrieval
//     system, or transmitted by any means, electronic, mechanical,
//   photocopying, recording, or otherwise without the prior written
//      permission of Puritan-Bennett Corporation of California.
//
//          Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: TaskControlMessage - Base class for all messages handled
//                              by Task-Control.
//---------------------------------------------------------------------
//@ Interface-Description
//  The TaskControlMessage is a virtual base class for all Task Control
//  messages.  It provides the base class constructor and destructor as
//  well as new and delete operators to allocate and deallocate
//  TaskControlMessages in a static shared BufferPool.  A task 
//  constructs a TaskControlMessage in the BufferPool.  It can then 
//  queue this message to another task in the same processor which can
//  reference the message directly since the TaskControlMessage is in 
//  the shared BufferPool.  TaskControlMessages transmitted from one
//  processor to the other cannot reference the message contents
//  directly but rather must create (copy construct) a new "concrete"
//  TaskControlMessage in its own address space.  This process rebuilds
//  the message's virtual pointers.  The reconstructed message can then
//  be referenced directly.
//---------------------------------------------------------------------
//@ Rationale
//  The TaskControlMessage class contains the base class methods
//  for all Task Control messages.  
//---------------------------------------------------------------------
//@ Implementation-Description
//  The TaskControlMessage new and delete operators override the
//  library operators for itself and all its derived classes.  These
//  operators allocate and deallocate memory for the message in the
//  global BufferPool.  The copy constructor is defined so the
//  receiving task can reconstruct the message in its own address space
//  with valid virtual pointers.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Sys-Init/vcssrc/TaskControlMessage.ccv   25.0.4.0   19 Nov 2013 14:33:58   pvcs  $
//
//@ Modification-Log
//
//   Revision 004   By:   sah      Date: 01/13/99    DR Number: 5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to a non-inlined method.
//
//   Revision 004   By:   syw      Date: 20-Aug-1998    DR Number:
//   Project:   Sigma   (R8027)
//   Description:
//		BiLevel Initial Release.  Made TCMESSAGE_BUFFER_LENGTH a function
//		of the BdReadyMessage.
//
//   Revision 003   By:   gdc      Date: 07-APR-1998    DR Number:  2755
//   Project:   Sigma   (R8027)
//   Description:
//      Doubled number of allocatable buffers due to increasing number of 
//      tasks using TaskControlMessages. Allocation now double projected 
//      worse case usage.
//
//   Revision 002   By:   sah      Date: 17-Dec-1997    DR Number:  2692
//   Project:   Sigma   (R8027)
//   Description:
//      Increased size of 'TCMESSAGE_BUFFER_LENGTH' due to buffer pool
//      overflow, because of change in revision string size.
//
//   Revision 001   By:   gdc      Date: 07/06/95    DR Number:   n/a  
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//=====================================================================

//=====================================================================
//
//      Includes...
//
//=====================================================================

#include "TaskControlMessage.hh"

//@ Usage-Classes
#include "IpcIds.hh"
#include "BufferPool.hh"
#include "BdReadyMessage.hh"

//@ End-Usage

//=====================================================================
//
//      Static Data...
//
//=====================================================================

static const Uint TCMESSAGE_BUFFER_LENGTH = ((sizeof( BdReadyMessage) / 4) + 2) * 4 ; // 320;

//  A MessageBuffer is sized to the maximum size of any TaskControlMessage
//  plus 4 bytes for control information used in BufferPool.  If this
//  buffer size is not large enough, the BufferPool::allocate method will
//  assert.
struct MessageBuffer
{
    Uint    memory[TCMESSAGE_BUFFER_LENGTH / sizeof(Uint)];
};

//  Allocate double the projected buffer requirement of 30
const Uint            NUM_MESSAGE_BUFFERS = 60;
static MessageBuffer  MessageBufferPool_[NUM_MESSAGE_BUFFERS];

//@ Code...

//=====================================================================
//
//      Public Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Initialize 
//
//@ Interface-Description
//  The TaskControlMessage Initialize function initializes the static
//  data for the class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Initializes the buffer pool that will contain the TaskControlMessages.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
void
TaskControlMessage::Initialize(void)
{
    CALL_TRACE("Initialize()");

    (void) GetBufferPool_();
    // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetBufferPool
//
//@ Interface-Description
//  This function returns a constant reference to the TaskControlMessage 
//  buffer pool instantiated as function static in the private 
//  GetBufferPool_.  The returned reference can be used to retrieve 
//  buffer pool statistics.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls the private GetBufferPool_ to return a non-const reference
//  to the buffer pool and then returns a const reference to the 
//  caller.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
const BufferPool &
TaskControlMessage::GetBufferPool(void)
{
    CALL_TRACE("GetBufferPool()");

    return GetBufferPool_();
    // $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TaskControlMessage [constructor]
//
//@ Interface-Description
//  The TaskControlMessage constructor accepts an enumerated class
//  identifier.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Initializes private data classId_ from the classId argument.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
TaskControlMessage::TaskControlMessage(enum Sys_Init::InitClassId classId)
    : classId_(classId)
{
    CALL_TRACE("TaskControlMessage()");
    // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TaskControlMessage [copy constructor]
//
//@ Interface-Description
//  TaskControlMessage class copy constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Copies the classId.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

TaskControlMessage::TaskControlMessage(const TaskControlMessage& rMessage) 
{
    CALL_TRACE("TaskControlMessage(const TaskControlMessage&)");

    classId_ = rMessage.classId_;
    // $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~TaskControlMessage [destructor]
//
//@ Interface-Description
//  TaskControlMessage destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

TaskControlMessage::~TaskControlMessage()
{
    CALL_TRACE("~TaskControlMessage()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: new
//
//@ Interface-Description
//  The TaskControlMessage class new operator overrides the library 
//  new operator to allocate memory for TaskControlMessage and any
//  of its derived classes.  This method requests a buffer from the 
//  shared memory BufferPool and returns its pointer.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls BufferPool class allocate method to allocate a message buffer.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void *
TaskControlMessage::operator new(size_t nbytes)
{
    CALL_TRACE("operator new(size_t)");

    return GetBufferPool_().allocate(nbytes);
    // $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: delete
//
//@ Interface-Description
//  The TaskControlMessage class delete operator ovverides the library 
//  delete operator to return TaskControlMessage memory buffer to the
//  BufferPool.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls the BufferPool class Free method to return the message buffer 
//  to the list of free buffers.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
TaskControlMessage::operator delete(void* pObject)
{
    CALL_TRACE("operator delete(void*)");

    GetBufferPool_().freeThePool(pObject);
    // $[TI1]
}

#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//
//@ Interface-Description
//   This method is called when a software fault is detected by the
//   fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//   'lineNumber' are essential pieces of information.  The 'pFileName'
//   and 'pPredicate' strings may be defaulted in the macro to reduce
//   code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This method receives the call for the SoftFault, adds it sub-system
//   and class name ID and sends the message to the non-member function
//   SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
void
TaskControlMessage::SoftFault(const SoftFaultID  softFaultID,
                              const Uint32       lineNumber,
                              const char*        pFileName,
                              const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
 
  FaultHandler::SoftFault(softFaultID, SYS_INIT,
                          Sys_Init::ID_TASKCONTROLMESSAGE, 
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)

//=====================================================================
//
//      Protected Methods...
//
//=====================================================================

//=====================================================================
//
//      Private Methods...
//
//=====================================================================



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetBufferPool_
//
//@ Interface-Description
//  This function returns a reference to the TaskControlMessage buffer
//  pool instantiated as function static.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Initializes the BufferPool on the first pass and returns its
//  reference.  Subsequent passes only returns the reference to the
//  static.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
BufferPool &
TaskControlMessage::GetBufferPool_(void)
{
    CALL_TRACE("GetBufferPool_()");

    static BufferPool   bufferPool( TC_BUFFER_POOL_MT, 
                                    (Uint*)MessageBufferPool_,
                                    sizeof(MessageBufferPool_),
                                    sizeof(MessageBuffer) );
    return bufferPool;
    // $[TI1]
}
