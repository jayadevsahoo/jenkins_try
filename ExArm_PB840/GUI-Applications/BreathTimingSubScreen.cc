#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: BreathTimingSubScreen - Displays the Breath Timing Diagram
// in the Lower Subscreen Area when the following Main Settings are selected in
// the Main Settings Area: Expiratory Time, Flow Pattern, I:E Ratio,
// Inspiratory Time, Peak Flow, Plateau Time, Respiratory Rate and Tidal
// Volume.  No adjustable settings are contained in this subscreen.
// However, the MainSettingsArea buttons are highly coupled with this
// subscreen in displaying the prompts and the cancel button.
//---------------------------------------------------------------------
//@ Interface-Description
// BreathTimingSubScreen displays the breath timing diagram which illustrates
// the relationship between the timing settings, namely: Respiratory Rate,
// Inspiratory Time, Expiratory Time and I:E Ratio.
//
// It is displayed when adjusting any of the following Main Settings:
// Expiratory Time, Flow Pattern, I:E Ratio, Inspiratory Time, Peak Flow,
// Plateau Time, Respiratory Rate and Tidal Volume.
//
// The activate()/deactivate() methods are called automatically on
// display/removal of this subscreen by our subscreen area owner.  The
// acceptHappened() method is called when the Accept key is pressed while the
// user is adjusting any of the above Main Settings.  initializeMainSetting()
// initialize flags to initial value.  Inform the main setting buttons
// to display the ACCEPTED values.  Clear the message area.  Deactivate
// the breath timing diagram.  Inform the Setting of the adjust batch
// process.  guiEventHappened() shall flatten the main setting buttons
// when INOP, GUI_ONLINE_READY, SETTINGS_TRANSACTION_SUCCESS,
// or COMMUNICATIONS_DOWN is detected.  buttonDownHappened() cancels
// all changes made through the MainSetting and deactivate this subscreen.
// adjustPanelAcceptPressHappened() is called when the operator presses
// down the Accept key to signal the accepting of mainSetting changes.
// ValueUpdateHappened() is a callback routine which determines whether to
// display this subscreen when a setting button's value is changed.
// UserEventHappened() is called when a setting button was pressed down/up,
// or a knob/accept event has occured, the appropriate prompt shall be
// displayed.  This is a callback routine, that is registered by
// MainSettingsArea.  initializeMainSetting() is called when resetting
// main setting buttons to initial values.  updateDisplay_() shall display
// the breath timing diagram and the cancelButton_.
//---------------------------------------------------------------------
//@ Rationale
// Implements the complete functionality of the Breath Timing subscreen in
// a single class.
//---------------------------------------------------------------------
//@ Implementation-Description
// Most of the functionality of the breath timing diagram is implemented
// by the BreathTimingDiagram class.  This class displays an instance of
// that class in the middle of the screen (along with a SubScreenTitleArea
// instance) and indicate to the diagram which of the Main Settings timing
// buttons (if any) is currently active, i.e. being adjusted by the user.
// This needs to happen so that the current setting (if it's a timing setting,
// i.e. one of Inspiratory Time, Expiratory Time, I:E Ratio or Respiratory
// Rate) can be highlighted on the timing diagram.  In addition, a cancel
// button shall be displayed if more than one setting values had been
// changed.  Appropriated prompt messages shall be displayed according to
// the state of this subScreen.
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and pre-conditions
// to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
// Only one instance of this class should be created.  It should only
// be displayed in LowerSubScreenArea.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/BreathTimingSubScreen.ccv   25.0.4.0   19 Nov 2013 14:07:38   pvcs  $
//
//@ Modification-Log   
//
//  Revision: 013  By:  gdc    Date:  10-Dec-2008    SCR Number: 6377
//  Project:  840S
//  Description:
//	Removed DELTA project single screen support.
//
//  Revision: 012 By: srp    Date: 28-May-2002   DR Number: 5908
//  Project:  VCP
//	Description:
//		Incorporated Remediation code review issue(s) -- commentary changes only.
//
//  Revision: 011  By: gfu     Date:  24-Apr-2002    DR Number: 5908
//  Project:  VTPC
//  Description:
//      Modified switch statements to adhere to coding standards in
//	guiEventHappened and UserEventHappened.
// 
//  Revision: 010  By: sah     Date:  16-Nov-2000    DR Number: 5796
//  Project:  VTPC
//  Description:
//      Changed ordering of tracking settings to ensure that "Ve-set"
//      is always in the same place (the first spot), and that "Vt/IBW"
//      would be next.  This change is to facilitate testing of these
//      tracking settings.
//
//  Revision: 009  By: sah     Date:  22-Aug-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      *  added general framework for tracking setting displays, with
//         automatic placement and display
//      *  added three new tracking settings:  minute volume, Vt-to-IBW
//         ratio, and Vt-supp-to-IBW ratio
//      *  deleted impossible state transition of being in the 'MODIFIED' state
//         with 'ZERO' items changed
//      *  removed unused 'ArrMainSettingChangeStates_[]'
//
//  Revision: 008  By: hhd	 Date: 05-May-2000   DCS Number:  5717
//  Project:  NeoMode
//  Description:
//      Disable KnobRotate sound when the main settings button(s) is in
//      deselected mode.
//
//  Revision: 007  By: sah	 Date: 10-Apr-2000   DCS Number:  5702
//  Project:  NeoMode
//  Description:
//      As part of the translation effort, the "Breath Timing" label
//      had to be re-aligned to allow for more flexibility with foreign
//      languages.
//
//  Revision: 006  By:  hhd	   Date:  07-Feb-2000    DCS Number: 5504
//  Project:  NeoMode
//  Description:
//      Added batch change capability to the MainSettingsArea, whereby multiple
//      settings can be changed and accepted as a group.
//
//  Revision: 005  By:  yyy	   Date:  17-Nov-1999	DCS Number: 5327
//  Project:  NeoMode
//  Description:
//		Initial version.
//		17-Nov_1999 (hhd) Modified to reduce header file dependencies.
//
//  Revision: 004  By:  sah	   Date:  11-Jan-1999    DCS Number: 5321
//  Project:  ATC
//  Description:
//		Added 'SIGMA_DEVELOPMENT' conditionals around this class (unused)
//		'SoftFault()' method.
//
//  Revision: 003  By: yyy      Date: 05-Sep-1998  DR Number: 
//    Project:  BiLevel (R8027)
//    Description:
//     Initial release for BiLevel.
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "Sigma.hh"
#include "GuiFoundation.hh"
#include "GuiApp.hh"
#include "GuiEventRegistrar.hh"
#include "BreathTimingSubScreen.hh"
#include "SettingContextHandle.hh"
#include "MandTypeValue.hh"

//@ Usage-Classes
#include "AdjustPanel.hh"
#include "LowerScreen.hh"
#include "LowerSubScreenArea.hh"
#include "MiscStrs.hh"
#include "Sound.hh"
#include "SettingSubject.hh"
#include "MainSettingsArea.hh"
#include "UpperSubScreenArea.hh"
//@ End-Usage

//@ Code...

// Initialize static constants.
static const Int32 TIMING_DIAGRAM_X_ = 107;
static const Int32 TIMING_DIAGRAM_Y_ = 68;
static const Int32 BUTTON_HEIGHT_ = 34;
static const Int32 BUTTON_WIDTH_ = 110;
static const Int32 BUTTON_X_ = 516;
static const Int32 BUTTON_Y_ = 200;
static const Int32 BUTTON_BORDER_ = 3;

static const Int32  TRACKING_LABEL_X_ = 5;
static const Int32  TRACKING_LABEL_Y_ = 5;
static const Int32  TRACKING_LABEL_HEIGHT_ = 20;
static const Int32  TRACKING_VALUE_X_ = 100;
static const Int32  TRACKING_CONTAINER_WIDTH_ = 170;

BreathTimingSubScreen::MainSettingAreaState BreathTimingSubScreen::MainSettingAreaState_;


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BreathTimingSubScreen()  [Constructor]
//
//@ Interface-Description
// Creates the Breath Timing subscreen.  Passed a pointer to the subscreen
// area which creates it (LowerSubScreenArea). 
//---------------------------------------------------------------------
//@ Implementation-Description
// Sizes and positions the subscreen container.  Appends and positions the
// breath timing diagram and subscreen title area.
//
// $[01079] Breath Timing subscreen displays breath timing graph.
// $[NE01001] When the Main Settings Area is in the up-and-unchanged state, all
// setting buttons ...
// $[NE01002] When the Main Settings Area is in the up-and-unchanged state, and a
// main setting button ...
// $[NE01003] When the Main Settings Area is in the up-and-unchanged state, the 
// following ...
// $[NE01004] When the Main Settings Area is in the down-and-unchanged
// state ...
// $[NE01005] When the Main Settings Area is in the down-and-unchanged
// state, a change of the ...
// $[NE01006] When the Main Settings Area is in the down-and-changed state...
// $[NE01007] When the Main Settings Area is in the down-and-changed state...
// $[NE01014] When the Main Settings Area is in the down-and-changed state
// $[NE01015] When the Main Settings Area is in the down-and-changed state, and the currently...
// $[NE01016] When the Main Settings Area is in the up-and-changed state, the following shall be...
// $[NE01017] When the Main Settings Area is in the up-and-changed state, the following shall cause...
// $[NE01018] When the Main Settings Area is in the up-and-changed state, and a main setting ...
//
// $[VC01005] The following tracking settings shall be displayed...
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

BreathTimingSubScreen::BreathTimingSubScreen(SubScreenArea *pSubScreenArea) :
			SubScreen(pSubScreenArea, TRUE),
			cancelButton_(BUTTON_X_, BUTTON_Y_,
							BUTTON_WIDTH_, BUTTON_HEIGHT_,
							Button::LIGHT, BUTTON_BORDER_,
							Button::ROUND, Button::NO_BORDER,
							MiscStrs::CANCEL_BREATH_TIMING_BUTTON_TITLE),
			breathTimingLabel_(MiscStrs::BREATH_TIMING_TITLE),

			minuteVolumeLabel_(MiscStrs::MINUTE_VOLUME_SETTING_LABEL,
							   MiscStrs::MINUTE_VOLUME_SETTING_HELP_MESSAGE),
			minuteVolumeValue_(TextFont::FOURTEEN, 3, CENTER),

			vtIbwRatioLabel_(MiscStrs::VT_IBW_RATIO_SETTING_LABEL,
						     MiscStrs::VT_IBW_RATIO_SETTING_VCV_HELP_MSG),
			vtIbwRatioValue_(TextFont::FOURTEEN, 3, CENTER),

			vsuppIbwRatioLabel_(MiscStrs::VSUPP_IBW_RATIO_SETTING_LABEL,
						        MiscStrs::VSUPP_IBW_RATIO_SETTING_HELP_MESSAGE),
			vsuppIbwRatioValue_(TextFont::FOURTEEN, 3, CENTER)
{
	CALL_TRACE("BreathTimingSubScreen::BreathTimingSubScreen"
										"(SubScreenArea *pSubScreenArea)");

	// Size and position the subscreen
	setX(0);
	setY(0);
	setWidth(LOWER_SUB_SCREEN_AREA_WIDTH);
	setHeight(LOWER_SUB_SCREEN_AREA_HEIGHT);

	// Add graphics
	breathTimingDiagram_.setX(TIMING_DIAGRAM_X_);
	breathTimingDiagram_.setY(TIMING_DIAGRAM_Y_);
	breathTimingDiagram_.setShow(TRUE);
	addDrawable(&breathTimingDiagram_);

	// Add breath timing label...
	breathTimingLabel_.setX(TIMING_DIAGRAM_X_);
	breathTimingLabel_.setY(TIMING_DIAGRAM_Y_+116);
	breathTimingLabel_.setColor(Colors::LIGHT_BLUE);
	breathTimingLabel_.setShow(TRUE);
	addDrawable(&breathTimingLabel_);

	// now that it has a parent container, center it within this subscreen...
	breathTimingLabel_.positionInContainer(::GRAVITY_CENTER);

	addDrawable(&cancelButton_);
	cancelButton_.setShow(FALSE);

	// Register for callbacks from cancel button
	cancelButton_.setButtonCallback(this);

	// Register for GUI event change callbacks.
	GuiEventRegistrar::RegisterTarget(this);

	Uint  idx = 0u;
	Uint  trackIdx;

	//-----------------------------------------------------------------
	// setup info for set minute volume...
	//-----------------------------------------------------------------

	arrTrackingValuesInfo_[idx].settingId = SettingId::MINUTE_VOLUME;
	arrTrackingValuesInfo_[idx].pLabel    = &minuteVolumeLabel_;
	arrTrackingValuesInfo_[idx].pValue    = &minuteVolumeValue_;

	trackIdx = 0u;
	arrTrackingValuesInfo_[idx].arrTrackingIds[trackIdx++] = SettingId::TIDAL_VOLUME;
	arrTrackingValuesInfo_[idx].arrTrackingIds[trackIdx++] = SettingId::RESP_RATE;
	arrTrackingValuesInfo_[idx].arrTrackingIds[trackIdx]   = SettingId::NULL_SETTING_ID;
	AUX_CLASS_ASSERTION((trackIdx <= NUM_TRACKED_SETTINGS_), trackIdx);

	idx++;

	//-----------------------------------------------------------------
	// setup info for Vt-to-IBW ratio...
	//-----------------------------------------------------------------

	arrTrackingValuesInfo_[idx].settingId = SettingId::VT_IBW_RATIO;
	arrTrackingValuesInfo_[idx].pLabel    = &vtIbwRatioLabel_;
	arrTrackingValuesInfo_[idx].pValue    = &vtIbwRatioValue_;

	trackIdx = 0u;
	arrTrackingValuesInfo_[idx].arrTrackingIds[trackIdx++] = SettingId::TIDAL_VOLUME;
	arrTrackingValuesInfo_[idx].arrTrackingIds[trackIdx]   = SettingId::NULL_SETTING_ID;
	AUX_CLASS_ASSERTION((trackIdx <= NUM_TRACKED_SETTINGS_), trackIdx);

	idx++;

	//-----------------------------------------------------------------
	// setup info for Vt-supp-to-IBW ratio...
	//-----------------------------------------------------------------

	arrTrackingValuesInfo_[idx].settingId = SettingId::VSUPP_IBW_RATIO;
	arrTrackingValuesInfo_[idx].pLabel    = &vsuppIbwRatioLabel_;
	arrTrackingValuesInfo_[idx].pValue    = &vsuppIbwRatioValue_;

	trackIdx = 0u;
	arrTrackingValuesInfo_[idx].arrTrackingIds[trackIdx++] = SettingId::VOLUME_SUPPORT;
	arrTrackingValuesInfo_[idx].arrTrackingIds[trackIdx]   = SettingId::NULL_SETTING_ID;
	AUX_CLASS_ASSERTION((trackIdx <= NUM_TRACKED_SETTINGS_), trackIdx);

	idx++;

	//-----------------------------------------------------------------
	// terminate array with NULL id...
	//-----------------------------------------------------------------

	arrTrackingValuesInfo_[idx].settingId = SettingId::NULL_SETTING_ID;

	AUX_CLASS_ASSERTION((idx <= NUM_TRACKING_INFO_), idx);

	//-----------------------------------------------------------------
	// setup tracking area...
	//-----------------------------------------------------------------

	for (idx = 0u;
		 arrTrackingValuesInfo_[idx].settingId != SettingId::NULL_SETTING_ID;
		 idx++)
	{
		TrackingInfo_&  rInfo = arrTrackingValuesInfo_[idx];

		// set up label...
		rInfo.pLabel->setColor(Colors::BLACK);
		rInfo.pLabel->setX(::TRACKING_LABEL_X_);
		rInfo.pLabel->setShow(FALSE);
		trackingSettingArea_.addDrawable(rInfo.pLabel);

		// set up value...
		rInfo.pValue->setColor(Colors::BLACK);
		rInfo.pValue->setX(::TRACKING_VALUE_X_);
		rInfo.pValue->setShow(FALSE);
		trackingSettingArea_.addDrawable(rInfo.pValue);
	}

	trackingSettingArea_.setFillColor(Colors::LIGHT_BLUE);
	trackingSettingArea_.setX(::TRACKING_LABEL_X_);
	trackingSettingArea_.setY(::TRACKING_LABEL_Y_);
	trackingSettingArea_.setWidth(::TRACKING_CONTAINER_WIDTH_);
	addDrawable(&trackingSettingArea_);

	//-----------------------------------------------------------------
	// setup state-transition table...
	//-----------------------------------------------------------------

	MainSettingAreaState_ = PASSIVE;

	for (Int32 stateIdx = 0; stateIdx < NUM_STATE; stateIdx++)
	{
		for (Int32 eventIdx = 0; eventIdx < SettingButton::NUM_EVENT; eventIdx++)
		{
			for (Int32 conditionIdx = 0; conditionIdx < NUM_CONDITION; conditionIdx++)
			{
				for (Int32 changes = 0;  changes < NUM_CHANGES; changes++)
				{
					if (SettingButton::TIMER == eventIdx)
					{								// $[TI1.1]
						xtionTable_[stateIdx][eventIdx][conditionIdx][changes] = PASSIVE;
					}
					else
					{								// $[TI1.2]
						xtionTable_[stateIdx][eventIdx][conditionIdx][changes] = stateIdx;
					}
				}
			}
		}
	}

	xtionTable_	[PASSIVE]		[SettingButton::DOWN]	[BY_OPERATOR] [ZERO] = UNMODIFIED;
	xtionTable_	[PASSIVE]		[SettingButton::DOWN]	[BY_OPERATOR] [GT_ZERO] = UNMODIFIED;

	xtionTable_	[UNMODIFIED]	[SettingButton::UP]		[BY_OPERATOR] [ZERO] = PASSIVE;
	xtionTable_	[UNMODIFIED]	[SettingButton::KNOB] 	[CHANGED]     [GT_ZERO] = MODIFIED;

	xtionTable_	[MODIFIED]		[SettingButton::CLEAR_KEY]	[BY_OPERATOR] [ZERO]	= UNMODIFIED;
	xtionTable_	[MODIFIED]		[SettingButton::CLEAR_KEY]	[BY_OPERATOR] [GT_ZERO]	= MODIFIED;

	xtionTable_	[MODIFIED]		[SettingButton::KNOB]	[CHANGED] [GT_ZERO]	= MODIFIED;

	xtionTable_	[MODIFIED]		[SettingButton::KNOB]	[NOT_CHANGED] [ZERO]	= UNMODIFIED;
	xtionTable_	[MODIFIED]		[SettingButton::KNOB]	[NOT_CHANGED] [GT_ZERO]	= MODIFIED;

	xtionTable_	[MODIFIED]	 	[SettingButton::UP] 	[BY_OPERATOR]  [GT_ZERO]	= DESELECTED;
	xtionTable_	[MODIFIED]	 	[SettingButton::UP] 	[BY_OPERATOR]  [ZERO]	= PASSIVE;

	xtionTable_	[DESELECTED]	[SettingButton::DOWN] 	[BY_OPERATOR]  [GT_ZERO]	= MODIFIED;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~BreathTimingSubScreen  [Destructor]
//
//@ Interface-Description
// Destroys BreathTimingSubScreen.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

BreathTimingSubScreen::~BreathTimingSubScreen(void)
{
	CALL_TRACE("BreathTimingSubScreen::BreathTimingSubScreen(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activate
//
//@ Interface-Description
// Called by our subscreen area before this subscreen is to be displayed
// allowing us to do any necessary setup. Needs no parameters.
//---------------------------------------------------------------------
//@ Implementation-Description
// Activates the breath timing diagram and based on the MandTypeValue
// determines which tracking setting, if any, to display.
//
// $[01079] The Breath Timing subscreen shall display a dynamically-updated
// 			Breath Timing Graph.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
BreathTimingSubScreen::activate(void)
{
	CALL_TRACE("BreathTimingSubScreen::activate(void)");

	LowerScreen::RLowerScreen.hideSubScreenBorders();

	// Activate the breath timing diagram
	breathTimingDiagram_.activate();

    const DiscreteValue MAND_TYPE_VALUE =
		SettingContextHandle::GetSettingValue(ContextId::ACCEPTED_CONTEXT_ID,
											  SettingId::MAND_TYPE);

	switch (MAND_TYPE_VALUE)
	{
	case MandTypeValue::VCV_MAND_TYPE :		// $[TI1]
		vtIbwRatioLabel_.setMessage(MiscStrs::VT_IBW_RATIO_SETTING_VCV_HELP_MSG);
		break;
	case MandTypeValue::VCP_MAND_TYPE :		// $[TI2]
		vtIbwRatioLabel_.setMessage(MiscStrs::VT_IBW_RATIO_SETTING_VCP_HELP_MSG);
		break;
	case MandTypeValue::PCV_MAND_TYPE :		// $[TI3]
	default :
		// do nothing...
		break;
	}

	updateDisplay_();
		// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: deactivate
//
//@ Interface-Description
// Called by our subscreen area just after this subscreen is removed from
// the display allowing us to do any necessary cleanup. Needs no parameters.
//---------------------------------------------------------------------
//@ Implementation-Description
// We clear the Adjust Panel focus to make sure the Main Setting button
// that is being pressed down loses focus.
// Then clear the prompt area
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
BreathTimingSubScreen::deactivate(void)
{
	CALL_TRACE("BreathTimingSubScreen::deactivate(void)");

	LowerScreen::RLowerScreen.showSubScreenBorders();

	// detach from each of the tracking settings...
	for (Uint idx = 0u;
		 arrTrackingValuesInfo_[idx].settingId != SettingId::NULL_SETTING_ID;
		 idx++)
	{
		const SettingId::SettingIdType  SETTING_ID =
										  arrTrackingValuesInfo_[idx].settingId;

		// ensure that the show state of all tracking settings are set to 'FALSE',
		// because that's what's expected upon re-entry into this screen...
		arrTrackingValuesInfo_[idx].pLabel->setShow(FALSE);
		arrTrackingValuesInfo_[idx].pValue->setShow(FALSE);

		// attach to tracking setting...
		detachFromSubject_(SETTING_ID, Notification::VALUE_CHANGED);
	}

	switch (MainSettingAreaState_)
	{
	case PASSIVE:								// $[TI1.1]
		// Clear Primary, Advisory and Secondary prompts
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY, 
						   	PromptArea::PA_HIGH, NULL_STRING_ID);
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_ADVISORY,
						   	PromptArea::PA_HIGH, NULL_STRING_ID);
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
						   	PromptArea::PA_HIGH, NULL_STRING_ID);
		break;
	case UNMODIFIED:				// $[TI1.2]
	case MODIFIED:
	case DESELECTED:
		break;
	default:
		AUX_CLASS_ASSERTION_FAILURE(MainSettingAreaState_);
		break;
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: acceptHappened
//
//@ Interface-Description
// Called by setting buttons when the operator presses the Accept key
// while adjusting a Main setting.  Informs the Settings-Validation
// subsystem of the event and dismisses this subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Make the "Accept" sound, clear the Adjust Panel focus, accept the
// adjusted settings, deactivate this subscreen and inform the Lower Screen
// of any apnea settings auto-correction that may have occurred.
//
// $[01053] How to change a main setting ...
// $[01054] Deselect main setting button and breath timing subscreen ...
// $[01257] The Accept key shall be the ultimate confirmation step for all ...
// $[01258] The GUI shall accept all adjusted settings.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
BreathTimingSubScreen::acceptHappened(void)
{
	CALL_TRACE("BreathTimingSubScreen::acceptHappened(void)");

	if (MainSettingAreaState_ == PASSIVE || MainSettingAreaState_ == UNMODIFIED)
	{											// $[TI1.1]
		Sound::Start(Sound::INVALID_ENTRY);
	}
	else
	{											// $[TI1.2]
		Sound::Start(Sound::ACCEPT);					// Make the Accept sound
		AdjustPanel::TakeNonPersistentFocus(NULL);		// Clear Adjust Panel focus

		// Clear the off screen key message
		GuiApp::PMessageArea->clearMessage(MessageArea::MA_MEDIUM);

		// Accept adjusted settings
		ApneaCorrectionStatus apneaCorrectionStatus;
		apneaCorrectionStatus = SettingContextHandle::AcceptBatch();

		MainSettingAreaState_ = PASSIVE;

		// Deactivate this subscreen and the main setting button, $[01054]
		getSubScreenArea()->deactivateSubScreen();

		//
		// The settings subsystem may have auto-adjusted some apnea
		// parameters upon acceptance of the vent settings above.  Pass the
		// correction status to the Lower Screen so that it can decide
		// whether or not to auto-launch the apnea setup screen.
		//
		LowerScreen::RLowerScreen.reviewApneaSetup(apneaCorrectionStatus);
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: guiEventHappened
//
//@ Interface-Description
// Called when a change in the GUI App event happens.  Passed the id of the
// event which changed.  
//---------------------------------------------------------------------
//@ Implementation-Description
// If the eventId is INOP, SETTINGS_LOCKOUT,
// or COMMUNICATIONS_DOWN then we shall flatten the main setting buttons.
// If the eventId is SETTINGS_TRANSACTION_SUCCESS  and 
// GuiApp::IsSettingsTransactions() is FALSE, then we also flatten the main
// settings buttons.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
BreathTimingSubScreen::guiEventHappened(GuiApp::GuiAppEvent eventId)
{
	CALL_TRACE("guiEventHappened(GuiApp::GuiAppEvent eventId)");

	switch (eventId)
	{
	case GuiApp::SETTINGS_TRANSACTION_SUCCESS:
		if (GuiApp::IsSettingsTransactionSuccess())
		{
			break;
		}
	case GuiApp::INOP:										// $[TI1.1]
	case GuiApp::COMMUNICATIONS_DOWN:						// $[TI1.2]
	case GuiApp::SETTINGS_LOCKOUT:
		initializeMainSetting();
		LowerScreen::RLowerScreen.getMainSettingsArea()->setToFlat();
		break;
		
	case GuiApp::SERVICE_READY:								// $[TI1.3]
		AUX_CLASS_ASSERTION_FAILURE(eventId);
		break;
		
	case GuiApp::UNDEFINED:
	case GuiApp::GUI_ONLINE_READY:
	case GuiApp::PATIENT_DATA_DISPLAY:
	case GuiApp::PATIENT_SETUP_ACCEPTED:
		break;
	
	default:
		// assert on illegal value
		AUX_CLASS_ASSERTION_FAILURE(eventId);
		break;
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: buttonDownHappened
//
//@ Interface-Description
// Called when cancel button is pressed down.  All changes made through
// the MainSetting shall be clearned.
//---------------------------------------------------------------------
//@ Implementation-Description
// Ignore event if byOperatorAction is FALSE and it is not the cancel
// button.  Pop up this button, initialize the data members, and
// deactivate this subscreen.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
BreathTimingSubScreen::buttonDownHappened(Button *pButton, Boolean byOperatorAction)
{
	CALL_TRACE("BreathTimingSubScreen::buttonDownHappened(Button *pButton, Boolean byOperatorAction)");

	// Only react to down events that come from the operator
	if (byOperatorAction)
	{													// $[TI1]
		if (pButton == (Button *) &cancelButton_)
		{												// $[TI1.1]
			pButton->setToUp();
			initializeMainSetting();

			// Deactivate this subscreen and the main setting button, $[01054]
			getSubScreenArea()->deactivateSubScreen();
		}												// $[TI1.2]
	}													// $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelAcceptPressHappened
//
//@ Interface-Description
// This is a virtual method inherited from being an AdjustPanelTarget.  It is
// normally called when the operator presses down the Accept key to signal the
// accepting of mainSetting changes.
//---------------------------------------------------------------------
//@ Implementation-Description
// Generate the accept sound, inform setting validation subsystem to accept
// the batch process, initialize the data members, deactivate this subscreen,
// and clear high prioritied prompts.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
BreathTimingSubScreen::adjustPanelAcceptPressHappened(void)
{
	CALL_TRACE("BreathTimingSubScreen::adjustPanelAcceptPressHappened(void)");

	Sound::Start(Sound::ACCEPT);					// Make the Accept sound
	SettingContextHandle::AcceptBatch();

	initializeMainSetting();

	// Deactivate this subscreen and the main setting button, $[01054]
	getSubScreenArea()->deactivateSubScreen();

	// Clear the off screen key message
	GuiApp::PMessageArea->clearMessage(MessageArea::MA_HIGH);
					// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelClearPressHappened
//
//@ Interface-Description
// This is a virtual method inherited from being an AdjustPanelTarget.  It is
// normally called when the operator presses down the Clear key to clear a
// mainSetting change.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Generate the INVALID ENTRY sound.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
BreathTimingSubScreen::adjustPanelClearPressHappened(void)
{
	CALL_TRACE("BreathTimingSubScreen::adjustPanelClearPressHappened(void)");

	Sound::Start(Sound::INVALID_ENTRY);					// Make the Invalid Entry sound
					// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: valueUpdate
//
//@ Interface-Description
// Called when a registered setting has changed.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
BreathTimingSubScreen::valueUpdate(const Notification::ChangeQualifier,
								   const SettingSubject* pSubject)
{
	CALL_TRACE("valueUpdate(qualifierId, pSubject)");

	// If we're invisible, do nothing
	if (!isVisible())
	{													// $[TI1]
		return;
	}													// $[TI2]

	for (Uint idx = 0u;
		 arrTrackingValuesInfo_[idx].settingId != SettingId::NULL_SETTING_ID;
		 idx++)
	{	// $[TI3] -- this path is ALWAYS taken...
		TrackingInfo_&  rInfo = arrTrackingValuesInfo_[idx];

		if (pSubject->getId() == rInfo.settingId)
		{	// $[TI3.1]
			const BoundedValue   CURRENT    = pSubject->getAdjustedValue();
			const Boolean        IS_CHANGED = pSubject->isChanged();

			rInfo.pValue->setValue(CURRENT.value);
			rInfo.pValue->setPrecision(CURRENT.precision);
			rInfo.pValue->setItalic(IS_CHANGED);
			rInfo.pValue->setHighlighted(IS_CHANGED);
			break;
		}	// $[TI3.2]
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ValueUpdateHappened
//
//@ Interface-Description
// Called when a setting was selected to decide if this screen needs
// to be displayed . Passed the following parameters from setting
// button to this subscreen:
// >Von
//  qualifierId			The context in which the change happened, either
//						ACCEPTED or ADJUSTED.
//  pSubject			The pointer to the Setting's Context subject.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
// Update the display of this subscreen.  Display the CANCEL ALL button
// according to whether there is setting change.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
BreathTimingSubScreen::ValueUpdateHappened(const Notification::ChangeQualifier,
										   const SettingSubject* pSubject)

{
    CALL_TRACE("valueUpdateHappened(qualifierId, pSubject)");

	LowerSubScreenArea::GetBreathTimingSubScreen()->updateDisplay_(
														pSubject->isChanged()
																  );
	// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: UserEventHappened
//
//@ Interface-Description
// Called when a setting button was pressed down/up, or a knob/accept
// event has occured, the appropriate prompt shall be displayed.
// Passed the following parameters:
// >Von
//	settingId			settingId enum
//	userEventId			user event enum
//	byOperatorAction	is by operation action or not
// >Voff
// A boolean flag is returned to indicate whether to keep the same adjust
// panel target.  TRUE  - keep the target focus.
//				  FALSE - release the target focus.
//---------------------------------------------------------------------
//@ Implementation-Description
// 
// we do the following: if we're on screen 1 then we have to check whether we
// should show the Support Type button or not (hide it if mode is A/C) and show
// the Spont Mandatory Type message (shown in Spont mode) or not.  We also 
// remove the Previous Setup button if it was being displayed.
//
// A screen 2 change to the Constant During Rate change setting
// will cause a new timing button to be displayed.
//
// If it is the first change on either screen then we record
// the fact that the settings have been adjusted ( we need this to determine
// when to activate the ACCEPT key).
// We also change the screen title to "Proposed" instead of "Current".
//
// $[01058] The accept key shall not be enabled... until at least one setting..
// $[01090] Remove Previous Setup button if a change is made.
// $[CL01001] When a non-selected padlock is selected, the corresponding
//            timing parameter's setting button is displayed, and selected,
//            in place of the "old" constant setting button.
//---------------------------------------------------------------------
//@ PreCondition
// The qualifierId must be either ACCEPTED or ADJUSTED.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Boolean
BreathTimingSubScreen::UserEventHappened(const SettingId::SettingIdType settingId,
										 const SettingButton::UserEventId userEventId,
										 const Boolean byOperatorAction)
{
    CALL_TRACE("UserEventHappened(settingId, userEventId, byOperatorAction)");

	//-----------------------------------------------------------------
	// Prior to state change...
	//-----------------------------------------------------------------

	if (MainSettingAreaState_ == PASSIVE)
	{	// $[TI1] -- transitioning out of the PASSIVE state...
		// update to the currently-accepted values...
		SettingContextHandle::AdjustBatch();
	}	// $[TI2] -- NOT starting in the PASSIVE state...

	//-----------------------------------------------------------------
	// State change...
	//-----------------------------------------------------------------

	BreathTimingSubScreen*  pSubScreen = LowerSubScreenArea::GetBreathTimingSubScreen();

	MainSettingAreaState_ = (MainSettingAreaState)
	 pSubScreen->xtionTable_[MainSettingAreaState_][userEventId][byOperatorAction][getNumChanged()];

	//-----------------------------------------------------------------
	// Following state change...
	//-----------------------------------------------------------------

	Boolean keepFocus = FALSE;
	Boolean displayCancelButton = FALSE;

	switch(MainSettingAreaState_)
	{
	//-----------------------------------------------------------------
	//  PASSIVE state is when nothing is changed, and no button is down...
	//-----------------------------------------------------------------
	case PASSIVE:									// $[TI3]
		// Deactivate the breath timing diagram
		pSubScreen->initializeMainSetting();

		// Deactivate this subscreen
		LowerScreen::RLowerScreen.getLowerSubScreenArea()->deactivateSubScreen();
		break;

	//-----------------------------------------------------------------
	//  UNMODIFIED state is when nothing is changed, but a button is down...
	//-----------------------------------------------------------------
	case UNMODIFIED:								// $[TI4]
		if (userEventId == SettingButton::DOWN)
		{											// $[TI4.1]
			// only update for DOWN events, thereby reducing "flashing"...
			pSubScreen->updateTrackingDisplay_(TRUE, settingId);
		}											// $[TI4.2]

		GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY,
							PromptArea::PA_HIGH,
							PromptStrs::USE_KNOB_TO_ADJUST_P);
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_ADVISORY,
							PromptArea::PA_HIGH, NULL_STRING_ID);
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
							PromptArea::PA_HIGH, 
							PromptStrs::MAIN_SETTING_CANCEL_S);
		break;

	//-----------------------------------------------------------------
	//  DESELECTED state is when at least one setting is changed, but
	//  no buttons are down...
	//-----------------------------------------------------------------
	case DESELECTED:								// $[TI5]
		if (userEventId == SettingButton::UP)
		{											// $[TI5.1]
			// only update for UP events, thereby reducing "flashing"...
			pSubScreen->updateTrackingDisplay_(FALSE, settingId);
		}											// $[TI5.2]

		displayCancelButton = TRUE;

		keepFocus = TRUE;
		AdjustPanel::TakeNonPersistentFocus(pSubScreen);
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY,
							PromptArea::PA_HIGH,
							PromptStrs::PRESS_ACCEPT_IF_OK_P);
		GuiApp::PPromptArea->setPrompt(
							PromptArea::PA_SECONDARY,
							PromptArea::PA_HIGH, 
							PromptStrs::MAIN_SETTING_CANCEL_BUTTON_S);

		// Disable the knob sound.
		AdjustPanel::DisableKnobRotateSound();
		break;

	//-----------------------------------------------------------------
	//  MODIFIED state is when at least one setting is changed, and a
	//  button is down...
	//-----------------------------------------------------------------
	case MODIFIED:								// $[TI6]
		if (userEventId == SettingButton::DOWN)
		{										// $[TI6.1]
			// only update for DOWN events, thereby reducing "flashing"...
			pSubScreen->updateTrackingDisplay_(TRUE, settingId);
		}										// $[TI6.2]

		displayCancelButton = TRUE;

		GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY,
							PromptArea::PA_HIGH,
							PromptStrs::USE_KNOB_TO_ADJUST_P);

		GuiApp::PPromptArea->setPrompt(
							PromptArea::PA_SECONDARY,
							PromptArea::PA_HIGH, 
							PromptStrs::MAIN_SETTING_CANCEL_BUTTON_S);
		break;

	default:
		// assert on illegal value
		AUX_CLASS_ASSERTION_FAILURE(MainSettingAreaState_);
		break;
	}


	pSubScreen->cancelButton_.setShow(displayCancelButton);
 
	return (keepFocus);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: initializeMainSetting
//
//@ Interface-Description
// Called when resetting main setting buttons to initial values.
//---------------------------------------------------------------------
//@ Implementation-Description
// Initialize flags to initial value.  Inform the main setting buttons
// to display the ACCEPTED values.  Clear the message area.  Deactivate
// the breath timing diagram.  Inform the Setting of the adjust batch
// process.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
BreathTimingSubScreen::initializeMainSetting(void)
{
	CALL_TRACE("initializeMainSetting(void)");

	MainSettingAreaState_ = PASSIVE;
	LowerScreen::RLowerScreen.getMainSettingsArea()->batchSettingUpdate(
							Notification::ACCEPTED, NULL,  (SettingId::SettingIdType) 0);

	AdjustPanel::TakeNonPersistentFocus(NULL);

	// Clear the Message Area
	GuiApp::PMessageArea->clearMessage(MessageArea::MA_MEDIUM);

	GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY, 
						   	PromptArea::PA_HIGH, NULL_STRING_ID);
	GuiApp::PPromptArea->setPrompt(PromptArea::PA_ADVISORY,
						   	PromptArea::PA_HIGH, NULL_STRING_ID);
	GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
						   	PromptArea::PA_HIGH, NULL_STRING_ID);

	breathTimingDiagram_.deactivate();

	SettingContextHandle::AdjustBatch();
				// $[TI1]	
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateDisplay_
//
//@ Interface-Description
// Called when a setting has changed. Passed the following parameters:
// >Von
//	breathTimingDisplay	 Display flag for breath timing diagram.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
// Display the cancelButton_ based on the MainSettingAreaState.
//---------------------------------------------------------------------
//@ PreCondition
// The qualifierId must be either ACCEPTED or ADJUSTED.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
BreathTimingSubScreen::updateDisplay_(Boolean displayCancelButton)
{
    CALL_TRACE("updateDisplay_(breathTimingDisplay, displayCancelButton)");
 
	// $[TI1] (TRUE)  $[TI2] (FALSE)...
	cancelButton_.setShow(displayCancelButton || getNumChanged());
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getNumChanged 
//
//@ Interface-Description
//  This method is to query whether there are setting changes for all of Main Settings Area.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Use SettingContextHandle to query setting changes.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Int32
BreathTimingSubScreen::getNumChanged(void)
{
	return(SettingContextHandle::AreAnyBatchSettingsChanged() ? GT_ZERO : ZERO);
	// $[TI1]
}			


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
BreathTimingSubScreen::SoftFault(const SoftFaultID  softFaultID,
							     const Uint32       lineNumber,
							     const char*        pFileName,
							     const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS,
				BREATHTIMINGSUBSCREEN, lineNumber, pFileName, pPredicate);
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  updateTrackingDisplay_
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
BreathTimingSubScreen::updateTrackingDisplay_(
							const Boolean                  isPressedDown,
							const SettingId::SettingIdType settingId
											 )
{
	Int   textHeight = 0;

	TrackingInfo_*  pInfo = arrTrackingValuesInfo_;

	for (; pInfo->settingId != SettingId::NULL_SETTING_ID; pInfo++)
	{
		const SettingSubject*  pSubject = getSubjectPtr_(pInfo->settingId);

		Boolean  displaySetting = FALSE, isChanged = FALSE;

		if (pSubject->getApplicability(Notification::ACCEPTED) ==
														Applicability::VIEWABLE)
		{	// $[TI1] -- this tracking setting is currently viewable...
			//-----------------------------------------------------------------
			// first see if it is in a changed state...
			//-----------------------------------------------------------------

			isChanged = pSubject->isChanged();

			//-----------------------------------------------------------------
			// if not, and the button has been pressed down, see if the button
			// is one of its tracked settings...
			//-----------------------------------------------------------------

			if (!isChanged  &&  isPressedDown)
			{	// $[TI1.1] -- check if pressed button is its tracked setting...
				SettingId::SettingIdType*  pTrackingIds = pInfo->arrTrackingIds;

				// see if this setting that is pressed is being tracked by this
				// tracking setting...
				for (; *pTrackingIds != settingId  &&
						*pTrackingIds != SettingId::NULL_SETTING_ID;
						pTrackingIds++)
				{
					// do nothing...
				}

				// if 'settingId' was found, then display this tracking
				// setting...
				displaySetting = (*pTrackingIds == settingId);
			}
			else
			{	// $[TI1.2] -- changed or not pressed down...
				displaySetting = isChanged;
			}
		}	// $[TI2] -- not viewable...

		//-----------------------------------------------------------------
		// now, display it if it passed the tests above...
		//-----------------------------------------------------------------

		if (displaySetting)
		{	// $[TI3] -- the tracking setting needs to be displayed...
			if (!pInfo->pLabel->isVisible())
			{	// $[TI3.1] -- it's not displayed yet...
				pInfo->pLabel->setShow(TRUE);

				const BoundedValue  CURRENT
					= (MainSettingAreaState_ == UNMODIFIED)
						? pSubject->getAcceptedValue()		// $[TI3.1.1]
						: pSubject->getAdjustedValue();		// $[TI3.1.2]

				pInfo->pValue->setValue(CURRENT.value);
				pInfo->pValue->setPrecision(CURRENT.precision);
				pInfo->pValue->setItalic(isChanged);
				pInfo->pValue->setHighlighted(isChanged);
				pInfo->pValue->setShow(TRUE);
			}	// $[TI3.2] -- already displayed...

			// set the 'Y' coordinates for the label and value...
			pInfo->pLabel->setY(::TRACKING_LABEL_Y_ + textHeight);
			pInfo->pValue->setY(pInfo->pLabel->getY());

			// attach to tracking setting...
			attachToSubject_(pInfo->settingId, Notification::VALUE_CHANGED);

			// include in overall text height...
			textHeight += (::TRACKING_LABEL_HEIGHT_ + ::TRACKING_LABEL_Y_);
		}
		else
		{	// $[TI4] -- this tracking setting is not viewable...
			pInfo->pLabel->setShow(FALSE);
			pInfo->pValue->setShow(FALSE);

			// detach from tracking setting...
			detachFromSubject_(pInfo->settingId, Notification::VALUE_CHANGED);
		}
	}

	// $[VC01005] -- don't show the tracking area until a tracked setting
	//               is pressed, and/or changed...
	if (textHeight > 0)
	{	// $[TI5]
		trackingSettingArea_.setShow(TRUE);
		trackingSettingArea_.setHeight(::TRACKING_LABEL_Y_ + textHeight);
	}
	else
	{	// $[TI6]
		trackingSettingArea_.setShow(FALSE);
	}
}
