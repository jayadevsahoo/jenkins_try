/*****************************************************************************/
/*   Module Name:   perf.h                                          
/*   Purpose:      840 Ventilator Final Test                                   
/*                                                                          
/*                                                                           
/*   Requirements:   This module implements requirements detailed in        
/*          <72361-93>     
/*	 $Revision::   1.1      $                                                 
/*                              
/*                                                                           
/*                                                                          
/*****************************************************************************/

/*****************************************************************************
THIS IS A PROPRIETARY WORK TO WHICH NELLCOR PURITAN BENNETT CORPORATION CLAIMS
EXCLUSIVE RIGHT.  NO PART OF THIS WORK MAY BE USED, DISCLOSED, REPRODUCED,
STORED IN AN INFORMATION RETIEVAL SYSTEM, OR TRANSMITTED, IN ANY FORM OR BY ANY
MEANS ELECTRONIC, MECHANICAL, PHOTOCOPYING, RECORDING OR OTHERWISE WITHOUT THE
PRIOR WRITTEN PERMISSION OF NELLCOR PURITAN BENNETT CORPORATION.

Copyright (c) 1999, Mallinckrodt Nellcor Puritan Bennett
All rights reserved.
*******************************************************************************/

#ifndef _PERF_H
#define _PERF_H

#include "common.h"

/***********/
/* DEFINES */
/***********/

int MeasurePressureSingle(double *reading, int timeOut);  
int MeasureVolumeTrig(double *reading, int timeOut);  
int MeasureFlowSingle(double *reading, int timeOut); 
int MeasureFlowSingleO2(double *reading, int timeOut);
int MeasurePressureSingleOneSec(double *reading, int timeOut);
int MeasurePressureSingleOneSecAfter(double *reading, int timeOut); 
int MeasureOxygenLocal(double *measuredVal);
int MeasureBarometricPressureLocal(double *measuredVal);
int SetupPressure(int sampleRate, int burst );
int RecievePressureReading(double *reading, int *pointsReceived,int timeReq);   
int Ack(void);
int SetupFlow(int sampleRate, int burst, int gas);
int RecieveReading(double *reading, int *pointsReceived, int timeReq);

#endif // #ifndef _PERF_H
