
#ifndef NovRamXaction_HH
#define NovRamXaction_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: NovRamXaction - NovRam Transaction Class.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Persistent-Objects/vcssrc/NovRamXaction.hhv   25.0.4.0   19 Nov 2013 14:18:52   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By:  sah    Date:  01-Jan-1999    DCS Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' to a non-inlined method.
//
//  Revision: 002  By:  sah    Date:  29-Sep-1997    DCS Number: 2525
//  Project:  Sigma (R8027)
//  Description:
//	Removed obsoleted date references.
//
//  Revision: 001  By:  sah    Date:  15-Mar-1997    DCS Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//          Initial version (Integration baseline).
//
//====================================================================

#include "Sigma.hh"
#include "PersistentObjsId.hh"
#include "NetworkMsgId.hh"

//@ Usage-Classes
class  BufferPool;	// forward declaration...

#include "ResultTableEntry.hh"
#include "CodeLogEntry.hh"
#include "ChangeStateMessage.hh"
#include "NovRamUpdateManager.hh"
#include "InterTaskMessage.hh"
//@ End-Usage


class NovRamXaction
{
  public:
    //@ Type:  XactionId
    // Identifier of the requested transaction.
    enum XactionId
    {
      DATA_UPDATE = ::FIRST_APPLICATION_MSG
    };

    static void  Initialize(void);
    static void  TaskDriver(void);

    static void  Send(const NovRamUpdateManager::UpdateTypeId novRamUpdateId);

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL, 
			   const char*       pPredicate = NULL);

    //@ Constant:  MAX_MISC_DATA_BLOCK_SIZE
    // Static, public constant that contains the maximum size allowed for a
    // miscellaneous data item.
    enum { MAX_MISC_DATA_ITEM_SIZE = 20 };

  private:
#if defined(SIGMA_UNIT_TEST)
  // provide access to private parts during unit testing...
  public:
#endif // defined(SIGMA_UNIT_TEST)
    NovRamXaction(const NovRamXaction&);		// not implemented...
    NovRamXaction(void);				// not implemented...
    ~NovRamXaction(void);				// not implemented...
    void  operator=(const NovRamXaction&);		// not implemented...

    //@ Type:  LogXmitBlock_
    // Structure for transmitting a diagnostic log.
    struct LogXmitBlock_
    {
      Uint16        updateId;
      Uint16        numOfEntries;
      CodeLogEntry  arrLogEntries[::MAX_CODE_LOG_ENTRIES];
    };

    //@ Type:  TableXmitBlock_
    // Structure for transmitting a result table.
    struct TableXmitBlock_
    {
      Uint16           updateId;
      Uint16           numOfEntries;
      ResultEntryData  arrTableEntries[::MAX_TEST_ENTRIES];
    };

    //@ Type:  MiscDataXmitBlock_
    // Structure for transmitting miscellaneous data blocks.
    struct MiscDataXmitBlock_
    {
      Uint16  updateId;
      Uint16  sizeOfDataBlock;
      Byte    pDataBlock[MAX_MISC_DATA_ITEM_SIZE];
    };

    //@ Type:  CommandXmitBlock_
    // Structure for transmitting commands.
    struct CommandXmitBlock_
    {
      Uint16  updateId;
      Uint16  UNUSED_WORD;
    };

    static inline Uint  CalcLogXmitBlockSize_  (const Uint numOfLogEntries);
    static inline Uint  CalcTableXmitBlockSize_(const Uint numOfTableEntries);

    static void  ChangeStateCallback_(
				  const ChangeStateMessage& changeStateMsg
				     );

    static void  ReceiveXaction_(XmitDataMsgId,
    				 void* pDataBlock,
				 Uint sizeOfDataBlock);

#if defined(SIGMA_GUI_CPU)
    static void  ReceiveUpdate_(void* pXferBlock);

    //@ Data-Member:  PChangeStateMsg_
    // Static pointer to a change-state message.
    static ChangeStateMessage*  PChangeStateMsg_;

    //@ Data-Member:  RDiagLogBufferPool_
    // Statically allocated buffer pool used for inter-task transmissions of
    // diagnostic log information.
    static BufferPool&  RDiagLogBufferPool_;

    //@ Data-Member:  RResultTableBufferPool_
    // Statically allocated buffer pool used for inter-task transmissions of
    // test result table information.
    static BufferPool&  RResultTableBufferPool_;

    //@ Data-Member:  RMiscDataBufferPool_
    // Statically allocated buffer pool used for inter-task transmissions of
    // miscellaneous data information.
    static BufferPool&  RMiscDataBufferPool_;
#endif // defined(SIGMA_GUI_CPU)
};


// Inlined methods...
#include "NovRamXaction.in"


#endif // NovRamXaction_HH 
