#ifndef Setting_HH
#define Setting_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  Setting - Setting Class.
//---------------------------------------------------------------------
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/Setting.hhv   25.0.4.0   19 Nov 2013 14:27:40   pvcs  $
//
//@ Modification-Log
//
//  Revision: 014   By: gdc    Date: 16-Aug-2010     SCR Number: 6663
//  Project: API/MAT
//  Description:
//  Added support for VentSet API.
// 
//  Revision: 013   By: gdc    Date: 21-Jan-2009    SCR Number: 6458
//  Project:  840S
//  Description:
//      Modified to use isNewlyApplicable() method to determine if
//		setting's applicability has changed from non-applicable to
//		an applicable state. Resolves problem of setting getting
//		into a changed state and not being able to get out of it when
//		even when the applicability reverts to non-applicable.
//
//  Revision: 012   By: gdc    Date: 08-Jan-2009    SCR Number: 6177
//  Project:  840S
//  Description:
//      Implemented isNewlyApplicable() to correctly identify when a
//      setting has become newly applicable in the adjusted context.
//      
//  Revision: 011   By: gdc    Date: 11-Aug-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support changing IBW while ventilating.
//
//  Revision: 010 By: rhj    Date: 01-Dec-2006   DR Number: 6291
//  Project:  RESPM
//	Description:
//      Added functionality to prevent settings getting stored into NOVRAM. 
//
//  Revision: 009  By: sah     Date:  22-Aug-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//	VTPC project-related changes:
//      *  added 'isEnabledValue()' to provide support for the new
//         drop-down menus
//      *  added 'TransitionState' enum and 'testTransition()' method
//         to provide support for the new conditional transition rules
//         of mandatory type
//
//  Revision: 008   By: sah    Date: 29-Jun-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  added new virtual method, 'resetConstraints()' for resetting
//         all dynamic constraints to their absolute limit counterpart,
//         to ensure correct calculations during new-patient initialization
//      *  re-designed much of the settings framework, moving many methods
//         up to this level, further simplifying, and genericizing, the
//         design, especially with respect to the soft-limit framework
//
//  Revision: 007   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  defined new, pure virtual 'getApplicability()' method
//	*  defined new, virtual 'settingObserverInit()' method
//	*  moved 'get{Max,Min}Limit()' methods from derived class up
//	   to here, to simplify implementation throughout subsystem
//	*  now derived off of new 'SettingSubject' class, thereby
//	   making all settings "subjects" for observers to monitor
//	*  moved 'getId()' and the forced-change-flag methods and data
//	   items up to the 'SettingSubject' base class
//
//  Revision: 006   By: dosman Date:  16-Sep-1998    DR Number: BiLevel 144
//  Project:  BILEVEL
//  Description:
//	See Setting.cc for details.
//
//  Revision: 005   By: dosman Date:  08-Sep-1998    DR Number: BILEVEL 130
//  Project:  BILEVEL
//  Description:
//	See Setting.cc for details.
//
//  Revision: 004   By: dosman Date:  16-Jun-1998    DR Number: BILEVEL 116
//  Project:  BILEVEL
//  Description:
//	See Setting.cc header for details.
//
//  Revision: 003   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 002   By: dosman Date:  23-Feb-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//      Inital BiLevel version.
//	Added updateAbsValues() (see Method Description for details)
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Initial version.
//
//====================================================================

#if defined(SIGMA_DEVELOPMENT)
#include "Ostream.hh"
#endif  // defined(SIGMA_DEVELOPMENT)

//@ Usage-Classes
#include "BoundStatus.hh"
#include "SettingSubject.hh"
//@ End-Usage


class Setting : public SettingSubject
{
#if defined(SIGMA_DEVELOPMENT)
    friend inline Ostream&  operator<<(Ostream&, const Setting&);
#endif // defined(SIGMA_DEVELOPMENT)

  public:
    virtual ~Setting(void);

    inline const BoundStatus&  getBoundStatus(void) const;

    virtual Applicability::Id  getApplicability(
			    const Notification::ChangeQualifier qualifierId
					       ) const = 0;

    virtual SettingValue  getDefaultValue   (void) const = 0;
    virtual SettingValue  getNewPatientValue(void) const = 0;

    virtual SettingValue  getAcceptedValue(void) const;
    virtual SettingValue  getAdjustedValue(void) const;

    virtual void  setAdjustedValue(const SettingValue& newAdjustedValue);
    virtual void  setAcceptedValue(const SettingValue& newAcceptedValue);

    virtual Real32  getMaxLimit(void);
    virtual Real32  getMinLimit(void);

    virtual Boolean isEnabledValue(const DiscreteValue value) const;

    virtual void  resetState(void);

    virtual void  settingObserverInit(void);

    virtual Boolean  isChanged(void) const = 0;
    virtual Boolean  isNewlyApplicable(void) const;

    virtual void  resetConstraints(void);

    virtual void  updateToDefaultValue   (void);
    virtual void  updateToNewPatientValue(void);
    virtual void  updateToNewIbwValue(void);

    virtual void  activationHappened(void);

    virtual const BoundStatus&  calcNewValue(const Int16 knobDelta) = 0;

    void  overrideSoftBound(const SettingBoundId boundId);

    void  resetToStoredValue(void); 

    virtual const BoundStatus*  acceptPrimaryChange(
				   const SettingId::SettingIdType primaryId
						   );

    //@ Type:  TransitionState
    // Indicates whether the transition performed is irrelavent to whether
    // others need to be performed ('STAND_ALONE'), or whether additional
    // transitions are needed ('CONDITIONAL') (see 'MandTypeSetting.cc').
    enum TransitionState {STAND_ALONE, CONDITIONAL};

    virtual TransitionState  testTransition(
				     const SettingId::SettingIdType settingId,
				     const DiscreteValue            newValue,
				     const DiscreteValue            currValue
					   );
    virtual void             acceptTransition(
				     const SettingId::SettingIdType settingId,
				     const DiscreteValue            newValue,
				     const DiscreteValue            currValue
					   ) = 0;

    //@ Type:  ChangeState
    // a Setting can be in only one of these states at a given time
    enum ChangeState {NON_CHANGING, IS_INCREASING, IS_DECREASING};

	// change requested through GUI serial interface
	virtual SigmaStatus ventset(const char* requestedValue);

#if defined(SIGMA_DEVELOPMENT)
    virtual Boolean  isAcceptedValid(void) const = 0;
    virtual Boolean  isAdjustedValid(void)       = 0;
#endif  // defined(SIGMA_DEVELOPMENT)

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL, 
			   const char*       pPredicate = NULL);

    //@ Type:  SettingIdAndValue
    // A structure to store the id and value of an individual setting.
    struct SettingIdAndValue
    {
      SettingId::SettingIdType  settingId;
      SettingValue              settingValue;
    };

    //@ Type:  SoftBoundInfo
    // Struct storing the overridden state a particular soft bound, for
    // this setting.
    struct SoftBoundInfo
    {
      SettingBoundId  boundId;
      Boolean         isActive;
    };

    inline void setDisableNovRamUpdate(Boolean isNovRamUpdateDisabled);

    inline Boolean isNovRamUpdateDisabled(void);


  protected:
    //@ Type:  SoftBoundType_
    // Type of soft bound (upper or lower) stored.
    enum SoftBoundType_
    {
      LOWER,
      UPPER
    };

    Setting(const SettingId::SettingIdType  settingId,
	    const SettingId::SettingIdType* arrDependentSettingIds);

    inline BoundStatus&  getBoundStatus_(void);

    virtual Boolean  calcDependentValues_(const Boolean isValueIncreasing);

    virtual void  updateConstraints_(void);

    virtual Boolean  resolveDependentChange_(
			const SettingId::SettingIdType dependentId,
			const Boolean                  isValueIncreasing
					    );

    static inline void StoreActivatedSetting_(
    				const SettingId::SettingIdType settingId,
				const SettingValue&            settingValue
					     );

    static inline SettingId::SettingIdType  GetStoredSettingId_   (void); 
    static inline SettingValue              GetStoredSettingValue_(void); 

    void     registerSoftBound_(const SettingBoundId          boundId,
				const Setting::SoftBoundType_ boundType);
    Boolean  isSoftBoundActive_(const SettingBoundId boundId) const;
    void     activateSoftBound_(const SettingBoundId boundId);
    void     deactivateSoftBound_(const SettingBoundId boundId);

    void  updateAllDependentsSoftBoundStates_(const Boolean isResetting);

    void  overrideAllSoftBoundStates_(void);
    void  updateAllSoftBoundStates_(const ChangeState changeState);

    virtual void  findSoftMinMaxValues_(Real32& rSoftMinValue,
					Real32& rSoftMaxValue) const;

    virtual Real32  getAbsoluteMaxValue_(void) const;
    virtual Real32  getAbsoluteMinValue_(void) const;

#if defined(SIGMA_DEVELOPMENT)
    virtual Ostream& print_(Ostream&) const = 0;
#endif // defined(SIGMA_DEVELOPMENT)

    //@ Constant:  ARR_DEPENDENT_SETTING_IDS_
    // Constant pointer to a null-id-terminated array of constant setting ids,
    // each of which identifies a dependent setting of this setting.
    const SettingId::SettingIdType *const  ARR_DEPENDENT_SETTING_IDS_;

    //@ Data-Member:  NULL_DEPENDENT_ARRAY_
    // Constant pointer to an empy array of dependent setting ids.  This array
    // is provided for use by all of the settings that have no dependent settings.
    static const SettingId::SettingIdType  NULL_DEPENDENT_ARRAY_[];

  private:
    Setting(const Setting&);            // not implemented...
    Setting(void);                      // not implemented...
    void  operator=(const Setting&);    // not implemented...

    const SoftBoundInfo*  findSoftBoundInfo_(const SettingBoundId boundId) const;

    //@ Data-Member:  RStoredSetting_
    // A storage for setting values that are being adjusted, and that may
    // need to be reset.
    static SettingIdAndValue&  RStoredSetting_;

    //@ Data-Member:  boundStatus_
    // Bound violation status for this setting.
    BoundStatus  boundStatus_;  

    //@ Data-Member:  lowerSoftBoundInfo_
    // Stores information of this setting's lower soft bound.
    SoftBoundInfo  lowerSoftBoundInfo_;

    //@ Data-Member:  upperSoftBoundInfo_
    // Stores information of this setting's upper soft bound.
    SoftBoundInfo  upperSoftBoundInfo_;

    //@ Data-Member:  isNovRamUpdateDisabled_
    // This flag prevents a setting id to be saved into novram.
	Boolean isNovRamUpdateDisabled_;

};

// Inlined methods
#include "Setting.in"


#endif // Setting_HH 
