
#ifndef ApneaScheduler_HH
#define ApneaScheduler_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: ApneaScheduler - This scheduler is responsible for handling
// breathing during apnea.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/ApneaScheduler.hhv   25.0.4.0   19 Nov 2013 13:59:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 005  By:  yyy    Date: 06-Jan-1998     DR Number:
//       Project:  BiLevel (R8027)
//       Description:
//          Initial release for Bilevel.
//
//  Revision: 004  By:  iv    Date:  07-Feb-1997    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Rework per Breath Delivery formal code review.
//
//  Revision: 003 By:  iv   Date:   21-Feb-1996    DR Number: DCS 674 
//       Project:  Sigma (R8027)
//       Description:
//             Removed #ifndef directives surrounding #include directives.
//
//  Revision: 002  By:  kam   Date:  27-Sep-1995    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Updated for BD-GUI user event request and event status interface.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//====================================================================


#  include "Sigma.hh"
#  include "Breath_Delivery.hh"
#include "BreathType.hh"
#include "BreathPhaseScheduler.hh"

//@ Usage-Classes
class ModeTrigger;
class BreathPhase;
//@ End-Usage

class ApneaScheduler: public BreathPhaseScheduler
{
  public:
	ApneaScheduler(void);
	virtual ~ApneaScheduler(void);
  
	static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL, 
			  const char*       pPredicate = NULL);

	virtual void determineBreathPhase(const BreathTrigger& breathTrigger);
	virtual void relinquishControl(const ModeTrigger& modeTrigger);
	virtual void takeControl(const BreathPhaseScheduler& scheduler);

  protected:
	virtual EventData::EventStatus reportEventStatus_(const EventData::EventId id,
													 const Boolean eventStatus);
	virtual void settingChangeHappened_(const SettingId::SettingIdType id);
	virtual void enableTriggers_(void);
	
  private:
    ApneaScheduler(const ApneaScheduler&);    // Declared but not implemented
    void operator=(const ApneaScheduler&); // Declared but not implemented

	void startInspiration_(const BreathTrigger& breathTrigger, BreathPhase* pBreathPhase, BreathType breathType);
	void startExhalation_(const BreathTrigger& breathTrigger,
	                      BreathPhase* pBreathPhase,
	                      const Boolean noInspiration = FALSE);
	
	//@ Data-Member: breathCycleTime_
	// The duration of the apnea breath cycle, in msec
	Int32 breathCycleTime_;

};


#endif // ApneaScheduler_HH 
