#include "stdafx.h"

#if defined(SIGMA_BD_CPU)  ||  defined(SIGMA_COMMON)

//======================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//======================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: PendingContext - Context for Settings Pending Phase-In.
//---------------------------------------------------------------------
//@ Interface-Description
//  A context is a storage pool for different kinds of setting values.  The
//  settings, themselves, do not contain their values, the contexts do.  This
//  context is used for managing the Breath-Delivery's pending settings.
//
//  This context processes the received transmission blocks, from a Settings'
//  Transaction, and provides methods that are used by the Phased-In Context
//  to update its setting values.  The update of the Phased-In Context is a
//  "pull", that is, it is initiated from the Phased-In Context.  Therefore,
//  this class provides methods for access to the pending flags and values.
//  This class also has callbacks that can be registered with it for
//  notification of individual settings changes.  Whenever ANY transmission
//  of settings is received, the pending values and flags of this context
//  are updated to reflect the update.  All value changes activate the
//  corresponding setting's callback, if any.  When an "vent-startup"
//  transmission is received while there are no setting values available
//  from NOVRAM, the Vent-Startup-Pending Flag is set for that batch of
//  pending values.
//
//  This class is available on the Breath-Delivery CPU, only.
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for the Pending Context.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class contains instances of 'BdSettingValues' which manages all
//  of the "pending" setting values.  This class also contains instances
//  of the bit array class, where each bit in the arrays corresponds to a
//  specific setting, indicating whether, or not, that setting is currently
//  pending.  Finally, a boolean flag is used to determine whether the
//  settings that are currently pending are part of a Vent-Startup update.
//  These data members are double buffered to protect "read" access during
//  updates.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/PendingContext.ccv   25.0.4.0   19 Nov 2013 14:27:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  using SafetyPcvSettingValue's new, generic interface
//	*  using new, BD-only 'SettingCallbackMgr' class
//
//  Revision: 003   By: sah  Date:  11-Jan-1999    DR Number:  5321
//  Project:  ATC
//  Description:
//	As part of making all 'SoftFault()' methods non-inlined, I'm
//	also making those 'SoftFault()' methods that are NOT called
//	in PRODUCTION mode, DEVELOPMENT-only.
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//  
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//=====================================================================

#include "PendingContext.hh"

#if defined(SIGMA_DEVELOPMENT)
#include "Ostream.hh"
#endif  // defined(SIGMA_DEVELOPMENT)

//@ Usage-Classes
#include "ContextMgr.hh"
#include "PhasedInContext.hh"
#include "NovRamManager.hh"
#include "SafetyPcvSettingValues.hh"
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Static Member Definitions...
//
//=====================================================================

SettingId::SettingIdType PendingContext::ArrPendingIds_[::NUM_BD_SETTING_IDS];

Uint  PendingContext::NumPendingIds_;


//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  PendingContext()  [Default Constructor]
//
//@ Interface-Description
//  Construct a default pending phase-in context.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

PendingContext::PendingContext(void)
{
  CALL_TRACE("PendingContext()");

  arrPendingInfo_[0].ventStartupPendingFlag = FALSE;
  arrPendingInfo_[0].pPendingBdValues       = &pendingBdValues0_;
  arrPendingInfo_[0].pPendingFlags          = &pendingFlags0_;

  arrPendingInfo_[1].ventStartupPendingFlag = FALSE;
  arrPendingInfo_[1].pPendingBdValues       = &pendingBdValues1_;
  arrPendingInfo_[1].pPendingFlags          = &pendingFlags1_;

  currReadBuffer_ = 0u;

  //===================================================================
  // initialization of the BD setting values...
  //===================================================================

  // get an alias to the "read" buffer's value manager class...
  BdSettingValues&  rPendingValues =
		    *(arrPendingInfo_[currReadBuffer_].pPendingBdValues);
  	

  if (ContextMgr::GetPhasedInContext().arePatientSettingsAvailable())
  {   // $[TI1]
    // a Vent-Startup had previously completed, therefore there are BD
    // setting values in NOVRAM; copy the BD setting values from NOVRAM
    // into the "read" buffer's BD value manager instance within the
    // Pending Context; no callbacks are activated...
    NovRamManager::GetAcceptedBatchSettings(rPendingValues);
  }
  else
  {   // $[TI2] -- no persistent BD values; initialized to default values...
    Uint  idx;

    for (idx  = SettingId::LOW_BATCH_BD_BOUNDED_ID_VALUE;
	 idx <= SettingId::HIGH_BATCH_BD_BOUNDED_ID_VALUE; idx++)
    {
      const SettingId::SettingIdType  SETTING_ID =
					  (SettingId::SettingIdType)idx;

      rPendingValues.setBoundedValue(SETTING_ID,
		   BoundedValue(SafetyPcvSettingValues::GetValue(SETTING_ID)));
    }

    for (idx  = SettingId::LOW_BATCH_BD_DISCRETE_ID_VALUE;
	 idx <= SettingId::HIGH_BATCH_BD_DISCRETE_ID_VALUE; idx++)
    {
      const SettingId::SettingIdType  SETTING_ID =
					  (SettingId::SettingIdType)idx;

      rPendingValues.setDiscreteValue(SETTING_ID,
		 DiscreteValue(SafetyPcvSettingValues::GetValue(SETTING_ID)));
    }
  }

  // register 'RegisterSettingReceipt_()' to be activated with changes to
  // 'pendingBdValues0_', which occur when a transmission of settings is
  // being received to Buffer #0...
  pendingBdValues0_.registerCallback(
  				 &PendingContext::RegisterSettingReceipt_
				    );

  // register 'RegisterSettingReceipt_()' to be activated with changes to
  // 'pendingBdValues1_', which occur when a transmission of settings is
  // being received to Buffer #1...
  pendingBdValues1_.registerCallback(
  				 &PendingContext::RegisterSettingReceipt_
				    );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~PendingContext(void)  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this pending phase-in context client.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

PendingContext::~PendingContext(void)
{
  CALL_TRACE("~PendingContext()");
}


#  if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  setBoundedValue(boundedId, newValue)
//
// Interface-Description
//  Set the value of the bounded setting indicated by 'boundedId' to
//  'newValue'.  This activates the callbacks, if any.
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//  (SettingId::IsBdBoundedId(boundedId))
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

void
PendingContext::setBoundedValue(const SettingId::SettingIdType boundedId,
			        const BoundedValue&            newValue)
{
  CALL_TRACE("setBoundedValue(boundedId, newValue)");
  SAFE_CLASS_PRE_CONDITION((SettingId::IsBdBoundedId(boundedId)));

  // store the new value...
  (getReadBufferPtr_()->pPendingBdValues)->setBoundedValue(boundedId,
  							   newValue);

  // set this setting's pending flag...
  (getReadBufferPtr_()->pPendingFlags)->setBit(boundedId);

  // activate the callback, if any, that is registered for this setting...
  SettingCallbackMgr::ActivateSettingCallback(boundedId);
}


//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  setDiscreteValue(discreteId, newValue)
//
// Interface-Description
//  Set the value of the discrete setting indicated by 'discreteId' to
//  'newValue'.  This activates the callbacks, if any.
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//  (SettingId::IsBdDiscreteId(discreteId))
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

void
PendingContext::setDiscreteValue(const SettingId::SettingIdType discreteId,
			         const DiscreteValue            newValue)
{
  CALL_TRACE("setDiscreteValue(discreteId, newValue)");
  SAFE_CLASS_PRE_CONDITION((SettingId::IsBdDiscreteId(discreteId)));

  // store the new value...
  (getReadBufferPtr_()->pPendingBdValues)->setDiscreteValue(discreteId,
  							    newValue);

  // set this setting's pending flag...
  (getReadBufferPtr_()->pPendingFlags)->setBit(discreteId);

  // activate the callback, if any, that is registered for this setting...
  SettingCallbackMgr::ActivateSettingCallback(discreteId);
}

#  endif // defined(SIGMA_DEVELOPMENT)


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  processTransmission(initXactionId, xmittedBdValues)  [static]
//
//@ Interface-Description
//  Receive the transmitted BD setting values, and incorporate them into
//  this context.
//---------------------------------------------------------------------
//@ Implementation-Description
// $[02034] -- Phase-in while a previously-accepted setting, has not yet 
//             been phased-in, the common settings shall be merged before
//             the phase-in occurs.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
PendingContext::processTransmission(const SettingsXactionId initXactionId,
				    const BdSettingValues&  xmittedBdValues)
{
  CALL_TRACE("processTransmission(initXactionId, xmittedBdValues)");

  //=================================================================
  //  Initialize the UPDATE buffer values, based on the values of the
  //  READ buffer and the incoming message type.
  //=================================================================

  // reset the number of BD settings that have been received...
  PendingContext::NumPendingIds_ = 0u;

  // get pointers to both the UPDATE and READ buffers...
  const PendingContext::BufferedValues* pReadBuffer   = getReadBufferPtr_  ();
  PendingContext::BufferedValues*       pUpdateBuffer = getUpdateBufferPtr_();

  FixedBitArray(NUM_BD_SETTING_IDS)&  rUpdateBufferFlags =
  					  *(pUpdateBuffer->pPendingFlags);
  BdSettingValues&  rUpdateBufferValues = *(pUpdateBuffer->pPendingBdValues);

  pUpdateBuffer->ventStartupPendingFlag =
			 (pReadBuffer->ventStartupPendingFlag  ||
			  initXactionId == ::VENT_STARTUP_UPDATE_XMITTED);

  if (pUpdateBuffer->ventStartupPendingFlag)
  {   // $[TI1] -- this is a Vent-Startup transmission...
    // copy in all of the transmitted BD setting values; NO callbacks are
    // activated...
    rUpdateBufferValues = xmittedBdValues;

    // ALL settings need to be phased-in for a Vent-Startup, therefore set
    // ALL of the pending flags...
    rUpdateBufferFlags.setAllBits();
  }
  else
  {   // $[TI2] -- this is NOT a Vent-Startup transmission...
    // initialize the pending flags of the UPDATE buffer with the flags
    // of the READ buffer...
    rUpdateBufferFlags = *(pReadBuffer->pPendingFlags);

    // initialize the UPDATE values with the current READ values -- NO
    // callbacks...
    rUpdateBufferValues = *(pReadBuffer->pPendingBdValues);

    // copy in the CHANGED BD setting values; callbacks ARE activated...
    rUpdateBufferValues.copyFrom(xmittedBdValues);
  }

  // store the newly received accepted setting values into NOVRAM...
  NovRamManager::UpdateAcceptedBatchSettings(rUpdateBufferValues);

  // update persistent flag that indicates that settings are stored in
  // NOVRAM...
  NovRamManager::UpdateBatchSettingsFlag(TRUE);

  // "accept" the UPDATE buffer as the new READ buffer, thereby, completing
  // the reception of the transmission...
  acceptUpdateBuffer_();

  Uint  idx;

  if (isVentStartupPending())
  {   // $[TI3] -- Vent-Startup IS pending, therefore ALL callbacks...
    SAFE_CLASS_ASSERTION((rUpdateBufferFlags.areAllSet()));

    // update persistent flag that indicates that patient settings are
    // stored in NOVRAM; this must be done AFTER "accepting" the new
    // READ buffer due to BD's dependency, with respect to the phase-ins,
    // on this flag...
    NovRamManager::UpdatePatientSettingsFlag(TRUE);

    for (idx  = SettingId::LOW_BATCH_BD_ID_VALUE;
         idx <= SettingId::HIGH_BATCH_BD_ID_VALUE; idx++)
    {
      // activate the callback, if any, of the setting represented by
      // 'idx'...
      SettingCallbackMgr::ActivateSettingCallback(
					  (SettingId::SettingIdType)idx
						 );
    }  // end of for...
  }
  else
  {   // $[TI4] -- Vent-Startup is NOT pending, therefore CHANGED callbacks...
    for (idx = 0u; idx < PendingContext::NumPendingIds_; idx++)
    {
      // get an ID of a setting that was changed due to the transmission...
      const SettingId::SettingIdType  CHANGED_ID =
				       PendingContext::ArrPendingIds_[idx];

      // activate the callback, if any, of the setting represented by
      // 'CHANGED_ID'...
      SettingCallbackMgr::ActivateSettingCallback(CHANGED_ID);
    }  // end of for...
  }
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
PendingContext::SoftFault(const SoftFaultID  softFaultID,
			  const Uint32       lineNumber,
			  const char*        pFileName,
			  const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
                          PENDING_CONTEXT, lineNumber, pFileName,
                          pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)


//=====================================================================
//
//  Private Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  RegisterSettingReceipt_()  [static]
//
//@ Interface-Description
//  This callback is activated every time any of the pending setting values
//  is changed.  When activated, this method will set the pending flag for
//  the setting that's changed.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (settingId <= SettingId::HIGH_BATCH_BD_ID_VALUE  &&
//   settingId >= SettingId::LOW_BATCH_BD_ID_VALUE)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
PendingContext::RegisterSettingReceipt_(
				    const SettingId::SettingIdType settingId
				       )
{
  CALL_TRACE("RegisterSettingReceipt_(settingId)");
  SAFE_CLASS_PRE_CONDITION((settingId <= SettingId::HIGH_BATCH_BD_ID_VALUE  &&
			    settingId >= SettingId::LOW_BATCH_BD_ID_VALUE));

  PendingContext&  rPendingContext = ContextMgr::GetPendingContext();

  // set the "pending" flag for the received setting in the "UPDATE"
  // buffer...
  (rPendingContext.getUpdateBufferPtr_()->pPendingFlags)->setBit(settingId);

  // store 'settingId' as an id of a BD setting value that was just
  // changed...
  PendingContext::ArrPendingIds_[PendingContext::NumPendingIds_++] =
								settingId;

#  if defined(SIGMA_DEVELOPMENT)
  if (Settings_Validation::IsDebugOn(::PENDING_CONTEXT))
  {
    cout << "Received PENDING Setting:" << endl;
    cout << "  Name:   " << SettingId::GetSettingName(settingId)
	 << endl;
    cout << "  Value:  ";

    const BdSettingValues&  rPendingBdValues =
		*(rPendingContext.getUpdateBufferPtr_()->pPendingBdValues);

    if (SettingId::IsBoundedId(settingId))
    {
      cout << rPendingBdValues.getBoundedValue(settingId);
    }
    else if (SettingId::IsDiscreteId(settingId))
    {
      cout << rPendingBdValues.getDiscreteValue(settingId);
    }

    cout << endl;
  }
#  endif // defined(SIGMA_DEVELOPMENT)
}   // $[TI1]


#endif // defined(SIGMA_BD_CPU)  ||  defined(SIGMA_COMMON)
