#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1995, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
// Class: ExitServiceModeSubScreen - Activated by selecting Exit Tab button
// at ServiceLowerSelectArea to exit Service mode.
//---------------------------------------------------------------------
//@ Interface-Description
// Only one instance of this class is created by LowerSubScreenArea.  The
// interface is pretty generic, as a SubScreen it defines the activate()
// and deactivate() methods and then receives button, adjustPanel events
// through via the usual means (i.e., the buttonDownHappened(),
// buttonUpHappened(), adjustPanelAcceptPressHappened(),
// adjustPanelRestoreFocusHappened(),  methods).  For conducting test it
// uses processTestPrompt(), processTestKeyAllowed(), processTestResultStatus()
// processTestData() methods to display the status and result of the test.
//---------------------------------------------------------------------
//@ Rationale
// This is a class which implements the exiting of the  Service mode.
//---------------------------------------------------------------------
//@ Implementation-Description
// For exiting Service mode, the confirmation of the user action is needed
// through the adjustPanelAcceptPressHappened().  For exiting from the SST
// no user confirmation is required.
// It waits for test result data and test commands received from Service-Data
// and and dispatched to the virtual methods processTestPrompt(),
// processTestKeyAllowed(), processTestResultStatus(), processTestResultCondition()
// and processTestData().  
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and
// pre-conditions to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
// Only one instance of this class should be created.  It should only be
// displayed in the LowerSubScreenArea.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/ExitServiceModeSubScreen.ccv   25.0.4.0   19 Nov 2013 14:07:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By: srp   Date:  24-Apr-2002    DR Number: 5327 & 5908
//  Project:  NeoMode
//  Description:
//      Added Modification log. Updated for NeoMode  03/01/00
//
//  Revision: 003  By: sah    Date:  07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 002  By: gdc      Date: 23-Sep-1997  DR Number: 2513
//    Project:  Sigma (R8027)
//    Description:
//    Cleaned up Service-Mode interfaces to support remote test.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "ExitServiceModeSubScreen.hh"

#if defined FAKE_SM
#	include "FakeServiceModeManager.hh"
#   define SmManager FakeServiceModeManager
#else		
#	include "SmManager.hh"
#endif	// FAKE_SM

//@ Usage-Classes
#include "AdjustPanel.hh"
#include "PromptStrs.hh"
#include "PromptArea.hh"
#include "SubScreenArea.hh"
#include "GuiApp.hh"
//@ End-Usage

//@ Code...

// Initialize static constants.

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ExitServiceModeSubScreen()  [Default Constructor]
//
//@ Interface-Description
// Creates the ExitServiceMode subscreen.  The given `pSubScreenArea' is the
// SubScreenArea in which it will be displayed (in this case the lower
// subscreen area).
//---------------------------------------------------------------------
//@ Implementation-Description
// Sets positions and size of this subscreen.  Also, set up the Test Result
// Data array and Test Command array.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

ExitServiceModeSubScreen::ExitServiceModeSubScreen(SubScreenArea *pSubScreenArea) :
                SubScreen(pSubScreenArea),
                testManager_(this)
{
	CALL_TRACE("SstSetupSubScreen::SstSetupSubScreen(pSubScreenArea)");

	// Size and position the sub-screen
	setX(0);
	setY(0);
	setWidth(SERVICE_UPPER_SUB_SCREEN_AREA_WIDTH);
	setHeight(SERVICE_UPPER_SUB_SCREEN_AREA_HEIGHT);

	// Associate the testResults object with the pointer to the test result
	// array.
	testManager_.setupTestResultDataArray();
	testManager_.setupTestCommandArray();
											// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~ExitServiceModeSubScreen  [Destructor]
//
//@ Interface-Description
// Destroys the ExitServiceMode subscreen.  Needs to do nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

ExitServiceModeSubScreen::~ExitServiceModeSubScreen(void)
{
	CALL_TRACE("ExitServiceModeSubScreen::~ExitServiceModeSubScreen(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activate
//
//@ Interface-Description
// For Service mode, it is called by Service LowerSubScreenArea just
// before this subscreen is displayed.   For SST mode, it is called by
// SstTestSubScreen before this subscren is displayed. No parameters needed.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The first thing to do is to register all general possible test results
//  and commands for test events.  Then we inform the SmManager that we
//  are about to conducting a Misc type of test.  Appropriated prompt shll
//  be displayed.  For SST, we start the test which is to reboot BD.  For
//  Service Mode, we grep the adjustPanelTarget and wait for user to
//  confirm to start the test.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ExitServiceModeSubScreen::activate(void)
{
	CALL_TRACE("ExitServiceModeSubScreen::activate(void)");

	//
	// Register all general possible test results for test events, but first
	// we have to initialize ServiceDataRegistrar.
	//
	testManager_.registerTestResultData();
	testManager_.registerTestCommands();

	// Let the service mode  manager knows the type of Service mode.
	SmManager::DoFunction(SmTestId::SET_TEST_TYPE_MISC_ID);

#if defined SERVICE_FORNOW
	// Grab Adjust Panel focus
	AdjustPanel::TakeNonPersistentFocus(this, TRUE);

	//
	// Set all prompts 
	//
	testManager_.setTestPrompt(PromptArea::PA_PRIMARY, 
						PromptArea::PA_HIGH,
						PromptStrs::SST_PRESS_ACCEPT_TO_CONFIRM_P);
	testManager_.setTestPrompt(PromptArea::PA_ADVISORY, 
						PromptArea::PA_HIGH,
						NULL_STRING_ID);
	testManager_.setTestPrompt(PromptArea::PA_SECONDARY,
						PromptArea::PA_HIGH,
						PromptStrs::EXIT_SERVICE_CANCEL_S);

#elif defined SST_FORNOW
	//
	// Set all prompts 
	//
	testManager_.setTestPrompt(PromptArea::PA_PRIMARY, 
						PromptArea::PA_HIGH,
						PromptStrs::SST_PLEASE_WAIT_P);
	testManager_.setTestPrompt(PromptArea::PA_ADVISORY, 
						PromptArea::PA_HIGH,
						NULL_STRING_ID);
	testManager_.setTestPrompt(PromptArea::PA_SECONDARY,
						PromptArea::PA_HIGH,
						NULL_STRING_ID);

	startTest_(SmTestId::RESTART_BD_ID);

#else
	if (GuiApp::GetGuiState() == STATE_SERVICE)
	{								// $[TI1]	
		// Grab Adjust Panel focus
		AdjustPanel::TakeNonPersistentFocus(this, TRUE);

		//
		// Set all prompts 
		//
		testManager_.setTestPrompt(PromptArea::PA_PRIMARY, 
							PromptArea::PA_HIGH,
							PromptStrs::SST_PRESS_ACCEPT_TO_CONFIRM_P);
		testManager_.setTestPrompt(PromptArea::PA_ADVISORY, 
							PromptArea::PA_HIGH,
							NULL_STRING_ID);
		testManager_.setTestPrompt(PromptArea::PA_SECONDARY,
							PromptArea::PA_HIGH,
							PromptStrs::EXIT_SERVICE_CANCEL_S);
		}
	else if (GuiApp::GetGuiState() == STATE_SST)
	{								// $[TI2]	
		//
		// Set all prompts 
		//
		testManager_.setTestPrompt(PromptArea::PA_PRIMARY, 
							PromptArea::PA_HIGH,
							PromptStrs::SST_PLEASE_WAIT_P);
		testManager_.setTestPrompt(PromptArea::PA_ADVISORY, 
							PromptArea::PA_HIGH,
							NULL_STRING_ID);
		testManager_.setTestPrompt(PromptArea::PA_SECONDARY,
							PromptArea::PA_HIGH,
							NULL_STRING_ID);

		startTest_(SmTestId::RESTART_BD_ID);
	}
	else
	{
		CLASS_ASSERTION_FAILURE();
	}
#endif	// SERVICE_FORNOW
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: deactivate
//
//@ Interface-Description
// Called by LowerSubScreenArea just after this subscreen has been
// removed from the screen.  No parameters are needed.
//---------------------------------------------------------------------
//@ Implementation-Description
// We perform any necessary cleanup. Clear the Prompt Area of any prompts
// that we may have set.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ExitServiceModeSubScreen::deactivate(void)
{
	CALL_TRACE("ExitServiceModeSubScreen::deactivate(void)");

	// Make sure that nothing in this subscreen keeps focus.
	AdjustPanel::TakeNonPersistentFocus(NULL, TRUE);

	// Clear the Primary prompt
	testManager_.setTestPrompt(PromptArea::PA_PRIMARY,
		 PromptArea::PA_HIGH, NULL_STRING_ID);

	testManager_.setTestPrompt(PromptArea::PA_PRIMARY,
		 PromptArea::PA_LOW, NULL_STRING_ID);

	// Clear the Advisory prompts
	testManager_.setTestPrompt(PromptArea::PA_ADVISORY,
		 PromptArea::PA_HIGH, NULL_STRING_ID);

	// Clear the Advisory prompts
	testManager_.setTestPrompt(PromptArea::PA_ADVISORY,
		 PromptArea::PA_LOW, NULL_STRING_ID);

	// Clear the Secondary prompt
	testManager_.setTestPrompt(PromptArea::PA_SECONDARY,
		PromptArea::PA_HIGH, NULL_STRING_ID);

	// Clear the Secondary prompt
	testManager_.setTestPrompt(PromptArea::PA_SECONDARY,
		PromptArea::PA_LOW, NULL_STRING_ID);
											// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelRestoreFocusHappened
//
//@ Interface-Description
// This is a virtual method inherited from being an AdjustPanelTarget.
// It is normally called when the operator release the off-screen
// keyboard key and the AdjustPanel needs to restore the default
// AdjustPanel target.  It is the duty of this inherited method to restore
// the previous test prompts in order to continue the Calibration
// process.  
//---------------------------------------------------------------------
//@ Implementation-Description
//  Call GuiManager's virtual method to do the job.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ExitServiceModeSubScreen::adjustPanelRestoreFocusHappened(void)
{
	CALL_TRACE("ExitServiceModeSubScreen::adjustPanelRestoreFocusHappened(void)");

#if defined FORNOW
printf("adjustPanelRestoreFocusHappened at line  %d\n", __LINE__);
#endif	// FORNOW

	testManager_.restoreTestPrompt();
											// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelAcceptPressHappened
//
//@ Interface-Description
//  This is a virtual method inherited from being an AdjustPanelTarget.  It is
//  normally called when the operator presses down the Accept key to signal the
//  accepting of continuing the test process.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Simplily set the appropriate prompts, followed by invoking rebooting
//  BD.
// $[07002] Upon exiting Service mode, the ventilatio must execute ...
// $[07042] This subscreen allows the user to exit from Service Mode ...
// $[07043] When the user presses the Accept key, a system reset ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ExitServiceModeSubScreen::adjustPanelAcceptPressHappened(void)
{
	CALL_TRACE("ExitServiceModeSubScreen::adjustPanelAcceptPressHappened(void)");

	// Set all prompts
	testManager_.setTestPrompt(PromptArea::PA_PRIMARY,
				PromptArea::PA_HIGH, NULL_STRING_ID);

	testManager_.setTestPrompt(PromptArea::PA_PRIMARY,
				PromptArea::PA_LOW, PromptStrs::EST_TESTING_P);

	startTest_(SmTestId::RESTART_BD_ID);
											// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: startTest_
//
//@ Interface-Description
//	Preparation step for a test to start.	
//---------------------------------------------------------------------
//@ Implementation-Description
//	Simplily tell Service Mode Manager to do the test.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ExitServiceModeSubScreen::startTest_(SmTestId::ServiceModeTestId currentTestId)
{
	CALL_TRACE("ExitServiceModeSubScreen::startTest_(SmTestId::ServiceModeTestId currentTestId)");

#if defined FORNOW
printf("startTest_ at line  %d\n", __LINE__);
#endif	// FORNOW

	currentTestId_ = currentTestId;

	// Start the test.
#if defined FAKE_SM
printf("DoTest, currentTestId = %d\n", currentTestId);
#endif	// FAKE_SM

	SmManager::DoTest(currentTestId);
											// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: nextTest_
//
//@ Interface-Description
//	Handles the test and interface control at the end of a test.
//---------------------------------------------------------------------
//@ Implementation-Description
//	The control differs depending what "test" was done prior to this
//	method.  If it is a BD test, then we shall invoke the GUI reboot.
// $[07002] Upon exiting Service mode, the ventilatio must execute ...
//---------------------------------------------------------------------
//@ Implementation-Description
//  None.
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ExitServiceModeSubScreen::nextTest_(void)
{
	CALL_TRACE("ExitServiceModeSubScreen::nextTest_(void)");

#if defined FORNOW
printf("nextTest_ at line  %d\n", __LINE__);
#endif	// FORNOW

	if (currentTestId_ == SmTestId::RESTART_BD_ID)
	{										// $[TI1]
		startTest_(SmTestId::RESTART_GUI_ID);
	}	  									// $[TI2]
}


//============================ m e t h o d   d e s c r i p t i o n ====
//@ Method: processTestPrompt_
//
//@ Interface-Description
//  Process the prompt command given by Service-Mode.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Should not receive any prompt.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ExitServiceModeSubScreen::processTestPrompt(Int command)
{
	CALL_TRACE("ExitServiceModeSubScreen::processTestPrompt(Int command)");

	CLASS_ASSERTION_FAILURE();
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processTestKeyAllowed
//
//@ Interface-Description
//  Process keyAllowed command given by Service-Mode.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Should not receive any keyAllowed command .
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ExitServiceModeSubScreen::processTestKeyAllowed(Int command)
{
	CALL_TRACE("ExitServiceModeSubScreen::processTestKeyAllowed(Int command)");

	CLASS_ASSERTION_FAILURE();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processTestResultStatus
//
//@ Interface-Description
//  Process the test result status given by Service Mode. 
//---------------------------------------------------------------------
//@ Implementation-Description
//  Dispatch the given command and make the corresponding response.
//---------------------------------------------------------------------
//@ PreCondition
// The given `resultStatus' must be valid.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ExitServiceModeSubScreen::processTestResultStatus(Int resultStatus, Int testResultId)
{
	CALL_TRACE("ExitServiceModeSubScreen::processTestResultStatus_"
								"(Int resultStatus Int testResultId)");


#if defined FORNOW
printf("ExitServiceModeSubScreen::processTestResultStatus() at line  %d, this = %d\n", __LINE__, this);
#endif	// FORNOW

	switch (resultStatus)
	{
	case SmStatusId::TEST_END:			// $[TI1]
		nextTest_();
		break;

	case SmStatusId::TEST_START:		// $[TI2]
		break;

	case SmStatusId::TEST_ALERT:	
	case SmStatusId::TEST_FAILURE:
		CLASS_ASSERTION_FAILURE();
		break;

	case SmStatusId::NOT_INSTALLED:

	default:
		CLASS_ASSERTION_FAILURE();
		break;
	}
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processTestResultCondition
//
//@ Interface-Description
//  Process test result condition passed by Service-Mode.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Check for the proper condition code.
//---------------------------------------------------------------------
//@ PreCondition
//  Result condition has to be greater than 0.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ExitServiceModeSubScreen::processTestResultCondition(Int resultCondition)
{
	CALL_TRACE("ExitServiceModeSubScreen::processTestResultCondition(Int resultCondition)");


#if defined FORNOW
printf("ExitServiceModeSubScreen::processTestResultCondition() at line  %d, this = %d\n", __LINE__, this);
#endif	// FORNOW

	if (resultCondition > 0)
	{  			// $[TI1]
		CLASS_ASSERTION_FAILURE();
	}  			// $[TI2]
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processTestData
//
//@ Interface-Description
//  Process data given by Service-Mode
//---------------------------------------------------------------------
//@ Implementation-Description
//  Since this subscreen is designed not to display test data,
//  this is never to be called.
//---------------------------------------------------------------------
//@ PreCondition
//  CLASS_ASSERTION_FAILURE()
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ExitServiceModeSubScreen::processTestData(Int dataIndex, TestResult *pResult)
{
	CALL_TRACE("ExitServiceModeSubScreen::processTestData(Int dataIndex, TestResult *pResult)");

#if defined FORNOW
printf("ExitServiceModeSubScreen::processTestData() at line  %d, this = %d\n", __LINE__, this);
#endif	// FORNOW

	CLASS_ASSERTION_FAILURE();
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
ExitServiceModeSubScreen::SoftFault(const SoftFaultID  softFaultID,
							 const Uint32       lineNumber,
							 const char*        pFileName,
							 const char*        pPredicate)  
{
	CALL_TRACE("ExitServiceModeSubScreen::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS,
							EXITSERVICEMODESUBSCREEN,
							lineNumber, pFileName, pPredicate);
}
