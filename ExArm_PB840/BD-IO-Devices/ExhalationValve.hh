
#ifndef ExhalationValve_HH
#define ExhalationValve_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
//  Class: ExhalationValve - Exhalation valve driver for command and
//         feedback.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/ExhalationValve.hhv   25.0.4.0   19 Nov 2013 13:54:04   pvcs  $
//
//@ Modification-Log
//
//  Revision: 007  By: sah    Date: 13-Jan-1999   DR Number: 5321, 5322
//  Project:  ATC
//  Description:
//		ATC initial release.
//      Changed 'SoftFault()' method to non-inlined method.
//		Added SLOWEST.  Delete SLOW and QUICK.
//
//	  Revision: 006 By: syw    Date: 02-May-1996   DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Change invertedTable_ to accept current counts as an input instead
//			of DAC counts which required changing the size of the
//			inverted table.  Added GUI CPU specific code.
//
//  Revision: 005 By: syw    Date: 07-Mar-1996   DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Added invertedTable_ and access method getInvertedTableEntry() to
//			meet TUV design.  Added pre-processor to generate CPU specific
//			code.
//
//  Revision: 004 By: syw    Date: 20-Feb-1996   DR Number: DCS 674
//  	Project:  Sigma (R8027)
//		Description:
//			Removed #ifndef around #include.
//
//  Revision: 003  By: syw    Date:  12-Jan-1996    DR Number: DCS 562
//       Project:  Sigma (R8027)
//       Description:
//             Added setPExhValveTable() method.
//
//  Revision: 002  By: syw    Date:  05-Dec-1995    DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Changed rExhValveTable_ to pExhValveTable.  pExhValveTable shall
//			point to FLASH where the calibration information is stored.
//			Added data member pDampingGainTable_.  Added deenergize() method.
//			Removed setExhValveGainEntryTable() method.
//
//	Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: INITIAL RELEASE
//      Project:  Sigma (R8027)
//      Description:
//      	Initial version
//
//=====================================================================

#include "Sigma.hh"
#include "BD_IO_Devices.hh"
#include "ExhValveCalibrationTable.h"

//@ Usage-Classes

#include "ExhValveTable.hh"
#include "Psol.hh"

#include "LinearSensor.hh"

//@ End-Usage

#if defined (SIGMA_GUI_CPU)

const Uint32 INVERSE_TABLE_SIZE = 1024 ;		// 2.5 Amps max reading
typedef Real32 InvertedEVTable[INVERSE_TABLE_SIZE] ;

#endif // defined (SIGMA_GUI_CPU)

class ExhalationValve : public Psol {
  public:

#if defined (SIGMA_BD_CPU)

    // the damping mode of the exhalation valve.  The amount of damping to
    // apply is dependent on the damping mode.
    enum DampingMode { NORMAL, SLOWEST} ;

	ExhalationValve(ExhValveTable *pExhValveTable);

#else

    ExhalationValve( ExhValveTable *pExhValveTable) ;

#endif // defined (SIGMA_BD_CPU)

    ~ExhalationValve( void) ;

    static void SoftFault( const SoftFaultID softFaultID,
	   const Uint32        lineNumber,
	   const char*         pFileName  = NULL,
	   const char*         pPredicate = NULL) ;

#if defined (SIGMA_BD_CPU)

    //  Used by the calibration process to update the cal data
	void UpdateCalData(ExhValveTable::ExhValveCalTable &exhValveCalTable);

    void updateValve( Real32 pressureCommand, const DampingMode mode) ;   //used by PEEP controller
	void updateValveL2h( Real32 pressureCommand, const DampingMode mode) ;  //used by pressure controller
	void deenergize( void) ;

// TODO E600 VM: check later; enable if needed or delete if not
	Real32 getExhValveCurrent( void) const {return 0;} // TODO E600 BDIO
	Real32 getPdrvFromPexh(Real32 Pexp);
	Real32 getPdrvFromUdac(Real32 Udac);
	Real32 getUdacFromPdrv(Real32 Pdrv);
	void commandValve(Uint32 Udac);

    void setPExhValveTable( ExhValveTable *pExhValveTable)
    {
        pExhValveTable_ = pExhValveTable ;
    }

    inline Real32 getPressureCommand( void) const ;

#endif // defined (SIGMA_BD_CPU)

#if defined (SIGMA_GUI_CPU)

    inline Real32 getInvertedTableEntry( Uint16 index) const ;

#ifdef SIGMA_DEBUG
	void dumpInvertedTable( void) ;
#endif //SIGMA_DEBUG

#endif // defined (SIGMA_GUI_CPU)

  protected:

  private:
    // these methods are purposely declared, but not implemented...
    ExhalationValve( const ExhalationValve&) ;    // not implemented
    void   operator=( const ExhalationValve&) ;   // not implemented
    ExhalationValve( void) ; // default constructor, not implemented

    //@ Data-Member: pExhValveTable_
    // The exhalation valve table for temperature compensation
    // and damping gain
    ExhValveTable *pExhValveTable_ ;

#if defined (SIGMA_BD_CPU)

    //@ Data-Member: pressureCommand_
    // input command to the exhalation valve
    Real32 pressureCommand_ ;

	ExhValveTable::ExhValveCalTable CalValveTable;

#else

	//@ Data-Member: invertedTable_
	// to store inverted ExhValveTable
	InvertedEVTable invertedTable_ ;

#endif // defined (SIGMA_GUI_CPU)

} ;


// Inlined methods
#include "ExhalationValve.in"

#endif // ExhalationValve_HH
