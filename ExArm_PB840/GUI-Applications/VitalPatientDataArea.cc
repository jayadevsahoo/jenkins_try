#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-2007, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: VitalPatientDataArea - Upper screen area for vital pt data and misc messages.
//---------------------------------------------------------------------
//@ Interface-Description
// This class is a container, derived from the GUI-Foundation Container
// class.  This class contains textual fields, numeric fields, and
// boxes.  This class inherits Patient Data callbacks from the abstract
// PatientDataTarget class.  This class also inherits timer callbacks 
// via the abstract GuiTimerTarget class.
//
// The current display configuration for this area is determined by the
// setDisplayMode() method.  Nine configurations are possible:
// 1) MODE_NORMAL, normal operation mode where patient data values are
// displayed every breath,
// 2) MODE_VENT_STARTUP, for ventilator startup when the 'Vent Startup
// in progress' message is displayed,
// 3) MODE_WAITING_FOR_PT, waiting for patient mode,
// 4) MODE_NO_VENT, for no ventilation when we display the time that no
// ventilation has been active (we are passed 1 second tick events for this
// through the timerEventHappened() method),
// 5) MODE_MALFUNCTION when communications between GUI and BD is lost,
// 6) MODE_INOP when it is discovered that the BD goes to the Ventilator
// Inoperative state,
// 7) MODE_SERVICE_REQUIRED when flash is not set or serial number does
// not match,
// 8) MODE_SVO when safety valve open is detected,
// 9) MODE_INOP_NO_VENT, for no ventilation when we display the time that
// ventilation has been active but inoperative was delcared thereafter,
// (we are passed 1 second tick events for this through the timerEventHappened()
//  method).
//
// During normal operation, this area relies on the data provided by the
// Patient Data subsystem for the measured patient data values and breath
// status values (passed via the patientDataChangeHappened() method).  During
// system startup, the area is dependent on the Vent Startup code to set the
// proper system startup messages.
//---------------------------------------------------------------------
//@ Rationale
// For the normal GUI, this class is dedicated to managing the display
// of the topmost portion of the Upper Screen.  This area is not
// displayed during Service Mode since there is no patient data.
//---------------------------------------------------------------------
//@ Implementation-Description
// To support normal patient data displays, this area displays textual
// and numeric fields as necessary for the following measured data 
// parameters:
// >Von
//		1.	Total respiratory rate.
//		2.	End inspiratory pressure.
//		3.	Exhaled tidal volume.
//		4.	Exhaled minute volume.
//		5.	I:E ratio.
//		6.	Peak circuit pressure.
//		7.	End Expiratory pressure.
//		8.	Breath type (CONTROL, ASSIST, SPONT).
// >Voff
// The Patient Data subsystem notifies this area when updated values
// are available.  The numeric measured values are updated once per
// breath and the breath phase is updated when toggling between
// inspiration/expiration.  Each data field has the capability of
// hiding the numeric value when there is no data available or
// flashing the numeric value when the data reaches a built-in limit.
// All patient data drawables are added to a single container to allow
// the patient data to be added and removed to the screen in a single
// simple operation.
//
// There is a single TextField object used to display any of the large
// status messages.  The display switches between displaying this object
// and the patient data container as system events affecting the Vital
// Patient Data Area occur.
// 
// To support the "no vent" display mode, this area displays a
// static message indicating that no ventilation is occurring and an
// elapsed time display which is updated once per second for up to ten
// minutes.  After ten minutes, the elapsed time display is no longer
// updated and the time remains fixed.  The idea is to give the user
// an indication how long it has been since the system has not been
// able to deliver a breath.  The elapsed time display is updated via
// a timer callback which is set to fire once per second.
//---------------------------------------------------------------------
//@ Fault-Handling
// Checks for out-of-range enum values with assertions.
//---------------------------------------------------------------------
//@ Restrictions
// None.  In particular, there is no restriction in changing from one
// display mode to another at any time.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/VitalPatientDataArea.ccv   25.0.4.0   19 Nov 2013 14:08:44   pvcs  $
//
//@ Modification-Log
//   
//  Revision: 029   By: rhj    Date: 31-Mar-2011   SCR Number: 6746 
//  Project:  PROX
//  Description:
//      Calls the other prox-related subscreens to redraw itself.
// 
//  Revision: 028   By: rhj    Date: 22-Dec-2010   SCR Number: 6631
//  Project:  PROX
//  Description:
//      Display 840's volume data during PROX startup.
//  
//  Revision: 027   By: rhj    Date: 19-Aug-2010   SCR Number: 6631
//  Project:  PROX
//  Description:
//      Prevent displaying PROX data during a PROX startup.
// 
//  Revision: 026   By: mnr    Date: 26-Feb-2010   SCR Number: 6436
//  Project:  PROX
//  Description:
//      Duplicate include and Trending events for PROX enable/disable 
//      removed. Events moved to TrendDbEventReceiver.cc.
// 
//  Revision: 025   By: mnr    Date: 17-Feb-2010   SCR Number: 6436
//  Project:  PROX
//  Description:
//      Trending events for PROX enable/disable added.
//
//  Revision: 024   By: gdc    Date: 24-Sep-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal non-invasive CPAP.
//
//  Revision: 023  By: gdc     Date: 26-May-2007     SCR Number: 6330
//  Project:  Trend
//	Description:
//  	Removed SUN prototype code.
//
//  Revision: 022   By: gdc    Date:  09-May-2007    SCR Number: 6237
//  Project:  Trend
//  Description:
//      Modified for trending. Show/hide direct draw graphics to 
//  	accomodate modified handling in Drawable class.
//
//  Revision: 021   By: gdc    Date:  15-Feb-2005    SCR Number: 6144
//  Project:  NIV
//  Description:
//      Display Vti instead of PEEP during NIV.
//
//  Revision: 020   By: dph    Date:  04-Apr-2000    DCS Number: 5691
//  Project:  NeoMode
//  Description:
//      Added PEEP to vital patient data area.
//
//  Revision: 019   By: sah    Date:  19-Jul-1999    DCS Number: 5327
//  Project:  NeoMode
//  Description:
//      NeoMode Project changes:
//      *  reorganized VPDA, and swapped Pe-end with Pmean.
//      *  merged units into VPDS help messages
//		*  Use large fonts to display the patient data.
//
//  Revision: 018   By: gdc    Date:  29-Jun-1999    DCS Number: 5327
//  Project:  NeoMode
//  Description:
//      Optimized graphics library. Implemented Direct Draw mode for drawing
//      outside of BaseContainer refresh cycle.
//
//  Revision: 017  By: sah    Date: 27-Aug-99   DR Number: 5519
//  Project:  ATC
//  Description:
//      As part of adding the new symbols, I needed to adjust placement
//      of some text field placement.
//
//  Revision: 016  By: yyy    Date: 24-Aug-99   DR Number: 5481
//  Project:  ATC
//  Description:
//      Use BREATH_TYPE_DISPLAY_ITEM instead of BREATH_TYPE_ITEM when
//		working on breath bar task.
//
//  Revision: 015  By: yyy      Date: 08-Feb-1998  DR Number: 5274
//    Project:  BiLevel (R8027)
//    Description:
//     Changed numeric number fileds' space holder to meet the patient
//	   data menager's definition.
//
//  Revision: 014  By: yyy      Date: 05-Jan-1998  DR Number: 
//    Project:  BiLevel (R8027)
//    Description:
//     Initial release for BiLevel adding software option.
//     Merged /840/Baseline/GUI-Applications/vcssrc/VitalPatientDataArea.ccv   1.81.1.0   07/30/98 10:22:42   gdc
//
//  Revision: 013  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 012  By:  hhd	   Date:  06-Oct-97    DR Number: 2540, 2545. 
//    Project:  Sigma (R8027)
//    Description:
//		Restored old value of ROW2_VALUE_Y_, to fix DCS# 2545.
//		Removed code that supports commas in floating point format per DCS#  2540..
//
//  Revision: 011  By: hhd       Date: 06-Oct-1997    DCS Number: 2540
//  Project:  Sigma (R8027)
//  Description:
//		  Removed code that was changed per DCS 2321.
//
//  Revision: 010  By: sah       Date: 03-Oct-1997    DCS Number: 2264
//  Project:  Sigma (R8027)
//  Description:
//      Fixed incorrect handling of VGA access requests/releases, which
//      may have caused the problem with the errant lines outside of the
//      waveform grid.

//  Revision: 009  By:  hhd    Date:  22-AUG-97    DR Number: 2321
//       Project:  Sigma (R8027)
//       Description:
//				Cleaned up code modification made for DCS 2321.
//
//  Revision: 008  By:  hhd    Date:  18-AUG-97    DR Number: 2321
//  Project:  Sigma (R8027)
//  Description:
//      Converted decimal point to comma for all European but German language.
//		  Changed display format for patient data values.
//
//  Revision: 007  By:  yyy   Date:  04-Aug-1997    DCS Number:  2315
//  Project:  Sigma (R8027)
//  Description:
//      Removed the call to UpperSubScreenArea to dismiss the upper subscreen.
//
//  Revision: 006  By:  yyy   Date:  04-Aug-1997    DCS Number:  1846
//  Project:  Sigma (R8027)
//  Description:
//      Re-evaluaate the DateTimeSettingsSubScreen setting buttons when the
//      display mode changes.
//
//  Revision: 005  By:  yyy   Date:  19-Jun-1997    DCS Number:  2249
//  Project:  Sigma (R8027)
//  Description:
//      Clearify the comments.
//
//  Revision: 004  By:  yyy   Date:  19-Jun-1997    DCS Number:  2235
//  Project:  Sigma (R8027)
//  Description:
//      Modified code to fix the timing issue between the GUI TaskExecutor
//		and the TaskBreathBar.  
// 		When changing to MODE_NORMAL displayMode_, we have to issue
// 		addDrawable(&dataContainer_) statement before set the
//		displayMode_ data member.  This is due to the fact that BreathBar
//		task will start posting breath type information once the
// 		displayMode_ is set to MODE_NORMAL.  With this sequence, we
// 		are guaranteed that the dataContainer is attached to the parent
//		container.
//
//  Revision: 003  By:  sah   Date:  12-Jun-1997    DCS Number:  2208
//  Project:  Sigma (R8027)
//  Description:
//      Modified code to incorporate new VGA interface.
//
//  Revision: 002  By:  yyy    Date:  09-Jun-97    DR Number: 2202
//		Project:  Sigma (R8027)
//		Description:
//			Removed all referenced to IsPowerFailure...., uses
//			IsVentStartupSequenceCompleted to detect the short power
//			failure.
//
//  Revision: 001  By:  wks    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "Sigma.hh"
#include "VitalPatientDataArea.hh"

//@ Usage-Classes
#include "BdEventRegistrar.hh"
#include "BdGuiEvent.hh"
#include "BreathDatumHandle.hh"
#include "BreathPhaseType.hh"
#include "BreathType.hh"
#include "Colors.hh"
#include "CommTaskAgent.hh"
#include "GuiEventRegistrar.hh"
#include "GuiTimerId.hh"
#include "GuiTimerRegistrar.hh"
#include "LowerScreen.hh"
#include "MiscStrs.hh"
#include "NovRamManager.hh"
#include "PatientDataRegistrar.hh"
#include "Setting.hh"
#include "SettingContextHandle.hh"
#include "SigmaState.hh"
#include "TaskControlAgent.hh"
#include "VentTypeValue.hh"
#include "ProxEnabledValue.hh"
#include "TrendDataMgr.hh"
#include "TrendEvents.hh"
#include "ContextSubject.hh"
#include "UpperScreen.hh"
#include "LowerScreen.hh"
#include "MoreDataSubScreen.hh"
#include "WaveformsSubScreen.hh"
#include "AlarmSetupSubScreen.hh"

//@ End-Usage
//@ Code...

// Initialize static constants.
static const Int32 CONTAINER_X_ = 0;
static const Int32 CONTAINER_Y_ = 0;
static const Int32 CONTAINER_WIDTH_ = 640;
static const Int32 CONTAINER_HEIGHT_ = 64;
static const Int32 BREATH_STATUS_BOX_X_ = 0;
static const Int32 BREATH_STATUS_BOX_Y_ = 0;
static const Int32 BREATH_STATUS_BOX_WIDTH_ = 50;		// 44;
static const Int32 BREATH_STATUS_BOX_HEIGHT_ = CONTAINER_HEIGHT_;
static const Int32 VALUE_WIDTH_80_ = 80;
static const Int32 VALUE_WIDTH_66_ = 66;
static const Int32 VALUE_WIDTH_88_ = 88;
static const Int32 VALUE_WIDTH_73_ = 73;
static const Int32 VALUE_WIDTH_95_ = 95;
static const Int32 VALUE_GAP_ = 4;
static const Int32 VALUE_HEIGHT_ = 35;
static const Int32 ROW1_LABEL_Y_ = 0;
static const Int32 ROW2_VALUE_Y_ = 28;
static const Int32 DISPLAY_OFFSET_ = 1;		// 4
static const Int32 COL1_LABEL_X_OFFSET_ = 19;
static const Int32 COL1_START_X_ = BREATH_STATUS_BOX_WIDTH_ + DISPLAY_OFFSET_;		// 2;
static const Int32 COL1_LABEL_X_ = COL1_START_X_ + COL1_LABEL_X_OFFSET_;
static const Int32 COL1_VALUE_X_ = COL1_START_X_;
static const Int32 COL2_START_X_ = COL1_START_X_ + VALUE_GAP_ + VALUE_WIDTH_80_;
static const Int32 COL2_LABEL_X_OFFSET_ = 17;
static const Int32 COL2_LABEL_X_ = COL2_START_X_ + COL2_LABEL_X_OFFSET_;
static const Int32 COL2_VALUE_X_ = COL2_START_X_;
static const Int32 COL3_START_X_ = COL2_START_X_ + VALUE_GAP_ + VALUE_WIDTH_80_;
static const Int32 COL3_LABEL_X_OFFSET_ = 10; 
static const Int32 COL3_LABEL_X_ = COL3_START_X_ + COL3_LABEL_X_OFFSET_;
static const Int32 COL3_VTI_LABEL_X_OFFSET_ = 28; 
static const Int32 COL3_VTI_LABEL_X_ = COL3_START_X_ + COL3_VTI_LABEL_X_OFFSET_;
static const Int32 COL3_VALUE_X_ = COL3_START_X_;
static const Int32 COL4_START_X_ = COL3_START_X_ + VALUE_GAP_ + VALUE_WIDTH_80_;
static const Int32 COL4_LABEL_X_OFFSET_ = 36;
static const Int32 COL4_LABEL_X_ = COL4_START_X_ + COL4_LABEL_X_OFFSET_;
static const Int32 COL4_VALUE_X_OFFSET_ = 10;
static const Int32 COL4_VALUE_X_ = COL4_START_X_ + COL4_VALUE_X_OFFSET_;
static const Int32 COL5_START_X_ = COL4_START_X_ + VALUE_GAP_ + VALUE_WIDTH_95_;	// 114;
static const Int32 COL5_LABEL_X_OFFSET_ = 22;
static const Int32 COL5_LABEL_X_ = COL5_START_X_ + COL5_LABEL_X_OFFSET_;
static const Int32 COL5_VALUE_X_ = COL5_START_X_;
static const Int32 COL6_START_X_ = COL5_START_X_ + VALUE_GAP_ + VALUE_WIDTH_66_;
static const Int32 COL6_LABEL_X_OFFSET_ = 28;
static const Int32 COL6_LABEL_X_ = COL6_START_X_ + COL6_LABEL_X_OFFSET_;
static const Int32 COL6_VALUE_X_ = COL6_START_X_;
static const Int32 COL7_START_X_ = COL6_START_X_ + VALUE_GAP_ + VALUE_WIDTH_88_;
static const Int32 COL7_LABEL_X_OFFSET_ = 10;
static const Int32 COL7_LABEL_X_ = COL7_START_X_ + COL7_LABEL_X_OFFSET_;
static const Int32 COL7_VALUE_X_ = COL7_START_X_;

static const Int32 BREATH_STATUS_TEXT_X_ = 5;
 
static wchar_t  PeakCircPressureMsg_[120]; 
static wchar_t  EndExhalationPressureMsg_[120]; 
static wchar_t  MeanCircPressureMsg_[120]; 


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: VitalPatientDataArea()  [Default Constructor]
//
//@ Interface-Description
// Default constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
// Initializes all members and initializes the colors, sizes, and positions
// of all graphical objects.  Adds all graphical objects to the parent or
// patient data container.  Registers for update callbacks for the patient
// data values and registers for timer callbacks for the "no vent"
// elapsed time display.  Also registers for all events (BD and GUI) that
// can affect the display.
//
// $[01006] All shorthand symbols in the VPDA must display their meaning
// in the Message Area ...
// $[01010] On Vent Startup display an explanatory message in VPDA ...
// $[01125] Each vital patient data value consists of 3 parts ...
// $[01132] Display Vent Startup message when ...
// $[07056] All pressure displayed are in cmH2O or hPa based on operator ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

VitalPatientDataArea::VitalPatientDataArea(void) :
breathStatusBox_(BREATH_STATUS_BOX_X_, BREATH_STATUS_BOX_Y_, 
				 BREATH_STATUS_BOX_WIDTH_, BREATH_STATUS_BOX_HEIGHT_),
	breathStatusText_(MiscStrs::TOUCHABLE_BREATH_STATUS_CONTROL,
					  MiscStrs::TOUCHABLE_BREATH_STATUS_CONTROL_MSG),

	peakCircPressureLabel_(MiscStrs::TOUCHABLE_PEAK_CIRC_PRES_LABEL),
	peakCircPressureValue_(TextFont::TWENTY_FOUR, 4, CENTER),

	endExhalationPressureLabel_(MiscStrs::TOUCHABLE_END_EXH_PRES_LABEL),
	endExhalationPressureValue_(TextFont::TWENTY_FOUR, 4, CENTER),

	meanCircPressureLabel_(MiscStrs::TOUCHABLE_MEAN_CIRC_PRES_LABEL),
	meanCircPressureValue_(TextFont::TWENTY_FOUR, 4, CENTER),

	deliveredVolumeLabel_(MiscStrs::TOUCHABLE_DELIVERED_VOL_LABEL),
	deliveredVolumeValue_(TextFont::TWENTY_FOUR, 4, CENTER),

	ieRatioLabel_(MiscStrs::TOUCHABLE_IE_RATIO_LABEL,
				  MiscStrs::TOUCHABLE_IE_RATIO_MSG),
	ieRatioValue_(NULL),

	totalRespiratoryRateLabel_(MiscStrs::TOUCHABLE_TOTAL_RESP_RATE_LABEL,
							   MiscStrs::TOUCHABLE_TOTAL_RESP_RATE_MSG),
	totalRespiratoryRateValue_(TextFont::TWENTY_FOUR, 2, CENTER),

	exhaledTidalVolumeLabel_(MiscStrs::TOUCHABLE_EXH_TIDAL_VOL_LABEL,
							 MiscStrs::TOUCHABLE_EXH_TIDAL_VOL_MSG),
	exhaledTidalVolumeValue_(TextFont::TWENTY_FOUR, 3, CENTER),

	exhaledMinuteVolumeLabel_(MiscStrs::TOUCHABLE_EXH_MINUTE_VOL_LABEL,
							  MiscStrs::TOUCHABLE_EXH_MINUTE_VOL_MSG),
	exhaledMinuteVolumeValue_(TextFont::TWENTY_FOUR, 3, CENTER),

	bigMessage_(MiscStrs::VENTILATOR_MALFUNCTION_LABEL),
	displayMode_(MODE_MALFUNCTION),
	dividingLine_(CONTAINER_X_, CONTAINER_HEIGHT_ - 1, 
				  CONTAINER_WIDTH_, CONTAINER_HEIGHT_ - 1),
	currentBreathPhase_(BreathPhaseType::EXHALATION),
	currentBreathType_(CONTROL)
{
	CALL_TRACE("VitalPatientDataArea::VitalPatientDataArea(void)");

	// Size and position the area
	setX(CONTAINER_X_);
	setY(CONTAINER_Y_);
	setWidth(CONTAINER_WIDTH_);
	setHeight(CONTAINER_HEIGHT_);

	// Default background color is black
	setFillColor(Colors::BLACK);

	// Size the patient data container
	dataContainer_.setWidth(CONTAINER_WIDTH_);
	dataContainer_.setHeight(CONTAINER_HEIGHT_);

	// Set permanent color of breath status box outline
	breathStatusBox_.setColor(Colors::WHITE);

	// Add the breath status box and text to the drawable collection
	dataContainer_.addDrawable(&breathStatusBox_);
	dataContainer_.addDrawable(&breathStatusText_);
	breathStatusText_.setX(BREATH_STATUS_TEXT_X_);
	breathStatusBox_.setShow(FALSE);
	breathStatusText_.setShow(FALSE);

	if ( !GuiApp::IsVentStartupSequenceComplete() )
	{	// GUI just went through AC recovery with the possibility of
		// UNKNOWN, EXCEPTION, long POWERFAIL, WATCHDOG, and INTENTIONAL
		// 
		// $[TI7]
		// Clear "No Vent" start time flag.
		noVentStartTime_.invalidate();

		// Clear the no vent time stamp.
		NovRamManager::ClearNoVentStartTime();
	}
	else
	{
		// $[TI8]
		// Only short power recovery falls into this category.
		NovRamManager::GetNoVentStartTime(noVentStartTime_);

		// If the "No Vent Start Time" is invalid then set it to the current
		// time - this is the first time for this condition to occure, then
		// we need to set the no vent time, else we should not clear the no
		// vent time stamp.
		if ( noVentStartTime_.isInvalid() )
		{											// $[TI8.1]
			NovRamManager::SetNoVentStartTime();

			// Set "No Vent" start time flag.
			noVentStartTime_.now();
		}											// $[TI8.2]
	}

	StringId  PRESS_UNITS =
		(GuiApp::PressUnits() == PressUnitsValue::CMH2O_UNIT_VALUE)    
		? L"cmH{S:2}O"  // $[TI9]
		: L"hPa";	   // $[TI10]

	// Set color, place, and add field labels.
	peakCircPressureLabel_.setColor(Colors::WHITE);
	peakCircPressureLabel_.setX(COL1_LABEL_X_);
	peakCircPressureLabel_.setY(ROW1_LABEL_Y_);
	swprintf(::PeakCircPressureMsg_, MiscStrs::TOUCHABLE_PEAK_CIRC_PRES_MSG,   
			PRESS_UNITS);
	peakCircPressureLabel_.setMessage(::PeakCircPressureMsg_);
	dataContainer_.addDrawable(&peakCircPressureLabel_);

	peakCircPressureValue_.setPrecision(TENTHS);
	peakCircPressureValue_.setX(COL1_VALUE_X_);
	peakCircPressureValue_.setY(ROW2_VALUE_Y_);
	// worst case: -14.4 or 144  = 22 * 2 + 18 + 11 + 7 = 80 pixels
	peakCircPressureValue_.setWidth(VALUE_WIDTH_80_);
	peakCircPressureValue_.setHeight(VALUE_HEIGHT_);
	peakCircPressureValue_.setColor(Colors::WHITE);
	dataContainer_.addDrawable(&peakCircPressureValue_);

	meanCircPressureLabel_.setColor(Colors::WHITE);
	meanCircPressureLabel_.setX(COL2_LABEL_X_);
	meanCircPressureLabel_.setY(ROW1_LABEL_Y_);
	swprintf(::MeanCircPressureMsg_, MiscStrs::TOUCHABLE_MEAN_CIRC_PRES_MSG,  
			PRESS_UNITS);
	meanCircPressureLabel_.setMessage(::MeanCircPressureMsg_);
	dataContainer_.addDrawable(&meanCircPressureLabel_);

	meanCircPressureValue_.setPrecision(TENTHS);
	meanCircPressureValue_.setX(COL2_VALUE_X_);
	meanCircPressureValue_.setY(ROW2_VALUE_Y_);
	// worst case: -14.4 or 144  = 22 * 2 + 18 + 11 + 7 = 80 pixels
	meanCircPressureValue_.setWidth(VALUE_WIDTH_80_);
	meanCircPressureValue_.setHeight(VALUE_HEIGHT_);
	meanCircPressureValue_.setColor(Colors::WHITE);
	dataContainer_.addDrawable(&meanCircPressureValue_);

	deliveredVolumeLabel_.setColor(Colors::WHITE);
	deliveredVolumeLabel_.setX(COL3_VTI_LABEL_X_);
	deliveredVolumeLabel_.setY(ROW1_LABEL_Y_);
	deliveredVolumeLabel_.setMessage(MiscStrs::TOUCHABLE_DELIVERED_VOL_MSG);
	dataContainer_.addDrawable(&deliveredVolumeLabel_);

	deliveredVolumeValue_.setPrecision(ONES);
	deliveredVolumeValue_.setX(COL3_VALUE_X_);
	deliveredVolumeValue_.setY(ROW2_VALUE_Y_);
	// worst case: 4444  = 22 * 4 = 88 pixels
	deliveredVolumeValue_.setWidth(VALUE_WIDTH_88_);
	deliveredVolumeValue_.setHeight(VALUE_HEIGHT_);
	deliveredVolumeValue_.setColor(Colors::WHITE);
	dataContainer_.addDrawable(&deliveredVolumeValue_);

	endExhalationPressureLabel_.setColor(Colors::WHITE);
	endExhalationPressureLabel_.setX(COL3_LABEL_X_);
	endExhalationPressureLabel_.setY(ROW1_LABEL_Y_);
	swprintf(::EndExhalationPressureMsg_, MiscStrs::TOUCHABLE_END_EXH_PRES_MSG,  
			PRESS_UNITS);
	endExhalationPressureLabel_.setMessage(::EndExhalationPressureMsg_);
	dataContainer_.addDrawable(&endExhalationPressureLabel_);

	endExhalationPressureValue_.setPrecision(TENTHS);
	endExhalationPressureValue_.setX(COL3_VALUE_X_);
	endExhalationPressureValue_.setY(ROW2_VALUE_Y_);
	// worst case: -14.4 or 144  = 22 * 2 + 18 + 11 + 7 = 80 pixels
	endExhalationPressureValue_.setWidth(VALUE_WIDTH_80_);
	endExhalationPressureValue_.setHeight(VALUE_HEIGHT_);
	endExhalationPressureValue_.setColor(Colors::WHITE);
	dataContainer_.addDrawable(&endExhalationPressureValue_);

	ieRatioLabel_.setColor(Colors::WHITE);
	ieRatioLabel_.setX(COL4_LABEL_X_);
	ieRatioLabel_.setY(ROW1_LABEL_Y_);
	dataContainer_.addDrawable(&ieRatioLabel_);

	ieRatioValue_.setColor(Colors::WHITE);
	ieRatioValue_.setX(COL4_VALUE_X_);
	ieRatioValue_.setY(ROW2_VALUE_Y_ + 2);
	dataContainer_.addDrawable(&ieRatioValue_);

	totalRespiratoryRateLabel_.setColor(Colors::WHITE);
	totalRespiratoryRateLabel_.setX(COL5_LABEL_X_);
	totalRespiratoryRateLabel_.setY(ROW1_LABEL_Y_);
	dataContainer_.addDrawable(&totalRespiratoryRateLabel_);

	totalRespiratoryRateValue_.setPrecision(ONES);
	totalRespiratoryRateValue_.setX(COL5_VALUE_X_);
	totalRespiratoryRateValue_.setY(ROW2_VALUE_Y_);
	// worst case: 244 or 4.4  = 22 * 2 + 22 = 66 pixels
	totalRespiratoryRateValue_.setWidth(VALUE_WIDTH_66_);
	totalRespiratoryRateValue_.setHeight(VALUE_HEIGHT_);
	totalRespiratoryRateValue_.setColor(Colors::WHITE);
	dataContainer_.addDrawable(&totalRespiratoryRateValue_);

	exhaledTidalVolumeLabel_.setColor(Colors::WHITE);
	exhaledTidalVolumeLabel_.setX(COL6_LABEL_X_);
	exhaledTidalVolumeLabel_.setY(ROW1_LABEL_Y_);
	dataContainer_.addDrawable(&exhaledTidalVolumeLabel_);

	exhaledTidalVolumeValue_.setPrecision(ONES);
	exhaledTidalVolumeValue_.setX(COL6_VALUE_X_);
	exhaledTidalVolumeValue_.setY(ROW2_VALUE_Y_);
	// worst case: 4444  = 22 * 4 = 88 pixels
	exhaledTidalVolumeValue_.setWidth(VALUE_WIDTH_88_);
	exhaledTidalVolumeValue_.setHeight(VALUE_HEIGHT_);
	exhaledTidalVolumeValue_.setColor(Colors::WHITE);
	dataContainer_.addDrawable(&exhaledTidalVolumeValue_);

	exhaledMinuteVolumeLabel_.setColor(Colors::WHITE);
	exhaledMinuteVolumeLabel_.setX(COL7_LABEL_X_);
	exhaledMinuteVolumeLabel_.setY(ROW1_LABEL_Y_);
	dataContainer_.addDrawable(&exhaledMinuteVolumeLabel_);

	exhaledMinuteVolumeValue_.setPrecision(TENTHS);
	exhaledMinuteVolumeValue_.setX(COL7_VALUE_X_);
	exhaledMinuteVolumeValue_.setY(ROW2_VALUE_Y_);
	// worst case: 4.44 or 44.4 22 * 3 + 7 = 73
	exhaledMinuteVolumeValue_.setWidth(VALUE_WIDTH_73_);
	exhaledMinuteVolumeValue_.setHeight(VALUE_HEIGHT_);
	exhaledMinuteVolumeValue_.setColor(Colors::WHITE);
	dataContainer_.addDrawable(&exhaledMinuteVolumeValue_);

	// Display and color big message text field
	addDrawable(&bigMessage_);
	bigMessage_.setColor(Colors::WHITE);
	bigMessage_.positionInContainer(GRAVITY_CENTER);

	dividingLine_.setColor(Colors::WHITE);
	addDrawable(&dividingLine_);

	// this subscreen needs to be attached to the accepted context subject...
	attachToSubject_(ContextId::ACCEPTED_CONTEXT_ID, Notification::BATCH_CHANGED);

	// Register for changes to vital patient data.  No need to register for
	// Breath Phase and Type as these patient data values are handled
	// specially in order to be updated as part of the Network-Application
	// task.  They need to be updated at a higher priority than the GUI-App
	// task to guarantee that they are in sync with patient breathing.
	// See the PatientDataRegistrar class for more information.
	PatientDataRegistrar::RegisterTarget(PatientDataId::BREATH_TYPE_DISPLAY_ITEM, this);
	PatientDataRegistrar::RegisterTarget(PatientDataId::BREATH_PHASE_ITEM, this);
	PatientDataRegistrar::RegisterTarget(PatientDataId::IE_RATIO_ITEM, this);
	PatientDataRegistrar::RegisterTarget(PatientDataId::TOTAL_RESP_RATE_ITEM,
										 this);
	PatientDataRegistrar::RegisterTarget(PatientDataId::END_EXP_PRESS_ITEM,
										 this);
	PatientDataRegistrar::RegisterTarget(PatientDataId::EXH_TIDAL_VOL_ITEM,
										 this);
	PatientDataRegistrar::RegisterTarget(PatientDataId::EXH_MINUTE_VOL_ITEM,
										 this);
	PatientDataRegistrar::RegisterTarget(PatientDataId::PEAK_CCT_PRESS_ITEM,
										 this);
	PatientDataRegistrar::RegisterTarget(PatientDataId::MEAN_CCT_PRESS_ITEM,
										 this);
	PatientDataRegistrar::RegisterTarget(PatientDataId::INSP_TIDAL_VOL_ITEM,
										 this);
	PatientDataRegistrar::RegisterTarget(PatientDataId::IS_PEEP_RECOVERY_ITEM,
										 this);

	// Register for timer event callbacks.
	GuiTimerRegistrar::RegisterTarget(GuiTimerId::NO_VENT_CLOCK, this);

	// Register for BD event callbacks
	BdEventRegistrar::RegisterTarget(EventData::OCCLUSION, this);
	BdEventRegistrar::RegisterTarget(EventData::PATIENT_CONNECT, this);
	BdEventRegistrar::RegisterTarget(EventData::SVO, this);
	BdEventRegistrar::RegisterTarget(EventData::VENT_INOP, this);
	BdEventRegistrar::RegisterTarget(EventData::APNEA_VENT, this);
	BdEventRegistrar::RegisterTarget(EventData::SAFETY_VENT, this);
	BdEventRegistrar::RegisterTarget(EventData::PROX_FAULT, this);
	BdEventRegistrar::RegisterTarget(EventData::PROX_READY, this);

	// Register for GUI state change callbacks.
	GuiEventRegistrar::RegisterTarget(this);

	isProxInop_ = FALSE;
	isProxReady_ = FALSE;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~VitalPatientDataArea  [Destructor]
//
//@ Interface-Description
//  Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  n/a
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

VitalPatientDataArea::~VitalPatientDataArea(void)
{
	CALL_TRACE("VitalPatientDataArea::VitalPatientDataArea(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setDisplayMode
//
//@ Interface-Description
// Sets the VitalPatientDataArea into one of its six display
// modes.  Passed 'displayMode', the new display mode value (enum).
//---------------------------------------------------------------------
//@ Implementation-Description
// In normal ventilation mode (MODE_NORMAL), this area displays normal
// patient data and breath status.  In vent startup mode
// (MODE_VENT_STARTUP), this area displays a static "Vent Startup in
// progress" message.  In waiting for patient mode (MODE_WAITING_FOR_PT),
// this area displays a static "Waiting for Patient Connect" message.  In no
// vent mode, (MODE_NO_VENT), this area displays a dynamic elapsed time
// display of the number of minutes/seconds elapsed since entering no vent
// mode. In malfunction mode (MODE_MALFUNCTION) the message "Ventilator
// Malfunction" is displayed.  In vent inop mode (MODE_INOP) "Ventilator
// Inoperative" is displayed.  In service required mode (MODE_SERVICE_REQUIRED)
// "Service Mode Required" is displayed.  In safety valve open mode (MODE_SVO)
// "No ventilation and Safety valve open." is displayed. In vent inop for.. mode
// (MODE_INOP_NO_VENT) "Vent inop for:xxxx"  is displayed.
//
// There are no restrictions and no error checking on changing from
// one mode to a different mode.
//
// $[01128] Vital patient data must be displayed at all times unless ...
// $[01131] If no breaths being delivered hide breath status indicator.
// $[01134] VPDA should display a timed no ventilation message ...
//---------------------------------------------------------------------
//@ PreCondition
//	The given displayMode value must be one of the expected enum values.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void 
	VitalPatientDataArea::setDisplayMode(VpdaDisplayMode displayMode)
{
	CALL_TRACE("VitalPatientDataArea::setDisplayMode(displayMode)");
	VpdaDisplayMode previousDisplayMode;

	// Only do the mode change if new mode is different than 
	// previous mode.
	if ( displayMode_ == displayMode )
	{													// $[TI1]
		return;
	}
	else
	{													// $[TI2]
		previousDisplayMode = displayMode_;
	}

	switch ( displayMode )
	{
		case MODE_NORMAL:									// $[TI3]
			// Make sure the big message is removed from screen.
			removeDrawable(&bigMessage_);

			updateDataComponents_();

			// Show the patient data field labels and values.
			addDrawable(&dataContainer_);

			// NOTE: The displayMode_ has to be set after the
			// addDrawable(&dataContainer_) statement.
			// This is due to the fact that BreathBar task will
			// start posting breath type information once the
			// displayMode is set to MODE_NORMAL.  With this
			// sequence, we are guaranteed to have the dataContainer
			// appended to the parent container.
			displayMode_ = displayMode;

			PatientDataRegistrar::ResetPatientValue();

			// Make sure background is black
			setFillColor(Colors::BLACK);

			// Stop and clear the "No Vent" timer in case it is going.
			GuiTimerRegistrar::CancelTimer(GuiTimerId::NO_VENT_CLOCK);
			noVentStartTime_.invalidate();

			// Reset the noVentStartTime.
			NovRamManager::ClearNoVentStartTime();
			break;

		case MODE_VENT_STARTUP:
		case MODE_MALFUNCTION:
		case MODE_SVO:
		case MODE_INOP:
		case MODE_SERVICE_REQUIRED:
		case MODE_WAITING_FOR_PT:							// $[TI4]
			// NOTE: The displayMode_ has to be set before the
			// addDrawable(&dataContainer_) statement.
			// Set the displayMode_ as soon as possible to disable 
			// the possibility that the BreathBar task will
			// start posting breath type information to this area.
			displayMode_ = displayMode;

			// Make sure the patient data field labels and values are hidden.
			removeDataContainerDrawables_();

			// Display the big message text field
			addDrawable(&bigMessage_);

			// Make sure background is black
			setFillColor(Colors::BLACK);

			if ( MODE_VENT_STARTUP == displayMode_ )
			{												// $[TI5]
				bigMessage_.setText(MiscStrs::VENT_STARTUP_LABEL);
			}
			else if ( MODE_WAITING_FOR_PT == displayMode_ )
			{												// $[TI6]
				bigMessage_.setText(MiscStrs::WAITING_FOR_PT_LABEL);
			}
			else if ( MODE_MALFUNCTION == displayMode_ )
			{												// $[TI7]
				bigMessage_.setText(MiscStrs::VENTILATOR_MALFUNCTION_LABEL);
			}
			else if ( MODE_INOP == displayMode_ )
			{												// $[TI8]
				bigMessage_.setText(MiscStrs::VENTILATOR_INOPERATIVE_LABEL);
			}
			else if ( MODE_SERVICE_REQUIRED == displayMode_ )
			{												// $[TI9]
				bigMessage_.setText(MiscStrs::SERVICE_REQUIRED_LABEL);
			}
			else if ( MODE_SVO == displayMode_ )
			{												// $[TI10]
				bigMessage_.setText(MiscStrs::SVO_LABEL);
			}
			else
			{
				SAFE_CLASS_ASSERTION_FAILURE();
			}
			bigMessage_.positionInContainer(GRAVITY_CENTER);

			// Stop the "No Vent" clock.
			GuiTimerRegistrar::CancelTimer(GuiTimerId::NO_VENT_CLOCK);
			break;

		case MODE_INOP_NO_VENT:
		case MODE_NO_VENT:									// $[TI11]
			// NOTE: The displayMode_ has to be set before the
			// addDrawable(&dataContainer_) statement.
			// Set the displayMode_ as soon as possible to disable 
			// the possibility that the BreathBar task will
			// start posting breath type information to this area.
			displayMode_ = displayMode;

			if ( MODE_INOP_NO_VENT == previousDisplayMode ||
				 MODE_NO_VENT == previousDisplayMode )
			{												// $[TI12]
				// Set the initial display and start the "No Vent" clock.
				//updateNoVentDisplay_();
				return;
			}
			else
			{												// $[TI13]
				// Make sure patient data is hidden
				removeDataContainerDrawables_();

				// Background color goes to black
				setFillColor(Colors::BLACK);

				// Display big message text field
				addDrawable(&bigMessage_);

				// If the "No Vent Start Time" is invalid then set it to the current
				// time.  It should normally be invalid but it will be valid if a
				// No Vent mode was interrupted temporarily by communications going
				// down (Malfunction) mode.  In that case the No Vent message
				// continues with the last "No Vent Start Time".
				if ( noVentStartTime_.isInvalid() )
				{												// $[TI14]
					NovRamManager::SetNoVentStartTime();

					// Set "No Vent" start time flag.
					noVentStartTime_.now();
				}												// $[TI15]						

				// Set the initial display and start the "No Vent" clock.
				updateNoVentDisplay_();

				GuiTimerRegistrar::StartTimer(GuiTimerId::NO_VENT_CLOCK);
			}
			break;

		case MAX_MODE:
		default:
			CLASS_ASSERTION_FAILURE();
			break;
	}

	LowerScreen::RLowerScreen.getLowerSubScreenArea()->updateDateTimeSettingScreen();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: bdEventHappened
//
//@ Interface-Description
// Called normally via the BdEventRegistrar when a BD event in which we are
// interested occurs, e.g. Safety Ventilation or Ventilator Inoperative.
// Passed the following parameters:
// >Von
//	eventId		The type of the BD event.
//	eventStatus	The status of the BD event.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
// Ignore passed parameters and just call setMode_() which will decide
// what display mode change is needed, if any.
// 
// When Breath-Delivery sends a PROX_FAULT message with an active 
// event id, remove all PROX symbols by setting the isProxInop_ 
// to TRUE, and reactivate this subscreen. 
// $[PX06003]
//---------------------------------------------------------------------
//@ PreCondition					      
//  none						      
//---------------------------------------------------------------------
//@ PostCondition					     
//  none						      
//@ End-Method						
//=====================================================================

void                              
	VitalPatientDataArea::bdEventHappened(EventData::EventId eventId,
										  EventData::EventStatus eventStatus,
										  EventData::EventPrompt eventPrompt)
{
	if (EventData::PROX_FAULT == eventId ||
		EventData::SAFETY_VENT == eventId)
	{
		// When PROX is inoperative, set the isProxInop_ to remove the PROX symbols 
		// and refresh the screen.
		if(eventStatus == EventData::ACTIVE)		  
		{
            isProxInop_ = TRUE;
		}
		else
		{
			isProxInop_ = FALSE;
		}
	}
	else if (EventData::PROX_READY == eventId)
	{

		if(eventStatus == EventData::ACTIVE)		  
		{
            isProxReady_ = TRUE;
		}
		else
		{
			isProxReady_ = FALSE;
		}
	}

	// Just call setMode_() to decide on new display mode, if any
	setMode_();											// $[TI1]
	// Update the display components
	updateDataComponents_();

    // Refresh all prox-related subscreens since display mode may have changed.
    if ((eventId != EventData::PROX_FAULT) && (EventData::PROX_READY != eventId) && (EventData::SAFETY_VENT != eventId)) 
    {
        if (UpperScreen::RUpperScreen.getUpperSubScreenArea()->GetMoreDataSubScreen()->isVisible())
        {
            UpperScreen::RUpperScreen.getUpperSubScreenArea()->GetMoreDataSubScreen()->deactivate();
            UpperScreen::RUpperScreen.getUpperSubScreenArea()->GetMoreDataSubScreen()->activate();
        }
        else if (UpperScreen::RUpperScreen.getUpperSubScreenArea()->GetWaveformsSubScreen()->isVisible())
        {
            UpperScreen::RUpperScreen.getUpperSubScreenArea()->GetWaveformsSubScreen()->deactivate();
            UpperScreen::RUpperScreen.getUpperSubScreenArea()->GetWaveformsSubScreen()->activate();
        }
        else if (UpperScreen::RUpperScreen.getUpperSubScreenArea()->GetRmDataSubScreen()->isVisible())
        {
            UpperScreen::RUpperScreen.getUpperSubScreenArea()->GetRmDataSubScreen()->deactivate();
            UpperScreen::RUpperScreen.getUpperSubScreenArea()->GetRmDataSubScreen()->activate();
        }
    
        if (LowerScreen::RLowerScreen.getLowerSubScreenArea()->GetAlarmSetupSubScreen()->isVisible())
        {
            LowerScreen::RLowerScreen.getLowerSubScreenArea()->GetAlarmSetupSubScreen()->deactivate();
            LowerScreen::RLowerScreen.getLowerSubScreenArea()->GetAlarmSetupSubScreen()->activate();
        }
    }
}                                


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: guiEventHappened
//
//@ Interface-Description
// Called normally via GuiEventRegistrar when the GUI event has changed.
// Passed the id of the state which changed.
//---------------------------------------------------------------------
//@ Implementation-Description
// Just call setMode_() to decide if a change in the display mode is needed.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
	VitalPatientDataArea::guiEventHappened(GuiApp::GuiAppEvent eventId)
{
	CALL_TRACE("VitalPatientDataArea::guiEventHappened(GuiApp::GuiAppEvent)");

	if ( eventId != GuiApp::PATIENT_DATA_DISPLAY )
	{
		// Just call setMode_() to decide if a change in the display mode is needed
		setMode_();											// $[TI1]
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: timerEventHappened [virtual]
//
//@ Interface-Description
// Handles "no vent" elapsed time counter events. The 'timerId' 
// identifies which timer went off.
//---------------------------------------------------------------------
//@ Implementation-Description
// Calls the internal display handler, updateNoVentDisplay_() to
// update the elapsed time display.  The area must be in the "no vent"
// display mode, otherwise no action will occur.
//---------------------------------------------------------------------
//@ PreCondition
//	The given timerId value must be the NO_VENT_CLOCK id.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void 
	VitalPatientDataArea::timerEventHappened(GuiTimerId::GuiTimerIdType timerId)
{
	CALL_TRACE("VitalPatientDataArea::timerEventHappened(timerId)");

	switch ( timerId )
	{
		case GuiTimerId::NO_VENT_CLOCK:						// $[TI1]
			if ( MODE_NO_VENT == displayMode_ ||
				 MODE_INOP_NO_VENT == displayMode_ )
			{												// $[TI2]
				updateNoVentDisplay_();
			}
			break;

		default:
			CLASS_ASSERTION_FAILURE();
			break;
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateBreathBar
//
//@ Interface-Description
// Draws the breath bar seen on the upper left hand corner of the 
// upper display. Since the breath bar must be displayed in as close to
// real-time as possible, this method actually calls the lower level
// drawing routines directly instead of waiting for the normal GUI
// Application scheduling of refresh screen events.
//---------------------------------------------------------------------
//@ Implementation-Description
// 1. Gets the semaphore for screen access from GuiFoundation using
//    GuiFoundation::RequestDisplayAccess. 
// 2. Saves the current display context. 
// 3. Sets the text and color of the breath bar according to the breath 
//    type and breath phase. 
// 4. Calls the drawing functions for the breath bar objects directly. 
// 5. Restores the saved display context. 
// 6. Releases display access using GuiFoundation::ReleaseDisplayAccess.
//
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void 
	VitalPatientDataArea::updateBreathBar_(void)
{
	// Draw Breath Phase/Type. First request access to the display 
	// by requesting Direct Draw access. Direct Draw access allows
	// modification of drawables without forcing them onto the changed
	// drawables list in the BaseContainer. By requesting Direct Draw 
	// access, we also lock access to the DisplayContext so we can issue
	// the drawDirect calls from this method instead of using the 
	// background refresh mechanism. The lock (or mutex) is required 
	// to reserve the VGA hardware resource.

	setDirectDraw(TRUE);

	Colors::ColorS breathBarColor_ = Colors::WHITE;

	switch ( currentBreathType_ )
	{
		case CONTROL:									// $[TI1.1]
			breathStatusText_.setMessage(MiscStrs::TOUCHABLE_BREATH_STATUS_CONTROL_MSG);
			breathStatusText_.setText(MiscStrs::TOUCHABLE_BREATH_STATUS_CONTROL);
			breathBarColor_ = Colors::GREEN;
			break;

		case ASSIST:									// $[TI1.2]
			breathStatusText_.setMessage(MiscStrs::TOUCHABLE_BREATH_STATUS_ASSIST_MSG);
			breathStatusText_.setText(MiscStrs::TOUCHABLE_BREATH_STATUS_ASSIST);
			breathBarColor_ = Colors::GREEN;
			break;

		case SPONT:										// $[TI1.3]
			breathStatusText_.setMessage(MiscStrs::TOUCHABLE_BREATH_STATUS_SPONT_MSG);
			breathStatusText_.setText(MiscStrs::TOUCHABLE_BREATH_STATUS_SPONT);
			breathBarColor_ = Colors::GOLDENROD;
			break;

		default:										// $[TI1.4]
			break;
	}

	switch ( currentBreathPhase_ )
	{
		case BreathPhaseType::INSPIRATION:				// $[TI2.1]
			breathStatusBox_.setColor(breathBarColor_);
			breathStatusBox_.setFillColor(breathBarColor_);
			breathStatusText_.setColor(Colors::BLACK);
			break;

		case BreathPhaseType::EXHALATION:
		case BreathPhaseType::EXPIRATORY_PAUSE:			// $[TI2.2]
			breathStatusBox_.setFillColor(Colors::BLACK);
			breathStatusBox_.setColor(breathBarColor_);
			breathStatusText_.setColor(breathBarColor_);
			break;

		default:										// $[TI2.3]
			break;
	}

	// show them when drawDirect called
	breathStatusBox_.setShow(TRUE);
	breathStatusText_.setShow(TRUE);

	breathStatusBox_.drawDirect();
	breathStatusText_.drawDirect();

	// but hide them from the background refresh
	breathStatusBox_.setShow(FALSE);
	breathStatusText_.setShow(FALSE);

	setDirectDraw(FALSE);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: patientDataChangeHappened [virtual]
//
//@ Interface-Description
// Handles update event notification for patient data fields.  The
// 'patientDataId' parameter identifies which patient data value changed.
//---------------------------------------------------------------------
//@ Implementation-Description
// Updates the display of a patient data field to the current measured
// value of the patient data item.  If the value for a patient data
// field is outside the allowable range (maintained by the Patient Data
// subsystem), then flash the (clipped) numeric value.  This processing
// only occurs if the area is in "normal" display mode.
//
// Changes to Breath Phase and Type are sent as part of a higher priority
// task, see the PatientDataRegistrar class for more details.  It is very
// important that Breath Phase/Type are in sync with patient breathing.
// It is not acceptable for the Phase/Type display being unable to keep up
// with Phase/Type updates at high breath rates so it was determined that
// the Phase/Type display needed updating as part of a higher priority task,
// in this case the Network-Application task.  
//
// To draw Breath Phase/Type in a task with a higher priority than GUI-Apps
// some special things need to be done.  For one, Breath Phase/Type cannot
// be drawn as part of the usual BaseContainer repaint() scheme which paints
// a whole screen.  Also, the Phase/Type drawing must be insulated from
// the actual repaint() scheme so that it does not upset an ongoing GUI-App
// screen repaint.  The first problem is solved by putting the Breath
// Phase/Type drawables into dataContainer_ in the hidden state, i.e.
// setShow(FALSE).  This will mean that any changes to the drawables will
// not cause a draw via the BaseContainer::repaint() method.  It also means
// that a direct call to the drawables draw() method will paint the
// drawables directly and immediately.  To insulate the drawing from
// upsetting a normal GUI-Apps screen repaint access to the display is first
// secured (via GuiFoundation::RequestDisplayAccess()), then GUI-Apps
// current display information is backed up (via
// GuiFoundation::SaveDisplayContext()), the Phase/Type drawing is done and
// finally the display information is restored and display is relinquished!
// 
// $[01127] Out of range values should flash ...
// $[01129] The following breath values must be displayed (Breath type/phase)
// $[01130] When visible, the breath status indicator shall distinguish ...
//---------------------------------------------------------------------
//@ PreCondition
// The given patientDataId value must be one of the id's associated
// with the vital patient data fields in this area.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void 
	VitalPatientDataArea::patientDataChangeHappened(
												   PatientDataId::PatientItemId patientDataId)
{
	CALL_TRACE("VitalPatientDataArea::patientDataChangeHappened"
			   "(PatientDataId::PatientItemId patientDataId)");

	if ( MODE_NORMAL != displayMode_ )
	{													// $[TI1]
		return;
	}													// $[TI2]
	// Create a handle to the patient datum so that we can retrieve the
	// current value
	BreathDatumHandle breathDatumHandle(patientDataId);
	BoundedBreathDatum boundedDatum;

	// Prepare the boundedDatum value as long as the changed patientDataId
	// is a bounded value, not a discrete one.
	if ( (patientDataId != PatientDataId::BREATH_TYPE_DISPLAY_ITEM)
		 && (patientDataId != PatientDataId::BREATH_PHASE_ITEM)
		 && (patientDataId != PatientDataId::IS_PEEP_RECOVERY_ITEM) )
	{													// $[TI3]
		boundedDatum = breathDatumHandle.getBoundedValue();
	}													// $[TI4]

	// Display the new value of whichever piece of patient data that changed
	switch ( patientDataId )
	{
		case PatientDataId::IE_RATIO_ITEM:					// $[TI5]
			{
				wchar_t ieRatioText[80];  

				// Depending on the value of I:E Ratio we either display it as 1:val or
				// val:1.  Use cheap text to formulate the display. sprintf rounds the
				// value to the precision specified.
				if ( boundedDatum.data.value < 0.0 )
				{												// $[TI6]
					// Formulate the cheap text for the 1:num display
					swprintf(ieRatioText, L"{p=32,y=21,N:1:%.*f}",  
							- boundedDatum.data.precision,
							- boundedDatum.data.value);
				}
				else
				{												// $[TI7]
					// Formulate the cheap text for the num:1 display
					swprintf(ieRatioText, L"{p=32,y=21,N:%.*f:1}",  
							- boundedDatum.data.precision,
							boundedDatum.data.value);
				}

				ieRatioValue_.setText(ieRatioText);

				// Turn Show flag back on
				ieRatioValue_.setShow(TRUE);

				// Flash the value if it's out of range
				if ( boundedDatum.clippingState == BoundedValue::ABSOLUTE_CLIP )
				{												// $[TI10]
					ieRatioValue_.
						setColor(Colors::WHITE_ON_EXTRA_DARK_GREY_BLINK);
				}
				else
				{												// $[TI11]
					ieRatioValue_.setColor(Colors::WHITE);
				}
			}
			break;

		case PatientDataId::TOTAL_RESP_RATE_ITEM:			// $[TI12]
			totalRespiratoryRateValue_.setValue(boundedDatum.data.value);
			totalRespiratoryRateValue_.setPrecision(boundedDatum.data.precision);

			// Turn Show flag back on
			totalRespiratoryRateValue_.setShow(TRUE);

			// Flash the value if it's out of range
			if ( boundedDatum.clippingState == BoundedValue::ABSOLUTE_CLIP )
			{											   // $[TI15]
				totalRespiratoryRateValue_.
					setColor(Colors::WHITE_ON_EXTRA_DARK_GREY_BLINK);
			}
			else
			{											// $[TI16]	
				totalRespiratoryRateValue_.setColor(Colors::WHITE);
			}
			break;

		case PatientDataId::END_EXP_PRESS_ITEM:			// $[TI17]
			endExhalationPressureValue_.setValue(
												boundedDatum.data.value);
			endExhalationPressureValue_.setPrecision(boundedDatum.data.precision);

			// Turn Show flag back on
			endExhalationPressureValue_.setShow(TRUE);

			// Flash the value if it's out of range
			if ( boundedDatum.clippingState == BoundedValue::ABSOLUTE_CLIP )
			{											// $[TI20]
				endExhalationPressureValue_.
					setColor(Colors::WHITE_ON_EXTRA_DARK_GREY_BLINK);
			}
			else
			{											// $[TI21]
				endExhalationPressureValue_.setColor(Colors::WHITE);
			}
			break;

		case PatientDataId::EXH_TIDAL_VOL_ITEM:				// $[TI22]
			exhaledTidalVolumeValue_.setValue(
											 boundedDatum.data.value);
			exhaledTidalVolumeValue_.setPrecision(boundedDatum.data.precision);

			// Turn Show flag back on
			exhaledTidalVolumeValue_.setShow(TRUE);

			// Flash the value if it's out of range
			if ( boundedDatum.clippingState == BoundedValue::ABSOLUTE_CLIP )
			{											// $[TI25]
				exhaledTidalVolumeValue_.
					setColor(Colors::WHITE_ON_EXTRA_DARK_GREY_BLINK);
			}
			else
			{											// $[TI26]
				exhaledTidalVolumeValue_.setColor(Colors::WHITE);
			}
			break;

		case PatientDataId::EXH_MINUTE_VOL_ITEM:			// $[TI27]
			exhaledMinuteVolumeValue_.setValue(boundedDatum.data.value);
			exhaledMinuteVolumeValue_.setPrecision(boundedDatum.data.precision);

			// Turn Show flag back on
			exhaledMinuteVolumeValue_.setShow(TRUE);

			// Flash the value if it's out of range
			if ( boundedDatum.clippingState == BoundedValue::ABSOLUTE_CLIP )
			{											// $[TI30]
				exhaledMinuteVolumeValue_.
					setColor(Colors::WHITE_ON_EXTRA_DARK_GREY_BLINK);
			}
			else
			{											// $[TI31]
				exhaledMinuteVolumeValue_.setColor(Colors::WHITE);
			}
			break;

		case PatientDataId::PEAK_CCT_PRESS_ITEM:			// $[TI32]
			peakCircPressureValue_.setValue(boundedDatum.data.value);
			peakCircPressureValue_.setPrecision(boundedDatum.data.precision);

			// Turn Show flag back on
			peakCircPressureValue_.setShow(TRUE);

			// Flash the value if it's out of range
			if ( boundedDatum.clippingState == BoundedValue::ABSOLUTE_CLIP )
			{											// $[TI35]
				peakCircPressureValue_.
					setColor(Colors::WHITE_ON_EXTRA_DARK_GREY_BLINK);
			}
			else
			{											// $[TI36]
				peakCircPressureValue_.setColor(Colors::WHITE);
			}
			break;

		case PatientDataId::MEAN_CCT_PRESS_ITEM:				// $[TI37]
			meanCircPressureValue_.setValue(boundedDatum.data.value);
			meanCircPressureValue_.setPrecision(boundedDatum.data.precision);

			// Turn Show flag back on
			meanCircPressureValue_.setShow(TRUE);

			// Flash the value if it's out of range
			if ( boundedDatum.clippingState == BoundedValue::ABSOLUTE_CLIP )
			{											// $[TI40]
				meanCircPressureValue_.
					setColor(Colors::WHITE_ON_EXTRA_DARK_GREY_BLINK);
			}
			else
			{											// $[TI41]
				meanCircPressureValue_.setColor(Colors::WHITE);
			}
			break;

		case PatientDataId::INSP_TIDAL_VOL_ITEM:		// $[TI49]
			deliveredVolumeValue_.setValue(boundedDatum.data.value);
			deliveredVolumeValue_.setPrecision(boundedDatum.data.precision);

			// Turn Show flag back on
			deliveredVolumeValue_.setShow(TRUE);

			// Flash the value if it's out of range
			if ( boundedDatum.clippingState == BoundedValue::ABSOLUTE_CLIP )
			{											// $[TI40]
				deliveredVolumeValue_.
					setColor(Colors::WHITE_ON_EXTRA_DARK_GREY_BLINK);
			}
			else
			{											// $[TI41]
				deliveredVolumeValue_.setColor(Colors::WHITE);
			}
			break;

		case PatientDataId::BREATH_TYPE_DISPLAY_ITEM:		// $[TI42]
			// Breath type has not timed out so display it.  Check which
			// type it is and set text appropriately.
			currentBreathType_ = (BreathType)GuiApp::GetGuiBreathType();

			updateBreathBar_();
			break;

		case PatientDataId::BREATH_PHASE_ITEM:				// $[TI48]
			// Check which breath phase it is and set the colors of the breath
			// status display accordingly.

			currentBreathPhase_ = 
				(BreathPhaseType::PhaseType)GuiApp::GetGuiBreathPhase();

			updateBreathBar_();
			break;

		case PatientDataId::IS_PEEP_RECOVERY_ITEM:
			break;

		default:
			CLASS_ASSERTION_FAILURE();
			break;
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: batchSettingUpdate
//
//@ Interface-Description
//  Called when batch settings in this subscreen changed.  Passed the
//  following parameters:
// >Von
//  qualifierId	The context in which the change happened, either
//					ACCEPTED or ADJUSTED.
//  pSubject	The pointer to the Setting's Context subject.

//	settingId	The enum id of the setting which changed.
// >Voff

//---------------------------------------------------------------------
//@ Implementation-Description
// We are only interested in changes to the Accepted Context so we
// ignore all others.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
	VitalPatientDataArea::batchSettingUpdate(
											const Notification::ChangeQualifier qualifierId,
											const ContextSubject*               pSubject,
											const SettingId::SettingIdType      settingId
											)
{
	CALL_TRACE("batchSettingUpdate(qualifierId, pSubject, settingId)");

	if ( qualifierId == Notification::ACCEPTED )
	{								 // $[TI1.1]
		updateDataComponents_();
	}								 // $[TI1.2]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setMode_
//
//@ Interface-Description
// Depending on the state of various indicators (the current GUI state,
// whether communications is up or down etc.) decides which display
// mode to set.
//---------------------------------------------------------------------
//@ Implementation-Description
// Checks the states of various BD and GUI indicators and decides what
// display mode to set.
// $[00539] GUI shall enter Vent startup with message indicating the serial
// numbers do not match.
// $[00525] If short power failure or not AC switch, and ....., and serial
// numbers match ...
// and serial numbers match.
// $[00526] GUI shall enter Vent startup when long power failure or AC switch
// and serial numbers match.
// $[00544] If after 10 seconds from power-up the GUI-CPU does not receive
// any message from the BD CPU ....
// $[01135] If the ventilator returns to normal ventilation, the Vital ...
// $[01324] If the ventilation never started, it does not make sense to ...
// $[01325] If the GUI unit cannot communicate with the BD unit, ...
// $[01326] If communication between the BD and GUI units is working, ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void 
	VitalPatientDataArea::setMode_(void)
{
	// The first term below indicates if Sys-Init has detected that BD is inop
	//   earlier.  The second term uses data recently received from the BD
	//   to check on its current inop status.  

	if ( (GuiApp::GetBdState() == STATE_INOP)
		 || (BdGuiEvent::GetEventStatus(EventData::VENT_INOP) ==
			 EventData::ACTIVE) )
	{													// $[TI1]
		// If we are already in displaying patient data (normal) mode or
		// in displaying patient disconnected (no vent) mode,
		// then go to Vent Inop No Ventilation mode (display no ventilation
		// time for vent inoperative), otherwise go to Ventilator Inop mode
		// (display vent inoperative) as long as we're not already in Vent
		// Inop No Ventilation mode.
		if ( MODE_NORMAL == displayMode_ ||
			 MODE_NO_VENT == displayMode_ )
		{												// $[TI1.1]
			setDisplayMode(MODE_INOP_NO_VENT);
		}
		else if ( MODE_INOP_NO_VENT != displayMode_ )
		{												// $[TI1.2]
			setDisplayMode(MODE_INOP);
		}												// $[TI1.3]
	}
	// Check for communication down, or timeout
	// If communications are down or the GUI timeout mode and the BD is
	// online then we have a malfunction.
	else if ( ! CommTaskAgent::IsCommunicationsUp()
			  || (GuiApp::GetGuiState() == STATE_TIMEOUT)
			  || GuiApp::IsServiceRequired() )
	{													// $[TI2]
		// This is a malfunction
		setDisplayMode(MODE_MALFUNCTION);
	}
	// Check for GUI inop
	// If GUI is in INOP mode and
	// if the BD is online then we have a malfunction.
	// if the BD is failed state then TUV had detected failure on BD
	// so we have inop.
	else if ( GuiApp::GetGuiState() == STATE_INOP )
	{													// $[TI3]
		if ( STATE_FAILED == TaskControlAgent::GetBdState() )
		{												// $[TI3.1]
			// This is a TUV failure
			if ( MODE_NORMAL == displayMode_ ||
				 MODE_NO_VENT == displayMode_ )
			{											// $[TI3.1.1]
				setDisplayMode(MODE_INOP_NO_VENT);
			}
			else if ( MODE_INOP_NO_VENT != displayMode_ )
			{											// $[TI3.1.2]
				setDisplayMode(MODE_INOP);
			}											// $[TI3.1.3]
		}
		else
		{	// This is a malfunction                    // $[TI3.2]
			setDisplayMode(MODE_MALFUNCTION);
		}
	}
#ifndef SIGMA_DEVELOPMENT
	// If BD and GUI flash table does not match then we goes to service
	// required state.

	// TODO E600 MS. Set this to be always false until we're done with the serial number
	// and datakey port.
	else if ( FALSE /*!GuiApp::IsBdAndGuiFlashTheSame() */ )
	{													// $[TI4]
		setDisplayMode(MODE_SERVICE_REQUIRED);
	}
	// If datakey serial numbers are not valid then we goes to service required
	// state.
	else if ( GuiApp::SERIAL_NUMBER_OK != GuiApp::GetSerialNumberInfoState() )
	{													// $[TI5]
		setDisplayMode(MODE_SERVICE_REQUIRED);
	}
#endif // SIGMA_DEVELOPMENT
	else if ( LowerScreen::RLowerScreen.isInPatientSetupMode() )
	{													// $[TI6]
		// Operator has not completed patient setup, check to see if
		// a patient is connected
		if ( BdGuiEvent::GetEventStatus(EventData::PATIENT_CONNECT) ==
			 EventData::ACTIVE )
		{												// $[TI6.1]
			if ( BdGuiEvent::GetEventStatus(EventData::OCCLUSION) ==
				 EventData::ACTIVE )
			{											// $[TI6.1.1]
				// Put up the "no ventilation" message.
				setDisplayMode(MODE_NO_VENT);
			}
			else
			{											// $[TI6.1.2]
				// Display patient data
				setDisplayMode(MODE_NORMAL);
			}
		}
		// If a patient was disconnected then go to No Ventilation mode
		else if ( BdGuiEvent::GetEventStatus(EventData::PATIENT_CONNECT) ==
				  EventData::CANCEL )
		{												// $[TI6.2]
			// Put up the "no ventilation" message.
			setDisplayMode(MODE_NO_VENT);
		}
		else
		{												// $[TI6.3]
			if ( BdGuiEvent::GetEventStatus(EventData::SVO) == EventData::ACTIVE )
			{											// $[TI6.3.1]
				setDisplayMode(MODE_SVO);
			}
			else
			{											// $[TI6.3.2]
				// In Vent Startup but initial patient connection has not occurred
				// $[01010], $[01132]
				setDisplayMode(MODE_VENT_STARTUP);
			}       
		}
	}
	else
	{													// $[TI7]
		// Patient has been setup and the hope is that we are ventilating
		// normally.  Let's check conditions.

		// Check if the patient has never been connected (status is IDLE)
		if ( BdGuiEvent::GetEventStatus(EventData::PATIENT_CONNECT) ==
			 EventData::IDLE )
		{												// $[TI7.1]
			// Patient has never been connected.  If SVO is active then
			// there has been a malfunction and we don't want the user to
			// connect a patient, display the Malfunction message.  Otherwise
			// display the Waiting for Patient Connect message.
			if ( BdGuiEvent::GetEventStatus(EventData::SVO) == EventData::ACTIVE )
			{											// $[TI7.1.1]
				setDisplayMode(MODE_SVO);
			}
			else
			{											// $[TI7.1.2]
				setDisplayMode(MODE_WAITING_FOR_PT);
			}
		}
		else
		{												// $[TI7.2]
			// Patient was previously connected

			// If we're in SVO, Occlusion or Patient Disconnect then go to
			// No Ventilation mode
			if ( (BdGuiEvent::GetEventStatus(EventData::SVO) ==
				  EventData::ACTIVE)
				 || (BdGuiEvent::GetEventStatus(EventData::OCCLUSION) ==
					 EventData::ACTIVE)
				 || (BdGuiEvent::GetEventStatus(EventData::PATIENT_CONNECT) ==
					 EventData::CANCEL) )
			{											// $[TI7.2.1]
				// Put up the "no ventilation" message.
				setDisplayMode(MODE_NO_VENT);
			}
			else
			{											// $[TI7.2.2]
				// Must be in apnea or normal ventilation, display patient data
				setDisplayMode(MODE_NORMAL);
			}
		}
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateNoVentDisplay_
//
//@ Interface-Description
// Updates the elapsed time portion of the "no vent" display.
//---------------------------------------------------------------------
//@ Implementation-Description
// Called every time the elapsed time portion of the "no vent" or
// "vent inop for " banner is to be updated, typically once every second.
// When the "no vent" or "vent inop for " mode begins, there is an internal
// time mark (noVentStartTime_ from NovRam) that saves the wall clock time
// when the mode began.  The delta between the current time and the saved
// time is the elapsed time displayed.  When the elapsed time reaches 10
// minutes, the elapsed time is no longer updated.
//
// $[01134] VPDA should display a timed no ventilation message ...
//---------------------------------------------------------------------
//@ PreCondition
// The current display mode (displayMode_) must be "no vent" and the
// saved wall clock time (noVentStartTime_) must be valid.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void 
	VitalPatientDataArea::updateNoVentDisplay_(void)
{
	CALL_TRACE("VitalPatientDataArea::updateNoVentDisplay_(void)");
	SAFE_CLASS_PRE_CONDITION(MODE_NO_VENT == displayMode_ ||
							 MODE_INOP_NO_VENT == displayMode_);
	SAFE_CLASS_PRE_CONDITION(!noVentStartTime_.isInvalid());

	TimeStamp currentTime;

	// Compute the number of elapsed seconds since the "no vent" mode started.
	// Convert the seconds into an hours:minutes:seconds display in Cheap Text
	// format.  If the elapsed time exceeds the given time limit,
	// then stop the display update. 
	Int32       seconds;				// number of elapsed seconds
	wchar_t        buf[200];				// string assembly area   
	wchar_t        typeOfNoVentBuf[200];	// string for specific type of no vent  
										// title
	const int   TIME_LIMIT = 600;		// ten minutes

	NovRamManager::GetNoVentStartTime(noVentStartTime_);
	currentTime.now();
	seconds = (currentTime - noVentStartTime_) / 1000;	// Convert from mSecs


	// The NO_VENT_LABEL and INOP_NO_VENT_LABEL strings are actually
	// format strings that are a little longer than the final result,
	// so this assert is on the conservative side.
	 
	CLASS_ASSERTION(wcslen(MiscStrs::NO_VENT_LABEL) < sizeof(buf));     
	CLASS_ASSERTION(wcslen(MiscStrs::INOP_NO_VENT_LABEL) < sizeof(buf));    

	if ( MODE_INOP_NO_VENT == displayMode_ )
	{													// $[TI1.1]
		wcscpy(typeOfNoVentBuf, MiscStrs::INOP_NO_VENT_LABEL);  
	}
	else
	{													// $[TI1.2]
		wcscpy(typeOfNoVentBuf, MiscStrs::NO_VENT_LABEL);  
	}

	// If the elapsed time goes above the time limit then display a
	// message of the form 'No ventilation for > 00:10:00' and cancel the
	// "No Vent" timer.  Otherwise display the actual elapsed time.
	if ( seconds > TIME_LIMIT )
	{													// $[TI2.1]
		GuiTimerRegistrar::CancelTimer(GuiTimerId::NO_VENT_CLOCK);

		swprintf(buf, typeOfNoVentBuf,
				L"> ", TIME_LIMIT / 3600, (TIME_LIMIT / 60) % 60,
				TIME_LIMIT % 60);
	}
	else
	{													// $[TI2.2]
		// Display new elapsed time
		swprintf(buf, typeOfNoVentBuf,
				L"", seconds / 3600, (seconds / 60) % 60, seconds % 60);
	}

	bigMessage_.setText(buf);
	bigMessage_.positionInContainer(GRAVITY_CENTER);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: removeDataContainerDrawables_
//
//@ Interface-Description
// Make sure the patient data field labels, units and values are hidden.
//---------------------------------------------------------------------
//@ Implementation-Description
// Called every time when vent goes into mode other than MODE_NORMAL.
// The container which holds all vital patient data shall be removed.
// The individual vital patient data shall set show to FALSE.  This will
// allow each vital patient data being displayed individually.
//
// $[01126] If there is no valid vital patient data value, then ...
// $[01134] VPDA should display a timed no ventilation message ...
//---------------------------------------------------------------------
//@ PreCondition
// None.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void 
	VitalPatientDataArea::removeDataContainerDrawables_(void)
{
	CALL_TRACE("VitalPatientDataArea::removeDataContainerDrawables_(void)");

	// Make sure the patient data field labels and values are hidden.
	removeDrawable(&dataContainer_);

	// Turn Show flag off
	ieRatioValue_.setShow(FALSE);
	totalRespiratoryRateValue_.setShow(FALSE);
	endExhalationPressureValue_.setShow(FALSE);
	exhaledTidalVolumeValue_.setShow(FALSE);
	exhaledMinuteVolumeValue_.setShow(FALSE);
	peakCircPressureValue_.setShow(FALSE);
	meanCircPressureValue_.setShow(FALSE);
	deliveredVolumeValue_.setShow(FALSE);
	breathStatusText_.setText(NULL_STRING_ID);

	// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate) [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.).  The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
	VitalPatientDataArea::SoftFault(const SoftFaultID  softFaultID,
									const Uint32       lineNumber,
									const char*        pFileName,
									const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, VITALPATIENTDATAAREA,
							lineNumber, pFileName, pPredicate);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateDataComponents_()
//
//@ Interface-Description
//  Configures the vital patient data components based on the latest
//  batch of accepted settings.
//---------------------------------------------------------------------
//@ Implementation-Description
// $[01128] The Vital Patient Data area shall display...
// $[LC02003]\d\ in CPAP, the Vte and Ve tot displays shall be removed...
// $[LC02003]\e\ in CPAP, measured PEEP Pe end shall be displayed
// $[LC02001]\d\ exiting CPAP, VTE and VE TOT displays shall be restored...
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
	VitalPatientDataArea::updateDataComponents_(void)
{
	CALL_TRACE("updateDataComponents_()");

	Setting*  pSetting = SettingsMgr::GetSettingPtr(SettingId::VENT_TYPE);
	const DiscreteValue  VENT_TYPE = pSetting->getAcceptedValue();
	pSetting = SettingsMgr::GetSettingPtr(SettingId::MODE);
	const DiscreteValue  MODE = pSetting->getAcceptedValue();


	pSetting = SettingsMgr::GetSettingPtr(SettingId::PROX_ENABLED);
	const DiscreteValue  PROX_ENABLED_VALUE = pSetting->getAcceptedValue();

	// $[PX01004] Add the prox symbol when PROX is enabled.
	if ((PROX_ENABLED_VALUE == ProxEnabledValue::PROX_ENABLED) && isProxReady_ && !isProxInop_ && 
        isVentNotInStartupOrWaitForPT())
	{

		exhaledTidalVolumeLabel_.setText(MiscStrs::TOUCHABLE_PROXIMAL_EXH_TIDAL_VOL_LABEL);
		exhaledTidalVolumeLabel_.setMessage(MiscStrs::TOUCHABLE_PROXIMAL_EXH_TIDAL_VOL_MSG);
		exhaledMinuteVolumeLabel_.setText(MiscStrs::TOUCHABLE_PROXIMAL_EXH_MINUTE_VOL_LABEL);
		exhaledMinuteVolumeLabel_.setMessage(MiscStrs::TOUCHABLE_PROXIMAL_EXH_MINUTE_VOL_MSG);
        deliveredVolumeLabel_.setText(MiscStrs::TOUCHABLE_PROXIMAL_DELIVERED_VOL_LABEL);
	}
	else
	{
		exhaledTidalVolumeLabel_.setText(MiscStrs::TOUCHABLE_EXH_TIDAL_VOL_LABEL);
		exhaledTidalVolumeLabel_.setMessage(MiscStrs::TOUCHABLE_EXH_TIDAL_VOL_MSG);
		exhaledMinuteVolumeLabel_.setText(MiscStrs::TOUCHABLE_EXH_MINUTE_VOL_LABEL);
		exhaledMinuteVolumeLabel_.setMessage(MiscStrs::TOUCHABLE_EXH_MINUTE_VOL_MSG);
        deliveredVolumeLabel_.setText(MiscStrs::TOUCHABLE_DELIVERED_VOL_LABEL);

	}


	if ( VentTypeValue::NIV_VENT_TYPE == VENT_TYPE )
	{											 // $[TI1.1]
		dataContainer_.removeDrawable(&endExhalationPressureLabel_);
		dataContainer_.removeDrawable(&endExhalationPressureValue_);
		dataContainer_.addDrawable(&deliveredVolumeLabel_);
		dataContainer_.addDrawable(&deliveredVolumeValue_);


	}
	else
	{											 // $[TI1.2]
		dataContainer_.addDrawable(&endExhalationPressureLabel_);
		dataContainer_.addDrawable(&endExhalationPressureValue_);
		dataContainer_.removeDrawable(&deliveredVolumeLabel_);
		dataContainer_.removeDrawable(&deliveredVolumeValue_);
	}


	if ( ModeValue::CPAP_MODE_VALUE == MODE )
	{
		dataContainer_.removeDrawable(&exhaledTidalVolumeLabel_);
		dataContainer_.removeDrawable(&exhaledTidalVolumeValue_);
		dataContainer_.removeDrawable(&exhaledMinuteVolumeLabel_);
		dataContainer_.removeDrawable(&exhaledMinuteVolumeValue_);

		endExhalationPressureLabel_.setX(COL6_LABEL_X_ - 20);
		endExhalationPressureLabel_.setY(ROW1_LABEL_Y_);
		dataContainer_.addDrawable(&endExhalationPressureLabel_);

		endExhalationPressureValue_.setX(COL6_VALUE_X_);
		endExhalationPressureValue_.setY(ROW2_VALUE_Y_);
		dataContainer_.addDrawable(&endExhalationPressureValue_);

	}
	else
	{
		dataContainer_.addDrawable(&exhaledTidalVolumeLabel_);
		dataContainer_.addDrawable(&exhaledTidalVolumeValue_);
		dataContainer_.addDrawable(&exhaledMinuteVolumeLabel_);
		dataContainer_.addDrawable(&exhaledMinuteVolumeValue_);    

		endExhalationPressureLabel_.setX(COL3_LABEL_X_);
		endExhalationPressureLabel_.setY(ROW1_LABEL_Y_);

		endExhalationPressureValue_.setX(COL3_VALUE_X_);
		endExhalationPressureValue_.setY(ROW2_VALUE_Y_);

		if ( VentTypeValue::NIV_VENT_TYPE == VENT_TYPE )
		{											 // $[TI1.1]
			dataContainer_.removeDrawable(&endExhalationPressureLabel_);
			dataContainer_.removeDrawable(&endExhalationPressureValue_);
		}
		else
		{											 // $[TI1.2]
			dataContainer_.addDrawable(&endExhalationPressureLabel_);
			dataContainer_.addDrawable(&endExhalationPressureValue_);
		}              


	}

}

