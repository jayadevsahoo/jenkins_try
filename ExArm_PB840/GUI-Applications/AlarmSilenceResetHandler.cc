#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: AlarmSilenceResetHandler -  Handles pressing of the Alarm Silence
// and Alarm Reset keys.
//---------------------------------------------------------------------
//@ Interface-Description
// This is a class dedicated to the handling of the offscreen keys devoted
// to alarms, namely the Alarm Silence and Alarm Reset keys.
//
// The keyPanelPressHappened() methods inform us of the pressing of these
// keys and the timerEventHappened() method indicates when the timeout
// related to the Alarm Silence key has expired.  guiEventHappened()
// method shall cancel alarm silence request.
//---------------------------------------------------------------------
//@ Rationale
// Something needs to handle these alarm keys and this is it.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply register for Alarm Silence and Reset key press and Alarm Silence
// timeout event callbacks and inform the Alarm Analysis subsystem of the
// particular events.
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and pre-conditions
// to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
// Only one instance of this class should be created.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/AlarmSilenceResetHandler.ccv   25.0.4.1   20 Nov 2013 17:35:34   rhjimen  $
//
//@ Modification-Log
//
//  Revision: 014  By: rpr    Date:  6-Nov-2012    SCR Number: 6777
//  Project:  BI
//  Description:
//      Removed all monitor software.
// 
//  Revision: 013  By: gdc     Date: 26-May-2007     SCR Number: 6330
//  Project:  Trend
//	Description:
//  Removed SUN prototype code.
//
//  Revision: 012  By: ljstar  Date: 13-Aug-2003     SCR Number: 
//  Project:  VCP
//	Description:
//  inserted missing function parentheses related to turning off the Remote Nurse Alarm
//
//  Revision: 011 By: srp    Date: 28-May-2002   DR Number: 5908
//  Project:  VCP
//	Description:
//		Incorporated Remediation code review issue(s) -- commentary changes only.
//
//  Revision: 010  By:  gfu	   Date:  24-Apr-2002    DCS Number: 5908
//  Project:  VCP
//  Description:
//	Modified switch statment structure in guiEventHappened to adhere
//	to coding standards.
//
//  Revision: 009  By: erm     Date:  13-Dec-2001    DCS Number: 5976
//  Project: GuiComms
//  Description:
//   Modified time monitor to OsTimeStamp instead of TimeStamp
//
//  Revision: 008  By:  hhd	   Date:  08-Feb-2000    DCS Number: 5504
//  Project:  NeoMode
//  Description:
//		Modified to provide additional off-screen key management, whereby Alarm Silence can be cancelled.
//
//  Revision: 007  By:  hhd	   Date:  17-Nov-1999    DCS Number: 5327
//  Project:  NeoMode
//  Description:
//		Initial version.
//		Modified to reduce header file dependencies.
//
//   Revision 006   By:   yyy      Date: 18-Aug-1999    DR Number:   5513
//      Project:   ATC
//      Description:
//         Modified to handle alarm lock and break through related features.
//
//  Revision: 005  By:  hhd	   Date:  19-Jul-1999    DCS Number: 5331
//  Project:  ATC
//  Description:
//      Modified so that if AlarmSilence key is pressed while vent is in Inoperative
//      mode, Nurse calls will not be turned off.
//
//  Revision: 004  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 003  By:  sah    Date:  28-Jul-1997    DCS Number: 2320
//  Project:  Sigma (R8027)
//  Description:
//      Now notifying 'AlarmStateManager' when Alarm Reset Key is pressed.
//
//  Revision: 002  By: yyy      Date: 15-May-1997  DR Number: 1878
//    Project:  Sigma (R8027)
//    Description:
//      Generated an ALARM_RESET BD event to force the MoreData
//		subscreen to display the appropriate labels.
//
//  Revision: 001  By:  mpm Date:  22-AUG-95    DR Number:
//  Project:  Sigma (R8027)
//  Description:
//      Integration baseline.
//====================================================================

#include "AlarmSilenceResetHandler.hh"

#include "Sigma.hh"
#include "Sound.hh"
#include "BdGuiEvent.hh"

//@ Usage-Classes
#include "AlarmAnnunciator.hh"
#include "AlarmSetupSubScreen.hh"
#include "AlarmStateManager.hh"
#include "GuiEventRegistrar.hh"
#include "GuiTimerRegistrar.hh"
#include "KeyPanel.hh"
#include "LowerSubScreenArea.hh"
#include "MsgQueue.hh"
#include "MiscStrs.hh"
#include "MessageArea.hh"
// TODO E600 remove
//#include "OsUtil.hh"
#include "PatientDataRegistrar.hh"
#include "UpperSubScreenArea.hh"
#include "MoreDataSubScreen.hh"
#include "OffKeysSubScreen.hh"

//@ End-Usage
Boolean AlarmSilenceResetHandler::manualAlarmsResetRequested_ = FALSE;

//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: AlarmSilenceResetHandler()  [Default Constructor]
//
//@ Interface-Description
// Creates the Alarm Silence/Reset Handler object.
//---------------------------------------------------------------------
//@ Implementation-Description
// Register for Alarm Silence and Reset Key, Alarm Silence timeout event
// and Alarm silence timer tick event callbacks.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

AlarmSilenceResetHandler::AlarmSilenceResetHandler(void)
{
	CALL_TRACE("AlarmSilenceResetHandler::AlarmSilenceResetHandler(void)");

	// Register for Alarm Silence and Reset key presses
	KeyPanel::SetCallback(KeyPanel::ALARM_SILENCE_KEY, this);
	KeyPanel::SetCallback(KeyPanel::ALARM_RESET_KEY, this);

	// Register for GUI event change callbacks.
	GuiEventRegistrar::RegisterTarget(this);

	// Register for Alarm Silence timeouts.  Alarm Silence timeouts are
	// handled internally by the Alarm-Analysis subsystem and then posted
	// to the GuiApp task as a timeout event so all we have to do is
	// register for the timeout, we do not have to start/stop the event.
	GuiTimerRegistrar::RegisterTarget(GuiTimerId::ALARM_SILENCE_TIMEOUT, this);

	// Register for transition to hard Alarm Silence timer.
	GuiTimerRegistrar::RegisterTarget(GuiTimerId::ALARM_PROGRESS_TIMER_TICK, this);
														// $[TI1] 
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~AlarmSilenceResetHandler  [Destructor]
//
//@ Interface-Description
// Destroys the Alarm Silence/Reset key handler object.  Does nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Does nothing 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

AlarmSilenceResetHandler::~AlarmSilenceResetHandler(void)
{
	CALL_TRACE("AlarmSilenceResetHandler::~AlarmSilenceResetHandler(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: keyPanelPressHappened
//
//@ Interface-Description
// Called when the Alarm Silence or Rest key is pressed.  
//---------------------------------------------------------------------
//@ Implementation-Description
// Display the offscreen key help message for the corresponding key.
// For alarm silence key, we inform the Alarm-Analysis subsystem of
// the alarm silence request, turn on the Alarm Silence LED, clear the
// remote nurse alarm, start the timer and update the offscreen subscreen.  
// For alarm reset key, simply call Cancel() to reset the alarm request.
//
// Note: pressing of these keys is invalid during service mode so just make
// the invalid entry sound in this case.
// $[01289] During SST, Alarm silence, alarm reset, manual inspiration ...
// $[07034] During service mide, Alarm silence, alarm reset, help, ...
// $[05025] During alarm silence, an indicator shall ...
// $[01006] The GUI must be able to display the meaning of ...
//---------------------------------------------------------------------
//@ PreCondition
//  KeyId must be one of ALARM_SILENCE_KEY or ALARM_RESET_KEY.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
AlarmSilenceResetHandler::keyPanelPressHappened(KeyPanel::KeyId keyId)
{
	CALL_TRACE("AlarmSilenceResetHandler::keyPanelPressHappened(KeyPanel::KeyId)");

	if (keyId == KeyPanel::ALARM_SILENCE_KEY)
	{													// $[TI1.1]
		// Display the offscreen key help message 
		GuiApp::PMessageArea->setMessage(
						MiscStrs::ALARM_SILENCE_HELP_MESSAGE,
								MessageArea::MA_HIGH);
	}
	else if (keyId == KeyPanel::ALARM_RESET_KEY)
	{													// $[TI1.2]
		// Display the offscreen key help message 
		GuiApp::PMessageArea->setMessage(
						MiscStrs::ALARM_RESET_HELP_MESSAGE,
								MessageArea::MA_HIGH);
	}													// $[TI1.3]

	// Only react to keys during normal ventilation mode.
	if (GuiApp::GetGuiState() != STATE_SERVICE &&
		GuiApp::GetGuiState() != STATE_SST &&
		GuiApp::GetGuiState() != STATE_INIT)
	{													// $[TI2.1]
		switch (keyId)
		{
		case KeyPanel::ALARM_SILENCE_KEY:				// $[TI2.1.1]
			// Inform Alarm-Analysis subsystem of event
			AlarmAnnunciator::AlarmSilence();

			// Switch on the key panel LED
			KeyPanel::SetLight(KeyPanel::ALARM_SILENCE_KEY, TRUE);

			// Switch off the remote nurse alarm
			// $[01354] When the alarm silence function become active ...
			if ( !GuiApp::IsSstFailure() && !GuiApp::IsServiceRequired())
			{  // $[TI2.1.1.1]

				// TODO E600 port
				//::ClearRemoteAlarm();		// Make sure remote alarm is off
			}  // $[TI2.1.1.2]

			// Inform TimerRegistrar of event
			GuiTimerRegistrar::StartTimer(GuiTimerId::ALARM_PROGRESS_TIMER_TICK);

			// Update Offkeys subScreen
			LowerSubScreenArea::GetOffKeysSubScreen()->updatePanel(EventData::ALARM_RESET,
																EventData::ACTIVE);
			startTime_.now();
			break;

		case KeyPanel::ALARM_RESET_KEY:					// $[TI2.1.2]
		{
			// Alarm reset shall reset the hard alarm silence
			AlarmSilenceResetHandler::manualAlarmsResetRequested_ = TRUE;
			Cancel();
			AlarmSilenceResetHandler::manualAlarmsResetRequested_ = FALSE;
		}
			break;

		default:										// $[TI2.1.3]
			CLASS_ASSERTION(FALSE);		// tsk, tsk ... code maintenance problem
			break;
		}
	}
	else
	{													// $[TI2.2]
		// Illegal key press during service mode
		Sound::Start(Sound::INVALID_ENTRY);
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: keyPanelReleaseHappened
//
//@ Interface-Description
// Called when the Alarm Reset or Alarm Silenct key is released.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Simply turn off the offscreen help message.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
AlarmSilenceResetHandler::keyPanelReleaseHappened(KeyPanel::KeyId)
{
	CALL_TRACE("AlarmSilenceResetHandler::keyPanelReleaseHappened(KeyPanel::KeyId)");

	//								 $[TI1]
	// Clear the off screen key message
	GuiApp::PMessageArea->clearMessage(MessageArea::MA_HIGH);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Cancel
//
//@ Interface-Description
// Called when the Alarm Reset or Alarm Silence key is released.
//---------------------------------------------------------------------
//@ Implementation-Description
// For alarm reset request, inform BD of Alarm Reset event, post the
// Alarm Reset Event to the TUV queue, inform Alarm-Analysis of such
// event through queue posting, hide the patient data pointers on all
// all alarm sliders and reset data stored in the Patient Data Registrar.
// For alarm silence cancel request, notify the alarm-state manager of 
// alarm-reset state.  Update Offkey subscreen and MoreData subscreen for both.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
AlarmSilenceResetHandler::Cancel(void)
{
	CALL_TRACE("AlarmSilenceResetHandler::Cancel(void)");

	//								 $[TI1]
	if (GuiApp::GetGuiState() != STATE_SERVICE &&
		GuiApp::GetGuiState() != STATE_SST &&
		GuiApp::GetGuiState() != STATE_INIT)
	{	//									 $[TI1.1]
		// Stop and clear the "alarm transition" timer in case it is going.
		GuiTimerRegistrar::CancelTimer(GuiTimerId::ALARM_PROGRESS_TIMER_TICK);

		// Update Offkeys subScreen
		LowerSubScreenArea::GetOffKeysSubScreen()->updatePanel(EventData::ALARM_RESET, EventData::CANCEL);


		if (AlarmSilenceResetHandler::manualAlarmsResetRequested_)
		{	//								 $[TI1.1.1]
			// Inform BD of Alarm Reset event
			BdGuiEvent::RequestUserEvent(EventData::ALARM_RESET);

			// Inform Alarm-Analysis of event
			MsgQueue alarmQ(OPERATING_PARAMETER_EVENT_Q);
			alarmQ.putMsg(MANUAL_RESET_EN);

			// Hide the Patient Data pointers on all alarm sliders (until new
			// values are available from the next patient breath).
			LowerSubScreenArea::GetAlarmSetupSubScreen()->hideAllDataPointers();
			LowerSubScreenArea::GetAlarmSetupSubScreen()->hideAllStdDeviationBoxes();
			PatientDataRegistrar::ResetPatientValue();

		}
		else
		{	//									 $[TI1.1.2]
			// Inform the Alarm-Analysis subsystem that alarm silence is cancelled
			AlarmAnnunciator::CancelAlarmSilence();
		}
		
		// notify the alarm-state manager of alarm-silence and reset state...
		AlarmStateManager::ManualAlarmsResetActivated();

		// Force MoreData subscreen to update the most recent display
		// In general, when we sent a BD event request, we should get a
		// BdEvent callback.  However, for ALARM_RESET, this is not
		// guaranteed.  So we simply force the refresh to happend here.
		UpperSubScreenArea::GetMoreDataSubScreen()->bdEventHappened(
												EventData::ALARM_RESET,
												EventData::COMPLETE);

		// Clear the off screen key message
		GuiApp::PMessageArea->clearMessage(MessageArea::MA_MEDIUM_LOW);

	}	//									 $[TI1.2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: 	ClearDisplay
//
//@ Interface-Description
// Called when the break through type of Alarm is active by
// AlarmStateManager
//---------------------------------------------------------------------
//@ Implementation-Description
//	Simply stop the ALARM_PROGRESS_TIMER_TICK timer, and inform the
//  offkey subscreen of such event to deactivate this display.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
AlarmSilenceResetHandler::ClearDisplay(void)
{
	CALL_TRACE("AlarmSilenceResetHandler::ClearDisplay(void)");


	if (GuiApp::GetGuiState() != STATE_SERVICE &&
		GuiApp::GetGuiState() != STATE_SST &&
		GuiApp::GetGuiState() != STATE_INIT)
	{	//								 $[TI1]
		// Inform BD of Alarm Reset event
		// Stop and clear the "alarm transition" timer in case it is going.
		GuiTimerRegistrar::CancelTimer(GuiTimerId::ALARM_PROGRESS_TIMER_TICK);

		// Update Offkeys subScreen
		LowerSubScreenArea::GetOffKeysSubScreen()->updatePanel(EventData::ALARM_RESET, EventData::CANCEL);
	}	//								 $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: timerEventHappened
//
//@ Interface-Description
// Called when the Alarm Silence timer runs out (the Alarm-Analysis subsystem
// handles the particular timing) or the Alarm Silence is timed out.  
//---------------------------------------------------------------------
//@ Implementation-Description
// When Alarm Silence timer is timed-out, we have to switch off
// the Alarm Silence key panel LED, inform the Alarm-Analysis subsystem
// that alarm silence is cancelled, reset all patient data value
// stored in PatientDataRegistrar, cancel the timer and update offkey 
// subscreen.  Or when ALARM_PROGRESS_TIMER_TICK runs
// out, simply inform the offkey subscreen to update the timer bar.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
AlarmSilenceResetHandler::timerEventHappened(GuiTimerId::GuiTimerIdType timerId)
{
	CALL_TRACE("AlarmSilenceResetHandler::timerEventHappened(GuiTimerId::GuiTimerIdType)");

	switch (timerId)
	{
	case GuiTimerId::ALARM_SILENCE_TIMEOUT:
		// $[TI1]
		// Switch off the Alarm Silence key panel LED
		KeyPanel::SetLight(KeyPanel::ALARM_SILENCE_KEY, FALSE);

		// Inform the Alarm-Analysis subsystem that alarm silence is cancelled
		AlarmAnnunciator::CancelAlarmSilence();
		PatientDataRegistrar::ResetPatientValue();

		// Update Offkeys subScreen
		GuiTimerRegistrar::CancelTimer(GuiTimerId::ALARM_PROGRESS_TIMER_TICK);

		LowerSubScreenArea::GetOffKeysSubScreen()->updatePanel(EventData::ALARM_RESET,
																EventData::COMPLETE);
		break;
		
	case GuiTimerId::ALARM_PROGRESS_TIMER_TICK:
		// $[TI2]
		// If timer is already canceled, this event callback might still be called.
		// To be sure, we prevent the OffKey subscreen from being redrawn here.
		if (GuiTimerRegistrar::isActive(GuiTimerId::ALARM_PROGRESS_TIMER_TICK))
		{	   // $[TI2.1]
			LowerSubScreenArea::GetOffKeysSubScreen()->updatePanel(EventData::ALARM_RESET,
																EventData::PLATEAU_ACTIVE,
																EventData::NULL_EVENT_PROMPT,
																(startTime_.getPassedTime())/1000);
		}	   // $[TI2.2]
		break;

	default:
		AUX_CLASS_ASSERTION(FALSE, timerId);	
		break;
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: guiEventHappened
//
//@ Interface-Description
// Called when a registerd change in the GUI App event happens.  Passed the id of the
// event which changed.  
//---------------------------------------------------------------------
//@ Implementation-Description
// If the eventId is INOP, SETTINGS_LOCKOUT, COMMUNICATIONS_DOWN, or if
// it is SETTINGS_TRANSACTION_SUCCESS and the SettingsTransactionSuccess flag
// is not set then we shall dismiss the alarm silence request.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
AlarmSilenceResetHandler::guiEventHappened(GuiApp::GuiAppEvent eventId)
{
	CALL_TRACE("guiEventHappened(GuiApp::GuiAppEvent eventId)");

	switch (eventId)
	{
	case GuiApp::SETTINGS_TRANSACTION_SUCCESS:
		if (GuiApp::IsSettingsTransactionSuccess())
		{											// $[TI1.1.1]
			break;
		}											// $[TI1.1.2]
	case GuiApp::INOP:								// $[TI1.2]
	case GuiApp::COMMUNICATIONS_DOWN:
	case GuiApp::SETTINGS_LOCKOUT:
		//Cancel();  //TODO E600 - commented - To bring GUI up on in PC mode, it was crashed - GUI First_Normal_Vent_Scr
		break;
		
	case GuiApp::SERVICE_READY:						// $[TI1.3]
	case GuiApp::GUI_ONLINE_READY:
	case GuiApp::PATIENT_DATA_DISPLAY:
	case GuiApp::PATIENT_SETUP_ACCEPTED:
	case GuiApp::UNDEFINED:
		break;

	default:
		// assert on illegal value
		AUX_CLASS_ASSERTION_FAILURE(eventId);
		break;
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
AlarmSilenceResetHandler::SoftFault(const SoftFaultID  softFaultID,
								    const Uint32       lineNumber,
								    const char*        pFileName,
								    const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS,
						ALARMSILENCERESETHANDLER, lineNumber, pFileName, pPredicate);
}
