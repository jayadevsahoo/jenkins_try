#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: PsolLiftoff - Proportional solenoid Liftoff class.
//---------------------------------------------------------------------
//@ Interface-Description
//      This class is used by NOVRAM to store the Psol liftoff values along
//		with the psol lookup table for the air and o2 psol.  Access methods
//		to get and set the liftoff values are provided along with methods to
//		set and get the lookup table values.
//---------------------------------------------------------------------
//@ Rationale
//      Abstraction of Psol liftoff values.
//---------------------------------------------------------------------
//@ Implementation-Description
//      Provides default values to interface with NOVRAM initialization.
//		The psol lookup table is implemented with an array.  The index of
//		the array corresponds to flow rates of (index+1) * FLOW_INCREMENT.
//
// $[00479]
//---------------------------------------------------------------------
//@ Fault-Handling
//		n/a
//---------------------------------------------------------------------
//@ Restrictions
//		None
//---------------------------------------------------------------------
//@ Invariants
//		None
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/PsolLiftoff.ccv   25.0.4.0   19 Nov 2013 13:54:10   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By: quf    Date: 26-Dec-2000   DR Number: 5832
//  Project:  Baseline
//  Description:
//		Added methods operator+() and operator/() to allow PSOL
//		liftoff calibration averaging, checkPsolTables() to check
//		for valid air and O2 PSOL tables.
//
//  Revision: 003  By: syw    Date: 02-Mar-2000   DR Number: 5684
//  Project:  NeoMode
//  Description:
//      Added initialization of psol lookup table and defined lookupPsolCommand().
//
//  Revision: 002 By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//	Description:
//		Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 001  By:  syw    Date: 08-Nov-1996    DR Number: INITIAL RELEASE
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "PsolLiftoff.hh"

//@ Usage-Classes

//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: PsolLiftoff()
//
//@ Interface-Description
//      Constructor.  This method has no arguments and has no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//      The air and O2 PSOL liftoff default values are stored along with
//		the psol lookup table.
//---------------------------------------------------------------------
//@ PreCondition
//      None
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================
PsolLiftoff::PsolLiftoff( void)
{
	CALL_TRACE("PsolLiftoff::PsolLiftoff( void)") ;

   	// $[TI1]
	const Uint DEFAULT_LIFTOFF = 813;

	airLiftoff_ = DEFAULT_LIFTOFF;
	o2Liftoff_ = DEFAULT_LIFTOFF;

	for (Uint32 ii=0; ii < PSOL_TABLE_SIZE; ii++)
	{
	   	// $[TI3]
		airPsolTable_[ii] = DEFAULT_LIFTOFF ;
		o2PsolTable_[ii] = DEFAULT_LIFTOFF ;
	}
   	// $[TI4]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~PsolLiftoff ()
//
//@ Interface-Description
//      Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//      None
//---------------------------------------------------------------------
//@ PreCondition
//      None
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================
PsolLiftoff::~PsolLiftoff( void)
{
	CALL_TRACE("PsolLiftoff::~PsolLiftoff( void)") ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: PsolLiftoff( const PsolLiftoff&)
//
//@ Interface-Description
//		Copy constructor.  This method takes a PsolLiftoff as an argument and
//		returns nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//      The object passed in is copied to the current object.
//---------------------------------------------------------------------
//@ PreCondition
//      None
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================
PsolLiftoff::PsolLiftoff( const PsolLiftoff& rPsolLiftoff)
{
	CALL_TRACE("PsolLiftoff::PsolLiftoff( const PsolLiftoff& rPsolLiftoff)");

   	// $[TI2]
	airLiftoff_ = rPsolLiftoff.airLiftoff_ ;
	o2Liftoff_ = rPsolLiftoff.o2Liftoff_ ;

	for (Uint32 ii=0; ii < PSOL_TABLE_SIZE; ii++)
	{
	   	// $[TI5]
		airPsolTable_[ii] = rPsolLiftoff.airPsolTable_[ii] ;
		o2PsolTable_[ii] = rPsolLiftoff.o2PsolTable_[ii]  ;
	}
   	// $[TI6]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: operator=( const PsolLiftoff& rPsolLiftOff)
//
//@ Interface-Description
//		This method has PsolLiftoff as an argument and returns nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//      The object passed in is copied to the current object.
//---------------------------------------------------------------------
//@ PreCondition
//      None
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================
void
PsolLiftoff::operator=(const PsolLiftoff& rPsolLiftoff) 
{
	CALL_TRACE("PsolLiftoff::operator=(const PsolLiftoff& rPsolLiftoff)");

	if (this != &rPsolLiftoff)
	{
	   	// $[TI1.1]
		airLiftoff_ = rPsolLiftoff.airLiftoff_;
		o2Liftoff_ = rPsolLiftoff.o2Liftoff_;

		for (Uint32 ii=0; ii < PSOL_TABLE_SIZE; ii++)
		{
		   	// $[TI2]
			airPsolTable_[ii] = rPsolLiftoff.airPsolTable_[ii] ;
			o2PsolTable_[ii] = rPsolLiftoff.o2PsolTable_[ii]  ;
		}
	   	// $[TI3]
	}	// implied else $[TI1.2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: operator+()
//
//@ Interface-Description
//	This method takes a PsolLiftoff object as an argument and returns a
//	PsolLiftoff object.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Add two PsolLiftoff objects item-by-item, return the resulting
//	PsolLiftoff object.
//---------------------------------------------------------------------
//@ PreCondition
//	None
//---------------------------------------------------------------------
//@ PostCondition
//	None
//@ End-Method
//=====================================================================
PsolLiftoff
PsolLiftoff::operator+(const PsolLiftoff& rPsolLiftoff) const
{
// $[TI1]
	CALL_TRACE("PsolLiftoff::operator+(const PsolLiftoff& rPsolLiftoff)");

	PsolLiftoff temp;

	temp.airLiftoff_ = airLiftoff_ + rPsolLiftoff.airLiftoff_ ;
	temp.o2Liftoff_  = o2Liftoff_  + rPsolLiftoff.o2Liftoff_ ;

	for (Uint32 ii=0; ii < PSOL_TABLE_SIZE; ii++)
	{
		temp.airPsolTable_[ii] = airPsolTable_[ii] + rPsolLiftoff.airPsolTable_[ii] ;
		temp.o2PsolTable_[ii]  = o2PsolTable_[ii]  + rPsolLiftoff.o2PsolTable_[ii]  ;
	}

	return temp;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:	operator/()
//
//@ Interface-Description
//	This method takes a Uint32 argument and returns a PsolLiftoff object.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Divide each data element of *this by the passed-in divisor, rounding
//	to the nearest 1 count, return the resulting PsolLiftoff object.
//---------------------------------------------------------------------
//@ PreCondition
//	divisor != 0
//---------------------------------------------------------------------
//@ PostCondition
//	None
//@ End-Method
//=====================================================================
PsolLiftoff
PsolLiftoff::operator/(const Uint32 divisor) const
{
//$[TI1]
	CALL_TRACE("PsolLiftoff::operator/(const Uint32 divisor)");

	CLASS_ASSERTION( divisor);

	PsolLiftoff temp;

	temp.airLiftoff_ = (Uint16)(((airLiftoff_ * 10 / divisor) + 5) / 10);
	temp.o2Liftoff_  = (Uint16)(((o2Liftoff_  * 10 / divisor) + 5) / 10);

	for (Uint32 ii=0; ii < PSOL_TABLE_SIZE; ii++)
	{
		temp.airPsolTable_[ii] = ((airPsolTable_[ii] * 10 / divisor) + 5) / 10;
		temp.o2PsolTable_[ii]  = ((o2PsolTable_[ii]  * 10 / divisor) + 5) / 10;
	}

	return temp;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:	lookupPsolCommand()
//
//@ Interface-Description
//		This method has the flow and the psol side as an argument and
//		returns the corresponding psol value.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Returns the psol value corresponding to the psol side.
//---------------------------------------------------------------------
//@ PreCondition
//      0 <= flow <= MAX_PSOL_LOOKUP_FLOW
//		side == AIR_SIDE || side == O2_SIDE
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================
Uint32
PsolLiftoff::lookupPsolCommand( const Real32 flow, const PsolSide side)
{
	CALL_TRACE("PsolLiftoff::lookupPsolCommand( const Real32 flow, const PsolSide side)") ;
	
	CLASS_PRE_CONDITION( flow >= 0.0 && flow <= MAX_PSOL_LOOKUP_FLOW) ;

	Uint32 rtnValue = 0;

	// round up flow to the nearest FLOW_INCREMENT
	Int32 index = (Int32)((flow + FLOW_INCREMENT / 2.0) * INVERSE_FLOW_INCREMENT - 1) ;

	if (side == AIR_SIDE)
	{
	   	// $[TI1]
		rtnValue = airPsolTable_[index] ;
	}
	else if (side == O2_SIDE)
	{
	   	// $[TI2]
		rtnValue = o2PsolTable_[index] ;
	}
	else
	{
		AUX_CLASS_ASSERTION( side == AIR_SIDE || side == O2_SIDE, side) ;
	}

	return( rtnValue) ; 
}


// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method:	 checkPsolTables()
//
//@ Interface - Description
//	This method takes no arguments and has no return value.
//-----------------------------------------------------------------------
//@ Implementation - Description
//	For each PSOL table, check that the delta between adjacent table values
//	is 0 or greater, i.e. values must not decrease.  Check that last
//	value of table is same as liftoff value.
//-----------------------------------------------------------------------
//@ PreCondition
//	none
//-----------------------------------------------------------------------
//@ PostCondition
//  none
//@ End - Method
//=======================================================================
void
PsolLiftoff::checkPsolTables( void)
{
	for (Int16 idx = 1; idx < PSOL_TABLE_SIZE; idx++)
	{
		CLASS_ASSERTION( airPsolTable_[idx] >= airPsolTable_[idx-1]);
		CLASS_ASSERTION( o2PsolTable_[idx]  >= o2PsolTable_[idx-1]);
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault()
//
//@ Interface-Description
//      This method takes four arguments: softFaultID, lineNumber, pFileName,
//      and pPredicate and is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//      'lineNumber' are essential pieces of information.  The 'pFileName'
//      and 'pPredicate' strings may be defaulted in the macro to reduce
//      code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
PsolLiftoff::SoftFault( const SoftFaultID  softFaultID,
                 const Uint32       lineNumber,
                 const char*        pFileName,
                 const char*        pPredicate)
{
    CALL_TRACE("SoftFault( softFaultID, lineNumber, pFileName, pPredicate)") ;
    
    FaultHandler::SoftFault( softFaultID, BD_IO_DEVICES, PSOLLIFTOFF, lineNumber,
                             pFileName, pPredicate) ;
}

//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//=====================================================================
//
//  Private Methods...
//
//=====================================================================
