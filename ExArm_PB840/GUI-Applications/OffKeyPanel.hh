#ifndef OffKeyPanel_HH
#define OffKeyPanel_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: OffKeyPanel - Generic OffKey handler class which allows
// the viewing of the progress of the key events.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/OffKeyPanel.hhv   25.0.4.0   19 Nov 2013 14:08:10   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004   By: gdc    Date: 26-Jan-2009    SCR Number: 6407
//  Project:  840S
//  Description:
//      Modified to bring source into strict ARM compliance.
//
//  Revision: 003  By: sah	Date: 10-Apr-2000   DCS Number:  5683
//  Project:  NeoMode
//  Description:
//      As part of this DCS, removed unneeded parameters from 'activatePanel()',
//      to go along with the removal of the corresponding unneeded strings.
//
//  Revision: 002  By:  hhd	   Date:  08-Feb-2000    DCS Number: 5504
//  Project:  NeoMode
//  Description:
//		Modified to provide additional off-screen key management, whereby 100% O2
//		and Alarm Silence can be cancelled.
//
//  Revision: 001  By:  yyy	   Date:  29-Jun-1998    DCS Number: 5327
//  Project:  NeoMode
//  Description:
//		Initial version.
// 	(hhd) 29-Nov-99	Modified to fix bugs.
//====================================================================

#include "Container.hh"
#include "ButtonTarget.hh"

//@ Usage-Classes
#include "TextField.hh"
#include "TextButton.hh"
#include "GuiAppClassIds.hh"
#include "EventData.hh"
#include "SubScreen.hh"
//@ End-Usage

class OffKeyPanel : public Container, 
						public ButtonTarget
{
public:
    //@ Type: UserEventCallbackPtr
    // A pointer to a function used as a call back for user events.
    typedef void (*UserEventCallbackPtr) (void);

	OffKeyPanel(Uint32 xOrg, Uint32 yOrg, Uint32 width, Uint32 height,
				Uint16 numButtons, StringId panelTitle,
				SubScreen *pFocusSubScreen, EventData::EventId eventId,
				UserEventCallbackPtr pEventCallback=NULL);
    ~OffKeyPanel(void);

	void activatePanel(StringId msg, Boolean inManeuver, Boolean displayCancelButton,
				Boolean redraw);
	virtual void deactivatePanel(void);
	void resetTitle(StringId titleMsg);
	void registerCallbackPtr(UserEventCallbackPtr pEventCallback, StringId panelMsg);
	void showPanel(Boolean redraw, Boolean displayCancelButton);


	// ButtonTarget virtual method
	virtual void buttonDownHappened(Button *pButton,
									Boolean byOperatorAction);

    static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL, 
			  const char*       pPredicate = NULL);
  
protected:

	//@ Data-Member: cancelButton_
	// The cancel command button
	TextButton cancelButton_;

	//@ Data-Member: eventId_
	// This is event id associated with this offKey panel.
	EventData::EventId eventId_;

private:
    // these methods are purposely declared, but not implemented...
    OffKeyPanel(const OffKeyPanel&);				// not implemented...
    void   operator=(const OffKeyPanel&);		// not implemented...

	enum
	{
		//@ Constant: MAX_BUTTONS_
		// The maximum number of rows that OffKeyPanel can display.
		MAX_BUTTONS_ = 1
	};

	//@ Data-Member: pUserEventCallBack_
	// A pointer to a callBack function to handle cancel request.
	UserEventCallbackPtr pUserEventCallBack_;

	//@ Data-Member: pFocusSubScreen_
	// This is a pointer to the sub-screen that is associated with this
	// panel
	SubScreen *pFocusSubScreen_;

	//@ Data-Member: panelMsg_
	// A message which informs the user of the panel status.
	TextField panelMsg_;

	//@ Data-Member: panelTitle_
	// A title text for this panel
	TextField panelTitle_;

	/*****************************************************/
	// The following is designed for future maneuver usage.
	// The user should derived a maneuverOffKeyPanel from this class
	// and add the following data members to this derived class.
	/*****************************************************/
	//@ Data-Member: selectButtons_
	// An array of pointers to row select buttons, allowing easier manipulation
	// of the buttons (cannot make an array of button objects as the Button
	// class has no default constructor).
	// Button *selectButtons_[MAX_BUTTONS_];

	//@ Data-Member: select0Button_
	// The select button 0.
	// Button select0Button_;

};

#endif // OffKeyPanel_HH
