#ifndef	SmcpRequestBuffer_HH
# define SmcpRequestBuffer_HH

//====================================================================
// This is a proprietary work to which Nellcor Puritan Bennett
// corporation of California claims exclusive right.  No part of this
// work may be used, disclosed, reproduced, sorted in an information
// retrieval system, or transmitted by any means, electronic,
// mechanical, photocopying, recording, or otherwise without the prior
// written permission of Nellcor Puritan Bennett Corporation.
//
//     Copyright (c) 1997, Nellcor Puritan Bennett Corporation
//=====================================================================

//=====================================================================
//@ Class:  SmcpRequestBuffer - SMCP Message Request Buffer
//---------------------------------------------------------------------
//@ Version-Information
// @ $Header:   /840/Baseline/GUI-Serial-Interface/vcssrc/SmcpRequestBuffer.hhv   25.0.4.0   19 Nov 2013 14:12:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002    By: quf       Date: 16-Aug-2001  DR Number: 5493
//  Project: GuiComms
//  Description:
//  Introduced encapsulated version of class IDs.
//
//  Revision: 001    By: Gary Cederquist  Date: 24-Nov-1997  DR Number: 2605
//    Project: Sigma (R8027)
//    Description:
//       Complete restructuring of GUI-Serial-Interface to fix
//       several problems.
//
//=====================================================================

#include "GUI_Serial_Interface.hh"
#include "SerialBuffer.hh"

//@ Usage-Classes
//@ End-Usage

class MsgQueue;

class SmcpRequestBuffer : public SerialBuffer
{
  public:

	enum
	{
		 SMCP_REQUEST_SIZE  = 128
		,SMCP_REQUEST_COUNT = 3
	};

	SmcpRequestBuffer(void);
	virtual ~SmcpRequestBuffer(void);

    static void  SoftFault(const SoftFaultID softFaultID,
                           const Uint32      lineNumber,
                           const char*       pFileName  = NULL,
                           const char*       pPredicate = NULL);

  protected:

  private:

	//@ Data-Member: packetMemory_
	//  Contains the SMCP message request packets.
	Uint packetMemory_[  SmcpRequestBuffer::SMCP_REQUEST_SIZE 
	                   * SmcpRequestBuffer::SMCP_REQUEST_COUNT / sizeof(Uint) ];

	//@ Data-Member: packetMap_
	//  Array containing pointers to each packet in packetMemory_.
	Uint * packetMap_[ SmcpRequestBuffer::SMCP_REQUEST_COUNT ];
};

#endif	// ! SmcpRequestBuffer_HH
