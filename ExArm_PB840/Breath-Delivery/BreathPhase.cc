#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sotred in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================


// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: BreathPhase - The base class for all breath phases.
//---------------------------------------------------------------------
//@ Interface-Description
//      This is an abstract class with pure virtual methods to be overwritten
//      by each derived class. The pure virtual functions enforce the
//      implementation of the following functionality:
//
//      1) A method to initialize the breath phase.
//
//      2) A method to maintain the breath phase trajectory each BD cycle.
//
//		A method to relinquish control to another breath phase, once a
//      new breath phase is about to start is provided.
//
//      The class also provides public services to other objects in the form of
//      static methods to get and set the current breath phase, to get the
//		breath trigger causing the change to the current phase, and to enable
//      the patient triggers.
//
//		Note that when a breath phase is set, it is also initialized.
//
//		The elapsed time since the start of the breath phase, the phase type and
//		the mix error sum can also be attained.
//
//		This class also maintains the mix errors occurred during the delivery
//		of the phase.  The mix will then be compensated based on the mix error
//		such that the desired mix is achieved.  The flow and volume controllers
//		shutdown states are also maintained based on whether the mix is set
//		at 21% or 100% mix.  The phasing in of settings is also managed by this
//		class.
//---------------------------------------------------------------------
//@ Rationale
//      There are several pre-defined breath phases built into Sigma.  For
//      example, VcvPhase, PcvPhase, etc.  Each derived class captures the
//      rules by which the breath is controlled, detected and terminated.
//      The BreathPhase provides a common interface to all derived breaths.
//---------------------------------------------------------------------
//@ Implementation-Description
//      The class provides access (Get and Set methods) to the current breath
//      phase which is stored as a static pointer to the BreathPhase object.
//      A method to enable patient triggers for the current breath phase is
//      implemented.  During exhalation the patient triggers are set based on
//      the trigger type setting value.  During inspiration, a fixed list of
//      triggers are set.
//---------------------------------------------------------------------
//@ Fault-Handling
//		N/A
//---------------------------------------------------------------------
//@ Restrictions
//		none
//---------------------------------------------------------------------
//@ Invariants
//		none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/BreathPhase.ccv   25.0.4.0   19 Nov 2013 13:59:40   pvcs  $
//
//@ Modification-Log
//
//  Revision: 018   By: rhj   Date: 07-Nov-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//       Leak Compensation project-related changes:
//       Added net flow backup inspiratory trigger.
// 
//  Revision: 017   By:   rhj    Date: 05-March-2007      DR Number: 6359
//  Project:  RESPM
//  Description:
//        Resolve merge conflict for RM to Baseline 
//
//  Revision: 016   By: gdc   Date:  22-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Changed to enable P100 trigger when P100 maneuver is pending
//
//  Revision: 015   By: syw   Date:  24-May-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//		PAV project-related changes:
//		Added RPavPhase to list for mix error sum updates.
//		Changes to accommodate PAV_INSPIRATORY_PAUSE enum.
//
//  Revision: 014  By: sah/jja     Date:  19-May-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//		VTPC project-related changes:
//		Added RVtpcvPhase and RVsvPhase to list for mix error sum updates.
//
//  Revision: 013  By: yyy     Date:  5-May-1999    DR Number: 5322
//  Project:  ATC
//  Description:
//      Added NewBreath() to update the tube information whenever a
//		newBreath occures.
//
//  Revision: 012  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 011  By:  syw    Date:  07-Jul-1998    DR Number: DCS 5157
//       Project:  Sigma (840)
//       Description:
//			Added BILEVEL_PAUSE_PHASE check for phaseType_.
//
//  Revision: 010  By:  syw    Date:  08-Dec-1997    DR Number: none
//       Project:  Sigma (840)
//       Description:
//		 	BiLevel initial version.  Added new phase checks in
//			setMixErrorSum().
//
//  Revision: 009  By: syw   Date:  12-Aug-1997    DR Number: 2356
//  	Project:  Sigma (840)
//		Description:
//			Added AdiabaticFactor_ data member.
//
//  Revision: 008  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 007 By: syw   Date: 16-Aug-1996   DR Number: DCS 1076
//  	Project:  Sigma (R8027)
//		Description:
//			Added updateFlowControllerFlags_() method.
//
//  Revision: 006 By: iv    Date: 07-May-1996   DR Number: DCS 967
//  	Project:  Sigma (R8027)
//		Description:
//			Added a check for this == (BreathPhase *)&RPeepRecoveryPhase) in methods
//          setMixErrorSum() and getMixErrorSum().
//
//  Revision: 005 By: iv    Date: 22-Apr-1996   DR Number: DCS 939
//  	Project:  Sigma (R8027)
//		Description:
//			Modified method EnablePatientTriggers() to enable the rNetFlowInspTrigger
//			only if phase == Exhalation and not enable if phase == ExpiratoryPause.
//
//  Revision: 004 By: syw    Date: 22-Feb-1996   DR Number: DCS 627
//  	Project:  Sigma (R8027)
//		Description:
//			Added code to phaseInSettings() prohibit phasing in pending batch
//			settings during	SafetyPcv.
//
//  Revision: 003 By: syw    Date: 22-Feb-1996   DR Number: DCS 600
//  	Project:  Sigma (R8027)
//		Description:
//			Removed setActiveControllers() method pertaining to Controls
//			Rev 04 changes.
//
//  Revision: 002 By: syw    Date: 21-Feb-1996   DR Number: DCS 674
//  	Project:  Sigma (R8027)
//		Description:
//			Removed #ifndef around #include.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "BreathPhase.hh"

#include "BdDiscreteValues.hh"
#include "TriggersRefs.hh"
#include "PhaseRefs.hh"
#include "ControllersRefs.hh"
#include "BreathMiscRefs.hh"

//@ Usage-Classes

#include "PhasedInContextHandle.hh"
#include "BreathTrigger.hh"
#include "MathUtilities.hh"
#include "O2Mixture.hh"
#include "VolumeController.hh"
#include "BreathPhaseScheduler.hh"
#include "SchedulerRefs.hh"
#include "BreathMiscRefs.hh"
#include "Tube.hh"
#include "P100Trigger.hh"
#include "ManeuverRefs.hh"
#include "P100Maneuver.hh"
#include "PressureInspTrigger.hh"
#include "PressureSetInspTrigger.hh"
#include "NetFlowInspTrigger.hh"
#include "LeakCompEnabledValue.hh"
#include "LeakCompMgr.hh"
#include <string.h>

#include "GainsManager.hh"
//@ End-Usage                 

#ifdef SIGMA_UNIT_TEST
#include <string.h>
#include "Ostream.hh"
#endif //SIGMA_UNIT_TEST

//@ Code...

// during initialization - set all static members:
BreathPhase* BreathPhase::PCurrentPhase_ = NULL ;
const BreathTrigger* BreathPhase::PBreathTrigger_ = NULL ;
Real32 BreathPhase::AdiabaticFactor_ = 1.0 ;

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BreathPhase
//
//@ Interface-Description
//      Constructor. This method has the phase type as an argument and returns
//		nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//      The phaseType_ and mixErrorSum_ members are initialized.
//		$[04282]
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
BreathPhase::BreathPhase(const BreathPhaseType::PhaseType phase)
{
	CALL_TRACE("BreathPhase::BreathPhase(const BreathPhaseType::PhaseType phase)") ;

   	// $[TI1]
	phaseType_ = phase ;
	mixErrorSum_ = 0.0F ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~BreathPhase 
//
//@ Interface-Description
//      Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//      none
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
BreathPhase::~BreathPhase()
{
	CALL_TRACE("BreathPhase::~BreathPhase()") ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: NewBreath
//
//@ Interface-Description
//      This method takes no arguments and returns nothing.  This method
//		is called by all derived classes's NewBreath methods.
//---------------------------------------------------------------------
//@ Implementation-Description
//      The method updates the tube information, starts the new phase
//		via newBreath and resets mix error sum used in cycle to cycle
//		O2 correction.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
BreathPhase::NewBreath(void)
{
	CALL_TRACE("BreathPhase::NewBreath(void)");

	RTube.updateTube();
	PCurrentPhase_->newBreath();
	RO2Mixture.resetMixErrorSum();
   	// $[TI1]

#if CONTROLS_TUNING
	// TODO E600_LL: to enable when SoftwareOptions is ready
	/if( SoftwareOptions::GetSoftwareOptions().getDataKeyType() ==
	          SoftwareOptions::ENGINEERING )*/
	{
	    // If we are here for the first time, then complete initialization
	    // of the gains manager by loading the gains from BD into the
	    // gains manager.
        static Boolean loadGains = TRUE;
        if( loadGains == TRUE )
        {
            if( GainsManagerNS::GainsManager::getInstance().loadGains() == TRUE )
            {
                loadGains = FALSE;
            }
        }
        else
        {
            // Distribute gains from the gains manager to the controllers
            GainsManagerNS::GainsManager::getInstance().distributeGains();
        }
	}
#endif
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: EnablePatientTrigger 
//
//@ Interface-Description
// 		This method takes a PhaseType as an argument and has no return value.
// 		This method provides a service to an object that is responsible for
// 		enabling patient triggers for the current breath phase.  Since patient
// 		triggers during inspiration are valid only for spontaneous breaths,
// 		the caller is responsible to place the call for the right type of breath.
//		This method should only be called by the following breath phases:
//		all spontaneous breaths (PsvPhase), ExhalationPhase, and ExpiratoryPausePhase.
//---------------------------------------------------------------------
//@ Implementation-Description
// 		This method enables the patient trigger for the phase type specified
// 		by its argument.  During inspiration there is a fixed list of triggers
// 		to be initialized.  During Exhalation, the list depends on the trigger
// 		type setting.
//
// 		The globaly declared triggers are enabled directly using their public
// 		enable() methods.
// 		$[04008]
//---------------------------------------------------------------------
//@ PreCondition
//		triggerType == TriggerTypeValue::FLOW_TRIGGER
//			|| TriggerTypeValue::PRESSURE_TRIGGER
//		phase == BreathPhaseType::INSPIRATION ||BreathPhaseType::EXHALATION
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
BreathPhase::EnablePatientTriggers( const BreathPhaseType::PhaseType phase)
{
	CALL_TRACE("BreathPhase::EnablePatientTriggers( const BreathPhaseType::PhaseType phase)") ;

    DiscreteValue triggerType =
        PhasedInContextHandle::GetDiscreteValue( SettingId::TRIGGER_TYPE) ;


    if (phase == BreathPhaseType::INSPIRATION)
    {
	   	// $[TI1.1]
        ((BreathTrigger&)RDeliveredFlowExpTrigger).enable() ;
        ((BreathTrigger&)RPressureExpTrigger).enable() ;
    }
    else if (phase == BreathPhaseType::EXHALATION)
    {
	   	// $[TI1.2]
		if (RP100Maneuver.getManeuverState() == PauseTypes::PENDING)
		{
			// $[RM12304] Otherwise the P100 maneuver shall transition to armed at the start of exhalation.
			// $[RM12090] When the P100 maneuver becomes armed, the P100 inspiratory trigger shall be enabled.
			// $[RM12305] When the P100 maneuver becomes armed, the flow, pressure and backup pressure triggers shall be disabled.

			RP100Trigger.enable();
		}
		else if (triggerType == TriggerTypeValue::PRESSURE_TRIGGER)
        {
		   	// $[TI2.1]
            RPressureInspTrigger.enable() ;

			if (RLeakCompMgr.isEnabled())
			{
				RNetFlowBackupInspTrigger.enable() ;
			}
        }
        else if (triggerType == TriggerTypeValue::FLOW_TRIGGER)
        {
		   	// $[TI2.2]
            RPressureBackupInspTrigger.enable() ;
            RNetFlowInspTrigger.enable() ;
        }
        else
        {
            CLASS_PRE_CONDITION((triggerType == TriggerTypeValue::FLOW_TRIGGER) ||
            						(triggerType == TriggerTypeValue::PRESSURE_TRIGGER)) ;
        }
    }
    else
    {
        CLASS_PRE_CONDITION( phase == BreathPhaseType::INSPIRATION ||
            phase == BreathPhaseType::EXHALATION) ;
    }
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: relinquishControl
//
//@ Interface-Description
//      This method takes a BreathTrigger& as an argument and has no return
//      value.  This method is called at the beginning of the next phase to
//      wrap up the current breath phase.
//---------------------------------------------------------------------
//@ Implementation-Description
//      The BreathTrigger& passed as an argument is used to obtain the
//      trigger id for the purpose of avoiding a compiler warning.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
BreathPhase::relinquishControl( const BreathTrigger& trigger)
{
	CALL_TRACE("BreathPhase::relinquishControl( BreathTrigger& trigger)") ;
	UNUSED_SYMBOL(trigger);		
	
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setMixErrorSum 
//
//@ Interface-Description
//      This method has mix error sum as an argument and returns nothing.
//		This method is called to set the mix error sum to the value passed in.
//---------------------------------------------------------------------
//@ Implementation-Description
//      Sets mixErrorSum_ to the value passed in.
//---------------------------------------------------------------------
//@ PreCondition
//      The breath phase must be one of the following: VCV, apnea VCV,
//		PCV, apnea PCV, VTPCV, PSV phase, TCV phase,
//		VSV phase, PAV phase or Peep Recovery phase.
//		$[04281]
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
BreathPhase::setMixErrorSum( const Real32 mixErrorSum)
{
	CALL_TRACE("BreathPhase::setMixErrorSum_( const Real32 mixErrorSum)") ;

   	// $[TI1]
	CLASS_PRE_CONDITION( this == (BreathPhase *)&RVcvPhase || 
						 this == (BreathPhase *)&RApneaVcvPhase ||
						 this == (BreathPhase *)&RPcvPhase || 
						 this == (BreathPhase *)&RApneaPcvPhase ||
						 this == (BreathPhase *)&RVtpcvPhase || 
						 this == (BreathPhase *)&RPsvPhase || 
						 this == (BreathPhase *)&RPeepRecoveryPhase ||
						 this == (BreathPhase *)&RTcvPhase || 
						 this == (BreathPhase *)&RVsvPhase ||
						 this == (BreathPhase *)&RPavPhase ||
						 this == (BreathPhase *)&RLowToHighPeepPhase || 
						 this == (BreathPhase *)&RHiLevelPsvPhase ||
						 this == (BreathPhase *)&RVcmPsvPhase) ;

	mixErrorSum_ = mixErrorSum ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getMixErrorSum 
//
//@ Interface-Description
//      This method has no arguments and returns the mix error sum.
//---------------------------------------------------------------------
//@ Implementation-Description
//      returns mixErrorSum_.
//---------------------------------------------------------------------
//@ PreCondition
//      The breath phase must be one of the following: VCV, apnea VCV,
//		PCV, apnea PCV, VTPCV, PSV phase, TCV phase 
//      VSV phase, PAV phase or Peep Recovery phase.
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Real32
BreathPhase::getMixErrorSum(void) const
{
	CALL_TRACE("BreathPhase::getMixErrorSum_(void)") ;

   	// $[TI1]
	CLASS_PRE_CONDITION( this == (BreathPhase *)&RVcvPhase || 
						 this == (BreathPhase *)&RApneaVcvPhase ||
						 this == (BreathPhase *)&RPcvPhase || 
						 this == (BreathPhase *)&RApneaPcvPhase ||
						 this == (BreathPhase *)&RVtpcvPhase || 
						 this == (BreathPhase *)&RPsvPhase ||
						 this == (BreathPhase *)&RPeepRecoveryPhase ||
						 this == (BreathPhase *)&RTcvPhase || 
						 this == (BreathPhase *)&RVsvPhase ||
						 this == (BreathPhase *)&RPavPhase ||
						 this == (BreathPhase *)&RLowToHighPeepPhase || 
						 this == (BreathPhase *)&RHiLevelPsvPhase ||
						 this == (BreathPhase *)&RVcmPsvPhase) ;

	return( mixErrorSum_) ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: phaseInSettings
//
//@ Interface-Description
//      This method has no arguments and returns nothing.  This method is
//		called to phase in the proper settings depending if the phase is
//		inspiration or exhalation.
//---------------------------------------------------------------------
//@ Implementation-Description
//      PhaseInBatchSettings is called with the proper DURING_xxxx, where
//		xxxx is INSPIRATION or EXHALATION depending on the phaseType_.
//---------------------------------------------------------------------
//@ PreCondition
//      BreathPhase != NULL
//		phaseType_ == BreathPhaseType::EXPIRATORY_PAUSE
//			|| BreathPhaseType::NON_BREATHING
//			|| BreathPhaseType::INSPIRATION
//			|| BreathPhaseType::EXHALATION
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void BreathPhase::phaseInSettings( void)
{
	CALL_TRACE("void BreathPhase::phaseInSettings( void)") ;

	if ( BreathPhaseScheduler::GetCurrentScheduler().getId() != SchedulerId::SAFETY_PCV)
	{
		switch (phaseType_)
		{
		// $[TI1.1]
		case BreathPhaseType::INSPIRATION:
			// $[TI2.1]
			PhasedInContextHandle::PhaseInPendingBatch( PhaseInEvent::DURING_INSPIRATION) ;
			break;
		case BreathPhaseType::EXHALATION:
			// $[TI2.2]
			PhasedInContextHandle::PhaseInPendingBatch( PhaseInEvent::DURING_EXHALATION) ;
			break;
		case BreathPhaseType::EXPIRATORY_PAUSE:
		case BreathPhaseType::INSPIRATORY_PAUSE:
		case BreathPhaseType::PAV_INSPIRATORY_PAUSE:
		case BreathPhaseType::NIF_PAUSE:
		case BreathPhaseType::P100_PAUSE:
		case BreathPhaseType::VITAL_CAPACITY_INSP:
		case BreathPhaseType::BILEVEL_PAUSE_PHASE:
		case BreathPhaseType::NON_BREATHING:
			// do nothing
			break;
		default:
			AUX_CLASS_ASSERTION_FAILURE( phaseType_ );
			break;
		}
	}	// implied else $[TI1.2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  activate(pPrevSpontPhase, pLostSpontPhase)
//
//@ Interface-Description
//      This method is called every time there is a change from one phase to
//      another.  The 'pPrevSpontPhase' pointer indicates the previous
//      inspiratory phase, if any, while 'pLostSpontPhase' indicates what
//      was the inspiratory phase when this phase's scheduler last relinquished
//      control.  This call is to notify this instance that it is becoming the
//      new phase.
//
//      For all phases that need to initialize data, alarms, etc. prior to
//      processing the first inspiration, this method should be overridden.
//      This method does nothing, by default.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
BreathPhase::activate(const BreathPhase*, const BreathPhase*)
{
	CALL_TRACE("activate(pPrevSpontPhase, pLostSpontPhase)") ;

	// $[TI1] -- do nothing...
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  deactivate(triggerId)
//
//@ Interface-Description
//      This method is called every time there is a change from one phase to
//      another.  The 'triggerId' parameter identifies the type of change when
//      a scheduler is changing from one to another.  This call is to notify
//      this instance that it is no longer the current phase.
//
//      For all phases that need to reset data, alarms, etc. prior to
//      relinquishing control to another phase, this method should be
//      overridden.  This method does nothing, by default.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
BreathPhase::deactivate(const Trigger::TriggerId)
{
	CALL_TRACE("deactivate(triggerId)") ;

	// $[TI1] -- do nothing...
}


#if defined(SIGMA_UNIT_TEST) || defined(SIGMA_DEVELOPMENT)

char* BreathPhase::getId(void) const
{
    static char id[30];
    
    if(this == (BreathPhase*)&RVcvPhase)
        strcpy(id,"VcvPhase");
    else if(this == (BreathPhase*)&RPcvPhase)
        strcpy(id,"PcvPhase");
    else if(this == (BreathPhase*)&RVtpcvPhase)
        strcpy(id,"VtpcvPhase");
    else if(this == (BreathPhase*)&RExpiratoryPausePhase)
        strcpy(id,"ExpiratoryPausePhase");
    else if(this == (BreathPhase*)&RApneaVcvPhase)
        strcpy(id,"ApneaVcvPhase");
    else if(this == (BreathPhase*)&RExhalationPhase)
        strcpy(id,"ExhalationPhase");
    else if(this == (BreathPhase*)&RExpiratoryPausePhase)
        strcpy(id,"ExpiratoryPausePhase");
    else if(this == (BreathPhase*)&RApneaPcvPhase)
        strcpy(id,"ApneaPcvPhase");
    else if(this == (BreathPhase*)&RPsvPhase)
        strcpy(id,"PsvPhase");
    else if(this == (BreathPhase*)&RVsvPhase)
        strcpy(id,"VsvPhase");
    else if(this == (BreathPhase*)&RDisconnectPhase)
        strcpy(id,"DisconnectPhase");
    else if(this == (BreathPhase*)&RSafeStatePhase)
        strcpy(id,"SafeStatePhase");
    else if(this == (BreathPhase*)&RSafetyValveClosePhase)
        strcpy(id,"SafetyValveClosePhase");
    else if(this == (BreathPhase*)&ROscPcvPhase)
        strcpy(id,"OscPcvPhase");
    else if(this == (BreathPhase*)&ROscExhPhase)
        strcpy(id,"OscExhPhase");
    else if(this == (BreathPhase*)&RStartupPhase)
        strcpy(id,"StartupPhase");
    else if(this == (BreathPhase*)&RPeepRecoveryPhase)
        strcpy(id,"PeepRecoveryPhase");
    else if(this == (BreathPhase*)&RPavPhase)
        strcpy(id,"PavPhase");
    else
        strcpy(id,"NoPhase");
        
    return (id);
}

#endif // defined(SIGMA_UNIT_TEST) || defined(SIGMA_DEVELOPMENT)


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
BreathPhase::SoftFault( const SoftFaultID  softFaultID,
                   		const Uint32       lineNumber,
				   		const char*        pFileName,
				   		const char*        pPredicate)
{
	CALL_TRACE("SoftFault( softFaultID, lineNumber, pFileName, pPredicate)") ;
	FaultHandler::SoftFault( softFaultID, BREATH_DELIVERY, BREATHPHASE,
                             lineNumber, pFileName, pPredicate) ;
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateFlowControllerFlags_
//
//@ Interface-Description
//		This method has no arguments and has no return value.  This method is
//		called to set the controller shutdown flags of the flow and
//		volume controllers based on mix.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Set o2 to true if 21%.  Set air to true if 100%.  False otherwise.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
BreathPhase::updateFlowControllerFlags_( void)
{
	CALL_TRACE("BreathPhase::updateFlowControllerFlags_( void)") ;

	Real32 o2Mix = RO2Mixture.getAppliedO2Percent() ;

	RAirFlowController.setControllerShutdown( FALSE) ;
	RO2FlowController.setControllerShutdown( FALSE) ;
	RAirVolumeController.setControllerShutdown( FALSE) ;
	RO2VolumeController.setControllerShutdown( FALSE) ;

	if (IsEquivalent( o2Mix, 21.00, ::THOUSANDTHS))
	{
	   	// $[TI1.1]
		RO2FlowController.setControllerShutdown( TRUE) ;
		RO2VolumeController.setControllerShutdown( TRUE) ;
	}
	else if (IsEquivalent( o2Mix, 100.00, ::THOUSANDTHS))
	{
	   	// $[TI1.2]
		RAirFlowController.setControllerShutdown( TRUE) ;
		RAirVolumeController.setControllerShutdown( TRUE) ;
	}  	// implied else $[TI1.3]
}

//=====================================================================
//
//  Private Methods...
//
//=====================================================================


