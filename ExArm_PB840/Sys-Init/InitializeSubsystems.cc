#include "stdafx.h"
//=====================================================================
//   This is a proprietary work to which Puritan-Bennett corporation
//   of California claims exclusive right.  No part of this work may
//  be used, disclosed, reproduced, sorted in an information retrieval
//     system, or transmitted by any means, electronic, mechanical,
//   photocopying, recording, or otherwise without the prior written
//      permission of Puritan-Bennett Corporation of California.
//
//          Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//==============================M O D U L E  D E S C R I P T I O N ====
//@ Filename: InitializeSubsystems.cc - Initialize Sigma subsystems
//---------------------------------------------------------------------
//@ Interface-Description
//  This file contains the Sys_Init::InitializeSubsystems_ private
//  method.  It is maintained in a separate file from other Sys_Init
//  class methods to allow development configurations the ability
//  to easily override its definition at link time.
//---------------------------------------------------------------------
//@ Rationale
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Sys-Init/vcssrc/InitializeSubsystems.ccv   25.0.4.0   19 Nov 2013 14:33:54   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By:  gdc    Date:  12-Dec-2008    SCR Number: 6068
//  Project:  840S
//  Description:
//		Removed SUN prototype code.
//
//   Revision 003   By: rhj      Date: 20-Jun-2007   SCR Number: 6237
//      Project:   Trend
//      Description:
//         Added Trend-Database initialize method.
//
//   Revision 002   By: sah      Date: 12-Jun-1997   DCS Number: 2208
//      Project:   Sigma   (R8027)
//      Description:
//         Modified name of VGA initialization method, as per the new
//         VGA interface.
//
//   Revision 001   By:   gdc      Date: 07/06/95    DR Number:   n/a
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//=====================================================================

//=====================================================================
//
//      Includes...
//
//=====================================================================

#include "Sys_Init.hh"

#include "Alarm_Analysis.hh"
#include "BD_IO_Devices.hh"
#include "Breath_Delivery.hh"
#include "Foundation.hh"
#include "NetworkApp.hh"
#include "PatientDataMgr.hh"
#include "PersistentObjs.hh"
#include "SafetyNet.hh"
#include "Settings_Validation.hh"
#include "ServiceDataMgr.hh"
#include "ServiceMode.hh"
#include "Utilities.hh"
#include "NovRamSemaphore.hh"
#include "AnalogInputThread.h"


#ifdef SIGMA_GUI_CPU
# include "VgaGraphicsDriver.hh"
# include "GuiIo.hh"
# include "GuiApp.hh"
# include "GuiFoundation.hh"
# include "Dci.hh"
# include "SerialInterface.hh"
# include "Trend_Database.hh"
# include "xallocator.hh"
#ifdef INTEGRATION_TEST_ENABLE
# include "SWATCommandServer.h"
#endif
#endif

//@ Usage-Classes
//@ End-Usage

//=====================================================================
//
//      Static Data...
//
//=====================================================================

//@ Code...

//=====================================================================
//
//      Public Methods...
//
//=====================================================================

//=====================================================================
//
//      Protected Methods...
//
//=====================================================================


//=====================================================================
//
//      Private Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Free-Function: Sys_Init::InitializeSubsystems_
//
//@ Interface-Description
//  This method calls each Sigma subsystem initialization function which
//  initialize the subsystem's static data.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls the Initialize() function for each subsystem in order of
//  subsystem dependency.  In particular, the following initialization
//  dependency must be observed:
//
//  1. Settings-Validation must be initialized before BD-IO-Devices.
//
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
Sys_Init::InitializeSubsystems_(void)
{
    CALL_TRACE("InitializeSubsystems_(void)");

    PersistentObjs::Initialize();  // this should be done first...
    Utilities::Initialize();
    Foundation::Initialize();
    NetworkApp::Initialize();
    Settings_Validation::Initialize();
    PatientDataMgr::Initialize();
    Alarm_Analysis::Initialize();
    ServiceDataMgr::Initialize();

#ifdef SIGMA_GUI_CPU

    VgaGraphicsDriver::Initialize();
	//TODO: BVP - E600 Hack to bring up GUI.. the GUI IO hardware is
	//not completely ported yet.
    GuiIoDevicesInitialize();
    GuiFoundation::Initialize();
    ServiceMode::Initialize();
    GuiApp::Initialize();
    SerialInterface::Initialize();
    Dci::Initialize();

    xallocator_init();

#ifdef INTEGRATION_TEST_ENABLE
	SWATCommandServer::Initialize();
#endif

	// Go last so that it can register with Gui-Bd events
	//TODO E600: BVP - E600 Hack to bring up GUI.. we do not have Trending yet.
    //TrendDatabaseInitialize();

#endif

#ifdef SIGMA_BD_CPU
	// AnalogInputThread needs to be initialized before ExhCommunicationThread
	// since AnalogInputThread creates a timer trigger event for ExhCommunicationThread
	AnalogInputThread_t::Initialize();

#endif

	BD_IO_Devices::Initialize();
    Breath_Delivery::Initialize();
	ServiceMode::Initialize();
	//TODO E600: enable SafetyNet when ready
    //SafetyNet::Initialize();
	DEBUG_MSG("Init complete");

    // enable the semaphore that protects NOVRAM...
    NovRamSemaphore::EnableBlocking();
    // $[TI1]
}


