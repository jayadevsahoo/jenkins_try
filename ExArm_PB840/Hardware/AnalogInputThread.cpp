#include "stdafx.h"
//*****************************************************************************
//
//  AnalogInputThread.cpp - Implementation file for AnalogInputThread_t class.
//
//      This thread runs at the highest priority (0). It provides the fundamental
//      timing to the rest of the threads by sending a message with analog data
//      to the Servos Task every 2ms.
//
//      Because it is running at such a high priority, it must be very quick.
//      It must simply read the analog data, send the data on to the servos,
//      and then sleep for 2ms.
//
//*****************************************************************************
#include "AnalogInputThread.h"
#include "GlobalObjects.h"
#include "gptimer.h"
#include "ExceptionHandler.h"
#include "AioDioDriver.h"
#include "A2dReadingsStructs.h"
#include "AdcData.h"
#include "AdcChannels.hh"
#include "MsgQueue.hh"
#include "BdQueuesMsg.hh"
#include "BDIORefs.hh"
#include "ServosGlobal.h"
#include "BDIOTest.h"

#define TIMER_2MS_EVENT_NAME    TEXT("TIMER_2MS_EVENT")
#define EXH_THREAD_EVENT_NAME	TEXT("EXH_THREAD_EVENT")

static HANDLE hGptTimer = NULL;
static HANDLE hGptTimerEvent = NULL;
static HANDLE hExhThreadEvent = NULL;

static LPCTSTR lpTimerEventName = TIMER_2MS_EVENT_NAME;
static DWORD EventNameSize = sizeof(lpTimerEventName);
static DWORD dwTimerEventCreated;

//*****************************************************************************
//
//  AnalogInputThread_t::AnalogInputThread_t - Construct the class
//
//*****************************************************************************

void AnalogInputThread_t::Initialize()
{
	LPCTSTR lpGPTName = TEXT("GPT9:");	// Name for Timer 10
	hGptTimer = CreateFile( lpGPTName,  GENERIC_READ | GENERIC_WRITE,
		0, 0, OPEN_EXISTING, 0, 0);
	if (!hGptTimer)
	{
		RETAILMSG(TRUE, (_T("AnalogInputThread: Can't open 'GPT9:'\r\n")));
		return;
	}

	hGptTimerEvent = CreateEvent(NULL, FALSE, FALSE, lpTimerEventName);
	if(!hGptTimerEvent)
	{
		RETAILMSG(TRUE, (_T("AnalogInputThread: Creating local timer event failed\r\n")));
		return;
	}

	dwTimerEventCreated = DeviceIoControl(hGptTimer, GPT_IOCTL_TIMER_CREATE_EVENT, (LPVOID)lpTimerEventName, EventNameSize, NULL, 0, NULL, NULL);
	if(!dwTimerEventCreated)
	{
		RETAILMSG(TRUE, (_T("AnalogInputThread: Can't create timer event\r\n")));
		return;
	}

	hExhThreadEvent = CreateEvent(NULL, FALSE, FALSE, EXH_THREAD_EVENT_NAME);
	if(hExhThreadEvent)
	{
		RETAILMSG(TRUE, (_T("AnalogInputThread: event %s created\r\n"), EXH_THREAD_EVENT_NAME ));
	}
	else
	{
		RETAILMSG(TRUE, (_T("AnalogInputThread: Creating event %s failed\r\n"), EXH_THREAD_EVENT_NAME));
	}
}


//*****************************************************************************
//
//  AnalogInputThread_t::~AnalogInputThread_t -
//
//*****************************************************************************

AnalogInputThread_t::~AnalogInputThread_t()
{
	if(hGptTimer)
		CloseHandle(hGptTimer);

	if(hGptTimerEvent)
		CloseHandle(hGptTimerEvent);

	if(dwTimerEventCreated)
	{
		DWORD dwResult = DeviceIoControl(hGptTimer, GPT_IOCTL_TIMER_RELEASE_EVENT, (LPVOID)lpTimerEventName, EventNameSize, NULL, 0, NULL, NULL);
		if(!dwResult)
		{
			RETAILMSG(TRUE, (_T("AnalogInputThread: Releasing Event failed\r\n")));
		}
	}
}


//*****************************************************************************
//
//  AnalogInputThread_t::ThreadProcedure - Read the analog data, send to the
//          servos task, and sleep for 2ms. This also captures the digital
//          speed signal.
//
//*****************************************************************************

static const int TIMER_INT_INTERVAL = 1;

uint32_t AnalogInputThread_t::ThreadProcedure(void *ProcedureArg)
{
	DWORD dwResult;

    Uint8 AdcReadCounter = 0;
    MsgQueue BdTasksQueue(::TIMER_5MS_Q);
	UNUSED_SYMBOL(ProcedureArg);

#define EXH_EVENT_TRIGGER_COUNT		5		// Trigger Exh. Comm Thread to read data every 6-ms.
											// Faster than this rate, it causes data to be invalid
	static uint8_t ExhEventCount = EXH_EVENT_TRIGGER_COUNT;

    if(hGptTimer)
	{
		DWORD TimerMode = TIMER_MODE_PERIODIC;
		DWORD TimerIntInterval = TIMER_INT_INTERVAL;

		dwResult = DeviceIoControl(hGptTimer, GPT_IOCTL_TIMER_SET_MODE, &TimerMode, sizeof(DWORD), NULL, 0, NULL, NULL);
		dwResult = DeviceIoControl(hGptTimer, GPT_IOCTL_TIMER_SET_EVENT_RATE, &TimerIntInterval, sizeof(DWORD), NULL, 0, NULL, NULL);
		dwResult = DeviceIoControl(hGptTimer, GPT_IOCTL_TIMER_START, NULL, 0, NULL, 0, NULL, NULL);
	}
	
	for(;;)
    {
        if(++ExhEventCount >= EXH_EVENT_TRIGGER_COUNT)
		{
			SetEvent(hExhThreadEvent);
			ExhEventCount = 0;
		}

		// Read the ADCs
        AdcData_t AdcData;
		AioDioDriver.Read(ADC_FIRST_ONBOARD_CHANNEL, ADC_LAST_ONBOARD_CHANNEL, &AdcData.data[0], sizeof(AdcData)/2);
		AioDioDriver.Read(ADC_FIRST_MANIFOLD_BOARD_CHANNEL, ADC_LAST_MANIFOLD_BOARD_CHANNEL, &AdcData.data[16], sizeof(AdcData)/2);

        // Save the current set of data in the RawDataBuf_ array
        AdcChannels::SetRawAdcData(AdcData);

        // After this many reads, send a message to the BD Tasks Thread.
        if (++AdcReadCounter >= NUM_ADC_READS_PER_CYCLE)
        {
            AdcReadCounter = 0;                                         // Reset the counter
            BdTasksQueue.putMsg(BdQueuesMsg::BD_MAIN_CYCLE_EVENT);      // Send the 5ms notification to the BD tasks thread
        }

// TODO E600 for debugging. Remove when not needed
#if 1
		RawA2dReadings_t RawA2dReadings;

		RawA2dReadings.InitValidState(false);

		if(AdcData.data[ADC_AIR_FLOW_VALID_INDEX])
		{
			RawA2dReadings.AirFlow.Value = (uint16_t)AdcData.data[ADC_AIR_FLOW_INDEX];
			RawA2dReadings.AirFlow.Valid = true;
		}
		if(AdcData.data[ADC_O2_FLOW_VALID_INDEX])
		{
			RawA2dReadings.O2Flow.Value = (uint16_t)AdcData.data[ADC_O2_FLOW_INDEX];
			RawA2dReadings.O2Flow.Valid = true;
		}
		if(AdcData.data[ADC_AIR_TEMP_VALID_INDEX])
		{
			RawA2dReadings.AirTemp.Value = (uint16_t)AdcData.data[ADC_AIR_TEMP_INDEX];
			RawA2dReadings.AirTemp.Valid = true;
		}
		if(AdcData.data[ADC_O2_TEMP_VALID_INDEX])
		{
			RawA2dReadings.O2Temp.Value = (uint16_t)AdcData.data[ADC_O2_TEMP_INDEX];
			RawA2dReadings.O2Temp.Valid = true;
		}
		if(AdcData.data[ADC_FIO2_SENSOR_VALID_INDEX])
		{
			RawA2dReadings.O2Sensor.Value = (uint16_t)AdcData.data[ADC_FIO2_SENSOR_INDEX];
			RawA2dReadings.O2Sensor.Valid = true;
		}
		if(AdcData.data[ADC_SIGNAL_TEST_VALID_INDEX])
		{
			RawA2dReadings.VoltageCheck.Value = (uint16_t)AdcData.data[ADC_SIGNAL_TEST_INDEX];
			RawA2dReadings.VoltageCheck.Valid = true;
		}
		if(AdcData.data[ADC_INT_BATTERY_VALID_INDEX])
		{
			RawA2dReadings.VbattInt.Value = (uint16_t)AdcData.data[ADC_INT_BATTERY_INDEX];
			RawA2dReadings.VbattInt.Valid = true;
		}

		if(AdcData.data[ADC_AIR_PRESSURE_TRANSDUCER_VALID_INDEX])
		{
			RawA2dReadings.AirPressure.Value = (uint16_t)AdcData.data[ADC_AIR_PRESSURE_TRANSDUCER_INDEX];
			RawA2dReadings.AirPressure.Valid = true;
		}
		if(AdcData.data[ADC_O2_PRESSURE_TRANSDUCER_VALID_INDEX])
		{
			RawA2dReadings.O2Pressure.Value = (uint16_t)AdcData.data[ADC_O2_PRESSURE_TRANSDUCER_INDEX];
			RawA2dReadings.O2Pressure.Valid = true;
		}
		if(AdcData.data[ADC_EXP_PRESSURE_TRANSDUCER_VALID_INDEX])
		{
			RawA2dReadings.ExpPressure.Value = (uint16_t)AdcData.data[ADC_EXP_PRESSURE_TRANSDUCER_INDEX];
			RawA2dReadings.ExpPressure.Valid = true;
		}
		if(AdcData.data[ADC_INSP_PRESSURE_TRANSDUCER_VALID_INDEX])
		{
			RawA2dReadings.InspPressure.Value = (uint16_t)AdcData.data[ADC_INSP_PRESSURE_TRANSDUCER_INDEX];
			RawA2dReadings.InspPressure.Valid = true;
		}
		if(AdcData.data[ADC_EXH_DRIVE_PRESSURE_TRANSDUCER_VALID_INDEX])
		{
			RawA2dReadings.ExhDrivePressure.Value = (uint16_t)AdcData.data[ADC_EXH_DRIVE_PRESSURE_TRANSDUCER_INDEX];
			RawA2dReadings.ExhDrivePressure.Valid = true;
		}
		if(AdcData.data[ADC_EXH_SUPPLY_PRESSURE_TRANSDUCER_VALID_INDEX])
		{
			RawA2dReadings.ExhSupplyPressure.Value = (uint16_t)AdcData.data[ADC_EXH_SUPPLY_PRESSURE_TRANSDUCER_INDEX];
			RawA2dReadings.ExhSupplyPressure.Valid = true;
		}
		if(AdcData.data[ADC_VOLTAGE_2P5V_VALID_INDEX])
		{
			RawA2dReadings.Vref2p5.Value = (uint16_t)AdcData.data[ADC_VOLTAGE_2P5V_INDEX];
			RawA2dReadings.Vref2p5.Valid = true;
		}
		if(AdcData.data[ADC_VOLTAGE_3P16V_VALID_INDEX])
		{
			RawA2dReadings.Vref3p16.Value = (uint16_t)AdcData.data[ADC_VOLTAGE_3P16V_INDEX];
			RawA2dReadings.Vref3p16.Valid = true;
		}

		BdioTest.SetRawReadings(RawA2dReadings);
#endif

		WaitForSingleObject(hGptTimerEvent, INFINITE);
    }

    return 0;
}


//*****************************************************************************
//
//  ThreadStartUp - This function gives the Win32 function ::CreateThread an
//      address to be passed as the startup location.
//
//*****************************************************************************

void AnalogInputThread_t::Thread()
{
#ifndef SIGMA_GUIPC_CPU

    uint32_t ThreadResult = 0;

    __try
    {
		ThreadResult = AnalogInputThread_t::ThreadProcedure(0);
    }
    __except(ExceptionHandler.EvaluateException(GetExceptionInformation()))
    {
        // Get exception info
        int32_t Code = _exception_code();


        // Call exception handler
        ExceptionHandler.StopVentilating(Code, "AnalogInputThread");
    }
#endif
}




