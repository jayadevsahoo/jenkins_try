#ifndef  ServiceSwitch_HH
#define  ServiceSwitch_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//=====================================================================
// Class: ServiceSwitch - Service Switch Power On Self Test
//
//---------------------------------------------------------------------
// Modification-Log
//
//
//  Revision: 001    By: Gary Cederquist   Date: 04-MAR-1997  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//=====================================================================
#ifdef SIGMA_BD_CPU

#include "Sigma.hh"
#include "Post.hh"

class ServiceSwitch : public Post
{
  public:
    static Criticality	TestAndArm(void);
    static Criticality	Disarm(void);
    static Boolean      IsServiceModeRequested(void);

  private:
    ServiceSwitch();        // not implemented
    ~ServiceSwitch();       // not implemented

    static Boolean		IsLatched_(void);
    static Boolean		IsClosed_(void);
};


#endif // SIGMA_BD_CPU
#endif // ServiceSwitch_HH

