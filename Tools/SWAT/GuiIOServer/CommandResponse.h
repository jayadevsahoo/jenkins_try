#pragma once
#include "Socket.h"

class CommandResponse
{
public:
	CommandResponse(Socket* socket);
	~CommandResponse(void);

	INT SendReceive(const CHAR* outBuf, CHAR* inBuf, INT inBufSize);
	bool Send(const CHAR* outBuf);
	INT Receive(CHAR* inBuf, INT inBufSize, LONG timeout=3000);
	static void CancelReceive() { m_cancelReceive = true; }

private:
	bool IsConnected();

	HANDLE m_threadHandle;
	Socket* m_socket;
	CHAR* m_inBuf;
	INT m_inBufSize;

	static bool m_cancelReceive;
};
