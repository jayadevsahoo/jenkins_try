#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
// Class: ScrollableEventLog - A scrollable log of Trend Event IDs and 
//                             descriptions.
//---------------------------------------------------------------------
//@ Interface-Description
//
// A ScrollableEventLog is used to display a log of event ids and event
// detail information. The event information is presented in tabular 
// form, as a table with a set number of rows and columns.
// 
// A scrollbar is provided because the number of entries in the log
// can possibly be more than the number of rows than can be displayed 
// in the table. All scrolling is handled by ScrollableEventLog, not 
// by its creator.  
//
// On construction the ScrollableEventLog class is told how many rows
// and columns will be in the table that it displays. It is also passed
// a LogTarget object. ScrollableEventLog retrieves the textual content
// of any cell in the table via this LogTarget. The textual content can
// be specified in a number of different ways, either as specific cheap
// text or generic text that is auto-formatted within ScrollableMenu,
// thus relieving the application of some of the burden of text
// formatting.
//
// To begin displaying a ScrollableEventLog, the minimum that must be
// done is: construct the ScrollableEventLog, call setColumnInfo() once
// for each column of the ScrollableEventLog, call setEntryInfo() to
// specify the number of entries in the log and call activate() before
// it will be displayed.
//
// Entries in the log can be selected and only one row can be selected
// at any one time.  Selection is hidden to the user.  The application
// has control over the selected row by calling setSelectedEntry().
// Changes by the user to the selected entry are communicated to the
// virtual method via the FocusButtonTarget::adjustKnobHappened().
//
// The application can choose whether the log has a scrollbar, or if the
// log has a scrollbar, whether to allow or disallow scrolling by the
// user.  On construction of ScrollableEventLog the last parameter
// determines whether the log will display a scrollbar.  The
// setDisplayScrollbar() also allows or disallow the scrollbar.
//---------------------------------------------------------------------
//@ Rationale
// Encapsulates the cell formatting functions for the scrollable event
// log.
//---------------------------------------------------------------------
//@ Implementation-Description 
// ScrollableEventLog is a GuiTable. The GuiTable base class controls
// most of the appearance of the window. ScrollableEventLog overrides
// the updateScroll_ method to provide a scrolling mechanism where the
// first entry in the window is the selected entry vs. the base class
// impelementation where the selected row is usually the middle row of
// the table. ScrollableEventLog also provides the refreshCell_ method
// to render the individual LogCell to the screen.
//
// $[TR01200] Touching the EVENT DETAIL button shall result...
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and
// pre-conditions to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
// None of the construction parameters can subsequently be changed after
// construction e.g. you cannot change the number of rows displayed by
// ScrollableEventLog after construction.  
//-------------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/ScrollableEventLog.ccv   25.0.4.0   19 Nov 2013 14:08:22   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By:  rhj    Date:  06-Jun-2007    SCR Number: 6237
//  Project:  Trend
//  Description:
//       Trend releated changes.
//=====================================================================


//@ Usage-Classes
#include "ScrollableEventLog.hh"
#include "LogTarget.hh"
// TODO E600 removed
//#include "Ostream.hh"
//@ End-Usage

#pragma instantiate GuiTable<EVENT_LOG_ROWS,EVENT_LOG_COLS>

//=====================================================================
//
//      Static Data...
//
//=====================================================================

// Initialize static constants.

static const Int SCROLLBAR_WIDTH_ = 10;

//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ScrollableEventLog()  [Constructor]
//
//@ Interface-Description
// Constructs a ScrollableEventLog.  Passed the following parameters:
// >Von
//  eventDetailItems    A LogTarget that provides the list of event IDs
//  					and descriptions of each event.
// 
//  numDisplayRows      Number of rows displayed by the log.
// 
//  numDisplayColumns   Number of columns displayed by the log.
// 
//  rowHeight           The height in pixels of a row (includes the one pixel
//                      of the horizontal dividing line between rows).
// 
//  displayScrollbar_   Pass FALSE if you do not want ScrollableEventLog to have
//                      a scrollbar.  Defaults to TRUE.
// 
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
// Initialize all private data members. Pass along the table size
// parameters and scrollbar to the GuiTable base class.
//---------------------------------------------------------------------
//@ PreCondition
// The pMenuTarget parameter must be non-null. 
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ScrollableEventLog::ScrollableEventLog(  LogTarget &eventDetailItems,
										 Uint16 numDisplayRows,
										 Uint16 numDisplayColumns, 
										 Uint16 rowHeight,
										 Boolean displayScrollbar)
:   GuiTable<EVENT_LOG_ROWS,EVENT_LOG_COLS>(numDisplayRows,
											numDisplayColumns,
											rowHeight,
											scrollbar_,
											displayScrollbar),
	scrollbar_(             rowHeight * numDisplayRows - 1, numDisplayRows),
	rLogTarget(				eventDetailItems)
{
	horizontalLineColor_ = Colors::MEDIUM_BLUE;
	scrollbar_.setWidth(SCROLLBAR_WIDTH_);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~ScrollableEventLog()  [Destructor]
//
//@ Interface-Description
// ScrollableEventLog destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ScrollableEventLog::~ScrollableEventLog(void)
{
	CALL_TRACE("ScrollableEventLog::~ScrollableEventLog(void)");

	// Do nothing
}




//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustKnobHappened [virtual]
//
//@ Interface-Description
//  This is a virtual method inherited from FocusButtonTarget.  Called
//  when the operator turns the knob.  It is passed an integer 
//  corresponding to the amount the knob was turned. The table is
//  scrolled one row at a time no matter what "delta" is passed. 
//---------------------------------------------------------------------
//@ Implementation-Description 
//  A positive "delta" scrolls the table backward one row. A negative
//  "delta" scrolls the table forward one row. This method limits the
//  new selected row (top row in the window) to the first entry in the
//  data table and to the last entry minus the number of rows in the
//  display window. This new selected entry is then passed to
//  setSelectedEntry() to move to and display the new selection.
//  Note: for the ScrollableEventLog, the selectedEntry_ becomes
//  synonymous with the firstDisplayedEntry_ when passed through
//  setSelectedEntry and updateScroll_.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void ScrollableEventLog::adjustKnobHappened(Int32 delta)
{
	CALL_TRACE("ScrollableEventLog::adjustKnobHappened(Int32 delta)");

	// no scrolling required if we're already showing all table entries
	if (numEntries_ <= NUM_DISPLAY_ROWS_)
	{
		return;
	}

	Int16 newSelectedEntry = selectedEntry_;

	if (delta > 0)
	{
		newSelectedEntry--;

		if (newSelectedEntry < 0)
		{
			newSelectedEntry = 0;
		}
	}
	else
	{
		newSelectedEntry++;

		if (newSelectedEntry > numEntries_ - NUM_DISPLAY_ROWS_)
		{
			newSelectedEntry = numEntries_ - NUM_DISPLAY_ROWS_;
		}
	}

	setSelectedEntry(newSelectedEntry);

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateScroll_
//
//@ Interface-Description 
//  The newSelectedEntry parameter specifies which entry in the table to
//  display at the top of the scrollable display window. This method
//  scrolls or slews to the selected entry and displays it at the top of
//  the scrollable window.
//---------------------------------------------------------------------
//@ Implementation-Description 
//  The newSelectedEntry parameter specifies which entry in the table to
//  display at the top of the scrollable display window. Based on the
//  previous first displayed entry, this method determines how many
//  rows to scroll or slew to display this entry at the top of the
//  window. For scrolling a single row up or down, the method uses the
//  DirectDraw feature of the graphics library to move the data in the
//  frame buffer and then refreshes the new top or bottom row. For
//  slewing more than one row, the method simply refreshes the entire
//  table.
//---------------------------------------------------------------------
//@ PreCondition
//  The newSelectedEntry parameter must be less than numEntries_.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void ScrollableEventLog::updateScroll_(Uint16 newSelectedEntry)
{
	AUX_CLASS_PRE_CONDITION(newSelectedEntry < numEntries_, newSelectedEntry);

	// Determine the new first displayed entry and the number of rows to scroll
	selectedEntry_ = newSelectedEntry;

	Uint16 newFirstDisplayedEntry = newSelectedEntry;
	Int32 numRowsToScroll = newFirstDisplayedEntry - firstDisplayedEntry_;

	firstDisplayedEntry_ = newFirstDisplayedEntry;

	// uses directDraw so we need a base container so we need to be visible
	if (!isVisible())
	{
		return;
	}

	if (ABS_VALUE(numRowsToScroll) > 1)
	{
		// multiple row scroll -- just refresh all cells
		refreshAll_();
		updateScrollbar_();
	}
	else if (numRowsToScroll > 0)
	{
		// scroll up one row

		setDirectDraw(TRUE);

		// First update the entire table so its Drawabes are updated. Since
		// DirectDraw is enable, nothing is added to changed Drawables list
		// so no background refresh occurs.
		refreshAll_();

		// now draw everything directly in the frame buffer using BitBlt ops
		// Container::scroll() and Drawable::drawDirect()
		// 
		// define scroll area
		Area area1( 0, logYOffset_ + ROW_HEIGHT_, 
					getWidth() - rScrollbar_.getWidth() - 2, 
					logYOffset_ + NUM_DISPLAY_ROWS_ * ROW_HEIGHT_ - 2 );

	    // scroll area up one row
		scroll(area1, 0, -ROW_HEIGHT_);

		// Draw bottom row
		drawDirectRow_(NUM_DISPLAY_ROWS_ - 1);

		setDirectDraw(FALSE);

		updateScrollbar_();
	}
	else if (numRowsToScroll < 0)
	{
		// scroll down one row

		setDirectDraw(TRUE);

		// First update the entire table so its Drawabes are updated. Since
		// DirectDraw is enable, nothing is added to changed Drawables list
		// so no background refresh occurs.
		refreshAll_();

		// now draw everything directly in the frame buffer using BitBlt ops
		// Container::scroll() and Drawable::drawDirect()
		// 
		// define scroll area
		Area area2( 0, logYOffset_,
					getWidth() - rScrollbar_.getWidth() - 2, 
					logYOffset_ + (NUM_DISPLAY_ROWS_ - 1) * ROW_HEIGHT_ - 3 );

		// scroll area down 
		scroll(area2, 0, ROW_HEIGHT_);

		// Refresh/draw top rows
		drawDirectRow_(0);

		setDirectDraw(FALSE);

		updateScrollbar_();
	}
	else
	{
		// nothing to scroll -- do nothing
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: refreshCell_ [virtual, private]
//
//@ Interface-Description
// Refreshes the contents of a single cell.  Passed the row and column
// number of the cell.
//---------------------------------------------------------------------
//@ Implementation-Description
// Calculates the entry number passed to the LogTarget from the
// specified row number and the current firstDisplayedEntry_.
// 
// Formats each cell as white text on a medium blue background. There is
// no special formatting required for selected rows.
// 
// Uses LogTarget::getLogEntryColumn to retrieve the data contents
// of the cell and LogCell::setText to format the text and present
// it in the table cell.
//---------------------------------------------------------------------
//@ PreCondition
// The specified row and column must be in range of the displayed table.
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void ScrollableEventLog::refreshCell_(Uint16 row, Uint16 col)
{

	CALL_TRACE("ScrollableEventLog::refreshCell_(Uint16 row, Uint16 col)");

	AUX_CLASS_PRE_CONDITION(row < NUM_DISPLAY_ROWS_, row);
	AUX_CLASS_PRE_CONDITION(col < NUM_DISPLAY_COLUMNS_, col);

	StringId string1 = NULL_STRING_ID;
	StringId string2 = NULL_STRING_ID;
	StringId string3 = NULL_STRING_ID;

	Boolean isCheapText = FALSE;
	StringId string1Message = NULL_STRING_ID;
	Uint16 entryNumber = firstDisplayedEntry_ + row;

	// First, check if cell is on a row that corresponds to a log entry
	if (entryNumber >= numEntries_)
	{
		// we should never get here 
		AUX_CLASS_ASSERTION_FAILURE(entryNumber);
	}
	else
	{
		// Render the cell in normal color
		cells_[row][col].setFillColor(Colors::MEDIUM_BLUE);
		cells_[row][col].setContentsColor(Colors::WHITE);

		// Ask the EventDataItems for the contents of the menu's column
		rLogTarget.getLogEntryColumn(entryNumber, col, isCheapText, string1,
									 string2,string3,string1Message);

		// Add the event text to the specific cell.
		cells_[row][col].setText(isCheapText,
								 string1, string2, string3,
								 columnInfos_[col].alignment,
								 columnInfos_[col].fontStyle,
								 columnInfos_[col].oneLineFontSize,
								 columnInfos_[col].margin,
								 string1Message);
	}
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//        [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition   
//  none
//@ End-Method
//=====================================================================

void ScrollableEventLog::SoftFault(const SoftFaultID  softFaultID,
								   const Uint32       lineNumber,
								   const char*        pFileName,
								   const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, SCROLLABLEEVENTLOG,
							lineNumber, pFileName, pPredicate);
}


