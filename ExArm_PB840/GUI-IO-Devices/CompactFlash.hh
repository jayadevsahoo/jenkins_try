#ifndef CompactFlash_HH
#define CompactFlash_HH


//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
// Class: CompactFlash.hh - An encapsulation of the Compact Flash interface
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-IO-Devices/vcssrc/CompactFlash.hhv   25.0.4.0   19 Nov 2013 14:12:18   pvcs  $
//
//@ Modification-Log
//  Revision: 001  By:  mjf    Date:  22-Aug-2006    SCR Number: 6237
//       Project:  Trend
//       Description:
//             Initial version
// 
//=====================================================================

//@ Usage-Classes
//@ End-Usage

#include "Sigma.hh"

// Forward declare
struct CompactFlashTaskFileRegs;
struct CompactFlashOptionReg;
struct CompactFlashAlternateReg;


class CompactFlash
{
public:                               

	//@ Type: CFStatus
	// Status of the compact flash interface
	enum CfStatus
	{
		// Initialize() has not been called
		NOT_INITIALIZED = 0,    

		// No errors
		STATUS_OK       = 1,    

		// Error in interfacing with the FPGA
		FPGA_ERROR      = 2,     

		// No compact card detected
		NO_CARD_ERROR   = 3,    

		// Compact flash reported an error
		CARD_ERROR      = 4 
	};

	//@ Type: BLOCK_SIZE
	// An enumeration type listing the size of the compact flash block
	// in byte pairs (16 bits)
	enum
	{
		BLOCK_SIZE = 256
	};

	//@ Type: TEST_BLOCK
	// The logical block used to verify the compact flash
	// other applications should not use this block
	// as it will be over written in selfTest()
	enum
	{
		TEST_BLOCK = 0
	};

	//@ Type: BlockPtr
	// Type to an array of 256 short (16bit) elements
	typedef Uint16 Block[ BLOCK_SIZE ], *BlockPtr;

	static void Initialize( void );
	static CompactFlash & GetCompactFlash( void );

	SigmaStatus open( void );
	SigmaStatus read( Uint32 blockLbaNum, BlockPtr pBlockData, Uint8 numBlocks = 1 );
	SigmaStatus write( Uint32 blocLbakNum, const BlockPtr pBlockData, Uint8 numBlocks = 1 );
	SigmaStatus selfTest( Boolean isExtended );

	inline Uint32 getCapacity( void ) const;
	inline CfStatus getStatus( void ) const;

private:

	CompactFlash( Uint32 cfOptionRegAddr,
				  Uint32 cfTaskFileRegsAddr,
				  Uint32 cfAltRegAddr );
	~CompactFlash( void );

	// copy constructor & assignment operator not implemented 
	CompactFlash( const CompactFlash& );           
	void operator=( const CompactFlash& );         

	SigmaStatus open_( void );
	SigmaStatus read_( Uint32 blockLbaNum, BlockPtr pBlockData, Uint8 numBlocks );
	SigmaStatus write_( Uint32 blockLbaNum, const BlockPtr pBlockData, Uint8 numBlocks );
	SigmaStatus selfTest_( Boolean extened );

	SigmaStatus sendCommand_( Uint8 ataCmd, Int32 blockkLbaNum = -1, Uint8 blockCnt = 1 );
	SigmaStatus waitDriveReady_( void );
	SigmaStatus loadBlock_( CompactFlash::BlockPtr pBlockData );
	SigmaStatus storeBlock_( const CompactFlash::BlockPtr pBlockData );


	//@ Data-Member: status_
	// Status of the compact flash interface
	CfStatus        status_;

	//@ Data-Member: capacity_
	// Capacity of the compact flash in blocks
	Uint32          capacity_;

	//@ Data-Member: optionReg_
	// An address to the FPGA register for the compact flash
	// configuration options
	volatile CompactFlashOptionReg *        pOptionReg_;

	//@ Data-Member: taskFileReg_
	// An address to the FPGA register for the compact flash
	// Task file
	volatile CompactFlashTaskFileRegs *     pTaskFileRegs_;

	//@ Data-Member: alternateReg_
	// An address to the FPGA register for the compact flash
	// Alternate status/control
	volatile CompactFlashAlternateReg *    pAlternateReg_;


};

// Inlined methods
#include "CompactFlash.in"

#endif // CompactFlash_HH

