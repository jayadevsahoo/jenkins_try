# Splits the langauge file into 2 files. One file contains the entire language except for
# the last line; the other contains just the last line. Later, the untranslated english
# is concatenated with (in fact between) these 2 files.
BEGIN{
  print "" > "languageWithoutLastLine.tmp"
  print "" > "languageWithLastLine.tmp"
}

{
  if ($0 !~/endif/)
    print $0 >> "languageWithoutLastLine.tmp"
  else
    print $0 >> "languageWithLastLine.tmp"
}
