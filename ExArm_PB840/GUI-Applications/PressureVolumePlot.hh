#ifndef PressureVolumePlot_HH
#define PressureVolumePlot_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: PressureVolumePlot - A loop plot which plots pressure against
// volume and displays the inspiratory area of the loop.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/PressureVolumePlot.hhv   25.0.4.0   19 Nov 2013 14:08:16   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//====================================================================

#include "LoopPlot.hh"

//@ Usage-Classes
#include "Box.hh"
#include "NumericField.hh"
#include "TextField.hh"
#include "GuiAppClassIds.hh"
//@ End-Usage

class WaveformIntervalIter;

class PressureVolumePlot : public LoopPlot
{
public:
	PressureVolumePlot(void);
	~PressureVolumePlot(void);

	virtual void update(WaveformIntervalIter& segmentIter);
	virtual void erase(void);

	virtual void setGridArea(Uint32 x, Uint32 y, Uint32 width, Uint32 height);

	static void SoftFault(const SoftFaultID softFaultID,
							const Uint32      lineNumber,
							const char*       pFileName  = NULL, 
							const char*       pPredicate = NULL);

protected:
	virtual void layoutGrid_(void);

private:
	// these methods are purposely declared, but not implemented...
	PressureVolumePlot(const PressureVolumePlot&);		// not implemented...
	void   operator=(const PressureVolumePlot&);	// not implemented...

	//@ Data-Member: inspAreaBox_
	// The box which forms a border around the inspiratory area value.
	Box inspAreaBox_;

	//@ Data-Member: inspAreaText_
	// The text which labels the inspiratory area value.
	TextField inspAreaText_;

	//@ Data-Member: inspAreaValue_
	// A numeric field which displays the inspiratory area value.
	NumericField inspAreaValue_;

	//@ Data-Member: workIndex_
	// The work index, the inspiratory area in liters times centimeters of
	// water.
	Real32 workIndex_;

};

#endif // PressureVolumePlot_HH 
