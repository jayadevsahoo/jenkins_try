//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file PatientDataUIUserInt.cpp
//----------------------------------------------------------------------------

//-------------------------------------
// I N C L U D E S
//-------------------------------------
#include"PatientDataUIUserInt.h"

//-------------------------------------
// C O D E
//-------------------------------------

UserInt::UserInt(char *pName,int i):Value(pName),m_iValue(i)
{
}
    

void UserInt::updatePacket(void *address)
{
    if(m_bIsDesired)
    {
        ((unsigned int *)address)[0]=htonl(*((unsigned int *)(&m_iDesiredValue)));
    }
}


int UserInt::getIntValue()
{
    return m_iValue;
}

bool UserInt::isInt()
{
    return true;
}

bool UserInt::setInt(int iValue)
{
    m_iDesiredValue     = iValue;
    m_bIsDesired        = true;
    return true;    
}

bool UserInt::updateValue(int fIn)
{
    m_bIsSet=true;
    m_iValue=fIn;
    return true;
}
